// 
// Decompiled by Procyon v0.6.0
// 

package androidx.compose.runtime.collection;

import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import java.util.Arrays;
import kotlin.collections.ArraysKt;
import kotlin.jvm.internal.Intrinsics;
import androidx.compose.runtime.ActualJvm_jvmKt;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000F\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0011\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\b\n\u0002\b\u0006\n\u0002\u0010\u0015\n\u0002\b\r\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\r\b\u0000\u0018\u0000*\b\b\u0000\u0010\u0001*\u00020\u00022\u00020\u0002B\u0005¢\u0006\u0002\u0010\u0003J\u001b\u0010\"\u001a\u00020#2\u0006\u0010$\u001a\u00020\u00022\u0006\u0010%\u001a\u00028\u0000¢\u0006\u0002\u0010&J\u0006\u0010'\u001a\u00020(J\u0011\u0010)\u001a\u00020#2\u0006\u0010*\u001a\u00020\u0002H\u0086\u0002J\u0012\u0010+\u001a\u00020\u000e2\b\u0010$\u001a\u0004\u0018\u00010\u0002H\u0002J\"\u0010,\u001a\u00020\u000e2\u0006\u0010-\u001a\u00020\u000e2\b\u0010$\u001a\u0004\u0018\u00010\u00022\u0006\u0010.\u001a\u00020\u000eH\u0002J7\u0010/\u001a\u00020(2\u0006\u0010$\u001a\u00020\u00022!\u00100\u001a\u001d\u0012\u0013\u0012\u00118\u0000¢\u0006\f\b2\u0012\b\b3\u0012\u0004\b\b(%\u0012\u0004\u0012\u00020(01H\u0086\b\u00f8\u0001\u0000J\u0016\u00104\u001a\b\u0012\u0004\u0012\u00028\u00000\u00062\u0006\u0010$\u001a\u00020\u0002H\u0002J\u001b\u00105\u001a\u00020#2\u0006\u0010$\u001a\u00020\u00022\u0006\u0010%\u001a\u00028\u0000¢\u0006\u0002\u0010&J\u0013\u00106\u001a\u00020(2\u0006\u0010%\u001a\u00028\u0000¢\u0006\u0002\u00107J/\u00108\u001a\u00020(2!\u00109\u001a\u001d\u0012\u0013\u0012\u00118\u0000¢\u0006\f\b2\u0012\b\b3\u0012\u0004\b\b(%\u0012\u0004\u0012\u00020#01H\u0086\b\u00f8\u0001\u0000J#\u0010:\u001a\u00020(2\u0018\u0010;\u001a\u0014\u0012\n\u0012\b\u0012\u0004\u0012\u00028\u00000\u0006\u0012\u0004\u0012\u00020(01H\u0082\bJ\u0016\u0010<\u001a\b\u0012\u0004\u0012\u00028\u00000\u00062\u0006\u0010=\u001a\u00020\u000eH\u0002J\u0011\u0010>\u001a\u00020\u00022\u0006\u0010=\u001a\u00020\u000eH\u0082\bR4\u0010\u0004\u001a\u0010\u0012\f\u0012\n\u0012\u0004\u0012\u00028\u0000\u0018\u00010\u00060\u00058\u0000@\u0000X\u0081\u000e¢\u0006\u0016\n\u0002\u0010\f\u0012\u0004\b\u0007\u0010\u0003\u001a\u0004\b\b\u0010\t\"\u0004\b\n\u0010\u000bR$\u0010\r\u001a\u00020\u000e8\u0000@\u0000X\u0081\u000e¢\u0006\u0014\n\u0000\u0012\u0004\b\u000f\u0010\u0003\u001a\u0004\b\u0010\u0010\u0011\"\u0004\b\u0012\u0010\u0013R$\u0010\u0014\u001a\u00020\u00158\u0000@\u0000X\u0081\u000e¢\u0006\u0014\n\u0000\u0012\u0004\b\u0016\u0010\u0003\u001a\u0004\b\u0017\u0010\u0018\"\u0004\b\u0019\u0010\u001aR.\u0010\u001b\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00020\u00058\u0000@\u0000X\u0081\u000e¢\u0006\u0016\n\u0002\u0010!\u0012\u0004\b\u001c\u0010\u0003\u001a\u0004\b\u001d\u0010\u001e\"\u0004\b\u001f\u0010 \u0082\u0002\u0007\n\u0005\b\u009920\u0001¨\u0006?" }, d2 = { "Landroidx/compose/runtime/collection/IdentityScopeMap;", "T", "", "()V", "scopeSets", "", "Landroidx/compose/runtime/collection/IdentityArraySet;", "getScopeSets$annotations", "getScopeSets", "()[Landroidx/compose/runtime/collection/IdentityArraySet;", "setScopeSets", "([Landroidx/compose/runtime/collection/IdentityArraySet;)V", "[Landroidx/compose/runtime/collection/IdentityArraySet;", "size", "", "getSize$annotations", "getSize", "()I", "setSize", "(I)V", "valueOrder", "", "getValueOrder$annotations", "getValueOrder", "()[I", "setValueOrder", "([I)V", "values", "getValues$annotations", "getValues", "()[Ljava/lang/Object;", "setValues", "([Ljava/lang/Object;)V", "[Ljava/lang/Object;", "add", "", "value", "scope", "(Ljava/lang/Object;Ljava/lang/Object;)Z", "clear", "", "contains", "element", "find", "findExactIndex", "midIndex", "valueHash", "forEachScopeOf", "block", "Lkotlin/Function1;", "Lkotlin/ParameterName;", "name", "getOrCreateIdentitySet", "remove", "removeScope", "(Ljava/lang/Object;)V", "removeValueIf", "predicate", "removingScopes", "removalOperation", "scopeSetAt", "index", "valueAt", "runtime_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
public final class IdentityScopeMap<T>
{
    private IdentityArraySet<T>[] scopeSets;
    private int size;
    private int[] valueOrder;
    private Object[] values;
    
    public IdentityScopeMap() {
        final int[] valueOrder = new int[50];
        for (int i = 0; i < 50; ++i) {
            valueOrder[i] = i;
        }
        this.valueOrder = valueOrder;
        this.values = new Object[50];
        this.scopeSets = new IdentityArraySet[50];
    }
    
    private final int find(final Object o) {
        final int identityHashCode = ActualJvm_jvmKt.identityHashCode(o);
        int n = this.size - 1;
        int i = 0;
        while (i <= n) {
            final int n2 = i + n >>> 1;
            final Object o2 = this.values[this.valueOrder[n2]];
            Intrinsics.checkNotNull(o2);
            final int identityHashCode2 = ActualJvm_jvmKt.identityHashCode(o2);
            if (identityHashCode2 < identityHashCode) {
                i = n2 + 1;
            }
            else if (identityHashCode2 > identityHashCode) {
                n = n2 - 1;
            }
            else {
                if (o == o2) {
                    return n2;
                }
                return this.findExactIndex(n2, o, identityHashCode);
            }
        }
        return -(i + 1);
    }
    
    private final int findExactIndex(int i, final Object o, final int n) {
        for (int n2 = i - 1; -1 < n2; --n2) {
            final Object o2 = this.values[this.valueOrder[n2]];
            Intrinsics.checkNotNull(o2);
            if (o2 == o) {
                return n2;
            }
            if (ActualJvm_jvmKt.identityHashCode(o2) != n) {
                break;
            }
        }
        ++i;
        while (i < this.size) {
            final Object o3 = this.values[this.valueOrder[i]];
            Intrinsics.checkNotNull(o3);
            if (o3 == o) {
                return i;
            }
            if (ActualJvm_jvmKt.identityHashCode(o3) != n) {
                return -(i + 1);
            }
            ++i;
        }
        i = this.size;
        return -(i + 1);
    }
    
    private final IdentityArraySet<T> getOrCreateIdentitySet(final Object o) {
        int n;
        if (this.size > 0) {
            final int find = this.find(o);
            if ((n = find) >= 0) {
                return this.scopeSetAt(find);
            }
        }
        else {
            n = -1;
        }
        final int n2 = -(n + 1);
        final int size = this.size;
        final int[] valueOrder = this.valueOrder;
        if (size < valueOrder.length) {
            final int n3 = valueOrder[size];
            this.values[n3] = o;
            IdentityArraySet<T> set;
            if ((set = this.scopeSets[n3]) == null) {
                set = new IdentityArraySet<T>();
                this.scopeSets[n3] = set;
            }
            final int size2 = this.size;
            if (n2 < size2) {
                final int[] valueOrder2 = this.valueOrder;
                ArraysKt.copyInto(valueOrder2, valueOrder2, n2 + 1, n2, size2);
            }
            this.valueOrder[n2] = n3;
            ++this.size;
            return set;
        }
        final int n4 = valueOrder.length * 2;
        final IdentityArraySet<T>[] copy = Arrays.copyOf(this.scopeSets, n4);
        Intrinsics.checkNotNullExpressionValue((Object)copy, "copyOf(this, newSize)");
        this.scopeSets = copy;
        final IdentityArraySet<T> set2 = new IdentityArraySet<T>();
        this.scopeSets[size] = set2;
        final Object[] copy2 = Arrays.copyOf(this.values, n4);
        Intrinsics.checkNotNullExpressionValue((Object)copy2, "copyOf(this, newSize)");
        (this.values = copy2)[size] = o;
        final int[] valueOrder3 = new int[n4];
        int size3 = this.size;
        while (++size3 < n4) {
            valueOrder3[size3] = size3;
        }
        final int size4 = this.size;
        if (n2 < size4) {
            ArraysKt.copyInto(this.valueOrder, valueOrder3, n2 + 1, n2, size4);
        }
        valueOrder3[n2] = size;
        if (n2 > 0) {
            ArraysKt.copyInto$default(this.valueOrder, valueOrder3, 0, 0, n2, 6, (Object)null);
        }
        this.valueOrder = valueOrder3;
        ++this.size;
        return set2;
    }
    
    private final void removingScopes(final Function1<? super IdentityArraySet<T>, Unit> function1) {
        final int size = this.getSize();
        int i = 0;
        int size2 = 0;
        while (i < size) {
            final int n = this.getValueOrder()[i];
            final IdentityArraySet<T> set = this.getScopeSets()[n];
            Intrinsics.checkNotNull((Object)set);
            function1.invoke((Object)set);
            int n2 = size2;
            if (set.size() > 0) {
                if (size2 != i) {
                    final int n3 = this.getValueOrder()[size2];
                    this.getValueOrder()[size2] = n;
                    this.getValueOrder()[i] = n3;
                }
                n2 = size2 + 1;
            }
            ++i;
            size2 = n2;
        }
        for (int size3 = this.getSize(), j = size2; j < size3; ++j) {
            this.getValues()[this.getValueOrder()[j]] = null;
        }
        this.setSize(size2);
    }
    
    private final IdentityArraySet<T> scopeSetAt(final int n) {
        final IdentityArraySet<T> set = this.scopeSets[this.valueOrder[n]];
        Intrinsics.checkNotNull((Object)set);
        return set;
    }
    
    private final Object valueAt(final int n) {
        final Object o = this.values[this.valueOrder[n]];
        Intrinsics.checkNotNull(o);
        return o;
    }
    
    public final boolean add(final Object o, final T t) {
        Intrinsics.checkNotNullParameter(o, "value");
        Intrinsics.checkNotNullParameter((Object)t, "scope");
        return this.getOrCreateIdentitySet(o).add(t);
    }
    
    public final void clear() {
        for (int length = this.scopeSets.length, i = 0; i < length; ++i) {
            final IdentityArraySet<T> set = this.scopeSets[i];
            if (set != null) {
                set.clear();
            }
            this.valueOrder[i] = i;
            this.values[i] = null;
        }
        this.size = 0;
    }
    
    public final boolean contains(final Object o) {
        Intrinsics.checkNotNullParameter(o, "element");
        return this.find(o) >= 0;
    }
    
    public final void forEachScopeOf(final Object o, final Function1<? super T, Unit> function1) {
        Intrinsics.checkNotNullParameter(o, "value");
        Intrinsics.checkNotNullParameter((Object)function1, "block");
        final int access$find = this.find(o);
        if (access$find >= 0) {
            final IdentityArraySet access$scopeSet = this.scopeSetAt(access$find);
            for (int i = 0; i < access$scopeSet.size(); ++i) {
                function1.invoke(access$scopeSet.get(i));
            }
        }
    }
    
    public final IdentityArraySet<T>[] getScopeSets() {
        return this.scopeSets;
    }
    
    public final int getSize() {
        return this.size;
    }
    
    public final int[] getValueOrder() {
        return this.valueOrder;
    }
    
    public final Object[] getValues() {
        return this.values;
    }
    
    public final boolean remove(final Object o, final T t) {
        Intrinsics.checkNotNullParameter(o, "value");
        Intrinsics.checkNotNullParameter((Object)t, "scope");
        final int find = this.find(o);
        if (find < 0) {
            return false;
        }
        final int n = this.valueOrder[find];
        final IdentityArraySet<T> set = this.scopeSets[n];
        if (set == null) {
            return false;
        }
        final boolean remove = set.remove(t);
        if (set.size() == 0) {
            final int n2 = find + 1;
            final int size = this.size;
            if (n2 < size) {
                final int[] valueOrder = this.valueOrder;
                ArraysKt.copyInto(valueOrder, valueOrder, find, n2, size);
            }
            final int[] valueOrder2 = this.valueOrder;
            final int size2 = this.size;
            valueOrder2[size2 - 1] = n;
            this.values[n] = null;
            this.size = size2 - 1;
        }
        return remove;
    }
    
    public final void removeScope(final T t) {
        Intrinsics.checkNotNullParameter((Object)t, "scope");
        final int size = this.getSize();
        int i = 0;
        int size2 = 0;
        while (i < size) {
            final int n = this.getValueOrder()[i];
            final IdentityArraySet<T> set = this.getScopeSets()[n];
            Intrinsics.checkNotNull((Object)set);
            set.remove(t);
            int n2 = size2;
            if (set.size() > 0) {
                if (size2 != i) {
                    final int n3 = this.getValueOrder()[size2];
                    this.getValueOrder()[size2] = n;
                    this.getValueOrder()[i] = n3;
                }
                n2 = size2 + 1;
            }
            ++i;
            size2 = n2;
        }
        for (int size3 = this.getSize(), j = size2; j < size3; ++j) {
            this.getValues()[this.getValueOrder()[j]] = null;
        }
        this.setSize(size2);
    }
    
    public final void removeValueIf(final Function1<? super T, Boolean> function1) {
        Intrinsics.checkNotNullParameter((Object)function1, "predicate");
        final int size = this.getSize();
        int i = 0;
        int size2 = 0;
        while (i < size) {
            final int n = this.getValueOrder()[i];
            final IdentityArraySet<T> set = this.getScopeSets()[n];
            Intrinsics.checkNotNull((Object)set);
            final int size3 = set.size();
            int j = 0;
            int size4 = 0;
            while (j < size3) {
                final Object o = set.getValues()[j];
                Intrinsics.checkNotNull(o, "null cannot be cast to non-null type T of androidx.compose.runtime.collection.IdentityArraySet");
                int n2 = size4;
                if (!(boolean)function1.invoke(o)) {
                    if (size4 != j) {
                        set.getValues()[size4] = o;
                    }
                    n2 = size4 + 1;
                }
                ++j;
                size4 = n2;
            }
            for (int size5 = set.size(), k = size4; k < size5; ++k) {
                set.getValues()[k] = null;
            }
            set.setSize(size4);
            int n3 = size2;
            if (set.size() > 0) {
                if (size2 != i) {
                    final int n4 = this.getValueOrder()[size2];
                    this.getValueOrder()[size2] = n;
                    this.getValueOrder()[i] = n4;
                }
                n3 = size2 + 1;
            }
            ++i;
            size2 = n3;
        }
        for (int size6 = this.getSize(), l = size2; l < size6; ++l) {
            this.getValues()[this.getValueOrder()[l]] = null;
        }
        this.setSize(size2);
    }
    
    public final void setScopeSets(final IdentityArraySet<T>[] scopeSets) {
        Intrinsics.checkNotNullParameter((Object)scopeSets, "<set-?>");
        this.scopeSets = scopeSets;
    }
    
    public final void setSize(final int size) {
        this.size = size;
    }
    
    public final void setValueOrder(final int[] valueOrder) {
        Intrinsics.checkNotNullParameter((Object)valueOrder, "<set-?>");
        this.valueOrder = valueOrder;
    }
    
    public final void setValues(final Object[] values) {
        Intrinsics.checkNotNullParameter((Object)values, "<set-?>");
        this.values = values;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.compose.runtime.collection;

import kotlin.jvm.internal.markers.KMutableListIterator;
import kotlin.jvm.internal.CollectionToArray;
import java.util.ListIterator;
import kotlin.jvm.internal.markers.KMutableList;
import java.util.Comparator;
import kotlin.Unit;
import kotlin.jvm.functions.Function3;
import kotlin.jvm.functions.Function2;
import kotlin.KotlinNothingValueException;
import java.util.NoSuchElementException;
import java.util.Arrays;
import kotlin.ranges.IntRange;
import kotlin.jvm.functions.Function1;
import java.util.Iterator;
import kotlin.collections.CollectionsKt;
import java.util.Collection;
import kotlin.collections.ArraysKt;
import kotlin.jvm.internal.Intrinsics;
import java.util.List;
import kotlin.Metadata;
import java.util.RandomAccess;

@Metadata(d1 = { "\u0000z\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0011\n\u0000\n\u0002\u0010\b\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010!\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0005\n\u0002\u0010\u001e\n\u0002\u0010 \n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0010\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b)\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0001\n\u0002\b\u0004\b\u0007\u0018\u0000*\u0004\b\u0000\u0010\u00012\u00060\u0002j\u0002`\u0003:\u0003pqrB\u001f\b\u0001\u0012\u000e\u0010\u0004\u001a\n\u0012\u0006\u0012\u0004\u0018\u00018\u00000\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007¢\u0006\u0002\u0010\bJ\u0013\u0010\u001b\u001a\u00020\u001c2\u0006\u0010\u001d\u001a\u00028\u0000¢\u0006\u0002\u0010\u001eJ\u001b\u0010\u001b\u001a\u00020\u001f2\u0006\u0010 \u001a\u00020\u00072\u0006\u0010\u001d\u001a\u00028\u0000¢\u0006\u0002\u0010!J\u0017\u0010\"\u001a\u00020\u001c2\f\u0010#\u001a\b\u0012\u0004\u0012\u00028\u00000\u0000H\u0086\bJ\u0019\u0010\"\u001a\u00020\u001c2\f\u0010#\u001a\b\u0012\u0004\u0012\u00028\u00000\u0005¢\u0006\u0002\u0010$J\u001c\u0010\"\u001a\u00020\u001c2\u0006\u0010 \u001a\u00020\u00072\f\u0010#\u001a\b\u0012\u0004\u0012\u00028\u00000\u0000J\u001c\u0010\"\u001a\u00020\u001c2\u0006\u0010 \u001a\u00020\u00072\f\u0010#\u001a\b\u0012\u0004\u0012\u00028\u00000%J\u001c\u0010\"\u001a\u00020\u001c2\u0006\u0010 \u001a\u00020\u00072\f\u0010#\u001a\b\u0012\u0004\u0012\u00028\u00000&J\u0014\u0010\"\u001a\u00020\u001c2\f\u0010#\u001a\b\u0012\u0004\u0012\u00028\u00000%J\u0017\u0010\"\u001a\u00020\u001c2\f\u0010#\u001a\b\u0012\u0004\u0012\u00028\u00000&H\u0086\bJ+\u0010'\u001a\u00020\u001c2\u0012\u0010(\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00020\u001c0)H\u0086\b\u00f8\u0001\u0000\u0082\u0002\b\n\u0006\b\u0001\u0012\u0002\u0010\u0001J\f\u0010*\u001a\b\u0012\u0004\u0012\u00028\u00000\u0018J\u0006\u0010+\u001a\u00020\u001fJ\u0016\u0010,\u001a\u00020\u001c2\u0006\u0010\u001d\u001a\u00028\u0000H\u0086\u0002¢\u0006\u0002\u0010\u001eJ\u0014\u0010-\u001a\u00020\u001c2\f\u0010#\u001a\b\u0012\u0004\u0012\u00028\u00000\u0000J\u0014\u0010-\u001a\u00020\u001c2\f\u0010#\u001a\b\u0012\u0004\u0012\u00028\u00000%J\u0014\u0010-\u001a\u00020\u001c2\f\u0010#\u001a\b\u0012\u0004\u0012\u00028\u00000&J\u0014\u0010.\u001a\u00020\u001c2\f\u0010/\u001a\b\u0012\u0004\u0012\u00028\u00000\u0000J\u000e\u00100\u001a\u00020\u001f2\u0006\u00101\u001a\u00020\u0007J\u000b\u00102\u001a\u00028\u0000¢\u0006\u0002\u00103J0\u00102\u001a\u00028\u00002\u0012\u0010(\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00020\u001c0)H\u0086\b\u00f8\u0001\u0000\u0082\u0002\b\n\u0006\b\u0001\u0012\u0002\u0010\u0001¢\u0006\u0002\u00104J\u0010\u00105\u001a\u0004\u0018\u00018\u0000H\u0086\b¢\u0006\u0002\u00103J2\u00105\u001a\u0004\u0018\u00018\u00002\u0012\u0010(\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00020\u001c0)H\u0086\b\u00f8\u0001\u0000\u0082\u0002\b\n\u0006\b\u0001\u0012\u0002\u0010\u0001¢\u0006\u0002\u00104JS\u00106\u001a\u0002H7\"\u0004\b\u0001\u001072\u0006\u00108\u001a\u0002H72'\u00109\u001a#\u0012\u0013\u0012\u0011H7¢\u0006\f\b;\u0012\b\b<\u0012\u0004\b\b(=\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u0002H70:H\u0086\b\u00f8\u0001\u0000\u0082\u0002\b\n\u0006\b\u0001\u0012\u0002\u0010\u0002¢\u0006\u0002\u0010>Jh\u0010?\u001a\u0002H7\"\u0004\b\u0001\u001072\u0006\u00108\u001a\u0002H72<\u00109\u001a8\u0012\u0013\u0012\u00110\u0007¢\u0006\f\b;\u0012\b\b<\u0012\u0004\b\b( \u0012\u0013\u0012\u0011H7¢\u0006\f\b;\u0012\b\b<\u0012\u0004\b\b(=\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u0002H70@H\u0086\b\u00f8\u0001\u0000\u0082\u0002\b\n\u0006\b\u0001\u0012\u0002\u0010\u0002¢\u0006\u0002\u0010AJS\u0010B\u001a\u0002H7\"\u0004\b\u0001\u001072\u0006\u00108\u001a\u0002H72'\u00109\u001a#\u0012\u0004\u0012\u00028\u0000\u0012\u0013\u0012\u0011H7¢\u0006\f\b;\u0012\b\b<\u0012\u0004\b\b(=\u0012\u0004\u0012\u0002H70:H\u0086\b\u00f8\u0001\u0000\u0082\u0002\b\n\u0006\b\u0001\u0012\u0002\u0010\u0002¢\u0006\u0002\u0010>Jh\u0010C\u001a\u0002H7\"\u0004\b\u0001\u001072\u0006\u00108\u001a\u0002H72<\u00109\u001a8\u0012\u0013\u0012\u00110\u0007¢\u0006\f\b;\u0012\b\b<\u0012\u0004\b\b( \u0012\u0004\u0012\u00028\u0000\u0012\u0013\u0012\u0011H7¢\u0006\f\b;\u0012\b\b<\u0012\u0004\b\b(=\u0012\u0004\u0012\u0002H70@H\u0086\b\u00f8\u0001\u0000\u0082\u0002\b\n\u0006\b\u0001\u0012\u0002\u0010\u0002¢\u0006\u0002\u0010AJ+\u0010D\u001a\u00020\u001f2\u0012\u0010E\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00020\u001f0)H\u0086\b\u00f8\u0001\u0000\u0082\u0002\b\n\u0006\b\u0001\u0012\u0002\u0010\u0001J1\u0010F\u001a\u00020\u001f2\u0018\u0010E\u001a\u0014\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00020\u001f0:H\u0086\b\u00f8\u0001\u0000\u0082\u0002\b\n\u0006\b\u0001\u0012\u0002\u0010\u0001J+\u0010G\u001a\u00020\u001f2\u0012\u0010E\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00020\u001f0)H\u0086\b\u00f8\u0001\u0000\u0082\u0002\b\n\u0006\b\u0001\u0012\u0002\u0010\u0001J1\u0010H\u001a\u00020\u001f2\u0018\u0010E\u001a\u0014\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00020\u001f0:H\u0086\b\u00f8\u0001\u0000\u0082\u0002\b\n\u0006\b\u0001\u0012\u0002\u0010\u0001J\u0016\u0010I\u001a\u00028\u00002\u0006\u0010 \u001a\u00020\u0007H\u0086\n¢\u0006\u0002\u0010JJ\u0013\u0010K\u001a\u00020\u00072\u0006\u0010\u001d\u001a\u00028\u0000¢\u0006\u0002\u0010LJ+\u0010M\u001a\u00020\u00072\u0012\u0010(\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00020\u001c0)H\u0086\b\u00f8\u0001\u0000\u0082\u0002\b\n\u0006\b\u0001\u0012\u0002\u0010\u0001J+\u0010N\u001a\u00020\u00072\u0012\u0010(\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00020\u001c0)H\u0086\b\u00f8\u0001\u0000\u0082\u0002\b\n\u0006\b\u0001\u0012\u0002\u0010\u0001J\u0006\u0010O\u001a\u00020\u001cJ\u0006\u0010P\u001a\u00020\u001cJ\u000b\u0010Q\u001a\u00028\u0000¢\u0006\u0002\u00103J0\u0010Q\u001a\u00028\u00002\u0012\u0010(\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00020\u001c0)H\u0086\b\u00f8\u0001\u0000\u0082\u0002\b\n\u0006\b\u0001\u0012\u0002\u0010\u0001¢\u0006\u0002\u00104J\u0013\u0010R\u001a\u00020\u00072\u0006\u0010\u001d\u001a\u00028\u0000¢\u0006\u0002\u0010LJ\u0010\u0010S\u001a\u0004\u0018\u00018\u0000H\u0086\b¢\u0006\u0002\u00103J2\u0010S\u001a\u0004\u0018\u00018\u00002\u0012\u0010(\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00020\u001c0)H\u0086\b\u00f8\u0001\u0000\u0082\u0002\b\n\u0006\b\u0001\u0012\u0002\u0010\u0001¢\u0006\u0002\u00104J>\u0010T\u001a\b\u0012\u0004\u0012\u0002H70\u0005\"\u0006\b\u0001\u00107\u0018\u00012\u0012\u0010U\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u0002H70)H\u0086\b\u00f8\u0001\u0000\u0082\u0002\b\n\u0006\b\u0001\u0012\u0002\u0010\u0001¢\u0006\u0002\u0010VJS\u0010W\u001a\b\u0012\u0004\u0012\u0002H70\u0005\"\u0006\b\u0001\u00107\u0018\u00012'\u0010U\u001a#\u0012\u0013\u0012\u00110\u0007¢\u0006\f\b;\u0012\b\b<\u0012\u0004\b\b( \u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u0002H70:H\u0086\b\u00f8\u0001\u0000\u0082\u0002\b\n\u0006\b\u0001\u0012\u0002\u0010\u0001¢\u0006\u0002\u0010XJP\u0010Y\u001a\b\u0012\u0004\u0012\u0002H70\u0000\"\u0006\b\u0001\u00107\u0018\u00012)\u0010U\u001a%\u0012\u0013\u0012\u00110\u0007¢\u0006\f\b;\u0012\b\b<\u0012\u0004\b\b( \u0012\u0004\u0012\u00028\u0000\u0012\u0006\u0012\u0004\u0018\u0001H70:H\u0086\b\u00f8\u0001\u0000\u0082\u0002\b\n\u0006\b\u0001\u0012\u0002\u0010\u0001J;\u0010Z\u001a\b\u0012\u0004\u0012\u0002H70\u0000\"\u0006\b\u0001\u00107\u0018\u00012\u0014\u0010U\u001a\u0010\u0012\u0004\u0012\u00028\u0000\u0012\u0006\u0012\u0004\u0018\u0001H70)H\u0086\b\u00f8\u0001\u0000\u0082\u0002\b\n\u0006\b\u0001\u0012\u0002\u0010\u0001J\u0016\u0010[\u001a\u00020\u001f2\u0006\u0010\u001d\u001a\u00028\u0000H\u0086\n¢\u0006\u0002\u0010\\J\u0016\u0010]\u001a\u00020\u001f2\u0006\u0010\u001d\u001a\u00028\u0000H\u0086\n¢\u0006\u0002\u0010\\J\u0013\u0010^\u001a\u00020\u001c2\u0006\u0010\u001d\u001a\u00028\u0000¢\u0006\u0002\u0010\u001eJ\u0014\u0010_\u001a\u00020\u001c2\f\u0010#\u001a\b\u0012\u0004\u0012\u00028\u00000\u0000J\u0014\u0010_\u001a\u00020\u001c2\f\u0010#\u001a\b\u0012\u0004\u0012\u00028\u00000%J\u0014\u0010_\u001a\u00020\u001c2\f\u0010#\u001a\b\u0012\u0004\u0012\u00028\u00000&J\u0013\u0010`\u001a\u00028\u00002\u0006\u0010 \u001a\u00020\u0007¢\u0006\u0002\u0010JJ\u0016\u0010a\u001a\u00020\u001f2\u0006\u0010b\u001a\u00020\u00072\u0006\u0010c\u001a\u00020\u0007J\u0014\u0010d\u001a\u00020\u001c2\f\u0010#\u001a\b\u0012\u0004\u0012\u00028\u00000%J+\u0010e\u001a\u00020\u001c2\u0012\u0010(\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00020\u001c0)H\u0086\b\u00f8\u0001\u0000\u0082\u0002\b\n\u0006\b\u0001\u0012\u0002\u0010\u0001J\u001e\u0010f\u001a\u00028\u00002\u0006\u0010 \u001a\u00020\u00072\u0006\u0010\u001d\u001a\u00028\u0000H\u0086\u0002¢\u0006\u0002\u0010gJ\u001e\u0010h\u001a\u00020\u001f2\u0016\u0010i\u001a\u0012\u0012\u0004\u0012\u00028\u00000jj\b\u0012\u0004\u0012\u00028\u0000`kJ+\u0010l\u001a\u00020\u00072\u0012\u0010m\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00020\u00070)H\u0086\b\u00f8\u0001\u0000\u0082\u0002\b\n\u0006\b\u0001\u0012\u0002\u0010\u0001J\b\u0010n\u001a\u00020oH\u0001R.\u0010\u0004\u001a\n\u0012\u0006\u0012\u0004\u0018\u00018\u00000\u00058\u0000@\u0000X\u0081\u000e¢\u0006\u0016\n\u0002\u0010\u000f\u0012\u0004\b\t\u0010\n\u001a\u0004\b\u000b\u0010\f\"\u0004\b\r\u0010\u000eR\u0012\u0010\u0010\u001a\u00020\u00118\u00c6\u0002¢\u0006\u0006\u001a\u0004\b\u0012\u0010\u0013R\u0012\u0010\u0014\u001a\u00020\u00078\u00c6\u0002¢\u0006\u0006\u001a\u0004\b\u0015\u0010\u0016R\u0016\u0010\u0017\u001a\n\u0012\u0004\u0012\u00028\u0000\u0018\u00010\u0018X\u0082\u000e¢\u0006\u0002\n\u0000R\u001e\u0010\u0006\u001a\u00020\u00072\u0006\u0010\u0019\u001a\u00020\u0007@BX\u0086\u000e¢\u0006\b\n\u0000\u001a\u0004\b\u001a\u0010\u0016\u0082\u0002\u0007\n\u0005\b\u009920\u0001¨\u0006s" }, d2 = { "Landroidx/compose/runtime/collection/MutableVector;", "T", "Ljava/util/RandomAccess;", "Lkotlin/collections/RandomAccess;", "content", "", "size", "", "([Ljava/lang/Object;I)V", "getContent$annotations", "()V", "getContent", "()[Ljava/lang/Object;", "setContent", "([Ljava/lang/Object;)V", "[Ljava/lang/Object;", "indices", "Lkotlin/ranges/IntRange;", "getIndices", "()Lkotlin/ranges/IntRange;", "lastIndex", "getLastIndex", "()I", "list", "", "<set-?>", "getSize", "add", "", "element", "(Ljava/lang/Object;)Z", "", "index", "(ILjava/lang/Object;)V", "addAll", "elements", "([Ljava/lang/Object;)Z", "", "", "any", "predicate", "Lkotlin/Function1;", "asMutableList", "clear", "contains", "containsAll", "contentEquals", "other", "ensureCapacity", "capacity", "first", "()Ljava/lang/Object;", "(Lkotlin/jvm/functions/Function1;)Ljava/lang/Object;", "firstOrNull", "fold", "R", "initial", "operation", "Lkotlin/Function2;", "Lkotlin/ParameterName;", "name", "acc", "(Ljava/lang/Object;Lkotlin/jvm/functions/Function2;)Ljava/lang/Object;", "foldIndexed", "Lkotlin/Function3;", "(Ljava/lang/Object;Lkotlin/jvm/functions/Function3;)Ljava/lang/Object;", "foldRight", "foldRightIndexed", "forEach", "block", "forEachIndexed", "forEachReversed", "forEachReversedIndexed", "get", "(I)Ljava/lang/Object;", "indexOf", "(Ljava/lang/Object;)I", "indexOfFirst", "indexOfLast", "isEmpty", "isNotEmpty", "last", "lastIndexOf", "lastOrNull", "map", "transform", "(Lkotlin/jvm/functions/Function1;)[Ljava/lang/Object;", "mapIndexed", "(Lkotlin/jvm/functions/Function2;)[Ljava/lang/Object;", "mapIndexedNotNull", "mapNotNull", "minusAssign", "(Ljava/lang/Object;)V", "plusAssign", "remove", "removeAll", "removeAt", "removeRange", "start", "end", "retainAll", "reversedAny", "set", "(ILjava/lang/Object;)Ljava/lang/Object;", "sortWith", "comparator", "Ljava/util/Comparator;", "Lkotlin/Comparator;", "sumBy", "selector", "throwNoSuchElementException", "", "MutableVectorList", "SubList", "VectorListIterator", "runtime_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
public final class MutableVector<T> implements RandomAccess
{
    public static final int $stable = 8;
    private T[] content;
    private List<T> list;
    private int size;
    
    public MutableVector(final T[] content, final int size) {
        Intrinsics.checkNotNullParameter((Object)content, "content");
        this.content = content;
        this.size = size;
    }
    
    public final void add(final int n, final T t) {
        this.ensureCapacity(this.size + 1);
        final T[] content = this.content;
        final int size = this.size;
        if (n != size) {
            ArraysKt.copyInto((Object[])content, (Object[])content, n + 1, n, size);
        }
        content[n] = t;
        ++this.size;
    }
    
    public final boolean add(final T t) {
        this.ensureCapacity(this.size + 1);
        final T[] content = this.content;
        final int size = this.size;
        content[size] = t;
        this.size = size + 1;
        return true;
    }
    
    public final boolean addAll(final int n, final MutableVector<T> mutableVector) {
        Intrinsics.checkNotNullParameter((Object)mutableVector, "elements");
        if (mutableVector.isEmpty()) {
            return false;
        }
        this.ensureCapacity(this.size + mutableVector.size);
        final T[] content = this.content;
        final int size = this.size;
        if (n != size) {
            ArraysKt.copyInto((Object[])content, (Object[])content, mutableVector.size + n, n, size);
        }
        ArraysKt.copyInto((Object[])mutableVector.content, (Object[])content, n, 0, mutableVector.size);
        this.size += mutableVector.size;
        return true;
    }
    
    public final boolean addAll(final int n, final Collection<? extends T> collection) {
        Intrinsics.checkNotNullParameter((Object)collection, "elements");
        final boolean empty = collection.isEmpty();
        int n2 = 0;
        if (empty) {
            return false;
        }
        this.ensureCapacity(this.size + collection.size());
        final T[] content = this.content;
        if (n != this.size) {
            ArraysKt.copyInto((Object[])content, (Object[])content, collection.size() + n, n, this.size);
        }
        for (final Object next : collection) {
            if (n2 < 0) {
                CollectionsKt.throwIndexOverflow();
            }
            content[n2 + n] = (T)next;
            ++n2;
        }
        this.size += collection.size();
        return true;
    }
    
    public final boolean addAll(final int n, final List<? extends T> list) {
        Intrinsics.checkNotNullParameter((Object)list, "elements");
        final boolean empty = list.isEmpty();
        int i = 0;
        if (empty) {
            return false;
        }
        this.ensureCapacity(this.size + list.size());
        final T[] content = this.content;
        if (n != this.size) {
            ArraysKt.copyInto((Object[])content, (Object[])content, list.size() + n, n, this.size);
        }
        while (i < list.size()) {
            content[n + i] = list.get(i);
            ++i;
        }
        this.size += list.size();
        return true;
    }
    
    public final boolean addAll(final MutableVector<T> mutableVector) {
        Intrinsics.checkNotNullParameter((Object)mutableVector, "elements");
        return this.addAll(this.getSize(), mutableVector);
    }
    
    public final boolean addAll(final Collection<? extends T> collection) {
        Intrinsics.checkNotNullParameter((Object)collection, "elements");
        return this.addAll(this.size, collection);
    }
    
    public final boolean addAll(final List<? extends T> list) {
        Intrinsics.checkNotNullParameter((Object)list, "elements");
        return this.addAll(this.getSize(), list);
    }
    
    public final boolean addAll(final T[] array) {
        Intrinsics.checkNotNullParameter((Object)array, "elements");
        if (array.length == 0) {
            return false;
        }
        this.ensureCapacity(this.size + array.length);
        ArraysKt.copyInto$default((Object[])array, (Object[])this.content, this.size, 0, 0, 12, (Object)null);
        this.size += array.length;
        return true;
    }
    
    public final boolean any(final Function1<? super T, Boolean> function1) {
        Intrinsics.checkNotNullParameter((Object)function1, "predicate");
        final int size = this.getSize();
        if (size > 0) {
            final Object[] content = this.getContent();
            int n = 0;
            while (!(boolean)function1.invoke(content[n])) {
                if (++n >= size) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }
    
    public final List<T> asMutableList() {
        List<T> list;
        if ((list = this.list) == null) {
            list = new MutableVectorList<T>(this);
            this.list = list;
        }
        return list;
    }
    
    public final void clear() {
        final T[] content = this.content;
        for (int n = this.getSize() - 1; -1 < n; --n) {
            content[n] = null;
        }
        this.size = 0;
    }
    
    public final boolean contains(final T t) {
        final int n = this.getSize() - 1;
        if (n >= 0) {
            for (int n2 = 0; !Intrinsics.areEqual((Object)this.getContent()[n2], (Object)t); ++n2) {
                if (n2 == n) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }
    
    public final boolean containsAll(final MutableVector<T> mutableVector) {
        Intrinsics.checkNotNullParameter((Object)mutableVector, "elements");
        final IntRange intRange = new IntRange(0, mutableVector.getSize() - 1);
        int first = intRange.getFirst();
        final int last = intRange.getLast();
        if (first <= last) {
            while (this.contains(mutableVector.getContent()[first])) {
                if (first == last) {
                    return true;
                }
                ++first;
            }
            return false;
        }
        return true;
    }
    
    public final boolean containsAll(final Collection<? extends T> collection) {
        Intrinsics.checkNotNullParameter((Object)collection, "elements");
        final Iterator iterator = collection.iterator();
        while (iterator.hasNext()) {
            if (!this.contains(iterator.next())) {
                return false;
            }
        }
        return true;
    }
    
    public final boolean containsAll(final List<? extends T> list) {
        Intrinsics.checkNotNullParameter((Object)list, "elements");
        for (int size = list.size(), i = 0; i < size; ++i) {
            if (!this.contains((T)list.get(i))) {
                return false;
            }
        }
        return true;
    }
    
    public final boolean contentEquals(final MutableVector<T> mutableVector) {
        Intrinsics.checkNotNullParameter((Object)mutableVector, "other");
        if (mutableVector.size != this.size) {
            return false;
        }
        final int n = this.getSize() - 1;
        if (n >= 0) {
            for (int n2 = 0; Intrinsics.areEqual(mutableVector.getContent()[n2], (Object)this.getContent()[n2]); ++n2) {
                if (n2 == n) {
                    return true;
                }
            }
            return false;
        }
        return true;
    }
    
    public final void ensureCapacity(final int a) {
        final T[] content = this.content;
        if (content.length < a) {
            final T[] copy = Arrays.copyOf(content, Math.max(a, content.length * 2));
            Intrinsics.checkNotNullExpressionValue((Object)copy, "copyOf(this, newSize)");
            this.content = copy;
        }
    }
    
    public final T first() {
        if (!this.isEmpty()) {
            return this.getContent()[0];
        }
        throw new NoSuchElementException("MutableVector is empty.");
    }
    
    public final T first(final Function1<? super T, Boolean> function1) {
        Intrinsics.checkNotNullParameter((Object)function1, "predicate");
        final int size = this.getSize();
        if (size > 0) {
            int n = 0;
            final Object[] content = this.getContent();
            do {
                final Object o = content[n];
                if (function1.invoke(o)) {
                    return (T)o;
                }
            } while (++n < size);
        }
        this.throwNoSuchElementException();
        throw new KotlinNothingValueException();
    }
    
    public final T firstOrNull() {
        T t;
        if (this.isEmpty()) {
            t = null;
        }
        else {
            t = this.getContent()[0];
        }
        return t;
    }
    
    public final T firstOrNull(final Function1<? super T, Boolean> function1) {
        Intrinsics.checkNotNullParameter((Object)function1, "predicate");
        final int size = this.getSize();
        if (size > 0) {
            int n = 0;
            final Object[] content = this.getContent();
            do {
                final Object o = content[n];
                if (function1.invoke(o)) {
                    return (T)o;
                }
            } while (++n < size);
        }
        return null;
    }
    
    public final <R> R fold(R r, final Function2<? super R, ? super T, ? extends R> function2) {
        Intrinsics.checkNotNullParameter((Object)function2, "operation");
        final int size = this.getSize();
        Object invoke = r;
        if (size > 0) {
            int n = 0;
            final Object[] content = this.getContent();
            int i;
            do {
                invoke = function2.invoke((Object)r, content[n]);
                i = ++n;
                r = (R)invoke;
            } while (i < size);
        }
        return (R)invoke;
    }
    
    public final <R> R foldIndexed(final R r, final Function3<? super Integer, ? super R, ? super T, ? extends R> function3) {
        Intrinsics.checkNotNullParameter((Object)function3, "operation");
        final int size = this.getSize();
        Object o = r;
        if (size > 0) {
            int i = 0;
            final Object[] content = this.getContent();
            Object o2 = r;
            int j;
            Object invoke;
            do {
                invoke = function3.invoke((Object)i, o2, content[i]);
                j = ++i;
                o2 = invoke;
            } while (j < size);
            o = invoke;
        }
        return (R)o;
    }
    
    public final <R> R foldRight(final R r, final Function2<? super T, ? super R, ? extends R> function2) {
        Intrinsics.checkNotNullParameter((Object)function2, "operation");
        int size = this.getSize();
        Object o = r;
        if (size > 0) {
            --size;
            final Object[] content = this.getContent();
            Object o2 = r;
            int i;
            Object invoke;
            do {
                invoke = function2.invoke(content[size], o2);
                i = --size;
                o2 = invoke;
            } while (i >= 0);
            o = invoke;
        }
        return (R)o;
    }
    
    public final <R> R foldRightIndexed(R r, final Function3<? super Integer, ? super T, ? super R, ? extends R> function3) {
        Intrinsics.checkNotNullParameter((Object)function3, "operation");
        int size = this.getSize();
        Object invoke = r;
        if (size > 0) {
            --size;
            final Object[] content = this.getContent();
            int i;
            do {
                invoke = function3.invoke((Object)size, content[size], (Object)r);
                i = --size;
                r = (R)invoke;
            } while (i >= 0);
        }
        return (R)invoke;
    }
    
    public final void forEach(final Function1<? super T, Unit> function1) {
        Intrinsics.checkNotNullParameter((Object)function1, "block");
        final int size = this.getSize();
        if (size > 0) {
            int n = 0;
            final Object[] content = this.getContent();
            do {
                function1.invoke(content[n]);
            } while (++n < size);
        }
    }
    
    public final void forEachIndexed(final Function2<? super Integer, ? super T, Unit> function2) {
        Intrinsics.checkNotNullParameter((Object)function2, "block");
        final int size = this.getSize();
        if (size > 0) {
            int i = 0;
            final Object[] content = this.getContent();
            do {
                function2.invoke((Object)i, content[i]);
            } while (++i < size);
        }
    }
    
    public final void forEachReversed(final Function1<? super T, Unit> function1) {
        Intrinsics.checkNotNullParameter((Object)function1, "block");
        int size = this.getSize();
        if (size > 0) {
            --size;
            final Object[] content = this.getContent();
            do {
                function1.invoke(content[size]);
            } while (--size >= 0);
        }
    }
    
    public final void forEachReversedIndexed(final Function2<? super Integer, ? super T, Unit> function2) {
        Intrinsics.checkNotNullParameter((Object)function2, "block");
        if (this.getSize() > 0) {
            int i = this.getSize() - 1;
            final Object[] content = this.getContent();
            do {
                function2.invoke((Object)i, content[i]);
            } while (--i >= 0);
        }
    }
    
    public final T get(final int n) {
        return this.getContent()[n];
    }
    
    public final T[] getContent() {
        return this.content;
    }
    
    public final IntRange getIndices() {
        return new IntRange(0, this.getSize() - 1);
    }
    
    public final int getLastIndex() {
        return this.getSize() - 1;
    }
    
    public final int getSize() {
        return this.size;
    }
    
    public final int indexOf(final T t) {
        final int size = this.size;
        if (size > 0) {
            int n = 0;
            while (!Intrinsics.areEqual((Object)t, (Object)this.content[n])) {
                if (++n >= size) {
                    return -1;
                }
            }
            return n;
        }
        return -1;
    }
    
    public final int indexOfFirst(final Function1<? super T, Boolean> function1) {
        Intrinsics.checkNotNullParameter((Object)function1, "predicate");
        final int size = this.getSize();
        if (size > 0) {
            int n = 0;
            while (!(boolean)function1.invoke((Object)this.getContent()[n])) {
                if (++n >= size) {
                    return -1;
                }
            }
            return n;
        }
        return -1;
    }
    
    public final int indexOfLast(final Function1<? super T, Boolean> function1) {
        Intrinsics.checkNotNullParameter((Object)function1, "predicate");
        int size = this.getSize();
        if (size > 0) {
            --size;
            while (!(boolean)function1.invoke((Object)this.getContent()[size])) {
                if (--size < 0) {
                    return -1;
                }
            }
            return size;
        }
        return -1;
    }
    
    public final boolean isEmpty() {
        return this.size == 0;
    }
    
    public final boolean isNotEmpty() {
        return this.size != 0;
    }
    
    public final T last() {
        if (!this.isEmpty()) {
            return this.getContent()[this.getSize() - 1];
        }
        throw new NoSuchElementException("MutableVector is empty.");
    }
    
    public final T last(final Function1<? super T, Boolean> function1) {
        Intrinsics.checkNotNullParameter((Object)function1, "predicate");
        int size = this.getSize();
        if (size > 0) {
            --size;
            final Object[] content = this.getContent();
            do {
                final Object o = content[size];
                if (function1.invoke(o)) {
                    return (T)o;
                }
            } while (--size >= 0);
        }
        this.throwNoSuchElementException();
        throw new KotlinNothingValueException();
    }
    
    public final int lastIndexOf(final T t) {
        int size = this.size;
        if (size > 0) {
            --size;
            while (!Intrinsics.areEqual((Object)t, (Object)this.content[size])) {
                if (--size < 0) {
                    return -1;
                }
            }
            return size;
        }
        return -1;
    }
    
    public final T lastOrNull() {
        T t;
        if (this.isEmpty()) {
            t = null;
        }
        else {
            t = this.getContent()[this.getSize() - 1];
        }
        return t;
    }
    
    public final T lastOrNull(final Function1<? super T, Boolean> function1) {
        Intrinsics.checkNotNullParameter((Object)function1, "predicate");
        int size = this.getSize();
        if (size > 0) {
            --size;
            final Object[] content = this.getContent();
            do {
                final Object o = content[size];
                if (function1.invoke(o)) {
                    return (T)o;
                }
            } while (--size >= 0);
        }
        return null;
    }
    
    public final void minusAssign(final T t) {
        this.remove(t);
    }
    
    public final void plusAssign(final T t) {
        this.add(t);
    }
    
    public final boolean remove(final T t) {
        final int index = this.indexOf(t);
        if (index >= 0) {
            this.removeAt(index);
            return true;
        }
        return false;
    }
    
    public final boolean removeAll(final MutableVector<T> mutableVector) {
        Intrinsics.checkNotNullParameter((Object)mutableVector, "elements");
        final int size = this.size;
        final int size2 = mutableVector.getSize();
        boolean b = true;
        final int n = size2 - 1;
        if (n >= 0) {
            int n2 = 0;
            while (true) {
                this.remove(mutableVector.getContent()[n2]);
                if (n2 == n) {
                    break;
                }
                ++n2;
            }
        }
        if (size == this.size) {
            b = false;
        }
        return b;
    }
    
    public final boolean removeAll(final Collection<? extends T> collection) {
        Intrinsics.checkNotNullParameter((Object)collection, "elements");
        final boolean empty = collection.isEmpty();
        boolean b = false;
        if (empty) {
            return false;
        }
        final int size = this.size;
        final Iterator iterator = collection.iterator();
        while (iterator.hasNext()) {
            this.remove(iterator.next());
        }
        if (size != this.size) {
            b = true;
        }
        return b;
    }
    
    public final boolean removeAll(final List<? extends T> list) {
        Intrinsics.checkNotNullParameter((Object)list, "elements");
        final int size = this.size;
        final int size2 = list.size();
        boolean b = false;
        for (int i = 0; i < size2; ++i) {
            this.remove((T)list.get(i));
        }
        if (size != this.size) {
            b = true;
        }
        return b;
    }
    
    public final T removeAt(int size) {
        final T[] content = this.content;
        final T t = content[size];
        if (size != this.getSize() - 1) {
            ArraysKt.copyInto((Object[])content, (Object[])content, size, size + 1, this.size);
        }
        size = this.size - 1;
        content[this.size = size] = null;
        return t;
    }
    
    public final void removeRange(int n, int size) {
        if (size > n) {
            final int size2 = this.size;
            if (size < size2) {
                final T[] content = this.content;
                ArraysKt.copyInto((Object[])content, (Object[])content, n, size, size2);
            }
            size = this.size - (size - n);
            final int n2 = this.getSize() - 1;
            if (size <= n2) {
                n = size;
                while (true) {
                    this.content[n] = null;
                    if (n == n2) {
                        break;
                    }
                    ++n;
                }
            }
            this.size = size;
        }
    }
    
    public final boolean retainAll(final Collection<? extends T> collection) {
        Intrinsics.checkNotNullParameter((Object)collection, "elements");
        final int size = this.size;
        int size2 = this.getSize();
        boolean b = true;
        --size2;
        while (-1 < size2) {
            if (!collection.contains(this.getContent()[size2])) {
                this.removeAt(size2);
            }
            --size2;
        }
        if (size == this.size) {
            b = false;
        }
        return b;
    }
    
    public final boolean reversedAny(final Function1<? super T, Boolean> function1) {
        Intrinsics.checkNotNullParameter((Object)function1, "predicate");
        int size = this.getSize();
        if (size > 0) {
            --size;
            while (!(boolean)function1.invoke((Object)this.getContent()[size])) {
                if (--size < 0) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }
    
    public final T set(final int n, final T t) {
        final T[] content = this.content;
        final T t2 = content[n];
        content[n] = t;
        return t2;
    }
    
    public final void setContent(final T[] content) {
        Intrinsics.checkNotNullParameter((Object)content, "<set-?>");
        this.content = content;
    }
    
    public final void sortWith(final Comparator<T> comparator) {
        Intrinsics.checkNotNullParameter((Object)comparator, "comparator");
        ArraysKt.sortWith((Object[])this.content, (Comparator)comparator, 0, this.size);
    }
    
    public final int sumBy(final Function1<? super T, Integer> function1) {
        Intrinsics.checkNotNullParameter((Object)function1, "selector");
        final int size = this.getSize();
        int n = 0;
        int n2 = 0;
        if (size > 0) {
            final Object[] content = this.getContent();
            int n3 = 0;
            int n4;
            do {
                n = n2 + ((Number)function1.invoke(content[n3])).intValue();
                n4 = n3 + 1;
                n2 = n;
            } while ((n3 = n4) < size);
        }
        return n;
    }
    
    public final Void throwNoSuchElementException() {
        throw new NoSuchElementException("MutableVector contains no element matching the predicate.");
    }
    
    @Metadata(d1 = { "\u0000D\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010!\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0010\u001e\n\u0002\b\t\n\u0002\u0010)\n\u0002\b\u0002\n\u0002\u0010+\n\u0002\b\n\b\u0002\u0018\u0000*\u0004\b\u0001\u0010\u00012\b\u0012\u0004\u0012\u0002H\u00010\u0002B\u0013\u0012\f\u0010\u0003\u001a\b\u0012\u0004\u0012\u00028\u00010\u0004¢\u0006\u0002\u0010\u0005J\u0015\u0010\n\u001a\u00020\u000b2\u0006\u0010\f\u001a\u00028\u0001H\u0016¢\u0006\u0002\u0010\rJ\u001d\u0010\n\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u00072\u0006\u0010\f\u001a\u00028\u0001H\u0016¢\u0006\u0002\u0010\u0010J\u001e\u0010\u0011\u001a\u00020\u000b2\u0006\u0010\u000f\u001a\u00020\u00072\f\u0010\u0012\u001a\b\u0012\u0004\u0012\u00028\u00010\u0013H\u0016J\u0016\u0010\u0011\u001a\u00020\u000b2\f\u0010\u0012\u001a\b\u0012\u0004\u0012\u00028\u00010\u0013H\u0016J\b\u0010\u0014\u001a\u00020\u000eH\u0016J\u0016\u0010\u0015\u001a\u00020\u000b2\u0006\u0010\f\u001a\u00028\u0001H\u0096\u0002¢\u0006\u0002\u0010\rJ\u0016\u0010\u0016\u001a\u00020\u000b2\f\u0010\u0012\u001a\b\u0012\u0004\u0012\u00028\u00010\u0013H\u0016J\u0016\u0010\u0017\u001a\u00028\u00012\u0006\u0010\u000f\u001a\u00020\u0007H\u0096\u0002¢\u0006\u0002\u0010\u0018J\u0015\u0010\u0019\u001a\u00020\u00072\u0006\u0010\f\u001a\u00028\u0001H\u0016¢\u0006\u0002\u0010\u001aJ\b\u0010\u001b\u001a\u00020\u000bH\u0016J\u000f\u0010\u001c\u001a\b\u0012\u0004\u0012\u00028\u00010\u001dH\u0096\u0002J\u0015\u0010\u001e\u001a\u00020\u00072\u0006\u0010\f\u001a\u00028\u0001H\u0016¢\u0006\u0002\u0010\u001aJ\u000e\u0010\u001f\u001a\b\u0012\u0004\u0012\u00028\u00010 H\u0016J\u0016\u0010\u001f\u001a\b\u0012\u0004\u0012\u00028\u00010 2\u0006\u0010\u000f\u001a\u00020\u0007H\u0016J\u0015\u0010!\u001a\u00020\u000b2\u0006\u0010\f\u001a\u00028\u0001H\u0016¢\u0006\u0002\u0010\rJ\u0016\u0010\"\u001a\u00020\u000b2\f\u0010\u0012\u001a\b\u0012\u0004\u0012\u00028\u00010\u0013H\u0016J\u0015\u0010#\u001a\u00028\u00012\u0006\u0010\u000f\u001a\u00020\u0007H\u0016¢\u0006\u0002\u0010\u0018J\u0016\u0010$\u001a\u00020\u000b2\f\u0010\u0012\u001a\b\u0012\u0004\u0012\u00028\u00010\u0013H\u0016J\u001e\u0010%\u001a\u00028\u00012\u0006\u0010\u000f\u001a\u00020\u00072\u0006\u0010\f\u001a\u00028\u0001H\u0096\u0002¢\u0006\u0002\u0010&J\u001e\u0010'\u001a\b\u0012\u0004\u0012\u00028\u00010\u00022\u0006\u0010(\u001a\u00020\u00072\u0006\u0010)\u001a\u00020\u0007H\u0016R\u0014\u0010\u0006\u001a\u00020\u00078VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\b\u0010\tR\u0014\u0010\u0003\u001a\b\u0012\u0004\u0012\u00028\u00010\u0004X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006*" }, d2 = { "Landroidx/compose/runtime/collection/MutableVector$MutableVectorList;", "T", "", "vector", "Landroidx/compose/runtime/collection/MutableVector;", "(Landroidx/compose/runtime/collection/MutableVector;)V", "size", "", "getSize", "()I", "add", "", "element", "(Ljava/lang/Object;)Z", "", "index", "(ILjava/lang/Object;)V", "addAll", "elements", "", "clear", "contains", "containsAll", "get", "(I)Ljava/lang/Object;", "indexOf", "(Ljava/lang/Object;)I", "isEmpty", "iterator", "", "lastIndexOf", "listIterator", "", "remove", "removeAll", "removeAt", "retainAll", "set", "(ILjava/lang/Object;)Ljava/lang/Object;", "subList", "fromIndex", "toIndex", "runtime_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
    private static final class MutableVectorList<T> implements List<T>, KMutableList
    {
        private final MutableVector<T> vector;
        
        public MutableVectorList(final MutableVector<T> vector) {
            Intrinsics.checkNotNullParameter((Object)vector, "vector");
            this.vector = vector;
        }
        
        @Override
        public void add(final int n, final T t) {
            this.vector.add(n, t);
        }
        
        @Override
        public boolean add(final T t) {
            return this.vector.add(t);
        }
        
        @Override
        public boolean addAll(final int n, final Collection<? extends T> collection) {
            Intrinsics.checkNotNullParameter((Object)collection, "elements");
            return this.vector.addAll(n, collection);
        }
        
        @Override
        public boolean addAll(final Collection<? extends T> collection) {
            Intrinsics.checkNotNullParameter((Object)collection, "elements");
            return this.vector.addAll(collection);
        }
        
        @Override
        public void clear() {
            this.vector.clear();
        }
        
        @Override
        public boolean contains(final Object o) {
            return this.vector.contains((T)o);
        }
        
        @Override
        public boolean containsAll(final Collection<?> collection) {
            Intrinsics.checkNotNullParameter((Object)collection, "elements");
            return this.vector.containsAll((Collection<? extends T>)collection);
        }
        
        @Override
        public T get(final int n) {
            MutableVectorKt.access$checkIndex(this, n);
            return this.vector.getContent()[n];
        }
        
        public int getSize() {
            return this.vector.getSize();
        }
        
        @Override
        public int indexOf(final Object o) {
            return this.vector.indexOf((T)o);
        }
        
        @Override
        public boolean isEmpty() {
            return this.vector.isEmpty();
        }
        
        @Override
        public Iterator<T> iterator() {
            return new VectorListIterator<T>(this, 0);
        }
        
        @Override
        public int lastIndexOf(final Object o) {
            return this.vector.lastIndexOf((T)o);
        }
        
        @Override
        public ListIterator<T> listIterator() {
            return new VectorListIterator<T>(this, 0);
        }
        
        @Override
        public ListIterator<T> listIterator(final int n) {
            return new VectorListIterator<T>(this, n);
        }
        
        @Override
        public final /* bridge */ T remove(final int n) {
            return this.removeAt(n);
        }
        
        @Override
        public boolean remove(final Object o) {
            return this.vector.remove((T)o);
        }
        
        @Override
        public boolean removeAll(final Collection<?> collection) {
            Intrinsics.checkNotNullParameter((Object)collection, "elements");
            return this.vector.removeAll((Collection<? extends T>)collection);
        }
        
        public T removeAt(final int n) {
            MutableVectorKt.access$checkIndex(this, n);
            return this.vector.removeAt(n);
        }
        
        @Override
        public boolean retainAll(final Collection<?> collection) {
            Intrinsics.checkNotNullParameter((Object)collection, "elements");
            return this.vector.retainAll((Collection<? extends T>)collection);
        }
        
        @Override
        public T set(final int n, final T t) {
            MutableVectorKt.access$checkIndex(this, n);
            return this.vector.set(n, t);
        }
        
        @Override
        public final /* bridge */ int size() {
            return this.getSize();
        }
        
        @Override
        public List<T> subList(final int n, final int n2) {
            final List list = this;
            MutableVectorKt.access$checkSubIndex(list, n, n2);
            return new SubList<T>(list, n, n2);
        }
        
        @Override
        public Object[] toArray() {
            return CollectionToArray.toArray((Collection)this);
        }
        
        @Override
        public <T> T[] toArray(final T[] array) {
            Intrinsics.checkNotNullParameter((Object)array, "array");
            return (T[])CollectionToArray.toArray((Collection)this, (Object[])array);
        }
    }
    
    @Metadata(d1 = { "\u0000>\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010!\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0010\u001e\n\u0002\b\t\n\u0002\u0010)\n\u0002\b\u0002\n\u0002\u0010+\n\u0002\b\n\b\u0002\u0018\u0000*\u0004\b\u0001\u0010\u00012\b\u0012\u0004\u0012\u0002H\u00010\u0002B#\u0012\f\u0010\u0003\u001a\b\u0012\u0004\u0012\u00028\u00010\u0002\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0005¢\u0006\u0002\u0010\u0007J\u0015\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00028\u0001H\u0016¢\u0006\u0002\u0010\u000eJ\u001d\u0010\u000b\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u00052\u0006\u0010\r\u001a\u00028\u0001H\u0016¢\u0006\u0002\u0010\u0011J\u001e\u0010\u0012\u001a\u00020\f2\u0006\u0010\u0010\u001a\u00020\u00052\f\u0010\u0013\u001a\b\u0012\u0004\u0012\u00028\u00010\u0014H\u0016J\u0016\u0010\u0012\u001a\u00020\f2\f\u0010\u0013\u001a\b\u0012\u0004\u0012\u00028\u00010\u0014H\u0016J\b\u0010\u0015\u001a\u00020\u000fH\u0016J\u0016\u0010\u0016\u001a\u00020\f2\u0006\u0010\r\u001a\u00028\u0001H\u0096\u0002¢\u0006\u0002\u0010\u000eJ\u0016\u0010\u0017\u001a\u00020\f2\f\u0010\u0013\u001a\b\u0012\u0004\u0012\u00028\u00010\u0014H\u0016J\u0016\u0010\u0018\u001a\u00028\u00012\u0006\u0010\u0010\u001a\u00020\u0005H\u0096\u0002¢\u0006\u0002\u0010\u0019J\u0015\u0010\u001a\u001a\u00020\u00052\u0006\u0010\r\u001a\u00028\u0001H\u0016¢\u0006\u0002\u0010\u001bJ\b\u0010\u001c\u001a\u00020\fH\u0016J\u000f\u0010\u001d\u001a\b\u0012\u0004\u0012\u00028\u00010\u001eH\u0096\u0002J\u0015\u0010\u001f\u001a\u00020\u00052\u0006\u0010\r\u001a\u00028\u0001H\u0016¢\u0006\u0002\u0010\u001bJ\u000e\u0010 \u001a\b\u0012\u0004\u0012\u00028\u00010!H\u0016J\u0016\u0010 \u001a\b\u0012\u0004\u0012\u00028\u00010!2\u0006\u0010\u0010\u001a\u00020\u0005H\u0016J\u0015\u0010\"\u001a\u00020\f2\u0006\u0010\r\u001a\u00028\u0001H\u0016¢\u0006\u0002\u0010\u000eJ\u0016\u0010#\u001a\u00020\f2\f\u0010\u0013\u001a\b\u0012\u0004\u0012\u00028\u00010\u0014H\u0016J\u0015\u0010$\u001a\u00028\u00012\u0006\u0010\u0010\u001a\u00020\u0005H\u0016¢\u0006\u0002\u0010\u0019J\u0016\u0010%\u001a\u00020\f2\f\u0010\u0013\u001a\b\u0012\u0004\u0012\u00028\u00010\u0014H\u0016J\u001e\u0010&\u001a\u00028\u00012\u0006\u0010\u0010\u001a\u00020\u00052\u0006\u0010\r\u001a\u00028\u0001H\u0096\u0002¢\u0006\u0002\u0010'J\u001e\u0010(\u001a\b\u0012\u0004\u0012\u00028\u00010\u00022\u0006\u0010)\u001a\u00020\u00052\u0006\u0010*\u001a\u00020\u0005H\u0016R\u000e\u0010\u0006\u001a\u00020\u0005X\u0082\u000e¢\u0006\u0002\n\u0000R\u0014\u0010\u0003\u001a\b\u0012\u0004\u0012\u00028\u00010\u0002X\u0082\u0004¢\u0006\u0002\n\u0000R\u0014\u0010\b\u001a\u00020\u00058VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\t\u0010\nR\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006+" }, d2 = { "Landroidx/compose/runtime/collection/MutableVector$SubList;", "T", "", "list", "start", "", "end", "(Ljava/util/List;II)V", "size", "getSize", "()I", "add", "", "element", "(Ljava/lang/Object;)Z", "", "index", "(ILjava/lang/Object;)V", "addAll", "elements", "", "clear", "contains", "containsAll", "get", "(I)Ljava/lang/Object;", "indexOf", "(Ljava/lang/Object;)I", "isEmpty", "iterator", "", "lastIndexOf", "listIterator", "", "remove", "removeAll", "removeAt", "retainAll", "set", "(ILjava/lang/Object;)Ljava/lang/Object;", "subList", "fromIndex", "toIndex", "runtime_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
    private static final class SubList<T> implements List<T>, KMutableList
    {
        private int end;
        private final List<T> list;
        private final int start;
        
        public SubList(final List<T> list, final int start, final int end) {
            Intrinsics.checkNotNullParameter((Object)list, "list");
            this.list = list;
            this.start = start;
            this.end = end;
        }
        
        @Override
        public void add(final int n, final T t) {
            this.list.add(n + this.start, t);
            ++this.end;
        }
        
        @Override
        public boolean add(final T t) {
            this.list.add(this.end++, t);
            return true;
        }
        
        @Override
        public boolean addAll(final int n, final Collection<? extends T> collection) {
            Intrinsics.checkNotNullParameter((Object)collection, "elements");
            this.list.addAll(n + this.start, collection);
            this.end += collection.size();
            return collection.size() > 0;
        }
        
        @Override
        public boolean addAll(final Collection<? extends T> collection) {
            Intrinsics.checkNotNullParameter((Object)collection, "elements");
            this.list.addAll(this.end, collection);
            this.end += collection.size();
            return collection.size() > 0;
        }
        
        @Override
        public void clear() {
            int n = this.end - 1;
            final int start = this.start;
            if (start <= n) {
                while (true) {
                    this.list.remove(n);
                    if (n == start) {
                        break;
                    }
                    --n;
                }
            }
            this.end = this.start;
        }
        
        @Override
        public boolean contains(final Object o) {
            for (int i = this.start; i < this.end; ++i) {
                if (Intrinsics.areEqual((Object)this.list.get(i), o)) {
                    return true;
                }
            }
            return false;
        }
        
        @Override
        public boolean containsAll(final Collection<?> collection) {
            Intrinsics.checkNotNullParameter((Object)collection, "elements");
            final Iterator iterator = collection.iterator();
            while (iterator.hasNext()) {
                if (!this.contains(iterator.next())) {
                    return false;
                }
            }
            return true;
        }
        
        @Override
        public T get(final int n) {
            MutableVectorKt.access$checkIndex(this, n);
            return this.list.get(n + this.start);
        }
        
        public int getSize() {
            return this.end - this.start;
        }
        
        @Override
        public int indexOf(final Object o) {
            for (int i = this.start; i < this.end; ++i) {
                if (Intrinsics.areEqual((Object)this.list.get(i), o)) {
                    return i - this.start;
                }
            }
            return -1;
        }
        
        @Override
        public boolean isEmpty() {
            return this.end == this.start;
        }
        
        @Override
        public Iterator<T> iterator() {
            return new VectorListIterator<T>(this, 0);
        }
        
        @Override
        public int lastIndexOf(final Object o) {
            int n = this.end - 1;
            final int start = this.start;
            if (start <= n) {
                while (!Intrinsics.areEqual((Object)this.list.get(n), o)) {
                    if (n == start) {
                        return -1;
                    }
                    --n;
                }
                return n - this.start;
            }
            return -1;
        }
        
        @Override
        public ListIterator<T> listIterator() {
            return new VectorListIterator<T>(this, 0);
        }
        
        @Override
        public ListIterator<T> listIterator(final int n) {
            return new VectorListIterator<T>(this, n);
        }
        
        @Override
        public final /* bridge */ T remove(final int n) {
            return this.removeAt(n);
        }
        
        @Override
        public boolean remove(final Object o) {
            for (int i = this.start; i < this.end; ++i) {
                if (Intrinsics.areEqual((Object)this.list.get(i), o)) {
                    this.list.remove(i);
                    --this.end;
                    return true;
                }
            }
            return false;
        }
        
        @Override
        public boolean removeAll(final Collection<?> collection) {
            Intrinsics.checkNotNullParameter((Object)collection, "elements");
            final int end = this.end;
            final Iterator iterator = collection.iterator();
            while (iterator.hasNext()) {
                this.remove(iterator.next());
            }
            return end != this.end;
        }
        
        public T removeAt(final int n) {
            MutableVectorKt.access$checkIndex(this, n);
            final T remove = this.list.remove(n + this.start);
            --this.end;
            return remove;
        }
        
        @Override
        public boolean retainAll(final Collection<?> collection) {
            Intrinsics.checkNotNullParameter((Object)collection, "elements");
            final int end = this.end;
            int n = end - 1;
            final int start = this.start;
            if (start <= n) {
                while (true) {
                    if (!collection.contains(this.list.get(n))) {
                        this.list.remove(n);
                        --this.end;
                    }
                    if (n == start) {
                        break;
                    }
                    --n;
                }
            }
            return end != this.end;
        }
        
        @Override
        public T set(final int n, final T t) {
            MutableVectorKt.access$checkIndex(this, n);
            return this.list.set(n + this.start, t);
        }
        
        @Override
        public final /* bridge */ int size() {
            return this.getSize();
        }
        
        @Override
        public List<T> subList(final int n, final int n2) {
            final List list = this;
            MutableVectorKt.access$checkSubIndex(list, n, n2);
            return new SubList(list, n, n2);
        }
        
        @Override
        public Object[] toArray() {
            return CollectionToArray.toArray((Collection)this);
        }
        
        @Override
        public <T> T[] toArray(final T[] array) {
            Intrinsics.checkNotNullParameter((Object)array, "array");
            return (T[])CollectionToArray.toArray((Collection)this, (Object[])array);
        }
    }
    
    @Metadata(d1 = { "\u0000*\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010+\n\u0000\n\u0002\u0010!\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\t\b\u0002\u0018\u0000*\u0004\b\u0001\u0010\u00012\b\u0012\u0004\u0012\u0002H\u00010\u0002B\u001b\u0012\f\u0010\u0003\u001a\b\u0012\u0004\u0012\u00028\u00010\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006¢\u0006\u0002\u0010\u0007J\u0015\u0010\b\u001a\u00020\t2\u0006\u0010\n\u001a\u00028\u0001H\u0016¢\u0006\u0002\u0010\u000bJ\t\u0010\f\u001a\u00020\rH\u0096\u0002J\b\u0010\u000e\u001a\u00020\rH\u0016J\u000e\u0010\u000f\u001a\u00028\u0001H\u0096\u0002¢\u0006\u0002\u0010\u0010J\b\u0010\u0011\u001a\u00020\u0006H\u0016J\r\u0010\u0012\u001a\u00028\u0001H\u0016¢\u0006\u0002\u0010\u0010J\b\u0010\u0013\u001a\u00020\u0006H\u0016J\b\u0010\u0014\u001a\u00020\tH\u0016J\u0015\u0010\u0015\u001a\u00020\t2\u0006\u0010\n\u001a\u00028\u0001H\u0016¢\u0006\u0002\u0010\u000bR\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u000e¢\u0006\u0002\n\u0000R\u0014\u0010\u0003\u001a\b\u0012\u0004\u0012\u00028\u00010\u0004X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u0016" }, d2 = { "Landroidx/compose/runtime/collection/MutableVector$VectorListIterator;", "T", "", "list", "", "index", "", "(Ljava/util/List;I)V", "add", "", "element", "(Ljava/lang/Object;)V", "hasNext", "", "hasPrevious", "next", "()Ljava/lang/Object;", "nextIndex", "previous", "previousIndex", "remove", "set", "runtime_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
    private static final class VectorListIterator<T> implements ListIterator<T>, KMutableListIterator
    {
        private int index;
        private final List<T> list;
        
        public VectorListIterator(final List<T> list, final int index) {
            Intrinsics.checkNotNullParameter((Object)list, "list");
            this.list = list;
            this.index = index;
        }
        
        @Override
        public void add(final T t) {
            this.list.add(this.index, t);
            ++this.index;
        }
        
        @Override
        public boolean hasNext() {
            return this.index < this.list.size();
        }
        
        @Override
        public boolean hasPrevious() {
            return this.index > 0;
        }
        
        @Override
        public T next() {
            return this.list.get(this.index++);
        }
        
        @Override
        public int nextIndex() {
            return this.index;
        }
        
        @Override
        public T previous() {
            final int index = this.index - 1;
            this.index = index;
            return this.list.get(index);
        }
        
        @Override
        public int previousIndex() {
            return this.index - 1;
        }
        
        @Override
        public void remove() {
            final int index = this.index - 1;
            this.index = index;
            this.list.remove(index);
        }
        
        @Override
        public void set(final T t) {
            this.list.set(this.index, t);
        }
    }
}

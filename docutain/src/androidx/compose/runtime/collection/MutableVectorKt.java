// 
// Decompiled by Procyon v0.6.0
// 

package androidx.compose.runtime.collection;

import java.util.List;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000.\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0011\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\u0010 \n\u0002\b\u0005\u001a!\u0010\u0000\u001a\b\u0012\u0004\u0012\u0002H\u00020\u0001\"\u0006\b\u0000\u0010\u0002\u0018\u00012\b\b\u0002\u0010\u0003\u001a\u00020\u0004H\u0086\b\u001aC\u0010\u0000\u001a\b\u0012\u0004\u0012\u0002H\u00020\u0001\"\u0006\b\u0000\u0010\u0002\u0018\u00012\u0006\u0010\u0005\u001a\u00020\u00042\u0014\b\b\u0010\u0006\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u0002H\u00020\u0007H\u0086\b\u00f8\u0001\u0000\u0082\u0002\b\n\u0006\b\u0001\u0012\u0002\u0010\u0002\u001a\u0017\u0010\b\u001a\b\u0012\u0004\u0012\u0002H\u00020\u0001\"\u0006\b\u0000\u0010\u0002\u0018\u0001H\u0086\b\u001a0\u0010\b\u001a\b\u0012\u0004\u0012\u0002H\u00020\u0001\"\u0006\b\u0000\u0010\u0002\u0018\u00012\u0012\u0010\t\u001a\n\u0012\u0006\b\u0001\u0012\u0002H\u00020\n\"\u0002H\u0002H\u0086\b¢\u0006\u0002\u0010\u000b\u001a\u0018\u0010\f\u001a\u00020\r*\u0006\u0012\u0002\b\u00030\u000e2\u0006\u0010\u000f\u001a\u00020\u0004H\u0002\u001a \u0010\u0010\u001a\u00020\r*\u0006\u0012\u0002\b\u00030\u000e2\u0006\u0010\u0011\u001a\u00020\u00042\u0006\u0010\u0012\u001a\u00020\u0004H\u0002\u0082\u0002\u0007\n\u0005\b\u009920\u0001¨\u0006\u0013" }, d2 = { "MutableVector", "Landroidx/compose/runtime/collection/MutableVector;", "T", "capacity", "", "size", "init", "Lkotlin/Function1;", "mutableVectorOf", "elements", "", "([Ljava/lang/Object;)Landroidx/compose/runtime/collection/MutableVector;", "checkIndex", "", "", "index", "checkSubIndex", "fromIndex", "toIndex", "runtime_release" }, k = 2, mv = { 1, 8, 0 }, xi = 48)
public final class MutableVectorKt
{
    private static final void checkIndex(final List<?> list, final int i) {
        final int size = list.size();
        if (i >= 0 && i < size) {
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Index ");
        sb.append(i);
        sb.append(" is out of bounds. The list has ");
        sb.append(size);
        sb.append(" elements.");
        throw new IndexOutOfBoundsException(sb.toString());
    }
    
    private static final void checkSubIndex(final List<?> list, final int n, final int n2) {
        final int size = list.size();
        if (n > n2) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Indices are out of order. fromIndex (");
            sb.append(n);
            sb.append(") is greater than toIndex (");
            sb.append(n2);
            sb.append(").");
            throw new IllegalArgumentException(sb.toString());
        }
        if (n < 0) {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("fromIndex (");
            sb2.append(n);
            sb2.append(") is less than 0.");
            throw new IndexOutOfBoundsException(sb2.toString());
        }
        if (n2 <= size) {
            return;
        }
        final StringBuilder sb3 = new StringBuilder();
        sb3.append("toIndex (");
        sb3.append(n2);
        sb3.append(") is more than than the list size (");
        sb3.append(size);
        sb3.append(')');
        throw new IndexOutOfBoundsException(sb3.toString());
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.compose.runtime.collection;

import kotlin.Unit;
import kotlin.jvm.functions.Function2;
import kotlin.collections.ArraysKt;
import kotlin.jvm.internal.Intrinsics;
import androidx.compose.runtime.ActualJvm_jvmKt;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000:\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0011\n\u0002\b\u0007\n\u0002\u0010\b\n\u0002\b\u0006\n\u0002\u0010\u0015\n\u0002\b\t\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0002\b\u0005\b\u0000\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\u0016\u0010\u0019\u001a\u00020\f2\u0006\u0010\u001a\u001a\u00020\u00012\u0006\u0010\u001b\u001a\u00020\fJ&\u0010\u001c\u001a\u00020\u001d2\u0018\u0010\u001e\u001a\u0014\u0012\u0004\u0012\u00020\u0001\u0012\u0004\u0012\u00020\f\u0012\u0004\u0012\u00020\u001d0\u001fH\u0086\b\u00f8\u0001\u0000J\u0012\u0010 \u001a\u00020\f2\b\u0010\u001a\u001a\u0004\u0018\u00010\u0001H\u0002J\"\u0010!\u001a\u00020\f2\u0006\u0010\"\u001a\u00020\f2\b\u0010\u001b\u001a\u0004\u0018\u00010\u00012\u0006\u0010#\u001a\u00020\fH\u0002J&\u0010$\u001a\u00020%2\u0018\u0010&\u001a\u0014\u0012\u0004\u0012\u00020\u0001\u0012\u0004\u0012\u00020\f\u0012\u0004\u0012\u00020%0\u001fH\u0086\b\u00f8\u0001\u0000J\u0011\u0010'\u001a\u00020\f2\u0006\u0010\u001a\u001a\u00020\u0001H\u0086\u0002J\u000e\u0010(\u001a\u00020\u001d2\u0006\u0010\u001a\u001a\u00020\u0001J&\u0010)\u001a\u00020%2\u0018\u0010\u001e\u001a\u0014\u0012\u0004\u0012\u00020\u0001\u0012\u0004\u0012\u00020\f\u0012\u0004\u0012\u00020\u001d0\u001fH\u0086\b\u00f8\u0001\u0000R.\u0010\u0003\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00010\u00048\u0000@\u0000X\u0081\u000e¢\u0006\u0016\n\u0002\u0010\n\u0012\u0004\b\u0005\u0010\u0002\u001a\u0004\b\u0006\u0010\u0007\"\u0004\b\b\u0010\tR$\u0010\u000b\u001a\u00020\f8\u0000@\u0000X\u0081\u000e¢\u0006\u0014\n\u0000\u0012\u0004\b\r\u0010\u0002\u001a\u0004\b\u000e\u0010\u000f\"\u0004\b\u0010\u0010\u0011R$\u0010\u0012\u001a\u00020\u00138\u0000@\u0000X\u0081\u000e¢\u0006\u0014\n\u0000\u0012\u0004\b\u0014\u0010\u0002\u001a\u0004\b\u0015\u0010\u0016\"\u0004\b\u0017\u0010\u0018\u0082\u0002\u0007\n\u0005\b\u009920\u0001¨\u0006*" }, d2 = { "Landroidx/compose/runtime/collection/IdentityArrayIntMap;", "", "()V", "keys", "", "getKeys$annotations", "getKeys", "()[Ljava/lang/Object;", "setKeys", "([Ljava/lang/Object;)V", "[Ljava/lang/Object;", "size", "", "getSize$annotations", "getSize", "()I", "setSize", "(I)V", "values", "", "getValues$annotations", "getValues", "()[I", "setValues", "([I)V", "add", "key", "value", "any", "", "predicate", "Lkotlin/Function2;", "find", "findExactIndex", "midIndex", "valueHash", "forEach", "", "block", "get", "remove", "removeValueIf", "runtime_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
public final class IdentityArrayIntMap
{
    private Object[] keys;
    private int size;
    private int[] values;
    
    public IdentityArrayIntMap() {
        this.keys = new Object[4];
        this.values = new int[4];
    }
    
    private final int find(final Object o) {
        int n = this.size - 1;
        final int identityHashCode = ActualJvm_jvmKt.identityHashCode(o);
        int i = 0;
        while (i <= n) {
            final int n2 = i + n >>> 1;
            final Object o2 = this.keys[n2];
            final int identityHashCode2 = ActualJvm_jvmKt.identityHashCode(o2);
            if (identityHashCode2 < identityHashCode) {
                i = n2 + 1;
            }
            else if (identityHashCode2 > identityHashCode) {
                n = n2 - 1;
            }
            else {
                if (o2 == o) {
                    return n2;
                }
                return this.findExactIndex(n2, o, identityHashCode);
            }
        }
        return -(i + 1);
    }
    
    private final int findExactIndex(int i, final Object o, final int n) {
        for (int n2 = i - 1; -1 < n2; --n2) {
            final Object o2 = this.keys[n2];
            if (o2 == o) {
                return n2;
            }
            if (ActualJvm_jvmKt.identityHashCode(o2) != n) {
                break;
            }
        }
        ++i;
        while (i < this.size) {
            final Object o3 = this.keys[i];
            if (o3 == o) {
                return i;
            }
            if (ActualJvm_jvmKt.identityHashCode(o3) != n) {
                return -(i + 1);
            }
            ++i;
        }
        i = this.size;
        return -(i + 1);
    }
    
    public final int add(final Object o, final int n) {
        Intrinsics.checkNotNullParameter(o, "key");
        int n2;
        if (this.size > 0) {
            final int find = this.find(o);
            if ((n2 = find) >= 0) {
                final int[] values = this.values;
                final int n3 = values[find];
                values[find] = n;
                return n3;
            }
        }
        else {
            n2 = -1;
        }
        final int n4 = -(n2 + 1);
        final int size = this.size;
        final Object[] keys = this.keys;
        if (size == keys.length) {
            final Object[] keys2 = new Object[keys.length * 2];
            final int[] values2 = new int[keys.length * 2];
            final int n5 = n4 + 1;
            ArraysKt.copyInto(keys, keys2, n5, n4, size);
            ArraysKt.copyInto(this.values, values2, n5, n4, this.size);
            ArraysKt.copyInto$default(this.keys, keys2, 0, 0, n4, 6, (Object)null);
            ArraysKt.copyInto$default(this.values, values2, 0, 0, n4, 6, (Object)null);
            this.keys = keys2;
            this.values = values2;
        }
        else {
            final int n6 = n4 + 1;
            ArraysKt.copyInto(keys, keys, n6, n4, size);
            final int[] values3 = this.values;
            ArraysKt.copyInto(values3, values3, n6, n4, this.size);
        }
        this.keys[n4] = o;
        this.values[n4] = n;
        ++this.size;
        return -1;
    }
    
    public final boolean any(final Function2<Object, ? super Integer, Boolean> function2) {
        Intrinsics.checkNotNullParameter((Object)function2, "predicate");
        for (int size = this.getSize(), i = 0; i < size; ++i) {
            final Object o = this.getKeys()[i];
            Intrinsics.checkNotNull(o, "null cannot be cast to non-null type kotlin.Any");
            if (function2.invoke(o, (Object)this.getValues()[i])) {
                return true;
            }
        }
        return false;
    }
    
    public final void forEach(final Function2<Object, ? super Integer, Unit> function2) {
        Intrinsics.checkNotNullParameter((Object)function2, "block");
        for (int size = this.getSize(), i = 0; i < size; ++i) {
            final Object o = this.getKeys()[i];
            Intrinsics.checkNotNull(o, "null cannot be cast to non-null type kotlin.Any");
            function2.invoke(o, (Object)this.getValues()[i]);
        }
    }
    
    public final int get(final Object o) {
        Intrinsics.checkNotNullParameter(o, "key");
        final int find = this.find(o);
        if (find >= 0) {
            return this.values[find];
        }
        throw new IllegalStateException("Key not found".toString());
    }
    
    public final Object[] getKeys() {
        return this.keys;
    }
    
    public final int getSize() {
        return this.size;
    }
    
    public final int[] getValues() {
        return this.values;
    }
    
    public final boolean remove(final Object o) {
        Intrinsics.checkNotNullParameter(o, "key");
        final int find = this.find(o);
        if (find >= 0) {
            final int size = this.size;
            if (find < size - 1) {
                final Object[] keys = this.keys;
                final int n = find + 1;
                ArraysKt.copyInto(keys, keys, find, n, size);
                final int[] values = this.values;
                ArraysKt.copyInto(values, values, find, n, this.size);
            }
            final int size2 = this.size - 1;
            this.size = size2;
            this.keys[size2] = null;
            return true;
        }
        return false;
    }
    
    public final void removeValueIf(final Function2<Object, ? super Integer, Boolean> function2) {
        Intrinsics.checkNotNullParameter((Object)function2, "predicate");
        final int size = this.getSize();
        int i = 0;
        int size2 = 0;
        while (i < size) {
            final Object o = this.getKeys()[i];
            Intrinsics.checkNotNull(o, "null cannot be cast to non-null type kotlin.Any");
            final int j = this.getValues()[i];
            int n = size2;
            if (!(boolean)function2.invoke(o, (Object)j)) {
                if (size2 != i) {
                    this.getKeys()[size2] = o;
                    this.getValues()[size2] = j;
                }
                n = size2 + 1;
            }
            ++i;
            size2 = n;
        }
        for (int size3 = this.getSize(), k = size2; k < size3; ++k) {
            this.getKeys()[k] = null;
        }
        this.setSize(size2);
    }
    
    public final void setKeys(final Object[] keys) {
        Intrinsics.checkNotNullParameter((Object)keys, "<set-?>");
        this.keys = keys;
    }
    
    public final void setSize(final int size) {
        this.size = size;
    }
    
    public final void setValues(final int[] values) {
        Intrinsics.checkNotNullParameter((Object)values, "<set-?>");
        this.values = values;
    }
}

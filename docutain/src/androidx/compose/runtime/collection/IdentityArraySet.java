// 
// Decompiled by Procyon v0.6.0
// 

package androidx.compose.runtime.collection;

import kotlin.jvm.internal.CollectionToArray;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import java.util.Iterator;
import java.util.Collection;
import kotlin.collections.ArraysKt;
import kotlin.jvm.internal.Intrinsics;
import androidx.compose.runtime.ActualJvm_jvmKt;
import kotlin.Metadata;
import kotlin.jvm.internal.markers.KMappedMarker;
import java.util.Set;

@Metadata(d1 = { "\u0000J\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0010\"\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0010\u0011\n\u0002\b\u0007\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0002\b\u0006\n\u0002\u0010\u001e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010(\n\u0002\b\u0004\b\u0000\u0018\u0000*\b\b\u0000\u0010\u0001*\u00020\u00022\b\u0012\u0004\u0012\u0002H\u00010\u0003B\u0005¢\u0006\u0002\u0010\u0004J\u0013\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00028\u0000¢\u0006\u0002\u0010\u0016J\u0010\u0010\u0017\u001a\u00020\u00182\u0006\u0010\u0019\u001a\u00020\u0006H\u0002J\u0006\u0010\u001a\u001a\u00020\u0018J\u0016\u0010\u001b\u001a\u00020\u00142\u0006\u0010\u001c\u001a\u00028\u0000H\u0096\u0002¢\u0006\u0002\u0010\u0016J\u0016\u0010\u001d\u001a\u00020\u00142\f\u0010\u001e\u001a\b\u0012\u0004\u0012\u00028\u00000\u001fH\u0016J+\u0010 \u001a\u00020\u00182\u0012\u0010!\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00020\u00180\"H\u0086\b\u00f8\u0001\u0000\u0082\u0002\b\n\u0006\b\u0001\u0012\u0002\u0010\u0001J\u0012\u0010#\u001a\u00020\u00062\b\u0010\u0015\u001a\u0004\u0018\u00010\u0002H\u0002J\"\u0010$\u001a\u00020\u00062\u0006\u0010%\u001a\u00020\u00062\b\u0010\u0015\u001a\u0004\u0018\u00010\u00022\u0006\u0010&\u001a\u00020\u0006H\u0002J\u0016\u0010'\u001a\u00028\u00002\u0006\u0010\u0019\u001a\u00020\u0006H\u0086\u0002¢\u0006\u0002\u0010(J\b\u0010)\u001a\u00020\u0014H\u0016J\u0006\u0010*\u001a\u00020\u0014J\u000f\u0010+\u001a\b\u0012\u0004\u0012\u00028\u00000,H\u0096\u0002J\u0013\u0010-\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00028\u0000¢\u0006\u0002\u0010\u0016J \u0010.\u001a\u00020\u00182\u0012\u0010/\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00020\u00140\"H\u0086\b\u00f8\u0001\u0000R\u001a\u0010\u0005\u001a\u00020\u0006X\u0096\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0007\u0010\b\"\u0004\b\t\u0010\nR.\u0010\u000b\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00020\f8\u0000@\u0000X\u0081\u000e¢\u0006\u0016\n\u0002\u0010\u0012\u0012\u0004\b\r\u0010\u0004\u001a\u0004\b\u000e\u0010\u000f\"\u0004\b\u0010\u0010\u0011\u0082\u0002\u0007\n\u0005\b\u009920\u0001¨\u00060" }, d2 = { "Landroidx/compose/runtime/collection/IdentityArraySet;", "T", "", "", "()V", "size", "", "getSize", "()I", "setSize", "(I)V", "values", "", "getValues$annotations", "getValues", "()[Ljava/lang/Object;", "setValues", "([Ljava/lang/Object;)V", "[Ljava/lang/Object;", "add", "", "value", "(Ljava/lang/Object;)Z", "checkIndexBounds", "", "index", "clear", "contains", "element", "containsAll", "elements", "", "fastForEach", "block", "Lkotlin/Function1;", "find", "findExactIndex", "midIndex", "valueHash", "get", "(I)Ljava/lang/Object;", "isEmpty", "isNotEmpty", "iterator", "", "remove", "removeValueIf", "predicate", "runtime_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
public final class IdentityArraySet<T> implements Set<T>, KMappedMarker
{
    private int size;
    private Object[] values;
    
    public IdentityArraySet() {
        this.values = new Object[16];
    }
    
    private final void checkIndexBounds(final int i) {
        int n = 0;
        if (i >= 0) {
            n = n;
            if (i < this.size()) {
                n = 1;
            }
        }
        if (n != 0) {
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Index ");
        sb.append(i);
        sb.append(", size ");
        sb.append(this.size());
        throw new IndexOutOfBoundsException(sb.toString());
    }
    
    private final int find(final Object o) {
        int n = this.size() - 1;
        final int identityHashCode = ActualJvm_jvmKt.identityHashCode(o);
        int i = 0;
        while (i <= n) {
            final int n2 = i + n >>> 1;
            final T value = this.get(n2);
            final int identityHashCode2 = ActualJvm_jvmKt.identityHashCode(value);
            if (identityHashCode2 < identityHashCode) {
                i = n2 + 1;
            }
            else if (identityHashCode2 > identityHashCode) {
                n = n2 - 1;
            }
            else {
                if (value == o) {
                    return n2;
                }
                return this.findExactIndex(n2, o, identityHashCode);
            }
        }
        return -(i + 1);
    }
    
    private final int findExactIndex(int i, final Object o, final int n) {
        for (int n2 = i - 1; -1 < n2; --n2) {
            final Object o2 = this.values[n2];
            if (o2 == o) {
                return n2;
            }
            if (ActualJvm_jvmKt.identityHashCode(o2) != n) {
                break;
            }
        }
        ++i;
        while (i < this.size()) {
            final Object o3 = this.values[i];
            if (o3 == o) {
                return i;
            }
            if (ActualJvm_jvmKt.identityHashCode(o3) != n) {
                return -(i + 1);
            }
            ++i;
        }
        i = this.size();
        return -(i + 1);
    }
    
    @Override
    public final boolean add(final T t) {
        Intrinsics.checkNotNullParameter((Object)t, "value");
        int find;
        if (this.size() > 0) {
            if ((find = this.find(t)) >= 0) {
                return false;
            }
        }
        else {
            find = -1;
        }
        final int n = -(find + 1);
        final int size = this.size();
        final Object[] values = this.values;
        if (size == values.length) {
            final Object[] values2 = new Object[values.length * 2];
            ArraysKt.copyInto(values, values2, n + 1, n, this.size());
            ArraysKt.copyInto$default(this.values, values2, 0, 0, n, 6, (Object)null);
            this.values = values2;
        }
        else {
            ArraysKt.copyInto(values, values, n + 1, n, this.size());
        }
        this.values[n] = t;
        this.setSize(this.size() + 1);
        return true;
    }
    
    @Override
    public boolean addAll(final Collection<? extends T> collection) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }
    
    @Override
    public final void clear() {
        ArraysKt.fill$default(this.values, (Object)null, 0, 0, 6, (Object)null);
        this.setSize(0);
    }
    
    @Override
    public boolean contains(final Object o) {
        boolean b = false;
        if (o == null) {
            return false;
        }
        if (this.find(o) >= 0) {
            b = true;
        }
        return b;
    }
    
    @Override
    public boolean containsAll(final Collection<?> collection) {
        Intrinsics.checkNotNullParameter((Object)collection, "elements");
        final Iterable iterable = collection;
        final boolean empty = ((Collection)iterable).isEmpty();
        final boolean b = true;
        boolean b2;
        if (empty) {
            b2 = b;
        }
        else {
            final Iterator iterator = iterable.iterator();
            do {
                b2 = b;
                if (iterator.hasNext()) {
                    continue;
                }
                return b2;
            } while (this.contains(iterator.next()));
            b2 = false;
        }
        return b2;
    }
    
    public final void fastForEach(final Function1<? super T, Unit> function1) {
        Intrinsics.checkNotNullParameter((Object)function1, "block");
        for (int size = this.size(), i = 0; i < size; ++i) {
            function1.invoke((Object)this.get(i));
        }
    }
    
    public final T get(final int n) {
        this.checkIndexBounds(n);
        final Object o = this.values[n];
        Intrinsics.checkNotNull(o, "null cannot be cast to non-null type T of androidx.compose.runtime.collection.IdentityArraySet");
        return (T)o;
    }
    
    public int getSize() {
        return this.size;
    }
    
    public final Object[] getValues() {
        return this.values;
    }
    
    @Override
    public boolean isEmpty() {
        return this.size() == 0;
    }
    
    public final boolean isNotEmpty() {
        return this.size() > 0;
    }
    
    @Override
    public Iterator<T> iterator() {
        return (Iterator<T>)new IdentityArraySet$iterator.IdentityArraySet$iterator$1(this);
    }
    
    @Override
    public final boolean remove(final T t) {
        if (t == null) {
            return false;
        }
        final int find = this.find(t);
        if (find >= 0) {
            if (find < this.size() - 1) {
                final Object[] values = this.values;
                ArraysKt.copyInto(values, values, find, find + 1, this.size());
            }
            this.setSize(this.size() - 1);
            this.values[this.size()] = null;
            return true;
        }
        return false;
    }
    
    @Override
    public boolean removeAll(final Collection<?> collection) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }
    
    public final void removeValueIf(final Function1<? super T, Boolean> function1) {
        Intrinsics.checkNotNullParameter((Object)function1, "predicate");
        final int size = this.size();
        int i = 0;
        int size2 = 0;
        while (i < size) {
            final Object o = this.getValues()[i];
            Intrinsics.checkNotNull(o, "null cannot be cast to non-null type T of androidx.compose.runtime.collection.IdentityArraySet");
            int n = size2;
            if (!(boolean)function1.invoke(o)) {
                if (size2 != i) {
                    this.getValues()[size2] = o;
                }
                n = size2 + 1;
            }
            ++i;
            size2 = n;
        }
        for (int size3 = this.size(), j = size2; j < size3; ++j) {
            this.getValues()[j] = null;
        }
        this.setSize(size2);
    }
    
    @Override
    public boolean retainAll(final Collection<?> collection) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }
    
    public void setSize(final int size) {
        this.size = size;
    }
    
    public final void setValues(final Object[] values) {
        Intrinsics.checkNotNullParameter((Object)values, "<set-?>");
        this.values = values;
    }
    
    @Override
    public final /* bridge */ int size() {
        return this.getSize();
    }
    
    @Override
    public Object[] toArray() {
        return CollectionToArray.toArray((Collection)this);
    }
    
    @Override
    public <T> T[] toArray(final T[] array) {
        Intrinsics.checkNotNullParameter((Object)array, "array");
        return (T[])CollectionToArray.toArray((Collection)this, (Object[])array);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.compose.runtime.collection;

import kotlin.jvm.functions.Function1;
import kotlin.Unit;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.internal.Intrinsics;
import kotlin.collections.ArraysKt;
import androidx.compose.runtime.ActualJvm_jvmKt;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000@\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0011\n\u0002\b\r\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\b\u0003\b\u0000\u0018\u0000*\b\b\u0000\u0010\u0001*\u00020\u0002*\u0004\b\u0001\u0010\u00032\u00020\u0002B\u000f\u0012\b\b\u0002\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\u0006\u0010\u0015\u001a\u00020\u0016J\u0016\u0010\u0017\u001a\u00020\u00182\u0006\u0010\u0019\u001a\u00028\u0000H\u0086\u0002¢\u0006\u0002\u0010\u001aJ\u0012\u0010\u001b\u001a\u00020\u00052\b\u0010\u0019\u001a\u0004\u0018\u00010\u0002H\u0002J\"\u0010\u001c\u001a\u00020\u00052\u0006\u0010\u001d\u001a\u00020\u00052\b\u0010\u0019\u001a\u0004\u0018\u00010\u00022\u0006\u0010\u001e\u001a\u00020\u0005H\u0002JD\u0010\u001f\u001a\u00020\u001626\u0010 \u001a2\u0012\u0013\u0012\u00118\u0000¢\u0006\f\b\"\u0012\b\b#\u0012\u0004\b\b(\u0019\u0012\u0013\u0012\u00118\u0001¢\u0006\f\b\"\u0012\b\b#\u0012\u0004\b\b($\u0012\u0004\u0012\u00020\u00160!H\u0086\b\u00f8\u0001\u0000J\u0018\u0010%\u001a\u0004\u0018\u00018\u00012\u0006\u0010\u0019\u001a\u00028\u0000H\u0086\u0002¢\u0006\u0002\u0010&J\u0006\u0010'\u001a\u00020\u0018J\u0006\u0010(\u001a\u00020\u0018J\u0015\u0010)\u001a\u0004\u0018\u00018\u00012\u0006\u0010\u0019\u001a\u00028\u0000¢\u0006\u0002\u0010&JD\u0010*\u001a\u00020\u001626\u0010 \u001a2\u0012\u0013\u0012\u00118\u0000¢\u0006\f\b\"\u0012\b\b#\u0012\u0004\b\b(\u0019\u0012\u0013\u0012\u00118\u0001¢\u0006\f\b\"\u0012\b\b#\u0012\u0004\b\b($\u0012\u0004\u0012\u00020\u00180!H\u0086\b\u00f8\u0001\u0000J/\u0010+\u001a\u00020\u00162!\u0010 \u001a\u001d\u0012\u0013\u0012\u00118\u0001¢\u0006\f\b\"\u0012\b\b#\u0012\u0004\b\b($\u0012\u0004\u0012\u00020\u00180,H\u0086\b\u00f8\u0001\u0000J\u001e\u0010-\u001a\u00020\u00162\u0006\u0010\u0019\u001a\u00028\u00002\u0006\u0010$\u001a\u00028\u0001H\u0086\u0002¢\u0006\u0002\u0010.R$\u0010\u0007\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00020\bX\u0080\u000e¢\u0006\u0010\n\u0002\u0010\r\u001a\u0004\b\t\u0010\n\"\u0004\b\u000b\u0010\fR\u001a\u0010\u000e\u001a\u00020\u0005X\u0080\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u000f\u0010\u0010\"\u0004\b\u0011\u0010\u0006R$\u0010\u0012\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00020\bX\u0080\u000e¢\u0006\u0010\n\u0002\u0010\r\u001a\u0004\b\u0013\u0010\n\"\u0004\b\u0014\u0010\f\u0082\u0002\u0007\n\u0005\b\u009920\u0001¨\u0006/" }, d2 = { "Landroidx/compose/runtime/collection/IdentityArrayMap;", "Key", "", "Value", "capacity", "", "(I)V", "keys", "", "getKeys$runtime_release", "()[Ljava/lang/Object;", "setKeys$runtime_release", "([Ljava/lang/Object;)V", "[Ljava/lang/Object;", "size", "getSize$runtime_release", "()I", "setSize$runtime_release", "values", "getValues$runtime_release", "setValues$runtime_release", "clear", "", "contains", "", "key", "(Ljava/lang/Object;)Z", "find", "findExactIndex", "midIndex", "keyHash", "forEach", "block", "Lkotlin/Function2;", "Lkotlin/ParameterName;", "name", "value", "get", "(Ljava/lang/Object;)Ljava/lang/Object;", "isEmpty", "isNotEmpty", "remove", "removeIf", "removeValueIf", "Lkotlin/Function1;", "set", "(Ljava/lang/Object;Ljava/lang/Object;)V", "runtime_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
public final class IdentityArrayMap<Key, Value>
{
    private Object[] keys;
    private int size;
    private Object[] values;
    
    public IdentityArrayMap() {
        this(0, 1, null);
    }
    
    public IdentityArrayMap(final int n) {
        this.keys = new Object[n];
        this.values = new Object[n];
    }
    
    private final int find(final Object o) {
        final int identityHashCode = ActualJvm_jvmKt.identityHashCode(o);
        int n = this.size - 1;
        int i = 0;
        while (i <= n) {
            final int n2 = i + n >>> 1;
            final Object o2 = this.keys[n2];
            final int identityHashCode2 = ActualJvm_jvmKt.identityHashCode(o2);
            if (identityHashCode2 < identityHashCode) {
                i = n2 + 1;
            }
            else if (identityHashCode2 > identityHashCode) {
                n = n2 - 1;
            }
            else {
                if (o == o2) {
                    return n2;
                }
                return this.findExactIndex(n2, o, identityHashCode);
            }
        }
        return -(i + 1);
    }
    
    private final int findExactIndex(int i, final Object o, final int n) {
        for (int n2 = i - 1; -1 < n2; --n2) {
            final Object o2 = this.keys[n2];
            if (o2 == o) {
                return n2;
            }
            if (ActualJvm_jvmKt.identityHashCode(o2) != n) {
                break;
            }
        }
        ++i;
        while (i < this.size) {
            final Object o3 = this.keys[i];
            if (o3 == o) {
                return i;
            }
            if (ActualJvm_jvmKt.identityHashCode(o3) != n) {
                return -(i + 1);
            }
            ++i;
        }
        i = this.size;
        return -(i + 1);
    }
    
    public final void clear() {
        this.size = 0;
        ArraysKt.fill$default(this.keys, (Object)null, 0, 0, 6, (Object)null);
        ArraysKt.fill$default(this.values, (Object)null, 0, 0, 6, (Object)null);
    }
    
    public final boolean contains(final Key key) {
        Intrinsics.checkNotNullParameter((Object)key, "key");
        return this.find(key) >= 0;
    }
    
    public final void forEach(final Function2<? super Key, ? super Value, Unit> function2) {
        Intrinsics.checkNotNullParameter((Object)function2, "block");
        for (int size$runtime_release = this.getSize$runtime_release(), i = 0; i < size$runtime_release; ++i) {
            final Object o = this.getKeys$runtime_release()[i];
            Intrinsics.checkNotNull(o, "null cannot be cast to non-null type Key of androidx.compose.runtime.collection.IdentityArrayMap");
            function2.invoke(o, this.getValues$runtime_release()[i]);
        }
    }
    
    public final Value get(final Key key) {
        Intrinsics.checkNotNullParameter((Object)key, "key");
        final int find = this.find(key);
        Object o;
        if (find >= 0) {
            o = this.values[find];
        }
        else {
            o = null;
        }
        return (Value)o;
    }
    
    public final Object[] getKeys$runtime_release() {
        return this.keys;
    }
    
    public final int getSize$runtime_release() {
        return this.size;
    }
    
    public final Object[] getValues$runtime_release() {
        return this.values;
    }
    
    public final boolean isEmpty() {
        return this.size == 0;
    }
    
    public final boolean isNotEmpty() {
        return this.size > 0;
    }
    
    public final Value remove(final Key key) {
        Intrinsics.checkNotNullParameter((Object)key, "key");
        final int find = this.find(key);
        if (find >= 0) {
            final Object[] values = this.values;
            final Object o = values[find];
            final int size = this.size;
            final Object[] keys = this.keys;
            final int n = find + 1;
            ArraysKt.copyInto(keys, keys, find, n, size);
            ArraysKt.copyInto(values, values, find, n, size);
            final int size2 = size - 1;
            values[size2] = (keys[size2] = null);
            this.size = size2;
            return (Value)o;
        }
        return null;
    }
    
    public final void removeIf(final Function2<? super Key, ? super Value, Boolean> function2) {
        Intrinsics.checkNotNullParameter((Object)function2, "block");
        final int size$runtime_release = this.getSize$runtime_release();
        int i = 0;
        int size$runtime_release2 = 0;
        while (i < size$runtime_release) {
            final Object o = this.getKeys$runtime_release()[i];
            Intrinsics.checkNotNull(o, "null cannot be cast to non-null type Key of androidx.compose.runtime.collection.IdentityArrayMap");
            int n = size$runtime_release2;
            if (!(boolean)function2.invoke(o, this.getValues$runtime_release()[i])) {
                if (size$runtime_release2 != i) {
                    this.getKeys$runtime_release()[size$runtime_release2] = o;
                    this.getValues$runtime_release()[size$runtime_release2] = this.getValues$runtime_release()[i];
                }
                n = size$runtime_release2 + 1;
            }
            ++i;
            size$runtime_release2 = n;
        }
        if (this.getSize$runtime_release() > size$runtime_release2) {
            for (int size$runtime_release3 = this.getSize$runtime_release(), j = size$runtime_release2; j < size$runtime_release3; ++j) {
                this.getKeys$runtime_release()[j] = null;
                this.getValues$runtime_release()[j] = null;
            }
            this.setSize$runtime_release(size$runtime_release2);
        }
    }
    
    public final void removeValueIf(final Function1<? super Value, Boolean> function1) {
        Intrinsics.checkNotNullParameter((Object)function1, "block");
        final int size$runtime_release = this.getSize$runtime_release();
        int i = 0;
        int size$runtime_release2 = 0;
        while (i < size$runtime_release) {
            final Object o = this.getKeys$runtime_release()[i];
            Intrinsics.checkNotNull(o, "null cannot be cast to non-null type Key of androidx.compose.runtime.collection.IdentityArrayMap");
            int n = size$runtime_release2;
            if (!(boolean)function1.invoke(this.getValues$runtime_release()[i])) {
                if (size$runtime_release2 != i) {
                    this.getKeys$runtime_release()[size$runtime_release2] = o;
                    this.getValues$runtime_release()[size$runtime_release2] = this.getValues$runtime_release()[i];
                }
                n = size$runtime_release2 + 1;
            }
            ++i;
            size$runtime_release2 = n;
        }
        if (this.getSize$runtime_release() > size$runtime_release2) {
            for (int size$runtime_release3 = this.getSize$runtime_release(), j = size$runtime_release2; j < size$runtime_release3; ++j) {
                this.getKeys$runtime_release()[j] = null;
                this.getValues$runtime_release()[j] = null;
            }
            this.setSize$runtime_release(size$runtime_release2);
        }
    }
    
    public final void set(final Key key, final Value value) {
        Intrinsics.checkNotNullParameter((Object)key, "key");
        final int find = this.find(key);
        if (find >= 0) {
            this.values[find] = value;
        }
        else {
            final int n = -(find + 1);
            final int size = this.size;
            final Object[] keys = this.keys;
            final boolean b = size == keys.length;
            Object[] keys2;
            if (b) {
                keys2 = new Object[size * 2];
            }
            else {
                keys2 = keys;
            }
            final int n2 = n + 1;
            ArraysKt.copyInto(keys, keys2, n2, n, size);
            if (b) {
                ArraysKt.copyInto$default(this.keys, keys2, 0, 0, n, 6, (Object)null);
            }
            keys2[n] = key;
            this.keys = keys2;
            Object[] values;
            if (b) {
                values = new Object[this.size * 2];
            }
            else {
                values = this.values;
            }
            ArraysKt.copyInto(this.values, values, n2, n, this.size);
            if (b) {
                ArraysKt.copyInto$default(this.values, values, 0, 0, n, 6, (Object)null);
            }
            values[n] = value;
            this.values = values;
            ++this.size;
        }
    }
    
    public final void setKeys$runtime_release(final Object[] keys) {
        Intrinsics.checkNotNullParameter((Object)keys, "<set-?>");
        this.keys = keys;
    }
    
    public final void setSize$runtime_release(final int size) {
        this.size = size;
    }
    
    public final void setValues$runtime_release(final Object[] values) {
        Intrinsics.checkNotNullParameter((Object)values, "<set-?>");
        this.values = values;
    }
}

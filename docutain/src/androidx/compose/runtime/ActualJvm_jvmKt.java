// 
// Decompiled by Procyon v0.6.0
// 

package androidx.compose.runtime;

import kotlin.jvm.internal.InlineMarker;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.TypeIntrinsics;
import kotlin.Unit;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.internal.Intrinsics;
import kotlin.Metadata;

@Metadata(d1 = { "\u00004\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u000b\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u001a\u0010\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\u0000\u001a\u0012\u0010\u0004\u001a\u00020\u00052\b\u0010\u0006\u001a\u0004\u0018\u00010\u0003H\u0000\u001a(\u0010\u0007\u001a\u00020\u00012\u0006\u0010\b\u001a\u00020\t2\u0011\u0010\n\u001a\r\u0012\u0004\u0012\u00020\u00010\u000b¢\u0006\u0002\b\fH\u0000¢\u0006\u0002\u0010\r\u001a.\u0010\u000e\u001a\u0002H\u000f\"\u0004\b\u0000\u0010\u000f2\u0006\u0010\b\u001a\u00020\t2\u0011\u0010\n\u001a\r\u0012\u0004\u0012\u0002H\u000f0\u000b¢\u0006\u0002\b\fH\u0000¢\u0006\u0002\u0010\u0010\u001a-\u0010\u0011\u001a\u0002H\u0012\"\u0004\b\u0000\u0010\u00122\u0006\u0010\u0013\u001a\u00020\u00032\f\u0010\u0014\u001a\b\u0012\u0004\u0012\u0002H\u00120\u000bH\u0081\b\u00f8\u0001\u0000¢\u0006\u0002\u0010\u0015*\u001e\b\u0000\u0010\u0016\u001a\u0004\b\u0000\u0010\u0017\"\b\u0012\u0004\u0012\u0002H\u00170\u00182\b\u0012\u0004\u0012\u0002H\u00170\u0018*\f\b\u0000\u0010\u0019\"\u00020\u001a2\u00020\u001a\u0082\u0002\u0007\n\u0005\b\u009920\u0001¨\u0006\u001b" }, d2 = { "ensureMutable", "", "it", "", "identityHashCode", "", "instance", "invokeComposable", "composer", "Landroidx/compose/runtime/Composer;", "composable", "Lkotlin/Function0;", "Landroidx/compose/runtime/Composable;", "(Landroidx/compose/runtime/Composer;Lkotlin/jvm/functions/Function2;)V", "invokeComposableForResult", "T", "(Landroidx/compose/runtime/Composer;Lkotlin/jvm/functions/Function2;)Ljava/lang/Object;", "synchronized", "R", "lock", "block", "(Ljava/lang/Object;Lkotlin/jvm/functions/Function0;)Ljava/lang/Object;", "AtomicReference", "V", "Ljava/util/concurrent/atomic/AtomicReference;", "TestOnly", "Lorg/jetbrains/annotations/TestOnly;", "runtime_release" }, k = 2, mv = { 1, 8, 0 }, xi = 48)
public final class ActualJvm_jvmKt
{
    public static final void ensureMutable(final Object o) {
        Intrinsics.checkNotNullParameter(o, "it");
    }
    
    public static final int identityHashCode(final Object o) {
        return System.identityHashCode(o);
    }
    
    public static final void invokeComposable(final Composer composer, final Function2<? super Composer, ? super Integer, Unit> function2) {
        Intrinsics.checkNotNullParameter((Object)composer, "composer");
        Intrinsics.checkNotNullParameter((Object)function2, "composable");
        ((Function2)TypeIntrinsics.beforeCheckcastToFunctionOfArity((Object)function2, 2)).invoke((Object)composer, (Object)1);
    }
    
    public static final <T> T invokeComposableForResult(final Composer composer, final Function2<? super Composer, ? super Integer, ? extends T> function2) {
        Intrinsics.checkNotNullParameter((Object)composer, "composer");
        Intrinsics.checkNotNullParameter((Object)function2, "composable");
        return (T)((Function2)TypeIntrinsics.beforeCheckcastToFunctionOfArity((Object)function2, 2)).invoke((Object)composer, (Object)1);
    }
    
    public static final <R> R synchronized(final Object o, final Function0<? extends R> function0) {
        Intrinsics.checkNotNullParameter(o, "lock");
        Intrinsics.checkNotNullParameter((Object)function0, "block");
        monitorenter(o);
        try {
            return (R)function0.invoke();
        }
        finally {
            InlineMarker.finallyStart(1);
            monitorexit(o);
            InlineMarker.finallyEnd(1);
        }
    }
}

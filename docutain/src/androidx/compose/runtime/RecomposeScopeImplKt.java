// 
// Decompiled by Procyon v0.6.0
// 

package androidx.compose.runtime;

import kotlin.Metadata;

@Metadata(d1 = { "\u0000\n\n\u0000\n\u0002\u0010\b\n\u0002\b\u000b\u001a\u0010\u0010\n\u001a\u00020\u00012\u0006\u0010\u000b\u001a\u00020\u0001H\u0001\"\u000e\u0010\u0000\u001a\u00020\u0001X\u0082T¢\u0006\u0002\n\u0000\"\u000e\u0010\u0002\u001a\u00020\u0001X\u0082T¢\u0006\u0002\n\u0000\"\u000e\u0010\u0003\u001a\u00020\u0001X\u0082T¢\u0006\u0002\n\u0000\"\u000e\u0010\u0004\u001a\u00020\u0001X\u0082T¢\u0006\u0002\n\u0000\"\u000e\u0010\u0005\u001a\u00020\u0001X\u0082T¢\u0006\u0002\n\u0000\"\u000e\u0010\u0006\u001a\u00020\u0001X\u0082T¢\u0006\u0002\n\u0000\"\u000e\u0010\u0007\u001a\u00020\u0001X\u0082T¢\u0006\u0002\n\u0000\"\u000e\u0010\b\u001a\u00020\u0001X\u0082T¢\u0006\u0002\n\u0000\"\u000e\u0010\t\u001a\u00020\u0001X\u0082T¢\u0006\u0002\n\u0000¨\u0006\f" }, d2 = { "DefaultsInScopeFlag", "", "DefaultsInvalidFlag", "RequiresRecomposeFlag", "RereadingFlag", "SkippedFlag", "UsedFlag", "changedHighBitMask", "changedLowBitMask", "changedMask", "updateChangedFlags", "flags", "runtime_release" }, k = 2, mv = { 1, 8, 0 }, xi = 48)
public final class RecomposeScopeImplKt
{
    private static final int DefaultsInScopeFlag = 2;
    private static final int DefaultsInvalidFlag = 4;
    private static final int RequiresRecomposeFlag = 8;
    private static final int RereadingFlag = 32;
    private static final int SkippedFlag = 16;
    private static final int UsedFlag = 1;
    private static final int changedHighBitMask = 613566756;
    private static final int changedLowBitMask = 306783378;
    private static final int changedMask = -920350135;
    
    public static final int updateChangedFlags(final int n) {
        final int n2 = 0x12492492 & n;
        final int n3 = 0x24924924 & n;
        return (n & 0xC9249249) | (n3 >> 1 | n2) | (n2 << 1 & n3);
    }
}

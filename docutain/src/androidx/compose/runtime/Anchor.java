// 
// Decompiled by Procyon v0.6.0
// 

package androidx.compose.runtime;

import kotlin.jvm.internal.Intrinsics;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0000\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u000e\u0010\r\u001a\u00020\u00032\u0006\u0010\u000e\u001a\u00020\u000fJ\u000e\u0010\r\u001a\u00020\u00032\u0006\u0010\u0010\u001a\u00020\u0011R\u001a\u0010\u0005\u001a\u00020\u0003X\u0080\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0006\u0010\u0007\"\u0004\b\b\u0010\u0004R\u0011\u0010\t\u001a\u00020\n8F¢\u0006\u0006\u001a\u0004\b\u000b\u0010\f¨\u0006\u0012" }, d2 = { "Landroidx/compose/runtime/Anchor;", "", "loc", "", "(I)V", "location", "getLocation$runtime_release", "()I", "setLocation$runtime_release", "valid", "", "getValid", "()Z", "toIndexFor", "slots", "Landroidx/compose/runtime/SlotTable;", "writer", "Landroidx/compose/runtime/SlotWriter;", "runtime_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
public final class Anchor
{
    private int location;
    
    public Anchor(final int location) {
        this.location = location;
    }
    
    public final int getLocation$runtime_release() {
        return this.location;
    }
    
    public final boolean getValid() {
        return this.location != Integer.MIN_VALUE;
    }
    
    public final void setLocation$runtime_release(final int location) {
        this.location = location;
    }
    
    public final int toIndexFor(final SlotTable slotTable) {
        Intrinsics.checkNotNullParameter((Object)slotTable, "slots");
        return slotTable.anchorIndex(this);
    }
    
    public final int toIndexFor(final SlotWriter slotWriter) {
        Intrinsics.checkNotNullParameter((Object)slotWriter, "writer");
        return slotWriter.anchorIndex(this);
    }
}

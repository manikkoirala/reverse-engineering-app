// 
// Decompiled by Procyon v0.6.0
// 

package androidx.compose.runtime.snapshots;

import androidx.compose.runtime.external.kotlinx.collections.immutable.PersistentMap;
import kotlin.Unit;
import kotlin.collections.CollectionsKt;
import java.util.Map;
import java.util.Set;
import java.util.Iterator;
import java.util.Collection;
import kotlin.KotlinNothingValueException;
import kotlin.jvm.internal.Intrinsics;
import kotlin.Metadata;

@Metadata(d1 = { "\u00004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0001\n\u0002\b\u0004\n\u0002\u0010\u001e\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\b\u0002\u0018\u0000*\u0004\b\u0000\u0010\u0001*\u0004\b\u0001\u0010\u00022\u0014\u0012\u0004\u0012\u0002H\u0001\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00020\u0003B\u0019\u0012\u0012\u0010\u0004\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u0005¢\u0006\u0002\u0010\u0006J\u0015\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00028\u0001H\u0016¢\u0006\u0002\u0010\nJ\u0016\u0010\u000b\u001a\u00020\b2\f\u0010\f\u001a\b\u0012\u0004\u0012\u00028\u00010\rH\u0016J\u0016\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\t\u001a\u00028\u0001H\u0096\u0002¢\u0006\u0002\u0010\u0010J\u0016\u0010\u0011\u001a\u00020\u000f2\f\u0010\f\u001a\b\u0012\u0004\u0012\u00028\u00010\rH\u0016J\u0015\u0010\u0012\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u0013H\u0096\u0002J\u0015\u0010\u0014\u001a\u00020\u000f2\u0006\u0010\t\u001a\u00028\u0001H\u0016¢\u0006\u0002\u0010\u0010J\u0016\u0010\u0015\u001a\u00020\u000f2\f\u0010\f\u001a\b\u0012\u0004\u0012\u00028\u00010\rH\u0016J\u0016\u0010\u0016\u001a\u00020\u000f2\f\u0010\f\u001a\b\u0012\u0004\u0012\u00028\u00010\rH\u0016¨\u0006\u0017" }, d2 = { "Landroidx/compose/runtime/snapshots/SnapshotMapValueSet;", "K", "V", "Landroidx/compose/runtime/snapshots/SnapshotMapSet;", "map", "Landroidx/compose/runtime/snapshots/SnapshotStateMap;", "(Landroidx/compose/runtime/snapshots/SnapshotStateMap;)V", "add", "", "element", "(Ljava/lang/Object;)Ljava/lang/Void;", "addAll", "elements", "", "contains", "", "(Ljava/lang/Object;)Z", "containsAll", "iterator", "Landroidx/compose/runtime/snapshots/StateMapMutableValuesIterator;", "remove", "removeAll", "retainAll", "runtime_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
final class SnapshotMapValueSet<K, V> extends SnapshotMapSet<K, V, V>
{
    public SnapshotMapValueSet(final SnapshotStateMap<K, V> snapshotStateMap) {
        Intrinsics.checkNotNullParameter((Object)snapshotStateMap, "map");
        super(snapshotStateMap);
    }
    
    public Void add(final V v) {
        SnapshotStateMapKt.unsupported();
        throw new KotlinNothingValueException();
    }
    
    public Void addAll(final Collection<? extends V> collection) {
        Intrinsics.checkNotNullParameter((Object)collection, "elements");
        SnapshotStateMapKt.unsupported();
        throw new KotlinNothingValueException();
    }
    
    @Override
    public boolean contains(final Object o) {
        return this.getMap().containsValue(o);
    }
    
    @Override
    public boolean containsAll(final Collection<?> collection) {
        Intrinsics.checkNotNullParameter((Object)collection, "elements");
        final Iterable iterable = collection;
        final boolean empty = ((Collection)iterable).isEmpty();
        final boolean b = true;
        boolean b2;
        if (empty) {
            b2 = b;
        }
        else {
            final Iterator iterator = iterable.iterator();
            do {
                b2 = b;
                if (iterator.hasNext()) {
                    continue;
                }
                return b2;
            } while (this.getMap().containsValue(iterator.next()));
            b2 = false;
        }
        return b2;
    }
    
    @Override
    public StateMapMutableValuesIterator<K, V> iterator() {
        return new StateMapMutableValuesIterator<K, V>(this.getMap(), (Iterator<? extends Map.Entry<? extends K, ? extends V>>)this.getMap().getReadable$runtime_release().getMap$runtime_release().entrySet().iterator());
    }
    
    @Override
    public boolean remove(final Object o) {
        return this.getMap().removeValue$runtime_release((V)o);
    }
    
    @Override
    public boolean removeAll(final Collection<?> collection) {
        Intrinsics.checkNotNullParameter((Object)collection, "elements");
        final Set set = CollectionsKt.toSet((Iterable)collection);
        final SnapshotStateMap<K, V> map = this.getMap();
        boolean b = false;
        while (true) {
            Object map$runtime_release = SnapshotStateMapKt.access$getSync$p();
            synchronized (map$runtime_release) {
                final StateRecord firstStateRecord = map.getFirstStateRecord();
                Intrinsics.checkNotNull((Object)firstStateRecord, "null cannot be cast to non-null type androidx.compose.runtime.snapshots.SnapshotStateMap.StateMapStateRecord<K of androidx.compose.runtime.snapshots.SnapshotStateMap, V of androidx.compose.runtime.snapshots.SnapshotStateMap>");
                final SnapshotStateMap.StateMapStateRecord stateMapStateRecord = SnapshotKt.current(firstStateRecord);
                final PersistentMap map$runtime_release2 = stateMapStateRecord.getMap$runtime_release();
                final int modification$runtime_release = stateMapStateRecord.getModification$runtime_release();
                final Unit instance = Unit.INSTANCE;
                monitorexit(map$runtime_release);
                Intrinsics.checkNotNull((Object)map$runtime_release2);
                map$runtime_release = map$runtime_release2.builder();
                final Map map2 = (Map)map$runtime_release;
                final Iterator iterator = map.entrySet().iterator();
                boolean b2 = true;
                boolean b3 = b;
                while (iterator.hasNext()) {
                    final Map.Entry<K, Object> entry = (Map.Entry<K, Object>)iterator.next();
                    if (set.contains(entry.getValue())) {
                        map2.remove(entry.getKey());
                        b3 = true;
                    }
                }
                final Unit instance2 = Unit.INSTANCE;
                map$runtime_release = ((PersistentMap.Builder)map$runtime_release).build();
                if (!Intrinsics.areEqual(map$runtime_release, (Object)map$runtime_release2)) {
                    final StateRecord firstStateRecord2 = map.getFirstStateRecord();
                    Intrinsics.checkNotNull((Object)firstStateRecord2, "null cannot be cast to non-null type androidx.compose.runtime.snapshots.SnapshotStateMap.StateMapStateRecord<K of androidx.compose.runtime.snapshots.SnapshotStateMap, V of androidx.compose.runtime.snapshots.SnapshotStateMap>");
                    final StateRecord stateRecord = firstStateRecord2;
                    SnapshotKt.getSnapshotInitializer();
                    synchronized (SnapshotKt.getLock()) {
                        final Snapshot current = Snapshot.Companion.getCurrent();
                        final SnapshotStateMap.StateMapStateRecord stateMapStateRecord2 = SnapshotKt.writableRecord(stateRecord, map, current);
                        synchronized (SnapshotStateMapKt.access$getSync$p()) {
                            if (stateMapStateRecord2.getModification$runtime_release() == modification$runtime_release) {
                                stateMapStateRecord2.setMap$runtime_release((PersistentMap)map$runtime_release);
                                stateMapStateRecord2.setModification$runtime_release(stateMapStateRecord2.getModification$runtime_release() + 1);
                            }
                            else {
                                b2 = false;
                            }
                            monitorexit(SnapshotStateMapKt.access$getSync$p());
                            monitorexit(SnapshotKt.getLock());
                            SnapshotKt.notifyWrite(current, map);
                            b = b3;
                            if (b2) {
                                return b3;
                            }
                            continue;
                        }
                    }
                }
                return b3;
            }
        }
    }
    
    @Override
    public boolean retainAll(final Collection<?> collection) {
        Intrinsics.checkNotNullParameter((Object)collection, "elements");
        final Set set = CollectionsKt.toSet((Iterable)collection);
        final SnapshotStateMap<K, V> map = this.getMap();
        boolean b = false;
        while (true) {
            Object map$runtime_release = SnapshotStateMapKt.access$getSync$p();
            synchronized (map$runtime_release) {
                final StateRecord firstStateRecord = map.getFirstStateRecord();
                Intrinsics.checkNotNull((Object)firstStateRecord, "null cannot be cast to non-null type androidx.compose.runtime.snapshots.SnapshotStateMap.StateMapStateRecord<K of androidx.compose.runtime.snapshots.SnapshotStateMap, V of androidx.compose.runtime.snapshots.SnapshotStateMap>");
                final SnapshotStateMap.StateMapStateRecord stateMapStateRecord = SnapshotKt.current(firstStateRecord);
                final PersistentMap map$runtime_release2 = stateMapStateRecord.getMap$runtime_release();
                final int modification$runtime_release = stateMapStateRecord.getModification$runtime_release();
                final Unit instance = Unit.INSTANCE;
                monitorexit(map$runtime_release);
                Intrinsics.checkNotNull((Object)map$runtime_release2);
                map$runtime_release = map$runtime_release2.builder();
                final Map map2 = (Map)map$runtime_release;
                final Iterator iterator = map.entrySet().iterator();
                boolean b2 = true;
                boolean b3 = b;
                while (iterator.hasNext()) {
                    final Map.Entry<K, Object> entry = (Map.Entry<K, Object>)iterator.next();
                    if (set.contains(entry.getValue()) ^ true) {
                        map2.remove(entry.getKey());
                        b3 = true;
                    }
                }
                final Unit instance2 = Unit.INSTANCE;
                map$runtime_release = ((PersistentMap.Builder)map$runtime_release).build();
                if (!Intrinsics.areEqual(map$runtime_release, (Object)map$runtime_release2)) {
                    final StateRecord firstStateRecord2 = map.getFirstStateRecord();
                    Intrinsics.checkNotNull((Object)firstStateRecord2, "null cannot be cast to non-null type androidx.compose.runtime.snapshots.SnapshotStateMap.StateMapStateRecord<K of androidx.compose.runtime.snapshots.SnapshotStateMap, V of androidx.compose.runtime.snapshots.SnapshotStateMap>");
                    final StateRecord stateRecord = firstStateRecord2;
                    SnapshotKt.getSnapshotInitializer();
                    synchronized (SnapshotKt.getLock()) {
                        final Snapshot current = Snapshot.Companion.getCurrent();
                        final SnapshotStateMap.StateMapStateRecord stateMapStateRecord2 = SnapshotKt.writableRecord(stateRecord, map, current);
                        synchronized (SnapshotStateMapKt.access$getSync$p()) {
                            if (stateMapStateRecord2.getModification$runtime_release() == modification$runtime_release) {
                                stateMapStateRecord2.setMap$runtime_release((PersistentMap)map$runtime_release);
                                stateMapStateRecord2.setModification$runtime_release(stateMapStateRecord2.getModification$runtime_release() + 1);
                            }
                            else {
                                b2 = false;
                            }
                            monitorexit(SnapshotStateMapKt.access$getSync$p());
                            monitorexit(SnapshotKt.getLock());
                            SnapshotKt.notifyWrite(current, map);
                            b = b3;
                            if (b2) {
                                return b3;
                            }
                            continue;
                        }
                    }
                }
                return b3;
            }
        }
    }
}

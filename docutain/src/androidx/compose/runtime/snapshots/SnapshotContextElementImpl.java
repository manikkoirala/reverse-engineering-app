// 
// Decompiled by Procyon v0.6.0
// 

package androidx.compose.runtime.snapshots;

import kotlin.coroutines.CoroutineContext;
import kotlin.coroutines.CoroutineContext$Key;
import kotlin.coroutines.CoroutineContext$Element;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.internal.Intrinsics;
import kotlin.Metadata;
import kotlinx.coroutines.ThreadContextElement;

@Metadata(d1 = { "\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\b\u0003\u0018\u00002\u00020\u00012\n\u0012\u0006\u0012\u0004\u0018\u00010\u00030\u0002B\r\u0012\u0006\u0010\u0004\u001a\u00020\u0003¢\u0006\u0002\u0010\u0005J\u001a\u0010\n\u001a\u00020\u000b2\u0006\u0010\f\u001a\u00020\r2\b\u0010\u000e\u001a\u0004\u0018\u00010\u0003H\u0016J\u0012\u0010\u000f\u001a\u0004\u0018\u00010\u00032\u0006\u0010\f\u001a\u00020\rH\u0016R\u0018\u0010\u0006\u001a\u0006\u0012\u0002\b\u00030\u00078VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\b\u0010\tR\u000e\u0010\u0004\u001a\u00020\u0003X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u0010" }, d2 = { "Landroidx/compose/runtime/snapshots/SnapshotContextElementImpl;", "Landroidx/compose/runtime/snapshots/SnapshotContextElement;", "Lkotlinx/coroutines/ThreadContextElement;", "Landroidx/compose/runtime/snapshots/Snapshot;", "snapshot", "(Landroidx/compose/runtime/snapshots/Snapshot;)V", "key", "Lkotlin/coroutines/CoroutineContext$Key;", "getKey", "()Lkotlin/coroutines/CoroutineContext$Key;", "restoreThreadContext", "", "context", "Lkotlin/coroutines/CoroutineContext;", "oldState", "updateThreadContext", "runtime_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
final class SnapshotContextElementImpl implements SnapshotContextElement, ThreadContextElement<Snapshot>
{
    private final Snapshot snapshot;
    
    public SnapshotContextElementImpl(final Snapshot snapshot) {
        Intrinsics.checkNotNullParameter((Object)snapshot, "snapshot");
        this.snapshot = snapshot;
    }
    
    public <R> R fold(final R r, final Function2<? super R, ? super CoroutineContext$Element, ? extends R> function2) {
        return DefaultImpls.fold(this, r, function2);
    }
    
    public <E extends CoroutineContext$Element> E get(final CoroutineContext$Key<E> coroutineContext$Key) {
        return DefaultImpls.get(this, coroutineContext$Key);
    }
    
    public CoroutineContext$Key<?> getKey() {
        return (CoroutineContext$Key<?>)SnapshotContextElement.Key;
    }
    
    public CoroutineContext minusKey(final CoroutineContext$Key<?> coroutineContext$Key) {
        return DefaultImpls.minusKey(this, coroutineContext$Key);
    }
    
    public CoroutineContext plus(final CoroutineContext coroutineContext) {
        return DefaultImpls.plus(this, coroutineContext);
    }
    
    public void restoreThreadContext(final CoroutineContext coroutineContext, final Snapshot snapshot) {
        Intrinsics.checkNotNullParameter((Object)coroutineContext, "context");
        this.snapshot.unsafeLeave(snapshot);
    }
    
    public Snapshot updateThreadContext(final CoroutineContext coroutineContext) {
        Intrinsics.checkNotNullParameter((Object)coroutineContext, "context");
        return this.snapshot.unsafeEnter();
    }
}

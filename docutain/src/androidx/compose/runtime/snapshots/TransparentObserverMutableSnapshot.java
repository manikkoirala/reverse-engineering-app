// 
// Decompiled by Procyon v0.6.0
// 

package androidx.compose.runtime.snapshots;

import kotlin.KotlinNothingValueException;
import java.util.Set;
import kotlin.jvm.internal.Intrinsics;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000V\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0006\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010#\n\u0002\u0018\u0002\n\u0002\b\f\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0001\n\u0000\n\u0002\u0018\u0002\n\u0002\b\r\b\u0000\u0018\u00002\u00020\u0001BK\u0012\b\u0010\u0002\u001a\u0004\u0018\u00010\u0001\u0012\u0014\u0010\u0003\u001a\u0010\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u0004\u0012\u0014\u0010\u0007\u001a\u0010\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u0004\u0012\u0006\u0010\b\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\t¢\u0006\u0002\u0010\u000bJ\b\u0010)\u001a\u00020*H\u0016J\b\u0010+\u001a\u00020\u0006H\u0016J\b\u0010,\u001a\u00020\tH\u0016J\u0015\u0010-\u001a\u00020.2\u0006\u0010/\u001a\u000200H\u0010¢\u0006\u0002\b1J\u0015\u00102\u001a\u00020.2\u0006\u0010/\u001a\u000200H\u0010¢\u0006\u0002\b3J\r\u00104\u001a\u00020\u0006H\u0010¢\u0006\u0002\b5J\u0015\u00106\u001a\u00020\u00062\u0006\u00107\u001a\u00020\u001dH\u0010¢\u0006\u0002\b8J4\u00109\u001a\u00020\u00012\u0014\u0010:\u001a\u0010\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u00042\u0014\u0010;\u001a\u0010\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u0004H\u0016J\u001e\u0010<\u001a\u0002002\u0014\u0010:\u001a\u0010\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u0004H\u0016R\u0014\u0010\f\u001a\u00020\u00018BX\u0082\u0004¢\u0006\u0006\u001a\u0004\b\r\u0010\u000eR$\u0010\u0011\u001a\u00020\u00102\u0006\u0010\u000f\u001a\u00020\u00108V@PX\u0096\u000e¢\u0006\f\u001a\u0004\b\u0012\u0010\u0013\"\u0004\b\u0014\u0010\u0015R$\u0010\u0017\u001a\u00020\u00162\u0006\u0010\u000f\u001a\u00020\u00168P@PX\u0090\u000e¢\u0006\f\u001a\u0004\b\u0018\u0010\u0019\"\u0004\b\u001a\u0010\u001bR\u000e\u0010\b\u001a\u00020\tX\u0082\u0004¢\u0006\u0002\n\u0000R4\u0010\u001e\u001a\n\u0012\u0004\u0012\u00020\u001d\u0018\u00010\u001c2\u000e\u0010\u000f\u001a\n\u0012\u0004\u0012\u00020\u001d\u0018\u00010\u001c8P@VX\u0090\u000e¢\u0006\f\u001a\u0004\b\u001f\u0010 \"\u0004\b!\u0010\"R\u000e\u0010\n\u001a\u00020\tX\u0082\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\u0002\u001a\u0004\u0018\u00010\u0001X\u0082\u0004¢\u0006\u0002\n\u0000R\u0014\u0010#\u001a\u00020\t8VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b$\u0010%R\"\u0010\u0003\u001a\u0010\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u0004X\u0080\u0004¢\u0006\b\n\u0000\u001a\u0004\b&\u0010'R\"\u0010\u0007\u001a\u0010\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u0004X\u0080\u0004¢\u0006\b\n\u0000\u001a\u0004\b(\u0010'¨\u0006=" }, d2 = { "Landroidx/compose/runtime/snapshots/TransparentObserverMutableSnapshot;", "Landroidx/compose/runtime/snapshots/MutableSnapshot;", "previousSnapshot", "specifiedReadObserver", "Lkotlin/Function1;", "", "", "specifiedWriteObserver", "mergeParentObservers", "", "ownsPreviousSnapshot", "(Landroidx/compose/runtime/snapshots/MutableSnapshot;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;ZZ)V", "currentSnapshot", "getCurrentSnapshot", "()Landroidx/compose/runtime/snapshots/MutableSnapshot;", "value", "", "id", "getId", "()I", "setId$runtime_release", "(I)V", "Landroidx/compose/runtime/snapshots/SnapshotIdSet;", "invalid", "getInvalid$runtime_release", "()Landroidx/compose/runtime/snapshots/SnapshotIdSet;", "setInvalid$runtime_release", "(Landroidx/compose/runtime/snapshots/SnapshotIdSet;)V", "", "Landroidx/compose/runtime/snapshots/StateObject;", "modified", "getModified$runtime_release", "()Ljava/util/Set;", "setModified", "(Ljava/util/Set;)V", "readOnly", "getReadOnly", "()Z", "getSpecifiedReadObserver$runtime_release", "()Lkotlin/jvm/functions/Function1;", "getSpecifiedWriteObserver$runtime_release", "apply", "Landroidx/compose/runtime/snapshots/SnapshotApplyResult;", "dispose", "hasPendingChanges", "nestedActivated", "", "snapshot", "Landroidx/compose/runtime/snapshots/Snapshot;", "nestedActivated$runtime_release", "nestedDeactivated", "nestedDeactivated$runtime_release", "notifyObjectsInitialized", "notifyObjectsInitialized$runtime_release", "recordModified", "state", "recordModified$runtime_release", "takeNestedMutableSnapshot", "readObserver", "writeObserver", "takeNestedSnapshot", "runtime_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
public final class TransparentObserverMutableSnapshot extends MutableSnapshot
{
    private final boolean mergeParentObservers;
    private final boolean ownsPreviousSnapshot;
    private final MutableSnapshot previousSnapshot;
    private final Function1<Object, Unit> specifiedReadObserver;
    private final Function1<Object, Unit> specifiedWriteObserver;
    
    public TransparentObserverMutableSnapshot(final MutableSnapshot previousSnapshot, final Function1<Object, Unit> specifiedReadObserver, final Function1<Object, Unit> specifiedWriteObserver, final boolean mergeParentObservers, final boolean ownsPreviousSnapshot) {
        final SnapshotIdSet empty = SnapshotIdSet.Companion.getEMPTY();
        Function1<Object, Unit> function1;
        if (previousSnapshot == null || (function1 = previousSnapshot.getReadObserver$runtime_release()) == null) {
            function1 = SnapshotKt.access$getCurrentGlobalSnapshot$p().get().getReadObserver$runtime_release();
        }
        final Function1 access$mergedReadObserver = SnapshotKt.access$mergedReadObserver(specifiedReadObserver, function1, mergeParentObservers);
        Function1<Object, Unit> function2;
        if (previousSnapshot == null || (function2 = previousSnapshot.getWriteObserver$runtime_release()) == null) {
            function2 = SnapshotKt.access$getCurrentGlobalSnapshot$p().get().getWriteObserver$runtime_release();
        }
        super(0, empty, (Function1<Object, Unit>)access$mergedReadObserver, (Function1<Object, Unit>)SnapshotKt.access$mergedWriteObserver(specifiedWriteObserver, function2));
        this.previousSnapshot = previousSnapshot;
        this.specifiedReadObserver = specifiedReadObserver;
        this.specifiedWriteObserver = specifiedWriteObserver;
        this.mergeParentObservers = mergeParentObservers;
        this.ownsPreviousSnapshot = ownsPreviousSnapshot;
    }
    
    private final MutableSnapshot getCurrentSnapshot() {
        MutableSnapshot previousSnapshot;
        if ((previousSnapshot = this.previousSnapshot) == null) {
            final MutableSnapshot value = SnapshotKt.access$getCurrentGlobalSnapshot$p().get();
            Intrinsics.checkNotNullExpressionValue((Object)value, "currentGlobalSnapshot.get()");
            previousSnapshot = value;
        }
        return previousSnapshot;
    }
    
    @Override
    public SnapshotApplyResult apply() {
        return this.getCurrentSnapshot().apply();
    }
    
    @Override
    public void dispose() {
        this.setDisposed$runtime_release(true);
        if (this.ownsPreviousSnapshot) {
            final MutableSnapshot previousSnapshot = this.previousSnapshot;
            if (previousSnapshot != null) {
                previousSnapshot.dispose();
            }
        }
    }
    
    @Override
    public int getId() {
        return this.getCurrentSnapshot().getId();
    }
    
    @Override
    public SnapshotIdSet getInvalid$runtime_release() {
        return this.getCurrentSnapshot().getInvalid$runtime_release();
    }
    
    @Override
    public Set<StateObject> getModified$runtime_release() {
        return this.getCurrentSnapshot().getModified$runtime_release();
    }
    
    @Override
    public boolean getReadOnly() {
        return this.getCurrentSnapshot().getReadOnly();
    }
    
    public final Function1<Object, Unit> getSpecifiedReadObserver$runtime_release() {
        return this.specifiedReadObserver;
    }
    
    public final Function1<Object, Unit> getSpecifiedWriteObserver$runtime_release() {
        return this.specifiedWriteObserver;
    }
    
    @Override
    public boolean hasPendingChanges() {
        return this.getCurrentSnapshot().hasPendingChanges();
    }
    
    public Void nestedActivated$runtime_release(final Snapshot snapshot) {
        Intrinsics.checkNotNullParameter((Object)snapshot, "snapshot");
        SnapshotStateMapKt.unsupported();
        throw new KotlinNothingValueException();
    }
    
    public Void nestedDeactivated$runtime_release(final Snapshot snapshot) {
        Intrinsics.checkNotNullParameter((Object)snapshot, "snapshot");
        SnapshotStateMapKt.unsupported();
        throw new KotlinNothingValueException();
    }
    
    @Override
    public void notifyObjectsInitialized$runtime_release() {
        this.getCurrentSnapshot().notifyObjectsInitialized$runtime_release();
    }
    
    @Override
    public void recordModified$runtime_release(final StateObject stateObject) {
        Intrinsics.checkNotNullParameter((Object)stateObject, "state");
        this.getCurrentSnapshot().recordModified$runtime_release(stateObject);
    }
    
    @Override
    public void setId$runtime_release(final int n) {
        SnapshotStateMapKt.unsupported();
        throw new KotlinNothingValueException();
    }
    
    @Override
    public void setInvalid$runtime_release(final SnapshotIdSet set) {
        Intrinsics.checkNotNullParameter((Object)set, "value");
        SnapshotStateMapKt.unsupported();
        throw new KotlinNothingValueException();
    }
    
    @Override
    public void setModified(final Set<StateObject> set) {
        SnapshotStateMapKt.unsupported();
        throw new KotlinNothingValueException();
    }
    
    @Override
    public MutableSnapshot takeNestedMutableSnapshot(final Function1<Object, Unit> function1, final Function1<Object, Unit> function2) {
        final Function1 mergedReadObserver$default = SnapshotKt.mergedReadObserver$default(function1, this.getReadObserver$runtime_release(), false, 4, null);
        final Function1 access$mergedWriteObserver = mergedWriteObserver(function2, this.getWriteObserver$runtime_release());
        MutableSnapshot takeNestedMutableSnapshot;
        if (!this.mergeParentObservers) {
            takeNestedMutableSnapshot = new TransparentObserverMutableSnapshot(this.getCurrentSnapshot().takeNestedMutableSnapshot(null, (Function1<Object, Unit>)access$mergedWriteObserver), (Function1<Object, Unit>)mergedReadObserver$default, (Function1<Object, Unit>)access$mergedWriteObserver, false, true);
        }
        else {
            takeNestedMutableSnapshot = this.getCurrentSnapshot().takeNestedMutableSnapshot((Function1<Object, Unit>)mergedReadObserver$default, (Function1<Object, Unit>)access$mergedWriteObserver);
        }
        return takeNestedMutableSnapshot;
    }
    
    @Override
    public Snapshot takeNestedSnapshot(final Function1<Object, Unit> function1) {
        final Function1 mergedReadObserver$default = SnapshotKt.mergedReadObserver$default(function1, this.getReadObserver$runtime_release(), false, 4, null);
        Snapshot snapshot;
        if (!this.mergeParentObservers) {
            snapshot = createTransparentSnapshotWithNoParentReadObserver(this.getCurrentSnapshot().takeNestedSnapshot(null), (Function1<Object, Unit>)mergedReadObserver$default, true);
        }
        else {
            snapshot = this.getCurrentSnapshot().takeNestedSnapshot((Function1<Object, Unit>)mergedReadObserver$default);
        }
        return snapshot;
    }
}

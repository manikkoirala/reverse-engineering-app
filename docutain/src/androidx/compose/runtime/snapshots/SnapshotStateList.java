// 
// Decompiled by Procyon v0.6.0
// 

package androidx.compose.runtime.snapshots;

import kotlin.jvm.internal.CollectionToArray;
import java.util.ListIterator;
import java.util.Iterator;
import java.util.Collection;
import kotlin.jvm.internal.InlineMarker;
import kotlin.Unit;
import kotlin.jvm.internal.Intrinsics;
import androidx.compose.runtime.external.kotlinx.collections.immutable.PersistentList;
import kotlin.jvm.functions.Function1;
import androidx.compose.runtime.external.kotlinx.collections.immutable.ExtensionsKt;
import kotlin.Metadata;
import kotlin.jvm.internal.markers.KMutableList;
import java.util.List;

@Metadata(d1 = { "\u0000n\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0010\u001e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0010)\n\u0002\b\u0002\n\u0002\u0010+\n\u0002\b\u0017\n\u0002\u0018\u0002\n\u0002\b\u0003\b\u0007\u0018\u0000*\u0004\b\u0000\u0010\u00012\b\u0012\u0004\u0012\u0002H\u00010\u00022\u00020\u0003:\u0001NB\u0005¢\u0006\u0002\u0010\u0004J\u0015\u0010\u001a\u001a\u00020\u001b2\u0006\u0010\u001c\u001a\u00028\u0000H\u0016¢\u0006\u0002\u0010\u001dJ\u001d\u0010\u001a\u001a\u00020\u001e2\u0006\u0010\u001f\u001a\u00020\u00102\u0006\u0010\u001c\u001a\u00028\u0000H\u0016¢\u0006\u0002\u0010 J\u001e\u0010!\u001a\u00020\u001b2\u0006\u0010\u001f\u001a\u00020\u00102\f\u0010\"\u001a\b\u0012\u0004\u0012\u00028\u00000#H\u0016J\u0016\u0010!\u001a\u00020\u001b2\f\u0010\"\u001a\b\u0012\u0004\u0012\u00028\u00000#H\u0016J\b\u0010$\u001a\u00020\u001eH\u0016J)\u0010%\u001a\u00020\u001b2\u001e\u0010&\u001a\u001a\u0012\n\u0012\b\u0012\u0004\u0012\u00028\u00000(\u0012\n\u0012\b\u0012\u0004\u0012\u00028\u00000(0'H\u0082\bJ\u0016\u0010)\u001a\u00020\u001b2\u0006\u0010\u001c\u001a\u00028\u0000H\u0096\u0002¢\u0006\u0002\u0010\u001dJ\u0016\u0010*\u001a\u00020\u001b2\f\u0010\"\u001a\b\u0012\u0004\u0012\u00028\u00000#H\u0016J\u0016\u0010+\u001a\u00028\u00002\u0006\u0010\u001f\u001a\u00020\u0010H\u0096\u0002¢\u0006\u0002\u0010,J\u0015\u0010-\u001a\u00020\u00102\u0006\u0010\u001c\u001a\u00028\u0000H\u0016¢\u0006\u0002\u0010.J\b\u0010/\u001a\u00020\u001bH\u0016J\u000f\u00100\u001a\b\u0012\u0004\u0012\u00028\u000001H\u0096\u0002J\u0015\u00102\u001a\u00020\u00102\u0006\u0010\u001c\u001a\u00028\u0000H\u0016¢\u0006\u0002\u0010.J\u000e\u00103\u001a\b\u0012\u0004\u0012\u00028\u000004H\u0016J\u0016\u00103\u001a\b\u0012\u0004\u0012\u00028\u0000042\u0006\u0010\u001f\u001a\u00020\u0010H\u0016J.\u00105\u001a\u0002H6\"\u0004\b\u0001\u001062\u0018\u0010&\u001a\u0014\u0012\n\u0012\b\u0012\u0004\u0012\u00028\u00000\u0002\u0012\u0004\u0012\u0002H60'H\u0082\b¢\u0006\u0002\u00107J\"\u00108\u001a\u00020\u001b2\u0018\u0010&\u001a\u0014\u0012\n\u0012\b\u0012\u0004\u0012\u00028\u00000\u0002\u0012\u0004\u0012\u00020\u001b0'H\u0002J\u0010\u00109\u001a\u00020\u001e2\u0006\u0010:\u001a\u00020\u000bH\u0016J\u0015\u0010;\u001a\u00020\u001b2\u0006\u0010\u001c\u001a\u00028\u0000H\u0016¢\u0006\u0002\u0010\u001dJ\u0016\u0010<\u001a\u00020\u001b2\f\u0010\"\u001a\b\u0012\u0004\u0012\u00028\u00000#H\u0016J\u0015\u0010=\u001a\u00028\u00002\u0006\u0010\u001f\u001a\u00020\u0010H\u0016¢\u0006\u0002\u0010,J\u0016\u0010>\u001a\u00020\u001e2\u0006\u0010?\u001a\u00020\u00102\u0006\u0010@\u001a\u00020\u0010J\u0016\u0010A\u001a\u00020\u001b2\f\u0010\"\u001a\b\u0012\u0004\u0012\u00028\u00000#H\u0016J+\u0010B\u001a\u00020\u00102\f\u0010\"\u001a\b\u0012\u0004\u0012\u00028\u00000#2\u0006\u0010C\u001a\u00020\u00102\u0006\u0010D\u001a\u00020\u0010H\u0000¢\u0006\u0002\bEJ\u001e\u0010F\u001a\u00028\u00002\u0006\u0010\u001f\u001a\u00020\u00102\u0006\u0010\u001c\u001a\u00028\u0000H\u0096\u0002¢\u0006\u0002\u0010GJ\u001e\u0010H\u001a\b\u0012\u0004\u0012\u00028\u00000\u00022\u0006\u0010?\u001a\u00020\u00102\u0006\u0010@\u001a\u00020\u0010H\u0016J\f\u0010I\u001a\b\u0012\u0004\u0012\u00028\u00000\u0006J)\u0010J\u001a\u00020\u001e2\u001e\u0010&\u001a\u001a\u0012\n\u0012\b\u0012\u0004\u0012\u00028\u00000(\u0012\n\u0012\b\u0012\u0004\u0012\u00028\u00000(0'H\u0082\bJ3\u0010K\u001a\u0002H6\"\u0004\b\u0001\u001062\u001d\u0010&\u001a\u0019\u0012\n\u0012\b\u0012\u0004\u0012\u00028\u00000\u0014\u0012\u0004\u0012\u0002H60'¢\u0006\u0002\bLH\u0082\b¢\u0006\u0002\u00107J3\u0010M\u001a\u0002H6\"\u0004\b\u0001\u001062\u001d\u0010&\u001a\u0019\u0012\n\u0012\b\u0012\u0004\u0012\u00028\u00000\u0014\u0012\u0004\u0012\u0002H60'¢\u0006\u0002\bLH\u0082\b¢\u0006\u0002\u00107R \u0010\u0005\u001a\b\u0012\u0004\u0012\u00028\u00000\u00068AX\u0080\u0004¢\u0006\f\u0012\u0004\b\u0007\u0010\u0004\u001a\u0004\b\b\u0010\tR\u001e\u0010\f\u001a\u00020\u000b2\u0006\u0010\n\u001a\u00020\u000b@RX\u0096\u000e¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000eR\u0014\u0010\u000f\u001a\u00020\u00108@X\u0080\u0004¢\u0006\u0006\u001a\u0004\b\u0011\u0010\u0012R \u0010\u0013\u001a\b\u0012\u0004\u0012\u00028\u00000\u00148@X\u0080\u0004¢\u0006\f\u0012\u0004\b\u0015\u0010\u0004\u001a\u0004\b\u0016\u0010\u0017R\u0014\u0010\u0018\u001a\u00020\u00108VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u0019\u0010\u0012¨\u0006O" }, d2 = { "Landroidx/compose/runtime/snapshots/SnapshotStateList;", "T", "", "Landroidx/compose/runtime/snapshots/StateObject;", "()V", "debuggerDisplayValue", "", "getDebuggerDisplayValue$annotations", "getDebuggerDisplayValue", "()Ljava/util/List;", "<set-?>", "Landroidx/compose/runtime/snapshots/StateRecord;", "firstStateRecord", "getFirstStateRecord", "()Landroidx/compose/runtime/snapshots/StateRecord;", "modification", "", "getModification$runtime_release", "()I", "readable", "Landroidx/compose/runtime/snapshots/SnapshotStateList$StateListStateRecord;", "getReadable$runtime_release$annotations", "getReadable$runtime_release", "()Landroidx/compose/runtime/snapshots/SnapshotStateList$StateListStateRecord;", "size", "getSize", "add", "", "element", "(Ljava/lang/Object;)Z", "", "index", "(ILjava/lang/Object;)V", "addAll", "elements", "", "clear", "conditionalUpdate", "block", "Lkotlin/Function1;", "Landroidx/compose/runtime/external/kotlinx/collections/immutable/PersistentList;", "contains", "containsAll", "get", "(I)Ljava/lang/Object;", "indexOf", "(Ljava/lang/Object;)I", "isEmpty", "iterator", "", "lastIndexOf", "listIterator", "", "mutate", "R", "(Lkotlin/jvm/functions/Function1;)Ljava/lang/Object;", "mutateBoolean", "prependStateRecord", "value", "remove", "removeAll", "removeAt", "removeRange", "fromIndex", "toIndex", "retainAll", "retainAllInRange", "start", "end", "retainAllInRange$runtime_release", "set", "(ILjava/lang/Object;)Ljava/lang/Object;", "subList", "toList", "update", "withCurrent", "Lkotlin/ExtensionFunctionType;", "writable", "StateListStateRecord", "runtime_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
public final class SnapshotStateList<T> implements List<T>, StateObject, KMutableList
{
    public static final int $stable = 0;
    private StateRecord firstStateRecord;
    
    public SnapshotStateList() {
        this.firstStateRecord = new StateListStateRecord<Object>(ExtensionsKt.persistentListOf());
    }
    
    private final boolean conditionalUpdate(final Function1<? super PersistentList<? extends T>, ? extends PersistentList<? extends T>> function1) {
        final SnapshotStateList list = this;
        while (true) {
            Object o = SnapshotStateListKt.access$getSync$p();
            monitorenter(o);
            boolean b = true;
            try {
                final StateRecord firstStateRecord = this.getFirstStateRecord();
                Intrinsics.checkNotNull((Object)firstStateRecord, "null cannot be cast to non-null type androidx.compose.runtime.snapshots.SnapshotStateList.StateListStateRecord<T of androidx.compose.runtime.snapshots.SnapshotStateList>");
                final StateListStateRecord stateListStateRecord = SnapshotKt.current(firstStateRecord);
                final int modification$runtime_release = stateListStateRecord.getModification$runtime_release();
                Object o2 = stateListStateRecord.getList$runtime_release();
                final Unit instance = Unit.INSTANCE;
                InlineMarker.finallyStart(1);
                monitorexit(o);
                InlineMarker.finallyEnd(1);
                Intrinsics.checkNotNull(o2);
                final PersistentList list$runtime_release = (PersistentList)function1.invoke(o2);
                final boolean equal = Intrinsics.areEqual((Object)list$runtime_release, o2);
                boolean b2 = false;
                if (equal) {
                    b = false;
                    return b;
                }
                o = this.getFirstStateRecord();
                Intrinsics.checkNotNull(o, "null cannot be cast to non-null type androidx.compose.runtime.snapshots.SnapshotStateList.StateListStateRecord<T of androidx.compose.runtime.snapshots.SnapshotStateList>");
                final StateRecord stateRecord = (StateRecord)o;
                SnapshotKt.getSnapshotInitializer();
                o = SnapshotKt.getLock();
                monitorenter(o);
                try {
                    final Snapshot current = Snapshot.Companion.getCurrent();
                    final StateListStateRecord stateListStateRecord2 = SnapshotKt.writableRecord(stateRecord, this, current);
                    o2 = SnapshotStateListKt.access$getSync$p();
                    monitorenter(o2);
                    try {
                        if (stateListStateRecord2.getModification$runtime_release() == modification$runtime_release) {
                            stateListStateRecord2.setList$runtime_release(list$runtime_release);
                            stateListStateRecord2.setModification$runtime_release(stateListStateRecord2.getModification$runtime_release() + 1);
                            b2 = true;
                        }
                        InlineMarker.finallyStart(1);
                        monitorexit(o2);
                        InlineMarker.finallyEnd(1);
                        InlineMarker.finallyStart(1);
                        monitorexit(o);
                        InlineMarker.finallyEnd(1);
                        SnapshotKt.notifyWrite(current, this);
                        if (b2) {
                            return b;
                        }
                        continue;
                    }
                    finally {
                        InlineMarker.finallyStart(1);
                        monitorexit(o2);
                        InlineMarker.finallyEnd(1);
                    }
                }
                finally {
                    InlineMarker.finallyStart(1);
                    monitorexit(o);
                    InlineMarker.finallyEnd(1);
                }
            }
            finally {
                InlineMarker.finallyStart(1);
                monitorexit(o);
                InlineMarker.finallyEnd(1);
            }
        }
    }
    
    private final <R> R mutate(final Function1<? super List<T>, ? extends R> function1) {
        while (true) {
            Object o = SnapshotStateListKt.access$getSync$p();
            monitorenter(o);
            try {
                final StateRecord firstStateRecord = this.getFirstStateRecord();
                Intrinsics.checkNotNull((Object)firstStateRecord, "null cannot be cast to non-null type androidx.compose.runtime.snapshots.SnapshotStateList.StateListStateRecord<T of androidx.compose.runtime.snapshots.SnapshotStateList>");
                final StateListStateRecord stateListStateRecord = SnapshotKt.current(firstStateRecord);
                final int modification$runtime_release = stateListStateRecord.getModification$runtime_release();
                final PersistentList list$runtime_release = stateListStateRecord.getList$runtime_release();
                final Unit instance = Unit.INSTANCE;
                InlineMarker.finallyStart(1);
                monitorexit(o);
                InlineMarker.finallyEnd(1);
                Intrinsics.checkNotNull((Object)list$runtime_release);
                final PersistentList.Builder builder = list$runtime_release.builder();
                o = function1.invoke((Object)builder);
                final PersistentList build = builder.build();
                if (!Intrinsics.areEqual((Object)build, (Object)list$runtime_release)) {
                    final StateRecord firstStateRecord2 = this.getFirstStateRecord();
                    Intrinsics.checkNotNull((Object)firstStateRecord2, "null cannot be cast to non-null type androidx.compose.runtime.snapshots.SnapshotStateList.StateListStateRecord<T of androidx.compose.runtime.snapshots.SnapshotStateList>");
                    final StateRecord stateRecord = firstStateRecord2;
                    SnapshotKt.getSnapshotInitializer();
                    final Object lock = SnapshotKt.getLock();
                    monitorenter(lock);
                    try {
                        final Snapshot current = Snapshot.Companion.getCurrent();
                        final StateListStateRecord stateListStateRecord2 = SnapshotKt.writableRecord(stateRecord, this, current);
                        final Object access$getSync$p = SnapshotStateListKt.access$getSync$p();
                        monitorenter(access$getSync$p);
                        try {
                            boolean b;
                            if (stateListStateRecord2.getModification$runtime_release() == modification$runtime_release) {
                                stateListStateRecord2.setList$runtime_release(build);
                                stateListStateRecord2.setModification$runtime_release(stateListStateRecord2.getModification$runtime_release() + 1);
                                b = true;
                            }
                            else {
                                b = false;
                            }
                            InlineMarker.finallyStart(1);
                            monitorexit(access$getSync$p);
                            InlineMarker.finallyEnd(1);
                            InlineMarker.finallyStart(1);
                            monitorexit(lock);
                            InlineMarker.finallyEnd(1);
                            SnapshotKt.notifyWrite(current, this);
                            if (b) {
                                return (R)o;
                            }
                            continue;
                        }
                        finally {
                            InlineMarker.finallyStart(1);
                            monitorexit(access$getSync$p);
                            InlineMarker.finallyEnd(1);
                        }
                    }
                    finally {
                        InlineMarker.finallyStart(1);
                        monitorexit(lock);
                        InlineMarker.finallyEnd(1);
                    }
                }
                return (R)o;
            }
            finally {
                InlineMarker.finallyStart(1);
                monitorexit(o);
                InlineMarker.finallyEnd(1);
            }
        }
    }
    
    private final boolean mutateBoolean(final Function1<? super List<T>, Boolean> function1) {
        while (true) {
            Object o = SnapshotStateListKt.access$getSync$p();
            synchronized (o) {
                final StateRecord firstStateRecord = this.getFirstStateRecord();
                Intrinsics.checkNotNull((Object)firstStateRecord, "null cannot be cast to non-null type androidx.compose.runtime.snapshots.SnapshotStateList.StateListStateRecord<T of androidx.compose.runtime.snapshots.SnapshotStateList>");
                final StateListStateRecord stateListStateRecord = SnapshotKt.current(firstStateRecord);
                final int modification$runtime_release = stateListStateRecord.getModification$runtime_release();
                final PersistentList list$runtime_release = stateListStateRecord.getList$runtime_release();
                final Unit instance = Unit.INSTANCE;
                monitorexit(o);
                Intrinsics.checkNotNull((Object)list$runtime_release);
                final PersistentList.Builder builder = list$runtime_release.builder();
                o = function1.invoke((Object)builder);
                final PersistentList build = builder.build();
                if (!Intrinsics.areEqual((Object)build, (Object)list$runtime_release)) {
                    final StateRecord firstStateRecord2 = this.getFirstStateRecord();
                    Intrinsics.checkNotNull((Object)firstStateRecord2, "null cannot be cast to non-null type androidx.compose.runtime.snapshots.SnapshotStateList.StateListStateRecord<T of androidx.compose.runtime.snapshots.SnapshotStateList>");
                    final StateRecord stateRecord = firstStateRecord2;
                    SnapshotKt.getSnapshotInitializer();
                    synchronized (SnapshotKt.getLock()) {
                        final Snapshot current = Snapshot.Companion.getCurrent();
                        final StateListStateRecord stateListStateRecord2 = SnapshotKt.writableRecord(stateRecord, this, current);
                        synchronized (SnapshotStateListKt.access$getSync$p()) {
                            final int modification$runtime_release2 = stateListStateRecord2.getModification$runtime_release();
                            boolean b = true;
                            if (modification$runtime_release2 == modification$runtime_release) {
                                stateListStateRecord2.setList$runtime_release(build);
                                stateListStateRecord2.setModification$runtime_release(stateListStateRecord2.getModification$runtime_release() + 1);
                            }
                            else {
                                b = false;
                            }
                            monitorexit(SnapshotStateListKt.access$getSync$p());
                            monitorexit(SnapshotKt.getLock());
                            SnapshotKt.notifyWrite(current, this);
                            if (b) {
                                return (boolean)o;
                            }
                            continue;
                        }
                    }
                }
                return (boolean)o;
            }
        }
    }
    
    private final void update(final Function1<? super PersistentList<? extends T>, ? extends PersistentList<? extends T>> function1) {
        final SnapshotStateList list = this;
        while (true) {
            Object o = SnapshotStateListKt.access$getSync$p();
            monitorenter(o);
            try {
                final StateRecord firstStateRecord = this.getFirstStateRecord();
                Intrinsics.checkNotNull((Object)firstStateRecord, "null cannot be cast to non-null type androidx.compose.runtime.snapshots.SnapshotStateList.StateListStateRecord<T of androidx.compose.runtime.snapshots.SnapshotStateList>");
                final StateListStateRecord stateListStateRecord = SnapshotKt.current(firstStateRecord);
                final int modification$runtime_release = stateListStateRecord.getModification$runtime_release();
                final PersistentList list$runtime_release = stateListStateRecord.getList$runtime_release();
                final Unit instance = Unit.INSTANCE;
                InlineMarker.finallyStart(1);
                monitorexit(o);
                InlineMarker.finallyEnd(1);
                Intrinsics.checkNotNull((Object)list$runtime_release);
                final PersistentList list$runtime_release2 = (PersistentList)function1.invoke((Object)list$runtime_release);
                if (Intrinsics.areEqual((Object)list$runtime_release2, (Object)list$runtime_release)) {
                    return;
                }
                o = this.getFirstStateRecord();
                Intrinsics.checkNotNull(o, "null cannot be cast to non-null type androidx.compose.runtime.snapshots.SnapshotStateList.StateListStateRecord<T of androidx.compose.runtime.snapshots.SnapshotStateList>");
                Object access$getSync$p = o;
                SnapshotKt.getSnapshotInitializer();
                o = SnapshotKt.getLock();
                monitorenter(o);
                try {
                    final Snapshot current = Snapshot.Companion.getCurrent();
                    final StateListStateRecord stateListStateRecord2 = SnapshotKt.writableRecord(access$getSync$p, this, current);
                    access$getSync$p = SnapshotStateListKt.access$getSync$p();
                    monitorenter(access$getSync$p);
                    try {
                        boolean b;
                        if (stateListStateRecord2.getModification$runtime_release() == modification$runtime_release) {
                            stateListStateRecord2.setList$runtime_release(list$runtime_release2);
                            stateListStateRecord2.setModification$runtime_release(stateListStateRecord2.getModification$runtime_release() + 1);
                            b = true;
                        }
                        else {
                            b = false;
                        }
                        InlineMarker.finallyStart(1);
                        monitorexit(access$getSync$p);
                        InlineMarker.finallyEnd(1);
                        InlineMarker.finallyStart(1);
                        monitorexit(o);
                        InlineMarker.finallyEnd(1);
                        SnapshotKt.notifyWrite(current, this);
                        if (b) {
                            return;
                        }
                        continue;
                    }
                    finally {
                        InlineMarker.finallyStart(1);
                        monitorexit(access$getSync$p);
                        InlineMarker.finallyEnd(1);
                    }
                }
                finally {
                    InlineMarker.finallyStart(1);
                    monitorexit(o);
                    InlineMarker.finallyEnd(1);
                }
            }
            finally {
                InlineMarker.finallyStart(1);
                monitorexit(o);
                InlineMarker.finallyEnd(1);
            }
        }
    }
    
    private final <R> R withCurrent(final Function1<? super StateListStateRecord<T>, ? extends R> function1) {
        final StateRecord firstStateRecord = this.getFirstStateRecord();
        Intrinsics.checkNotNull((Object)firstStateRecord, "null cannot be cast to non-null type androidx.compose.runtime.snapshots.SnapshotStateList.StateListStateRecord<T of androidx.compose.runtime.snapshots.SnapshotStateList>");
        return (R)function1.invoke((Object)SnapshotKt.current(firstStateRecord));
    }
    
    private final <R> R writable(final Function1<? super StateListStateRecord<T>, ? extends R> function1) {
        final StateRecord firstStateRecord = this.getFirstStateRecord();
        Intrinsics.checkNotNull((Object)firstStateRecord, "null cannot be cast to non-null type androidx.compose.runtime.snapshots.SnapshotStateList.StateListStateRecord<T of androidx.compose.runtime.snapshots.SnapshotStateList>");
        final StateRecord stateRecord = firstStateRecord;
        SnapshotKt.getSnapshotInitializer();
        final Object lock = SnapshotKt.getLock();
        monitorenter(lock);
        try {
            final Snapshot current = Snapshot.Companion.getCurrent();
            final Object invoke = function1.invoke((Object)SnapshotKt.writableRecord(stateRecord, this, current));
            InlineMarker.finallyStart(1);
            monitorexit(lock);
            InlineMarker.finallyEnd(1);
            SnapshotKt.notifyWrite(current, this);
            return (R)invoke;
        }
        finally {
            InlineMarker.finallyStart(1);
            monitorexit(lock);
            InlineMarker.finallyEnd(1);
        }
    }
    
    @Override
    public void add(final int n, final T t) {
        final SnapshotStateList list = this;
        while (true) {
            Object o = SnapshotStateListKt.access$getSync$p();
            synchronized (o) {
                final StateRecord firstStateRecord = this.getFirstStateRecord();
                Intrinsics.checkNotNull((Object)firstStateRecord, "null cannot be cast to non-null type androidx.compose.runtime.snapshots.SnapshotStateList.StateListStateRecord<T of androidx.compose.runtime.snapshots.SnapshotStateList>");
                final StateListStateRecord stateListStateRecord = SnapshotKt.current(firstStateRecord);
                final int modification$runtime_release = stateListStateRecord.getModification$runtime_release();
                final PersistentList list$runtime_release = stateListStateRecord.getList$runtime_release();
                final Unit instance = Unit.INSTANCE;
                monitorexit(o);
                Intrinsics.checkNotNull((Object)list$runtime_release);
                final PersistentList add = list$runtime_release.add(n, (Object)t);
                if (Intrinsics.areEqual((Object)add, (Object)list$runtime_release)) {
                    return;
                }
                o = this.getFirstStateRecord();
                Intrinsics.checkNotNull(o, "null cannot be cast to non-null type androidx.compose.runtime.snapshots.SnapshotStateList.StateListStateRecord<T of androidx.compose.runtime.snapshots.SnapshotStateList>");
                final StateRecord stateRecord = (StateRecord)o;
                SnapshotKt.getSnapshotInitializer();
                synchronized (SnapshotKt.getLock()) {
                    final Snapshot current = Snapshot.Companion.getCurrent();
                    final StateListStateRecord stateListStateRecord2 = SnapshotKt.writableRecord(stateRecord, this, current);
                    synchronized (SnapshotStateListKt.access$getSync$p()) {
                        final int modification$runtime_release2 = stateListStateRecord2.getModification$runtime_release();
                        boolean b = true;
                        if (modification$runtime_release2 == modification$runtime_release) {
                            stateListStateRecord2.setList$runtime_release(add);
                            stateListStateRecord2.setModification$runtime_release(stateListStateRecord2.getModification$runtime_release() + 1);
                        }
                        else {
                            b = false;
                        }
                        monitorexit(SnapshotStateListKt.access$getSync$p());
                        monitorexit(SnapshotKt.getLock());
                        SnapshotKt.notifyWrite(current, this);
                        if (b) {
                            return;
                        }
                        continue;
                    }
                }
            }
        }
    }
    
    @Override
    public boolean add(final T t) {
        final SnapshotStateList list = this;
        while (true) {
            Object o = SnapshotStateListKt.access$getSync$p();
            synchronized (o) {
                final StateRecord firstStateRecord = this.getFirstStateRecord();
                Intrinsics.checkNotNull((Object)firstStateRecord, "null cannot be cast to non-null type androidx.compose.runtime.snapshots.SnapshotStateList.StateListStateRecord<T of androidx.compose.runtime.snapshots.SnapshotStateList>");
                final StateListStateRecord stateListStateRecord = SnapshotKt.current(firstStateRecord);
                final int modification$runtime_release = stateListStateRecord.getModification$runtime_release();
                final PersistentList list$runtime_release = stateListStateRecord.getList$runtime_release();
                final Unit instance = Unit.INSTANCE;
                monitorexit(o);
                Intrinsics.checkNotNull((Object)list$runtime_release);
                final PersistentList add = list$runtime_release.add((Object)t);
                final boolean equal = Intrinsics.areEqual((Object)add, (Object)list$runtime_release);
                boolean b = false;
                boolean b2 = false;
                if (equal) {
                    return b;
                }
                o = this.getFirstStateRecord();
                Intrinsics.checkNotNull(o, "null cannot be cast to non-null type androidx.compose.runtime.snapshots.SnapshotStateList.StateListStateRecord<T of androidx.compose.runtime.snapshots.SnapshotStateList>");
                final StateRecord stateRecord = (StateRecord)o;
                SnapshotKt.getSnapshotInitializer();
                synchronized (SnapshotKt.getLock()) {
                    final Snapshot current = Snapshot.Companion.getCurrent();
                    final StateListStateRecord stateListStateRecord2 = SnapshotKt.writableRecord(stateRecord, this, current);
                    synchronized (SnapshotStateListKt.access$getSync$p()) {
                        if (stateListStateRecord2.getModification$runtime_release() == modification$runtime_release) {
                            stateListStateRecord2.setList$runtime_release(add);
                            stateListStateRecord2.setModification$runtime_release(stateListStateRecord2.getModification$runtime_release() + 1);
                            b2 = true;
                        }
                        monitorexit(SnapshotStateListKt.access$getSync$p());
                        monitorexit(SnapshotKt.getLock());
                        SnapshotKt.notifyWrite(current, this);
                        if (b2) {
                            b = true;
                            return b;
                        }
                        continue;
                    }
                }
            }
        }
    }
    
    @Override
    public boolean addAll(final int n, final Collection<? extends T> collection) {
        Intrinsics.checkNotNullParameter((Object)collection, "elements");
        return this.mutateBoolean((Function1<? super List<T>, Boolean>)new SnapshotStateList$addAll.SnapshotStateList$addAll$1(n, (Collection)collection));
    }
    
    @Override
    public boolean addAll(final Collection<? extends T> collection) {
        Intrinsics.checkNotNullParameter((Object)collection, "elements");
        final SnapshotStateList list = this;
        while (true) {
            Object list$runtime_release = SnapshotStateListKt.access$getSync$p();
            synchronized (list$runtime_release) {
                final StateRecord firstStateRecord = this.getFirstStateRecord();
                Intrinsics.checkNotNull((Object)firstStateRecord, "null cannot be cast to non-null type androidx.compose.runtime.snapshots.SnapshotStateList.StateListStateRecord<T of androidx.compose.runtime.snapshots.SnapshotStateList>");
                final StateListStateRecord stateListStateRecord = SnapshotKt.current(firstStateRecord);
                final int modification$runtime_release = stateListStateRecord.getModification$runtime_release();
                final PersistentList list$runtime_release2 = stateListStateRecord.getList$runtime_release();
                final Unit instance = Unit.INSTANCE;
                monitorexit(list$runtime_release);
                Intrinsics.checkNotNull((Object)list$runtime_release2);
                list$runtime_release = list$runtime_release2.addAll((Collection)collection);
                final boolean equal = Intrinsics.areEqual(list$runtime_release, (Object)list$runtime_release2);
                boolean b = false;
                boolean b2 = false;
                if (equal) {
                    return b;
                }
                final StateRecord firstStateRecord2 = this.getFirstStateRecord();
                Intrinsics.checkNotNull((Object)firstStateRecord2, "null cannot be cast to non-null type androidx.compose.runtime.snapshots.SnapshotStateList.StateListStateRecord<T of androidx.compose.runtime.snapshots.SnapshotStateList>");
                final StateRecord stateRecord = firstStateRecord2;
                SnapshotKt.getSnapshotInitializer();
                synchronized (SnapshotKt.getLock()) {
                    final Snapshot current = Snapshot.Companion.getCurrent();
                    final StateListStateRecord stateListStateRecord2 = SnapshotKt.writableRecord(stateRecord, this, current);
                    synchronized (SnapshotStateListKt.access$getSync$p()) {
                        if (stateListStateRecord2.getModification$runtime_release() == modification$runtime_release) {
                            stateListStateRecord2.setList$runtime_release((PersistentList)list$runtime_release);
                            stateListStateRecord2.setModification$runtime_release(stateListStateRecord2.getModification$runtime_release() + 1);
                            b2 = true;
                        }
                        monitorexit(SnapshotStateListKt.access$getSync$p());
                        monitorexit(SnapshotKt.getLock());
                        SnapshotKt.notifyWrite(current, this);
                        if (b2) {
                            b = true;
                            return b;
                        }
                        continue;
                    }
                }
            }
        }
    }
    
    @Override
    public void clear() {
        final StateRecord firstStateRecord = this.getFirstStateRecord();
        Intrinsics.checkNotNull((Object)firstStateRecord, "null cannot be cast to non-null type androidx.compose.runtime.snapshots.SnapshotStateList.StateListStateRecord<T of androidx.compose.runtime.snapshots.SnapshotStateList>");
        final StateRecord stateRecord = firstStateRecord;
        SnapshotKt.getSnapshotInitializer();
        synchronized (SnapshotKt.getLock()) {
            final Snapshot current = Snapshot.Companion.getCurrent();
            final StateListStateRecord stateListStateRecord = SnapshotKt.writableRecord(stateRecord, this, current);
            synchronized (SnapshotStateListKt.access$getSync$p()) {
                stateListStateRecord.setList$runtime_release(ExtensionsKt.persistentListOf());
                stateListStateRecord.setModification$runtime_release(stateListStateRecord.getModification$runtime_release() + 1);
                monitorexit(SnapshotStateListKt.access$getSync$p());
                monitorexit(SnapshotKt.getLock());
                SnapshotKt.notifyWrite(current, this);
            }
        }
    }
    
    @Override
    public boolean contains(final Object o) {
        return this.getReadable$runtime_release().getList$runtime_release().contains(o);
    }
    
    @Override
    public boolean containsAll(final Collection<?> collection) {
        Intrinsics.checkNotNullParameter((Object)collection, "elements");
        return this.getReadable$runtime_release().getList$runtime_release().containsAll(collection);
    }
    
    @Override
    public T get(final int n) {
        return this.getReadable$runtime_release().getList$runtime_release().get(n);
    }
    
    public final List<T> getDebuggerDisplayValue() {
        final StateRecord firstStateRecord = this.getFirstStateRecord();
        Intrinsics.checkNotNull((Object)firstStateRecord, "null cannot be cast to non-null type androidx.compose.runtime.snapshots.SnapshotStateList.StateListStateRecord<T of androidx.compose.runtime.snapshots.SnapshotStateList>");
        return SnapshotKt.current(firstStateRecord).getList$runtime_release();
    }
    
    @Override
    public StateRecord getFirstStateRecord() {
        return this.firstStateRecord;
    }
    
    public final int getModification$runtime_release() {
        final StateRecord firstStateRecord = this.getFirstStateRecord();
        Intrinsics.checkNotNull((Object)firstStateRecord, "null cannot be cast to non-null type androidx.compose.runtime.snapshots.SnapshotStateList.StateListStateRecord<T of androidx.compose.runtime.snapshots.SnapshotStateList>");
        return SnapshotKt.current(firstStateRecord).getModification$runtime_release();
    }
    
    public final StateListStateRecord<T> getReadable$runtime_release() {
        final StateRecord firstStateRecord = this.getFirstStateRecord();
        Intrinsics.checkNotNull((Object)firstStateRecord, "null cannot be cast to non-null type androidx.compose.runtime.snapshots.SnapshotStateList.StateListStateRecord<T of androidx.compose.runtime.snapshots.SnapshotStateList>");
        return SnapshotKt.readable(firstStateRecord, this);
    }
    
    public int getSize() {
        return this.getReadable$runtime_release().getList$runtime_release().size();
    }
    
    @Override
    public int indexOf(final Object o) {
        return this.getReadable$runtime_release().getList$runtime_release().indexOf(o);
    }
    
    @Override
    public boolean isEmpty() {
        return this.getReadable$runtime_release().getList$runtime_release().isEmpty();
    }
    
    @Override
    public Iterator<T> iterator() {
        return this.listIterator();
    }
    
    @Override
    public int lastIndexOf(final Object o) {
        return this.getReadable$runtime_release().getList$runtime_release().lastIndexOf(o);
    }
    
    @Override
    public ListIterator<T> listIterator() {
        return new StateListIterator<T>(this, 0);
    }
    
    @Override
    public ListIterator<T> listIterator(final int n) {
        return new StateListIterator<T>(this, n);
    }
    
    @Override
    public void prependStateRecord(final StateRecord stateRecord) {
        Intrinsics.checkNotNullParameter((Object)stateRecord, "value");
        stateRecord.setNext$runtime_release(this.getFirstStateRecord());
        this.firstStateRecord = stateRecord;
    }
    
    @Override
    public final /* bridge */ T remove(final int n) {
        return this.removeAt(n);
    }
    
    @Override
    public boolean remove(final Object o) {
        final SnapshotStateList list = this;
        while (true) {
            Object list$runtime_release = SnapshotStateListKt.access$getSync$p();
            synchronized (list$runtime_release) {
                final StateRecord firstStateRecord = this.getFirstStateRecord();
                Intrinsics.checkNotNull((Object)firstStateRecord, "null cannot be cast to non-null type androidx.compose.runtime.snapshots.SnapshotStateList.StateListStateRecord<T of androidx.compose.runtime.snapshots.SnapshotStateList>");
                final StateListStateRecord stateListStateRecord = SnapshotKt.current(firstStateRecord);
                final int modification$runtime_release = stateListStateRecord.getModification$runtime_release();
                final PersistentList list$runtime_release2 = stateListStateRecord.getList$runtime_release();
                final Unit instance = Unit.INSTANCE;
                monitorexit(list$runtime_release);
                Intrinsics.checkNotNull((Object)list$runtime_release2);
                list$runtime_release = list$runtime_release2.remove(o);
                final boolean equal = Intrinsics.areEqual(list$runtime_release, (Object)list$runtime_release2);
                boolean b = false;
                boolean b2 = false;
                if (equal) {
                    return b;
                }
                final StateRecord firstStateRecord2 = this.getFirstStateRecord();
                Intrinsics.checkNotNull((Object)firstStateRecord2, "null cannot be cast to non-null type androidx.compose.runtime.snapshots.SnapshotStateList.StateListStateRecord<T of androidx.compose.runtime.snapshots.SnapshotStateList>");
                final StateRecord stateRecord = firstStateRecord2;
                SnapshotKt.getSnapshotInitializer();
                synchronized (SnapshotKt.getLock()) {
                    final Snapshot current = Snapshot.Companion.getCurrent();
                    final StateListStateRecord stateListStateRecord2 = SnapshotKt.writableRecord(stateRecord, this, current);
                    synchronized (SnapshotStateListKt.access$getSync$p()) {
                        if (stateListStateRecord2.getModification$runtime_release() == modification$runtime_release) {
                            stateListStateRecord2.setList$runtime_release((PersistentList)list$runtime_release);
                            stateListStateRecord2.setModification$runtime_release(stateListStateRecord2.getModification$runtime_release() + 1);
                            b2 = true;
                        }
                        monitorexit(SnapshotStateListKt.access$getSync$p());
                        monitorexit(SnapshotKt.getLock());
                        SnapshotKt.notifyWrite(current, this);
                        if (b2) {
                            b = true;
                            return b;
                        }
                        continue;
                    }
                }
            }
        }
    }
    
    @Override
    public boolean removeAll(final Collection<?> collection) {
        Intrinsics.checkNotNullParameter((Object)collection, "elements");
        final SnapshotStateList list = this;
        while (true) {
            Object list$runtime_release = SnapshotStateListKt.access$getSync$p();
            synchronized (list$runtime_release) {
                final StateRecord firstStateRecord = this.getFirstStateRecord();
                Intrinsics.checkNotNull((Object)firstStateRecord, "null cannot be cast to non-null type androidx.compose.runtime.snapshots.SnapshotStateList.StateListStateRecord<T of androidx.compose.runtime.snapshots.SnapshotStateList>");
                final StateListStateRecord stateListStateRecord = SnapshotKt.current(firstStateRecord);
                final int modification$runtime_release = stateListStateRecord.getModification$runtime_release();
                final PersistentList list$runtime_release2 = stateListStateRecord.getList$runtime_release();
                final Unit instance = Unit.INSTANCE;
                monitorexit(list$runtime_release);
                Intrinsics.checkNotNull((Object)list$runtime_release2);
                list$runtime_release = list$runtime_release2.removeAll((Collection)collection);
                final boolean equal = Intrinsics.areEqual(list$runtime_release, (Object)list$runtime_release2);
                boolean b = false;
                boolean b2 = false;
                if (equal) {
                    return b;
                }
                final StateRecord firstStateRecord2 = this.getFirstStateRecord();
                Intrinsics.checkNotNull((Object)firstStateRecord2, "null cannot be cast to non-null type androidx.compose.runtime.snapshots.SnapshotStateList.StateListStateRecord<T of androidx.compose.runtime.snapshots.SnapshotStateList>");
                final StateRecord stateRecord = firstStateRecord2;
                SnapshotKt.getSnapshotInitializer();
                synchronized (SnapshotKt.getLock()) {
                    final Snapshot current = Snapshot.Companion.getCurrent();
                    final StateListStateRecord stateListStateRecord2 = SnapshotKt.writableRecord(stateRecord, this, current);
                    synchronized (SnapshotStateListKt.access$getSync$p()) {
                        if (stateListStateRecord2.getModification$runtime_release() == modification$runtime_release) {
                            stateListStateRecord2.setList$runtime_release((PersistentList)list$runtime_release);
                            stateListStateRecord2.setModification$runtime_release(stateListStateRecord2.getModification$runtime_release() + 1);
                            b2 = true;
                        }
                        monitorexit(SnapshotStateListKt.access$getSync$p());
                        monitorexit(SnapshotKt.getLock());
                        SnapshotKt.notifyWrite(current, this);
                        if (b2) {
                            b = true;
                            return b;
                        }
                        continue;
                    }
                }
            }
        }
    }
    
    public T removeAt(final int n) {
        final T value = this.get(n);
        final SnapshotStateList list = this;
        while (true) {
            Object list$runtime_release = SnapshotStateListKt.access$getSync$p();
            synchronized (list$runtime_release) {
                final StateRecord firstStateRecord = this.getFirstStateRecord();
                Intrinsics.checkNotNull((Object)firstStateRecord, "null cannot be cast to non-null type androidx.compose.runtime.snapshots.SnapshotStateList.StateListStateRecord<T of androidx.compose.runtime.snapshots.SnapshotStateList>");
                final StateListStateRecord stateListStateRecord = SnapshotKt.current(firstStateRecord);
                final int modification$runtime_release = stateListStateRecord.getModification$runtime_release();
                final PersistentList list$runtime_release2 = stateListStateRecord.getList$runtime_release();
                final Unit instance = Unit.INSTANCE;
                monitorexit(list$runtime_release);
                Intrinsics.checkNotNull((Object)list$runtime_release2);
                list$runtime_release = list$runtime_release2.removeAt(n);
                if (Intrinsics.areEqual(list$runtime_release, (Object)list$runtime_release2)) {
                    return value;
                }
                final StateRecord firstStateRecord2 = this.getFirstStateRecord();
                Intrinsics.checkNotNull((Object)firstStateRecord2, "null cannot be cast to non-null type androidx.compose.runtime.snapshots.SnapshotStateList.StateListStateRecord<T of androidx.compose.runtime.snapshots.SnapshotStateList>");
                final StateRecord stateRecord = firstStateRecord2;
                SnapshotKt.getSnapshotInitializer();
                synchronized (SnapshotKt.getLock()) {
                    final Snapshot current = Snapshot.Companion.getCurrent();
                    final StateListStateRecord stateListStateRecord2 = SnapshotKt.writableRecord(stateRecord, this, current);
                    synchronized (SnapshotStateListKt.access$getSync$p()) {
                        final int modification$runtime_release2 = stateListStateRecord2.getModification$runtime_release();
                        boolean b = true;
                        if (modification$runtime_release2 == modification$runtime_release) {
                            stateListStateRecord2.setList$runtime_release((PersistentList)list$runtime_release);
                            stateListStateRecord2.setModification$runtime_release(stateListStateRecord2.getModification$runtime_release() + 1);
                        }
                        else {
                            b = false;
                        }
                        monitorexit(SnapshotStateListKt.access$getSync$p());
                        monitorexit(SnapshotKt.getLock());
                        SnapshotKt.notifyWrite(current, this);
                        if (b) {
                            return value;
                        }
                        continue;
                    }
                }
            }
        }
    }
    
    public final void removeRange(final int n, final int n2) {
        while (true) {
            Object list$runtime_release = SnapshotStateListKt.access$getSync$p();
            synchronized (list$runtime_release) {
                final StateRecord firstStateRecord = this.getFirstStateRecord();
                Intrinsics.checkNotNull((Object)firstStateRecord, "null cannot be cast to non-null type androidx.compose.runtime.snapshots.SnapshotStateList.StateListStateRecord<T of androidx.compose.runtime.snapshots.SnapshotStateList>");
                final StateListStateRecord stateListStateRecord = SnapshotKt.current(firstStateRecord);
                final int modification$runtime_release = stateListStateRecord.getModification$runtime_release();
                final PersistentList list$runtime_release2 = stateListStateRecord.getList$runtime_release();
                final Unit instance = Unit.INSTANCE;
                monitorexit(list$runtime_release);
                Intrinsics.checkNotNull((Object)list$runtime_release2);
                final PersistentList.Builder builder = list$runtime_release2.builder();
                builder.subList(n, n2).clear();
                list$runtime_release = Unit.INSTANCE;
                list$runtime_release = builder.build();
                if (!Intrinsics.areEqual(list$runtime_release, (Object)list$runtime_release2)) {
                    final StateRecord firstStateRecord2 = this.getFirstStateRecord();
                    Intrinsics.checkNotNull((Object)firstStateRecord2, "null cannot be cast to non-null type androidx.compose.runtime.snapshots.SnapshotStateList.StateListStateRecord<T of androidx.compose.runtime.snapshots.SnapshotStateList>");
                    final StateRecord stateRecord = firstStateRecord2;
                    SnapshotKt.getSnapshotInitializer();
                    final Object lock = SnapshotKt.getLock();
                    monitorenter(lock);
                    try {
                        final Snapshot current = Snapshot.Companion.getCurrent();
                        final StateListStateRecord stateListStateRecord2 = SnapshotKt.writableRecord(stateRecord, this, current);
                        synchronized (SnapshotStateListKt.access$getSync$p()) {
                            final int modification$runtime_release2 = stateListStateRecord2.getModification$runtime_release();
                            boolean b = true;
                            if (modification$runtime_release2 == modification$runtime_release) {
                                stateListStateRecord2.setList$runtime_release((PersistentList)list$runtime_release);
                                stateListStateRecord2.setModification$runtime_release(stateListStateRecord2.getModification$runtime_release() + 1);
                            }
                            else {
                                b = false;
                            }
                            monitorexit(SnapshotStateListKt.access$getSync$p());
                            monitorexit(lock);
                            SnapshotKt.notifyWrite(current, this);
                            if (b) {
                                return;
                            }
                            continue;
                        }
                    }
                    finally {}
                }
            }
        }
    }
    
    @Override
    public boolean retainAll(final Collection<?> collection) {
        Intrinsics.checkNotNullParameter((Object)collection, "elements");
        return this.mutateBoolean((Function1<? super List<T>, Boolean>)new SnapshotStateList$retainAll.SnapshotStateList$retainAll$1((Collection)collection));
    }
    
    public final int retainAllInRange$runtime_release(final Collection<? extends T> collection, final int n, final int n2) {
        Intrinsics.checkNotNullParameter((Object)collection, "elements");
        final int size = this.size();
        while (true) {
            Object o = SnapshotStateListKt.access$getSync$p();
            synchronized (o) {
                final StateRecord firstStateRecord = this.getFirstStateRecord();
                Intrinsics.checkNotNull((Object)firstStateRecord, "null cannot be cast to non-null type androidx.compose.runtime.snapshots.SnapshotStateList.StateListStateRecord<T of androidx.compose.runtime.snapshots.SnapshotStateList>");
                final StateListStateRecord stateListStateRecord = SnapshotKt.current(firstStateRecord);
                final int modification$runtime_release = stateListStateRecord.getModification$runtime_release();
                final PersistentList list$runtime_release = stateListStateRecord.getList$runtime_release();
                final Unit instance = Unit.INSTANCE;
                monitorexit(o);
                Intrinsics.checkNotNull((Object)list$runtime_release);
                final PersistentList.Builder builder = list$runtime_release.builder();
                builder.subList(n, n2).retainAll(collection);
                o = Unit.INSTANCE;
                final PersistentList build = builder.build();
                if (!Intrinsics.areEqual((Object)build, (Object)list$runtime_release)) {
                    o = this.getFirstStateRecord();
                    Intrinsics.checkNotNull(o, "null cannot be cast to non-null type androidx.compose.runtime.snapshots.SnapshotStateList.StateListStateRecord<T of androidx.compose.runtime.snapshots.SnapshotStateList>");
                    final StateRecord stateRecord = (StateRecord)o;
                    SnapshotKt.getSnapshotInitializer();
                    synchronized (SnapshotKt.getLock()) {
                        final Snapshot current = Snapshot.Companion.getCurrent();
                        final StateListStateRecord stateListStateRecord2 = SnapshotKt.writableRecord(stateRecord, this, current);
                        synchronized (SnapshotStateListKt.access$getSync$p()) {
                            final int modification$runtime_release2 = stateListStateRecord2.getModification$runtime_release();
                            boolean b = true;
                            if (modification$runtime_release2 == modification$runtime_release) {
                                stateListStateRecord2.setList$runtime_release(build);
                                stateListStateRecord2.setModification$runtime_release(stateListStateRecord2.getModification$runtime_release() + 1);
                            }
                            else {
                                b = false;
                            }
                            monitorexit(SnapshotStateListKt.access$getSync$p());
                            monitorexit(SnapshotKt.getLock());
                            SnapshotKt.notifyWrite(current, this);
                            if (b) {
                                return size - this.size();
                            }
                            continue;
                        }
                    }
                }
                return size - this.size();
            }
        }
    }
    
    @Override
    public T set(final int n, final T t) {
        final T value = this.get(n);
        final SnapshotStateList list = this;
        while (true) {
            Object list$runtime_release = SnapshotStateListKt.access$getSync$p();
            synchronized (list$runtime_release) {
                final StateRecord firstStateRecord = this.getFirstStateRecord();
                Intrinsics.checkNotNull((Object)firstStateRecord, "null cannot be cast to non-null type androidx.compose.runtime.snapshots.SnapshotStateList.StateListStateRecord<T of androidx.compose.runtime.snapshots.SnapshotStateList>");
                final StateListStateRecord stateListStateRecord = SnapshotKt.current(firstStateRecord);
                final int modification$runtime_release = stateListStateRecord.getModification$runtime_release();
                final PersistentList list$runtime_release2 = stateListStateRecord.getList$runtime_release();
                final Unit instance = Unit.INSTANCE;
                monitorexit(list$runtime_release);
                Intrinsics.checkNotNull((Object)list$runtime_release2);
                list$runtime_release = list$runtime_release2.set(n, (Object)t);
                if (Intrinsics.areEqual(list$runtime_release, (Object)list$runtime_release2)) {
                    return value;
                }
                final StateRecord firstStateRecord2 = this.getFirstStateRecord();
                Intrinsics.checkNotNull((Object)firstStateRecord2, "null cannot be cast to non-null type androidx.compose.runtime.snapshots.SnapshotStateList.StateListStateRecord<T of androidx.compose.runtime.snapshots.SnapshotStateList>");
                final StateRecord stateRecord = firstStateRecord2;
                SnapshotKt.getSnapshotInitializer();
                synchronized (SnapshotKt.getLock()) {
                    final Snapshot current = Snapshot.Companion.getCurrent();
                    final StateListStateRecord stateListStateRecord2 = SnapshotKt.writableRecord(stateRecord, this, current);
                    synchronized (SnapshotStateListKt.access$getSync$p()) {
                        final int modification$runtime_release2 = stateListStateRecord2.getModification$runtime_release();
                        boolean b = true;
                        if (modification$runtime_release2 == modification$runtime_release) {
                            stateListStateRecord2.setList$runtime_release((PersistentList)list$runtime_release);
                            stateListStateRecord2.setModification$runtime_release(stateListStateRecord2.getModification$runtime_release() + 1);
                        }
                        else {
                            b = false;
                        }
                        monitorexit(SnapshotStateListKt.access$getSync$p());
                        monitorexit(SnapshotKt.getLock());
                        SnapshotKt.notifyWrite(current, this);
                        if (b) {
                            return value;
                        }
                        continue;
                    }
                }
            }
        }
    }
    
    @Override
    public final /* bridge */ int size() {
        return this.getSize();
    }
    
    @Override
    public List<T> subList(final int n, final int n2) {
        final int n3 = 1;
        int n4;
        if (n >= 0 && n <= n2 && n2 <= this.size()) {
            n4 = n3;
        }
        else {
            n4 = 0;
        }
        if (n4 != 0) {
            return new SubList<T>(this, n, n2);
        }
        throw new IllegalArgumentException("Failed requirement.".toString());
    }
    
    @Override
    public Object[] toArray() {
        return CollectionToArray.toArray((Collection)this);
    }
    
    @Override
    public <T> T[] toArray(final T[] array) {
        Intrinsics.checkNotNullParameter((Object)array, "array");
        return (T[])CollectionToArray.toArray((Collection)this, (Object[])array);
    }
    
    public final List<T> toList() {
        return this.getReadable$runtime_release().getList$runtime_release();
    }
    
    @Metadata(d1 = { "\u0000$\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0002\b\u0003\b\u0000\u0018\u0000*\u0004\b\u0001\u0010\u00012\u00020\u0002B\u0015\b\u0000\u0012\f\u0010\u0003\u001a\b\u0012\u0004\u0012\u00028\u00010\u0004¢\u0006\u0002\u0010\u0005J\u0010\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u0002H\u0016J\b\u0010\u0012\u001a\u00020\u0002H\u0016R \u0010\u0003\u001a\b\u0012\u0004\u0012\u00028\u00010\u0004X\u0080\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0006\u0010\u0007\"\u0004\b\b\u0010\u0005R\u001a\u0010\t\u001a\u00020\nX\u0080\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u000b\u0010\f\"\u0004\b\r\u0010\u000e¨\u0006\u0013" }, d2 = { "Landroidx/compose/runtime/snapshots/SnapshotStateList$StateListStateRecord;", "T", "Landroidx/compose/runtime/snapshots/StateRecord;", "list", "Landroidx/compose/runtime/external/kotlinx/collections/immutable/PersistentList;", "(Landroidx/compose/runtime/external/kotlinx/collections/immutable/PersistentList;)V", "getList$runtime_release", "()Landroidx/compose/runtime/external/kotlinx/collections/immutable/PersistentList;", "setList$runtime_release", "modification", "", "getModification$runtime_release", "()I", "setModification$runtime_release", "(I)V", "assign", "", "value", "create", "runtime_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
    public static final class StateListStateRecord<T> extends StateRecord
    {
        private PersistentList<? extends T> list;
        private int modification;
        
        public StateListStateRecord(final PersistentList<? extends T> list) {
            Intrinsics.checkNotNullParameter((Object)list, "list");
            this.list = list;
        }
        
        @Override
        public void assign(final StateRecord stateRecord) {
            Intrinsics.checkNotNullParameter((Object)stateRecord, "value");
            synchronized (SnapshotStateListKt.access$getSync$p()) {
                this.list = ((StateListStateRecord)stateRecord).list;
                this.modification = ((StateListStateRecord)stateRecord).modification;
                final Unit instance = Unit.INSTANCE;
            }
        }
        
        @Override
        public StateRecord create() {
            return new StateListStateRecord<Object>(this.list);
        }
        
        public final PersistentList<T> getList$runtime_release() {
            return (PersistentList<T>)this.list;
        }
        
        public final int getModification$runtime_release() {
            return this.modification;
        }
        
        public final void setList$runtime_release(final PersistentList<? extends T> list) {
            Intrinsics.checkNotNullParameter((Object)list, "<set-?>");
            this.list = list;
        }
        
        public final void setModification$runtime_release(final int modification) {
            this.modification = modification;
        }
    }
}

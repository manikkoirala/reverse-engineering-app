// 
// Decompiled by Procyon v0.6.0
// 

package androidx.compose.runtime.snapshots;

import kotlin.Pair;
import androidx.compose.runtime.external.kotlinx.collections.immutable.PersistentMap;
import kotlin.Unit;
import kotlin.TuplesKt;
import java.util.LinkedHashMap;
import kotlin.ranges.RangesKt;
import kotlin.collections.MapsKt;
import kotlin.collections.CollectionsKt;
import java.util.Set;
import java.util.Iterator;
import kotlin.jvm.internal.TypeIntrinsics;
import java.util.Collection;
import kotlin.KotlinNothingValueException;
import kotlin.jvm.internal.Intrinsics;
import kotlin.Metadata;
import java.util.Map;

@Metadata(d1 = { "\u00008\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010'\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0001\n\u0002\b\u0003\n\u0002\u0010\u001e\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010)\n\u0002\b\u0004\b\u0002\u0018\u0000*\u0004\b\u0000\u0010\u0001*\u0004\b\u0001\u0010\u00022 \u0012\u0004\u0012\u0002H\u0001\u0012\u0004\u0012\u0002H\u0002\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u0002H\u0001\u0012\u0004\u0012\u0002H\u00020\u00040\u0003B\u0019\u0012\u0012\u0010\u0005\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u0006¢\u0006\u0002\u0010\u0007J\u001c\u0010\b\u001a\u00020\t2\u0012\u0010\n\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u0004H\u0016J\"\u0010\u000b\u001a\u00020\t2\u0018\u0010\f\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u00040\rH\u0016J\u001d\u0010\u000e\u001a\u00020\u000f2\u0012\u0010\n\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u0004H\u0096\u0002J\"\u0010\u0010\u001a\u00020\u000f2\u0018\u0010\f\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u00040\rH\u0016J\u001b\u0010\u0011\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u00040\u0012H\u0096\u0002J\u001c\u0010\u0013\u001a\u00020\u000f2\u0012\u0010\n\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u0004H\u0016J\"\u0010\u0014\u001a\u00020\u000f2\u0018\u0010\f\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u00040\rH\u0016J\"\u0010\u0015\u001a\u00020\u000f2\u0018\u0010\f\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u00040\rH\u0016¨\u0006\u0016" }, d2 = { "Landroidx/compose/runtime/snapshots/SnapshotMapEntrySet;", "K", "V", "Landroidx/compose/runtime/snapshots/SnapshotMapSet;", "", "map", "Landroidx/compose/runtime/snapshots/SnapshotStateMap;", "(Landroidx/compose/runtime/snapshots/SnapshotStateMap;)V", "add", "", "element", "addAll", "elements", "", "contains", "", "containsAll", "iterator", "", "remove", "removeAll", "retainAll", "runtime_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
final class SnapshotMapEntrySet<K, V> extends SnapshotMapSet<K, V, Map.Entry<K, V>>
{
    public SnapshotMapEntrySet(final SnapshotStateMap<K, V> snapshotStateMap) {
        Intrinsics.checkNotNullParameter((Object)snapshotStateMap, "map");
        super(snapshotStateMap);
    }
    
    public Void add(final Map.Entry<K, V> entry) {
        Intrinsics.checkNotNullParameter((Object)entry, "element");
        SnapshotStateMapKt.unsupported();
        throw new KotlinNothingValueException();
    }
    
    public Void addAll(final Collection<? extends Map.Entry<K, V>> collection) {
        Intrinsics.checkNotNullParameter((Object)collection, "elements");
        SnapshotStateMapKt.unsupported();
        throw new KotlinNothingValueException();
    }
    
    @Override
    public final /* bridge */ boolean contains(final Object o) {
        return TypeIntrinsics.isMutableMapEntry(o) && this.contains((Map.Entry<K, V>)o);
    }
    
    public boolean contains(final Map.Entry<K, V> entry) {
        Intrinsics.checkNotNullParameter((Object)entry, "element");
        return Intrinsics.areEqual((Object)this.getMap().get(entry.getKey()), entry.getValue());
    }
    
    @Override
    public boolean containsAll(final Collection<?> collection) {
        Intrinsics.checkNotNullParameter((Object)collection, "elements");
        final Iterable iterable = collection;
        final boolean empty = ((Collection)iterable).isEmpty();
        final boolean b = true;
        boolean b2;
        if (empty) {
            b2 = b;
        }
        else {
            final Iterator iterator = iterable.iterator();
            do {
                b2 = b;
                if (iterator.hasNext()) {
                    continue;
                }
                return b2;
            } while (this.contains(iterator.next()));
            b2 = false;
        }
        return b2;
    }
    
    @Override
    public Iterator<Map.Entry<K, V>> iterator() {
        return new StateMapMutableEntriesIterator<K, V>(this.getMap(), (Iterator<? extends Map.Entry<? extends K, ? extends V>>)this.getMap().getReadable$runtime_release().getMap$runtime_release().entrySet().iterator());
    }
    
    @Override
    public final /* bridge */ boolean remove(final Object o) {
        return TypeIntrinsics.isMutableMapEntry(o) && this.remove((Map.Entry<K, V>)o);
    }
    
    public boolean remove(final Map.Entry<K, V> entry) {
        Intrinsics.checkNotNullParameter((Object)entry, "element");
        return this.getMap().remove(entry.getKey()) != null;
    }
    
    @Override
    public boolean removeAll(final Collection<?> collection) {
        Intrinsics.checkNotNullParameter((Object)collection, "elements");
        final Iterator<?> iterator = collection.iterator();
        boolean b = false;
    Label_0013:
        while (true) {
            b = false;
            while (iterator.hasNext()) {
                if (this.getMap().remove(((Map.Entry<Object, V>)iterator.next()).getKey()) == null && !b) {
                    continue Label_0013;
                }
                b = true;
            }
            break;
        }
        return b;
    }
    
    @Override
    public boolean retainAll(final Collection<?> collection) {
        Intrinsics.checkNotNullParameter((Object)collection, "elements");
        final Iterable iterable = collection;
        final Map map = new LinkedHashMap(RangesKt.coerceAtLeast(MapsKt.mapCapacity(CollectionsKt.collectionSizeOrDefault(iterable, 10)), 16));
        for (final Map.Entry<Object, V> entry : iterable) {
            final Pair to = TuplesKt.to(entry.getKey(), (Object)entry.getValue());
            map.put(to.getFirst(), to.getSecond());
        }
        final SnapshotStateMap<K, V> map2 = this.getMap();
        boolean b = false;
        while (true) {
            Object map$runtime_release = SnapshotStateMapKt.access$getSync$p();
            synchronized (map$runtime_release) {
                final StateRecord firstStateRecord = map2.getFirstStateRecord();
                Intrinsics.checkNotNull((Object)firstStateRecord, "null cannot be cast to non-null type androidx.compose.runtime.snapshots.SnapshotStateMap.StateMapStateRecord<K of androidx.compose.runtime.snapshots.SnapshotStateMap, V of androidx.compose.runtime.snapshots.SnapshotStateMap>");
                final SnapshotStateMap.StateMapStateRecord stateMapStateRecord = SnapshotKt.current(firstStateRecord);
                final PersistentMap map$runtime_release2 = stateMapStateRecord.getMap$runtime_release();
                final int modification$runtime_release = stateMapStateRecord.getModification$runtime_release();
                final Unit instance = Unit.INSTANCE;
                monitorexit(map$runtime_release);
                Intrinsics.checkNotNull((Object)map$runtime_release2);
                map$runtime_release = map$runtime_release2.builder();
                final Map map3 = (Map)map$runtime_release;
                final Iterator iterator2 = map2.entrySet().iterator();
                final int n = 1;
                boolean b2 = b;
                while (iterator2.hasNext()) {
                    final Map.Entry<Object, V> entry2 = (Map.Entry<Object, V>)iterator2.next();
                    if (!map.containsKey(entry2.getKey()) || !Intrinsics.areEqual(map.get(entry2.getKey()), (Object)entry2.getValue())) {
                        map3.remove(entry2.getKey());
                        b2 = true;
                    }
                }
                final Unit instance2 = Unit.INSTANCE;
                map$runtime_release = ((PersistentMap.Builder)map$runtime_release).build();
                if (!Intrinsics.areEqual(map$runtime_release, (Object)map$runtime_release2)) {
                    final StateRecord firstStateRecord2 = map2.getFirstStateRecord();
                    Intrinsics.checkNotNull((Object)firstStateRecord2, "null cannot be cast to non-null type androidx.compose.runtime.snapshots.SnapshotStateMap.StateMapStateRecord<K of androidx.compose.runtime.snapshots.SnapshotStateMap, V of androidx.compose.runtime.snapshots.SnapshotStateMap>");
                    final StateRecord stateRecord = firstStateRecord2;
                    SnapshotKt.getSnapshotInitializer();
                    synchronized (SnapshotKt.getLock()) {
                        final Snapshot current = Snapshot.Companion.getCurrent();
                        final SnapshotStateMap.StateMapStateRecord stateMapStateRecord2 = SnapshotKt.writableRecord(stateRecord, map2, current);
                        synchronized (SnapshotStateMapKt.access$getSync$p()) {
                            int n2;
                            if (stateMapStateRecord2.getModification$runtime_release() == modification$runtime_release) {
                                stateMapStateRecord2.setMap$runtime_release((PersistentMap)map$runtime_release);
                                stateMapStateRecord2.setModification$runtime_release(stateMapStateRecord2.getModification$runtime_release() + 1);
                                n2 = n;
                            }
                            else {
                                n2 = 0;
                            }
                            monitorexit(SnapshotStateMapKt.access$getSync$p());
                            monitorexit(SnapshotKt.getLock());
                            SnapshotKt.notifyWrite(current, map2);
                            b = b2;
                            if (n2 != 0) {
                                return b2;
                            }
                            continue;
                        }
                    }
                }
                return b2;
            }
        }
    }
}

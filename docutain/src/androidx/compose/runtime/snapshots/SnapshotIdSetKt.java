// 
// Decompiled by Procyon v0.6.0
// 

package androidx.compose.runtime.snapshots;

import kotlin.jvm.internal.Intrinsics;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000\u0016\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0010\u0015\n\u0002\b\u0002\u001a\u0010\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\u0002\u001a\u0014\u0010\u0004\u001a\u00020\u0001*\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0001H\u0000¨\u0006\u0007" }, d2 = { "lowestBitOf", "", "bits", "", "binarySearch", "", "value", "runtime_release" }, k = 2, mv = { 1, 8, 0 }, xi = 48)
public final class SnapshotIdSetKt
{
    public static final int binarySearch(final int[] array, final int n) {
        Intrinsics.checkNotNullParameter((Object)array, "<this>");
        int n2 = array.length - 1;
        int i = 0;
        while (i <= n2) {
            final int n3 = i + n2 >>> 1;
            final int n4 = array[n3];
            if (n > n4) {
                i = n3 + 1;
            }
            else {
                if (n >= n4) {
                    return n3;
                }
                n2 = n3 - 1;
            }
        }
        return -(i + 1);
    }
    
    private static final int lowestBitOf(long n) {
        int n2 = 32;
        long n3;
        if ((0xFFFFFFFFL & n) == 0x0L) {
            n3 = n >> 32;
        }
        else {
            n2 = 0;
            n3 = n;
        }
        int n4 = n2;
        n = n3;
        if ((0xFFFFL & n3) == 0x0L) {
            n4 = n2 + 16;
            n = n3 >> 16;
        }
        int n5 = n4;
        long n6 = n;
        if ((0xFFL & n) == 0x0L) {
            n5 = n4 + 8;
            n6 = n >> 8;
        }
        int n7 = n5;
        n = n6;
        if ((0xFL & n6) == 0x0L) {
            n7 = n5 + 4;
            n = n6 >> 4;
        }
        if ((0x1L & n) != 0x0L) {
            return n7;
        }
        if ((0x2L & n) != 0x0L) {
            return n7 + 1;
        }
        if ((0x4L & n) != 0x0L) {
            return n7 + 2;
        }
        if ((n & 0x8L) != 0x0L) {
            return n7 + 3;
        }
        return -1;
    }
}

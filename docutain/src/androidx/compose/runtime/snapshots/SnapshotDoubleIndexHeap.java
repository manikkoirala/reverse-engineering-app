// 
// Decompiled by Procyon v0.6.0
// 

package androidx.compose.runtime.snapshots;

import kotlin.collections.ArraysKt;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0015\n\u0002\b\n\n\u0002\u0010\u0002\n\u0002\b\u000e\b\u0000\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\u000e\u0010\r\u001a\u00020\u00042\u0006\u0010\u000e\u001a\u00020\u0004J\b\u0010\u000f\u001a\u00020\u0004H\u0002J\u0010\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u0004H\u0002J\u0010\u0010\u0013\u001a\u00020\u00112\u0006\u0010\u0014\u001a\u00020\u0004H\u0002J\u0010\u0010\u0015\u001a\u00020\u00042\b\b\u0002\u0010\u0016\u001a\u00020\u0004J\u000e\u0010\u0017\u001a\u00020\u00112\u0006\u0010\u0014\u001a\u00020\u0004J\u0010\u0010\u0018\u001a\u00020\u00112\u0006\u0010\u0007\u001a\u00020\u0004H\u0002J\u0010\u0010\u0019\u001a\u00020\u00112\u0006\u0010\u0007\u001a\u00020\u0004H\u0002J\u0018\u0010\u001a\u001a\u00020\u00112\u0006\u0010\u001b\u001a\u00020\u00042\u0006\u0010\u001c\u001a\u00020\u0004H\u0002J\u0006\u0010\u001d\u001a\u00020\u0011J\u0016\u0010\u001e\u001a\u00020\u00112\u0006\u0010\u0014\u001a\u00020\u00042\u0006\u0010\u000e\u001a\u00020\u0004R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0006X\u0082\u000e¢\u0006\u0002\n\u0000R\u001e\u0010\t\u001a\u00020\u00042\u0006\u0010\b\u001a\u00020\u0004@BX\u0086\u000e¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000bR\u000e\u0010\f\u001a\u00020\u0006X\u0082\u000e¢\u0006\u0002\n\u0000¨\u0006\u001f" }, d2 = { "Landroidx/compose/runtime/snapshots/SnapshotDoubleIndexHeap;", "", "()V", "firstFreeHandle", "", "handles", "", "index", "<set-?>", "size", "getSize", "()I", "values", "add", "value", "allocateHandle", "ensure", "", "atLeast", "freeHandle", "handle", "lowestOrDefault", "default", "remove", "shiftDown", "shiftUp", "swap", "a", "b", "validate", "validateHandle", "runtime_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
public final class SnapshotDoubleIndexHeap
{
    private int firstFreeHandle;
    private int[] handles;
    private int[] index;
    private int size;
    private int[] values;
    
    public SnapshotDoubleIndexHeap() {
        this.values = new int[16];
        this.index = new int[16];
        final int[] handles = new int[16];
        int n;
        for (int i = 0; i < 16; i = n) {
            n = i + 1;
            handles[i] = n;
        }
        this.handles = handles;
    }
    
    private final int allocateHandle() {
        final int length = this.handles.length;
        if (this.firstFreeHandle >= length) {
            int i = 0;
            final int n = length * 2;
            final int[] handles = new int[n];
            while (i < n) {
                final int n2 = i + 1;
                handles[i] = n2;
                i = n2;
            }
            ArraysKt.copyInto$default(this.handles, handles, 0, 0, 0, 14, (Object)null);
            this.handles = handles;
        }
        final int firstFreeHandle = this.firstFreeHandle;
        this.firstFreeHandle = this.handles[firstFreeHandle];
        return firstFreeHandle;
    }
    
    private final void ensure(int n) {
        final int[] values = this.values;
        final int length = values.length;
        if (n <= length) {
            return;
        }
        n = length * 2;
        final int[] values2 = new int[n];
        final int[] index = new int[n];
        ArraysKt.copyInto$default(values, values2, 0, 0, 0, 14, (Object)null);
        ArraysKt.copyInto$default(this.index, index, 0, 0, 0, 14, (Object)null);
        this.values = values2;
        this.index = index;
    }
    
    private final void freeHandle(final int firstFreeHandle) {
        this.handles[firstFreeHandle] = this.firstFreeHandle;
        this.firstFreeHandle = firstFreeHandle;
    }
    
    private final void shiftDown(int i) {
        final int[] values = this.values;
        while (i < this.size >> 1) {
            final int n = i + 1 << 1;
            final int n2 = n - 1;
            if (n < this.size) {
                final int n3 = values[n];
                if (n3 < values[n2]) {
                    if (n3 < values[i]) {
                        this.swap(n, i);
                        i = n;
                        continue;
                    }
                    return;
                }
            }
            if (values[n2] >= values[i]) {
                break;
            }
            this.swap(n2, i);
            i = n2;
        }
    }
    
    private final void shiftUp(int i) {
        final int[] values = this.values;
        final int n = values[i];
        while (i > 0) {
            final int n2 = (i + 1 >> 1) - 1;
            if (values[n2] <= n) {
                break;
            }
            this.swap(n2, i);
            i = n2;
        }
    }
    
    private final void swap(final int n, final int n2) {
        final int[] values = this.values;
        final int[] index = this.index;
        final int[] handles = this.handles;
        final int n3 = values[n];
        values[n] = values[n2];
        values[n2] = n3;
        final int n4 = index[n];
        index[n] = index[n2];
        index[n2] = n4;
        handles[index[n]] = n;
        handles[index[n2]] = n2;
    }
    
    public final int add(final int n) {
        this.ensure(this.size + 1);
        final int n2 = this.size++;
        final int allocateHandle = this.allocateHandle();
        this.values[n2] = n;
        this.index[n2] = allocateHandle;
        this.shiftUp(this.handles[allocateHandle] = n2);
        return allocateHandle;
    }
    
    public final int getSize() {
        return this.size;
    }
    
    public final int lowestOrDefault(int n) {
        if (this.size > 0) {
            n = this.values[0];
        }
        return n;
    }
    
    public final void remove(final int n) {
        final int n2 = this.handles[n];
        this.swap(n2, this.size - 1);
        --this.size;
        this.shiftUp(n2);
        this.shiftDown(n2);
        this.freeHandle(n);
    }
    
    public final void validate() {
        int n;
        for (int size = this.size, i = 1; i < size; i = n) {
            n = i + 1;
            final int[] values = this.values;
            if (values[(n >> 1) - 1] > values[i]) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Index ");
                sb.append(i);
                sb.append(" is out of place");
                throw new IllegalStateException(sb.toString().toString());
            }
        }
    }
    
    public final void validateHandle(final int n, final int i) {
        final int n2 = this.handles[n];
        if (this.index[n2] != n) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Index for handle ");
            sb.append(n);
            sb.append(" is corrupted");
            throw new IllegalStateException(sb.toString().toString());
        }
        if (this.values[n2] == i) {
            return;
        }
        final StringBuilder sb2 = new StringBuilder();
        sb2.append("Value for handle ");
        sb2.append(n);
        sb2.append(" was ");
        sb2.append(this.values[n2]);
        sb2.append(" but was supposed to be ");
        sb2.append(i);
        throw new IllegalStateException(sb2.toString().toString());
    }
}

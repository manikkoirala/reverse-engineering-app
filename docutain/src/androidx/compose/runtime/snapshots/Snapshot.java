// 
// Decompiled by Procyon v0.6.0
// 

package androidx.compose.runtime.snapshots;

import java.util.Collection;
import kotlin.jvm.functions.Function2;
import kotlin.collections.CollectionsKt;
import java.util.Set;
import kotlin.jvm.internal.InlineMarker;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.functions.Function0;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000R\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0010\n\u0002\u0010#\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0011\n\u0002\u0018\u0002\n\u0002\b\u001b\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b7\u0018\u0000 M2\u00020\u0001:\u0001MB\u0017\b\u0004\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\r\u0010*\u001a\u00020 H\u0000¢\u0006\u0002\b+J\r\u0010,\u001a\u00020 H\u0010¢\u0006\u0002\b-J\b\u0010.\u001a\u00020 H\u0016J%\u0010/\u001a\u0002H0\"\u0004\b\u0000\u001002\f\u00101\u001a\b\u0012\u0004\u0012\u0002H002H\u0086\b\u00f8\u0001\u0000¢\u0006\u0002\u00103J\b\u00104\u001a\u00020\bH&J\n\u00105\u001a\u0004\u0018\u00010\u0000H\u0011J\u0015\u00106\u001a\u00020 2\u0006\u00107\u001a\u00020\u0000H ¢\u0006\u0002\b8J\u0015\u00109\u001a\u00020 2\u0006\u00107\u001a\u00020\u0000H ¢\u0006\u0002\b:J\r\u0010;\u001a\u00020 H ¢\u0006\u0002\b<J\u0015\u0010=\u001a\u00020 2\u0006\u0010>\u001a\u00020\u001aH ¢\u0006\u0002\b?J\r\u0010@\u001a\u00020 H\u0000¢\u0006\u0002\bAJ\r\u0010B\u001a\u00020 H\u0010¢\u0006\u0002\bCJ\u0012\u0010D\u001a\u00020 2\b\u00107\u001a\u0004\u0018\u00010\u0000H\u0011J \u0010E\u001a\u00020\u00002\u0016\b\u0002\u0010\u001e\u001a\u0010\u0012\u0004\u0012\u00020\u0001\u0012\u0004\u0012\u00020 \u0018\u00010\u001fH&J\r\u0010F\u001a\u00020\u0003H\u0000¢\u0006\u0002\bGJ\n\u0010H\u001a\u0004\u0018\u00010\u0000H\u0007J\u0012\u0010I\u001a\u00020 2\b\u0010J\u001a\u0004\u0018\u00010\u0000H\u0007J\r\u0010K\u001a\u00020 H\u0000¢\u0006\u0002\bLR\u001a\u0010\u0007\u001a\u00020\bX\u0080\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\t\u0010\n\"\u0004\b\u000b\u0010\fR$\u0010\u0002\u001a\u00020\u00032\u0006\u0010\r\u001a\u00020\u0003@PX\u0096\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u000e\u0010\u000f\"\u0004\b\u0010\u0010\u0011R\u001a\u0010\u0004\u001a\u00020\u0005X\u0090\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0012\u0010\u0013\"\u0004\b\u0014\u0010\u0015R\u0015\u0010\u0016\u001a\u00020\b8\u00c0\u0002X\u0080\u0004¢\u0006\u0006\u001a\u0004\b\u0017\u0010\nR\u001a\u0010\u0018\u001a\n\u0012\u0004\u0012\u00020\u001a\u0018\u00010\u0019X \u0004¢\u0006\u0006\u001a\u0004\b\u001b\u0010\u001cR\u000e\u0010\u001d\u001a\u00020\u0003X\u0082\u000e¢\u0006\u0002\n\u0000R \u0010\u001e\u001a\u0010\u0012\u0004\u0012\u00020\u0001\u0012\u0004\u0012\u00020 \u0018\u00010\u001fX \u0004¢\u0006\u0006\u001a\u0004\b!\u0010\"R\u0012\u0010#\u001a\u00020\bX¦\u0004¢\u0006\u0006\u001a\u0004\b$\u0010\nR\u0012\u0010%\u001a\u00020\u0000X¦\u0004¢\u0006\u0006\u001a\u0004\b&\u0010'R \u0010(\u001a\u0010\u0012\u0004\u0012\u00020\u0001\u0012\u0004\u0012\u00020 \u0018\u00010\u001fX \u0004¢\u0006\u0006\u001a\u0004\b)\u0010\"\u0082\u0001\u0004NOPQ\u0082\u0002\u0007\n\u0005\b\u009920\u0001¨\u0006R" }, d2 = { "Landroidx/compose/runtime/snapshots/Snapshot;", "", "id", "", "invalid", "Landroidx/compose/runtime/snapshots/SnapshotIdSet;", "(ILandroidx/compose/runtime/snapshots/SnapshotIdSet;)V", "disposed", "", "getDisposed$runtime_release", "()Z", "setDisposed$runtime_release", "(Z)V", "<set-?>", "getId", "()I", "setId$runtime_release", "(I)V", "getInvalid$runtime_release", "()Landroidx/compose/runtime/snapshots/SnapshotIdSet;", "setInvalid$runtime_release", "(Landroidx/compose/runtime/snapshots/SnapshotIdSet;)V", "isPinned", "isPinned$runtime_release", "modified", "", "Landroidx/compose/runtime/snapshots/StateObject;", "getModified$runtime_release", "()Ljava/util/Set;", "pinningTrackingHandle", "readObserver", "Lkotlin/Function1;", "", "getReadObserver$runtime_release", "()Lkotlin/jvm/functions/Function1;", "readOnly", "getReadOnly", "root", "getRoot", "()Landroidx/compose/runtime/snapshots/Snapshot;", "writeObserver", "getWriteObserver$runtime_release", "closeAndReleasePinning", "closeAndReleasePinning$runtime_release", "closeLocked", "closeLocked$runtime_release", "dispose", "enter", "T", "block", "Lkotlin/Function0;", "(Lkotlin/jvm/functions/Function0;)Ljava/lang/Object;", "hasPendingChanges", "makeCurrent", "nestedActivated", "snapshot", "nestedActivated$runtime_release", "nestedDeactivated", "nestedDeactivated$runtime_release", "notifyObjectsInitialized", "notifyObjectsInitialized$runtime_release", "recordModified", "state", "recordModified$runtime_release", "releasePinnedSnapshotLocked", "releasePinnedSnapshotLocked$runtime_release", "releasePinnedSnapshotsForCloseLocked", "releasePinnedSnapshotsForCloseLocked$runtime_release", "restoreCurrent", "takeNestedSnapshot", "takeoverPinnedSnapshot", "takeoverPinnedSnapshot$runtime_release", "unsafeEnter", "unsafeLeave", "oldSnapshot", "validateNotDisposed", "validateNotDisposed$runtime_release", "Companion", "Landroidx/compose/runtime/snapshots/MutableSnapshot;", "Landroidx/compose/runtime/snapshots/NestedReadonlySnapshot;", "Landroidx/compose/runtime/snapshots/ReadonlySnapshot;", "Landroidx/compose/runtime/snapshots/TransparentObserverSnapshot;", "runtime_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
public abstract class Snapshot
{
    public static final int $stable;
    public static final Companion Companion;
    private boolean disposed;
    private int id;
    private SnapshotIdSet invalid;
    private int pinningTrackingHandle;
    
    static {
        Companion = new Companion(null);
        $stable = 8;
    }
    
    private Snapshot(int trackPinning, final SnapshotIdSet invalid) {
        this.invalid = invalid;
        this.id = trackPinning;
        if (trackPinning != 0) {
            trackPinning = SnapshotKt.trackPinning(trackPinning, this.getInvalid$runtime_release());
        }
        else {
            trackPinning = -1;
        }
        this.pinningTrackingHandle = trackPinning;
    }
    
    public static final /* synthetic */ int access$getPinningTrackingHandle$p(final Snapshot snapshot) {
        return snapshot.pinningTrackingHandle;
    }
    
    public final void closeAndReleasePinning$runtime_release() {
        synchronized (SnapshotKt.getLock()) {
            this.closeLocked$runtime_release();
            this.releasePinnedSnapshotsForCloseLocked$runtime_release();
            final Unit instance = Unit.INSTANCE;
        }
    }
    
    public void closeLocked$runtime_release() {
        SnapshotKt.access$setOpenSnapshots$p(SnapshotKt.access$getOpenSnapshots$p().clear(this.getId()));
    }
    
    public void dispose() {
        this.disposed = true;
        synchronized (SnapshotKt.getLock()) {
            this.releasePinnedSnapshotLocked$runtime_release();
            final Unit instance = Unit.INSTANCE;
        }
    }
    
    public final <T> T enter(final Function0<? extends T> function0) {
        Intrinsics.checkNotNullParameter((Object)function0, "block");
        final Snapshot current = this.makeCurrent();
        try {
            return (T)function0.invoke();
        }
        finally {
            InlineMarker.finallyStart(1);
            this.restoreCurrent(current);
            InlineMarker.finallyEnd(1);
        }
    }
    
    public final boolean getDisposed$runtime_release() {
        return this.disposed;
    }
    
    public int getId() {
        return this.id;
    }
    
    public SnapshotIdSet getInvalid$runtime_release() {
        return this.invalid;
    }
    
    public abstract Set<StateObject> getModified$runtime_release();
    
    public abstract Function1<Object, Unit> getReadObserver$runtime_release();
    
    public abstract boolean getReadOnly();
    
    public abstract Snapshot getRoot();
    
    public abstract Function1<Object, Unit> getWriteObserver$runtime_release();
    
    public abstract boolean hasPendingChanges();
    
    public final boolean isPinned$runtime_release() {
        return access$getPinningTrackingHandle$p(this) >= 0;
    }
    
    public Snapshot makeCurrent() {
        final Snapshot snapshot = SnapshotKt.access$getThreadSnapshot$p().get();
        SnapshotKt.access$getThreadSnapshot$p().set(this);
        return snapshot;
    }
    
    public abstract void nestedActivated$runtime_release(final Snapshot p0);
    
    public abstract void nestedDeactivated$runtime_release(final Snapshot p0);
    
    public abstract void notifyObjectsInitialized$runtime_release();
    
    public abstract void recordModified$runtime_release(final StateObject p0);
    
    public final void releasePinnedSnapshotLocked$runtime_release() {
        final int pinningTrackingHandle = this.pinningTrackingHandle;
        if (pinningTrackingHandle >= 0) {
            SnapshotKt.releasePinningLocked(pinningTrackingHandle);
            this.pinningTrackingHandle = -1;
        }
    }
    
    public void releasePinnedSnapshotsForCloseLocked$runtime_release() {
        this.releasePinnedSnapshotLocked$runtime_release();
    }
    
    public void restoreCurrent(final Snapshot snapshot) {
        SnapshotKt.access$getThreadSnapshot$p().set(snapshot);
    }
    
    public final void setDisposed$runtime_release(final boolean disposed) {
        this.disposed = disposed;
    }
    
    public void setId$runtime_release(final int id) {
        this.id = id;
    }
    
    public void setInvalid$runtime_release(final SnapshotIdSet invalid) {
        Intrinsics.checkNotNullParameter((Object)invalid, "<set-?>");
        this.invalid = invalid;
    }
    
    public abstract Snapshot takeNestedSnapshot(final Function1<Object, Unit> p0);
    
    public final int takeoverPinnedSnapshot$runtime_release() {
        final int pinningTrackingHandle = this.pinningTrackingHandle;
        this.pinningTrackingHandle = -1;
        return pinningTrackingHandle;
    }
    
    public final Snapshot unsafeEnter() {
        return this.makeCurrent();
    }
    
    public final void unsafeLeave(final Snapshot snapshot) {
        if (SnapshotKt.access$getThreadSnapshot$p().get() == this) {
            this.restoreCurrent(snapshot);
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Cannot leave snapshot; ");
        sb.append(this);
        sb.append(" is not the current snapshot");
        throw new IllegalStateException(sb.toString().toString());
    }
    
    public final void validateNotDisposed$runtime_release() {
        if (this.disposed ^ true) {
            return;
        }
        throw new IllegalArgumentException("Cannot use a disposed snapshot".toString());
    }
    
    @Metadata(d1 = { "\u0000R\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\"\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\b\u0010\u0007\u001a\u00020\u0004H\u0001J%\u0010\b\u001a\u0002H\t\"\u0004\b\u0000\u0010\t2\f\u0010\n\u001a\b\u0012\u0004\u0012\u0002H\t0\u000bH\u0086\b\u00f8\u0001\u0000¢\u0006\u0002\u0010\fJ\u0006\u0010\r\u001a\u00020\u000eJO\u0010\u000f\u001a\u0002H\t\"\u0004\b\u0000\u0010\t2\u0016\b\u0002\u0010\u0010\u001a\u0010\u0012\u0004\u0012\u00020\u0001\u0012\u0004\u0012\u00020\u000e\u0018\u00010\u00112\u0016\b\u0002\u0010\u0012\u001a\u0010\u0012\u0004\u0012\u00020\u0001\u0012\u0004\u0012\u00020\u000e\u0018\u00010\u00112\f\u0010\n\u001a\b\u0012\u0004\u0012\u0002H\t0\u000b¢\u0006\u0002\u0010\u0013J\b\u0010\u0014\u001a\u00020\u0015H\u0007J&\u0010\u0016\u001a\u00020\u00172\u001e\u0010\u0018\u001a\u001a\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00010\u001a\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u000e0\u0019J\u001a\u0010\u001b\u001a\u00020\u00172\u0012\u0010\u0018\u001a\u000e\u0012\u0004\u0012\u00020\u0001\u0012\u0004\u0012\u00020\u000e0\u0011J\n\u0010\u001c\u001a\u0004\u0018\u00010\u0004H\u0001J\u0012\u0010\u001d\u001a\u00020\u000e2\b\u0010\u001e\u001a\u0004\u0018\u00010\u0004H\u0001J\u0006\u0010\u001f\u001a\u00020\u000eJ6\u0010 \u001a\u00020!2\u0016\b\u0002\u0010\u0010\u001a\u0010\u0012\u0004\u0012\u00020\u0001\u0012\u0004\u0012\u00020\u000e\u0018\u00010\u00112\u0016\b\u0002\u0010\u0012\u001a\u0010\u0012\u0004\u0012\u00020\u0001\u0012\u0004\u0012\u00020\u000e\u0018\u00010\u0011J\u001e\u0010\"\u001a\u00020\u00042\u0016\b\u0002\u0010\u0010\u001a\u0010\u0012\u0004\u0012\u00020\u0001\u0012\u0004\u0012\u00020\u000e\u0018\u00010\u0011J%\u0010#\u001a\u0002H$\"\u0004\b\u0000\u0010$2\f\u0010\n\u001a\b\u0012\u0004\u0012\u0002H$0\u000bH\u0086\b\u00f8\u0001\u0000¢\u0006\u0002\u0010\fJ7\u0010%\u001a\u0002H\t\"\u0004\b\u0000\u0010\t2\u0011\u0010\n\u001a\r\u0012\u0004\u0012\u0002H\t0\u000b¢\u0006\u0002\b&H\u0086\b\u00f8\u0001\u0000\u0082\u0002\n\n\b\b\u0001\u0012\u0002\u0010\u0001 \u0001¢\u0006\u0002\u0010\fR\u0011\u0010\u0003\u001a\u00020\u00048F¢\u0006\u0006\u001a\u0004\b\u0005\u0010\u0006\u0082\u0002\u0007\n\u0005\b\u009920\u0001¨\u0006'" }, d2 = { "Landroidx/compose/runtime/snapshots/Snapshot$Companion;", "", "()V", "current", "Landroidx/compose/runtime/snapshots/Snapshot;", "getCurrent", "()Landroidx/compose/runtime/snapshots/Snapshot;", "createNonObservableSnapshot", "global", "T", "block", "Lkotlin/Function0;", "(Lkotlin/jvm/functions/Function0;)Ljava/lang/Object;", "notifyObjectsInitialized", "", "observe", "readObserver", "Lkotlin/Function1;", "writeObserver", "(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;)Ljava/lang/Object;", "openSnapshotCount", "", "registerApplyObserver", "Landroidx/compose/runtime/snapshots/ObserverHandle;", "observer", "Lkotlin/Function2;", "", "registerGlobalWriteObserver", "removeCurrent", "restoreCurrent", "previous", "sendApplyNotifications", "takeMutableSnapshot", "Landroidx/compose/runtime/snapshots/MutableSnapshot;", "takeSnapshot", "withMutableSnapshot", "R", "withoutReadObservation", "Landroidx/compose/runtime/DisallowComposableCalls;", "runtime_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
    public static final class Companion
    {
        private Companion() {
        }
        
        public static /* synthetic */ MutableSnapshot takeMutableSnapshot$default(final Companion companion, Function1 function1, Function1 function2, final int n, final Object o) {
            if ((n & 0x1) != 0x0) {
                function1 = null;
            }
            if ((n & 0x2) != 0x0) {
                function2 = null;
            }
            return companion.takeMutableSnapshot((Function1<Object, Unit>)function1, (Function1<Object, Unit>)function2);
        }
        
        public final Snapshot createNonObservableSnapshot() {
            return SnapshotKt.createTransparentSnapshotWithNoParentReadObserver$default(SnapshotKt.access$getThreadSnapshot$p().get(), null, false, 6, null);
        }
        
        public final Snapshot getCurrent() {
            return SnapshotKt.currentSnapshot();
        }
        
        public final <T> T global(final Function0<? extends T> function0) {
            Intrinsics.checkNotNullParameter((Object)function0, "block");
            final Snapshot removeCurrent = this.removeCurrent();
            final Object invoke = function0.invoke();
            Snapshot.Companion.restoreCurrent(removeCurrent);
            return (T)invoke;
        }
        
        public final void notifyObjectsInitialized() {
            SnapshotKt.currentSnapshot().notifyObjectsInitialized$runtime_release();
        }
        
        public final <T> T observe(Function1<Object, Unit> takeNestedSnapshot, Function1<Object, Unit> current, final Function0<? extends T> function0) {
            Intrinsics.checkNotNullParameter((Object)function0, "block");
            if (takeNestedSnapshot == null && current == null) {
                return (T)function0.invoke();
            }
            final Snapshot snapshot = SnapshotKt.access$getThreadSnapshot$p().get();
            if (snapshot != null && !(snapshot instanceof MutableSnapshot)) {
                if (takeNestedSnapshot == null) {
                    return (T)function0.invoke();
                }
                takeNestedSnapshot = snapshot.takeNestedSnapshot((Function1<Object, Unit>)takeNestedSnapshot);
            }
            else {
                MutableSnapshot mutableSnapshot;
                if (snapshot instanceof MutableSnapshot) {
                    mutableSnapshot = (MutableSnapshot)snapshot;
                }
                else {
                    mutableSnapshot = null;
                }
                takeNestedSnapshot = new TransparentObserverMutableSnapshot(mutableSnapshot, (Function1<Object, Unit>)takeNestedSnapshot, (Function1<Object, Unit>)current, true, false);
            }
            try {
                current = takeNestedSnapshot.makeCurrent();
                try {
                    return (T)function0.invoke();
                }
                finally {
                    takeNestedSnapshot.restoreCurrent(current);
                }
            }
            finally {
                takeNestedSnapshot.dispose();
            }
        }
        
        public final int openSnapshotCount() {
            return CollectionsKt.toList((Iterable)SnapshotKt.access$getOpenSnapshots$p()).size();
        }
        
        public final ObserverHandle registerApplyObserver(final Function2<? super Set<?>, ? super Snapshot, Unit> function2) {
            Intrinsics.checkNotNullParameter((Object)function2, "observer");
            advanceGlobalSnapshot((kotlin.jvm.functions.Function1<? super SnapshotIdSet, ?>)SnapshotKt.access$getEmptyLambda$p());
            synchronized (SnapshotKt.getLock()) {
                SnapshotKt.access$getApplyObservers$p().add(function2);
                monitorexit(SnapshotKt.getLock());
                return (ObserverHandle)new Snapshot$Companion$registerApplyObserver.Snapshot$Companion$registerApplyObserver$2((Function2)function2);
            }
        }
        
        public final ObserverHandle registerGlobalWriteObserver(final Function1<Object, Unit> function1) {
            Intrinsics.checkNotNullParameter((Object)function1, "observer");
            synchronized (SnapshotKt.getLock()) {
                SnapshotKt.access$getGlobalWriteObservers$p().add(function1);
                monitorexit(SnapshotKt.getLock());
                advanceGlobalSnapshot();
                return (ObserverHandle)new Snapshot$Companion$registerGlobalWriteObserver.Snapshot$Companion$registerGlobalWriteObserver$2((Function1)function1);
            }
        }
        
        public final Snapshot removeCurrent() {
            final Snapshot snapshot = SnapshotKt.access$getThreadSnapshot$p().get();
            if (snapshot != null) {
                SnapshotKt.access$getThreadSnapshot$p().set(null);
            }
            return snapshot;
        }
        
        public final void restoreCurrent(final Snapshot snapshot) {
            if (snapshot != null) {
                SnapshotKt.access$getThreadSnapshot$p().set(snapshot);
            }
        }
        
        public final void sendApplyNotifications() {
            synchronized (SnapshotKt.getLock()) {
                final Set<StateObject> modified$runtime_release = SnapshotKt.access$getCurrentGlobalSnapshot$p().get().getModified$runtime_release();
                int n = 0;
                if (modified$runtime_release != null) {
                    final boolean empty = modified$runtime_release.isEmpty();
                    n = n;
                    if (empty ^ true) {
                        n = 1;
                    }
                }
                monitorexit(SnapshotKt.getLock());
                if (n != 0) {
                    advanceGlobalSnapshot();
                }
            }
        }
        
        public final MutableSnapshot takeMutableSnapshot(final Function1<Object, Unit> function1, final Function1<Object, Unit> function2) {
            final Snapshot currentSnapshot = SnapshotKt.currentSnapshot();
            MutableSnapshot mutableSnapshot;
            if (currentSnapshot instanceof MutableSnapshot) {
                mutableSnapshot = (MutableSnapshot)currentSnapshot;
            }
            else {
                mutableSnapshot = null;
            }
            if (mutableSnapshot != null) {
                final MutableSnapshot takeNestedMutableSnapshot = mutableSnapshot.takeNestedMutableSnapshot(function1, function2);
                if (takeNestedMutableSnapshot != null) {
                    return takeNestedMutableSnapshot;
                }
            }
            throw new IllegalStateException("Cannot create a mutable snapshot of an read-only snapshot".toString());
        }
        
        public final Snapshot takeSnapshot(final Function1<Object, Unit> function1) {
            return SnapshotKt.currentSnapshot().takeNestedSnapshot(function1);
        }
        
        public final <R> R withMutableSnapshot(final Function0<? extends R> function0) {
            Intrinsics.checkNotNullParameter((Object)function0, "block");
            final MutableSnapshot takeMutableSnapshot$default = takeMutableSnapshot$default(this, null, null, 3, null);
            try {
                final Snapshot snapshot = takeMutableSnapshot$default;
                final Snapshot current = snapshot.makeCurrent();
                try {
                    final Object invoke = function0.invoke();
                    InlineMarker.finallyStart(1);
                    snapshot.restoreCurrent(current);
                    InlineMarker.finallyEnd(1);
                    takeMutableSnapshot$default.apply().check();
                    return (R)invoke;
                }
                finally {
                    InlineMarker.finallyStart(1);
                    snapshot.restoreCurrent(current);
                    InlineMarker.finallyEnd(1);
                }
            }
            finally {
                InlineMarker.finallyStart(1);
                takeMutableSnapshot$default.dispose();
                InlineMarker.finallyEnd(1);
            }
        }
        
        public final <T> T withoutReadObservation(final Function0<? extends T> function0) {
            Intrinsics.checkNotNullParameter((Object)function0, "block");
            final Snapshot nonObservableSnapshot = this.createNonObservableSnapshot();
            try {
                final Snapshot current = nonObservableSnapshot.makeCurrent();
                try {
                    return (T)function0.invoke();
                }
                finally {
                    InlineMarker.finallyStart(1);
                    nonObservableSnapshot.restoreCurrent(current);
                    InlineMarker.finallyEnd(1);
                }
            }
            finally {
                InlineMarker.finallyStart(1);
                nonObservableSnapshot.dispose();
                InlineMarker.finallyEnd(1);
            }
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.compose.runtime.snapshots;

import java.util.Iterator;
import androidx.compose.runtime.external.kotlinx.collections.immutable.PersistentMap;
import kotlin.jvm.internal.InlineMarker;
import kotlin.Unit;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.functions.Function1;
import androidx.compose.runtime.external.kotlinx.collections.immutable.ExtensionsKt;
import java.util.Collection;
import java.util.Set;
import kotlin.Metadata;
import kotlin.jvm.internal.markers.KMutableMap;
import java.util.Map;

@Metadata(d1 = { "\u0000p\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010%\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010$\n\u0002\b\u0004\n\u0002\u0010#\n\u0002\u0010'\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u001f\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010&\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0002\b\u0018\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\b\u0007\u0018\u0000*\u0004\b\u0000\u0010\u0001*\u0004\b\u0001\u0010\u00022\u000e\u0012\u0004\u0012\u0002H\u0001\u0012\u0004\u0012\u0002H\u00020\u00032\u00020\u0004:\u0001LB\u0005¢\u0006\u0002\u0010\u0005J1\u0010&\u001a\u00020'2\u001e\u0010(\u001a\u001a\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010*\u0012\u0004\u0012\u00020'0)H\u0080\b\u00f8\u0001\u0000¢\u0006\u0002\b+J1\u0010,\u001a\u00020'2\u001e\u0010(\u001a\u001a\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010*\u0012\u0004\u0012\u00020'0)H\u0080\b\u00f8\u0001\u0000¢\u0006\u0002\b-J\b\u0010.\u001a\u00020/H\u0016J\u0015\u00100\u001a\u00020'2\u0006\u00101\u001a\u00028\u0000H\u0016¢\u0006\u0002\u00102J\u0015\u00103\u001a\u00020'2\u0006\u00104\u001a\u00028\u0001H\u0016¢\u0006\u0002\u00102J\u0018\u00105\u001a\u0004\u0018\u00018\u00012\u0006\u00101\u001a\u00028\u0000H\u0096\u0002¢\u0006\u0002\u00106J\b\u00107\u001a\u00020'H\u0016J4\u00108\u001a\u0002H9\"\u0004\b\u0002\u001092\u001e\u0010:\u001a\u001a\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u0003\u0012\u0004\u0012\u0002H90)H\u0082\b¢\u0006\u0002\u0010;J\u0010\u0010<\u001a\u00020/2\u0006\u00104\u001a\u00020\u0011H\u0016J\u001f\u0010=\u001a\u0004\u0018\u00018\u00012\u0006\u00101\u001a\u00028\u00002\u0006\u00104\u001a\u00028\u0001H\u0016¢\u0006\u0002\u0010>J\u001e\u0010?\u001a\u00020/2\u0014\u0010@\u001a\u0010\u0012\u0006\b\u0001\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u0007H\u0016J\u0017\u0010A\u001a\u0004\u0018\u00018\u00012\u0006\u00101\u001a\u00028\u0000H\u0016¢\u0006\u0002\u00106J1\u0010B\u001a\u00020'2\u001e\u0010(\u001a\u001a\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\r\u0012\u0004\u0012\u00020'0)H\u0080\b\u00f8\u0001\u0000¢\u0006\u0002\bCJ\u0017\u0010D\u001a\u00020'2\u0006\u00104\u001a\u00028\u0001H\u0000¢\u0006\u0004\bE\u00102J\u0012\u0010F\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u0007J5\u0010G\u001a\u00020/2*\u0010:\u001a&\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010H\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010H0)H\u0082\bJ9\u0010I\u001a\u0002H9\"\u0004\b\u0002\u001092#\u0010:\u001a\u001f\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u001c\u0012\u0004\u0012\u0002H90)¢\u0006\u0002\bJH\u0082\b¢\u0006\u0002\u0010;J9\u0010K\u001a\u0002H9\"\u0004\b\u0002\u001092#\u0010:\u001a\u001f\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u001c\u0012\u0004\u0012\u0002H90)¢\u0006\u0002\bJH\u0082\b¢\u0006\u0002\u0010;R&\u0010\u0006\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u00078AX\u0080\u0004¢\u0006\f\u0012\u0004\b\b\u0010\u0005\u001a\u0004\b\t\u0010\nR&\u0010\u000b\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\r0\fX\u0096\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\u000fR\u001e\u0010\u0012\u001a\u00020\u00112\u0006\u0010\u0010\u001a\u00020\u0011@RX\u0096\u000e¢\u0006\b\n\u0000\u001a\u0004\b\u0013\u0010\u0014R\u001a\u0010\u0015\u001a\b\u0012\u0004\u0012\u00028\u00000\fX\u0096\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0016\u0010\u000fR\u0014\u0010\u0017\u001a\u00020\u00188@X\u0080\u0004¢\u0006\u0006\u001a\u0004\b\u0019\u0010\u001aR&\u0010\u001b\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u001c8@X\u0080\u0004¢\u0006\f\u0012\u0004\b\u001d\u0010\u0005\u001a\u0004\b\u001e\u0010\u001fR\u0014\u0010 \u001a\u00020\u00188VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b!\u0010\u001aR\u001a\u0010\"\u001a\b\u0012\u0004\u0012\u00028\u00010#X\u0096\u0004¢\u0006\b\n\u0000\u001a\u0004\b$\u0010%\u0082\u0002\u0007\n\u0005\b\u009920\u0001¨\u0006M" }, d2 = { "Landroidx/compose/runtime/snapshots/SnapshotStateMap;", "K", "V", "", "Landroidx/compose/runtime/snapshots/StateObject;", "()V", "debuggerDisplayValue", "", "getDebuggerDisplayValue$annotations", "getDebuggerDisplayValue", "()Ljava/util/Map;", "entries", "", "", "getEntries", "()Ljava/util/Set;", "<set-?>", "Landroidx/compose/runtime/snapshots/StateRecord;", "firstStateRecord", "getFirstStateRecord", "()Landroidx/compose/runtime/snapshots/StateRecord;", "keys", "getKeys", "modification", "", "getModification$runtime_release", "()I", "readable", "Landroidx/compose/runtime/snapshots/SnapshotStateMap$StateMapStateRecord;", "getReadable$runtime_release$annotations", "getReadable$runtime_release", "()Landroidx/compose/runtime/snapshots/SnapshotStateMap$StateMapStateRecord;", "size", "getSize", "values", "", "getValues", "()Ljava/util/Collection;", "all", "", "predicate", "Lkotlin/Function1;", "", "all$runtime_release", "any", "any$runtime_release", "clear", "", "containsKey", "key", "(Ljava/lang/Object;)Z", "containsValue", "value", "get", "(Ljava/lang/Object;)Ljava/lang/Object;", "isEmpty", "mutate", "R", "block", "(Lkotlin/jvm/functions/Function1;)Ljava/lang/Object;", "prependStateRecord", "put", "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;", "putAll", "from", "remove", "removeIf", "removeIf$runtime_release", "removeValue", "removeValue$runtime_release", "toMap", "update", "Landroidx/compose/runtime/external/kotlinx/collections/immutable/PersistentMap;", "withCurrent", "Lkotlin/ExtensionFunctionType;", "writable", "StateMapStateRecord", "runtime_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
public final class SnapshotStateMap<K, V> implements Map<K, V>, StateObject, KMutableMap
{
    public static final int $stable = 0;
    private final Set<Entry<K, V>> entries;
    private StateRecord firstStateRecord;
    private final Set<K> keys;
    private final Collection<V> values;
    
    public SnapshotStateMap() {
        this.firstStateRecord = new StateMapStateRecord<Object, Object>(ExtensionsKt.persistentHashMapOf());
        this.entries = (Set)new SnapshotMapEntrySet((SnapshotStateMap<Object, Object>)this);
        this.keys = (Set)new SnapshotMapKeySet((SnapshotStateMap<Object, Object>)this);
        this.values = (Collection)new SnapshotMapValueSet((SnapshotStateMap<Object, Object>)this);
    }
    
    private final <R> R mutate(final Function1<? super Map<K, V>, ? extends R> function1) {
        while (true) {
            Object o = SnapshotStateMapKt.access$getSync$p();
            monitorenter(o);
            try {
                final StateRecord firstStateRecord = this.getFirstStateRecord();
                Intrinsics.checkNotNull((Object)firstStateRecord, "null cannot be cast to non-null type androidx.compose.runtime.snapshots.SnapshotStateMap.StateMapStateRecord<K of androidx.compose.runtime.snapshots.SnapshotStateMap, V of androidx.compose.runtime.snapshots.SnapshotStateMap>");
                final StateMapStateRecord stateMapStateRecord = SnapshotKt.current(firstStateRecord);
                final PersistentMap map$runtime_release = stateMapStateRecord.getMap$runtime_release();
                final int modification$runtime_release = stateMapStateRecord.getModification$runtime_release();
                final Unit instance = Unit.INSTANCE;
                InlineMarker.finallyStart(1);
                monitorexit(o);
                InlineMarker.finallyEnd(1);
                Intrinsics.checkNotNull((Object)map$runtime_release);
                final PersistentMap.Builder builder = map$runtime_release.builder();
                o = function1.invoke((Object)builder);
                final PersistentMap build = builder.build();
                if (!Intrinsics.areEqual((Object)build, (Object)map$runtime_release)) {
                    final StateRecord firstStateRecord2 = this.getFirstStateRecord();
                    Intrinsics.checkNotNull((Object)firstStateRecord2, "null cannot be cast to non-null type androidx.compose.runtime.snapshots.SnapshotStateMap.StateMapStateRecord<K of androidx.compose.runtime.snapshots.SnapshotStateMap, V of androidx.compose.runtime.snapshots.SnapshotStateMap>");
                    final StateRecord stateRecord = firstStateRecord2;
                    SnapshotKt.getSnapshotInitializer();
                    final Object lock = SnapshotKt.getLock();
                    monitorenter(lock);
                    try {
                        final Snapshot current = Snapshot.Companion.getCurrent();
                        final StateMapStateRecord stateMapStateRecord2 = SnapshotKt.writableRecord(stateRecord, this, current);
                        final Object access$getSync$p = SnapshotStateMapKt.access$getSync$p();
                        monitorenter(access$getSync$p);
                        try {
                            boolean b;
                            if (stateMapStateRecord2.getModification$runtime_release() == modification$runtime_release) {
                                stateMapStateRecord2.setMap$runtime_release(build);
                                stateMapStateRecord2.setModification$runtime_release(stateMapStateRecord2.getModification$runtime_release() + 1);
                                b = true;
                            }
                            else {
                                b = false;
                            }
                            InlineMarker.finallyStart(1);
                            monitorexit(access$getSync$p);
                            InlineMarker.finallyEnd(1);
                            InlineMarker.finallyStart(1);
                            monitorexit(lock);
                            InlineMarker.finallyEnd(1);
                            SnapshotKt.notifyWrite(current, this);
                            if (b) {
                                return (R)o;
                            }
                            continue;
                        }
                        finally {
                            InlineMarker.finallyStart(1);
                            monitorexit(access$getSync$p);
                            InlineMarker.finallyEnd(1);
                        }
                    }
                    finally {
                        InlineMarker.finallyStart(1);
                        monitorexit(lock);
                        InlineMarker.finallyEnd(1);
                    }
                }
                return (R)o;
            }
            finally {
                InlineMarker.finallyStart(1);
                monitorexit(o);
                InlineMarker.finallyEnd(1);
            }
        }
    }
    
    private final void update(final Function1<? super PersistentMap<K, ? extends V>, ? extends PersistentMap<K, ? extends V>> function1) {
        final StateRecord firstStateRecord = this.getFirstStateRecord();
        Intrinsics.checkNotNull((Object)firstStateRecord, "null cannot be cast to non-null type androidx.compose.runtime.snapshots.SnapshotStateMap.StateMapStateRecord<K of androidx.compose.runtime.snapshots.SnapshotStateMap, V of androidx.compose.runtime.snapshots.SnapshotStateMap>");
        final StateMapStateRecord stateMapStateRecord = SnapshotKt.current(firstStateRecord);
        final PersistentMap map$runtime_release = (PersistentMap)function1.invoke((Object)stateMapStateRecord.getMap$runtime_release());
        if (map$runtime_release != stateMapStateRecord.getMap$runtime_release()) {
            final StateRecord firstStateRecord2 = this.getFirstStateRecord();
            Intrinsics.checkNotNull((Object)firstStateRecord2, "null cannot be cast to non-null type androidx.compose.runtime.snapshots.SnapshotStateMap.StateMapStateRecord<K of androidx.compose.runtime.snapshots.SnapshotStateMap, V of androidx.compose.runtime.snapshots.SnapshotStateMap>");
            final StateRecord stateRecord = firstStateRecord2;
            SnapshotKt.getSnapshotInitializer();
            final Object lock = SnapshotKt.getLock();
            monitorenter(lock);
            try {
                final Snapshot current = Snapshot.Companion.getCurrent();
                final StateMapStateRecord stateMapStateRecord2 = SnapshotKt.writableRecord(stateRecord, this, current);
                final Object access$getSync$p = SnapshotStateMapKt.access$getSync$p();
                monitorenter(access$getSync$p);
                try {
                    stateMapStateRecord2.setMap$runtime_release(map$runtime_release);
                    stateMapStateRecord2.setModification$runtime_release(stateMapStateRecord2.getModification$runtime_release() + 1);
                    InlineMarker.finallyStart(1);
                    monitorexit(access$getSync$p);
                    InlineMarker.finallyEnd(1);
                    InlineMarker.finallyStart(1);
                    monitorexit(lock);
                    InlineMarker.finallyEnd(1);
                    SnapshotKt.notifyWrite(current, this);
                }
                finally {
                    InlineMarker.finallyStart(1);
                    monitorexit(access$getSync$p);
                    InlineMarker.finallyEnd(1);
                }
            }
            finally {
                InlineMarker.finallyStart(1);
                monitorexit(lock);
                InlineMarker.finallyEnd(1);
            }
        }
    }
    
    private final <R> R withCurrent(final Function1<? super StateMapStateRecord<K, V>, ? extends R> function1) {
        final StateRecord firstStateRecord = this.getFirstStateRecord();
        Intrinsics.checkNotNull((Object)firstStateRecord, "null cannot be cast to non-null type androidx.compose.runtime.snapshots.SnapshotStateMap.StateMapStateRecord<K of androidx.compose.runtime.snapshots.SnapshotStateMap, V of androidx.compose.runtime.snapshots.SnapshotStateMap>");
        return (R)function1.invoke((Object)SnapshotKt.current(firstStateRecord));
    }
    
    private final <R> R writable(final Function1<? super StateMapStateRecord<K, V>, ? extends R> function1) {
        final StateRecord firstStateRecord = this.getFirstStateRecord();
        Intrinsics.checkNotNull((Object)firstStateRecord, "null cannot be cast to non-null type androidx.compose.runtime.snapshots.SnapshotStateMap.StateMapStateRecord<K of androidx.compose.runtime.snapshots.SnapshotStateMap, V of androidx.compose.runtime.snapshots.SnapshotStateMap>");
        final StateRecord stateRecord = firstStateRecord;
        SnapshotKt.getSnapshotInitializer();
        final Object lock = SnapshotKt.getLock();
        monitorenter(lock);
        try {
            final Snapshot current = Snapshot.Companion.getCurrent();
            final Object invoke = function1.invoke((Object)SnapshotKt.writableRecord(stateRecord, this, current));
            InlineMarker.finallyStart(1);
            monitorexit(lock);
            InlineMarker.finallyEnd(1);
            SnapshotKt.notifyWrite(current, this);
            return (R)invoke;
        }
        finally {
            InlineMarker.finallyStart(1);
            monitorexit(lock);
            InlineMarker.finallyEnd(1);
        }
    }
    
    public final boolean all$runtime_release(final Function1<? super Entry<? extends K, ? extends V>, Boolean> function1) {
        Intrinsics.checkNotNullParameter((Object)function1, "predicate");
        final Iterator<Entry<Object, Object>> iterator = this.getReadable$runtime_release().getMap$runtime_release().entrySet().iterator();
        while (iterator.hasNext()) {
            if (!(boolean)function1.invoke((Object)iterator.next())) {
                return false;
            }
        }
        return true;
    }
    
    public final boolean any$runtime_release(final Function1<? super Entry<? extends K, ? extends V>, Boolean> function1) {
        Intrinsics.checkNotNullParameter((Object)function1, "predicate");
        final Iterator<Entry<Object, Object>> iterator = this.getReadable$runtime_release().getMap$runtime_release().entrySet().iterator();
        while (iterator.hasNext()) {
            if (function1.invoke((Object)iterator.next())) {
                return true;
            }
        }
        return false;
    }
    
    @Override
    public void clear() {
        final StateRecord firstStateRecord = this.getFirstStateRecord();
        Intrinsics.checkNotNull((Object)firstStateRecord, "null cannot be cast to non-null type androidx.compose.runtime.snapshots.SnapshotStateMap.StateMapStateRecord<K of androidx.compose.runtime.snapshots.SnapshotStateMap, V of androidx.compose.runtime.snapshots.SnapshotStateMap>");
        final StateMapStateRecord stateMapStateRecord = SnapshotKt.current(firstStateRecord);
        stateMapStateRecord.getMap$runtime_release();
        final PersistentMap<Object, Object> persistentHashMap = ExtensionsKt.persistentHashMapOf();
        if (persistentHashMap != stateMapStateRecord.getMap$runtime_release()) {
            final StateRecord firstStateRecord2 = this.getFirstStateRecord();
            Intrinsics.checkNotNull((Object)firstStateRecord2, "null cannot be cast to non-null type androidx.compose.runtime.snapshots.SnapshotStateMap.StateMapStateRecord<K of androidx.compose.runtime.snapshots.SnapshotStateMap, V of androidx.compose.runtime.snapshots.SnapshotStateMap>");
            final StateRecord stateRecord = firstStateRecord2;
            SnapshotKt.getSnapshotInitializer();
            synchronized (SnapshotKt.getLock()) {
                final Snapshot current = Snapshot.Companion.getCurrent();
                final StateMapStateRecord stateMapStateRecord2 = SnapshotKt.writableRecord(stateRecord, this, current);
                synchronized (SnapshotStateMapKt.access$getSync$p()) {
                    stateMapStateRecord2.setMap$runtime_release(persistentHashMap);
                    stateMapStateRecord2.setModification$runtime_release(stateMapStateRecord2.getModification$runtime_release() + 1);
                    monitorexit(SnapshotStateMapKt.access$getSync$p());
                    monitorexit(SnapshotKt.getLock());
                    SnapshotKt.notifyWrite(current, this);
                }
            }
        }
    }
    
    @Override
    public boolean containsKey(final Object o) {
        return this.getReadable$runtime_release().getMap$runtime_release().containsKey(o);
    }
    
    @Override
    public boolean containsValue(final Object o) {
        return this.getReadable$runtime_release().getMap$runtime_release().containsValue(o);
    }
    
    @Override
    public final /* bridge */ Set<Entry<K, V>> entrySet() {
        return this.getEntries();
    }
    
    @Override
    public V get(final Object o) {
        return this.getReadable$runtime_release().getMap$runtime_release().get(o);
    }
    
    public final Map<K, V> getDebuggerDisplayValue() {
        final StateRecord firstStateRecord = this.getFirstStateRecord();
        Intrinsics.checkNotNull((Object)firstStateRecord, "null cannot be cast to non-null type androidx.compose.runtime.snapshots.SnapshotStateMap.StateMapStateRecord<K of androidx.compose.runtime.snapshots.SnapshotStateMap, V of androidx.compose.runtime.snapshots.SnapshotStateMap>");
        return SnapshotKt.current(firstStateRecord).getMap$runtime_release();
    }
    
    public Set<Entry<K, V>> getEntries() {
        return this.entries;
    }
    
    @Override
    public StateRecord getFirstStateRecord() {
        return this.firstStateRecord;
    }
    
    public Set<K> getKeys() {
        return this.keys;
    }
    
    public final int getModification$runtime_release() {
        return this.getReadable$runtime_release().getModification$runtime_release();
    }
    
    public final StateMapStateRecord<K, V> getReadable$runtime_release() {
        final StateRecord firstStateRecord = this.getFirstStateRecord();
        Intrinsics.checkNotNull((Object)firstStateRecord, "null cannot be cast to non-null type androidx.compose.runtime.snapshots.SnapshotStateMap.StateMapStateRecord<K of androidx.compose.runtime.snapshots.SnapshotStateMap, V of androidx.compose.runtime.snapshots.SnapshotStateMap>");
        return SnapshotKt.readable(firstStateRecord, this);
    }
    
    public int getSize() {
        return this.getReadable$runtime_release().getMap$runtime_release().size();
    }
    
    public Collection<V> getValues() {
        return this.values;
    }
    
    @Override
    public boolean isEmpty() {
        return this.getReadable$runtime_release().getMap$runtime_release().isEmpty();
    }
    
    @Override
    public final /* bridge */ Set<K> keySet() {
        return this.getKeys();
    }
    
    @Override
    public void prependStateRecord(final StateRecord stateRecord) {
        Intrinsics.checkNotNullParameter((Object)stateRecord, "value");
        this.firstStateRecord = stateRecord;
    }
    
    @Override
    public V put(final K k, final V v) {
        while (true) {
            Object o = SnapshotStateMapKt.access$getSync$p();
            synchronized (o) {
                final StateRecord firstStateRecord = this.getFirstStateRecord();
                Intrinsics.checkNotNull((Object)firstStateRecord, "null cannot be cast to non-null type androidx.compose.runtime.snapshots.SnapshotStateMap.StateMapStateRecord<K of androidx.compose.runtime.snapshots.SnapshotStateMap, V of androidx.compose.runtime.snapshots.SnapshotStateMap>");
                final StateMapStateRecord stateMapStateRecord = SnapshotKt.current(firstStateRecord);
                final PersistentMap map$runtime_release = stateMapStateRecord.getMap$runtime_release();
                final int modification$runtime_release = stateMapStateRecord.getModification$runtime_release();
                final Unit instance = Unit.INSTANCE;
                monitorexit(o);
                Intrinsics.checkNotNull((Object)map$runtime_release);
                final PersistentMap.Builder builder = map$runtime_release.builder();
                o = builder.put(k, v);
                final PersistentMap build = builder.build();
                if (!Intrinsics.areEqual((Object)build, (Object)map$runtime_release)) {
                    final StateRecord firstStateRecord2 = this.getFirstStateRecord();
                    Intrinsics.checkNotNull((Object)firstStateRecord2, "null cannot be cast to non-null type androidx.compose.runtime.snapshots.SnapshotStateMap.StateMapStateRecord<K of androidx.compose.runtime.snapshots.SnapshotStateMap, V of androidx.compose.runtime.snapshots.SnapshotStateMap>");
                    final StateRecord stateRecord = firstStateRecord2;
                    SnapshotKt.getSnapshotInitializer();
                    synchronized (SnapshotKt.getLock()) {
                        final Snapshot current = Snapshot.Companion.getCurrent();
                        final StateMapStateRecord stateMapStateRecord2 = SnapshotKt.writableRecord(stateRecord, this, current);
                        synchronized (SnapshotStateMapKt.access$getSync$p()) {
                            final int modification$runtime_release2 = stateMapStateRecord2.getModification$runtime_release();
                            boolean b = true;
                            if (modification$runtime_release2 == modification$runtime_release) {
                                stateMapStateRecord2.setMap$runtime_release(build);
                                stateMapStateRecord2.setModification$runtime_release(stateMapStateRecord2.getModification$runtime_release() + 1);
                            }
                            else {
                                b = false;
                            }
                            monitorexit(SnapshotStateMapKt.access$getSync$p());
                            monitorexit(SnapshotKt.getLock());
                            SnapshotKt.notifyWrite(current, this);
                            if (b) {
                                return (V)o;
                            }
                            continue;
                        }
                    }
                }
                return (V)o;
            }
        }
    }
    
    @Override
    public void putAll(final Map<? extends K, ? extends V> map) {
        Intrinsics.checkNotNullParameter((Object)map, "from");
        while (true) {
            Object map$runtime_release = SnapshotStateMapKt.access$getSync$p();
            synchronized (map$runtime_release) {
                final StateRecord firstStateRecord = this.getFirstStateRecord();
                Intrinsics.checkNotNull((Object)firstStateRecord, "null cannot be cast to non-null type androidx.compose.runtime.snapshots.SnapshotStateMap.StateMapStateRecord<K of androidx.compose.runtime.snapshots.SnapshotStateMap, V of androidx.compose.runtime.snapshots.SnapshotStateMap>");
                final StateMapStateRecord stateMapStateRecord = SnapshotKt.current(firstStateRecord);
                final PersistentMap map$runtime_release2 = stateMapStateRecord.getMap$runtime_release();
                final int modification$runtime_release = stateMapStateRecord.getModification$runtime_release();
                final Unit instance = Unit.INSTANCE;
                monitorexit(map$runtime_release);
                Intrinsics.checkNotNull((Object)map$runtime_release2);
                map$runtime_release = map$runtime_release2.builder();
                ((Map)map$runtime_release).putAll(map);
                final Unit instance2 = Unit.INSTANCE;
                map$runtime_release = ((PersistentMap.Builder)map$runtime_release).build();
                if (!Intrinsics.areEqual(map$runtime_release, (Object)map$runtime_release2)) {
                    final StateRecord firstStateRecord2 = this.getFirstStateRecord();
                    Intrinsics.checkNotNull((Object)firstStateRecord2, "null cannot be cast to non-null type androidx.compose.runtime.snapshots.SnapshotStateMap.StateMapStateRecord<K of androidx.compose.runtime.snapshots.SnapshotStateMap, V of androidx.compose.runtime.snapshots.SnapshotStateMap>");
                    final StateRecord stateRecord = firstStateRecord2;
                    SnapshotKt.getSnapshotInitializer();
                    final Object lock = SnapshotKt.getLock();
                    monitorenter(lock);
                    try {
                        final Snapshot current = Snapshot.Companion.getCurrent();
                        final StateMapStateRecord stateMapStateRecord2 = SnapshotKt.writableRecord(stateRecord, this, current);
                        synchronized (SnapshotStateMapKt.access$getSync$p()) {
                            final int modification$runtime_release2 = stateMapStateRecord2.getModification$runtime_release();
                            boolean b = true;
                            if (modification$runtime_release2 == modification$runtime_release) {
                                stateMapStateRecord2.setMap$runtime_release((PersistentMap)map$runtime_release);
                                stateMapStateRecord2.setModification$runtime_release(stateMapStateRecord2.getModification$runtime_release() + 1);
                            }
                            else {
                                b = false;
                            }
                            monitorexit(SnapshotStateMapKt.access$getSync$p());
                            monitorexit(lock);
                            SnapshotKt.notifyWrite(current, this);
                            if (b) {
                                return;
                            }
                            continue;
                        }
                    }
                    finally {}
                }
            }
        }
    }
    
    @Override
    public V remove(final Object o) {
        while (true) {
            Object o2 = SnapshotStateMapKt.access$getSync$p();
            synchronized (o2) {
                final StateRecord firstStateRecord = this.getFirstStateRecord();
                Intrinsics.checkNotNull((Object)firstStateRecord, "null cannot be cast to non-null type androidx.compose.runtime.snapshots.SnapshotStateMap.StateMapStateRecord<K of androidx.compose.runtime.snapshots.SnapshotStateMap, V of androidx.compose.runtime.snapshots.SnapshotStateMap>");
                final StateMapStateRecord stateMapStateRecord = SnapshotKt.current(firstStateRecord);
                final PersistentMap map$runtime_release = stateMapStateRecord.getMap$runtime_release();
                final int modification$runtime_release = stateMapStateRecord.getModification$runtime_release();
                final Unit instance = Unit.INSTANCE;
                monitorexit(o2);
                Intrinsics.checkNotNull((Object)map$runtime_release);
                final PersistentMap.Builder builder = map$runtime_release.builder();
                o2 = builder.remove(o);
                final PersistentMap build = builder.build();
                if (!Intrinsics.areEqual((Object)build, (Object)map$runtime_release)) {
                    final StateRecord firstStateRecord2 = this.getFirstStateRecord();
                    Intrinsics.checkNotNull((Object)firstStateRecord2, "null cannot be cast to non-null type androidx.compose.runtime.snapshots.SnapshotStateMap.StateMapStateRecord<K of androidx.compose.runtime.snapshots.SnapshotStateMap, V of androidx.compose.runtime.snapshots.SnapshotStateMap>");
                    final StateRecord stateRecord = firstStateRecord2;
                    SnapshotKt.getSnapshotInitializer();
                    synchronized (SnapshotKt.getLock()) {
                        final Snapshot current = Snapshot.Companion.getCurrent();
                        final StateMapStateRecord stateMapStateRecord2 = SnapshotKt.writableRecord(stateRecord, this, current);
                        synchronized (SnapshotStateMapKt.access$getSync$p()) {
                            final int modification$runtime_release2 = stateMapStateRecord2.getModification$runtime_release();
                            boolean b = true;
                            if (modification$runtime_release2 == modification$runtime_release) {
                                stateMapStateRecord2.setMap$runtime_release(build);
                                stateMapStateRecord2.setModification$runtime_release(stateMapStateRecord2.getModification$runtime_release() + 1);
                            }
                            else {
                                b = false;
                            }
                            monitorexit(SnapshotStateMapKt.access$getSync$p());
                            monitorexit(SnapshotKt.getLock());
                            SnapshotKt.notifyWrite(current, this);
                            if (b) {
                                return (V)o2;
                            }
                            continue;
                        }
                    }
                }
                return (V)o2;
            }
        }
    }
    
    public final boolean removeIf$runtime_release(final Function1<? super Entry<K, V>, Boolean> function1) {
        Intrinsics.checkNotNullParameter((Object)function1, "predicate");
        boolean b = false;
        while (true) {
            Object map$runtime_release = SnapshotStateMapKt.access$getSync$p();
            monitorenter(map$runtime_release);
            try {
                final StateRecord firstStateRecord = this.getFirstStateRecord();
                Intrinsics.checkNotNull((Object)firstStateRecord, "null cannot be cast to non-null type androidx.compose.runtime.snapshots.SnapshotStateMap.StateMapStateRecord<K of androidx.compose.runtime.snapshots.SnapshotStateMap, V of androidx.compose.runtime.snapshots.SnapshotStateMap>");
                final StateMapStateRecord stateMapStateRecord = SnapshotKt.current(firstStateRecord);
                final PersistentMap map$runtime_release2 = stateMapStateRecord.getMap$runtime_release();
                final int modification$runtime_release = stateMapStateRecord.getModification$runtime_release();
                final Unit instance = Unit.INSTANCE;
                InlineMarker.finallyStart(1);
                monitorexit(map$runtime_release);
                InlineMarker.finallyEnd(1);
                Intrinsics.checkNotNull((Object)map$runtime_release2);
                map$runtime_release = map$runtime_release2.builder();
                final Map map = (Map)map$runtime_release;
                final Iterator<Entry<K, V>> iterator = this.entrySet().iterator();
                boolean b2 = b;
                while (iterator.hasNext()) {
                    final Entry<Object, V> entry = (Entry<Object, V>)iterator.next();
                    if (function1.invoke((Object)entry)) {
                        map.remove(entry.getKey());
                        b2 = true;
                    }
                }
                final Unit instance2 = Unit.INSTANCE;
                map$runtime_release = ((PersistentMap.Builder)map$runtime_release).build();
                if (!Intrinsics.areEqual(map$runtime_release, (Object)map$runtime_release2)) {
                    final StateRecord firstStateRecord2 = this.getFirstStateRecord();
                    Intrinsics.checkNotNull((Object)firstStateRecord2, "null cannot be cast to non-null type androidx.compose.runtime.snapshots.SnapshotStateMap.StateMapStateRecord<K of androidx.compose.runtime.snapshots.SnapshotStateMap, V of androidx.compose.runtime.snapshots.SnapshotStateMap>");
                    final StateRecord stateRecord = firstStateRecord2;
                    SnapshotKt.getSnapshotInitializer();
                    final Object lock = SnapshotKt.getLock();
                    monitorenter(lock);
                    try {
                        final Snapshot current = Snapshot.Companion.getCurrent();
                        final StateMapStateRecord stateMapStateRecord2 = SnapshotKt.writableRecord(stateRecord, this, current);
                        final Object access$getSync$p = SnapshotStateMapKt.access$getSync$p();
                        monitorenter(access$getSync$p);
                        try {
                            boolean b3;
                            if (stateMapStateRecord2.getModification$runtime_release() == modification$runtime_release) {
                                stateMapStateRecord2.setMap$runtime_release((PersistentMap)map$runtime_release);
                                stateMapStateRecord2.setModification$runtime_release(stateMapStateRecord2.getModification$runtime_release() + 1);
                                b3 = true;
                            }
                            else {
                                b3 = false;
                            }
                            InlineMarker.finallyStart(1);
                            monitorexit(access$getSync$p);
                            InlineMarker.finallyEnd(1);
                            InlineMarker.finallyStart(1);
                            monitorexit(lock);
                            InlineMarker.finallyEnd(1);
                            SnapshotKt.notifyWrite(current, this);
                            b = b2;
                            if (b3) {
                                return b2;
                            }
                            continue;
                        }
                        finally {
                            InlineMarker.finallyStart(1);
                            monitorexit(access$getSync$p);
                            InlineMarker.finallyEnd(1);
                        }
                    }
                    finally {
                        InlineMarker.finallyStart(1);
                        monitorexit(lock);
                        InlineMarker.finallyEnd(1);
                    }
                }
                return b2;
            }
            finally {
                InlineMarker.finallyStart(1);
                monitorexit(map$runtime_release);
                InlineMarker.finallyEnd(1);
            }
        }
    }
    
    public final boolean removeValue$runtime_release(final V v) {
        while (true) {
            for (final Object next : this.entrySet()) {
                if (Intrinsics.areEqual(((Entry<K, Object>)next).getValue(), (Object)v)) {
                    final Object o = next;
                    final Entry<Object, V> entry = (Entry<Object, V>)o;
                    boolean b;
                    if (entry != null) {
                        this.remove(entry.getKey());
                        b = true;
                    }
                    else {
                        b = false;
                    }
                    return b;
                }
            }
            final Object o = null;
            continue;
        }
    }
    
    @Override
    public final /* bridge */ int size() {
        return this.getSize();
    }
    
    public final Map<K, V> toMap() {
        return this.getReadable$runtime_release().getMap$runtime_release();
    }
    
    @Override
    public final /* bridge */ Collection<V> values() {
        return this.getValues();
    }
    
    @Metadata(d1 = { "\u0000&\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0002\b\u0003\b\u0000\u0018\u0000*\u0004\b\u0002\u0010\u0001*\u0004\b\u0003\u0010\u00022\u00020\u0003B\u001b\b\u0000\u0012\u0012\u0010\u0004\u001a\u000e\u0012\u0004\u0012\u00028\u0002\u0012\u0004\u0012\u00028\u00030\u0005¢\u0006\u0002\u0010\u0006J\u0010\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u0003H\u0016J\b\u0010\u0013\u001a\u00020\u0003H\u0016R&\u0010\u0004\u001a\u000e\u0012\u0004\u0012\u00028\u0002\u0012\u0004\u0012\u00028\u00030\u0005X\u0080\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0007\u0010\b\"\u0004\b\t\u0010\u0006R\u001a\u0010\n\u001a\u00020\u000bX\u0080\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\f\u0010\r\"\u0004\b\u000e\u0010\u000f¨\u0006\u0014" }, d2 = { "Landroidx/compose/runtime/snapshots/SnapshotStateMap$StateMapStateRecord;", "K", "V", "Landroidx/compose/runtime/snapshots/StateRecord;", "map", "Landroidx/compose/runtime/external/kotlinx/collections/immutable/PersistentMap;", "(Landroidx/compose/runtime/external/kotlinx/collections/immutable/PersistentMap;)V", "getMap$runtime_release", "()Landroidx/compose/runtime/external/kotlinx/collections/immutable/PersistentMap;", "setMap$runtime_release", "modification", "", "getModification$runtime_release", "()I", "setModification$runtime_release", "(I)V", "assign", "", "value", "create", "runtime_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
    public static final class StateMapStateRecord<K, V> extends StateRecord
    {
        private PersistentMap<K, ? extends V> map;
        private int modification;
        
        public StateMapStateRecord(final PersistentMap<K, ? extends V> map) {
            Intrinsics.checkNotNullParameter((Object)map, "map");
            this.map = map;
        }
        
        @Override
        public void assign(final StateRecord stateRecord) {
            Intrinsics.checkNotNullParameter((Object)stateRecord, "value");
            final StateMapStateRecord stateMapStateRecord = (StateMapStateRecord)stateRecord;
            synchronized (SnapshotStateMapKt.access$getSync$p()) {
                this.map = stateMapStateRecord.map;
                this.modification = stateMapStateRecord.modification;
                final Unit instance = Unit.INSTANCE;
            }
        }
        
        @Override
        public StateRecord create() {
            return new StateMapStateRecord<Object, Object>(this.map);
        }
        
        public final PersistentMap<K, V> getMap$runtime_release() {
            return (PersistentMap<K, V>)this.map;
        }
        
        public final int getModification$runtime_release() {
            return this.modification;
        }
        
        public final void setMap$runtime_release(final PersistentMap<K, ? extends V> map) {
            Intrinsics.checkNotNullParameter((Object)map, "<set-?>");
            this.map = map;
        }
        
        public final void setModification$runtime_release(final int modification) {
            this.modification = modification;
        }
    }
}

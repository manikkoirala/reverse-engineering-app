// 
// Decompiled by Procyon v0.6.0
// 

package androidx.compose.runtime.snapshots;

import kotlin.Unit;
import java.util.ConcurrentModificationException;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.Intrinsics;
import java.util.Iterator;
import java.util.Map;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000>\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010(\n\u0002\u0010&\n\u0002\b\u000b\n\u0002\u0010\b\n\u0002\b\b\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\b\"\u0018\u0000*\u0004\b\u0000\u0010\u0001*\u0004\b\u0001\u0010\u00022\u00020\u0003B3\u0012\u0012\u0010\u0004\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u0005\u0012\u0018\u0010\u0006\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\b0\u0007¢\u0006\u0002\u0010\tJ\b\u0010\u001c\u001a\u00020\u001dH\u0004J\u0006\u0010\u001e\u001a\u00020\u001fJ%\u0010 \u001a\u0002H!\"\u0004\b\u0002\u0010!2\f\u0010\"\u001a\b\u0012\u0004\u0012\u0002H!0#H\u0084\b\u00f8\u0001\u0000¢\u0006\u0002\u0010$J\u0006\u0010%\u001a\u00020\u001dR(\u0010\n\u001a\u0010\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u0001\u0018\u00010\bX\u0084\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u000b\u0010\f\"\u0004\b\r\u0010\u000eR#\u0010\u0006\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\b0\u0007¢\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u0010R\u001d\u0010\u0004\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\u0012R\u001a\u0010\u0013\u001a\u00020\u0014X\u0084\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0015\u0010\u0016\"\u0004\b\u0017\u0010\u0018R(\u0010\u0019\u001a\u0010\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u0001\u0018\u00010\bX\u0084\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u001a\u0010\f\"\u0004\b\u001b\u0010\u000e\u0082\u0002\u0007\n\u0005\b\u009920\u0001¨\u0006&" }, d2 = { "Landroidx/compose/runtime/snapshots/StateMapMutableIterator;", "K", "V", "", "map", "Landroidx/compose/runtime/snapshots/SnapshotStateMap;", "iterator", "", "", "(Landroidx/compose/runtime/snapshots/SnapshotStateMap;Ljava/util/Iterator;)V", "current", "getCurrent", "()Ljava/util/Map$Entry;", "setCurrent", "(Ljava/util/Map$Entry;)V", "getIterator", "()Ljava/util/Iterator;", "getMap", "()Landroidx/compose/runtime/snapshots/SnapshotStateMap;", "modification", "", "getModification", "()I", "setModification", "(I)V", "next", "getNext", "setNext", "advance", "", "hasNext", "", "modify", "T", "block", "Lkotlin/Function0;", "(Lkotlin/jvm/functions/Function0;)Ljava/lang/Object;", "remove", "runtime_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
abstract class StateMapMutableIterator<K, V>
{
    private Map.Entry<? extends K, ? extends V> current;
    private final Iterator<Map.Entry<K, V>> iterator;
    private final SnapshotStateMap<K, V> map;
    private int modification;
    private Map.Entry<? extends K, ? extends V> next;
    
    public StateMapMutableIterator(final SnapshotStateMap<K, V> map, final Iterator<? extends Map.Entry<? extends K, ? extends V>> iterator) {
        Intrinsics.checkNotNullParameter((Object)map, "map");
        Intrinsics.checkNotNullParameter((Object)iterator, "iterator");
        this.map = map;
        this.iterator = (Iterator<Map.Entry<K, V>>)iterator;
        this.modification = map.getModification$runtime_release();
        this.advance();
    }
    
    public static final /* synthetic */ int access$getModification(final StateMapMutableIterator stateMapMutableIterator) {
        return stateMapMutableIterator.modification;
    }
    
    public static final /* synthetic */ void access$setModification(final StateMapMutableIterator stateMapMutableIterator, final int modification) {
        stateMapMutableIterator.modification = modification;
    }
    
    protected final void advance() {
        this.current = this.next;
        Map.Entry next;
        if (this.iterator.hasNext()) {
            next = this.iterator.next();
        }
        else {
            next = null;
        }
        this.next = next;
    }
    
    protected final Map.Entry<K, V> getCurrent() {
        return (Map.Entry<K, V>)this.current;
    }
    
    public final Iterator<Map.Entry<K, V>> getIterator() {
        return this.iterator;
    }
    
    public final SnapshotStateMap<K, V> getMap() {
        return this.map;
    }
    
    protected final int getModification() {
        return this.modification;
    }
    
    protected final Map.Entry<K, V> getNext() {
        return (Map.Entry<K, V>)this.next;
    }
    
    public final boolean hasNext() {
        return this.next != null;
    }
    
    protected final <T> T modify(final Function0<? extends T> function0) {
        Intrinsics.checkNotNullParameter((Object)function0, "block");
        if (this.getMap().getModification$runtime_release() == access$getModification(this)) {
            final Object invoke = function0.invoke();
            access$setModification(this, this.getMap().getModification$runtime_release());
            return (T)invoke;
        }
        throw new ConcurrentModificationException();
    }
    
    public final void remove() {
        if (this.getMap().getModification$runtime_release() != access$getModification(this)) {
            throw new ConcurrentModificationException();
        }
        final Map.Entry<? extends K, ? extends V> current = this.current;
        if (current != null) {
            this.map.remove(current.getKey());
            this.current = null;
            final Unit instance = Unit.INSTANCE;
            access$setModification(this, this.getMap().getModification$runtime_release());
            return;
        }
        throw new IllegalStateException();
    }
    
    protected final void setCurrent(final Map.Entry<? extends K, ? extends V> current) {
        this.current = current;
    }
    
    protected final void setModification(final int modification) {
        this.modification = modification;
    }
    
    protected final void setNext(final Map.Entry<? extends K, ? extends V> next) {
        this.next = next;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.compose.runtime.snapshots;

import java.util.Collection;
import java.util.Set;
import java.util.HashSet;
import java.util.Map;
import kotlin.jvm.internal.Intrinsics;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000>\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\b\u0000\u0018\u00002\u00020\u0001BI\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0014\u0010\u0006\u001a\u0010\u0012\u0004\u0012\u00020\b\u0012\u0004\u0012\u00020\t\u0018\u00010\u0007\u0012\u0014\u0010\n\u001a\u0010\u0012\u0004\u0012\u00020\b\u0012\u0004\u0012\u00020\t\u0018\u00010\u0007\u0012\u0006\u0010\u000b\u001a\u00020\u0001¢\u0006\u0002\u0010\fJ\b\u0010\u0015\u001a\u00020\u0016H\u0016J\b\u0010\u0017\u001a\u00020\tH\u0002J\b\u0010\u0018\u001a\u00020\tH\u0016R\u000e\u0010\r\u001a\u00020\u000eX\u0082\u000e¢\u0006\u0002\n\u0000R\u0011\u0010\u000b\u001a\u00020\u0001¢\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u0010R\u0014\u0010\u0011\u001a\u00020\u00128VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u0013\u0010\u0014¨\u0006\u0019" }, d2 = { "Landroidx/compose/runtime/snapshots/NestedMutableSnapshot;", "Landroidx/compose/runtime/snapshots/MutableSnapshot;", "id", "", "invalid", "Landroidx/compose/runtime/snapshots/SnapshotIdSet;", "readObserver", "Lkotlin/Function1;", "", "", "writeObserver", "parent", "(ILandroidx/compose/runtime/snapshots/SnapshotIdSet;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Landroidx/compose/runtime/snapshots/MutableSnapshot;)V", "deactivated", "", "getParent", "()Landroidx/compose/runtime/snapshots/MutableSnapshot;", "root", "Landroidx/compose/runtime/snapshots/Snapshot;", "getRoot", "()Landroidx/compose/runtime/snapshots/Snapshot;", "apply", "Landroidx/compose/runtime/snapshots/SnapshotApplyResult;", "deactivate", "dispose", "runtime_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
public final class NestedMutableSnapshot extends MutableSnapshot
{
    private boolean deactivated;
    private final MutableSnapshot parent;
    
    public NestedMutableSnapshot(final int n, final SnapshotIdSet set, final Function1<Object, Unit> function1, final Function1<Object, Unit> function2, final MutableSnapshot parent) {
        Intrinsics.checkNotNullParameter((Object)set, "invalid");
        Intrinsics.checkNotNullParameter((Object)parent, "parent");
        super(n, set, function1, function2);
        (this.parent = parent).nestedActivated$runtime_release(this);
    }
    
    private final void deactivate() {
        if (!this.deactivated) {
            this.deactivated = true;
            this.parent.nestedDeactivated$runtime_release(this);
        }
    }
    
    @Override
    public SnapshotApplyResult apply() {
        if (!this.parent.getApplied$runtime_release()) {
            if (!this.parent.getDisposed$runtime_release()) {
                final Set<StateObject> modified$runtime_release = this.getModified$runtime_release();
                final int id = this.getId();
                Map access$optimisticMerges;
                if (modified$runtime_release != null) {
                    final MutableSnapshot parent = this.parent;
                    access$optimisticMerges = SnapshotKt.access$optimisticMerges(parent, this, parent.getInvalid$runtime_release());
                }
                else {
                    access$optimisticMerges = null;
                }
                synchronized (SnapshotKt.getLock()) {
                    SnapshotKt.access$validateOpen(this);
                    if (modified$runtime_release != null && modified$runtime_release.size()) {
                        final SnapshotApplyResult innerApplyLocked$runtime_release = this.innerApplyLocked$runtime_release(this.parent.getId(), access$optimisticMerges, this.parent.getInvalid$runtime_release());
                        if (!Intrinsics.areEqual((Object)innerApplyLocked$runtime_release, (Object)SnapshotApplyResult.Success.INSTANCE)) {
                            return innerApplyLocked$runtime_release;
                        }
                        Set<StateObject> modified$runtime_release2;
                        if ((modified$runtime_release2 = this.parent.getModified$runtime_release()) == null) {
                            final HashSet<StateObject> set = new HashSet<StateObject>();
                            this.parent.setModified(set);
                            modified$runtime_release2 = set;
                        }
                        modified$runtime_release2.addAll(modified$runtime_release);
                    }
                    else {
                        this.closeAndReleasePinning$runtime_release();
                    }
                    if (this.parent.getId() < id) {
                        this.parent.advance$runtime_release();
                    }
                    final MutableSnapshot parent2 = this.parent;
                    parent2.setInvalid$runtime_release(parent2.getInvalid$runtime_release().clear(id).andNot(this.getPreviousIds$runtime_release()));
                    this.parent.recordPrevious$runtime_release(id);
                    this.parent.recordPreviousPinnedSnapshot$runtime_release(this.takeoverPinnedSnapshot$runtime_release());
                    this.parent.recordPreviousList$runtime_release(this.getPreviousIds$runtime_release());
                    this.parent.recordPreviousPinnedSnapshots$runtime_release(this.getPreviousPinnedSnapshots$runtime_release());
                    final Unit instance = Unit.INSTANCE;
                    monitorexit(SnapshotKt.getLock());
                    this.setApplied$runtime_release(true);
                    this.deactivate();
                    return SnapshotApplyResult.Success.INSTANCE;
                }
            }
        }
        return new SnapshotApplyResult.Failure(this);
    }
    
    @Override
    public void dispose() {
        if (!this.getDisposed$runtime_release()) {
            super.dispose();
            this.deactivate();
        }
    }
    
    public final MutableSnapshot getParent() {
        return this.parent;
    }
    
    @Override
    public Snapshot getRoot() {
        return this.parent.getRoot();
    }
}

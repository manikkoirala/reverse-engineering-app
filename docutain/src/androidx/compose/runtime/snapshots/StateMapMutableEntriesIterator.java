// 
// Decompiled by Procyon v0.6.0
// 

package androidx.compose.runtime.snapshots;

import kotlin.jvm.internal.Intrinsics;
import kotlin.Metadata;
import kotlin.jvm.internal.markers.KMutableIterator;
import java.util.Map;
import java.util.Iterator;

@Metadata(d1 = { "\u0000(\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010)\n\u0002\u0010'\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010(\n\u0002\u0010&\n\u0002\b\u0003\b\u0002\u0018\u0000*\u0004\b\u0000\u0010\u0001*\u0004\b\u0001\u0010\u00022\u000e\u0012\u0004\u0012\u0002H\u0001\u0012\u0004\u0012\u0002H\u00020\u00032\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u0002H\u0001\u0012\u0004\u0012\u0002H\u00020\u00050\u0004B3\u0012\u0012\u0010\u0006\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u0007\u0012\u0018\u0010\b\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\n0\t¢\u0006\u0002\u0010\u000bJ\u0015\u0010\f\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u0005H\u0096\u0002¨\u0006\r" }, d2 = { "Landroidx/compose/runtime/snapshots/StateMapMutableEntriesIterator;", "K", "V", "Landroidx/compose/runtime/snapshots/StateMapMutableIterator;", "", "", "map", "Landroidx/compose/runtime/snapshots/SnapshotStateMap;", "iterator", "", "", "(Landroidx/compose/runtime/snapshots/SnapshotStateMap;Ljava/util/Iterator;)V", "next", "runtime_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
final class StateMapMutableEntriesIterator<K, V> extends StateMapMutableIterator<K, V> implements Iterator<Map.Entry<K, V>>, KMutableIterator
{
    public StateMapMutableEntriesIterator(final SnapshotStateMap<K, V> snapshotStateMap, final Iterator<? extends Map.Entry<? extends K, ? extends V>> iterator) {
        Intrinsics.checkNotNullParameter((Object)snapshotStateMap, "map");
        Intrinsics.checkNotNullParameter((Object)iterator, "iterator");
        super(snapshotStateMap, iterator);
    }
    
    @Override
    public Map.Entry<K, V> next() {
        this.advance();
        if (this.getCurrent() != null) {
            return (Map.Entry)new StateMapMutableEntriesIterator$next.StateMapMutableEntriesIterator$next$1(this);
        }
        throw new IllegalStateException();
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.compose.runtime.snapshots;

import java.util.Map;
import androidx.compose.runtime.SnapshotMutationPolicy;
import java.util.Iterator;
import kotlin.jvm.internal.DefaultConstructorMarker;
import androidx.compose.runtime.collection.IdentityArrayMap;
import java.util.HashMap;
import androidx.compose.runtime.collection.IdentityArraySet;
import androidx.compose.runtime.DerivedState;
import androidx.compose.runtime.collection.IdentityScopeMap;
import kotlin.ReplaceWith;
import kotlin.Deprecated;
import androidx.compose.runtime.State;
import androidx.compose.runtime.SnapshotStateKt;
import androidx.compose.runtime.collection.IdentityArrayIntMap;
import androidx.compose.runtime.ComposerKt;
import kotlin.jvm.internal.InlineMarker;
import kotlin.jvm.internal.TypeIntrinsics;
import kotlin.KotlinNothingValueException;
import java.util.List;
import kotlin.collections.CollectionsKt;
import java.util.Collection;
import androidx.camera.view.PreviewView$1$$ExternalSyntheticBackportWithForwarding0;
import kotlin.jvm.internal.Intrinsics;
import java.util.concurrent.atomic.AtomicReference;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import androidx.compose.runtime.collection.MutableVector;
import kotlin.Unit;
import java.util.Set;
import kotlin.jvm.functions.Function2;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000X\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0010\"\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0016\n\u0002\u0010\u0001\n\u0002\b\u0006\b\u0007\u0018\u00002\u00020\u0001:\u00014B.\u0012'\u0010\u0002\u001a#\u0012\u0019\u0012\u0017\u0012\u0004\u0012\u00020\u00050\u0004¢\u0006\f\b\u0006\u0012\b\b\u0007\u0012\u0004\b\b(\b\u0012\u0004\u0012\u00020\u00050\u0003¢\u0006\u0002\u0010\tJ\u0016\u0010\u001b\u001a\u00020\u00052\f\u0010\u001c\u001a\b\u0012\u0004\u0012\u00020\u00010\fH\u0002J\u0006\u0010\u001d\u001a\u00020\u0005J\u000e\u0010\u001d\u001a\u00020\u00052\u0006\u0010\u001e\u001a\u00020\u0001J)\u0010\u001f\u001a\u00020\u00052!\u0010 \u001a\u001d\u0012\u0013\u0012\u00110\u0001¢\u0006\f\b\u0006\u0012\b\b\u0007\u0012\u0004\b\b(\u001e\u0012\u0004\u0012\u00020\u00130\u0003J\b\u0010!\u001a\u00020\u0013H\u0002J&\u0010\"\u001a\u00020\u0011\"\b\b\u0000\u0010#*\u00020\u00012\u0012\u0010$\u001a\u000e\u0012\u0004\u0012\u0002H#\u0012\u0004\u0012\u00020\u00050\u0003H\u0002J\u001d\u0010%\u001a\u00020\u00052\u0012\u0010&\u001a\u000e\u0012\u0004\u0012\u00020\u0011\u0012\u0004\u0012\u00020\u00050\u0003H\u0082\bJ\u001c\u0010'\u001a\u00020\u00052\f\u0010(\u001a\b\u0012\u0004\u0012\u00020\u00010\f2\u0006\u0010)\u001a\u00020\rJ?\u0010*\u001a\u00020\u0005\"\b\b\u0000\u0010#*\u00020\u00012\u0006\u0010\u001e\u001a\u0002H#2\u0012\u0010+\u001a\u000e\u0012\u0004\u0012\u0002H#\u0012\u0004\u0012\u00020\u00050\u00032\f\u0010&\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004¢\u0006\u0002\u0010,J\u0010\u0010-\u001a\n\u0012\u0004\u0012\u00020\u0001\u0018\u00010\fH\u0002J\b\u0010.\u001a\u00020/H\u0002J\b\u00100\u001a\u00020\u0005H\u0002J\u0006\u00101\u001a\u00020\u0005J\u0006\u00102\u001a\u00020\u0005J\u0016\u00103\u001a\u00020\u00052\f\u0010&\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004H\u0007R&\u0010\n\u001a\u001a\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00010\f\u0012\u0004\u0012\u00020\r\u0012\u0004\u0012\u00020\u00050\u000bX\u0082\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\u000e\u001a\u0004\u0018\u00010\u000fX\u0082\u000e¢\u0006\u0002\n\u0000R\u0010\u0010\u0010\u001a\u0004\u0018\u00010\u0011X\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0013X\u0082\u000e¢\u0006\u0002\n\u0000R\u0014\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\u00110\u0015X\u0082\u0004¢\u0006\u0002\n\u0000R/\u0010\u0002\u001a#\u0012\u0019\u0012\u0017\u0012\u0004\u0012\u00020\u00050\u0004¢\u0006\f\b\u0006\u0012\b\b\u0007\u0012\u0004\b\b(\b\u0012\u0004\u0012\u00020\u00050\u0003X\u0082\u0004¢\u0006\u0002\n\u0000R\"\u0010\u0016\u001a\u0016\u0012\u0006\u0012\u0004\u0018\u00010\u00010\u0017j\n\u0012\u0006\u0012\u0004\u0018\u00010\u0001`\u0018X\u0082\u0004¢\u0006\u0002\n\u0000R\u001a\u0010\u0019\u001a\u000e\u0012\u0004\u0012\u00020\u0001\u0012\u0004\u0012\u00020\u00050\u0003X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u001a\u001a\u00020\u0013X\u0082\u000e¢\u0006\u0002\n\u0000¨\u00065" }, d2 = { "Landroidx/compose/runtime/snapshots/SnapshotStateObserver;", "", "onChangedExecutor", "Lkotlin/Function1;", "Lkotlin/Function0;", "", "Lkotlin/ParameterName;", "name", "callback", "(Lkotlin/jvm/functions/Function1;)V", "applyObserver", "Lkotlin/Function2;", "", "Landroidx/compose/runtime/snapshots/Snapshot;", "applyUnsubscribe", "Landroidx/compose/runtime/snapshots/ObserverHandle;", "currentMap", "Landroidx/compose/runtime/snapshots/SnapshotStateObserver$ObservedScopeMap;", "isPaused", "", "observedScopeMaps", "Landroidx/compose/runtime/collection/MutableVector;", "pendingChanges", "Ljava/util/concurrent/atomic/AtomicReference;", "Landroidx/compose/runtime/AtomicReference;", "readObserver", "sendingNotifications", "addChanges", "set", "clear", "scope", "clearIf", "predicate", "drainChanges", "ensureMap", "T", "onChanged", "forEachScopeMap", "block", "notifyChanges", "changes", "snapshot", "observeReads", "onValueChangedForScope", "(Ljava/lang/Object;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;)V", "removeChanges", "report", "", "sendNotifications", "start", "stop", "withNoObservations", "ObservedScopeMap", "runtime_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
public final class SnapshotStateObserver
{
    public static final int $stable = 8;
    private final Function2<Set<?>, Snapshot, Unit> applyObserver;
    private ObserverHandle applyUnsubscribe;
    private ObservedScopeMap currentMap;
    private boolean isPaused;
    private final MutableVector<ObservedScopeMap> observedScopeMaps;
    private final Function1<Function0<Unit>, Unit> onChangedExecutor;
    private final AtomicReference<Object> pendingChanges;
    private final Function1<Object, Unit> readObserver;
    private boolean sendingNotifications;
    
    public SnapshotStateObserver(final Function1<? super Function0<Unit>, Unit> onChangedExecutor) {
        Intrinsics.checkNotNullParameter((Object)onChangedExecutor, "onChangedExecutor");
        this.onChangedExecutor = (Function1<Function0<Unit>, Unit>)onChangedExecutor;
        this.pendingChanges = new AtomicReference<Object>(null);
        this.applyObserver = (Function2<Set<?>, Snapshot, Unit>)new SnapshotStateObserver$applyObserver.SnapshotStateObserver$applyObserver$1(this);
        this.readObserver = (Function1<Object, Unit>)new SnapshotStateObserver$readObserver.SnapshotStateObserver$readObserver$1(this);
        this.observedScopeMaps = new MutableVector<ObservedScopeMap>(new ObservedScopeMap[16], 0);
    }
    
    private final void addChanges(final Set<?> set) {
        Set value;
        Collection collection;
        do {
            value = this.pendingChanges.get();
            if (value == null) {
                collection = set;
            }
            else if (value instanceof Set) {
                collection = CollectionsKt.listOf((Object[])new Set[] { value, set });
            }
            else {
                if (!(value instanceof List)) {
                    this.report();
                    throw new KotlinNothingValueException();
                }
                collection = CollectionsKt.plus((Collection)value, (Iterable)CollectionsKt.listOf((Object)set));
            }
        } while (!PreviewView$1$$ExternalSyntheticBackportWithForwarding0.m(this.pendingChanges, value, collection));
    }
    
    private final boolean drainChanges() {
        Object o = this.observedScopeMaps;
        synchronized (o) {
            final boolean sendingNotifications = this.sendingNotifications;
            monitorexit(o);
            if (sendingNotifications) {
                return false;
            }
            int n = 0;
            while (true) {
                o = this.removeChanges();
                if (o == null) {
                    break;
                }
                synchronized (this.observedScopeMaps) {
                    final MutableVector<ObservedScopeMap> observedScopeMaps = this.observedScopeMaps;
                    final int size = observedScopeMaps.getSize();
                    int n2 = n;
                    if (size > 0) {
                        final Object[] content = observedScopeMaps.getContent();
                        int n3 = 0;
                        int n4 = n;
                        int n5;
                        do {
                            n = ((((ObservedScopeMap)content[n3]).recordInvalidation((Set<?>)o) || n4 != 0) ? 1 : 0);
                            n5 = n3 + 1;
                            n4 = n;
                        } while ((n3 = n5) < size);
                        n2 = n;
                    }
                    final Unit instance = Unit.INSTANCE;
                    monitorexit(this.observedScopeMaps);
                    n = n2;
                }
            }
            return n != 0;
        }
    }
    
    private final <T> ObservedScopeMap ensureMap(final Function1<? super T, Unit> function1) {
        final MutableVector<ObservedScopeMap> observedScopeMaps = this.observedScopeMaps;
        final int size = observedScopeMaps.getSize();
        Object o = null;
        Label_0074: {
            if (size > 0) {
                final Object[] content = observedScopeMaps.getContent();
                int n = 0;
                do {
                    o = content[n];
                    if (((ObservedScopeMap)o).getOnChanged() == function1) {
                        break Label_0074;
                    }
                } while (++n < size);
            }
            o = null;
        }
        final ObservedScopeMap observedScopeMap = (ObservedScopeMap)o;
        if (observedScopeMap == null) {
            Intrinsics.checkNotNull((Object)function1, "null cannot be cast to non-null type kotlin.Function1<kotlin.Any, kotlin.Unit>");
            final ObservedScopeMap observedScopeMap2 = new ObservedScopeMap((Function1<Object, Unit>)TypeIntrinsics.beforeCheckcastToFunctionOfArity((Object)function1, 1));
            this.observedScopeMaps.add(observedScopeMap2);
            return observedScopeMap2;
        }
        return observedScopeMap;
    }
    
    private final void forEachScopeMap(final Function1<? super ObservedScopeMap, Unit> function1) {
        final MutableVector<ObservedScopeMap> observedScopeMaps = this.observedScopeMaps;
        monitorenter(observedScopeMaps);
        try {
            final MutableVector<ObservedScopeMap> observedScopeMaps2 = this.observedScopeMaps;
            final int size = observedScopeMaps2.getSize();
            if (size > 0) {
                int n = 0;
                final Object[] content = observedScopeMaps2.getContent();
                do {
                    function1.invoke(content[n]);
                } while (++n < size);
            }
            final Unit instance = Unit.INSTANCE;
        }
        finally {
            InlineMarker.finallyStart(1);
            monitorexit(observedScopeMaps);
            InlineMarker.finallyEnd(1);
        }
    }
    
    private final Set<Object> removeChanges() {
        Set value;
        Object o;
        Set set;
        do {
            value = this.pendingChanges.get();
            final Object o2 = null;
            o = null;
            if (value == null) {
                return null;
            }
            if (value instanceof Set) {
                set = value;
                o = o2;
            }
            else {
                if (!(value instanceof List)) {
                    this.report();
                    throw new KotlinNothingValueException();
                }
                final List list = (List)value;
                set = (Set)list.get(0);
                if (list.size() == 2) {
                    o = list.get(1);
                }
                else {
                    if (list.size() <= 2) {
                        continue;
                    }
                    o = list.subList(1, list.size());
                }
            }
        } while (!PreviewView$1$$ExternalSyntheticBackportWithForwarding0.m(this.pendingChanges, value, o));
        return set;
    }
    
    private final Void report() {
        ComposerKt.composeRuntimeError("Unexpected notification");
        throw new KotlinNothingValueException();
    }
    
    private final void sendNotifications() {
        this.onChangedExecutor.invoke((Object)new SnapshotStateObserver$sendNotifications.SnapshotStateObserver$sendNotifications$1(this));
    }
    
    public final void clear() {
        synchronized (this.observedScopeMaps) {
            final MutableVector<ObservedScopeMap> observedScopeMaps = this.observedScopeMaps;
            final int size = observedScopeMaps.getSize();
            if (size > 0) {
                int n = 0;
                final Object[] content = observedScopeMaps.getContent();
                do {
                    ((ObservedScopeMap)content[n]).clear();
                } while (++n < size);
            }
            final Unit instance = Unit.INSTANCE;
        }
    }
    
    public final void clear(final Object o) {
        Intrinsics.checkNotNullParameter(o, "scope");
        synchronized (this.observedScopeMaps) {
            final MutableVector<ObservedScopeMap> observedScopeMaps = this.observedScopeMaps;
            final int size = observedScopeMaps.getSize();
            if (size > 0) {
                int n = 0;
                final Object[] content = observedScopeMaps.getContent();
                do {
                    ((ObservedScopeMap)content[n]).clearScopeObservations(o);
                } while (++n < size);
            }
            final Unit instance = Unit.INSTANCE;
        }
    }
    
    public final void clearIf(final Function1<Object, Boolean> function1) {
        Intrinsics.checkNotNullParameter((Object)function1, "predicate");
        synchronized (this.observedScopeMaps) {
            final MutableVector<ObservedScopeMap> observedScopeMaps = this.observedScopeMaps;
            final int size = observedScopeMaps.getSize();
            if (size > 0) {
                int n = 0;
                final Object[] content = observedScopeMaps.getContent();
                do {
                    ((ObservedScopeMap)content[n]).removeScopeIf(function1);
                } while (++n < size);
            }
            final Unit instance = Unit.INSTANCE;
        }
    }
    
    public final void notifyChanges(final Set<?> set, final Snapshot snapshot) {
        Intrinsics.checkNotNullParameter((Object)set, "changes");
        Intrinsics.checkNotNullParameter((Object)snapshot, "snapshot");
        this.applyObserver.invoke((Object)set, (Object)snapshot);
    }
    
    public final <T> void observeReads(final T t, final Function1<? super T, Unit> function1, final Function0<Unit> function2) {
        Intrinsics.checkNotNullParameter((Object)t, "scope");
        Intrinsics.checkNotNullParameter((Object)function1, "onValueChangedForScope");
        Intrinsics.checkNotNullParameter((Object)function2, "block");
        Object currentMap = this.observedScopeMaps;
        synchronized (currentMap) {
            final ObservedScopeMap ensureMap = this.ensureMap((kotlin.jvm.functions.Function1<? super Object, Unit>)function1);
            monitorexit(currentMap);
            final boolean isPaused = this.isPaused;
            currentMap = this.currentMap;
            try {
                this.isPaused = false;
                this.currentMap = ensureMap;
                final Object access$getCurrentScope$p = ObservedScopeMap.access$getCurrentScope$p(ensureMap);
                final IdentityArrayIntMap access$getCurrentScopeReads$p = ObservedScopeMap.access$getCurrentScopeReads$p(ensureMap);
                final int access$getCurrentToken$p = ObservedScopeMap.access$getCurrentToken$p(ensureMap);
                ObservedScopeMap.access$setCurrentScope$p(ensureMap, t);
                ObservedScopeMap.access$setCurrentScopeReads$p(ensureMap, (IdentityArrayIntMap)ObservedScopeMap.access$getScopeToValues$p(ensureMap).get(t));
                if (ObservedScopeMap.access$getCurrentToken$p(ensureMap) == -1) {
                    ObservedScopeMap.access$setCurrentToken$p(ensureMap, SnapshotKt.currentSnapshot().getId());
                }
                SnapshotStateKt.observeDerivedStateRecalculations((Function1<? super State<?>, Unit>)ensureMap.getDerivedStateEnterObserver(), (Function1<? super State<?>, Unit>)ensureMap.getDerivedStateExitObserver(), (kotlin.jvm.functions.Function0<?>)new SnapshotStateObserver$observeReads$1.SnapshotStateObserver$observeReads$1$1(this, (Function0)function2));
                final Object access$getCurrentScope$p2 = ObservedScopeMap.access$getCurrentScope$p(ensureMap);
                Intrinsics.checkNotNull(access$getCurrentScope$p2);
                ensureMap.clearObsoleteStateReads(access$getCurrentScope$p2);
                ObservedScopeMap.access$setCurrentScope$p(ensureMap, access$getCurrentScope$p);
                ObservedScopeMap.access$setCurrentScopeReads$p(ensureMap, access$getCurrentScopeReads$p);
                ObservedScopeMap.access$setCurrentToken$p(ensureMap, access$getCurrentToken$p);
            }
            finally {
                this.currentMap = (ObservedScopeMap)currentMap;
                this.isPaused = isPaused;
            }
        }
    }
    
    public final void start() {
        this.applyUnsubscribe = Snapshot.Companion.registerApplyObserver(this.applyObserver);
    }
    
    public final void stop() {
        final ObserverHandle applyUnsubscribe = this.applyUnsubscribe;
        if (applyUnsubscribe != null) {
            applyUnsubscribe.dispose();
        }
    }
    
    @Deprecated(message = "Replace with Snapshot.withoutReadObservation()", replaceWith = @ReplaceWith(expression = "Snapshot.withoutReadObservation(block)", imports = { "androidx.compose.runtime.snapshots.Snapshot" }))
    public final void withNoObservations(final Function0<Unit> function0) {
        Intrinsics.checkNotNullParameter((Object)function0, "block");
        final boolean isPaused = this.isPaused;
        this.isPaused = true;
        try {
            function0.invoke();
        }
        finally {
            this.isPaused = isPaused;
        }
    }
    
    @Metadata(d1 = { "\u0000l\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\"\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0002\u0018\u00002\u00020\u0001B\u0019\u0012\u0012\u0010\u0002\u001a\u000e\u0012\u0004\u0012\u00020\u0001\u0012\u0004\u0012\u00020\u00040\u0003¢\u0006\u0002\u0010\u0005J\u0006\u0010\u001e\u001a\u00020\u0004J\u0010\u0010\u001f\u001a\u00020\u00042\u0006\u0010 \u001a\u00020\u0001H\u0002J\u000e\u0010!\u001a\u00020\u00042\u0006\u0010 \u001a\u00020\u0001J\u0006\u0010\"\u001a\u00020\u0004J\"\u0010#\u001a\u00020\u00042\u0006\u0010 \u001a\u00020\u00012\f\u0010$\u001a\b\u0012\u0004\u0012\u00020\u00040%H\u0086\b\u00f8\u0001\u0000J\u0014\u0010&\u001a\u00020'2\f\u0010(\u001a\b\u0012\u0004\u0012\u00020\u00010)J\u000e\u0010*\u001a\u00020\u00042\u0006\u0010+\u001a\u00020\u0001J\u0018\u0010,\u001a\u00020\u00042\u0006\u0010 \u001a\u00020\u00012\u0006\u0010+\u001a\u00020\u0001H\u0002J)\u0010-\u001a\u00020\u00042!\u0010.\u001a\u001d\u0012\u0013\u0012\u00110\u0001¢\u0006\f\b/\u0012\b\b0\u0012\u0004\b\b( \u0012\u0004\u0012\u00020'0\u0003R\u0010\u0010\u0006\u001a\u0004\u0018\u00010\u0001X\u0082\u000e¢\u0006\u0002\n\u0000R\u0010\u0010\u0007\u001a\u0004\u0018\u00010\bX\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082\u000e¢\u0006\u0002\n\u0000R\u0018\u0010\u000b\u001a\f\u0012\b\u0012\u0006\u0012\u0002\b\u00030\r0\fX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\nX\u0082\u000e¢\u0006\u0002\n\u0000R!\u0010\u000f\u001a\u0012\u0012\b\u0012\u0006\u0012\u0002\b\u00030\u0010\u0012\u0004\u0012\u00020\u00040\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\u0012R!\u0010\u0013\u001a\u0012\u0012\b\u0012\u0006\u0012\u0002\b\u00030\u0010\u0012\u0004\u0012\u00020\u00040\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0014\u0010\u0012R\u0014\u0010\u0015\u001a\b\u0012\u0004\u0012\u00020\u00010\u0016X\u0082\u0004¢\u0006\u0002\n\u0000R\u001d\u0010\u0002\u001a\u000e\u0012\u0004\u0012\u00020\u0001\u0012\u0004\u0012\u00020\u00040\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0017\u0010\u0012R6\u0010\u0018\u001a*\u0012\b\u0012\u0006\u0012\u0002\b\u00030\r\u0012\u0006\u0012\u0004\u0018\u00010\u00010\u0019j\u0014\u0012\b\u0012\u0006\u0012\u0002\b\u00030\r\u0012\u0006\u0012\u0004\u0018\u00010\u0001`\u001aX\u0082\u0004¢\u0006\u0002\n\u0000R\u001a\u0010\u001b\u001a\u000e\u0012\u0004\u0012\u00020\u0001\u0012\u0004\u0012\u00020\b0\u001cX\u0082\u0004¢\u0006\u0002\n\u0000R\u0014\u0010\u001d\u001a\b\u0012\u0004\u0012\u00020\u00010\fX\u0082\u0004¢\u0006\u0002\n\u0000\u0082\u0002\u0007\n\u0005\b\u009920\u0001¨\u00061" }, d2 = { "Landroidx/compose/runtime/snapshots/SnapshotStateObserver$ObservedScopeMap;", "", "onChanged", "Lkotlin/Function1;", "", "(Lkotlin/jvm/functions/Function1;)V", "currentScope", "currentScopeReads", "Landroidx/compose/runtime/collection/IdentityArrayIntMap;", "currentToken", "", "dependencyToDerivedStates", "Landroidx/compose/runtime/collection/IdentityScopeMap;", "Landroidx/compose/runtime/DerivedState;", "deriveStateScopeCount", "derivedStateEnterObserver", "Landroidx/compose/runtime/State;", "getDerivedStateEnterObserver", "()Lkotlin/jvm/functions/Function1;", "derivedStateExitObserver", "getDerivedStateExitObserver", "invalidated", "Landroidx/compose/runtime/collection/IdentityArraySet;", "getOnChanged", "recordedDerivedStateValues", "Ljava/util/HashMap;", "Lkotlin/collections/HashMap;", "scopeToValues", "Landroidx/compose/runtime/collection/IdentityArrayMap;", "valueToScopes", "clear", "clearObsoleteStateReads", "scope", "clearScopeObservations", "notifyInvalidatedScopes", "observe", "block", "Lkotlin/Function0;", "recordInvalidation", "", "changes", "", "recordRead", "value", "removeObservation", "removeScopeIf", "predicate", "Lkotlin/ParameterName;", "name", "runtime_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
    private static final class ObservedScopeMap
    {
        private Object currentScope;
        private IdentityArrayIntMap currentScopeReads;
        private int currentToken;
        private final IdentityScopeMap<DerivedState<?>> dependencyToDerivedStates;
        private int deriveStateScopeCount;
        private final Function1<State<?>, Unit> derivedStateEnterObserver;
        private final Function1<State<?>, Unit> derivedStateExitObserver;
        private final IdentityArraySet<Object> invalidated;
        private final Function1<Object, Unit> onChanged;
        private final HashMap<DerivedState<?>, Object> recordedDerivedStateValues;
        private final IdentityArrayMap<Object, IdentityArrayIntMap> scopeToValues;
        private final IdentityScopeMap<Object> valueToScopes;
        
        public ObservedScopeMap(final Function1<Object, Unit> onChanged) {
            Intrinsics.checkNotNullParameter((Object)onChanged, "onChanged");
            this.onChanged = onChanged;
            this.currentToken = -1;
            this.valueToScopes = new IdentityScopeMap<Object>();
            this.scopeToValues = new IdentityArrayMap<Object, IdentityArrayIntMap>(0, 1, null);
            this.invalidated = new IdentityArraySet<Object>();
            this.derivedStateEnterObserver = (Function1<State<?>, Unit>)new SnapshotStateObserver$ObservedScopeMap$derivedStateEnterObserver.SnapshotStateObserver$ObservedScopeMap$derivedStateEnterObserver$1(this);
            this.derivedStateExitObserver = (Function1<State<?>, Unit>)new SnapshotStateObserver$ObservedScopeMap$derivedStateExitObserver.SnapshotStateObserver$ObservedScopeMap$derivedStateExitObserver$1(this);
            this.dependencyToDerivedStates = new IdentityScopeMap<DerivedState<?>>();
            this.recordedDerivedStateValues = new HashMap<DerivedState<?>, Object>();
        }
        
        public static final /* synthetic */ Object access$getCurrentScope$p(final ObservedScopeMap observedScopeMap) {
            return observedScopeMap.currentScope;
        }
        
        public static final /* synthetic */ IdentityArrayIntMap access$getCurrentScopeReads$p(final ObservedScopeMap observedScopeMap) {
            return observedScopeMap.currentScopeReads;
        }
        
        public static final /* synthetic */ int access$getCurrentToken$p(final ObservedScopeMap observedScopeMap) {
            return observedScopeMap.currentToken;
        }
        
        public static final /* synthetic */ IdentityArrayMap access$getScopeToValues$p(final ObservedScopeMap observedScopeMap) {
            return observedScopeMap.scopeToValues;
        }
        
        public static final /* synthetic */ void access$setCurrentScope$p(final ObservedScopeMap observedScopeMap, final Object currentScope) {
            observedScopeMap.currentScope = currentScope;
        }
        
        public static final /* synthetic */ void access$setCurrentScopeReads$p(final ObservedScopeMap observedScopeMap, final IdentityArrayIntMap currentScopeReads) {
            observedScopeMap.currentScopeReads = currentScopeReads;
        }
        
        public static final /* synthetic */ void access$setCurrentToken$p(final ObservedScopeMap observedScopeMap, final int currentToken) {
            observedScopeMap.currentToken = currentToken;
        }
        
        private final void clearObsoleteStateReads(final Object o) {
            final IdentityArrayIntMap currentScopeReads = this.currentScopeReads;
            if (currentScopeReads != null) {
                final int size = currentScopeReads.getSize();
                int i = 0;
                int size2 = 0;
                while (i < size) {
                    final Object o2 = currentScopeReads.getKeys()[i];
                    Intrinsics.checkNotNull(o2, "null cannot be cast to non-null type kotlin.Any");
                    final int n = currentScopeReads.getValues()[i];
                    final boolean b = n != this.currentToken;
                    if (b) {
                        this.removeObservation(o, o2);
                    }
                    int n2 = size2;
                    if (!b) {
                        if (size2 != i) {
                            currentScopeReads.getKeys()[size2] = o2;
                            currentScopeReads.getValues()[size2] = n;
                        }
                        n2 = size2 + 1;
                    }
                    ++i;
                    size2 = n2;
                }
                for (int size3 = currentScopeReads.getSize(), j = size2; j < size3; ++j) {
                    currentScopeReads.getKeys()[j] = null;
                }
                currentScopeReads.setSize(size2);
            }
        }
        
        private final void removeObservation(final Object o, final Object key) {
            this.valueToScopes.remove(key, o);
            if (key instanceof DerivedState && !this.valueToScopes.contains(key)) {
                this.dependencyToDerivedStates.removeScope((DerivedState<?>)key);
                this.recordedDerivedStateValues.remove(key);
            }
        }
        
        public final void clear() {
            this.valueToScopes.clear();
            this.scopeToValues.clear();
            this.dependencyToDerivedStates.clear();
            this.recordedDerivedStateValues.clear();
        }
        
        public final void clearScopeObservations(final Object o) {
            Intrinsics.checkNotNullParameter(o, "scope");
            final IdentityArrayIntMap identityArrayIntMap = this.scopeToValues.remove(o);
            if (identityArrayIntMap == null) {
                return;
            }
            for (int i = 0; i < identityArrayIntMap.getSize(); ++i) {
                final Object o2 = identityArrayIntMap.getKeys()[i];
                Intrinsics.checkNotNull(o2, "null cannot be cast to non-null type kotlin.Any");
                final int n = identityArrayIntMap.getValues()[i];
                this.removeObservation(o, o2);
            }
        }
        
        public final Function1<State<?>, Unit> getDerivedStateEnterObserver() {
            return this.derivedStateEnterObserver;
        }
        
        public final Function1<State<?>, Unit> getDerivedStateExitObserver() {
            return this.derivedStateExitObserver;
        }
        
        public final Function1<Object, Unit> getOnChanged() {
            return this.onChanged;
        }
        
        public final void notifyInvalidatedScopes() {
            final IdentityArraySet<Object> invalidated = this.invalidated;
            final Function1<Object, Unit> onChanged = this.onChanged;
            for (int size = invalidated.size(), i = 0; i < size; ++i) {
                onChanged.invoke(invalidated.get(i));
            }
            this.invalidated.clear();
        }
        
        public final void observe(Object access$getCurrentScope$p, final Function0<Unit> function0) {
            Intrinsics.checkNotNullParameter(access$getCurrentScope$p, "scope");
            Intrinsics.checkNotNullParameter((Object)function0, "block");
            final Object access$getCurrentScope$p2 = access$getCurrentScope$p(this);
            final IdentityArrayIntMap access$getCurrentScopeReads$p = access$getCurrentScopeReads$p(this);
            final int access$getCurrentToken$p = access$getCurrentToken$p(this);
            access$setCurrentScope$p(this, access$getCurrentScope$p);
            access$setCurrentScopeReads$p(this, access$getScopeToValues$p(this).get(access$getCurrentScope$p));
            if (access$getCurrentToken$p(this) == -1) {
                access$setCurrentToken$p(this, SnapshotKt.currentSnapshot().getId());
            }
            function0.invoke();
            access$getCurrentScope$p = access$getCurrentScope$p(this);
            Intrinsics.checkNotNull(access$getCurrentScope$p);
            this.clearObsoleteStateReads(access$getCurrentScope$p);
            access$setCurrentScope$p(this, access$getCurrentScope$p2);
            access$setCurrentScopeReads$p(this, access$getCurrentScopeReads$p);
            access$setCurrentToken$p(this, access$getCurrentToken$p);
        }
        
        public final boolean recordInvalidation(final Set<?> set) {
            Intrinsics.checkNotNullParameter((Object)set, "changes");
            final Iterator<?> iterator = set.iterator();
            boolean b = false;
            while (iterator.hasNext()) {
                final Object next = iterator.next();
                boolean b2 = b;
                if (this.dependencyToDerivedStates.contains(next)) {
                    final IdentityScopeMap<DerivedState<?>> dependencyToDerivedStates = this.dependencyToDerivedStates;
                    final int access$find = IdentityScopeMap.access$find((IdentityScopeMap<Object>)dependencyToDerivedStates, next);
                    b2 = b;
                    if (access$find >= 0) {
                        final IdentityArraySet access$scopeSet = IdentityScopeMap.access$scopeSetAt((IdentityScopeMap<Object>)dependencyToDerivedStates, access$find);
                        final int size = access$scopeSet.size();
                        int n = 0;
                        while (true) {
                            b2 = b;
                            if (n >= size) {
                                break;
                            }
                            final DerivedState key = access$scopeSet.get(n);
                            Intrinsics.checkNotNull((Object)key, "null cannot be cast to non-null type androidx.compose.runtime.DerivedState<kotlin.Any?>");
                            final Object value = this.recordedDerivedStateValues.get(key);
                            SnapshotMutationPolicy<Object> snapshotMutationPolicy;
                            if ((snapshotMutationPolicy = key.getPolicy()) == null) {
                                snapshotMutationPolicy = SnapshotStateKt.structuralEqualityPolicy();
                            }
                            boolean b3 = b;
                            if (!snapshotMutationPolicy.equivalent(key.getCurrentValue(), value)) {
                                final IdentityScopeMap<Object> valueToScopes = this.valueToScopes;
                                final int access$find2 = IdentityScopeMap.access$find(valueToScopes, key);
                                b3 = b;
                                if (access$find2 >= 0) {
                                    final IdentityArraySet access$scopeSet2 = IdentityScopeMap.access$scopeSetAt(valueToScopes, access$find2);
                                    final int size2 = access$scopeSet2.size();
                                    int n2 = 0;
                                    while (true) {
                                        b3 = b;
                                        if (n2 >= size2) {
                                            break;
                                        }
                                        this.invalidated.add(access$scopeSet2.get(n2));
                                        ++n2;
                                        b = true;
                                    }
                                }
                            }
                            ++n;
                            b = b3;
                        }
                    }
                }
                final IdentityScopeMap<Object> valueToScopes2 = this.valueToScopes;
                final int access$find3 = IdentityScopeMap.access$find(valueToScopes2, next);
                b = b2;
                if (access$find3 >= 0) {
                    final IdentityArraySet access$scopeSet3 = IdentityScopeMap.access$scopeSetAt(valueToScopes2, access$find3);
                    final int size3 = access$scopeSet3.size();
                    int i = 0;
                    b = b2;
                    while (i < size3) {
                        this.invalidated.add(access$scopeSet3.get(i));
                        ++i;
                        b = true;
                    }
                }
            }
            return b;
        }
        
        public final void recordRead(final Object o) {
            Intrinsics.checkNotNullParameter(o, "value");
            if (this.deriveStateScopeCount > 0) {
                return;
            }
            final Object currentScope = this.currentScope;
            Intrinsics.checkNotNull(currentScope);
            IdentityArrayIntMap currentScopeReads;
            if ((currentScopeReads = this.currentScopeReads) == null) {
                currentScopeReads = new IdentityArrayIntMap();
                this.currentScopeReads = currentScopeReads;
                this.scopeToValues.set(currentScope, currentScopeReads);
            }
            final int add = currentScopeReads.add(o, this.currentToken);
            if (o instanceof DerivedState && add != this.currentToken) {
                final DerivedState derivedState = (DerivedState)o;
                final Object[] dependencies = derivedState.getDependencies();
                for (int i = 0; i < dependencies.length; ++i) {
                    final Object o2 = dependencies[i];
                    if (o2 == null) {
                        break;
                    }
                    this.dependencyToDerivedStates.add(o2, (DerivedState<?>)o);
                }
                this.recordedDerivedStateValues.put(o, derivedState.getCurrentValue());
            }
            if (add == -1) {
                this.valueToScopes.add(o, currentScope);
            }
        }
        
        public final void removeScopeIf(final Function1<Object, Boolean> function1) {
            Intrinsics.checkNotNullParameter((Object)function1, "predicate");
            final IdentityArrayMap<Object, IdentityArrayIntMap> scopeToValues = this.scopeToValues;
            final int size$runtime_release = scopeToValues.getSize$runtime_release();
            int i = 0;
            int size$runtime_release2 = 0;
            while (i < size$runtime_release) {
                final Object o = scopeToValues.getKeys$runtime_release()[i];
                Intrinsics.checkNotNull(o, "null cannot be cast to non-null type Key of androidx.compose.runtime.collection.IdentityArrayMap");
                final IdentityArrayIntMap identityArrayIntMap = (IdentityArrayIntMap)scopeToValues.getValues$runtime_release()[i];
                final Boolean b = (Boolean)function1.invoke(o);
                if (b) {
                    for (int size = identityArrayIntMap.getSize(), j = 0; j < size; ++j) {
                        final Object o2 = identityArrayIntMap.getKeys()[j];
                        Intrinsics.checkNotNull(o2, "null cannot be cast to non-null type kotlin.Any");
                        final int n = identityArrayIntMap.getValues()[j];
                        this.removeObservation(o, o2);
                    }
                }
                int n2 = size$runtime_release2;
                if (!b) {
                    if (size$runtime_release2 != i) {
                        scopeToValues.getKeys$runtime_release()[size$runtime_release2] = o;
                        scopeToValues.getValues$runtime_release()[size$runtime_release2] = scopeToValues.getValues$runtime_release()[i];
                    }
                    n2 = size$runtime_release2 + 1;
                }
                ++i;
                size$runtime_release2 = n2;
            }
            if (scopeToValues.getSize$runtime_release() > size$runtime_release2) {
                for (int size$runtime_release3 = scopeToValues.getSize$runtime_release(), k = size$runtime_release2; k < size$runtime_release3; ++k) {
                    scopeToValues.getKeys$runtime_release()[k] = null;
                    scopeToValues.getValues$runtime_release()[k] = null;
                }
                scopeToValues.setSize$runtime_release(size$runtime_release2);
            }
        }
    }
}

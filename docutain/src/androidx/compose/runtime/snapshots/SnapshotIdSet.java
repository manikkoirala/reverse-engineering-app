// 
// Decompiled by Procyon v0.6.0
// 

package androidx.compose.runtime.snapshots;

import kotlin.collections.CollectionsKt;
import java.util.Collection;
import java.util.ArrayList;
import java.util.List;
import kotlin.sequences.SequencesKt;
import kotlin.coroutines.Continuation;
import kotlin.jvm.functions.Function2;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.collections.ArraysKt;
import java.util.Iterator;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.Metadata;
import kotlin.jvm.internal.markers.KMappedMarker;

@Metadata(d1 = { "\u0000@\n\u0002\u0018\u0002\n\u0002\u0010\u001c\n\u0002\u0010\b\n\u0000\n\u0002\u0010\t\n\u0002\b\u0003\n\u0002\u0010\u0015\n\u0002\b\u0007\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010(\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0002\b\u0001\u0018\u0000 \u001d2\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u001dB)\b\u0002\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0004\u0012\u0006\u0010\u0006\u001a\u00020\u0002\u0012\b\u0010\u0007\u001a\u0004\u0018\u00010\b¢\u0006\u0002\u0010\tJ\u000e\u0010\n\u001a\u00020\u00002\u0006\u0010\u000b\u001a\u00020\u0000J\u000e\u0010\f\u001a\u00020\u00002\u0006\u0010\u000b\u001a\u00020\u0000J\u000e\u0010\r\u001a\u00020\u00002\u0006\u0010\u000e\u001a\u00020\u0002J \u0010\u000f\u001a\u00020\u00102\u0012\u0010\u0011\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00100\u0012H\u0086\b\u00f8\u0001\u0000J\u000e\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u000e\u001a\u00020\u0002J\u000f\u0010\u0015\u001a\b\u0012\u0004\u0012\u00020\u00020\u0016H\u0096\u0002J\u000e\u0010\u0017\u001a\u00020\u00022\u0006\u0010\u0018\u001a\u00020\u0002J\u000e\u0010\u0019\u001a\u00020\u00002\u0006\u0010\u000b\u001a\u00020\u0000J\u000e\u0010\u001a\u001a\u00020\u00002\u0006\u0010\u000e\u001a\u00020\u0002J\b\u0010\u001b\u001a\u00020\u001cH\u0016R\u0010\u0010\u0007\u001a\u0004\u0018\u00010\bX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0002X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004¢\u0006\u0002\n\u0000\u0082\u0002\u0007\n\u0005\b\u009920\u0001¨\u0006\u001e" }, d2 = { "Landroidx/compose/runtime/snapshots/SnapshotIdSet;", "", "", "upperSet", "", "lowerSet", "lowerBound", "belowBound", "", "(JJI[I)V", "and", "bits", "andNot", "clear", "bit", "fastForEach", "", "block", "Lkotlin/Function1;", "get", "", "iterator", "", "lowest", "default", "or", "set", "toString", "", "Companion", "runtime_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
public final class SnapshotIdSet implements Iterable<Integer>, KMappedMarker
{
    public static final Companion Companion;
    private static final SnapshotIdSet EMPTY;
    private final int[] belowBound;
    private final int lowerBound;
    private final long lowerSet;
    private final long upperSet;
    
    static {
        Companion = new Companion(null);
        EMPTY = new SnapshotIdSet(0L, 0L, 0, null);
    }
    
    private SnapshotIdSet(final long upperSet, final long lowerSet, final int lowerBound, final int[] belowBound) {
        this.upperSet = upperSet;
        this.lowerSet = lowerSet;
        this.lowerBound = lowerBound;
        this.belowBound = belowBound;
    }
    
    public static final /* synthetic */ int[] access$getBelowBound$p(final SnapshotIdSet set) {
        return set.belowBound;
    }
    
    public static final /* synthetic */ SnapshotIdSet access$getEMPTY$cp() {
        return SnapshotIdSet.EMPTY;
    }
    
    public static final /* synthetic */ int access$getLowerBound$p(final SnapshotIdSet set) {
        return set.lowerBound;
    }
    
    public static final /* synthetic */ long access$getLowerSet$p(final SnapshotIdSet set) {
        return set.lowerSet;
    }
    
    public static final /* synthetic */ long access$getUpperSet$p(final SnapshotIdSet set) {
        return set.upperSet;
    }
    
    public final SnapshotIdSet and(SnapshotIdSet set) {
        Intrinsics.checkNotNullParameter((Object)set, "bits");
        SnapshotIdSet empty = SnapshotIdSet.EMPTY;
        if (Intrinsics.areEqual((Object)set, (Object)empty)) {
            return empty;
        }
        if (Intrinsics.areEqual((Object)this, (Object)empty)) {
            return empty;
        }
        final int lowerBound = set.lowerBound;
        final int lowerBound2 = this.lowerBound;
        if (lowerBound == lowerBound2) {
            final int[] belowBound = set.belowBound;
            final int[] belowBound2 = this.belowBound;
            if (belowBound == belowBound2) {
                final long upperSet = this.upperSet;
                final long upperSet2 = set.upperSet;
                final long lowerSet = this.lowerSet;
                final long lowerSet2 = set.lowerSet;
                if ((upperSet & upperSet2) == 0x0L && (lowerSet & lowerSet2) == 0x0L && belowBound2 == null) {
                    return empty;
                }
                empty = new SnapshotIdSet(upperSet2 & upperSet, lowerSet & lowerSet2, lowerBound2, belowBound2);
                return empty;
            }
        }
        if (this.belowBound == null) {
            final Iterator iterator = this.iterator();
            SnapshotIdSet set2 = empty;
            while (true) {
                empty = set2;
                if (!iterator.hasNext()) {
                    break;
                }
                final int intValue = ((Number)iterator.next()).intValue();
                if (!set.get(intValue)) {
                    continue;
                }
                set2 = set2.set(intValue);
            }
        }
        else {
            final Iterator iterator2 = set.iterator();
            set = empty;
            while (true) {
                empty = set;
                if (!iterator2.hasNext()) {
                    break;
                }
                final int intValue2 = ((Number)iterator2.next()).intValue();
                if (!this.get(intValue2)) {
                    continue;
                }
                set = set.set(intValue2);
            }
        }
        return empty;
    }
    
    public final SnapshotIdSet andNot(SnapshotIdSet clear) {
        Intrinsics.checkNotNullParameter((Object)clear, "bits");
        final SnapshotIdSet empty = SnapshotIdSet.EMPTY;
        if (clear == empty) {
            return this;
        }
        if (this == empty) {
            return empty;
        }
        final int lowerBound = clear.lowerBound;
        final int lowerBound2 = this.lowerBound;
        if (lowerBound == lowerBound2) {
            final int[] belowBound = clear.belowBound;
            final int[] belowBound2 = this.belowBound;
            if (belowBound == belowBound2) {
                clear = new SnapshotIdSet(this.upperSet & ~clear.upperSet, this.lowerSet & ~clear.lowerSet, lowerBound2, belowBound2);
                return clear;
            }
        }
        final Iterator iterator = clear.iterator();
        clear = this;
        while (iterator.hasNext()) {
            final int intValue = ((Number)iterator.next()).intValue();
            final SnapshotIdSet set = clear;
            clear = clear.clear(intValue);
        }
        final SnapshotIdSet set2 = clear;
        return clear;
    }
    
    public final SnapshotIdSet clear(int n) {
        final int lowerBound = this.lowerBound;
        final int n2 = n - lowerBound;
        if (n2 >= 0 && n2 < 64) {
            final long n3 = 1L << n2;
            final long lowerSet = this.lowerSet;
            if ((lowerSet & n3) != 0x0L) {
                return new SnapshotIdSet(this.upperSet, lowerSet & ~n3, lowerBound, this.belowBound);
            }
        }
        else if (n2 >= 64 && n2 < 128) {
            final long n4 = 1L << n2 - 64;
            final long upperSet = this.upperSet;
            if ((upperSet & n4) != 0x0L) {
                return new SnapshotIdSet(upperSet & ~n4, this.lowerSet, lowerBound, this.belowBound);
            }
        }
        else if (n2 < 0) {
            final int[] belowBound = this.belowBound;
            if (belowBound != null) {
                final int binarySearch = SnapshotIdSetKt.binarySearch(belowBound, n);
                if (binarySearch >= 0) {
                    n = belowBound.length - 1;
                    if (n == 0) {
                        return new SnapshotIdSet(this.upperSet, this.lowerSet, this.lowerBound, null);
                    }
                    final int[] array = new int[n];
                    if (binarySearch > 0) {
                        ArraysKt.copyInto(belowBound, array, 0, 0, binarySearch);
                    }
                    if (binarySearch < n) {
                        ArraysKt.copyInto(belowBound, array, binarySearch, binarySearch + 1, n + 1);
                    }
                    return new SnapshotIdSet(this.upperSet, this.lowerSet, this.lowerBound, array);
                }
            }
        }
        return this;
    }
    
    public final void fastForEach(final Function1<? super Integer, Unit> function1) {
        Intrinsics.checkNotNullParameter((Object)function1, "block");
        final int[] access$getBelowBound$p = access$getBelowBound$p(this);
        final int n = 0;
        if (access$getBelowBound$p != null) {
            for (int length = access$getBelowBound$p.length, i = 0; i < length; ++i) {
                function1.invoke((Object)access$getBelowBound$p[i]);
            }
        }
        if (access$getLowerSet$p(this) != 0L) {
            for (int j = 0; j < 64; ++j) {
                if ((access$getLowerSet$p(this) & 1L << j) != 0x0L) {
                    function1.invoke((Object)(access$getLowerBound$p(this) + j));
                }
            }
        }
        if (access$getUpperSet$p(this) != 0L) {
            for (int k = n; k < 64; ++k) {
                if ((access$getUpperSet$p(this) & 1L << k) != 0x0L) {
                    function1.invoke((Object)(k + 64 + access$getLowerBound$p(this)));
                }
            }
        }
    }
    
    public final boolean get(final int n) {
        final int n2 = n - this.lowerBound;
        final boolean b = true;
        final boolean b2 = true;
        boolean b3 = true;
        final boolean b4 = false;
        if (n2 >= 0 && n2 < 64) {
            if ((1L << n2 & this.lowerSet) == 0x0L) {
                b3 = false;
            }
            return b3;
        }
        if (n2 >= 64 && n2 < 128) {
            return (1L << n2 - 64 & this.upperSet) != 0x0L && b;
        }
        if (n2 > 0) {
            return false;
        }
        final int[] belowBound = this.belowBound;
        boolean b5 = b4;
        if (belowBound != null) {
            b5 = (SnapshotIdSetKt.binarySearch(belowBound, n) >= 0 && b2);
        }
        return b5;
    }
    
    @Override
    public Iterator<Integer> iterator() {
        return SequencesKt.sequence((Function2)new SnapshotIdSet$iterator.SnapshotIdSet$iterator$1(this, (Continuation)null)).iterator();
    }
    
    public final int lowest(int n) {
        final int[] belowBound = this.belowBound;
        if (belowBound != null) {
            return belowBound[0];
        }
        final long lowerSet = this.lowerSet;
        if (lowerSet != 0L) {
            return this.lowerBound + SnapshotIdSetKt.access$lowestBitOf(lowerSet);
        }
        final long upperSet = this.upperSet;
        if (upperSet != 0L) {
            n = this.lowerBound + 64 + SnapshotIdSetKt.access$lowestBitOf(upperSet);
        }
        return n;
    }
    
    public final SnapshotIdSet or(SnapshotIdSet set) {
        Intrinsics.checkNotNullParameter((Object)set, "bits");
        final SnapshotIdSet empty = SnapshotIdSet.EMPTY;
        if (set == empty) {
            return this;
        }
        if (this == empty) {
            return set;
        }
        final int lowerBound = set.lowerBound;
        final int lowerBound2 = this.lowerBound;
        if (lowerBound == lowerBound2) {
            final int[] belowBound = set.belowBound;
            final int[] belowBound2 = this.belowBound;
            if (belowBound == belowBound2) {
                set = new SnapshotIdSet(this.upperSet | set.upperSet, this.lowerSet | set.lowerSet, lowerBound2, belowBound2);
                return set;
            }
        }
        if (this.belowBound == null) {
            final Iterator iterator = this.iterator();
            while (iterator.hasNext()) {
                set = set.set(((Number)iterator.next()).intValue());
            }
        }
        else {
            final Iterator iterator2 = set.iterator();
            set = this;
            while (iterator2.hasNext()) {
                final int intValue = ((Number)iterator2.next()).intValue();
                final SnapshotIdSet set2 = set;
                set = set.set(intValue);
            }
            final SnapshotIdSet set3 = set;
        }
        return set;
    }
    
    public final SnapshotIdSet set(final int n) {
        final int lowerBound = this.lowerBound;
        final int n2 = n - lowerBound;
        long n3 = 0L;
        if (n2 >= 0 && n2 < 64) {
            final long n4 = 1L << n2;
            final long lowerSet = this.lowerSet;
            if ((lowerSet & n4) == 0x0L) {
                return new SnapshotIdSet(this.upperSet, lowerSet | n4, lowerBound, this.belowBound);
            }
        }
        else if (n2 >= 64 && n2 < 128) {
            final long n5 = 1L << n2 - 64;
            final long upperSet = this.upperSet;
            if ((upperSet & n5) == 0x0L) {
                return new SnapshotIdSet(upperSet | n5, this.lowerSet, lowerBound, this.belowBound);
            }
        }
        else if (n2 >= 128) {
            if (!this.get(n)) {
                long upperSet2 = this.upperSet;
                long lowerSet2 = this.lowerSet;
                int i = this.lowerBound;
                List list = null;
                while (true) {
                    List list2;
                    long n7;
                    for (int n6 = (n + 1) / 64 * 64; i < n6; i += 64, lowerSet2 = upperSet2, upperSet2 = n7, list = list2, n3 = n7) {
                        list2 = list;
                        n7 = n3;
                        if (lowerSet2 != n3) {
                            if ((list2 = list) == null) {
                                final List list3 = new ArrayList();
                                final int[] belowBound = this.belowBound;
                                list2 = list3;
                                if (belowBound != null) {
                                    final int length = belowBound.length;
                                    int n8 = 0;
                                    while (true) {
                                        list2 = list3;
                                        if (n8 >= length) {
                                            break;
                                        }
                                        list3.add(belowBound[n8]);
                                        ++n8;
                                    }
                                }
                            }
                            for (int j = 0; j < 64; ++j) {
                                if ((1L << j & lowerSet2) != 0x0L) {
                                    list2.add(j + i);
                                }
                            }
                            n7 = 0L;
                        }
                        if (upperSet2 == n7) {
                            i = n6;
                            list = list2;
                            int[] array;
                            if (list == null || (array = CollectionsKt.toIntArray((Collection)list)) == null) {
                                array = this.belowBound;
                            }
                            return new SnapshotIdSet(upperSet2, n7, i, array).set(n);
                        }
                    }
                    n7 = lowerSet2;
                    continue;
                }
            }
        }
        else {
            final int[] belowBound2 = this.belowBound;
            if (belowBound2 == null) {
                return new SnapshotIdSet(this.upperSet, this.lowerSet, lowerBound, new int[] { n });
            }
            final int binarySearch = SnapshotIdSetKt.binarySearch(belowBound2, n);
            if (binarySearch < 0) {
                final int n9 = -(binarySearch + 1);
                final int n10 = belowBound2.length + 1;
                final int[] array2 = new int[n10];
                ArraysKt.copyInto(belowBound2, array2, 0, 0, n9);
                ArraysKt.copyInto(belowBound2, array2, n9 + 1, n9, n10 - 1);
                array2[n9] = n;
                return new SnapshotIdSet(this.upperSet, this.lowerSet, this.lowerBound, array2);
            }
        }
        return this;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append(super.toString());
        sb.append(" [");
        final Iterable iterable = this;
        final Collection collection = new ArrayList(CollectionsKt.collectionSizeOrDefault(iterable, 10));
        final Iterator iterator = iterable.iterator();
        while (iterator.hasNext()) {
            collection.add(String.valueOf(((Number)iterator.next()).intValue()));
        }
        sb.append(ListUtilsKt.fastJoinToString$default((List)collection, null, null, null, 0, null, null, 63, null));
        sb.append(']');
        return sb.toString();
    }
    
    @Metadata(d1 = { "\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u0011\u0010\u0003\u001a\u00020\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006¨\u0006\u0007" }, d2 = { "Landroidx/compose/runtime/snapshots/SnapshotIdSet$Companion;", "", "()V", "EMPTY", "Landroidx/compose/runtime/snapshots/SnapshotIdSet;", "getEMPTY", "()Landroidx/compose/runtime/snapshots/SnapshotIdSet;", "runtime_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
    public static final class Companion
    {
        private Companion() {
        }
        
        public final SnapshotIdSet getEMPTY() {
            return SnapshotIdSet.access$getEMPTY$cp();
        }
    }
}

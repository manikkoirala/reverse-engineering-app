// 
// Decompiled by Procyon v0.6.0
// 

package androidx.compose.runtime.snapshots;

import kotlin.KotlinNothingValueException;
import java.util.List;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.collections.CollectionsKt;
import java.util.Collection;
import kotlin.jvm.internal.Intrinsics;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000>\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0001\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0003\b\u0000\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\b\u0010\u0007\u001a\u00020\bH\u0016J\b\u0010\t\u001a\u00020\nH\u0016J\u0015\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u000eH\u0010¢\u0006\u0002\b\u000fJ\u0015\u0010\u0010\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u000eH\u0010¢\u0006\u0002\b\u0011J\r\u0010\u0012\u001a\u00020\nH\u0010¢\u0006\u0002\b\u0013J4\u0010\u0014\u001a\u00020\u00012\u0014\u0010\u0015\u001a\u0010\u0012\u0004\u0012\u00020\u0017\u0012\u0004\u0012\u00020\n\u0018\u00010\u00162\u0014\u0010\u0018\u001a\u0010\u0012\u0004\u0012\u00020\u0017\u0012\u0004\u0012\u00020\n\u0018\u00010\u0016H\u0016J\u001e\u0010\u0019\u001a\u00020\u000e2\u0014\u0010\u0015\u001a\u0010\u0012\u0004\u0012\u00020\u0017\u0012\u0004\u0012\u00020\n\u0018\u00010\u0016H\u0016¨\u0006\u001a" }, d2 = { "Landroidx/compose/runtime/snapshots/GlobalSnapshot;", "Landroidx/compose/runtime/snapshots/MutableSnapshot;", "id", "", "invalid", "Landroidx/compose/runtime/snapshots/SnapshotIdSet;", "(ILandroidx/compose/runtime/snapshots/SnapshotIdSet;)V", "apply", "Landroidx/compose/runtime/snapshots/SnapshotApplyResult;", "dispose", "", "nestedActivated", "", "snapshot", "Landroidx/compose/runtime/snapshots/Snapshot;", "nestedActivated$runtime_release", "nestedDeactivated", "nestedDeactivated$runtime_release", "notifyObjectsInitialized", "notifyObjectsInitialized$runtime_release", "takeNestedMutableSnapshot", "readObserver", "Lkotlin/Function1;", "", "writeObserver", "takeNestedSnapshot", "runtime_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
public final class GlobalSnapshot extends MutableSnapshot
{
    public GlobalSnapshot(final int n, final SnapshotIdSet set) {
        Intrinsics.checkNotNullParameter((Object)set, "invalid");
        synchronized (SnapshotKt.getLock()) {
            List mutableList;
            if (SnapshotKt.access$getGlobalWriteObservers$p().isEmpty() ^ true) {
                mutableList = CollectionsKt.toMutableList((Collection)SnapshotKt.access$getGlobalWriteObservers$p());
            }
            else {
                mutableList = null;
            }
            Function1 function1;
            if (mutableList != null) {
                if ((function1 = (Function1)CollectionsKt.singleOrNull(mutableList)) == null) {
                    function1 = (Function1)new GlobalSnapshot$1$1.GlobalSnapshot$1$1$1(mutableList);
                }
            }
            else {
                function1 = null;
            }
            monitorexit(SnapshotKt.getLock());
            super(n, set, null, (Function1<Object, Unit>)function1);
        }
    }
    
    @Override
    public SnapshotApplyResult apply() {
        throw new IllegalStateException("Cannot apply the global snapshot directly. Call Snapshot.advanceGlobalSnapshot".toString());
    }
    
    @Override
    public void dispose() {
        synchronized (SnapshotKt.getLock()) {
            this.releasePinnedSnapshotLocked$runtime_release();
            final Unit instance = Unit.INSTANCE;
        }
    }
    
    public Void nestedActivated$runtime_release(final Snapshot snapshot) {
        Intrinsics.checkNotNullParameter((Object)snapshot, "snapshot");
        SnapshotStateMapKt.unsupported();
        throw new KotlinNothingValueException();
    }
    
    public Void nestedDeactivated$runtime_release(final Snapshot snapshot) {
        Intrinsics.checkNotNullParameter((Object)snapshot, "snapshot");
        SnapshotStateMapKt.unsupported();
        throw new KotlinNothingValueException();
    }
    
    @Override
    public void notifyObjectsInitialized$runtime_release() {
        SnapshotKt.access$advanceGlobalSnapshot();
    }
    
    @Override
    public MutableSnapshot takeNestedMutableSnapshot(final Function1<Object, Unit> function1, final Function1<Object, Unit> function2) {
        return (MutableSnapshot)SnapshotKt.access$takeNewSnapshot((Function1)new GlobalSnapshot$takeNestedMutableSnapshot.GlobalSnapshot$takeNestedMutableSnapshot$1((Function1)function1, (Function1)function2));
    }
    
    @Override
    public Snapshot takeNestedSnapshot(final Function1<Object, Unit> function1) {
        return SnapshotKt.access$takeNewSnapshot((Function1)new GlobalSnapshot$takeNestedSnapshot.GlobalSnapshot$takeNestedSnapshot$1((Function1)function1));
    }
}

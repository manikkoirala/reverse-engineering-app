// 
// Decompiled by Procyon v0.6.0
// 

package androidx.compose.runtime.snapshots;

import kotlin.KotlinNothingValueException;
import java.util.Set;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000F\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0006\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010#\n\u0002\u0018\u0002\n\u0002\b\u0012\n\u0002\u0010\u0001\n\u0002\b\u000b\b\u0000\u0018\u00002\u00020\u0001B5\u0012\b\u0010\u0002\u001a\u0004\u0018\u00010\u0001\u0012\u0014\u0010\u0003\u001a\u0010\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u0004\u0012\u0006\u0010\u0007\u001a\u00020\b\u0012\u0006\u0010\t\u001a\u00020\b¢\u0006\u0002\u0010\nJ\b\u0010,\u001a\u00020\u0006H\u0016J\b\u0010-\u001a\u00020\bH\u0016J\u0015\u0010.\u001a\u00020/2\u0006\u00100\u001a\u00020\u0001H\u0010¢\u0006\u0002\b1J\u0015\u00102\u001a\u00020/2\u0006\u00100\u001a\u00020\u0001H\u0010¢\u0006\u0002\b3J\r\u00104\u001a\u00020\u0006H\u0010¢\u0006\u0002\b5J\u0015\u00106\u001a\u00020\u00062\u0006\u00107\u001a\u00020\u001cH\u0010¢\u0006\u0002\b8J\u001e\u00109\u001a\u00020\u00012\u0014\u0010\"\u001a\u0010\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u0004H\u0016R\u0014\u0010\u000b\u001a\u00020\u00018BX\u0082\u0004¢\u0006\u0006\u001a\u0004\b\f\u0010\rR$\u0010\u0010\u001a\u00020\u000f2\u0006\u0010\u000e\u001a\u00020\u000f8V@PX\u0096\u000e¢\u0006\f\u001a\u0004\b\u0011\u0010\u0012\"\u0004\b\u0013\u0010\u0014R$\u0010\u0016\u001a\u00020\u00152\u0006\u0010\u000e\u001a\u00020\u00158P@PX\u0090\u000e¢\u0006\f\u001a\u0004\b\u0017\u0010\u0018\"\u0004\b\u0019\u0010\u001aR\u000e\u0010\u0007\u001a\u00020\bX\u0082\u0004¢\u0006\u0002\n\u0000R4\u0010\u001d\u001a\n\u0012\u0004\u0012\u00020\u001c\u0018\u00010\u001b2\u000e\u0010\u000e\u001a\n\u0012\u0004\u0012\u00020\u001c\u0018\u00010\u001b8P@VX\u0090\u000e¢\u0006\f\u001a\u0004\b\u001e\u0010\u001f\"\u0004\b \u0010!R\u000e\u0010\t\u001a\u00020\bX\u0082\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\u0002\u001a\u0004\u0018\u00010\u0001X\u0082\u0004¢\u0006\u0002\n\u0000R\"\u0010\"\u001a\u0010\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u0004X\u0090\u0004¢\u0006\b\n\u0000\u001a\u0004\b#\u0010$R\u0014\u0010%\u001a\u00020\b8VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b&\u0010'R\u0014\u0010(\u001a\u00020\u0001X\u0096\u0004¢\u0006\b\n\u0000\u001a\u0004\b)\u0010\rR\"\u0010*\u001a\u0010\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u0004X\u0090\u0004¢\u0006\b\n\u0000\u001a\u0004\b+\u0010$¨\u0006:" }, d2 = { "Landroidx/compose/runtime/snapshots/TransparentObserverSnapshot;", "Landroidx/compose/runtime/snapshots/Snapshot;", "previousSnapshot", "specifiedReadObserver", "Lkotlin/Function1;", "", "", "mergeParentObservers", "", "ownsPreviousSnapshot", "(Landroidx/compose/runtime/snapshots/Snapshot;Lkotlin/jvm/functions/Function1;ZZ)V", "currentSnapshot", "getCurrentSnapshot", "()Landroidx/compose/runtime/snapshots/Snapshot;", "value", "", "id", "getId", "()I", "setId$runtime_release", "(I)V", "Landroidx/compose/runtime/snapshots/SnapshotIdSet;", "invalid", "getInvalid$runtime_release", "()Landroidx/compose/runtime/snapshots/SnapshotIdSet;", "setInvalid$runtime_release", "(Landroidx/compose/runtime/snapshots/SnapshotIdSet;)V", "", "Landroidx/compose/runtime/snapshots/StateObject;", "modified", "getModified$runtime_release", "()Ljava/util/Set;", "setModified", "(Ljava/util/Set;)V", "readObserver", "getReadObserver$runtime_release", "()Lkotlin/jvm/functions/Function1;", "readOnly", "getReadOnly", "()Z", "root", "getRoot", "writeObserver", "getWriteObserver$runtime_release", "dispose", "hasPendingChanges", "nestedActivated", "", "snapshot", "nestedActivated$runtime_release", "nestedDeactivated", "nestedDeactivated$runtime_release", "notifyObjectsInitialized", "notifyObjectsInitialized$runtime_release", "recordModified", "state", "recordModified$runtime_release", "takeNestedSnapshot", "runtime_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
public final class TransparentObserverSnapshot extends Snapshot
{
    private final boolean mergeParentObservers;
    private final boolean ownsPreviousSnapshot;
    private final Snapshot previousSnapshot;
    private final Function1<Object, Unit> readObserver;
    private final Snapshot root;
    private final Function1<Object, Unit> writeObserver;
    
    public TransparentObserverSnapshot(final Snapshot previousSnapshot, final Function1<Object, Unit> function1, final boolean mergeParentObservers, final boolean ownsPreviousSnapshot) {
        super(0, SnapshotIdSet.Companion.getEMPTY(), null);
        this.previousSnapshot = previousSnapshot;
        this.mergeParentObservers = mergeParentObservers;
        this.ownsPreviousSnapshot = ownsPreviousSnapshot;
        Function1<Object, Unit> function2;
        if (previousSnapshot == null || (function2 = previousSnapshot.getReadObserver$runtime_release()) == null) {
            function2 = SnapshotKt.access$getCurrentGlobalSnapshot$p().get().getReadObserver$runtime_release();
        }
        this.readObserver = (Function1<Object, Unit>)SnapshotKt.access$mergedReadObserver(function1, function2, mergeParentObservers);
        this.root = this;
    }
    
    private final Snapshot getCurrentSnapshot() {
        Snapshot previousSnapshot;
        if ((previousSnapshot = this.previousSnapshot) == null) {
            final Snapshot value = SnapshotKt.access$getCurrentGlobalSnapshot$p().get();
            Intrinsics.checkNotNullExpressionValue((Object)value, "currentGlobalSnapshot.get()");
            previousSnapshot = value;
        }
        return previousSnapshot;
    }
    
    @Override
    public void dispose() {
        this.setDisposed$runtime_release(true);
        if (this.ownsPreviousSnapshot) {
            final Snapshot previousSnapshot = this.previousSnapshot;
            if (previousSnapshot != null) {
                previousSnapshot.dispose();
            }
        }
    }
    
    @Override
    public int getId() {
        return this.getCurrentSnapshot().getId();
    }
    
    @Override
    public SnapshotIdSet getInvalid$runtime_release() {
        return this.getCurrentSnapshot().getInvalid$runtime_release();
    }
    
    @Override
    public Set<StateObject> getModified$runtime_release() {
        return this.getCurrentSnapshot().getModified$runtime_release();
    }
    
    @Override
    public Function1<Object, Unit> getReadObserver$runtime_release() {
        return this.readObserver;
    }
    
    @Override
    public boolean getReadOnly() {
        return this.getCurrentSnapshot().getReadOnly();
    }
    
    @Override
    public Snapshot getRoot() {
        return this.root;
    }
    
    @Override
    public Function1<Object, Unit> getWriteObserver$runtime_release() {
        return this.writeObserver;
    }
    
    @Override
    public boolean hasPendingChanges() {
        return this.getCurrentSnapshot().hasPendingChanges();
    }
    
    public Void nestedActivated$runtime_release(final Snapshot snapshot) {
        Intrinsics.checkNotNullParameter((Object)snapshot, "snapshot");
        SnapshotStateMapKt.unsupported();
        throw new KotlinNothingValueException();
    }
    
    public Void nestedDeactivated$runtime_release(final Snapshot snapshot) {
        Intrinsics.checkNotNullParameter((Object)snapshot, "snapshot");
        SnapshotStateMapKt.unsupported();
        throw new KotlinNothingValueException();
    }
    
    @Override
    public void notifyObjectsInitialized$runtime_release() {
        this.getCurrentSnapshot().notifyObjectsInitialized$runtime_release();
    }
    
    @Override
    public void recordModified$runtime_release(final StateObject stateObject) {
        Intrinsics.checkNotNullParameter((Object)stateObject, "state");
        this.getCurrentSnapshot().recordModified$runtime_release(stateObject);
    }
    
    @Override
    public void setId$runtime_release(final int n) {
        SnapshotStateMapKt.unsupported();
        throw new KotlinNothingValueException();
    }
    
    @Override
    public void setInvalid$runtime_release(final SnapshotIdSet set) {
        Intrinsics.checkNotNullParameter((Object)set, "value");
        SnapshotStateMapKt.unsupported();
        throw new KotlinNothingValueException();
    }
    
    public void setModified(final Set<StateObject> set) {
        SnapshotStateMapKt.unsupported();
        throw new KotlinNothingValueException();
    }
    
    @Override
    public Snapshot takeNestedSnapshot(final Function1<Object, Unit> function1) {
        final Function1 mergedReadObserver$default = SnapshotKt.mergedReadObserver$default(function1, this.getReadObserver$runtime_release(), false, 4, null);
        Snapshot snapshot;
        if (!this.mergeParentObservers) {
            snapshot = createTransparentSnapshotWithNoParentReadObserver(this.getCurrentSnapshot().takeNestedSnapshot(null), (Function1<Object, Unit>)mergedReadObserver$default, true);
        }
        else {
            snapshot = this.getCurrentSnapshot().takeNestedSnapshot((Function1<Object, Unit>)mergedReadObserver$default);
        }
        return snapshot;
    }
}

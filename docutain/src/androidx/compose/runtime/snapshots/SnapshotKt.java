// 
// Decompiled by Procyon v0.6.0
// 

package androidx.compose.runtime.snapshots;

import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.InlineMarker;
import java.util.Iterator;
import java.util.HashMap;
import kotlin.KotlinNothingValueException;
import kotlin.collections.CollectionsKt;
import java.util.Collection;
import java.util.Map;
import kotlin.jvm.internal.Intrinsics;
import java.util.ArrayList;
import androidx.compose.runtime.SnapshotThreadLocal;
import kotlin.jvm.functions.Function1;
import java.util.concurrent.atomic.AtomicReference;
import kotlin.Unit;
import java.util.Set;
import kotlin.jvm.functions.Function2;
import java.util.List;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000\u008a\u0001\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0002\u0010\"\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u000b\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\f\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u0001\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0017\n\u0002\u0018\u0002\n\u0002\b\n\u001a\b\u0010$\u001a\u00020\bH\u0002\u001a6\u0010$\u001a\u0002H%\"\u0004\b\u0000\u0010%2!\u0010&\u001a\u001d\u0012\u0013\u0012\u00110\u0010¢\u0006\f\b\u0011\u0012\b\b\u0012\u0012\u0004\b\b(\u0013\u0012\u0004\u0012\u0002H%0\u000fH\u0002¢\u0006\u0002\u0010'\u001a4\u0010(\u001a\u00020\u00072\b\u0010)\u001a\u0004\u0018\u00010\u00072\u0016\b\u0002\u0010*\u001a\u0010\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\b\u0018\u00010\u000f2\b\b\u0002\u0010+\u001a\u00020,H\u0002\u001a\u001f\u0010-\u001a\u0002H%\"\b\b\u0000\u0010%*\u00020.2\u0006\u0010/\u001a\u0002H%H\u0001¢\u0006\u0002\u00100\u001a'\u0010-\u001a\u0002H%\"\b\b\u0000\u0010%*\u00020.2\u0006\u0010/\u001a\u0002H%2\u0006\u00101\u001a\u00020\u0007H\u0001¢\u0006\u0002\u00102\u001a\b\u00103\u001a\u00020\u0007H\u0000\u001aL\u00104\u001a\u0010\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\b\u0018\u00010\u000f2\u0014\u0010*\u001a\u0010\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\b\u0018\u00010\u000f2\u0014\u00105\u001a\u0010\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\b\u0018\u00010\u000f2\b\b\u0002\u00106\u001a\u00020,H\u0002\u001aB\u00107\u001a\u0010\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\b\u0018\u00010\u000f2\u0014\u00108\u001a\u0010\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\b\u0018\u00010\u000f2\u0014\u00105\u001a\u0010\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\b\u0018\u00010\u000fH\u0002\u001a\u0018\u00109\u001a\u00020\b2\u0006\u00101\u001a\u00020\u00072\u0006\u0010:\u001a\u00020;H\u0001\u001a.\u0010<\u001a\u0010\u0012\u0004\u0012\u00020.\u0012\u0004\u0012\u00020.\u0018\u00010=2\u0006\u00103\u001a\u00020>2\u0006\u0010?\u001a\u00020>2\u0006\u0010@\u001a\u00020\u0010H\u0002\u001a\u0010\u0010A\u001a\u00020,2\u0006\u0010:\u001a\u00020;H\u0002\u001a\b\u0010B\u001a\u00020CH\u0002\u001a1\u0010D\u001a\u0004\u0018\u0001H%\"\b\b\u0000\u0010%*\u00020.2\u0006\u0010/\u001a\u0002H%2\u0006\u0010E\u001a\u00020\u00012\u0006\u0010\u0013\u001a\u00020\u0010H\u0002¢\u0006\u0002\u0010F\u001a\u0010\u0010G\u001a\u00020\b2\u0006\u0010H\u001a\u00020\u0001H\u0000\u001a\b\u0010I\u001a\u00020CH\u0002\u001a%\u0010J\u001a\u0002H%\"\u0004\b\u0000\u0010%2\f\u0010&\u001a\b\u0012\u0004\u0012\u0002H%0KH\u0081\b\u00f8\u0001\u0000¢\u0006\u0002\u0010L\u001a>\u0010M\u001a\u0002H%\"\u0004\b\u0000\u0010%2\u0006\u0010N\u001a\u00020\u00072!\u0010&\u001a\u001d\u0012\u0013\u0012\u00110\u0010¢\u0006\f\b\u0011\u0012\b\b\u0012\u0012\u0004\b\b(\u0013\u0012\u0004\u0012\u0002H%0\u000fH\u0002¢\u0006\u0002\u0010O\u001a:\u0010P\u001a\u0002H%\"\b\b\u0000\u0010%*\u00020\u00072!\u0010&\u001a\u001d\u0012\u0013\u0012\u00110\u0010¢\u0006\f\b\u0011\u0012\b\b\u0012\u0012\u0004\b\b(\u0013\u0012\u0004\u0012\u0002H%0\u000fH\u0002¢\u0006\u0002\u0010Q\u001a\u0018\u0010R\u001a\u00020\u00012\u0006\u0010E\u001a\u00020\u00012\u0006\u0010\u0013\u001a\u00020\u0010H\u0000\u001a\u0012\u0010S\u001a\u0004\u0018\u00010.2\u0006\u0010:\u001a\u00020;H\u0002\u001a \u0010T\u001a\u00020,2\u0006\u0010U\u001a\u00020.2\u0006\u00101\u001a\u00020\u00012\u0006\u0010\u0013\u001a\u00020\u0010H\u0002\u001a \u0010T\u001a\u00020,2\u0006\u00103\u001a\u00020\u00012\u0006\u0010V\u001a\u00020\u00012\u0006\u0010\u0013\u001a\u00020\u0010H\u0002\u001a\u0010\u0010W\u001a\u00020\b2\u0006\u00101\u001a\u00020\u0007H\u0002\u001a\u001c\u0010X\u001a\u00020\u0010*\u00020\u00102\u0006\u0010Y\u001a\u00020\u00012\u0006\u0010Z\u001a\u00020\u0001H\u0000\u001a#\u0010[\u001a\u0002H%\"\b\b\u0000\u0010%*\u00020.*\u0002H%2\u0006\u0010:\u001a\u00020;H\u0000¢\u0006\u0002\u0010\\\u001a+\u0010]\u001a\u0002H%\"\b\b\u0000\u0010%*\u00020.*\u0002H%2\u0006\u0010:\u001a\u00020;2\u0006\u00101\u001a\u00020\u0007H\u0000¢\u0006\u0002\u0010^\u001a+\u0010_\u001a\u0002H%\"\b\b\u0000\u0010%*\u00020.*\u0002H%2\u0006\u0010:\u001a\u00020;2\u0006\u00101\u001a\u00020\u0007H\u0002¢\u0006\u0002\u0010^\u001aN\u0010`\u001a\u0002Ha\"\b\b\u0000\u0010%*\u00020.\"\u0004\b\u0001\u0010a*\u0002H%2\u0006\u0010:\u001a\u00020;2\u0006\u0010b\u001a\u0002H%2\u0017\u0010&\u001a\u0013\u0012\u0004\u0012\u0002H%\u0012\u0004\u0012\u0002Ha0\u000f¢\u0006\u0002\bcH\u0080\b\u00f8\u0001\u0000¢\u0006\u0002\u0010d\u001a3\u0010e\u001a\u0002H%\"\b\b\u0000\u0010%*\u00020.*\u0002H%2\u0006\u0010:\u001a\u00020;2\u0006\u00101\u001a\u00020\u00072\u0006\u0010b\u001a\u0002H%H\u0000¢\u0006\u0002\u0010f\u001a!\u0010D\u001a\u0002H%\"\b\b\u0000\u0010%*\u00020.*\u0002H%2\u0006\u0010:\u001a\u00020;¢\u0006\u0002\u0010\\\u001a)\u0010D\u001a\u0002H%\"\b\b\u0000\u0010%*\u00020.*\u0002H%2\u0006\u0010:\u001a\u00020;2\u0006\u00101\u001a\u00020\u0007¢\u0006\u0002\u0010^\u001aH\u0010g\u001a\u0002Ha\"\b\b\u0000\u0010%*\u00020.\"\u0004\b\u0001\u0010a*\u0002H%2!\u0010&\u001a\u001d\u0012\u0013\u0012\u0011H%¢\u0006\f\b\u0011\u0012\b\b\u0012\u0012\u0004\b\b(/\u0012\u0004\u0012\u0002Ha0\u000fH\u0086\b\u00f8\u0001\u0000¢\u0006\u0002\u0010h\u001aF\u0010i\u001a\u0002Ha\"\b\b\u0000\u0010%*\u00020.\"\u0004\b\u0001\u0010a*\u0002H%2\u0006\u0010:\u001a\u00020;2\u0017\u0010&\u001a\u0013\u0012\u0004\u0012\u0002H%\u0012\u0004\u0012\u0002Ha0\u000f¢\u0006\u0002\bcH\u0086\b\u00f8\u0001\u0000¢\u0006\u0002\u0010j\u001aN\u0010i\u001a\u0002Ha\"\b\b\u0000\u0010%*\u00020.\"\u0004\b\u0001\u0010a*\u0002H%2\u0006\u0010:\u001a\u00020;2\u0006\u00101\u001a\u00020\u00072\u0017\u0010&\u001a\u0013\u0012\u0004\u0012\u0002H%\u0012\u0004\u0012\u0002Ha0\u000f¢\u0006\u0002\bcH\u0086\b\u00f8\u0001\u0000¢\u0006\u0002\u0010k\u001a+\u0010l\u001a\u0002H%\"\b\b\u0000\u0010%*\u00020.*\u0002H%2\u0006\u0010:\u001a\u00020;2\u0006\u00101\u001a\u00020\u0007H\u0001¢\u0006\u0002\u0010^\"\u000e\u0010\u0000\u001a\u00020\u0001X\u0082T¢\u0006\u0002\n\u0000\",\u0010\u0002\u001a \u0012\u001c\u0012\u001a\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00060\u0005\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\b0\u00040\u0003X\u0082\u0004¢\u0006\u0002\n\u0000\".\u0010\t\u001a\"\u0012\f\u0012\n \f*\u0004\u0018\u00010\u000b0\u000b0\nj\u0010\u0012\f\u0012\n \f*\u0004\u0018\u00010\u000b0\u000b`\rX\u0082\u0004¢\u0006\u0002\n\u0000\")\u0010\u000e\u001a\u001d\u0012\u0013\u0012\u00110\u0010¢\u0006\f\b\u0011\u0012\b\b\u0012\u0012\u0004\b\b(\u0013\u0012\u0004\u0012\u00020\b0\u000fX\u0082\u0004¢\u0006\u0002\n\u0000\" \u0010\u0014\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\b0\u000f0\u0003X\u0082\u0004¢\u0006\u0002\n\u0000\"\u001c\u0010\u0015\u001a\u00020\u00068\u0000X\u0081\u0004¢\u0006\u000e\n\u0000\u0012\u0004\b\u0016\u0010\u0017\u001a\u0004\b\u0018\u0010\u0019\"\u000e\u0010\u001a\u001a\u00020\u0001X\u0082\u000e¢\u0006\u0002\n\u0000\"\u000e\u0010\u001b\u001a\u00020\u0010X\u0082\u000e¢\u0006\u0002\n\u0000\"\u000e\u0010\u001c\u001a\u00020\u001dX\u0082\u0004¢\u0006\u0002\n\u0000\"\u001c\u0010\u001e\u001a\u00020\u00078\u0000X\u0081\u0004¢\u0006\u000e\n\u0000\u0012\u0004\b\u001f\u0010\u0017\u001a\u0004\b \u0010!\"\u0014\u0010\"\u001a\b\u0012\u0004\u0012\u00020\u00070#X\u0082\u0004¢\u0006\u0002\n\u0000\u0082\u0002\u0007\n\u0005\b\u009920\u0001¨\u0006m" }, d2 = { "INVALID_SNAPSHOT", "", "applyObservers", "", "Lkotlin/Function2;", "", "", "Landroidx/compose/runtime/snapshots/Snapshot;", "", "currentGlobalSnapshot", "Ljava/util/concurrent/atomic/AtomicReference;", "Landroidx/compose/runtime/snapshots/GlobalSnapshot;", "kotlin.jvm.PlatformType", "Landroidx/compose/runtime/AtomicReference;", "emptyLambda", "Lkotlin/Function1;", "Landroidx/compose/runtime/snapshots/SnapshotIdSet;", "Lkotlin/ParameterName;", "name", "invalid", "globalWriteObservers", "lock", "getLock$annotations", "()V", "getLock", "()Ljava/lang/Object;", "nextSnapshotId", "openSnapshots", "pinningTable", "Landroidx/compose/runtime/snapshots/SnapshotDoubleIndexHeap;", "snapshotInitializer", "getSnapshotInitializer$annotations", "getSnapshotInitializer", "()Landroidx/compose/runtime/snapshots/Snapshot;", "threadSnapshot", "Landroidx/compose/runtime/SnapshotThreadLocal;", "advanceGlobalSnapshot", "T", "block", "(Lkotlin/jvm/functions/Function1;)Ljava/lang/Object;", "createTransparentSnapshotWithNoParentReadObserver", "previousSnapshot", "readObserver", "ownsPreviousSnapshot", "", "current", "Landroidx/compose/runtime/snapshots/StateRecord;", "r", "(Landroidx/compose/runtime/snapshots/StateRecord;)Landroidx/compose/runtime/snapshots/StateRecord;", "snapshot", "(Landroidx/compose/runtime/snapshots/StateRecord;Landroidx/compose/runtime/snapshots/Snapshot;)Landroidx/compose/runtime/snapshots/StateRecord;", "currentSnapshot", "mergedReadObserver", "parentObserver", "mergeReadObserver", "mergedWriteObserver", "writeObserver", "notifyWrite", "state", "Landroidx/compose/runtime/snapshots/StateObject;", "optimisticMerges", "", "Landroidx/compose/runtime/snapshots/MutableSnapshot;", "applyingSnapshot", "invalidSnapshots", "overwriteUnusedRecordsLocked", "readError", "", "readable", "id", "(Landroidx/compose/runtime/snapshots/StateRecord;ILandroidx/compose/runtime/snapshots/SnapshotIdSet;)Landroidx/compose/runtime/snapshots/StateRecord;", "releasePinningLocked", "handle", "reportReadonlySnapshotWrite", "sync", "Lkotlin/Function0;", "(Lkotlin/jvm/functions/Function0;)Ljava/lang/Object;", "takeNewGlobalSnapshot", "previousGlobalSnapshot", "(Landroidx/compose/runtime/snapshots/Snapshot;Lkotlin/jvm/functions/Function1;)Ljava/lang/Object;", "takeNewSnapshot", "(Lkotlin/jvm/functions/Function1;)Landroidx/compose/runtime/snapshots/Snapshot;", "trackPinning", "usedLocked", "valid", "data", "candidateSnapshot", "validateOpen", "addRange", "from", "until", "newOverwritableRecordLocked", "(Landroidx/compose/runtime/snapshots/StateRecord;Landroidx/compose/runtime/snapshots/StateObject;)Landroidx/compose/runtime/snapshots/StateRecord;", "newWritableRecord", "(Landroidx/compose/runtime/snapshots/StateRecord;Landroidx/compose/runtime/snapshots/StateObject;Landroidx/compose/runtime/snapshots/Snapshot;)Landroidx/compose/runtime/snapshots/StateRecord;", "newWritableRecordLocked", "overwritable", "R", "candidate", "Lkotlin/ExtensionFunctionType;", "(Landroidx/compose/runtime/snapshots/StateRecord;Landroidx/compose/runtime/snapshots/StateObject;Landroidx/compose/runtime/snapshots/StateRecord;Lkotlin/jvm/functions/Function1;)Ljava/lang/Object;", "overwritableRecord", "(Landroidx/compose/runtime/snapshots/StateRecord;Landroidx/compose/runtime/snapshots/StateObject;Landroidx/compose/runtime/snapshots/Snapshot;Landroidx/compose/runtime/snapshots/StateRecord;)Landroidx/compose/runtime/snapshots/StateRecord;", "withCurrent", "(Landroidx/compose/runtime/snapshots/StateRecord;Lkotlin/jvm/functions/Function1;)Ljava/lang/Object;", "writable", "(Landroidx/compose/runtime/snapshots/StateRecord;Landroidx/compose/runtime/snapshots/StateObject;Lkotlin/jvm/functions/Function1;)Ljava/lang/Object;", "(Landroidx/compose/runtime/snapshots/StateRecord;Landroidx/compose/runtime/snapshots/StateObject;Landroidx/compose/runtime/snapshots/Snapshot;Lkotlin/jvm/functions/Function1;)Ljava/lang/Object;", "writableRecord", "runtime_release" }, k = 2, mv = { 1, 8, 0 }, xi = 48)
public final class SnapshotKt
{
    private static final int INVALID_SNAPSHOT = 0;
    private static final List<Function2<Set<?>, Snapshot, Unit>> applyObservers;
    private static final AtomicReference<GlobalSnapshot> currentGlobalSnapshot;
    private static final Function1<SnapshotIdSet, Unit> emptyLambda;
    private static final List<Function1<Object, Unit>> globalWriteObservers;
    private static final Object lock;
    private static int nextSnapshotId;
    private static SnapshotIdSet openSnapshots;
    private static final SnapshotDoubleIndexHeap pinningTable;
    private static final Snapshot snapshotInitializer;
    private static final SnapshotThreadLocal<Snapshot> threadSnapshot;
    
    static {
        emptyLambda = (Function1)SnapshotKt$emptyLambda.SnapshotKt$emptyLambda$1.INSTANCE;
        threadSnapshot = new SnapshotThreadLocal<Snapshot>();
        lock = new Object();
        SnapshotKt.openSnapshots = SnapshotIdSet.Companion.getEMPTY();
        SnapshotKt.nextSnapshotId = 1;
        pinningTable = new SnapshotDoubleIndexHeap();
        applyObservers = new ArrayList<Function2<Set<?>, Snapshot, Unit>>();
        globalWriteObservers = new ArrayList<Function1<Object, Unit>>();
        final int nextSnapshotId = SnapshotKt.nextSnapshotId;
        SnapshotKt.nextSnapshotId = nextSnapshotId + 1;
        final GlobalSnapshot initialValue = new GlobalSnapshot(nextSnapshotId, SnapshotIdSet.Companion.getEMPTY());
        SnapshotKt.openSnapshots = SnapshotKt.openSnapshots.set(initialValue.getId());
        final GlobalSnapshot value = (currentGlobalSnapshot = new AtomicReference<GlobalSnapshot>(initialValue)).get();
        Intrinsics.checkNotNullExpressionValue((Object)value, "currentGlobalSnapshot.get()");
        snapshotInitializer = value;
    }
    
    public static final SnapshotIdSet addRange(SnapshotIdSet set, int i, final int n) {
        Intrinsics.checkNotNullParameter((Object)set, "<this>");
        while (i < n) {
            set = set.set(i);
            ++i;
        }
        return set;
    }
    
    private static final <T> T advanceGlobalSnapshot(final Function1<? super SnapshotIdSet, ? extends T> function1) {
        final Snapshot snapshotInitializer = SnapshotKt.snapshotInitializer;
        Intrinsics.checkNotNull((Object)snapshotInitializer, "null cannot be cast to non-null type androidx.compose.runtime.snapshots.GlobalSnapshot");
        final GlobalSnapshot globalSnapshot = (GlobalSnapshot)snapshotInitializer;
        Object o = getLock();
        synchronized (o) {
            final GlobalSnapshot value = SnapshotKt.currentGlobalSnapshot.get();
            Intrinsics.checkNotNullExpressionValue((Object)value, "currentGlobalSnapshot.get()");
            final Object takeNewGlobalSnapshot = takeNewGlobalSnapshot((Snapshot)value, (kotlin.jvm.functions.Function1<? super SnapshotIdSet, ?>)function1);
            monitorexit(o);
            final Set<StateObject> modified$runtime_release = value.getModified$runtime_release();
            if (modified$runtime_release != null) {
                synchronized (getLock()) {
                    final List mutableList = CollectionsKt.toMutableList((Collection)SnapshotKt.applyObservers);
                    monitorexit(getLock());
                    for (int i = 0; i < mutableList.size(); ++i) {
                        ((Function2)mutableList.get(i)).invoke((Object)modified$runtime_release, (Object)value);
                    }
                }
            }
            final Object lock = getLock();
            monitorenter(lock);
            if (modified$runtime_release != null) {
                try {
                    o = modified$runtime_release.iterator();
                    while (((Iterator)o).hasNext()) {
                        overwriteUnusedRecordsLocked((StateObject)((Iterator)o).next());
                    }
                    o = Unit.INSTANCE;
                }
                finally {
                    monitorexit(lock);
                }
            }
            return (T)takeNewGlobalSnapshot;
        }
    }
    
    private static final void advanceGlobalSnapshot() {
        advanceGlobalSnapshot((kotlin.jvm.functions.Function1<? super SnapshotIdSet, ?>)SnapshotKt$advanceGlobalSnapshot.SnapshotKt$advanceGlobalSnapshot$3.INSTANCE);
    }
    
    private static final Snapshot createTransparentSnapshotWithNoParentReadObserver(Snapshot snapshot, final Function1<Object, Unit> function1, final boolean b) {
        final boolean b2 = snapshot instanceof MutableSnapshot;
        if (!b2 && snapshot != null) {
            snapshot = new TransparentObserverSnapshot(snapshot, function1, false, b);
        }
        else {
            MutableSnapshot mutableSnapshot;
            if (b2) {
                mutableSnapshot = (MutableSnapshot)snapshot;
            }
            else {
                mutableSnapshot = null;
            }
            snapshot = new TransparentObserverMutableSnapshot(mutableSnapshot, function1, null, false, b);
        }
        return snapshot;
    }
    
    public static final <T extends StateRecord> T current(final T t) {
        Intrinsics.checkNotNullParameter((Object)t, "r");
        final Snapshot current = Snapshot.Companion.getCurrent();
        Object o;
        if ((o = readable(t, current.getId(), current.getInvalid$runtime_release())) == null) {
            synchronized (getLock()) {
                o = Snapshot.Companion.getCurrent();
                o = readable(t, ((Snapshot)o).getId(), ((Snapshot)o).getInvalid$runtime_release());
                monitorexit(getLock());
                if (o == null) {
                    readError();
                    throw new KotlinNothingValueException();
                }
            }
        }
        return (T)o;
    }
    
    public static final <T extends StateRecord> T current(final T t, final Snapshot snapshot) {
        Intrinsics.checkNotNullParameter((Object)t, "r");
        Intrinsics.checkNotNullParameter((Object)snapshot, "snapshot");
        final StateRecord readable = readable(t, snapshot.getId(), snapshot.getInvalid$runtime_release());
        if (readable != null) {
            return (T)readable;
        }
        readError();
        throw new KotlinNothingValueException();
    }
    
    public static final Snapshot currentSnapshot() {
        Snapshot snapshot;
        if ((snapshot = SnapshotKt.threadSnapshot.get()) == null) {
            final GlobalSnapshot value = SnapshotKt.currentGlobalSnapshot.get();
            Intrinsics.checkNotNullExpressionValue((Object)value, "currentGlobalSnapshot.get()");
            snapshot = value;
        }
        return snapshot;
    }
    
    public static final Object getLock() {
        return SnapshotKt.lock;
    }
    
    public static final Snapshot getSnapshotInitializer() {
        return SnapshotKt.snapshotInitializer;
    }
    
    private static final Function1<Object, Unit> mergedReadObserver(final Function1<Object, Unit> function1, Function1<Object, Unit> function2, final boolean b) {
        Function1 function3;
        if (b) {
            function3 = function2;
        }
        else {
            function3 = null;
        }
        if (function1 != null && function3 != null && !Intrinsics.areEqual((Object)function1, (Object)function3)) {
            function2 = (Function1)new SnapshotKt$mergedReadObserver.SnapshotKt$mergedReadObserver$1((Function1)function1, function3);
        }
        else if ((function2 = function1) == null) {
            function2 = function3;
        }
        return (Function1<Object, Unit>)function2;
    }
    
    private static final Function1<Object, Unit> mergedWriteObserver(final Function1<Object, Unit> function1, final Function1<Object, Unit> function2) {
        Function1 function3;
        if (function1 != null && function2 != null && !Intrinsics.areEqual((Object)function1, (Object)function2)) {
            function3 = (Function1)new SnapshotKt$mergedWriteObserver.SnapshotKt$mergedWriteObserver$1((Function1)function1, (Function1)function2);
        }
        else if ((function3 = function1) == null) {
            function3 = function2;
        }
        return (Function1<Object, Unit>)function3;
    }
    
    public static final <T extends StateRecord> T newOverwritableRecordLocked(final T t, final StateObject stateObject) {
        Intrinsics.checkNotNullParameter((Object)t, "<this>");
        Intrinsics.checkNotNullParameter((Object)stateObject, "state");
        final StateRecord usedLocked = usedLocked(stateObject);
        StateRecord create;
        if (usedLocked != null) {
            usedLocked.setSnapshotId$runtime_release(Integer.MAX_VALUE);
            create = usedLocked;
        }
        else {
            create = t.create();
            create.setSnapshotId$runtime_release(Integer.MAX_VALUE);
            create.setNext$runtime_release(stateObject.getFirstStateRecord());
            Intrinsics.checkNotNull((Object)create, "null cannot be cast to non-null type T of androidx.compose.runtime.snapshots.SnapshotKt.newOverwritableRecordLocked$lambda$13");
            stateObject.prependStateRecord(create);
            Intrinsics.checkNotNull((Object)create, "null cannot be cast to non-null type T of androidx.compose.runtime.snapshots.SnapshotKt.newOverwritableRecordLocked");
        }
        return (T)create;
    }
    
    public static final <T extends StateRecord> T newWritableRecord(final T t, final StateObject stateObject, final Snapshot snapshot) {
        Intrinsics.checkNotNullParameter((Object)t, "<this>");
        Intrinsics.checkNotNullParameter((Object)stateObject, "state");
        Intrinsics.checkNotNullParameter((Object)snapshot, "snapshot");
        synchronized (getLock()) {
            return (T)newWritableRecordLocked((StateRecord)t, stateObject, snapshot);
        }
    }
    
    private static final <T extends StateRecord> T newWritableRecordLocked(final T t, final StateObject stateObject, final Snapshot snapshot) {
        final StateRecord overwritableRecordLocked = newOverwritableRecordLocked(t, stateObject);
        overwritableRecordLocked.assign(t);
        overwritableRecordLocked.setSnapshotId$runtime_release(snapshot.getId());
        return (T)overwritableRecordLocked;
    }
    
    public static final void notifyWrite(final Snapshot snapshot, final StateObject stateObject) {
        Intrinsics.checkNotNullParameter((Object)snapshot, "snapshot");
        Intrinsics.checkNotNullParameter((Object)stateObject, "state");
        final Function1<Object, Unit> writeObserver$runtime_release = snapshot.getWriteObserver$runtime_release();
        if (writeObserver$runtime_release != null) {
            writeObserver$runtime_release.invoke((Object)stateObject);
        }
    }
    
    private static final Map<StateRecord, StateRecord> optimisticMerges(final MutableSnapshot mutableSnapshot, final MutableSnapshot mutableSnapshot2, final SnapshotIdSet set) {
        final Set<StateObject> modified$runtime_release = mutableSnapshot2.getModified$runtime_release();
        final int id = mutableSnapshot.getId();
        if (modified$runtime_release == null) {
            return null;
        }
        final SnapshotIdSet or = mutableSnapshot2.getInvalid$runtime_release().set(mutableSnapshot2.getId()).or(mutableSnapshot2.getPreviousIds$runtime_release());
        final Iterator<StateObject> iterator = modified$runtime_release.iterator();
        Map map = null;
        while (iterator.hasNext()) {
            final StateObject stateObject = iterator.next();
            final StateRecord firstStateRecord = stateObject.getFirstStateRecord();
            final StateRecord readable = readable(firstStateRecord, id, set);
            if (readable == null) {
                continue;
            }
            final StateRecord readable2 = readable(firstStateRecord, id, or);
            if (readable2 == null) {
                continue;
            }
            if (Intrinsics.areEqual((Object)readable, (Object)readable2)) {
                continue;
            }
            final StateRecord readable3 = readable(firstStateRecord, mutableSnapshot2.getId(), mutableSnapshot2.getInvalid$runtime_release());
            if (readable3 == null) {
                readError();
                throw new KotlinNothingValueException();
            }
            final StateRecord mergeRecords = stateObject.mergeRecords(readable2, readable, readable3);
            if (mergeRecords == null) {
                return null;
            }
            final Map map2 = map;
            Map map3 = map;
            Map map4;
            if ((map4 = map2) == null) {
                map3 = new HashMap();
                map4 = map3;
            }
            map4.put(readable, mergeRecords);
            map = map3;
        }
        return map;
    }
    
    public static final <T extends StateRecord, R> R overwritable(final T t, final StateObject stateObject, final T t2, final Function1<? super T, ? extends R> function1) {
        Intrinsics.checkNotNullParameter((Object)t, "<this>");
        Intrinsics.checkNotNullParameter((Object)stateObject, "state");
        Intrinsics.checkNotNullParameter((Object)t2, "candidate");
        Intrinsics.checkNotNullParameter((Object)function1, "block");
        getSnapshotInitializer();
        final Object lock = getLock();
        monitorenter(lock);
        try {
            final Snapshot current = Snapshot.Companion.getCurrent();
            final Object invoke = function1.invoke((Object)overwritableRecord(t, stateObject, current, t2));
            InlineMarker.finallyStart(1);
            monitorexit(lock);
            InlineMarker.finallyEnd(1);
            notifyWrite(current, stateObject);
            return (R)invoke;
        }
        finally {
            InlineMarker.finallyStart(1);
            monitorexit(lock);
            InlineMarker.finallyEnd(1);
        }
    }
    
    public static final <T extends StateRecord> T overwritableRecord(final T t, final StateObject stateObject, final Snapshot snapshot, final T t2) {
        Intrinsics.checkNotNullParameter((Object)t, "<this>");
        Intrinsics.checkNotNullParameter((Object)stateObject, "state");
        Intrinsics.checkNotNullParameter((Object)snapshot, "snapshot");
        Intrinsics.checkNotNullParameter((Object)t2, "candidate");
        if (snapshot.getReadOnly()) {
            snapshot.recordModified$runtime_release(stateObject);
        }
        final int id = snapshot.getId();
        if (t2.getSnapshotId$runtime_release() == id) {
            return t2;
        }
        synchronized (getLock()) {
            final StateRecord overwritableRecordLocked = newOverwritableRecordLocked(t, stateObject);
            monitorexit(getLock());
            overwritableRecordLocked.setSnapshotId$runtime_release(id);
            snapshot.recordModified$runtime_release(stateObject);
            return (T)overwritableRecordLocked;
        }
    }
    
    private static final boolean overwriteUnusedRecordsLocked(final StateObject stateObject) {
        StateRecord stateRecord = stateObject.getFirstStateRecord();
        final int lowestOrDefault = SnapshotKt.pinningTable.lowestOrDefault(SnapshotKt.nextSnapshotId);
        boolean b = true;
        StateRecord stateRecord2 = null;
        int n = 0;
        while (stateRecord != null) {
            final int snapshotId$runtime_release = stateRecord.getSnapshotId$runtime_release();
            StateRecord stateRecord3 = stateRecord2;
            int n2 = n;
            if (snapshotId$runtime_release != 0) {
                if (snapshotId$runtime_release <= lowestOrDefault - 1) {
                    if (stateRecord2 == null) {
                        stateRecord3 = stateRecord;
                        n2 = n;
                    }
                    else {
                        if (stateRecord.getSnapshotId$runtime_release() < stateRecord2.getSnapshotId$runtime_release()) {
                            stateRecord3 = stateRecord2;
                            stateRecord2 = stateRecord;
                        }
                        else {
                            stateRecord3 = stateRecord;
                        }
                        stateRecord2.setSnapshotId$runtime_release(0);
                        stateRecord2.assign(stateRecord3);
                        n2 = n;
                    }
                }
                else {
                    n2 = n + 1;
                    stateRecord3 = stateRecord2;
                }
            }
            stateRecord = stateRecord.getNext$runtime_release();
            stateRecord2 = stateRecord3;
            n = n2;
        }
        if (n >= 1) {
            b = false;
        }
        return b;
    }
    
    private static final Void readError() {
        throw new IllegalStateException("Reading a state that was created after the snapshot was taken or in a snapshot that has not yet been applied".toString());
    }
    
    private static final <T extends StateRecord> T readable(T next$runtime_release, final int n, final SnapshotIdSet set) {
        StateRecord stateRecord = null;
        while (next$runtime_release != null) {
            StateRecord stateRecord2 = stateRecord;
            Label_0044: {
                if (valid(next$runtime_release, n, set)) {
                    if (stateRecord != null) {
                        stateRecord2 = stateRecord;
                        if (stateRecord.getSnapshotId$runtime_release() >= next$runtime_release.getSnapshotId$runtime_release()) {
                            break Label_0044;
                        }
                    }
                    stateRecord2 = next$runtime_release;
                }
            }
            next$runtime_release = (T)next$runtime_release.getNext$runtime_release();
            stateRecord = stateRecord2;
        }
        if (stateRecord != null) {
            return (T)stateRecord;
        }
        return null;
    }
    
    public static final <T extends StateRecord> T readable(final T t, final StateObject stateObject) {
        Intrinsics.checkNotNullParameter((Object)t, "<this>");
        Intrinsics.checkNotNullParameter((Object)stateObject, "state");
        final Snapshot current = Snapshot.Companion.getCurrent();
        final Function1<Object, Unit> readObserver$runtime_release = current.getReadObserver$runtime_release();
        if (readObserver$runtime_release != null) {
            readObserver$runtime_release.invoke((Object)stateObject);
        }
        final StateRecord readable;
        if ((readable = readable(t, current.getId(), current.getInvalid$runtime_release())) == null) {
            synchronized (getLock()) {
                final Snapshot current2 = Snapshot.Companion.getCurrent();
                final StateRecord firstStateRecord = stateObject.getFirstStateRecord();
                Intrinsics.checkNotNull((Object)firstStateRecord, "null cannot be cast to non-null type T of androidx.compose.runtime.snapshots.SnapshotKt.readable$lambda$7");
                if (readable(firstStateRecord, current2.getId(), current2.getInvalid$runtime_release()) == null) {
                    readError();
                    throw new KotlinNothingValueException();
                }
            }
        }
        return (T)readable;
    }
    
    public static final <T extends StateRecord> T readable(final T t, final StateObject stateObject, final Snapshot snapshot) {
        Intrinsics.checkNotNullParameter((Object)t, "<this>");
        Intrinsics.checkNotNullParameter((Object)stateObject, "state");
        Intrinsics.checkNotNullParameter((Object)snapshot, "snapshot");
        final Function1<Object, Unit> readObserver$runtime_release = snapshot.getReadObserver$runtime_release();
        if (readObserver$runtime_release != null) {
            readObserver$runtime_release.invoke((Object)stateObject);
        }
        final StateRecord readable = readable(t, snapshot.getId(), snapshot.getInvalid$runtime_release());
        if (readable != null) {
            return (T)readable;
        }
        readError();
        throw new KotlinNothingValueException();
    }
    
    public static final void releasePinningLocked(final int n) {
        SnapshotKt.pinningTable.remove(n);
    }
    
    private static final Void reportReadonlySnapshotWrite() {
        throw new IllegalStateException("Cannot modify a state object in a read-only snapshot".toString());
    }
    
    public static final <T> T sync(final Function0<? extends T> function0) {
        Intrinsics.checkNotNullParameter((Object)function0, "block");
        final Object lock = getLock();
        monitorenter(lock);
        try {
            return (T)function0.invoke();
        }
        finally {
            InlineMarker.finallyStart(1);
            monitorexit(lock);
            InlineMarker.finallyEnd(1);
        }
    }
    
    private static final <T> T takeNewGlobalSnapshot(final Snapshot snapshot, final Function1<? super SnapshotIdSet, ? extends T> function1) {
        final Object invoke = function1.invoke((Object)SnapshotKt.openSnapshots.clear(snapshot.getId()));
        synchronized (getLock()) {
            final int nextSnapshotId = SnapshotKt.nextSnapshotId;
            SnapshotKt.nextSnapshotId = nextSnapshotId + 1;
            SnapshotKt.openSnapshots = SnapshotKt.openSnapshots.clear(snapshot.getId());
            SnapshotKt.currentGlobalSnapshot.set(new GlobalSnapshot(nextSnapshotId, SnapshotKt.openSnapshots));
            snapshot.dispose();
            SnapshotKt.openSnapshots = SnapshotKt.openSnapshots.set(nextSnapshotId);
            final Unit instance = Unit.INSTANCE;
            return (T)invoke;
        }
    }
    
    private static final <T extends Snapshot> T takeNewSnapshot(final Function1<? super SnapshotIdSet, ? extends T> function1) {
        return advanceGlobalSnapshot((kotlin.jvm.functions.Function1<? super SnapshotIdSet, ? extends T>)new SnapshotKt$takeNewSnapshot.SnapshotKt$takeNewSnapshot$1((Function1)function1));
    }
    
    public static final int trackPinning(int n, final SnapshotIdSet set) {
        Intrinsics.checkNotNullParameter((Object)set, "invalid");
        n = set.lowest(n);
        synchronized (getLock()) {
            n = SnapshotKt.pinningTable.add(n);
            return n;
        }
    }
    
    private static final StateRecord usedLocked(final StateObject stateObject) {
        StateRecord stateRecord = stateObject.getFirstStateRecord();
        final int lowestOrDefault = SnapshotKt.pinningTable.lowestOrDefault(SnapshotKt.nextSnapshotId);
        final SnapshotIdSet empty = SnapshotIdSet.Companion.getEMPTY();
        StateRecord stateRecord2 = null;
        while (stateRecord != null) {
            if (stateRecord.getSnapshotId$runtime_release() == 0) {
                return stateRecord;
            }
            StateRecord stateRecord3 = stateRecord2;
            if (valid(stateRecord, lowestOrDefault - 1, empty)) {
                if (stateRecord2 != null) {
                    if (stateRecord.getSnapshotId$runtime_release() >= stateRecord2.getSnapshotId$runtime_release()) {
                        stateRecord = stateRecord2;
                    }
                    return stateRecord;
                }
                stateRecord3 = stateRecord;
            }
            stateRecord = stateRecord.getNext$runtime_release();
            stateRecord2 = stateRecord3;
        }
        return null;
    }
    
    private static final boolean valid(final int n, final int n2, final SnapshotIdSet set) {
        return n2 != 0 && n2 <= n && !set.get(n2);
    }
    
    private static final boolean valid(final StateRecord stateRecord, final int n, final SnapshotIdSet set) {
        return valid(n, stateRecord.getSnapshotId$runtime_release(), set);
    }
    
    private static final void validateOpen(final Snapshot snapshot) {
        if (SnapshotKt.openSnapshots.get(snapshot.getId())) {
            return;
        }
        throw new IllegalStateException("Snapshot is not open".toString());
    }
    
    public static final <T extends StateRecord, R> R withCurrent(final T t, final Function1<? super T, ? extends R> function1) {
        Intrinsics.checkNotNullParameter((Object)t, "<this>");
        Intrinsics.checkNotNullParameter((Object)function1, "block");
        return (R)function1.invoke((Object)current(t));
    }
    
    public static final <T extends StateRecord, R> R writable(final T t, final StateObject stateObject, final Snapshot snapshot, final Function1<? super T, ? extends R> function1) {
        Intrinsics.checkNotNullParameter((Object)t, "<this>");
        Intrinsics.checkNotNullParameter((Object)stateObject, "state");
        Intrinsics.checkNotNullParameter((Object)snapshot, "snapshot");
        Intrinsics.checkNotNullParameter((Object)function1, "block");
        final Object lock = getLock();
        monitorenter(lock);
        try {
            final Object invoke = function1.invoke((Object)writableRecord(t, stateObject, snapshot));
            InlineMarker.finallyStart(1);
            monitorexit(lock);
            InlineMarker.finallyEnd(1);
            notifyWrite(snapshot, stateObject);
            return (R)invoke;
        }
        finally {
            InlineMarker.finallyStart(1);
            monitorexit(lock);
            InlineMarker.finallyEnd(1);
        }
    }
    
    public static final <T extends StateRecord, R> R writable(final T t, final StateObject stateObject, final Function1<? super T, ? extends R> function1) {
        Intrinsics.checkNotNullParameter((Object)t, "<this>");
        Intrinsics.checkNotNullParameter((Object)stateObject, "state");
        Intrinsics.checkNotNullParameter((Object)function1, "block");
        getSnapshotInitializer();
        final Object lock = getLock();
        monitorenter(lock);
        try {
            final Snapshot current = Snapshot.Companion.getCurrent();
            final Object invoke = function1.invoke((Object)writableRecord(t, stateObject, current));
            InlineMarker.finallyStart(1);
            monitorexit(lock);
            InlineMarker.finallyEnd(1);
            notifyWrite(current, stateObject);
            return (R)invoke;
        }
        finally {
            InlineMarker.finallyStart(1);
            monitorexit(lock);
            InlineMarker.finallyEnd(1);
        }
    }
    
    public static final <T extends StateRecord> T writableRecord(final T t, final StateObject stateObject, final Snapshot snapshot) {
        Intrinsics.checkNotNullParameter((Object)t, "<this>");
        Intrinsics.checkNotNullParameter((Object)stateObject, "state");
        Intrinsics.checkNotNullParameter((Object)snapshot, "snapshot");
        if (snapshot.getReadOnly()) {
            snapshot.recordModified$runtime_release(stateObject);
        }
        final StateRecord readable = readable(t, snapshot.getId(), snapshot.getInvalid$runtime_release());
        if (readable == null) {
            readError();
            throw new KotlinNothingValueException();
        }
        if (readable.getSnapshotId$runtime_release() == snapshot.getId()) {
            return (T)readable;
        }
        final StateRecord writableRecord = newWritableRecord(readable, stateObject, snapshot);
        snapshot.recordModified$runtime_release(stateObject);
        return (T)writableRecord;
    }
}

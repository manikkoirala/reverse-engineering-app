// 
// Decompiled by Procyon v0.6.0
// 

package androidx.compose.runtime.snapshots;

import kotlin.jvm.internal.CollectionToArray;
import kotlin.jvm.internal.Ref$IntRef;
import java.util.ListIterator;
import kotlin.collections.IntIterator;
import kotlin.ranges.RangesKt;
import java.util.Iterator;
import java.util.Collection;
import java.util.ConcurrentModificationException;
import kotlin.jvm.internal.Intrinsics;
import kotlin.Metadata;
import kotlin.jvm.internal.markers.KMutableList;
import java.util.List;

@Metadata(d1 = { "\u0000B\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010!\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u000b\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0010\u001e\n\u0002\b\t\n\u0002\u0010)\n\u0002\b\u0002\n\u0002\u0010+\n\u0002\b\t\b\u0002\u0018\u0000*\u0004\b\u0000\u0010\u00012\b\u0012\u0004\u0012\u0002H\u00010\u0002B#\u0012\f\u0010\u0003\u001a\b\u0012\u0004\u0012\u00028\u00000\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0006¢\u0006\u0002\u0010\bJ\u0015\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00028\u0000H\u0016¢\u0006\u0002\u0010\u0014J\u001d\u0010\u0011\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\u00062\u0006\u0010\u0013\u001a\u00028\u0000H\u0016¢\u0006\u0002\u0010\u0017J\u001e\u0010\u0018\u001a\u00020\u00122\u0006\u0010\u0016\u001a\u00020\u00062\f\u0010\u0019\u001a\b\u0012\u0004\u0012\u00028\u00000\u001aH\u0016J\u0016\u0010\u0018\u001a\u00020\u00122\f\u0010\u0019\u001a\b\u0012\u0004\u0012\u00028\u00000\u001aH\u0016J\b\u0010\u001b\u001a\u00020\u0015H\u0016J\u0016\u0010\u001c\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00028\u0000H\u0096\u0002¢\u0006\u0002\u0010\u0014J\u0016\u0010\u001d\u001a\u00020\u00122\f\u0010\u0019\u001a\b\u0012\u0004\u0012\u00028\u00000\u001aH\u0016J\u0016\u0010\u001e\u001a\u00028\u00002\u0006\u0010\u0016\u001a\u00020\u0006H\u0096\u0002¢\u0006\u0002\u0010\u001fJ\u0015\u0010 \u001a\u00020\u00062\u0006\u0010\u0013\u001a\u00028\u0000H\u0016¢\u0006\u0002\u0010!J\b\u0010\"\u001a\u00020\u0012H\u0016J\u000f\u0010#\u001a\b\u0012\u0004\u0012\u00028\u00000$H\u0096\u0002J\u0015\u0010%\u001a\u00020\u00062\u0006\u0010\u0013\u001a\u00028\u0000H\u0016¢\u0006\u0002\u0010!J\u000e\u0010&\u001a\b\u0012\u0004\u0012\u00028\u00000'H\u0016J\u0016\u0010&\u001a\b\u0012\u0004\u0012\u00028\u00000'2\u0006\u0010\u0016\u001a\u00020\u0006H\u0016J\u0015\u0010(\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00028\u0000H\u0016¢\u0006\u0002\u0010\u0014J\u0016\u0010)\u001a\u00020\u00122\f\u0010\u0019\u001a\b\u0012\u0004\u0012\u00028\u00000\u001aH\u0016J\u0015\u0010*\u001a\u00028\u00002\u0006\u0010\u0016\u001a\u00020\u0006H\u0016¢\u0006\u0002\u0010\u001fJ\u0016\u0010+\u001a\u00020\u00122\f\u0010\u0019\u001a\b\u0012\u0004\u0012\u00028\u00000\u001aH\u0016J\u001e\u0010,\u001a\u00028\u00002\u0006\u0010\u0016\u001a\u00020\u00062\u0006\u0010\u0013\u001a\u00028\u0000H\u0096\u0002¢\u0006\u0002\u0010-J\u001e\u0010.\u001a\b\u0012\u0004\u0012\u00028\u00000\u00022\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0006H\u0016J\b\u0010/\u001a\u00020\u0015H\u0002R\u000e\u0010\t\u001a\u00020\u0006X\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u0006X\u0082\u0004¢\u0006\u0002\n\u0000R\u0017\u0010\u0003\u001a\b\u0012\u0004\u0012\u00028\u00000\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u001e\u0010\u000e\u001a\u00020\u00062\u0006\u0010\r\u001a\u00020\u0006@RX\u0096\u000e¢\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u0010¨\u00060" }, d2 = { "Landroidx/compose/runtime/snapshots/SubList;", "T", "", "parentList", "Landroidx/compose/runtime/snapshots/SnapshotStateList;", "fromIndex", "", "toIndex", "(Landroidx/compose/runtime/snapshots/SnapshotStateList;II)V", "modification", "offset", "getParentList", "()Landroidx/compose/runtime/snapshots/SnapshotStateList;", "<set-?>", "size", "getSize", "()I", "add", "", "element", "(Ljava/lang/Object;)Z", "", "index", "(ILjava/lang/Object;)V", "addAll", "elements", "", "clear", "contains", "containsAll", "get", "(I)Ljava/lang/Object;", "indexOf", "(Ljava/lang/Object;)I", "isEmpty", "iterator", "", "lastIndexOf", "listIterator", "", "remove", "removeAll", "removeAt", "retainAll", "set", "(ILjava/lang/Object;)Ljava/lang/Object;", "subList", "validateModification", "runtime_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
final class SubList<T> implements List<T>, KMutableList
{
    private int modification;
    private final int offset;
    private final SnapshotStateList<T> parentList;
    private int size;
    
    public SubList(final SnapshotStateList<T> parentList, final int offset, final int n) {
        Intrinsics.checkNotNullParameter((Object)parentList, "parentList");
        this.parentList = parentList;
        this.offset = offset;
        this.modification = parentList.getModification$runtime_release();
        this.size = n - offset;
    }
    
    private final void validateModification() {
        if (this.parentList.getModification$runtime_release() == this.modification) {
            return;
        }
        throw new ConcurrentModificationException();
    }
    
    @Override
    public void add(final int n, final T t) {
        this.validateModification();
        this.parentList.add(this.offset + n, t);
        this.size = this.size() + 1;
        this.modification = this.parentList.getModification$runtime_release();
    }
    
    @Override
    public boolean add(final T t) {
        this.validateModification();
        this.parentList.add(this.offset + this.size(), t);
        this.size = this.size() + 1;
        this.modification = this.parentList.getModification$runtime_release();
        return true;
    }
    
    @Override
    public boolean addAll(final int n, final Collection<? extends T> collection) {
        Intrinsics.checkNotNullParameter((Object)collection, "elements");
        this.validateModification();
        final boolean addAll = this.parentList.addAll(n + this.offset, collection);
        if (addAll) {
            this.size = this.size() + collection.size();
            this.modification = this.parentList.getModification$runtime_release();
        }
        return addAll;
    }
    
    @Override
    public boolean addAll(final Collection<? extends T> collection) {
        Intrinsics.checkNotNullParameter((Object)collection, "elements");
        return this.addAll(this.size(), collection);
    }
    
    @Override
    public void clear() {
        if (this.size() > 0) {
            this.validateModification();
            final SnapshotStateList<T> parentList = this.parentList;
            final int offset = this.offset;
            parentList.removeRange(offset, this.size() + offset);
            this.size = 0;
            this.modification = this.parentList.getModification$runtime_release();
        }
    }
    
    @Override
    public boolean contains(final Object o) {
        return this.indexOf(o) >= 0;
    }
    
    @Override
    public boolean containsAll(final Collection<?> collection) {
        Intrinsics.checkNotNullParameter((Object)collection, "elements");
        final Iterable iterable = collection;
        final boolean empty = ((Collection)iterable).isEmpty();
        final boolean b = true;
        boolean b2;
        if (empty) {
            b2 = b;
        }
        else {
            final Iterator iterator = iterable.iterator();
            do {
                b2 = b;
                if (iterator.hasNext()) {
                    continue;
                }
                return b2;
            } while (this.contains(iterator.next()));
            b2 = false;
        }
        return b2;
    }
    
    @Override
    public T get(final int n) {
        this.validateModification();
        SnapshotStateListKt.access$validateRange(n, this.size());
        return this.parentList.get(this.offset + n);
    }
    
    public final SnapshotStateList<T> getParentList() {
        return this.parentList;
    }
    
    public int getSize() {
        return this.size;
    }
    
    @Override
    public int indexOf(final Object o) {
        this.validateModification();
        final int offset = this.offset;
        final Iterator iterator = ((Iterable)RangesKt.until(offset, this.size() + offset)).iterator();
        while (iterator.hasNext()) {
            final int nextInt = ((IntIterator)iterator).nextInt();
            if (Intrinsics.areEqual(o, (Object)this.parentList.get(nextInt))) {
                return nextInt - this.offset;
            }
        }
        return -1;
    }
    
    @Override
    public boolean isEmpty() {
        return this.size() == 0;
    }
    
    @Override
    public Iterator<T> iterator() {
        return this.listIterator();
    }
    
    @Override
    public int lastIndexOf(final Object o) {
        this.validateModification();
        for (int i = this.offset + this.size() - 1; i >= this.offset; --i) {
            if (Intrinsics.areEqual(o, (Object)this.parentList.get(i))) {
                return i - this.offset;
            }
        }
        return -1;
    }
    
    @Override
    public ListIterator<T> listIterator() {
        return this.listIterator(0);
    }
    
    @Override
    public ListIterator<T> listIterator(final int n) {
        this.validateModification();
        final Ref$IntRef ref$IntRef = new Ref$IntRef();
        ref$IntRef.element = n - 1;
        return (ListIterator<T>)new SubList$listIterator.SubList$listIterator$1(ref$IntRef, this);
    }
    
    @Override
    public final /* bridge */ T remove(final int n) {
        return this.removeAt(n);
    }
    
    @Override
    public boolean remove(final Object o) {
        final int index = this.indexOf(o);
        boolean b;
        if (index >= 0) {
            this.remove(index);
            b = true;
        }
        else {
            b = false;
        }
        return b;
    }
    
    @Override
    public boolean removeAll(final Collection<?> collection) {
        Intrinsics.checkNotNullParameter((Object)collection, "elements");
        final Iterator<?> iterator = collection.iterator();
        boolean b = false;
    Label_0013:
        while (true) {
            b = false;
            while (iterator.hasNext()) {
                if (!this.remove(iterator.next()) && !b) {
                    continue Label_0013;
                }
                b = true;
            }
            break;
        }
        return b;
    }
    
    public T removeAt(final int n) {
        this.validateModification();
        final T remove = this.parentList.remove(this.offset + n);
        this.size = this.size() - 1;
        this.modification = this.parentList.getModification$runtime_release();
        return remove;
    }
    
    @Override
    public boolean retainAll(final Collection<?> collection) {
        Intrinsics.checkNotNullParameter((Object)collection, "elements");
        this.validateModification();
        final SnapshotStateList<T> parentList = this.parentList;
        final int offset = this.offset;
        final int retainAllInRange$runtime_release = parentList.retainAllInRange$runtime_release((Collection<? extends T>)collection, offset, this.size() + offset);
        if (retainAllInRange$runtime_release > 0) {
            this.modification = this.parentList.getModification$runtime_release();
            this.size = this.size() - retainAllInRange$runtime_release;
        }
        return retainAllInRange$runtime_release > 0;
    }
    
    @Override
    public T set(final int n, final T t) {
        SnapshotStateListKt.access$validateRange(n, this.size());
        this.validateModification();
        final T set = this.parentList.set(n + this.offset, t);
        this.modification = this.parentList.getModification$runtime_release();
        return set;
    }
    
    @Override
    public final /* bridge */ int size() {
        return this.getSize();
    }
    
    @Override
    public List<T> subList(final int n, final int n2) {
        final int n3 = 1;
        int n4;
        if (n >= 0 && n <= n2 && n2 <= this.size()) {
            n4 = n3;
        }
        else {
            n4 = 0;
        }
        if (n4 != 0) {
            this.validateModification();
            final SnapshotStateList<T> parentList = this.parentList;
            final int offset = this.offset;
            return new SubList((SnapshotStateList<Object>)parentList, n + offset, n2 + offset);
        }
        throw new IllegalArgumentException("Failed requirement.".toString());
    }
    
    @Override
    public Object[] toArray() {
        return CollectionToArray.toArray((Collection)this);
    }
    
    @Override
    public <T> T[] toArray(final T[] array) {
        Intrinsics.checkNotNullParameter((Object)array, "array");
        return (T[])CollectionToArray.toArray((Collection)this, (Object[])array);
    }
}

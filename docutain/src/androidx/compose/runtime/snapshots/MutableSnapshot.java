// 
// Decompiled by Procyon v0.6.0
// 

package androidx.compose.runtime.snapshots;

import kotlin.collections.ArraysKt;
import java.util.HashSet;
import java.util.ArrayList;
import java.util.List;
import kotlin.KotlinNothingValueException;
import kotlin.Pair;
import kotlin.jvm.functions.Function2;
import kotlin.TuplesKt;
import java.util.Collection;
import java.util.Map;
import kotlin.jvm.internal.InlineMarker;
import kotlin.jvm.functions.Function0;
import java.util.Iterator;
import kotlin.collections.CollectionsKt;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import java.util.Set;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000^\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0010#\n\u0002\u0018\u0002\n\u0002\b\u000b\n\u0002\u0010\u0015\n\u0002\b\u0012\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\b \b\u0017\u0018\u00002\u00020\u0001BC\b\u0000\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0014\u0010\u0006\u001a\u0010\u0012\u0004\u0012\u00020\b\u0012\u0004\u0012\u00020\t\u0018\u00010\u0007\u0012\u0014\u0010\n\u001a\u0010\u0012\u0004\u0012\u00020\b\u0012\u0004\u0012\u00020\t\u0018\u00010\u0007¢\u0006\u0002\u0010\u000bJ\b\u0010.\u001a\u00020\tH\u0002J\r\u0010/\u001a\u00020\tH\u0000¢\u0006\u0002\b0J'\u0010/\u001a\u0002H1\"\u0004\b\u0000\u001012\f\u00102\u001a\b\u0012\u0004\u0012\u0002H103H\u0080\b\u00f8\u0001\u0000¢\u0006\u0004\b0\u00104J\b\u00105\u001a\u000206H\u0016J\r\u00107\u001a\u00020\tH\u0010¢\u0006\u0002\b8J\b\u00109\u001a\u00020\tH\u0016J\b\u0010:\u001a\u00020\rH\u0016J3\u0010;\u001a\u0002062\u0006\u0010<\u001a\u00020\u00032\u0014\u0010=\u001a\u0010\u0012\u0004\u0012\u00020?\u0012\u0004\u0012\u00020?\u0018\u00010>2\u0006\u0010@\u001a\u00020\u0005H\u0000¢\u0006\u0002\bAJ\u0015\u0010B\u001a\u00020\t2\u0006\u0010C\u001a\u00020\u0001H\u0010¢\u0006\u0002\bDJ\u0015\u0010E\u001a\u00020\t2\u0006\u0010C\u001a\u00020\u0001H\u0010¢\u0006\u0002\bFJ\r\u0010G\u001a\u00020\tH\u0010¢\u0006\u0002\bHJ\u0015\u0010I\u001a\u00020\t2\u0006\u0010J\u001a\u00020\u0014H\u0010¢\u0006\u0002\bKJ\u0015\u0010L\u001a\u00020\t2\u0006\u0010\u0002\u001a\u00020\u0003H\u0000¢\u0006\u0002\bMJ\u0015\u0010N\u001a\u00020\t2\u0006\u0010,\u001a\u00020\u0005H\u0000¢\u0006\u0002\bOJ\u0015\u0010P\u001a\u00020\t2\u0006\u0010\u0002\u001a\u00020\u0003H\u0000¢\u0006\u0002\bQJ\u0015\u0010R\u001a\u00020\t2\u0006\u0010S\u001a\u00020 H\u0000¢\u0006\u0002\bTJ\r\u0010U\u001a\u00020\tH\u0010¢\u0006\u0002\bVJ\r\u0010W\u001a\u00020\tH\u0000¢\u0006\u0002\bXJ8\u0010Y\u001a\u00020\u00002\u0016\b\u0002\u0010\u0006\u001a\u0010\u0012\u0004\u0012\u00020\b\u0012\u0004\u0012\u00020\t\u0018\u00010\u00072\u0016\b\u0002\u0010\n\u001a\u0010\u0012\u0004\u0012\u00020\b\u0012\u0004\u0012\u00020\t\u0018\u00010\u0007H\u0016J\u001e\u0010Z\u001a\u00020\u00012\u0014\u0010\u0006\u001a\u0010\u0012\u0004\u0012\u00020\b\u0012\u0004\u0012\u00020\t\u0018\u00010\u0007H\u0016J\r\u0010[\u001a\u00020\tH\u0000¢\u0006\u0002\b\\J\r\u0010]\u001a\u00020\tH\u0000¢\u0006\u0002\b^R\u001a\u0010\f\u001a\u00020\rX\u0080\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u000e\u0010\u000f\"\u0004\b\u0010\u0010\u0011R4\u0010\u0015\u001a\n\u0012\u0004\u0012\u00020\u0014\u0018\u00010\u00132\u000e\u0010\u0012\u001a\n\u0012\u0004\u0012\u00020\u0014\u0018\u00010\u0013@VX\u0090\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0016\u0010\u0017\"\u0004\b\u0018\u0010\u0019R\u001a\u0010\u001a\u001a\u00020\u0005X\u0080\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u001b\u0010\u001c\"\u0004\b\u001d\u0010\u001eR\u001a\u0010\u001f\u001a\u00020 X\u0080\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b!\u0010\"\"\u0004\b#\u0010$R\"\u0010\u0006\u001a\u0010\u0012\u0004\u0012\u00020\b\u0012\u0004\u0012\u00020\t\u0018\u00010\u0007X\u0090\u0004¢\u0006\b\n\u0000\u001a\u0004\b%\u0010&R\u0014\u0010'\u001a\u00020\r8VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b(\u0010\u000fR\u0014\u0010)\u001a\u00020\u00018VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b*\u0010+R\u000e\u0010,\u001a\u00020\u0003X\u0082\u000e¢\u0006\u0002\n\u0000R\"\u0010\n\u001a\u0010\u0012\u0004\u0012\u00020\b\u0012\u0004\u0012\u00020\t\u0018\u00010\u0007X\u0090\u0004¢\u0006\b\n\u0000\u001a\u0004\b-\u0010&\u0082\u0002\u0007\n\u0005\b\u009920\u0001¨\u0006_" }, d2 = { "Landroidx/compose/runtime/snapshots/MutableSnapshot;", "Landroidx/compose/runtime/snapshots/Snapshot;", "id", "", "invalid", "Landroidx/compose/runtime/snapshots/SnapshotIdSet;", "readObserver", "Lkotlin/Function1;", "", "", "writeObserver", "(ILandroidx/compose/runtime/snapshots/SnapshotIdSet;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V", "applied", "", "getApplied$runtime_release", "()Z", "setApplied$runtime_release", "(Z)V", "<set-?>", "", "Landroidx/compose/runtime/snapshots/StateObject;", "modified", "getModified$runtime_release", "()Ljava/util/Set;", "setModified", "(Ljava/util/Set;)V", "previousIds", "getPreviousIds$runtime_release", "()Landroidx/compose/runtime/snapshots/SnapshotIdSet;", "setPreviousIds$runtime_release", "(Landroidx/compose/runtime/snapshots/SnapshotIdSet;)V", "previousPinnedSnapshots", "", "getPreviousPinnedSnapshots$runtime_release", "()[I", "setPreviousPinnedSnapshots$runtime_release", "([I)V", "getReadObserver$runtime_release", "()Lkotlin/jvm/functions/Function1;", "readOnly", "getReadOnly", "root", "getRoot", "()Landroidx/compose/runtime/snapshots/Snapshot;", "snapshots", "getWriteObserver$runtime_release", "abandon", "advance", "advance$runtime_release", "T", "block", "Lkotlin/Function0;", "(Lkotlin/jvm/functions/Function0;)Ljava/lang/Object;", "apply", "Landroidx/compose/runtime/snapshots/SnapshotApplyResult;", "closeLocked", "closeLocked$runtime_release", "dispose", "hasPendingChanges", "innerApplyLocked", "snapshotId", "optimisticMerges", "", "Landroidx/compose/runtime/snapshots/StateRecord;", "invalidSnapshots", "innerApplyLocked$runtime_release", "nestedActivated", "snapshot", "nestedActivated$runtime_release", "nestedDeactivated", "nestedDeactivated$runtime_release", "notifyObjectsInitialized", "notifyObjectsInitialized$runtime_release", "recordModified", "state", "recordModified$runtime_release", "recordPrevious", "recordPrevious$runtime_release", "recordPreviousList", "recordPreviousList$runtime_release", "recordPreviousPinnedSnapshot", "recordPreviousPinnedSnapshot$runtime_release", "recordPreviousPinnedSnapshots", "handles", "recordPreviousPinnedSnapshots$runtime_release", "releasePinnedSnapshotsForCloseLocked", "releasePinnedSnapshotsForCloseLocked$runtime_release", "releasePreviouslyPinnedSnapshotsLocked", "releasePreviouslyPinnedSnapshotsLocked$runtime_release", "takeNestedMutableSnapshot", "takeNestedSnapshot", "validateNotApplied", "validateNotApplied$runtime_release", "validateNotAppliedOrPinned", "validateNotAppliedOrPinned$runtime_release", "runtime_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
public class MutableSnapshot extends Snapshot
{
    public static final int $stable = 8;
    private boolean applied;
    private Set<StateObject> modified;
    private SnapshotIdSet previousIds;
    private int[] previousPinnedSnapshots;
    private final Function1<Object, Unit> readObserver;
    private int snapshots;
    private final Function1<Object, Unit> writeObserver;
    
    public MutableSnapshot(final int n, final SnapshotIdSet set, final Function1<Object, Unit> readObserver, final Function1<Object, Unit> writeObserver) {
        Intrinsics.checkNotNullParameter((Object)set, "invalid");
        super(n, set, null);
        this.readObserver = readObserver;
        this.writeObserver = writeObserver;
        this.previousIds = SnapshotIdSet.Companion.getEMPTY();
        this.previousPinnedSnapshots = new int[0];
        this.snapshots = 1;
    }
    
    private final void abandon() {
        final Set<StateObject> modified$runtime_release = this.getModified$runtime_release();
        if (modified$runtime_release != null) {
            this.validateNotApplied$runtime_release();
            this.setModified(null);
            final int id = this.getId();
            final Iterator<StateObject> iterator = modified$runtime_release.iterator();
            while (iterator.hasNext()) {
                for (StateRecord stateRecord = iterator.next().getFirstStateRecord(); stateRecord != null; stateRecord = stateRecord.getNext$runtime_release()) {
                    if (stateRecord.getSnapshotId$runtime_release() == id || CollectionsKt.contains((Iterable)this.previousIds, (Object)stateRecord.getSnapshotId$runtime_release())) {
                        stateRecord.setSnapshotId$runtime_release(0);
                    }
                }
            }
        }
        this.closeAndReleasePinning$runtime_release();
    }
    
    public final <T> T advance$runtime_release(Function0<? extends T> lock) {
        Intrinsics.checkNotNullParameter(lock, "block");
        this.recordPrevious$runtime_release(this.getId());
        final Object invoke = ((Function0)lock).invoke();
        if (!this.getApplied$runtime_release() && !this.getDisposed$runtime_release()) {
            final int id = this.getId();
            lock = SnapshotKt.getLock();
            monitorenter(lock);
            try {
                final int access$getNextSnapshotId$p = SnapshotKt.access$getNextSnapshotId$p();
                SnapshotKt.access$setNextSnapshotId$p(access$getNextSnapshotId$p + 1);
                this.setId$runtime_release(access$getNextSnapshotId$p);
                SnapshotKt.access$setOpenSnapshots$p(SnapshotKt.access$getOpenSnapshots$p().set(this.getId()));
                final Unit instance = Unit.INSTANCE;
                InlineMarker.finallyStart(1);
                monitorexit(lock);
                InlineMarker.finallyEnd(1);
                this.setInvalid$runtime_release(SnapshotKt.addRange(this.getInvalid$runtime_release(), id + 1, this.getId()));
            }
            finally {
                InlineMarker.finallyStart(1);
                monitorexit(lock);
                InlineMarker.finallyEnd(1);
            }
        }
        return (T)invoke;
    }
    
    public final void advance$runtime_release() {
        this.recordPrevious$runtime_release(this.getId());
        final Unit instance = Unit.INSTANCE;
        if (!this.getApplied$runtime_release() && !this.getDisposed$runtime_release()) {
            final int id = this.getId();
            synchronized (SnapshotKt.getLock()) {
                final int access$getNextSnapshotId$p = SnapshotKt.access$getNextSnapshotId$p();
                SnapshotKt.access$setNextSnapshotId$p(access$getNextSnapshotId$p + 1);
                this.setId$runtime_release(access$getNextSnapshotId$p);
                SnapshotKt.access$setOpenSnapshots$p(SnapshotKt.access$getOpenSnapshots$p().set(this.getId()));
                final Unit instance2 = Unit.INSTANCE;
                monitorexit(SnapshotKt.getLock());
                this.setInvalid$runtime_release(SnapshotKt.addRange(this.getInvalid$runtime_release(), id + 1, this.getId()));
            }
        }
    }
    
    public SnapshotApplyResult apply() {
        final Set<StateObject> modified$runtime_release = this.getModified$runtime_release();
        Map access$optimisticMerges;
        if (modified$runtime_release != null) {
            final MutableSnapshot value = SnapshotKt.access$getCurrentGlobalSnapshot$p().get();
            Intrinsics.checkNotNullExpressionValue((Object)value, "currentGlobalSnapshot.get()");
            access$optimisticMerges = SnapshotKt.access$optimisticMerges(value, this, SnapshotKt.access$getOpenSnapshots$p().clear(SnapshotKt.access$getCurrentGlobalSnapshot$p().get().getId()));
        }
        else {
            access$optimisticMerges = null;
        }
        Object o = SnapshotKt.getLock();
        synchronized (o) {
            SnapshotKt.access$validateOpen(this);
            final boolean b = true;
            Pair pair;
            if (modified$runtime_release != null && modified$runtime_release.size() != 0) {
                final GlobalSnapshot globalSnapshot = SnapshotKt.access$getCurrentGlobalSnapshot$p().get();
                final SnapshotApplyResult innerApplyLocked$runtime_release = this.innerApplyLocked$runtime_release(SnapshotKt.access$getNextSnapshotId$p(), access$optimisticMerges, SnapshotKt.access$getOpenSnapshots$p().clear(globalSnapshot.getId()));
                if (!Intrinsics.areEqual((Object)innerApplyLocked$runtime_release, (Object)SnapshotApplyResult.Success.INSTANCE)) {
                    return innerApplyLocked$runtime_release;
                }
                this.closeLocked$runtime_release();
                Intrinsics.checkNotNullExpressionValue((Object)globalSnapshot, "previousGlobalSnapshot");
                SnapshotKt.access$takeNewGlobalSnapshot(globalSnapshot, SnapshotKt.access$getEmptyLambda$p());
                final Set<StateObject> modified$runtime_release2 = globalSnapshot.getModified$runtime_release();
                this.setModified(null);
                globalSnapshot.setModified(null);
                pair = TuplesKt.to((Object)CollectionsKt.toMutableList((Collection)SnapshotKt.access$getApplyObservers$p()), (Object)modified$runtime_release2);
            }
            else {
                this.closeLocked$runtime_release();
                final GlobalSnapshot globalSnapshot2 = SnapshotKt.access$getCurrentGlobalSnapshot$p().get();
                Intrinsics.checkNotNullExpressionValue((Object)globalSnapshot2, "previousGlobalSnapshot");
                SnapshotKt.access$takeNewGlobalSnapshot(globalSnapshot2, SnapshotKt.access$getEmptyLambda$p());
                final Set<StateObject> modified$runtime_release3 = globalSnapshot2.getModified$runtime_release();
                if (modified$runtime_release3 != null && (modified$runtime_release3.isEmpty() ^ true)) {
                    pair = TuplesKt.to((Object)CollectionsKt.toMutableList((Collection)SnapshotKt.access$getApplyObservers$p()), (Object)modified$runtime_release3);
                }
                else {
                    pair = TuplesKt.to((Object)CollectionsKt.emptyList(), (Object)null);
                }
            }
            monitorexit(o);
            o = pair.component1();
            final Set set = (Set)pair.component2();
            this.applied = true;
            final Collection collection = set;
            final int n = 0;
            if (collection != null && !collection.isEmpty()) {
                for (int size = ((List)o).size(), i = 0; i < size; ++i) {
                    ((Function2)((List)o).get(i)).invoke((Object)set, (Object)this);
                }
            }
            final Collection collection2 = modified$runtime_release;
            int n2 = b ? 1 : 0;
            if (collection2 != null) {
                if (collection2.isEmpty()) {
                    n2 = (b ? 1 : 0);
                }
                else {
                    n2 = 0;
                }
            }
            if (n2 == 0) {
                for (int size2 = ((List)o).size(), j = n; j < size2; ++j) {
                    ((Function2)((List)o).get(j)).invoke((Object)modified$runtime_release, (Object)this);
                }
            }
            synchronized (SnapshotKt.getLock()) {
                this.releasePinnedSnapshotsForCloseLocked$runtime_release();
                if (set != null) {
                    o = set.iterator();
                    while (((Iterator)o).hasNext()) {
                        SnapshotKt.access$overwriteUnusedRecordsLocked(((Iterator<StateObject>)o).next());
                    }
                }
                if (modified$runtime_release != null) {
                    final Iterator iterator = modified$runtime_release.iterator();
                    while (iterator.hasNext()) {
                        SnapshotKt.access$overwriteUnusedRecordsLocked((StateObject)iterator.next());
                    }
                    final Unit instance = Unit.INSTANCE;
                }
                return SnapshotApplyResult.Success.INSTANCE;
            }
        }
    }
    
    @Override
    public void closeLocked$runtime_release() {
        SnapshotKt.access$setOpenSnapshots$p(SnapshotKt.access$getOpenSnapshots$p().clear(this.getId()).andNot(this.previousIds));
    }
    
    @Override
    public void dispose() {
        if (!this.getDisposed$runtime_release()) {
            super.dispose();
            this.nestedDeactivated$runtime_release(this);
        }
    }
    
    public final boolean getApplied$runtime_release() {
        return this.applied;
    }
    
    @Override
    public Set<StateObject> getModified$runtime_release() {
        return this.modified;
    }
    
    public final SnapshotIdSet getPreviousIds$runtime_release() {
        return this.previousIds;
    }
    
    public final int[] getPreviousPinnedSnapshots$runtime_release() {
        return this.previousPinnedSnapshots;
    }
    
    @Override
    public Function1<Object, Unit> getReadObserver$runtime_release() {
        return this.readObserver;
    }
    
    @Override
    public boolean getReadOnly() {
        return false;
    }
    
    @Override
    public Snapshot getRoot() {
        return this;
    }
    
    @Override
    public Function1<Object, Unit> getWriteObserver$runtime_release() {
        return this.writeObserver;
    }
    
    @Override
    public boolean hasPendingChanges() {
        final Set<StateObject> modified$runtime_release = this.getModified$runtime_release();
        boolean b = false;
        if (modified$runtime_release != null) {
            b = b;
            if (modified$runtime_release.isEmpty() ^ true) {
                b = true;
            }
        }
        return b;
    }
    
    public final SnapshotApplyResult innerApplyLocked$runtime_release(int i, final Map<StateRecord, ? extends StateRecord> map, final SnapshotIdSet set) {
        Intrinsics.checkNotNullParameter((Object)set, "invalidSnapshots");
        final SnapshotIdSet or = this.getInvalid$runtime_release().set(this.getId()).or(this.previousIds);
        final Set<StateObject> modified$runtime_release = this.getModified$runtime_release();
        Intrinsics.checkNotNull((Object)modified$runtime_release);
        final Iterator<StateObject> iterator = modified$runtime_release.iterator();
        List list = null;
        List list2 = null;
        while (iterator.hasNext()) {
            final StateObject stateObject = iterator.next();
            final StateRecord firstStateRecord = stateObject.getFirstStateRecord();
            final StateRecord access$readable = SnapshotKt.access$readable(firstStateRecord, i, set);
            if (access$readable == null) {
                continue;
            }
            final StateRecord access$readable2 = SnapshotKt.access$readable(firstStateRecord, this.getId(), or);
            if (access$readable2 == null) {
                continue;
            }
            if (Intrinsics.areEqual((Object)access$readable, (Object)access$readable2)) {
                continue;
            }
            final StateRecord access$readable3 = SnapshotKt.access$readable(firstStateRecord, this.getId(), this.getInvalid$runtime_release());
            if (access$readable3 == null) {
                SnapshotKt.access$readError();
                throw new KotlinNothingValueException();
            }
            StateRecord mergeRecords;
            if (map == null || (mergeRecords = map.get(access$readable)) == null) {
                final MutableSnapshot mutableSnapshot = this;
                mergeRecords = stateObject.mergeRecords(access$readable2, access$readable, access$readable3);
            }
            if (mergeRecords == null) {
                return new SnapshotApplyResult.Failure(this);
            }
            if (Intrinsics.areEqual((Object)mergeRecords, (Object)access$readable3)) {
                continue;
            }
            if (Intrinsics.areEqual((Object)mergeRecords, (Object)access$readable)) {
                List list3;
                if ((list3 = list) == null) {
                    list3 = new ArrayList();
                }
                list3.add(TuplesKt.to((Object)stateObject, (Object)access$readable.create()));
                List list4;
                if ((list4 = list2) == null) {
                    list4 = new ArrayList();
                }
                list4.add(stateObject);
                list = list3;
                list2 = list4;
            }
            else {
                List list5;
                if ((list5 = list) == null) {
                    list5 = new ArrayList();
                }
                Pair pair;
                if (!Intrinsics.areEqual((Object)mergeRecords, (Object)access$readable2)) {
                    pair = TuplesKt.to((Object)stateObject, (Object)mergeRecords);
                }
                else {
                    pair = TuplesKt.to((Object)stateObject, (Object)access$readable2.create());
                }
                list5.add(pair);
                list = list5;
            }
        }
        if (list != null) {
            this.advance$runtime_release();
            i = 0;
            while (i < list.size()) {
                final Pair pair2 = list.get(i);
                final StateObject stateObject2 = (StateObject)pair2.component1();
                final StateRecord stateRecord = (StateRecord)pair2.component2();
                stateRecord.setSnapshotId$runtime_release(this.getId());
                synchronized (SnapshotKt.getLock()) {
                    stateRecord.setNext$runtime_release(stateObject2.getFirstStateRecord());
                    stateObject2.prependStateRecord(stateRecord);
                    final Unit instance = Unit.INSTANCE;
                    monitorexit(SnapshotKt.getLock());
                    ++i;
                    continue;
                }
                break;
            }
        }
        if (list2 != null) {
            modified$runtime_release.removeAll(list2);
        }
        return SnapshotApplyResult.Success.INSTANCE;
    }
    
    @Override
    public void nestedActivated$runtime_release(final Snapshot snapshot) {
        Intrinsics.checkNotNullParameter((Object)snapshot, "snapshot");
        ++this.snapshots;
    }
    
    @Override
    public void nestedDeactivated$runtime_release(final Snapshot snapshot) {
        Intrinsics.checkNotNullParameter((Object)snapshot, "snapshot");
        final int snapshots = this.snapshots;
        if (snapshots > 0) {
            final int snapshots2 = snapshots - 1;
            this.snapshots = snapshots2;
            if (snapshots2 == 0 && !this.applied) {
                this.abandon();
            }
            return;
        }
        throw new IllegalArgumentException("Failed requirement.".toString());
    }
    
    @Override
    public void notifyObjectsInitialized$runtime_release() {
        if (!this.applied) {
            if (!this.getDisposed$runtime_release()) {
                this.advance$runtime_release();
            }
        }
    }
    
    @Override
    public void recordModified$runtime_release(final StateObject stateObject) {
        Intrinsics.checkNotNullParameter((Object)stateObject, "state");
        Set<StateObject> modified$runtime_release;
        if ((modified$runtime_release = this.getModified$runtime_release()) == null) {
            modified$runtime_release = new HashSet<StateObject>();
            this.setModified(modified$runtime_release);
        }
        modified$runtime_release.add(stateObject);
    }
    
    public final void recordPrevious$runtime_release(final int n) {
        synchronized (SnapshotKt.getLock()) {
            this.previousIds = this.previousIds.set(n);
            final Unit instance = Unit.INSTANCE;
        }
    }
    
    public final void recordPreviousList$runtime_release(final SnapshotIdSet set) {
        Intrinsics.checkNotNullParameter((Object)set, "snapshots");
        synchronized (SnapshotKt.getLock()) {
            this.previousIds = this.previousIds.or(set);
            final Unit instance = Unit.INSTANCE;
        }
    }
    
    public final void recordPreviousPinnedSnapshot$runtime_release(final int n) {
        if (n >= 0) {
            this.previousPinnedSnapshots = ArraysKt.plus(this.previousPinnedSnapshots, n);
        }
    }
    
    public final void recordPreviousPinnedSnapshots$runtime_release(final int[] previousPinnedSnapshots) {
        Intrinsics.checkNotNullParameter((Object)previousPinnedSnapshots, "handles");
        final int length = previousPinnedSnapshots.length;
        final int n = 1;
        if (length == 0) {
            return;
        }
        final int[] previousPinnedSnapshots2 = this.previousPinnedSnapshots;
        int n2;
        if (previousPinnedSnapshots2.length == 0) {
            n2 = n;
        }
        else {
            n2 = 0;
        }
        if (n2 != 0) {
            this.previousPinnedSnapshots = previousPinnedSnapshots;
        }
        else {
            this.previousPinnedSnapshots = ArraysKt.plus(previousPinnedSnapshots2, previousPinnedSnapshots);
        }
    }
    
    @Override
    public void releasePinnedSnapshotsForCloseLocked$runtime_release() {
        this.releasePreviouslyPinnedSnapshotsLocked$runtime_release();
        super.releasePinnedSnapshotsForCloseLocked$runtime_release();
    }
    
    public final void releasePreviouslyPinnedSnapshotsLocked$runtime_release() {
        for (int length = this.previousPinnedSnapshots.length, i = 0; i < length; ++i) {
            SnapshotKt.releasePinningLocked(this.previousPinnedSnapshots[i]);
        }
    }
    
    public final void setApplied$runtime_release(final boolean applied) {
        this.applied = applied;
    }
    
    public void setModified(final Set<StateObject> modified) {
        this.modified = modified;
    }
    
    public final void setPreviousIds$runtime_release(final SnapshotIdSet previousIds) {
        Intrinsics.checkNotNullParameter((Object)previousIds, "<set-?>");
        this.previousIds = previousIds;
    }
    
    public final void setPreviousPinnedSnapshots$runtime_release(final int[] previousPinnedSnapshots) {
        Intrinsics.checkNotNullParameter((Object)previousPinnedSnapshots, "<set-?>");
        this.previousPinnedSnapshots = previousPinnedSnapshots;
    }
    
    public MutableSnapshot takeNestedMutableSnapshot(final Function1<Object, Unit> function1, final Function1<Object, Unit> function2) {
        this.validateNotDisposed$runtime_release();
        this.validateNotAppliedOrPinned$runtime_release();
        this.recordPrevious$runtime_release(this.getId());
        synchronized (SnapshotKt.getLock()) {
            final int access$getNextSnapshotId$p = SnapshotKt.access$getNextSnapshotId$p();
            SnapshotKt.access$setNextSnapshotId$p(access$getNextSnapshotId$p + 1);
            SnapshotKt.access$setOpenSnapshots$p(SnapshotKt.access$getOpenSnapshots$p().set(access$getNextSnapshotId$p));
            final SnapshotIdSet invalid$runtime_release = this.getInvalid$runtime_release();
            this.setInvalid$runtime_release(invalid$runtime_release.set(access$getNextSnapshotId$p));
            final NestedMutableSnapshot nestedMutableSnapshot = new NestedMutableSnapshot(access$getNextSnapshotId$p, SnapshotKt.addRange(invalid$runtime_release, this.getId() + 1, access$getNextSnapshotId$p), (Function1<Object, Unit>)SnapshotKt.mergedReadObserver$default(function1, this.getReadObserver$runtime_release(), false, 4, null), mergedWriteObserver(function2, this.getWriteObserver$runtime_release()), this);
            monitorexit(SnapshotKt.getLock());
            if (!this.getApplied$runtime_release() && !this.getDisposed$runtime_release()) {
                final int id = this.getId();
                synchronized (SnapshotKt.getLock()) {
                    final int access$getNextSnapshotId$p2 = SnapshotKt.access$getNextSnapshotId$p();
                    SnapshotKt.access$setNextSnapshotId$p(access$getNextSnapshotId$p2 + 1);
                    this.setId$runtime_release(access$getNextSnapshotId$p2);
                    SnapshotKt.access$setOpenSnapshots$p(SnapshotKt.access$getOpenSnapshots$p().set(this.getId()));
                    final Unit instance = Unit.INSTANCE;
                    monitorexit(SnapshotKt.getLock());
                    this.setInvalid$runtime_release(SnapshotKt.addRange(this.getInvalid$runtime_release(), id + 1, this.getId()));
                }
            }
            return nestedMutableSnapshot;
        }
    }
    
    @Override
    public Snapshot takeNestedSnapshot(final Function1<Object, Unit> function1) {
        this.validateNotDisposed$runtime_release();
        this.validateNotAppliedOrPinned$runtime_release();
        final int id = this.getId();
        this.recordPrevious$runtime_release(this.getId());
        synchronized (SnapshotKt.getLock()) {
            final int access$getNextSnapshotId$p = SnapshotKt.access$getNextSnapshotId$p();
            SnapshotKt.access$setNextSnapshotId$p(access$getNextSnapshotId$p + 1);
            SnapshotKt.access$setOpenSnapshots$p(SnapshotKt.access$getOpenSnapshots$p().set(access$getNextSnapshotId$p));
            final NestedReadonlySnapshot nestedReadonlySnapshot = new NestedReadonlySnapshot(access$getNextSnapshotId$p, SnapshotKt.addRange(this.getInvalid$runtime_release(), id + 1, access$getNextSnapshotId$p), function1, this);
            monitorexit(SnapshotKt.getLock());
            if (!this.getApplied$runtime_release() && !this.getDisposed$runtime_release()) {
                final int id2 = this.getId();
                SnapshotKt.getLock();
                synchronized (SnapshotKt.getLock()) {
                    final int access$getNextSnapshotId$p2 = SnapshotKt.access$getNextSnapshotId$p();
                    SnapshotKt.access$setNextSnapshotId$p(access$getNextSnapshotId$p2 + 1);
                    this.setId$runtime_release(access$getNextSnapshotId$p2);
                    SnapshotKt.access$setOpenSnapshots$p(SnapshotKt.access$getOpenSnapshots$p().set(this.getId()));
                    final Unit instance = Unit.INSTANCE;
                    monitorexit(SnapshotKt.getLock());
                    this.setInvalid$runtime_release(SnapshotKt.addRange(this.getInvalid$runtime_release(), id2 + 1, this.getId()));
                }
            }
            return nestedReadonlySnapshot;
        }
    }
    
    public final void validateNotApplied$runtime_release() {
        if (this.applied ^ true) {
            return;
        }
        throw new IllegalStateException("Unsupported operation on a snapshot that has been applied".toString());
    }
    
    public final void validateNotAppliedOrPinned$runtime_release() {
        final boolean applied = this.applied;
        boolean b = false;
        if (!applied || Snapshot.access$getPinningTrackingHandle$p(this) >= 0) {
            b = true;
        }
        if (b) {
            return;
        }
        throw new IllegalStateException("Unsupported operation on a disposed or applied snapshot".toString());
    }
}

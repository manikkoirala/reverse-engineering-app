// 
// Decompiled by Procyon v0.6.0
// 

package androidx.compose.runtime.snapshots;

import kotlin.coroutines.CoroutineContext;
import kotlin.coroutines.CoroutineContext$Key;
import kotlin.coroutines.CoroutineContext$Element$DefaultImpls;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.functions.Function2;
import kotlin.Metadata;
import kotlin.coroutines.CoroutineContext$Element;

@Metadata(d1 = { "\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\bg\u0018\u0000 \u00022\u00020\u0001:\u0001\u0002\u00f8\u0001\u0000\u0082\u0002\u0006\n\u0004\b!0\u0001¨\u0006\u0003\u00c0\u0006\u0001" }, d2 = { "Landroidx/compose/runtime/snapshots/SnapshotContextElement;", "Lkotlin/coroutines/CoroutineContext$Element;", "Key", "runtime_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
public interface SnapshotContextElement extends CoroutineContext$Element
{
    public static final Key Key = SnapshotContextElement.Key.$$INSTANCE;
    
    @Metadata(k = 3, mv = { 1, 8, 0 }, xi = 48)
    public static final class DefaultImpls
    {
        public static <R> R fold(final SnapshotContextElement snapshotContextElement, final R r, final Function2<? super R, ? super CoroutineContext$Element, ? extends R> function2) {
            Intrinsics.checkNotNullParameter((Object)function2, "operation");
            return (R)CoroutineContext$Element$DefaultImpls.fold((CoroutineContext$Element)snapshotContextElement, (Object)r, (Function2)function2);
        }
        
        public static <E extends CoroutineContext$Element> E get(final SnapshotContextElement snapshotContextElement, final CoroutineContext$Key<E> coroutineContext$Key) {
            Intrinsics.checkNotNullParameter((Object)coroutineContext$Key, "key");
            return (E)CoroutineContext$Element$DefaultImpls.get((CoroutineContext$Element)snapshotContextElement, (CoroutineContext$Key)coroutineContext$Key);
        }
        
        public static CoroutineContext minusKey(final SnapshotContextElement snapshotContextElement, final CoroutineContext$Key<?> coroutineContext$Key) {
            Intrinsics.checkNotNullParameter((Object)coroutineContext$Key, "key");
            return CoroutineContext$Element$DefaultImpls.minusKey((CoroutineContext$Element)snapshotContextElement, (CoroutineContext$Key)coroutineContext$Key);
        }
        
        public static CoroutineContext plus(final SnapshotContextElement snapshotContextElement, final CoroutineContext coroutineContext) {
            Intrinsics.checkNotNullParameter((Object)coroutineContext, "context");
            return CoroutineContext$Element$DefaultImpls.plus((CoroutineContext$Element)snapshotContextElement, coroutineContext);
        }
    }
    
    @Metadata(d1 = { "\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0086\u0003\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0003¨\u0006\u0004" }, d2 = { "Landroidx/compose/runtime/snapshots/SnapshotContextElement$Key;", "Lkotlin/coroutines/CoroutineContext$Key;", "Landroidx/compose/runtime/snapshots/SnapshotContextElement;", "()V", "runtime_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
    public static final class Key implements CoroutineContext$Key<SnapshotContextElement>
    {
        static final Key $$INSTANCE;
        
        static {
            $$INSTANCE = new Key();
        }
        
        private Key() {
        }
    }
}

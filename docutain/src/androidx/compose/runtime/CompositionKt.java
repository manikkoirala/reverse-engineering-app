// 
// Decompiled by Procyon v0.6.0
// 

package androidx.compose.runtime;

import kotlin.jvm.functions.Function1;
import java.util.HashSet;
import kotlin.coroutines.EmptyCoroutineContext;
import java.util.Iterator;
import kotlin.TuplesKt;
import java.util.ArrayList;
import kotlin.collections.CollectionsKt;
import java.util.Collection;
import kotlin.Pair;
import java.util.List;
import kotlin.Unit;
import androidx.compose.runtime.collection.IdentityArraySet;
import androidx.compose.runtime.collection.IdentityArrayMap;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.coroutines.CoroutineContext;
import kotlin.jvm.internal.Intrinsics;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000j\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u001a\u001a\u0010\t\u001a\u00020\n2\n\u0010\u000b\u001a\u0006\u0012\u0002\b\u00030\f2\u0006\u0010\r\u001a\u00020\u000e\u001a$\u0010\t\u001a\u00020\n2\n\u0010\u000b\u001a\u0006\u0012\u0002\b\u00030\f2\u0006\u0010\r\u001a\u00020\u000e2\u0006\u0010\u0002\u001a\u00020\u0003H\u0007\u001a\u001a\u0010\u000f\u001a\u00020\u00042\n\u0010\u000b\u001a\u0006\u0012\u0002\b\u00030\f2\u0006\u0010\r\u001a\u00020\u000e\u001a$\u0010\u000f\u001a\u00020\u00042\n\u0010\u000b\u001a\u0006\u0012\u0002\b\u00030\f2\u0006\u0010\r\u001a\u00020\u000e2\u0006\u0010\u0002\u001a\u00020\u0003H\u0007\u001a\u0006\u0010\u0010\u001a\u00020\u0011\u001a\u001c\u0010\u0012\u001a\u0018\u0012\u0014\u0012\u0012\u0012\b\u0012\u00060\u0015j\u0002`\u0016\u0012\u0004\u0012\u00020\u00170\u00140\u0013\u001a\u000e\u0010\u0018\u001a\u00020\u00112\u0006\u0010\u0019\u001a\u00020\u001a\u001a\u000e\u0010\u001b\u001a\u00020\u00112\u0006\u0010\u001c\u001a\u00020\u0001\u001aI\u0010\u001d\u001a\u00020\u0011\"\b\b\u0000\u0010\u001e*\u00020\u0001\"\b\b\u0001\u0010\u001f*\u00020\u0001*\u0016\u0012\u0004\u0012\u0002H\u001e\u0012\f\u0012\n\u0012\u0004\u0012\u0002H\u001f\u0018\u00010!0 2\u0006\u0010\u0019\u001a\u0002H\u001e2\u0006\u0010\"\u001a\u0002H\u001fH\u0002¢\u0006\u0002\u0010#\u001a7\u0010$\u001a\u00020\u0011\"\u0004\b\u0000\u0010%*\u0012\u0012\u0004\u0012\u0002H%0&j\b\u0012\u0004\u0012\u0002H%`'2\u0012\u0010(\u001a\u000e\u0012\u0004\u0012\u0002H%\u0012\u0004\u0012\u00020\u00170)H\u0082\b\"\u000e\u0010\u0000\u001a\u00020\u0001X\u0082\u0004¢\u0006\u0002\n\u0000\"\u001e\u0010\u0002\u001a\u00020\u0003*\u00020\u00048GX\u0087\u0004¢\u0006\f\u0012\u0004\b\u0005\u0010\u0006\u001a\u0004\b\u0007\u0010\b¨\u0006*" }, d2 = { "PendingApplyNoModifications", "", "recomposeCoroutineContext", "Lkotlin/coroutines/CoroutineContext;", "Landroidx/compose/runtime/ControlledComposition;", "getRecomposeCoroutineContext$annotations", "(Landroidx/compose/runtime/ControlledComposition;)V", "getRecomposeCoroutineContext", "(Landroidx/compose/runtime/ControlledComposition;)Lkotlin/coroutines/CoroutineContext;", "Composition", "Landroidx/compose/runtime/Composition;", "applier", "Landroidx/compose/runtime/Applier;", "parent", "Landroidx/compose/runtime/CompositionContext;", "ControlledComposition", "clearCompositionErrors", "", "currentCompositionErrors", "", "Lkotlin/Pair;", "Ljava/lang/Exception;", "Lkotlin/Exception;", "", "invalidateGroupsWithKey", "key", "", "simulateHotReload", "context", "addValue", "K", "V", "Landroidx/compose/runtime/collection/IdentityArrayMap;", "Landroidx/compose/runtime/collection/IdentityArraySet;", "value", "(Landroidx/compose/runtime/collection/IdentityArrayMap;Ljava/lang/Object;Ljava/lang/Object;)V", "removeValueIf", "E", "Ljava/util/HashSet;", "Lkotlin/collections/HashSet;", "predicate", "Lkotlin/Function1;", "runtime_release" }, k = 2, mv = { 1, 8, 0 }, xi = 48)
public final class CompositionKt
{
    private static final Object PendingApplyNoModifications;
    
    static {
        PendingApplyNoModifications = new Object();
    }
    
    public static final Composition Composition(final Applier<?> applier, final CompositionContext compositionContext) {
        Intrinsics.checkNotNullParameter((Object)applier, "applier");
        Intrinsics.checkNotNullParameter((Object)compositionContext, "parent");
        return new CompositionImpl(compositionContext, applier, null, 4, null);
    }
    
    public static final Composition Composition(final Applier<?> applier, final CompositionContext compositionContext, final CoroutineContext coroutineContext) {
        Intrinsics.checkNotNullParameter((Object)applier, "applier");
        Intrinsics.checkNotNullParameter((Object)compositionContext, "parent");
        Intrinsics.checkNotNullParameter((Object)coroutineContext, "recomposeCoroutineContext");
        return new CompositionImpl(compositionContext, applier, coroutineContext);
    }
    
    public static final ControlledComposition ControlledComposition(final Applier<?> applier, final CompositionContext compositionContext) {
        Intrinsics.checkNotNullParameter((Object)applier, "applier");
        Intrinsics.checkNotNullParameter((Object)compositionContext, "parent");
        return new CompositionImpl(compositionContext, applier, null, 4, null);
    }
    
    public static final ControlledComposition ControlledComposition(final Applier<?> applier, final CompositionContext compositionContext, final CoroutineContext coroutineContext) {
        Intrinsics.checkNotNullParameter((Object)applier, "applier");
        Intrinsics.checkNotNullParameter((Object)compositionContext, "parent");
        Intrinsics.checkNotNullParameter((Object)coroutineContext, "recomposeCoroutineContext");
        return new CompositionImpl(compositionContext, applier, coroutineContext);
    }
    
    private static final <K, V> void addValue(final IdentityArrayMap<K, IdentityArraySet<V>> identityArrayMap, final K k, final V v) {
        if (identityArrayMap.contains(k)) {
            final IdentityArraySet set = identityArrayMap.get(k);
            if (set != null) {
                set.add(v);
            }
        }
        else {
            final IdentityArraySet set2 = new IdentityArraySet();
            set2.add(v);
            final Unit instance = Unit.INSTANCE;
            identityArrayMap.set(k, set2);
        }
    }
    
    public static final void clearCompositionErrors() {
        HotReloader.Companion.clearErrors$runtime_release();
    }
    
    public static final List<Pair<Exception, Boolean>> currentCompositionErrors() {
        final Iterable iterable = HotReloader.Companion.getCurrentErrors$runtime_release();
        final Collection collection = new ArrayList(CollectionsKt.collectionSizeOrDefault(iterable, 10));
        for (final RecomposerErrorInfo recomposerErrorInfo : iterable) {
            collection.add(TuplesKt.to((Object)recomposerErrorInfo.getCause(), (Object)recomposerErrorInfo.getRecoverable()));
        }
        return (List<Pair<Exception, Boolean>>)collection;
    }
    
    public static final CoroutineContext getRecomposeCoroutineContext(final ControlledComposition controlledComposition) {
        Intrinsics.checkNotNullParameter((Object)controlledComposition, "<this>");
        CompositionImpl compositionImpl;
        if (controlledComposition instanceof CompositionImpl) {
            compositionImpl = (CompositionImpl)controlledComposition;
        }
        else {
            compositionImpl = null;
        }
        CoroutineContext recomposeContext;
        if (compositionImpl == null || (recomposeContext = compositionImpl.getRecomposeContext()) == null) {
            recomposeContext = (CoroutineContext)EmptyCoroutineContext.INSTANCE;
        }
        return recomposeContext;
    }
    
    public static final void invalidateGroupsWithKey(final int n) {
        HotReloader.Companion.invalidateGroupsWithKey$runtime_release(n);
    }
    
    private static final <E> void removeValueIf(final HashSet<E> set, final Function1<? super E, Boolean> function1) {
        final Iterator<E> iterator = set.iterator();
        Intrinsics.checkNotNullExpressionValue((Object)iterator, "iterator()");
        while (iterator.hasNext()) {
            if (function1.invoke((Object)iterator.next())) {
                iterator.remove();
            }
        }
    }
    
    public static final void simulateHotReload(final Object o) {
        Intrinsics.checkNotNullParameter(o, "context");
        HotReloader.Companion.simulateHotReload$runtime_release(o);
    }
}

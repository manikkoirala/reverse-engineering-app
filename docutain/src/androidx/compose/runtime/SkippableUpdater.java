// 
// Decompiled by Procyon v0.6.0
// 

package androidx.compose.runtime;

import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.JvmInline;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000B\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\b\u0087@\u0018\u0000*\u0004\b\u0000\u0010\u00012\u00020\u0002B\u0012\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u00f8\u0001\u0000¢\u0006\u0004\b\u0005\u0010\u0006J\u001a\u0010\t\u001a\u00020\n2\b\u0010\u000b\u001a\u0004\u0018\u00010\u0002H\u00d6\u0003¢\u0006\u0004\b\f\u0010\rJ\u0010\u0010\u000e\u001a\u00020\u000fH\u00d6\u0001¢\u0006\u0004\b\u0010\u0010\u0011J\u0010\u0010\u0012\u001a\u00020\u0013H\u00d6\u0001¢\u0006\u0004\b\u0014\u0010\u0015J5\u0010\u0016\u001a\u00020\u00172\u001d\u0010\u0018\u001a\u0019\u0012\n\u0012\b\u0012\u0004\u0012\u00028\u00000\u001a\u0012\u0004\u0012\u00020\u00170\u0019¢\u0006\u0002\b\u001bH\u0086\b\u00f8\u0001\u0001\u00f8\u0001\u0000¢\u0006\u0004\b\u001c\u0010\u001dR\u0016\u0010\u0003\u001a\u00020\u00048\u0000X\u0081\u0004¢\u0006\b\n\u0000\u0012\u0004\b\u0007\u0010\b\u0088\u0001\u0003\u0092\u0001\u00020\u0004\u00f8\u0001\u0000\u0082\u0002\u000b\n\u0002\b\u0019\n\u0005\b\u009920\u0001¨\u0006\u001e" }, d2 = { "Landroidx/compose/runtime/SkippableUpdater;", "T", "", "composer", "Landroidx/compose/runtime/Composer;", "constructor-impl", "(Landroidx/compose/runtime/Composer;)Landroidx/compose/runtime/Composer;", "getComposer$annotations", "()V", "equals", "", "other", "equals-impl", "(Landroidx/compose/runtime/Composer;Ljava/lang/Object;)Z", "hashCode", "", "hashCode-impl", "(Landroidx/compose/runtime/Composer;)I", "toString", "", "toString-impl", "(Landroidx/compose/runtime/Composer;)Ljava/lang/String;", "update", "", "block", "Lkotlin/Function1;", "Landroidx/compose/runtime/Updater;", "Lkotlin/ExtensionFunctionType;", "update-impl", "(Landroidx/compose/runtime/Composer;Lkotlin/jvm/functions/Function1;)V", "runtime_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
@JvmInline
public final class SkippableUpdater<T>
{
    private final Composer composer = composer;
    
    public static <T> Composer constructor-impl(final Composer composer) {
        Intrinsics.checkNotNullParameter((Object)composer, "composer");
        return composer;
    }
    
    public static boolean equals-impl(final Composer composer, final Object o) {
        return o instanceof SkippableUpdater && Intrinsics.areEqual((Object)composer, (Object)((SkippableUpdater)o).unbox-impl());
    }
    
    public static final boolean equals-impl0(final Composer composer, final Composer composer2) {
        return Intrinsics.areEqual((Object)composer, (Object)composer2);
    }
    
    public static int hashCode-impl(final Composer composer) {
        return composer.hashCode();
    }
    
    public static String toString-impl(final Composer obj) {
        final StringBuilder sb = new StringBuilder();
        sb.append("SkippableUpdater(composer=");
        sb.append(obj);
        sb.append(')');
        return sb.toString();
    }
    
    public static final void update-impl(final Composer composer, final Function1<? super Updater<T>, Unit> function1) {
        Intrinsics.checkNotNullParameter((Object)function1, "block");
        composer.startReplaceableGroup(509942095);
        function1.invoke((Object)Updater.box-impl(Updater.constructor-impl(composer)));
        composer.endReplaceableGroup();
    }
    
    @Override
    public boolean equals(final Object o) {
        return equals-impl(this.composer, o);
    }
    
    @Override
    public int hashCode() {
        return hashCode-impl(this.composer);
    }
    
    @Override
    public String toString() {
        return toString-impl(this.composer);
    }
    
    public final /* synthetic */ Composer unbox-impl() {
        return this.composer;
    }
}

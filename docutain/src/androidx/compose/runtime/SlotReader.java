// 
// Decompiled by Procyon v0.6.0
// 

package androidx.compose.runtime;

import kotlin.Unit;
import kotlin.jvm.functions.Function2;
import java.util.List;
import kotlin.KotlinNothingValueException;
import java.util.ArrayList;
import kotlin.jvm.internal.Intrinsics;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000b\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u001a\n\u0002\u0010\u0015\n\u0002\b\u0010\n\u0002\u0010\u0011\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0005\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0011\n\u0002\u0010\u000e\n\u0002\b\u0003\b\u0000\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u0010\u0010;\u001a\u00020<2\b\b\u0002\u0010=\u001a\u00020\nJ\u0006\u0010>\u001a\u00020?J\u0006\u0010@\u001a\u00020?J\u000e\u0010A\u001a\u00020\u00062\u0006\u0010=\u001a\u00020\nJ\u0006\u0010B\u001a\u00020?J\u0006\u0010C\u001a\u00020?J\f\u0010D\u001a\b\u0012\u0004\u0012\u00020F0EJO\u0010G\u001a\u00020?2\u0006\u0010H\u001a\u00020\n28\u0010I\u001a4\u0012\u0013\u0012\u00110\n¢\u0006\f\bK\u0012\b\bL\u0012\u0004\b\b(=\u0012\u0015\u0012\u0013\u0018\u00010\u0001¢\u0006\f\bK\u0012\b\bL\u0012\u0004\b\b(M\u0012\u0004\u0012\u00020?0JH\u0000¢\u0006\u0002\bNJ\u0010\u0010O\u001a\u0004\u0018\u00010\u00012\u0006\u0010=\u001a\u00020\nJ\u0010\u0010\u0013\u001a\u0004\u0018\u00010\u00012\u0006\u0010=\u001a\u00020\nJ\u000e\u0010\u0016\u001a\u00020\n2\u0006\u0010=\u001a\u00020\nJ\u0010\u0010P\u001a\u0004\u0018\u00010\u00012\u0006\u0010=\u001a\u00020\nJ\u0018\u0010P\u001a\u0004\u0018\u00010\u00012\u0006\u0010H\u001a\u00020\n2\u0006\u0010=\u001a\u00020\nJ\u000e\u0010\u0018\u001a\u00020\n2\u0006\u0010;\u001a\u00020<J\u000e\u0010\u0018\u001a\u00020\n2\u0006\u0010=\u001a\u00020\nJ\u0010\u0010\u001c\u001a\u0004\u0018\u00010\u00012\u0006\u0010=\u001a\u00020\nJ\u000e\u0010\u001e\u001a\u00020\n2\u0006\u0010=\u001a\u00020\nJ\u000e\u0010Q\u001a\u00020\u00062\u0006\u0010=\u001a\u00020\nJ\u000e\u0010R\u001a\u00020\u00062\u0006\u0010=\u001a\u00020\nJ\u000e\u0010*\u001a\u00020\u00062\u0006\u0010=\u001a\u00020\nJ\b\u0010S\u001a\u0004\u0018\u00010\u0001J\u0010\u0010T\u001a\u0004\u0018\u00010\u00012\u0006\u0010=\u001a\u00020\nJ\u000e\u0010+\u001a\u00020\n2\u0006\u0010=\u001a\u00020\nJ\u000e\u0010-\u001a\u00020\n2\u0006\u0010=\u001a\u00020\nJ\u000e\u0010U\u001a\u00020\n2\u0006\u0010=\u001a\u00020\nJ\u000e\u0010V\u001a\u00020?2\u0006\u0010=\u001a\u00020\nJ\u000e\u0010W\u001a\u00020?2\u0006\u0010=\u001a\u00020\nJ\u0006\u0010X\u001a\u00020\nJ\u0006\u0010Y\u001a\u00020?J\u0006\u0010Z\u001a\u00020?J\u0006\u0010[\u001a\u00020?J\b\u0010\\\u001a\u00020]H\u0016J\u0016\u0010^\u001a\u0004\u0018\u00010\u0001*\u00020%2\u0006\u0010=\u001a\u00020\nH\u0002J\u0016\u0010T\u001a\u0004\u0018\u00010\u0001*\u00020%2\u0006\u0010=\u001a\u00020\nH\u0002J\u0016\u0010_\u001a\u0004\u0018\u00010\u0001*\u00020%2\u0006\u0010=\u001a\u00020\nH\u0002R\u001e\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0005\u001a\u00020\u0006@BX\u0086\u000e¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010\tR\u001e\u0010\u000b\u001a\u00020\n2\u0006\u0010\u0005\u001a\u00020\n@BX\u0086\u000e¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\rR\u001e\u0010\u000e\u001a\u00020\n2\u0006\u0010\u0005\u001a\u00020\n@BX\u0086\u000e¢\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\rR\u000e\u0010\u0010\u001a\u00020\nX\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\nX\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\nX\u0082\u000e¢\u0006\u0002\n\u0000R\u0013\u0010\u0013\u001a\u0004\u0018\u00010\u00018F¢\u0006\u0006\u001a\u0004\b\u0014\u0010\u0015R\u0011\u0010\u0016\u001a\u00020\n8F¢\u0006\u0006\u001a\u0004\b\u0017\u0010\rR\u0011\u0010\u0018\u001a\u00020\n8F¢\u0006\u0006\u001a\u0004\b\u0019\u0010\rR\u0013\u0010\u001a\u001a\u0004\u0018\u00010\u00018F¢\u0006\u0006\u001a\u0004\b\u001b\u0010\u0015R\u0013\u0010\u001c\u001a\u0004\u0018\u00010\u00018F¢\u0006\u0006\u001a\u0004\b\u001d\u0010\u0015R\u0011\u0010\u001e\u001a\u00020\n8F¢\u0006\u0006\u001a\u0004\b\u001f\u0010\rR\u0011\u0010 \u001a\u00020\n8F¢\u0006\u0006\u001a\u0004\b!\u0010\rR\u0011\u0010\"\u001a\u00020\n8F¢\u0006\u0006\u001a\u0004\b#\u0010\rR\u000e\u0010$\u001a\u00020%X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010&\u001a\u00020\nX\u0082\u0004¢\u0006\u0002\n\u0000R\u0011\u0010'\u001a\u00020\u00068F¢\u0006\u0006\u001a\u0004\b(\u0010\tR\u0011\u0010)\u001a\u00020\u00068F¢\u0006\u0006\u001a\u0004\b)\u0010\tR\u0011\u0010*\u001a\u00020\u00068F¢\u0006\u0006\u001a\u0004\b*\u0010\tR\u0011\u0010+\u001a\u00020\n8F¢\u0006\u0006\u001a\u0004\b,\u0010\rR\u001e\u0010-\u001a\u00020\n2\u0006\u0010\u0005\u001a\u00020\n@BX\u0086\u000e¢\u0006\b\n\u0000\u001a\u0004\b.\u0010\rR\u0011\u0010/\u001a\u00020\n8F¢\u0006\u0006\u001a\u0004\b0\u0010\rR\u0011\u00101\u001a\u00020\n8F¢\u0006\u0006\u001a\u0004\b2\u0010\rR\u0011\u00103\u001a\u00020\n8F¢\u0006\u0006\u001a\u0004\b4\u0010\rR\u0018\u00105\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u000106X\u0082\u0004¢\u0006\u0004\n\u0002\u00107R\u000e\u00108\u001a\u00020\nX\u0082\u0004¢\u0006\u0002\n\u0000R\u0014\u0010\u0002\u001a\u00020\u0003X\u0080\u0004¢\u0006\b\n\u0000\u001a\u0004\b9\u0010:¨\u0006`" }, d2 = { "Landroidx/compose/runtime/SlotReader;", "", "table", "Landroidx/compose/runtime/SlotTable;", "(Landroidx/compose/runtime/SlotTable;)V", "<set-?>", "", "closed", "getClosed", "()Z", "", "currentEnd", "getCurrentEnd", "()I", "currentGroup", "getCurrentGroup", "currentSlot", "currentSlotEnd", "emptyCount", "groupAux", "getGroupAux", "()Ljava/lang/Object;", "groupEnd", "getGroupEnd", "groupKey", "getGroupKey", "groupNode", "getGroupNode", "groupObjectKey", "getGroupObjectKey", "groupSize", "getGroupSize", "groupSlotCount", "getGroupSlotCount", "groupSlotIndex", "getGroupSlotIndex", "groups", "", "groupsSize", "inEmpty", "getInEmpty", "isGroupEnd", "isNode", "nodeCount", "getNodeCount", "parent", "getParent", "parentNodes", "getParentNodes", "size", "getSize", "slot", "getSlot", "slots", "", "[Ljava/lang/Object;", "slotsSize", "getTable$runtime_release", "()Landroidx/compose/runtime/SlotTable;", "anchor", "Landroidx/compose/runtime/Anchor;", "index", "beginEmpty", "", "close", "containsMark", "endEmpty", "endGroup", "extractKeys", "", "Landroidx/compose/runtime/KeyInfo;", "forEachData", "group", "block", "Lkotlin/Function2;", "Lkotlin/ParameterName;", "name", "data", "forEachData$runtime_release", "get", "groupGet", "hasMark", "hasObjectKey", "next", "node", "parentOf", "reposition", "restoreParent", "skipGroup", "skipToGroupEnd", "startGroup", "startNode", "toString", "", "aux", "objectKey", "runtime_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
public final class SlotReader
{
    private boolean closed;
    private int currentEnd;
    private int currentGroup;
    private int currentSlot;
    private int currentSlotEnd;
    private int emptyCount;
    private final int[] groups;
    private final int groupsSize;
    private int parent;
    private final Object[] slots;
    private final int slotsSize;
    private final SlotTable table;
    
    public SlotReader(final SlotTable table) {
        Intrinsics.checkNotNullParameter((Object)table, "table");
        this.table = table;
        this.groups = table.getGroups();
        final int groupsSize = table.getGroupsSize();
        this.groupsSize = groupsSize;
        this.slots = table.getSlots();
        this.slotsSize = table.getSlotsSize();
        this.currentEnd = groupsSize;
        this.parent = -1;
    }
    
    private final Object aux(final int[] array, final int n) {
        Object empty;
        if (SlotTableKt.access$hasAux(array, n)) {
            empty = this.slots[SlotTableKt.access$auxIndex(array, n)];
        }
        else {
            empty = Composer.Companion.getEmpty();
        }
        return empty;
    }
    
    private final Object node(final int[] array, final int n) {
        Object empty;
        if (SlotTableKt.access$isNode(array, n)) {
            empty = this.slots[SlotTableKt.access$nodeIndex(array, n)];
        }
        else {
            empty = Composer.Companion.getEmpty();
        }
        return empty;
    }
    
    private final Object objectKey(final int[] array, final int n) {
        Object o;
        if (SlotTableKt.access$hasObjectKey(array, n)) {
            o = this.slots[SlotTableKt.access$objectKeyIndex(array, n)];
        }
        else {
            o = null;
        }
        return o;
    }
    
    public final Anchor anchor(final int n) {
        final ArrayList<Anchor> anchors$runtime_release = this.table.getAnchors$runtime_release();
        final int access$search = SlotTableKt.access$search(anchors$runtime_release, n, this.groupsSize);
        Anchor element;
        if (access$search < 0) {
            element = new Anchor(n);
            anchors$runtime_release.add(-(access$search + 1), element);
        }
        else {
            final Anchor value = anchors$runtime_release.get(access$search);
            Intrinsics.checkNotNullExpressionValue((Object)value, "get(location)");
            element = value;
        }
        return element;
    }
    
    public final void beginEmpty() {
        ++this.emptyCount;
    }
    
    public final void close() {
        this.closed = true;
        this.table.close$runtime_release(this);
    }
    
    public final boolean containsMark(final int n) {
        return SlotTableKt.access$containsMark(this.groups, n);
    }
    
    public final void endEmpty() {
        final int emptyCount = this.emptyCount;
        if (emptyCount > 0) {
            this.emptyCount = emptyCount - 1;
            return;
        }
        throw new IllegalArgumentException("Unbalanced begin/end empty".toString());
    }
    
    public final void endGroup() {
        if (this.emptyCount == 0) {
            if (this.currentGroup != this.currentEnd) {
                ComposerKt.composeRuntimeError("endGroup() not called at the end of a group".toString());
                throw new KotlinNothingValueException();
            }
            final int access$parentAnchor = SlotTableKt.access$parentAnchor(this.groups, this.parent);
            int groupsSize;
            if ((this.parent = access$parentAnchor) < 0) {
                groupsSize = this.groupsSize;
            }
            else {
                groupsSize = access$parentAnchor + SlotTableKt.access$groupSize(this.groups, access$parentAnchor);
            }
            this.currentEnd = groupsSize;
        }
    }
    
    public final List<KeyInfo> extractKeys() {
        final List list = new ArrayList();
        if (this.emptyCount > 0) {
            return list;
        }
        for (int i = this.currentGroup, n = 0; i < this.currentEnd; i += SlotTableKt.access$groupSize(this.groups, i), ++n) {
            final int access$key = SlotTableKt.access$key(this.groups, i);
            final Object objectKey = this.objectKey(this.groups, i);
            int access$nodeCount;
            if (SlotTableKt.access$isNode(this.groups, i)) {
                access$nodeCount = 1;
            }
            else {
                access$nodeCount = SlotTableKt.access$nodeCount(this.groups, i);
            }
            list.add(new KeyInfo(access$key, objectKey, i, access$nodeCount, n));
        }
        return list;
    }
    
    public final void forEachData$runtime_release(int n, final Function2<? super Integer, Object, Unit> function2) {
        Intrinsics.checkNotNullParameter((Object)function2, "block");
        final int access$slotAnchor = SlotTableKt.access$slotAnchor(this.groups, n);
        if (++n < this.table.getGroupsSize()) {
            n = SlotTableKt.access$dataAnchor(this.table.getGroups(), n);
        }
        else {
            n = this.table.getSlotsSize();
        }
        for (int i = access$slotAnchor; i < n; ++i) {
            function2.invoke((Object)(i - access$slotAnchor), this.slots[i]);
        }
    }
    
    public final Object get(int n) {
        n += this.currentSlot;
        Object empty;
        if (n < this.currentSlotEnd) {
            empty = this.slots[n];
        }
        else {
            empty = Composer.Companion.getEmpty();
        }
        return empty;
    }
    
    public final boolean getClosed() {
        return this.closed;
    }
    
    public final int getCurrentEnd() {
        return this.currentEnd;
    }
    
    public final int getCurrentGroup() {
        return this.currentGroup;
    }
    
    public final Object getGroupAux() {
        final int currentGroup = this.currentGroup;
        Object o;
        if (currentGroup < this.currentEnd) {
            o = this.aux(this.groups, currentGroup);
        }
        else {
            o = 0;
        }
        return o;
    }
    
    public final int getGroupEnd() {
        return this.currentEnd;
    }
    
    public final int getGroupKey() {
        final int currentGroup = this.currentGroup;
        int access$key;
        if (currentGroup < this.currentEnd) {
            access$key = SlotTableKt.access$key(this.groups, currentGroup);
        }
        else {
            access$key = 0;
        }
        return access$key;
    }
    
    public final Object getGroupNode() {
        final int currentGroup = this.currentGroup;
        Object node;
        if (currentGroup < this.currentEnd) {
            node = this.node(this.groups, currentGroup);
        }
        else {
            node = null;
        }
        return node;
    }
    
    public final Object getGroupObjectKey() {
        final int currentGroup = this.currentGroup;
        Object objectKey;
        if (currentGroup < this.currentEnd) {
            objectKey = this.objectKey(this.groups, currentGroup);
        }
        else {
            objectKey = null;
        }
        return objectKey;
    }
    
    public final int getGroupSize() {
        return SlotTableKt.access$groupSize(this.groups, this.currentGroup);
    }
    
    public final int getGroupSlotCount() {
        int currentGroup = this.currentGroup;
        final int access$slotAnchor = SlotTableKt.access$slotAnchor(this.groups, currentGroup);
        int n;
        if (++currentGroup < this.groupsSize) {
            n = SlotTableKt.access$dataAnchor(this.groups, currentGroup);
        }
        else {
            n = this.slotsSize;
        }
        return n - access$slotAnchor;
    }
    
    public final int getGroupSlotIndex() {
        return this.currentSlot - SlotTableKt.access$slotAnchor(this.groups, this.parent);
    }
    
    public final boolean getInEmpty() {
        return this.emptyCount > 0;
    }
    
    public final int getNodeCount() {
        return SlotTableKt.access$nodeCount(this.groups, this.currentGroup);
    }
    
    public final int getParent() {
        return this.parent;
    }
    
    public final int getParentNodes() {
        final int parent = this.parent;
        int access$nodeCount;
        if (parent >= 0) {
            access$nodeCount = SlotTableKt.access$nodeCount(this.groups, parent);
        }
        else {
            access$nodeCount = 0;
        }
        return access$nodeCount;
    }
    
    public final int getSize() {
        return this.groupsSize;
    }
    
    public final int getSlot() {
        return this.currentSlot - SlotTableKt.access$slotAnchor(this.groups, this.parent);
    }
    
    public final SlotTable getTable$runtime_release() {
        return this.table;
    }
    
    public final Object groupAux(final int n) {
        return this.aux(this.groups, n);
    }
    
    public final int groupEnd(final int n) {
        return n + SlotTableKt.access$groupSize(this.groups, n);
    }
    
    public final Object groupGet(final int n) {
        return this.groupGet(this.currentGroup, n);
    }
    
    public final Object groupGet(int n, int n2) {
        final int access$slotAnchor = SlotTableKt.access$slotAnchor(this.groups, n);
        if (++n < this.groupsSize) {
            n = SlotTableKt.access$dataAnchor(this.groups, n);
        }
        else {
            n = this.slotsSize;
        }
        n2 += access$slotAnchor;
        Object empty;
        if (n2 < n) {
            empty = this.slots[n2];
        }
        else {
            empty = Composer.Companion.getEmpty();
        }
        return empty;
    }
    
    public final int groupKey(final int n) {
        return SlotTableKt.access$key(this.groups, n);
    }
    
    public final int groupKey(final Anchor anchor) {
        Intrinsics.checkNotNullParameter((Object)anchor, "anchor");
        int access$key;
        if (anchor.getValid()) {
            access$key = SlotTableKt.access$key(this.groups, this.table.anchorIndex(anchor));
        }
        else {
            access$key = 0;
        }
        return access$key;
    }
    
    public final Object groupObjectKey(final int n) {
        return this.objectKey(this.groups, n);
    }
    
    public final int groupSize(final int n) {
        return SlotTableKt.access$groupSize(this.groups, n);
    }
    
    public final boolean hasMark(final int n) {
        return SlotTableKt.access$hasMark(this.groups, n);
    }
    
    public final boolean hasObjectKey(final int n) {
        return SlotTableKt.access$hasObjectKey(this.groups, n);
    }
    
    public final boolean isGroupEnd() {
        return this.getInEmpty() || this.currentGroup == this.currentEnd;
    }
    
    public final boolean isNode() {
        return SlotTableKt.access$isNode(this.groups, this.currentGroup);
    }
    
    public final boolean isNode(final int n) {
        return SlotTableKt.access$isNode(this.groups, n);
    }
    
    public final Object next() {
        if (this.emptyCount <= 0) {
            final int currentSlot = this.currentSlot;
            if (currentSlot < this.currentSlotEnd) {
                final Object[] slots = this.slots;
                this.currentSlot = currentSlot + 1;
                return slots[currentSlot];
            }
        }
        return Composer.Companion.getEmpty();
    }
    
    public final Object node(final int n) {
        Object node;
        if (SlotTableKt.access$isNode(this.groups, n)) {
            node = this.node(this.groups, n);
        }
        else {
            node = null;
        }
        return node;
    }
    
    public final int nodeCount(final int n) {
        return SlotTableKt.access$nodeCount(this.groups, n);
    }
    
    public final int parent(final int n) {
        return SlotTableKt.access$parentAnchor(this.groups, n);
    }
    
    public final int parentOf(final int i) {
        if (i >= 0 && i < this.groupsSize) {
            return SlotTableKt.access$parentAnchor(this.groups, i);
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Invalid group index ");
        sb.append(i);
        throw new IllegalArgumentException(sb.toString().toString());
    }
    
    public final void reposition(int access$parentAnchor) {
        if (this.emptyCount == 0) {
            if ((this.currentGroup = access$parentAnchor) < this.groupsSize) {
                access$parentAnchor = SlotTableKt.access$parentAnchor(this.groups, access$parentAnchor);
            }
            else {
                access$parentAnchor = -1;
            }
            this.parent = access$parentAnchor;
            if (access$parentAnchor < 0) {
                this.currentEnd = this.groupsSize;
            }
            else {
                this.currentEnd = access$parentAnchor + SlotTableKt.access$groupSize(this.groups, access$parentAnchor);
            }
            this.currentSlot = 0;
            this.currentSlotEnd = 0;
            return;
        }
        ComposerKt.composeRuntimeError("Cannot reposition while in an empty region".toString());
        throw new KotlinNothingValueException();
    }
    
    public final void restoreParent(final int n) {
        final int currentEnd = SlotTableKt.access$groupSize(this.groups, n) + n;
        final int currentGroup = this.currentGroup;
        if (currentGroup >= n && currentGroup <= currentEnd) {
            this.parent = n;
            this.currentEnd = currentEnd;
            this.currentSlot = 0;
            this.currentSlotEnd = 0;
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Index ");
        sb.append(n);
        sb.append(" is not a parent of ");
        sb.append(currentGroup);
        ComposerKt.composeRuntimeError(sb.toString().toString());
        throw new KotlinNothingValueException();
    }
    
    public final int skipGroup() {
        final int emptyCount = this.emptyCount;
        final int n = 1;
        if (emptyCount == 0) {
            int access$nodeCount;
            if (SlotTableKt.access$isNode(this.groups, this.currentGroup)) {
                access$nodeCount = n;
            }
            else {
                access$nodeCount = SlotTableKt.access$nodeCount(this.groups, this.currentGroup);
            }
            final int currentGroup = this.currentGroup;
            this.currentGroup = currentGroup + SlotTableKt.access$groupSize(this.groups, currentGroup);
            return access$nodeCount;
        }
        ComposerKt.composeRuntimeError("Cannot skip while in an empty region".toString());
        throw new KotlinNothingValueException();
    }
    
    public final void skipToGroupEnd() {
        if (this.emptyCount == 0) {
            this.currentGroup = this.currentEnd;
            return;
        }
        ComposerKt.composeRuntimeError("Cannot skip the enclosing group while in an empty region".toString());
        throw new KotlinNothingValueException();
    }
    
    public final void startGroup() {
        if (this.emptyCount <= 0) {
            if (SlotTableKt.access$parentAnchor(this.groups, this.currentGroup) != this.parent) {
                throw new IllegalArgumentException("Invalid slot table detected".toString());
            }
            final int currentGroup = this.currentGroup;
            this.parent = currentGroup;
            this.currentEnd = currentGroup + SlotTableKt.access$groupSize(this.groups, currentGroup);
            final int currentGroup2 = this.currentGroup;
            final int currentGroup3 = currentGroup2 + 1;
            this.currentGroup = currentGroup3;
            this.currentSlot = SlotTableKt.access$slotAnchor(this.groups, currentGroup2);
            int currentSlotEnd;
            if (currentGroup2 >= this.groupsSize - 1) {
                currentSlotEnd = this.slotsSize;
            }
            else {
                currentSlotEnd = SlotTableKt.access$dataAnchor(this.groups, currentGroup3);
            }
            this.currentSlotEnd = currentSlotEnd;
        }
    }
    
    public final void startNode() {
        if (this.emptyCount <= 0) {
            if (!SlotTableKt.access$isNode(this.groups, this.currentGroup)) {
                throw new IllegalArgumentException("Expected a node group".toString());
            }
            this.startGroup();
        }
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("SlotReader(current=");
        sb.append(this.currentGroup);
        sb.append(", key=");
        sb.append(this.getGroupKey());
        sb.append(", parent=");
        sb.append(this.parent);
        sb.append(", end=");
        sb.append(this.currentEnd);
        sb.append(')');
        return sb.toString();
    }
}

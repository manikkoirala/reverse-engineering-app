// 
// Decompiled by Procyon v0.6.0
// 

package androidx.compose.runtime;

import kotlin.coroutines.jvm.internal.DebugProbesKt;
import android.view.Choreographer$FrameCallback;
import kotlinx.coroutines.CancellableContinuation;
import kotlinx.coroutines.CancellableContinuationImpl;
import kotlin.coroutines.intrinsics.IntrinsicsKt;
import kotlin.jvm.functions.Function1;
import kotlin.coroutines.CoroutineContext$Key;
import kotlin.coroutines.CoroutineContext$Element;
import kotlinx.coroutines.BuildersKt;
import kotlin.coroutines.Continuation;
import kotlin.jvm.functions.Function2;
import kotlinx.coroutines.Dispatchers;
import kotlin.coroutines.CoroutineContext;
import android.view.Choreographer;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0004\b\u00c2\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J:\u0010\u0006\u001a\u0002H\u0007\"\u0004\b\u0000\u0010\u00072!\u0010\b\u001a\u001d\u0012\u0013\u0012\u00110\n¢\u0006\f\b\u000b\u0012\b\b\f\u0012\u0004\b\b(\r\u0012\u0004\u0012\u0002H\u00070\tH\u0096@\u00f8\u0001\u0000¢\u0006\u0002\u0010\u000eR\u0016\u0010\u0003\u001a\n \u0005*\u0004\u0018\u00010\u00040\u0004X\u0082\u0004¢\u0006\u0002\n\u0000\u0082\u0002\u0004\n\u0002\b\u0019¨\u0006\u000f" }, d2 = { "Landroidx/compose/runtime/DefaultChoreographerFrameClock;", "Landroidx/compose/runtime/MonotonicFrameClock;", "()V", "choreographer", "Landroid/view/Choreographer;", "kotlin.jvm.PlatformType", "withFrameNanos", "R", "onFrame", "Lkotlin/Function1;", "", "Lkotlin/ParameterName;", "name", "frameTimeNanos", "(Lkotlin/jvm/functions/Function1;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "runtime_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
final class DefaultChoreographerFrameClock implements MonotonicFrameClock
{
    public static final DefaultChoreographerFrameClock INSTANCE;
    private static final Choreographer choreographer;
    
    static {
        INSTANCE = new DefaultChoreographerFrameClock();
        choreographer = (Choreographer)BuildersKt.runBlocking((CoroutineContext)Dispatchers.getMain().getImmediate(), (Function2)new DefaultChoreographerFrameClock$choreographer.DefaultChoreographerFrameClock$choreographer$1((Continuation)null));
    }
    
    private DefaultChoreographerFrameClock() {
    }
    
    public static final /* synthetic */ Choreographer access$getChoreographer$p() {
        return DefaultChoreographerFrameClock.choreographer;
    }
    
    public <R> R fold(final R r, final Function2<? super R, ? super CoroutineContext$Element, ? extends R> function2) {
        return DefaultImpls.fold(this, r, function2);
    }
    
    public <E extends CoroutineContext$Element> E get(final CoroutineContext$Key<E> coroutineContext$Key) {
        return DefaultImpls.get(this, coroutineContext$Key);
    }
    
    public CoroutineContext minusKey(final CoroutineContext$Key<?> coroutineContext$Key) {
        return DefaultImpls.minusKey(this, coroutineContext$Key);
    }
    
    public CoroutineContext plus(final CoroutineContext coroutineContext) {
        return DefaultImpls.plus(this, coroutineContext);
    }
    
    @Override
    public <R> Object withFrameNanos(final Function1<? super Long, ? extends R> function1, final Continuation<? super R> continuation) {
        final CancellableContinuationImpl cancellableContinuationImpl = new CancellableContinuationImpl(IntrinsicsKt.intercepted((Continuation)continuation), 1);
        cancellableContinuationImpl.initCancellability();
        final CancellableContinuation cancellableContinuation = (CancellableContinuation)cancellableContinuationImpl;
        final Choreographer$FrameCallback choreographer$FrameCallback = (Choreographer$FrameCallback)new DefaultChoreographerFrameClock$withFrameNanos$2$callback.DefaultChoreographerFrameClock$withFrameNanos$2$callback$1(cancellableContinuation, (Function1)function1);
        access$getChoreographer$p().postFrameCallback(choreographer$FrameCallback);
        cancellableContinuation.invokeOnCancellation((Function1)new DefaultChoreographerFrameClock$withFrameNanos$2.DefaultChoreographerFrameClock$withFrameNanos$2$1(choreographer$FrameCallback));
        final Object result = cancellableContinuationImpl.getResult();
        if (result == IntrinsicsKt.getCOROUTINE_SUSPENDED()) {
            DebugProbesKt.probeCoroutineSuspended((Continuation)continuation);
        }
        return result;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.compose.runtime;

import kotlin.jvm.internal.Intrinsics;
import kotlin.Result$Companion;
import kotlin.coroutines.jvm.internal.DebugProbesKt;
import kotlin.Result;
import kotlinx.coroutines.CancellableContinuation;
import kotlinx.coroutines.CancellableContinuationImpl;
import kotlin.coroutines.intrinsics.IntrinsicsKt;
import kotlin.Unit;
import kotlin.coroutines.Continuation;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0002\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\u0019\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0001H\u0086@\u00f8\u0001\u0000¢\u0006\u0002\u0010\u0007J\u000e\u0010\b\u001a\n\u0012\u0004\u0012\u00020\u0005\u0018\u00010\tJ\u0006\u0010\n\u001a\u00020\u0005R\u0010\u0010\u0003\u001a\u0004\u0018\u00010\u0001X\u0082\u000e¢\u0006\u0002\n\u0000\u0082\u0002\u0004\n\u0002\b\u0019¨\u0006\u000b" }, d2 = { "Landroidx/compose/runtime/ProduceFrameSignal;", "", "()V", "pendingFrameContinuation", "awaitFrameRequest", "", "lock", "(Ljava/lang/Object;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "requestFrameLocked", "Lkotlin/coroutines/Continuation;", "takeFrameRequestLocked", "runtime_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
final class ProduceFrameSignal
{
    private Object pendingFrameContinuation;
    
    public ProduceFrameSignal() {
    }
    
    public static final /* synthetic */ Object access$getPendingFrameContinuation$p(final ProduceFrameSignal produceFrameSignal) {
        return produceFrameSignal.pendingFrameContinuation;
    }
    
    public static final /* synthetic */ void access$setPendingFrameContinuation$p(final ProduceFrameSignal produceFrameSignal, final Object pendingFrameContinuation) {
        produceFrameSignal.pendingFrameContinuation = pendingFrameContinuation;
    }
    
    public final Object awaitFrameRequest(Object result, final Continuation<? super Unit> continuation) {
        synchronized (result) {
            if (this.pendingFrameContinuation == RecomposerKt.access$getProduceAnotherFrame$p()) {
                this.pendingFrameContinuation = RecomposerKt.access$getFramePending$p();
                return Unit.INSTANCE;
            }
            final Unit instance = Unit.INSTANCE;
            monitorexit(result);
            final CancellableContinuationImpl cancellableContinuationImpl = new CancellableContinuationImpl(IntrinsicsKt.intercepted((Continuation)continuation), 1);
            cancellableContinuationImpl.initCancellability();
            Object o = cancellableContinuationImpl;
            synchronized (result) {
                if (access$getPendingFrameContinuation$p(this) == RecomposerKt.access$getProduceAnotherFrame$p()) {
                    access$setPendingFrameContinuation$p(this, RecomposerKt.access$getFramePending$p());
                }
                else {
                    access$setPendingFrameContinuation$p(this, o);
                    o = null;
                }
                monitorexit(result);
                if (o != null) {
                    result = o;
                    final Result$Companion companion = Result.Companion;
                    ((Continuation)result).resumeWith(Result.constructor-impl((Object)Unit.INSTANCE));
                }
                result = cancellableContinuationImpl.getResult();
                if (result == IntrinsicsKt.getCOROUTINE_SUSPENDED()) {
                    DebugProbesKt.probeCoroutineSuspended((Continuation)continuation);
                }
                if (result == IntrinsicsKt.getCOROUTINE_SUSPENDED()) {
                    return result;
                }
                return Unit.INSTANCE;
            }
        }
    }
    
    public final Continuation<Unit> requestFrameLocked() {
        final Object pendingFrameContinuation = this.pendingFrameContinuation;
        final boolean b = pendingFrameContinuation instanceof Continuation;
        Continuation continuation = null;
        if (b) {
            this.pendingFrameContinuation = RecomposerKt.access$getFramePending$p();
            continuation = (Continuation)pendingFrameContinuation;
        }
        else if (!Intrinsics.areEqual(pendingFrameContinuation, RecomposerKt.access$getProduceAnotherFrame$p()) && !Intrinsics.areEqual(pendingFrameContinuation, RecomposerKt.access$getFramePending$p())) {
            if (pendingFrameContinuation != null) {
                final StringBuilder sb = new StringBuilder();
                sb.append("invalid pendingFrameContinuation ");
                sb.append(pendingFrameContinuation);
                throw new IllegalStateException(sb.toString().toString());
            }
            this.pendingFrameContinuation = RecomposerKt.access$getProduceAnotherFrame$p();
        }
        return (Continuation<Unit>)continuation;
    }
    
    public final void takeFrameRequestLocked() {
        if (this.pendingFrameContinuation == RecomposerKt.access$getFramePending$p()) {
            this.pendingFrameContinuation = null;
            return;
        }
        throw new IllegalStateException("frame not pending".toString());
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.compose.runtime.reflect;

import kotlin.Metadata;

@Metadata(d1 = { "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\b\n\u0002\b\u0011\n\u0002\u0010\u000e\n\u0000\b\u0080\b\u0018\u00002\u00020\u0001B%\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0005\u0012\u0006\u0010\u0007\u001a\u00020\u0005¢\u0006\u0002\u0010\bJ\t\u0010\u000e\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u000f\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u0010\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u0011\u001a\u00020\u0005H\u00c6\u0003J1\u0010\u0012\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00052\b\b\u0002\u0010\u0006\u001a\u00020\u00052\b\b\u0002\u0010\u0007\u001a\u00020\u0005H\u00c6\u0001J\u0013\u0010\u0013\u001a\u00020\u00032\b\u0010\u0014\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u0015\u001a\u00020\u0005H\u00d6\u0001J\t\u0010\u0016\u001a\u00020\u0017H\u00d6\u0001R\u0011\u0010\u0006\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0011\u0010\u0007\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\nR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0002\u0010\fR\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\n¨\u0006\u0018" }, d2 = { "Landroidx/compose/runtime/reflect/ComposableInfo;", "", "isComposable", "", "realParamsCount", "", "changedParams", "defaultParams", "(ZIII)V", "getChangedParams", "()I", "getDefaultParams", "()Z", "getRealParamsCount", "component1", "component2", "component3", "component4", "copy", "equals", "other", "hashCode", "toString", "", "runtime_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
public final class ComposableInfo
{
    private final int changedParams;
    private final int defaultParams;
    private final boolean isComposable;
    private final int realParamsCount;
    
    public ComposableInfo(final boolean isComposable, final int realParamsCount, final int changedParams, final int defaultParams) {
        this.isComposable = isComposable;
        this.realParamsCount = realParamsCount;
        this.changedParams = changedParams;
        this.defaultParams = defaultParams;
    }
    
    public final boolean component1() {
        return this.isComposable;
    }
    
    public final int component2() {
        return this.realParamsCount;
    }
    
    public final int component3() {
        return this.changedParams;
    }
    
    public final int component4() {
        return this.defaultParams;
    }
    
    public final ComposableInfo copy(final boolean b, final int n, final int n2, final int n3) {
        return new ComposableInfo(b, n, n2, n3);
    }
    
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ComposableInfo)) {
            return false;
        }
        final ComposableInfo composableInfo = (ComposableInfo)o;
        return this.isComposable == composableInfo.isComposable && this.realParamsCount == composableInfo.realParamsCount && this.changedParams == composableInfo.changedParams && this.defaultParams == composableInfo.defaultParams;
    }
    
    public final int getChangedParams() {
        return this.changedParams;
    }
    
    public final int getDefaultParams() {
        return this.defaultParams;
    }
    
    public final int getRealParamsCount() {
        return this.realParamsCount;
    }
    
    @Override
    public int hashCode() {
        int isComposable;
        if ((isComposable = (this.isComposable ? 1 : 0)) != 0) {
            isComposable = 1;
        }
        return ((isComposable * 31 + this.realParamsCount) * 31 + this.changedParams) * 31 + this.defaultParams;
    }
    
    public final boolean isComposable() {
        return this.isComposable;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("ComposableInfo(isComposable=");
        sb.append(this.isComposable);
        sb.append(", realParamsCount=");
        sb.append(this.realParamsCount);
        sb.append(", changedParams=");
        sb.append(this.changedParams);
        sb.append(", defaultParams=");
        sb.append(this.defaultParams);
        sb.append(')');
        return sb.toString();
    }
}

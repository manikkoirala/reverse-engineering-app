// 
// Decompiled by Procyon v0.6.0
// 

package androidx.compose.runtime.reflect;

import java.lang.constant.Constable;
import kotlin.jvm.internal.SpreadBuilder;
import java.lang.reflect.Modifier;
import androidx.compose.runtime.Composer;
import java.util.Iterator;
import java.util.List;
import kotlin.collections.IntIterator;
import java.util.ArrayList;
import kotlin.collections.CollectionsKt;
import java.util.Collection;
import kotlin.ranges.RangesKt;
import kotlin.jvm.internal.Intrinsics;
import java.lang.reflect.Method;
import kotlin.Metadata;

@Metadata(d1 = { "\u00006\n\u0000\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0011\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\u0000\n\u0000\u001a\u0018\u0010\u0002\u001a\u00020\u00012\u0006\u0010\u0003\u001a\u00020\u00012\u0006\u0010\u0004\u001a\u00020\u0001H\u0002\u001a\u0010\u0010\u0005\u001a\u00020\u00012\u0006\u0010\u0003\u001a\u00020\u0001H\u0002\u001a\f\u0010\u0006\u001a\u0004\u0018\u00010\u0007*\u00020\b\u001a(\u0010\t\u001a\b\u0012\u0004\u0012\u0002H\u000b0\n\"\u0006\b\u0000\u0010\u000b\u0018\u0001*\u0002H\u000b2\u0006\u0010\f\u001a\u00020\u0001H\u0082\b¢\u0006\u0002\u0010\r\u001a\f\u0010\u000e\u001a\u00020\u000f*\u00020\bH\u0002\u001a7\u0010\u0010\u001a\u00020\u0007*\u0006\u0012\u0002\b\u00030\u00112\u0006\u0010\u0012\u001a\u00020\u00132\u001a\u0010\u0014\u001a\u000e\u0012\n\b\u0001\u0012\u0006\u0012\u0002\b\u00030\u00110\n\"\u0006\u0012\u0002\b\u00030\u0011¢\u0006\u0002\u0010\u0015\u001a\u0012\u0010\u0016\u001a\u0004\u0018\u00010\u0017*\u0006\u0012\u0002\b\u00030\u0011H\u0002\"\u000e\u0010\u0000\u001a\u00020\u0001X\u0082T¢\u0006\u0002\n\u0000¨\u0006\u0018" }, d2 = { "BITS_PER_INT", "", "changedParamCount", "realValueParams", "thisParams", "defaultParamCount", "asComposableMethod", "Landroidx/compose/runtime/reflect/ComposableMethod;", "Ljava/lang/reflect/Method;", "dup", "", "T", "count", "(Ljava/lang/Object;I)[Ljava/lang/Object;", "getComposableInfo", "Landroidx/compose/runtime/reflect/ComposableInfo;", "getDeclaredComposableMethod", "Ljava/lang/Class;", "methodName", "", "args", "(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;)Landroidx/compose/runtime/reflect/ComposableMethod;", "getDefaultValue", "", "runtime_release" }, k = 2, mv = { 1, 8, 0 }, xi = 48)
public final class ComposableMethodKt
{
    private static final int BITS_PER_INT = 31;
    
    public static final ComposableMethod asComposableMethod(final Method method) {
        Intrinsics.checkNotNullParameter((Object)method, "<this>");
        final ComposableInfo composableInfo = getComposableInfo(method);
        if (composableInfo.isComposable()) {
            return new ComposableMethod(method, composableInfo);
        }
        return null;
    }
    
    private static final int changedParamCount(final int n, final int n2) {
        if (n == 0) {
            return 1;
        }
        return (int)Math.ceil((n + n2) / 10.0);
    }
    
    private static final int defaultParamCount(final int n) {
        return (int)Math.ceil(n / 31.0);
    }
    
    private static final ComposableInfo getComposableInfo(final Method method) {
        final Class<?>[] parameterTypes = method.getParameterTypes();
        Intrinsics.checkNotNullExpressionValue((Object)parameterTypes, "parameterTypes");
        final Object[] array = parameterTypes;
        int n = array.length - 1;
        Label_0066: {
            if (n >= 0) {
                while (true) {
                    final int n2 = n - 1;
                    if (Intrinsics.areEqual((Object)array[n], (Object)Composer.class)) {
                        break Label_0066;
                    }
                    if (n2 < 0) {
                        break;
                    }
                    n = n2;
                }
            }
            n = -1;
        }
        boolean b = false;
        if (n == -1) {
            return new ComposableInfo(false, method.getParameterTypes().length, 0, 0);
        }
        final int changedParamCount = changedParamCount(n, (Modifier.isStatic(method.getModifiers()) ^ true) ? 1 : 0);
        final int n3 = n + 1 + changedParamCount;
        final int length = method.getParameterTypes().length;
        int defaultParamCount;
        if (length != n3) {
            defaultParamCount = defaultParamCount(n);
        }
        else {
            defaultParamCount = 0;
        }
        if (n3 + defaultParamCount == length) {
            b = true;
        }
        return new ComposableInfo(b, n, changedParamCount, defaultParamCount);
    }
    
    public static final ComposableMethod getDeclaredComposableMethod(final Class<?> clazz, final String str, Class<?>... genericDeclaration) throws NoSuchMethodException {
        Intrinsics.checkNotNullParameter((Object)clazz, "<this>");
        Intrinsics.checkNotNullParameter((Object)str, "methodName");
        Intrinsics.checkNotNullParameter((Object)genericDeclaration, "args");
        final int changedParamCount = changedParamCount(((Method)genericDeclaration).length, 0);
        try {
            final SpreadBuilder spreadBuilder = new SpreadBuilder(3);
            spreadBuilder.addSpread((Object)genericDeclaration);
            spreadBuilder.add((Object)Composer.class);
            final Class<Integer> type = Integer.TYPE;
            final Iterable iterable = (Iterable)RangesKt.until(0, changedParamCount);
            final Collection collection = new ArrayList<Class<Integer>>(CollectionsKt.collectionSizeOrDefault(iterable, 10));
            final Iterator iterator = iterable.iterator();
            while (iterator.hasNext()) {
                ((IntIterator)iterator).nextInt();
                collection.add(type);
            }
            spreadBuilder.addSpread((Object)((Collection)(List<E>)collection).toArray(new Class[0]));
            genericDeclaration = clazz.getDeclaredMethod(str, (Class[])spreadBuilder.toArray((Object[])new Class[spreadBuilder.size()]));
        }
        catch (final ReflectiveOperationException ex) {
            final int defaultParamCount = defaultParamCount(((Method)genericDeclaration).length);
            try {
                final SpreadBuilder spreadBuilder2 = new SpreadBuilder(4);
                spreadBuilder2.addSpread((Object)genericDeclaration);
                spreadBuilder2.add((Object)Composer.class);
                genericDeclaration = Integer.TYPE;
                final Iterable iterable2 = (Iterable)RangesKt.until(0, changedParamCount);
                final Collection collection2 = new ArrayList(CollectionsKt.collectionSizeOrDefault(iterable2, 10));
                final Iterator iterator2 = iterable2.iterator();
                while (iterator2.hasNext()) {
                    ((IntIterator)iterator2).nextInt();
                    collection2.add(genericDeclaration);
                }
                spreadBuilder2.addSpread((Object)((Collection)(List<E>)collection2).toArray(new Class[0]));
                genericDeclaration = Integer.TYPE;
                final Iterable iterable3 = (Iterable)RangesKt.until(0, defaultParamCount);
                final Collection collection3 = new ArrayList(CollectionsKt.collectionSizeOrDefault(iterable3, 10));
                final Iterator iterator3 = iterable3.iterator();
                while (iterator3.hasNext()) {
                    ((IntIterator)iterator3).nextInt();
                    collection3.add(genericDeclaration);
                }
                spreadBuilder2.addSpread((Object)((Collection)(List<E>)collection3).toArray(new Class[0]));
                genericDeclaration = clazz.getDeclaredMethod(str, (Class[])spreadBuilder2.toArray((Object[])new Class[spreadBuilder2.size()]));
            }
            catch (final ReflectiveOperationException ex2) {
                genericDeclaration = null;
            }
        }
        if (genericDeclaration != null) {
            final ComposableMethod composableMethod = asComposableMethod((Method)genericDeclaration);
            Intrinsics.checkNotNull((Object)composableMethod);
            return composableMethod;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append(clazz.getName());
        sb.append('.');
        sb.append(str);
        throw new NoSuchMethodException(sb.toString());
    }
    
    private static final Object getDefaultValue(final Class<?> clazz) {
        final String name = clazz.getName();
        if (name != null) {
            switch (name) {
                case "short": {
                    return 0;
                }
                case "float": {
                    return 0.0f;
                }
                case "boolean": {
                    return false;
                }
                case "long": {
                    return 0L;
                }
                case "char": {
                    return '\0';
                }
                case "byte": {
                    return 0;
                }
                case "int": {
                    return 0;
                }
                case "double": {
                    return 0.0;
                }
                default:
                    break;
            }
        }
        return null;
    }
}

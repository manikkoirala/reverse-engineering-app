// 
// Decompiled by Procyon v0.6.0
// 

package androidx.compose.runtime.reflect;

import java.util.Iterator;
import java.util.Arrays;
import java.util.List;
import kotlin.collections.IntIterator;
import java.util.ArrayList;
import kotlin.collections.CollectionsKt;
import java.util.Collection;
import kotlin.ranges.RangesKt;
import androidx.compose.runtime.Composer;
import java.lang.reflect.Parameter;
import kotlin.collections.ArraysKt;
import kotlin.jvm.internal.Intrinsics;
import java.lang.reflect.Method;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000D\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u0011\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\b\u0007\u0018\u00002\u00020\u0001B\u0017\b\u0000\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\u0006\u0010\u0014\u001a\u00020\u0003J\u0013\u0010\u0015\u001a\u00020\u00162\b\u0010\u0017\u001a\u0004\u0018\u00010\u0001H\u0096\u0002J\b\u0010\u0018\u001a\u00020\bH\u0016J:\u0010\u0019\u001a\u0004\u0018\u00010\u00012\u0006\u0010\u001a\u001a\u00020\u001b2\b\u0010\u001c\u001a\u0004\u0018\u00010\u00012\u0016\u0010\u001d\u001a\f\u0012\b\b\u0001\u0012\u0004\u0018\u00010\u00010\f\"\u0004\u0018\u00010\u0001H\u0086\u0002¢\u0006\u0002\u0010\u001eR\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004¢\u0006\u0002\n\u0000R\u0011\u0010\u0007\u001a\u00020\b8F¢\u0006\u0006\u001a\u0004\b\t\u0010\nR\u001b\u0010\u000b\u001a\f\u0012\b\u0012\u0006\u0012\u0002\b\u00030\r0\f8F¢\u0006\u0006\u001a\u0004\b\u000e\u0010\u000fR\u0017\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u00110\f8F¢\u0006\u0006\u001a\u0004\b\u0012\u0010\u0013¨\u0006\u001f" }, d2 = { "Landroidx/compose/runtime/reflect/ComposableMethod;", "", "method", "Ljava/lang/reflect/Method;", "composableInfo", "Landroidx/compose/runtime/reflect/ComposableInfo;", "(Ljava/lang/reflect/Method;Landroidx/compose/runtime/reflect/ComposableInfo;)V", "parameterCount", "", "getParameterCount", "()I", "parameterTypes", "", "Ljava/lang/Class;", "getParameterTypes", "()[Ljava/lang/Class;", "parameters", "Ljava/lang/reflect/Parameter;", "getParameters", "()[Ljava/lang/reflect/Parameter;", "asMethod", "equals", "", "other", "hashCode", "invoke", "composer", "Landroidx/compose/runtime/Composer;", "instance", "args", "(Landroidx/compose/runtime/Composer;Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;", "runtime_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
public final class ComposableMethod
{
    public static final int $stable = 8;
    private final ComposableInfo composableInfo;
    private final Method method;
    
    public ComposableMethod(final Method method, final ComposableInfo composableInfo) {
        Intrinsics.checkNotNullParameter((Object)method, "method");
        Intrinsics.checkNotNullParameter((Object)composableInfo, "composableInfo");
        this.method = method;
        this.composableInfo = composableInfo;
    }
    
    public final Method asMethod() {
        return this.method;
    }
    
    @Override
    public boolean equals(final Object o) {
        return o instanceof ComposableMethod && Intrinsics.areEqual((Object)this.method, (Object)((ComposableMethod)o).method);
    }
    
    public final int getParameterCount() {
        return this.composableInfo.getRealParamsCount();
    }
    
    public final Class<?>[] getParameterTypes() {
        final Class<?>[] parameterTypes = this.method.getParameterTypes();
        Intrinsics.checkNotNullExpressionValue((Object)parameterTypes, "method.parameterTypes");
        return (Class[])ArraysKt.copyOfRange((Object[])parameterTypes, 0, this.composableInfo.getRealParamsCount());
    }
    
    public final Parameter[] getParameters() {
        final Parameter[] parameters = this.method.getParameters();
        Intrinsics.checkNotNullExpressionValue((Object)parameters, "method.parameters");
        return (Parameter[])ArraysKt.copyOfRange((Object[])parameters, 0, this.composableInfo.getRealParamsCount());
    }
    
    @Override
    public int hashCode() {
        return this.method.hashCode();
    }
    
    public final Object invoke(final Composer composer, final Object obj, final Object... array) {
        Intrinsics.checkNotNullParameter((Object)composer, "composer");
        Intrinsics.checkNotNullParameter((Object)array, "args");
        final ComposableInfo composableInfo = this.composableInfo;
        final int component2 = composableInfo.component2();
        final int component3 = composableInfo.component3();
        final int component4 = composableInfo.component4();
        final int length = this.method.getParameterTypes().length;
        final int n = component2 + 1;
        final int n2 = component3 + n;
        final Integer[] array2 = new Integer[component4];
        for (int i = 0; i < component4; ++i) {
            final int n3 = i * 31;
            final Iterable iterable = (Iterable)RangesKt.until(n3, Math.min(n3 + 31, component2));
            final Collection collection = new ArrayList(CollectionsKt.collectionSizeOrDefault(iterable, 10));
            final Iterator iterator = iterable.iterator();
            while (iterator.hasNext()) {
                final int nextInt = ((IntIterator)iterator).nextInt();
                int j;
                if (nextInt < array.length && array[nextInt] != null) {
                    j = 0;
                }
                else {
                    j = 1;
                }
                collection.add(j);
            }
            final Iterator iterator2 = collection.iterator();
            int k = 0;
            int n4 = 0;
            while (iterator2.hasNext()) {
                final Object next = iterator2.next();
                if (n4 < 0) {
                    CollectionsKt.throwIndexOverflow();
                }
                k |= ((Number)next).intValue() << n4;
                ++n4;
            }
            array2[i] = k;
        }
        final Object[] original = new Object[length];
        for (int l = 0; l < length; ++l) {
            Object o;
            if (l >= 0 && l < component2) {
                if (l >= 0 && l <= ArraysKt.getLastIndex(array)) {
                    o = array[l];
                }
                else {
                    final Class<?> clazz = this.method.getParameterTypes()[l];
                    Intrinsics.checkNotNullExpressionValue((Object)clazz, "method.parameterTypes[idx]");
                    o = ComposableMethodKt.access$getDefaultValue(clazz);
                }
            }
            else if (l == component2) {
                o = composer;
            }
            else if (l != n && (n + 1 > l || l >= n2)) {
                if (n2 > l || l >= length) {
                    throw new IllegalStateException("Unexpected index".toString());
                }
                o = array2[l - n2];
            }
            else {
                o = 0;
            }
            original[l] = o;
        }
        return this.method.invoke(obj, Arrays.copyOf(original, length));
    }
}

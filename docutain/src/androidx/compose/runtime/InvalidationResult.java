// 
// Decompiled by Procyon v0.6.0
// 

package androidx.compose.runtime;

import kotlin.Metadata;

@Metadata(d1 = { "\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0006\b\u0080\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002j\u0002\b\u0003j\u0002\b\u0004j\u0002\b\u0005j\u0002\b\u0006¨\u0006\u0007" }, d2 = { "Landroidx/compose/runtime/InvalidationResult;", "", "(Ljava/lang/String;I)V", "IGNORED", "SCHEDULED", "DEFERRED", "IMMINENT", "runtime_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
public enum InvalidationResult
{
    private static final InvalidationResult[] $VALUES;
    
    DEFERRED, 
    IGNORED, 
    IMMINENT, 
    SCHEDULED;
    
    private static final /* synthetic */ InvalidationResult[] $values() {
        return new InvalidationResult[] { InvalidationResult.IGNORED, InvalidationResult.SCHEDULED, InvalidationResult.DEFERRED, InvalidationResult.IMMINENT };
    }
    
    static {
        $VALUES = $values();
    }
}

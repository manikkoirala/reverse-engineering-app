// 
// Decompiled by Procyon v0.6.0
// 

package androidx.compose.runtime;

import androidx.compose.runtime.internal.ComposableLambdaKt;
import kotlin.Unit;
import kotlin.jvm.functions.Function2;
import kotlin.Metadata;

@Metadata(k = 3, mv = { 1, 8, 0 }, xi = 48)
public final class ComposableSingletons$CompositionKt
{
    public static final ComposableSingletons$CompositionKt INSTANCE;
    public static Function2<Composer, Integer, Unit> lambda-1;
    public static Function2<Composer, Integer, Unit> lambda-2;
    
    static {
        INSTANCE = new ComposableSingletons$CompositionKt();
        ComposableSingletons$CompositionKt.lambda-1 = (Function2<Composer, Integer, Unit>)ComposableLambdaKt.composableLambdaInstance(954879418, false, ComposableSingletons$CompositionKt$lambda_1.ComposableSingletons$CompositionKt$lambda_1$1.INSTANCE);
        ComposableSingletons$CompositionKt.lambda-2 = (Function2<Composer, Integer, Unit>)ComposableLambdaKt.composableLambdaInstance(1918065384, false, ComposableSingletons$CompositionKt$lambda_2.ComposableSingletons$CompositionKt$lambda_2$1.INSTANCE);
    }
    
    public final Function2<Composer, Integer, Unit> getLambda-1$runtime_release() {
        return ComposableSingletons$CompositionKt.lambda-1;
    }
    
    public final Function2<Composer, Integer, Unit> getLambda-2$runtime_release() {
        return ComposableSingletons$CompositionKt.lambda-2;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.compose.runtime;

import java.util.Iterator;
import java.util.Collection;
import java.util.LinkedHashSet;
import kotlin.LazyKt;
import kotlin.jvm.functions.Function0;
import java.util.Map;
import java.util.ArrayList;
import kotlin.jvm.internal.Intrinsics;
import kotlin.Lazy;
import java.util.List;
import java.util.HashMap;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000N\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010 \n\u0002\b\b\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\f\b\u0002\u0018\u00002\u00020\u0001B\u001b\u0012\f\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006¢\u0006\u0002\u0010\u0007J\u001a\u0010\u001f\u001a\u0004\u0018\u00010\u00042\u0006\u0010 \u001a\u00020\u00062\b\u0010!\u001a\u0004\u0018\u00010\u0001J\u000e\u0010\"\u001a\u00020\u00062\u0006\u0010#\u001a\u00020\u0004J\u000e\u0010$\u001a\u00020%2\u0006\u0010#\u001a\u00020\u0004J\u0016\u0010&\u001a\u00020'2\u0006\u0010#\u001a\u00020\u00042\u0006\u0010(\u001a\u00020\u0006J\u001e\u0010)\u001a\u00020'2\u0006\u0010*\u001a\u00020\u00062\u0006\u0010+\u001a\u00020\u00062\u0006\u0010,\u001a\u00020\u0006J\u0016\u0010-\u001a\u00020'2\u0006\u0010*\u001a\u00020\u00062\u0006\u0010+\u001a\u00020\u0006J\u000e\u0010.\u001a\u00020\u00062\u0006\u0010#\u001a\u00020\u0004J\u0016\u0010/\u001a\u00020%2\u0006\u00100\u001a\u00020\u00062\u0006\u00101\u001a\u00020\u0006J\u000e\u00102\u001a\u00020\u00062\u0006\u0010#\u001a\u00020\u0004R\u001a\u0010\b\u001a\u00020\u0006X\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\t\u0010\n\"\u0004\b\u000b\u0010\fR*\u0010\r\u001a\u001e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u000f0\u000ej\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u000f`\u0010X\u0082\u0004¢\u0006\u0002\n\u0000R\u0017\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\u0012RW\u0010\u0013\u001a>\u0012\u0004\u0012\u00020\u0001\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u00040\u0014j\b\u0012\u0004\u0012\u00020\u0004`\u00150\u000ej\u001e\u0012\u0004\u0012\u00020\u0001\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u00040\u0014j\b\u0012\u0004\u0012\u00020\u0004`\u0015`\u00108FX\u0086\u0084\u0002¢\u0006\f\n\u0004\b\u0018\u0010\u0019\u001a\u0004\b\u0016\u0010\u0017R\u0011\u0010\u0005\u001a\u00020\u0006¢\u0006\b\n\u0000\u001a\u0004\b\u001a\u0010\nR\u0017\u0010\u001b\u001a\b\u0012\u0004\u0012\u00020\u00040\u001c8F¢\u0006\u0006\u001a\u0004\b\u001d\u0010\u0012R\u0014\u0010\u001e\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003X\u0082\u0004¢\u0006\u0002\n\u0000¨\u00063" }, d2 = { "Landroidx/compose/runtime/Pending;", "", "keyInfos", "", "Landroidx/compose/runtime/KeyInfo;", "startIndex", "", "(Ljava/util/List;I)V", "groupIndex", "getGroupIndex", "()I", "setGroupIndex", "(I)V", "groupInfos", "Ljava/util/HashMap;", "Landroidx/compose/runtime/GroupInfo;", "Lkotlin/collections/HashMap;", "getKeyInfos", "()Ljava/util/List;", "keyMap", "Ljava/util/LinkedHashSet;", "Lkotlin/collections/LinkedHashSet;", "getKeyMap", "()Ljava/util/HashMap;", "keyMap$delegate", "Lkotlin/Lazy;", "getStartIndex", "used", "", "getUsed", "usedKeys", "getNext", "key", "dataKey", "nodePositionOf", "keyInfo", "recordUsed", "", "registerInsert", "", "insertIndex", "registerMoveNode", "from", "to", "count", "registerMoveSlot", "slotPositionOf", "updateNodeCount", "group", "newCount", "updatedNodeCountOf", "runtime_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
final class Pending
{
    private int groupIndex;
    private final HashMap<Integer, GroupInfo> groupInfos;
    private final List<KeyInfo> keyInfos;
    private final Lazy keyMap$delegate;
    private final int startIndex;
    private final List<KeyInfo> usedKeys;
    
    public Pending(final List<KeyInfo> keyInfos, int i) {
        Intrinsics.checkNotNullParameter((Object)keyInfos, "keyInfos");
        this.keyInfos = keyInfos;
        this.startIndex = i;
        final int n = 0;
        if (i >= 0) {
            i = 1;
        }
        else {
            i = 0;
        }
        if (i != 0) {
            this.usedKeys = new ArrayList<KeyInfo>();
            final Pending pending = this;
            final HashMap<Integer, GroupInfo> groupInfos = new HashMap<Integer, GroupInfo>();
            final int size = this.keyInfos.size();
            final int n2 = 0;
            i = n;
            int n3 = n2;
            while (i < size) {
                final KeyInfo keyInfo = this.keyInfos.get(i);
                groupInfos.put(keyInfo.getLocation(), new GroupInfo(i, n3, keyInfo.getNodes()));
                n3 += keyInfo.getNodes();
                ++i;
            }
            this.groupInfos = groupInfos;
            this.keyMap$delegate = LazyKt.lazy((Function0)new Pending$keyMap.Pending$keyMap$2(this));
            return;
        }
        throw new IllegalArgumentException("Invalid start index".toString());
    }
    
    public final int getGroupIndex() {
        return this.groupIndex;
    }
    
    public final List<KeyInfo> getKeyInfos() {
        return this.keyInfos;
    }
    
    public final HashMap<Object, LinkedHashSet<KeyInfo>> getKeyMap() {
        return (HashMap)this.keyMap$delegate.getValue();
    }
    
    public final KeyInfo getNext(final int n, final Object o) {
        Object value;
        if (o != null) {
            value = new JoinedKey(n, o);
        }
        else {
            value = n;
        }
        return (KeyInfo)ComposerKt.access$pop(this.getKeyMap(), value);
    }
    
    public final int getStartIndex() {
        return this.startIndex;
    }
    
    public final List<KeyInfo> getUsed() {
        return this.usedKeys;
    }
    
    public final int nodePositionOf(final KeyInfo keyInfo) {
        Intrinsics.checkNotNullParameter((Object)keyInfo, "keyInfo");
        final GroupInfo groupInfo = this.groupInfos.get(keyInfo.getLocation());
        int nodeIndex;
        if (groupInfo != null) {
            nodeIndex = groupInfo.getNodeIndex();
        }
        else {
            nodeIndex = -1;
        }
        return nodeIndex;
    }
    
    public final boolean recordUsed(final KeyInfo keyInfo) {
        Intrinsics.checkNotNullParameter((Object)keyInfo, "keyInfo");
        return this.usedKeys.add(keyInfo);
    }
    
    public final void registerInsert(final KeyInfo keyInfo, final int n) {
        Intrinsics.checkNotNullParameter((Object)keyInfo, "keyInfo");
        this.groupInfos.put(keyInfo.getLocation(), new GroupInfo(-1, n, 0));
    }
    
    public final void registerMoveNode(final int n, final int n2, final int n3) {
        if (n > n2) {
            final Collection<GroupInfo> values = this.groupInfos.values();
            Intrinsics.checkNotNullExpressionValue((Object)values, "groupInfos.values");
            for (final GroupInfo groupInfo : values) {
                final int nodeIndex = groupInfo.getNodeIndex();
                if (n <= nodeIndex && nodeIndex < n + n3) {
                    groupInfo.setNodeIndex(nodeIndex - n + n2);
                }
                else {
                    if (n2 > nodeIndex || nodeIndex >= n) {
                        continue;
                    }
                    groupInfo.setNodeIndex(nodeIndex + n3);
                }
            }
        }
        else if (n2 > n) {
            final Collection<GroupInfo> values2 = this.groupInfos.values();
            Intrinsics.checkNotNullExpressionValue((Object)values2, "groupInfos.values");
            for (final GroupInfo groupInfo2 : values2) {
                final int nodeIndex2 = groupInfo2.getNodeIndex();
                if (n <= nodeIndex2 && nodeIndex2 < n + n3) {
                    groupInfo2.setNodeIndex(nodeIndex2 - n + n2);
                }
                else {
                    if (n + 1 > nodeIndex2 || nodeIndex2 >= n2) {
                        continue;
                    }
                    groupInfo2.setNodeIndex(nodeIndex2 - n3);
                }
            }
        }
    }
    
    public final void registerMoveSlot(final int n, final int n2) {
        if (n > n2) {
            final Collection<GroupInfo> values = this.groupInfos.values();
            Intrinsics.checkNotNullExpressionValue((Object)values, "groupInfos.values");
            for (final GroupInfo groupInfo : values) {
                final int slotIndex = groupInfo.getSlotIndex();
                if (slotIndex == n) {
                    groupInfo.setSlotIndex(n2);
                }
                else {
                    if (n2 > slotIndex || slotIndex >= n) {
                        continue;
                    }
                    groupInfo.setSlotIndex(slotIndex + 1);
                }
            }
        }
        else if (n2 > n) {
            final Collection<GroupInfo> values2 = this.groupInfos.values();
            Intrinsics.checkNotNullExpressionValue((Object)values2, "groupInfos.values");
            for (final GroupInfo groupInfo2 : values2) {
                final int slotIndex2 = groupInfo2.getSlotIndex();
                if (slotIndex2 == n) {
                    groupInfo2.setSlotIndex(n2);
                }
                else {
                    if (n + 1 > slotIndex2 || slotIndex2 >= n2) {
                        continue;
                    }
                    groupInfo2.setSlotIndex(slotIndex2 - 1);
                }
            }
        }
    }
    
    public final void setGroupIndex(final int groupIndex) {
        this.groupIndex = groupIndex;
    }
    
    public final int slotPositionOf(final KeyInfo keyInfo) {
        Intrinsics.checkNotNullParameter((Object)keyInfo, "keyInfo");
        final GroupInfo groupInfo = this.groupInfos.get(keyInfo.getLocation());
        int slotIndex;
        if (groupInfo != null) {
            slotIndex = groupInfo.getSlotIndex();
        }
        else {
            slotIndex = -1;
        }
        return slotIndex;
    }
    
    public final boolean updateNodeCount(int nodeIndex, int n) {
        final GroupInfo groupInfo = this.groupInfos.get(nodeIndex);
        if (groupInfo != null) {
            nodeIndex = groupInfo.getNodeIndex();
            final int n2 = n - groupInfo.getNodeCount();
            groupInfo.setNodeCount(n);
            if (n2 != 0) {
                final Collection<GroupInfo> values = this.groupInfos.values();
                Intrinsics.checkNotNullExpressionValue((Object)values, "groupInfos.values");
                for (final GroupInfo groupInfo2 : values) {
                    if (groupInfo2.getNodeIndex() >= nodeIndex && !Intrinsics.areEqual((Object)groupInfo2, (Object)groupInfo)) {
                        n = groupInfo2.getNodeIndex() + n2;
                        if (n < 0) {
                            continue;
                        }
                        groupInfo2.setNodeIndex(n);
                    }
                }
            }
            return true;
        }
        return false;
    }
    
    public final int updatedNodeCountOf(final KeyInfo keyInfo) {
        Intrinsics.checkNotNullParameter((Object)keyInfo, "keyInfo");
        final GroupInfo groupInfo = this.groupInfos.get(keyInfo.getLocation());
        int n;
        if (groupInfo != null) {
            n = groupInfo.getNodeCount();
        }
        else {
            n = keyInfo.getNodes();
        }
        return n;
    }
}

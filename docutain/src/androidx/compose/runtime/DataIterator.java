// 
// Decompiled by Procyon v0.6.0
// 

package androidx.compose.runtime;

import kotlin.jvm.internal.Intrinsics;
import kotlin.Metadata;
import kotlin.jvm.internal.markers.KMappedMarker;
import java.util.Iterator;

@Metadata(d1 = { "\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u001c\n\u0002\u0010\u0000\n\u0002\u0010(\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u000e\n\u0002\u0010\u000b\n\u0002\b\u0003\b\u0002\u0018\u00002\n\u0012\u0006\u0012\u0004\u0018\u00010\u00020\u00012\n\u0012\u0006\u0012\u0004\u0018\u00010\u00020\u0003B\u0015\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007¢\u0006\u0002\u0010\bJ\t\u0010\u0015\u001a\u00020\u0016H\u0096\u0002J\u0011\u0010\u0017\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00020\u0003H\u0096\u0002J\u000b\u0010\u0018\u001a\u0004\u0018\u00010\u0002H\u0096\u0002R\u0011\u0010\t\u001a\u00020\u0007¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000bR\u0011\u0010\u0006\u001a\u00020\u0007¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\u000bR\u001a\u0010\r\u001a\u00020\u0007X\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u000e\u0010\u000b\"\u0004\b\u000f\u0010\u0010R\u0011\u0010\u0011\u001a\u00020\u0007¢\u0006\b\n\u0000\u001a\u0004\b\u0012\u0010\u000bR\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0013\u0010\u0014¨\u0006\u0019" }, d2 = { "Landroidx/compose/runtime/DataIterator;", "", "", "", "table", "Landroidx/compose/runtime/SlotTable;", "group", "", "(Landroidx/compose/runtime/SlotTable;I)V", "end", "getEnd", "()I", "getGroup", "index", "getIndex", "setIndex", "(I)V", "start", "getStart", "getTable", "()Landroidx/compose/runtime/SlotTable;", "hasNext", "", "iterator", "next", "runtime_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
final class DataIterator implements Iterable<Object>, Iterator<Object>, KMappedMarker
{
    private final int end;
    private final int group;
    private int index;
    private final int start;
    private final SlotTable table;
    
    public DataIterator(final SlotTable table, int n) {
        Intrinsics.checkNotNullParameter((Object)table, "table");
        this.table = table;
        this.group = n;
        final int access$dataAnchor = SlotTableKt.access$dataAnchor(table.getGroups(), n);
        this.start = access$dataAnchor;
        if (n + 1 < table.getGroupsSize()) {
            n = SlotTableKt.access$dataAnchor(table.getGroups(), n + 1);
        }
        else {
            n = table.getSlotsSize();
        }
        this.end = n;
        this.index = access$dataAnchor;
    }
    
    public final int getEnd() {
        return this.end;
    }
    
    public final int getGroup() {
        return this.group;
    }
    
    public final int getIndex() {
        return this.index;
    }
    
    public final int getStart() {
        return this.start;
    }
    
    public final SlotTable getTable() {
        return this.table;
    }
    
    @Override
    public boolean hasNext() {
        return this.index < this.end;
    }
    
    @Override
    public Iterator<Object> iterator() {
        return this;
    }
    
    @Override
    public Object next() {
        final int index = this.index;
        Object o;
        if (index >= 0 && index < this.table.getSlots().length) {
            o = this.table.getSlots()[this.index];
        }
        else {
            o = null;
        }
        ++this.index;
        return o;
    }
    
    @Override
    public void remove() {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }
    
    public final void setIndex(final int index) {
        this.index = index;
    }
}

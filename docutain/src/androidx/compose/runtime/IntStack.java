// 
// Decompiled by Procyon v0.6.0
// 

package androidx.compose.runtime;

import kotlin.jvm.internal.Intrinsics;
import java.util.Arrays;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u0015\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\b\b\u0000\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\u0006\u0010\n\u001a\u00020\u000bJ\u000e\u0010\f\u001a\u00020\u00042\u0006\u0010\r\u001a\u00020\u0004J\u0006\u0010\u000e\u001a\u00020\u000fJ\u0006\u0010\u0010\u001a\u00020\u000fJ\u0006\u0010\u0011\u001a\u00020\u0004J\u000e\u0010\u0011\u001a\u00020\u00042\u0006\u0010\u0012\u001a\u00020\u0004J\u000e\u0010\u0013\u001a\u00020\u00042\u0006\u0010\u0014\u001a\u00020\u0004J\u0006\u0010\u0015\u001a\u00020\u0004J\u000e\u0010\u0016\u001a\u00020\u000b2\u0006\u0010\r\u001a\u00020\u0004R\u0011\u0010\u0003\u001a\u00020\u00048F¢\u0006\u0006\u001a\u0004\b\u0005\u0010\u0006R\u000e\u0010\u0007\u001a\u00020\bX\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\u0004X\u0082\u000e¢\u0006\u0002\n\u0000¨\u0006\u0017" }, d2 = { "Landroidx/compose/runtime/IntStack;", "", "()V", "size", "", "getSize", "()I", "slots", "", "tos", "clear", "", "indexOf", "value", "isEmpty", "", "isNotEmpty", "peek", "index", "peekOr", "default", "pop", "push", "runtime_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
public final class IntStack
{
    private int[] slots;
    private int tos;
    
    public IntStack() {
        this.slots = new int[10];
    }
    
    public final void clear() {
        this.tos = 0;
    }
    
    public final int getSize() {
        return this.tos;
    }
    
    public final int indexOf(final int n) {
        for (int tos = this.tos, i = 0; i < tos; ++i) {
            if (this.slots[i] == n) {
                return i;
            }
        }
        return -1;
    }
    
    public final boolean isEmpty() {
        return this.tos == 0;
    }
    
    public final boolean isNotEmpty() {
        return this.tos != 0;
    }
    
    public final int peek() {
        return this.slots[this.tos - 1];
    }
    
    public final int peek(final int n) {
        return this.slots[n];
    }
    
    public final int peekOr(int peek) {
        if (this.tos > 0) {
            peek = this.peek();
        }
        return peek;
    }
    
    public final int pop() {
        final int[] slots = this.slots;
        final int tos = this.tos - 1;
        this.tos = tos;
        return slots[tos];
    }
    
    public final void push(final int n) {
        final int tos = this.tos;
        final int[] slots = this.slots;
        if (tos >= slots.length) {
            final int[] copy = Arrays.copyOf(slots, slots.length * 2);
            Intrinsics.checkNotNullExpressionValue((Object)copy, "copyOf(this, newSize)");
            this.slots = copy;
        }
        this.slots[this.tos++] = n;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.cardview;

public final class R
{
    public static final class attr
    {
        public static final int cardBackgroundColor = 2130968741;
        public static final int cardCornerRadius = 2130968742;
        public static final int cardElevation = 2130968743;
        public static final int cardMaxElevation = 2130968745;
        public static final int cardPreventCornerOverlap = 2130968746;
        public static final int cardUseCompatPadding = 2130968747;
        public static final int cardViewStyle = 2130968748;
        public static final int contentPadding = 2130968913;
        public static final int contentPaddingBottom = 2130968914;
        public static final int contentPaddingLeft = 2130968916;
        public static final int contentPaddingRight = 2130968917;
        public static final int contentPaddingTop = 2130968919;
    }
    
    public static final class color
    {
        public static final int cardview_dark_background = 2131099701;
        public static final int cardview_light_background = 2131099702;
        public static final int cardview_shadow_end_color = 2131099703;
        public static final int cardview_shadow_start_color = 2131099704;
    }
    
    public static final class dimen
    {
        public static final int cardview_compat_inset_shadow = 2131165268;
        public static final int cardview_default_elevation = 2131165269;
        public static final int cardview_default_radius = 2131165270;
    }
    
    public static final class style
    {
        public static final int Base_CardView = 2131951631;
        public static final int CardView = 2131951899;
        public static final int CardView_Dark = 2131951900;
        public static final int CardView_Light = 2131951901;
    }
    
    public static final class styleable
    {
        public static final int[] CardView;
        public static final int CardView_android_minHeight = 1;
        public static final int CardView_android_minWidth = 0;
        public static final int CardView_cardBackgroundColor = 2;
        public static final int CardView_cardCornerRadius = 3;
        public static final int CardView_cardElevation = 4;
        public static final int CardView_cardMaxElevation = 5;
        public static final int CardView_cardPreventCornerOverlap = 6;
        public static final int CardView_cardUseCompatPadding = 7;
        public static final int CardView_contentPadding = 8;
        public static final int CardView_contentPaddingBottom = 9;
        public static final int CardView_contentPaddingLeft = 10;
        public static final int CardView_contentPaddingRight = 11;
        public static final int CardView_contentPaddingTop = 12;
        
        static {
            CardView = new int[] { 16843071, 16843072, 2130968741, 2130968742, 2130968743, 2130968745, 2130968746, 2130968747, 2130968913, 2130968914, 2130968916, 2130968917, 2130968919 };
        }
    }
}

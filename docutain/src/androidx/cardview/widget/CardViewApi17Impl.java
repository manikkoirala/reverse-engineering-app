// 
// Decompiled by Procyon v0.6.0
// 

package androidx.cardview.widget;

import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Canvas;

class CardViewApi17Impl extends CardViewBaseImpl
{
    @Override
    public void initStatic() {
        RoundRectDrawableWithShadow.sRoundRectHelper = (RoundRectDrawableWithShadow.RoundRectHelper)new RoundRectDrawableWithShadow.RoundRectHelper(this) {
            final CardViewApi17Impl this$0;
            
            @Override
            public void drawRoundRect(final Canvas canvas, final RectF rectF, final float n, final Paint paint) {
                canvas.drawRoundRect(rectF, n, n, paint);
            }
        };
    }
}

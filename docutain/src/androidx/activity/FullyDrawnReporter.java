// 
// Decompiled by Procyon v0.6.0
// 

package androidx.activity;

import java.util.Iterator;
import java.util.Collection;
import java.util.ArrayList;
import kotlin.jvm.internal.Intrinsics;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import java.util.List;
import java.util.concurrent.Executor;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000<\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0010!\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\b\u0018\u00002\u00020\u0001B\u001b\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005¢\u0006\u0002\u0010\u0007J\u0014\u0010\u0014\u001a\u00020\u00062\f\u0010\u0015\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005J\u0006\u0010\u0016\u001a\u00020\u0006J\b\u0010\u0017\u001a\u00020\u0006H\u0007J\b\u0010\u0018\u001a\u00020\u0006H\u0002J\u0014\u0010\u0019\u001a\u00020\u00062\f\u0010\u0015\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005J\u0006\u0010\u001a\u001a\u00020\u0006R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004¢\u0006\u0002\n\u0000R\u0011\u0010\b\u001a\u00020\t8F¢\u0006\u0006\u001a\u0004\b\b\u0010\nR\u000e\u0010\u000b\u001a\u00020\u0001X\u0082\u0004¢\u0006\u0002\n\u0000R\u001c\u0010\f\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00060\u00050\r8\u0002X\u0083\u0004¢\u0006\u0002\n\u0000R\u0014\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005X\u0082\u0004¢\u0006\u0002\n\u0000R\u0012\u0010\u000e\u001a\u00020\t8\u0002@\u0002X\u0083\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082\u0004¢\u0006\u0002\n\u0000R\u0012\u0010\u0011\u001a\u00020\t8\u0002@\u0002X\u0083\u000e¢\u0006\u0002\n\u0000R\u0012\u0010\u0012\u001a\u00020\u00138\u0002@\u0002X\u0083\u000e¢\u0006\u0002\n\u0000¨\u0006\u001b" }, d2 = { "Landroidx/activity/FullyDrawnReporter;", "", "executor", "Ljava/util/concurrent/Executor;", "reportFullyDrawn", "Lkotlin/Function0;", "", "(Ljava/util/concurrent/Executor;Lkotlin/jvm/functions/Function0;)V", "isFullyDrawnReported", "", "()Z", "lock", "onReportCallbacks", "", "reportPosted", "reportRunnable", "Ljava/lang/Runnable;", "reportedFullyDrawn", "reporterCount", "", "addOnReportDrawnListener", "callback", "addReporter", "fullyDrawnReported", "postWhenReportersAreDone", "removeOnReportDrawnListener", "removeReporter", "activity_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
public final class FullyDrawnReporter
{
    private final Executor executor;
    private final Object lock;
    private final List<Function0<Unit>> onReportCallbacks;
    private final Function0<Unit> reportFullyDrawn;
    private boolean reportPosted;
    private final Runnable reportRunnable;
    private boolean reportedFullyDrawn;
    private int reporterCount;
    
    public FullyDrawnReporter(final Executor executor, final Function0<Unit> reportFullyDrawn) {
        Intrinsics.checkNotNullParameter((Object)executor, "executor");
        Intrinsics.checkNotNullParameter((Object)reportFullyDrawn, "reportFullyDrawn");
        this.executor = executor;
        this.reportFullyDrawn = reportFullyDrawn;
        this.lock = new Object();
        this.onReportCallbacks = new ArrayList<Function0<Unit>>();
        this.reportRunnable = new FullyDrawnReporter$$ExternalSyntheticLambda0(this);
    }
    
    private final void postWhenReportersAreDone() {
        if (!this.reportPosted && this.reporterCount == 0) {
            this.reportPosted = true;
            this.executor.execute(this.reportRunnable);
        }
    }
    
    private static final void reportRunnable$lambda$2(final FullyDrawnReporter fullyDrawnReporter) {
        Intrinsics.checkNotNullParameter((Object)fullyDrawnReporter, "this$0");
        synchronized (fullyDrawnReporter.lock) {
            fullyDrawnReporter.reportPosted = false;
            if (fullyDrawnReporter.reporterCount == 0 && !fullyDrawnReporter.reportedFullyDrawn) {
                fullyDrawnReporter.reportFullyDrawn.invoke();
                fullyDrawnReporter.fullyDrawnReported();
            }
            final Unit instance = Unit.INSTANCE;
        }
    }
    
    public final void addOnReportDrawnListener(final Function0<Unit> function0) {
        Intrinsics.checkNotNullParameter((Object)function0, "callback");
        synchronized (this.lock) {
            boolean b;
            if (this.reportedFullyDrawn) {
                b = true;
            }
            else {
                this.onReportCallbacks.add(function0);
                b = false;
            }
            monitorexit(this.lock);
            if (b) {
                function0.invoke();
            }
        }
    }
    
    public final void addReporter() {
        synchronized (this.lock) {
            if (!this.reportedFullyDrawn) {
                ++this.reporterCount;
            }
            final Unit instance = Unit.INSTANCE;
        }
    }
    
    public final void fullyDrawnReported() {
        synchronized (this.lock) {
            this.reportedFullyDrawn = true;
            final Iterator iterator = this.onReportCallbacks.iterator();
            while (iterator.hasNext()) {
                ((Function0)iterator.next()).invoke();
            }
            this.onReportCallbacks.clear();
            final Unit instance = Unit.INSTANCE;
        }
    }
    
    public final boolean isFullyDrawnReported() {
        synchronized (this.lock) {
            return this.reportedFullyDrawn;
        }
    }
    
    public final void removeOnReportDrawnListener(final Function0<Unit> function0) {
        Intrinsics.checkNotNullParameter((Object)function0, "callback");
        synchronized (this.lock) {
            this.onReportCallbacks.remove(function0);
            final Unit instance = Unit.INSTANCE;
        }
    }
    
    public final void removeReporter() {
        synchronized (this.lock) {
            if (!this.reportedFullyDrawn) {
                final int reporterCount = this.reporterCount;
                if (reporterCount > 0) {
                    this.reporterCount = reporterCount - 1;
                    this.postWhenReportersAreDone();
                }
            }
            final Unit instance = Unit.INSTANCE;
        }
    }
}

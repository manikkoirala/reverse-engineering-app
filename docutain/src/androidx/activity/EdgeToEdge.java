// 
// Decompiled by Procyon v0.6.0
// 

package androidx.activity;

import android.view.Window;
import android.content.res.Resources;
import kotlin.jvm.functions.Function1;
import android.view.View;
import android.os.Build$VERSION;
import kotlin.jvm.internal.Intrinsics;
import android.graphics.Color;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000\"\n\u0000\n\u0002\u0010\b\n\u0002\b\b\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u001a%\u0010\u000b\u001a\u00020\f*\u00020\r2\b\b\u0002\u0010\u000e\u001a\u00020\u000f2\b\b\u0002\u0010\u0010\u001a\u00020\u000fH\u0007¢\u0006\u0002\b\u0011\"\u001c\u0010\u0000\u001a\u00020\u00018\u0000X\u0081\u0004¢\u0006\u000e\n\u0000\u0012\u0004\b\u0002\u0010\u0003\u001a\u0004\b\u0004\u0010\u0005\"\u001c\u0010\u0006\u001a\u00020\u00018\u0000X\u0081\u0004¢\u0006\u000e\n\u0000\u0012\u0004\b\u0007\u0010\u0003\u001a\u0004\b\b\u0010\u0005\"\u0010\u0010\t\u001a\u0004\u0018\u00010\nX\u0082\u000e¢\u0006\u0002\n\u0000¨\u0006\u0012" }, d2 = { "DefaultDarkScrim", "", "getDefaultDarkScrim$annotations", "()V", "getDefaultDarkScrim", "()I", "DefaultLightScrim", "getDefaultLightScrim$annotations", "getDefaultLightScrim", "Impl", "Landroidx/activity/EdgeToEdgeImpl;", "enableEdgeToEdge", "", "Landroidx/activity/ComponentActivity;", "statusBarStyle", "Landroidx/activity/SystemBarStyle;", "navigationBarStyle", "enable", "activity_release" }, k = 2, mv = { 1, 8, 0 }, xi = 48)
public final class EdgeToEdge
{
    private static final int DefaultDarkScrim;
    private static final int DefaultLightScrim;
    private static EdgeToEdgeImpl Impl;
    
    static {
        DefaultLightScrim = Color.argb(230, 255, 255, 255);
        DefaultDarkScrim = Color.argb(128, 27, 27, 27);
    }
    
    public static final void enable(final ComponentActivity componentActivity) {
        Intrinsics.checkNotNullParameter((Object)componentActivity, "<this>");
        enable$default(componentActivity, null, null, 3, null);
    }
    
    public static final void enable(final ComponentActivity componentActivity, final SystemBarStyle systemBarStyle) {
        Intrinsics.checkNotNullParameter((Object)componentActivity, "<this>");
        Intrinsics.checkNotNullParameter((Object)systemBarStyle, "statusBarStyle");
        enable$default(componentActivity, systemBarStyle, null, 2, null);
    }
    
    public static final void enable(final ComponentActivity componentActivity, final SystemBarStyle systemBarStyle, final SystemBarStyle systemBarStyle2) {
        Intrinsics.checkNotNullParameter((Object)componentActivity, "<this>");
        Intrinsics.checkNotNullParameter((Object)systemBarStyle, "statusBarStyle");
        Intrinsics.checkNotNullParameter((Object)systemBarStyle2, "navigationBarStyle");
        final View decorView = componentActivity.getWindow().getDecorView();
        Intrinsics.checkNotNullExpressionValue((Object)decorView, "window.decorView");
        final Function1<Resources, Boolean> detectDarkMode$activity_release = systemBarStyle.getDetectDarkMode$activity_release();
        final Resources resources = decorView.getResources();
        Intrinsics.checkNotNullExpressionValue((Object)resources, "view.resources");
        final boolean booleanValue = (boolean)detectDarkMode$activity_release.invoke((Object)resources);
        final Function1<Resources, Boolean> detectDarkMode$activity_release2 = systemBarStyle2.getDetectDarkMode$activity_release();
        final Resources resources2 = decorView.getResources();
        Intrinsics.checkNotNullExpressionValue((Object)resources2, "view.resources");
        final boolean booleanValue2 = (boolean)detectDarkMode$activity_release2.invoke((Object)resources2);
        EdgeToEdgeImpl impl;
        if ((impl = EdgeToEdge.Impl) == null) {
            if (Build$VERSION.SDK_INT >= 29) {
                impl = new EdgeToEdgeApi29();
            }
            else if (Build$VERSION.SDK_INT >= 26) {
                impl = new EdgeToEdgeApi26();
            }
            else if (Build$VERSION.SDK_INT >= 23) {
                impl = new EdgeToEdgeApi23();
            }
            else {
                if (Build$VERSION.SDK_INT >= 21) {
                    impl = new EdgeToEdgeApi21();
                }
                else {
                    impl = new EdgeToEdgeBase();
                }
                EdgeToEdge.Impl = impl;
            }
        }
        final Window window = componentActivity.getWindow();
        Intrinsics.checkNotNullExpressionValue((Object)window, "window");
        impl.setUp(systemBarStyle, systemBarStyle2, window, decorView, booleanValue, booleanValue2);
    }
    
    public static /* synthetic */ void enable$default(final ComponentActivity componentActivity, SystemBarStyle auto$default, SystemBarStyle auto$default2, final int n, final Object o) {
        if ((n & 0x1) != 0x0) {
            auto$default = SystemBarStyle.Companion.auto$default(SystemBarStyle.Companion, 0, 0, null, 4, null);
        }
        if ((n & 0x2) != 0x0) {
            auto$default2 = SystemBarStyle.Companion.auto$default(SystemBarStyle.Companion, EdgeToEdge.DefaultLightScrim, EdgeToEdge.DefaultDarkScrim, null, 4, null);
        }
        enable(componentActivity, auto$default, auto$default2);
    }
    
    public static final int getDefaultDarkScrim() {
        return EdgeToEdge.DefaultDarkScrim;
    }
    
    public static final int getDefaultLightScrim() {
        return EdgeToEdge.DefaultLightScrim;
    }
}

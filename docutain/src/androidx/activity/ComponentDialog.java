// 
// Decompiled by Procyon v0.6.0
// 

package androidx.activity;

import android.window.OnBackInvokedDispatcher;
import android.os.Build$VERSION;
import android.os.Bundle;
import android.view.Window;
import androidx.savedstate.ViewTreeSavedStateRegistryOwner;
import androidx.lifecycle.ViewTreeLifecycleOwner;
import androidx.savedstate.SavedStateRegistry;
import androidx.lifecycle.Lifecycle;
import android.view.ViewGroup$LayoutParams;
import android.view.View;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import android.content.Context;
import androidx.savedstate.SavedStateRegistryController;
import androidx.lifecycle.LifecycleRegistry;
import kotlin.Metadata;
import androidx.savedstate.SavedStateRegistryOwner;
import androidx.lifecycle.LifecycleOwner;
import android.app.Dialog;

@Metadata(d1 = { "\u0000d\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0006\b\u0016\u0018\u00002\u00020\u00012\u00020\u00022\u00020\u00032\u00020\u0004B\u0019\b\u0007\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\b\b\u0003\u0010\u0007\u001a\u00020\b¢\u0006\u0002\u0010\tJ\u001a\u0010\u001f\u001a\u00020 2\u0006\u0010!\u001a\u00020\"2\b\u0010#\u001a\u0004\u0018\u00010$H\u0016J\b\u0010%\u001a\u00020 H\u0017J\b\u0010&\u001a\u00020 H\u0017J\u0012\u0010'\u001a\u00020 2\b\u0010(\u001a\u0004\u0018\u00010)H\u0015J\b\u0010*\u001a\u00020)H\u0016J\b\u0010+\u001a\u00020 H\u0015J\b\u0010,\u001a\u00020 H\u0015J\u0010\u0010-\u001a\u00020 2\u0006\u0010!\u001a\u00020\"H\u0016J\u001a\u0010-\u001a\u00020 2\u0006\u0010!\u001a\u00020\"2\b\u0010#\u001a\u0004\u0018\u00010$H\u0016J\u0010\u0010-\u001a\u00020 2\u0006\u0010.\u001a\u00020\bH\u0016R\u0010\u0010\n\u001a\u0004\u0018\u00010\u000bX\u0082\u000e¢\u0006\u0002\n\u0000R\u0014\u0010\f\u001a\u00020\r8VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u000e\u0010\u000fR\u0014\u0010\u0010\u001a\u00020\u000b8BX\u0082\u0004¢\u0006\u0006\u001a\u0004\b\u0011\u0010\u0012R\u0017\u0010\u0013\u001a\u00020\u0014¢\u0006\u000e\n\u0000\u0012\u0004\b\u0015\u0010\u0016\u001a\u0004\b\u0017\u0010\u0018R\u0014\u0010\u0019\u001a\u00020\u001a8VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u001b\u0010\u001cR\u000e\u0010\u001d\u001a\u00020\u001eX\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006/" }, d2 = { "Landroidx/activity/ComponentDialog;", "Landroid/app/Dialog;", "Landroidx/lifecycle/LifecycleOwner;", "Landroidx/activity/OnBackPressedDispatcherOwner;", "Landroidx/savedstate/SavedStateRegistryOwner;", "context", "Landroid/content/Context;", "themeResId", "", "(Landroid/content/Context;I)V", "_lifecycleRegistry", "Landroidx/lifecycle/LifecycleRegistry;", "lifecycle", "Landroidx/lifecycle/Lifecycle;", "getLifecycle", "()Landroidx/lifecycle/Lifecycle;", "lifecycleRegistry", "getLifecycleRegistry", "()Landroidx/lifecycle/LifecycleRegistry;", "onBackPressedDispatcher", "Landroidx/activity/OnBackPressedDispatcher;", "getOnBackPressedDispatcher$annotations", "()V", "getOnBackPressedDispatcher", "()Landroidx/activity/OnBackPressedDispatcher;", "savedStateRegistry", "Landroidx/savedstate/SavedStateRegistry;", "getSavedStateRegistry", "()Landroidx/savedstate/SavedStateRegistry;", "savedStateRegistryController", "Landroidx/savedstate/SavedStateRegistryController;", "addContentView", "", "view", "Landroid/view/View;", "params", "Landroid/view/ViewGroup$LayoutParams;", "initializeViewTreeOwners", "onBackPressed", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onSaveInstanceState", "onStart", "onStop", "setContentView", "layoutResID", "activity_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
public class ComponentDialog extends Dialog implements LifecycleOwner, OnBackPressedDispatcherOwner, SavedStateRegistryOwner
{
    private LifecycleRegistry _lifecycleRegistry;
    private final OnBackPressedDispatcher onBackPressedDispatcher;
    private final SavedStateRegistryController savedStateRegistryController;
    
    public ComponentDialog(final Context context) {
        Intrinsics.checkNotNullParameter((Object)context, "context");
        this(context, 0, 2, null);
    }
    
    public ComponentDialog(final Context context, final int n) {
        Intrinsics.checkNotNullParameter((Object)context, "context");
        super(context, n);
        this.savedStateRegistryController = SavedStateRegistryController.Companion.create(this);
        this.onBackPressedDispatcher = new OnBackPressedDispatcher(new ComponentDialog$$ExternalSyntheticLambda0(this));
    }
    
    private final LifecycleRegistry getLifecycleRegistry() {
        LifecycleRegistry lifecycleRegistry;
        if ((lifecycleRegistry = this._lifecycleRegistry) == null) {
            lifecycleRegistry = new LifecycleRegistry(this);
            this._lifecycleRegistry = lifecycleRegistry;
        }
        return lifecycleRegistry;
    }
    
    private static final void onBackPressedDispatcher$lambda$1(final ComponentDialog componentDialog) {
        Intrinsics.checkNotNullParameter((Object)componentDialog, "this$0");
        componentDialog.onBackPressed();
    }
    
    public void addContentView(final View view, final ViewGroup$LayoutParams viewGroup$LayoutParams) {
        Intrinsics.checkNotNullParameter((Object)view, "view");
        this.initializeViewTreeOwners();
        super.addContentView(view, viewGroup$LayoutParams);
    }
    
    public Lifecycle getLifecycle() {
        return this.getLifecycleRegistry();
    }
    
    public final OnBackPressedDispatcher getOnBackPressedDispatcher() {
        return this.onBackPressedDispatcher;
    }
    
    public SavedStateRegistry getSavedStateRegistry() {
        return this.savedStateRegistryController.getSavedStateRegistry();
    }
    
    public void initializeViewTreeOwners() {
        final Window window = this.getWindow();
        Intrinsics.checkNotNull((Object)window);
        final View decorView = window.getDecorView();
        Intrinsics.checkNotNullExpressionValue((Object)decorView, "window!!.decorView");
        ViewTreeLifecycleOwner.set(decorView, this);
        final Window window2 = this.getWindow();
        Intrinsics.checkNotNull((Object)window2);
        final View decorView2 = window2.getDecorView();
        Intrinsics.checkNotNullExpressionValue((Object)decorView2, "window!!.decorView");
        ViewTreeOnBackPressedDispatcherOwner.set(decorView2, this);
        final Window window3 = this.getWindow();
        Intrinsics.checkNotNull((Object)window3);
        final View decorView3 = window3.getDecorView();
        Intrinsics.checkNotNullExpressionValue((Object)decorView3, "window!!.decorView");
        ViewTreeSavedStateRegistryOwner.set(decorView3, this);
    }
    
    public void onBackPressed() {
        this.onBackPressedDispatcher.onBackPressed();
    }
    
    protected void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        if (Build$VERSION.SDK_INT >= 33) {
            final OnBackPressedDispatcher onBackPressedDispatcher = this.onBackPressedDispatcher;
            final OnBackInvokedDispatcher onBackInvokedDispatcher = this.getOnBackInvokedDispatcher();
            Intrinsics.checkNotNullExpressionValue((Object)onBackInvokedDispatcher, "onBackInvokedDispatcher");
            onBackPressedDispatcher.setOnBackInvokedDispatcher(onBackInvokedDispatcher);
        }
        this.savedStateRegistryController.performRestore(bundle);
        this.getLifecycleRegistry().handleLifecycleEvent(Lifecycle.Event.ON_CREATE);
    }
    
    public Bundle onSaveInstanceState() {
        final Bundle onSaveInstanceState = super.onSaveInstanceState();
        Intrinsics.checkNotNullExpressionValue((Object)onSaveInstanceState, "super.onSaveInstanceState()");
        this.savedStateRegistryController.performSave(onSaveInstanceState);
        return onSaveInstanceState;
    }
    
    protected void onStart() {
        super.onStart();
        this.getLifecycleRegistry().handleLifecycleEvent(Lifecycle.Event.ON_RESUME);
    }
    
    protected void onStop() {
        this.getLifecycleRegistry().handleLifecycleEvent(Lifecycle.Event.ON_DESTROY);
        this._lifecycleRegistry = null;
        super.onStop();
    }
    
    public void setContentView(final int contentView) {
        this.initializeViewTreeOwners();
        super.setContentView(contentView);
    }
    
    public void setContentView(final View contentView) {
        Intrinsics.checkNotNullParameter((Object)contentView, "view");
        this.initializeViewTreeOwners();
        super.setContentView(contentView);
    }
    
    public void setContentView(final View view, final ViewGroup$LayoutParams viewGroup$LayoutParams) {
        Intrinsics.checkNotNullParameter((Object)view, "view");
        this.initializeViewTreeOwners();
        super.setContentView(view, viewGroup$LayoutParams);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.activity;

import androidx.core.view.WindowInsetsControllerCompat;
import androidx.core.view.WindowCompat;
import kotlin.jvm.internal.Intrinsics;
import android.view.View;
import android.view.Window;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J8\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u00062\u0006\u0010\b\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000b2\u0006\u0010\f\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\rH\u0017¨\u0006\u000f" }, d2 = { "Landroidx/activity/EdgeToEdgeApi29;", "Landroidx/activity/EdgeToEdgeImpl;", "()V", "setUp", "", "statusBarStyle", "Landroidx/activity/SystemBarStyle;", "navigationBarStyle", "window", "Landroid/view/Window;", "view", "Landroid/view/View;", "statusBarIsDark", "", "navigationBarIsDark", "activity_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
final class EdgeToEdgeApi29 implements EdgeToEdgeImpl
{
    public EdgeToEdgeApi29() {
    }
    
    @Override
    public void setUp(final SystemBarStyle systemBarStyle, final SystemBarStyle systemBarStyle2, final Window window, final View view, final boolean b, final boolean b2) {
        Intrinsics.checkNotNullParameter((Object)systemBarStyle, "statusBarStyle");
        Intrinsics.checkNotNullParameter((Object)systemBarStyle2, "navigationBarStyle");
        Intrinsics.checkNotNullParameter((Object)window, "window");
        Intrinsics.checkNotNullParameter((Object)view, "view");
        boolean navigationBarContrastEnforced = false;
        WindowCompat.setDecorFitsSystemWindows(window, false);
        window.setStatusBarColor(systemBarStyle.getScrimWithEnforcedContrast$activity_release(b));
        window.setNavigationBarColor(systemBarStyle2.getScrimWithEnforcedContrast$activity_release(b2));
        window.setStatusBarContrastEnforced(false);
        if (systemBarStyle2.getNightMode$activity_release() == 0) {
            navigationBarContrastEnforced = true;
        }
        window.setNavigationBarContrastEnforced(navigationBarContrastEnforced);
        final WindowInsetsControllerCompat windowInsetsControllerCompat = new WindowInsetsControllerCompat(window, view);
        windowInsetsControllerCompat.setAppearanceLightStatusBars(b ^ true);
        windowInsetsControllerCompat.setAppearanceLightNavigationBars(true ^ b2);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.activity;

import kotlin.jvm.internal.InlineMarker;
import kotlin.ResultKt;
import kotlin.coroutines.intrinsics.IntrinsicsKt;
import kotlin.Unit;
import kotlin.coroutines.Continuation;
import kotlin.jvm.functions.Function1;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000\u001c\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\u001a3\u0010\u0000\u001a\u00020\u0001*\u00020\u00022\u001c\u0010\u0003\u001a\u0018\b\u0001\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00010\u0005\u0012\u0006\u0012\u0004\u0018\u00010\u00060\u0004H\u0086H\u00f8\u0001\u0000¢\u0006\u0002\u0010\u0007\u0082\u0002\u0004\n\u0002\b\u0019¨\u0006\b" }, d2 = { "reportWhenComplete", "", "Landroidx/activity/FullyDrawnReporter;", "reporter", "Lkotlin/Function1;", "Lkotlin/coroutines/Continuation;", "", "(Landroidx/activity/FullyDrawnReporter;Lkotlin/jvm/functions/Function1;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "activity_release" }, k = 2, mv = { 1, 8, 0 }, xi = 48)
public final class FullyDrawnReporterKt
{
    public static final Object reportWhenComplete(FullyDrawnReporter l$0, final Function1<? super Continuation<? super Unit>, ?> function1, Continuation<? super Unit> fullyDrawnReporter) {
        FullyDrawnReporterKt$reportWhenComplete.FullyDrawnReporterKt$reportWhenComplete$1 fullyDrawnReporterKt$reportWhenComplete$1 = null;
        Label_0050: {
            if (fullyDrawnReporter instanceof FullyDrawnReporterKt$reportWhenComplete.FullyDrawnReporterKt$reportWhenComplete$1) {
                fullyDrawnReporterKt$reportWhenComplete$1 = (FullyDrawnReporterKt$reportWhenComplete.FullyDrawnReporterKt$reportWhenComplete$1)fullyDrawnReporter;
                if ((fullyDrawnReporterKt$reportWhenComplete$1.label & Integer.MIN_VALUE) != 0x0) {
                    fullyDrawnReporterKt$reportWhenComplete$1.label += Integer.MIN_VALUE;
                    break Label_0050;
                }
            }
            fullyDrawnReporterKt$reportWhenComplete$1 = new FullyDrawnReporterKt$reportWhenComplete.FullyDrawnReporterKt$reportWhenComplete$1((Continuation)fullyDrawnReporter);
        }
        final Object result = fullyDrawnReporterKt$reportWhenComplete$1.result;
        final Object coroutine_SUSPENDED = IntrinsicsKt.getCOROUTINE_SUSPENDED();
        final int label = fullyDrawnReporterKt$reportWhenComplete$1.label;
        Label_0106: {
            if (label == 0) {
                break Label_0106;
            }
            Label_0096: {
                if (label != 1) {
                    break Label_0096;
                }
                l$0 = (fullyDrawnReporter = (FullyDrawnReporter)fullyDrawnReporterKt$reportWhenComplete$1.L$0);
                try {
                    ResultKt.throwOnFailure(result);
                    Label_0162: {
                        return Unit.INSTANCE;
                    }
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    ResultKt.throwOnFailure(result);
                    l$0.addReporter();
                    iftrue(Label_0126:)(!l$0.isFullyDrawnReported());
                    return Unit.INSTANCE;
                    Label_0126:
                    fullyDrawnReporter = l$0;
                    fullyDrawnReporterKt$reportWhenComplete$1.L$0 = l$0;
                    fullyDrawnReporter = l$0;
                    fullyDrawnReporterKt$reportWhenComplete$1.label = 1;
                    fullyDrawnReporter = l$0;
                    iftrue(Label_0162:)(function1.invoke((Object)fullyDrawnReporterKt$reportWhenComplete$1) != coroutine_SUSPENDED);
                    return coroutine_SUSPENDED;
                }
                finally {
                    InlineMarker.finallyStart(1);
                    fullyDrawnReporter.removeReporter();
                    InlineMarker.finallyEnd(1);
                }
            }
        }
    }
    
    private static final Object reportWhenComplete$$forInline(final FullyDrawnReporter fullyDrawnReporter, final Function1<? super Continuation<? super Unit>, ?> function1, final Continuation<? super Unit> continuation) {
        fullyDrawnReporter.addReporter();
        if (fullyDrawnReporter.isFullyDrawnReported()) {
            return Unit.INSTANCE;
        }
        try {
            function1.invoke((Object)continuation);
            return Unit.INSTANCE;
        }
        finally {
            InlineMarker.finallyStart(1);
            fullyDrawnReporter.removeReporter();
            InlineMarker.finallyEnd(1);
        }
    }
}

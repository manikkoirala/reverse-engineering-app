// 
// Decompiled by Procyon v0.6.0
// 

package androidx.activity;

import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.JvmStatic;
import kotlin.jvm.internal.DefaultConstructorMarker;
import android.content.res.Resources;
import kotlin.jvm.functions.Function1;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\b\r\u0018\u0000 \u00152\u00020\u0001:\u0001\u0015B3\b\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0003\u0012\u0012\u0010\u0006\u001a\u000e\u0012\u0004\u0012\u00020\b\u0012\u0004\u0012\u00020\t0\u0007¢\u0006\u0002\u0010\nJ\u0015\u0010\u0010\u001a\u00020\u00032\u0006\u0010\u0011\u001a\u00020\tH\u0000¢\u0006\u0002\b\u0012J\u0015\u0010\u0013\u001a\u00020\u00032\u0006\u0010\u0011\u001a\u00020\tH\u0000¢\u0006\u0002\b\u0014R\u0014\u0010\u0004\u001a\u00020\u0003X\u0080\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR \u0010\u0006\u001a\u000e\u0012\u0004\u0012\u00020\b\u0012\u0004\u0012\u00020\t0\u0007X\u0080\u0004¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000eR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004¢\u0006\u0002\n\u0000R\u0014\u0010\u0005\u001a\u00020\u0003X\u0080\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\f¨\u0006\u0016" }, d2 = { "Landroidx/activity/SystemBarStyle;", "", "lightScrim", "", "darkScrim", "nightMode", "detectDarkMode", "Lkotlin/Function1;", "Landroid/content/res/Resources;", "", "(IIILkotlin/jvm/functions/Function1;)V", "getDarkScrim$activity_release", "()I", "getDetectDarkMode$activity_release", "()Lkotlin/jvm/functions/Function1;", "getNightMode$activity_release", "getScrim", "isDark", "getScrim$activity_release", "getScrimWithEnforcedContrast", "getScrimWithEnforcedContrast$activity_release", "Companion", "activity_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
public final class SystemBarStyle
{
    public static final Companion Companion;
    private final int darkScrim;
    private final Function1<Resources, Boolean> detectDarkMode;
    private final int lightScrim;
    private final int nightMode;
    
    static {
        Companion = new Companion(null);
    }
    
    private SystemBarStyle(final int lightScrim, final int darkScrim, final int nightMode, final Function1<? super Resources, Boolean> detectDarkMode) {
        this.lightScrim = lightScrim;
        this.darkScrim = darkScrim;
        this.nightMode = nightMode;
        this.detectDarkMode = (Function1<Resources, Boolean>)detectDarkMode;
    }
    
    @JvmStatic
    public static final SystemBarStyle auto(final int n, final int n2) {
        return SystemBarStyle.Companion.auto(n, n2);
    }
    
    @JvmStatic
    public static final SystemBarStyle auto(final int n, final int n2, final Function1<? super Resources, Boolean> function1) {
        return SystemBarStyle.Companion.auto(n, n2, function1);
    }
    
    @JvmStatic
    public static final SystemBarStyle dark(final int n) {
        return SystemBarStyle.Companion.dark(n);
    }
    
    @JvmStatic
    public static final SystemBarStyle light(final int n, final int n2) {
        return SystemBarStyle.Companion.light(n, n2);
    }
    
    public final int getDarkScrim$activity_release() {
        return this.darkScrim;
    }
    
    public final Function1<Resources, Boolean> getDetectDarkMode$activity_release() {
        return this.detectDarkMode;
    }
    
    public final int getNightMode$activity_release() {
        return this.nightMode;
    }
    
    public final int getScrim$activity_release(final boolean b) {
        int n;
        if (b) {
            n = this.darkScrim;
        }
        else {
            n = this.lightScrim;
        }
        return n;
    }
    
    public final int getScrimWithEnforcedContrast$activity_release(final boolean b) {
        int n;
        if (this.nightMode == 0) {
            n = 0;
        }
        else if (b) {
            n = this.darkScrim;
        }
        else {
            n = this.lightScrim;
        }
        return n;
    }
    
    @Metadata(d1 = { "\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\b\u0004\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J2\u0010\u0003\u001a\u00020\u00042\b\b\u0001\u0010\u0005\u001a\u00020\u00062\b\b\u0001\u0010\u0007\u001a\u00020\u00062\u0014\b\u0002\u0010\b\u001a\u000e\u0012\u0004\u0012\u00020\n\u0012\u0004\u0012\u00020\u000b0\tH\u0007J\u0012\u0010\f\u001a\u00020\u00042\b\b\u0001\u0010\r\u001a\u00020\u0006H\u0007J\u001c\u0010\u000e\u001a\u00020\u00042\b\b\u0001\u0010\r\u001a\u00020\u00062\b\b\u0001\u0010\u0007\u001a\u00020\u0006H\u0007¨\u0006\u000f" }, d2 = { "Landroidx/activity/SystemBarStyle$Companion;", "", "()V", "auto", "Landroidx/activity/SystemBarStyle;", "lightScrim", "", "darkScrim", "detectDarkMode", "Lkotlin/Function1;", "Landroid/content/res/Resources;", "", "dark", "scrim", "light", "activity_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
    public static final class Companion
    {
        private Companion() {
        }
        
        public static /* synthetic */ SystemBarStyle auto$default(final Companion companion, final int n, final int n2, Function1 function1, final int n3, final Object o) {
            if ((n3 & 0x4) != 0x0) {
                function1 = (Function1)SystemBarStyle$Companion$auto.SystemBarStyle$Companion$auto$1.INSTANCE;
            }
            return companion.auto(n, n2, (Function1<? super Resources, Boolean>)function1);
        }
        
        @JvmStatic
        public final SystemBarStyle auto(final int n, final int n2) {
            return auto$default(this, n, n2, null, 4, null);
        }
        
        @JvmStatic
        public final SystemBarStyle auto(final int n, final int n2, final Function1<? super Resources, Boolean> function1) {
            Intrinsics.checkNotNullParameter((Object)function1, "detectDarkMode");
            return new SystemBarStyle(n, n2, 0, function1, null);
        }
        
        @JvmStatic
        public final SystemBarStyle dark(final int n) {
            return new SystemBarStyle(n, n, 2, (Function1)SystemBarStyle$Companion$dark.SystemBarStyle$Companion$dark$1.INSTANCE, null);
        }
        
        @JvmStatic
        public final SystemBarStyle light(final int n, final int n2) {
            return new SystemBarStyle(n, n2, 1, (Function1)SystemBarStyle$Companion$light.SystemBarStyle$Companion$light$1.INSTANCE, null);
        }
    }
}

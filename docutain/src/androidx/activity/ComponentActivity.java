// 
// Decompiled by Procyon v0.6.0
// 

package androidx.activity;

import android.os.SystemClock;
import android.view.ViewTreeObserver$OnDrawListener;
import android.window.OnBackInvokedDispatcher;
import android.content.IntentSender;
import androidx.core.content.ContextCompat;
import androidx.tracing.Trace;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.ActivityResultCallback;
import android.view.MenuItem;
import android.view.Menu;
import androidx.lifecycle.ReportFragment;
import java.util.Iterator;
import androidx.savedstate.ViewTreeSavedStateRegistryOwner;
import androidx.lifecycle.ViewTreeViewModelStoreOwner;
import androidx.lifecycle.ViewTreeLifecycleOwner;
import android.text.TextUtils;
import android.app.Application;
import androidx.lifecycle.SavedStateViewModelFactory;
import androidx.lifecycle.viewmodel.MutableCreationExtras;
import androidx.lifecycle.viewmodel.CreationExtras;
import androidx.core.view.MenuProvider;
import android.view.ViewGroup$LayoutParams;
import androidx.activity.contextaware.OnContextAvailableListener;
import androidx.savedstate.SavedStateRegistry;
import androidx.lifecycle.SavedStateHandleSupport;
import androidx.lifecycle.LifecycleObserver;
import android.view.View;
import android.view.Window;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleEventObserver;
import android.os.Build$VERSION;
import android.os.Bundle;
import java.io.Serializable;
import android.content.IntentSender$SendIntentException;
import androidx.activity.result.IntentSenderRequest;
import android.app.Activity;
import androidx.core.app.ActivityCompat;
import android.os.Handler;
import android.os.Looper;
import android.content.Context;
import androidx.core.app.ActivityOptionsCompat;
import androidx.activity.result.contract.ActivityResultContract;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import java.util.concurrent.Executor;
import androidx.lifecycle.ViewModelStore;
import androidx.savedstate.SavedStateRegistryController;
import androidx.core.app.PictureInPictureModeChangedInfo;
import android.content.Intent;
import androidx.core.app.MultiWindowModeChangedInfo;
import android.content.res.Configuration;
import androidx.core.util.Consumer;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.atomic.AtomicInteger;
import androidx.core.view.MenuHostHelper;
import androidx.lifecycle.LifecycleRegistry;
import androidx.lifecycle.ViewModelProvider;
import androidx.activity.contextaware.ContextAwareHelper;
import androidx.activity.result.ActivityResultRegistry;
import androidx.core.view.MenuHost;
import androidx.core.app.OnPictureInPictureModeChangedProvider;
import androidx.core.app.OnMultiWindowModeChangedProvider;
import androidx.core.app.OnNewIntentProvider;
import androidx.core.content.OnTrimMemoryProvider;
import androidx.core.content.OnConfigurationChangedProvider;
import androidx.activity.result.ActivityResultCaller;
import androidx.activity.result.ActivityResultRegistryOwner;
import androidx.savedstate.SavedStateRegistryOwner;
import androidx.lifecycle.HasDefaultViewModelProviderFactory;
import androidx.lifecycle.ViewModelStoreOwner;
import androidx.lifecycle.LifecycleOwner;
import androidx.activity.contextaware.ContextAware;

public class ComponentActivity extends androidx.core.app.ComponentActivity implements ContextAware, LifecycleOwner, ViewModelStoreOwner, HasDefaultViewModelProviderFactory, SavedStateRegistryOwner, OnBackPressedDispatcherOwner, ActivityResultRegistryOwner, ActivityResultCaller, OnConfigurationChangedProvider, OnTrimMemoryProvider, OnNewIntentProvider, OnMultiWindowModeChangedProvider, OnPictureInPictureModeChangedProvider, MenuHost, FullyDrawnReporterOwner
{
    private static final String ACTIVITY_RESULT_TAG = "android:support:activity-result";
    private final ActivityResultRegistry mActivityResultRegistry;
    private int mContentLayoutId;
    final ContextAwareHelper mContextAwareHelper;
    private ViewModelProvider.Factory mDefaultFactory;
    private boolean mDispatchingOnMultiWindowModeChanged;
    private boolean mDispatchingOnPictureInPictureModeChanged;
    final FullyDrawnReporter mFullyDrawnReporter;
    private final LifecycleRegistry mLifecycleRegistry;
    private final MenuHostHelper mMenuHostHelper;
    private final AtomicInteger mNextLocalRequestCode;
    private OnBackPressedDispatcher mOnBackPressedDispatcher;
    private final CopyOnWriteArrayList<Consumer<Configuration>> mOnConfigurationChangedListeners;
    private final CopyOnWriteArrayList<Consumer<MultiWindowModeChangedInfo>> mOnMultiWindowModeChangedListeners;
    private final CopyOnWriteArrayList<Consumer<Intent>> mOnNewIntentListeners;
    private final CopyOnWriteArrayList<Consumer<PictureInPictureModeChangedInfo>> mOnPictureInPictureModeChangedListeners;
    private final CopyOnWriteArrayList<Consumer<Integer>> mOnTrimMemoryListeners;
    final ReportFullyDrawnExecutor mReportFullyDrawnExecutor;
    final SavedStateRegistryController mSavedStateRegistryController;
    private ViewModelStore mViewModelStore;
    
    public ComponentActivity() {
        this.mContextAwareHelper = new ContextAwareHelper();
        this.mMenuHostHelper = new MenuHostHelper(new ComponentActivity$$ExternalSyntheticLambda0(this));
        this.mLifecycleRegistry = new LifecycleRegistry(this);
        final SavedStateRegistryController create = SavedStateRegistryController.create(this);
        this.mSavedStateRegistryController = create;
        this.mOnBackPressedDispatcher = null;
        final ReportFullyDrawnExecutor fullyDrawnExecutor = this.createFullyDrawnExecutor();
        this.mReportFullyDrawnExecutor = fullyDrawnExecutor;
        this.mFullyDrawnReporter = new FullyDrawnReporter(fullyDrawnExecutor, (Function0<Unit>)new ComponentActivity$$ExternalSyntheticLambda1(this));
        this.mNextLocalRequestCode = new AtomicInteger();
        this.mActivityResultRegistry = new ActivityResultRegistry() {
            final ComponentActivity this$0;
            
            @Override
            public <I, O> void onLaunch(final int n, final ActivityResultContract<I, O> activityResultContract, final I n2, final ActivityOptionsCompat activityOptionsCompat) {
                final ComponentActivity this$0 = this.this$0;
                final ActivityResultContract.SynchronousResult<O> synchronousResult = activityResultContract.getSynchronousResult((Context)this$0, n2);
                if (synchronousResult != null) {
                    new Handler(Looper.getMainLooper()).post((Runnable)new Runnable(this, n, synchronousResult) {
                        final ComponentActivity$1 this$1;
                        final int val$requestCode;
                        final ActivityResultContract.SynchronousResult val$synchronousResult;
                        
                        @Override
                        public void run() {
                            this.this$1.dispatchResult(this.val$requestCode, this.val$synchronousResult.getValue());
                        }
                    });
                    return;
                }
                final Intent intent = activityResultContract.createIntent((Context)this$0, n2);
                Bundle bundle = null;
                if (intent.getExtras() != null && intent.getExtras().getClassLoader() == null) {
                    intent.setExtrasClassLoader(this$0.getClassLoader());
                }
                if (intent.hasExtra("androidx.activity.result.contract.extra.ACTIVITY_OPTIONS_BUNDLE")) {
                    bundle = intent.getBundleExtra("androidx.activity.result.contract.extra.ACTIVITY_OPTIONS_BUNDLE");
                    intent.removeExtra("androidx.activity.result.contract.extra.ACTIVITY_OPTIONS_BUNDLE");
                }
                else if (activityOptionsCompat != null) {
                    bundle = activityOptionsCompat.toBundle();
                }
                if ("androidx.activity.result.contract.action.REQUEST_PERMISSIONS".equals(intent.getAction())) {
                    String[] stringArrayExtra;
                    if ((stringArrayExtra = intent.getStringArrayExtra("androidx.activity.result.contract.extra.PERMISSIONS")) == null) {
                        stringArrayExtra = new String[0];
                    }
                    ActivityCompat.requestPermissions(this$0, stringArrayExtra, n);
                }
                else if ("androidx.activity.result.contract.action.INTENT_SENDER_REQUEST".equals(intent.getAction())) {
                    final IntentSenderRequest intentSenderRequest = (IntentSenderRequest)intent.getParcelableExtra("androidx.activity.result.contract.extra.INTENT_SENDER_REQUEST");
                    try {
                        ActivityCompat.startIntentSenderForResult(this$0, intentSenderRequest.getIntentSender(), n, intentSenderRequest.getFillInIntent(), intentSenderRequest.getFlagsMask(), intentSenderRequest.getFlagsValues(), 0, bundle);
                    }
                    catch (final IntentSender$SendIntentException ex) {
                        new Handler(Looper.getMainLooper()).post((Runnable)new Runnable(this, n, ex) {
                            final ComponentActivity$1 this$1;
                            final IntentSender$SendIntentException val$e;
                            final int val$requestCode;
                            
                            @Override
                            public void run() {
                                this.this$1.dispatchResult(this.val$requestCode, 0, new Intent().setAction("androidx.activity.result.contract.action.INTENT_SENDER_REQUEST").putExtra("androidx.activity.result.contract.extra.SEND_INTENT_EXCEPTION", (Serializable)this.val$e));
                            }
                        });
                    }
                }
                else {
                    ActivityCompat.startActivityForResult(this$0, intent, n, bundle);
                }
            }
        };
        this.mOnConfigurationChangedListeners = new CopyOnWriteArrayList<Consumer<Configuration>>();
        this.mOnTrimMemoryListeners = new CopyOnWriteArrayList<Consumer<Integer>>();
        this.mOnNewIntentListeners = new CopyOnWriteArrayList<Consumer<Intent>>();
        this.mOnMultiWindowModeChangedListeners = new CopyOnWriteArrayList<Consumer<MultiWindowModeChangedInfo>>();
        this.mOnPictureInPictureModeChangedListeners = new CopyOnWriteArrayList<Consumer<PictureInPictureModeChangedInfo>>();
        this.mDispatchingOnMultiWindowModeChanged = false;
        this.mDispatchingOnPictureInPictureModeChanged = false;
        if (this.getLifecycle() != null) {
            if (Build$VERSION.SDK_INT >= 19) {
                this.getLifecycle().addObserver(new LifecycleEventObserver(this) {
                    final ComponentActivity this$0;
                    
                    @Override
                    public void onStateChanged(final LifecycleOwner lifecycleOwner, final Lifecycle.Event event) {
                        if (event == Lifecycle.Event.ON_STOP) {
                            final Window window = this.this$0.getWindow();
                            View peekDecorView;
                            if (window != null) {
                                peekDecorView = window.peekDecorView();
                            }
                            else {
                                peekDecorView = null;
                            }
                            if (peekDecorView != null) {
                                Api19Impl.cancelPendingInputEvents(peekDecorView);
                            }
                        }
                    }
                });
            }
            this.getLifecycle().addObserver(new LifecycleEventObserver(this) {
                final ComponentActivity this$0;
                
                @Override
                public void onStateChanged(final LifecycleOwner lifecycleOwner, final Lifecycle.Event event) {
                    if (event == Lifecycle.Event.ON_DESTROY) {
                        this.this$0.mContextAwareHelper.clearAvailableContext();
                        if (!this.this$0.isChangingConfigurations()) {
                            this.this$0.getViewModelStore().clear();
                        }
                        this.this$0.mReportFullyDrawnExecutor.activityDestroyed();
                    }
                }
            });
            this.getLifecycle().addObserver(new LifecycleEventObserver(this) {
                final ComponentActivity this$0;
                
                @Override
                public void onStateChanged(final LifecycleOwner lifecycleOwner, final Lifecycle.Event event) {
                    this.this$0.ensureViewModelStore();
                    this.this$0.getLifecycle().removeObserver(this);
                }
            });
            create.performAttach();
            SavedStateHandleSupport.enableSavedStateHandles(this);
            if (19 <= Build$VERSION.SDK_INT && Build$VERSION.SDK_INT <= 23) {
                this.getLifecycle().addObserver(new ImmLeaksCleaner(this));
            }
            this.getSavedStateRegistry().registerSavedStateProvider("android:support:activity-result", (SavedStateRegistry.SavedStateProvider)new ComponentActivity$$ExternalSyntheticLambda2(this));
            this.addOnContextAvailableListener(new ComponentActivity$$ExternalSyntheticLambda3(this));
            return;
        }
        throw new IllegalStateException("getLifecycle() returned null in ComponentActivity's constructor. Please make sure you are lazily constructing your Lifecycle in the first call to getLifecycle() rather than relying on field initialization.");
    }
    
    public ComponentActivity(final int mContentLayoutId) {
        this();
        this.mContentLayoutId = mContentLayoutId;
    }
    
    static /* synthetic */ void access$001(final ComponentActivity componentActivity) {
        componentActivity.onBackPressed();
    }
    
    private ReportFullyDrawnExecutor createFullyDrawnExecutor() {
        if (Build$VERSION.SDK_INT < 16) {
            return (ReportFullyDrawnExecutor)new ReportFullyDrawnExecutorApi1();
        }
        return (ReportFullyDrawnExecutor)new ReportFullyDrawnExecutorApi16Impl();
    }
    
    public void addContentView(final View view, final ViewGroup$LayoutParams viewGroup$LayoutParams) {
        this.initializeViewTreeOwners();
        this.mReportFullyDrawnExecutor.viewCreated(this.getWindow().getDecorView());
        super.addContentView(view, viewGroup$LayoutParams);
    }
    
    @Override
    public void addMenuProvider(final MenuProvider menuProvider) {
        this.mMenuHostHelper.addMenuProvider(menuProvider);
    }
    
    @Override
    public void addMenuProvider(final MenuProvider menuProvider, final LifecycleOwner lifecycleOwner) {
        this.mMenuHostHelper.addMenuProvider(menuProvider, lifecycleOwner);
    }
    
    @Override
    public void addMenuProvider(final MenuProvider menuProvider, final LifecycleOwner lifecycleOwner, final Lifecycle.State state) {
        this.mMenuHostHelper.addMenuProvider(menuProvider, lifecycleOwner, state);
    }
    
    @Override
    public final void addOnConfigurationChangedListener(final Consumer<Configuration> e) {
        this.mOnConfigurationChangedListeners.add(e);
    }
    
    @Override
    public final void addOnContextAvailableListener(final OnContextAvailableListener onContextAvailableListener) {
        this.mContextAwareHelper.addOnContextAvailableListener(onContextAvailableListener);
    }
    
    @Override
    public final void addOnMultiWindowModeChangedListener(final Consumer<MultiWindowModeChangedInfo> e) {
        this.mOnMultiWindowModeChangedListeners.add(e);
    }
    
    @Override
    public final void addOnNewIntentListener(final Consumer<Intent> e) {
        this.mOnNewIntentListeners.add(e);
    }
    
    @Override
    public final void addOnPictureInPictureModeChangedListener(final Consumer<PictureInPictureModeChangedInfo> e) {
        this.mOnPictureInPictureModeChangedListeners.add(e);
    }
    
    @Override
    public final void addOnTrimMemoryListener(final Consumer<Integer> e) {
        this.mOnTrimMemoryListeners.add(e);
    }
    
    void ensureViewModelStore() {
        if (this.mViewModelStore == null) {
            final NonConfigurationInstances nonConfigurationInstances = (NonConfigurationInstances)this.getLastNonConfigurationInstance();
            if (nonConfigurationInstances != null) {
                this.mViewModelStore = nonConfigurationInstances.viewModelStore;
            }
            if (this.mViewModelStore == null) {
                this.mViewModelStore = new ViewModelStore();
            }
        }
    }
    
    @Override
    public final ActivityResultRegistry getActivityResultRegistry() {
        return this.mActivityResultRegistry;
    }
    
    @Override
    public CreationExtras getDefaultViewModelCreationExtras() {
        final MutableCreationExtras mutableCreationExtras = new MutableCreationExtras();
        if (this.getApplication() != null) {
            mutableCreationExtras.set(ViewModelProvider.AndroidViewModelFactory.APPLICATION_KEY, this.getApplication());
        }
        mutableCreationExtras.set(SavedStateHandleSupport.SAVED_STATE_REGISTRY_OWNER_KEY, this);
        mutableCreationExtras.set(SavedStateHandleSupport.VIEW_MODEL_STORE_OWNER_KEY, this);
        if (this.getIntent() != null && this.getIntent().getExtras() != null) {
            mutableCreationExtras.set(SavedStateHandleSupport.DEFAULT_ARGS_KEY, this.getIntent().getExtras());
        }
        return mutableCreationExtras;
    }
    
    @Override
    public ViewModelProvider.Factory getDefaultViewModelProviderFactory() {
        if (this.mDefaultFactory == null) {
            final Application application = this.getApplication();
            Bundle extras;
            if (this.getIntent() != null) {
                extras = this.getIntent().getExtras();
            }
            else {
                extras = null;
            }
            this.mDefaultFactory = new SavedStateViewModelFactory(application, this, extras);
        }
        return this.mDefaultFactory;
    }
    
    @Override
    public FullyDrawnReporter getFullyDrawnReporter() {
        return this.mFullyDrawnReporter;
    }
    
    @Deprecated
    public Object getLastCustomNonConfigurationInstance() {
        final NonConfigurationInstances nonConfigurationInstances = (NonConfigurationInstances)this.getLastNonConfigurationInstance();
        Object custom;
        if (nonConfigurationInstances != null) {
            custom = nonConfigurationInstances.custom;
        }
        else {
            custom = null;
        }
        return custom;
    }
    
    @Override
    public Lifecycle getLifecycle() {
        return this.mLifecycleRegistry;
    }
    
    @Override
    public final OnBackPressedDispatcher getOnBackPressedDispatcher() {
        if (this.mOnBackPressedDispatcher == null) {
            this.mOnBackPressedDispatcher = new OnBackPressedDispatcher(new Runnable(this) {
                final ComponentActivity this$0;
                
                @Override
                public void run() {
                    try {
                        ComponentActivity.access$001(this.this$0);
                    }
                    catch (final NullPointerException ex) {
                        if (!TextUtils.equals((CharSequence)ex.getMessage(), (CharSequence)"Attempt to invoke virtual method 'android.os.Handler android.app.FragmentHostCallback.getHandler()' on a null object reference")) {
                            throw ex;
                        }
                    }
                    catch (final IllegalStateException ex2) {
                        if (!TextUtils.equals((CharSequence)ex2.getMessage(), (CharSequence)"Can not perform this action after onSaveInstanceState")) {
                            throw ex2;
                        }
                    }
                }
            });
            this.getLifecycle().addObserver(new LifecycleEventObserver(this) {
                final ComponentActivity this$0;
                
                @Override
                public void onStateChanged(final LifecycleOwner lifecycleOwner, final Lifecycle.Event event) {
                    if (event == Lifecycle.Event.ON_CREATE && Build$VERSION.SDK_INT >= 33) {
                        this.this$0.mOnBackPressedDispatcher.setOnBackInvokedDispatcher(Api33Impl.getOnBackInvokedDispatcher((Activity)lifecycleOwner));
                    }
                }
            });
        }
        return this.mOnBackPressedDispatcher;
    }
    
    @Override
    public final SavedStateRegistry getSavedStateRegistry() {
        return this.mSavedStateRegistryController.getSavedStateRegistry();
    }
    
    @Override
    public ViewModelStore getViewModelStore() {
        if (this.getApplication() != null) {
            this.ensureViewModelStore();
            return this.mViewModelStore;
        }
        throw new IllegalStateException("Your activity is not yet attached to the Application instance. You can't request ViewModel before onCreate call.");
    }
    
    public void initializeViewTreeOwners() {
        ViewTreeLifecycleOwner.set(this.getWindow().getDecorView(), this);
        ViewTreeViewModelStoreOwner.set(this.getWindow().getDecorView(), this);
        ViewTreeSavedStateRegistryOwner.set(this.getWindow().getDecorView(), this);
        ViewTreeOnBackPressedDispatcherOwner.set(this.getWindow().getDecorView(), this);
        ViewTreeFullyDrawnReporterOwner.set(this.getWindow().getDecorView(), this);
    }
    
    @Override
    public void invalidateMenu() {
        this.invalidateOptionsMenu();
    }
    
    @Deprecated
    protected void onActivityResult(final int n, final int n2, final Intent intent) {
        if (!this.mActivityResultRegistry.dispatchResult(n, n2, intent)) {
            super.onActivityResult(n, n2, intent);
        }
    }
    
    @Deprecated
    public void onBackPressed() {
        this.getOnBackPressedDispatcher().onBackPressed();
    }
    
    public void onConfigurationChanged(final Configuration configuration) {
        super.onConfigurationChanged(configuration);
        final Iterator<Consumer<Configuration>> iterator = this.mOnConfigurationChangedListeners.iterator();
        while (iterator.hasNext()) {
            iterator.next().accept(configuration);
        }
    }
    
    @Override
    protected void onCreate(final Bundle bundle) {
        this.mSavedStateRegistryController.performRestore(bundle);
        this.mContextAwareHelper.dispatchOnContextAvailable((Context)this);
        super.onCreate(bundle);
        ReportFragment.injectIfNeededIn(this);
        final int mContentLayoutId = this.mContentLayoutId;
        if (mContentLayoutId != 0) {
            this.setContentView(mContentLayoutId);
        }
    }
    
    public boolean onCreatePanelMenu(final int n, final Menu menu) {
        if (n == 0) {
            super.onCreatePanelMenu(n, menu);
            this.mMenuHostHelper.onCreateMenu(menu, this.getMenuInflater());
        }
        return true;
    }
    
    public boolean onMenuItemSelected(final int n, final MenuItem menuItem) {
        return super.onMenuItemSelected(n, menuItem) || (n == 0 && this.mMenuHostHelper.onMenuItemSelected(menuItem));
    }
    
    public void onMultiWindowModeChanged(final boolean b) {
        if (this.mDispatchingOnMultiWindowModeChanged) {
            return;
        }
        final Iterator<Consumer<MultiWindowModeChangedInfo>> iterator = this.mOnMultiWindowModeChangedListeners.iterator();
        while (iterator.hasNext()) {
            iterator.next().accept(new MultiWindowModeChangedInfo(b));
        }
    }
    
    public void onMultiWindowModeChanged(final boolean b, final Configuration configuration) {
        this.mDispatchingOnMultiWindowModeChanged = true;
        try {
            super.onMultiWindowModeChanged(b, configuration);
            this.mDispatchingOnMultiWindowModeChanged = false;
            final Iterator<Consumer<MultiWindowModeChangedInfo>> iterator = this.mOnMultiWindowModeChangedListeners.iterator();
            while (iterator.hasNext()) {
                iterator.next().accept(new MultiWindowModeChangedInfo(b, configuration));
            }
        }
        finally {
            this.mDispatchingOnMultiWindowModeChanged = false;
        }
    }
    
    protected void onNewIntent(final Intent intent) {
        super.onNewIntent(intent);
        final Iterator<Consumer<Intent>> iterator = this.mOnNewIntentListeners.iterator();
        while (iterator.hasNext()) {
            iterator.next().accept(intent);
        }
    }
    
    public void onPanelClosed(final int n, final Menu menu) {
        this.mMenuHostHelper.onMenuClosed(menu);
        super.onPanelClosed(n, menu);
    }
    
    public void onPictureInPictureModeChanged(final boolean b) {
        if (this.mDispatchingOnPictureInPictureModeChanged) {
            return;
        }
        final Iterator<Consumer<PictureInPictureModeChangedInfo>> iterator = this.mOnPictureInPictureModeChangedListeners.iterator();
        while (iterator.hasNext()) {
            iterator.next().accept(new PictureInPictureModeChangedInfo(b));
        }
    }
    
    public void onPictureInPictureModeChanged(final boolean b, final Configuration configuration) {
        this.mDispatchingOnPictureInPictureModeChanged = true;
        try {
            super.onPictureInPictureModeChanged(b, configuration);
            this.mDispatchingOnPictureInPictureModeChanged = false;
            final Iterator<Consumer<PictureInPictureModeChangedInfo>> iterator = this.mOnPictureInPictureModeChangedListeners.iterator();
            while (iterator.hasNext()) {
                iterator.next().accept(new PictureInPictureModeChangedInfo(b, configuration));
            }
        }
        finally {
            this.mDispatchingOnPictureInPictureModeChanged = false;
        }
    }
    
    public boolean onPreparePanel(final int n, final View view, final Menu menu) {
        if (n == 0) {
            super.onPreparePanel(n, view, menu);
            this.mMenuHostHelper.onPrepareMenu(menu);
        }
        return true;
    }
    
    @Deprecated
    public void onRequestPermissionsResult(final int n, final String[] array, final int[] array2) {
        if (!this.mActivityResultRegistry.dispatchResult(n, -1, new Intent().putExtra("androidx.activity.result.contract.extra.PERMISSIONS", array).putExtra("androidx.activity.result.contract.extra.PERMISSION_GRANT_RESULTS", array2)) && Build$VERSION.SDK_INT >= 23) {
            super.onRequestPermissionsResult(n, array, array2);
        }
    }
    
    @Deprecated
    public Object onRetainCustomNonConfigurationInstance() {
        return null;
    }
    
    public final Object onRetainNonConfigurationInstance() {
        final Object onRetainCustomNonConfigurationInstance = this.onRetainCustomNonConfigurationInstance();
        ViewModelStore viewModelStore2;
        final ViewModelStore viewModelStore = viewModelStore2 = this.mViewModelStore;
        if (viewModelStore == null) {
            final NonConfigurationInstances nonConfigurationInstances = (NonConfigurationInstances)this.getLastNonConfigurationInstance();
            viewModelStore2 = viewModelStore;
            if (nonConfigurationInstances != null) {
                viewModelStore2 = nonConfigurationInstances.viewModelStore;
            }
        }
        if (viewModelStore2 == null && onRetainCustomNonConfigurationInstance == null) {
            return null;
        }
        final NonConfigurationInstances nonConfigurationInstances2 = new NonConfigurationInstances();
        nonConfigurationInstances2.custom = onRetainCustomNonConfigurationInstance;
        nonConfigurationInstances2.viewModelStore = viewModelStore2;
        return nonConfigurationInstances2;
    }
    
    @Override
    protected void onSaveInstanceState(final Bundle bundle) {
        final Lifecycle lifecycle = this.getLifecycle();
        if (lifecycle instanceof LifecycleRegistry) {
            ((LifecycleRegistry)lifecycle).setCurrentState(Lifecycle.State.CREATED);
        }
        super.onSaveInstanceState(bundle);
        this.mSavedStateRegistryController.performSave(bundle);
    }
    
    public void onTrimMemory(final int i) {
        super.onTrimMemory(i);
        final Iterator<Consumer<Integer>> iterator = this.mOnTrimMemoryListeners.iterator();
        while (iterator.hasNext()) {
            iterator.next().accept(i);
        }
    }
    
    @Override
    public Context peekAvailableContext() {
        return this.mContextAwareHelper.peekAvailableContext();
    }
    
    @Override
    public final <I, O> ActivityResultLauncher<I> registerForActivityResult(final ActivityResultContract<I, O> activityResultContract, final ActivityResultCallback<O> activityResultCallback) {
        return this.registerForActivityResult(activityResultContract, this.mActivityResultRegistry, activityResultCallback);
    }
    
    @Override
    public final <I, O> ActivityResultLauncher<I> registerForActivityResult(final ActivityResultContract<I, O> activityResultContract, final ActivityResultRegistry activityResultRegistry, final ActivityResultCallback<O> activityResultCallback) {
        final StringBuilder sb = new StringBuilder();
        sb.append("activity_rq#");
        sb.append(this.mNextLocalRequestCode.getAndIncrement());
        return activityResultRegistry.register(sb.toString(), this, activityResultContract, activityResultCallback);
    }
    
    @Override
    public void removeMenuProvider(final MenuProvider menuProvider) {
        this.mMenuHostHelper.removeMenuProvider(menuProvider);
    }
    
    @Override
    public final void removeOnConfigurationChangedListener(final Consumer<Configuration> o) {
        this.mOnConfigurationChangedListeners.remove(o);
    }
    
    @Override
    public final void removeOnContextAvailableListener(final OnContextAvailableListener onContextAvailableListener) {
        this.mContextAwareHelper.removeOnContextAvailableListener(onContextAvailableListener);
    }
    
    @Override
    public final void removeOnMultiWindowModeChangedListener(final Consumer<MultiWindowModeChangedInfo> o) {
        this.mOnMultiWindowModeChangedListeners.remove(o);
    }
    
    @Override
    public final void removeOnNewIntentListener(final Consumer<Intent> o) {
        this.mOnNewIntentListeners.remove(o);
    }
    
    @Override
    public final void removeOnPictureInPictureModeChangedListener(final Consumer<PictureInPictureModeChangedInfo> o) {
        this.mOnPictureInPictureModeChangedListeners.remove(o);
    }
    
    @Override
    public final void removeOnTrimMemoryListener(final Consumer<Integer> o) {
        this.mOnTrimMemoryListeners.remove(o);
    }
    
    public void reportFullyDrawn() {
        try {
            if (Trace.isEnabled()) {
                Trace.beginSection("reportFullyDrawn() for ComponentActivity");
            }
            if (Build$VERSION.SDK_INT > 19) {
                super.reportFullyDrawn();
            }
            else if (Build$VERSION.SDK_INT == 19 && ContextCompat.checkSelfPermission((Context)this, "android.permission.UPDATE_DEVICE_STATS") == 0) {
                super.reportFullyDrawn();
            }
            this.mFullyDrawnReporter.fullyDrawnReported();
        }
        finally {
            Trace.endSection();
        }
    }
    
    public void setContentView(final int contentView) {
        this.initializeViewTreeOwners();
        this.mReportFullyDrawnExecutor.viewCreated(this.getWindow().getDecorView());
        super.setContentView(contentView);
    }
    
    public void setContentView(final View contentView) {
        this.initializeViewTreeOwners();
        this.mReportFullyDrawnExecutor.viewCreated(this.getWindow().getDecorView());
        super.setContentView(contentView);
    }
    
    public void setContentView(final View view, final ViewGroup$LayoutParams viewGroup$LayoutParams) {
        this.initializeViewTreeOwners();
        this.mReportFullyDrawnExecutor.viewCreated(this.getWindow().getDecorView());
        super.setContentView(view, viewGroup$LayoutParams);
    }
    
    @Deprecated
    public void startActivityForResult(final Intent intent, final int n) {
        super.startActivityForResult(intent, n);
    }
    
    @Deprecated
    public void startActivityForResult(final Intent intent, final int n, final Bundle bundle) {
        super.startActivityForResult(intent, n, bundle);
    }
    
    @Deprecated
    public void startIntentSenderForResult(final IntentSender intentSender, final int n, final Intent intent, final int n2, final int n3, final int n4) throws IntentSender$SendIntentException {
        super.startIntentSenderForResult(intentSender, n, intent, n2, n3, n4);
    }
    
    @Deprecated
    public void startIntentSenderForResult(final IntentSender intentSender, final int n, final Intent intent, final int n2, final int n3, final int n4, final Bundle bundle) throws IntentSender$SendIntentException {
        super.startIntentSenderForResult(intentSender, n, intent, n2, n3, n4, bundle);
    }
    
    static class Api19Impl
    {
        private Api19Impl() {
        }
        
        static void cancelPendingInputEvents(final View view) {
            view.cancelPendingInputEvents();
        }
    }
    
    static class Api33Impl
    {
        private Api33Impl() {
        }
        
        static OnBackInvokedDispatcher getOnBackInvokedDispatcher(final Activity activity) {
            return activity.getOnBackInvokedDispatcher();
        }
    }
    
    static final class NonConfigurationInstances
    {
        Object custom;
        ViewModelStore viewModelStore;
    }
    
    private interface ReportFullyDrawnExecutor extends Executor
    {
        void activityDestroyed();
        
        void viewCreated(final View p0);
    }
    
    static class ReportFullyDrawnExecutorApi1 implements ReportFullyDrawnExecutor
    {
        final Handler mHandler;
        
        ReportFullyDrawnExecutorApi1() {
            this.mHandler = this.createHandler();
        }
        
        private Handler createHandler() {
            Looper looper;
            if ((looper = Looper.myLooper()) == null) {
                looper = Looper.getMainLooper();
            }
            return new Handler(looper);
        }
        
        @Override
        public void activityDestroyed() {
        }
        
        @Override
        public void execute(final Runnable runnable) {
            this.mHandler.postAtFrontOfQueue(runnable);
        }
        
        @Override
        public void viewCreated(final View view) {
        }
    }
    
    class ReportFullyDrawnExecutorApi16Impl implements ReportFullyDrawnExecutor, ViewTreeObserver$OnDrawListener, Runnable
    {
        final long mEndWatchTimeMillis;
        boolean mOnDrawScheduled;
        Runnable mRunnable;
        final ComponentActivity this$0;
        
        ReportFullyDrawnExecutorApi16Impl(final ComponentActivity this$0) {
            this.this$0 = this$0;
            this.mEndWatchTimeMillis = SystemClock.uptimeMillis() + 10000L;
            this.mOnDrawScheduled = false;
        }
        
        @Override
        public void activityDestroyed() {
            this.this$0.getWindow().getDecorView().removeCallbacks((Runnable)this);
            this.this$0.getWindow().getDecorView().getViewTreeObserver().removeOnDrawListener((ViewTreeObserver$OnDrawListener)this);
        }
        
        public void execute(final Runnable mRunnable) {
            this.mRunnable = mRunnable;
            final View decorView = this.this$0.getWindow().getDecorView();
            if (this.mOnDrawScheduled) {
                if (Looper.myLooper() == Looper.getMainLooper()) {
                    decorView.invalidate();
                }
                else {
                    decorView.postInvalidate();
                }
            }
            else {
                decorView.postOnAnimation((Runnable)new ComponentActivity$ReportFullyDrawnExecutorApi16Impl$$ExternalSyntheticLambda0(this));
            }
        }
        
        public void onDraw() {
            final Runnable mRunnable = this.mRunnable;
            if (mRunnable != null) {
                mRunnable.run();
                this.mRunnable = null;
                if (this.this$0.mFullyDrawnReporter.isFullyDrawnReported()) {
                    this.mOnDrawScheduled = false;
                    this.this$0.getWindow().getDecorView().post((Runnable)this);
                }
            }
            else if (SystemClock.uptimeMillis() > this.mEndWatchTimeMillis) {
                this.mOnDrawScheduled = false;
                this.this$0.getWindow().getDecorView().post((Runnable)this);
            }
        }
        
        public void run() {
            this.this$0.getWindow().getDecorView().getViewTreeObserver().removeOnDrawListener((ViewTreeObserver$OnDrawListener)this);
        }
        
        @Override
        public void viewCreated(final View view) {
            if (!this.mOnDrawScheduled) {
                this.mOnDrawScheduled = true;
                view.getViewTreeObserver().addOnDrawListener((ViewTreeObserver$OnDrawListener)this);
            }
        }
    }
}

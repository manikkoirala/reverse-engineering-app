// 
// Decompiled by Procyon v0.6.0
// 

package androidx.activity;

import kotlin.coroutines.intrinsics.IntrinsicsKt;
import kotlinx.coroutines.flow.FlowCollector;
import kotlinx.coroutines.flow.FlowKt;
import kotlin.jvm.functions.Function2;
import kotlin.Unit;
import kotlin.coroutines.Continuation;
import android.app.Activity;
import android.graphics.Rect;
import android.view.View;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000\u0014\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u001a\u001d\u0010\u0000\u001a\u00020\u0001*\u00020\u00022\u0006\u0010\u0003\u001a\u00020\u0004H\u0087@\u00f8\u0001\u0000¢\u0006\u0002\u0010\u0005\u0082\u0002\u0004\n\u0002\b\u0019¨\u0006\u0006" }, d2 = { "trackPipAnimationHintView", "", "Landroid/app/Activity;", "view", "Landroid/view/View;", "(Landroid/app/Activity;Landroid/view/View;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "activity-ktx_release" }, k = 2, mv = { 1, 8, 0 }, xi = 48)
public final class PipHintTrackerKt
{
    public static final Object trackPipAnimationHintView(final Activity activity, final View view, final Continuation<? super Unit> continuation) {
        final Object collect = FlowKt.callbackFlow((Function2)new PipHintTrackerKt$trackPipAnimationHintView$flow.PipHintTrackerKt$trackPipAnimationHintView$flow$1(view, (Continuation)null)).collect((FlowCollector)new PipHintTrackerKt$trackPipAnimationHintView.PipHintTrackerKt$trackPipAnimationHintView$2(activity), (Continuation)continuation);
        if (collect == IntrinsicsKt.getCOROUTINE_SUSPENDED()) {
            return collect;
        }
        return Unit.INSTANCE;
    }
    
    private static final Rect trackPipAnimationHintView$positionInWindow(final View view) {
        final Rect rect = new Rect();
        view.getGlobalVisibleRect(rect);
        return rect;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.activity;

import kotlin.jvm.internal.Intrinsics;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import androidx.lifecycle.LifecycleOwner;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000&\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0000\u001a9\u0010\u0000\u001a\u00020\u0001*\u00020\u00022\n\b\u0002\u0010\u0003\u001a\u0004\u0018\u00010\u00042\b\b\u0002\u0010\u0005\u001a\u00020\u00062\u0017\u0010\u0007\u001a\u0013\u0012\u0004\u0012\u00020\u0001\u0012\u0004\u0012\u00020\t0\b¢\u0006\u0002\b\n¨\u0006\u000b" }, d2 = { "addCallback", "Landroidx/activity/OnBackPressedCallback;", "Landroidx/activity/OnBackPressedDispatcher;", "owner", "Landroidx/lifecycle/LifecycleOwner;", "enabled", "", "onBackPressed", "Lkotlin/Function1;", "", "Lkotlin/ExtensionFunctionType;", "activity_release" }, k = 2, mv = { 1, 8, 0 }, xi = 48)
public final class OnBackPressedDispatcherKt
{
    public static final OnBackPressedCallback addCallback(final OnBackPressedDispatcher onBackPressedDispatcher, final LifecycleOwner lifecycleOwner, final boolean b, final Function1<? super OnBackPressedCallback, Unit> function1) {
        Intrinsics.checkNotNullParameter((Object)onBackPressedDispatcher, "<this>");
        Intrinsics.checkNotNullParameter((Object)function1, "onBackPressed");
        final OnBackPressedDispatcherKt$addCallback$callback.OnBackPressedDispatcherKt$addCallback$callback$1 onBackPressedDispatcherKt$addCallback$callback$1 = new OnBackPressedDispatcherKt$addCallback$callback.OnBackPressedDispatcherKt$addCallback$callback$1(b, (Function1)function1);
        if (lifecycleOwner != null) {
            onBackPressedDispatcher.addCallback(lifecycleOwner, (OnBackPressedCallback)onBackPressedDispatcherKt$addCallback$callback$1);
        }
        else {
            onBackPressedDispatcher.addCallback((OnBackPressedCallback)onBackPressedDispatcherKt$addCallback$callback$1);
        }
        return (OnBackPressedCallback)onBackPressedDispatcherKt$addCallback$callback$1;
    }
}

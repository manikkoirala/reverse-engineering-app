// 
// Decompiled by Procyon v0.6.0
// 

package androidx.activity;

import androidx.lifecycle.LifecycleObserver;
import androidx.lifecycle.LifecycleEventObserver;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleOwner;
import java.util.Iterator;
import java.util.Collection;
import java.util.ListIterator;
import java.util.List;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Lambda;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import android.os.Build$VERSION;
import kotlin.jvm.internal.DefaultConstructorMarker;
import androidx.core.util.Consumer;
import kotlin.collections.ArrayDeque;
import android.window.OnBackInvokedCallback;
import android.window.OnBackInvokedDispatcher;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000T\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u000f\u0018\u00002\u00020\u0001:\u0004)*+,B\u0013\b\u0017\u0012\n\b\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u0003¢\u0006\u0002\u0010\u0004B\u001f\u0012\b\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\u000e\u0010\u0005\u001a\n\u0012\u0004\u0012\u00020\u0007\u0018\u00010\u0006¢\u0006\u0002\u0010\bJ\u0010\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\fH\u0007J\u0018\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0016\u001a\u00020\u00172\u0006\u0010\u0015\u001a\u00020\fH\u0007J\u0015\u0010\u0018\u001a\u00020\u00192\u0006\u0010\u0015\u001a\u00020\fH\u0001¢\u0006\u0002\b\u001aJ\b\u0010\u001b\u001a\u00020\u0014H\u0007J\u0010\u0010\u001c\u001a\u00020\u00142\u0006\u0010\u001d\u001a\u00020\u001eH\u0007J\u0010\u0010\u001f\u001a\u00020\u00142\u0006\u0010\u001d\u001a\u00020\u001eH\u0007J\b\u0010\n\u001a\u00020\u0007H\u0007J\b\u0010 \u001a\u00020\u0014H\u0003J\b\u0010!\u001a\u00020\u0014H\u0007J\u0010\u0010\"\u001a\u00020\u00142\u0006\u0010\u001d\u001a\u00020\u001eH\u0003J\u0010\u0010#\u001a\u00020\u00142\u0006\u0010\u001d\u001a\u00020\u001eH\u0003J\u0010\u0010$\u001a\u00020\u00142\u0006\u0010%\u001a\u00020\u000eH\u0007J\u0010\u0010&\u001a\u00020\u00142\u0006\u0010'\u001a\u00020\u0007H\u0003J\b\u0010(\u001a\u00020\u0014H\u0002R\u000e\u0010\t\u001a\u00020\u0007X\u0082\u000e¢\u0006\u0002\n\u0000R\u0010\u0010\u0002\u001a\u0004\u0018\u00010\u0003X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u0007X\u0082\u000e¢\u0006\u0002\n\u0000R\u0010\u0010\u000b\u001a\u0004\u0018\u00010\fX\u0082\u000e¢\u0006\u0002\n\u0000R\u0010\u0010\r\u001a\u0004\u0018\u00010\u000eX\u0082\u000e¢\u0006\u0002\n\u0000R\u0010\u0010\u000f\u001a\u0004\u0018\u00010\u0010X\u0082\u000e¢\u0006\u0002\n\u0000R\u0014\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\f0\u0012X\u0082\u0004¢\u0006\u0002\n\u0000R\u0016\u0010\u0005\u001a\n\u0012\u0004\u0012\u00020\u0007\u0018\u00010\u0006X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006-" }, d2 = { "Landroidx/activity/OnBackPressedDispatcher;", "", "fallbackOnBackPressed", "Ljava/lang/Runnable;", "(Ljava/lang/Runnable;)V", "onHasEnabledCallbacksChanged", "Landroidx/core/util/Consumer;", "", "(Ljava/lang/Runnable;Landroidx/core/util/Consumer;)V", "backInvokedCallbackRegistered", "hasEnabledCallbacks", "inProgressCallback", "Landroidx/activity/OnBackPressedCallback;", "invokedDispatcher", "Landroid/window/OnBackInvokedDispatcher;", "onBackInvokedCallback", "Landroid/window/OnBackInvokedCallback;", "onBackPressedCallbacks", "Lkotlin/collections/ArrayDeque;", "addCallback", "", "onBackPressedCallback", "owner", "Landroidx/lifecycle/LifecycleOwner;", "addCancellableCallback", "Landroidx/activity/Cancellable;", "addCancellableCallback$activity_release", "dispatchOnBackCancelled", "dispatchOnBackProgressed", "backEvent", "Landroidx/activity/BackEventCompat;", "dispatchOnBackStarted", "onBackCancelled", "onBackPressed", "onBackProgressed", "onBackStarted", "setOnBackInvokedDispatcher", "invoker", "updateBackInvokedCallbackState", "shouldBeRegistered", "updateEnabledCallbacks", "Api33Impl", "Api34Impl", "LifecycleOnBackPressedCancellable", "OnBackPressedCancellable", "activity_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
public final class OnBackPressedDispatcher
{
    private boolean backInvokedCallbackRegistered;
    private final Runnable fallbackOnBackPressed;
    private boolean hasEnabledCallbacks;
    private OnBackPressedCallback inProgressCallback;
    private OnBackInvokedDispatcher invokedDispatcher;
    private OnBackInvokedCallback onBackInvokedCallback;
    private final ArrayDeque<OnBackPressedCallback> onBackPressedCallbacks;
    private final Consumer<Boolean> onHasEnabledCallbacksChanged;
    
    public OnBackPressedDispatcher() {
        this(null, 1, null);
    }
    
    public OnBackPressedDispatcher(final Runnable runnable) {
        this(runnable, null);
    }
    
    public OnBackPressedDispatcher(final Runnable fallbackOnBackPressed, final Consumer<Boolean> onHasEnabledCallbacksChanged) {
        this.fallbackOnBackPressed = fallbackOnBackPressed;
        this.onHasEnabledCallbacksChanged = onHasEnabledCallbacksChanged;
        this.onBackPressedCallbacks = (ArrayDeque<OnBackPressedCallback>)new ArrayDeque();
        if (Build$VERSION.SDK_INT >= 33) {
            OnBackInvokedCallback onBackInvokedCallback;
            if (Build$VERSION.SDK_INT >= 34) {
                onBackInvokedCallback = Api34Impl.INSTANCE.createOnBackAnimationCallback((Function1<? super BackEventCompat, Unit>)new Function1<BackEventCompat, Unit>(this) {
                    final OnBackPressedDispatcher this$0;
                    
                    public final void invoke(final BackEventCompat backEventCompat) {
                        Intrinsics.checkNotNullParameter((Object)backEventCompat, "backEvent");
                        this.this$0.onBackStarted(backEventCompat);
                    }
                }, (Function1<? super BackEventCompat, Unit>)new Function1<BackEventCompat, Unit>(this) {
                    final OnBackPressedDispatcher this$0;
                    
                    public final void invoke(final BackEventCompat backEventCompat) {
                        Intrinsics.checkNotNullParameter((Object)backEventCompat, "backEvent");
                        this.this$0.onBackProgressed(backEventCompat);
                    }
                }, (Function0<Unit>)new Function0<Unit>(this) {
                    final OnBackPressedDispatcher this$0;
                    
                    public final void invoke() {
                        this.this$0.onBackPressed();
                    }
                }, (Function0<Unit>)new Function0<Unit>(this) {
                    final OnBackPressedDispatcher this$0;
                    
                    public final void invoke() {
                        this.this$0.onBackCancelled();
                    }
                });
            }
            else {
                onBackInvokedCallback = Api33Impl.INSTANCE.createOnBackInvokedCallback((Function0<Unit>)new Function0<Unit>(this) {
                    final OnBackPressedDispatcher this$0;
                    
                    public final void invoke() {
                        this.this$0.onBackPressed();
                    }
                });
            }
            this.onBackInvokedCallback = onBackInvokedCallback;
        }
    }
    
    public static final /* synthetic */ OnBackPressedCallback access$getInProgressCallback$p(final OnBackPressedDispatcher onBackPressedDispatcher) {
        return onBackPressedDispatcher.inProgressCallback;
    }
    
    public static final /* synthetic */ ArrayDeque access$getOnBackPressedCallbacks$p(final OnBackPressedDispatcher onBackPressedDispatcher) {
        return onBackPressedDispatcher.onBackPressedCallbacks;
    }
    
    public static final /* synthetic */ void access$setInProgressCallback$p(final OnBackPressedDispatcher onBackPressedDispatcher, final OnBackPressedCallback inProgressCallback) {
        onBackPressedDispatcher.inProgressCallback = inProgressCallback;
    }
    
    private final void onBackCancelled() {
        final List list = (List)this.onBackPressedCallbacks;
        final ListIterator listIterator = list.listIterator(list.size());
        while (true) {
            while (listIterator.hasPrevious()) {
                final Object previous = listIterator.previous();
                if (((OnBackPressedCallback)previous).isEnabled()) {
                    final OnBackPressedCallback onBackPressedCallback = (OnBackPressedCallback)previous;
                    this.inProgressCallback = null;
                    if (onBackPressedCallback != null) {
                        onBackPressedCallback.handleOnBackCancelled();
                    }
                    return;
                }
            }
            final Object previous = null;
            continue;
        }
    }
    
    private final void onBackProgressed(final BackEventCompat backEventCompat) {
        final List list = (List)this.onBackPressedCallbacks;
        final ListIterator listIterator = list.listIterator(list.size());
        while (true) {
            while (listIterator.hasPrevious()) {
                final Object previous = listIterator.previous();
                if (((OnBackPressedCallback)previous).isEnabled()) {
                    final OnBackPressedCallback onBackPressedCallback = (OnBackPressedCallback)previous;
                    if (onBackPressedCallback != null) {
                        onBackPressedCallback.handleOnBackProgressed(backEventCompat);
                    }
                    return;
                }
            }
            final Object previous = null;
            continue;
        }
    }
    
    private final void onBackStarted(final BackEventCompat backEventCompat) {
        final List list = (List)this.onBackPressedCallbacks;
        final ListIterator listIterator = list.listIterator(list.size());
        while (true) {
            while (listIterator.hasPrevious()) {
                final Object previous = listIterator.previous();
                if (((OnBackPressedCallback)previous).isEnabled()) {
                    final OnBackPressedCallback inProgressCallback = (OnBackPressedCallback)previous;
                    this.inProgressCallback = inProgressCallback;
                    if (inProgressCallback != null) {
                        inProgressCallback.handleOnBackStarted(backEventCompat);
                    }
                    return;
                }
            }
            final Object previous = null;
            continue;
        }
    }
    
    private final void updateBackInvokedCallbackState(final boolean b) {
        final OnBackInvokedDispatcher invokedDispatcher = this.invokedDispatcher;
        final OnBackInvokedCallback onBackInvokedCallback = this.onBackInvokedCallback;
        if (invokedDispatcher != null && onBackInvokedCallback != null) {
            if (b && !this.backInvokedCallbackRegistered) {
                Api33Impl.INSTANCE.registerOnBackInvokedCallback(invokedDispatcher, 0, onBackInvokedCallback);
                this.backInvokedCallbackRegistered = true;
            }
            else if (!b && this.backInvokedCallbackRegistered) {
                Api33Impl.INSTANCE.unregisterOnBackInvokedCallback(invokedDispatcher, onBackInvokedCallback);
                this.backInvokedCallbackRegistered = false;
            }
        }
    }
    
    private final void updateEnabledCallbacks() {
        final boolean hasEnabledCallbacks = this.hasEnabledCallbacks;
        final Iterable iterable = (Iterable)this.onBackPressedCallbacks;
        final boolean b = iterable instanceof Collection;
        final boolean b2 = false;
        boolean b3 = false;
        Label_0083: {
            if (b && ((Collection)iterable).isEmpty()) {
                b3 = b2;
            }
            else {
                final Iterator iterator = iterable.iterator();
                do {
                    b3 = b2;
                    if (iterator.hasNext()) {
                        continue;
                    }
                    break Label_0083;
                } while (!((OnBackPressedCallback)iterator.next()).isEnabled());
                b3 = true;
            }
        }
        if ((this.hasEnabledCallbacks = b3) != hasEnabledCallbacks) {
            final Consumer<Boolean> onHasEnabledCallbacksChanged = this.onHasEnabledCallbacksChanged;
            if (onHasEnabledCallbacksChanged != null) {
                onHasEnabledCallbacksChanged.accept(b3);
            }
            if (Build$VERSION.SDK_INT >= 33) {
                this.updateBackInvokedCallbackState(b3);
            }
        }
    }
    
    public final void addCallback(final OnBackPressedCallback onBackPressedCallback) {
        Intrinsics.checkNotNullParameter((Object)onBackPressedCallback, "onBackPressedCallback");
        this.addCancellableCallback$activity_release(onBackPressedCallback);
    }
    
    public final void addCallback(final LifecycleOwner lifecycleOwner, final OnBackPressedCallback onBackPressedCallback) {
        Intrinsics.checkNotNullParameter((Object)lifecycleOwner, "owner");
        Intrinsics.checkNotNullParameter((Object)onBackPressedCallback, "onBackPressedCallback");
        final Lifecycle lifecycle = lifecycleOwner.getLifecycle();
        if (lifecycle.getCurrentState() == Lifecycle.State.DESTROYED) {
            return;
        }
        onBackPressedCallback.addCancellable(new LifecycleOnBackPressedCancellable(lifecycle, onBackPressedCallback));
        this.updateEnabledCallbacks();
        onBackPressedCallback.setEnabledChangedCallback$activity_release((Function0<Unit>)new OnBackPressedDispatcher$addCallback.OnBackPressedDispatcher$addCallback$1((Object)this));
    }
    
    public final Cancellable addCancellableCallback$activity_release(final OnBackPressedCallback onBackPressedCallback) {
        Intrinsics.checkNotNullParameter((Object)onBackPressedCallback, "onBackPressedCallback");
        this.onBackPressedCallbacks.add((Object)onBackPressedCallback);
        final Cancellable cancellable = new OnBackPressedCancellable(onBackPressedCallback);
        onBackPressedCallback.addCancellable(cancellable);
        this.updateEnabledCallbacks();
        onBackPressedCallback.setEnabledChangedCallback$activity_release((Function0<Unit>)new OnBackPressedDispatcher$addCancellableCallback.OnBackPressedDispatcher$addCancellableCallback$1((Object)this));
        return cancellable;
    }
    
    public final void dispatchOnBackCancelled() {
        this.onBackCancelled();
    }
    
    public final void dispatchOnBackProgressed(final BackEventCompat backEventCompat) {
        Intrinsics.checkNotNullParameter((Object)backEventCompat, "backEvent");
        this.onBackProgressed(backEventCompat);
    }
    
    public final void dispatchOnBackStarted(final BackEventCompat backEventCompat) {
        Intrinsics.checkNotNullParameter((Object)backEventCompat, "backEvent");
        this.onBackStarted(backEventCompat);
    }
    
    public final boolean hasEnabledCallbacks() {
        return this.hasEnabledCallbacks;
    }
    
    public final void onBackPressed() {
        final List list = (List)this.onBackPressedCallbacks;
        final ListIterator listIterator = list.listIterator(list.size());
        while (true) {
            while (listIterator.hasPrevious()) {
                final Object previous = listIterator.previous();
                if (((OnBackPressedCallback)previous).isEnabled()) {
                    final OnBackPressedCallback onBackPressedCallback = (OnBackPressedCallback)previous;
                    this.inProgressCallback = null;
                    if (onBackPressedCallback != null) {
                        onBackPressedCallback.handleOnBackPressed();
                        return;
                    }
                    final Runnable fallbackOnBackPressed = this.fallbackOnBackPressed;
                    if (fallbackOnBackPressed != null) {
                        fallbackOnBackPressed.run();
                    }
                    return;
                }
            }
            final Object previous = null;
            continue;
        }
    }
    
    public final void setOnBackInvokedDispatcher(final OnBackInvokedDispatcher invokedDispatcher) {
        Intrinsics.checkNotNullParameter((Object)invokedDispatcher, "invoker");
        this.invokedDispatcher = invokedDispatcher;
        this.updateBackInvokedCallbackState(this.hasEnabledCallbacks);
    }
    
    @Metadata(d1 = { "\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0003\b\u00c1\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0016\u0010\u0003\u001a\u00020\u00042\f\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006H\u0007J \u0010\b\u001a\u00020\u00072\u0006\u0010\t\u001a\u00020\u00012\u0006\u0010\n\u001a\u00020\u000b2\u0006\u0010\f\u001a\u00020\u0001H\u0007J\u0018\u0010\r\u001a\u00020\u00072\u0006\u0010\t\u001a\u00020\u00012\u0006\u0010\f\u001a\u00020\u0001H\u0007¨\u0006\u000e" }, d2 = { "Landroidx/activity/OnBackPressedDispatcher$Api33Impl;", "", "()V", "createOnBackInvokedCallback", "Landroid/window/OnBackInvokedCallback;", "onBackInvoked", "Lkotlin/Function0;", "", "registerOnBackInvokedCallback", "dispatcher", "priority", "", "callback", "unregisterOnBackInvokedCallback", "activity_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
    public static final class Api33Impl
    {
        public static final Api33Impl INSTANCE;
        
        static {
            INSTANCE = new Api33Impl();
        }
        
        private Api33Impl() {
        }
        
        private static final void createOnBackInvokedCallback$lambda$0(final Function0 function0) {
            Intrinsics.checkNotNullParameter((Object)function0, "$onBackInvoked");
            function0.invoke();
        }
        
        public final OnBackInvokedCallback createOnBackInvokedCallback(final Function0<Unit> function0) {
            Intrinsics.checkNotNullParameter((Object)function0, "onBackInvoked");
            return (OnBackInvokedCallback)new OnBackPressedDispatcher$Api33Impl$$ExternalSyntheticLambda0(function0);
        }
        
        public final void registerOnBackInvokedCallback(final Object o, final int n, final Object o2) {
            Intrinsics.checkNotNullParameter(o, "dispatcher");
            Intrinsics.checkNotNullParameter(o2, "callback");
            ((OnBackInvokedDispatcher)o).registerOnBackInvokedCallback(n, (OnBackInvokedCallback)o2);
        }
        
        public final void unregisterOnBackInvokedCallback(final Object o, final Object o2) {
            Intrinsics.checkNotNullParameter(o, "dispatcher");
            Intrinsics.checkNotNullParameter(o2, "callback");
            ((OnBackInvokedDispatcher)o).unregisterOnBackInvokedCallback((OnBackInvokedCallback)o2);
        }
    }
    
    @Metadata(d1 = { "\u00002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u00c1\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002Jj\u0010\u0003\u001a\u00020\u00042!\u0010\u0005\u001a\u001d\u0012\u0013\u0012\u00110\u0007¢\u0006\f\b\b\u0012\b\b\t\u0012\u0004\b\b(\n\u0012\u0004\u0012\u00020\u000b0\u00062!\u0010\f\u001a\u001d\u0012\u0013\u0012\u00110\u0007¢\u0006\f\b\b\u0012\b\b\t\u0012\u0004\b\b(\n\u0012\u0004\u0012\u00020\u000b0\u00062\f\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u000b0\u000e2\f\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u000b0\u000eH\u0007¨\u0006\u0010" }, d2 = { "Landroidx/activity/OnBackPressedDispatcher$Api34Impl;", "", "()V", "createOnBackAnimationCallback", "Landroid/window/OnBackInvokedCallback;", "onBackStarted", "Lkotlin/Function1;", "Landroidx/activity/BackEventCompat;", "Lkotlin/ParameterName;", "name", "backEvent", "", "onBackProgressed", "onBackInvoked", "Lkotlin/Function0;", "onBackCancelled", "activity_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
    public static final class Api34Impl
    {
        public static final Api34Impl INSTANCE;
        
        static {
            INSTANCE = new Api34Impl();
        }
        
        private Api34Impl() {
        }
        
        public final OnBackInvokedCallback createOnBackAnimationCallback(final Function1<? super BackEventCompat, Unit> function1, final Function1<? super BackEventCompat, Unit> function2, final Function0<Unit> function3, final Function0<Unit> function4) {
            Intrinsics.checkNotNullParameter((Object)function1, "onBackStarted");
            Intrinsics.checkNotNullParameter((Object)function2, "onBackProgressed");
            Intrinsics.checkNotNullParameter((Object)function3, "onBackInvoked");
            Intrinsics.checkNotNullParameter((Object)function4, "onBackCancelled");
            return (OnBackInvokedCallback)new OnBackPressedDispatcher$Api34Impl$createOnBackAnimationCallback.OnBackPressedDispatcher$Api34Impl$createOnBackAnimationCallback$1((Function1)function1, (Function1)function2, (Function0)function3, (Function0)function4);
        }
    }
    
    @Metadata(d1 = { "\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0082\u0004\u0018\u00002\u00020\u00012\u00020\u0002B\u0015\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006¢\u0006\u0002\u0010\u0007J\b\u0010\t\u001a\u00020\nH\u0016J\u0018\u0010\u000b\u001a\u00020\n2\u0006\u0010\f\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u000fH\u0016R\u0010\u0010\b\u001a\u0004\u0018\u00010\u0002X\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u0010" }, d2 = { "Landroidx/activity/OnBackPressedDispatcher$LifecycleOnBackPressedCancellable;", "Landroidx/lifecycle/LifecycleEventObserver;", "Landroidx/activity/Cancellable;", "lifecycle", "Landroidx/lifecycle/Lifecycle;", "onBackPressedCallback", "Landroidx/activity/OnBackPressedCallback;", "(Landroidx/activity/OnBackPressedDispatcher;Landroidx/lifecycle/Lifecycle;Landroidx/activity/OnBackPressedCallback;)V", "currentCancellable", "cancel", "", "onStateChanged", "source", "Landroidx/lifecycle/LifecycleOwner;", "event", "Landroidx/lifecycle/Lifecycle$Event;", "activity_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
    private final class LifecycleOnBackPressedCancellable implements LifecycleEventObserver, Cancellable
    {
        private Cancellable currentCancellable;
        private final Lifecycle lifecycle;
        private final OnBackPressedCallback onBackPressedCallback;
        final OnBackPressedDispatcher this$0;
        
        public LifecycleOnBackPressedCancellable(final OnBackPressedDispatcher this$0, final Lifecycle lifecycle, final OnBackPressedCallback onBackPressedCallback) {
            Intrinsics.checkNotNullParameter((Object)lifecycle, "lifecycle");
            Intrinsics.checkNotNullParameter((Object)onBackPressedCallback, "onBackPressedCallback");
            this.this$0 = this$0;
            this.lifecycle = lifecycle;
            this.onBackPressedCallback = onBackPressedCallback;
            lifecycle.addObserver(this);
        }
        
        @Override
        public void cancel() {
            this.lifecycle.removeObserver(this);
            this.onBackPressedCallback.removeCancellable(this);
            final Cancellable currentCancellable = this.currentCancellable;
            if (currentCancellable != null) {
                currentCancellable.cancel();
            }
            this.currentCancellable = null;
        }
        
        @Override
        public void onStateChanged(final LifecycleOwner lifecycleOwner, final Lifecycle.Event event) {
            Intrinsics.checkNotNullParameter((Object)lifecycleOwner, "source");
            Intrinsics.checkNotNullParameter((Object)event, "event");
            if (event == Lifecycle.Event.ON_START) {
                this.currentCancellable = this.this$0.addCancellableCallback$activity_release(this.onBackPressedCallback);
            }
            else if (event == Lifecycle.Event.ON_STOP) {
                final Cancellable currentCancellable = this.currentCancellable;
                if (currentCancellable != null) {
                    currentCancellable.cancel();
                }
            }
            else if (event == Lifecycle.Event.ON_DESTROY) {
                this.cancel();
            }
        }
    }
    
    @Metadata(d1 = { "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\b\u0082\u0004\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\b\u0010\u0005\u001a\u00020\u0006H\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u0007" }, d2 = { "Landroidx/activity/OnBackPressedDispatcher$OnBackPressedCancellable;", "Landroidx/activity/Cancellable;", "onBackPressedCallback", "Landroidx/activity/OnBackPressedCallback;", "(Landroidx/activity/OnBackPressedDispatcher;Landroidx/activity/OnBackPressedCallback;)V", "cancel", "", "activity_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
    private final class OnBackPressedCancellable implements Cancellable
    {
        private final OnBackPressedCallback onBackPressedCallback;
        final OnBackPressedDispatcher this$0;
        
        public OnBackPressedCancellable(final OnBackPressedDispatcher this$0, final OnBackPressedCallback onBackPressedCallback) {
            Intrinsics.checkNotNullParameter((Object)onBackPressedCallback, "onBackPressedCallback");
            this.this$0 = this$0;
            this.onBackPressedCallback = onBackPressedCallback;
        }
        
        @Override
        public void cancel() {
            OnBackPressedDispatcher.access$getOnBackPressedCallbacks$p(this.this$0).remove((Object)this.onBackPressedCallback);
            if (Intrinsics.areEqual((Object)OnBackPressedDispatcher.access$getInProgressCallback$p(this.this$0), (Object)this.onBackPressedCallback)) {
                this.onBackPressedCallback.handleOnBackCancelled();
                OnBackPressedDispatcher.access$setInProgressCallback$p(this.this$0, null);
            }
            this.onBackPressedCallback.removeCancellable(this);
            final Function0<Unit> enabledChangedCallback$activity_release = this.onBackPressedCallback.getEnabledChangedCallback$activity_release();
            if (enabledChangedCallback$activity_release != null) {
                enabledChangedCallback$activity_release.invoke();
            }
            this.onBackPressedCallback.setEnabledChangedCallback$activity_release(null);
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.activity.result;

import android.content.Intent;
import kotlin.jvm.internal.Intrinsics;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000\u0012\n\u0000\n\u0002\u0010\b\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u001a\r\u0010\u0000\u001a\u00020\u0001*\u00020\u0002H\u0086\u0002\u001a\u000f\u0010\u0003\u001a\u0004\u0018\u00010\u0004*\u00020\u0002H\u0086\u0002¨\u0006\u0005" }, d2 = { "component1", "", "Landroidx/activity/result/ActivityResult;", "component2", "Landroid/content/Intent;", "activity-ktx_release" }, k = 2, mv = { 1, 8, 0 }, xi = 48)
public final class ActivityResultKt
{
    public static final int component1(final ActivityResult activityResult) {
        Intrinsics.checkNotNullParameter((Object)activityResult, "<this>");
        return activityResult.getResultCode();
    }
    
    public static final Intent component2(final ActivityResult activityResult) {
        Intrinsics.checkNotNullParameter((Object)activityResult, "<this>");
        return activityResult.getData();
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.activity.result;

import androidx.core.app.ActivityOptionsCompat;
import androidx.activity.result.contract.ActivityResultContract;

public abstract class ActivityResultLauncher<I>
{
    public abstract ActivityResultContract<I, ?> getContract();
    
    public void launch(final I n) {
        this.launch(n, null);
    }
    
    public abstract void launch(final I p0, final ActivityOptionsCompat p1);
    
    public abstract void unregister();
}

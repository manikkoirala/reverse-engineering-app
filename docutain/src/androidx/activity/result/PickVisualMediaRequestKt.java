// 
// Decompiled by Procyon v0.6.0
// 

package androidx.activity.result;

import kotlin.jvm.internal.Intrinsics;
import androidx.activity.result.contract.ActivityResultContracts;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u001a\u0010\u0010\u0000\u001a\u00020\u00012\b\b\u0002\u0010\u0002\u001a\u00020\u0003¨\u0006\u0004" }, d2 = { "PickVisualMediaRequest", "Landroidx/activity/result/PickVisualMediaRequest;", "mediaType", "Landroidx/activity/result/contract/ActivityResultContracts$PickVisualMedia$VisualMediaType;", "activity_release" }, k = 2, mv = { 1, 8, 0 }, xi = 48)
public final class PickVisualMediaRequestKt
{
    public static final PickVisualMediaRequest PickVisualMediaRequest(final ActivityResultContracts.PickVisualMedia.VisualMediaType mediaType) {
        Intrinsics.checkNotNullParameter((Object)mediaType, "mediaType");
        return new PickVisualMediaRequest.Builder().setMediaType(mediaType).build();
    }
}

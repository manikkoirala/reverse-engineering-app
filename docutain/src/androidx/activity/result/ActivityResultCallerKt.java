// 
// Decompiled by Procyon v0.6.0
// 

package androidx.activity.result;

import kotlin.jvm.internal.Intrinsics;
import kotlin.Unit;
import androidx.activity.result.contract.ActivityResultContract;
import kotlin.jvm.functions.Function1;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000,\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\u001aQ\u0010\u0000\u001a\b\u0012\u0004\u0012\u00020\u00020\u0001\"\u0004\b\u0000\u0010\u0003\"\u0004\b\u0001\u0010\u0004*\u00020\u00052\u0012\u0010\u0006\u001a\u000e\u0012\u0004\u0012\u0002H\u0003\u0012\u0004\u0012\u0002H\u00040\u00072\u0006\u0010\b\u001a\u0002H\u00032\u0012\u0010\t\u001a\u000e\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u00020\u00020\n¢\u0006\u0002\u0010\u000b\u001aY\u0010\u0000\u001a\b\u0012\u0004\u0012\u00020\u00020\u0001\"\u0004\b\u0000\u0010\u0003\"\u0004\b\u0001\u0010\u0004*\u00020\u00052\u0012\u0010\u0006\u001a\u000e\u0012\u0004\u0012\u0002H\u0003\u0012\u0004\u0012\u0002H\u00040\u00072\u0006\u0010\b\u001a\u0002H\u00032\u0006\u0010\f\u001a\u00020\r2\u0012\u0010\t\u001a\u000e\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u00020\u00020\n¢\u0006\u0002\u0010\u000e¨\u0006\u000f" }, d2 = { "registerForActivityResult", "Landroidx/activity/result/ActivityResultLauncher;", "", "I", "O", "Landroidx/activity/result/ActivityResultCaller;", "contract", "Landroidx/activity/result/contract/ActivityResultContract;", "input", "callback", "Lkotlin/Function1;", "(Landroidx/activity/result/ActivityResultCaller;Landroidx/activity/result/contract/ActivityResultContract;Ljava/lang/Object;Lkotlin/jvm/functions/Function1;)Landroidx/activity/result/ActivityResultLauncher;", "registry", "Landroidx/activity/result/ActivityResultRegistry;", "(Landroidx/activity/result/ActivityResultCaller;Landroidx/activity/result/contract/ActivityResultContract;Ljava/lang/Object;Landroidx/activity/result/ActivityResultRegistry;Lkotlin/jvm/functions/Function1;)Landroidx/activity/result/ActivityResultLauncher;", "activity-ktx_release" }, k = 2, mv = { 1, 8, 0 }, xi = 48)
public final class ActivityResultCallerKt
{
    public static final <I, O> ActivityResultLauncher<Unit> registerForActivityResult(final ActivityResultCaller activityResultCaller, final ActivityResultContract<I, O> activityResultContract, final I n, final ActivityResultRegistry activityResultRegistry, final Function1<? super O, Unit> function1) {
        Intrinsics.checkNotNullParameter((Object)activityResultCaller, "<this>");
        Intrinsics.checkNotNullParameter((Object)activityResultContract, "contract");
        Intrinsics.checkNotNullParameter((Object)activityResultRegistry, "registry");
        Intrinsics.checkNotNullParameter((Object)function1, "callback");
        final ActivityResultLauncher<I> registerForActivityResult = activityResultCaller.registerForActivityResult(activityResultContract, activityResultRegistry, new ActivityResultCallerKt$$ExternalSyntheticLambda1(function1));
        Intrinsics.checkNotNullExpressionValue((Object)registerForActivityResult, "registerForActivityResul\u2026egistry) { callback(it) }");
        return new ActivityResultCallerLauncher<Object, Object>((ActivityResultLauncher<Object>)registerForActivityResult, (ActivityResultContract<Object, Object>)activityResultContract, n);
    }
    
    public static final <I, O> ActivityResultLauncher<Unit> registerForActivityResult(final ActivityResultCaller activityResultCaller, final ActivityResultContract<I, O> activityResultContract, final I n, final Function1<? super O, Unit> function1) {
        Intrinsics.checkNotNullParameter((Object)activityResultCaller, "<this>");
        Intrinsics.checkNotNullParameter((Object)activityResultContract, "contract");
        Intrinsics.checkNotNullParameter((Object)function1, "callback");
        final ActivityResultLauncher<I> registerForActivityResult = activityResultCaller.registerForActivityResult(activityResultContract, new ActivityResultCallerKt$$ExternalSyntheticLambda0(function1));
        Intrinsics.checkNotNullExpressionValue((Object)registerForActivityResult, "registerForActivityResul\u2026ontract) { callback(it) }");
        return new ActivityResultCallerLauncher<Object, Object>((ActivityResultLauncher<Object>)registerForActivityResult, (ActivityResultContract<Object, Object>)activityResultContract, n);
    }
    
    private static final void registerForActivityResult$lambda$0(final Function1 function1, final Object o) {
        Intrinsics.checkNotNullParameter((Object)function1, "$callback");
        function1.invoke(o);
    }
    
    private static final void registerForActivityResult$lambda$1(final Function1 function1, final Object o) {
        Intrinsics.checkNotNullParameter((Object)function1, "$callback");
        function1.invoke(o);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.activity.result.contract;

import android.graphics.Bitmap;
import androidx.activity.result.IntentSenderRequest;
import androidx.activity.result.ActivityResult;
import kotlin.collections.ArraysKt;
import kotlin.Pair;
import kotlin.TuplesKt;
import java.util.LinkedHashMap;
import kotlin.ranges.RangesKt;
import androidx.core.content.ContextCompat;
import kotlin.collections.MapsKt;
import java.util.Map;
import android.os.ext.SdkExtensions;
import kotlin.NoWhenBranchMatchedException;
import kotlin.jvm.JvmStatic;
import android.content.pm.ActivityInfo;
import android.content.pm.ResolveInfo;
import android.provider.MediaStore;
import androidx.activity.result.PickVisualMediaRequest;
import android.os.Build$VERSION;
import android.content.ClipData;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashSet;
import kotlin.collections.CollectionsKt;
import kotlin.jvm.internal.DefaultConstructorMarker;
import java.util.List;
import kotlin.ReplaceWith;
import kotlin.Deprecated;
import android.os.Parcelable;
import kotlin.jvm.internal.Intrinsics;
import android.content.Intent;
import android.content.Context;
import android.net.Uri;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0013\u0018\u00002\u00020\u0001:\u0011\u0003\u0004\u0005\u0006\u0007\b\t\n\u000b\f\r\u000e\u000f\u0010\u0011\u0012\u0013B\u0007\b\u0002¢\u0006\u0002\u0010\u0002¨\u0006\u0014" }, d2 = { "Landroidx/activity/result/contract/ActivityResultContracts;", "", "()V", "CaptureVideo", "CreateDocument", "GetContent", "GetMultipleContents", "OpenDocument", "OpenDocumentTree", "OpenMultipleDocuments", "PickContact", "PickMultipleVisualMedia", "PickVisualMedia", "RequestMultiplePermissions", "RequestPermission", "StartActivityForResult", "StartIntentSenderForResult", "TakePicture", "TakePicturePreview", "TakeVideo", "activity_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
public final class ActivityResultContracts
{
    private ActivityResultContracts() {
    }
    
    @Metadata(d1 = { "\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\b\u0016\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001B\u0005¢\u0006\u0002\u0010\u0004J\u0018\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\u0002H\u0017J\u001e\u0010\n\u001a\n\u0012\u0004\u0012\u00020\u0003\u0018\u00010\u000b2\u0006\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\u0002J\u001d\u0010\f\u001a\u00020\u00032\u0006\u0010\r\u001a\u00020\u000e2\b\u0010\u000f\u001a\u0004\u0018\u00010\u0006¢\u0006\u0002\u0010\u0010¨\u0006\u0011" }, d2 = { "Landroidx/activity/result/contract/ActivityResultContracts$CaptureVideo;", "Landroidx/activity/result/contract/ActivityResultContract;", "Landroid/net/Uri;", "", "()V", "createIntent", "Landroid/content/Intent;", "context", "Landroid/content/Context;", "input", "getSynchronousResult", "Landroidx/activity/result/contract/ActivityResultContract$SynchronousResult;", "parseResult", "resultCode", "", "intent", "(ILandroid/content/Intent;)Ljava/lang/Boolean;", "activity_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
    public static class CaptureVideo extends ActivityResultContract<Uri, Boolean>
    {
        @Override
        public Intent createIntent(final Context context, final Uri uri) {
            Intrinsics.checkNotNullParameter((Object)context, "context");
            Intrinsics.checkNotNullParameter((Object)uri, "input");
            final Intent putExtra = new Intent("android.media.action.VIDEO_CAPTURE").putExtra("output", (Parcelable)uri);
            Intrinsics.checkNotNullExpressionValue((Object)putExtra, "Intent(MediaStore.ACTION\u2026tore.EXTRA_OUTPUT, input)");
            return putExtra;
        }
        
        public final SynchronousResult<Boolean> getSynchronousResult(final Context context, final Uri uri) {
            Intrinsics.checkNotNullParameter((Object)context, "context");
            Intrinsics.checkNotNullParameter((Object)uri, "input");
            return null;
        }
        
        @Override
        public final Boolean parseResult(final int n, final Intent intent) {
            return n == -1;
        }
    }
    
    @Metadata(d1 = { "\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\b\u0017\u0018\u00002\u0010\u0012\u0004\u0012\u00020\u0002\u0012\u0006\u0012\u0004\u0018\u00010\u00030\u0001B\u0007\b\u0017¢\u0006\u0002\u0010\u0004B\r\u0012\u0006\u0010\u0005\u001a\u00020\u0002¢\u0006\u0002\u0010\u0006J\u0018\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u0002H\u0017J \u0010\f\u001a\f\u0012\u0006\u0012\u0004\u0018\u00010\u0003\u0018\u00010\r2\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u0002J\u001a\u0010\u000e\u001a\u0004\u0018\u00010\u00032\u0006\u0010\u000f\u001a\u00020\u00102\b\u0010\u0011\u001a\u0004\u0018\u00010\bR\u000e\u0010\u0005\u001a\u00020\u0002X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u0012" }, d2 = { "Landroidx/activity/result/contract/ActivityResultContracts$CreateDocument;", "Landroidx/activity/result/contract/ActivityResultContract;", "", "Landroid/net/Uri;", "()V", "mimeType", "(Ljava/lang/String;)V", "createIntent", "Landroid/content/Intent;", "context", "Landroid/content/Context;", "input", "getSynchronousResult", "Landroidx/activity/result/contract/ActivityResultContract$SynchronousResult;", "parseResult", "resultCode", "", "intent", "activity_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
    public static class CreateDocument extends ActivityResultContract<String, Uri>
    {
        private final String mimeType;
        
        @Deprecated(message = "Using a wildcard mime type with CreateDocument is not recommended as it breaks the automatic handling of file extensions. Instead, specify the mime type by using the constructor that takes an concrete mime type (e.g.., CreateDocument(\"image/png\")).", replaceWith = @ReplaceWith(expression = "CreateDocument(\"todo/todo\")", imports = {}))
        public CreateDocument() {
            this("*/*");
        }
        
        public CreateDocument(final String mimeType) {
            Intrinsics.checkNotNullParameter((Object)mimeType, "mimeType");
            this.mimeType = mimeType;
        }
        
        @Override
        public Intent createIntent(final Context context, final String s) {
            Intrinsics.checkNotNullParameter((Object)context, "context");
            Intrinsics.checkNotNullParameter((Object)s, "input");
            final Intent putExtra = new Intent("android.intent.action.CREATE_DOCUMENT").setType(this.mimeType).putExtra("android.intent.extra.TITLE", s);
            Intrinsics.checkNotNullExpressionValue((Object)putExtra, "Intent(Intent.ACTION_CRE\u2026ntent.EXTRA_TITLE, input)");
            return putExtra;
        }
        
        public final SynchronousResult<Uri> getSynchronousResult(final Context context, final String s) {
            Intrinsics.checkNotNullParameter((Object)context, "context");
            Intrinsics.checkNotNullParameter((Object)s, "input");
            return null;
        }
        
        @Override
        public final Uri parseResult(int n, Intent intent) {
            if (n == -1) {
                n = 1;
            }
            else {
                n = 0;
            }
            Uri data = null;
            if (n == 0) {
                intent = null;
            }
            if (intent != null) {
                data = intent.getData();
            }
            return data;
        }
    }
    
    @Metadata(d1 = { "\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\b\u0016\u0018\u00002\u0010\u0012\u0004\u0012\u00020\u0002\u0012\u0006\u0012\u0004\u0018\u00010\u00030\u0001B\u0005¢\u0006\u0002\u0010\u0004J\u0018\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\u0002H\u0017J \u0010\n\u001a\f\u0012\u0006\u0012\u0004\u0018\u00010\u0003\u0018\u00010\u000b2\u0006\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\u0002J\u001a\u0010\f\u001a\u0004\u0018\u00010\u00032\u0006\u0010\r\u001a\u00020\u000e2\b\u0010\u000f\u001a\u0004\u0018\u00010\u0006¨\u0006\u0010" }, d2 = { "Landroidx/activity/result/contract/ActivityResultContracts$GetContent;", "Landroidx/activity/result/contract/ActivityResultContract;", "", "Landroid/net/Uri;", "()V", "createIntent", "Landroid/content/Intent;", "context", "Landroid/content/Context;", "input", "getSynchronousResult", "Landroidx/activity/result/contract/ActivityResultContract$SynchronousResult;", "parseResult", "resultCode", "", "intent", "activity_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
    public static class GetContent extends ActivityResultContract<String, Uri>
    {
        @Override
        public Intent createIntent(final Context context, final String type) {
            Intrinsics.checkNotNullParameter((Object)context, "context");
            Intrinsics.checkNotNullParameter((Object)type, "input");
            final Intent setType = new Intent("android.intent.action.GET_CONTENT").addCategory("android.intent.category.OPENABLE").setType(type);
            Intrinsics.checkNotNullExpressionValue((Object)setType, "Intent(Intent.ACTION_GET\u2026          .setType(input)");
            return setType;
        }
        
        public final SynchronousResult<Uri> getSynchronousResult(final Context context, final String s) {
            Intrinsics.checkNotNullParameter((Object)context, "context");
            Intrinsics.checkNotNullParameter((Object)s, "input");
            return null;
        }
        
        @Override
        public final Uri parseResult(int n, Intent intent) {
            if (n == -1) {
                n = 1;
            }
            else {
                n = 0;
            }
            Uri data = null;
            if (n == 0) {
                intent = null;
            }
            if (intent != null) {
                data = intent.getData();
            }
            return data;
        }
    }
    
    @Metadata(d1 = { "\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\b\u0017\u0018\u0000 \u00122\u0019\u0012\u0004\u0012\u00020\u0002\u0012\u000f\u0012\r\u0012\t\u0012\u00070\u0004¢\u0006\u0002\b\u00050\u00030\u0001:\u0001\u0012B\u0005¢\u0006\u0002\u0010\u0006J\u0018\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u0002H\u0017J$\u0010\f\u001a\u0010\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00040\u0003\u0018\u00010\r2\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u0002J\u001e\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\u00040\u00032\u0006\u0010\u000f\u001a\u00020\u00102\b\u0010\u0011\u001a\u0004\u0018\u00010\b¨\u0006\u0013" }, d2 = { "Landroidx/activity/result/contract/ActivityResultContracts$GetMultipleContents;", "Landroidx/activity/result/contract/ActivityResultContract;", "", "", "Landroid/net/Uri;", "Lkotlin/jvm/JvmSuppressWildcards;", "()V", "createIntent", "Landroid/content/Intent;", "context", "Landroid/content/Context;", "input", "getSynchronousResult", "Landroidx/activity/result/contract/ActivityResultContract$SynchronousResult;", "parseResult", "resultCode", "", "intent", "Companion", "activity_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
    public static class GetMultipleContents extends ActivityResultContract<String, List<Uri>>
    {
        public static final Companion Companion;
        
        static {
            Companion = new Companion(null);
        }
        
        @Override
        public Intent createIntent(final Context context, final String type) {
            Intrinsics.checkNotNullParameter((Object)context, "context");
            Intrinsics.checkNotNullParameter((Object)type, "input");
            final Intent putExtra = new Intent("android.intent.action.GET_CONTENT").addCategory("android.intent.category.OPENABLE").setType(type).putExtra("android.intent.extra.ALLOW_MULTIPLE", true);
            Intrinsics.checkNotNullExpressionValue((Object)putExtra, "Intent(Intent.ACTION_GET\u2026TRA_ALLOW_MULTIPLE, true)");
            return putExtra;
        }
        
        public final SynchronousResult<List<Uri>> getSynchronousResult(final Context context, final String s) {
            Intrinsics.checkNotNullParameter((Object)context, "context");
            Intrinsics.checkNotNullParameter((Object)s, "input");
            return null;
        }
        
        @Override
        public final List<Uri> parseResult(int n, Intent intent) {
            if (n == -1) {
                n = 1;
            }
            else {
                n = 0;
            }
            if (n == 0) {
                intent = null;
            }
            if (intent != null) {
                final List<Uri> list = GetMultipleContents.Companion.getClipDataUris$activity_release(intent);
                if (list != null) {
                    return list;
                }
            }
            return CollectionsKt.emptyList();
        }
        
        @Metadata(d1 = { "\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0081\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0017\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004*\u00020\u0006H\u0000¢\u0006\u0002\b\u0007¨\u0006\b" }, d2 = { "Landroidx/activity/result/contract/ActivityResultContracts$GetMultipleContents$Companion;", "", "()V", "getClipDataUris", "", "Landroid/net/Uri;", "Landroid/content/Intent;", "getClipDataUris$activity_release", "activity_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
        public static final class Companion
        {
            private Companion() {
            }
            
            public final List<Uri> getClipDataUris$activity_release(final Intent intent) {
                Intrinsics.checkNotNullParameter((Object)intent, "<this>");
                final LinkedHashSet set = new LinkedHashSet();
                final Uri data = intent.getData();
                if (data != null) {
                    set.add(data);
                }
                final ClipData clipData = intent.getClipData();
                if (clipData == null && set.isEmpty()) {
                    return CollectionsKt.emptyList();
                }
                if (clipData != null) {
                    for (int i = 0; i < clipData.getItemCount(); ++i) {
                        final Uri uri = clipData.getItemAt(i).getUri();
                        if (uri != null) {
                            set.add(uri);
                        }
                    }
                }
                return new ArrayList<Uri>(set);
            }
        }
    }
    
    @Metadata(d1 = { "\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0011\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0002\b\u0017\u0018\u00002\u0016\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00030\u0002\u0012\u0006\u0012\u0004\u0018\u00010\u00040\u0001B\u0005¢\u0006\u0002\u0010\u0005J#\u0010\u0006\u001a\u00020\u00072\u0006\u0010\b\u001a\u00020\t2\f\u0010\n\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002H\u0017¢\u0006\u0002\u0010\u000bJ+\u0010\f\u001a\f\u0012\u0006\u0012\u0004\u0018\u00010\u0004\u0018\u00010\r2\u0006\u0010\b\u001a\u00020\t2\f\u0010\n\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002¢\u0006\u0002\u0010\u000eJ\u001a\u0010\u000f\u001a\u0004\u0018\u00010\u00042\u0006\u0010\u0010\u001a\u00020\u00112\b\u0010\u0012\u001a\u0004\u0018\u00010\u0007¨\u0006\u0013" }, d2 = { "Landroidx/activity/result/contract/ActivityResultContracts$OpenDocument;", "Landroidx/activity/result/contract/ActivityResultContract;", "", "", "Landroid/net/Uri;", "()V", "createIntent", "Landroid/content/Intent;", "context", "Landroid/content/Context;", "input", "(Landroid/content/Context;[Ljava/lang/String;)Landroid/content/Intent;", "getSynchronousResult", "Landroidx/activity/result/contract/ActivityResultContract$SynchronousResult;", "(Landroid/content/Context;[Ljava/lang/String;)Landroidx/activity/result/contract/ActivityResultContract$SynchronousResult;", "parseResult", "resultCode", "", "intent", "activity_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
    public static class OpenDocument extends ActivityResultContract<String[], Uri>
    {
        @Override
        public Intent createIntent(final Context context, final String[] array) {
            Intrinsics.checkNotNullParameter((Object)context, "context");
            Intrinsics.checkNotNullParameter((Object)array, "input");
            final Intent setType = new Intent("android.intent.action.OPEN_DOCUMENT").putExtra("android.intent.extra.MIME_TYPES", array).setType("*/*");
            Intrinsics.checkNotNullExpressionValue((Object)setType, "Intent(Intent.ACTION_OPE\u2026          .setType(\"*/*\")");
            return setType;
        }
        
        public final SynchronousResult<Uri> getSynchronousResult(final Context context, final String[] array) {
            Intrinsics.checkNotNullParameter((Object)context, "context");
            Intrinsics.checkNotNullParameter((Object)array, "input");
            return null;
        }
        
        @Override
        public final Uri parseResult(int n, Intent intent) {
            if (n == -1) {
                n = 1;
            }
            else {
                n = 0;
            }
            Uri data = null;
            if (n == 0) {
                intent = null;
            }
            if (intent != null) {
                data = intent.getData();
            }
            return data;
        }
    }
    
    @Metadata(d1 = { "\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\b\u0017\u0018\u00002\u0012\u0012\u0006\u0012\u0004\u0018\u00010\u0002\u0012\u0006\u0012\u0004\u0018\u00010\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0003J\u001a\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u00072\b\u0010\b\u001a\u0004\u0018\u00010\u0002H\u0017J\"\u0010\t\u001a\f\u0012\u0006\u0012\u0004\u0018\u00010\u0002\u0018\u00010\n2\u0006\u0010\u0006\u001a\u00020\u00072\b\u0010\b\u001a\u0004\u0018\u00010\u0002J\u001a\u0010\u000b\u001a\u0004\u0018\u00010\u00022\u0006\u0010\f\u001a\u00020\r2\b\u0010\u000e\u001a\u0004\u0018\u00010\u0005¨\u0006\u000f" }, d2 = { "Landroidx/activity/result/contract/ActivityResultContracts$OpenDocumentTree;", "Landroidx/activity/result/contract/ActivityResultContract;", "Landroid/net/Uri;", "()V", "createIntent", "Landroid/content/Intent;", "context", "Landroid/content/Context;", "input", "getSynchronousResult", "Landroidx/activity/result/contract/ActivityResultContract$SynchronousResult;", "parseResult", "resultCode", "", "intent", "activity_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
    public static class OpenDocumentTree extends ActivityResultContract<Uri, Uri>
    {
        @Override
        public Intent createIntent(final Context context, final Uri uri) {
            Intrinsics.checkNotNullParameter((Object)context, "context");
            final Intent intent = new Intent("android.intent.action.OPEN_DOCUMENT_TREE");
            if (Build$VERSION.SDK_INT >= 26 && uri != null) {
                intent.putExtra("android.provider.extra.INITIAL_URI", (Parcelable)uri);
            }
            return intent;
        }
        
        public final SynchronousResult<Uri> getSynchronousResult(final Context context, final Uri uri) {
            Intrinsics.checkNotNullParameter((Object)context, "context");
            return null;
        }
        
        @Override
        public final Uri parseResult(int n, Intent intent) {
            if (n == -1) {
                n = 1;
            }
            else {
                n = 0;
            }
            Uri data = null;
            if (n == 0) {
                intent = null;
            }
            if (intent != null) {
                data = intent.getData();
            }
            return data;
        }
    }
    
    @Metadata(d1 = { "\u0000>\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0011\n\u0002\u0010\u000e\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0002\b\u0017\u0018\u00002\u001f\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00030\u0002\u0012\u000f\u0012\r\u0012\t\u0012\u00070\u0005¢\u0006\u0002\b\u00060\u00040\u0001B\u0005¢\u0006\u0002\u0010\u0007J#\u0010\b\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000b2\f\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002H\u0017¢\u0006\u0002\u0010\rJ/\u0010\u000e\u001a\u0010\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00050\u0004\u0018\u00010\u000f2\u0006\u0010\n\u001a\u00020\u000b2\f\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002¢\u0006\u0002\u0010\u0010J\u001e\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\u00050\u00042\u0006\u0010\u0012\u001a\u00020\u00132\b\u0010\u0014\u001a\u0004\u0018\u00010\t¨\u0006\u0015" }, d2 = { "Landroidx/activity/result/contract/ActivityResultContracts$OpenMultipleDocuments;", "Landroidx/activity/result/contract/ActivityResultContract;", "", "", "", "Landroid/net/Uri;", "Lkotlin/jvm/JvmSuppressWildcards;", "()V", "createIntent", "Landroid/content/Intent;", "context", "Landroid/content/Context;", "input", "(Landroid/content/Context;[Ljava/lang/String;)Landroid/content/Intent;", "getSynchronousResult", "Landroidx/activity/result/contract/ActivityResultContract$SynchronousResult;", "(Landroid/content/Context;[Ljava/lang/String;)Landroidx/activity/result/contract/ActivityResultContract$SynchronousResult;", "parseResult", "resultCode", "", "intent", "activity_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
    public static class OpenMultipleDocuments extends ActivityResultContract<String[], List<Uri>>
    {
        @Override
        public Intent createIntent(final Context context, final String[] array) {
            Intrinsics.checkNotNullParameter((Object)context, "context");
            Intrinsics.checkNotNullParameter((Object)array, "input");
            final Intent setType = new Intent("android.intent.action.OPEN_DOCUMENT").putExtra("android.intent.extra.MIME_TYPES", array).putExtra("android.intent.extra.ALLOW_MULTIPLE", true).setType("*/*");
            Intrinsics.checkNotNullExpressionValue((Object)setType, "Intent(Intent.ACTION_OPE\u2026          .setType(\"*/*\")");
            return setType;
        }
        
        public final SynchronousResult<List<Uri>> getSynchronousResult(final Context context, final String[] array) {
            Intrinsics.checkNotNullParameter((Object)context, "context");
            Intrinsics.checkNotNullParameter((Object)array, "input");
            return null;
        }
        
        @Override
        public final List<Uri> parseResult(int n, Intent intent) {
            if (n == -1) {
                n = 1;
            }
            else {
                n = 0;
            }
            if (n == 0) {
                intent = null;
            }
            if (intent != null) {
                final List<Uri> list = GetMultipleContents.Companion.getClipDataUris$activity_release(intent);
                if (list != null) {
                    return list;
                }
            }
            return CollectionsKt.emptyList();
        }
    }
    
    @Metadata(d1 = { "\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0002\u0018\u00002\u0012\u0012\u0006\u0012\u0004\u0018\u00010\u0002\u0012\u0006\u0012\u0004\u0018\u00010\u00030\u0001B\u0005¢\u0006\u0002\u0010\u0004J\u001a\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b2\b\u0010\t\u001a\u0004\u0018\u00010\u0002H\u0016J\u001c\u0010\n\u001a\u0004\u0018\u00010\u00032\u0006\u0010\u000b\u001a\u00020\f2\b\u0010\r\u001a\u0004\u0018\u00010\u0006H\u0016¨\u0006\u000e" }, d2 = { "Landroidx/activity/result/contract/ActivityResultContracts$PickContact;", "Landroidx/activity/result/contract/ActivityResultContract;", "Ljava/lang/Void;", "Landroid/net/Uri;", "()V", "createIntent", "Landroid/content/Intent;", "context", "Landroid/content/Context;", "input", "parseResult", "resultCode", "", "intent", "activity_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
    public static final class PickContact extends ActivityResultContract<Void, Uri>
    {
        @Override
        public Intent createIntent(final Context context, final Void void1) {
            Intrinsics.checkNotNullParameter((Object)context, "context");
            final Intent setType = new Intent("android.intent.action.PICK").setType("vnd.android.cursor.dir/contact");
            Intrinsics.checkNotNullExpressionValue((Object)setType, "Intent(Intent.ACTION_PIC\u2026ct.Contacts.CONTENT_TYPE)");
            return setType;
        }
        
        @Override
        public Uri parseResult(int n, Intent intent) {
            if (n == -1) {
                n = 1;
            }
            else {
                n = 0;
            }
            Uri data = null;
            if (n == 0) {
                intent = null;
            }
            if (intent != null) {
                data = intent.getData();
            }
            return data;
        }
    }
    
    @Metadata(d1 = { "\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0017\u0018\u0000 \u00132\u0019\u0012\u0004\u0012\u00020\u0002\u0012\u000f\u0012\r\u0012\t\u0012\u00070\u0004¢\u0006\u0002\b\u00050\u00030\u0001:\u0001\u0013B\u000f\u0012\b\b\u0002\u0010\u0006\u001a\u00020\u0007¢\u0006\u0002\u0010\bJ\u0018\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u0002H\u0017J)\u0010\u000e\u001a\u0015\u0012\u000f\u0012\r\u0012\t\u0012\u00070\u0004¢\u0006\u0002\b\u00050\u0003\u0018\u00010\u000f2\u0006\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u0002J\u001e\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u00040\u00032\u0006\u0010\u0011\u001a\u00020\u00072\b\u0010\u0012\u001a\u0004\u0018\u00010\nR\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u0014" }, d2 = { "Landroidx/activity/result/contract/ActivityResultContracts$PickMultipleVisualMedia;", "Landroidx/activity/result/contract/ActivityResultContract;", "Landroidx/activity/result/PickVisualMediaRequest;", "", "Landroid/net/Uri;", "Lkotlin/jvm/JvmSuppressWildcards;", "maxItems", "", "(I)V", "createIntent", "Landroid/content/Intent;", "context", "Landroid/content/Context;", "input", "getSynchronousResult", "Landroidx/activity/result/contract/ActivityResultContract$SynchronousResult;", "parseResult", "resultCode", "intent", "Companion", "activity_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
    public static class PickMultipleVisualMedia extends ActivityResultContract<PickVisualMediaRequest, List<Uri>>
    {
        public static final Companion Companion;
        private final int maxItems;
        
        static {
            Companion = new Companion(null);
        }
        
        public PickMultipleVisualMedia() {
            this(0, 1, null);
        }
        
        public PickMultipleVisualMedia(int maxItems) {
            this.maxItems = maxItems;
            final int n = 1;
            if (maxItems > 1) {
                maxItems = n;
            }
            else {
                maxItems = 0;
            }
            if (maxItems != 0) {
                return;
            }
            throw new IllegalArgumentException("Max items must be higher than 1".toString());
        }
        
        @Override
        public Intent createIntent(final Context context, final PickVisualMediaRequest pickVisualMediaRequest) {
            Intrinsics.checkNotNullParameter((Object)context, "context");
            Intrinsics.checkNotNullParameter((Object)pickVisualMediaRequest, "input");
            final boolean systemPickerAvailable$activity_release = PickVisualMedia.Companion.isSystemPickerAvailable$activity_release();
            boolean b = true;
            Intent intent;
            if (systemPickerAvailable$activity_release) {
                intent = new Intent("android.provider.action.PICK_IMAGES");
                intent.setType(PickVisualMedia.Companion.getVisualMimeType$activity_release(pickVisualMediaRequest.getMediaType()));
                if (this.maxItems > MediaStore.getPickImagesMaxLimit()) {
                    b = false;
                }
                if (!b) {
                    throw new IllegalArgumentException("Max items must be less or equals MediaStore.getPickImagesMaxLimit()".toString());
                }
                intent.putExtra("android.provider.extra.PICK_IMAGES_MAX", this.maxItems);
            }
            else if (PickVisualMedia.Companion.isSystemFallbackPickerAvailable$activity_release(context)) {
                final ResolveInfo systemFallbackPicker$activity_release = PickVisualMedia.Companion.getSystemFallbackPicker$activity_release(context);
                if (systemFallbackPicker$activity_release == null) {
                    throw new IllegalStateException("Required value was null.".toString());
                }
                final ActivityInfo activityInfo = systemFallbackPicker$activity_release.activityInfo;
                intent = new Intent("androidx.activity.result.contract.action.PICK_IMAGES");
                intent.setClassName(activityInfo.applicationInfo.packageName, activityInfo.name);
                intent.setType(PickVisualMedia.Companion.getVisualMimeType$activity_release(pickVisualMediaRequest.getMediaType()));
                intent.putExtra("com.google.android.gms.provider.extra.PICK_IMAGES_MAX", this.maxItems);
            }
            else if (PickVisualMedia.Companion.isGmsPickerAvailable$activity_release(context)) {
                final ResolveInfo gmsPicker$activity_release = PickVisualMedia.Companion.getGmsPicker$activity_release(context);
                if (gmsPicker$activity_release == null) {
                    throw new IllegalStateException("Required value was null.".toString());
                }
                final ActivityInfo activityInfo2 = gmsPicker$activity_release.activityInfo;
                intent = new Intent("com.google.android.gms.provider.action.PICK_IMAGES");
                intent.setClassName(activityInfo2.applicationInfo.packageName, activityInfo2.name);
                intent.putExtra("com.google.android.gms.provider.extra.PICK_IMAGES_MAX", this.maxItems);
            }
            else {
                final Intent intent2 = new Intent("android.intent.action.OPEN_DOCUMENT");
                intent2.setType(PickVisualMedia.Companion.getVisualMimeType$activity_release(pickVisualMediaRequest.getMediaType()));
                intent2.putExtra("android.intent.extra.ALLOW_MULTIPLE", true);
                intent = intent2;
                if (intent2.getType() == null) {
                    intent2.setType("*/*");
                    intent2.putExtra("android.intent.extra.MIME_TYPES", new String[] { "image/*", "video/*" });
                    intent = intent2;
                }
            }
            return intent;
        }
        
        public final SynchronousResult<List<Uri>> getSynchronousResult(final Context context, final PickVisualMediaRequest pickVisualMediaRequest) {
            Intrinsics.checkNotNullParameter((Object)context, "context");
            Intrinsics.checkNotNullParameter((Object)pickVisualMediaRequest, "input");
            return null;
        }
        
        @Override
        public final List<Uri> parseResult(int n, Intent intent) {
            if (n == -1) {
                n = 1;
            }
            else {
                n = 0;
            }
            if (n == 0) {
                intent = null;
            }
            if (intent != null) {
                final List<Uri> list = GetMultipleContents.Companion.getClipDataUris$activity_release(intent);
                if (list != null) {
                    return list;
                }
            }
            return CollectionsKt.emptyList();
        }
        
        @Metadata(d1 = { "\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\b\u0080\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\r\u0010\u0003\u001a\u00020\u0004H\u0001¢\u0006\u0002\b\u0005¨\u0006\u0006" }, d2 = { "Landroidx/activity/result/contract/ActivityResultContracts$PickMultipleVisualMedia$Companion;", "", "()V", "getMaxItems", "", "getMaxItems$activity_release", "activity_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
        public static final class Companion
        {
            private Companion() {
            }
            
            public final int getMaxItems$activity_release() {
                int pickImagesMaxLimit;
                if (PickVisualMedia.Companion.isSystemPickerAvailable$activity_release()) {
                    pickImagesMaxLimit = MediaStore.getPickImagesMaxLimit();
                }
                else {
                    pickImagesMaxLimit = Integer.MAX_VALUE;
                }
                return pickImagesMaxLimit;
            }
        }
    }
    
    @Metadata(d1 = { "\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\b\b\u0017\u0018\u0000 \u00102\u0010\u0012\u0004\u0012\u00020\u0002\u0012\u0006\u0012\u0004\u0018\u00010\u00030\u0001:\u0006\u0010\u0011\u0012\u0013\u0014\u0015B\u0005¢\u0006\u0002\u0010\u0004J\u0018\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\u0002H\u0017J \u0010\n\u001a\f\u0012\u0006\u0012\u0004\u0018\u00010\u0003\u0018\u00010\u000b2\u0006\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\u0002J\u001a\u0010\f\u001a\u0004\u0018\u00010\u00032\u0006\u0010\r\u001a\u00020\u000e2\b\u0010\u000f\u001a\u0004\u0018\u00010\u0006¨\u0006\u0016" }, d2 = { "Landroidx/activity/result/contract/ActivityResultContracts$PickVisualMedia;", "Landroidx/activity/result/contract/ActivityResultContract;", "Landroidx/activity/result/PickVisualMediaRequest;", "Landroid/net/Uri;", "()V", "createIntent", "Landroid/content/Intent;", "context", "Landroid/content/Context;", "input", "getSynchronousResult", "Landroidx/activity/result/contract/ActivityResultContract$SynchronousResult;", "parseResult", "resultCode", "", "intent", "Companion", "ImageAndVideo", "ImageOnly", "SingleMimeType", "VideoOnly", "VisualMediaType", "activity_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
    public static class PickVisualMedia extends ActivityResultContract<PickVisualMediaRequest, Uri>
    {
        public static final String ACTION_SYSTEM_FALLBACK_PICK_IMAGES = "androidx.activity.result.contract.action.PICK_IMAGES";
        public static final Companion Companion;
        public static final String EXTRA_SYSTEM_FALLBACK_PICK_IMAGES_MAX = "androidx.activity.result.contract.extra.PICK_IMAGES_MAX";
        public static final String GMS_ACTION_PICK_IMAGES = "com.google.android.gms.provider.action.PICK_IMAGES";
        public static final String GMS_EXTRA_PICK_IMAGES_MAX = "com.google.android.gms.provider.extra.PICK_IMAGES_MAX";
        
        static {
            Companion = new Companion(null);
        }
        
        @JvmStatic
        public static final ResolveInfo getGmsPicker$activity_release(final Context context) {
            return PickVisualMedia.Companion.getGmsPicker$activity_release(context);
        }
        
        @JvmStatic
        public static final ResolveInfo getSystemFallbackPicker$activity_release(final Context context) {
            return PickVisualMedia.Companion.getSystemFallbackPicker$activity_release(context);
        }
        
        @JvmStatic
        public static final boolean isGmsPickerAvailable$activity_release(final Context context) {
            return PickVisualMedia.Companion.isGmsPickerAvailable$activity_release(context);
        }
        
        @Deprecated(message = "This method is deprecated in favor of isPhotoPickerAvailable(context) to support the picker provided by updatable system apps", replaceWith = @ReplaceWith(expression = "isPhotoPickerAvailable(context)", imports = {}))
        @JvmStatic
        public static final boolean isPhotoPickerAvailable() {
            return PickVisualMedia.Companion.isPhotoPickerAvailable();
        }
        
        @JvmStatic
        public static final boolean isPhotoPickerAvailable(final Context context) {
            return PickVisualMedia.Companion.isPhotoPickerAvailable(context);
        }
        
        @JvmStatic
        public static final boolean isSystemFallbackPickerAvailable$activity_release(final Context context) {
            return PickVisualMedia.Companion.isSystemFallbackPickerAvailable$activity_release(context);
        }
        
        @JvmStatic
        public static final boolean isSystemPickerAvailable$activity_release() {
            return PickVisualMedia.Companion.isSystemPickerAvailable$activity_release();
        }
        
        @Override
        public Intent createIntent(final Context context, final PickVisualMediaRequest pickVisualMediaRequest) {
            Intrinsics.checkNotNullParameter((Object)context, "context");
            Intrinsics.checkNotNullParameter((Object)pickVisualMediaRequest, "input");
            final Companion companion = PickVisualMedia.Companion;
            Intent intent;
            if (companion.isSystemPickerAvailable$activity_release()) {
                intent = new Intent("android.provider.action.PICK_IMAGES");
                intent.setType(companion.getVisualMimeType$activity_release(pickVisualMediaRequest.getMediaType()));
            }
            else if (companion.isSystemFallbackPickerAvailable$activity_release(context)) {
                final ResolveInfo systemFallbackPicker$activity_release = companion.getSystemFallbackPicker$activity_release(context);
                if (systemFallbackPicker$activity_release == null) {
                    throw new IllegalStateException("Required value was null.".toString());
                }
                final ActivityInfo activityInfo = systemFallbackPicker$activity_release.activityInfo;
                intent = new Intent("androidx.activity.result.contract.action.PICK_IMAGES");
                intent.setClassName(activityInfo.applicationInfo.packageName, activityInfo.name);
                intent.setType(companion.getVisualMimeType$activity_release(pickVisualMediaRequest.getMediaType()));
            }
            else if (companion.isGmsPickerAvailable$activity_release(context)) {
                final ResolveInfo gmsPicker$activity_release = companion.getGmsPicker$activity_release(context);
                if (gmsPicker$activity_release == null) {
                    throw new IllegalStateException("Required value was null.".toString());
                }
                final ActivityInfo activityInfo2 = gmsPicker$activity_release.activityInfo;
                intent = new Intent("com.google.android.gms.provider.action.PICK_IMAGES");
                intent.setClassName(activityInfo2.applicationInfo.packageName, activityInfo2.name);
                intent.setType(companion.getVisualMimeType$activity_release(pickVisualMediaRequest.getMediaType()));
            }
            else {
                final Intent intent2 = new Intent("android.intent.action.OPEN_DOCUMENT");
                intent2.setType(companion.getVisualMimeType$activity_release(pickVisualMediaRequest.getMediaType()));
                intent = intent2;
                if (intent2.getType() == null) {
                    intent2.setType("*/*");
                    intent2.putExtra("android.intent.extra.MIME_TYPES", new String[] { "image/*", "video/*" });
                    intent = intent2;
                }
            }
            return intent;
        }
        
        public final SynchronousResult<Uri> getSynchronousResult(final Context context, final PickVisualMediaRequest pickVisualMediaRequest) {
            Intrinsics.checkNotNullParameter((Object)context, "context");
            Intrinsics.checkNotNullParameter((Object)pickVisualMediaRequest, "input");
            return null;
        }
        
        @Override
        public final Uri parseResult(int n, final Intent intent) {
            if (n == -1) {
                n = 1;
            }
            else {
                n = 0;
            }
            final Uri uri = null;
            Intent intent2;
            if (n != 0) {
                intent2 = intent;
            }
            else {
                intent2 = null;
            }
            Uri data = uri;
            if (intent2 != null && (data = intent2.getData()) == null) {
                data = (Uri)CollectionsKt.firstOrNull((List)GetMultipleContents.Companion.getClipDataUris$activity_release(intent2));
            }
            return data;
        }
        
        @Metadata(d1 = { "\u00002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0007\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0017\u0010\n\u001a\u0004\u0018\u00010\u000b2\u0006\u0010\f\u001a\u00020\rH\u0001¢\u0006\u0002\b\u000eJ\u0017\u0010\u000f\u001a\u0004\u0018\u00010\u000b2\u0006\u0010\f\u001a\u00020\rH\u0001¢\u0006\u0002\b\u0010J\u0017\u0010\u0011\u001a\u0004\u0018\u00010\u00042\u0006\u0010\u0012\u001a\u00020\u0013H\u0000¢\u0006\u0002\b\u0014J\u0015\u0010\u0015\u001a\u00020\u00162\u0006\u0010\f\u001a\u00020\rH\u0001¢\u0006\u0002\b\u0017J\b\u0010\u0018\u001a\u00020\u0016H\u0007J\u0010\u0010\u0018\u001a\u00020\u00162\u0006\u0010\f\u001a\u00020\rH\u0007J\u0015\u0010\u0019\u001a\u00020\u00162\u0006\u0010\f\u001a\u00020\rH\u0001¢\u0006\u0002\b\u001aJ\r\u0010\u001b\u001a\u00020\u0016H\u0001¢\u0006\u0002\b\u001cR\u0014\u0010\u0003\u001a\u00020\u0004X\u0086T¢\u0006\b\n\u0000\u0012\u0004\b\u0005\u0010\u0002R\u0014\u0010\u0006\u001a\u00020\u0004X\u0086T¢\u0006\b\n\u0000\u0012\u0004\b\u0007\u0010\u0002R\u000e\u0010\b\u001a\u00020\u0004X\u0080T¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\u0004X\u0080T¢\u0006\u0002\n\u0000¨\u0006\u001d" }, d2 = { "Landroidx/activity/result/contract/ActivityResultContracts$PickVisualMedia$Companion;", "", "()V", "ACTION_SYSTEM_FALLBACK_PICK_IMAGES", "", "getACTION_SYSTEM_FALLBACK_PICK_IMAGES$annotations", "EXTRA_SYSTEM_FALLBACK_PICK_IMAGES_MAX", "getEXTRA_SYSTEM_FALLBACK_PICK_IMAGES_MAX$annotations", "GMS_ACTION_PICK_IMAGES", "GMS_EXTRA_PICK_IMAGES_MAX", "getGmsPicker", "Landroid/content/pm/ResolveInfo;", "context", "Landroid/content/Context;", "getGmsPicker$activity_release", "getSystemFallbackPicker", "getSystemFallbackPicker$activity_release", "getVisualMimeType", "input", "Landroidx/activity/result/contract/ActivityResultContracts$PickVisualMedia$VisualMediaType;", "getVisualMimeType$activity_release", "isGmsPickerAvailable", "", "isGmsPickerAvailable$activity_release", "isPhotoPickerAvailable", "isSystemFallbackPickerAvailable", "isSystemFallbackPickerAvailable$activity_release", "isSystemPickerAvailable", "isSystemPickerAvailable$activity_release", "activity_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
        public static final class Companion
        {
            private Companion() {
            }
            
            @JvmStatic
            public final ResolveInfo getGmsPicker$activity_release(final Context context) {
                Intrinsics.checkNotNullParameter((Object)context, "context");
                return context.getPackageManager().resolveActivity(new Intent("com.google.android.gms.provider.action.PICK_IMAGES"), 1114112);
            }
            
            @JvmStatic
            public final ResolveInfo getSystemFallbackPicker$activity_release(final Context context) {
                Intrinsics.checkNotNullParameter((Object)context, "context");
                return context.getPackageManager().resolveActivity(new Intent("androidx.activity.result.contract.action.PICK_IMAGES"), 1114112);
            }
            
            public final String getVisualMimeType$activity_release(final VisualMediaType visualMediaType) {
                Intrinsics.checkNotNullParameter((Object)visualMediaType, "input");
                String mimeType;
                if (visualMediaType instanceof ImageOnly) {
                    mimeType = "image/*";
                }
                else if (visualMediaType instanceof VideoOnly) {
                    mimeType = "video/*";
                }
                else if (visualMediaType instanceof SingleMimeType) {
                    mimeType = ((SingleMimeType)visualMediaType).getMimeType();
                }
                else {
                    if (!(visualMediaType instanceof ImageAndVideo)) {
                        throw new NoWhenBranchMatchedException();
                    }
                    mimeType = null;
                }
                return mimeType;
            }
            
            @JvmStatic
            public final boolean isGmsPickerAvailable$activity_release(final Context context) {
                Intrinsics.checkNotNullParameter((Object)context, "context");
                return this.getGmsPicker$activity_release(context) != null;
            }
            
            @Deprecated(message = "This method is deprecated in favor of isPhotoPickerAvailable(context) to support the picker provided by updatable system apps", replaceWith = @ReplaceWith(expression = "isPhotoPickerAvailable(context)", imports = {}))
            @JvmStatic
            public final boolean isPhotoPickerAvailable() {
                return this.isSystemPickerAvailable$activity_release();
            }
            
            @JvmStatic
            public final boolean isPhotoPickerAvailable(final Context context) {
                Intrinsics.checkNotNullParameter((Object)context, "context");
                return this.isSystemPickerAvailable$activity_release() || this.isSystemFallbackPickerAvailable$activity_release(context) || this.isGmsPickerAvailable$activity_release(context);
            }
            
            @JvmStatic
            public final boolean isSystemFallbackPickerAvailable$activity_release(final Context context) {
                Intrinsics.checkNotNullParameter((Object)context, "context");
                return this.getSystemFallbackPicker$activity_release(context) != null;
            }
            
            @JvmStatic
            public final boolean isSystemPickerAvailable$activity_release() {
                final int sdk_INT = Build$VERSION.SDK_INT;
                final boolean b = false;
                if (sdk_INT < 33) {
                    boolean b2 = b;
                    if (Build$VERSION.SDK_INT < 30) {
                        return b2;
                    }
                    b2 = b;
                    if (SdkExtensions.getExtensionVersion(30) < 2) {
                        return b2;
                    }
                }
                return true;
            }
        }
        
        @Metadata(d1 = { "\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002¨\u0006\u0003" }, d2 = { "Landroidx/activity/result/contract/ActivityResultContracts$PickVisualMedia$ImageAndVideo;", "Landroidx/activity/result/contract/ActivityResultContracts$PickVisualMedia$VisualMediaType;", "()V", "activity_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
        public static final class ImageAndVideo implements VisualMediaType
        {
            public static final ImageAndVideo INSTANCE;
            
            static {
                INSTANCE = new ImageAndVideo();
            }
            
            private ImageAndVideo() {
            }
        }
        
        @Metadata(d1 = { "\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002¨\u0006\u0003" }, d2 = { "Landroidx/activity/result/contract/ActivityResultContracts$PickVisualMedia$ImageOnly;", "Landroidx/activity/result/contract/ActivityResultContracts$PickVisualMedia$VisualMediaType;", "()V", "activity_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
        public static final class ImageOnly implements VisualMediaType
        {
            public static final ImageOnly INSTANCE;
            
            static {
                INSTANCE = new ImageOnly();
            }
            
            private ImageOnly() {
            }
        }
        
        @Metadata(d1 = { "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0004\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006¨\u0006\u0007" }, d2 = { "Landroidx/activity/result/contract/ActivityResultContracts$PickVisualMedia$SingleMimeType;", "Landroidx/activity/result/contract/ActivityResultContracts$PickVisualMedia$VisualMediaType;", "mimeType", "", "(Ljava/lang/String;)V", "getMimeType", "()Ljava/lang/String;", "activity_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
        public static final class SingleMimeType implements VisualMediaType
        {
            private final String mimeType;
            
            public SingleMimeType(final String mimeType) {
                Intrinsics.checkNotNullParameter((Object)mimeType, "mimeType");
                this.mimeType = mimeType;
            }
            
            public final String getMimeType() {
                return this.mimeType;
            }
        }
        
        @Metadata(d1 = { "\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002¨\u0006\u0003" }, d2 = { "Landroidx/activity/result/contract/ActivityResultContracts$PickVisualMedia$VideoOnly;", "Landroidx/activity/result/contract/ActivityResultContracts$PickVisualMedia$VisualMediaType;", "()V", "activity_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
        public static final class VideoOnly implements VisualMediaType
        {
            public static final VideoOnly INSTANCE;
            
            static {
                INSTANCE = new VideoOnly();
            }
            
            private VideoOnly() {
            }
        }
        
        @Metadata(d1 = { "\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\bv\u0018\u00002\u00020\u0001\u0082\u0001\u0004\u0002\u0003\u0004\u0005\u00f8\u0001\u0000\u0082\u0002\u0006\n\u0004\b!0\u0001¨\u0006\u0006\u00c0\u0006\u0001" }, d2 = { "Landroidx/activity/result/contract/ActivityResultContracts$PickVisualMedia$VisualMediaType;", "", "Landroidx/activity/result/contract/ActivityResultContracts$PickVisualMedia$ImageAndVideo;", "Landroidx/activity/result/contract/ActivityResultContracts$PickVisualMedia$ImageOnly;", "Landroidx/activity/result/contract/ActivityResultContracts$PickVisualMedia$SingleMimeType;", "Landroidx/activity/result/contract/ActivityResultContracts$PickVisualMedia$VideoOnly;", "activity_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
        public interface VisualMediaType
        {
        }
    }
    
    @Metadata(d1 = { "\u0000>\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0011\n\u0002\u0010\u000e\n\u0002\u0010$\n\u0002\u0010\u000b\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0003\u0018\u0000 \u00152%\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00030\u0002\u0012\u0015\u0012\u0013\u0012\u0004\u0012\u00020\u0003\u0012\t\u0012\u00070\u0005¢\u0006\u0002\b\u00060\u00040\u0001:\u0001\u0015B\u0005¢\u0006\u0002\u0010\u0007J#\u0010\b\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000b2\f\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002H\u0016¢\u0006\u0002\u0010\rJ7\u0010\u000e\u001a\u0016\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00050\u0004\u0018\u00010\u000f2\u0006\u0010\n\u001a\u00020\u000b2\f\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002H\u0016¢\u0006\u0002\u0010\u0010J&\u0010\u0011\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00050\u00042\u0006\u0010\u0012\u001a\u00020\u00132\b\u0010\u0014\u001a\u0004\u0018\u00010\tH\u0016¨\u0006\u0016" }, d2 = { "Landroidx/activity/result/contract/ActivityResultContracts$RequestMultiplePermissions;", "Landroidx/activity/result/contract/ActivityResultContract;", "", "", "", "", "Lkotlin/jvm/JvmSuppressWildcards;", "()V", "createIntent", "Landroid/content/Intent;", "context", "Landroid/content/Context;", "input", "(Landroid/content/Context;[Ljava/lang/String;)Landroid/content/Intent;", "getSynchronousResult", "Landroidx/activity/result/contract/ActivityResultContract$SynchronousResult;", "(Landroid/content/Context;[Ljava/lang/String;)Landroidx/activity/result/contract/ActivityResultContract$SynchronousResult;", "parseResult", "resultCode", "", "intent", "Companion", "activity_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
    public static final class RequestMultiplePermissions extends ActivityResultContract<String[], Map<String, Boolean>>
    {
        public static final String ACTION_REQUEST_PERMISSIONS = "androidx.activity.result.contract.action.REQUEST_PERMISSIONS";
        public static final Companion Companion;
        public static final String EXTRA_PERMISSIONS = "androidx.activity.result.contract.extra.PERMISSIONS";
        public static final String EXTRA_PERMISSION_GRANT_RESULTS = "androidx.activity.result.contract.extra.PERMISSION_GRANT_RESULTS";
        
        static {
            Companion = new Companion(null);
        }
        
        @Override
        public Intent createIntent(final Context context, final String[] array) {
            Intrinsics.checkNotNullParameter((Object)context, "context");
            Intrinsics.checkNotNullParameter((Object)array, "input");
            return RequestMultiplePermissions.Companion.createIntent$activity_release(array);
        }
        
        public SynchronousResult<Map<String, Boolean>> getSynchronousResult(final Context context, final String[] array) {
            Intrinsics.checkNotNullParameter((Object)context, "context");
            Intrinsics.checkNotNullParameter((Object)array, "input");
            final int length = array.length;
            final int n = 0;
            if (length == 0) {
                return (SynchronousResult<Map<String, Boolean>>)new SynchronousResult(MapsKt.emptyMap());
            }
            while (true) {
                for (int length2 = array.length, i = 0; i < length2; ++i) {
                    if (ContextCompat.checkSelfPermission(context, array[i]) != 0) {
                        final boolean b = false;
                        SynchronousResult<Map<String, Boolean>> synchronousResult;
                        if (b) {
                            final Map map = new LinkedHashMap(RangesKt.coerceAtLeast(MapsKt.mapCapacity(array.length), 16));
                            for (int length3 = array.length, j = n; j < length3; ++j) {
                                final Pair to = TuplesKt.to((Object)array[j], (Object)true);
                                map.put(to.getFirst(), to.getSecond());
                            }
                            synchronousResult = (SynchronousResult<Map<String, Boolean>>)new SynchronousResult(map);
                        }
                        else {
                            synchronousResult = null;
                        }
                        return synchronousResult;
                    }
                }
                final boolean b = true;
                continue;
            }
        }
        
        @Override
        public Map<String, Boolean> parseResult(int i, final Intent intent) {
            if (i != -1) {
                return MapsKt.emptyMap();
            }
            if (intent == null) {
                return MapsKt.emptyMap();
            }
            final String[] stringArrayExtra = intent.getStringArrayExtra("androidx.activity.result.contract.extra.PERMISSIONS");
            final int[] intArrayExtra = intent.getIntArrayExtra("androidx.activity.result.contract.extra.PERMISSION_GRANT_RESULTS");
            if (intArrayExtra != null && stringArrayExtra != null) {
                final Collection collection = new ArrayList(intArrayExtra.length);
                int length;
                for (length = intArrayExtra.length, i = 0; i < length; ++i) {
                    collection.add(intArrayExtra[i] == 0);
                }
                return MapsKt.toMap((Iterable)CollectionsKt.zip((Iterable)ArraysKt.filterNotNull((Object[])stringArrayExtra), (Iterable)collection));
            }
            return MapsKt.emptyMap();
        }
        
        @Metadata(d1 = { "\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0011\n\u0002\b\u0003\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u001d\u0010\u0007\u001a\u00020\b2\f\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u00040\nH\u0000¢\u0006\u0004\b\u000b\u0010\fR\u000e\u0010\u0003\u001a\u00020\u0004X\u0086T¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0086T¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004X\u0086T¢\u0006\u0002\n\u0000¨\u0006\r" }, d2 = { "Landroidx/activity/result/contract/ActivityResultContracts$RequestMultiplePermissions$Companion;", "", "()V", "ACTION_REQUEST_PERMISSIONS", "", "EXTRA_PERMISSIONS", "EXTRA_PERMISSION_GRANT_RESULTS", "createIntent", "Landroid/content/Intent;", "input", "", "createIntent$activity_release", "([Ljava/lang/String;)Landroid/content/Intent;", "activity_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
        public static final class Companion
        {
            private Companion() {
            }
            
            public final Intent createIntent$activity_release(final String[] array) {
                Intrinsics.checkNotNullParameter((Object)array, "input");
                final Intent putExtra = new Intent("androidx.activity.result.contract.action.REQUEST_PERMISSIONS").putExtra("androidx.activity.result.contract.extra.PERMISSIONS", array);
                Intrinsics.checkNotNullExpressionValue((Object)putExtra, "Intent(ACTION_REQUEST_PE\u2026EXTRA_PERMISSIONS, input)");
                return putExtra;
            }
        }
    }
    
    @Metadata(d1 = { "\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001B\u0005¢\u0006\u0002\u0010\u0004J\u0018\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\u0002H\u0016J \u0010\n\u001a\n\u0012\u0004\u0012\u00020\u0003\u0018\u00010\u000b2\u0006\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\u0002H\u0016J\u001f\u0010\f\u001a\u00020\u00032\u0006\u0010\r\u001a\u00020\u000e2\b\u0010\u000f\u001a\u0004\u0018\u00010\u0006H\u0016¢\u0006\u0002\u0010\u0010¨\u0006\u0011" }, d2 = { "Landroidx/activity/result/contract/ActivityResultContracts$RequestPermission;", "Landroidx/activity/result/contract/ActivityResultContract;", "", "", "()V", "createIntent", "Landroid/content/Intent;", "context", "Landroid/content/Context;", "input", "getSynchronousResult", "Landroidx/activity/result/contract/ActivityResultContract$SynchronousResult;", "parseResult", "resultCode", "", "intent", "(ILandroid/content/Intent;)Ljava/lang/Boolean;", "activity_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
    public static final class RequestPermission extends ActivityResultContract<String, Boolean>
    {
        @Override
        public Intent createIntent(final Context context, final String s) {
            Intrinsics.checkNotNullParameter((Object)context, "context");
            Intrinsics.checkNotNullParameter((Object)s, "input");
            return RequestMultiplePermissions.Companion.createIntent$activity_release(new String[] { s });
        }
        
        public SynchronousResult<Boolean> getSynchronousResult(final Context context, final String s) {
            Intrinsics.checkNotNullParameter((Object)context, "context");
            Intrinsics.checkNotNullParameter((Object)s, "input");
            SynchronousResult<Boolean> synchronousResult;
            if (ContextCompat.checkSelfPermission(context, s) == 0) {
                synchronousResult = (SynchronousResult<Boolean>)new SynchronousResult(true);
            }
            else {
                synchronousResult = null;
            }
            return synchronousResult;
        }
        
        @Override
        public Boolean parseResult(int i, final Intent intent) {
            final boolean b = false;
            if (intent != null && i == -1) {
                final int[] intArrayExtra = intent.getIntArrayExtra("androidx.activity.result.contract.extra.PERMISSION_GRANT_RESULTS");
                boolean b2 = b;
                if (intArrayExtra != null) {
                    final int length = intArrayExtra.length;
                    i = 0;
                    while (true) {
                        while (i < length) {
                            if (intArrayExtra[i] == 0) {
                                i = 1;
                                b2 = b;
                                if (i == 1) {
                                    b2 = true;
                                    return b2;
                                }
                                return b2;
                            }
                            else {
                                ++i;
                            }
                        }
                        i = 0;
                        continue;
                    }
                }
                return b2;
            }
            return false;
        }
    }
    
    @Metadata(d1 = { "\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0003\u0018\u0000 \r2\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001:\u0001\rB\u0005¢\u0006\u0002\u0010\u0004J\u0018\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0006\u001a\u00020\u00072\u0006\u0010\b\u001a\u00020\u0002H\u0016J\u001a\u0010\t\u001a\u00020\u00032\u0006\u0010\n\u001a\u00020\u000b2\b\u0010\f\u001a\u0004\u0018\u00010\u0002H\u0016¨\u0006\u000e" }, d2 = { "Landroidx/activity/result/contract/ActivityResultContracts$StartActivityForResult;", "Landroidx/activity/result/contract/ActivityResultContract;", "Landroid/content/Intent;", "Landroidx/activity/result/ActivityResult;", "()V", "createIntent", "context", "Landroid/content/Context;", "input", "parseResult", "resultCode", "", "intent", "Companion", "activity_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
    public static final class StartActivityForResult extends ActivityResultContract<Intent, ActivityResult>
    {
        public static final Companion Companion;
        public static final String EXTRA_ACTIVITY_OPTIONS_BUNDLE = "androidx.activity.result.contract.extra.ACTIVITY_OPTIONS_BUNDLE";
        
        static {
            Companion = new Companion(null);
        }
        
        @Override
        public Intent createIntent(final Context context, final Intent intent) {
            Intrinsics.checkNotNullParameter((Object)context, "context");
            Intrinsics.checkNotNullParameter((Object)intent, "input");
            return intent;
        }
        
        @Override
        public ActivityResult parseResult(final int n, final Intent intent) {
            return new ActivityResult(n, intent);
        }
        
        @Metadata(d1 = { "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0086T¢\u0006\u0002\n\u0000¨\u0006\u0005" }, d2 = { "Landroidx/activity/result/contract/ActivityResultContracts$StartActivityForResult$Companion;", "", "()V", "EXTRA_ACTIVITY_OPTIONS_BUNDLE", "", "activity_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
        public static final class Companion
        {
            private Companion() {
            }
        }
    }
    
    @Metadata(d1 = { "\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0003\u0018\u0000 \u000e2\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001:\u0001\u000eB\u0005¢\u0006\u0002\u0010\u0004J\u0018\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\u0002H\u0016J\u001a\u0010\n\u001a\u00020\u00032\u0006\u0010\u000b\u001a\u00020\f2\b\u0010\r\u001a\u0004\u0018\u00010\u0006H\u0016¨\u0006\u000f" }, d2 = { "Landroidx/activity/result/contract/ActivityResultContracts$StartIntentSenderForResult;", "Landroidx/activity/result/contract/ActivityResultContract;", "Landroidx/activity/result/IntentSenderRequest;", "Landroidx/activity/result/ActivityResult;", "()V", "createIntent", "Landroid/content/Intent;", "context", "Landroid/content/Context;", "input", "parseResult", "resultCode", "", "intent", "Companion", "activity_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
    public static final class StartIntentSenderForResult extends ActivityResultContract<IntentSenderRequest, ActivityResult>
    {
        public static final String ACTION_INTENT_SENDER_REQUEST = "androidx.activity.result.contract.action.INTENT_SENDER_REQUEST";
        public static final Companion Companion;
        public static final String EXTRA_INTENT_SENDER_REQUEST = "androidx.activity.result.contract.extra.INTENT_SENDER_REQUEST";
        public static final String EXTRA_SEND_INTENT_EXCEPTION = "androidx.activity.result.contract.extra.SEND_INTENT_EXCEPTION";
        
        static {
            Companion = new Companion(null);
        }
        
        @Override
        public Intent createIntent(final Context context, final IntentSenderRequest intentSenderRequest) {
            Intrinsics.checkNotNullParameter((Object)context, "context");
            Intrinsics.checkNotNullParameter((Object)intentSenderRequest, "input");
            final Intent putExtra = new Intent("androidx.activity.result.contract.action.INTENT_SENDER_REQUEST").putExtra("androidx.activity.result.contract.extra.INTENT_SENDER_REQUEST", (Parcelable)intentSenderRequest);
            Intrinsics.checkNotNullExpressionValue((Object)putExtra, "Intent(ACTION_INTENT_SEN\u2026NT_SENDER_REQUEST, input)");
            return putExtra;
        }
        
        @Override
        public ActivityResult parseResult(final int n, final Intent intent) {
            return new ActivityResult(n, intent);
        }
        
        @Metadata(d1 = { "\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0086T¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0086T¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004X\u0086T¢\u0006\u0002\n\u0000¨\u0006\u0007" }, d2 = { "Landroidx/activity/result/contract/ActivityResultContracts$StartIntentSenderForResult$Companion;", "", "()V", "ACTION_INTENT_SENDER_REQUEST", "", "EXTRA_INTENT_SENDER_REQUEST", "EXTRA_SEND_INTENT_EXCEPTION", "activity_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
        public static final class Companion
        {
            private Companion() {
            }
        }
    }
    
    @Metadata(d1 = { "\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\b\u0016\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001B\u0005¢\u0006\u0002\u0010\u0004J\u0018\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\u0002H\u0017J\u001e\u0010\n\u001a\n\u0012\u0004\u0012\u00020\u0003\u0018\u00010\u000b2\u0006\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\u0002J\u001d\u0010\f\u001a\u00020\u00032\u0006\u0010\r\u001a\u00020\u000e2\b\u0010\u000f\u001a\u0004\u0018\u00010\u0006¢\u0006\u0002\u0010\u0010¨\u0006\u0011" }, d2 = { "Landroidx/activity/result/contract/ActivityResultContracts$TakePicture;", "Landroidx/activity/result/contract/ActivityResultContract;", "Landroid/net/Uri;", "", "()V", "createIntent", "Landroid/content/Intent;", "context", "Landroid/content/Context;", "input", "getSynchronousResult", "Landroidx/activity/result/contract/ActivityResultContract$SynchronousResult;", "parseResult", "resultCode", "", "intent", "(ILandroid/content/Intent;)Ljava/lang/Boolean;", "activity_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
    public static class TakePicture extends ActivityResultContract<Uri, Boolean>
    {
        @Override
        public Intent createIntent(final Context context, final Uri uri) {
            Intrinsics.checkNotNullParameter((Object)context, "context");
            Intrinsics.checkNotNullParameter((Object)uri, "input");
            final Intent putExtra = new Intent("android.media.action.IMAGE_CAPTURE").putExtra("output", (Parcelable)uri);
            Intrinsics.checkNotNullExpressionValue((Object)putExtra, "Intent(MediaStore.ACTION\u2026tore.EXTRA_OUTPUT, input)");
            return putExtra;
        }
        
        public final SynchronousResult<Boolean> getSynchronousResult(final Context context, final Uri uri) {
            Intrinsics.checkNotNullParameter((Object)context, "context");
            Intrinsics.checkNotNullParameter((Object)uri, "input");
            return null;
        }
        
        @Override
        public final Boolean parseResult(final int n, final Intent intent) {
            return n == -1;
        }
    }
    
    @Metadata(d1 = { "\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\b\u0016\u0018\u00002\u0012\u0012\u0006\u0012\u0004\u0018\u00010\u0002\u0012\u0006\u0012\u0004\u0018\u00010\u00030\u0001B\u0005¢\u0006\u0002\u0010\u0004J\u001a\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b2\b\u0010\t\u001a\u0004\u0018\u00010\u0002H\u0017J\"\u0010\n\u001a\f\u0012\u0006\u0012\u0004\u0018\u00010\u0003\u0018\u00010\u000b2\u0006\u0010\u0007\u001a\u00020\b2\b\u0010\t\u001a\u0004\u0018\u00010\u0002J\u001a\u0010\f\u001a\u0004\u0018\u00010\u00032\u0006\u0010\r\u001a\u00020\u000e2\b\u0010\u000f\u001a\u0004\u0018\u00010\u0006¨\u0006\u0010" }, d2 = { "Landroidx/activity/result/contract/ActivityResultContracts$TakePicturePreview;", "Landroidx/activity/result/contract/ActivityResultContract;", "Ljava/lang/Void;", "Landroid/graphics/Bitmap;", "()V", "createIntent", "Landroid/content/Intent;", "context", "Landroid/content/Context;", "input", "getSynchronousResult", "Landroidx/activity/result/contract/ActivityResultContract$SynchronousResult;", "parseResult", "resultCode", "", "intent", "activity_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
    public static class TakePicturePreview extends ActivityResultContract<Void, Bitmap>
    {
        @Override
        public Intent createIntent(final Context context, final Void void1) {
            Intrinsics.checkNotNullParameter((Object)context, "context");
            return new Intent("android.media.action.IMAGE_CAPTURE");
        }
        
        public final SynchronousResult<Bitmap> getSynchronousResult(final Context context, final Void void1) {
            Intrinsics.checkNotNullParameter((Object)context, "context");
            return null;
        }
        
        @Override
        public final Bitmap parseResult(int n, Intent intent) {
            if (n == -1) {
                n = 1;
            }
            else {
                n = 0;
            }
            Bitmap bitmap = null;
            if (n == 0) {
                intent = null;
            }
            if (intent != null) {
                bitmap = (Bitmap)intent.getParcelableExtra("data");
            }
            return bitmap;
        }
    }
    
    @Deprecated(message = "The thumbnail bitmap is rarely returned and is not a good signal to determine\n      whether the video was actually successfully captured. Use {@link CaptureVideo} instead.")
    @Metadata(d1 = { "\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\b\u0017\u0018\u00002\u0010\u0012\u0004\u0012\u00020\u0002\u0012\u0006\u0012\u0004\u0018\u00010\u00030\u0001B\u0005¢\u0006\u0002\u0010\u0004J\u0018\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\u0002H\u0017J \u0010\n\u001a\f\u0012\u0006\u0012\u0004\u0018\u00010\u0003\u0018\u00010\u000b2\u0006\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\u0002J\u001a\u0010\f\u001a\u0004\u0018\u00010\u00032\u0006\u0010\r\u001a\u00020\u000e2\b\u0010\u000f\u001a\u0004\u0018\u00010\u0006¨\u0006\u0010" }, d2 = { "Landroidx/activity/result/contract/ActivityResultContracts$TakeVideo;", "Landroidx/activity/result/contract/ActivityResultContract;", "Landroid/net/Uri;", "Landroid/graphics/Bitmap;", "()V", "createIntent", "Landroid/content/Intent;", "context", "Landroid/content/Context;", "input", "getSynchronousResult", "Landroidx/activity/result/contract/ActivityResultContract$SynchronousResult;", "parseResult", "resultCode", "", "intent", "activity_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
    public static class TakeVideo extends ActivityResultContract<Uri, Bitmap>
    {
        @Override
        public Intent createIntent(final Context context, final Uri uri) {
            Intrinsics.checkNotNullParameter((Object)context, "context");
            Intrinsics.checkNotNullParameter((Object)uri, "input");
            final Intent putExtra = new Intent("android.media.action.VIDEO_CAPTURE").putExtra("output", (Parcelable)uri);
            Intrinsics.checkNotNullExpressionValue((Object)putExtra, "Intent(MediaStore.ACTION\u2026tore.EXTRA_OUTPUT, input)");
            return putExtra;
        }
        
        public final SynchronousResult<Bitmap> getSynchronousResult(final Context context, final Uri uri) {
            Intrinsics.checkNotNullParameter((Object)context, "context");
            Intrinsics.checkNotNullParameter((Object)uri, "input");
            return null;
        }
        
        @Override
        public final Bitmap parseResult(int n, Intent intent) {
            if (n == -1) {
                n = 1;
            }
            else {
                n = 0;
            }
            Bitmap bitmap = null;
            if (n == 0) {
                intent = null;
            }
            if (intent != null) {
                bitmap = (Bitmap)intent.getParcelableExtra("data");
            }
            return bitmap;
        }
    }
}

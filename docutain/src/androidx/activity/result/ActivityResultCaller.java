// 
// Decompiled by Procyon v0.6.0
// 

package androidx.activity.result;

import androidx.activity.result.contract.ActivityResultContract;

public interface ActivityResultCaller
{
     <I, O> ActivityResultLauncher<I> registerForActivityResult(final ActivityResultContract<I, O> p0, final ActivityResultCallback<O> p1);
    
     <I, O> ActivityResultLauncher<I> registerForActivityResult(final ActivityResultContract<I, O> p0, final ActivityResultRegistry p1, final ActivityResultCallback<O> p2);
}

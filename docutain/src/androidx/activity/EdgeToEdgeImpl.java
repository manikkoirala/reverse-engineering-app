// 
// Decompiled by Procyon v0.6.0
// 

package androidx.activity;

import android.view.View;
import android.view.Window;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\bb\u0018\u00002\u00020\u0001J8\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u00052\u0006\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\fH&\u00f8\u0001\u0000\u0082\u0002\u0006\n\u0004\b!0\u0001¨\u0006\u000e\u00c0\u0006\u0001" }, d2 = { "Landroidx/activity/EdgeToEdgeImpl;", "", "setUp", "", "statusBarStyle", "Landroidx/activity/SystemBarStyle;", "navigationBarStyle", "window", "Landroid/view/Window;", "view", "Landroid/view/View;", "statusBarIsDark", "", "navigationBarIsDark", "activity_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
interface EdgeToEdgeImpl
{
    void setUp(final SystemBarStyle p0, final SystemBarStyle p1, final Window p2, final View p3, final boolean p4, final boolean p5);
}

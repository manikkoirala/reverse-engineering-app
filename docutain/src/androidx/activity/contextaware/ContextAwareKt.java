// 
// Decompiled by Procyon v0.6.0
// 

package androidx.activity.contextaware;

import kotlin.Unit;
import kotlin.jvm.internal.InlineMarker;
import kotlin.coroutines.jvm.internal.DebugProbesKt;
import kotlinx.coroutines.CancellableContinuation;
import kotlinx.coroutines.CancellableContinuationImpl;
import kotlin.coroutines.intrinsics.IntrinsicsKt;
import kotlin.coroutines.Continuation;
import android.content.Context;
import kotlin.jvm.functions.Function1;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000\u001a\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\u001a@\u0010\u0000\u001a\u0007H\u0001¢\u0006\u0002\b\u0002\"\u0004\b\u0000\u0010\u0001*\u00020\u00032\u001e\b\u0004\u0010\u0004\u001a\u0018\u0012\t\u0012\u00070\u0006¢\u0006\u0002\b\u0002\u0012\t\u0012\u0007H\u0001¢\u0006\u0002\b\u00020\u0005H\u0086H\u00f8\u0001\u0000¢\u0006\u0002\u0010\u0007\u0082\u0002\u0004\n\u0002\b\u0019¨\u0006\b" }, d2 = { "withContextAvailable", "R", "Lkotlin/jvm/JvmSuppressWildcards;", "Landroidx/activity/contextaware/ContextAware;", "onContextAvailable", "Lkotlin/Function1;", "Landroid/content/Context;", "(Landroidx/activity/contextaware/ContextAware;Lkotlin/jvm/functions/Function1;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "activity_release" }, k = 2, mv = { 1, 8, 0 }, xi = 48)
public final class ContextAwareKt
{
    public static final <R> Object withContextAvailable(final ContextAware contextAware, final Function1<Context, R> function1, final Continuation<R> continuation) {
        final Context peekAvailableContext = contextAware.peekAvailableContext();
        if (peekAvailableContext != null) {
            return function1.invoke((Object)peekAvailableContext);
        }
        final CancellableContinuationImpl cancellableContinuationImpl = new CancellableContinuationImpl(IntrinsicsKt.intercepted((Continuation)continuation), 1);
        cancellableContinuationImpl.initCancellability();
        final CancellableContinuation cancellableContinuation = (CancellableContinuation)cancellableContinuationImpl;
        final ContextAwareKt$withContextAvailable$2$listener.ContextAwareKt$withContextAvailable$2$listener$1 contextAwareKt$withContextAvailable$2$listener$1 = new ContextAwareKt$withContextAvailable$2$listener.ContextAwareKt$withContextAvailable$2$listener$1(cancellableContinuation, (Function1)function1);
        contextAware.addOnContextAvailableListener((OnContextAvailableListener)contextAwareKt$withContextAvailable$2$listener$1);
        cancellableContinuation.invokeOnCancellation((Function1)new ContextAwareKt$withContextAvailable$2.ContextAwareKt$withContextAvailable$2$1(contextAware, contextAwareKt$withContextAvailable$2$listener$1));
        final Object result = cancellableContinuationImpl.getResult();
        if (result == IntrinsicsKt.getCOROUTINE_SUSPENDED()) {
            DebugProbesKt.probeCoroutineSuspended((Continuation)continuation);
        }
        return result;
    }
    
    private static final <R> Object withContextAvailable$$forInline(final ContextAware contextAware, final Function1<Context, R> function1, final Continuation<R> continuation) {
        final Context peekAvailableContext = contextAware.peekAvailableContext();
        if (peekAvailableContext != null) {
            return function1.invoke((Object)peekAvailableContext);
        }
        InlineMarker.mark(0);
        final CancellableContinuationImpl cancellableContinuationImpl = new CancellableContinuationImpl(IntrinsicsKt.intercepted((Continuation)continuation), 1);
        cancellableContinuationImpl.initCancellability();
        final CancellableContinuation cancellableContinuation = (CancellableContinuation)cancellableContinuationImpl;
        final ContextAwareKt$withContextAvailable$2$listener.ContextAwareKt$withContextAvailable$2$listener$1 contextAwareKt$withContextAvailable$2$listener$1 = new ContextAwareKt$withContextAvailable$2$listener.ContextAwareKt$withContextAvailable$2$listener$1(cancellableContinuation, (Function1)function1);
        contextAware.addOnContextAvailableListener((OnContextAvailableListener)contextAwareKt$withContextAvailable$2$listener$1);
        cancellableContinuation.invokeOnCancellation((Function1)new ContextAwareKt$withContextAvailable$2.ContextAwareKt$withContextAvailable$2$1(contextAware, contextAwareKt$withContextAvailable$2$listener$1));
        final Unit instance = Unit.INSTANCE;
        final Object result = cancellableContinuationImpl.getResult();
        if (result == IntrinsicsKt.getCOROUTINE_SUSPENDED()) {
            DebugProbesKt.probeCoroutineSuspended((Continuation)continuation);
        }
        InlineMarker.mark(1);
        return result;
    }
}

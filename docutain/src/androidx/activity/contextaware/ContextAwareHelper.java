// 
// Decompiled by Procyon v0.6.0
// 

package androidx.activity.contextaware;

import java.util.Iterator;
import kotlin.jvm.internal.Intrinsics;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.Set;
import android.content.Context;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010#\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0006\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\u000e\u0010\b\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u0007J\u0006\u0010\u000b\u001a\u00020\tJ\u000e\u0010\f\u001a\u00020\t2\u0006\u0010\u0003\u001a\u00020\u0004J\b\u0010\r\u001a\u0004\u0018\u00010\u0004J\u000e\u0010\u000e\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u0007R\u0010\u0010\u0003\u001a\u0004\u0018\u00010\u0004X\u0082\u000e¢\u0006\u0002\n\u0000R\u0014\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u000f" }, d2 = { "Landroidx/activity/contextaware/ContextAwareHelper;", "", "()V", "context", "Landroid/content/Context;", "listeners", "", "Landroidx/activity/contextaware/OnContextAvailableListener;", "addOnContextAvailableListener", "", "listener", "clearAvailableContext", "dispatchOnContextAvailable", "peekAvailableContext", "removeOnContextAvailableListener", "activity_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
public final class ContextAwareHelper
{
    private volatile Context context;
    private final Set<OnContextAvailableListener> listeners;
    
    public ContextAwareHelper() {
        this.listeners = new CopyOnWriteArraySet<OnContextAvailableListener>();
    }
    
    public final void addOnContextAvailableListener(final OnContextAvailableListener onContextAvailableListener) {
        Intrinsics.checkNotNullParameter((Object)onContextAvailableListener, "listener");
        final Context context = this.context;
        if (context != null) {
            onContextAvailableListener.onContextAvailable(context);
        }
        this.listeners.add(onContextAvailableListener);
    }
    
    public final void clearAvailableContext() {
        this.context = null;
    }
    
    public final void dispatchOnContextAvailable(final Context context) {
        Intrinsics.checkNotNullParameter((Object)context, "context");
        this.context = context;
        final Iterator<OnContextAvailableListener> iterator = this.listeners.iterator();
        while (iterator.hasNext()) {
            iterator.next().onContextAvailable(context);
        }
    }
    
    public final Context peekAvailableContext() {
        return this.context;
    }
    
    public final void removeOnContextAvailableListener(final OnContextAvailableListener onContextAvailableListener) {
        Intrinsics.checkNotNullParameter((Object)onContextAvailableListener, "listener");
        this.listeners.remove(onContextAvailableListener);
    }
}

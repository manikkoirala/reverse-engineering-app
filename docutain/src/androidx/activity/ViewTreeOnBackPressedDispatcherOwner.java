// 
// Decompiled by Procyon v0.6.0
// 

package androidx.activity;

import kotlin.sequences.SequencesKt;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import android.view.View;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\u001a\u0013\u0010\u0000\u001a\u0004\u0018\u00010\u0001*\u00020\u0002H\u0007¢\u0006\u0002\b\u0003\u001a\u0019\u0010\u0004\u001a\u00020\u0005*\u00020\u00022\u0006\u0010\u0006\u001a\u00020\u0001H\u0007¢\u0006\u0002\b\u0007¨\u0006\b" }, d2 = { "findViewTreeOnBackPressedDispatcherOwner", "Landroidx/activity/OnBackPressedDispatcherOwner;", "Landroid/view/View;", "get", "setViewTreeOnBackPressedDispatcherOwner", "", "onBackPressedDispatcherOwner", "set", "activity_release" }, k = 2, mv = { 1, 8, 0 }, xi = 48)
public final class ViewTreeOnBackPressedDispatcherOwner
{
    public static final OnBackPressedDispatcherOwner get(final View view) {
        Intrinsics.checkNotNullParameter((Object)view, "<this>");
        return (OnBackPressedDispatcherOwner)SequencesKt.firstOrNull(SequencesKt.mapNotNull(SequencesKt.generateSequence((Object)view, (Function1)ViewTreeOnBackPressedDispatcherOwner$findViewTreeOnBackPressedDispatcherOwner.ViewTreeOnBackPressedDispatcherOwner$findViewTreeOnBackPressedDispatcherOwner$1.INSTANCE), (Function1)ViewTreeOnBackPressedDispatcherOwner$findViewTreeOnBackPressedDispatcherOwner.ViewTreeOnBackPressedDispatcherOwner$findViewTreeOnBackPressedDispatcherOwner$2.INSTANCE));
    }
    
    public static final void set(final View view, final OnBackPressedDispatcherOwner onBackPressedDispatcherOwner) {
        Intrinsics.checkNotNullParameter((Object)view, "<this>");
        Intrinsics.checkNotNullParameter((Object)onBackPressedDispatcherOwner, "onBackPressedDispatcherOwner");
        view.setTag(R.id.view_tree_on_back_pressed_dispatcher_owner, (Object)onBackPressedDispatcherOwner);
    }
}

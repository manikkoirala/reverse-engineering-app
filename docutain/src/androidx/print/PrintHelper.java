// 
// Decompiled by Procyon v0.6.0
// 

package androidx.print;

import android.os.CancellationSignal$OnCancelListener;
import android.print.PrintDocumentInfo$Builder;
import android.os.Bundle;
import android.print.PrintDocumentAdapter$LayoutResultCallback;
import android.print.PageRange;
import android.os.AsyncTask;
import android.print.PrintAttributes$Margins;
import android.print.PrintDocumentAdapter$WriteResultCallback;
import android.os.CancellationSignal;
import android.os.ParcelFileDescriptor;
import android.print.PrintDocumentAdapter;
import android.print.PrintAttributes$MediaSize;
import android.print.PrintManager;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.IOException;
import android.util.Log;
import android.graphics.Rect;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.graphics.Matrix;
import android.graphics.RectF;
import android.print.PrintAttributes$Builder;
import android.print.PrintAttributes;
import android.graphics.ColorFilter;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.ColorMatrix;
import android.graphics.Paint;
import android.graphics.Canvas;
import android.graphics.Bitmap$Config;
import android.graphics.Bitmap;
import android.os.Build$VERSION;
import android.graphics.BitmapFactory$Options;
import android.content.Context;

public final class PrintHelper
{
    public static final int COLOR_MODE_COLOR = 2;
    public static final int COLOR_MODE_MONOCHROME = 1;
    static final boolean IS_MIN_MARGINS_HANDLING_CORRECT;
    private static final String LOG_TAG = "PrintHelper";
    private static final int MAX_PRINT_SIZE = 3500;
    public static final int ORIENTATION_LANDSCAPE = 1;
    public static final int ORIENTATION_PORTRAIT = 2;
    static final boolean PRINT_ACTIVITY_RESPECTS_ORIENTATION;
    public static final int SCALE_MODE_FILL = 2;
    public static final int SCALE_MODE_FIT = 1;
    int mColorMode;
    final Context mContext;
    BitmapFactory$Options mDecodeOptions;
    final Object mLock;
    int mOrientation;
    int mScaleMode;
    
    static {
        final int sdk_INT = Build$VERSION.SDK_INT;
        final boolean b = false;
        PRINT_ACTIVITY_RESPECTS_ORIENTATION = (sdk_INT < 20 || Build$VERSION.SDK_INT > 23);
        boolean is_MIN_MARGINS_HANDLING_CORRECT = b;
        if (Build$VERSION.SDK_INT != 23) {
            is_MIN_MARGINS_HANDLING_CORRECT = true;
        }
        IS_MIN_MARGINS_HANDLING_CORRECT = is_MIN_MARGINS_HANDLING_CORRECT;
    }
    
    public PrintHelper(final Context mContext) {
        this.mDecodeOptions = null;
        this.mLock = new Object();
        this.mScaleMode = 2;
        this.mColorMode = 2;
        this.mOrientation = 1;
        this.mContext = mContext;
    }
    
    static Bitmap convertBitmapForColorMode(final Bitmap bitmap, final int n) {
        if (n != 1) {
            return bitmap;
        }
        final Bitmap bitmap2 = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap$Config.ARGB_8888);
        final Canvas canvas = new Canvas(bitmap2);
        final Paint paint = new Paint();
        final ColorMatrix colorMatrix = new ColorMatrix();
        colorMatrix.setSaturation(0.0f);
        paint.setColorFilter((ColorFilter)new ColorMatrixColorFilter(colorMatrix));
        canvas.drawBitmap(bitmap, 0.0f, 0.0f, paint);
        canvas.setBitmap((Bitmap)null);
        return bitmap2;
    }
    
    private static PrintAttributes$Builder copyAttributes(final PrintAttributes printAttributes) {
        final PrintAttributes$Builder setMinMargins = new PrintAttributes$Builder().setMediaSize(printAttributes.getMediaSize()).setResolution(printAttributes.getResolution()).setMinMargins(printAttributes.getMinMargins());
        if (printAttributes.getColorMode() != 0) {
            setMinMargins.setColorMode(printAttributes.getColorMode());
        }
        if (Build$VERSION.SDK_INT >= 23 && printAttributes.getDuplexMode() != 0) {
            setMinMargins.setDuplexMode(printAttributes.getDuplexMode());
        }
        return setMinMargins;
    }
    
    static Matrix getMatrix(final int n, final int n2, final RectF rectF, final int n3) {
        final Matrix matrix = new Matrix();
        final float width = rectF.width();
        final float n4 = (float)n;
        final float n5 = width / n4;
        float n6;
        if (n3 == 2) {
            n6 = Math.max(n5, rectF.height() / n2);
        }
        else {
            n6 = Math.min(n5, rectF.height() / n2);
        }
        matrix.postScale(n6, n6);
        matrix.postTranslate((rectF.width() - n4 * n6) / 2.0f, (rectF.height() - n2 * n6) / 2.0f);
        return matrix;
    }
    
    static boolean isPortrait(final Bitmap bitmap) {
        return bitmap.getWidth() <= bitmap.getHeight();
    }
    
    private Bitmap loadBitmap(Uri decodeStream, final BitmapFactory$Options bitmapFactory$Options) throws FileNotFoundException {
        if (decodeStream != null) {
            final Context mContext = this.mContext;
            if (mContext != null) {
                final InputStream inputStream = null;
                InputStream inputStream2;
                try {
                    final InputStream openInputStream = mContext.getContentResolver().openInputStream(decodeStream);
                    try {
                        decodeStream = (Uri)BitmapFactory.decodeStream(openInputStream, (Rect)null, bitmapFactory$Options);
                        if (openInputStream != null) {
                            try {
                                openInputStream.close();
                            }
                            catch (final IOException ex) {
                                Log.w("PrintHelper", "close fail ", (Throwable)ex);
                            }
                        }
                        return (Bitmap)decodeStream;
                    }
                    finally {}
                }
                finally {
                    inputStream2 = inputStream;
                }
                if (inputStream2 != null) {
                    try {
                        inputStream2.close();
                    }
                    catch (final IOException ex2) {
                        Log.w("PrintHelper", "close fail ", (Throwable)ex2);
                    }
                }
            }
        }
        throw new IllegalArgumentException("bad argument to loadBitmap");
    }
    
    public static boolean systemSupportsPrint() {
        return Build$VERSION.SDK_INT >= 19;
    }
    
    public int getColorMode() {
        return this.mColorMode;
    }
    
    public int getOrientation() {
        if (Build$VERSION.SDK_INT >= 19 && this.mOrientation == 0) {
            return 1;
        }
        return this.mOrientation;
    }
    
    public int getScaleMode() {
        return this.mScaleMode;
    }
    
    Bitmap loadConstrainedBitmap(final Uri uri) throws FileNotFoundException {
        if (uri != null && this.mContext != null) {
            final BitmapFactory$Options bitmapFactory$Options = new BitmapFactory$Options();
            bitmapFactory$Options.inJustDecodeBounds = true;
            this.loadBitmap(uri, bitmapFactory$Options);
            final int outWidth = bitmapFactory$Options.outWidth;
            final int outHeight = bitmapFactory$Options.outHeight;
            if (outWidth > 0) {
                if (outHeight > 0) {
                    int i;
                    int inSampleSize;
                    for (i = Math.max(outWidth, outHeight), inSampleSize = 1; i > 3500; i >>>= 1, inSampleSize <<= 1) {}
                    if (inSampleSize > 0) {
                        if (Math.min(outWidth, outHeight) / inSampleSize > 0) {
                            final Object mLock = this.mLock;
                            synchronized (mLock) {
                                final BitmapFactory$Options mDecodeOptions = new BitmapFactory$Options();
                                this.mDecodeOptions = mDecodeOptions;
                                mDecodeOptions.inMutable = true;
                                this.mDecodeOptions.inSampleSize = inSampleSize;
                                final BitmapFactory$Options mDecodeOptions2 = this.mDecodeOptions;
                                monitorexit(mLock);
                                try {
                                    final Bitmap loadBitmap = this.loadBitmap(uri, mDecodeOptions2);
                                    synchronized (this.mLock) {
                                        this.mDecodeOptions = null;
                                        return loadBitmap;
                                    }
                                }
                                finally {
                                    synchronized (this.mLock) {
                                        this.mDecodeOptions = null;
                                        monitorexit(this.mLock);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return null;
        }
        throw new IllegalArgumentException("bad argument to getScaledBitmap");
    }
    
    public void printBitmap(final String s, final Bitmap bitmap) {
        this.printBitmap(s, bitmap, null);
    }
    
    public void printBitmap(final String s, final Bitmap bitmap, final OnPrintFinishCallback onPrintFinishCallback) {
        if (Build$VERSION.SDK_INT >= 19) {
            if (bitmap != null) {
                final PrintManager printManager = (PrintManager)this.mContext.getSystemService("print");
                PrintAttributes$MediaSize mediaSize;
                if (isPortrait(bitmap)) {
                    mediaSize = PrintAttributes$MediaSize.UNKNOWN_PORTRAIT;
                }
                else {
                    mediaSize = PrintAttributes$MediaSize.UNKNOWN_LANDSCAPE;
                }
                printManager.print(s, (PrintDocumentAdapter)new PrintBitmapAdapter(s, this.mScaleMode, bitmap, onPrintFinishCallback), new PrintAttributes$Builder().setMediaSize(mediaSize).setColorMode(this.mColorMode).build());
            }
        }
    }
    
    public void printBitmap(final String s, final Uri uri) throws FileNotFoundException {
        this.printBitmap(s, uri, null);
    }
    
    public void printBitmap(final String s, final Uri uri, final OnPrintFinishCallback onPrintFinishCallback) throws FileNotFoundException {
        if (Build$VERSION.SDK_INT < 19) {
            return;
        }
        final PrintUriAdapter printUriAdapter = new PrintUriAdapter(s, uri, onPrintFinishCallback, this.mScaleMode);
        final PrintManager printManager = (PrintManager)this.mContext.getSystemService("print");
        final PrintAttributes$Builder printAttributes$Builder = new PrintAttributes$Builder();
        printAttributes$Builder.setColorMode(this.mColorMode);
        final int mOrientation = this.mOrientation;
        if (mOrientation != 1 && mOrientation != 0) {
            if (mOrientation == 2) {
                printAttributes$Builder.setMediaSize(PrintAttributes$MediaSize.UNKNOWN_PORTRAIT);
            }
        }
        else {
            printAttributes$Builder.setMediaSize(PrintAttributes$MediaSize.UNKNOWN_LANDSCAPE);
        }
        printManager.print(s, (PrintDocumentAdapter)printUriAdapter, printAttributes$Builder.build());
    }
    
    public void setColorMode(final int mColorMode) {
        this.mColorMode = mColorMode;
    }
    
    public void setOrientation(final int mOrientation) {
        this.mOrientation = mOrientation;
    }
    
    public void setScaleMode(final int mScaleMode) {
        this.mScaleMode = mScaleMode;
    }
    
    void writeBitmap(final PrintAttributes printAttributes, final int n, final Bitmap bitmap, final ParcelFileDescriptor parcelFileDescriptor, final CancellationSignal cancellationSignal, final PrintDocumentAdapter$WriteResultCallback printDocumentAdapter$WriteResultCallback) {
        PrintAttributes build;
        if (PrintHelper.IS_MIN_MARGINS_HANDLING_CORRECT) {
            build = printAttributes;
        }
        else {
            build = copyAttributes(printAttributes).setMinMargins(new PrintAttributes$Margins(0, 0, 0, 0)).build();
        }
        new AsyncTask<Void, Void, Throwable>(this, cancellationSignal, build, bitmap, printAttributes, n, parcelFileDescriptor, printDocumentAdapter$WriteResultCallback) {
            final PrintHelper this$0;
            final PrintAttributes val$attributes;
            final Bitmap val$bitmap;
            final CancellationSignal val$cancellationSignal;
            final ParcelFileDescriptor val$fileDescriptor;
            final int val$fittingMode;
            final PrintAttributes val$pdfAttributes;
            final PrintDocumentAdapter$WriteResultCallback val$writeResultCallback;
            
            protected Throwable doInBackground(final Void... p0) {
                // 
                // This method could not be decompiled.
                // 
                // Original Bytecode:
                // 
                //     1: getfield        androidx/print/PrintHelper$1.val$cancellationSignal:Landroid/os/CancellationSignal;
                //     4: invokevirtual   android/os/CancellationSignal.isCanceled:()Z
                //     7: ifeq            12
                //    10: aconst_null    
                //    11: areturn        
                //    12: new             Landroid/print/pdf/PrintedPdfDocument;
                //    15: astore          4
                //    17: aload           4
                //    19: aload_0        
                //    20: getfield        androidx/print/PrintHelper$1.this$0:Landroidx/print/PrintHelper;
                //    23: getfield        androidx/print/PrintHelper.mContext:Landroid/content/Context;
                //    26: aload_0        
                //    27: getfield        androidx/print/PrintHelper$1.val$pdfAttributes:Landroid/print/PrintAttributes;
                //    30: invokespecial   android/print/pdf/PrintedPdfDocument.<init>:(Landroid/content/Context;Landroid/print/PrintAttributes;)V
                //    33: aload_0        
                //    34: getfield        androidx/print/PrintHelper$1.val$bitmap:Landroid/graphics/Bitmap;
                //    37: aload_0        
                //    38: getfield        androidx/print/PrintHelper$1.val$pdfAttributes:Landroid/print/PrintAttributes;
                //    41: invokevirtual   android/print/PrintAttributes.getColorMode:()I
                //    44: invokestatic    androidx/print/PrintHelper.convertBitmapForColorMode:(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;
                //    47: astore_3       
                //    48: aload_0        
                //    49: getfield        androidx/print/PrintHelper$1.val$cancellationSignal:Landroid/os/CancellationSignal;
                //    52: invokevirtual   android/os/CancellationSignal.isCanceled:()Z
                //    55: istore_2       
                //    56: iload_2        
                //    57: ifeq            62
                //    60: aconst_null    
                //    61: areturn        
                //    62: aload           4
                //    64: iconst_1       
                //    65: invokevirtual   android/print/pdf/PrintedPdfDocument.startPage:(I)Landroid/graphics/pdf/PdfDocument$Page;
                //    68: astore          5
                //    70: getstatic       androidx/print/PrintHelper.IS_MIN_MARGINS_HANDLING_CORRECT:Z
                //    73: ifeq            95
                //    76: new             Landroid/graphics/RectF;
                //    79: astore_1       
                //    80: aload_1        
                //    81: aload           5
                //    83: invokevirtual   android/graphics/pdf/PdfDocument$Page.getInfo:()Landroid/graphics/pdf/PdfDocument$PageInfo;
                //    86: invokevirtual   android/graphics/pdf/PdfDocument$PageInfo.getContentRect:()Landroid/graphics/Rect;
                //    89: invokespecial   android/graphics/RectF.<init>:(Landroid/graphics/Rect;)V
                //    92: goto            152
                //    95: new             Landroid/print/pdf/PrintedPdfDocument;
                //    98: astore          6
                //   100: aload           6
                //   102: aload_0        
                //   103: getfield        androidx/print/PrintHelper$1.this$0:Landroidx/print/PrintHelper;
                //   106: getfield        androidx/print/PrintHelper.mContext:Landroid/content/Context;
                //   109: aload_0        
                //   110: getfield        androidx/print/PrintHelper$1.val$attributes:Landroid/print/PrintAttributes;
                //   113: invokespecial   android/print/pdf/PrintedPdfDocument.<init>:(Landroid/content/Context;Landroid/print/PrintAttributes;)V
                //   116: aload           6
                //   118: iconst_1       
                //   119: invokevirtual   android/print/pdf/PrintedPdfDocument.startPage:(I)Landroid/graphics/pdf/PdfDocument$Page;
                //   122: astore          7
                //   124: new             Landroid/graphics/RectF;
                //   127: astore_1       
                //   128: aload_1        
                //   129: aload           7
                //   131: invokevirtual   android/graphics/pdf/PdfDocument$Page.getInfo:()Landroid/graphics/pdf/PdfDocument$PageInfo;
                //   134: invokevirtual   android/graphics/pdf/PdfDocument$PageInfo.getContentRect:()Landroid/graphics/Rect;
                //   137: invokespecial   android/graphics/RectF.<init>:(Landroid/graphics/Rect;)V
                //   140: aload           6
                //   142: aload           7
                //   144: invokevirtual   android/print/pdf/PrintedPdfDocument.finishPage:(Landroid/graphics/pdf/PdfDocument$Page;)V
                //   147: aload           6
                //   149: invokevirtual   android/print/pdf/PrintedPdfDocument.close:()V
                //   152: aload_3        
                //   153: invokevirtual   android/graphics/Bitmap.getWidth:()I
                //   156: aload_3        
                //   157: invokevirtual   android/graphics/Bitmap.getHeight:()I
                //   160: aload_1        
                //   161: aload_0        
                //   162: getfield        androidx/print/PrintHelper$1.val$fittingMode:I
                //   165: invokestatic    androidx/print/PrintHelper.getMatrix:(IILandroid/graphics/RectF;I)Landroid/graphics/Matrix;
                //   168: astore          6
                //   170: getstatic       androidx/print/PrintHelper.IS_MIN_MARGINS_HANDLING_CORRECT:Z
                //   173: ifeq            179
                //   176: goto            203
                //   179: aload           6
                //   181: aload_1        
                //   182: getfield        android/graphics/RectF.left:F
                //   185: aload_1        
                //   186: getfield        android/graphics/RectF.top:F
                //   189: invokevirtual   android/graphics/Matrix.postTranslate:(FF)Z
                //   192: pop            
                //   193: aload           5
                //   195: invokevirtual   android/graphics/pdf/PdfDocument$Page.getCanvas:()Landroid/graphics/Canvas;
                //   198: aload_1        
                //   199: invokevirtual   android/graphics/Canvas.clipRect:(Landroid/graphics/RectF;)Z
                //   202: pop            
                //   203: aload           5
                //   205: invokevirtual   android/graphics/pdf/PdfDocument$Page.getCanvas:()Landroid/graphics/Canvas;
                //   208: aload_3        
                //   209: aload           6
                //   211: aconst_null    
                //   212: invokevirtual   android/graphics/Canvas.drawBitmap:(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;Landroid/graphics/Paint;)V
                //   215: aload           4
                //   217: aload           5
                //   219: invokevirtual   android/print/pdf/PrintedPdfDocument.finishPage:(Landroid/graphics/pdf/PdfDocument$Page;)V
                //   222: aload_0        
                //   223: getfield        androidx/print/PrintHelper$1.val$cancellationSignal:Landroid/os/CancellationSignal;
                //   226: invokevirtual   android/os/CancellationSignal.isCanceled:()Z
                //   229: istore_2       
                //   230: iload_2        
                //   231: ifeq            266
                //   234: aload           4
                //   236: invokevirtual   android/print/pdf/PrintedPdfDocument.close:()V
                //   239: aload_0        
                //   240: getfield        androidx/print/PrintHelper$1.val$fileDescriptor:Landroid/os/ParcelFileDescriptor;
                //   243: astore_1       
                //   244: aload_1        
                //   245: ifnull          252
                //   248: aload_1        
                //   249: invokevirtual   android/os/ParcelFileDescriptor.close:()V
                //   252: aload_3        
                //   253: aload_0        
                //   254: getfield        androidx/print/PrintHelper$1.val$bitmap:Landroid/graphics/Bitmap;
                //   257: if_acmpeq       264
                //   260: aload_3        
                //   261: invokevirtual   android/graphics/Bitmap.recycle:()V
                //   264: aconst_null    
                //   265: areturn        
                //   266: new             Ljava/io/FileOutputStream;
                //   269: astore_1       
                //   270: aload_1        
                //   271: aload_0        
                //   272: getfield        androidx/print/PrintHelper$1.val$fileDescriptor:Landroid/os/ParcelFileDescriptor;
                //   275: invokevirtual   android/os/ParcelFileDescriptor.getFileDescriptor:()Ljava/io/FileDescriptor;
                //   278: invokespecial   java/io/FileOutputStream.<init>:(Ljava/io/FileDescriptor;)V
                //   281: aload           4
                //   283: aload_1        
                //   284: invokevirtual   android/print/pdf/PrintedPdfDocument.writeTo:(Ljava/io/OutputStream;)V
                //   287: aload           4
                //   289: invokevirtual   android/print/pdf/PrintedPdfDocument.close:()V
                //   292: aload_0        
                //   293: getfield        androidx/print/PrintHelper$1.val$fileDescriptor:Landroid/os/ParcelFileDescriptor;
                //   296: astore_1       
                //   297: aload_1        
                //   298: ifnull          305
                //   301: aload_1        
                //   302: invokevirtual   android/os/ParcelFileDescriptor.close:()V
                //   305: aload_3        
                //   306: aload_0        
                //   307: getfield        androidx/print/PrintHelper$1.val$bitmap:Landroid/graphics/Bitmap;
                //   310: if_acmpeq       317
                //   313: aload_3        
                //   314: invokevirtual   android/graphics/Bitmap.recycle:()V
                //   317: aconst_null    
                //   318: areturn        
                //   319: astore_1       
                //   320: aload           4
                //   322: invokevirtual   android/print/pdf/PrintedPdfDocument.close:()V
                //   325: aload_0        
                //   326: getfield        androidx/print/PrintHelper$1.val$fileDescriptor:Landroid/os/ParcelFileDescriptor;
                //   329: astore          4
                //   331: aload           4
                //   333: ifnull          341
                //   336: aload           4
                //   338: invokevirtual   android/os/ParcelFileDescriptor.close:()V
                //   341: aload_3        
                //   342: aload_0        
                //   343: getfield        androidx/print/PrintHelper$1.val$bitmap:Landroid/graphics/Bitmap;
                //   346: if_acmpeq       353
                //   349: aload_3        
                //   350: invokevirtual   android/graphics/Bitmap.recycle:()V
                //   353: aload_1        
                //   354: athrow         
                //   355: astore_1       
                //   356: aload_1        
                //   357: areturn        
                //   358: astore_1       
                //   359: goto            252
                //   362: astore_1       
                //   363: goto            305
                //   366: astore          4
                //   368: goto            341
                //    Exceptions:
                //  Try           Handler
                //  Start  End    Start  End    Type                 
                //  -----  -----  -----  -----  ---------------------
                //  0      10     355    358    Any
                //  12     56     355    358    Any
                //  62     92     319    355    Any
                //  95     152    319    355    Any
                //  152    176    319    355    Any
                //  179    203    319    355    Any
                //  203    230    319    355    Any
                //  234    244    355    358    Any
                //  248    252    358    362    Ljava/io/IOException;
                //  248    252    355    358    Any
                //  252    264    355    358    Any
                //  266    287    319    355    Any
                //  287    297    355    358    Any
                //  301    305    362    366    Ljava/io/IOException;
                //  301    305    355    358    Any
                //  305    317    355    358    Any
                //  320    331    355    358    Any
                //  336    341    366    371    Ljava/io/IOException;
                //  336    341    355    358    Any
                //  341    353    355    358    Any
                //  353    355    355    358    Any
                // 
                // The error that occurred was:
                // 
                // java.lang.IndexOutOfBoundsException: Index 179 out of bounds for length 179
                //     at java.base/jdk.internal.util.Preconditions.outOfBounds(Preconditions.java:64)
                //     at java.base/jdk.internal.util.Preconditions.outOfBoundsCheckIndex(Preconditions.java:70)
                //     at java.base/jdk.internal.util.Preconditions.checkIndex(Preconditions.java:266)
                //     at java.base/java.util.Objects.checkIndex(Objects.java:361)
                //     at java.base/java.util.ArrayList.get(ArrayList.java:427)
                //     at com.strobel.decompiler.ast.AstBuilder.convertToAst(AstBuilder.java:3362)
                //     at com.strobel.decompiler.ast.AstBuilder.build(AstBuilder.java:112)
                //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:203)
                //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
                //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.transformCall(AstMethodBodyBuilder.java:1151)
                //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.transformByteCode(AstMethodBodyBuilder.java:993)
                //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.transformExpression(AstMethodBodyBuilder.java:534)
                //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.transformByteCode(AstMethodBodyBuilder.java:548)
                //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.transformExpression(AstMethodBodyBuilder.java:534)
                //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.transformNode(AstMethodBodyBuilder.java:377)
                //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.transformBlock(AstMethodBodyBuilder.java:318)
                //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:213)
                //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
                //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
                //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
                //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
                //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
                //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
                // 
                throw new IllegalStateException("An error occurred while decompiling this method.");
            }
            
            protected void onPostExecute(final Throwable t) {
                if (this.val$cancellationSignal.isCanceled()) {
                    this.val$writeResultCallback.onWriteCancelled();
                }
                else if (t == null) {
                    this.val$writeResultCallback.onWriteFinished(new PageRange[] { PageRange.ALL_PAGES });
                }
                else {
                    Log.e("PrintHelper", "Error writing printed content", t);
                    this.val$writeResultCallback.onWriteFailed((CharSequence)null);
                }
            }
        }.execute((Object[])new Void[0]);
    }
    
    public interface OnPrintFinishCallback
    {
        void onFinish();
    }
    
    private class PrintBitmapAdapter extends PrintDocumentAdapter
    {
        private PrintAttributes mAttributes;
        private final Bitmap mBitmap;
        private final OnPrintFinishCallback mCallback;
        private final int mFittingMode;
        private final String mJobName;
        final PrintHelper this$0;
        
        PrintBitmapAdapter(final PrintHelper this$0, final String mJobName, final int mFittingMode, final Bitmap mBitmap, final OnPrintFinishCallback mCallback) {
            this.this$0 = this$0;
            this.mJobName = mJobName;
            this.mFittingMode = mFittingMode;
            this.mBitmap = mBitmap;
            this.mCallback = mCallback;
        }
        
        public void onFinish() {
            final OnPrintFinishCallback mCallback = this.mCallback;
            if (mCallback != null) {
                mCallback.onFinish();
            }
        }
        
        public void onLayout(final PrintAttributes printAttributes, final PrintAttributes mAttributes, final CancellationSignal cancellationSignal, final PrintDocumentAdapter$LayoutResultCallback printDocumentAdapter$LayoutResultCallback, final Bundle bundle) {
            this.mAttributes = mAttributes;
            printDocumentAdapter$LayoutResultCallback.onLayoutFinished(new PrintDocumentInfo$Builder(this.mJobName).setContentType(1).setPageCount(1).build(), mAttributes.equals((Object)printAttributes) ^ true);
        }
        
        public void onWrite(final PageRange[] array, final ParcelFileDescriptor parcelFileDescriptor, final CancellationSignal cancellationSignal, final PrintDocumentAdapter$WriteResultCallback printDocumentAdapter$WriteResultCallback) {
            this.this$0.writeBitmap(this.mAttributes, this.mFittingMode, this.mBitmap, parcelFileDescriptor, cancellationSignal, printDocumentAdapter$WriteResultCallback);
        }
    }
    
    private class PrintUriAdapter extends PrintDocumentAdapter
    {
        PrintAttributes mAttributes;
        Bitmap mBitmap;
        final OnPrintFinishCallback mCallback;
        final int mFittingMode;
        final Uri mImageFile;
        final String mJobName;
        AsyncTask<Uri, Boolean, Bitmap> mLoadBitmap;
        final PrintHelper this$0;
        
        PrintUriAdapter(final PrintHelper this$0, final String mJobName, final Uri mImageFile, final OnPrintFinishCallback mCallback, final int mFittingMode) {
            this.this$0 = this$0;
            this.mJobName = mJobName;
            this.mImageFile = mImageFile;
            this.mCallback = mCallback;
            this.mFittingMode = mFittingMode;
            this.mBitmap = null;
        }
        
        void cancelLoad() {
            synchronized (this.this$0.mLock) {
                if (this.this$0.mDecodeOptions != null) {
                    if (Build$VERSION.SDK_INT < 24) {
                        this.this$0.mDecodeOptions.requestCancelDecode();
                    }
                    this.this$0.mDecodeOptions = null;
                }
            }
        }
        
        public void onFinish() {
            super.onFinish();
            this.cancelLoad();
            final AsyncTask<Uri, Boolean, Bitmap> mLoadBitmap = this.mLoadBitmap;
            if (mLoadBitmap != null) {
                mLoadBitmap.cancel(true);
            }
            final OnPrintFinishCallback mCallback = this.mCallback;
            if (mCallback != null) {
                mCallback.onFinish();
            }
            final Bitmap mBitmap = this.mBitmap;
            if (mBitmap != null) {
                mBitmap.recycle();
                this.mBitmap = null;
            }
        }
        
        public void onLayout(final PrintAttributes printAttributes, final PrintAttributes mAttributes, final CancellationSignal cancellationSignal, final PrintDocumentAdapter$LayoutResultCallback printDocumentAdapter$LayoutResultCallback, final Bundle bundle) {
            synchronized (this) {
                this.mAttributes = mAttributes;
                monitorexit(this);
                if (cancellationSignal.isCanceled()) {
                    printDocumentAdapter$LayoutResultCallback.onLayoutCancelled();
                    return;
                }
                if (this.mBitmap != null) {
                    printDocumentAdapter$LayoutResultCallback.onLayoutFinished(new PrintDocumentInfo$Builder(this.mJobName).setContentType(1).setPageCount(1).build(), mAttributes.equals((Object)printAttributes) ^ true);
                    return;
                }
                this.mLoadBitmap = (AsyncTask<Uri, Boolean, Bitmap>)new AsyncTask<Uri, Boolean, Bitmap>(this, cancellationSignal, mAttributes, printAttributes, printDocumentAdapter$LayoutResultCallback) {
                    final PrintUriAdapter this$1;
                    final CancellationSignal val$cancellationSignal;
                    final PrintDocumentAdapter$LayoutResultCallback val$layoutResultCallback;
                    final PrintAttributes val$newPrintAttributes;
                    final PrintAttributes val$oldPrintAttributes;
                    
                    protected Bitmap doInBackground(final Uri... array) {
                        try {
                            return this.this$1.this$0.loadConstrainedBitmap(this.this$1.mImageFile);
                        }
                        catch (final FileNotFoundException ex) {
                            return null;
                        }
                    }
                    
                    protected void onCancelled(final Bitmap bitmap) {
                        this.val$layoutResultCallback.onLayoutCancelled();
                        this.this$1.mLoadBitmap = null;
                    }
                    
                    protected void onPostExecute(final Bitmap bitmap) {
                        super.onPostExecute((Object)bitmap);
                        Object bitmap2 = bitmap;
                        Label_0109: {
                            if (bitmap != null) {
                                if (PrintHelper.PRINT_ACTIVITY_RESPECTS_ORIENTATION) {
                                    bitmap2 = bitmap;
                                    if (this.this$1.this$0.mOrientation != 0) {
                                        break Label_0109;
                                    }
                                }
                                synchronized (this) {
                                    final PrintAttributes$MediaSize mediaSize = this.this$1.mAttributes.getMediaSize();
                                    monitorexit(this);
                                    bitmap2 = bitmap;
                                    if (mediaSize != null) {
                                        bitmap2 = bitmap;
                                        if (mediaSize.isPortrait() != PrintHelper.isPortrait(bitmap)) {
                                            bitmap2 = new Matrix();
                                            ((Matrix)bitmap2).postRotate(90.0f);
                                            bitmap2 = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), (Matrix)bitmap2, true);
                                        }
                                    }
                                }
                            }
                        }
                        if ((this.this$1.mBitmap = (Bitmap)bitmap2) != null) {
                            this.val$layoutResultCallback.onLayoutFinished(new PrintDocumentInfo$Builder(this.this$1.mJobName).setContentType(1).setPageCount(1).build(), true ^ this.val$newPrintAttributes.equals((Object)this.val$oldPrintAttributes));
                        }
                        else {
                            this.val$layoutResultCallback.onLayoutFailed((CharSequence)null);
                        }
                        this.this$1.mLoadBitmap = null;
                    }
                    
                    protected void onPreExecute() {
                        this.val$cancellationSignal.setOnCancelListener((CancellationSignal$OnCancelListener)new CancellationSignal$OnCancelListener(this) {
                            final PrintHelper$PrintUriAdapter$1 this$2;
                            
                            public void onCancel() {
                                this.this$2.this$1.cancelLoad();
                                this.this$2.cancel(false);
                            }
                        });
                    }
                }.execute((Object[])new Uri[0]);
            }
        }
        
        public void onWrite(final PageRange[] array, final ParcelFileDescriptor parcelFileDescriptor, final CancellationSignal cancellationSignal, final PrintDocumentAdapter$WriteResultCallback printDocumentAdapter$WriteResultCallback) {
            this.this$0.writeBitmap(this.mAttributes, this.mFittingMode, this.mBitmap, parcelFileDescriptor, cancellationSignal, printDocumentAdapter$WriteResultCallback);
        }
    }
}

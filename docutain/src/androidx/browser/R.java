// 
// Decompiled by Procyon v0.6.0
// 

package androidx.browser;

public final class R
{
    public static final class color
    {
        public static final int browser_actions_bg_grey = 2131099693;
        public static final int browser_actions_divider_color = 2131099694;
        public static final int browser_actions_text_color = 2131099695;
        public static final int browser_actions_title_color = 2131099696;
    }
    
    public static final class dimen
    {
        public static final int browser_actions_context_menu_max_width = 2131165266;
        public static final int browser_actions_context_menu_min_padding = 2131165267;
    }
    
    public static final class id
    {
        public static final int browser_actions_header_text = 2131361965;
        public static final int browser_actions_menu_item_icon = 2131361966;
        public static final int browser_actions_menu_item_text = 2131361967;
        public static final int browser_actions_menu_items = 2131361968;
        public static final int browser_actions_menu_view = 2131361969;
    }
    
    public static final class layout
    {
        public static final int browser_actions_context_menu_page = 2131558434;
        public static final int browser_actions_context_menu_row = 2131558435;
    }
    
    public static final class string
    {
        public static final int copy_toast_msg = 2131886147;
        public static final int fallback_menu_item_copy_link = 2131886156;
        public static final int fallback_menu_item_open_in_browser = 2131886157;
        public static final int fallback_menu_item_share_link = 2131886158;
    }
    
    public static final class xml
    {
        public static final int image_share_filepaths = 2132148225;
    }
}

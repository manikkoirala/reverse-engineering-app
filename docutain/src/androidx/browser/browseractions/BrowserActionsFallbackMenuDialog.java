// 
// Decompiled by Procyon v0.6.0
// 

package androidx.browser.browseractions;

import android.graphics.drawable.Drawable;
import android.graphics.drawable.ColorDrawable;
import android.view.MotionEvent;
import android.animation.Animator$AnimatorListener;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.TimeInterpolator;
import androidx.interpolator.view.animation.LinearOutSlowInInterpolator;
import android.content.Context;
import android.view.View;
import android.app.Dialog;

@Deprecated
class BrowserActionsFallbackMenuDialog extends Dialog
{
    private static final long ENTER_ANIMATION_DURATION_MS = 250L;
    private static final long EXIT_ANIMATION_DURATION_MS = 150L;
    private final View mContentView;
    
    BrowserActionsFallbackMenuDialog(final Context context, final View mContentView) {
        super(context);
        this.mContentView = mContentView;
    }
    
    static /* synthetic */ void access$001(final BrowserActionsFallbackMenuDialog browserActionsFallbackMenuDialog) {
        browserActionsFallbackMenuDialog.dismiss();
    }
    
    private void startAnimation(final boolean b) {
        float n = 0.0f;
        float n2;
        if (b) {
            n2 = 0.0f;
        }
        else {
            n2 = 1.0f;
        }
        if (b) {
            n = 1.0f;
        }
        long duration;
        if (b) {
            duration = 250L;
        }
        else {
            duration = 150L;
        }
        this.mContentView.setScaleX(n2);
        this.mContentView.setScaleY(n2);
        this.mContentView.animate().scaleX(n).scaleY(n).setDuration(duration).setInterpolator((TimeInterpolator)new LinearOutSlowInInterpolator()).setListener((Animator$AnimatorListener)new AnimatorListenerAdapter(this, b) {
            final BrowserActionsFallbackMenuDialog this$0;
            final boolean val$isEnterAnimation;
            
            public void onAnimationEnd(final Animator animator) {
                if (!this.val$isEnterAnimation) {
                    BrowserActionsFallbackMenuDialog.access$001(this.this$0);
                }
            }
        }).start();
    }
    
    public void dismiss() {
        this.startAnimation(false);
    }
    
    public boolean onTouchEvent(final MotionEvent motionEvent) {
        if (motionEvent.getAction() == 0) {
            this.dismiss();
            return true;
        }
        return false;
    }
    
    public void show() {
        this.getWindow().setBackgroundDrawable((Drawable)new ColorDrawable(0));
        this.startAnimation(true);
        super.show();
    }
}

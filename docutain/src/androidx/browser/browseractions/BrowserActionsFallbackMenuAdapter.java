// 
// Decompiled by Procyon v0.6.0
// 

package androidx.browser.browseractions;

import java.util.concurrent.Executor;
import java.util.concurrent.ExecutionException;
import android.graphics.Bitmap;
import android.text.TextUtils;
import com.google.common.util.concurrent.ListenableFuture;
import android.content.res.Resources$Theme;
import androidx.core.content.res.ResourcesCompat;
import android.widget.TextView;
import android.widget.ImageView;
import androidx.browser.R;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.view.View;
import java.util.List;
import android.content.Context;
import android.widget.BaseAdapter;

@Deprecated
class BrowserActionsFallbackMenuAdapter extends BaseAdapter
{
    private final Context mContext;
    private final List<BrowserActionItem> mMenuItems;
    
    BrowserActionsFallbackMenuAdapter(final List<BrowserActionItem> mMenuItems, final Context mContext) {
        this.mMenuItems = mMenuItems;
        this.mContext = mContext;
    }
    
    public int getCount() {
        return this.mMenuItems.size();
    }
    
    public Object getItem(final int n) {
        return this.mMenuItems.get(n);
    }
    
    public long getItemId(final int n) {
        return n;
    }
    
    public View getView(final int n, final View view, final ViewGroup viewGroup) {
        final BrowserActionItem browserActionItem = this.mMenuItems.get(n);
        View inflate;
        ViewHolderItem tag;
        if (view == null) {
            inflate = LayoutInflater.from(this.mContext).inflate(R.layout.browser_actions_context_menu_row, (ViewGroup)null);
            final ImageView imageView = (ImageView)inflate.findViewById(R.id.browser_actions_menu_item_icon);
            final TextView textView = (TextView)inflate.findViewById(R.id.browser_actions_menu_item_text);
            if (imageView == null || textView == null) {
                throw new IllegalStateException("Browser Actions fallback UI does not contain necessary Views.");
            }
            tag = new ViewHolderItem(imageView, textView);
            inflate.setTag((Object)tag);
        }
        else {
            final ViewHolderItem viewHolderItem = (ViewHolderItem)view.getTag();
            inflate = view;
            tag = viewHolderItem;
        }
        final String title = browserActionItem.getTitle();
        tag.mText.setText((CharSequence)title);
        if (browserActionItem.getIconId() != 0) {
            tag.mIcon.setImageDrawable(ResourcesCompat.getDrawable(this.mContext.getResources(), browserActionItem.getIconId(), null));
        }
        else if (browserActionItem.getIconUri() != null) {
            final ListenableFuture<Bitmap> loadBitmap = BrowserServiceFileProvider.loadBitmap(this.mContext.getContentResolver(), browserActionItem.getIconUri());
            loadBitmap.addListener((Runnable)new Runnable(this, title, tag, loadBitmap) {
                final BrowserActionsFallbackMenuAdapter this$0;
                final ListenableFuture val$bitmapFuture;
                final String val$titleText;
                final ViewHolderItem val$viewHolder;
                
                @Override
                public void run() {
                    if (!TextUtils.equals((CharSequence)this.val$titleText, this.val$viewHolder.mText.getText())) {
                        return;
                    }
                    Bitmap imageBitmap;
                    try {
                        imageBitmap = (Bitmap)this.val$bitmapFuture.get();
                    }
                    catch (final ExecutionException | InterruptedException ex) {
                        imageBitmap = null;
                    }
                    if (imageBitmap != null) {
                        this.val$viewHolder.mIcon.setVisibility(0);
                        this.val$viewHolder.mIcon.setImageBitmap(imageBitmap);
                    }
                    else {
                        this.val$viewHolder.mIcon.setVisibility(4);
                        this.val$viewHolder.mIcon.setImageBitmap((Bitmap)null);
                    }
                }
            }, (Executor)new Executor(this) {
                final BrowserActionsFallbackMenuAdapter this$0;
                
                @Override
                public void execute(final Runnable runnable) {
                    runnable.run();
                }
            });
        }
        else {
            tag.mIcon.setImageBitmap((Bitmap)null);
            tag.mIcon.setVisibility(4);
        }
        return inflate;
    }
    
    static class ViewHolderItem
    {
        final ImageView mIcon;
        final TextView mText;
        
        ViewHolderItem(final ImageView mIcon, final TextView mText) {
            this.mIcon = mIcon;
            this.mText = mText;
        }
    }
}

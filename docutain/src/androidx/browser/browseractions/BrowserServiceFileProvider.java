// 
// Decompiled by Procyon v0.6.0
// 

package androidx.browser.browseractions;

import java.io.FileOutputStream;
import java.io.OutputStream;
import android.graphics.Bitmap$CompressFormat;
import androidx.core.util.AtomicFile;
import android.os.Build$VERSION;
import android.content.SharedPreferences$Editor;
import android.util.Log;
import android.content.SharedPreferences;
import java.io.File;
import java.util.concurrent.TimeUnit;
import android.os.ParcelFileDescriptor;
import java.io.IOException;
import android.graphics.BitmapFactory;
import java.io.FileNotFoundException;
import android.os.AsyncTask;
import androidx.concurrent.futures.ResolvableFuture;
import android.graphics.Bitmap;
import com.google.common.util.concurrent.ListenableFuture;
import android.content.ContentResolver;
import android.content.ClipData$Item;
import android.content.ClipData;
import java.util.List;
import android.content.Intent;
import android.net.Uri$Builder;
import android.net.Uri;
import android.content.Context;
import androidx.core.content.FileProvider;

@Deprecated
public final class BrowserServiceFileProvider extends FileProvider
{
    private static final String AUTHORITY_SUFFIX = ".image_provider";
    private static final String CLIP_DATA_LABEL = "image_provider_uris";
    private static final String CONTENT_SCHEME = "content";
    private static final String FILE_EXTENSION = ".png";
    private static final String FILE_SUB_DIR = "image_provider";
    private static final String FILE_SUB_DIR_NAME = "image_provider_images/";
    private static final String LAST_CLEANUP_TIME_KEY = "last_cleanup_time";
    private static final String TAG = "BrowserServiceFP";
    static Object sFileCleanupLock;
    
    static {
        BrowserServiceFileProvider.sFileCleanupLock = new Object();
    }
    
    private static Uri generateUri(final Context context, String string) {
        final StringBuilder sb = new StringBuilder();
        sb.append("image_provider_images/");
        sb.append(string);
        sb.append(".png");
        string = sb.toString();
        final Uri$Builder scheme = new Uri$Builder().scheme("content");
        final StringBuilder sb2 = new StringBuilder();
        sb2.append(context.getPackageName());
        sb2.append(".image_provider");
        return scheme.authority(sb2.toString()).path(string).build();
    }
    
    public static void grantReadPermission(final Intent intent, final List<Uri> list, final Context context) {
        if (list != null) {
            if (list.size() != 0) {
                final ContentResolver contentResolver = context.getContentResolver();
                int i = 1;
                intent.addFlags(1);
                final ClipData uri = ClipData.newUri(contentResolver, (CharSequence)"image_provider_uris", (Uri)list.get(0));
                while (i < list.size()) {
                    uri.addItem(new ClipData$Item((Uri)list.get(i)));
                    ++i;
                }
                intent.setClipData(uri);
            }
        }
    }
    
    public static ListenableFuture<Bitmap> loadBitmap(final ContentResolver contentResolver, final Uri uri) {
        final ResolvableFuture<Object> create = ResolvableFuture.create();
        AsyncTask.THREAD_POOL_EXECUTOR.execute(new Runnable(contentResolver, uri, create) {
            final ContentResolver val$resolver;
            final ResolvableFuture val$result;
            final Uri val$uri;
            
            @Override
            public void run() {
                try {
                    final ParcelFileDescriptor openFileDescriptor = this.val$resolver.openFileDescriptor(this.val$uri, "r");
                    if (openFileDescriptor == null) {
                        this.val$result.setException(new FileNotFoundException());
                        return;
                    }
                    final Bitmap decodeFileDescriptor = BitmapFactory.decodeFileDescriptor(openFileDescriptor.getFileDescriptor());
                    openFileDescriptor.close();
                    if (decodeFileDescriptor == null) {
                        this.val$result.setException(new IOException("File could not be decoded."));
                        return;
                    }
                    this.val$result.set(decodeFileDescriptor);
                }
                catch (final IOException exception) {
                    this.val$result.setException(exception);
                }
            }
        });
        return (ListenableFuture<Bitmap>)create;
    }
    
    public static ResolvableFuture<Uri> saveBitmap(final Context context, final Bitmap bitmap, String string, final int i) {
        final StringBuilder sb = new StringBuilder();
        sb.append(string);
        sb.append("_");
        sb.append(Integer.toString(i));
        string = sb.toString();
        final Uri generateUri = generateUri(context, string);
        final ResolvableFuture<Object> create = ResolvableFuture.create();
        new FileSaveTask(context, string, bitmap, generateUri, (ResolvableFuture<Uri>)create).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, (Object[])new String[0]);
        return (ResolvableFuture<Uri>)create;
    }
    
    private static class FileCleanupTask extends AsyncTask<Void, Void, Void>
    {
        private static final long CLEANUP_REQUIRED_TIME_SPAN;
        private static final long DELETION_FAILED_REATTEMPT_DURATION;
        private static final long IMAGE_RETENTION_DURATION;
        private final Context mAppContext;
        
        static {
            IMAGE_RETENTION_DURATION = TimeUnit.DAYS.toMillis(7L);
            CLEANUP_REQUIRED_TIME_SPAN = TimeUnit.DAYS.toMillis(7L);
            DELETION_FAILED_REATTEMPT_DURATION = TimeUnit.DAYS.toMillis(1L);
        }
        
        FileCleanupTask(final Context context) {
            this.mAppContext = context.getApplicationContext();
        }
        
        private static boolean isImageFile(final File file) {
            return file.getName().endsWith("..png");
        }
        
        private static boolean shouldCleanUp(final SharedPreferences sharedPreferences) {
            return System.currentTimeMillis() > sharedPreferences.getLong("last_cleanup_time", System.currentTimeMillis()) + FileCleanupTask.CLEANUP_REQUIRED_TIME_SPAN;
        }
        
        protected Void doInBackground(final Void... array) {
            final Context mAppContext = this.mAppContext;
            final StringBuilder sb = new StringBuilder();
            sb.append(this.mAppContext.getPackageName());
            sb.append(".image_provider");
            final SharedPreferences sharedPreferences = mAppContext.getSharedPreferences(sb.toString(), 0);
            if (!shouldCleanUp(sharedPreferences)) {
                return null;
            }
            synchronized (BrowserServiceFileProvider.sFileCleanupLock) {
                final File file = new File(this.mAppContext.getFilesDir(), "image_provider");
                if (!file.exists()) {
                    return null;
                }
                final File[] listFiles = file.listFiles();
                final long currentTimeMillis = System.currentTimeMillis();
                final long image_RETENTION_DURATION = FileCleanupTask.IMAGE_RETENTION_DURATION;
                final int length = listFiles.length;
                int n = 1;
                int n2;
                for (int i = 0; i < length; ++i, n = n2) {
                    final File file2 = listFiles[i];
                    if (!isImageFile(file2)) {
                        n2 = n;
                    }
                    else {
                        n2 = n;
                        if (file2.lastModified() < currentTimeMillis - image_RETENTION_DURATION) {
                            n2 = n;
                            if (!file2.delete()) {
                                final StringBuilder sb2 = new StringBuilder();
                                sb2.append("Fail to delete image: ");
                                sb2.append(file2.getAbsoluteFile());
                                Log.e("BrowserServiceFP", sb2.toString());
                                n2 = 0;
                            }
                        }
                    }
                }
                long currentTimeMillis2;
                if (n != 0) {
                    currentTimeMillis2 = System.currentTimeMillis();
                }
                else {
                    currentTimeMillis2 = System.currentTimeMillis() - FileCleanupTask.CLEANUP_REQUIRED_TIME_SPAN + FileCleanupTask.DELETION_FAILED_REATTEMPT_DURATION;
                }
                final SharedPreferences$Editor edit = sharedPreferences.edit();
                edit.putLong("last_cleanup_time", currentTimeMillis2);
                edit.apply();
                return null;
            }
        }
    }
    
    private static class FileSaveTask extends AsyncTask<String, Void, Void>
    {
        private final Context mAppContext;
        private final Bitmap mBitmap;
        private final Uri mFileUri;
        private final String mFilename;
        private final ResolvableFuture<Uri> mResultFuture;
        
        FileSaveTask(final Context context, final String mFilename, final Bitmap mBitmap, final Uri mFileUri, final ResolvableFuture<Uri> mResultFuture) {
            this.mAppContext = context.getApplicationContext();
            this.mFilename = mFilename;
            this.mBitmap = mBitmap;
            this.mFileUri = mFileUri;
            this.mResultFuture = mResultFuture;
        }
        
        private void saveFileBlocking(final File file) {
            if (Build$VERSION.SDK_INT >= 22) {
                final AtomicFile atomicFile = new AtomicFile(file);
                FileOutputStream startWrite;
                try {
                    startWrite = atomicFile.startWrite();
                    try {
                        this.mBitmap.compress(Bitmap$CompressFormat.PNG, 100, (OutputStream)startWrite);
                        startWrite.close();
                        atomicFile.finishWrite(startWrite);
                        this.mResultFuture.set(this.mFileUri);
                    }
                    catch (final IOException exception) {}
                }
                catch (final IOException exception) {
                    startWrite = null;
                }
                atomicFile.failWrite(startWrite);
                final IOException exception;
                this.mResultFuture.setException(exception);
            }
            else {
                try {
                    final FileOutputStream fileOutputStream = new FileOutputStream(file);
                    this.mBitmap.compress(Bitmap$CompressFormat.PNG, 100, (OutputStream)fileOutputStream);
                    fileOutputStream.close();
                    this.mResultFuture.set(this.mFileUri);
                }
                catch (final IOException exception2) {
                    this.mResultFuture.setException(exception2);
                }
            }
        }
        
        private void saveFileIfNeededBlocking() {
            final File parent = new File(this.mAppContext.getFilesDir(), "image_provider");
            synchronized (BrowserServiceFileProvider.sFileCleanupLock) {
                if (!parent.exists() && !parent.mkdir()) {
                    this.mResultFuture.setException(new IOException("Could not create file directory."));
                    return;
                }
                final StringBuilder sb = new StringBuilder();
                sb.append(this.mFilename);
                sb.append(".png");
                final File file = new File(parent, sb.toString());
                if (file.exists()) {
                    this.mResultFuture.set(this.mFileUri);
                }
                else {
                    this.saveFileBlocking(file);
                }
                file.setLastModified(System.currentTimeMillis());
            }
        }
        
        protected Void doInBackground(final String... array) {
            this.saveFileIfNeededBlocking();
            return null;
        }
        
        protected void onPostExecute(final Void void1) {
            new FileCleanupTask(this.mAppContext).executeOnExecutor(AsyncTask.SERIAL_EXECUTOR, (Object[])new Void[0]);
        }
    }
}

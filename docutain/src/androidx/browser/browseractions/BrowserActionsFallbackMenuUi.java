// 
// Decompiled by Procyon v0.6.0
// 

package androidx.browser.browseractions;

import android.app.PendingIntent$CanceledException;
import android.widget.AdapterView;
import android.util.Log;
import android.content.DialogInterface;
import android.content.DialogInterface$OnShowListener;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.text.TextUtils$TruncateAt;
import androidx.core.widget.TextViewCompat;
import android.view.View$OnClickListener;
import android.widget.TextView;
import android.view.View;
import android.content.Intent;
import android.app.PendingIntent;
import java.util.Collection;
import java.util.ArrayList;
import android.widget.Toast;
import androidx.browser.R;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.net.Uri;
import java.util.List;
import android.content.Context;
import android.widget.AdapterView$OnItemClickListener;

@Deprecated
class BrowserActionsFallbackMenuUi implements AdapterView$OnItemClickListener
{
    private static final String TAG = "BrowserActionskMenuUi";
    private BrowserActionsFallbackMenuDialog mBrowserActionsDialog;
    final Context mContext;
    private final List<BrowserActionItem> mMenuItems;
    BrowserActionsFallMenuUiListener mMenuUiListener;
    final Uri mUri;
    
    BrowserActionsFallbackMenuUi(final Context mContext, final Uri mUri, final List<BrowserActionItem> list) {
        this.mContext = mContext;
        this.mUri = mUri;
        this.mMenuItems = this.buildFallbackMenuItemList(list);
    }
    
    private Runnable buildCopyAction() {
        return new Runnable(this) {
            final BrowserActionsFallbackMenuUi this$0;
            
            @Override
            public void run() {
                ((ClipboardManager)this.this$0.mContext.getSystemService("clipboard")).setPrimaryClip(ClipData.newPlainText((CharSequence)"url", (CharSequence)this.this$0.mUri.toString()));
                Toast.makeText(this.this$0.mContext, (CharSequence)this.this$0.mContext.getString(R.string.copy_toast_msg), 0).show();
            }
        };
    }
    
    private List<BrowserActionItem> buildFallbackMenuItemList(final List<BrowserActionItem> list) {
        final ArrayList list2 = new ArrayList();
        list2.add(new BrowserActionItem(this.mContext.getString(R.string.fallback_menu_item_open_in_browser), this.buildOpenInBrowserAction()));
        list2.add(new BrowserActionItem(this.mContext.getString(R.string.fallback_menu_item_copy_link), this.buildCopyAction()));
        list2.add(new BrowserActionItem(this.mContext.getString(R.string.fallback_menu_item_share_link), this.buildShareAction()));
        list2.addAll(list);
        return list2;
    }
    
    private PendingIntent buildOpenInBrowserAction() {
        return PendingIntent.getActivity(this.mContext, 0, new Intent("android.intent.action.VIEW", this.mUri), 67108864);
    }
    
    private PendingIntent buildShareAction() {
        final Intent intent = new Intent("android.intent.action.SEND");
        intent.putExtra("android.intent.extra.TEXT", this.mUri.toString());
        intent.setType("text/plain");
        return PendingIntent.getActivity(this.mContext, 0, intent, 67108864);
    }
    
    private BrowserActionsFallbackMenuView initMenuView(final View view) {
        final BrowserActionsFallbackMenuView browserActionsFallbackMenuView = (BrowserActionsFallbackMenuView)view.findViewById(R.id.browser_actions_menu_view);
        final TextView textView = (TextView)view.findViewById(R.id.browser_actions_header_text);
        textView.setText((CharSequence)this.mUri.toString());
        textView.setOnClickListener((View$OnClickListener)new View$OnClickListener(this, textView) {
            final BrowserActionsFallbackMenuUi this$0;
            final TextView val$urlTextView;
            
            public void onClick(final View view) {
                if (TextViewCompat.getMaxLines(this.val$urlTextView) == Integer.MAX_VALUE) {
                    this.val$urlTextView.setMaxLines(1);
                    this.val$urlTextView.setEllipsize(TextUtils$TruncateAt.END);
                }
                else {
                    this.val$urlTextView.setMaxLines(Integer.MAX_VALUE);
                    this.val$urlTextView.setEllipsize((TextUtils$TruncateAt)null);
                }
            }
        });
        final ListView listView = (ListView)view.findViewById(R.id.browser_actions_menu_items);
        listView.setAdapter((ListAdapter)new BrowserActionsFallbackMenuAdapter(this.mMenuItems, this.mContext));
        listView.setOnItemClickListener((AdapterView$OnItemClickListener)this);
        return browserActionsFallbackMenuView;
    }
    
    public void displayMenu() {
        final View inflate = LayoutInflater.from(this.mContext).inflate(R.layout.browser_actions_context_menu_page, (ViewGroup)null);
        (this.mBrowserActionsDialog = new BrowserActionsFallbackMenuDialog(this.mContext, (View)this.initMenuView(inflate))).setContentView(inflate);
        if (this.mMenuUiListener != null) {
            this.mBrowserActionsDialog.setOnShowListener((DialogInterface$OnShowListener)new DialogInterface$OnShowListener(this, inflate) {
                final BrowserActionsFallbackMenuUi this$0;
                final View val$view;
                
                public void onShow(final DialogInterface dialogInterface) {
                    if (this.this$0.mMenuUiListener == null) {
                        Log.e("BrowserActionskMenuUi", "Cannot trigger menu item listener, it is null");
                        return;
                    }
                    this.this$0.mMenuUiListener.onMenuShown(this.val$view);
                }
            });
        }
        this.mBrowserActionsDialog.show();
    }
    
    public void onItemClick(final AdapterView<?> adapterView, final View view, final int n, final long n2) {
        final BrowserActionItem browserActionItem = this.mMenuItems.get(n);
        if (browserActionItem.getAction() != null) {
            try {
                browserActionItem.getAction().send();
            }
            catch (final PendingIntent$CanceledException ex) {
                Log.e("BrowserActionskMenuUi", "Failed to send custom item action", (Throwable)ex);
            }
        }
        else if (browserActionItem.getRunnableAction() != null) {
            browserActionItem.getRunnableAction().run();
        }
        final BrowserActionsFallbackMenuDialog mBrowserActionsDialog = this.mBrowserActionsDialog;
        if (mBrowserActionsDialog == null) {
            Log.e("BrowserActionskMenuUi", "Cannot dismiss dialog, it has already been dismissed.");
            return;
        }
        mBrowserActionsDialog.dismiss();
    }
    
    void setMenuUiListener(final BrowserActionsFallMenuUiListener mMenuUiListener) {
        this.mMenuUiListener = mMenuUiListener;
    }
    
    interface BrowserActionsFallMenuUiListener
    {
        void onMenuShown(final View p0);
    }
}

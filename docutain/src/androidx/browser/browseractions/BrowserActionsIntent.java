// 
// Decompiled by Procyon v0.6.0
// 

package androidx.browser.browseractions;

import java.util.Collection;
import java.util.Arrays;
import android.os.Parcelable;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;
import android.text.TextUtils;
import java.util.ArrayList;
import android.os.Bundle;
import androidx.core.content.ContextCompat;
import android.app.PendingIntent;
import android.net.Uri;
import android.content.pm.ResolveInfo;
import java.util.List;
import android.content.Context;
import android.content.Intent;

@Deprecated
public class BrowserActionsIntent
{
    public static final String ACTION_BROWSER_ACTIONS_OPEN = "androidx.browser.browseractions.browser_action_open";
    public static final String EXTRA_APP_ID = "androidx.browser.browseractions.APP_ID";
    public static final String EXTRA_MENU_ITEMS = "androidx.browser.browseractions.extra.MENU_ITEMS";
    public static final String EXTRA_SELECTED_ACTION_PENDING_INTENT = "androidx.browser.browseractions.extra.SELECTED_ACTION_PENDING_INTENT";
    public static final String EXTRA_TYPE = "androidx.browser.browseractions.extra.TYPE";
    public static final int ITEM_COPY = 3;
    public static final int ITEM_DOWNLOAD = 2;
    public static final int ITEM_INVALID_ITEM = -1;
    public static final int ITEM_OPEN_IN_INCOGNITO = 1;
    public static final int ITEM_OPEN_IN_NEW_TAB = 0;
    public static final int ITEM_SHARE = 4;
    public static final String KEY_ACTION = "androidx.browser.browseractions.ACTION";
    public static final String KEY_ICON_ID = "androidx.browser.browseractions.ICON_ID";
    private static final String KEY_ICON_URI = "androidx.browser.browseractions.ICON_URI";
    public static final String KEY_TITLE = "androidx.browser.browseractions.TITLE";
    public static final int MAX_CUSTOM_ITEMS = 5;
    private static final String TAG = "BrowserActions";
    private static final String TEST_URL = "https://www.example.com";
    public static final int URL_TYPE_AUDIO = 3;
    public static final int URL_TYPE_FILE = 4;
    public static final int URL_TYPE_IMAGE = 1;
    public static final int URL_TYPE_NONE = 0;
    public static final int URL_TYPE_PLUGIN = 5;
    public static final int URL_TYPE_VIDEO = 2;
    private static BrowserActionsFallDialogListener sDialogListenter;
    private final Intent mIntent;
    
    BrowserActionsIntent(final Intent mIntent) {
        this.mIntent = mIntent;
    }
    
    public static List<ResolveInfo> getBrowserActionsIntentHandlers(final Context context) {
        return context.getPackageManager().queryIntentActivities(new Intent("androidx.browser.browseractions.browser_action_open", Uri.parse("https://www.example.com")), 131072);
    }
    
    @Deprecated
    public static String getCreatorPackageName(final Intent intent) {
        return getUntrustedCreatorPackageName(intent);
    }
    
    public static String getUntrustedCreatorPackageName(final Intent intent) {
        final PendingIntent pendingIntent = (PendingIntent)intent.getParcelableExtra("androidx.browser.browseractions.APP_ID");
        if (pendingIntent != null) {
            return pendingIntent.getTargetPackage();
        }
        return null;
    }
    
    public static void launchIntent(final Context context, final Intent intent) {
        launchIntent(context, intent, getBrowserActionsIntentHandlers(context));
    }
    
    static void launchIntent(final Context context, final Intent intent, final List<ResolveInfo> list) {
        if (list != null && list.size() != 0) {
            final int size = list.size();
            int i = 0;
            if (size == 1) {
                intent.setPackage(((ResolveInfo)list.get(0)).activityInfo.packageName);
            }
            else {
                final ResolveInfo resolveActivity = context.getPackageManager().resolveActivity(new Intent("android.intent.action.VIEW", Uri.parse("https://www.example.com")), 65536);
                if (resolveActivity != null) {
                    final String packageName = resolveActivity.activityInfo.packageName;
                    while (i < list.size()) {
                        if (packageName.equals(list.get(i).activityInfo.packageName)) {
                            intent.setPackage(packageName);
                            break;
                        }
                        ++i;
                    }
                }
            }
            ContextCompat.startActivity(context, intent, null);
            return;
        }
        openFallbackBrowserActionsMenu(context, intent);
    }
    
    public static void openBrowserAction(final Context context, final Uri uri) {
        launchIntent(context, new Builder(context, uri).build().getIntent());
    }
    
    public static void openBrowserAction(final Context context, final Uri uri, final int urlType, final ArrayList<BrowserActionItem> customItems, final PendingIntent onItemSelectedAction) {
        launchIntent(context, new Builder(context, uri).setUrlType(urlType).setCustomItems(customItems).setOnItemSelectedAction(onItemSelectedAction).build().getIntent());
    }
    
    private static void openFallbackBrowserActionsMenu(final Context context, final Intent intent) {
        final Uri data = intent.getData();
        final ArrayList parcelableArrayListExtra = intent.getParcelableArrayListExtra("androidx.browser.browseractions.extra.MENU_ITEMS");
        List<BrowserActionItem> browserActionItems;
        if (parcelableArrayListExtra != null) {
            browserActionItems = parseBrowserActionItems(parcelableArrayListExtra);
        }
        else {
            browserActionItems = null;
        }
        openFallbackBrowserActionsMenu(context, data, browserActionItems);
    }
    
    private static void openFallbackBrowserActionsMenu(final Context context, final Uri uri, final List<BrowserActionItem> list) {
        new BrowserActionsFallbackMenuUi(context, uri, list).displayMenu();
        final BrowserActionsFallDialogListener sDialogListenter = BrowserActionsIntent.sDialogListenter;
        if (sDialogListenter != null) {
            sDialogListenter.onDialogShown();
        }
    }
    
    public static List<BrowserActionItem> parseBrowserActionItems(final ArrayList<Bundle> list) {
        final ArrayList list2 = new ArrayList();
        for (int i = 0; i < list.size(); ++i) {
            final Bundle bundle = list.get(i);
            final String string = bundle.getString("androidx.browser.browseractions.TITLE");
            final PendingIntent pendingIntent = (PendingIntent)bundle.getParcelable("androidx.browser.browseractions.ACTION");
            final int int1 = bundle.getInt("androidx.browser.browseractions.ICON_ID");
            final Uri uri = (Uri)bundle.getParcelable("androidx.browser.browseractions.ICON_URI");
            if (TextUtils.isEmpty((CharSequence)string) || pendingIntent == null) {
                throw new IllegalArgumentException("Custom item should contain a non-empty title and non-null intent.");
            }
            BrowserActionItem browserActionItem;
            if (int1 != 0) {
                browserActionItem = new BrowserActionItem(string, pendingIntent, int1);
            }
            else {
                browserActionItem = new BrowserActionItem(string, pendingIntent, uri);
            }
            list2.add(browserActionItem);
        }
        return list2;
    }
    
    static void setDialogShownListenter(final BrowserActionsFallDialogListener sDialogListenter) {
        BrowserActionsIntent.sDialogListenter = sDialogListenter;
    }
    
    public Intent getIntent() {
        return this.mIntent;
    }
    
    interface BrowserActionsFallDialogListener
    {
        void onDialogShown();
    }
    
    @Retention(RetentionPolicy.SOURCE)
    public @interface BrowserActionsItemId {
    }
    
    @Retention(RetentionPolicy.SOURCE)
    public @interface BrowserActionsUrlType {
    }
    
    public static final class Builder
    {
        private Context mContext;
        private List<Uri> mImageUris;
        private final Intent mIntent;
        private ArrayList<Bundle> mMenuItems;
        private PendingIntent mOnItemSelectedPendingIntent;
        private int mType;
        private Uri mUri;
        
        public Builder(final Context mContext, final Uri mUri) {
            this.mIntent = new Intent("androidx.browser.browseractions.browser_action_open");
            this.mType = 0;
            this.mMenuItems = new ArrayList<Bundle>();
            this.mOnItemSelectedPendingIntent = null;
            this.mImageUris = new ArrayList<Uri>();
            this.mContext = mContext;
            this.mUri = mUri;
        }
        
        private Bundle getBundleFromItem(final BrowserActionItem browserActionItem) {
            final Bundle bundle = new Bundle();
            bundle.putString("androidx.browser.browseractions.TITLE", browserActionItem.getTitle());
            bundle.putParcelable("androidx.browser.browseractions.ACTION", (Parcelable)browserActionItem.getAction());
            if (browserActionItem.getIconId() != 0) {
                bundle.putInt("androidx.browser.browseractions.ICON_ID", browserActionItem.getIconId());
            }
            if (browserActionItem.getIconUri() != null) {
                bundle.putParcelable("androidx.browser.browseractions.ICON_URI", (Parcelable)browserActionItem.getIconUri());
            }
            return bundle;
        }
        
        public BrowserActionsIntent build() {
            this.mIntent.setData(this.mUri);
            this.mIntent.putExtra("androidx.browser.browseractions.extra.TYPE", this.mType);
            this.mIntent.putParcelableArrayListExtra("androidx.browser.browseractions.extra.MENU_ITEMS", (ArrayList)this.mMenuItems);
            this.mIntent.putExtra("androidx.browser.browseractions.APP_ID", (Parcelable)PendingIntent.getActivity(this.mContext, 0, new Intent(), 67108864));
            final PendingIntent mOnItemSelectedPendingIntent = this.mOnItemSelectedPendingIntent;
            if (mOnItemSelectedPendingIntent != null) {
                this.mIntent.putExtra("androidx.browser.browseractions.extra.SELECTED_ACTION_PENDING_INTENT", (Parcelable)mOnItemSelectedPendingIntent);
            }
            BrowserServiceFileProvider.grantReadPermission(this.mIntent, this.mImageUris, this.mContext);
            return new BrowserActionsIntent(this.mIntent);
        }
        
        public Builder setCustomItems(final ArrayList<BrowserActionItem> list) {
            if (list.size() <= 5) {
                for (int i = 0; i < list.size(); ++i) {
                    if (TextUtils.isEmpty((CharSequence)((BrowserActionItem)list.get(i)).getTitle()) || ((BrowserActionItem)list.get(i)).getAction() == null) {
                        throw new IllegalArgumentException("Custom item should contain a non-empty title and non-null intent.");
                    }
                    this.mMenuItems.add(this.getBundleFromItem((BrowserActionItem)list.get(i)));
                    if (((BrowserActionItem)list.get(i)).getIconUri() != null) {
                        this.mImageUris.add(((BrowserActionItem)list.get(i)).getIconUri());
                    }
                }
                return this;
            }
            throw new IllegalStateException("Exceeded maximum toolbar item count of 5");
        }
        
        public Builder setCustomItems(final BrowserActionItem... a) {
            return this.setCustomItems(new ArrayList<BrowserActionItem>(Arrays.asList(a)));
        }
        
        public Builder setOnItemSelectedAction(final PendingIntent mOnItemSelectedPendingIntent) {
            this.mOnItemSelectedPendingIntent = mOnItemSelectedPendingIntent;
            return this;
        }
        
        public Builder setUrlType(final int mType) {
            this.mType = mType;
            return this;
        }
    }
}

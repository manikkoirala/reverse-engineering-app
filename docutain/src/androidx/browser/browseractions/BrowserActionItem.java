// 
// Decompiled by Procyon v0.6.0
// 

package androidx.browser.browseractions;

import android.net.Uri;
import android.app.PendingIntent;

@Deprecated
public class BrowserActionItem
{
    private final PendingIntent mAction;
    private int mIconId;
    private Uri mIconUri;
    private Runnable mRunnableAction;
    private final String mTitle;
    
    public BrowserActionItem(final String s, final PendingIntent pendingIntent) {
        this(s, pendingIntent, 0);
    }
    
    public BrowserActionItem(final String mTitle, final PendingIntent mAction, final int mIconId) {
        this.mTitle = mTitle;
        this.mAction = mAction;
        this.mIconId = mIconId;
    }
    
    public BrowserActionItem(final String mTitle, final PendingIntent mAction, final Uri mIconUri) {
        this.mTitle = mTitle;
        this.mAction = mAction;
        this.mIconUri = mIconUri;
    }
    
    BrowserActionItem(final String mTitle, final Runnable mRunnableAction) {
        this.mTitle = mTitle;
        this.mAction = null;
        this.mRunnableAction = mRunnableAction;
    }
    
    public PendingIntent getAction() {
        final PendingIntent mAction = this.mAction;
        if (mAction != null) {
            return mAction;
        }
        throw new IllegalStateException("Can't call getAction on BrowserActionItem with null action.");
    }
    
    public int getIconId() {
        return this.mIconId;
    }
    
    public Uri getIconUri() {
        return this.mIconUri;
    }
    
    Runnable getRunnableAction() {
        return this.mRunnableAction;
    }
    
    public String getTitle() {
        return this.mTitle;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.browser.trusted.splashscreens;

public final class SplashScreenVersion
{
    public static final String V1 = "androidx.browser.trusted.category.TrustedWebActivitySplashScreensV1";
    
    private SplashScreenVersion() {
    }
}

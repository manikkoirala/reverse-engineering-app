// 
// Decompiled by Procyon v0.6.0
// 

package androidx.browser.trusted;

public interface TokenStore
{
    Token load();
    
    void store(final Token p0);
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.browser.trusted;

import androidx.concurrent.futures.ResolvableFuture;
import com.google.common.util.concurrent.ListenableFuture;

class FutureUtils
{
    private FutureUtils() {
    }
    
    static <T> ListenableFuture<T> immediateFailedFuture(final Throwable exception) {
        final ResolvableFuture<Object> create = ResolvableFuture.create();
        create.setException(exception);
        return (ListenableFuture<T>)create;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.browser.trusted;

import android.app.Notification;
import android.content.pm.ServiceInfo;
import android.content.pm.PackageManager$NameNotFoundException;
import android.content.ComponentName;
import android.graphics.BitmapFactory;
import android.os.Parcelable;
import android.content.Intent;
import android.os.Build$VERSION;
import android.content.Context;
import androidx.core.app.NotificationManagerCompat;
import java.util.Locale;
import android.os.IBinder;
import android.os.Bundle;
import android.content.pm.PackageManager;
import android.app.NotificationManager;
import android.support.customtabs.trusted.ITrustedWebActivityService;
import android.app.Service;

public abstract class TrustedWebActivityService extends Service
{
    public static final String ACTION_TRUSTED_WEB_ACTIVITY_SERVICE = "android.support.customtabs.trusted.TRUSTED_WEB_ACTIVITY_SERVICE";
    public static final String KEY_SMALL_ICON_BITMAP = "android.support.customtabs.trusted.SMALL_ICON_BITMAP";
    public static final String KEY_SUCCESS = "androidx.browser.trusted.SUCCESS";
    public static final String META_DATA_NAME_SMALL_ICON = "android.support.customtabs.trusted.SMALL_ICON";
    public static final int SMALL_ICON_NOT_SET = -1;
    private final ITrustedWebActivityService.Stub mBinder;
    private NotificationManager mNotificationManager;
    int mVerifiedUid;
    
    public TrustedWebActivityService() {
        this.mVerifiedUid = -1;
        this.mBinder = new ITrustedWebActivityService.Stub() {
            final TrustedWebActivityService this$0;
            
            private void checkCaller() {
                if (this.this$0.mVerifiedUid == -1) {
                    final String[] packagesForUid = this.this$0.getPackageManager().getPackagesForUid(getCallingUid());
                    int i = 0;
                    String[] array;
                    if ((array = packagesForUid) == null) {
                        array = new String[0];
                    }
                    final Token load = this.this$0.getTokenStore().load();
                    final PackageManager packageManager = this.this$0.getPackageManager();
                    if (load != null) {
                        while (i < array.length) {
                            if (load.matches(array[i], packageManager)) {
                                this.this$0.mVerifiedUid = getCallingUid();
                                break;
                            }
                            ++i;
                        }
                    }
                }
                if (this.this$0.mVerifiedUid == getCallingUid()) {
                    return;
                }
                throw new SecurityException("Caller is not verified as Trusted Web Activity provider.");
            }
            
            public Bundle areNotificationsEnabled(final Bundle bundle) {
                this.checkCaller();
                return new TrustedWebActivityServiceConnection.ResultArgs(this.this$0.onAreNotificationsEnabled(TrustedWebActivityServiceConnection.NotificationsEnabledArgs.fromBundle(bundle).channelName)).toBundle();
            }
            
            public void cancelNotification(final Bundle bundle) {
                this.checkCaller();
                final TrustedWebActivityServiceConnection.CancelNotificationArgs fromBundle = TrustedWebActivityServiceConnection.CancelNotificationArgs.fromBundle(bundle);
                this.this$0.onCancelNotification(fromBundle.platformTag, fromBundle.platformId);
            }
            
            public Bundle extraCommand(final String s, final Bundle bundle, final IBinder binder) {
                this.checkCaller();
                return this.this$0.onExtraCommand(s, bundle, TrustedWebActivityCallbackRemote.fromBinder(binder));
            }
            
            public Bundle getActiveNotifications() {
                this.checkCaller();
                return new TrustedWebActivityServiceConnection.ActiveNotificationsArgs(this.this$0.onGetActiveNotifications()).toBundle();
            }
            
            public Bundle getSmallIconBitmap() {
                this.checkCaller();
                return this.this$0.onGetSmallIconBitmap();
            }
            
            public int getSmallIconId() {
                this.checkCaller();
                return this.this$0.onGetSmallIconId();
            }
            
            public Bundle notifyNotificationWithChannel(final Bundle bundle) {
                this.checkCaller();
                final TrustedWebActivityServiceConnection.NotifyNotificationArgs fromBundle = TrustedWebActivityServiceConnection.NotifyNotificationArgs.fromBundle(bundle);
                return new TrustedWebActivityServiceConnection.ResultArgs(this.this$0.onNotifyNotificationWithChannel(fromBundle.platformTag, fromBundle.platformId, fromBundle.notification, fromBundle.channelName)).toBundle();
            }
        };
    }
    
    private static String channelNameToId(final String s) {
        final StringBuilder sb = new StringBuilder();
        sb.append(s.toLowerCase(Locale.ROOT).replace(' ', '_'));
        sb.append("_channel_id");
        return sb.toString();
    }
    
    private void ensureOnCreateCalled() {
        if (this.mNotificationManager != null) {
            return;
        }
        throw new IllegalStateException("TrustedWebActivityService has not been properly initialized. Did onCreate() call super.onCreate()?");
    }
    
    public abstract TokenStore getTokenStore();
    
    public boolean onAreNotificationsEnabled(final String s) {
        this.ensureOnCreateCalled();
        return NotificationManagerCompat.from((Context)this).areNotificationsEnabled() && (Build$VERSION.SDK_INT < 26 || NotificationApiHelperForO.isChannelEnabled(this.mNotificationManager, channelNameToId(s)));
    }
    
    public final IBinder onBind(final Intent intent) {
        return (IBinder)this.mBinder;
    }
    
    public void onCancelNotification(final String s, final int n) {
        this.ensureOnCreateCalled();
        this.mNotificationManager.cancel(s, n);
    }
    
    public void onCreate() {
        super.onCreate();
        this.mNotificationManager = (NotificationManager)this.getSystemService("notification");
    }
    
    public Bundle onExtraCommand(final String s, final Bundle bundle, final TrustedWebActivityCallbackRemote trustedWebActivityCallbackRemote) {
        return null;
    }
    
    public Parcelable[] onGetActiveNotifications() {
        this.ensureOnCreateCalled();
        if (Build$VERSION.SDK_INT >= 23) {
            return NotificationApiHelperForM.getActiveNotifications(this.mNotificationManager);
        }
        throw new IllegalStateException("onGetActiveNotifications cannot be called pre-M.");
    }
    
    public Bundle onGetSmallIconBitmap() {
        final int onGetSmallIconId = this.onGetSmallIconId();
        final Bundle bundle = new Bundle();
        if (onGetSmallIconId == -1) {
            return bundle;
        }
        bundle.putParcelable("android.support.customtabs.trusted.SMALL_ICON_BITMAP", (Parcelable)BitmapFactory.decodeResource(this.getResources(), onGetSmallIconId));
        return bundle;
    }
    
    public int onGetSmallIconId() {
        int int1 = -1;
        try {
            final ServiceInfo serviceInfo = this.getPackageManager().getServiceInfo(new ComponentName((Context)this, (Class)this.getClass()), 128);
            if (serviceInfo.metaData == null) {
                return -1;
            }
            int1 = serviceInfo.metaData.getInt("android.support.customtabs.trusted.SMALL_ICON", -1);
            return int1;
        }
        catch (final PackageManager$NameNotFoundException ex) {
            return int1;
        }
    }
    
    public boolean onNotifyNotificationWithChannel(final String s, final int n, final Notification notification, final String s2) {
        this.ensureOnCreateCalled();
        if (!NotificationManagerCompat.from((Context)this).areNotificationsEnabled()) {
            return false;
        }
        Notification copyNotificationOntoChannel = notification;
        if (Build$VERSION.SDK_INT >= 26) {
            final String channelNameToId = channelNameToId(s2);
            copyNotificationOntoChannel = NotificationApiHelperForO.copyNotificationOntoChannel((Context)this, this.mNotificationManager, notification, channelNameToId, s2);
            if (!NotificationApiHelperForO.isChannelEnabled(this.mNotificationManager, channelNameToId)) {
                return false;
            }
        }
        this.mNotificationManager.notify(s, n, copyNotificationOntoChannel);
        return true;
    }
    
    public final boolean onUnbind(final Intent intent) {
        this.mVerifiedUid = -1;
        return super.onUnbind(intent);
    }
}

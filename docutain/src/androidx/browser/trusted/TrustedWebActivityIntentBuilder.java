// 
// Decompiled by Procyon v0.6.0
// 

package androidx.browser.trusted;

import androidx.browser.customtabs.CustomTabColorSchemeParams;
import android.content.Intent;
import java.util.Collections;
import java.io.Serializable;
import java.util.Collection;
import java.util.ArrayList;
import androidx.browser.customtabs.CustomTabsSession;
import android.net.Uri;
import android.os.Bundle;
import androidx.browser.trusted.sharing.ShareTarget;
import androidx.browser.trusted.sharing.ShareData;
import androidx.browser.customtabs.CustomTabsIntent;
import java.util.List;

public class TrustedWebActivityIntentBuilder
{
    public static final String EXTRA_ADDITIONAL_TRUSTED_ORIGINS = "android.support.customtabs.extra.ADDITIONAL_TRUSTED_ORIGINS";
    public static final String EXTRA_DISPLAY_MODE = "androidx.browser.trusted.extra.DISPLAY_MODE";
    public static final String EXTRA_SCREEN_ORIENTATION = "androidx.browser.trusted.extra.SCREEN_ORIENTATION";
    public static final String EXTRA_SHARE_DATA = "androidx.browser.trusted.extra.SHARE_DATA";
    public static final String EXTRA_SHARE_TARGET = "androidx.browser.trusted.extra.SHARE_TARGET";
    public static final String EXTRA_SPLASH_SCREEN_PARAMS = "androidx.browser.trusted.EXTRA_SPLASH_SCREEN_PARAMS";
    private List<String> mAdditionalTrustedOrigins;
    private TrustedWebActivityDisplayMode mDisplayMode;
    private final CustomTabsIntent.Builder mIntentBuilder;
    private int mScreenOrientation;
    private ShareData mShareData;
    private ShareTarget mShareTarget;
    private Bundle mSplashScreenParams;
    private final Uri mUri;
    
    public TrustedWebActivityIntentBuilder(final Uri mUri) {
        this.mIntentBuilder = new CustomTabsIntent.Builder();
        this.mDisplayMode = new TrustedWebActivityDisplayMode.DefaultMode();
        this.mScreenOrientation = 0;
        this.mUri = mUri;
    }
    
    public TrustedWebActivityIntent build(final CustomTabsSession session) {
        if (session != null) {
            this.mIntentBuilder.setSession(session);
            final Intent intent = this.mIntentBuilder.build().intent;
            intent.setData(this.mUri);
            intent.putExtra("android.support.customtabs.extra.LAUNCH_AS_TRUSTED_WEB_ACTIVITY", true);
            if (this.mAdditionalTrustedOrigins != null) {
                intent.putExtra("android.support.customtabs.extra.ADDITIONAL_TRUSTED_ORIGINS", (Serializable)new ArrayList<Object>(this.mAdditionalTrustedOrigins));
            }
            final Bundle mSplashScreenParams = this.mSplashScreenParams;
            if (mSplashScreenParams != null) {
                intent.putExtra("androidx.browser.trusted.EXTRA_SPLASH_SCREEN_PARAMS", mSplashScreenParams);
            }
            final List<Object> emptyList = (List<Object>)Collections.emptyList();
            final ShareTarget mShareTarget = this.mShareTarget;
            Object uris = emptyList;
            if (mShareTarget != null) {
                uris = emptyList;
                if (this.mShareData != null) {
                    intent.putExtra("androidx.browser.trusted.extra.SHARE_TARGET", mShareTarget.toBundle());
                    intent.putExtra("androidx.browser.trusted.extra.SHARE_DATA", this.mShareData.toBundle());
                    uris = emptyList;
                    if (this.mShareData.uris != null) {
                        uris = this.mShareData.uris;
                    }
                }
            }
            intent.putExtra("androidx.browser.trusted.extra.DISPLAY_MODE", this.mDisplayMode.toBundle());
            intent.putExtra("androidx.browser.trusted.extra.SCREEN_ORIENTATION", this.mScreenOrientation);
            return new TrustedWebActivityIntent(intent, (List<Uri>)uris);
        }
        throw new NullPointerException("CustomTabsSession is required for launching a TWA");
    }
    
    public CustomTabsIntent buildCustomTabsIntent() {
        return this.mIntentBuilder.build();
    }
    
    public TrustedWebActivityDisplayMode getDisplayMode() {
        return this.mDisplayMode;
    }
    
    public Uri getUri() {
        return this.mUri;
    }
    
    public TrustedWebActivityIntentBuilder setAdditionalTrustedOrigins(final List<String> mAdditionalTrustedOrigins) {
        this.mAdditionalTrustedOrigins = mAdditionalTrustedOrigins;
        return this;
    }
    
    public TrustedWebActivityIntentBuilder setColorScheme(final int colorScheme) {
        this.mIntentBuilder.setColorScheme(colorScheme);
        return this;
    }
    
    public TrustedWebActivityIntentBuilder setColorSchemeParams(final int n, final CustomTabColorSchemeParams customTabColorSchemeParams) {
        this.mIntentBuilder.setColorSchemeParams(n, customTabColorSchemeParams);
        return this;
    }
    
    public TrustedWebActivityIntentBuilder setDefaultColorSchemeParams(final CustomTabColorSchemeParams defaultColorSchemeParams) {
        this.mIntentBuilder.setDefaultColorSchemeParams(defaultColorSchemeParams);
        return this;
    }
    
    public TrustedWebActivityIntentBuilder setDisplayMode(final TrustedWebActivityDisplayMode mDisplayMode) {
        this.mDisplayMode = mDisplayMode;
        return this;
    }
    
    public TrustedWebActivityIntentBuilder setNavigationBarColor(final int navigationBarColor) {
        this.mIntentBuilder.setNavigationBarColor(navigationBarColor);
        return this;
    }
    
    public TrustedWebActivityIntentBuilder setNavigationBarDividerColor(final int navigationBarDividerColor) {
        this.mIntentBuilder.setNavigationBarDividerColor(navigationBarDividerColor);
        return this;
    }
    
    public TrustedWebActivityIntentBuilder setScreenOrientation(final int mScreenOrientation) {
        this.mScreenOrientation = mScreenOrientation;
        return this;
    }
    
    public TrustedWebActivityIntentBuilder setShareParams(final ShareTarget mShareTarget, final ShareData mShareData) {
        this.mShareTarget = mShareTarget;
        this.mShareData = mShareData;
        return this;
    }
    
    public TrustedWebActivityIntentBuilder setSplashScreenParams(final Bundle mSplashScreenParams) {
        this.mSplashScreenParams = mSplashScreenParams;
        return this;
    }
    
    public TrustedWebActivityIntentBuilder setToolbarColor(final int toolbarColor) {
        this.mIntentBuilder.setToolbarColor(toolbarColor);
        return this;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.browser.trusted;

import android.os.Bundle;
import androidx.core.content.ContextCompat;
import java.util.Iterator;
import android.content.Context;
import android.net.Uri;
import java.util.List;
import android.content.Intent;

public final class TrustedWebActivityIntent
{
    private final Intent mIntent;
    private final List<Uri> mSharedFileUris;
    
    TrustedWebActivityIntent(final Intent mIntent, final List<Uri> mSharedFileUris) {
        this.mIntent = mIntent;
        this.mSharedFileUris = mSharedFileUris;
    }
    
    private void grantUriPermissionToProvider(final Context context) {
        final Iterator<Uri> iterator = this.mSharedFileUris.iterator();
        while (iterator.hasNext()) {
            context.grantUriPermission(this.mIntent.getPackage(), (Uri)iterator.next(), 1);
        }
    }
    
    public Intent getIntent() {
        return this.mIntent;
    }
    
    public void launchTrustedWebActivity(final Context context) {
        this.grantUriPermissionToProvider(context);
        ContextCompat.startActivity(context, this.mIntent, null);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.browser.trusted;

import android.os.AsyncTask;
import android.content.ServiceConnection;
import com.google.common.util.concurrent.ListenableFuture;
import java.util.concurrent.Executor;
import java.util.Iterator;
import android.content.ComponentName;
import android.util.Log;
import android.content.pm.ResolveInfo;
import android.content.Intent;
import java.util.Set;
import java.util.HashMap;
import android.content.Context;
import android.net.Uri;
import java.util.Map;

public final class TrustedWebActivityServiceConnectionPool
{
    private static final String TAG = "TWAConnectionPool";
    private final Map<Uri, ConnectionHolder> mConnections;
    private final Context mContext;
    
    private TrustedWebActivityServiceConnectionPool(final Context context) {
        this.mConnections = new HashMap<Uri, ConnectionHolder>();
        this.mContext = context.getApplicationContext();
    }
    
    public static TrustedWebActivityServiceConnectionPool create(final Context context) {
        return new TrustedWebActivityServiceConnectionPool(context);
    }
    
    private Intent createServiceIntent(final Context context, final Uri obj, final Set<Token> set, final boolean b) {
        if (set == null || set.size() == 0) {
            return null;
        }
        final Intent intent = new Intent();
        intent.setData(obj);
        intent.setAction("android.intent.action.VIEW");
        final Iterator iterator = context.getPackageManager().queryIntentActivities(intent, 65536).iterator();
        String s = null;
        while (iterator.hasNext()) {
            final String packageName = ((ResolveInfo)iterator.next()).activityInfo.packageName;
            final Iterator iterator2 = set.iterator();
            while (iterator2.hasNext()) {
                if (((Token)iterator2.next()).matches(packageName, context.getPackageManager())) {
                    s = packageName;
                    break;
                }
            }
        }
        if (s == null) {
            if (b) {
                final StringBuilder sb = new StringBuilder();
                sb.append("No TWA candidates for ");
                sb.append(obj);
                sb.append(" have been registered.");
                Log.w("TWAConnectionPool", sb.toString());
            }
            return null;
        }
        final Intent intent2 = new Intent();
        intent2.setPackage(s);
        intent2.setAction("android.support.customtabs.trusted.TRUSTED_WEB_ACTIVITY_SERVICE");
        final ResolveInfo resolveService = context.getPackageManager().resolveService(intent2, 131072);
        if (resolveService == null) {
            if (b) {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("Could not find TWAService for ");
                sb2.append(s);
                Log.w("TWAConnectionPool", sb2.toString());
            }
            return null;
        }
        if (b) {
            final StringBuilder sb3 = new StringBuilder();
            sb3.append("Found ");
            sb3.append(resolveService.serviceInfo.name);
            sb3.append(" to handle request for ");
            sb3.append(obj);
            Log.i("TWAConnectionPool", sb3.toString());
        }
        final Intent intent3 = new Intent();
        intent3.setComponent(new ComponentName(s, resolveService.serviceInfo.name));
        return intent3;
    }
    
    public ListenableFuture<TrustedWebActivityServiceConnection> connect(final Uri uri, final Set<Token> set, final Executor executor) {
        final ConnectionHolder connectionHolder = this.mConnections.get(uri);
        if (connectionHolder != null) {
            return connectionHolder.getServiceWrapper();
        }
        final Intent serviceIntent = this.createServiceIntent(this.mContext, uri, set, true);
        if (serviceIntent == null) {
            return FutureUtils.immediateFailedFuture(new IllegalArgumentException("No service exists for scope"));
        }
        final ConnectionHolder connectionHolder2 = new ConnectionHolder(new TrustedWebActivityServiceConnectionPool$$ExternalSyntheticLambda0(this, uri));
        this.mConnections.put(uri, connectionHolder2);
        new BindToServiceAsyncTask(this.mContext, serviceIntent, connectionHolder2).executeOnExecutor(executor, (Object[])new Void[0]);
        return connectionHolder2.getServiceWrapper();
    }
    
    public boolean serviceExistsForScope(final Uri uri, final Set<Token> set) {
        final ConnectionHolder value = this.mConnections.get(uri);
        boolean b = true;
        if (value != null) {
            return true;
        }
        if (this.createServiceIntent(this.mContext, uri, set, false) == null) {
            b = false;
        }
        return b;
    }
    
    void unbindAllConnections() {
        final Iterator<ConnectionHolder> iterator = this.mConnections.values().iterator();
        while (iterator.hasNext()) {
            this.mContext.unbindService((ServiceConnection)iterator.next());
        }
        this.mConnections.clear();
    }
    
    static class BindToServiceAsyncTask extends AsyncTask<Void, Void, Exception>
    {
        private final Context mAppContext;
        private final ConnectionHolder mConnection;
        private final Intent mIntent;
        
        BindToServiceAsyncTask(final Context context, final Intent mIntent, final ConnectionHolder mConnection) {
            this.mAppContext = context.getApplicationContext();
            this.mIntent = mIntent;
            this.mConnection = mConnection;
        }
        
        protected Exception doInBackground(final Void... array) {
            try {
                if (this.mAppContext.bindService(this.mIntent, (ServiceConnection)this.mConnection, 4097)) {
                    return null;
                }
                this.mAppContext.unbindService((ServiceConnection)this.mConnection);
                return new IllegalStateException("Could not bind to the service");
            }
            catch (final SecurityException ex) {
                Log.w("TWAConnectionPool", "SecurityException while binding.", (Throwable)ex);
                return ex;
            }
        }
        
        protected void onPostExecute(final Exception ex) {
            if (ex != null) {
                this.mConnection.cancel(ex);
            }
        }
    }
}

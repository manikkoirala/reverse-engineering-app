// 
// Decompiled by Procyon v0.6.0
// 

package androidx.browser.trusted;

import android.app.Notification$Builder;
import android.app.NotificationChannel;
import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;

class NotificationApiHelperForO
{
    private NotificationApiHelperForO() {
    }
    
    static Notification copyNotificationOntoChannel(final Context context, final NotificationManager notificationManager, final Notification notification, final String channelId, final String s) {
        notificationManager.createNotificationChannel(new NotificationChannel(channelId, (CharSequence)s, 3));
        if (notificationManager.getNotificationChannel(channelId).getImportance() == 0) {
            return null;
        }
        final Notification$Builder recoverBuilder = Notification$Builder.recoverBuilder(context, notification);
        recoverBuilder.setChannelId(channelId);
        return recoverBuilder.build();
    }
    
    static boolean isChannelEnabled(final NotificationManager notificationManager, final String s) {
        final NotificationChannel notificationChannel = notificationManager.getNotificationChannel(s);
        return notificationChannel == null || notificationChannel.getImportance() != 0;
    }
}

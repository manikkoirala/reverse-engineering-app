// 
// Decompiled by Procyon v0.6.0
// 

package androidx.browser.trusted;

import android.os.Bundle;

public abstract class TrustedWebActivityCallback
{
    public abstract void onExtraCallback(final String p0, final Bundle p1);
}

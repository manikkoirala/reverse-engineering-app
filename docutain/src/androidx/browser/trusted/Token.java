// 
// Decompiled by Procyon v0.6.0
// 

package androidx.browser.trusted;

import java.util.List;
import java.io.IOException;
import android.util.Log;
import android.content.pm.PackageManager;

public final class Token
{
    private static final String TAG = "Token";
    private final TokenContents mContents;
    
    private Token(final TokenContents mContents) {
        this.mContents = mContents;
    }
    
    public static Token create(final String s, final PackageManager packageManager) {
        final List<byte[]> fingerprintsForPackage = PackageIdentityUtils.getFingerprintsForPackage(s, packageManager);
        if (fingerprintsForPackage == null) {
            return null;
        }
        try {
            return new Token(TokenContents.create(s, fingerprintsForPackage));
        }
        catch (final IOException ex) {
            Log.e("Token", "Exception when creating token.", (Throwable)ex);
            return null;
        }
    }
    
    public static Token deserialize(final byte[] array) {
        return new Token(TokenContents.deserialize(array));
    }
    
    public boolean matches(final String s, final PackageManager packageManager) {
        return PackageIdentityUtils.packageMatchesToken(s, packageManager, this.mContents);
    }
    
    public byte[] serialize() {
        return this.mContents.serialize();
    }
}

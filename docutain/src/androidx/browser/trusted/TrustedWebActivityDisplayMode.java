// 
// Decompiled by Procyon v0.6.0
// 

package androidx.browser.trusted;

import android.os.Bundle;

public interface TrustedWebActivityDisplayMode
{
    public static final String KEY_ID = "androidx.browser.trusted.displaymode.KEY_ID";
    
    Bundle toBundle();
    
    public static class DefaultMode implements TrustedWebActivityDisplayMode
    {
        private static final int ID = 0;
        
        @Override
        public Bundle toBundle() {
            final Bundle bundle = new Bundle();
            bundle.putInt("androidx.browser.trusted.displaymode.KEY_ID", 0);
            return bundle;
        }
    }
    
    public static class ImmersiveMode implements TrustedWebActivityDisplayMode
    {
        private static final int ID = 1;
        public static final String KEY_CUTOUT_MODE = "androidx.browser.trusted.displaymode.KEY_CUTOUT_MODE";
        public static final String KEY_STICKY = "androidx.browser.trusted.displaymode.KEY_STICKY";
        private final boolean mIsSticky;
        private final int mLayoutInDisplayCutoutMode;
        
        public ImmersiveMode(final boolean mIsSticky, final int mLayoutInDisplayCutoutMode) {
            this.mIsSticky = mIsSticky;
            this.mLayoutInDisplayCutoutMode = mLayoutInDisplayCutoutMode;
        }
        
        static TrustedWebActivityDisplayMode fromBundle(final Bundle bundle) {
            return new ImmersiveMode(bundle.getBoolean("androidx.browser.trusted.displaymode.KEY_STICKY"), bundle.getInt("androidx.browser.trusted.displaymode.KEY_CUTOUT_MODE"));
        }
        
        public boolean isSticky() {
            return this.mIsSticky;
        }
        
        public int layoutInDisplayCutoutMode() {
            return this.mLayoutInDisplayCutoutMode;
        }
        
        @Override
        public Bundle toBundle() {
            final Bundle bundle = new Bundle();
            bundle.putInt("androidx.browser.trusted.displaymode.KEY_ID", 1);
            bundle.putBoolean("androidx.browser.trusted.displaymode.KEY_STICKY", this.mIsSticky);
            bundle.putInt("androidx.browser.trusted.displaymode.KEY_CUTOUT_MODE", this.mLayoutInDisplayCutoutMode);
            return bundle;
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.browser.trusted;

import android.os.Parcelable;
import android.app.NotificationManager;

public class NotificationApiHelperForM
{
    private NotificationApiHelperForM() {
    }
    
    static Parcelable[] getActiveNotifications(final NotificationManager notificationManager) {
        return (Parcelable[])notificationManager.getActiveNotifications();
    }
}

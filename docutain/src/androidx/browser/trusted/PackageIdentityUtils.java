// 
// Decompiled by Procyon v0.6.0
// 

package androidx.browser.trusted;

import android.content.pm.SigningInfo;
import android.content.pm.PackageInfo;
import java.util.ArrayList;
import java.io.IOException;
import android.os.Build$VERSION;
import android.content.pm.PackageManager$NameNotFoundException;
import android.util.Log;
import java.util.List;
import android.content.pm.PackageManager;
import java.security.NoSuchAlgorithmException;
import java.security.MessageDigest;
import android.content.pm.Signature;

class PackageIdentityUtils
{
    private static final String TAG = "PackageIdentity";
    
    private PackageIdentityUtils() {
    }
    
    static byte[] getCertificateSHA256Fingerprint(final Signature signature) {
        try {
            return MessageDigest.getInstance("SHA256").digest(signature.toByteArray());
        }
        catch (final NoSuchAlgorithmException ex) {
            return null;
        }
    }
    
    static List<byte[]> getFingerprintsForPackage(final String s, final PackageManager packageManager) {
        try {
            return getImpl().getFingerprintsForPackage(s, packageManager);
        }
        catch (final PackageManager$NameNotFoundException ex) {
            Log.e("PackageIdentity", "Could not get fingerprint for package.", (Throwable)ex);
            return null;
        }
    }
    
    private static SignaturesCompat getImpl() {
        if (Build$VERSION.SDK_INT >= 28) {
            return (SignaturesCompat)new Api28Implementation();
        }
        return (SignaturesCompat)new Pre28Implementation();
    }
    
    static boolean packageMatchesToken(final String ex, final PackageManager packageManager, final TokenContents tokenContents) {
        try {
            return getImpl().packageMatchesToken((String)ex, packageManager, tokenContents);
        }
        catch (final PackageManager$NameNotFoundException ex) {}
        catch (final IOException ex2) {}
        Log.e("PackageIdentity", "Could not check if package matches token.", (Throwable)ex);
        return false;
    }
    
    static class Api28Implementation implements SignaturesCompat
    {
        @Override
        public List<byte[]> getFingerprintsForPackage(final String s, final PackageManager packageManager) throws PackageManager$NameNotFoundException {
            final PackageInfo packageInfo = packageManager.getPackageInfo(s, 134217728);
            final ArrayList list = new ArrayList();
            final SigningInfo signingInfo = packageInfo.signingInfo;
            final boolean hasMultipleSigners = signingInfo.hasMultipleSigners();
            int i = 0;
            if (hasMultipleSigners) {
                for (Signature[] apkContentsSigners = signingInfo.getApkContentsSigners(); i < apkContentsSigners.length; ++i) {
                    list.add(PackageIdentityUtils.getCertificateSHA256Fingerprint(apkContentsSigners[i]));
                }
            }
            else {
                list.add(PackageIdentityUtils.getCertificateSHA256Fingerprint(signingInfo.getSigningCertificateHistory()[0]));
            }
            return list;
        }
        
        @Override
        public boolean packageMatchesToken(final String anObject, final PackageManager packageManager, final TokenContents tokenContents) throws PackageManager$NameNotFoundException, IOException {
            if (!tokenContents.getPackageName().equals(anObject)) {
                return false;
            }
            final List<byte[]> fingerprintsForPackage = this.getFingerprintsForPackage(anObject, packageManager);
            if (fingerprintsForPackage == null) {
                return false;
            }
            if (fingerprintsForPackage.size() == 1) {
                return packageManager.hasSigningCertificate(anObject, tokenContents.getFingerprint(0), 1);
            }
            return tokenContents.equals(TokenContents.create(anObject, fingerprintsForPackage));
        }
    }
    
    static class Pre28Implementation implements SignaturesCompat
    {
        @Override
        public List<byte[]> getFingerprintsForPackage(final String s, final PackageManager packageManager) throws PackageManager$NameNotFoundException {
            final PackageInfo packageInfo = packageManager.getPackageInfo(s, 64);
            final ArrayList list = new ArrayList(packageInfo.signatures.length);
            final Signature[] signatures = packageInfo.signatures;
            for (int length = signatures.length, i = 0; i < length; ++i) {
                final byte[] certificateSHA256Fingerprint = PackageIdentityUtils.getCertificateSHA256Fingerprint(signatures[i]);
                if (certificateSHA256Fingerprint == null) {
                    return null;
                }
                list.add((Object)certificateSHA256Fingerprint);
            }
            return (List<byte[]>)list;
        }
        
        @Override
        public boolean packageMatchesToken(final String s, final PackageManager packageManager, final TokenContents tokenContents) throws IOException, PackageManager$NameNotFoundException {
            if (!s.equals(tokenContents.getPackageName())) {
                return false;
            }
            final List<byte[]> fingerprintsForPackage = this.getFingerprintsForPackage(s, packageManager);
            return fingerprintsForPackage != null && tokenContents.equals(TokenContents.create(s, fingerprintsForPackage));
        }
    }
    
    interface SignaturesCompat
    {
        List<byte[]> getFingerprintsForPackage(final String p0, final PackageManager p1) throws PackageManager$NameNotFoundException;
        
        boolean packageMatchesToken(final String p0, final PackageManager p1, final TokenContents p2) throws IOException, PackageManager$NameNotFoundException;
    }
}

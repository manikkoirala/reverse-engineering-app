// 
// Decompiled by Procyon v0.6.0
// 

package androidx.browser.trusted.sharing;

import java.util.Iterator;
import java.util.Collection;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;
import android.os.Bundle;

public final class ShareTarget
{
    public static final String ENCODING_TYPE_MULTIPART = "multipart/form-data";
    public static final String ENCODING_TYPE_URL_ENCODED = "application/x-www-form-urlencoded";
    public static final String KEY_ACTION = "androidx.browser.trusted.sharing.KEY_ACTION";
    public static final String KEY_ENCTYPE = "androidx.browser.trusted.sharing.KEY_ENCTYPE";
    public static final String KEY_METHOD = "androidx.browser.trusted.sharing.KEY_METHOD";
    public static final String KEY_PARAMS = "androidx.browser.trusted.sharing.KEY_PARAMS";
    public static final String METHOD_GET = "GET";
    public static final String METHOD_POST = "POST";
    public final String action;
    public final String encodingType;
    public final String method;
    public final Params params;
    
    public ShareTarget(final String action, final String method, final String encodingType, final Params params) {
        this.action = action;
        this.method = method;
        this.encodingType = encodingType;
        this.params = params;
    }
    
    public static ShareTarget fromBundle(final Bundle bundle) {
        final String string = bundle.getString("androidx.browser.trusted.sharing.KEY_ACTION");
        final String string2 = bundle.getString("androidx.browser.trusted.sharing.KEY_METHOD");
        final String string3 = bundle.getString("androidx.browser.trusted.sharing.KEY_ENCTYPE");
        final Params fromBundle = Params.fromBundle(bundle.getBundle("androidx.browser.trusted.sharing.KEY_PARAMS"));
        if (string != null && fromBundle != null) {
            return new ShareTarget(string, string2, string3, fromBundle);
        }
        return null;
    }
    
    public Bundle toBundle() {
        final Bundle bundle = new Bundle();
        bundle.putString("androidx.browser.trusted.sharing.KEY_ACTION", this.action);
        bundle.putString("androidx.browser.trusted.sharing.KEY_METHOD", this.method);
        bundle.putString("androidx.browser.trusted.sharing.KEY_ENCTYPE", this.encodingType);
        bundle.putBundle("androidx.browser.trusted.sharing.KEY_PARAMS", this.params.toBundle());
        return bundle;
    }
    
    @Retention(RetentionPolicy.SOURCE)
    public @interface EncodingType {
    }
    
    public static final class FileFormField
    {
        public static final String KEY_ACCEPTED_TYPES = "androidx.browser.trusted.sharing.KEY_ACCEPTED_TYPES";
        public static final String KEY_NAME = "androidx.browser.trusted.sharing.KEY_FILE_NAME";
        public final List<String> acceptedTypes;
        public final String name;
        
        public FileFormField(final String name, final List<String> list) {
            this.name = name;
            this.acceptedTypes = Collections.unmodifiableList((List<? extends String>)list);
        }
        
        static FileFormField fromBundle(final Bundle bundle) {
            final FileFormField fileFormField = null;
            if (bundle == null) {
                return null;
            }
            final String string = bundle.getString("androidx.browser.trusted.sharing.KEY_FILE_NAME");
            final ArrayList stringArrayList = bundle.getStringArrayList("androidx.browser.trusted.sharing.KEY_ACCEPTED_TYPES");
            FileFormField fileFormField2 = fileFormField;
            if (string != null) {
                if (stringArrayList == null) {
                    fileFormField2 = fileFormField;
                }
                else {
                    fileFormField2 = new FileFormField(string, stringArrayList);
                }
            }
            return fileFormField2;
        }
        
        Bundle toBundle() {
            final Bundle bundle = new Bundle();
            bundle.putString("androidx.browser.trusted.sharing.KEY_FILE_NAME", this.name);
            bundle.putStringArrayList("androidx.browser.trusted.sharing.KEY_ACCEPTED_TYPES", new ArrayList((Collection<? extends E>)this.acceptedTypes));
            return bundle;
        }
    }
    
    public static class Params
    {
        public static final String KEY_FILES = "androidx.browser.trusted.sharing.KEY_FILES";
        public static final String KEY_TEXT = "androidx.browser.trusted.sharing.KEY_TEXT";
        public static final String KEY_TITLE = "androidx.browser.trusted.sharing.KEY_TITLE";
        public final List<FileFormField> files;
        public final String text;
        public final String title;
        
        public Params(final String title, final String text, final List<FileFormField> files) {
            this.title = title;
            this.text = text;
            this.files = files;
        }
        
        static Params fromBundle(final Bundle bundle) {
            List<FileFormField> list = null;
            if (bundle == null) {
                return null;
            }
            final ArrayList parcelableArrayList = bundle.getParcelableArrayList("androidx.browser.trusted.sharing.KEY_FILES");
            if (parcelableArrayList != null) {
                final ArrayList list2 = new ArrayList();
                final Iterator iterator = parcelableArrayList.iterator();
                while (true) {
                    list = list2;
                    if (!iterator.hasNext()) {
                        break;
                    }
                    list2.add(FileFormField.fromBundle((Bundle)iterator.next()));
                }
            }
            return new Params(bundle.getString("androidx.browser.trusted.sharing.KEY_TITLE"), bundle.getString("androidx.browser.trusted.sharing.KEY_TEXT"), list);
        }
        
        Bundle toBundle() {
            final Bundle bundle = new Bundle();
            bundle.putString("androidx.browser.trusted.sharing.KEY_TITLE", this.title);
            bundle.putString("androidx.browser.trusted.sharing.KEY_TEXT", this.text);
            if (this.files != null) {
                final ArrayList list = new ArrayList();
                final Iterator<FileFormField> iterator = this.files.iterator();
                while (iterator.hasNext()) {
                    list.add(iterator.next().toBundle());
                }
                bundle.putParcelableArrayList("androidx.browser.trusted.sharing.KEY_FILES", list);
            }
            return bundle;
        }
    }
    
    @Retention(RetentionPolicy.SOURCE)
    public @interface RequestMethod {
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.browser.trusted.sharing;

import java.util.Collection;
import java.util.ArrayList;
import android.os.Bundle;
import android.net.Uri;
import java.util.List;

public final class ShareData
{
    public static final String KEY_TEXT = "androidx.browser.trusted.sharing.KEY_TEXT";
    public static final String KEY_TITLE = "androidx.browser.trusted.sharing.KEY_TITLE";
    public static final String KEY_URIS = "androidx.browser.trusted.sharing.KEY_URIS";
    public final String text;
    public final String title;
    public final List<Uri> uris;
    
    public ShareData(final String title, final String text, final List<Uri> uris) {
        this.title = title;
        this.text = text;
        this.uris = uris;
    }
    
    public static ShareData fromBundle(final Bundle bundle) {
        return new ShareData(bundle.getString("androidx.browser.trusted.sharing.KEY_TITLE"), bundle.getString("androidx.browser.trusted.sharing.KEY_TEXT"), bundle.getParcelableArrayList("androidx.browser.trusted.sharing.KEY_URIS"));
    }
    
    public Bundle toBundle() {
        final Bundle bundle = new Bundle();
        bundle.putString("androidx.browser.trusted.sharing.KEY_TITLE", this.title);
        bundle.putString("androidx.browser.trusted.sharing.KEY_TEXT", this.text);
        if (this.uris != null) {
            bundle.putParcelableArrayList("androidx.browser.trusted.sharing.KEY_URIS", new ArrayList((Collection<? extends E>)this.uris));
        }
        return bundle;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.browser.trusted;

import android.os.RemoteException;
import android.os.Bundle;
import android.os.IBinder;
import android.support.customtabs.trusted.ITrustedWebActivityCallback;

public class TrustedWebActivityCallbackRemote
{
    private final ITrustedWebActivityCallback mCallbackBinder;
    
    private TrustedWebActivityCallbackRemote(final ITrustedWebActivityCallback mCallbackBinder) {
        this.mCallbackBinder = mCallbackBinder;
    }
    
    static TrustedWebActivityCallbackRemote fromBinder(final IBinder binder) {
        ITrustedWebActivityCallback interface1;
        if (binder == null) {
            interface1 = null;
        }
        else {
            interface1 = ITrustedWebActivityCallback.Stub.asInterface(binder);
        }
        if (interface1 == null) {
            return null;
        }
        return new TrustedWebActivityCallbackRemote(interface1);
    }
    
    public void runExtraCallback(final String s, final Bundle bundle) throws RemoteException {
        this.mCallbackBinder.onExtraCallback(s, bundle);
    }
}

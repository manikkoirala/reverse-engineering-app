// 
// Decompiled by Procyon v0.6.0
// 

package androidx.browser.customtabs;

import androidx.core.content.FileProvider;
import java.io.File;
import android.app.PendingIntent;
import android.os.Parcelable;
import android.os.Bundle;
import androidx.core.app.BundleCompat;
import android.net.Uri;
import android.content.pm.ResolveInfo;
import android.content.Intent;
import android.content.Context;

public class TrustedWebUtils
{
    public static final String ACTION_MANAGE_TRUSTED_WEB_ACTIVITY_DATA = "android.support.customtabs.action.ACTION_MANAGE_TRUSTED_WEB_ACTIVITY_DATA";
    public static final String EXTRA_LAUNCH_AS_TRUSTED_WEB_ACTIVITY = "android.support.customtabs.extra.LAUNCH_AS_TRUSTED_WEB_ACTIVITY";
    
    private TrustedWebUtils() {
    }
    
    public static boolean areSplashScreensSupported(final Context context, final String package1, final String s) {
        final ResolveInfo resolveService = context.getPackageManager().resolveService(new Intent().setAction("android.support.customtabs.action.CustomTabsService").setPackage(package1), 64);
        return resolveService != null && resolveService.filter != null && resolveService.filter.hasCategory(s);
    }
    
    @Deprecated
    public static void launchAsTrustedWebActivity(final Context context, final CustomTabsIntent customTabsIntent, final Uri uri) {
        if (BundleCompat.getBinder(customTabsIntent.intent.getExtras(), "android.support.customtabs.extra.SESSION") != null) {
            customTabsIntent.intent.putExtra("android.support.customtabs.extra.LAUNCH_AS_TRUSTED_WEB_ACTIVITY", true);
            customTabsIntent.launchUrl(context, uri);
            return;
        }
        throw new IllegalArgumentException("Given CustomTabsIntent should be associated with a valid CustomTabsSession");
    }
    
    public static void launchBrowserSiteSettings(final Context context, final CustomTabsSession customTabsSession, final Uri data) {
        final Intent intent = new Intent("android.support.customtabs.action.ACTION_MANAGE_TRUSTED_WEB_ACTIVITY_DATA");
        intent.setPackage(customTabsSession.getComponentName().getPackageName());
        intent.setData(data);
        final Bundle bundle = new Bundle();
        BundleCompat.putBinder(bundle, "android.support.customtabs.extra.SESSION", customTabsSession.getBinder());
        intent.putExtras(bundle);
        final PendingIntent id = customTabsSession.getId();
        if (id != null) {
            intent.putExtra("android.support.customtabs.extra.SESSION_ID", (Parcelable)id);
        }
        context.startActivity(intent);
    }
    
    public static boolean transferSplashImage(final Context context, final File file, final String s, final String s2, final CustomTabsSession customTabsSession) {
        final Uri uriForFile = FileProvider.getUriForFile(context, s, file);
        context.grantUriPermission(s2, uriForFile, 1);
        return customTabsSession.receiveFile(uriForFile, 1, null);
    }
}

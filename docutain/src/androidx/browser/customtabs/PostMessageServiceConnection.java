// 
// Decompiled by Procyon v0.6.0
// 

package androidx.browser.customtabs;

import android.content.ComponentName;
import android.util.Log;
import android.content.Intent;
import android.content.Context;
import android.os.RemoteException;
import android.os.Bundle;
import android.os.IBinder;
import android.support.customtabs.ICustomTabsCallback;
import android.support.customtabs.IPostMessageService;
import android.content.ServiceConnection;

public abstract class PostMessageServiceConnection implements PostMessageBackend, ServiceConnection
{
    private static final String TAG = "PostMessageServConn";
    private final Object mLock;
    private boolean mMessageChannelCreated;
    private String mPackageName;
    private IPostMessageService mService;
    private final ICustomTabsCallback mSessionBinder;
    
    public PostMessageServiceConnection(final CustomTabsSessionToken customTabsSessionToken) {
        this.mLock = new Object();
        final IBinder callbackBinder = customTabsSessionToken.getCallbackBinder();
        if (callbackBinder != null) {
            this.mSessionBinder = ICustomTabsCallback.Stub.asInterface(callbackBinder);
            return;
        }
        throw new IllegalArgumentException("Provided session must have binder.");
    }
    
    private boolean isBoundToService() {
        return this.mService != null;
    }
    
    private boolean notifyMessageChannelReadyInternal(final Bundle bundle) {
        if (this.mService == null) {
            return false;
        }
        final Object mLock = this.mLock;
        monitorenter(mLock);
        try {
            try {
                this.mService.onMessageChannelReady(this.mSessionBinder, bundle);
                monitorexit(mLock);
                return true;
            }
            finally {
                monitorexit(mLock);
            }
        }
        catch (final RemoteException ex) {}
    }
    
    public boolean bindSessionToPostMessageService(final Context context) {
        final String mPackageName = this.mPackageName;
        if (mPackageName != null) {
            return this.bindSessionToPostMessageService(context, mPackageName);
        }
        throw new IllegalStateException("setPackageName must be called before bindSessionToPostMessageService.");
    }
    
    public boolean bindSessionToPostMessageService(final Context context, final String s) {
        final Intent intent = new Intent();
        intent.setClassName(s, PostMessageService.class.getName());
        final boolean bindService = context.bindService(intent, (ServiceConnection)this, 1);
        if (!bindService) {
            Log.w("PostMessageServConn", "Could not bind to PostMessageService in client.");
        }
        return bindService;
    }
    
    public void cleanup(final Context context) {
        if (this.isBoundToService()) {
            this.unbindFromContext(context);
        }
    }
    
    public final boolean notifyMessageChannelReady(final Bundle bundle) {
        this.mMessageChannelCreated = true;
        return this.notifyMessageChannelReadyInternal(bundle);
    }
    
    @Override
    public void onDisconnectChannel(final Context context) {
        this.unbindFromContext(context);
    }
    
    @Override
    public final boolean onNotifyMessageChannelReady(final Bundle bundle) {
        return this.notifyMessageChannelReady(bundle);
    }
    
    @Override
    public final boolean onPostMessage(final String s, final Bundle bundle) {
        return this.postMessage(s, bundle);
    }
    
    public void onPostMessageServiceConnected() {
        if (this.mMessageChannelCreated) {
            this.notifyMessageChannelReadyInternal(null);
        }
    }
    
    public void onPostMessageServiceDisconnected() {
    }
    
    public final void onServiceConnected(final ComponentName componentName, final IBinder binder) {
        this.mService = IPostMessageService.Stub.asInterface(binder);
        this.onPostMessageServiceConnected();
    }
    
    public final void onServiceDisconnected(final ComponentName componentName) {
        this.mService = null;
        this.onPostMessageServiceDisconnected();
    }
    
    public final boolean postMessage(final String s, final Bundle bundle) {
        if (this.mService == null) {
            return false;
        }
        final Object mLock = this.mLock;
        monitorenter(mLock);
        try {
            try {
                this.mService.onPostMessage(this.mSessionBinder, s, bundle);
                monitorexit(mLock);
                return true;
            }
            finally {
                monitorexit(mLock);
            }
        }
        catch (final RemoteException ex) {}
    }
    
    public void setPackageName(final String mPackageName) {
        this.mPackageName = mPackageName;
    }
    
    public void unbindFromContext(final Context context) {
        if (this.isBoundToService()) {
            context.unbindService((ServiceConnection)this);
            this.mService = null;
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.browser.customtabs;

import androidx.core.app.BundleCompat;
import android.content.Intent;
import android.os.IBinder;
import android.net.Uri;
import android.os.RemoteException;
import android.util.Log;
import android.os.Bundle;
import android.app.PendingIntent;
import android.support.customtabs.ICustomTabsCallback;

public class CustomTabsSessionToken
{
    private static final String TAG = "CustomTabsSessionToken";
    private final CustomTabsCallback mCallback;
    final ICustomTabsCallback mCallbackBinder;
    private final PendingIntent mSessionId;
    
    CustomTabsSessionToken(final ICustomTabsCallback mCallbackBinder, final PendingIntent mSessionId) {
        if (mCallbackBinder == null && mSessionId == null) {
            throw new IllegalStateException("CustomTabsSessionToken must have either a session id or a callback (or both).");
        }
        this.mCallbackBinder = mCallbackBinder;
        this.mSessionId = mSessionId;
        CustomTabsCallback mCallback;
        if (mCallbackBinder == null) {
            mCallback = null;
        }
        else {
            mCallback = new CustomTabsCallback(this) {
                final CustomTabsSessionToken this$0;
                
                @Override
                public void extraCallback(final String s, final Bundle bundle) {
                    try {
                        this.this$0.mCallbackBinder.extraCallback(s, bundle);
                    }
                    catch (final RemoteException ex) {
                        Log.e("CustomTabsSessionToken", "RemoteException during ICustomTabsCallback transaction");
                    }
                }
                
                @Override
                public Bundle extraCallbackWithResult(final String s, final Bundle bundle) {
                    try {
                        return this.this$0.mCallbackBinder.extraCallbackWithResult(s, bundle);
                    }
                    catch (final RemoteException ex) {
                        Log.e("CustomTabsSessionToken", "RemoteException during ICustomTabsCallback transaction");
                        return null;
                    }
                }
                
                @Override
                public void onActivityResized(final int n, final int n2, final Bundle bundle) {
                    try {
                        this.this$0.mCallbackBinder.onActivityResized(n, n2, bundle);
                    }
                    catch (final RemoteException ex) {
                        Log.e("CustomTabsSessionToken", "RemoteException during ICustomTabsCallback transaction");
                    }
                }
                
                @Override
                public void onMessageChannelReady(final Bundle bundle) {
                    try {
                        this.this$0.mCallbackBinder.onMessageChannelReady(bundle);
                    }
                    catch (final RemoteException ex) {
                        Log.e("CustomTabsSessionToken", "RemoteException during ICustomTabsCallback transaction");
                    }
                }
                
                @Override
                public void onNavigationEvent(final int n, final Bundle bundle) {
                    try {
                        this.this$0.mCallbackBinder.onNavigationEvent(n, bundle);
                    }
                    catch (final RemoteException ex) {
                        Log.e("CustomTabsSessionToken", "RemoteException during ICustomTabsCallback transaction");
                    }
                }
                
                @Override
                public void onPostMessage(final String s, final Bundle bundle) {
                    try {
                        this.this$0.mCallbackBinder.onPostMessage(s, bundle);
                    }
                    catch (final RemoteException ex) {
                        Log.e("CustomTabsSessionToken", "RemoteException during ICustomTabsCallback transaction");
                    }
                }
                
                @Override
                public void onRelationshipValidationResult(final int n, final Uri uri, final boolean b, final Bundle bundle) {
                    try {
                        this.this$0.mCallbackBinder.onRelationshipValidationResult(n, uri, b, bundle);
                    }
                    catch (final RemoteException ex) {
                        Log.e("CustomTabsSessionToken", "RemoteException during ICustomTabsCallback transaction");
                    }
                }
            };
        }
        this.mCallback = mCallback;
    }
    
    public static CustomTabsSessionToken createMockSessionTokenForTesting() {
        return new CustomTabsSessionToken(new MockCallback(), null);
    }
    
    private IBinder getCallbackBinderAssertNotNull() {
        final ICustomTabsCallback mCallbackBinder = this.mCallbackBinder;
        if (mCallbackBinder != null) {
            return mCallbackBinder.asBinder();
        }
        throw new IllegalStateException("CustomTabSessionToken must have valid binder or pending session");
    }
    
    public static CustomTabsSessionToken getSessionTokenFromIntent(final Intent intent) {
        final Bundle extras = intent.getExtras();
        final ICustomTabsCallback customTabsCallback = null;
        if (extras == null) {
            return null;
        }
        final IBinder binder = BundleCompat.getBinder(extras, "android.support.customtabs.extra.SESSION");
        final PendingIntent pendingIntent = (PendingIntent)intent.getParcelableExtra("android.support.customtabs.extra.SESSION_ID");
        if (binder == null && pendingIntent == null) {
            return null;
        }
        ICustomTabsCallback interface1;
        if (binder == null) {
            interface1 = customTabsCallback;
        }
        else {
            interface1 = ICustomTabsCallback.Stub.asInterface(binder);
        }
        return new CustomTabsSessionToken(interface1, pendingIntent);
    }
    
    @Override
    public boolean equals(final Object o) {
        if (!(o instanceof CustomTabsSessionToken)) {
            return false;
        }
        final CustomTabsSessionToken customTabsSessionToken = (CustomTabsSessionToken)o;
        final PendingIntent id = customTabsSessionToken.getId();
        final PendingIntent mSessionId = this.mSessionId;
        int n = true ? 1 : 0;
        final boolean b = mSessionId == null;
        if (id != null) {
            n = (false ? 1 : 0);
        }
        if ((b ? 1 : 0) != n) {
            return false;
        }
        if (mSessionId != null) {
            return mSessionId.equals((Object)id);
        }
        return this.getCallbackBinderAssertNotNull().equals(customTabsSessionToken.getCallbackBinderAssertNotNull());
    }
    
    public CustomTabsCallback getCallback() {
        return this.mCallback;
    }
    
    IBinder getCallbackBinder() {
        final ICustomTabsCallback mCallbackBinder = this.mCallbackBinder;
        if (mCallbackBinder == null) {
            return null;
        }
        return mCallbackBinder.asBinder();
    }
    
    PendingIntent getId() {
        return this.mSessionId;
    }
    
    public boolean hasCallback() {
        return this.mCallbackBinder != null;
    }
    
    public boolean hasId() {
        return this.mSessionId != null;
    }
    
    @Override
    public int hashCode() {
        final PendingIntent mSessionId = this.mSessionId;
        if (mSessionId != null) {
            return mSessionId.hashCode();
        }
        return this.getCallbackBinderAssertNotNull().hashCode();
    }
    
    public boolean isAssociatedWith(final CustomTabsSession customTabsSession) {
        return customTabsSession.getBinder().equals(this.mCallbackBinder);
    }
    
    static class MockCallback extends Stub
    {
        @Override
        public IBinder asBinder() {
            return (IBinder)this;
        }
        
        public void extraCallback(final String s, final Bundle bundle) {
        }
        
        public Bundle extraCallbackWithResult(final String s, final Bundle bundle) {
            return null;
        }
        
        public void onActivityResized(final int n, final int n2, final Bundle bundle) {
        }
        
        public void onMessageChannelReady(final Bundle bundle) {
        }
        
        public void onNavigationEvent(final int n, final Bundle bundle) {
        }
        
        public void onPostMessage(final String s, final Bundle bundle) {
        }
        
        public void onRelationshipValidationResult(final int n, final Uri uri, final boolean b, final Bundle bundle) {
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.browser.customtabs;

import android.os.Bundle;
import android.content.Context;

public interface PostMessageBackend
{
    void onDisconnectChannel(final Context p0);
    
    boolean onNotifyMessageChannelReady(final Bundle p0);
    
    boolean onPostMessage(final String p0, final Bundle p1);
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.browser.customtabs;

import android.widget.RemoteViews;
import androidx.core.app.ActivityOptionsCompat;
import android.os.Build$VERSION;
import android.graphics.Bitmap;
import android.os.Parcelable;
import androidx.core.app.BundleCompat;
import android.app.PendingIntent;
import android.os.IBinder;
import android.text.TextUtils;
import java.util.ArrayList;
import android.os.LocaleList;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;
import androidx.core.content.ContextCompat;
import android.net.Uri;
import android.content.Context;
import android.util.SparseArray;
import android.os.Bundle;
import android.content.Intent;

public final class CustomTabsIntent
{
    public static final int ACTIVITY_HEIGHT_ADJUSTABLE = 1;
    public static final int ACTIVITY_HEIGHT_DEFAULT = 0;
    public static final int ACTIVITY_HEIGHT_FIXED = 2;
    private static final int ACTIVITY_HEIGHT_MAX = 2;
    public static final int CLOSE_BUTTON_POSITION_DEFAULT = 0;
    public static final int CLOSE_BUTTON_POSITION_END = 2;
    private static final int CLOSE_BUTTON_POSITION_MAX = 2;
    public static final int CLOSE_BUTTON_POSITION_START = 1;
    public static final int COLOR_SCHEME_DARK = 2;
    public static final int COLOR_SCHEME_LIGHT = 1;
    private static final int COLOR_SCHEME_MAX = 2;
    public static final int COLOR_SCHEME_SYSTEM = 0;
    public static final String EXTRA_ACTION_BUTTON_BUNDLE = "android.support.customtabs.extra.ACTION_BUTTON_BUNDLE";
    public static final String EXTRA_ACTIVITY_HEIGHT_RESIZE_BEHAVIOR = "androidx.browser.customtabs.extra.ACTIVITY_HEIGHT_RESIZE_BEHAVIOR";
    public static final String EXTRA_CLOSE_BUTTON_ICON = "android.support.customtabs.extra.CLOSE_BUTTON_ICON";
    public static final String EXTRA_CLOSE_BUTTON_POSITION = "androidx.browser.customtabs.extra.CLOSE_BUTTON_POSITION";
    public static final String EXTRA_COLOR_SCHEME = "androidx.browser.customtabs.extra.COLOR_SCHEME";
    public static final String EXTRA_COLOR_SCHEME_PARAMS = "androidx.browser.customtabs.extra.COLOR_SCHEME_PARAMS";
    @Deprecated
    public static final String EXTRA_DEFAULT_SHARE_MENU_ITEM = "android.support.customtabs.extra.SHARE_MENU_ITEM";
    public static final String EXTRA_ENABLE_INSTANT_APPS = "android.support.customtabs.extra.EXTRA_ENABLE_INSTANT_APPS";
    public static final String EXTRA_ENABLE_URLBAR_HIDING = "android.support.customtabs.extra.ENABLE_URLBAR_HIDING";
    public static final String EXTRA_EXIT_ANIMATION_BUNDLE = "android.support.customtabs.extra.EXIT_ANIMATION_BUNDLE";
    public static final String EXTRA_INITIAL_ACTIVITY_HEIGHT_PX = "androidx.browser.customtabs.extra.INITIAL_ACTIVITY_HEIGHT_PX";
    public static final String EXTRA_MENU_ITEMS = "android.support.customtabs.extra.MENU_ITEMS";
    public static final String EXTRA_NAVIGATION_BAR_COLOR = "androidx.browser.customtabs.extra.NAVIGATION_BAR_COLOR";
    public static final String EXTRA_NAVIGATION_BAR_DIVIDER_COLOR = "androidx.browser.customtabs.extra.NAVIGATION_BAR_DIVIDER_COLOR";
    public static final String EXTRA_REMOTEVIEWS = "android.support.customtabs.extra.EXTRA_REMOTEVIEWS";
    public static final String EXTRA_REMOTEVIEWS_CLICKED_ID = "android.support.customtabs.extra.EXTRA_REMOTEVIEWS_CLICKED_ID";
    public static final String EXTRA_REMOTEVIEWS_PENDINGINTENT = "android.support.customtabs.extra.EXTRA_REMOTEVIEWS_PENDINGINTENT";
    public static final String EXTRA_REMOTEVIEWS_VIEW_IDS = "android.support.customtabs.extra.EXTRA_REMOTEVIEWS_VIEW_IDS";
    public static final String EXTRA_SECONDARY_TOOLBAR_COLOR = "android.support.customtabs.extra.SECONDARY_TOOLBAR_COLOR";
    public static final String EXTRA_SESSION = "android.support.customtabs.extra.SESSION";
    public static final String EXTRA_SESSION_ID = "android.support.customtabs.extra.SESSION_ID";
    public static final String EXTRA_SHARE_STATE = "androidx.browser.customtabs.extra.SHARE_STATE";
    public static final String EXTRA_TINT_ACTION_BUTTON = "android.support.customtabs.extra.TINT_ACTION_BUTTON";
    public static final String EXTRA_TITLE_VISIBILITY_STATE = "android.support.customtabs.extra.TITLE_VISIBILITY";
    public static final String EXTRA_TOOLBAR_COLOR = "android.support.customtabs.extra.TOOLBAR_COLOR";
    public static final String EXTRA_TOOLBAR_CORNER_RADIUS_DP = "androidx.browser.customtabs.extra.TOOLBAR_CORNER_RADIUS_DP";
    public static final String EXTRA_TOOLBAR_ITEMS = "android.support.customtabs.extra.TOOLBAR_ITEMS";
    private static final String EXTRA_USER_OPT_OUT_FROM_CUSTOM_TABS = "android.support.customtabs.extra.user_opt_out";
    private static final String HTTP_ACCEPT_LANGUAGE = "Accept-Language";
    public static final String KEY_DESCRIPTION = "android.support.customtabs.customaction.DESCRIPTION";
    public static final String KEY_ICON = "android.support.customtabs.customaction.ICON";
    public static final String KEY_ID = "android.support.customtabs.customaction.ID";
    public static final String KEY_MENU_ITEM_TITLE = "android.support.customtabs.customaction.MENU_ITEM_TITLE";
    public static final String KEY_PENDING_INTENT = "android.support.customtabs.customaction.PENDING_INTENT";
    private static final int MAX_TOOLBAR_CORNER_RADIUS_DP = 16;
    private static final int MAX_TOOLBAR_ITEMS = 5;
    public static final int NO_TITLE = 0;
    public static final int SHARE_STATE_DEFAULT = 0;
    private static final int SHARE_STATE_MAX = 2;
    public static final int SHARE_STATE_OFF = 2;
    public static final int SHARE_STATE_ON = 1;
    public static final int SHOW_PAGE_TITLE = 1;
    public static final int TOOLBAR_ACTION_BUTTON_ID = 0;
    public final Intent intent;
    public final Bundle startAnimationBundle;
    
    CustomTabsIntent(final Intent intent, final Bundle startAnimationBundle) {
        this.intent = intent;
        this.startAnimationBundle = startAnimationBundle;
    }
    
    public static int getActivityResizeBehavior(final Intent intent) {
        return intent.getIntExtra("androidx.browser.customtabs.extra.ACTIVITY_HEIGHT_RESIZE_BEHAVIOR", 0);
    }
    
    public static int getCloseButtonPosition(final Intent intent) {
        return intent.getIntExtra("androidx.browser.customtabs.extra.CLOSE_BUTTON_POSITION", 0);
    }
    
    public static CustomTabColorSchemeParams getColorSchemeParams(final Intent intent, final int i) {
        if (i < 0 || i > 2 || i == 0) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Invalid colorScheme: ");
            sb.append(i);
            throw new IllegalArgumentException(sb.toString());
        }
        final Bundle extras = intent.getExtras();
        if (extras == null) {
            return CustomTabColorSchemeParams.fromBundle(null);
        }
        final CustomTabColorSchemeParams fromBundle = CustomTabColorSchemeParams.fromBundle(extras);
        final SparseArray sparseParcelableArray = extras.getSparseParcelableArray("androidx.browser.customtabs.extra.COLOR_SCHEME_PARAMS");
        if (sparseParcelableArray != null) {
            final Bundle bundle = (Bundle)sparseParcelableArray.get(i);
            if (bundle != null) {
                return CustomTabColorSchemeParams.fromBundle(bundle).withDefaults(fromBundle);
            }
        }
        return fromBundle;
    }
    
    public static int getInitialActivityHeightPx(final Intent intent) {
        return intent.getIntExtra("androidx.browser.customtabs.extra.INITIAL_ACTIVITY_HEIGHT_PX", 0);
    }
    
    public static int getMaxToolbarItems() {
        return 5;
    }
    
    public static int getToolbarCornerRadiusDp(final Intent intent) {
        return intent.getIntExtra("androidx.browser.customtabs.extra.TOOLBAR_CORNER_RADIUS_DP", 16);
    }
    
    public static Intent setAlwaysUseBrowserUI(final Intent intent) {
        Intent intent2 = intent;
        if (intent == null) {
            intent2 = new Intent("android.intent.action.VIEW");
        }
        intent2.addFlags(268435456);
        intent2.putExtra("android.support.customtabs.extra.user_opt_out", true);
        return intent2;
    }
    
    public static boolean shouldAlwaysUseBrowserUI(final Intent intent) {
        boolean b = false;
        if (intent.getBooleanExtra("android.support.customtabs.extra.user_opt_out", false)) {
            b = b;
            if ((intent.getFlags() & 0x10000000) != 0x0) {
                b = true;
            }
        }
        return b;
    }
    
    public void launchUrl(final Context context, final Uri data) {
        this.intent.setData(data);
        ContextCompat.startActivity(context, this.intent, this.startAnimationBundle);
    }
    
    @Retention(RetentionPolicy.SOURCE)
    public @interface ActivityHeightResizeBehavior {
    }
    
    private static class Api24Impl
    {
        static String getDefaultLocale() {
            final LocaleList adjustedDefault = LocaleList.getAdjustedDefault();
            String languageTag;
            if (adjustedDefault.size() > 0) {
                languageTag = adjustedDefault.get(0).toLanguageTag();
            }
            else {
                languageTag = null;
            }
            return languageTag;
        }
    }
    
    public static final class Builder
    {
        private ArrayList<Bundle> mActionButtons;
        private SparseArray<Bundle> mColorSchemeParamBundles;
        private final CustomTabColorSchemeParams.Builder mDefaultColorSchemeBuilder;
        private Bundle mDefaultColorSchemeBundle;
        private boolean mInstantAppsEnabled;
        private final Intent mIntent;
        private ArrayList<Bundle> mMenuItems;
        private int mShareState;
        private Bundle mStartAnimationBundle;
        
        public Builder() {
            this.mIntent = new Intent("android.intent.action.VIEW");
            this.mDefaultColorSchemeBuilder = new CustomTabColorSchemeParams.Builder();
            this.mShareState = 0;
            this.mInstantAppsEnabled = true;
        }
        
        public Builder(final CustomTabsSession session) {
            this.mIntent = new Intent("android.intent.action.VIEW");
            this.mDefaultColorSchemeBuilder = new CustomTabColorSchemeParams.Builder();
            this.mShareState = 0;
            this.mInstantAppsEnabled = true;
            if (session != null) {
                this.setSession(session);
            }
        }
        
        private void setCurrentLocaleAsDefaultAcceptLanguage() {
            final String defaultLocale = Api24Impl.getDefaultLocale();
            if (!TextUtils.isEmpty((CharSequence)defaultLocale)) {
                Bundle bundleExtra;
                if (this.mIntent.hasExtra("com.android.browser.headers")) {
                    bundleExtra = this.mIntent.getBundleExtra("com.android.browser.headers");
                }
                else {
                    bundleExtra = new Bundle();
                }
                if (!bundleExtra.containsKey("Accept-Language")) {
                    bundleExtra.putString("Accept-Language", defaultLocale);
                    this.mIntent.putExtra("com.android.browser.headers", bundleExtra);
                }
            }
        }
        
        private void setSessionParameters(final IBinder binder, final PendingIntent pendingIntent) {
            final Bundle bundle = new Bundle();
            BundleCompat.putBinder(bundle, "android.support.customtabs.extra.SESSION", binder);
            if (pendingIntent != null) {
                bundle.putParcelable("android.support.customtabs.extra.SESSION_ID", (Parcelable)pendingIntent);
            }
            this.mIntent.putExtras(bundle);
        }
        
        @Deprecated
        public Builder addDefaultShareMenuItem() {
            this.setShareState(1);
            return this;
        }
        
        public Builder addMenuItem(final String s, final PendingIntent pendingIntent) {
            if (this.mMenuItems == null) {
                this.mMenuItems = new ArrayList<Bundle>();
            }
            final Bundle e = new Bundle();
            e.putString("android.support.customtabs.customaction.MENU_ITEM_TITLE", s);
            e.putParcelable("android.support.customtabs.customaction.PENDING_INTENT", (Parcelable)pendingIntent);
            this.mMenuItems.add(e);
            return this;
        }
        
        @Deprecated
        public Builder addToolbarItem(final int n, final Bitmap bitmap, final String s, final PendingIntent pendingIntent) throws IllegalStateException {
            if (this.mActionButtons == null) {
                this.mActionButtons = new ArrayList<Bundle>();
            }
            if (this.mActionButtons.size() < 5) {
                final Bundle e = new Bundle();
                e.putInt("android.support.customtabs.customaction.ID", n);
                e.putParcelable("android.support.customtabs.customaction.ICON", (Parcelable)bitmap);
                e.putString("android.support.customtabs.customaction.DESCRIPTION", s);
                e.putParcelable("android.support.customtabs.customaction.PENDING_INTENT", (Parcelable)pendingIntent);
                this.mActionButtons.add(e);
                return this;
            }
            throw new IllegalStateException("Exceeded maximum toolbar item count of 5");
        }
        
        public CustomTabsIntent build() {
            if (!this.mIntent.hasExtra("android.support.customtabs.extra.SESSION")) {
                this.setSessionParameters(null, null);
            }
            final ArrayList<Bundle> mMenuItems = this.mMenuItems;
            if (mMenuItems != null) {
                this.mIntent.putParcelableArrayListExtra("android.support.customtabs.extra.MENU_ITEMS", (ArrayList)mMenuItems);
            }
            final ArrayList<Bundle> mActionButtons = this.mActionButtons;
            if (mActionButtons != null) {
                this.mIntent.putParcelableArrayListExtra("android.support.customtabs.extra.TOOLBAR_ITEMS", (ArrayList)mActionButtons);
            }
            this.mIntent.putExtra("android.support.customtabs.extra.EXTRA_ENABLE_INSTANT_APPS", this.mInstantAppsEnabled);
            this.mIntent.putExtras(this.mDefaultColorSchemeBuilder.build().toBundle());
            final Bundle mDefaultColorSchemeBundle = this.mDefaultColorSchemeBundle;
            if (mDefaultColorSchemeBundle != null) {
                this.mIntent.putExtras(mDefaultColorSchemeBundle);
            }
            if (this.mColorSchemeParamBundles != null) {
                final Bundle bundle = new Bundle();
                bundle.putSparseParcelableArray("androidx.browser.customtabs.extra.COLOR_SCHEME_PARAMS", (SparseArray)this.mColorSchemeParamBundles);
                this.mIntent.putExtras(bundle);
            }
            this.mIntent.putExtra("androidx.browser.customtabs.extra.SHARE_STATE", this.mShareState);
            if (Build$VERSION.SDK_INT >= 24) {
                this.setCurrentLocaleAsDefaultAcceptLanguage();
            }
            return new CustomTabsIntent(this.mIntent, this.mStartAnimationBundle);
        }
        
        @Deprecated
        public Builder enableUrlBarHiding() {
            this.mIntent.putExtra("android.support.customtabs.extra.ENABLE_URLBAR_HIDING", true);
            return this;
        }
        
        public Builder setActionButton(final Bitmap bitmap, final String s, final PendingIntent pendingIntent) {
            return this.setActionButton(bitmap, s, pendingIntent, false);
        }
        
        public Builder setActionButton(final Bitmap bitmap, final String s, final PendingIntent pendingIntent, final boolean b) {
            final Bundle bundle = new Bundle();
            bundle.putInt("android.support.customtabs.customaction.ID", 0);
            bundle.putParcelable("android.support.customtabs.customaction.ICON", (Parcelable)bitmap);
            bundle.putString("android.support.customtabs.customaction.DESCRIPTION", s);
            bundle.putParcelable("android.support.customtabs.customaction.PENDING_INTENT", (Parcelable)pendingIntent);
            this.mIntent.putExtra("android.support.customtabs.extra.ACTION_BUTTON_BUNDLE", bundle);
            this.mIntent.putExtra("android.support.customtabs.extra.TINT_ACTION_BUTTON", b);
            return this;
        }
        
        public Builder setCloseButtonIcon(final Bitmap bitmap) {
            this.mIntent.putExtra("android.support.customtabs.extra.CLOSE_BUTTON_ICON", (Parcelable)bitmap);
            return this;
        }
        
        public Builder setCloseButtonPosition(final int n) {
            if (n >= 0 && n <= 2) {
                this.mIntent.putExtra("androidx.browser.customtabs.extra.CLOSE_BUTTON_POSITION", n);
                return this;
            }
            throw new IllegalArgumentException("Invalid value for the position argument");
        }
        
        public Builder setColorScheme(final int n) {
            if (n >= 0 && n <= 2) {
                this.mIntent.putExtra("androidx.browser.customtabs.extra.COLOR_SCHEME", n);
                return this;
            }
            throw new IllegalArgumentException("Invalid value for the colorScheme argument");
        }
        
        public Builder setColorSchemeParams(final int i, final CustomTabColorSchemeParams customTabColorSchemeParams) {
            if (i >= 0 && i <= 2 && i != 0) {
                if (this.mColorSchemeParamBundles == null) {
                    this.mColorSchemeParamBundles = (SparseArray<Bundle>)new SparseArray();
                }
                this.mColorSchemeParamBundles.put(i, (Object)customTabColorSchemeParams.toBundle());
                return this;
            }
            final StringBuilder sb = new StringBuilder();
            sb.append("Invalid colorScheme: ");
            sb.append(i);
            throw new IllegalArgumentException(sb.toString());
        }
        
        public Builder setDefaultColorSchemeParams(final CustomTabColorSchemeParams customTabColorSchemeParams) {
            this.mDefaultColorSchemeBundle = customTabColorSchemeParams.toBundle();
            return this;
        }
        
        @Deprecated
        public Builder setDefaultShareMenuItemEnabled(final boolean b) {
            if (b) {
                this.setShareState(1);
            }
            else {
                this.setShareState(2);
            }
            return this;
        }
        
        public Builder setExitAnimations(final Context context, final int n, final int n2) {
            this.mIntent.putExtra("android.support.customtabs.extra.EXIT_ANIMATION_BUNDLE", ActivityOptionsCompat.makeCustomAnimation(context, n, n2).toBundle());
            return this;
        }
        
        public Builder setInitialActivityHeightPx(final int n) {
            return this.setInitialActivityHeightPx(n, 0);
        }
        
        public Builder setInitialActivityHeightPx(final int n, final int n2) {
            if (n <= 0) {
                throw new IllegalArgumentException("Invalid value for the initialHeightPx argument");
            }
            if (n2 >= 0 && n2 <= 2) {
                this.mIntent.putExtra("androidx.browser.customtabs.extra.INITIAL_ACTIVITY_HEIGHT_PX", n);
                this.mIntent.putExtra("androidx.browser.customtabs.extra.ACTIVITY_HEIGHT_RESIZE_BEHAVIOR", n2);
                return this;
            }
            throw new IllegalArgumentException("Invalid value for the activityHeightResizeBehavior argument");
        }
        
        public Builder setInstantAppsEnabled(final boolean mInstantAppsEnabled) {
            this.mInstantAppsEnabled = mInstantAppsEnabled;
            return this;
        }
        
        @Deprecated
        public Builder setNavigationBarColor(final int navigationBarColor) {
            this.mDefaultColorSchemeBuilder.setNavigationBarColor(navigationBarColor);
            return this;
        }
        
        @Deprecated
        public Builder setNavigationBarDividerColor(final int navigationBarDividerColor) {
            this.mDefaultColorSchemeBuilder.setNavigationBarDividerColor(navigationBarDividerColor);
            return this;
        }
        
        public Builder setPendingSession(final CustomTabsSession.PendingSession pendingSession) {
            this.setSessionParameters(null, pendingSession.getId());
            return this;
        }
        
        @Deprecated
        public Builder setSecondaryToolbarColor(final int secondaryToolbarColor) {
            this.mDefaultColorSchemeBuilder.setSecondaryToolbarColor(secondaryToolbarColor);
            return this;
        }
        
        public Builder setSecondaryToolbarViews(final RemoteViews remoteViews, final int[] array, final PendingIntent pendingIntent) {
            this.mIntent.putExtra("android.support.customtabs.extra.EXTRA_REMOTEVIEWS", (Parcelable)remoteViews);
            this.mIntent.putExtra("android.support.customtabs.extra.EXTRA_REMOTEVIEWS_VIEW_IDS", array);
            this.mIntent.putExtra("android.support.customtabs.extra.EXTRA_REMOTEVIEWS_PENDINGINTENT", (Parcelable)pendingIntent);
            return this;
        }
        
        public Builder setSession(final CustomTabsSession customTabsSession) {
            this.mIntent.setPackage(customTabsSession.getComponentName().getPackageName());
            this.setSessionParameters(customTabsSession.getBinder(), customTabsSession.getId());
            return this;
        }
        
        public Builder setShareState(final int mShareState) {
            if (mShareState >= 0 && mShareState <= 2) {
                if ((this.mShareState = mShareState) == 1) {
                    this.mIntent.putExtra("android.support.customtabs.extra.SHARE_MENU_ITEM", true);
                }
                else if (mShareState == 2) {
                    this.mIntent.putExtra("android.support.customtabs.extra.SHARE_MENU_ITEM", false);
                }
                else {
                    this.mIntent.removeExtra("android.support.customtabs.extra.SHARE_MENU_ITEM");
                }
                return this;
            }
            throw new IllegalArgumentException("Invalid value for the shareState argument");
        }
        
        public Builder setShowTitle(final boolean b) {
            this.mIntent.putExtra("android.support.customtabs.extra.TITLE_VISIBILITY", (int)(b ? 1 : 0));
            return this;
        }
        
        public Builder setStartAnimations(final Context context, final int n, final int n2) {
            this.mStartAnimationBundle = ActivityOptionsCompat.makeCustomAnimation(context, n, n2).toBundle();
            return this;
        }
        
        @Deprecated
        public Builder setToolbarColor(final int toolbarColor) {
            this.mDefaultColorSchemeBuilder.setToolbarColor(toolbarColor);
            return this;
        }
        
        public Builder setToolbarCornerRadiusDp(final int n) {
            if (n >= 0 && n <= 16) {
                this.mIntent.putExtra("androidx.browser.customtabs.extra.TOOLBAR_CORNER_RADIUS_DP", n);
                return this;
            }
            throw new IllegalArgumentException("Invalid value for the cornerRadiusDp argument");
        }
        
        public Builder setUrlBarHidingEnabled(final boolean b) {
            this.mIntent.putExtra("android.support.customtabs.extra.ENABLE_URLBAR_HIDING", b);
            return this;
        }
    }
    
    @Retention(RetentionPolicy.SOURCE)
    public @interface CloseButtonPosition {
    }
    
    @Retention(RetentionPolicy.SOURCE)
    public @interface ColorScheme {
    }
    
    @Retention(RetentionPolicy.SOURCE)
    public @interface ShareState {
    }
}

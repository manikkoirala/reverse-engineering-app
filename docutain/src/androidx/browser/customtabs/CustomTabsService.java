// 
// Decompiled by Procyon v0.6.0
// 

package androidx.browser.customtabs;

import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;
import android.content.Intent;
import java.util.NoSuchElementException;
import java.util.List;
import android.net.Uri;
import android.os.RemoteException;
import android.support.customtabs.ICustomTabsCallback;
import android.app.PendingIntent;
import android.os.Bundle;
import android.os.IBinder$DeathRecipient;
import android.os.IBinder;
import androidx.collection.SimpleArrayMap;
import android.support.customtabs.ICustomTabsService;
import android.app.Service;

public abstract class CustomTabsService extends Service
{
    public static final String ACTION_CUSTOM_TABS_CONNECTION = "android.support.customtabs.action.CustomTabsService";
    public static final String CATEGORY_COLOR_SCHEME_CUSTOMIZATION = "androidx.browser.customtabs.category.ColorSchemeCustomization";
    public static final String CATEGORY_NAVBAR_COLOR_CUSTOMIZATION = "androidx.browser.customtabs.category.NavBarColorCustomization";
    public static final String CATEGORY_TRUSTED_WEB_ACTIVITY_IMMERSIVE_MODE = "androidx.browser.trusted.category.ImmersiveMode";
    public static final String CATEGORY_WEB_SHARE_TARGET_V2 = "androidx.browser.trusted.category.WebShareTargetV2";
    public static final int FILE_PURPOSE_TRUSTED_WEB_ACTIVITY_SPLASH_IMAGE = 1;
    public static final String KEY_SUCCESS = "androidx.browser.customtabs.SUCCESS";
    public static final String KEY_URL = "android.support.customtabs.otherurls.URL";
    public static final int RELATION_HANDLE_ALL_URLS = 2;
    public static final int RELATION_USE_AS_ORIGIN = 1;
    public static final int RESULT_FAILURE_DISALLOWED = -1;
    public static final int RESULT_FAILURE_MESSAGING_ERROR = -3;
    public static final int RESULT_FAILURE_REMOTE_ERROR = -2;
    public static final int RESULT_SUCCESS = 0;
    public static final String TRUSTED_WEB_ACTIVITY_CATEGORY = "androidx.browser.trusted.category.TrustedWebActivities";
    private ICustomTabsService.Stub mBinder;
    final SimpleArrayMap<IBinder, IBinder$DeathRecipient> mDeathRecipientMap;
    
    public CustomTabsService() {
        this.mDeathRecipientMap = new SimpleArrayMap<IBinder, IBinder$DeathRecipient>();
        this.mBinder = new ICustomTabsService.Stub() {
            final CustomTabsService this$0;
            
            private PendingIntent getSessionIdFromBundle(final Bundle bundle) {
                if (bundle == null) {
                    return null;
                }
                final PendingIntent pendingIntent = (PendingIntent)bundle.getParcelable("android.support.customtabs.extra.SESSION_ID");
                bundle.remove("android.support.customtabs.extra.SESSION_ID");
                return pendingIntent;
            }
            
            private boolean newSessionInternal(final ICustomTabsCallback customTabsCallback, final PendingIntent pendingIntent) {
                final CustomTabsSessionToken customTabsSessionToken = new CustomTabsSessionToken(customTabsCallback, pendingIntent);
                try {
                    final CustomTabsService$1$$ExternalSyntheticLambda0 customTabsService$1$$ExternalSyntheticLambda0 = new CustomTabsService$1$$ExternalSyntheticLambda0(this, customTabsSessionToken);
                    synchronized (this.this$0.mDeathRecipientMap) {
                        customTabsCallback.asBinder().linkToDeath((IBinder$DeathRecipient)customTabsService$1$$ExternalSyntheticLambda0, 0);
                        this.this$0.mDeathRecipientMap.put(customTabsCallback.asBinder(), (IBinder$DeathRecipient)customTabsService$1$$ExternalSyntheticLambda0);
                        monitorexit(this.this$0.mDeathRecipientMap);
                        return this.this$0.newSession(customTabsSessionToken);
                    }
                }
                catch (final RemoteException ex) {
                    return false;
                }
            }
            
            public Bundle extraCommand(final String s, final Bundle bundle) {
                return this.this$0.extraCommand(s, bundle);
            }
            
            public boolean mayLaunchUrl(final ICustomTabsCallback customTabsCallback, final Uri uri, final Bundle bundle, final List<Bundle> list) {
                return this.this$0.mayLaunchUrl(new CustomTabsSessionToken(customTabsCallback, this.getSessionIdFromBundle(bundle)), uri, bundle, list);
            }
            
            public boolean newSession(final ICustomTabsCallback customTabsCallback) {
                return this.newSessionInternal(customTabsCallback, null);
            }
            
            public boolean newSessionWithExtras(final ICustomTabsCallback customTabsCallback, final Bundle bundle) {
                return this.newSessionInternal(customTabsCallback, this.getSessionIdFromBundle(bundle));
            }
            
            public int postMessage(final ICustomTabsCallback customTabsCallback, final String s, final Bundle bundle) {
                return this.this$0.postMessage(new CustomTabsSessionToken(customTabsCallback, this.getSessionIdFromBundle(bundle)), s, bundle);
            }
            
            public boolean receiveFile(final ICustomTabsCallback customTabsCallback, final Uri uri, final int n, final Bundle bundle) {
                return this.this$0.receiveFile(new CustomTabsSessionToken(customTabsCallback, this.getSessionIdFromBundle(bundle)), uri, n, bundle);
            }
            
            public boolean requestPostMessageChannel(final ICustomTabsCallback customTabsCallback, final Uri uri) {
                return this.this$0.requestPostMessageChannel(new CustomTabsSessionToken(customTabsCallback, null), uri);
            }
            
            public boolean requestPostMessageChannelWithExtras(final ICustomTabsCallback customTabsCallback, final Uri uri, final Bundle bundle) {
                return this.this$0.requestPostMessageChannel(new CustomTabsSessionToken(customTabsCallback, this.getSessionIdFromBundle(bundle)), uri);
            }
            
            public boolean updateVisuals(final ICustomTabsCallback customTabsCallback, final Bundle bundle) {
                return this.this$0.updateVisuals(new CustomTabsSessionToken(customTabsCallback, this.getSessionIdFromBundle(bundle)), bundle);
            }
            
            public boolean validateRelationship(final ICustomTabsCallback customTabsCallback, final int n, final Uri uri, final Bundle bundle) {
                return this.this$0.validateRelationship(new CustomTabsSessionToken(customTabsCallback, this.getSessionIdFromBundle(bundle)), n, uri, bundle);
            }
            
            public boolean warmup(final long n) {
                return this.this$0.warmup(n);
            }
        };
    }
    
    protected boolean cleanUpSession(final CustomTabsSessionToken customTabsSessionToken) {
        try {
            synchronized (this.mDeathRecipientMap) {
                final IBinder callbackBinder = customTabsSessionToken.getCallbackBinder();
                if (callbackBinder == null) {
                    return false;
                }
                callbackBinder.unlinkToDeath((IBinder$DeathRecipient)this.mDeathRecipientMap.get(callbackBinder), 0);
                this.mDeathRecipientMap.remove(callbackBinder);
                return true;
            }
        }
        catch (final NoSuchElementException ex) {
            return false;
        }
    }
    
    protected abstract Bundle extraCommand(final String p0, final Bundle p1);
    
    protected abstract boolean mayLaunchUrl(final CustomTabsSessionToken p0, final Uri p1, final Bundle p2, final List<Bundle> p3);
    
    protected abstract boolean newSession(final CustomTabsSessionToken p0);
    
    public IBinder onBind(final Intent intent) {
        return (IBinder)this.mBinder;
    }
    
    protected abstract int postMessage(final CustomTabsSessionToken p0, final String p1, final Bundle p2);
    
    protected abstract boolean receiveFile(final CustomTabsSessionToken p0, final Uri p1, final int p2, final Bundle p3);
    
    protected abstract boolean requestPostMessageChannel(final CustomTabsSessionToken p0, final Uri p1);
    
    protected abstract boolean updateVisuals(final CustomTabsSessionToken p0, final Bundle p1);
    
    protected abstract boolean validateRelationship(final CustomTabsSessionToken p0, final int p1, final Uri p2, final Bundle p3);
    
    protected abstract boolean warmup(final long p0);
    
    @Retention(RetentionPolicy.SOURCE)
    public @interface FilePurpose {
    }
    
    @Retention(RetentionPolicy.SOURCE)
    public @interface Relation {
    }
    
    @Retention(RetentionPolicy.SOURCE)
    public @interface Result {
    }
}

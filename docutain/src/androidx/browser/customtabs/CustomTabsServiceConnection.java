// 
// Decompiled by Procyon v0.6.0
// 

package androidx.browser.customtabs;

import android.support.customtabs.ICustomTabsService;
import android.os.IBinder;
import android.content.ComponentName;
import android.content.Context;
import android.content.ServiceConnection;

public abstract class CustomTabsServiceConnection implements ServiceConnection
{
    private Context mApplicationContext;
    
    Context getApplicationContext() {
        return this.mApplicationContext;
    }
    
    public abstract void onCustomTabsServiceConnected(final ComponentName p0, final CustomTabsClient p1);
    
    public final void onServiceConnected(final ComponentName componentName, final IBinder binder) {
        if (this.mApplicationContext != null) {
            this.onCustomTabsServiceConnected(componentName, new CustomTabsClient(this, ICustomTabsService.Stub.asInterface(binder), componentName, this.mApplicationContext) {
                final CustomTabsServiceConnection this$0;
            });
            return;
        }
        throw new IllegalStateException("Custom Tabs Service connected before an applicationcontext has been provided.");
    }
    
    void setApplicationContext(final Context mApplicationContext) {
        this.mApplicationContext = mApplicationContext;
    }
}

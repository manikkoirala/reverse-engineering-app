// 
// Decompiled by Procyon v0.6.0
// 

package androidx.browser.customtabs;

import android.net.Uri;
import android.os.Bundle;

public class CustomTabsCallback
{
    public static final int NAVIGATION_ABORTED = 4;
    public static final int NAVIGATION_FAILED = 3;
    public static final int NAVIGATION_FINISHED = 2;
    public static final int NAVIGATION_STARTED = 1;
    public static final String ONLINE_EXTRAS_KEY = "online";
    public static final int TAB_HIDDEN = 6;
    public static final int TAB_SHOWN = 5;
    
    public void extraCallback(final String s, final Bundle bundle) {
    }
    
    public Bundle extraCallbackWithResult(final String s, final Bundle bundle) {
        return null;
    }
    
    public void onActivityResized(final int n, final int n2, final Bundle bundle) {
    }
    
    public void onMessageChannelReady(final Bundle bundle) {
    }
    
    public void onNavigationEvent(final int n, final Bundle bundle) {
    }
    
    public void onPostMessage(final String s, final Bundle bundle) {
    }
    
    public void onRelationshipValidationResult(final int n, final Uri uri, final boolean b, final Bundle bundle) {
    }
}

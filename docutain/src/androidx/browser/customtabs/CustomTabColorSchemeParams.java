// 
// Decompiled by Procyon v0.6.0
// 

package androidx.browser.customtabs;

import android.os.Bundle;

public final class CustomTabColorSchemeParams
{
    public final Integer navigationBarColor;
    public final Integer navigationBarDividerColor;
    public final Integer secondaryToolbarColor;
    public final Integer toolbarColor;
    
    CustomTabColorSchemeParams(final Integer toolbarColor, final Integer secondaryToolbarColor, final Integer navigationBarColor, final Integer navigationBarDividerColor) {
        this.toolbarColor = toolbarColor;
        this.secondaryToolbarColor = secondaryToolbarColor;
        this.navigationBarColor = navigationBarColor;
        this.navigationBarDividerColor = navigationBarDividerColor;
    }
    
    static CustomTabColorSchemeParams fromBundle(final Bundle bundle) {
        Bundle bundle2 = bundle;
        if (bundle == null) {
            bundle2 = new Bundle(0);
        }
        return new CustomTabColorSchemeParams((Integer)bundle2.get("android.support.customtabs.extra.TOOLBAR_COLOR"), (Integer)bundle2.get("android.support.customtabs.extra.SECONDARY_TOOLBAR_COLOR"), (Integer)bundle2.get("androidx.browser.customtabs.extra.NAVIGATION_BAR_COLOR"), (Integer)bundle2.get("androidx.browser.customtabs.extra.NAVIGATION_BAR_DIVIDER_COLOR"));
    }
    
    Bundle toBundle() {
        final Bundle bundle = new Bundle();
        final Integer toolbarColor = this.toolbarColor;
        if (toolbarColor != null) {
            bundle.putInt("android.support.customtabs.extra.TOOLBAR_COLOR", (int)toolbarColor);
        }
        final Integer secondaryToolbarColor = this.secondaryToolbarColor;
        if (secondaryToolbarColor != null) {
            bundle.putInt("android.support.customtabs.extra.SECONDARY_TOOLBAR_COLOR", (int)secondaryToolbarColor);
        }
        final Integer navigationBarColor = this.navigationBarColor;
        if (navigationBarColor != null) {
            bundle.putInt("androidx.browser.customtabs.extra.NAVIGATION_BAR_COLOR", (int)navigationBarColor);
        }
        final Integer navigationBarDividerColor = this.navigationBarDividerColor;
        if (navigationBarDividerColor != null) {
            bundle.putInt("androidx.browser.customtabs.extra.NAVIGATION_BAR_DIVIDER_COLOR", (int)navigationBarDividerColor);
        }
        return bundle;
    }
    
    CustomTabColorSchemeParams withDefaults(final CustomTabColorSchemeParams customTabColorSchemeParams) {
        Integer n;
        if ((n = this.toolbarColor) == null) {
            n = customTabColorSchemeParams.toolbarColor;
        }
        Integer n2;
        if ((n2 = this.secondaryToolbarColor) == null) {
            n2 = customTabColorSchemeParams.secondaryToolbarColor;
        }
        Integer n3;
        if ((n3 = this.navigationBarColor) == null) {
            n3 = customTabColorSchemeParams.navigationBarColor;
        }
        Integer n4;
        if ((n4 = this.navigationBarDividerColor) == null) {
            n4 = customTabColorSchemeParams.navigationBarDividerColor;
        }
        return new CustomTabColorSchemeParams(n, n2, n3, n4);
    }
    
    public static final class Builder
    {
        private Integer mNavigationBarColor;
        private Integer mNavigationBarDividerColor;
        private Integer mSecondaryToolbarColor;
        private Integer mToolbarColor;
        
        public CustomTabColorSchemeParams build() {
            return new CustomTabColorSchemeParams(this.mToolbarColor, this.mSecondaryToolbarColor, this.mNavigationBarColor, this.mNavigationBarDividerColor);
        }
        
        public Builder setNavigationBarColor(final int n) {
            this.mNavigationBarColor = (n | 0xFF000000);
            return this;
        }
        
        public Builder setNavigationBarDividerColor(final int i) {
            this.mNavigationBarDividerColor = i;
            return this;
        }
        
        public Builder setSecondaryToolbarColor(final int i) {
            this.mSecondaryToolbarColor = i;
            return this;
        }
        
        public Builder setToolbarColor(final int n) {
            this.mToolbarColor = (n | 0xFF000000);
            return this;
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.preference;

public final class R
{
    public static final class attr
    {
        public static final int adjustable = 2130968622;
        public static final int allowDividerAbove = 2130968628;
        public static final int allowDividerAfterLastItem = 2130968629;
        public static final int allowDividerBelow = 2130968630;
        public static final int checkBoxPreferenceStyle = 2130968761;
        public static final int defaultValue = 2130968966;
        public static final int dependency = 2130968969;
        public static final int dialogIcon = 2130968973;
        public static final int dialogLayout = 2130968974;
        public static final int dialogMessage = 2130968975;
        public static final int dialogPreferenceStyle = 2130968976;
        public static final int dialogTitle = 2130968979;
        public static final int disableDependentsState = 2130968980;
        public static final int dropdownPreferenceStyle = 2130969008;
        public static final int editTextPreferenceStyle = 2130969013;
        public static final int enableCopying = 2130969020;
        public static final int enabled = 2130969022;
        public static final int entries = 2130969035;
        public static final int entryValues = 2130969036;
        public static final int fragment = 2130969134;
        public static final int icon = 2130969164;
        public static final int iconSpaceReserved = 2130969169;
        public static final int initialExpandedChildrenCount = 2130969192;
        public static final int isPreferenceVisible = 2130969198;
        public static final int key = 2130969228;
        public static final int layout = 2130969239;
        public static final int maxHeight = 2130969403;
        public static final int maxWidth = 2130969407;
        public static final int min = 2130969414;
        public static final int negativeButtonText = 2130969480;
        public static final int order = 2130969495;
        public static final int orderingFromXml = 2130969496;
        public static final int persistent = 2130969524;
        public static final int positiveButtonText = 2130969540;
        public static final int preferenceCategoryStyle = 2130969541;
        public static final int preferenceCategoryTitleTextAppearance = 2130969542;
        public static final int preferenceCategoryTitleTextColor = 2130969543;
        public static final int preferenceFragmentCompatStyle = 2130969544;
        public static final int preferenceFragmentListStyle = 2130969545;
        public static final int preferenceFragmentStyle = 2130969546;
        public static final int preferenceInformationStyle = 2130969547;
        public static final int preferenceScreenStyle = 2130969548;
        public static final int preferenceStyle = 2130969549;
        public static final int preferenceTheme = 2130969550;
        public static final int seekBarIncrement = 2130969982;
        public static final int seekBarPreferenceStyle = 2130969983;
        public static final int selectable = 2130969985;
        public static final int selectableItemBackground = 2130969986;
        public static final int shouldDisableView = 2130970003;
        public static final int showSeekBarValue = 2130970011;
        public static final int singleLineTitle = 2130970023;
        public static final int summary = 2130970089;
        public static final int summaryOff = 2130970090;
        public static final int summaryOn = 2130970091;
        public static final int switchPreferenceCompatStyle = 2130970095;
        public static final int switchPreferenceStyle = 2130970096;
        public static final int switchTextOff = 2130970099;
        public static final int switchTextOn = 2130970100;
        public static final int title = 2130970227;
        public static final int updatesContinuously = 2130970278;
        public static final int useSimpleSummaryProvider = 2130970284;
        public static final int widgetLayout = 2130970302;
    }
    
    public static final class bool
    {
        public static final int config_materialPreferenceIconSpaceReserved = 2131034114;
    }
    
    public static final class color
    {
        public static final int preference_fallback_accent_color = 2131100421;
    }
    
    public static final class dimen
    {
        public static final int preference_dropdown_padding_start = 2131165973;
        public static final int preference_icon_minWidth = 2131165974;
        public static final int preference_seekbar_padding_horizontal = 2131165975;
        public static final int preference_seekbar_padding_vertical = 2131165976;
        public static final int preference_seekbar_value_minWidth = 2131165977;
        public static final int preferences_detail_width = 2131165978;
        public static final int preferences_header_width = 2131165979;
    }
    
    public static final class drawable
    {
        public static final int ic_arrow_down_24dp = 2131230988;
        public static final int preference_list_divider_material = 2131231150;
    }
    
    public static final class id
    {
        public static final int icon_frame = 2131362152;
        public static final int preferences_detail = 2131362325;
        public static final int preferences_header = 2131362326;
        public static final int preferences_sliding_pane_layout = 2131362327;
        public static final int recycler_view = 2131362941;
        public static final int seekbar = 2131362995;
        public static final int seekbar_value = 2131362996;
        public static final int spinner = 2131363025;
        public static final int switchWidget = 2131363057;
    }
    
    public static final class integer
    {
        public static final int preferences_detail_pane_weight = 2131427395;
        public static final int preferences_header_pane_weight = 2131427396;
    }
    
    public static final class layout
    {
        public static final int expand_button = 2131558473;
        public static final int image_frame = 2131558481;
        public static final int preference = 2131558574;
        public static final int preference_category = 2131558575;
        public static final int preference_category_material = 2131558576;
        public static final int preference_dialog_edittext = 2131558577;
        public static final int preference_dropdown = 2131558578;
        public static final int preference_dropdown_material = 2131558579;
        public static final int preference_information = 2131558580;
        public static final int preference_information_material = 2131558581;
        public static final int preference_list_fragment = 2131558582;
        public static final int preference_material = 2131558583;
        public static final int preference_recyclerview = 2131558584;
        public static final int preference_widget_checkbox = 2131558585;
        public static final int preference_widget_seekbar = 2131558586;
        public static final int preference_widget_seekbar_material = 2131558587;
        public static final int preference_widget_switch = 2131558588;
        public static final int preference_widget_switch_compat = 2131558589;
    }
    
    public static final class string
    {
        public static final int copy = 2131886146;
        public static final int expand_button_title = 2131886152;
        public static final int not_set = 2131886284;
        public static final int preference_copied = 2131886291;
        public static final int summary_collapsed_preference_list = 2131886712;
        public static final int v7_preference_off = 2131886713;
        public static final int v7_preference_on = 2131886714;
    }
    
    public static final class style
    {
        public static final int BasePreferenceThemeOverlay = 2131951898;
        public static final int Preference = 2131952072;
        public static final int PreferenceCategoryTitleTextStyle = 2131952094;
        public static final int PreferenceFragment = 2131952095;
        public static final int PreferenceFragmentList = 2131952097;
        public static final int PreferenceFragmentList_Material = 2131952098;
        public static final int PreferenceFragment_Material = 2131952096;
        public static final int PreferenceSummaryTextStyle = 2131952099;
        public static final int PreferenceThemeOverlay = 2131952100;
        public static final int PreferenceThemeOverlay_v14 = 2131952101;
        public static final int PreferenceThemeOverlay_v14_Material = 2131952102;
        public static final int Preference_Category = 2131952073;
        public static final int Preference_Category_Material = 2131952074;
        public static final int Preference_CheckBoxPreference = 2131952075;
        public static final int Preference_CheckBoxPreference_Material = 2131952076;
        public static final int Preference_DialogPreference = 2131952077;
        public static final int Preference_DialogPreference_EditTextPreference = 2131952078;
        public static final int Preference_DialogPreference_EditTextPreference_Material = 2131952079;
        public static final int Preference_DialogPreference_Material = 2131952080;
        public static final int Preference_DropDown = 2131952081;
        public static final int Preference_DropDown_Material = 2131952082;
        public static final int Preference_Information = 2131952083;
        public static final int Preference_Information_Material = 2131952084;
        public static final int Preference_Material = 2131952085;
        public static final int Preference_PreferenceScreen = 2131952086;
        public static final int Preference_PreferenceScreen_Material = 2131952087;
        public static final int Preference_SeekBarPreference = 2131952088;
        public static final int Preference_SeekBarPreference_Material = 2131952089;
        public static final int Preference_SwitchPreference = 2131952090;
        public static final int Preference_SwitchPreferenceCompat = 2131952092;
        public static final int Preference_SwitchPreferenceCompat_Material = 2131952093;
        public static final int Preference_SwitchPreference_Material = 2131952091;
    }
    
    public static final class styleable
    {
        public static final int[] BackgroundStyle;
        public static final int BackgroundStyle_android_selectableItemBackground = 0;
        public static final int BackgroundStyle_selectableItemBackground = 1;
        public static final int[] CheckBoxPreference;
        public static final int CheckBoxPreference_android_disableDependentsState = 2;
        public static final int CheckBoxPreference_android_summaryOff = 1;
        public static final int CheckBoxPreference_android_summaryOn = 0;
        public static final int CheckBoxPreference_disableDependentsState = 3;
        public static final int CheckBoxPreference_summaryOff = 4;
        public static final int CheckBoxPreference_summaryOn = 5;
        public static final int[] DialogPreference;
        public static final int DialogPreference_android_dialogIcon = 2;
        public static final int DialogPreference_android_dialogLayout = 5;
        public static final int DialogPreference_android_dialogMessage = 1;
        public static final int DialogPreference_android_dialogTitle = 0;
        public static final int DialogPreference_android_negativeButtonText = 4;
        public static final int DialogPreference_android_positiveButtonText = 3;
        public static final int DialogPreference_dialogIcon = 6;
        public static final int DialogPreference_dialogLayout = 7;
        public static final int DialogPreference_dialogMessage = 8;
        public static final int DialogPreference_dialogTitle = 9;
        public static final int DialogPreference_negativeButtonText = 10;
        public static final int DialogPreference_positiveButtonText = 11;
        public static final int[] EditTextPreference;
        public static final int EditTextPreference_useSimpleSummaryProvider = 0;
        public static final int[] ListPreference;
        public static final int ListPreference_android_entries = 0;
        public static final int ListPreference_android_entryValues = 1;
        public static final int ListPreference_entries = 2;
        public static final int ListPreference_entryValues = 3;
        public static final int ListPreference_useSimpleSummaryProvider = 4;
        public static final int[] MultiSelectListPreference;
        public static final int MultiSelectListPreference_android_entries = 0;
        public static final int MultiSelectListPreference_android_entryValues = 1;
        public static final int MultiSelectListPreference_entries = 2;
        public static final int MultiSelectListPreference_entryValues = 3;
        public static final int[] Preference;
        public static final int[] PreferenceFragment;
        public static final int[] PreferenceFragmentCompat;
        public static final int PreferenceFragmentCompat_allowDividerAfterLastItem = 3;
        public static final int PreferenceFragmentCompat_android_divider = 1;
        public static final int PreferenceFragmentCompat_android_dividerHeight = 2;
        public static final int PreferenceFragmentCompat_android_layout = 0;
        public static final int PreferenceFragment_allowDividerAfterLastItem = 3;
        public static final int PreferenceFragment_android_divider = 1;
        public static final int PreferenceFragment_android_dividerHeight = 2;
        public static final int PreferenceFragment_android_layout = 0;
        public static final int[] PreferenceGroup;
        public static final int PreferenceGroup_android_orderingFromXml = 0;
        public static final int PreferenceGroup_initialExpandedChildrenCount = 1;
        public static final int PreferenceGroup_orderingFromXml = 2;
        public static final int[] PreferenceImageView;
        public static final int PreferenceImageView_android_maxHeight = 1;
        public static final int PreferenceImageView_android_maxWidth = 0;
        public static final int PreferenceImageView_maxHeight = 2;
        public static final int PreferenceImageView_maxWidth = 3;
        public static final int[] PreferenceTheme;
        public static final int PreferenceTheme_checkBoxPreferenceStyle = 0;
        public static final int PreferenceTheme_dialogPreferenceStyle = 1;
        public static final int PreferenceTheme_dropdownPreferenceStyle = 2;
        public static final int PreferenceTheme_editTextPreferenceStyle = 3;
        public static final int PreferenceTheme_preferenceCategoryStyle = 4;
        public static final int PreferenceTheme_preferenceCategoryTitleTextAppearance = 5;
        public static final int PreferenceTheme_preferenceCategoryTitleTextColor = 6;
        public static final int PreferenceTheme_preferenceFragmentCompatStyle = 7;
        public static final int PreferenceTheme_preferenceFragmentListStyle = 8;
        public static final int PreferenceTheme_preferenceFragmentStyle = 9;
        public static final int PreferenceTheme_preferenceInformationStyle = 10;
        public static final int PreferenceTheme_preferenceScreenStyle = 11;
        public static final int PreferenceTheme_preferenceStyle = 12;
        public static final int PreferenceTheme_preferenceTheme = 13;
        public static final int PreferenceTheme_seekBarPreferenceStyle = 14;
        public static final int PreferenceTheme_switchPreferenceCompatStyle = 15;
        public static final int PreferenceTheme_switchPreferenceStyle = 16;
        public static final int Preference_allowDividerAbove = 16;
        public static final int Preference_allowDividerBelow = 17;
        public static final int Preference_android_defaultValue = 11;
        public static final int Preference_android_dependency = 10;
        public static final int Preference_android_enabled = 2;
        public static final int Preference_android_fragment = 13;
        public static final int Preference_android_icon = 0;
        public static final int Preference_android_iconSpaceReserved = 15;
        public static final int Preference_android_key = 6;
        public static final int Preference_android_layout = 3;
        public static final int Preference_android_order = 8;
        public static final int Preference_android_persistent = 1;
        public static final int Preference_android_selectable = 5;
        public static final int Preference_android_shouldDisableView = 12;
        public static final int Preference_android_singleLineTitle = 14;
        public static final int Preference_android_summary = 7;
        public static final int Preference_android_title = 4;
        public static final int Preference_android_widgetLayout = 9;
        public static final int Preference_defaultValue = 18;
        public static final int Preference_dependency = 19;
        public static final int Preference_enableCopying = 20;
        public static final int Preference_enabled = 21;
        public static final int Preference_fragment = 22;
        public static final int Preference_icon = 23;
        public static final int Preference_iconSpaceReserved = 24;
        public static final int Preference_isPreferenceVisible = 25;
        public static final int Preference_key = 26;
        public static final int Preference_layout = 27;
        public static final int Preference_order = 28;
        public static final int Preference_persistent = 29;
        public static final int Preference_selectable = 30;
        public static final int Preference_shouldDisableView = 31;
        public static final int Preference_singleLineTitle = 32;
        public static final int Preference_summary = 33;
        public static final int Preference_title = 34;
        public static final int Preference_widgetLayout = 35;
        public static final int[] SeekBarPreference;
        public static final int SeekBarPreference_adjustable = 2;
        public static final int SeekBarPreference_android_layout = 0;
        public static final int SeekBarPreference_android_max = 1;
        public static final int SeekBarPreference_min = 3;
        public static final int SeekBarPreference_seekBarIncrement = 4;
        public static final int SeekBarPreference_showSeekBarValue = 5;
        public static final int SeekBarPreference_updatesContinuously = 6;
        public static final int[] SwitchPreference;
        public static final int[] SwitchPreferenceCompat;
        public static final int SwitchPreferenceCompat_android_disableDependentsState = 2;
        public static final int SwitchPreferenceCompat_android_summaryOff = 1;
        public static final int SwitchPreferenceCompat_android_summaryOn = 0;
        public static final int SwitchPreferenceCompat_android_switchTextOff = 4;
        public static final int SwitchPreferenceCompat_android_switchTextOn = 3;
        public static final int SwitchPreferenceCompat_disableDependentsState = 5;
        public static final int SwitchPreferenceCompat_summaryOff = 6;
        public static final int SwitchPreferenceCompat_summaryOn = 7;
        public static final int SwitchPreferenceCompat_switchTextOff = 8;
        public static final int SwitchPreferenceCompat_switchTextOn = 9;
        public static final int SwitchPreference_android_disableDependentsState = 2;
        public static final int SwitchPreference_android_summaryOff = 1;
        public static final int SwitchPreference_android_summaryOn = 0;
        public static final int SwitchPreference_android_switchTextOff = 4;
        public static final int SwitchPreference_android_switchTextOn = 3;
        public static final int SwitchPreference_disableDependentsState = 5;
        public static final int SwitchPreference_summaryOff = 6;
        public static final int SwitchPreference_summaryOn = 7;
        public static final int SwitchPreference_switchTextOff = 8;
        public static final int SwitchPreference_switchTextOn = 9;
        
        static {
            BackgroundStyle = new int[] { 16843534, 2130969986 };
            CheckBoxPreference = new int[] { 16843247, 16843248, 16843249, 2130968980, 2130970090, 2130970091 };
            DialogPreference = new int[] { 16843250, 16843251, 16843252, 16843253, 16843254, 16843255, 2130968973, 2130968974, 2130968975, 2130968979, 2130969480, 2130969540 };
            EditTextPreference = new int[] { 2130970284 };
            ListPreference = new int[] { 16842930, 16843256, 2130969035, 2130969036, 2130970284 };
            MultiSelectListPreference = new int[] { 16842930, 16843256, 2130969035, 2130969036 };
            Preference = new int[] { 16842754, 16842765, 16842766, 16842994, 16843233, 16843238, 16843240, 16843241, 16843242, 16843243, 16843244, 16843245, 16843246, 16843491, 16844124, 16844129, 2130968628, 2130968630, 2130968966, 2130968969, 2130969020, 2130969022, 2130969134, 2130969164, 2130969169, 2130969198, 2130969228, 2130969239, 2130969495, 2130969524, 2130969985, 2130970003, 2130970023, 2130970089, 2130970227, 2130970302 };
            PreferenceFragment = new int[] { 16842994, 16843049, 16843050, 2130968629 };
            PreferenceFragmentCompat = new int[] { 16842994, 16843049, 16843050, 2130968629 };
            PreferenceGroup = new int[] { 16843239, 2130969192, 2130969496 };
            PreferenceImageView = new int[] { 16843039, 16843040, 2130969403, 2130969407 };
            PreferenceTheme = new int[] { 2130968761, 2130968976, 2130969008, 2130969013, 2130969541, 2130969542, 2130969543, 2130969544, 2130969545, 2130969546, 2130969547, 2130969548, 2130969549, 2130969550, 2130969983, 2130970095, 2130970096 };
            SeekBarPreference = new int[] { 16842994, 16843062, 2130968622, 2130969414, 2130969982, 2130970011, 2130970278 };
            SwitchPreference = new int[] { 16843247, 16843248, 16843249, 16843627, 16843628, 2130968980, 2130970090, 2130970091, 2130970099, 2130970100 };
            SwitchPreferenceCompat = new int[] { 16843247, 16843248, 16843249, 16843627, 16843628, 2130968980, 2130970090, 2130970091, 2130970099, 2130970100 };
        }
    }
}

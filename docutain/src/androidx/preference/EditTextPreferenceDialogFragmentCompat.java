// 
// Decompiled by Procyon v0.6.0
// 

package androidx.preference;

import android.view.inputmethod.InputMethodManager;
import android.view.View;
import android.os.Bundle;
import android.os.SystemClock;
import android.widget.EditText;

public class EditTextPreferenceDialogFragmentCompat extends PreferenceDialogFragmentCompat
{
    private static final String SAVE_STATE_TEXT = "EditTextPreferenceDialogFragment.text";
    private static final int SHOW_REQUEST_TIMEOUT = 1000;
    private EditText mEditText;
    private long mShowRequestTime;
    private final Runnable mShowSoftInputRunnable;
    private CharSequence mText;
    
    public EditTextPreferenceDialogFragmentCompat() {
        this.mShowSoftInputRunnable = new Runnable() {
            final EditTextPreferenceDialogFragmentCompat this$0;
            
            @Override
            public void run() {
                this.this$0.scheduleShowSoftInputInner();
            }
        };
        this.mShowRequestTime = -1L;
    }
    
    private EditTextPreference getEditTextPreference() {
        return (EditTextPreference)this.getPreference();
    }
    
    private boolean hasPendingShowSoftInputRequest() {
        final long mShowRequestTime = this.mShowRequestTime;
        return mShowRequestTime != -1L && mShowRequestTime + 1000L > SystemClock.currentThreadTimeMillis();
    }
    
    public static EditTextPreferenceDialogFragmentCompat newInstance(final String s) {
        final EditTextPreferenceDialogFragmentCompat editTextPreferenceDialogFragmentCompat = new EditTextPreferenceDialogFragmentCompat();
        final Bundle arguments = new Bundle(1);
        arguments.putString("key", s);
        editTextPreferenceDialogFragmentCompat.setArguments(arguments);
        return editTextPreferenceDialogFragmentCompat;
    }
    
    private void setPendingShowSoftInputRequest(final boolean b) {
        long currentThreadTimeMillis;
        if (b) {
            currentThreadTimeMillis = SystemClock.currentThreadTimeMillis();
        }
        else {
            currentThreadTimeMillis = -1L;
        }
        this.mShowRequestTime = currentThreadTimeMillis;
    }
    
    @Override
    protected boolean needInputMethod() {
        return true;
    }
    
    @Override
    protected void onBindDialogView(final View view) {
        super.onBindDialogView(view);
        final EditText mEditText = (EditText)view.findViewById(16908291);
        this.mEditText = mEditText;
        if (mEditText != null) {
            mEditText.requestFocus();
            this.mEditText.setText(this.mText);
            final EditText mEditText2 = this.mEditText;
            mEditText2.setSelection(mEditText2.getText().length());
            if (this.getEditTextPreference().getOnBindEditTextListener() != null) {
                this.getEditTextPreference().getOnBindEditTextListener().onBindEditText(this.mEditText);
            }
            return;
        }
        throw new IllegalStateException("Dialog view must contain an EditText with id @android:id/edit");
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        if (bundle == null) {
            this.mText = this.getEditTextPreference().getText();
        }
        else {
            this.mText = bundle.getCharSequence("EditTextPreferenceDialogFragment.text");
        }
    }
    
    @Override
    public void onDialogClosed(final boolean b) {
        if (b) {
            final String string = this.mEditText.getText().toString();
            final EditTextPreference editTextPreference = this.getEditTextPreference();
            if (editTextPreference.callChangeListener(string)) {
                editTextPreference.setText(string);
            }
        }
    }
    
    @Override
    public void onSaveInstanceState(final Bundle bundle) {
        super.onSaveInstanceState(bundle);
        bundle.putCharSequence("EditTextPreferenceDialogFragment.text", this.mText);
    }
    
    @Override
    protected void scheduleShowSoftInput() {
        this.setPendingShowSoftInputRequest(true);
        this.scheduleShowSoftInputInner();
    }
    
    void scheduleShowSoftInputInner() {
        if (this.hasPendingShowSoftInputRequest()) {
            final EditText mEditText = this.mEditText;
            if (mEditText != null && mEditText.isFocused()) {
                if (((InputMethodManager)this.mEditText.getContext().getSystemService("input_method")).showSoftInput((View)this.mEditText, 0)) {
                    this.setPendingShowSoftInputRequest(false);
                }
                else {
                    this.mEditText.removeCallbacks(this.mShowSoftInputRunnable);
                    this.mEditText.postDelayed(this.mShowSoftInputRunnable, 50L);
                }
            }
            else {
                this.setPendingShowSoftInputRequest(false);
            }
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.preference;

import android.graphics.Canvas;
import android.graphics.Rect;
import androidx.fragment.app.FragmentManager;
import android.util.Log;
import android.graphics.drawable.Drawable;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import androidx.recyclerview.widget.RecyclerViewAccessibilityDelegate;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import androidx.recyclerview.widget.LinearLayoutManager;
import android.util.TypedValue;
import android.os.Bundle;
import android.view.View;
import android.os.Message;
import android.os.Looper;
import androidx.recyclerview.widget.RecyclerView;
import android.os.Handler;
import androidx.fragment.app.Fragment;

public abstract class PreferenceFragmentCompat extends Fragment implements OnPreferenceTreeClickListener, OnDisplayPreferenceDialogListener, OnNavigateToScreenListener, TargetFragment
{
    public static final String ARG_PREFERENCE_ROOT = "androidx.preference.PreferenceFragmentCompat.PREFERENCE_ROOT";
    private static final String DIALOG_FRAGMENT_TAG = "androidx.preference.PreferenceFragment.DIALOG";
    private static final int MSG_BIND_PREFERENCES = 1;
    private static final String PREFERENCES_TAG = "android:preferences";
    private static final String TAG = "PreferenceFragment";
    private final DividerDecoration mDividerDecoration;
    private final Handler mHandler;
    private boolean mHavePrefs;
    private boolean mInitDone;
    private int mLayoutResId;
    RecyclerView mList;
    private PreferenceManager mPreferenceManager;
    private final Runnable mRequestFocus;
    private Runnable mSelectPreferenceRunnable;
    
    public PreferenceFragmentCompat() {
        this.mDividerDecoration = new DividerDecoration();
        this.mLayoutResId = R.layout.preference_list_fragment;
        this.mHandler = new Handler(Looper.getMainLooper()) {
            final PreferenceFragmentCompat this$0;
            
            public void handleMessage(final Message message) {
                if (message.what == 1) {
                    this.this$0.bindPreferences();
                }
            }
        };
        this.mRequestFocus = new Runnable() {
            final PreferenceFragmentCompat this$0;
            
            @Override
            public void run() {
                this.this$0.mList.focusableViewAvailable((View)this.this$0.mList);
            }
        };
    }
    
    private void postBindPreferences() {
        if (this.mHandler.hasMessages(1)) {
            return;
        }
        this.mHandler.obtainMessage(1).sendToTarget();
    }
    
    private void requirePreferenceManager() {
        if (this.mPreferenceManager != null) {
            return;
        }
        throw new RuntimeException("This should be called after super.onCreate.");
    }
    
    private void scrollToPreferenceInternal(final Preference preference, final String s) {
        final Runnable mSelectPreferenceRunnable = new Runnable(this, preference, s) {
            final PreferenceFragmentCompat this$0;
            final String val$key;
            final Preference val$preference;
            
            @Override
            public void run() {
                final RecyclerView.Adapter adapter = this.this$0.mList.getAdapter();
                if (adapter instanceof PreferenceGroup.PreferencePositionCallback) {
                    final Preference val$preference = this.val$preference;
                    int n;
                    if (val$preference != null) {
                        n = ((PreferenceGroup.PreferencePositionCallback)adapter).getPreferenceAdapterPosition(val$preference);
                    }
                    else {
                        n = ((PreferenceGroup.PreferencePositionCallback)adapter).getPreferenceAdapterPosition(this.val$key);
                    }
                    if (n != -1) {
                        this.this$0.mList.scrollToPosition(n);
                    }
                    else {
                        adapter.registerAdapterDataObserver(new ScrollToPreferenceObserver(adapter, this.this$0.mList, this.val$preference, this.val$key));
                    }
                    return;
                }
                if (adapter == null) {
                    return;
                }
                throw new IllegalStateException("Adapter must implement PreferencePositionCallback");
            }
        };
        if (this.mList == null) {
            this.mSelectPreferenceRunnable = mSelectPreferenceRunnable;
        }
        else {
            mSelectPreferenceRunnable.run();
        }
    }
    
    private void unbindPreferences() {
        this.getListView().setAdapter(null);
        final PreferenceScreen preferenceScreen = this.getPreferenceScreen();
        if (preferenceScreen != null) {
            preferenceScreen.onDetached();
        }
        this.onUnbindPreferences();
    }
    
    public void addPreferencesFromResource(final int n) {
        this.requirePreferenceManager();
        this.setPreferenceScreen(this.mPreferenceManager.inflateFromResource(this.requireContext(), n, this.getPreferenceScreen()));
    }
    
    void bindPreferences() {
        final PreferenceScreen preferenceScreen = this.getPreferenceScreen();
        if (preferenceScreen != null) {
            this.getListView().setAdapter(this.onCreateAdapter(preferenceScreen));
            preferenceScreen.onAttached();
        }
        this.onBindPreferences();
    }
    
    @Override
    public <T extends Preference> T findPreference(final CharSequence charSequence) {
        final PreferenceManager mPreferenceManager = this.mPreferenceManager;
        if (mPreferenceManager == null) {
            return null;
        }
        return (T)mPreferenceManager.findPreference(charSequence);
    }
    
    public Fragment getCallbackFragment() {
        return null;
    }
    
    public final RecyclerView getListView() {
        return this.mList;
    }
    
    public PreferenceManager getPreferenceManager() {
        return this.mPreferenceManager;
    }
    
    public PreferenceScreen getPreferenceScreen() {
        return this.mPreferenceManager.getPreferenceScreen();
    }
    
    protected void onBindPreferences() {
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        final TypedValue typedValue = new TypedValue();
        this.requireContext().getTheme().resolveAttribute(R.attr.preferenceTheme, typedValue, true);
        int n;
        if ((n = typedValue.resourceId) == 0) {
            n = R.style.PreferenceThemeOverlay;
        }
        this.requireContext().getTheme().applyStyle(n, false);
        (this.mPreferenceManager = new PreferenceManager(this.requireContext())).setOnNavigateToScreenListener((PreferenceManager.OnNavigateToScreenListener)this);
        String string;
        if (this.getArguments() != null) {
            string = this.getArguments().getString("androidx.preference.PreferenceFragmentCompat.PREFERENCE_ROOT");
        }
        else {
            string = null;
        }
        this.onCreatePreferences(bundle, string);
    }
    
    protected RecyclerView.Adapter onCreateAdapter(final PreferenceScreen preferenceScreen) {
        return new PreferenceGroupAdapter(preferenceScreen);
    }
    
    public RecyclerView.LayoutManager onCreateLayoutManager() {
        return new LinearLayoutManager(this.requireContext());
    }
    
    public abstract void onCreatePreferences(final Bundle p0, final String p1);
    
    public RecyclerView onCreateRecyclerView(final LayoutInflater layoutInflater, final ViewGroup viewGroup, final Bundle bundle) {
        if (this.requireContext().getPackageManager().hasSystemFeature("android.hardware.type.automotive")) {
            final RecyclerView recyclerView = (RecyclerView)viewGroup.findViewById(R.id.recycler_view);
            if (recyclerView != null) {
                return recyclerView;
            }
        }
        final RecyclerView recyclerView2 = (RecyclerView)layoutInflater.inflate(R.layout.preference_recyclerview, viewGroup, false);
        recyclerView2.setLayoutManager(this.onCreateLayoutManager());
        recyclerView2.setAccessibilityDelegateCompat(new PreferenceRecyclerViewAccessibilityDelegate(recyclerView2));
        return recyclerView2;
    }
    
    @Override
    public View onCreateView(LayoutInflater cloneInContext, final ViewGroup viewGroup, final Bundle bundle) {
        final TypedArray obtainStyledAttributes = this.requireContext().obtainStyledAttributes((AttributeSet)null, R.styleable.PreferenceFragmentCompat, R.attr.preferenceFragmentCompatStyle, 0);
        this.mLayoutResId = obtainStyledAttributes.getResourceId(R.styleable.PreferenceFragmentCompat_android_layout, this.mLayoutResId);
        final Drawable drawable = obtainStyledAttributes.getDrawable(R.styleable.PreferenceFragmentCompat_android_divider);
        final int dimensionPixelSize = obtainStyledAttributes.getDimensionPixelSize(R.styleable.PreferenceFragmentCompat_android_dividerHeight, -1);
        final boolean boolean1 = obtainStyledAttributes.getBoolean(R.styleable.PreferenceFragmentCompat_allowDividerAfterLastItem, true);
        obtainStyledAttributes.recycle();
        cloneInContext = cloneInContext.cloneInContext(this.requireContext());
        final View inflate = cloneInContext.inflate(this.mLayoutResId, viewGroup, false);
        final View viewById = inflate.findViewById(16908351);
        if (!(viewById instanceof ViewGroup)) {
            throw new IllegalStateException("Content has view with id attribute 'android.R.id.list_container' that is not a ViewGroup class");
        }
        final ViewGroup viewGroup2 = (ViewGroup)viewById;
        final RecyclerView onCreateRecyclerView = this.onCreateRecyclerView(cloneInContext, viewGroup2, bundle);
        if (onCreateRecyclerView != null) {
            (this.mList = onCreateRecyclerView).addItemDecoration((RecyclerView.ItemDecoration)this.mDividerDecoration);
            this.setDivider(drawable);
            if (dimensionPixelSize != -1) {
                this.setDividerHeight(dimensionPixelSize);
            }
            this.mDividerDecoration.setAllowDividerAfterLastItem(boolean1);
            if (this.mList.getParent() == null) {
                viewGroup2.addView((View)this.mList);
            }
            this.mHandler.post(this.mRequestFocus);
            return inflate;
        }
        throw new RuntimeException("Could not create RecyclerView");
    }
    
    @Override
    public void onDestroyView() {
        this.mHandler.removeCallbacks(this.mRequestFocus);
        this.mHandler.removeMessages(1);
        if (this.mHavePrefs) {
            this.unbindPreferences();
        }
        this.mList = null;
        super.onDestroyView();
    }
    
    @Override
    public void onDisplayPreferenceDialog(final Preference preference) {
        boolean onPreferenceDisplayDialog = this.getCallbackFragment() instanceof OnPreferenceDisplayDialogCallback && ((OnPreferenceDisplayDialogCallback)this.getCallbackFragment()).onPreferenceDisplayDialog(this, preference);
        for (Fragment parentFragment = this; !onPreferenceDisplayDialog && parentFragment != null; parentFragment = parentFragment.getParentFragment()) {
            if (parentFragment instanceof OnPreferenceDisplayDialogCallback) {
                onPreferenceDisplayDialog = ((OnPreferenceDisplayDialogCallback)parentFragment).onPreferenceDisplayDialog(this, preference);
            }
        }
        boolean onPreferenceDisplayDialog2;
        if (!(onPreferenceDisplayDialog2 = onPreferenceDisplayDialog)) {
            onPreferenceDisplayDialog2 = onPreferenceDisplayDialog;
            if (this.getContext() instanceof OnPreferenceDisplayDialogCallback) {
                onPreferenceDisplayDialog2 = ((OnPreferenceDisplayDialogCallback)this.getContext()).onPreferenceDisplayDialog(this, preference);
            }
        }
        boolean onPreferenceDisplayDialog3;
        if (!(onPreferenceDisplayDialog3 = onPreferenceDisplayDialog2)) {
            onPreferenceDisplayDialog3 = onPreferenceDisplayDialog2;
            if (this.getActivity() instanceof OnPreferenceDisplayDialogCallback) {
                onPreferenceDisplayDialog3 = ((OnPreferenceDisplayDialogCallback)this.getActivity()).onPreferenceDisplayDialog(this, preference);
            }
        }
        if (onPreferenceDisplayDialog3) {
            return;
        }
        if (this.getParentFragmentManager().findFragmentByTag("androidx.preference.PreferenceFragment.DIALOG") != null) {
            return;
        }
        PreferenceDialogFragmentCompat preferenceDialogFragmentCompat;
        if (preference instanceof EditTextPreference) {
            preferenceDialogFragmentCompat = EditTextPreferenceDialogFragmentCompat.newInstance(preference.getKey());
        }
        else if (preference instanceof ListPreference) {
            preferenceDialogFragmentCompat = ListPreferenceDialogFragmentCompat.newInstance(preference.getKey());
        }
        else {
            if (!(preference instanceof MultiSelectListPreference)) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Cannot display dialog for an unknown Preference type: ");
                sb.append(preference.getClass().getSimpleName());
                sb.append(". Make sure to implement onPreferenceDisplayDialog() to handle displaying a custom dialog for this Preference.");
                throw new IllegalArgumentException(sb.toString());
            }
            preferenceDialogFragmentCompat = MultiSelectListPreferenceDialogFragmentCompat.newInstance(preference.getKey());
        }
        preferenceDialogFragmentCompat.setTargetFragment(this, 0);
        preferenceDialogFragmentCompat.show(this.getParentFragmentManager(), "androidx.preference.PreferenceFragment.DIALOG");
    }
    
    @Override
    public void onNavigateToScreen(final PreferenceScreen preferenceScreen) {
        boolean onPreferenceStartScreen = this.getCallbackFragment() instanceof OnPreferenceStartScreenCallback && ((OnPreferenceStartScreenCallback)this.getCallbackFragment()).onPreferenceStartScreen(this, preferenceScreen);
        for (Fragment parentFragment = this; !onPreferenceStartScreen && parentFragment != null; parentFragment = parentFragment.getParentFragment()) {
            if (parentFragment instanceof OnPreferenceStartScreenCallback) {
                onPreferenceStartScreen = ((OnPreferenceStartScreenCallback)parentFragment).onPreferenceStartScreen(this, preferenceScreen);
            }
        }
        boolean onPreferenceStartScreen2;
        if (!(onPreferenceStartScreen2 = onPreferenceStartScreen)) {
            onPreferenceStartScreen2 = onPreferenceStartScreen;
            if (this.getContext() instanceof OnPreferenceStartScreenCallback) {
                onPreferenceStartScreen2 = ((OnPreferenceStartScreenCallback)this.getContext()).onPreferenceStartScreen(this, preferenceScreen);
            }
        }
        if (!onPreferenceStartScreen2 && this.getActivity() instanceof OnPreferenceStartScreenCallback) {
            ((OnPreferenceStartScreenCallback)this.getActivity()).onPreferenceStartScreen(this, preferenceScreen);
        }
    }
    
    @Override
    public boolean onPreferenceTreeClick(final Preference preference) {
        if (preference.getFragment() != null) {
            boolean onPreferenceStartFragment = this.getCallbackFragment() instanceof OnPreferenceStartFragmentCallback && ((OnPreferenceStartFragmentCallback)this.getCallbackFragment()).onPreferenceStartFragment(this, preference);
            for (Fragment parentFragment = this; !onPreferenceStartFragment && parentFragment != null; parentFragment = parentFragment.getParentFragment()) {
                if (parentFragment instanceof OnPreferenceStartFragmentCallback) {
                    onPreferenceStartFragment = ((OnPreferenceStartFragmentCallback)parentFragment).onPreferenceStartFragment(this, preference);
                }
            }
            boolean onPreferenceStartFragment2;
            if (!(onPreferenceStartFragment2 = onPreferenceStartFragment)) {
                onPreferenceStartFragment2 = onPreferenceStartFragment;
                if (this.getContext() instanceof OnPreferenceStartFragmentCallback) {
                    onPreferenceStartFragment2 = ((OnPreferenceStartFragmentCallback)this.getContext()).onPreferenceStartFragment(this, preference);
                }
            }
            boolean onPreferenceStartFragment3;
            if (!(onPreferenceStartFragment3 = onPreferenceStartFragment2)) {
                onPreferenceStartFragment3 = onPreferenceStartFragment2;
                if (this.getActivity() instanceof OnPreferenceStartFragmentCallback) {
                    onPreferenceStartFragment3 = ((OnPreferenceStartFragmentCallback)this.getActivity()).onPreferenceStartFragment(this, preference);
                }
            }
            if (!onPreferenceStartFragment3) {
                Log.w("PreferenceFragment", "onPreferenceStartFragment is not implemented in the parent activity - attempting to use a fallback implementation. You should implement this method so that you can configure the new fragment that will be displayed, and set a transition between the fragments.");
                final FragmentManager parentFragmentManager = this.getParentFragmentManager();
                final Bundle extras = preference.getExtras();
                final Fragment instantiate = parentFragmentManager.getFragmentFactory().instantiate(this.requireActivity().getClassLoader(), preference.getFragment());
                instantiate.setArguments(extras);
                instantiate.setTargetFragment(this, 0);
                parentFragmentManager.beginTransaction().replace(((View)this.requireView().getParent()).getId(), instantiate).addToBackStack(null).commit();
            }
            return true;
        }
        return false;
    }
    
    @Override
    public void onSaveInstanceState(final Bundle bundle) {
        super.onSaveInstanceState(bundle);
        final PreferenceScreen preferenceScreen = this.getPreferenceScreen();
        if (preferenceScreen != null) {
            final Bundle bundle2 = new Bundle();
            preferenceScreen.saveHierarchyState(bundle2);
            bundle.putBundle("android:preferences", bundle2);
        }
    }
    
    @Override
    public void onStart() {
        super.onStart();
        this.mPreferenceManager.setOnPreferenceTreeClickListener((PreferenceManager.OnPreferenceTreeClickListener)this);
        this.mPreferenceManager.setOnDisplayPreferenceDialogListener((PreferenceManager.OnDisplayPreferenceDialogListener)this);
    }
    
    @Override
    public void onStop() {
        super.onStop();
        this.mPreferenceManager.setOnPreferenceTreeClickListener(null);
        this.mPreferenceManager.setOnDisplayPreferenceDialogListener(null);
    }
    
    protected void onUnbindPreferences() {
    }
    
    @Override
    public void onViewCreated(final View view, Bundle bundle) {
        super.onViewCreated(view, bundle);
        if (bundle != null) {
            bundle = bundle.getBundle("android:preferences");
            if (bundle != null) {
                final PreferenceScreen preferenceScreen = this.getPreferenceScreen();
                if (preferenceScreen != null) {
                    preferenceScreen.restoreHierarchyState(bundle);
                }
            }
        }
        if (this.mHavePrefs) {
            this.bindPreferences();
            final Runnable mSelectPreferenceRunnable = this.mSelectPreferenceRunnable;
            if (mSelectPreferenceRunnable != null) {
                mSelectPreferenceRunnable.run();
                this.mSelectPreferenceRunnable = null;
            }
        }
        this.mInitDone = true;
    }
    
    public void scrollToPreference(final Preference preference) {
        this.scrollToPreferenceInternal(preference, null);
    }
    
    public void scrollToPreference(final String s) {
        this.scrollToPreferenceInternal(null, s);
    }
    
    public void setDivider(final Drawable divider) {
        this.mDividerDecoration.setDivider(divider);
    }
    
    public void setDividerHeight(final int dividerHeight) {
        this.mDividerDecoration.setDividerHeight(dividerHeight);
    }
    
    public void setPreferenceScreen(final PreferenceScreen preferences) {
        if (this.mPreferenceManager.setPreferences(preferences) && preferences != null) {
            this.onUnbindPreferences();
            this.mHavePrefs = true;
            if (this.mInitDone) {
                this.postBindPreferences();
            }
        }
    }
    
    public void setPreferencesFromResource(final int n, final String str) {
        this.requirePreferenceManager();
        PreferenceScreen preferenceScreen2;
        final PreferenceScreen preferenceScreen = preferenceScreen2 = this.mPreferenceManager.inflateFromResource(this.requireContext(), n, null);
        if (str != null) {
            preferenceScreen2 = (PreferenceScreen)preferenceScreen.findPreference(str);
            if (!(preferenceScreen2 instanceof PreferenceScreen)) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Preference object with key ");
                sb.append(str);
                sb.append(" is not a PreferenceScreen");
                throw new IllegalArgumentException(sb.toString());
            }
        }
        this.setPreferenceScreen(preferenceScreen2);
    }
    
    private class DividerDecoration extends ItemDecoration
    {
        private boolean mAllowDividerAfterLastItem;
        private Drawable mDivider;
        private int mDividerHeight;
        final PreferenceFragmentCompat this$0;
        
        DividerDecoration(final PreferenceFragmentCompat this$0) {
            this.this$0 = this$0;
            this.mAllowDividerAfterLastItem = true;
        }
        
        private boolean shouldDrawDividerBelow(final View view, final RecyclerView recyclerView) {
            final RecyclerView.ViewHolder childViewHolder = recyclerView.getChildViewHolder(view);
            final boolean b = childViewHolder instanceof PreferenceViewHolder;
            final boolean b2 = false;
            if (!b || !((PreferenceViewHolder)childViewHolder).isDividerAllowedBelow()) {
                return false;
            }
            boolean mAllowDividerAfterLastItem = this.mAllowDividerAfterLastItem;
            final int indexOfChild = recyclerView.indexOfChild(view);
            if (indexOfChild < recyclerView.getChildCount() - 1) {
                final RecyclerView.ViewHolder childViewHolder2 = recyclerView.getChildViewHolder(recyclerView.getChildAt(indexOfChild + 1));
                mAllowDividerAfterLastItem = b2;
                if (childViewHolder2 instanceof PreferenceViewHolder) {
                    mAllowDividerAfterLastItem = b2;
                    if (((PreferenceViewHolder)childViewHolder2).isDividerAllowedAbove()) {
                        mAllowDividerAfterLastItem = true;
                    }
                }
            }
            return mAllowDividerAfterLastItem;
        }
        
        @Override
        public void getItemOffsets(final Rect rect, final View view, final RecyclerView recyclerView, final State state) {
            if (this.shouldDrawDividerBelow(view, recyclerView)) {
                rect.bottom = this.mDividerHeight;
            }
        }
        
        @Override
        public void onDrawOver(final Canvas canvas, final RecyclerView recyclerView, final State state) {
            if (this.mDivider == null) {
                return;
            }
            final int childCount = recyclerView.getChildCount();
            final int width = recyclerView.getWidth();
            for (int i = 0; i < childCount; ++i) {
                final View child = recyclerView.getChildAt(i);
                if (this.shouldDrawDividerBelow(child, recyclerView)) {
                    final int n = (int)child.getY() + child.getHeight();
                    this.mDivider.setBounds(0, n, width, this.mDividerHeight + n);
                    this.mDivider.draw(canvas);
                }
            }
        }
        
        public void setAllowDividerAfterLastItem(final boolean mAllowDividerAfterLastItem) {
            this.mAllowDividerAfterLastItem = mAllowDividerAfterLastItem;
        }
        
        public void setDivider(final Drawable mDivider) {
            if (mDivider != null) {
                this.mDividerHeight = mDivider.getIntrinsicHeight();
            }
            else {
                this.mDividerHeight = 0;
            }
            this.mDivider = mDivider;
            this.this$0.mList.invalidateItemDecorations();
        }
        
        public void setDividerHeight(final int mDividerHeight) {
            this.mDividerHeight = mDividerHeight;
            this.this$0.mList.invalidateItemDecorations();
        }
    }
    
    public interface OnPreferenceDisplayDialogCallback
    {
        boolean onPreferenceDisplayDialog(final PreferenceFragmentCompat p0, final Preference p1);
    }
    
    public interface OnPreferenceStartFragmentCallback
    {
        boolean onPreferenceStartFragment(final PreferenceFragmentCompat p0, final Preference p1);
    }
    
    public interface OnPreferenceStartScreenCallback
    {
        boolean onPreferenceStartScreen(final PreferenceFragmentCompat p0, final PreferenceScreen p1);
    }
    
    private static class ScrollToPreferenceObserver extends AdapterDataObserver
    {
        private final Adapter<?> mAdapter;
        private final String mKey;
        private final RecyclerView mList;
        private final Preference mPreference;
        
        ScrollToPreferenceObserver(final Adapter<?> mAdapter, final RecyclerView mList, final Preference mPreference, final String mKey) {
            this.mAdapter = mAdapter;
            this.mList = mList;
            this.mPreference = mPreference;
            this.mKey = mKey;
        }
        
        private void scrollToPreference() {
            this.mAdapter.unregisterAdapterDataObserver(this);
            final Preference mPreference = this.mPreference;
            int n;
            if (mPreference != null) {
                n = ((PreferenceGroup.PreferencePositionCallback)this.mAdapter).getPreferenceAdapterPosition(mPreference);
            }
            else {
                n = ((PreferenceGroup.PreferencePositionCallback)this.mAdapter).getPreferenceAdapterPosition(this.mKey);
            }
            if (n != -1) {
                this.mList.scrollToPosition(n);
            }
        }
        
        @Override
        public void onChanged() {
            this.scrollToPreference();
        }
        
        @Override
        public void onItemRangeChanged(final int n, final int n2) {
            this.scrollToPreference();
        }
        
        @Override
        public void onItemRangeChanged(final int n, final int n2, final Object o) {
            this.scrollToPreference();
        }
        
        @Override
        public void onItemRangeInserted(final int n, final int n2) {
            this.scrollToPreference();
        }
        
        @Override
        public void onItemRangeMoved(final int n, final int n2, final int n3) {
            this.scrollToPreference();
        }
        
        @Override
        public void onItemRangeRemoved(final int n, final int n2) {
            this.scrollToPreference();
        }
    }
}

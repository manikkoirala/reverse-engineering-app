// 
// Decompiled by Procyon v0.6.0
// 

package androidx.preference;

import androidx.lifecycle.LifecycleOwner;
import androidx.activity.OnBackPressedDispatcher;
import androidx.activity.OnBackPressedDispatcherOwner;
import androidx.activity.ViewTreeOnBackPressedDispatcherOwner;
import android.view.View$OnLayoutChangeListener;
import androidx.core.view.ViewCompat;
import androidx.fragment.app.FragmentFactory;
import android.os.Bundle;
import android.view.ViewGroup;
import android.content.Context;
import androidx.fragment.app.FragmentTransaction;
import androidx.fragment.app.FragmentManager;
import android.content.Intent;
import kotlin.jvm.internal.Intrinsics;
import android.view.ViewGroup$LayoutParams;
import android.view.View;
import androidx.fragment.app.FragmentContainerView;
import androidx.slidingpanelayout.widget.SlidingPaneLayout;
import android.view.LayoutInflater;
import androidx.activity.OnBackPressedCallback;
import kotlin.Metadata;
import androidx.fragment.app.Fragment;

@Metadata(d1 = { "\u0000b\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\b&\u0018\u00002\u00020\u00012\u00020\u0002:\u0001&B\u0005¢\u0006\u0002\u0010\u0003J\u0010\u0010\n\u001a\u00020\u00072\u0006\u0010\u000b\u001a\u00020\fH\u0002J\u0010\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0010H\u0017J\n\u0010\u0011\u001a\u0004\u0018\u00010\u0001H\u0016J\b\u0010\u0012\u001a\u00020\u0013H&J$\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u000b\u001a\u00020\f2\b\u0010\u0016\u001a\u0004\u0018\u00010\u00172\b\u0010\u0018\u001a\u0004\u0018\u00010\u0019H\u0017J\u0018\u0010\u001a\u001a\u00020\u001b2\u0006\u0010\u001c\u001a\u00020\u00132\u0006\u0010\u001d\u001a\u00020\u001eH\u0017J\u001a\u0010\u001f\u001a\u00020\u000e2\u0006\u0010 \u001a\u00020\u00152\b\u0010\u0018\u001a\u0004\u0018\u00010\u0019H\u0017J\u0012\u0010!\u001a\u00020\u000e2\b\u0010\u0018\u001a\u0004\u0018\u00010\u0019H\u0016J\u0012\u0010\"\u001a\u00020\u000e2\b\u0010#\u001a\u0004\u0018\u00010$H\u0002J\u0010\u0010\"\u001a\u00020\u000e2\u0006\u0010%\u001a\u00020\u001eH\u0002R\u0010\u0010\u0004\u001a\u0004\u0018\u00010\u0005X\u0082\u000e¢\u0006\u0002\n\u0000R\u0011\u0010\u0006\u001a\u00020\u00078F¢\u0006\u0006\u001a\u0004\b\b\u0010\t¨\u0006'" }, d2 = { "Landroidx/preference/PreferenceHeaderFragmentCompat;", "Landroidx/fragment/app/Fragment;", "Landroidx/preference/PreferenceFragmentCompat$OnPreferenceStartFragmentCallback;", "()V", "onBackPressedCallback", "Landroidx/activity/OnBackPressedCallback;", "slidingPaneLayout", "Landroidx/slidingpanelayout/widget/SlidingPaneLayout;", "getSlidingPaneLayout", "()Landroidx/slidingpanelayout/widget/SlidingPaneLayout;", "buildContentView", "inflater", "Landroid/view/LayoutInflater;", "onAttach", "", "context", "Landroid/content/Context;", "onCreateInitialDetailFragment", "onCreatePreferenceHeader", "Landroidx/preference/PreferenceFragmentCompat;", "onCreateView", "Landroid/view/View;", "container", "Landroid/view/ViewGroup;", "savedInstanceState", "Landroid/os/Bundle;", "onPreferenceStartFragment", "", "caller", "pref", "Landroidx/preference/Preference;", "onViewCreated", "view", "onViewStateRestored", "openPreferenceHeader", "intent", "Landroid/content/Intent;", "header", "InnerOnBackPressedCallback", "preference_release" }, k = 1, mv = { 1, 6, 0 }, xi = 48)
public abstract class PreferenceHeaderFragmentCompat extends Fragment implements OnPreferenceStartFragmentCallback
{
    private OnBackPressedCallback onBackPressedCallback;
    
    public static final /* synthetic */ OnBackPressedCallback access$getOnBackPressedCallback$p(final PreferenceHeaderFragmentCompat preferenceHeaderFragmentCompat) {
        return preferenceHeaderFragmentCompat.onBackPressedCallback;
    }
    
    private final SlidingPaneLayout buildContentView(final LayoutInflater layoutInflater) {
        final SlidingPaneLayout slidingPaneLayout = new SlidingPaneLayout(layoutInflater.getContext());
        slidingPaneLayout.setId(R.id.preferences_sliding_pane_layout);
        final FragmentContainerView fragmentContainerView = new FragmentContainerView(layoutInflater.getContext());
        fragmentContainerView.setId(R.id.preferences_header);
        final SlidingPaneLayout.LayoutParams layoutParams = new SlidingPaneLayout.LayoutParams(this.getResources().getDimensionPixelSize(R.dimen.preferences_header_width), -1);
        layoutParams.weight = (float)this.getResources().getInteger(R.integer.preferences_header_pane_weight);
        slidingPaneLayout.addView((View)fragmentContainerView, (ViewGroup$LayoutParams)layoutParams);
        final FragmentContainerView fragmentContainerView2 = new FragmentContainerView(layoutInflater.getContext());
        fragmentContainerView2.setId(R.id.preferences_detail);
        final SlidingPaneLayout.LayoutParams layoutParams2 = new SlidingPaneLayout.LayoutParams(this.getResources().getDimensionPixelSize(R.dimen.preferences_detail_width), -1);
        layoutParams2.weight = (float)this.getResources().getInteger(R.integer.preferences_detail_pane_weight);
        slidingPaneLayout.addView((View)fragmentContainerView2, (ViewGroup$LayoutParams)layoutParams2);
        return slidingPaneLayout;
    }
    
    private static final void onViewCreated$lambda-10(final PreferenceHeaderFragmentCompat preferenceHeaderFragmentCompat) {
        Intrinsics.checkNotNullParameter((Object)preferenceHeaderFragmentCompat, "this$0");
        final OnBackPressedCallback onBackPressedCallback = preferenceHeaderFragmentCompat.onBackPressedCallback;
        Intrinsics.checkNotNull((Object)onBackPressedCallback);
        onBackPressedCallback.setEnabled(preferenceHeaderFragmentCompat.getChildFragmentManager().getBackStackEntryCount() == 0);
    }
    
    private final void openPreferenceHeader(final Intent intent) {
        if (intent == null) {
            return;
        }
        this.startActivity(intent);
    }
    
    private final void openPreferenceHeader(final Preference preference) {
        if (preference.getFragment() == null) {
            this.openPreferenceHeader(preference.getIntent());
            return;
        }
        final String fragment = preference.getFragment();
        Fragment instantiate;
        if (fragment == null) {
            instantiate = null;
        }
        else {
            instantiate = this.getChildFragmentManager().getFragmentFactory().instantiate(this.requireContext().getClassLoader(), fragment);
        }
        if (instantiate != null) {
            instantiate.setArguments(preference.getExtras());
        }
        if (this.getChildFragmentManager().getBackStackEntryCount() > 0) {
            final FragmentManager.BackStackEntry backStackEntry = this.getChildFragmentManager().getBackStackEntryAt(0);
            Intrinsics.checkNotNullExpressionValue((Object)backStackEntry, "childFragmentManager.getBackStackEntryAt(0)");
            this.getChildFragmentManager().popBackStack(backStackEntry.getId(), 1);
        }
        final FragmentManager childFragmentManager = this.getChildFragmentManager();
        Intrinsics.checkNotNullExpressionValue((Object)childFragmentManager, "childFragmentManager");
        final FragmentTransaction beginTransaction = childFragmentManager.beginTransaction();
        Intrinsics.checkNotNullExpressionValue((Object)beginTransaction, "beginTransaction()");
        beginTransaction.setReorderingAllowed(true);
        final int preferences_detail = R.id.preferences_detail;
        Intrinsics.checkNotNull((Object)instantiate);
        beginTransaction.replace(preferences_detail, instantiate);
        if (this.getSlidingPaneLayout().isOpen()) {
            beginTransaction.setTransition(4099);
        }
        this.getSlidingPaneLayout().openPane();
        beginTransaction.commit();
    }
    
    public final SlidingPaneLayout getSlidingPaneLayout() {
        return (SlidingPaneLayout)this.requireView();
    }
    
    @Override
    public void onAttach(final Context context) {
        Intrinsics.checkNotNullParameter((Object)context, "context");
        super.onAttach(context);
        final FragmentManager parentFragmentManager = this.getParentFragmentManager();
        Intrinsics.checkNotNullExpressionValue((Object)parentFragmentManager, "parentFragmentManager");
        final FragmentTransaction beginTransaction = parentFragmentManager.beginTransaction();
        Intrinsics.checkNotNullExpressionValue((Object)beginTransaction, "beginTransaction()");
        beginTransaction.setPrimaryNavigationFragment(this);
        beginTransaction.commit();
    }
    
    public Fragment onCreateInitialDetailFragment() {
        final Fragment fragmentById = this.getChildFragmentManager().findFragmentById(R.id.preferences_header);
        if (fragmentById == null) {
            throw new NullPointerException("null cannot be cast to non-null type androidx.preference.PreferenceFragmentCompat");
        }
        final PreferenceFragmentCompat preferenceFragmentCompat = (PreferenceFragmentCompat)fragmentById;
        final int preferenceCount = preferenceFragmentCompat.getPreferenceScreen().getPreferenceCount();
        final Fragment fragment = null;
        final Fragment fragment2 = null;
        if (preferenceCount <= 0) {
            return null;
        }
        int n = 0;
        final int preferenceCount2 = preferenceFragmentCompat.getPreferenceScreen().getPreferenceCount();
        Fragment instantiate;
        while (true) {
            instantiate = fragment;
            if (n >= preferenceCount2) {
                break;
            }
            final Preference preference = preferenceFragmentCompat.getPreferenceScreen().getPreference(n);
            Intrinsics.checkNotNullExpressionValue((Object)preference, "headerFragment.preferenc\u2026reen.getPreference(index)");
            if (preference.getFragment() == null) {
                ++n;
            }
            else {
                final String fragment3 = preference.getFragment();
                if (fragment3 == null) {
                    instantiate = fragment2;
                }
                else {
                    instantiate = this.getChildFragmentManager().getFragmentFactory().instantiate(this.requireContext().getClassLoader(), fragment3);
                }
                if (instantiate == null) {
                    break;
                }
                instantiate.setArguments(preference.getExtras());
                break;
            }
        }
        return instantiate;
    }
    
    public abstract PreferenceFragmentCompat onCreatePreferenceHeader();
    
    @Override
    public View onCreateView(final LayoutInflater layoutInflater, final ViewGroup viewGroup, final Bundle bundle) {
        Intrinsics.checkNotNullParameter((Object)layoutInflater, "inflater");
        final SlidingPaneLayout buildContentView = this.buildContentView(layoutInflater);
        if (this.getChildFragmentManager().findFragmentById(R.id.preferences_header) == null) {
            final PreferenceFragmentCompat onCreatePreferenceHeader = this.onCreatePreferenceHeader();
            final FragmentManager childFragmentManager = this.getChildFragmentManager();
            Intrinsics.checkNotNullExpressionValue((Object)childFragmentManager, "childFragmentManager");
            final FragmentTransaction beginTransaction = childFragmentManager.beginTransaction();
            Intrinsics.checkNotNullExpressionValue((Object)beginTransaction, "beginTransaction()");
            beginTransaction.setReorderingAllowed(true);
            beginTransaction.add(R.id.preferences_header, onCreatePreferenceHeader);
            beginTransaction.commit();
        }
        buildContentView.setLockMode(3);
        return (View)buildContentView;
    }
    
    @Override
    public boolean onPreferenceStartFragment(final PreferenceFragmentCompat preferenceFragmentCompat, final Preference preference) {
        Intrinsics.checkNotNullParameter((Object)preferenceFragmentCompat, "caller");
        Intrinsics.checkNotNullParameter((Object)preference, "pref");
        if (preferenceFragmentCompat.getId() == R.id.preferences_header) {
            this.openPreferenceHeader(preference);
            return true;
        }
        if (preferenceFragmentCompat.getId() == R.id.preferences_detail) {
            final FragmentFactory fragmentFactory = this.getChildFragmentManager().getFragmentFactory();
            final ClassLoader classLoader = this.requireContext().getClassLoader();
            final String fragment = preference.getFragment();
            Intrinsics.checkNotNull((Object)fragment);
            final Fragment instantiate = fragmentFactory.instantiate(classLoader, fragment);
            Intrinsics.checkNotNullExpressionValue((Object)instantiate, "childFragmentManager.fra\u2026.fragment!!\n            )");
            instantiate.setArguments(preference.getExtras());
            final FragmentManager childFragmentManager = this.getChildFragmentManager();
            Intrinsics.checkNotNullExpressionValue((Object)childFragmentManager, "childFragmentManager");
            final FragmentTransaction beginTransaction = childFragmentManager.beginTransaction();
            Intrinsics.checkNotNullExpressionValue((Object)beginTransaction, "beginTransaction()");
            beginTransaction.setReorderingAllowed(true);
            beginTransaction.replace(R.id.preferences_detail, instantiate);
            beginTransaction.setTransition(4099);
            beginTransaction.addToBackStack(null);
            beginTransaction.commit();
            return true;
        }
        return false;
    }
    
    @Override
    public void onViewCreated(final View view, final Bundle bundle) {
        Intrinsics.checkNotNullParameter((Object)view, "view");
        super.onViewCreated(view, bundle);
        this.onBackPressedCallback = new InnerOnBackPressedCallback(this);
        final View view2 = (View)this.getSlidingPaneLayout();
        if (ViewCompat.isLaidOut(view2) && !view2.isLayoutRequested()) {
            final OnBackPressedCallback access$getOnBackPressedCallback$p = access$getOnBackPressedCallback$p(this);
            Intrinsics.checkNotNull((Object)access$getOnBackPressedCallback$p);
            access$getOnBackPressedCallback$p.setEnabled(this.getSlidingPaneLayout().isSlideable() && this.getSlidingPaneLayout().isOpen());
        }
        else {
            view2.addOnLayoutChangeListener((View$OnLayoutChangeListener)new PreferenceHeaderFragmentCompat$onViewCreated$$inlined$doOnLayout.PreferenceHeaderFragmentCompat$onViewCreated$$inlined$doOnLayout$1(this));
        }
        this.getChildFragmentManager().addOnBackStackChangedListener((FragmentManager.OnBackStackChangedListener)new PreferenceHeaderFragmentCompat$$ExternalSyntheticLambda0(this));
        final OnBackPressedDispatcherOwner value = ViewTreeOnBackPressedDispatcherOwner.get(view);
        if (value != null) {
            final OnBackPressedDispatcher onBackPressedDispatcher = value.getOnBackPressedDispatcher();
            if (onBackPressedDispatcher != null) {
                final LifecycleOwner viewLifecycleOwner = this.getViewLifecycleOwner();
                final OnBackPressedCallback onBackPressedCallback = this.onBackPressedCallback;
                Intrinsics.checkNotNull((Object)onBackPressedCallback);
                onBackPressedDispatcher.addCallback(viewLifecycleOwner, onBackPressedCallback);
            }
        }
    }
    
    @Override
    public void onViewStateRestored(final Bundle bundle) {
        super.onViewStateRestored(bundle);
        if (bundle == null) {
            final Fragment onCreateInitialDetailFragment = this.onCreateInitialDetailFragment();
            if (onCreateInitialDetailFragment != null) {
                final FragmentManager childFragmentManager = this.getChildFragmentManager();
                Intrinsics.checkNotNullExpressionValue((Object)childFragmentManager, "childFragmentManager");
                final FragmentTransaction beginTransaction = childFragmentManager.beginTransaction();
                Intrinsics.checkNotNullExpressionValue((Object)beginTransaction, "beginTransaction()");
                beginTransaction.setReorderingAllowed(true);
                beginTransaction.replace(R.id.preferences_detail, onCreateInitialDetailFragment);
                beginTransaction.commit();
            }
        }
    }
    
    @Metadata(d1 = { "\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0007\n\u0000\b\u0002\u0018\u00002\u00020\u00012\u00020\u0002B\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004¢\u0006\u0002\u0010\u0005J\b\u0010\u0006\u001a\u00020\u0007H\u0016J\u0010\u0010\b\u001a\u00020\u00072\u0006\u0010\t\u001a\u00020\nH\u0016J\u0010\u0010\u000b\u001a\u00020\u00072\u0006\u0010\t\u001a\u00020\nH\u0016J\u0018\u0010\f\u001a\u00020\u00072\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\r\u001a\u00020\u000eH\u0016R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u000f" }, d2 = { "Landroidx/preference/PreferenceHeaderFragmentCompat$InnerOnBackPressedCallback;", "Landroidx/activity/OnBackPressedCallback;", "Landroidx/slidingpanelayout/widget/SlidingPaneLayout$PanelSlideListener;", "caller", "Landroidx/preference/PreferenceHeaderFragmentCompat;", "(Landroidx/preference/PreferenceHeaderFragmentCompat;)V", "handleOnBackPressed", "", "onPanelClosed", "panel", "Landroid/view/View;", "onPanelOpened", "onPanelSlide", "slideOffset", "", "preference_release" }, k = 1, mv = { 1, 6, 0 }, xi = 48)
    private static final class InnerOnBackPressedCallback extends OnBackPressedCallback implements PanelSlideListener
    {
        private final PreferenceHeaderFragmentCompat caller;
        
        public InnerOnBackPressedCallback(final PreferenceHeaderFragmentCompat caller) {
            Intrinsics.checkNotNullParameter((Object)caller, "caller");
            super(true);
            this.caller = caller;
            caller.getSlidingPaneLayout().addPanelSlideListener((PanelSlideListener)this);
        }
        
        @Override
        public void handleOnBackPressed() {
            this.caller.getSlidingPaneLayout().closePane();
        }
        
        @Override
        public void onPanelClosed(final View view) {
            Intrinsics.checkNotNullParameter((Object)view, "panel");
            this.setEnabled(false);
        }
        
        @Override
        public void onPanelOpened(final View view) {
            Intrinsics.checkNotNullParameter((Object)view, "panel");
            this.setEnabled(true);
        }
        
        @Override
        public void onPanelSlide(final View view, final float n) {
            Intrinsics.checkNotNullParameter((Object)view, "panel");
        }
    }
}

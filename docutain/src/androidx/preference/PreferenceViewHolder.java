// 
// Decompiled by Procyon v0.6.0
// 

package androidx.preference;

import androidx.core.view.ViewCompat;
import android.widget.TextView;
import android.content.res.ColorStateList;
import android.view.View;
import android.util.SparseArray;
import android.graphics.drawable.Drawable;
import androidx.recyclerview.widget.RecyclerView;

public class PreferenceViewHolder extends ViewHolder
{
    private final Drawable mBackground;
    private final SparseArray<View> mCachedViews;
    private boolean mDividerAllowedAbove;
    private boolean mDividerAllowedBelow;
    private ColorStateList mTitleTextColors;
    
    PreferenceViewHolder(final View view) {
        super(view);
        final SparseArray mCachedViews = new SparseArray(4);
        this.mCachedViews = (SparseArray<View>)mCachedViews;
        final TextView textView = (TextView)view.findViewById(16908310);
        mCachedViews.put(16908310, (Object)textView);
        mCachedViews.put(16908304, (Object)view.findViewById(16908304));
        mCachedViews.put(16908294, (Object)view.findViewById(16908294));
        mCachedViews.put(R.id.icon_frame, (Object)view.findViewById(R.id.icon_frame));
        mCachedViews.put(16908350, (Object)view.findViewById(16908350));
        this.mBackground = view.getBackground();
        if (textView != null) {
            this.mTitleTextColors = textView.getTextColors();
        }
    }
    
    public static PreferenceViewHolder createInstanceForTests(final View view) {
        return new PreferenceViewHolder(view);
    }
    
    public View findViewById(final int n) {
        final View view = (View)this.mCachedViews.get(n);
        if (view != null) {
            return view;
        }
        final View viewById = this.itemView.findViewById(n);
        if (viewById != null) {
            this.mCachedViews.put(n, (Object)viewById);
        }
        return viewById;
    }
    
    public boolean isDividerAllowedAbove() {
        return this.mDividerAllowedAbove;
    }
    
    public boolean isDividerAllowedBelow() {
        return this.mDividerAllowedBelow;
    }
    
    void resetState() {
        if (this.itemView.getBackground() != this.mBackground) {
            ViewCompat.setBackground(this.itemView, this.mBackground);
        }
        final TextView textView = (TextView)this.findViewById(16908310);
        if (textView != null && this.mTitleTextColors != null && !textView.getTextColors().equals(this.mTitleTextColors)) {
            textView.setTextColor(this.mTitleTextColors);
        }
    }
    
    public void setDividerAllowedAbove(final boolean mDividerAllowedAbove) {
        this.mDividerAllowedAbove = mDividerAllowedAbove;
    }
    
    public void setDividerAllowedBelow(final boolean mDividerAllowedBelow) {
        this.mDividerAllowedBelow = mDividerAllowedBelow;
    }
}

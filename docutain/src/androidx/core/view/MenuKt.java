// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.view;

import java.util.Iterator;
import kotlin.sequences.Sequence;
import kotlin.jvm.functions.Function2;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import android.view.MenuItem;
import android.view.Menu;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000D\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010)\n\u0002\b\u0003\u001a\u0015\u0010\n\u001a\u00020\u000b*\u00020\u00032\u0006\u0010\f\u001a\u00020\u0002H\u0086\u0002\u001a0\u0010\r\u001a\u00020\u000e*\u00020\u00032!\u0010\u000f\u001a\u001d\u0012\u0013\u0012\u00110\u0002¢\u0006\f\b\u0011\u0012\b\b\u0012\u0012\u0004\b\b(\f\u0012\u0004\u0012\u00020\u000e0\u0010H\u0086\b\u001aE\u0010\u0013\u001a\u00020\u000e*\u00020\u000326\u0010\u000f\u001a2\u0012\u0013\u0012\u00110\u0007¢\u0006\f\b\u0011\u0012\b\b\u0012\u0012\u0004\b\b(\u0015\u0012\u0013\u0012\u00110\u0002¢\u0006\f\b\u0011\u0012\b\b\u0012\u0012\u0004\b\b(\f\u0012\u0004\u0012\u00020\u000e0\u0014H\u0086\b\u001a\u0015\u0010\u0016\u001a\u00020\u0002*\u00020\u00032\u0006\u0010\u0015\u001a\u00020\u0007H\u0086\n\u001a\r\u0010\u0017\u001a\u00020\u000b*\u00020\u0003H\u0086\b\u001a\r\u0010\u0018\u001a\u00020\u000b*\u00020\u0003H\u0086\b\u001a\u0013\u0010\u0019\u001a\b\u0012\u0004\u0012\u00020\u00020\u001a*\u00020\u0003H\u0086\u0002\u001a\u0015\u0010\u001b\u001a\u00020\u000e*\u00020\u00032\u0006\u0010\f\u001a\u00020\u0002H\u0086\n\u001a\u0015\u0010\u001c\u001a\u00020\u000e*\u00020\u00032\u0006\u0010\u0015\u001a\u00020\u0007H\u0086\b\"\u001b\u0010\u0000\u001a\b\u0012\u0004\u0012\u00020\u00020\u0001*\u00020\u00038F¢\u0006\u0006\u001a\u0004\b\u0004\u0010\u0005\"\u0016\u0010\u0006\u001a\u00020\u0007*\u00020\u00038\u00c6\u0002¢\u0006\u0006\u001a\u0004\b\b\u0010\t¨\u0006\u001d" }, d2 = { "children", "Lkotlin/sequences/Sequence;", "Landroid/view/MenuItem;", "Landroid/view/Menu;", "getChildren", "(Landroid/view/Menu;)Lkotlin/sequences/Sequence;", "size", "", "getSize", "(Landroid/view/Menu;)I", "contains", "", "item", "forEach", "", "action", "Lkotlin/Function1;", "Lkotlin/ParameterName;", "name", "forEachIndexed", "Lkotlin/Function2;", "index", "get", "isEmpty", "isNotEmpty", "iterator", "", "minusAssign", "removeItemAt", "core-ktx_release" }, k = 2, mv = { 1, 8, 0 }, xi = 48)
public final class MenuKt
{
    public static final boolean contains(final Menu menu, final MenuItem menuItem) {
        for (int size = menu.size(), i = 0; i < size; ++i) {
            if (Intrinsics.areEqual((Object)menu.getItem(i), (Object)menuItem)) {
                return true;
            }
        }
        return false;
    }
    
    public static final void forEach(final Menu menu, final Function1<? super MenuItem, Unit> function1) {
        for (int size = menu.size(), i = 0; i < size; ++i) {
            function1.invoke((Object)menu.getItem(i));
        }
    }
    
    public static final void forEachIndexed(final Menu menu, final Function2<? super Integer, ? super MenuItem, Unit> function2) {
        for (int size = menu.size(), i = 0; i < size; ++i) {
            function2.invoke((Object)i, (Object)menu.getItem(i));
        }
    }
    
    public static final MenuItem get(final Menu menu, final int n) {
        return menu.getItem(n);
    }
    
    public static final Sequence<MenuItem> getChildren(final Menu menu) {
        return (Sequence<MenuItem>)new MenuKt$children.MenuKt$children$1(menu);
    }
    
    public static final int getSize(final Menu menu) {
        return menu.size();
    }
    
    public static final boolean isEmpty(final Menu menu) {
        return menu.size() == 0;
    }
    
    public static final boolean isNotEmpty(final Menu menu) {
        return menu.size() != 0;
    }
    
    public static final Iterator<MenuItem> iterator(final Menu menu) {
        return (Iterator<MenuItem>)new MenuKt$iterator.MenuKt$iterator$1(menu);
    }
    
    public static final void minusAssign(final Menu menu, final MenuItem menuItem) {
        menu.removeItem(menuItem.getItemId());
    }
    
    public static final void removeItemAt(final Menu menu, final int n) {
        final MenuItem item = menu.getItem(n);
        Unit instance;
        if (item != null) {
            menu.removeItem(item.getItemId());
            instance = Unit.INSTANCE;
        }
        else {
            instance = null;
        }
        if (instance != null) {
            return;
        }
        throw new IndexOutOfBoundsException();
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.view;

import android.view.ViewGroup$MarginLayoutParams;
import java.util.Iterator;
import kotlin.ranges.RangesKt;
import kotlin.ranges.IntRange;
import kotlin.sequences.SequencesKt;
import kotlin.coroutines.Continuation;
import kotlin.sequences.Sequence;
import kotlin.jvm.functions.Function2;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import android.view.View;
import android.view.ViewGroup;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000T\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010)\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\t\u001a\u0015\u0010\u0010\u001a\u00020\u0011*\u00020\u00032\u0006\u0010\u0012\u001a\u00020\u0002H\u0086\n\u001a0\u0010\u0013\u001a\u00020\u0014*\u00020\u00032!\u0010\u0015\u001a\u001d\u0012\u0013\u0012\u00110\u0002¢\u0006\f\b\u0017\u0012\b\b\u0018\u0012\u0004\b\b(\u0012\u0012\u0004\u0012\u00020\u00140\u0016H\u0086\b\u001aE\u0010\u0019\u001a\u00020\u0014*\u00020\u000326\u0010\u0015\u001a2\u0012\u0013\u0012\u00110\r¢\u0006\f\b\u0017\u0012\b\b\u0018\u0012\u0004\b\b(\u001b\u0012\u0013\u0012\u00110\u0002¢\u0006\f\b\u0017\u0012\b\b\u0018\u0012\u0004\b\b(\u0012\u0012\u0004\u0012\u00020\u00140\u001aH\u0086\b\u001a\u0015\u0010\u001c\u001a\u00020\u0002*\u00020\u00032\u0006\u0010\u001b\u001a\u00020\rH\u0086\u0002\u001a\r\u0010\u001d\u001a\u00020\u0011*\u00020\u0003H\u0086\b\u001a\r\u0010\u001e\u001a\u00020\u0011*\u00020\u0003H\u0086\b\u001a\u0013\u0010\u001f\u001a\b\u0012\u0004\u0012\u00020\u00020 *\u00020\u0003H\u0086\u0002\u001a\u0015\u0010!\u001a\u00020\u0014*\u00020\u00032\u0006\u0010\u0012\u001a\u00020\u0002H\u0086\n\u001a\u0015\u0010\"\u001a\u00020\u0014*\u00020\u00032\u0006\u0010\u0012\u001a\u00020\u0002H\u0086\n\u001a\u0017\u0010#\u001a\u00020\u0014*\u00020$2\b\b\u0001\u0010\f\u001a\u00020\rH\u0086\b\u001a5\u0010%\u001a\u00020\u0014*\u00020$2\b\b\u0003\u0010&\u001a\u00020\r2\b\b\u0003\u0010'\u001a\u00020\r2\b\b\u0003\u0010(\u001a\u00020\r2\b\b\u0003\u0010)\u001a\u00020\rH\u0086\b\u001a5\u0010*\u001a\u00020\u0014*\u00020$2\b\b\u0003\u0010+\u001a\u00020\r2\b\b\u0003\u0010'\u001a\u00020\r2\b\b\u0003\u0010,\u001a\u00020\r2\b\b\u0003\u0010)\u001a\u00020\rH\u0087\b\"\u001b\u0010\u0000\u001a\b\u0012\u0004\u0012\u00020\u00020\u0001*\u00020\u00038F¢\u0006\u0006\u001a\u0004\b\u0004\u0010\u0005\"\u001b\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00020\u0001*\u00020\u00038F¢\u0006\u0006\u001a\u0004\b\u0007\u0010\u0005\"\u0016\u0010\b\u001a\u00020\t*\u00020\u00038\u00c6\u0002¢\u0006\u0006\u001a\u0004\b\n\u0010\u000b\"\u0016\u0010\f\u001a\u00020\r*\u00020\u00038\u00c6\u0002¢\u0006\u0006\u001a\u0004\b\u000e\u0010\u000f¨\u0006-" }, d2 = { "children", "Lkotlin/sequences/Sequence;", "Landroid/view/View;", "Landroid/view/ViewGroup;", "getChildren", "(Landroid/view/ViewGroup;)Lkotlin/sequences/Sequence;", "descendants", "getDescendants", "indices", "Lkotlin/ranges/IntRange;", "getIndices", "(Landroid/view/ViewGroup;)Lkotlin/ranges/IntRange;", "size", "", "getSize", "(Landroid/view/ViewGroup;)I", "contains", "", "view", "forEach", "", "action", "Lkotlin/Function1;", "Lkotlin/ParameterName;", "name", "forEachIndexed", "Lkotlin/Function2;", "index", "get", "isEmpty", "isNotEmpty", "iterator", "", "minusAssign", "plusAssign", "setMargins", "Landroid/view/ViewGroup$MarginLayoutParams;", "updateMargins", "left", "top", "right", "bottom", "updateMarginsRelative", "start", "end", "core-ktx_release" }, k = 2, mv = { 1, 8, 0 }, xi = 48)
public final class ViewGroupKt
{
    public static final boolean contains(final ViewGroup viewGroup, final View view) {
        return viewGroup.indexOfChild(view) != -1;
    }
    
    public static final void forEach(final ViewGroup viewGroup, final Function1<? super View, Unit> function1) {
        for (int childCount = viewGroup.getChildCount(), i = 0; i < childCount; ++i) {
            function1.invoke((Object)viewGroup.getChildAt(i));
        }
    }
    
    public static final void forEachIndexed(final ViewGroup viewGroup, final Function2<? super Integer, ? super View, Unit> function2) {
        for (int childCount = viewGroup.getChildCount(), i = 0; i < childCount; ++i) {
            function2.invoke((Object)i, (Object)viewGroup.getChildAt(i));
        }
    }
    
    public static final View get(final ViewGroup viewGroup, final int i) {
        final View child = viewGroup.getChildAt(i);
        if (child != null) {
            return child;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Index: ");
        sb.append(i);
        sb.append(", Size: ");
        sb.append(viewGroup.getChildCount());
        throw new IndexOutOfBoundsException(sb.toString());
    }
    
    public static final Sequence<View> getChildren(final ViewGroup viewGroup) {
        return (Sequence<View>)new ViewGroupKt$children.ViewGroupKt$children$1(viewGroup);
    }
    
    public static final Sequence<View> getDescendants(final ViewGroup viewGroup) {
        return (Sequence<View>)SequencesKt.sequence((Function2)new ViewGroupKt$descendants.ViewGroupKt$descendants$1(viewGroup, (Continuation)null));
    }
    
    public static final IntRange getIndices(final ViewGroup viewGroup) {
        return RangesKt.until(0, viewGroup.getChildCount());
    }
    
    public static final int getSize(final ViewGroup viewGroup) {
        return viewGroup.getChildCount();
    }
    
    public static final boolean isEmpty(final ViewGroup viewGroup) {
        return viewGroup.getChildCount() == 0;
    }
    
    public static final boolean isNotEmpty(final ViewGroup viewGroup) {
        return viewGroup.getChildCount() != 0;
    }
    
    public static final Iterator<View> iterator(final ViewGroup viewGroup) {
        return (Iterator<View>)new ViewGroupKt$iterator.ViewGroupKt$iterator$1(viewGroup);
    }
    
    public static final void minusAssign(final ViewGroup viewGroup, final View view) {
        viewGroup.removeView(view);
    }
    
    public static final void plusAssign(final ViewGroup viewGroup, final View view) {
        viewGroup.addView(view);
    }
    
    public static final void setMargins(final ViewGroup$MarginLayoutParams viewGroup$MarginLayoutParams, final int n) {
        viewGroup$MarginLayoutParams.setMargins(n, n, n, n);
    }
    
    public static final void updateMargins(final ViewGroup$MarginLayoutParams viewGroup$MarginLayoutParams, final int n, final int n2, final int n3, final int n4) {
        viewGroup$MarginLayoutParams.setMargins(n, n2, n3, n4);
    }
    
    public static final void updateMarginsRelative(final ViewGroup$MarginLayoutParams viewGroup$MarginLayoutParams, final int marginStart, final int topMargin, final int marginEnd, final int bottomMargin) {
        viewGroup$MarginLayoutParams.setMarginStart(marginStart);
        viewGroup$MarginLayoutParams.topMargin = topMargin;
        viewGroup$MarginLayoutParams.setMarginEnd(marginEnd);
        viewGroup$MarginLayoutParams.bottomMargin = bottomMargin;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.view;

public interface NestedScrollingChild3 extends NestedScrollingChild2
{
    void dispatchNestedScroll(final int p0, final int p1, final int p2, final int p3, final int[] p4, final int p5, final int[] p6);
}

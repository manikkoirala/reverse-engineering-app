// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.view;

import android.view.View;

public class ViewPropertyAnimatorListenerAdapter implements ViewPropertyAnimatorListener
{
    @Override
    public void onAnimationCancel(final View view) {
    }
    
    @Override
    public void onAnimationEnd(final View view) {
    }
    
    @Override
    public void onAnimationStart(final View view) {
    }
}

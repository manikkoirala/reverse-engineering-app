// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.view;

import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;
import android.os.Build$VERSION;

public final class HapticFeedbackConstantsCompat
{
    public static final int CLOCK_TICK = 4;
    public static final int CONFIRM = 16;
    public static final int CONTEXT_CLICK = 6;
    public static final int DRAG_START = 25;
    static final int FIRST_CONSTANT_INT = 0;
    public static final int FLAG_IGNORE_VIEW_SETTING = 1;
    public static final int GESTURE_END = 13;
    public static final int GESTURE_START = 12;
    public static final int GESTURE_THRESHOLD_ACTIVATE = 23;
    public static final int GESTURE_THRESHOLD_DEACTIVATE = 24;
    public static final int KEYBOARD_PRESS = 3;
    public static final int KEYBOARD_RELEASE = 7;
    public static final int KEYBOARD_TAP = 3;
    static final int LAST_CONSTANT_INT = 27;
    public static final int LONG_PRESS = 0;
    public static final int NO_HAPTICS = -1;
    public static final int REJECT = 17;
    public static final int SEGMENT_FREQUENT_TICK = 27;
    public static final int SEGMENT_TICK = 26;
    public static final int TEXT_HANDLE_MOVE = 9;
    public static final int TOGGLE_OFF = 22;
    public static final int TOGGLE_ON = 21;
    public static final int VIRTUAL_KEY = 1;
    public static final int VIRTUAL_KEY_RELEASE = 8;
    
    private HapticFeedbackConstantsCompat() {
    }
    
    static int getFeedbackConstantOrFallback(int n) {
        final int n2 = -1;
        if (n == -1) {
            return -1;
        }
        final int sdk_INT = Build$VERSION.SDK_INT;
        final int n3 = 0;
        int n4 = n;
        if (sdk_INT < 34) {
            switch (n) {
                default: {
                    n4 = n;
                    break;
                }
                case 25: {
                    n4 = 0;
                    break;
                }
                case 22:
                case 24:
                case 27: {
                    n4 = 4;
                    break;
                }
                case 21:
                case 23:
                case 26: {
                    n4 = 6;
                    break;
                }
            }
        }
        Label_0136: {
            Label_0134: {
                if (Build$VERSION.SDK_INT < 30) {
                    if (n4 != 12) {
                        if (n4 == 13) {
                            n = 6;
                            break Label_0136;
                        }
                        if (n4 != 16) {
                            n = n3;
                            if (n4 != 17) {
                                break Label_0134;
                            }
                            break Label_0136;
                        }
                    }
                    n = 1;
                    break Label_0136;
                }
            }
            n = n4;
        }
        int n5 = n;
        if (Build$VERSION.SDK_INT < 27) {
            if (n != 7 && n != 8 && n != 9) {
                n5 = n;
            }
            else {
                n5 = -1;
            }
        }
        n = n5;
        if (Build$VERSION.SDK_INT < 23) {
            if (n5 != 6) {
                n = n5;
            }
            else {
                n = 4;
            }
        }
        if (Build$VERSION.SDK_INT < 21) {
            final int n6 = n2;
            if (n == 4) {
                return n6;
            }
        }
        return n;
    }
    
    @Retention(RetentionPolicy.SOURCE)
    public @interface HapticFeedbackFlags {
    }
    
    @Retention(RetentionPolicy.SOURCE)
    public @interface HapticFeedbackType {
    }
}

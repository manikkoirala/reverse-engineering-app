// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.view.inputmethod;

import android.text.SpannableStringBuilder;
import androidx.core.util.Preconditions;
import android.os.Bundle;
import android.text.TextUtils;
import android.os.Build$VERSION;
import android.view.inputmethod.EditorInfo;

public final class EditorInfoCompat
{
    private static final String CONTENT_MIME_TYPES_INTEROP_KEY = "android.support.v13.view.inputmethod.EditorInfoCompat.CONTENT_MIME_TYPES";
    private static final String CONTENT_MIME_TYPES_KEY = "androidx.core.view.inputmethod.EditorInfoCompat.CONTENT_MIME_TYPES";
    private static final String CONTENT_SELECTION_END_KEY = "androidx.core.view.inputmethod.EditorInfoCompat.CONTENT_SELECTION_END";
    private static final String CONTENT_SELECTION_HEAD_KEY = "androidx.core.view.inputmethod.EditorInfoCompat.CONTENT_SELECTION_HEAD";
    private static final String CONTENT_SURROUNDING_TEXT_KEY = "androidx.core.view.inputmethod.EditorInfoCompat.CONTENT_SURROUNDING_TEXT";
    private static final String[] EMPTY_STRING_ARRAY;
    public static final int IME_FLAG_FORCE_ASCII = Integer.MIN_VALUE;
    public static final int IME_FLAG_NO_PERSONALIZED_LEARNING = 16777216;
    static final int MAX_INITIAL_SELECTION_LENGTH = 1024;
    static final int MEMORY_EFFICIENT_TEXT_LENGTH = 2048;
    
    static {
        EMPTY_STRING_ARRAY = new String[0];
    }
    
    @Deprecated
    public EditorInfoCompat() {
    }
    
    public static String[] getContentMimeTypes(final EditorInfo editorInfo) {
        if (Build$VERSION.SDK_INT >= 25) {
            String[] array = editorInfo.contentMimeTypes;
            if (array == null) {
                array = EditorInfoCompat.EMPTY_STRING_ARRAY;
            }
            return array;
        }
        if (editorInfo.extras == null) {
            return EditorInfoCompat.EMPTY_STRING_ARRAY;
        }
        String[] array2;
        if ((array2 = editorInfo.extras.getStringArray("androidx.core.view.inputmethod.EditorInfoCompat.CONTENT_MIME_TYPES")) == null) {
            array2 = editorInfo.extras.getStringArray("android.support.v13.view.inputmethod.EditorInfoCompat.CONTENT_MIME_TYPES");
        }
        if (array2 == null) {
            array2 = EditorInfoCompat.EMPTY_STRING_ARRAY;
        }
        return array2;
    }
    
    public static CharSequence getInitialSelectedText(final EditorInfo editorInfo, final int n) {
        if (Build$VERSION.SDK_INT >= 30) {
            return Api30Impl.getInitialSelectedText(editorInfo, n);
        }
        if (editorInfo.extras == null) {
            return null;
        }
        final int min = Math.min(editorInfo.initialSelStart, editorInfo.initialSelEnd);
        final int max = Math.max(editorInfo.initialSelStart, editorInfo.initialSelEnd);
        final int int1 = editorInfo.extras.getInt("androidx.core.view.inputmethod.EditorInfoCompat.CONTENT_SELECTION_HEAD");
        final int int2 = editorInfo.extras.getInt("androidx.core.view.inputmethod.EditorInfoCompat.CONTENT_SELECTION_END");
        if (editorInfo.initialSelStart < 0 || editorInfo.initialSelEnd < 0 || int2 - int1 != max - min) {
            return null;
        }
        final CharSequence charSequence = editorInfo.extras.getCharSequence("androidx.core.view.inputmethod.EditorInfoCompat.CONTENT_SURROUNDING_TEXT");
        if (charSequence == null) {
            return null;
        }
        CharSequence charSequence2;
        if ((n & 0x1) != 0x0) {
            charSequence2 = charSequence.subSequence(int1, int2);
        }
        else {
            charSequence2 = TextUtils.substring(charSequence, int1, int2);
        }
        return charSequence2;
    }
    
    public static CharSequence getInitialTextAfterCursor(final EditorInfo editorInfo, int min, final int n) {
        if (Build$VERSION.SDK_INT >= 30) {
            return Api30Impl.getInitialTextAfterCursor(editorInfo, min, n);
        }
        if (editorInfo.extras == null) {
            return null;
        }
        final CharSequence charSequence = editorInfo.extras.getCharSequence("androidx.core.view.inputmethod.EditorInfoCompat.CONTENT_SURROUNDING_TEXT");
        if (charSequence == null) {
            return null;
        }
        final int int1 = editorInfo.extras.getInt("androidx.core.view.inputmethod.EditorInfoCompat.CONTENT_SELECTION_END");
        min = Math.min(min, charSequence.length() - int1);
        CharSequence charSequence2;
        if ((n & 0x1) != 0x0) {
            charSequence2 = charSequence.subSequence(int1, min + int1);
        }
        else {
            charSequence2 = TextUtils.substring(charSequence, int1, min + int1);
        }
        return charSequence2;
    }
    
    public static CharSequence getInitialTextBeforeCursor(final EditorInfo editorInfo, int min, final int n) {
        if (Build$VERSION.SDK_INT >= 30) {
            return Api30Impl.getInitialTextBeforeCursor(editorInfo, min, n);
        }
        if (editorInfo.extras == null) {
            return null;
        }
        final CharSequence charSequence = editorInfo.extras.getCharSequence("androidx.core.view.inputmethod.EditorInfoCompat.CONTENT_SURROUNDING_TEXT");
        if (charSequence == null) {
            return null;
        }
        final int int1 = editorInfo.extras.getInt("androidx.core.view.inputmethod.EditorInfoCompat.CONTENT_SELECTION_HEAD");
        min = Math.min(min, int1);
        CharSequence charSequence2;
        if ((n & 0x1) != 0x0) {
            charSequence2 = charSequence.subSequence(int1 - min, int1);
        }
        else {
            charSequence2 = TextUtils.substring(charSequence, int1 - min, int1);
        }
        return charSequence2;
    }
    
    static int getProtocol(final EditorInfo editorInfo) {
        if (Build$VERSION.SDK_INT >= 25) {
            return 1;
        }
        if (editorInfo.extras == null) {
            return 0;
        }
        final boolean containsKey = editorInfo.extras.containsKey("androidx.core.view.inputmethod.EditorInfoCompat.CONTENT_MIME_TYPES");
        final boolean containsKey2 = editorInfo.extras.containsKey("android.support.v13.view.inputmethod.EditorInfoCompat.CONTENT_MIME_TYPES");
        if (containsKey && containsKey2) {
            return 4;
        }
        if (containsKey) {
            return 3;
        }
        if (containsKey2) {
            return 2;
        }
        return 0;
    }
    
    private static boolean isCutOnSurrogate(final CharSequence charSequence, final int n, final int n2) {
        if (n2 != 0) {
            return n2 == 1 && Character.isHighSurrogate(charSequence.charAt(n));
        }
        return Character.isLowSurrogate(charSequence.charAt(n));
    }
    
    private static boolean isPasswordInputType(int n) {
        n &= 0xFFF;
        return n == 129 || n == 225 || n == 18;
    }
    
    public static void setContentMimeTypes(final EditorInfo editorInfo, final String[] contentMimeTypes) {
        if (Build$VERSION.SDK_INT >= 25) {
            editorInfo.contentMimeTypes = contentMimeTypes;
        }
        else {
            if (editorInfo.extras == null) {
                editorInfo.extras = new Bundle();
            }
            editorInfo.extras.putStringArray("androidx.core.view.inputmethod.EditorInfoCompat.CONTENT_MIME_TYPES", contentMimeTypes);
            editorInfo.extras.putStringArray("android.support.v13.view.inputmethod.EditorInfoCompat.CONTENT_MIME_TYPES", contentMimeTypes);
        }
    }
    
    public static void setInitialSurroundingSubText(final EditorInfo editorInfo, final CharSequence charSequence, final int n) {
        Preconditions.checkNotNull(charSequence);
        if (Build$VERSION.SDK_INT >= 30) {
            Api30Impl.setInitialSurroundingSubText(editorInfo, charSequence, n);
            return;
        }
        int n2;
        if (editorInfo.initialSelStart > editorInfo.initialSelEnd) {
            n2 = editorInfo.initialSelEnd;
        }
        else {
            n2 = editorInfo.initialSelStart;
        }
        final int n3 = n2 - n;
        int n4;
        if (editorInfo.initialSelStart > editorInfo.initialSelEnd) {
            n4 = editorInfo.initialSelStart;
        }
        else {
            n4 = editorInfo.initialSelEnd;
        }
        final int n5 = n4 - n;
        final int length = charSequence.length();
        if (n < 0 || n3 < 0 || n5 > length) {
            setSurroundingText(editorInfo, null, 0, 0);
            return;
        }
        if (isPasswordInputType(editorInfo.inputType)) {
            setSurroundingText(editorInfo, null, 0, 0);
            return;
        }
        if (length <= 2048) {
            setSurroundingText(editorInfo, charSequence, n3, n5);
            return;
        }
        trimLongSurroundingText(editorInfo, charSequence, n3, n5);
    }
    
    public static void setInitialSurroundingText(final EditorInfo editorInfo, final CharSequence charSequence) {
        if (Build$VERSION.SDK_INT >= 30) {
            Api30Impl.setInitialSurroundingSubText(editorInfo, charSequence, 0);
        }
        else {
            setInitialSurroundingSubText(editorInfo, charSequence, 0);
        }
    }
    
    private static void setSurroundingText(final EditorInfo editorInfo, final CharSequence charSequence, final int n, final int n2) {
        if (editorInfo.extras == null) {
            editorInfo.extras = new Bundle();
        }
        Object o;
        if (charSequence != null) {
            o = new SpannableStringBuilder(charSequence);
        }
        else {
            o = null;
        }
        editorInfo.extras.putCharSequence("androidx.core.view.inputmethod.EditorInfoCompat.CONTENT_SURROUNDING_TEXT", (CharSequence)o);
        editorInfo.extras.putInt("androidx.core.view.inputmethod.EditorInfoCompat.CONTENT_SELECTION_HEAD", n);
        editorInfo.extras.putInt("androidx.core.view.inputmethod.EditorInfoCompat.CONTENT_SELECTION_END", n2);
    }
    
    private static void trimLongSurroundingText(final EditorInfo editorInfo, CharSequence charSequence, int n, final int n2) {
        final int n3 = n2 - n;
        int n4;
        if (n3 > 1024) {
            n4 = 0;
        }
        else {
            n4 = n3;
        }
        final int length = charSequence.length();
        final int n5 = 2048 - n4;
        final int min = Math.min(length - n2, n5 - Math.min(n, (int)(n5 * 0.8)));
        final int min2 = Math.min(n, n5 - min);
        final int n6 = n - min2;
        int n7 = min2;
        n = n6;
        if (isCutOnSurrogate(charSequence, n6, 0)) {
            n = n6 + 1;
            n7 = min2 - 1;
        }
        int n8 = min;
        if (isCutOnSurrogate(charSequence, n2 + min - 1, 1)) {
            n8 = min - 1;
        }
        if (n4 != n3) {
            charSequence = TextUtils.concat(new CharSequence[] { charSequence.subSequence(n, n + n7), charSequence.subSequence(n2, n8 + n2) });
        }
        else {
            charSequence = charSequence.subSequence(n, n7 + n4 + n8 + n);
        }
        n = n7 + 0;
        setSurroundingText(editorInfo, charSequence, n, n4 + n);
    }
    
    private static class Api30Impl
    {
        static CharSequence getInitialSelectedText(final EditorInfo editorInfo, final int n) {
            return editorInfo.getInitialSelectedText(n);
        }
        
        static CharSequence getInitialTextAfterCursor(final EditorInfo editorInfo, final int n, final int n2) {
            return editorInfo.getInitialTextAfterCursor(n, n2);
        }
        
        static CharSequence getInitialTextBeforeCursor(final EditorInfo editorInfo, final int n, final int n2) {
            return editorInfo.getInitialTextBeforeCursor(n, n2);
        }
        
        static void setInitialSurroundingSubText(final EditorInfo editorInfo, final CharSequence charSequence, final int n) {
            editorInfo.setInitialSurroundingSubText(charSequence, n);
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.view;

import android.view.View;

public interface ViewPropertyAnimatorUpdateListener
{
    void onAnimationUpdate(final View p0);
}

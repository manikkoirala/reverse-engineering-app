// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.view;

import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleOwner;

public interface MenuHost
{
    void addMenuProvider(final MenuProvider p0);
    
    void addMenuProvider(final MenuProvider p0, final LifecycleOwner p1);
    
    void addMenuProvider(final MenuProvider p0, final LifecycleOwner p1, final Lifecycle.State p2);
    
    void invalidateMenu();
    
    void removeMenuProvider(final MenuProvider p0);
}

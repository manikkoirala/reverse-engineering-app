// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.view;

import android.view.View;

public interface OnApplyWindowInsetsListener
{
    WindowInsetsCompat onApplyWindowInsets(final View p0, final WindowInsetsCompat p1);
}

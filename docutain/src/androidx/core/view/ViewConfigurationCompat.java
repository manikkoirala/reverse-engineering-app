// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.view;

import android.view.InputDevice;
import android.content.res.Resources;
import android.util.TypedValue;
import android.content.Context;
import android.util.Log;
import android.view.ViewConfiguration;
import android.os.Build$VERSION;
import java.lang.reflect.Method;

public final class ViewConfigurationCompat
{
    private static final String TAG = "ViewConfigCompat";
    private static Method sGetScaledScrollFactorMethod;
    
    static {
        if (Build$VERSION.SDK_INT == 25) {
            try {
                ViewConfigurationCompat.sGetScaledScrollFactorMethod = ViewConfiguration.class.getDeclaredMethod("getScaledScrollFactor", (Class<?>[])new Class[0]);
            }
            catch (final Exception ex) {
                Log.i("ViewConfigCompat", "Could not find method getScaledScrollFactor() on ViewConfiguration");
            }
        }
    }
    
    private ViewConfigurationCompat() {
    }
    
    private static float getLegacyScrollFactor(final ViewConfiguration obj, final Context context) {
        if (Build$VERSION.SDK_INT >= 25) {
            final Method sGetScaledScrollFactorMethod = ViewConfigurationCompat.sGetScaledScrollFactorMethod;
            if (sGetScaledScrollFactorMethod != null) {
                try {
                    return (float)(int)sGetScaledScrollFactorMethod.invoke(obj, new Object[0]);
                }
                catch (final Exception ex) {
                    Log.i("ViewConfigCompat", "Could not find method getScaledScrollFactor() on ViewConfiguration");
                }
            }
        }
        final TypedValue typedValue = new TypedValue();
        if (context.getTheme().resolveAttribute(16842829, typedValue, true)) {
            return typedValue.getDimension(context.getResources().getDisplayMetrics());
        }
        return 0.0f;
    }
    
    private static int getPlatformResId(final Resources resources, final String s, final String s2) {
        return resources.getIdentifier(s, s2, "android");
    }
    
    private static int getPreApi34MaximumFlingVelocityResId(final Resources resources, final int n, final int n2) {
        if (n == 4194304 && n2 == 26) {
            return getPlatformResId(resources, "config_viewMaxRotaryEncoderFlingVelocity", "dimen");
        }
        return 0;
    }
    
    private static int getPreApi34MinimumFlingVelocityResId(final Resources resources, final int n, final int n2) {
        if (n == 4194304 && n2 == 26) {
            return getPlatformResId(resources, "config_viewMinRotaryEncoderFlingVelocity", "dimen");
        }
        return 0;
    }
    
    public static float getScaledHorizontalScrollFactor(final ViewConfiguration viewConfiguration, final Context context) {
        if (Build$VERSION.SDK_INT >= 26) {
            return Api26Impl.getScaledHorizontalScrollFactor(viewConfiguration);
        }
        return getLegacyScrollFactor(viewConfiguration, context);
    }
    
    public static int getScaledHoverSlop(final ViewConfiguration viewConfiguration) {
        if (Build$VERSION.SDK_INT >= 28) {
            return Api28Impl.getScaledHoverSlop(viewConfiguration);
        }
        return viewConfiguration.getScaledTouchSlop() / 2;
    }
    
    public static int getScaledMaximumFlingVelocity(final Context context, final ViewConfiguration viewConfiguration, int n, int n2, final int n3) {
        if (Build$VERSION.SDK_INT >= 34) {
            return Api34Impl.getScaledMaximumFlingVelocity(viewConfiguration, n, n2, n3);
        }
        final boolean inputDeviceInfoValid = isInputDeviceInfoValid(n, n2, n3);
        n = Integer.MIN_VALUE;
        if (!inputDeviceInfoValid) {
            return Integer.MIN_VALUE;
        }
        final Resources resources = context.getResources();
        n2 = getPreApi34MaximumFlingVelocityResId(resources, n3, n2);
        if (n2 != 0) {
            n2 = resources.getDimensionPixelSize(n2);
            if (n2 >= 0) {
                n = n2;
            }
            return n;
        }
        return viewConfiguration.getScaledMaximumFlingVelocity();
    }
    
    public static int getScaledMinimumFlingVelocity(final Context context, final ViewConfiguration viewConfiguration, int n, int n2, final int n3) {
        if (Build$VERSION.SDK_INT >= 34) {
            return Api34Impl.getScaledMinimumFlingVelocity(viewConfiguration, n, n2, n3);
        }
        final boolean inputDeviceInfoValid = isInputDeviceInfoValid(n, n2, n3);
        n = Integer.MAX_VALUE;
        if (!inputDeviceInfoValid) {
            return Integer.MAX_VALUE;
        }
        final Resources resources = context.getResources();
        n2 = getPreApi34MinimumFlingVelocityResId(resources, n3, n2);
        if (n2 != 0) {
            n2 = resources.getDimensionPixelSize(n2);
            if (n2 >= 0) {
                n = n2;
            }
            return n;
        }
        return viewConfiguration.getScaledMinimumFlingVelocity();
    }
    
    @Deprecated
    public static int getScaledPagingTouchSlop(final ViewConfiguration viewConfiguration) {
        return viewConfiguration.getScaledPagingTouchSlop();
    }
    
    public static float getScaledVerticalScrollFactor(final ViewConfiguration viewConfiguration, final Context context) {
        if (Build$VERSION.SDK_INT >= 26) {
            return Api26Impl.getScaledVerticalScrollFactor(viewConfiguration);
        }
        return getLegacyScrollFactor(viewConfiguration, context);
    }
    
    @Deprecated
    public static boolean hasPermanentMenuKey(final ViewConfiguration viewConfiguration) {
        return viewConfiguration.hasPermanentMenuKey();
    }
    
    private static boolean isInputDeviceInfoValid(final int n, final int n2, final int n3) {
        final InputDevice device = InputDevice.getDevice(n);
        return device != null && device.getMotionRange(n2, n3) != null;
    }
    
    public static boolean shouldShowMenuShortcutsWhenKeyboardPresent(final ViewConfiguration viewConfiguration, final Context context) {
        if (Build$VERSION.SDK_INT >= 28) {
            return Api28Impl.shouldShowMenuShortcutsWhenKeyboardPresent(viewConfiguration);
        }
        final Resources resources = context.getResources();
        final int platformResId = getPlatformResId(resources, "config_showMenuShortcutsWhenKeyboardPresent", "bool");
        return platformResId != 0 && resources.getBoolean(platformResId);
    }
    
    static class Api26Impl
    {
        private Api26Impl() {
        }
        
        static float getScaledHorizontalScrollFactor(final ViewConfiguration viewConfiguration) {
            return viewConfiguration.getScaledHorizontalScrollFactor();
        }
        
        static float getScaledVerticalScrollFactor(final ViewConfiguration viewConfiguration) {
            return viewConfiguration.getScaledVerticalScrollFactor();
        }
    }
    
    static class Api28Impl
    {
        private Api28Impl() {
        }
        
        static int getScaledHoverSlop(final ViewConfiguration viewConfiguration) {
            return viewConfiguration.getScaledHoverSlop();
        }
        
        static boolean shouldShowMenuShortcutsWhenKeyboardPresent(final ViewConfiguration viewConfiguration) {
            return viewConfiguration.shouldShowMenuShortcutsWhenKeyboardPresent();
        }
    }
    
    static class Api34Impl
    {
        private Api34Impl() {
        }
        
        static int getScaledMaximumFlingVelocity(final ViewConfiguration viewConfiguration, final int n, final int n2, final int n3) {
            return viewConfiguration.getScaledMaximumFlingVelocity(n, n2, n3);
        }
        
        static int getScaledMinimumFlingVelocity(final ViewConfiguration viewConfiguration, final int n, final int n2, final int n3) {
            return viewConfiguration.getScaledMinimumFlingVelocity(n, n2, n3);
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.view;

public interface WindowInsetsAnimationControlListenerCompat
{
    void onCancelled(final WindowInsetsAnimationControllerCompat p0);
    
    void onFinished(final WindowInsetsAnimationControllerCompat p0);
    
    void onReady(final WindowInsetsAnimationControllerCompat p0, final int p1);
}

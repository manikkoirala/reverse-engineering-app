// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.view;

import java.util.ArrayList;
import java.util.HashMap;
import android.view.WindowInsetsAnimation$Callback;
import android.animation.Animator$AnimatorListener;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import java.util.Collections;
import android.animation.ValueAnimator$AnimatorUpdateListener;
import android.animation.ValueAnimator;
import java.util.Objects;
import androidx.core.R;
import android.view.ViewGroup;
import android.view.View$OnApplyWindowInsetsListener;
import android.view.animation.DecelerateInterpolator;
import androidx.interpolator.view.animation.FastOutLinearInInterpolator;
import android.view.animation.PathInterpolator;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;
import java.util.List;
import android.view.WindowInsets;
import android.view.WindowInsetsAnimation$Bounds;
import androidx.core.graphics.Insets;
import android.view.View;
import android.view.WindowInsetsAnimation;
import android.os.Build$VERSION;
import android.view.animation.Interpolator;

public final class WindowInsetsAnimationCompat
{
    private static final boolean DEBUG = false;
    private static final String TAG = "WindowInsetsAnimCompat";
    private Impl mImpl;
    
    public WindowInsetsAnimationCompat(final int n, final Interpolator interpolator, final long n2) {
        if (Build$VERSION.SDK_INT >= 30) {
            this.mImpl = (Impl)new Impl30(n, interpolator, n2);
        }
        else if (Build$VERSION.SDK_INT >= 21) {
            this.mImpl = (Impl)new Impl21(n, interpolator, n2);
        }
        else {
            this.mImpl = new Impl(0, interpolator, n2);
        }
    }
    
    private WindowInsetsAnimationCompat(final WindowInsetsAnimation windowInsetsAnimation) {
        this(0, null, 0L);
        if (Build$VERSION.SDK_INT >= 30) {
            this.mImpl = (Impl)new Impl30(windowInsetsAnimation);
        }
    }
    
    static void setCallback(final View view, final Callback callback) {
        if (Build$VERSION.SDK_INT >= 30) {
            Impl30.setCallback(view, callback);
        }
        else if (Build$VERSION.SDK_INT >= 21) {
            Impl21.setCallback(view, callback);
        }
    }
    
    static WindowInsetsAnimationCompat toWindowInsetsAnimationCompat(final WindowInsetsAnimation windowInsetsAnimation) {
        return new WindowInsetsAnimationCompat(windowInsetsAnimation);
    }
    
    public float getAlpha() {
        return this.mImpl.getAlpha();
    }
    
    public long getDurationMillis() {
        return this.mImpl.getDurationMillis();
    }
    
    public float getFraction() {
        return this.mImpl.getFraction();
    }
    
    public float getInterpolatedFraction() {
        return this.mImpl.getInterpolatedFraction();
    }
    
    public Interpolator getInterpolator() {
        return this.mImpl.getInterpolator();
    }
    
    public int getTypeMask() {
        return this.mImpl.getTypeMask();
    }
    
    public void setAlpha(final float alpha) {
        this.mImpl.setAlpha(alpha);
    }
    
    public void setFraction(final float fraction) {
        this.mImpl.setFraction(fraction);
    }
    
    public static final class BoundsCompat
    {
        private final Insets mLowerBound;
        private final Insets mUpperBound;
        
        private BoundsCompat(final WindowInsetsAnimation$Bounds windowInsetsAnimation$Bounds) {
            this.mLowerBound = Impl30.getLowerBounds(windowInsetsAnimation$Bounds);
            this.mUpperBound = Impl30.getHigherBounds(windowInsetsAnimation$Bounds);
        }
        
        public BoundsCompat(final Insets mLowerBound, final Insets mUpperBound) {
            this.mLowerBound = mLowerBound;
            this.mUpperBound = mUpperBound;
        }
        
        public static BoundsCompat toBoundsCompat(final WindowInsetsAnimation$Bounds windowInsetsAnimation$Bounds) {
            return new BoundsCompat(windowInsetsAnimation$Bounds);
        }
        
        public Insets getLowerBound() {
            return this.mLowerBound;
        }
        
        public Insets getUpperBound() {
            return this.mUpperBound;
        }
        
        public BoundsCompat inset(final Insets insets) {
            return new BoundsCompat(WindowInsetsCompat.insetInsets(this.mLowerBound, insets.left, insets.top, insets.right, insets.bottom), WindowInsetsCompat.insetInsets(this.mUpperBound, insets.left, insets.top, insets.right, insets.bottom));
        }
        
        public WindowInsetsAnimation$Bounds toBounds() {
            return Impl30.createPlatformBounds(this);
        }
        
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder();
            sb.append("Bounds{lower=");
            sb.append(this.mLowerBound);
            sb.append(" upper=");
            sb.append(this.mUpperBound);
            sb.append("}");
            return sb.toString();
        }
    }
    
    public abstract static class Callback
    {
        public static final int DISPATCH_MODE_CONTINUE_ON_SUBTREE = 1;
        public static final int DISPATCH_MODE_STOP = 0;
        WindowInsets mDispachedInsets;
        private final int mDispatchMode;
        
        public Callback(final int mDispatchMode) {
            this.mDispatchMode = mDispatchMode;
        }
        
        public final int getDispatchMode() {
            return this.mDispatchMode;
        }
        
        public void onEnd(final WindowInsetsAnimationCompat windowInsetsAnimationCompat) {
        }
        
        public void onPrepare(final WindowInsetsAnimationCompat windowInsetsAnimationCompat) {
        }
        
        public abstract WindowInsetsCompat onProgress(final WindowInsetsCompat p0, final List<WindowInsetsAnimationCompat> p1);
        
        public BoundsCompat onStart(final WindowInsetsAnimationCompat windowInsetsAnimationCompat, final BoundsCompat boundsCompat) {
            return boundsCompat;
        }
        
        @Retention(RetentionPolicy.SOURCE)
        public @interface DispatchMode {
        }
    }
    
    private static class Impl
    {
        private float mAlpha;
        private final long mDurationMillis;
        private float mFraction;
        private final Interpolator mInterpolator;
        private final int mTypeMask;
        
        Impl(final int mTypeMask, final Interpolator mInterpolator, final long mDurationMillis) {
            this.mTypeMask = mTypeMask;
            this.mInterpolator = mInterpolator;
            this.mDurationMillis = mDurationMillis;
        }
        
        public float getAlpha() {
            return this.mAlpha;
        }
        
        public long getDurationMillis() {
            return this.mDurationMillis;
        }
        
        public float getFraction() {
            return this.mFraction;
        }
        
        public float getInterpolatedFraction() {
            final Interpolator mInterpolator = this.mInterpolator;
            if (mInterpolator != null) {
                return mInterpolator.getInterpolation(this.mFraction);
            }
            return this.mFraction;
        }
        
        public Interpolator getInterpolator() {
            return this.mInterpolator;
        }
        
        public int getTypeMask() {
            return this.mTypeMask;
        }
        
        public void setAlpha(final float mAlpha) {
            this.mAlpha = mAlpha;
        }
        
        public void setFraction(final float mFraction) {
            this.mFraction = mFraction;
        }
    }
    
    private static class Impl21 extends Impl
    {
        private static final Interpolator DEFAULT_INSET_INTERPOLATOR;
        private static final Interpolator HIDE_IME_INTERPOLATOR;
        private static final Interpolator SHOW_IME_INTERPOLATOR;
        
        static {
            SHOW_IME_INTERPOLATOR = (Interpolator)new PathInterpolator(0.0f, 1.1f, 0.0f, 1.0f);
            HIDE_IME_INTERPOLATOR = (Interpolator)new FastOutLinearInInterpolator();
            DEFAULT_INSET_INTERPOLATOR = (Interpolator)new DecelerateInterpolator();
        }
        
        Impl21(final int n, final Interpolator interpolator, final long n2) {
            super(n, interpolator, n2);
        }
        
        static int buildAnimationMask(final WindowInsetsCompat windowInsetsCompat, final WindowInsetsCompat windowInsetsCompat2) {
            int i = 1;
            int n = 0;
            while (i <= 256) {
                int n2 = n;
                if (!windowInsetsCompat.getInsets(i).equals(windowInsetsCompat2.getInsets(i))) {
                    n2 = (n | i);
                }
                i <<= 1;
                n = n2;
            }
            return n;
        }
        
        static BoundsCompat computeAnimationBounds(final WindowInsetsCompat windowInsetsCompat, final WindowInsetsCompat windowInsetsCompat2, final int n) {
            final Insets insets = windowInsetsCompat.getInsets(n);
            final Insets insets2 = windowInsetsCompat2.getInsets(n);
            return new BoundsCompat(Insets.of(Math.min(insets.left, insets2.left), Math.min(insets.top, insets2.top), Math.min(insets.right, insets2.right), Math.min(insets.bottom, insets2.bottom)), Insets.of(Math.max(insets.left, insets2.left), Math.max(insets.top, insets2.top), Math.max(insets.right, insets2.right), Math.max(insets.bottom, insets2.bottom)));
        }
        
        static Interpolator createInsetInterpolator(final int n, final WindowInsetsCompat windowInsetsCompat, final WindowInsetsCompat windowInsetsCompat2) {
            if ((n & 0x8) == 0x0) {
                return Impl21.DEFAULT_INSET_INTERPOLATOR;
            }
            if (windowInsetsCompat.getInsets(WindowInsetsCompat.Type.ime()).bottom > windowInsetsCompat2.getInsets(WindowInsetsCompat.Type.ime()).bottom) {
                return Impl21.SHOW_IME_INTERPOLATOR;
            }
            return Impl21.HIDE_IME_INTERPOLATOR;
        }
        
        private static View$OnApplyWindowInsetsListener createProxyListener(final View view, final Callback callback) {
            return (View$OnApplyWindowInsetsListener)new Impl21OnApplyWindowInsetsListener(view, callback);
        }
        
        static void dispatchOnEnd(final View view, final WindowInsetsAnimationCompat windowInsetsAnimationCompat) {
            final Callback callback = getCallback(view);
            if (callback != null) {
                callback.onEnd(windowInsetsAnimationCompat);
                if (callback.getDispatchMode() == 0) {
                    return;
                }
            }
            if (view instanceof ViewGroup) {
                final ViewGroup viewGroup = (ViewGroup)view;
                for (int i = 0; i < viewGroup.getChildCount(); ++i) {
                    dispatchOnEnd(viewGroup.getChildAt(i), windowInsetsAnimationCompat);
                }
            }
        }
        
        static void dispatchOnPrepare(final View view, final WindowInsetsAnimationCompat windowInsetsAnimationCompat, final WindowInsets mDispachedInsets, final boolean b) {
            final Callback callback = getCallback(view);
            int i = 0;
            boolean b2 = b;
            if (callback != null) {
                callback.mDispachedInsets = mDispachedInsets;
                if (!(b2 = b)) {
                    callback.onPrepare(windowInsetsAnimationCompat);
                    b2 = (callback.getDispatchMode() == 0);
                }
            }
            if (view instanceof ViewGroup) {
                for (ViewGroup viewGroup = (ViewGroup)view; i < viewGroup.getChildCount(); ++i) {
                    dispatchOnPrepare(viewGroup.getChildAt(i), windowInsetsAnimationCompat, mDispachedInsets, b2);
                }
            }
        }
        
        static void dispatchOnProgress(final View view, final WindowInsetsCompat windowInsetsCompat, final List<WindowInsetsAnimationCompat> list) {
            final Callback callback = getCallback(view);
            WindowInsetsCompat onProgress = windowInsetsCompat;
            if (callback != null) {
                onProgress = callback.onProgress(windowInsetsCompat, list);
                if (callback.getDispatchMode() == 0) {
                    return;
                }
            }
            if (view instanceof ViewGroup) {
                final ViewGroup viewGroup = (ViewGroup)view;
                for (int i = 0; i < viewGroup.getChildCount(); ++i) {
                    dispatchOnProgress(viewGroup.getChildAt(i), onProgress, list);
                }
            }
        }
        
        static void dispatchOnStart(final View view, final WindowInsetsAnimationCompat windowInsetsAnimationCompat, final BoundsCompat boundsCompat) {
            final Callback callback = getCallback(view);
            if (callback != null) {
                callback.onStart(windowInsetsAnimationCompat, boundsCompat);
                if (callback.getDispatchMode() == 0) {
                    return;
                }
            }
            if (view instanceof ViewGroup) {
                final ViewGroup viewGroup = (ViewGroup)view;
                for (int i = 0; i < viewGroup.getChildCount(); ++i) {
                    dispatchOnStart(viewGroup.getChildAt(i), windowInsetsAnimationCompat, boundsCompat);
                }
            }
        }
        
        static WindowInsets forwardToViewIfNeeded(final View view, final WindowInsets windowInsets) {
            if (view.getTag(R.id.tag_on_apply_window_listener) != null) {
                return windowInsets;
            }
            return view.onApplyWindowInsets(windowInsets);
        }
        
        static Callback getCallback(final View view) {
            final Object tag = view.getTag(R.id.tag_window_insets_animation_callback);
            Callback mCallback;
            if (tag instanceof Impl21OnApplyWindowInsetsListener) {
                mCallback = ((Impl21OnApplyWindowInsetsListener)tag).mCallback;
            }
            else {
                mCallback = null;
            }
            return mCallback;
        }
        
        static WindowInsetsCompat interpolateInsets(final WindowInsetsCompat windowInsetsCompat, final WindowInsetsCompat windowInsetsCompat2, final float n, final int n2) {
            final WindowInsetsCompat.Builder builder = new WindowInsetsCompat.Builder(windowInsetsCompat);
            for (int i = 1; i <= 256; i <<= 1) {
                if ((n2 & i) == 0x0) {
                    builder.setInsets(i, windowInsetsCompat.getInsets(i));
                }
                else {
                    final Insets insets = windowInsetsCompat.getInsets(i);
                    final Insets insets2 = windowInsetsCompat2.getInsets(i);
                    final float n3 = (float)(insets.left - insets2.left);
                    final float n4 = 1.0f - n;
                    builder.setInsets(i, WindowInsetsCompat.insetInsets(insets, (int)(n3 * n4 + 0.5), (int)((insets.top - insets2.top) * n4 + 0.5), (int)((insets.right - insets2.right) * n4 + 0.5), (int)((insets.bottom - insets2.bottom) * n4 + 0.5)));
                }
            }
            return builder.build();
        }
        
        static void setCallback(final View view, final Callback callback) {
            final Object tag = view.getTag(R.id.tag_on_apply_window_listener);
            if (callback == null) {
                view.setTag(R.id.tag_window_insets_animation_callback, (Object)null);
                if (tag == null) {
                    view.setOnApplyWindowInsetsListener((View$OnApplyWindowInsetsListener)null);
                }
            }
            else {
                final View$OnApplyWindowInsetsListener proxyListener = createProxyListener(view, callback);
                view.setTag(R.id.tag_window_insets_animation_callback, (Object)proxyListener);
                if (tag == null) {
                    view.setOnApplyWindowInsetsListener(proxyListener);
                }
            }
        }
        
        private static class Impl21OnApplyWindowInsetsListener implements View$OnApplyWindowInsetsListener
        {
            private static final int COMPAT_ANIMATION_DURATION = 160;
            final Callback mCallback;
            private WindowInsetsCompat mLastInsets;
            
            Impl21OnApplyWindowInsetsListener(final View view, final Callback mCallback) {
                this.mCallback = mCallback;
                final WindowInsetsCompat rootWindowInsets = ViewCompat.getRootWindowInsets(view);
                WindowInsetsCompat build;
                if (rootWindowInsets != null) {
                    build = new WindowInsetsCompat.Builder(rootWindowInsets).build();
                }
                else {
                    build = null;
                }
                this.mLastInsets = build;
            }
            
            public WindowInsets onApplyWindowInsets(final View view, final WindowInsets b) {
                if (!view.isLaidOut()) {
                    this.mLastInsets = WindowInsetsCompat.toWindowInsetsCompat(b, view);
                    return Impl21.forwardToViewIfNeeded(view, b);
                }
                final WindowInsetsCompat windowInsetsCompat = WindowInsetsCompat.toWindowInsetsCompat(b, view);
                if (this.mLastInsets == null) {
                    this.mLastInsets = ViewCompat.getRootWindowInsets(view);
                }
                if (this.mLastInsets == null) {
                    this.mLastInsets = windowInsetsCompat;
                    return Impl21.forwardToViewIfNeeded(view, b);
                }
                final Callback callback = Impl21.getCallback(view);
                if (callback != null && Objects.equals(callback.mDispachedInsets, b)) {
                    return Impl21.forwardToViewIfNeeded(view, b);
                }
                final int buildAnimationMask = Impl21.buildAnimationMask(windowInsetsCompat, this.mLastInsets);
                if (buildAnimationMask == 0) {
                    return Impl21.forwardToViewIfNeeded(view, b);
                }
                final WindowInsetsCompat mLastInsets = this.mLastInsets;
                final WindowInsetsAnimationCompat windowInsetsAnimationCompat = new WindowInsetsAnimationCompat(buildAnimationMask, Impl21.createInsetInterpolator(buildAnimationMask, windowInsetsCompat, mLastInsets), 160L);
                windowInsetsAnimationCompat.setFraction(0.0f);
                final ValueAnimator setDuration = ValueAnimator.ofFloat(new float[] { 0.0f, 1.0f }).setDuration(windowInsetsAnimationCompat.getDurationMillis());
                final BoundsCompat computeAnimationBounds = Impl21.computeAnimationBounds(windowInsetsCompat, mLastInsets, buildAnimationMask);
                Impl21.dispatchOnPrepare(view, windowInsetsAnimationCompat, b, false);
                setDuration.addUpdateListener((ValueAnimator$AnimatorUpdateListener)new ValueAnimator$AnimatorUpdateListener(this, windowInsetsAnimationCompat, windowInsetsCompat, mLastInsets, buildAnimationMask, view) {
                    final Impl21OnApplyWindowInsetsListener this$0;
                    final WindowInsetsAnimationCompat val$anim;
                    final int val$animationMask;
                    final WindowInsetsCompat val$startingInsets;
                    final WindowInsetsCompat val$targetInsets;
                    final View val$v;
                    
                    public void onAnimationUpdate(final ValueAnimator valueAnimator) {
                        this.val$anim.setFraction(valueAnimator.getAnimatedFraction());
                        Impl21.dispatchOnProgress(this.val$v, Impl21.interpolateInsets(this.val$targetInsets, this.val$startingInsets, this.val$anim.getInterpolatedFraction(), this.val$animationMask), Collections.singletonList(this.val$anim));
                    }
                });
                setDuration.addListener((Animator$AnimatorListener)new AnimatorListenerAdapter(this, windowInsetsAnimationCompat, view) {
                    final Impl21OnApplyWindowInsetsListener this$0;
                    final WindowInsetsAnimationCompat val$anim;
                    final View val$v;
                    
                    public void onAnimationEnd(final Animator animator) {
                        this.val$anim.setFraction(1.0f);
                        Impl21.dispatchOnEnd(this.val$v, this.val$anim);
                    }
                });
                OneShotPreDrawListener.add(view, new Runnable(this, view, windowInsetsAnimationCompat, computeAnimationBounds, setDuration) {
                    final Impl21OnApplyWindowInsetsListener this$0;
                    final WindowInsetsAnimationCompat val$anim;
                    final BoundsCompat val$animationBounds;
                    final ValueAnimator val$animator;
                    final View val$v;
                    
                    @Override
                    public void run() {
                        Impl21.dispatchOnStart(this.val$v, this.val$anim, this.val$animationBounds);
                        this.val$animator.start();
                    }
                });
                this.mLastInsets = windowInsetsCompat;
                return Impl21.forwardToViewIfNeeded(view, b);
            }
        }
    }
    
    private static class Impl30 extends Impl
    {
        private final WindowInsetsAnimation mWrapped;
        
        Impl30(final int n, final Interpolator interpolator, final long n2) {
            this(new WindowInsetsAnimation(n, interpolator, n2));
        }
        
        Impl30(final WindowInsetsAnimation mWrapped) {
            super(0, null, 0L);
            this.mWrapped = mWrapped;
        }
        
        public static WindowInsetsAnimation$Bounds createPlatformBounds(final BoundsCompat boundsCompat) {
            return new WindowInsetsAnimation$Bounds(boundsCompat.getLowerBound().toPlatformInsets(), boundsCompat.getUpperBound().toPlatformInsets());
        }
        
        public static Insets getHigherBounds(final WindowInsetsAnimation$Bounds windowInsetsAnimation$Bounds) {
            return Insets.toCompatInsets(windowInsetsAnimation$Bounds.getUpperBound());
        }
        
        public static Insets getLowerBounds(final WindowInsetsAnimation$Bounds windowInsetsAnimation$Bounds) {
            return Insets.toCompatInsets(windowInsetsAnimation$Bounds.getLowerBound());
        }
        
        public static void setCallback(final View view, final Callback callback) {
            ProxyCallback windowInsetsAnimationCallback;
            if (callback != null) {
                windowInsetsAnimationCallback = new ProxyCallback(callback);
            }
            else {
                windowInsetsAnimationCallback = null;
            }
            view.setWindowInsetsAnimationCallback((WindowInsetsAnimation$Callback)windowInsetsAnimationCallback);
        }
        
        @Override
        public long getDurationMillis() {
            return this.mWrapped.getDurationMillis();
        }
        
        @Override
        public float getFraction() {
            return this.mWrapped.getFraction();
        }
        
        @Override
        public float getInterpolatedFraction() {
            return this.mWrapped.getInterpolatedFraction();
        }
        
        @Override
        public Interpolator getInterpolator() {
            return this.mWrapped.getInterpolator();
        }
        
        @Override
        public int getTypeMask() {
            return this.mWrapped.getTypeMask();
        }
        
        @Override
        public void setFraction(final float fraction) {
            this.mWrapped.setFraction(fraction);
        }
        
        private static class ProxyCallback extends WindowInsetsAnimation$Callback
        {
            private final HashMap<WindowInsetsAnimation, WindowInsetsAnimationCompat> mAnimations;
            private final Callback mCompat;
            private List<WindowInsetsAnimationCompat> mRORunningAnimations;
            private ArrayList<WindowInsetsAnimationCompat> mTmpRunningAnimations;
            
            ProxyCallback(final Callback mCompat) {
                super(mCompat.getDispatchMode());
                this.mAnimations = new HashMap<WindowInsetsAnimation, WindowInsetsAnimationCompat>();
                this.mCompat = mCompat;
            }
            
            private WindowInsetsAnimationCompat getWindowInsetsAnimationCompat(final WindowInsetsAnimation windowInsetsAnimation) {
                WindowInsetsAnimationCompat windowInsetsAnimationCompat;
                if ((windowInsetsAnimationCompat = this.mAnimations.get(windowInsetsAnimation)) == null) {
                    windowInsetsAnimationCompat = WindowInsetsAnimationCompat.toWindowInsetsAnimationCompat(windowInsetsAnimation);
                    this.mAnimations.put(windowInsetsAnimation, windowInsetsAnimationCompat);
                }
                return windowInsetsAnimationCompat;
            }
            
            public void onEnd(final WindowInsetsAnimation key) {
                this.mCompat.onEnd(this.getWindowInsetsAnimationCompat(key));
                this.mAnimations.remove(key);
            }
            
            public void onPrepare(final WindowInsetsAnimation windowInsetsAnimation) {
                this.mCompat.onPrepare(this.getWindowInsetsAnimationCompat(windowInsetsAnimation));
            }
            
            public WindowInsets onProgress(final WindowInsets windowInsets, final List<WindowInsetsAnimation> list) {
                final ArrayList<WindowInsetsAnimationCompat> mTmpRunningAnimations = this.mTmpRunningAnimations;
                if (mTmpRunningAnimations == null) {
                    final ArrayList list2 = new ArrayList(list.size());
                    this.mTmpRunningAnimations = list2;
                    this.mRORunningAnimations = (List<WindowInsetsAnimationCompat>)Collections.unmodifiableList((List<?>)list2);
                }
                else {
                    mTmpRunningAnimations.clear();
                }
                for (int i = list.size() - 1; i >= 0; --i) {
                    final WindowInsetsAnimation windowInsetsAnimation = list.get(i);
                    final WindowInsetsAnimationCompat windowInsetsAnimationCompat = this.getWindowInsetsAnimationCompat(windowInsetsAnimation);
                    windowInsetsAnimationCompat.setFraction(windowInsetsAnimation.getFraction());
                    this.mTmpRunningAnimations.add(windowInsetsAnimationCompat);
                }
                return this.mCompat.onProgress(WindowInsetsCompat.toWindowInsetsCompat(windowInsets), this.mRORunningAnimations).toWindowInsets();
            }
            
            public WindowInsetsAnimation$Bounds onStart(final WindowInsetsAnimation windowInsetsAnimation, final WindowInsetsAnimation$Bounds windowInsetsAnimation$Bounds) {
                return this.mCompat.onStart(this.getWindowInsetsAnimationCompat(windowInsetsAnimation), BoundsCompat.toBoundsCompat(windowInsetsAnimation$Bounds)).toBounds();
            }
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.view;

import android.view.WindowInsets$Type;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;
import java.util.Objects;
import java.lang.reflect.Method;
import android.view.DisplayCutout;
import android.view.WindowInsets$Builder;
import java.lang.reflect.Constructor;
import android.util.Log;
import java.lang.reflect.Field;
import android.graphics.Rect;
import androidx.core.util.ObjectsCompat;
import androidx.core.util.Preconditions;
import android.view.View;
import androidx.core.graphics.Insets;
import android.view.WindowInsets;
import android.os.Build$VERSION;

public class WindowInsetsCompat
{
    public static final WindowInsetsCompat CONSUMED;
    private static final String TAG = "WindowInsetsCompat";
    private final Impl mImpl;
    
    static {
        if (Build$VERSION.SDK_INT >= 30) {
            CONSUMED = Impl30.CONSUMED;
        }
        else {
            CONSUMED = Impl.CONSUMED;
        }
    }
    
    private WindowInsetsCompat(final WindowInsets windowInsets) {
        if (Build$VERSION.SDK_INT >= 30) {
            this.mImpl = (Impl)new Impl30(this, windowInsets);
        }
        else if (Build$VERSION.SDK_INT >= 29) {
            this.mImpl = (Impl)new Impl29(this, windowInsets);
        }
        else if (Build$VERSION.SDK_INT >= 28) {
            this.mImpl = (Impl)new Impl28(this, windowInsets);
        }
        else if (Build$VERSION.SDK_INT >= 21) {
            this.mImpl = (Impl)new Impl21(this, windowInsets);
        }
        else if (Build$VERSION.SDK_INT >= 20) {
            this.mImpl = (Impl)new Impl20(this, windowInsets);
        }
        else {
            this.mImpl = new Impl(this);
        }
    }
    
    public WindowInsetsCompat(final WindowInsetsCompat windowInsetsCompat) {
        if (windowInsetsCompat != null) {
            final Impl mImpl = windowInsetsCompat.mImpl;
            if (Build$VERSION.SDK_INT >= 30 && mImpl instanceof Impl30) {
                this.mImpl = (Impl)new Impl30(this, (Impl30)mImpl);
            }
            else if (Build$VERSION.SDK_INT >= 29 && mImpl instanceof Impl29) {
                this.mImpl = (Impl)new Impl29(this, (Impl29)mImpl);
            }
            else if (Build$VERSION.SDK_INT >= 28 && mImpl instanceof Impl28) {
                this.mImpl = (Impl)new Impl28(this, (Impl28)mImpl);
            }
            else if (Build$VERSION.SDK_INT >= 21 && mImpl instanceof Impl21) {
                this.mImpl = (Impl)new Impl21(this, (Impl21)mImpl);
            }
            else if (Build$VERSION.SDK_INT >= 20 && mImpl instanceof Impl20) {
                this.mImpl = (Impl)new Impl20(this, (Impl20)mImpl);
            }
            else {
                this.mImpl = new Impl(this);
            }
            mImpl.copyWindowDataInto(this);
        }
        else {
            this.mImpl = new Impl(this);
        }
    }
    
    static Insets insetInsets(final Insets insets, final int n, final int n2, final int n3, final int n4) {
        final int max = Math.max(0, insets.left - n);
        final int max2 = Math.max(0, insets.top - n2);
        final int max3 = Math.max(0, insets.right - n3);
        final int max4 = Math.max(0, insets.bottom - n4);
        if (max == n && max2 == n2 && max3 == n3 && max4 == n4) {
            return insets;
        }
        return Insets.of(max, max2, max3, max4);
    }
    
    public static WindowInsetsCompat toWindowInsetsCompat(final WindowInsets windowInsets) {
        return toWindowInsetsCompat(windowInsets, null);
    }
    
    public static WindowInsetsCompat toWindowInsetsCompat(final WindowInsets windowInsets, final View view) {
        final WindowInsetsCompat windowInsetsCompat = new WindowInsetsCompat(Preconditions.checkNotNull(windowInsets));
        if (view != null && ViewCompat.isAttachedToWindow(view)) {
            windowInsetsCompat.setRootWindowInsets(ViewCompat.getRootWindowInsets(view));
            windowInsetsCompat.copyRootViewBounds(view.getRootView());
        }
        return windowInsetsCompat;
    }
    
    @Deprecated
    public WindowInsetsCompat consumeDisplayCutout() {
        return this.mImpl.consumeDisplayCutout();
    }
    
    @Deprecated
    public WindowInsetsCompat consumeStableInsets() {
        return this.mImpl.consumeStableInsets();
    }
    
    @Deprecated
    public WindowInsetsCompat consumeSystemWindowInsets() {
        return this.mImpl.consumeSystemWindowInsets();
    }
    
    void copyRootViewBounds(final View view) {
        this.mImpl.copyRootViewBounds(view);
    }
    
    @Override
    public boolean equals(final Object o) {
        return this == o || (o instanceof WindowInsetsCompat && ObjectsCompat.equals(this.mImpl, ((WindowInsetsCompat)o).mImpl));
    }
    
    public DisplayCutoutCompat getDisplayCutout() {
        return this.mImpl.getDisplayCutout();
    }
    
    public Insets getInsets(final int n) {
        return this.mImpl.getInsets(n);
    }
    
    public Insets getInsetsIgnoringVisibility(final int n) {
        return this.mImpl.getInsetsIgnoringVisibility(n);
    }
    
    @Deprecated
    public Insets getMandatorySystemGestureInsets() {
        return this.mImpl.getMandatorySystemGestureInsets();
    }
    
    @Deprecated
    public int getStableInsetBottom() {
        return this.mImpl.getStableInsets().bottom;
    }
    
    @Deprecated
    public int getStableInsetLeft() {
        return this.mImpl.getStableInsets().left;
    }
    
    @Deprecated
    public int getStableInsetRight() {
        return this.mImpl.getStableInsets().right;
    }
    
    @Deprecated
    public int getStableInsetTop() {
        return this.mImpl.getStableInsets().top;
    }
    
    @Deprecated
    public Insets getStableInsets() {
        return this.mImpl.getStableInsets();
    }
    
    @Deprecated
    public Insets getSystemGestureInsets() {
        return this.mImpl.getSystemGestureInsets();
    }
    
    @Deprecated
    public int getSystemWindowInsetBottom() {
        return this.mImpl.getSystemWindowInsets().bottom;
    }
    
    @Deprecated
    public int getSystemWindowInsetLeft() {
        return this.mImpl.getSystemWindowInsets().left;
    }
    
    @Deprecated
    public int getSystemWindowInsetRight() {
        return this.mImpl.getSystemWindowInsets().right;
    }
    
    @Deprecated
    public int getSystemWindowInsetTop() {
        return this.mImpl.getSystemWindowInsets().top;
    }
    
    @Deprecated
    public Insets getSystemWindowInsets() {
        return this.mImpl.getSystemWindowInsets();
    }
    
    @Deprecated
    public Insets getTappableElementInsets() {
        return this.mImpl.getTappableElementInsets();
    }
    
    public boolean hasInsets() {
        return !this.getInsets(Type.all()).equals(Insets.NONE) || !this.getInsetsIgnoringVisibility(Type.all() ^ Type.ime()).equals(Insets.NONE) || this.getDisplayCutout() != null;
    }
    
    @Deprecated
    public boolean hasStableInsets() {
        return this.mImpl.getStableInsets().equals(Insets.NONE) ^ true;
    }
    
    @Deprecated
    public boolean hasSystemWindowInsets() {
        return this.mImpl.getSystemWindowInsets().equals(Insets.NONE) ^ true;
    }
    
    @Override
    public int hashCode() {
        final Impl mImpl = this.mImpl;
        int hashCode;
        if (mImpl == null) {
            hashCode = 0;
        }
        else {
            hashCode = mImpl.hashCode();
        }
        return hashCode;
    }
    
    public WindowInsetsCompat inset(final int n, final int n2, final int n3, final int n4) {
        return this.mImpl.inset(n, n2, n3, n4);
    }
    
    public WindowInsetsCompat inset(final Insets insets) {
        return this.inset(insets.left, insets.top, insets.right, insets.bottom);
    }
    
    public boolean isConsumed() {
        return this.mImpl.isConsumed();
    }
    
    public boolean isRound() {
        return this.mImpl.isRound();
    }
    
    public boolean isVisible(final int n) {
        return this.mImpl.isVisible(n);
    }
    
    @Deprecated
    public WindowInsetsCompat replaceSystemWindowInsets(final int n, final int n2, final int n3, final int n4) {
        return new Builder(this).setSystemWindowInsets(Insets.of(n, n2, n3, n4)).build();
    }
    
    @Deprecated
    public WindowInsetsCompat replaceSystemWindowInsets(final Rect rect) {
        return new Builder(this).setSystemWindowInsets(Insets.of(rect)).build();
    }
    
    void setOverriddenInsets(final Insets[] overriddenInsets) {
        this.mImpl.setOverriddenInsets(overriddenInsets);
    }
    
    void setRootViewData(final Insets rootViewData) {
        this.mImpl.setRootViewData(rootViewData);
    }
    
    void setRootWindowInsets(final WindowInsetsCompat rootWindowInsets) {
        this.mImpl.setRootWindowInsets(rootWindowInsets);
    }
    
    void setStableInsets(final Insets stableInsets) {
        this.mImpl.setStableInsets(stableInsets);
    }
    
    public WindowInsets toWindowInsets() {
        final Impl mImpl = this.mImpl;
        WindowInsets mPlatformInsets;
        if (mImpl instanceof Impl20) {
            mPlatformInsets = ((Impl20)mImpl).mPlatformInsets;
        }
        else {
            mPlatformInsets = null;
        }
        return mPlatformInsets;
    }
    
    static class Api21ReflectionHolder
    {
        private static Field sContentInsets;
        private static boolean sReflectionSucceeded;
        private static Field sStableInsets;
        private static Field sViewAttachInfoField;
        
        static {
            try {
                (Api21ReflectionHolder.sViewAttachInfoField = View.class.getDeclaredField("mAttachInfo")).setAccessible(true);
                final Class<?> forName = Class.forName("android.view.View$AttachInfo");
                (Api21ReflectionHolder.sStableInsets = forName.getDeclaredField("mStableInsets")).setAccessible(true);
                (Api21ReflectionHolder.sContentInsets = forName.getDeclaredField("mContentInsets")).setAccessible(true);
                Api21ReflectionHolder.sReflectionSucceeded = true;
            }
            catch (final ReflectiveOperationException ex) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Failed to get visible insets from AttachInfo ");
                sb.append(ex.getMessage());
                Log.w("WindowInsetsCompat", sb.toString(), (Throwable)ex);
            }
        }
        
        private Api21ReflectionHolder() {
        }
        
        public static WindowInsetsCompat getRootWindowInsets(final View view) {
            if (Api21ReflectionHolder.sReflectionSucceeded) {
                if (view.isAttachedToWindow()) {
                    final View rootView = view.getRootView();
                    try {
                        final Object value = Api21ReflectionHolder.sViewAttachInfoField.get(rootView);
                        if (value != null) {
                            final Rect rect = (Rect)Api21ReflectionHolder.sStableInsets.get(value);
                            final Rect rect2 = (Rect)Api21ReflectionHolder.sContentInsets.get(value);
                            if (rect != null && rect2 != null) {
                                final WindowInsetsCompat build = new Builder().setStableInsets(Insets.of(rect)).setSystemWindowInsets(Insets.of(rect2)).build();
                                build.setRootWindowInsets(build);
                                build.copyRootViewBounds(view.getRootView());
                                return build;
                            }
                        }
                    }
                    catch (final IllegalAccessException ex) {
                        final StringBuilder sb = new StringBuilder();
                        sb.append("Failed to get insets from AttachInfo. ");
                        sb.append(ex.getMessage());
                        Log.w("WindowInsetsCompat", sb.toString(), (Throwable)ex);
                    }
                }
            }
            return null;
        }
    }
    
    public static final class Builder
    {
        private final BuilderImpl mImpl;
        
        public Builder() {
            if (Build$VERSION.SDK_INT >= 30) {
                this.mImpl = new BuilderImpl30();
            }
            else if (Build$VERSION.SDK_INT >= 29) {
                this.mImpl = new BuilderImpl29();
            }
            else if (Build$VERSION.SDK_INT >= 20) {
                this.mImpl = new BuilderImpl20();
            }
            else {
                this.mImpl = new BuilderImpl();
            }
        }
        
        public Builder(final WindowInsetsCompat windowInsetsCompat) {
            if (Build$VERSION.SDK_INT >= 30) {
                this.mImpl = new BuilderImpl30(windowInsetsCompat);
            }
            else if (Build$VERSION.SDK_INT >= 29) {
                this.mImpl = new BuilderImpl29(windowInsetsCompat);
            }
            else if (Build$VERSION.SDK_INT >= 20) {
                this.mImpl = new BuilderImpl20(windowInsetsCompat);
            }
            else {
                this.mImpl = new BuilderImpl(windowInsetsCompat);
            }
        }
        
        public WindowInsetsCompat build() {
            return this.mImpl.build();
        }
        
        public Builder setDisplayCutout(final DisplayCutoutCompat displayCutout) {
            this.mImpl.setDisplayCutout(displayCutout);
            return this;
        }
        
        public Builder setInsets(final int n, final Insets insets) {
            this.mImpl.setInsets(n, insets);
            return this;
        }
        
        public Builder setInsetsIgnoringVisibility(final int n, final Insets insets) {
            this.mImpl.setInsetsIgnoringVisibility(n, insets);
            return this;
        }
        
        @Deprecated
        public Builder setMandatorySystemGestureInsets(final Insets mandatorySystemGestureInsets) {
            this.mImpl.setMandatorySystemGestureInsets(mandatorySystemGestureInsets);
            return this;
        }
        
        @Deprecated
        public Builder setStableInsets(final Insets stableInsets) {
            this.mImpl.setStableInsets(stableInsets);
            return this;
        }
        
        @Deprecated
        public Builder setSystemGestureInsets(final Insets systemGestureInsets) {
            this.mImpl.setSystemGestureInsets(systemGestureInsets);
            return this;
        }
        
        @Deprecated
        public Builder setSystemWindowInsets(final Insets systemWindowInsets) {
            this.mImpl.setSystemWindowInsets(systemWindowInsets);
            return this;
        }
        
        @Deprecated
        public Builder setTappableElementInsets(final Insets tappableElementInsets) {
            this.mImpl.setTappableElementInsets(tappableElementInsets);
            return this;
        }
        
        public Builder setVisible(final int n, final boolean b) {
            this.mImpl.setVisible(n, b);
            return this;
        }
    }
    
    private static class BuilderImpl
    {
        private final WindowInsetsCompat mInsets;
        Insets[] mInsetsTypeMask;
        
        BuilderImpl() {
            final WindowInsetsCompat windowInsetsCompat = null;
            this(new WindowInsetsCompat((WindowInsetsCompat)null));
        }
        
        BuilderImpl(final WindowInsetsCompat mInsets) {
            this.mInsets = mInsets;
        }
        
        protected final void applyInsetTypes() {
            final Insets[] mInsetsTypeMask = this.mInsetsTypeMask;
            if (mInsetsTypeMask != null) {
                final Insets insets = mInsetsTypeMask[Type.indexOf(1)];
                Insets insets2;
                if ((insets2 = this.mInsetsTypeMask[Type.indexOf(2)]) == null) {
                    insets2 = this.mInsets.getInsets(2);
                }
                Insets insets3;
                if ((insets3 = insets) == null) {
                    insets3 = this.mInsets.getInsets(1);
                }
                this.setSystemWindowInsets(Insets.max(insets3, insets2));
                final Insets systemGestureInsets = this.mInsetsTypeMask[Type.indexOf(16)];
                if (systemGestureInsets != null) {
                    this.setSystemGestureInsets(systemGestureInsets);
                }
                final Insets mandatorySystemGestureInsets = this.mInsetsTypeMask[Type.indexOf(32)];
                if (mandatorySystemGestureInsets != null) {
                    this.setMandatorySystemGestureInsets(mandatorySystemGestureInsets);
                }
                final Insets tappableElementInsets = this.mInsetsTypeMask[Type.indexOf(64)];
                if (tappableElementInsets != null) {
                    this.setTappableElementInsets(tappableElementInsets);
                }
            }
        }
        
        WindowInsetsCompat build() {
            this.applyInsetTypes();
            return this.mInsets;
        }
        
        void setDisplayCutout(final DisplayCutoutCompat displayCutoutCompat) {
        }
        
        void setInsets(final int n, final Insets insets) {
            if (this.mInsetsTypeMask == null) {
                this.mInsetsTypeMask = new Insets[9];
            }
            for (int i = 1; i <= 256; i <<= 1) {
                if ((n & i) != 0x0) {
                    this.mInsetsTypeMask[Type.indexOf(i)] = insets;
                }
            }
        }
        
        void setInsetsIgnoringVisibility(final int n, final Insets insets) {
            if (n != 8) {
                return;
            }
            throw new IllegalArgumentException("Ignoring visibility inset not available for IME");
        }
        
        void setMandatorySystemGestureInsets(final Insets insets) {
        }
        
        void setStableInsets(final Insets insets) {
        }
        
        void setSystemGestureInsets(final Insets insets) {
        }
        
        void setSystemWindowInsets(final Insets insets) {
        }
        
        void setTappableElementInsets(final Insets insets) {
        }
        
        void setVisible(final int n, final boolean b) {
        }
    }
    
    private static class BuilderImpl20 extends BuilderImpl
    {
        private static Constructor<WindowInsets> sConstructor;
        private static boolean sConstructorFetched = false;
        private static Field sConsumedField;
        private static boolean sConsumedFieldFetched = false;
        private WindowInsets mPlatformInsets;
        private Insets mStableInsets;
        
        BuilderImpl20() {
            this.mPlatformInsets = createWindowInsetsInstance();
        }
        
        BuilderImpl20(final WindowInsetsCompat windowInsetsCompat) {
            super(windowInsetsCompat);
            this.mPlatformInsets = windowInsetsCompat.toWindowInsets();
        }
        
        private static WindowInsets createWindowInsetsInstance() {
            if (!BuilderImpl20.sConsumedFieldFetched) {
                try {
                    BuilderImpl20.sConsumedField = WindowInsets.class.getDeclaredField("CONSUMED");
                }
                catch (final ReflectiveOperationException ex) {
                    Log.i("WindowInsetsCompat", "Could not retrieve WindowInsets.CONSUMED field", (Throwable)ex);
                }
                BuilderImpl20.sConsumedFieldFetched = true;
            }
            final Field sConsumedField = BuilderImpl20.sConsumedField;
            if (sConsumedField != null) {
                try {
                    final WindowInsets windowInsets = (WindowInsets)sConsumedField.get(null);
                    if (windowInsets != null) {
                        return new WindowInsets(windowInsets);
                    }
                }
                catch (final ReflectiveOperationException ex2) {
                    Log.i("WindowInsetsCompat", "Could not get value from WindowInsets.CONSUMED field", (Throwable)ex2);
                }
            }
            if (!BuilderImpl20.sConstructorFetched) {
                try {
                    BuilderImpl20.sConstructor = WindowInsets.class.getConstructor(Rect.class);
                }
                catch (final ReflectiveOperationException ex3) {
                    Log.i("WindowInsetsCompat", "Could not retrieve WindowInsets(Rect) constructor", (Throwable)ex3);
                }
                BuilderImpl20.sConstructorFetched = true;
            }
            final Constructor<WindowInsets> sConstructor = BuilderImpl20.sConstructor;
            if (sConstructor != null) {
                try {
                    return sConstructor.newInstance(new Rect());
                }
                catch (final ReflectiveOperationException ex4) {
                    Log.i("WindowInsetsCompat", "Could not invoke WindowInsets(Rect) constructor", (Throwable)ex4);
                }
            }
            return null;
        }
        
        @Override
        WindowInsetsCompat build() {
            ((BuilderImpl)this).applyInsetTypes();
            final WindowInsetsCompat windowInsetsCompat = WindowInsetsCompat.toWindowInsetsCompat(this.mPlatformInsets);
            windowInsetsCompat.setOverriddenInsets(this.mInsetsTypeMask);
            windowInsetsCompat.setStableInsets(this.mStableInsets);
            return windowInsetsCompat;
        }
        
        @Override
        void setStableInsets(final Insets mStableInsets) {
            this.mStableInsets = mStableInsets;
        }
        
        @Override
        void setSystemWindowInsets(final Insets insets) {
            final WindowInsets mPlatformInsets = this.mPlatformInsets;
            if (mPlatformInsets != null) {
                this.mPlatformInsets = mPlatformInsets.replaceSystemWindowInsets(insets.left, insets.top, insets.right, insets.bottom);
            }
        }
    }
    
    private static class BuilderImpl29 extends BuilderImpl
    {
        final WindowInsets$Builder mPlatBuilder;
        
        BuilderImpl29() {
            this.mPlatBuilder = new WindowInsets$Builder();
        }
        
        BuilderImpl29(final WindowInsetsCompat windowInsetsCompat) {
            super(windowInsetsCompat);
            final WindowInsets windowInsets = windowInsetsCompat.toWindowInsets();
            WindowInsets$Builder mPlatBuilder;
            if (windowInsets != null) {
                mPlatBuilder = new WindowInsets$Builder(windowInsets);
            }
            else {
                mPlatBuilder = new WindowInsets$Builder();
            }
            this.mPlatBuilder = mPlatBuilder;
        }
        
        @Override
        WindowInsetsCompat build() {
            ((BuilderImpl)this).applyInsetTypes();
            final WindowInsetsCompat windowInsetsCompat = WindowInsetsCompat.toWindowInsetsCompat(this.mPlatBuilder.build());
            windowInsetsCompat.setOverriddenInsets(this.mInsetsTypeMask);
            return windowInsetsCompat;
        }
        
        @Override
        void setDisplayCutout(final DisplayCutoutCompat displayCutoutCompat) {
            final WindowInsets$Builder mPlatBuilder = this.mPlatBuilder;
            DisplayCutout unwrap;
            if (displayCutoutCompat != null) {
                unwrap = displayCutoutCompat.unwrap();
            }
            else {
                unwrap = null;
            }
            mPlatBuilder.setDisplayCutout(unwrap);
        }
        
        @Override
        void setMandatorySystemGestureInsets(final Insets insets) {
            this.mPlatBuilder.setMandatorySystemGestureInsets(insets.toPlatformInsets());
        }
        
        @Override
        void setStableInsets(final Insets insets) {
            this.mPlatBuilder.setStableInsets(insets.toPlatformInsets());
        }
        
        @Override
        void setSystemGestureInsets(final Insets insets) {
            this.mPlatBuilder.setSystemGestureInsets(insets.toPlatformInsets());
        }
        
        @Override
        void setSystemWindowInsets(final Insets insets) {
            this.mPlatBuilder.setSystemWindowInsets(insets.toPlatformInsets());
        }
        
        @Override
        void setTappableElementInsets(final Insets insets) {
            this.mPlatBuilder.setTappableElementInsets(insets.toPlatformInsets());
        }
    }
    
    private static class BuilderImpl30 extends BuilderImpl29
    {
        BuilderImpl30() {
        }
        
        BuilderImpl30(final WindowInsetsCompat windowInsetsCompat) {
            super(windowInsetsCompat);
        }
        
        @Override
        void setInsets(final int n, final Insets insets) {
            this.mPlatBuilder.setInsets(TypeImpl30.toPlatformType(n), insets.toPlatformInsets());
        }
        
        @Override
        void setInsetsIgnoringVisibility(final int n, final Insets insets) {
            this.mPlatBuilder.setInsetsIgnoringVisibility(TypeImpl30.toPlatformType(n), insets.toPlatformInsets());
        }
        
        @Override
        void setVisible(final int n, final boolean b) {
            this.mPlatBuilder.setVisible(TypeImpl30.toPlatformType(n), b);
        }
    }
    
    private static class Impl
    {
        static final WindowInsetsCompat CONSUMED;
        final WindowInsetsCompat mHost;
        
        static {
            CONSUMED = new Builder().build().consumeDisplayCutout().consumeStableInsets().consumeSystemWindowInsets();
        }
        
        Impl(final WindowInsetsCompat mHost) {
            this.mHost = mHost;
        }
        
        WindowInsetsCompat consumeDisplayCutout() {
            return this.mHost;
        }
        
        WindowInsetsCompat consumeStableInsets() {
            return this.mHost;
        }
        
        WindowInsetsCompat consumeSystemWindowInsets() {
            return this.mHost;
        }
        
        void copyRootViewBounds(final View view) {
        }
        
        void copyWindowDataInto(final WindowInsetsCompat windowInsetsCompat) {
        }
        
        @Override
        public boolean equals(final Object o) {
            boolean b = true;
            if (this == o) {
                return true;
            }
            if (!(o instanceof Impl)) {
                return false;
            }
            final Impl impl = (Impl)o;
            if (this.isRound() != impl.isRound() || this.isConsumed() != impl.isConsumed() || !ObjectsCompat.equals(this.getSystemWindowInsets(), impl.getSystemWindowInsets()) || !ObjectsCompat.equals(this.getStableInsets(), impl.getStableInsets()) || !ObjectsCompat.equals(this.getDisplayCutout(), impl.getDisplayCutout())) {
                b = false;
            }
            return b;
        }
        
        DisplayCutoutCompat getDisplayCutout() {
            return null;
        }
        
        Insets getInsets(final int n) {
            return Insets.NONE;
        }
        
        Insets getInsetsIgnoringVisibility(final int n) {
            if ((n & 0x8) == 0x0) {
                return Insets.NONE;
            }
            throw new IllegalArgumentException("Unable to query the maximum insets for IME");
        }
        
        Insets getMandatorySystemGestureInsets() {
            return this.getSystemWindowInsets();
        }
        
        Insets getStableInsets() {
            return Insets.NONE;
        }
        
        Insets getSystemGestureInsets() {
            return this.getSystemWindowInsets();
        }
        
        Insets getSystemWindowInsets() {
            return Insets.NONE;
        }
        
        Insets getTappableElementInsets() {
            return this.getSystemWindowInsets();
        }
        
        @Override
        public int hashCode() {
            return ObjectsCompat.hash(this.isRound(), this.isConsumed(), this.getSystemWindowInsets(), this.getStableInsets(), this.getDisplayCutout());
        }
        
        WindowInsetsCompat inset(final int n, final int n2, final int n3, final int n4) {
            return Impl.CONSUMED;
        }
        
        boolean isConsumed() {
            return false;
        }
        
        boolean isRound() {
            return false;
        }
        
        boolean isVisible(final int n) {
            return true;
        }
        
        public void setOverriddenInsets(final Insets[] array) {
        }
        
        void setRootViewData(final Insets insets) {
        }
        
        void setRootWindowInsets(final WindowInsetsCompat windowInsetsCompat) {
        }
        
        public void setStableInsets(final Insets insets) {
        }
    }
    
    private static class Impl20 extends Impl
    {
        private static Class<?> sAttachInfoClass;
        private static Field sAttachInfoField;
        private static Method sGetViewRootImplMethod;
        private static Field sVisibleInsetsField;
        private static boolean sVisibleRectReflectionFetched = false;
        private Insets[] mOverriddenInsets;
        final WindowInsets mPlatformInsets;
        Insets mRootViewVisibleInsets;
        private WindowInsetsCompat mRootWindowInsets;
        private Insets mSystemWindowInsets;
        
        Impl20(final WindowInsetsCompat windowInsetsCompat, final WindowInsets mPlatformInsets) {
            super(windowInsetsCompat);
            this.mSystemWindowInsets = null;
            this.mPlatformInsets = mPlatformInsets;
        }
        
        Impl20(final WindowInsetsCompat windowInsetsCompat, final Impl20 impl20) {
            this(windowInsetsCompat, new WindowInsets(impl20.mPlatformInsets));
        }
        
        private Insets getInsets(final int n, final boolean b) {
            Insets insets = Insets.NONE;
            for (int i = 1; i <= 256; i <<= 1) {
                if ((n & i) != 0x0) {
                    insets = Insets.max(insets, this.getInsetsForType(i, b));
                }
            }
            return insets;
        }
        
        private Insets getRootStableInsets() {
            final WindowInsetsCompat mRootWindowInsets = this.mRootWindowInsets;
            if (mRootWindowInsets != null) {
                return mRootWindowInsets.getStableInsets();
            }
            return Insets.NONE;
        }
        
        private Insets getVisibleInsets(final View obj) {
            if (Build$VERSION.SDK_INT < 30) {
                if (!Impl20.sVisibleRectReflectionFetched) {
                    loadReflectionField();
                }
                final Method sGetViewRootImplMethod = Impl20.sGetViewRootImplMethod;
                final Insets insets = null;
                if (sGetViewRootImplMethod != null && Impl20.sAttachInfoClass != null) {
                    if (Impl20.sVisibleInsetsField != null) {
                        try {
                            final Object invoke = sGetViewRootImplMethod.invoke(obj, new Object[0]);
                            if (invoke == null) {
                                Log.w("WindowInsetsCompat", "Failed to get visible insets. getViewRootImpl() returned null from the provided view. This means that the view is either not attached or the method has been overridden", (Throwable)new NullPointerException());
                                return null;
                            }
                            final Rect rect = (Rect)Impl20.sVisibleInsetsField.get(Impl20.sAttachInfoField.get(invoke));
                            Insets of = insets;
                            if (rect != null) {
                                of = Insets.of(rect);
                            }
                            return of;
                        }
                        catch (final ReflectiveOperationException ex) {
                            final StringBuilder sb = new StringBuilder();
                            sb.append("Failed to get visible insets. (Reflection error). ");
                            sb.append(ex.getMessage());
                            Log.e("WindowInsetsCompat", sb.toString(), (Throwable)ex);
                        }
                    }
                }
                return null;
            }
            throw new UnsupportedOperationException("getVisibleInsets() should not be called on API >= 30. Use WindowInsets.isVisible() instead.");
        }
        
        private static void loadReflectionField() {
            try {
                Impl20.sGetViewRootImplMethod = View.class.getDeclaredMethod("getViewRootImpl", (Class<?>[])new Class[0]);
                Impl20.sVisibleInsetsField = (Impl20.sAttachInfoClass = Class.forName("android.view.View$AttachInfo")).getDeclaredField("mVisibleInsets");
                Impl20.sAttachInfoField = Class.forName("android.view.ViewRootImpl").getDeclaredField("mAttachInfo");
                Impl20.sVisibleInsetsField.setAccessible(true);
                Impl20.sAttachInfoField.setAccessible(true);
            }
            catch (final ReflectiveOperationException ex) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Failed to get visible insets. (Reflection error). ");
                sb.append(ex.getMessage());
                Log.e("WindowInsetsCompat", sb.toString(), (Throwable)ex);
            }
            Impl20.sVisibleRectReflectionFetched = true;
        }
        
        @Override
        void copyRootViewBounds(final View view) {
            Insets rootViewData;
            if ((rootViewData = this.getVisibleInsets(view)) == null) {
                rootViewData = Insets.NONE;
            }
            this.setRootViewData(rootViewData);
        }
        
        @Override
        void copyWindowDataInto(final WindowInsetsCompat windowInsetsCompat) {
            windowInsetsCompat.setRootWindowInsets(this.mRootWindowInsets);
            windowInsetsCompat.setRootViewData(this.mRootViewVisibleInsets);
        }
        
        @Override
        public boolean equals(final Object o) {
            return super.equals(o) && Objects.equals(this.mRootViewVisibleInsets, ((Impl20)o).mRootViewVisibleInsets);
        }
        
        public Insets getInsets(final int n) {
            return this.getInsets(n, false);
        }
        
        protected Insets getInsetsForType(int n, final boolean b) {
            if (n != 1) {
                Insets stableInsets = null;
                final Insets insets = null;
                if (n != 2) {
                    if (n != 8) {
                        if (n == 16) {
                            return ((Impl)this).getSystemGestureInsets();
                        }
                        if (n == 32) {
                            return ((Impl)this).getMandatorySystemGestureInsets();
                        }
                        if (n == 64) {
                            return ((Impl)this).getTappableElementInsets();
                        }
                        if (n != 128) {
                            return Insets.NONE;
                        }
                        final WindowInsetsCompat mRootWindowInsets = this.mRootWindowInsets;
                        DisplayCutoutCompat displayCutoutCompat;
                        if (mRootWindowInsets != null) {
                            displayCutoutCompat = mRootWindowInsets.getDisplayCutout();
                        }
                        else {
                            displayCutoutCompat = ((Impl)this).getDisplayCutout();
                        }
                        if (displayCutoutCompat != null) {
                            return Insets.of(displayCutoutCompat.getSafeInsetLeft(), displayCutoutCompat.getSafeInsetTop(), displayCutoutCompat.getSafeInsetRight(), displayCutoutCompat.getSafeInsetBottom());
                        }
                        return Insets.NONE;
                    }
                    else {
                        final Insets[] mOverriddenInsets = this.mOverriddenInsets;
                        Insets insets2 = insets;
                        if (mOverriddenInsets != null) {
                            insets2 = mOverriddenInsets[Type.indexOf(8)];
                        }
                        if (insets2 != null) {
                            return insets2;
                        }
                        final Insets systemWindowInsets = this.getSystemWindowInsets();
                        final Insets rootStableInsets = this.getRootStableInsets();
                        if (systemWindowInsets.bottom > rootStableInsets.bottom) {
                            return Insets.of(0, 0, 0, systemWindowInsets.bottom);
                        }
                        final Insets mRootViewVisibleInsets = this.mRootViewVisibleInsets;
                        if (mRootViewVisibleInsets != null && !mRootViewVisibleInsets.equals(Insets.NONE) && this.mRootViewVisibleInsets.bottom > rootStableInsets.bottom) {
                            return Insets.of(0, 0, 0, this.mRootViewVisibleInsets.bottom);
                        }
                        return Insets.NONE;
                    }
                }
                else {
                    if (b) {
                        final Insets rootStableInsets2 = this.getRootStableInsets();
                        final Insets stableInsets2 = ((Impl)this).getStableInsets();
                        return Insets.of(Math.max(rootStableInsets2.left, stableInsets2.left), 0, Math.max(rootStableInsets2.right, stableInsets2.right), Math.max(rootStableInsets2.bottom, stableInsets2.bottom));
                    }
                    final Insets systemWindowInsets2 = this.getSystemWindowInsets();
                    final WindowInsetsCompat mRootWindowInsets2 = this.mRootWindowInsets;
                    if (mRootWindowInsets2 != null) {
                        stableInsets = mRootWindowInsets2.getStableInsets();
                    }
                    final int a = n = systemWindowInsets2.bottom;
                    if (stableInsets != null) {
                        n = Math.min(a, stableInsets.bottom);
                    }
                    return Insets.of(systemWindowInsets2.left, 0, systemWindowInsets2.right, n);
                }
            }
            else {
                if (b) {
                    return Insets.of(0, Math.max(this.getRootStableInsets().top, this.getSystemWindowInsets().top), 0, 0);
                }
                return Insets.of(0, this.getSystemWindowInsets().top, 0, 0);
            }
        }
        
        public Insets getInsetsIgnoringVisibility(final int n) {
            return this.getInsets(n, true);
        }
        
        @Override
        final Insets getSystemWindowInsets() {
            if (this.mSystemWindowInsets == null) {
                this.mSystemWindowInsets = Insets.of(this.mPlatformInsets.getSystemWindowInsetLeft(), this.mPlatformInsets.getSystemWindowInsetTop(), this.mPlatformInsets.getSystemWindowInsetRight(), this.mPlatformInsets.getSystemWindowInsetBottom());
            }
            return this.mSystemWindowInsets;
        }
        
        @Override
        WindowInsetsCompat inset(final int n, final int n2, final int n3, final int n4) {
            final Builder builder = new Builder(WindowInsetsCompat.toWindowInsetsCompat(this.mPlatformInsets));
            builder.setSystemWindowInsets(WindowInsetsCompat.insetInsets(this.getSystemWindowInsets(), n, n2, n3, n4));
            builder.setStableInsets(WindowInsetsCompat.insetInsets(((Impl)this).getStableInsets(), n, n2, n3, n4));
            return builder.build();
        }
        
        @Override
        boolean isRound() {
            return this.mPlatformInsets.isRound();
        }
        
        protected boolean isTypeVisible(final int n) {
            if (n != 1 && n != 2) {
                if (n == 4) {
                    return false;
                }
                if (n != 8 && n != 128) {
                    return true;
                }
            }
            return this.getInsetsForType(n, false).equals(Insets.NONE) ^ true;
        }
        
        @Override
        boolean isVisible(final int n) {
            for (int i = 1; i <= 256; i <<= 1) {
                if ((n & i) != 0x0) {
                    if (!this.isTypeVisible(i)) {
                        return false;
                    }
                }
            }
            return true;
        }
        
        @Override
        public void setOverriddenInsets(final Insets[] mOverriddenInsets) {
            this.mOverriddenInsets = mOverriddenInsets;
        }
        
        @Override
        void setRootViewData(final Insets mRootViewVisibleInsets) {
            this.mRootViewVisibleInsets = mRootViewVisibleInsets;
        }
        
        @Override
        void setRootWindowInsets(final WindowInsetsCompat mRootWindowInsets) {
            this.mRootWindowInsets = mRootWindowInsets;
        }
    }
    
    private static class Impl21 extends Impl20
    {
        private Insets mStableInsets;
        
        Impl21(final WindowInsetsCompat windowInsetsCompat, final WindowInsets windowInsets) {
            super(windowInsetsCompat, windowInsets);
            this.mStableInsets = null;
        }
        
        Impl21(final WindowInsetsCompat windowInsetsCompat, final Impl21 impl21) {
            super(windowInsetsCompat, (Impl20)impl21);
            this.mStableInsets = null;
            this.mStableInsets = impl21.mStableInsets;
        }
        
        @Override
        WindowInsetsCompat consumeStableInsets() {
            return WindowInsetsCompat.toWindowInsetsCompat(this.mPlatformInsets.consumeStableInsets());
        }
        
        @Override
        WindowInsetsCompat consumeSystemWindowInsets() {
            return WindowInsetsCompat.toWindowInsetsCompat(this.mPlatformInsets.consumeSystemWindowInsets());
        }
        
        @Override
        final Insets getStableInsets() {
            if (this.mStableInsets == null) {
                this.mStableInsets = Insets.of(this.mPlatformInsets.getStableInsetLeft(), this.mPlatformInsets.getStableInsetTop(), this.mPlatformInsets.getStableInsetRight(), this.mPlatformInsets.getStableInsetBottom());
            }
            return this.mStableInsets;
        }
        
        @Override
        boolean isConsumed() {
            return this.mPlatformInsets.isConsumed();
        }
        
        @Override
        public void setStableInsets(final Insets mStableInsets) {
            this.mStableInsets = mStableInsets;
        }
    }
    
    private static class Impl28 extends Impl21
    {
        Impl28(final WindowInsetsCompat windowInsetsCompat, final WindowInsets windowInsets) {
            super(windowInsetsCompat, windowInsets);
        }
        
        Impl28(final WindowInsetsCompat windowInsetsCompat, final Impl28 impl28) {
            super(windowInsetsCompat, (Impl21)impl28);
        }
        
        @Override
        WindowInsetsCompat consumeDisplayCutout() {
            return WindowInsetsCompat.toWindowInsetsCompat(this.mPlatformInsets.consumeDisplayCutout());
        }
        
        @Override
        public boolean equals(final Object o) {
            boolean b = true;
            if (this == o) {
                return true;
            }
            if (!(o instanceof Impl28)) {
                return false;
            }
            final Impl28 impl28 = (Impl28)o;
            if (!Objects.equals(this.mPlatformInsets, impl28.mPlatformInsets) || !Objects.equals(this.mRootViewVisibleInsets, impl28.mRootViewVisibleInsets)) {
                b = false;
            }
            return b;
        }
        
        @Override
        DisplayCutoutCompat getDisplayCutout() {
            return DisplayCutoutCompat.wrap(this.mPlatformInsets.getDisplayCutout());
        }
        
        @Override
        public int hashCode() {
            return this.mPlatformInsets.hashCode();
        }
    }
    
    private static class Impl29 extends Impl28
    {
        private Insets mMandatorySystemGestureInsets;
        private Insets mSystemGestureInsets;
        private Insets mTappableElementInsets;
        
        Impl29(final WindowInsetsCompat windowInsetsCompat, final WindowInsets windowInsets) {
            super(windowInsetsCompat, windowInsets);
            this.mSystemGestureInsets = null;
            this.mMandatorySystemGestureInsets = null;
            this.mTappableElementInsets = null;
        }
        
        Impl29(final WindowInsetsCompat windowInsetsCompat, final Impl29 impl29) {
            super(windowInsetsCompat, (Impl28)impl29);
            this.mSystemGestureInsets = null;
            this.mMandatorySystemGestureInsets = null;
            this.mTappableElementInsets = null;
        }
        
        @Override
        Insets getMandatorySystemGestureInsets() {
            if (this.mMandatorySystemGestureInsets == null) {
                this.mMandatorySystemGestureInsets = Insets.toCompatInsets(this.mPlatformInsets.getMandatorySystemGestureInsets());
            }
            return this.mMandatorySystemGestureInsets;
        }
        
        @Override
        Insets getSystemGestureInsets() {
            if (this.mSystemGestureInsets == null) {
                this.mSystemGestureInsets = Insets.toCompatInsets(this.mPlatformInsets.getSystemGestureInsets());
            }
            return this.mSystemGestureInsets;
        }
        
        @Override
        Insets getTappableElementInsets() {
            if (this.mTappableElementInsets == null) {
                this.mTappableElementInsets = Insets.toCompatInsets(this.mPlatformInsets.getTappableElementInsets());
            }
            return this.mTappableElementInsets;
        }
        
        @Override
        WindowInsetsCompat inset(final int n, final int n2, final int n3, final int n4) {
            return WindowInsetsCompat.toWindowInsetsCompat(this.mPlatformInsets.inset(n, n2, n3, n4));
        }
        
        @Override
        public void setStableInsets(final Insets insets) {
        }
    }
    
    private static class Impl30 extends Impl29
    {
        static final WindowInsetsCompat CONSUMED;
        
        static {
            CONSUMED = WindowInsetsCompat.toWindowInsetsCompat(WindowInsets.CONSUMED);
        }
        
        Impl30(final WindowInsetsCompat windowInsetsCompat, final WindowInsets windowInsets) {
            super(windowInsetsCompat, windowInsets);
        }
        
        Impl30(final WindowInsetsCompat windowInsetsCompat, final Impl30 impl30) {
            super(windowInsetsCompat, (Impl29)impl30);
        }
        
        @Override
        final void copyRootViewBounds(final View view) {
        }
        
        @Override
        public Insets getInsets(final int n) {
            return Insets.toCompatInsets(this.mPlatformInsets.getInsets(TypeImpl30.toPlatformType(n)));
        }
        
        @Override
        public Insets getInsetsIgnoringVisibility(final int n) {
            return Insets.toCompatInsets(this.mPlatformInsets.getInsetsIgnoringVisibility(TypeImpl30.toPlatformType(n)));
        }
        
        public boolean isVisible(final int n) {
            return this.mPlatformInsets.isVisible(TypeImpl30.toPlatformType(n));
        }
    }
    
    public static final class Type
    {
        static final int CAPTION_BAR = 4;
        static final int DISPLAY_CUTOUT = 128;
        static final int FIRST = 1;
        static final int IME = 8;
        static final int LAST = 256;
        static final int MANDATORY_SYSTEM_GESTURES = 32;
        static final int NAVIGATION_BARS = 2;
        static final int SIZE = 9;
        static final int STATUS_BARS = 1;
        static final int SYSTEM_GESTURES = 16;
        static final int TAPPABLE_ELEMENT = 64;
        static final int WINDOW_DECOR = 256;
        
        private Type() {
        }
        
        static int all() {
            return -1;
        }
        
        public static int captionBar() {
            return 4;
        }
        
        public static int displayCutout() {
            return 128;
        }
        
        public static int ime() {
            return 8;
        }
        
        static int indexOf(final int i) {
            if (i == 1) {
                return 0;
            }
            if (i == 2) {
                return 1;
            }
            if (i == 4) {
                return 2;
            }
            if (i == 8) {
                return 3;
            }
            if (i == 16) {
                return 4;
            }
            if (i == 32) {
                return 5;
            }
            if (i == 64) {
                return 6;
            }
            if (i == 128) {
                return 7;
            }
            if (i == 256) {
                return 8;
            }
            final StringBuilder sb = new StringBuilder();
            sb.append("type needs to be >= FIRST and <= LAST, type=");
            sb.append(i);
            throw new IllegalArgumentException(sb.toString());
        }
        
        public static int mandatorySystemGestures() {
            return 32;
        }
        
        public static int navigationBars() {
            return 2;
        }
        
        public static int statusBars() {
            return 1;
        }
        
        public static int systemBars() {
            return 7;
        }
        
        public static int systemGestures() {
            return 16;
        }
        
        public static int tappableElement() {
            return 64;
        }
        
        @Retention(RetentionPolicy.SOURCE)
        public @interface InsetsType {
        }
    }
    
    private static final class TypeImpl30
    {
        static int toPlatformType(final int n) {
            int n2 = 0;
            int n3;
            for (int i = 1; i <= 256; i <<= 1, n2 = n3) {
                n3 = n2;
                if ((n & i) != 0x0) {
                    int n4;
                    if (i != 1) {
                        if (i != 2) {
                            if (i != 4) {
                                if (i != 8) {
                                    if (i != 16) {
                                        if (i != 32) {
                                            if (i != 64) {
                                                if (i != 128) {
                                                    n3 = n2;
                                                    continue;
                                                }
                                                n4 = WindowInsets$Type.displayCutout();
                                            }
                                            else {
                                                n4 = WindowInsets$Type.tappableElement();
                                            }
                                        }
                                        else {
                                            n4 = WindowInsets$Type.mandatorySystemGestures();
                                        }
                                    }
                                    else {
                                        n4 = WindowInsets$Type.systemGestures();
                                    }
                                }
                                else {
                                    n4 = WindowInsets$Type.ime();
                                }
                            }
                            else {
                                n4 = WindowInsets$Type.captionBar();
                            }
                        }
                        else {
                            n4 = WindowInsets$Type.navigationBars();
                        }
                    }
                    else {
                        n4 = WindowInsets$Type.statusBars();
                    }
                    n3 = (n2 | n4);
                }
            }
            return n2;
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.view;

import android.view.MenuItem;
import android.view.MenuInflater;
import android.view.Menu;

public interface MenuProvider
{
    void onCreateMenu(final Menu p0, final MenuInflater p1);
    
    void onMenuClosed(final Menu p0);
    
    boolean onMenuItemSelected(final MenuItem p0);
    
    void onPrepareMenu(final Menu p0);
}

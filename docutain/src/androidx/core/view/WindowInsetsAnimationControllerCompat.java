// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.view;

import androidx.core.graphics.Insets;
import android.view.WindowInsetsAnimationController;

public final class WindowInsetsAnimationControllerCompat
{
    private final Impl mImpl;
    
    WindowInsetsAnimationControllerCompat(final WindowInsetsAnimationController windowInsetsAnimationController) {
        this.mImpl = (Impl)new Impl30(windowInsetsAnimationController);
    }
    
    public void finish(final boolean b) {
        this.mImpl.finish(b);
    }
    
    public float getCurrentAlpha() {
        return this.mImpl.getCurrentAlpha();
    }
    
    public float getCurrentFraction() {
        return this.mImpl.getCurrentFraction();
    }
    
    public Insets getCurrentInsets() {
        return this.mImpl.getCurrentInsets();
    }
    
    public Insets getHiddenStateInsets() {
        return this.mImpl.getHiddenStateInsets();
    }
    
    public Insets getShownStateInsets() {
        return this.mImpl.getShownStateInsets();
    }
    
    public int getTypes() {
        return this.mImpl.getTypes();
    }
    
    public boolean isCancelled() {
        return this.mImpl.isCancelled();
    }
    
    public boolean isFinished() {
        return this.mImpl.isFinished();
    }
    
    public boolean isReady() {
        return !this.isFinished() && !this.isCancelled();
    }
    
    public void setInsetsAndAlpha(final Insets insets, final float n, final float n2) {
        this.mImpl.setInsetsAndAlpha(insets, n, n2);
    }
    
    private static class Impl
    {
        Impl() {
        }
        
        void finish(final boolean b) {
        }
        
        public float getCurrentAlpha() {
            return 0.0f;
        }
        
        public float getCurrentFraction() {
            return 0.0f;
        }
        
        public Insets getCurrentInsets() {
            return Insets.NONE;
        }
        
        public Insets getHiddenStateInsets() {
            return Insets.NONE;
        }
        
        public Insets getShownStateInsets() {
            return Insets.NONE;
        }
        
        public int getTypes() {
            return 0;
        }
        
        boolean isCancelled() {
            return true;
        }
        
        boolean isFinished() {
            return false;
        }
        
        public void setInsetsAndAlpha(final Insets insets, final float n, final float n2) {
        }
    }
    
    private static class Impl30 extends Impl
    {
        private final WindowInsetsAnimationController mController;
        
        Impl30(final WindowInsetsAnimationController mController) {
            this.mController = mController;
        }
        
        @Override
        void finish(final boolean b) {
            this.mController.finish(b);
        }
        
        @Override
        public float getCurrentAlpha() {
            return this.mController.getCurrentAlpha();
        }
        
        @Override
        public float getCurrentFraction() {
            return this.mController.getCurrentFraction();
        }
        
        @Override
        public Insets getCurrentInsets() {
            return Insets.toCompatInsets(this.mController.getCurrentInsets());
        }
        
        @Override
        public Insets getHiddenStateInsets() {
            return Insets.toCompatInsets(this.mController.getHiddenStateInsets());
        }
        
        @Override
        public Insets getShownStateInsets() {
            return Insets.toCompatInsets(this.mController.getShownStateInsets());
        }
        
        @Override
        public int getTypes() {
            return this.mController.getTypes();
        }
        
        @Override
        boolean isCancelled() {
            return this.mController.isCancelled();
        }
        
        @Override
        boolean isFinished() {
            return this.mController.isFinished();
        }
        
        @Override
        public void setInsetsAndAlpha(final Insets insets, final float n, final float n2) {
            final WindowInsetsAnimationController mController = this.mController;
            android.graphics.Insets platformInsets;
            if (insets == null) {
                platformInsets = null;
            }
            else {
                platformInsets = insets.toPlatformInsets();
            }
            mController.setInsetsAndAlpha(platformInsets, n, n2);
        }
    }
}

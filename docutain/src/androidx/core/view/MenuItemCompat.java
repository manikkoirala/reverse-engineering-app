// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.view;

import android.view.MenuItem$OnActionExpandListener;
import android.graphics.PorterDuff$Mode;
import android.content.res.ColorStateList;
import android.os.Build$VERSION;
import android.view.View;
import android.util.Log;
import androidx.core.internal.view.SupportMenuItem;
import android.view.MenuItem;

public final class MenuItemCompat
{
    @Deprecated
    public static final int SHOW_AS_ACTION_ALWAYS = 2;
    @Deprecated
    public static final int SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW = 8;
    @Deprecated
    public static final int SHOW_AS_ACTION_IF_ROOM = 1;
    @Deprecated
    public static final int SHOW_AS_ACTION_NEVER = 0;
    @Deprecated
    public static final int SHOW_AS_ACTION_WITH_TEXT = 4;
    private static final String TAG = "MenuItemCompat";
    
    private MenuItemCompat() {
    }
    
    @Deprecated
    public static boolean collapseActionView(final MenuItem menuItem) {
        return menuItem.collapseActionView();
    }
    
    @Deprecated
    public static boolean expandActionView(final MenuItem menuItem) {
        return menuItem.expandActionView();
    }
    
    public static ActionProvider getActionProvider(final MenuItem menuItem) {
        if (menuItem instanceof SupportMenuItem) {
            return ((SupportMenuItem)menuItem).getSupportActionProvider();
        }
        Log.w("MenuItemCompat", "getActionProvider: item does not implement SupportMenuItem; returning null");
        return null;
    }
    
    @Deprecated
    public static View getActionView(final MenuItem menuItem) {
        return menuItem.getActionView();
    }
    
    public static int getAlphabeticModifiers(final MenuItem menuItem) {
        if (menuItem instanceof SupportMenuItem) {
            return ((SupportMenuItem)menuItem).getAlphabeticModifiers();
        }
        if (Build$VERSION.SDK_INT >= 26) {
            return Api26Impl.getAlphabeticModifiers(menuItem);
        }
        return 0;
    }
    
    public static CharSequence getContentDescription(final MenuItem menuItem) {
        if (menuItem instanceof SupportMenuItem) {
            return ((SupportMenuItem)menuItem).getContentDescription();
        }
        if (Build$VERSION.SDK_INT >= 26) {
            return Api26Impl.getContentDescription(menuItem);
        }
        return null;
    }
    
    public static ColorStateList getIconTintList(final MenuItem menuItem) {
        if (menuItem instanceof SupportMenuItem) {
            return ((SupportMenuItem)menuItem).getIconTintList();
        }
        if (Build$VERSION.SDK_INT >= 26) {
            return Api26Impl.getIconTintList(menuItem);
        }
        return null;
    }
    
    public static PorterDuff$Mode getIconTintMode(final MenuItem menuItem) {
        if (menuItem instanceof SupportMenuItem) {
            return ((SupportMenuItem)menuItem).getIconTintMode();
        }
        if (Build$VERSION.SDK_INT >= 26) {
            return Api26Impl.getIconTintMode(menuItem);
        }
        return null;
    }
    
    public static int getNumericModifiers(final MenuItem menuItem) {
        if (menuItem instanceof SupportMenuItem) {
            return ((SupportMenuItem)menuItem).getNumericModifiers();
        }
        if (Build$VERSION.SDK_INT >= 26) {
            return Api26Impl.getNumericModifiers(menuItem);
        }
        return 0;
    }
    
    public static CharSequence getTooltipText(final MenuItem menuItem) {
        if (menuItem instanceof SupportMenuItem) {
            return ((SupportMenuItem)menuItem).getTooltipText();
        }
        if (Build$VERSION.SDK_INT >= 26) {
            return Api26Impl.getTooltipText(menuItem);
        }
        return null;
    }
    
    @Deprecated
    public static boolean isActionViewExpanded(final MenuItem menuItem) {
        return menuItem.isActionViewExpanded();
    }
    
    public static MenuItem setActionProvider(final MenuItem menuItem, final ActionProvider supportActionProvider) {
        if (menuItem instanceof SupportMenuItem) {
            return (MenuItem)((SupportMenuItem)menuItem).setSupportActionProvider(supportActionProvider);
        }
        Log.w("MenuItemCompat", "setActionProvider: item does not implement SupportMenuItem; ignoring");
        return menuItem;
    }
    
    @Deprecated
    public static MenuItem setActionView(final MenuItem menuItem, final int actionView) {
        return menuItem.setActionView(actionView);
    }
    
    @Deprecated
    public static MenuItem setActionView(final MenuItem menuItem, final View actionView) {
        return menuItem.setActionView(actionView);
    }
    
    public static void setAlphabeticShortcut(final MenuItem menuItem, final char c, final int n) {
        if (menuItem instanceof SupportMenuItem) {
            ((SupportMenuItem)menuItem).setAlphabeticShortcut(c, n);
        }
        else if (Build$VERSION.SDK_INT >= 26) {
            Api26Impl.setAlphabeticShortcut(menuItem, c, n);
        }
    }
    
    public static void setContentDescription(final MenuItem menuItem, final CharSequence contentDescription) {
        if (menuItem instanceof SupportMenuItem) {
            ((SupportMenuItem)menuItem).setContentDescription(contentDescription);
        }
        else if (Build$VERSION.SDK_INT >= 26) {
            Api26Impl.setContentDescription(menuItem, contentDescription);
        }
    }
    
    public static void setIconTintList(final MenuItem menuItem, final ColorStateList iconTintList) {
        if (menuItem instanceof SupportMenuItem) {
            ((SupportMenuItem)menuItem).setIconTintList(iconTintList);
        }
        else if (Build$VERSION.SDK_INT >= 26) {
            Api26Impl.setIconTintList(menuItem, iconTintList);
        }
    }
    
    public static void setIconTintMode(final MenuItem menuItem, final PorterDuff$Mode iconTintMode) {
        if (menuItem instanceof SupportMenuItem) {
            ((SupportMenuItem)menuItem).setIconTintMode(iconTintMode);
        }
        else if (Build$VERSION.SDK_INT >= 26) {
            Api26Impl.setIconTintMode(menuItem, iconTintMode);
        }
    }
    
    public static void setNumericShortcut(final MenuItem menuItem, final char c, final int n) {
        if (menuItem instanceof SupportMenuItem) {
            ((SupportMenuItem)menuItem).setNumericShortcut(c, n);
        }
        else if (Build$VERSION.SDK_INT >= 26) {
            Api26Impl.setNumericShortcut(menuItem, c, n);
        }
    }
    
    @Deprecated
    public static MenuItem setOnActionExpandListener(final MenuItem menuItem, final OnActionExpandListener onActionExpandListener) {
        return menuItem.setOnActionExpandListener((MenuItem$OnActionExpandListener)new MenuItem$OnActionExpandListener(onActionExpandListener) {
            final OnActionExpandListener val$listener;
            
            public boolean onMenuItemActionCollapse(final MenuItem menuItem) {
                return this.val$listener.onMenuItemActionCollapse(menuItem);
            }
            
            public boolean onMenuItemActionExpand(final MenuItem menuItem) {
                return this.val$listener.onMenuItemActionExpand(menuItem);
            }
        });
    }
    
    public static void setShortcut(final MenuItem menuItem, final char c, final char c2, final int n, final int n2) {
        if (menuItem instanceof SupportMenuItem) {
            ((SupportMenuItem)menuItem).setShortcut(c, c2, n, n2);
        }
        else if (Build$VERSION.SDK_INT >= 26) {
            Api26Impl.setShortcut(menuItem, c, c2, n, n2);
        }
    }
    
    @Deprecated
    public static void setShowAsAction(final MenuItem menuItem, final int showAsAction) {
        menuItem.setShowAsAction(showAsAction);
    }
    
    public static void setTooltipText(final MenuItem menuItem, final CharSequence tooltipText) {
        if (menuItem instanceof SupportMenuItem) {
            ((SupportMenuItem)menuItem).setTooltipText(tooltipText);
        }
        else if (Build$VERSION.SDK_INT >= 26) {
            Api26Impl.setTooltipText(menuItem, tooltipText);
        }
    }
    
    static class Api26Impl
    {
        private Api26Impl() {
        }
        
        static int getAlphabeticModifiers(final MenuItem menuItem) {
            return menuItem.getAlphabeticModifiers();
        }
        
        static CharSequence getContentDescription(final MenuItem menuItem) {
            return menuItem.getContentDescription();
        }
        
        static ColorStateList getIconTintList(final MenuItem menuItem) {
            return menuItem.getIconTintList();
        }
        
        static PorterDuff$Mode getIconTintMode(final MenuItem menuItem) {
            return menuItem.getIconTintMode();
        }
        
        static int getNumericModifiers(final MenuItem menuItem) {
            return menuItem.getNumericModifiers();
        }
        
        static CharSequence getTooltipText(final MenuItem menuItem) {
            return menuItem.getTooltipText();
        }
        
        static MenuItem setAlphabeticShortcut(final MenuItem menuItem, final char c, final int n) {
            return menuItem.setAlphabeticShortcut(c, n);
        }
        
        static MenuItem setContentDescription(final MenuItem menuItem, final CharSequence contentDescription) {
            return menuItem.setContentDescription(contentDescription);
        }
        
        static MenuItem setIconTintList(final MenuItem menuItem, final ColorStateList iconTintList) {
            return menuItem.setIconTintList(iconTintList);
        }
        
        static MenuItem setIconTintMode(final MenuItem menuItem, final PorterDuff$Mode iconTintMode) {
            return menuItem.setIconTintMode(iconTintMode);
        }
        
        static MenuItem setNumericShortcut(final MenuItem menuItem, final char c, final int n) {
            return menuItem.setNumericShortcut(c, n);
        }
        
        static MenuItem setShortcut(final MenuItem menuItem, final char c, final char c2, final int n, final int n2) {
            return menuItem.setShortcut(c, c2, n, n2);
        }
        
        static MenuItem setTooltipText(final MenuItem menuItem, final CharSequence tooltipText) {
            return menuItem.setTooltipText(tooltipText);
        }
    }
    
    @Deprecated
    public interface OnActionExpandListener
    {
        boolean onMenuItemActionCollapse(final MenuItem p0);
        
        boolean onMenuItemActionExpand(final MenuItem p0);
    }
}

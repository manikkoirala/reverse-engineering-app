// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.view;

import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;
import android.os.Build$VERSION;
import android.view.VelocityTracker;

public final class VelocityTrackerCompat
{
    private VelocityTrackerCompat() {
    }
    
    public static float getAxisVelocity(final VelocityTracker velocityTracker, final int n) {
        if (Build$VERSION.SDK_INT >= 34) {
            return Api34Impl.getAxisVelocity(velocityTracker, n);
        }
        if (n == 0) {
            return velocityTracker.getXVelocity();
        }
        if (n == 1) {
            return velocityTracker.getYVelocity();
        }
        return 0.0f;
    }
    
    public static float getAxisVelocity(final VelocityTracker velocityTracker, final int n, final int n2) {
        if (Build$VERSION.SDK_INT >= 34) {
            return Api34Impl.getAxisVelocity(velocityTracker, n, n2);
        }
        if (n == 0) {
            return velocityTracker.getXVelocity(n2);
        }
        if (n == 1) {
            return velocityTracker.getYVelocity(n2);
        }
        return 0.0f;
    }
    
    @Deprecated
    public static float getXVelocity(final VelocityTracker velocityTracker, final int n) {
        return velocityTracker.getXVelocity(n);
    }
    
    @Deprecated
    public static float getYVelocity(final VelocityTracker velocityTracker, final int n) {
        return velocityTracker.getYVelocity(n);
    }
    
    public static boolean isAxisSupported(final VelocityTracker velocityTracker, final int n) {
        if (Build$VERSION.SDK_INT >= 34) {
            return Api34Impl.isAxisSupported(velocityTracker, n);
        }
        boolean b = true;
        if (n != 0) {
            b = (n == 1 && b);
        }
        return b;
    }
    
    private static class Api34Impl
    {
        static float getAxisVelocity(final VelocityTracker velocityTracker, final int n) {
            return velocityTracker.getAxisVelocity(n);
        }
        
        static float getAxisVelocity(final VelocityTracker velocityTracker, final int n, final int n2) {
            return velocityTracker.getAxisVelocity(n, n2);
        }
        
        static boolean isAxisSupported(final VelocityTracker velocityTracker, final int n) {
            return velocityTracker.isAxisSupported(n);
        }
    }
    
    @Retention(RetentionPolicy.SOURCE)
    public @interface VelocityTrackableMotionEventAxis {
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.view.contentcapture;

import android.os.Bundle;
import android.view.ViewStructure;
import java.util.List;
import androidx.core.view.ViewStructureCompat;
import java.util.Objects;
import androidx.core.view.ViewCompat;
import androidx.core.view.autofill.AutofillIdCompat;
import android.os.Build$VERSION;
import android.view.autofill.AutofillId;
import android.view.contentcapture.ContentCaptureSession;
import android.view.View;

public class ContentCaptureSessionCompat
{
    private static final String KEY_VIEW_TREE_APPEARED = "TREAT_AS_VIEW_TREE_APPEARED";
    private static final String KEY_VIEW_TREE_APPEARING = "TREAT_AS_VIEW_TREE_APPEARING";
    private final View mView;
    private final Object mWrappedObj;
    
    private ContentCaptureSessionCompat(final ContentCaptureSession mWrappedObj, final View mView) {
        this.mWrappedObj = mWrappedObj;
        this.mView = mView;
    }
    
    public static ContentCaptureSessionCompat toContentCaptureSessionCompat(final ContentCaptureSession contentCaptureSession, final View view) {
        return new ContentCaptureSessionCompat(contentCaptureSession, view);
    }
    
    public AutofillId newAutofillId(final long n) {
        if (Build$VERSION.SDK_INT >= 29) {
            return Api29Impl.newAutofillId((ContentCaptureSession)this.mWrappedObj, Objects.requireNonNull(ViewCompat.getAutofillId(this.mView)).toAutofillId(), n);
        }
        return null;
    }
    
    public ViewStructureCompat newVirtualViewStructure(final AutofillId autofillId, final long n) {
        if (Build$VERSION.SDK_INT >= 29) {
            return ViewStructureCompat.toViewStructureCompat(Api29Impl.newVirtualViewStructure((ContentCaptureSession)this.mWrappedObj, autofillId, n));
        }
        return null;
    }
    
    public void notifyViewTextChanged(final AutofillId autofillId, final CharSequence charSequence) {
        if (Build$VERSION.SDK_INT >= 29) {
            Api29Impl.notifyViewTextChanged((ContentCaptureSession)this.mWrappedObj, autofillId, charSequence);
        }
    }
    
    public void notifyViewsAppeared(final List<ViewStructure> list) {
        if (Build$VERSION.SDK_INT >= 34) {
            Api34Impl.notifyViewsAppeared((ContentCaptureSession)this.mWrappedObj, list);
        }
        else if (Build$VERSION.SDK_INT >= 29) {
            final ViewStructure viewStructure = Api29Impl.newViewStructure((ContentCaptureSession)this.mWrappedObj, this.mView);
            Api23Impl.getExtras(viewStructure).putBoolean("TREAT_AS_VIEW_TREE_APPEARING", true);
            Api29Impl.notifyViewAppeared((ContentCaptureSession)this.mWrappedObj, viewStructure);
            for (int i = 0; i < list.size(); ++i) {
                Api29Impl.notifyViewAppeared((ContentCaptureSession)this.mWrappedObj, (ViewStructure)list.get(i));
            }
            final ViewStructure viewStructure2 = Api29Impl.newViewStructure((ContentCaptureSession)this.mWrappedObj, this.mView);
            Api23Impl.getExtras(viewStructure2).putBoolean("TREAT_AS_VIEW_TREE_APPEARED", true);
            Api29Impl.notifyViewAppeared((ContentCaptureSession)this.mWrappedObj, viewStructure2);
        }
    }
    
    public void notifyViewsDisappeared(final long[] array) {
        if (Build$VERSION.SDK_INT >= 34) {
            Api29Impl.notifyViewsDisappeared((ContentCaptureSession)this.mWrappedObj, Objects.requireNonNull(ViewCompat.getAutofillId(this.mView)).toAutofillId(), array);
        }
        else if (Build$VERSION.SDK_INT >= 29) {
            final ViewStructure viewStructure = Api29Impl.newViewStructure((ContentCaptureSession)this.mWrappedObj, this.mView);
            Api23Impl.getExtras(viewStructure).putBoolean("TREAT_AS_VIEW_TREE_APPEARING", true);
            Api29Impl.notifyViewAppeared((ContentCaptureSession)this.mWrappedObj, viewStructure);
            Api29Impl.notifyViewsDisappeared((ContentCaptureSession)this.mWrappedObj, Objects.requireNonNull(ViewCompat.getAutofillId(this.mView)).toAutofillId(), array);
            final ViewStructure viewStructure2 = Api29Impl.newViewStructure((ContentCaptureSession)this.mWrappedObj, this.mView);
            Api23Impl.getExtras(viewStructure2).putBoolean("TREAT_AS_VIEW_TREE_APPEARED", true);
            Api29Impl.notifyViewAppeared((ContentCaptureSession)this.mWrappedObj, viewStructure2);
        }
    }
    
    public ContentCaptureSession toContentCaptureSession() {
        return (ContentCaptureSession)this.mWrappedObj;
    }
    
    private static class Api23Impl
    {
        static Bundle getExtras(final ViewStructure viewStructure) {
            return viewStructure.getExtras();
        }
    }
    
    private static class Api29Impl
    {
        static AutofillId newAutofillId(final ContentCaptureSession contentCaptureSession, final AutofillId autofillId, final long n) {
            return contentCaptureSession.newAutofillId(autofillId, n);
        }
        
        static ViewStructure newViewStructure(final ContentCaptureSession contentCaptureSession, final View view) {
            return contentCaptureSession.newViewStructure(view);
        }
        
        static ViewStructure newVirtualViewStructure(final ContentCaptureSession contentCaptureSession, final AutofillId autofillId, final long n) {
            return contentCaptureSession.newVirtualViewStructure(autofillId, n);
        }
        
        static void notifyViewAppeared(final ContentCaptureSession contentCaptureSession, final ViewStructure viewStructure) {
            contentCaptureSession.notifyViewAppeared(viewStructure);
        }
        
        public static void notifyViewTextChanged(final ContentCaptureSession contentCaptureSession, final AutofillId autofillId, final CharSequence charSequence) {
            contentCaptureSession.notifyViewTextChanged(autofillId, charSequence);
        }
        
        static void notifyViewsDisappeared(final ContentCaptureSession contentCaptureSession, final AutofillId autofillId, final long[] array) {
            contentCaptureSession.notifyViewsDisappeared(autofillId, array);
        }
    }
    
    private static class Api34Impl
    {
        static void notifyViewsAppeared(final ContentCaptureSession contentCaptureSession, final List<ViewStructure> list) {
            contentCaptureSession.notifyViewsAppeared((List)list);
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.view;

import android.os.Build$VERSION;
import android.view.ScaleGestureDetector;

public final class ScaleGestureDetectorCompat
{
    private ScaleGestureDetectorCompat() {
    }
    
    public static boolean isQuickScaleEnabled(final ScaleGestureDetector scaleGestureDetector) {
        return Build$VERSION.SDK_INT >= 19 && Api19Impl.isQuickScaleEnabled(scaleGestureDetector);
    }
    
    @Deprecated
    public static boolean isQuickScaleEnabled(final Object o) {
        return isQuickScaleEnabled((ScaleGestureDetector)o);
    }
    
    public static void setQuickScaleEnabled(final ScaleGestureDetector scaleGestureDetector, final boolean b) {
        if (Build$VERSION.SDK_INT >= 19) {
            Api19Impl.setQuickScaleEnabled(scaleGestureDetector, b);
        }
    }
    
    @Deprecated
    public static void setQuickScaleEnabled(final Object o, final boolean b) {
        setQuickScaleEnabled((ScaleGestureDetector)o, b);
    }
    
    static class Api19Impl
    {
        private Api19Impl() {
        }
        
        static boolean isQuickScaleEnabled(final ScaleGestureDetector scaleGestureDetector) {
            return scaleGestureDetector.isQuickScaleEnabled();
        }
        
        static void setQuickScaleEnabled(final ScaleGestureDetector scaleGestureDetector, final boolean quickScaleEnabled) {
            scaleGestureDetector.setQuickScaleEnabled(quickScaleEnabled);
        }
    }
}

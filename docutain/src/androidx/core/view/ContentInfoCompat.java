// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.view;

import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;
import androidx.core.util.Preconditions;
import android.os.Build$VERSION;
import android.view.ContentInfo$Builder;
import java.util.Objects;
import android.net.Uri;
import android.os.Bundle;
import android.view.ContentInfo;
import java.util.ArrayList;
import android.util.Pair;
import androidx.core.util.Predicate;
import android.content.ClipData;
import android.content.ClipData$Item;
import java.util.List;
import android.content.ClipDescription;

public final class ContentInfoCompat
{
    public static final int FLAG_CONVERT_TO_PLAIN_TEXT = 1;
    public static final int SOURCE_APP = 0;
    public static final int SOURCE_AUTOFILL = 4;
    public static final int SOURCE_CLIPBOARD = 1;
    public static final int SOURCE_DRAG_AND_DROP = 3;
    public static final int SOURCE_INPUT_METHOD = 2;
    public static final int SOURCE_PROCESS_TEXT = 5;
    private final Compat mCompat;
    
    ContentInfoCompat(final Compat mCompat) {
        this.mCompat = mCompat;
    }
    
    static ClipData buildClipData(final ClipDescription clipDescription, final List<ClipData$Item> list) {
        final ClipData clipData = new ClipData(new ClipDescription(clipDescription), (ClipData$Item)list.get(0));
        for (int i = 1; i < list.size(); ++i) {
            clipData.addItem((ClipData$Item)list.get(i));
        }
        return clipData;
    }
    
    static String flagsToString(final int i) {
        if ((i & 0x1) != 0x0) {
            return "FLAG_CONVERT_TO_PLAIN_TEXT";
        }
        return String.valueOf(i);
    }
    
    static Pair<ClipData, ClipData> partition(final ClipData clipData, final Predicate<ClipData$Item> predicate) {
        int i = 0;
        List<ClipData$Item> list = null;
        List<ClipData$Item> list2 = null;
        while (i < clipData.getItemCount()) {
            final ClipData$Item item = clipData.getItemAt(i);
            if (predicate.test(item)) {
                ArrayList<ClipData$Item> list3;
                if ((list3 = (ArrayList<ClipData$Item>)list) == null) {
                    list3 = new ArrayList<ClipData$Item>();
                }
                list3.add(item);
                list = list3;
            }
            else {
                ArrayList<ClipData$Item> list4;
                if ((list4 = (ArrayList<ClipData$Item>)list2) == null) {
                    list4 = new ArrayList<ClipData$Item>();
                }
                list4.add(item);
                list2 = list4;
            }
            ++i;
        }
        if (list == null) {
            return (Pair<ClipData, ClipData>)Pair.create((Object)null, (Object)clipData);
        }
        if (list2 == null) {
            return (Pair<ClipData, ClipData>)Pair.create((Object)clipData, (Object)null);
        }
        return (Pair<ClipData, ClipData>)Pair.create((Object)buildClipData(clipData.getDescription(), list), (Object)buildClipData(clipData.getDescription(), list2));
    }
    
    public static Pair<ContentInfo, ContentInfo> partition(final ContentInfo contentInfo, final java.util.function.Predicate<ClipData$Item> predicate) {
        return Api31Impl.partition(contentInfo, predicate);
    }
    
    static String sourceToString(final int i) {
        if (i == 0) {
            return "SOURCE_APP";
        }
        if (i == 1) {
            return "SOURCE_CLIPBOARD";
        }
        if (i == 2) {
            return "SOURCE_INPUT_METHOD";
        }
        if (i == 3) {
            return "SOURCE_DRAG_AND_DROP";
        }
        if (i == 4) {
            return "SOURCE_AUTOFILL";
        }
        if (i != 5) {
            return String.valueOf(i);
        }
        return "SOURCE_PROCESS_TEXT";
    }
    
    public static ContentInfoCompat toContentInfoCompat(final ContentInfo contentInfo) {
        return new ContentInfoCompat((Compat)new Compat31Impl(contentInfo));
    }
    
    public ClipData getClip() {
        return this.mCompat.getClip();
    }
    
    public Bundle getExtras() {
        return this.mCompat.getExtras();
    }
    
    public int getFlags() {
        return this.mCompat.getFlags();
    }
    
    public Uri getLinkUri() {
        return this.mCompat.getLinkUri();
    }
    
    public int getSource() {
        return this.mCompat.getSource();
    }
    
    public Pair<ContentInfoCompat, ContentInfoCompat> partition(final Predicate<ClipData$Item> predicate) {
        final ClipData clip = this.mCompat.getClip();
        final int itemCount = clip.getItemCount();
        Object o = null;
        if (itemCount == 1) {
            final boolean test = predicate.test(clip.getItemAt(0));
            ContentInfoCompat contentInfoCompat;
            if (test) {
                contentInfoCompat = this;
            }
            else {
                contentInfoCompat = null;
            }
            if (!test) {
                o = this;
            }
            return (Pair<ContentInfoCompat, ContentInfoCompat>)Pair.create((Object)contentInfoCompat, o);
        }
        final Pair<ClipData, ClipData> partition = partition(clip, predicate);
        if (partition.first == null) {
            return (Pair<ContentInfoCompat, ContentInfoCompat>)Pair.create((Object)null, (Object)this);
        }
        if (partition.second == null) {
            return (Pair<ContentInfoCompat, ContentInfoCompat>)Pair.create((Object)this, (Object)null);
        }
        return (Pair<ContentInfoCompat, ContentInfoCompat>)Pair.create((Object)new Builder(this).setClip((ClipData)partition.first).build(), (Object)new Builder(this).setClip((ClipData)partition.second).build());
    }
    
    public ContentInfo toContentInfo() {
        return Objects.requireNonNull(this.mCompat.getWrapped());
    }
    
    @Override
    public String toString() {
        return this.mCompat.toString();
    }
    
    private static final class Api31Impl
    {
        public static Pair<ContentInfo, ContentInfo> partition(ContentInfo contentInfo, final java.util.function.Predicate<ClipData$Item> obj) {
            final ClipData clip = contentInfo.getClip();
            if (clip.getItemCount() == 1) {
                final boolean test = obj.test(clip.getItemAt(0));
                ContentInfo contentInfo2;
                if (test) {
                    contentInfo2 = contentInfo;
                }
                else {
                    contentInfo2 = null;
                }
                if (test) {
                    contentInfo = null;
                }
                return (Pair<ContentInfo, ContentInfo>)Pair.create((Object)contentInfo2, (Object)contentInfo);
            }
            Objects.requireNonNull(obj);
            final Pair<ClipData, ClipData> partition = ContentInfoCompat.partition(clip, new ContentInfoCompat$Api31Impl$$ExternalSyntheticLambda0(obj));
            if (partition.first == null) {
                return (Pair<ContentInfo, ContentInfo>)Pair.create((Object)null, (Object)contentInfo);
            }
            if (partition.second == null) {
                return (Pair<ContentInfo, ContentInfo>)Pair.create((Object)contentInfo, (Object)null);
            }
            return (Pair<ContentInfo, ContentInfo>)Pair.create((Object)new ContentInfo$Builder(contentInfo).setClip((ClipData)partition.first).build(), (Object)new ContentInfo$Builder(contentInfo).setClip((ClipData)partition.second).build());
        }
    }
    
    public static final class Builder
    {
        private final BuilderCompat mBuilderCompat;
        
        public Builder(final ClipData clipData, final int n) {
            if (Build$VERSION.SDK_INT >= 31) {
                this.mBuilderCompat = new BuilderCompat31Impl(clipData, n);
            }
            else {
                this.mBuilderCompat = new BuilderCompatImpl(clipData, n);
            }
        }
        
        public Builder(final ContentInfoCompat contentInfoCompat) {
            if (Build$VERSION.SDK_INT >= 31) {
                this.mBuilderCompat = new BuilderCompat31Impl(contentInfoCompat);
            }
            else {
                this.mBuilderCompat = new BuilderCompatImpl(contentInfoCompat);
            }
        }
        
        public ContentInfoCompat build() {
            return this.mBuilderCompat.build();
        }
        
        public Builder setClip(final ClipData clip) {
            this.mBuilderCompat.setClip(clip);
            return this;
        }
        
        public Builder setExtras(final Bundle extras) {
            this.mBuilderCompat.setExtras(extras);
            return this;
        }
        
        public Builder setFlags(final int flags) {
            this.mBuilderCompat.setFlags(flags);
            return this;
        }
        
        public Builder setLinkUri(final Uri linkUri) {
            this.mBuilderCompat.setLinkUri(linkUri);
            return this;
        }
        
        public Builder setSource(final int source) {
            this.mBuilderCompat.setSource(source);
            return this;
        }
    }
    
    private interface BuilderCompat
    {
        ContentInfoCompat build();
        
        void setClip(final ClipData p0);
        
        void setExtras(final Bundle p0);
        
        void setFlags(final int p0);
        
        void setLinkUri(final Uri p0);
        
        void setSource(final int p0);
    }
    
    private static final class BuilderCompat31Impl implements BuilderCompat
    {
        private final ContentInfo$Builder mPlatformBuilder;
        
        BuilderCompat31Impl(final ClipData clipData, final int n) {
            this.mPlatformBuilder = new ContentInfo$Builder(clipData, n);
        }
        
        BuilderCompat31Impl(final ContentInfoCompat contentInfoCompat) {
            this.mPlatformBuilder = new ContentInfo$Builder(contentInfoCompat.toContentInfo());
        }
        
        @Override
        public ContentInfoCompat build() {
            return new ContentInfoCompat((Compat)new Compat31Impl(this.mPlatformBuilder.build()));
        }
        
        @Override
        public void setClip(final ClipData clip) {
            this.mPlatformBuilder.setClip(clip);
        }
        
        @Override
        public void setExtras(final Bundle extras) {
            this.mPlatformBuilder.setExtras(extras);
        }
        
        @Override
        public void setFlags(final int flags) {
            this.mPlatformBuilder.setFlags(flags);
        }
        
        @Override
        public void setLinkUri(final Uri linkUri) {
            this.mPlatformBuilder.setLinkUri(linkUri);
        }
        
        @Override
        public void setSource(final int source) {
            this.mPlatformBuilder.setSource(source);
        }
    }
    
    private static final class BuilderCompatImpl implements BuilderCompat
    {
        ClipData mClip;
        Bundle mExtras;
        int mFlags;
        Uri mLinkUri;
        int mSource;
        
        BuilderCompatImpl(final ClipData mClip, final int mSource) {
            this.mClip = mClip;
            this.mSource = mSource;
        }
        
        BuilderCompatImpl(final ContentInfoCompat contentInfoCompat) {
            this.mClip = contentInfoCompat.getClip();
            this.mSource = contentInfoCompat.getSource();
            this.mFlags = contentInfoCompat.getFlags();
            this.mLinkUri = contentInfoCompat.getLinkUri();
            this.mExtras = contentInfoCompat.getExtras();
        }
        
        @Override
        public ContentInfoCompat build() {
            return new ContentInfoCompat((Compat)new CompatImpl(this));
        }
        
        @Override
        public void setClip(final ClipData mClip) {
            this.mClip = mClip;
        }
        
        @Override
        public void setExtras(final Bundle mExtras) {
            this.mExtras = mExtras;
        }
        
        @Override
        public void setFlags(final int mFlags) {
            this.mFlags = mFlags;
        }
        
        @Override
        public void setLinkUri(final Uri mLinkUri) {
            this.mLinkUri = mLinkUri;
        }
        
        @Override
        public void setSource(final int mSource) {
            this.mSource = mSource;
        }
    }
    
    private interface Compat
    {
        ClipData getClip();
        
        Bundle getExtras();
        
        int getFlags();
        
        Uri getLinkUri();
        
        int getSource();
        
        ContentInfo getWrapped();
    }
    
    private static final class Compat31Impl implements Compat
    {
        private final ContentInfo mWrapped;
        
        Compat31Impl(final ContentInfo contentInfo) {
            this.mWrapped = Preconditions.checkNotNull(contentInfo);
        }
        
        @Override
        public ClipData getClip() {
            return this.mWrapped.getClip();
        }
        
        @Override
        public Bundle getExtras() {
            return this.mWrapped.getExtras();
        }
        
        @Override
        public int getFlags() {
            return this.mWrapped.getFlags();
        }
        
        @Override
        public Uri getLinkUri() {
            return this.mWrapped.getLinkUri();
        }
        
        @Override
        public int getSource() {
            return this.mWrapped.getSource();
        }
        
        @Override
        public ContentInfo getWrapped() {
            return this.mWrapped;
        }
        
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder();
            sb.append("ContentInfoCompat{");
            sb.append(this.mWrapped);
            sb.append("}");
            return sb.toString();
        }
    }
    
    private static final class CompatImpl implements Compat
    {
        private final ClipData mClip;
        private final Bundle mExtras;
        private final int mFlags;
        private final Uri mLinkUri;
        private final int mSource;
        
        CompatImpl(final BuilderCompatImpl builderCompatImpl) {
            this.mClip = Preconditions.checkNotNull(builderCompatImpl.mClip);
            this.mSource = Preconditions.checkArgumentInRange(builderCompatImpl.mSource, 0, 5, "source");
            this.mFlags = Preconditions.checkFlagsArgument(builderCompatImpl.mFlags, 1);
            this.mLinkUri = builderCompatImpl.mLinkUri;
            this.mExtras = builderCompatImpl.mExtras;
        }
        
        @Override
        public ClipData getClip() {
            return this.mClip;
        }
        
        @Override
        public Bundle getExtras() {
            return this.mExtras;
        }
        
        @Override
        public int getFlags() {
            return this.mFlags;
        }
        
        @Override
        public Uri getLinkUri() {
            return this.mLinkUri;
        }
        
        @Override
        public int getSource() {
            return this.mSource;
        }
        
        @Override
        public ContentInfo getWrapped() {
            return null;
        }
        
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder();
            sb.append("ContentInfoCompat{clip=");
            sb.append(this.mClip.getDescription());
            sb.append(", source=");
            sb.append(ContentInfoCompat.sourceToString(this.mSource));
            sb.append(", flags=");
            sb.append(ContentInfoCompat.flagsToString(this.mFlags));
            final Uri mLinkUri = this.mLinkUri;
            final String s = "";
            String string;
            if (mLinkUri == null) {
                string = "";
            }
            else {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append(", hasLinkUri(");
                sb2.append(this.mLinkUri.toString().length());
                sb2.append(")");
                string = sb2.toString();
            }
            sb.append(string);
            String str;
            if (this.mExtras == null) {
                str = s;
            }
            else {
                str = ", hasExtras";
            }
            sb.append(str);
            sb.append("}");
            return sb.toString();
        }
    }
    
    @Retention(RetentionPolicy.SOURCE)
    public @interface Flags {
    }
    
    @Retention(RetentionPolicy.SOURCE)
    public @interface Source {
    }
}

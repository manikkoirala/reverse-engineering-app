// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.view;

import android.os.Build$VERSION;
import android.view.DragEvent;
import android.app.Activity;
import android.view.DragAndDropPermissions;

public final class DragAndDropPermissionsCompat
{
    private final DragAndDropPermissions mDragAndDropPermissions;
    
    private DragAndDropPermissionsCompat(final DragAndDropPermissions mDragAndDropPermissions) {
        this.mDragAndDropPermissions = mDragAndDropPermissions;
    }
    
    public static DragAndDropPermissionsCompat request(final Activity activity, final DragEvent dragEvent) {
        if (Build$VERSION.SDK_INT >= 24) {
            final DragAndDropPermissions requestDragAndDropPermissions = Api24Impl.requestDragAndDropPermissions(activity, dragEvent);
            if (requestDragAndDropPermissions != null) {
                return new DragAndDropPermissionsCompat(requestDragAndDropPermissions);
            }
        }
        return null;
    }
    
    public void release() {
        if (Build$VERSION.SDK_INT >= 24) {
            Api24Impl.release(this.mDragAndDropPermissions);
        }
    }
    
    static class Api24Impl
    {
        private Api24Impl() {
        }
        
        static void release(final DragAndDropPermissions dragAndDropPermissions) {
            dragAndDropPermissions.release();
        }
        
        static DragAndDropPermissions requestDragAndDropPermissions(final Activity activity, final DragEvent dragEvent) {
            return activity.requestDragAndDropPermissions(dragEvent);
        }
    }
}

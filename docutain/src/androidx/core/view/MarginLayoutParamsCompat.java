// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.view;

import android.os.Build$VERSION;
import android.view.ViewGroup$MarginLayoutParams;

public final class MarginLayoutParamsCompat
{
    private MarginLayoutParamsCompat() {
    }
    
    public static int getLayoutDirection(final ViewGroup$MarginLayoutParams viewGroup$MarginLayoutParams) {
        final int sdk_INT = Build$VERSION.SDK_INT;
        final int n = 0;
        int layoutDirection;
        if (sdk_INT >= 17) {
            layoutDirection = Api17Impl.getLayoutDirection(viewGroup$MarginLayoutParams);
        }
        else {
            layoutDirection = 0;
        }
        if (layoutDirection != 0 && layoutDirection != 1) {
            layoutDirection = n;
        }
        return layoutDirection;
    }
    
    public static int getMarginEnd(final ViewGroup$MarginLayoutParams viewGroup$MarginLayoutParams) {
        if (Build$VERSION.SDK_INT >= 17) {
            return Api17Impl.getMarginEnd(viewGroup$MarginLayoutParams);
        }
        return viewGroup$MarginLayoutParams.rightMargin;
    }
    
    public static int getMarginStart(final ViewGroup$MarginLayoutParams viewGroup$MarginLayoutParams) {
        if (Build$VERSION.SDK_INT >= 17) {
            return Api17Impl.getMarginStart(viewGroup$MarginLayoutParams);
        }
        return viewGroup$MarginLayoutParams.leftMargin;
    }
    
    public static boolean isMarginRelative(final ViewGroup$MarginLayoutParams viewGroup$MarginLayoutParams) {
        return Build$VERSION.SDK_INT >= 17 && Api17Impl.isMarginRelative(viewGroup$MarginLayoutParams);
    }
    
    public static void resolveLayoutDirection(final ViewGroup$MarginLayoutParams viewGroup$MarginLayoutParams, final int n) {
        if (Build$VERSION.SDK_INT >= 17) {
            Api17Impl.resolveLayoutDirection(viewGroup$MarginLayoutParams, n);
        }
    }
    
    public static void setLayoutDirection(final ViewGroup$MarginLayoutParams viewGroup$MarginLayoutParams, final int n) {
        if (Build$VERSION.SDK_INT >= 17) {
            Api17Impl.setLayoutDirection(viewGroup$MarginLayoutParams, n);
        }
    }
    
    public static void setMarginEnd(final ViewGroup$MarginLayoutParams viewGroup$MarginLayoutParams, final int rightMargin) {
        if (Build$VERSION.SDK_INT >= 17) {
            Api17Impl.setMarginEnd(viewGroup$MarginLayoutParams, rightMargin);
        }
        else {
            viewGroup$MarginLayoutParams.rightMargin = rightMargin;
        }
    }
    
    public static void setMarginStart(final ViewGroup$MarginLayoutParams viewGroup$MarginLayoutParams, final int leftMargin) {
        if (Build$VERSION.SDK_INT >= 17) {
            Api17Impl.setMarginStart(viewGroup$MarginLayoutParams, leftMargin);
        }
        else {
            viewGroup$MarginLayoutParams.leftMargin = leftMargin;
        }
    }
    
    static class Api17Impl
    {
        private Api17Impl() {
        }
        
        static int getLayoutDirection(final ViewGroup$MarginLayoutParams viewGroup$MarginLayoutParams) {
            return viewGroup$MarginLayoutParams.getLayoutDirection();
        }
        
        static int getMarginEnd(final ViewGroup$MarginLayoutParams viewGroup$MarginLayoutParams) {
            return viewGroup$MarginLayoutParams.getMarginEnd();
        }
        
        static int getMarginStart(final ViewGroup$MarginLayoutParams viewGroup$MarginLayoutParams) {
            return viewGroup$MarginLayoutParams.getMarginStart();
        }
        
        static boolean isMarginRelative(final ViewGroup$MarginLayoutParams viewGroup$MarginLayoutParams) {
            return viewGroup$MarginLayoutParams.isMarginRelative();
        }
        
        static void resolveLayoutDirection(final ViewGroup$MarginLayoutParams viewGroup$MarginLayoutParams, final int n) {
            viewGroup$MarginLayoutParams.resolveLayoutDirection(n);
        }
        
        static void setLayoutDirection(final ViewGroup$MarginLayoutParams viewGroup$MarginLayoutParams, final int layoutDirection) {
            viewGroup$MarginLayoutParams.setLayoutDirection(layoutDirection);
        }
        
        static void setMarginEnd(final ViewGroup$MarginLayoutParams viewGroup$MarginLayoutParams, final int marginEnd) {
            viewGroup$MarginLayoutParams.setMarginEnd(marginEnd);
        }
        
        static void setMarginStart(final ViewGroup$MarginLayoutParams viewGroup$MarginLayoutParams, final int marginStart) {
            viewGroup$MarginLayoutParams.setMarginStart(marginStart);
        }
    }
}

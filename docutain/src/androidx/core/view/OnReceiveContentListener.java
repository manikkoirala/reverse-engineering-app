// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.view;

import android.view.View;

public interface OnReceiveContentListener
{
    ContentInfoCompat onReceiveContent(final View p0, final ContentInfoCompat p1);
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.view;

import androidx.core.util.Preconditions;
import android.view.Display$Mode;
import android.text.TextUtils;
import android.app.UiModeManager;
import android.os.Build;
import android.os.Build$VERSION;
import android.graphics.Point;
import android.view.Display;
import android.content.Context;

public final class DisplayCompat
{
    private static final int DISPLAY_SIZE_4K_HEIGHT = 2160;
    private static final int DISPLAY_SIZE_4K_WIDTH = 3840;
    
    private DisplayCompat() {
    }
    
    static Point getCurrentDisplaySizeFromWorkarounds(final Context context, final Display display) {
        Point point;
        if (Build$VERSION.SDK_INT < 28) {
            point = parsePhysicalDisplaySizeFromSystemProperties("sys.display-size", display);
        }
        else {
            point = parsePhysicalDisplaySizeFromSystemProperties("vendor.display-size", display);
        }
        if (point != null) {
            return point;
        }
        final boolean sonyBravia4kTv = isSonyBravia4kTv(context);
        Point point2 = null;
        if (sonyBravia4kTv) {
            point2 = point2;
            if (isCurrentModeTheLargestMode(display)) {
                point2 = new Point(3840, 2160);
            }
        }
        return point2;
    }
    
    private static Point getDisplaySize(final Context context, final Display display) {
        final Point currentDisplaySizeFromWorkarounds = getCurrentDisplaySizeFromWorkarounds(context, display);
        if (currentDisplaySizeFromWorkarounds != null) {
            return currentDisplaySizeFromWorkarounds;
        }
        final Point point = new Point();
        if (Build$VERSION.SDK_INT >= 17) {
            Api17Impl.getRealSize(display, point);
        }
        else {
            display.getSize(point);
        }
        return point;
    }
    
    public static ModeCompat getMode(final Context context, final Display display) {
        if (Build$VERSION.SDK_INT >= 23) {
            return Api23Impl.getMode(context, display);
        }
        return new ModeCompat(getDisplaySize(context, display));
    }
    
    public static ModeCompat[] getSupportedModes(final Context context, final Display display) {
        if (Build$VERSION.SDK_INT >= 23) {
            return Api23Impl.getSupportedModes(context, display);
        }
        return new ModeCompat[] { getMode(context, display) };
    }
    
    private static String getSystemProperty(String s) {
        try {
            final Class<?> forName = Class.forName("android.os.SystemProperties");
            s = (String)forName.getMethod("get", String.class).invoke(forName, s);
            return s;
        }
        catch (final Exception ex) {
            return null;
        }
    }
    
    static boolean isCurrentModeTheLargestMode(final Display display) {
        return Build$VERSION.SDK_INT < 23 || Api23Impl.isCurrentModeTheLargestMode(display);
    }
    
    private static boolean isSonyBravia4kTv(final Context context) {
        return isTv(context) && "Sony".equals(Build.MANUFACTURER) && Build.MODEL.startsWith("BRAVIA") && context.getPackageManager().hasSystemFeature("com.sony.dtv.hardware.panel.qfhd");
    }
    
    private static boolean isTv(final Context context) {
        final UiModeManager uiModeManager = (UiModeManager)context.getSystemService("uimode");
        return uiModeManager != null && uiModeManager.getCurrentModeType() == 4;
    }
    
    private static Point parseDisplaySize(final String s) throws NumberFormatException {
        final String[] split = s.trim().split("x", -1);
        if (split.length == 2) {
            final int int1 = Integer.parseInt(split[0]);
            final int int2 = Integer.parseInt(split[1]);
            if (int1 > 0 && int2 > 0) {
                return new Point(int1, int2);
            }
        }
        throw new NumberFormatException();
    }
    
    private static Point parsePhysicalDisplaySizeFromSystemProperties(String systemProperty, final Display display) {
        if (display.getDisplayId() != 0) {
            return null;
        }
        systemProperty = getSystemProperty(systemProperty);
        Label_0035: {
            if (TextUtils.isEmpty((CharSequence)systemProperty)) {
                break Label_0035;
            }
            if (systemProperty == null) {
                break Label_0035;
            }
            try {
                return parseDisplaySize(systemProperty);
                return null;
            }
            catch (final NumberFormatException ex) {
                return null;
            }
        }
    }
    
    static class Api17Impl
    {
        private Api17Impl() {
        }
        
        static void getRealSize(final Display display, final Point point) {
            display.getRealSize(point);
        }
    }
    
    static class Api23Impl
    {
        private Api23Impl() {
        }
        
        static ModeCompat getMode(final Context context, final Display display) {
            final Display$Mode mode = display.getMode();
            final Point currentDisplaySizeFromWorkarounds = DisplayCompat.getCurrentDisplaySizeFromWorkarounds(context, display);
            ModeCompat modeCompat;
            if (currentDisplaySizeFromWorkarounds != null && !physicalSizeEquals(mode, currentDisplaySizeFromWorkarounds)) {
                modeCompat = new ModeCompat(mode, currentDisplaySizeFromWorkarounds);
            }
            else {
                modeCompat = new ModeCompat(mode, true);
            }
            return modeCompat;
        }
        
        public static ModeCompat[] getSupportedModes(final Context context, final Display display) {
            final Display$Mode[] supportedModes = display.getSupportedModes();
            final ModeCompat[] array = new ModeCompat[supportedModes.length];
            final Display$Mode mode = display.getMode();
            final Point currentDisplaySizeFromWorkarounds = DisplayCompat.getCurrentDisplaySizeFromWorkarounds(context, display);
            int i;
            final int n = i = 0;
            if (currentDisplaySizeFromWorkarounds != null) {
                if (!physicalSizeEquals(mode, currentDisplaySizeFromWorkarounds)) {
                    for (int j = 0; j < supportedModes.length; ++j) {
                        ModeCompat modeCompat;
                        if (physicalSizeEquals(supportedModes[j], mode)) {
                            modeCompat = new ModeCompat(supportedModes[j], currentDisplaySizeFromWorkarounds);
                        }
                        else {
                            modeCompat = new ModeCompat(supportedModes[j], false);
                        }
                        array[j] = modeCompat;
                    }
                    return array;
                }
                i = n;
            }
            while (i < supportedModes.length) {
                array[i] = new ModeCompat(supportedModes[i], physicalSizeEquals(supportedModes[i], mode));
                ++i;
            }
            return array;
        }
        
        static boolean isCurrentModeTheLargestMode(final Display display) {
            final Display$Mode mode = display.getMode();
            for (final Display$Mode display$Mode : display.getSupportedModes()) {
                if (mode.getPhysicalHeight() < display$Mode.getPhysicalHeight() || mode.getPhysicalWidth() < display$Mode.getPhysicalWidth()) {
                    return false;
                }
            }
            return true;
        }
        
        static boolean physicalSizeEquals(final Display$Mode display$Mode, final Point point) {
            return (display$Mode.getPhysicalWidth() == point.x && display$Mode.getPhysicalHeight() == point.y) || (display$Mode.getPhysicalWidth() == point.y && display$Mode.getPhysicalHeight() == point.x);
        }
        
        static boolean physicalSizeEquals(final Display$Mode display$Mode, final Display$Mode display$Mode2) {
            return display$Mode.getPhysicalWidth() == display$Mode2.getPhysicalWidth() && display$Mode.getPhysicalHeight() == display$Mode2.getPhysicalHeight();
        }
    }
    
    public static final class ModeCompat
    {
        private final boolean mIsNative;
        private final Display$Mode mMode;
        private final Point mPhysicalSize;
        
        ModeCompat(final Point mPhysicalSize) {
            Preconditions.checkNotNull(mPhysicalSize, "physicalSize == null");
            this.mPhysicalSize = mPhysicalSize;
            this.mMode = null;
            this.mIsNative = true;
        }
        
        ModeCompat(final Display$Mode mMode, final Point mPhysicalSize) {
            Preconditions.checkNotNull(mMode, "mode == null, can't wrap a null reference");
            Preconditions.checkNotNull(mPhysicalSize, "physicalSize == null");
            this.mPhysicalSize = mPhysicalSize;
            this.mMode = mMode;
            this.mIsNative = true;
        }
        
        ModeCompat(final Display$Mode mMode, final boolean mIsNative) {
            Preconditions.checkNotNull(mMode, "mode == null, can't wrap a null reference");
            this.mPhysicalSize = new Point(Api23Impl.getPhysicalWidth(mMode), Api23Impl.getPhysicalHeight(mMode));
            this.mMode = mMode;
            this.mIsNative = mIsNative;
        }
        
        public int getPhysicalHeight() {
            return this.mPhysicalSize.y;
        }
        
        public int getPhysicalWidth() {
            return this.mPhysicalSize.x;
        }
        
        @Deprecated
        public boolean isNative() {
            return this.mIsNative;
        }
        
        public Display$Mode toMode() {
            return this.mMode;
        }
        
        static class Api23Impl
        {
            private Api23Impl() {
            }
            
            static int getPhysicalHeight(final Display$Mode display$Mode) {
                return display$Mode.getPhysicalHeight();
            }
            
            static int getPhysicalWidth(final Display$Mode display$Mode) {
                return display$Mode.getPhysicalWidth();
            }
        }
    }
}

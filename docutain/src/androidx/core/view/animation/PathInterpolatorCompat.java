// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.view.animation;

import android.view.animation.PathInterpolator;
import android.graphics.Path;
import android.os.Build$VERSION;
import android.view.animation.Interpolator;

public final class PathInterpolatorCompat
{
    private PathInterpolatorCompat() {
    }
    
    public static Interpolator create(final float n, final float n2) {
        if (Build$VERSION.SDK_INT >= 21) {
            return Api21Impl.createPathInterpolator(n, n2);
        }
        return (Interpolator)new PathInterpolatorApi14(n, n2);
    }
    
    public static Interpolator create(final float n, final float n2, final float n3, final float n4) {
        if (Build$VERSION.SDK_INT >= 21) {
            return Api21Impl.createPathInterpolator(n, n2, n3, n4);
        }
        return (Interpolator)new PathInterpolatorApi14(n, n2, n3, n4);
    }
    
    public static Interpolator create(final Path path) {
        if (Build$VERSION.SDK_INT >= 21) {
            return Api21Impl.createPathInterpolator(path);
        }
        return (Interpolator)new PathInterpolatorApi14(path);
    }
    
    static class Api21Impl
    {
        private Api21Impl() {
        }
        
        static Interpolator createPathInterpolator(final float n, final float n2) {
            return (Interpolator)new PathInterpolator(n, n2);
        }
        
        static Interpolator createPathInterpolator(final float n, final float n2, final float n3, final float n4) {
            return (Interpolator)new PathInterpolator(n, n2, n3, n4);
        }
        
        static Interpolator createPathInterpolator(final Path path) {
            return (Interpolator)new PathInterpolator(path);
        }
    }
}

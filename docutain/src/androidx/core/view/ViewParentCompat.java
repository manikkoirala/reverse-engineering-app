// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.view;

import android.view.accessibility.AccessibilityEvent;
import android.util.Log;
import android.os.Build$VERSION;
import android.view.View;
import android.view.ViewParent;

public final class ViewParentCompat
{
    private static final String TAG = "ViewParentCompat";
    private static int[] sTempNestedScrollConsumed;
    
    private ViewParentCompat() {
    }
    
    private static int[] getTempNestedScrollConsumed() {
        final int[] sTempNestedScrollConsumed = ViewParentCompat.sTempNestedScrollConsumed;
        if (sTempNestedScrollConsumed == null) {
            ViewParentCompat.sTempNestedScrollConsumed = new int[2];
        }
        else {
            sTempNestedScrollConsumed[1] = (sTempNestedScrollConsumed[0] = 0);
        }
        return ViewParentCompat.sTempNestedScrollConsumed;
    }
    
    public static void notifySubtreeAccessibilityStateChanged(final ViewParent viewParent, final View view, final View view2, final int n) {
        if (Build$VERSION.SDK_INT >= 19) {
            Api19Impl.notifySubtreeAccessibilityStateChanged(viewParent, view, view2, n);
        }
    }
    
    public static boolean onNestedFling(final ViewParent obj, final View view, final float n, final float n2, final boolean b) {
        if (Build$VERSION.SDK_INT >= 21) {
            try {
                return Api21Impl.onNestedFling(obj, view, n, n2, b);
            }
            catch (final AbstractMethodError abstractMethodError) {
                final StringBuilder sb = new StringBuilder();
                sb.append("ViewParent ");
                sb.append(obj);
                sb.append(" does not implement interface method onNestedFling");
                Log.e("ViewParentCompat", sb.toString(), (Throwable)abstractMethodError);
                return false;
            }
        }
        if (obj instanceof NestedScrollingParent) {
            return ((NestedScrollingParent)obj).onNestedFling(view, n, n2, b);
        }
        return false;
    }
    
    public static boolean onNestedPreFling(final ViewParent obj, final View view, final float n, final float n2) {
        if (Build$VERSION.SDK_INT >= 21) {
            try {
                return Api21Impl.onNestedPreFling(obj, view, n, n2);
            }
            catch (final AbstractMethodError abstractMethodError) {
                final StringBuilder sb = new StringBuilder();
                sb.append("ViewParent ");
                sb.append(obj);
                sb.append(" does not implement interface method onNestedPreFling");
                Log.e("ViewParentCompat", sb.toString(), (Throwable)abstractMethodError);
                return false;
            }
        }
        if (obj instanceof NestedScrollingParent) {
            return ((NestedScrollingParent)obj).onNestedPreFling(view, n, n2);
        }
        return false;
    }
    
    public static void onNestedPreScroll(final ViewParent viewParent, final View view, final int n, final int n2, final int[] array) {
        onNestedPreScroll(viewParent, view, n, n2, array, 0);
    }
    
    public static void onNestedPreScroll(final ViewParent obj, final View view, final int n, final int n2, final int[] array, final int n3) {
        if (obj instanceof NestedScrollingParent2) {
            ((NestedScrollingParent2)obj).onNestedPreScroll(view, n, n2, array, n3);
        }
        else if (n3 == 0) {
            if (Build$VERSION.SDK_INT >= 21) {
                try {
                    Api21Impl.onNestedPreScroll(obj, view, n, n2, array);
                }
                catch (final AbstractMethodError abstractMethodError) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("ViewParent ");
                    sb.append(obj);
                    sb.append(" does not implement interface method onNestedPreScroll");
                    Log.e("ViewParentCompat", sb.toString(), (Throwable)abstractMethodError);
                }
            }
            else if (obj instanceof NestedScrollingParent) {
                ((NestedScrollingParent)obj).onNestedPreScroll(view, n, n2, array);
            }
        }
    }
    
    public static void onNestedScroll(final ViewParent viewParent, final View view, final int n, final int n2, final int n3, final int n4) {
        onNestedScroll(viewParent, view, n, n2, n3, n4, 0, getTempNestedScrollConsumed());
    }
    
    public static void onNestedScroll(final ViewParent viewParent, final View view, final int n, final int n2, final int n3, final int n4, final int n5) {
        onNestedScroll(viewParent, view, n, n2, n3, n4, n5, getTempNestedScrollConsumed());
    }
    
    public static void onNestedScroll(final ViewParent obj, final View view, final int n, final int n2, final int n3, final int n4, final int n5, final int[] array) {
        if (obj instanceof NestedScrollingParent3) {
            ((NestedScrollingParent3)obj).onNestedScroll(view, n, n2, n3, n4, n5, array);
        }
        else {
            array[0] += n3;
            array[1] += n4;
            if (obj instanceof NestedScrollingParent2) {
                ((NestedScrollingParent2)obj).onNestedScroll(view, n, n2, n3, n4, n5);
            }
            else if (n5 == 0) {
                if (Build$VERSION.SDK_INT >= 21) {
                    try {
                        Api21Impl.onNestedScroll(obj, view, n, n2, n3, n4);
                    }
                    catch (final AbstractMethodError abstractMethodError) {
                        final StringBuilder sb = new StringBuilder();
                        sb.append("ViewParent ");
                        sb.append(obj);
                        sb.append(" does not implement interface method onNestedScroll");
                        Log.e("ViewParentCompat", sb.toString(), (Throwable)abstractMethodError);
                    }
                }
                else if (obj instanceof NestedScrollingParent) {
                    ((NestedScrollingParent)obj).onNestedScroll(view, n, n2, n3, n4);
                }
            }
        }
    }
    
    public static void onNestedScrollAccepted(final ViewParent viewParent, final View view, final View view2, final int n) {
        onNestedScrollAccepted(viewParent, view, view2, n, 0);
    }
    
    public static void onNestedScrollAccepted(final ViewParent obj, final View view, final View view2, final int n, final int n2) {
        if (obj instanceof NestedScrollingParent2) {
            ((NestedScrollingParent2)obj).onNestedScrollAccepted(view, view2, n, n2);
        }
        else if (n2 == 0) {
            if (Build$VERSION.SDK_INT >= 21) {
                try {
                    Api21Impl.onNestedScrollAccepted(obj, view, view2, n);
                }
                catch (final AbstractMethodError abstractMethodError) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("ViewParent ");
                    sb.append(obj);
                    sb.append(" does not implement interface method onNestedScrollAccepted");
                    Log.e("ViewParentCompat", sb.toString(), (Throwable)abstractMethodError);
                }
            }
            else if (obj instanceof NestedScrollingParent) {
                ((NestedScrollingParent)obj).onNestedScrollAccepted(view, view2, n);
            }
        }
    }
    
    public static boolean onStartNestedScroll(final ViewParent viewParent, final View view, final View view2, final int n) {
        return onStartNestedScroll(viewParent, view, view2, n, 0);
    }
    
    public static boolean onStartNestedScroll(final ViewParent obj, final View view, final View view2, final int n, final int n2) {
        if (obj instanceof NestedScrollingParent2) {
            return ((NestedScrollingParent2)obj).onStartNestedScroll(view, view2, n, n2);
        }
        if (n2 == 0) {
            if (Build$VERSION.SDK_INT >= 21) {
                try {
                    return Api21Impl.onStartNestedScroll(obj, view, view2, n);
                }
                catch (final AbstractMethodError abstractMethodError) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("ViewParent ");
                    sb.append(obj);
                    sb.append(" does not implement interface method onStartNestedScroll");
                    Log.e("ViewParentCompat", sb.toString(), (Throwable)abstractMethodError);
                    return false;
                }
            }
            if (obj instanceof NestedScrollingParent) {
                return ((NestedScrollingParent)obj).onStartNestedScroll(view, view2, n);
            }
        }
        return false;
    }
    
    public static void onStopNestedScroll(final ViewParent viewParent, final View view) {
        onStopNestedScroll(viewParent, view, 0);
    }
    
    public static void onStopNestedScroll(final ViewParent obj, final View view, final int n) {
        if (obj instanceof NestedScrollingParent2) {
            ((NestedScrollingParent2)obj).onStopNestedScroll(view, n);
        }
        else if (n == 0) {
            if (Build$VERSION.SDK_INT >= 21) {
                try {
                    Api21Impl.onStopNestedScroll(obj, view);
                }
                catch (final AbstractMethodError abstractMethodError) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("ViewParent ");
                    sb.append(obj);
                    sb.append(" does not implement interface method onStopNestedScroll");
                    Log.e("ViewParentCompat", sb.toString(), (Throwable)abstractMethodError);
                }
            }
            else if (obj instanceof NestedScrollingParent) {
                ((NestedScrollingParent)obj).onStopNestedScroll(view);
            }
        }
    }
    
    @Deprecated
    public static boolean requestSendAccessibilityEvent(final ViewParent viewParent, final View view, final AccessibilityEvent accessibilityEvent) {
        return viewParent.requestSendAccessibilityEvent(view, accessibilityEvent);
    }
    
    static class Api19Impl
    {
        private Api19Impl() {
        }
        
        static void notifySubtreeAccessibilityStateChanged(final ViewParent viewParent, final View view, final View view2, final int n) {
            viewParent.notifySubtreeAccessibilityStateChanged(view, view2, n);
        }
    }
    
    static class Api21Impl
    {
        private Api21Impl() {
        }
        
        static boolean onNestedFling(final ViewParent viewParent, final View view, final float n, final float n2, final boolean b) {
            return viewParent.onNestedFling(view, n, n2, b);
        }
        
        static boolean onNestedPreFling(final ViewParent viewParent, final View view, final float n, final float n2) {
            return viewParent.onNestedPreFling(view, n, n2);
        }
        
        static void onNestedPreScroll(final ViewParent viewParent, final View view, final int n, final int n2, final int[] array) {
            viewParent.onNestedPreScroll(view, n, n2, array);
        }
        
        static void onNestedScroll(final ViewParent viewParent, final View view, final int n, final int n2, final int n3, final int n4) {
            viewParent.onNestedScroll(view, n, n2, n3, n4);
        }
        
        static void onNestedScrollAccepted(final ViewParent viewParent, final View view, final View view2, final int n) {
            viewParent.onNestedScrollAccepted(view, view2, n);
        }
        
        static boolean onStartNestedScroll(final ViewParent viewParent, final View view, final View view2, final int n) {
            return viewParent.onStartNestedScroll(view, view2, n);
        }
        
        static void onStopNestedScroll(final ViewParent viewParent, final View view) {
            viewParent.onStopNestedScroll(view);
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.view;

public interface OnReceiveContentViewBehavior
{
    ContentInfoCompat onReceiveContent(final ContentInfoCompat p0);
}

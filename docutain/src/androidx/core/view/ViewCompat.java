// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.view;

import android.util.SparseArray;
import java.lang.ref.WeakReference;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;
import android.view.ContentInfo;
import android.view.WindowInsetsController;
import android.view.View$OnUnhandledKeyEventListener;
import java.util.Objects;
import androidx.collection.SimpleArrayMap;
import android.view.autofill.AutofillId;
import android.view.View$OnApplyWindowInsetsListener;
import android.view.ViewTreeObserver;
import java.util.Iterator;
import java.util.Map;
import android.view.View$OnAttachStateChangeListener;
import android.view.ViewTreeObserver$OnGlobalLayoutListener;
import android.view.View$DragShadowBuilder;
import android.content.ClipData;
import android.view.PointerIcon;
import java.util.Arrays;
import androidx.core.util.Preconditions;
import android.graphics.Paint;
import java.lang.reflect.InvocationTargetException;
import android.view.ViewGroup;
import android.graphics.drawable.Drawable;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.animation.ValueAnimator;
import android.os.Bundle;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityManager;
import android.view.Window;
import android.content.Context;
import android.app.Activity;
import android.content.ContextWrapper;
import java.util.Collections;
import android.graphics.Matrix;
import android.view.WindowManager;
import android.view.Display;
import android.view.contentcapture.ContentCaptureSession;
import androidx.core.view.contentcapture.ContentCaptureSessionCompat;
import android.graphics.PorterDuff$Mode;
import android.content.res.ColorStateList;
import android.text.TextUtils;
import androidx.core.view.autofill.AutofillIdCompat;
import java.util.List;
import android.view.accessibility.AccessibilityNodeProvider;
import androidx.core.view.accessibility.AccessibilityNodeProviderCompat;
import android.view.View$AccessibilityDelegate;
import android.view.KeyEvent;
import android.view.WindowInsets;
import android.view.ViewParent;
import android.util.Log;
import java.util.ArrayList;
import java.util.Collection;
import android.os.Build$VERSION;
import androidx.core.view.accessibility.AccessibilityNodeInfoCompat;
import androidx.core.view.accessibility.AccessibilityViewCommand;
import androidx.core.R;
import android.view.View;
import java.util.WeakHashMap;
import android.graphics.Rect;
import java.util.concurrent.atomic.AtomicInteger;
import java.lang.reflect.Method;
import java.lang.reflect.Field;

public class ViewCompat
{
    private static final int[] ACCESSIBILITY_ACTIONS_RESOURCE_IDS;
    public static final int ACCESSIBILITY_LIVE_REGION_ASSERTIVE = 2;
    public static final int ACCESSIBILITY_LIVE_REGION_NONE = 0;
    public static final int ACCESSIBILITY_LIVE_REGION_POLITE = 1;
    public static final int IMPORTANT_FOR_ACCESSIBILITY_AUTO = 0;
    public static final int IMPORTANT_FOR_ACCESSIBILITY_NO = 2;
    public static final int IMPORTANT_FOR_ACCESSIBILITY_NO_HIDE_DESCENDANTS = 4;
    public static final int IMPORTANT_FOR_ACCESSIBILITY_YES = 1;
    public static final int IMPORTANT_FOR_CONTENT_CAPTURE_AUTO = 0;
    public static final int IMPORTANT_FOR_CONTENT_CAPTURE_NO = 2;
    public static final int IMPORTANT_FOR_CONTENT_CAPTURE_NO_EXCLUDE_DESCENDANTS = 8;
    public static final int IMPORTANT_FOR_CONTENT_CAPTURE_YES = 1;
    public static final int IMPORTANT_FOR_CONTENT_CAPTURE_YES_EXCLUDE_DESCENDANTS = 4;
    @Deprecated
    public static final int LAYER_TYPE_HARDWARE = 2;
    @Deprecated
    public static final int LAYER_TYPE_NONE = 0;
    @Deprecated
    public static final int LAYER_TYPE_SOFTWARE = 1;
    public static final int LAYOUT_DIRECTION_INHERIT = 2;
    public static final int LAYOUT_DIRECTION_LOCALE = 3;
    public static final int LAYOUT_DIRECTION_LTR = 0;
    public static final int LAYOUT_DIRECTION_RTL = 1;
    @Deprecated
    public static final int MEASURED_HEIGHT_STATE_SHIFT = 16;
    @Deprecated
    public static final int MEASURED_SIZE_MASK = 16777215;
    @Deprecated
    public static final int MEASURED_STATE_MASK = -16777216;
    @Deprecated
    public static final int MEASURED_STATE_TOO_SMALL = 16777216;
    private static final OnReceiveContentViewBehavior NO_OP_ON_RECEIVE_CONTENT_VIEW_BEHAVIOR;
    @Deprecated
    public static final int OVER_SCROLL_ALWAYS = 0;
    @Deprecated
    public static final int OVER_SCROLL_IF_CONTENT_SCROLLS = 1;
    @Deprecated
    public static final int OVER_SCROLL_NEVER = 2;
    public static final int SCROLL_AXIS_HORIZONTAL = 1;
    public static final int SCROLL_AXIS_NONE = 0;
    public static final int SCROLL_AXIS_VERTICAL = 2;
    public static final int SCROLL_INDICATOR_BOTTOM = 2;
    public static final int SCROLL_INDICATOR_END = 32;
    public static final int SCROLL_INDICATOR_LEFT = 4;
    public static final int SCROLL_INDICATOR_RIGHT = 8;
    public static final int SCROLL_INDICATOR_START = 16;
    public static final int SCROLL_INDICATOR_TOP = 1;
    private static final String TAG = "ViewCompat";
    public static final int TYPE_NON_TOUCH = 1;
    public static final int TYPE_TOUCH = 0;
    private static boolean sAccessibilityDelegateCheckFailed;
    private static Field sAccessibilityDelegateField;
    private static final AccessibilityPaneVisibilityManager sAccessibilityPaneVisibilityManager;
    private static Method sChildrenDrawingOrderMethod;
    private static Method sDispatchFinishTemporaryDetach;
    private static Method sDispatchStartTemporaryDetach;
    private static Field sMinHeightField;
    private static boolean sMinHeightFieldFetched;
    private static Field sMinWidthField;
    private static boolean sMinWidthFieldFetched;
    private static final AtomicInteger sNextGeneratedId;
    private static boolean sTempDetachBound;
    private static ThreadLocal<Rect> sThreadLocalRect;
    private static WeakHashMap<View, String> sTransitionNameMap;
    private static WeakHashMap<View, ViewPropertyAnimatorCompat> sViewPropertyAnimatorMap;
    
    static {
        sNextGeneratedId = new AtomicInteger(1);
        ViewCompat.sViewPropertyAnimatorMap = null;
        ViewCompat.sAccessibilityDelegateCheckFailed = false;
        ACCESSIBILITY_ACTIONS_RESOURCE_IDS = new int[] { R.id.accessibility_custom_action_0, R.id.accessibility_custom_action_1, R.id.accessibility_custom_action_2, R.id.accessibility_custom_action_3, R.id.accessibility_custom_action_4, R.id.accessibility_custom_action_5, R.id.accessibility_custom_action_6, R.id.accessibility_custom_action_7, R.id.accessibility_custom_action_8, R.id.accessibility_custom_action_9, R.id.accessibility_custom_action_10, R.id.accessibility_custom_action_11, R.id.accessibility_custom_action_12, R.id.accessibility_custom_action_13, R.id.accessibility_custom_action_14, R.id.accessibility_custom_action_15, R.id.accessibility_custom_action_16, R.id.accessibility_custom_action_17, R.id.accessibility_custom_action_18, R.id.accessibility_custom_action_19, R.id.accessibility_custom_action_20, R.id.accessibility_custom_action_21, R.id.accessibility_custom_action_22, R.id.accessibility_custom_action_23, R.id.accessibility_custom_action_24, R.id.accessibility_custom_action_25, R.id.accessibility_custom_action_26, R.id.accessibility_custom_action_27, R.id.accessibility_custom_action_28, R.id.accessibility_custom_action_29, R.id.accessibility_custom_action_30, R.id.accessibility_custom_action_31 };
        NO_OP_ON_RECEIVE_CONTENT_VIEW_BEHAVIOR = new ViewCompat$$ExternalSyntheticLambda0();
        sAccessibilityPaneVisibilityManager = new AccessibilityPaneVisibilityManager();
    }
    
    @Deprecated
    protected ViewCompat() {
    }
    
    private static AccessibilityViewProperty<Boolean> accessibilityHeadingProperty() {
        return (AccessibilityViewProperty<Boolean>)new AccessibilityViewProperty<Boolean>(R.id.tag_accessibility_heading, Boolean.class, 28) {
            Boolean frameworkGet(final View view) {
                return Api28Impl.isAccessibilityHeading(view);
            }
            
            void frameworkSet(final View view, final Boolean b) {
                Api28Impl.setAccessibilityHeading(view, b);
            }
            
            boolean shouldUpdate(final Boolean b, final Boolean b2) {
                return ((AccessibilityViewProperty)this).booleanNullToFalseEquals(b, b2) ^ true;
            }
        };
    }
    
    public static int addAccessibilityAction(final View view, final CharSequence charSequence, final AccessibilityViewCommand accessibilityViewCommand) {
        final int availableActionIdFromResources = getAvailableActionIdFromResources(view, charSequence);
        if (availableActionIdFromResources != -1) {
            addAccessibilityAction(view, new AccessibilityNodeInfoCompat.AccessibilityActionCompat(availableActionIdFromResources, charSequence, accessibilityViewCommand));
        }
        return availableActionIdFromResources;
    }
    
    private static void addAccessibilityAction(final View view, final AccessibilityNodeInfoCompat.AccessibilityActionCompat accessibilityActionCompat) {
        if (Build$VERSION.SDK_INT >= 21) {
            ensureAccessibilityDelegateCompat(view);
            removeActionWithId(accessibilityActionCompat.getId(), view);
            getActionList(view).add(accessibilityActionCompat);
            notifyViewAccessibilityStateChangedIfNeeded(view, 0);
        }
    }
    
    public static void addKeyboardNavigationClusters(final View view, final Collection<View> collection, final int n) {
        if (Build$VERSION.SDK_INT >= 26) {
            Api26Impl.addKeyboardNavigationClusters(view, collection, n);
        }
    }
    
    public static void addOnUnhandledKeyEventListener(final View view, final OnUnhandledKeyEventListenerCompat e) {
        if (Build$VERSION.SDK_INT >= 28) {
            Api28Impl.addOnUnhandledKeyEventListener(view, e);
            return;
        }
        ArrayList list;
        if ((list = (ArrayList)view.getTag(R.id.tag_unhandled_key_listeners)) == null) {
            list = new ArrayList();
            view.setTag(R.id.tag_unhandled_key_listeners, (Object)list);
        }
        list.add(e);
        if (list.size() == 1) {
            UnhandledKeyEventManager.registerListeningView(view);
        }
    }
    
    public static ViewPropertyAnimatorCompat animate(final View view) {
        if (ViewCompat.sViewPropertyAnimatorMap == null) {
            ViewCompat.sViewPropertyAnimatorMap = new WeakHashMap<View, ViewPropertyAnimatorCompat>();
        }
        ViewPropertyAnimatorCompat value;
        if ((value = ViewCompat.sViewPropertyAnimatorMap.get(view)) == null) {
            value = new ViewPropertyAnimatorCompat(view);
            ViewCompat.sViewPropertyAnimatorMap.put(view, value);
        }
        return value;
    }
    
    private static void bindTempDetach() {
        try {
            ViewCompat.sDispatchStartTemporaryDetach = View.class.getDeclaredMethod("dispatchStartTemporaryDetach", (Class<?>[])new Class[0]);
            ViewCompat.sDispatchFinishTemporaryDetach = View.class.getDeclaredMethod("dispatchFinishTemporaryDetach", (Class<?>[])new Class[0]);
        }
        catch (final NoSuchMethodException ex) {
            Log.e("ViewCompat", "Couldn't find method", (Throwable)ex);
        }
        ViewCompat.sTempDetachBound = true;
    }
    
    @Deprecated
    public static boolean canScrollHorizontally(final View view, final int n) {
        return view.canScrollHorizontally(n);
    }
    
    @Deprecated
    public static boolean canScrollVertically(final View view, final int n) {
        return view.canScrollVertically(n);
    }
    
    public static void cancelDragAndDrop(final View view) {
        if (Build$VERSION.SDK_INT >= 24) {
            Api24Impl.cancelDragAndDrop(view);
        }
    }
    
    @Deprecated
    public static int combineMeasuredStates(final int n, final int n2) {
        return View.combineMeasuredStates(n, n2);
    }
    
    private static void compatOffsetLeftAndRight(final View view, final int n) {
        view.offsetLeftAndRight(n);
        if (view.getVisibility() == 0) {
            tickleInvalidationFlag(view);
            final ViewParent parent = view.getParent();
            if (parent instanceof View) {
                tickleInvalidationFlag((View)parent);
            }
        }
    }
    
    private static void compatOffsetTopAndBottom(final View view, final int n) {
        view.offsetTopAndBottom(n);
        if (view.getVisibility() == 0) {
            tickleInvalidationFlag(view);
            final ViewParent parent = view.getParent();
            if (parent instanceof View) {
                tickleInvalidationFlag((View)parent);
            }
        }
    }
    
    public static WindowInsetsCompat computeSystemWindowInsets(final View view, final WindowInsetsCompat windowInsetsCompat, final Rect rect) {
        if (Build$VERSION.SDK_INT >= 21) {
            return Api21Impl.computeSystemWindowInsets(view, windowInsetsCompat, rect);
        }
        return windowInsetsCompat;
    }
    
    public static WindowInsetsCompat dispatchApplyWindowInsets(final View view, final WindowInsetsCompat windowInsetsCompat) {
        if (Build$VERSION.SDK_INT >= 21) {
            final WindowInsets windowInsets = windowInsetsCompat.toWindowInsets();
            if (windowInsets != null) {
                final WindowInsets dispatchApplyWindowInsets = Api20Impl.dispatchApplyWindowInsets(view, windowInsets);
                if (!dispatchApplyWindowInsets.equals((Object)windowInsets)) {
                    return WindowInsetsCompat.toWindowInsetsCompat(dispatchApplyWindowInsets, view);
                }
            }
        }
        return windowInsetsCompat;
    }
    
    public static void dispatchFinishTemporaryDetach(final View obj) {
        if (Build$VERSION.SDK_INT >= 24) {
            Api24Impl.dispatchFinishTemporaryDetach(obj);
        }
        else {
            if (!ViewCompat.sTempDetachBound) {
                bindTempDetach();
            }
            final Method sDispatchFinishTemporaryDetach = ViewCompat.sDispatchFinishTemporaryDetach;
            if (sDispatchFinishTemporaryDetach != null) {
                try {
                    sDispatchFinishTemporaryDetach.invoke(obj, new Object[0]);
                }
                catch (final Exception ex) {
                    Log.d("ViewCompat", "Error calling dispatchFinishTemporaryDetach", (Throwable)ex);
                }
            }
            else {
                obj.onFinishTemporaryDetach();
            }
        }
    }
    
    public static boolean dispatchNestedFling(final View view, final float n, final float n2, final boolean b) {
        if (Build$VERSION.SDK_INT >= 21) {
            return Api21Impl.dispatchNestedFling(view, n, n2, b);
        }
        return view instanceof NestedScrollingChild && ((NestedScrollingChild)view).dispatchNestedFling(n, n2, b);
    }
    
    public static boolean dispatchNestedPreFling(final View view, final float n, final float n2) {
        if (Build$VERSION.SDK_INT >= 21) {
            return Api21Impl.dispatchNestedPreFling(view, n, n2);
        }
        return view instanceof NestedScrollingChild && ((NestedScrollingChild)view).dispatchNestedPreFling(n, n2);
    }
    
    public static boolean dispatchNestedPreScroll(final View view, final int n, final int n2, final int[] array, final int[] array2) {
        if (Build$VERSION.SDK_INT >= 21) {
            return Api21Impl.dispatchNestedPreScroll(view, n, n2, array, array2);
        }
        return view instanceof NestedScrollingChild && ((NestedScrollingChild)view).dispatchNestedPreScroll(n, n2, array, array2);
    }
    
    public static boolean dispatchNestedPreScroll(final View view, final int n, final int n2, final int[] array, final int[] array2, final int n3) {
        if (view instanceof NestedScrollingChild2) {
            return ((NestedScrollingChild2)view).dispatchNestedPreScroll(n, n2, array, array2, n3);
        }
        return n3 == 0 && dispatchNestedPreScroll(view, n, n2, array, array2);
    }
    
    public static void dispatchNestedScroll(final View view, final int n, final int n2, final int n3, final int n4, final int[] array, final int n5, final int[] array2) {
        if (view instanceof NestedScrollingChild3) {
            ((NestedScrollingChild3)view).dispatchNestedScroll(n, n2, n3, n4, array, n5, array2);
        }
        else {
            dispatchNestedScroll(view, n, n2, n3, n4, array, n5);
        }
    }
    
    public static boolean dispatchNestedScroll(final View view, final int n, final int n2, final int n3, final int n4, final int[] array) {
        if (Build$VERSION.SDK_INT >= 21) {
            return Api21Impl.dispatchNestedScroll(view, n, n2, n3, n4, array);
        }
        return view instanceof NestedScrollingChild && ((NestedScrollingChild)view).dispatchNestedScroll(n, n2, n3, n4, array);
    }
    
    public static boolean dispatchNestedScroll(final View view, final int n, final int n2, final int n3, final int n4, final int[] array, final int n5) {
        if (view instanceof NestedScrollingChild2) {
            return ((NestedScrollingChild2)view).dispatchNestedScroll(n, n2, n3, n4, array, n5);
        }
        return n5 == 0 && dispatchNestedScroll(view, n, n2, n3, n4, array);
    }
    
    public static void dispatchStartTemporaryDetach(final View obj) {
        if (Build$VERSION.SDK_INT >= 24) {
            Api24Impl.dispatchStartTemporaryDetach(obj);
        }
        else {
            if (!ViewCompat.sTempDetachBound) {
                bindTempDetach();
            }
            final Method sDispatchStartTemporaryDetach = ViewCompat.sDispatchStartTemporaryDetach;
            if (sDispatchStartTemporaryDetach != null) {
                try {
                    sDispatchStartTemporaryDetach.invoke(obj, new Object[0]);
                }
                catch (final Exception ex) {
                    Log.d("ViewCompat", "Error calling dispatchStartTemporaryDetach", (Throwable)ex);
                }
            }
            else {
                obj.onStartTemporaryDetach();
            }
        }
    }
    
    static boolean dispatchUnhandledKeyEventBeforeCallback(final View view, final KeyEvent keyEvent) {
        return Build$VERSION.SDK_INT < 28 && UnhandledKeyEventManager.at(view).dispatch(view, keyEvent);
    }
    
    static boolean dispatchUnhandledKeyEventBeforeHierarchy(final View view, final KeyEvent keyEvent) {
        return Build$VERSION.SDK_INT < 28 && UnhandledKeyEventManager.at(view).preDispatch(keyEvent);
    }
    
    public static void enableAccessibleClickableSpanSupport(final View view) {
        if (Build$VERSION.SDK_INT >= 19) {
            ensureAccessibilityDelegateCompat(view);
        }
    }
    
    static void ensureAccessibilityDelegateCompat(final View view) {
        AccessibilityDelegateCompat accessibilityDelegate;
        if ((accessibilityDelegate = getAccessibilityDelegate(view)) == null) {
            accessibilityDelegate = new AccessibilityDelegateCompat();
        }
        setAccessibilityDelegate(view, accessibilityDelegate);
    }
    
    public static int generateViewId() {
        if (Build$VERSION.SDK_INT >= 17) {
            return Api17Impl.generateViewId();
        }
        AtomicInteger sNextGeneratedId;
        int value;
        int newValue;
        do {
            sNextGeneratedId = ViewCompat.sNextGeneratedId;
            value = sNextGeneratedId.get();
            if ((newValue = value + 1) > 16777215) {
                newValue = 1;
            }
        } while (!sNextGeneratedId.compareAndSet(value, newValue));
        return value;
    }
    
    public static AccessibilityDelegateCompat getAccessibilityDelegate(final View view) {
        final View$AccessibilityDelegate accessibilityDelegateInternal = getAccessibilityDelegateInternal(view);
        if (accessibilityDelegateInternal == null) {
            return null;
        }
        if (accessibilityDelegateInternal instanceof AccessibilityDelegateCompat.AccessibilityDelegateAdapter) {
            return ((AccessibilityDelegateCompat.AccessibilityDelegateAdapter)accessibilityDelegateInternal).mCompat;
        }
        return new AccessibilityDelegateCompat(accessibilityDelegateInternal);
    }
    
    private static View$AccessibilityDelegate getAccessibilityDelegateInternal(final View view) {
        if (Build$VERSION.SDK_INT >= 29) {
            return Api29Impl.getAccessibilityDelegate(view);
        }
        return getAccessibilityDelegateThroughReflection(view);
    }
    
    private static View$AccessibilityDelegate getAccessibilityDelegateThroughReflection(final View obj) {
        if (ViewCompat.sAccessibilityDelegateCheckFailed) {
            return null;
        }
        if (ViewCompat.sAccessibilityDelegateField == null) {
            try {
                (ViewCompat.sAccessibilityDelegateField = View.class.getDeclaredField("mAccessibilityDelegate")).setAccessible(true);
            }
            finally {
                ViewCompat.sAccessibilityDelegateCheckFailed = true;
                return null;
            }
        }
        try {
            final Object value = ViewCompat.sAccessibilityDelegateField.get(obj);
            if (value instanceof View$AccessibilityDelegate) {
                return (View$AccessibilityDelegate)value;
            }
            return null;
        }
        finally {
            ViewCompat.sAccessibilityDelegateCheckFailed = true;
            return null;
        }
    }
    
    public static int getAccessibilityLiveRegion(final View view) {
        if (Build$VERSION.SDK_INT >= 19) {
            return Api19Impl.getAccessibilityLiveRegion(view);
        }
        return 0;
    }
    
    public static AccessibilityNodeProviderCompat getAccessibilityNodeProvider(final View view) {
        if (Build$VERSION.SDK_INT >= 16) {
            final AccessibilityNodeProvider accessibilityNodeProvider = Api16Impl.getAccessibilityNodeProvider(view);
            if (accessibilityNodeProvider != null) {
                return new AccessibilityNodeProviderCompat(accessibilityNodeProvider);
            }
        }
        return null;
    }
    
    public static CharSequence getAccessibilityPaneTitle(final View view) {
        return paneTitleProperty().get(view);
    }
    
    private static List<AccessibilityNodeInfoCompat.AccessibilityActionCompat> getActionList(final View view) {
        ArrayList list;
        if ((list = (ArrayList)view.getTag(R.id.tag_accessibility_actions)) == null) {
            list = new ArrayList();
            view.setTag(R.id.tag_accessibility_actions, (Object)list);
        }
        return list;
    }
    
    @Deprecated
    public static float getAlpha(final View view) {
        return view.getAlpha();
    }
    
    public static AutofillIdCompat getAutofillId(final View view) {
        if (Build$VERSION.SDK_INT >= 26) {
            return AutofillIdCompat.toAutofillIdCompat(Api26Impl.getAutofillId(view));
        }
        return null;
    }
    
    private static int getAvailableActionIdFromResources(final View view, final CharSequence charSequence) {
        final List<AccessibilityNodeInfoCompat.AccessibilityActionCompat> actionList = getActionList(view);
        for (int i = 0; i < actionList.size(); ++i) {
            if (TextUtils.equals(charSequence, ((AccessibilityNodeInfoCompat.AccessibilityActionCompat)actionList.get(i)).getLabel())) {
                return ((AccessibilityNodeInfoCompat.AccessibilityActionCompat)actionList.get(i)).getId();
            }
        }
        int n = 0;
        int n2 = -1;
        while (true) {
            final int[] accessibility_ACTIONS_RESOURCE_IDS = ViewCompat.ACCESSIBILITY_ACTIONS_RESOURCE_IDS;
            if (n >= accessibility_ACTIONS_RESOURCE_IDS.length || n2 != -1) {
                break;
            }
            final int n3 = accessibility_ACTIONS_RESOURCE_IDS[n];
            int j = 0;
            boolean b = true;
            while (j < actionList.size()) {
                b &= (((AccessibilityNodeInfoCompat.AccessibilityActionCompat)actionList.get(j)).getId() != n3);
                ++j;
            }
            if (b) {
                n2 = n3;
            }
            ++n;
        }
        return n2;
    }
    
    public static ColorStateList getBackgroundTintList(final View view) {
        if (Build$VERSION.SDK_INT >= 21) {
            return Api21Impl.getBackgroundTintList(view);
        }
        ColorStateList supportBackgroundTintList;
        if (view instanceof TintableBackgroundView) {
            supportBackgroundTintList = ((TintableBackgroundView)view).getSupportBackgroundTintList();
        }
        else {
            supportBackgroundTintList = null;
        }
        return supportBackgroundTintList;
    }
    
    public static PorterDuff$Mode getBackgroundTintMode(final View view) {
        if (Build$VERSION.SDK_INT >= 21) {
            return Api21Impl.getBackgroundTintMode(view);
        }
        PorterDuff$Mode supportBackgroundTintMode;
        if (view instanceof TintableBackgroundView) {
            supportBackgroundTintMode = ((TintableBackgroundView)view).getSupportBackgroundTintMode();
        }
        else {
            supportBackgroundTintMode = null;
        }
        return supportBackgroundTintMode;
    }
    
    public static Rect getClipBounds(final View view) {
        if (Build$VERSION.SDK_INT >= 18) {
            return Api18Impl.getClipBounds(view);
        }
        return null;
    }
    
    public static ContentCaptureSessionCompat getContentCaptureSession(final View view) {
        if (Build$VERSION.SDK_INT < 29) {
            return null;
        }
        final ContentCaptureSession contentCaptureSession = Api29Impl.getContentCaptureSession(view);
        if (contentCaptureSession == null) {
            return null;
        }
        return ContentCaptureSessionCompat.toContentCaptureSessionCompat(contentCaptureSession, view);
    }
    
    public static Display getDisplay(final View view) {
        if (Build$VERSION.SDK_INT >= 17) {
            return Api17Impl.getDisplay(view);
        }
        if (isAttachedToWindow(view)) {
            return ((WindowManager)view.getContext().getSystemService("window")).getDefaultDisplay();
        }
        return null;
    }
    
    public static float getElevation(final View view) {
        if (Build$VERSION.SDK_INT >= 21) {
            return Api21Impl.getElevation(view);
        }
        return 0.0f;
    }
    
    private static Rect getEmptyTempRect() {
        if (ViewCompat.sThreadLocalRect == null) {
            ViewCompat.sThreadLocalRect = new ThreadLocal<Rect>();
        }
        Rect value;
        if ((value = ViewCompat.sThreadLocalRect.get()) == null) {
            value = new Rect();
            ViewCompat.sThreadLocalRect.set(value);
        }
        value.setEmpty();
        return value;
    }
    
    private static OnReceiveContentViewBehavior getFallback(final View view) {
        if (view instanceof OnReceiveContentViewBehavior) {
            return (OnReceiveContentViewBehavior)view;
        }
        return ViewCompat.NO_OP_ON_RECEIVE_CONTENT_VIEW_BEHAVIOR;
    }
    
    public static boolean getFitsSystemWindows(final View view) {
        return Build$VERSION.SDK_INT >= 16 && Api16Impl.getFitsSystemWindows(view);
    }
    
    public static int getImportantForAccessibility(final View view) {
        if (Build$VERSION.SDK_INT >= 16) {
            return Api16Impl.getImportantForAccessibility(view);
        }
        return 0;
    }
    
    public static int getImportantForAutofill(final View view) {
        if (Build$VERSION.SDK_INT >= 26) {
            return Api26Impl.getImportantForAutofill(view);
        }
        return 0;
    }
    
    public static int getImportantForContentCapture(final View view) {
        if (Build$VERSION.SDK_INT >= 30) {
            return Api30Impl.getImportantForContentCapture(view);
        }
        return 0;
    }
    
    public static int getLabelFor(final View view) {
        if (Build$VERSION.SDK_INT >= 17) {
            return Api17Impl.getLabelFor(view);
        }
        return 0;
    }
    
    @Deprecated
    public static int getLayerType(final View view) {
        return view.getLayerType();
    }
    
    public static int getLayoutDirection(final View view) {
        if (Build$VERSION.SDK_INT >= 17) {
            return Api17Impl.getLayoutDirection(view);
        }
        return 0;
    }
    
    @Deprecated
    public static Matrix getMatrix(final View view) {
        return view.getMatrix();
    }
    
    @Deprecated
    public static int getMeasuredHeightAndState(final View view) {
        return view.getMeasuredHeightAndState();
    }
    
    @Deprecated
    public static int getMeasuredState(final View view) {
        return view.getMeasuredState();
    }
    
    @Deprecated
    public static int getMeasuredWidthAndState(final View view) {
        return view.getMeasuredWidthAndState();
    }
    
    public static int getMinimumHeight(final View p0) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     3: bipush          16
        //     5: if_icmplt       13
        //     8: aload_0        
        //     9: invokestatic    androidx/core/view/ViewCompat$Api16Impl.getMinimumHeight:(Landroid/view/View;)I
        //    12: ireturn        
        //    13: getstatic       androidx/core/view/ViewCompat.sMinHeightFieldFetched:Z
        //    16: ifne            42
        //    19: ldc_w           Landroid/view/View;.class
        //    22: ldc_w           "mMinHeight"
        //    25: invokevirtual   java/lang/Class.getDeclaredField:(Ljava/lang/String;)Ljava/lang/reflect/Field;
        //    28: astore_2       
        //    29: aload_2        
        //    30: putstatic       androidx/core/view/ViewCompat.sMinHeightField:Ljava/lang/reflect/Field;
        //    33: aload_2        
        //    34: iconst_1       
        //    35: invokevirtual   java/lang/reflect/Field.setAccessible:(Z)V
        //    38: iconst_1       
        //    39: putstatic       androidx/core/view/ViewCompat.sMinHeightFieldFetched:Z
        //    42: getstatic       androidx/core/view/ViewCompat.sMinHeightField:Ljava/lang/reflect/Field;
        //    45: astore_2       
        //    46: aload_2        
        //    47: ifnull          64
        //    50: aload_2        
        //    51: aload_0        
        //    52: invokevirtual   java/lang/reflect/Field.get:(Ljava/lang/Object;)Ljava/lang/Object;
        //    55: checkcast       Ljava/lang/Integer;
        //    58: invokevirtual   java/lang/Integer.intValue:()I
        //    61: istore_1       
        //    62: iload_1        
        //    63: ireturn        
        //    64: iconst_0       
        //    65: ireturn        
        //    66: astore_2       
        //    67: goto            38
        //    70: astore_0       
        //    71: goto            64
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                            
        //  -----  -----  -----  -----  --------------------------------
        //  19     38     66     70     Ljava/lang/NoSuchFieldException;
        //  50     62     70     74     Ljava/lang/Exception;
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0064:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2604)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:206)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public static int getMinimumWidth(final View p0) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     3: bipush          16
        //     5: if_icmplt       13
        //     8: aload_0        
        //     9: invokestatic    androidx/core/view/ViewCompat$Api16Impl.getMinimumWidth:(Landroid/view/View;)I
        //    12: ireturn        
        //    13: getstatic       androidx/core/view/ViewCompat.sMinWidthFieldFetched:Z
        //    16: ifne            42
        //    19: ldc_w           Landroid/view/View;.class
        //    22: ldc_w           "mMinWidth"
        //    25: invokevirtual   java/lang/Class.getDeclaredField:(Ljava/lang/String;)Ljava/lang/reflect/Field;
        //    28: astore_2       
        //    29: aload_2        
        //    30: putstatic       androidx/core/view/ViewCompat.sMinWidthField:Ljava/lang/reflect/Field;
        //    33: aload_2        
        //    34: iconst_1       
        //    35: invokevirtual   java/lang/reflect/Field.setAccessible:(Z)V
        //    38: iconst_1       
        //    39: putstatic       androidx/core/view/ViewCompat.sMinWidthFieldFetched:Z
        //    42: getstatic       androidx/core/view/ViewCompat.sMinWidthField:Ljava/lang/reflect/Field;
        //    45: astore_2       
        //    46: aload_2        
        //    47: ifnull          64
        //    50: aload_2        
        //    51: aload_0        
        //    52: invokevirtual   java/lang/reflect/Field.get:(Ljava/lang/Object;)Ljava/lang/Object;
        //    55: checkcast       Ljava/lang/Integer;
        //    58: invokevirtual   java/lang/Integer.intValue:()I
        //    61: istore_1       
        //    62: iload_1        
        //    63: ireturn        
        //    64: iconst_0       
        //    65: ireturn        
        //    66: astore_2       
        //    67: goto            38
        //    70: astore_0       
        //    71: goto            64
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                            
        //  -----  -----  -----  -----  --------------------------------
        //  19     38     66     70     Ljava/lang/NoSuchFieldException;
        //  50     62     70     74     Ljava/lang/Exception;
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0064:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2604)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:206)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public static int getNextClusterForwardId(final View view) {
        if (Build$VERSION.SDK_INT >= 26) {
            return Api26Impl.getNextClusterForwardId(view);
        }
        return -1;
    }
    
    public static String[] getOnReceiveContentMimeTypes(final View view) {
        if (Build$VERSION.SDK_INT >= 31) {
            return Api31Impl.getReceiveContentMimeTypes(view);
        }
        return (String[])view.getTag(R.id.tag_on_receive_content_mime_types);
    }
    
    @Deprecated
    public static int getOverScrollMode(final View view) {
        return view.getOverScrollMode();
    }
    
    public static int getPaddingEnd(final View view) {
        if (Build$VERSION.SDK_INT >= 17) {
            return Api17Impl.getPaddingEnd(view);
        }
        return view.getPaddingRight();
    }
    
    public static int getPaddingStart(final View view) {
        if (Build$VERSION.SDK_INT >= 17) {
            return Api17Impl.getPaddingStart(view);
        }
        return view.getPaddingLeft();
    }
    
    public static ViewParent getParentForAccessibility(final View view) {
        if (Build$VERSION.SDK_INT >= 16) {
            return Api16Impl.getParentForAccessibility(view);
        }
        return view.getParent();
    }
    
    @Deprecated
    public static float getPivotX(final View view) {
        return view.getPivotX();
    }
    
    @Deprecated
    public static float getPivotY(final View view) {
        return view.getPivotY();
    }
    
    public static WindowInsetsCompat getRootWindowInsets(final View view) {
        if (Build$VERSION.SDK_INT >= 23) {
            return Api23Impl.getRootWindowInsets(view);
        }
        if (Build$VERSION.SDK_INT >= 21) {
            return Api21Impl.getRootWindowInsets(view);
        }
        return null;
    }
    
    @Deprecated
    public static float getRotation(final View view) {
        return view.getRotation();
    }
    
    @Deprecated
    public static float getRotationX(final View view) {
        return view.getRotationX();
    }
    
    @Deprecated
    public static float getRotationY(final View view) {
        return view.getRotationY();
    }
    
    @Deprecated
    public static float getScaleX(final View view) {
        return view.getScaleX();
    }
    
    @Deprecated
    public static float getScaleY(final View view) {
        return view.getScaleY();
    }
    
    public static int getScrollIndicators(final View view) {
        if (Build$VERSION.SDK_INT >= 23) {
            return Api23Impl.getScrollIndicators(view);
        }
        return 0;
    }
    
    public static CharSequence getStateDescription(final View view) {
        return stateDescriptionProperty().get(view);
    }
    
    public static List<Rect> getSystemGestureExclusionRects(final View view) {
        if (Build$VERSION.SDK_INT >= 29) {
            return Api29Impl.getSystemGestureExclusionRects(view);
        }
        return Collections.emptyList();
    }
    
    public static String getTransitionName(final View key) {
        if (Build$VERSION.SDK_INT >= 21) {
            return Api21Impl.getTransitionName(key);
        }
        final WeakHashMap<View, String> sTransitionNameMap = ViewCompat.sTransitionNameMap;
        if (sTransitionNameMap == null) {
            return null;
        }
        return sTransitionNameMap.get(key);
    }
    
    @Deprecated
    public static float getTranslationX(final View view) {
        return view.getTranslationX();
    }
    
    @Deprecated
    public static float getTranslationY(final View view) {
        return view.getTranslationY();
    }
    
    public static float getTranslationZ(final View view) {
        if (Build$VERSION.SDK_INT >= 21) {
            return Api21Impl.getTranslationZ(view);
        }
        return 0.0f;
    }
    
    @Deprecated
    public static WindowInsetsControllerCompat getWindowInsetsController(final View view) {
        if (Build$VERSION.SDK_INT >= 30) {
            return Api30Impl.getWindowInsetsController(view);
        }
        Context context = view.getContext();
        while (true) {
            final boolean b = context instanceof ContextWrapper;
            final WindowInsetsControllerCompat windowInsetsControllerCompat = null;
            if (!b) {
                return null;
            }
            if (context instanceof Activity) {
                final Window window = ((Activity)context).getWindow();
                WindowInsetsControllerCompat insetsController = windowInsetsControllerCompat;
                if (window != null) {
                    insetsController = WindowCompat.getInsetsController(window, view);
                }
                return insetsController;
            }
            context = ((ContextWrapper)context).getBaseContext();
        }
    }
    
    @Deprecated
    public static int getWindowSystemUiVisibility(final View view) {
        if (Build$VERSION.SDK_INT >= 16) {
            return Api16Impl.getWindowSystemUiVisibility(view);
        }
        return 0;
    }
    
    @Deprecated
    public static float getX(final View view) {
        return view.getX();
    }
    
    @Deprecated
    public static float getY(final View view) {
        return view.getY();
    }
    
    public static float getZ(final View view) {
        if (Build$VERSION.SDK_INT >= 21) {
            return Api21Impl.getZ(view);
        }
        return 0.0f;
    }
    
    public static boolean hasAccessibilityDelegate(final View view) {
        return getAccessibilityDelegateInternal(view) != null;
    }
    
    public static boolean hasExplicitFocusable(final View view) {
        if (Build$VERSION.SDK_INT >= 26) {
            return Api26Impl.hasExplicitFocusable(view);
        }
        return view.hasFocusable();
    }
    
    public static boolean hasNestedScrollingParent(final View view) {
        if (Build$VERSION.SDK_INT >= 21) {
            return Api21Impl.hasNestedScrollingParent(view);
        }
        return view instanceof NestedScrollingChild && ((NestedScrollingChild)view).hasNestedScrollingParent();
    }
    
    public static boolean hasNestedScrollingParent(final View view, final int n) {
        if (view instanceof NestedScrollingChild2) {
            ((NestedScrollingChild2)view).hasNestedScrollingParent(n);
        }
        else if (n == 0) {
            return hasNestedScrollingParent(view);
        }
        return false;
    }
    
    public static boolean hasOnClickListeners(final View view) {
        return Build$VERSION.SDK_INT >= 15 && Api15Impl.hasOnClickListeners(view);
    }
    
    public static boolean hasOverlappingRendering(final View view) {
        return Build$VERSION.SDK_INT < 16 || Api16Impl.hasOverlappingRendering(view);
    }
    
    public static boolean hasTransientState(final View view) {
        return Build$VERSION.SDK_INT >= 16 && Api16Impl.hasTransientState(view);
    }
    
    public static boolean isAccessibilityHeading(final View view) {
        final Boolean b = accessibilityHeadingProperty().get(view);
        return b != null && b;
    }
    
    public static boolean isAttachedToWindow(final View view) {
        if (Build$VERSION.SDK_INT >= 19) {
            return Api19Impl.isAttachedToWindow(view);
        }
        return view.getWindowToken() != null;
    }
    
    public static boolean isFocusedByDefault(final View view) {
        return Build$VERSION.SDK_INT >= 26 && Api26Impl.isFocusedByDefault(view);
    }
    
    public static boolean isImportantForAccessibility(final View view) {
        return Build$VERSION.SDK_INT < 21 || Api21Impl.isImportantForAccessibility(view);
    }
    
    public static boolean isImportantForAutofill(final View view) {
        return Build$VERSION.SDK_INT < 26 || Api26Impl.isImportantForAutofill(view);
    }
    
    public static boolean isImportantForContentCapture(final View view) {
        return Build$VERSION.SDK_INT >= 30 && Api30Impl.isImportantForContentCapture(view);
    }
    
    public static boolean isInLayout(final View view) {
        return Build$VERSION.SDK_INT >= 18 && Api18Impl.isInLayout(view);
    }
    
    public static boolean isKeyboardNavigationCluster(final View view) {
        return Build$VERSION.SDK_INT >= 26 && Api26Impl.isKeyboardNavigationCluster(view);
    }
    
    public static boolean isLaidOut(final View view) {
        if (Build$VERSION.SDK_INT >= 19) {
            return Api19Impl.isLaidOut(view);
        }
        return view.getWidth() > 0 && view.getHeight() > 0;
    }
    
    public static boolean isLayoutDirectionResolved(final View view) {
        return Build$VERSION.SDK_INT >= 19 && Api19Impl.isLayoutDirectionResolved(view);
    }
    
    public static boolean isNestedScrollingEnabled(final View view) {
        if (Build$VERSION.SDK_INT >= 21) {
            return Api21Impl.isNestedScrollingEnabled(view);
        }
        return view instanceof NestedScrollingChild && ((NestedScrollingChild)view).isNestedScrollingEnabled();
    }
    
    @Deprecated
    public static boolean isOpaque(final View view) {
        return view.isOpaque();
    }
    
    public static boolean isPaddingRelative(final View view) {
        return Build$VERSION.SDK_INT >= 17 && Api17Impl.isPaddingRelative(view);
    }
    
    public static boolean isScreenReaderFocusable(final View view) {
        final Boolean b = screenReaderFocusableProperty().get(view);
        return b != null && b;
    }
    
    @Deprecated
    public static void jumpDrawablesToCurrentState(final View view) {
        view.jumpDrawablesToCurrentState();
    }
    
    public static View keyboardNavigationClusterSearch(final View view, final View view2, final int n) {
        if (Build$VERSION.SDK_INT >= 26) {
            return Api26Impl.keyboardNavigationClusterSearch(view, view2, n);
        }
        return null;
    }
    
    static void notifyViewAccessibilityStateChangedIfNeeded(final View view, final int n) {
        final AccessibilityManager accessibilityManager = (AccessibilityManager)view.getContext().getSystemService("accessibility");
        if (!accessibilityManager.isEnabled()) {
            return;
        }
        final boolean b = getAccessibilityPaneTitle(view) != null && view.isShown() && view.getWindowVisibility() == 0;
        final int accessibilityLiveRegion = getAccessibilityLiveRegion(view);
        int eventType = 32;
        if (accessibilityLiveRegion == 0 && !b) {
            if (n == 32) {
                final AccessibilityEvent obtain = AccessibilityEvent.obtain();
                view.onInitializeAccessibilityEvent(obtain);
                obtain.setEventType(32);
                Api19Impl.setContentChangeTypes(obtain, n);
                obtain.setSource(view);
                view.onPopulateAccessibilityEvent(obtain);
                obtain.getText().add(getAccessibilityPaneTitle(view));
                accessibilityManager.sendAccessibilityEvent(obtain);
            }
            else if (view.getParent() != null) {
                final ViewParent parent = view.getParent();
                try {
                    Api19Impl.notifySubtreeAccessibilityStateChanged(parent, view, view, n);
                }
                catch (final AbstractMethodError abstractMethodError) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append(view.getParent().getClass().getSimpleName());
                    sb.append(" does not fully implement ViewParent");
                    Log.e("ViewCompat", sb.toString(), (Throwable)abstractMethodError);
                }
            }
        }
        else {
            final AccessibilityEvent obtain2 = AccessibilityEvent.obtain();
            if (!b) {
                eventType = 2048;
            }
            obtain2.setEventType(eventType);
            Api19Impl.setContentChangeTypes(obtain2, n);
            if (b) {
                obtain2.getText().add(getAccessibilityPaneTitle(view));
                setImportantForAccessibilityIfNeeded(view);
            }
            view.sendAccessibilityEventUnchecked(obtain2);
        }
    }
    
    public static void offsetLeftAndRight(final View view, final int n) {
        if (Build$VERSION.SDK_INT >= 23) {
            view.offsetLeftAndRight(n);
        }
        else if (Build$VERSION.SDK_INT >= 21) {
            final Rect emptyTempRect = getEmptyTempRect();
            boolean b = false;
            final ViewParent parent = view.getParent();
            if (parent instanceof View) {
                final View view2 = (View)parent;
                emptyTempRect.set(view2.getLeft(), view2.getTop(), view2.getRight(), view2.getBottom());
                b = (emptyTempRect.intersects(view.getLeft(), view.getTop(), view.getRight(), view.getBottom()) ^ true);
            }
            compatOffsetLeftAndRight(view, n);
            if (b && emptyTempRect.intersect(view.getLeft(), view.getTop(), view.getRight(), view.getBottom())) {
                ((View)parent).invalidate(emptyTempRect);
            }
        }
        else {
            compatOffsetLeftAndRight(view, n);
        }
    }
    
    public static void offsetTopAndBottom(final View view, final int n) {
        if (Build$VERSION.SDK_INT >= 23) {
            view.offsetTopAndBottom(n);
        }
        else if (Build$VERSION.SDK_INT >= 21) {
            final Rect emptyTempRect = getEmptyTempRect();
            boolean b = false;
            final ViewParent parent = view.getParent();
            if (parent instanceof View) {
                final View view2 = (View)parent;
                emptyTempRect.set(view2.getLeft(), view2.getTop(), view2.getRight(), view2.getBottom());
                b = (emptyTempRect.intersects(view.getLeft(), view.getTop(), view.getRight(), view.getBottom()) ^ true);
            }
            compatOffsetTopAndBottom(view, n);
            if (b && emptyTempRect.intersect(view.getLeft(), view.getTop(), view.getRight(), view.getBottom())) {
                ((View)parent).invalidate(emptyTempRect);
            }
        }
        else {
            compatOffsetTopAndBottom(view, n);
        }
    }
    
    public static WindowInsetsCompat onApplyWindowInsets(final View view, final WindowInsetsCompat windowInsetsCompat) {
        if (Build$VERSION.SDK_INT >= 21) {
            final WindowInsets windowInsets = windowInsetsCompat.toWindowInsets();
            if (windowInsets != null) {
                final WindowInsets onApplyWindowInsets = Api20Impl.onApplyWindowInsets(view, windowInsets);
                if (!onApplyWindowInsets.equals((Object)windowInsets)) {
                    return WindowInsetsCompat.toWindowInsetsCompat(onApplyWindowInsets, view);
                }
            }
        }
        return windowInsetsCompat;
    }
    
    @Deprecated
    public static void onInitializeAccessibilityEvent(final View view, final AccessibilityEvent accessibilityEvent) {
        view.onInitializeAccessibilityEvent(accessibilityEvent);
    }
    
    public static void onInitializeAccessibilityNodeInfo(final View view, final AccessibilityNodeInfoCompat accessibilityNodeInfoCompat) {
        view.onInitializeAccessibilityNodeInfo(accessibilityNodeInfoCompat.unwrap());
    }
    
    @Deprecated
    public static void onPopulateAccessibilityEvent(final View view, final AccessibilityEvent accessibilityEvent) {
        view.onPopulateAccessibilityEvent(accessibilityEvent);
    }
    
    private static AccessibilityViewProperty<CharSequence> paneTitleProperty() {
        return (AccessibilityViewProperty<CharSequence>)new AccessibilityViewProperty<CharSequence>(R.id.tag_accessibility_pane_title, CharSequence.class, 8, 28) {
            CharSequence frameworkGet(final View view) {
                return Api28Impl.getAccessibilityPaneTitle(view);
            }
            
            void frameworkSet(final View view, final CharSequence charSequence) {
                Api28Impl.setAccessibilityPaneTitle(view, charSequence);
            }
            
            boolean shouldUpdate(final CharSequence charSequence, final CharSequence charSequence2) {
                return TextUtils.equals(charSequence, charSequence2) ^ true;
            }
        };
    }
    
    public static boolean performAccessibilityAction(final View view, final int n, final Bundle bundle) {
        return Build$VERSION.SDK_INT >= 16 && Api16Impl.performAccessibilityAction(view, n, bundle);
    }
    
    public static boolean performHapticFeedback(final View view, int feedbackConstantOrFallback) {
        feedbackConstantOrFallback = HapticFeedbackConstantsCompat.getFeedbackConstantOrFallback(feedbackConstantOrFallback);
        return feedbackConstantOrFallback != -1 && view.performHapticFeedback(feedbackConstantOrFallback);
    }
    
    public static boolean performHapticFeedback(final View view, int feedbackConstantOrFallback, final int n) {
        feedbackConstantOrFallback = HapticFeedbackConstantsCompat.getFeedbackConstantOrFallback(feedbackConstantOrFallback);
        return feedbackConstantOrFallback != -1 && view.performHapticFeedback(feedbackConstantOrFallback, n);
    }
    
    public static ContentInfoCompat performReceiveContent(final View view, ContentInfoCompat onReceiveContent) {
        if (Log.isLoggable("ViewCompat", 3)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("performReceiveContent: ");
            sb.append(onReceiveContent);
            sb.append(", view=");
            sb.append(view.getClass().getSimpleName());
            sb.append("[");
            sb.append(view.getId());
            sb.append("]");
            Log.d("ViewCompat", sb.toString());
        }
        if (Build$VERSION.SDK_INT >= 31) {
            return Api31Impl.performReceiveContent(view, onReceiveContent);
        }
        final OnReceiveContentListener onReceiveContentListener = (OnReceiveContentListener)view.getTag(R.id.tag_on_receive_content_listener);
        if (onReceiveContentListener != null) {
            onReceiveContent = onReceiveContentListener.onReceiveContent(view, onReceiveContent);
            ContentInfoCompat onReceiveContent2;
            if (onReceiveContent == null) {
                onReceiveContent2 = null;
            }
            else {
                onReceiveContent2 = getFallback(view).onReceiveContent(onReceiveContent);
            }
            return onReceiveContent2;
        }
        return getFallback(view).onReceiveContent(onReceiveContent);
    }
    
    public static void postInvalidateOnAnimation(final View view) {
        if (Build$VERSION.SDK_INT >= 16) {
            Api16Impl.postInvalidateOnAnimation(view);
        }
        else {
            view.postInvalidate();
        }
    }
    
    public static void postInvalidateOnAnimation(final View view, final int n, final int n2, final int n3, final int n4) {
        if (Build$VERSION.SDK_INT >= 16) {
            Api16Impl.postInvalidateOnAnimation(view, n, n2, n3, n4);
        }
        else {
            view.postInvalidate(n, n2, n3, n4);
        }
    }
    
    public static void postOnAnimation(final View view, final Runnable runnable) {
        if (Build$VERSION.SDK_INT >= 16) {
            Api16Impl.postOnAnimation(view, runnable);
        }
        else {
            view.postDelayed(runnable, ValueAnimator.getFrameDelay());
        }
    }
    
    public static void postOnAnimationDelayed(final View view, final Runnable runnable, final long n) {
        if (Build$VERSION.SDK_INT >= 16) {
            Api16Impl.postOnAnimationDelayed(view, runnable, n);
        }
        else {
            view.postDelayed(runnable, ValueAnimator.getFrameDelay() + n);
        }
    }
    
    public static void removeAccessibilityAction(final View view, final int n) {
        if (Build$VERSION.SDK_INT >= 21) {
            removeActionWithId(n, view);
            notifyViewAccessibilityStateChangedIfNeeded(view, 0);
        }
    }
    
    private static void removeActionWithId(final int n, final View view) {
        final List<AccessibilityNodeInfoCompat.AccessibilityActionCompat> actionList = getActionList(view);
        for (int i = 0; i < actionList.size(); ++i) {
            if (((AccessibilityNodeInfoCompat.AccessibilityActionCompat)actionList.get(i)).getId() == n) {
                actionList.remove(i);
                break;
            }
        }
    }
    
    public static void removeOnUnhandledKeyEventListener(final View view, final OnUnhandledKeyEventListenerCompat o) {
        if (Build$VERSION.SDK_INT >= 28) {
            Api28Impl.removeOnUnhandledKeyEventListener(view, o);
            return;
        }
        final ArrayList list = (ArrayList)view.getTag(R.id.tag_unhandled_key_listeners);
        if (list != null) {
            list.remove(o);
            if (list.size() == 0) {
                UnhandledKeyEventManager.unregisterListeningView(view);
            }
        }
    }
    
    public static void replaceAccessibilityAction(final View view, final AccessibilityNodeInfoCompat.AccessibilityActionCompat accessibilityActionCompat, final CharSequence charSequence, final AccessibilityViewCommand accessibilityViewCommand) {
        if (accessibilityViewCommand == null && charSequence == null) {
            removeAccessibilityAction(view, accessibilityActionCompat.getId());
        }
        else {
            addAccessibilityAction(view, accessibilityActionCompat.createReplacementAction(charSequence, accessibilityViewCommand));
        }
    }
    
    public static void requestApplyInsets(final View view) {
        if (Build$VERSION.SDK_INT >= 20) {
            Api20Impl.requestApplyInsets(view);
        }
        else if (Build$VERSION.SDK_INT >= 16) {
            Api16Impl.requestFitSystemWindows(view);
        }
    }
    
    public static <T extends View> T requireViewById(View viewById, final int n) {
        if (Build$VERSION.SDK_INT >= 28) {
            return Api28Impl.requireViewById(viewById, n);
        }
        viewById = viewById.findViewById(n);
        if (viewById != null) {
            return (T)viewById;
        }
        throw new IllegalArgumentException("ID does not reference a View inside this View");
    }
    
    @Deprecated
    public static int resolveSizeAndState(final int n, final int n2, final int n3) {
        return View.resolveSizeAndState(n, n2, n3);
    }
    
    public static boolean restoreDefaultFocus(final View view) {
        if (Build$VERSION.SDK_INT >= 26) {
            return Api26Impl.restoreDefaultFocus(view);
        }
        return view.requestFocus();
    }
    
    public static void saveAttributeDataForStyleable(final View view, final Context context, final int[] array, final AttributeSet set, final TypedArray typedArray, final int n, final int n2) {
        if (Build$VERSION.SDK_INT >= 29) {
            Api29Impl.saveAttributeDataForStyleable(view, context, array, set, typedArray, n, n2);
        }
    }
    
    private static AccessibilityViewProperty<Boolean> screenReaderFocusableProperty() {
        return (AccessibilityViewProperty<Boolean>)new AccessibilityViewProperty<Boolean>(R.id.tag_screen_reader_focusable, Boolean.class, 28) {
            Boolean frameworkGet(final View view) {
                return Api28Impl.isScreenReaderFocusable(view);
            }
            
            void frameworkSet(final View view, final Boolean b) {
                Api28Impl.setScreenReaderFocusable(view, b);
            }
            
            boolean shouldUpdate(final Boolean b, final Boolean b2) {
                return ((AccessibilityViewProperty)this).booleanNullToFalseEquals(b, b2) ^ true;
            }
        };
    }
    
    public static void setAccessibilityDelegate(final View importantForAccessibilityIfNeeded, final AccessibilityDelegateCompat accessibilityDelegateCompat) {
        AccessibilityDelegateCompat accessibilityDelegateCompat2 = accessibilityDelegateCompat;
        if (accessibilityDelegateCompat == null) {
            accessibilityDelegateCompat2 = accessibilityDelegateCompat;
            if (getAccessibilityDelegateInternal(importantForAccessibilityIfNeeded) instanceof AccessibilityDelegateCompat.AccessibilityDelegateAdapter) {
                accessibilityDelegateCompat2 = new AccessibilityDelegateCompat();
            }
        }
        setImportantForAccessibilityIfNeeded(importantForAccessibilityIfNeeded);
        View$AccessibilityDelegate bridge;
        if (accessibilityDelegateCompat2 == null) {
            bridge = null;
        }
        else {
            bridge = accessibilityDelegateCompat2.getBridge();
        }
        importantForAccessibilityIfNeeded.setAccessibilityDelegate(bridge);
    }
    
    public static void setAccessibilityHeading(final View view, final boolean b) {
        accessibilityHeadingProperty().set(view, b);
    }
    
    public static void setAccessibilityLiveRegion(final View view, final int n) {
        if (Build$VERSION.SDK_INT >= 19) {
            Api19Impl.setAccessibilityLiveRegion(view, n);
        }
    }
    
    public static void setAccessibilityPaneTitle(final View view, final CharSequence charSequence) {
        if (Build$VERSION.SDK_INT >= 19) {
            paneTitleProperty().set(view, charSequence);
            if (charSequence != null) {
                ViewCompat.sAccessibilityPaneVisibilityManager.addAccessibilityPane(view);
            }
            else {
                ViewCompat.sAccessibilityPaneVisibilityManager.removeAccessibilityPane(view);
            }
        }
    }
    
    @Deprecated
    public static void setActivated(final View view, final boolean activated) {
        view.setActivated(activated);
    }
    
    @Deprecated
    public static void setAlpha(final View view, final float alpha) {
        view.setAlpha(alpha);
    }
    
    public static void setAutofillHints(final View view, final String... array) {
        if (Build$VERSION.SDK_INT >= 26) {
            Api26Impl.setAutofillHints(view, array);
        }
    }
    
    public static void setAutofillId(final View view, final AutofillIdCompat autofillIdCompat) {
        if (Build$VERSION.SDK_INT >= 28) {
            Api28Impl.setAutofillId(view, autofillIdCompat.toAutofillId());
        }
    }
    
    public static void setBackground(final View view, final Drawable backgroundDrawable) {
        if (Build$VERSION.SDK_INT >= 16) {
            Api16Impl.setBackground(view, backgroundDrawable);
        }
        else {
            view.setBackgroundDrawable(backgroundDrawable);
        }
    }
    
    public static void setBackgroundTintList(final View view, final ColorStateList supportBackgroundTintList) {
        if (Build$VERSION.SDK_INT >= 21) {
            Api21Impl.setBackgroundTintList(view, supportBackgroundTintList);
            if (Build$VERSION.SDK_INT == 21) {
                final Drawable background = view.getBackground();
                final boolean b = Api21Impl.getBackgroundTintList(view) != null || Api21Impl.getBackgroundTintMode(view) != null;
                if (background != null && b) {
                    if (background.isStateful()) {
                        background.setState(view.getDrawableState());
                    }
                    Api16Impl.setBackground(view, background);
                }
            }
        }
        else if (view instanceof TintableBackgroundView) {
            ((TintableBackgroundView)view).setSupportBackgroundTintList(supportBackgroundTintList);
        }
    }
    
    public static void setBackgroundTintMode(final View view, final PorterDuff$Mode supportBackgroundTintMode) {
        if (Build$VERSION.SDK_INT >= 21) {
            Api21Impl.setBackgroundTintMode(view, supportBackgroundTintMode);
            if (Build$VERSION.SDK_INT == 21) {
                final Drawable background = view.getBackground();
                final boolean b = Api21Impl.getBackgroundTintList(view) != null || Api21Impl.getBackgroundTintMode(view) != null;
                if (background != null && b) {
                    if (background.isStateful()) {
                        background.setState(view.getDrawableState());
                    }
                    Api16Impl.setBackground(view, background);
                }
            }
        }
        else if (view instanceof TintableBackgroundView) {
            ((TintableBackgroundView)view).setSupportBackgroundTintMode(supportBackgroundTintMode);
        }
    }
    
    @Deprecated
    public static void setChildrenDrawingOrderEnabled(final ViewGroup obj, final boolean b) {
        if (ViewCompat.sChildrenDrawingOrderMethod == null) {
            try {
                ViewCompat.sChildrenDrawingOrderMethod = ViewGroup.class.getDeclaredMethod("setChildrenDrawingOrderEnabled", Boolean.TYPE);
            }
            catch (final NoSuchMethodException ex) {
                Log.e("ViewCompat", "Unable to find childrenDrawingOrderEnabled", (Throwable)ex);
            }
            ViewCompat.sChildrenDrawingOrderMethod.setAccessible(true);
        }
        try {
            ViewCompat.sChildrenDrawingOrderMethod.invoke(obj, b);
        }
        catch (final InvocationTargetException ex2) {
            Log.e("ViewCompat", "Unable to invoke childrenDrawingOrderEnabled", (Throwable)ex2);
        }
        catch (final IllegalArgumentException ex3) {
            Log.e("ViewCompat", "Unable to invoke childrenDrawingOrderEnabled", (Throwable)ex3);
        }
        catch (final IllegalAccessException ex4) {
            Log.e("ViewCompat", "Unable to invoke childrenDrawingOrderEnabled", (Throwable)ex4);
        }
    }
    
    public static void setClipBounds(final View view, final Rect rect) {
        if (Build$VERSION.SDK_INT >= 18) {
            Api18Impl.setClipBounds(view, rect);
        }
    }
    
    public static void setContentCaptureSession(final View view, final ContentCaptureSessionCompat contentCaptureSessionCompat) {
        if (Build$VERSION.SDK_INT >= 29) {
            Api29Impl.setContentCaptureSession(view, contentCaptureSessionCompat.toContentCaptureSession());
        }
    }
    
    public static void setElevation(final View view, final float n) {
        if (Build$VERSION.SDK_INT >= 21) {
            Api21Impl.setElevation(view, n);
        }
    }
    
    @Deprecated
    public static void setFitsSystemWindows(final View view, final boolean fitsSystemWindows) {
        view.setFitsSystemWindows(fitsSystemWindows);
    }
    
    public static void setFocusedByDefault(final View view, final boolean b) {
        if (Build$VERSION.SDK_INT >= 26) {
            Api26Impl.setFocusedByDefault(view, b);
        }
    }
    
    public static void setHasTransientState(final View view, final boolean b) {
        if (Build$VERSION.SDK_INT >= 16) {
            Api16Impl.setHasTransientState(view, b);
        }
    }
    
    public static void setImportantForAccessibility(final View view, final int n) {
        if (Build$VERSION.SDK_INT >= 19) {
            Api16Impl.setImportantForAccessibility(view, n);
        }
        else if (Build$VERSION.SDK_INT >= 16) {
            int n2;
            if ((n2 = n) == 4) {
                n2 = 2;
            }
            Api16Impl.setImportantForAccessibility(view, n2);
        }
    }
    
    private static void setImportantForAccessibilityIfNeeded(final View view) {
        if (getImportantForAccessibility(view) == 0) {
            setImportantForAccessibility(view, 1);
        }
    }
    
    public static void setImportantForAutofill(final View view, final int n) {
        if (Build$VERSION.SDK_INT >= 26) {
            Api26Impl.setImportantForAutofill(view, n);
        }
    }
    
    public static void setImportantForContentCapture(final View view, final int n) {
        if (Build$VERSION.SDK_INT >= 30) {
            Api30Impl.setImportantForContentCapture(view, n);
        }
    }
    
    public static void setKeyboardNavigationCluster(final View view, final boolean b) {
        if (Build$VERSION.SDK_INT >= 26) {
            Api26Impl.setKeyboardNavigationCluster(view, b);
        }
    }
    
    public static void setLabelFor(final View view, final int n) {
        if (Build$VERSION.SDK_INT >= 17) {
            Api17Impl.setLabelFor(view, n);
        }
    }
    
    public static void setLayerPaint(final View view, final Paint paint) {
        if (Build$VERSION.SDK_INT >= 17) {
            Api17Impl.setLayerPaint(view, paint);
        }
        else {
            view.setLayerType(view.getLayerType(), paint);
            view.invalidate();
        }
    }
    
    @Deprecated
    public static void setLayerType(final View view, final int n, final Paint paint) {
        view.setLayerType(n, paint);
    }
    
    public static void setLayoutDirection(final View view, final int n) {
        if (Build$VERSION.SDK_INT >= 17) {
            Api17Impl.setLayoutDirection(view, n);
        }
    }
    
    public static void setNestedScrollingEnabled(final View view, final boolean nestedScrollingEnabled) {
        if (Build$VERSION.SDK_INT >= 21) {
            Api21Impl.setNestedScrollingEnabled(view, nestedScrollingEnabled);
        }
        else if (view instanceof NestedScrollingChild) {
            ((NestedScrollingChild)view).setNestedScrollingEnabled(nestedScrollingEnabled);
        }
    }
    
    public static void setNextClusterForwardId(final View view, final int n) {
        if (Build$VERSION.SDK_INT >= 26) {
            Api26Impl.setNextClusterForwardId(view, n);
        }
    }
    
    public static void setOnApplyWindowInsetsListener(final View view, final OnApplyWindowInsetsListener onApplyWindowInsetsListener) {
        if (Build$VERSION.SDK_INT >= 21) {
            Api21Impl.setOnApplyWindowInsetsListener(view, onApplyWindowInsetsListener);
        }
    }
    
    public static void setOnReceiveContentListener(final View view, final String[] array, final OnReceiveContentListener onReceiveContentListener) {
        if (Build$VERSION.SDK_INT >= 31) {
            Api31Impl.setOnReceiveContentListener(view, array, onReceiveContentListener);
            return;
        }
        String[] a = null;
        Label_0030: {
            if (array != null) {
                a = array;
                if (array.length != 0) {
                    break Label_0030;
                }
            }
            a = null;
        }
        final int n = 0;
        if (onReceiveContentListener != null) {
            Preconditions.checkArgument(a != null, (Object)"When the listener is set, MIME types must also be set");
        }
        if (a != null) {
            final int length = a.length;
            int n2 = 0;
            int n3;
            while (true) {
                n3 = n;
                if (n2 >= length) {
                    break;
                }
                if (a[n2].startsWith("*")) {
                    n3 = 1;
                    break;
                }
                ++n2;
            }
            final StringBuilder sb = new StringBuilder();
            sb.append("A MIME type set here must not start with *: ");
            sb.append(Arrays.toString(a));
            Preconditions.checkArgument((boolean)((n3 ^ 0x1) != 0x0), (Object)sb.toString());
        }
        view.setTag(R.id.tag_on_receive_content_mime_types, (Object)a);
        view.setTag(R.id.tag_on_receive_content_listener, (Object)onReceiveContentListener);
    }
    
    @Deprecated
    public static void setOverScrollMode(final View view, final int overScrollMode) {
        view.setOverScrollMode(overScrollMode);
    }
    
    public static void setPaddingRelative(final View view, final int n, final int n2, final int n3, final int n4) {
        if (Build$VERSION.SDK_INT >= 17) {
            Api17Impl.setPaddingRelative(view, n, n2, n3, n4);
        }
        else {
            view.setPadding(n, n2, n3, n4);
        }
    }
    
    @Deprecated
    public static void setPivotX(final View view, final float pivotX) {
        view.setPivotX(pivotX);
    }
    
    @Deprecated
    public static void setPivotY(final View view, final float pivotY) {
        view.setPivotY(pivotY);
    }
    
    public static void setPointerIcon(final View view, final PointerIconCompat pointerIconCompat) {
        if (Build$VERSION.SDK_INT >= 24) {
            Object pointerIcon;
            if (pointerIconCompat != null) {
                pointerIcon = pointerIconCompat.getPointerIcon();
            }
            else {
                pointerIcon = null;
            }
            Api24Impl.setPointerIcon(view, (PointerIcon)pointerIcon);
        }
    }
    
    @Deprecated
    public static void setRotation(final View view, final float rotation) {
        view.setRotation(rotation);
    }
    
    @Deprecated
    public static void setRotationX(final View view, final float rotationX) {
        view.setRotationX(rotationX);
    }
    
    @Deprecated
    public static void setRotationY(final View view, final float rotationY) {
        view.setRotationY(rotationY);
    }
    
    @Deprecated
    public static void setSaveFromParentEnabled(final View view, final boolean saveFromParentEnabled) {
        view.setSaveFromParentEnabled(saveFromParentEnabled);
    }
    
    @Deprecated
    public static void setScaleX(final View view, final float scaleX) {
        view.setScaleX(scaleX);
    }
    
    @Deprecated
    public static void setScaleY(final View view, final float scaleY) {
        view.setScaleY(scaleY);
    }
    
    public static void setScreenReaderFocusable(final View view, final boolean b) {
        screenReaderFocusableProperty().set(view, b);
    }
    
    public static void setScrollIndicators(final View view, final int n) {
        if (Build$VERSION.SDK_INT >= 23) {
            Api23Impl.setScrollIndicators(view, n);
        }
    }
    
    public static void setScrollIndicators(final View view, final int n, final int n2) {
        if (Build$VERSION.SDK_INT >= 23) {
            Api23Impl.setScrollIndicators(view, n, n2);
        }
    }
    
    public static void setStateDescription(final View view, final CharSequence charSequence) {
        if (Build$VERSION.SDK_INT >= 19) {
            stateDescriptionProperty().set(view, charSequence);
        }
    }
    
    public static void setSystemGestureExclusionRects(final View view, final List<Rect> list) {
        if (Build$VERSION.SDK_INT >= 29) {
            Api29Impl.setSystemGestureExclusionRects(view, list);
        }
    }
    
    public static void setTooltipText(final View view, final CharSequence charSequence) {
        if (Build$VERSION.SDK_INT >= 26) {
            Api26Impl.setTooltipText(view, charSequence);
        }
    }
    
    public static void setTransitionName(final View key, final String value) {
        if (Build$VERSION.SDK_INT >= 21) {
            Api21Impl.setTransitionName(key, value);
        }
        else {
            if (ViewCompat.sTransitionNameMap == null) {
                ViewCompat.sTransitionNameMap = new WeakHashMap<View, String>();
            }
            ViewCompat.sTransitionNameMap.put(key, value);
        }
    }
    
    @Deprecated
    public static void setTranslationX(final View view, final float translationX) {
        view.setTranslationX(translationX);
    }
    
    @Deprecated
    public static void setTranslationY(final View view, final float translationY) {
        view.setTranslationY(translationY);
    }
    
    public static void setTranslationZ(final View view, final float n) {
        if (Build$VERSION.SDK_INT >= 21) {
            Api21Impl.setTranslationZ(view, n);
        }
    }
    
    public static void setWindowInsetsAnimationCallback(final View view, final WindowInsetsAnimationCompat.Callback callback) {
        WindowInsetsAnimationCompat.setCallback(view, callback);
    }
    
    @Deprecated
    public static void setX(final View view, final float x) {
        view.setX(x);
    }
    
    @Deprecated
    public static void setY(final View view, final float y) {
        view.setY(y);
    }
    
    public static void setZ(final View view, final float n) {
        if (Build$VERSION.SDK_INT >= 21) {
            Api21Impl.setZ(view, n);
        }
    }
    
    public static boolean startDragAndDrop(final View view, final ClipData clipData, final View$DragShadowBuilder view$DragShadowBuilder, final Object o, final int n) {
        if (Build$VERSION.SDK_INT >= 24) {
            return Api24Impl.startDragAndDrop(view, clipData, view$DragShadowBuilder, o, n);
        }
        return view.startDrag(clipData, view$DragShadowBuilder, o, n);
    }
    
    public static boolean startNestedScroll(final View view, final int n) {
        if (Build$VERSION.SDK_INT >= 21) {
            return Api21Impl.startNestedScroll(view, n);
        }
        return view instanceof NestedScrollingChild && ((NestedScrollingChild)view).startNestedScroll(n);
    }
    
    public static boolean startNestedScroll(final View view, final int n, final int n2) {
        if (view instanceof NestedScrollingChild2) {
            return ((NestedScrollingChild2)view).startNestedScroll(n, n2);
        }
        return n2 == 0 && startNestedScroll(view, n);
    }
    
    private static AccessibilityViewProperty<CharSequence> stateDescriptionProperty() {
        return (AccessibilityViewProperty<CharSequence>)new AccessibilityViewProperty<CharSequence>(R.id.tag_state_description, CharSequence.class, 64, 30) {
            CharSequence frameworkGet(final View view) {
                return Api30Impl.getStateDescription(view);
            }
            
            void frameworkSet(final View view, final CharSequence charSequence) {
                Api30Impl.setStateDescription(view, charSequence);
            }
            
            boolean shouldUpdate(final CharSequence charSequence, final CharSequence charSequence2) {
                return TextUtils.equals(charSequence, charSequence2) ^ true;
            }
        };
    }
    
    public static void stopNestedScroll(final View view) {
        if (Build$VERSION.SDK_INT >= 21) {
            Api21Impl.stopNestedScroll(view);
        }
        else if (view instanceof NestedScrollingChild) {
            ((NestedScrollingChild)view).stopNestedScroll();
        }
    }
    
    public static void stopNestedScroll(final View view, final int n) {
        if (view instanceof NestedScrollingChild2) {
            ((NestedScrollingChild2)view).stopNestedScroll(n);
        }
        else if (n == 0) {
            stopNestedScroll(view);
        }
    }
    
    private static void tickleInvalidationFlag(final View view) {
        final float translationY = view.getTranslationY();
        view.setTranslationY(1.0f + translationY);
        view.setTranslationY(translationY);
    }
    
    public static void updateDragShadow(final View view, final View$DragShadowBuilder view$DragShadowBuilder) {
        if (Build$VERSION.SDK_INT >= 24) {
            Api24Impl.updateDragShadow(view, view$DragShadowBuilder);
        }
    }
    
    static class AccessibilityPaneVisibilityManager implements ViewTreeObserver$OnGlobalLayoutListener, View$OnAttachStateChangeListener
    {
        private final WeakHashMap<View, Boolean> mPanesToVisible;
        
        AccessibilityPaneVisibilityManager() {
            this.mPanesToVisible = new WeakHashMap<View, Boolean>();
        }
        
        private void checkPaneVisibility(final View key, final boolean b) {
            final boolean b2 = key.isShown() && key.getWindowVisibility() == 0;
            if (b != b2) {
                int n;
                if (b2) {
                    n = 16;
                }
                else {
                    n = 32;
                }
                ViewCompat.notifyViewAccessibilityStateChangedIfNeeded(key, n);
                this.mPanesToVisible.put(key, b2);
            }
        }
        
        private void registerForLayoutCallback(final View view) {
            view.getViewTreeObserver().addOnGlobalLayoutListener((ViewTreeObserver$OnGlobalLayoutListener)this);
        }
        
        private void unregisterForLayoutCallback(final View view) {
            Api16Impl.removeOnGlobalLayoutListener(view.getViewTreeObserver(), (ViewTreeObserver$OnGlobalLayoutListener)this);
        }
        
        void addAccessibilityPane(final View key) {
            this.mPanesToVisible.put(key, key.isShown() && key.getWindowVisibility() == 0);
            key.addOnAttachStateChangeListener((View$OnAttachStateChangeListener)this);
            if (Api19Impl.isAttachedToWindow(key)) {
                this.registerForLayoutCallback(key);
            }
        }
        
        public void onGlobalLayout() {
            if (Build$VERSION.SDK_INT < 28) {
                for (final Map.Entry<View, V> entry : this.mPanesToVisible.entrySet()) {
                    this.checkPaneVisibility(entry.getKey(), (boolean)entry.getValue());
                }
            }
        }
        
        public void onViewAttachedToWindow(final View view) {
            this.registerForLayoutCallback(view);
        }
        
        public void onViewDetachedFromWindow(final View view) {
        }
        
        void removeAccessibilityPane(final View key) {
            this.mPanesToVisible.remove(key);
            key.removeOnAttachStateChangeListener((View$OnAttachStateChangeListener)this);
            this.unregisterForLayoutCallback(key);
        }
    }
    
    abstract static class AccessibilityViewProperty<T>
    {
        private final int mContentChangeType;
        private final int mFrameworkMinimumSdk;
        private final int mTagKey;
        private final Class<T> mType;
        
        AccessibilityViewProperty(final int n, final Class<T> clazz, final int n2) {
            this(n, clazz, 0, n2);
        }
        
        AccessibilityViewProperty(final int mTagKey, final Class<T> mType, final int mContentChangeType, final int mFrameworkMinimumSdk) {
            this.mTagKey = mTagKey;
            this.mType = mType;
            this.mContentChangeType = mContentChangeType;
            this.mFrameworkMinimumSdk = mFrameworkMinimumSdk;
        }
        
        private boolean extrasAvailable() {
            return Build$VERSION.SDK_INT >= 19;
        }
        
        private boolean frameworkAvailable() {
            return Build$VERSION.SDK_INT >= this.mFrameworkMinimumSdk;
        }
        
        boolean booleanNullToFalseEquals(final Boolean b, final Boolean b2) {
            boolean b3 = true;
            if ((b != null && b) != (b2 != null && b2)) {
                b3 = false;
            }
            return b3;
        }
        
        abstract T frameworkGet(final View p0);
        
        abstract void frameworkSet(final View p0, final T p1);
        
        T get(final View view) {
            if (this.frameworkAvailable()) {
                return this.frameworkGet(view);
            }
            if (this.extrasAvailable()) {
                final Object tag = view.getTag(this.mTagKey);
                if (this.mType.isInstance(tag)) {
                    return (T)tag;
                }
            }
            return null;
        }
        
        void set(final View view, final T t) {
            if (this.frameworkAvailable()) {
                this.frameworkSet(view, t);
            }
            else if (this.extrasAvailable() && this.shouldUpdate(this.get(view), t)) {
                ViewCompat.ensureAccessibilityDelegateCompat(view);
                view.setTag(this.mTagKey, (Object)t);
                ViewCompat.notifyViewAccessibilityStateChangedIfNeeded(view, this.mContentChangeType);
            }
        }
        
        boolean shouldUpdate(final T obj, final T t) {
            return t.equals(obj) ^ true;
        }
    }
    
    static class Api15Impl
    {
        private Api15Impl() {
        }
        
        static boolean hasOnClickListeners(final View view) {
            return view.hasOnClickListeners();
        }
    }
    
    static class Api16Impl
    {
        private Api16Impl() {
        }
        
        static AccessibilityNodeProvider getAccessibilityNodeProvider(final View view) {
            return view.getAccessibilityNodeProvider();
        }
        
        static boolean getFitsSystemWindows(final View view) {
            return view.getFitsSystemWindows();
        }
        
        static int getImportantForAccessibility(final View view) {
            return view.getImportantForAccessibility();
        }
        
        static int getMinimumHeight(final View view) {
            return view.getMinimumHeight();
        }
        
        static int getMinimumWidth(final View view) {
            return view.getMinimumWidth();
        }
        
        static ViewParent getParentForAccessibility(final View view) {
            return view.getParentForAccessibility();
        }
        
        static int getWindowSystemUiVisibility(final View view) {
            return view.getWindowSystemUiVisibility();
        }
        
        static boolean hasOverlappingRendering(final View view) {
            return view.hasOverlappingRendering();
        }
        
        static boolean hasTransientState(final View view) {
            return view.hasTransientState();
        }
        
        static boolean performAccessibilityAction(final View view, final int n, final Bundle bundle) {
            return view.performAccessibilityAction(n, bundle);
        }
        
        static void postInvalidateOnAnimation(final View view) {
            view.postInvalidateOnAnimation();
        }
        
        static void postInvalidateOnAnimation(final View view, final int n, final int n2, final int n3, final int n4) {
            view.postInvalidateOnAnimation(n, n2, n3, n4);
        }
        
        static void postOnAnimation(final View view, final Runnable runnable) {
            view.postOnAnimation(runnable);
        }
        
        static void postOnAnimationDelayed(final View view, final Runnable runnable, final long n) {
            view.postOnAnimationDelayed(runnable, n);
        }
        
        static void removeOnGlobalLayoutListener(final ViewTreeObserver viewTreeObserver, final ViewTreeObserver$OnGlobalLayoutListener viewTreeObserver$OnGlobalLayoutListener) {
            viewTreeObserver.removeOnGlobalLayoutListener(viewTreeObserver$OnGlobalLayoutListener);
        }
        
        static void requestFitSystemWindows(final View view) {
            view.requestFitSystemWindows();
        }
        
        static void setBackground(final View view, final Drawable background) {
            view.setBackground(background);
        }
        
        static void setHasTransientState(final View view, final boolean hasTransientState) {
            view.setHasTransientState(hasTransientState);
        }
        
        static void setImportantForAccessibility(final View view, final int importantForAccessibility) {
            view.setImportantForAccessibility(importantForAccessibility);
        }
    }
    
    static class Api17Impl
    {
        private Api17Impl() {
        }
        
        static int generateViewId() {
            return View.generateViewId();
        }
        
        static Display getDisplay(final View view) {
            return view.getDisplay();
        }
        
        static int getLabelFor(final View view) {
            return view.getLabelFor();
        }
        
        static int getLayoutDirection(final View view) {
            return view.getLayoutDirection();
        }
        
        static int getPaddingEnd(final View view) {
            return view.getPaddingEnd();
        }
        
        static int getPaddingStart(final View view) {
            return view.getPaddingStart();
        }
        
        static boolean isPaddingRelative(final View view) {
            return view.isPaddingRelative();
        }
        
        static void setLabelFor(final View view, final int labelFor) {
            view.setLabelFor(labelFor);
        }
        
        static void setLayerPaint(final View view, final Paint layerPaint) {
            view.setLayerPaint(layerPaint);
        }
        
        static void setLayoutDirection(final View view, final int layoutDirection) {
            view.setLayoutDirection(layoutDirection);
        }
        
        static void setPaddingRelative(final View view, final int n, final int n2, final int n3, final int n4) {
            view.setPaddingRelative(n, n2, n3, n4);
        }
    }
    
    static class Api18Impl
    {
        private Api18Impl() {
        }
        
        static Rect getClipBounds(final View view) {
            return view.getClipBounds();
        }
        
        static boolean isInLayout(final View view) {
            return view.isInLayout();
        }
        
        static void setClipBounds(final View view, final Rect clipBounds) {
            view.setClipBounds(clipBounds);
        }
    }
    
    static class Api19Impl
    {
        private Api19Impl() {
        }
        
        static int getAccessibilityLiveRegion(final View view) {
            return view.getAccessibilityLiveRegion();
        }
        
        static boolean isAttachedToWindow(final View view) {
            return view.isAttachedToWindow();
        }
        
        static boolean isLaidOut(final View view) {
            return view.isLaidOut();
        }
        
        static boolean isLayoutDirectionResolved(final View view) {
            return view.isLayoutDirectionResolved();
        }
        
        static void notifySubtreeAccessibilityStateChanged(final ViewParent viewParent, final View view, final View view2, final int n) {
            viewParent.notifySubtreeAccessibilityStateChanged(view, view2, n);
        }
        
        static void setAccessibilityLiveRegion(final View view, final int accessibilityLiveRegion) {
            view.setAccessibilityLiveRegion(accessibilityLiveRegion);
        }
        
        static void setContentChangeTypes(final AccessibilityEvent accessibilityEvent, final int contentChangeTypes) {
            accessibilityEvent.setContentChangeTypes(contentChangeTypes);
        }
    }
    
    static class Api20Impl
    {
        private Api20Impl() {
        }
        
        static WindowInsets dispatchApplyWindowInsets(final View view, final WindowInsets windowInsets) {
            return view.dispatchApplyWindowInsets(windowInsets);
        }
        
        static WindowInsets onApplyWindowInsets(final View view, final WindowInsets windowInsets) {
            return view.onApplyWindowInsets(windowInsets);
        }
        
        static void requestApplyInsets(final View view) {
            view.requestApplyInsets();
        }
    }
    
    private static class Api21Impl
    {
        static void callCompatInsetAnimationCallback(final WindowInsets windowInsets, final View view) {
            final View$OnApplyWindowInsetsListener view$OnApplyWindowInsetsListener = (View$OnApplyWindowInsetsListener)view.getTag(R.id.tag_window_insets_animation_callback);
            if (view$OnApplyWindowInsetsListener != null) {
                view$OnApplyWindowInsetsListener.onApplyWindowInsets(view, windowInsets);
            }
        }
        
        static WindowInsetsCompat computeSystemWindowInsets(final View view, final WindowInsetsCompat windowInsetsCompat, final Rect rect) {
            final WindowInsets windowInsets = windowInsetsCompat.toWindowInsets();
            if (windowInsets != null) {
                return WindowInsetsCompat.toWindowInsetsCompat(view.computeSystemWindowInsets(windowInsets, rect), view);
            }
            rect.setEmpty();
            return windowInsetsCompat;
        }
        
        static boolean dispatchNestedFling(final View view, final float n, final float n2, final boolean b) {
            return view.dispatchNestedFling(n, n2, b);
        }
        
        static boolean dispatchNestedPreFling(final View view, final float n, final float n2) {
            return view.dispatchNestedPreFling(n, n2);
        }
        
        static boolean dispatchNestedPreScroll(final View view, final int n, final int n2, final int[] array, final int[] array2) {
            return view.dispatchNestedPreScroll(n, n2, array, array2);
        }
        
        static boolean dispatchNestedScroll(final View view, final int n, final int n2, final int n3, final int n4, final int[] array) {
            return view.dispatchNestedScroll(n, n2, n3, n4, array);
        }
        
        static ColorStateList getBackgroundTintList(final View view) {
            return view.getBackgroundTintList();
        }
        
        static PorterDuff$Mode getBackgroundTintMode(final View view) {
            return view.getBackgroundTintMode();
        }
        
        static float getElevation(final View view) {
            return view.getElevation();
        }
        
        public static WindowInsetsCompat getRootWindowInsets(final View view) {
            return WindowInsetsCompat.Api21ReflectionHolder.getRootWindowInsets(view);
        }
        
        static String getTransitionName(final View view) {
            return view.getTransitionName();
        }
        
        static float getTranslationZ(final View view) {
            return view.getTranslationZ();
        }
        
        static float getZ(final View view) {
            return view.getZ();
        }
        
        static boolean hasNestedScrollingParent(final View view) {
            return view.hasNestedScrollingParent();
        }
        
        static boolean isImportantForAccessibility(final View view) {
            return view.isImportantForAccessibility();
        }
        
        static boolean isNestedScrollingEnabled(final View view) {
            return view.isNestedScrollingEnabled();
        }
        
        static void setBackgroundTintList(final View view, final ColorStateList backgroundTintList) {
            view.setBackgroundTintList(backgroundTintList);
        }
        
        static void setBackgroundTintMode(final View view, final PorterDuff$Mode backgroundTintMode) {
            view.setBackgroundTintMode(backgroundTintMode);
        }
        
        static void setElevation(final View view, final float elevation) {
            view.setElevation(elevation);
        }
        
        static void setNestedScrollingEnabled(final View view, final boolean nestedScrollingEnabled) {
            view.setNestedScrollingEnabled(nestedScrollingEnabled);
        }
        
        static void setOnApplyWindowInsetsListener(final View view, final OnApplyWindowInsetsListener onApplyWindowInsetsListener) {
            if (Build$VERSION.SDK_INT < 30) {
                view.setTag(R.id.tag_on_apply_window_listener, (Object)onApplyWindowInsetsListener);
            }
            if (onApplyWindowInsetsListener == null) {
                view.setOnApplyWindowInsetsListener((View$OnApplyWindowInsetsListener)view.getTag(R.id.tag_window_insets_animation_callback));
                return;
            }
            view.setOnApplyWindowInsetsListener((View$OnApplyWindowInsetsListener)new View$OnApplyWindowInsetsListener(view, onApplyWindowInsetsListener) {
                WindowInsetsCompat mLastInsets = null;
                final OnApplyWindowInsetsListener val$listener;
                final View val$v;
                
                public WindowInsets onApplyWindowInsets(final View view, final WindowInsets windowInsets) {
                    final WindowInsetsCompat windowInsetsCompat = WindowInsetsCompat.toWindowInsetsCompat(windowInsets, view);
                    if (Build$VERSION.SDK_INT < 30) {
                        Api21Impl.callCompatInsetAnimationCallback(windowInsets, this.val$v);
                        if (windowInsetsCompat.equals(this.mLastInsets)) {
                            return this.val$listener.onApplyWindowInsets(view, windowInsetsCompat).toWindowInsets();
                        }
                    }
                    this.mLastInsets = windowInsetsCompat;
                    final WindowInsetsCompat onApplyWindowInsets = this.val$listener.onApplyWindowInsets(view, windowInsetsCompat);
                    if (Build$VERSION.SDK_INT >= 30) {
                        return onApplyWindowInsets.toWindowInsets();
                    }
                    ViewCompat.requestApplyInsets(view);
                    return onApplyWindowInsets.toWindowInsets();
                }
            });
        }
        
        static void setTransitionName(final View view, final String transitionName) {
            view.setTransitionName(transitionName);
        }
        
        static void setTranslationZ(final View view, final float translationZ) {
            view.setTranslationZ(translationZ);
        }
        
        static void setZ(final View view, final float z) {
            view.setZ(z);
        }
        
        static boolean startNestedScroll(final View view, final int n) {
            return view.startNestedScroll(n);
        }
        
        static void stopNestedScroll(final View view) {
            view.stopNestedScroll();
        }
    }
    
    private static class Api23Impl
    {
        public static WindowInsetsCompat getRootWindowInsets(final View view) {
            final WindowInsets rootWindowInsets = view.getRootWindowInsets();
            if (rootWindowInsets == null) {
                return null;
            }
            final WindowInsetsCompat windowInsetsCompat = WindowInsetsCompat.toWindowInsetsCompat(rootWindowInsets);
            windowInsetsCompat.setRootWindowInsets(windowInsetsCompat);
            windowInsetsCompat.copyRootViewBounds(view.getRootView());
            return windowInsetsCompat;
        }
        
        static int getScrollIndicators(final View view) {
            return view.getScrollIndicators();
        }
        
        static void setScrollIndicators(final View view, final int scrollIndicators) {
            view.setScrollIndicators(scrollIndicators);
        }
        
        static void setScrollIndicators(final View view, final int n, final int n2) {
            view.setScrollIndicators(n, n2);
        }
    }
    
    static class Api24Impl
    {
        private Api24Impl() {
        }
        
        static void cancelDragAndDrop(final View view) {
            view.cancelDragAndDrop();
        }
        
        static void dispatchFinishTemporaryDetach(final View view) {
            view.dispatchFinishTemporaryDetach();
        }
        
        static void dispatchStartTemporaryDetach(final View view) {
            view.dispatchStartTemporaryDetach();
        }
        
        static void setPointerIcon(final View view, final PointerIcon pointerIcon) {
            view.setPointerIcon(pointerIcon);
        }
        
        static boolean startDragAndDrop(final View view, final ClipData clipData, final View$DragShadowBuilder view$DragShadowBuilder, final Object o, final int n) {
            return view.startDragAndDrop(clipData, view$DragShadowBuilder, o, n);
        }
        
        static void updateDragShadow(final View view, final View$DragShadowBuilder view$DragShadowBuilder) {
            view.updateDragShadow(view$DragShadowBuilder);
        }
    }
    
    static class Api26Impl
    {
        private Api26Impl() {
        }
        
        static void addKeyboardNavigationClusters(final View view, final Collection<View> collection, final int n) {
            view.addKeyboardNavigationClusters((Collection)collection, n);
        }
        
        public static AutofillId getAutofillId(final View view) {
            return view.getAutofillId();
        }
        
        static int getImportantForAutofill(final View view) {
            return view.getImportantForAutofill();
        }
        
        static int getNextClusterForwardId(final View view) {
            return view.getNextClusterForwardId();
        }
        
        static boolean hasExplicitFocusable(final View view) {
            return view.hasExplicitFocusable();
        }
        
        static boolean isFocusedByDefault(final View view) {
            return view.isFocusedByDefault();
        }
        
        static boolean isImportantForAutofill(final View view) {
            return view.isImportantForAutofill();
        }
        
        static boolean isKeyboardNavigationCluster(final View view) {
            return view.isKeyboardNavigationCluster();
        }
        
        static View keyboardNavigationClusterSearch(final View view, final View view2, final int n) {
            return view.keyboardNavigationClusterSearch(view2, n);
        }
        
        static boolean restoreDefaultFocus(final View view) {
            return view.restoreDefaultFocus();
        }
        
        static void setAutofillHints(final View view, final String... autofillHints) {
            view.setAutofillHints(autofillHints);
        }
        
        static void setFocusedByDefault(final View view, final boolean focusedByDefault) {
            view.setFocusedByDefault(focusedByDefault);
        }
        
        static void setImportantForAutofill(final View view, final int importantForAutofill) {
            view.setImportantForAutofill(importantForAutofill);
        }
        
        static void setKeyboardNavigationCluster(final View view, final boolean keyboardNavigationCluster) {
            view.setKeyboardNavigationCluster(keyboardNavigationCluster);
        }
        
        static void setNextClusterForwardId(final View view, final int nextClusterForwardId) {
            view.setNextClusterForwardId(nextClusterForwardId);
        }
        
        static void setTooltipText(final View view, final CharSequence tooltipText) {
            view.setTooltipText(tooltipText);
        }
    }
    
    static class Api28Impl
    {
        private Api28Impl() {
        }
        
        static void addOnUnhandledKeyEventListener(final View view, final OnUnhandledKeyEventListenerCompat obj) {
            SimpleArrayMap simpleArrayMap;
            if ((simpleArrayMap = (SimpleArrayMap)view.getTag(R.id.tag_unhandled_key_listeners)) == null) {
                simpleArrayMap = new SimpleArrayMap();
                view.setTag(R.id.tag_unhandled_key_listeners, (Object)simpleArrayMap);
            }
            Objects.requireNonNull(obj);
            final ViewCompat$Api28Impl$$ExternalSyntheticLambda0 viewCompat$Api28Impl$$ExternalSyntheticLambda0 = new ViewCompat$Api28Impl$$ExternalSyntheticLambda0(obj);
            simpleArrayMap.put(obj, viewCompat$Api28Impl$$ExternalSyntheticLambda0);
            view.addOnUnhandledKeyEventListener((View$OnUnhandledKeyEventListener)viewCompat$Api28Impl$$ExternalSyntheticLambda0);
        }
        
        static CharSequence getAccessibilityPaneTitle(final View view) {
            return view.getAccessibilityPaneTitle();
        }
        
        static boolean isAccessibilityHeading(final View view) {
            return view.isAccessibilityHeading();
        }
        
        static boolean isScreenReaderFocusable(final View view) {
            return view.isScreenReaderFocusable();
        }
        
        static void removeOnUnhandledKeyEventListener(final View view, final OnUnhandledKeyEventListenerCompat onUnhandledKeyEventListenerCompat) {
            final SimpleArrayMap simpleArrayMap = (SimpleArrayMap)view.getTag(R.id.tag_unhandled_key_listeners);
            if (simpleArrayMap == null) {
                return;
            }
            final View$OnUnhandledKeyEventListener view$OnUnhandledKeyEventListener = simpleArrayMap.get(onUnhandledKeyEventListenerCompat);
            if (view$OnUnhandledKeyEventListener != null) {
                view.removeOnUnhandledKeyEventListener(view$OnUnhandledKeyEventListener);
            }
        }
        
        static <T> T requireViewById(final View view, final int n) {
            return (T)view.requireViewById(n);
        }
        
        static void setAccessibilityHeading(final View view, final boolean accessibilityHeading) {
            view.setAccessibilityHeading(accessibilityHeading);
        }
        
        static void setAccessibilityPaneTitle(final View view, final CharSequence accessibilityPaneTitle) {
            view.setAccessibilityPaneTitle(accessibilityPaneTitle);
        }
        
        public static void setAutofillId(final View view, final AutofillId autofillId) {
            view.setAutofillId(autofillId);
        }
        
        static void setScreenReaderFocusable(final View view, final boolean screenReaderFocusable) {
            view.setScreenReaderFocusable(screenReaderFocusable);
        }
    }
    
    private static class Api29Impl
    {
        static View$AccessibilityDelegate getAccessibilityDelegate(final View view) {
            return view.getAccessibilityDelegate();
        }
        
        static ContentCaptureSession getContentCaptureSession(final View view) {
            return view.getContentCaptureSession();
        }
        
        static List<Rect> getSystemGestureExclusionRects(final View view) {
            return view.getSystemGestureExclusionRects();
        }
        
        static void saveAttributeDataForStyleable(final View view, final Context context, final int[] array, final AttributeSet set, final TypedArray typedArray, final int n, final int n2) {
            view.saveAttributeDataForStyleable(context, array, set, typedArray, n, n2);
        }
        
        static void setContentCaptureSession(final View view, final ContentCaptureSession contentCaptureSession) {
            view.setContentCaptureSession(contentCaptureSession);
        }
        
        static void setSystemGestureExclusionRects(final View view, final List<Rect> systemGestureExclusionRects) {
            view.setSystemGestureExclusionRects((List)systemGestureExclusionRects);
        }
    }
    
    private static class Api30Impl
    {
        static int getImportantForContentCapture(final View view) {
            return view.getImportantForContentCapture();
        }
        
        static CharSequence getStateDescription(final View view) {
            return view.getStateDescription();
        }
        
        public static WindowInsetsControllerCompat getWindowInsetsController(final View view) {
            final WindowInsetsController windowInsetsController = view.getWindowInsetsController();
            WindowInsetsControllerCompat windowInsetsControllerCompat;
            if (windowInsetsController != null) {
                windowInsetsControllerCompat = WindowInsetsControllerCompat.toWindowInsetsControllerCompat(windowInsetsController);
            }
            else {
                windowInsetsControllerCompat = null;
            }
            return windowInsetsControllerCompat;
        }
        
        static boolean isImportantForContentCapture(final View view) {
            return view.isImportantForContentCapture();
        }
        
        static void setImportantForContentCapture(final View view, final int importantForContentCapture) {
            view.setImportantForContentCapture(importantForContentCapture);
        }
        
        static void setStateDescription(final View view, final CharSequence stateDescription) {
            view.setStateDescription(stateDescription);
        }
    }
    
    private static final class Api31Impl
    {
        public static String[] getReceiveContentMimeTypes(final View view) {
            return view.getReceiveContentMimeTypes();
        }
        
        public static ContentInfoCompat performReceiveContent(final View view, final ContentInfoCompat contentInfoCompat) {
            final ContentInfo contentInfo = contentInfoCompat.toContentInfo();
            final ContentInfo performReceiveContent = view.performReceiveContent(contentInfo);
            if (performReceiveContent == null) {
                return null;
            }
            if (performReceiveContent == contentInfo) {
                return contentInfoCompat;
            }
            return ContentInfoCompat.toContentInfoCompat(performReceiveContent);
        }
        
        public static void setOnReceiveContentListener(final View view, final String[] array, final OnReceiveContentListener onReceiveContentListener) {
            if (onReceiveContentListener == null) {
                view.setOnReceiveContentListener(array, (android.view.OnReceiveContentListener)null);
            }
            else {
                view.setOnReceiveContentListener(array, (android.view.OnReceiveContentListener)new OnReceiveContentListenerAdapter(onReceiveContentListener));
            }
        }
    }
    
    @Retention(RetentionPolicy.SOURCE)
    public @interface FocusDirection {
    }
    
    @Retention(RetentionPolicy.SOURCE)
    public @interface FocusRealDirection {
    }
    
    @Retention(RetentionPolicy.SOURCE)
    public @interface FocusRelativeDirection {
    }
    
    @Retention(RetentionPolicy.SOURCE)
    public @interface NestedScrollType {
    }
    
    private static final class OnReceiveContentListenerAdapter implements android.view.OnReceiveContentListener
    {
        private final OnReceiveContentListener mJetpackListener;
        
        OnReceiveContentListenerAdapter(final OnReceiveContentListener mJetpackListener) {
            this.mJetpackListener = mJetpackListener;
        }
        
        public ContentInfo onReceiveContent(final View view, final ContentInfo contentInfo) {
            final ContentInfoCompat contentInfoCompat = ContentInfoCompat.toContentInfoCompat(contentInfo);
            final ContentInfoCompat onReceiveContent = this.mJetpackListener.onReceiveContent(view, contentInfoCompat);
            if (onReceiveContent == null) {
                return null;
            }
            if (onReceiveContent == contentInfoCompat) {
                return contentInfo;
            }
            return onReceiveContent.toContentInfo();
        }
    }
    
    public interface OnUnhandledKeyEventListenerCompat
    {
        boolean onUnhandledKeyEvent(final View p0, final KeyEvent p1);
    }
    
    @Retention(RetentionPolicy.SOURCE)
    public @interface ScrollAxis {
    }
    
    @Retention(RetentionPolicy.SOURCE)
    public @interface ScrollIndicators {
    }
    
    static class UnhandledKeyEventManager
    {
        private static final ArrayList<WeakReference<View>> sViewsWithListeners;
        private SparseArray<WeakReference<View>> mCapturedKeys;
        private WeakReference<KeyEvent> mLastDispatchedPreViewKeyEvent;
        private WeakHashMap<View, Boolean> mViewsContainingListeners;
        
        static {
            sViewsWithListeners = new ArrayList<WeakReference<View>>();
        }
        
        UnhandledKeyEventManager() {
            this.mViewsContainingListeners = null;
            this.mCapturedKeys = null;
            this.mLastDispatchedPreViewKeyEvent = null;
        }
        
        static UnhandledKeyEventManager at(final View view) {
            UnhandledKeyEventManager unhandledKeyEventManager;
            if ((unhandledKeyEventManager = (UnhandledKeyEventManager)view.getTag(R.id.tag_unhandled_key_event_manager)) == null) {
                unhandledKeyEventManager = new UnhandledKeyEventManager();
                view.setTag(R.id.tag_unhandled_key_event_manager, (Object)unhandledKeyEventManager);
            }
            return unhandledKeyEventManager;
        }
        
        private View dispatchInOrder(final View key, final KeyEvent keyEvent) {
            final WeakHashMap<View, Boolean> mViewsContainingListeners = this.mViewsContainingListeners;
            if (mViewsContainingListeners != null) {
                if (mViewsContainingListeners.containsKey(key)) {
                    if (key instanceof ViewGroup) {
                        final ViewGroup viewGroup = (ViewGroup)key;
                        for (int i = viewGroup.getChildCount() - 1; i >= 0; --i) {
                            final View dispatchInOrder = this.dispatchInOrder(viewGroup.getChildAt(i), keyEvent);
                            if (dispatchInOrder != null) {
                                return dispatchInOrder;
                            }
                        }
                    }
                    if (this.onUnhandledKeyEvent(key, keyEvent)) {
                        return key;
                    }
                }
            }
            return null;
        }
        
        private SparseArray<WeakReference<View>> getCapturedKeys() {
            if (this.mCapturedKeys == null) {
                this.mCapturedKeys = (SparseArray<WeakReference<View>>)new SparseArray();
            }
            return this.mCapturedKeys;
        }
        
        private boolean onUnhandledKeyEvent(final View view, final KeyEvent keyEvent) {
            final ArrayList list = (ArrayList)view.getTag(R.id.tag_unhandled_key_listeners);
            if (list != null) {
                for (int i = list.size() - 1; i >= 0; --i) {
                    if (((OnUnhandledKeyEventListenerCompat)list.get(i)).onUnhandledKeyEvent(view, keyEvent)) {
                        return true;
                    }
                }
            }
            return false;
        }
        
        private void recalcViewsWithUnhandled() {
            final WeakHashMap<View, Boolean> mViewsContainingListeners = this.mViewsContainingListeners;
            if (mViewsContainingListeners != null) {
                mViewsContainingListeners.clear();
            }
            final ArrayList<WeakReference<View>> sViewsWithListeners = UnhandledKeyEventManager.sViewsWithListeners;
            if (sViewsWithListeners.isEmpty()) {
                return;
            }
            synchronized (sViewsWithListeners) {
                if (this.mViewsContainingListeners == null) {
                    this.mViewsContainingListeners = new WeakHashMap<View, Boolean>();
                }
                for (int i = sViewsWithListeners.size() - 1; i >= 0; --i) {
                    final ArrayList<WeakReference<View>> sViewsWithListeners2 = UnhandledKeyEventManager.sViewsWithListeners;
                    final View key = sViewsWithListeners2.get(i).get();
                    if (key == null) {
                        sViewsWithListeners2.remove(i);
                    }
                    else {
                        this.mViewsContainingListeners.put(key, Boolean.TRUE);
                        for (ViewParent viewParent = key.getParent(); viewParent instanceof View; viewParent = viewParent.getParent()) {
                            this.mViewsContainingListeners.put((View)viewParent, Boolean.TRUE);
                        }
                    }
                }
            }
        }
        
        static void registerListeningView(final View referent) {
            final ArrayList<WeakReference<View>> sViewsWithListeners = UnhandledKeyEventManager.sViewsWithListeners;
            synchronized (sViewsWithListeners) {
                final Iterator<WeakReference<View>> iterator = sViewsWithListeners.iterator();
                while (iterator.hasNext()) {
                    if (iterator.next().get() == referent) {
                        return;
                    }
                }
                UnhandledKeyEventManager.sViewsWithListeners.add(new WeakReference<View>(referent));
            }
        }
        
        static void unregisterListeningView(final View view) {
            final ArrayList<WeakReference<View>> sViewsWithListeners = UnhandledKeyEventManager.sViewsWithListeners;
            monitorenter(sViewsWithListeners);
            int n = 0;
            try {
                while (true) {
                    final ArrayList<WeakReference<View>> sViewsWithListeners2 = UnhandledKeyEventManager.sViewsWithListeners;
                    if (n >= sViewsWithListeners2.size()) {
                        return;
                    }
                    if (((WeakReference)sViewsWithListeners2.get(n)).get() == view) {
                        sViewsWithListeners2.remove(n);
                        return;
                    }
                    ++n;
                }
            }
            finally {
                monitorexit(sViewsWithListeners);
            }
        }
        
        boolean dispatch(View dispatchInOrder, final KeyEvent keyEvent) {
            if (keyEvent.getAction() == 0) {
                this.recalcViewsWithUnhandled();
            }
            dispatchInOrder = this.dispatchInOrder(dispatchInOrder, keyEvent);
            if (keyEvent.getAction() == 0) {
                final int keyCode = keyEvent.getKeyCode();
                if (dispatchInOrder != null && !KeyEvent.isModifierKey(keyCode)) {
                    this.getCapturedKeys().put(keyCode, (Object)new WeakReference(dispatchInOrder));
                }
            }
            return dispatchInOrder != null;
        }
        
        boolean preDispatch(final KeyEvent referent) {
            final WeakReference<KeyEvent> mLastDispatchedPreViewKeyEvent = this.mLastDispatchedPreViewKeyEvent;
            if (mLastDispatchedPreViewKeyEvent != null && mLastDispatchedPreViewKeyEvent.get() == referent) {
                return false;
            }
            this.mLastDispatchedPreViewKeyEvent = new WeakReference<KeyEvent>(referent);
            final WeakReference weakReference = null;
            final SparseArray<WeakReference<View>> capturedKeys = this.getCapturedKeys();
            WeakReference weakReference2 = weakReference;
            if (referent.getAction() == 1) {
                final int indexOfKey = capturedKeys.indexOfKey(referent.getKeyCode());
                weakReference2 = weakReference;
                if (indexOfKey >= 0) {
                    weakReference2 = (WeakReference)capturedKeys.valueAt(indexOfKey);
                    capturedKeys.removeAt(indexOfKey);
                }
            }
            WeakReference weakReference3;
            if ((weakReference3 = weakReference2) == null) {
                weakReference3 = (WeakReference)capturedKeys.get(referent.getKeyCode());
            }
            if (weakReference3 != null) {
                final View view = (View)weakReference3.get();
                if (view != null && ViewCompat.isAttachedToWindow(view)) {
                    this.onUnhandledKeyEvent(view, referent);
                }
                return true;
            }
            return false;
        }
    }
}

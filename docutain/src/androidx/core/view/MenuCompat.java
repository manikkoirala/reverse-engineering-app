// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.view;

import android.view.MenuItem;
import android.os.Build$VERSION;
import androidx.core.internal.view.SupportMenu;
import android.view.Menu;

public final class MenuCompat
{
    private MenuCompat() {
    }
    
    public static void setGroupDividerEnabled(final Menu menu, final boolean groupDividerEnabled) {
        if (menu instanceof SupportMenu) {
            ((SupportMenu)menu).setGroupDividerEnabled(groupDividerEnabled);
        }
        else if (Build$VERSION.SDK_INT >= 28) {
            Api28Impl.setGroupDividerEnabled(menu, groupDividerEnabled);
        }
    }
    
    @Deprecated
    public static void setShowAsAction(final MenuItem menuItem, final int showAsAction) {
        menuItem.setShowAsAction(showAsAction);
    }
    
    static class Api28Impl
    {
        private Api28Impl() {
        }
        
        static void setGroupDividerEnabled(final Menu menu, final boolean groupDividerEnabled) {
            menu.setGroupDividerEnabled(groupDividerEnabled);
        }
    }
}

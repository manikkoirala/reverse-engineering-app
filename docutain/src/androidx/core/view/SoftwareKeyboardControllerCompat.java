// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.view;

import android.view.WindowInsets$Type;
import android.view.WindowInsetsController$OnControllableInsetsChangedListener;
import java.util.concurrent.atomic.AtomicBoolean;
import android.view.inputmethod.InputMethodManager;
import android.view.WindowInsetsController;
import android.os.Build$VERSION;
import android.view.View;

public final class SoftwareKeyboardControllerCompat
{
    private final Impl mImpl;
    
    public SoftwareKeyboardControllerCompat(final View view) {
        if (Build$VERSION.SDK_INT >= 30) {
            this.mImpl = (Impl)new Impl30(view);
        }
        else if (Build$VERSION.SDK_INT >= 20) {
            this.mImpl = (Impl)new Impl20(view);
        }
        else {
            this.mImpl = new Impl();
        }
    }
    
    @Deprecated
    SoftwareKeyboardControllerCompat(final WindowInsetsController windowInsetsController) {
        this.mImpl = (Impl)new Impl30(windowInsetsController);
    }
    
    public void hide() {
        this.mImpl.hide();
    }
    
    public void show() {
        this.mImpl.show();
    }
    
    private static class Impl
    {
        Impl() {
        }
        
        void hide() {
        }
        
        void show() {
        }
    }
    
    private static class Impl20 extends Impl
    {
        private final View mView;
        
        Impl20(final View mView) {
            this.mView = mView;
        }
        
        @Override
        void hide() {
            final View mView = this.mView;
            if (mView != null) {
                ((InputMethodManager)mView.getContext().getSystemService("input_method")).hideSoftInputFromWindow(this.mView.getWindowToken(), 0);
            }
        }
        
        @Override
        void show() {
            View view = this.mView;
            if (view == null) {
                return;
            }
            if (!view.isInEditMode() && !view.onCheckIsTextEditor()) {
                view = view.getRootView().findFocus();
            }
            else {
                view.requestFocus();
            }
            View viewById = view;
            if (view == null) {
                viewById = this.mView.getRootView().findViewById(16908290);
            }
            if (viewById != null && viewById.hasWindowFocus()) {
                viewById.post((Runnable)new SoftwareKeyboardControllerCompat$Impl20$$ExternalSyntheticLambda0(viewById));
            }
        }
    }
    
    private static class Impl30 extends Impl20
    {
        private View mView;
        private WindowInsetsController mWindowInsetsController;
        
        Impl30(final View mView) {
            super(mView);
            this.mView = mView;
        }
        
        Impl30(final WindowInsetsController mWindowInsetsController) {
            super(null);
            this.mWindowInsetsController = mWindowInsetsController;
        }
        
        @Override
        void hide() {
            WindowInsetsController windowInsetsController = this.mWindowInsetsController;
            if (windowInsetsController == null) {
                final View mView = this.mView;
                if (mView != null) {
                    windowInsetsController = mView.getWindowInsetsController();
                }
                else {
                    windowInsetsController = null;
                }
            }
            if (windowInsetsController != null) {
                final AtomicBoolean atomicBoolean = new AtomicBoolean(false);
                final SoftwareKeyboardControllerCompat$Impl30$$ExternalSyntheticLambda0 softwareKeyboardControllerCompat$Impl30$$ExternalSyntheticLambda0 = new SoftwareKeyboardControllerCompat$Impl30$$ExternalSyntheticLambda0(atomicBoolean);
                windowInsetsController.addOnControllableInsetsChangedListener((WindowInsetsController$OnControllableInsetsChangedListener)softwareKeyboardControllerCompat$Impl30$$ExternalSyntheticLambda0);
                if (!atomicBoolean.get()) {
                    final View mView2 = this.mView;
                    if (mView2 != null) {
                        ((InputMethodManager)mView2.getContext().getSystemService("input_method")).hideSoftInputFromWindow(this.mView.getWindowToken(), 0);
                    }
                }
                windowInsetsController.removeOnControllableInsetsChangedListener((WindowInsetsController$OnControllableInsetsChangedListener)softwareKeyboardControllerCompat$Impl30$$ExternalSyntheticLambda0);
                windowInsetsController.hide(WindowInsets$Type.ime());
            }
            else {
                super.hide();
            }
        }
        
        @Override
        void show() {
            if (this.mView != null && Build$VERSION.SDK_INT < 33) {
                ((InputMethodManager)this.mView.getContext().getSystemService("input_method")).isActive();
            }
            WindowInsetsController windowInsetsController = null;
            final WindowInsetsController mWindowInsetsController = this.mWindowInsetsController;
            if (mWindowInsetsController != null) {
                windowInsetsController = mWindowInsetsController;
            }
            else {
                final View mView = this.mView;
                if (mView != null) {
                    windowInsetsController = mView.getWindowInsetsController();
                }
            }
            if (windowInsetsController != null) {
                windowInsetsController.show(WindowInsets$Type.ime());
            }
            else {
                super.show();
            }
        }
    }
}

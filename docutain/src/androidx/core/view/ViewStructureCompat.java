// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.view;

import android.os.Build$VERSION;
import android.view.ViewStructure;

public class ViewStructureCompat
{
    private final Object mWrappedObj;
    
    private ViewStructureCompat(final ViewStructure mWrappedObj) {
        this.mWrappedObj = mWrappedObj;
    }
    
    public static ViewStructureCompat toViewStructureCompat(final ViewStructure viewStructure) {
        return new ViewStructureCompat(viewStructure);
    }
    
    public void setClassName(final String s) {
        if (Build$VERSION.SDK_INT >= 23) {
            Api23Impl.setClassName((ViewStructure)this.mWrappedObj, s);
        }
    }
    
    public void setContentDescription(final CharSequence charSequence) {
        if (Build$VERSION.SDK_INT >= 23) {
            Api23Impl.setContentDescription((ViewStructure)this.mWrappedObj, charSequence);
        }
    }
    
    public void setDimens(final int n, final int n2, final int n3, final int n4, final int n5, final int n6) {
        if (Build$VERSION.SDK_INT >= 23) {
            Api23Impl.setDimens((ViewStructure)this.mWrappedObj, n, n2, n3, n4, n5, n6);
        }
    }
    
    public void setText(final CharSequence charSequence) {
        if (Build$VERSION.SDK_INT >= 23) {
            Api23Impl.setText((ViewStructure)this.mWrappedObj, charSequence);
        }
    }
    
    public ViewStructure toViewStructure() {
        return (ViewStructure)this.mWrappedObj;
    }
    
    private static class Api23Impl
    {
        static void setClassName(final ViewStructure viewStructure, final String className) {
            viewStructure.setClassName(className);
        }
        
        static void setContentDescription(final ViewStructure viewStructure, final CharSequence contentDescription) {
            viewStructure.setContentDescription(contentDescription);
        }
        
        static void setDimens(final ViewStructure viewStructure, final int n, final int n2, final int n3, final int n4, final int n5, final int n6) {
            viewStructure.setDimens(n, n2, n3, n4, n5, n6);
        }
        
        static void setText(final ViewStructure viewStructure, final CharSequence text) {
            viewStructure.setText(text);
        }
    }
}

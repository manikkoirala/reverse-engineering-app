// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.view.autofill;

import android.view.autofill.AutofillId;

public class AutofillIdCompat
{
    private final Object mWrappedObj;
    
    private AutofillIdCompat(final AutofillId mWrappedObj) {
        this.mWrappedObj = mWrappedObj;
    }
    
    public static AutofillIdCompat toAutofillIdCompat(final AutofillId autofillId) {
        return new AutofillIdCompat(autofillId);
    }
    
    public AutofillId toAutofillId() {
        return (AutofillId)this.mWrappedObj;
    }
}

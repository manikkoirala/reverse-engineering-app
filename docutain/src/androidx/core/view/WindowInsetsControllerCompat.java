// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.view;

import android.view.WindowInsetsAnimationController;
import android.view.WindowInsetsAnimationControlListener;
import android.view.WindowInsetsController$OnControllableInsetsChangedListener;
import androidx.collection.SimpleArrayMap;
import android.os.CancellationSignal;
import android.view.animation.Interpolator;
import android.view.WindowInsetsController;
import android.os.Build$VERSION;
import android.view.View;
import android.view.Window;

public final class WindowInsetsControllerCompat
{
    public static final int BEHAVIOR_DEFAULT = 1;
    @Deprecated
    public static final int BEHAVIOR_SHOW_BARS_BY_SWIPE = 1;
    @Deprecated
    public static final int BEHAVIOR_SHOW_BARS_BY_TOUCH = 0;
    public static final int BEHAVIOR_SHOW_TRANSIENT_BARS_BY_SWIPE = 2;
    private final Impl mImpl;
    
    public WindowInsetsControllerCompat(final Window window, final View view) {
        final SoftwareKeyboardControllerCompat softwareKeyboardControllerCompat = new SoftwareKeyboardControllerCompat(view);
        if (Build$VERSION.SDK_INT >= 30) {
            this.mImpl = (Impl)new Impl30(window, this, softwareKeyboardControllerCompat);
        }
        else if (Build$VERSION.SDK_INT >= 26) {
            this.mImpl = (Impl)new Impl26(window, softwareKeyboardControllerCompat);
        }
        else if (Build$VERSION.SDK_INT >= 23) {
            this.mImpl = (Impl)new Impl23(window, softwareKeyboardControllerCompat);
        }
        else if (Build$VERSION.SDK_INT >= 20) {
            this.mImpl = (Impl)new Impl20(window, softwareKeyboardControllerCompat);
        }
        else {
            this.mImpl = new Impl();
        }
    }
    
    @Deprecated
    private WindowInsetsControllerCompat(final WindowInsetsController windowInsetsController) {
        this.mImpl = (Impl)new Impl30(windowInsetsController, this, new SoftwareKeyboardControllerCompat(windowInsetsController));
    }
    
    @Deprecated
    public static WindowInsetsControllerCompat toWindowInsetsControllerCompat(final WindowInsetsController windowInsetsController) {
        return new WindowInsetsControllerCompat(windowInsetsController);
    }
    
    public void addOnControllableInsetsChangedListener(final OnControllableInsetsChangedListener onControllableInsetsChangedListener) {
        this.mImpl.addOnControllableInsetsChangedListener(onControllableInsetsChangedListener);
    }
    
    public void controlWindowInsetsAnimation(final int n, final long n2, final Interpolator interpolator, final CancellationSignal cancellationSignal, final WindowInsetsAnimationControlListenerCompat windowInsetsAnimationControlListenerCompat) {
        this.mImpl.controlWindowInsetsAnimation(n, n2, interpolator, cancellationSignal, windowInsetsAnimationControlListenerCompat);
    }
    
    public int getSystemBarsBehavior() {
        return this.mImpl.getSystemBarsBehavior();
    }
    
    public void hide(final int n) {
        this.mImpl.hide(n);
    }
    
    public boolean isAppearanceLightNavigationBars() {
        return this.mImpl.isAppearanceLightNavigationBars();
    }
    
    public boolean isAppearanceLightStatusBars() {
        return this.mImpl.isAppearanceLightStatusBars();
    }
    
    public void removeOnControllableInsetsChangedListener(final OnControllableInsetsChangedListener onControllableInsetsChangedListener) {
        this.mImpl.removeOnControllableInsetsChangedListener(onControllableInsetsChangedListener);
    }
    
    public void setAppearanceLightNavigationBars(final boolean appearanceLightNavigationBars) {
        this.mImpl.setAppearanceLightNavigationBars(appearanceLightNavigationBars);
    }
    
    public void setAppearanceLightStatusBars(final boolean appearanceLightStatusBars) {
        this.mImpl.setAppearanceLightStatusBars(appearanceLightStatusBars);
    }
    
    public void setSystemBarsBehavior(final int systemBarsBehavior) {
        this.mImpl.setSystemBarsBehavior(systemBarsBehavior);
    }
    
    public void show(final int n) {
        this.mImpl.show(n);
    }
    
    private static class Impl
    {
        Impl() {
        }
        
        void addOnControllableInsetsChangedListener(final OnControllableInsetsChangedListener onControllableInsetsChangedListener) {
        }
        
        void controlWindowInsetsAnimation(final int n, final long n2, final Interpolator interpolator, final CancellationSignal cancellationSignal, final WindowInsetsAnimationControlListenerCompat windowInsetsAnimationControlListenerCompat) {
        }
        
        int getSystemBarsBehavior() {
            return 0;
        }
        
        void hide(final int n) {
        }
        
        public boolean isAppearanceLightNavigationBars() {
            return false;
        }
        
        public boolean isAppearanceLightStatusBars() {
            return false;
        }
        
        void removeOnControllableInsetsChangedListener(final OnControllableInsetsChangedListener onControllableInsetsChangedListener) {
        }
        
        public void setAppearanceLightNavigationBars(final boolean b) {
        }
        
        public void setAppearanceLightStatusBars(final boolean b) {
        }
        
        void setSystemBarsBehavior(final int n) {
        }
        
        void show(final int n) {
        }
    }
    
    private static class Impl20 extends Impl
    {
        private final SoftwareKeyboardControllerCompat mSoftwareKeyboardControllerCompat;
        protected final Window mWindow;
        
        Impl20(final Window mWindow, final SoftwareKeyboardControllerCompat mSoftwareKeyboardControllerCompat) {
            this.mWindow = mWindow;
            this.mSoftwareKeyboardControllerCompat = mSoftwareKeyboardControllerCompat;
        }
        
        private void hideForType(final int n) {
            if (n == 1) {
                this.setSystemUiFlag(4);
                return;
            }
            if (n != 2) {
                if (n == 8) {
                    this.mSoftwareKeyboardControllerCompat.hide();
                }
                return;
            }
            this.setSystemUiFlag(2);
        }
        
        private void showForType(final int n) {
            if (n == 1) {
                this.unsetSystemUiFlag(4);
                this.unsetWindowFlag(1024);
                return;
            }
            if (n != 2) {
                if (n == 8) {
                    this.mSoftwareKeyboardControllerCompat.show();
                }
                return;
            }
            this.unsetSystemUiFlag(2);
        }
        
        @Override
        void addOnControllableInsetsChangedListener(final OnControllableInsetsChangedListener onControllableInsetsChangedListener) {
        }
        
        @Override
        void controlWindowInsetsAnimation(final int n, final long n2, final Interpolator interpolator, final CancellationSignal cancellationSignal, final WindowInsetsAnimationControlListenerCompat windowInsetsAnimationControlListenerCompat) {
        }
        
        @Override
        int getSystemBarsBehavior() {
            return 0;
        }
        
        @Override
        void hide(final int n) {
            for (int i = 1; i <= 256; i <<= 1) {
                if ((n & i) != 0x0) {
                    this.hideForType(i);
                }
            }
        }
        
        @Override
        void removeOnControllableInsetsChangedListener(final OnControllableInsetsChangedListener onControllableInsetsChangedListener) {
        }
        
        @Override
        void setSystemBarsBehavior(final int n) {
            if (n != 0) {
                if (n != 1) {
                    if (n == 2) {
                        this.unsetSystemUiFlag(2048);
                        this.setSystemUiFlag(4096);
                    }
                }
                else {
                    this.unsetSystemUiFlag(4096);
                    this.setSystemUiFlag(2048);
                }
            }
            else {
                this.unsetSystemUiFlag(6144);
            }
        }
        
        protected void setSystemUiFlag(final int n) {
            final View decorView = this.mWindow.getDecorView();
            decorView.setSystemUiVisibility(n | decorView.getSystemUiVisibility());
        }
        
        protected void setWindowFlag(final int n) {
            this.mWindow.addFlags(n);
        }
        
        @Override
        void show(final int n) {
            for (int i = 1; i <= 256; i <<= 1) {
                if ((n & i) != 0x0) {
                    this.showForType(i);
                }
            }
        }
        
        protected void unsetSystemUiFlag(final int n) {
            final View decorView = this.mWindow.getDecorView();
            decorView.setSystemUiVisibility(~n & decorView.getSystemUiVisibility());
        }
        
        protected void unsetWindowFlag(final int n) {
            this.mWindow.clearFlags(n);
        }
    }
    
    private static class Impl23 extends Impl20
    {
        Impl23(final Window window, final SoftwareKeyboardControllerCompat softwareKeyboardControllerCompat) {
            super(window, softwareKeyboardControllerCompat);
        }
        
        @Override
        public boolean isAppearanceLightStatusBars() {
            return (this.mWindow.getDecorView().getSystemUiVisibility() & 0x2000) != 0x0;
        }
        
        @Override
        public void setAppearanceLightStatusBars(final boolean b) {
            if (b) {
                ((Impl20)this).unsetWindowFlag(67108864);
                ((Impl20)this).setWindowFlag(Integer.MIN_VALUE);
                ((Impl20)this).setSystemUiFlag(8192);
            }
            else {
                ((Impl20)this).unsetSystemUiFlag(8192);
            }
        }
    }
    
    private static class Impl26 extends Impl23
    {
        Impl26(final Window window, final SoftwareKeyboardControllerCompat softwareKeyboardControllerCompat) {
            super(window, softwareKeyboardControllerCompat);
        }
        
        @Override
        public boolean isAppearanceLightNavigationBars() {
            return (this.mWindow.getDecorView().getSystemUiVisibility() & 0x10) != 0x0;
        }
        
        @Override
        public void setAppearanceLightNavigationBars(final boolean b) {
            if (b) {
                ((Impl20)this).unsetWindowFlag(134217728);
                ((Impl20)this).setWindowFlag(Integer.MIN_VALUE);
                ((Impl20)this).setSystemUiFlag(16);
            }
            else {
                ((Impl20)this).unsetSystemUiFlag(16);
            }
        }
    }
    
    private static class Impl30 extends Impl
    {
        final WindowInsetsControllerCompat mCompatController;
        final WindowInsetsController mInsetsController;
        private final SimpleArrayMap<OnControllableInsetsChangedListener, WindowInsetsController$OnControllableInsetsChangedListener> mListeners;
        final SoftwareKeyboardControllerCompat mSoftwareKeyboardControllerCompat;
        protected Window mWindow;
        
        Impl30(final Window mWindow, final WindowInsetsControllerCompat windowInsetsControllerCompat, final SoftwareKeyboardControllerCompat softwareKeyboardControllerCompat) {
            this(mWindow.getInsetsController(), windowInsetsControllerCompat, softwareKeyboardControllerCompat);
            this.mWindow = mWindow;
        }
        
        Impl30(final WindowInsetsController mInsetsController, final WindowInsetsControllerCompat mCompatController, final SoftwareKeyboardControllerCompat mSoftwareKeyboardControllerCompat) {
            this.mListeners = new SimpleArrayMap<OnControllableInsetsChangedListener, WindowInsetsController$OnControllableInsetsChangedListener>();
            this.mInsetsController = mInsetsController;
            this.mCompatController = mCompatController;
            this.mSoftwareKeyboardControllerCompat = mSoftwareKeyboardControllerCompat;
        }
        
        @Override
        void addOnControllableInsetsChangedListener(final OnControllableInsetsChangedListener onControllableInsetsChangedListener) {
            if (this.mListeners.containsKey(onControllableInsetsChangedListener)) {
                return;
            }
            final WindowInsetsControllerCompat$Impl30$$ExternalSyntheticLambda0 windowInsetsControllerCompat$Impl30$$ExternalSyntheticLambda0 = new WindowInsetsControllerCompat$Impl30$$ExternalSyntheticLambda0(this, onControllableInsetsChangedListener);
            this.mListeners.put(onControllableInsetsChangedListener, (WindowInsetsController$OnControllableInsetsChangedListener)windowInsetsControllerCompat$Impl30$$ExternalSyntheticLambda0);
            this.mInsetsController.addOnControllableInsetsChangedListener((WindowInsetsController$OnControllableInsetsChangedListener)windowInsetsControllerCompat$Impl30$$ExternalSyntheticLambda0);
        }
        
        @Override
        void controlWindowInsetsAnimation(final int n, final long n2, final Interpolator interpolator, final CancellationSignal cancellationSignal, final WindowInsetsAnimationControlListenerCompat windowInsetsAnimationControlListenerCompat) {
            this.mInsetsController.controlWindowInsetsAnimation(n, n2, interpolator, cancellationSignal, (WindowInsetsAnimationControlListener)new WindowInsetsAnimationControlListener(this, windowInsetsAnimationControlListenerCompat) {
                private WindowInsetsAnimationControllerCompat mCompatAnimController = null;
                final Impl30 this$0;
                final WindowInsetsAnimationControlListenerCompat val$listener;
                
                public void onCancelled(final WindowInsetsAnimationController windowInsetsAnimationController) {
                    final WindowInsetsAnimationControlListenerCompat val$listener = this.val$listener;
                    WindowInsetsAnimationControllerCompat mCompatAnimController;
                    if (windowInsetsAnimationController == null) {
                        mCompatAnimController = null;
                    }
                    else {
                        mCompatAnimController = this.mCompatAnimController;
                    }
                    val$listener.onCancelled(mCompatAnimController);
                }
                
                public void onFinished(final WindowInsetsAnimationController windowInsetsAnimationController) {
                    this.val$listener.onFinished(this.mCompatAnimController);
                }
                
                public void onReady(final WindowInsetsAnimationController windowInsetsAnimationController, final int n) {
                    final WindowInsetsAnimationControllerCompat mCompatAnimController = new WindowInsetsAnimationControllerCompat(windowInsetsAnimationController);
                    this.mCompatAnimController = mCompatAnimController;
                    this.val$listener.onReady(mCompatAnimController, n);
                }
            });
        }
        
        @Override
        int getSystemBarsBehavior() {
            return this.mInsetsController.getSystemBarsBehavior();
        }
        
        @Override
        void hide(final int n) {
            if ((n & 0x8) != 0x0) {
                this.mSoftwareKeyboardControllerCompat.hide();
            }
            this.mInsetsController.hide(n & 0xFFFFFFF7);
        }
        
        @Override
        public boolean isAppearanceLightNavigationBars() {
            return (this.mInsetsController.getSystemBarsAppearance() & 0x10) != 0x0;
        }
        
        @Override
        public boolean isAppearanceLightStatusBars() {
            return (this.mInsetsController.getSystemBarsAppearance() & 0x8) != 0x0;
        }
        
        @Override
        void removeOnControllableInsetsChangedListener(final OnControllableInsetsChangedListener onControllableInsetsChangedListener) {
            final WindowInsetsController$OnControllableInsetsChangedListener windowInsetsController$OnControllableInsetsChangedListener = this.mListeners.remove(onControllableInsetsChangedListener);
            if (windowInsetsController$OnControllableInsetsChangedListener != null) {
                this.mInsetsController.removeOnControllableInsetsChangedListener(windowInsetsController$OnControllableInsetsChangedListener);
            }
        }
        
        @Override
        public void setAppearanceLightNavigationBars(final boolean b) {
            if (b) {
                if (this.mWindow != null) {
                    this.setSystemUiFlag(16);
                }
                this.mInsetsController.setSystemBarsAppearance(16, 16);
            }
            else {
                if (this.mWindow != null) {
                    this.unsetSystemUiFlag(16);
                }
                this.mInsetsController.setSystemBarsAppearance(0, 16);
            }
        }
        
        @Override
        public void setAppearanceLightStatusBars(final boolean b) {
            if (b) {
                if (this.mWindow != null) {
                    this.setSystemUiFlag(8192);
                }
                this.mInsetsController.setSystemBarsAppearance(8, 8);
            }
            else {
                if (this.mWindow != null) {
                    this.unsetSystemUiFlag(8192);
                }
                this.mInsetsController.setSystemBarsAppearance(0, 8);
            }
        }
        
        @Override
        void setSystemBarsBehavior(final int systemBarsBehavior) {
            this.mInsetsController.setSystemBarsBehavior(systemBarsBehavior);
        }
        
        protected void setSystemUiFlag(final int n) {
            final View decorView = this.mWindow.getDecorView();
            decorView.setSystemUiVisibility(n | decorView.getSystemUiVisibility());
        }
        
        @Override
        void show(final int n) {
            if ((n & 0x8) != 0x0) {
                this.mSoftwareKeyboardControllerCompat.show();
            }
            this.mInsetsController.show(n & 0xFFFFFFF7);
        }
        
        protected void unsetSystemUiFlag(final int n) {
            final View decorView = this.mWindow.getDecorView();
            decorView.setSystemUiVisibility(~n & decorView.getSystemUiVisibility());
        }
    }
    
    public interface OnControllableInsetsChangedListener
    {
        void onControllableInsetsChanged(final WindowInsetsControllerCompat p0, final int p1);
    }
}

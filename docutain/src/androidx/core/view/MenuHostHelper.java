// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.view;

import androidx.lifecycle.LifecycleObserver;
import android.view.MenuItem;
import java.util.Iterator;
import android.view.MenuInflater;
import android.view.Menu;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleEventObserver;
import androidx.lifecycle.LifecycleOwner;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;

public class MenuHostHelper
{
    private final CopyOnWriteArrayList<MenuProvider> mMenuProviders;
    private final Runnable mOnInvalidateMenuCallback;
    private final Map<MenuProvider, LifecycleContainer> mProviderToLifecycleContainers;
    
    public MenuHostHelper(final Runnable mOnInvalidateMenuCallback) {
        this.mMenuProviders = new CopyOnWriteArrayList<MenuProvider>();
        this.mProviderToLifecycleContainers = new HashMap<MenuProvider, LifecycleContainer>();
        this.mOnInvalidateMenuCallback = mOnInvalidateMenuCallback;
    }
    
    public void addMenuProvider(final MenuProvider e) {
        this.mMenuProviders.add(e);
        this.mOnInvalidateMenuCallback.run();
    }
    
    public void addMenuProvider(final MenuProvider menuProvider, final LifecycleOwner lifecycleOwner) {
        this.addMenuProvider(menuProvider);
        final Lifecycle lifecycle = lifecycleOwner.getLifecycle();
        final LifecycleContainer lifecycleContainer = this.mProviderToLifecycleContainers.remove(menuProvider);
        if (lifecycleContainer != null) {
            lifecycleContainer.clearObservers();
        }
        this.mProviderToLifecycleContainers.put(menuProvider, new LifecycleContainer(lifecycle, new MenuHostHelper$$ExternalSyntheticLambda0(this, menuProvider)));
    }
    
    public void addMenuProvider(final MenuProvider menuProvider, final LifecycleOwner lifecycleOwner, final Lifecycle.State state) {
        final Lifecycle lifecycle = lifecycleOwner.getLifecycle();
        final LifecycleContainer lifecycleContainer = this.mProviderToLifecycleContainers.remove(menuProvider);
        if (lifecycleContainer != null) {
            lifecycleContainer.clearObservers();
        }
        this.mProviderToLifecycleContainers.put(menuProvider, new LifecycleContainer(lifecycle, new MenuHostHelper$$ExternalSyntheticLambda1(this, state, menuProvider)));
    }
    
    public void onCreateMenu(final Menu menu, final MenuInflater menuInflater) {
        final Iterator<MenuProvider> iterator = this.mMenuProviders.iterator();
        while (iterator.hasNext()) {
            iterator.next().onCreateMenu(menu, menuInflater);
        }
    }
    
    public void onMenuClosed(final Menu menu) {
        final Iterator<MenuProvider> iterator = this.mMenuProviders.iterator();
        while (iterator.hasNext()) {
            iterator.next().onMenuClosed(menu);
        }
    }
    
    public boolean onMenuItemSelected(final MenuItem menuItem) {
        final Iterator<MenuProvider> iterator = this.mMenuProviders.iterator();
        while (iterator.hasNext()) {
            if (iterator.next().onMenuItemSelected(menuItem)) {
                return true;
            }
        }
        return false;
    }
    
    public void onPrepareMenu(final Menu menu) {
        final Iterator<MenuProvider> iterator = this.mMenuProviders.iterator();
        while (iterator.hasNext()) {
            iterator.next().onPrepareMenu(menu);
        }
    }
    
    public void removeMenuProvider(final MenuProvider o) {
        this.mMenuProviders.remove(o);
        final LifecycleContainer lifecycleContainer = this.mProviderToLifecycleContainers.remove(o);
        if (lifecycleContainer != null) {
            lifecycleContainer.clearObservers();
        }
        this.mOnInvalidateMenuCallback.run();
    }
    
    private static class LifecycleContainer
    {
        final Lifecycle mLifecycle;
        private LifecycleEventObserver mObserver;
        
        LifecycleContainer(final Lifecycle mLifecycle, final LifecycleEventObserver mObserver) {
            (this.mLifecycle = mLifecycle).addObserver(this.mObserver = mObserver);
        }
        
        void clearObservers() {
            this.mLifecycle.removeObserver(this.mObserver);
            this.mObserver = null;
        }
    }
}

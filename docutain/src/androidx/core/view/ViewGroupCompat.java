// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.view;

import android.view.accessibility.AccessibilityEvent;
import android.view.View;
import androidx.core.R;
import android.os.Build$VERSION;
import android.view.ViewGroup;

public final class ViewGroupCompat
{
    public static final int LAYOUT_MODE_CLIP_BOUNDS = 0;
    public static final int LAYOUT_MODE_OPTICAL_BOUNDS = 1;
    
    private ViewGroupCompat() {
    }
    
    public static int getLayoutMode(final ViewGroup viewGroup) {
        if (Build$VERSION.SDK_INT >= 18) {
            return Api18Impl.getLayoutMode(viewGroup);
        }
        return 0;
    }
    
    public static int getNestedScrollAxes(final ViewGroup viewGroup) {
        if (Build$VERSION.SDK_INT >= 21) {
            return Api21Impl.getNestedScrollAxes(viewGroup);
        }
        if (viewGroup instanceof NestedScrollingParent) {
            return ((NestedScrollingParent)viewGroup).getNestedScrollAxes();
        }
        return 0;
    }
    
    public static boolean isTransitionGroup(final ViewGroup viewGroup) {
        if (Build$VERSION.SDK_INT >= 21) {
            return Api21Impl.isTransitionGroup(viewGroup);
        }
        final Boolean b = (Boolean)viewGroup.getTag(R.id.tag_transition_group);
        return (b != null && b) || viewGroup.getBackground() != null || ViewCompat.getTransitionName((View)viewGroup) != null;
    }
    
    @Deprecated
    public static boolean onRequestSendAccessibilityEvent(final ViewGroup viewGroup, final View view, final AccessibilityEvent accessibilityEvent) {
        return viewGroup.onRequestSendAccessibilityEvent(view, accessibilityEvent);
    }
    
    public static void setLayoutMode(final ViewGroup viewGroup, final int n) {
        if (Build$VERSION.SDK_INT >= 18) {
            Api18Impl.setLayoutMode(viewGroup, n);
        }
    }
    
    @Deprecated
    public static void setMotionEventSplittingEnabled(final ViewGroup viewGroup, final boolean motionEventSplittingEnabled) {
        viewGroup.setMotionEventSplittingEnabled(motionEventSplittingEnabled);
    }
    
    public static void setTransitionGroup(final ViewGroup viewGroup, final boolean b) {
        if (Build$VERSION.SDK_INT >= 21) {
            Api21Impl.setTransitionGroup(viewGroup, b);
        }
        else {
            viewGroup.setTag(R.id.tag_transition_group, (Object)b);
        }
    }
    
    static class Api18Impl
    {
        private Api18Impl() {
        }
        
        static int getLayoutMode(final ViewGroup viewGroup) {
            return viewGroup.getLayoutMode();
        }
        
        static void setLayoutMode(final ViewGroup viewGroup, final int layoutMode) {
            viewGroup.setLayoutMode(layoutMode);
        }
    }
    
    static class Api21Impl
    {
        private Api21Impl() {
        }
        
        static int getNestedScrollAxes(final ViewGroup viewGroup) {
            return viewGroup.getNestedScrollAxes();
        }
        
        static boolean isTransitionGroup(final ViewGroup viewGroup) {
            return viewGroup.isTransitionGroup();
        }
        
        static void setTransitionGroup(final ViewGroup viewGroup, final boolean transitionGroup) {
            viewGroup.setTransitionGroup(transitionGroup);
        }
    }
}

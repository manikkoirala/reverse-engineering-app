// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.view;

import kotlin.jvm.internal.Intrinsics;
import android.view.ViewGroup$LayoutParams;
import android.view.ViewGroup$MarginLayoutParams;
import android.view.ViewParent;
import kotlin.sequences.SequencesKt;
import kotlin.coroutines.Continuation;
import kotlin.jvm.functions.Function2;
import kotlin.sequences.Sequence;
import android.graphics.Canvas;
import android.graphics.Bitmap;
import android.graphics.Bitmap$Config;
import android.view.View$OnLayoutChangeListener;
import android.view.View$OnAttachStateChangeListener;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import android.view.View;
import kotlin.jvm.functions.Function0;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000j\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\t\n\u0002\u0010\b\n\u0002\b\r\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\n\u001a2\u0010 \u001a\u00020!*\u00020\u00022#\b\u0004\u0010\"\u001a\u001d\u0012\u0013\u0012\u00110\u0002¢\u0006\f\b$\u0012\b\b%\u0012\u0004\b\b(&\u0012\u0004\u0012\u00020!0#H\u0086\b\u001a2\u0010'\u001a\u00020!*\u00020\u00022#\b\u0004\u0010\"\u001a\u001d\u0012\u0013\u0012\u00110\u0002¢\u0006\f\b$\u0012\b\b%\u0012\u0004\b\b(&\u0012\u0004\u0012\u00020!0#H\u0086\b\u001a2\u0010(\u001a\u00020!*\u00020\u00022#\b\u0004\u0010\"\u001a\u001d\u0012\u0013\u0012\u00110\u0002¢\u0006\f\b$\u0012\b\b%\u0012\u0004\b\b(&\u0012\u0004\u0012\u00020!0#H\u0086\b\u001a2\u0010)\u001a\u00020!*\u00020\u00022#\b\u0004\u0010\"\u001a\u001d\u0012\u0013\u0012\u00110\u0002¢\u0006\f\b$\u0012\b\b%\u0012\u0004\b\b(&\u0012\u0004\u0012\u00020!0#H\u0086\b\u001a2\u0010*\u001a\u00020+*\u00020\u00022#\b\u0004\u0010\"\u001a\u001d\u0012\u0013\u0012\u00110\u0002¢\u0006\f\b$\u0012\b\b%\u0012\u0004\b\b(&\u0012\u0004\u0012\u00020!0#H\u0086\b\u001a\u0014\u0010,\u001a\u00020-*\u00020\u00022\b\b\u0002\u0010.\u001a\u00020/\u001a%\u00100\u001a\u000201*\u00020\u00022\u0006\u00102\u001a\u0002032\u000e\b\u0004\u0010\"\u001a\b\u0012\u0004\u0012\u00020!04H\u0086\b\u001a\"\u00105\u001a\u000201*\u00020\u00022\u0006\u00102\u001a\u0002032\f\u0010\"\u001a\b\u0012\u0004\u0012\u00020!04H\u0007\u001a\u0017\u00106\u001a\u00020!*\u00020\u00022\b\b\u0001\u00107\u001a\u00020\u0013H\u0086\b\u001a7\u00108\u001a\u00020!\"\n\b\u0000\u00109\u0018\u0001*\u00020:*\u00020\u00022\u0017\u0010;\u001a\u0013\u0012\u0004\u0012\u0002H9\u0012\u0004\u0012\u00020!0#¢\u0006\u0002\b<H\u0087\b¢\u0006\u0002\b=\u001a&\u00108\u001a\u00020!*\u00020\u00022\u0017\u0010;\u001a\u0013\u0012\u0004\u0012\u00020:\u0012\u0004\u0012\u00020!0#¢\u0006\u0002\b<H\u0086\b\u001a5\u0010>\u001a\u00020!*\u00020\u00022\b\b\u0003\u0010?\u001a\u00020\u00132\b\b\u0003\u0010@\u001a\u00020\u00132\b\b\u0003\u0010A\u001a\u00020\u00132\b\b\u0003\u0010B\u001a\u00020\u0013H\u0086\b\u001a5\u0010C\u001a\u00020!*\u00020\u00022\b\b\u0003\u0010D\u001a\u00020\u00132\b\b\u0003\u0010@\u001a\u00020\u00132\b\b\u0003\u0010E\u001a\u00020\u00132\b\b\u0003\u0010B\u001a\u00020\u0013H\u0087\b\"\u001b\u0010\u0000\u001a\b\u0012\u0004\u0012\u00020\u00020\u0001*\u00020\u00028F¢\u0006\u0006\u001a\u0004\b\u0003\u0010\u0004\"\u001b\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00060\u0001*\u00020\u00028F¢\u0006\u0006\u001a\u0004\b\u0007\u0010\u0004\"*\u0010\n\u001a\u00020\t*\u00020\u00022\u0006\u0010\b\u001a\u00020\t8\u00c6\u0002@\u00c6\u0002X\u0086\u000e¢\u0006\f\u001a\u0004\b\n\u0010\u000b\"\u0004\b\f\u0010\r\"*\u0010\u000e\u001a\u00020\t*\u00020\u00022\u0006\u0010\b\u001a\u00020\t8\u00c6\u0002@\u00c6\u0002X\u0086\u000e¢\u0006\f\u001a\u0004\b\u000e\u0010\u000b\"\u0004\b\u000f\u0010\r\"*\u0010\u0010\u001a\u00020\t*\u00020\u00022\u0006\u0010\b\u001a\u00020\t8\u00c6\u0002@\u00c6\u0002X\u0086\u000e¢\u0006\f\u001a\u0004\b\u0010\u0010\u000b\"\u0004\b\u0011\u0010\r\"\u0016\u0010\u0012\u001a\u00020\u0013*\u00020\u00028\u00c6\u0002¢\u0006\u0006\u001a\u0004\b\u0014\u0010\u0015\"\u0016\u0010\u0016\u001a\u00020\u0013*\u00020\u00028\u00c6\u0002¢\u0006\u0006\u001a\u0004\b\u0017\u0010\u0015\"\u0016\u0010\u0018\u001a\u00020\u0013*\u00020\u00028\u00c6\u0002¢\u0006\u0006\u001a\u0004\b\u0019\u0010\u0015\"\u0016\u0010\u001a\u001a\u00020\u0013*\u00020\u00028\u00c6\u0002¢\u0006\u0006\u001a\u0004\b\u001b\u0010\u0015\"\u0016\u0010\u001c\u001a\u00020\u0013*\u00020\u00028\u00c6\u0002¢\u0006\u0006\u001a\u0004\b\u001d\u0010\u0015\"\u0016\u0010\u001e\u001a\u00020\u0013*\u00020\u00028\u00c6\u0002¢\u0006\u0006\u001a\u0004\b\u001f\u0010\u0015¨\u0006F" }, d2 = { "allViews", "Lkotlin/sequences/Sequence;", "Landroid/view/View;", "getAllViews", "(Landroid/view/View;)Lkotlin/sequences/Sequence;", "ancestors", "Landroid/view/ViewParent;", "getAncestors", "value", "", "isGone", "(Landroid/view/View;)Z", "setGone", "(Landroid/view/View;Z)V", "isInvisible", "setInvisible", "isVisible", "setVisible", "marginBottom", "", "getMarginBottom", "(Landroid/view/View;)I", "marginEnd", "getMarginEnd", "marginLeft", "getMarginLeft", "marginRight", "getMarginRight", "marginStart", "getMarginStart", "marginTop", "getMarginTop", "doOnAttach", "", "action", "Lkotlin/Function1;", "Lkotlin/ParameterName;", "name", "view", "doOnDetach", "doOnLayout", "doOnNextLayout", "doOnPreDraw", "Landroidx/core/view/OneShotPreDrawListener;", "drawToBitmap", "Landroid/graphics/Bitmap;", "config", "Landroid/graphics/Bitmap$Config;", "postDelayed", "Ljava/lang/Runnable;", "delayInMillis", "", "Lkotlin/Function0;", "postOnAnimationDelayed", "setPadding", "size", "updateLayoutParams", "T", "Landroid/view/ViewGroup$LayoutParams;", "block", "Lkotlin/ExtensionFunctionType;", "updateLayoutParamsTyped", "updatePadding", "left", "top", "right", "bottom", "updatePaddingRelative", "start", "end", "core-ktx_release" }, k = 2, mv = { 1, 8, 0 }, xi = 48)
public final class ViewKt
{
    public static final void doOnAttach(final View view, final Function1<? super View, Unit> function1) {
        if (ViewCompat.isAttachedToWindow(view)) {
            function1.invoke((Object)view);
        }
        else {
            view.addOnAttachStateChangeListener((View$OnAttachStateChangeListener)new ViewKt$doOnAttach.ViewKt$doOnAttach$1(view, (Function1)function1));
        }
    }
    
    public static final void doOnDetach(final View view, final Function1<? super View, Unit> function1) {
        if (!ViewCompat.isAttachedToWindow(view)) {
            function1.invoke((Object)view);
        }
        else {
            view.addOnAttachStateChangeListener((View$OnAttachStateChangeListener)new ViewKt$doOnDetach.ViewKt$doOnDetach$1(view, (Function1)function1));
        }
    }
    
    public static final void doOnLayout(final View view, final Function1<? super View, Unit> function1) {
        if (ViewCompat.isLaidOut(view) && !view.isLayoutRequested()) {
            function1.invoke((Object)view);
        }
        else {
            view.addOnLayoutChangeListener((View$OnLayoutChangeListener)new ViewKt$doOnLayout$$inlined$doOnNextLayout.ViewKt$doOnLayout$$inlined$doOnNextLayout$1((Function1)function1));
        }
    }
    
    public static final void doOnNextLayout(final View view, final Function1<? super View, Unit> function1) {
        view.addOnLayoutChangeListener((View$OnLayoutChangeListener)new ViewKt$doOnNextLayout.ViewKt$doOnNextLayout$1((Function1)function1));
    }
    
    public static final OneShotPreDrawListener doOnPreDraw(final View view, final Function1<? super View, Unit> function1) {
        return OneShotPreDrawListener.add(view, (Runnable)new ViewKt$doOnPreDraw.ViewKt$doOnPreDraw$1((Function1)function1, view));
    }
    
    public static final Bitmap drawToBitmap(final View view, final Bitmap$Config bitmap$Config) {
        if (ViewCompat.isLaidOut(view)) {
            final Bitmap bitmap = Bitmap.createBitmap(view.getWidth(), view.getHeight(), bitmap$Config);
            final Canvas canvas = new Canvas(bitmap);
            canvas.translate(-(float)view.getScrollX(), -(float)view.getScrollY());
            view.draw(canvas);
            return bitmap;
        }
        throw new IllegalStateException("View needs to be laid out before calling drawToBitmap()");
    }
    
    public static final Sequence<View> getAllViews(final View view) {
        return (Sequence<View>)SequencesKt.sequence((Function2)new ViewKt$allViews.ViewKt$allViews$1(view, (Continuation)null));
    }
    
    public static final Sequence<ViewParent> getAncestors(final View view) {
        return (Sequence<ViewParent>)SequencesKt.generateSequence((Object)view.getParent(), (Function1)ViewKt$ancestors.ViewKt$ancestors$1.INSTANCE);
    }
    
    public static final int getMarginBottom(final View view) {
        final ViewGroup$LayoutParams layoutParams = view.getLayoutParams();
        ViewGroup$MarginLayoutParams viewGroup$MarginLayoutParams;
        if (layoutParams instanceof ViewGroup$MarginLayoutParams) {
            viewGroup$MarginLayoutParams = (ViewGroup$MarginLayoutParams)layoutParams;
        }
        else {
            viewGroup$MarginLayoutParams = null;
        }
        int bottomMargin;
        if (viewGroup$MarginLayoutParams != null) {
            bottomMargin = viewGroup$MarginLayoutParams.bottomMargin;
        }
        else {
            bottomMargin = 0;
        }
        return bottomMargin;
    }
    
    public static final int getMarginEnd(final View view) {
        final ViewGroup$LayoutParams layoutParams = view.getLayoutParams();
        int marginEnd;
        if (layoutParams instanceof ViewGroup$MarginLayoutParams) {
            marginEnd = MarginLayoutParamsCompat.getMarginEnd((ViewGroup$MarginLayoutParams)layoutParams);
        }
        else {
            marginEnd = 0;
        }
        return marginEnd;
    }
    
    public static final int getMarginLeft(final View view) {
        final ViewGroup$LayoutParams layoutParams = view.getLayoutParams();
        ViewGroup$MarginLayoutParams viewGroup$MarginLayoutParams;
        if (layoutParams instanceof ViewGroup$MarginLayoutParams) {
            viewGroup$MarginLayoutParams = (ViewGroup$MarginLayoutParams)layoutParams;
        }
        else {
            viewGroup$MarginLayoutParams = null;
        }
        int leftMargin;
        if (viewGroup$MarginLayoutParams != null) {
            leftMargin = viewGroup$MarginLayoutParams.leftMargin;
        }
        else {
            leftMargin = 0;
        }
        return leftMargin;
    }
    
    public static final int getMarginRight(final View view) {
        final ViewGroup$LayoutParams layoutParams = view.getLayoutParams();
        ViewGroup$MarginLayoutParams viewGroup$MarginLayoutParams;
        if (layoutParams instanceof ViewGroup$MarginLayoutParams) {
            viewGroup$MarginLayoutParams = (ViewGroup$MarginLayoutParams)layoutParams;
        }
        else {
            viewGroup$MarginLayoutParams = null;
        }
        int rightMargin;
        if (viewGroup$MarginLayoutParams != null) {
            rightMargin = viewGroup$MarginLayoutParams.rightMargin;
        }
        else {
            rightMargin = 0;
        }
        return rightMargin;
    }
    
    public static final int getMarginStart(final View view) {
        final ViewGroup$LayoutParams layoutParams = view.getLayoutParams();
        int marginStart;
        if (layoutParams instanceof ViewGroup$MarginLayoutParams) {
            marginStart = MarginLayoutParamsCompat.getMarginStart((ViewGroup$MarginLayoutParams)layoutParams);
        }
        else {
            marginStart = 0;
        }
        return marginStart;
    }
    
    public static final int getMarginTop(final View view) {
        final ViewGroup$LayoutParams layoutParams = view.getLayoutParams();
        ViewGroup$MarginLayoutParams viewGroup$MarginLayoutParams;
        if (layoutParams instanceof ViewGroup$MarginLayoutParams) {
            viewGroup$MarginLayoutParams = (ViewGroup$MarginLayoutParams)layoutParams;
        }
        else {
            viewGroup$MarginLayoutParams = null;
        }
        int topMargin;
        if (viewGroup$MarginLayoutParams != null) {
            topMargin = viewGroup$MarginLayoutParams.topMargin;
        }
        else {
            topMargin = 0;
        }
        return topMargin;
    }
    
    public static final boolean isGone(final View view) {
        return view.getVisibility() == 8;
    }
    
    public static final boolean isInvisible(final View view) {
        return view.getVisibility() == 4;
    }
    
    public static final boolean isVisible(final View view) {
        return view.getVisibility() == 0;
    }
    
    public static final Runnable postDelayed(final View view, final long n, final Function0<Unit> function0) {
        final Runnable runnable = (Runnable)new ViewKt$postDelayed$runnable.ViewKt$postDelayed$runnable$1((Function0)function0);
        view.postDelayed(runnable, n);
        return runnable;
    }
    
    public static final Runnable postOnAnimationDelayed(final View view, final long n, final Function0<Unit> function0) {
        final ViewKt$$ExternalSyntheticLambda0 viewKt$$ExternalSyntheticLambda0 = new ViewKt$$ExternalSyntheticLambda0(function0);
        Api16Impl.postOnAnimationDelayed(view, viewKt$$ExternalSyntheticLambda0, n);
        return viewKt$$ExternalSyntheticLambda0;
    }
    
    private static final void postOnAnimationDelayed$lambda$1(final Function0 function0) {
        function0.invoke();
    }
    
    public static final void setGone(final View view, final boolean b) {
        int visibility;
        if (b) {
            visibility = 8;
        }
        else {
            visibility = 0;
        }
        view.setVisibility(visibility);
    }
    
    public static final void setInvisible(final View view, final boolean b) {
        int visibility;
        if (b) {
            visibility = 4;
        }
        else {
            visibility = 0;
        }
        view.setVisibility(visibility);
    }
    
    public static final void setPadding(final View view, final int n) {
        view.setPadding(n, n, n, n);
    }
    
    public static final void setVisible(final View view, final boolean b) {
        int visibility;
        if (b) {
            visibility = 0;
        }
        else {
            visibility = 8;
        }
        view.setVisibility(visibility);
    }
    
    public static final void updateLayoutParams(final View view, final Function1<? super ViewGroup$LayoutParams, Unit> function1) {
        final ViewGroup$LayoutParams layoutParams = view.getLayoutParams();
        if (layoutParams != null) {
            function1.invoke((Object)layoutParams);
            view.setLayoutParams(layoutParams);
            return;
        }
        throw new NullPointerException("null cannot be cast to non-null type android.view.ViewGroup.LayoutParams");
    }
    
    public static final void updatePadding(final View view, final int n, final int n2, final int n3, final int n4) {
        view.setPadding(n, n2, n3, n4);
    }
    
    public static final void updatePaddingRelative(final View view, final int n, final int n2, final int n3, final int n4) {
        view.setPaddingRelative(n, n2, n3, n4);
    }
}

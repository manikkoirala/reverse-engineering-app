// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.view;

import java.util.Collections;
import androidx.core.util.ObjectsCompat;
import java.util.ArrayList;
import androidx.core.graphics.Insets;
import android.os.Build$VERSION;
import java.util.List;
import android.graphics.Rect;
import android.view.DisplayCutout;

public final class DisplayCutoutCompat
{
    private final DisplayCutout mDisplayCutout;
    
    public DisplayCutoutCompat(final Rect rect, final List<Rect> list) {
        DisplayCutout displayCutout;
        if (Build$VERSION.SDK_INT >= 28) {
            displayCutout = Api28Impl.createDisplayCutout(rect, list);
        }
        else {
            displayCutout = null;
        }
        this(displayCutout);
    }
    
    private DisplayCutoutCompat(final DisplayCutout mDisplayCutout) {
        this.mDisplayCutout = mDisplayCutout;
    }
    
    public DisplayCutoutCompat(final Insets insets, final Rect rect, final Rect rect2, final Rect rect3, final Rect rect4, final Insets insets2) {
        this(constructDisplayCutout(insets, rect, rect2, rect3, rect4, insets2));
    }
    
    private static DisplayCutout constructDisplayCutout(final Insets insets, final Rect e, final Rect e2, final Rect e3, final Rect e4, final Insets insets2) {
        if (Build$VERSION.SDK_INT >= 30) {
            return Api30Impl.createDisplayCutout(insets.toPlatformInsets(), e, e2, e3, e4, insets2.toPlatformInsets());
        }
        if (Build$VERSION.SDK_INT >= 29) {
            return Api29Impl.createDisplayCutout(insets.toPlatformInsets(), e, e2, e3, e4);
        }
        if (Build$VERSION.SDK_INT >= 28) {
            final Rect rect = new Rect(insets.left, insets.top, insets.right, insets.bottom);
            final ArrayList list = new ArrayList();
            if (e != null) {
                list.add(e);
            }
            if (e2 != null) {
                list.add(e2);
            }
            if (e3 != null) {
                list.add(e3);
            }
            if (e4 != null) {
                list.add(e4);
            }
            return Api28Impl.createDisplayCutout(rect, list);
        }
        return null;
    }
    
    static DisplayCutoutCompat wrap(final DisplayCutout displayCutout) {
        DisplayCutoutCompat displayCutoutCompat;
        if (displayCutout == null) {
            displayCutoutCompat = null;
        }
        else {
            displayCutoutCompat = new DisplayCutoutCompat(displayCutout);
        }
        return displayCutoutCompat;
    }
    
    @Override
    public boolean equals(final Object o) {
        return this == o || (o != null && this.getClass() == o.getClass() && ObjectsCompat.equals(this.mDisplayCutout, ((DisplayCutoutCompat)o).mDisplayCutout));
    }
    
    public List<Rect> getBoundingRects() {
        if (Build$VERSION.SDK_INT >= 28) {
            return Api28Impl.getBoundingRects(this.mDisplayCutout);
        }
        return Collections.emptyList();
    }
    
    public int getSafeInsetBottom() {
        if (Build$VERSION.SDK_INT >= 28) {
            return Api28Impl.getSafeInsetBottom(this.mDisplayCutout);
        }
        return 0;
    }
    
    public int getSafeInsetLeft() {
        if (Build$VERSION.SDK_INT >= 28) {
            return Api28Impl.getSafeInsetLeft(this.mDisplayCutout);
        }
        return 0;
    }
    
    public int getSafeInsetRight() {
        if (Build$VERSION.SDK_INT >= 28) {
            return Api28Impl.getSafeInsetRight(this.mDisplayCutout);
        }
        return 0;
    }
    
    public int getSafeInsetTop() {
        if (Build$VERSION.SDK_INT >= 28) {
            return Api28Impl.getSafeInsetTop(this.mDisplayCutout);
        }
        return 0;
    }
    
    public Insets getWaterfallInsets() {
        if (Build$VERSION.SDK_INT >= 30) {
            return Insets.toCompatInsets(Api30Impl.getWaterfallInsets(this.mDisplayCutout));
        }
        return Insets.NONE;
    }
    
    @Override
    public int hashCode() {
        final DisplayCutout mDisplayCutout = this.mDisplayCutout;
        int hashCode;
        if (mDisplayCutout == null) {
            hashCode = 0;
        }
        else {
            hashCode = mDisplayCutout.hashCode();
        }
        return hashCode;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("DisplayCutoutCompat{");
        sb.append(this.mDisplayCutout);
        sb.append("}");
        return sb.toString();
    }
    
    DisplayCutout unwrap() {
        return this.mDisplayCutout;
    }
    
    static class Api28Impl
    {
        private Api28Impl() {
        }
        
        static DisplayCutout createDisplayCutout(final Rect rect, final List<Rect> list) {
            return new DisplayCutout(rect, (List)list);
        }
        
        static List<Rect> getBoundingRects(final DisplayCutout displayCutout) {
            return displayCutout.getBoundingRects();
        }
        
        static int getSafeInsetBottom(final DisplayCutout displayCutout) {
            return displayCutout.getSafeInsetBottom();
        }
        
        static int getSafeInsetLeft(final DisplayCutout displayCutout) {
            return displayCutout.getSafeInsetLeft();
        }
        
        static int getSafeInsetRight(final DisplayCutout displayCutout) {
            return displayCutout.getSafeInsetRight();
        }
        
        static int getSafeInsetTop(final DisplayCutout displayCutout) {
            return displayCutout.getSafeInsetTop();
        }
    }
    
    static class Api29Impl
    {
        private Api29Impl() {
        }
        
        static DisplayCutout createDisplayCutout(final android.graphics.Insets insets, final Rect rect, final Rect rect2, final Rect rect3, final Rect rect4) {
            return new DisplayCutout(insets, rect, rect2, rect3, rect4);
        }
    }
    
    static class Api30Impl
    {
        private Api30Impl() {
        }
        
        static DisplayCutout createDisplayCutout(final android.graphics.Insets insets, final Rect rect, final Rect rect2, final Rect rect3, final Rect rect4, final android.graphics.Insets insets2) {
            return new DisplayCutout(insets, rect, rect2, rect3, rect4, insets2);
        }
        
        static android.graphics.Insets getWaterfallInsets(final DisplayCutout displayCutout) {
            return displayCutout.getWaterfallInsets();
        }
    }
}

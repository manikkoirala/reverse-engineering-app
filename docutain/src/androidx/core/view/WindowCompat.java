// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.view;

import android.os.Build$VERSION;
import android.view.View;
import android.view.Window;

public final class WindowCompat
{
    public static final int FEATURE_ACTION_BAR = 8;
    public static final int FEATURE_ACTION_BAR_OVERLAY = 9;
    public static final int FEATURE_ACTION_MODE_OVERLAY = 10;
    
    private WindowCompat() {
    }
    
    public static WindowInsetsControllerCompat getInsetsController(final Window window, final View view) {
        return new WindowInsetsControllerCompat(window, view);
    }
    
    public static <T extends View> T requireViewById(final Window window, final int n) {
        if (Build$VERSION.SDK_INT >= 28) {
            return Api28Impl.requireViewById(window, n);
        }
        final View viewById = window.findViewById(n);
        if (viewById != null) {
            return (T)viewById;
        }
        throw new IllegalArgumentException("ID does not reference a View inside this Window");
    }
    
    public static void setDecorFitsSystemWindows(final Window window, final boolean b) {
        if (Build$VERSION.SDK_INT >= 30) {
            Api30Impl.setDecorFitsSystemWindows(window, b);
        }
        else if (Build$VERSION.SDK_INT >= 16) {
            Api16Impl.setDecorFitsSystemWindows(window, b);
        }
    }
    
    static class Api16Impl
    {
        private Api16Impl() {
        }
        
        static void setDecorFitsSystemWindows(final Window window, final boolean b) {
            final View decorView = window.getDecorView();
            final int systemUiVisibility = decorView.getSystemUiVisibility();
            int systemUiVisibility2;
            if (b) {
                systemUiVisibility2 = (systemUiVisibility & 0xFFFFF8FF);
            }
            else {
                systemUiVisibility2 = (systemUiVisibility | 0x700);
            }
            decorView.setSystemUiVisibility(systemUiVisibility2);
        }
    }
    
    static class Api28Impl
    {
        private Api28Impl() {
        }
        
        static <T> T requireViewById(final Window window, final int n) {
            return (T)window.requireViewById(n);
        }
    }
    
    static class Api30Impl
    {
        private Api30Impl() {
        }
        
        static void setDecorFitsSystemWindows(final Window window, final boolean decorFitsSystemWindows) {
            window.setDecorFitsSystemWindows(decorFitsSystemWindows);
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.view.accessibility;

import android.text.Spannable;
import android.graphics.Region;
import java.util.Map;
import java.time.Duration;
import android.view.accessibility.AccessibilityNodeInfo$CollectionItemInfo$Builder;
import android.util.Log;
import android.os.Parcelable;
import android.view.accessibility.AccessibilityNodeInfo$TouchDelegateInfo;
import android.text.SpannableString;
import android.text.TextUtils;
import android.view.accessibility.AccessibilityNodeInfo$RangeInfo;
import android.view.accessibility.AccessibilityNodeInfo$ExtraRenderingInfo;
import android.view.accessibility.AccessibilityNodeInfo$CollectionItemInfo;
import android.view.accessibility.AccessibilityNodeInfo$CollectionInfo;
import android.graphics.Rect;
import java.util.Iterator;
import java.util.Collections;
import android.view.accessibility.AccessibilityNodeInfo$AccessibilityAction;
import androidx.core.R;
import java.lang.ref.WeakReference;
import android.util.SparseArray;
import android.view.View;
import android.os.Bundle;
import java.util.ArrayList;
import java.util.List;
import android.os.Build$VERSION;
import android.text.Spanned;
import android.text.style.ClickableSpan;
import android.view.accessibility.AccessibilityNodeInfo;

public class AccessibilityNodeInfoCompat
{
    public static final int ACTION_ACCESSIBILITY_FOCUS = 64;
    public static final String ACTION_ARGUMENT_COLUMN_INT = "android.view.accessibility.action.ARGUMENT_COLUMN_INT";
    public static final String ACTION_ARGUMENT_DIRECTION_INT = "androidx.core.view.accessibility.action.ARGUMENT_DIRECTION_INT";
    public static final String ACTION_ARGUMENT_EXTEND_SELECTION_BOOLEAN = "ACTION_ARGUMENT_EXTEND_SELECTION_BOOLEAN";
    public static final String ACTION_ARGUMENT_HTML_ELEMENT_STRING = "ACTION_ARGUMENT_HTML_ELEMENT_STRING";
    public static final String ACTION_ARGUMENT_MOVEMENT_GRANULARITY_INT = "ACTION_ARGUMENT_MOVEMENT_GRANULARITY_INT";
    public static final String ACTION_ARGUMENT_MOVE_WINDOW_X = "ACTION_ARGUMENT_MOVE_WINDOW_X";
    public static final String ACTION_ARGUMENT_MOVE_WINDOW_Y = "ACTION_ARGUMENT_MOVE_WINDOW_Y";
    public static final String ACTION_ARGUMENT_PRESS_AND_HOLD_DURATION_MILLIS_INT = "android.view.accessibility.action.ARGUMENT_PRESS_AND_HOLD_DURATION_MILLIS_INT";
    public static final String ACTION_ARGUMENT_PROGRESS_VALUE = "android.view.accessibility.action.ARGUMENT_PROGRESS_VALUE";
    public static final String ACTION_ARGUMENT_ROW_INT = "android.view.accessibility.action.ARGUMENT_ROW_INT";
    public static final String ACTION_ARGUMENT_SCROLL_AMOUNT_FLOAT = "androidx.core.view.accessibility.action.ARGUMENT_SCROLL_AMOUNT_FLOAT";
    public static final String ACTION_ARGUMENT_SELECTION_END_INT = "ACTION_ARGUMENT_SELECTION_END_INT";
    public static final String ACTION_ARGUMENT_SELECTION_START_INT = "ACTION_ARGUMENT_SELECTION_START_INT";
    public static final String ACTION_ARGUMENT_SET_TEXT_CHARSEQUENCE = "ACTION_ARGUMENT_SET_TEXT_CHARSEQUENCE";
    public static final int ACTION_CLEAR_ACCESSIBILITY_FOCUS = 128;
    public static final int ACTION_CLEAR_FOCUS = 2;
    public static final int ACTION_CLEAR_SELECTION = 8;
    public static final int ACTION_CLICK = 16;
    public static final int ACTION_COLLAPSE = 524288;
    public static final int ACTION_COPY = 16384;
    public static final int ACTION_CUT = 65536;
    public static final int ACTION_DISMISS = 1048576;
    public static final int ACTION_EXPAND = 262144;
    public static final int ACTION_FOCUS = 1;
    public static final int ACTION_LONG_CLICK = 32;
    public static final int ACTION_NEXT_AT_MOVEMENT_GRANULARITY = 256;
    public static final int ACTION_NEXT_HTML_ELEMENT = 1024;
    public static final int ACTION_PASTE = 32768;
    public static final int ACTION_PREVIOUS_AT_MOVEMENT_GRANULARITY = 512;
    public static final int ACTION_PREVIOUS_HTML_ELEMENT = 2048;
    public static final int ACTION_SCROLL_BACKWARD = 8192;
    public static final int ACTION_SCROLL_FORWARD = 4096;
    public static final int ACTION_SELECT = 4;
    public static final int ACTION_SET_SELECTION = 131072;
    public static final int ACTION_SET_TEXT = 2097152;
    private static final int BOOLEAN_PROPERTY_ACCESSIBILITY_DATA_SENSITIVE = 64;
    private static final int BOOLEAN_PROPERTY_HAS_REQUEST_INITIAL_ACCESSIBILITY_FOCUS = 32;
    private static final int BOOLEAN_PROPERTY_IS_HEADING = 2;
    private static final int BOOLEAN_PROPERTY_IS_SHOWING_HINT = 4;
    private static final int BOOLEAN_PROPERTY_IS_TEXT_ENTRY_KEY = 8;
    private static final String BOOLEAN_PROPERTY_KEY = "androidx.view.accessibility.AccessibilityNodeInfoCompat.BOOLEAN_PROPERTY_KEY";
    private static final int BOOLEAN_PROPERTY_SCREEN_READER_FOCUSABLE = 1;
    private static final int BOOLEAN_PROPERTY_SUPPORTS_GRANULAR_SCROLLING = 67108864;
    private static final int BOOLEAN_PROPERTY_TEXT_SELECTABLE = 8388608;
    private static final String BOUNDS_IN_WINDOW_KEY = "androidx.view.accessibility.AccessibilityNodeInfoCompat.BOUNDS_IN_WINDOW_KEY";
    private static final String CONTAINER_TITLE_KEY = "androidx.view.accessibility.AccessibilityNodeInfoCompat.CONTAINER_TITLE_KEY";
    public static final String EXTRA_DATA_TEXT_CHARACTER_LOCATION_ARG_LENGTH = "android.core.view.accessibility.extra.DATA_TEXT_CHARACTER_LOCATION_ARG_LENGTH";
    public static final int EXTRA_DATA_TEXT_CHARACTER_LOCATION_ARG_MAX_LENGTH = 20000;
    public static final String EXTRA_DATA_TEXT_CHARACTER_LOCATION_ARG_START_INDEX = "android.core.view.accessibility.extra.DATA_TEXT_CHARACTER_LOCATION_ARG_START_INDEX";
    public static final String EXTRA_DATA_TEXT_CHARACTER_LOCATION_KEY = "android.core.view.accessibility.extra.DATA_TEXT_CHARACTER_LOCATION_KEY";
    public static final int FLAG_PREFETCH_ANCESTORS = 1;
    public static final int FLAG_PREFETCH_DESCENDANTS_BREADTH_FIRST = 16;
    public static final int FLAG_PREFETCH_DESCENDANTS_DEPTH_FIRST = 8;
    public static final int FLAG_PREFETCH_DESCENDANTS_HYBRID = 4;
    public static final int FLAG_PREFETCH_SIBLINGS = 2;
    public static final int FLAG_PREFETCH_UNINTERRUPTIBLE = 32;
    public static final int FOCUS_ACCESSIBILITY = 2;
    public static final int FOCUS_INPUT = 1;
    private static final String HINT_TEXT_KEY = "androidx.view.accessibility.AccessibilityNodeInfoCompat.HINT_TEXT_KEY";
    public static final int MAX_NUMBER_OF_PREFETCHED_NODES = 50;
    private static final String MIN_DURATION_BETWEEN_CONTENT_CHANGES_KEY = "androidx.view.accessibility.AccessibilityNodeInfoCompat.MIN_DURATION_BETWEEN_CONTENT_CHANGES_KEY";
    public static final int MOVEMENT_GRANULARITY_CHARACTER = 1;
    public static final int MOVEMENT_GRANULARITY_LINE = 4;
    public static final int MOVEMENT_GRANULARITY_PAGE = 16;
    public static final int MOVEMENT_GRANULARITY_PARAGRAPH = 8;
    public static final int MOVEMENT_GRANULARITY_WORD = 2;
    private static final String PANE_TITLE_KEY = "androidx.view.accessibility.AccessibilityNodeInfoCompat.PANE_TITLE_KEY";
    private static final String ROLE_DESCRIPTION_KEY = "AccessibilityNodeInfo.roleDescription";
    private static final String SPANS_ACTION_ID_KEY = "androidx.view.accessibility.AccessibilityNodeInfoCompat.SPANS_ACTION_ID_KEY";
    private static final String SPANS_END_KEY = "androidx.view.accessibility.AccessibilityNodeInfoCompat.SPANS_END_KEY";
    private static final String SPANS_FLAGS_KEY = "androidx.view.accessibility.AccessibilityNodeInfoCompat.SPANS_FLAGS_KEY";
    private static final String SPANS_ID_KEY = "androidx.view.accessibility.AccessibilityNodeInfoCompat.SPANS_ID_KEY";
    private static final String SPANS_START_KEY = "androidx.view.accessibility.AccessibilityNodeInfoCompat.SPANS_START_KEY";
    private static final String STATE_DESCRIPTION_KEY = "androidx.view.accessibility.AccessibilityNodeInfoCompat.STATE_DESCRIPTION_KEY";
    private static final String TOOLTIP_TEXT_KEY = "androidx.view.accessibility.AccessibilityNodeInfoCompat.TOOLTIP_TEXT_KEY";
    private static final String UNIQUE_ID_KEY = "androidx.view.accessibility.AccessibilityNodeInfoCompat.UNIQUE_ID_KEY";
    private static int sClickableSpanId;
    private final AccessibilityNodeInfo mInfo;
    public int mParentVirtualDescendantId;
    private int mVirtualDescendantId;
    
    private AccessibilityNodeInfoCompat(final AccessibilityNodeInfo mInfo) {
        this.mParentVirtualDescendantId = -1;
        this.mVirtualDescendantId = -1;
        this.mInfo = mInfo;
    }
    
    @Deprecated
    public AccessibilityNodeInfoCompat(final Object o) {
        this.mParentVirtualDescendantId = -1;
        this.mVirtualDescendantId = -1;
        this.mInfo = (AccessibilityNodeInfo)o;
    }
    
    private void addSpanLocationToExtras(final ClickableSpan clickableSpan, final Spanned spanned, final int i) {
        this.extrasIntList("androidx.view.accessibility.AccessibilityNodeInfoCompat.SPANS_START_KEY").add(spanned.getSpanStart((Object)clickableSpan));
        this.extrasIntList("androidx.view.accessibility.AccessibilityNodeInfoCompat.SPANS_END_KEY").add(spanned.getSpanEnd((Object)clickableSpan));
        this.extrasIntList("androidx.view.accessibility.AccessibilityNodeInfoCompat.SPANS_FLAGS_KEY").add(spanned.getSpanFlags((Object)clickableSpan));
        this.extrasIntList("androidx.view.accessibility.AccessibilityNodeInfoCompat.SPANS_ID_KEY").add(i);
    }
    
    private void clearExtrasSpans() {
        if (Build$VERSION.SDK_INT >= 19) {
            Api19Impl.getExtras(this.mInfo).remove("androidx.view.accessibility.AccessibilityNodeInfoCompat.SPANS_START_KEY");
            Api19Impl.getExtras(this.mInfo).remove("androidx.view.accessibility.AccessibilityNodeInfoCompat.SPANS_END_KEY");
            Api19Impl.getExtras(this.mInfo).remove("androidx.view.accessibility.AccessibilityNodeInfoCompat.SPANS_FLAGS_KEY");
            Api19Impl.getExtras(this.mInfo).remove("androidx.view.accessibility.AccessibilityNodeInfoCompat.SPANS_ID_KEY");
        }
    }
    
    private List<Integer> extrasIntList(final String s) {
        if (Build$VERSION.SDK_INT < 19) {
            return new ArrayList<Integer>();
        }
        ArrayList integerArrayList;
        if ((integerArrayList = Api19Impl.getExtras(this.mInfo).getIntegerArrayList(s)) == null) {
            integerArrayList = new ArrayList();
            Api19Impl.getExtras(this.mInfo).putIntegerArrayList(s, integerArrayList);
        }
        return integerArrayList;
    }
    
    static String getActionSymbolicName(final int n) {
        if (n == 1) {
            return "ACTION_FOCUS";
        }
        if (n == 2) {
            return "ACTION_CLEAR_FOCUS";
        }
        switch (n) {
            default: {
                switch (n) {
                    default: {
                        switch (n) {
                            default: {
                                switch (n) {
                                    default: {
                                        return "ACTION_UNKNOWN";
                                    }
                                    case 16908375: {
                                        return "ACTION_DRAG_CANCEL";
                                    }
                                    case 16908374: {
                                        return "ACTION_DRAG_DROP";
                                    }
                                    case 16908373: {
                                        return "ACTION_DRAG_START";
                                    }
                                    case 16908372: {
                                        return "ACTION_IME_ENTER";
                                    }
                                }
                                break;
                            }
                            case 16908362: {
                                return "ACTION_PRESS_AND_HOLD";
                            }
                            case 16908361: {
                                return "ACTION_PAGE_RIGHT";
                            }
                            case 16908360: {
                                return "ACTION_PAGE_LEFT";
                            }
                            case 16908359: {
                                return "ACTION_PAGE_DOWN";
                            }
                            case 16908358: {
                                return "ACTION_PAGE_UP";
                            }
                            case 16908357: {
                                return "ACTION_HIDE_TOOLTIP";
                            }
                            case 16908356: {
                                return "ACTION_SHOW_TOOLTIP";
                            }
                        }
                        break;
                    }
                    case 16908349: {
                        return "ACTION_SET_PROGRESS";
                    }
                    case 16908348: {
                        return "ACTION_CONTEXT_CLICK";
                    }
                    case 16908347: {
                        return "ACTION_SCROLL_RIGHT";
                    }
                    case 16908346: {
                        return "ACTION_SCROLL_DOWN";
                    }
                    case 16908345: {
                        return "ACTION_SCROLL_LEFT";
                    }
                    case 16908344: {
                        return "ACTION_SCROLL_UP";
                    }
                    case 16908343: {
                        return "ACTION_SCROLL_TO_POSITION";
                    }
                    case 16908342: {
                        return "ACTION_SHOW_ON_SCREEN";
                    }
                }
                break;
            }
            case 16908382: {
                return "ACTION_SCROLL_IN_DIRECTION";
            }
            case 16908354: {
                return "ACTION_MOVE_WINDOW";
            }
            case 2097152: {
                return "ACTION_SET_TEXT";
            }
            case 524288: {
                return "ACTION_COLLAPSE";
            }
            case 262144: {
                return "ACTION_EXPAND";
            }
            case 131072: {
                return "ACTION_SET_SELECTION";
            }
            case 65536: {
                return "ACTION_CUT";
            }
            case 32768: {
                return "ACTION_PASTE";
            }
            case 16384: {
                return "ACTION_COPY";
            }
            case 8192: {
                return "ACTION_SCROLL_BACKWARD";
            }
            case 4096: {
                return "ACTION_SCROLL_FORWARD";
            }
            case 2048: {
                return "ACTION_PREVIOUS_HTML_ELEMENT";
            }
            case 1024: {
                return "ACTION_NEXT_HTML_ELEMENT";
            }
            case 512: {
                return "ACTION_PREVIOUS_AT_MOVEMENT_GRANULARITY";
            }
            case 256: {
                return "ACTION_NEXT_AT_MOVEMENT_GRANULARITY";
            }
            case 128: {
                return "ACTION_CLEAR_ACCESSIBILITY_FOCUS";
            }
            case 64: {
                return "ACTION_ACCESSIBILITY_FOCUS";
            }
            case 32: {
                return "ACTION_LONG_CLICK";
            }
            case 16: {
                return "ACTION_CLICK";
            }
            case 8: {
                return "ACTION_CLEAR_SELECTION";
            }
            case 4: {
                return "ACTION_SELECT";
            }
        }
    }
    
    private boolean getBooleanProperty(final int n) {
        final Bundle extras = this.getExtras();
        boolean b = false;
        if (extras == null) {
            return false;
        }
        if ((extras.getInt("androidx.view.accessibility.AccessibilityNodeInfoCompat.BOOLEAN_PROPERTY_KEY", 0) & n) == n) {
            b = true;
        }
        return b;
    }
    
    public static ClickableSpan[] getClickableSpans(final CharSequence charSequence) {
        if (charSequence instanceof Spanned) {
            return (ClickableSpan[])((Spanned)charSequence).getSpans(0, charSequence.length(), (Class)ClickableSpan.class);
        }
        return null;
    }
    
    private SparseArray<WeakReference<ClickableSpan>> getOrCreateSpansFromViewTags(final View view) {
        SparseArray spansFromViewTags;
        if ((spansFromViewTags = this.getSpansFromViewTags(view)) == null) {
            spansFromViewTags = new SparseArray();
            view.setTag(R.id.tag_accessibility_clickable_spans, (Object)spansFromViewTags);
        }
        return (SparseArray<WeakReference<ClickableSpan>>)spansFromViewTags;
    }
    
    private SparseArray<WeakReference<ClickableSpan>> getSpansFromViewTags(final View view) {
        return (SparseArray<WeakReference<ClickableSpan>>)view.getTag(R.id.tag_accessibility_clickable_spans);
    }
    
    private boolean hasSpans() {
        return this.extrasIntList("androidx.view.accessibility.AccessibilityNodeInfoCompat.SPANS_START_KEY").isEmpty() ^ true;
    }
    
    private int idForClickableSpan(final ClickableSpan clickableSpan, final SparseArray<WeakReference<ClickableSpan>> sparseArray) {
        if (sparseArray != null) {
            for (int i = 0; i < sparseArray.size(); ++i) {
                if (clickableSpan.equals(((WeakReference)sparseArray.valueAt(i)).get())) {
                    return sparseArray.keyAt(i);
                }
            }
        }
        final int sClickableSpanId = AccessibilityNodeInfoCompat.sClickableSpanId;
        AccessibilityNodeInfoCompat.sClickableSpanId = sClickableSpanId + 1;
        return sClickableSpanId;
    }
    
    public static AccessibilityNodeInfoCompat obtain() {
        return wrap(AccessibilityNodeInfo.obtain());
    }
    
    public static AccessibilityNodeInfoCompat obtain(final View view) {
        return wrap(AccessibilityNodeInfo.obtain(view));
    }
    
    public static AccessibilityNodeInfoCompat obtain(final View view, final int n) {
        if (Build$VERSION.SDK_INT >= 16) {
            return wrapNonNullInstance(AccessibilityNodeInfo.obtain(view, n));
        }
        return null;
    }
    
    public static AccessibilityNodeInfoCompat obtain(final AccessibilityNodeInfoCompat accessibilityNodeInfoCompat) {
        return wrap(AccessibilityNodeInfo.obtain(accessibilityNodeInfoCompat.mInfo));
    }
    
    private void removeCollectedSpans(final View view) {
        final SparseArray<WeakReference<ClickableSpan>> spansFromViewTags = this.getSpansFromViewTags(view);
        if (spansFromViewTags != null) {
            final ArrayList list = new ArrayList();
            final int n = 0;
            int i = 0;
            int j;
            while (true) {
                j = n;
                if (i >= spansFromViewTags.size()) {
                    break;
                }
                if (((WeakReference)spansFromViewTags.valueAt(i)).get() == null) {
                    list.add(i);
                }
                ++i;
            }
            while (j < list.size()) {
                spansFromViewTags.remove((int)list.get(j));
                ++j;
            }
        }
    }
    
    private void setBooleanProperty(final int n, final boolean b) {
        final Bundle extras = this.getExtras();
        if (extras != null) {
            final int int1 = extras.getInt("androidx.view.accessibility.AccessibilityNodeInfoCompat.BOOLEAN_PROPERTY_KEY", 0);
            int n2;
            if (b) {
                n2 = n;
            }
            else {
                n2 = 0;
            }
            extras.putInt("androidx.view.accessibility.AccessibilityNodeInfoCompat.BOOLEAN_PROPERTY_KEY", n2 | (int1 & ~n));
        }
    }
    
    public static AccessibilityNodeInfoCompat wrap(final AccessibilityNodeInfo accessibilityNodeInfo) {
        return new AccessibilityNodeInfoCompat(accessibilityNodeInfo);
    }
    
    static AccessibilityNodeInfoCompat wrapNonNullInstance(final Object o) {
        if (o != null) {
            return new AccessibilityNodeInfoCompat(o);
        }
        return null;
    }
    
    public void addAction(final int n) {
        this.mInfo.addAction(n);
    }
    
    public void addAction(final AccessibilityActionCompat accessibilityActionCompat) {
        if (Build$VERSION.SDK_INT >= 21) {
            this.mInfo.addAction((AccessibilityNodeInfo$AccessibilityAction)accessibilityActionCompat.mAction);
        }
    }
    
    public void addChild(final View view) {
        this.mInfo.addChild(view);
    }
    
    public void addChild(final View view, final int n) {
        if (Build$VERSION.SDK_INT >= 16) {
            this.mInfo.addChild(view, n);
        }
    }
    
    public void addSpansToExtras(final CharSequence charSequence, final View view) {
        if (Build$VERSION.SDK_INT >= 19 && Build$VERSION.SDK_INT < 26) {
            this.clearExtrasSpans();
            this.removeCollectedSpans(view);
            final ClickableSpan[] clickableSpans = getClickableSpans(charSequence);
            if (clickableSpans != null && clickableSpans.length > 0) {
                this.getExtras().putInt("androidx.view.accessibility.AccessibilityNodeInfoCompat.SPANS_ACTION_ID_KEY", R.id.accessibility_action_clickable_span);
                final SparseArray<WeakReference<ClickableSpan>> orCreateSpansFromViewTags = this.getOrCreateSpansFromViewTags(view);
                for (int n = 0; clickableSpans != null && n < clickableSpans.length; ++n) {
                    final int idForClickableSpan = this.idForClickableSpan(clickableSpans[n], orCreateSpansFromViewTags);
                    orCreateSpansFromViewTags.put(idForClickableSpan, (Object)new WeakReference(clickableSpans[n]));
                    this.addSpanLocationToExtras(clickableSpans[n], (Spanned)charSequence, idForClickableSpan);
                }
            }
        }
    }
    
    public boolean canOpenPopup() {
        return Build$VERSION.SDK_INT >= 19 && this.mInfo.canOpenPopup();
    }
    
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null) {
            return false;
        }
        if (!(o instanceof AccessibilityNodeInfoCompat)) {
            return false;
        }
        final AccessibilityNodeInfoCompat accessibilityNodeInfoCompat = (AccessibilityNodeInfoCompat)o;
        final AccessibilityNodeInfo mInfo = this.mInfo;
        if (mInfo == null) {
            if (accessibilityNodeInfoCompat.mInfo != null) {
                return false;
            }
        }
        else if (!mInfo.equals((Object)accessibilityNodeInfoCompat.mInfo)) {
            return false;
        }
        return this.mVirtualDescendantId == accessibilityNodeInfoCompat.mVirtualDescendantId && this.mParentVirtualDescendantId == accessibilityNodeInfoCompat.mParentVirtualDescendantId;
    }
    
    public List<AccessibilityNodeInfoCompat> findAccessibilityNodeInfosByText(final String s) {
        final ArrayList list = new ArrayList();
        final List accessibilityNodeInfosByText = this.mInfo.findAccessibilityNodeInfosByText(s);
        for (int size = accessibilityNodeInfosByText.size(), i = 0; i < size; ++i) {
            list.add(wrap((AccessibilityNodeInfo)accessibilityNodeInfosByText.get(i)));
        }
        return list;
    }
    
    public List<AccessibilityNodeInfoCompat> findAccessibilityNodeInfosByViewId(final String s) {
        if (Build$VERSION.SDK_INT >= 18) {
            final List accessibilityNodeInfosByViewId = this.mInfo.findAccessibilityNodeInfosByViewId(s);
            final ArrayList list = new ArrayList();
            final Iterator iterator = accessibilityNodeInfosByViewId.iterator();
            while (iterator.hasNext()) {
                list.add(wrap((AccessibilityNodeInfo)iterator.next()));
            }
            return list;
        }
        return Collections.emptyList();
    }
    
    public AccessibilityNodeInfoCompat findFocus(final int n) {
        if (Build$VERSION.SDK_INT >= 16) {
            return wrapNonNullInstance(this.mInfo.findFocus(n));
        }
        return null;
    }
    
    public AccessibilityNodeInfoCompat focusSearch(final int n) {
        if (Build$VERSION.SDK_INT >= 16) {
            return wrapNonNullInstance(this.mInfo.focusSearch(n));
        }
        return null;
    }
    
    public List<AccessibilityActionCompat> getActionList() {
        List actionList;
        if (Build$VERSION.SDK_INT >= 21) {
            actionList = this.mInfo.getActionList();
        }
        else {
            actionList = null;
        }
        if (actionList != null) {
            final ArrayList list = new ArrayList();
            for (int size = actionList.size(), i = 0; i < size; ++i) {
                list.add(new AccessibilityActionCompat(actionList.get(i)));
            }
            return list;
        }
        return Collections.emptyList();
    }
    
    @Deprecated
    public int getActions() {
        return this.mInfo.getActions();
    }
    
    public List<String> getAvailableExtraData() {
        if (Build$VERSION.SDK_INT >= 26) {
            return this.mInfo.getAvailableExtraData();
        }
        return Collections.emptyList();
    }
    
    @Deprecated
    public void getBoundsInParent(final Rect rect) {
        this.mInfo.getBoundsInParent(rect);
    }
    
    public void getBoundsInScreen(final Rect rect) {
        this.mInfo.getBoundsInScreen(rect);
    }
    
    public void getBoundsInWindow(final Rect rect) {
        if (Build$VERSION.SDK_INT >= 34) {
            Api34Impl.getBoundsInWindow(this.mInfo, rect);
        }
        else if (Build$VERSION.SDK_INT >= 19) {
            final Rect rect2 = (Rect)Api19Impl.getExtras(this.mInfo).getParcelable("androidx.view.accessibility.AccessibilityNodeInfoCompat.BOUNDS_IN_WINDOW_KEY");
            if (rect2 != null) {
                rect.set(rect2.left, rect2.top, rect2.right, rect2.bottom);
            }
        }
    }
    
    public AccessibilityNodeInfoCompat getChild(final int n) {
        return wrapNonNullInstance(this.mInfo.getChild(n));
    }
    
    public AccessibilityNodeInfoCompat getChild(final int n, final int n2) {
        if (Build$VERSION.SDK_INT >= 33) {
            return Api33Impl.getChild(this.mInfo, n, n2);
        }
        return this.getChild(n);
    }
    
    public int getChildCount() {
        return this.mInfo.getChildCount();
    }
    
    public CharSequence getClassName() {
        return this.mInfo.getClassName();
    }
    
    public CollectionInfoCompat getCollectionInfo() {
        if (Build$VERSION.SDK_INT >= 19) {
            final AccessibilityNodeInfo$CollectionInfo collectionInfo = this.mInfo.getCollectionInfo();
            if (collectionInfo != null) {
                return new CollectionInfoCompat(collectionInfo);
            }
        }
        return null;
    }
    
    public CollectionItemInfoCompat getCollectionItemInfo() {
        if (Build$VERSION.SDK_INT >= 19) {
            final AccessibilityNodeInfo$CollectionItemInfo collectionItemInfo = this.mInfo.getCollectionItemInfo();
            if (collectionItemInfo != null) {
                return new CollectionItemInfoCompat(collectionItemInfo);
            }
        }
        return null;
    }
    
    public CharSequence getContainerTitle() {
        if (Build$VERSION.SDK_INT >= 34) {
            return Api34Impl.getContainerTitle(this.mInfo);
        }
        if (Build$VERSION.SDK_INT >= 19) {
            return Api19Impl.getExtras(this.mInfo).getCharSequence("androidx.view.accessibility.AccessibilityNodeInfoCompat.CONTAINER_TITLE_KEY");
        }
        return null;
    }
    
    public CharSequence getContentDescription() {
        return this.mInfo.getContentDescription();
    }
    
    public int getDrawingOrder() {
        if (Build$VERSION.SDK_INT >= 24) {
            return this.mInfo.getDrawingOrder();
        }
        return 0;
    }
    
    public CharSequence getError() {
        if (Build$VERSION.SDK_INT >= 21) {
            return this.mInfo.getError();
        }
        return null;
    }
    
    public AccessibilityNodeInfo$ExtraRenderingInfo getExtraRenderingInfo() {
        if (Build$VERSION.SDK_INT >= 33) {
            return Api33Impl.getExtraRenderingInfo(this.mInfo);
        }
        return null;
    }
    
    public Bundle getExtras() {
        if (Build$VERSION.SDK_INT >= 19) {
            return Api19Impl.getExtras(this.mInfo);
        }
        return new Bundle();
    }
    
    public CharSequence getHintText() {
        if (Build$VERSION.SDK_INT >= 26) {
            return this.mInfo.getHintText();
        }
        if (Build$VERSION.SDK_INT >= 19) {
            return Api19Impl.getExtras(this.mInfo).getCharSequence("androidx.view.accessibility.AccessibilityNodeInfoCompat.HINT_TEXT_KEY");
        }
        return null;
    }
    
    @Deprecated
    public Object getInfo() {
        return this.mInfo;
    }
    
    public int getInputType() {
        if (Build$VERSION.SDK_INT >= 19) {
            return this.mInfo.getInputType();
        }
        return 0;
    }
    
    public AccessibilityNodeInfoCompat getLabelFor() {
        if (Build$VERSION.SDK_INT >= 17) {
            return wrapNonNullInstance(this.mInfo.getLabelFor());
        }
        return null;
    }
    
    public AccessibilityNodeInfoCompat getLabeledBy() {
        if (Build$VERSION.SDK_INT >= 17) {
            return wrapNonNullInstance(this.mInfo.getLabeledBy());
        }
        return null;
    }
    
    public int getLiveRegion() {
        if (Build$VERSION.SDK_INT >= 19) {
            return this.mInfo.getLiveRegion();
        }
        return 0;
    }
    
    public int getMaxTextLength() {
        if (Build$VERSION.SDK_INT >= 21) {
            return this.mInfo.getMaxTextLength();
        }
        return -1;
    }
    
    public long getMinDurationBetweenContentChangesMillis() {
        if (Build$VERSION.SDK_INT >= 34) {
            return Api34Impl.getMinDurationBetweenContentChangeMillis(this.mInfo);
        }
        if (Build$VERSION.SDK_INT >= 19) {
            return Api19Impl.getExtras(this.mInfo).getLong("androidx.view.accessibility.AccessibilityNodeInfoCompat.MIN_DURATION_BETWEEN_CONTENT_CHANGES_KEY");
        }
        return 0L;
    }
    
    public int getMovementGranularities() {
        if (Build$VERSION.SDK_INT >= 16) {
            return this.mInfo.getMovementGranularities();
        }
        return 0;
    }
    
    public CharSequence getPackageName() {
        return this.mInfo.getPackageName();
    }
    
    public CharSequence getPaneTitle() {
        if (Build$VERSION.SDK_INT >= 28) {
            return this.mInfo.getPaneTitle();
        }
        if (Build$VERSION.SDK_INT >= 19) {
            return Api19Impl.getExtras(this.mInfo).getCharSequence("androidx.view.accessibility.AccessibilityNodeInfoCompat.PANE_TITLE_KEY");
        }
        return null;
    }
    
    public AccessibilityNodeInfoCompat getParent() {
        return wrapNonNullInstance(this.mInfo.getParent());
    }
    
    public AccessibilityNodeInfoCompat getParent(final int n) {
        if (Build$VERSION.SDK_INT >= 33) {
            return Api33Impl.getParent(this.mInfo, n);
        }
        return this.getParent();
    }
    
    public RangeInfoCompat getRangeInfo() {
        if (Build$VERSION.SDK_INT >= 19) {
            final AccessibilityNodeInfo$RangeInfo rangeInfo = this.mInfo.getRangeInfo();
            if (rangeInfo != null) {
                return new RangeInfoCompat(rangeInfo);
            }
        }
        return null;
    }
    
    public CharSequence getRoleDescription() {
        if (Build$VERSION.SDK_INT >= 19) {
            return Api19Impl.getExtras(this.mInfo).getCharSequence("AccessibilityNodeInfo.roleDescription");
        }
        return null;
    }
    
    public CharSequence getStateDescription() {
        if (Build$VERSION.SDK_INT >= 30) {
            return Api30Impl.getStateDescription(this.mInfo);
        }
        if (Build$VERSION.SDK_INT >= 19) {
            return Api19Impl.getExtras(this.mInfo).getCharSequence("androidx.view.accessibility.AccessibilityNodeInfoCompat.STATE_DESCRIPTION_KEY");
        }
        return null;
    }
    
    public CharSequence getText() {
        if (this.hasSpans()) {
            final List<Integer> extrasIntList = this.extrasIntList("androidx.view.accessibility.AccessibilityNodeInfoCompat.SPANS_START_KEY");
            final List<Integer> extrasIntList2 = this.extrasIntList("androidx.view.accessibility.AccessibilityNodeInfoCompat.SPANS_END_KEY");
            final List<Integer> extrasIntList3 = this.extrasIntList("androidx.view.accessibility.AccessibilityNodeInfoCompat.SPANS_FLAGS_KEY");
            final List<Integer> extrasIntList4 = this.extrasIntList("androidx.view.accessibility.AccessibilityNodeInfoCompat.SPANS_ID_KEY");
            final CharSequence text = this.mInfo.getText();
            final int length = this.mInfo.getText().length();
            int i = 0;
            final SpannableString spannableString = new SpannableString((CharSequence)TextUtils.substring(text, 0, length));
            while (i < extrasIntList.size()) {
                ((Spannable)spannableString).setSpan((Object)new AccessibilityClickableSpanCompat(extrasIntList4.get(i), this, this.getExtras().getInt("androidx.view.accessibility.AccessibilityNodeInfoCompat.SPANS_ACTION_ID_KEY")), (int)extrasIntList.get(i), (int)extrasIntList2.get(i), (int)extrasIntList3.get(i));
                ++i;
            }
            return (CharSequence)spannableString;
        }
        return this.mInfo.getText();
    }
    
    public int getTextSelectionEnd() {
        if (Build$VERSION.SDK_INT >= 18) {
            return this.mInfo.getTextSelectionEnd();
        }
        return -1;
    }
    
    public int getTextSelectionStart() {
        if (Build$VERSION.SDK_INT >= 18) {
            return this.mInfo.getTextSelectionStart();
        }
        return -1;
    }
    
    public CharSequence getTooltipText() {
        if (Build$VERSION.SDK_INT >= 28) {
            return this.mInfo.getTooltipText();
        }
        if (Build$VERSION.SDK_INT >= 19) {
            return Api19Impl.getExtras(this.mInfo).getCharSequence("androidx.view.accessibility.AccessibilityNodeInfoCompat.TOOLTIP_TEXT_KEY");
        }
        return null;
    }
    
    public TouchDelegateInfoCompat getTouchDelegateInfo() {
        if (Build$VERSION.SDK_INT >= 29) {
            final AccessibilityNodeInfo$TouchDelegateInfo touchDelegateInfo = this.mInfo.getTouchDelegateInfo();
            if (touchDelegateInfo != null) {
                return new TouchDelegateInfoCompat(touchDelegateInfo);
            }
        }
        return null;
    }
    
    public AccessibilityNodeInfoCompat getTraversalAfter() {
        if (Build$VERSION.SDK_INT >= 22) {
            return wrapNonNullInstance(this.mInfo.getTraversalAfter());
        }
        return null;
    }
    
    public AccessibilityNodeInfoCompat getTraversalBefore() {
        if (Build$VERSION.SDK_INT >= 22) {
            return wrapNonNullInstance(this.mInfo.getTraversalBefore());
        }
        return null;
    }
    
    public String getUniqueId() {
        if (Build$VERSION.SDK_INT >= 33) {
            return Api33Impl.getUniqueId(this.mInfo);
        }
        if (Build$VERSION.SDK_INT >= 19) {
            return Api19Impl.getExtras(this.mInfo).getString("androidx.view.accessibility.AccessibilityNodeInfoCompat.UNIQUE_ID_KEY");
        }
        return null;
    }
    
    public String getViewIdResourceName() {
        if (Build$VERSION.SDK_INT >= 18) {
            return this.mInfo.getViewIdResourceName();
        }
        return null;
    }
    
    public AccessibilityWindowInfoCompat getWindow() {
        if (Build$VERSION.SDK_INT >= 21) {
            return AccessibilityWindowInfoCompat.wrapNonNullInstance(this.mInfo.getWindow());
        }
        return null;
    }
    
    public int getWindowId() {
        return this.mInfo.getWindowId();
    }
    
    public boolean hasRequestInitialAccessibilityFocus() {
        if (Build$VERSION.SDK_INT >= 34) {
            return Api34Impl.hasRequestInitialAccessibilityFocus(this.mInfo);
        }
        return this.getBooleanProperty(32);
    }
    
    @Override
    public int hashCode() {
        final AccessibilityNodeInfo mInfo = this.mInfo;
        int hashCode;
        if (mInfo == null) {
            hashCode = 0;
        }
        else {
            hashCode = mInfo.hashCode();
        }
        return hashCode;
    }
    
    public boolean isAccessibilityDataSensitive() {
        if (Build$VERSION.SDK_INT >= 34) {
            return Api34Impl.isAccessibilityDataSensitive(this.mInfo);
        }
        return this.getBooleanProperty(64);
    }
    
    public boolean isAccessibilityFocused() {
        return Build$VERSION.SDK_INT >= 16 && this.mInfo.isAccessibilityFocused();
    }
    
    public boolean isCheckable() {
        return this.mInfo.isCheckable();
    }
    
    public boolean isChecked() {
        return this.mInfo.isChecked();
    }
    
    public boolean isClickable() {
        return this.mInfo.isClickable();
    }
    
    public boolean isContentInvalid() {
        return Build$VERSION.SDK_INT >= 19 && this.mInfo.isContentInvalid();
    }
    
    public boolean isContextClickable() {
        return Build$VERSION.SDK_INT >= 23 && this.mInfo.isContextClickable();
    }
    
    public boolean isDismissable() {
        return Build$VERSION.SDK_INT >= 19 && this.mInfo.isDismissable();
    }
    
    public boolean isEditable() {
        return Build$VERSION.SDK_INT >= 18 && this.mInfo.isEditable();
    }
    
    public boolean isEnabled() {
        return this.mInfo.isEnabled();
    }
    
    public boolean isFocusable() {
        return this.mInfo.isFocusable();
    }
    
    public boolean isFocused() {
        return this.mInfo.isFocused();
    }
    
    public boolean isGranularScrollingSupported() {
        return this.getBooleanProperty(67108864);
    }
    
    public boolean isHeading() {
        if (Build$VERSION.SDK_INT >= 28) {
            return this.mInfo.isHeading();
        }
        final boolean booleanProperty = this.getBooleanProperty(2);
        boolean b = true;
        if (booleanProperty) {
            return true;
        }
        final CollectionItemInfoCompat collectionItemInfo = this.getCollectionItemInfo();
        if (collectionItemInfo == null || !collectionItemInfo.isHeading()) {
            b = false;
        }
        return b;
    }
    
    public boolean isImportantForAccessibility() {
        return Build$VERSION.SDK_INT < 24 || this.mInfo.isImportantForAccessibility();
    }
    
    public boolean isLongClickable() {
        return this.mInfo.isLongClickable();
    }
    
    public boolean isMultiLine() {
        return Build$VERSION.SDK_INT >= 19 && this.mInfo.isMultiLine();
    }
    
    public boolean isPassword() {
        return this.mInfo.isPassword();
    }
    
    public boolean isScreenReaderFocusable() {
        if (Build$VERSION.SDK_INT >= 28) {
            return this.mInfo.isScreenReaderFocusable();
        }
        return this.getBooleanProperty(1);
    }
    
    public boolean isScrollable() {
        return this.mInfo.isScrollable();
    }
    
    public boolean isSelected() {
        return this.mInfo.isSelected();
    }
    
    public boolean isShowingHintText() {
        if (Build$VERSION.SDK_INT >= 26) {
            return this.mInfo.isShowingHintText();
        }
        return this.getBooleanProperty(4);
    }
    
    public boolean isTextEntryKey() {
        if (Build$VERSION.SDK_INT >= 29) {
            return this.mInfo.isTextEntryKey();
        }
        return this.getBooleanProperty(8);
    }
    
    public boolean isTextSelectable() {
        if (Build$VERSION.SDK_INT >= 33) {
            return Api33Impl.isTextSelectable(this.mInfo);
        }
        return this.getBooleanProperty(8388608);
    }
    
    public boolean isVisibleToUser() {
        return Build$VERSION.SDK_INT >= 16 && this.mInfo.isVisibleToUser();
    }
    
    public boolean performAction(final int n) {
        return this.mInfo.performAction(n);
    }
    
    public boolean performAction(final int n, final Bundle bundle) {
        return Build$VERSION.SDK_INT >= 16 && this.mInfo.performAction(n, bundle);
    }
    
    @Deprecated
    public void recycle() {
    }
    
    public boolean refresh() {
        return Build$VERSION.SDK_INT >= 18 && this.mInfo.refresh();
    }
    
    public boolean removeAction(final AccessibilityActionCompat accessibilityActionCompat) {
        return Build$VERSION.SDK_INT >= 21 && this.mInfo.removeAction((AccessibilityNodeInfo$AccessibilityAction)accessibilityActionCompat.mAction);
    }
    
    public boolean removeChild(final View view) {
        return Build$VERSION.SDK_INT >= 21 && this.mInfo.removeChild(view);
    }
    
    public boolean removeChild(final View view, final int n) {
        return Build$VERSION.SDK_INT >= 21 && this.mInfo.removeChild(view, n);
    }
    
    public void setAccessibilityDataSensitive(final boolean b) {
        if (Build$VERSION.SDK_INT >= 34) {
            Api34Impl.setAccessibilityDataSensitive(this.mInfo, b);
        }
        else {
            this.setBooleanProperty(64, b);
        }
    }
    
    public void setAccessibilityFocused(final boolean accessibilityFocused) {
        if (Build$VERSION.SDK_INT >= 16) {
            this.mInfo.setAccessibilityFocused(accessibilityFocused);
        }
    }
    
    public void setAvailableExtraData(final List<String> availableExtraData) {
        if (Build$VERSION.SDK_INT >= 26) {
            this.mInfo.setAvailableExtraData((List)availableExtraData);
        }
    }
    
    @Deprecated
    public void setBoundsInParent(final Rect boundsInParent) {
        this.mInfo.setBoundsInParent(boundsInParent);
    }
    
    public void setBoundsInScreen(final Rect boundsInScreen) {
        this.mInfo.setBoundsInScreen(boundsInScreen);
    }
    
    public void setBoundsInWindow(final Rect rect) {
        if (Build$VERSION.SDK_INT >= 34) {
            Api34Impl.setBoundsInWindow(this.mInfo, rect);
        }
        else if (Build$VERSION.SDK_INT >= 19) {
            Api19Impl.getExtras(this.mInfo).putParcelable("androidx.view.accessibility.AccessibilityNodeInfoCompat.BOUNDS_IN_WINDOW_KEY", (Parcelable)rect);
        }
    }
    
    public void setCanOpenPopup(final boolean canOpenPopup) {
        if (Build$VERSION.SDK_INT >= 19) {
            this.mInfo.setCanOpenPopup(canOpenPopup);
        }
    }
    
    public void setCheckable(final boolean checkable) {
        this.mInfo.setCheckable(checkable);
    }
    
    public void setChecked(final boolean checked) {
        this.mInfo.setChecked(checked);
    }
    
    public void setClassName(final CharSequence className) {
        this.mInfo.setClassName(className);
    }
    
    public void setClickable(final boolean clickable) {
        this.mInfo.setClickable(clickable);
    }
    
    public void setCollectionInfo(final Object o) {
        if (Build$VERSION.SDK_INT >= 19) {
            final AccessibilityNodeInfo mInfo = this.mInfo;
            AccessibilityNodeInfo$CollectionInfo collectionInfo;
            if (o == null) {
                collectionInfo = null;
            }
            else {
                collectionInfo = (AccessibilityNodeInfo$CollectionInfo)((CollectionInfoCompat)o).mInfo;
            }
            mInfo.setCollectionInfo(collectionInfo);
        }
    }
    
    public void setCollectionItemInfo(final Object o) {
        if (Build$VERSION.SDK_INT >= 19) {
            final AccessibilityNodeInfo mInfo = this.mInfo;
            AccessibilityNodeInfo$CollectionItemInfo collectionItemInfo;
            if (o == null) {
                collectionItemInfo = null;
            }
            else {
                collectionItemInfo = (AccessibilityNodeInfo$CollectionItemInfo)((CollectionItemInfoCompat)o).mInfo;
            }
            mInfo.setCollectionItemInfo(collectionItemInfo);
        }
    }
    
    public void setContainerTitle(final CharSequence charSequence) {
        if (Build$VERSION.SDK_INT >= 34) {
            Api34Impl.setContainerTitle(this.mInfo, charSequence);
        }
        else if (Build$VERSION.SDK_INT >= 19) {
            Api19Impl.getExtras(this.mInfo).putCharSequence("androidx.view.accessibility.AccessibilityNodeInfoCompat.CONTAINER_TITLE_KEY", charSequence);
        }
    }
    
    public void setContentDescription(final CharSequence contentDescription) {
        this.mInfo.setContentDescription(contentDescription);
    }
    
    public void setContentInvalid(final boolean contentInvalid) {
        if (Build$VERSION.SDK_INT >= 19) {
            this.mInfo.setContentInvalid(contentInvalid);
        }
    }
    
    public void setContextClickable(final boolean contextClickable) {
        if (Build$VERSION.SDK_INT >= 23) {
            this.mInfo.setContextClickable(contextClickable);
        }
    }
    
    public void setDismissable(final boolean dismissable) {
        if (Build$VERSION.SDK_INT >= 19) {
            this.mInfo.setDismissable(dismissable);
        }
    }
    
    public void setDrawingOrder(final int drawingOrder) {
        if (Build$VERSION.SDK_INT >= 24) {
            this.mInfo.setDrawingOrder(drawingOrder);
        }
    }
    
    public void setEditable(final boolean editable) {
        if (Build$VERSION.SDK_INT >= 18) {
            this.mInfo.setEditable(editable);
        }
    }
    
    public void setEnabled(final boolean enabled) {
        this.mInfo.setEnabled(enabled);
    }
    
    public void setError(final CharSequence error) {
        if (Build$VERSION.SDK_INT >= 21) {
            this.mInfo.setError(error);
        }
    }
    
    public void setFocusable(final boolean focusable) {
        this.mInfo.setFocusable(focusable);
    }
    
    public void setFocused(final boolean focused) {
        this.mInfo.setFocused(focused);
    }
    
    public void setGranularScrollingSupported(final boolean b) {
        this.setBooleanProperty(67108864, b);
    }
    
    public void setHeading(final boolean heading) {
        if (Build$VERSION.SDK_INT >= 28) {
            this.mInfo.setHeading(heading);
        }
        else {
            this.setBooleanProperty(2, heading);
        }
    }
    
    public void setHintText(final CharSequence hintText) {
        if (Build$VERSION.SDK_INT >= 26) {
            this.mInfo.setHintText(hintText);
        }
        else if (Build$VERSION.SDK_INT >= 19) {
            Api19Impl.getExtras(this.mInfo).putCharSequence("androidx.view.accessibility.AccessibilityNodeInfoCompat.HINT_TEXT_KEY", hintText);
        }
    }
    
    public void setImportantForAccessibility(final boolean importantForAccessibility) {
        if (Build$VERSION.SDK_INT >= 24) {
            this.mInfo.setImportantForAccessibility(importantForAccessibility);
        }
    }
    
    public void setInputType(final int inputType) {
        if (Build$VERSION.SDK_INT >= 19) {
            this.mInfo.setInputType(inputType);
        }
    }
    
    public void setLabelFor(final View labelFor) {
        if (Build$VERSION.SDK_INT >= 17) {
            this.mInfo.setLabelFor(labelFor);
        }
    }
    
    public void setLabelFor(final View view, final int n) {
        if (Build$VERSION.SDK_INT >= 17) {
            this.mInfo.setLabelFor(view, n);
        }
    }
    
    public void setLabeledBy(final View labeledBy) {
        if (Build$VERSION.SDK_INT >= 17) {
            this.mInfo.setLabeledBy(labeledBy);
        }
    }
    
    public void setLabeledBy(final View view, final int n) {
        if (Build$VERSION.SDK_INT >= 17) {
            this.mInfo.setLabeledBy(view, n);
        }
    }
    
    public void setLiveRegion(final int liveRegion) {
        if (Build$VERSION.SDK_INT >= 19) {
            this.mInfo.setLiveRegion(liveRegion);
        }
    }
    
    public void setLongClickable(final boolean longClickable) {
        this.mInfo.setLongClickable(longClickable);
    }
    
    public void setMaxTextLength(final int maxTextLength) {
        if (Build$VERSION.SDK_INT >= 21) {
            this.mInfo.setMaxTextLength(maxTextLength);
        }
    }
    
    public void setMinDurationBetweenContentChangesMillis(final long n) {
        if (Build$VERSION.SDK_INT >= 34) {
            Api34Impl.setMinDurationBetweenContentChangeMillis(this.mInfo, n);
        }
        else if (Build$VERSION.SDK_INT >= 19) {
            Api19Impl.getExtras(this.mInfo).putLong("androidx.view.accessibility.AccessibilityNodeInfoCompat.MIN_DURATION_BETWEEN_CONTENT_CHANGES_KEY", n);
        }
    }
    
    public void setMovementGranularities(final int movementGranularities) {
        if (Build$VERSION.SDK_INT >= 16) {
            this.mInfo.setMovementGranularities(movementGranularities);
        }
    }
    
    public void setMultiLine(final boolean multiLine) {
        if (Build$VERSION.SDK_INT >= 19) {
            this.mInfo.setMultiLine(multiLine);
        }
    }
    
    public void setPackageName(final CharSequence packageName) {
        this.mInfo.setPackageName(packageName);
    }
    
    public void setPaneTitle(final CharSequence paneTitle) {
        if (Build$VERSION.SDK_INT >= 28) {
            this.mInfo.setPaneTitle(paneTitle);
        }
        else if (Build$VERSION.SDK_INT >= 19) {
            Api19Impl.getExtras(this.mInfo).putCharSequence("androidx.view.accessibility.AccessibilityNodeInfoCompat.PANE_TITLE_KEY", paneTitle);
        }
    }
    
    public void setParent(final View parent) {
        this.mParentVirtualDescendantId = -1;
        this.mInfo.setParent(parent);
    }
    
    public void setParent(final View view, final int mParentVirtualDescendantId) {
        this.mParentVirtualDescendantId = mParentVirtualDescendantId;
        if (Build$VERSION.SDK_INT >= 16) {
            this.mInfo.setParent(view, mParentVirtualDescendantId);
        }
    }
    
    public void setPassword(final boolean password) {
        this.mInfo.setPassword(password);
    }
    
    public void setQueryFromAppProcessEnabled(final View view, final boolean b) {
        if (Build$VERSION.SDK_INT >= 34) {
            Api34Impl.setQueryFromAppProcessEnabled(this.mInfo, view, b);
        }
    }
    
    public void setRangeInfo(final RangeInfoCompat rangeInfoCompat) {
        if (Build$VERSION.SDK_INT >= 19) {
            this.mInfo.setRangeInfo((AccessibilityNodeInfo$RangeInfo)rangeInfoCompat.mInfo);
        }
    }
    
    public void setRequestInitialAccessibilityFocus(final boolean b) {
        if (Build$VERSION.SDK_INT >= 34) {
            Api34Impl.setRequestInitialAccessibilityFocus(this.mInfo, b);
        }
        else {
            this.setBooleanProperty(32, b);
        }
    }
    
    public void setRoleDescription(final CharSequence charSequence) {
        if (Build$VERSION.SDK_INT >= 19) {
            Api19Impl.getExtras(this.mInfo).putCharSequence("AccessibilityNodeInfo.roleDescription", charSequence);
        }
    }
    
    public void setScreenReaderFocusable(final boolean screenReaderFocusable) {
        if (Build$VERSION.SDK_INT >= 28) {
            this.mInfo.setScreenReaderFocusable(screenReaderFocusable);
        }
        else {
            this.setBooleanProperty(1, screenReaderFocusable);
        }
    }
    
    public void setScrollable(final boolean scrollable) {
        this.mInfo.setScrollable(scrollable);
    }
    
    public void setSelected(final boolean selected) {
        this.mInfo.setSelected(selected);
    }
    
    public void setShowingHintText(final boolean showingHintText) {
        if (Build$VERSION.SDK_INT >= 26) {
            this.mInfo.setShowingHintText(showingHintText);
        }
        else {
            this.setBooleanProperty(4, showingHintText);
        }
    }
    
    public void setSource(final View source) {
        this.mVirtualDescendantId = -1;
        this.mInfo.setSource(source);
    }
    
    public void setSource(final View view, final int mVirtualDescendantId) {
        this.mVirtualDescendantId = mVirtualDescendantId;
        if (Build$VERSION.SDK_INT >= 16) {
            this.mInfo.setSource(view, mVirtualDescendantId);
        }
    }
    
    public void setStateDescription(final CharSequence charSequence) {
        if (Build$VERSION.SDK_INT >= 30) {
            Api30Impl.setStateDescription(this.mInfo, charSequence);
        }
        else if (Build$VERSION.SDK_INT >= 19) {
            Api19Impl.getExtras(this.mInfo).putCharSequence("androidx.view.accessibility.AccessibilityNodeInfoCompat.STATE_DESCRIPTION_KEY", charSequence);
        }
    }
    
    public void setText(final CharSequence text) {
        this.mInfo.setText(text);
    }
    
    public void setTextEntryKey(final boolean textEntryKey) {
        if (Build$VERSION.SDK_INT >= 29) {
            this.mInfo.setTextEntryKey(textEntryKey);
        }
        else {
            this.setBooleanProperty(8, textEntryKey);
        }
    }
    
    public void setTextSelectable(final boolean b) {
        if (Build$VERSION.SDK_INT >= 33) {
            Api33Impl.setTextSelectable(this.mInfo, b);
        }
        else {
            this.setBooleanProperty(8388608, b);
        }
    }
    
    public void setTextSelection(final int n, final int n2) {
        if (Build$VERSION.SDK_INT >= 18) {
            this.mInfo.setTextSelection(n, n2);
        }
    }
    
    public void setTooltipText(final CharSequence tooltipText) {
        if (Build$VERSION.SDK_INT >= 28) {
            this.mInfo.setTooltipText(tooltipText);
        }
        else if (Build$VERSION.SDK_INT >= 19) {
            Api19Impl.getExtras(this.mInfo).putCharSequence("androidx.view.accessibility.AccessibilityNodeInfoCompat.TOOLTIP_TEXT_KEY", tooltipText);
        }
    }
    
    public void setTouchDelegateInfo(final TouchDelegateInfoCompat touchDelegateInfoCompat) {
        if (Build$VERSION.SDK_INT >= 29) {
            this.mInfo.setTouchDelegateInfo(touchDelegateInfoCompat.mInfo);
        }
    }
    
    public void setTraversalAfter(final View traversalAfter) {
        if (Build$VERSION.SDK_INT >= 22) {
            this.mInfo.setTraversalAfter(traversalAfter);
        }
    }
    
    public void setTraversalAfter(final View view, final int n) {
        if (Build$VERSION.SDK_INT >= 22) {
            this.mInfo.setTraversalAfter(view, n);
        }
    }
    
    public void setTraversalBefore(final View traversalBefore) {
        if (Build$VERSION.SDK_INT >= 22) {
            this.mInfo.setTraversalBefore(traversalBefore);
        }
    }
    
    public void setTraversalBefore(final View view, final int n) {
        if (Build$VERSION.SDK_INT >= 22) {
            this.mInfo.setTraversalBefore(view, n);
        }
    }
    
    public void setUniqueId(final String s) {
        if (Build$VERSION.SDK_INT >= 33) {
            Api33Impl.setUniqueId(this.mInfo, s);
        }
        else if (Build$VERSION.SDK_INT >= 19) {
            Api19Impl.getExtras(this.mInfo).putString("androidx.view.accessibility.AccessibilityNodeInfoCompat.UNIQUE_ID_KEY", s);
        }
    }
    
    public void setViewIdResourceName(final String viewIdResourceName) {
        if (Build$VERSION.SDK_INT >= 18) {
            this.mInfo.setViewIdResourceName(viewIdResourceName);
        }
    }
    
    public void setVisibleToUser(final boolean visibleToUser) {
        if (Build$VERSION.SDK_INT >= 16) {
            this.mInfo.setVisibleToUser(visibleToUser);
        }
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append(super.toString());
        final Rect obj = new Rect();
        this.getBoundsInParent(obj);
        final StringBuilder sb2 = new StringBuilder();
        sb2.append("; boundsInParent: ");
        sb2.append(obj);
        sb.append(sb2.toString());
        this.getBoundsInScreen(obj);
        final StringBuilder sb3 = new StringBuilder();
        sb3.append("; boundsInScreen: ");
        sb3.append(obj);
        sb.append(sb3.toString());
        this.getBoundsInWindow(obj);
        final StringBuilder sb4 = new StringBuilder();
        sb4.append("; boundsInWindow: ");
        sb4.append(obj);
        sb.append(sb4.toString());
        sb.append("; packageName: ");
        sb.append(this.getPackageName());
        sb.append("; className: ");
        sb.append(this.getClassName());
        sb.append("; text: ");
        sb.append(this.getText());
        sb.append("; error: ");
        sb.append(this.getError());
        sb.append("; maxTextLength: ");
        sb.append(this.getMaxTextLength());
        sb.append("; stateDescription: ");
        sb.append(this.getStateDescription());
        sb.append("; contentDescription: ");
        sb.append(this.getContentDescription());
        sb.append("; tooltipText: ");
        sb.append(this.getTooltipText());
        sb.append("; viewIdResName: ");
        sb.append(this.getViewIdResourceName());
        sb.append("; uniqueId: ");
        sb.append(this.getUniqueId());
        sb.append("; checkable: ");
        sb.append(this.isCheckable());
        sb.append("; checked: ");
        sb.append(this.isChecked());
        sb.append("; focusable: ");
        sb.append(this.isFocusable());
        sb.append("; focused: ");
        sb.append(this.isFocused());
        sb.append("; selected: ");
        sb.append(this.isSelected());
        sb.append("; clickable: ");
        sb.append(this.isClickable());
        sb.append("; longClickable: ");
        sb.append(this.isLongClickable());
        sb.append("; contextClickable: ");
        sb.append(this.isContextClickable());
        sb.append("; enabled: ");
        sb.append(this.isEnabled());
        sb.append("; password: ");
        sb.append(this.isPassword());
        final StringBuilder sb5 = new StringBuilder();
        sb5.append("; scrollable: ");
        sb5.append(this.isScrollable());
        sb.append(sb5.toString());
        sb.append("; containerTitle: ");
        sb.append(this.getContainerTitle());
        sb.append("; granularScrollingSupported: ");
        sb.append(this.isGranularScrollingSupported());
        sb.append("; importantForAccessibility: ");
        sb.append(this.isImportantForAccessibility());
        sb.append("; visible: ");
        sb.append(this.isVisibleToUser());
        sb.append("; isTextSelectable: ");
        sb.append(this.isTextSelectable());
        sb.append("; accessibilityDataSensitive: ");
        sb.append(this.isAccessibilityDataSensitive());
        sb.append("; [");
        if (Build$VERSION.SDK_INT >= 21) {
            final List<AccessibilityActionCompat> actionList = this.getActionList();
            for (int i = 0; i < actionList.size(); ++i) {
                final AccessibilityActionCompat accessibilityActionCompat = actionList.get(i);
                String str;
                final String s = str = getActionSymbolicName(accessibilityActionCompat.getId());
                if (s.equals("ACTION_UNKNOWN")) {
                    str = s;
                    if (accessibilityActionCompat.getLabel() != null) {
                        str = accessibilityActionCompat.getLabel().toString();
                    }
                }
                sb.append(str);
                if (i != actionList.size() - 1) {
                    sb.append(", ");
                }
            }
        }
        else {
            int n2;
            for (int j = this.getActions(); j != 0; j = n2) {
                final int n = 1 << Integer.numberOfTrailingZeros(j);
                n2 = (j & ~n);
                sb.append(getActionSymbolicName(n));
                if ((j = n2) != 0) {
                    sb.append(", ");
                }
            }
        }
        sb.append("]");
        return sb.toString();
    }
    
    public AccessibilityNodeInfo unwrap() {
        return this.mInfo;
    }
    
    public static class AccessibilityActionCompat
    {
        public static final AccessibilityActionCompat ACTION_ACCESSIBILITY_FOCUS;
        public static final AccessibilityActionCompat ACTION_CLEAR_ACCESSIBILITY_FOCUS;
        public static final AccessibilityActionCompat ACTION_CLEAR_FOCUS;
        public static final AccessibilityActionCompat ACTION_CLEAR_SELECTION;
        public static final AccessibilityActionCompat ACTION_CLICK;
        public static final AccessibilityActionCompat ACTION_COLLAPSE;
        public static final AccessibilityActionCompat ACTION_CONTEXT_CLICK;
        public static final AccessibilityActionCompat ACTION_COPY;
        public static final AccessibilityActionCompat ACTION_CUT;
        public static final AccessibilityActionCompat ACTION_DISMISS;
        public static final AccessibilityActionCompat ACTION_DRAG_CANCEL;
        public static final AccessibilityActionCompat ACTION_DRAG_DROP;
        public static final AccessibilityActionCompat ACTION_DRAG_START;
        public static final AccessibilityActionCompat ACTION_EXPAND;
        public static final AccessibilityActionCompat ACTION_FOCUS;
        public static final AccessibilityActionCompat ACTION_HIDE_TOOLTIP;
        public static final AccessibilityActionCompat ACTION_IME_ENTER;
        public static final AccessibilityActionCompat ACTION_LONG_CLICK;
        public static final AccessibilityActionCompat ACTION_MOVE_WINDOW;
        public static final AccessibilityActionCompat ACTION_NEXT_AT_MOVEMENT_GRANULARITY;
        public static final AccessibilityActionCompat ACTION_NEXT_HTML_ELEMENT;
        public static final AccessibilityActionCompat ACTION_PAGE_DOWN;
        public static final AccessibilityActionCompat ACTION_PAGE_LEFT;
        public static final AccessibilityActionCompat ACTION_PAGE_RIGHT;
        public static final AccessibilityActionCompat ACTION_PAGE_UP;
        public static final AccessibilityActionCompat ACTION_PASTE;
        public static final AccessibilityActionCompat ACTION_PRESS_AND_HOLD;
        public static final AccessibilityActionCompat ACTION_PREVIOUS_AT_MOVEMENT_GRANULARITY;
        public static final AccessibilityActionCompat ACTION_PREVIOUS_HTML_ELEMENT;
        public static final AccessibilityActionCompat ACTION_SCROLL_BACKWARD;
        public static final AccessibilityActionCompat ACTION_SCROLL_DOWN;
        public static final AccessibilityActionCompat ACTION_SCROLL_FORWARD;
        public static final AccessibilityActionCompat ACTION_SCROLL_IN_DIRECTION;
        public static final AccessibilityActionCompat ACTION_SCROLL_LEFT;
        public static final AccessibilityActionCompat ACTION_SCROLL_RIGHT;
        public static final AccessibilityActionCompat ACTION_SCROLL_TO_POSITION;
        public static final AccessibilityActionCompat ACTION_SCROLL_UP;
        public static final AccessibilityActionCompat ACTION_SELECT;
        public static final AccessibilityActionCompat ACTION_SET_PROGRESS;
        public static final AccessibilityActionCompat ACTION_SET_SELECTION;
        public static final AccessibilityActionCompat ACTION_SET_TEXT;
        public static final AccessibilityActionCompat ACTION_SHOW_ON_SCREEN;
        public static final AccessibilityActionCompat ACTION_SHOW_TEXT_SUGGESTIONS;
        public static final AccessibilityActionCompat ACTION_SHOW_TOOLTIP;
        private static final String TAG = "A11yActionCompat";
        final Object mAction;
        protected final AccessibilityViewCommand mCommand;
        private final int mId;
        private final Class<? extends AccessibilityViewCommand.CommandArguments> mViewCommandArgumentClass;
        
        static {
            final Object o = null;
            ACTION_FOCUS = new AccessibilityActionCompat(1, null);
            ACTION_CLEAR_FOCUS = new AccessibilityActionCompat(2, null);
            ACTION_SELECT = new AccessibilityActionCompat(4, null);
            ACTION_CLEAR_SELECTION = new AccessibilityActionCompat(8, null);
            ACTION_CLICK = new AccessibilityActionCompat(16, null);
            ACTION_LONG_CLICK = new AccessibilityActionCompat(32, null);
            ACTION_ACCESSIBILITY_FOCUS = new AccessibilityActionCompat(64, null);
            ACTION_CLEAR_ACCESSIBILITY_FOCUS = new AccessibilityActionCompat(128, null);
            ACTION_NEXT_AT_MOVEMENT_GRANULARITY = new AccessibilityActionCompat(256, null, AccessibilityViewCommand.MoveAtGranularityArguments.class);
            ACTION_PREVIOUS_AT_MOVEMENT_GRANULARITY = new AccessibilityActionCompat(512, null, AccessibilityViewCommand.MoveAtGranularityArguments.class);
            ACTION_NEXT_HTML_ELEMENT = new AccessibilityActionCompat(1024, null, AccessibilityViewCommand.MoveHtmlArguments.class);
            ACTION_PREVIOUS_HTML_ELEMENT = new AccessibilityActionCompat(2048, null, AccessibilityViewCommand.MoveHtmlArguments.class);
            ACTION_SCROLL_FORWARD = new AccessibilityActionCompat(4096, null);
            ACTION_SCROLL_BACKWARD = new AccessibilityActionCompat(8192, null);
            ACTION_COPY = new AccessibilityActionCompat(16384, null);
            ACTION_PASTE = new AccessibilityActionCompat(32768, null);
            ACTION_CUT = new AccessibilityActionCompat(65536, null);
            ACTION_SET_SELECTION = new AccessibilityActionCompat(131072, null, AccessibilityViewCommand.SetSelectionArguments.class);
            ACTION_EXPAND = new AccessibilityActionCompat(262144, null);
            ACTION_COLLAPSE = new AccessibilityActionCompat(524288, null);
            ACTION_DISMISS = new AccessibilityActionCompat(1048576, null);
            ACTION_SET_TEXT = new AccessibilityActionCompat(2097152, null, AccessibilityViewCommand.SetTextArguments.class);
            AccessibilityNodeInfo$AccessibilityAction action_SHOW_ON_SCREEN;
            if (Build$VERSION.SDK_INT >= 23) {
                action_SHOW_ON_SCREEN = AccessibilityNodeInfo$AccessibilityAction.ACTION_SHOW_ON_SCREEN;
            }
            else {
                action_SHOW_ON_SCREEN = null;
            }
            ACTION_SHOW_ON_SCREEN = new AccessibilityActionCompat(action_SHOW_ON_SCREEN, 16908342, null, null, null);
            AccessibilityNodeInfo$AccessibilityAction action_SCROLL_TO_POSITION;
            if (Build$VERSION.SDK_INT >= 23) {
                action_SCROLL_TO_POSITION = AccessibilityNodeInfo$AccessibilityAction.ACTION_SCROLL_TO_POSITION;
            }
            else {
                action_SCROLL_TO_POSITION = null;
            }
            ACTION_SCROLL_TO_POSITION = new AccessibilityActionCompat(action_SCROLL_TO_POSITION, 16908343, null, null, AccessibilityViewCommand.ScrollToPositionArguments.class);
            AccessibilityNodeInfo$AccessibilityAction action_SCROLL_UP;
            if (Build$VERSION.SDK_INT >= 23) {
                action_SCROLL_UP = AccessibilityNodeInfo$AccessibilityAction.ACTION_SCROLL_UP;
            }
            else {
                action_SCROLL_UP = null;
            }
            ACTION_SCROLL_UP = new AccessibilityActionCompat(action_SCROLL_UP, 16908344, null, null, null);
            AccessibilityNodeInfo$AccessibilityAction action_SCROLL_LEFT;
            if (Build$VERSION.SDK_INT >= 23) {
                action_SCROLL_LEFT = AccessibilityNodeInfo$AccessibilityAction.ACTION_SCROLL_LEFT;
            }
            else {
                action_SCROLL_LEFT = null;
            }
            ACTION_SCROLL_LEFT = new AccessibilityActionCompat(action_SCROLL_LEFT, 16908345, null, null, null);
            AccessibilityNodeInfo$AccessibilityAction action_SCROLL_DOWN;
            if (Build$VERSION.SDK_INT >= 23) {
                action_SCROLL_DOWN = AccessibilityNodeInfo$AccessibilityAction.ACTION_SCROLL_DOWN;
            }
            else {
                action_SCROLL_DOWN = null;
            }
            ACTION_SCROLL_DOWN = new AccessibilityActionCompat(action_SCROLL_DOWN, 16908346, null, null, null);
            AccessibilityNodeInfo$AccessibilityAction action_SCROLL_RIGHT;
            if (Build$VERSION.SDK_INT >= 23) {
                action_SCROLL_RIGHT = AccessibilityNodeInfo$AccessibilityAction.ACTION_SCROLL_RIGHT;
            }
            else {
                action_SCROLL_RIGHT = null;
            }
            ACTION_SCROLL_RIGHT = new AccessibilityActionCompat(action_SCROLL_RIGHT, 16908347, null, null, null);
            AccessibilityNodeInfo$AccessibilityAction action_PAGE_UP;
            if (Build$VERSION.SDK_INT >= 29) {
                action_PAGE_UP = AccessibilityNodeInfo$AccessibilityAction.ACTION_PAGE_UP;
            }
            else {
                action_PAGE_UP = null;
            }
            ACTION_PAGE_UP = new AccessibilityActionCompat(action_PAGE_UP, 16908358, null, null, null);
            AccessibilityNodeInfo$AccessibilityAction action_PAGE_DOWN;
            if (Build$VERSION.SDK_INT >= 29) {
                action_PAGE_DOWN = AccessibilityNodeInfo$AccessibilityAction.ACTION_PAGE_DOWN;
            }
            else {
                action_PAGE_DOWN = null;
            }
            ACTION_PAGE_DOWN = new AccessibilityActionCompat(action_PAGE_DOWN, 16908359, null, null, null);
            AccessibilityNodeInfo$AccessibilityAction action_PAGE_LEFT;
            if (Build$VERSION.SDK_INT >= 29) {
                action_PAGE_LEFT = AccessibilityNodeInfo$AccessibilityAction.ACTION_PAGE_LEFT;
            }
            else {
                action_PAGE_LEFT = null;
            }
            ACTION_PAGE_LEFT = new AccessibilityActionCompat(action_PAGE_LEFT, 16908360, null, null, null);
            AccessibilityNodeInfo$AccessibilityAction action_PAGE_RIGHT;
            if (Build$VERSION.SDK_INT >= 29) {
                action_PAGE_RIGHT = AccessibilityNodeInfo$AccessibilityAction.ACTION_PAGE_RIGHT;
            }
            else {
                action_PAGE_RIGHT = null;
            }
            ACTION_PAGE_RIGHT = new AccessibilityActionCompat(action_PAGE_RIGHT, 16908361, null, null, null);
            AccessibilityNodeInfo$AccessibilityAction action_CONTEXT_CLICK;
            if (Build$VERSION.SDK_INT >= 23) {
                action_CONTEXT_CLICK = AccessibilityNodeInfo$AccessibilityAction.ACTION_CONTEXT_CLICK;
            }
            else {
                action_CONTEXT_CLICK = null;
            }
            ACTION_CONTEXT_CLICK = new AccessibilityActionCompat(action_CONTEXT_CLICK, 16908348, null, null, null);
            AccessibilityNodeInfo$AccessibilityAction action_SET_PROGRESS;
            if (Build$VERSION.SDK_INT >= 24) {
                action_SET_PROGRESS = AccessibilityNodeInfo$AccessibilityAction.ACTION_SET_PROGRESS;
            }
            else {
                action_SET_PROGRESS = null;
            }
            ACTION_SET_PROGRESS = new AccessibilityActionCompat(action_SET_PROGRESS, 16908349, null, null, AccessibilityViewCommand.SetProgressArguments.class);
            AccessibilityNodeInfo$AccessibilityAction action_MOVE_WINDOW;
            if (Build$VERSION.SDK_INT >= 26) {
                action_MOVE_WINDOW = AccessibilityNodeInfo$AccessibilityAction.ACTION_MOVE_WINDOW;
            }
            else {
                action_MOVE_WINDOW = null;
            }
            ACTION_MOVE_WINDOW = new AccessibilityActionCompat(action_MOVE_WINDOW, 16908354, null, null, AccessibilityViewCommand.MoveWindowArguments.class);
            AccessibilityNodeInfo$AccessibilityAction action_SHOW_TOOLTIP;
            if (Build$VERSION.SDK_INT >= 28) {
                action_SHOW_TOOLTIP = AccessibilityNodeInfo$AccessibilityAction.ACTION_SHOW_TOOLTIP;
            }
            else {
                action_SHOW_TOOLTIP = null;
            }
            ACTION_SHOW_TOOLTIP = new AccessibilityActionCompat(action_SHOW_TOOLTIP, 16908356, null, null, null);
            AccessibilityNodeInfo$AccessibilityAction action_HIDE_TOOLTIP;
            if (Build$VERSION.SDK_INT >= 28) {
                action_HIDE_TOOLTIP = AccessibilityNodeInfo$AccessibilityAction.ACTION_HIDE_TOOLTIP;
            }
            else {
                action_HIDE_TOOLTIP = null;
            }
            ACTION_HIDE_TOOLTIP = new AccessibilityActionCompat(action_HIDE_TOOLTIP, 16908357, null, null, null);
            AccessibilityNodeInfo$AccessibilityAction action_PRESS_AND_HOLD;
            if (Build$VERSION.SDK_INT >= 30) {
                action_PRESS_AND_HOLD = AccessibilityNodeInfo$AccessibilityAction.ACTION_PRESS_AND_HOLD;
            }
            else {
                action_PRESS_AND_HOLD = null;
            }
            ACTION_PRESS_AND_HOLD = new AccessibilityActionCompat(action_PRESS_AND_HOLD, 16908362, null, null, null);
            AccessibilityNodeInfo$AccessibilityAction action_IME_ENTER;
            if (Build$VERSION.SDK_INT >= 30) {
                action_IME_ENTER = AccessibilityNodeInfo$AccessibilityAction.ACTION_IME_ENTER;
            }
            else {
                action_IME_ENTER = null;
            }
            ACTION_IME_ENTER = new AccessibilityActionCompat(action_IME_ENTER, 16908372, null, null, null);
            AccessibilityNodeInfo$AccessibilityAction action_DRAG_START;
            if (Build$VERSION.SDK_INT >= 32) {
                action_DRAG_START = AccessibilityNodeInfo$AccessibilityAction.ACTION_DRAG_START;
            }
            else {
                action_DRAG_START = null;
            }
            ACTION_DRAG_START = new AccessibilityActionCompat(action_DRAG_START, 16908373, null, null, null);
            AccessibilityNodeInfo$AccessibilityAction action_DRAG_DROP;
            if (Build$VERSION.SDK_INT >= 32) {
                action_DRAG_DROP = AccessibilityNodeInfo$AccessibilityAction.ACTION_DRAG_DROP;
            }
            else {
                action_DRAG_DROP = null;
            }
            ACTION_DRAG_DROP = new AccessibilityActionCompat(action_DRAG_DROP, 16908374, null, null, null);
            AccessibilityNodeInfo$AccessibilityAction action_DRAG_CANCEL;
            if (Build$VERSION.SDK_INT >= 32) {
                action_DRAG_CANCEL = AccessibilityNodeInfo$AccessibilityAction.ACTION_DRAG_CANCEL;
            }
            else {
                action_DRAG_CANCEL = null;
            }
            ACTION_DRAG_CANCEL = new AccessibilityActionCompat(action_DRAG_CANCEL, 16908375, null, null, null);
            AccessibilityNodeInfo$AccessibilityAction action_SHOW_TEXT_SUGGESTIONS;
            if (Build$VERSION.SDK_INT >= 33) {
                action_SHOW_TEXT_SUGGESTIONS = AccessibilityNodeInfo$AccessibilityAction.ACTION_SHOW_TEXT_SUGGESTIONS;
            }
            else {
                action_SHOW_TEXT_SUGGESTIONS = null;
            }
            ACTION_SHOW_TEXT_SUGGESTIONS = new AccessibilityActionCompat(action_SHOW_TEXT_SUGGESTIONS, 16908376, null, null, null);
            Object action_SCROLL_IN_DIRECTION = o;
            if (Build$VERSION.SDK_INT >= 34) {
                action_SCROLL_IN_DIRECTION = AccessibilityNodeInfo$AccessibilityAction.ACTION_SCROLL_IN_DIRECTION;
            }
            ACTION_SCROLL_IN_DIRECTION = new AccessibilityActionCompat(action_SCROLL_IN_DIRECTION, 16908382, null, null, null);
        }
        
        public AccessibilityActionCompat(final int n, final CharSequence charSequence) {
            this(null, n, charSequence, null, null);
        }
        
        public AccessibilityActionCompat(final int n, final CharSequence charSequence, final AccessibilityViewCommand accessibilityViewCommand) {
            this(null, n, charSequence, accessibilityViewCommand, null);
        }
        
        private AccessibilityActionCompat(final int n, final CharSequence charSequence, final Class<? extends AccessibilityViewCommand.CommandArguments> clazz) {
            this(null, n, charSequence, null, clazz);
        }
        
        AccessibilityActionCompat(final Object o) {
            this(o, 0, null, null, null);
        }
        
        AccessibilityActionCompat(final Object mAction, final int mId, final CharSequence charSequence, final AccessibilityViewCommand mCommand, final Class<? extends AccessibilityViewCommand.CommandArguments> mViewCommandArgumentClass) {
            this.mId = mId;
            this.mCommand = mCommand;
            if (Build$VERSION.SDK_INT >= 21 && mAction == null) {
                this.mAction = new AccessibilityNodeInfo$AccessibilityAction(mId, charSequence);
            }
            else {
                this.mAction = mAction;
            }
            this.mViewCommandArgumentClass = mViewCommandArgumentClass;
        }
        
        public AccessibilityActionCompat createReplacementAction(final CharSequence charSequence, final AccessibilityViewCommand accessibilityViewCommand) {
            return new AccessibilityActionCompat(null, this.mId, charSequence, accessibilityViewCommand, this.mViewCommandArgumentClass);
        }
        
        @Override
        public boolean equals(final Object o) {
            if (o == null) {
                return false;
            }
            if (!(o instanceof AccessibilityActionCompat)) {
                return false;
            }
            final AccessibilityActionCompat accessibilityActionCompat = (AccessibilityActionCompat)o;
            final Object mAction = this.mAction;
            if (mAction == null) {
                if (accessibilityActionCompat.mAction != null) {
                    return false;
                }
            }
            else if (!mAction.equals(accessibilityActionCompat.mAction)) {
                return false;
            }
            return true;
        }
        
        public int getId() {
            if (Build$VERSION.SDK_INT >= 21) {
                return ((AccessibilityNodeInfo$AccessibilityAction)this.mAction).getId();
            }
            return 0;
        }
        
        public CharSequence getLabel() {
            if (Build$VERSION.SDK_INT >= 21) {
                return ((AccessibilityNodeInfo$AccessibilityAction)this.mAction).getLabel();
            }
            return null;
        }
        
        @Override
        public int hashCode() {
            final Object mAction = this.mAction;
            int hashCode;
            if (mAction != null) {
                hashCode = mAction.hashCode();
            }
            else {
                hashCode = 0;
            }
            return hashCode;
        }
        
        public boolean perform(final View view, final Bundle bundle) {
            if (this.mCommand != null) {
                Object o = null;
                final Exception ex = null;
                final Class<? extends AccessibilityViewCommand.CommandArguments> mViewCommandArgumentClass = this.mViewCommandArgumentClass;
                if (mViewCommandArgumentClass != null) {
                    Object o2;
                    Exception ex2 = null;
                    try {
                        o = (AccessibilityViewCommand.CommandArguments)mViewCommandArgumentClass.getDeclaredConstructor((Class<?>[])new Class[0]).newInstance(new Object[0]);
                        try {
                            ((AccessibilityViewCommand.CommandArguments)o).setBundle(bundle);
                        }
                        catch (final Exception ex) {
                            o2 = o;
                            ex2 = ex;
                        }
                    }
                    catch (final Exception ex2) {
                        o2 = ex;
                    }
                    final Class<? extends AccessibilityViewCommand.CommandArguments> mViewCommandArgumentClass2 = this.mViewCommandArgumentClass;
                    String name;
                    if (mViewCommandArgumentClass2 == null) {
                        name = "null";
                    }
                    else {
                        name = mViewCommandArgumentClass2.getName();
                    }
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Failed to execute command with argument class ViewCommandArgument: ");
                    sb.append(name);
                    Log.e("A11yActionCompat", sb.toString(), (Throwable)ex2);
                    o = o2;
                }
                return this.mCommand.perform(view, (AccessibilityViewCommand.CommandArguments)o);
            }
            return false;
        }
        
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder();
            sb.append("AccessibilityActionCompat: ");
            String str;
            final String s = str = AccessibilityNodeInfoCompat.getActionSymbolicName(this.mId);
            if (s.equals("ACTION_UNKNOWN")) {
                str = s;
                if (this.getLabel() != null) {
                    str = this.getLabel().toString();
                }
            }
            sb.append(str);
            return sb.toString();
        }
    }
    
    private static class Api19Impl
    {
        public static CollectionItemInfoCompat createCollectionItemInfo(final int n, final int n2, final int n3, final int n4, final boolean b) {
            return new CollectionItemInfoCompat(AccessibilityNodeInfo$CollectionItemInfo.obtain(n, n2, n3, n4, b));
        }
        
        public static Object createRangeInfo(final int n, final float n2, final float n3, final float n4) {
            return AccessibilityNodeInfo$RangeInfo.obtain(n, n2, n3, n4);
        }
        
        public static Bundle getExtras(final AccessibilityNodeInfo accessibilityNodeInfo) {
            return accessibilityNodeInfo.getExtras();
        }
    }
    
    private static class Api21Impl
    {
        public static CollectionItemInfoCompat createCollectionItemInfo(final int n, final int n2, final int n3, final int n4, final boolean b, final boolean b2) {
            return new CollectionItemInfoCompat(AccessibilityNodeInfo$CollectionItemInfo.obtain(n, n2, n3, n4, b, b2));
        }
    }
    
    private static class Api30Impl
    {
        public static Object createRangeInfo(final int n, final float n2, final float n3, final float n4) {
            return new AccessibilityNodeInfo$RangeInfo(n, n2, n3, n4);
        }
        
        public static CharSequence getStateDescription(final AccessibilityNodeInfo accessibilityNodeInfo) {
            return accessibilityNodeInfo.getStateDescription();
        }
        
        public static void setStateDescription(final AccessibilityNodeInfo accessibilityNodeInfo, final CharSequence stateDescription) {
            accessibilityNodeInfo.setStateDescription(stateDescription);
        }
    }
    
    private static class Api33Impl
    {
        public static CollectionItemInfoCompat buildCollectionItemInfoCompat(final boolean heading, final int columnIndex, final int rowIndex, final int columnSpan, final int rowSpan, final boolean selected, final String rowTitle, final String columnTitle) {
            return new CollectionItemInfoCompat(new AccessibilityNodeInfo$CollectionItemInfo$Builder().setHeading(heading).setColumnIndex(columnIndex).setRowIndex(rowIndex).setColumnSpan(columnSpan).setRowSpan(rowSpan).setSelected(selected).setRowTitle(rowTitle).setColumnTitle(columnTitle).build());
        }
        
        public static AccessibilityNodeInfoCompat getChild(final AccessibilityNodeInfo accessibilityNodeInfo, final int n, final int n2) {
            return AccessibilityNodeInfoCompat.wrapNonNullInstance(accessibilityNodeInfo.getChild(n, n2));
        }
        
        public static String getCollectionItemColumnTitle(final Object o) {
            return ((AccessibilityNodeInfo$CollectionItemInfo)o).getColumnTitle();
        }
        
        public static String getCollectionItemRowTitle(final Object o) {
            return ((AccessibilityNodeInfo$CollectionItemInfo)o).getRowTitle();
        }
        
        public static AccessibilityNodeInfo$ExtraRenderingInfo getExtraRenderingInfo(final AccessibilityNodeInfo accessibilityNodeInfo) {
            return accessibilityNodeInfo.getExtraRenderingInfo();
        }
        
        public static AccessibilityNodeInfoCompat getParent(final AccessibilityNodeInfo accessibilityNodeInfo, final int n) {
            return AccessibilityNodeInfoCompat.wrapNonNullInstance(accessibilityNodeInfo.getParent(n));
        }
        
        public static String getUniqueId(final AccessibilityNodeInfo accessibilityNodeInfo) {
            return accessibilityNodeInfo.getUniqueId();
        }
        
        public static boolean isTextSelectable(final AccessibilityNodeInfo accessibilityNodeInfo) {
            return accessibilityNodeInfo.isTextSelectable();
        }
        
        public static void setTextSelectable(final AccessibilityNodeInfo accessibilityNodeInfo, final boolean textSelectable) {
            accessibilityNodeInfo.setTextSelectable(textSelectable);
        }
        
        public static void setUniqueId(final AccessibilityNodeInfo accessibilityNodeInfo, final String uniqueId) {
            accessibilityNodeInfo.setUniqueId(uniqueId);
        }
    }
    
    private static class Api34Impl
    {
        public static void getBoundsInWindow(final AccessibilityNodeInfo accessibilityNodeInfo, final Rect rect) {
            accessibilityNodeInfo.getBoundsInWindow(rect);
        }
        
        public static CharSequence getContainerTitle(final AccessibilityNodeInfo accessibilityNodeInfo) {
            return accessibilityNodeInfo.getContainerTitle();
        }
        
        public static long getMinDurationBetweenContentChangeMillis(final AccessibilityNodeInfo accessibilityNodeInfo) {
            return accessibilityNodeInfo.getMinDurationBetweenContentChanges().toMillis();
        }
        
        public static boolean hasRequestInitialAccessibilityFocus(final AccessibilityNodeInfo accessibilityNodeInfo) {
            return accessibilityNodeInfo.hasRequestInitialAccessibilityFocus();
        }
        
        public static boolean isAccessibilityDataSensitive(final AccessibilityNodeInfo accessibilityNodeInfo) {
            return accessibilityNodeInfo.isAccessibilityDataSensitive();
        }
        
        public static void setAccessibilityDataSensitive(final AccessibilityNodeInfo accessibilityNodeInfo, final boolean accessibilityDataSensitive) {
            accessibilityNodeInfo.setAccessibilityDataSensitive(accessibilityDataSensitive);
        }
        
        public static void setBoundsInWindow(final AccessibilityNodeInfo accessibilityNodeInfo, final Rect boundsInWindow) {
            accessibilityNodeInfo.setBoundsInWindow(boundsInWindow);
        }
        
        public static void setContainerTitle(final AccessibilityNodeInfo accessibilityNodeInfo, final CharSequence containerTitle) {
            accessibilityNodeInfo.setContainerTitle(containerTitle);
        }
        
        public static void setMinDurationBetweenContentChangeMillis(final AccessibilityNodeInfo accessibilityNodeInfo, final long millis) {
            accessibilityNodeInfo.setMinDurationBetweenContentChanges(Duration.ofMillis(millis));
        }
        
        public static void setQueryFromAppProcessEnabled(final AccessibilityNodeInfo accessibilityNodeInfo, final View view, final boolean b) {
            accessibilityNodeInfo.setQueryFromAppProcessEnabled(view, b);
        }
        
        public static void setRequestInitialAccessibilityFocus(final AccessibilityNodeInfo accessibilityNodeInfo, final boolean requestInitialAccessibilityFocus) {
            accessibilityNodeInfo.setRequestInitialAccessibilityFocus(requestInitialAccessibilityFocus);
        }
    }
    
    public static class CollectionInfoCompat
    {
        public static final int SELECTION_MODE_MULTIPLE = 2;
        public static final int SELECTION_MODE_NONE = 0;
        public static final int SELECTION_MODE_SINGLE = 1;
        final Object mInfo;
        
        CollectionInfoCompat(final Object mInfo) {
            this.mInfo = mInfo;
        }
        
        public static CollectionInfoCompat obtain(final int n, final int n2, final boolean b) {
            if (Build$VERSION.SDK_INT >= 19) {
                return new CollectionInfoCompat(AccessibilityNodeInfo$CollectionInfo.obtain(n, n2, b));
            }
            return new CollectionInfoCompat(null);
        }
        
        public static CollectionInfoCompat obtain(final int n, final int n2, final boolean b, final int n3) {
            if (Build$VERSION.SDK_INT >= 21) {
                return new CollectionInfoCompat(AccessibilityNodeInfo$CollectionInfo.obtain(n, n2, b, n3));
            }
            if (Build$VERSION.SDK_INT >= 19) {
                return new CollectionInfoCompat(AccessibilityNodeInfo$CollectionInfo.obtain(n, n2, b));
            }
            return new CollectionInfoCompat(null);
        }
        
        public int getColumnCount() {
            if (Build$VERSION.SDK_INT >= 19) {
                return ((AccessibilityNodeInfo$CollectionInfo)this.mInfo).getColumnCount();
            }
            return -1;
        }
        
        public int getRowCount() {
            if (Build$VERSION.SDK_INT >= 19) {
                return ((AccessibilityNodeInfo$CollectionInfo)this.mInfo).getRowCount();
            }
            return -1;
        }
        
        public int getSelectionMode() {
            if (Build$VERSION.SDK_INT >= 21) {
                return ((AccessibilityNodeInfo$CollectionInfo)this.mInfo).getSelectionMode();
            }
            return 0;
        }
        
        public boolean isHierarchical() {
            return Build$VERSION.SDK_INT >= 19 && ((AccessibilityNodeInfo$CollectionInfo)this.mInfo).isHierarchical();
        }
    }
    
    public static class CollectionItemInfoCompat
    {
        final Object mInfo;
        
        CollectionItemInfoCompat(final Object mInfo) {
            this.mInfo = mInfo;
        }
        
        public static CollectionItemInfoCompat obtain(final int n, final int n2, final int n3, final int n4, final boolean b) {
            if (Build$VERSION.SDK_INT >= 19) {
                return new CollectionItemInfoCompat(AccessibilityNodeInfo$CollectionItemInfo.obtain(n, n2, n3, n4, b));
            }
            return new CollectionItemInfoCompat(null);
        }
        
        public static CollectionItemInfoCompat obtain(final int n, final int n2, final int n3, final int n4, final boolean b, final boolean b2) {
            if (Build$VERSION.SDK_INT >= 21) {
                return new CollectionItemInfoCompat(AccessibilityNodeInfo$CollectionItemInfo.obtain(n, n2, n3, n4, b, b2));
            }
            if (Build$VERSION.SDK_INT >= 19) {
                return new CollectionItemInfoCompat(AccessibilityNodeInfo$CollectionItemInfo.obtain(n, n2, n3, n4, b));
            }
            return new CollectionItemInfoCompat(null);
        }
        
        public int getColumnIndex() {
            if (Build$VERSION.SDK_INT >= 19) {
                return ((AccessibilityNodeInfo$CollectionItemInfo)this.mInfo).getColumnIndex();
            }
            return 0;
        }
        
        public int getColumnSpan() {
            if (Build$VERSION.SDK_INT >= 19) {
                return ((AccessibilityNodeInfo$CollectionItemInfo)this.mInfo).getColumnSpan();
            }
            return 0;
        }
        
        public String getColumnTitle() {
            if (Build$VERSION.SDK_INT >= 33) {
                return Api33Impl.getCollectionItemColumnTitle(this.mInfo);
            }
            return null;
        }
        
        public int getRowIndex() {
            if (Build$VERSION.SDK_INT >= 19) {
                return ((AccessibilityNodeInfo$CollectionItemInfo)this.mInfo).getRowIndex();
            }
            return 0;
        }
        
        public int getRowSpan() {
            if (Build$VERSION.SDK_INT >= 19) {
                return ((AccessibilityNodeInfo$CollectionItemInfo)this.mInfo).getRowSpan();
            }
            return 0;
        }
        
        public String getRowTitle() {
            if (Build$VERSION.SDK_INT >= 33) {
                return Api33Impl.getCollectionItemRowTitle(this.mInfo);
            }
            return null;
        }
        
        @Deprecated
        public boolean isHeading() {
            return Build$VERSION.SDK_INT >= 19 && ((AccessibilityNodeInfo$CollectionItemInfo)this.mInfo).isHeading();
        }
        
        public boolean isSelected() {
            return Build$VERSION.SDK_INT >= 21 && ((AccessibilityNodeInfo$CollectionItemInfo)this.mInfo).isSelected();
        }
        
        public static final class Builder
        {
            private int mColumnIndex;
            private int mColumnSpan;
            private String mColumnTitle;
            private boolean mHeading;
            private int mRowIndex;
            private int mRowSpan;
            private String mRowTitle;
            private boolean mSelected;
            
            public CollectionItemInfoCompat build() {
                if (Build$VERSION.SDK_INT >= 33) {
                    return Api33Impl.buildCollectionItemInfoCompat(this.mHeading, this.mColumnIndex, this.mRowIndex, this.mColumnSpan, this.mRowSpan, this.mSelected, this.mRowTitle, this.mColumnTitle);
                }
                if (Build$VERSION.SDK_INT >= 21) {
                    return Api21Impl.createCollectionItemInfo(this.mRowIndex, this.mRowSpan, this.mColumnIndex, this.mColumnSpan, this.mHeading, this.mSelected);
                }
                if (Build$VERSION.SDK_INT >= 19) {
                    return Api19Impl.createCollectionItemInfo(this.mRowIndex, this.mRowSpan, this.mColumnIndex, this.mColumnSpan, this.mHeading);
                }
                return new CollectionItemInfoCompat(null);
            }
            
            public Builder setColumnIndex(final int mColumnIndex) {
                this.mColumnIndex = mColumnIndex;
                return this;
            }
            
            public Builder setColumnSpan(final int mColumnSpan) {
                this.mColumnSpan = mColumnSpan;
                return this;
            }
            
            public Builder setColumnTitle(final String mColumnTitle) {
                this.mColumnTitle = mColumnTitle;
                return this;
            }
            
            public Builder setHeading(final boolean mHeading) {
                this.mHeading = mHeading;
                return this;
            }
            
            public Builder setRowIndex(final int mRowIndex) {
                this.mRowIndex = mRowIndex;
                return this;
            }
            
            public Builder setRowSpan(final int mRowSpan) {
                this.mRowSpan = mRowSpan;
                return this;
            }
            
            public Builder setRowTitle(final String mRowTitle) {
                this.mRowTitle = mRowTitle;
                return this;
            }
            
            public Builder setSelected(final boolean mSelected) {
                this.mSelected = mSelected;
                return this;
            }
        }
    }
    
    public static class RangeInfoCompat
    {
        public static final int RANGE_TYPE_FLOAT = 1;
        public static final int RANGE_TYPE_INT = 0;
        public static final int RANGE_TYPE_PERCENT = 2;
        final Object mInfo;
        
        public RangeInfoCompat(final int n, final float n2, final float n3, final float n4) {
            if (Build$VERSION.SDK_INT >= 30) {
                this.mInfo = Api30Impl.createRangeInfo(n, n2, n3, n4);
            }
            else if (Build$VERSION.SDK_INT >= 19) {
                this.mInfo = Api19Impl.createRangeInfo(n, n2, n3, n4);
            }
            else {
                this.mInfo = null;
            }
        }
        
        RangeInfoCompat(final Object mInfo) {
            this.mInfo = mInfo;
        }
        
        public static RangeInfoCompat obtain(final int n, final float n2, final float n3, final float n4) {
            if (Build$VERSION.SDK_INT >= 19) {
                return new RangeInfoCompat(AccessibilityNodeInfo$RangeInfo.obtain(n, n2, n3, n4));
            }
            return new RangeInfoCompat(null);
        }
        
        public float getCurrent() {
            if (Build$VERSION.SDK_INT >= 19) {
                return ((AccessibilityNodeInfo$RangeInfo)this.mInfo).getCurrent();
            }
            return 0.0f;
        }
        
        public float getMax() {
            if (Build$VERSION.SDK_INT >= 19) {
                return ((AccessibilityNodeInfo$RangeInfo)this.mInfo).getMax();
            }
            return 0.0f;
        }
        
        public float getMin() {
            if (Build$VERSION.SDK_INT >= 19) {
                return ((AccessibilityNodeInfo$RangeInfo)this.mInfo).getMin();
            }
            return 0.0f;
        }
        
        public int getType() {
            if (Build$VERSION.SDK_INT >= 19) {
                return ((AccessibilityNodeInfo$RangeInfo)this.mInfo).getType();
            }
            return 0;
        }
    }
    
    public static final class TouchDelegateInfoCompat
    {
        final AccessibilityNodeInfo$TouchDelegateInfo mInfo;
        
        TouchDelegateInfoCompat(final AccessibilityNodeInfo$TouchDelegateInfo mInfo) {
            this.mInfo = mInfo;
        }
        
        public TouchDelegateInfoCompat(final Map<Region, View> map) {
            if (Build$VERSION.SDK_INT >= 29) {
                this.mInfo = new AccessibilityNodeInfo$TouchDelegateInfo((Map)map);
            }
            else {
                this.mInfo = null;
            }
        }
        
        public Region getRegionAt(final int n) {
            if (Build$VERSION.SDK_INT >= 29) {
                return this.mInfo.getRegionAt(n);
            }
            return null;
        }
        
        public int getRegionCount() {
            if (Build$VERSION.SDK_INT >= 29) {
                return this.mInfo.getRegionCount();
            }
            return 0;
        }
        
        public AccessibilityNodeInfoCompat getTargetForRegion(final Region region) {
            if (Build$VERSION.SDK_INT >= 29) {
                final AccessibilityNodeInfo targetForRegion = this.mInfo.getTargetForRegion(region);
                if (targetForRegion != null) {
                    return AccessibilityNodeInfoCompat.wrap(targetForRegion);
                }
            }
            return null;
        }
    }
}

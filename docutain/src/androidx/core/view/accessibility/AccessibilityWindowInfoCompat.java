// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.view.accessibility;

import android.os.LocaleList;
import android.view.accessibility.AccessibilityNodeInfo;
import android.graphics.Region;
import androidx.core.os.LocaleListCompat;
import android.graphics.Rect;
import android.view.accessibility.AccessibilityWindowInfo;
import android.os.Build$VERSION;

public class AccessibilityWindowInfoCompat
{
    public static final int TYPE_ACCESSIBILITY_OVERLAY = 4;
    public static final int TYPE_APPLICATION = 1;
    public static final int TYPE_INPUT_METHOD = 2;
    public static final int TYPE_MAGNIFICATION_OVERLAY = 6;
    public static final int TYPE_SPLIT_SCREEN_DIVIDER = 5;
    public static final int TYPE_SYSTEM = 3;
    private static final int UNDEFINED = -1;
    private final Object mInfo;
    
    public AccessibilityWindowInfoCompat() {
        if (Build$VERSION.SDK_INT >= 30) {
            this.mInfo = Api30Impl.instantiateAccessibilityWindowInfo();
        }
        else {
            this.mInfo = null;
        }
    }
    
    private AccessibilityWindowInfoCompat(final Object mInfo) {
        this.mInfo = mInfo;
    }
    
    public static AccessibilityWindowInfoCompat obtain() {
        if (Build$VERSION.SDK_INT >= 21) {
            return wrapNonNullInstance(Api21Impl.obtain());
        }
        return null;
    }
    
    public static AccessibilityWindowInfoCompat obtain(final AccessibilityWindowInfoCompat accessibilityWindowInfoCompat) {
        final int sdk_INT = Build$VERSION.SDK_INT;
        AccessibilityWindowInfoCompat wrapNonNullInstance = null;
        if (sdk_INT >= 21) {
            if (accessibilityWindowInfoCompat == null) {
                wrapNonNullInstance = wrapNonNullInstance;
            }
            else {
                wrapNonNullInstance = wrapNonNullInstance(Api21Impl.obtain((AccessibilityWindowInfo)accessibilityWindowInfoCompat.mInfo));
            }
        }
        return wrapNonNullInstance;
    }
    
    private static String typeToString(final int n) {
        if (n == 1) {
            return "TYPE_APPLICATION";
        }
        if (n == 2) {
            return "TYPE_INPUT_METHOD";
        }
        if (n == 3) {
            return "TYPE_SYSTEM";
        }
        if (n != 4) {
            return "<UNKNOWN>";
        }
        return "TYPE_ACCESSIBILITY_OVERLAY";
    }
    
    static AccessibilityWindowInfoCompat wrapNonNullInstance(final Object o) {
        if (o != null) {
            return new AccessibilityWindowInfoCompat(o);
        }
        return null;
    }
    
    @Override
    public boolean equals(Object mInfo) {
        boolean b = true;
        if (this == mInfo) {
            return true;
        }
        if (mInfo == null) {
            return false;
        }
        if (!(mInfo instanceof AccessibilityWindowInfoCompat)) {
            return false;
        }
        final AccessibilityWindowInfoCompat accessibilityWindowInfoCompat = (AccessibilityWindowInfoCompat)mInfo;
        mInfo = this.mInfo;
        if (mInfo == null) {
            if (accessibilityWindowInfoCompat.mInfo != null) {
                b = false;
            }
            return b;
        }
        return mInfo.equals(accessibilityWindowInfoCompat.mInfo);
    }
    
    public AccessibilityNodeInfoCompat getAnchor() {
        if (Build$VERSION.SDK_INT >= 24) {
            return AccessibilityNodeInfoCompat.wrapNonNullInstance(Api24Impl.getAnchor((AccessibilityWindowInfo)this.mInfo));
        }
        return null;
    }
    
    public void getBoundsInScreen(final Rect rect) {
        if (Build$VERSION.SDK_INT >= 21) {
            Api21Impl.getBoundsInScreen((AccessibilityWindowInfo)this.mInfo, rect);
        }
    }
    
    public AccessibilityWindowInfoCompat getChild(final int n) {
        if (Build$VERSION.SDK_INT >= 21) {
            return wrapNonNullInstance(Api21Impl.getChild((AccessibilityWindowInfo)this.mInfo, n));
        }
        return null;
    }
    
    public int getChildCount() {
        if (Build$VERSION.SDK_INT >= 21) {
            return Api21Impl.getChildCount((AccessibilityWindowInfo)this.mInfo);
        }
        return 0;
    }
    
    public int getDisplayId() {
        if (Build$VERSION.SDK_INT >= 33) {
            return Api33Impl.getDisplayId((AccessibilityWindowInfo)this.mInfo);
        }
        return 0;
    }
    
    public int getId() {
        if (Build$VERSION.SDK_INT >= 21) {
            return Api21Impl.getId((AccessibilityWindowInfo)this.mInfo);
        }
        return -1;
    }
    
    public int getLayer() {
        if (Build$VERSION.SDK_INT >= 21) {
            return Api21Impl.getLayer((AccessibilityWindowInfo)this.mInfo);
        }
        return -1;
    }
    
    public LocaleListCompat getLocales() {
        if (Build$VERSION.SDK_INT >= 34) {
            return LocaleListCompat.wrap(Api34Impl.getLocales((AccessibilityWindowInfo)this.mInfo));
        }
        return LocaleListCompat.getEmptyLocaleList();
    }
    
    public AccessibilityWindowInfoCompat getParent() {
        if (Build$VERSION.SDK_INT >= 21) {
            return wrapNonNullInstance(Api21Impl.getParent((AccessibilityWindowInfo)this.mInfo));
        }
        return null;
    }
    
    public void getRegionInScreen(final Region region) {
        if (Build$VERSION.SDK_INT >= 33) {
            Api33Impl.getRegionInScreen((AccessibilityWindowInfo)this.mInfo, region);
        }
        else if (Build$VERSION.SDK_INT >= 21) {
            final Rect rect = new Rect();
            Api21Impl.getBoundsInScreen((AccessibilityWindowInfo)this.mInfo, rect);
            region.set(rect);
        }
    }
    
    public AccessibilityNodeInfoCompat getRoot() {
        if (Build$VERSION.SDK_INT >= 21) {
            return AccessibilityNodeInfoCompat.wrapNonNullInstance(Api21Impl.getRoot((AccessibilityWindowInfo)this.mInfo));
        }
        return null;
    }
    
    public AccessibilityNodeInfoCompat getRoot(final int n) {
        if (Build$VERSION.SDK_INT >= 33) {
            return Api33Impl.getRoot(this.mInfo, n);
        }
        return this.getRoot();
    }
    
    public CharSequence getTitle() {
        if (Build$VERSION.SDK_INT >= 24) {
            return Api24Impl.getTitle((AccessibilityWindowInfo)this.mInfo);
        }
        return null;
    }
    
    public long getTransitionTimeMillis() {
        if (Build$VERSION.SDK_INT >= 34) {
            return Api34Impl.getTransitionTimeMillis((AccessibilityWindowInfo)this.mInfo);
        }
        return 0L;
    }
    
    public int getType() {
        if (Build$VERSION.SDK_INT >= 21) {
            return Api21Impl.getType((AccessibilityWindowInfo)this.mInfo);
        }
        return -1;
    }
    
    @Override
    public int hashCode() {
        final Object mInfo = this.mInfo;
        int hashCode;
        if (mInfo == null) {
            hashCode = 0;
        }
        else {
            hashCode = mInfo.hashCode();
        }
        return hashCode;
    }
    
    public boolean isAccessibilityFocused() {
        return Build$VERSION.SDK_INT < 21 || Api21Impl.isAccessibilityFocused((AccessibilityWindowInfo)this.mInfo);
    }
    
    public boolean isActive() {
        return Build$VERSION.SDK_INT < 21 || Api21Impl.isActive((AccessibilityWindowInfo)this.mInfo);
    }
    
    public boolean isFocused() {
        return Build$VERSION.SDK_INT < 21 || Api21Impl.isFocused((AccessibilityWindowInfo)this.mInfo);
    }
    
    public boolean isInPictureInPictureMode() {
        return Build$VERSION.SDK_INT >= 26 && Api26Impl.isInPictureInPictureMode((AccessibilityWindowInfo)this.mInfo);
    }
    
    @Deprecated
    public void recycle() {
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        final Rect obj = new Rect();
        this.getBoundsInScreen(obj);
        sb.append("AccessibilityWindowInfo[");
        sb.append("id=");
        sb.append(this.getId());
        sb.append(", type=");
        sb.append(typeToString(this.getType()));
        sb.append(", layer=");
        sb.append(this.getLayer());
        sb.append(", bounds=");
        sb.append(obj);
        sb.append(", focused=");
        sb.append(this.isFocused());
        sb.append(", active=");
        sb.append(this.isActive());
        sb.append(", hasParent=");
        final AccessibilityWindowInfoCompat parent = this.getParent();
        final boolean b = true;
        sb.append(parent != null);
        sb.append(", hasChildren=");
        sb.append(this.getChildCount() > 0 && b);
        sb.append(", transitionTime=");
        sb.append(this.getTransitionTimeMillis());
        sb.append(", locales=");
        sb.append(this.getLocales());
        sb.append(']');
        return sb.toString();
    }
    
    public AccessibilityWindowInfo unwrap() {
        if (Build$VERSION.SDK_INT >= 21) {
            return (AccessibilityWindowInfo)this.mInfo;
        }
        return null;
    }
    
    private static class Api21Impl
    {
        static void getBoundsInScreen(final AccessibilityWindowInfo accessibilityWindowInfo, final Rect rect) {
            accessibilityWindowInfo.getBoundsInScreen(rect);
        }
        
        static AccessibilityWindowInfo getChild(final AccessibilityWindowInfo accessibilityWindowInfo, final int n) {
            return accessibilityWindowInfo.getChild(n);
        }
        
        static int getChildCount(final AccessibilityWindowInfo accessibilityWindowInfo) {
            return accessibilityWindowInfo.getChildCount();
        }
        
        static int getId(final AccessibilityWindowInfo accessibilityWindowInfo) {
            return accessibilityWindowInfo.getId();
        }
        
        static int getLayer(final AccessibilityWindowInfo accessibilityWindowInfo) {
            return accessibilityWindowInfo.getLayer();
        }
        
        static AccessibilityWindowInfo getParent(final AccessibilityWindowInfo accessibilityWindowInfo) {
            return accessibilityWindowInfo.getParent();
        }
        
        static AccessibilityNodeInfo getRoot(final AccessibilityWindowInfo accessibilityWindowInfo) {
            return accessibilityWindowInfo.getRoot();
        }
        
        static int getType(final AccessibilityWindowInfo accessibilityWindowInfo) {
            return accessibilityWindowInfo.getType();
        }
        
        static boolean isAccessibilityFocused(final AccessibilityWindowInfo accessibilityWindowInfo) {
            return accessibilityWindowInfo.isAccessibilityFocused();
        }
        
        static boolean isActive(final AccessibilityWindowInfo accessibilityWindowInfo) {
            return accessibilityWindowInfo.isActive();
        }
        
        static boolean isFocused(final AccessibilityWindowInfo accessibilityWindowInfo) {
            return accessibilityWindowInfo.isFocused();
        }
        
        static AccessibilityWindowInfo obtain() {
            return AccessibilityWindowInfo.obtain();
        }
        
        static AccessibilityWindowInfo obtain(final AccessibilityWindowInfo accessibilityWindowInfo) {
            return AccessibilityWindowInfo.obtain(accessibilityWindowInfo);
        }
    }
    
    private static class Api24Impl
    {
        static AccessibilityNodeInfo getAnchor(final AccessibilityWindowInfo accessibilityWindowInfo) {
            return accessibilityWindowInfo.getAnchor();
        }
        
        static CharSequence getTitle(final AccessibilityWindowInfo accessibilityWindowInfo) {
            return accessibilityWindowInfo.getTitle();
        }
    }
    
    private static class Api26Impl
    {
        static boolean isInPictureInPictureMode(final AccessibilityWindowInfo accessibilityWindowInfo) {
            return accessibilityWindowInfo.isInPictureInPictureMode();
        }
    }
    
    private static class Api30Impl
    {
        static AccessibilityWindowInfo instantiateAccessibilityWindowInfo() {
            return new AccessibilityWindowInfo();
        }
    }
    
    private static class Api33Impl
    {
        static int getDisplayId(final AccessibilityWindowInfo accessibilityWindowInfo) {
            return accessibilityWindowInfo.getDisplayId();
        }
        
        static void getRegionInScreen(final AccessibilityWindowInfo accessibilityWindowInfo, final Region region) {
            accessibilityWindowInfo.getRegionInScreen(region);
        }
        
        public static AccessibilityNodeInfoCompat getRoot(final Object o, final int n) {
            return AccessibilityNodeInfoCompat.wrapNonNullInstance(((AccessibilityWindowInfo)o).getRoot(n));
        }
    }
    
    private static class Api34Impl
    {
        static LocaleList getLocales(final AccessibilityWindowInfo accessibilityWindowInfo) {
            return accessibilityWindowInfo.getLocales();
        }
        
        public static long getTransitionTimeMillis(final AccessibilityWindowInfo accessibilityWindowInfo) {
            return accessibilityWindowInfo.getTransitionTimeMillis();
        }
    }
}

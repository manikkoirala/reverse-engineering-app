// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.view.accessibility;

import android.view.accessibility.AccessibilityManager$TouchExplorationStateChangeListener;
import android.accessibilityservice.AccessibilityServiceInfo;
import java.util.List;
import android.os.Build$VERSION;
import android.view.accessibility.AccessibilityManager$AccessibilityStateChangeListener;
import android.view.accessibility.AccessibilityManager;

public final class AccessibilityManagerCompat
{
    private AccessibilityManagerCompat() {
    }
    
    @Deprecated
    public static boolean addAccessibilityStateChangeListener(final AccessibilityManager accessibilityManager, final AccessibilityStateChangeListener accessibilityStateChangeListener) {
        return accessibilityStateChangeListener != null && accessibilityManager.addAccessibilityStateChangeListener((AccessibilityManager$AccessibilityStateChangeListener)new AccessibilityStateChangeListenerWrapper(accessibilityStateChangeListener));
    }
    
    public static boolean addTouchExplorationStateChangeListener(final AccessibilityManager accessibilityManager, final TouchExplorationStateChangeListener touchExplorationStateChangeListener) {
        return Build$VERSION.SDK_INT >= 19 && Api19Impl.addTouchExplorationStateChangeListenerWrapper(accessibilityManager, touchExplorationStateChangeListener);
    }
    
    @Deprecated
    public static List<AccessibilityServiceInfo> getEnabledAccessibilityServiceList(final AccessibilityManager accessibilityManager, final int n) {
        return accessibilityManager.getEnabledAccessibilityServiceList(n);
    }
    
    @Deprecated
    public static List<AccessibilityServiceInfo> getInstalledAccessibilityServiceList(final AccessibilityManager accessibilityManager) {
        return accessibilityManager.getInstalledAccessibilityServiceList();
    }
    
    public static boolean isRequestFromAccessibilityTool(final AccessibilityManager accessibilityManager) {
        return Build$VERSION.SDK_INT < 34 || Api34Impl.isRequestFromAccessibilityTool(accessibilityManager);
    }
    
    @Deprecated
    public static boolean isTouchExplorationEnabled(final AccessibilityManager accessibilityManager) {
        return accessibilityManager.isTouchExplorationEnabled();
    }
    
    @Deprecated
    public static boolean removeAccessibilityStateChangeListener(final AccessibilityManager accessibilityManager, final AccessibilityStateChangeListener accessibilityStateChangeListener) {
        return accessibilityStateChangeListener != null && accessibilityManager.removeAccessibilityStateChangeListener((AccessibilityManager$AccessibilityStateChangeListener)new AccessibilityStateChangeListenerWrapper(accessibilityStateChangeListener));
    }
    
    public static boolean removeTouchExplorationStateChangeListener(final AccessibilityManager accessibilityManager, final TouchExplorationStateChangeListener touchExplorationStateChangeListener) {
        return Build$VERSION.SDK_INT >= 19 && Api19Impl.removeTouchExplorationStateChangeListenerWrapper(accessibilityManager, touchExplorationStateChangeListener);
    }
    
    @Deprecated
    public interface AccessibilityStateChangeListener
    {
        @Deprecated
        void onAccessibilityStateChanged(final boolean p0);
    }
    
    @Deprecated
    public abstract static class AccessibilityStateChangeListenerCompat implements AccessibilityStateChangeListener
    {
    }
    
    private static class AccessibilityStateChangeListenerWrapper implements AccessibilityManager$AccessibilityStateChangeListener
    {
        AccessibilityStateChangeListener mListener;
        
        AccessibilityStateChangeListenerWrapper(final AccessibilityStateChangeListener mListener) {
            this.mListener = mListener;
        }
        
        @Override
        public boolean equals(final Object o) {
            return this == o || (o instanceof AccessibilityStateChangeListenerWrapper && this.mListener.equals(((AccessibilityStateChangeListenerWrapper)o).mListener));
        }
        
        @Override
        public int hashCode() {
            return this.mListener.hashCode();
        }
        
        public void onAccessibilityStateChanged(final boolean b) {
            this.mListener.onAccessibilityStateChanged(b);
        }
    }
    
    static class Api19Impl
    {
        private Api19Impl() {
        }
        
        static boolean addTouchExplorationStateChangeListenerWrapper(final AccessibilityManager accessibilityManager, final TouchExplorationStateChangeListener touchExplorationStateChangeListener) {
            return accessibilityManager.addTouchExplorationStateChangeListener((AccessibilityManager$TouchExplorationStateChangeListener)new TouchExplorationStateChangeListenerWrapper(touchExplorationStateChangeListener));
        }
        
        static boolean removeTouchExplorationStateChangeListenerWrapper(final AccessibilityManager accessibilityManager, final TouchExplorationStateChangeListener touchExplorationStateChangeListener) {
            return accessibilityManager.removeTouchExplorationStateChangeListener((AccessibilityManager$TouchExplorationStateChangeListener)new TouchExplorationStateChangeListenerWrapper(touchExplorationStateChangeListener));
        }
    }
    
    static class Api34Impl
    {
        private Api34Impl() {
        }
        
        static boolean isRequestFromAccessibilityTool(final AccessibilityManager accessibilityManager) {
            return accessibilityManager.isRequestFromAccessibilityTool();
        }
    }
    
    public interface TouchExplorationStateChangeListener
    {
        void onTouchExplorationStateChanged(final boolean p0);
    }
    
    private static final class TouchExplorationStateChangeListenerWrapper implements AccessibilityManager$TouchExplorationStateChangeListener
    {
        final TouchExplorationStateChangeListener mListener;
        
        TouchExplorationStateChangeListenerWrapper(final TouchExplorationStateChangeListener mListener) {
            this.mListener = mListener;
        }
        
        @Override
        public boolean equals(final Object o) {
            return this == o || (o instanceof TouchExplorationStateChangeListenerWrapper && this.mListener.equals(((TouchExplorationStateChangeListenerWrapper)o).mListener));
        }
        
        @Override
        public int hashCode() {
            return this.mListener.hashCode();
        }
        
        public void onTouchExplorationStateChanged(final boolean b) {
            this.mListener.onTouchExplorationStateChanged(b);
        }
    }
}

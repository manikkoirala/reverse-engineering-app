// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.provider;

import android.graphics.Typeface;
import android.os.Handler;

class CallbackWithHandler
{
    private final FontsContractCompat.FontRequestCallback mCallback;
    private final Handler mCallbackHandler;
    
    CallbackWithHandler(final FontsContractCompat.FontRequestCallback mCallback) {
        this.mCallback = mCallback;
        this.mCallbackHandler = CalleeHandler.create();
    }
    
    CallbackWithHandler(final FontsContractCompat.FontRequestCallback mCallback, final Handler mCallbackHandler) {
        this.mCallback = mCallback;
        this.mCallbackHandler = mCallbackHandler;
    }
    
    private void onTypefaceRequestFailed(final int n) {
        this.mCallbackHandler.post((Runnable)new Runnable(this, this.mCallback, n) {
            final CallbackWithHandler this$0;
            final FontsContractCompat.FontRequestCallback val$callback;
            final int val$reason;
            
            @Override
            public void run() {
                this.val$callback.onTypefaceRequestFailed(this.val$reason);
            }
        });
    }
    
    private void onTypefaceRetrieved(final Typeface typeface) {
        this.mCallbackHandler.post((Runnable)new Runnable(this, this.mCallback, typeface) {
            final CallbackWithHandler this$0;
            final FontsContractCompat.FontRequestCallback val$callback;
            final Typeface val$typeface;
            
            @Override
            public void run() {
                this.val$callback.onTypefaceRetrieved(this.val$typeface);
            }
        });
    }
    
    void onTypefaceResult(final FontRequestWorker.TypefaceResult typefaceResult) {
        if (typefaceResult.isSuccess()) {
            this.onTypefaceRetrieved(typefaceResult.mTypeface);
        }
        else {
            this.onTypefaceRequestFailed(typefaceResult.mResult);
        }
    }
}

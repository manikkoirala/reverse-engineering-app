// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.provider;

import android.provider.DocumentsContract;
import java.util.List;
import android.content.Context;
import java.io.FileNotFoundException;
import android.content.ContentResolver;
import android.os.Build$VERSION;
import android.net.Uri;

public final class DocumentsContractCompat
{
    private static final String PATH_TREE = "tree";
    
    private DocumentsContractCompat() {
    }
    
    public static Uri buildChildDocumentsUri(final String s, final String s2) {
        if (Build$VERSION.SDK_INT >= 21) {
            return DocumentsContractApi21Impl.buildChildDocumentsUri(s, s2);
        }
        return null;
    }
    
    public static Uri buildChildDocumentsUriUsingTree(final Uri uri, final String s) {
        if (Build$VERSION.SDK_INT >= 21) {
            return DocumentsContractApi21Impl.buildChildDocumentsUriUsingTree(uri, s);
        }
        return null;
    }
    
    public static Uri buildDocumentUri(final String s, final String s2) {
        if (Build$VERSION.SDK_INT >= 19) {
            return DocumentsContractApi19Impl.buildDocumentUri(s, s2);
        }
        return null;
    }
    
    public static Uri buildDocumentUriUsingTree(final Uri uri, final String s) {
        if (Build$VERSION.SDK_INT >= 21) {
            return DocumentsContractApi21Impl.buildDocumentUriUsingTree(uri, s);
        }
        return null;
    }
    
    public static Uri buildTreeDocumentUri(final String s, final String s2) {
        if (Build$VERSION.SDK_INT >= 21) {
            return DocumentsContractApi21Impl.buildTreeDocumentUri(s, s2);
        }
        return null;
    }
    
    public static Uri createDocument(final ContentResolver contentResolver, final Uri uri, final String s, final String s2) throws FileNotFoundException {
        if (Build$VERSION.SDK_INT >= 21) {
            return DocumentsContractApi21Impl.createDocument(contentResolver, uri, s, s2);
        }
        return null;
    }
    
    public static String getDocumentId(final Uri uri) {
        if (Build$VERSION.SDK_INT >= 19) {
            return DocumentsContractApi19Impl.getDocumentId(uri);
        }
        return null;
    }
    
    public static String getTreeDocumentId(final Uri uri) {
        if (Build$VERSION.SDK_INT >= 21) {
            return DocumentsContractApi21Impl.getTreeDocumentId(uri);
        }
        return null;
    }
    
    public static boolean isDocumentUri(final Context context, final Uri uri) {
        return Build$VERSION.SDK_INT >= 19 && DocumentsContractApi19Impl.isDocumentUri(context, uri);
    }
    
    public static boolean isTreeUri(final Uri uri) {
        final int sdk_INT = Build$VERSION.SDK_INT;
        final boolean b = false;
        if (sdk_INT < 21) {
            return false;
        }
        if (Build$VERSION.SDK_INT < 24) {
            final List pathSegments = uri.getPathSegments();
            boolean b2 = b;
            if (pathSegments.size() >= 2) {
                b2 = b;
                if ("tree".equals(pathSegments.get(0))) {
                    b2 = true;
                }
            }
            return b2;
        }
        return DocumentsContractApi24Impl.isTreeUri(uri);
    }
    
    public static boolean removeDocument(final ContentResolver contentResolver, final Uri uri, final Uri uri2) throws FileNotFoundException {
        if (Build$VERSION.SDK_INT >= 24) {
            return DocumentsContractApi24Impl.removeDocument(contentResolver, uri, uri2);
        }
        return Build$VERSION.SDK_INT >= 19 && DocumentsContractApi19Impl.deleteDocument(contentResolver, uri);
    }
    
    public static Uri renameDocument(final ContentResolver contentResolver, final Uri uri, final String s) throws FileNotFoundException {
        if (Build$VERSION.SDK_INT >= 21) {
            return DocumentsContractApi21Impl.renameDocument(contentResolver, uri, s);
        }
        return null;
    }
    
    public static final class DocumentCompat
    {
        public static final int FLAG_VIRTUAL_DOCUMENT = 512;
        
        private DocumentCompat() {
        }
    }
    
    private static class DocumentsContractApi19Impl
    {
        public static Uri buildDocumentUri(final String s, final String s2) {
            return DocumentsContract.buildDocumentUri(s, s2);
        }
        
        static boolean deleteDocument(final ContentResolver contentResolver, final Uri uri) throws FileNotFoundException {
            return DocumentsContract.deleteDocument(contentResolver, uri);
        }
        
        static String getDocumentId(final Uri uri) {
            return DocumentsContract.getDocumentId(uri);
        }
        
        static boolean isDocumentUri(final Context context, final Uri uri) {
            return DocumentsContract.isDocumentUri(context, uri);
        }
    }
    
    private static class DocumentsContractApi21Impl
    {
        static Uri buildChildDocumentsUri(final String s, final String s2) {
            return DocumentsContract.buildChildDocumentsUri(s, s2);
        }
        
        static Uri buildChildDocumentsUriUsingTree(final Uri uri, final String s) {
            return DocumentsContract.buildChildDocumentsUriUsingTree(uri, s);
        }
        
        static Uri buildDocumentUriUsingTree(final Uri uri, final String s) {
            return DocumentsContract.buildDocumentUriUsingTree(uri, s);
        }
        
        public static Uri buildTreeDocumentUri(final String s, final String s2) {
            return DocumentsContract.buildTreeDocumentUri(s, s2);
        }
        
        static Uri createDocument(final ContentResolver contentResolver, final Uri uri, final String s, final String s2) throws FileNotFoundException {
            return DocumentsContract.createDocument(contentResolver, uri, s, s2);
        }
        
        static String getTreeDocumentId(final Uri uri) {
            return DocumentsContract.getTreeDocumentId(uri);
        }
        
        static Uri renameDocument(final ContentResolver contentResolver, final Uri uri, final String s) throws FileNotFoundException {
            return DocumentsContract.renameDocument(contentResolver, uri, s);
        }
    }
    
    private static class DocumentsContractApi24Impl
    {
        static boolean isTreeUri(final Uri uri) {
            return DocumentsContract.isTreeUri(uri);
        }
        
        static boolean removeDocument(final ContentResolver contentResolver, final Uri uri, final Uri uri2) throws FileNotFoundException {
            return DocumentsContract.removeDocument(contentResolver, uri, uri2);
        }
    }
}

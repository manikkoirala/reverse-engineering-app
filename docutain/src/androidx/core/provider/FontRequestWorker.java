// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.provider;

import java.util.concurrent.Callable;
import java.util.concurrent.Executor;
import android.content.pm.PackageManager$NameNotFoundException;
import androidx.core.graphics.TypefaceCompat;
import android.os.CancellationSignal;
import android.content.Context;
import android.graphics.Typeface;
import androidx.collection.LruCache;
import androidx.core.util.Consumer;
import java.util.ArrayList;
import androidx.collection.SimpleArrayMap;
import java.util.concurrent.ExecutorService;

class FontRequestWorker
{
    private static final ExecutorService DEFAULT_EXECUTOR_SERVICE;
    static final Object LOCK;
    static final SimpleArrayMap<String, ArrayList<Consumer<TypefaceResult>>> PENDING_REPLIES;
    static final LruCache<String, Typeface> sTypefaceCache;
    
    static {
        sTypefaceCache = new LruCache<String, Typeface>(16);
        DEFAULT_EXECUTOR_SERVICE = RequestExecutor.createDefaultExecutor("fonts-androidx", 10, 10000);
        LOCK = new Object();
        PENDING_REPLIES = new SimpleArrayMap<String, ArrayList<Consumer<TypefaceResult>>>();
    }
    
    private FontRequestWorker() {
    }
    
    private static String createCacheId(final FontRequest fontRequest, final int i) {
        final StringBuilder sb = new StringBuilder();
        sb.append(fontRequest.getId());
        sb.append("-");
        sb.append(i);
        return sb.toString();
    }
    
    private static int getFontFamilyResultStatus(final FontsContractCompat.FontFamilyResult fontFamilyResult) {
        final int statusCode = fontFamilyResult.getStatusCode();
        final int n = -3;
        final boolean b = true;
        if (statusCode == 0) {
            final FontsContractCompat.FontInfo[] fonts = fontFamilyResult.getFonts();
            int n2 = b ? 1 : 0;
            if (fonts != null) {
                if (fonts.length == 0) {
                    n2 = (b ? 1 : 0);
                }
                else {
                    final int length = fonts.length;
                    final int n3 = 0;
                    int n4 = 0;
                    while (true) {
                        n2 = n3;
                        if (n4 >= length) {
                            break;
                        }
                        int resultCode = fonts[n4].getResultCode();
                        if (resultCode != 0) {
                            if (resultCode < 0) {
                                resultCode = n;
                            }
                            return resultCode;
                        }
                        ++n4;
                    }
                }
            }
            return n2;
        }
        if (fontFamilyResult.getStatusCode() != 1) {
            return -3;
        }
        return -2;
    }
    
    static TypefaceResult getFontSync(final String s, final Context context, final FontRequest fontRequest, final int n) {
        final LruCache<String, Typeface> sTypefaceCache = FontRequestWorker.sTypefaceCache;
        final Typeface typeface = sTypefaceCache.get(s);
        if (typeface != null) {
            return new TypefaceResult(typeface);
        }
        try {
            final FontsContractCompat.FontFamilyResult fontFamilyResult = FontProvider.getFontFamilyResult(context, fontRequest, null);
            final int fontFamilyResultStatus = getFontFamilyResultStatus(fontFamilyResult);
            if (fontFamilyResultStatus != 0) {
                return new TypefaceResult(fontFamilyResultStatus);
            }
            final Typeface fromFontInfo = TypefaceCompat.createFromFontInfo(context, null, fontFamilyResult.getFonts(), n);
            if (fromFontInfo != null) {
                sTypefaceCache.put(s, fromFontInfo);
                return new TypefaceResult(fromFontInfo);
            }
            return new TypefaceResult(-3);
        }
        catch (final PackageManager$NameNotFoundException ex) {
            return new TypefaceResult(-1);
        }
    }
    
    static Typeface requestFontAsync(final Context context, final FontRequest fontRequest, final int n, final Executor executor, final CallbackWithHandler callbackWithHandler) {
        final String cacheId = createCacheId(fontRequest, n);
        final Typeface typeface = FontRequestWorker.sTypefaceCache.get(cacheId);
        if (typeface != null) {
            callbackWithHandler.onTypefaceResult(new TypefaceResult(typeface));
            return typeface;
        }
        final Consumer<TypefaceResult> consumer = new Consumer<TypefaceResult>(callbackWithHandler) {
            final CallbackWithHandler val$callback;
            
            @Override
            public void accept(final TypefaceResult typefaceResult) {
                TypefaceResult typefaceResult2 = typefaceResult;
                if (typefaceResult == null) {
                    typefaceResult2 = new TypefaceResult(-3);
                }
                this.val$callback.onTypefaceResult(typefaceResult2);
            }
        };
        synchronized (FontRequestWorker.LOCK) {
            final SimpleArrayMap<String, ArrayList<Consumer<TypefaceResult>>> pending_REPLIES = FontRequestWorker.PENDING_REPLIES;
            final ArrayList list = pending_REPLIES.get(cacheId);
            if (list != null) {
                list.add(consumer);
                return null;
            }
            final ArrayList<Consumer<TypefaceResult>> list2 = new ArrayList<Consumer<TypefaceResult>>();
            list2.add(consumer);
            pending_REPLIES.put(cacheId, list2);
            monitorexit(FontRequestWorker.LOCK);
            final Callable<TypefaceResult> callable = new Callable<TypefaceResult>(cacheId, context, fontRequest, n) {
                final Context val$context;
                final String val$id;
                final FontRequest val$request;
                final int val$style;
                
                @Override
                public TypefaceResult call() {
                    try {
                        return FontRequestWorker.getFontSync(this.val$id, this.val$context, this.val$request, this.val$style);
                    }
                    finally {
                        return new TypefaceResult(-3);
                    }
                }
            };
            Executor default_EXECUTOR_SERVICE;
            if ((default_EXECUTOR_SERVICE = executor) == null) {
                default_EXECUTOR_SERVICE = FontRequestWorker.DEFAULT_EXECUTOR_SERVICE;
            }
            RequestExecutor.execute(default_EXECUTOR_SERVICE, (Callable<Object>)callable, (Consumer<Object>)new Consumer<TypefaceResult>(cacheId) {
                final String val$id;
                
                @Override
                public void accept(final TypefaceResult typefaceResult) {
                    synchronized (FontRequestWorker.LOCK) {
                        final ArrayList list = FontRequestWorker.PENDING_REPLIES.get(this.val$id);
                        if (list == null) {
                            return;
                        }
                        FontRequestWorker.PENDING_REPLIES.remove(this.val$id);
                        monitorexit(FontRequestWorker.LOCK);
                        for (int i = 0; i < list.size(); ++i) {
                            ((Consumer<TypefaceResult>)list.get(i)).accept(typefaceResult);
                        }
                    }
                }
            });
            return null;
        }
    }
    
    static Typeface requestFontSync(final Context context, final FontRequest fontRequest, final CallbackWithHandler callbackWithHandler, final int n, final int n2) {
        final String cacheId = createCacheId(fontRequest, n);
        final Typeface typeface = FontRequestWorker.sTypefaceCache.get(cacheId);
        if (typeface != null) {
            callbackWithHandler.onTypefaceResult(new TypefaceResult(typeface));
            return typeface;
        }
        if (n2 == -1) {
            final TypefaceResult fontSync = getFontSync(cacheId, context, fontRequest, n);
            callbackWithHandler.onTypefaceResult(fontSync);
            return fontSync.mTypeface;
        }
        final Callable<TypefaceResult> callable = new Callable<TypefaceResult>(cacheId, context, fontRequest, n) {
            final Context val$context;
            final String val$id;
            final FontRequest val$request;
            final int val$style;
            
            @Override
            public TypefaceResult call() {
                return FontRequestWorker.getFontSync(this.val$id, this.val$context, this.val$request, this.val$style);
            }
        };
        try {
            final TypefaceResult typefaceResult = RequestExecutor.submit(FontRequestWorker.DEFAULT_EXECUTOR_SERVICE, (Callable<TypefaceResult>)callable, n2);
            callbackWithHandler.onTypefaceResult(typefaceResult);
            return typefaceResult.mTypeface;
        }
        catch (final InterruptedException ex) {
            callbackWithHandler.onTypefaceResult(new TypefaceResult(-3));
            return null;
        }
    }
    
    static void resetTypefaceCache() {
        FontRequestWorker.sTypefaceCache.evictAll();
    }
    
    static final class TypefaceResult
    {
        final int mResult;
        final Typeface mTypeface;
        
        TypefaceResult(final int mResult) {
            this.mTypeface = null;
            this.mResult = mResult;
        }
        
        TypefaceResult(final Typeface mTypeface) {
            this.mTypeface = mTypeface;
            this.mResult = 0;
        }
        
        boolean isSuccess() {
            return this.mResult == 0;
        }
    }
}

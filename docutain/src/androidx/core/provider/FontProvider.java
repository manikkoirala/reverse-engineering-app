// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.provider;

import android.database.Cursor;
import android.content.ContentResolver;
import android.net.Uri;
import android.content.ContentUris;
import android.os.Build$VERSION;
import android.net.Uri$Builder;
import java.util.Collection;
import java.util.Collections;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager$NameNotFoundException;
import android.content.pm.ProviderInfo;
import android.os.CancellationSignal;
import android.content.Context;
import androidx.core.content.res.FontResourcesParserCompat;
import android.content.res.Resources;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.List;
import android.content.pm.Signature;
import java.util.Comparator;

class FontProvider
{
    private static final Comparator<byte[]> sByteArrayComparator;
    
    static {
        sByteArrayComparator = new FontProvider$$ExternalSyntheticLambda0();
    }
    
    private FontProvider() {
    }
    
    private static List<byte[]> convertToByteArrayList(final Signature[] array) {
        final ArrayList list = new ArrayList();
        for (int length = array.length, i = 0; i < length; ++i) {
            list.add(array[i].toByteArray());
        }
        return list;
    }
    
    private static boolean equalsByteArrayList(final List<byte[]> list, final List<byte[]> list2) {
        if (list.size() != list2.size()) {
            return false;
        }
        for (int i = 0; i < list.size(); ++i) {
            if (!Arrays.equals((byte[])list.get(i), (byte[])list2.get(i))) {
                return false;
            }
        }
        return true;
    }
    
    private static List<List<byte[]>> getCertificates(final FontRequest fontRequest, final Resources resources) {
        if (fontRequest.getCertificates() != null) {
            return fontRequest.getCertificates();
        }
        return FontResourcesParserCompat.readCerts(resources, fontRequest.getCertificatesArrayResId());
    }
    
    static FontsContractCompat.FontFamilyResult getFontFamilyResult(final Context context, final FontRequest fontRequest, final CancellationSignal cancellationSignal) throws PackageManager$NameNotFoundException {
        final ProviderInfo provider = getProvider(context.getPackageManager(), fontRequest, context.getResources());
        if (provider == null) {
            return FontsContractCompat.FontFamilyResult.create(1, null);
        }
        return FontsContractCompat.FontFamilyResult.create(0, query(context, fontRequest, provider.authority, cancellationSignal));
    }
    
    static ProviderInfo getProvider(final PackageManager packageManager, final FontRequest fontRequest, final Resources resources) throws PackageManager$NameNotFoundException {
        final String providerAuthority = fontRequest.getProviderAuthority();
        int i = 0;
        final ProviderInfo resolveContentProvider = packageManager.resolveContentProvider(providerAuthority, 0);
        if (resolveContentProvider == null) {
            final StringBuilder sb = new StringBuilder();
            sb.append("No package found for authority: ");
            sb.append(providerAuthority);
            throw new PackageManager$NameNotFoundException(sb.toString());
        }
        if (resolveContentProvider.packageName.equals(fontRequest.getProviderPackage())) {
            final List<byte[]> convertToByteArrayList = convertToByteArrayList(packageManager.getPackageInfo(resolveContentProvider.packageName, 64).signatures);
            Collections.sort((List<Object>)convertToByteArrayList, (Comparator<? super Object>)FontProvider.sByteArrayComparator);
            for (List<List<byte[]>> certificates = getCertificates(fontRequest, resources); i < certificates.size(); ++i) {
                final ArrayList list = new ArrayList<byte[]>((Collection<? extends T>)certificates.get(i));
                Collections.sort((List<E>)list, (Comparator<? super E>)FontProvider.sByteArrayComparator);
                if (equalsByteArrayList(convertToByteArrayList, (List<byte[]>)list)) {
                    return resolveContentProvider;
                }
            }
            return null;
        }
        final StringBuilder sb2 = new StringBuilder();
        sb2.append("Found content provider ");
        sb2.append(providerAuthority);
        sb2.append(", but package was not ");
        sb2.append(fontRequest.getProviderPackage());
        throw new PackageManager$NameNotFoundException(sb2.toString());
    }
    
    static FontsContractCompat.FontInfo[] query(final Context context, final FontRequest fontRequest, String s, final CancellationSignal cancellationSignal) {
        final ArrayList list = new ArrayList();
        final Uri build = new Uri$Builder().scheme("content").authority(s).build();
        final Uri build2 = new Uri$Builder().scheme("content").authority(s).appendPath("file").build();
        final String s2 = s = null;
        try {
            final String[] array = { "_id", "file_id", "font_ttc_index", "font_variation_settings", "font_weight", "font_italic", "result_code" };
            s = s2;
            final ContentResolver contentResolver = context.getContentResolver();
            s = s2;
            Object o;
            if (Build$VERSION.SDK_INT > 16) {
                s = s2;
                o = Api16Impl.query(contentResolver, build, array, "query = ?", new String[] { fontRequest.getQuery() }, null, cancellationSignal);
            }
            else {
                s = s2;
                o = contentResolver.query(build, array, "query = ?", new String[] { fontRequest.getQuery() }, (String)null);
            }
            ArrayList<FontsContractCompat.FontInfo> list2 = list;
            if (o != null) {
                list2 = list;
                s = (String)o;
                if (((Cursor)o).getCount() > 0) {
                    s = (String)o;
                    final int columnIndex = ((Cursor)o).getColumnIndex("result_code");
                    s = (String)o;
                    s = (String)o;
                    final ArrayList<FontsContractCompat.FontInfo> list3 = new ArrayList<FontsContractCompat.FontInfo>();
                    s = (String)o;
                    final int columnIndex2 = ((Cursor)o).getColumnIndex("_id");
                    s = (String)o;
                    final int columnIndex3 = ((Cursor)o).getColumnIndex("file_id");
                    s = (String)o;
                    final int columnIndex4 = ((Cursor)o).getColumnIndex("font_ttc_index");
                    s = (String)o;
                    final int columnIndex5 = ((Cursor)o).getColumnIndex("font_weight");
                    s = (String)o;
                    final int columnIndex6 = ((Cursor)o).getColumnIndex("font_italic");
                    while (true) {
                        s = (String)o;
                        if (!((Cursor)o).moveToNext()) {
                            break;
                        }
                        int int1;
                        if (columnIndex != -1) {
                            s = (String)o;
                            int1 = ((Cursor)o).getInt(columnIndex);
                        }
                        else {
                            int1 = 0;
                        }
                        int int2;
                        if (columnIndex4 != -1) {
                            s = (String)o;
                            int2 = ((Cursor)o).getInt(columnIndex4);
                        }
                        else {
                            int2 = 0;
                        }
                        Uri uri;
                        if (columnIndex3 == -1) {
                            s = (String)o;
                            uri = ContentUris.withAppendedId(build, ((Cursor)o).getLong(columnIndex2));
                        }
                        else {
                            s = (String)o;
                            uri = ContentUris.withAppendedId(build2, ((Cursor)o).getLong(columnIndex3));
                        }
                        int int3;
                        if (columnIndex5 != -1) {
                            s = (String)o;
                            int3 = ((Cursor)o).getInt(columnIndex5);
                        }
                        else {
                            int3 = 400;
                        }
                        boolean b = false;
                        Label_0447: {
                            if (columnIndex6 != -1) {
                                s = (String)o;
                                if (((Cursor)o).getInt(columnIndex6) == 1) {
                                    b = true;
                                    break Label_0447;
                                }
                            }
                            b = false;
                        }
                        s = (String)o;
                        list3.add(FontsContractCompat.FontInfo.create(uri, int2, int3, b, int1));
                    }
                    list2 = list3;
                }
            }
            if (o != null) {
                ((Cursor)o).close();
            }
            return list2.toArray(new FontsContractCompat.FontInfo[0]);
        }
        finally {
            if (s != null) {
                ((Cursor)s).close();
            }
        }
    }
    
    static class Api16Impl
    {
        private Api16Impl() {
        }
        
        static Cursor query(final ContentResolver contentResolver, final Uri uri, final String[] array, final String s, final String[] array2, final String s2, final Object o) {
            return contentResolver.query(uri, array, s, array2, s2, (CancellationSignal)o);
        }
    }
}

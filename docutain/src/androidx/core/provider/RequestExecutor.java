// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.provider;

import java.util.concurrent.RejectedExecutionException;
import androidx.core.util.Preconditions;
import android.os.Process;
import java.util.concurrent.Future;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.ExecutorService;
import androidx.core.util.Consumer;
import java.util.concurrent.Callable;
import java.util.concurrent.Executor;
import android.os.Handler;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.ThreadPoolExecutor;

class RequestExecutor
{
    private RequestExecutor() {
    }
    
    static ThreadPoolExecutor createDefaultExecutor(final String s, final int n, final int n2) {
        final ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(0, 1, n2, TimeUnit.MILLISECONDS, new LinkedBlockingDeque<Runnable>(), new DefaultThreadFactory(s, n));
        threadPoolExecutor.allowCoreThreadTimeOut(true);
        return threadPoolExecutor;
    }
    
    static Executor createHandlerExecutor(final Handler handler) {
        return new HandlerExecutor(handler);
    }
    
    static <T> void execute(final Executor executor, final Callable<T> callable, final Consumer<T> consumer) {
        executor.execute(new ReplyRunnable<Object>(CalleeHandler.create(), callable, consumer));
    }
    
    static <T> T submit(final ExecutorService executorService, final Callable<T> callable, final int n) throws InterruptedException {
        final Future<T> submit = executorService.submit(callable);
        final long n2 = n;
        try {
            return submit.get(n2, TimeUnit.MILLISECONDS);
        }
        catch (final TimeoutException ex) {
            throw new InterruptedException("timeout");
        }
        catch (final InterruptedException ex2) {
            throw ex2;
        }
        catch (final ExecutionException cause) {
            throw new RuntimeException(cause);
        }
    }
    
    private static class DefaultThreadFactory implements ThreadFactory
    {
        private int mPriority;
        private String mThreadName;
        
        DefaultThreadFactory(final String mThreadName, final int mPriority) {
            this.mThreadName = mThreadName;
            this.mPriority = mPriority;
        }
        
        @Override
        public Thread newThread(final Runnable runnable) {
            return new ProcessPriorityThread(runnable, this.mThreadName, this.mPriority);
        }
        
        private static class ProcessPriorityThread extends Thread
        {
            private final int mPriority;
            
            ProcessPriorityThread(final Runnable target, final String name, final int mPriority) {
                super(target, name);
                this.mPriority = mPriority;
            }
            
            @Override
            public void run() {
                Process.setThreadPriority(this.mPriority);
                super.run();
            }
        }
    }
    
    private static class HandlerExecutor implements Executor
    {
        private final Handler mHandler;
        
        HandlerExecutor(final Handler handler) {
            this.mHandler = Preconditions.checkNotNull(handler);
        }
        
        @Override
        public void execute(final Runnable runnable) {
            if (this.mHandler.post((Runnable)Preconditions.checkNotNull(runnable))) {
                return;
            }
            final StringBuilder sb = new StringBuilder();
            sb.append(this.mHandler);
            sb.append(" is shutting down");
            throw new RejectedExecutionException(sb.toString());
        }
    }
    
    private static class ReplyRunnable<T> implements Runnable
    {
        private Callable<T> mCallable;
        private Consumer<T> mConsumer;
        private Handler mHandler;
        
        ReplyRunnable(final Handler mHandler, final Callable<T> mCallable, final Consumer<T> mConsumer) {
            this.mCallable = mCallable;
            this.mConsumer = mConsumer;
            this.mHandler = mHandler;
        }
        
        @Override
        public void run() {
            Object call;
            try {
                call = this.mCallable.call();
            }
            catch (final Exception ex) {
                call = null;
            }
            this.mHandler.post((Runnable)new Runnable(this, this.mConsumer, call) {
                final ReplyRunnable this$0;
                final Consumer val$consumer;
                final Object val$result;
                
                @Override
                public void run() {
                    this.val$consumer.accept(this.val$result);
                }
            });
        }
    }
}

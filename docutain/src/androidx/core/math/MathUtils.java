// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.math;

public class MathUtils
{
    private MathUtils() {
    }
    
    public static int addExact(int n, int n2) {
        final int n3 = n + n2;
        final int n4 = 1;
        final boolean b = n >= 0;
        if (n2 >= 0) {
            n2 = 1;
        }
        else {
            n2 = 0;
        }
        if ((b ? 1 : 0) == n2) {
            if (n >= 0) {
                n = 1;
            }
            else {
                n = 0;
            }
            if (n3 >= 0) {
                n2 = n4;
            }
            else {
                n2 = 0;
            }
            if (n != n2) {
                throw new ArithmeticException("integer overflow");
            }
        }
        return n3;
    }
    
    public static long addExact(final long n, final long n2) {
        final long n3 = n + n2;
        final boolean b = true;
        final long n4 = lcmp(n, 0L);
        if (n4 >= 0 == n2 >= 0L && n4 >= 0 != (n3 >= 0L && b)) {
            throw new ArithmeticException("integer overflow");
        }
        return n3;
    }
    
    public static double clamp(final double n, final double n2, final double n3) {
        if (n < n2) {
            return n2;
        }
        if (n > n3) {
            return n3;
        }
        return n;
    }
    
    public static float clamp(final float n, final float n2, final float n3) {
        if (n < n2) {
            return n2;
        }
        if (n > n3) {
            return n3;
        }
        return n;
    }
    
    public static int clamp(final int n, final int n2, final int n3) {
        if (n < n2) {
            return n2;
        }
        if (n > n3) {
            return n3;
        }
        return n;
    }
    
    public static long clamp(final long n, final long n2, final long n3) {
        if (n < n2) {
            return n2;
        }
        if (n > n3) {
            return n3;
        }
        return n;
    }
    
    public static int decrementExact(final int n) {
        if (n != Integer.MIN_VALUE) {
            return n - 1;
        }
        throw new ArithmeticException("integer overflow");
    }
    
    public static long decrementExact(final long n) {
        if (n != Long.MIN_VALUE) {
            return n - 1L;
        }
        throw new ArithmeticException("integer overflow");
    }
    
    public static int incrementExact(final int n) {
        if (n != Integer.MAX_VALUE) {
            return n + 1;
        }
        throw new ArithmeticException("integer overflow");
    }
    
    public static long incrementExact(final long n) {
        if (n != Long.MAX_VALUE) {
            return n + 1L;
        }
        throw new ArithmeticException("integer overflow");
    }
    
    public static int multiplyExact(final int n, final int n2) {
        final int n3 = n * n2;
        if (n != 0 && n2 != 0 && (n3 / n != n2 || n3 / n2 != n)) {
            throw new ArithmeticException("integer overflow");
        }
        return n3;
    }
    
    public static long multiplyExact(final long n, final long n2) {
        final long n3 = n * n2;
        if (n != 0L && n2 != 0L && (n3 / n != n2 || n3 / n2 != n)) {
            throw new ArithmeticException("integer overflow");
        }
        return n3;
    }
    
    public static int negateExact(final int n) {
        if (n != Integer.MIN_VALUE) {
            return -n;
        }
        throw new ArithmeticException("integer overflow");
    }
    
    public static long negateExact(final long n) {
        if (n != Long.MIN_VALUE) {
            return -n;
        }
        throw new ArithmeticException("integer overflow");
    }
    
    public static int subtractExact(int n, int n2) {
        final int n3 = n - n2;
        final int n4 = 1;
        final boolean b = n < 0;
        if (n2 < 0) {
            n2 = 1;
        }
        else {
            n2 = 0;
        }
        if ((b ? 1 : 0) != n2) {
            if (n < 0) {
                n = 1;
            }
            else {
                n = 0;
            }
            if (n3 < 0) {
                n2 = n4;
            }
            else {
                n2 = 0;
            }
            if (n != n2) {
                throw new ArithmeticException("integer overflow");
            }
        }
        return n3;
    }
    
    public static long subtractExact(final long n, final long n2) {
        final long n3 = n - n2;
        final boolean b = true;
        final long n4 = lcmp(n, 0L);
        if (n4 < 0 != n2 < 0L && n4 < 0 != (n3 < 0L && b)) {
            throw new ArithmeticException("integer overflow");
        }
        return n3;
    }
    
    public static int toIntExact(final long n) {
        if (n <= 2147483647L && n >= -2147483648L) {
            return (int)n;
        }
        throw new ArithmeticException("integer overflow");
    }
}

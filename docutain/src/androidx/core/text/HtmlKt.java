// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.text;

import android.text.Spanned;
import android.text.Html$TagHandler;
import android.text.Html$ImageGetter;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000 \n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u001a/\u0010\u0000\u001a\u00020\u0001*\u00020\u00022\b\b\u0002\u0010\u0003\u001a\u00020\u00042\n\b\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u00062\n\b\u0002\u0010\u0007\u001a\u0004\u0018\u00010\bH\u0086\b\u001a\u0017\u0010\t\u001a\u00020\u0002*\u00020\u00012\b\b\u0002\u0010\n\u001a\u00020\u0004H\u0086\b¨\u0006\u000b" }, d2 = { "parseAsHtml", "Landroid/text/Spanned;", "", "flags", "", "imageGetter", "Landroid/text/Html$ImageGetter;", "tagHandler", "Landroid/text/Html$TagHandler;", "toHtml", "option", "core-ktx_release" }, k = 2, mv = { 1, 8, 0 }, xi = 48)
public final class HtmlKt
{
    public static final Spanned parseAsHtml(final String s, final int n, final Html$ImageGetter html$ImageGetter, final Html$TagHandler html$TagHandler) {
        return HtmlCompat.fromHtml(s, n, html$ImageGetter, html$TagHandler);
    }
    
    public static final String toHtml(final Spanned spanned, final int n) {
        return HtmlCompat.toHtml(spanned, n);
    }
}

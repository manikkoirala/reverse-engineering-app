// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.text;

import android.icu.util.ULocale;
import java.lang.reflect.InvocationTargetException;
import java.util.Locale;
import android.util.Log;
import android.os.Build$VERSION;
import java.lang.reflect.Method;

public final class ICUCompat
{
    private static final String TAG = "ICUCompat";
    private static Method sAddLikelySubtagsMethod;
    private static Method sGetScriptMethod;
    
    static {
        if (Build$VERSION.SDK_INT < 21) {
            try {
                final Class<?> forName = Class.forName("libcore.icu.ICU");
                ICUCompat.sGetScriptMethod = forName.getMethod("getScript", String.class);
                ICUCompat.sAddLikelySubtagsMethod = forName.getMethod("addLikelySubtags", String.class);
            }
            catch (final Exception ex) {
                ICUCompat.sGetScriptMethod = null;
                ICUCompat.sAddLikelySubtagsMethod = null;
                Log.w("ICUCompat", (Throwable)ex);
            }
        }
        else if (Build$VERSION.SDK_INT < 24) {
            try {
                ICUCompat.sAddLikelySubtagsMethod = Class.forName("libcore.icu.ICU").getMethod("addLikelySubtags", Locale.class);
            }
            catch (final Exception cause) {
                throw new IllegalStateException(cause);
            }
        }
    }
    
    private ICUCompat() {
    }
    
    private static String addLikelySubtagsBelowApi21(Locale string) {
        string = (Locale)string.toString();
        try {
            final Method sAddLikelySubtagsMethod = ICUCompat.sAddLikelySubtagsMethod;
            if (sAddLikelySubtagsMethod != null) {
                return (String)sAddLikelySubtagsMethod.invoke(null, string);
            }
        }
        catch (final InvocationTargetException ex) {
            Log.w("ICUCompat", (Throwable)ex);
        }
        catch (final IllegalAccessException ex2) {
            Log.w("ICUCompat", (Throwable)ex2);
        }
        return (String)string;
    }
    
    private static String getScriptBelowApi21(String s) {
        try {
            final Method sGetScriptMethod = ICUCompat.sGetScriptMethod;
            if (sGetScriptMethod != null) {
                s = (String)sGetScriptMethod.invoke(null, s);
                return s;
            }
        }
        catch (final InvocationTargetException ex) {
            Log.w("ICUCompat", (Throwable)ex);
        }
        catch (final IllegalAccessException ex2) {
            Log.w("ICUCompat", (Throwable)ex2);
        }
        return null;
    }
    
    public static String maximizeAndGetScript(final Locale locale) {
        if (Build$VERSION.SDK_INT >= 24) {
            return Api24Impl.getScript(Api24Impl.addLikelySubtags(Api24Impl.forLocale(locale)));
        }
        if (Build$VERSION.SDK_INT >= 21) {
            try {
                return Api21Impl.getScript((Locale)ICUCompat.sAddLikelySubtagsMethod.invoke(null, locale));
            }
            catch (final IllegalAccessException ex) {
                Log.w("ICUCompat", (Throwable)ex);
            }
            catch (final InvocationTargetException ex2) {
                Log.w("ICUCompat", (Throwable)ex2);
            }
            return Api21Impl.getScript(locale);
        }
        final String addLikelySubtagsBelowApi21 = addLikelySubtagsBelowApi21(locale);
        if (addLikelySubtagsBelowApi21 != null) {
            return getScriptBelowApi21(addLikelySubtagsBelowApi21);
        }
        return null;
    }
    
    static class Api21Impl
    {
        private Api21Impl() {
        }
        
        static String getScript(final Locale locale) {
            return locale.getScript();
        }
    }
    
    static class Api24Impl
    {
        private Api24Impl() {
        }
        
        static ULocale addLikelySubtags(final Object o) {
            return ULocale.addLikelySubtags((ULocale)o);
        }
        
        static ULocale forLocale(final Locale locale) {
            return ULocale.forLocale(locale);
        }
        
        static String getScript(final Object o) {
            return ((ULocale)o).getScript();
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.text.method;

import android.text.Layout;
import android.text.method.Touch;
import android.text.Selection;
import androidx.core.os.BuildCompat;
import android.view.MotionEvent;
import android.text.Spannable;
import android.widget.TextView;
import android.text.method.LinkMovementMethod;

public class LinkMovementMethodCompat extends LinkMovementMethod
{
    private static LinkMovementMethodCompat sInstance;
    
    private LinkMovementMethodCompat() {
    }
    
    public static LinkMovementMethodCompat getInstance() {
        if (LinkMovementMethodCompat.sInstance == null) {
            LinkMovementMethodCompat.sInstance = new LinkMovementMethodCompat();
        }
        return LinkMovementMethodCompat.sInstance;
    }
    
    public boolean onTouchEvent(final TextView textView, final Spannable spannable, final MotionEvent motionEvent) {
        if (!BuildCompat.isAtLeastV()) {
            final int action = motionEvent.getAction();
            final boolean b = true;
            if (action == 1 || action == 0) {
                final int n = (int)motionEvent.getX();
                final int n2 = (int)motionEvent.getY();
                final int totalPaddingLeft = textView.getTotalPaddingLeft();
                final int totalPaddingTop = textView.getTotalPaddingTop();
                final int scrollX = textView.getScrollX();
                final int n3 = n2 - totalPaddingTop + textView.getScrollY();
                final Layout layout = textView.getLayout();
                int n4 = b ? 1 : 0;
                if (n3 >= 0) {
                    if (n3 > layout.getHeight()) {
                        n4 = (b ? 1 : 0);
                    }
                    else {
                        final int lineForVertical = layout.getLineForVertical(n3);
                        final float n5 = (float)(n - totalPaddingLeft + scrollX);
                        n4 = (b ? 1 : 0);
                        if (n5 >= layout.getLineLeft(lineForVertical)) {
                            if (n5 > layout.getLineRight(lineForVertical)) {
                                n4 = (b ? 1 : 0);
                            }
                            else {
                                n4 = 0;
                            }
                        }
                    }
                }
                if (n4 != 0) {
                    Selection.removeSelection(spannable);
                    return Touch.onTouchEvent(textView, spannable, motionEvent);
                }
            }
        }
        return super.onTouchEvent(textView, spannable, motionEvent);
    }
}

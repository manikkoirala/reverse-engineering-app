// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.text.util;

import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;
import android.icu.util.MeasureUnit;
import android.icu.number.NumberFormatter;
import android.icu.number.UnlocalizedNumberFormatter;
import android.icu.text.DateFormat$HourCycle;
import android.icu.text.DateTimePatternGenerator;
import java.util.Arrays;
import android.os.Build$VERSION;
import android.text.format.DateFormat;
import java.util.Calendar;
import java.util.Locale;

public final class LocalePreferences
{
    private static final String TAG = "LocalePreferences";
    private static final String[] WEATHER_FAHRENHEIT_COUNTRIES;
    
    static {
        WEATHER_FAHRENHEIT_COUNTRIES = new String[] { "BS", "BZ", "KY", "PR", "PW", "US" };
    }
    
    private LocalePreferences() {
    }
    
    private static String getBaseFirstDayOfWeek(final Locale aLocale) {
        return getStringOfFirstDayOfWeek(Calendar.getInstance(aLocale).getFirstDayOfWeek());
    }
    
    private static String getBaseHourCycle(final Locale locale) {
        String s;
        if (DateFormat.getBestDateTimePattern(locale, "jm").contains("H")) {
            s = "h23";
        }
        else {
            s = "h12";
        }
        return s;
    }
    
    public static String getCalendarType() {
        return getCalendarType(true);
    }
    
    public static String getCalendarType(final Locale locale) {
        return getCalendarType(locale, true);
    }
    
    public static String getCalendarType(final Locale locale, final boolean b) {
        final String s = "";
        final String unicodeLocaleType = getUnicodeLocaleType("ca", "", locale, b);
        if (unicodeLocaleType != null) {
            return unicodeLocaleType;
        }
        if (Build$VERSION.SDK_INT >= 24) {
            return Api24Impl.getCalendarType(locale);
        }
        String s2 = s;
        if (b) {
            s2 = "gregorian";
        }
        return s2;
    }
    
    public static String getCalendarType(final boolean b) {
        Locale locale;
        if (Build$VERSION.SDK_INT >= 24) {
            locale = Api24Impl.getDefaultLocale();
        }
        else {
            locale = getDefaultLocale();
        }
        return getCalendarType(locale, b);
    }
    
    private static Locale getDefaultLocale() {
        return Locale.getDefault();
    }
    
    public static String getFirstDayOfWeek() {
        return getFirstDayOfWeek(true);
    }
    
    public static String getFirstDayOfWeek(final Locale locale) {
        return getFirstDayOfWeek(locale, true);
    }
    
    public static String getFirstDayOfWeek(final Locale locale, final boolean b) {
        final String unicodeLocaleType = getUnicodeLocaleType("fw", "", locale, b);
        String baseFirstDayOfWeek;
        if (unicodeLocaleType != null) {
            baseFirstDayOfWeek = unicodeLocaleType;
        }
        else {
            baseFirstDayOfWeek = getBaseFirstDayOfWeek(locale);
        }
        return baseFirstDayOfWeek;
    }
    
    public static String getFirstDayOfWeek(final boolean b) {
        Locale locale;
        if (Build$VERSION.SDK_INT >= 24) {
            locale = Api24Impl.getDefaultLocale();
        }
        else {
            locale = getDefaultLocale();
        }
        return getFirstDayOfWeek(locale, b);
    }
    
    public static String getHourCycle() {
        return getHourCycle(true);
    }
    
    public static String getHourCycle(final Locale locale) {
        return getHourCycle(locale, true);
    }
    
    public static String getHourCycle(final Locale locale, final boolean b) {
        final String unicodeLocaleType = getUnicodeLocaleType("hc", "", locale, b);
        if (unicodeLocaleType != null) {
            return unicodeLocaleType;
        }
        if (Build$VERSION.SDK_INT >= 33) {
            return Api33Impl.getHourCycle(locale);
        }
        return getBaseHourCycle(locale);
    }
    
    public static String getHourCycle(final boolean b) {
        Locale locale;
        if (Build$VERSION.SDK_INT >= 24) {
            locale = Api24Impl.getDefaultLocale();
        }
        else {
            locale = getDefaultLocale();
        }
        return getHourCycle(locale, b);
    }
    
    private static String getStringOfFirstDayOfWeek(final int n) {
        String s;
        if (n >= 1 && n <= 7) {
            s = (new String[] { "sun", "mon", "tue", "wed", "thu", "fri", "sat" })[n - 1];
        }
        else {
            s = "";
        }
        return s;
    }
    
    private static String getTemperatureHardCoded(final Locale locale) {
        String s;
        if (Arrays.binarySearch(LocalePreferences.WEATHER_FAHRENHEIT_COUNTRIES, locale.getCountry()) >= 0) {
            s = "fahrenhe";
        }
        else {
            s = "celsius";
        }
        return s;
    }
    
    public static String getTemperatureUnit() {
        return getTemperatureUnit(true);
    }
    
    public static String getTemperatureUnit(final Locale locale) {
        return getTemperatureUnit(locale, true);
    }
    
    public static String getTemperatureUnit(final Locale locale, final boolean b) {
        final String unicodeLocaleType = getUnicodeLocaleType("mu", "", locale, b);
        if (unicodeLocaleType != null) {
            return unicodeLocaleType;
        }
        if (Build$VERSION.SDK_INT >= 33) {
            return Api33Impl.getResolvedTemperatureUnit(locale);
        }
        return getTemperatureHardCoded(locale);
    }
    
    public static String getTemperatureUnit(final boolean b) {
        Locale locale;
        if (Build$VERSION.SDK_INT >= 24) {
            locale = Api24Impl.getDefaultLocale();
        }
        else {
            locale = getDefaultLocale();
        }
        return getTemperatureUnit(locale, b);
    }
    
    private static String getUnicodeLocaleType(String unicodeLocaleType, final String s, final Locale locale, final boolean b) {
        unicodeLocaleType = locale.getUnicodeLocaleType(unicodeLocaleType);
        if (unicodeLocaleType != null) {
            return unicodeLocaleType;
        }
        if (!b) {
            return s;
        }
        return null;
    }
    
    private static class Api24Impl
    {
        static String getCalendarType(final Locale locale) {
            return android.icu.util.Calendar.getInstance(locale).getType();
        }
        
        static Locale getDefaultLocale() {
            return Locale.getDefault(Locale.Category.FORMAT);
        }
    }
    
    private static class Api33Impl
    {
        static String getHourCycle(final Locale locale) {
            return getHourCycleType(DateTimePatternGenerator.getInstance(locale).getDefaultHourCycle());
        }
        
        private static String getHourCycleType(final DateFormat$HourCycle dateFormat$HourCycle) {
            final int n = LocalePreferences$1.$SwitchMap$android$icu$text$DateFormat$HourCycle[dateFormat$HourCycle.ordinal()];
            if (n == 1) {
                return "h11";
            }
            if (n == 2) {
                return "h12";
            }
            if (n == 3) {
                return "h23";
            }
            if (n != 4) {
                return "";
            }
            return "h24";
        }
        
        static String getResolvedTemperatureUnit(final Locale locale) {
            final String identifier = ((UnlocalizedNumberFormatter)((UnlocalizedNumberFormatter)NumberFormatter.with().usage("weather")).unit(MeasureUnit.CELSIUS)).locale(locale).format(1L).getOutputUnit().getIdentifier();
            if (identifier.startsWith("fahrenhe")) {
                return "fahrenhe";
            }
            return identifier;
        }
    }
    
    public static class CalendarType
    {
        public static final String CHINESE = "chinese";
        public static final String DANGI = "dangi";
        public static final String DEFAULT = "";
        public static final String GREGORIAN = "gregorian";
        public static final String HEBREW = "hebrew";
        public static final String INDIAN = "indian";
        public static final String ISLAMIC = "islamic";
        public static final String ISLAMIC_CIVIL = "islamic-civil";
        public static final String ISLAMIC_RGSA = "islamic-rgsa";
        public static final String ISLAMIC_TBLA = "islamic-tbla";
        public static final String ISLAMIC_UMALQURA = "islamic-umalqura";
        public static final String PERSIAN = "persian";
        private static final String U_EXTENSION_TAG = "ca";
        
        private CalendarType() {
        }
        
        @Retention(RetentionPolicy.SOURCE)
        public @interface CalendarTypes {
        }
    }
    
    public static class FirstDayOfWeek
    {
        public static final String DEFAULT = "";
        public static final String FRIDAY = "fri";
        public static final String MONDAY = "mon";
        public static final String SATURDAY = "sat";
        public static final String SUNDAY = "sun";
        public static final String THURSDAY = "thu";
        public static final String TUESDAY = "tue";
        private static final String U_EXTENSION_TAG = "fw";
        public static final String WEDNESDAY = "wed";
        
        private FirstDayOfWeek() {
        }
        
        @Retention(RetentionPolicy.SOURCE)
        public @interface Days {
        }
    }
    
    public static class HourCycle
    {
        public static final String DEFAULT = "";
        public static final String H11 = "h11";
        public static final String H12 = "h12";
        public static final String H23 = "h23";
        public static final String H24 = "h24";
        private static final String U_EXTENSION_TAG = "hc";
        
        private HourCycle() {
        }
        
        @Retention(RetentionPolicy.SOURCE)
        public @interface HourCycleTypes {
        }
    }
    
    public static class TemperatureUnit
    {
        public static final String CELSIUS = "celsius";
        public static final String DEFAULT = "";
        public static final String FAHRENHEIT = "fahrenhe";
        public static final String KELVIN = "kelvin";
        private static final String U_EXTENSION_TAG = "mu";
        
        private TemperatureUnit() {
        }
        
        @Retention(RetentionPolicy.SOURCE)
        public @interface TemperatureUnits {
        }
    }
}

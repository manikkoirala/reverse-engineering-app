// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.text;

import android.text.style.UnderlineSpan;
import android.text.style.SuperscriptSpan;
import android.text.style.SubscriptSpan;
import android.text.style.StrikethroughSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.ForegroundColorSpan;
import android.text.SpannedString;
import android.text.style.StyleSpan;
import android.text.style.BackgroundColorSpan;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import android.text.SpannableStringBuilder;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000:\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0011\n\u0002\b\u0004\n\u0002\u0010\u0007\n\u0002\b\u0005\u001a\"\u0010\u0000\u001a\u00020\u00012\u0017\u0010\u0002\u001a\u0013\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u0003¢\u0006\u0002\b\u0006H\u0086\b\u001a0\u0010\u0007\u001a\u00020\u0004*\u00020\u00042\b\b\u0001\u0010\b\u001a\u00020\t2\u0017\u0010\u0002\u001a\u0013\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u0003¢\u0006\u0002\b\u0006H\u0086\b\u001a&\u0010\n\u001a\u00020\u0004*\u00020\u00042\u0017\u0010\u0002\u001a\u0013\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u0003¢\u0006\u0002\b\u0006H\u0086\b\u001a0\u0010\b\u001a\u00020\u0004*\u00020\u00042\b\b\u0001\u0010\b\u001a\u00020\t2\u0017\u0010\u0002\u001a\u0013\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u0003¢\u0006\u0002\b\u0006H\u0086\b\u001a.\u0010\u000b\u001a\u00020\u0004*\u00020\u00042\u0006\u0010\f\u001a\u00020\r2\u0017\u0010\u0002\u001a\u0013\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u0003¢\u0006\u0002\b\u0006H\u0086\b\u001a?\u0010\u000b\u001a\u00020\u0004*\u00020\u00042\u0012\u0010\u000e\u001a\n\u0012\u0006\b\u0001\u0012\u00020\r0\u000f\"\u00020\r2\u0017\u0010\u0002\u001a\u0013\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u0003¢\u0006\u0002\b\u0006H\u0086\b¢\u0006\u0002\u0010\u0010\u001a&\u0010\u0011\u001a\u00020\u0004*\u00020\u00042\u0017\u0010\u0002\u001a\u0013\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u0003¢\u0006\u0002\b\u0006H\u0086\b\u001a.\u0010\u0012\u001a\u00020\u0004*\u00020\u00042\u0006\u0010\u0013\u001a\u00020\u00142\u0017\u0010\u0002\u001a\u0013\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u0003¢\u0006\u0002\b\u0006H\u0086\b\u001a&\u0010\u0015\u001a\u00020\u0004*\u00020\u00042\u0017\u0010\u0002\u001a\u0013\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u0003¢\u0006\u0002\b\u0006H\u0086\b\u001a&\u0010\u0016\u001a\u00020\u0004*\u00020\u00042\u0017\u0010\u0002\u001a\u0013\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u0003¢\u0006\u0002\b\u0006H\u0086\b\u001a&\u0010\u0017\u001a\u00020\u0004*\u00020\u00042\u0017\u0010\u0002\u001a\u0013\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u0003¢\u0006\u0002\b\u0006H\u0086\b\u001a&\u0010\u0018\u001a\u00020\u0004*\u00020\u00042\u0017\u0010\u0002\u001a\u0013\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u0003¢\u0006\u0002\b\u0006H\u0086\b¨\u0006\u0019" }, d2 = { "buildSpannedString", "Landroid/text/SpannedString;", "builderAction", "Lkotlin/Function1;", "Landroid/text/SpannableStringBuilder;", "", "Lkotlin/ExtensionFunctionType;", "backgroundColor", "color", "", "bold", "inSpans", "span", "", "spans", "", "(Landroid/text/SpannableStringBuilder;[Ljava/lang/Object;Lkotlin/jvm/functions/Function1;)Landroid/text/SpannableStringBuilder;", "italic", "scale", "proportion", "", "strikeThrough", "subscript", "superscript", "underline", "core-ktx_release" }, k = 2, mv = { 1, 8, 0 }, xi = 48)
public final class SpannableStringBuilderKt
{
    public static final SpannableStringBuilder backgroundColor(final SpannableStringBuilder spannableStringBuilder, int length, final Function1<? super SpannableStringBuilder, Unit> function1) {
        final BackgroundColorSpan backgroundColorSpan = new BackgroundColorSpan(length);
        length = spannableStringBuilder.length();
        function1.invoke((Object)spannableStringBuilder);
        spannableStringBuilder.setSpan((Object)backgroundColorSpan, length, spannableStringBuilder.length(), 17);
        return spannableStringBuilder;
    }
    
    public static final SpannableStringBuilder bold(final SpannableStringBuilder spannableStringBuilder, final Function1<? super SpannableStringBuilder, Unit> function1) {
        final StyleSpan styleSpan = new StyleSpan(1);
        final int length = spannableStringBuilder.length();
        function1.invoke((Object)spannableStringBuilder);
        spannableStringBuilder.setSpan((Object)styleSpan, length, spannableStringBuilder.length(), 17);
        return spannableStringBuilder;
    }
    
    public static final SpannedString buildSpannedString(final Function1<? super SpannableStringBuilder, Unit> function1) {
        final SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder();
        function1.invoke((Object)spannableStringBuilder);
        return new SpannedString((CharSequence)spannableStringBuilder);
    }
    
    public static final SpannableStringBuilder color(final SpannableStringBuilder spannableStringBuilder, int length, final Function1<? super SpannableStringBuilder, Unit> function1) {
        final ForegroundColorSpan foregroundColorSpan = new ForegroundColorSpan(length);
        length = spannableStringBuilder.length();
        function1.invoke((Object)spannableStringBuilder);
        spannableStringBuilder.setSpan((Object)foregroundColorSpan, length, spannableStringBuilder.length(), 17);
        return spannableStringBuilder;
    }
    
    public static final SpannableStringBuilder inSpans(final SpannableStringBuilder spannableStringBuilder, final Object o, final Function1<? super SpannableStringBuilder, Unit> function1) {
        final int length = spannableStringBuilder.length();
        function1.invoke((Object)spannableStringBuilder);
        spannableStringBuilder.setSpan(o, length, spannableStringBuilder.length(), 17);
        return spannableStringBuilder;
    }
    
    public static final SpannableStringBuilder inSpans(final SpannableStringBuilder spannableStringBuilder, final Object[] array, final Function1<? super SpannableStringBuilder, Unit> function1) {
        final int length = spannableStringBuilder.length();
        function1.invoke((Object)spannableStringBuilder);
        for (int length2 = array.length, i = 0; i < length2; ++i) {
            spannableStringBuilder.setSpan(array[i], length, spannableStringBuilder.length(), 17);
        }
        return spannableStringBuilder;
    }
    
    public static final SpannableStringBuilder italic(final SpannableStringBuilder spannableStringBuilder, final Function1<? super SpannableStringBuilder, Unit> function1) {
        final StyleSpan styleSpan = new StyleSpan(2);
        final int length = spannableStringBuilder.length();
        function1.invoke((Object)spannableStringBuilder);
        spannableStringBuilder.setSpan((Object)styleSpan, length, spannableStringBuilder.length(), 17);
        return spannableStringBuilder;
    }
    
    public static final SpannableStringBuilder scale(final SpannableStringBuilder spannableStringBuilder, final float n, final Function1<? super SpannableStringBuilder, Unit> function1) {
        final RelativeSizeSpan relativeSizeSpan = new RelativeSizeSpan(n);
        final int length = spannableStringBuilder.length();
        function1.invoke((Object)spannableStringBuilder);
        spannableStringBuilder.setSpan((Object)relativeSizeSpan, length, spannableStringBuilder.length(), 17);
        return spannableStringBuilder;
    }
    
    public static final SpannableStringBuilder strikeThrough(final SpannableStringBuilder spannableStringBuilder, final Function1<? super SpannableStringBuilder, Unit> function1) {
        final StrikethroughSpan strikethroughSpan = new StrikethroughSpan();
        final int length = spannableStringBuilder.length();
        function1.invoke((Object)spannableStringBuilder);
        spannableStringBuilder.setSpan((Object)strikethroughSpan, length, spannableStringBuilder.length(), 17);
        return spannableStringBuilder;
    }
    
    public static final SpannableStringBuilder subscript(final SpannableStringBuilder spannableStringBuilder, final Function1<? super SpannableStringBuilder, Unit> function1) {
        final SubscriptSpan subscriptSpan = new SubscriptSpan();
        final int length = spannableStringBuilder.length();
        function1.invoke((Object)spannableStringBuilder);
        spannableStringBuilder.setSpan((Object)subscriptSpan, length, spannableStringBuilder.length(), 17);
        return spannableStringBuilder;
    }
    
    public static final SpannableStringBuilder superscript(final SpannableStringBuilder spannableStringBuilder, final Function1<? super SpannableStringBuilder, Unit> function1) {
        final SuperscriptSpan superscriptSpan = new SuperscriptSpan();
        final int length = spannableStringBuilder.length();
        function1.invoke((Object)spannableStringBuilder);
        spannableStringBuilder.setSpan((Object)superscriptSpan, length, spannableStringBuilder.length(), 17);
        return spannableStringBuilder;
    }
    
    public static final SpannableStringBuilder underline(final SpannableStringBuilder spannableStringBuilder, final Function1<? super SpannableStringBuilder, Unit> function1) {
        final UnderlineSpan underlineSpan = new UnderlineSpan();
        final int length = spannableStringBuilder.length();
        function1.invoke((Object)spannableStringBuilder);
        spannableStringBuilder.setSpan((Object)underlineSpan, length, spannableStringBuilder.length(), 17);
        return spannableStringBuilder;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.database;

import android.os.Build$VERSION;
import android.database.CursorWindow;

public final class CursorWindowCompat
{
    private CursorWindowCompat() {
    }
    
    public static CursorWindow create(final String s, final long n) {
        if (Build$VERSION.SDK_INT >= 28) {
            return Api28Impl.createCursorWindow(s, n);
        }
        if (Build$VERSION.SDK_INT >= 15) {
            return Api15Impl.createCursorWindow(s);
        }
        return new CursorWindow(false);
    }
    
    static class Api15Impl
    {
        private Api15Impl() {
        }
        
        static CursorWindow createCursorWindow(final String s) {
            return new CursorWindow(s);
        }
    }
    
    static class Api28Impl
    {
        private Api28Impl() {
        }
        
        static CursorWindow createCursorWindow(final String s, final long n) {
            return new CursorWindow(s, n);
        }
    }
}

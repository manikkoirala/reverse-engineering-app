// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.database.sqlite;

import android.os.Build$VERSION;
import android.database.sqlite.SQLiteCursor;

public final class SQLiteCursorCompat
{
    private SQLiteCursorCompat() {
    }
    
    public static void setFillWindowForwardOnly(final SQLiteCursor sqLiteCursor, final boolean b) {
        if (Build$VERSION.SDK_INT >= 28) {
            Api28Impl.setFillWindowForwardOnly(sqLiteCursor, b);
        }
    }
    
    static class Api28Impl
    {
        private Api28Impl() {
        }
        
        static void setFillWindowForwardOnly(final SQLiteCursor sqLiteCursor, final boolean fillWindowForwardOnly) {
            sqLiteCursor.setFillWindowForwardOnly(fillWindowForwardOnly);
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.location;

import android.os.Bundle;
import android.location.Location;
import java.util.List;
import android.location.LocationListener;

public interface LocationListenerCompat extends LocationListener
{
    void onFlushComplete(final int p0);
    
    void onLocationChanged(final List<Location> p0);
    
    void onProviderDisabled(final String p0);
    
    void onProviderEnabled(final String p0);
    
    void onStatusChanged(final String p0, final int p1, final Bundle p2);
}

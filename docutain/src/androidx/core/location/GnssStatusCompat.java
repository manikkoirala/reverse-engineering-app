// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.location;

import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;
import android.location.GpsStatus;
import android.location.GnssStatus;

public abstract class GnssStatusCompat
{
    public static final int CONSTELLATION_BEIDOU = 5;
    public static final int CONSTELLATION_GALILEO = 6;
    public static final int CONSTELLATION_GLONASS = 3;
    public static final int CONSTELLATION_GPS = 1;
    public static final int CONSTELLATION_IRNSS = 7;
    public static final int CONSTELLATION_QZSS = 4;
    public static final int CONSTELLATION_SBAS = 2;
    public static final int CONSTELLATION_UNKNOWN = 0;
    
    GnssStatusCompat() {
    }
    
    public static GnssStatusCompat wrap(final GnssStatus gnssStatus) {
        return new GnssStatusWrapper(gnssStatus);
    }
    
    public static GnssStatusCompat wrap(final GpsStatus gpsStatus) {
        return new GpsStatusWrapper(gpsStatus);
    }
    
    public abstract float getAzimuthDegrees(final int p0);
    
    public abstract float getBasebandCn0DbHz(final int p0);
    
    public abstract float getCarrierFrequencyHz(final int p0);
    
    public abstract float getCn0DbHz(final int p0);
    
    public abstract int getConstellationType(final int p0);
    
    public abstract float getElevationDegrees(final int p0);
    
    public abstract int getSatelliteCount();
    
    public abstract int getSvid(final int p0);
    
    public abstract boolean hasAlmanacData(final int p0);
    
    public abstract boolean hasBasebandCn0DbHz(final int p0);
    
    public abstract boolean hasCarrierFrequencyHz(final int p0);
    
    public abstract boolean hasEphemerisData(final int p0);
    
    public abstract boolean usedInFix(final int p0);
    
    public abstract static class Callback
    {
        public void onFirstFix(final int n) {
        }
        
        public void onSatelliteStatusChanged(final GnssStatusCompat gnssStatusCompat) {
        }
        
        public void onStarted() {
        }
        
        public void onStopped() {
        }
    }
    
    @Retention(RetentionPolicy.SOURCE)
    public @interface ConstellationType {
    }
}

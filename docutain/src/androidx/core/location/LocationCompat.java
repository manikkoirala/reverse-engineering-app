// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.location;

import java.lang.reflect.InvocationTargetException;
import androidx.core.util.Preconditions;
import android.os.SystemClock;
import java.util.concurrent.TimeUnit;
import android.os.Build$VERSION;
import android.os.Bundle;
import android.location.Location;
import java.lang.reflect.Method;
import java.lang.reflect.Field;

public final class LocationCompat
{
    public static final String EXTRA_BEARING_ACCURACY = "bearingAccuracy";
    public static final String EXTRA_IS_MOCK = "mockLocation";
    public static final String EXTRA_MSL_ALTITUDE = "androidx.core.location.extra.MSL_ALTITUDE";
    public static final String EXTRA_MSL_ALTITUDE_ACCURACY = "androidx.core.location.extra.MSL_ALTITUDE_ACCURACY";
    public static final String EXTRA_SPEED_ACCURACY = "speedAccuracy";
    public static final String EXTRA_VERTICAL_ACCURACY = "verticalAccuracy";
    private static Field sFieldsMaskField;
    private static Integer sHasBearingAccuracyMask;
    private static Integer sHasSpeedAccuracyMask;
    private static Integer sHasVerticalAccuracyMask;
    private static Method sSetIsFromMockProviderMethod;
    
    private LocationCompat() {
    }
    
    private static boolean containsExtra(final Location location, final String s) {
        final Bundle extras = location.getExtras();
        return extras != null && extras.containsKey(s);
    }
    
    public static float getBearingAccuracyDegrees(final Location location) {
        if (Build$VERSION.SDK_INT >= 26) {
            return Api26Impl.getBearingAccuracyDegrees(location);
        }
        final Bundle extras = location.getExtras();
        if (extras == null) {
            return 0.0f;
        }
        return extras.getFloat("bearingAccuracy", 0.0f);
    }
    
    public static long getElapsedRealtimeMillis(final Location location) {
        if (Build$VERSION.SDK_INT >= 17) {
            return TimeUnit.NANOSECONDS.toMillis(Api17Impl.getElapsedRealtimeNanos(location));
        }
        final long n = System.currentTimeMillis() - location.getTime();
        final long elapsedRealtime = SystemClock.elapsedRealtime();
        if (n < 0L) {
            return elapsedRealtime;
        }
        if (n > elapsedRealtime) {
            return 0L;
        }
        return elapsedRealtime - n;
    }
    
    public static long getElapsedRealtimeNanos(final Location location) {
        if (Build$VERSION.SDK_INT >= 17) {
            return Api17Impl.getElapsedRealtimeNanos(location);
        }
        return TimeUnit.MILLISECONDS.toNanos(getElapsedRealtimeMillis(location));
    }
    
    static Field getFieldsMaskField() throws NoSuchFieldException {
        if (LocationCompat.sFieldsMaskField == null) {
            (LocationCompat.sFieldsMaskField = Location.class.getDeclaredField("mFieldsMask")).setAccessible(true);
        }
        return LocationCompat.sFieldsMaskField;
    }
    
    static int getHasBearingAccuracyMask() throws NoSuchFieldException, IllegalAccessException {
        if (LocationCompat.sHasBearingAccuracyMask == null) {
            final Field declaredField = Location.class.getDeclaredField("HAS_BEARING_ACCURACY_MASK");
            declaredField.setAccessible(true);
            LocationCompat.sHasBearingAccuracyMask = declaredField.getInt(null);
        }
        return LocationCompat.sHasBearingAccuracyMask;
    }
    
    static int getHasSpeedAccuracyMask() throws NoSuchFieldException, IllegalAccessException {
        if (LocationCompat.sHasSpeedAccuracyMask == null) {
            final Field declaredField = Location.class.getDeclaredField("HAS_SPEED_ACCURACY_MASK");
            declaredField.setAccessible(true);
            LocationCompat.sHasSpeedAccuracyMask = declaredField.getInt(null);
        }
        return LocationCompat.sHasSpeedAccuracyMask;
    }
    
    static int getHasVerticalAccuracyMask() throws NoSuchFieldException, IllegalAccessException {
        if (LocationCompat.sHasVerticalAccuracyMask == null) {
            final Field declaredField = Location.class.getDeclaredField("HAS_VERTICAL_ACCURACY_MASK");
            declaredField.setAccessible(true);
            LocationCompat.sHasVerticalAccuracyMask = declaredField.getInt(null);
        }
        return LocationCompat.sHasVerticalAccuracyMask;
    }
    
    public static float getMslAltitudeAccuracyMeters(final Location location) {
        if (Build$VERSION.SDK_INT >= 34) {
            return Api34Impl.getMslAltitudeAccuracyMeters(location);
        }
        Preconditions.checkState(hasMslAltitudeAccuracy(location), "The Mean Sea Level altitude accuracy of the location is not set.");
        return getOrCreateExtras(location).getFloat("androidx.core.location.extra.MSL_ALTITUDE_ACCURACY");
    }
    
    public static double getMslAltitudeMeters(final Location location) {
        if (Build$VERSION.SDK_INT >= 34) {
            return Api34Impl.getMslAltitudeMeters(location);
        }
        Preconditions.checkState(hasMslAltitude(location), "The Mean Sea Level altitude of the location is not set.");
        return getOrCreateExtras(location).getDouble("androidx.core.location.extra.MSL_ALTITUDE");
    }
    
    private static Bundle getOrCreateExtras(final Location location) {
        Bundle bundle;
        if ((bundle = location.getExtras()) == null) {
            location.setExtras(new Bundle());
            bundle = location.getExtras();
        }
        return bundle;
    }
    
    private static Method getSetIsFromMockProviderMethod() throws NoSuchMethodException {
        if (LocationCompat.sSetIsFromMockProviderMethod == null) {
            (LocationCompat.sSetIsFromMockProviderMethod = Location.class.getDeclaredMethod("setIsFromMockProvider", Boolean.TYPE)).setAccessible(true);
        }
        return LocationCompat.sSetIsFromMockProviderMethod;
    }
    
    public static float getSpeedAccuracyMetersPerSecond(final Location location) {
        if (Build$VERSION.SDK_INT >= 26) {
            return Api26Impl.getSpeedAccuracyMetersPerSecond(location);
        }
        final Bundle extras = location.getExtras();
        if (extras == null) {
            return 0.0f;
        }
        return extras.getFloat("speedAccuracy", 0.0f);
    }
    
    public static float getVerticalAccuracyMeters(final Location location) {
        if (Build$VERSION.SDK_INT >= 26) {
            return Api26Impl.getVerticalAccuracyMeters(location);
        }
        final Bundle extras = location.getExtras();
        if (extras == null) {
            return 0.0f;
        }
        return extras.getFloat("verticalAccuracy", 0.0f);
    }
    
    public static boolean hasBearingAccuracy(final Location location) {
        if (Build$VERSION.SDK_INT >= 26) {
            return Api26Impl.hasBearingAccuracy(location);
        }
        return containsExtra(location, "bearingAccuracy");
    }
    
    public static boolean hasMslAltitude(final Location location) {
        if (Build$VERSION.SDK_INT >= 34) {
            return Api34Impl.hasMslAltitude(location);
        }
        return containsExtra(location, "androidx.core.location.extra.MSL_ALTITUDE");
    }
    
    public static boolean hasMslAltitudeAccuracy(final Location location) {
        if (Build$VERSION.SDK_INT >= 34) {
            return Api34Impl.hasMslAltitudeAccuracy(location);
        }
        return containsExtra(location, "androidx.core.location.extra.MSL_ALTITUDE_ACCURACY");
    }
    
    public static boolean hasSpeedAccuracy(final Location location) {
        if (Build$VERSION.SDK_INT >= 26) {
            return Api26Impl.hasSpeedAccuracy(location);
        }
        return containsExtra(location, "speedAccuracy");
    }
    
    public static boolean hasVerticalAccuracy(final Location location) {
        if (Build$VERSION.SDK_INT >= 26) {
            return Api26Impl.hasVerticalAccuracy(location);
        }
        return containsExtra(location, "verticalAccuracy");
    }
    
    public static boolean isMock(final Location location) {
        if (Build$VERSION.SDK_INT >= 18) {
            return Api18Impl.isMock(location);
        }
        final Bundle extras = location.getExtras();
        return extras != null && extras.getBoolean("mockLocation", false);
    }
    
    public static void removeBearingAccuracy(final Location location) {
        if (Build$VERSION.SDK_INT >= 33) {
            Api33Impl.removeBearingAccuracy(location);
        }
        else if (Build$VERSION.SDK_INT >= 29) {
            Api29Impl.removeBearingAccuracy(location);
        }
        else if (Build$VERSION.SDK_INT >= 28) {
            Api28Impl.removeBearingAccuracy(location);
        }
        else if (Build$VERSION.SDK_INT >= 26) {
            Api26Impl.removeBearingAccuracy(location);
        }
        else {
            removeExtra(location, "bearingAccuracy");
        }
    }
    
    private static void removeExtra(final Location location, final String s) {
        final Bundle extras = location.getExtras();
        if (extras != null) {
            extras.remove(s);
            if (extras.isEmpty()) {
                location.setExtras((Bundle)null);
            }
        }
    }
    
    public static void removeMslAltitude(final Location location) {
        if (Build$VERSION.SDK_INT >= 34) {
            Api34Impl.removeMslAltitude(location);
        }
        else {
            removeExtra(location, "androidx.core.location.extra.MSL_ALTITUDE");
        }
    }
    
    public static void removeMslAltitudeAccuracy(final Location location) {
        if (Build$VERSION.SDK_INT >= 34) {
            Api34Impl.removeMslAltitudeAccuracy(location);
        }
        else {
            removeExtra(location, "androidx.core.location.extra.MSL_ALTITUDE_ACCURACY");
        }
    }
    
    public static void removeSpeedAccuracy(final Location location) {
        if (Build$VERSION.SDK_INT >= 33) {
            Api33Impl.removeSpeedAccuracy(location);
        }
        else if (Build$VERSION.SDK_INT >= 29) {
            Api29Impl.removeSpeedAccuracy(location);
        }
        else if (Build$VERSION.SDK_INT >= 28) {
            Api28Impl.removeSpeedAccuracy(location);
        }
        else if (Build$VERSION.SDK_INT >= 26) {
            Api26Impl.removeSpeedAccuracy(location);
        }
        else {
            removeExtra(location, "speedAccuracy");
        }
    }
    
    public static void removeVerticalAccuracy(final Location location) {
        if (Build$VERSION.SDK_INT >= 33) {
            Api33Impl.removeVerticalAccuracy(location);
        }
        else if (Build$VERSION.SDK_INT >= 29) {
            Api29Impl.removeVerticalAccuracy(location);
        }
        else if (Build$VERSION.SDK_INT >= 28) {
            Api28Impl.removeVerticalAccuracy(location);
        }
        else if (Build$VERSION.SDK_INT >= 26) {
            Api26Impl.removeVerticalAccuracy(location);
        }
        else {
            removeExtra(location, "verticalAccuracy");
        }
    }
    
    public static void setBearingAccuracyDegrees(final Location location, final float n) {
        if (Build$VERSION.SDK_INT >= 26) {
            Api26Impl.setBearingAccuracyDegrees(location, n);
        }
        else {
            getOrCreateExtras(location).putFloat("bearingAccuracy", n);
        }
    }
    
    public static void setMock(final Location obj, final boolean b) {
        if (Build$VERSION.SDK_INT >= 18) {
            try {
                getSetIsFromMockProviderMethod().invoke(obj, b);
                return;
            }
            catch (final InvocationTargetException cause) {
                throw new RuntimeException(cause);
            }
            catch (final IllegalAccessException cause2) {
                final IllegalAccessError illegalAccessError = new IllegalAccessError();
                illegalAccessError.initCause(cause2);
                throw illegalAccessError;
            }
            catch (final NoSuchMethodException cause3) {
                final NoSuchMethodError noSuchMethodError = new NoSuchMethodError();
                noSuchMethodError.initCause(cause3);
                throw noSuchMethodError;
            }
        }
        final Bundle extras = obj.getExtras();
        if (extras == null) {
            if (b) {
                final Bundle extras2 = new Bundle();
                extras2.putBoolean("mockLocation", true);
                obj.setExtras(extras2);
            }
        }
        else if (b) {
            extras.putBoolean("mockLocation", true);
        }
        else {
            extras.remove("mockLocation");
            if (extras.isEmpty()) {
                obj.setExtras((Bundle)null);
            }
        }
    }
    
    public static void setMslAltitudeAccuracyMeters(final Location location, final float n) {
        if (Build$VERSION.SDK_INT >= 34) {
            Api34Impl.setMslAltitudeAccuracyMeters(location, n);
        }
        else {
            getOrCreateExtras(location).putFloat("androidx.core.location.extra.MSL_ALTITUDE_ACCURACY", n);
        }
    }
    
    public static void setMslAltitudeMeters(final Location location, final double n) {
        if (Build$VERSION.SDK_INT >= 34) {
            Api34Impl.setMslAltitudeMeters(location, n);
        }
        else {
            getOrCreateExtras(location).putDouble("androidx.core.location.extra.MSL_ALTITUDE", n);
        }
    }
    
    public static void setSpeedAccuracyMetersPerSecond(final Location location, final float n) {
        if (Build$VERSION.SDK_INT >= 26) {
            Api26Impl.setSpeedAccuracyMetersPerSecond(location, n);
        }
        else {
            getOrCreateExtras(location).putFloat("speedAccuracy", n);
        }
    }
    
    public static void setVerticalAccuracyMeters(final Location location, final float n) {
        if (Build$VERSION.SDK_INT >= 26) {
            Api26Impl.setVerticalAccuracyMeters(location, n);
        }
        else {
            getOrCreateExtras(location).putFloat("verticalAccuracy", n);
        }
    }
    
    private static class Api17Impl
    {
        static long getElapsedRealtimeNanos(final Location location) {
            return location.getElapsedRealtimeNanos();
        }
    }
    
    private static class Api18Impl
    {
        static boolean isMock(final Location location) {
            return location.isFromMockProvider();
        }
    }
    
    private static class Api26Impl
    {
        static float getBearingAccuracyDegrees(final Location location) {
            return location.getBearingAccuracyDegrees();
        }
        
        static float getSpeedAccuracyMetersPerSecond(final Location location) {
            return location.getSpeedAccuracyMetersPerSecond();
        }
        
        static float getVerticalAccuracyMeters(final Location location) {
            return location.getVerticalAccuracyMeters();
        }
        
        static boolean hasBearingAccuracy(final Location location) {
            return location.hasBearingAccuracy();
        }
        
        static boolean hasSpeedAccuracy(final Location location) {
            return location.hasSpeedAccuracy();
        }
        
        static boolean hasVerticalAccuracy(final Location location) {
            return location.hasVerticalAccuracy();
        }
        
        static void removeBearingAccuracy(final Location location) {
            try {
                LocationCompat.getFieldsMaskField().setByte(location, (byte)(LocationCompat.getFieldsMaskField().getByte(location) & ~LocationCompat.getHasBearingAccuracyMask()));
            }
            catch (final IllegalAccessException cause) {
                final IllegalAccessError illegalAccessError = new IllegalAccessError();
                illegalAccessError.initCause(cause);
                throw illegalAccessError;
            }
            catch (final NoSuchFieldException cause2) {
                final NoSuchFieldError noSuchFieldError = new NoSuchFieldError();
                noSuchFieldError.initCause(cause2);
                throw noSuchFieldError;
            }
        }
        
        static void removeSpeedAccuracy(final Location location) {
            try {
                LocationCompat.getFieldsMaskField().setByte(location, (byte)(LocationCompat.getFieldsMaskField().getByte(location) & ~LocationCompat.getHasSpeedAccuracyMask()));
            }
            catch (final IllegalAccessException cause) {
                final IllegalAccessError illegalAccessError = new IllegalAccessError();
                illegalAccessError.initCause(cause);
                throw illegalAccessError;
            }
            catch (final NoSuchFieldException cause2) {
                final NoSuchFieldError noSuchFieldError = new NoSuchFieldError();
                noSuchFieldError.initCause(cause2);
                throw noSuchFieldError;
            }
        }
        
        static void removeVerticalAccuracy(final Location cause) {
            try {
                LocationCompat.getFieldsMaskField().setByte(cause, (byte)(LocationCompat.getFieldsMaskField().getByte(cause) & ~LocationCompat.getHasVerticalAccuracyMask()));
                return;
            }
            catch (final IllegalAccessException cause) {}
            catch (final NoSuchFieldException ex) {}
            final IllegalAccessError illegalAccessError = new IllegalAccessError();
            illegalAccessError.initCause(cause);
            throw illegalAccessError;
        }
        
        static void setBearingAccuracyDegrees(final Location location, final float bearingAccuracyDegrees) {
            location.setBearingAccuracyDegrees(bearingAccuracyDegrees);
        }
        
        static void setSpeedAccuracyMetersPerSecond(final Location location, final float speedAccuracyMetersPerSecond) {
            location.setSpeedAccuracyMetersPerSecond(speedAccuracyMetersPerSecond);
        }
        
        static void setVerticalAccuracyMeters(final Location location, final float verticalAccuracyMeters) {
            location.setVerticalAccuracyMeters(verticalAccuracyMeters);
        }
    }
    
    private static class Api28Impl
    {
        static void removeBearingAccuracy(final Location location) {
            if (!location.hasBearingAccuracy()) {
                return;
            }
            final String provider = location.getProvider();
            final long time = location.getTime();
            final long elapsedRealtimeNanos = location.getElapsedRealtimeNanos();
            final double latitude = location.getLatitude();
            final double longitude = location.getLongitude();
            final boolean hasAltitude = location.hasAltitude();
            final double altitude = location.getAltitude();
            final boolean hasSpeed = location.hasSpeed();
            final float speed = location.getSpeed();
            final boolean hasBearing = location.hasBearing();
            final float bearing = location.getBearing();
            final boolean hasAccuracy = location.hasAccuracy();
            final float accuracy = location.getAccuracy();
            final boolean hasVerticalAccuracy = location.hasVerticalAccuracy();
            final float verticalAccuracyMeters = location.getVerticalAccuracyMeters();
            final boolean hasSpeedAccuracy = location.hasSpeedAccuracy();
            final float speedAccuracyMetersPerSecond = location.getSpeedAccuracyMetersPerSecond();
            final Bundle extras = location.getExtras();
            location.reset();
            location.setProvider(provider);
            location.setTime(time);
            location.setElapsedRealtimeNanos(elapsedRealtimeNanos);
            location.setLatitude(latitude);
            location.setLongitude(longitude);
            if (hasAltitude) {
                location.setAltitude(altitude);
            }
            if (hasSpeed) {
                location.setSpeed(speed);
            }
            if (hasBearing) {
                location.setBearing(bearing);
            }
            if (hasAccuracy) {
                location.setAccuracy(accuracy);
            }
            if (hasVerticalAccuracy) {
                location.setVerticalAccuracyMeters(verticalAccuracyMeters);
            }
            if (hasSpeedAccuracy) {
                location.setBearingAccuracyDegrees(speedAccuracyMetersPerSecond);
            }
            if (extras != null) {
                location.setExtras(extras);
            }
        }
        
        static void removeSpeedAccuracy(final Location location) {
            if (!location.hasSpeedAccuracy()) {
                return;
            }
            final String provider = location.getProvider();
            final long time = location.getTime();
            final long elapsedRealtimeNanos = location.getElapsedRealtimeNanos();
            final double latitude = location.getLatitude();
            final double longitude = location.getLongitude();
            final boolean hasAltitude = location.hasAltitude();
            final double altitude = location.getAltitude();
            final boolean hasSpeed = location.hasSpeed();
            final float speed = location.getSpeed();
            final boolean hasBearing = location.hasBearing();
            final float bearing = location.getBearing();
            final boolean hasAccuracy = location.hasAccuracy();
            final float accuracy = location.getAccuracy();
            final boolean hasVerticalAccuracy = location.hasVerticalAccuracy();
            final float verticalAccuracyMeters = location.getVerticalAccuracyMeters();
            final boolean hasBearingAccuracy = location.hasBearingAccuracy();
            final float bearingAccuracyDegrees = location.getBearingAccuracyDegrees();
            final Bundle extras = location.getExtras();
            location.reset();
            location.setProvider(provider);
            location.setTime(time);
            location.setElapsedRealtimeNanos(elapsedRealtimeNanos);
            location.setLatitude(latitude);
            location.setLongitude(longitude);
            if (hasAltitude) {
                location.setAltitude(altitude);
            }
            if (hasSpeed) {
                location.setSpeed(speed);
            }
            if (hasBearing) {
                location.setBearing(bearing);
            }
            if (hasAccuracy) {
                location.setAccuracy(accuracy);
            }
            if (hasVerticalAccuracy) {
                location.setVerticalAccuracyMeters(verticalAccuracyMeters);
            }
            if (hasBearingAccuracy) {
                location.setBearingAccuracyDegrees(bearingAccuracyDegrees);
            }
            if (extras != null) {
                location.setExtras(extras);
            }
        }
        
        static void removeVerticalAccuracy(final Location location) {
            if (!location.hasVerticalAccuracy()) {
                return;
            }
            final String provider = location.getProvider();
            final long time = location.getTime();
            final long elapsedRealtimeNanos = location.getElapsedRealtimeNanos();
            final double latitude = location.getLatitude();
            final double longitude = location.getLongitude();
            final boolean hasAltitude = location.hasAltitude();
            final double altitude = location.getAltitude();
            final boolean hasSpeed = location.hasSpeed();
            final float speed = location.getSpeed();
            final boolean hasBearing = location.hasBearing();
            final float bearing = location.getBearing();
            final boolean hasAccuracy = location.hasAccuracy();
            final float accuracy = location.getAccuracy();
            final boolean hasSpeedAccuracy = location.hasSpeedAccuracy();
            final float speedAccuracyMetersPerSecond = location.getSpeedAccuracyMetersPerSecond();
            final boolean hasBearingAccuracy = location.hasBearingAccuracy();
            final float bearingAccuracyDegrees = location.getBearingAccuracyDegrees();
            final Bundle extras = location.getExtras();
            location.reset();
            location.setProvider(provider);
            location.setTime(time);
            location.setElapsedRealtimeNanos(elapsedRealtimeNanos);
            location.setLatitude(latitude);
            location.setLongitude(longitude);
            if (hasAltitude) {
                location.setAltitude(altitude);
            }
            if (hasSpeed) {
                location.setSpeed(speed);
            }
            if (hasBearing) {
                location.setBearing(bearing);
            }
            if (hasAccuracy) {
                location.setAccuracy(accuracy);
            }
            if (hasSpeedAccuracy) {
                location.setSpeedAccuracyMetersPerSecond(speedAccuracyMetersPerSecond);
            }
            if (hasBearingAccuracy) {
                location.setBearingAccuracyDegrees(bearingAccuracyDegrees);
            }
            if (extras != null) {
                location.setExtras(extras);
            }
        }
    }
    
    private static class Api29Impl
    {
        static void removeBearingAccuracy(final Location location) {
            if (!location.hasBearingAccuracy()) {
                return;
            }
            final double elapsedRealtimeUncertaintyNanos = location.getElapsedRealtimeUncertaintyNanos();
            Api28Impl.removeBearingAccuracy(location);
            location.setElapsedRealtimeUncertaintyNanos(elapsedRealtimeUncertaintyNanos);
        }
        
        static void removeSpeedAccuracy(final Location location) {
            if (!location.hasSpeedAccuracy()) {
                return;
            }
            final double elapsedRealtimeUncertaintyNanos = location.getElapsedRealtimeUncertaintyNanos();
            Api28Impl.removeSpeedAccuracy(location);
            location.setElapsedRealtimeUncertaintyNanos(elapsedRealtimeUncertaintyNanos);
        }
        
        static void removeVerticalAccuracy(final Location location) {
            if (!location.hasVerticalAccuracy()) {
                return;
            }
            final double elapsedRealtimeUncertaintyNanos = location.getElapsedRealtimeUncertaintyNanos();
            Api28Impl.removeVerticalAccuracy(location);
            location.setElapsedRealtimeUncertaintyNanos(elapsedRealtimeUncertaintyNanos);
        }
    }
    
    private static class Api33Impl
    {
        static void removeBearingAccuracy(final Location location) {
            location.removeBearingAccuracy();
        }
        
        static void removeSpeedAccuracy(final Location location) {
            location.removeSpeedAccuracy();
        }
        
        static void removeVerticalAccuracy(final Location location) {
            location.removeVerticalAccuracy();
        }
    }
    
    private static class Api34Impl
    {
        static float getMslAltitudeAccuracyMeters(final Location location) {
            return location.getMslAltitudeAccuracyMeters();
        }
        
        static double getMslAltitudeMeters(final Location location) {
            return location.getMslAltitudeMeters();
        }
        
        static boolean hasMslAltitude(final Location location) {
            return location.hasMslAltitude();
        }
        
        static boolean hasMslAltitudeAccuracy(final Location location) {
            return location.hasMslAltitudeAccuracy();
        }
        
        static void removeMslAltitude(final Location location) {
            location.removeMslAltitude();
        }
        
        static void removeMslAltitudeAccuracy(final Location location) {
            location.removeMslAltitudeAccuracy();
        }
        
        static void setMslAltitudeAccuracyMeters(final Location location, final float mslAltitudeAccuracyMeters) {
            location.setMslAltitudeAccuracyMeters(mslAltitudeAccuracyMeters);
        }
        
        static void setMslAltitudeMeters(final Location location, final double mslAltitudeMeters) {
            location.setMslAltitudeMeters(mslAltitudeMeters);
        }
    }
}

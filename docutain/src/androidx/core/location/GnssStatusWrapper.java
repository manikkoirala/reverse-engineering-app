// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.location;

import android.os.Build$VERSION;
import androidx.core.util.Preconditions;
import android.location.GnssStatus;

class GnssStatusWrapper extends GnssStatusCompat
{
    private final GnssStatus mWrapped;
    
    GnssStatusWrapper(final Object o) {
        this.mWrapped = Preconditions.checkNotNull(o);
    }
    
    @Override
    public boolean equals(final Object o) {
        return this == o || (o instanceof GnssStatusWrapper && this.mWrapped.equals((Object)((GnssStatusWrapper)o).mWrapped));
    }
    
    @Override
    public float getAzimuthDegrees(final int n) {
        return this.mWrapped.getAzimuthDegrees(n);
    }
    
    @Override
    public float getBasebandCn0DbHz(final int n) {
        if (Build$VERSION.SDK_INT >= 30) {
            return Api30Impl.getBasebandCn0DbHz(this.mWrapped, n);
        }
        throw new UnsupportedOperationException();
    }
    
    @Override
    public float getCarrierFrequencyHz(final int n) {
        if (Build$VERSION.SDK_INT >= 26) {
            return Api26Impl.getCarrierFrequencyHz(this.mWrapped, n);
        }
        throw new UnsupportedOperationException();
    }
    
    @Override
    public float getCn0DbHz(final int n) {
        return this.mWrapped.getCn0DbHz(n);
    }
    
    @Override
    public int getConstellationType(final int n) {
        return this.mWrapped.getConstellationType(n);
    }
    
    @Override
    public float getElevationDegrees(final int n) {
        return this.mWrapped.getElevationDegrees(n);
    }
    
    @Override
    public int getSatelliteCount() {
        return this.mWrapped.getSatelliteCount();
    }
    
    @Override
    public int getSvid(final int n) {
        return this.mWrapped.getSvid(n);
    }
    
    @Override
    public boolean hasAlmanacData(final int n) {
        return this.mWrapped.hasAlmanacData(n);
    }
    
    @Override
    public boolean hasBasebandCn0DbHz(final int n) {
        return Build$VERSION.SDK_INT >= 30 && Api30Impl.hasBasebandCn0DbHz(this.mWrapped, n);
    }
    
    @Override
    public boolean hasCarrierFrequencyHz(final int n) {
        return Build$VERSION.SDK_INT >= 26 && Api26Impl.hasCarrierFrequencyHz(this.mWrapped, n);
    }
    
    @Override
    public boolean hasEphemerisData(final int n) {
        return this.mWrapped.hasEphemerisData(n);
    }
    
    @Override
    public int hashCode() {
        return this.mWrapped.hashCode();
    }
    
    @Override
    public boolean usedInFix(final int n) {
        return this.mWrapped.usedInFix(n);
    }
    
    static class Api26Impl
    {
        private Api26Impl() {
        }
        
        static float getCarrierFrequencyHz(final GnssStatus gnssStatus, final int n) {
            return gnssStatus.getCarrierFrequencyHz(n);
        }
        
        static boolean hasCarrierFrequencyHz(final GnssStatus gnssStatus, final int n) {
            return gnssStatus.hasCarrierFrequencyHz(n);
        }
    }
    
    static class Api30Impl
    {
        private Api30Impl() {
        }
        
        static float getBasebandCn0DbHz(final GnssStatus gnssStatus, final int n) {
            return gnssStatus.getBasebandCn0DbHz(n);
        }
        
        static boolean hasBasebandCn0DbHz(final GnssStatus gnssStatus, final int n) {
            return gnssStatus.hasBasebandCn0DbHz(n);
        }
    }
}

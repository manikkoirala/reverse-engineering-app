// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.location;

import java.util.List;
import androidx.core.util.ObjectsCompat;
import java.util.concurrent.RejectedExecutionException;
import android.location.GpsStatus;
import android.location.GnssStatus;
import android.location.GnssMeasurementsEvent;
import androidx.collection.SimpleArrayMap;
import android.os.Bundle;
import java.util.Objects;
import android.location.GnssStatus$Callback;
import androidx.core.util.Preconditions;
import android.location.LocationRequest;
import java.util.Iterator;
import java.util.ArrayList;
import java.lang.reflect.InvocationTargetException;
import androidx.core.os.ExecutorCompat;
import android.os.Handler;
import android.location.GnssMeasurementsEvent$Callback;
import android.location.GpsStatus$Listener;
import android.text.TextUtils;
import android.provider.Settings$Secure;
import android.content.Context;
import android.location.LocationListener;
import android.os.Looper;
import android.os.SystemClock;
import android.os.Build$VERSION;
import android.location.Location;
import androidx.core.util.Consumer;
import java.util.concurrent.Executor;
import androidx.core.os.CancellationSignal;
import android.location.LocationManager;
import java.lang.ref.WeakReference;
import java.util.WeakHashMap;
import java.lang.reflect.Method;
import java.lang.reflect.Field;

public final class LocationManagerCompat
{
    private static final long GET_CURRENT_LOCATION_TIMEOUT_MS = 30000L;
    private static final long MAX_CURRENT_LOCATION_AGE_MS = 10000L;
    private static final long PRE_N_LOOPER_TIMEOUT_S = 5L;
    private static Field sContextField;
    private static Method sGnssRequestBuilderBuildMethod;
    private static Class<?> sGnssRequestBuilderClass;
    static final WeakHashMap<LocationListenerKey, WeakReference<LocationListenerTransport>> sLocationListeners;
    private static Method sRegisterGnssMeasurementsCallbackMethod;
    
    static {
        sLocationListeners = new WeakHashMap<LocationListenerKey, WeakReference<LocationListenerTransport>>();
    }
    
    private LocationManagerCompat() {
    }
    
    public static void getCurrentLocation(final LocationManager locationManager, final String s, final CancellationSignal cancellationSignal, final Executor executor, final Consumer<Location> consumer) {
        if (Build$VERSION.SDK_INT >= 30) {
            Api30Impl.getCurrentLocation(locationManager, s, cancellationSignal, executor, consumer);
            return;
        }
        if (cancellationSignal != null) {
            cancellationSignal.throwIfCanceled();
        }
        final Location lastKnownLocation = locationManager.getLastKnownLocation(s);
        if (lastKnownLocation != null && SystemClock.elapsedRealtime() - LocationCompat.getElapsedRealtimeMillis(lastKnownLocation) < 10000L) {
            executor.execute(new LocationManagerCompat$$ExternalSyntheticLambda1(consumer, lastKnownLocation));
            return;
        }
        final CancellableLocationListener cancellableLocationListener = new CancellableLocationListener(locationManager, executor, consumer);
        locationManager.requestLocationUpdates(s, 0L, 0.0f, (LocationListener)cancellableLocationListener, Looper.getMainLooper());
        if (cancellationSignal != null) {
            cancellationSignal.setOnCancelListener((CancellationSignal.OnCancelListener)new LocationManagerCompat$$ExternalSyntheticLambda2(cancellableLocationListener));
        }
        cancellableLocationListener.startTimeout(30000L);
    }
    
    public static String getGnssHardwareModelName(final LocationManager locationManager) {
        if (Build$VERSION.SDK_INT >= 28) {
            return Api28Impl.getGnssHardwareModelName(locationManager);
        }
        return null;
    }
    
    public static int getGnssYearOfHardware(final LocationManager locationManager) {
        if (Build$VERSION.SDK_INT >= 28) {
            return Api28Impl.getGnssYearOfHardware(locationManager);
        }
        return 0;
    }
    
    public static boolean hasProvider(final LocationManager locationManager, final String s) {
        if (Build$VERSION.SDK_INT >= 31) {
            return Api31Impl.hasProvider(locationManager, s);
        }
        final boolean contains = locationManager.getAllProviders().contains(s);
        boolean b = true;
        if (contains) {
            return true;
        }
        try {
            if (locationManager.getProvider(s) == null) {
                b = false;
            }
            return b;
        }
        catch (final SecurityException ex) {
            return false;
        }
    }
    
    public static boolean isLocationEnabled(final LocationManager obj) {
        if (Build$VERSION.SDK_INT >= 28) {
            return Api28Impl.isLocationEnabled(obj);
        }
        final int sdk_INT = Build$VERSION.SDK_INT;
        final boolean b = false;
        boolean b2 = false;
        if (sdk_INT <= 19) {
            try {
                if (LocationManagerCompat.sContextField == null) {
                    (LocationManagerCompat.sContextField = LocationManager.class.getDeclaredField("mContext")).setAccessible(true);
                }
                final Context context = (Context)LocationManagerCompat.sContextField.get(obj);
                if (context != null) {
                    if (Build$VERSION.SDK_INT == 19) {
                        if (Settings$Secure.getInt(context.getContentResolver(), "location_mode", 0) != 0) {
                            b2 = true;
                        }
                        return b2;
                    }
                    return TextUtils.isEmpty((CharSequence)Settings$Secure.getString(context.getContentResolver(), "location_providers_allowed")) ^ true;
                }
            }
            catch (final ClassCastException | SecurityException | NoSuchFieldException | IllegalAccessException ex) {}
        }
        if (!obj.isProviderEnabled("network")) {
            final boolean b3 = b;
            if (!obj.isProviderEnabled("gps")) {
                return b3;
            }
        }
        return true;
    }
    
    public static boolean registerGnssMeasurementsCallback(final LocationManager locationManager, final GnssMeasurementsEvent$Callback gnssMeasurementsEvent$Callback, final Handler handler) {
        if (Build$VERSION.SDK_INT > 30) {
            return Api24Impl.registerGnssMeasurementsCallback(locationManager, gnssMeasurementsEvent$Callback, handler);
        }
        if (Build$VERSION.SDK_INT == 30) {
            return registerGnssMeasurementsCallbackOnR(locationManager, ExecutorCompat.create(handler), gnssMeasurementsEvent$Callback);
        }
        synchronized (GnssListenersHolder.sGnssMeasurementListeners) {
            unregisterGnssMeasurementsCallback(locationManager, gnssMeasurementsEvent$Callback);
            if (Api24Impl.registerGnssMeasurementsCallback(locationManager, gnssMeasurementsEvent$Callback, handler)) {
                GnssListenersHolder.sGnssMeasurementListeners.put(gnssMeasurementsEvent$Callback, gnssMeasurementsEvent$Callback);
                return true;
            }
            return false;
        }
    }
    
    public static boolean registerGnssMeasurementsCallback(final LocationManager locationManager, final Executor executor, final GnssMeasurementsEvent$Callback gnssMeasurementsEvent$Callback) {
        if (Build$VERSION.SDK_INT > 30) {
            return Api31Impl.registerGnssMeasurementsCallback(locationManager, executor, gnssMeasurementsEvent$Callback);
        }
        if (Build$VERSION.SDK_INT == 30) {
            return registerGnssMeasurementsCallbackOnR(locationManager, executor, gnssMeasurementsEvent$Callback);
        }
        synchronized (GnssListenersHolder.sGnssMeasurementListeners) {
            final GnssMeasurementsTransport gnssMeasurementsTransport = new GnssMeasurementsTransport(gnssMeasurementsEvent$Callback, executor);
            unregisterGnssMeasurementsCallback(locationManager, gnssMeasurementsEvent$Callback);
            if (Api24Impl.registerGnssMeasurementsCallback(locationManager, gnssMeasurementsTransport)) {
                GnssListenersHolder.sGnssMeasurementListeners.put(gnssMeasurementsEvent$Callback, gnssMeasurementsTransport);
                return true;
            }
            return false;
        }
    }
    
    private static boolean registerGnssMeasurementsCallbackOnR(final LocationManager obj, final Executor executor, final GnssMeasurementsEvent$Callback gnssMeasurementsEvent$Callback) {
        Label_0187: {
            if (Build$VERSION.SDK_INT != 30) {
                break Label_0187;
            }
            final boolean b = false;
            try {
                if (LocationManagerCompat.sGnssRequestBuilderClass == null) {
                    LocationManagerCompat.sGnssRequestBuilderClass = Class.forName("android.location.GnssRequest$Builder");
                }
                if (LocationManagerCompat.sGnssRequestBuilderBuildMethod == null) {
                    (LocationManagerCompat.sGnssRequestBuilderBuildMethod = LocationManagerCompat.sGnssRequestBuilderClass.getDeclaredMethod("build", (Class<?>[])new Class[0])).setAccessible(true);
                }
                if (LocationManagerCompat.sRegisterGnssMeasurementsCallbackMethod == null) {
                    (LocationManagerCompat.sRegisterGnssMeasurementsCallbackMethod = LocationManager.class.getDeclaredMethod("registerGnssMeasurementsCallback", Class.forName("android.location.GnssRequest"), Executor.class, GnssMeasurementsEvent$Callback.class)).setAccessible(true);
                }
                final Object invoke = LocationManagerCompat.sRegisterGnssMeasurementsCallbackMethod.invoke(obj, LocationManagerCompat.sGnssRequestBuilderBuildMethod.invoke(LocationManagerCompat.sGnssRequestBuilderClass.getDeclaredConstructor((Class<?>[])new Class[0]).newInstance(new Object[0]), new Object[0]), executor, gnssMeasurementsEvent$Callback);
                boolean b2 = b;
                if (invoke != null) {
                    final boolean booleanValue = (boolean)invoke;
                    b2 = b;
                    if (booleanValue) {
                        b2 = true;
                    }
                }
                return b2;
                throw new IllegalStateException();
            }
            catch (final ClassNotFoundException | InvocationTargetException | NoSuchMethodException | IllegalAccessException | InstantiationException ex) {
                return b;
            }
        }
    }
    
    private static boolean registerGnssStatusCallback(final LocationManager p0, final Handler p1, final Executor p2, final GnssStatusCompat.Callback p3) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     3: bipush          30
        //     5: if_icmplt       16
        //     8: aload_0        
        //     9: aload_1        
        //    10: aload_2        
        //    11: aload_3        
        //    12: invokestatic    androidx/core/location/LocationManagerCompat$Api30Impl.registerGnssStatusCallback:(Landroid/location/LocationManager;Landroid/os/Handler;Ljava/util/concurrent/Executor;Landroidx/core/location/GnssStatusCompat$Callback;)Z
        //    15: ireturn        
        //    16: getstatic       android/os/Build$VERSION.SDK_INT:I
        //    19: bipush          24
        //    21: if_icmplt       32
        //    24: aload_0        
        //    25: aload_1        
        //    26: aload_2        
        //    27: aload_3        
        //    28: invokestatic    androidx/core/location/LocationManagerCompat$Api24Impl.registerGnssStatusCallback:(Landroid/location/LocationManager;Landroid/os/Handler;Ljava/util/concurrent/Executor;Landroidx/core/location/GnssStatusCompat$Callback;)Z
        //    31: ireturn        
        //    32: iconst_1       
        //    33: istore          6
        //    35: iconst_1       
        //    36: istore          7
        //    38: iconst_1       
        //    39: istore          5
        //    41: aload_1        
        //    42: ifnull          51
        //    45: iconst_1       
        //    46: istore          14
        //    48: goto            54
        //    51: iconst_0       
        //    52: istore          14
        //    54: iload           14
        //    56: invokestatic    androidx/core/util/Preconditions.checkArgument:(Z)V
        //    59: getstatic       androidx/core/location/LocationManagerCompat$GnssListenersHolder.sGnssStatusListeners:Landroidx/collection/SimpleArrayMap;
        //    62: astore          16
        //    64: aload           16
        //    66: monitorenter   
        //    67: getstatic       androidx/core/location/LocationManagerCompat$GnssListenersHolder.sGnssStatusListeners:Landroidx/collection/SimpleArrayMap;
        //    70: aload_3        
        //    71: invokevirtual   androidx/collection/SimpleArrayMap.get:(Ljava/lang/Object;)Ljava/lang/Object;
        //    74: checkcast       Landroidx/core/location/LocationManagerCompat$GpsStatusTransport;
        //    77: astore          15
        //    79: aload           15
        //    81: ifnonnull       99
        //    84: new             Landroidx/core/location/LocationManagerCompat$GpsStatusTransport;
        //    87: astore          15
        //    89: aload           15
        //    91: aload_0        
        //    92: aload_3        
        //    93: invokespecial   androidx/core/location/LocationManagerCompat$GpsStatusTransport.<init>:(Landroid/location/LocationManager;Landroidx/core/location/GnssStatusCompat$Callback;)V
        //    96: goto            104
        //    99: aload           15
        //   101: invokevirtual   androidx/core/location/LocationManagerCompat$GpsStatusTransport.unregister:()V
        //   104: aload           15
        //   106: aload_2        
        //   107: invokevirtual   androidx/core/location/LocationManagerCompat$GpsStatusTransport.register:(Ljava/util/concurrent/Executor;)V
        //   110: new             Ljava/util/concurrent/FutureTask;
        //   113: astore_2       
        //   114: new             Landroidx/core/location/LocationManagerCompat$$ExternalSyntheticLambda0;
        //   117: astore          17
        //   119: aload           17
        //   121: aload_0        
        //   122: aload           15
        //   124: invokespecial   androidx/core/location/LocationManagerCompat$$ExternalSyntheticLambda0.<init>:(Landroid/location/LocationManager;Landroidx/core/location/LocationManagerCompat$GpsStatusTransport;)V
        //   127: aload_2        
        //   128: aload           17
        //   130: invokespecial   java/util/concurrent/FutureTask.<init>:(Ljava/util/concurrent/Callable;)V
        //   133: invokestatic    android/os/Looper.myLooper:()Landroid/os/Looper;
        //   136: aload_1        
        //   137: invokevirtual   android/os/Handler.getLooper:()Landroid/os/Looper;
        //   140: if_acmpne       150
        //   143: aload_2        
        //   144: invokevirtual   java/util/concurrent/FutureTask.run:()V
        //   147: goto            162
        //   150: aload_1        
        //   151: aload_2        
        //   152: invokevirtual   android/os/Handler.post:(Ljava/lang/Runnable;)Z
        //   155: istore          14
        //   157: iload           14
        //   159: ifeq            472
        //   162: getstatic       java/util/concurrent/TimeUnit.SECONDS:Ljava/util/concurrent/TimeUnit;
        //   165: ldc2_w          5
        //   168: invokevirtual   java/util/concurrent/TimeUnit.toNanos:(J)J
        //   171: lstore          10
        //   173: invokestatic    java/lang/System.nanoTime:()J
        //   176: lstore          12
        //   178: iconst_0       
        //   179: istore          4
        //   181: lload           10
        //   183: lstore          8
        //   185: aload_2        
        //   186: lload           8
        //   188: getstatic       java/util/concurrent/TimeUnit.NANOSECONDS:Ljava/util/concurrent/TimeUnit;
        //   191: invokevirtual   java/util/concurrent/FutureTask.get:(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;
        //   194: checkcast       Ljava/lang/Boolean;
        //   197: invokevirtual   java/lang/Boolean.booleanValue:()Z
        //   200: ifeq            229
        //   203: getstatic       androidx/core/location/LocationManagerCompat$GnssListenersHolder.sGnssStatusListeners:Landroidx/collection/SimpleArrayMap;
        //   206: aload_3        
        //   207: aload           15
        //   209: invokevirtual   androidx/collection/SimpleArrayMap.put:(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
        //   212: pop            
        //   213: iload           4
        //   215: ifeq            224
        //   218: invokestatic    java/lang/Thread.currentThread:()Ljava/lang/Thread;
        //   221: invokevirtual   java/lang/Thread.interrupt:()V
        //   224: aload           16
        //   226: monitorexit    
        //   227: iconst_1       
        //   228: ireturn        
        //   229: iload           4
        //   231: ifeq            240
        //   234: invokestatic    java/lang/Thread.currentThread:()Ljava/lang/Thread;
        //   237: invokevirtual   java/lang/Thread.interrupt:()V
        //   240: aload           16
        //   242: monitorexit    
        //   243: iconst_0       
        //   244: ireturn        
        //   245: astore_0       
        //   246: goto            459
        //   249: astore_0       
        //   250: iload           4
        //   252: istore          5
        //   254: goto            314
        //   257: astore_0       
        //   258: iload           4
        //   260: istore          5
        //   262: goto            383
        //   265: astore_0       
        //   266: iload           7
        //   268: istore          4
        //   270: invokestatic    java/lang/System.nanoTime:()J
        //   273: lstore          8
        //   275: lload           12
        //   277: lload           10
        //   279: ladd           
        //   280: lload           8
        //   282: lsub           
        //   283: lstore          8
        //   285: iconst_1       
        //   286: istore          4
        //   288: goto            185
        //   291: astore_0       
        //   292: goto            314
        //   295: astore_0       
        //   296: iload           6
        //   298: istore          5
        //   300: goto            383
        //   303: astore_0       
        //   304: iconst_0       
        //   305: istore          4
        //   307: goto            459
        //   310: astore_0       
        //   311: iconst_0       
        //   312: istore          5
        //   314: iload           5
        //   316: istore          4
        //   318: new             Ljava/lang/IllegalStateException;
        //   321: astore_2       
        //   322: iload           5
        //   324: istore          4
        //   326: new             Ljava/lang/StringBuilder;
        //   329: astore_3       
        //   330: iload           5
        //   332: istore          4
        //   334: aload_3        
        //   335: invokespecial   java/lang/StringBuilder.<init>:()V
        //   338: iload           5
        //   340: istore          4
        //   342: aload_3        
        //   343: aload_1        
        //   344: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //   347: pop            
        //   348: iload           5
        //   350: istore          4
        //   352: aload_3        
        //   353: ldc_w           " appears to be blocked, please run registerGnssStatusCallback() directly on a Looper thread or ensure the main Looper is not blocked by this thread"
        //   356: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   359: pop            
        //   360: iload           5
        //   362: istore          4
        //   364: aload_2        
        //   365: aload_3        
        //   366: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   369: aload_0        
        //   370: invokespecial   java/lang/IllegalStateException.<init>:(Ljava/lang/String;Ljava/lang/Throwable;)V
        //   373: iload           5
        //   375: istore          4
        //   377: aload_2        
        //   378: athrow         
        //   379: astore_0       
        //   380: iconst_0       
        //   381: istore          5
        //   383: iload           5
        //   385: istore          4
        //   387: aload_0        
        //   388: invokevirtual   java/util/concurrent/ExecutionException.getCause:()Ljava/lang/Throwable;
        //   391: instanceof      Ljava/lang/RuntimeException;
        //   394: ifne            446
        //   397: iload           5
        //   399: istore          4
        //   401: aload_0        
        //   402: invokevirtual   java/util/concurrent/ExecutionException.getCause:()Ljava/lang/Throwable;
        //   405: instanceof      Ljava/lang/Error;
        //   408: ifeq            423
        //   411: iload           5
        //   413: istore          4
        //   415: aload_0        
        //   416: invokevirtual   java/util/concurrent/ExecutionException.getCause:()Ljava/lang/Throwable;
        //   419: checkcast       Ljava/lang/Error;
        //   422: athrow         
        //   423: iload           5
        //   425: istore          4
        //   427: new             Ljava/lang/IllegalStateException;
        //   430: astore_1       
        //   431: iload           5
        //   433: istore          4
        //   435: aload_1        
        //   436: aload_0        
        //   437: invokespecial   java/lang/IllegalStateException.<init>:(Ljava/lang/Throwable;)V
        //   440: iload           5
        //   442: istore          4
        //   444: aload_1        
        //   445: athrow         
        //   446: iload           5
        //   448: istore          4
        //   450: aload_0        
        //   451: invokevirtual   java/util/concurrent/ExecutionException.getCause:()Ljava/lang/Throwable;
        //   454: checkcast       Ljava/lang/RuntimeException;
        //   457: athrow         
        //   458: astore_0       
        //   459: iload           4
        //   461: ifeq            470
        //   464: invokestatic    java/lang/Thread.currentThread:()Ljava/lang/Thread;
        //   467: invokevirtual   java/lang/Thread.interrupt:()V
        //   470: aload_0        
        //   471: athrow         
        //   472: new             Ljava/lang/IllegalStateException;
        //   475: astore_2       
        //   476: new             Ljava/lang/StringBuilder;
        //   479: astore_0       
        //   480: aload_0        
        //   481: invokespecial   java/lang/StringBuilder.<init>:()V
        //   484: aload_0        
        //   485: aload_1        
        //   486: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //   489: pop            
        //   490: aload_0        
        //   491: ldc_w           " is shutting down"
        //   494: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   497: pop            
        //   498: aload_2        
        //   499: aload_0        
        //   500: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   503: invokespecial   java/lang/IllegalStateException.<init>:(Ljava/lang/String;)V
        //   506: aload_2        
        //   507: athrow         
        //   508: astore_0       
        //   509: aload           16
        //   511: monitorexit    
        //   512: aload_0        
        //   513: athrow         
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                                     
        //  -----  -----  -----  -----  -----------------------------------------
        //  67     79     508    514    Any
        //  84     96     508    514    Any
        //  99     104    508    514    Any
        //  104    147    508    514    Any
        //  150    157    508    514    Any
        //  162    178    379    383    Ljava/util/concurrent/ExecutionException;
        //  162    178    310    314    Ljava/util/concurrent/TimeoutException;
        //  162    178    303    310    Any
        //  185    213    265    303    Ljava/lang/InterruptedException;
        //  185    213    257    265    Ljava/util/concurrent/ExecutionException;
        //  185    213    249    257    Ljava/util/concurrent/TimeoutException;
        //  185    213    245    249    Any
        //  218    224    508    514    Any
        //  224    227    508    514    Any
        //  234    240    508    514    Any
        //  240    243    508    514    Any
        //  270    275    295    303    Ljava/util/concurrent/ExecutionException;
        //  270    275    291    295    Ljava/util/concurrent/TimeoutException;
        //  270    275    458    459    Any
        //  318    322    458    459    Any
        //  326    330    458    459    Any
        //  334    338    458    459    Any
        //  342    348    458    459    Any
        //  352    360    458    459    Any
        //  364    373    458    459    Any
        //  377    379    458    459    Any
        //  387    397    458    459    Any
        //  401    411    458    459    Any
        //  415    423    458    459    Any
        //  427    431    458    459    Any
        //  435    440    458    459    Any
        //  444    446    458    459    Any
        //  450    458    458    459    Any
        //  464    470    508    514    Any
        //  470    472    508    514    Any
        //  472    508    508    514    Any
        //  509    512    508    514    Any
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0314:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2604)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:206)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public static boolean registerGnssStatusCallback(final LocationManager locationManager, final GnssStatusCompat.Callback callback, final Handler handler) {
        if (Build$VERSION.SDK_INT >= 30) {
            return registerGnssStatusCallback(locationManager, ExecutorCompat.create(handler), callback);
        }
        return registerGnssStatusCallback(locationManager, new InlineHandlerExecutor(handler), callback);
    }
    
    public static boolean registerGnssStatusCallback(final LocationManager locationManager, final Executor executor, final GnssStatusCompat.Callback callback) {
        if (Build$VERSION.SDK_INT >= 30) {
            return registerGnssStatusCallback(locationManager, null, executor, callback);
        }
        Looper looper;
        if ((looper = Looper.myLooper()) == null) {
            looper = Looper.getMainLooper();
        }
        return registerGnssStatusCallback(locationManager, new Handler(looper), executor, callback);
    }
    
    static void registerLocationListenerTransport(final LocationManager locationManager, LocationListenerTransport referent) {
        final WeakReference<LocationListenerTransport> weakReference = LocationManagerCompat.sLocationListeners.put(referent.getKey(), new WeakReference<LocationListenerTransport>(referent));
        if (weakReference != null) {
            referent = weakReference.get();
        }
        else {
            referent = null;
        }
        if (referent != null) {
            referent.unregister();
            locationManager.removeUpdates((LocationListener)referent);
        }
    }
    
    public static void removeUpdates(final LocationManager locationManager, final LocationListenerCompat locationListenerCompat) {
        final WeakHashMap<LocationListenerKey, WeakReference<LocationListenerTransport>> sLocationListeners = LocationManagerCompat.sLocationListeners;
        monitorenter(sLocationListeners);
        ArrayList list = null;
        try {
            final Iterator<WeakReference<LocationListenerTransport>> iterator = sLocationListeners.values().iterator();
            while (iterator.hasNext()) {
                final LocationListenerTransport locationListenerTransport = iterator.next().get();
                if (locationListenerTransport == null) {
                    continue;
                }
                final LocationListenerKey key = locationListenerTransport.getKey();
                if (key.mListener != locationListenerCompat) {
                    continue;
                }
                ArrayList list2;
                if ((list2 = list) == null) {
                    list2 = new ArrayList();
                }
                list2.add(key);
                locationListenerTransport.unregister();
                locationManager.removeUpdates((LocationListener)locationListenerTransport);
                list = list2;
            }
            if (list != null) {
                final Iterator iterator2 = list.iterator();
                while (iterator2.hasNext()) {
                    LocationManagerCompat.sLocationListeners.remove(iterator2.next());
                }
            }
            monitorexit(sLocationListeners);
            locationManager.removeUpdates((LocationListener)locationListenerCompat);
        }
        finally {
            monitorexit(sLocationListeners);
        }
    }
    
    public static void requestLocationUpdates(final LocationManager locationManager, final String s, final LocationRequestCompat locationRequestCompat, final LocationListenerCompat locationListenerCompat, final Looper looper) {
        if (Build$VERSION.SDK_INT >= 31) {
            Api31Impl.requestLocationUpdates(locationManager, s, locationRequestCompat.toLocationRequest(), ExecutorCompat.create(new Handler(looper)), (LocationListener)locationListenerCompat);
            return;
        }
        if (Build$VERSION.SDK_INT >= 19 && Api19Impl.tryRequestLocationUpdates(locationManager, s, locationRequestCompat, locationListenerCompat, looper)) {
            return;
        }
        locationManager.requestLocationUpdates(s, locationRequestCompat.getIntervalMillis(), locationRequestCompat.getMinUpdateDistanceMeters(), (LocationListener)locationListenerCompat, looper);
    }
    
    public static void requestLocationUpdates(final LocationManager locationManager, final String s, final LocationRequestCompat locationRequestCompat, final Executor executor, final LocationListenerCompat locationListenerCompat) {
        if (Build$VERSION.SDK_INT >= 31) {
            Api31Impl.requestLocationUpdates(locationManager, s, locationRequestCompat.toLocationRequest(), executor, (LocationListener)locationListenerCompat);
            return;
        }
        if (Build$VERSION.SDK_INT >= 30 && Api30Impl.tryRequestLocationUpdates(locationManager, s, locationRequestCompat, executor, locationListenerCompat)) {
            return;
        }
        final LocationListenerTransport locationListenerTransport = new LocationListenerTransport(new LocationListenerKey(s, locationListenerCompat), executor);
        if (Build$VERSION.SDK_INT >= 19 && Api19Impl.tryRequestLocationUpdates(locationManager, s, locationRequestCompat, locationListenerTransport)) {
            return;
        }
        synchronized (LocationManagerCompat.sLocationListeners) {
            locationManager.requestLocationUpdates(s, locationRequestCompat.getIntervalMillis(), locationRequestCompat.getMinUpdateDistanceMeters(), (LocationListener)locationListenerTransport, Looper.getMainLooper());
            registerLocationListenerTransport(locationManager, locationListenerTransport);
        }
    }
    
    public static void unregisterGnssMeasurementsCallback(final LocationManager locationManager, GnssMeasurementsEvent$Callback gnssMeasurementsEvent$Callback) {
        if (Build$VERSION.SDK_INT >= 30) {
            Api24Impl.unregisterGnssMeasurementsCallback(locationManager, gnssMeasurementsEvent$Callback);
            return;
        }
        synchronized (GnssListenersHolder.sGnssMeasurementListeners) {
            gnssMeasurementsEvent$Callback = GnssListenersHolder.sGnssMeasurementListeners.remove(gnssMeasurementsEvent$Callback);
            if (gnssMeasurementsEvent$Callback != null) {
                if (gnssMeasurementsEvent$Callback instanceof GnssMeasurementsTransport) {
                    ((GnssMeasurementsTransport)gnssMeasurementsEvent$Callback).unregister();
                }
                Api24Impl.unregisterGnssMeasurementsCallback(locationManager, gnssMeasurementsEvent$Callback);
            }
        }
    }
    
    public static void unregisterGnssStatusCallback(final LocationManager locationManager, final GnssStatusCompat.Callback callback) {
        if (Build$VERSION.SDK_INT >= 24) {
            synchronized (GnssListenersHolder.sGnssStatusListeners) {
                final Object remove = GnssListenersHolder.sGnssStatusListeners.remove(callback);
                if (remove != null) {
                    Api24Impl.unregisterGnssStatusCallback(locationManager, remove);
                }
                return;
            }
        }
        synchronized (GnssListenersHolder.sGnssStatusListeners) {
            final GpsStatusTransport gpsStatusTransport = GnssListenersHolder.sGnssStatusListeners.remove(callback);
            if (gpsStatusTransport != null) {
                gpsStatusTransport.unregister();
                locationManager.removeGpsStatusListener((GpsStatus$Listener)gpsStatusTransport);
            }
        }
    }
    
    static class Api19Impl
    {
        private static Class<?> sLocationRequestClass;
        private static Method sRequestLocationUpdatesLooperMethod;
        
        private Api19Impl() {
        }
        
        static boolean tryRequestLocationUpdates(final LocationManager obj, final String s, final LocationRequestCompat locationRequestCompat, final LocationListenerCompat locationListenerCompat, final Looper looper) {
            if (Build$VERSION.SDK_INT < 19) {
                return false;
            }
            try {
                if (Api19Impl.sLocationRequestClass == null) {
                    Api19Impl.sLocationRequestClass = Class.forName("android.location.LocationRequest");
                }
                if (Api19Impl.sRequestLocationUpdatesLooperMethod == null) {
                    (Api19Impl.sRequestLocationUpdatesLooperMethod = LocationManager.class.getDeclaredMethod("requestLocationUpdates", Api19Impl.sLocationRequestClass, LocationListener.class, Looper.class)).setAccessible(true);
                }
                final LocationRequest locationRequest = locationRequestCompat.toLocationRequest(s);
                if (locationRequest != null) {
                    Api19Impl.sRequestLocationUpdatesLooperMethod.invoke(obj, locationRequest, locationListenerCompat, looper);
                    return true;
                }
                return false;
            }
            catch (final NoSuchMethodException | InvocationTargetException | IllegalAccessException | ClassNotFoundException | UnsupportedOperationException ex) {
                return false;
            }
        }
        
        static boolean tryRequestLocationUpdates(final LocationManager obj, final String s, final LocationRequestCompat locationRequestCompat, final LocationListenerTransport locationListenerTransport) {
            if (Build$VERSION.SDK_INT < 19) {
                return false;
            }
            try {
                if (Api19Impl.sLocationRequestClass == null) {
                    Api19Impl.sLocationRequestClass = Class.forName("android.location.LocationRequest");
                }
                if (Api19Impl.sRequestLocationUpdatesLooperMethod == null) {
                    (Api19Impl.sRequestLocationUpdatesLooperMethod = LocationManager.class.getDeclaredMethod("requestLocationUpdates", Api19Impl.sLocationRequestClass, LocationListener.class, Looper.class)).setAccessible(true);
                }
                final LocationRequest locationRequest = locationRequestCompat.toLocationRequest(s);
                if (locationRequest != null) {
                    synchronized (LocationManagerCompat.sLocationListeners) {
                        Api19Impl.sRequestLocationUpdatesLooperMethod.invoke(obj, locationRequest, locationListenerTransport, Looper.getMainLooper());
                        LocationManagerCompat.registerLocationListenerTransport(obj, locationListenerTransport);
                        return true;
                    }
                }
                return false;
            }
            catch (final NoSuchMethodException | InvocationTargetException | IllegalAccessException | ClassNotFoundException | UnsupportedOperationException ex) {
                return false;
            }
        }
    }
    
    static class Api24Impl
    {
        private Api24Impl() {
        }
        
        static boolean registerGnssMeasurementsCallback(final LocationManager locationManager, final GnssMeasurementsEvent$Callback gnssMeasurementsEvent$Callback) {
            return locationManager.registerGnssMeasurementsCallback(gnssMeasurementsEvent$Callback);
        }
        
        static boolean registerGnssMeasurementsCallback(final LocationManager locationManager, final GnssMeasurementsEvent$Callback gnssMeasurementsEvent$Callback, final Handler handler) {
            return locationManager.registerGnssMeasurementsCallback(gnssMeasurementsEvent$Callback, handler);
        }
        
        static boolean registerGnssStatusCallback(final LocationManager locationManager, final Handler handler, final Executor executor, final GnssStatusCompat.Callback callback) {
            Preconditions.checkArgument(handler != null);
            synchronized (GnssListenersHolder.sGnssStatusListeners) {
                PreRGnssStatusTransport preRGnssStatusTransport = GnssListenersHolder.sGnssStatusListeners.get(callback);
                if (preRGnssStatusTransport == null) {
                    preRGnssStatusTransport = new PreRGnssStatusTransport(callback);
                }
                else {
                    preRGnssStatusTransport.unregister();
                }
                preRGnssStatusTransport.register(executor);
                if (locationManager.registerGnssStatusCallback((GnssStatus$Callback)preRGnssStatusTransport, handler)) {
                    GnssListenersHolder.sGnssStatusListeners.put(callback, preRGnssStatusTransport);
                    return true;
                }
                return false;
            }
        }
        
        static void unregisterGnssMeasurementsCallback(final LocationManager locationManager, final GnssMeasurementsEvent$Callback gnssMeasurementsEvent$Callback) {
            locationManager.unregisterGnssMeasurementsCallback(gnssMeasurementsEvent$Callback);
        }
        
        static void unregisterGnssStatusCallback(final LocationManager locationManager, final Object o) {
            if (o instanceof PreRGnssStatusTransport) {
                ((PreRGnssStatusTransport)o).unregister();
            }
            locationManager.unregisterGnssStatusCallback((GnssStatus$Callback)o);
        }
    }
    
    private static class Api28Impl
    {
        static String getGnssHardwareModelName(final LocationManager locationManager) {
            return locationManager.getGnssHardwareModelName();
        }
        
        static int getGnssYearOfHardware(final LocationManager locationManager) {
            return locationManager.getGnssYearOfHardware();
        }
        
        static boolean isLocationEnabled(final LocationManager locationManager) {
            return locationManager.isLocationEnabled();
        }
    }
    
    private static class Api30Impl
    {
        private static Class<?> sLocationRequestClass;
        private static Method sRequestLocationUpdatesExecutorMethod;
        
        static void getCurrentLocation(final LocationManager locationManager, final String s, final CancellationSignal cancellationSignal, final Executor executor, final Consumer<Location> obj) {
            android.os.CancellationSignal cancellationSignal2;
            if (cancellationSignal != null) {
                cancellationSignal2 = (android.os.CancellationSignal)cancellationSignal.getCancellationSignalObject();
            }
            else {
                cancellationSignal2 = null;
            }
            Objects.requireNonNull(obj);
            locationManager.getCurrentLocation(s, cancellationSignal2, executor, (java.util.function.Consumer)new LocationManagerCompat$Api30Impl$$ExternalSyntheticLambda0(obj));
        }
        
        public static boolean registerGnssStatusCallback(final LocationManager locationManager, final Handler handler, final Executor executor, final GnssStatusCompat.Callback callback) {
            synchronized (GnssListenersHolder.sGnssStatusListeners) {
                GnssStatusTransport gnssStatusTransport;
                if ((gnssStatusTransport = GnssListenersHolder.sGnssStatusListeners.get(callback)) == null) {
                    gnssStatusTransport = new GnssStatusTransport(callback);
                }
                if (locationManager.registerGnssStatusCallback(executor, (GnssStatus$Callback)gnssStatusTransport)) {
                    GnssListenersHolder.sGnssStatusListeners.put(callback, gnssStatusTransport);
                    return true;
                }
                return false;
            }
        }
        
        public static boolean tryRequestLocationUpdates(final LocationManager obj, final String s, final LocationRequestCompat locationRequestCompat, final Executor executor, final LocationListenerCompat locationListenerCompat) {
            if (Build$VERSION.SDK_INT < 30) {
                return false;
            }
            try {
                if (Api30Impl.sLocationRequestClass == null) {
                    Api30Impl.sLocationRequestClass = Class.forName("android.location.LocationRequest");
                }
                if (Api30Impl.sRequestLocationUpdatesExecutorMethod == null) {
                    (Api30Impl.sRequestLocationUpdatesExecutorMethod = LocationManager.class.getDeclaredMethod("requestLocationUpdates", Api30Impl.sLocationRequestClass, Executor.class, LocationListener.class)).setAccessible(true);
                }
                final LocationRequest locationRequest = locationRequestCompat.toLocationRequest(s);
                if (locationRequest != null) {
                    Api30Impl.sRequestLocationUpdatesExecutorMethod.invoke(obj, locationRequest, executor, locationListenerCompat);
                    return true;
                }
                return false;
            }
            catch (final NoSuchMethodException | InvocationTargetException | IllegalAccessException | ClassNotFoundException | UnsupportedOperationException ex) {
                return false;
            }
        }
    }
    
    private static class Api31Impl
    {
        static boolean hasProvider(final LocationManager locationManager, final String s) {
            return locationManager.hasProvider(s);
        }
        
        static boolean registerGnssMeasurementsCallback(final LocationManager locationManager, final Executor executor, final GnssMeasurementsEvent$Callback gnssMeasurementsEvent$Callback) {
            return locationManager.registerGnssMeasurementsCallback(executor, gnssMeasurementsEvent$Callback);
        }
        
        static void requestLocationUpdates(final LocationManager locationManager, final String s, final LocationRequest locationRequest, final Executor executor, final LocationListener locationListener) {
            locationManager.requestLocationUpdates(s, locationRequest, executor, locationListener);
        }
    }
    
    private static final class CancellableLocationListener implements LocationListener
    {
        private Consumer<Location> mConsumer;
        private final Executor mExecutor;
        private final LocationManager mLocationManager;
        private final Handler mTimeoutHandler;
        Runnable mTimeoutRunnable;
        private boolean mTriggered;
        
        CancellableLocationListener(final LocationManager mLocationManager, final Executor mExecutor, final Consumer<Location> mConsumer) {
            this.mLocationManager = mLocationManager;
            this.mExecutor = mExecutor;
            this.mTimeoutHandler = new Handler(Looper.getMainLooper());
            this.mConsumer = mConsumer;
        }
        
        private void cleanup() {
            this.mConsumer = null;
            this.mLocationManager.removeUpdates((LocationListener)this);
            final Runnable mTimeoutRunnable = this.mTimeoutRunnable;
            if (mTimeoutRunnable != null) {
                this.mTimeoutHandler.removeCallbacks(mTimeoutRunnable);
                this.mTimeoutRunnable = null;
            }
        }
        
        public void cancel() {
            synchronized (this) {
                if (this.mTriggered) {
                    return;
                }
                this.mTriggered = true;
                monitorexit(this);
                this.cleanup();
            }
        }
        
        public void onLocationChanged(final Location location) {
            synchronized (this) {
                if (this.mTriggered) {
                    return;
                }
                this.mTriggered = true;
                monitorexit(this);
                this.mExecutor.execute(new LocationManagerCompat$CancellableLocationListener$$ExternalSyntheticLambda1(this.mConsumer, location));
                this.cleanup();
            }
        }
        
        public void onProviderDisabled(final String s) {
            final Location location = null;
            this.onLocationChanged(null);
        }
        
        public void onProviderEnabled(final String s) {
        }
        
        public void onStatusChanged(final String s, final int n, final Bundle bundle) {
        }
        
        public void startTimeout(final long n) {
            synchronized (this) {
                if (this.mTriggered) {
                    return;
                }
                final LocationManagerCompat$CancellableLocationListener$$ExternalSyntheticLambda0 mTimeoutRunnable = new LocationManagerCompat$CancellableLocationListener$$ExternalSyntheticLambda0(this);
                this.mTimeoutRunnable = mTimeoutRunnable;
                this.mTimeoutHandler.postDelayed((Runnable)mTimeoutRunnable, n);
            }
        }
    }
    
    private static class GnssListenersHolder
    {
        static final SimpleArrayMap<GnssMeasurementsEvent$Callback, GnssMeasurementsEvent$Callback> sGnssMeasurementListeners;
        static final SimpleArrayMap<Object, Object> sGnssStatusListeners;
        
        static {
            sGnssStatusListeners = new SimpleArrayMap<Object, Object>();
            sGnssMeasurementListeners = new SimpleArrayMap<GnssMeasurementsEvent$Callback, GnssMeasurementsEvent$Callback>();
        }
    }
    
    private static class GnssMeasurementsTransport extends GnssMeasurementsEvent$Callback
    {
        final GnssMeasurementsEvent$Callback mCallback;
        volatile Executor mExecutor;
        
        GnssMeasurementsTransport(final GnssMeasurementsEvent$Callback mCallback, final Executor mExecutor) {
            this.mCallback = mCallback;
            this.mExecutor = mExecutor;
        }
        
        public void onGnssMeasurementsReceived(final GnssMeasurementsEvent gnssMeasurementsEvent) {
            final Executor mExecutor = this.mExecutor;
            if (mExecutor == null) {
                return;
            }
            mExecutor.execute(new LocationManagerCompat$GnssMeasurementsTransport$$ExternalSyntheticLambda0(this, mExecutor, gnssMeasurementsEvent));
        }
        
        public void onStatusChanged(final int n) {
            final Executor mExecutor = this.mExecutor;
            if (mExecutor == null) {
                return;
            }
            mExecutor.execute(new LocationManagerCompat$GnssMeasurementsTransport$$ExternalSyntheticLambda1(this, mExecutor, n));
        }
        
        public void unregister() {
            this.mExecutor = null;
        }
    }
    
    private static class GnssStatusTransport extends GnssStatus$Callback
    {
        final GnssStatusCompat.Callback mCallback;
        
        GnssStatusTransport(final GnssStatusCompat.Callback mCallback) {
            Preconditions.checkArgument(mCallback != null, (Object)"invalid null callback");
            this.mCallback = mCallback;
        }
        
        public void onFirstFix(final int n) {
            this.mCallback.onFirstFix(n);
        }
        
        public void onSatelliteStatusChanged(final GnssStatus gnssStatus) {
            this.mCallback.onSatelliteStatusChanged(GnssStatusCompat.wrap(gnssStatus));
        }
        
        public void onStarted() {
            this.mCallback.onStarted();
        }
        
        public void onStopped() {
            this.mCallback.onStopped();
        }
    }
    
    private static class GpsStatusTransport implements GpsStatus$Listener
    {
        final GnssStatusCompat.Callback mCallback;
        volatile Executor mExecutor;
        private final LocationManager mLocationManager;
        
        GpsStatusTransport(final LocationManager mLocationManager, final GnssStatusCompat.Callback mCallback) {
            Preconditions.checkArgument(mCallback != null, (Object)"invalid null callback");
            this.mLocationManager = mLocationManager;
            this.mCallback = mCallback;
        }
        
        public void onGpsStatusChanged(final int n) {
            final Executor mExecutor = this.mExecutor;
            if (mExecutor == null) {
                return;
            }
            if (n != 1) {
                if (n != 2) {
                    if (n != 3) {
                        if (n == 4) {
                            final GpsStatus gpsStatus = this.mLocationManager.getGpsStatus((GpsStatus)null);
                            if (gpsStatus != null) {
                                mExecutor.execute(new LocationManagerCompat$GpsStatusTransport$$ExternalSyntheticLambda3(this, mExecutor, GnssStatusCompat.wrap(gpsStatus)));
                            }
                        }
                    }
                    else {
                        final GpsStatus gpsStatus2 = this.mLocationManager.getGpsStatus((GpsStatus)null);
                        if (gpsStatus2 != null) {
                            mExecutor.execute(new LocationManagerCompat$GpsStatusTransport$$ExternalSyntheticLambda2(this, mExecutor, gpsStatus2.getTimeToFirstFix()));
                        }
                    }
                }
                else {
                    mExecutor.execute(new LocationManagerCompat$GpsStatusTransport$$ExternalSyntheticLambda1(this, mExecutor));
                }
            }
            else {
                mExecutor.execute(new LocationManagerCompat$GpsStatusTransport$$ExternalSyntheticLambda0(this, mExecutor));
            }
        }
        
        public void register(final Executor mExecutor) {
            Preconditions.checkState(this.mExecutor == null);
            this.mExecutor = mExecutor;
        }
        
        public void unregister() {
            this.mExecutor = null;
        }
    }
    
    private static final class InlineHandlerExecutor implements Executor
    {
        private final Handler mHandler;
        
        InlineHandlerExecutor(final Handler handler) {
            this.mHandler = Preconditions.checkNotNull(handler);
        }
        
        @Override
        public void execute(final Runnable runnable) {
            if (Looper.myLooper() == this.mHandler.getLooper()) {
                runnable.run();
            }
            else if (!this.mHandler.post((Runnable)Preconditions.checkNotNull(runnable))) {
                final StringBuilder sb = new StringBuilder();
                sb.append(this.mHandler);
                sb.append(" is shutting down");
                throw new RejectedExecutionException(sb.toString());
            }
        }
    }
    
    private static class LocationListenerKey
    {
        final LocationListenerCompat mListener;
        final String mProvider;
        
        LocationListenerKey(final String s, final LocationListenerCompat locationListenerCompat) {
            this.mProvider = ObjectsCompat.requireNonNull(s, "invalid null provider");
            this.mListener = ObjectsCompat.requireNonNull(locationListenerCompat, "invalid null listener");
        }
        
        @Override
        public boolean equals(final Object o) {
            final boolean b = o instanceof LocationListenerKey;
            final boolean b2 = false;
            if (!b) {
                return false;
            }
            final LocationListenerKey locationListenerKey = (LocationListenerKey)o;
            boolean b3 = b2;
            if (this.mProvider.equals(locationListenerKey.mProvider)) {
                b3 = b2;
                if (this.mListener.equals(locationListenerKey.mListener)) {
                    b3 = true;
                }
            }
            return b3;
        }
        
        @Override
        public int hashCode() {
            return ObjectsCompat.hash(this.mProvider, this.mListener);
        }
    }
    
    private static class LocationListenerTransport implements LocationListener
    {
        final Executor mExecutor;
        volatile LocationListenerKey mKey;
        
        LocationListenerTransport(final LocationListenerKey mKey, final Executor mExecutor) {
            this.mKey = mKey;
            this.mExecutor = mExecutor;
        }
        
        public LocationListenerKey getKey() {
            return ObjectsCompat.requireNonNull(this.mKey);
        }
        
        public void onFlushComplete(final int n) {
            if (this.mKey == null) {
                return;
            }
            this.mExecutor.execute(new LocationManagerCompat$LocationListenerTransport$$ExternalSyntheticLambda1(this, n));
        }
        
        public void onLocationChanged(final Location location) {
            if (this.mKey == null) {
                return;
            }
            this.mExecutor.execute(new LocationManagerCompat$LocationListenerTransport$$ExternalSyntheticLambda4(this, location));
        }
        
        public void onLocationChanged(final List<Location> list) {
            if (this.mKey == null) {
                return;
            }
            this.mExecutor.execute(new LocationManagerCompat$LocationListenerTransport$$ExternalSyntheticLambda2(this, list));
        }
        
        public void onProviderDisabled(final String s) {
            if (this.mKey == null) {
                return;
            }
            this.mExecutor.execute(new LocationManagerCompat$LocationListenerTransport$$ExternalSyntheticLambda3(this, s));
        }
        
        public void onProviderEnabled(final String s) {
            if (this.mKey == null) {
                return;
            }
            this.mExecutor.execute(new LocationManagerCompat$LocationListenerTransport$$ExternalSyntheticLambda0(this, s));
        }
        
        public void onStatusChanged(final String s, final int n, final Bundle bundle) {
            if (this.mKey == null) {
                return;
            }
            this.mExecutor.execute(new LocationManagerCompat$LocationListenerTransport$$ExternalSyntheticLambda5(this, s, n, bundle));
        }
        
        public void unregister() {
            this.mKey = null;
        }
    }
    
    private static class PreRGnssStatusTransport extends GnssStatus$Callback
    {
        final GnssStatusCompat.Callback mCallback;
        volatile Executor mExecutor;
        
        PreRGnssStatusTransport(final GnssStatusCompat.Callback mCallback) {
            Preconditions.checkArgument(mCallback != null, (Object)"invalid null callback");
            this.mCallback = mCallback;
        }
        
        public void onFirstFix(final int n) {
            final Executor mExecutor = this.mExecutor;
            if (mExecutor == null) {
                return;
            }
            mExecutor.execute(new LocationManagerCompat$PreRGnssStatusTransport$$ExternalSyntheticLambda2(this, mExecutor, n));
        }
        
        public void onSatelliteStatusChanged(final GnssStatus gnssStatus) {
            final Executor mExecutor = this.mExecutor;
            if (mExecutor == null) {
                return;
            }
            mExecutor.execute(new LocationManagerCompat$PreRGnssStatusTransport$$ExternalSyntheticLambda1(this, mExecutor, gnssStatus));
        }
        
        public void onStarted() {
            final Executor mExecutor = this.mExecutor;
            if (mExecutor == null) {
                return;
            }
            mExecutor.execute(new LocationManagerCompat$PreRGnssStatusTransport$$ExternalSyntheticLambda0(this, mExecutor));
        }
        
        public void onStopped() {
            final Executor mExecutor = this.mExecutor;
            if (mExecutor == null) {
                return;
            }
            mExecutor.execute(new LocationManagerCompat$PreRGnssStatusTransport$$ExternalSyntheticLambda3(this, mExecutor));
        }
        
        public void register(final Executor mExecutor) {
            final boolean b = true;
            Preconditions.checkArgument(mExecutor != null, (Object)"invalid null executor");
            Preconditions.checkState(this.mExecutor == null && b);
            this.mExecutor = mExecutor;
        }
        
        public void unregister() {
            this.mExecutor = null;
        }
    }
}

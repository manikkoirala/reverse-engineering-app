// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.util;

import java.util.Objects;
import java.util.Arrays;
import android.os.Build$VERSION;

public class ObjectsCompat
{
    private ObjectsCompat() {
    }
    
    public static boolean equals(final Object o, final Object obj) {
        if (Build$VERSION.SDK_INT >= 19) {
            return Api19Impl.equals(o, obj);
        }
        return o == obj || (o != null && o.equals(obj));
    }
    
    public static int hash(final Object... a) {
        if (Build$VERSION.SDK_INT >= 19) {
            return Api19Impl.hash(a);
        }
        return Arrays.hashCode(a);
    }
    
    public static int hashCode(final Object o) {
        int hashCode;
        if (o != null) {
            hashCode = o.hashCode();
        }
        else {
            hashCode = 0;
        }
        return hashCode;
    }
    
    public static <T> T requireNonNull(final T t) {
        t.getClass();
        return t;
    }
    
    public static <T> T requireNonNull(final T t, final String s) {
        if (t != null) {
            return t;
        }
        throw new NullPointerException(s);
    }
    
    public static String toString(final Object o, String string) {
        if (o != null) {
            string = o.toString();
        }
        return string;
    }
    
    static class Api19Impl
    {
        private Api19Impl() {
        }
        
        static boolean equals(final Object a, final Object b) {
            return Objects.equals(a, b);
        }
        
        static int hash(final Object... values) {
            return Objects.hash(values);
        }
    }
}

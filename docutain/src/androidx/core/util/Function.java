// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.util;

@FunctionalInterface
public interface Function<T, R>
{
    R apply(final T p0);
}

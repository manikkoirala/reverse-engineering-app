// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.util;

import java.util.Iterator;
import kotlin.jvm.internal.Intrinsics;
import kotlin.collections.IntIterator;
import kotlin.jvm.functions.Function0;
import kotlin.Unit;
import kotlin.jvm.functions.Function2;
import android.util.SparseArray;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000@\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0006\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0010(\n\u0000\u001a!\u0010\u0006\u001a\u00020\u0007\"\u0004\b\u0000\u0010\u0002*\b\u0012\u0004\u0012\u0002H\u00020\u00032\u0006\u0010\b\u001a\u00020\u0001H\u0086\n\u001a!\u0010\t\u001a\u00020\u0007\"\u0004\b\u0000\u0010\u0002*\b\u0012\u0004\u0012\u0002H\u00020\u00032\u0006\u0010\b\u001a\u00020\u0001H\u0086\b\u001a&\u0010\n\u001a\u00020\u0007\"\u0004\b\u0000\u0010\u0002*\b\u0012\u0004\u0012\u0002H\u00020\u00032\u0006\u0010\u000b\u001a\u0002H\u0002H\u0086\b¢\u0006\u0002\u0010\f\u001aQ\u0010\r\u001a\u00020\u000e\"\u0004\b\u0000\u0010\u0002*\b\u0012\u0004\u0012\u0002H\u00020\u000326\u0010\u000f\u001a2\u0012\u0013\u0012\u00110\u0001¢\u0006\f\b\u0011\u0012\b\b\u0012\u0012\u0004\b\b(\b\u0012\u0013\u0012\u0011H\u0002¢\u0006\f\b\u0011\u0012\b\b\u0012\u0012\u0004\b\b(\u000b\u0012\u0004\u0012\u00020\u000e0\u0010H\u0086\b\u001a.\u0010\u0013\u001a\u0002H\u0002\"\u0004\b\u0000\u0010\u0002*\b\u0012\u0004\u0012\u0002H\u00020\u00032\u0006\u0010\b\u001a\u00020\u00012\u0006\u0010\u0014\u001a\u0002H\u0002H\u0086\b¢\u0006\u0002\u0010\u0015\u001a4\u0010\u0016\u001a\u0002H\u0002\"\u0004\b\u0000\u0010\u0002*\b\u0012\u0004\u0012\u0002H\u00020\u00032\u0006\u0010\b\u001a\u00020\u00012\f\u0010\u0014\u001a\b\u0012\u0004\u0012\u0002H\u00020\u0017H\u0086\b¢\u0006\u0002\u0010\u0018\u001a\u0019\u0010\u0019\u001a\u00020\u0007\"\u0004\b\u0000\u0010\u0002*\b\u0012\u0004\u0012\u0002H\u00020\u0003H\u0086\b\u001a\u0019\u0010\u001a\u001a\u00020\u0007\"\u0004\b\u0000\u0010\u0002*\b\u0012\u0004\u0012\u0002H\u00020\u0003H\u0086\b\u001a\u0016\u0010\u001b\u001a\u00020\u001c\"\u0004\b\u0000\u0010\u0002*\b\u0012\u0004\u0012\u0002H\u00020\u0003\u001a-\u0010\u001d\u001a\b\u0012\u0004\u0012\u0002H\u00020\u0003\"\u0004\b\u0000\u0010\u0002*\b\u0012\u0004\u0012\u0002H\u00020\u00032\f\u0010\u001e\u001a\b\u0012\u0004\u0012\u0002H\u00020\u0003H\u0086\u0002\u001a$\u0010\u001f\u001a\u00020\u000e\"\u0004\b\u0000\u0010\u0002*\b\u0012\u0004\u0012\u0002H\u00020\u00032\f\u0010\u001e\u001a\b\u0012\u0004\u0012\u0002H\u00020\u0003\u001a+\u0010 \u001a\u00020\u0007\"\u0004\b\u0000\u0010\u0002*\b\u0012\u0004\u0012\u0002H\u00020\u00032\u0006\u0010\b\u001a\u00020\u00012\u0006\u0010\u000b\u001a\u0002H\u0002¢\u0006\u0002\u0010!\u001a.\u0010\"\u001a\u00020\u000e\"\u0004\b\u0000\u0010\u0002*\b\u0012\u0004\u0012\u0002H\u00020\u00032\u0006\u0010\b\u001a\u00020\u00012\u0006\u0010\u000b\u001a\u0002H\u0002H\u0086\n¢\u0006\u0002\u0010#\u001a\u001c\u0010$\u001a\b\u0012\u0004\u0012\u0002H\u00020%\"\u0004\b\u0000\u0010\u0002*\b\u0012\u0004\u0012\u0002H\u00020\u0003\"\"\u0010\u0000\u001a\u00020\u0001\"\u0004\b\u0000\u0010\u0002*\b\u0012\u0004\u0012\u0002H\u00020\u00038\u00c6\u0002¢\u0006\u0006\u001a\u0004\b\u0004\u0010\u0005¨\u0006&" }, d2 = { "size", "", "T", "Landroid/util/SparseArray;", "getSize", "(Landroid/util/SparseArray;)I", "contains", "", "key", "containsKey", "containsValue", "value", "(Landroid/util/SparseArray;Ljava/lang/Object;)Z", "forEach", "", "action", "Lkotlin/Function2;", "Lkotlin/ParameterName;", "name", "getOrDefault", "defaultValue", "(Landroid/util/SparseArray;ILjava/lang/Object;)Ljava/lang/Object;", "getOrElse", "Lkotlin/Function0;", "(Landroid/util/SparseArray;ILkotlin/jvm/functions/Function0;)Ljava/lang/Object;", "isEmpty", "isNotEmpty", "keyIterator", "Lkotlin/collections/IntIterator;", "plus", "other", "putAll", "remove", "(Landroid/util/SparseArray;ILjava/lang/Object;)Z", "set", "(Landroid/util/SparseArray;ILjava/lang/Object;)V", "valueIterator", "", "core-ktx_release" }, k = 2, mv = { 1, 8, 0 }, xi = 48)
public final class SparseArrayKt
{
    public static final <T> boolean contains(final SparseArray<T> sparseArray, final int n) {
        return sparseArray.indexOfKey(n) >= 0;
    }
    
    public static final <T> boolean containsKey(final SparseArray<T> sparseArray, final int n) {
        return sparseArray.indexOfKey(n) >= 0;
    }
    
    public static final <T> boolean containsValue(final SparseArray<T> sparseArray, final T t) {
        return sparseArray.indexOfValue((Object)t) >= 0;
    }
    
    public static final <T> void forEach(final SparseArray<T> sparseArray, final Function2<? super Integer, ? super T, Unit> function2) {
        for (int size = sparseArray.size(), i = 0; i < size; ++i) {
            function2.invoke((Object)sparseArray.keyAt(i), sparseArray.valueAt(i));
        }
    }
    
    public static final <T> T getOrDefault(final SparseArray<T> sparseArray, final int n, final T t) {
        Object value = sparseArray.get(n);
        if (value == null) {
            value = t;
        }
        return (T)value;
    }
    
    public static final <T> T getOrElse(final SparseArray<T> sparseArray, final int n, final Function0<? extends T> function0) {
        Object o;
        if ((o = sparseArray.get(n)) == null) {
            o = function0.invoke();
        }
        return (T)o;
    }
    
    public static final <T> int getSize(final SparseArray<T> sparseArray) {
        return sparseArray.size();
    }
    
    public static final <T> boolean isEmpty(final SparseArray<T> sparseArray) {
        return sparseArray.size() == 0;
    }
    
    public static final <T> boolean isNotEmpty(final SparseArray<T> sparseArray) {
        return sparseArray.size() != 0;
    }
    
    public static final <T> IntIterator keyIterator(final SparseArray<T> sparseArray) {
        return (IntIterator)new SparseArrayKt$keyIterator.SparseArrayKt$keyIterator$1((SparseArray)sparseArray);
    }
    
    public static final <T> SparseArray<T> plus(final SparseArray<T> sparseArray, final SparseArray<T> sparseArray2) {
        final SparseArray sparseArray3 = new SparseArray(sparseArray.size() + sparseArray2.size());
        putAll((android.util.SparseArray<Object>)sparseArray3, (android.util.SparseArray<Object>)sparseArray);
        putAll((android.util.SparseArray<Object>)sparseArray3, (android.util.SparseArray<Object>)sparseArray2);
        return (SparseArray<T>)sparseArray3;
    }
    
    public static final <T> void putAll(final SparseArray<T> sparseArray, final SparseArray<T> sparseArray2) {
        for (int size = sparseArray2.size(), i = 0; i < size; ++i) {
            sparseArray.put(sparseArray2.keyAt(i), sparseArray2.valueAt(i));
        }
    }
    
    public static final <T> boolean remove(final SparseArray<T> sparseArray, int indexOfKey, final T t) {
        indexOfKey = sparseArray.indexOfKey(indexOfKey);
        if (indexOfKey >= 0 && Intrinsics.areEqual((Object)t, sparseArray.valueAt(indexOfKey))) {
            sparseArray.removeAt(indexOfKey);
            return true;
        }
        return false;
    }
    
    public static final <T> void set(final SparseArray<T> sparseArray, final int n, final T t) {
        sparseArray.put(n, (Object)t);
    }
    
    public static final <T> Iterator<T> valueIterator(final SparseArray<T> sparseArray) {
        return (Iterator<T>)new SparseArrayKt$valueIterator.SparseArrayKt$valueIterator$1((SparseArray)sparseArray);
    }
}

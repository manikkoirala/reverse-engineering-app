// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.util;

import kotlin.Result$Companion;
import kotlin.Result;
import kotlin.coroutines.Continuation;
import kotlin.Metadata;
import java.util.function.Consumer;
import java.util.concurrent.atomic.AtomicBoolean;

@Metadata(d1 = { "\u0000&\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0000\b\u0003\u0018\u0000*\u0004\b\u0000\u0010\u00012\b\u0012\u0004\u0012\u0002H\u00010\u00022\u00020\u0003B\u0013\u0012\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00028\u00000\u0005¢\u0006\u0002\u0010\u0006J\u0015\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00028\u0000H\u0016¢\u0006\u0002\u0010\nJ\b\u0010\u000b\u001a\u00020\fH\u0016R\u0014\u0010\u0004\u001a\b\u0012\u0004\u0012\u00028\u00000\u0005X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\r" }, d2 = { "Landroidx/core/util/ContinuationConsumer;", "T", "Ljava/util/function/Consumer;", "Ljava/util/concurrent/atomic/AtomicBoolean;", "continuation", "Lkotlin/coroutines/Continuation;", "(Lkotlin/coroutines/Continuation;)V", "accept", "", "value", "(Ljava/lang/Object;)V", "toString", "", "core-ktx_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
final class ContinuationConsumer<T> extends AtomicBoolean implements Consumer<T>
{
    private final Continuation<T> continuation;
    
    public ContinuationConsumer(final Continuation<? super T> continuation) {
        super(false);
        this.continuation = (Continuation<T>)continuation;
    }
    
    @Override
    public void accept(final T t) {
        if (this.compareAndSet(false, true)) {
            final Continuation<T> continuation = this.continuation;
            final Result$Companion companion = Result.Companion;
            continuation.resumeWith(Result.constructor-impl((Object)t));
        }
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("ContinuationConsumer(resultAccepted = ");
        sb.append(this.get());
        sb.append(')');
        return sb.toString();
    }
}

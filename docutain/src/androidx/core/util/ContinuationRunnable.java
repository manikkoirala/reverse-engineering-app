// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.util;

import kotlin.Result$Companion;
import kotlin.Result;
import kotlin.Unit;
import kotlin.coroutines.Continuation;
import kotlin.Metadata;
import java.util.concurrent.atomic.AtomicBoolean;

@Metadata(d1 = { "\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0000\b\u0002\u0018\u00002\u00020\u00012\u00020\u0002B\u0013\u0012\f\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004¢\u0006\u0002\u0010\u0006J\b\u0010\u0007\u001a\u00020\u0005H\u0016J\b\u0010\b\u001a\u00020\tH\u0016R\u0014\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\n" }, d2 = { "Landroidx/core/util/ContinuationRunnable;", "Ljava/lang/Runnable;", "Ljava/util/concurrent/atomic/AtomicBoolean;", "continuation", "Lkotlin/coroutines/Continuation;", "", "(Lkotlin/coroutines/Continuation;)V", "run", "toString", "", "core-ktx_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
final class ContinuationRunnable extends AtomicBoolean implements Runnable
{
    private final Continuation<Unit> continuation;
    
    public ContinuationRunnable(final Continuation<? super Unit> continuation) {
        super(false);
        this.continuation = (Continuation<Unit>)continuation;
    }
    
    @Override
    public void run() {
        if (this.compareAndSet(false, true)) {
            final Continuation<Unit> continuation = this.continuation;
            final Result$Companion companion = Result.Companion;
            continuation.resumeWith(Result.constructor-impl((Object)Unit.INSTANCE));
        }
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("ContinuationRunnable(ran = ");
        sb.append(this.get());
        sb.append(')');
        return sb.toString();
    }
}

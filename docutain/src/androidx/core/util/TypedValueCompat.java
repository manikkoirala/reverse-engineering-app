// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.util;

import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;
import android.util.TypedValue;
import android.os.Build$VERSION;
import android.util.DisplayMetrics;

public class TypedValueCompat
{
    private static final float INCHES_PER_MM = 0.03937008f;
    private static final float INCHES_PER_PT = 0.013888889f;
    
    private TypedValueCompat() {
    }
    
    public static float deriveDimension(final int i, float n, final DisplayMetrics displayMetrics) {
        if (Build$VERSION.SDK_INT >= 34) {
            return Api34Impl.deriveDimension(i, n, displayMetrics);
        }
        if (i != 0) {
            float n2;
            if (i != 1) {
                if (i != 2) {
                    if (i != 3) {
                        if (i != 4) {
                            if (i != 5) {
                                final StringBuilder sb = new StringBuilder();
                                sb.append("Invalid unitToConvertTo ");
                                sb.append(i);
                                throw new IllegalArgumentException(sb.toString());
                            }
                            if (displayMetrics.xdpi == 0.0f) {
                                return 0.0f;
                            }
                            n /= displayMetrics.xdpi;
                            n2 = 0.03937008f;
                        }
                        else {
                            if (displayMetrics.xdpi == 0.0f) {
                                return 0.0f;
                            }
                            n2 = displayMetrics.xdpi;
                        }
                    }
                    else {
                        if (displayMetrics.xdpi == 0.0f) {
                            return 0.0f;
                        }
                        n /= displayMetrics.xdpi;
                        n2 = 0.013888889f;
                    }
                }
                else {
                    if (displayMetrics.scaledDensity == 0.0f) {
                        return 0.0f;
                    }
                    n2 = displayMetrics.scaledDensity;
                }
            }
            else {
                if (displayMetrics.density == 0.0f) {
                    return 0.0f;
                }
                n2 = displayMetrics.density;
            }
            return n / n2;
        }
        return n;
    }
    
    public static float dpToPx(final float n, final DisplayMetrics displayMetrics) {
        return TypedValue.applyDimension(1, n, displayMetrics);
    }
    
    public static int getUnitFromComplexDimension(final int n) {
        return n >> 0 & 0xF;
    }
    
    public static float pxToDp(final float n, final DisplayMetrics displayMetrics) {
        return deriveDimension(1, n, displayMetrics);
    }
    
    public static float pxToSp(final float n, final DisplayMetrics displayMetrics) {
        return deriveDimension(2, n, displayMetrics);
    }
    
    public static float spToPx(final float n, final DisplayMetrics displayMetrics) {
        return TypedValue.applyDimension(2, n, displayMetrics);
    }
    
    private static class Api34Impl
    {
        public static float deriveDimension(final int n, final float n2, final DisplayMetrics displayMetrics) {
            return TypedValue.deriveDimension(n, n2, displayMetrics);
        }
    }
    
    @Retention(RetentionPolicy.SOURCE)
    public @interface ComplexDimensionUnit {
    }
}

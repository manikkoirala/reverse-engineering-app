// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.util;

import kotlin.collections.BooleanIterator;
import kotlin.collections.IntIterator;
import kotlin.jvm.functions.Function0;
import kotlin.Unit;
import kotlin.jvm.functions.Function2;
import android.util.SparseBooleanArray;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000>\n\u0000\n\u0002\u0010\b\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0000\u001a\u0015\u0010\u0005\u001a\u00020\u0006*\u00020\u00022\u0006\u0010\u0007\u001a\u00020\u0001H\u0086\n\u001a\u0015\u0010\b\u001a\u00020\u0006*\u00020\u00022\u0006\u0010\u0007\u001a\u00020\u0001H\u0086\b\u001a\u0015\u0010\t\u001a\u00020\u0006*\u00020\u00022\u0006\u0010\n\u001a\u00020\u0006H\u0086\b\u001aE\u0010\u000b\u001a\u00020\f*\u00020\u000226\u0010\r\u001a2\u0012\u0013\u0012\u00110\u0001¢\u0006\f\b\u000f\u0012\b\b\u0010\u0012\u0004\b\b(\u0007\u0012\u0013\u0012\u00110\u0006¢\u0006\f\b\u000f\u0012\b\b\u0010\u0012\u0004\b\b(\n\u0012\u0004\u0012\u00020\f0\u000eH\u0086\b\u001a\u001d\u0010\u0011\u001a\u00020\u0006*\u00020\u00022\u0006\u0010\u0007\u001a\u00020\u00012\u0006\u0010\u0012\u001a\u00020\u0006H\u0086\b\u001a#\u0010\u0013\u001a\u00020\u0006*\u00020\u00022\u0006\u0010\u0007\u001a\u00020\u00012\f\u0010\u0012\u001a\b\u0012\u0004\u0012\u00020\u00060\u0014H\u0086\b\u001a\r\u0010\u0015\u001a\u00020\u0006*\u00020\u0002H\u0086\b\u001a\r\u0010\u0016\u001a\u00020\u0006*\u00020\u0002H\u0086\b\u001a\n\u0010\u0017\u001a\u00020\u0018*\u00020\u0002\u001a\u0015\u0010\u0019\u001a\u00020\u0002*\u00020\u00022\u0006\u0010\u001a\u001a\u00020\u0002H\u0086\u0002\u001a\u0012\u0010\u001b\u001a\u00020\f*\u00020\u00022\u0006\u0010\u001a\u001a\u00020\u0002\u001a\u001a\u0010\u001c\u001a\u00020\u0006*\u00020\u00022\u0006\u0010\u0007\u001a\u00020\u00012\u0006\u0010\n\u001a\u00020\u0006\u001a\u001d\u0010\u001d\u001a\u00020\f*\u00020\u00022\u0006\u0010\u0007\u001a\u00020\u00012\u0006\u0010\n\u001a\u00020\u0006H\u0086\n\u001a\n\u0010\u001e\u001a\u00020\u001f*\u00020\u0002\"\u0016\u0010\u0000\u001a\u00020\u0001*\u00020\u00028\u00c6\u0002¢\u0006\u0006\u001a\u0004\b\u0003\u0010\u0004¨\u0006 " }, d2 = { "size", "", "Landroid/util/SparseBooleanArray;", "getSize", "(Landroid/util/SparseBooleanArray;)I", "contains", "", "key", "containsKey", "containsValue", "value", "forEach", "", "action", "Lkotlin/Function2;", "Lkotlin/ParameterName;", "name", "getOrDefault", "defaultValue", "getOrElse", "Lkotlin/Function0;", "isEmpty", "isNotEmpty", "keyIterator", "Lkotlin/collections/IntIterator;", "plus", "other", "putAll", "remove", "set", "valueIterator", "Lkotlin/collections/BooleanIterator;", "core-ktx_release" }, k = 2, mv = { 1, 8, 0 }, xi = 48)
public final class SparseBooleanArrayKt
{
    public static final boolean contains(final SparseBooleanArray sparseBooleanArray, final int n) {
        return sparseBooleanArray.indexOfKey(n) >= 0;
    }
    
    public static final boolean containsKey(final SparseBooleanArray sparseBooleanArray, final int n) {
        return sparseBooleanArray.indexOfKey(n) >= 0;
    }
    
    public static final boolean containsValue(final SparseBooleanArray sparseBooleanArray, final boolean b) {
        return sparseBooleanArray.indexOfValue(b) >= 0;
    }
    
    public static final void forEach(final SparseBooleanArray sparseBooleanArray, final Function2<? super Integer, ? super Boolean, Unit> function2) {
        for (int size = sparseBooleanArray.size(), i = 0; i < size; ++i) {
            function2.invoke((Object)sparseBooleanArray.keyAt(i), (Object)sparseBooleanArray.valueAt(i));
        }
    }
    
    public static final boolean getOrDefault(final SparseBooleanArray sparseBooleanArray, final int n, final boolean b) {
        return sparseBooleanArray.get(n, b);
    }
    
    public static final boolean getOrElse(final SparseBooleanArray sparseBooleanArray, int indexOfKey, final Function0<Boolean> function0) {
        indexOfKey = sparseBooleanArray.indexOfKey(indexOfKey);
        boolean b;
        if (indexOfKey >= 0) {
            b = sparseBooleanArray.valueAt(indexOfKey);
        }
        else {
            b = (boolean)function0.invoke();
        }
        return b;
    }
    
    public static final int getSize(final SparseBooleanArray sparseBooleanArray) {
        return sparseBooleanArray.size();
    }
    
    public static final boolean isEmpty(final SparseBooleanArray sparseBooleanArray) {
        return sparseBooleanArray.size() == 0;
    }
    
    public static final boolean isNotEmpty(final SparseBooleanArray sparseBooleanArray) {
        return sparseBooleanArray.size() != 0;
    }
    
    public static final IntIterator keyIterator(final SparseBooleanArray sparseBooleanArray) {
        return (IntIterator)new SparseBooleanArrayKt$keyIterator.SparseBooleanArrayKt$keyIterator$1(sparseBooleanArray);
    }
    
    public static final SparseBooleanArray plus(final SparseBooleanArray sparseBooleanArray, final SparseBooleanArray sparseBooleanArray2) {
        final SparseBooleanArray sparseBooleanArray3 = new SparseBooleanArray(sparseBooleanArray.size() + sparseBooleanArray2.size());
        putAll(sparseBooleanArray3, sparseBooleanArray);
        putAll(sparseBooleanArray3, sparseBooleanArray2);
        return sparseBooleanArray3;
    }
    
    public static final void putAll(final SparseBooleanArray sparseBooleanArray, final SparseBooleanArray sparseBooleanArray2) {
        for (int size = sparseBooleanArray2.size(), i = 0; i < size; ++i) {
            sparseBooleanArray.put(sparseBooleanArray2.keyAt(i), sparseBooleanArray2.valueAt(i));
        }
    }
    
    public static final boolean remove(final SparseBooleanArray sparseBooleanArray, final int n, final boolean b) {
        final int indexOfKey = sparseBooleanArray.indexOfKey(n);
        if (indexOfKey >= 0 && b == sparseBooleanArray.valueAt(indexOfKey)) {
            sparseBooleanArray.delete(n);
            return true;
        }
        return false;
    }
    
    public static final void set(final SparseBooleanArray sparseBooleanArray, final int n, final boolean b) {
        sparseBooleanArray.put(n, b);
    }
    
    public static final BooleanIterator valueIterator(final SparseBooleanArray sparseBooleanArray) {
        return (BooleanIterator)new SparseBooleanArrayKt$valueIterator.SparseBooleanArrayKt$valueIterator$1(sparseBooleanArray);
    }
}

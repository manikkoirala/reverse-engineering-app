// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.util;

import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.InlineMarker;
import kotlin.Unit;
import java.io.FileOutputStream;
import kotlin.jvm.functions.Function1;
import kotlin.text.Charsets;
import java.nio.charset.Charset;
import android.util.AtomicFile;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000.\n\u0000\n\u0002\u0010\u0012\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\u001a\r\u0010\u0000\u001a\u00020\u0001*\u00020\u0002H\u0087\b\u001a\u0016\u0010\u0003\u001a\u00020\u0004*\u00020\u00022\b\b\u0002\u0010\u0005\u001a\u00020\u0006H\u0007\u001a0\u0010\u0007\u001a\u00020\b*\u00020\u00022!\u0010\t\u001a\u001d\u0012\u0013\u0012\u00110\u000b¢\u0006\f\b\f\u0012\b\b\r\u0012\u0004\b\b(\u000e\u0012\u0004\u0012\u00020\b0\nH\u0087\b\u001a\u0014\u0010\u000f\u001a\u00020\b*\u00020\u00022\u0006\u0010\u0010\u001a\u00020\u0001H\u0007\u001a\u001e\u0010\u0011\u001a\u00020\b*\u00020\u00022\u0006\u0010\u0012\u001a\u00020\u00042\b\b\u0002\u0010\u0005\u001a\u00020\u0006H\u0007¨\u0006\u0013" }, d2 = { "readBytes", "", "Landroid/util/AtomicFile;", "readText", "", "charset", "Ljava/nio/charset/Charset;", "tryWrite", "", "block", "Lkotlin/Function1;", "Ljava/io/FileOutputStream;", "Lkotlin/ParameterName;", "name", "out", "writeBytes", "array", "writeText", "text", "core-ktx_release" }, k = 2, mv = { 1, 8, 0 }, xi = 48)
public final class AtomicFileKt
{
    public static final byte[] readBytes(final AtomicFile atomicFile) {
        return atomicFile.readFully();
    }
    
    public static final String readText(final AtomicFile atomicFile, final Charset charset) {
        return new String(atomicFile.readFully(), charset);
    }
    
    public static final void tryWrite(final AtomicFile atomicFile, final Function1<? super FileOutputStream, Unit> function1) {
        final FileOutputStream startWrite = atomicFile.startWrite();
        try {
            function1.invoke((Object)startWrite);
            InlineMarker.finallyStart(1);
            atomicFile.finishWrite(startWrite);
            InlineMarker.finallyEnd(1);
        }
        finally {
            InlineMarker.finallyStart(1);
            atomicFile.failWrite(startWrite);
            InlineMarker.finallyEnd(1);
        }
    }
    
    public static final void writeBytes(final AtomicFile atomicFile, final byte[] b) {
        final FileOutputStream startWrite = atomicFile.startWrite();
        try {
            startWrite.write(b);
            atomicFile.finishWrite(startWrite);
        }
        finally {
            atomicFile.failWrite(startWrite);
        }
    }
    
    public static final void writeText(final AtomicFile atomicFile, final String s, final Charset charset) {
        final byte[] bytes = s.getBytes(charset);
        Intrinsics.checkNotNullExpressionValue((Object)bytes, "this as java.lang.String).getBytes(charset)");
        writeBytes(atomicFile, bytes);
    }
}

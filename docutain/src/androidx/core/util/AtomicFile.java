// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.util;

import java.io.FileNotFoundException;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.FileOutputStream;
import android.util.Log;
import java.io.File;

public class AtomicFile
{
    private static final String LOG_TAG = "AtomicFile";
    private final File mBaseName;
    private final File mLegacyBackupName;
    private final File mNewName;
    
    public AtomicFile(final File mBaseName) {
        this.mBaseName = mBaseName;
        final StringBuilder sb = new StringBuilder();
        sb.append(mBaseName.getPath());
        sb.append(".new");
        this.mNewName = new File(sb.toString());
        final StringBuilder sb2 = new StringBuilder();
        sb2.append(mBaseName.getPath());
        sb2.append(".bak");
        this.mLegacyBackupName = new File(sb2.toString());
    }
    
    private static void rename(final File obj, final File obj2) {
        if (obj2.isDirectory() && !obj2.delete()) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Failed to delete file which is a directory ");
            sb.append(obj2);
            Log.e("AtomicFile", sb.toString());
        }
        if (!obj.renameTo(obj2)) {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("Failed to rename ");
            sb2.append(obj);
            sb2.append(" to ");
            sb2.append(obj2);
            Log.e("AtomicFile", sb2.toString());
        }
    }
    
    private static boolean sync(final FileOutputStream fileOutputStream) {
        try {
            fileOutputStream.getFD().sync();
            return true;
        }
        catch (final IOException ex) {
            return false;
        }
    }
    
    public void delete() {
        this.mBaseName.delete();
        this.mNewName.delete();
        this.mLegacyBackupName.delete();
    }
    
    public void failWrite(final FileOutputStream fileOutputStream) {
        if (fileOutputStream == null) {
            return;
        }
        if (!sync(fileOutputStream)) {
            Log.e("AtomicFile", "Failed to sync file output stream");
        }
        try {
            fileOutputStream.close();
        }
        catch (final IOException ex) {
            Log.e("AtomicFile", "Failed to close file output stream", (Throwable)ex);
        }
        if (!this.mNewName.delete()) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Failed to delete new file ");
            sb.append(this.mNewName);
            Log.e("AtomicFile", sb.toString());
        }
    }
    
    public void finishWrite(final FileOutputStream fileOutputStream) {
        if (fileOutputStream == null) {
            return;
        }
        if (!sync(fileOutputStream)) {
            Log.e("AtomicFile", "Failed to sync file output stream");
        }
        try {
            fileOutputStream.close();
        }
        catch (final IOException ex) {
            Log.e("AtomicFile", "Failed to close file output stream", (Throwable)ex);
        }
        rename(this.mNewName, this.mBaseName);
    }
    
    public File getBaseFile() {
        return this.mBaseName;
    }
    
    public FileInputStream openRead() throws FileNotFoundException {
        if (this.mLegacyBackupName.exists()) {
            rename(this.mLegacyBackupName, this.mBaseName);
        }
        if (this.mNewName.exists() && this.mBaseName.exists() && !this.mNewName.delete()) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Failed to delete outdated new file ");
            sb.append(this.mNewName);
            Log.e("AtomicFile", sb.toString());
        }
        return new FileInputStream(this.mBaseName);
    }
    
    public byte[] readFully() throws IOException {
        final FileInputStream openRead = this.openRead();
        try {
            byte[] b = new byte[openRead.available()];
            int off = 0;
            while (true) {
                final int read = openRead.read(b, off, b.length - off);
                if (read <= 0) {
                    break;
                }
                final int n = off + read;
                final int available = openRead.available();
                off = n;
                if (available <= b.length - n) {
                    continue;
                }
                final byte[] array = new byte[available + n];
                System.arraycopy(b, 0, array, 0, n);
                b = array;
                off = n;
            }
            return b;
        }
        finally {
            openRead.close();
        }
    }
    
    public FileOutputStream startWrite() throws IOException {
        if (this.mLegacyBackupName.exists()) {
            rename(this.mLegacyBackupName, this.mBaseName);
        }
        try {
            return new FileOutputStream(this.mNewName);
        }
        catch (final FileNotFoundException ex) {
            if (this.mNewName.getParentFile().mkdirs()) {
                try {
                    return new FileOutputStream(this.mNewName);
                }
                catch (final FileNotFoundException cause) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Failed to create new file ");
                    sb.append(this.mNewName);
                    throw new IOException(sb.toString(), cause);
                }
            }
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("Failed to create directory for ");
            sb2.append(this.mNewName);
            throw new IOException(sb2.toString());
        }
    }
}

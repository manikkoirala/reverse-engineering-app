// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.util;

import kotlin.collections.LongIterator;
import kotlin.collections.IntIterator;
import kotlin.jvm.functions.Function0;
import kotlin.Unit;
import kotlin.jvm.functions.Function2;
import android.util.SparseLongArray;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000D\n\u0000\n\u0002\u0010\b\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0010\t\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0000\u001a\u0015\u0010\u0005\u001a\u00020\u0006*\u00020\u00022\u0006\u0010\u0007\u001a\u00020\u0001H\u0087\n\u001a\u0015\u0010\b\u001a\u00020\u0006*\u00020\u00022\u0006\u0010\u0007\u001a\u00020\u0001H\u0087\b\u001a\u0015\u0010\t\u001a\u00020\u0006*\u00020\u00022\u0006\u0010\n\u001a\u00020\u000bH\u0087\b\u001aE\u0010\f\u001a\u00020\r*\u00020\u000226\u0010\u000e\u001a2\u0012\u0013\u0012\u00110\u0001¢\u0006\f\b\u0010\u0012\b\b\u0011\u0012\u0004\b\b(\u0007\u0012\u0013\u0012\u00110\u000b¢\u0006\f\b\u0010\u0012\b\b\u0011\u0012\u0004\b\b(\n\u0012\u0004\u0012\u00020\r0\u000fH\u0087\b\u001a\u001d\u0010\u0012\u001a\u00020\u000b*\u00020\u00022\u0006\u0010\u0007\u001a\u00020\u00012\u0006\u0010\u0013\u001a\u00020\u000bH\u0087\b\u001a#\u0010\u0014\u001a\u00020\u000b*\u00020\u00022\u0006\u0010\u0007\u001a\u00020\u00012\f\u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\u000b0\u0015H\u0087\b\u001a\r\u0010\u0016\u001a\u00020\u0006*\u00020\u0002H\u0087\b\u001a\r\u0010\u0017\u001a\u00020\u0006*\u00020\u0002H\u0087\b\u001a\f\u0010\u0018\u001a\u00020\u0019*\u00020\u0002H\u0007\u001a\u0015\u0010\u001a\u001a\u00020\u0002*\u00020\u00022\u0006\u0010\u001b\u001a\u00020\u0002H\u0087\u0002\u001a\u0014\u0010\u001c\u001a\u00020\r*\u00020\u00022\u0006\u0010\u001b\u001a\u00020\u0002H\u0007\u001a\u001c\u0010\u001d\u001a\u00020\u0006*\u00020\u00022\u0006\u0010\u0007\u001a\u00020\u00012\u0006\u0010\n\u001a\u00020\u000bH\u0007\u001a\u001d\u0010\u001e\u001a\u00020\r*\u00020\u00022\u0006\u0010\u0007\u001a\u00020\u00012\u0006\u0010\n\u001a\u00020\u000bH\u0087\n\u001a\f\u0010\u001f\u001a\u00020 *\u00020\u0002H\u0007\"\u0016\u0010\u0000\u001a\u00020\u0001*\u00020\u00028\u00c7\u0002¢\u0006\u0006\u001a\u0004\b\u0003\u0010\u0004¨\u0006!" }, d2 = { "size", "", "Landroid/util/SparseLongArray;", "getSize", "(Landroid/util/SparseLongArray;)I", "contains", "", "key", "containsKey", "containsValue", "value", "", "forEach", "", "action", "Lkotlin/Function2;", "Lkotlin/ParameterName;", "name", "getOrDefault", "defaultValue", "getOrElse", "Lkotlin/Function0;", "isEmpty", "isNotEmpty", "keyIterator", "Lkotlin/collections/IntIterator;", "plus", "other", "putAll", "remove", "set", "valueIterator", "Lkotlin/collections/LongIterator;", "core-ktx_release" }, k = 2, mv = { 1, 8, 0 }, xi = 48)
public final class SparseLongArrayKt
{
    public static final boolean contains(final SparseLongArray sparseLongArray, final int n) {
        return sparseLongArray.indexOfKey(n) >= 0;
    }
    
    public static final boolean containsKey(final SparseLongArray sparseLongArray, final int n) {
        return sparseLongArray.indexOfKey(n) >= 0;
    }
    
    public static final boolean containsValue(final SparseLongArray sparseLongArray, final long n) {
        return sparseLongArray.indexOfValue(n) >= 0;
    }
    
    public static final void forEach(final SparseLongArray sparseLongArray, final Function2<? super Integer, ? super Long, Unit> function2) {
        for (int size = sparseLongArray.size(), i = 0; i < size; ++i) {
            function2.invoke((Object)sparseLongArray.keyAt(i), (Object)sparseLongArray.valueAt(i));
        }
    }
    
    public static final long getOrDefault(final SparseLongArray sparseLongArray, final int n, final long n2) {
        return sparseLongArray.get(n, n2);
    }
    
    public static final long getOrElse(final SparseLongArray sparseLongArray, int indexOfKey, final Function0<Long> function0) {
        indexOfKey = sparseLongArray.indexOfKey(indexOfKey);
        long n;
        if (indexOfKey >= 0) {
            n = sparseLongArray.valueAt(indexOfKey);
        }
        else {
            n = ((Number)function0.invoke()).longValue();
        }
        return n;
    }
    
    public static final int getSize(final SparseLongArray sparseLongArray) {
        return sparseLongArray.size();
    }
    
    public static final boolean isEmpty(final SparseLongArray sparseLongArray) {
        return sparseLongArray.size() == 0;
    }
    
    public static final boolean isNotEmpty(final SparseLongArray sparseLongArray) {
        return sparseLongArray.size() != 0;
    }
    
    public static final IntIterator keyIterator(final SparseLongArray sparseLongArray) {
        return (IntIterator)new SparseLongArrayKt$keyIterator.SparseLongArrayKt$keyIterator$1(sparseLongArray);
    }
    
    public static final SparseLongArray plus(final SparseLongArray sparseLongArray, final SparseLongArray sparseLongArray2) {
        final SparseLongArray sparseLongArray3 = new SparseLongArray(sparseLongArray.size() + sparseLongArray2.size());
        putAll(sparseLongArray3, sparseLongArray);
        putAll(sparseLongArray3, sparseLongArray2);
        return sparseLongArray3;
    }
    
    public static final void putAll(final SparseLongArray sparseLongArray, final SparseLongArray sparseLongArray2) {
        for (int size = sparseLongArray2.size(), i = 0; i < size; ++i) {
            sparseLongArray.put(sparseLongArray2.keyAt(i), sparseLongArray2.valueAt(i));
        }
    }
    
    public static final boolean remove(final SparseLongArray sparseLongArray, int indexOfKey, final long n) {
        indexOfKey = sparseLongArray.indexOfKey(indexOfKey);
        if (indexOfKey >= 0 && n == sparseLongArray.valueAt(indexOfKey)) {
            sparseLongArray.removeAt(indexOfKey);
            return true;
        }
        return false;
    }
    
    public static final void set(final SparseLongArray sparseLongArray, final int n, final long n2) {
        sparseLongArray.put(n, n2);
    }
    
    public static final LongIterator valueIterator(final SparseLongArray sparseLongArray) {
        return (LongIterator)new SparseLongArrayKt$valueIterator.SparseLongArrayKt$valueIterator$1(sparseLongArray);
    }
}

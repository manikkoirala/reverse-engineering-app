// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.util;

public interface Predicate<T>
{
    Predicate<T> and(final Predicate<? super T> p0);
    
    Predicate<T> negate();
    
    Predicate<T> or(final Predicate<? super T> p0);
    
    boolean test(final T p0);
}

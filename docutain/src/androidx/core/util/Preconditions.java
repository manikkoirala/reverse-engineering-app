// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.util;

import android.text.TextUtils;
import java.util.Locale;

public final class Preconditions
{
    private Preconditions() {
    }
    
    public static void checkArgument(final boolean b) {
        if (b) {
            return;
        }
        throw new IllegalArgumentException();
    }
    
    public static void checkArgument(final boolean b, final Object obj) {
        if (b) {
            return;
        }
        throw new IllegalArgumentException(String.valueOf(obj));
    }
    
    public static void checkArgument(final boolean b, final String format, final Object... args) {
        if (b) {
            return;
        }
        throw new IllegalArgumentException(String.format(format, args));
    }
    
    public static float checkArgumentFinite(final float n, final String s) {
        if (Float.isNaN(n)) {
            final StringBuilder sb = new StringBuilder();
            sb.append(s);
            sb.append(" must not be NaN");
            throw new IllegalArgumentException(sb.toString());
        }
        if (!Float.isInfinite(n)) {
            return n;
        }
        final StringBuilder sb2 = new StringBuilder();
        sb2.append(s);
        sb2.append(" must not be infinite");
        throw new IllegalArgumentException(sb2.toString());
    }
    
    public static double checkArgumentInRange(final double n, final double n2, final double n3, final String s) {
        if (n < n2) {
            throw new IllegalArgumentException(String.format(Locale.US, "%s is out of range of [%f, %f] (too low)", s, n2, n3));
        }
        if (n <= n3) {
            return n;
        }
        throw new IllegalArgumentException(String.format(Locale.US, "%s is out of range of [%f, %f] (too high)", s, n2, n3));
    }
    
    public static float checkArgumentInRange(final float n, final float n2, final float n3, final String s) {
        if (n < n2) {
            throw new IllegalArgumentException(String.format(Locale.US, "%s is out of range of [%f, %f] (too low)", s, n2, n3));
        }
        if (n <= n3) {
            return n;
        }
        throw new IllegalArgumentException(String.format(Locale.US, "%s is out of range of [%f, %f] (too high)", s, n2, n3));
    }
    
    public static int checkArgumentInRange(final int n, final int n2, final int n3, final String s) {
        if (n < n2) {
            throw new IllegalArgumentException(String.format(Locale.US, "%s is out of range of [%d, %d] (too low)", s, n2, n3));
        }
        if (n <= n3) {
            return n;
        }
        throw new IllegalArgumentException(String.format(Locale.US, "%s is out of range of [%d, %d] (too high)", s, n2, n3));
    }
    
    public static long checkArgumentInRange(final long n, final long n2, final long n3, final String s) {
        if (n < n2) {
            throw new IllegalArgumentException(String.format(Locale.US, "%s is out of range of [%d, %d] (too low)", s, n2, n3));
        }
        if (n <= n3) {
            return n;
        }
        throw new IllegalArgumentException(String.format(Locale.US, "%s is out of range of [%d, %d] (too high)", s, n2, n3));
    }
    
    public static int checkArgumentNonnegative(final int n) {
        if (n >= 0) {
            return n;
        }
        throw new IllegalArgumentException();
    }
    
    public static int checkArgumentNonnegative(final int n, final String s) {
        if (n >= 0) {
            return n;
        }
        throw new IllegalArgumentException(s);
    }
    
    public static int checkFlagsArgument(final int i, final int j) {
        if ((i & j) == i) {
            return i;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Requested flags 0x");
        sb.append(Integer.toHexString(i));
        sb.append(", but only 0x");
        sb.append(Integer.toHexString(j));
        sb.append(" are allowed");
        throw new IllegalArgumentException(sb.toString());
    }
    
    public static <T> T checkNotNull(final T t) {
        t.getClass();
        return t;
    }
    
    public static <T> T checkNotNull(final T t, final Object obj) {
        if (t != null) {
            return t;
        }
        throw new NullPointerException(String.valueOf(obj));
    }
    
    public static void checkState(final boolean b) {
        checkState(b, null);
    }
    
    public static void checkState(final boolean b, final String s) {
        if (b) {
            return;
        }
        throw new IllegalStateException(s);
    }
    
    public static <T extends CharSequence> T checkStringNotEmpty(final T t) {
        if (!TextUtils.isEmpty((CharSequence)t)) {
            return t;
        }
        throw new IllegalArgumentException();
    }
    
    public static <T extends CharSequence> T checkStringNotEmpty(final T t, final Object obj) {
        if (!TextUtils.isEmpty((CharSequence)t)) {
            return t;
        }
        throw new IllegalArgumentException(String.valueOf(obj));
    }
    
    public static <T extends CharSequence> T checkStringNotEmpty(final T t, final String format, final Object... args) {
        if (!TextUtils.isEmpty((CharSequence)t)) {
            return t;
        }
        throw new IllegalArgumentException(String.format(format, args));
    }
}

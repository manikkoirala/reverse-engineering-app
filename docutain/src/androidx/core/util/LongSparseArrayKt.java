// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.util;

import java.util.Iterator;
import kotlin.jvm.internal.Intrinsics;
import kotlin.collections.LongIterator;
import kotlin.jvm.functions.Function0;
import kotlin.Unit;
import kotlin.jvm.functions.Function2;
import android.util.LongSparseArray;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000F\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\t\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0010(\n\u0000\u001a!\u0010\u0006\u001a\u00020\u0007\"\u0004\b\u0000\u0010\u0002*\b\u0012\u0004\u0012\u0002H\u00020\u00032\u0006\u0010\b\u001a\u00020\tH\u0087\n\u001a!\u0010\n\u001a\u00020\u0007\"\u0004\b\u0000\u0010\u0002*\b\u0012\u0004\u0012\u0002H\u00020\u00032\u0006\u0010\b\u001a\u00020\tH\u0087\b\u001a&\u0010\u000b\u001a\u00020\u0007\"\u0004\b\u0000\u0010\u0002*\b\u0012\u0004\u0012\u0002H\u00020\u00032\u0006\u0010\f\u001a\u0002H\u0002H\u0087\b¢\u0006\u0002\u0010\r\u001aQ\u0010\u000e\u001a\u00020\u000f\"\u0004\b\u0000\u0010\u0002*\b\u0012\u0004\u0012\u0002H\u00020\u000326\u0010\u0010\u001a2\u0012\u0013\u0012\u00110\t¢\u0006\f\b\u0012\u0012\b\b\u0013\u0012\u0004\b\b(\b\u0012\u0013\u0012\u0011H\u0002¢\u0006\f\b\u0012\u0012\b\b\u0013\u0012\u0004\b\b(\f\u0012\u0004\u0012\u00020\u000f0\u0011H\u0087\b\u001a.\u0010\u0014\u001a\u0002H\u0002\"\u0004\b\u0000\u0010\u0002*\b\u0012\u0004\u0012\u0002H\u00020\u00032\u0006\u0010\b\u001a\u00020\t2\u0006\u0010\u0015\u001a\u0002H\u0002H\u0087\b¢\u0006\u0002\u0010\u0016\u001a4\u0010\u0017\u001a\u0002H\u0002\"\u0004\b\u0000\u0010\u0002*\b\u0012\u0004\u0012\u0002H\u00020\u00032\u0006\u0010\b\u001a\u00020\t2\f\u0010\u0015\u001a\b\u0012\u0004\u0012\u0002H\u00020\u0018H\u0087\b¢\u0006\u0002\u0010\u0019\u001a\u0019\u0010\u001a\u001a\u00020\u0007\"\u0004\b\u0000\u0010\u0002*\b\u0012\u0004\u0012\u0002H\u00020\u0003H\u0087\b\u001a\u0019\u0010\u001b\u001a\u00020\u0007\"\u0004\b\u0000\u0010\u0002*\b\u0012\u0004\u0012\u0002H\u00020\u0003H\u0087\b\u001a\u0018\u0010\u001c\u001a\u00020\u001d\"\u0004\b\u0000\u0010\u0002*\b\u0012\u0004\u0012\u0002H\u00020\u0003H\u0007\u001a-\u0010\u001e\u001a\b\u0012\u0004\u0012\u0002H\u00020\u0003\"\u0004\b\u0000\u0010\u0002*\b\u0012\u0004\u0012\u0002H\u00020\u00032\f\u0010\u001f\u001a\b\u0012\u0004\u0012\u0002H\u00020\u0003H\u0087\u0002\u001a&\u0010 \u001a\u00020\u000f\"\u0004\b\u0000\u0010\u0002*\b\u0012\u0004\u0012\u0002H\u00020\u00032\f\u0010\u001f\u001a\b\u0012\u0004\u0012\u0002H\u00020\u0003H\u0007\u001a-\u0010!\u001a\u00020\u0007\"\u0004\b\u0000\u0010\u0002*\b\u0012\u0004\u0012\u0002H\u00020\u00032\u0006\u0010\b\u001a\u00020\t2\u0006\u0010\f\u001a\u0002H\u0002H\u0007¢\u0006\u0002\u0010\"\u001a.\u0010#\u001a\u00020\u000f\"\u0004\b\u0000\u0010\u0002*\b\u0012\u0004\u0012\u0002H\u00020\u00032\u0006\u0010\b\u001a\u00020\t2\u0006\u0010\f\u001a\u0002H\u0002H\u0087\n¢\u0006\u0002\u0010$\u001a\u001e\u0010%\u001a\b\u0012\u0004\u0012\u0002H\u00020&\"\u0004\b\u0000\u0010\u0002*\b\u0012\u0004\u0012\u0002H\u00020\u0003H\u0007\"\"\u0010\u0000\u001a\u00020\u0001\"\u0004\b\u0000\u0010\u0002*\b\u0012\u0004\u0012\u0002H\u00020\u00038\u00c7\u0002¢\u0006\u0006\u001a\u0004\b\u0004\u0010\u0005¨\u0006'" }, d2 = { "size", "", "T", "Landroid/util/LongSparseArray;", "getSize", "(Landroid/util/LongSparseArray;)I", "contains", "", "key", "", "containsKey", "containsValue", "value", "(Landroid/util/LongSparseArray;Ljava/lang/Object;)Z", "forEach", "", "action", "Lkotlin/Function2;", "Lkotlin/ParameterName;", "name", "getOrDefault", "defaultValue", "(Landroid/util/LongSparseArray;JLjava/lang/Object;)Ljava/lang/Object;", "getOrElse", "Lkotlin/Function0;", "(Landroid/util/LongSparseArray;JLkotlin/jvm/functions/Function0;)Ljava/lang/Object;", "isEmpty", "isNotEmpty", "keyIterator", "Lkotlin/collections/LongIterator;", "plus", "other", "putAll", "remove", "(Landroid/util/LongSparseArray;JLjava/lang/Object;)Z", "set", "(Landroid/util/LongSparseArray;JLjava/lang/Object;)V", "valueIterator", "", "core-ktx_release" }, k = 2, mv = { 1, 8, 0 }, xi = 48)
public final class LongSparseArrayKt
{
    public static final <T> boolean contains(final LongSparseArray<T> longSparseArray, final long n) {
        return longSparseArray.indexOfKey(n) >= 0;
    }
    
    public static final <T> boolean containsKey(final LongSparseArray<T> longSparseArray, final long n) {
        return longSparseArray.indexOfKey(n) >= 0;
    }
    
    public static final <T> boolean containsValue(final LongSparseArray<T> longSparseArray, final T t) {
        return longSparseArray.indexOfValue((Object)t) >= 0;
    }
    
    public static final <T> void forEach(final LongSparseArray<T> longSparseArray, final Function2<? super Long, ? super T, Unit> function2) {
        for (int size = longSparseArray.size(), i = 0; i < size; ++i) {
            function2.invoke((Object)longSparseArray.keyAt(i), longSparseArray.valueAt(i));
        }
    }
    
    public static final <T> T getOrDefault(final LongSparseArray<T> longSparseArray, final long n, final T t) {
        Object value = longSparseArray.get(n);
        if (value == null) {
            value = t;
        }
        return (T)value;
    }
    
    public static final <T> T getOrElse(final LongSparseArray<T> longSparseArray, final long n, final Function0<? extends T> function0) {
        Object o;
        if ((o = longSparseArray.get(n)) == null) {
            o = function0.invoke();
        }
        return (T)o;
    }
    
    public static final <T> int getSize(final LongSparseArray<T> longSparseArray) {
        return longSparseArray.size();
    }
    
    public static final <T> boolean isEmpty(final LongSparseArray<T> longSparseArray) {
        return longSparseArray.size() == 0;
    }
    
    public static final <T> boolean isNotEmpty(final LongSparseArray<T> longSparseArray) {
        return longSparseArray.size() != 0;
    }
    
    public static final <T> LongIterator keyIterator(final LongSparseArray<T> longSparseArray) {
        return (LongIterator)new LongSparseArrayKt$keyIterator.LongSparseArrayKt$keyIterator$1((LongSparseArray)longSparseArray);
    }
    
    public static final <T> LongSparseArray<T> plus(final LongSparseArray<T> longSparseArray, final LongSparseArray<T> longSparseArray2) {
        final LongSparseArray longSparseArray3 = new LongSparseArray(longSparseArray.size() + longSparseArray2.size());
        putAll((android.util.LongSparseArray<Object>)longSparseArray3, (android.util.LongSparseArray<Object>)longSparseArray);
        putAll((android.util.LongSparseArray<Object>)longSparseArray3, (android.util.LongSparseArray<Object>)longSparseArray2);
        return (LongSparseArray<T>)longSparseArray3;
    }
    
    public static final <T> void putAll(final LongSparseArray<T> longSparseArray, final LongSparseArray<T> longSparseArray2) {
        for (int size = longSparseArray2.size(), i = 0; i < size; ++i) {
            longSparseArray.put(longSparseArray2.keyAt(i), longSparseArray2.valueAt(i));
        }
    }
    
    public static final <T> boolean remove(final LongSparseArray<T> longSparseArray, final long n, final T t) {
        final int indexOfKey = longSparseArray.indexOfKey(n);
        if (indexOfKey >= 0 && Intrinsics.areEqual((Object)t, longSparseArray.valueAt(indexOfKey))) {
            longSparseArray.removeAt(indexOfKey);
            return true;
        }
        return false;
    }
    
    public static final <T> void set(final LongSparseArray<T> longSparseArray, final long n, final T t) {
        longSparseArray.put(n, (Object)t);
    }
    
    public static final <T> Iterator<T> valueIterator(final LongSparseArray<T> longSparseArray) {
        return (Iterator<T>)new LongSparseArrayKt$valueIterator.LongSparseArrayKt$valueIterator$1((LongSparseArray)longSparseArray);
    }
}

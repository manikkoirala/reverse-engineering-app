// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.telephony;

import java.lang.reflect.InvocationTargetException;
import android.telephony.SubscriptionManager;
import android.os.Build$VERSION;
import java.lang.reflect.Method;

public class SubscriptionManagerCompat
{
    private static Method sGetSlotIndexMethod;
    
    private SubscriptionManagerCompat() {
    }
    
    public static int getSlotIndex(int intValue) {
        if (intValue == -1) {
            return -1;
        }
        if (Build$VERSION.SDK_INT >= 29) {
            return Api29Impl.getSlotIndex(intValue);
        }
        try {
            if (SubscriptionManagerCompat.sGetSlotIndexMethod == null) {
                if (Build$VERSION.SDK_INT >= 26) {
                    SubscriptionManagerCompat.sGetSlotIndexMethod = SubscriptionManager.class.getDeclaredMethod("getSlotIndex", Integer.TYPE);
                }
                else {
                    SubscriptionManagerCompat.sGetSlotIndexMethod = SubscriptionManager.class.getDeclaredMethod("getSlotId", Integer.TYPE);
                }
                SubscriptionManagerCompat.sGetSlotIndexMethod.setAccessible(true);
            }
            final Integer n = (Integer)SubscriptionManagerCompat.sGetSlotIndexMethod.invoke(null, intValue);
            if (n != null) {
                intValue = n;
                return intValue;
            }
            return -1;
        }
        catch (final NoSuchMethodException | IllegalAccessException | InvocationTargetException ex) {
            return -1;
        }
    }
    
    private static class Api29Impl
    {
        static int getSlotIndex(final int n) {
            return SubscriptionManager.getSlotIndex(n);
        }
    }
}

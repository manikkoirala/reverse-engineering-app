// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.telephony.mbms;

import java.util.Iterator;
import java.util.Set;
import java.util.Locale;
import android.os.Build$VERSION;
import android.telephony.mbms.ServiceInfo;
import android.content.Context;

public final class MbmsHelper
{
    private MbmsHelper() {
    }
    
    public static CharSequence getBestNameForService(final Context context, final ServiceInfo serviceInfo) {
        if (Build$VERSION.SDK_INT >= 28) {
            return Api28Impl.getBestNameForService(context, serviceInfo);
        }
        return null;
    }
    
    static class Api28Impl
    {
        private Api28Impl() {
        }
        
        static CharSequence getBestNameForService(final Context context, final ServiceInfo serviceInfo) {
            final Set namedContentLocales = serviceInfo.getNamedContentLocales();
            final boolean empty = namedContentLocales.isEmpty();
            final CharSequence charSequence = null;
            if (empty) {
                return null;
            }
            final String[] array = new String[namedContentLocales.size()];
            int n = 0;
            final Iterator iterator = serviceInfo.getNamedContentLocales().iterator();
            while (iterator.hasNext()) {
                array[n] = ((Locale)iterator.next()).toLanguageTag();
                ++n;
            }
            final Locale firstMatch = context.getResources().getConfiguration().getLocales().getFirstMatch(array);
            CharSequence nameForLocale;
            if (firstMatch == null) {
                nameForLocale = charSequence;
            }
            else {
                nameForLocale = serviceInfo.getNameForLocale(firstMatch);
            }
            return nameForLocale;
        }
    }
}

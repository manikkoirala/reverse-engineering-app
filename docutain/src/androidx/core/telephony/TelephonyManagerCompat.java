// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.telephony;

import java.lang.reflect.InvocationTargetException;
import android.os.Build$VERSION;
import android.telephony.TelephonyManager;
import java.lang.reflect.Method;

public class TelephonyManagerCompat
{
    private static Method sGetDeviceIdMethod;
    private static Method sGetSubIdMethod;
    
    private TelephonyManagerCompat() {
    }
    
    public static String getImei(final TelephonyManager obj) {
        if (Build$VERSION.SDK_INT >= 26) {
            return Api26Impl.getImei(obj);
        }
        if (Build$VERSION.SDK_INT >= 22) {
            final int subscriptionId = getSubscriptionId(obj);
            if (subscriptionId != Integer.MAX_VALUE && subscriptionId != -1) {
                final int slotIndex = SubscriptionManagerCompat.getSlotIndex(subscriptionId);
                if (Build$VERSION.SDK_INT >= 23) {
                    return Api23Impl.getDeviceId(obj, slotIndex);
                }
                try {
                    if (TelephonyManagerCompat.sGetDeviceIdMethod == null) {
                        (TelephonyManagerCompat.sGetDeviceIdMethod = TelephonyManager.class.getDeclaredMethod("getDeviceId", Integer.TYPE)).setAccessible(true);
                    }
                    return (String)TelephonyManagerCompat.sGetDeviceIdMethod.invoke(obj, slotIndex);
                }
                catch (final NoSuchMethodException | IllegalAccessException | InvocationTargetException ex) {
                    return null;
                }
            }
        }
        return obj.getDeviceId();
    }
    
    public static int getSubscriptionId(final TelephonyManager obj) {
        if (Build$VERSION.SDK_INT >= 30) {
            return Api30Impl.getSubscriptionId(obj);
        }
        if (Build$VERSION.SDK_INT < 22) {
            return Integer.MAX_VALUE;
        }
        try {
            if (TelephonyManagerCompat.sGetSubIdMethod == null) {
                (TelephonyManagerCompat.sGetSubIdMethod = TelephonyManager.class.getDeclaredMethod("getSubId", (Class<?>[])new Class[0])).setAccessible(true);
            }
            final Integer n = (Integer)TelephonyManagerCompat.sGetSubIdMethod.invoke(obj, new Object[0]);
            if (n != null && n != -1) {
                return n;
            }
            return Integer.MAX_VALUE;
        }
        catch (final InvocationTargetException | IllegalAccessException | NoSuchMethodException ex) {
            return Integer.MAX_VALUE;
        }
    }
    
    private static class Api23Impl
    {
        static String getDeviceId(final TelephonyManager telephonyManager, final int n) {
            return telephonyManager.getDeviceId(n);
        }
    }
    
    private static class Api26Impl
    {
        static String getImei(final TelephonyManager telephonyManager) {
            return telephonyManager.getImei();
        }
    }
    
    private static class Api30Impl
    {
        static int getSubscriptionId(final TelephonyManager telephonyManager) {
            return telephonyManager.getSubscriptionId();
        }
    }
}

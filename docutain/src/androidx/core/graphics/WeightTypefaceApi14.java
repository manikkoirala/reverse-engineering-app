// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.graphics;

import androidx.core.content.res.FontResourcesParserCompat;
import android.content.Context;
import android.util.Log;
import android.graphics.Typeface;
import android.util.SparseArray;
import androidx.collection.LongSparseArray;
import java.lang.reflect.Field;

final class WeightTypefaceApi14
{
    private static final String NATIVE_INSTANCE_FIELD = "native_instance";
    private static final String TAG = "WeightTypeface";
    private static final Field sNativeInstance;
    private static final Object sWeightCacheLock;
    private static final LongSparseArray<SparseArray<Typeface>> sWeightTypefaceCache;
    
    static {
        Field declaredField;
        try {
            declaredField = Typeface.class.getDeclaredField("native_instance");
            declaredField.setAccessible(true);
        }
        catch (final Exception ex) {
            Log.e("WeightTypeface", ex.getClass().getName(), (Throwable)ex);
            declaredField = null;
        }
        sNativeInstance = declaredField;
        sWeightTypefaceCache = new LongSparseArray<SparseArray<Typeface>>(3);
        sWeightCacheLock = new Object();
    }
    
    private WeightTypefaceApi14() {
    }
    
    static Typeface createWeightStyle(final TypefaceCompatBaseImpl typefaceCompatBaseImpl, final Context context, final Typeface typeface, final int n, final boolean b) {
        if (!isPrivateApiAvailable()) {
            return null;
        }
        final int n2 = n << 1 | (b ? 1 : 0);
        synchronized (WeightTypefaceApi14.sWeightCacheLock) {
            final long nativeInstance = getNativeInstance(typeface);
            final LongSparseArray<SparseArray<Typeface>> sWeightTypefaceCache = WeightTypefaceApi14.sWeightTypefaceCache;
            SparseArray sparseArray = sWeightTypefaceCache.get(nativeInstance);
            if (sparseArray == null) {
                sparseArray = new SparseArray(4);
                sWeightTypefaceCache.put(nativeInstance, (SparseArray<Typeface>)sparseArray);
            }
            else {
                final Typeface typeface2 = (Typeface)sparseArray.get(n2);
                if (typeface2 != null) {
                    return typeface2;
                }
            }
            Typeface typeface3;
            if ((typeface3 = getBestFontFromFamily(typefaceCompatBaseImpl, context, typeface, n, b)) == null) {
                typeface3 = platformTypefaceCreate(typeface, n, b);
            }
            sparseArray.put(n2, (Object)typeface3);
            return typeface3;
        }
    }
    
    private static Typeface getBestFontFromFamily(final TypefaceCompatBaseImpl typefaceCompatBaseImpl, final Context context, final Typeface typeface, final int n, final boolean b) {
        final FontResourcesParserCompat.FontFamilyFilesResourceEntry fontFamily = typefaceCompatBaseImpl.getFontFamily(typeface);
        if (fontFamily == null) {
            return null;
        }
        return typefaceCompatBaseImpl.createFromFontFamilyFilesResourceEntry(context, fontFamily, context.getResources(), n, b);
    }
    
    private static long getNativeInstance(final Typeface obj) {
        try {
            return ((Number)WeightTypefaceApi14.sNativeInstance.get(obj)).longValue();
        }
        catch (final IllegalAccessException cause) {
            throw new RuntimeException(cause);
        }
    }
    
    private static boolean isPrivateApiAvailable() {
        return WeightTypefaceApi14.sNativeInstance != null;
    }
    
    private static Typeface platformTypefaceCreate(final Typeface typeface, int n, final boolean b) {
        final int n2 = 1;
        if (n >= 600) {
            n = 1;
        }
        else {
            n = 0;
        }
        if (n == 0 && !b) {
            n = 0;
        }
        else if (n == 0) {
            n = 2;
        }
        else if (!b) {
            n = n2;
        }
        else {
            n = 3;
        }
        return Typeface.create(typeface, n);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.graphics;

public enum BlendModeCompat
{
    private static final BlendModeCompat[] $VALUES;
    
    CLEAR, 
    COLOR, 
    COLOR_BURN, 
    COLOR_DODGE, 
    DARKEN, 
    DIFFERENCE, 
    DST, 
    DST_ATOP, 
    DST_IN, 
    DST_OUT, 
    DST_OVER, 
    EXCLUSION, 
    HARD_LIGHT, 
    HUE, 
    LIGHTEN, 
    LUMINOSITY, 
    MODULATE, 
    MULTIPLY, 
    OVERLAY, 
    PLUS, 
    SATURATION, 
    SCREEN, 
    SOFT_LIGHT, 
    SRC, 
    SRC_ATOP, 
    SRC_IN, 
    SRC_OUT, 
    SRC_OVER, 
    XOR;
    
    private static /* synthetic */ BlendModeCompat[] $values() {
        return new BlendModeCompat[] { BlendModeCompat.CLEAR, BlendModeCompat.SRC, BlendModeCompat.DST, BlendModeCompat.SRC_OVER, BlendModeCompat.DST_OVER, BlendModeCompat.SRC_IN, BlendModeCompat.DST_IN, BlendModeCompat.SRC_OUT, BlendModeCompat.DST_OUT, BlendModeCompat.SRC_ATOP, BlendModeCompat.DST_ATOP, BlendModeCompat.XOR, BlendModeCompat.PLUS, BlendModeCompat.MODULATE, BlendModeCompat.SCREEN, BlendModeCompat.OVERLAY, BlendModeCompat.DARKEN, BlendModeCompat.LIGHTEN, BlendModeCompat.COLOR_DODGE, BlendModeCompat.COLOR_BURN, BlendModeCompat.HARD_LIGHT, BlendModeCompat.SOFT_LIGHT, BlendModeCompat.DIFFERENCE, BlendModeCompat.EXCLUSION, BlendModeCompat.MULTIPLY, BlendModeCompat.HUE, BlendModeCompat.SATURATION, BlendModeCompat.COLOR, BlendModeCompat.LUMINOSITY };
    }
    
    static {
        $VALUES = $values();
    }
}

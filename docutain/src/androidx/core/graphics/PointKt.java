// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.graphics;

import android.graphics.Point;
import android.graphics.PointF;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000\u0016\n\u0000\n\u0002\u0010\b\n\u0002\u0018\u0002\n\u0002\u0010\u0007\n\u0002\u0018\u0002\n\u0002\b\f\u001a\r\u0010\u0000\u001a\u00020\u0001*\u00020\u0002H\u0086\n\u001a\r\u0010\u0000\u001a\u00020\u0003*\u00020\u0004H\u0086\n\u001a\r\u0010\u0005\u001a\u00020\u0001*\u00020\u0002H\u0086\n\u001a\r\u0010\u0005\u001a\u00020\u0003*\u00020\u0004H\u0086\n\u001a\u0015\u0010\u0006\u001a\u00020\u0002*\u00020\u00022\u0006\u0010\u0007\u001a\u00020\u0003H\u0086\n\u001a\u0015\u0010\u0006\u001a\u00020\u0004*\u00020\u00042\u0006\u0010\u0007\u001a\u00020\u0003H\u0086\n\u001a\u0015\u0010\b\u001a\u00020\u0002*\u00020\u00022\u0006\u0010\t\u001a\u00020\u0002H\u0086\n\u001a\u0015\u0010\b\u001a\u00020\u0002*\u00020\u00022\u0006\u0010\n\u001a\u00020\u0001H\u0086\n\u001a\u0015\u0010\b\u001a\u00020\u0004*\u00020\u00042\u0006\u0010\t\u001a\u00020\u0004H\u0086\n\u001a\u0015\u0010\b\u001a\u00020\u0004*\u00020\u00042\u0006\u0010\n\u001a\u00020\u0003H\u0086\n\u001a\u0015\u0010\u000b\u001a\u00020\u0002*\u00020\u00022\u0006\u0010\t\u001a\u00020\u0002H\u0086\n\u001a\u0015\u0010\u000b\u001a\u00020\u0002*\u00020\u00022\u0006\u0010\n\u001a\u00020\u0001H\u0086\n\u001a\u0015\u0010\u000b\u001a\u00020\u0004*\u00020\u00042\u0006\u0010\t\u001a\u00020\u0004H\u0086\n\u001a\u0015\u0010\u000b\u001a\u00020\u0004*\u00020\u00042\u0006\u0010\n\u001a\u00020\u0003H\u0086\n\u001a\u0015\u0010\f\u001a\u00020\u0002*\u00020\u00022\u0006\u0010\u0007\u001a\u00020\u0003H\u0086\n\u001a\u0015\u0010\f\u001a\u00020\u0004*\u00020\u00042\u0006\u0010\u0007\u001a\u00020\u0003H\u0086\n\u001a\r\u0010\r\u001a\u00020\u0002*\u00020\u0004H\u0086\b\u001a\r\u0010\u000e\u001a\u00020\u0004*\u00020\u0002H\u0086\b\u001a\r\u0010\u000f\u001a\u00020\u0002*\u00020\u0002H\u0086\n\u001a\r\u0010\u000f\u001a\u00020\u0004*\u00020\u0004H\u0086\n¨\u0006\u0010" }, d2 = { "component1", "", "Landroid/graphics/Point;", "", "Landroid/graphics/PointF;", "component2", "div", "scalar", "minus", "p", "xy", "plus", "times", "toPoint", "toPointF", "unaryMinus", "core-ktx_release" }, k = 2, mv = { 1, 8, 0 }, xi = 48)
public final class PointKt
{
    public static final float component1(final PointF pointF) {
        return pointF.x;
    }
    
    public static final int component1(final Point point) {
        return point.x;
    }
    
    public static final float component2(final PointF pointF) {
        return pointF.y;
    }
    
    public static final int component2(final Point point) {
        return point.y;
    }
    
    public static final Point div(final Point point, final float n) {
        return new Point(Math.round(point.x / n), Math.round(point.y / n));
    }
    
    public static final PointF div(final PointF pointF, final float n) {
        return new PointF(pointF.x / n, pointF.y / n);
    }
    
    public static final Point minus(Point point, int n) {
        point = new Point(point.x, point.y);
        n = -n;
        point.offset(n, n);
        return point;
    }
    
    public static final Point minus(Point point, final Point point2) {
        point = new Point(point.x, point.y);
        point.offset(-point2.x, -point2.y);
        return point;
    }
    
    public static final PointF minus(PointF pointF, float n) {
        pointF = new PointF(pointF.x, pointF.y);
        n = -n;
        pointF.offset(n, n);
        return pointF;
    }
    
    public static final PointF minus(PointF pointF, final PointF pointF2) {
        pointF = new PointF(pointF.x, pointF.y);
        pointF.offset(-pointF2.x, -pointF2.y);
        return pointF;
    }
    
    public static final Point plus(Point point, final int n) {
        point = new Point(point.x, point.y);
        point.offset(n, n);
        return point;
    }
    
    public static final Point plus(Point point, final Point point2) {
        point = new Point(point.x, point.y);
        point.offset(point2.x, point2.y);
        return point;
    }
    
    public static final PointF plus(PointF pointF, final float n) {
        pointF = new PointF(pointF.x, pointF.y);
        pointF.offset(n, n);
        return pointF;
    }
    
    public static final PointF plus(PointF pointF, final PointF pointF2) {
        pointF = new PointF(pointF.x, pointF.y);
        pointF.offset(pointF2.x, pointF2.y);
        return pointF;
    }
    
    public static final Point times(final Point point, final float n) {
        return new Point(Math.round(point.x * n), Math.round(point.y * n));
    }
    
    public static final PointF times(final PointF pointF, final float n) {
        return new PointF(pointF.x * n, pointF.y * n);
    }
    
    public static final Point toPoint(final PointF pointF) {
        return new Point((int)pointF.x, (int)pointF.y);
    }
    
    public static final PointF toPointF(final Point point) {
        return new PointF(point);
    }
    
    public static final Point unaryMinus(final Point point) {
        return new Point(-point.x, -point.y);
    }
    
    public static final PointF unaryMinus(final PointF pointF) {
        return new PointF(-pointF.x, -pointF.y);
    }
}

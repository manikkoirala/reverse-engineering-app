// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.graphics;

import kotlin.jvm.internal.InlineMarker;
import kotlin.Unit;
import android.graphics.Canvas;
import kotlin.jvm.functions.Function1;
import android.graphics.Picture;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000\"\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0000\u001a6\u0010\u0000\u001a\u00020\u0001*\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00032\u0017\u0010\u0005\u001a\u0013\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\b0\u0006¢\u0006\u0002\b\tH\u0086\b¨\u0006\n" }, d2 = { "record", "Landroid/graphics/Picture;", "width", "", "height", "block", "Lkotlin/Function1;", "Landroid/graphics/Canvas;", "", "Lkotlin/ExtensionFunctionType;", "core-ktx_release" }, k = 2, mv = { 1, 8, 0 }, xi = 48)
public final class PictureKt
{
    public static final Picture record(final Picture picture, final int n, final int n2, final Function1<? super Canvas, Unit> function1) {
        final Canvas beginRecording = picture.beginRecording(n, n2);
        try {
            function1.invoke((Object)beginRecording);
            return picture;
        }
        finally {
            InlineMarker.finallyStart(1);
            picture.endRecording();
            InlineMarker.finallyEnd(1);
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.graphics;

import android.graphics.BlendMode;
import android.graphics.ColorSpace;
import android.graphics.ColorSpace$Named;
import android.graphics.Bitmap$Config;
import android.graphics.Canvas;
import android.graphics.Xfermode;
import android.graphics.PorterDuffXfermode;
import android.graphics.PorterDuff$Mode;
import android.graphics.Paint;
import android.os.Build$VERSION;
import android.graphics.Rect;
import android.graphics.Bitmap;

public final class BitmapCompat
{
    private BitmapCompat() {
    }
    
    public static Bitmap createScaledBitmap(Bitmap bitmap, final int n, final int n2, final Rect rect, final boolean b) {
        if (n <= 0 || n2 <= 0) {
            throw new IllegalArgumentException("dstW and dstH must be > 0!");
        }
        if (rect != null && (rect.isEmpty() || rect.left < 0 || rect.right > bitmap.getWidth() || rect.top < 0 || rect.bottom > bitmap.getHeight())) {
            throw new IllegalArgumentException("srcRect must be contained by srcBm!");
        }
        Bitmap copyBitmapIfHardware;
        if (Build$VERSION.SDK_INT >= 27) {
            copyBitmapIfHardware = Api27Impl.copyBitmapIfHardware(bitmap);
        }
        else {
            copyBitmapIfHardware = bitmap;
        }
        int n3;
        if (rect != null) {
            n3 = rect.width();
        }
        else {
            n3 = bitmap.getWidth();
        }
        int n4;
        if (rect != null) {
            n4 = rect.height();
        }
        else {
            n4 = bitmap.getHeight();
        }
        final float n5 = n / (float)n3;
        final float n6 = n2 / (float)n4;
        int left;
        if (rect != null) {
            left = rect.left;
        }
        else {
            left = 0;
        }
        int top;
        if (rect != null) {
            top = rect.top;
        }
        else {
            top = 0;
        }
        if (left == 0 && top == 0 && n == bitmap.getWidth() && n2 == bitmap.getHeight()) {
            if (bitmap.isMutable() && bitmap == copyBitmapIfHardware) {
                return bitmap.copy(bitmap.getConfig(), true);
            }
            return copyBitmapIfHardware;
        }
        else {
            Paint paintBlendMode = new Paint(1);
            paintBlendMode.setFilterBitmap(true);
            if (Build$VERSION.SDK_INT >= 29) {
                Api29Impl.setPaintBlendMode(paintBlendMode);
            }
            else {
                paintBlendMode.setXfermode((Xfermode)new PorterDuffXfermode(PorterDuff$Mode.SRC));
            }
            if (n3 == n && n4 == n2) {
                bitmap = Bitmap.createBitmap(n, n2, copyBitmapIfHardware.getConfig());
                new Canvas(bitmap).drawBitmap(copyBitmapIfHardware, (float)(-left), (float)(-top), paintBlendMode);
                return bitmap;
            }
            final double log = Math.log(2.0);
            double n7;
            if (n5 > 1.0f) {
                n7 = Math.ceil(Math.log(n5) / log);
            }
            else {
                n7 = Math.floor(Math.log(n5) / log);
            }
            final int n8 = (int)n7;
            double n9;
            if (n6 > 1.0f) {
                n9 = Math.ceil(Math.log(n6) / log);
            }
            else {
                n9 = Math.floor(Math.log(n6) / log);
            }
            final int n10 = (int)n9;
            Bitmap bitmap2 = null;
            int n11;
            int n12;
            if (b && Build$VERSION.SDK_INT >= 27 && !Api27Impl.isAlreadyF16AndLinear(bitmap)) {
                int sizeAtStep;
                if (n8 > 0) {
                    sizeAtStep = sizeAtStep(n3, n, 1, n8);
                }
                else {
                    sizeAtStep = n3;
                }
                int sizeAtStep2;
                if (n10 > 0) {
                    sizeAtStep2 = sizeAtStep(n4, n2, 1, n10);
                }
                else {
                    sizeAtStep2 = n4;
                }
                final Bitmap bitmapWithSourceColorspace = Api27Impl.createBitmapWithSourceColorspace(sizeAtStep, sizeAtStep2, bitmap, true);
                new Canvas(bitmapWithSourceColorspace).drawBitmap(copyBitmapIfHardware, (float)(-left), (float)(-top), paintBlendMode);
                top = 0;
                n11 = 0;
                n12 = 1;
                bitmap2 = copyBitmapIfHardware;
                copyBitmapIfHardware = bitmapWithSourceColorspace;
            }
            else {
                n11 = left;
                n12 = 0;
            }
            final Rect rect2 = new Rect(n11, top, n3, n4);
            final Rect rect3 = new Rect();
            int n15;
            int n16;
            Paint paint;
            Bitmap bitmap3;
            for (int n13 = n8, n14 = n10; n13 != 0 || n14 != 0; n14 = n16, paint = paintBlendMode, bitmap3 = copyBitmapIfHardware, copyBitmapIfHardware = bitmap2, bitmap2 = bitmap3, n13 = n15, paintBlendMode = paint) {
                if (n13 < 0) {
                    n15 = n13 + 1;
                }
                else if ((n15 = n13) > 0) {
                    n15 = n13 - 1;
                }
                if (n14 < 0) {
                    n16 = n14 + 1;
                }
                else if ((n16 = n14) > 0) {
                    n16 = n14 - 1;
                }
                rect3.set(0, 0, sizeAtStep(n3, n, n15, n8), sizeAtStep(n4, n2, n16, n10));
                final boolean b2 = n15 == 0 && n16 == 0;
                final boolean b3 = bitmap2 != null && bitmap2.getWidth() == n && bitmap2.getHeight() == n2;
                Label_0917: {
                    Label_0794: {
                        if (bitmap2 != null && bitmap2 != bitmap && (!b || Build$VERSION.SDK_INT < 27 || Api27Impl.isAlreadyF16AndLinear(bitmap2))) {
                            if (b2) {
                                if (!b3) {
                                    break Label_0794;
                                }
                                if (n12 != 0) {
                                    break Label_0794;
                                }
                            }
                            break Label_0917;
                        }
                    }
                    if (bitmap2 != bitmap && bitmap2 != null) {
                        bitmap2.recycle();
                    }
                    int n17;
                    if (n15 > 0) {
                        n17 = n12;
                    }
                    else {
                        n17 = n15;
                    }
                    final int sizeAtStep3 = sizeAtStep(n3, n, n17, n8);
                    int n18;
                    if (n16 > 0) {
                        n18 = n12;
                    }
                    else {
                        n18 = n16;
                    }
                    final int sizeAtStep4 = sizeAtStep(n4, n2, n18, n10);
                    if (Build$VERSION.SDK_INT >= 27) {
                        bitmap2 = Api27Impl.createBitmapWithSourceColorspace(sizeAtStep3, sizeAtStep4, bitmap, b && !b2);
                    }
                    else {
                        bitmap2 = Bitmap.createBitmap(sizeAtStep3, sizeAtStep4, copyBitmapIfHardware.getConfig());
                    }
                }
                new Canvas(bitmap2).drawBitmap(copyBitmapIfHardware, rect2, rect3, paintBlendMode);
                rect2.set(rect3);
            }
            if (bitmap2 != bitmap && bitmap2 != null) {
                bitmap2.recycle();
            }
            return copyBitmapIfHardware;
        }
    }
    
    public static int getAllocationByteCount(final Bitmap bitmap) {
        if (Build$VERSION.SDK_INT >= 19) {
            return Api19Impl.getAllocationByteCount(bitmap);
        }
        return bitmap.getByteCount();
    }
    
    public static boolean hasMipMap(final Bitmap bitmap) {
        return Build$VERSION.SDK_INT >= 17 && Api17Impl.hasMipMap(bitmap);
    }
    
    public static void setHasMipMap(final Bitmap bitmap, final boolean b) {
        if (Build$VERSION.SDK_INT >= 17) {
            Api17Impl.setHasMipMap(bitmap, b);
        }
    }
    
    static int sizeAtStep(final int n, final int n2, final int n3, final int n4) {
        if (n3 == 0) {
            return n2;
        }
        if (n3 > 0) {
            return n * (1 << n4 - n3);
        }
        return n2 << -n3 - 1;
    }
    
    static class Api17Impl
    {
        private Api17Impl() {
        }
        
        static boolean hasMipMap(final Bitmap bitmap) {
            return bitmap.hasMipMap();
        }
        
        static void setHasMipMap(final Bitmap bitmap, final boolean hasMipMap) {
            bitmap.setHasMipMap(hasMipMap);
        }
    }
    
    static class Api19Impl
    {
        private Api19Impl() {
        }
        
        static int getAllocationByteCount(final Bitmap bitmap) {
            return bitmap.getAllocationByteCount();
        }
    }
    
    static class Api27Impl
    {
        private Api27Impl() {
        }
        
        static Bitmap copyBitmapIfHardware(final Bitmap bitmap) {
            Bitmap copy = bitmap;
            if (bitmap.getConfig() == Bitmap$Config.HARDWARE) {
                Bitmap$Config bitmap$Config = Bitmap$Config.ARGB_8888;
                if (Build$VERSION.SDK_INT >= 31) {
                    bitmap$Config = Api31Impl.getHardwareBitmapConfig(bitmap);
                }
                copy = bitmap.copy(bitmap$Config, true);
            }
            return copy;
        }
        
        static Bitmap createBitmapWithSourceColorspace(final int n, final int n2, final Bitmap bitmap, final boolean b) {
            Bitmap$Config bitmap$Config = bitmap.getConfig();
            final ColorSpace colorSpace = bitmap.getColorSpace();
            ColorSpace value = ColorSpace.get(ColorSpace$Named.LINEAR_EXTENDED_SRGB);
            if (b && !bitmap.getColorSpace().equals((Object)value)) {
                bitmap$Config = Bitmap$Config.RGBA_F16;
            }
            else {
                value = colorSpace;
                if (bitmap.getConfig() == Bitmap$Config.HARDWARE) {
                    bitmap$Config = Bitmap$Config.ARGB_8888;
                    value = colorSpace;
                    if (Build$VERSION.SDK_INT >= 31) {
                        bitmap$Config = Api31Impl.getHardwareBitmapConfig(bitmap);
                        value = colorSpace;
                    }
                }
            }
            return Bitmap.createBitmap(n, n2, bitmap$Config, bitmap.hasAlpha(), value);
        }
        
        static boolean isAlreadyF16AndLinear(final Bitmap bitmap) {
            final ColorSpace value = ColorSpace.get(ColorSpace$Named.LINEAR_EXTENDED_SRGB);
            return bitmap.getConfig() == Bitmap$Config.RGBA_F16 && bitmap.getColorSpace().equals((Object)value);
        }
    }
    
    static class Api29Impl
    {
        private Api29Impl() {
        }
        
        static void setPaintBlendMode(final Paint paint) {
            paint.setBlendMode(BlendMode.SRC);
        }
    }
    
    static class Api31Impl
    {
        private Api31Impl() {
        }
        
        static Bitmap$Config getHardwareBitmapConfig(final Bitmap bitmap) {
            if (bitmap.getHardwareBuffer().getFormat() == 22) {
                return Bitmap$Config.RGBA_F16;
            }
            return Bitmap$Config.ARGB_8888;
        }
    }
}

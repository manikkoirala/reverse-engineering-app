// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.graphics;

import android.os.Handler;
import androidx.core.content.res.ResourcesCompat;
import android.content.res.Resources;
import androidx.core.content.res.FontResourcesParserCompat;
import androidx.core.provider.FontsContractCompat;
import android.os.CancellationSignal;
import androidx.core.util.Preconditions;
import android.content.Context;
import android.os.Build$VERSION;
import android.graphics.Typeface;
import androidx.collection.LruCache;

public class TypefaceCompat
{
    private static final LruCache<String, Typeface> sTypefaceCache;
    private static final TypefaceCompatBaseImpl sTypefaceCompatImpl;
    
    static {
        if (Build$VERSION.SDK_INT >= 29) {
            sTypefaceCompatImpl = new TypefaceCompatApi29Impl();
        }
        else if (Build$VERSION.SDK_INT >= 28) {
            sTypefaceCompatImpl = new TypefaceCompatApi28Impl();
        }
        else if (Build$VERSION.SDK_INT >= 26) {
            sTypefaceCompatImpl = new TypefaceCompatApi26Impl();
        }
        else if (Build$VERSION.SDK_INT >= 24 && TypefaceCompatApi24Impl.isUsable()) {
            sTypefaceCompatImpl = new TypefaceCompatApi24Impl();
        }
        else if (Build$VERSION.SDK_INT >= 21) {
            sTypefaceCompatImpl = new TypefaceCompatApi21Impl();
        }
        else {
            sTypefaceCompatImpl = new TypefaceCompatBaseImpl();
        }
        sTypefaceCache = new LruCache<String, Typeface>(16);
    }
    
    private TypefaceCompat() {
    }
    
    public static void clearCache() {
        TypefaceCompat.sTypefaceCache.evictAll();
    }
    
    public static Typeface create(final Context context, final Typeface typeface, final int n) {
        if (context != null) {
            if (Build$VERSION.SDK_INT < 21) {
                final Typeface bestFontFromFamily = getBestFontFromFamily(context, typeface, n);
                if (bestFontFromFamily != null) {
                    return bestFontFromFamily;
                }
            }
            return Typeface.create(typeface, n);
        }
        throw new IllegalArgumentException("Context cannot be null");
    }
    
    public static Typeface create(final Context context, final Typeface typeface, final int n, final boolean b) {
        if (context != null) {
            Preconditions.checkArgumentInRange(n, 1, 1000, "weight");
            Typeface default1;
            if ((default1 = typeface) == null) {
                default1 = Typeface.DEFAULT;
            }
            return TypefaceCompat.sTypefaceCompatImpl.createWeightStyle(context, default1, n, b);
        }
        throw new IllegalArgumentException("Context cannot be null");
    }
    
    public static Typeface createFromFontInfo(final Context context, final CancellationSignal cancellationSignal, final FontsContractCompat.FontInfo[] array, final int n) {
        return TypefaceCompat.sTypefaceCompatImpl.createFromFontInfo(context, cancellationSignal, array, n);
    }
    
    @Deprecated
    public static Typeface createFromResourcesFamilyXml(final Context context, final FontResourcesParserCompat.FamilyResourceEntry familyResourceEntry, final Resources resources, final int n, final int n2, final ResourcesCompat.FontCallback fontCallback, final Handler handler, final boolean b) {
        return createFromResourcesFamilyXml(context, familyResourceEntry, resources, n, null, 0, n2, fontCallback, handler, b);
    }
    
    public static Typeface createFromResourcesFamilyXml(final Context context, final FontResourcesParserCompat.FamilyResourceEntry familyResourceEntry, final Resources resources, final int n, final String s, final int n2, final int n3, final ResourcesCompat.FontCallback fontCallback, Handler handler, final boolean b) {
        Typeface typeface;
        if (familyResourceEntry instanceof FontResourcesParserCompat.ProviderResourceEntry) {
            final FontResourcesParserCompat.ProviderResourceEntry providerResourceEntry = (FontResourcesParserCompat.ProviderResourceEntry)familyResourceEntry;
            final Typeface systemFontFamily = getSystemFontFamily(providerResourceEntry.getSystemFontFamilyName());
            if (systemFontFamily != null) {
                if (fontCallback != null) {
                    fontCallback.callbackSuccessAsync(systemFontFamily, handler);
                }
                return systemFontFamily;
            }
            final boolean b2 = b ? (providerResourceEntry.getFetchStrategy() == 0) : (fontCallback == null);
            int timeout;
            if (b) {
                timeout = providerResourceEntry.getTimeout();
            }
            else {
                timeout = -1;
            }
            handler = ResourcesCompat.FontCallback.getHandler(handler);
            typeface = FontsContractCompat.requestFont(context, providerResourceEntry.getRequest(), n3, b2, timeout, handler, (FontsContractCompat.FontRequestCallback)new ResourcesCallbackAdapter(fontCallback));
        }
        else {
            final Typeface typeface2 = typeface = TypefaceCompat.sTypefaceCompatImpl.createFromFontFamilyFilesResourceEntry(context, (FontResourcesParserCompat.FontFamilyFilesResourceEntry)familyResourceEntry, resources, n3);
            if (fontCallback != null) {
                if (typeface2 != null) {
                    fontCallback.callbackSuccessAsync(typeface2, handler);
                    typeface = typeface2;
                }
                else {
                    fontCallback.callbackFailAsync(-3, handler);
                    typeface = typeface2;
                }
            }
        }
        if (typeface != null) {
            TypefaceCompat.sTypefaceCache.put(createResourceUid(resources, n, s, n2, n3), typeface);
        }
        return typeface;
    }
    
    @Deprecated
    public static Typeface createFromResourcesFontFile(final Context context, final Resources resources, final int n, final String s, final int n2) {
        return createFromResourcesFontFile(context, resources, n, s, 0, n2);
    }
    
    public static Typeface createFromResourcesFontFile(final Context context, final Resources resources, final int n, final String s, final int n2, final int n3) {
        final Typeface fromResourcesFontFile = TypefaceCompat.sTypefaceCompatImpl.createFromResourcesFontFile(context, resources, n, s, n3);
        if (fromResourcesFontFile != null) {
            TypefaceCompat.sTypefaceCache.put(createResourceUid(resources, n, s, n2, n3), fromResourcesFontFile);
        }
        return fromResourcesFontFile;
    }
    
    private static String createResourceUid(final Resources resources, final int i, final String str, final int j, final int k) {
        final StringBuilder sb = new StringBuilder(resources.getResourcePackageName(i));
        sb.append('-');
        sb.append(str);
        sb.append('-');
        sb.append(j);
        sb.append('-');
        sb.append(i);
        sb.append('-');
        sb.append(k);
        return sb.toString();
    }
    
    @Deprecated
    public static Typeface findFromCache(final Resources resources, final int n, final int n2) {
        return findFromCache(resources, n, null, 0, n2);
    }
    
    public static Typeface findFromCache(final Resources resources, final int n, final String s, final int n2, final int n3) {
        return TypefaceCompat.sTypefaceCache.get(createResourceUid(resources, n, s, n2, n3));
    }
    
    private static Typeface getBestFontFromFamily(final Context context, final Typeface typeface, final int n) {
        final TypefaceCompatBaseImpl sTypefaceCompatImpl = TypefaceCompat.sTypefaceCompatImpl;
        final FontResourcesParserCompat.FontFamilyFilesResourceEntry fontFamily = sTypefaceCompatImpl.getFontFamily(typeface);
        if (fontFamily == null) {
            return null;
        }
        return sTypefaceCompatImpl.createFromFontFamilyFilesResourceEntry(context, fontFamily, context.getResources(), n);
    }
    
    private static Typeface getSystemFontFamily(final String s) {
        Typeface typeface2;
        final Typeface typeface = typeface2 = null;
        if (s != null) {
            if (s.isEmpty()) {
                typeface2 = typeface;
            }
            else {
                final Typeface create = Typeface.create(s, 0);
                final Typeface create2 = Typeface.create(Typeface.DEFAULT, 0);
                typeface2 = typeface;
                if (create != null) {
                    typeface2 = typeface;
                    if (!create.equals((Object)create2)) {
                        typeface2 = create;
                    }
                }
            }
        }
        return typeface2;
    }
    
    public static class ResourcesCallbackAdapter extends FontRequestCallback
    {
        private ResourcesCompat.FontCallback mFontCallback;
        
        public ResourcesCallbackAdapter(final ResourcesCompat.FontCallback mFontCallback) {
            this.mFontCallback = mFontCallback;
        }
        
        @Override
        public void onTypefaceRequestFailed(final int n) {
            final ResourcesCompat.FontCallback mFontCallback = this.mFontCallback;
            if (mFontCallback != null) {
                mFontCallback.onFontRetrievalFailed(n);
            }
        }
        
        @Override
        public void onTypefaceRetrieved(final Typeface typeface) {
            final ResourcesCompat.FontCallback mFontCallback = this.mFontCallback;
            if (mFontCallback != null) {
                mFontCallback.onFontRetrieved(typeface);
            }
        }
    }
}

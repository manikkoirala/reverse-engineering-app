// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.graphics;

import android.graphics.ColorSpace;
import android.graphics.ColorSpace$Named;
import android.graphics.Color;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000>\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0007\n\u0002\u0010\t\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000b\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u000e\n\u0002\b\u0002\u001a\r\u0010\u0018\u001a\u00020\u0004*\u00020\u0019H\u0087\n\u001a\r\u0010\u0018\u001a\u00020\u0001*\u00020\u0001H\u0086\n\u001a\r\u0010\u0018\u001a\u00020\u0004*\u00020\u0005H\u0087\n\u001a\r\u0010\u001a\u001a\u00020\u0004*\u00020\u0019H\u0087\n\u001a\r\u0010\u001a\u001a\u00020\u0001*\u00020\u0001H\u0086\n\u001a\r\u0010\u001a\u001a\u00020\u0004*\u00020\u0005H\u0087\n\u001a\r\u0010\u001b\u001a\u00020\u0004*\u00020\u0019H\u0087\n\u001a\r\u0010\u001b\u001a\u00020\u0001*\u00020\u0001H\u0086\n\u001a\r\u0010\u001b\u001a\u00020\u0004*\u00020\u0005H\u0087\n\u001a\r\u0010\u001c\u001a\u00020\u0004*\u00020\u0019H\u0087\n\u001a\r\u0010\u001c\u001a\u00020\u0001*\u00020\u0001H\u0086\n\u001a\r\u0010\u001c\u001a\u00020\u0004*\u00020\u0005H\u0087\n\u001a\u0015\u0010\u001d\u001a\u00020\u0019*\u00020\u00192\u0006\u0010\t\u001a\u00020\nH\u0087\f\u001a\u0015\u0010\u001d\u001a\u00020\u0019*\u00020\u00192\u0006\u0010\t\u001a\u00020\u001eH\u0087\f\u001a\u0015\u0010\u001d\u001a\u00020\u0005*\u00020\u00012\u0006\u0010\t\u001a\u00020\nH\u0087\f\u001a\u0015\u0010\u001d\u001a\u00020\u0005*\u00020\u00012\u0006\u0010\t\u001a\u00020\u001eH\u0087\f\u001a\u0015\u0010\u001d\u001a\u00020\u0005*\u00020\u00052\u0006\u0010\t\u001a\u00020\nH\u0087\f\u001a\u0015\u0010\u001d\u001a\u00020\u0005*\u00020\u00052\u0006\u0010\t\u001a\u00020\u001eH\u0087\f\u001a\u0015\u0010\u001f\u001a\u00020\u0019*\u00020\u00192\u0006\u0010 \u001a\u00020\u0019H\u0087\u0002\u001a\r\u0010!\u001a\u00020\u0019*\u00020\u0001H\u0087\b\u001a\r\u0010!\u001a\u00020\u0019*\u00020\u0005H\u0087\b\u001a\r\u0010\"\u001a\u00020\u0001*\u00020\u0005H\u0087\b\u001a\r\u0010\"\u001a\u00020\u0001*\u00020#H\u0087\b\u001a\r\u0010$\u001a\u00020\u0005*\u00020\u0001H\u0087\b\"\u0016\u0010\u0000\u001a\u00020\u0001*\u00020\u00018\u00c6\u0002¢\u0006\u0006\u001a\u0004\b\u0002\u0010\u0003\"\u0016\u0010\u0000\u001a\u00020\u0004*\u00020\u00058\u00c7\u0002¢\u0006\u0006\u001a\u0004\b\u0002\u0010\u0006\"\u0016\u0010\u0007\u001a\u00020\u0001*\u00020\u00018\u00c6\u0002¢\u0006\u0006\u001a\u0004\b\b\u0010\u0003\"\u0016\u0010\u0007\u001a\u00020\u0004*\u00020\u00058\u00c7\u0002¢\u0006\u0006\u001a\u0004\b\b\u0010\u0006\"\u0016\u0010\t\u001a\u00020\n*\u00020\u00058\u00c7\u0002¢\u0006\u0006\u001a\u0004\b\u000b\u0010\f\"\u0016\u0010\r\u001a\u00020\u0001*\u00020\u00018\u00c6\u0002¢\u0006\u0006\u001a\u0004\b\u000e\u0010\u0003\"\u0016\u0010\r\u001a\u00020\u0004*\u00020\u00058\u00c7\u0002¢\u0006\u0006\u001a\u0004\b\u000e\u0010\u0006\"\u0016\u0010\u000f\u001a\u00020\u0010*\u00020\u00058\u00c7\u0002¢\u0006\u0006\u001a\u0004\b\u000f\u0010\u0011\"\u0016\u0010\u0012\u001a\u00020\u0010*\u00020\u00058\u00c7\u0002¢\u0006\u0006\u001a\u0004\b\u0012\u0010\u0011\"\u0016\u0010\u0013\u001a\u00020\u0004*\u00020\u00018\u00c7\u0002¢\u0006\u0006\u001a\u0004\b\u0014\u0010\u0015\"\u0016\u0010\u0013\u001a\u00020\u0004*\u00020\u00058\u00c7\u0002¢\u0006\u0006\u001a\u0004\b\u0014\u0010\u0006\"\u0016\u0010\u0016\u001a\u00020\u0001*\u00020\u00018\u00c6\u0002¢\u0006\u0006\u001a\u0004\b\u0017\u0010\u0003\"\u0016\u0010\u0016\u001a\u00020\u0004*\u00020\u00058\u00c7\u0002¢\u0006\u0006\u001a\u0004\b\u0017\u0010\u0006¨\u0006%" }, d2 = { "alpha", "", "getAlpha", "(I)I", "", "", "(J)F", "blue", "getBlue", "colorSpace", "Landroid/graphics/ColorSpace;", "getColorSpace", "(J)Landroid/graphics/ColorSpace;", "green", "getGreen", "isSrgb", "", "(J)Z", "isWideGamut", "luminance", "getLuminance", "(I)F", "red", "getRed", "component1", "Landroid/graphics/Color;", "component2", "component3", "component4", "convertTo", "Landroid/graphics/ColorSpace$Named;", "plus", "c", "toColor", "toColorInt", "", "toColorLong", "core-ktx_release" }, k = 2, mv = { 1, 8, 0 }, xi = 48)
public final class ColorKt
{
    public static final float component1(final long n) {
        return Color.red(n);
    }
    
    public static final float component1(final Color color) {
        return color.getComponent(0);
    }
    
    public static final int component1(final int n) {
        return n >> 24 & 0xFF;
    }
    
    public static final float component2(final long n) {
        return Color.green(n);
    }
    
    public static final float component2(final Color color) {
        return color.getComponent(1);
    }
    
    public static final int component2(final int n) {
        return n >> 16 & 0xFF;
    }
    
    public static final float component3(final long n) {
        return Color.blue(n);
    }
    
    public static final float component3(final Color color) {
        return color.getComponent(2);
    }
    
    public static final int component3(final int n) {
        return n >> 8 & 0xFF;
    }
    
    public static final float component4(final long n) {
        return Color.alpha(n);
    }
    
    public static final float component4(final Color color) {
        return color.getComponent(3);
    }
    
    public static final int component4(final int n) {
        return n & 0xFF;
    }
    
    public static final long convertTo(final int n, final ColorSpace$Named colorSpace$Named) {
        return Color.convert(n, ColorSpace.get(colorSpace$Named));
    }
    
    public static final long convertTo(final int n, final ColorSpace colorSpace) {
        return Color.convert(n, colorSpace);
    }
    
    public static final long convertTo(final long n, final ColorSpace$Named colorSpace$Named) {
        return Color.convert(n, ColorSpace.get(colorSpace$Named));
    }
    
    public static final long convertTo(final long n, final ColorSpace colorSpace) {
        return Color.convert(n, colorSpace);
    }
    
    public static final Color convertTo(final Color color, final ColorSpace$Named colorSpace$Named) {
        return color.convert(ColorSpace.get(colorSpace$Named));
    }
    
    public static final Color convertTo(final Color color, final ColorSpace colorSpace) {
        return color.convert(colorSpace);
    }
    
    public static final float getAlpha(final long n) {
        return Color.alpha(n);
    }
    
    public static final int getAlpha(final int n) {
        return n >> 24 & 0xFF;
    }
    
    public static final float getBlue(final long n) {
        return Color.blue(n);
    }
    
    public static final int getBlue(final int n) {
        return n & 0xFF;
    }
    
    public static final ColorSpace getColorSpace(final long n) {
        return Color.colorSpace(n);
    }
    
    public static final float getGreen(final long n) {
        return Color.green(n);
    }
    
    public static final int getGreen(final int n) {
        return n >> 8 & 0xFF;
    }
    
    public static final float getLuminance(final int n) {
        return Color.luminance(n);
    }
    
    public static final float getLuminance(final long n) {
        return Color.luminance(n);
    }
    
    public static final float getRed(final long n) {
        return Color.red(n);
    }
    
    public static final int getRed(final int n) {
        return n >> 16 & 0xFF;
    }
    
    public static final boolean isSrgb(final long n) {
        return Color.isSrgb(n);
    }
    
    public static final boolean isWideGamut(final long n) {
        return Color.isWideGamut(n);
    }
    
    public static final Color plus(final Color color, final Color color2) {
        return ColorUtils.compositeColors(color2, color);
    }
    
    public static final Color toColor(final int n) {
        return Color.valueOf(n);
    }
    
    public static final Color toColor(final long n) {
        return Color.valueOf(n);
    }
    
    public static final int toColorInt(final long n) {
        return Color.toArgb(n);
    }
    
    public static final int toColorInt(final String s) {
        return Color.parseColor(s);
    }
    
    public static final long toColorLong(final int n) {
        return Color.pack(n);
    }
}

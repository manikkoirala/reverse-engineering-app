// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.graphics;

import android.graphics.Rect;

public final class Insets
{
    public static final Insets NONE;
    public final int bottom;
    public final int left;
    public final int right;
    public final int top;
    
    static {
        NONE = new Insets(0, 0, 0, 0);
    }
    
    private Insets(final int left, final int top, final int right, final int bottom) {
        this.left = left;
        this.top = top;
        this.right = right;
        this.bottom = bottom;
    }
    
    public static Insets add(final Insets insets, final Insets insets2) {
        return of(insets.left + insets2.left, insets.top + insets2.top, insets.right + insets2.right, insets.bottom + insets2.bottom);
    }
    
    public static Insets max(final Insets insets, final Insets insets2) {
        return of(Math.max(insets.left, insets2.left), Math.max(insets.top, insets2.top), Math.max(insets.right, insets2.right), Math.max(insets.bottom, insets2.bottom));
    }
    
    public static Insets min(final Insets insets, final Insets insets2) {
        return of(Math.min(insets.left, insets2.left), Math.min(insets.top, insets2.top), Math.min(insets.right, insets2.right), Math.min(insets.bottom, insets2.bottom));
    }
    
    public static Insets of(final int n, final int n2, final int n3, final int n4) {
        if (n == 0 && n2 == 0 && n3 == 0 && n4 == 0) {
            return Insets.NONE;
        }
        return new Insets(n, n2, n3, n4);
    }
    
    public static Insets of(final Rect rect) {
        return of(rect.left, rect.top, rect.right, rect.bottom);
    }
    
    public static Insets subtract(final Insets insets, final Insets insets2) {
        return of(insets.left - insets2.left, insets.top - insets2.top, insets.right - insets2.right, insets.bottom - insets2.bottom);
    }
    
    public static Insets toCompatInsets(final android.graphics.Insets insets) {
        return of(insets.left, insets.top, insets.right, insets.bottom);
    }
    
    @Deprecated
    public static Insets wrap(final android.graphics.Insets insets) {
        return toCompatInsets(insets);
    }
    
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o != null && this.getClass() == o.getClass()) {
            final Insets insets = (Insets)o;
            return this.bottom == insets.bottom && this.left == insets.left && this.right == insets.right && this.top == insets.top;
        }
        return false;
    }
    
    @Override
    public int hashCode() {
        return ((this.left * 31 + this.top) * 31 + this.right) * 31 + this.bottom;
    }
    
    public android.graphics.Insets toPlatformInsets() {
        return Api29Impl.of(this.left, this.top, this.right, this.bottom);
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("Insets{left=");
        sb.append(this.left);
        sb.append(", top=");
        sb.append(this.top);
        sb.append(", right=");
        sb.append(this.right);
        sb.append(", bottom=");
        sb.append(this.bottom);
        sb.append('}');
        return sb.toString();
    }
    
    static class Api29Impl
    {
        private Api29Impl() {
        }
        
        static android.graphics.Insets of(final int n, final int n2, final int n3, final int n4) {
            return android.graphics.Insets.of(n, n2, n3, n4);
        }
    }
}

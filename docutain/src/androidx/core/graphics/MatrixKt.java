// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.graphics;

import android.graphics.Matrix;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0007\n\u0002\b\u000b\n\u0002\u0010\u0014\n\u0000\u001a\"\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00032\b\b\u0002\u0010\u0005\u001a\u00020\u0003\u001a\u001a\u0010\u0006\u001a\u00020\u00012\b\b\u0002\u0010\u0007\u001a\u00020\u00032\b\b\u0002\u0010\b\u001a\u00020\u0003\u001a\u001a\u0010\t\u001a\u00020\u00012\b\b\u0002\u0010\n\u001a\u00020\u00032\b\b\u0002\u0010\u000b\u001a\u00020\u0003\u001a\u0015\u0010\f\u001a\u00020\u0001*\u00020\u00012\u0006\u0010\r\u001a\u00020\u0001H\u0086\n\u001a\r\u0010\u000e\u001a\u00020\u000f*\u00020\u0001H\u0086\b¨\u0006\u0010" }, d2 = { "rotationMatrix", "Landroid/graphics/Matrix;", "degrees", "", "px", "py", "scaleMatrix", "sx", "sy", "translationMatrix", "tx", "ty", "times", "m", "values", "", "core-ktx_release" }, k = 2, mv = { 1, 8, 0 }, xi = 48)
public final class MatrixKt
{
    public static final Matrix rotationMatrix(final float n, final float n2, final float n3) {
        final Matrix matrix = new Matrix();
        matrix.setRotate(n, n2, n3);
        return matrix;
    }
    
    public static final Matrix scaleMatrix(final float n, final float n2) {
        final Matrix matrix = new Matrix();
        matrix.setScale(n, n2);
        return matrix;
    }
    
    public static final Matrix times(Matrix matrix, final Matrix matrix2) {
        matrix = new Matrix(matrix);
        matrix.preConcat(matrix2);
        return matrix;
    }
    
    public static final Matrix translationMatrix(final float n, final float n2) {
        final Matrix matrix = new Matrix();
        matrix.setTranslate(n, n2);
        return matrix;
    }
    
    public static final float[] values(final Matrix matrix) {
        final float[] array = new float[9];
        matrix.getValues(array);
        return array;
    }
}

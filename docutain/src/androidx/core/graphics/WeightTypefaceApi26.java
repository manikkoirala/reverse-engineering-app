// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.graphics;

import java.lang.reflect.InvocationTargetException;
import android.util.Log;
import android.util.SparseArray;
import androidx.collection.LongSparseArray;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import android.graphics.Typeface;
import java.lang.reflect.Constructor;

final class WeightTypefaceApi26
{
    private static final String NATIVE_CREATE_FROM_TYPEFACE_WITH_EXACT_STYLE_METHOD = "nativeCreateFromTypefaceWithExactStyle";
    private static final String NATIVE_INSTANCE_FIELD = "native_instance";
    private static final String TAG = "WeightTypeface";
    private static final Constructor<Typeface> sConstructor;
    private static final Method sNativeCreateFromTypefaceWithExactStyle;
    private static final Field sNativeInstance;
    private static final Object sWeightCacheLock;
    private static final LongSparseArray<SparseArray<Typeface>> sWeightTypefaceCache;
    
    static {
        final Field field = null;
        Field declaredField = null;
        Method declaredMethod = null;
        Constructor<Typeface> declaredConstructor = null;
        Label_0094: {
            try {
                declaredField = Typeface.class.getDeclaredField("native_instance");
                declaredMethod = Typeface.class.getDeclaredMethod("nativeCreateFromTypefaceWithExactStyle", Long.TYPE, Integer.TYPE, Boolean.TYPE);
                declaredMethod.setAccessible(true);
                declaredConstructor = Typeface.class.getDeclaredConstructor(Long.TYPE);
                declaredConstructor.setAccessible(true);
                break Label_0094;
            }
            catch (final NoSuchMethodException declaredField) {}
            catch (final NoSuchFieldException ex) {}
            Log.e("WeightTypeface", ((NoSuchMethodException)declaredField).getClass().getName(), (Throwable)declaredField);
            declaredMethod = null;
            declaredConstructor = null;
            declaredField = field;
        }
        sNativeInstance = declaredField;
        sNativeCreateFromTypefaceWithExactStyle = declaredMethod;
        sConstructor = declaredConstructor;
        sWeightTypefaceCache = new LongSparseArray<SparseArray<Typeface>>(3);
        sWeightCacheLock = new Object();
    }
    
    private WeightTypefaceApi26() {
    }
    
    private static Typeface create(final long l) {
        try {
            return WeightTypefaceApi26.sConstructor.newInstance(l);
        }
        catch (final IllegalAccessException | InstantiationException | InvocationTargetException ex) {
            return null;
        }
    }
    
    static Typeface createWeightStyle(final Typeface typeface, final int n, final boolean b) {
        if (!isPrivateApiAvailable()) {
            return null;
        }
        final int n2 = n << 1 | (b ? 1 : 0);
        synchronized (WeightTypefaceApi26.sWeightCacheLock) {
            final long nativeInstance = getNativeInstance(typeface);
            final LongSparseArray<SparseArray<Typeface>> sWeightTypefaceCache = WeightTypefaceApi26.sWeightTypefaceCache;
            SparseArray sparseArray = sWeightTypefaceCache.get(nativeInstance);
            if (sparseArray == null) {
                sparseArray = new SparseArray(4);
                sWeightTypefaceCache.put(nativeInstance, (SparseArray<Typeface>)sparseArray);
            }
            else {
                final Typeface typeface2 = (Typeface)sparseArray.get(n2);
                if (typeface2 != null) {
                    return typeface2;
                }
            }
            final Typeface create = create(nativeCreateFromTypefaceWithExactStyle(nativeInstance, n, b));
            sparseArray.put(n2, (Object)create);
            return create;
        }
    }
    
    private static long getNativeInstance(final Typeface obj) {
        try {
            return WeightTypefaceApi26.sNativeInstance.getLong(obj);
        }
        catch (final IllegalAccessException cause) {
            throw new RuntimeException(cause);
        }
    }
    
    private static boolean isPrivateApiAvailable() {
        return WeightTypefaceApi26.sNativeInstance != null;
    }
    
    private static long nativeCreateFromTypefaceWithExactStyle(long longValue, final int i, final boolean b) {
        try {
            longValue = (long)WeightTypefaceApi26.sNativeCreateFromTypefaceWithExactStyle.invoke(null, longValue, i, b);
            return longValue;
        }
        catch (final InvocationTargetException cause) {
            throw new RuntimeException(cause);
        }
        catch (final IllegalAccessException cause2) {
            throw new RuntimeException(cause2);
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.graphics;

import android.graphics.drawable.Drawable;
import android.graphics.ImageDecoder$OnHeaderDecodedListener;
import android.graphics.Bitmap;
import kotlin.Unit;
import android.graphics.ImageDecoder$ImageInfo;
import android.graphics.ImageDecoder;
import kotlin.jvm.functions.Function3;
import android.graphics.ImageDecoder$Source;
import kotlin.Metadata;

@Metadata(d1 = { "\u00000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u001aR\u0010\u0000\u001a\u00020\u0001*\u00020\u00022C\b\u0004\u0010\u0003\u001a=\u0012\u0004\u0012\u00020\u0005\u0012\u0013\u0012\u00110\u0006¢\u0006\f\b\u0007\u0012\b\b\b\u0012\u0004\b\b(\t\u0012\u0013\u0012\u00110\u0002¢\u0006\f\b\u0007\u0012\b\b\b\u0012\u0004\b\b(\n\u0012\u0004\u0012\u00020\u000b0\u0004¢\u0006\u0002\b\fH\u0087\b\u001aR\u0010\r\u001a\u00020\u000e*\u00020\u00022C\b\u0004\u0010\u0003\u001a=\u0012\u0004\u0012\u00020\u0005\u0012\u0013\u0012\u00110\u0006¢\u0006\f\b\u0007\u0012\b\b\b\u0012\u0004\b\b(\t\u0012\u0013\u0012\u00110\u0002¢\u0006\f\b\u0007\u0012\b\b\b\u0012\u0004\b\b(\n\u0012\u0004\u0012\u00020\u000b0\u0004¢\u0006\u0002\b\fH\u0087\b¨\u0006\u000f" }, d2 = { "decodeBitmap", "Landroid/graphics/Bitmap;", "Landroid/graphics/ImageDecoder$Source;", "action", "Lkotlin/Function3;", "Landroid/graphics/ImageDecoder;", "Landroid/graphics/ImageDecoder$ImageInfo;", "Lkotlin/ParameterName;", "name", "info", "source", "", "Lkotlin/ExtensionFunctionType;", "decodeDrawable", "Landroid/graphics/drawable/Drawable;", "core-ktx_release" }, k = 2, mv = { 1, 8, 0 }, xi = 48)
public final class ImageDecoderKt
{
    public static final Bitmap decodeBitmap(final ImageDecoder$Source imageDecoder$Source, final Function3<? super ImageDecoder, ? super ImageDecoder$ImageInfo, ? super ImageDecoder$Source, Unit> function3) {
        return ImageDecoder.decodeBitmap(imageDecoder$Source, (ImageDecoder$OnHeaderDecodedListener)new ImageDecoderKt$decodeBitmap.ImageDecoderKt$decodeBitmap$1((Function3)function3));
    }
    
    public static final Drawable decodeDrawable(final ImageDecoder$Source imageDecoder$Source, final Function3<? super ImageDecoder, ? super ImageDecoder$ImageInfo, ? super ImageDecoder$Source, Unit> function3) {
        return ImageDecoder.decodeDrawable(imageDecoder$Source, (ImageDecoder$OnHeaderDecodedListener)new ImageDecoderKt$decodeDrawable.ImageDecoderKt$decodeDrawable$1((Function3)function3));
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.graphics.drawable;

import android.graphics.PorterDuff$Mode;
import android.content.res.ColorStateList;
import java.io.IOException;
import org.xmlpull.v1.XmlPullParserException;
import android.util.AttributeSet;
import org.xmlpull.v1.XmlPullParser;
import android.content.res.Resources;
import android.util.Log;
import android.graphics.ColorFilter;
import android.graphics.drawable.DrawableContainer$DrawableContainerState;
import android.graphics.drawable.DrawableContainer;
import android.graphics.drawable.InsetDrawable;
import android.os.Build$VERSION;
import android.content.res.Resources$Theme;
import android.graphics.drawable.Drawable;
import java.lang.reflect.Method;

public final class DrawableCompat
{
    private static final String TAG = "DrawableCompat";
    private static Method sGetLayoutDirectionMethod;
    private static boolean sGetLayoutDirectionMethodFetched;
    private static Method sSetLayoutDirectionMethod;
    private static boolean sSetLayoutDirectionMethodFetched;
    
    private DrawableCompat() {
    }
    
    public static void applyTheme(final Drawable drawable, final Resources$Theme resources$Theme) {
        if (Build$VERSION.SDK_INT >= 21) {
            Api21Impl.applyTheme(drawable, resources$Theme);
        }
    }
    
    public static boolean canApplyTheme(final Drawable drawable) {
        return Build$VERSION.SDK_INT >= 21 && Api21Impl.canApplyTheme(drawable);
    }
    
    public static void clearColorFilter(final Drawable drawable) {
        if (Build$VERSION.SDK_INT >= 23) {
            drawable.clearColorFilter();
        }
        else if (Build$VERSION.SDK_INT >= 21) {
            drawable.clearColorFilter();
            if (drawable instanceof InsetDrawable) {
                clearColorFilter(Api19Impl.getDrawable((InsetDrawable)drawable));
            }
            else if (drawable instanceof WrappedDrawable) {
                clearColorFilter(((WrappedDrawable)drawable).getWrappedDrawable());
            }
            else if (drawable instanceof DrawableContainer) {
                final DrawableContainer$DrawableContainerState drawableContainer$DrawableContainerState = (DrawableContainer$DrawableContainerState)((DrawableContainer)drawable).getConstantState();
                if (drawableContainer$DrawableContainerState != null) {
                    for (int i = 0; i < drawableContainer$DrawableContainerState.getChildCount(); ++i) {
                        final Drawable child = Api19Impl.getChild(drawableContainer$DrawableContainerState, i);
                        if (child != null) {
                            clearColorFilter(child);
                        }
                    }
                }
            }
        }
        else {
            drawable.clearColorFilter();
        }
    }
    
    public static int getAlpha(final Drawable drawable) {
        if (Build$VERSION.SDK_INT >= 19) {
            return Api19Impl.getAlpha(drawable);
        }
        return 0;
    }
    
    public static ColorFilter getColorFilter(final Drawable drawable) {
        if (Build$VERSION.SDK_INT >= 21) {
            return Api21Impl.getColorFilter(drawable);
        }
        return null;
    }
    
    public static int getLayoutDirection(final Drawable obj) {
        if (Build$VERSION.SDK_INT >= 23) {
            return Api23Impl.getLayoutDirection(obj);
        }
        if (Build$VERSION.SDK_INT >= 17) {
            if (!DrawableCompat.sGetLayoutDirectionMethodFetched) {
                try {
                    (DrawableCompat.sGetLayoutDirectionMethod = Drawable.class.getDeclaredMethod("getLayoutDirection", (Class<?>[])new Class[0])).setAccessible(true);
                }
                catch (final NoSuchMethodException ex) {
                    Log.i("DrawableCompat", "Failed to retrieve getLayoutDirection() method", (Throwable)ex);
                }
                DrawableCompat.sGetLayoutDirectionMethodFetched = true;
            }
            final Method sGetLayoutDirectionMethod = DrawableCompat.sGetLayoutDirectionMethod;
            if (sGetLayoutDirectionMethod != null) {
                try {
                    return (int)sGetLayoutDirectionMethod.invoke(obj, new Object[0]);
                }
                catch (final Exception ex2) {
                    Log.i("DrawableCompat", "Failed to invoke getLayoutDirection() via reflection", (Throwable)ex2);
                    DrawableCompat.sGetLayoutDirectionMethod = null;
                }
            }
        }
        return 0;
    }
    
    public static void inflate(final Drawable drawable, final Resources resources, final XmlPullParser xmlPullParser, final AttributeSet set, final Resources$Theme resources$Theme) throws XmlPullParserException, IOException {
        if (Build$VERSION.SDK_INT >= 21) {
            Api21Impl.inflate(drawable, resources, xmlPullParser, set, resources$Theme);
        }
        else {
            drawable.inflate(resources, xmlPullParser, set);
        }
    }
    
    public static boolean isAutoMirrored(final Drawable drawable) {
        return Build$VERSION.SDK_INT >= 19 && Api19Impl.isAutoMirrored(drawable);
    }
    
    @Deprecated
    public static void jumpToCurrentState(final Drawable drawable) {
        drawable.jumpToCurrentState();
    }
    
    public static void setAutoMirrored(final Drawable drawable, final boolean b) {
        if (Build$VERSION.SDK_INT >= 19) {
            Api19Impl.setAutoMirrored(drawable, b);
        }
    }
    
    public static void setHotspot(final Drawable drawable, final float n, final float n2) {
        if (Build$VERSION.SDK_INT >= 21) {
            Api21Impl.setHotspot(drawable, n, n2);
        }
    }
    
    public static void setHotspotBounds(final Drawable drawable, final int n, final int n2, final int n3, final int n4) {
        if (Build$VERSION.SDK_INT >= 21) {
            Api21Impl.setHotspotBounds(drawable, n, n2, n3, n4);
        }
    }
    
    public static boolean setLayoutDirection(final Drawable obj, final int i) {
        if (Build$VERSION.SDK_INT >= 23) {
            return Api23Impl.setLayoutDirection(obj, i);
        }
        if (Build$VERSION.SDK_INT >= 17) {
            if (!DrawableCompat.sSetLayoutDirectionMethodFetched) {
                try {
                    (DrawableCompat.sSetLayoutDirectionMethod = Drawable.class.getDeclaredMethod("setLayoutDirection", Integer.TYPE)).setAccessible(true);
                }
                catch (final NoSuchMethodException ex) {
                    Log.i("DrawableCompat", "Failed to retrieve setLayoutDirection(int) method", (Throwable)ex);
                }
                DrawableCompat.sSetLayoutDirectionMethodFetched = true;
            }
            final Method sSetLayoutDirectionMethod = DrawableCompat.sSetLayoutDirectionMethod;
            if (sSetLayoutDirectionMethod != null) {
                try {
                    sSetLayoutDirectionMethod.invoke(obj, i);
                    return true;
                }
                catch (final Exception ex2) {
                    Log.i("DrawableCompat", "Failed to invoke setLayoutDirection(int) via reflection", (Throwable)ex2);
                    DrawableCompat.sSetLayoutDirectionMethod = null;
                }
            }
        }
        return false;
    }
    
    public static void setTint(final Drawable drawable, final int tint) {
        if (Build$VERSION.SDK_INT >= 21) {
            Api21Impl.setTint(drawable, tint);
        }
        else if (drawable instanceof TintAwareDrawable) {
            ((TintAwareDrawable)drawable).setTint(tint);
        }
    }
    
    public static void setTintList(final Drawable drawable, final ColorStateList tintList) {
        if (Build$VERSION.SDK_INT >= 21) {
            Api21Impl.setTintList(drawable, tintList);
        }
        else if (drawable instanceof TintAwareDrawable) {
            ((TintAwareDrawable)drawable).setTintList(tintList);
        }
    }
    
    public static void setTintMode(final Drawable drawable, final PorterDuff$Mode tintMode) {
        if (Build$VERSION.SDK_INT >= 21) {
            Api21Impl.setTintMode(drawable, tintMode);
        }
        else if (drawable instanceof TintAwareDrawable) {
            ((TintAwareDrawable)drawable).setTintMode(tintMode);
        }
    }
    
    public static <T extends Drawable> T unwrap(final Drawable drawable) {
        Drawable wrappedDrawable = drawable;
        if (drawable instanceof WrappedDrawable) {
            wrappedDrawable = ((WrappedDrawable)drawable).getWrappedDrawable();
        }
        return (T)wrappedDrawable;
    }
    
    public static Drawable wrap(final Drawable drawable) {
        if (Build$VERSION.SDK_INT >= 23) {
            return drawable;
        }
        if (Build$VERSION.SDK_INT >= 21) {
            if (!(drawable instanceof TintAwareDrawable)) {
                return new WrappedDrawableApi21(drawable);
            }
            return drawable;
        }
        else {
            if (!(drawable instanceof TintAwareDrawable)) {
                return new WrappedDrawableApi14(drawable);
            }
            return drawable;
        }
    }
    
    static class Api19Impl
    {
        private Api19Impl() {
        }
        
        static int getAlpha(final Drawable drawable) {
            return drawable.getAlpha();
        }
        
        static Drawable getChild(final DrawableContainer$DrawableContainerState drawableContainer$DrawableContainerState, final int n) {
            return drawableContainer$DrawableContainerState.getChild(n);
        }
        
        static Drawable getDrawable(final InsetDrawable insetDrawable) {
            return insetDrawable.getDrawable();
        }
        
        static boolean isAutoMirrored(final Drawable drawable) {
            return drawable.isAutoMirrored();
        }
        
        static void setAutoMirrored(final Drawable drawable, final boolean autoMirrored) {
            drawable.setAutoMirrored(autoMirrored);
        }
    }
    
    static class Api21Impl
    {
        private Api21Impl() {
        }
        
        static void applyTheme(final Drawable drawable, final Resources$Theme resources$Theme) {
            drawable.applyTheme(resources$Theme);
        }
        
        static boolean canApplyTheme(final Drawable drawable) {
            return drawable.canApplyTheme();
        }
        
        static ColorFilter getColorFilter(final Drawable drawable) {
            return drawable.getColorFilter();
        }
        
        static void inflate(final Drawable drawable, final Resources resources, final XmlPullParser xmlPullParser, final AttributeSet set, final Resources$Theme resources$Theme) throws XmlPullParserException, IOException {
            drawable.inflate(resources, xmlPullParser, set, resources$Theme);
        }
        
        static void setHotspot(final Drawable drawable, final float n, final float n2) {
            drawable.setHotspot(n, n2);
        }
        
        static void setHotspotBounds(final Drawable drawable, final int n, final int n2, final int n3, final int n4) {
            drawable.setHotspotBounds(n, n2, n3, n4);
        }
        
        static void setTint(final Drawable drawable, final int tint) {
            drawable.setTint(tint);
        }
        
        static void setTintList(final Drawable drawable, final ColorStateList tintList) {
            drawable.setTintList(tintList);
        }
        
        static void setTintMode(final Drawable drawable, final PorterDuff$Mode tintMode) {
            drawable.setTintMode(tintMode);
        }
    }
    
    static class Api23Impl
    {
        private Api23Impl() {
        }
        
        static int getLayoutDirection(final Drawable drawable) {
            return drawable.getLayoutDirection();
        }
        
        static boolean setLayoutDirection(final Drawable drawable, final int layoutDirection) {
            return drawable.setLayoutDirection(layoutDirection);
        }
    }
}

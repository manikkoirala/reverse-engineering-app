// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.graphics.drawable;

import android.graphics.Rect;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.Bitmap;
import android.graphics.Bitmap$Config;
import android.graphics.drawable.Drawable;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000$\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0005\u001a*\u0010\u0000\u001a\u00020\u0001*\u00020\u00022\b\b\u0003\u0010\u0003\u001a\u00020\u00042\b\b\u0003\u0010\u0005\u001a\u00020\u00042\n\b\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u0007\u001a,\u0010\b\u001a\u0004\u0018\u00010\u0001*\u00020\u00022\b\b\u0003\u0010\u0003\u001a\u00020\u00042\b\b\u0003\u0010\u0005\u001a\u00020\u00042\n\b\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u0007\u001a2\u0010\t\u001a\u00020\n*\u00020\u00022\b\b\u0003\u0010\u000b\u001a\u00020\u00042\b\b\u0003\u0010\f\u001a\u00020\u00042\b\b\u0003\u0010\r\u001a\u00020\u00042\b\b\u0003\u0010\u000e\u001a\u00020\u0004¨\u0006\u000f" }, d2 = { "toBitmap", "Landroid/graphics/Bitmap;", "Landroid/graphics/drawable/Drawable;", "width", "", "height", "config", "Landroid/graphics/Bitmap$Config;", "toBitmapOrNull", "updateBounds", "", "left", "top", "right", "bottom", "core-ktx_release" }, k = 2, mv = { 1, 8, 0 }, xi = 48)
public final class DrawableKt
{
    public static final Bitmap toBitmap(final Drawable drawable, final int n, final int n2, final Bitmap$Config bitmap$Config) {
        if (drawable instanceof BitmapDrawable) {
            final BitmapDrawable bitmapDrawable = (BitmapDrawable)drawable;
            if (bitmapDrawable.getBitmap() == null) {
                throw new IllegalArgumentException("bitmap is null");
            }
            if (bitmap$Config == null || bitmapDrawable.getBitmap().getConfig() == bitmap$Config) {
                if (n == bitmapDrawable.getBitmap().getWidth() && n2 == bitmapDrawable.getBitmap().getHeight()) {
                    return bitmapDrawable.getBitmap();
                }
                return Bitmap.createScaledBitmap(bitmapDrawable.getBitmap(), n, n2, true);
            }
        }
        final Rect bounds = drawable.getBounds();
        final int left = bounds.left;
        final int top = bounds.top;
        final int right = bounds.right;
        final int bottom = bounds.bottom;
        Bitmap$Config argb_8888;
        if ((argb_8888 = bitmap$Config) == null) {
            argb_8888 = Bitmap$Config.ARGB_8888;
        }
        final Bitmap bitmap = Bitmap.createBitmap(n, n2, argb_8888);
        drawable.setBounds(0, 0, n, n2);
        drawable.draw(new Canvas(bitmap));
        drawable.setBounds(left, top, right, bottom);
        return bitmap;
    }
    
    public static final Bitmap toBitmapOrNull(final Drawable drawable, final int n, final int n2, final Bitmap$Config bitmap$Config) {
        if (drawable instanceof BitmapDrawable && ((BitmapDrawable)drawable).getBitmap() == null) {
            return null;
        }
        return toBitmap(drawable, n, n2, bitmap$Config);
    }
    
    public static final void updateBounds(final Drawable drawable, final int n, final int n2, final int n3, final int n4) {
        drawable.setBounds(n, n2, n3, n4);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.graphics.drawable;

import android.net.Uri;
import android.graphics.drawable.Icon;
import android.graphics.Bitmap;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0012\n\u0000\u001a\r\u0010\u0000\u001a\u00020\u0001*\u00020\u0002H\u0087\b\u001a\r\u0010\u0003\u001a\u00020\u0001*\u00020\u0002H\u0087\b\u001a\r\u0010\u0003\u001a\u00020\u0001*\u00020\u0004H\u0087\b\u001a\r\u0010\u0003\u001a\u00020\u0001*\u00020\u0005H\u0087\b¨\u0006\u0006" }, d2 = { "toAdaptiveIcon", "Landroid/graphics/drawable/Icon;", "Landroid/graphics/Bitmap;", "toIcon", "Landroid/net/Uri;", "", "core-ktx_release" }, k = 2, mv = { 1, 8, 0 }, xi = 48)
public final class IconKt
{
    public static final Icon toAdaptiveIcon(final Bitmap bitmap) {
        return Icon.createWithAdaptiveBitmap(bitmap);
    }
    
    public static final Icon toIcon(final Bitmap bitmap) {
        return Icon.createWithBitmap(bitmap);
    }
    
    public static final Icon toIcon(final Uri uri) {
        return Icon.createWithContentUri(uri);
    }
    
    public static final Icon toIcon(final byte[] array) {
        return Icon.createWithData(array, 0, array.length);
    }
}

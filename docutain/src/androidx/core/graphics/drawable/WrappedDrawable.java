// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.graphics.drawable;

import android.graphics.drawable.Drawable;

public interface WrappedDrawable
{
    Drawable getWrappedDrawable();
    
    void setWrappedDrawable(final Drawable p0);
}

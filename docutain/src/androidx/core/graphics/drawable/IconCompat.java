// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.graphics.drawable;

import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;
import android.graphics.drawable.AdaptiveIconDrawable;
import java.lang.reflect.InvocationTargetException;
import java.io.OutputStream;
import android.graphics.Bitmap$CompressFormat;
import java.io.ByteArrayOutputStream;
import java.nio.charset.Charset;
import java.io.FileNotFoundException;
import java.io.FileInputStream;
import java.io.File;
import android.app.ActivityManager;
import androidx.core.content.ContextCompat;
import android.content.Intent$ShortcutIconResource;
import android.content.Intent;
import java.io.InputStream;
import androidx.core.content.res.ResourcesCompat;
import android.text.TextUtils;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.BitmapFactory;
import android.os.Build$VERSION;
import android.graphics.drawable.Drawable;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager$NameNotFoundException;
import android.content.res.Resources$NotFoundException;
import android.content.res.Resources;
import android.net.Uri;
import androidx.core.util.ObjectsCompat;
import android.graphics.Shader;
import android.graphics.Matrix;
import android.graphics.BitmapShader;
import android.graphics.Shader$TileMode;
import android.graphics.Paint;
import android.graphics.Canvas;
import android.graphics.Bitmap$Config;
import android.graphics.Bitmap;
import androidx.core.util.Preconditions;
import android.graphics.drawable.Icon;
import android.content.Context;
import android.util.Log;
import android.os.Bundle;
import android.content.res.ColorStateList;
import android.os.Parcelable;
import android.graphics.PorterDuff$Mode;
import androidx.versionedparcelable.CustomVersionedParcelable;

public class IconCompat extends CustomVersionedParcelable
{
    private static final float ADAPTIVE_ICON_INSET_FACTOR = 0.25f;
    private static final int AMBIENT_SHADOW_ALPHA = 30;
    private static final float BLUR_FACTOR = 0.010416667f;
    static final PorterDuff$Mode DEFAULT_TINT_MODE;
    private static final float DEFAULT_VIEW_PORT_SCALE = 0.6666667f;
    static final String EXTRA_INT1 = "int1";
    static final String EXTRA_INT2 = "int2";
    static final String EXTRA_OBJ = "obj";
    static final String EXTRA_STRING1 = "string1";
    static final String EXTRA_TINT_LIST = "tint_list";
    static final String EXTRA_TINT_MODE = "tint_mode";
    static final String EXTRA_TYPE = "type";
    private static final float ICON_DIAMETER_FACTOR = 0.9166667f;
    private static final int KEY_SHADOW_ALPHA = 61;
    private static final float KEY_SHADOW_OFFSET_FACTOR = 0.020833334f;
    private static final String TAG = "IconCompat";
    public static final int TYPE_ADAPTIVE_BITMAP = 5;
    public static final int TYPE_BITMAP = 1;
    public static final int TYPE_DATA = 3;
    public static final int TYPE_RESOURCE = 2;
    public static final int TYPE_UNKNOWN = -1;
    public static final int TYPE_URI = 4;
    public static final int TYPE_URI_ADAPTIVE_BITMAP = 6;
    public byte[] mData;
    public int mInt1;
    public int mInt2;
    Object mObj1;
    public Parcelable mParcelable;
    public String mString1;
    public ColorStateList mTintList;
    PorterDuff$Mode mTintMode;
    public String mTintModeStr;
    public int mType;
    
    static {
        DEFAULT_TINT_MODE = PorterDuff$Mode.SRC_IN;
    }
    
    public IconCompat() {
        this.mType = -1;
        this.mData = null;
        this.mParcelable = null;
        this.mInt1 = 0;
        this.mInt2 = 0;
        this.mTintList = null;
        this.mTintMode = IconCompat.DEFAULT_TINT_MODE;
        this.mTintModeStr = null;
    }
    
    IconCompat(final int mType) {
        this.mData = null;
        this.mParcelable = null;
        this.mInt1 = 0;
        this.mInt2 = 0;
        this.mTintList = null;
        this.mTintMode = IconCompat.DEFAULT_TINT_MODE;
        this.mTintModeStr = null;
        this.mType = mType;
    }
    
    public static IconCompat createFromBundle(final Bundle bundle) {
        final int int1 = bundle.getInt("type");
        final IconCompat iconCompat = new IconCompat(int1);
        iconCompat.mInt1 = bundle.getInt("int1");
        iconCompat.mInt2 = bundle.getInt("int2");
        iconCompat.mString1 = bundle.getString("string1");
        if (bundle.containsKey("tint_list")) {
            iconCompat.mTintList = (ColorStateList)bundle.getParcelable("tint_list");
        }
        if (bundle.containsKey("tint_mode")) {
            iconCompat.mTintMode = PorterDuff$Mode.valueOf(bundle.getString("tint_mode"));
        }
        switch (int1) {
            default: {
                final StringBuilder sb = new StringBuilder();
                sb.append("Unknown type ");
                sb.append(int1);
                Log.w("IconCompat", sb.toString());
                return null;
            }
            case 3: {
                iconCompat.mObj1 = bundle.getByteArray("obj");
                break;
            }
            case 2:
            case 4:
            case 6: {
                iconCompat.mObj1 = bundle.getString("obj");
                break;
            }
            case -1:
            case 1:
            case 5: {
                iconCompat.mObj1 = bundle.getParcelable("obj");
                break;
            }
        }
        return iconCompat;
    }
    
    public static IconCompat createFromIcon(final Context context, final Icon icon) {
        Preconditions.checkNotNull(icon);
        return Api23Impl.createFromIcon(context, icon);
    }
    
    public static IconCompat createFromIcon(final Icon icon) {
        return Api23Impl.createFromIconInner(icon);
    }
    
    public static IconCompat createFromIconOrNullIfZeroResId(final Icon icon) {
        if (Api23Impl.getType(icon) == 2 && Api23Impl.getResId(icon) == 0) {
            return null;
        }
        return Api23Impl.createFromIconInner(icon);
    }
    
    static Bitmap createLegacyIconFromAdaptiveIcon(final Bitmap bitmap, final boolean b) {
        final int n = (int)(Math.min(bitmap.getWidth(), bitmap.getHeight()) * 0.6666667f);
        final Bitmap bitmap2 = Bitmap.createBitmap(n, n, Bitmap$Config.ARGB_8888);
        final Canvas canvas = new Canvas(bitmap2);
        final Paint paint = new Paint(3);
        final float n2 = (float)n;
        final float n3 = 0.5f * n2;
        final float n4 = 0.9166667f * n3;
        if (b) {
            final float n5 = 0.010416667f * n2;
            paint.setColor(0);
            paint.setShadowLayer(n5, 0.0f, n2 * 0.020833334f, 1023410176);
            canvas.drawCircle(n3, n3, n4, paint);
            paint.setShadowLayer(n5, 0.0f, 0.0f, 503316480);
            canvas.drawCircle(n3, n3, n4, paint);
            paint.clearShadowLayer();
        }
        paint.setColor(-16777216);
        final BitmapShader shader = new BitmapShader(bitmap, Shader$TileMode.CLAMP, Shader$TileMode.CLAMP);
        final Matrix localMatrix = new Matrix();
        localMatrix.setTranslate(-(bitmap.getWidth() - n) / 2.0f, -(bitmap.getHeight() - n) / 2.0f);
        shader.setLocalMatrix(localMatrix);
        paint.setShader((Shader)shader);
        canvas.drawCircle(n3, n3, n4, paint);
        canvas.setBitmap((Bitmap)null);
        return bitmap2;
    }
    
    public static IconCompat createWithAdaptiveBitmap(final Bitmap mObj1) {
        ObjectsCompat.requireNonNull(mObj1);
        final IconCompat iconCompat = new IconCompat(5);
        iconCompat.mObj1 = mObj1;
        return iconCompat;
    }
    
    public static IconCompat createWithAdaptiveBitmapContentUri(final Uri uri) {
        ObjectsCompat.requireNonNull(uri);
        return createWithAdaptiveBitmapContentUri(uri.toString());
    }
    
    public static IconCompat createWithAdaptiveBitmapContentUri(final String mObj1) {
        ObjectsCompat.requireNonNull(mObj1);
        final IconCompat iconCompat = new IconCompat(6);
        iconCompat.mObj1 = mObj1;
        return iconCompat;
    }
    
    public static IconCompat createWithBitmap(final Bitmap mObj1) {
        ObjectsCompat.requireNonNull(mObj1);
        final IconCompat iconCompat = new IconCompat(1);
        iconCompat.mObj1 = mObj1;
        return iconCompat;
    }
    
    public static IconCompat createWithContentUri(final Uri uri) {
        ObjectsCompat.requireNonNull(uri);
        return createWithContentUri(uri.toString());
    }
    
    public static IconCompat createWithContentUri(final String mObj1) {
        ObjectsCompat.requireNonNull(mObj1);
        final IconCompat iconCompat = new IconCompat(4);
        iconCompat.mObj1 = mObj1;
        return iconCompat;
    }
    
    public static IconCompat createWithData(final byte[] mObj1, final int mInt1, final int mInt2) {
        ObjectsCompat.requireNonNull(mObj1);
        final IconCompat iconCompat = new IconCompat(3);
        iconCompat.mObj1 = mObj1;
        iconCompat.mInt1 = mInt1;
        iconCompat.mInt2 = mInt2;
        return iconCompat;
    }
    
    public static IconCompat createWithResource(final Context context, final int n) {
        ObjectsCompat.requireNonNull(context);
        return createWithResource(context.getResources(), context.getPackageName(), n);
    }
    
    public static IconCompat createWithResource(final Resources resources, final String s, final int mInt1) {
        ObjectsCompat.requireNonNull(s);
        if (mInt1 != 0) {
            final IconCompat iconCompat = new IconCompat(2);
            iconCompat.mInt1 = mInt1;
            Label_0056: {
                if (resources != null) {
                    try {
                        iconCompat.mObj1 = resources.getResourceName(mInt1);
                        break Label_0056;
                    }
                    catch (final Resources$NotFoundException ex) {
                        throw new IllegalArgumentException("Icon resource cannot be found");
                    }
                }
                iconCompat.mObj1 = s;
            }
            iconCompat.mString1 = s;
            return iconCompat;
        }
        throw new IllegalArgumentException("Drawable resource ID must not be 0");
    }
    
    static Resources getResources(final Context context, final String anObject) {
        if ("android".equals(anObject)) {
            return Resources.getSystem();
        }
        final PackageManager packageManager = context.getPackageManager();
        try {
            final ApplicationInfo applicationInfo = packageManager.getApplicationInfo(anObject, 8192);
            if (applicationInfo != null) {
                return packageManager.getResourcesForApplication(applicationInfo);
            }
            return null;
        }
        catch (final PackageManager$NameNotFoundException ex) {
            Log.e("IconCompat", String.format("Unable to find pkg=%s for icon", anObject), (Throwable)ex);
            return null;
        }
    }
    
    private Drawable loadDrawableInner(final Context context) {
        switch (this.mType) {
            case 6: {
                final InputStream uriInputStream = this.getUriInputStream(context);
                if (uriInputStream == null) {
                    break;
                }
                if (Build$VERSION.SDK_INT >= 26) {
                    return Api26Impl.createAdaptiveIconDrawable(null, (Drawable)new BitmapDrawable(context.getResources(), BitmapFactory.decodeStream(uriInputStream)));
                }
                return (Drawable)new BitmapDrawable(context.getResources(), createLegacyIconFromAdaptiveIcon(BitmapFactory.decodeStream(uriInputStream), false));
            }
            case 5: {
                return (Drawable)new BitmapDrawable(context.getResources(), createLegacyIconFromAdaptiveIcon((Bitmap)this.mObj1, false));
            }
            case 4: {
                final InputStream uriInputStream2 = this.getUriInputStream(context);
                if (uriInputStream2 != null) {
                    return (Drawable)new BitmapDrawable(context.getResources(), BitmapFactory.decodeStream(uriInputStream2));
                }
                break;
            }
            case 3: {
                return (Drawable)new BitmapDrawable(context.getResources(), BitmapFactory.decodeByteArray((byte[])this.mObj1, this.mInt1, this.mInt2));
            }
            case 2: {
                String s;
                if (TextUtils.isEmpty((CharSequence)(s = this.getResPackage()))) {
                    s = context.getPackageName();
                }
                final Resources resources = getResources(context, s);
                try {
                    return ResourcesCompat.getDrawable(resources, this.mInt1, context.getTheme());
                }
                catch (final RuntimeException ex) {
                    Log.e("IconCompat", String.format("Unable to load resource 0x%08x from pkg=%s", this.mInt1, this.mObj1), (Throwable)ex);
                    break;
                }
            }
            case 1: {
                return (Drawable)new BitmapDrawable(context.getResources(), (Bitmap)this.mObj1);
            }
        }
        return null;
    }
    
    private static String typeToString(final int n) {
        switch (n) {
            default: {
                return "UNKNOWN";
            }
            case 6: {
                return "URI_MASKABLE";
            }
            case 5: {
                return "BITMAP_MASKABLE";
            }
            case 4: {
                return "URI";
            }
            case 3: {
                return "DATA";
            }
            case 2: {
                return "RESOURCE";
            }
            case 1: {
                return "BITMAP";
            }
        }
    }
    
    public void addToShortcutIntent(final Intent intent, final Drawable drawable, Context packageContext) {
        this.checkResource(packageContext);
        final int mType = this.mType;
        Bitmap bitmap = null;
        Label_0264: {
            if (mType != 1) {
                if (mType != 2) {
                    if (mType == 5) {
                        bitmap = createLegacyIconFromAdaptiveIcon((Bitmap)this.mObj1, true);
                        break Label_0264;
                    }
                    throw new IllegalArgumentException("Icon type not supported for intent shortcuts");
                }
                else {
                    try {
                        packageContext = packageContext.createPackageContext(this.getResPackage(), 0);
                        if (drawable == null) {
                            intent.putExtra("android.intent.extra.shortcut.ICON_RESOURCE", (Parcelable)Intent$ShortcutIconResource.fromContext(packageContext, this.mInt1));
                            return;
                        }
                        final Drawable drawable2 = ContextCompat.getDrawable(packageContext, this.mInt1);
                        if (drawable2.getIntrinsicWidth() > 0 && drawable2.getIntrinsicHeight() > 0) {
                            bitmap = Bitmap.createBitmap(drawable2.getIntrinsicWidth(), drawable2.getIntrinsicHeight(), Bitmap$Config.ARGB_8888);
                        }
                        else {
                            final int launcherLargeIconSize = ((ActivityManager)packageContext.getSystemService("activity")).getLauncherLargeIconSize();
                            bitmap = Bitmap.createBitmap(launcherLargeIconSize, launcherLargeIconSize, Bitmap$Config.ARGB_8888);
                        }
                        drawable2.setBounds(0, 0, bitmap.getWidth(), bitmap.getHeight());
                        drawable2.draw(new Canvas(bitmap));
                        break Label_0264;
                    }
                    catch (final PackageManager$NameNotFoundException cause) {
                        final StringBuilder sb = new StringBuilder();
                        sb.append("Can't find package ");
                        sb.append(this.mObj1);
                        throw new IllegalArgumentException(sb.toString(), (Throwable)cause);
                    }
                }
            }
            final Bitmap bitmap2 = bitmap = (Bitmap)this.mObj1;
            if (drawable != null) {
                bitmap = bitmap2.copy(bitmap2.getConfig(), true);
            }
        }
        if (drawable != null) {
            final int width = bitmap.getWidth();
            final int height = bitmap.getHeight();
            drawable.setBounds(width / 2, height / 2, width, height);
            drawable.draw(new Canvas(bitmap));
        }
        intent.putExtra("android.intent.extra.shortcut.ICON", (Parcelable)bitmap);
    }
    
    public void checkResource(final Context context) {
        if (this.mType == 2) {
            final Object mObj1 = this.mObj1;
            if (mObj1 != null) {
                final String str = (String)mObj1;
                if (!str.contains(":")) {
                    return;
                }
                final String s = str.split(":", -1)[1];
                final String s2 = s.split("/", -1)[0];
                final String anObject = s.split("/", -1)[1];
                final String s3 = str.split(":", -1)[0];
                if ("0_resource_name_obfuscated".equals(anObject)) {
                    Log.i("IconCompat", "Found obfuscated resource, not trying to update resource id for it");
                    return;
                }
                final String resPackage = this.getResPackage();
                final int identifier = getResources(context, resPackage).getIdentifier(anObject, s2, s3);
                if (this.mInt1 != identifier) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Id has changed for ");
                    sb.append(resPackage);
                    sb.append(" ");
                    sb.append(str);
                    Log.i("IconCompat", sb.toString());
                    this.mInt1 = identifier;
                }
            }
        }
    }
    
    public Bitmap getBitmap() {
        if (this.mType == -1 && Build$VERSION.SDK_INT >= 23) {
            final Object mObj1 = this.mObj1;
            if (mObj1 instanceof Bitmap) {
                return (Bitmap)mObj1;
            }
            return null;
        }
        else {
            final int mType = this.mType;
            if (mType == 1) {
                return (Bitmap)this.mObj1;
            }
            if (mType == 5) {
                return createLegacyIconFromAdaptiveIcon((Bitmap)this.mObj1, true);
            }
            final StringBuilder sb = new StringBuilder();
            sb.append("called getBitmap() on ");
            sb.append(this);
            throw new IllegalStateException(sb.toString());
        }
    }
    
    public int getResId() {
        if (this.mType == -1 && Build$VERSION.SDK_INT >= 23) {
            return Api23Impl.getResId(this.mObj1);
        }
        if (this.mType == 2) {
            return this.mInt1;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("called getResId() on ");
        sb.append(this);
        throw new IllegalStateException(sb.toString());
    }
    
    public String getResPackage() {
        if (this.mType == -1 && Build$VERSION.SDK_INT >= 23) {
            return Api23Impl.getResPackage(this.mObj1);
        }
        if (this.mType != 2) {
            final StringBuilder sb = new StringBuilder();
            sb.append("called getResPackage() on ");
            sb.append(this);
            throw new IllegalStateException(sb.toString());
        }
        final String mString1 = this.mString1;
        if (mString1 != null && !TextUtils.isEmpty((CharSequence)mString1)) {
            return this.mString1;
        }
        return ((String)this.mObj1).split(":", -1)[0];
    }
    
    public int getType() {
        if (this.mType == -1 && Build$VERSION.SDK_INT >= 23) {
            return Api23Impl.getType(this.mObj1);
        }
        return this.mType;
    }
    
    public Uri getUri() {
        if (this.mType == -1 && Build$VERSION.SDK_INT >= 23) {
            return Api23Impl.getUri(this.mObj1);
        }
        final int mType = this.mType;
        if (mType != 4 && mType != 6) {
            final StringBuilder sb = new StringBuilder();
            sb.append("called getUri() on ");
            sb.append(this);
            throw new IllegalStateException(sb.toString());
        }
        return Uri.parse((String)this.mObj1);
    }
    
    public InputStream getUriInputStream(final Context context) {
        final Uri uri = this.getUri();
        final String scheme = uri.getScheme();
        if (!"content".equals(scheme)) {
            if (!"file".equals(scheme)) {
                try {
                    return new FileInputStream(new File((String)this.mObj1));
                }
                catch (final FileNotFoundException ex) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Unable to load image from path: ");
                    sb.append(uri);
                    Log.w("IconCompat", sb.toString(), (Throwable)ex);
                    return null;
                }
            }
        }
        try {
            return context.getContentResolver().openInputStream(uri);
        }
        catch (final Exception ex2) {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("Unable to load image from URI: ");
            sb2.append(uri);
            Log.w("IconCompat", sb2.toString(), (Throwable)ex2);
        }
        return null;
    }
    
    public Drawable loadDrawable(final Context context) {
        this.checkResource(context);
        if (Build$VERSION.SDK_INT >= 23) {
            return Api23Impl.loadDrawable(this.toIcon(context), context);
        }
        final Drawable loadDrawableInner = this.loadDrawableInner(context);
        if (loadDrawableInner != null && (this.mTintList != null || this.mTintMode != IconCompat.DEFAULT_TINT_MODE)) {
            loadDrawableInner.mutate();
            DrawableCompat.setTintList(loadDrawableInner, this.mTintList);
            DrawableCompat.setTintMode(loadDrawableInner, this.mTintMode);
        }
        return loadDrawableInner;
    }
    
    @Override
    public void onPostParceling() {
        this.mTintMode = PorterDuff$Mode.valueOf(this.mTintModeStr);
        switch (this.mType) {
            case 3: {
                this.mObj1 = this.mData;
                break;
            }
            case 2:
            case 4:
            case 6: {
                final String mObj1 = new String(this.mData, Charset.forName("UTF-16"));
                this.mObj1 = mObj1;
                if (this.mType == 2 && this.mString1 == null) {
                    final String s = mObj1;
                    this.mString1 = mObj1.split(":", -1)[0];
                    break;
                }
                break;
            }
            case 1:
            case 5: {
                final Parcelable mParcelable = this.mParcelable;
                if (mParcelable != null) {
                    this.mObj1 = mParcelable;
                    break;
                }
                final byte[] mData = this.mData;
                this.mObj1 = mData;
                this.mType = 3;
                this.mInt1 = 0;
                this.mInt2 = mData.length;
                break;
            }
            case -1: {
                final Parcelable mParcelable2 = this.mParcelable;
                if (mParcelable2 != null) {
                    this.mObj1 = mParcelable2;
                    break;
                }
                throw new IllegalArgumentException("Invalid icon");
            }
        }
    }
    
    @Override
    public void onPreParceling(final boolean b) {
        this.mTintModeStr = this.mTintMode.name();
        switch (this.mType) {
            case 4:
            case 6: {
                this.mData = this.mObj1.toString().getBytes(Charset.forName("UTF-16"));
                break;
            }
            case 3: {
                this.mData = (byte[])this.mObj1;
                break;
            }
            case 2: {
                this.mData = ((String)this.mObj1).getBytes(Charset.forName("UTF-16"));
                break;
            }
            case 1:
            case 5: {
                if (b) {
                    final Bitmap bitmap = (Bitmap)this.mObj1;
                    final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap$CompressFormat.PNG, 90, (OutputStream)byteArrayOutputStream);
                    this.mData = byteArrayOutputStream.toByteArray();
                    break;
                }
                this.mParcelable = (Parcelable)this.mObj1;
                break;
            }
            case -1: {
                if (!b) {
                    this.mParcelable = (Parcelable)this.mObj1;
                    break;
                }
                throw new IllegalArgumentException("Can't serialize Icon created with IconCompat#createFromIcon");
            }
        }
    }
    
    public IconCompat setTint(final int n) {
        return this.setTintList(ColorStateList.valueOf(n));
    }
    
    public IconCompat setTintList(final ColorStateList mTintList) {
        this.mTintList = mTintList;
        return this;
    }
    
    public IconCompat setTintMode(final PorterDuff$Mode mTintMode) {
        this.mTintMode = mTintMode;
        return this;
    }
    
    public Bundle toBundle() {
        final Bundle bundle = new Bundle();
        switch (this.mType) {
            default: {
                throw new IllegalArgumentException("Invalid icon");
            }
            case 3: {
                bundle.putByteArray("obj", (byte[])this.mObj1);
                break;
            }
            case 2:
            case 4:
            case 6: {
                bundle.putString("obj", (String)this.mObj1);
                break;
            }
            case 1:
            case 5: {
                bundle.putParcelable("obj", (Parcelable)this.mObj1);
                break;
            }
            case -1: {
                bundle.putParcelable("obj", (Parcelable)this.mObj1);
                break;
            }
        }
        bundle.putInt("type", this.mType);
        bundle.putInt("int1", this.mInt1);
        bundle.putInt("int2", this.mInt2);
        bundle.putString("string1", this.mString1);
        final ColorStateList mTintList = this.mTintList;
        if (mTintList != null) {
            bundle.putParcelable("tint_list", (Parcelable)mTintList);
        }
        final PorterDuff$Mode mTintMode = this.mTintMode;
        if (mTintMode != IconCompat.DEFAULT_TINT_MODE) {
            bundle.putString("tint_mode", mTintMode.name());
        }
        return bundle;
    }
    
    @Deprecated
    public Icon toIcon() {
        return this.toIcon(null);
    }
    
    public Icon toIcon(final Context context) {
        if (Build$VERSION.SDK_INT >= 23) {
            return Api23Impl.toIcon(this, context);
        }
        throw new UnsupportedOperationException("This method is only supported on API level 23+");
    }
    
    @Override
    public String toString() {
        if (this.mType == -1) {
            return String.valueOf(this.mObj1);
        }
        final StringBuilder sb = new StringBuilder("Icon(typ=");
        sb.append(typeToString(this.mType));
        switch (this.mType) {
            case 4:
            case 6: {
                sb.append(" uri=");
                sb.append(this.mObj1);
                break;
            }
            case 3: {
                sb.append(" len=");
                sb.append(this.mInt1);
                if (this.mInt2 != 0) {
                    sb.append(" off=");
                    sb.append(this.mInt2);
                    break;
                }
                break;
            }
            case 2: {
                sb.append(" pkg=");
                sb.append(this.mString1);
                sb.append(" id=");
                sb.append(String.format("0x%08x", this.getResId()));
                break;
            }
            case 1:
            case 5: {
                sb.append(" size=");
                sb.append(((Bitmap)this.mObj1).getWidth());
                sb.append("x");
                sb.append(((Bitmap)this.mObj1).getHeight());
                break;
            }
        }
        if (this.mTintList != null) {
            sb.append(" tint=");
            sb.append(this.mTintList);
        }
        if (this.mTintMode != IconCompat.DEFAULT_TINT_MODE) {
            sb.append(" mode=");
            sb.append(this.mTintMode);
        }
        sb.append(")");
        return sb.toString();
    }
    
    static class Api23Impl
    {
        private Api23Impl() {
        }
        
        static IconCompat createFromIcon(final Context context, final Icon mObj1) {
            final int type = getType(mObj1);
            if (type != 2) {
                if (type == 4) {
                    return IconCompat.createWithContentUri(getUri(mObj1));
                }
                if (type != 6) {
                    final IconCompat iconCompat = new IconCompat(-1);
                    iconCompat.mObj1 = mObj1;
                    return iconCompat;
                }
                return IconCompat.createWithAdaptiveBitmapContentUri(getUri(mObj1));
            }
            else {
                final String resPackage = getResPackage(mObj1);
                try {
                    return IconCompat.createWithResource(IconCompat.getResources(context, resPackage), resPackage, getResId(mObj1));
                }
                catch (final Resources$NotFoundException ex) {
                    throw new IllegalArgumentException("Icon resource cannot be found");
                }
            }
        }
        
        static IconCompat createFromIconInner(final Object mObj1) {
            Preconditions.checkNotNull(mObj1);
            final int type = getType(mObj1);
            if (type == 2) {
                return IconCompat.createWithResource(null, getResPackage(mObj1), getResId(mObj1));
            }
            if (type == 4) {
                return IconCompat.createWithContentUri(getUri(mObj1));
            }
            if (type != 6) {
                final IconCompat iconCompat = new IconCompat(-1);
                iconCompat.mObj1 = mObj1;
                return iconCompat;
            }
            return IconCompat.createWithAdaptiveBitmapContentUri(getUri(mObj1));
        }
        
        static int getResId(final Object obj) {
            if (Build$VERSION.SDK_INT >= 28) {
                return Api28Impl.getResId(obj);
            }
            try {
                return (int)obj.getClass().getMethod("getResId", (Class<?>[])new Class[0]).invoke(obj, new Object[0]);
            }
            catch (final NoSuchMethodException ex) {
                Log.e("IconCompat", "Unable to get icon resource", (Throwable)ex);
                return 0;
            }
            catch (final InvocationTargetException ex2) {
                Log.e("IconCompat", "Unable to get icon resource", (Throwable)ex2);
                return 0;
            }
            catch (final IllegalAccessException ex3) {
                Log.e("IconCompat", "Unable to get icon resource", (Throwable)ex3);
                return 0;
            }
        }
        
        static String getResPackage(final Object obj) {
            if (Build$VERSION.SDK_INT >= 28) {
                return Api28Impl.getResPackage(obj);
            }
            try {
                return (String)obj.getClass().getMethod("getResPackage", (Class<?>[])new Class[0]).invoke(obj, new Object[0]);
            }
            catch (final NoSuchMethodException ex) {
                Log.e("IconCompat", "Unable to get icon package", (Throwable)ex);
                return null;
            }
            catch (final InvocationTargetException ex2) {
                Log.e("IconCompat", "Unable to get icon package", (Throwable)ex2);
                return null;
            }
            catch (final IllegalAccessException ex3) {
                Log.e("IconCompat", "Unable to get icon package", (Throwable)ex3);
                return null;
            }
        }
        
        static int getType(final Object o) {
            if (Build$VERSION.SDK_INT >= 28) {
                return Api28Impl.getType(o);
            }
            try {
                return (int)o.getClass().getMethod("getType", (Class<?>[])new Class[0]).invoke(o, new Object[0]);
            }
            catch (final NoSuchMethodException ex) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Unable to get icon type ");
                sb.append(o);
                Log.e("IconCompat", sb.toString(), (Throwable)ex);
                return -1;
            }
            catch (final InvocationTargetException ex2) {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("Unable to get icon type ");
                sb2.append(o);
                Log.e("IconCompat", sb2.toString(), (Throwable)ex2);
                return -1;
            }
            catch (final IllegalAccessException ex3) {
                final StringBuilder sb3 = new StringBuilder();
                sb3.append("Unable to get icon type ");
                sb3.append(o);
                Log.e("IconCompat", sb3.toString(), (Throwable)ex3);
                return -1;
            }
        }
        
        static Uri getUri(final Object obj) {
            if (Build$VERSION.SDK_INT >= 28) {
                return Api28Impl.getUri(obj);
            }
            try {
                return (Uri)obj.getClass().getMethod("getUri", (Class<?>[])new Class[0]).invoke(obj, new Object[0]);
            }
            catch (final NoSuchMethodException ex) {
                Log.e("IconCompat", "Unable to get icon uri", (Throwable)ex);
                return null;
            }
            catch (final InvocationTargetException ex2) {
                Log.e("IconCompat", "Unable to get icon uri", (Throwable)ex2);
                return null;
            }
            catch (final IllegalAccessException ex3) {
                Log.e("IconCompat", "Unable to get icon uri", (Throwable)ex3);
                return null;
            }
        }
        
        static Drawable loadDrawable(final Icon icon, final Context context) {
            return icon.loadDrawable(context);
        }
        
        static Icon toIcon(final IconCompat iconCompat, final Context context) {
            Icon icon = null;
            switch (iconCompat.mType) {
                default: {
                    throw new IllegalArgumentException("Unknown type");
                }
                case 6: {
                    if (Build$VERSION.SDK_INT >= 30) {
                        icon = Api30Impl.createWithAdaptiveBitmapContentUri(iconCompat.getUri());
                        break;
                    }
                    if (context == null) {
                        final StringBuilder sb = new StringBuilder();
                        sb.append("Context is required to resolve the file uri of the icon: ");
                        sb.append(iconCompat.getUri());
                        throw new IllegalArgumentException(sb.toString());
                    }
                    final InputStream uriInputStream = iconCompat.getUriInputStream(context);
                    if (uriInputStream == null) {
                        final StringBuilder sb2 = new StringBuilder();
                        sb2.append("Cannot load adaptive icon from uri: ");
                        sb2.append(iconCompat.getUri());
                        throw new IllegalStateException(sb2.toString());
                    }
                    if (Build$VERSION.SDK_INT >= 26) {
                        icon = Api26Impl.createWithAdaptiveBitmap(BitmapFactory.decodeStream(uriInputStream));
                        break;
                    }
                    icon = Icon.createWithBitmap(IconCompat.createLegacyIconFromAdaptiveIcon(BitmapFactory.decodeStream(uriInputStream), false));
                    break;
                }
                case 5: {
                    if (Build$VERSION.SDK_INT >= 26) {
                        icon = Api26Impl.createWithAdaptiveBitmap((Bitmap)iconCompat.mObj1);
                        break;
                    }
                    icon = Icon.createWithBitmap(IconCompat.createLegacyIconFromAdaptiveIcon((Bitmap)iconCompat.mObj1, false));
                    break;
                }
                case 4: {
                    icon = Icon.createWithContentUri((String)iconCompat.mObj1);
                    break;
                }
                case 3: {
                    icon = Icon.createWithData((byte[])iconCompat.mObj1, iconCompat.mInt1, iconCompat.mInt2);
                    break;
                }
                case 2: {
                    icon = Icon.createWithResource(iconCompat.getResPackage(), iconCompat.mInt1);
                    break;
                }
                case 1: {
                    icon = Icon.createWithBitmap((Bitmap)iconCompat.mObj1);
                    break;
                }
                case -1: {
                    return (Icon)iconCompat.mObj1;
                }
            }
            if (iconCompat.mTintList != null) {
                icon.setTintList(iconCompat.mTintList);
            }
            if (iconCompat.mTintMode != IconCompat.DEFAULT_TINT_MODE) {
                icon.setTintMode(iconCompat.mTintMode);
            }
            return icon;
        }
    }
    
    static class Api26Impl
    {
        private Api26Impl() {
        }
        
        static Drawable createAdaptiveIconDrawable(final Drawable drawable, final Drawable drawable2) {
            return (Drawable)new AdaptiveIconDrawable(drawable, drawable2);
        }
        
        static Icon createWithAdaptiveBitmap(final Bitmap bitmap) {
            return Icon.createWithAdaptiveBitmap(bitmap);
        }
    }
    
    static class Api28Impl
    {
        private Api28Impl() {
        }
        
        static int getResId(final Object o) {
            return ((Icon)o).getResId();
        }
        
        static String getResPackage(final Object o) {
            return ((Icon)o).getResPackage();
        }
        
        static int getType(final Object o) {
            return ((Icon)o).getType();
        }
        
        static Uri getUri(final Object o) {
            return ((Icon)o).getUri();
        }
    }
    
    static class Api30Impl
    {
        private Api30Impl() {
        }
        
        static Icon createWithAdaptiveBitmapContentUri(final Uri uri) {
            return Icon.createWithAdaptiveBitmapContentUri(uri);
        }
    }
    
    @Retention(RetentionPolicy.SOURCE)
    public @interface IconType {
    }
}

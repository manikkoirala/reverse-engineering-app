// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.graphics;

import android.graphics.Typeface$CustomFallbackBuilder;
import android.graphics.fonts.FontFamily$Builder;
import android.graphics.fonts.Font$Builder;
import java.io.InputStream;
import androidx.core.provider.FontsContractCompat;
import android.os.CancellationSignal;
import android.graphics.Typeface;
import android.content.res.Resources;
import androidx.core.content.res.FontResourcesParserCompat;
import android.content.Context;
import android.graphics.fonts.FontStyle;
import android.graphics.fonts.Font;
import android.graphics.fonts.FontFamily;

public class TypefaceCompatApi29Impl extends TypefaceCompatBaseImpl
{
    private Font findBaseFont(final FontFamily fontFamily, int i) {
        int n;
        if ((i & 0x1) != 0x0) {
            n = 700;
        }
        else {
            n = 400;
        }
        final int n2 = 1;
        if ((i & 0x2) != 0x0) {
            i = 1;
        }
        else {
            i = 0;
        }
        final FontStyle fontStyle = new FontStyle(n, i);
        Font font = fontFamily.getFont(0);
        int matchScore = getMatchScore(fontStyle, font.getStyle());
        Font font2;
        int matchScore2;
        int n3;
        for (i = n2; i < fontFamily.getSize(); ++i, matchScore = n3) {
            font2 = fontFamily.getFont(i);
            matchScore2 = getMatchScore(fontStyle, font2.getStyle());
            if (matchScore2 < (n3 = matchScore)) {
                font = font2;
                n3 = matchScore2;
            }
        }
        return font;
    }
    
    private static int getMatchScore(final FontStyle fontStyle, final FontStyle fontStyle2) {
        final int n = Math.abs(fontStyle.getWeight() - fontStyle2.getWeight()) / 100;
        int n2;
        if (fontStyle.getSlant() == fontStyle2.getSlant()) {
            n2 = 0;
        }
        else {
            n2 = 2;
        }
        return n + n2;
    }
    
    @Override
    public Typeface createFromFontFamilyFilesResourceEntry(final Context p0, final FontResourcesParserCompat.FontFamilyFilesResourceEntry p1, final Resources p2, final int p3) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: astore          8
        //     3: aload_2        
        //     4: invokevirtual   androidx/core/content/res/FontResourcesParserCompat$FontFamilyFilesResourceEntry.getEntries:()[Landroidx/core/content/res/FontResourcesParserCompat$FontFileResourceEntry;
        //     7: astore          9
        //     9: aload           9
        //    11: arraylength    
        //    12: istore          7
        //    14: aconst_null    
        //    15: astore_1       
        //    16: iconst_0       
        //    17: istore          5
        //    19: iload           5
        //    21: iload           7
        //    23: if_icmpge       132
        //    26: aload           9
        //    28: iload           5
        //    30: aaload         
        //    31: astore_2       
        //    32: new             Landroid/graphics/fonts/Font$Builder;
        //    35: astore          10
        //    37: aload           10
        //    39: aload_3        
        //    40: aload_2        
        //    41: invokevirtual   androidx/core/content/res/FontResourcesParserCompat$FontFileResourceEntry.getResourceId:()I
        //    44: invokespecial   android/graphics/fonts/Font$Builder.<init>:(Landroid/content/res/Resources;I)V
        //    47: aload           10
        //    49: aload_2        
        //    50: invokevirtual   androidx/core/content/res/FontResourcesParserCompat$FontFileResourceEntry.getWeight:()I
        //    53: invokevirtual   android/graphics/fonts/Font$Builder.setWeight:(I)Landroid/graphics/fonts/Font$Builder;
        //    56: astore          10
        //    58: aload_2        
        //    59: invokevirtual   androidx/core/content/res/FontResourcesParserCompat$FontFileResourceEntry.isItalic:()Z
        //    62: ifeq            71
        //    65: iconst_1       
        //    66: istore          6
        //    68: goto            74
        //    71: iconst_0       
        //    72: istore          6
        //    74: aload           10
        //    76: iload           6
        //    78: invokevirtual   android/graphics/fonts/Font$Builder.setSlant:(I)Landroid/graphics/fonts/Font$Builder;
        //    81: aload_2        
        //    82: invokevirtual   androidx/core/content/res/FontResourcesParserCompat$FontFileResourceEntry.getTtcIndex:()I
        //    85: invokevirtual   android/graphics/fonts/Font$Builder.setTtcIndex:(I)Landroid/graphics/fonts/Font$Builder;
        //    88: aload_2        
        //    89: invokevirtual   androidx/core/content/res/FontResourcesParserCompat$FontFileResourceEntry.getVariationSettings:()Ljava/lang/String;
        //    92: invokevirtual   android/graphics/fonts/Font$Builder.setFontVariationSettings:(Ljava/lang/String;)Landroid/graphics/fonts/Font$Builder;
        //    95: invokevirtual   android/graphics/fonts/Font$Builder.build:()Landroid/graphics/fonts/Font;
        //    98: astore          10
        //   100: aload_1        
        //   101: ifnonnull       119
        //   104: new             Landroid/graphics/fonts/FontFamily$Builder;
        //   107: astore_2       
        //   108: aload_2        
        //   109: aload           10
        //   111: invokespecial   android/graphics/fonts/FontFamily$Builder.<init>:(Landroid/graphics/fonts/Font;)V
        //   114: aload_2        
        //   115: astore_1       
        //   116: goto            126
        //   119: aload_1        
        //   120: aload           10
        //   122: invokevirtual   android/graphics/fonts/FontFamily$Builder.addFont:(Landroid/graphics/fonts/Font;)Landroid/graphics/fonts/FontFamily$Builder;
        //   125: pop            
        //   126: iinc            5, 1
        //   129: goto            19
        //   132: aload_1        
        //   133: ifnonnull       138
        //   136: aconst_null    
        //   137: areturn        
        //   138: aload_1        
        //   139: invokevirtual   android/graphics/fonts/FontFamily$Builder.build:()Landroid/graphics/fonts/FontFamily;
        //   142: astore_2       
        //   143: new             Landroid/graphics/Typeface$CustomFallbackBuilder;
        //   146: astore_1       
        //   147: aload_1        
        //   148: aload_2        
        //   149: invokespecial   android/graphics/Typeface$CustomFallbackBuilder.<init>:(Landroid/graphics/fonts/FontFamily;)V
        //   152: aload_1        
        //   153: aload_0        
        //   154: aload_2        
        //   155: iload           4
        //   157: invokespecial   androidx/core/graphics/TypefaceCompatApi29Impl.findBaseFont:(Landroid/graphics/fonts/FontFamily;I)Landroid/graphics/fonts/Font;
        //   160: invokevirtual   android/graphics/fonts/Font.getStyle:()Landroid/graphics/fonts/FontStyle;
        //   163: invokevirtual   android/graphics/Typeface$CustomFallbackBuilder.setStyle:(Landroid/graphics/fonts/FontStyle;)Landroid/graphics/Typeface$CustomFallbackBuilder;
        //   166: invokevirtual   android/graphics/Typeface$CustomFallbackBuilder.build:()Landroid/graphics/Typeface;
        //   169: astore_1       
        //   170: aload_1        
        //   171: areturn        
        //   172: astore_1       
        //   173: aload           8
        //   175: astore_1       
        //   176: goto            170
        //   179: astore_2       
        //   180: goto            126
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                 
        //  -----  -----  -----  -----  ---------------------
        //  3      14     172    179    Ljava/lang/Exception;
        //  32     65     179    183    Ljava/io/IOException;
        //  32     65     172    179    Ljava/lang/Exception;
        //  74     100    179    183    Ljava/io/IOException;
        //  74     100    172    179    Ljava/lang/Exception;
        //  104    114    179    183    Ljava/io/IOException;
        //  104    114    172    179    Ljava/lang/Exception;
        //  119    126    179    183    Ljava/io/IOException;
        //  119    126    172    179    Ljava/lang/Exception;
        //  138    170    172    179    Ljava/lang/Exception;
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0071:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2604)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:206)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    @Override
    public Typeface createFromFontInfo(final Context p0, final CancellationSignal p1, final FontsContractCompat.FontInfo[] p2, final int p3) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: invokevirtual   android/content/Context.getContentResolver:()Landroid/content/ContentResolver;
        //     4: astore          9
        //     6: aload_3        
        //     7: arraylength    
        //     8: istore          7
        //    10: aconst_null    
        //    11: astore_1       
        //    12: iconst_0       
        //    13: istore          5
        //    15: iload           5
        //    17: iload           7
        //    19: if_icmpge       215
        //    22: aload_3        
        //    23: iload           5
        //    25: aaload         
        //    26: astore          11
        //    28: aload_1        
        //    29: astore          8
        //    31: aload           9
        //    33: aload           11
        //    35: invokevirtual   androidx/core/provider/FontsContractCompat$FontInfo.getUri:()Landroid/net/Uri;
        //    38: ldc             "r"
        //    40: aload_2        
        //    41: invokevirtual   android/content/ContentResolver.openFileDescriptor:(Landroid/net/Uri;Ljava/lang/String;Landroid/os/CancellationSignal;)Landroid/os/ParcelFileDescriptor;
        //    44: astore          10
        //    46: aload           10
        //    48: ifnonnull       73
        //    51: aload_1        
        //    52: astore          8
        //    54: aload           10
        //    56: ifnull          206
        //    59: aload_1        
        //    60: astore          8
        //    62: aload           10
        //    64: invokevirtual   android/os/ParcelFileDescriptor.close:()V
        //    67: aload_1        
        //    68: astore          8
        //    70: goto            206
        //    73: new             Landroid/graphics/fonts/Font$Builder;
        //    76: astore          8
        //    78: aload           8
        //    80: aload           10
        //    82: invokespecial   android/graphics/fonts/Font$Builder.<init>:(Landroid/os/ParcelFileDescriptor;)V
        //    85: aload           8
        //    87: aload           11
        //    89: invokevirtual   androidx/core/provider/FontsContractCompat$FontInfo.getWeight:()I
        //    92: invokevirtual   android/graphics/fonts/Font$Builder.setWeight:(I)Landroid/graphics/fonts/Font$Builder;
        //    95: astore          8
        //    97: aload           11
        //    99: invokevirtual   androidx/core/provider/FontsContractCompat$FontInfo.isItalic:()Z
        //   102: ifeq            111
        //   105: iconst_1       
        //   106: istore          6
        //   108: goto            114
        //   111: iconst_0       
        //   112: istore          6
        //   114: aload           8
        //   116: iload           6
        //   118: invokevirtual   android/graphics/fonts/Font$Builder.setSlant:(I)Landroid/graphics/fonts/Font$Builder;
        //   121: aload           11
        //   123: invokevirtual   androidx/core/provider/FontsContractCompat$FontInfo.getTtcIndex:()I
        //   126: invokevirtual   android/graphics/fonts/Font$Builder.setTtcIndex:(I)Landroid/graphics/fonts/Font$Builder;
        //   129: invokevirtual   android/graphics/fonts/Font$Builder.build:()Landroid/graphics/fonts/Font;
        //   132: astore          8
        //   134: aload_1        
        //   135: ifnonnull       155
        //   138: new             Landroid/graphics/fonts/FontFamily$Builder;
        //   141: dup            
        //   142: aload           8
        //   144: invokespecial   android/graphics/fonts/FontFamily$Builder.<init>:(Landroid/graphics/fonts/Font;)V
        //   147: astore          8
        //   149: aload           8
        //   151: astore_1       
        //   152: goto            162
        //   155: aload_1        
        //   156: aload           8
        //   158: invokevirtual   android/graphics/fonts/FontFamily$Builder.addFont:(Landroid/graphics/fonts/Font;)Landroid/graphics/fonts/FontFamily$Builder;
        //   161: pop            
        //   162: aload_1        
        //   163: astore          8
        //   165: aload           10
        //   167: ifnull          206
        //   170: goto            59
        //   173: astore          11
        //   175: aload           10
        //   177: ifnull          200
        //   180: aload           10
        //   182: invokevirtual   android/os/ParcelFileDescriptor.close:()V
        //   185: goto            200
        //   188: astore          10
        //   190: aload_1        
        //   191: astore          8
        //   193: aload           11
        //   195: aload           10
        //   197: invokevirtual   java/lang/Throwable.addSuppressed:(Ljava/lang/Throwable;)V
        //   200: aload_1        
        //   201: astore          8
        //   203: aload           11
        //   205: athrow         
        //   206: iinc            5, 1
        //   209: aload           8
        //   211: astore_1       
        //   212: goto            15
        //   215: aload_1        
        //   216: ifnonnull       221
        //   219: aconst_null    
        //   220: areturn        
        //   221: aload_1        
        //   222: invokevirtual   android/graphics/fonts/FontFamily$Builder.build:()Landroid/graphics/fonts/FontFamily;
        //   225: astore_1       
        //   226: new             Landroid/graphics/Typeface$CustomFallbackBuilder;
        //   229: astore_2       
        //   230: aload_2        
        //   231: aload_1        
        //   232: invokespecial   android/graphics/Typeface$CustomFallbackBuilder.<init>:(Landroid/graphics/fonts/FontFamily;)V
        //   235: aload_2        
        //   236: aload_0        
        //   237: aload_1        
        //   238: iload           4
        //   240: invokespecial   androidx/core/graphics/TypefaceCompatApi29Impl.findBaseFont:(Landroid/graphics/fonts/FontFamily;I)Landroid/graphics/fonts/Font;
        //   243: invokevirtual   android/graphics/fonts/Font.getStyle:()Landroid/graphics/fonts/FontStyle;
        //   246: invokevirtual   android/graphics/Typeface$CustomFallbackBuilder.setStyle:(Landroid/graphics/fonts/FontStyle;)Landroid/graphics/Typeface$CustomFallbackBuilder;
        //   249: invokevirtual   android/graphics/Typeface$CustomFallbackBuilder.build:()Landroid/graphics/Typeface;
        //   252: astore_1       
        //   253: aload_1        
        //   254: areturn        
        //   255: astore_1       
        //   256: aconst_null    
        //   257: areturn        
        //   258: astore_1       
        //   259: goto            206
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                 
        //  -----  -----  -----  -----  ---------------------
        //  6      10     255    258    Ljava/lang/Exception;
        //  31     46     258    262    Ljava/io/IOException;
        //  31     46     255    258    Ljava/lang/Exception;
        //  62     67     258    262    Ljava/io/IOException;
        //  62     67     255    258    Ljava/lang/Exception;
        //  73     105    173    206    Any
        //  114    134    173    206    Any
        //  138    149    173    206    Any
        //  155    162    173    206    Any
        //  180    185    188    200    Any
        //  193    200    258    262    Ljava/io/IOException;
        //  193    200    255    258    Ljava/lang/Exception;
        //  203    206    258    262    Ljava/io/IOException;
        //  203    206    255    258    Ljava/lang/Exception;
        //  221    253    255    258    Ljava/lang/Exception;
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0059:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2604)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:206)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    @Override
    protected Typeface createFromInputStream(final Context context, final InputStream inputStream) {
        throw new RuntimeException("Do not use this function in API 29 or later.");
    }
    
    @Override
    public Typeface createFromResourcesFontFile(final Context context, final Resources resources, final int n, final String s, final int n2) {
        try {
            final Font build = new Font$Builder(resources, n).build();
            return new Typeface$CustomFallbackBuilder(new FontFamily$Builder(build).build()).setStyle(build.getStyle()).build();
        }
        catch (final Exception ex) {
            return null;
        }
    }
    
    @Override
    Typeface createWeightStyle(final Context context, final Typeface typeface, final int n, final boolean b) {
        return Typeface.create(typeface, n, b);
    }
    
    @Override
    protected FontsContractCompat.FontInfo findBestInfo(final FontsContractCompat.FontInfo[] array, final int n) {
        throw new RuntimeException("Do not use this function in API 29 or later.");
    }
}

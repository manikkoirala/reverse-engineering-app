// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.graphics;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Executable;
import android.util.Log;
import android.util.SparseArray;
import androidx.collection.LongSparseArray;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import android.graphics.Typeface;
import java.lang.reflect.Constructor;

final class WeightTypefaceApi21
{
    private static final String NATIVE_CREATE_FROM_TYPEFACE_METHOD = "nativeCreateFromTypeface";
    private static final String NATIVE_CREATE_WEIGHT_ALIAS_METHOD = "nativeCreateWeightAlias";
    private static final String NATIVE_INSTANCE_FIELD = "native_instance";
    private static final String TAG = "WeightTypeface";
    private static final Constructor<Typeface> sConstructor;
    private static final Method sNativeCreateFromTypeface;
    private static final Method sNativeCreateWeightAlias;
    private static final Field sNativeInstance;
    private static final Object sWeightCacheLock;
    private static final LongSparseArray<SparseArray<Typeface>> sWeightTypefaceCache;
    
    static {
        Field sNativeInstance2 = null;
        Method declaredMethod = null;
        Executable declaredMethod2 = null;
        Constructor<Typeface> declaredConstructor = null;
        Label_0121: {
            try {
                final Field declaredField = Typeface.class.getDeclaredField("native_instance");
                declaredMethod = Typeface.class.getDeclaredMethod("nativeCreateFromTypeface", Long.TYPE, Integer.TYPE);
                declaredMethod.setAccessible(true);
                declaredMethod2 = Typeface.class.getDeclaredMethod("nativeCreateWeightAlias", Long.TYPE, Integer.TYPE);
                ((Method)declaredMethod2).setAccessible(true);
                declaredConstructor = Typeface.class.getDeclaredConstructor(Long.TYPE);
                declaredConstructor.setAccessible(true);
                sNativeInstance2 = declaredField;
                break Label_0121;
            }
            catch (final NoSuchMethodException declaredMethod) {}
            catch (final NoSuchFieldException ex) {}
            Log.e("WeightTypeface", declaredMethod.getClass().getName(), (Throwable)declaredMethod);
            declaredMethod = null;
            declaredMethod2 = (declaredConstructor = null);
        }
        sNativeInstance = sNativeInstance2;
        sNativeCreateFromTypeface = declaredMethod;
        sNativeCreateWeightAlias = (Method)declaredMethod2;
        sConstructor = declaredConstructor;
        sWeightTypefaceCache = new LongSparseArray<SparseArray<Typeface>>(3);
        sWeightCacheLock = new Object();
    }
    
    private WeightTypefaceApi21() {
    }
    
    private static Typeface create(final long l) {
        try {
            return WeightTypefaceApi21.sConstructor.newInstance(l);
        }
        catch (final IllegalAccessException | InstantiationException | InvocationTargetException ex) {
            return null;
        }
    }
    
    static Typeface createWeightStyle(Typeface typeface, final int n, final boolean b) {
        if (!isPrivateApiAvailable()) {
            return null;
        }
        final int n2 = n << 1 | (b ? 1 : 0);
        synchronized (WeightTypefaceApi21.sWeightCacheLock) {
            final long nativeInstance = getNativeInstance(typeface);
            final LongSparseArray<SparseArray<Typeface>> sWeightTypefaceCache = WeightTypefaceApi21.sWeightTypefaceCache;
            SparseArray sparseArray = sWeightTypefaceCache.get(nativeInstance);
            if (sparseArray == null) {
                sparseArray = new SparseArray(4);
                sWeightTypefaceCache.put(nativeInstance, (SparseArray<Typeface>)sparseArray);
            }
            else {
                final Typeface typeface2 = (Typeface)sparseArray.get(n2);
                if (typeface2 != null) {
                    return typeface2;
                }
            }
            if (b == typeface.isItalic()) {
                typeface = create(nativeCreateWeightAlias(nativeInstance, n));
            }
            else {
                typeface = create(nativeCreateFromTypefaceWithExactStyle(nativeInstance, n, b));
            }
            sparseArray.put(n2, (Object)typeface);
            return typeface;
        }
    }
    
    private static long getNativeInstance(final Typeface obj) {
        try {
            return WeightTypefaceApi21.sNativeInstance.getLong(obj);
        }
        catch (final IllegalAccessException cause) {
            throw new RuntimeException(cause);
        }
    }
    
    private static boolean isPrivateApiAvailable() {
        return WeightTypefaceApi21.sNativeInstance != null;
    }
    
    private static long nativeCreateFromTypefaceWithExactStyle(long n, final int i, final boolean b) {
        int j;
        if (b) {
            j = 2;
        }
        else {
            j = 0;
        }
        try {
            n = (long)WeightTypefaceApi21.sNativeCreateFromTypeface.invoke(null, n, j);
            n = (long)WeightTypefaceApi21.sNativeCreateWeightAlias.invoke(null, n, i);
            return n;
        }
        catch (final InvocationTargetException cause) {
            throw new RuntimeException(cause);
        }
        catch (final IllegalAccessException cause2) {
            throw new RuntimeException(cause2);
        }
    }
    
    private static long nativeCreateWeightAlias(long longValue, final int i) {
        try {
            longValue = (long)WeightTypefaceApi21.sNativeCreateWeightAlias.invoke(null, longValue, i);
            return longValue;
        }
        catch (final InvocationTargetException cause) {
            throw new RuntimeException(cause);
        }
        catch (final IllegalAccessException cause2) {
            throw new RuntimeException(cause2);
        }
    }
}

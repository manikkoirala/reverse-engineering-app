// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.graphics;

import android.graphics.BlendMode;
import android.graphics.PorterDuff$Mode;
import android.graphics.Xfermode;
import android.graphics.PorterDuffXfermode;
import android.os.Build$VERSION;
import android.graphics.Paint;
import android.graphics.Rect;
import androidx.core.util.Pair;

public final class PaintCompat
{
    private static final String EM_STRING = "m";
    private static final String TOFU_STRING = "\udb3f\udffd";
    private static final ThreadLocal<Pair<Rect, Rect>> sRectThreadLocal;
    
    static {
        sRectThreadLocal = new ThreadLocal<Pair<Rect, Rect>>();
    }
    
    private PaintCompat() {
    }
    
    public static boolean hasGlyph(final Paint paint, final String s) {
        if (Build$VERSION.SDK_INT >= 23) {
            return Api23Impl.hasGlyph(paint, s);
        }
        final int length = s.length();
        if (length == 1 && Character.isWhitespace(s.charAt(0))) {
            return true;
        }
        final float measureText = paint.measureText("\udb3f\udffd");
        final float measureText2 = paint.measureText("m");
        final float measureText3 = paint.measureText(s);
        float n = 0.0f;
        if (measureText3 == 0.0f) {
            return false;
        }
        if (s.codePointCount(0, s.length()) > 1) {
            if (measureText3 > measureText2 * 2.0f) {
                return false;
            }
            int n2;
            for (int i = 0; i < length; i = n2) {
                n2 = Character.charCount(s.codePointAt(i)) + i;
                n += paint.measureText(s, i, n2);
            }
            if (measureText3 >= n) {
                return false;
            }
        }
        if (measureText3 != measureText) {
            return true;
        }
        final Pair<Rect, Rect> obtainEmptyRects = obtainEmptyRects();
        paint.getTextBounds("\udb3f\udffd", 0, 2, (Rect)obtainEmptyRects.first);
        paint.getTextBounds(s, 0, length, (Rect)obtainEmptyRects.second);
        return obtainEmptyRects.first.equals((Object)obtainEmptyRects.second) ^ true;
    }
    
    private static Pair<Rect, Rect> obtainEmptyRects() {
        final ThreadLocal<Pair<Rect, Rect>> sRectThreadLocal = PaintCompat.sRectThreadLocal;
        Pair value = sRectThreadLocal.get();
        if (value == null) {
            value = new Pair(new Rect(), new Rect());
            sRectThreadLocal.set(value);
        }
        else {
            ((Rect)value.first).setEmpty();
            ((Rect)value.second).setEmpty();
        }
        return value;
    }
    
    public static boolean setBlendMode(final Paint paint, final BlendModeCompat blendModeCompat) {
        final int sdk_INT = Build$VERSION.SDK_INT;
        boolean b = true;
        final Xfermode xfermode = null;
        final Object o = null;
        if (sdk_INT >= 29) {
            Object obtainBlendModeFromCompat = o;
            if (blendModeCompat != null) {
                obtainBlendModeFromCompat = BlendModeUtils.Api29Impl.obtainBlendModeFromCompat(blendModeCompat);
            }
            Api29Impl.setBlendMode(paint, obtainBlendModeFromCompat);
            return true;
        }
        if (blendModeCompat != null) {
            final PorterDuff$Mode obtainPorterDuffFromCompat = BlendModeUtils.obtainPorterDuffFromCompat(blendModeCompat);
            Object xfermode2 = xfermode;
            if (obtainPorterDuffFromCompat != null) {
                xfermode2 = new PorterDuffXfermode(obtainPorterDuffFromCompat);
            }
            paint.setXfermode((Xfermode)xfermode2);
            if (obtainPorterDuffFromCompat == null) {
                b = false;
            }
            return b;
        }
        paint.setXfermode((Xfermode)null);
        return true;
    }
    
    static class Api23Impl
    {
        private Api23Impl() {
        }
        
        static boolean hasGlyph(final Paint paint, final String s) {
            return paint.hasGlyph(s);
        }
    }
    
    static class Api29Impl
    {
        private Api29Impl() {
        }
        
        static void setBlendMode(final Paint paint, final Object o) {
            paint.setBlendMode((BlendMode)o);
        }
    }
}

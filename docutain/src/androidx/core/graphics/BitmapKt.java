// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.graphics;

import android.graphics.ColorSpace$Named;
import android.graphics.ColorSpace;
import android.graphics.Bitmap$Config;
import android.graphics.PointF;
import android.graphics.Point;
import kotlin.Unit;
import android.graphics.Canvas;
import kotlin.jvm.functions.Function1;
import android.graphics.Bitmap;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000D\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\b\u001a#\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00032\b\b\u0002\u0010\u0005\u001a\u00020\u0006H\u0086\b\u001a7\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00032\b\b\u0002\u0010\u0005\u001a\u00020\u00062\b\b\u0002\u0010\u0007\u001a\u00020\b2\b\b\u0002\u0010\t\u001a\u00020\nH\u0087\b\u001a&\u0010\u000b\u001a\u00020\u0001*\u00020\u00012\u0017\u0010\f\u001a\u0013\u0012\u0004\u0012\u00020\u000e\u0012\u0004\u0012\u00020\u000f0\r¢\u0006\u0002\b\u0010H\u0086\b\u001a\u0015\u0010\u0011\u001a\u00020\b*\u00020\u00012\u0006\u0010\u0012\u001a\u00020\u0013H\u0086\n\u001a\u0015\u0010\u0011\u001a\u00020\b*\u00020\u00012\u0006\u0010\u0012\u001a\u00020\u0014H\u0086\n\u001a\u001d\u0010\u0015\u001a\u00020\u0003*\u00020\u00012\u0006\u0010\u0016\u001a\u00020\u00032\u0006\u0010\u0017\u001a\u00020\u0003H\u0086\n\u001a'\u0010\u0018\u001a\u00020\u0001*\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00032\b\b\u0002\u0010\u0019\u001a\u00020\bH\u0086\b\u001a'\u0010\u001a\u001a\u00020\u000f*\u00020\u00012\u0006\u0010\u0016\u001a\u00020\u00032\u0006\u0010\u0017\u001a\u00020\u00032\b\b\u0001\u0010\u001b\u001a\u00020\u0003H\u0086\n¨\u0006\u001c" }, d2 = { "createBitmap", "Landroid/graphics/Bitmap;", "width", "", "height", "config", "Landroid/graphics/Bitmap$Config;", "hasAlpha", "", "colorSpace", "Landroid/graphics/ColorSpace;", "applyCanvas", "block", "Lkotlin/Function1;", "Landroid/graphics/Canvas;", "", "Lkotlin/ExtensionFunctionType;", "contains", "p", "Landroid/graphics/Point;", "Landroid/graphics/PointF;", "get", "x", "y", "scale", "filter", "set", "color", "core-ktx_release" }, k = 2, mv = { 1, 8, 0 }, xi = 48)
public final class BitmapKt
{
    public static final Bitmap applyCanvas(final Bitmap bitmap, final Function1<? super Canvas, Unit> function1) {
        function1.invoke((Object)new Canvas(bitmap));
        return bitmap;
    }
    
    public static final boolean contains(final Bitmap bitmap, final Point point) {
        final int width = bitmap.getWidth();
        final int x = point.x;
        boolean b = true;
        if (x < 0 || x >= width || point.y < 0 || point.y >= bitmap.getHeight()) {
            b = false;
        }
        return b;
    }
    
    public static final boolean contains(final Bitmap bitmap, final PointF pointF) {
        return pointF.x >= 0.0f && pointF.x < bitmap.getWidth() && pointF.y >= 0.0f && pointF.y < bitmap.getHeight();
    }
    
    public static final Bitmap createBitmap(final int n, final int n2, final Bitmap$Config bitmap$Config) {
        return Bitmap.createBitmap(n, n2, bitmap$Config);
    }
    
    public static final Bitmap createBitmap(final int n, final int n2, final Bitmap$Config bitmap$Config, final boolean b, final ColorSpace colorSpace) {
        return Bitmap.createBitmap(n, n2, bitmap$Config, b, colorSpace);
    }
    
    public static final int get(final Bitmap bitmap, final int n, final int n2) {
        return bitmap.getPixel(n, n2);
    }
    
    public static final Bitmap scale(final Bitmap bitmap, final int n, final int n2, final boolean b) {
        return Bitmap.createScaledBitmap(bitmap, n, n2, b);
    }
    
    public static final void set(final Bitmap bitmap, final int n, final int n2, final int n3) {
        bitmap.setPixel(n, n2, n3);
    }
}

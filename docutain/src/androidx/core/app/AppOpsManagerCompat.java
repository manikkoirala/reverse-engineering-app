// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.app;

import android.app.AppOpsManager;
import android.os.Binder;
import android.os.Build$VERSION;
import android.content.Context;

public final class AppOpsManagerCompat
{
    public static final int MODE_ALLOWED = 0;
    public static final int MODE_DEFAULT = 3;
    public static final int MODE_ERRORED = 2;
    public static final int MODE_IGNORED = 1;
    
    private AppOpsManagerCompat() {
    }
    
    public static int checkOrNoteProxyOp(final Context context, final int n, final String s, final String s2) {
        if (Build$VERSION.SDK_INT < 29) {
            return noteProxyOpNoThrow(context, s, s2);
        }
        final AppOpsManager systemService = Api29Impl.getSystemService(context);
        final int checkOpNoThrow = Api29Impl.checkOpNoThrow(systemService, s, Binder.getCallingUid(), s2);
        if (checkOpNoThrow != 0) {
            return checkOpNoThrow;
        }
        return Api29Impl.checkOpNoThrow(systemService, s, n, Api29Impl.getOpPackageName(context));
    }
    
    public static int noteOp(final Context context, final String s, final int n, final String s2) {
        if (Build$VERSION.SDK_INT >= 19) {
            return Api19Impl.noteOp((AppOpsManager)context.getSystemService("appops"), s, n, s2);
        }
        return 1;
    }
    
    public static int noteOpNoThrow(final Context context, final String s, final int n, final String s2) {
        if (Build$VERSION.SDK_INT >= 19) {
            return Api19Impl.noteOpNoThrow((AppOpsManager)context.getSystemService("appops"), s, n, s2);
        }
        return 1;
    }
    
    public static int noteProxyOp(final Context context, final String s, final String s2) {
        if (Build$VERSION.SDK_INT >= 23) {
            return Api23Impl.noteProxyOp(Api23Impl.getSystemService(context, AppOpsManager.class), s, s2);
        }
        return 1;
    }
    
    public static int noteProxyOpNoThrow(final Context context, final String s, final String s2) {
        if (Build$VERSION.SDK_INT >= 23) {
            return Api23Impl.noteProxyOpNoThrow(Api23Impl.getSystemService(context, AppOpsManager.class), s, s2);
        }
        return 1;
    }
    
    public static String permissionToOp(final String s) {
        if (Build$VERSION.SDK_INT >= 23) {
            return Api23Impl.permissionToOp(s);
        }
        return null;
    }
    
    static class Api19Impl
    {
        private Api19Impl() {
        }
        
        static int noteOp(final AppOpsManager appOpsManager, final String s, final int n, final String s2) {
            return appOpsManager.noteOp(s, n, s2);
        }
        
        static int noteOpNoThrow(final AppOpsManager appOpsManager, final String s, final int n, final String s2) {
            return appOpsManager.noteOpNoThrow(s, n, s2);
        }
    }
    
    static class Api23Impl
    {
        private Api23Impl() {
        }
        
        static <T> T getSystemService(final Context context, final Class<T> clazz) {
            return (T)context.getSystemService((Class)clazz);
        }
        
        static int noteProxyOp(final AppOpsManager appOpsManager, final String s, final String s2) {
            return appOpsManager.noteProxyOp(s, s2);
        }
        
        static int noteProxyOpNoThrow(final AppOpsManager appOpsManager, final String s, final String s2) {
            return appOpsManager.noteProxyOpNoThrow(s, s2);
        }
        
        static String permissionToOp(final String s) {
            return AppOpsManager.permissionToOp(s);
        }
    }
    
    static class Api29Impl
    {
        private Api29Impl() {
        }
        
        static int checkOpNoThrow(final AppOpsManager appOpsManager, final String s, final int n, final String s2) {
            if (appOpsManager == null) {
                return 1;
            }
            return appOpsManager.checkOpNoThrow(s, n, s2);
        }
        
        static String getOpPackageName(final Context context) {
            return context.getOpPackageName();
        }
        
        static AppOpsManager getSystemService(final Context context) {
            return (AppOpsManager)context.getSystemService((Class)AppOpsManager.class);
        }
    }
}

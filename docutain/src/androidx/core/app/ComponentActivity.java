// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.app;

import androidx.lifecycle.ReportFragment;
import android.os.Bundle;
import androidx.lifecycle.Lifecycle;
import android.view.View;
import android.view.Window$Callback;
import android.view.KeyEvent;
import android.os.Build$VERSION;
import androidx.lifecycle.LifecycleRegistry;
import androidx.collection.SimpleArrayMap;
import androidx.core.view.KeyEventDispatcher;
import androidx.lifecycle.LifecycleOwner;
import android.app.Activity;

public class ComponentActivity extends Activity implements LifecycleOwner, Component
{
    private SimpleArrayMap<Class<? extends ExtraData>, ExtraData> mExtraDataMap;
    private LifecycleRegistry mLifecycleRegistry;
    
    public ComponentActivity() {
        this.mExtraDataMap = new SimpleArrayMap<Class<? extends ExtraData>, ExtraData>();
        this.mLifecycleRegistry = new LifecycleRegistry(this);
    }
    
    private static boolean shouldSkipDump(final String[] array) {
        final boolean b = false;
        final boolean b2 = false;
        final boolean b3 = false;
        final boolean b4 = false;
        boolean b5 = b3;
        if (array != null) {
            b5 = b3;
            if (array.length > 0) {
                final String s = array[0];
                s.hashCode();
                int n = -1;
                switch (s) {
                    case "--autofill": {
                        n = 4;
                        break;
                    }
                    case "--contentcapture": {
                        n = 3;
                        break;
                    }
                    case "--list-dumpables": {
                        n = 2;
                        break;
                    }
                    case "--dump-dumpable": {
                        n = 1;
                        break;
                    }
                    case "--translation": {
                        n = 0;
                        break;
                    }
                    default:
                        break;
                }
                switch (n) {
                    default: {
                        b5 = b3;
                        break;
                    }
                    case 4: {
                        boolean b6 = b4;
                        if (Build$VERSION.SDK_INT >= 26) {
                            b6 = true;
                        }
                        return b6;
                    }
                    case 3: {
                        boolean b7 = b;
                        if (Build$VERSION.SDK_INT >= 29) {
                            b7 = true;
                        }
                        return b7;
                    }
                    case 1:
                    case 2: {
                        boolean b8 = b2;
                        if (Build$VERSION.SDK_INT >= 33) {
                            b8 = true;
                        }
                        return b8;
                    }
                    case 0: {
                        b5 = b3;
                        if (Build$VERSION.SDK_INT >= 31) {
                            b5 = true;
                            break;
                        }
                        break;
                    }
                }
            }
        }
        return b5;
    }
    
    public boolean dispatchKeyEvent(final KeyEvent keyEvent) {
        final View decorView = this.getWindow().getDecorView();
        return (decorView != null && KeyEventDispatcher.dispatchBeforeHierarchy(decorView, keyEvent)) || KeyEventDispatcher.dispatchKeyEvent((KeyEventDispatcher.Component)this, decorView, (Window$Callback)this, keyEvent);
    }
    
    public boolean dispatchKeyShortcutEvent(final KeyEvent keyEvent) {
        final View decorView = this.getWindow().getDecorView();
        return (decorView != null && KeyEventDispatcher.dispatchBeforeHierarchy(decorView, keyEvent)) || super.dispatchKeyShortcutEvent(keyEvent);
    }
    
    @Deprecated
    public <T extends ExtraData> T getExtraData(final Class<T> clazz) {
        return (T)this.mExtraDataMap.get(clazz);
    }
    
    public Lifecycle getLifecycle() {
        return this.mLifecycleRegistry;
    }
    
    protected void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        ReportFragment.injectIfNeededIn(this);
    }
    
    protected void onSaveInstanceState(final Bundle bundle) {
        this.mLifecycleRegistry.markState(Lifecycle.State.CREATED);
        super.onSaveInstanceState(bundle);
    }
    
    @Deprecated
    public void putExtraData(final ExtraData extraData) {
        this.mExtraDataMap.put(extraData.getClass(), extraData);
    }
    
    protected final boolean shouldDumpInternalState(final String[] array) {
        return shouldSkipDump(array) ^ true;
    }
    
    public boolean superDispatchKeyEvent(final KeyEvent keyEvent) {
        return super.dispatchKeyEvent(keyEvent);
    }
    
    @Deprecated
    public static class ExtraData
    {
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.app;

import android.app.AlarmManager$AlarmClockInfo;
import android.os.Build$VERSION;
import android.app.PendingIntent;
import android.app.AlarmManager;

public final class AlarmManagerCompat
{
    private AlarmManagerCompat() {
    }
    
    public static void setAlarmClock(final AlarmManager alarmManager, final long n, final PendingIntent pendingIntent, final PendingIntent pendingIntent2) {
        if (Build$VERSION.SDK_INT >= 21) {
            Api21Impl.setAlarmClock(alarmManager, Api21Impl.createAlarmClockInfo(n, pendingIntent), pendingIntent2);
        }
        else {
            setExact(alarmManager, 0, n, pendingIntent2);
        }
    }
    
    public static void setAndAllowWhileIdle(final AlarmManager alarmManager, final int n, final long n2, final PendingIntent pendingIntent) {
        if (Build$VERSION.SDK_INT >= 23) {
            Api23Impl.setAndAllowWhileIdle(alarmManager, n, n2, pendingIntent);
        }
        else {
            alarmManager.set(n, n2, pendingIntent);
        }
    }
    
    public static void setExact(final AlarmManager alarmManager, final int n, final long n2, final PendingIntent pendingIntent) {
        if (Build$VERSION.SDK_INT >= 19) {
            Api19Impl.setExact(alarmManager, n, n2, pendingIntent);
        }
        else {
            alarmManager.set(n, n2, pendingIntent);
        }
    }
    
    public static void setExactAndAllowWhileIdle(final AlarmManager alarmManager, final int n, final long n2, final PendingIntent pendingIntent) {
        if (Build$VERSION.SDK_INT >= 23) {
            Api23Impl.setExactAndAllowWhileIdle(alarmManager, n, n2, pendingIntent);
        }
        else {
            setExact(alarmManager, n, n2, pendingIntent);
        }
    }
    
    static class Api19Impl
    {
        private Api19Impl() {
        }
        
        static void setExact(final AlarmManager alarmManager, final int n, final long n2, final PendingIntent pendingIntent) {
            alarmManager.setExact(n, n2, pendingIntent);
        }
    }
    
    static class Api21Impl
    {
        private Api21Impl() {
        }
        
        static AlarmManager$AlarmClockInfo createAlarmClockInfo(final long n, final PendingIntent pendingIntent) {
            return new AlarmManager$AlarmClockInfo(n, pendingIntent);
        }
        
        static void setAlarmClock(final AlarmManager alarmManager, final Object o, final PendingIntent pendingIntent) {
            alarmManager.setAlarmClock((AlarmManager$AlarmClockInfo)o, pendingIntent);
        }
    }
    
    static class Api23Impl
    {
        private Api23Impl() {
        }
        
        static void setAndAllowWhileIdle(final AlarmManager alarmManager, final int n, final long n2, final PendingIntent pendingIntent) {
            alarmManager.setAndAllowWhileIdle(n, n2, pendingIntent);
        }
        
        static void setExactAndAllowWhileIdle(final AlarmManager alarmManager, final int n, final long n2, final PendingIntent pendingIntent) {
            alarmManager.setExactAndAllowWhileIdle(n, n2, pendingIntent);
        }
    }
}

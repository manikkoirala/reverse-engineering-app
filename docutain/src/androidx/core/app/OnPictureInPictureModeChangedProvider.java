// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.app;

import androidx.core.util.Consumer;

public interface OnPictureInPictureModeChangedProvider
{
    void addOnPictureInPictureModeChangedListener(final Consumer<PictureInPictureModeChangedInfo> p0);
    
    void removeOnPictureInPictureModeChangedListener(final Consumer<PictureInPictureModeChangedInfo> p0);
}

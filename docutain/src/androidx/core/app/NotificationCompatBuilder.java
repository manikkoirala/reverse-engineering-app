// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.app;

import android.content.LocusId;
import android.app.Notification$BubbleMetadata;
import android.media.AudioAttributes;
import android.app.PendingIntent;
import android.app.Notification$Action;
import android.util.SparseArray;
import java.util.Collection;
import androidx.collection.ArraySet;
import android.app.Notification$Action$Builder;
import androidx.core.graphics.drawable.IconCompat;
import java.util.Iterator;
import android.graphics.drawable.Icon;
import android.graphics.Bitmap;
import android.app.Notification;
import android.net.Uri;
import android.text.TextUtils;
import android.os.Build$VERSION;
import java.util.ArrayList;
import android.content.Context;
import android.app.Notification$Builder;
import android.widget.RemoteViews;
import android.os.Bundle;
import java.util.List;

class NotificationCompatBuilder implements NotificationBuilderWithBuilderAccessor
{
    private final List<Bundle> mActionExtrasList;
    private RemoteViews mBigContentView;
    private final Notification$Builder mBuilder;
    private final NotificationCompat.Builder mBuilderCompat;
    private RemoteViews mContentView;
    private final Context mContext;
    private final Bundle mExtras;
    private int mGroupAlertBehavior;
    private RemoteViews mHeadsUpContentView;
    
    NotificationCompatBuilder(final NotificationCompat.Builder mBuilderCompat) {
        this.mActionExtrasList = new ArrayList<Bundle>();
        this.mExtras = new Bundle();
        this.mBuilderCompat = mBuilderCompat;
        final Context mContext = mBuilderCompat.mContext;
        this.mContext = mContext;
        if (Build$VERSION.SDK_INT >= 26) {
            this.mBuilder = Api26Impl.createBuilder(mBuilderCompat.mContext, mBuilderCompat.mChannelId);
        }
        else {
            this.mBuilder = new Notification$Builder(mBuilderCompat.mContext);
        }
        final Notification mNotification = mBuilderCompat.mNotification;
        this.mBuilder.setWhen(mNotification.when).setSmallIcon(mNotification.icon, mNotification.iconLevel).setContent(mNotification.contentView).setTicker(mNotification.tickerText, mBuilderCompat.mTickerView).setVibrate(mNotification.vibrate).setLights(mNotification.ledARGB, mNotification.ledOnMS, mNotification.ledOffMS).setOngoing((mNotification.flags & 0x2) != 0x0).setOnlyAlertOnce((mNotification.flags & 0x8) != 0x0).setAutoCancel((mNotification.flags & 0x10) != 0x0).setDefaults(mNotification.defaults).setContentTitle(mBuilderCompat.mContentTitle).setContentText(mBuilderCompat.mContentText).setContentInfo(mBuilderCompat.mContentInfo).setContentIntent(mBuilderCompat.mContentIntent).setDeleteIntent(mNotification.deleteIntent).setFullScreenIntent(mBuilderCompat.mFullScreenIntent, (mNotification.flags & 0x80) != 0x0).setNumber(mBuilderCompat.mNumber).setProgress(mBuilderCompat.mProgressMax, mBuilderCompat.mProgress, mBuilderCompat.mProgressIndeterminate);
        if (Build$VERSION.SDK_INT < 23) {
            final Notification$Builder mBuilder = this.mBuilder;
            Bitmap bitmap;
            if (mBuilderCompat.mLargeIcon == null) {
                bitmap = null;
            }
            else {
                bitmap = mBuilderCompat.mLargeIcon.getBitmap();
            }
            mBuilder.setLargeIcon(bitmap);
        }
        else {
            final Notification$Builder mBuilder2 = this.mBuilder;
            Icon icon;
            if (mBuilderCompat.mLargeIcon == null) {
                icon = null;
            }
            else {
                icon = mBuilderCompat.mLargeIcon.toIcon(mContext);
            }
            Api23Impl.setLargeIcon(mBuilder2, icon);
        }
        if (Build$VERSION.SDK_INT < 21) {
            this.mBuilder.setSound(mNotification.sound, mNotification.audioStreamType);
        }
        if (Build$VERSION.SDK_INT >= 16) {
            Api16Impl.setPriority(Api16Impl.setUsesChronometer(Api16Impl.setSubText(this.mBuilder, mBuilderCompat.mSubText), mBuilderCompat.mUseChronometer), mBuilderCompat.mPriority);
            if (Build$VERSION.SDK_INT >= 20 && mBuilderCompat.mStyle instanceof NotificationCompat.CallStyle) {
                final Iterator<Object> iterator = ((NotificationCompat.CallStyle)mBuilderCompat.mStyle).getActionsListWithSystemActions().iterator();
                while (iterator.hasNext()) {
                    this.addAction(iterator.next());
                }
            }
            else {
                final Iterator<NotificationCompat.Action> iterator2 = mBuilderCompat.mActions.iterator();
                while (iterator2.hasNext()) {
                    this.addAction(iterator2.next());
                }
            }
            if (mBuilderCompat.mExtras != null) {
                this.mExtras.putAll(mBuilderCompat.mExtras);
            }
            if (Build$VERSION.SDK_INT < 20) {
                if (mBuilderCompat.mLocalOnly) {
                    this.mExtras.putBoolean("android.support.localOnly", true);
                }
                if (mBuilderCompat.mGroupKey != null) {
                    this.mExtras.putString("android.support.groupKey", mBuilderCompat.mGroupKey);
                    if (mBuilderCompat.mGroupSummary) {
                        this.mExtras.putBoolean("android.support.isGroupSummary", true);
                    }
                    else {
                        this.mExtras.putBoolean("android.support.useSideChannel", true);
                    }
                }
                if (mBuilderCompat.mSortKey != null) {
                    this.mExtras.putString("android.support.sortKey", mBuilderCompat.mSortKey);
                }
            }
            this.mContentView = mBuilderCompat.mContentView;
            this.mBigContentView = mBuilderCompat.mBigContentView;
        }
        if (Build$VERSION.SDK_INT >= 17) {
            Api17Impl.setShowWhen(this.mBuilder, mBuilderCompat.mShowWhen);
        }
        if (Build$VERSION.SDK_INT >= 19 && Build$VERSION.SDK_INT < 21) {
            final List<String> combineLists = combineLists(getPeople(mBuilderCompat.mPersonList), mBuilderCompat.mPeople);
            if (combineLists != null && !combineLists.isEmpty()) {
                this.mExtras.putStringArray("android.people", (String[])combineLists.toArray(new String[combineLists.size()]));
            }
        }
        if (Build$VERSION.SDK_INT >= 20) {
            Api20Impl.setLocalOnly(this.mBuilder, mBuilderCompat.mLocalOnly);
            Api20Impl.setGroup(this.mBuilder, mBuilderCompat.mGroupKey);
            Api20Impl.setSortKey(this.mBuilder, mBuilderCompat.mSortKey);
            Api20Impl.setGroupSummary(this.mBuilder, mBuilderCompat.mGroupSummary);
            this.mGroupAlertBehavior = mBuilderCompat.mGroupAlertBehavior;
        }
        if (Build$VERSION.SDK_INT >= 21) {
            Api21Impl.setCategory(this.mBuilder, mBuilderCompat.mCategory);
            Api21Impl.setColor(this.mBuilder, mBuilderCompat.mColor);
            Api21Impl.setVisibility(this.mBuilder, mBuilderCompat.mVisibility);
            Api21Impl.setPublicVersion(this.mBuilder, mBuilderCompat.mPublicVersion);
            Api21Impl.setSound(this.mBuilder, mNotification.sound, mNotification.audioAttributes);
            List<String> list;
            if (Build$VERSION.SDK_INT < 28) {
                list = combineLists(getPeople(mBuilderCompat.mPersonList), mBuilderCompat.mPeople);
            }
            else {
                list = mBuilderCompat.mPeople;
            }
            if (list != null && !list.isEmpty()) {
                final Iterator iterator3 = list.iterator();
                while (iterator3.hasNext()) {
                    Api21Impl.addPerson(this.mBuilder, (String)iterator3.next());
                }
            }
            this.mHeadsUpContentView = mBuilderCompat.mHeadsUpContentView;
            if (mBuilderCompat.mInvisibleActions.size() > 0) {
                Bundle bundle;
                if ((bundle = mBuilderCompat.getExtras().getBundle("android.car.EXTENSIONS")) == null) {
                    bundle = new Bundle();
                }
                final Bundle bundle2 = new Bundle(bundle);
                final Bundle bundle3 = new Bundle();
                for (int i = 0; i < mBuilderCompat.mInvisibleActions.size(); ++i) {
                    bundle3.putBundle(Integer.toString(i), NotificationCompatJellybean.getBundleForAction(mBuilderCompat.mInvisibleActions.get(i)));
                }
                bundle.putBundle("invisible_actions", bundle3);
                bundle2.putBundle("invisible_actions", bundle3);
                mBuilderCompat.getExtras().putBundle("android.car.EXTENSIONS", bundle);
                this.mExtras.putBundle("android.car.EXTENSIONS", bundle2);
            }
        }
        if (Build$VERSION.SDK_INT >= 23 && mBuilderCompat.mSmallIcon != null) {
            Api23Impl.setSmallIcon(this.mBuilder, mBuilderCompat.mSmallIcon);
        }
        if (Build$VERSION.SDK_INT >= 24) {
            Api19Impl.setExtras(this.mBuilder, mBuilderCompat.mExtras);
            Api24Impl.setRemoteInputHistory(this.mBuilder, mBuilderCompat.mRemoteInputHistory);
            if (mBuilderCompat.mContentView != null) {
                Api24Impl.setCustomContentView(this.mBuilder, mBuilderCompat.mContentView);
            }
            if (mBuilderCompat.mBigContentView != null) {
                Api24Impl.setCustomBigContentView(this.mBuilder, mBuilderCompat.mBigContentView);
            }
            if (mBuilderCompat.mHeadsUpContentView != null) {
                Api24Impl.setCustomHeadsUpContentView(this.mBuilder, mBuilderCompat.mHeadsUpContentView);
            }
        }
        if (Build$VERSION.SDK_INT >= 26) {
            Api26Impl.setBadgeIconType(this.mBuilder, mBuilderCompat.mBadgeIcon);
            Api26Impl.setSettingsText(this.mBuilder, mBuilderCompat.mSettingsText);
            Api26Impl.setShortcutId(this.mBuilder, mBuilderCompat.mShortcutId);
            Api26Impl.setTimeoutAfter(this.mBuilder, mBuilderCompat.mTimeout);
            Api26Impl.setGroupAlertBehavior(this.mBuilder, mBuilderCompat.mGroupAlertBehavior);
            if (mBuilderCompat.mColorizedSet) {
                Api26Impl.setColorized(this.mBuilder, mBuilderCompat.mColorized);
            }
            if (!TextUtils.isEmpty((CharSequence)mBuilderCompat.mChannelId)) {
                this.mBuilder.setSound((Uri)null).setDefaults(0).setLights(0, 0, 0).setVibrate((long[])null);
            }
        }
        if (Build$VERSION.SDK_INT >= 28) {
            final Iterator<Person> iterator4 = mBuilderCompat.mPersonList.iterator();
            while (iterator4.hasNext()) {
                Api28Impl.addPerson(this.mBuilder, iterator4.next().toAndroidPerson());
            }
        }
        if (Build$VERSION.SDK_INT >= 29) {
            Api29Impl.setAllowSystemGeneratedContextualActions(this.mBuilder, mBuilderCompat.mAllowSystemGeneratedContextualActions);
            Api29Impl.setBubbleMetadata(this.mBuilder, NotificationCompat.BubbleMetadata.toPlatform(mBuilderCompat.mBubbleMetadata));
            if (mBuilderCompat.mLocusId != null) {
                Api29Impl.setLocusId(this.mBuilder, mBuilderCompat.mLocusId.toLocusId());
            }
        }
        if (Build$VERSION.SDK_INT >= 31 && mBuilderCompat.mFgsDeferBehavior != 0) {
            Api31Impl.setForegroundServiceBehavior(this.mBuilder, mBuilderCompat.mFgsDeferBehavior);
        }
        if (mBuilderCompat.mSilent) {
            if (this.mBuilderCompat.mGroupSummary) {
                this.mGroupAlertBehavior = 2;
            }
            else {
                this.mGroupAlertBehavior = 1;
            }
            this.mBuilder.setVibrate((long[])null);
            this.mBuilder.setSound((Uri)null);
            mNotification.defaults &= 0xFFFFFFFE;
            mNotification.defaults &= 0xFFFFFFFD;
            this.mBuilder.setDefaults(mNotification.defaults);
            if (Build$VERSION.SDK_INT >= 26) {
                if (TextUtils.isEmpty((CharSequence)this.mBuilderCompat.mGroupKey)) {
                    Api20Impl.setGroup(this.mBuilder, "silent");
                }
                Api26Impl.setGroupAlertBehavior(this.mBuilder, this.mGroupAlertBehavior);
            }
        }
    }
    
    private void addAction(final NotificationCompat.Action action) {
        if (Build$VERSION.SDK_INT >= 20) {
            final IconCompat iconCompat = action.getIconCompat();
            final int sdk_INT = Build$VERSION.SDK_INT;
            final int n = 0;
            Notification$Action$Builder notification$Action$Builder;
            if (sdk_INT >= 23) {
                Icon icon;
                if (iconCompat != null) {
                    icon = iconCompat.toIcon();
                }
                else {
                    icon = null;
                }
                notification$Action$Builder = Api23Impl.createBuilder(icon, action.getTitle(), action.getActionIntent());
            }
            else {
                int resId;
                if (iconCompat != null) {
                    resId = iconCompat.getResId();
                }
                else {
                    resId = 0;
                }
                notification$Action$Builder = Api20Impl.createBuilder(resId, action.getTitle(), action.getActionIntent());
            }
            if (action.getRemoteInputs() != null) {
                final android.app.RemoteInput[] fromCompat = RemoteInput.fromCompat(action.getRemoteInputs());
                for (int length = fromCompat.length, i = n; i < length; ++i) {
                    Api20Impl.addRemoteInput(notification$Action$Builder, fromCompat[i]);
                }
            }
            Bundle bundle;
            if (action.getExtras() != null) {
                bundle = new Bundle(action.getExtras());
            }
            else {
                bundle = new Bundle();
            }
            bundle.putBoolean("android.support.allowGeneratedReplies", action.getAllowGeneratedReplies());
            if (Build$VERSION.SDK_INT >= 24) {
                Api24Impl.setAllowGeneratedReplies(notification$Action$Builder, action.getAllowGeneratedReplies());
            }
            bundle.putInt("android.support.action.semanticAction", action.getSemanticAction());
            if (Build$VERSION.SDK_INT >= 28) {
                Api28Impl.setSemanticAction(notification$Action$Builder, action.getSemanticAction());
            }
            if (Build$VERSION.SDK_INT >= 29) {
                Api29Impl.setContextual(notification$Action$Builder, action.isContextual());
            }
            if (Build$VERSION.SDK_INT >= 31) {
                Api31Impl.setAuthenticationRequired(notification$Action$Builder, action.isAuthenticationRequired());
            }
            bundle.putBoolean("android.support.action.showsUserInterface", action.getShowsUserInterface());
            Api20Impl.addExtras(notification$Action$Builder, bundle);
            Api20Impl.addAction(this.mBuilder, Api20Impl.build(notification$Action$Builder));
        }
        else if (Build$VERSION.SDK_INT >= 16) {
            this.mActionExtrasList.add(NotificationCompatJellybean.writeActionAndGetExtras(this.mBuilder, action));
        }
    }
    
    private static List<String> combineLists(final List<String> list, final List<String> list2) {
        if (list == null) {
            return list2;
        }
        if (list2 == null) {
            return list;
        }
        final ArraySet c = new ArraySet(list.size() + list2.size());
        c.addAll(list);
        c.addAll(list2);
        return new ArrayList<String>(c);
    }
    
    private static List<String> getPeople(final List<Person> list) {
        if (list == null) {
            return null;
        }
        final ArrayList list2 = new ArrayList(list.size());
        final Iterator iterator = list.iterator();
        while (iterator.hasNext()) {
            list2.add(((Person)iterator.next()).resolveToLegacyUri());
        }
        return list2;
    }
    
    private void removeSoundAndVibration(final Notification notification) {
        notification.sound = null;
        notification.vibrate = null;
        notification.defaults &= 0xFFFFFFFE;
        notification.defaults &= 0xFFFFFFFD;
    }
    
    public Notification build() {
        final NotificationCompat.Style mStyle = this.mBuilderCompat.mStyle;
        if (mStyle != null) {
            mStyle.apply(this);
        }
        RemoteViews contentView;
        if (mStyle != null) {
            contentView = mStyle.makeContentView(this);
        }
        else {
            contentView = null;
        }
        final Notification buildInternal = this.buildInternal();
        if (contentView != null) {
            buildInternal.contentView = contentView;
        }
        else if (this.mBuilderCompat.mContentView != null) {
            buildInternal.contentView = this.mBuilderCompat.mContentView;
        }
        if (Build$VERSION.SDK_INT >= 16 && mStyle != null) {
            final RemoteViews bigContentView = mStyle.makeBigContentView(this);
            if (bigContentView != null) {
                buildInternal.bigContentView = bigContentView;
            }
        }
        if (Build$VERSION.SDK_INT >= 21 && mStyle != null) {
            final RemoteViews headsUpContentView = this.mBuilderCompat.mStyle.makeHeadsUpContentView(this);
            if (headsUpContentView != null) {
                buildInternal.headsUpContentView = headsUpContentView;
            }
        }
        if (Build$VERSION.SDK_INT >= 16 && mStyle != null) {
            final Bundle extras = NotificationCompat.getExtras(buildInternal);
            if (extras != null) {
                mStyle.addCompatExtras(extras);
            }
        }
        return buildInternal;
    }
    
    protected Notification buildInternal() {
        if (Build$VERSION.SDK_INT >= 26) {
            return Api16Impl.build(this.mBuilder);
        }
        if (Build$VERSION.SDK_INT >= 24) {
            final Notification build = Api16Impl.build(this.mBuilder);
            if (this.mGroupAlertBehavior != 0) {
                if (Api20Impl.getGroup(build) != null && (build.flags & 0x200) != 0x0 && this.mGroupAlertBehavior == 2) {
                    this.removeSoundAndVibration(build);
                }
                if (Api20Impl.getGroup(build) != null && (build.flags & 0x200) == 0x0 && this.mGroupAlertBehavior == 1) {
                    this.removeSoundAndVibration(build);
                }
            }
            return build;
        }
        if (Build$VERSION.SDK_INT >= 21) {
            Api19Impl.setExtras(this.mBuilder, this.mExtras);
            final Notification build2 = Api16Impl.build(this.mBuilder);
            final RemoteViews mContentView = this.mContentView;
            if (mContentView != null) {
                build2.contentView = mContentView;
            }
            final RemoteViews mBigContentView = this.mBigContentView;
            if (mBigContentView != null) {
                build2.bigContentView = mBigContentView;
            }
            final RemoteViews mHeadsUpContentView = this.mHeadsUpContentView;
            if (mHeadsUpContentView != null) {
                build2.headsUpContentView = mHeadsUpContentView;
            }
            if (this.mGroupAlertBehavior != 0) {
                if (Api20Impl.getGroup(build2) != null && (build2.flags & 0x200) != 0x0 && this.mGroupAlertBehavior == 2) {
                    this.removeSoundAndVibration(build2);
                }
                if (Api20Impl.getGroup(build2) != null && (build2.flags & 0x200) == 0x0 && this.mGroupAlertBehavior == 1) {
                    this.removeSoundAndVibration(build2);
                }
            }
            return build2;
        }
        if (Build$VERSION.SDK_INT >= 20) {
            Api19Impl.setExtras(this.mBuilder, this.mExtras);
            final Notification build3 = Api16Impl.build(this.mBuilder);
            final RemoteViews mContentView2 = this.mContentView;
            if (mContentView2 != null) {
                build3.contentView = mContentView2;
            }
            final RemoteViews mBigContentView2 = this.mBigContentView;
            if (mBigContentView2 != null) {
                build3.bigContentView = mBigContentView2;
            }
            if (this.mGroupAlertBehavior != 0) {
                if (Api20Impl.getGroup(build3) != null && (build3.flags & 0x200) != 0x0 && this.mGroupAlertBehavior == 2) {
                    this.removeSoundAndVibration(build3);
                }
                if (Api20Impl.getGroup(build3) != null && (build3.flags & 0x200) == 0x0 && this.mGroupAlertBehavior == 1) {
                    this.removeSoundAndVibration(build3);
                }
            }
            return build3;
        }
        if (Build$VERSION.SDK_INT >= 19) {
            final SparseArray<Bundle> buildActionExtrasMap = NotificationCompatJellybean.buildActionExtrasMap(this.mActionExtrasList);
            if (buildActionExtrasMap != null) {
                this.mExtras.putSparseParcelableArray("android.support.actionExtras", (SparseArray)buildActionExtrasMap);
            }
            Api19Impl.setExtras(this.mBuilder, this.mExtras);
            final Notification build4 = Api16Impl.build(this.mBuilder);
            final RemoteViews mContentView3 = this.mContentView;
            if (mContentView3 != null) {
                build4.contentView = mContentView3;
            }
            final RemoteViews mBigContentView3 = this.mBigContentView;
            if (mBigContentView3 != null) {
                build4.bigContentView = mBigContentView3;
            }
            return build4;
        }
        if (Build$VERSION.SDK_INT >= 16) {
            final Notification build5 = Api16Impl.build(this.mBuilder);
            final Bundle extras = NotificationCompat.getExtras(build5);
            final Bundle bundle = new Bundle(this.mExtras);
            for (final String s : this.mExtras.keySet()) {
                if (extras.containsKey(s)) {
                    bundle.remove(s);
                }
            }
            extras.putAll(bundle);
            final SparseArray<Bundle> buildActionExtrasMap2 = NotificationCompatJellybean.buildActionExtrasMap(this.mActionExtrasList);
            if (buildActionExtrasMap2 != null) {
                NotificationCompat.getExtras(build5).putSparseParcelableArray("android.support.actionExtras", (SparseArray)buildActionExtrasMap2);
            }
            final RemoteViews mContentView4 = this.mContentView;
            if (mContentView4 != null) {
                build5.contentView = mContentView4;
            }
            final RemoteViews mBigContentView4 = this.mBigContentView;
            if (mBigContentView4 != null) {
                build5.bigContentView = mBigContentView4;
            }
            return build5;
        }
        return this.mBuilder.getNotification();
    }
    
    @Override
    public Notification$Builder getBuilder() {
        return this.mBuilder;
    }
    
    Context getContext() {
        return this.mContext;
    }
    
    static class Api16Impl
    {
        private Api16Impl() {
        }
        
        static Notification build(final Notification$Builder notification$Builder) {
            return notification$Builder.build();
        }
        
        static Notification$Builder setPriority(final Notification$Builder notification$Builder, final int priority) {
            return notification$Builder.setPriority(priority);
        }
        
        static Notification$Builder setSubText(final Notification$Builder notification$Builder, final CharSequence subText) {
            return notification$Builder.setSubText(subText);
        }
        
        static Notification$Builder setUsesChronometer(final Notification$Builder notification$Builder, final boolean usesChronometer) {
            return notification$Builder.setUsesChronometer(usesChronometer);
        }
    }
    
    static class Api17Impl
    {
        private Api17Impl() {
        }
        
        static Notification$Builder setShowWhen(final Notification$Builder notification$Builder, final boolean showWhen) {
            return notification$Builder.setShowWhen(showWhen);
        }
    }
    
    static class Api19Impl
    {
        private Api19Impl() {
        }
        
        static Notification$Builder setExtras(final Notification$Builder notification$Builder, final Bundle extras) {
            return notification$Builder.setExtras(extras);
        }
    }
    
    static class Api20Impl
    {
        private Api20Impl() {
        }
        
        static Notification$Builder addAction(final Notification$Builder notification$Builder, final Notification$Action notification$Action) {
            return notification$Builder.addAction(notification$Action);
        }
        
        static Notification$Action$Builder addExtras(final Notification$Action$Builder notification$Action$Builder, final Bundle bundle) {
            return notification$Action$Builder.addExtras(bundle);
        }
        
        static Notification$Action$Builder addRemoteInput(final Notification$Action$Builder notification$Action$Builder, final android.app.RemoteInput remoteInput) {
            return notification$Action$Builder.addRemoteInput(remoteInput);
        }
        
        static Notification$Action build(final Notification$Action$Builder notification$Action$Builder) {
            return notification$Action$Builder.build();
        }
        
        static Notification$Action$Builder createBuilder(final int n, final CharSequence charSequence, final PendingIntent pendingIntent) {
            return new Notification$Action$Builder(n, charSequence, pendingIntent);
        }
        
        static String getGroup(final Notification notification) {
            return notification.getGroup();
        }
        
        static Notification$Builder setGroup(final Notification$Builder notification$Builder, final String group) {
            return notification$Builder.setGroup(group);
        }
        
        static Notification$Builder setGroupSummary(final Notification$Builder notification$Builder, final boolean groupSummary) {
            return notification$Builder.setGroupSummary(groupSummary);
        }
        
        static Notification$Builder setLocalOnly(final Notification$Builder notification$Builder, final boolean localOnly) {
            return notification$Builder.setLocalOnly(localOnly);
        }
        
        static Notification$Builder setSortKey(final Notification$Builder notification$Builder, final String sortKey) {
            return notification$Builder.setSortKey(sortKey);
        }
    }
    
    static class Api21Impl
    {
        private Api21Impl() {
        }
        
        static Notification$Builder addPerson(final Notification$Builder notification$Builder, final String s) {
            return notification$Builder.addPerson(s);
        }
        
        static Notification$Builder setCategory(final Notification$Builder notification$Builder, final String category) {
            return notification$Builder.setCategory(category);
        }
        
        static Notification$Builder setColor(final Notification$Builder notification$Builder, final int color) {
            return notification$Builder.setColor(color);
        }
        
        static Notification$Builder setPublicVersion(final Notification$Builder notification$Builder, final Notification publicVersion) {
            return notification$Builder.setPublicVersion(publicVersion);
        }
        
        static Notification$Builder setSound(final Notification$Builder notification$Builder, final Uri uri, final Object o) {
            return notification$Builder.setSound(uri, (AudioAttributes)o);
        }
        
        static Notification$Builder setVisibility(final Notification$Builder notification$Builder, final int visibility) {
            return notification$Builder.setVisibility(visibility);
        }
    }
    
    static class Api23Impl
    {
        private Api23Impl() {
        }
        
        static Notification$Action$Builder createBuilder(final Icon icon, final CharSequence charSequence, final PendingIntent pendingIntent) {
            return new Notification$Action$Builder(icon, charSequence, pendingIntent);
        }
        
        static Notification$Builder setLargeIcon(final Notification$Builder notification$Builder, final Icon largeIcon) {
            return notification$Builder.setLargeIcon(largeIcon);
        }
        
        static Notification$Builder setSmallIcon(final Notification$Builder notification$Builder, final Object o) {
            return notification$Builder.setSmallIcon((Icon)o);
        }
    }
    
    static class Api24Impl
    {
        private Api24Impl() {
        }
        
        static Notification$Action$Builder setAllowGeneratedReplies(final Notification$Action$Builder notification$Action$Builder, final boolean allowGeneratedReplies) {
            return notification$Action$Builder.setAllowGeneratedReplies(allowGeneratedReplies);
        }
        
        static Notification$Builder setCustomBigContentView(final Notification$Builder notification$Builder, final RemoteViews customBigContentView) {
            return notification$Builder.setCustomBigContentView(customBigContentView);
        }
        
        static Notification$Builder setCustomContentView(final Notification$Builder notification$Builder, final RemoteViews customContentView) {
            return notification$Builder.setCustomContentView(customContentView);
        }
        
        static Notification$Builder setCustomHeadsUpContentView(final Notification$Builder notification$Builder, final RemoteViews customHeadsUpContentView) {
            return notification$Builder.setCustomHeadsUpContentView(customHeadsUpContentView);
        }
        
        static Notification$Builder setRemoteInputHistory(final Notification$Builder notification$Builder, final CharSequence[] remoteInputHistory) {
            return notification$Builder.setRemoteInputHistory(remoteInputHistory);
        }
    }
    
    static class Api26Impl
    {
        private Api26Impl() {
        }
        
        static Notification$Builder createBuilder(final Context context, final String s) {
            return new Notification$Builder(context, s);
        }
        
        static Notification$Builder setBadgeIconType(final Notification$Builder notification$Builder, final int badgeIconType) {
            return notification$Builder.setBadgeIconType(badgeIconType);
        }
        
        static Notification$Builder setColorized(final Notification$Builder notification$Builder, final boolean colorized) {
            return notification$Builder.setColorized(colorized);
        }
        
        static Notification$Builder setGroupAlertBehavior(final Notification$Builder notification$Builder, final int groupAlertBehavior) {
            return notification$Builder.setGroupAlertBehavior(groupAlertBehavior);
        }
        
        static Notification$Builder setSettingsText(final Notification$Builder notification$Builder, final CharSequence settingsText) {
            return notification$Builder.setSettingsText(settingsText);
        }
        
        static Notification$Builder setShortcutId(final Notification$Builder notification$Builder, final String shortcutId) {
            return notification$Builder.setShortcutId(shortcutId);
        }
        
        static Notification$Builder setTimeoutAfter(final Notification$Builder notification$Builder, final long timeoutAfter) {
            return notification$Builder.setTimeoutAfter(timeoutAfter);
        }
    }
    
    static class Api28Impl
    {
        private Api28Impl() {
        }
        
        static Notification$Builder addPerson(final Notification$Builder notification$Builder, final android.app.Person person) {
            return notification$Builder.addPerson(person);
        }
        
        static Notification$Action$Builder setSemanticAction(final Notification$Action$Builder notification$Action$Builder, final int semanticAction) {
            return notification$Action$Builder.setSemanticAction(semanticAction);
        }
    }
    
    static class Api29Impl
    {
        private Api29Impl() {
        }
        
        static Notification$Builder setAllowSystemGeneratedContextualActions(final Notification$Builder notification$Builder, final boolean allowSystemGeneratedContextualActions) {
            return notification$Builder.setAllowSystemGeneratedContextualActions(allowSystemGeneratedContextualActions);
        }
        
        static Notification$Builder setBubbleMetadata(final Notification$Builder notification$Builder, final Notification$BubbleMetadata bubbleMetadata) {
            return notification$Builder.setBubbleMetadata(bubbleMetadata);
        }
        
        static Notification$Action$Builder setContextual(final Notification$Action$Builder notification$Action$Builder, final boolean contextual) {
            return notification$Action$Builder.setContextual(contextual);
        }
        
        static Notification$Builder setLocusId(final Notification$Builder notification$Builder, final Object o) {
            return notification$Builder.setLocusId((LocusId)o);
        }
    }
    
    static class Api31Impl
    {
        private Api31Impl() {
        }
        
        static Notification$Action$Builder setAuthenticationRequired(final Notification$Action$Builder notification$Action$Builder, final boolean authenticationRequired) {
            return notification$Action$Builder.setAuthenticationRequired(authenticationRequired);
        }
        
        static Notification$Builder setForegroundServiceBehavior(final Notification$Builder notification$Builder, final int foregroundServiceBehavior) {
            return notification$Builder.setForegroundServiceBehavior(foregroundServiceBehavior);
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.app;

import android.app.LocaleManager;
import android.os.LocaleList;
import android.content.res.Resources;
import java.util.Locale;
import android.content.res.Configuration;
import android.os.Build$VERSION;
import androidx.core.os.LocaleListCompat;
import android.content.Context;

public final class LocaleManagerCompat
{
    private LocaleManagerCompat() {
    }
    
    public static LocaleListCompat getApplicationLocales(final Context context) {
        if (Build$VERSION.SDK_INT < 33) {
            return LocaleListCompat.forLanguageTags(AppLocalesStorageHelper.readLocales(context));
        }
        final Object localeManagerForApplication = getLocaleManagerForApplication(context);
        if (localeManagerForApplication != null) {
            return LocaleListCompat.wrap(Api33Impl.localeManagerGetApplicationLocales(localeManagerForApplication));
        }
        return LocaleListCompat.getEmptyLocaleList();
    }
    
    static LocaleListCompat getConfigurationLocales(final Configuration configuration) {
        if (Build$VERSION.SDK_INT >= 24) {
            return Api24Impl.getLocales(configuration);
        }
        if (Build$VERSION.SDK_INT >= 21) {
            return LocaleListCompat.forLanguageTags(Api21Impl.toLanguageTag(configuration.locale));
        }
        return LocaleListCompat.create(configuration.locale);
    }
    
    private static Object getLocaleManagerForApplication(final Context context) {
        return context.getSystemService("locale");
    }
    
    public static LocaleListCompat getSystemLocales(final Context context) {
        final LocaleListCompat emptyLocaleList = LocaleListCompat.getEmptyLocaleList();
        LocaleListCompat localeListCompat;
        if (Build$VERSION.SDK_INT >= 33) {
            final Object localeManagerForApplication = getLocaleManagerForApplication(context);
            localeListCompat = emptyLocaleList;
            if (localeManagerForApplication != null) {
                localeListCompat = LocaleListCompat.wrap(Api33Impl.localeManagerGetSystemLocales(localeManagerForApplication));
            }
        }
        else {
            localeListCompat = getConfigurationLocales(Resources.getSystem().getConfiguration());
        }
        return localeListCompat;
    }
    
    static class Api21Impl
    {
        private Api21Impl() {
        }
        
        static String toLanguageTag(final Locale locale) {
            return locale.toLanguageTag();
        }
    }
    
    static class Api24Impl
    {
        private Api24Impl() {
        }
        
        static LocaleListCompat getLocales(final Configuration configuration) {
            return LocaleListCompat.forLanguageTags(configuration.getLocales().toLanguageTags());
        }
    }
    
    static class Api33Impl
    {
        private Api33Impl() {
        }
        
        static LocaleList localeManagerGetApplicationLocales(final Object o) {
            return ((LocaleManager)o).getApplicationLocales();
        }
        
        static LocaleList localeManagerGetSystemLocales(final Object o) {
            return ((LocaleManager)o).getSystemLocales();
        }
    }
}

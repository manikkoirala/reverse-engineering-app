// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.app;

import java.util.Iterator;
import java.util.ArrayList;
import androidx.core.util.Preconditions;
import android.os.Build$VERSION;
import android.app.NotificationChannel;
import java.util.Collections;
import android.app.NotificationChannelGroup;
import java.util.List;

public class NotificationChannelGroupCompat
{
    private boolean mBlocked;
    private List<NotificationChannelCompat> mChannels;
    String mDescription;
    final String mId;
    CharSequence mName;
    
    NotificationChannelGroupCompat(final NotificationChannelGroup notificationChannelGroup) {
        this(notificationChannelGroup, Collections.emptyList());
    }
    
    NotificationChannelGroupCompat(final NotificationChannelGroup notificationChannelGroup, final List<NotificationChannel> list) {
        this(Api26Impl.getId(notificationChannelGroup));
        this.mName = Api26Impl.getName(notificationChannelGroup);
        if (Build$VERSION.SDK_INT >= 28) {
            this.mDescription = Api28Impl.getDescription(notificationChannelGroup);
        }
        if (Build$VERSION.SDK_INT >= 28) {
            this.mBlocked = Api28Impl.isBlocked(notificationChannelGroup);
            this.mChannels = this.getChannelsCompat(Api26Impl.getChannels(notificationChannelGroup));
        }
        else {
            this.mChannels = this.getChannelsCompat(list);
        }
    }
    
    NotificationChannelGroupCompat(final String s) {
        this.mChannels = Collections.emptyList();
        this.mId = Preconditions.checkNotNull(s);
    }
    
    private List<NotificationChannelCompat> getChannelsCompat(final List<NotificationChannel> list) {
        final ArrayList list2 = new ArrayList();
        for (final NotificationChannel notificationChannel : list) {
            if (this.mId.equals(Api26Impl.getGroup(notificationChannel))) {
                list2.add(new NotificationChannelCompat(notificationChannel));
            }
        }
        return list2;
    }
    
    public List<NotificationChannelCompat> getChannels() {
        return this.mChannels;
    }
    
    public String getDescription() {
        return this.mDescription;
    }
    
    public String getId() {
        return this.mId;
    }
    
    public CharSequence getName() {
        return this.mName;
    }
    
    NotificationChannelGroup getNotificationChannelGroup() {
        if (Build$VERSION.SDK_INT < 26) {
            return null;
        }
        final NotificationChannelGroup notificationChannelGroup = Api26Impl.createNotificationChannelGroup(this.mId, this.mName);
        if (Build$VERSION.SDK_INT >= 28) {
            Api28Impl.setDescription(notificationChannelGroup, this.mDescription);
        }
        return notificationChannelGroup;
    }
    
    public boolean isBlocked() {
        return this.mBlocked;
    }
    
    public Builder toBuilder() {
        return new Builder(this.mId).setName(this.mName).setDescription(this.mDescription);
    }
    
    static class Api26Impl
    {
        private Api26Impl() {
        }
        
        static NotificationChannelGroup createNotificationChannelGroup(final String s, final CharSequence charSequence) {
            return new NotificationChannelGroup(s, charSequence);
        }
        
        static List<NotificationChannel> getChannels(final NotificationChannelGroup notificationChannelGroup) {
            return notificationChannelGroup.getChannels();
        }
        
        static String getGroup(final NotificationChannel notificationChannel) {
            return notificationChannel.getGroup();
        }
        
        static String getId(final NotificationChannelGroup notificationChannelGroup) {
            return notificationChannelGroup.getId();
        }
        
        static CharSequence getName(final NotificationChannelGroup notificationChannelGroup) {
            return notificationChannelGroup.getName();
        }
    }
    
    static class Api28Impl
    {
        private Api28Impl() {
        }
        
        static String getDescription(final NotificationChannelGroup notificationChannelGroup) {
            return notificationChannelGroup.getDescription();
        }
        
        static boolean isBlocked(final NotificationChannelGroup notificationChannelGroup) {
            return notificationChannelGroup.isBlocked();
        }
        
        static void setDescription(final NotificationChannelGroup notificationChannelGroup, final String description) {
            notificationChannelGroup.setDescription(description);
        }
    }
    
    public static class Builder
    {
        final NotificationChannelGroupCompat mGroup;
        
        public Builder(final String s) {
            this.mGroup = new NotificationChannelGroupCompat(s);
        }
        
        public NotificationChannelGroupCompat build() {
            return this.mGroup;
        }
        
        public Builder setDescription(final String mDescription) {
            this.mGroup.mDescription = mDescription;
            return this;
        }
        
        public Builder setName(final CharSequence mName) {
            this.mGroup.mName = mName;
            return this;
        }
    }
}

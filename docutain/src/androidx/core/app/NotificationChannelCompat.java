// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.app;

import android.app.Notification;
import androidx.core.util.Preconditions;
import android.provider.Settings$System;
import android.os.Build$VERSION;
import android.app.NotificationChannel;
import android.net.Uri;
import android.media.AudioAttributes;

public class NotificationChannelCompat
{
    public static final String DEFAULT_CHANNEL_ID = "miscellaneous";
    private static final int DEFAULT_LIGHT_COLOR = 0;
    private static final boolean DEFAULT_SHOW_BADGE = true;
    AudioAttributes mAudioAttributes;
    private boolean mBypassDnd;
    private boolean mCanBubble;
    String mConversationId;
    String mDescription;
    String mGroupId;
    final String mId;
    int mImportance;
    private boolean mImportantConversation;
    int mLightColor;
    boolean mLights;
    private int mLockscreenVisibility;
    CharSequence mName;
    String mParentId;
    boolean mShowBadge;
    Uri mSound;
    boolean mVibrationEnabled;
    long[] mVibrationPattern;
    
    NotificationChannelCompat(final NotificationChannel notificationChannel) {
        this(Api26Impl.getId(notificationChannel), Api26Impl.getImportance(notificationChannel));
        this.mName = Api26Impl.getName(notificationChannel);
        this.mDescription = Api26Impl.getDescription(notificationChannel);
        this.mGroupId = Api26Impl.getGroup(notificationChannel);
        this.mShowBadge = Api26Impl.canShowBadge(notificationChannel);
        this.mSound = Api26Impl.getSound(notificationChannel);
        this.mAudioAttributes = Api26Impl.getAudioAttributes(notificationChannel);
        this.mLights = Api26Impl.shouldShowLights(notificationChannel);
        this.mLightColor = Api26Impl.getLightColor(notificationChannel);
        this.mVibrationEnabled = Api26Impl.shouldVibrate(notificationChannel);
        this.mVibrationPattern = Api26Impl.getVibrationPattern(notificationChannel);
        if (Build$VERSION.SDK_INT >= 30) {
            this.mParentId = Api30Impl.getParentChannelId(notificationChannel);
            this.mConversationId = Api30Impl.getConversationId(notificationChannel);
        }
        this.mBypassDnd = Api26Impl.canBypassDnd(notificationChannel);
        this.mLockscreenVisibility = Api26Impl.getLockscreenVisibility(notificationChannel);
        if (Build$VERSION.SDK_INT >= 29) {
            this.mCanBubble = Api29Impl.canBubble(notificationChannel);
        }
        if (Build$VERSION.SDK_INT >= 30) {
            this.mImportantConversation = Api30Impl.isImportantConversation(notificationChannel);
        }
    }
    
    NotificationChannelCompat(final String s, final int mImportance) {
        this.mShowBadge = true;
        this.mSound = Settings$System.DEFAULT_NOTIFICATION_URI;
        this.mLightColor = 0;
        this.mId = Preconditions.checkNotNull(s);
        this.mImportance = mImportance;
        if (Build$VERSION.SDK_INT >= 21) {
            this.mAudioAttributes = Notification.AUDIO_ATTRIBUTES_DEFAULT;
        }
    }
    
    public boolean canBubble() {
        return this.mCanBubble;
    }
    
    public boolean canBypassDnd() {
        return this.mBypassDnd;
    }
    
    public boolean canShowBadge() {
        return this.mShowBadge;
    }
    
    public AudioAttributes getAudioAttributes() {
        return this.mAudioAttributes;
    }
    
    public String getConversationId() {
        return this.mConversationId;
    }
    
    public String getDescription() {
        return this.mDescription;
    }
    
    public String getGroup() {
        return this.mGroupId;
    }
    
    public String getId() {
        return this.mId;
    }
    
    public int getImportance() {
        return this.mImportance;
    }
    
    public int getLightColor() {
        return this.mLightColor;
    }
    
    public int getLockscreenVisibility() {
        return this.mLockscreenVisibility;
    }
    
    public CharSequence getName() {
        return this.mName;
    }
    
    NotificationChannel getNotificationChannel() {
        if (Build$VERSION.SDK_INT < 26) {
            return null;
        }
        final NotificationChannel notificationChannel = Api26Impl.createNotificationChannel(this.mId, this.mName, this.mImportance);
        Api26Impl.setDescription(notificationChannel, this.mDescription);
        Api26Impl.setGroup(notificationChannel, this.mGroupId);
        Api26Impl.setShowBadge(notificationChannel, this.mShowBadge);
        Api26Impl.setSound(notificationChannel, this.mSound, this.mAudioAttributes);
        Api26Impl.enableLights(notificationChannel, this.mLights);
        Api26Impl.setLightColor(notificationChannel, this.mLightColor);
        Api26Impl.setVibrationPattern(notificationChannel, this.mVibrationPattern);
        Api26Impl.enableVibration(notificationChannel, this.mVibrationEnabled);
        if (Build$VERSION.SDK_INT >= 30) {
            final String mParentId = this.mParentId;
            if (mParentId != null) {
                final String mConversationId = this.mConversationId;
                if (mConversationId != null) {
                    Api30Impl.setConversationId(notificationChannel, mParentId, mConversationId);
                }
            }
        }
        return notificationChannel;
    }
    
    public String getParentChannelId() {
        return this.mParentId;
    }
    
    public Uri getSound() {
        return this.mSound;
    }
    
    public long[] getVibrationPattern() {
        return this.mVibrationPattern;
    }
    
    public boolean isImportantConversation() {
        return this.mImportantConversation;
    }
    
    public boolean shouldShowLights() {
        return this.mLights;
    }
    
    public boolean shouldVibrate() {
        return this.mVibrationEnabled;
    }
    
    public Builder toBuilder() {
        return new Builder(this.mId, this.mImportance).setName(this.mName).setDescription(this.mDescription).setGroup(this.mGroupId).setShowBadge(this.mShowBadge).setSound(this.mSound, this.mAudioAttributes).setLightsEnabled(this.mLights).setLightColor(this.mLightColor).setVibrationEnabled(this.mVibrationEnabled).setVibrationPattern(this.mVibrationPattern).setConversationId(this.mParentId, this.mConversationId);
    }
    
    static class Api26Impl
    {
        private Api26Impl() {
        }
        
        static boolean canBypassDnd(final NotificationChannel notificationChannel) {
            return notificationChannel.canBypassDnd();
        }
        
        static boolean canShowBadge(final NotificationChannel notificationChannel) {
            return notificationChannel.canShowBadge();
        }
        
        static NotificationChannel createNotificationChannel(final String s, final CharSequence charSequence, final int n) {
            return new NotificationChannel(s, charSequence, n);
        }
        
        static void enableLights(final NotificationChannel notificationChannel, final boolean b) {
            notificationChannel.enableLights(b);
        }
        
        static void enableVibration(final NotificationChannel notificationChannel, final boolean b) {
            notificationChannel.enableVibration(b);
        }
        
        static AudioAttributes getAudioAttributes(final NotificationChannel notificationChannel) {
            return notificationChannel.getAudioAttributes();
        }
        
        static String getDescription(final NotificationChannel notificationChannel) {
            return notificationChannel.getDescription();
        }
        
        static String getGroup(final NotificationChannel notificationChannel) {
            return notificationChannel.getGroup();
        }
        
        static String getId(final NotificationChannel notificationChannel) {
            return notificationChannel.getId();
        }
        
        static int getImportance(final NotificationChannel notificationChannel) {
            return notificationChannel.getImportance();
        }
        
        static int getLightColor(final NotificationChannel notificationChannel) {
            return notificationChannel.getLightColor();
        }
        
        static int getLockscreenVisibility(final NotificationChannel notificationChannel) {
            return notificationChannel.getLockscreenVisibility();
        }
        
        static CharSequence getName(final NotificationChannel notificationChannel) {
            return notificationChannel.getName();
        }
        
        static Uri getSound(final NotificationChannel notificationChannel) {
            return notificationChannel.getSound();
        }
        
        static long[] getVibrationPattern(final NotificationChannel notificationChannel) {
            return notificationChannel.getVibrationPattern();
        }
        
        static void setDescription(final NotificationChannel notificationChannel, final String description) {
            notificationChannel.setDescription(description);
        }
        
        static void setGroup(final NotificationChannel notificationChannel, final String group) {
            notificationChannel.setGroup(group);
        }
        
        static void setLightColor(final NotificationChannel notificationChannel, final int lightColor) {
            notificationChannel.setLightColor(lightColor);
        }
        
        static void setShowBadge(final NotificationChannel notificationChannel, final boolean showBadge) {
            notificationChannel.setShowBadge(showBadge);
        }
        
        static void setSound(final NotificationChannel notificationChannel, final Uri uri, final AudioAttributes audioAttributes) {
            notificationChannel.setSound(uri, audioAttributes);
        }
        
        static void setVibrationPattern(final NotificationChannel notificationChannel, final long[] vibrationPattern) {
            notificationChannel.setVibrationPattern(vibrationPattern);
        }
        
        static boolean shouldShowLights(final NotificationChannel notificationChannel) {
            return notificationChannel.shouldShowLights();
        }
        
        static boolean shouldVibrate(final NotificationChannel notificationChannel) {
            return notificationChannel.shouldVibrate();
        }
    }
    
    static class Api29Impl
    {
        private Api29Impl() {
        }
        
        static boolean canBubble(final NotificationChannel notificationChannel) {
            return notificationChannel.canBubble();
        }
    }
    
    static class Api30Impl
    {
        private Api30Impl() {
        }
        
        static String getConversationId(final NotificationChannel notificationChannel) {
            return notificationChannel.getConversationId();
        }
        
        static String getParentChannelId(final NotificationChannel notificationChannel) {
            return notificationChannel.getParentChannelId();
        }
        
        static boolean isImportantConversation(final NotificationChannel notificationChannel) {
            return notificationChannel.isImportantConversation();
        }
        
        static void setConversationId(final NotificationChannel notificationChannel, final String s, final String s2) {
            notificationChannel.setConversationId(s, s2);
        }
    }
    
    public static class Builder
    {
        private final NotificationChannelCompat mChannel;
        
        public Builder(final String s, final int n) {
            this.mChannel = new NotificationChannelCompat(s, n);
        }
        
        public NotificationChannelCompat build() {
            return this.mChannel;
        }
        
        public Builder setConversationId(final String mParentId, final String mConversationId) {
            if (Build$VERSION.SDK_INT >= 30) {
                this.mChannel.mParentId = mParentId;
                this.mChannel.mConversationId = mConversationId;
            }
            return this;
        }
        
        public Builder setDescription(final String mDescription) {
            this.mChannel.mDescription = mDescription;
            return this;
        }
        
        public Builder setGroup(final String mGroupId) {
            this.mChannel.mGroupId = mGroupId;
            return this;
        }
        
        public Builder setImportance(final int mImportance) {
            this.mChannel.mImportance = mImportance;
            return this;
        }
        
        public Builder setLightColor(final int mLightColor) {
            this.mChannel.mLightColor = mLightColor;
            return this;
        }
        
        public Builder setLightsEnabled(final boolean mLights) {
            this.mChannel.mLights = mLights;
            return this;
        }
        
        public Builder setName(final CharSequence mName) {
            this.mChannel.mName = mName;
            return this;
        }
        
        public Builder setShowBadge(final boolean mShowBadge) {
            this.mChannel.mShowBadge = mShowBadge;
            return this;
        }
        
        public Builder setSound(final Uri mSound, final AudioAttributes mAudioAttributes) {
            this.mChannel.mSound = mSound;
            this.mChannel.mAudioAttributes = mAudioAttributes;
            return this;
        }
        
        public Builder setVibrationEnabled(final boolean mVibrationEnabled) {
            this.mChannel.mVibrationEnabled = mVibrationEnabled;
            return this;
        }
        
        public Builder setVibrationPattern(final long[] mVibrationPattern) {
            this.mChannel.mVibrationEnabled = (mVibrationPattern != null && mVibrationPattern.length > 0);
            this.mChannel.mVibrationPattern = mVibrationPattern;
            return this;
        }
    }
}

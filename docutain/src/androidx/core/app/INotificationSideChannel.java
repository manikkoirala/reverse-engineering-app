// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.app;

import android.os.Parcelable$Creator;
import android.os.Parcelable;
import android.os.Parcel;
import android.os.Binder;
import android.os.IBinder;
import android.app.Notification;
import android.os.RemoteException;
import android.os.IInterface;

public interface INotificationSideChannel extends IInterface
{
    public static final String DESCRIPTOR = "android$support$v4$app$INotificationSideChannel".replace('$', '.');
    
    void cancel(final String p0, final int p1, final String p2) throws RemoteException;
    
    void cancelAll(final String p0) throws RemoteException;
    
    void notify(final String p0, final int p1, final String p2, final Notification p3) throws RemoteException;
    
    public static class Default implements INotificationSideChannel
    {
        public IBinder asBinder() {
            return null;
        }
        
        @Override
        public void cancel(final String s, final int n, final String s2) throws RemoteException {
        }
        
        @Override
        public void cancelAll(final String s) throws RemoteException {
        }
        
        @Override
        public void notify(final String s, final int n, final String s2, final Notification notification) throws RemoteException {
        }
    }
    
    public abstract static class Stub extends Binder implements INotificationSideChannel
    {
        static final int TRANSACTION_cancel = 2;
        static final int TRANSACTION_cancelAll = 3;
        static final int TRANSACTION_notify = 1;
        
        public Stub() {
            this.attachInterface((IInterface)this, Stub.DESCRIPTOR);
        }
        
        public static INotificationSideChannel asInterface(final IBinder binder) {
            if (binder == null) {
                return null;
            }
            final IInterface queryLocalInterface = binder.queryLocalInterface(Stub.DESCRIPTOR);
            if (queryLocalInterface != null && queryLocalInterface instanceof INotificationSideChannel) {
                return (INotificationSideChannel)queryLocalInterface;
            }
            return new Proxy(binder);
        }
        
        public IBinder asBinder() {
            return (IBinder)this;
        }
        
        public boolean onTransact(final int n, final Parcel parcel, final Parcel parcel2, final int n2) throws RemoteException {
            final String descriptor = Stub.DESCRIPTOR;
            if (n >= 1 && n <= 16777215) {
                parcel.enforceInterface(descriptor);
            }
            if (n == 1598968902) {
                parcel2.writeString(descriptor);
                return true;
            }
            if (n != 1) {
                if (n != 2) {
                    if (n != 3) {
                        return super.onTransact(n, parcel, parcel2, n2);
                    }
                    this.cancelAll(parcel.readString());
                }
                else {
                    this.cancel(parcel.readString(), parcel.readInt(), parcel.readString());
                }
            }
            else {
                this.notify(parcel.readString(), parcel.readInt(), parcel.readString(), (Notification)readTypedObject(parcel, (android.os.Parcelable$Creator<Object>)Notification.CREATOR));
            }
            return true;
        }
        
        private static class Proxy implements INotificationSideChannel
        {
            private IBinder mRemote;
            
            Proxy(final IBinder mRemote) {
                this.mRemote = mRemote;
            }
            
            public IBinder asBinder() {
                return this.mRemote;
            }
            
            @Override
            public void cancel(final String s, final int n, final String s2) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Proxy.DESCRIPTOR);
                    obtain.writeString(s);
                    obtain.writeInt(n);
                    obtain.writeString(s2);
                    this.mRemote.transact(2, obtain, (Parcel)null, 1);
                }
                finally {
                    obtain.recycle();
                }
            }
            
            @Override
            public void cancelAll(final String s) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Proxy.DESCRIPTOR);
                    obtain.writeString(s);
                    this.mRemote.transact(3, obtain, (Parcel)null, 1);
                }
                finally {
                    obtain.recycle();
                }
            }
            
            public String getInterfaceDescriptor() {
                return Proxy.DESCRIPTOR;
            }
            
            @Override
            public void notify(final String s, final int n, final String s2, final Notification notification) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Proxy.DESCRIPTOR);
                    obtain.writeString(s);
                    obtain.writeInt(n);
                    obtain.writeString(s2);
                    writeTypedObject(obtain, (Parcelable)notification, 0);
                    this.mRemote.transact(1, obtain, (Parcel)null, 1);
                }
                finally {
                    obtain.recycle();
                }
            }
        }
    }
    
    public static class _Parcel
    {
        private static <T> T readTypedObject(final Parcel parcel, final Parcelable$Creator<T> parcelable$Creator) {
            if (parcel.readInt() != 0) {
                return (T)parcelable$Creator.createFromParcel(parcel);
            }
            return null;
        }
        
        private static <T extends Parcelable> void writeTypedObject(final Parcel parcel, final T t, final int n) {
            if (t != null) {
                parcel.writeInt(1);
                t.writeToParcel(parcel, n);
            }
            else {
                parcel.writeInt(0);
            }
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.app;

import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;
import android.app.GrammaticalInflectionManager;
import android.os.Build$VERSION;
import android.content.Context;

public final class GrammaticalInflectionManagerCompat
{
    public static final int GRAMMATICAL_GENDER_FEMININE = 2;
    public static final int GRAMMATICAL_GENDER_MASCULINE = 3;
    public static final int GRAMMATICAL_GENDER_NEUTRAL = 1;
    public static final int GRAMMATICAL_GENDER_NOT_SPECIFIED = 0;
    
    private GrammaticalInflectionManagerCompat() {
    }
    
    public static int getApplicationGrammaticalGender(final Context context) {
        if (Build$VERSION.SDK_INT >= 34) {
            return Api34Impl.getApplicationGrammaticalGender(context);
        }
        return 0;
    }
    
    public static void setRequestedApplicationGrammaticalGender(final Context context, final int n) {
        if (Build$VERSION.SDK_INT >= 34) {
            Api34Impl.setRequestedApplicationGrammaticalGender(context, n);
        }
    }
    
    static class Api34Impl
    {
        private Api34Impl() {
        }
        
        static int getApplicationGrammaticalGender(final Context context) {
            return getGrammaticalInflectionManager(context).getApplicationGrammaticalGender();
        }
        
        private static GrammaticalInflectionManager getGrammaticalInflectionManager(final Context context) {
            return (GrammaticalInflectionManager)context.getSystemService((Class)GrammaticalInflectionManager.class);
        }
        
        static void setRequestedApplicationGrammaticalGender(final Context context, final int requestedApplicationGrammaticalGender) {
            getGrammaticalInflectionManager(context).setRequestedApplicationGrammaticalGender(requestedApplicationGrammaticalGender);
        }
    }
    
    @Retention(RetentionPolicy.SOURCE)
    public @interface GrammaticalGender {
    }
}

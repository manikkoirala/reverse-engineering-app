// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.app;

import java.util.concurrent.CountDownLatch;
import java.io.Closeable;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;
import android.app.PendingIntent$CanceledException;
import android.os.Handler;
import android.app.PendingIntent$OnFinished;
import android.app.PendingIntent;
import android.os.Bundle;
import android.content.Intent;
import android.content.Context;
import android.os.Build$VERSION;

public final class PendingIntentCompat
{
    private PendingIntentCompat() {
    }
    
    private static int addMutabilityFlags(final boolean b, final int n) {
        int n3;
        if (b) {
            final int n2 = n;
            if (Build$VERSION.SDK_INT < 31) {
                return n2;
            }
            n3 = 33554432;
        }
        else {
            final int n2 = n;
            if (Build$VERSION.SDK_INT < 23) {
                return n2;
            }
            n3 = 67108864;
        }
        return n | n3;
    }
    
    public static PendingIntent getActivities(final Context context, final int n, final Intent[] array, final int n2, final Bundle bundle, final boolean b) {
        if (Build$VERSION.SDK_INT >= 16) {
            return Api16Impl.getActivities(context, n, array, addMutabilityFlags(b, n2), bundle);
        }
        return PendingIntent.getActivities(context, n, array, n2);
    }
    
    public static PendingIntent getActivities(final Context context, final int n, final Intent[] array, final int n2, final boolean b) {
        return PendingIntent.getActivities(context, n, array, addMutabilityFlags(b, n2));
    }
    
    public static PendingIntent getActivity(final Context context, final int n, final Intent intent, final int n2, final Bundle bundle, final boolean b) {
        if (Build$VERSION.SDK_INT >= 16) {
            return Api16Impl.getActivity(context, n, intent, addMutabilityFlags(b, n2), bundle);
        }
        return PendingIntent.getActivity(context, n, intent, n2);
    }
    
    public static PendingIntent getActivity(final Context context, final int n, final Intent intent, final int n2, final boolean b) {
        return PendingIntent.getActivity(context, n, intent, addMutabilityFlags(b, n2));
    }
    
    public static PendingIntent getBroadcast(final Context context, final int n, final Intent intent, final int n2, final boolean b) {
        return PendingIntent.getBroadcast(context, n, intent, addMutabilityFlags(b, n2));
    }
    
    public static PendingIntent getForegroundService(final Context context, final int n, final Intent intent, final int n2, final boolean b) {
        return Api26Impl.getForegroundService(context, n, intent, addMutabilityFlags(b, n2));
    }
    
    public static PendingIntent getService(final Context context, final int n, final Intent intent, final int n2, final boolean b) {
        return PendingIntent.getService(context, n, intent, addMutabilityFlags(b, n2));
    }
    
    public static void send(final PendingIntent pendingIntent, final int n, PendingIntent$OnFinished pendingIntent$OnFinished, final Handler handler) throws PendingIntent$CanceledException {
        pendingIntent$OnFinished = (PendingIntent$OnFinished)new GatedCallback(pendingIntent$OnFinished);
        try {
            pendingIntent.send(n, ((GatedCallback)pendingIntent$OnFinished).getCallback(), handler);
            ((GatedCallback)pendingIntent$OnFinished).complete();
            ((GatedCallback)pendingIntent$OnFinished).close();
        }
        finally {
            try {
                ((GatedCallback)pendingIntent$OnFinished).close();
            }
            finally {
                final Throwable exception;
                ((Throwable)pendingIntent).addSuppressed(exception);
            }
        }
    }
    
    public static void send(final PendingIntent pendingIntent, final Context context, final int n, final Intent intent, final PendingIntent$OnFinished pendingIntent$OnFinished, final Handler handler) throws PendingIntent$CanceledException {
        send(pendingIntent, context, n, intent, pendingIntent$OnFinished, handler, null, null);
    }
    
    public static void send(final PendingIntent pendingIntent, final Context context, final int n, final Intent intent, final PendingIntent$OnFinished pendingIntent$OnFinished, final Handler handler, final String s, final Bundle bundle) throws PendingIntent$CanceledException {
        final GatedCallback gatedCallback = new GatedCallback(pendingIntent$OnFinished);
        try {
            if (Build$VERSION.SDK_INT >= 23) {
                Api23Impl.send(pendingIntent, context, n, intent, pendingIntent$OnFinished, handler, s, bundle);
            }
            else {
                pendingIntent.send(context, n, intent, gatedCallback.getCallback(), handler, s);
            }
            gatedCallback.complete();
            gatedCallback.close();
        }
        finally {
            try {
                gatedCallback.close();
            }
            finally {
                final Throwable exception;
                ((Throwable)pendingIntent).addSuppressed(exception);
            }
        }
    }
    
    private static class Api16Impl
    {
        public static PendingIntent getActivities(final Context context, final int n, final Intent[] array, final int n2, final Bundle bundle) {
            return PendingIntent.getActivities(context, n, array, n2, bundle);
        }
        
        public static PendingIntent getActivity(final Context context, final int n, final Intent intent, final int n2, final Bundle bundle) {
            return PendingIntent.getActivity(context, n, intent, n2, bundle);
        }
    }
    
    private static class Api23Impl
    {
        public static void send(final PendingIntent pendingIntent, final Context context, final int n, final Intent intent, final PendingIntent$OnFinished pendingIntent$OnFinished, final Handler handler, final String s, final Bundle bundle) throws PendingIntent$CanceledException {
            pendingIntent.send(context, n, intent, pendingIntent$OnFinished, handler, s, bundle);
        }
    }
    
    private static class Api26Impl
    {
        public static PendingIntent getForegroundService(final Context context, final int n, final Intent intent, final int n2) {
            return PendingIntent.getForegroundService(context, n, intent, n2);
        }
    }
    
    @Retention(RetentionPolicy.SOURCE)
    public @interface Flags {
    }
    
    private static class GatedCallback implements Closeable
    {
        private PendingIntent$OnFinished mCallback;
        private final CountDownLatch mComplete;
        private boolean mSuccess;
        
        GatedCallback(final PendingIntent$OnFinished mCallback) {
            this.mComplete = new CountDownLatch(1);
            this.mCallback = mCallback;
            this.mSuccess = false;
        }
        
        private void onSendFinished(final PendingIntent pendingIntent, final Intent intent, final int n, final String s, final Bundle bundle) {
            int n2 = 0;
            while (true) {
                try {
                    this.mComplete.await();
                    if (n2 != 0) {
                        Thread.currentThread().interrupt();
                    }
                    final PendingIntent$OnFinished mCallback = this.mCallback;
                    if (mCallback != null) {
                        mCallback.onSendFinished(pendingIntent, intent, n, s, bundle);
                        this.mCallback = null;
                    }
                }
                catch (final InterruptedException ex) {
                    n2 = 1;
                    continue;
                }
                finally {
                    if (n2 != 0) {
                        Thread.currentThread().interrupt();
                    }
                }
                break;
            }
        }
        
        @Override
        public void close() {
            if (!this.mSuccess) {
                this.mCallback = null;
            }
            this.mComplete.countDown();
        }
        
        public void complete() {
            this.mSuccess = true;
        }
        
        public PendingIntent$OnFinished getCallback() {
            if (this.mCallback == null) {
                return null;
            }
            return (PendingIntent$OnFinished)new PendingIntentCompat$GatedCallback$$ExternalSyntheticLambda0(this);
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.app;

import android.app.ActivityOptions;
import android.os.Bundle;
import android.app.PendingIntent;
import android.graphics.Rect;
import android.graphics.Bitmap;
import androidx.core.util.Pair;
import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.os.Build$VERSION;

public class ActivityOptionsCompat
{
    public static final String EXTRA_USAGE_TIME_REPORT = "android.activity.usage_time";
    public static final String EXTRA_USAGE_TIME_REPORT_PACKAGES = "android.usage_time_packages";
    
    protected ActivityOptionsCompat() {
    }
    
    public static ActivityOptionsCompat makeBasic() {
        if (Build$VERSION.SDK_INT >= 23) {
            return new ActivityOptionsCompatImpl(Api23Impl.makeBasic());
        }
        return new ActivityOptionsCompat();
    }
    
    public static ActivityOptionsCompat makeClipRevealAnimation(final View view, final int n, final int n2, final int n3, final int n4) {
        if (Build$VERSION.SDK_INT >= 23) {
            return new ActivityOptionsCompatImpl(Api23Impl.makeClipRevealAnimation(view, n, n2, n3, n4));
        }
        return new ActivityOptionsCompat();
    }
    
    public static ActivityOptionsCompat makeCustomAnimation(final Context context, final int n, final int n2) {
        if (Build$VERSION.SDK_INT >= 16) {
            return new ActivityOptionsCompatImpl(Api16Impl.makeCustomAnimation(context, n, n2));
        }
        return new ActivityOptionsCompat();
    }
    
    public static ActivityOptionsCompat makeScaleUpAnimation(final View view, final int n, final int n2, final int n3, final int n4) {
        if (Build$VERSION.SDK_INT >= 16) {
            return new ActivityOptionsCompatImpl(Api16Impl.makeScaleUpAnimation(view, n, n2, n3, n4));
        }
        return new ActivityOptionsCompat();
    }
    
    public static ActivityOptionsCompat makeSceneTransitionAnimation(final Activity activity, final View view, final String s) {
        if (Build$VERSION.SDK_INT >= 21) {
            return new ActivityOptionsCompatImpl(Api21Impl.makeSceneTransitionAnimation(activity, view, s));
        }
        return new ActivityOptionsCompat();
    }
    
    public static ActivityOptionsCompat makeSceneTransitionAnimation(final Activity activity, final Pair<View, String>... array) {
        if (Build$VERSION.SDK_INT >= 21) {
            android.util.Pair<View, String>[] array2 = null;
            if (array != null) {
                final android.util.Pair[] array3 = new android.util.Pair[array.length];
                int n = 0;
                while (true) {
                    array2 = (android.util.Pair<View, String>[])array3;
                    if (n >= array.length) {
                        break;
                    }
                    array3[n] = android.util.Pair.create((Object)array[n].first, (Object)array[n].second);
                    ++n;
                }
            }
            return new ActivityOptionsCompatImpl(Api21Impl.makeSceneTransitionAnimation(activity, array2));
        }
        return new ActivityOptionsCompat();
    }
    
    public static ActivityOptionsCompat makeTaskLaunchBehind() {
        if (Build$VERSION.SDK_INT >= 21) {
            return new ActivityOptionsCompatImpl(Api21Impl.makeTaskLaunchBehind());
        }
        return new ActivityOptionsCompat();
    }
    
    public static ActivityOptionsCompat makeThumbnailScaleUpAnimation(final View view, final Bitmap bitmap, final int n, final int n2) {
        if (Build$VERSION.SDK_INT >= 16) {
            return new ActivityOptionsCompatImpl(Api16Impl.makeThumbnailScaleUpAnimation(view, bitmap, n, n2));
        }
        return new ActivityOptionsCompat();
    }
    
    public Rect getLaunchBounds() {
        return null;
    }
    
    public void requestUsageTimeReport(final PendingIntent pendingIntent) {
    }
    
    public ActivityOptionsCompat setLaunchBounds(final Rect rect) {
        return this;
    }
    
    public Bundle toBundle() {
        return null;
    }
    
    public void update(final ActivityOptionsCompat activityOptionsCompat) {
    }
    
    private static class ActivityOptionsCompatImpl extends ActivityOptionsCompat
    {
        private final ActivityOptions mActivityOptions;
        
        ActivityOptionsCompatImpl(final ActivityOptions mActivityOptions) {
            this.mActivityOptions = mActivityOptions;
        }
        
        @Override
        public Rect getLaunchBounds() {
            if (Build$VERSION.SDK_INT < 24) {
                return null;
            }
            return Api24Impl.getLaunchBounds(this.mActivityOptions);
        }
        
        @Override
        public void requestUsageTimeReport(final PendingIntent pendingIntent) {
            if (Build$VERSION.SDK_INT >= 23) {
                Api23Impl.requestUsageTimeReport(this.mActivityOptions, pendingIntent);
            }
        }
        
        @Override
        public ActivityOptionsCompat setLaunchBounds(final Rect rect) {
            if (Build$VERSION.SDK_INT < 24) {
                return this;
            }
            return new ActivityOptionsCompatImpl(Api24Impl.setLaunchBounds(this.mActivityOptions, rect));
        }
        
        @Override
        public Bundle toBundle() {
            return this.mActivityOptions.toBundle();
        }
        
        @Override
        public void update(final ActivityOptionsCompat activityOptionsCompat) {
            if (activityOptionsCompat instanceof ActivityOptionsCompatImpl) {
                this.mActivityOptions.update(((ActivityOptionsCompatImpl)activityOptionsCompat).mActivityOptions);
            }
        }
    }
    
    static class Api16Impl
    {
        private Api16Impl() {
        }
        
        static ActivityOptions makeCustomAnimation(final Context context, final int n, final int n2) {
            return ActivityOptions.makeCustomAnimation(context, n, n2);
        }
        
        static ActivityOptions makeScaleUpAnimation(final View view, final int n, final int n2, final int n3, final int n4) {
            return ActivityOptions.makeScaleUpAnimation(view, n, n2, n3, n4);
        }
        
        static ActivityOptions makeThumbnailScaleUpAnimation(final View view, final Bitmap bitmap, final int n, final int n2) {
            return ActivityOptions.makeThumbnailScaleUpAnimation(view, bitmap, n, n2);
        }
    }
    
    static class Api21Impl
    {
        private Api21Impl() {
        }
        
        static ActivityOptions makeSceneTransitionAnimation(final Activity activity, final View view, final String s) {
            return ActivityOptions.makeSceneTransitionAnimation(activity, view, s);
        }
        
        @SafeVarargs
        static ActivityOptions makeSceneTransitionAnimation(final Activity activity, final android.util.Pair<View, String>... array) {
            return ActivityOptions.makeSceneTransitionAnimation(activity, (android.util.Pair[])array);
        }
        
        static ActivityOptions makeTaskLaunchBehind() {
            return ActivityOptions.makeTaskLaunchBehind();
        }
    }
    
    static class Api23Impl
    {
        private Api23Impl() {
        }
        
        static ActivityOptions makeBasic() {
            return ActivityOptions.makeBasic();
        }
        
        static ActivityOptions makeClipRevealAnimation(final View view, final int n, final int n2, final int n3, final int n4) {
            return ActivityOptions.makeClipRevealAnimation(view, n, n2, n3, n4);
        }
        
        static void requestUsageTimeReport(final ActivityOptions activityOptions, final PendingIntent pendingIntent) {
            activityOptions.requestUsageTimeReport(pendingIntent);
        }
    }
    
    static class Api24Impl
    {
        private Api24Impl() {
        }
        
        static Rect getLaunchBounds(final ActivityOptions activityOptions) {
            return activityOptions.getLaunchBounds();
        }
        
        static ActivityOptions setLaunchBounds(final ActivityOptions activityOptions, final Rect launchBounds) {
            return activityOptions.setLaunchBounds(launchBounds);
        }
    }
}

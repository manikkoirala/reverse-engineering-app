// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.app;

import java.util.Map;
import java.util.List;
import android.content.Context;
import android.os.Parcelable;
import android.graphics.RectF;
import android.graphics.Matrix;
import java.lang.reflect.InvocationTargetException;
import android.content.LocusId;
import android.view.Display;
import android.app.SharedElementCallback$OnSharedElementsReadyListener;
import android.content.IntentSender$SendIntentException;
import android.content.IntentSender;
import android.os.Bundle;
import androidx.core.content.LocusIdCompat;
import android.view.View;
import android.content.pm.PackageManager;
import android.os.Looper;
import java.util.Arrays;
import android.text.TextUtils;
import java.util.HashSet;
import androidx.core.view.DragAndDropPermissionsCompat;
import android.view.DragEvent;
import android.os.Handler;
import android.content.ContextWrapper;
import android.content.Intent;
import android.net.Uri;
import android.os.Build$VERSION;
import android.app.Activity;
import androidx.core.content.ContextCompat;

public class ActivityCompat extends ContextCompat
{
    private static PermissionCompatDelegate sDelegate;
    
    protected ActivityCompat() {
    }
    
    public static void finishAffinity(final Activity activity) {
        if (Build$VERSION.SDK_INT >= 16) {
            Api16Impl.finishAffinity(activity);
        }
        else {
            activity.finish();
        }
    }
    
    public static void finishAfterTransition(final Activity activity) {
        if (Build$VERSION.SDK_INT >= 21) {
            Api21Impl.finishAfterTransition(activity);
        }
        else {
            activity.finish();
        }
    }
    
    public static PermissionCompatDelegate getPermissionCompatDelegate() {
        return ActivityCompat.sDelegate;
    }
    
    public static Uri getReferrer(final Activity activity) {
        if (Build$VERSION.SDK_INT >= 22) {
            return Api22Impl.getReferrer(activity);
        }
        final Intent intent = activity.getIntent();
        final Uri uri = (Uri)intent.getParcelableExtra("android.intent.extra.REFERRER");
        if (uri != null) {
            return uri;
        }
        final String stringExtra = intent.getStringExtra("android.intent.extra.REFERRER_NAME");
        if (stringExtra != null) {
            return Uri.parse(stringExtra);
        }
        return null;
    }
    
    @Deprecated
    public static boolean invalidateOptionsMenu(final Activity activity) {
        activity.invalidateOptionsMenu();
        return true;
    }
    
    public static boolean isLaunchedFromBubble(final Activity activity) {
        if (Build$VERSION.SDK_INT >= 31) {
            return Api31Impl.isLaunchedFromBubble(activity);
        }
        final int sdk_INT = Build$VERSION.SDK_INT;
        final boolean b = true;
        boolean b2 = true;
        if (sdk_INT == 30) {
            if (Api30Impl.getDisplay((ContextWrapper)activity) == null || Api30Impl.getDisplay((ContextWrapper)activity).getDisplayId() == 0) {
                b2 = false;
            }
            return b2;
        }
        return Build$VERSION.SDK_INT == 29 && activity.getWindowManager().getDefaultDisplay() != null && activity.getWindowManager().getDefaultDisplay().getDisplayId() != 0 && b;
    }
    
    public static void postponeEnterTransition(final Activity activity) {
        if (Build$VERSION.SDK_INT >= 21) {
            Api21Impl.postponeEnterTransition(activity);
        }
    }
    
    public static void recreate(final Activity activity) {
        if (Build$VERSION.SDK_INT >= 28) {
            activity.recreate();
        }
        else {
            new Handler(activity.getMainLooper()).post((Runnable)new ActivityCompat$$ExternalSyntheticLambda0(activity));
        }
    }
    
    public static DragAndDropPermissionsCompat requestDragAndDropPermissions(final Activity activity, final DragEvent dragEvent) {
        return DragAndDropPermissionsCompat.request(activity, dragEvent);
    }
    
    public static void requestPermissions(final Activity activity, final String[] a, final int n) {
        final PermissionCompatDelegate sDelegate = ActivityCompat.sDelegate;
        if (sDelegate != null && sDelegate.requestPermissions(activity, a, n)) {
            return;
        }
        final HashSet set = new HashSet();
        int i = 0;
        for (int j = 0; j < a.length; ++j) {
            if (TextUtils.isEmpty((CharSequence)a[j])) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Permission request for permissions ");
                sb.append(Arrays.toString(a));
                sb.append(" must not contain null or empty values");
                throw new IllegalArgumentException(sb.toString());
            }
            if (Build$VERSION.SDK_INT < 33 && TextUtils.equals((CharSequence)a[j], (CharSequence)"android.permission.POST_NOTIFICATIONS")) {
                set.add(j);
            }
        }
        final int size = set.size();
        String[] array;
        if (size > 0) {
            array = new String[a.length - size];
        }
        else {
            array = a;
        }
        if (size > 0) {
            if (size == a.length) {
                return;
            }
            int n2 = 0;
            while (i < a.length) {
                int n3 = n2;
                if (!set.contains(i)) {
                    array[n2] = a[i];
                    n3 = n2 + 1;
                }
                ++i;
                n2 = n3;
            }
        }
        if (Build$VERSION.SDK_INT >= 23) {
            if (activity instanceof RequestPermissionsRequestCodeValidator) {
                ((RequestPermissionsRequestCodeValidator)activity).validateRequestPermissionsRequestCode(n);
            }
            Api23Impl.requestPermissions(activity, a, n);
        }
        else if (activity instanceof OnRequestPermissionsResultCallback) {
            new Handler(Looper.getMainLooper()).post((Runnable)new Runnable(array, activity, n) {
                final Activity val$activity;
                final String[] val$permissionsArray;
                final int val$requestCode;
                
                @Override
                public void run() {
                    final int[] array = new int[this.val$permissionsArray.length];
                    final PackageManager packageManager = this.val$activity.getPackageManager();
                    final String packageName = this.val$activity.getPackageName();
                    for (int length = this.val$permissionsArray.length, i = 0; i < length; ++i) {
                        array[i] = packageManager.checkPermission(this.val$permissionsArray[i], packageName);
                    }
                    ((OnRequestPermissionsResultCallback)this.val$activity).onRequestPermissionsResult(this.val$requestCode, this.val$permissionsArray, array);
                }
            });
        }
    }
    
    public static <T extends View> T requireViewById(final Activity activity, final int n) {
        if (Build$VERSION.SDK_INT >= 28) {
            return Api28Impl.requireViewById(activity, n);
        }
        final View viewById = activity.findViewById(n);
        if (viewById != null) {
            return (T)viewById;
        }
        throw new IllegalArgumentException("ID does not reference a View inside this Activity");
    }
    
    public static void setEnterSharedElementCallback(final Activity activity, final SharedElementCallback sharedElementCallback) {
        if (Build$VERSION.SDK_INT >= 21) {
            SharedElementCallback21Impl sharedElementCallback21Impl;
            if (sharedElementCallback != null) {
                sharedElementCallback21Impl = new SharedElementCallback21Impl(sharedElementCallback);
            }
            else {
                sharedElementCallback21Impl = null;
            }
            Api21Impl.setEnterSharedElementCallback(activity, sharedElementCallback21Impl);
        }
    }
    
    public static void setExitSharedElementCallback(final Activity activity, final SharedElementCallback sharedElementCallback) {
        if (Build$VERSION.SDK_INT >= 21) {
            SharedElementCallback21Impl sharedElementCallback21Impl;
            if (sharedElementCallback != null) {
                sharedElementCallback21Impl = new SharedElementCallback21Impl(sharedElementCallback);
            }
            else {
                sharedElementCallback21Impl = null;
            }
            Api21Impl.setExitSharedElementCallback(activity, sharedElementCallback21Impl);
        }
    }
    
    public static void setLocusContext(final Activity activity, final LocusIdCompat locusIdCompat, final Bundle bundle) {
        if (Build$VERSION.SDK_INT >= 30) {
            Api30Impl.setLocusContext(activity, locusIdCompat, bundle);
        }
    }
    
    public static void setPermissionCompatDelegate(final PermissionCompatDelegate sDelegate) {
        ActivityCompat.sDelegate = sDelegate;
    }
    
    public static boolean shouldShowRequestPermissionRationale(final Activity activity, final String s) {
        if (Build$VERSION.SDK_INT < 33 && TextUtils.equals((CharSequence)"android.permission.POST_NOTIFICATIONS", (CharSequence)s)) {
            return false;
        }
        if (Build$VERSION.SDK_INT >= 32) {
            return Api32Impl.shouldShowRequestPermissionRationale(activity, s);
        }
        if (Build$VERSION.SDK_INT == 31) {
            return Api31Impl.shouldShowRequestPermissionRationale(activity, s);
        }
        return Build$VERSION.SDK_INT >= 23 && Api23Impl.shouldShowRequestPermissionRationale(activity, s);
    }
    
    public static void startActivityForResult(final Activity activity, final Intent intent, final int n, final Bundle bundle) {
        if (Build$VERSION.SDK_INT >= 16) {
            Api16Impl.startActivityForResult(activity, intent, n, bundle);
        }
        else {
            activity.startActivityForResult(intent, n);
        }
    }
    
    public static void startIntentSenderForResult(final Activity activity, final IntentSender intentSender, final int n, final Intent intent, final int n2, final int n3, final int n4, final Bundle bundle) throws IntentSender$SendIntentException {
        if (Build$VERSION.SDK_INT >= 16) {
            Api16Impl.startIntentSenderForResult(activity, intentSender, n, intent, n2, n3, n4, bundle);
        }
        else {
            activity.startIntentSenderForResult(intentSender, n, intent, n2, n3, n4);
        }
    }
    
    public static void startPostponedEnterTransition(final Activity activity) {
        if (Build$VERSION.SDK_INT >= 21) {
            Api21Impl.startPostponedEnterTransition(activity);
        }
    }
    
    static class Api16Impl
    {
        private Api16Impl() {
        }
        
        static void finishAffinity(final Activity activity) {
            activity.finishAffinity();
        }
        
        static void startActivityForResult(final Activity activity, final Intent intent, final int n, final Bundle bundle) {
            activity.startActivityForResult(intent, n, bundle);
        }
        
        static void startIntentSenderForResult(final Activity activity, final IntentSender intentSender, final int n, final Intent intent, final int n2, final int n3, final int n4, final Bundle bundle) throws IntentSender$SendIntentException {
            activity.startIntentSenderForResult(intentSender, n, intent, n2, n3, n4, bundle);
        }
    }
    
    static class Api21Impl
    {
        private Api21Impl() {
        }
        
        static void finishAfterTransition(final Activity activity) {
            activity.finishAfterTransition();
        }
        
        static void postponeEnterTransition(final Activity activity) {
            activity.postponeEnterTransition();
        }
        
        static void setEnterSharedElementCallback(final Activity activity, final android.app.SharedElementCallback enterSharedElementCallback) {
            activity.setEnterSharedElementCallback(enterSharedElementCallback);
        }
        
        static void setExitSharedElementCallback(final Activity activity, final android.app.SharedElementCallback exitSharedElementCallback) {
            activity.setExitSharedElementCallback(exitSharedElementCallback);
        }
        
        static void startPostponedEnterTransition(final Activity activity) {
            activity.startPostponedEnterTransition();
        }
    }
    
    static class Api22Impl
    {
        private Api22Impl() {
        }
        
        static Uri getReferrer(final Activity activity) {
            return activity.getReferrer();
        }
    }
    
    static class Api23Impl
    {
        private Api23Impl() {
        }
        
        static void onSharedElementsReady(final Object o) {
            ((SharedElementCallback$OnSharedElementsReadyListener)o).onSharedElementsReady();
        }
        
        static void requestPermissions(final Activity activity, final String[] array, final int n) {
            activity.requestPermissions(array, n);
        }
        
        static boolean shouldShowRequestPermissionRationale(final Activity activity, final String s) {
            return activity.shouldShowRequestPermissionRationale(s);
        }
    }
    
    static class Api28Impl
    {
        private Api28Impl() {
        }
        
        static <T> T requireViewById(final Activity activity, final int n) {
            return (T)activity.requireViewById(n);
        }
    }
    
    static class Api30Impl
    {
        private Api30Impl() {
        }
        
        static Display getDisplay(final ContextWrapper contextWrapper) {
            return contextWrapper.getDisplay();
        }
        
        static void setLocusContext(final Activity activity, final LocusIdCompat locusIdCompat, final Bundle bundle) {
            LocusId locusId;
            if (locusIdCompat == null) {
                locusId = null;
            }
            else {
                locusId = locusIdCompat.toLocusId();
            }
            activity.setLocusContext(locusId, bundle);
        }
    }
    
    static class Api31Impl
    {
        private Api31Impl() {
        }
        
        static boolean isLaunchedFromBubble(final Activity activity) {
            return activity.isLaunchedFromBubble();
        }
        
        static boolean shouldShowRequestPermissionRationale(final Activity activity, final String s) {
            try {
                return (boolean)PackageManager.class.getMethod("shouldShowRequestPermissionRationale", String.class).invoke(activity.getApplication().getPackageManager(), s);
            }
            catch (final NoSuchMethodException | InvocationTargetException | IllegalAccessException ex) {
                return activity.shouldShowRequestPermissionRationale(s);
            }
        }
    }
    
    static class Api32Impl
    {
        private Api32Impl() {
        }
        
        static boolean shouldShowRequestPermissionRationale(final Activity activity, final String s) {
            return activity.shouldShowRequestPermissionRationale(s);
        }
    }
    
    public interface OnRequestPermissionsResultCallback
    {
        void onRequestPermissionsResult(final int p0, final String[] p1, final int[] p2);
    }
    
    public interface PermissionCompatDelegate
    {
        boolean onActivityResult(final Activity p0, final int p1, final int p2, final Intent p3);
        
        boolean requestPermissions(final Activity p0, final String[] p1, final int p2);
    }
    
    public interface RequestPermissionsRequestCodeValidator
    {
        void validateRequestPermissionsRequestCode(final int p0);
    }
    
    static class SharedElementCallback21Impl extends android.app.SharedElementCallback
    {
        private final SharedElementCallback mCallback;
        
        SharedElementCallback21Impl(final SharedElementCallback mCallback) {
            this.mCallback = mCallback;
        }
        
        public Parcelable onCaptureSharedElementSnapshot(final View view, final Matrix matrix, final RectF rectF) {
            return this.mCallback.onCaptureSharedElementSnapshot(view, matrix, rectF);
        }
        
        public View onCreateSnapshotView(final Context context, final Parcelable parcelable) {
            return this.mCallback.onCreateSnapshotView(context, parcelable);
        }
        
        public void onMapSharedElements(final List<String> list, final Map<String, View> map) {
            this.mCallback.onMapSharedElements(list, map);
        }
        
        public void onRejectSharedElements(final List<View> list) {
            this.mCallback.onRejectSharedElements(list);
        }
        
        public void onSharedElementEnd(final List<String> list, final List<View> list2, final List<View> list3) {
            this.mCallback.onSharedElementEnd(list, list2, list3);
        }
        
        public void onSharedElementStart(final List<String> list, final List<View> list2, final List<View> list3) {
            this.mCallback.onSharedElementStart(list, list2, list3);
        }
        
        public void onSharedElementsArrived(final List<String> list, final List<View> list2, final SharedElementCallback$OnSharedElementsReadyListener sharedElementCallback$OnSharedElementsReadyListener) {
            this.mCallback.onSharedElementsArrived(list, list2, (SharedElementCallback.OnSharedElementsReadyListener)new ActivityCompat$SharedElementCallback21Impl$$ExternalSyntheticLambda0(sharedElementCallback$OnSharedElementsReadyListener));
        }
    }
}

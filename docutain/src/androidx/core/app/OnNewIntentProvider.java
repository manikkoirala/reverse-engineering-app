// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.app;

import android.content.Intent;
import androidx.core.util.Consumer;

public interface OnNewIntentProvider
{
    void addOnNewIntentListener(final Consumer<Intent> p0);
    
    void removeOnNewIntentListener(final Consumer<Intent> p0);
}

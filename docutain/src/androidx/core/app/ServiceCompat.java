// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.app;

import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;
import android.os.Build$VERSION;
import android.app.Notification;
import android.app.Service;

public final class ServiceCompat
{
    private static final int FOREGROUND_SERVICE_TYPE_ALLOWED_SINCE_Q = 255;
    private static final int FOREGROUND_SERVICE_TYPE_ALLOWED_SINCE_U = 1073745919;
    public static final int START_STICKY = 1;
    public static final int STOP_FOREGROUND_DETACH = 2;
    public static final int STOP_FOREGROUND_REMOVE = 1;
    
    private ServiceCompat() {
    }
    
    public static void startForeground(final Service service, final int n, final Notification notification, final int n2) {
        if (Build$VERSION.SDK_INT >= 34) {
            Api34Impl.startForeground(service, n, notification, n2);
        }
        else if (Build$VERSION.SDK_INT >= 29) {
            Api29Impl.startForeground(service, n, notification, n2);
        }
        else {
            service.startForeground(n, notification);
        }
    }
    
    public static void stopForeground(final Service service, final int n) {
        if (Build$VERSION.SDK_INT >= 24) {
            Api24Impl.stopForeground(service, n);
        }
        else {
            boolean b = true;
            if ((n & 0x1) == 0x0) {
                b = false;
            }
            service.stopForeground(b);
        }
    }
    
    static class Api24Impl
    {
        private Api24Impl() {
        }
        
        static void stopForeground(final Service service, final int n) {
            service.stopForeground(n);
        }
    }
    
    static class Api29Impl
    {
        private Api29Impl() {
        }
        
        static void startForeground(final Service service, final int n, final Notification notification, final int n2) {
            if (n2 != 0 && n2 != -1) {
                service.startForeground(n, notification, n2 & 0xFF);
            }
            else {
                service.startForeground(n, notification, n2);
            }
        }
    }
    
    static class Api34Impl
    {
        private Api34Impl() {
        }
        
        static void startForeground(final Service service, final int n, final Notification notification, final int n2) {
            if (n2 != 0 && n2 != -1) {
                service.startForeground(n, notification, n2 & 0x40000FFF);
            }
            else {
                service.startForeground(n, notification, n2);
            }
        }
    }
    
    @Retention(RetentionPolicy.SOURCE)
    public @interface StopForegroundFlags {
    }
}

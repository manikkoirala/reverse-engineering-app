// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.app;

import android.graphics.drawable.Icon;
import android.os.Build$VERSION;
import android.app.RemoteAction;
import androidx.core.util.Preconditions;
import androidx.core.graphics.drawable.IconCompat;
import android.app.PendingIntent;
import androidx.versionedparcelable.VersionedParcelable;

public final class RemoteActionCompat implements VersionedParcelable
{
    public PendingIntent mActionIntent;
    public CharSequence mContentDescription;
    public boolean mEnabled;
    public IconCompat mIcon;
    public boolean mShouldShowIcon;
    public CharSequence mTitle;
    
    public RemoteActionCompat() {
    }
    
    public RemoteActionCompat(final RemoteActionCompat remoteActionCompat) {
        Preconditions.checkNotNull(remoteActionCompat);
        this.mIcon = remoteActionCompat.mIcon;
        this.mTitle = remoteActionCompat.mTitle;
        this.mContentDescription = remoteActionCompat.mContentDescription;
        this.mActionIntent = remoteActionCompat.mActionIntent;
        this.mEnabled = remoteActionCompat.mEnabled;
        this.mShouldShowIcon = remoteActionCompat.mShouldShowIcon;
    }
    
    public RemoteActionCompat(final IconCompat iconCompat, final CharSequence charSequence, final CharSequence charSequence2, final PendingIntent pendingIntent) {
        this.mIcon = Preconditions.checkNotNull(iconCompat);
        this.mTitle = Preconditions.checkNotNull(charSequence);
        this.mContentDescription = Preconditions.checkNotNull(charSequence2);
        this.mActionIntent = Preconditions.checkNotNull(pendingIntent);
        this.mEnabled = true;
        this.mShouldShowIcon = true;
    }
    
    public static RemoteActionCompat createFromRemoteAction(final RemoteAction remoteAction) {
        Preconditions.checkNotNull(remoteAction);
        final RemoteActionCompat remoteActionCompat = new RemoteActionCompat(IconCompat.createFromIcon(Api26Impl.getIcon(remoteAction)), Api26Impl.getTitle(remoteAction), Api26Impl.getContentDescription(remoteAction), Api26Impl.getActionIntent(remoteAction));
        remoteActionCompat.setEnabled(Api26Impl.isEnabled(remoteAction));
        if (Build$VERSION.SDK_INT >= 28) {
            remoteActionCompat.setShouldShowIcon(Api28Impl.shouldShowIcon(remoteAction));
        }
        return remoteActionCompat;
    }
    
    public PendingIntent getActionIntent() {
        return this.mActionIntent;
    }
    
    public CharSequence getContentDescription() {
        return this.mContentDescription;
    }
    
    public IconCompat getIcon() {
        return this.mIcon;
    }
    
    public CharSequence getTitle() {
        return this.mTitle;
    }
    
    public boolean isEnabled() {
        return this.mEnabled;
    }
    
    public void setEnabled(final boolean mEnabled) {
        this.mEnabled = mEnabled;
    }
    
    public void setShouldShowIcon(final boolean mShouldShowIcon) {
        this.mShouldShowIcon = mShouldShowIcon;
    }
    
    public boolean shouldShowIcon() {
        return this.mShouldShowIcon;
    }
    
    public RemoteAction toRemoteAction() {
        final RemoteAction remoteAction = Api26Impl.createRemoteAction(this.mIcon.toIcon(), this.mTitle, this.mContentDescription, this.mActionIntent);
        Api26Impl.setEnabled(remoteAction, this.isEnabled());
        if (Build$VERSION.SDK_INT >= 28) {
            Api28Impl.setShouldShowIcon(remoteAction, this.shouldShowIcon());
        }
        return remoteAction;
    }
    
    static class Api26Impl
    {
        private Api26Impl() {
        }
        
        static RemoteAction createRemoteAction(final Icon icon, final CharSequence charSequence, final CharSequence charSequence2, final PendingIntent pendingIntent) {
            return new RemoteAction(icon, charSequence, charSequence2, pendingIntent);
        }
        
        static PendingIntent getActionIntent(final RemoteAction remoteAction) {
            return remoteAction.getActionIntent();
        }
        
        static CharSequence getContentDescription(final RemoteAction remoteAction) {
            return remoteAction.getContentDescription();
        }
        
        static Icon getIcon(final RemoteAction remoteAction) {
            return remoteAction.getIcon();
        }
        
        static CharSequence getTitle(final RemoteAction remoteAction) {
            return remoteAction.getTitle();
        }
        
        static boolean isEnabled(final RemoteAction remoteAction) {
            return remoteAction.isEnabled();
        }
        
        static void setEnabled(final RemoteAction remoteAction, final boolean enabled) {
            remoteAction.setEnabled(enabled);
        }
    }
    
    static class Api28Impl
    {
        private Api28Impl() {
        }
        
        static void setShouldShowIcon(final RemoteAction remoteAction, final boolean shouldShowIcon) {
            remoteAction.setShouldShowIcon(shouldShowIcon);
        }
        
        static boolean shouldShowIcon(final RemoteAction remoteAction) {
            return remoteAction.shouldShowIcon();
        }
    }
}

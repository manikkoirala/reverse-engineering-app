// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.app;

import android.content.Context;

public class AppLocalesStorageHelper
{
    static final String APPLICATION_LOCALES_RECORD_FILE = "androidx.appcompat.app.AppCompatDelegate.application_locales_record_file";
    static final boolean DEBUG = false;
    static final String LOCALE_RECORD_ATTRIBUTE_TAG = "application_locales";
    static final String LOCALE_RECORD_FILE_TAG = "locales";
    static final String TAG = "AppLocalesStorageHelper";
    private static final Object sAppLocaleStorageSync;
    
    static {
        sAppLocaleStorageSync = new Object();
    }
    
    private AppLocalesStorageHelper() {
    }
    
    public static void persistLocales(final Context p0, final String p1) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     3: astore_2       
        //     4: aload_2        
        //     5: monitorenter   
        //     6: aload_1        
        //     7: ldc             ""
        //     9: invokevirtual   java/lang/String.equals:(Ljava/lang/Object;)Z
        //    12: ifeq            25
        //    15: aload_0        
        //    16: ldc             "androidx.appcompat.app.AppCompatDelegate.application_locales_record_file"
        //    18: invokevirtual   android/content/Context.deleteFile:(Ljava/lang/String;)Z
        //    21: pop            
        //    22: aload_2        
        //    23: monitorexit    
        //    24: return         
        //    25: aload_0        
        //    26: ldc             "androidx.appcompat.app.AppCompatDelegate.application_locales_record_file"
        //    28: iconst_0       
        //    29: invokevirtual   android/content/Context.openFileOutput:(Ljava/lang/String;I)Ljava/io/FileOutputStream;
        //    32: astore_0       
        //    33: invokestatic    android/util/Xml.newSerializer:()Lorg/xmlpull/v1/XmlSerializer;
        //    36: astore_3       
        //    37: aload_3        
        //    38: aload_0        
        //    39: aconst_null    
        //    40: invokeinterface org/xmlpull/v1/XmlSerializer.setOutput:(Ljava/io/OutputStream;Ljava/lang/String;)V
        //    45: aload_3        
        //    46: ldc             "UTF-8"
        //    48: iconst_1       
        //    49: invokestatic    java/lang/Boolean.valueOf:(Z)Ljava/lang/Boolean;
        //    52: invokeinterface org/xmlpull/v1/XmlSerializer.startDocument:(Ljava/lang/String;Ljava/lang/Boolean;)V
        //    57: aload_3        
        //    58: aconst_null    
        //    59: ldc             "locales"
        //    61: invokeinterface org/xmlpull/v1/XmlSerializer.startTag:(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;
        //    66: pop            
        //    67: aload_3        
        //    68: aconst_null    
        //    69: ldc             "application_locales"
        //    71: aload_1        
        //    72: invokeinterface org/xmlpull/v1/XmlSerializer.attribute:(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;
        //    77: pop            
        //    78: aload_3        
        //    79: aconst_null    
        //    80: ldc             "locales"
        //    82: invokeinterface org/xmlpull/v1/XmlSerializer.endTag:(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;
        //    87: pop            
        //    88: aload_3        
        //    89: invokeinterface org/xmlpull/v1/XmlSerializer.endDocument:()V
        //    94: aload_0        
        //    95: ifnull          126
        //    98: aload_0        
        //    99: invokevirtual   java/io/FileOutputStream.close:()V
        //   102: goto            126
        //   105: astore_1       
        //   106: goto            129
        //   109: astore_1       
        //   110: ldc             "AppLocalesStorageHelper"
        //   112: ldc             "Storing App Locales : Failed to persist app-locales in storage "
        //   114: aload_1        
        //   115: invokestatic    android/util/Log.w:(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //   118: pop            
        //   119: aload_0        
        //   120: ifnull          126
        //   123: goto            98
        //   126: aload_2        
        //   127: monitorexit    
        //   128: return         
        //   129: aload_0        
        //   130: ifnull          137
        //   133: aload_0        
        //   134: invokevirtual   java/io/FileOutputStream.close:()V
        //   137: aload_1        
        //   138: athrow         
        //   139: astore_0       
        //   140: ldc             "AppLocalesStorageHelper"
        //   142: ldc             "Storing App Locales : FileNotFoundException: Cannot open file %s for writing "
        //   144: iconst_1       
        //   145: anewarray       Ljava/lang/Object;
        //   148: dup            
        //   149: iconst_0       
        //   150: ldc             "androidx.appcompat.app.AppCompatDelegate.application_locales_record_file"
        //   152: aastore        
        //   153: invokestatic    java/lang/String.format:(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
        //   156: invokestatic    android/util/Log.w:(Ljava/lang/String;Ljava/lang/String;)I
        //   159: pop            
        //   160: aload_2        
        //   161: monitorexit    
        //   162: return         
        //   163: astore_0       
        //   164: aload_2        
        //   165: monitorexit    
        //   166: aload_0        
        //   167: athrow         
        //   168: astore_0       
        //   169: goto            126
        //   172: astore_0       
        //   173: goto            137
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                           
        //  -----  -----  -----  -----  -------------------------------
        //  6      24     163    168    Any
        //  25     33     139    163    Ljava/io/FileNotFoundException;
        //  25     33     163    168    Any
        //  33     37     163    168    Any
        //  37     94     109    126    Ljava/lang/Exception;
        //  37     94     105    139    Any
        //  98     102    168    172    Ljava/io/IOException;
        //  98     102    163    168    Any
        //  110    119    105    139    Any
        //  126    128    163    168    Any
        //  133    137    172    176    Ljava/io/IOException;
        //  133    137    163    168    Any
        //  137    139    163    168    Any
        //  140    162    163    168    Any
        //  164    166    163    168    Any
        // 
        // The error that occurred was:
        // 
        // java.lang.IndexOutOfBoundsException: Index 98 out of bounds for length 98
        //     at java.base/jdk.internal.util.Preconditions.outOfBounds(Preconditions.java:64)
        //     at java.base/jdk.internal.util.Preconditions.outOfBoundsCheckIndex(Preconditions.java:70)
        //     at java.base/jdk.internal.util.Preconditions.checkIndex(Preconditions.java:266)
        //     at java.base/java.util.Objects.checkIndex(Objects.java:361)
        //     at java.base/java.util.ArrayList.get(ArrayList.java:427)
        //     at com.strobel.decompiler.ast.AstBuilder.convertToAst(AstBuilder.java:3362)
        //     at com.strobel.decompiler.ast.AstBuilder.build(AstBuilder.java:112)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:203)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public static String readLocales(final Context p0) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     3: astore          6
        //     5: aload           6
        //     7: monitorenter   
        //     8: ldc             ""
        //    10: astore          4
        //    12: aload_0        
        //    13: ldc             "androidx.appcompat.app.AppCompatDelegate.application_locales_record_file"
        //    15: invokevirtual   android/content/Context.openFileInput:(Ljava/lang/String;)Ljava/io/FileInputStream;
        //    18: astore          7
        //    20: invokestatic    android/util/Xml.newPullParser:()Lorg/xmlpull/v1/XmlPullParser;
        //    23: astore          5
        //    25: aload           5
        //    27: aload           7
        //    29: ldc             "UTF-8"
        //    31: invokeinterface org/xmlpull/v1/XmlPullParser.setInput:(Ljava/io/InputStream;Ljava/lang/String;)V
        //    36: aload           5
        //    38: invokeinterface org/xmlpull/v1/XmlPullParser.getDepth:()I
        //    43: istore_2       
        //    44: aload           5
        //    46: invokeinterface org/xmlpull/v1/XmlPullParser.next:()I
        //    51: istore_1       
        //    52: aload           4
        //    54: astore_3       
        //    55: iload_1        
        //    56: iconst_1       
        //    57: if_icmpeq       118
        //    60: iload_1        
        //    61: iconst_3       
        //    62: if_icmpne       79
        //    65: aload           4
        //    67: astore_3       
        //    68: aload           5
        //    70: invokeinterface org/xmlpull/v1/XmlPullParser.getDepth:()I
        //    75: iload_2        
        //    76: if_icmple       118
        //    79: iload_1        
        //    80: iconst_3       
        //    81: if_icmpeq       44
        //    84: iload_1        
        //    85: iconst_4       
        //    86: if_icmpne       92
        //    89: goto            44
        //    92: aload           5
        //    94: invokeinterface org/xmlpull/v1/XmlPullParser.getName:()Ljava/lang/String;
        //    99: ldc             "locales"
        //   101: invokevirtual   java/lang/String.equals:(Ljava/lang/Object;)Z
        //   104: ifeq            44
        //   107: aload           5
        //   109: aconst_null    
        //   110: ldc             "application_locales"
        //   112: invokeinterface org/xmlpull/v1/XmlPullParser.getAttributeValue:(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
        //   117: astore_3       
        //   118: aload_3        
        //   119: astore          5
        //   121: aload           7
        //   123: ifnull          165
        //   126: aload           7
        //   128: invokevirtual   java/io/FileInputStream.close:()V
        //   131: aload_3        
        //   132: astore          5
        //   134: goto            165
        //   137: astore_0       
        //   138: goto            189
        //   141: astore_3       
        //   142: ldc             "AppLocalesStorageHelper"
        //   144: ldc             "Reading app Locales : Unable to parse through file :androidx.appcompat.app.AppCompatDelegate.application_locales_record_file"
        //   146: invokestatic    android/util/Log.w:(Ljava/lang/String;Ljava/lang/String;)I
        //   149: pop            
        //   150: aload           4
        //   152: astore          5
        //   154: aload           7
        //   156: ifnull          165
        //   159: aload           4
        //   161: astore_3       
        //   162: goto            126
        //   165: aload           5
        //   167: invokevirtual   java/lang/String.isEmpty:()Z
        //   170: ifne            176
        //   173: goto            183
        //   176: aload_0        
        //   177: ldc             "androidx.appcompat.app.AppCompatDelegate.application_locales_record_file"
        //   179: invokevirtual   android/content/Context.deleteFile:(Ljava/lang/String;)Z
        //   182: pop            
        //   183: aload           6
        //   185: monitorexit    
        //   186: aload           5
        //   188: areturn        
        //   189: aload           7
        //   191: ifnull          199
        //   194: aload           7
        //   196: invokevirtual   java/io/FileInputStream.close:()V
        //   199: aload_0        
        //   200: athrow         
        //   201: astore_0       
        //   202: aload           6
        //   204: monitorexit    
        //   205: ldc             ""
        //   207: areturn        
        //   208: astore_0       
        //   209: aload           6
        //   211: monitorexit    
        //   212: aload_0        
        //   213: athrow         
        //   214: astore          4
        //   216: aload_3        
        //   217: astore          5
        //   219: goto            165
        //   222: astore_3       
        //   223: goto            199
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                                   
        //  -----  -----  -----  -----  ---------------------------------------
        //  12     20     201    208    Ljava/io/FileNotFoundException;
        //  12     20     208    214    Any
        //  20     44     141    165    Lorg/xmlpull/v1/XmlPullParserException;
        //  20     44     141    165    Ljava/io/IOException;
        //  20     44     137    201    Any
        //  44     52     141    165    Lorg/xmlpull/v1/XmlPullParserException;
        //  44     52     141    165    Ljava/io/IOException;
        //  44     52     137    201    Any
        //  68     79     141    165    Lorg/xmlpull/v1/XmlPullParserException;
        //  68     79     141    165    Ljava/io/IOException;
        //  68     79     137    201    Any
        //  92     118    141    165    Lorg/xmlpull/v1/XmlPullParserException;
        //  92     118    141    165    Ljava/io/IOException;
        //  92     118    137    201    Any
        //  126    131    214    222    Ljava/io/IOException;
        //  126    131    208    214    Any
        //  142    150    137    201    Any
        //  165    173    208    214    Any
        //  176    183    208    214    Any
        //  183    186    208    214    Any
        //  194    199    222    226    Ljava/io/IOException;
        //  194    199    208    214    Any
        //  199    201    208    214    Any
        //  202    205    208    214    Any
        //  209    212    208    214    Any
        // 
        // The error that occurred was:
        // 
        // java.util.ConcurrentModificationException
        //     at java.base/java.util.ArrayList$Itr.checkForComodification(ArrayList.java:1013)
        //     at java.base/java.util.ArrayList$Itr.next(ArrayList.java:967)
        //     at com.strobel.decompiler.ast.AstBuilder.convertLocalVariables(AstBuilder.java:2913)
        //     at com.strobel.decompiler.ast.AstBuilder.performStackAnalysis(AstBuilder.java:2501)
        //     at com.strobel.decompiler.ast.AstBuilder.build(AstBuilder.java:108)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:203)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
}

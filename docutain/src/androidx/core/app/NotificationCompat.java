// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.app;

import android.app.Notification$MessagingStyle$Message;
import androidx.core.text.BidiFormatter;
import android.content.res.ColorStateList;
import android.text.style.TextAppearanceSpan;
import java.util.Collections;
import android.app.Notification$Style;
import android.app.RemoteInput$Builder;
import android.app.Notification$Action$Builder;
import android.app.Notification$CallStyle;
import android.util.Log;
import android.text.style.ForegroundColorSpan;
import android.text.SpannableStringBuilder;
import androidx.core.content.ContextCompat;
import java.util.Objects;
import android.media.AudioAttributes;
import android.media.AudioAttributes$Builder;
import android.net.Uri;
import androidx.core.content.pm.ShortcutInfoCompat;
import android.text.TextUtils;
import android.app.Notification$BubbleMetadata$Builder;
import android.os.SystemClock;
import java.text.NumberFormat;
import android.widget.RemoteViews;
import android.graphics.drawable.Drawable;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.PorterDuffColorFilter;
import android.graphics.PorterDuff$Mode;
import android.graphics.Bitmap$Config;
import android.app.Notification$DecoratedCustomViewStyle;
import android.app.Notification$MessagingStyle;
import android.app.Notification$InboxStyle;
import android.app.Notification$BigTextStyle;
import android.app.Notification$Builder;
import android.app.Notification$BigPictureStyle;
import android.app.Notification$BubbleMetadata;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;
import android.graphics.drawable.Icon;
import java.util.Collection;
import java.util.Arrays;
import android.app.PendingIntent;
import android.content.res.Resources;
import androidx.core.R;
import android.graphics.Bitmap;
import android.content.Context;
import java.util.Iterator;
import android.os.Parcelable;
import android.content.LocusId;
import androidx.core.content.LocusIdCompat;
import java.util.ArrayList;
import java.util.List;
import androidx.core.graphics.drawable.IconCompat;
import java.util.Set;
import android.util.SparseArray;
import android.app.Notification$Action;
import android.os.Bundle;
import android.os.Build$VERSION;
import android.app.Notification;

public class NotificationCompat
{
    public static final int BADGE_ICON_LARGE = 2;
    public static final int BADGE_ICON_NONE = 0;
    public static final int BADGE_ICON_SMALL = 1;
    public static final String CATEGORY_ALARM = "alarm";
    public static final String CATEGORY_CALL = "call";
    public static final String CATEGORY_EMAIL = "email";
    public static final String CATEGORY_ERROR = "err";
    public static final String CATEGORY_EVENT = "event";
    public static final String CATEGORY_LOCATION_SHARING = "location_sharing";
    public static final String CATEGORY_MESSAGE = "msg";
    public static final String CATEGORY_MISSED_CALL = "missed_call";
    public static final String CATEGORY_NAVIGATION = "navigation";
    public static final String CATEGORY_PROGRESS = "progress";
    public static final String CATEGORY_PROMO = "promo";
    public static final String CATEGORY_RECOMMENDATION = "recommendation";
    public static final String CATEGORY_REMINDER = "reminder";
    public static final String CATEGORY_SERVICE = "service";
    public static final String CATEGORY_SOCIAL = "social";
    public static final String CATEGORY_STATUS = "status";
    public static final String CATEGORY_STOPWATCH = "stopwatch";
    public static final String CATEGORY_SYSTEM = "sys";
    public static final String CATEGORY_TRANSPORT = "transport";
    public static final String CATEGORY_WORKOUT = "workout";
    public static final int COLOR_DEFAULT = 0;
    public static final int DEFAULT_ALL = -1;
    public static final int DEFAULT_LIGHTS = 4;
    public static final int DEFAULT_SOUND = 1;
    public static final int DEFAULT_VIBRATE = 2;
    public static final String EXTRA_ANSWER_COLOR = "android.answerColor";
    public static final String EXTRA_ANSWER_INTENT = "android.answerIntent";
    public static final String EXTRA_AUDIO_CONTENTS_URI = "android.audioContents";
    public static final String EXTRA_BACKGROUND_IMAGE_URI = "android.backgroundImageUri";
    public static final String EXTRA_BIG_TEXT = "android.bigText";
    public static final String EXTRA_CALL_IS_VIDEO = "android.callIsVideo";
    public static final String EXTRA_CALL_PERSON = "android.callPerson";
    public static final String EXTRA_CALL_PERSON_COMPAT = "android.callPersonCompat";
    public static final String EXTRA_CALL_TYPE = "android.callType";
    public static final String EXTRA_CHANNEL_GROUP_ID = "android.intent.extra.CHANNEL_GROUP_ID";
    public static final String EXTRA_CHANNEL_ID = "android.intent.extra.CHANNEL_ID";
    public static final String EXTRA_CHRONOMETER_COUNT_DOWN = "android.chronometerCountDown";
    public static final String EXTRA_COLORIZED = "android.colorized";
    public static final String EXTRA_COMPACT_ACTIONS = "android.compactActions";
    public static final String EXTRA_COMPAT_TEMPLATE = "androidx.core.app.extra.COMPAT_TEMPLATE";
    public static final String EXTRA_CONVERSATION_TITLE = "android.conversationTitle";
    public static final String EXTRA_DECLINE_COLOR = "android.declineColor";
    public static final String EXTRA_DECLINE_INTENT = "android.declineIntent";
    public static final String EXTRA_HANG_UP_INTENT = "android.hangUpIntent";
    public static final String EXTRA_HIDDEN_CONVERSATION_TITLE = "android.hiddenConversationTitle";
    public static final String EXTRA_HISTORIC_MESSAGES = "android.messages.historic";
    public static final String EXTRA_INFO_TEXT = "android.infoText";
    public static final String EXTRA_IS_GROUP_CONVERSATION = "android.isGroupConversation";
    public static final String EXTRA_LARGE_ICON = "android.largeIcon";
    public static final String EXTRA_LARGE_ICON_BIG = "android.largeIcon.big";
    public static final String EXTRA_MEDIA_SESSION = "android.mediaSession";
    public static final String EXTRA_MESSAGES = "android.messages";
    public static final String EXTRA_MESSAGING_STYLE_USER = "android.messagingStyleUser";
    public static final String EXTRA_NOTIFICATION_ID = "android.intent.extra.NOTIFICATION_ID";
    public static final String EXTRA_NOTIFICATION_TAG = "android.intent.extra.NOTIFICATION_TAG";
    @Deprecated
    public static final String EXTRA_PEOPLE = "android.people";
    public static final String EXTRA_PEOPLE_LIST = "android.people.list";
    public static final String EXTRA_PICTURE = "android.picture";
    public static final String EXTRA_PICTURE_CONTENT_DESCRIPTION = "android.pictureContentDescription";
    public static final String EXTRA_PICTURE_ICON = "android.pictureIcon";
    public static final String EXTRA_PROGRESS = "android.progress";
    public static final String EXTRA_PROGRESS_INDETERMINATE = "android.progressIndeterminate";
    public static final String EXTRA_PROGRESS_MAX = "android.progressMax";
    public static final String EXTRA_REMOTE_INPUT_HISTORY = "android.remoteInputHistory";
    public static final String EXTRA_SELF_DISPLAY_NAME = "android.selfDisplayName";
    public static final String EXTRA_SHOW_BIG_PICTURE_WHEN_COLLAPSED = "android.showBigPictureWhenCollapsed";
    public static final String EXTRA_SHOW_CHRONOMETER = "android.showChronometer";
    public static final String EXTRA_SHOW_WHEN = "android.showWhen";
    public static final String EXTRA_SMALL_ICON = "android.icon";
    public static final String EXTRA_SUB_TEXT = "android.subText";
    public static final String EXTRA_SUMMARY_TEXT = "android.summaryText";
    public static final String EXTRA_TEMPLATE = "android.template";
    public static final String EXTRA_TEXT = "android.text";
    public static final String EXTRA_TEXT_LINES = "android.textLines";
    public static final String EXTRA_TITLE = "android.title";
    public static final String EXTRA_TITLE_BIG = "android.title.big";
    public static final String EXTRA_VERIFICATION_ICON = "android.verificationIcon";
    public static final String EXTRA_VERIFICATION_ICON_COMPAT = "android.verificationIconCompat";
    public static final String EXTRA_VERIFICATION_TEXT = "android.verificationText";
    public static final int FLAG_AUTO_CANCEL = 16;
    public static final int FLAG_BUBBLE = 4096;
    public static final int FLAG_FOREGROUND_SERVICE = 64;
    public static final int FLAG_GROUP_SUMMARY = 512;
    @Deprecated
    public static final int FLAG_HIGH_PRIORITY = 128;
    public static final int FLAG_INSISTENT = 4;
    public static final int FLAG_LOCAL_ONLY = 256;
    public static final int FLAG_NO_CLEAR = 32;
    public static final int FLAG_ONGOING_EVENT = 2;
    public static final int FLAG_ONLY_ALERT_ONCE = 8;
    public static final int FLAG_SHOW_LIGHTS = 1;
    public static final int FOREGROUND_SERVICE_DEFAULT = 0;
    public static final int FOREGROUND_SERVICE_DEFERRED = 2;
    public static final int FOREGROUND_SERVICE_IMMEDIATE = 1;
    public static final int GROUP_ALERT_ALL = 0;
    public static final int GROUP_ALERT_CHILDREN = 2;
    public static final int GROUP_ALERT_SUMMARY = 1;
    public static final String GROUP_KEY_SILENT = "silent";
    public static final String INTENT_CATEGORY_NOTIFICATION_PREFERENCES = "android.intent.category.NOTIFICATION_PREFERENCES";
    public static final int MAX_ACTION_BUTTONS = 3;
    public static final int PRIORITY_DEFAULT = 0;
    public static final int PRIORITY_HIGH = 1;
    public static final int PRIORITY_LOW = -1;
    public static final int PRIORITY_MAX = 2;
    public static final int PRIORITY_MIN = -2;
    public static final int STREAM_DEFAULT = -1;
    private static final String TAG = "NotifCompat";
    public static final int VISIBILITY_PRIVATE = 0;
    public static final int VISIBILITY_PUBLIC = 1;
    public static final int VISIBILITY_SECRET = -1;
    
    @Deprecated
    public NotificationCompat() {
    }
    
    public static Action getAction(final Notification notification, final int n) {
        if (Build$VERSION.SDK_INT >= 20) {
            return getActionCompatFromAction(notification.actions[n]);
        }
        final int sdk_INT = Build$VERSION.SDK_INT;
        final Bundle bundle = null;
        if (sdk_INT >= 19) {
            final Notification$Action notification$Action = notification.actions[n];
            final SparseArray sparseParcelableArray = notification.extras.getSparseParcelableArray("android.support.actionExtras");
            Bundle bundle2 = bundle;
            if (sparseParcelableArray != null) {
                bundle2 = (Bundle)sparseParcelableArray.get(n);
            }
            return NotificationCompatJellybean.readAction(notification$Action.icon, notification$Action.title, notification$Action.actionIntent, bundle2);
        }
        if (Build$VERSION.SDK_INT >= 16) {
            return NotificationCompatJellybean.getAction(notification, n);
        }
        return null;
    }
    
    static Action getActionCompatFromAction(final Notification$Action notification$Action) {
        final android.app.RemoteInput[] remoteInputs = Api20Impl.getRemoteInputs(notification$Action);
        IconCompat fromIconOrNullIfZeroResId = null;
        RemoteInput[] array;
        if (remoteInputs == null) {
            array = null;
        }
        else {
            array = new RemoteInput[remoteInputs.length];
            for (int i = 0; i < remoteInputs.length; ++i) {
                final android.app.RemoteInput remoteInput = remoteInputs[i];
                final String resultKey = Api20Impl.getResultKey(remoteInput);
                final CharSequence label = Api20Impl.getLabel(remoteInput);
                final CharSequence[] choices = Api20Impl.getChoices(remoteInput);
                final boolean allowFreeFormInput = Api20Impl.getAllowFreeFormInput(remoteInput);
                int editChoicesBeforeSending;
                if (Build$VERSION.SDK_INT >= 29) {
                    editChoicesBeforeSending = Api29Impl.getEditChoicesBeforeSending(remoteInput);
                }
                else {
                    editChoicesBeforeSending = 0;
                }
                array[i] = new RemoteInput(resultKey, label, choices, allowFreeFormInput, editChoicesBeforeSending, Api20Impl.getExtras(remoteInput), null);
            }
        }
        boolean boolean1;
        if (Build$VERSION.SDK_INT >= 24) {
            boolean1 = (Api20Impl.getExtras(notification$Action).getBoolean("android.support.allowGeneratedReplies") || Api24Impl.getAllowGeneratedReplies(notification$Action));
        }
        else {
            boolean1 = Api20Impl.getExtras(notification$Action).getBoolean("android.support.allowGeneratedReplies");
        }
        final boolean boolean2 = Api20Impl.getExtras(notification$Action).getBoolean("android.support.action.showsUserInterface", true);
        int n;
        if (Build$VERSION.SDK_INT >= 28) {
            n = Api28Impl.getSemanticAction(notification$Action);
        }
        else {
            n = Api20Impl.getExtras(notification$Action).getInt("android.support.action.semanticAction", 0);
        }
        final boolean b = Build$VERSION.SDK_INT >= 29 && Api29Impl.isContextual(notification$Action);
        final boolean b2 = Build$VERSION.SDK_INT >= 31 && Api31Impl.isAuthenticationRequired(notification$Action);
        if (Build$VERSION.SDK_INT < 23) {
            return new Action(notification$Action.icon, notification$Action.title, notification$Action.actionIntent, Api20Impl.getExtras(notification$Action), array, null, boolean1, n, boolean2, b, b2);
        }
        if (Api23Impl.getIcon(notification$Action) == null && notification$Action.icon != 0) {
            return new Action(notification$Action.icon, notification$Action.title, notification$Action.actionIntent, Api20Impl.getExtras(notification$Action), array, null, boolean1, n, boolean2, b, b2);
        }
        if (Api23Impl.getIcon(notification$Action) != null) {
            fromIconOrNullIfZeroResId = IconCompat.createFromIconOrNullIfZeroResId(Api23Impl.getIcon(notification$Action));
        }
        return new Action(fromIconOrNullIfZeroResId, notification$Action.title, notification$Action.actionIntent, Api20Impl.getExtras(notification$Action), array, null, boolean1, n, boolean2, b, b2);
    }
    
    public static int getActionCount(final Notification notification) {
        final int sdk_INT = Build$VERSION.SDK_INT;
        int length = 0;
        if (sdk_INT >= 19) {
            if (notification.actions != null) {
                length = notification.actions.length;
            }
            return length;
        }
        if (Build$VERSION.SDK_INT >= 16) {
            return NotificationCompatJellybean.getActionCount(notification);
        }
        return 0;
    }
    
    public static boolean getAllowSystemGeneratedContextualActions(final Notification notification) {
        return Build$VERSION.SDK_INT >= 29 && Api29Impl.getAllowSystemGeneratedContextualActions(notification);
    }
    
    public static boolean getAutoCancel(final Notification notification) {
        return (notification.flags & 0x10) != 0x0;
    }
    
    public static int getBadgeIconType(final Notification notification) {
        if (Build$VERSION.SDK_INT >= 26) {
            return Api26Impl.getBadgeIconType(notification);
        }
        return 0;
    }
    
    public static BubbleMetadata getBubbleMetadata(final Notification notification) {
        if (Build$VERSION.SDK_INT >= 29) {
            return BubbleMetadata.fromPlatform(Api29Impl.getBubbleMetadata(notification));
        }
        return null;
    }
    
    public static String getCategory(final Notification notification) {
        if (Build$VERSION.SDK_INT >= 21) {
            return notification.category;
        }
        return null;
    }
    
    public static String getChannelId(final Notification notification) {
        if (Build$VERSION.SDK_INT >= 26) {
            return Api26Impl.getChannelId(notification);
        }
        return null;
    }
    
    public static int getColor(final Notification notification) {
        if (Build$VERSION.SDK_INT >= 21) {
            return notification.color;
        }
        return 0;
    }
    
    public static CharSequence getContentInfo(final Notification notification) {
        return notification.extras.getCharSequence("android.infoText");
    }
    
    public static CharSequence getContentText(final Notification notification) {
        return notification.extras.getCharSequence("android.text");
    }
    
    public static CharSequence getContentTitle(final Notification notification) {
        return notification.extras.getCharSequence("android.title");
    }
    
    public static Bundle getExtras(final Notification notification) {
        if (Build$VERSION.SDK_INT >= 19) {
            return notification.extras;
        }
        if (Build$VERSION.SDK_INT >= 16) {
            return NotificationCompatJellybean.getExtras(notification);
        }
        return null;
    }
    
    public static String getGroup(final Notification notification) {
        if (Build$VERSION.SDK_INT >= 20) {
            return Api20Impl.getGroup(notification);
        }
        if (Build$VERSION.SDK_INT >= 19) {
            return notification.extras.getString("android.support.groupKey");
        }
        if (Build$VERSION.SDK_INT >= 16) {
            return NotificationCompatJellybean.getExtras(notification).getString("android.support.groupKey");
        }
        return null;
    }
    
    public static int getGroupAlertBehavior(final Notification notification) {
        if (Build$VERSION.SDK_INT >= 26) {
            return Api26Impl.getGroupAlertBehavior(notification);
        }
        return 0;
    }
    
    static boolean getHighPriority(final Notification notification) {
        return (notification.flags & 0x80) != 0x0;
    }
    
    public static List<Action> getInvisibleActions(final Notification notification) {
        final ArrayList list = new ArrayList();
        if (Build$VERSION.SDK_INT >= 19) {
            final Bundle bundle = notification.extras.getBundle("android.car.EXTENSIONS");
            if (bundle == null) {
                return list;
            }
            final Bundle bundle2 = bundle.getBundle("invisible_actions");
            if (bundle2 != null) {
                for (int i = 0; i < bundle2.size(); ++i) {
                    list.add(NotificationCompatJellybean.getActionFromBundle(bundle2.getBundle(Integer.toString(i))));
                }
            }
        }
        return list;
    }
    
    public static boolean getLocalOnly(final Notification notification) {
        final int sdk_INT = Build$VERSION.SDK_INT;
        boolean b = false;
        if (sdk_INT >= 20) {
            if ((notification.flags & 0x100) != 0x0) {
                b = true;
            }
            return b;
        }
        if (Build$VERSION.SDK_INT >= 19) {
            return notification.extras.getBoolean("android.support.localOnly");
        }
        return Build$VERSION.SDK_INT >= 16 && NotificationCompatJellybean.getExtras(notification).getBoolean("android.support.localOnly");
    }
    
    public static LocusIdCompat getLocusId(final Notification notification) {
        final int sdk_INT = Build$VERSION.SDK_INT;
        LocusIdCompat locusIdCompat = null;
        if (sdk_INT >= 29) {
            final LocusId locusId = Api29Impl.getLocusId(notification);
            if (locusId == null) {
                locusIdCompat = locusIdCompat;
            }
            else {
                locusIdCompat = LocusIdCompat.toLocusIdCompat(locusId);
            }
        }
        return locusIdCompat;
    }
    
    static Notification[] getNotificationArrayFromBundle(final Bundle bundle, final String s) {
        final Parcelable[] parcelableArray = bundle.getParcelableArray(s);
        if (!(parcelableArray instanceof Notification[]) && parcelableArray != null) {
            final Notification[] array = new Notification[parcelableArray.length];
            for (int i = 0; i < parcelableArray.length; ++i) {
                array[i] = (Notification)parcelableArray[i];
            }
            bundle.putParcelableArray(s, (Parcelable[])array);
            return array;
        }
        return (Notification[])parcelableArray;
    }
    
    public static boolean getOngoing(final Notification notification) {
        return (notification.flags & 0x2) != 0x0;
    }
    
    public static boolean getOnlyAlertOnce(final Notification notification) {
        return (notification.flags & 0x8) != 0x0;
    }
    
    public static List<Person> getPeople(final Notification notification) {
        final ArrayList list = new ArrayList();
        if (Build$VERSION.SDK_INT >= 28) {
            final ArrayList parcelableArrayList = notification.extras.getParcelableArrayList("android.people.list");
            if (parcelableArrayList != null && !parcelableArrayList.isEmpty()) {
                final Iterator iterator = parcelableArrayList.iterator();
                while (iterator.hasNext()) {
                    list.add(Person.fromAndroidPerson((android.app.Person)iterator.next()));
                }
            }
        }
        else if (Build$VERSION.SDK_INT >= 19) {
            final String[] stringArray = notification.extras.getStringArray("android.people");
            if (stringArray != null && stringArray.length != 0) {
                for (int length = stringArray.length, i = 0; i < length; ++i) {
                    list.add(new Person.Builder().setUri(stringArray[i]).build());
                }
            }
        }
        return list;
    }
    
    public static Notification getPublicVersion(final Notification notification) {
        if (Build$VERSION.SDK_INT >= 21) {
            return notification.publicVersion;
        }
        return null;
    }
    
    public static CharSequence getSettingsText(final Notification notification) {
        if (Build$VERSION.SDK_INT >= 26) {
            return Api26Impl.getSettingsText(notification);
        }
        return null;
    }
    
    public static String getShortcutId(final Notification notification) {
        if (Build$VERSION.SDK_INT >= 26) {
            return Api26Impl.getShortcutId(notification);
        }
        return null;
    }
    
    public static boolean getShowWhen(final Notification notification) {
        return notification.extras.getBoolean("android.showWhen");
    }
    
    public static String getSortKey(final Notification notification) {
        if (Build$VERSION.SDK_INT >= 20) {
            return Api20Impl.getSortKey(notification);
        }
        if (Build$VERSION.SDK_INT >= 19) {
            return notification.extras.getString("android.support.sortKey");
        }
        if (Build$VERSION.SDK_INT >= 16) {
            return NotificationCompatJellybean.getExtras(notification).getString("android.support.sortKey");
        }
        return null;
    }
    
    public static CharSequence getSubText(final Notification notification) {
        return notification.extras.getCharSequence("android.subText");
    }
    
    public static long getTimeoutAfter(final Notification notification) {
        if (Build$VERSION.SDK_INT >= 26) {
            return Api26Impl.getTimeoutAfter(notification);
        }
        return 0L;
    }
    
    public static boolean getUsesChronometer(final Notification notification) {
        return notification.extras.getBoolean("android.showChronometer");
    }
    
    public static int getVisibility(final Notification notification) {
        if (Build$VERSION.SDK_INT >= 21) {
            return notification.visibility;
        }
        return 0;
    }
    
    public static boolean isGroupSummary(final Notification notification) {
        final int sdk_INT = Build$VERSION.SDK_INT;
        boolean b = false;
        if (sdk_INT >= 20) {
            if ((notification.flags & 0x200) != 0x0) {
                b = true;
            }
            return b;
        }
        if (Build$VERSION.SDK_INT >= 19) {
            return notification.extras.getBoolean("android.support.isGroupSummary");
        }
        return Build$VERSION.SDK_INT >= 16 && NotificationCompatJellybean.getExtras(notification).getBoolean("android.support.isGroupSummary");
    }
    
    public static Bitmap reduceLargeIconSize(final Context context, final Bitmap bitmap) {
        if (bitmap == null || Build$VERSION.SDK_INT >= 27) {
            return bitmap;
        }
        final Resources resources = context.getResources();
        final int dimensionPixelSize = resources.getDimensionPixelSize(R.dimen.compat_notification_large_icon_max_width);
        final int dimensionPixelSize2 = resources.getDimensionPixelSize(R.dimen.compat_notification_large_icon_max_height);
        if (bitmap.getWidth() <= dimensionPixelSize && bitmap.getHeight() <= dimensionPixelSize2) {
            return bitmap;
        }
        final double min = Math.min(dimensionPixelSize / (double)Math.max(1, bitmap.getWidth()), dimensionPixelSize2 / (double)Math.max(1, bitmap.getHeight()));
        return Bitmap.createScaledBitmap(bitmap, (int)Math.ceil(bitmap.getWidth() * min), (int)Math.ceil(bitmap.getHeight() * min), true);
    }
    
    public static class Action
    {
        static final String EXTRA_SEMANTIC_ACTION = "android.support.action.semanticAction";
        static final String EXTRA_SHOWS_USER_INTERFACE = "android.support.action.showsUserInterface";
        public static final int SEMANTIC_ACTION_ARCHIVE = 5;
        public static final int SEMANTIC_ACTION_CALL = 10;
        public static final int SEMANTIC_ACTION_DELETE = 4;
        public static final int SEMANTIC_ACTION_MARK_AS_READ = 2;
        public static final int SEMANTIC_ACTION_MARK_AS_UNREAD = 3;
        public static final int SEMANTIC_ACTION_MUTE = 6;
        public static final int SEMANTIC_ACTION_NONE = 0;
        public static final int SEMANTIC_ACTION_REPLY = 1;
        public static final int SEMANTIC_ACTION_THUMBS_DOWN = 9;
        public static final int SEMANTIC_ACTION_THUMBS_UP = 8;
        public static final int SEMANTIC_ACTION_UNMUTE = 7;
        public PendingIntent actionIntent;
        @Deprecated
        public int icon;
        private boolean mAllowGeneratedReplies;
        private boolean mAuthenticationRequired;
        private final RemoteInput[] mDataOnlyRemoteInputs;
        final Bundle mExtras;
        private IconCompat mIcon;
        private final boolean mIsContextual;
        private final RemoteInput[] mRemoteInputs;
        private final int mSemanticAction;
        boolean mShowsUserInterface;
        public CharSequence title;
        
        public Action(final int n, final CharSequence charSequence, final PendingIntent pendingIntent) {
            IconCompat withResource = null;
            if (n != 0) {
                withResource = IconCompat.createWithResource(null, "", n);
            }
            this(withResource, charSequence, pendingIntent);
        }
        
        Action(final int n, final CharSequence charSequence, final PendingIntent pendingIntent, final Bundle bundle, final RemoteInput[] array, final RemoteInput[] array2, final boolean b, final int n2, final boolean b2, final boolean b3, final boolean b4) {
            IconCompat withResource = null;
            if (n != 0) {
                withResource = IconCompat.createWithResource(null, "", n);
            }
            this(withResource, charSequence, pendingIntent, bundle, array, array2, b, n2, b2, b3, b4);
        }
        
        public Action(final IconCompat iconCompat, final CharSequence charSequence, final PendingIntent pendingIntent) {
            this(iconCompat, charSequence, pendingIntent, new Bundle(), null, null, true, 0, true, false, false);
        }
        
        Action(final IconCompat mIcon, final CharSequence charSequence, final PendingIntent actionIntent, Bundle mExtras, final RemoteInput[] mRemoteInputs, final RemoteInput[] mDataOnlyRemoteInputs, final boolean mAllowGeneratedReplies, final int mSemanticAction, final boolean mShowsUserInterface, final boolean mIsContextual, final boolean mAuthenticationRequired) {
            this.mShowsUserInterface = true;
            this.mIcon = mIcon;
            if (mIcon != null && mIcon.getType() == 2) {
                this.icon = mIcon.getResId();
            }
            this.title = NotificationCompat.Builder.limitCharSequenceLength(charSequence);
            this.actionIntent = actionIntent;
            if (mExtras == null) {
                mExtras = new Bundle();
            }
            this.mExtras = mExtras;
            this.mRemoteInputs = mRemoteInputs;
            this.mDataOnlyRemoteInputs = mDataOnlyRemoteInputs;
            this.mAllowGeneratedReplies = mAllowGeneratedReplies;
            this.mSemanticAction = mSemanticAction;
            this.mShowsUserInterface = mShowsUserInterface;
            this.mIsContextual = mIsContextual;
            this.mAuthenticationRequired = mAuthenticationRequired;
        }
        
        public PendingIntent getActionIntent() {
            return this.actionIntent;
        }
        
        public boolean getAllowGeneratedReplies() {
            return this.mAllowGeneratedReplies;
        }
        
        public RemoteInput[] getDataOnlyRemoteInputs() {
            return this.mDataOnlyRemoteInputs;
        }
        
        public Bundle getExtras() {
            return this.mExtras;
        }
        
        @Deprecated
        public int getIcon() {
            return this.icon;
        }
        
        public IconCompat getIconCompat() {
            if (this.mIcon == null) {
                final int icon = this.icon;
                if (icon != 0) {
                    this.mIcon = IconCompat.createWithResource(null, "", icon);
                }
            }
            return this.mIcon;
        }
        
        public RemoteInput[] getRemoteInputs() {
            return this.mRemoteInputs;
        }
        
        public int getSemanticAction() {
            return this.mSemanticAction;
        }
        
        public boolean getShowsUserInterface() {
            return this.mShowsUserInterface;
        }
        
        public CharSequence getTitle() {
            return this.title;
        }
        
        public boolean isAuthenticationRequired() {
            return this.mAuthenticationRequired;
        }
        
        public boolean isContextual() {
            return this.mIsContextual;
        }
        
        public static final class Builder
        {
            private boolean mAllowGeneratedReplies;
            private boolean mAuthenticationRequired;
            private final Bundle mExtras;
            private final IconCompat mIcon;
            private final PendingIntent mIntent;
            private boolean mIsContextual;
            private ArrayList<RemoteInput> mRemoteInputs;
            private int mSemanticAction;
            private boolean mShowsUserInterface;
            private final CharSequence mTitle;
            
            public Builder(final int n, final CharSequence charSequence, final PendingIntent pendingIntent) {
                IconCompat withResource = null;
                if (n != 0) {
                    withResource = IconCompat.createWithResource(null, "", n);
                }
                this(withResource, charSequence, pendingIntent, new Bundle(), null, true, 0, true, false, false);
            }
            
            public Builder(final Action action) {
                this(action.getIconCompat(), action.title, action.actionIntent, new Bundle(action.mExtras), action.getRemoteInputs(), action.getAllowGeneratedReplies(), action.getSemanticAction(), action.mShowsUserInterface, action.isContextual(), action.isAuthenticationRequired());
            }
            
            public Builder(final IconCompat iconCompat, final CharSequence charSequence, final PendingIntent pendingIntent) {
                this(iconCompat, charSequence, pendingIntent, new Bundle(), null, true, 0, true, false, false);
            }
            
            private Builder(final IconCompat mIcon, final CharSequence charSequence, final PendingIntent mIntent, final Bundle mExtras, final RemoteInput[] a, final boolean mAllowGeneratedReplies, final int mSemanticAction, final boolean mShowsUserInterface, final boolean mIsContextual, final boolean mAuthenticationRequired) {
                this.mAllowGeneratedReplies = true;
                this.mShowsUserInterface = true;
                this.mIcon = mIcon;
                this.mTitle = NotificationCompat.Builder.limitCharSequenceLength(charSequence);
                this.mIntent = mIntent;
                this.mExtras = mExtras;
                ArrayList<RemoteInput> mRemoteInputs;
                if (a == null) {
                    mRemoteInputs = null;
                }
                else {
                    mRemoteInputs = new ArrayList<RemoteInput>(Arrays.asList(a));
                }
                this.mRemoteInputs = mRemoteInputs;
                this.mAllowGeneratedReplies = mAllowGeneratedReplies;
                this.mSemanticAction = mSemanticAction;
                this.mShowsUserInterface = mShowsUserInterface;
                this.mIsContextual = mIsContextual;
                this.mAuthenticationRequired = mAuthenticationRequired;
            }
            
            private void checkContextualActionNullFields() {
                if (!this.mIsContextual) {
                    return;
                }
                if (this.mIntent != null) {
                    return;
                }
                throw new NullPointerException("Contextual Actions must contain a valid PendingIntent");
            }
            
            public static Builder fromAndroidAction(final Notification$Action notification$Action) {
                Builder builder;
                if (Build$VERSION.SDK_INT >= 23 && Api23Impl.getIcon(notification$Action) != null) {
                    builder = new Builder(IconCompat.createFromIconOrNullIfZeroResId(Api23Impl.getIcon(notification$Action)), notification$Action.title, notification$Action.actionIntent);
                }
                else {
                    builder = new Builder(notification$Action.icon, notification$Action.title, notification$Action.actionIntent);
                }
                if (Build$VERSION.SDK_INT >= 20) {
                    final android.app.RemoteInput[] remoteInputs = Api20Impl.getRemoteInputs(notification$Action);
                    if (remoteInputs != null && remoteInputs.length != 0) {
                        for (int length = remoteInputs.length, i = 0; i < length; ++i) {
                            builder.addRemoteInput(RemoteInput.fromPlatform(remoteInputs[i]));
                        }
                    }
                }
                if (Build$VERSION.SDK_INT >= 24) {
                    builder.mAllowGeneratedReplies = Api24Impl.getAllowGeneratedReplies(notification$Action);
                }
                if (Build$VERSION.SDK_INT >= 28) {
                    builder.setSemanticAction(Api28Impl.getSemanticAction(notification$Action));
                }
                if (Build$VERSION.SDK_INT >= 29) {
                    builder.setContextual(Api29Impl.isContextual(notification$Action));
                }
                if (Build$VERSION.SDK_INT >= 31) {
                    builder.setAuthenticationRequired(Api31Impl.isAuthenticationRequired(notification$Action));
                }
                if (Build$VERSION.SDK_INT >= 20) {
                    builder.addExtras(Api20Impl.getExtras(notification$Action));
                }
                return builder;
            }
            
            public Builder addExtras(final Bundle bundle) {
                if (bundle != null) {
                    this.mExtras.putAll(bundle);
                }
                return this;
            }
            
            public Builder addRemoteInput(final RemoteInput e) {
                if (this.mRemoteInputs == null) {
                    this.mRemoteInputs = new ArrayList<RemoteInput>();
                }
                if (e != null) {
                    this.mRemoteInputs.add(e);
                }
                return this;
            }
            
            public Action build() {
                this.checkContextualActionNullFields();
                final ArrayList list = new ArrayList();
                final ArrayList list2 = new ArrayList();
                final ArrayList<RemoteInput> mRemoteInputs = this.mRemoteInputs;
                if (mRemoteInputs != null) {
                    for (final RemoteInput remoteInput : mRemoteInputs) {
                        if (remoteInput.isDataOnly()) {
                            list.add(remoteInput);
                        }
                        else {
                            list2.add(remoteInput);
                        }
                    }
                }
                final boolean empty = list.isEmpty();
                RemoteInput[] array = null;
                RemoteInput[] array2;
                if (empty) {
                    array2 = null;
                }
                else {
                    array2 = (RemoteInput[])list.toArray(new RemoteInput[list.size()]);
                }
                if (!list2.isEmpty()) {
                    array = (RemoteInput[])list2.toArray(new RemoteInput[list2.size()]);
                }
                return new Action(this.mIcon, this.mTitle, this.mIntent, this.mExtras, array, array2, this.mAllowGeneratedReplies, this.mSemanticAction, this.mShowsUserInterface, this.mIsContextual, this.mAuthenticationRequired);
            }
            
            public Builder extend(final Extender extender) {
                extender.extend(this);
                return this;
            }
            
            public Bundle getExtras() {
                return this.mExtras;
            }
            
            public Builder setAllowGeneratedReplies(final boolean mAllowGeneratedReplies) {
                this.mAllowGeneratedReplies = mAllowGeneratedReplies;
                return this;
            }
            
            public Builder setAuthenticationRequired(final boolean mAuthenticationRequired) {
                this.mAuthenticationRequired = mAuthenticationRequired;
                return this;
            }
            
            public Builder setContextual(final boolean mIsContextual) {
                this.mIsContextual = mIsContextual;
                return this;
            }
            
            public Builder setSemanticAction(final int mSemanticAction) {
                this.mSemanticAction = mSemanticAction;
                return this;
            }
            
            public Builder setShowsUserInterface(final boolean mShowsUserInterface) {
                this.mShowsUserInterface = mShowsUserInterface;
                return this;
            }
            
            static class Api20Impl
            {
                private Api20Impl() {
                }
                
                static Bundle getExtras(final Notification$Action notification$Action) {
                    return notification$Action.getExtras();
                }
                
                static android.app.RemoteInput[] getRemoteInputs(final Notification$Action notification$Action) {
                    return notification$Action.getRemoteInputs();
                }
            }
            
            static class Api23Impl
            {
                private Api23Impl() {
                }
                
                static Icon getIcon(final Notification$Action notification$Action) {
                    return notification$Action.getIcon();
                }
            }
            
            static class Api24Impl
            {
                private Api24Impl() {
                }
                
                static boolean getAllowGeneratedReplies(final Notification$Action notification$Action) {
                    return notification$Action.getAllowGeneratedReplies();
                }
            }
            
            static class Api28Impl
            {
                private Api28Impl() {
                }
                
                static int getSemanticAction(final Notification$Action notification$Action) {
                    return notification$Action.getSemanticAction();
                }
            }
            
            static class Api29Impl
            {
                private Api29Impl() {
                }
                
                static boolean isContextual(final Notification$Action notification$Action) {
                    return notification$Action.isContextual();
                }
            }
            
            static class Api31Impl
            {
                private Api31Impl() {
                }
                
                static boolean isAuthenticationRequired(final Notification$Action notification$Action) {
                    return notification$Action.isAuthenticationRequired();
                }
            }
        }
        
        public interface Extender
        {
            Builder extend(final Builder p0);
        }
        
        @Retention(RetentionPolicy.SOURCE)
        public @interface SemanticAction {
        }
        
        public static final class WearableExtender implements Extender
        {
            private static final int DEFAULT_FLAGS = 1;
            private static final String EXTRA_WEARABLE_EXTENSIONS = "android.wearable.EXTENSIONS";
            private static final int FLAG_AVAILABLE_OFFLINE = 1;
            private static final int FLAG_HINT_DISPLAY_INLINE = 4;
            private static final int FLAG_HINT_LAUNCHES_ACTIVITY = 2;
            private static final String KEY_CANCEL_LABEL = "cancelLabel";
            private static final String KEY_CONFIRM_LABEL = "confirmLabel";
            private static final String KEY_FLAGS = "flags";
            private static final String KEY_IN_PROGRESS_LABEL = "inProgressLabel";
            private CharSequence mCancelLabel;
            private CharSequence mConfirmLabel;
            private int mFlags;
            private CharSequence mInProgressLabel;
            
            public WearableExtender() {
                this.mFlags = 1;
            }
            
            public WearableExtender(final Action action) {
                this.mFlags = 1;
                final Bundle bundle = action.getExtras().getBundle("android.wearable.EXTENSIONS");
                if (bundle != null) {
                    this.mFlags = bundle.getInt("flags", 1);
                    this.mInProgressLabel = bundle.getCharSequence("inProgressLabel");
                    this.mConfirmLabel = bundle.getCharSequence("confirmLabel");
                    this.mCancelLabel = bundle.getCharSequence("cancelLabel");
                }
            }
            
            private void setFlag(final int n, final boolean b) {
                if (b) {
                    this.mFlags |= n;
                }
                else {
                    this.mFlags &= ~n;
                }
            }
            
            public WearableExtender clone() {
                final WearableExtender wearableExtender = new WearableExtender();
                wearableExtender.mFlags = this.mFlags;
                wearableExtender.mInProgressLabel = this.mInProgressLabel;
                wearableExtender.mConfirmLabel = this.mConfirmLabel;
                wearableExtender.mCancelLabel = this.mCancelLabel;
                return wearableExtender;
            }
            
            @Override
            public Builder extend(final Builder builder) {
                final Bundle bundle = new Bundle();
                final int mFlags = this.mFlags;
                if (mFlags != 1) {
                    bundle.putInt("flags", mFlags);
                }
                final CharSequence mInProgressLabel = this.mInProgressLabel;
                if (mInProgressLabel != null) {
                    bundle.putCharSequence("inProgressLabel", mInProgressLabel);
                }
                final CharSequence mConfirmLabel = this.mConfirmLabel;
                if (mConfirmLabel != null) {
                    bundle.putCharSequence("confirmLabel", mConfirmLabel);
                }
                final CharSequence mCancelLabel = this.mCancelLabel;
                if (mCancelLabel != null) {
                    bundle.putCharSequence("cancelLabel", mCancelLabel);
                }
                builder.getExtras().putBundle("android.wearable.EXTENSIONS", bundle);
                return builder;
            }
            
            @Deprecated
            public CharSequence getCancelLabel() {
                return this.mCancelLabel;
            }
            
            @Deprecated
            public CharSequence getConfirmLabel() {
                return this.mConfirmLabel;
            }
            
            public boolean getHintDisplayActionInline() {
                return (this.mFlags & 0x4) != 0x0;
            }
            
            public boolean getHintLaunchesActivity() {
                return (this.mFlags & 0x2) != 0x0;
            }
            
            @Deprecated
            public CharSequence getInProgressLabel() {
                return this.mInProgressLabel;
            }
            
            public boolean isAvailableOffline() {
                final int mFlags = this.mFlags;
                boolean b = true;
                if ((mFlags & 0x1) == 0x0) {
                    b = false;
                }
                return b;
            }
            
            public WearableExtender setAvailableOffline(final boolean b) {
                this.setFlag(1, b);
                return this;
            }
            
            @Deprecated
            public WearableExtender setCancelLabel(final CharSequence mCancelLabel) {
                this.mCancelLabel = mCancelLabel;
                return this;
            }
            
            @Deprecated
            public WearableExtender setConfirmLabel(final CharSequence mConfirmLabel) {
                this.mConfirmLabel = mConfirmLabel;
                return this;
            }
            
            public WearableExtender setHintDisplayActionInline(final boolean b) {
                this.setFlag(4, b);
                return this;
            }
            
            public WearableExtender setHintLaunchesActivity(final boolean b) {
                this.setFlag(2, b);
                return this;
            }
            
            @Deprecated
            public WearableExtender setInProgressLabel(final CharSequence mInProgressLabel) {
                this.mInProgressLabel = mInProgressLabel;
                return this;
            }
        }
    }
    
    static class Api20Impl
    {
        private Api20Impl() {
        }
        
        static boolean getAllowFreeFormInput(final android.app.RemoteInput remoteInput) {
            return remoteInput.getAllowFreeFormInput();
        }
        
        static CharSequence[] getChoices(final android.app.RemoteInput remoteInput) {
            return remoteInput.getChoices();
        }
        
        static Bundle getExtras(final Notification$Action notification$Action) {
            return notification$Action.getExtras();
        }
        
        static Bundle getExtras(final android.app.RemoteInput remoteInput) {
            return remoteInput.getExtras();
        }
        
        static String getGroup(final Notification notification) {
            return notification.getGroup();
        }
        
        static CharSequence getLabel(final android.app.RemoteInput remoteInput) {
            return remoteInput.getLabel();
        }
        
        static android.app.RemoteInput[] getRemoteInputs(final Notification$Action notification$Action) {
            return notification$Action.getRemoteInputs();
        }
        
        static String getResultKey(final android.app.RemoteInput remoteInput) {
            return remoteInput.getResultKey();
        }
        
        static String getSortKey(final Notification notification) {
            return notification.getSortKey();
        }
    }
    
    static class Api23Impl
    {
        private Api23Impl() {
        }
        
        static Icon getIcon(final Notification$Action notification$Action) {
            return notification$Action.getIcon();
        }
    }
    
    static class Api24Impl
    {
        private Api24Impl() {
        }
        
        static boolean getAllowGeneratedReplies(final Notification$Action notification$Action) {
            return notification$Action.getAllowGeneratedReplies();
        }
    }
    
    static class Api26Impl
    {
        private Api26Impl() {
        }
        
        static int getBadgeIconType(final Notification notification) {
            return notification.getBadgeIconType();
        }
        
        static String getChannelId(final Notification notification) {
            return notification.getChannelId();
        }
        
        static int getGroupAlertBehavior(final Notification notification) {
            return notification.getGroupAlertBehavior();
        }
        
        static CharSequence getSettingsText(final Notification notification) {
            return notification.getSettingsText();
        }
        
        static String getShortcutId(final Notification notification) {
            return notification.getShortcutId();
        }
        
        static long getTimeoutAfter(final Notification notification) {
            return notification.getTimeoutAfter();
        }
    }
    
    static class Api28Impl
    {
        private Api28Impl() {
        }
        
        static int getSemanticAction(final Notification$Action notification$Action) {
            return notification$Action.getSemanticAction();
        }
    }
    
    static class Api29Impl
    {
        private Api29Impl() {
        }
        
        static boolean getAllowSystemGeneratedContextualActions(final Notification notification) {
            return notification.getAllowSystemGeneratedContextualActions();
        }
        
        static Notification$BubbleMetadata getBubbleMetadata(final Notification notification) {
            return notification.getBubbleMetadata();
        }
        
        static int getEditChoicesBeforeSending(final android.app.RemoteInput remoteInput) {
            return remoteInput.getEditChoicesBeforeSending();
        }
        
        static LocusId getLocusId(final Notification notification) {
            return notification.getLocusId();
        }
        
        static boolean isContextual(final Notification$Action notification$Action) {
            return notification$Action.isContextual();
        }
    }
    
    static class Api31Impl
    {
        private Api31Impl() {
        }
        
        static boolean isAuthenticationRequired(final Notification$Action notification$Action) {
            return notification$Action.isAuthenticationRequired();
        }
    }
    
    @Retention(RetentionPolicy.SOURCE)
    public @interface BadgeIconType {
    }
    
    public static class BigPictureStyle extends Style
    {
        private static final String TEMPLATE_CLASS_NAME = "androidx.core.app.NotificationCompat$BigPictureStyle";
        private IconCompat mBigLargeIcon;
        private boolean mBigLargeIconSet;
        private CharSequence mPictureContentDescription;
        private IconCompat mPictureIcon;
        private boolean mShowBigPictureWhenCollapsed;
        
        public BigPictureStyle() {
        }
        
        public BigPictureStyle(final NotificationCompat.Builder builder) {
            ((Style)this).setBuilder(builder);
        }
        
        private static IconCompat asIconCompat(final Parcelable parcelable) {
            if (parcelable != null) {
                if (Build$VERSION.SDK_INT >= 23 && parcelable instanceof Icon) {
                    return IconCompat.createFromIcon((Icon)parcelable);
                }
                if (parcelable instanceof Bitmap) {
                    return IconCompat.createWithBitmap((Bitmap)parcelable);
                }
            }
            return null;
        }
        
        public static IconCompat getPictureIcon(final Bundle bundle) {
            if (bundle == null) {
                return null;
            }
            final Parcelable parcelable = bundle.getParcelable("android.picture");
            if (parcelable != null) {
                return asIconCompat(parcelable);
            }
            return asIconCompat(bundle.getParcelable("android.pictureIcon"));
        }
        
        @Override
        public void apply(final NotificationBuilderWithBuilderAccessor notificationBuilderWithBuilderAccessor) {
            if (Build$VERSION.SDK_INT >= 16) {
                final Notification$BigPictureStyle setBigContentTitle = Api16Impl.setBigContentTitle(Api16Impl.createBigPictureStyle(notificationBuilderWithBuilderAccessor.getBuilder()), this.mBigContentTitle);
                final IconCompat mPictureIcon = this.mPictureIcon;
                Context context = null;
                Notification$BigPictureStyle bigPicture = setBigContentTitle;
                if (mPictureIcon != null) {
                    if (Build$VERSION.SDK_INT >= 31) {
                        Context context2;
                        if (notificationBuilderWithBuilderAccessor instanceof NotificationCompatBuilder) {
                            context2 = ((NotificationCompatBuilder)notificationBuilderWithBuilderAccessor).getContext();
                        }
                        else {
                            context2 = null;
                        }
                        Api31Impl.setBigPicture(setBigContentTitle, this.mPictureIcon.toIcon(context2));
                        bigPicture = setBigContentTitle;
                    }
                    else {
                        bigPicture = setBigContentTitle;
                        if (this.mPictureIcon.getType() == 1) {
                            bigPicture = Api16Impl.bigPicture(setBigContentTitle, this.mPictureIcon.getBitmap());
                        }
                    }
                }
                if (this.mBigLargeIconSet) {
                    if (this.mBigLargeIcon == null) {
                        Api16Impl.setBigLargeIcon(bigPicture, null);
                    }
                    else if (Build$VERSION.SDK_INT >= 23) {
                        if (notificationBuilderWithBuilderAccessor instanceof NotificationCompatBuilder) {
                            context = ((NotificationCompatBuilder)notificationBuilderWithBuilderAccessor).getContext();
                        }
                        Api23Impl.setBigLargeIcon(bigPicture, this.mBigLargeIcon.toIcon(context));
                    }
                    else if (this.mBigLargeIcon.getType() == 1) {
                        Api16Impl.setBigLargeIcon(bigPicture, this.mBigLargeIcon.getBitmap());
                    }
                    else {
                        Api16Impl.setBigLargeIcon(bigPicture, null);
                    }
                }
                if (this.mSummaryTextSet) {
                    Api16Impl.setSummaryText(bigPicture, this.mSummaryText);
                }
                if (Build$VERSION.SDK_INT >= 31) {
                    Api31Impl.showBigPictureWhenCollapsed(bigPicture, this.mShowBigPictureWhenCollapsed);
                    Api31Impl.setContentDescription(bigPicture, this.mPictureContentDescription);
                }
            }
        }
        
        public BigPictureStyle bigLargeIcon(final Bitmap bitmap) {
            IconCompat withBitmap;
            if (bitmap == null) {
                withBitmap = null;
            }
            else {
                withBitmap = IconCompat.createWithBitmap(bitmap);
            }
            this.mBigLargeIcon = withBitmap;
            this.mBigLargeIconSet = true;
            return this;
        }
        
        public BigPictureStyle bigLargeIcon(final Icon icon) {
            IconCompat fromIcon;
            if (icon == null) {
                fromIcon = null;
            }
            else {
                fromIcon = IconCompat.createFromIcon(icon);
            }
            this.mBigLargeIcon = fromIcon;
            this.mBigLargeIconSet = true;
            return this;
        }
        
        public BigPictureStyle bigPicture(final Bitmap bitmap) {
            IconCompat withBitmap;
            if (bitmap == null) {
                withBitmap = null;
            }
            else {
                withBitmap = IconCompat.createWithBitmap(bitmap);
            }
            this.mPictureIcon = withBitmap;
            return this;
        }
        
        public BigPictureStyle bigPicture(final Icon icon) {
            this.mPictureIcon = IconCompat.createFromIcon(icon);
            return this;
        }
        
        @Override
        protected void clearCompatExtraKeys(final Bundle bundle) {
            super.clearCompatExtraKeys(bundle);
            bundle.remove("android.largeIcon.big");
            bundle.remove("android.picture");
            bundle.remove("android.pictureIcon");
            bundle.remove("android.showBigPictureWhenCollapsed");
        }
        
        @Override
        protected String getClassName() {
            return "androidx.core.app.NotificationCompat$BigPictureStyle";
        }
        
        @Override
        protected void restoreFromCompatExtras(final Bundle bundle) {
            super.restoreFromCompatExtras(bundle);
            if (bundle.containsKey("android.largeIcon.big")) {
                this.mBigLargeIcon = asIconCompat(bundle.getParcelable("android.largeIcon.big"));
                this.mBigLargeIconSet = true;
            }
            this.mPictureIcon = getPictureIcon(bundle);
            this.mShowBigPictureWhenCollapsed = bundle.getBoolean("android.showBigPictureWhenCollapsed");
        }
        
        public BigPictureStyle setBigContentTitle(final CharSequence charSequence) {
            this.mBigContentTitle = NotificationCompat.Builder.limitCharSequenceLength(charSequence);
            return this;
        }
        
        public BigPictureStyle setContentDescription(final CharSequence mPictureContentDescription) {
            this.mPictureContentDescription = mPictureContentDescription;
            return this;
        }
        
        public BigPictureStyle setSummaryText(final CharSequence charSequence) {
            this.mSummaryText = NotificationCompat.Builder.limitCharSequenceLength(charSequence);
            this.mSummaryTextSet = true;
            return this;
        }
        
        public BigPictureStyle showBigPictureWhenCollapsed(final boolean mShowBigPictureWhenCollapsed) {
            this.mShowBigPictureWhenCollapsed = mShowBigPictureWhenCollapsed;
            return this;
        }
        
        private static class Api16Impl
        {
            static Notification$BigPictureStyle bigPicture(final Notification$BigPictureStyle notification$BigPictureStyle, final Bitmap bitmap) {
                return notification$BigPictureStyle.bigPicture(bitmap);
            }
            
            static Notification$BigPictureStyle createBigPictureStyle(final Notification$Builder notification$Builder) {
                return new Notification$BigPictureStyle(notification$Builder);
            }
            
            static Notification$BigPictureStyle setBigContentTitle(final Notification$BigPictureStyle notification$BigPictureStyle, final CharSequence bigContentTitle) {
                return notification$BigPictureStyle.setBigContentTitle(bigContentTitle);
            }
            
            static void setBigLargeIcon(final Notification$BigPictureStyle notification$BigPictureStyle, final Bitmap bitmap) {
                notification$BigPictureStyle.bigLargeIcon(bitmap);
            }
            
            static void setSummaryText(final Notification$BigPictureStyle notification$BigPictureStyle, final CharSequence summaryText) {
                notification$BigPictureStyle.setSummaryText(summaryText);
            }
        }
        
        private static class Api23Impl
        {
            static void setBigLargeIcon(final Notification$BigPictureStyle notification$BigPictureStyle, final Icon icon) {
                notification$BigPictureStyle.bigLargeIcon(icon);
            }
        }
        
        private static class Api31Impl
        {
            static void setBigPicture(final Notification$BigPictureStyle notification$BigPictureStyle, final Icon icon) {
                notification$BigPictureStyle.bigPicture(icon);
            }
            
            static void setContentDescription(final Notification$BigPictureStyle notification$BigPictureStyle, final CharSequence contentDescription) {
                notification$BigPictureStyle.setContentDescription(contentDescription);
            }
            
            static void showBigPictureWhenCollapsed(final Notification$BigPictureStyle notification$BigPictureStyle, final boolean b) {
                notification$BigPictureStyle.showBigPictureWhenCollapsed(b);
            }
        }
    }
    
    public abstract static class Style
    {
        CharSequence mBigContentTitle;
        protected NotificationCompat.Builder mBuilder;
        CharSequence mSummaryText;
        boolean mSummaryTextSet;
        
        public Style() {
            this.mSummaryTextSet = false;
        }
        
        private int calculateTopPadding() {
            final Resources resources = this.mBuilder.mContext.getResources();
            final int dimensionPixelSize = resources.getDimensionPixelSize(R.dimen.notification_top_pad);
            final int dimensionPixelSize2 = resources.getDimensionPixelSize(R.dimen.notification_top_pad_large_text);
            final float n = (constrain(resources.getConfiguration().fontScale, 1.0f, 1.3f) - 1.0f) / 0.29999995f;
            return Math.round((1.0f - n) * dimensionPixelSize + n * dimensionPixelSize2);
        }
        
        private static float constrain(final float n, float n2, final float n3) {
            if (n >= n2) {
                n2 = n;
                if (n > n3) {
                    n2 = n3;
                }
            }
            return n2;
        }
        
        static Style constructCompatStyleByName(final String s) {
            if (s != null) {
                s.hashCode();
                int n = -1;
                switch (s) {
                    case "androidx.core.app.NotificationCompat$MessagingStyle": {
                        n = 5;
                        break;
                    }
                    case "androidx.core.app.NotificationCompat$BigTextStyle": {
                        n = 4;
                        break;
                    }
                    case "androidx.core.app.NotificationCompat$InboxStyle": {
                        n = 3;
                        break;
                    }
                    case "androidx.core.app.NotificationCompat$CallStyle": {
                        n = 2;
                        break;
                    }
                    case "androidx.core.app.NotificationCompat$BigPictureStyle": {
                        n = 1;
                        break;
                    }
                    case "androidx.core.app.NotificationCompat$DecoratedCustomViewStyle": {
                        n = 0;
                        break;
                    }
                    default:
                        break;
                }
                switch (n) {
                    case 5: {
                        return (Style)new MessagingStyle();
                    }
                    case 4: {
                        return (Style)new BigTextStyle();
                    }
                    case 3: {
                        return (Style)new InboxStyle();
                    }
                    case 2: {
                        return (Style)new CallStyle();
                    }
                    case 1: {
                        return (Style)new BigPictureStyle();
                    }
                    case 0: {
                        return (Style)new DecoratedCustomViewStyle();
                    }
                }
            }
            return null;
        }
        
        private static Style constructCompatStyleByPlatformName(final String s) {
            if (s == null) {
                return null;
            }
            if (Build$VERSION.SDK_INT >= 16) {
                if (s.equals(Notification$BigPictureStyle.class.getName())) {
                    return (Style)new BigPictureStyle();
                }
                if (s.equals(Notification$BigTextStyle.class.getName())) {
                    return (Style)new BigTextStyle();
                }
                if (s.equals(Notification$InboxStyle.class.getName())) {
                    return (Style)new InboxStyle();
                }
                if (Build$VERSION.SDK_INT >= 24) {
                    if (s.equals(Notification$MessagingStyle.class.getName())) {
                        return (Style)new MessagingStyle();
                    }
                    if (s.equals(Notification$DecoratedCustomViewStyle.class.getName())) {
                        return (Style)new DecoratedCustomViewStyle();
                    }
                }
            }
            return null;
        }
        
        static Style constructCompatStyleForBundle(final Bundle bundle) {
            final Style constructCompatStyleByName = constructCompatStyleByName(bundle.getString("androidx.core.app.extra.COMPAT_TEMPLATE"));
            if (constructCompatStyleByName != null) {
                return constructCompatStyleByName;
            }
            if (bundle.containsKey("android.selfDisplayName") || bundle.containsKey("android.messagingStyleUser")) {
                return (Style)new MessagingStyle();
            }
            if (bundle.containsKey("android.picture") || bundle.containsKey("android.pictureIcon")) {
                return (Style)new BigPictureStyle();
            }
            if (bundle.containsKey("android.bigText")) {
                return (Style)new BigTextStyle();
            }
            if (bundle.containsKey("android.textLines")) {
                return (Style)new InboxStyle();
            }
            if (bundle.containsKey("android.callType")) {
                return (Style)new CallStyle();
            }
            return constructCompatStyleByPlatformName(bundle.getString("android.template"));
        }
        
        static Style constructStyleForExtras(final Bundle bundle) {
            final Style constructCompatStyleForBundle = constructCompatStyleForBundle(bundle);
            if (constructCompatStyleForBundle == null) {
                return null;
            }
            try {
                constructCompatStyleForBundle.restoreFromCompatExtras(bundle);
                return constructCompatStyleForBundle;
            }
            catch (final ClassCastException ex) {
                return null;
            }
        }
        
        private Bitmap createColoredBitmap(final int n, final int n2, final int n3) {
            return this.createColoredBitmap(IconCompat.createWithResource(this.mBuilder.mContext, n), n2, n3);
        }
        
        private Bitmap createColoredBitmap(final IconCompat iconCompat, final int n, final int n2) {
            final Drawable loadDrawable = iconCompat.loadDrawable(this.mBuilder.mContext);
            int intrinsicWidth;
            if (n2 == 0) {
                intrinsicWidth = loadDrawable.getIntrinsicWidth();
            }
            else {
                intrinsicWidth = n2;
            }
            int intrinsicHeight = n2;
            if (n2 == 0) {
                intrinsicHeight = loadDrawable.getIntrinsicHeight();
            }
            final Bitmap bitmap = Bitmap.createBitmap(intrinsicWidth, intrinsicHeight, Bitmap$Config.ARGB_8888);
            loadDrawable.setBounds(0, 0, intrinsicWidth, intrinsicHeight);
            if (n != 0) {
                loadDrawable.mutate().setColorFilter((ColorFilter)new PorterDuffColorFilter(n, PorterDuff$Mode.SRC_IN));
            }
            loadDrawable.draw(new Canvas(bitmap));
            return bitmap;
        }
        
        private Bitmap createIconWithBackground(int n, int n2, final int n3, final int n4) {
            final int notification_icon_background = R.drawable.notification_icon_background;
            int n5 = n4;
            if (n4 == 0) {
                n5 = 0;
            }
            final Bitmap coloredBitmap = this.createColoredBitmap(notification_icon_background, n5, n2);
            final Canvas canvas = new Canvas(coloredBitmap);
            final Drawable mutate = this.mBuilder.mContext.getResources().getDrawable(n).mutate();
            mutate.setFilterBitmap(true);
            n = (n2 - n3) / 2;
            n2 = n3 + n;
            mutate.setBounds(n, n, n2, n2);
            mutate.setColorFilter((ColorFilter)new PorterDuffColorFilter(-1, PorterDuff$Mode.SRC_ATOP));
            mutate.draw(canvas);
            return coloredBitmap;
        }
        
        public static Style extractStyleFromNotification(final Notification notification) {
            final Bundle extras = NotificationCompat.getExtras(notification);
            if (extras == null) {
                return null;
            }
            return constructStyleForExtras(extras);
        }
        
        private void hideNormalContent(final RemoteViews remoteViews) {
            remoteViews.setViewVisibility(R.id.title, 8);
            remoteViews.setViewVisibility(R.id.text2, 8);
            remoteViews.setViewVisibility(R.id.text, 8);
        }
        
        public void addCompatExtras(final Bundle bundle) {
            if (this.mSummaryTextSet) {
                bundle.putCharSequence("android.summaryText", this.mSummaryText);
            }
            final CharSequence mBigContentTitle = this.mBigContentTitle;
            if (mBigContentTitle != null) {
                bundle.putCharSequence("android.title.big", mBigContentTitle);
            }
            final String className = this.getClassName();
            if (className != null) {
                bundle.putString("androidx.core.app.extra.COMPAT_TEMPLATE", className);
            }
        }
        
        public void apply(final NotificationBuilderWithBuilderAccessor notificationBuilderWithBuilderAccessor) {
        }
        
        public RemoteViews applyStandardTemplate(final boolean b, int n, final boolean b2) {
            final Resources resources = this.mBuilder.mContext.getResources();
            final RemoteViews remoteViews = new RemoteViews(this.mBuilder.mContext.getPackageName(), n);
            n = this.mBuilder.getPriority();
            final int n2 = 1;
            final int n3 = 0;
            if (n < -1) {
                n = 1;
            }
            else {
                n = 0;
            }
            if (Build$VERSION.SDK_INT >= 16 && Build$VERSION.SDK_INT < 21) {
                if (n != 0) {
                    remoteViews.setInt(R.id.notification_background, "setBackgroundResource", R.drawable.notification_bg_low);
                    remoteViews.setInt(R.id.icon, "setBackgroundResource", R.drawable.notification_template_icon_low_bg);
                }
                else {
                    remoteViews.setInt(R.id.notification_background, "setBackgroundResource", R.drawable.notification_bg);
                    remoteViews.setInt(R.id.icon, "setBackgroundResource", R.drawable.notification_template_icon_bg);
                }
            }
            if (this.mBuilder.mLargeIcon != null) {
                if (Build$VERSION.SDK_INT >= 16) {
                    remoteViews.setViewVisibility(R.id.icon, 0);
                    remoteViews.setImageViewBitmap(R.id.icon, this.createColoredBitmap(this.mBuilder.mLargeIcon, 0));
                }
                else {
                    remoteViews.setViewVisibility(R.id.icon, 8);
                }
                if (b && this.mBuilder.mNotification.icon != 0) {
                    final int dimensionPixelSize = resources.getDimensionPixelSize(R.dimen.notification_right_icon_size);
                    n = resources.getDimensionPixelSize(R.dimen.notification_small_icon_background_padding);
                    if (Build$VERSION.SDK_INT >= 21) {
                        remoteViews.setImageViewBitmap(R.id.right_icon, this.createIconWithBackground(this.mBuilder.mNotification.icon, dimensionPixelSize, dimensionPixelSize - n * 2, this.mBuilder.getColor()));
                    }
                    else {
                        remoteViews.setImageViewBitmap(R.id.right_icon, this.createColoredBitmap(this.mBuilder.mNotification.icon, -1));
                    }
                    remoteViews.setViewVisibility(R.id.right_icon, 0);
                }
            }
            else if (b && this.mBuilder.mNotification.icon != 0) {
                remoteViews.setViewVisibility(R.id.icon, 0);
                if (Build$VERSION.SDK_INT >= 21) {
                    n = resources.getDimensionPixelSize(R.dimen.notification_large_icon_width);
                    remoteViews.setImageViewBitmap(R.id.icon, this.createIconWithBackground(this.mBuilder.mNotification.icon, n - resources.getDimensionPixelSize(R.dimen.notification_big_circle_margin), resources.getDimensionPixelSize(R.dimen.notification_small_icon_size_as_large), this.mBuilder.getColor()));
                }
                else {
                    remoteViews.setImageViewBitmap(R.id.icon, this.createColoredBitmap(this.mBuilder.mNotification.icon, -1));
                }
            }
            if (this.mBuilder.mContentTitle != null) {
                remoteViews.setTextViewText(R.id.title, this.mBuilder.mContentTitle);
            }
            boolean b3;
            if (this.mBuilder.mContentText != null) {
                remoteViews.setTextViewText(R.id.text, this.mBuilder.mContentText);
                b3 = true;
            }
            else {
                b3 = false;
            }
            if (Build$VERSION.SDK_INT < 21 && this.mBuilder.mLargeIcon != null) {
                n = 1;
            }
            else {
                n = 0;
            }
            Label_0671: {
                if (this.mBuilder.mContentInfo != null) {
                    remoteViews.setTextViewText(R.id.info, this.mBuilder.mContentInfo);
                    remoteViews.setViewVisibility(R.id.info, 0);
                }
                else {
                    if (this.mBuilder.mNumber <= 0) {
                        remoteViews.setViewVisibility(R.id.info, 8);
                        break Label_0671;
                    }
                    n = resources.getInteger(R.integer.status_bar_notification_info_maxnum);
                    if (this.mBuilder.mNumber > n) {
                        remoteViews.setTextViewText(R.id.info, (CharSequence)resources.getString(R.string.status_bar_notification_info_overflow));
                    }
                    else {
                        remoteViews.setTextViewText(R.id.info, (CharSequence)NumberFormat.getIntegerInstance().format(this.mBuilder.mNumber));
                    }
                    remoteViews.setViewVisibility(R.id.info, 0);
                }
                b3 = true;
                n = 1;
            }
            boolean b4 = false;
            Label_0757: {
                if (this.mBuilder.mSubText != null && Build$VERSION.SDK_INT >= 16) {
                    remoteViews.setTextViewText(R.id.text, this.mBuilder.mSubText);
                    if (this.mBuilder.mContentText != null) {
                        remoteViews.setTextViewText(R.id.text2, this.mBuilder.mContentText);
                        remoteViews.setViewVisibility(R.id.text2, 0);
                        b4 = true;
                        break Label_0757;
                    }
                    remoteViews.setViewVisibility(R.id.text2, 8);
                }
                b4 = false;
            }
            if (b4 && Build$VERSION.SDK_INT >= 16) {
                if (b2) {
                    Api16Impl.setTextViewTextSize(remoteViews, R.id.text, 0, (float)resources.getDimensionPixelSize(R.dimen.notification_subtext_size));
                }
                Api16Impl.setViewPadding(remoteViews, R.id.line1, 0, 0, 0, 0);
            }
            if (this.mBuilder.getWhenIfShowing() != 0L) {
                if (this.mBuilder.mUseChronometer && Build$VERSION.SDK_INT >= 16) {
                    remoteViews.setViewVisibility(R.id.chronometer, 0);
                    remoteViews.setLong(R.id.chronometer, "setBase", this.mBuilder.getWhenIfShowing() + (SystemClock.elapsedRealtime() - System.currentTimeMillis()));
                    remoteViews.setBoolean(R.id.chronometer, "setStarted", true);
                    n = n2;
                    if (this.mBuilder.mChronometerCountDown) {
                        n = n2;
                        if (Build$VERSION.SDK_INT >= 24) {
                            Api24Impl.setChronometerCountDown(remoteViews, R.id.chronometer, this.mBuilder.mChronometerCountDown);
                            n = n2;
                        }
                    }
                }
                else {
                    remoteViews.setViewVisibility(R.id.time, 0);
                    remoteViews.setLong(R.id.time, "setTime", this.mBuilder.getWhenIfShowing());
                    n = n2;
                }
            }
            final int right_side = R.id.right_side;
            if (n != 0) {
                n = 0;
            }
            else {
                n = 8;
            }
            remoteViews.setViewVisibility(right_side, n);
            final int line3 = R.id.line3;
            if (b3) {
                n = n3;
            }
            else {
                n = 8;
            }
            remoteViews.setViewVisibility(line3, n);
            return remoteViews;
        }
        
        public Notification build() {
            final NotificationCompat.Builder mBuilder = this.mBuilder;
            Notification build;
            if (mBuilder != null) {
                build = mBuilder.build();
            }
            else {
                build = null;
            }
            return build;
        }
        
        public void buildIntoRemoteViews(final RemoteViews remoteViews, final RemoteViews remoteViews2) {
            this.hideNormalContent(remoteViews);
            remoteViews.removeAllViews(R.id.notification_main_column);
            remoteViews.addView(R.id.notification_main_column, remoteViews2.clone());
            remoteViews.setViewVisibility(R.id.notification_main_column, 0);
            if (Build$VERSION.SDK_INT >= 21) {
                Api16Impl.setViewPadding(remoteViews, R.id.notification_main_column_container, 0, this.calculateTopPadding(), 0, 0);
            }
        }
        
        protected void clearCompatExtraKeys(final Bundle bundle) {
            bundle.remove("android.summaryText");
            bundle.remove("android.title.big");
            bundle.remove("androidx.core.app.extra.COMPAT_TEMPLATE");
        }
        
        public Bitmap createColoredBitmap(final int n, final int n2) {
            return this.createColoredBitmap(n, n2, 0);
        }
        
        Bitmap createColoredBitmap(final IconCompat iconCompat, final int n) {
            return this.createColoredBitmap(iconCompat, n, 0);
        }
        
        public boolean displayCustomViewInline() {
            return false;
        }
        
        protected String getClassName() {
            return null;
        }
        
        public RemoteViews makeBigContentView(final NotificationBuilderWithBuilderAccessor notificationBuilderWithBuilderAccessor) {
            return null;
        }
        
        public RemoteViews makeContentView(final NotificationBuilderWithBuilderAccessor notificationBuilderWithBuilderAccessor) {
            return null;
        }
        
        public RemoteViews makeHeadsUpContentView(final NotificationBuilderWithBuilderAccessor notificationBuilderWithBuilderAccessor) {
            return null;
        }
        
        protected void restoreFromCompatExtras(final Bundle bundle) {
            if (bundle.containsKey("android.summaryText")) {
                this.mSummaryText = bundle.getCharSequence("android.summaryText");
                this.mSummaryTextSet = true;
            }
            this.mBigContentTitle = bundle.getCharSequence("android.title.big");
        }
        
        public void setBuilder(final NotificationCompat.Builder mBuilder) {
            if (this.mBuilder != mBuilder && (this.mBuilder = mBuilder) != null) {
                mBuilder.setStyle(this);
            }
        }
        
        static class Api16Impl
        {
            private Api16Impl() {
            }
            
            static void setTextViewTextSize(final RemoteViews remoteViews, final int n, final int n2, final float n3) {
                remoteViews.setTextViewTextSize(n, n2, n3);
            }
            
            static void setViewPadding(final RemoteViews remoteViews, final int n, final int n2, final int n3, final int n4, final int n5) {
                remoteViews.setViewPadding(n, n2, n3, n4, n5);
            }
        }
        
        static class Api24Impl
        {
            private Api24Impl() {
            }
            
            static void setChronometerCountDown(final RemoteViews remoteViews, final int n, final boolean b) {
                remoteViews.setChronometerCountDown(n, b);
            }
        }
    }
    
    public static class BigTextStyle extends Style
    {
        private static final String TEMPLATE_CLASS_NAME = "androidx.core.app.NotificationCompat$BigTextStyle";
        private CharSequence mBigText;
        
        public BigTextStyle() {
        }
        
        public BigTextStyle(final NotificationCompat.Builder builder) {
            ((Style)this).setBuilder(builder);
        }
        
        @Override
        public void addCompatExtras(final Bundle bundle) {
            super.addCompatExtras(bundle);
            if (Build$VERSION.SDK_INT < 21) {
                bundle.putCharSequence("android.bigText", this.mBigText);
            }
        }
        
        @Override
        public void apply(final NotificationBuilderWithBuilderAccessor notificationBuilderWithBuilderAccessor) {
            if (Build$VERSION.SDK_INT >= 16) {
                final Notification$BigTextStyle bigText = Api16Impl.bigText(Api16Impl.setBigContentTitle(Api16Impl.createBigTextStyle(notificationBuilderWithBuilderAccessor.getBuilder()), this.mBigContentTitle), this.mBigText);
                if (this.mSummaryTextSet) {
                    Api16Impl.setSummaryText(bigText, this.mSummaryText);
                }
            }
        }
        
        public BigTextStyle bigText(final CharSequence charSequence) {
            this.mBigText = NotificationCompat.Builder.limitCharSequenceLength(charSequence);
            return this;
        }
        
        @Override
        protected void clearCompatExtraKeys(final Bundle bundle) {
            super.clearCompatExtraKeys(bundle);
            bundle.remove("android.bigText");
        }
        
        @Override
        protected String getClassName() {
            return "androidx.core.app.NotificationCompat$BigTextStyle";
        }
        
        @Override
        protected void restoreFromCompatExtras(final Bundle bundle) {
            super.restoreFromCompatExtras(bundle);
            this.mBigText = bundle.getCharSequence("android.bigText");
        }
        
        public BigTextStyle setBigContentTitle(final CharSequence charSequence) {
            this.mBigContentTitle = NotificationCompat.Builder.limitCharSequenceLength(charSequence);
            return this;
        }
        
        public BigTextStyle setSummaryText(final CharSequence charSequence) {
            this.mSummaryText = NotificationCompat.Builder.limitCharSequenceLength(charSequence);
            this.mSummaryTextSet = true;
            return this;
        }
        
        static class Api16Impl
        {
            private Api16Impl() {
            }
            
            static Notification$BigTextStyle bigText(final Notification$BigTextStyle notification$BigTextStyle, final CharSequence charSequence) {
                return notification$BigTextStyle.bigText(charSequence);
            }
            
            static Notification$BigTextStyle createBigTextStyle(final Notification$Builder notification$Builder) {
                return new Notification$BigTextStyle(notification$Builder);
            }
            
            static Notification$BigTextStyle setBigContentTitle(final Notification$BigTextStyle notification$BigTextStyle, final CharSequence bigContentTitle) {
                return notification$BigTextStyle.setBigContentTitle(bigContentTitle);
            }
            
            static Notification$BigTextStyle setSummaryText(final Notification$BigTextStyle notification$BigTextStyle, final CharSequence summaryText) {
                return notification$BigTextStyle.setSummaryText(summaryText);
            }
        }
    }
    
    public static final class BubbleMetadata
    {
        private static final int FLAG_AUTO_EXPAND_BUBBLE = 1;
        private static final int FLAG_SUPPRESS_NOTIFICATION = 2;
        private PendingIntent mDeleteIntent;
        private int mDesiredHeight;
        private int mDesiredHeightResId;
        private int mFlags;
        private IconCompat mIcon;
        private PendingIntent mPendingIntent;
        private String mShortcutId;
        
        private BubbleMetadata(final PendingIntent mPendingIntent, final PendingIntent mDeleteIntent, final IconCompat mIcon, final int mDesiredHeight, final int mDesiredHeightResId, final int mFlags, final String mShortcutId) {
            this.mPendingIntent = mPendingIntent;
            this.mIcon = mIcon;
            this.mDesiredHeight = mDesiredHeight;
            this.mDesiredHeightResId = mDesiredHeightResId;
            this.mDeleteIntent = mDeleteIntent;
            this.mFlags = mFlags;
            this.mShortcutId = mShortcutId;
        }
        
        public static BubbleMetadata fromPlatform(final Notification$BubbleMetadata notification$BubbleMetadata) {
            if (notification$BubbleMetadata == null) {
                return null;
            }
            if (Build$VERSION.SDK_INT >= 30) {
                return Api30Impl.fromPlatform(notification$BubbleMetadata);
            }
            if (Build$VERSION.SDK_INT == 29) {
                return Api29Impl.fromPlatform(notification$BubbleMetadata);
            }
            return null;
        }
        
        public static Notification$BubbleMetadata toPlatform(final BubbleMetadata bubbleMetadata) {
            if (bubbleMetadata == null) {
                return null;
            }
            if (Build$VERSION.SDK_INT >= 30) {
                return Api30Impl.toPlatform(bubbleMetadata);
            }
            if (Build$VERSION.SDK_INT == 29) {
                return Api29Impl.toPlatform(bubbleMetadata);
            }
            return null;
        }
        
        public boolean getAutoExpandBubble() {
            final int mFlags = this.mFlags;
            boolean b = true;
            if ((mFlags & 0x1) == 0x0) {
                b = false;
            }
            return b;
        }
        
        public PendingIntent getDeleteIntent() {
            return this.mDeleteIntent;
        }
        
        public int getDesiredHeight() {
            return this.mDesiredHeight;
        }
        
        public int getDesiredHeightResId() {
            return this.mDesiredHeightResId;
        }
        
        public IconCompat getIcon() {
            return this.mIcon;
        }
        
        public PendingIntent getIntent() {
            return this.mPendingIntent;
        }
        
        public String getShortcutId() {
            return this.mShortcutId;
        }
        
        public boolean isNotificationSuppressed() {
            return (this.mFlags & 0x2) != 0x0;
        }
        
        public void setFlags(final int mFlags) {
            this.mFlags = mFlags;
        }
        
        private static class Api29Impl
        {
            static BubbleMetadata fromPlatform(final Notification$BubbleMetadata notification$BubbleMetadata) {
                if (notification$BubbleMetadata == null) {
                    return null;
                }
                if (notification$BubbleMetadata.getIntent() == null) {
                    return null;
                }
                final Builder setSuppressNotification = new Builder(notification$BubbleMetadata.getIntent(), IconCompat.createFromIcon(notification$BubbleMetadata.getIcon())).setAutoExpandBubble(notification$BubbleMetadata.getAutoExpandBubble()).setDeleteIntent(notification$BubbleMetadata.getDeleteIntent()).setSuppressNotification(notification$BubbleMetadata.isNotificationSuppressed());
                if (notification$BubbleMetadata.getDesiredHeight() != 0) {
                    setSuppressNotification.setDesiredHeight(notification$BubbleMetadata.getDesiredHeight());
                }
                if (notification$BubbleMetadata.getDesiredHeightResId() != 0) {
                    setSuppressNotification.setDesiredHeightResId(notification$BubbleMetadata.getDesiredHeightResId());
                }
                return setSuppressNotification.build();
            }
            
            static Notification$BubbleMetadata toPlatform(final BubbleMetadata bubbleMetadata) {
                if (bubbleMetadata == null) {
                    return null;
                }
                if (bubbleMetadata.getIntent() == null) {
                    return null;
                }
                final Notification$BubbleMetadata$Builder setSuppressNotification = new Notification$BubbleMetadata$Builder().setIcon(bubbleMetadata.getIcon().toIcon()).setIntent(bubbleMetadata.getIntent()).setDeleteIntent(bubbleMetadata.getDeleteIntent()).setAutoExpandBubble(bubbleMetadata.getAutoExpandBubble()).setSuppressNotification(bubbleMetadata.isNotificationSuppressed());
                if (bubbleMetadata.getDesiredHeight() != 0) {
                    setSuppressNotification.setDesiredHeight(bubbleMetadata.getDesiredHeight());
                }
                if (bubbleMetadata.getDesiredHeightResId() != 0) {
                    setSuppressNotification.setDesiredHeightResId(bubbleMetadata.getDesiredHeightResId());
                }
                return setSuppressNotification.build();
            }
        }
        
        private static class Api30Impl
        {
            static BubbleMetadata fromPlatform(final Notification$BubbleMetadata notification$BubbleMetadata) {
                if (notification$BubbleMetadata == null) {
                    return null;
                }
                Builder builder;
                if (notification$BubbleMetadata.getShortcutId() != null) {
                    builder = new Builder(notification$BubbleMetadata.getShortcutId());
                }
                else {
                    builder = new Builder(notification$BubbleMetadata.getIntent(), IconCompat.createFromIcon(notification$BubbleMetadata.getIcon()));
                }
                builder.setAutoExpandBubble(notification$BubbleMetadata.getAutoExpandBubble()).setDeleteIntent(notification$BubbleMetadata.getDeleteIntent()).setSuppressNotification(notification$BubbleMetadata.isNotificationSuppressed());
                if (notification$BubbleMetadata.getDesiredHeight() != 0) {
                    builder.setDesiredHeight(notification$BubbleMetadata.getDesiredHeight());
                }
                if (notification$BubbleMetadata.getDesiredHeightResId() != 0) {
                    builder.setDesiredHeightResId(notification$BubbleMetadata.getDesiredHeightResId());
                }
                return builder.build();
            }
            
            static Notification$BubbleMetadata toPlatform(final BubbleMetadata bubbleMetadata) {
                if (bubbleMetadata == null) {
                    return null;
                }
                Notification$BubbleMetadata$Builder notification$BubbleMetadata$Builder;
                if (bubbleMetadata.getShortcutId() != null) {
                    notification$BubbleMetadata$Builder = new Notification$BubbleMetadata$Builder(bubbleMetadata.getShortcutId());
                }
                else {
                    notification$BubbleMetadata$Builder = new Notification$BubbleMetadata$Builder(bubbleMetadata.getIntent(), bubbleMetadata.getIcon().toIcon());
                }
                notification$BubbleMetadata$Builder.setDeleteIntent(bubbleMetadata.getDeleteIntent()).setAutoExpandBubble(bubbleMetadata.getAutoExpandBubble()).setSuppressNotification(bubbleMetadata.isNotificationSuppressed());
                if (bubbleMetadata.getDesiredHeight() != 0) {
                    notification$BubbleMetadata$Builder.setDesiredHeight(bubbleMetadata.getDesiredHeight());
                }
                if (bubbleMetadata.getDesiredHeightResId() != 0) {
                    notification$BubbleMetadata$Builder.setDesiredHeightResId(bubbleMetadata.getDesiredHeightResId());
                }
                return notification$BubbleMetadata$Builder.build();
            }
        }
        
        public static final class Builder
        {
            private PendingIntent mDeleteIntent;
            private int mDesiredHeight;
            private int mDesiredHeightResId;
            private int mFlags;
            private IconCompat mIcon;
            private PendingIntent mPendingIntent;
            private String mShortcutId;
            
            @Deprecated
            public Builder() {
            }
            
            public Builder(final PendingIntent mPendingIntent, final IconCompat mIcon) {
                if (mPendingIntent == null) {
                    throw new NullPointerException("Bubble requires non-null pending intent");
                }
                if (mIcon != null) {
                    this.mPendingIntent = mPendingIntent;
                    this.mIcon = mIcon;
                    return;
                }
                throw new NullPointerException("Bubbles require non-null icon");
            }
            
            public Builder(final String mShortcutId) {
                if (!TextUtils.isEmpty((CharSequence)mShortcutId)) {
                    this.mShortcutId = mShortcutId;
                    return;
                }
                throw new NullPointerException("Bubble requires a non-null shortcut id");
            }
            
            private Builder setFlag(final int n, final boolean b) {
                if (b) {
                    this.mFlags |= n;
                }
                else {
                    this.mFlags &= ~n;
                }
                return this;
            }
            
            public BubbleMetadata build() {
                final String mShortcutId = this.mShortcutId;
                if (mShortcutId == null && this.mPendingIntent == null) {
                    throw new NullPointerException("Must supply pending intent or shortcut to bubble");
                }
                if (mShortcutId == null && this.mIcon == null) {
                    throw new NullPointerException("Must supply an icon or shortcut for the bubble");
                }
                final BubbleMetadata bubbleMetadata = new BubbleMetadata(this.mPendingIntent, this.mDeleteIntent, this.mIcon, this.mDesiredHeight, this.mDesiredHeightResId, this.mFlags, mShortcutId);
                bubbleMetadata.setFlags(this.mFlags);
                return bubbleMetadata;
            }
            
            public Builder setAutoExpandBubble(final boolean b) {
                this.setFlag(1, b);
                return this;
            }
            
            public Builder setDeleteIntent(final PendingIntent mDeleteIntent) {
                this.mDeleteIntent = mDeleteIntent;
                return this;
            }
            
            public Builder setDesiredHeight(final int a) {
                this.mDesiredHeight = Math.max(a, 0);
                this.mDesiredHeightResId = 0;
                return this;
            }
            
            public Builder setDesiredHeightResId(final int mDesiredHeightResId) {
                this.mDesiredHeightResId = mDesiredHeightResId;
                this.mDesiredHeight = 0;
                return this;
            }
            
            public Builder setIcon(final IconCompat mIcon) {
                if (this.mShortcutId != null) {
                    throw new IllegalStateException("Created as a shortcut bubble, cannot set an Icon. Consider using BubbleMetadata.Builder(PendingIntent,Icon) instead.");
                }
                if (mIcon != null) {
                    this.mIcon = mIcon;
                    return this;
                }
                throw new NullPointerException("Bubbles require non-null icon");
            }
            
            public Builder setIntent(final PendingIntent mPendingIntent) {
                if (this.mShortcutId != null) {
                    throw new IllegalStateException("Created as a shortcut bubble, cannot set a PendingIntent. Consider using BubbleMetadata.Builder(PendingIntent,Icon) instead.");
                }
                if (mPendingIntent != null) {
                    this.mPendingIntent = mPendingIntent;
                    return this;
                }
                throw new NullPointerException("Bubble requires non-null pending intent");
            }
            
            public Builder setSuppressNotification(final boolean b) {
                this.setFlag(2, b);
                return this;
            }
        }
    }
    
    public static class Builder
    {
        private static final int MAX_CHARSEQUENCE_LENGTH = 5120;
        public ArrayList<Action> mActions;
        boolean mAllowSystemGeneratedContextualActions;
        int mBadgeIcon;
        RemoteViews mBigContentView;
        BubbleMetadata mBubbleMetadata;
        String mCategory;
        String mChannelId;
        boolean mChronometerCountDown;
        int mColor;
        boolean mColorized;
        boolean mColorizedSet;
        CharSequence mContentInfo;
        PendingIntent mContentIntent;
        CharSequence mContentText;
        CharSequence mContentTitle;
        RemoteViews mContentView;
        public Context mContext;
        Bundle mExtras;
        int mFgsDeferBehavior;
        PendingIntent mFullScreenIntent;
        int mGroupAlertBehavior;
        String mGroupKey;
        boolean mGroupSummary;
        RemoteViews mHeadsUpContentView;
        ArrayList<Action> mInvisibleActions;
        IconCompat mLargeIcon;
        boolean mLocalOnly;
        LocusIdCompat mLocusId;
        Notification mNotification;
        int mNumber;
        @Deprecated
        public ArrayList<String> mPeople;
        public ArrayList<Person> mPersonList;
        int mPriority;
        int mProgress;
        boolean mProgressIndeterminate;
        int mProgressMax;
        Notification mPublicVersion;
        CharSequence[] mRemoteInputHistory;
        CharSequence mSettingsText;
        String mShortcutId;
        boolean mShowWhen;
        boolean mSilent;
        Object mSmallIcon;
        String mSortKey;
        Style mStyle;
        CharSequence mSubText;
        RemoteViews mTickerView;
        long mTimeout;
        boolean mUseChronometer;
        int mVisibility;
        
        @Deprecated
        public Builder(final Context context) {
            final String s = null;
            this(context, (String)null);
        }
        
        public Builder(final Context context, final Notification notification) {
            this(context, NotificationCompat.getChannelId(notification));
            final Bundle extras = notification.extras;
            final Style styleFromNotification = Style.extractStyleFromNotification(notification);
            this.setContentTitle(NotificationCompat.getContentTitle(notification)).setContentText(NotificationCompat.getContentText(notification)).setContentInfo(NotificationCompat.getContentInfo(notification)).setSubText(NotificationCompat.getSubText(notification)).setSettingsText(NotificationCompat.getSettingsText(notification)).setStyle(styleFromNotification).setContentIntent(notification.contentIntent).setGroup(NotificationCompat.getGroup(notification)).setGroupSummary(NotificationCompat.isGroupSummary(notification)).setLocusId(NotificationCompat.getLocusId(notification)).setWhen(notification.when).setShowWhen(NotificationCompat.getShowWhen(notification)).setUsesChronometer(NotificationCompat.getUsesChronometer(notification)).setAutoCancel(NotificationCompat.getAutoCancel(notification)).setOnlyAlertOnce(NotificationCompat.getOnlyAlertOnce(notification)).setOngoing(NotificationCompat.getOngoing(notification)).setLocalOnly(NotificationCompat.getLocalOnly(notification)).setLargeIcon(notification.largeIcon).setBadgeIconType(NotificationCompat.getBadgeIconType(notification)).setCategory(NotificationCompat.getCategory(notification)).setBubbleMetadata(NotificationCompat.getBubbleMetadata(notification)).setNumber(notification.number).setTicker(notification.tickerText).setContentIntent(notification.contentIntent).setDeleteIntent(notification.deleteIntent).setFullScreenIntent(notification.fullScreenIntent, NotificationCompat.getHighPriority(notification)).setSound(notification.sound, notification.audioStreamType).setVibrate(notification.vibrate).setLights(notification.ledARGB, notification.ledOnMS, notification.ledOffMS).setDefaults(notification.defaults).setPriority(notification.priority).setColor(NotificationCompat.getColor(notification)).setVisibility(NotificationCompat.getVisibility(notification)).setPublicVersion(NotificationCompat.getPublicVersion(notification)).setSortKey(NotificationCompat.getSortKey(notification)).setTimeoutAfter(NotificationCompat.getTimeoutAfter(notification)).setShortcutId(NotificationCompat.getShortcutId(notification)).setProgress(extras.getInt("android.progressMax"), extras.getInt("android.progress"), extras.getBoolean("android.progressIndeterminate")).setAllowSystemGeneratedContextualActions(NotificationCompat.getAllowSystemGeneratedContextualActions(notification)).setSmallIcon(notification.icon, notification.iconLevel).addExtras(getExtrasWithoutDuplicateData(notification, styleFromNotification));
            if (Build$VERSION.SDK_INT >= 23) {
                this.mSmallIcon = Api23Impl.getSmallIcon(notification);
                final Icon largeIcon = Api23Impl.getLargeIcon(notification);
                if (largeIcon != null) {
                    this.mLargeIcon = IconCompat.createFromIcon(largeIcon);
                }
            }
            final Notification$Action[] actions = notification.actions;
            final int n = 0;
            if (actions != null && notification.actions.length != 0) {
                final Notification$Action[] actions2 = notification.actions;
                for (int length = actions2.length, i = 0; i < length; ++i) {
                    this.addAction(Action.Builder.fromAndroidAction(actions2[i]).build());
                }
            }
            if (Build$VERSION.SDK_INT >= 21) {
                final List<Action> invisibleActions = NotificationCompat.getInvisibleActions(notification);
                if (!invisibleActions.isEmpty()) {
                    final Iterator iterator = invisibleActions.iterator();
                    while (iterator.hasNext()) {
                        this.addInvisibleAction((Action)iterator.next());
                    }
                }
            }
            final String[] stringArray = notification.extras.getStringArray("android.people");
            if (stringArray != null && stringArray.length != 0) {
                for (int length2 = stringArray.length, j = n; j < length2; ++j) {
                    this.addPerson(stringArray[j]);
                }
            }
            if (Build$VERSION.SDK_INT >= 28) {
                final ArrayList parcelableArrayList = notification.extras.getParcelableArrayList("android.people.list");
                if (parcelableArrayList != null && !parcelableArrayList.isEmpty()) {
                    final Iterator iterator2 = parcelableArrayList.iterator();
                    while (iterator2.hasNext()) {
                        this.addPerson(Person.fromAndroidPerson((android.app.Person)iterator2.next()));
                    }
                }
            }
            if (Build$VERSION.SDK_INT >= 24 && extras.containsKey("android.chronometerCountDown")) {
                this.setChronometerCountDown(extras.getBoolean("android.chronometerCountDown"));
            }
            if (Build$VERSION.SDK_INT >= 26 && extras.containsKey("android.colorized")) {
                this.setColorized(extras.getBoolean("android.colorized"));
            }
        }
        
        public Builder(final Context mContext, final String mChannelId) {
            this.mActions = new ArrayList<Action>();
            this.mPersonList = new ArrayList<Person>();
            this.mInvisibleActions = new ArrayList<Action>();
            this.mShowWhen = true;
            this.mLocalOnly = false;
            this.mColor = 0;
            this.mVisibility = 0;
            this.mBadgeIcon = 0;
            this.mGroupAlertBehavior = 0;
            this.mFgsDeferBehavior = 0;
            final Notification mNotification = new Notification();
            this.mNotification = mNotification;
            this.mContext = mContext;
            this.mChannelId = mChannelId;
            mNotification.when = System.currentTimeMillis();
            this.mNotification.audioStreamType = -1;
            this.mPriority = 0;
            this.mPeople = new ArrayList<String>();
            this.mAllowSystemGeneratedContextualActions = true;
        }
        
        private static Bundle getExtrasWithoutDuplicateData(final Notification notification, final Style style) {
            if (notification.extras == null) {
                return null;
            }
            final Bundle bundle = new Bundle(notification.extras);
            bundle.remove("android.title");
            bundle.remove("android.text");
            bundle.remove("android.infoText");
            bundle.remove("android.subText");
            bundle.remove("android.intent.extra.CHANNEL_ID");
            bundle.remove("android.intent.extra.CHANNEL_GROUP_ID");
            bundle.remove("android.showWhen");
            bundle.remove("android.progress");
            bundle.remove("android.progressMax");
            bundle.remove("android.progressIndeterminate");
            bundle.remove("android.chronometerCountDown");
            bundle.remove("android.colorized");
            bundle.remove("android.people.list");
            bundle.remove("android.people");
            bundle.remove("android.support.sortKey");
            bundle.remove("android.support.groupKey");
            bundle.remove("android.support.isGroupSummary");
            bundle.remove("android.support.localOnly");
            bundle.remove("android.support.actionExtras");
            final Bundle bundle2 = bundle.getBundle("android.car.EXTENSIONS");
            if (bundle2 != null) {
                final Bundle bundle3 = new Bundle(bundle2);
                bundle3.remove("invisible_actions");
                bundle.putBundle("android.car.EXTENSIONS", bundle3);
            }
            if (style != null) {
                style.clearCompatExtraKeys(bundle);
            }
            return bundle;
        }
        
        protected static CharSequence limitCharSequenceLength(final CharSequence charSequence) {
            if (charSequence == null) {
                return charSequence;
            }
            CharSequence subSequence = charSequence;
            if (charSequence.length() > 5120) {
                subSequence = charSequence.subSequence(0, 5120);
            }
            return subSequence;
        }
        
        private void setFlag(final int n, final boolean b) {
            if (b) {
                final Notification mNotification = this.mNotification;
                mNotification.flags |= n;
            }
            else {
                final Notification mNotification2 = this.mNotification;
                mNotification2.flags &= ~n;
            }
        }
        
        private boolean useExistingRemoteView() {
            final Style mStyle = this.mStyle;
            return mStyle == null || !mStyle.displayCustomViewInline();
        }
        
        public Builder addAction(final int n, final CharSequence charSequence, final PendingIntent pendingIntent) {
            this.mActions.add(new Action(n, charSequence, pendingIntent));
            return this;
        }
        
        public Builder addAction(final Action e) {
            if (e != null) {
                this.mActions.add(e);
            }
            return this;
        }
        
        public Builder addExtras(final Bundle bundle) {
            if (bundle != null) {
                final Bundle mExtras = this.mExtras;
                if (mExtras == null) {
                    this.mExtras = new Bundle(bundle);
                }
                else {
                    mExtras.putAll(bundle);
                }
            }
            return this;
        }
        
        public Builder addInvisibleAction(final int n, final CharSequence charSequence, final PendingIntent pendingIntent) {
            this.mInvisibleActions.add(new Action(n, charSequence, pendingIntent));
            return this;
        }
        
        public Builder addInvisibleAction(final Action e) {
            if (e != null) {
                this.mInvisibleActions.add(e);
            }
            return this;
        }
        
        public Builder addPerson(final Person e) {
            if (e != null) {
                this.mPersonList.add(e);
            }
            return this;
        }
        
        @Deprecated
        public Builder addPerson(final String e) {
            if (e != null && !e.isEmpty()) {
                this.mPeople.add(e);
            }
            return this;
        }
        
        public Notification build() {
            return new NotificationCompatBuilder(this).build();
        }
        
        public Builder clearActions() {
            this.mActions.clear();
            return this;
        }
        
        public Builder clearInvisibleActions() {
            this.mInvisibleActions.clear();
            final Bundle bundle = this.mExtras.getBundle("android.car.EXTENSIONS");
            if (bundle != null) {
                final Bundle bundle2 = new Bundle(bundle);
                bundle2.remove("invisible_actions");
                this.mExtras.putBundle("android.car.EXTENSIONS", bundle2);
            }
            return this;
        }
        
        public Builder clearPeople() {
            this.mPersonList.clear();
            this.mPeople.clear();
            return this;
        }
        
        public RemoteViews createBigContentView() {
            if (Build$VERSION.SDK_INT < 16) {
                return null;
            }
            if (this.mBigContentView != null && this.useExistingRemoteView()) {
                return this.mBigContentView;
            }
            final NotificationCompatBuilder notificationCompatBuilder = new NotificationCompatBuilder(this);
            final Style mStyle = this.mStyle;
            if (mStyle != null) {
                final RemoteViews bigContentView = mStyle.makeBigContentView(notificationCompatBuilder);
                if (bigContentView != null) {
                    return bigContentView;
                }
            }
            final Notification build = notificationCompatBuilder.build();
            if (Build$VERSION.SDK_INT >= 24) {
                return Api24Impl.createBigContentView(Api24Impl.recoverBuilder(this.mContext, build));
            }
            return build.bigContentView;
        }
        
        public RemoteViews createContentView() {
            if (this.mContentView != null && this.useExistingRemoteView()) {
                return this.mContentView;
            }
            final NotificationCompatBuilder notificationCompatBuilder = new NotificationCompatBuilder(this);
            final Style mStyle = this.mStyle;
            if (mStyle != null) {
                final RemoteViews contentView = mStyle.makeContentView(notificationCompatBuilder);
                if (contentView != null) {
                    return contentView;
                }
            }
            final Notification build = notificationCompatBuilder.build();
            if (Build$VERSION.SDK_INT >= 24) {
                return Api24Impl.createContentView(Api24Impl.recoverBuilder(this.mContext, build));
            }
            return build.contentView;
        }
        
        public RemoteViews createHeadsUpContentView() {
            if (Build$VERSION.SDK_INT < 21) {
                return null;
            }
            if (this.mHeadsUpContentView != null && this.useExistingRemoteView()) {
                return this.mHeadsUpContentView;
            }
            final NotificationCompatBuilder notificationCompatBuilder = new NotificationCompatBuilder(this);
            final Style mStyle = this.mStyle;
            if (mStyle != null) {
                final RemoteViews headsUpContentView = mStyle.makeHeadsUpContentView(notificationCompatBuilder);
                if (headsUpContentView != null) {
                    return headsUpContentView;
                }
            }
            final Notification build = notificationCompatBuilder.build();
            if (Build$VERSION.SDK_INT >= 24) {
                return Api24Impl.createHeadsUpContentView(Api24Impl.recoverBuilder(this.mContext, build));
            }
            return build.headsUpContentView;
        }
        
        public Builder extend(final NotificationCompat.Extender extender) {
            extender.extend(this);
            return this;
        }
        
        public RemoteViews getBigContentView() {
            return this.mBigContentView;
        }
        
        public BubbleMetadata getBubbleMetadata() {
            return this.mBubbleMetadata;
        }
        
        public int getColor() {
            return this.mColor;
        }
        
        public RemoteViews getContentView() {
            return this.mContentView;
        }
        
        public Bundle getExtras() {
            if (this.mExtras == null) {
                this.mExtras = new Bundle();
            }
            return this.mExtras;
        }
        
        public int getForegroundServiceBehavior() {
            return this.mFgsDeferBehavior;
        }
        
        public RemoteViews getHeadsUpContentView() {
            return this.mHeadsUpContentView;
        }
        
        @Deprecated
        public Notification getNotification() {
            return this.build();
        }
        
        public int getPriority() {
            return this.mPriority;
        }
        
        public long getWhenIfShowing() {
            long when;
            if (this.mShowWhen) {
                when = this.mNotification.when;
            }
            else {
                when = 0L;
            }
            return when;
        }
        
        public Builder setAllowSystemGeneratedContextualActions(final boolean mAllowSystemGeneratedContextualActions) {
            this.mAllowSystemGeneratedContextualActions = mAllowSystemGeneratedContextualActions;
            return this;
        }
        
        public Builder setAutoCancel(final boolean b) {
            this.setFlag(16, b);
            return this;
        }
        
        public Builder setBadgeIconType(final int mBadgeIcon) {
            this.mBadgeIcon = mBadgeIcon;
            return this;
        }
        
        public Builder setBubbleMetadata(final BubbleMetadata mBubbleMetadata) {
            this.mBubbleMetadata = mBubbleMetadata;
            return this;
        }
        
        public Builder setCategory(final String mCategory) {
            this.mCategory = mCategory;
            return this;
        }
        
        public Builder setChannelId(final String mChannelId) {
            this.mChannelId = mChannelId;
            return this;
        }
        
        public Builder setChronometerCountDown(final boolean mChronometerCountDown) {
            this.mChronometerCountDown = mChronometerCountDown;
            this.getExtras().putBoolean("android.chronometerCountDown", mChronometerCountDown);
            return this;
        }
        
        public Builder setColor(final int mColor) {
            this.mColor = mColor;
            return this;
        }
        
        public Builder setColorized(final boolean mColorized) {
            this.mColorized = mColorized;
            this.mColorizedSet = true;
            return this;
        }
        
        public Builder setContent(final RemoteViews contentView) {
            this.mNotification.contentView = contentView;
            return this;
        }
        
        public Builder setContentInfo(final CharSequence charSequence) {
            this.mContentInfo = limitCharSequenceLength(charSequence);
            return this;
        }
        
        public Builder setContentIntent(final PendingIntent mContentIntent) {
            this.mContentIntent = mContentIntent;
            return this;
        }
        
        public Builder setContentText(final CharSequence charSequence) {
            this.mContentText = limitCharSequenceLength(charSequence);
            return this;
        }
        
        public Builder setContentTitle(final CharSequence charSequence) {
            this.mContentTitle = limitCharSequenceLength(charSequence);
            return this;
        }
        
        public Builder setCustomBigContentView(final RemoteViews mBigContentView) {
            this.mBigContentView = mBigContentView;
            return this;
        }
        
        public Builder setCustomContentView(final RemoteViews mContentView) {
            this.mContentView = mContentView;
            return this;
        }
        
        public Builder setCustomHeadsUpContentView(final RemoteViews mHeadsUpContentView) {
            this.mHeadsUpContentView = mHeadsUpContentView;
            return this;
        }
        
        public Builder setDefaults(final int defaults) {
            this.mNotification.defaults = defaults;
            if ((defaults & 0x4) != 0x0) {
                final Notification mNotification = this.mNotification;
                mNotification.flags |= 0x1;
            }
            return this;
        }
        
        public Builder setDeleteIntent(final PendingIntent deleteIntent) {
            this.mNotification.deleteIntent = deleteIntent;
            return this;
        }
        
        public Builder setExtras(final Bundle mExtras) {
            this.mExtras = mExtras;
            return this;
        }
        
        public Builder setForegroundServiceBehavior(final int mFgsDeferBehavior) {
            this.mFgsDeferBehavior = mFgsDeferBehavior;
            return this;
        }
        
        public Builder setFullScreenIntent(final PendingIntent mFullScreenIntent, final boolean b) {
            this.mFullScreenIntent = mFullScreenIntent;
            this.setFlag(128, b);
            return this;
        }
        
        public Builder setGroup(final String mGroupKey) {
            this.mGroupKey = mGroupKey;
            return this;
        }
        
        public Builder setGroupAlertBehavior(final int mGroupAlertBehavior) {
            this.mGroupAlertBehavior = mGroupAlertBehavior;
            return this;
        }
        
        public Builder setGroupSummary(final boolean mGroupSummary) {
            this.mGroupSummary = mGroupSummary;
            return this;
        }
        
        public Builder setLargeIcon(final Bitmap bitmap) {
            IconCompat withBitmap;
            if (bitmap == null) {
                withBitmap = null;
            }
            else {
                withBitmap = IconCompat.createWithBitmap(NotificationCompat.reduceLargeIconSize(this.mContext, bitmap));
            }
            this.mLargeIcon = withBitmap;
            return this;
        }
        
        public Builder setLargeIcon(final Icon icon) {
            IconCompat fromIcon;
            if (icon == null) {
                fromIcon = null;
            }
            else {
                fromIcon = IconCompat.createFromIcon(icon);
            }
            this.mLargeIcon = fromIcon;
            return this;
        }
        
        public Builder setLights(int ledARGB, final int ledOnMS, final int ledOffMS) {
            this.mNotification.ledARGB = ledARGB;
            this.mNotification.ledOnMS = ledOnMS;
            this.mNotification.ledOffMS = ledOffMS;
            if (this.mNotification.ledOnMS != 0 && this.mNotification.ledOffMS != 0) {
                ledARGB = 1;
            }
            else {
                ledARGB = 0;
            }
            final Notification mNotification = this.mNotification;
            mNotification.flags = (ledARGB | (mNotification.flags & 0xFFFFFFFE));
            return this;
        }
        
        public Builder setLocalOnly(final boolean mLocalOnly) {
            this.mLocalOnly = mLocalOnly;
            return this;
        }
        
        public Builder setLocusId(final LocusIdCompat mLocusId) {
            this.mLocusId = mLocusId;
            return this;
        }
        
        @Deprecated
        public Builder setNotificationSilent() {
            this.mSilent = true;
            return this;
        }
        
        public Builder setNumber(final int mNumber) {
            this.mNumber = mNumber;
            return this;
        }
        
        public Builder setOngoing(final boolean b) {
            this.setFlag(2, b);
            return this;
        }
        
        public Builder setOnlyAlertOnce(final boolean b) {
            this.setFlag(8, b);
            return this;
        }
        
        public Builder setPriority(final int mPriority) {
            this.mPriority = mPriority;
            return this;
        }
        
        public Builder setProgress(final int mProgressMax, final int mProgress, final boolean mProgressIndeterminate) {
            this.mProgressMax = mProgressMax;
            this.mProgress = mProgress;
            this.mProgressIndeterminate = mProgressIndeterminate;
            return this;
        }
        
        public Builder setPublicVersion(final Notification mPublicVersion) {
            this.mPublicVersion = mPublicVersion;
            return this;
        }
        
        public Builder setRemoteInputHistory(final CharSequence[] mRemoteInputHistory) {
            this.mRemoteInputHistory = mRemoteInputHistory;
            return this;
        }
        
        public Builder setSettingsText(final CharSequence charSequence) {
            this.mSettingsText = limitCharSequenceLength(charSequence);
            return this;
        }
        
        public Builder setShortcutId(final String mShortcutId) {
            this.mShortcutId = mShortcutId;
            return this;
        }
        
        public Builder setShortcutInfo(final ShortcutInfoCompat shortcutInfoCompat) {
            if (shortcutInfoCompat == null) {
                return this;
            }
            this.mShortcutId = shortcutInfoCompat.getId();
            if (this.mLocusId == null) {
                if (shortcutInfoCompat.getLocusId() != null) {
                    this.mLocusId = shortcutInfoCompat.getLocusId();
                }
                else if (shortcutInfoCompat.getId() != null) {
                    this.mLocusId = new LocusIdCompat(shortcutInfoCompat.getId());
                }
            }
            if (this.mContentTitle == null) {
                this.setContentTitle(shortcutInfoCompat.getShortLabel());
            }
            return this;
        }
        
        public Builder setShowWhen(final boolean mShowWhen) {
            this.mShowWhen = mShowWhen;
            return this;
        }
        
        public Builder setSilent(final boolean mSilent) {
            this.mSilent = mSilent;
            return this;
        }
        
        public Builder setSmallIcon(final int icon) {
            this.mNotification.icon = icon;
            return this;
        }
        
        public Builder setSmallIcon(final int icon, final int iconLevel) {
            this.mNotification.icon = icon;
            this.mNotification.iconLevel = iconLevel;
            return this;
        }
        
        public Builder setSmallIcon(final IconCompat iconCompat) {
            this.mSmallIcon = iconCompat.toIcon(this.mContext);
            return this;
        }
        
        public Builder setSortKey(final String mSortKey) {
            this.mSortKey = mSortKey;
            return this;
        }
        
        public Builder setSound(final Uri sound) {
            this.mNotification.sound = sound;
            this.mNotification.audioStreamType = -1;
            if (Build$VERSION.SDK_INT >= 21) {
                this.mNotification.audioAttributes = Api21Impl.build(Api21Impl.setUsage(Api21Impl.setContentType(Api21Impl.createBuilder(), 4), 5));
            }
            return this;
        }
        
        public Builder setSound(final Uri sound, final int audioStreamType) {
            this.mNotification.sound = sound;
            this.mNotification.audioStreamType = audioStreamType;
            if (Build$VERSION.SDK_INT >= 21) {
                this.mNotification.audioAttributes = Api21Impl.build(Api21Impl.setLegacyStreamType(Api21Impl.setContentType(Api21Impl.createBuilder(), 4), audioStreamType));
            }
            return this;
        }
        
        public Builder setStyle(final Style mStyle) {
            if (this.mStyle != mStyle && (this.mStyle = mStyle) != null) {
                mStyle.setBuilder(this);
            }
            return this;
        }
        
        public Builder setSubText(final CharSequence charSequence) {
            this.mSubText = limitCharSequenceLength(charSequence);
            return this;
        }
        
        public Builder setTicker(final CharSequence charSequence) {
            this.mNotification.tickerText = limitCharSequenceLength(charSequence);
            return this;
        }
        
        @Deprecated
        public Builder setTicker(final CharSequence charSequence, final RemoteViews mTickerView) {
            this.mNotification.tickerText = limitCharSequenceLength(charSequence);
            this.mTickerView = mTickerView;
            return this;
        }
        
        public Builder setTimeoutAfter(final long mTimeout) {
            this.mTimeout = mTimeout;
            return this;
        }
        
        public Builder setUsesChronometer(final boolean mUseChronometer) {
            this.mUseChronometer = mUseChronometer;
            return this;
        }
        
        public Builder setVibrate(final long[] vibrate) {
            this.mNotification.vibrate = vibrate;
            return this;
        }
        
        public Builder setVisibility(final int mVisibility) {
            this.mVisibility = mVisibility;
            return this;
        }
        
        public Builder setWhen(final long when) {
            this.mNotification.when = when;
            return this;
        }
        
        static class Api21Impl
        {
            private Api21Impl() {
            }
            
            static AudioAttributes build(final AudioAttributes$Builder audioAttributes$Builder) {
                return audioAttributes$Builder.build();
            }
            
            static AudioAttributes$Builder createBuilder() {
                return new AudioAttributes$Builder();
            }
            
            static AudioAttributes$Builder setContentType(final AudioAttributes$Builder audioAttributes$Builder, final int contentType) {
                return audioAttributes$Builder.setContentType(contentType);
            }
            
            static AudioAttributes$Builder setLegacyStreamType(final AudioAttributes$Builder audioAttributes$Builder, final int legacyStreamType) {
                return audioAttributes$Builder.setLegacyStreamType(legacyStreamType);
            }
            
            static AudioAttributes$Builder setUsage(final AudioAttributes$Builder audioAttributes$Builder, final int usage) {
                return audioAttributes$Builder.setUsage(usage);
            }
        }
        
        static class Api23Impl
        {
            private Api23Impl() {
            }
            
            static Icon getLargeIcon(final Notification notification) {
                return notification.getLargeIcon();
            }
            
            static Icon getSmallIcon(final Notification notification) {
                return notification.getSmallIcon();
            }
        }
        
        static class Api24Impl
        {
            private Api24Impl() {
            }
            
            static RemoteViews createBigContentView(final Notification$Builder notification$Builder) {
                return notification$Builder.createHeadsUpContentView();
            }
            
            static RemoteViews createContentView(final Notification$Builder notification$Builder) {
                return notification$Builder.createContentView();
            }
            
            static RemoteViews createHeadsUpContentView(final Notification$Builder notification$Builder) {
                return notification$Builder.createHeadsUpContentView();
            }
            
            static Notification$Builder recoverBuilder(final Context context, final Notification notification) {
                return Notification$Builder.recoverBuilder(context, notification);
            }
        }
    }
    
    public static class CallStyle extends Style
    {
        public static final int CALL_TYPE_INCOMING = 1;
        public static final int CALL_TYPE_ONGOING = 2;
        public static final int CALL_TYPE_SCREENING = 3;
        public static final int CALL_TYPE_UNKNOWN = 0;
        private static final String KEY_ACTION_PRIORITY = "key_action_priority";
        private static final String TEMPLATE_CLASS_NAME = "androidx.core.app.NotificationCompat$CallStyle";
        private Integer mAnswerButtonColor;
        private PendingIntent mAnswerIntent;
        private int mCallType;
        private Integer mDeclineButtonColor;
        private PendingIntent mDeclineIntent;
        private PendingIntent mHangUpIntent;
        private boolean mIsVideo;
        private Person mPerson;
        private IconCompat mVerificationIcon;
        private CharSequence mVerificationText;
        
        public CallStyle() {
        }
        
        private CallStyle(final int mCallType, final Person mPerson, final PendingIntent mHangUpIntent, final PendingIntent mDeclineIntent, final PendingIntent mAnswerIntent) {
            if (mPerson != null && !TextUtils.isEmpty(mPerson.getName())) {
                this.mCallType = mCallType;
                this.mPerson = mPerson;
                this.mAnswerIntent = mAnswerIntent;
                this.mDeclineIntent = mDeclineIntent;
                this.mHangUpIntent = mHangUpIntent;
                return;
            }
            throw new IllegalArgumentException("person must have a non-empty a name");
        }
        
        public CallStyle(final NotificationCompat.Builder builder) {
            ((Style)this).setBuilder(builder);
        }
        
        public static CallStyle forIncomingCall(final Person person, final PendingIntent obj, final PendingIntent obj2) {
            return new CallStyle(1, person, null, Objects.requireNonNull(obj, "declineIntent is required"), Objects.requireNonNull(obj2, "answerIntent is required"));
        }
        
        public static CallStyle forOngoingCall(final Person person, final PendingIntent obj) {
            return new CallStyle(2, person, Objects.requireNonNull(obj, "hangUpIntent is required"), null, null);
        }
        
        public static CallStyle forScreeningCall(final Person person, final PendingIntent obj, final PendingIntent obj2) {
            return new CallStyle(3, person, Objects.requireNonNull(obj, "hangUpIntent is required"), null, Objects.requireNonNull(obj2, "answerIntent is required"));
        }
        
        private String getDefaultText() {
            final int mCallType = this.mCallType;
            if (mCallType == 1) {
                return this.mBuilder.mContext.getResources().getString(R.string.call_notification_incoming_text);
            }
            if (mCallType == 2) {
                return this.mBuilder.mContext.getResources().getString(R.string.call_notification_ongoing_text);
            }
            if (mCallType != 3) {
                return null;
            }
            return this.mBuilder.mContext.getResources().getString(R.string.call_notification_screening_text);
        }
        
        private boolean isActionAddedByCallStyle(final Action action) {
            return action != null && action.getExtras().getBoolean("key_action_priority");
        }
        
        private Action makeAction(final int n, final int n2, final Integer n3, final int n4, final PendingIntent pendingIntent) {
            Integer value = n3;
            if (n3 == null) {
                value = ContextCompat.getColor(this.mBuilder.mContext, n4);
            }
            final SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder();
            spannableStringBuilder.append((CharSequence)this.mBuilder.mContext.getResources().getString(n2));
            spannableStringBuilder.setSpan((Object)new ForegroundColorSpan((int)value), 0, spannableStringBuilder.length(), 18);
            final Action build = new Action.Builder(IconCompat.createWithResource(this.mBuilder.mContext, n), (CharSequence)spannableStringBuilder, pendingIntent).build();
            build.getExtras().putBoolean("key_action_priority", true);
            return build;
        }
        
        private Action makeAnswerAction() {
            int n = R.drawable.ic_call_answer_video_low;
            int n2 = R.drawable.ic_call_answer_low;
            if (Build$VERSION.SDK_INT >= 21) {
                n = R.drawable.ic_call_answer_video;
                n2 = R.drawable.ic_call_answer;
            }
            Action action;
            if (this.mAnswerIntent == null) {
                action = null;
            }
            else {
                final boolean mIsVideo = this.mIsVideo;
                if (mIsVideo) {
                    n2 = n;
                }
                int n3;
                if (mIsVideo) {
                    n3 = R.string.call_notification_answer_video_action;
                }
                else {
                    n3 = R.string.call_notification_answer_action;
                }
                action = this.makeAction(n2, n3, this.mAnswerButtonColor, R.color.call_notification_answer_color, this.mAnswerIntent);
            }
            return action;
        }
        
        private Action makeNegativeAction() {
            int n = R.drawable.ic_call_decline_low;
            if (Build$VERSION.SDK_INT >= 21) {
                n = R.drawable.ic_call_decline;
            }
            if (this.mDeclineIntent == null) {
                return this.makeAction(n, R.string.call_notification_hang_up_action, this.mDeclineButtonColor, R.color.call_notification_decline_color, this.mHangUpIntent);
            }
            return this.makeAction(n, R.string.call_notification_decline_action, this.mDeclineButtonColor, R.color.call_notification_decline_color, this.mDeclineIntent);
        }
        
        @Override
        public void addCompatExtras(final Bundle bundle) {
            super.addCompatExtras(bundle);
            bundle.putInt("android.callType", this.mCallType);
            bundle.putBoolean("android.callIsVideo", this.mIsVideo);
            if (this.mPerson != null) {
                if (Build$VERSION.SDK_INT >= 28) {
                    bundle.putParcelable("android.callPerson", Api28Impl.castToParcelable(this.mPerson.toAndroidPerson()));
                }
                else {
                    bundle.putParcelable("android.callPersonCompat", (Parcelable)this.mPerson.toBundle());
                }
            }
            if (this.mVerificationIcon != null) {
                if (Build$VERSION.SDK_INT >= 23) {
                    bundle.putParcelable("android.verificationIcon", Api23Impl.castToParcelable(this.mVerificationIcon.toIcon(this.mBuilder.mContext)));
                }
                else {
                    bundle.putParcelable("android.verificationIconCompat", (Parcelable)this.mVerificationIcon.toBundle());
                }
            }
            bundle.putCharSequence("android.verificationText", this.mVerificationText);
            bundle.putParcelable("android.answerIntent", (Parcelable)this.mAnswerIntent);
            bundle.putParcelable("android.declineIntent", (Parcelable)this.mDeclineIntent);
            bundle.putParcelable("android.hangUpIntent", (Parcelable)this.mHangUpIntent);
            final Integer mAnswerButtonColor = this.mAnswerButtonColor;
            if (mAnswerButtonColor != null) {
                bundle.putInt("android.answerColor", (int)mAnswerButtonColor);
            }
            final Integer mDeclineButtonColor = this.mDeclineButtonColor;
            if (mDeclineButtonColor != null) {
                bundle.putInt("android.declineColor", (int)mDeclineButtonColor);
            }
        }
        
        @Override
        public void apply(final NotificationBuilderWithBuilderAccessor notificationBuilderWithBuilderAccessor) {
            final int sdk_INT = Build$VERSION.SDK_INT;
            final CharSequence charSequence = null;
            final Notification$CallStyle notification$CallStyle = null;
            if (sdk_INT >= 31) {
                final int mCallType = this.mCallType;
                Notification$CallStyle notification$CallStyle2;
                if (mCallType != 1) {
                    if (mCallType != 2) {
                        if (mCallType != 3) {
                            notification$CallStyle2 = notification$CallStyle;
                            if (Log.isLoggable("NotifCompat", 3)) {
                                final StringBuilder sb = new StringBuilder();
                                sb.append("Unrecognized call type in CallStyle: ");
                                sb.append(String.valueOf(this.mCallType));
                                Log.d("NotifCompat", sb.toString());
                                notification$CallStyle2 = notification$CallStyle;
                            }
                        }
                        else {
                            notification$CallStyle2 = Api31Impl.forScreeningCall(this.mPerson.toAndroidPerson(), this.mHangUpIntent, this.mAnswerIntent);
                        }
                    }
                    else {
                        notification$CallStyle2 = Api31Impl.forOngoingCall(this.mPerson.toAndroidPerson(), this.mHangUpIntent);
                    }
                }
                else {
                    notification$CallStyle2 = Api31Impl.forIncomingCall(this.mPerson.toAndroidPerson(), this.mDeclineIntent, this.mAnswerIntent);
                }
                if (notification$CallStyle2 != null) {
                    Api16Impl.setBuilder(notification$CallStyle2, notificationBuilderWithBuilderAccessor.getBuilder());
                    final Integer mAnswerButtonColor = this.mAnswerButtonColor;
                    if (mAnswerButtonColor != null) {
                        Api31Impl.setAnswerButtonColorHint(notification$CallStyle2, mAnswerButtonColor);
                    }
                    final Integer mDeclineButtonColor = this.mDeclineButtonColor;
                    if (mDeclineButtonColor != null) {
                        Api31Impl.setDeclineButtonColorHint(notification$CallStyle2, mDeclineButtonColor);
                    }
                    Api31Impl.setVerificationText(notification$CallStyle2, this.mVerificationText);
                    final IconCompat mVerificationIcon = this.mVerificationIcon;
                    if (mVerificationIcon != null) {
                        Api31Impl.setVerificationIcon(notification$CallStyle2, mVerificationIcon.toIcon(this.mBuilder.mContext));
                    }
                    Api31Impl.setIsVideo(notification$CallStyle2, this.mIsVideo);
                }
            }
            else {
                final Notification$Builder builder = notificationBuilderWithBuilderAccessor.getBuilder();
                final Person mPerson = this.mPerson;
                CharSequence name;
                if (mPerson != null) {
                    name = mPerson.getName();
                }
                else {
                    name = null;
                }
                builder.setContentTitle(name);
                CharSequence charSequence2 = charSequence;
                if (this.mBuilder.mExtras != null) {
                    charSequence2 = charSequence;
                    if (this.mBuilder.mExtras.containsKey("android.text")) {
                        charSequence2 = this.mBuilder.mExtras.getCharSequence("android.text");
                    }
                }
                CharSequence defaultText;
                if ((defaultText = charSequence2) == null) {
                    defaultText = this.getDefaultText();
                }
                builder.setContentText(defaultText);
                if (this.mPerson != null) {
                    if (Build$VERSION.SDK_INT >= 23 && this.mPerson.getIcon() != null) {
                        Api23Impl.setLargeIcon(builder, this.mPerson.getIcon().toIcon(this.mBuilder.mContext));
                    }
                    if (Build$VERSION.SDK_INT >= 28) {
                        Api28Impl.addPerson(builder, this.mPerson.toAndroidPerson());
                    }
                    else if (Build$VERSION.SDK_INT >= 21) {
                        Api21Impl.addPerson(builder, this.mPerson.getUri());
                    }
                }
                if (Build$VERSION.SDK_INT >= 21) {
                    Api21Impl.setCategory(builder, "call");
                }
            }
        }
        
        @Override
        public boolean displayCustomViewInline() {
            return true;
        }
        
        public ArrayList<Action> getActionsListWithSystemActions() {
            final Action negativeAction = this.makeNegativeAction();
            final Action answerAction = this.makeAnswerAction();
            final ArrayList list = new ArrayList(3);
            list.add(negativeAction);
            int n = 2;
            final ArrayList<Action> mActions = this.mBuilder.mActions;
            int n2 = n;
            if (mActions != null) {
                final Iterator<Object> iterator = mActions.iterator();
                while (true) {
                    n2 = n;
                    if (!iterator.hasNext()) {
                        break;
                    }
                    final Action action = iterator.next();
                    int n3;
                    if (action.isContextual()) {
                        list.add(action);
                        n3 = n;
                    }
                    else if (this.isActionAddedByCallStyle(action)) {
                        n3 = n;
                    }
                    else if ((n3 = n) > 1) {
                        list.add(action);
                        n3 = n - 1;
                    }
                    n = n3;
                    if (answerAction == null || (n = n3) != 1) {
                        continue;
                    }
                    list.add(answerAction);
                    n = n3 - 1;
                }
            }
            if (answerAction != null && n2 >= 1) {
                list.add(answerAction);
            }
            return list;
        }
        
        @Override
        protected String getClassName() {
            return "androidx.core.app.NotificationCompat$CallStyle";
        }
        
        @Override
        protected void restoreFromCompatExtras(final Bundle bundle) {
            super.restoreFromCompatExtras(bundle);
            this.mCallType = bundle.getInt("android.callType");
            this.mIsVideo = bundle.getBoolean("android.callIsVideo");
            if (Build$VERSION.SDK_INT >= 28 && bundle.containsKey("android.callPerson")) {
                this.mPerson = Person.fromAndroidPerson((android.app.Person)bundle.getParcelable("android.callPerson"));
            }
            else if (bundle.containsKey("android.callPersonCompat")) {
                this.mPerson = Person.fromBundle(bundle.getBundle("android.callPersonCompat"));
            }
            if (Build$VERSION.SDK_INT >= 23 && bundle.containsKey("android.verificationIcon")) {
                this.mVerificationIcon = IconCompat.createFromIcon((Icon)bundle.getParcelable("android.verificationIcon"));
            }
            else if (bundle.containsKey("android.verificationIconCompat")) {
                this.mVerificationIcon = IconCompat.createFromBundle(bundle.getBundle("android.verificationIconCompat"));
            }
            this.mVerificationText = bundle.getCharSequence("android.verificationText");
            this.mAnswerIntent = (PendingIntent)bundle.getParcelable("android.answerIntent");
            this.mDeclineIntent = (PendingIntent)bundle.getParcelable("android.declineIntent");
            this.mHangUpIntent = (PendingIntent)bundle.getParcelable("android.hangUpIntent");
            final boolean containsKey = bundle.containsKey("android.answerColor");
            final Integer n = null;
            Integer value;
            if (containsKey) {
                value = bundle.getInt("android.answerColor");
            }
            else {
                value = null;
            }
            this.mAnswerButtonColor = value;
            Integer value2 = n;
            if (bundle.containsKey("android.declineColor")) {
                value2 = bundle.getInt("android.declineColor");
            }
            this.mDeclineButtonColor = value2;
        }
        
        public CallStyle setAnswerButtonColorHint(final int i) {
            this.mAnswerButtonColor = i;
            return this;
        }
        
        public CallStyle setDeclineButtonColorHint(final int i) {
            this.mDeclineButtonColor = i;
            return this;
        }
        
        public CallStyle setIsVideo(final boolean mIsVideo) {
            this.mIsVideo = mIsVideo;
            return this;
        }
        
        public CallStyle setVerificationIcon(final Bitmap bitmap) {
            this.mVerificationIcon = IconCompat.createWithBitmap(bitmap);
            return this;
        }
        
        public CallStyle setVerificationIcon(final Icon icon) {
            IconCompat fromIcon;
            if (icon == null) {
                fromIcon = null;
            }
            else {
                fromIcon = IconCompat.createFromIcon(icon);
            }
            this.mVerificationIcon = fromIcon;
            return this;
        }
        
        public CallStyle setVerificationText(final CharSequence mVerificationText) {
            this.mVerificationText = mVerificationText;
            return this;
        }
        
        static class Api16Impl
        {
            private Api16Impl() {
            }
            
            static void setBuilder(final Notification$CallStyle notification$CallStyle, final Notification$Builder builder) {
                notification$CallStyle.setBuilder(builder);
            }
        }
        
        static class Api20Impl
        {
            private Api20Impl() {
            }
            
            static Notification$Action$Builder addExtras(final Notification$Action$Builder notification$Action$Builder, final Bundle bundle) {
                return notification$Action$Builder.addExtras(bundle);
            }
            
            static Notification$Action$Builder addRemoteInput(final Notification$Action$Builder notification$Action$Builder, final android.app.RemoteInput remoteInput) {
                return notification$Action$Builder.addRemoteInput(remoteInput);
            }
            
            static Notification$Action build(final Notification$Action$Builder notification$Action$Builder) {
                return notification$Action$Builder.build();
            }
            
            static Notification$Action$Builder createActionBuilder(final int n, final CharSequence charSequence, final PendingIntent pendingIntent) {
                return new Notification$Action$Builder(n, charSequence, pendingIntent);
            }
        }
        
        static class Api21Impl
        {
            private Api21Impl() {
            }
            
            static Notification$Builder addPerson(final Notification$Builder notification$Builder, final String s) {
                return notification$Builder.addPerson(s);
            }
            
            static Notification$Builder setCategory(final Notification$Builder notification$Builder, final String category) {
                return notification$Builder.setCategory(category);
            }
        }
        
        static class Api23Impl
        {
            private Api23Impl() {
            }
            
            static Parcelable castToParcelable(final Icon icon) {
                return (Parcelable)icon;
            }
            
            static Notification$Action$Builder createActionBuilder(final Icon icon, final CharSequence charSequence, final PendingIntent pendingIntent) {
                return new Notification$Action$Builder(icon, charSequence, pendingIntent);
            }
            
            static void setLargeIcon(final Notification$Builder notification$Builder, final Icon largeIcon) {
                notification$Builder.setLargeIcon(largeIcon);
            }
        }
        
        static class Api24Impl
        {
            private Api24Impl() {
            }
            
            static Notification$Action$Builder setAllowGeneratedReplies(final Notification$Action$Builder notification$Action$Builder, final boolean allowGeneratedReplies) {
                return notification$Action$Builder.setAllowGeneratedReplies(allowGeneratedReplies);
            }
        }
        
        static class Api28Impl
        {
            private Api28Impl() {
            }
            
            static Notification$Builder addPerson(final Notification$Builder notification$Builder, final android.app.Person person) {
                return notification$Builder.addPerson(person);
            }
            
            static Parcelable castToParcelable(final android.app.Person person) {
                return (Parcelable)person;
            }
        }
        
        static class Api31Impl
        {
            private Api31Impl() {
            }
            
            static Notification$CallStyle forIncomingCall(final android.app.Person person, final PendingIntent pendingIntent, final PendingIntent pendingIntent2) {
                return Notification$CallStyle.forIncomingCall(person, pendingIntent, pendingIntent2);
            }
            
            static Notification$CallStyle forOngoingCall(final android.app.Person person, final PendingIntent pendingIntent) {
                return Notification$CallStyle.forOngoingCall(person, pendingIntent);
            }
            
            static Notification$CallStyle forScreeningCall(final android.app.Person person, final PendingIntent pendingIntent, final PendingIntent pendingIntent2) {
                return Notification$CallStyle.forScreeningCall(person, pendingIntent, pendingIntent2);
            }
            
            static Notification$CallStyle setAnswerButtonColorHint(final Notification$CallStyle notification$CallStyle, final int answerButtonColorHint) {
                return notification$CallStyle.setAnswerButtonColorHint(answerButtonColorHint);
            }
            
            static Notification$Action$Builder setAuthenticationRequired(final Notification$Action$Builder notification$Action$Builder, final boolean authenticationRequired) {
                return notification$Action$Builder.setAuthenticationRequired(authenticationRequired);
            }
            
            static Notification$CallStyle setDeclineButtonColorHint(final Notification$CallStyle notification$CallStyle, final int declineButtonColorHint) {
                return notification$CallStyle.setDeclineButtonColorHint(declineButtonColorHint);
            }
            
            static Notification$CallStyle setIsVideo(final Notification$CallStyle notification$CallStyle, final boolean isVideo) {
                return notification$CallStyle.setIsVideo(isVideo);
            }
            
            static Notification$CallStyle setVerificationIcon(final Notification$CallStyle notification$CallStyle, final Icon verificationIcon) {
                return notification$CallStyle.setVerificationIcon(verificationIcon);
            }
            
            static Notification$CallStyle setVerificationText(final Notification$CallStyle notification$CallStyle, final CharSequence verificationText) {
                return notification$CallStyle.setVerificationText(verificationText);
            }
        }
        
        @Retention(RetentionPolicy.SOURCE)
        public @interface CallType {
        }
    }
    
    public static final class CarExtender implements NotificationCompat.Extender
    {
        static final String EXTRA_CAR_EXTENDER = "android.car.EXTENSIONS";
        private static final String EXTRA_COLOR = "app_color";
        private static final String EXTRA_CONVERSATION = "car_conversation";
        static final String EXTRA_INVISIBLE_ACTIONS = "invisible_actions";
        private static final String EXTRA_LARGE_ICON = "large_icon";
        private static final String KEY_AUTHOR = "author";
        private static final String KEY_MESSAGES = "messages";
        private static final String KEY_ON_READ = "on_read";
        private static final String KEY_ON_REPLY = "on_reply";
        private static final String KEY_PARTICIPANTS = "participants";
        private static final String KEY_REMOTE_INPUT = "remote_input";
        private static final String KEY_TEXT = "text";
        private static final String KEY_TIMESTAMP = "timestamp";
        private int mColor;
        private Bitmap mLargeIcon;
        private UnreadConversation mUnreadConversation;
        
        public CarExtender() {
            this.mColor = 0;
        }
        
        public CarExtender(final Notification notification) {
            this.mColor = 0;
            if (Build$VERSION.SDK_INT < 21) {
                return;
            }
            Bundle bundle;
            if (NotificationCompat.getExtras(notification) == null) {
                bundle = null;
            }
            else {
                bundle = NotificationCompat.getExtras(notification).getBundle("android.car.EXTENSIONS");
            }
            if (bundle != null) {
                this.mLargeIcon = (Bitmap)bundle.getParcelable("large_icon");
                this.mColor = bundle.getInt("app_color", 0);
                this.mUnreadConversation = getUnreadConversationFromBundle(bundle.getBundle("car_conversation"));
            }
        }
        
        private static Bundle getBundleForUnreadConversation(final UnreadConversation unreadConversation) {
            final Bundle bundle = new Bundle();
            final String[] participants = unreadConversation.getParticipants();
            int i = 0;
            String s;
            if (participants != null && unreadConversation.getParticipants().length > 1) {
                s = unreadConversation.getParticipants()[0];
            }
            else {
                s = null;
            }
            final int length = unreadConversation.getMessages().length;
            final Parcelable[] array = new Parcelable[length];
            while (i < length) {
                final Bundle bundle2 = new Bundle();
                bundle2.putString("text", unreadConversation.getMessages()[i]);
                bundle2.putString("author", s);
                array[i] = (Parcelable)bundle2;
                ++i;
            }
            bundle.putParcelableArray("messages", array);
            final RemoteInput remoteInput = unreadConversation.getRemoteInput();
            if (remoteInput != null) {
                final RemoteInput$Builder builder = Api20Impl.createBuilder(remoteInput.getResultKey());
                Api20Impl.setLabel(builder, remoteInput.getLabel());
                Api20Impl.setChoices(builder, remoteInput.getChoices());
                Api20Impl.setAllowFreeFormInput(builder, remoteInput.getAllowFreeFormInput());
                Api20Impl.addExtras(builder, remoteInput.getExtras());
                bundle.putParcelable("remote_input", Api20Impl.castToParcelable(Api20Impl.build(builder)));
            }
            bundle.putParcelable("on_reply", (Parcelable)unreadConversation.getReplyPendingIntent());
            bundle.putParcelable("on_read", (Parcelable)unreadConversation.getReadPendingIntent());
            bundle.putStringArray("participants", unreadConversation.getParticipants());
            bundle.putLong("timestamp", unreadConversation.getLatestTimestamp());
            return bundle;
        }
        
        private static UnreadConversation getUnreadConversationFromBundle(final Bundle bundle) {
            final UnreadConversation unreadConversation = null;
            final RemoteInput remoteInput = null;
            if (bundle == null) {
                return null;
            }
            final Parcelable[] parcelableArray = bundle.getParcelableArray("messages");
            String[] array = null;
            Label_0107: {
                if (parcelableArray != null) {
                    final int length = parcelableArray.length;
                    array = new String[length];
                    int i = 0;
                    while (true) {
                        while (i < length) {
                            final Parcelable parcelable = parcelableArray[i];
                            if (parcelable instanceof Bundle && (array[i] = ((Bundle)parcelable).getString("text")) != null) {
                                ++i;
                            }
                            else {
                                final boolean b = false;
                                if (b) {
                                    break Label_0107;
                                }
                                return null;
                            }
                        }
                        final boolean b = true;
                        continue;
                    }
                }
                array = null;
            }
            final PendingIntent pendingIntent = (PendingIntent)bundle.getParcelable("on_read");
            final PendingIntent pendingIntent2 = (PendingIntent)bundle.getParcelable("on_reply");
            final android.app.RemoteInput remoteInput2 = (android.app.RemoteInput)bundle.getParcelable("remote_input");
            final String[] stringArray = bundle.getStringArray("participants");
            UnreadConversation unreadConversation2 = unreadConversation;
            if (stringArray != null) {
                if (stringArray.length != 1) {
                    unreadConversation2 = unreadConversation;
                }
                else {
                    RemoteInput remoteInput3 = remoteInput;
                    if (remoteInput2 != null) {
                        final String resultKey = Api20Impl.getResultKey(remoteInput2);
                        final CharSequence label = Api20Impl.getLabel(remoteInput2);
                        final CharSequence[] choices = Api20Impl.getChoices(remoteInput2);
                        final boolean allowFreeFormInput = Api20Impl.getAllowFreeFormInput(remoteInput2);
                        int editChoicesBeforeSending;
                        if (Build$VERSION.SDK_INT >= 29) {
                            editChoicesBeforeSending = Api29Impl.getEditChoicesBeforeSending(remoteInput2);
                        }
                        else {
                            editChoicesBeforeSending = 0;
                        }
                        remoteInput3 = new RemoteInput(resultKey, label, choices, allowFreeFormInput, editChoicesBeforeSending, Api20Impl.getExtras(remoteInput2), null);
                    }
                    unreadConversation2 = new UnreadConversation(array, remoteInput3, pendingIntent2, pendingIntent, stringArray, bundle.getLong("timestamp"));
                }
            }
            return unreadConversation2;
        }
        
        @Override
        public NotificationCompat.Builder extend(final NotificationCompat.Builder builder) {
            if (Build$VERSION.SDK_INT < 21) {
                return builder;
            }
            final Bundle bundle = new Bundle();
            final Bitmap mLargeIcon = this.mLargeIcon;
            if (mLargeIcon != null) {
                bundle.putParcelable("large_icon", (Parcelable)mLargeIcon);
            }
            final int mColor = this.mColor;
            if (mColor != 0) {
                bundle.putInt("app_color", mColor);
            }
            final UnreadConversation mUnreadConversation = this.mUnreadConversation;
            if (mUnreadConversation != null) {
                bundle.putBundle("car_conversation", getBundleForUnreadConversation(mUnreadConversation));
            }
            builder.getExtras().putBundle("android.car.EXTENSIONS", bundle);
            return builder;
        }
        
        public int getColor() {
            return this.mColor;
        }
        
        public Bitmap getLargeIcon() {
            return this.mLargeIcon;
        }
        
        @Deprecated
        public UnreadConversation getUnreadConversation() {
            return this.mUnreadConversation;
        }
        
        public CarExtender setColor(final int mColor) {
            this.mColor = mColor;
            return this;
        }
        
        public CarExtender setLargeIcon(final Bitmap mLargeIcon) {
            this.mLargeIcon = mLargeIcon;
            return this;
        }
        
        @Deprecated
        public CarExtender setUnreadConversation(final UnreadConversation mUnreadConversation) {
            this.mUnreadConversation = mUnreadConversation;
            return this;
        }
        
        static class Api20Impl
        {
            private Api20Impl() {
            }
            
            static RemoteInput$Builder addExtras(final RemoteInput$Builder remoteInput$Builder, final Bundle bundle) {
                return remoteInput$Builder.addExtras(bundle);
            }
            
            static android.app.RemoteInput build(final RemoteInput$Builder remoteInput$Builder) {
                return remoteInput$Builder.build();
            }
            
            static Parcelable castToParcelable(final android.app.RemoteInput remoteInput) {
                return (Parcelable)remoteInput;
            }
            
            static RemoteInput$Builder createBuilder(final String s) {
                return new RemoteInput$Builder(s);
            }
            
            static boolean getAllowFreeFormInput(final android.app.RemoteInput remoteInput) {
                return remoteInput.getAllowFreeFormInput();
            }
            
            static CharSequence[] getChoices(final android.app.RemoteInput remoteInput) {
                return remoteInput.getChoices();
            }
            
            static Bundle getExtras(final android.app.RemoteInput remoteInput) {
                return remoteInput.getExtras();
            }
            
            static CharSequence getLabel(final android.app.RemoteInput remoteInput) {
                return remoteInput.getLabel();
            }
            
            static String getResultKey(final android.app.RemoteInput remoteInput) {
                return remoteInput.getResultKey();
            }
            
            static RemoteInput$Builder setAllowFreeFormInput(final RemoteInput$Builder remoteInput$Builder, final boolean allowFreeFormInput) {
                return remoteInput$Builder.setAllowFreeFormInput(allowFreeFormInput);
            }
            
            static RemoteInput$Builder setChoices(final RemoteInput$Builder remoteInput$Builder, final CharSequence[] choices) {
                return remoteInput$Builder.setChoices(choices);
            }
            
            static RemoteInput$Builder setLabel(final RemoteInput$Builder remoteInput$Builder, final CharSequence label) {
                return remoteInput$Builder.setLabel(label);
            }
        }
        
        static class Api29Impl
        {
            private Api29Impl() {
            }
            
            static int getEditChoicesBeforeSending(final android.app.RemoteInput remoteInput) {
                return remoteInput.getEditChoicesBeforeSending();
            }
        }
        
        @Deprecated
        public static class UnreadConversation
        {
            private final long mLatestTimestamp;
            private final String[] mMessages;
            private final String[] mParticipants;
            private final PendingIntent mReadPendingIntent;
            private final RemoteInput mRemoteInput;
            private final PendingIntent mReplyPendingIntent;
            
            UnreadConversation(final String[] mMessages, final RemoteInput mRemoteInput, final PendingIntent mReplyPendingIntent, final PendingIntent mReadPendingIntent, final String[] mParticipants, final long mLatestTimestamp) {
                this.mMessages = mMessages;
                this.mRemoteInput = mRemoteInput;
                this.mReadPendingIntent = mReadPendingIntent;
                this.mReplyPendingIntent = mReplyPendingIntent;
                this.mParticipants = mParticipants;
                this.mLatestTimestamp = mLatestTimestamp;
            }
            
            public long getLatestTimestamp() {
                return this.mLatestTimestamp;
            }
            
            public String[] getMessages() {
                return this.mMessages;
            }
            
            public String getParticipant() {
                final String[] mParticipants = this.mParticipants;
                String s;
                if (mParticipants.length > 0) {
                    s = mParticipants[0];
                }
                else {
                    s = null;
                }
                return s;
            }
            
            public String[] getParticipants() {
                return this.mParticipants;
            }
            
            public PendingIntent getReadPendingIntent() {
                return this.mReadPendingIntent;
            }
            
            public RemoteInput getRemoteInput() {
                return this.mRemoteInput;
            }
            
            public PendingIntent getReplyPendingIntent() {
                return this.mReplyPendingIntent;
            }
            
            public static class Builder
            {
                private long mLatestTimestamp;
                private final List<String> mMessages;
                private final String mParticipant;
                private PendingIntent mReadPendingIntent;
                private RemoteInput mRemoteInput;
                private PendingIntent mReplyPendingIntent;
                
                public Builder(final String mParticipant) {
                    this.mMessages = new ArrayList<String>();
                    this.mParticipant = mParticipant;
                }
                
                public Builder addMessage(final String s) {
                    if (s != null) {
                        this.mMessages.add(s);
                    }
                    return this;
                }
                
                public UnreadConversation build() {
                    final List<String> mMessages = this.mMessages;
                    return new UnreadConversation(mMessages.toArray(new String[mMessages.size()]), this.mRemoteInput, this.mReplyPendingIntent, this.mReadPendingIntent, new String[] { this.mParticipant }, this.mLatestTimestamp);
                }
                
                public Builder setLatestTimestamp(final long mLatestTimestamp) {
                    this.mLatestTimestamp = mLatestTimestamp;
                    return this;
                }
                
                public Builder setReadPendingIntent(final PendingIntent mReadPendingIntent) {
                    this.mReadPendingIntent = mReadPendingIntent;
                    return this;
                }
                
                public Builder setReplyAction(final PendingIntent mReplyPendingIntent, final RemoteInput mRemoteInput) {
                    this.mRemoteInput = mRemoteInput;
                    this.mReplyPendingIntent = mReplyPendingIntent;
                    return this;
                }
            }
        }
    }
    
    public static class DecoratedCustomViewStyle extends Style
    {
        private static final int MAX_ACTION_BUTTONS = 3;
        private static final String TEMPLATE_CLASS_NAME = "androidx.core.app.NotificationCompat$DecoratedCustomViewStyle";
        
        private RemoteViews createRemoteViews(final RemoteViews remoteViews, final boolean b) {
            final int notification_template_custom_big = R.layout.notification_template_custom_big;
            final int n = 1;
            final int n2 = 0;
            final RemoteViews applyStandardTemplate = ((Style)this).applyStandardTemplate(true, notification_template_custom_big, false);
            applyStandardTemplate.removeAllViews(R.id.actions);
            final List<Action> nonContextualActions = getNonContextualActions(this.mBuilder.mActions);
            int n4 = 0;
            Label_0114: {
                if (b && nonContextualActions != null) {
                    final int min = Math.min(nonContextualActions.size(), 3);
                    if (min > 0) {
                        int n3 = 0;
                        while (true) {
                            n4 = n;
                            if (n3 >= min) {
                                break Label_0114;
                            }
                            applyStandardTemplate.addView(R.id.actions, this.generateActionButton((Action)nonContextualActions.get(n3)));
                            ++n3;
                        }
                    }
                }
                n4 = 0;
            }
            int n5;
            if (n4 != 0) {
                n5 = n2;
            }
            else {
                n5 = 8;
            }
            applyStandardTemplate.setViewVisibility(R.id.actions, n5);
            applyStandardTemplate.setViewVisibility(R.id.action_divider, n5);
            ((Style)this).buildIntoRemoteViews(applyStandardTemplate, remoteViews);
            return applyStandardTemplate;
        }
        
        private RemoteViews generateActionButton(final Action action) {
            final boolean b = action.actionIntent == null;
            final String packageName = this.mBuilder.mContext.getPackageName();
            int n;
            if (b) {
                n = R.layout.notification_action_tombstone;
            }
            else {
                n = R.layout.notification_action;
            }
            final RemoteViews remoteViews = new RemoteViews(packageName, n);
            final IconCompat iconCompat = action.getIconCompat();
            if (iconCompat != null) {
                remoteViews.setImageViewBitmap(R.id.action_image, ((Style)this).createColoredBitmap(iconCompat, R.color.notification_action_color_filter));
            }
            remoteViews.setTextViewText(R.id.action_text, action.title);
            if (!b) {
                remoteViews.setOnClickPendingIntent(R.id.action_container, action.actionIntent);
            }
            if (Build$VERSION.SDK_INT >= 15) {
                Api15Impl.setContentDescription(remoteViews, R.id.action_container, action.title);
            }
            return remoteViews;
        }
        
        private static List<Action> getNonContextualActions(final List<Action> list) {
            if (list == null) {
                return null;
            }
            final ArrayList list2 = new ArrayList();
            for (final Action action : list) {
                if (!action.isContextual()) {
                    list2.add(action);
                }
            }
            return list2;
        }
        
        @Override
        public void apply(final NotificationBuilderWithBuilderAccessor notificationBuilderWithBuilderAccessor) {
            if (Build$VERSION.SDK_INT >= 24) {
                Api16Impl.setStyle(notificationBuilderWithBuilderAccessor.getBuilder(), Api24Impl.createDecoratedCustomViewStyle());
            }
        }
        
        @Override
        public boolean displayCustomViewInline() {
            return true;
        }
        
        @Override
        protected String getClassName() {
            return "androidx.core.app.NotificationCompat$DecoratedCustomViewStyle";
        }
        
        @Override
        public RemoteViews makeBigContentView(final NotificationBuilderWithBuilderAccessor notificationBuilderWithBuilderAccessor) {
            if (Build$VERSION.SDK_INT >= 24) {
                return null;
            }
            RemoteViews remoteViews = this.mBuilder.getBigContentView();
            if (remoteViews == null) {
                remoteViews = this.mBuilder.getContentView();
            }
            if (remoteViews == null) {
                return null;
            }
            return this.createRemoteViews(remoteViews, true);
        }
        
        @Override
        public RemoteViews makeContentView(final NotificationBuilderWithBuilderAccessor notificationBuilderWithBuilderAccessor) {
            if (Build$VERSION.SDK_INT >= 24) {
                return null;
            }
            if (this.mBuilder.getContentView() == null) {
                return null;
            }
            return this.createRemoteViews(this.mBuilder.getContentView(), false);
        }
        
        @Override
        public RemoteViews makeHeadsUpContentView(final NotificationBuilderWithBuilderAccessor notificationBuilderWithBuilderAccessor) {
            if (Build$VERSION.SDK_INT >= 24) {
                return null;
            }
            final RemoteViews headsUpContentView = this.mBuilder.getHeadsUpContentView();
            RemoteViews contentView;
            if (headsUpContentView != null) {
                contentView = headsUpContentView;
            }
            else {
                contentView = this.mBuilder.getContentView();
            }
            if (headsUpContentView == null) {
                return null;
            }
            return this.createRemoteViews(contentView, true);
        }
        
        static class Api15Impl
        {
            private Api15Impl() {
            }
            
            static void setContentDescription(final RemoteViews remoteViews, final int n, final CharSequence charSequence) {
                remoteViews.setContentDescription(n, charSequence);
            }
        }
        
        static class Api16Impl
        {
            private Api16Impl() {
            }
            
            static Notification$Builder setStyle(final Notification$Builder notification$Builder, final Object o) {
                return notification$Builder.setStyle((Notification$Style)o);
            }
        }
        
        static class Api24Impl
        {
            private Api24Impl() {
            }
            
            static Notification$DecoratedCustomViewStyle createDecoratedCustomViewStyle() {
                return new Notification$DecoratedCustomViewStyle();
            }
        }
    }
    
    public interface Extender
    {
        NotificationCompat.Builder extend(final NotificationCompat.Builder p0);
    }
    
    @Retention(RetentionPolicy.SOURCE)
    public @interface GroupAlertBehavior {
    }
    
    public static class InboxStyle extends Style
    {
        private static final String TEMPLATE_CLASS_NAME = "androidx.core.app.NotificationCompat$InboxStyle";
        private ArrayList<CharSequence> mTexts;
        
        public InboxStyle() {
            this.mTexts = new ArrayList<CharSequence>();
        }
        
        public InboxStyle(final NotificationCompat.Builder builder) {
            this.mTexts = new ArrayList<CharSequence>();
            ((Style)this).setBuilder(builder);
        }
        
        public InboxStyle addLine(final CharSequence charSequence) {
            if (charSequence != null) {
                this.mTexts.add(NotificationCompat.Builder.limitCharSequenceLength(charSequence));
            }
            return this;
        }
        
        @Override
        public void apply(final NotificationBuilderWithBuilderAccessor notificationBuilderWithBuilderAccessor) {
            if (Build$VERSION.SDK_INT >= 16) {
                final Notification$InboxStyle setBigContentTitle = Api16Impl.setBigContentTitle(Api16Impl.createInboxStyle(notificationBuilderWithBuilderAccessor.getBuilder()), this.mBigContentTitle);
                if (this.mSummaryTextSet) {
                    Api16Impl.setSummaryText(setBigContentTitle, this.mSummaryText);
                }
                final Iterator<CharSequence> iterator = this.mTexts.iterator();
                while (iterator.hasNext()) {
                    Api16Impl.addLine(setBigContentTitle, iterator.next());
                }
            }
        }
        
        @Override
        protected void clearCompatExtraKeys(final Bundle bundle) {
            super.clearCompatExtraKeys(bundle);
            bundle.remove("android.textLines");
        }
        
        @Override
        protected String getClassName() {
            return "androidx.core.app.NotificationCompat$InboxStyle";
        }
        
        @Override
        protected void restoreFromCompatExtras(final Bundle bundle) {
            super.restoreFromCompatExtras(bundle);
            this.mTexts.clear();
            if (bundle.containsKey("android.textLines")) {
                Collections.addAll(this.mTexts, bundle.getCharSequenceArray("android.textLines"));
            }
        }
        
        public InboxStyle setBigContentTitle(final CharSequence charSequence) {
            this.mBigContentTitle = NotificationCompat.Builder.limitCharSequenceLength(charSequence);
            return this;
        }
        
        public InboxStyle setSummaryText(final CharSequence charSequence) {
            this.mSummaryText = NotificationCompat.Builder.limitCharSequenceLength(charSequence);
            this.mSummaryTextSet = true;
            return this;
        }
        
        static class Api16Impl
        {
            private Api16Impl() {
            }
            
            static Notification$InboxStyle addLine(final Notification$InboxStyle notification$InboxStyle, final CharSequence charSequence) {
                return notification$InboxStyle.addLine(charSequence);
            }
            
            static Notification$InboxStyle createInboxStyle(final Notification$Builder notification$Builder) {
                return new Notification$InboxStyle(notification$Builder);
            }
            
            static Notification$InboxStyle setBigContentTitle(final Notification$InboxStyle notification$InboxStyle, final CharSequence bigContentTitle) {
                return notification$InboxStyle.setBigContentTitle(bigContentTitle);
            }
            
            static Notification$InboxStyle setSummaryText(final Notification$InboxStyle notification$InboxStyle, final CharSequence summaryText) {
                return notification$InboxStyle.setSummaryText(summaryText);
            }
        }
    }
    
    public static class MessagingStyle extends Style
    {
        public static final int MAXIMUM_RETAINED_MESSAGES = 25;
        private static final String TEMPLATE_CLASS_NAME = "androidx.core.app.NotificationCompat$MessagingStyle";
        private CharSequence mConversationTitle;
        private final List<Message> mHistoricMessages;
        private Boolean mIsGroupConversation;
        private final List<Message> mMessages;
        private Person mUser;
        
        MessagingStyle() {
            this.mMessages = new ArrayList<Message>();
            this.mHistoricMessages = new ArrayList<Message>();
        }
        
        public MessagingStyle(final Person mUser) {
            this.mMessages = new ArrayList<Message>();
            this.mHistoricMessages = new ArrayList<Message>();
            if (!TextUtils.isEmpty(mUser.getName())) {
                this.mUser = mUser;
                return;
            }
            throw new IllegalArgumentException("User's name must not be empty.");
        }
        
        @Deprecated
        public MessagingStyle(final CharSequence name) {
            this.mMessages = new ArrayList<Message>();
            this.mHistoricMessages = new ArrayList<Message>();
            this.mUser = new Person.Builder().setName(name).build();
        }
        
        public static MessagingStyle extractMessagingStyleFromNotification(final Notification notification) {
            final Style styleFromNotification = Style.extractStyleFromNotification(notification);
            if (styleFromNotification instanceof MessagingStyle) {
                return (MessagingStyle)styleFromNotification;
            }
            return null;
        }
        
        private Message findLatestIncomingMessage() {
            for (int i = this.mMessages.size() - 1; i >= 0; --i) {
                final Message message = this.mMessages.get(i);
                if (message.getPerson() != null && !TextUtils.isEmpty(message.getPerson().getName())) {
                    return message;
                }
            }
            if (!this.mMessages.isEmpty()) {
                final List<Message> mMessages = this.mMessages;
                return mMessages.get(mMessages.size() - 1);
            }
            return null;
        }
        
        private boolean hasMessagesWithoutSender() {
            for (int i = this.mMessages.size() - 1; i >= 0; --i) {
                final Message message = this.mMessages.get(i);
                if (message.getPerson() != null && message.getPerson().getName() == null) {
                    return true;
                }
            }
            return false;
        }
        
        private TextAppearanceSpan makeFontColorSpan(final int n) {
            return new TextAppearanceSpan((String)null, 0, 0, ColorStateList.valueOf(n), (ColorStateList)null);
        }
        
        private CharSequence makeMessageLine(final Message message) {
            final BidiFormatter instance = BidiFormatter.getInstance();
            final SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder();
            final boolean b = Build$VERSION.SDK_INT >= 21;
            int n;
            if (b) {
                n = -16777216;
            }
            else {
                n = -1;
            }
            final Person person = message.getPerson();
            final String s = "";
            CharSequence name;
            if (person == null) {
                name = "";
            }
            else {
                name = message.getPerson().getName();
            }
            int color = n;
            CharSequence charSequence = name;
            if (TextUtils.isEmpty(name)) {
                final CharSequence name2 = this.mUser.getName();
                color = n;
                charSequence = name2;
                if (b) {
                    color = n;
                    charSequence = name2;
                    if (this.mBuilder.getColor() != 0) {
                        color = this.mBuilder.getColor();
                        charSequence = name2;
                    }
                }
            }
            final CharSequence unicodeWrap = instance.unicodeWrap(charSequence);
            spannableStringBuilder.append(unicodeWrap);
            spannableStringBuilder.setSpan((Object)this.makeFontColorSpan(color), spannableStringBuilder.length() - unicodeWrap.length(), spannableStringBuilder.length(), 33);
            CharSequence text;
            if (message.getText() == null) {
                text = s;
            }
            else {
                text = message.getText();
            }
            spannableStringBuilder.append((CharSequence)"  ").append(instance.unicodeWrap(text));
            return (CharSequence)spannableStringBuilder;
        }
        
        @Override
        public void addCompatExtras(final Bundle bundle) {
            super.addCompatExtras(bundle);
            bundle.putCharSequence("android.selfDisplayName", this.mUser.getName());
            bundle.putBundle("android.messagingStyleUser", this.mUser.toBundle());
            bundle.putCharSequence("android.hiddenConversationTitle", this.mConversationTitle);
            if (this.mConversationTitle != null && this.mIsGroupConversation) {
                bundle.putCharSequence("android.conversationTitle", this.mConversationTitle);
            }
            if (!this.mMessages.isEmpty()) {
                bundle.putParcelableArray("android.messages", (Parcelable[])Message.getBundleArrayForMessages(this.mMessages));
            }
            if (!this.mHistoricMessages.isEmpty()) {
                bundle.putParcelableArray("android.messages.historic", (Parcelable[])Message.getBundleArrayForMessages(this.mHistoricMessages));
            }
            final Boolean mIsGroupConversation = this.mIsGroupConversation;
            if (mIsGroupConversation != null) {
                bundle.putBoolean("android.isGroupConversation", (boolean)mIsGroupConversation);
            }
        }
        
        public MessagingStyle addHistoricMessage(final Message message) {
            if (message != null) {
                this.mHistoricMessages.add(message);
                if (this.mHistoricMessages.size() > 25) {
                    this.mHistoricMessages.remove(0);
                }
            }
            return this;
        }
        
        public MessagingStyle addMessage(final Message message) {
            if (message != null) {
                this.mMessages.add(message);
                if (this.mMessages.size() > 25) {
                    this.mMessages.remove(0);
                }
            }
            return this;
        }
        
        public MessagingStyle addMessage(final CharSequence charSequence, final long n, final Person person) {
            this.addMessage(new Message(charSequence, n, person));
            return this;
        }
        
        @Deprecated
        public MessagingStyle addMessage(final CharSequence charSequence, final long n, final CharSequence name) {
            this.mMessages.add(new Message(charSequence, n, new Person.Builder().setName(name).build()));
            if (this.mMessages.size() > 25) {
                this.mMessages.remove(0);
            }
            return this;
        }
        
        @Override
        public void apply(final NotificationBuilderWithBuilderAccessor notificationBuilderWithBuilderAccessor) {
            this.setGroupConversation(this.isGroupConversation());
            if (Build$VERSION.SDK_INT >= 24) {
                Notification$MessagingStyle notification$MessagingStyle;
                if (Build$VERSION.SDK_INT >= 28) {
                    notification$MessagingStyle = Api28Impl.createMessagingStyle(this.mUser.toAndroidPerson());
                }
                else {
                    notification$MessagingStyle = Api24Impl.createMessagingStyle(this.mUser.getName());
                }
                final Iterator<Message> iterator = this.mMessages.iterator();
                while (iterator.hasNext()) {
                    Api24Impl.addMessage(notification$MessagingStyle, iterator.next().toAndroidMessage());
                }
                if (Build$VERSION.SDK_INT >= 26) {
                    final Iterator<Message> iterator2 = this.mHistoricMessages.iterator();
                    while (iterator2.hasNext()) {
                        Api26Impl.addHistoricMessage(notification$MessagingStyle, iterator2.next().toAndroidMessage());
                    }
                }
                if (this.mIsGroupConversation || Build$VERSION.SDK_INT >= 28) {
                    Api24Impl.setConversationTitle(notification$MessagingStyle, this.mConversationTitle);
                }
                if (Build$VERSION.SDK_INT >= 28) {
                    Api28Impl.setGroupConversation(notification$MessagingStyle, this.mIsGroupConversation);
                }
                Api16Impl.setBuilder((Notification$Style)notification$MessagingStyle, notificationBuilderWithBuilderAccessor.getBuilder());
            }
            else {
                final Message latestIncomingMessage = this.findLatestIncomingMessage();
                if (this.mConversationTitle != null && this.mIsGroupConversation) {
                    notificationBuilderWithBuilderAccessor.getBuilder().setContentTitle(this.mConversationTitle);
                }
                else if (latestIncomingMessage != null) {
                    notificationBuilderWithBuilderAccessor.getBuilder().setContentTitle((CharSequence)"");
                    if (latestIncomingMessage.getPerson() != null) {
                        notificationBuilderWithBuilderAccessor.getBuilder().setContentTitle(latestIncomingMessage.getPerson().getName());
                    }
                }
                if (latestIncomingMessage != null) {
                    final Notification$Builder builder = notificationBuilderWithBuilderAccessor.getBuilder();
                    CharSequence contentText;
                    if (this.mConversationTitle != null) {
                        contentText = this.makeMessageLine(latestIncomingMessage);
                    }
                    else {
                        contentText = latestIncomingMessage.getText();
                    }
                    builder.setContentText(contentText);
                }
                if (Build$VERSION.SDK_INT >= 16) {
                    final SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder();
                    final boolean b = this.mConversationTitle != null || this.hasMessagesWithoutSender();
                    for (int i = this.mMessages.size() - 1; i >= 0; --i) {
                        final Message message = this.mMessages.get(i);
                        CharSequence charSequence;
                        if (b) {
                            charSequence = this.makeMessageLine(message);
                        }
                        else {
                            charSequence = message.getText();
                        }
                        if (i != this.mMessages.size() - 1) {
                            spannableStringBuilder.insert(0, (CharSequence)"\n");
                        }
                        spannableStringBuilder.insert(0, charSequence);
                    }
                    Api16Impl.bigText(Api16Impl.setBigContentTitle(Api16Impl.createBigTextStyle(notificationBuilderWithBuilderAccessor.getBuilder()), null), (CharSequence)spannableStringBuilder);
                }
            }
        }
        
        @Override
        protected void clearCompatExtraKeys(final Bundle bundle) {
            super.clearCompatExtraKeys(bundle);
            bundle.remove("android.messagingStyleUser");
            bundle.remove("android.selfDisplayName");
            bundle.remove("android.conversationTitle");
            bundle.remove("android.hiddenConversationTitle");
            bundle.remove("android.messages");
            bundle.remove("android.messages.historic");
            bundle.remove("android.isGroupConversation");
        }
        
        @Override
        protected String getClassName() {
            return "androidx.core.app.NotificationCompat$MessagingStyle";
        }
        
        public CharSequence getConversationTitle() {
            return this.mConversationTitle;
        }
        
        public List<Message> getHistoricMessages() {
            return this.mHistoricMessages;
        }
        
        public List<Message> getMessages() {
            return this.mMessages;
        }
        
        public Person getUser() {
            return this.mUser;
        }
        
        @Deprecated
        public CharSequence getUserDisplayName() {
            return this.mUser.getName();
        }
        
        public boolean isGroupConversation() {
            final NotificationCompat.Builder mBuilder = this.mBuilder;
            boolean booleanValue = false;
            final boolean b = false;
            if (mBuilder != null && this.mBuilder.mContext.getApplicationInfo().targetSdkVersion < 28 && this.mIsGroupConversation == null) {
                boolean b2 = b;
                if (this.mConversationTitle != null) {
                    b2 = true;
                }
                return b2;
            }
            final Boolean mIsGroupConversation = this.mIsGroupConversation;
            if (mIsGroupConversation != null) {
                booleanValue = mIsGroupConversation;
            }
            return booleanValue;
        }
        
        @Override
        protected void restoreFromCompatExtras(final Bundle bundle) {
            super.restoreFromCompatExtras(bundle);
            this.mMessages.clear();
            if (bundle.containsKey("android.messagingStyleUser")) {
                this.mUser = Person.fromBundle(bundle.getBundle("android.messagingStyleUser"));
            }
            else {
                this.mUser = new Person.Builder().setName(bundle.getString("android.selfDisplayName")).build();
            }
            final CharSequence charSequence = bundle.getCharSequence("android.conversationTitle");
            this.mConversationTitle = charSequence;
            if (charSequence == null) {
                this.mConversationTitle = bundle.getCharSequence("android.hiddenConversationTitle");
            }
            final Parcelable[] parcelableArray = bundle.getParcelableArray("android.messages");
            if (parcelableArray != null) {
                this.mMessages.addAll(Message.getMessagesFromBundleArray(parcelableArray));
            }
            final Parcelable[] parcelableArray2 = bundle.getParcelableArray("android.messages.historic");
            if (parcelableArray2 != null) {
                this.mHistoricMessages.addAll(Message.getMessagesFromBundleArray(parcelableArray2));
            }
            if (bundle.containsKey("android.isGroupConversation")) {
                this.mIsGroupConversation = bundle.getBoolean("android.isGroupConversation");
            }
        }
        
        public MessagingStyle setConversationTitle(final CharSequence mConversationTitle) {
            this.mConversationTitle = mConversationTitle;
            return this;
        }
        
        public MessagingStyle setGroupConversation(final boolean b) {
            this.mIsGroupConversation = b;
            return this;
        }
        
        static class Api16Impl
        {
            private Api16Impl() {
            }
            
            static Notification$BigTextStyle bigText(final Notification$BigTextStyle notification$BigTextStyle, final CharSequence charSequence) {
                return notification$BigTextStyle.bigText(charSequence);
            }
            
            static Notification$BigTextStyle createBigTextStyle(final Notification$Builder notification$Builder) {
                return new Notification$BigTextStyle(notification$Builder);
            }
            
            static Notification$BigTextStyle setBigContentTitle(final Notification$BigTextStyle notification$BigTextStyle, final CharSequence bigContentTitle) {
                return notification$BigTextStyle.setBigContentTitle(bigContentTitle);
            }
            
            static void setBuilder(final Notification$Style notification$Style, final Notification$Builder builder) {
                notification$Style.setBuilder(builder);
            }
        }
        
        static class Api24Impl
        {
            private Api24Impl() {
            }
            
            static Notification$MessagingStyle addMessage(final Notification$MessagingStyle notification$MessagingStyle, final Notification$MessagingStyle$Message notification$MessagingStyle$Message) {
                return notification$MessagingStyle.addMessage(notification$MessagingStyle$Message);
            }
            
            static Notification$MessagingStyle createMessagingStyle(final CharSequence charSequence) {
                return new Notification$MessagingStyle(charSequence);
            }
            
            static Notification$MessagingStyle setConversationTitle(final Notification$MessagingStyle notification$MessagingStyle, final CharSequence conversationTitle) {
                return notification$MessagingStyle.setConversationTitle(conversationTitle);
            }
        }
        
        static class Api26Impl
        {
            private Api26Impl() {
            }
            
            static Notification$MessagingStyle addHistoricMessage(final Notification$MessagingStyle notification$MessagingStyle, final Notification$MessagingStyle$Message notification$MessagingStyle$Message) {
                return notification$MessagingStyle.addHistoricMessage(notification$MessagingStyle$Message);
            }
        }
        
        static class Api28Impl
        {
            private Api28Impl() {
            }
            
            static Notification$MessagingStyle createMessagingStyle(final android.app.Person person) {
                return new Notification$MessagingStyle(person);
            }
            
            static Notification$MessagingStyle setGroupConversation(final Notification$MessagingStyle notification$MessagingStyle, final boolean groupConversation) {
                return notification$MessagingStyle.setGroupConversation(groupConversation);
            }
        }
        
        public static final class Message
        {
            static final String KEY_DATA_MIME_TYPE = "type";
            static final String KEY_DATA_URI = "uri";
            static final String KEY_EXTRAS_BUNDLE = "extras";
            static final String KEY_NOTIFICATION_PERSON = "sender_person";
            static final String KEY_PERSON = "person";
            static final String KEY_SENDER = "sender";
            static final String KEY_TEXT = "text";
            static final String KEY_TIMESTAMP = "time";
            private String mDataMimeType;
            private Uri mDataUri;
            private Bundle mExtras;
            private final Person mPerson;
            private final CharSequence mText;
            private final long mTimestamp;
            
            public Message(final CharSequence mText, final long mTimestamp, final Person mPerson) {
                this.mExtras = new Bundle();
                this.mText = mText;
                this.mTimestamp = mTimestamp;
                this.mPerson = mPerson;
            }
            
            @Deprecated
            public Message(final CharSequence charSequence, final long n, final CharSequence name) {
                this(charSequence, n, new Person.Builder().setName(name).build());
            }
            
            static Bundle[] getBundleArrayForMessages(final List<Message> list) {
                final Bundle[] array = new Bundle[list.size()];
                for (int size = list.size(), i = 0; i < size; ++i) {
                    array[i] = ((Message)list.get(i)).toBundle();
                }
                return array;
            }
            
            static Message getMessageFromBundle(final Bundle bundle) {
                try {
                    if (bundle.containsKey("text")) {
                        if (bundle.containsKey("time")) {
                            Person person;
                            if (bundle.containsKey("person")) {
                                person = Person.fromBundle(bundle.getBundle("person"));
                            }
                            else if (bundle.containsKey("sender_person") && Build$VERSION.SDK_INT >= 28) {
                                person = Person.fromAndroidPerson((android.app.Person)bundle.getParcelable("sender_person"));
                            }
                            else if (bundle.containsKey("sender")) {
                                person = new Person.Builder().setName(bundle.getCharSequence("sender")).build();
                            }
                            else {
                                person = null;
                            }
                            final Message message = new Message(bundle.getCharSequence("text"), bundle.getLong("time"), person);
                            if (bundle.containsKey("type") && bundle.containsKey("uri")) {
                                message.setData(bundle.getString("type"), (Uri)bundle.getParcelable("uri"));
                            }
                            if (bundle.containsKey("extras")) {
                                message.getExtras().putAll(bundle.getBundle("extras"));
                            }
                            return message;
                        }
                    }
                    return null;
                }
                catch (final ClassCastException ex) {
                    return null;
                }
            }
            
            static List<Message> getMessagesFromBundleArray(final Parcelable[] array) {
                final ArrayList list = new ArrayList(array.length);
                for (int i = 0; i < array.length; ++i) {
                    final Parcelable parcelable = array[i];
                    if (parcelable instanceof Bundle) {
                        final Message messageFromBundle = getMessageFromBundle((Bundle)parcelable);
                        if (messageFromBundle != null) {
                            list.add(messageFromBundle);
                        }
                    }
                }
                return list;
            }
            
            private Bundle toBundle() {
                final Bundle bundle = new Bundle();
                final CharSequence mText = this.mText;
                if (mText != null) {
                    bundle.putCharSequence("text", mText);
                }
                bundle.putLong("time", this.mTimestamp);
                final Person mPerson = this.mPerson;
                if (mPerson != null) {
                    bundle.putCharSequence("sender", mPerson.getName());
                    if (Build$VERSION.SDK_INT >= 28) {
                        bundle.putParcelable("sender_person", Api28Impl.castToParcelable(this.mPerson.toAndroidPerson()));
                    }
                    else {
                        bundle.putBundle("person", this.mPerson.toBundle());
                    }
                }
                final String mDataMimeType = this.mDataMimeType;
                if (mDataMimeType != null) {
                    bundle.putString("type", mDataMimeType);
                }
                final Uri mDataUri = this.mDataUri;
                if (mDataUri != null) {
                    bundle.putParcelable("uri", (Parcelable)mDataUri);
                }
                final Bundle mExtras = this.mExtras;
                if (mExtras != null) {
                    bundle.putBundle("extras", mExtras);
                }
                return bundle;
            }
            
            public String getDataMimeType() {
                return this.mDataMimeType;
            }
            
            public Uri getDataUri() {
                return this.mDataUri;
            }
            
            public Bundle getExtras() {
                return this.mExtras;
            }
            
            public Person getPerson() {
                return this.mPerson;
            }
            
            @Deprecated
            public CharSequence getSender() {
                final Person mPerson = this.mPerson;
                CharSequence name;
                if (mPerson == null) {
                    name = null;
                }
                else {
                    name = mPerson.getName();
                }
                return name;
            }
            
            public CharSequence getText() {
                return this.mText;
            }
            
            public long getTimestamp() {
                return this.mTimestamp;
            }
            
            public Message setData(final String mDataMimeType, final Uri mDataUri) {
                this.mDataMimeType = mDataMimeType;
                this.mDataUri = mDataUri;
                return this;
            }
            
            Notification$MessagingStyle$Message toAndroidMessage() {
                final Person person = this.getPerson();
                final int sdk_INT = Build$VERSION.SDK_INT;
                final CharSequence charSequence = null;
                android.app.Person androidPerson = null;
                Notification$MessagingStyle$Message notification$MessagingStyle$Message;
                if (sdk_INT >= 28) {
                    final CharSequence text = this.getText();
                    final long timestamp = this.getTimestamp();
                    if (person != null) {
                        androidPerson = person.toAndroidPerson();
                    }
                    notification$MessagingStyle$Message = Api28Impl.createMessage(text, timestamp, androidPerson);
                }
                else {
                    final CharSequence text2 = this.getText();
                    final long timestamp2 = this.getTimestamp();
                    CharSequence name;
                    if (person == null) {
                        name = charSequence;
                    }
                    else {
                        name = person.getName();
                    }
                    notification$MessagingStyle$Message = Api24Impl.createMessage(text2, timestamp2, name);
                }
                if (this.getDataMimeType() != null) {
                    Api24Impl.setData(notification$MessagingStyle$Message, this.getDataMimeType(), this.getDataUri());
                }
                return notification$MessagingStyle$Message;
            }
            
            static class Api24Impl
            {
                private Api24Impl() {
                }
                
                static Notification$MessagingStyle$Message createMessage(final CharSequence charSequence, final long n, final CharSequence charSequence2) {
                    return new Notification$MessagingStyle$Message(charSequence, n, charSequence2);
                }
                
                static Notification$MessagingStyle$Message setData(final Notification$MessagingStyle$Message notification$MessagingStyle$Message, final String s, final Uri uri) {
                    return notification$MessagingStyle$Message.setData(s, uri);
                }
            }
            
            static class Api28Impl
            {
                private Api28Impl() {
                }
                
                static Parcelable castToParcelable(final android.app.Person person) {
                    return (Parcelable)person;
                }
                
                static Notification$MessagingStyle$Message createMessage(final CharSequence charSequence, final long n, final android.app.Person person) {
                    return new Notification$MessagingStyle$Message(charSequence, n, person);
                }
            }
        }
    }
    
    @Retention(RetentionPolicy.SOURCE)
    public @interface NotificationVisibility {
    }
    
    @Retention(RetentionPolicy.SOURCE)
    public @interface ServiceNotificationBehavior {
    }
    
    @Retention(RetentionPolicy.SOURCE)
    public @interface StreamType {
    }
    
    public static final class TvExtender implements NotificationCompat.Extender
    {
        static final String EXTRA_CHANNEL_ID = "channel_id";
        static final String EXTRA_CONTENT_INTENT = "content_intent";
        static final String EXTRA_DELETE_INTENT = "delete_intent";
        private static final String EXTRA_FLAGS = "flags";
        static final String EXTRA_SUPPRESS_SHOW_OVER_APPS = "suppressShowOverApps";
        static final String EXTRA_TV_EXTENDER = "android.tv.EXTENSIONS";
        private static final int FLAG_AVAILABLE_ON_TV = 1;
        private static final String TAG = "TvExtender";
        private String mChannelId;
        private PendingIntent mContentIntent;
        private PendingIntent mDeleteIntent;
        private int mFlags;
        private boolean mSuppressShowOverApps;
        
        public TvExtender() {
            this.mFlags = 1;
        }
        
        public TvExtender(final Notification notification) {
            if (Build$VERSION.SDK_INT < 26) {
                return;
            }
            Bundle bundle;
            if (notification.extras == null) {
                bundle = null;
            }
            else {
                bundle = notification.extras.getBundle("android.tv.EXTENSIONS");
            }
            if (bundle != null) {
                this.mFlags = bundle.getInt("flags");
                this.mChannelId = bundle.getString("channel_id");
                this.mSuppressShowOverApps = bundle.getBoolean("suppressShowOverApps");
                this.mContentIntent = (PendingIntent)bundle.getParcelable("content_intent");
                this.mDeleteIntent = (PendingIntent)bundle.getParcelable("delete_intent");
            }
        }
        
        @Override
        public NotificationCompat.Builder extend(final NotificationCompat.Builder builder) {
            if (Build$VERSION.SDK_INT < 26) {
                return builder;
            }
            final Bundle bundle = new Bundle();
            bundle.putInt("flags", this.mFlags);
            bundle.putString("channel_id", this.mChannelId);
            bundle.putBoolean("suppressShowOverApps", this.mSuppressShowOverApps);
            final PendingIntent mContentIntent = this.mContentIntent;
            if (mContentIntent != null) {
                bundle.putParcelable("content_intent", (Parcelable)mContentIntent);
            }
            final PendingIntent mDeleteIntent = this.mDeleteIntent;
            if (mDeleteIntent != null) {
                bundle.putParcelable("delete_intent", (Parcelable)mDeleteIntent);
            }
            builder.getExtras().putBundle("android.tv.EXTENSIONS", bundle);
            return builder;
        }
        
        public String getChannelId() {
            return this.mChannelId;
        }
        
        public PendingIntent getContentIntent() {
            return this.mContentIntent;
        }
        
        public PendingIntent getDeleteIntent() {
            return this.mDeleteIntent;
        }
        
        public boolean isAvailableOnTv() {
            final int mFlags = this.mFlags;
            boolean b = true;
            if ((mFlags & 0x1) == 0x0) {
                b = false;
            }
            return b;
        }
        
        public boolean isSuppressShowOverApps() {
            return this.mSuppressShowOverApps;
        }
        
        public TvExtender setChannelId(final String mChannelId) {
            this.mChannelId = mChannelId;
            return this;
        }
        
        public TvExtender setContentIntent(final PendingIntent mContentIntent) {
            this.mContentIntent = mContentIntent;
            return this;
        }
        
        public TvExtender setDeleteIntent(final PendingIntent mDeleteIntent) {
            this.mDeleteIntent = mDeleteIntent;
            return this;
        }
        
        public TvExtender setSuppressShowOverApps(final boolean mSuppressShowOverApps) {
            this.mSuppressShowOverApps = mSuppressShowOverApps;
            return this;
        }
    }
    
    public static final class WearableExtender implements NotificationCompat.Extender
    {
        private static final int DEFAULT_CONTENT_ICON_GRAVITY = 8388613;
        private static final int DEFAULT_FLAGS = 1;
        private static final int DEFAULT_GRAVITY = 80;
        private static final String EXTRA_WEARABLE_EXTENSIONS = "android.wearable.EXTENSIONS";
        private static final int FLAG_BIG_PICTURE_AMBIENT = 32;
        private static final int FLAG_CONTENT_INTENT_AVAILABLE_OFFLINE = 1;
        private static final int FLAG_HINT_AVOID_BACKGROUND_CLIPPING = 16;
        private static final int FLAG_HINT_CONTENT_INTENT_LAUNCHES_ACTIVITY = 64;
        private static final int FLAG_HINT_HIDE_ICON = 2;
        private static final int FLAG_HINT_SHOW_BACKGROUND_ONLY = 4;
        private static final int FLAG_START_SCROLL_BOTTOM = 8;
        private static final String KEY_ACTIONS = "actions";
        private static final String KEY_BACKGROUND = "background";
        private static final String KEY_BRIDGE_TAG = "bridgeTag";
        private static final String KEY_CONTENT_ACTION_INDEX = "contentActionIndex";
        private static final String KEY_CONTENT_ICON = "contentIcon";
        private static final String KEY_CONTENT_ICON_GRAVITY = "contentIconGravity";
        private static final String KEY_CUSTOM_CONTENT_HEIGHT = "customContentHeight";
        private static final String KEY_CUSTOM_SIZE_PRESET = "customSizePreset";
        private static final String KEY_DISMISSAL_ID = "dismissalId";
        private static final String KEY_DISPLAY_INTENT = "displayIntent";
        private static final String KEY_FLAGS = "flags";
        private static final String KEY_GRAVITY = "gravity";
        private static final String KEY_HINT_SCREEN_TIMEOUT = "hintScreenTimeout";
        private static final String KEY_PAGES = "pages";
        @Deprecated
        public static final int SCREEN_TIMEOUT_LONG = -1;
        @Deprecated
        public static final int SCREEN_TIMEOUT_SHORT = 0;
        @Deprecated
        public static final int SIZE_DEFAULT = 0;
        @Deprecated
        public static final int SIZE_FULL_SCREEN = 5;
        @Deprecated
        public static final int SIZE_LARGE = 4;
        @Deprecated
        public static final int SIZE_MEDIUM = 3;
        @Deprecated
        public static final int SIZE_SMALL = 2;
        @Deprecated
        public static final int SIZE_XSMALL = 1;
        public static final int UNSET_ACTION_INDEX = -1;
        private ArrayList<Action> mActions;
        private Bitmap mBackground;
        private String mBridgeTag;
        private int mContentActionIndex;
        private int mContentIcon;
        private int mContentIconGravity;
        private int mCustomContentHeight;
        private int mCustomSizePreset;
        private String mDismissalId;
        private PendingIntent mDisplayIntent;
        private int mFlags;
        private int mGravity;
        private int mHintScreenTimeout;
        private ArrayList<Notification> mPages;
        
        public WearableExtender() {
            this.mActions = new ArrayList<Action>();
            this.mFlags = 1;
            this.mPages = new ArrayList<Notification>();
            this.mContentIconGravity = 8388613;
            this.mContentActionIndex = -1;
            this.mCustomSizePreset = 0;
            this.mGravity = 80;
        }
        
        public WearableExtender(final Notification notification) {
            this.mActions = new ArrayList<Action>();
            this.mFlags = 1;
            this.mPages = new ArrayList<Notification>();
            this.mContentIconGravity = 8388613;
            this.mContentActionIndex = -1;
            this.mCustomSizePreset = 0;
            this.mGravity = 80;
            final Bundle extras = NotificationCompat.getExtras(notification);
            Bundle bundle;
            if (extras != null) {
                bundle = extras.getBundle("android.wearable.EXTENSIONS");
            }
            else {
                bundle = null;
            }
            if (bundle != null) {
                final ArrayList parcelableArrayList = bundle.getParcelableArrayList("actions");
                if (Build$VERSION.SDK_INT >= 16 && parcelableArrayList != null) {
                    final int size = parcelableArrayList.size();
                    final Action[] elements = new Action[size];
                    for (int i = 0; i < size; ++i) {
                        if (Build$VERSION.SDK_INT >= 20) {
                            elements[i] = Api20Impl.getActionCompatFromAction(parcelableArrayList, i);
                        }
                        else if (Build$VERSION.SDK_INT >= 16) {
                            elements[i] = NotificationCompatJellybean.getActionFromBundle((Bundle)parcelableArrayList.get(i));
                        }
                    }
                    Collections.addAll(this.mActions, elements);
                }
                this.mFlags = bundle.getInt("flags", 1);
                this.mDisplayIntent = (PendingIntent)bundle.getParcelable("displayIntent");
                final Notification[] notificationArrayFromBundle = NotificationCompat.getNotificationArrayFromBundle(bundle, "pages");
                if (notificationArrayFromBundle != null) {
                    Collections.addAll(this.mPages, notificationArrayFromBundle);
                }
                this.mBackground = (Bitmap)bundle.getParcelable("background");
                this.mContentIcon = bundle.getInt("contentIcon");
                this.mContentIconGravity = bundle.getInt("contentIconGravity", 8388613);
                this.mContentActionIndex = bundle.getInt("contentActionIndex", -1);
                this.mCustomSizePreset = bundle.getInt("customSizePreset", 0);
                this.mCustomContentHeight = bundle.getInt("customContentHeight");
                this.mGravity = bundle.getInt("gravity", 80);
                this.mHintScreenTimeout = bundle.getInt("hintScreenTimeout");
                this.mDismissalId = bundle.getString("dismissalId");
                this.mBridgeTag = bundle.getString("bridgeTag");
            }
        }
        
        private static Notification$Action getActionFromActionCompat(final Action action) {
            final int sdk_INT = Build$VERSION.SDK_INT;
            final int n = 0;
            Notification$Action$Builder notification$Action$Builder;
            if (sdk_INT >= 23) {
                final IconCompat iconCompat = action.getIconCompat();
                Icon icon;
                if (iconCompat == null) {
                    icon = null;
                }
                else {
                    icon = iconCompat.toIcon();
                }
                notification$Action$Builder = Api23Impl.createBuilder(icon, action.getTitle(), action.getActionIntent());
            }
            else {
                final IconCompat iconCompat2 = action.getIconCompat();
                int resId;
                if (iconCompat2 != null && iconCompat2.getType() == 2) {
                    resId = iconCompat2.getResId();
                }
                else {
                    resId = 0;
                }
                notification$Action$Builder = Api20Impl.createBuilder(resId, action.getTitle(), action.getActionIntent());
            }
            Bundle bundle;
            if (action.getExtras() != null) {
                bundle = new Bundle(action.getExtras());
            }
            else {
                bundle = new Bundle();
            }
            bundle.putBoolean("android.support.allowGeneratedReplies", action.getAllowGeneratedReplies());
            if (Build$VERSION.SDK_INT >= 24) {
                Api24Impl.setAllowGeneratedReplies(notification$Action$Builder, action.getAllowGeneratedReplies());
            }
            if (Build$VERSION.SDK_INT >= 31) {
                Api31Impl.setAuthenticationRequired(notification$Action$Builder, action.isAuthenticationRequired());
            }
            Api20Impl.addExtras(notification$Action$Builder, bundle);
            final RemoteInput[] remoteInputs = action.getRemoteInputs();
            if (remoteInputs != null) {
                final android.app.RemoteInput[] fromCompat = RemoteInput.fromCompat(remoteInputs);
                for (int length = fromCompat.length, i = n; i < length; ++i) {
                    Api20Impl.addRemoteInput(notification$Action$Builder, fromCompat[i]);
                }
            }
            return Api20Impl.build(notification$Action$Builder);
        }
        
        private void setFlag(final int n, final boolean b) {
            if (b) {
                this.mFlags |= n;
            }
            else {
                this.mFlags &= ~n;
            }
        }
        
        public WearableExtender addAction(final Action e) {
            this.mActions.add(e);
            return this;
        }
        
        public WearableExtender addActions(final List<Action> c) {
            this.mActions.addAll(c);
            return this;
        }
        
        @Deprecated
        public WearableExtender addPage(final Notification e) {
            this.mPages.add(e);
            return this;
        }
        
        @Deprecated
        public WearableExtender addPages(final List<Notification> c) {
            this.mPages.addAll(c);
            return this;
        }
        
        public WearableExtender clearActions() {
            this.mActions.clear();
            return this;
        }
        
        @Deprecated
        public WearableExtender clearPages() {
            this.mPages.clear();
            return this;
        }
        
        public WearableExtender clone() {
            final WearableExtender wearableExtender = new WearableExtender();
            wearableExtender.mActions = new ArrayList<Action>(this.mActions);
            wearableExtender.mFlags = this.mFlags;
            wearableExtender.mDisplayIntent = this.mDisplayIntent;
            wearableExtender.mPages = new ArrayList<Notification>(this.mPages);
            wearableExtender.mBackground = this.mBackground;
            wearableExtender.mContentIcon = this.mContentIcon;
            wearableExtender.mContentIconGravity = this.mContentIconGravity;
            wearableExtender.mContentActionIndex = this.mContentActionIndex;
            wearableExtender.mCustomSizePreset = this.mCustomSizePreset;
            wearableExtender.mCustomContentHeight = this.mCustomContentHeight;
            wearableExtender.mGravity = this.mGravity;
            wearableExtender.mHintScreenTimeout = this.mHintScreenTimeout;
            wearableExtender.mDismissalId = this.mDismissalId;
            wearableExtender.mBridgeTag = this.mBridgeTag;
            return wearableExtender;
        }
        
        @Override
        public NotificationCompat.Builder extend(final NotificationCompat.Builder builder) {
            final Bundle bundle = new Bundle();
            if (!this.mActions.isEmpty()) {
                if (Build$VERSION.SDK_INT >= 16) {
                    final ArrayList list = new ArrayList(this.mActions.size());
                    for (final Action action : this.mActions) {
                        if (Build$VERSION.SDK_INT >= 20) {
                            list.add(getActionFromActionCompat(action));
                        }
                        else {
                            if (Build$VERSION.SDK_INT < 16) {
                                continue;
                            }
                            list.add(NotificationCompatJellybean.getBundleForAction(action));
                        }
                    }
                    bundle.putParcelableArrayList("actions", list);
                }
                else {
                    bundle.putParcelableArrayList("actions", (ArrayList)null);
                }
            }
            final int mFlags = this.mFlags;
            if (mFlags != 1) {
                bundle.putInt("flags", mFlags);
            }
            final PendingIntent mDisplayIntent = this.mDisplayIntent;
            if (mDisplayIntent != null) {
                bundle.putParcelable("displayIntent", (Parcelable)mDisplayIntent);
            }
            if (!this.mPages.isEmpty()) {
                final ArrayList<Notification> mPages = this.mPages;
                bundle.putParcelableArray("pages", (Parcelable[])mPages.toArray((Parcelable[])new Notification[mPages.size()]));
            }
            final Bitmap mBackground = this.mBackground;
            if (mBackground != null) {
                bundle.putParcelable("background", (Parcelable)mBackground);
            }
            final int mContentIcon = this.mContentIcon;
            if (mContentIcon != 0) {
                bundle.putInt("contentIcon", mContentIcon);
            }
            final int mContentIconGravity = this.mContentIconGravity;
            if (mContentIconGravity != 8388613) {
                bundle.putInt("contentIconGravity", mContentIconGravity);
            }
            final int mContentActionIndex = this.mContentActionIndex;
            if (mContentActionIndex != -1) {
                bundle.putInt("contentActionIndex", mContentActionIndex);
            }
            final int mCustomSizePreset = this.mCustomSizePreset;
            if (mCustomSizePreset != 0) {
                bundle.putInt("customSizePreset", mCustomSizePreset);
            }
            final int mCustomContentHeight = this.mCustomContentHeight;
            if (mCustomContentHeight != 0) {
                bundle.putInt("customContentHeight", mCustomContentHeight);
            }
            final int mGravity = this.mGravity;
            if (mGravity != 80) {
                bundle.putInt("gravity", mGravity);
            }
            final int mHintScreenTimeout = this.mHintScreenTimeout;
            if (mHintScreenTimeout != 0) {
                bundle.putInt("hintScreenTimeout", mHintScreenTimeout);
            }
            final String mDismissalId = this.mDismissalId;
            if (mDismissalId != null) {
                bundle.putString("dismissalId", mDismissalId);
            }
            final String mBridgeTag = this.mBridgeTag;
            if (mBridgeTag != null) {
                bundle.putString("bridgeTag", mBridgeTag);
            }
            builder.getExtras().putBundle("android.wearable.EXTENSIONS", bundle);
            return builder;
        }
        
        public List<Action> getActions() {
            return this.mActions;
        }
        
        @Deprecated
        public Bitmap getBackground() {
            return this.mBackground;
        }
        
        public String getBridgeTag() {
            return this.mBridgeTag;
        }
        
        public int getContentAction() {
            return this.mContentActionIndex;
        }
        
        @Deprecated
        public int getContentIcon() {
            return this.mContentIcon;
        }
        
        @Deprecated
        public int getContentIconGravity() {
            return this.mContentIconGravity;
        }
        
        public boolean getContentIntentAvailableOffline() {
            final int mFlags = this.mFlags;
            boolean b = true;
            if ((mFlags & 0x1) == 0x0) {
                b = false;
            }
            return b;
        }
        
        @Deprecated
        public int getCustomContentHeight() {
            return this.mCustomContentHeight;
        }
        
        @Deprecated
        public int getCustomSizePreset() {
            return this.mCustomSizePreset;
        }
        
        public String getDismissalId() {
            return this.mDismissalId;
        }
        
        @Deprecated
        public PendingIntent getDisplayIntent() {
            return this.mDisplayIntent;
        }
        
        @Deprecated
        public int getGravity() {
            return this.mGravity;
        }
        
        @Deprecated
        public boolean getHintAmbientBigPicture() {
            return (this.mFlags & 0x20) != 0x0;
        }
        
        @Deprecated
        public boolean getHintAvoidBackgroundClipping() {
            return (this.mFlags & 0x10) != 0x0;
        }
        
        public boolean getHintContentIntentLaunchesActivity() {
            return (this.mFlags & 0x40) != 0x0;
        }
        
        @Deprecated
        public boolean getHintHideIcon() {
            return (this.mFlags & 0x2) != 0x0;
        }
        
        @Deprecated
        public int getHintScreenTimeout() {
            return this.mHintScreenTimeout;
        }
        
        @Deprecated
        public boolean getHintShowBackgroundOnly() {
            return (this.mFlags & 0x4) != 0x0;
        }
        
        @Deprecated
        public List<Notification> getPages() {
            return this.mPages;
        }
        
        public boolean getStartScrollBottom() {
            return (this.mFlags & 0x8) != 0x0;
        }
        
        @Deprecated
        public WearableExtender setBackground(final Bitmap mBackground) {
            this.mBackground = mBackground;
            return this;
        }
        
        public WearableExtender setBridgeTag(final String mBridgeTag) {
            this.mBridgeTag = mBridgeTag;
            return this;
        }
        
        public WearableExtender setContentAction(final int mContentActionIndex) {
            this.mContentActionIndex = mContentActionIndex;
            return this;
        }
        
        @Deprecated
        public WearableExtender setContentIcon(final int mContentIcon) {
            this.mContentIcon = mContentIcon;
            return this;
        }
        
        @Deprecated
        public WearableExtender setContentIconGravity(final int mContentIconGravity) {
            this.mContentIconGravity = mContentIconGravity;
            return this;
        }
        
        public WearableExtender setContentIntentAvailableOffline(final boolean b) {
            this.setFlag(1, b);
            return this;
        }
        
        @Deprecated
        public WearableExtender setCustomContentHeight(final int mCustomContentHeight) {
            this.mCustomContentHeight = mCustomContentHeight;
            return this;
        }
        
        @Deprecated
        public WearableExtender setCustomSizePreset(final int mCustomSizePreset) {
            this.mCustomSizePreset = mCustomSizePreset;
            return this;
        }
        
        public WearableExtender setDismissalId(final String mDismissalId) {
            this.mDismissalId = mDismissalId;
            return this;
        }
        
        @Deprecated
        public WearableExtender setDisplayIntent(final PendingIntent mDisplayIntent) {
            this.mDisplayIntent = mDisplayIntent;
            return this;
        }
        
        @Deprecated
        public WearableExtender setGravity(final int mGravity) {
            this.mGravity = mGravity;
            return this;
        }
        
        @Deprecated
        public WearableExtender setHintAmbientBigPicture(final boolean b) {
            this.setFlag(32, b);
            return this;
        }
        
        @Deprecated
        public WearableExtender setHintAvoidBackgroundClipping(final boolean b) {
            this.setFlag(16, b);
            return this;
        }
        
        public WearableExtender setHintContentIntentLaunchesActivity(final boolean b) {
            this.setFlag(64, b);
            return this;
        }
        
        @Deprecated
        public WearableExtender setHintHideIcon(final boolean b) {
            this.setFlag(2, b);
            return this;
        }
        
        @Deprecated
        public WearableExtender setHintScreenTimeout(final int mHintScreenTimeout) {
            this.mHintScreenTimeout = mHintScreenTimeout;
            return this;
        }
        
        @Deprecated
        public WearableExtender setHintShowBackgroundOnly(final boolean b) {
            this.setFlag(4, b);
            return this;
        }
        
        public WearableExtender setStartScrollBottom(final boolean b) {
            this.setFlag(8, b);
            return this;
        }
        
        static class Api20Impl
        {
            private Api20Impl() {
            }
            
            static Notification$Action$Builder addExtras(final Notification$Action$Builder notification$Action$Builder, final Bundle bundle) {
                return notification$Action$Builder.addExtras(bundle);
            }
            
            static Notification$Action$Builder addRemoteInput(final Notification$Action$Builder notification$Action$Builder, final android.app.RemoteInput remoteInput) {
                return notification$Action$Builder.addRemoteInput(remoteInput);
            }
            
            static Notification$Action build(final Notification$Action$Builder notification$Action$Builder) {
                return notification$Action$Builder.build();
            }
            
            static Notification$Action$Builder createBuilder(final int n, final CharSequence charSequence, final PendingIntent pendingIntent) {
                return new Notification$Action$Builder(n, charSequence, pendingIntent);
            }
            
            public static Action getActionCompatFromAction(final ArrayList<Parcelable> list, final int index) {
                return NotificationCompat.getActionCompatFromAction((Notification$Action)list.get(index));
            }
        }
        
        static class Api23Impl
        {
            private Api23Impl() {
            }
            
            static Notification$Action$Builder createBuilder(final Icon icon, final CharSequence charSequence, final PendingIntent pendingIntent) {
                return new Notification$Action$Builder(icon, charSequence, pendingIntent);
            }
        }
        
        static class Api24Impl
        {
            private Api24Impl() {
            }
            
            static Notification$Action$Builder setAllowGeneratedReplies(final Notification$Action$Builder notification$Action$Builder, final boolean allowGeneratedReplies) {
                return notification$Action$Builder.setAllowGeneratedReplies(allowGeneratedReplies);
            }
        }
        
        static class Api31Impl
        {
            private Api31Impl() {
            }
            
            static Notification$Action$Builder setAuthenticationRequired(final Notification$Action$Builder notification$Action$Builder, final boolean authenticationRequired) {
                return notification$Action$Builder.setAuthenticationRequired(authenticationRequired);
            }
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.app;

import android.os.IBinder;
import android.os.Bundle;

@Deprecated
public final class BundleCompat
{
    private BundleCompat() {
    }
    
    public static IBinder getBinder(final Bundle bundle, final String s) {
        return androidx.core.os.BundleCompat.getBinder(bundle, s);
    }
    
    public static void putBinder(final Bundle bundle, final String s, final IBinder binder) {
        androidx.core.os.BundleCompat.putBinder(bundle, s, binder);
    }
}

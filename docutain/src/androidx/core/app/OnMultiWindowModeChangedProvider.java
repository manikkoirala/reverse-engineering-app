// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.app;

import androidx.core.util.Consumer;

public interface OnMultiWindowModeChangedProvider
{
    void addOnMultiWindowModeChangedListener(final Consumer<MultiWindowModeChangedInfo> p0);
    
    void removeOnMultiWindowModeChangedListener(final Consumer<MultiWindowModeChangedInfo> p0);
}

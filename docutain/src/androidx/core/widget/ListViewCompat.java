// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.widget;

import android.view.View;
import android.os.Build$VERSION;
import android.widget.ListView;

public final class ListViewCompat
{
    private ListViewCompat() {
    }
    
    public static boolean canScrollList(final ListView listView, int n) {
        if (Build$VERSION.SDK_INT >= 19) {
            return Api19Impl.canScrollList(listView, n);
        }
        final int childCount = listView.getChildCount();
        boolean b = false;
        final boolean b2 = false;
        if (childCount == 0) {
            return false;
        }
        final int firstVisiblePosition = listView.getFirstVisiblePosition();
        if (n > 0) {
            n = listView.getChildAt(childCount - 1).getBottom();
            if (firstVisiblePosition + childCount >= listView.getCount()) {
                final boolean b3 = b2;
                if (n <= listView.getHeight() - listView.getListPaddingBottom()) {
                    return b3;
                }
            }
            return true;
        }
        n = listView.getChildAt(0).getTop();
        if (firstVisiblePosition > 0 || n < listView.getListPaddingTop()) {
            b = true;
        }
        return b;
    }
    
    public static void scrollListBy(final ListView listView, final int n) {
        if (Build$VERSION.SDK_INT >= 19) {
            Api19Impl.scrollListBy(listView, n);
        }
        else {
            final int firstVisiblePosition = listView.getFirstVisiblePosition();
            if (firstVisiblePosition == -1) {
                return;
            }
            final View child = listView.getChildAt(0);
            if (child == null) {
                return;
            }
            listView.setSelectionFromTop(firstVisiblePosition, child.getTop() - n);
        }
    }
    
    static class Api19Impl
    {
        private Api19Impl() {
        }
        
        static boolean canScrollList(final ListView listView, final int n) {
            return listView.canScrollList(n);
        }
        
        static void scrollListBy(final ListView listView, final int n) {
            listView.scrollListBy(n);
        }
    }
}

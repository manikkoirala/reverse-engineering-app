// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.widget;

import android.util.Log;
import java.lang.reflect.Field;
import android.graphics.PorterDuff$Mode;
import android.content.res.ColorStateList;
import android.os.Build$VERSION;
import android.graphics.drawable.Drawable;
import android.widget.CheckedTextView;

public final class CheckedTextViewCompat
{
    private static final String TAG = "CheckedTextViewCompat";
    
    private CheckedTextViewCompat() {
    }
    
    public static Drawable getCheckMarkDrawable(final CheckedTextView checkedTextView) {
        if (Build$VERSION.SDK_INT >= 16) {
            return Api16Impl.getCheckMarkDrawable(checkedTextView);
        }
        return Api14Impl.getCheckMarkDrawable(checkedTextView);
    }
    
    public static ColorStateList getCheckMarkTintList(final CheckedTextView checkedTextView) {
        if (Build$VERSION.SDK_INT >= 21) {
            return Api21Impl.getCheckMarkTintList(checkedTextView);
        }
        if (checkedTextView instanceof TintableCheckedTextView) {
            return ((TintableCheckedTextView)checkedTextView).getSupportCheckMarkTintList();
        }
        return null;
    }
    
    public static PorterDuff$Mode getCheckMarkTintMode(final CheckedTextView checkedTextView) {
        if (Build$VERSION.SDK_INT >= 21) {
            return Api21Impl.getCheckMarkTintMode(checkedTextView);
        }
        if (checkedTextView instanceof TintableCheckedTextView) {
            return ((TintableCheckedTextView)checkedTextView).getSupportCheckMarkTintMode();
        }
        return null;
    }
    
    public static void setCheckMarkTintList(final CheckedTextView checkedTextView, final ColorStateList supportCheckMarkTintList) {
        if (Build$VERSION.SDK_INT >= 21) {
            Api21Impl.setCheckMarkTintList(checkedTextView, supportCheckMarkTintList);
        }
        else if (checkedTextView instanceof TintableCheckedTextView) {
            ((TintableCheckedTextView)checkedTextView).setSupportCheckMarkTintList(supportCheckMarkTintList);
        }
    }
    
    public static void setCheckMarkTintMode(final CheckedTextView checkedTextView, final PorterDuff$Mode supportCheckMarkTintMode) {
        if (Build$VERSION.SDK_INT >= 21) {
            Api21Impl.setCheckMarkTintMode(checkedTextView, supportCheckMarkTintMode);
        }
        else if (checkedTextView instanceof TintableCheckedTextView) {
            ((TintableCheckedTextView)checkedTextView).setSupportCheckMarkTintMode(supportCheckMarkTintMode);
        }
    }
    
    private static class Api14Impl
    {
        private static Field sCheckMarkDrawableField;
        private static boolean sResolved;
        
        static Drawable getCheckMarkDrawable(final CheckedTextView obj) {
            if (!Api14Impl.sResolved) {
                try {
                    (Api14Impl.sCheckMarkDrawableField = CheckedTextView.class.getDeclaredField("mCheckMarkDrawable")).setAccessible(true);
                }
                catch (final NoSuchFieldException ex) {
                    Log.i("CheckedTextViewCompat", "Failed to retrieve mCheckMarkDrawable field", (Throwable)ex);
                }
                Api14Impl.sResolved = true;
            }
            final Field sCheckMarkDrawableField = Api14Impl.sCheckMarkDrawableField;
            if (sCheckMarkDrawableField != null) {
                try {
                    return (Drawable)sCheckMarkDrawableField.get(obj);
                }
                catch (final IllegalAccessException ex2) {
                    Log.i("CheckedTextViewCompat", "Failed to get check mark drawable via reflection", (Throwable)ex2);
                    Api14Impl.sCheckMarkDrawableField = null;
                }
            }
            return null;
        }
    }
    
    private static class Api16Impl
    {
        static Drawable getCheckMarkDrawable(final CheckedTextView checkedTextView) {
            return checkedTextView.getCheckMarkDrawable();
        }
    }
    
    private static class Api21Impl
    {
        static ColorStateList getCheckMarkTintList(final CheckedTextView checkedTextView) {
            return checkedTextView.getCheckMarkTintList();
        }
        
        static PorterDuff$Mode getCheckMarkTintMode(final CheckedTextView checkedTextView) {
            return checkedTextView.getCheckMarkTintMode();
        }
        
        static void setCheckMarkTintList(final CheckedTextView checkedTextView, final ColorStateList checkMarkTintList) {
            checkedTextView.setCheckMarkTintList(checkMarkTintList);
        }
        
        static void setCheckMarkTintMode(final CheckedTextView checkedTextView, final PorterDuff$Mode checkMarkTintMode) {
            checkedTextView.setCheckMarkTintMode(checkMarkTintMode);
        }
    }
}

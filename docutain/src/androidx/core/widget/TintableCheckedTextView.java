// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.widget;

import android.graphics.PorterDuff$Mode;
import android.content.res.ColorStateList;

public interface TintableCheckedTextView
{
    ColorStateList getSupportCheckMarkTintList();
    
    PorterDuff$Mode getSupportCheckMarkTintMode();
    
    void setSupportCheckMarkTintList(final ColorStateList p0);
    
    void setSupportCheckMarkTintMode(final PorterDuff$Mode p0);
}

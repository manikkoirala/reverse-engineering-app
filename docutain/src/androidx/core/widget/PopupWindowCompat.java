// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.widget;

import androidx.core.view.GravityCompat;
import androidx.core.view.ViewCompat;
import android.view.View;
import android.util.Log;
import android.os.Build$VERSION;
import android.widget.PopupWindow;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

public final class PopupWindowCompat
{
    private static final String TAG = "PopupWindowCompatApi21";
    private static Method sGetWindowLayoutTypeMethod;
    private static boolean sGetWindowLayoutTypeMethodAttempted;
    private static Field sOverlapAnchorField;
    private static boolean sOverlapAnchorFieldAttempted;
    private static Method sSetWindowLayoutTypeMethod;
    private static boolean sSetWindowLayoutTypeMethodAttempted;
    
    private PopupWindowCompat() {
    }
    
    public static boolean getOverlapAnchor(final PopupWindow obj) {
        if (Build$VERSION.SDK_INT >= 23) {
            return Api23Impl.getOverlapAnchor(obj);
        }
        if (Build$VERSION.SDK_INT >= 21) {
            if (!PopupWindowCompat.sOverlapAnchorFieldAttempted) {
                try {
                    (PopupWindowCompat.sOverlapAnchorField = PopupWindow.class.getDeclaredField("mOverlapAnchor")).setAccessible(true);
                }
                catch (final NoSuchFieldException ex) {
                    Log.i("PopupWindowCompatApi21", "Could not fetch mOverlapAnchor field from PopupWindow", (Throwable)ex);
                }
                PopupWindowCompat.sOverlapAnchorFieldAttempted = true;
            }
            final Field sOverlapAnchorField = PopupWindowCompat.sOverlapAnchorField;
            if (sOverlapAnchorField != null) {
                try {
                    return (boolean)sOverlapAnchorField.get(obj);
                }
                catch (final IllegalAccessException ex2) {
                    Log.i("PopupWindowCompatApi21", "Could not get overlap anchor field in PopupWindow", (Throwable)ex2);
                }
            }
        }
        return false;
    }
    
    public static int getWindowLayoutType(final PopupWindow p0) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     3: bipush          23
        //     5: if_icmplt       13
        //     8: aload_0        
        //     9: invokestatic    androidx/core/widget/PopupWindowCompat$Api23Impl.getWindowLayoutType:(Landroid/widget/PopupWindow;)I
        //    12: ireturn        
        //    13: getstatic       androidx/core/widget/PopupWindowCompat.sGetWindowLayoutTypeMethodAttempted:Z
        //    16: ifne            44
        //    19: ldc             Landroid/widget/PopupWindow;.class
        //    21: ldc             "getWindowLayoutType"
        //    23: iconst_0       
        //    24: anewarray       Ljava/lang/Class;
        //    27: invokevirtual   java/lang/Class.getDeclaredMethod:(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
        //    30: astore_2       
        //    31: aload_2        
        //    32: putstatic       androidx/core/widget/PopupWindowCompat.sGetWindowLayoutTypeMethod:Ljava/lang/reflect/Method;
        //    35: aload_2        
        //    36: iconst_1       
        //    37: invokevirtual   java/lang/reflect/Method.setAccessible:(Z)V
        //    40: iconst_1       
        //    41: putstatic       androidx/core/widget/PopupWindowCompat.sGetWindowLayoutTypeMethodAttempted:Z
        //    44: getstatic       androidx/core/widget/PopupWindowCompat.sGetWindowLayoutTypeMethod:Ljava/lang/reflect/Method;
        //    47: astore_2       
        //    48: aload_2        
        //    49: ifnull          70
        //    52: aload_2        
        //    53: aload_0        
        //    54: iconst_0       
        //    55: anewarray       Ljava/lang/Object;
        //    58: invokevirtual   java/lang/reflect/Method.invoke:(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
        //    61: checkcast       Ljava/lang/Integer;
        //    64: invokevirtual   java/lang/Integer.intValue:()I
        //    67: istore_1       
        //    68: iload_1        
        //    69: ireturn        
        //    70: iconst_0       
        //    71: ireturn        
        //    72: astore_2       
        //    73: goto            40
        //    76: astore_0       
        //    77: goto            70
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                 
        //  -----  -----  -----  -----  ---------------------
        //  19     40     72     76     Ljava/lang/Exception;
        //  52     68     76     80     Ljava/lang/Exception;
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0070:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2604)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:206)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public static void setOverlapAnchor(final PopupWindow obj, final boolean b) {
        if (Build$VERSION.SDK_INT >= 23) {
            Api23Impl.setOverlapAnchor(obj, b);
        }
        else if (Build$VERSION.SDK_INT >= 21) {
            if (!PopupWindowCompat.sOverlapAnchorFieldAttempted) {
                try {
                    (PopupWindowCompat.sOverlapAnchorField = PopupWindow.class.getDeclaredField("mOverlapAnchor")).setAccessible(true);
                }
                catch (final NoSuchFieldException ex) {
                    Log.i("PopupWindowCompatApi21", "Could not fetch mOverlapAnchor field from PopupWindow", (Throwable)ex);
                }
                PopupWindowCompat.sOverlapAnchorFieldAttempted = true;
            }
            final Field sOverlapAnchorField = PopupWindowCompat.sOverlapAnchorField;
            if (sOverlapAnchorField != null) {
                try {
                    sOverlapAnchorField.set(obj, b);
                }
                catch (final IllegalAccessException ex2) {
                    Log.i("PopupWindowCompatApi21", "Could not set overlap anchor field in PopupWindow", (Throwable)ex2);
                }
            }
        }
    }
    
    public static void setWindowLayoutType(final PopupWindow p0, final int p1) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     3: bipush          23
        //     5: if_icmplt       16
        //     8: aload_0        
        //     9: iload_1        
        //    10: invokestatic    androidx/core/widget/PopupWindowCompat$Api23Impl.setWindowLayoutType:(Landroid/widget/PopupWindow;I)V
        //    13: goto            78
        //    16: getstatic       androidx/core/widget/PopupWindowCompat.sSetWindowLayoutTypeMethodAttempted:Z
        //    19: ifne            53
        //    22: ldc             Landroid/widget/PopupWindow;.class
        //    24: ldc             "setWindowLayoutType"
        //    26: iconst_1       
        //    27: anewarray       Ljava/lang/Class;
        //    30: dup            
        //    31: iconst_0       
        //    32: getstatic       java/lang/Integer.TYPE:Ljava/lang/Class;
        //    35: aastore        
        //    36: invokevirtual   java/lang/Class.getDeclaredMethod:(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
        //    39: astore_2       
        //    40: aload_2        
        //    41: putstatic       androidx/core/widget/PopupWindowCompat.sSetWindowLayoutTypeMethod:Ljava/lang/reflect/Method;
        //    44: aload_2        
        //    45: iconst_1       
        //    46: invokevirtual   java/lang/reflect/Method.setAccessible:(Z)V
        //    49: iconst_1       
        //    50: putstatic       androidx/core/widget/PopupWindowCompat.sSetWindowLayoutTypeMethodAttempted:Z
        //    53: getstatic       androidx/core/widget/PopupWindowCompat.sSetWindowLayoutTypeMethod:Ljava/lang/reflect/Method;
        //    56: astore_2       
        //    57: aload_2        
        //    58: ifnull          78
        //    61: aload_2        
        //    62: aload_0        
        //    63: iconst_1       
        //    64: anewarray       Ljava/lang/Object;
        //    67: dup            
        //    68: iconst_0       
        //    69: iload_1        
        //    70: invokestatic    java/lang/Integer.valueOf:(I)Ljava/lang/Integer;
        //    73: aastore        
        //    74: invokevirtual   java/lang/reflect/Method.invoke:(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
        //    77: pop            
        //    78: return         
        //    79: astore_2       
        //    80: goto            49
        //    83: astore_0       
        //    84: goto            78
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                 
        //  -----  -----  -----  -----  ---------------------
        //  22     49     79     83     Ljava/lang/Exception;
        //  61     78     83     87     Ljava/lang/Exception;
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0078:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2604)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:206)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public static void showAsDropDown(final PopupWindow popupWindow, final View view, final int n, final int n2, final int n3) {
        if (Build$VERSION.SDK_INT >= 19) {
            Api19Impl.showAsDropDown(popupWindow, view, n, n2, n3);
        }
        else {
            int n4 = n;
            if ((GravityCompat.getAbsoluteGravity(n3, ViewCompat.getLayoutDirection(view)) & 0x7) == 0x5) {
                n4 = n - (popupWindow.getWidth() - view.getWidth());
            }
            popupWindow.showAsDropDown(view, n4, n2);
        }
    }
    
    static class Api19Impl
    {
        private Api19Impl() {
        }
        
        static void showAsDropDown(final PopupWindow popupWindow, final View view, final int n, final int n2, final int n3) {
            popupWindow.showAsDropDown(view, n, n2, n3);
        }
    }
    
    static class Api23Impl
    {
        private Api23Impl() {
        }
        
        static boolean getOverlapAnchor(final PopupWindow popupWindow) {
            return popupWindow.getOverlapAnchor();
        }
        
        static int getWindowLayoutType(final PopupWindow popupWindow) {
            return popupWindow.getWindowLayoutType();
        }
        
        static void setOverlapAnchor(final PopupWindow popupWindow, final boolean overlapAnchor) {
            popupWindow.setOverlapAnchor(overlapAnchor);
        }
        
        static void setWindowLayoutType(final PopupWindow popupWindow, final int windowLayoutType) {
            popupWindow.setWindowLayoutType(windowLayoutType);
        }
    }
}

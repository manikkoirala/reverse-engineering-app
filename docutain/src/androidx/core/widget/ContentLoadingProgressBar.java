// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.widget;

import android.util.AttributeSet;
import android.content.Context;
import android.widget.ProgressBar;

public class ContentLoadingProgressBar extends ProgressBar
{
    private static final int MIN_DELAY_MS = 500;
    private static final int MIN_SHOW_TIME_MS = 500;
    private final Runnable mDelayedHide;
    private final Runnable mDelayedShow;
    boolean mDismissed;
    boolean mPostedHide;
    boolean mPostedShow;
    long mStartTime;
    
    public ContentLoadingProgressBar(final Context context) {
        this(context, null);
    }
    
    public ContentLoadingProgressBar(final Context context, final AttributeSet set) {
        super(context, set, 0);
        this.mStartTime = -1L;
        this.mPostedHide = false;
        this.mPostedShow = false;
        this.mDismissed = false;
        this.mDelayedHide = new ContentLoadingProgressBar$$ExternalSyntheticLambda2(this);
        this.mDelayedShow = new ContentLoadingProgressBar$$ExternalSyntheticLambda3(this);
    }
    
    private void hideOnUiThread() {
        this.mDismissed = true;
        this.removeCallbacks(this.mDelayedShow);
        this.mPostedShow = false;
        final long currentTimeMillis = System.currentTimeMillis();
        final long mStartTime = this.mStartTime;
        final long n = currentTimeMillis - mStartTime;
        if (n < 500L && mStartTime != -1L) {
            if (!this.mPostedHide) {
                this.postDelayed(this.mDelayedHide, 500L - n);
                this.mPostedHide = true;
            }
        }
        else {
            this.setVisibility(8);
        }
    }
    
    private void removeCallbacks() {
        this.removeCallbacks(this.mDelayedHide);
        this.removeCallbacks(this.mDelayedShow);
    }
    
    private void showOnUiThread() {
        this.mStartTime = -1L;
        this.mDismissed = false;
        this.removeCallbacks(this.mDelayedHide);
        this.mPostedHide = false;
        if (!this.mPostedShow) {
            this.postDelayed(this.mDelayedShow, 500L);
            this.mPostedShow = true;
        }
    }
    
    public void hide() {
        this.post((Runnable)new ContentLoadingProgressBar$$ExternalSyntheticLambda1(this));
    }
    
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        this.removeCallbacks();
    }
    
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        this.removeCallbacks();
    }
    
    public void show() {
        this.post((Runnable)new ContentLoadingProgressBar$$ExternalSyntheticLambda0(this));
    }
}

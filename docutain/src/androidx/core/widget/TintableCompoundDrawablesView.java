// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.widget;

import android.graphics.PorterDuff$Mode;
import android.content.res.ColorStateList;

public interface TintableCompoundDrawablesView
{
    ColorStateList getSupportCompoundDrawablesTintList();
    
    PorterDuff$Mode getSupportCompoundDrawablesTintMode();
    
    void setSupportCompoundDrawablesTintList(final ColorStateList p0);
    
    void setSupportCompoundDrawablesTintMode(final PorterDuff$Mode p0);
}

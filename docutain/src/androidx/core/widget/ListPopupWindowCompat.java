// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.widget;

import android.os.Build$VERSION;
import android.view.View$OnTouchListener;
import android.view.View;
import android.widget.ListPopupWindow;

public final class ListPopupWindowCompat
{
    private ListPopupWindowCompat() {
    }
    
    public static View$OnTouchListener createDragToOpenListener(final ListPopupWindow listPopupWindow, final View view) {
        if (Build$VERSION.SDK_INT >= 19) {
            return Api19Impl.createDragToOpenListener(listPopupWindow, view);
        }
        return null;
    }
    
    @Deprecated
    public static View$OnTouchListener createDragToOpenListener(final Object o, final View view) {
        return createDragToOpenListener((ListPopupWindow)o, view);
    }
    
    static class Api19Impl
    {
        private Api19Impl() {
        }
        
        static View$OnTouchListener createDragToOpenListener(final ListPopupWindow listPopupWindow, final View view) {
            return listPopupWindow.createDragToOpenListener(view);
        }
    }
}

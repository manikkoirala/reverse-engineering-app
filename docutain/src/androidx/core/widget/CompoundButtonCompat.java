// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.widget;

import android.graphics.PorterDuff$Mode;
import android.content.res.ColorStateList;
import android.util.Log;
import android.os.Build$VERSION;
import android.graphics.drawable.Drawable;
import android.widget.CompoundButton;
import java.lang.reflect.Field;

public final class CompoundButtonCompat
{
    private static final String TAG = "CompoundButtonCompat";
    private static Field sButtonDrawableField;
    private static boolean sButtonDrawableFieldFetched;
    
    private CompoundButtonCompat() {
    }
    
    public static Drawable getButtonDrawable(final CompoundButton obj) {
        if (Build$VERSION.SDK_INT >= 23) {
            return Api23Impl.getButtonDrawable(obj);
        }
        if (!CompoundButtonCompat.sButtonDrawableFieldFetched) {
            try {
                (CompoundButtonCompat.sButtonDrawableField = CompoundButton.class.getDeclaredField("mButtonDrawable")).setAccessible(true);
            }
            catch (final NoSuchFieldException ex) {
                Log.i("CompoundButtonCompat", "Failed to retrieve mButtonDrawable field", (Throwable)ex);
            }
            CompoundButtonCompat.sButtonDrawableFieldFetched = true;
        }
        final Field sButtonDrawableField = CompoundButtonCompat.sButtonDrawableField;
        if (sButtonDrawableField != null) {
            try {
                return (Drawable)sButtonDrawableField.get(obj);
            }
            catch (final IllegalAccessException ex2) {
                Log.i("CompoundButtonCompat", "Failed to get button drawable via reflection", (Throwable)ex2);
                CompoundButtonCompat.sButtonDrawableField = null;
            }
        }
        return null;
    }
    
    public static ColorStateList getButtonTintList(final CompoundButton compoundButton) {
        if (Build$VERSION.SDK_INT >= 21) {
            return Api21Impl.getButtonTintList(compoundButton);
        }
        if (compoundButton instanceof TintableCompoundButton) {
            return ((TintableCompoundButton)compoundButton).getSupportButtonTintList();
        }
        return null;
    }
    
    public static PorterDuff$Mode getButtonTintMode(final CompoundButton compoundButton) {
        if (Build$VERSION.SDK_INT >= 21) {
            return Api21Impl.getButtonTintMode(compoundButton);
        }
        if (compoundButton instanceof TintableCompoundButton) {
            return ((TintableCompoundButton)compoundButton).getSupportButtonTintMode();
        }
        return null;
    }
    
    public static void setButtonTintList(final CompoundButton compoundButton, final ColorStateList supportButtonTintList) {
        if (Build$VERSION.SDK_INT >= 21) {
            Api21Impl.setButtonTintList(compoundButton, supportButtonTintList);
        }
        else if (compoundButton instanceof TintableCompoundButton) {
            ((TintableCompoundButton)compoundButton).setSupportButtonTintList(supportButtonTintList);
        }
    }
    
    public static void setButtonTintMode(final CompoundButton compoundButton, final PorterDuff$Mode supportButtonTintMode) {
        if (Build$VERSION.SDK_INT >= 21) {
            Api21Impl.setButtonTintMode(compoundButton, supportButtonTintMode);
        }
        else if (compoundButton instanceof TintableCompoundButton) {
            ((TintableCompoundButton)compoundButton).setSupportButtonTintMode(supportButtonTintMode);
        }
    }
    
    static class Api21Impl
    {
        private Api21Impl() {
        }
        
        static ColorStateList getButtonTintList(final CompoundButton compoundButton) {
            return compoundButton.getButtonTintList();
        }
        
        static PorterDuff$Mode getButtonTintMode(final CompoundButton compoundButton) {
            return compoundButton.getButtonTintMode();
        }
        
        static void setButtonTintList(final CompoundButton compoundButton, final ColorStateList buttonTintList) {
            compoundButton.setButtonTintList(buttonTintList);
        }
        
        static void setButtonTintMode(final CompoundButton compoundButton, final PorterDuff$Mode buttonTintMode) {
            compoundButton.setButtonTintMode(buttonTintMode);
        }
    }
    
    static class Api23Impl
    {
        private Api23Impl() {
        }
        
        static Drawable getButtonDrawable(final CompoundButton compoundButton) {
            return compoundButton.getButtonDrawable();
        }
    }
}

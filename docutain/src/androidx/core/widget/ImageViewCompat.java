// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.widget;

import android.graphics.drawable.Drawable;
import android.graphics.PorterDuff$Mode;
import android.os.Build$VERSION;
import android.content.res.ColorStateList;
import android.widget.ImageView;

public class ImageViewCompat
{
    private ImageViewCompat() {
    }
    
    public static ColorStateList getImageTintList(final ImageView imageView) {
        if (Build$VERSION.SDK_INT >= 21) {
            return Api21Impl.getImageTintList(imageView);
        }
        ColorStateList supportImageTintList;
        if (imageView instanceof TintableImageSourceView) {
            supportImageTintList = ((TintableImageSourceView)imageView).getSupportImageTintList();
        }
        else {
            supportImageTintList = null;
        }
        return supportImageTintList;
    }
    
    public static PorterDuff$Mode getImageTintMode(final ImageView imageView) {
        if (Build$VERSION.SDK_INT >= 21) {
            return Api21Impl.getImageTintMode(imageView);
        }
        PorterDuff$Mode supportImageTintMode;
        if (imageView instanceof TintableImageSourceView) {
            supportImageTintMode = ((TintableImageSourceView)imageView).getSupportImageTintMode();
        }
        else {
            supportImageTintMode = null;
        }
        return supportImageTintMode;
    }
    
    public static void setImageTintList(final ImageView imageView, final ColorStateList supportImageTintList) {
        if (Build$VERSION.SDK_INT >= 21) {
            Api21Impl.setImageTintList(imageView, supportImageTintList);
            if (Build$VERSION.SDK_INT == 21) {
                final Drawable drawable = imageView.getDrawable();
                if (drawable != null && Api21Impl.getImageTintList(imageView) != null) {
                    if (drawable.isStateful()) {
                        drawable.setState(imageView.getDrawableState());
                    }
                    imageView.setImageDrawable(drawable);
                }
            }
        }
        else if (imageView instanceof TintableImageSourceView) {
            ((TintableImageSourceView)imageView).setSupportImageTintList(supportImageTintList);
        }
    }
    
    public static void setImageTintMode(final ImageView imageView, final PorterDuff$Mode supportImageTintMode) {
        if (Build$VERSION.SDK_INT >= 21) {
            Api21Impl.setImageTintMode(imageView, supportImageTintMode);
            if (Build$VERSION.SDK_INT == 21) {
                final Drawable drawable = imageView.getDrawable();
                if (drawable != null && Api21Impl.getImageTintList(imageView) != null) {
                    if (drawable.isStateful()) {
                        drawable.setState(imageView.getDrawableState());
                    }
                    imageView.setImageDrawable(drawable);
                }
            }
        }
        else if (imageView instanceof TintableImageSourceView) {
            ((TintableImageSourceView)imageView).setSupportImageTintMode(supportImageTintMode);
        }
    }
    
    static class Api21Impl
    {
        private Api21Impl() {
        }
        
        static ColorStateList getImageTintList(final ImageView imageView) {
            return imageView.getImageTintList();
        }
        
        static PorterDuff$Mode getImageTintMode(final ImageView imageView) {
            return imageView.getImageTintMode();
        }
        
        static void setImageTintList(final ImageView imageView, final ColorStateList imageTintList) {
            imageView.setImageTintList(imageTintList);
        }
        
        static void setImageTintMode(final ImageView imageView, final PorterDuff$Mode imageTintMode) {
            imageView.setImageTintMode(imageTintMode);
        }
    }
}

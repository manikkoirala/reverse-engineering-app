// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.widget;

import android.graphics.Canvas;
import android.os.Build$VERSION;
import android.util.AttributeSet;
import android.content.Context;
import android.widget.EdgeEffect;

public final class EdgeEffectCompat
{
    private final EdgeEffect mEdgeEffect;
    
    @Deprecated
    public EdgeEffectCompat(final Context context) {
        this.mEdgeEffect = new EdgeEffect(context);
    }
    
    public static EdgeEffect create(final Context context, final AttributeSet set) {
        if (Build$VERSION.SDK_INT >= 31) {
            return Api31Impl.create(context, set);
        }
        return new EdgeEffect(context);
    }
    
    public static float getDistance(final EdgeEffect edgeEffect) {
        if (Build$VERSION.SDK_INT >= 31) {
            return Api31Impl.getDistance(edgeEffect);
        }
        return 0.0f;
    }
    
    public static void onPull(final EdgeEffect edgeEffect, final float n, final float n2) {
        if (Build$VERSION.SDK_INT >= 21) {
            Api21Impl.onPull(edgeEffect, n, n2);
        }
        else {
            edgeEffect.onPull(n);
        }
    }
    
    public static float onPullDistance(final EdgeEffect edgeEffect, final float n, final float n2) {
        if (Build$VERSION.SDK_INT >= 31) {
            return Api31Impl.onPullDistance(edgeEffect, n, n2);
        }
        onPull(edgeEffect, n, n2);
        return n;
    }
    
    @Deprecated
    public boolean draw(final Canvas canvas) {
        return this.mEdgeEffect.draw(canvas);
    }
    
    @Deprecated
    public void finish() {
        this.mEdgeEffect.finish();
    }
    
    @Deprecated
    public boolean isFinished() {
        return this.mEdgeEffect.isFinished();
    }
    
    @Deprecated
    public boolean onAbsorb(final int n) {
        this.mEdgeEffect.onAbsorb(n);
        return true;
    }
    
    @Deprecated
    public boolean onPull(final float n) {
        this.mEdgeEffect.onPull(n);
        return true;
    }
    
    @Deprecated
    public boolean onPull(final float n, final float n2) {
        onPull(this.mEdgeEffect, n, n2);
        return true;
    }
    
    @Deprecated
    public boolean onRelease() {
        this.mEdgeEffect.onRelease();
        return this.mEdgeEffect.isFinished();
    }
    
    @Deprecated
    public void setSize(final int n, final int n2) {
        this.mEdgeEffect.setSize(n, n2);
    }
    
    static class Api21Impl
    {
        private Api21Impl() {
        }
        
        static void onPull(final EdgeEffect edgeEffect, final float n, final float n2) {
            edgeEffect.onPull(n, n2);
        }
    }
    
    private static class Api31Impl
    {
        public static EdgeEffect create(final Context context, final AttributeSet set) {
            try {
                return new EdgeEffect(context, set);
            }
            finally {
                return new EdgeEffect(context);
            }
        }
        
        public static float getDistance(final EdgeEffect edgeEffect) {
            try {
                return edgeEffect.getDistance();
            }
            finally {
                return 0.0f;
            }
        }
        
        public static float onPullDistance(final EdgeEffect edgeEffect, final float n, final float n2) {
            try {
                return edgeEffect.onPullDistance(n, n2);
            }
            finally {
                edgeEffect.onPull(n, n2);
                return 0.0f;
            }
        }
    }
}

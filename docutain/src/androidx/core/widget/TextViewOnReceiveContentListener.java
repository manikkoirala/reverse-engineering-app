// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.widget;

import android.text.Spanned;
import android.content.ClipData;
import android.widget.TextView;
import android.util.Log;
import androidx.core.view.ContentInfoCompat;
import android.view.View;
import android.text.Spannable;
import android.text.Selection;
import android.text.Editable;
import android.os.Build$VERSION;
import android.content.ClipData$Item;
import android.content.Context;
import androidx.core.view.OnReceiveContentListener;

public final class TextViewOnReceiveContentListener implements OnReceiveContentListener
{
    private static final String LOG_TAG = "ReceiveContent";
    
    private static CharSequence coerceToText(final Context context, final ClipData$Item clipData$Item, final int n) {
        if (Build$VERSION.SDK_INT >= 16) {
            return Api16Impl.coerce(context, clipData$Item, n);
        }
        return ApiImpl.coerce(context, clipData$Item, n);
    }
    
    private static void replaceSelection(final Editable editable, final CharSequence charSequence) {
        final int selectionStart = Selection.getSelectionStart((CharSequence)editable);
        final int selectionEnd = Selection.getSelectionEnd((CharSequence)editable);
        final int max = Math.max(0, Math.min(selectionStart, selectionEnd));
        final int max2 = Math.max(0, Math.max(selectionStart, selectionEnd));
        Selection.setSelection((Spannable)editable, max2);
        editable.replace(max, max2, charSequence);
    }
    
    @Override
    public ContentInfoCompat onReceiveContent(final View view, final ContentInfoCompat obj) {
        if (Log.isLoggable("ReceiveContent", 3)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("onReceive: ");
            sb.append(obj);
            Log.d("ReceiveContent", sb.toString());
        }
        if (obj.getSource() == 2) {
            return obj;
        }
        final ClipData clip = obj.getClip();
        final int flags = obj.getFlags();
        final TextView textView = (TextView)view;
        final Editable editable = (Editable)textView.getText();
        final Context context = textView.getContext();
        int i = 0;
        int n = 0;
        while (i < clip.getItemCount()) {
            final CharSequence coerceToText = coerceToText(context, clip.getItemAt(i), flags);
            int n2 = n;
            if (coerceToText != null) {
                if (n == 0) {
                    replaceSelection(editable, coerceToText);
                    n2 = 1;
                }
                else {
                    editable.insert(Selection.getSelectionEnd((CharSequence)editable), (CharSequence)"\n");
                    editable.insert(Selection.getSelectionEnd((CharSequence)editable), coerceToText);
                    n2 = n;
                }
            }
            ++i;
            n = n2;
        }
        return null;
    }
    
    private static final class Api16Impl
    {
        static CharSequence coerce(final Context context, final ClipData$Item clipData$Item, final int n) {
            if ((n & 0x1) != 0x0) {
                CharSequence charSequence2;
                final CharSequence charSequence = charSequence2 = clipData$Item.coerceToText(context);
                if (charSequence instanceof Spanned) {
                    charSequence2 = charSequence.toString();
                }
                return charSequence2;
            }
            return clipData$Item.coerceToStyledText(context);
        }
    }
    
    private static final class ApiImpl
    {
        static CharSequence coerce(final Context context, final ClipData$Item clipData$Item, final int n) {
            CharSequence charSequence2;
            final CharSequence charSequence = charSequence2 = clipData$Item.coerceToText(context);
            if ((n & 0x1) != 0x0) {
                charSequence2 = charSequence;
                if (charSequence instanceof Spanned) {
                    charSequence2 = charSequence.toString();
                }
            }
            return charSequence2;
        }
    }
}

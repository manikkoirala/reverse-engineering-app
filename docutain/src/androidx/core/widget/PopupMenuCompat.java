// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.widget;

import android.widget.PopupMenu;
import android.os.Build$VERSION;
import android.view.View$OnTouchListener;

public final class PopupMenuCompat
{
    private PopupMenuCompat() {
    }
    
    public static View$OnTouchListener getDragToOpenListener(final Object o) {
        if (Build$VERSION.SDK_INT >= 19) {
            return Api19Impl.getDragToOpenListener((PopupMenu)o);
        }
        return null;
    }
    
    static class Api19Impl
    {
        private Api19Impl() {
        }
        
        static View$OnTouchListener getDragToOpenListener(final PopupMenu popupMenu) {
            return popupMenu.getDragToOpenListener();
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.hardware.display;

import android.view.WindowManager;
import android.hardware.display.DisplayManager;
import android.os.Build$VERSION;
import android.view.Display;
import java.lang.ref.WeakReference;
import android.content.Context;
import java.util.WeakHashMap;

public final class DisplayManagerCompat
{
    public static final String DISPLAY_CATEGORY_PRESENTATION = "android.hardware.display.category.PRESENTATION";
    private static final WeakHashMap<Context, WeakReference<DisplayManagerCompat>> sInstances;
    private final Context mContext;
    
    static {
        sInstances = new WeakHashMap<Context, WeakReference<DisplayManagerCompat>>();
    }
    
    private DisplayManagerCompat(final Context mContext) {
        this.mContext = mContext;
    }
    
    public static DisplayManagerCompat getInstance(final Context context) {
        final WeakHashMap<Context, WeakReference<DisplayManagerCompat>> sInstances = DisplayManagerCompat.sInstances;
        synchronized (sInstances) {
            final WeakReference weakReference = sInstances.get(context);
            if (weakReference != null) {
                final WeakReference value = weakReference;
                if (weakReference.get() != null) {
                    return (DisplayManagerCompat)value.get();
                }
            }
            final WeakReference value = new WeakReference((T)new DisplayManagerCompat(context));
            sInstances.put(context, value);
            return (DisplayManagerCompat)value.get();
        }
    }
    
    public Display getDisplay(final int n) {
        if (Build$VERSION.SDK_INT >= 17) {
            return Api17Impl.getDisplay((DisplayManager)this.mContext.getSystemService("display"), n);
        }
        final Display defaultDisplay = ((WindowManager)this.mContext.getSystemService("window")).getDefaultDisplay();
        if (defaultDisplay.getDisplayId() == n) {
            return defaultDisplay;
        }
        return null;
    }
    
    public Display[] getDisplays() {
        if (Build$VERSION.SDK_INT >= 17) {
            return Api17Impl.getDisplays((DisplayManager)this.mContext.getSystemService("display"));
        }
        return new Display[] { ((WindowManager)this.mContext.getSystemService("window")).getDefaultDisplay() };
    }
    
    public Display[] getDisplays(final String s) {
        if (Build$VERSION.SDK_INT >= 17) {
            return Api17Impl.getDisplays((DisplayManager)this.mContext.getSystemService("display"));
        }
        if (s == null) {
            return new Display[0];
        }
        return new Display[] { ((WindowManager)this.mContext.getSystemService("window")).getDefaultDisplay() };
    }
    
    static class Api17Impl
    {
        private Api17Impl() {
        }
        
        static Display getDisplay(final DisplayManager displayManager, final int n) {
            return displayManager.getDisplay(n);
        }
        
        static Display[] getDisplays(final DisplayManager displayManager) {
            return displayManager.getDisplays();
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.hardware.fingerprint;

import java.security.Signature;
import javax.crypto.Mac;
import javax.crypto.Cipher;
import android.os.Build$VERSION;
import android.os.Handler;
import androidx.core.os.CancellationSignal;
import android.hardware.fingerprint.FingerprintManager$AuthenticationResult;
import android.hardware.fingerprint.FingerprintManager$AuthenticationCallback;
import android.hardware.fingerprint.FingerprintManager$CryptoObject;
import android.hardware.fingerprint.FingerprintManager;
import android.content.Context;

@Deprecated
public class FingerprintManagerCompat
{
    private final Context mContext;
    
    private FingerprintManagerCompat(final Context mContext) {
        this.mContext = mContext;
    }
    
    public static FingerprintManagerCompat from(final Context context) {
        return new FingerprintManagerCompat(context);
    }
    
    private static FingerprintManager getFingerprintManagerOrNull(final Context context) {
        return Api23Impl.getFingerprintManagerOrNull(context);
    }
    
    static CryptoObject unwrapCryptoObject(final FingerprintManager$CryptoObject fingerprintManager$CryptoObject) {
        return Api23Impl.unwrapCryptoObject(fingerprintManager$CryptoObject);
    }
    
    private static FingerprintManager$AuthenticationCallback wrapCallback(final AuthenticationCallback authenticationCallback) {
        return new FingerprintManager$AuthenticationCallback(authenticationCallback) {
            final AuthenticationCallback val$callback;
            
            public void onAuthenticationError(final int n, final CharSequence charSequence) {
                this.val$callback.onAuthenticationError(n, charSequence);
            }
            
            public void onAuthenticationFailed() {
                this.val$callback.onAuthenticationFailed();
            }
            
            public void onAuthenticationHelp(final int n, final CharSequence charSequence) {
                this.val$callback.onAuthenticationHelp(n, charSequence);
            }
            
            public void onAuthenticationSucceeded(final FingerprintManager$AuthenticationResult fingerprintManager$AuthenticationResult) {
                this.val$callback.onAuthenticationSucceeded(new AuthenticationResult(FingerprintManagerCompat.unwrapCryptoObject(Api23Impl.getCryptoObject(fingerprintManager$AuthenticationResult))));
            }
        };
    }
    
    private static FingerprintManager$CryptoObject wrapCryptoObject(final CryptoObject cryptoObject) {
        return Api23Impl.wrapCryptoObject(cryptoObject);
    }
    
    public void authenticate(final CryptoObject cryptoObject, final int n, final CancellationSignal cancellationSignal, final AuthenticationCallback authenticationCallback, final Handler handler) {
        if (Build$VERSION.SDK_INT >= 23) {
            final FingerprintManager fingerprintManagerOrNull = getFingerprintManagerOrNull(this.mContext);
            if (fingerprintManagerOrNull != null) {
                android.os.CancellationSignal cancellationSignal2;
                if (cancellationSignal != null) {
                    cancellationSignal2 = (android.os.CancellationSignal)cancellationSignal.getCancellationSignalObject();
                }
                else {
                    cancellationSignal2 = null;
                }
                Api23Impl.authenticate(fingerprintManagerOrNull, wrapCryptoObject(cryptoObject), cancellationSignal2, n, wrapCallback(authenticationCallback), handler);
            }
        }
    }
    
    public boolean hasEnrolledFingerprints() {
        final int sdk_INT = Build$VERSION.SDK_INT;
        boolean b2;
        final boolean b = b2 = false;
        if (sdk_INT >= 23) {
            final FingerprintManager fingerprintManagerOrNull = getFingerprintManagerOrNull(this.mContext);
            b2 = b;
            if (fingerprintManagerOrNull != null) {
                b2 = b;
                if (Api23Impl.hasEnrolledFingerprints(fingerprintManagerOrNull)) {
                    b2 = true;
                }
            }
        }
        return b2;
    }
    
    public boolean isHardwareDetected() {
        final int sdk_INT = Build$VERSION.SDK_INT;
        boolean b2;
        final boolean b = b2 = false;
        if (sdk_INT >= 23) {
            final FingerprintManager fingerprintManagerOrNull = getFingerprintManagerOrNull(this.mContext);
            b2 = b;
            if (fingerprintManagerOrNull != null) {
                b2 = b;
                if (Api23Impl.isHardwareDetected(fingerprintManagerOrNull)) {
                    b2 = true;
                }
            }
        }
        return b2;
    }
    
    static class Api23Impl
    {
        private Api23Impl() {
        }
        
        static void authenticate(final Object o, final Object o2, final android.os.CancellationSignal cancellationSignal, final int n, final Object o3, final Handler handler) {
            ((FingerprintManager)o).authenticate((FingerprintManager$CryptoObject)o2, cancellationSignal, n, (FingerprintManager$AuthenticationCallback)o3, handler);
        }
        
        static FingerprintManager$CryptoObject getCryptoObject(final Object o) {
            return ((FingerprintManager$AuthenticationResult)o).getCryptoObject();
        }
        
        public static FingerprintManager getFingerprintManagerOrNull(final Context context) {
            if (Build$VERSION.SDK_INT == 23) {
                return (FingerprintManager)context.getSystemService((Class)FingerprintManager.class);
            }
            if (Build$VERSION.SDK_INT > 23 && context.getPackageManager().hasSystemFeature("android.hardware.fingerprint")) {
                return (FingerprintManager)context.getSystemService((Class)FingerprintManager.class);
            }
            return null;
        }
        
        static boolean hasEnrolledFingerprints(final Object o) {
            return ((FingerprintManager)o).hasEnrolledFingerprints();
        }
        
        static boolean isHardwareDetected(final Object o) {
            return ((FingerprintManager)o).isHardwareDetected();
        }
        
        public static CryptoObject unwrapCryptoObject(final Object o) {
            final FingerprintManager$CryptoObject fingerprintManager$CryptoObject = (FingerprintManager$CryptoObject)o;
            Object o2 = null;
            if (fingerprintManager$CryptoObject == null) {
                return null;
            }
            if (fingerprintManager$CryptoObject.getCipher() != null) {
                return new CryptoObject(fingerprintManager$CryptoObject.getCipher());
            }
            if (fingerprintManager$CryptoObject.getSignature() != null) {
                return new CryptoObject(fingerprintManager$CryptoObject.getSignature());
            }
            if (fingerprintManager$CryptoObject.getMac() != null) {
                o2 = new CryptoObject(fingerprintManager$CryptoObject.getMac());
            }
            return (CryptoObject)o2;
        }
        
        public static FingerprintManager$CryptoObject wrapCryptoObject(final CryptoObject cryptoObject) {
            FingerprintManager$CryptoObject fingerprintManager$CryptoObject = null;
            if (cryptoObject == null) {
                return null;
            }
            if (cryptoObject.getCipher() != null) {
                return new FingerprintManager$CryptoObject(cryptoObject.getCipher());
            }
            if (cryptoObject.getSignature() != null) {
                return new FingerprintManager$CryptoObject(cryptoObject.getSignature());
            }
            if (cryptoObject.getMac() != null) {
                fingerprintManager$CryptoObject = new FingerprintManager$CryptoObject(cryptoObject.getMac());
            }
            return fingerprintManager$CryptoObject;
        }
    }
    
    public abstract static class AuthenticationCallback
    {
        public void onAuthenticationError(final int n, final CharSequence charSequence) {
        }
        
        public void onAuthenticationFailed() {
        }
        
        public void onAuthenticationHelp(final int n, final CharSequence charSequence) {
        }
        
        public void onAuthenticationSucceeded(final AuthenticationResult authenticationResult) {
        }
    }
    
    public static final class AuthenticationResult
    {
        private final CryptoObject mCryptoObject;
        
        public AuthenticationResult(final CryptoObject mCryptoObject) {
            this.mCryptoObject = mCryptoObject;
        }
        
        public CryptoObject getCryptoObject() {
            return this.mCryptoObject;
        }
    }
    
    public static class CryptoObject
    {
        private final Cipher mCipher;
        private final Mac mMac;
        private final Signature mSignature;
        
        public CryptoObject(final Signature mSignature) {
            this.mSignature = mSignature;
            this.mCipher = null;
            this.mMac = null;
        }
        
        public CryptoObject(final Cipher mCipher) {
            this.mCipher = mCipher;
            this.mSignature = null;
            this.mMac = null;
        }
        
        public CryptoObject(final Mac mMac) {
            this.mMac = mMac;
            this.mCipher = null;
            this.mSignature = null;
        }
        
        public Cipher getCipher() {
            return this.mCipher;
        }
        
        public Mac getMac() {
            return this.mMac;
        }
        
        public Signature getSignature() {
            return this.mSignature;
        }
    }
}

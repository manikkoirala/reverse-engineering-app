// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.net;

import android.net.Uri;

public final class UriCompat
{
    private UriCompat() {
    }
    
    public static String toSafeString(final Uri uri) {
        final String scheme = uri.getScheme();
        String str;
        final String s = str = uri.getSchemeSpecificPart();
        Label_0333: {
            if (scheme != null) {
                if (scheme.equalsIgnoreCase("tel") || scheme.equalsIgnoreCase("sip") || scheme.equalsIgnoreCase("sms") || scheme.equalsIgnoreCase("smsto") || scheme.equalsIgnoreCase("mailto") || scheme.equalsIgnoreCase("nfc")) {
                    final StringBuilder sb = new StringBuilder(64);
                    sb.append(scheme);
                    sb.append(':');
                    if (s != null) {
                        for (int i = 0; i < s.length(); ++i) {
                            final char char1 = s.charAt(i);
                            if (char1 != '-' && char1 != '@' && char1 != '.') {
                                sb.append('x');
                            }
                            else {
                                sb.append(char1);
                            }
                        }
                    }
                    return sb.toString();
                }
                if (!scheme.equalsIgnoreCase("http") && !scheme.equalsIgnoreCase("https") && !scheme.equalsIgnoreCase("ftp")) {
                    str = s;
                    if (!scheme.equalsIgnoreCase("rtsp")) {
                        break Label_0333;
                    }
                }
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("//");
                final String host = uri.getHost();
                final String s2 = "";
                String host2;
                if (host != null) {
                    host2 = uri.getHost();
                }
                else {
                    host2 = "";
                }
                sb2.append(host2);
                String string = s2;
                if (uri.getPort() != -1) {
                    final StringBuilder sb3 = new StringBuilder();
                    sb3.append(":");
                    sb3.append(uri.getPort());
                    string = sb3.toString();
                }
                sb2.append(string);
                sb2.append("/...");
                str = sb2.toString();
            }
        }
        final StringBuilder sb4 = new StringBuilder(64);
        if (scheme != null) {
            sb4.append(scheme);
            sb4.append(':');
        }
        if (str != null) {
            sb4.append(str);
        }
        return sb4.toString();
    }
}

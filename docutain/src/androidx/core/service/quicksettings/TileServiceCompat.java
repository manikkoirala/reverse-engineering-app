// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.service.quicksettings;

import android.app.PendingIntent;
import android.content.Intent;
import android.os.Build$VERSION;
import android.service.quicksettings.TileService;

public class TileServiceCompat
{
    private static TileServiceWrapper sTileServiceWrapper;
    
    private TileServiceCompat() {
    }
    
    public static void clearTileServiceWrapper() {
        TileServiceCompat.sTileServiceWrapper = null;
    }
    
    public static void setTileServiceWrapper(final TileServiceWrapper sTileServiceWrapper) {
        TileServiceCompat.sTileServiceWrapper = sTileServiceWrapper;
    }
    
    public static void startActivityAndCollapse(final TileService tileService, final PendingIntentActivityWrapper pendingIntentActivityWrapper) {
        if (Build$VERSION.SDK_INT >= 34) {
            final TileServiceWrapper sTileServiceWrapper = TileServiceCompat.sTileServiceWrapper;
            if (sTileServiceWrapper != null) {
                sTileServiceWrapper.startActivityAndCollapse(pendingIntentActivityWrapper.getPendingIntent());
            }
            else {
                Api34Impl.startActivityAndCollapse(tileService, pendingIntentActivityWrapper.getPendingIntent());
            }
        }
        else if (Build$VERSION.SDK_INT >= 24) {
            final TileServiceWrapper sTileServiceWrapper2 = TileServiceCompat.sTileServiceWrapper;
            if (sTileServiceWrapper2 != null) {
                sTileServiceWrapper2.startActivityAndCollapse(pendingIntentActivityWrapper.getIntent());
            }
            else {
                Api24Impl.startActivityAndCollapse(tileService, pendingIntentActivityWrapper.getIntent());
            }
        }
    }
    
    private static class Api24Impl
    {
        static void startActivityAndCollapse(final TileService tileService, final Intent intent) {
            tileService.startActivityAndCollapse(intent);
        }
    }
    
    private static class Api34Impl
    {
        static void startActivityAndCollapse(final TileService tileService, final PendingIntent pendingIntent) {
            tileService.startActivityAndCollapse(pendingIntent);
        }
    }
    
    interface TileServiceWrapper
    {
        void startActivityAndCollapse(final PendingIntent p0);
        
        void startActivityAndCollapse(final Intent p0);
    }
}

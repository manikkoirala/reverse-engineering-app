// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.service.quicksettings;

import androidx.core.app.PendingIntentCompat;
import android.app.PendingIntent;
import android.os.Bundle;
import android.content.Intent;
import android.content.Context;

public class PendingIntentActivityWrapper
{
    private final Context mContext;
    private final int mFlags;
    private final Intent mIntent;
    private final boolean mIsMutable;
    private final Bundle mOptions;
    private final PendingIntent mPendingIntent;
    private final int mRequestCode;
    
    public PendingIntentActivityWrapper(final Context mContext, final int mRequestCode, final Intent mIntent, final int mFlags, final Bundle mOptions, final boolean mIsMutable) {
        this.mContext = mContext;
        this.mRequestCode = mRequestCode;
        this.mIntent = mIntent;
        this.mFlags = mFlags;
        this.mOptions = mOptions;
        this.mIsMutable = mIsMutable;
        this.mPendingIntent = this.createPendingIntent();
    }
    
    public PendingIntentActivityWrapper(final Context context, final int n, final Intent intent, final int n2, final boolean b) {
        this(context, n, intent, n2, null, b);
    }
    
    private PendingIntent createPendingIntent() {
        final Bundle mOptions = this.mOptions;
        if (mOptions == null) {
            return PendingIntentCompat.getActivity(this.mContext, this.mRequestCode, this.mIntent, this.mFlags, this.mIsMutable);
        }
        return PendingIntentCompat.getActivity(this.mContext, this.mRequestCode, this.mIntent, this.mFlags, mOptions, this.mIsMutable);
    }
    
    public Context getContext() {
        return this.mContext;
    }
    
    public int getFlags() {
        return this.mFlags;
    }
    
    public Intent getIntent() {
        return this.mIntent;
    }
    
    public Bundle getOptions() {
        return this.mOptions;
    }
    
    public PendingIntent getPendingIntent() {
        return this.mPendingIntent;
    }
    
    public int getRequestCode() {
        return this.mRequestCode;
    }
    
    public boolean isMutable() {
        return this.mIsMutable;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.os;

import kotlin.annotation.AnnotationRetention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;
import android.os.ext.SdkExtensions;
import java.util.Locale;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.JvmStatic;
import kotlin.ReplaceWith;
import kotlin.Deprecated;
import android.os.Build$VERSION;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0002\b\u0006\n\u0002\u0010\u000e\n\u0002\b\u000b\b\u00c6\u0002\u0018\u00002\u00020\u0001:\u0002\u0019\u001aB\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\b\u0010\b\u001a\u00020\tH\u0007J\b\u0010\n\u001a\u00020\tH\u0007J\b\u0010\u000b\u001a\u00020\tH\u0007J\b\u0010\f\u001a\u00020\tH\u0007J\b\u0010\r\u001a\u00020\tH\u0007J\u0018\u0010\u000e\u001a\u00020\t2\u0006\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u0010H\u0007J\b\u0010\u0012\u001a\u00020\tH\u0007J\b\u0010\u0013\u001a\u00020\tH\u0007J\b\u0010\u0014\u001a\u00020\tH\u0007J\b\u0010\u0015\u001a\u00020\tH\u0007J\b\u0010\u0016\u001a\u00020\tH\u0007J\b\u0010\u0017\u001a\u00020\tH\u0007J\b\u0010\u0018\u001a\u00020\tH\u0007R\u0010\u0010\u0003\u001a\u00020\u00048\u0006X\u0087\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\u0005\u001a\u00020\u00048\u0006X\u0087\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\u0006\u001a\u00020\u00048\u0006X\u0087\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\u0007\u001a\u00020\u00048\u0006X\u0087\u0004¢\u0006\u0002\n\u0000¨\u0006\u001b" }, d2 = { "Landroidx/core/os/BuildCompat;", "", "()V", "AD_SERVICES_EXTENSION_INT", "", "R_EXTENSION_INT", "S_EXTENSION_INT", "T_EXTENSION_INT", "isAtLeastN", "", "isAtLeastNMR1", "isAtLeastO", "isAtLeastOMR1", "isAtLeastP", "isAtLeastPreReleaseCodename", "codename", "", "buildCodename", "isAtLeastQ", "isAtLeastR", "isAtLeastS", "isAtLeastSv2", "isAtLeastT", "isAtLeastU", "isAtLeastV", "Api30Impl", "PrereleaseSdkCheck", "core_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
public final class BuildCompat
{
    public static final int AD_SERVICES_EXTENSION_INT;
    public static final BuildCompat INSTANCE;
    public static final int R_EXTENSION_INT;
    public static final int S_EXTENSION_INT;
    public static final int T_EXTENSION_INT;
    
    static {
        INSTANCE = new BuildCompat();
        final int sdk_INT = Build$VERSION.SDK_INT;
        final int n = 0;
        int extensionVersion;
        if (sdk_INT >= 30) {
            extensionVersion = Api30Impl.INSTANCE.getExtensionVersion(30);
        }
        else {
            extensionVersion = 0;
        }
        R_EXTENSION_INT = extensionVersion;
        int extensionVersion2;
        if (Build$VERSION.SDK_INT >= 30) {
            extensionVersion2 = Api30Impl.INSTANCE.getExtensionVersion(31);
        }
        else {
            extensionVersion2 = 0;
        }
        S_EXTENSION_INT = extensionVersion2;
        int extensionVersion3;
        if (Build$VERSION.SDK_INT >= 30) {
            extensionVersion3 = Api30Impl.INSTANCE.getExtensionVersion(33);
        }
        else {
            extensionVersion3 = 0;
        }
        T_EXTENSION_INT = extensionVersion3;
        int extensionVersion4 = n;
        if (Build$VERSION.SDK_INT >= 30) {
            extensionVersion4 = Api30Impl.INSTANCE.getExtensionVersion(1000000);
        }
        AD_SERVICES_EXTENSION_INT = extensionVersion4;
    }
    
    private BuildCompat() {
    }
    
    @Deprecated(message = "Android N is a finalized release and this method is no longer necessary. It will be removed in a future release of this library. Instead, use `Build.VERSION.SDK_INT >= 24`.", replaceWith = @ReplaceWith(expression = "android.os.Build.VERSION.SDK_INT >= 24", imports = {}))
    @JvmStatic
    public static final boolean isAtLeastN() {
        return Build$VERSION.SDK_INT >= 24;
    }
    
    @Deprecated(message = "Android N MR1 is a finalized release and this method is no longer necessary. It will be removed in a future release of this library. Instead, use `Build.VERSION.SDK_INT >= 25`.", replaceWith = @ReplaceWith(expression = "android.os.Build.VERSION.SDK_INT >= 25", imports = {}))
    @JvmStatic
    public static final boolean isAtLeastNMR1() {
        return Build$VERSION.SDK_INT >= 25;
    }
    
    @Deprecated(message = "Android O is a finalized release and this method is no longer necessary. It will be removed in a future release of this library. Instead use `Build.VERSION.SDK_INT >= 26`.", replaceWith = @ReplaceWith(expression = "android.os.Build.VERSION.SDK_INT >= 26", imports = {}))
    @JvmStatic
    public static final boolean isAtLeastO() {
        return Build$VERSION.SDK_INT >= 26;
    }
    
    @Deprecated(message = "Android O MR1 is a finalized release and this method is no longer necessary. It will be removed in a future release of this library. Instead, use `Build.VERSION.SDK_INT >= 27`.", replaceWith = @ReplaceWith(expression = "android.os.Build.VERSION.SDK_INT >= 27", imports = {}))
    @JvmStatic
    public static final boolean isAtLeastOMR1() {
        return Build$VERSION.SDK_INT >= 27;
    }
    
    @Deprecated(message = "Android P is a finalized release and this method is no longer necessary. It will be removed in a future release of this library. Instead, use `Build.VERSION.SDK_INT >= 28`.", replaceWith = @ReplaceWith(expression = "android.os.Build.VERSION.SDK_INT >= 28", imports = {}))
    @JvmStatic
    public static final boolean isAtLeastP() {
        return Build$VERSION.SDK_INT >= 28;
    }
    
    @JvmStatic
    public static final boolean isAtLeastPreReleaseCodename(String upperCase, String upperCase2) {
        Intrinsics.checkNotNullParameter((Object)upperCase, "codename");
        Intrinsics.checkNotNullParameter((Object)upperCase2, "buildCodename");
        final boolean equal = Intrinsics.areEqual((Object)"REL", (Object)upperCase2);
        boolean b = false;
        if (equal) {
            return false;
        }
        upperCase2 = upperCase2.toUpperCase(Locale.ROOT);
        Intrinsics.checkNotNullExpressionValue((Object)upperCase2, "this as java.lang.String).toUpperCase(Locale.ROOT)");
        upperCase = upperCase.toUpperCase(Locale.ROOT);
        Intrinsics.checkNotNullExpressionValue((Object)upperCase, "this as java.lang.String).toUpperCase(Locale.ROOT)");
        if (upperCase2.compareTo(upperCase) >= 0) {
            b = true;
        }
        return b;
    }
    
    @Deprecated(message = "Android Q is a finalized release and this method is no longer necessary. It will be removed in a future release of this library. Instead, use `Build.VERSION.SDK_INT >= 29`.", replaceWith = @ReplaceWith(expression = "android.os.Build.VERSION.SDK_INT >= 29", imports = {}))
    @JvmStatic
    public static final boolean isAtLeastQ() {
        return Build$VERSION.SDK_INT >= 29;
    }
    
    @Deprecated(message = "Android R is a finalized release and this method is no longer necessary. It will be removed in a future release of this library. Instead, use `Build.VERSION.SDK_INT >= 30`.", replaceWith = @ReplaceWith(expression = "android.os.Build.VERSION.SDK_INT >= 30", imports = {}))
    @JvmStatic
    public static final boolean isAtLeastR() {
        return Build$VERSION.SDK_INT >= 30;
    }
    
    @Deprecated(message = "Android S is a finalized release and this method is no longer necessary. It will be removed in a future release of this library. Instead, use `Build.VERSION.SDK_INT >= 31`.", replaceWith = @ReplaceWith(expression = "android.os.Build.VERSION.SDK_INT >= 31", imports = {}))
    @JvmStatic
    public static final boolean isAtLeastS() {
        if (Build$VERSION.SDK_INT < 31) {
            if (Build$VERSION.SDK_INT >= 30) {
                final String codename = Build$VERSION.CODENAME;
                Intrinsics.checkNotNullExpressionValue((Object)codename, "CODENAME");
                if (isAtLeastPreReleaseCodename("S", codename)) {
                    return true;
                }
            }
            return false;
        }
        return true;
    }
    
    @Deprecated(message = "Android Sv2 is a finalized release and this method is no longer necessary. It will be removed in a future release of this library. Instead, use `Build.VERSION.SDK_INT >= 32`.", replaceWith = @ReplaceWith(expression = "android.os.Build.VERSION.SDK_INT >= 32", imports = {}))
    @JvmStatic
    public static final boolean isAtLeastSv2() {
        if (Build$VERSION.SDK_INT < 32) {
            if (Build$VERSION.SDK_INT >= 31) {
                final String codename = Build$VERSION.CODENAME;
                Intrinsics.checkNotNullExpressionValue((Object)codename, "CODENAME");
                if (isAtLeastPreReleaseCodename("Sv2", codename)) {
                    return true;
                }
            }
            return false;
        }
        return true;
    }
    
    @Deprecated(message = "Android Tiramisu is a finalized release and this method is no longer necessary. It will be removed in a future release of this library. Instead, use `Build.VERSION.SDK_INT >= 33`.", replaceWith = @ReplaceWith(expression = "android.os.Build.VERSION.SDK_INT >= 33", imports = {}))
    @JvmStatic
    public static final boolean isAtLeastT() {
        if (Build$VERSION.SDK_INT < 33) {
            if (Build$VERSION.SDK_INT >= 32) {
                final String codename = Build$VERSION.CODENAME;
                Intrinsics.checkNotNullExpressionValue((Object)codename, "CODENAME");
                if (isAtLeastPreReleaseCodename("Tiramisu", codename)) {
                    return true;
                }
            }
            return false;
        }
        return true;
    }
    
    @Deprecated(message = "Android UpsideDownCase is a finalized release and this method is no longer necessary. It will be removed in a future release of this library. Instead, use `Build.VERSION.SDK_INT >= 34`.", replaceWith = @ReplaceWith(expression = "android.os.Build.VERSION.SDK_INT >= 34", imports = {}))
    @JvmStatic
    public static final boolean isAtLeastU() {
        if (Build$VERSION.SDK_INT < 34) {
            if (Build$VERSION.SDK_INT >= 33) {
                final String codename = Build$VERSION.CODENAME;
                Intrinsics.checkNotNullExpressionValue((Object)codename, "CODENAME");
                if (isAtLeastPreReleaseCodename("UpsideDownCake", codename)) {
                    return true;
                }
            }
            return false;
        }
        return true;
    }
    
    @JvmStatic
    public static final boolean isAtLeastV() {
        if (Build$VERSION.SDK_INT >= 34) {
            final String codename = Build$VERSION.CODENAME;
            Intrinsics.checkNotNullExpressionValue((Object)codename, "CODENAME");
            if (isAtLeastPreReleaseCodename("VanillaIceCream", codename)) {
                return true;
            }
        }
        return false;
    }
    
    @Metadata(d1 = { "\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\b\u00c3\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0004H\u0007¨\u0006\u0006" }, d2 = { "Landroidx/core/os/BuildCompat$Api30Impl;", "", "()V", "getExtensionVersion", "", "extension", "core_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
    private static final class Api30Impl
    {
        public static final Api30Impl INSTANCE;
        
        static {
            INSTANCE = new Api30Impl();
        }
        
        public final int getExtensionVersion(final int n) {
            return SdkExtensions.getExtensionVersion(n);
        }
    }
    
    @Retention(RetentionPolicy.CLASS)
    @Metadata(d1 = { "\u0000\n\n\u0002\u0018\u0002\n\u0002\u0010\u001b\n\u0000\b\u0087\u0002\u0018\u00002\u00020\u0001B\u0000¨\u0006\u0002" }, d2 = { "Landroidx/core/os/BuildCompat$PrereleaseSdkCheck;", "", "core_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
    @kotlin.annotation.Retention(AnnotationRetention.BINARY)
    public @interface PrereleaseSdkCheck {
    }
}

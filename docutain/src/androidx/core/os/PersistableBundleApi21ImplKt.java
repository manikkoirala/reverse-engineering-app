// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.os;

import kotlin.jvm.internal.Intrinsics;
import android.os.Build$VERSION;
import kotlin.jvm.JvmStatic;
import android.os.PersistableBundle;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\b\u00c3\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0007J$\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\u00042\b\u0010\n\u001a\u0004\u0018\u00010\u000b2\b\u0010\f\u001a\u0004\u0018\u00010\u0001H\u0007¨\u0006\r" }, d2 = { "Landroidx/core/os/PersistableBundleApi21ImplKt;", "", "()V", "createPersistableBundle", "Landroid/os/PersistableBundle;", "capacity", "", "putValue", "", "persistableBundle", "key", "", "value", "core-ktx_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
final class PersistableBundleApi21ImplKt
{
    public static final PersistableBundleApi21ImplKt INSTANCE;
    
    static {
        INSTANCE = new PersistableBundleApi21ImplKt();
    }
    
    private PersistableBundleApi21ImplKt() {
    }
    
    @JvmStatic
    public static final PersistableBundle createPersistableBundle(final int n) {
        return new PersistableBundle(n);
    }
    
    @JvmStatic
    public static final void putValue(final PersistableBundle persistableBundle, final String s, final Object o) {
        if (o == null) {
            persistableBundle.putString(s, (String)null);
        }
        else if (o instanceof Boolean) {
            if (Build$VERSION.SDK_INT < 22) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Illegal value type boolean for key \"");
                sb.append(s);
                sb.append('\"');
                throw new IllegalArgumentException(sb.toString());
            }
            PersistableBundleApi22ImplKt.putBoolean(persistableBundle, s, (boolean)o);
        }
        else if (o instanceof Double) {
            persistableBundle.putDouble(s, ((Number)o).doubleValue());
        }
        else if (o instanceof Integer) {
            persistableBundle.putInt(s, ((Number)o).intValue());
        }
        else if (o instanceof Long) {
            persistableBundle.putLong(s, ((Number)o).longValue());
        }
        else if (o instanceof String) {
            persistableBundle.putString(s, (String)o);
        }
        else if (o instanceof boolean[]) {
            if (Build$VERSION.SDK_INT < 22) {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("Illegal value type boolean[] for key \"");
                sb2.append(s);
                sb2.append('\"');
                throw new IllegalArgumentException(sb2.toString());
            }
            PersistableBundleApi22ImplKt.putBooleanArray(persistableBundle, s, (boolean[])o);
        }
        else if (o instanceof double[]) {
            persistableBundle.putDoubleArray(s, (double[])o);
        }
        else if (o instanceof int[]) {
            persistableBundle.putIntArray(s, (int[])o);
        }
        else if (o instanceof long[]) {
            persistableBundle.putLongArray(s, (long[])o);
        }
        else {
            if (!(o instanceof Object[])) {
                final String canonicalName = o.getClass().getCanonicalName();
                final StringBuilder sb3 = new StringBuilder();
                sb3.append("Illegal value type ");
                sb3.append(canonicalName);
                sb3.append(" for key \"");
                sb3.append(s);
                sb3.append('\"');
                throw new IllegalArgumentException(sb3.toString());
            }
            final Class<?> componentType = o.getClass().getComponentType();
            Intrinsics.checkNotNull((Object)componentType);
            if (!String.class.isAssignableFrom(componentType)) {
                final String canonicalName2 = componentType.getCanonicalName();
                final StringBuilder sb4 = new StringBuilder();
                sb4.append("Illegal value array type ");
                sb4.append(canonicalName2);
                sb4.append(" for key \"");
                sb4.append(s);
                sb4.append('\"');
                throw new IllegalArgumentException(sb4.toString());
            }
            Intrinsics.checkNotNull(o, "null cannot be cast to non-null type kotlin.Array<kotlin.String>");
            persistableBundle.putStringArray(s, (String[])o);
        }
    }
}

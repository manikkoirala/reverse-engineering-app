// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.os;

import android.os.Process;
import android.os.UserHandle;
import java.lang.reflect.Method;
import android.os.Build$VERSION;

public final class ProcessCompat
{
    private ProcessCompat() {
    }
    
    public static boolean isApplicationUid(final int n) {
        if (Build$VERSION.SDK_INT >= 24) {
            return Api24Impl.isApplicationUid(n);
        }
        if (Build$VERSION.SDK_INT >= 17) {
            return Api17Impl.isApplicationUid(n);
        }
        return Build$VERSION.SDK_INT != 16 || Api16Impl.isApplicationUid(n);
    }
    
    static class Api16Impl
    {
        private static Method sMethodUserIdIsAppMethod;
        private static boolean sResolved;
        private static final Object sResolvedLock;
        
        static {
            sResolvedLock = new Object();
        }
        
        private Api16Impl() {
        }
        
        static boolean isApplicationUid(final int i) {
            try {
                Object o = Api16Impl.sResolvedLock;
                synchronized (o) {
                    if (!Api16Impl.sResolved) {
                        Api16Impl.sResolved = true;
                        Api16Impl.sMethodUserIdIsAppMethod = Class.forName("android.os.UserId").getDeclaredMethod("isApp", Integer.TYPE);
                    }
                    monitorexit(o);
                    o = Api16Impl.sMethodUserIdIsAppMethod;
                    if (o == null) {
                        return true;
                    }
                    o = ((Method)o).invoke(null, i);
                    if (o != null) {
                        return (boolean)o;
                    }
                    o = new NullPointerException();
                    throw o;
                }
            }
            catch (final Exception ex) {
                ex.printStackTrace();
            }
            return true;
        }
    }
    
    static class Api17Impl
    {
        private static Method sMethodUserHandleIsAppMethod;
        private static boolean sResolved;
        private static final Object sResolvedLock;
        
        static {
            sResolvedLock = new Object();
        }
        
        private Api17Impl() {
        }
        
        static boolean isApplicationUid(final int i) {
            try {
                Object o = Api17Impl.sResolvedLock;
                synchronized (o) {
                    if (!Api17Impl.sResolved) {
                        Api17Impl.sResolved = true;
                        Api17Impl.sMethodUserHandleIsAppMethod = UserHandle.class.getDeclaredMethod("isApp", Integer.TYPE);
                    }
                    monitorexit(o);
                    o = Api17Impl.sMethodUserHandleIsAppMethod;
                    if (o == null) {
                        return true;
                    }
                    o = ((Method)o).invoke(null, i);
                    if (o != null) {
                        return (boolean)o;
                    }
                    o = new NullPointerException();
                    throw o;
                }
            }
            catch (final Exception ex) {
                ex.printStackTrace();
            }
            return true;
        }
    }
    
    static class Api24Impl
    {
        private Api24Impl() {
        }
        
        static boolean isApplicationUid(final int n) {
            return Process.isApplicationUid(n);
        }
    }
}

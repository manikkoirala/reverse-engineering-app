// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.os;

import android.util.Log;
import android.os.Trace;
import android.os.Build$VERSION;
import java.lang.reflect.Method;

@Deprecated
public final class TraceCompat
{
    private static final String TAG = "TraceCompat";
    private static Method sAsyncTraceBeginMethod;
    private static Method sAsyncTraceEndMethod;
    private static Method sIsTagEnabledMethod;
    private static Method sTraceCounterMethod;
    private static long sTraceTagApp;
    
    static {
        if (Build$VERSION.SDK_INT >= 18 && Build$VERSION.SDK_INT < 29) {
            try {
                TraceCompat.sTraceTagApp = Trace.class.getField("TRACE_TAG_APP").getLong(null);
                TraceCompat.sIsTagEnabledMethod = Trace.class.getMethod("isTagEnabled", Long.TYPE);
                TraceCompat.sAsyncTraceBeginMethod = Trace.class.getMethod("asyncTraceBegin", Long.TYPE, String.class, Integer.TYPE);
                TraceCompat.sAsyncTraceEndMethod = Trace.class.getMethod("asyncTraceEnd", Long.TYPE, String.class, Integer.TYPE);
                TraceCompat.sTraceCounterMethod = Trace.class.getMethod("traceCounter", Long.TYPE, String.class, Integer.TYPE);
            }
            catch (final Exception ex) {
                Log.i("TraceCompat", "Unable to initialize via reflection.", (Throwable)ex);
            }
        }
    }
    
    private TraceCompat() {
    }
    
    public static void beginAsyncSection(final String s, final int i) {
        if (Build$VERSION.SDK_INT >= 29) {
            Api29Impl.beginAsyncSection(s, i);
        }
        else if (Build$VERSION.SDK_INT >= 18) {
            try {
                TraceCompat.sAsyncTraceBeginMethod.invoke(null, TraceCompat.sTraceTagApp, s, i);
            }
            catch (final Exception ex) {
                Log.v("TraceCompat", "Unable to invoke asyncTraceBegin() via reflection.");
            }
        }
    }
    
    public static void beginSection(final String s) {
        if (Build$VERSION.SDK_INT >= 18) {
            Api18Impl.beginSection(s);
        }
    }
    
    public static void endAsyncSection(final String s, final int i) {
        if (Build$VERSION.SDK_INT >= 29) {
            Api29Impl.endAsyncSection(s, i);
        }
        else if (Build$VERSION.SDK_INT >= 18) {
            try {
                TraceCompat.sAsyncTraceEndMethod.invoke(null, TraceCompat.sTraceTagApp, s, i);
            }
            catch (final Exception ex) {
                Log.v("TraceCompat", "Unable to invoke endAsyncSection() via reflection.");
            }
        }
    }
    
    public static void endSection() {
        if (Build$VERSION.SDK_INT >= 18) {
            Api18Impl.endSection();
        }
    }
    
    public static boolean isEnabled() {
        if (Build$VERSION.SDK_INT >= 29) {
            return Api29Impl.isEnabled();
        }
        if (Build$VERSION.SDK_INT >= 18) {
            try {
                return (boolean)TraceCompat.sIsTagEnabledMethod.invoke(null, TraceCompat.sTraceTagApp);
            }
            catch (final Exception ex) {
                Log.v("TraceCompat", "Unable to invoke isTagEnabled() via reflection.");
            }
        }
        return false;
    }
    
    public static void setCounter(final String s, final int i) {
        if (Build$VERSION.SDK_INT >= 29) {
            Api29Impl.setCounter(s, i);
        }
        else if (Build$VERSION.SDK_INT >= 18) {
            try {
                TraceCompat.sTraceCounterMethod.invoke(null, TraceCompat.sTraceTagApp, s, i);
            }
            catch (final Exception ex) {
                Log.v("TraceCompat", "Unable to invoke traceCounter() via reflection.");
            }
        }
    }
    
    static class Api18Impl
    {
        private Api18Impl() {
        }
        
        static void beginSection(final String s) {
            Trace.beginSection(s);
        }
        
        static void endSection() {
            Trace.endSection();
        }
    }
    
    static class Api29Impl
    {
        private Api29Impl() {
        }
        
        static void beginAsyncSection(final String s, final int n) {
            Trace.beginAsyncSection(s, n);
        }
        
        static void endAsyncSection(final String s, final int n) {
            Trace.endAsyncSection(s, n);
        }
        
        static boolean isEnabled() {
            return Trace.isEnabled();
        }
        
        static void setCounter(final String s, final long n) {
            Trace.setCounter(s, n);
        }
    }
}

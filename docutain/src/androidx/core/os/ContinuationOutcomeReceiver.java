// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.os;

import kotlin.Result$Companion;
import kotlin.ResultKt;
import kotlin.Result;
import kotlin.coroutines.Continuation;
import kotlin.Metadata;
import android.os.OutcomeReceiver;
import java.util.concurrent.atomic.AtomicBoolean;

@Metadata(d1 = { "\u0000,\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0006\n\u0002\u0010\u000e\n\u0000\b\u0003\u0018\u0000*\u0004\b\u0000\u0010\u0001*\b\b\u0001\u0010\u0002*\u00020\u00032\u000e\u0012\u0004\u0012\u0002H\u0001\u0012\u0004\u0012\u0002H\u00020\u00042\u00020\u0005B\u0013\u0012\f\u0010\u0006\u001a\b\u0012\u0004\u0012\u00028\u00000\u0007¢\u0006\u0002\u0010\bJ\u0015\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00028\u0001H\u0016¢\u0006\u0002\u0010\fJ\u0015\u0010\r\u001a\u00020\n2\u0006\u0010\u000e\u001a\u00028\u0000H\u0016¢\u0006\u0002\u0010\u000fJ\b\u0010\u0010\u001a\u00020\u0011H\u0016R\u0014\u0010\u0006\u001a\b\u0012\u0004\u0012\u00028\u00000\u0007X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u0012" }, d2 = { "Landroidx/core/os/ContinuationOutcomeReceiver;", "R", "E", "", "Landroid/os/OutcomeReceiver;", "Ljava/util/concurrent/atomic/AtomicBoolean;", "continuation", "Lkotlin/coroutines/Continuation;", "(Lkotlin/coroutines/Continuation;)V", "onError", "", "error", "(Ljava/lang/Throwable;)V", "onResult", "result", "(Ljava/lang/Object;)V", "toString", "", "core-ktx_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
final class ContinuationOutcomeReceiver<R, E extends Throwable> extends AtomicBoolean implements OutcomeReceiver<R, E>
{
    private final Continuation<R> continuation;
    
    public ContinuationOutcomeReceiver(final Continuation<? super R> continuation) {
        super(false);
        this.continuation = (Continuation<R>)continuation;
    }
    
    public void onError(final E e) {
        if (this.compareAndSet(false, true)) {
            final Continuation<R> continuation = this.continuation;
            final Result$Companion companion = Result.Companion;
            continuation.resumeWith(Result.constructor-impl(ResultKt.createFailure((Throwable)e)));
        }
    }
    
    public void onResult(final R r) {
        if (this.compareAndSet(false, true)) {
            final Continuation<R> continuation = this.continuation;
            final Result$Companion companion = Result.Companion;
            continuation.resumeWith(Result.constructor-impl((Object)r));
        }
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("ContinuationOutcomeReceiver(outcomeReceived = ");
        sb.append(this.get());
        sb.append(')');
        return sb.toString();
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.os;

import android.os.LocaleList;
import java.util.Locale;
import android.os.Build$VERSION;
import android.content.res.Configuration;

public final class ConfigurationCompat
{
    private ConfigurationCompat() {
    }
    
    public static LocaleListCompat getLocales(final Configuration configuration) {
        if (Build$VERSION.SDK_INT >= 24) {
            return LocaleListCompat.wrap(Api24Impl.getLocales(configuration));
        }
        return LocaleListCompat.create(configuration.locale);
    }
    
    public static void setLocales(final Configuration configuration, final LocaleListCompat localeListCompat) {
        if (Build$VERSION.SDK_INT >= 24) {
            Api24Impl.setLocales(configuration, localeListCompat);
        }
        else if (Build$VERSION.SDK_INT >= 17) {
            Api17Impl.setLocale(configuration, localeListCompat);
        }
    }
    
    static class Api17Impl
    {
        private Api17Impl() {
        }
        
        static void setLocale(final Configuration configuration, final LocaleListCompat localeListCompat) {
            if (!localeListCompat.isEmpty()) {
                configuration.setLocale(localeListCompat.get(0));
            }
        }
    }
    
    static class Api24Impl
    {
        private Api24Impl() {
        }
        
        static LocaleList getLocales(final Configuration configuration) {
            return configuration.getLocales();
        }
        
        static void setLocales(final Configuration configuration, final LocaleListCompat localeListCompat) {
            configuration.setLocales((LocaleList)localeListCompat.unwrap());
        }
    }
}

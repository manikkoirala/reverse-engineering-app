// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.os;

import android.os.Message;
import android.util.Log;
import java.lang.reflect.InvocationTargetException;
import android.os.Handler$Callback;
import android.os.Build$VERSION;
import android.os.Handler;
import android.os.Looper;

public final class HandlerCompat
{
    private static final String TAG = "HandlerCompat";
    
    private HandlerCompat() {
    }
    
    public static Handler createAsync(final Looper looper) {
        if (Build$VERSION.SDK_INT >= 28) {
            return Api28Impl.createAsync(looper);
        }
        if (Build$VERSION.SDK_INT >= 17) {
            Handler handler = null;
            try {
                handler = Handler.class.getDeclaredConstructor(Looper.class, Handler$Callback.class, Boolean.TYPE).newInstance(looper, null, true);
                return handler;
            }
            catch (final InvocationTargetException ex) {
                final Throwable cause = ex.getCause();
                if (cause instanceof RuntimeException) {
                    throw (RuntimeException)cause;
                }
                if (cause instanceof Error) {
                    throw (Error)cause;
                }
                throw new RuntimeException(cause);
            }
            catch (final NoSuchMethodException handler) {}
            catch (final InstantiationException handler) {}
            catch (final IllegalAccessException ex2) {}
            Log.w("HandlerCompat", "Unable to invoke Handler(Looper, Callback, boolean) constructor", (Throwable)handler);
        }
        return new Handler(looper);
    }
    
    public static Handler createAsync(final Looper looper, final Handler$Callback handler$Callback) {
        if (Build$VERSION.SDK_INT >= 28) {
            return Api28Impl.createAsync(looper, handler$Callback);
        }
        if (Build$VERSION.SDK_INT >= 17) {
            Handler handler = null;
            try {
                handler = Handler.class.getDeclaredConstructor(Looper.class, Handler$Callback.class, Boolean.TYPE).newInstance(looper, handler$Callback, true);
                return handler;
            }
            catch (final InvocationTargetException ex) {
                final Throwable cause = ex.getCause();
                if (cause instanceof RuntimeException) {
                    throw (RuntimeException)cause;
                }
                if (cause instanceof Error) {
                    throw (Error)cause;
                }
                throw new RuntimeException(cause);
            }
            catch (final NoSuchMethodException handler) {}
            catch (final InstantiationException handler) {}
            catch (final IllegalAccessException ex2) {}
            Log.w("HandlerCompat", "Unable to invoke Handler(Looper, Callback, boolean) constructor", (Throwable)handler);
        }
        return new Handler(looper, handler$Callback);
    }
    
    public static boolean hasCallbacks(final Handler obj, final Runnable runnable) {
        if (Build$VERSION.SDK_INT >= 29) {
            return Api29Impl.hasCallbacks(obj, runnable);
        }
        if (Build$VERSION.SDK_INT >= 16) {
            try {
                return (boolean)Handler.class.getMethod("hasCallbacks", Runnable.class).invoke(obj, runnable);
            }
            catch (final NullPointerException ex) {
                throw new UnsupportedOperationException("Failed to call Handler.hasCallbacks(), but there is no safe failure mode for this method. Raising exception.", ex);
            }
            catch (final NoSuchMethodException ex) {
                throw new UnsupportedOperationException("Failed to call Handler.hasCallbacks(), but there is no safe failure mode for this method. Raising exception.", ex);
            }
            catch (final IllegalAccessException ex) {
                throw new UnsupportedOperationException("Failed to call Handler.hasCallbacks(), but there is no safe failure mode for this method. Raising exception.", ex);
            }
            catch (final InvocationTargetException ex2) {
                final Throwable cause = ex2.getCause();
                if (cause instanceof RuntimeException) {
                    throw (RuntimeException)cause;
                }
                if (cause instanceof Error) {
                    throw (Error)cause;
                }
                throw new RuntimeException(cause);
            }
        }
        final NullPointerException ex = null;
        throw new UnsupportedOperationException("Failed to call Handler.hasCallbacks(), but there is no safe failure mode for this method. Raising exception.", ex);
    }
    
    public static boolean postDelayed(final Handler handler, final Runnable runnable, final Object obj, final long n) {
        if (Build$VERSION.SDK_INT >= 28) {
            return Api28Impl.postDelayed(handler, runnable, obj, n);
        }
        final Message obtain = Message.obtain(handler, runnable);
        obtain.obj = obj;
        return handler.sendMessageDelayed(obtain, n);
    }
    
    private static class Api28Impl
    {
        public static Handler createAsync(final Looper looper) {
            return Handler.createAsync(looper);
        }
        
        public static Handler createAsync(final Looper looper, final Handler$Callback handler$Callback) {
            return Handler.createAsync(looper, handler$Callback);
        }
        
        public static boolean postDelayed(final Handler handler, final Runnable runnable, final Object o, final long n) {
            return handler.postDelayed(runnable, o, n);
        }
    }
    
    private static class Api29Impl
    {
        public static boolean hasCallbacks(final Handler handler, final Runnable runnable) {
            return handler.hasCallbacks(runnable);
        }
    }
}

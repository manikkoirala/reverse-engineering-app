// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.os;

import android.util.SparseArray;
import java.io.Serializable;
import android.os.Parcelable$Creator;
import java.lang.reflect.Array;
import android.os.BadParcelableException;
import android.os.Parcelable;
import java.util.Map;
import java.util.List;
import java.util.HashMap;
import java.util.ArrayList;
import android.os.Build$VERSION;
import android.os.Parcel;

public final class ParcelCompat
{
    private ParcelCompat() {
    }
    
    public static <T> Object[] readArray(final Parcel parcel, final ClassLoader classLoader, final Class<T> clazz) {
        if (Build$VERSION.SDK_INT >= 34) {
            return Api33Impl.readArray(parcel, classLoader, clazz);
        }
        return parcel.readArray(classLoader);
    }
    
    public static <T> ArrayList<T> readArrayList(final Parcel parcel, final ClassLoader classLoader, final Class<? extends T> clazz) {
        if (Build$VERSION.SDK_INT >= 34) {
            return Api33Impl.readArrayList(parcel, classLoader, clazz);
        }
        return parcel.readArrayList(classLoader);
    }
    
    public static boolean readBoolean(final Parcel parcel) {
        return parcel.readInt() != 0;
    }
    
    public static <K, V> HashMap<K, V> readHashMap(final Parcel parcel, final ClassLoader classLoader, final Class<? extends K> clazz, final Class<? extends V> clazz2) {
        if (Build$VERSION.SDK_INT >= 34) {
            return Api33Impl.readHashMap(parcel, classLoader, clazz, clazz2);
        }
        return parcel.readHashMap(classLoader);
    }
    
    public static <T> void readList(final Parcel parcel, final List<? super T> list, final ClassLoader classLoader, final Class<T> clazz) {
        if (Build$VERSION.SDK_INT >= 34) {
            Api33Impl.readList(parcel, list, classLoader, clazz);
        }
        else {
            parcel.readList((List)list, classLoader);
        }
    }
    
    public static <K, V> void readMap(final Parcel parcel, final Map<? super K, ? super V> map, final ClassLoader classLoader, final Class<K> clazz, final Class<V> clazz2) {
        if (Build$VERSION.SDK_INT >= 34) {
            Api33Impl.readMap(parcel, map, classLoader, clazz, clazz2);
        }
        else {
            parcel.readMap((Map)map, classLoader);
        }
    }
    
    public static <T extends Parcelable> T readParcelable(final Parcel parcel, final ClassLoader classLoader, final Class<T> clazz) {
        if (Build$VERSION.SDK_INT >= 34) {
            return Api33Impl.readParcelable(parcel, classLoader, clazz);
        }
        final Parcelable parcelable = parcel.readParcelable(classLoader);
        if (parcelable != null && !clazz.isInstance(parcelable)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Parcelable ");
            sb.append(parcelable.getClass());
            sb.append(" is not a subclass of required class ");
            sb.append(clazz.getName());
            sb.append(" provided in the parameter");
            throw new BadParcelableException(sb.toString());
        }
        return (T)parcelable;
    }
    
    @Deprecated
    public static <T> T[] readParcelableArray(final Parcel parcel, final ClassLoader classLoader, final Class<T> componentType) {
        if (Build$VERSION.SDK_INT >= 34) {
            return Api33Impl.readParcelableArray(parcel, classLoader, componentType);
        }
        final Parcelable[] parcelableArray = parcel.readParcelableArray(classLoader);
        if (componentType.isAssignableFrom(Parcelable.class)) {
            return (T[])parcelableArray;
        }
        final Object[] array = (Object[])Array.newInstance(componentType, parcelableArray.length);
        int i = 0;
        while (i < parcelableArray.length) {
            try {
                array[i] = componentType.cast(parcelableArray[i]);
                ++i;
                continue;
            }
            catch (final ClassCastException ex) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Parcelable at index ");
                sb.append(i);
                sb.append(" is not a subclass of required class ");
                sb.append(componentType.getName());
                sb.append(" provided in the parameter");
                throw new BadParcelableException(sb.toString());
            }
            break;
        }
        return (T[])array;
    }
    
    public static <T> Parcelable[] readParcelableArrayTyped(final Parcel parcel, final ClassLoader classLoader, final Class<T> clazz) {
        if (Build$VERSION.SDK_INT >= 34) {
            return Api33Impl.readParcelableArray(parcel, classLoader, (Class<Parcelable>)clazz);
        }
        return parcel.readParcelableArray(classLoader);
    }
    
    public static <T> Parcelable$Creator<T> readParcelableCreator(final Parcel parcel, final ClassLoader classLoader, final Class<T> clazz) {
        if (Build$VERSION.SDK_INT >= 34) {
            return Api33Impl.readParcelableCreator(parcel, classLoader, clazz);
        }
        return (Parcelable$Creator<T>)Api30Impl.readParcelableCreator(parcel, classLoader);
    }
    
    public static <T> List<T> readParcelableList(final Parcel parcel, final List<T> list, final ClassLoader classLoader, final Class<T> clazz) {
        if (Build$VERSION.SDK_INT >= 34) {
            return Api33Impl.readParcelableList(parcel, list, classLoader, clazz);
        }
        return Api29Impl.readParcelableList(parcel, list, classLoader);
    }
    
    public static <T extends Serializable> T readSerializable(final Parcel parcel, final ClassLoader classLoader, final Class<T> clazz) {
        if (Build$VERSION.SDK_INT >= 33) {
            return Api33Impl.readSerializable(parcel, classLoader, clazz);
        }
        return (T)parcel.readSerializable();
    }
    
    public static <T> SparseArray<T> readSparseArray(final Parcel parcel, final ClassLoader classLoader, final Class<? extends T> clazz) {
        if (Build$VERSION.SDK_INT >= 34) {
            return Api33Impl.readSparseArray(parcel, classLoader, clazz);
        }
        return (SparseArray<T>)parcel.readSparseArray(classLoader);
    }
    
    public static void writeBoolean(final Parcel parcel, final boolean b) {
        parcel.writeInt((int)(b ? 1 : 0));
    }
    
    static class Api29Impl
    {
        private Api29Impl() {
        }
        
        static <T extends Parcelable> List<T> readParcelableList(final Parcel parcel, final List<T> list, final ClassLoader classLoader) {
            return parcel.readParcelableList((List)list, classLoader);
        }
    }
    
    static class Api30Impl
    {
        private Api30Impl() {
        }
        
        static Parcelable$Creator<?> readParcelableCreator(final Parcel parcel, final ClassLoader classLoader) {
            return (Parcelable$Creator<?>)parcel.readParcelableCreator(classLoader);
        }
    }
    
    static class Api33Impl
    {
        private Api33Impl() {
        }
        
        static <T> T[] readArray(final Parcel parcel, final ClassLoader classLoader, final Class<T> clazz) {
            return (T[])parcel.readArray(classLoader, (Class)clazz);
        }
        
        static <T> ArrayList<T> readArrayList(final Parcel parcel, final ClassLoader classLoader, final Class<? extends T> clazz) {
            return parcel.readArrayList(classLoader, (Class)clazz);
        }
        
        static <V, K> HashMap<K, V> readHashMap(final Parcel parcel, final ClassLoader classLoader, final Class<? extends K> clazz, final Class<? extends V> clazz2) {
            return parcel.readHashMap(classLoader, (Class)clazz, (Class)clazz2);
        }
        
        static <T> void readList(final Parcel parcel, final List<? super T> list, final ClassLoader classLoader, final Class<T> clazz) {
            parcel.readList((List)list, classLoader, (Class)clazz);
        }
        
        static <K, V> void readMap(final Parcel parcel, final Map<? super K, ? super V> map, final ClassLoader classLoader, final Class<K> clazz, final Class<V> clazz2) {
            parcel.readMap((Map)map, classLoader, (Class)clazz, (Class)clazz2);
        }
        
        static <T extends Parcelable> T readParcelable(final Parcel parcel, final ClassLoader classLoader, final Class<T> clazz) {
            return (T)parcel.readParcelable(classLoader, (Class)clazz);
        }
        
        static <T> T[] readParcelableArray(final Parcel parcel, final ClassLoader classLoader, final Class<T> clazz) {
            return (T[])parcel.readParcelableArray(classLoader, (Class)clazz);
        }
        
        static <T> Parcelable$Creator<T> readParcelableCreator(final Parcel parcel, final ClassLoader classLoader, final Class<T> clazz) {
            return (Parcelable$Creator<T>)parcel.readParcelableCreator(classLoader, (Class)clazz);
        }
        
        static <T> List<T> readParcelableList(final Parcel parcel, final List<T> list, final ClassLoader classLoader, final Class<T> clazz) {
            return parcel.readParcelableList((List)list, classLoader, (Class)clazz);
        }
        
        static <T extends Serializable> T readSerializable(final Parcel parcel, final ClassLoader classLoader, final Class<T> clazz) {
            return (T)parcel.readSerializable(classLoader, (Class)clazz);
        }
        
        static <T> SparseArray<T> readSparseArray(final Parcel parcel, final ClassLoader classLoader, final Class<? extends T> clazz) {
            return (SparseArray<T>)parcel.readSparseArray(classLoader, (Class)clazz);
        }
    }
}

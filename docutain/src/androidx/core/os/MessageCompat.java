// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.os;

import android.os.Build$VERSION;
import android.os.Message;

public final class MessageCompat
{
    private static boolean sTryIsAsynchronous = true;
    private static boolean sTrySetAsynchronous = true;
    
    private MessageCompat() {
    }
    
    public static boolean isAsynchronous(final Message message) {
        if (Build$VERSION.SDK_INT >= 22) {
            return Api22Impl.isAsynchronous(message);
        }
        if (MessageCompat.sTryIsAsynchronous && Build$VERSION.SDK_INT >= 16) {
            try {
                return Api22Impl.isAsynchronous(message);
            }
            catch (final NoSuchMethodError noSuchMethodError) {
                MessageCompat.sTryIsAsynchronous = false;
            }
        }
        return false;
    }
    
    public static void setAsynchronous(final Message message, final boolean b) {
        if (Build$VERSION.SDK_INT >= 22) {
            Api22Impl.setAsynchronous(message, b);
            return;
        }
        if (MessageCompat.sTrySetAsynchronous && Build$VERSION.SDK_INT >= 16) {
            try {
                Api22Impl.setAsynchronous(message, b);
            }
            catch (final NoSuchMethodError noSuchMethodError) {
                MessageCompat.sTrySetAsynchronous = false;
            }
        }
    }
    
    static class Api22Impl
    {
        private Api22Impl() {
        }
        
        static boolean isAsynchronous(final Message message) {
            return message.isAsynchronous();
        }
        
        static void setAsynchronous(final Message message, final boolean asynchronous) {
            message.setAsynchronous(asynchronous);
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.os;

import kotlin.jvm.JvmStatic;
import android.os.PersistableBundle;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0018\n\u0000\b\u00c3\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\"\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\b\u0010\u0007\u001a\u0004\u0018\u00010\b2\u0006\u0010\t\u001a\u00020\nH\u0007J\"\u0010\u000b\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\b\u0010\u0007\u001a\u0004\u0018\u00010\b2\u0006\u0010\t\u001a\u00020\fH\u0007¨\u0006\r" }, d2 = { "Landroidx/core/os/PersistableBundleApi22ImplKt;", "", "()V", "putBoolean", "", "persistableBundle", "Landroid/os/PersistableBundle;", "key", "", "value", "", "putBooleanArray", "", "core-ktx_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
final class PersistableBundleApi22ImplKt
{
    public static final PersistableBundleApi22ImplKt INSTANCE;
    
    static {
        INSTANCE = new PersistableBundleApi22ImplKt();
    }
    
    private PersistableBundleApi22ImplKt() {
    }
    
    @JvmStatic
    public static final void putBoolean(final PersistableBundle persistableBundle, final String s, final boolean b) {
        persistableBundle.putBoolean(s, b);
    }
    
    @JvmStatic
    public static final void putBooleanArray(final PersistableBundle persistableBundle, final String s, final boolean[] array) {
        persistableBundle.putBooleanArray(s, array);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.os;

import android.os.Parcelable$Creator;
import android.os.Parcelable;
import android.os.Parcel;
import android.os.Binder;
import android.os.IBinder;
import android.os.RemoteException;
import android.os.Bundle;
import android.os.IInterface;

public interface IResultReceiver extends IInterface
{
    public static final String DESCRIPTOR = "android$support$v4$os$IResultReceiver".replace('$', '.');
    
    void send(final int p0, final Bundle p1) throws RemoteException;
    
    public static class Default implements IResultReceiver
    {
        public IBinder asBinder() {
            return null;
        }
        
        @Override
        public void send(final int n, final Bundle bundle) throws RemoteException {
        }
    }
    
    public abstract static class Stub extends Binder implements IResultReceiver
    {
        static final int TRANSACTION_send = 1;
        
        public Stub() {
            this.attachInterface((IInterface)this, Stub.DESCRIPTOR);
        }
        
        public static IResultReceiver asInterface(final IBinder binder) {
            if (binder == null) {
                return null;
            }
            final IInterface queryLocalInterface = binder.queryLocalInterface(Stub.DESCRIPTOR);
            if (queryLocalInterface != null && queryLocalInterface instanceof IResultReceiver) {
                return (IResultReceiver)queryLocalInterface;
            }
            return new Proxy(binder);
        }
        
        public IBinder asBinder() {
            return (IBinder)this;
        }
        
        public boolean onTransact(final int n, final Parcel parcel, final Parcel parcel2, final int n2) throws RemoteException {
            final String descriptor = Stub.DESCRIPTOR;
            if (n >= 1 && n <= 16777215) {
                parcel.enforceInterface(descriptor);
            }
            if (n == 1598968902) {
                parcel2.writeString(descriptor);
                return true;
            }
            if (n != 1) {
                return super.onTransact(n, parcel, parcel2, n2);
            }
            this.send(parcel.readInt(), (Bundle)readTypedObject(parcel, (android.os.Parcelable$Creator<Object>)Bundle.CREATOR));
            return true;
        }
        
        private static class Proxy implements IResultReceiver
        {
            private IBinder mRemote;
            
            Proxy(final IBinder mRemote) {
                this.mRemote = mRemote;
            }
            
            public IBinder asBinder() {
                return this.mRemote;
            }
            
            public String getInterfaceDescriptor() {
                return Proxy.DESCRIPTOR;
            }
            
            @Override
            public void send(final int n, final Bundle bundle) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Proxy.DESCRIPTOR);
                    obtain.writeInt(n);
                    writeTypedObject(obtain, (Parcelable)bundle, 0);
                    this.mRemote.transact(1, obtain, (Parcel)null, 1);
                }
                finally {
                    obtain.recycle();
                }
            }
        }
    }
    
    public static class _Parcel
    {
        private static <T> T readTypedObject(final Parcel parcel, final Parcelable$Creator<T> parcelable$Creator) {
            if (parcel.readInt() != 0) {
                return (T)parcelable$Creator.createFromParcel(parcel);
            }
            return null;
        }
        
        private static <T extends Parcelable> void writeTypedObject(final Parcel parcel, final T t, final int n) {
            if (t != null) {
                parcel.writeInt(1);
                t.writeToParcel(parcel, n);
            }
            else {
                parcel.writeInt(0);
            }
        }
    }
}

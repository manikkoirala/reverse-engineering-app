// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.os;

import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import android.os.Handler;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000$\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\u001a1\u0010\u0000\u001a\u00020\u0001*\u00020\u00022\u0006\u0010\u0003\u001a\u00020\u00042\n\b\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u00062\u000e\b\u0004\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\t0\bH\u0086\b\u001a1\u0010\n\u001a\u00020\u0001*\u00020\u00022\u0006\u0010\u000b\u001a\u00020\u00042\n\b\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u00062\u000e\b\u0004\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\t0\bH\u0086\b¨\u0006\f" }, d2 = { "postAtTime", "Ljava/lang/Runnable;", "Landroid/os/Handler;", "uptimeMillis", "", "token", "", "action", "Lkotlin/Function0;", "", "postDelayed", "delayInMillis", "core-ktx_release" }, k = 2, mv = { 1, 8, 0 }, xi = 48)
public final class HandlerKt
{
    public static final Runnable postAtTime(final Handler handler, final long n, final Object o, final Function0<Unit> function0) {
        final Runnable runnable = (Runnable)new HandlerKt$postAtTime$runnable.HandlerKt$postAtTime$runnable$1((Function0)function0);
        handler.postAtTime(runnable, o, n);
        return runnable;
    }
    
    public static final Runnable postDelayed(final Handler handler, final long n, final Object o, final Function0<Unit> function0) {
        final Runnable runnable = (Runnable)new HandlerKt$postDelayed$runnable.HandlerKt$postDelayed$runnable$1((Function0)function0);
        if (o == null) {
            handler.postDelayed(runnable, n);
        }
        else {
            HandlerCompat.postDelayed(handler, runnable, o, n);
        }
        return runnable;
    }
}

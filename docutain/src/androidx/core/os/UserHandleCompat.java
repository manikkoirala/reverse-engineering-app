// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.os;

import java.lang.reflect.InvocationTargetException;
import android.os.Build$VERSION;
import android.os.UserHandle;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;

public class UserHandleCompat
{
    private static Method sGetUserIdMethod;
    private static Constructor<UserHandle> sUserHandleConstructor;
    
    private UserHandleCompat() {
    }
    
    private static Method getGetUserIdMethod() throws NoSuchMethodException {
        if (UserHandleCompat.sGetUserIdMethod == null) {
            (UserHandleCompat.sGetUserIdMethod = UserHandle.class.getDeclaredMethod("getUserId", Integer.TYPE)).setAccessible(true);
        }
        return UserHandleCompat.sGetUserIdMethod;
    }
    
    private static Constructor<UserHandle> getUserHandleConstructor() throws NoSuchMethodException {
        if (UserHandleCompat.sUserHandleConstructor == null) {
            (UserHandleCompat.sUserHandleConstructor = UserHandle.class.getDeclaredConstructor(Integer.TYPE)).setAccessible(true);
        }
        return UserHandleCompat.sUserHandleConstructor;
    }
    
    public static UserHandle getUserHandleForUid(final int i) {
        if (Build$VERSION.SDK_INT >= 24) {
            return Api24Impl.getUserHandleForUid(i);
        }
        try {
            return getUserHandleConstructor().newInstance(getGetUserIdMethod().invoke(null, i));
        }
        catch (final InvocationTargetException cause) {
            throw new RuntimeException(cause);
        }
        catch (final InstantiationException cause2) {
            final InstantiationError instantiationError = new InstantiationError();
            instantiationError.initCause(cause2);
            throw instantiationError;
        }
        catch (final IllegalAccessException cause3) {
            final IllegalAccessError illegalAccessError = new IllegalAccessError();
            illegalAccessError.initCause(cause3);
            throw illegalAccessError;
        }
        catch (final NoSuchMethodException cause4) {
            final NoSuchMethodError noSuchMethodError = new NoSuchMethodError();
            noSuchMethodError.initCause(cause4);
            throw noSuchMethodError;
        }
    }
    
    private static class Api24Impl
    {
        static UserHandle getUserHandleForUid(final int n) {
            return UserHandle.getUserHandleForUid(n);
        }
    }
}

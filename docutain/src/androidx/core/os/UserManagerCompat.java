// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.os;

import android.os.UserManager;
import android.os.Build$VERSION;
import android.content.Context;

public class UserManagerCompat
{
    private UserManagerCompat() {
    }
    
    public static boolean isUserUnlocked(final Context context) {
        return Build$VERSION.SDK_INT < 24 || Api24Impl.isUserUnlocked(context);
    }
    
    static class Api24Impl
    {
        private Api24Impl() {
        }
        
        static boolean isUserUnlocked(final Context context) {
            return ((UserManager)context.getSystemService((Class)UserManager.class)).isUserUnlocked();
        }
    }
}

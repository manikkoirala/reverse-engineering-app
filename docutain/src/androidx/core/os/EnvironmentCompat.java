// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.os;

import java.io.IOException;
import android.util.Log;
import android.os.Environment;
import android.os.Build$VERSION;
import java.io.File;

public final class EnvironmentCompat
{
    public static final String MEDIA_UNKNOWN = "unknown";
    private static final String TAG = "EnvironmentCompat";
    
    private EnvironmentCompat() {
    }
    
    public static String getStorageState(final File file) {
        if (Build$VERSION.SDK_INT >= 21) {
            return Api21Impl.getExternalStorageState(file);
        }
        if (Build$VERSION.SDK_INT >= 19) {
            return Api19Impl.getStorageState(file);
        }
        try {
            if (file.getCanonicalPath().startsWith(Environment.getExternalStorageDirectory().getCanonicalPath())) {
                return Environment.getExternalStorageState();
            }
        }
        catch (final IOException obj) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Failed to resolve canonical path: ");
            sb.append(obj);
            Log.w("EnvironmentCompat", sb.toString());
        }
        return "unknown";
    }
    
    static class Api19Impl
    {
        private Api19Impl() {
        }
        
        static String getStorageState(final File file) {
            return Environment.getStorageState(file);
        }
    }
    
    static class Api21Impl
    {
        private Api21Impl() {
        }
        
        static String getExternalStorageState(final File file) {
            return Environment.getExternalStorageState(file);
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.os;

import java.lang.reflect.InvocationTargetException;
import android.util.Log;
import java.lang.reflect.Method;
import android.util.SparseArray;
import java.util.ArrayList;
import android.os.Parcelable;
import android.os.Build$VERSION;
import android.os.IBinder;
import android.os.Bundle;

public final class BundleCompat
{
    private BundleCompat() {
    }
    
    public static IBinder getBinder(final Bundle bundle, final String s) {
        if (Build$VERSION.SDK_INT >= 18) {
            return Api18Impl.getBinder(bundle, s);
        }
        return BeforeApi18Impl.getBinder(bundle, s);
    }
    
    public static <T> T getParcelable(final Bundle bundle, final String s, final Class<T> clazz) {
        if (Build$VERSION.SDK_INT >= 34) {
            return Api33Impl.getParcelable(bundle, s, clazz);
        }
        Object parcelable = bundle.getParcelable(s);
        if (!clazz.isInstance(parcelable)) {
            parcelable = null;
        }
        return (T)parcelable;
    }
    
    public static Parcelable[] getParcelableArray(final Bundle bundle, final String s, final Class<? extends Parcelable> clazz) {
        if (Build$VERSION.SDK_INT >= 34) {
            return Api33Impl.getParcelableArray(bundle, s, clazz);
        }
        return bundle.getParcelableArray(s);
    }
    
    public static <T> ArrayList<T> getParcelableArrayList(final Bundle bundle, final String s, final Class<? extends T> clazz) {
        if (Build$VERSION.SDK_INT >= 34) {
            return Api33Impl.getParcelableArrayList(bundle, s, clazz);
        }
        return bundle.getParcelableArrayList(s);
    }
    
    public static <T> SparseArray<T> getSparseParcelableArray(final Bundle bundle, final String s, final Class<? extends T> clazz) {
        if (Build$VERSION.SDK_INT >= 34) {
            return Api33Impl.getSparseParcelableArray(bundle, s, clazz);
        }
        return (SparseArray<T>)bundle.getSparseParcelableArray(s);
    }
    
    public static void putBinder(final Bundle bundle, final String s, final IBinder binder) {
        if (Build$VERSION.SDK_INT >= 18) {
            Api18Impl.putBinder(bundle, s, binder);
        }
        else {
            BeforeApi18Impl.putBinder(bundle, s, binder);
        }
    }
    
    static class Api18Impl
    {
        private Api18Impl() {
        }
        
        static IBinder getBinder(final Bundle bundle, final String s) {
            return bundle.getBinder(s);
        }
        
        static void putBinder(final Bundle bundle, final String s, final IBinder binder) {
            bundle.putBinder(s, binder);
        }
    }
    
    static class Api33Impl
    {
        private Api33Impl() {
        }
        
        static <T> T getParcelable(final Bundle bundle, final String s, final Class<T> clazz) {
            return (T)bundle.getParcelable(s, (Class)clazz);
        }
        
        static <T> T[] getParcelableArray(final Bundle bundle, final String s, final Class<T> clazz) {
            return (T[])bundle.getParcelableArray(s, (Class)clazz);
        }
        
        static <T> ArrayList<T> getParcelableArrayList(final Bundle bundle, final String s, final Class<? extends T> clazz) {
            return bundle.getParcelableArrayList(s, (Class)clazz);
        }
        
        static <T> SparseArray<T> getSparseParcelableArray(final Bundle bundle, final String s, final Class<? extends T> clazz) {
            return (SparseArray<T>)bundle.getSparseParcelableArray(s, (Class)clazz);
        }
    }
    
    static class BeforeApi18Impl
    {
        private static final String TAG = "BundleCompat";
        private static Method sGetIBinderMethod;
        private static boolean sGetIBinderMethodFetched;
        private static Method sPutIBinderMethod;
        private static boolean sPutIBinderMethodFetched;
        
        private BeforeApi18Impl() {
        }
        
        public static IBinder getBinder(Bundle obj, final String s) {
            if (!BeforeApi18Impl.sGetIBinderMethodFetched) {
                try {
                    (BeforeApi18Impl.sGetIBinderMethod = Bundle.class.getMethod("getIBinder", String.class)).setAccessible(true);
                }
                catch (final NoSuchMethodException ex) {
                    Log.i("BundleCompat", "Failed to retrieve getIBinder method", (Throwable)ex);
                }
                BeforeApi18Impl.sGetIBinderMethodFetched = true;
            }
            final Method sGetIBinderMethod = BeforeApi18Impl.sGetIBinderMethod;
            if (sGetIBinderMethod != null) {
                try {
                    obj = (IllegalArgumentException)sGetIBinderMethod.invoke(obj, s);
                    return (IBinder)obj;
                }
                catch (final IllegalArgumentException obj) {}
                catch (final IllegalAccessException obj) {}
                catch (final InvocationTargetException ex2) {}
                Log.i("BundleCompat", "Failed to invoke getIBinder via reflection", (Throwable)obj);
                BeforeApi18Impl.sGetIBinderMethod = null;
            }
            return null;
        }
        
        public static void putBinder(final Bundle obj, final String s, final IBinder binder) {
            if (!BeforeApi18Impl.sPutIBinderMethodFetched) {
                try {
                    (BeforeApi18Impl.sPutIBinderMethod = Bundle.class.getMethod("putIBinder", String.class, IBinder.class)).setAccessible(true);
                }
                catch (final NoSuchMethodException ex) {
                    Log.i("BundleCompat", "Failed to retrieve putIBinder method", (Throwable)ex);
                }
                BeforeApi18Impl.sPutIBinderMethodFetched = true;
            }
            final Method sPutIBinderMethod = BeforeApi18Impl.sPutIBinderMethod;
            if (sPutIBinderMethod != null) {
                try {
                    sPutIBinderMethod.invoke(obj, s, binder);
                    return;
                }
                catch (final IllegalArgumentException obj) {}
                catch (final IllegalAccessException obj) {}
                catch (final InvocationTargetException ex2) {}
                Log.i("BundleCompat", "Failed to invoke putIBinder via reflection", (Throwable)obj);
                BeforeApi18Impl.sPutIBinderMethod = null;
            }
        }
    }
}

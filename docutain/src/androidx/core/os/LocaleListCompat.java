// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.os;

import androidx.core.text.ICUCompat;
import android.os.LocaleList;
import android.os.Build$VERSION;
import java.util.Locale;

public final class LocaleListCompat
{
    private static final LocaleListCompat sEmptyLocaleList;
    private final LocaleListInterface mImpl;
    
    static {
        sEmptyLocaleList = create(new Locale[0]);
    }
    
    private LocaleListCompat(final LocaleListInterface mImpl) {
        this.mImpl = mImpl;
    }
    
    public static LocaleListCompat create(final Locale... array) {
        if (Build$VERSION.SDK_INT >= 24) {
            return wrap(Api24Impl.createLocaleList(array));
        }
        return new LocaleListCompat(new LocaleListCompatWrapper(array));
    }
    
    static Locale forLanguageTagCompat(final String s) {
        if (s.contains("-")) {
            final String[] split = s.split("-", -1);
            if (split.length > 2) {
                return new Locale(split[0], split[1], split[2]);
            }
            if (split.length > 1) {
                return new Locale(split[0], split[1]);
            }
            if (split.length == 1) {
                return new Locale(split[0]);
            }
        }
        else {
            if (!s.contains("_")) {
                return new Locale(s);
            }
            final String[] split2 = s.split("_", -1);
            if (split2.length > 2) {
                return new Locale(split2[0], split2[1], split2[2]);
            }
            if (split2.length > 1) {
                return new Locale(split2[0], split2[1]);
            }
            if (split2.length == 1) {
                return new Locale(split2[0]);
            }
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Can not parse language tag: [");
        sb.append(s);
        sb.append("]");
        throw new IllegalArgumentException(sb.toString());
    }
    
    public static LocaleListCompat forLanguageTags(final String s) {
        if (s != null && !s.isEmpty()) {
            final String[] split = s.split(",", -1);
            final int length = split.length;
            final Locale[] array = new Locale[length];
            for (int i = 0; i < length; ++i) {
                Locale locale;
                if (Build$VERSION.SDK_INT >= 21) {
                    locale = Api21Impl.forLanguageTag(split[i]);
                }
                else {
                    locale = forLanguageTagCompat(split[i]);
                }
                array[i] = locale;
            }
            return create(array);
        }
        return getEmptyLocaleList();
    }
    
    public static LocaleListCompat getAdjustedDefault() {
        if (Build$VERSION.SDK_INT >= 24) {
            return wrap(Api24Impl.getAdjustedDefault());
        }
        return create(Locale.getDefault());
    }
    
    public static LocaleListCompat getDefault() {
        if (Build$VERSION.SDK_INT >= 24) {
            return wrap(Api24Impl.getDefault());
        }
        return create(Locale.getDefault());
    }
    
    public static LocaleListCompat getEmptyLocaleList() {
        return LocaleListCompat.sEmptyLocaleList;
    }
    
    public static boolean matchesLanguageAndScript(final Locale locale, final Locale locale2) {
        if (Build$VERSION.SDK_INT >= 33) {
            return LocaleList.matchesLanguageAndScript(locale, locale2);
        }
        if (Build$VERSION.SDK_INT >= 21) {
            return Api21Impl.matchesLanguageAndScript(locale, locale2);
        }
        throw new UnsupportedOperationException("This method is only supported on API level 21+");
    }
    
    public static LocaleListCompat wrap(final LocaleList list) {
        return new LocaleListCompat(new LocaleListPlatformWrapper(list));
    }
    
    @Deprecated
    public static LocaleListCompat wrap(final Object o) {
        return wrap((LocaleList)o);
    }
    
    @Override
    public boolean equals(final Object o) {
        return o instanceof LocaleListCompat && this.mImpl.equals(((LocaleListCompat)o).mImpl);
    }
    
    public Locale get(final int n) {
        return this.mImpl.get(n);
    }
    
    public Locale getFirstMatch(final String[] array) {
        return this.mImpl.getFirstMatch(array);
    }
    
    @Override
    public int hashCode() {
        return this.mImpl.hashCode();
    }
    
    public int indexOf(final Locale locale) {
        return this.mImpl.indexOf(locale);
    }
    
    public boolean isEmpty() {
        return this.mImpl.isEmpty();
    }
    
    public int size() {
        return this.mImpl.size();
    }
    
    public String toLanguageTags() {
        return this.mImpl.toLanguageTags();
    }
    
    @Override
    public String toString() {
        return this.mImpl.toString();
    }
    
    public Object unwrap() {
        return this.mImpl.getLocaleList();
    }
    
    static class Api21Impl
    {
        private static final Locale[] PSEUDO_LOCALE;
        
        static {
            PSEUDO_LOCALE = new Locale[] { new Locale("en", "XA"), new Locale("ar", "XB") };
        }
        
        private Api21Impl() {
        }
        
        static Locale forLanguageTag(final String languageTag) {
            return Locale.forLanguageTag(languageTag);
        }
        
        private static boolean isPseudoLocale(final Locale obj) {
            final Locale[] pseudo_LOCALE = Api21Impl.PSEUDO_LOCALE;
            for (int length = pseudo_LOCALE.length, i = 0; i < length; ++i) {
                if (pseudo_LOCALE[i].equals(obj)) {
                    return true;
                }
            }
            return false;
        }
        
        static boolean matchesLanguageAndScript(final Locale locale, final Locale obj) {
            final boolean equals = locale.equals(obj);
            final boolean b = true;
            if (equals) {
                return true;
            }
            if (!locale.getLanguage().equals(obj.getLanguage())) {
                return false;
            }
            if (isPseudoLocale(locale) || isPseudoLocale(obj)) {
                return false;
            }
            final String maximizeAndGetScript = ICUCompat.maximizeAndGetScript(locale);
            if (maximizeAndGetScript.isEmpty()) {
                final String country = locale.getCountry();
                boolean b2 = b;
                if (!country.isEmpty()) {
                    b2 = (country.equals(obj.getCountry()) && b);
                }
                return b2;
            }
            return maximizeAndGetScript.equals(ICUCompat.maximizeAndGetScript(obj));
        }
    }
    
    static class Api24Impl
    {
        private Api24Impl() {
        }
        
        static LocaleList createLocaleList(final Locale... array) {
            return new LocaleList(array);
        }
        
        static LocaleList getAdjustedDefault() {
            return LocaleList.getAdjustedDefault();
        }
        
        static LocaleList getDefault() {
            return LocaleList.getDefault();
        }
    }
}

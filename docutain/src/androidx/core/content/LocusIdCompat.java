// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.content;

import android.os.Build$VERSION;
import androidx.core.util.Preconditions;
import android.content.LocusId;

public final class LocusIdCompat
{
    private final String mId;
    private final LocusId mWrapped;
    
    public LocusIdCompat(final String s) {
        this.mId = Preconditions.checkStringNotEmpty(s, (Object)"id cannot be empty");
        if (Build$VERSION.SDK_INT >= 29) {
            this.mWrapped = Api29Impl.create(s);
        }
        else {
            this.mWrapped = null;
        }
    }
    
    private String getSanitizedId() {
        final int length = this.mId.length();
        final StringBuilder sb = new StringBuilder();
        sb.append(length);
        sb.append("_chars");
        return sb.toString();
    }
    
    public static LocusIdCompat toLocusIdCompat(final LocusId locusId) {
        Preconditions.checkNotNull(locusId, "locusId cannot be null");
        return new LocusIdCompat(Preconditions.checkStringNotEmpty(Api29Impl.getId(locusId), (Object)"id cannot be empty"));
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (this == o) {
            return true;
        }
        if (o == null) {
            return false;
        }
        if (this.getClass() != o.getClass()) {
            return false;
        }
        final LocusIdCompat locusIdCompat = (LocusIdCompat)o;
        final String mId = this.mId;
        if (mId == null) {
            if (locusIdCompat.mId != null) {
                b = false;
            }
            return b;
        }
        return mId.equals(locusIdCompat.mId);
    }
    
    public String getId() {
        return this.mId;
    }
    
    @Override
    public int hashCode() {
        final String mId = this.mId;
        int hashCode;
        if (mId == null) {
            hashCode = 0;
        }
        else {
            hashCode = mId.hashCode();
        }
        return 31 + hashCode;
    }
    
    public LocusId toLocusId() {
        return this.mWrapped;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("LocusIdCompat[");
        sb.append(this.getSanitizedId());
        sb.append("]");
        return sb.toString();
    }
    
    private static class Api29Impl
    {
        static LocusId create(final String s) {
            return new LocusId(s);
        }
        
        static String getId(final LocusId locusId) {
            return locusId.getId();
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.content;

import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import android.util.Log;
import androidx.core.os.UserManagerCompat;
import androidx.concurrent.futures.ResolvableFuture;
import com.google.common.util.concurrent.ListenableFuture;
import android.content.Context;
import java.util.Iterator;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.content.Intent;
import android.os.Build$VERSION;
import android.content.pm.PackageManager;

public final class PackageManagerCompat
{
    public static final String ACTION_PERMISSION_REVOCATION_SETTINGS = "android.intent.action.AUTO_REVOKE_PERMISSIONS";
    public static final String LOG_TAG = "PackageManagerCompat";
    
    private PackageManagerCompat() {
    }
    
    public static boolean areUnusedAppRestrictionsAvailable(final PackageManager packageManager) {
        final int sdk_INT = Build$VERSION.SDK_INT;
        final boolean b = true;
        final boolean b2 = sdk_INT >= 30;
        final boolean b3 = Build$VERSION.SDK_INT >= 23 && Build$VERSION.SDK_INT < 30;
        final boolean b4 = getPermissionRevocationVerifierApp(packageManager) != null;
        boolean b5 = b;
        if (!b2) {
            b5 = (b3 && b4 && b);
        }
        return b5;
    }
    
    public static String getPermissionRevocationVerifierApp(final PackageManager packageManager) {
        final Intent intent = new Intent("android.intent.action.AUTO_REVOKE_PERMISSIONS");
        String s = null;
        final Iterator iterator = packageManager.queryIntentActivities(intent.setData(Uri.fromParts("package", "com.example", (String)null)), 0).iterator();
        while (iterator.hasNext()) {
            final String packageName = ((ResolveInfo)iterator.next()).activityInfo.packageName;
            if (packageManager.checkPermission("android.permission.PACKAGE_VERIFICATION_AGENT", packageName) != 0) {
                continue;
            }
            if (s != null) {
                return s;
            }
            s = packageName;
        }
        return s;
    }
    
    public static ListenableFuture<Integer> getUnusedAppRestrictionsStatus(final Context context) {
        final ResolvableFuture<Object> create = ResolvableFuture.create();
        final boolean userUnlocked = UserManagerCompat.isUserUnlocked(context);
        final Integer value = 0;
        if (!userUnlocked) {
            create.set(value);
            Log.e("PackageManagerCompat", "User is in locked direct boot mode");
            return (ListenableFuture<Integer>)create;
        }
        if (!areUnusedAppRestrictionsAvailable(context.getPackageManager())) {
            create.set(1);
            return (ListenableFuture<Integer>)create;
        }
        final int targetSdkVersion = context.getApplicationInfo().targetSdkVersion;
        if (targetSdkVersion < 30) {
            create.set(value);
            Log.e("PackageManagerCompat", "Target SDK version below API 30");
            return (ListenableFuture<Integer>)create;
        }
        final int sdk_INT = Build$VERSION.SDK_INT;
        int n = 4;
        if (sdk_INT >= 31) {
            if (Api30Impl.areUnusedAppRestrictionsEnabled(context)) {
                if (targetSdkVersion >= 31) {
                    n = 5;
                }
                create.set(n);
            }
            else {
                create.set(2);
            }
            return (ListenableFuture<Integer>)create;
        }
        if (Build$VERSION.SDK_INT == 30) {
            if (!Api30Impl.areUnusedAppRestrictionsEnabled(context)) {
                n = 2;
            }
            create.set(n);
            return (ListenableFuture<Integer>)create;
        }
        final UnusedAppRestrictionsBackportServiceConnection unusedAppRestrictionsBackportServiceConnection = new UnusedAppRestrictionsBackportServiceConnection(context);
        create.addListener(new PackageManagerCompat$$ExternalSyntheticLambda0(unusedAppRestrictionsBackportServiceConnection), Executors.newSingleThreadExecutor());
        unusedAppRestrictionsBackportServiceConnection.connectAndFetchResult((ResolvableFuture<Integer>)create);
        return (ListenableFuture<Integer>)create;
    }
    
    private static class Api30Impl
    {
        static boolean areUnusedAppRestrictionsEnabled(final Context context) {
            return context.getPackageManager().isAutoRevokeWhitelisted() ^ true;
        }
    }
    
    @Retention(RetentionPolicy.SOURCE)
    public @interface UnusedAppRestrictionsStatus {
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.content.pm;

import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;
import androidx.core.util.Preconditions;
import androidx.collection.ArraySet;
import androidx.core.net.UriCompat;
import java.util.HashMap;
import java.util.Collection;
import java.util.HashSet;
import android.net.Uri;
import java.util.Map;
import android.text.TextUtils;
import android.content.pm.ShortcutInfo$Builder;
import java.util.Arrays;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.content.pm.PackageManager$NameNotFoundException;
import android.os.Parcelable;
import android.os.Build$VERSION;
import java.util.Iterator;
import java.util.ArrayList;
import android.content.pm.ShortcutInfo;
import java.util.List;
import android.os.UserHandle;
import android.os.Bundle;
import androidx.core.app.Person;
import androidx.core.content.LocusIdCompat;
import android.content.Intent;
import androidx.core.graphics.drawable.IconCompat;
import android.os.PersistableBundle;
import android.content.Context;
import java.util.Set;
import android.content.ComponentName;

public class ShortcutInfoCompat
{
    private static final String EXTRA_LOCUS_ID = "extraLocusId";
    private static final String EXTRA_LONG_LIVED = "extraLongLived";
    private static final String EXTRA_PERSON_ = "extraPerson_";
    private static final String EXTRA_PERSON_COUNT = "extraPersonCount";
    private static final String EXTRA_SLICE_URI = "extraSliceUri";
    public static final int SURFACE_LAUNCHER = 1;
    ComponentName mActivity;
    Set<String> mCategories;
    Context mContext;
    CharSequence mDisabledMessage;
    int mDisabledReason;
    int mExcludedSurfaces;
    PersistableBundle mExtras;
    boolean mHasKeyFieldsOnly;
    IconCompat mIcon;
    String mId;
    Intent[] mIntents;
    boolean mIsAlwaysBadged;
    boolean mIsCached;
    boolean mIsDeclaredInManifest;
    boolean mIsDynamic;
    boolean mIsEnabled;
    boolean mIsImmutable;
    boolean mIsLongLived;
    boolean mIsPinned;
    CharSequence mLabel;
    long mLastChangedTimestamp;
    LocusIdCompat mLocusId;
    CharSequence mLongLabel;
    String mPackageName;
    Person[] mPersons;
    int mRank;
    Bundle mTransientExtras;
    UserHandle mUser;
    
    ShortcutInfoCompat() {
        this.mIsEnabled = true;
    }
    
    private PersistableBundle buildLegacyExtrasBundle() {
        if (this.mExtras == null) {
            this.mExtras = new PersistableBundle();
        }
        final Person[] mPersons = this.mPersons;
        if (mPersons != null && mPersons.length > 0) {
            this.mExtras.putInt("extraPersonCount", mPersons.length);
            int j;
            for (int i = 0; i < this.mPersons.length; i = j) {
                final PersistableBundle mExtras = this.mExtras;
                final StringBuilder sb = new StringBuilder();
                sb.append("extraPerson_");
                j = i + 1;
                sb.append(j);
                mExtras.putPersistableBundle(sb.toString(), this.mPersons[i].toPersistableBundle());
            }
        }
        final LocusIdCompat mLocusId = this.mLocusId;
        if (mLocusId != null) {
            this.mExtras.putString("extraLocusId", mLocusId.getId());
        }
        this.mExtras.putBoolean("extraLongLived", this.mIsLongLived);
        return this.mExtras;
    }
    
    static List<ShortcutInfoCompat> fromShortcuts(final Context context, final List<ShortcutInfo> list) {
        final ArrayList list2 = new ArrayList(list.size());
        final Iterator iterator = list.iterator();
        while (iterator.hasNext()) {
            list2.add(new Builder(context, (ShortcutInfo)iterator.next()).build());
        }
        return list2;
    }
    
    static LocusIdCompat getLocusId(final ShortcutInfo shortcutInfo) {
        if (Build$VERSION.SDK_INT < 29) {
            return getLocusIdFromExtra(shortcutInfo.getExtras());
        }
        if (shortcutInfo.getLocusId() == null) {
            return null;
        }
        return LocusIdCompat.toLocusIdCompat(shortcutInfo.getLocusId());
    }
    
    private static LocusIdCompat getLocusIdFromExtra(final PersistableBundle persistableBundle) {
        final LocusIdCompat locusIdCompat = null;
        if (persistableBundle == null) {
            return null;
        }
        final String string = persistableBundle.getString("extraLocusId");
        LocusIdCompat locusIdCompat2;
        if (string == null) {
            locusIdCompat2 = locusIdCompat;
        }
        else {
            locusIdCompat2 = new LocusIdCompat(string);
        }
        return locusIdCompat2;
    }
    
    static boolean getLongLivedFromExtra(final PersistableBundle persistableBundle) {
        return persistableBundle != null && persistableBundle.containsKey("extraLongLived") && persistableBundle.getBoolean("extraLongLived");
    }
    
    static Person[] getPersonsFromExtra(final PersistableBundle persistableBundle) {
        if (persistableBundle != null && persistableBundle.containsKey("extraPersonCount")) {
            final int int1 = persistableBundle.getInt("extraPersonCount");
            final Person[] array = new Person[int1];
            int j;
            for (int i = 0; i < int1; i = j) {
                final StringBuilder sb = new StringBuilder();
                sb.append("extraPerson_");
                j = i + 1;
                sb.append(j);
                array[i] = Person.fromPersistableBundle(persistableBundle.getPersistableBundle(sb.toString()));
            }
            return array;
        }
        return null;
    }
    
    Intent addToIntent(final Intent intent) {
        final Intent[] mIntents = this.mIntents;
        intent.putExtra("android.intent.extra.shortcut.INTENT", (Parcelable)mIntents[mIntents.length - 1]).putExtra("android.intent.extra.shortcut.NAME", this.mLabel.toString());
        if (this.mIcon != null) {
            Drawable loadIcon = null;
            final Drawable drawable = null;
            if (this.mIsAlwaysBadged) {
                final PackageManager packageManager = this.mContext.getPackageManager();
                final ComponentName mActivity = this.mActivity;
                Drawable activityIcon = drawable;
                if (mActivity != null) {
                    try {
                        activityIcon = packageManager.getActivityIcon(mActivity);
                    }
                    catch (final PackageManager$NameNotFoundException ex) {
                        activityIcon = drawable;
                    }
                }
                if ((loadIcon = activityIcon) == null) {
                    loadIcon = this.mContext.getApplicationInfo().loadIcon(packageManager);
                }
            }
            this.mIcon.addToShortcutIntent(intent, loadIcon, this.mContext);
        }
        return intent;
    }
    
    public ComponentName getActivity() {
        return this.mActivity;
    }
    
    public Set<String> getCategories() {
        return this.mCategories;
    }
    
    public CharSequence getDisabledMessage() {
        return this.mDisabledMessage;
    }
    
    public int getDisabledReason() {
        return this.mDisabledReason;
    }
    
    public int getExcludedFromSurfaces() {
        return this.mExcludedSurfaces;
    }
    
    public PersistableBundle getExtras() {
        return this.mExtras;
    }
    
    public IconCompat getIcon() {
        return this.mIcon;
    }
    
    public String getId() {
        return this.mId;
    }
    
    public Intent getIntent() {
        final Intent[] mIntents = this.mIntents;
        return mIntents[mIntents.length - 1];
    }
    
    public Intent[] getIntents() {
        final Intent[] mIntents = this.mIntents;
        return Arrays.copyOf(mIntents, mIntents.length);
    }
    
    public long getLastChangedTimestamp() {
        return this.mLastChangedTimestamp;
    }
    
    public LocusIdCompat getLocusId() {
        return this.mLocusId;
    }
    
    public CharSequence getLongLabel() {
        return this.mLongLabel;
    }
    
    public String getPackage() {
        return this.mPackageName;
    }
    
    public int getRank() {
        return this.mRank;
    }
    
    public CharSequence getShortLabel() {
        return this.mLabel;
    }
    
    public Bundle getTransientExtras() {
        return this.mTransientExtras;
    }
    
    public UserHandle getUserHandle() {
        return this.mUser;
    }
    
    public boolean hasKeyFieldsOnly() {
        return this.mHasKeyFieldsOnly;
    }
    
    public boolean isCached() {
        return this.mIsCached;
    }
    
    public boolean isDeclaredInManifest() {
        return this.mIsDeclaredInManifest;
    }
    
    public boolean isDynamic() {
        return this.mIsDynamic;
    }
    
    public boolean isEnabled() {
        return this.mIsEnabled;
    }
    
    public boolean isExcludedFromSurfaces(final int n) {
        return (n & this.mExcludedSurfaces) != 0x0;
    }
    
    public boolean isImmutable() {
        return this.mIsImmutable;
    }
    
    public boolean isPinned() {
        return this.mIsPinned;
    }
    
    public ShortcutInfo toShortcutInfo() {
        final ShortcutInfo$Builder setIntents = new ShortcutInfo$Builder(this.mContext, this.mId).setShortLabel(this.mLabel).setIntents(this.mIntents);
        final IconCompat mIcon = this.mIcon;
        if (mIcon != null) {
            setIntents.setIcon(mIcon.toIcon(this.mContext));
        }
        if (!TextUtils.isEmpty(this.mLongLabel)) {
            setIntents.setLongLabel(this.mLongLabel);
        }
        if (!TextUtils.isEmpty(this.mDisabledMessage)) {
            setIntents.setDisabledMessage(this.mDisabledMessage);
        }
        final ComponentName mActivity = this.mActivity;
        if (mActivity != null) {
            setIntents.setActivity(mActivity);
        }
        final Set<String> mCategories = this.mCategories;
        if (mCategories != null) {
            setIntents.setCategories((Set)mCategories);
        }
        setIntents.setRank(this.mRank);
        final PersistableBundle mExtras = this.mExtras;
        if (mExtras != null) {
            setIntents.setExtras(mExtras);
        }
        if (Build$VERSION.SDK_INT >= 29) {
            final Person[] mPersons = this.mPersons;
            if (mPersons != null && mPersons.length > 0) {
                final int length = mPersons.length;
                final android.app.Person[] persons = new android.app.Person[length];
                for (int i = 0; i < length; ++i) {
                    persons[i] = this.mPersons[i].toAndroidPerson();
                }
                setIntents.setPersons(persons);
            }
            final LocusIdCompat mLocusId = this.mLocusId;
            if (mLocusId != null) {
                setIntents.setLocusId(mLocusId.toLocusId());
            }
            setIntents.setLongLived(this.mIsLongLived);
        }
        else {
            setIntents.setExtras(this.buildLegacyExtrasBundle());
        }
        if (Build$VERSION.SDK_INT >= 33) {
            Api33Impl.setExcludedFromSurfaces(setIntents, this.mExcludedSurfaces);
        }
        return setIntents.build();
    }
    
    private static class Api33Impl
    {
        static void setExcludedFromSurfaces(final ShortcutInfo$Builder shortcutInfo$Builder, final int excludedFromSurfaces) {
            shortcutInfo$Builder.setExcludedFromSurfaces(excludedFromSurfaces);
        }
    }
    
    public static class Builder
    {
        private Map<String, Map<String, List<String>>> mCapabilityBindingParams;
        private Set<String> mCapabilityBindings;
        private final ShortcutInfoCompat mInfo;
        private boolean mIsConversation;
        private Uri mSliceUri;
        
        public Builder(final Context mContext, final ShortcutInfo shortcutInfo) {
            final ShortcutInfoCompat mInfo = new ShortcutInfoCompat();
            this.mInfo = mInfo;
            mInfo.mContext = mContext;
            mInfo.mId = shortcutInfo.getId();
            mInfo.mPackageName = shortcutInfo.getPackage();
            final Intent[] intents = shortcutInfo.getIntents();
            mInfo.mIntents = Arrays.copyOf(intents, intents.length);
            mInfo.mActivity = shortcutInfo.getActivity();
            mInfo.mLabel = shortcutInfo.getShortLabel();
            mInfo.mLongLabel = shortcutInfo.getLongLabel();
            mInfo.mDisabledMessage = shortcutInfo.getDisabledMessage();
            if (Build$VERSION.SDK_INT >= 28) {
                mInfo.mDisabledReason = shortcutInfo.getDisabledReason();
            }
            else {
                int mDisabledReason;
                if (shortcutInfo.isEnabled()) {
                    mDisabledReason = 0;
                }
                else {
                    mDisabledReason = 3;
                }
                mInfo.mDisabledReason = mDisabledReason;
            }
            mInfo.mCategories = shortcutInfo.getCategories();
            mInfo.mPersons = ShortcutInfoCompat.getPersonsFromExtra(shortcutInfo.getExtras());
            mInfo.mUser = shortcutInfo.getUserHandle();
            mInfo.mLastChangedTimestamp = shortcutInfo.getLastChangedTimestamp();
            if (Build$VERSION.SDK_INT >= 30) {
                mInfo.mIsCached = shortcutInfo.isCached();
            }
            mInfo.mIsDynamic = shortcutInfo.isDynamic();
            mInfo.mIsPinned = shortcutInfo.isPinned();
            mInfo.mIsDeclaredInManifest = shortcutInfo.isDeclaredInManifest();
            mInfo.mIsImmutable = shortcutInfo.isImmutable();
            mInfo.mIsEnabled = shortcutInfo.isEnabled();
            mInfo.mHasKeyFieldsOnly = shortcutInfo.hasKeyFieldsOnly();
            mInfo.mLocusId = ShortcutInfoCompat.getLocusId(shortcutInfo);
            mInfo.mRank = shortcutInfo.getRank();
            mInfo.mExtras = shortcutInfo.getExtras();
        }
        
        public Builder(final Context mContext, final String mId) {
            final ShortcutInfoCompat mInfo = new ShortcutInfoCompat();
            this.mInfo = mInfo;
            mInfo.mContext = mContext;
            mInfo.mId = mId;
        }
        
        public Builder(final ShortcutInfoCompat shortcutInfoCompat) {
            final ShortcutInfoCompat mInfo = new ShortcutInfoCompat();
            this.mInfo = mInfo;
            mInfo.mContext = shortcutInfoCompat.mContext;
            mInfo.mId = shortcutInfoCompat.mId;
            mInfo.mPackageName = shortcutInfoCompat.mPackageName;
            mInfo.mIntents = Arrays.copyOf(shortcutInfoCompat.mIntents, shortcutInfoCompat.mIntents.length);
            mInfo.mActivity = shortcutInfoCompat.mActivity;
            mInfo.mLabel = shortcutInfoCompat.mLabel;
            mInfo.mLongLabel = shortcutInfoCompat.mLongLabel;
            mInfo.mDisabledMessage = shortcutInfoCompat.mDisabledMessage;
            mInfo.mDisabledReason = shortcutInfoCompat.mDisabledReason;
            mInfo.mIcon = shortcutInfoCompat.mIcon;
            mInfo.mIsAlwaysBadged = shortcutInfoCompat.mIsAlwaysBadged;
            mInfo.mUser = shortcutInfoCompat.mUser;
            mInfo.mLastChangedTimestamp = shortcutInfoCompat.mLastChangedTimestamp;
            mInfo.mIsCached = shortcutInfoCompat.mIsCached;
            mInfo.mIsDynamic = shortcutInfoCompat.mIsDynamic;
            mInfo.mIsPinned = shortcutInfoCompat.mIsPinned;
            mInfo.mIsDeclaredInManifest = shortcutInfoCompat.mIsDeclaredInManifest;
            mInfo.mIsImmutable = shortcutInfoCompat.mIsImmutable;
            mInfo.mIsEnabled = shortcutInfoCompat.mIsEnabled;
            mInfo.mLocusId = shortcutInfoCompat.mLocusId;
            mInfo.mIsLongLived = shortcutInfoCompat.mIsLongLived;
            mInfo.mHasKeyFieldsOnly = shortcutInfoCompat.mHasKeyFieldsOnly;
            mInfo.mRank = shortcutInfoCompat.mRank;
            if (shortcutInfoCompat.mPersons != null) {
                mInfo.mPersons = Arrays.copyOf(shortcutInfoCompat.mPersons, shortcutInfoCompat.mPersons.length);
            }
            if (shortcutInfoCompat.mCategories != null) {
                mInfo.mCategories = new HashSet<String>(shortcutInfoCompat.mCategories);
            }
            if (shortcutInfoCompat.mExtras != null) {
                mInfo.mExtras = shortcutInfoCompat.mExtras;
            }
            mInfo.mExcludedSurfaces = shortcutInfoCompat.mExcludedSurfaces;
        }
        
        public Builder addCapabilityBinding(final String s) {
            if (this.mCapabilityBindings == null) {
                this.mCapabilityBindings = new HashSet<String>();
            }
            this.mCapabilityBindings.add(s);
            return this;
        }
        
        public Builder addCapabilityBinding(final String s, final String s2, final List<String> list) {
            this.addCapabilityBinding(s);
            if (!list.isEmpty()) {
                if (this.mCapabilityBindingParams == null) {
                    this.mCapabilityBindingParams = new HashMap<String, Map<String, List<String>>>();
                }
                if (this.mCapabilityBindingParams.get(s) == null) {
                    this.mCapabilityBindingParams.put(s, new HashMap<String, List<String>>());
                }
                this.mCapabilityBindingParams.get(s).put(s2, list);
            }
            return this;
        }
        
        public ShortcutInfoCompat build() {
            if (TextUtils.isEmpty(this.mInfo.mLabel)) {
                throw new IllegalArgumentException("Shortcut must have a non-empty label");
            }
            if (this.mInfo.mIntents != null && this.mInfo.mIntents.length != 0) {
                if (this.mIsConversation) {
                    if (this.mInfo.mLocusId == null) {
                        final ShortcutInfoCompat mInfo = this.mInfo;
                        mInfo.mLocusId = new LocusIdCompat(mInfo.mId);
                    }
                    this.mInfo.mIsLongLived = true;
                }
                if (this.mCapabilityBindings != null) {
                    if (this.mInfo.mCategories == null) {
                        this.mInfo.mCategories = new HashSet<String>();
                    }
                    this.mInfo.mCategories.addAll(this.mCapabilityBindings);
                }
                if (Build$VERSION.SDK_INT >= 21) {
                    if (this.mCapabilityBindingParams != null) {
                        if (this.mInfo.mExtras == null) {
                            this.mInfo.mExtras = new PersistableBundle();
                        }
                        for (final String str : this.mCapabilityBindingParams.keySet()) {
                            final Map map = this.mCapabilityBindingParams.get(str);
                            this.mInfo.mExtras.putStringArray(str, (String[])map.keySet().toArray(new String[0]));
                            for (final String str2 : map.keySet()) {
                                final List list = (List)map.get(str2);
                                final PersistableBundle mExtras = this.mInfo.mExtras;
                                final StringBuilder sb = new StringBuilder();
                                sb.append(str);
                                sb.append("/");
                                sb.append(str2);
                                final String string = sb.toString();
                                String[] array;
                                if (list == null) {
                                    array = new String[0];
                                }
                                else {
                                    array = (String[])list.toArray(new String[0]);
                                }
                                mExtras.putStringArray(string, array);
                            }
                        }
                    }
                    if (this.mSliceUri != null) {
                        if (this.mInfo.mExtras == null) {
                            this.mInfo.mExtras = new PersistableBundle();
                        }
                        this.mInfo.mExtras.putString("extraSliceUri", UriCompat.toSafeString(this.mSliceUri));
                    }
                }
                return this.mInfo;
            }
            throw new IllegalArgumentException("Shortcut must have an intent");
        }
        
        public Builder setActivity(final ComponentName mActivity) {
            this.mInfo.mActivity = mActivity;
            return this;
        }
        
        public Builder setAlwaysBadged() {
            this.mInfo.mIsAlwaysBadged = true;
            return this;
        }
        
        public Builder setCategories(final Set<String> set) {
            final ArraySet mCategories = new ArraySet();
            mCategories.addAll(set);
            this.mInfo.mCategories = mCategories;
            return this;
        }
        
        public Builder setDisabledMessage(final CharSequence mDisabledMessage) {
            this.mInfo.mDisabledMessage = mDisabledMessage;
            return this;
        }
        
        public Builder setExcludedFromSurfaces(final int mExcludedSurfaces) {
            this.mInfo.mExcludedSurfaces = mExcludedSurfaces;
            return this;
        }
        
        public Builder setExtras(final PersistableBundle mExtras) {
            this.mInfo.mExtras = mExtras;
            return this;
        }
        
        public Builder setIcon(final IconCompat mIcon) {
            this.mInfo.mIcon = mIcon;
            return this;
        }
        
        public Builder setIntent(final Intent intent) {
            return this.setIntents(new Intent[] { intent });
        }
        
        public Builder setIntents(final Intent[] mIntents) {
            this.mInfo.mIntents = mIntents;
            return this;
        }
        
        public Builder setIsConversation() {
            this.mIsConversation = true;
            return this;
        }
        
        public Builder setLocusId(final LocusIdCompat mLocusId) {
            this.mInfo.mLocusId = mLocusId;
            return this;
        }
        
        public Builder setLongLabel(final CharSequence mLongLabel) {
            this.mInfo.mLongLabel = mLongLabel;
            return this;
        }
        
        @Deprecated
        public Builder setLongLived() {
            this.mInfo.mIsLongLived = true;
            return this;
        }
        
        public Builder setLongLived(final boolean mIsLongLived) {
            this.mInfo.mIsLongLived = mIsLongLived;
            return this;
        }
        
        public Builder setPerson(final Person person) {
            return this.setPersons(new Person[] { person });
        }
        
        public Builder setPersons(final Person[] mPersons) {
            this.mInfo.mPersons = mPersons;
            return this;
        }
        
        public Builder setRank(final int mRank) {
            this.mInfo.mRank = mRank;
            return this;
        }
        
        public Builder setShortLabel(final CharSequence mLabel) {
            this.mInfo.mLabel = mLabel;
            return this;
        }
        
        public Builder setSliceUri(final Uri mSliceUri) {
            this.mSliceUri = mSliceUri;
            return this;
        }
        
        public Builder setTransientExtras(final Bundle bundle) {
            this.mInfo.mTransientExtras = Preconditions.checkNotNull(bundle);
            return this;
        }
    }
    
    @Retention(RetentionPolicy.SOURCE)
    public @interface Surface {
    }
}

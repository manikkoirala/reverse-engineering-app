// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.content.pm;

import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;
import android.content.IntentSender$SendIntentException;
import android.os.Handler;
import android.content.IntentSender$OnFinished;
import android.content.BroadcastReceiver;
import android.content.IntentSender;
import java.util.Objects;
import java.util.Arrays;
import android.text.TextUtils;
import androidx.core.content.ContextCompat;
import java.util.Collections;
import android.os.Bundle;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import androidx.core.util.Preconditions;
import android.util.DisplayMetrics;
import android.app.ActivityManager;
import android.content.pm.ShortcutInfo;
import android.content.Intent;
import java.util.Collection;
import android.graphics.Bitmap;
import java.io.InputStream;
import androidx.core.graphics.drawable.IconCompat;
import android.graphics.BitmapFactory;
import java.util.Iterator;
import android.content.pm.ShortcutManager;
import java.util.ArrayList;
import android.os.Build$VERSION;
import android.content.Context;
import java.util.List;

public class ShortcutManagerCompat
{
    static final String ACTION_INSTALL_SHORTCUT = "com.android.launcher.action.INSTALL_SHORTCUT";
    private static final int DEFAULT_MAX_ICON_DIMENSION_DP = 96;
    private static final int DEFAULT_MAX_ICON_DIMENSION_LOWRAM_DP = 48;
    public static final String EXTRA_SHORTCUT_ID = "android.intent.extra.shortcut.ID";
    public static final int FLAG_MATCH_CACHED = 8;
    public static final int FLAG_MATCH_DYNAMIC = 2;
    public static final int FLAG_MATCH_MANIFEST = 1;
    public static final int FLAG_MATCH_PINNED = 4;
    static final String INSTALL_SHORTCUT_PERMISSION = "com.android.launcher.permission.INSTALL_SHORTCUT";
    private static final String SHORTCUT_LISTENER_INTENT_FILTER_ACTION = "androidx.core.content.pm.SHORTCUT_LISTENER";
    private static final String SHORTCUT_LISTENER_META_DATA_KEY = "androidx.core.content.pm.shortcut_listener_impl";
    private static volatile List<ShortcutInfoChangeListener> sShortcutInfoChangeListeners;
    private static volatile ShortcutInfoCompatSaver<?> sShortcutInfoCompatSaver;
    
    private ShortcutManagerCompat() {
    }
    
    public static boolean addDynamicShortcuts(final Context context, final List<ShortcutInfoCompat> list) {
        final List<ShortcutInfoCompat> removeShortcutsExcludedFromSurface = removeShortcutsExcludedFromSurface(list, 1);
        if (Build$VERSION.SDK_INT <= 29) {
            convertUriIconsToBitmapIcons(context, removeShortcutsExcludedFromSurface);
        }
        if (Build$VERSION.SDK_INT >= 25) {
            final ArrayList list2 = new ArrayList();
            final Iterator<ShortcutInfoCompat> iterator = removeShortcutsExcludedFromSurface.iterator();
            while (iterator.hasNext()) {
                list2.add(iterator.next().toShortcutInfo());
            }
            if (!((ShortcutManager)context.getSystemService((Class)ShortcutManager.class)).addDynamicShortcuts((List)list2)) {
                return false;
            }
        }
        getShortcutInfoSaverInstance(context).addShortcuts(removeShortcutsExcludedFromSurface);
        final Iterator<ShortcutInfoChangeListener> iterator2 = getShortcutInfoListeners(context).iterator();
        while (iterator2.hasNext()) {
            iterator2.next().onShortcutAdded(list);
        }
        return true;
    }
    
    static boolean convertUriIconToBitmapIcon(final Context context, final ShortcutInfoCompat shortcutInfoCompat) {
        if (shortcutInfoCompat.mIcon == null) {
            return false;
        }
        final int mType = shortcutInfoCompat.mIcon.mType;
        if (mType != 6 && mType != 4) {
            return true;
        }
        final InputStream uriInputStream = shortcutInfoCompat.mIcon.getUriInputStream(context);
        if (uriInputStream == null) {
            return false;
        }
        final Bitmap decodeStream = BitmapFactory.decodeStream(uriInputStream);
        if (decodeStream == null) {
            return false;
        }
        IconCompat mIcon;
        if (mType == 6) {
            mIcon = IconCompat.createWithAdaptiveBitmap(decodeStream);
        }
        else {
            mIcon = IconCompat.createWithBitmap(decodeStream);
        }
        shortcutInfoCompat.mIcon = mIcon;
        return true;
    }
    
    static void convertUriIconsToBitmapIcons(final Context context, final List<ShortcutInfoCompat> c) {
        for (final ShortcutInfoCompat shortcutInfoCompat : new ArrayList(c)) {
            if (!convertUriIconToBitmapIcon(context, shortcutInfoCompat)) {
                c.remove(shortcutInfoCompat);
            }
        }
    }
    
    public static Intent createShortcutResultIntent(final Context context, final ShortcutInfoCompat shortcutInfoCompat) {
        Intent shortcutResultIntent;
        if (Build$VERSION.SDK_INT >= 26) {
            shortcutResultIntent = ((ShortcutManager)context.getSystemService((Class)ShortcutManager.class)).createShortcutResultIntent(shortcutInfoCompat.toShortcutInfo());
        }
        else {
            shortcutResultIntent = null;
        }
        Intent intent = shortcutResultIntent;
        if (shortcutResultIntent == null) {
            intent = new Intent();
        }
        return shortcutInfoCompat.addToIntent(intent);
    }
    
    public static void disableShortcuts(final Context context, final List<String> list, final CharSequence charSequence) {
        if (Build$VERSION.SDK_INT >= 25) {
            ((ShortcutManager)context.getSystemService((Class)ShortcutManager.class)).disableShortcuts((List)list, charSequence);
        }
        getShortcutInfoSaverInstance(context).removeShortcuts(list);
        final Iterator<ShortcutInfoChangeListener> iterator = getShortcutInfoListeners(context).iterator();
        while (iterator.hasNext()) {
            iterator.next().onShortcutRemoved(list);
        }
    }
    
    public static void enableShortcuts(final Context context, final List<ShortcutInfoCompat> list) {
        final List<ShortcutInfoCompat> removeShortcutsExcludedFromSurface = removeShortcutsExcludedFromSurface(list, 1);
        if (Build$VERSION.SDK_INT >= 25) {
            final ArrayList list2 = new ArrayList(list.size());
            final Iterator<ShortcutInfoCompat> iterator = removeShortcutsExcludedFromSurface.iterator();
            while (iterator.hasNext()) {
                list2.add(iterator.next().mId);
            }
            ((ShortcutManager)context.getSystemService((Class)ShortcutManager.class)).enableShortcuts((List)list2);
        }
        getShortcutInfoSaverInstance(context).addShortcuts(removeShortcutsExcludedFromSurface);
        final Iterator<ShortcutInfoChangeListener> iterator2 = getShortcutInfoListeners(context).iterator();
        while (iterator2.hasNext()) {
            iterator2.next().onShortcutAdded(list);
        }
    }
    
    public static List<ShortcutInfoCompat> getDynamicShortcuts(final Context context) {
        if (Build$VERSION.SDK_INT >= 25) {
            final List dynamicShortcuts = ((ShortcutManager)context.getSystemService((Class)ShortcutManager.class)).getDynamicShortcuts();
            final ArrayList list = new ArrayList(dynamicShortcuts.size());
            final Iterator iterator = dynamicShortcuts.iterator();
            while (iterator.hasNext()) {
                list.add((Object)new ShortcutInfoCompat.Builder(context, (ShortcutInfo)iterator.next()).build());
            }
            return (List<ShortcutInfoCompat>)list;
        }
        try {
            return getShortcutInfoSaverInstance(context).getShortcuts();
        }
        catch (final Exception ex) {
            return new ArrayList<ShortcutInfoCompat>();
        }
    }
    
    private static int getIconDimensionInternal(final Context context, final boolean b) {
        final ActivityManager activityManager = (ActivityManager)context.getSystemService("activity");
        int b2;
        if (Build$VERSION.SDK_INT < 19 || activityManager == null || activityManager.isLowRamDevice()) {
            b2 = 48;
        }
        else {
            b2 = 96;
        }
        final int max = Math.max(1, b2);
        final DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        float n;
        if (b) {
            n = displayMetrics.xdpi;
        }
        else {
            n = displayMetrics.ydpi;
        }
        return (int)(max * (n / 160.0f));
    }
    
    public static int getIconMaxHeight(final Context context) {
        Preconditions.checkNotNull(context);
        if (Build$VERSION.SDK_INT >= 25) {
            return ((ShortcutManager)context.getSystemService((Class)ShortcutManager.class)).getIconMaxHeight();
        }
        return getIconDimensionInternal(context, false);
    }
    
    public static int getIconMaxWidth(final Context context) {
        Preconditions.checkNotNull(context);
        if (Build$VERSION.SDK_INT >= 25) {
            return ((ShortcutManager)context.getSystemService((Class)ShortcutManager.class)).getIconMaxWidth();
        }
        return getIconDimensionInternal(context, true);
    }
    
    public static int getMaxShortcutCountPerActivity(final Context context) {
        Preconditions.checkNotNull(context);
        if (Build$VERSION.SDK_INT >= 25) {
            return ((ShortcutManager)context.getSystemService((Class)ShortcutManager.class)).getMaxShortcutCountPerActivity();
        }
        return 5;
    }
    
    static List<ShortcutInfoChangeListener> getShortcutInfoChangeListeners() {
        return ShortcutManagerCompat.sShortcutInfoChangeListeners;
    }
    
    private static String getShortcutInfoCompatWithLowestRank(final List<ShortcutInfoCompat> list) {
        final Iterator<ShortcutInfoCompat> iterator = list.iterator();
        int rank = -1;
        String id = null;
        while (iterator.hasNext()) {
            final ShortcutInfoCompat shortcutInfoCompat = iterator.next();
            if (shortcutInfoCompat.getRank() > rank) {
                id = shortcutInfoCompat.getId();
                rank = shortcutInfoCompat.getRank();
            }
        }
        return id;
    }
    
    private static List<ShortcutInfoChangeListener> getShortcutInfoListeners(final Context context) {
        if (ShortcutManagerCompat.sShortcutInfoChangeListeners == null) {
            final ArrayList sShortcutInfoChangeListeners = new ArrayList();
            if (Build$VERSION.SDK_INT >= 21) {
                final PackageManager packageManager = context.getPackageManager();
                final Intent intent = new Intent("androidx.core.content.pm.SHORTCUT_LISTENER");
                intent.setPackage(context.getPackageName());
                final Iterator iterator = packageManager.queryIntentActivities(intent, 128).iterator();
                while (iterator.hasNext()) {
                    final ActivityInfo activityInfo = ((ResolveInfo)iterator.next()).activityInfo;
                    if (activityInfo == null) {
                        continue;
                    }
                    final Bundle metaData = activityInfo.metaData;
                    if (metaData == null) {
                        continue;
                    }
                    final String string = metaData.getString("androidx.core.content.pm.shortcut_listener_impl");
                    if (string == null) {
                        continue;
                    }
                    try {
                        sShortcutInfoChangeListeners.add(Class.forName(string, false, ShortcutManagerCompat.class.getClassLoader()).getMethod("getInstance", Context.class).invoke(null, context));
                    }
                    catch (final Exception ex) {}
                }
            }
            if (ShortcutManagerCompat.sShortcutInfoChangeListeners == null) {
                ShortcutManagerCompat.sShortcutInfoChangeListeners = sShortcutInfoChangeListeners;
            }
        }
        return ShortcutManagerCompat.sShortcutInfoChangeListeners;
    }
    
    private static ShortcutInfoCompatSaver<?> getShortcutInfoSaverInstance(final Context context) {
        if (ShortcutManagerCompat.sShortcutInfoCompatSaver == null) {
            if (Build$VERSION.SDK_INT >= 23) {
                try {
                    ShortcutManagerCompat.sShortcutInfoCompatSaver = (ShortcutInfoCompatSaver)Class.forName("androidx.sharetarget.ShortcutInfoCompatSaverImpl", false, ShortcutManagerCompat.class.getClassLoader()).getMethod("getInstance", Context.class).invoke(null, context);
                }
                catch (final Exception ex) {}
            }
            if (ShortcutManagerCompat.sShortcutInfoCompatSaver == null) {
                ShortcutManagerCompat.sShortcutInfoCompatSaver = new ShortcutInfoCompatSaver.NoopImpl();
            }
        }
        return ShortcutManagerCompat.sShortcutInfoCompatSaver;
    }
    
    public static List<ShortcutInfoCompat> getShortcuts(final Context context, final int n) {
        if (Build$VERSION.SDK_INT >= 30) {
            return ShortcutInfoCompat.fromShortcuts(context, ((ShortcutManager)context.getSystemService((Class)ShortcutManager.class)).getShortcuts(n));
        }
        if (Build$VERSION.SDK_INT >= 25) {
            final ShortcutManager shortcutManager = (ShortcutManager)context.getSystemService((Class)ShortcutManager.class);
            final ArrayList list = new ArrayList();
            if ((n & 0x1) != 0x0) {
                list.addAll(shortcutManager.getManifestShortcuts());
            }
            if ((n & 0x2) != 0x0) {
                list.addAll(shortcutManager.getDynamicShortcuts());
            }
            if ((n & 0x4) != 0x0) {
                list.addAll(shortcutManager.getPinnedShortcuts());
            }
            return ShortcutInfoCompat.fromShortcuts(context, list);
        }
        Label_0125: {
            if ((n & 0x2) == 0x0) {
                break Label_0125;
            }
            try {
                return getShortcutInfoSaverInstance(context).getShortcuts();
                return Collections.emptyList();
            }
            catch (final Exception ex) {
                return Collections.emptyList();
            }
        }
    }
    
    public static boolean isRateLimitingActive(final Context context) {
        Preconditions.checkNotNull(context);
        if (Build$VERSION.SDK_INT >= 25) {
            return ((ShortcutManager)context.getSystemService((Class)ShortcutManager.class)).isRateLimitingActive();
        }
        return getShortcuts(context, 3).size() == getMaxShortcutCountPerActivity(context);
    }
    
    public static boolean isRequestPinShortcutSupported(final Context context) {
        if (Build$VERSION.SDK_INT >= 26) {
            return ((ShortcutManager)context.getSystemService((Class)ShortcutManager.class)).isRequestPinShortcutSupported();
        }
        if (ContextCompat.checkSelfPermission(context, "com.android.launcher.permission.INSTALL_SHORTCUT") != 0) {
            return false;
        }
        final Iterator iterator = context.getPackageManager().queryBroadcastReceivers(new Intent("com.android.launcher.action.INSTALL_SHORTCUT"), 0).iterator();
        while (iterator.hasNext()) {
            final String permission = ((ResolveInfo)iterator.next()).activityInfo.permission;
            if (TextUtils.isEmpty((CharSequence)permission) || "com.android.launcher.permission.INSTALL_SHORTCUT".equals(permission)) {
                return true;
            }
        }
        return false;
    }
    
    public static boolean pushDynamicShortcut(final Context context, final ShortcutInfoCompat shortcutInfoCompat) {
        Preconditions.checkNotNull(context);
        Preconditions.checkNotNull(shortcutInfoCompat);
        if (Build$VERSION.SDK_INT <= 32 && shortcutInfoCompat.isExcludedFromSurfaces(1)) {
            final Iterator<ShortcutInfoChangeListener> iterator = getShortcutInfoListeners(context).iterator();
            while (iterator.hasNext()) {
                iterator.next().onShortcutAdded(Collections.singletonList(shortcutInfoCompat));
            }
            return true;
        }
        final int maxShortcutCountPerActivity = getMaxShortcutCountPerActivity(context);
        if (maxShortcutCountPerActivity == 0) {
            return false;
        }
        if (Build$VERSION.SDK_INT <= 29) {
            convertUriIconToBitmapIcon(context, shortcutInfoCompat);
        }
        if (Build$VERSION.SDK_INT >= 30) {
            ((ShortcutManager)context.getSystemService((Class)ShortcutManager.class)).pushDynamicShortcut(shortcutInfoCompat.toShortcutInfo());
        }
        else if (Build$VERSION.SDK_INT >= 25) {
            final ShortcutManager shortcutManager = (ShortcutManager)context.getSystemService((Class)ShortcutManager.class);
            if (shortcutManager.isRateLimitingActive()) {
                return false;
            }
            final List dynamicShortcuts = shortcutManager.getDynamicShortcuts();
            if (dynamicShortcuts.size() >= maxShortcutCountPerActivity) {
                shortcutManager.removeDynamicShortcuts((List)Arrays.asList(Api25Impl.getShortcutInfoWithLowestRank(dynamicShortcuts)));
            }
            shortcutManager.addDynamicShortcuts((List)Arrays.asList(shortcutInfoCompat.toShortcutInfo()));
        }
        final ShortcutInfoCompatSaver<?> shortcutInfoSaverInstance = getShortcutInfoSaverInstance(context);
        try {
            final List shortcuts = shortcutInfoSaverInstance.getShortcuts();
            if (shortcuts.size() >= maxShortcutCountPerActivity) {
                shortcutInfoSaverInstance.removeShortcuts(Arrays.asList(getShortcutInfoCompatWithLowestRank(shortcuts)));
            }
            shortcutInfoSaverInstance.addShortcuts(Arrays.asList(shortcutInfoCompat));
            return true;
        }
        catch (final Exception ex) {
            return false;
        }
        finally {
            final Iterator<ShortcutInfoChangeListener> iterator2 = getShortcutInfoListeners(context).iterator();
            while (iterator2.hasNext()) {
                iterator2.next().onShortcutAdded(Collections.singletonList(shortcutInfoCompat));
            }
            reportShortcutUsed(context, shortcutInfoCompat.getId());
        }
    }
    
    public static void removeAllDynamicShortcuts(final Context context) {
        if (Build$VERSION.SDK_INT >= 25) {
            ((ShortcutManager)context.getSystemService((Class)ShortcutManager.class)).removeAllDynamicShortcuts();
        }
        getShortcutInfoSaverInstance(context).removeAllShortcuts();
        final Iterator<ShortcutInfoChangeListener> iterator = getShortcutInfoListeners(context).iterator();
        while (iterator.hasNext()) {
            iterator.next().onAllShortcutsRemoved();
        }
    }
    
    public static void removeDynamicShortcuts(final Context context, final List<String> list) {
        if (Build$VERSION.SDK_INT >= 25) {
            ((ShortcutManager)context.getSystemService((Class)ShortcutManager.class)).removeDynamicShortcuts((List)list);
        }
        getShortcutInfoSaverInstance(context).removeShortcuts(list);
        final Iterator<ShortcutInfoChangeListener> iterator = getShortcutInfoListeners(context).iterator();
        while (iterator.hasNext()) {
            iterator.next().onShortcutRemoved(list);
        }
    }
    
    public static void removeLongLivedShortcuts(final Context context, final List<String> list) {
        if (Build$VERSION.SDK_INT < 30) {
            removeDynamicShortcuts(context, list);
            return;
        }
        ((ShortcutManager)context.getSystemService((Class)ShortcutManager.class)).removeLongLivedShortcuts((List)list);
        getShortcutInfoSaverInstance(context).removeShortcuts(list);
        final Iterator<ShortcutInfoChangeListener> iterator = getShortcutInfoListeners(context).iterator();
        while (iterator.hasNext()) {
            iterator.next().onShortcutRemoved(list);
        }
    }
    
    private static List<ShortcutInfoCompat> removeShortcutsExcludedFromSurface(final List<ShortcutInfoCompat> list, final int n) {
        Objects.requireNonNull(list);
        if (Build$VERSION.SDK_INT > 32) {
            return list;
        }
        final ArrayList list2 = new ArrayList((Collection<? extends E>)list);
        for (final ShortcutInfoCompat shortcutInfoCompat : list) {
            if (shortcutInfoCompat.isExcludedFromSurfaces(n)) {
                list2.remove(shortcutInfoCompat);
            }
        }
        return list2;
    }
    
    public static void reportShortcutUsed(final Context context, final String o) {
        Preconditions.checkNotNull(context);
        Preconditions.checkNotNull(o);
        if (Build$VERSION.SDK_INT >= 25) {
            ((ShortcutManager)context.getSystemService((Class)ShortcutManager.class)).reportShortcutUsed(o);
        }
        final Iterator<ShortcutInfoChangeListener> iterator = getShortcutInfoListeners(context).iterator();
        while (iterator.hasNext()) {
            iterator.next().onShortcutUsageReported(Collections.singletonList(o));
        }
    }
    
    public static boolean requestPinShortcut(final Context context, final ShortcutInfoCompat shortcutInfoCompat, final IntentSender intentSender) {
        if (Build$VERSION.SDK_INT <= 32 && shortcutInfoCompat.isExcludedFromSurfaces(1)) {
            return false;
        }
        if (Build$VERSION.SDK_INT >= 26) {
            return ((ShortcutManager)context.getSystemService((Class)ShortcutManager.class)).requestPinShortcut(shortcutInfoCompat.toShortcutInfo(), intentSender);
        }
        if (!isRequestPinShortcutSupported(context)) {
            return false;
        }
        final Intent addToIntent = shortcutInfoCompat.addToIntent(new Intent("com.android.launcher.action.INSTALL_SHORTCUT"));
        if (intentSender == null) {
            context.sendBroadcast(addToIntent);
            return true;
        }
        context.sendOrderedBroadcast(addToIntent, (String)null, (BroadcastReceiver)new BroadcastReceiver(intentSender) {
            final IntentSender val$callback;
            
            public void onReceive(final Context context, final Intent intent) {
                try {
                    this.val$callback.sendIntent(context, 0, (Intent)null, (IntentSender$OnFinished)null, (Handler)null);
                }
                catch (final IntentSender$SendIntentException ex) {}
            }
        }, (Handler)null, -1, (String)null, (Bundle)null);
        return true;
    }
    
    public static boolean setDynamicShortcuts(final Context context, final List<ShortcutInfoCompat> list) {
        Preconditions.checkNotNull(context);
        Preconditions.checkNotNull(list);
        final List<ShortcutInfoCompat> removeShortcutsExcludedFromSurface = removeShortcutsExcludedFromSurface(list, 1);
        if (Build$VERSION.SDK_INT >= 25) {
            final ArrayList dynamicShortcuts = new ArrayList(removeShortcutsExcludedFromSurface.size());
            final Iterator iterator = removeShortcutsExcludedFromSurface.iterator();
            while (iterator.hasNext()) {
                dynamicShortcuts.add((Object)((ShortcutInfoCompat)iterator.next()).toShortcutInfo());
            }
            if (!((ShortcutManager)context.getSystemService((Class)ShortcutManager.class)).setDynamicShortcuts((List)dynamicShortcuts)) {
                return false;
            }
        }
        getShortcutInfoSaverInstance(context).removeAllShortcuts();
        getShortcutInfoSaverInstance(context).addShortcuts(removeShortcutsExcludedFromSurface);
        for (final ShortcutInfoChangeListener shortcutInfoChangeListener : getShortcutInfoListeners(context)) {
            shortcutInfoChangeListener.onAllShortcutsRemoved();
            shortcutInfoChangeListener.onShortcutAdded(list);
        }
        return true;
    }
    
    static void setShortcutInfoChangeListeners(final List<ShortcutInfoChangeListener> sShortcutInfoChangeListeners) {
        ShortcutManagerCompat.sShortcutInfoChangeListeners = sShortcutInfoChangeListeners;
    }
    
    static void setShortcutInfoCompatSaver(final ShortcutInfoCompatSaver<Void> sShortcutInfoCompatSaver) {
        ShortcutManagerCompat.sShortcutInfoCompatSaver = sShortcutInfoCompatSaver;
    }
    
    public static boolean updateShortcuts(final Context context, final List<ShortcutInfoCompat> list) {
        final List<ShortcutInfoCompat> removeShortcutsExcludedFromSurface = removeShortcutsExcludedFromSurface(list, 1);
        if (Build$VERSION.SDK_INT <= 29) {
            convertUriIconsToBitmapIcons(context, removeShortcutsExcludedFromSurface);
        }
        if (Build$VERSION.SDK_INT >= 25) {
            final ArrayList list2 = new ArrayList();
            final Iterator<ShortcutInfoCompat> iterator = removeShortcutsExcludedFromSurface.iterator();
            while (iterator.hasNext()) {
                list2.add(iterator.next().toShortcutInfo());
            }
            if (!((ShortcutManager)context.getSystemService((Class)ShortcutManager.class)).updateShortcuts((List)list2)) {
                return false;
            }
        }
        getShortcutInfoSaverInstance(context).addShortcuts(removeShortcutsExcludedFromSurface);
        final Iterator<ShortcutInfoChangeListener> iterator2 = getShortcutInfoListeners(context).iterator();
        while (iterator2.hasNext()) {
            iterator2.next().onShortcutUpdated(list);
        }
        return true;
    }
    
    private static class Api25Impl
    {
        static String getShortcutInfoWithLowestRank(final List<ShortcutInfo> list) {
            final Iterator<ShortcutInfo> iterator = list.iterator();
            int rank = -1;
            String id = null;
            while (iterator.hasNext()) {
                final ShortcutInfo shortcutInfo = iterator.next();
                if (shortcutInfo.getRank() > rank) {
                    id = shortcutInfo.getId();
                    rank = shortcutInfo.getRank();
                }
            }
            return id;
        }
    }
    
    @Retention(RetentionPolicy.SOURCE)
    public @interface ShortcutMatchFlags {
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.content.pm;

import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;
import android.os.Build$VERSION;
import android.content.pm.PermissionInfo;

public final class PermissionInfoCompat
{
    private PermissionInfoCompat() {
    }
    
    public static int getProtection(final PermissionInfo permissionInfo) {
        if (Build$VERSION.SDK_INT >= 28) {
            return Api28Impl.getProtection(permissionInfo);
        }
        return permissionInfo.protectionLevel & 0xF;
    }
    
    public static int getProtectionFlags(final PermissionInfo permissionInfo) {
        if (Build$VERSION.SDK_INT >= 28) {
            return Api28Impl.getProtectionFlags(permissionInfo);
        }
        return permissionInfo.protectionLevel & 0xFFFFFFF0;
    }
    
    static class Api28Impl
    {
        private Api28Impl() {
        }
        
        static int getProtection(final PermissionInfo permissionInfo) {
            return permissionInfo.getProtection();
        }
        
        static int getProtectionFlags(final PermissionInfo permissionInfo) {
            return permissionInfo.getProtectionFlags();
        }
    }
    
    @Retention(RetentionPolicy.SOURCE)
    public @interface Protection {
    }
    
    @Retention(RetentionPolicy.SOURCE)
    public @interface ProtectionFlags {
    }
}

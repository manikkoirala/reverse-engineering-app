// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.content.pm;

import java.util.List;

public abstract class ShortcutInfoChangeListener
{
    public void onAllShortcutsRemoved() {
    }
    
    public void onShortcutAdded(final List<ShortcutInfoCompat> list) {
    }
    
    public void onShortcutRemoved(final List<String> list) {
    }
    
    public void onShortcutUpdated(final List<ShortcutInfoCompat> list) {
    }
    
    public void onShortcutUsageReported(final List<String> list) {
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.content.pm;

import android.os.Bundle;
import java.util.Iterator;
import android.util.Log;
import android.content.pm.ResolveInfo;
import android.content.Intent;
import java.util.HashSet;
import java.util.Set;
import org.xmlpull.v1.XmlPullParserException;
import java.io.IOException;
import android.content.res.XmlResourceParser;
import android.content.pm.ActivityInfo;
import java.util.Collection;
import java.util.List;
import android.content.Context;
import org.xmlpull.v1.XmlPullParser;
import java.util.ArrayList;

public class ShortcutXmlParser
{
    private static final String ATTR_SHORTCUT_ID = "shortcutId";
    private static final Object GET_INSTANCE_LOCK;
    private static final String META_DATA_APP_SHORTCUTS = "android.app.shortcuts";
    private static final String TAG = "ShortcutXmlParser";
    private static final String TAG_SHORTCUT = "shortcut";
    private static volatile ArrayList<String> sShortcutIds;
    
    static {
        GET_INSTANCE_LOCK = new Object();
    }
    
    private ShortcutXmlParser() {
    }
    
    private static String getAttributeValue(final XmlPullParser xmlPullParser, final String s) {
        String s2;
        if ((s2 = xmlPullParser.getAttributeValue("http://schemas.android.com/apk/res/android", s)) == null) {
            s2 = xmlPullParser.getAttributeValue((String)null, s);
        }
        return s2;
    }
    
    public static List<String> getShortcutIds(final Context context) {
        if (ShortcutXmlParser.sShortcutIds == null) {
            synchronized (ShortcutXmlParser.GET_INSTANCE_LOCK) {
                if (ShortcutXmlParser.sShortcutIds == null) {
                    (ShortcutXmlParser.sShortcutIds = new ArrayList<String>()).addAll(parseShortcutIds(context));
                }
            }
        }
        return ShortcutXmlParser.sShortcutIds;
    }
    
    private static XmlResourceParser getXmlResourceParser(final Context context, final ActivityInfo activityInfo) {
        final XmlResourceParser loadXmlMetaData = activityInfo.loadXmlMetaData(context.getPackageManager(), "android.app.shortcuts");
        if (loadXmlMetaData != null) {
            return loadXmlMetaData;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Failed to open android.app.shortcuts meta-data resource of ");
        sb.append(activityInfo.name);
        throw new IllegalArgumentException(sb.toString());
    }
    
    public static List<String> parseShortcutIds(final XmlPullParser xmlPullParser) throws IOException, XmlPullParserException {
        final ArrayList list = new ArrayList(1);
        while (true) {
            final int next = xmlPullParser.next();
            if (next == 1 || (next == 3 && xmlPullParser.getDepth() <= 0)) {
                break;
            }
            final int depth = xmlPullParser.getDepth();
            final String name = xmlPullParser.getName();
            if (next != 2 || depth != 2 || !"shortcut".equals(name)) {
                continue;
            }
            final String attributeValue = getAttributeValue(xmlPullParser, "shortcutId");
            if (attributeValue == null) {
                continue;
            }
            list.add(attributeValue);
        }
        return list;
    }
    
    private static Set<String> parseShortcutIds(final Context context) {
        final HashSet set = new HashSet();
        final Intent intent = new Intent("android.intent.action.MAIN");
        intent.addCategory("android.intent.category.LAUNCHER");
        intent.setPackage(context.getPackageName());
        final List queryIntentActivities = context.getPackageManager().queryIntentActivities(intent, 128);
        if (queryIntentActivities != null) {
            if (queryIntentActivities.size() != 0) {
                try {
                    final Iterator iterator = queryIntentActivities.iterator();
                    while (iterator.hasNext()) {
                        final ActivityInfo activityInfo = iterator.next().activityInfo;
                        final Bundle metaData = activityInfo.metaData;
                        if (metaData != null && metaData.containsKey("android.app.shortcuts")) {
                            final XmlResourceParser xmlResourceParser = getXmlResourceParser(context, activityInfo);
                            try {
                                set.addAll(parseShortcutIds((XmlPullParser)xmlResourceParser));
                                if (xmlResourceParser != null) {
                                    xmlResourceParser.close();
                                    continue;
                                }
                                continue;
                            }
                            finally {
                                if (xmlResourceParser != null) {
                                    try {
                                        xmlResourceParser.close();
                                    }
                                    finally {
                                        ((Throwable)context).addSuppressed((Throwable)iterator);
                                    }
                                }
                            }
                        }
                    }
                }
                catch (final Exception ex) {
                    Log.e("ShortcutXmlParser", "Failed to parse the Xml resource: ", (Throwable)ex);
                }
            }
        }
        return set;
    }
}

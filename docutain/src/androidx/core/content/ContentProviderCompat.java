// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.content;

import android.content.Context;
import android.content.ContentProvider;

public final class ContentProviderCompat
{
    private ContentProviderCompat() {
    }
    
    public static Context requireContext(final ContentProvider contentProvider) {
        final Context context = contentProvider.getContext();
        if (context != null) {
            return context;
        }
        throw new IllegalStateException("Cannot find context from the provider.");
    }
}

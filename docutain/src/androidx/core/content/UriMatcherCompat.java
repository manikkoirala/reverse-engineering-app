// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.content;

import android.net.Uri;
import androidx.core.util.Predicate;
import android.content.UriMatcher;

public class UriMatcherCompat
{
    private UriMatcherCompat() {
    }
    
    public static Predicate<Uri> asPredicate(final UriMatcher uriMatcher) {
        return new UriMatcherCompat$$ExternalSyntheticLambda0(uriMatcher);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.content;

import java.util.ArrayList;
import android.os.Parcelable;
import androidx.core.util.Preconditions;
import android.net.Uri;
import android.os.Build$VERSION;
import android.content.Intent;
import android.content.Context;

public final class IntentCompat
{
    public static final String ACTION_CREATE_REMINDER = "android.intent.action.CREATE_REMINDER";
    public static final String CATEGORY_LEANBACK_LAUNCHER = "android.intent.category.LEANBACK_LAUNCHER";
    public static final String EXTRA_HTML_TEXT = "android.intent.extra.HTML_TEXT";
    public static final String EXTRA_START_PLAYBACK = "android.intent.extra.START_PLAYBACK";
    public static final String EXTRA_TIME = "android.intent.extra.TIME";
    
    private IntentCompat() {
    }
    
    public static Intent createManageUnusedAppRestrictionsIntent(final Context context, final String s) {
        if (!PackageManagerCompat.areUnusedAppRestrictionsAvailable(context.getPackageManager())) {
            throw new UnsupportedOperationException("Unused App Restriction features are not available on this device");
        }
        if (Build$VERSION.SDK_INT >= 31) {
            return new Intent("android.settings.APPLICATION_DETAILS_SETTINGS").setData(Uri.fromParts("package", s, (String)null));
        }
        final Intent setData = new Intent("android.intent.action.AUTO_REVOKE_PERMISSIONS").setData(Uri.fromParts("package", s, (String)null));
        if (Build$VERSION.SDK_INT >= 30) {
            return setData;
        }
        return setData.setPackage((String)Preconditions.checkNotNull(PackageManagerCompat.getPermissionRevocationVerifierApp(context.getPackageManager())));
    }
    
    public static Parcelable[] getParcelableArrayExtra(final Intent intent, final String s, final Class<? extends Parcelable> clazz) {
        if (Build$VERSION.SDK_INT >= 34) {
            return Api33Impl.getParcelableArrayExtra(intent, s, clazz);
        }
        return intent.getParcelableArrayExtra(s);
    }
    
    public static <T> ArrayList<T> getParcelableArrayListExtra(final Intent intent, final String s, final Class<? extends T> clazz) {
        if (Build$VERSION.SDK_INT >= 34) {
            return Api33Impl.getParcelableArrayListExtra(intent, s, clazz);
        }
        return intent.getParcelableArrayListExtra(s);
    }
    
    public static <T> T getParcelableExtra(final Intent intent, final String s, final Class<T> clazz) {
        if (Build$VERSION.SDK_INT >= 34) {
            return Api33Impl.getParcelableExtra(intent, s, clazz);
        }
        Object parcelableExtra = intent.getParcelableExtra(s);
        if (!clazz.isInstance(parcelableExtra)) {
            parcelableExtra = null;
        }
        return (T)parcelableExtra;
    }
    
    public static Intent makeMainSelectorActivity(final String s, final String s2) {
        if (Build$VERSION.SDK_INT >= 15) {
            return Api15Impl.makeMainSelectorActivity(s, s2);
        }
        final Intent intent = new Intent(s);
        intent.addCategory(s2);
        return intent;
    }
    
    static class Api15Impl
    {
        private Api15Impl() {
        }
        
        static Intent makeMainSelectorActivity(final String s, final String s2) {
            return Intent.makeMainSelectorActivity(s, s2);
        }
    }
    
    static class Api33Impl
    {
        private Api33Impl() {
        }
        
        static <T> T[] getParcelableArrayExtra(final Intent intent, final String s, final Class<T> clazz) {
            return (T[])intent.getParcelableArrayExtra(s, (Class)clazz);
        }
        
        static <T> ArrayList<T> getParcelableArrayListExtra(final Intent intent, final String s, final Class<? extends T> clazz) {
            return intent.getParcelableArrayListExtra(s, (Class)clazz);
        }
        
        static <T> T getParcelableExtra(final Intent intent, final String s, final Class<T> clazz) {
            return (T)intent.getParcelableExtra(s, (Class)clazz);
        }
    }
}

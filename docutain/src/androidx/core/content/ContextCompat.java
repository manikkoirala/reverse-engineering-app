// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.content;

import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;
import android.net.wifi.WifiManager;
import android.net.wifi.p2p.WifiP2pManager;
import android.app.WallpaperManager;
import android.os.Vibrator;
import android.hardware.usb.UsbManager;
import android.app.UiModeManager;
import android.view.textservice.TextServicesManager;
import android.telephony.TelephonyManager;
import android.os.storage.StorageManager;
import android.hardware.SensorManager;
import android.app.SearchManager;
import android.os.PowerManager;
import android.app.NotificationManager;
import android.nfc.NfcManager;
import android.location.LocationManager;
import android.view.LayoutInflater;
import android.app.KeyguardManager;
import android.view.inputmethod.InputMethodManager;
import android.os.DropBoxManager;
import android.app.DownloadManager;
import android.app.admin.DevicePolicyManager;
import android.net.ConnectivityManager;
import android.content.ClipboardManager;
import android.media.AudioManager;
import android.app.AlarmManager;
import android.app.ActivityManager;
import android.accounts.AccountManager;
import android.view.accessibility.AccessibilityManager;
import android.net.nsd.NsdManager;
import android.media.MediaRouter;
import android.hardware.input.InputManager;
import android.os.UserManager;
import android.bluetooth.BluetoothManager;
import android.print.PrintManager;
import android.hardware.ConsumerIrManager;
import android.view.accessibility.CaptioningManager;
import android.app.AppOpsManager;
import android.media.tv.TvInputManager;
import android.telecom.TelecomManager;
import android.content.RestrictionsManager;
import android.media.session.MediaSessionManager;
import android.media.projection.MediaProjectionManager;
import android.content.pm.LauncherApps;
import android.app.job.JobScheduler;
import android.hardware.camera2.CameraManager;
import android.os.BatteryManager;
import android.appwidget.AppWidgetManager;
import android.app.usage.UsageStatsManager;
import android.telephony.SubscriptionManager;
import java.util.HashMap;
import android.hardware.display.DisplayManager;
import android.content.ComponentName;
import android.os.Bundle;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.BroadcastReceiver;
import androidx.core.os.ExecutorCompat;
import android.os.Handler;
import java.util.concurrent.Executor;
import android.graphics.drawable.Drawable;
import android.view.WindowManager;
import android.view.Display;
import androidx.core.os.LocaleListCompat;
import androidx.core.os.ConfigurationCompat;
import android.content.res.Configuration;
import androidx.core.app.LocaleManagerCompat;
import androidx.core.content.res.ResourcesCompat;
import android.content.res.ColorStateList;
import android.util.Log;
import java.io.File;
import android.os.Process;
import androidx.core.app.NotificationManagerCompat;
import android.text.TextUtils;
import android.os.Build$VERSION;
import androidx.core.util.ObjectsCompat;
import android.content.Context;
import android.util.TypedValue;

public class ContextCompat
{
    private static final String DYNAMIC_RECEIVER_NOT_EXPORTED_PERMISSION_SUFFIX = ".DYNAMIC_RECEIVER_NOT_EXPORTED_PERMISSION";
    public static final int RECEIVER_EXPORTED = 2;
    public static final int RECEIVER_NOT_EXPORTED = 4;
    public static final int RECEIVER_VISIBLE_TO_INSTANT_APPS = 1;
    private static final String TAG = "ContextCompat";
    private static final Object sLock;
    private static final Object sSync;
    private static TypedValue sTempValue;
    
    static {
        sLock = new Object();
        sSync = new Object();
    }
    
    protected ContextCompat() {
    }
    
    public static int checkSelfPermission(final Context context, final String s) {
        ObjectsCompat.requireNonNull(s, "permission must be non-null");
        if (Build$VERSION.SDK_INT < 33 && TextUtils.equals((CharSequence)"android.permission.POST_NOTIFICATIONS", (CharSequence)s)) {
            int n;
            if (NotificationManagerCompat.from(context).areNotificationsEnabled()) {
                n = 0;
            }
            else {
                n = -1;
            }
            return n;
        }
        return context.checkPermission(s, Process.myPid(), Process.myUid());
    }
    
    public static Context createDeviceProtectedStorageContext(final Context context) {
        if (Build$VERSION.SDK_INT >= 24) {
            return Api24Impl.createDeviceProtectedStorageContext(context);
        }
        return null;
    }
    
    private static File createFilesDir(final File file) {
        synchronized (ContextCompat.sSync) {
            if (!file.exists()) {
                if (file.mkdirs()) {
                    return file;
                }
                final StringBuilder sb = new StringBuilder();
                sb.append("Unable to create files subdir ");
                sb.append(file.getPath());
                Log.w("ContextCompat", sb.toString());
            }
            return file;
        }
    }
    
    public static String getAttributionTag(final Context context) {
        if (Build$VERSION.SDK_INT >= 30) {
            return Api30Impl.getAttributionTag(context);
        }
        return null;
    }
    
    public static File getCodeCacheDir(final Context context) {
        if (Build$VERSION.SDK_INT >= 21) {
            return Api21Impl.getCodeCacheDir(context);
        }
        return createFilesDir(new File(context.getApplicationInfo().dataDir, "code_cache"));
    }
    
    public static int getColor(final Context context, final int n) {
        if (Build$VERSION.SDK_INT >= 23) {
            return Api23Impl.getColor(context, n);
        }
        return context.getResources().getColor(n);
    }
    
    public static ColorStateList getColorStateList(final Context context, final int n) {
        return ResourcesCompat.getColorStateList(context.getResources(), n, context.getTheme());
    }
    
    public static Context getContextForLanguage(final Context context) {
        final LocaleListCompat applicationLocales = LocaleManagerCompat.getApplicationLocales(context);
        Context configurationContext = context;
        if (Build$VERSION.SDK_INT <= 32) {
            configurationContext = context;
            if (Build$VERSION.SDK_INT >= 17) {
                configurationContext = context;
                if (!applicationLocales.isEmpty()) {
                    final Configuration configuration = new Configuration(context.getResources().getConfiguration());
                    ConfigurationCompat.setLocales(configuration, applicationLocales);
                    configurationContext = Api17Impl.createConfigurationContext(context, configuration);
                }
            }
        }
        return configurationContext;
    }
    
    public static File getDataDir(final Context context) {
        if (Build$VERSION.SDK_INT >= 24) {
            return Api24Impl.getDataDir(context);
        }
        final String dataDir = context.getApplicationInfo().dataDir;
        File file;
        if (dataDir != null) {
            file = new File(dataDir);
        }
        else {
            file = null;
        }
        return file;
    }
    
    public static Display getDisplayOrDefault(final Context context) {
        if (Build$VERSION.SDK_INT >= 30) {
            return Api30Impl.getDisplayOrDefault(context);
        }
        return ((WindowManager)context.getSystemService("window")).getDefaultDisplay();
    }
    
    public static Drawable getDrawable(final Context context, int resourceId) {
        if (Build$VERSION.SDK_INT >= 21) {
            return Api21Impl.getDrawable(context, resourceId);
        }
        if (Build$VERSION.SDK_INT >= 16) {
            return context.getResources().getDrawable(resourceId);
        }
        synchronized (ContextCompat.sLock) {
            if (ContextCompat.sTempValue == null) {
                ContextCompat.sTempValue = new TypedValue();
            }
            context.getResources().getValue(resourceId, ContextCompat.sTempValue, true);
            resourceId = ContextCompat.sTempValue.resourceId;
            monitorexit(ContextCompat.sLock);
            return context.getResources().getDrawable(resourceId);
        }
    }
    
    public static File[] getExternalCacheDirs(final Context context) {
        if (Build$VERSION.SDK_INT >= 19) {
            return Api19Impl.getExternalCacheDirs(context);
        }
        return new File[] { context.getExternalCacheDir() };
    }
    
    public static File[] getExternalFilesDirs(final Context context, final String s) {
        if (Build$VERSION.SDK_INT >= 19) {
            return Api19Impl.getExternalFilesDirs(context, s);
        }
        return new File[] { context.getExternalFilesDir(s) };
    }
    
    public static Executor getMainExecutor(final Context context) {
        if (Build$VERSION.SDK_INT >= 28) {
            return Api28Impl.getMainExecutor(context);
        }
        return ExecutorCompat.create(new Handler(context.getMainLooper()));
    }
    
    public static File getNoBackupFilesDir(final Context context) {
        if (Build$VERSION.SDK_INT >= 21) {
            return Api21Impl.getNoBackupFilesDir(context);
        }
        return createFilesDir(new File(context.getApplicationInfo().dataDir, "no_backup"));
    }
    
    public static File[] getObbDirs(final Context context) {
        if (Build$VERSION.SDK_INT >= 19) {
            return Api19Impl.getObbDirs(context);
        }
        return new File[] { context.getObbDir() };
    }
    
    public static String getString(final Context context, final int n) {
        return getContextForLanguage(context).getString(n);
    }
    
    public static <T> T getSystemService(final Context context, final Class<T> clazz) {
        if (Build$VERSION.SDK_INT >= 23) {
            return Api23Impl.getSystemService(context, clazz);
        }
        final String systemServiceName = getSystemServiceName(context, clazz);
        Object systemService;
        if (systemServiceName != null) {
            systemService = context.getSystemService(systemServiceName);
        }
        else {
            systemService = null;
        }
        return (T)systemService;
    }
    
    public static String getSystemServiceName(final Context context, final Class<?> key) {
        if (Build$VERSION.SDK_INT >= 23) {
            return Api23Impl.getSystemServiceName(context, key);
        }
        return LegacyServiceMapHolder.SERVICES.get(key);
    }
    
    public static boolean isDeviceProtectedStorage(final Context context) {
        return Build$VERSION.SDK_INT >= 24 && Api24Impl.isDeviceProtectedStorage(context);
    }
    
    static String obtainAndCheckReceiverPermission(final Context context) {
        final StringBuilder sb = new StringBuilder();
        sb.append(context.getPackageName());
        sb.append(".DYNAMIC_RECEIVER_NOT_EXPORTED_PERMISSION");
        final String string = sb.toString();
        if (PermissionChecker.checkSelfPermission(context, string) == 0) {
            return string;
        }
        final StringBuilder sb2 = new StringBuilder();
        sb2.append("Permission ");
        sb2.append(string);
        sb2.append(" is required by your application to receive broadcasts, please add it to your manifest");
        throw new RuntimeException(sb2.toString());
    }
    
    public static Intent registerReceiver(final Context context, final BroadcastReceiver broadcastReceiver, final IntentFilter intentFilter, final int n) {
        return registerReceiver(context, broadcastReceiver, intentFilter, null, null, n);
    }
    
    public static Intent registerReceiver(final Context context, final BroadcastReceiver broadcastReceiver, final IntentFilter intentFilter, final String s, final Handler handler, int n) {
        final int n2 = n & 0x1;
        if (n2 != 0 && (n & 0x4) != 0x0) {
            throw new IllegalArgumentException("Cannot specify both RECEIVER_VISIBLE_TO_INSTANT_APPS and RECEIVER_NOT_EXPORTED");
        }
        int n3 = n;
        if (n2 != 0) {
            n3 = (n | 0x2);
        }
        n = (n3 & 0x2);
        if (n == 0 && (n3 & 0x4) == 0x0) {
            throw new IllegalArgumentException("One of either RECEIVER_EXPORTED or RECEIVER_NOT_EXPORTED is required");
        }
        if (n != 0 && (n3 & 0x4) != 0x0) {
            throw new IllegalArgumentException("Cannot specify both RECEIVER_EXPORTED and RECEIVER_NOT_EXPORTED");
        }
        if (Build$VERSION.SDK_INT >= 33) {
            return Api33Impl.registerReceiver(context, broadcastReceiver, intentFilter, s, handler, n3);
        }
        if (Build$VERSION.SDK_INT >= 26) {
            return Api26Impl.registerReceiver(context, broadcastReceiver, intentFilter, s, handler, n3);
        }
        if ((n3 & 0x4) != 0x0 && s == null) {
            return context.registerReceiver(broadcastReceiver, intentFilter, obtainAndCheckReceiverPermission(context), handler);
        }
        return context.registerReceiver(broadcastReceiver, intentFilter, s, handler);
    }
    
    public static boolean startActivities(final Context context, final Intent[] array) {
        return startActivities(context, array, null);
    }
    
    public static boolean startActivities(final Context context, final Intent[] array, final Bundle bundle) {
        if (Build$VERSION.SDK_INT >= 16) {
            Api16Impl.startActivities(context, array, bundle);
        }
        else {
            context.startActivities(array);
        }
        return true;
    }
    
    public static void startActivity(final Context context, final Intent intent, final Bundle bundle) {
        if (Build$VERSION.SDK_INT >= 16) {
            Api16Impl.startActivity(context, intent, bundle);
        }
        else {
            context.startActivity(intent);
        }
    }
    
    public static void startForegroundService(final Context context, final Intent intent) {
        if (Build$VERSION.SDK_INT >= 26) {
            Api26Impl.startForegroundService(context, intent);
        }
        else {
            context.startService(intent);
        }
    }
    
    static class Api16Impl
    {
        private Api16Impl() {
        }
        
        static void startActivities(final Context context, final Intent[] array, final Bundle bundle) {
            context.startActivities(array, bundle);
        }
        
        static void startActivity(final Context context, final Intent intent, final Bundle bundle) {
            context.startActivity(intent, bundle);
        }
    }
    
    static class Api17Impl
    {
        private Api17Impl() {
        }
        
        static Context createConfigurationContext(final Context context, final Configuration configuration) {
            return context.createConfigurationContext(configuration);
        }
    }
    
    static class Api19Impl
    {
        private Api19Impl() {
        }
        
        static File[] getExternalCacheDirs(final Context context) {
            return context.getExternalCacheDirs();
        }
        
        static File[] getExternalFilesDirs(final Context context, final String s) {
            return context.getExternalFilesDirs(s);
        }
        
        static File[] getObbDirs(final Context context) {
            return context.getObbDirs();
        }
    }
    
    static class Api21Impl
    {
        private Api21Impl() {
        }
        
        static File getCodeCacheDir(final Context context) {
            return context.getCodeCacheDir();
        }
        
        static Drawable getDrawable(final Context context, final int n) {
            return context.getDrawable(n);
        }
        
        static File getNoBackupFilesDir(final Context context) {
            return context.getNoBackupFilesDir();
        }
    }
    
    static class Api23Impl
    {
        private Api23Impl() {
        }
        
        static int getColor(final Context context, final int n) {
            return context.getColor(n);
        }
        
        static <T> T getSystemService(final Context context, final Class<T> clazz) {
            return (T)context.getSystemService((Class)clazz);
        }
        
        static String getSystemServiceName(final Context context, final Class<?> clazz) {
            return context.getSystemServiceName((Class)clazz);
        }
    }
    
    static class Api24Impl
    {
        private Api24Impl() {
        }
        
        static Context createDeviceProtectedStorageContext(final Context context) {
            return context.createDeviceProtectedStorageContext();
        }
        
        static File getDataDir(final Context context) {
            return context.getDataDir();
        }
        
        static boolean isDeviceProtectedStorage(final Context context) {
            return context.isDeviceProtectedStorage();
        }
    }
    
    static class Api26Impl
    {
        private Api26Impl() {
        }
        
        static Intent registerReceiver(final Context context, final BroadcastReceiver broadcastReceiver, final IntentFilter intentFilter, final String s, final Handler handler, final int n) {
            if ((n & 0x4) != 0x0 && s == null) {
                return context.registerReceiver(broadcastReceiver, intentFilter, ContextCompat.obtainAndCheckReceiverPermission(context), handler);
            }
            return context.registerReceiver(broadcastReceiver, intentFilter, s, handler, n & 0x1);
        }
        
        static ComponentName startForegroundService(final Context context, final Intent intent) {
            return context.startForegroundService(intent);
        }
    }
    
    static class Api28Impl
    {
        private Api28Impl() {
        }
        
        static Executor getMainExecutor(final Context context) {
            return context.getMainExecutor();
        }
    }
    
    static class Api30Impl
    {
        private Api30Impl() {
        }
        
        static String getAttributionTag(final Context context) {
            return context.getAttributionTag();
        }
        
        static Display getDisplayOrDefault(final Context obj) {
            try {
                return obj.getDisplay();
            }
            catch (final UnsupportedOperationException ex) {
                final StringBuilder sb = new StringBuilder();
                sb.append("The context:");
                sb.append(obj);
                sb.append(" is not associated with any display. Return a fallback display instead.");
                Log.w("ContextCompat", sb.toString());
                return ((DisplayManager)obj.getSystemService((Class)DisplayManager.class)).getDisplay(0);
            }
        }
    }
    
    static class Api33Impl
    {
        private Api33Impl() {
        }
        
        static Intent registerReceiver(final Context context, final BroadcastReceiver broadcastReceiver, final IntentFilter intentFilter, final String s, final Handler handler, final int n) {
            return context.registerReceiver(broadcastReceiver, intentFilter, s, handler, n);
        }
    }
    
    private static final class LegacyServiceMapHolder
    {
        static final HashMap<Class<?>, String> SERVICES;
        
        static {
            final HashMap<Class<?>, String> hashMap = SERVICES = new HashMap<Class<?>, String>();
            if (Build$VERSION.SDK_INT >= 22) {
                hashMap.put(SubscriptionManager.class, "telephony_subscription_service");
                hashMap.put(UsageStatsManager.class, "usagestats");
            }
            if (Build$VERSION.SDK_INT >= 21) {
                hashMap.put(AppWidgetManager.class, "appwidget");
                hashMap.put(BatteryManager.class, "batterymanager");
                hashMap.put(CameraManager.class, "camera");
                hashMap.put(JobScheduler.class, "jobscheduler");
                hashMap.put(LauncherApps.class, "launcherapps");
                hashMap.put(MediaProjectionManager.class, "media_projection");
                hashMap.put(MediaSessionManager.class, "media_session");
                hashMap.put(RestrictionsManager.class, "restrictions");
                hashMap.put(TelecomManager.class, "telecom");
                hashMap.put(TvInputManager.class, "tv_input");
            }
            if (Build$VERSION.SDK_INT >= 19) {
                hashMap.put(AppOpsManager.class, "appops");
                hashMap.put(CaptioningManager.class, "captioning");
                hashMap.put(ConsumerIrManager.class, "consumer_ir");
                hashMap.put(PrintManager.class, "print");
            }
            if (Build$VERSION.SDK_INT >= 18) {
                hashMap.put(BluetoothManager.class, "bluetooth");
            }
            if (Build$VERSION.SDK_INT >= 17) {
                hashMap.put(DisplayManager.class, "display");
                hashMap.put(UserManager.class, "user");
            }
            if (Build$VERSION.SDK_INT >= 16) {
                hashMap.put(InputManager.class, "input");
                hashMap.put(MediaRouter.class, "media_router");
                hashMap.put(NsdManager.class, "servicediscovery");
            }
            hashMap.put(AccessibilityManager.class, "accessibility");
            hashMap.put(AccountManager.class, "account");
            hashMap.put(ActivityManager.class, "activity");
            hashMap.put(AlarmManager.class, "alarm");
            hashMap.put(AudioManager.class, "audio");
            hashMap.put(ClipboardManager.class, "clipboard");
            hashMap.put(ConnectivityManager.class, "connectivity");
            hashMap.put(DevicePolicyManager.class, "device_policy");
            hashMap.put(DownloadManager.class, "download");
            hashMap.put(DropBoxManager.class, "dropbox");
            hashMap.put(InputMethodManager.class, "input_method");
            hashMap.put(KeyguardManager.class, "keyguard");
            hashMap.put(LayoutInflater.class, "layout_inflater");
            hashMap.put(LocationManager.class, "location");
            hashMap.put(NfcManager.class, "nfc");
            hashMap.put(NotificationManager.class, "notification");
            hashMap.put(PowerManager.class, "power");
            hashMap.put(SearchManager.class, "search");
            hashMap.put(SensorManager.class, "sensor");
            hashMap.put(StorageManager.class, "storage");
            hashMap.put(TelephonyManager.class, "phone");
            hashMap.put(TextServicesManager.class, "textservices");
            hashMap.put(UiModeManager.class, "uimode");
            hashMap.put(UsbManager.class, "usb");
            hashMap.put(Vibrator.class, "vibrator");
            hashMap.put(WallpaperManager.class, "wallpaper");
            hashMap.put(WifiP2pManager.class, "wifip2p");
            hashMap.put(WifiManager.class, "wifi");
            hashMap.put(WindowManager.class, "window");
        }
    }
    
    @Retention(RetentionPolicy.SOURCE)
    public @interface RegisterReceiverFlags {
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.content;

import android.content.res.Configuration;
import androidx.core.util.Consumer;

public interface OnConfigurationChangedProvider
{
    void addOnConfigurationChangedListener(final Consumer<Configuration> p0);
    
    void removeOnConfigurationChangedListener(final Consumer<Configuration> p0);
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.content;

import android.os.IBinder;
import android.content.Intent;
import android.os.RemoteException;
import androidx.core.app.unusedapprestrictions.IUnusedAppRestrictionsBackportCallback;
import androidx.core.app.unusedapprestrictions.IUnusedAppRestrictionsBackportService;
import android.app.Service;

public abstract class UnusedAppRestrictionsBackportService extends Service
{
    public static final String ACTION_UNUSED_APP_RESTRICTIONS_BACKPORT_CONNECTION = "android.support.unusedapprestrictions.action.CustomUnusedAppRestrictionsBackportService";
    private IUnusedAppRestrictionsBackportService.Stub mBinder;
    
    public UnusedAppRestrictionsBackportService() {
        this.mBinder = new IUnusedAppRestrictionsBackportService.Stub() {
            final UnusedAppRestrictionsBackportService this$0;
            
            public void isPermissionRevocationEnabledForApp(final IUnusedAppRestrictionsBackportCallback unusedAppRestrictionsBackportCallback) throws RemoteException {
                if (unusedAppRestrictionsBackportCallback == null) {
                    return;
                }
                this.this$0.isPermissionRevocationEnabled(new UnusedAppRestrictionsBackportCallback(unusedAppRestrictionsBackportCallback));
            }
        };
    }
    
    protected abstract void isPermissionRevocationEnabled(final UnusedAppRestrictionsBackportCallback p0);
    
    public IBinder onBind(final Intent intent) {
        return (IBinder)this.mBinder;
    }
}

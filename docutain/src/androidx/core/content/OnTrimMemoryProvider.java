// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.content;

import androidx.core.util.Consumer;

public interface OnTrimMemoryProvider
{
    void addOnTrimMemoryListener(final Consumer<Integer> p0);
    
    void removeOnTrimMemoryListener(final Consumer<Integer> p0);
}

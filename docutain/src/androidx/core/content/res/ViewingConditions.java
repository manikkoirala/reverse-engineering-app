// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.content.res;

final class ViewingConditions
{
    static final ViewingConditions DEFAULT;
    private final float mAw;
    private final float mC;
    private final float mFl;
    private final float mFlRoot;
    private final float mN;
    private final float mNbb;
    private final float mNc;
    private final float mNcb;
    private final float[] mRgbD;
    private final float mZ;
    
    static {
        DEFAULT = make(CamUtils.WHITE_POINT_D65, (float)(CamUtils.yFromLStar(50.0f) * 63.66197723675813 / 100.0), 50.0f, 2.0f, false);
    }
    
    private ViewingConditions(final float mn, final float mAw, final float mNbb, final float mNcb, final float mc, final float mNc, final float[] mRgbD, final float mFl, final float mFlRoot, final float mz) {
        this.mN = mn;
        this.mAw = mAw;
        this.mNbb = mNbb;
        this.mNcb = mNcb;
        this.mC = mc;
        this.mNc = mNc;
        this.mRgbD = mRgbD;
        this.mFl = mFl;
        this.mFlRoot = mFlRoot;
        this.mZ = mz;
    }
    
    static ViewingConditions make(float[] array, float n, float n2, float n3, final boolean b) {
        final float[][] xyz_TO_CAM16RGB = CamUtils.XYZ_TO_CAM16RGB;
        final float n4 = array[0];
        final float[] array2 = xyz_TO_CAM16RGB[0];
        final float n5 = array2[0];
        final float n6 = array[1];
        final float n7 = array2[1];
        final float n8 = array[2];
        final float n9 = n5 * n4 + n7 * n6 + array2[2] * n8;
        final float[] array3 = xyz_TO_CAM16RGB[1];
        final float n10 = array3[0] * n4 + array3[1] * n6 + array3[2] * n8;
        final float[] array4 = xyz_TO_CAM16RGB[2];
        final float n11 = n4 * array4[0] + n6 * array4[1] + n8 * array4[2];
        final float n12 = n3 / 10.0f + 0.8f;
        float n13;
        if (n12 >= 0.9) {
            n13 = CamUtils.lerp(0.59f, 0.69f, (n12 - 0.9f) * 10.0f);
        }
        else {
            n13 = CamUtils.lerp(0.525f, 0.59f, (n12 - 0.8f) * 10.0f);
        }
        if (b) {
            n3 = 1.0f;
        }
        else {
            n3 = (1.0f - (float)Math.exp((-n - 42.0f) / 92.0f) * 0.2777778f) * n12;
        }
        final double n14 = n3;
        if (n14 > 1.0) {
            n3 = 1.0f;
        }
        else if (n14 < 0.0) {
            n3 = 0.0f;
        }
        final float[] array5 = { 100.0f / n9 * n3 + 1.0f - n3, 100.0f / n10 * n3 + 1.0f - n3, 100.0f / n11 * n3 + 1.0f - n3 };
        n3 = 1.0f / (5.0f * n + 1.0f);
        final float n15 = n3 * n3 * n3 * n3;
        n3 = 1.0f - n15;
        n = n15 * n + 0.1f * n3 * n3 * (float)Math.cbrt(n * 5.0);
        n3 = CamUtils.yFromLStar(n2) / array[1];
        final double n16 = n3;
        final float n17 = (float)Math.sqrt(n16);
        n2 = 0.725f / (float)Math.pow(n16, 0.2);
        array = new float[] { (float)Math.pow(array5[0] * n * n9 / 100.0, 0.42), (float)Math.pow(array5[1] * n * n10 / 100.0, 0.42), 0.0f };
        final float n18 = (float)Math.pow(array5[2] * n * n11 / 100.0, 0.42);
        array[2] = n18;
        final float n19 = array[0];
        final float n20 = n19 * 400.0f / (n19 + 27.13f);
        final float n21 = array[1];
        return new ViewingConditions(n3, (n20 * 2.0f + n21 * 400.0f / (n21 + 27.13f) + 400.0f * n18 / (n18 + 27.13f) * 0.05f) * n2, n2, n2, n13, n12, array5, n, (float)Math.pow(n, 0.25), n17 + 1.48f);
    }
    
    float getAw() {
        return this.mAw;
    }
    
    float getC() {
        return this.mC;
    }
    
    float getFl() {
        return this.mFl;
    }
    
    float getFlRoot() {
        return this.mFlRoot;
    }
    
    float getN() {
        return this.mN;
    }
    
    float getNbb() {
        return this.mNbb;
    }
    
    float getNc() {
        return this.mNc;
    }
    
    float getNcb() {
        return this.mNcb;
    }
    
    float[] getRgbD() {
        return this.mRgbD;
    }
    
    float getZ() {
        return this.mZ;
    }
}

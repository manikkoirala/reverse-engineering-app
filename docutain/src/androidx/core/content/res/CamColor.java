// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.content.res;

import androidx.core.graphics.ColorUtils;

public class CamColor
{
    private static final float CHROMA_SEARCH_ENDPOINT = 0.4f;
    private static final float DE_MAX = 1.0f;
    private static final float DL_MAX = 0.2f;
    private static final float LIGHTNESS_SEARCH_ENDPOINT = 0.01f;
    private final float mAstar;
    private final float mBstar;
    private final float mChroma;
    private final float mHue;
    private final float mJ;
    private final float mJstar;
    private final float mM;
    private final float mQ;
    private final float mS;
    
    CamColor(final float mHue, final float mChroma, final float mj, final float mq, final float mm, final float ms, final float mJstar, final float mAstar, final float mBstar) {
        this.mHue = mHue;
        this.mChroma = mChroma;
        this.mJ = mj;
        this.mQ = mq;
        this.mM = mm;
        this.mS = ms;
        this.mJstar = mJstar;
        this.mAstar = mAstar;
        this.mBstar = mBstar;
    }
    
    private static CamColor findCamByJ(final float n, final float n2, final float n3) {
        float n4 = 1000.0f;
        CamColor camColor = null;
        float n5 = 1000.0f;
        float n6 = 100.0f;
        float n7 = 0.0f;
        CamColor camColor2;
        while (true) {
            camColor2 = camColor;
            if (Math.abs(n7 - n6) <= 0.01f) {
                break;
            }
            final float n8 = (n6 - n7) / 2.0f + n7;
            final int viewedInSrgb = fromJch(n8, n2, n).viewedInSrgb();
            final float lStarFromInt = CamUtils.lStarFromInt(viewedInSrgb);
            final float abs = Math.abs(n3 - lStarFromInt);
            float n9 = n4;
            float n10 = n5;
            camColor2 = camColor;
            if (abs < 0.2f) {
                final CamColor fromColor = fromColor(viewedInSrgb);
                final float distance = fromColor.distance(fromJch(fromColor.getJ(), fromColor.getChroma(), n));
                n9 = n4;
                n10 = n5;
                camColor2 = camColor;
                if (distance <= 1.0f) {
                    camColor2 = fromColor;
                    n9 = abs;
                    n10 = distance;
                }
            }
            if (n9 == 0.0f && n10 == 0.0f) {
                break;
            }
            if (lStarFromInt < n3) {
                n7 = n8;
                n4 = n9;
                n5 = n10;
                camColor = camColor2;
            }
            else {
                n6 = n8;
                n4 = n9;
                n5 = n10;
                camColor = camColor2;
            }
        }
        return camColor2;
    }
    
    static CamColor fromColor(final int n) {
        final float[] array = new float[7];
        final float[] array2 = new float[3];
        fromColorInViewingConditions(n, ViewingConditions.DEFAULT, array, array2);
        return new CamColor(array2[0], array2[1], array[0], array[1], array[2], array[3], array[4], array[5], array[6]);
    }
    
    static void fromColorInViewingConditions(final int n, final ViewingConditions viewingConditions, final float[] array, final float[] array2) {
        CamUtils.xyzFromInt(n, array2);
        final float[][] xyz_TO_CAM16RGB = CamUtils.XYZ_TO_CAM16RGB;
        final float n2 = array2[0];
        final float[] array3 = xyz_TO_CAM16RGB[0];
        final float n3 = array3[0];
        final float n4 = array2[1];
        final float n5 = array3[1];
        final float n6 = array2[2];
        final float n7 = array3[2];
        final float[] array4 = xyz_TO_CAM16RGB[1];
        final float n8 = array4[0];
        final float n9 = array4[1];
        final float n10 = array4[2];
        final float[] array5 = xyz_TO_CAM16RGB[2];
        final float n11 = array5[0];
        final float n12 = array5[1];
        final float n13 = array5[2];
        final float n14 = viewingConditions.getRgbD()[0] * (n3 * n2 + n5 * n4 + n7 * n6);
        final float n15 = viewingConditions.getRgbD()[1] * (n8 * n2 + n9 * n4 + n10 * n6);
        final float n16 = viewingConditions.getRgbD()[2] * (n2 * n11 + n4 * n12 + n6 * n13);
        final float n17 = (float)Math.pow(viewingConditions.getFl() * Math.abs(n14) / 100.0, 0.42);
        final float n18 = (float)Math.pow(viewingConditions.getFl() * Math.abs(n15) / 100.0, 0.42);
        final float n19 = (float)Math.pow(viewingConditions.getFl() * Math.abs(n16) / 100.0, 0.42);
        final float n20 = Math.signum(n14) * 400.0f * n17 / (n17 + 27.13f);
        final float n21 = Math.signum(n15) * 400.0f * n18 / (n18 + 27.13f);
        final float n22 = Math.signum(n16) * 400.0f * n19 / (n19 + 27.13f);
        final double n23 = n20;
        final double n24 = n21;
        final double n25 = n22;
        final float n26 = (float)(n23 * 11.0 + n24 * -12.0 + n25) / 11.0f;
        final float n27 = (float)(n20 + n21 - n25 * 2.0) / 9.0f;
        final float n28 = n21 * 20.0f;
        final float n29 = (n20 * 20.0f + n28 + 21.0f * n22) / 20.0f;
        final float n30 = (n20 * 40.0f + n28 + n22) / 20.0f;
        final float n31 = (float)Math.atan2(n27, n26) * 180.0f / 3.1415927f;
        float n32;
        if (n31 < 0.0f) {
            n32 = n31 + 360.0f;
        }
        else {
            n32 = n31;
            if (n31 >= 360.0f) {
                n32 = n31 - 360.0f;
            }
        }
        final float n33 = 3.1415927f * n32 / 180.0f;
        final float n34 = (float)Math.pow(n30 * viewingConditions.getNbb() / viewingConditions.getAw(), viewingConditions.getC() * viewingConditions.getZ()) * 100.0f;
        final float n35 = 4.0f / viewingConditions.getC();
        final float n36 = (float)Math.sqrt(n34 / 100.0f);
        final float aw = viewingConditions.getAw();
        final float flRoot = viewingConditions.getFlRoot();
        float n37;
        if (n32 < 20.14) {
            n37 = 360.0f + n32;
        }
        else {
            n37 = n32;
        }
        final float n38 = (float)Math.pow(1.64 - Math.pow(0.29, viewingConditions.getN()), 0.73) * (float)Math.pow((float)(Math.cos(n37 * 3.141592653589793 / 180.0 + 2.0) + 3.8) * 0.25f * 3846.1538f * viewingConditions.getNc() * viewingConditions.getNcb() * (float)Math.sqrt(n26 * n26 + n27 * n27) / (n29 + 0.305f), 0.9);
        final float n39 = (float)Math.sqrt(n34 / 100.0) * n38;
        final float n40 = viewingConditions.getFlRoot() * n39;
        final float n41 = (float)Math.sqrt(n38 * viewingConditions.getC() / (viewingConditions.getAw() + 4.0f));
        final float n42 = 1.7f * n34 / (0.007f * n34 + 1.0f);
        final float n43 = (float)Math.log(0.0228f * n40 + 1.0f) * 43.85965f;
        final double n44 = n33;
        final float n45 = (float)Math.cos(n44);
        final float n46 = (float)Math.sin(n44);
        array2[0] = n32;
        array2[1] = n39;
        if (array != null) {
            array[0] = n34;
            array[1] = n35 * n36 * (aw + 4.0f) * flRoot;
            array[2] = n40;
            array[3] = n41 * 50.0f;
            array[4] = n42;
            array[5] = n45 * n43;
            array[6] = n43 * n46;
        }
    }
    
    private static CamColor fromJch(final float n, final float n2, final float n3) {
        return fromJchInFrame(n, n2, n3, ViewingConditions.DEFAULT);
    }
    
    private static CamColor fromJchInFrame(final float n, final float n2, final float n3, final ViewingConditions viewingConditions) {
        final float n4 = 4.0f / viewingConditions.getC();
        final double n5 = n / 100.0;
        final float n6 = (float)Math.sqrt(n5);
        final float aw = viewingConditions.getAw();
        final float flRoot = viewingConditions.getFlRoot();
        final float n7 = n2 * viewingConditions.getFlRoot();
        final float n8 = (float)Math.sqrt(n2 / (float)Math.sqrt(n5) * viewingConditions.getC() / (viewingConditions.getAw() + 4.0f));
        final float n9 = 3.1415927f * n3 / 180.0f;
        final float n10 = 1.7f * n / (0.007f * n + 1.0f);
        final float n11 = (float)Math.log(n7 * 0.0228 + 1.0) * 43.85965f;
        final double n12 = n9;
        return new CamColor(n3, n2, n, n4 * n6 * (aw + 4.0f) * flRoot, n7, n8 * 50.0f, n10, n11 * (float)Math.cos(n12), n11 * (float)Math.sin(n12));
    }
    
    public static void getM3HCTfromColor(final int n, final float[] array) {
        fromColorInViewingConditions(n, ViewingConditions.DEFAULT, null, array);
        array[2] = CamUtils.lStarFromInt(n);
    }
    
    public static int toColor(final float n, final float n2, final float n3) {
        return toColor(n, n2, n3, ViewingConditions.DEFAULT);
    }
    
    static int toColor(float b, float n, final float n2, final ViewingConditions viewingConditions) {
        if (n < 1.0 || Math.round(n2) <= 0.0 || Math.round(n2) >= 100.0) {
            return CamUtils.intFromLStar(n2);
        }
        float min;
        if (b < 0.0f) {
            min = 0.0f;
        }
        else {
            min = Math.min(360.0f, b);
        }
        b = n;
        CamColor camColor = null;
        float n3 = 0.0f;
        int n4 = 1;
        while (Math.abs(n3 - n) >= 0.4f) {
            final CamColor camByJ = findCamByJ(min, b, n2);
            if (n4 != 0) {
                if (camByJ != null) {
                    return camByJ.viewed(viewingConditions);
                }
                n4 = 0;
            }
            else if (camByJ == null) {
                n = b;
            }
            else {
                camColor = camByJ;
                n3 = b;
            }
            b = (n - n3) / 2.0f + n3;
        }
        if (camColor == null) {
            return CamUtils.intFromLStar(n2);
        }
        return camColor.viewed(viewingConditions);
    }
    
    float distance(final CamColor camColor) {
        final float n = this.getJStar() - camColor.getJStar();
        final float n2 = this.getAStar() - camColor.getAStar();
        final float n3 = this.getBStar() - camColor.getBStar();
        return (float)(Math.pow(Math.sqrt(n * n + n2 * n2 + n3 * n3), 0.63) * 1.41);
    }
    
    float getAStar() {
        return this.mAstar;
    }
    
    float getBStar() {
        return this.mBstar;
    }
    
    float getChroma() {
        return this.mChroma;
    }
    
    float getHue() {
        return this.mHue;
    }
    
    float getJ() {
        return this.mJ;
    }
    
    float getJStar() {
        return this.mJstar;
    }
    
    float getM() {
        return this.mM;
    }
    
    float getQ() {
        return this.mQ;
    }
    
    float getS() {
        return this.mS;
    }
    
    int viewed(final ViewingConditions viewingConditions) {
        float n;
        if (this.getChroma() != 0.0 && this.getJ() != 0.0) {
            n = this.getChroma() / (float)Math.sqrt(this.getJ() / 100.0);
        }
        else {
            n = 0.0f;
        }
        final float n2 = (float)Math.pow(n / Math.pow(1.64 - Math.pow(0.29, viewingConditions.getN()), 0.73), 1.1111111111111112);
        final double n3 = this.getHue() * 3.1415927f / 180.0f;
        final float n4 = (float)(Math.cos(2.0 + n3) + 3.8);
        final float aw = viewingConditions.getAw();
        final float n5 = (float)Math.pow(this.getJ() / 100.0, 1.0 / viewingConditions.getC() / viewingConditions.getZ());
        final float nc = viewingConditions.getNc();
        final float ncb = viewingConditions.getNcb();
        final float n6 = aw * n5 / viewingConditions.getNbb();
        final float n7 = (float)Math.sin(n3);
        final float n8 = (float)Math.cos(n3);
        final float n9 = (0.305f + n6) * 23.0f * n2 / (n4 * 0.25f * 3846.1538f * nc * ncb * 23.0f + 11.0f * n2 * n8 + n2 * 108.0f * n7);
        final float n10 = n8 * n9;
        final float n11 = n9 * n7;
        final float n12 = n6 * 460.0f;
        final float f = (451.0f * n10 + n12 + 288.0f * n11) / 1403.0f;
        final float f2 = (n12 - 891.0f * n10 - 261.0f * n11) / 1403.0f;
        final float f3 = (n12 - n10 * 220.0f - n11 * 6300.0f) / 1403.0f;
        final float n13 = (float)Math.max(0.0, Math.abs(f) * 27.13 / (400.0 - Math.abs(f)));
        final float signum = Math.signum(f);
        final float n14 = 100.0f / viewingConditions.getFl();
        final float n15 = (float)Math.pow(n13, 2.380952380952381);
        final float n16 = (float)Math.max(0.0, Math.abs(f2) * 27.13 / (400.0 - Math.abs(f2)));
        final float signum2 = Math.signum(f2);
        final float n17 = 100.0f / viewingConditions.getFl();
        final float n18 = (float)Math.pow(n16, 2.380952380952381);
        final float n19 = (float)Math.max(0.0, Math.abs(f3) * 27.13 / (400.0 - Math.abs(f3)));
        final float signum3 = Math.signum(f3);
        final float n20 = 100.0f / viewingConditions.getFl();
        final float n21 = (float)Math.pow(n19, 2.380952380952381);
        final float n22 = signum * n14 * n15 / viewingConditions.getRgbD()[0];
        final float n23 = signum2 * n17 * n18 / viewingConditions.getRgbD()[1];
        final float n24 = signum3 * n20 * n21 / viewingConditions.getRgbD()[2];
        final float[][] cam16RGB_TO_XYZ = CamUtils.CAM16RGB_TO_XYZ;
        final float[] array = cam16RGB_TO_XYZ[0];
        final float n25 = array[0];
        final float n26 = array[1];
        final float n27 = array[2];
        final float[] array2 = cam16RGB_TO_XYZ[1];
        final float n28 = array2[0];
        final float n29 = array2[1];
        final float n30 = array2[2];
        final float[] array3 = cam16RGB_TO_XYZ[2];
        return ColorUtils.XYZToColor(n25 * n22 + n26 * n23 + n27 * n24, n28 * n22 + n29 * n23 + n30 * n24, n22 * array3[0] + n23 * array3[1] + n24 * array3[2]);
    }
    
    int viewedInSrgb() {
        return this.viewed(ViewingConditions.DEFAULT);
    }
}

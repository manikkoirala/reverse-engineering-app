// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.content.res;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import android.os.Looper;
import androidx.core.util.ObjectsCompat;
import android.content.res.Configuration;
import org.xmlpull.v1.XmlPullParserException;
import java.io.IOException;
import androidx.core.graphics.TypefaceCompat;
import android.content.res.XmlResourceParser;
import android.util.Log;
import org.xmlpull.v1.XmlPullParser;
import androidx.core.util.Preconditions;
import android.graphics.drawable.Drawable;
import android.os.Build$VERSION;
import android.content.res.Resources;
import android.content.res.Resources$NotFoundException;
import android.os.Handler;
import android.graphics.Typeface;
import android.content.Context;
import java.util.Iterator;
import android.content.res.Resources$Theme;
import android.content.res.ColorStateList;
import android.util.TypedValue;
import android.util.SparseArray;
import java.util.WeakHashMap;

public final class ResourcesCompat
{
    public static final int ID_NULL = 0;
    private static final String TAG = "ResourcesCompat";
    private static final Object sColorStateCacheLock;
    private static final WeakHashMap<ColorStateListCacheKey, SparseArray<ColorStateListCacheEntry>> sColorStateCaches;
    private static final ThreadLocal<TypedValue> sTempTypedValue;
    
    static {
        sTempTypedValue = new ThreadLocal<TypedValue>();
        sColorStateCaches = new WeakHashMap<ColorStateListCacheKey, SparseArray<ColorStateListCacheEntry>>(0);
        sColorStateCacheLock = new Object();
    }
    
    private ResourcesCompat() {
    }
    
    private static void addColorStateListToCache(final ColorStateListCacheKey colorStateListCacheKey, final int n, final ColorStateList list, final Resources$Theme resources$Theme) {
        synchronized (ResourcesCompat.sColorStateCacheLock) {
            final WeakHashMap<ColorStateListCacheKey, SparseArray<ColorStateListCacheEntry>> sColorStateCaches = ResourcesCompat.sColorStateCaches;
            SparseArray value;
            if ((value = sColorStateCaches.get(colorStateListCacheKey)) == null) {
                value = new SparseArray();
                sColorStateCaches.put(colorStateListCacheKey, (SparseArray<ColorStateListCacheEntry>)value);
            }
            value.append(n, (Object)new ColorStateListCacheEntry(list, colorStateListCacheKey.mResources.getConfiguration(), resources$Theme));
        }
    }
    
    public static void clearCachesForTheme(final Resources$Theme resources$Theme) {
        synchronized (ResourcesCompat.sColorStateCacheLock) {
            final Iterator<ColorStateListCacheKey> iterator = ResourcesCompat.sColorStateCaches.keySet().iterator();
            while (iterator.hasNext()) {
                final ColorStateListCacheKey colorStateListCacheKey = iterator.next();
                if (colorStateListCacheKey != null && resources$Theme.equals((Object)colorStateListCacheKey.mTheme)) {
                    iterator.remove();
                }
            }
        }
    }
    
    private static ColorStateList getCachedColorStateList(final ColorStateListCacheKey key, final int n) {
        synchronized (ResourcesCompat.sColorStateCacheLock) {
            final SparseArray sparseArray = ResourcesCompat.sColorStateCaches.get(key);
            if (sparseArray != null && sparseArray.size() > 0) {
                final ColorStateListCacheEntry colorStateListCacheEntry = (ColorStateListCacheEntry)sparseArray.get(n);
                if (colorStateListCacheEntry != null) {
                    if (colorStateListCacheEntry.mConfiguration.equals(key.mResources.getConfiguration()) && ((key.mTheme == null && colorStateListCacheEntry.mThemeHash == 0) || (key.mTheme != null && colorStateListCacheEntry.mThemeHash == key.mTheme.hashCode()))) {
                        return colorStateListCacheEntry.mValue;
                    }
                    sparseArray.remove(n);
                }
            }
            return null;
        }
    }
    
    public static Typeface getCachedFont(final Context context, final int n) throws Resources$NotFoundException {
        if (context.isRestricted()) {
            return null;
        }
        return loadFont(context, n, new TypedValue(), 0, null, null, false, true);
    }
    
    public static int getColor(final Resources resources, final int n, final Resources$Theme resources$Theme) throws Resources$NotFoundException {
        if (Build$VERSION.SDK_INT >= 23) {
            return Api23Impl.getColor(resources, n, resources$Theme);
        }
        return resources.getColor(n);
    }
    
    public static ColorStateList getColorStateList(final Resources resources, final int n, final Resources$Theme resources$Theme) throws Resources$NotFoundException {
        final ColorStateListCacheKey colorStateListCacheKey = new ColorStateListCacheKey(resources, resources$Theme);
        final ColorStateList cachedColorStateList = getCachedColorStateList(colorStateListCacheKey, n);
        if (cachedColorStateList != null) {
            return cachedColorStateList;
        }
        final ColorStateList inflateColorStateList = inflateColorStateList(resources, n, resources$Theme);
        if (inflateColorStateList != null) {
            addColorStateListToCache(colorStateListCacheKey, n, inflateColorStateList, resources$Theme);
            return inflateColorStateList;
        }
        if (Build$VERSION.SDK_INT >= 23) {
            return Api23Impl.getColorStateList(resources, n, resources$Theme);
        }
        return resources.getColorStateList(n);
    }
    
    public static Drawable getDrawable(final Resources resources, final int n, final Resources$Theme resources$Theme) throws Resources$NotFoundException {
        if (Build$VERSION.SDK_INT >= 21) {
            return Api21Impl.getDrawable(resources, n, resources$Theme);
        }
        return resources.getDrawable(n);
    }
    
    public static Drawable getDrawableForDensity(final Resources resources, final int n, final int n2, final Resources$Theme resources$Theme) throws Resources$NotFoundException {
        if (Build$VERSION.SDK_INT >= 21) {
            return Api21Impl.getDrawableForDensity(resources, n, n2, resources$Theme);
        }
        if (Build$VERSION.SDK_INT >= 15) {
            return Api15Impl.getDrawableForDensity(resources, n, n2);
        }
        return resources.getDrawable(n);
    }
    
    public static float getFloat(final Resources resources, final int i) {
        if (Build$VERSION.SDK_INT >= 29) {
            return Api29Impl.getFloat(resources, i);
        }
        final TypedValue typedValue = getTypedValue();
        resources.getValue(i, typedValue, true);
        if (typedValue.type == 4) {
            return typedValue.getFloat();
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Resource ID #0x");
        sb.append(Integer.toHexString(i));
        sb.append(" type #0x");
        sb.append(Integer.toHexString(typedValue.type));
        sb.append(" is not valid");
        throw new Resources$NotFoundException(sb.toString());
    }
    
    public static Typeface getFont(final Context context, final int n) throws Resources$NotFoundException {
        if (context.isRestricted()) {
            return null;
        }
        return loadFont(context, n, new TypedValue(), 0, null, null, false, false);
    }
    
    public static Typeface getFont(final Context context, final int n, final TypedValue typedValue, final int n2, final FontCallback fontCallback) throws Resources$NotFoundException {
        if (context.isRestricted()) {
            return null;
        }
        return loadFont(context, n, typedValue, n2, fontCallback, null, true, false);
    }
    
    public static void getFont(final Context context, final int n, final FontCallback fontCallback, final Handler handler) throws Resources$NotFoundException {
        Preconditions.checkNotNull(fontCallback);
        if (context.isRestricted()) {
            fontCallback.callbackFailAsync(-4, handler);
            return;
        }
        loadFont(context, n, new TypedValue(), 0, fontCallback, handler, false, false);
    }
    
    private static TypedValue getTypedValue() {
        final ThreadLocal<TypedValue> sTempTypedValue = ResourcesCompat.sTempTypedValue;
        TypedValue value;
        if ((value = sTempTypedValue.get()) == null) {
            value = new TypedValue();
            sTempTypedValue.set(value);
        }
        return value;
    }
    
    private static ColorStateList inflateColorStateList(final Resources resources, final int n, final Resources$Theme resources$Theme) {
        if (isColorInt(resources, n)) {
            return null;
        }
        final XmlResourceParser xml = resources.getXml(n);
        try {
            return ColorStateListInflaterCompat.createFromXml(resources, (XmlPullParser)xml, resources$Theme);
        }
        catch (final Exception ex) {
            Log.w("ResourcesCompat", "Failed to inflate ColorStateList, leaving it to the framework", (Throwable)ex);
            return null;
        }
    }
    
    private static boolean isColorInt(final Resources resources, final int n) {
        final TypedValue typedValue = getTypedValue();
        boolean b = true;
        resources.getValue(n, typedValue, true);
        if (typedValue.type < 28 || typedValue.type > 31) {
            b = false;
        }
        return b;
    }
    
    private static Typeface loadFont(final Context context, final int i, final TypedValue typedValue, final int n, final FontCallback fontCallback, final Handler handler, final boolean b, final boolean b2) {
        final Resources resources = context.getResources();
        resources.getValue(i, typedValue, true);
        final Typeface loadFont = loadFont(context, resources, typedValue, i, n, fontCallback, handler, b, b2);
        if (loadFont == null && fontCallback == null && !b2) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Font resource ID #0x");
            sb.append(Integer.toHexString(i));
            sb.append(" could not be retrieved.");
            throw new Resources$NotFoundException(sb.toString());
        }
        return loadFont;
    }
    
    private static Typeface loadFont(final Context context, final Resources resources, final TypedValue obj, final int i, final int n, final FontCallback fontCallback, final Handler handler, final boolean b, final boolean b2) {
        if (obj.string == null) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Resource \"");
            sb.append(resources.getResourceName(i));
            sb.append("\" (");
            sb.append(Integer.toHexString(i));
            sb.append(") is not a Font: ");
            sb.append(obj);
            throw new Resources$NotFoundException(sb.toString());
        }
        final String string = obj.string.toString();
        if (!string.startsWith("res/")) {
            if (fontCallback != null) {
                fontCallback.callbackFailAsync(-3, handler);
            }
            return null;
        }
        final Typeface fromCache = TypefaceCompat.findFromCache(resources, i, string, obj.assetCookie, n);
        if (fromCache != null) {
            if (fontCallback != null) {
                fontCallback.callbackSuccessAsync(fromCache, handler);
            }
            return fromCache;
        }
        if (b2) {
            return null;
        }
        try {
            if (!string.toLowerCase().endsWith(".xml")) {
                final Typeface fromResourcesFontFile = TypefaceCompat.createFromResourcesFontFile(context, resources, i, string, obj.assetCookie, n);
                if (fontCallback != null) {
                    if (fromResourcesFontFile != null) {
                        fontCallback.callbackSuccessAsync(fromResourcesFontFile, handler);
                    }
                    else {
                        fontCallback.callbackFailAsync(-3, handler);
                    }
                }
                return fromResourcesFontFile;
            }
            final FontResourcesParserCompat.FamilyResourceEntry parse = FontResourcesParserCompat.parse((XmlPullParser)resources.getXml(i), resources);
            if (parse == null) {
                Log.e("ResourcesCompat", "Failed to find font-family tag");
                if (fontCallback != null) {
                    fontCallback.callbackFailAsync(-3, handler);
                }
                return null;
            }
            return TypefaceCompat.createFromResourcesFamilyXml(context, parse, resources, i, string, obj.assetCookie, n, fontCallback, handler, b);
        }
        catch (final IOException ex) {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("Failed to read xml resource ");
            sb2.append(string);
            Log.e("ResourcesCompat", sb2.toString(), (Throwable)ex);
        }
        catch (final XmlPullParserException ex2) {
            final StringBuilder sb3 = new StringBuilder();
            sb3.append("Failed to parse xml resource ");
            sb3.append(string);
            Log.e("ResourcesCompat", sb3.toString(), (Throwable)ex2);
        }
        if (fontCallback != null) {
            fontCallback.callbackFailAsync(-3, handler);
        }
        return null;
    }
    
    static class Api15Impl
    {
        private Api15Impl() {
        }
        
        static Drawable getDrawableForDensity(final Resources resources, final int n, final int n2) {
            return resources.getDrawableForDensity(n, n2);
        }
    }
    
    static class Api21Impl
    {
        private Api21Impl() {
        }
        
        static Drawable getDrawable(final Resources resources, final int n, final Resources$Theme resources$Theme) {
            return resources.getDrawable(n, resources$Theme);
        }
        
        static Drawable getDrawableForDensity(final Resources resources, final int n, final int n2, final Resources$Theme resources$Theme) {
            return resources.getDrawableForDensity(n, n2, resources$Theme);
        }
    }
    
    static class Api23Impl
    {
        private Api23Impl() {
        }
        
        static int getColor(final Resources resources, final int n, final Resources$Theme resources$Theme) {
            return resources.getColor(n, resources$Theme);
        }
        
        static ColorStateList getColorStateList(final Resources resources, final int n, final Resources$Theme resources$Theme) {
            return resources.getColorStateList(n, resources$Theme);
        }
    }
    
    static class Api29Impl
    {
        private Api29Impl() {
        }
        
        static float getFloat(final Resources resources, final int n) {
            return resources.getFloat(n);
        }
    }
    
    private static class ColorStateListCacheEntry
    {
        final Configuration mConfiguration;
        final int mThemeHash;
        final ColorStateList mValue;
        
        ColorStateListCacheEntry(final ColorStateList mValue, final Configuration mConfiguration, final Resources$Theme resources$Theme) {
            this.mValue = mValue;
            this.mConfiguration = mConfiguration;
            int hashCode;
            if (resources$Theme == null) {
                hashCode = 0;
            }
            else {
                hashCode = resources$Theme.hashCode();
            }
            this.mThemeHash = hashCode;
        }
    }
    
    private static final class ColorStateListCacheKey
    {
        final Resources mResources;
        final Resources$Theme mTheme;
        
        ColorStateListCacheKey(final Resources mResources, final Resources$Theme mTheme) {
            this.mResources = mResources;
            this.mTheme = mTheme;
        }
        
        @Override
        public boolean equals(final Object o) {
            boolean b = true;
            if (this == o) {
                return true;
            }
            if (o != null && this.getClass() == o.getClass()) {
                final ColorStateListCacheKey colorStateListCacheKey = (ColorStateListCacheKey)o;
                if (!this.mResources.equals(colorStateListCacheKey.mResources) || !ObjectsCompat.equals(this.mTheme, colorStateListCacheKey.mTheme)) {
                    b = false;
                }
                return b;
            }
            return false;
        }
        
        @Override
        public int hashCode() {
            return ObjectsCompat.hash(this.mResources, this.mTheme);
        }
    }
    
    public abstract static class FontCallback
    {
        public static Handler getHandler(final Handler handler) {
            Handler handler2 = handler;
            if (handler == null) {
                handler2 = new Handler(Looper.getMainLooper());
            }
            return handler2;
        }
        
        public final void callbackFailAsync(final int n, final Handler handler) {
            getHandler(handler).post((Runnable)new ResourcesCompat$FontCallback$$ExternalSyntheticLambda1(this, n));
        }
        
        public final void callbackSuccessAsync(final Typeface typeface, final Handler handler) {
            getHandler(handler).post((Runnable)new ResourcesCompat$FontCallback$$ExternalSyntheticLambda0(this, typeface));
        }
        
        public abstract void onFontRetrievalFailed(final int p0);
        
        public abstract void onFontRetrieved(final Typeface p0);
    }
    
    public static final class ThemeCompat
    {
        private ThemeCompat() {
        }
        
        public static void rebase(final Resources$Theme resources$Theme) {
            if (Build$VERSION.SDK_INT >= 29) {
                Api29Impl.rebase(resources$Theme);
            }
            else if (Build$VERSION.SDK_INT >= 23) {
                Api23Impl.rebase(resources$Theme);
            }
        }
        
        static class Api23Impl
        {
            private static Method sRebaseMethod;
            private static boolean sRebaseMethodFetched;
            private static final Object sRebaseMethodLock;
            
            static {
                sRebaseMethodLock = new Object();
            }
            
            private Api23Impl() {
            }
            
            static void rebase(final Resources$Theme obj) {
                synchronized (Api23Impl.sRebaseMethodLock) {
                    if (!Api23Impl.sRebaseMethodFetched) {
                        try {
                            (Api23Impl.sRebaseMethod = Resources$Theme.class.getDeclaredMethod("rebase", (Class<?>[])new Class[0])).setAccessible(true);
                        }
                        catch (final NoSuchMethodException ex) {
                            Log.i("ResourcesCompat", "Failed to retrieve rebase() method", (Throwable)ex);
                        }
                        Api23Impl.sRebaseMethodFetched = true;
                    }
                    final Method sRebaseMethod = Api23Impl.sRebaseMethod;
                    if (sRebaseMethod != null) {
                        try {
                            sRebaseMethod.invoke(obj, new Object[0]);
                            return;
                        }
                        catch (final InvocationTargetException obj) {}
                        catch (final IllegalAccessException ex2) {}
                        Log.i("ResourcesCompat", "Failed to invoke rebase() method via reflection", (Throwable)obj);
                        Api23Impl.sRebaseMethod = null;
                    }
                }
            }
        }
        
        static class Api29Impl
        {
            private Api29Impl() {
            }
            
            static void rebase(final Resources$Theme resources$Theme) {
                resources$Theme.rebase();
            }
        }
    }
}

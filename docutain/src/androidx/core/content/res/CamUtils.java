// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.content.res;

import android.graphics.Color;
import androidx.core.graphics.ColorUtils;

final class CamUtils
{
    static final float[][] CAM16RGB_TO_XYZ;
    static final float[][] SRGB_TO_XYZ;
    static final float[] WHITE_POINT_D65;
    static final float[][] XYZ_TO_CAM16RGB;
    
    static {
        XYZ_TO_CAM16RGB = new float[][] { { 0.401288f, 0.650173f, -0.051461f }, { -0.250268f, 1.204414f, 0.045854f }, { -0.002079f, 0.048952f, 0.953127f } };
        CAM16RGB_TO_XYZ = new float[][] { { 1.8620678f, -1.0112547f, 0.14918678f }, { 0.38752654f, 0.62144744f, -0.00897398f }, { -0.0158415f, -0.03412294f, 1.0499644f } };
        WHITE_POINT_D65 = new float[] { 95.047f, 100.0f, 108.883f };
        SRGB_TO_XYZ = new float[][] { { 0.41233894f, 0.35762063f, 0.18051042f }, { 0.2126f, 0.7152f, 0.0722f }, { 0.01932141f, 0.11916382f, 0.9503448f } };
    }
    
    private CamUtils() {
    }
    
    static int intFromLStar(float n) {
        if (n < 1.0f) {
            return -16777216;
        }
        if (n > 99.0f) {
            return -1;
        }
        final float n2 = (n + 16.0f) / 116.0f;
        if (n > 8.0f) {
            n = n2 * n2 * n2;
        }
        else {
            n /= 903.2963f;
        }
        float n3 = n2 * n2 * n2;
        final boolean b = n3 > 0.008856452f;
        float n4;
        if (b) {
            n4 = n3;
        }
        else {
            n4 = (n2 * 116.0f - 16.0f) / 903.2963f;
        }
        if (!b) {
            n3 = (n2 * 116.0f - 16.0f) / 903.2963f;
        }
        final float[] white_POINT_D65 = CamUtils.WHITE_POINT_D65;
        return ColorUtils.XYZToColor(n4 * white_POINT_D65[0], n * white_POINT_D65[1], n3 * white_POINT_D65[2]);
    }
    
    static float lStarFromInt(final int n) {
        return lStarFromY(yFromInt(n));
    }
    
    static float lStarFromY(float n) {
        n /= 100.0f;
        if (n <= 0.008856452f) {
            return n * 903.2963f;
        }
        return (float)Math.cbrt(n) * 116.0f - 16.0f;
    }
    
    static float lerp(final float n, final float n2, final float n3) {
        return n + (n2 - n) * n3;
    }
    
    static float linearized(final int n) {
        final float n2 = n / 255.0f;
        float n3;
        if (n2 <= 0.04045f) {
            n3 = n2 / 12.92f;
        }
        else {
            n3 = (float)Math.pow((n2 + 0.055f) / 1.055f, 2.4000000953674316);
        }
        return n3 * 100.0f;
    }
    
    static void xyzFromInt(final int n, final float[] array) {
        final float linearized = linearized(Color.red(n));
        final float linearized2 = linearized(Color.green(n));
        final float linearized3 = linearized(Color.blue(n));
        final float[][] srgb_TO_XYZ = CamUtils.SRGB_TO_XYZ;
        final float[] array2 = srgb_TO_XYZ[0];
        array[0] = array2[0] * linearized + array2[1] * linearized2 + array2[2] * linearized3;
        final float[] array3 = srgb_TO_XYZ[1];
        array[1] = array3[0] * linearized + array3[1] * linearized2 + array3[2] * linearized3;
        final float[] array4 = srgb_TO_XYZ[2];
        array[2] = linearized * array4[0] + linearized2 * array4[1] + linearized3 * array4[2];
    }
    
    static float yFromInt(final int n) {
        final float linearized = linearized(Color.red(n));
        final float linearized2 = linearized(Color.green(n));
        final float linearized3 = linearized(Color.blue(n));
        final float[] array = CamUtils.SRGB_TO_XYZ[1];
        return linearized * array[0] + linearized2 * array[1] + linearized3 * array[2];
    }
    
    static float yFromLStar(float n) {
        if (n > 8.0f) {
            n = (float)Math.pow((n + 16.0) / 116.0, 3.0);
        }
        else {
            n /= 903.2963f;
        }
        return n * 100.0f;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.content;

import android.os.IBinder;
import android.content.ComponentName;
import android.content.Intent;
import android.os.RemoteException;
import android.util.Log;
import androidx.core.app.unusedapprestrictions.IUnusedAppRestrictionsBackportCallback;
import androidx.core.app.unusedapprestrictions.IUnusedAppRestrictionsBackportService;
import androidx.concurrent.futures.ResolvableFuture;
import android.content.Context;
import android.content.ServiceConnection;

class UnusedAppRestrictionsBackportServiceConnection implements ServiceConnection
{
    private final Context mContext;
    private boolean mHasBoundService;
    ResolvableFuture<Integer> mResultFuture;
    IUnusedAppRestrictionsBackportService mUnusedAppRestrictionsService;
    
    UnusedAppRestrictionsBackportServiceConnection(final Context mContext) {
        this.mUnusedAppRestrictionsService = null;
        this.mHasBoundService = false;
        this.mContext = mContext;
    }
    
    private IUnusedAppRestrictionsBackportCallback getBackportCallback() {
        return new IUnusedAppRestrictionsBackportCallback.Stub(this) {
            final UnusedAppRestrictionsBackportServiceConnection this$0;
            
            public void onIsPermissionRevocationEnabledForAppResult(final boolean b, final boolean b2) throws RemoteException {
                if (b) {
                    if (b2) {
                        this.this$0.mResultFuture.set(3);
                    }
                    else {
                        this.this$0.mResultFuture.set(2);
                    }
                }
                else {
                    this.this$0.mResultFuture.set(0);
                    Log.e("PackageManagerCompat", "Unable to retrieve the permission revocation setting from the backport");
                }
            }
        };
    }
    
    public void connectAndFetchResult(final ResolvableFuture<Integer> mResultFuture) {
        if (!this.mHasBoundService) {
            this.mHasBoundService = true;
            this.mResultFuture = mResultFuture;
            this.mContext.bindService(new Intent("android.support.unusedapprestrictions.action.CustomUnusedAppRestrictionsBackportService").setPackage(PackageManagerCompat.getPermissionRevocationVerifierApp(this.mContext.getPackageManager())), (ServiceConnection)this, 1);
            return;
        }
        throw new IllegalStateException("Each UnusedAppRestrictionsBackportServiceConnection can only be bound once.");
    }
    
    public void disconnectFromService() {
        if (this.mHasBoundService) {
            this.mHasBoundService = false;
            this.mContext.unbindService((ServiceConnection)this);
            return;
        }
        throw new IllegalStateException("bindService must be called before unbind");
    }
    
    public void onServiceConnected(final ComponentName componentName, final IBinder binder) {
        final IUnusedAppRestrictionsBackportService interface1 = IUnusedAppRestrictionsBackportService.Stub.asInterface(binder);
        this.mUnusedAppRestrictionsService = interface1;
        try {
            interface1.isPermissionRevocationEnabledForApp(this.getBackportCallback());
        }
        catch (final RemoteException ex) {
            this.mResultFuture.set(0);
        }
    }
    
    public void onServiceDisconnected(final ComponentName componentName) {
        this.mUnusedAppRestrictionsService = null;
    }
}

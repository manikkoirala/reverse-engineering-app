// 
// Decompiled by Procyon v0.6.0
// 

package androidx.concurrent.futures;

import java.util.concurrent.Executor;

public enum DirectExecutor implements Executor
{
    private static final DirectExecutor[] $VALUES;
    
    INSTANCE;
    
    @Override
    public void execute(final Runnable runnable) {
        runnable.run();
    }
    
    @Override
    public String toString() {
        return "DirectExecutor";
    }
}

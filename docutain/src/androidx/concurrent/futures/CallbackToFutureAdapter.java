// 
// Decompiled by Procyon v0.6.0
// 

package androidx.concurrent.futures;

import java.util.concurrent.TimeoutException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.ExecutionException;
import java.lang.ref.WeakReference;
import java.util.concurrent.Executor;
import com.google.common.util.concurrent.ListenableFuture;

public final class CallbackToFutureAdapter
{
    private CallbackToFutureAdapter() {
    }
    
    public static <T> ListenableFuture<T> getFuture(final Resolver<T> resolver) {
        final Completer completer = new Completer();
        final SafeFuture future = new SafeFuture((Completer<Object>)completer);
        completer.future = (SafeFuture<T>)future;
        completer.tag = resolver.getClass();
        try {
            final Object attachCompleter = resolver.attachCompleter((Completer<T>)completer);
            if (attachCompleter != null) {
                completer.tag = attachCompleter;
            }
        }
        catch (final Exception exception) {
            future.setException(exception);
        }
        return (ListenableFuture<T>)future;
    }
    
    public static final class Completer<T>
    {
        private boolean attemptedSetting;
        private ResolvableFuture<Void> cancellationFuture;
        SafeFuture<T> future;
        Object tag;
        
        Completer() {
            this.cancellationFuture = ResolvableFuture.create();
        }
        
        private void setCompletedNormally() {
            this.tag = null;
            this.future = null;
            this.cancellationFuture = null;
        }
        
        public void addCancellationListener(final Runnable runnable, final Executor executor) {
            final ResolvableFuture<Void> cancellationFuture = this.cancellationFuture;
            if (cancellationFuture != null) {
                ((ListenableFuture)cancellationFuture).addListener(runnable, executor);
            }
        }
        
        @Override
        protected void finalize() {
            final SafeFuture<T> future = this.future;
            if (future != null && !future.isDone()) {
                final StringBuilder sb = new StringBuilder();
                sb.append("The completer object was garbage collected - this future would otherwise never complete. The tag was: ");
                sb.append(this.tag);
                future.setException(new FutureGarbageCollectedException(sb.toString()));
            }
            if (!this.attemptedSetting) {
                final ResolvableFuture<Void> cancellationFuture = this.cancellationFuture;
                if (cancellationFuture != null) {
                    cancellationFuture.set(null);
                }
            }
        }
        
        void fireCancellationListeners() {
            this.tag = null;
            this.future = null;
            this.cancellationFuture.set(null);
        }
        
        public boolean set(final T t) {
            boolean b = true;
            this.attemptedSetting = true;
            final SafeFuture<T> future = this.future;
            if (future == null || !future.set(t)) {
                b = false;
            }
            if (b) {
                this.setCompletedNormally();
            }
            return b;
        }
        
        public boolean setCancelled() {
            boolean b = true;
            this.attemptedSetting = true;
            final SafeFuture<T> future = this.future;
            if (future == null || !future.cancelWithoutNotifyingCompleter(true)) {
                b = false;
            }
            if (b) {
                this.setCompletedNormally();
            }
            return b;
        }
        
        public boolean setException(final Throwable exception) {
            boolean b = true;
            this.attemptedSetting = true;
            final SafeFuture<T> future = this.future;
            if (future == null || !future.setException(exception)) {
                b = false;
            }
            if (b) {
                this.setCompletedNormally();
            }
            return b;
        }
    }
    
    static final class FutureGarbageCollectedException extends Throwable
    {
        FutureGarbageCollectedException(final String message) {
            super(message);
        }
        
        @Override
        public Throwable fillInStackTrace() {
            monitorenter(this);
            monitorexit(this);
            return this;
        }
    }
    
    public interface Resolver<T>
    {
        Object attachCompleter(final Completer<T> p0) throws Exception;
    }
    
    private static final class SafeFuture<T> implements ListenableFuture<T>
    {
        final WeakReference<Completer<T>> completerWeakReference;
        private final AbstractResolvableFuture<T> delegate;
        
        SafeFuture(final Completer<T> referent) {
            this.delegate = new AbstractResolvableFuture<T>() {
                final SafeFuture this$0;
                
                @Override
                protected String pendingToString() {
                    final Completer completer = (Completer)this.this$0.completerWeakReference.get();
                    if (completer == null) {
                        return "Completer object has been garbage collected, future will fail soon";
                    }
                    final StringBuilder sb = new StringBuilder();
                    sb.append("tag=[");
                    sb.append(completer.tag);
                    sb.append("]");
                    return sb.toString();
                }
            };
            this.completerWeakReference = new WeakReference<Completer<T>>(referent);
        }
        
        public void addListener(final Runnable runnable, final Executor executor) {
            this.delegate.addListener(runnable, executor);
        }
        
        public boolean cancel(final boolean b) {
            final Completer completer = (Completer)this.completerWeakReference.get();
            final boolean cancel = this.delegate.cancel(b);
            if (cancel && completer != null) {
                completer.fireCancellationListeners();
            }
            return cancel;
        }
        
        boolean cancelWithoutNotifyingCompleter(final boolean b) {
            return this.delegate.cancel(b);
        }
        
        public T get() throws InterruptedException, ExecutionException {
            return this.delegate.get();
        }
        
        public T get(final long n, final TimeUnit timeUnit) throws InterruptedException, ExecutionException, TimeoutException {
            return this.delegate.get(n, timeUnit);
        }
        
        public boolean isCancelled() {
            return this.delegate.isCancelled();
        }
        
        public boolean isDone() {
            return this.delegate.isDone();
        }
        
        boolean set(final T t) {
            return this.delegate.set(t);
        }
        
        boolean setException(final Throwable exception) {
            return this.delegate.setException(exception);
        }
        
        @Override
        public String toString() {
            return this.delegate.toString();
        }
    }
}

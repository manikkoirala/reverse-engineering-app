// 
// Decompiled by Procyon v0.6.0
// 

package androidx.concurrent.futures;

import com.google.common.util.concurrent.ListenableFuture;

public final class ResolvableFuture<V> extends AbstractResolvableFuture<V>
{
    private ResolvableFuture() {
    }
    
    public static <V> ResolvableFuture<V> create() {
        return new ResolvableFuture<V>();
    }
    
    public boolean set(final V v) {
        return super.set(v);
    }
    
    public boolean setException(final Throwable exception) {
        return super.setException(exception);
    }
    
    public boolean setFuture(final ListenableFuture<? extends V> future) {
        return super.setFuture(future);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.sqlite.db.framework;

import kotlin.jvm.internal.Intrinsics;
import android.database.sqlite.SQLiteProgram;
import kotlin.Metadata;
import androidx.sqlite.db.SupportSQLiteProgram;

@Metadata(d1 = { "\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0012\n\u0000\n\u0002\u0010\u0006\n\u0000\n\u0002\u0010\t\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\b\u0010\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u0018\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\nH\u0016J\u0018\u0010\u000b\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\fH\u0016J\u0018\u0010\r\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\u000eH\u0016J\u0010\u0010\u000f\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bH\u0016J\u0018\u0010\u0010\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\u0011H\u0016J\b\u0010\u0012\u001a\u00020\u0006H\u0016J\b\u0010\u0013\u001a\u00020\u0006H\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u0014" }, d2 = { "Landroidx/sqlite/db/framework/FrameworkSQLiteProgram;", "Landroidx/sqlite/db/SupportSQLiteProgram;", "delegate", "Landroid/database/sqlite/SQLiteProgram;", "(Landroid/database/sqlite/SQLiteProgram;)V", "bindBlob", "", "index", "", "value", "", "bindDouble", "", "bindLong", "", "bindNull", "bindString", "", "clearBindings", "close", "sqlite-framework_release" }, k = 1, mv = { 1, 7, 1 }, xi = 48)
public class FrameworkSQLiteProgram implements SupportSQLiteProgram
{
    private final SQLiteProgram delegate;
    
    public FrameworkSQLiteProgram(final SQLiteProgram delegate) {
        Intrinsics.checkNotNullParameter((Object)delegate, "delegate");
        this.delegate = delegate;
    }
    
    @Override
    public void bindBlob(final int n, final byte[] array) {
        Intrinsics.checkNotNullParameter((Object)array, "value");
        this.delegate.bindBlob(n, array);
    }
    
    @Override
    public void bindDouble(final int n, final double n2) {
        this.delegate.bindDouble(n, n2);
    }
    
    @Override
    public void bindLong(final int n, final long n2) {
        this.delegate.bindLong(n, n2);
    }
    
    @Override
    public void bindNull(final int n) {
        this.delegate.bindNull(n);
    }
    
    @Override
    public void bindString(final int n, final String s) {
        Intrinsics.checkNotNullParameter((Object)s, "value");
        this.delegate.bindString(n, s);
    }
    
    @Override
    public void clearBindings() {
        this.delegate.clearBindings();
    }
    
    @Override
    public void close() {
        this.delegate.close();
    }
}

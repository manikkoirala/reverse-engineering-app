// 
// Decompiled by Procyon v0.6.0
// 

package androidx.sqlite.db.framework;

import java.io.File;
import android.database.sqlite.SQLiteException;
import android.util.Log;
import java.util.UUID;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase$CursorFactory;
import android.database.sqlite.SQLiteDatabase;
import androidx.sqlite.util.ProcessLock;
import androidx.sqlite.db.SupportSQLiteCompat;
import android.database.sqlite.SQLiteOpenHelper;
import androidx.sqlite.db.SupportSQLiteDatabase;
import kotlin.LazyKt;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.Lazy;
import android.content.Context;
import kotlin.Metadata;
import androidx.sqlite.db.SupportSQLiteOpenHelper;

@Metadata(d1 = { "\u0000B\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u0002\n\u0002\b\u0006\b\u0000\u0018\u0000 \"2\u00020\u0001:\u0003\"#$B5\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\b\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\b\b\u0002\u0010\b\u001a\u00020\t\u0012\b\b\u0002\u0010\n\u001a\u00020\t¢\u0006\u0002\u0010\u000bJ\b\u0010\u001e\u001a\u00020\u001fH\u0016J\u0010\u0010 \u001a\u00020\u001f2\u0006\u0010!\u001a\u00020\tH\u0017R\u000e\u0010\n\u001a\u00020\tX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004¢\u0006\u0002\n\u0000R\u0016\u0010\f\u001a\u0004\u0018\u00010\u00058VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\r\u0010\u000eR\u001b\u0010\u000f\u001a\u00020\u00108BX\u0082\u0084\u0002¢\u0006\f\u001a\u0004\b\u0013\u0010\u0014*\u0004\b\u0011\u0010\u0012R\u0014\u0010\u0015\u001a\b\u0012\u0004\u0012\u00020\u00100\u0016X\u0082\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\u0004\u001a\u0004\u0018\u00010\u0005X\u0082\u0004¢\u0006\u0002\n\u0000R\u0014\u0010\u0017\u001a\u00020\u00188VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u0019\u0010\u001aR\u000e\u0010\b\u001a\u00020\tX\u0082\u0004¢\u0006\u0002\n\u0000R\u0014\u0010\u001b\u001a\u00020\u00188VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u001c\u0010\u001aR\u000e\u0010\u001d\u001a\u00020\tX\u0082\u000e¢\u0006\u0002\n\u0000¨\u0006%" }, d2 = { "Landroidx/sqlite/db/framework/FrameworkSQLiteOpenHelper;", "Landroidx/sqlite/db/SupportSQLiteOpenHelper;", "context", "Landroid/content/Context;", "name", "", "callback", "Landroidx/sqlite/db/SupportSQLiteOpenHelper$Callback;", "useNoBackupDirectory", "", "allowDataLossOnRecovery", "(Landroid/content/Context;Ljava/lang/String;Landroidx/sqlite/db/SupportSQLiteOpenHelper$Callback;ZZ)V", "databaseName", "getDatabaseName", "()Ljava/lang/String;", "delegate", "Landroidx/sqlite/db/framework/FrameworkSQLiteOpenHelper$OpenHelper;", "getDelegate$delegate", "(Landroidx/sqlite/db/framework/FrameworkSQLiteOpenHelper;)Ljava/lang/Object;", "getDelegate", "()Landroidx/sqlite/db/framework/FrameworkSQLiteOpenHelper$OpenHelper;", "lazyDelegate", "Lkotlin/Lazy;", "readableDatabase", "Landroidx/sqlite/db/SupportSQLiteDatabase;", "getReadableDatabase", "()Landroidx/sqlite/db/SupportSQLiteDatabase;", "writableDatabase", "getWritableDatabase", "writeAheadLoggingEnabled", "close", "", "setWriteAheadLoggingEnabled", "enabled", "Companion", "DBRefHolder", "OpenHelper", "sqlite-framework_release" }, k = 1, mv = { 1, 7, 1 }, xi = 48)
public final class FrameworkSQLiteOpenHelper implements SupportSQLiteOpenHelper
{
    public static final Companion Companion;
    private static final String TAG = "SupportSQLite";
    private final boolean allowDataLossOnRecovery;
    private final Callback callback;
    private final Context context;
    private final Lazy<OpenHelper> lazyDelegate;
    private final String name;
    private final boolean useNoBackupDirectory;
    private boolean writeAheadLoggingEnabled;
    
    static {
        Companion = new Companion(null);
    }
    
    public FrameworkSQLiteOpenHelper(final Context context, final String s, final Callback callback) {
        Intrinsics.checkNotNullParameter((Object)context, "context");
        Intrinsics.checkNotNullParameter((Object)callback, "callback");
        this(context, s, callback, false, false, 24, null);
    }
    
    public FrameworkSQLiteOpenHelper(final Context context, final String s, final Callback callback, final boolean b) {
        Intrinsics.checkNotNullParameter((Object)context, "context");
        Intrinsics.checkNotNullParameter((Object)callback, "callback");
        this(context, s, callback, b, false, 16, null);
    }
    
    public FrameworkSQLiteOpenHelper(final Context context, final String name, final Callback callback, final boolean useNoBackupDirectory, final boolean allowDataLossOnRecovery) {
        Intrinsics.checkNotNullParameter((Object)context, "context");
        Intrinsics.checkNotNullParameter((Object)callback, "callback");
        this.context = context;
        this.name = name;
        this.callback = callback;
        this.useNoBackupDirectory = useNoBackupDirectory;
        this.allowDataLossOnRecovery = allowDataLossOnRecovery;
        this.lazyDelegate = (Lazy<OpenHelper>)LazyKt.lazy((Function0)new FrameworkSQLiteOpenHelper$lazyDelegate.FrameworkSQLiteOpenHelper$lazyDelegate$1(this));
    }
    
    private final OpenHelper getDelegate() {
        return (OpenHelper)this.lazyDelegate.getValue();
    }
    
    private static Object getDelegate$delegate(final FrameworkSQLiteOpenHelper frameworkSQLiteOpenHelper) {
        return frameworkSQLiteOpenHelper.lazyDelegate;
    }
    
    @Override
    public void close() {
        if (this.lazyDelegate.isInitialized()) {
            this.getDelegate().close();
        }
    }
    
    @Override
    public String getDatabaseName() {
        return this.name;
    }
    
    @Override
    public SupportSQLiteDatabase getReadableDatabase() {
        return this.getDelegate().getSupportDatabase(false);
    }
    
    @Override
    public SupportSQLiteDatabase getWritableDatabase() {
        return this.getDelegate().getSupportDatabase(true);
    }
    
    @Override
    public void setWriteAheadLoggingEnabled(final boolean writeAheadLoggingEnabled) {
        if (this.lazyDelegate.isInitialized()) {
            SupportSQLiteCompat.Api16Impl.setWriteAheadLoggingEnabled(this.getDelegate(), writeAheadLoggingEnabled);
        }
        this.writeAheadLoggingEnabled = writeAheadLoggingEnabled;
    }
    
    @Metadata(d1 = { "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082T¢\u0006\u0002\n\u0000¨\u0006\u0005" }, d2 = { "Landroidx/sqlite/db/framework/FrameworkSQLiteOpenHelper$Companion;", "", "()V", "TAG", "", "sqlite-framework_release" }, k = 1, mv = { 1, 7, 1 }, xi = 48)
    public static final class Companion
    {
        private Companion() {
        }
    }
    
    @Metadata(d1 = { "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0002\u0018\u00002\u00020\u0001B\u000f\u0012\b\u0010\u0002\u001a\u0004\u0018\u00010\u0003¢\u0006\u0002\u0010\u0004R\u001c\u0010\u0002\u001a\u0004\u0018\u00010\u0003X\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\u0004¨\u0006\b" }, d2 = { "Landroidx/sqlite/db/framework/FrameworkSQLiteOpenHelper$DBRefHolder;", "", "db", "Landroidx/sqlite/db/framework/FrameworkSQLiteDatabase;", "(Landroidx/sqlite/db/framework/FrameworkSQLiteDatabase;)V", "getDb", "()Landroidx/sqlite/db/framework/FrameworkSQLiteDatabase;", "setDb", "sqlite-framework_release" }, k = 1, mv = { 1, 7, 1 }, xi = 48)
    private static final class DBRefHolder
    {
        private FrameworkSQLiteDatabase db;
        
        public DBRefHolder(final FrameworkSQLiteDatabase db) {
            this.db = db;
        }
        
        public final FrameworkSQLiteDatabase getDb() {
            return this.db;
        }
        
        public final void setDb(final FrameworkSQLiteDatabase db) {
            this.db = db;
        }
    }
    
    @Metadata(d1 = { "\u0000V\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\n\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\b\n\u0002\b\u0007\b\u0002\u0018\u0000 /2\u00020\u0001:\u0003-./B/\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\b\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b¢\u0006\u0002\u0010\fJ\b\u0010\u0019\u001a\u00020\u001aH\u0016J\u000e\u0010\u001b\u001a\u00020\u001c2\u0006\u0010\u001d\u001a\u00020\u000bJ\u000e\u0010\u001e\u001a\u00020\u001f2\u0006\u0010 \u001a\u00020!J\u0010\u0010\"\u001a\u00020!2\u0006\u0010\u001d\u001a\u00020\u000bH\u0002J\u0010\u0010#\u001a\u00020!2\u0006\u0010\u001d\u001a\u00020\u000bH\u0002J\u0010\u0010$\u001a\u00020\u001a2\u0006\u0010%\u001a\u00020!H\u0016J\u0010\u0010&\u001a\u00020\u001a2\u0006\u0010 \u001a\u00020!H\u0016J \u0010'\u001a\u00020\u001a2\u0006\u0010%\u001a\u00020!2\u0006\u0010(\u001a\u00020)2\u0006\u0010*\u001a\u00020)H\u0016J\u0010\u0010+\u001a\u00020\u001a2\u0006\u0010%\u001a\u00020!H\u0016J \u0010,\u001a\u00020\u001a2\u0006\u0010 \u001a\u00020!2\u0006\u0010(\u001a\u00020)2\u0006\u0010*\u001a\u00020)H\u0016R\u0011\u0010\n\u001a\u00020\u000b¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000eR\u0011\u0010\b\u001a\u00020\t¢\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u0010R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\u0012R\u0011\u0010\u0006\u001a\u00020\u0007¢\u0006\b\n\u0000\u001a\u0004\b\u0013\u0010\u0014R\u000e\u0010\u0015\u001a\u00020\u0016X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0017\u001a\u00020\u000bX\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0018\u001a\u00020\u000bX\u0082\u000e¢\u0006\u0002\n\u0000¨\u00060" }, d2 = { "Landroidx/sqlite/db/framework/FrameworkSQLiteOpenHelper$OpenHelper;", "Landroid/database/sqlite/SQLiteOpenHelper;", "context", "Landroid/content/Context;", "name", "", "dbRef", "Landroidx/sqlite/db/framework/FrameworkSQLiteOpenHelper$DBRefHolder;", "callback", "Landroidx/sqlite/db/SupportSQLiteOpenHelper$Callback;", "allowDataLossOnRecovery", "", "(Landroid/content/Context;Ljava/lang/String;Landroidx/sqlite/db/framework/FrameworkSQLiteOpenHelper$DBRefHolder;Landroidx/sqlite/db/SupportSQLiteOpenHelper$Callback;Z)V", "getAllowDataLossOnRecovery", "()Z", "getCallback", "()Landroidx/sqlite/db/SupportSQLiteOpenHelper$Callback;", "getContext", "()Landroid/content/Context;", "getDbRef", "()Landroidx/sqlite/db/framework/FrameworkSQLiteOpenHelper$DBRefHolder;", "lock", "Landroidx/sqlite/util/ProcessLock;", "migrated", "opened", "close", "", "getSupportDatabase", "Landroidx/sqlite/db/SupportSQLiteDatabase;", "writable", "getWrappedDb", "Landroidx/sqlite/db/framework/FrameworkSQLiteDatabase;", "sqLiteDatabase", "Landroid/database/sqlite/SQLiteDatabase;", "getWritableOrReadableDatabase", "innerGetDatabase", "onConfigure", "db", "onCreate", "onDowngrade", "oldVersion", "", "newVersion", "onOpen", "onUpgrade", "CallbackException", "CallbackName", "Companion", "sqlite-framework_release" }, k = 1, mv = { 1, 7, 1 }, xi = 48)
    private static final class OpenHelper extends SQLiteOpenHelper
    {
        public static final Companion Companion;
        private final boolean allowDataLossOnRecovery;
        private final Callback callback;
        private final Context context;
        private final DBRefHolder dbRef;
        private final ProcessLock lock;
        private boolean migrated;
        private boolean opened;
        
        static {
            Companion = new Companion(null);
        }
        
        public OpenHelper(final Context context, final String s, final DBRefHolder dbRef, final Callback callback, final boolean allowDataLossOnRecovery) {
            Intrinsics.checkNotNullParameter((Object)context, "context");
            Intrinsics.checkNotNullParameter((Object)dbRef, "dbRef");
            Intrinsics.checkNotNullParameter((Object)callback, "callback");
            super(context, s, (SQLiteDatabase$CursorFactory)null, callback.version, (DatabaseErrorHandler)new FrameworkSQLiteOpenHelper$OpenHelper$$ExternalSyntheticLambda0(callback, dbRef));
            this.context = context;
            this.dbRef = dbRef;
            this.callback = callback;
            this.allowDataLossOnRecovery = allowDataLossOnRecovery;
            String string = s;
            if (s == null) {
                string = UUID.randomUUID().toString();
                Intrinsics.checkNotNullExpressionValue((Object)string, "randomUUID().toString()");
            }
            this.lock = new ProcessLock(string, context.getCacheDir(), false);
        }
        
        private static final void _init_$lambda$0(final Callback callback, final DBRefHolder dbRefHolder, final SQLiteDatabase sqLiteDatabase) {
            Intrinsics.checkNotNullParameter((Object)callback, "$callback");
            Intrinsics.checkNotNullParameter((Object)dbRefHolder, "$dbRef");
            final Companion companion = OpenHelper.Companion;
            Intrinsics.checkNotNullExpressionValue((Object)sqLiteDatabase, "dbObj");
            callback.onCorruption(companion.getWrappedDb(dbRefHolder, sqLiteDatabase));
        }
        
        private final SQLiteDatabase getWritableOrReadableDatabase(final boolean b) {
            SQLiteDatabase sqLiteDatabase;
            if (b) {
                sqLiteDatabase = super.getWritableDatabase();
                Intrinsics.checkNotNullExpressionValue((Object)sqLiteDatabase, "{\n                super.\u2026eDatabase()\n            }");
            }
            else {
                sqLiteDatabase = super.getReadableDatabase();
                Intrinsics.checkNotNullExpressionValue((Object)sqLiteDatabase, "{\n                super.\u2026eDatabase()\n            }");
            }
            return sqLiteDatabase;
        }
        
        private final SQLiteDatabase innerGetDatabase(final boolean b) {
            final String databaseName = this.getDatabaseName();
            final boolean opened = this.opened;
            if (databaseName != null && !opened) {
                final File parentFile = this.context.getDatabasePath(databaseName).getParentFile();
                if (parentFile != null) {
                    parentFile.mkdirs();
                    if (!parentFile.isDirectory()) {
                        final StringBuilder sb = new StringBuilder();
                        sb.append("Invalid database parent file, not a directory: ");
                        sb.append(parentFile);
                        Log.w("SupportSQLite", sb.toString());
                    }
                }
            }
            try {
                return this.getWritableOrReadableDatabase(b);
            }
            finally {
                super.close();
                try {
                    Thread.sleep(500L);
                    try {
                        return this.getWritableOrReadableDatabase(b);
                    }
                    finally {
                        super.close();
                        final CallbackException ex;
                        if (ex instanceof CallbackException) {
                            final CallbackException ex2 = ex;
                            final Throwable cause = ex2.getCause();
                            final int n = WhenMappings.$EnumSwitchMapping$0[ex2.getCallbackName().ordinal()];
                            if (n == 1 || n == 2 || n == 3 || n == 4) {
                                throw cause;
                            }
                            if (!(cause instanceof SQLiteException)) {
                                throw cause;
                            }
                        }
                        else if (ex instanceof SQLiteException && (databaseName != null && this.allowDataLossOnRecovery)) {}
                        this.context.deleteDatabase(databaseName);
                        try {
                            return this.getWritableOrReadableDatabase(b);
                        }
                        catch (final CallbackException ex3) {
                            throw ex3.getCause();
                        }
                    }
                }
                catch (final InterruptedException ex4) {}
            }
        }
        
        public void close() {
            try {
                ProcessLock.lock$default(this.lock, false, 1, null);
                super.close();
                this.dbRef.setDb(null);
                this.opened = false;
            }
            finally {
                this.lock.unlock();
            }
        }
        
        public final boolean getAllowDataLossOnRecovery() {
            return this.allowDataLossOnRecovery;
        }
        
        public final Callback getCallback() {
            return this.callback;
        }
        
        public final Context getContext() {
            return this.context;
        }
        
        public final DBRefHolder getDbRef() {
            return this.dbRef;
        }
        
        public final SupportSQLiteDatabase getSupportDatabase(final boolean b) {
            try {
                this.lock.lock(!this.opened && this.getDatabaseName() != null);
                this.migrated = false;
                final SQLiteDatabase innerGetDatabase = this.innerGetDatabase(b);
                if (this.migrated) {
                    this.close();
                    return this.getSupportDatabase(b);
                }
                return this.getWrappedDb(innerGetDatabase);
            }
            finally {
                this.lock.unlock();
            }
        }
        
        public final FrameworkSQLiteDatabase getWrappedDb(final SQLiteDatabase sqLiteDatabase) {
            Intrinsics.checkNotNullParameter((Object)sqLiteDatabase, "sqLiteDatabase");
            return OpenHelper.Companion.getWrappedDb(this.dbRef, sqLiteDatabase);
        }
        
        public void onConfigure(final SQLiteDatabase sqLiteDatabase) {
            Intrinsics.checkNotNullParameter((Object)sqLiteDatabase, "db");
            if (!this.migrated && this.callback.version != sqLiteDatabase.getVersion()) {
                sqLiteDatabase.setMaxSqlCacheSize(1);
            }
            try {
                this.callback.onConfigure(this.getWrappedDb(sqLiteDatabase));
            }
            finally {
                final Throwable t;
                throw new CallbackException(CallbackName.ON_CONFIGURE, t);
            }
        }
        
        public void onCreate(final SQLiteDatabase sqLiteDatabase) {
            Intrinsics.checkNotNullParameter((Object)sqLiteDatabase, "sqLiteDatabase");
            try {
                this.callback.onCreate(this.getWrappedDb(sqLiteDatabase));
            }
            finally {
                final Throwable t;
                throw new CallbackException(CallbackName.ON_CREATE, t);
            }
        }
        
        public void onDowngrade(final SQLiteDatabase sqLiteDatabase, final int n, final int n2) {
            Intrinsics.checkNotNullParameter((Object)sqLiteDatabase, "db");
            this.migrated = true;
            try {
                this.callback.onDowngrade(this.getWrappedDb(sqLiteDatabase), n, n2);
            }
            finally {
                final Throwable t;
                throw new CallbackException(CallbackName.ON_DOWNGRADE, t);
            }
        }
        
        public void onOpen(final SQLiteDatabase sqLiteDatabase) {
            Intrinsics.checkNotNullParameter((Object)sqLiteDatabase, "db");
            if (!this.migrated) {
                try {
                    this.callback.onOpen(this.getWrappedDb(sqLiteDatabase));
                }
                finally {
                    final Throwable t;
                    throw new CallbackException(CallbackName.ON_OPEN, t);
                }
            }
            this.opened = true;
        }
        
        public void onUpgrade(final SQLiteDatabase sqLiteDatabase, final int n, final int n2) {
            Intrinsics.checkNotNullParameter((Object)sqLiteDatabase, "sqLiteDatabase");
            this.migrated = true;
            try {
                this.callback.onUpgrade(this.getWrappedDb(sqLiteDatabase), n, n2);
            }
            finally {
                final Throwable t;
                throw new CallbackException(CallbackName.ON_UPGRADE, t);
            }
        }
        
        @Metadata(d1 = { "\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0003\n\u0002\b\u0006\b\u0002\u0018\u00002\u00060\u0001j\u0002`\u0002B\u0015\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006¢\u0006\u0002\u0010\u0007R\u0011\u0010\u0003\u001a\u00020\u0004¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010\tR\u0014\u0010\u0005\u001a\u00020\u0006X\u0096\u0004¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000b¨\u0006\f" }, d2 = { "Landroidx/sqlite/db/framework/FrameworkSQLiteOpenHelper$OpenHelper$CallbackException;", "Ljava/lang/RuntimeException;", "Lkotlin/RuntimeException;", "callbackName", "Landroidx/sqlite/db/framework/FrameworkSQLiteOpenHelper$OpenHelper$CallbackName;", "cause", "", "(Landroidx/sqlite/db/framework/FrameworkSQLiteOpenHelper$OpenHelper$CallbackName;Ljava/lang/Throwable;)V", "getCallbackName", "()Landroidx/sqlite/db/framework/FrameworkSQLiteOpenHelper$OpenHelper$CallbackName;", "getCause", "()Ljava/lang/Throwable;", "sqlite-framework_release" }, k = 1, mv = { 1, 7, 1 }, xi = 48)
        private static final class CallbackException extends RuntimeException
        {
            private final CallbackName callbackName;
            private final Throwable cause;
            
            public CallbackException(final CallbackName callbackName, final Throwable t) {
                Intrinsics.checkNotNullParameter((Object)callbackName, "callbackName");
                Intrinsics.checkNotNullParameter((Object)t, "cause");
                super(t);
                this.callbackName = callbackName;
                this.cause = t;
            }
            
            public final CallbackName getCallbackName() {
                return this.callbackName;
            }
            
            @Override
            public Throwable getCause() {
                return this.cause;
            }
        }
        
        @Metadata(d1 = { "\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0007\b\u0080\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002j\u0002\b\u0003j\u0002\b\u0004j\u0002\b\u0005j\u0002\b\u0006j\u0002\b\u0007¨\u0006\b" }, d2 = { "Landroidx/sqlite/db/framework/FrameworkSQLiteOpenHelper$OpenHelper$CallbackName;", "", "(Ljava/lang/String;I)V", "ON_CONFIGURE", "ON_CREATE", "ON_UPGRADE", "ON_DOWNGRADE", "ON_OPEN", "sqlite-framework_release" }, k = 1, mv = { 1, 7, 1 }, xi = 48)
        public enum CallbackName
        {
            private static final CallbackName[] $VALUES;
            
            ON_CONFIGURE, 
            ON_CREATE, 
            ON_DOWNGRADE, 
            ON_OPEN, 
            ON_UPGRADE;
            
            private static final /* synthetic */ CallbackName[] $values() {
                return new CallbackName[] { CallbackName.ON_CONFIGURE, CallbackName.ON_CREATE, CallbackName.ON_UPGRADE, CallbackName.ON_DOWNGRADE, CallbackName.ON_OPEN };
            }
            
            static {
                $VALUES = $values();
            }
        }
        
        @Metadata(d1 = { "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0016\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b¨\u0006\t" }, d2 = { "Landroidx/sqlite/db/framework/FrameworkSQLiteOpenHelper$OpenHelper$Companion;", "", "()V", "getWrappedDb", "Landroidx/sqlite/db/framework/FrameworkSQLiteDatabase;", "refHolder", "Landroidx/sqlite/db/framework/FrameworkSQLiteOpenHelper$DBRefHolder;", "sqLiteDatabase", "Landroid/database/sqlite/SQLiteDatabase;", "sqlite-framework_release" }, k = 1, mv = { 1, 7, 1 }, xi = 48)
        public static final class Companion
        {
            private Companion() {
            }
            
            public final FrameworkSQLiteDatabase getWrappedDb(final DBRefHolder dbRefHolder, final SQLiteDatabase sqLiteDatabase) {
                Intrinsics.checkNotNullParameter((Object)dbRefHolder, "refHolder");
                Intrinsics.checkNotNullParameter((Object)sqLiteDatabase, "sqLiteDatabase");
                final FrameworkSQLiteDatabase db = dbRefHolder.getDb();
                if (db != null) {
                    final FrameworkSQLiteDatabase db2 = db;
                    if (db.isDelegate(sqLiteDatabase)) {
                        return db2;
                    }
                }
                final FrameworkSQLiteDatabase db2 = new FrameworkSQLiteDatabase(sqLiteDatabase);
                dbRefHolder.setDb(db2);
                return db2;
            }
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.sqlite.db.framework;

import android.database.sqlite.SQLiteProgram;
import kotlin.jvm.internal.Intrinsics;
import android.database.sqlite.SQLiteStatement;
import kotlin.Metadata;
import androidx.sqlite.db.SupportSQLiteStatement;

@Metadata(d1 = { "\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\b\u0000\u0018\u00002\u00020\u00012\u00020\u0002B\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004¢\u0006\u0002\u0010\u0005J\b\u0010\u0006\u001a\u00020\u0007H\u0016J\b\u0010\b\u001a\u00020\tH\u0016J\b\u0010\n\u001a\u00020\u000bH\u0016J\b\u0010\f\u001a\u00020\tH\u0016J\n\u0010\r\u001a\u0004\u0018\u00010\u000eH\u0016R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u000f" }, d2 = { "Landroidx/sqlite/db/framework/FrameworkSQLiteStatement;", "Landroidx/sqlite/db/framework/FrameworkSQLiteProgram;", "Landroidx/sqlite/db/SupportSQLiteStatement;", "delegate", "Landroid/database/sqlite/SQLiteStatement;", "(Landroid/database/sqlite/SQLiteStatement;)V", "execute", "", "executeInsert", "", "executeUpdateDelete", "", "simpleQueryForLong", "simpleQueryForString", "", "sqlite-framework_release" }, k = 1, mv = { 1, 7, 1 }, xi = 48)
public final class FrameworkSQLiteStatement extends FrameworkSQLiteProgram implements SupportSQLiteStatement
{
    private final SQLiteStatement delegate;
    
    public FrameworkSQLiteStatement(final SQLiteStatement delegate) {
        Intrinsics.checkNotNullParameter((Object)delegate, "delegate");
        super((SQLiteProgram)delegate);
        this.delegate = delegate;
    }
    
    @Override
    public void execute() {
        this.delegate.execute();
    }
    
    @Override
    public long executeInsert() {
        return this.delegate.executeInsert();
    }
    
    @Override
    public int executeUpdateDelete() {
        return this.delegate.executeUpdateDelete();
    }
    
    @Override
    public long simpleQueryForLong() {
        return this.delegate.simpleQueryForLong();
    }
    
    @Override
    public String simpleQueryForString() {
        return this.delegate.simpleQueryForString();
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.sqlite.db.framework;

import kotlin.jvm.internal.Intrinsics;
import kotlin.Metadata;
import androidx.sqlite.db.SupportSQLiteOpenHelper;

@Metadata(d1 = { "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0016¨\u0006\u0007" }, d2 = { "Landroidx/sqlite/db/framework/FrameworkSQLiteOpenHelperFactory;", "Landroidx/sqlite/db/SupportSQLiteOpenHelper$Factory;", "()V", "create", "Landroidx/sqlite/db/SupportSQLiteOpenHelper;", "configuration", "Landroidx/sqlite/db/SupportSQLiteOpenHelper$Configuration;", "sqlite-framework_release" }, k = 1, mv = { 1, 7, 1 }, xi = 48)
public final class FrameworkSQLiteOpenHelperFactory implements Factory
{
    @Override
    public SupportSQLiteOpenHelper create(final Configuration configuration) {
        Intrinsics.checkNotNullParameter((Object)configuration, "configuration");
        return new FrameworkSQLiteOpenHelper(configuration.context, configuration.name, configuration.callback, configuration.useNoBackupDirectory, configuration.allowDataLossOnRecovery);
    }
}

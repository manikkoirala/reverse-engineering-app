// 
// Decompiled by Procyon v0.6.0
// 

package androidx.sqlite.db.framework;

import java.util.Iterator;
import android.text.TextUtils;
import java.util.Locale;
import android.os.CancellationSignal;
import android.database.sqlite.SQLiteDatabase$CursorFactory;
import android.content.ContentValues;
import android.util.Pair;
import java.util.List;
import android.database.SQLException;
import android.os.Build$VERSION;
import androidx.sqlite.db.SupportSQLiteCompat;
import androidx.sqlite.db.SimpleSQLiteQuery;
import android.database.sqlite.SQLiteStatement;
import androidx.sqlite.db.SupportSQLiteStatement;
import java.io.IOException;
import android.database.sqlite.SQLiteTransactionListener;
import android.database.sqlite.SQLiteCursor;
import android.database.sqlite.SQLiteProgram;
import androidx.sqlite.db.SupportSQLiteProgram;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.functions.Function4;
import android.database.Cursor;
import android.database.sqlite.SQLiteQuery;
import android.database.sqlite.SQLiteCursorDriver;
import androidx.sqlite.db.SupportSQLiteQuery;
import android.database.sqlite.SQLiteDatabase;
import kotlin.Metadata;
import androidx.sqlite.db.SupportSQLiteDatabase;

@Metadata(d1 = { "\u0000\u0080\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0007\n\u0002\u0010\t\n\u0002\b\f\n\u0002\u0010\b\n\u0002\b\u0006\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0011\n\u0002\u0010\u0000\n\u0002\b\f\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\n\b\u0000\u0018\u0000 \\2\u00020\u0001:\u0002[\\B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\b\u0010'\u001a\u00020(H\u0016J\b\u0010)\u001a\u00020(H\u0016J\u0010\u0010*\u001a\u00020(2\u0006\u0010+\u001a\u00020,H\u0016J\u0010\u0010-\u001a\u00020(2\u0006\u0010+\u001a\u00020,H\u0016J\b\u0010.\u001a\u00020(H\u0016J\u0010\u0010/\u001a\u0002002\u0006\u00101\u001a\u00020\bH\u0016J3\u00102\u001a\u00020!2\u0006\u00103\u001a\u00020\b2\b\u00104\u001a\u0004\u0018\u00010\b2\u0012\u00105\u001a\u000e\u0012\b\b\u0001\u0012\u0004\u0018\u000107\u0018\u000106H\u0016¢\u0006\u0002\u00108J\b\u00109\u001a\u00020(H\u0017J\b\u0010:\u001a\u00020\fH\u0016J\b\u0010;\u001a\u00020(H\u0016J)\u0010<\u001a\u00020(2\u0006\u00101\u001a\u00020\b2\u0012\u0010=\u001a\u000e\u0012\b\b\u0001\u0012\u0004\u0018\u000107\u0018\u000106H\u0016¢\u0006\u0002\u0010>J\u0010\u0010?\u001a\u00020(2\u0006\u00101\u001a\u00020\bH\u0016J'\u0010?\u001a\u00020(2\u0006\u00101\u001a\u00020\b2\u0010\u0010=\u001a\f\u0012\b\b\u0001\u0012\u0004\u0018\u00010706H\u0016¢\u0006\u0002\u0010>J\b\u0010@\u001a\u00020\fH\u0016J \u0010A\u001a\u00020\u00142\u0006\u00103\u001a\u00020\b2\u0006\u0010B\u001a\u00020!2\u0006\u0010C\u001a\u00020DH\u0016J\u000e\u0010E\u001a\u00020\f2\u0006\u0010F\u001a\u00020\u0003J\u0010\u0010G\u001a\u00020\f2\u0006\u0010H\u001a\u00020!H\u0016J\u0010\u0010I\u001a\u00020J2\u0006\u0010I\u001a\u00020KH\u0016J\u001a\u0010I\u001a\u00020J2\u0006\u0010I\u001a\u00020K2\b\u0010L\u001a\u0004\u0018\u00010MH\u0017J\u0010\u0010I\u001a\u00020J2\u0006\u0010I\u001a\u00020\bH\u0016J'\u0010I\u001a\u00020J2\u0006\u0010I\u001a\u00020\b2\u0010\u0010=\u001a\f\u0012\b\b\u0001\u0012\u0004\u0018\u00010706H\u0016¢\u0006\u0002\u0010NJ\u0010\u0010O\u001a\u00020(2\u0006\u0010P\u001a\u00020\fH\u0017J\u0010\u0010Q\u001a\u00020(2\u0006\u0010R\u001a\u00020SH\u0016J\u0010\u0010T\u001a\u00020(2\u0006\u0010U\u001a\u00020!H\u0016J\u0010\u0010\u0018\u001a\u00020\u00142\u0006\u0010\u0013\u001a\u00020\u0014H\u0016J\b\u0010V\u001a\u00020(H\u0016JC\u0010W\u001a\u00020!2\u0006\u00103\u001a\u00020\b2\u0006\u0010B\u001a\u00020!2\u0006\u0010C\u001a\u00020D2\b\u00104\u001a\u0004\u0018\u00010\b2\u0012\u00105\u001a\u000e\u0012\b\b\u0001\u0012\u0004\u0018\u000107\u0018\u000106H\u0016¢\u0006\u0002\u0010XJ\b\u0010Y\u001a\u00020\fH\u0016J\u0010\u0010Y\u001a\u00020\f2\u0006\u0010Z\u001a\u00020\u0014H\u0016R(\u0010\u0005\u001a\u0016\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\b\u0012\u0004\u0012\u00020\b0\u0007\u0018\u00010\u00068VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\t\u0010\nR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004¢\u0006\u0002\n\u0000R\u0014\u0010\u000b\u001a\u00020\f8VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u000b\u0010\rR\u0014\u0010\u000e\u001a\u00020\f8VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u000e\u0010\rR\u0014\u0010\u000f\u001a\u00020\f8VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u000f\u0010\rR\u0014\u0010\u0010\u001a\u00020\f8VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u0010\u0010\rR\u0014\u0010\u0011\u001a\u00020\f8VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u0011\u0010\rR\u0014\u0010\u0012\u001a\u00020\f8WX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u0012\u0010\rR$\u0010\u0015\u001a\u00020\u00142\u0006\u0010\u0013\u001a\u00020\u00148V@VX\u0096\u000e¢\u0006\f\u001a\u0004\b\u0016\u0010\u0017\"\u0004\b\u0018\u0010\u0019R$\u0010\u001a\u001a\u00020\u00142\u0006\u0010\u0013\u001a\u00020\u00148V@VX\u0096\u000e¢\u0006\f\u001a\u0004\b\u001b\u0010\u0017\"\u0004\b\u001c\u0010\u0019R\u0016\u0010\u001d\u001a\u0004\u0018\u00010\b8VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u001e\u0010\u001fR$\u0010\"\u001a\u00020!2\u0006\u0010 \u001a\u00020!8V@VX\u0096\u000e¢\u0006\f\u001a\u0004\b#\u0010$\"\u0004\b%\u0010&¨\u0006]" }, d2 = { "Landroidx/sqlite/db/framework/FrameworkSQLiteDatabase;", "Landroidx/sqlite/db/SupportSQLiteDatabase;", "delegate", "Landroid/database/sqlite/SQLiteDatabase;", "(Landroid/database/sqlite/SQLiteDatabase;)V", "attachedDbs", "", "Landroid/util/Pair;", "", "getAttachedDbs", "()Ljava/util/List;", "isDatabaseIntegrityOk", "", "()Z", "isDbLockedByCurrentThread", "isExecPerConnectionSQLSupported", "isOpen", "isReadOnly", "isWriteAheadLoggingEnabled", "numBytes", "", "maximumSize", "getMaximumSize", "()J", "setMaximumSize", "(J)V", "pageSize", "getPageSize", "setPageSize", "path", "getPath", "()Ljava/lang/String;", "value", "", "version", "getVersion", "()I", "setVersion", "(I)V", "beginTransaction", "", "beginTransactionNonExclusive", "beginTransactionWithListener", "transactionListener", "Landroid/database/sqlite/SQLiteTransactionListener;", "beginTransactionWithListenerNonExclusive", "close", "compileStatement", "Landroidx/sqlite/db/SupportSQLiteStatement;", "sql", "delete", "table", "whereClause", "whereArgs", "", "", "(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I", "disableWriteAheadLogging", "enableWriteAheadLogging", "endTransaction", "execPerConnectionSQL", "bindArgs", "(Ljava/lang/String;[Ljava/lang/Object;)V", "execSQL", "inTransaction", "insert", "conflictAlgorithm", "values", "Landroid/content/ContentValues;", "isDelegate", "sqLiteDatabase", "needUpgrade", "newVersion", "query", "Landroid/database/Cursor;", "Landroidx/sqlite/db/SupportSQLiteQuery;", "cancellationSignal", "Landroid/os/CancellationSignal;", "(Ljava/lang/String;[Ljava/lang/Object;)Landroid/database/Cursor;", "setForeignKeyConstraintsEnabled", "enabled", "setLocale", "locale", "Ljava/util/Locale;", "setMaxSqlCacheSize", "cacheSize", "setTransactionSuccessful", "update", "(Ljava/lang/String;ILandroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/Object;)I", "yieldIfContendedSafely", "sleepAfterYieldDelayMillis", "Api30Impl", "Companion", "sqlite-framework_release" }, k = 1, mv = { 1, 7, 1 }, xi = 48)
public final class FrameworkSQLiteDatabase implements SupportSQLiteDatabase
{
    private static final String[] CONFLICT_VALUES;
    public static final Companion Companion;
    private static final String[] EMPTY_STRING_ARRAY;
    private final SQLiteDatabase delegate;
    
    static {
        Companion = new Companion(null);
        CONFLICT_VALUES = new String[] { "", " OR ROLLBACK ", " OR ABORT ", " OR FAIL ", " OR IGNORE ", " OR REPLACE " };
        EMPTY_STRING_ARRAY = new String[0];
    }
    
    public FrameworkSQLiteDatabase(final SQLiteDatabase delegate) {
        Intrinsics.checkNotNullParameter((Object)delegate, "delegate");
        this.delegate = delegate;
    }
    
    private static final Cursor query$lambda$0(final Function4 function4, final SQLiteDatabase sqLiteDatabase, final SQLiteCursorDriver sqLiteCursorDriver, final String s, final SQLiteQuery sqLiteQuery) {
        Intrinsics.checkNotNullParameter((Object)function4, "$tmp0");
        return (Cursor)function4.invoke((Object)sqLiteDatabase, (Object)sqLiteCursorDriver, (Object)s, (Object)sqLiteQuery);
    }
    
    private static final Cursor query$lambda$1(final SupportSQLiteQuery supportSQLiteQuery, final SQLiteDatabase sqLiteDatabase, final SQLiteCursorDriver sqLiteCursorDriver, final String s, final SQLiteQuery sqLiteQuery) {
        Intrinsics.checkNotNullParameter((Object)supportSQLiteQuery, "$query");
        Intrinsics.checkNotNull((Object)sqLiteQuery);
        supportSQLiteQuery.bindTo(new FrameworkSQLiteProgram((SQLiteProgram)sqLiteQuery));
        return (Cursor)new SQLiteCursor(sqLiteCursorDriver, s, sqLiteQuery);
    }
    
    @Override
    public void beginTransaction() {
        this.delegate.beginTransaction();
    }
    
    @Override
    public void beginTransactionNonExclusive() {
        this.delegate.beginTransactionNonExclusive();
    }
    
    @Override
    public void beginTransactionWithListener(final SQLiteTransactionListener sqLiteTransactionListener) {
        Intrinsics.checkNotNullParameter((Object)sqLiteTransactionListener, "transactionListener");
        this.delegate.beginTransactionWithListener(sqLiteTransactionListener);
    }
    
    @Override
    public void beginTransactionWithListenerNonExclusive(final SQLiteTransactionListener sqLiteTransactionListener) {
        Intrinsics.checkNotNullParameter((Object)sqLiteTransactionListener, "transactionListener");
        this.delegate.beginTransactionWithListenerNonExclusive(sqLiteTransactionListener);
    }
    
    @Override
    public void close() throws IOException {
        this.delegate.close();
    }
    
    @Override
    public SupportSQLiteStatement compileStatement(final String s) {
        Intrinsics.checkNotNullParameter((Object)s, "sql");
        final SQLiteStatement compileStatement = this.delegate.compileStatement(s);
        Intrinsics.checkNotNullExpressionValue((Object)compileStatement, "delegate.compileStatement(sql)");
        return new FrameworkSQLiteStatement(compileStatement);
    }
    
    @Override
    public int delete(String string, final String str, final Object[] array) {
        Intrinsics.checkNotNullParameter((Object)string, "table");
        final StringBuilder sb = new StringBuilder();
        sb.append("DELETE FROM ");
        sb.append(string);
        final CharSequence charSequence = str;
        if (charSequence != null && charSequence.length() != 0) {
            sb.append(" WHERE ");
            sb.append(str);
        }
        string = sb.toString();
        Intrinsics.checkNotNullExpressionValue((Object)string, "StringBuilder().apply(builderAction).toString()");
        final SupportSQLiteStatement compileStatement = this.compileStatement(string);
        SimpleSQLiteQuery.Companion.bind(compileStatement, array);
        return compileStatement.executeUpdateDelete();
    }
    
    @Override
    public void disableWriteAheadLogging() {
        SupportSQLiteCompat.Api16Impl.disableWriteAheadLogging(this.delegate);
    }
    
    @Override
    public boolean enableWriteAheadLogging() {
        return this.delegate.enableWriteAheadLogging();
    }
    
    @Override
    public void endTransaction() {
        this.delegate.endTransaction();
    }
    
    @Override
    public void execPerConnectionSQL(final String s, final Object[] array) {
        Intrinsics.checkNotNullParameter((Object)s, "sql");
        if (Build$VERSION.SDK_INT >= 30) {
            Api30Impl.INSTANCE.execPerConnectionSQL(this.delegate, s, array);
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("execPerConnectionSQL is not supported on a SDK version lower than 30, current version is: ");
        sb.append(Build$VERSION.SDK_INT);
        throw new UnsupportedOperationException(sb.toString());
    }
    
    @Override
    public void execSQL(final String s) throws SQLException {
        Intrinsics.checkNotNullParameter((Object)s, "sql");
        this.delegate.execSQL(s);
    }
    
    @Override
    public void execSQL(final String s, final Object[] array) throws SQLException {
        Intrinsics.checkNotNullParameter((Object)s, "sql");
        Intrinsics.checkNotNullParameter((Object)array, "bindArgs");
        this.delegate.execSQL(s, array);
    }
    
    @Override
    public List<Pair<String, String>> getAttachedDbs() {
        return this.delegate.getAttachedDbs();
    }
    
    @Override
    public long getMaximumSize() {
        return this.delegate.getMaximumSize();
    }
    
    @Override
    public long getPageSize() {
        return this.delegate.getPageSize();
    }
    
    @Override
    public String getPath() {
        return this.delegate.getPath();
    }
    
    @Override
    public int getVersion() {
        return this.delegate.getVersion();
    }
    
    @Override
    public boolean inTransaction() {
        return this.delegate.inTransaction();
    }
    
    @Override
    public long insert(final String s, final int n, final ContentValues contentValues) throws SQLException {
        Intrinsics.checkNotNullParameter((Object)s, "table");
        Intrinsics.checkNotNullParameter((Object)contentValues, "values");
        return this.delegate.insertWithOnConflict(s, (String)null, contentValues, n);
    }
    
    @Override
    public boolean isDatabaseIntegrityOk() {
        return this.delegate.isDatabaseIntegrityOk();
    }
    
    @Override
    public boolean isDbLockedByCurrentThread() {
        return this.delegate.isDbLockedByCurrentThread();
    }
    
    public final boolean isDelegate(final SQLiteDatabase sqLiteDatabase) {
        Intrinsics.checkNotNullParameter((Object)sqLiteDatabase, "sqLiteDatabase");
        return Intrinsics.areEqual((Object)this.delegate, (Object)sqLiteDatabase);
    }
    
    @Override
    public boolean isExecPerConnectionSQLSupported() {
        return Build$VERSION.SDK_INT >= 30;
    }
    
    @Override
    public boolean isOpen() {
        return this.delegate.isOpen();
    }
    
    @Override
    public boolean isReadOnly() {
        return this.delegate.isReadOnly();
    }
    
    @Override
    public boolean isWriteAheadLoggingEnabled() {
        return SupportSQLiteCompat.Api16Impl.isWriteAheadLoggingEnabled(this.delegate);
    }
    
    @Override
    public boolean needUpgrade(final int n) {
        return this.delegate.needUpgrade(n);
    }
    
    @Override
    public Cursor query(final SupportSQLiteQuery supportSQLiteQuery) {
        Intrinsics.checkNotNullParameter((Object)supportSQLiteQuery, "query");
        final Cursor rawQueryWithFactory = this.delegate.rawQueryWithFactory((SQLiteDatabase$CursorFactory)new FrameworkSQLiteDatabase$$ExternalSyntheticLambda1((Function4)new FrameworkSQLiteDatabase$query$cursorFactory.FrameworkSQLiteDatabase$query$cursorFactory$1(supportSQLiteQuery)), supportSQLiteQuery.getSql(), FrameworkSQLiteDatabase.EMPTY_STRING_ARRAY, (String)null);
        Intrinsics.checkNotNullExpressionValue((Object)rawQueryWithFactory, "delegate.rawQueryWithFac\u2026EMPTY_STRING_ARRAY, null)");
        return rawQueryWithFactory;
    }
    
    @Override
    public Cursor query(final SupportSQLiteQuery supportSQLiteQuery, final CancellationSignal cancellationSignal) {
        Intrinsics.checkNotNullParameter((Object)supportSQLiteQuery, "query");
        final SQLiteDatabase delegate = this.delegate;
        final String sql = supportSQLiteQuery.getSql();
        final String[] empty_STRING_ARRAY = FrameworkSQLiteDatabase.EMPTY_STRING_ARRAY;
        Intrinsics.checkNotNull((Object)cancellationSignal);
        return SupportSQLiteCompat.Api16Impl.rawQueryWithFactory(delegate, sql, empty_STRING_ARRAY, null, cancellationSignal, (SQLiteDatabase$CursorFactory)new FrameworkSQLiteDatabase$$ExternalSyntheticLambda0(supportSQLiteQuery));
    }
    
    @Override
    public Cursor query(final String s) {
        Intrinsics.checkNotNullParameter((Object)s, "query");
        return this.query(new SimpleSQLiteQuery(s));
    }
    
    @Override
    public Cursor query(final String s, final Object[] array) {
        Intrinsics.checkNotNullParameter((Object)s, "query");
        Intrinsics.checkNotNullParameter((Object)array, "bindArgs");
        return this.query(new SimpleSQLiteQuery(s, array));
    }
    
    @Override
    public void setForeignKeyConstraintsEnabled(final boolean b) {
        SupportSQLiteCompat.Api16Impl.setForeignKeyConstraintsEnabled(this.delegate, b);
    }
    
    @Override
    public void setLocale(final Locale locale) {
        Intrinsics.checkNotNullParameter((Object)locale, "locale");
        this.delegate.setLocale(locale);
    }
    
    @Override
    public void setMaxSqlCacheSize(final int maxSqlCacheSize) {
        this.delegate.setMaxSqlCacheSize(maxSqlCacheSize);
    }
    
    @Override
    public long setMaximumSize(final long maximumSize) {
        this.delegate.setMaximumSize(maximumSize);
        return this.delegate.getMaximumSize();
    }
    
    public void setMaximumSize(final long maximumSize) {
        this.delegate.setMaximumSize(maximumSize);
    }
    
    @Override
    public void setPageSize(final long pageSize) {
        this.delegate.setPageSize(pageSize);
    }
    
    @Override
    public void setTransactionSuccessful() {
        this.delegate.setTransactionSuccessful();
    }
    
    @Override
    public void setVersion(final int version) {
        this.delegate.setVersion(version);
    }
    
    @Override
    public int update(String string, int i, final ContentValues contentValues, final String str, final Object[] array) {
        Intrinsics.checkNotNullParameter((Object)string, "table");
        Intrinsics.checkNotNullParameter((Object)contentValues, "values");
        final int size = contentValues.size();
        final int n = 0;
        if (size != 0) {
            final int size2 = contentValues.size();
            int n2;
            if (array == null) {
                n2 = size2;
            }
            else {
                n2 = array.length + size2;
            }
            final Object[] array2 = new Object[n2];
            final StringBuilder sb = new StringBuilder();
            sb.append("UPDATE ");
            sb.append(FrameworkSQLiteDatabase.CONFLICT_VALUES[i]);
            sb.append(string);
            sb.append(" SET ");
            final Iterator iterator = contentValues.keySet().iterator();
            i = n;
            while (iterator.hasNext()) {
                final String str2 = (String)iterator.next();
                if (i > 0) {
                    string = ",";
                }
                else {
                    string = "";
                }
                sb.append(string);
                sb.append(str2);
                array2[i] = contentValues.get(str2);
                sb.append("=?");
                ++i;
            }
            if (array != null) {
                for (i = size2; i < n2; ++i) {
                    array2[i] = array[i - size2];
                }
            }
            if (!TextUtils.isEmpty((CharSequence)str)) {
                sb.append(" WHERE ");
                sb.append(str);
            }
            string = sb.toString();
            Intrinsics.checkNotNullExpressionValue((Object)string, "StringBuilder().apply(builderAction).toString()");
            final SupportSQLiteStatement compileStatement = this.compileStatement(string);
            SimpleSQLiteQuery.Companion.bind(compileStatement, array2);
            return compileStatement.executeUpdateDelete();
        }
        throw new IllegalArgumentException("Empty values".toString());
    }
    
    @Override
    public boolean yieldIfContendedSafely() {
        return this.delegate.yieldIfContendedSafely();
    }
    
    @Override
    public boolean yieldIfContendedSafely(final long n) {
        return this.delegate.yieldIfContendedSafely(n);
    }
    
    @Metadata(d1 = { "\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0011\n\u0002\b\u0002\b\u00c1\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J1\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b2\u0012\u0010\t\u001a\u000e\u0012\b\b\u0001\u0012\u0004\u0018\u00010\u0001\u0018\u00010\nH\u0007¢\u0006\u0002\u0010\u000b¨\u0006\f" }, d2 = { "Landroidx/sqlite/db/framework/FrameworkSQLiteDatabase$Api30Impl;", "", "()V", "execPerConnectionSQL", "", "sQLiteDatabase", "Landroid/database/sqlite/SQLiteDatabase;", "sql", "", "bindArgs", "", "(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/Object;)V", "sqlite-framework_release" }, k = 1, mv = { 1, 7, 1 }, xi = 48)
    public static final class Api30Impl
    {
        public static final Api30Impl INSTANCE;
        
        static {
            INSTANCE = new Api30Impl();
        }
        
        private Api30Impl() {
        }
        
        public final void execPerConnectionSQL(final SQLiteDatabase sqLiteDatabase, final String s, final Object[] array) {
            Intrinsics.checkNotNullParameter((Object)sqLiteDatabase, "sQLiteDatabase");
            Intrinsics.checkNotNullParameter((Object)s, "sql");
            sqLiteDatabase.execPerConnectionSQL(s, array);
        }
    }
    
    @Metadata(d1 = { "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0011\n\u0002\u0010\u000e\n\u0002\b\u0003\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u0016\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004X\u0082\u0004¢\u0006\u0004\n\u0002\u0010\u0006R\u0018\u0010\u0007\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00050\u0004X\u0082\u0004¢\u0006\u0004\n\u0002\u0010\u0006¨\u0006\b" }, d2 = { "Landroidx/sqlite/db/framework/FrameworkSQLiteDatabase$Companion;", "", "()V", "CONFLICT_VALUES", "", "", "[Ljava/lang/String;", "EMPTY_STRING_ARRAY", "sqlite-framework_release" }, k = 1, mv = { 1, 7, 1 }, xi = 48)
    public static final class Companion
    {
        private Companion() {
        }
    }
}

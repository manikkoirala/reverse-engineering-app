// 
// Decompiled by Procyon v0.6.0
// 

package androidx.sqlite.db;

import java.util.Iterator;
import kotlin.jvm.JvmStatic;
import android.content.Context;
import android.database.sqlite.SQLiteException;
import java.io.IOException;
import android.util.Pair;
import java.io.File;
import android.os.Build$VERSION;
import android.util.Log;
import kotlin.jvm.internal.Intrinsics;
import kotlin.text.StringsKt;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.Metadata;
import java.io.Closeable;

@Metadata(d1 = { "\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0004\bf\u0018\u00002\u00020\u0001:\u0003\u0011\u0012\u0013J\b\u0010\f\u001a\u00020\rH&J\u0010\u0010\u000e\u001a\u00020\r2\u0006\u0010\u000f\u001a\u00020\u0010H'R\u0014\u0010\u0002\u001a\u0004\u0018\u00010\u0003X¦\u0004¢\u0006\u0006\u001a\u0004\b\u0004\u0010\u0005R\u0012\u0010\u0006\u001a\u00020\u0007X¦\u0004¢\u0006\u0006\u001a\u0004\b\b\u0010\tR\u0012\u0010\n\u001a\u00020\u0007X¦\u0004¢\u0006\u0006\u001a\u0004\b\u000b\u0010\t\u00f8\u0001\u0000\u0082\u0002\u0006\n\u0004\b!0\u0001¨\u0006\u0014\u00c0\u0006\u0001" }, d2 = { "Landroidx/sqlite/db/SupportSQLiteOpenHelper;", "Ljava/io/Closeable;", "databaseName", "", "getDatabaseName", "()Ljava/lang/String;", "readableDatabase", "Landroidx/sqlite/db/SupportSQLiteDatabase;", "getReadableDatabase", "()Landroidx/sqlite/db/SupportSQLiteDatabase;", "writableDatabase", "getWritableDatabase", "close", "", "setWriteAheadLoggingEnabled", "enabled", "", "Callback", "Configuration", "Factory", "sqlite_release" }, k = 1, mv = { 1, 7, 1 }, xi = 48)
public interface SupportSQLiteOpenHelper extends Closeable
{
    void close();
    
    String getDatabaseName();
    
    SupportSQLiteDatabase getReadableDatabase();
    
    SupportSQLiteDatabase getWritableDatabase();
    
    void setWriteAheadLoggingEnabled(final boolean p0);
    
    @Metadata(d1 = { "\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\t\b&\u0018\u0000 \u00132\u00020\u0001:\u0001\u0013B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u0010\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bH\u0002J\u0010\u0010\t\u001a\u00020\u00062\u0006\u0010\n\u001a\u00020\u000bH\u0016J\u0010\u0010\f\u001a\u00020\u00062\u0006\u0010\n\u001a\u00020\u000bH\u0016J\u0010\u0010\r\u001a\u00020\u00062\u0006\u0010\n\u001a\u00020\u000bH&J \u0010\u000e\u001a\u00020\u00062\u0006\u0010\n\u001a\u00020\u000b2\u0006\u0010\u000f\u001a\u00020\u00032\u0006\u0010\u0010\u001a\u00020\u0003H\u0016J\u0010\u0010\u0011\u001a\u00020\u00062\u0006\u0010\n\u001a\u00020\u000bH\u0016J \u0010\u0012\u001a\u00020\u00062\u0006\u0010\n\u001a\u00020\u000b2\u0006\u0010\u000f\u001a\u00020\u00032\u0006\u0010\u0010\u001a\u00020\u0003H&R\u0010\u0010\u0002\u001a\u00020\u00038\u0006X\u0087\u0004¢\u0006\u0002\n\u0000¨\u0006\u0014" }, d2 = { "Landroidx/sqlite/db/SupportSQLiteOpenHelper$Callback;", "", "version", "", "(I)V", "deleteDatabaseFile", "", "fileName", "", "onConfigure", "db", "Landroidx/sqlite/db/SupportSQLiteDatabase;", "onCorruption", "onCreate", "onDowngrade", "oldVersion", "newVersion", "onOpen", "onUpgrade", "Companion", "sqlite_release" }, k = 1, mv = { 1, 7, 1 }, xi = 48)
    public abstract static class Callback
    {
        public static final Companion Companion;
        private static final String TAG = "SupportSQLite";
        public final int version;
        
        static {
            Companion = new Companion(null);
        }
        
        public Callback(final int version) {
            this.version = version;
        }
        
        private final void deleteDatabaseFile(final String s) {
            final int n = 1;
            if (!StringsKt.equals(s, ":memory:", true)) {
                final CharSequence charSequence = s;
                int n2 = charSequence.length() - 1;
                int i = 0;
                int n3 = 0;
                while (i <= n2) {
                    int n4;
                    if (n3 == 0) {
                        n4 = i;
                    }
                    else {
                        n4 = n2;
                    }
                    final boolean b = Intrinsics.compare((int)charSequence.charAt(n4), 32) <= 0;
                    if (n3 == 0) {
                        if (!b) {
                            n3 = 1;
                        }
                        else {
                            ++i;
                        }
                    }
                    else {
                        if (!b) {
                            break;
                        }
                        --n2;
                    }
                }
                int n5;
                if (charSequence.subSequence(i, n2 + 1).toString().length() == 0) {
                    n5 = n;
                }
                else {
                    n5 = 0;
                }
                if (n5 == 0) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("deleting the database file: ");
                    sb.append(s);
                    Log.w("SupportSQLite", sb.toString());
                    try {
                        if (Build$VERSION.SDK_INT >= 16) {
                            SupportSQLiteCompat.Api16Impl.deleteDatabase(new File(s));
                        }
                        else {
                            try {
                                if (!new File(s).delete()) {
                                    final StringBuilder sb2 = new StringBuilder();
                                    sb2.append("Could not delete the database file ");
                                    sb2.append(s);
                                    Log.e("SupportSQLite", sb2.toString());
                                }
                            }
                            catch (final Exception ex) {
                                Log.e("SupportSQLite", "error while deleting corrupted database file", (Throwable)ex);
                            }
                        }
                    }
                    catch (final Exception ex2) {
                        Log.w("SupportSQLite", "delete failed: ", (Throwable)ex2);
                    }
                }
            }
        }
        
        public void onConfigure(final SupportSQLiteDatabase supportSQLiteDatabase) {
            Intrinsics.checkNotNullParameter((Object)supportSQLiteDatabase, "db");
        }
        
        public void onCorruption(SupportSQLiteDatabase obj) {
            Intrinsics.checkNotNullParameter((Object)obj, "db");
            final StringBuilder sb = new StringBuilder();
            sb.append("Corruption reported by sqlite on database: ");
            sb.append(obj);
            sb.append(".path");
            Log.e("SupportSQLite", sb.toString());
            if (!obj.isOpen()) {
                final String path = obj.getPath();
                if (path != null) {
                    this.deleteDatabaseFile(path);
                }
                return;
            }
            final Iterable iterable = null;
        Block_14_Outer:
            while (true) {
                try {
                    Object o = null;
                    Label_0175: {
                        try {
                            o = obj.getAttachedDbs();
                            break Label_0092;
                        }
                        finally {
                            Label_0172: {
                                break Label_0172;
                                try {
                                    obj.close();
                                    break Label_0175;
                                    Label_0117: {
                                        while (true) {
                                            while (true) {
                                                obj = (SupportSQLiteDatabase)iterable.iterator();
                                                break Label_0117;
                                                this.deleteDatabaseFile((String)obj);
                                                iftrue(Label_0156:)(iterable == null);
                                                continue Block_14_Outer;
                                            }
                                            final Object second = ((Iterator<Pair>)obj).next().second;
                                            Intrinsics.checkNotNullExpressionValue(second, "p.second");
                                            this.deleteDatabaseFile((String)second);
                                            break Label_0117;
                                            Label_0156: {
                                                obj = (SupportSQLiteDatabase)obj.getPath();
                                            }
                                            iftrue(Label_0172:)(obj == null);
                                            continue;
                                        }
                                    }
                                    iftrue(Label_0172:)(!((Iterator)obj).hasNext());
                                }
                                catch (final IOException ex) {}
                            }
                        }
                    }
                    if (o != null) {
                        obj = (SupportSQLiteDatabase)((Iterable)o).iterator();
                        while (((Iterator)obj).hasNext()) {
                            o = ((Iterator<Pair>)obj).next().second;
                            Intrinsics.checkNotNullExpressionValue(o, "p.second");
                            this.deleteDatabaseFile((String)o);
                        }
                    }
                    else {
                        obj = (SupportSQLiteDatabase)obj.getPath();
                        if (obj != null) {
                            this.deleteDatabaseFile((String)obj);
                        }
                    }
                }
                catch (final SQLiteException ex2) {
                    continue;
                }
                break;
            }
        }
        
        public abstract void onCreate(final SupportSQLiteDatabase p0);
        
        public void onDowngrade(final SupportSQLiteDatabase supportSQLiteDatabase, final int i, final int j) {
            Intrinsics.checkNotNullParameter((Object)supportSQLiteDatabase, "db");
            final StringBuilder sb = new StringBuilder();
            sb.append("Can't downgrade database from version ");
            sb.append(i);
            sb.append(" to ");
            sb.append(j);
            throw new SQLiteException(sb.toString());
        }
        
        public void onOpen(final SupportSQLiteDatabase supportSQLiteDatabase) {
            Intrinsics.checkNotNullParameter((Object)supportSQLiteDatabase, "db");
        }
        
        public abstract void onUpgrade(final SupportSQLiteDatabase p0, final int p1, final int p2);
        
        @Metadata(d1 = { "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\b\u0080\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082T¢\u0006\u0002\n\u0000¨\u0006\u0005" }, d2 = { "Landroidx/sqlite/db/SupportSQLiteOpenHelper$Callback$Companion;", "", "()V", "TAG", "", "sqlite_release" }, k = 1, mv = { 1, 7, 1 }, xi = 48)
        public static final class Companion
        {
            private Companion() {
            }
        }
    }
    
    @Metadata(d1 = { "\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0005\u0018\u0000 \r2\u00020\u0001:\u0002\f\rB3\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\b\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\b\b\u0002\u0010\b\u001a\u00020\t\u0012\b\b\u0002\u0010\n\u001a\u00020\t¢\u0006\u0002\u0010\u000bR\u0010\u0010\n\u001a\u00020\t8\u0006X\u0087\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\u0006\u001a\u00020\u00078\u0006X\u0087\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\u0002\u001a\u00020\u00038\u0006X\u0087\u0004¢\u0006\u0002\n\u0000R\u0012\u0010\u0004\u001a\u0004\u0018\u00010\u00058\u0006X\u0087\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\b\u001a\u00020\t8\u0006X\u0087\u0004¢\u0006\u0002\n\u0000¨\u0006\u000e" }, d2 = { "Landroidx/sqlite/db/SupportSQLiteOpenHelper$Configuration;", "", "context", "Landroid/content/Context;", "name", "", "callback", "Landroidx/sqlite/db/SupportSQLiteOpenHelper$Callback;", "useNoBackupDirectory", "", "allowDataLossOnRecovery", "(Landroid/content/Context;Ljava/lang/String;Landroidx/sqlite/db/SupportSQLiteOpenHelper$Callback;ZZ)V", "Builder", "Companion", "sqlite_release" }, k = 1, mv = { 1, 7, 1 }, xi = 48)
    public static final class Configuration
    {
        public static final Companion Companion;
        public final boolean allowDataLossOnRecovery;
        public final Callback callback;
        public final Context context;
        public final String name;
        public final boolean useNoBackupDirectory;
        
        static {
            Companion = new Companion(null);
        }
        
        public Configuration(final Context context, final String name, final Callback callback, final boolean useNoBackupDirectory, final boolean allowDataLossOnRecovery) {
            Intrinsics.checkNotNullParameter((Object)context, "context");
            Intrinsics.checkNotNullParameter((Object)callback, "callback");
            this.context = context;
            this.name = name;
            this.callback = callback;
            this.useNoBackupDirectory = useNoBackupDirectory;
            this.allowDataLossOnRecovery = allowDataLossOnRecovery;
        }
        
        @JvmStatic
        public static final Builder builder(final Context context) {
            return Configuration.Companion.builder(context);
        }
        
        @Metadata(d1 = { "\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0016\u0018\u00002\u00020\u0001B\u000f\b\u0000\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u0010\u0010\u0005\u001a\u00020\u00002\u0006\u0010\u0005\u001a\u00020\u0006H\u0016J\b\u0010\f\u001a\u00020\rH\u0016J\u0010\u0010\u0007\u001a\u00020\u00002\u0006\u0010\u0007\u001a\u00020\bH\u0016J\u0012\u0010\t\u001a\u00020\u00002\b\u0010\t\u001a\u0004\u0018\u00010\nH\u0016J\u0010\u0010\u000e\u001a\u00020\u00002\u0006\u0010\u000b\u001a\u00020\u0006H\u0016R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u000e¢\u0006\u0002\n\u0000R\u0010\u0010\u0007\u001a\u0004\u0018\u00010\bX\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\t\u001a\u0004\u0018\u00010\nX\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u0006X\u0082\u000e¢\u0006\u0002\n\u0000¨\u0006\u000f" }, d2 = { "Landroidx/sqlite/db/SupportSQLiteOpenHelper$Configuration$Builder;", "", "context", "Landroid/content/Context;", "(Landroid/content/Context;)V", "allowDataLossOnRecovery", "", "callback", "Landroidx/sqlite/db/SupportSQLiteOpenHelper$Callback;", "name", "", "useNoBackupDirectory", "build", "Landroidx/sqlite/db/SupportSQLiteOpenHelper$Configuration;", "noBackupDirectory", "sqlite_release" }, k = 1, mv = { 1, 7, 1 }, xi = 48)
        public static class Builder
        {
            private boolean allowDataLossOnRecovery;
            private Callback callback;
            private final Context context;
            private String name;
            private boolean useNoBackupDirectory;
            
            public Builder(final Context context) {
                Intrinsics.checkNotNullParameter((Object)context, "context");
                this.context = context;
            }
            
            public Builder allowDataLossOnRecovery(final boolean allowDataLossOnRecovery) {
                final Builder builder = this;
                this.allowDataLossOnRecovery = allowDataLossOnRecovery;
                return this;
            }
            
            public Configuration build() {
                final Callback callback = this.callback;
                if (callback == null) {
                    throw new IllegalArgumentException("Must set a callback to create the configuration.".toString());
                }
                final boolean useNoBackupDirectory = this.useNoBackupDirectory;
                boolean b = false;
                Label_0062: {
                    if (useNoBackupDirectory) {
                        final CharSequence charSequence = this.name;
                        if (charSequence == null || charSequence.length() == 0) {
                            break Label_0062;
                        }
                    }
                    b = true;
                }
                if (b) {
                    return new Configuration(this.context, this.name, callback, this.useNoBackupDirectory, this.allowDataLossOnRecovery);
                }
                throw new IllegalArgumentException("Must set a non-null database name to a configuration that uses the no backup directory.".toString());
            }
            
            public Builder callback(final Callback callback) {
                Intrinsics.checkNotNullParameter((Object)callback, "callback");
                final Builder builder = this;
                this.callback = callback;
                return this;
            }
            
            public Builder name(final String name) {
                final Builder builder = this;
                this.name = name;
                return this;
            }
            
            public Builder noBackupDirectory(final boolean useNoBackupDirectory) {
                final Builder builder = this;
                this.useNoBackupDirectory = useNoBackupDirectory;
                return this;
            }
        }
        
        @Metadata(d1 = { "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0007¨\u0006\u0007" }, d2 = { "Landroidx/sqlite/db/SupportSQLiteOpenHelper$Configuration$Companion;", "", "()V", "builder", "Landroidx/sqlite/db/SupportSQLiteOpenHelper$Configuration$Builder;", "context", "Landroid/content/Context;", "sqlite_release" }, k = 1, mv = { 1, 7, 1 }, xi = 48)
        public static final class Companion
        {
            private Companion() {
            }
            
            @JvmStatic
            public final Builder builder(final Context context) {
                Intrinsics.checkNotNullParameter((Object)context, "context");
                return new Builder(context);
            }
        }
    }
    
    @Metadata(d1 = { "\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u00e6\u0080\u0001\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&\u00f8\u0001\u0000\u0082\u0002\u0006\n\u0004\b!0\u0001¨\u0006\u0006\u00c0\u0006\u0001" }, d2 = { "Landroidx/sqlite/db/SupportSQLiteOpenHelper$Factory;", "", "create", "Landroidx/sqlite/db/SupportSQLiteOpenHelper;", "configuration", "Landroidx/sqlite/db/SupportSQLiteOpenHelper$Configuration;", "sqlite_release" }, k = 1, mv = { 1, 7, 1 }, xi = 48)
    public interface Factory
    {
        SupportSQLiteOpenHelper create(final Configuration p0);
    }
}

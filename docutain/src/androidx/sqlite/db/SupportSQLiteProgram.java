// 
// Decompiled by Procyon v0.6.0
// 

package androidx.sqlite.db;

import kotlin.Metadata;
import java.io.Closeable;

@Metadata(d1 = { "\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0012\n\u0000\n\u0002\u0010\u0006\n\u0000\n\u0002\u0010\t\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\bf\u0018\u00002\u00020\u0001J\u0018\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0007H&J\u0018\u0010\b\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\tH&J\u0018\u0010\n\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u000bH&J\u0010\u0010\f\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&J\u0018\u0010\r\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u000eH&J\b\u0010\u000f\u001a\u00020\u0003H&\u00f8\u0001\u0000\u0082\u0002\u0006\n\u0004\b!0\u0001¨\u0006\u0010\u00c0\u0006\u0001" }, d2 = { "Landroidx/sqlite/db/SupportSQLiteProgram;", "Ljava/io/Closeable;", "bindBlob", "", "index", "", "value", "", "bindDouble", "", "bindLong", "", "bindNull", "bindString", "", "clearBindings", "sqlite_release" }, k = 1, mv = { 1, 7, 1 }, xi = 48)
public interface SupportSQLiteProgram extends Closeable
{
    void bindBlob(final int p0, final byte[] p1);
    
    void bindDouble(final int p0, final double p1);
    
    void bindLong(final int p0, final long p1);
    
    void bindNull(final int p0);
    
    void bindString(final int p0, final String p1);
    
    void clearBindings();
}

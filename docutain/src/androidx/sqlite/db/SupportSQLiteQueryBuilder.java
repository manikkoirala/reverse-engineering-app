// 
// Decompiled by Procyon v0.6.0
// 

package androidx.sqlite.db;

import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.JvmStatic;
import kotlin.jvm.internal.DefaultConstructorMarker;
import java.util.regex.Pattern;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000:\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\u0011\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u0000 \u001d2\u00020\u0001:\u0001\u001dB\u000f\b\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u001d\u0010\b\u001a\u00020\u00002\u0010\u0010\b\u001a\f\u0012\u0006\b\u0001\u0012\u00020\u0003\u0018\u00010\u0006¢\u0006\u0002\u0010\u0011J\u0006\u0010\u0012\u001a\u00020\u0013J\u0006\u0010\n\u001a\u00020\u0000J\u0010\u0010\f\u001a\u00020\u00002\b\u0010\f\u001a\u0004\u0018\u00010\u0003J\u0010\u0010\r\u001a\u00020\u00002\b\u0010\r\u001a\u0004\u0018\u00010\u0003J\u000e\u0010\u000e\u001a\u00020\u00002\u0006\u0010\u000e\u001a\u00020\u0003J\u0010\u0010\u000f\u001a\u00020\u00002\b\u0010\u000f\u001a\u0004\u0018\u00010\u0003J)\u0010\u0010\u001a\u00020\u00002\b\u0010\u0010\u001a\u0004\u0018\u00010\u00032\u0012\u0010\u0005\u001a\u000e\u0012\b\b\u0001\u0012\u0004\u0018\u00010\u0001\u0018\u00010\u0006¢\u0006\u0002\u0010\u0014J\"\u0010\u0015\u001a\u00020\u0016*\u00060\u0017j\u0002`\u00182\u0006\u0010\u0019\u001a\u00020\u00032\b\u0010\u001a\u001a\u0004\u0018\u00010\u0003H\u0002J%\u0010\u001b\u001a\u00020\u0016*\u00060\u0017j\u0002`\u00182\u000e\u0010\b\u001a\n\u0012\u0006\b\u0001\u0012\u00020\u00030\u0006H\u0002¢\u0006\u0002\u0010\u001cR\u001c\u0010\u0005\u001a\u000e\u0012\b\b\u0001\u0012\u0004\u0018\u00010\u0001\u0018\u00010\u0006X\u0082\u000e¢\u0006\u0004\n\u0002\u0010\u0007R\u001a\u0010\b\u001a\f\u0012\u0006\b\u0001\u0012\u00020\u0003\u0018\u00010\u0006X\u0082\u000e¢\u0006\u0004\n\u0002\u0010\tR\u000e\u0010\n\u001a\u00020\u000bX\u0082\u000e¢\u0006\u0002\n\u0000R\u0010\u0010\f\u001a\u0004\u0018\u00010\u0003X\u0082\u000e¢\u0006\u0002\n\u0000R\u0010\u0010\r\u001a\u0004\u0018\u00010\u0003X\u0082\u000e¢\u0006\u0002\n\u0000R\u0010\u0010\u000e\u001a\u0004\u0018\u00010\u0003X\u0082\u000e¢\u0006\u0002\n\u0000R\u0010\u0010\u000f\u001a\u0004\u0018\u00010\u0003X\u0082\u000e¢\u0006\u0002\n\u0000R\u0010\u0010\u0010\u001a\u0004\u0018\u00010\u0003X\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u001e" }, d2 = { "Landroidx/sqlite/db/SupportSQLiteQueryBuilder;", "", "table", "", "(Ljava/lang/String;)V", "bindArgs", "", "[Ljava/lang/Object;", "columns", "[Ljava/lang/String;", "distinct", "", "groupBy", "having", "limit", "orderBy", "selection", "([Ljava/lang/String;)Landroidx/sqlite/db/SupportSQLiteQueryBuilder;", "create", "Landroidx/sqlite/db/SupportSQLiteQuery;", "(Ljava/lang/String;[Ljava/lang/Object;)Landroidx/sqlite/db/SupportSQLiteQueryBuilder;", "appendClause", "", "Ljava/lang/StringBuilder;", "Lkotlin/text/StringBuilder;", "name", "clause", "appendColumns", "(Ljava/lang/StringBuilder;[Ljava/lang/String;)V", "Companion", "sqlite_release" }, k = 1, mv = { 1, 7, 1 }, xi = 48)
public final class SupportSQLiteQueryBuilder
{
    public static final Companion Companion;
    private static final Pattern limitPattern;
    private Object[] bindArgs;
    private String[] columns;
    private boolean distinct;
    private String groupBy;
    private String having;
    private String limit;
    private String orderBy;
    private String selection;
    private final String table;
    
    static {
        Companion = new Companion(null);
        limitPattern = Pattern.compile("\\s*\\d+\\s*(,\\s*\\d+\\s*)?");
    }
    
    private SupportSQLiteQueryBuilder(final String table) {
        this.table = table;
    }
    
    private final void appendClause(final StringBuilder sb, final String str, final String str2) {
        final CharSequence charSequence = str2;
        if (charSequence != null && charSequence.length() != 0) {
            sb.append(str);
            sb.append(str2);
        }
    }
    
    private final void appendColumns(final StringBuilder sb, final String[] array) {
        for (int length = array.length, i = 0; i < length; ++i) {
            final String str = array[i];
            if (i > 0) {
                sb.append(", ");
            }
            sb.append(str);
        }
        sb.append(' ');
    }
    
    @JvmStatic
    public static final SupportSQLiteQueryBuilder builder(final String s) {
        return SupportSQLiteQueryBuilder.Companion.builder(s);
    }
    
    public final SupportSQLiteQueryBuilder columns(final String[] columns) {
        final SupportSQLiteQueryBuilder supportSQLiteQueryBuilder = this;
        this.columns = columns;
        return this;
    }
    
    public final SupportSQLiteQuery create() {
        final CharSequence charSequence = this.groupBy;
        boolean b = false;
        boolean b2 = false;
        Label_0082: {
            if (charSequence == null || charSequence.length() == 0) {
                final CharSequence charSequence2 = this.having;
                if (charSequence2 != null && charSequence2.length() != 0) {
                    b2 = false;
                    break Label_0082;
                }
            }
            b2 = true;
        }
        if (b2) {
            final StringBuilder sb = new StringBuilder(120);
            sb.append("SELECT ");
            if (this.distinct) {
                sb.append("DISTINCT ");
            }
            final String[] columns = this.columns;
            if (columns == null || columns.length == 0) {
                b = true;
            }
            if (!b) {
                Intrinsics.checkNotNull((Object)columns);
                this.appendColumns(sb, columns);
            }
            else {
                sb.append("* ");
            }
            sb.append("FROM ");
            sb.append(this.table);
            this.appendClause(sb, " WHERE ", this.selection);
            this.appendClause(sb, " GROUP BY ", this.groupBy);
            this.appendClause(sb, " HAVING ", this.having);
            this.appendClause(sb, " ORDER BY ", this.orderBy);
            this.appendClause(sb, " LIMIT ", this.limit);
            final String string = sb.toString();
            Intrinsics.checkNotNullExpressionValue((Object)string, "StringBuilder(capacity).\u2026builderAction).toString()");
            return new SimpleSQLiteQuery(string, this.bindArgs);
        }
        throw new IllegalArgumentException("HAVING clauses are only permitted when using a groupBy clause".toString());
    }
    
    public final SupportSQLiteQueryBuilder distinct() {
        final SupportSQLiteQueryBuilder supportSQLiteQueryBuilder = this;
        this.distinct = true;
        return this;
    }
    
    public final SupportSQLiteQueryBuilder groupBy(final String groupBy) {
        final SupportSQLiteQueryBuilder supportSQLiteQueryBuilder = this;
        this.groupBy = groupBy;
        return this;
    }
    
    public final SupportSQLiteQueryBuilder having(final String having) {
        final SupportSQLiteQueryBuilder supportSQLiteQueryBuilder = this;
        this.having = having;
        return this;
    }
    
    public final SupportSQLiteQueryBuilder limit(final String s) {
        Intrinsics.checkNotNullParameter((Object)s, "limit");
        final SupportSQLiteQueryBuilder supportSQLiteQueryBuilder = this;
        final Pattern limitPattern = SupportSQLiteQueryBuilder.limitPattern;
        final CharSequence input = s;
        final boolean matches = limitPattern.matcher(input).matches();
        final int length = input.length();
        final boolean b = true;
        final boolean b2 = length == 0;
        int n = b ? 1 : 0;
        if (!b2) {
            if (matches) {
                n = (b ? 1 : 0);
            }
            else {
                n = 0;
            }
        }
        if (n != 0) {
            this.limit = s;
            return this;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("invalid LIMIT clauses:");
        sb.append(s);
        throw new IllegalArgumentException(sb.toString().toString());
    }
    
    public final SupportSQLiteQueryBuilder orderBy(final String orderBy) {
        final SupportSQLiteQueryBuilder supportSQLiteQueryBuilder = this;
        this.orderBy = orderBy;
        return this;
    }
    
    public final SupportSQLiteQueryBuilder selection(final String selection, final Object[] bindArgs) {
        final SupportSQLiteQueryBuilder supportSQLiteQueryBuilder = this;
        this.selection = selection;
        this.bindArgs = bindArgs;
        return this;
    }
    
    @Metadata(d1 = { "\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0010\u0010\u0006\u001a\u00020\u00072\u0006\u0010\b\u001a\u00020\tH\u0007R\u0016\u0010\u0003\u001a\n \u0005*\u0004\u0018\u00010\u00040\u0004X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\n" }, d2 = { "Landroidx/sqlite/db/SupportSQLiteQueryBuilder$Companion;", "", "()V", "limitPattern", "Ljava/util/regex/Pattern;", "kotlin.jvm.PlatformType", "builder", "Landroidx/sqlite/db/SupportSQLiteQueryBuilder;", "tableName", "", "sqlite_release" }, k = 1, mv = { 1, 7, 1 }, xi = 48)
    public static final class Companion
    {
        private Companion() {
        }
        
        @JvmStatic
        public final SupportSQLiteQueryBuilder builder(final String s) {
            Intrinsics.checkNotNullParameter((Object)s, "tableName");
            return new SupportSQLiteQueryBuilder(s, null);
        }
    }
}

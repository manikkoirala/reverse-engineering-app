// 
// Decompiled by Procyon v0.6.0
// 

package androidx.sqlite.util;

import java.util.concurrent.locks.ReentrantLock;
import android.util.Log;
import java.io.FileOutputStream;
import java.io.IOException;
import kotlin.jvm.internal.Intrinsics;
import java.util.HashMap;
import kotlin.jvm.internal.DefaultConstructorMarker;
import java.io.File;
import java.nio.channels.FileChannel;
import java.util.concurrent.locks.Lock;
import java.util.Map;
import kotlin.Metadata;

@Metadata(d1 = { "\u00004\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\b\u0007\u0018\u0000 \u00112\u00020\u0001:\u0001\u0011B\u001f\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\b\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007¢\u0006\u0002\u0010\bJ\u0010\u0010\u000e\u001a\u00020\u000f2\b\b\u0002\u0010\u0006\u001a\u00020\u0007J\u0006\u0010\u0010\u001a\u00020\u000fR\u0010\u0010\t\u001a\u0004\u0018\u00010\nX\u0082\u000e¢\u0006\u0002\n\u0000R\u0010\u0010\u000b\u001a\u0004\u0018\u00010\u0005X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\f\u001a\u00020\r8\u0002X\u0083\u0004¢\u0006\u0002\n\u0000¨\u0006\u0012" }, d2 = { "Landroidx/sqlite/util/ProcessLock;", "", "name", "", "lockDir", "Ljava/io/File;", "processLock", "", "(Ljava/lang/String;Ljava/io/File;Z)V", "lockChannel", "Ljava/nio/channels/FileChannel;", "lockFile", "threadLock", "Ljava/util/concurrent/locks/Lock;", "lock", "", "unlock", "Companion", "sqlite-framework_release" }, k = 1, mv = { 1, 7, 1 }, xi = 48)
public final class ProcessLock
{
    public static final Companion Companion;
    private static final String TAG = "SupportSQLiteLock";
    private static final Map<String, Lock> threadLocksMap;
    private FileChannel lockChannel;
    private final File lockFile;
    private final boolean processLock;
    private final Lock threadLock;
    
    static {
        Companion = new Companion(null);
        threadLocksMap = new HashMap<String, Lock>();
    }
    
    public ProcessLock(final String str, File file, final boolean processLock) {
        Intrinsics.checkNotNullParameter((Object)str, "name");
        this.processLock = processLock;
        if (file != null) {
            final StringBuilder sb = new StringBuilder();
            sb.append(str);
            sb.append(".lck");
            file = new File(file, sb.toString());
        }
        else {
            file = null;
        }
        this.lockFile = file;
        this.threadLock = ProcessLock.Companion.getThreadLock(str);
    }
    
    public static final /* synthetic */ Map access$getThreadLocksMap$cp() {
        return ProcessLock.threadLocksMap;
    }
    
    public final void lock(final boolean b) {
        this.threadLock.lock();
        if (b) {
            try {
                final File lockFile = this.lockFile;
                if (lockFile == null) {
                    throw new IOException("No lock directory was provided.");
                }
                final File parentFile = lockFile.getParentFile();
                if (parentFile != null) {
                    parentFile.mkdirs();
                }
                final FileChannel channel = new FileOutputStream(this.lockFile).getChannel();
                channel.lock();
                this.lockChannel = channel;
            }
            catch (final IOException ex) {
                this.lockChannel = null;
                Log.w("SupportSQLiteLock", "Unable to grab file lock.", (Throwable)ex);
            }
        }
    }
    
    public final void unlock() {
        while (true) {
            try {
                final FileChannel lockChannel = this.lockChannel;
                if (lockChannel != null) {
                    lockChannel.close();
                }
                this.threadLock.unlock();
            }
            catch (final IOException ex) {
                continue;
            }
            break;
        }
    }
    
    @Metadata(d1 = { "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010%\n\u0002\u0018\u0002\n\u0002\b\u0003\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0010\u0010\b\u001a\u00020\u00072\u0006\u0010\t\u001a\u00020\u0004H\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082T¢\u0006\u0002\n\u0000R\u001a\u0010\u0005\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00070\u0006X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\n" }, d2 = { "Landroidx/sqlite/util/ProcessLock$Companion;", "", "()V", "TAG", "", "threadLocksMap", "", "Ljava/util/concurrent/locks/Lock;", "getThreadLock", "key", "sqlite-framework_release" }, k = 1, mv = { 1, 7, 1 }, xi = 48)
    public static final class Companion
    {
        private Companion() {
        }
        
        private final Lock getThreadLock(final String s) {
            synchronized (ProcessLock.access$getThreadLocksMap$cp()) {
                final Map access$getThreadLocksMap$cp = ProcessLock.access$getThreadLocksMap$cp();
                Object value;
                if ((value = access$getThreadLocksMap$cp.get(s)) == null) {
                    value = new ReentrantLock();
                    access$getThreadLocksMap$cp.put(s, value);
                }
                return (Lock)value;
            }
        }
    }
}

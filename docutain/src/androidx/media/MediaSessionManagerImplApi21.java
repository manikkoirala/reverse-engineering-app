// 
// Decompiled by Procyon v0.6.0
// 

package androidx.media;

import android.content.Context;

class MediaSessionManagerImplApi21 extends MediaSessionManagerImplBase
{
    MediaSessionManagerImplApi21(final Context mContext) {
        super(mContext);
        this.mContext = mContext;
    }
    
    private boolean hasMediaControlPermission(final RemoteUserInfoImpl remoteUserInfoImpl) {
        return this.getContext().checkPermission("android.permission.MEDIA_CONTENT_CONTROL", remoteUserInfoImpl.getPid(), remoteUserInfoImpl.getUid()) == 0;
    }
    
    @Override
    public boolean isTrustedForMediaControl(final RemoteUserInfoImpl remoteUserInfoImpl) {
        return this.hasMediaControlPermission(remoteUserInfoImpl) || super.isTrustedForMediaControl(remoteUserInfoImpl);
    }
}

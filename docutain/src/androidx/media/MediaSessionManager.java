// 
// Decompiled by Procyon v0.6.0
// 

package androidx.media;

import android.text.TextUtils;
import android.media.session.MediaSessionManager$RemoteUserInfo;
import android.os.Build$VERSION;
import android.content.Context;
import android.util.Log;

public final class MediaSessionManager
{
    static final boolean DEBUG;
    static final String TAG = "MediaSessionManager";
    private static final Object sLock;
    private static volatile MediaSessionManager sSessionManager;
    MediaSessionManagerImpl mImpl;
    
    static {
        DEBUG = Log.isLoggable("MediaSessionManager", 3);
        sLock = new Object();
    }
    
    private MediaSessionManager(final Context context) {
        if (Build$VERSION.SDK_INT >= 28) {
            this.mImpl = (MediaSessionManagerImpl)new MediaSessionManagerImplApi28(context);
        }
        else if (Build$VERSION.SDK_INT >= 21) {
            this.mImpl = (MediaSessionManagerImpl)new MediaSessionManagerImplApi21(context);
        }
        else {
            this.mImpl = (MediaSessionManagerImpl)new MediaSessionManagerImplBase(context);
        }
    }
    
    public static MediaSessionManager getSessionManager(final Context context) {
        if (context != null) {
            synchronized (MediaSessionManager.sLock) {
                if (MediaSessionManager.sSessionManager == null) {
                    MediaSessionManager.sSessionManager = new MediaSessionManager(context.getApplicationContext());
                }
                return MediaSessionManager.sSessionManager;
            }
        }
        throw new IllegalArgumentException("context cannot be null");
    }
    
    Context getContext() {
        return this.mImpl.getContext();
    }
    
    public boolean isTrustedForMediaControl(final RemoteUserInfo remoteUserInfo) {
        if (remoteUserInfo != null) {
            return this.mImpl.isTrustedForMediaControl(remoteUserInfo.mImpl);
        }
        throw new IllegalArgumentException("userInfo should not be null");
    }
    
    interface MediaSessionManagerImpl
    {
        Context getContext();
        
        boolean isTrustedForMediaControl(final RemoteUserInfoImpl p0);
    }
    
    public static final class RemoteUserInfo
    {
        public static final String LEGACY_CONTROLLER = "android.media.session.MediaController";
        public static final int UNKNOWN_PID = -1;
        public static final int UNKNOWN_UID = -1;
        RemoteUserInfoImpl mImpl;
        
        public RemoteUserInfo(final MediaSessionManager$RemoteUserInfo mediaSessionManager$RemoteUserInfo) {
            final String packageName = MediaSessionManagerImplApi28.RemoteUserInfoImplApi28.getPackageName(mediaSessionManager$RemoteUserInfo);
            if (packageName == null) {
                throw new NullPointerException("package shouldn't be null");
            }
            if (!TextUtils.isEmpty((CharSequence)packageName)) {
                this.mImpl = new MediaSessionManagerImplApi28.RemoteUserInfoImplApi28(mediaSessionManager$RemoteUserInfo);
                return;
            }
            throw new IllegalArgumentException("packageName should be nonempty");
        }
        
        public RemoteUserInfo(final String s, final int n, final int n2) {
            if (s == null) {
                throw new NullPointerException("package shouldn't be null");
            }
            if (!TextUtils.isEmpty((CharSequence)s)) {
                if (Build$VERSION.SDK_INT >= 28) {
                    this.mImpl = new MediaSessionManagerImplApi28.RemoteUserInfoImplApi28(s, n, n2);
                }
                else {
                    this.mImpl = new MediaSessionManagerImplBase.RemoteUserInfoImplBase(s, n, n2);
                }
                return;
            }
            throw new IllegalArgumentException("packageName should be nonempty");
        }
        
        @Override
        public boolean equals(final Object o) {
            return this == o || (o instanceof RemoteUserInfo && this.mImpl.equals(((RemoteUserInfo)o).mImpl));
        }
        
        public String getPackageName() {
            return this.mImpl.getPackageName();
        }
        
        public int getPid() {
            return this.mImpl.getPid();
        }
        
        public int getUid() {
            return this.mImpl.getUid();
        }
        
        @Override
        public int hashCode() {
            return this.mImpl.hashCode();
        }
    }
    
    interface RemoteUserInfoImpl
    {
        String getPackageName();
        
        int getPid();
        
        int getUid();
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.media.app;

import android.os.IBinder;
import android.os.Parcelable;
import android.os.Bundle;
import android.os.Parcel;
import androidx.core.app.BundleCompat;
import android.app.Notification;
import android.app.PendingIntent;
import android.os.Build$VERSION;
import androidx.core.app.NotificationBuilderWithBuilderAccessor;
import androidx.media.R;
import android.app.Notification$DecoratedMediaCustomViewStyle;
import android.app.Notification$Style;
import android.app.Notification$Builder;
import android.media.session.MediaSession$Token;
import android.support.v4.media.session.MediaSessionCompat;
import android.app.Notification$MediaStyle;
import android.widget.RemoteViews;

public class NotificationCompat
{
    private NotificationCompat() {
    }
    
    private static class Api15Impl
    {
        static void setContentDescription(final RemoteViews remoteViews, final int n, final CharSequence charSequence) {
            remoteViews.setContentDescription(n, charSequence);
        }
    }
    
    private static class Api21Impl
    {
        static Notification$MediaStyle createMediaStyle() {
            return new Notification$MediaStyle();
        }
        
        static Notification$MediaStyle fillInMediaStyle(final Notification$MediaStyle notification$MediaStyle, final int[] array, final MediaSessionCompat.Token token) {
            if (array != null) {
                setShowActionsInCompactView(notification$MediaStyle, array);
            }
            if (token != null) {
                setMediaSession(notification$MediaStyle, (MediaSession$Token)token.getToken());
            }
            return notification$MediaStyle;
        }
        
        static void setMediaSession(final Notification$MediaStyle notification$MediaStyle, final MediaSession$Token mediaSession) {
            notification$MediaStyle.setMediaSession(mediaSession);
        }
        
        static void setMediaStyle(final Notification$Builder notification$Builder, final Notification$MediaStyle style) {
            notification$Builder.setStyle((Notification$Style)style);
        }
        
        static void setShowActionsInCompactView(final Notification$MediaStyle notification$MediaStyle, final int... showActionsInCompactView) {
            notification$MediaStyle.setShowActionsInCompactView(showActionsInCompactView);
        }
    }
    
    private static class Api24Impl
    {
        static Notification$DecoratedMediaCustomViewStyle createDecoratedMediaCustomViewStyle() {
            return new Notification$DecoratedMediaCustomViewStyle();
        }
    }
    
    public static class DecoratedMediaCustomViewStyle extends MediaStyle
    {
        private void setBackgroundColor(final RemoteViews remoteViews) {
            int n;
            if (this.mBuilder.getColor() != 0) {
                n = this.mBuilder.getColor();
            }
            else {
                n = this.mBuilder.mContext.getResources().getColor(R.color.notification_material_background_media_default_color);
            }
            remoteViews.setInt(R.id.status_bar_latest_event_content, "setBackgroundColor", n);
        }
        
        @Override
        public void apply(final NotificationBuilderWithBuilderAccessor notificationBuilderWithBuilderAccessor) {
            if (Build$VERSION.SDK_INT >= 24) {
                Api21Impl.setMediaStyle(notificationBuilderWithBuilderAccessor.getBuilder(), Api21Impl.fillInMediaStyle((Notification$MediaStyle)Api24Impl.createDecoratedMediaCustomViewStyle(), this.mActionsToShowInCompact, this.mToken));
            }
            else {
                super.apply(notificationBuilderWithBuilderAccessor);
            }
        }
        
        @Override
        int getBigContentViewLayoutResource(int n) {
            if (n <= 3) {
                n = R.layout.notification_template_big_media_narrow_custom;
            }
            else {
                n = R.layout.notification_template_big_media_custom;
            }
            return n;
        }
        
        @Override
        int getContentViewLayoutResource() {
            int n;
            if (this.mBuilder.getContentView() != null) {
                n = R.layout.notification_template_media_custom;
            }
            else {
                n = super.getContentViewLayoutResource();
            }
            return n;
        }
        
        @Override
        public RemoteViews makeBigContentView(final NotificationBuilderWithBuilderAccessor notificationBuilderWithBuilderAccessor) {
            if (Build$VERSION.SDK_INT >= 24) {
                return null;
            }
            RemoteViews remoteViews;
            if (this.mBuilder.getBigContentView() != null) {
                remoteViews = this.mBuilder.getBigContentView();
            }
            else {
                remoteViews = this.mBuilder.getContentView();
            }
            if (remoteViews == null) {
                return null;
            }
            final RemoteViews generateBigContentView = ((MediaStyle)this).generateBigContentView();
            ((Style)this).buildIntoRemoteViews(generateBigContentView, remoteViews);
            if (Build$VERSION.SDK_INT >= 21) {
                this.setBackgroundColor(generateBigContentView);
            }
            return generateBigContentView;
        }
        
        @Override
        public RemoteViews makeContentView(final NotificationBuilderWithBuilderAccessor notificationBuilderWithBuilderAccessor) {
            if (Build$VERSION.SDK_INT >= 24) {
                return null;
            }
            final RemoteViews contentView = this.mBuilder.getContentView();
            final boolean b = true;
            final boolean b2 = contentView != null;
            if (Build$VERSION.SDK_INT >= 21) {
                int n = b ? 1 : 0;
                if (!b2) {
                    if (this.mBuilder.getBigContentView() != null) {
                        n = (b ? 1 : 0);
                    }
                    else {
                        n = 0;
                    }
                }
                if (n != 0) {
                    final RemoteViews generateContentView = ((MediaStyle)this).generateContentView();
                    if (b2) {
                        ((Style)this).buildIntoRemoteViews(generateContentView, this.mBuilder.getContentView());
                    }
                    this.setBackgroundColor(generateContentView);
                    return generateContentView;
                }
            }
            else {
                final RemoteViews generateContentView2 = ((MediaStyle)this).generateContentView();
                if (b2) {
                    ((Style)this).buildIntoRemoteViews(generateContentView2, this.mBuilder.getContentView());
                    return generateContentView2;
                }
            }
            return null;
        }
        
        @Override
        public RemoteViews makeHeadsUpContentView(final NotificationBuilderWithBuilderAccessor notificationBuilderWithBuilderAccessor) {
            if (Build$VERSION.SDK_INT >= 24) {
                return null;
            }
            RemoteViews remoteViews;
            if (this.mBuilder.getHeadsUpContentView() != null) {
                remoteViews = this.mBuilder.getHeadsUpContentView();
            }
            else {
                remoteViews = this.mBuilder.getContentView();
            }
            if (remoteViews == null) {
                return null;
            }
            final RemoteViews generateBigContentView = ((MediaStyle)this).generateBigContentView();
            ((Style)this).buildIntoRemoteViews(generateBigContentView, remoteViews);
            if (Build$VERSION.SDK_INT >= 21) {
                this.setBackgroundColor(generateBigContentView);
            }
            return generateBigContentView;
        }
    }
    
    public static class MediaStyle extends Style
    {
        private static final int MAX_MEDIA_BUTTONS = 5;
        private static final int MAX_MEDIA_BUTTONS_IN_COMPACT = 3;
        int[] mActionsToShowInCompact;
        PendingIntent mCancelButtonIntent;
        boolean mShowCancelButton;
        MediaSessionCompat.Token mToken;
        
        public MediaStyle() {
            this.mActionsToShowInCompact = null;
        }
        
        public MediaStyle(final NotificationCompat.Builder builder) {
            this.mActionsToShowInCompact = null;
            ((Style)this).setBuilder(builder);
        }
        
        private RemoteViews generateMediaActionButton(final Action action) {
            final boolean b = action.getActionIntent() == null;
            final RemoteViews remoteViews = new RemoteViews(this.mBuilder.mContext.getPackageName(), R.layout.notification_media_action);
            remoteViews.setImageViewResource(R.id.action0, action.getIcon());
            if (!b) {
                remoteViews.setOnClickPendingIntent(R.id.action0, action.getActionIntent());
            }
            if (Build$VERSION.SDK_INT >= 15) {
                Api15Impl.setContentDescription(remoteViews, R.id.action0, action.getTitle());
            }
            return remoteViews;
        }
        
        public static MediaSessionCompat.Token getMediaSession(final Notification notification) {
            final Bundle extras = NotificationCompat.getExtras(notification);
            if (extras != null) {
                if (Build$VERSION.SDK_INT >= 21) {
                    final Parcelable parcelable = extras.getParcelable("android.mediaSession");
                    if (parcelable != null) {
                        return MediaSessionCompat.Token.fromToken(parcelable);
                    }
                }
                else {
                    final IBinder binder = BundleCompat.getBinder(extras, "android.mediaSession");
                    if (binder != null) {
                        final Parcel obtain = Parcel.obtain();
                        obtain.writeStrongBinder(binder);
                        obtain.setDataPosition(0);
                        final MediaSessionCompat.Token token = (MediaSessionCompat.Token)MediaSessionCompat.Token.CREATOR.createFromParcel(obtain);
                        obtain.recycle();
                        return token;
                    }
                }
            }
            return null;
        }
        
        @Override
        public void apply(final NotificationBuilderWithBuilderAccessor notificationBuilderWithBuilderAccessor) {
            if (Build$VERSION.SDK_INT >= 21) {
                Api21Impl.setMediaStyle(notificationBuilderWithBuilderAccessor.getBuilder(), Api21Impl.fillInMediaStyle(Api21Impl.createMediaStyle(), this.mActionsToShowInCompact, this.mToken));
            }
            else if (this.mShowCancelButton) {
                notificationBuilderWithBuilderAccessor.getBuilder().setOngoing(true);
            }
        }
        
        RemoteViews generateBigContentView() {
            final int min = Math.min(this.mBuilder.mActions.size(), 5);
            final RemoteViews applyStandardTemplate = ((Style)this).applyStandardTemplate(false, this.getBigContentViewLayoutResource(min), false);
            applyStandardTemplate.removeAllViews(R.id.media_actions);
            if (min > 0) {
                for (int i = 0; i < min; ++i) {
                    applyStandardTemplate.addView(R.id.media_actions, this.generateMediaActionButton(this.mBuilder.mActions.get(i)));
                }
            }
            if (this.mShowCancelButton) {
                applyStandardTemplate.setViewVisibility(R.id.cancel_action, 0);
                applyStandardTemplate.setInt(R.id.cancel_action, "setAlpha", this.mBuilder.mContext.getResources().getInteger(R.integer.cancel_button_image_alpha));
                applyStandardTemplate.setOnClickPendingIntent(R.id.cancel_action, this.mCancelButtonIntent);
            }
            else {
                applyStandardTemplate.setViewVisibility(R.id.cancel_action, 8);
            }
            return applyStandardTemplate;
        }
        
        RemoteViews generateContentView() {
            final RemoteViews applyStandardTemplate = ((Style)this).applyStandardTemplate(false, this.getContentViewLayoutResource(), true);
            final int size = this.mBuilder.mActions.size();
            final int[] mActionsToShowInCompact = this.mActionsToShowInCompact;
            int min;
            if (mActionsToShowInCompact == null) {
                min = 0;
            }
            else {
                min = Math.min(mActionsToShowInCompact.length, 3);
            }
            applyStandardTemplate.removeAllViews(R.id.media_actions);
            if (min > 0) {
                for (int i = 0; i < min; ++i) {
                    if (i >= size) {
                        throw new IllegalArgumentException(String.format("setShowActionsInCompactView: action %d out of bounds (max %d)", i, size - 1));
                    }
                    applyStandardTemplate.addView(R.id.media_actions, this.generateMediaActionButton(this.mBuilder.mActions.get(this.mActionsToShowInCompact[i])));
                }
            }
            if (this.mShowCancelButton) {
                applyStandardTemplate.setViewVisibility(R.id.end_padder, 8);
                applyStandardTemplate.setViewVisibility(R.id.cancel_action, 0);
                applyStandardTemplate.setOnClickPendingIntent(R.id.cancel_action, this.mCancelButtonIntent);
                applyStandardTemplate.setInt(R.id.cancel_action, "setAlpha", this.mBuilder.mContext.getResources().getInteger(R.integer.cancel_button_image_alpha));
            }
            else {
                applyStandardTemplate.setViewVisibility(R.id.end_padder, 0);
                applyStandardTemplate.setViewVisibility(R.id.cancel_action, 8);
            }
            return applyStandardTemplate;
        }
        
        int getBigContentViewLayoutResource(int n) {
            if (n <= 3) {
                n = R.layout.notification_template_big_media_narrow;
            }
            else {
                n = R.layout.notification_template_big_media;
            }
            return n;
        }
        
        int getContentViewLayoutResource() {
            return R.layout.notification_template_media;
        }
        
        @Override
        public RemoteViews makeBigContentView(final NotificationBuilderWithBuilderAccessor notificationBuilderWithBuilderAccessor) {
            if (Build$VERSION.SDK_INT >= 21) {
                return null;
            }
            return this.generateBigContentView();
        }
        
        @Override
        public RemoteViews makeContentView(final NotificationBuilderWithBuilderAccessor notificationBuilderWithBuilderAccessor) {
            if (Build$VERSION.SDK_INT >= 21) {
                return null;
            }
            return this.generateContentView();
        }
        
        public MediaStyle setCancelButtonIntent(final PendingIntent mCancelButtonIntent) {
            this.mCancelButtonIntent = mCancelButtonIntent;
            return this;
        }
        
        public MediaStyle setMediaSession(final MediaSessionCompat.Token mToken) {
            this.mToken = mToken;
            return this;
        }
        
        public MediaStyle setShowActionsInCompactView(final int... mActionsToShowInCompact) {
            this.mActionsToShowInCompact = mActionsToShowInCompact;
            return this;
        }
        
        public MediaStyle setShowCancelButton(final boolean mShowCancelButton) {
            if (Build$VERSION.SDK_INT < 21) {
                this.mShowCancelButton = mShowCancelButton;
            }
            return this;
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.media;

import android.util.Log;
import java.util.Arrays;

public class AudioAttributesImplBase implements AudioAttributesImpl
{
    public int mContentType;
    public int mFlags;
    public int mLegacyStream;
    public int mUsage;
    
    public AudioAttributesImplBase() {
        this.mUsage = 0;
        this.mContentType = 0;
        this.mFlags = 0;
        this.mLegacyStream = -1;
    }
    
    AudioAttributesImplBase(final int mContentType, final int mFlags, final int mUsage, final int mLegacyStream) {
        this.mContentType = mContentType;
        this.mFlags = mFlags;
        this.mUsage = mUsage;
        this.mLegacyStream = mLegacyStream;
    }
    
    static int usageForStreamType(final int n) {
        switch (n) {
            default: {
                return 0;
            }
            case 10: {
                return 11;
            }
            case 8: {
                return 3;
            }
            case 6: {
                return 2;
            }
            case 5: {
                return 5;
            }
            case 4: {
                return 4;
            }
            case 3: {
                return 1;
            }
            case 2: {
                return 6;
            }
            case 1:
            case 7: {
                return 13;
            }
            case 0: {
                return 2;
            }
        }
    }
    
    @Override
    public boolean equals(final Object o) {
        final boolean b = o instanceof AudioAttributesImplBase;
        final boolean b2 = false;
        if (!b) {
            return false;
        }
        final AudioAttributesImplBase audioAttributesImplBase = (AudioAttributesImplBase)o;
        boolean b3 = b2;
        if (this.mContentType == audioAttributesImplBase.getContentType()) {
            b3 = b2;
            if (this.mFlags == audioAttributesImplBase.getFlags()) {
                b3 = b2;
                if (this.mUsage == audioAttributesImplBase.getUsage()) {
                    b3 = b2;
                    if (this.mLegacyStream == audioAttributesImplBase.mLegacyStream) {
                        b3 = true;
                    }
                }
            }
        }
        return b3;
    }
    
    @Override
    public Object getAudioAttributes() {
        return null;
    }
    
    @Override
    public int getContentType() {
        return this.mContentType;
    }
    
    @Override
    public int getFlags() {
        final int mFlags = this.mFlags;
        final int legacyStreamType = this.getLegacyStreamType();
        int n;
        if (legacyStreamType == 6) {
            n = (mFlags | 0x4);
        }
        else {
            n = mFlags;
            if (legacyStreamType == 7) {
                n = (mFlags | 0x1);
            }
        }
        return n & 0x111;
    }
    
    @Override
    public int getLegacyStreamType() {
        final int mLegacyStream = this.mLegacyStream;
        if (mLegacyStream != -1) {
            return mLegacyStream;
        }
        return AudioAttributesCompat.toVolumeStreamType(false, this.mFlags, this.mUsage);
    }
    
    @Override
    public int getRawLegacyStreamType() {
        return this.mLegacyStream;
    }
    
    @Override
    public int getUsage() {
        return this.mUsage;
    }
    
    @Override
    public int getVolumeControlStream() {
        return AudioAttributesCompat.toVolumeStreamType(true, this.mFlags, this.mUsage);
    }
    
    @Override
    public int hashCode() {
        return Arrays.hashCode(new Object[] { this.mContentType, this.mFlags, this.mUsage, this.mLegacyStream });
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("AudioAttributesCompat:");
        if (this.mLegacyStream != -1) {
            sb.append(" stream=");
            sb.append(this.mLegacyStream);
            sb.append(" derived");
        }
        sb.append(" usage=");
        sb.append(AudioAttributesCompat.usageToString(this.mUsage));
        sb.append(" content=");
        sb.append(this.mContentType);
        sb.append(" flags=0x");
        sb.append(Integer.toHexString(this.mFlags).toUpperCase());
        return sb.toString();
    }
    
    static class Builder implements AudioAttributesImpl.Builder
    {
        private int mContentType;
        private int mFlags;
        private int mLegacyStream;
        private int mUsage;
        
        Builder() {
            this.mUsage = 0;
            this.mContentType = 0;
            this.mFlags = 0;
            this.mLegacyStream = -1;
        }
        
        Builder(final AudioAttributesCompat audioAttributesCompat) {
            this.mUsage = 0;
            this.mContentType = 0;
            this.mFlags = 0;
            this.mLegacyStream = -1;
            this.mUsage = audioAttributesCompat.getUsage();
            this.mContentType = audioAttributesCompat.getContentType();
            this.mFlags = audioAttributesCompat.getFlags();
            this.mLegacyStream = audioAttributesCompat.getRawLegacyStreamType();
        }
        
        private Builder setInternalLegacyStreamType(final int i) {
            switch (i) {
                default: {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Invalid stream type ");
                    sb.append(i);
                    sb.append(" for AudioAttributesCompat");
                    Log.e("AudioAttributesCompat", sb.toString());
                    break;
                }
                case 10: {
                    this.mContentType = 1;
                    break;
                }
                case 9: {
                    this.mContentType = 4;
                    break;
                }
                case 8: {
                    this.mContentType = 4;
                    break;
                }
                case 6: {
                    this.mContentType = 1;
                    this.mFlags |= 0x4;
                    break;
                }
                case 5: {
                    this.mContentType = 4;
                    break;
                }
                case 4: {
                    this.mContentType = 4;
                    break;
                }
                case 3: {
                    this.mContentType = 2;
                    break;
                }
                case 2: {
                    this.mContentType = 4;
                    break;
                }
                case 7: {
                    this.mFlags |= 0x1;
                }
                case 1: {
                    this.mContentType = 4;
                    break;
                }
                case 0: {
                    this.mContentType = 1;
                    break;
                }
            }
            this.mUsage = AudioAttributesImplBase.usageForStreamType(i);
            return this;
        }
        
        @Override
        public AudioAttributesImpl build() {
            return new AudioAttributesImplBase(this.mContentType, this.mFlags, this.mUsage, this.mLegacyStream);
        }
        
        public Builder setContentType(final int mContentType) {
            if (mContentType != 0 && mContentType != 1 && mContentType != 2 && mContentType != 3 && mContentType != 4) {
                this.mContentType = 0;
            }
            else {
                this.mContentType = mContentType;
            }
            return this;
        }
        
        public Builder setFlags(final int n) {
            this.mFlags |= (n & 0x3FF);
            return this;
        }
        
        public Builder setLegacyStreamType(final int n) {
            if (n != 10) {
                this.mLegacyStream = n;
                return this.setInternalLegacyStreamType(n);
            }
            throw new IllegalArgumentException("STREAM_ACCESSIBILITY is not a legacy stream type that was used for audio playback");
        }
        
        public Builder setUsage(final int mUsage) {
            switch (mUsage) {
                default: {
                    this.mUsage = 0;
                    break;
                }
                case 16: {
                    this.mUsage = 12;
                    break;
                }
                case 0:
                case 1:
                case 2:
                case 3:
                case 4:
                case 5:
                case 6:
                case 7:
                case 8:
                case 9:
                case 10:
                case 11:
                case 12:
                case 13:
                case 14:
                case 15: {
                    this.mUsage = mUsage;
                    break;
                }
            }
            return this;
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.media;

import androidx.versionedparcelable.VersionedParcelable;

public interface AudioAttributesImpl extends VersionedParcelable
{
    Object getAudioAttributes();
    
    int getContentType();
    
    int getFlags();
    
    int getLegacyStreamType();
    
    int getRawLegacyStreamType();
    
    int getUsage();
    
    int getVolumeControlStream();
    
    public interface Builder
    {
        AudioAttributesImpl build();
        
        Builder setContentType(final int p0);
        
        Builder setFlags(final int p0);
        
        Builder setLegacyStreamType(final int p0);
        
        Builder setUsage(final int p0);
    }
}

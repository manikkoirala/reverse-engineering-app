// 
// Decompiled by Procyon v0.6.0
// 

package androidx.media;

import android.media.AudioFocusRequest;
import android.os.Build$VERSION;
import android.media.AudioManager;

public final class AudioManagerCompat
{
    public static final int AUDIOFOCUS_GAIN = 1;
    public static final int AUDIOFOCUS_GAIN_TRANSIENT = 2;
    public static final int AUDIOFOCUS_GAIN_TRANSIENT_EXCLUSIVE = 4;
    public static final int AUDIOFOCUS_GAIN_TRANSIENT_MAY_DUCK = 3;
    private static final String TAG = "AudioManCompat";
    
    private AudioManagerCompat() {
    }
    
    public static int abandonAudioFocusRequest(final AudioManager audioManager, final AudioFocusRequestCompat audioFocusRequestCompat) {
        if (audioManager == null) {
            throw new IllegalArgumentException("AudioManager must not be null");
        }
        if (audioFocusRequestCompat == null) {
            throw new IllegalArgumentException("AudioFocusRequestCompat must not be null");
        }
        if (Build$VERSION.SDK_INT >= 26) {
            return Api26Impl.abandonAudioFocusRequest(audioManager, audioFocusRequestCompat.getAudioFocusRequest());
        }
        return audioManager.abandonAudioFocus(audioFocusRequestCompat.getOnAudioFocusChangeListener());
    }
    
    public static int getStreamMaxVolume(final AudioManager audioManager, final int n) {
        return audioManager.getStreamMaxVolume(n);
    }
    
    public static int getStreamMinVolume(final AudioManager audioManager, final int n) {
        if (Build$VERSION.SDK_INT >= 28) {
            return Api28Impl.getStreamMinVolume(audioManager, n);
        }
        return 0;
    }
    
    public static boolean isVolumeFixed(final AudioManager audioManager) {
        return Build$VERSION.SDK_INT >= 21 && Api21Impl.isVolumeFixed(audioManager);
    }
    
    public static int requestAudioFocus(final AudioManager audioManager, final AudioFocusRequestCompat audioFocusRequestCompat) {
        if (audioManager == null) {
            throw new IllegalArgumentException("AudioManager must not be null");
        }
        if (audioFocusRequestCompat == null) {
            throw new IllegalArgumentException("AudioFocusRequestCompat must not be null");
        }
        if (Build$VERSION.SDK_INT >= 26) {
            return Api26Impl.requestAudioFocus(audioManager, audioFocusRequestCompat.getAudioFocusRequest());
        }
        return audioManager.requestAudioFocus(audioFocusRequestCompat.getOnAudioFocusChangeListener(), audioFocusRequestCompat.getAudioAttributesCompat().getLegacyStreamType(), audioFocusRequestCompat.getFocusGain());
    }
    
    private static class Api21Impl
    {
        static boolean isVolumeFixed(final AudioManager audioManager) {
            return audioManager.isVolumeFixed();
        }
    }
    
    private static class Api26Impl
    {
        static int abandonAudioFocusRequest(final AudioManager audioManager, final AudioFocusRequest audioFocusRequest) {
            return audioManager.abandonAudioFocusRequest(audioFocusRequest);
        }
        
        static int requestAudioFocus(final AudioManager audioManager, final AudioFocusRequest audioFocusRequest) {
            return audioManager.requestAudioFocus(audioFocusRequest);
        }
    }
    
    private static class Api28Impl
    {
        static int getStreamMinVolume(final AudioManager audioManager, final int n) {
            return audioManager.getStreamMinVolume(n);
        }
    }
}

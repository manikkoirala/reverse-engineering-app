// 
// Decompiled by Procyon v0.6.0
// 

package androidx.media;

import android.media.session.MediaSessionManager$RemoteUserInfo;
import android.content.Context;
import android.media.session.MediaSessionManager;

class MediaSessionManagerImplApi28 extends MediaSessionManagerImplApi21
{
    android.media.session.MediaSessionManager mObject;
    
    MediaSessionManagerImplApi28(final Context context) {
        super(context);
        this.mObject = (android.media.session.MediaSessionManager)context.getSystemService("media_session");
    }
    
    @Override
    public boolean isTrustedForMediaControl(final RemoteUserInfoImpl remoteUserInfoImpl) {
        return super.isTrustedForMediaControl(remoteUserInfoImpl);
    }
    
    static final class RemoteUserInfoImplApi28 extends RemoteUserInfoImplBase
    {
        final MediaSessionManager$RemoteUserInfo mObject;
        
        RemoteUserInfoImplApi28(final MediaSessionManager$RemoteUserInfo mObject) {
            super(mObject.getPackageName(), mObject.getPid(), mObject.getUid());
            this.mObject = mObject;
        }
        
        RemoteUserInfoImplApi28(final String s, final int n, final int n2) {
            super(s, n, n2);
            this.mObject = new MediaSessionManager$RemoteUserInfo(s, n, n2);
        }
        
        static String getPackageName(final MediaSessionManager$RemoteUserInfo mediaSessionManager$RemoteUserInfo) {
            return mediaSessionManager$RemoteUserInfo.getPackageName();
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.media;

import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;
import android.os.Build$VERSION;
import android.media.VolumeProvider;

public abstract class VolumeProviderCompat
{
    public static final int VOLUME_CONTROL_ABSOLUTE = 2;
    public static final int VOLUME_CONTROL_FIXED = 0;
    public static final int VOLUME_CONTROL_RELATIVE = 1;
    private Callback mCallback;
    private final String mControlId;
    private final int mControlType;
    private int mCurrentVolume;
    private final int mMaxVolume;
    private VolumeProvider mVolumeProviderFwk;
    
    public VolumeProviderCompat(final int n, final int n2, final int n3) {
        this(n, n2, n3, null);
    }
    
    public VolumeProviderCompat(final int mControlType, final int mMaxVolume, final int mCurrentVolume, final String mControlId) {
        this.mControlType = mControlType;
        this.mMaxVolume = mMaxVolume;
        this.mCurrentVolume = mCurrentVolume;
        this.mControlId = mControlId;
    }
    
    public final int getCurrentVolume() {
        return this.mCurrentVolume;
    }
    
    public final int getMaxVolume() {
        return this.mMaxVolume;
    }
    
    public final int getVolumeControl() {
        return this.mControlType;
    }
    
    public final String getVolumeControlId() {
        return this.mControlId;
    }
    
    public Object getVolumeProvider() {
        if (this.mVolumeProviderFwk == null) {
            if (Build$VERSION.SDK_INT >= 30) {
                this.mVolumeProviderFwk = new VolumeProvider(this, this.mControlType, this.mMaxVolume, this.mCurrentVolume, this.mControlId) {
                    final VolumeProviderCompat this$0;
                    
                    public void onAdjustVolume(final int n) {
                        this.this$0.onAdjustVolume(n);
                    }
                    
                    public void onSetVolumeTo(final int n) {
                        this.this$0.onSetVolumeTo(n);
                    }
                };
            }
            else if (Build$VERSION.SDK_INT >= 21) {
                this.mVolumeProviderFwk = new VolumeProvider(this, this.mControlType, this.mMaxVolume, this.mCurrentVolume) {
                    final VolumeProviderCompat this$0;
                    
                    public void onAdjustVolume(final int n) {
                        this.this$0.onAdjustVolume(n);
                    }
                    
                    public void onSetVolumeTo(final int n) {
                        this.this$0.onSetVolumeTo(n);
                    }
                };
            }
        }
        return this.mVolumeProviderFwk;
    }
    
    public void onAdjustVolume(final int n) {
    }
    
    public void onSetVolumeTo(final int n) {
    }
    
    public void setCallback(final Callback mCallback) {
        this.mCallback = mCallback;
    }
    
    public final void setCurrentVolume(final int mCurrentVolume) {
        this.mCurrentVolume = mCurrentVolume;
        if (Build$VERSION.SDK_INT >= 21) {
            Api21Impl.setCurrentVolume((VolumeProvider)this.getVolumeProvider(), mCurrentVolume);
        }
        final Callback mCallback = this.mCallback;
        if (mCallback != null) {
            mCallback.onVolumeChanged(this);
        }
    }
    
    private static class Api21Impl
    {
        static void setCurrentVolume(final VolumeProvider volumeProvider, final int currentVolume) {
            volumeProvider.setCurrentVolume(currentVolume);
        }
    }
    
    public abstract static class Callback
    {
        public abstract void onVolumeChanged(final VolumeProviderCompat p0);
    }
    
    @Retention(RetentionPolicy.SOURCE)
    public @interface ControlType {
    }
}

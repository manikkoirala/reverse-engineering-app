// 
// Decompiled by Procyon v0.6.0
// 

package androidx.media;

public final class R
{
    public static final class color
    {
        public static final int notification_material_background_media_default_color = 2131100418;
        public static final int primary_text_default_material_dark = 2131100426;
        public static final int secondary_text_default_material_dark = 2131100584;
    }
    
    public static final class id
    {
        public static final int action0 = 2131361863;
        public static final int action_divider = 2131361879;
        public static final int cancel_action = 2131361985;
        public static final int chronometer = 2131362002;
        public static final int end_padder = 2131362087;
        public static final int icon = 2131362151;
        public static final int info = 2131362170;
        public static final int line1 = 2131362184;
        public static final int line3 = 2131362185;
        public static final int media_actions = 2131362227;
        public static final int media_controller_compat_view_tag = 2131362228;
        public static final int notification_main_column = 2131362293;
        public static final int notification_main_column_container = 2131362294;
        public static final int right_side = 2131362947;
        public static final int status_bar_latest_event_content = 2131363043;
        public static final int text = 2131363074;
        public static final int text2 = 2131363075;
        public static final int time = 2131363097;
        public static final int title = 2131363098;
    }
    
    public static final class integer
    {
        public static final int cancel_button_image_alpha = 2131427332;
    }
    
    public static final class layout
    {
        public static final int notification_media_action = 2131558558;
        public static final int notification_media_cancel_action = 2131558559;
        public static final int notification_template_big_media = 2131558560;
        public static final int notification_template_big_media_custom = 2131558561;
        public static final int notification_template_big_media_narrow = 2131558562;
        public static final int notification_template_big_media_narrow_custom = 2131558563;
        public static final int notification_template_lines_media = 2131558566;
        public static final int notification_template_media = 2131558567;
        public static final int notification_template_media_custom = 2131558568;
    }
    
    public static final class style
    {
        public static final int TextAppearance_Compat_Notification_Info_Media = 2131952230;
        public static final int TextAppearance_Compat_Notification_Line2_Media = 2131952232;
        public static final int TextAppearance_Compat_Notification_Media = 2131952233;
        public static final int TextAppearance_Compat_Notification_Time_Media = 2131952235;
        public static final int TextAppearance_Compat_Notification_Title_Media = 2131952237;
    }
}

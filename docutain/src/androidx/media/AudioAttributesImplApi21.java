// 
// Decompiled by Procyon v0.6.0
// 

package androidx.media;

import android.media.AudioAttributes$Builder;
import android.media.AudioAttributes;

public class AudioAttributesImplApi21 implements AudioAttributesImpl
{
    public AudioAttributes mAudioAttributes;
    public int mLegacyStreamType;
    
    public AudioAttributesImplApi21() {
        this.mLegacyStreamType = -1;
    }
    
    AudioAttributesImplApi21(final AudioAttributes audioAttributes) {
        this(audioAttributes, -1);
    }
    
    AudioAttributesImplApi21(final AudioAttributes mAudioAttributes, final int mLegacyStreamType) {
        this.mAudioAttributes = mAudioAttributes;
        this.mLegacyStreamType = mLegacyStreamType;
    }
    
    @Override
    public boolean equals(final Object o) {
        return o instanceof AudioAttributesImplApi21 && this.mAudioAttributes.equals((Object)((AudioAttributesImplApi21)o).mAudioAttributes);
    }
    
    @Override
    public Object getAudioAttributes() {
        return this.mAudioAttributes;
    }
    
    @Override
    public int getContentType() {
        return this.mAudioAttributes.getContentType();
    }
    
    @Override
    public int getFlags() {
        return this.mAudioAttributes.getFlags();
    }
    
    @Override
    public int getLegacyStreamType() {
        final int mLegacyStreamType = this.mLegacyStreamType;
        if (mLegacyStreamType != -1) {
            return mLegacyStreamType;
        }
        return AudioAttributesCompat.toVolumeStreamType(false, this.getFlags(), this.getUsage());
    }
    
    @Override
    public int getRawLegacyStreamType() {
        return this.mLegacyStreamType;
    }
    
    @Override
    public int getUsage() {
        return this.mAudioAttributes.getUsage();
    }
    
    @Override
    public int getVolumeControlStream() {
        return AudioAttributesCompat.toVolumeStreamType(true, this.getFlags(), this.getUsage());
    }
    
    @Override
    public int hashCode() {
        return this.mAudioAttributes.hashCode();
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("AudioAttributesCompat: audioattributes=");
        sb.append(this.mAudioAttributes);
        return sb.toString();
    }
    
    static class Builder implements AudioAttributesImpl.Builder
    {
        final AudioAttributes$Builder mFwkBuilder;
        
        Builder() {
            this.mFwkBuilder = new AudioAttributes$Builder();
        }
        
        Builder(final Object o) {
            this.mFwkBuilder = new AudioAttributes$Builder((AudioAttributes)o);
        }
        
        @Override
        public AudioAttributesImpl build() {
            return new AudioAttributesImplApi21(this.mFwkBuilder.build());
        }
        
        public Builder setContentType(final int contentType) {
            this.mFwkBuilder.setContentType(contentType);
            return this;
        }
        
        public Builder setFlags(final int flags) {
            this.mFwkBuilder.setFlags(flags);
            return this;
        }
        
        public Builder setLegacyStreamType(final int legacyStreamType) {
            this.mFwkBuilder.setLegacyStreamType(legacyStreamType);
            return this;
        }
        
        public Builder setUsage(final int n) {
            int usage = n;
            if (n == 16) {
                usage = 12;
            }
            this.mFwkBuilder.setUsage(usage);
            return this;
        }
    }
}

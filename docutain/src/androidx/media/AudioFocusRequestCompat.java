// 
// Decompiled by Procyon v0.6.0
// 

package androidx.media;

import android.os.Message;
import android.os.Handler$Callback;
import android.media.AudioFocusRequest$Builder;
import android.media.AudioFocusRequest;
import android.media.AudioAttributes;
import androidx.core.util.ObjectsCompat;
import android.os.Looper;
import android.os.Build$VERSION;
import android.media.AudioManager$OnAudioFocusChangeListener;
import android.os.Handler;

public class AudioFocusRequestCompat
{
    static final AudioAttributesCompat FOCUS_DEFAULT_ATTR;
    private final AudioAttributesCompat mAudioAttributesCompat;
    private final Handler mFocusChangeHandler;
    private final int mFocusGain;
    private final Object mFrameworkAudioFocusRequest;
    private final AudioManager$OnAudioFocusChangeListener mOnAudioFocusChangeListener;
    private final boolean mPauseOnDuck;
    
    static {
        FOCUS_DEFAULT_ATTR = new AudioAttributesCompat.Builder().setUsage(1).build();
    }
    
    AudioFocusRequestCompat(final int mFocusGain, final AudioManager$OnAudioFocusChangeListener mOnAudioFocusChangeListener, final Handler mFocusChangeHandler, final AudioAttributesCompat mAudioAttributesCompat, final boolean mPauseOnDuck) {
        this.mFocusGain = mFocusGain;
        this.mFocusChangeHandler = mFocusChangeHandler;
        this.mAudioAttributesCompat = mAudioAttributesCompat;
        this.mPauseOnDuck = mPauseOnDuck;
        if (Build$VERSION.SDK_INT < 26 && mFocusChangeHandler.getLooper() != Looper.getMainLooper()) {
            this.mOnAudioFocusChangeListener = (AudioManager$OnAudioFocusChangeListener)new OnAudioFocusChangeListenerHandlerCompat(mOnAudioFocusChangeListener, mFocusChangeHandler);
        }
        else {
            this.mOnAudioFocusChangeListener = mOnAudioFocusChangeListener;
        }
        if (Build$VERSION.SDK_INT >= 26) {
            this.mFrameworkAudioFocusRequest = Api26Impl.createInstance(mFocusGain, this.getAudioAttributes(), mPauseOnDuck, this.mOnAudioFocusChangeListener, mFocusChangeHandler);
        }
        else {
            this.mFrameworkAudioFocusRequest = null;
        }
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (this == o) {
            return true;
        }
        if (!(o instanceof AudioFocusRequestCompat)) {
            return false;
        }
        final AudioFocusRequestCompat audioFocusRequestCompat = (AudioFocusRequestCompat)o;
        if (this.mFocusGain != audioFocusRequestCompat.mFocusGain || this.mPauseOnDuck != audioFocusRequestCompat.mPauseOnDuck || !ObjectsCompat.equals(this.mOnAudioFocusChangeListener, audioFocusRequestCompat.mOnAudioFocusChangeListener) || !ObjectsCompat.equals(this.mFocusChangeHandler, audioFocusRequestCompat.mFocusChangeHandler) || !ObjectsCompat.equals(this.mAudioAttributesCompat, audioFocusRequestCompat.mAudioAttributesCompat)) {
            b = false;
        }
        return b;
    }
    
    AudioAttributes getAudioAttributes() {
        final AudioAttributesCompat mAudioAttributesCompat = this.mAudioAttributesCompat;
        AudioAttributes audioAttributes;
        if (mAudioAttributesCompat != null) {
            audioAttributes = (AudioAttributes)mAudioAttributesCompat.unwrap();
        }
        else {
            audioAttributes = null;
        }
        return audioAttributes;
    }
    
    public AudioAttributesCompat getAudioAttributesCompat() {
        return this.mAudioAttributesCompat;
    }
    
    AudioFocusRequest getAudioFocusRequest() {
        return (AudioFocusRequest)this.mFrameworkAudioFocusRequest;
    }
    
    public Handler getFocusChangeHandler() {
        return this.mFocusChangeHandler;
    }
    
    public int getFocusGain() {
        return this.mFocusGain;
    }
    
    public AudioManager$OnAudioFocusChangeListener getOnAudioFocusChangeListener() {
        return this.mOnAudioFocusChangeListener;
    }
    
    @Override
    public int hashCode() {
        return ObjectsCompat.hash(this.mFocusGain, this.mOnAudioFocusChangeListener, this.mFocusChangeHandler, this.mAudioAttributesCompat, this.mPauseOnDuck);
    }
    
    public boolean willPauseWhenDucked() {
        return this.mPauseOnDuck;
    }
    
    private static class Api26Impl
    {
        static AudioFocusRequest createInstance(final int n, final AudioAttributes audioAttributes, final boolean willPauseWhenDucked, final AudioManager$OnAudioFocusChangeListener audioManager$OnAudioFocusChangeListener, final Handler handler) {
            return new AudioFocusRequest$Builder(n).setAudioAttributes(audioAttributes).setWillPauseWhenDucked(willPauseWhenDucked).setOnAudioFocusChangeListener(audioManager$OnAudioFocusChangeListener, handler).build();
        }
    }
    
    public static final class Builder
    {
        private AudioAttributesCompat mAudioAttributesCompat;
        private Handler mFocusChangeHandler;
        private int mFocusGain;
        private AudioManager$OnAudioFocusChangeListener mOnAudioFocusChangeListener;
        private boolean mPauseOnDuck;
        
        public Builder(final int focusGain) {
            this.mAudioAttributesCompat = AudioFocusRequestCompat.FOCUS_DEFAULT_ATTR;
            this.setFocusGain(focusGain);
        }
        
        public Builder(final AudioFocusRequestCompat audioFocusRequestCompat) {
            this.mAudioAttributesCompat = AudioFocusRequestCompat.FOCUS_DEFAULT_ATTR;
            if (audioFocusRequestCompat != null) {
                this.mFocusGain = audioFocusRequestCompat.getFocusGain();
                this.mOnAudioFocusChangeListener = audioFocusRequestCompat.getOnAudioFocusChangeListener();
                this.mFocusChangeHandler = audioFocusRequestCompat.getFocusChangeHandler();
                this.mAudioAttributesCompat = audioFocusRequestCompat.getAudioAttributesCompat();
                this.mPauseOnDuck = audioFocusRequestCompat.willPauseWhenDucked();
                return;
            }
            throw new IllegalArgumentException("AudioFocusRequestCompat to copy must not be null");
        }
        
        private static boolean isValidFocusGain(final int n) {
            return n == 1 || n == 2 || n == 3 || n == 4;
        }
        
        public AudioFocusRequestCompat build() {
            if (this.mOnAudioFocusChangeListener != null) {
                return new AudioFocusRequestCompat(this.mFocusGain, this.mOnAudioFocusChangeListener, this.mFocusChangeHandler, this.mAudioAttributesCompat, this.mPauseOnDuck);
            }
            throw new IllegalStateException("Can't build an AudioFocusRequestCompat instance without a listener");
        }
        
        public Builder setAudioAttributes(final AudioAttributesCompat mAudioAttributesCompat) {
            if (mAudioAttributesCompat != null) {
                this.mAudioAttributesCompat = mAudioAttributesCompat;
                return this;
            }
            throw new NullPointerException("Illegal null AudioAttributes");
        }
        
        public Builder setFocusGain(final int i) {
            if (isValidFocusGain(i)) {
                int mFocusGain = i;
                if (Build$VERSION.SDK_INT < 19 && (mFocusGain = i) == 4) {
                    mFocusGain = 2;
                }
                this.mFocusGain = mFocusGain;
                return this;
            }
            final StringBuilder sb = new StringBuilder();
            sb.append("Illegal audio focus gain type ");
            sb.append(i);
            throw new IllegalArgumentException(sb.toString());
        }
        
        public Builder setOnAudioFocusChangeListener(final AudioManager$OnAudioFocusChangeListener audioManager$OnAudioFocusChangeListener) {
            return this.setOnAudioFocusChangeListener(audioManager$OnAudioFocusChangeListener, new Handler(Looper.getMainLooper()));
        }
        
        public Builder setOnAudioFocusChangeListener(final AudioManager$OnAudioFocusChangeListener mOnAudioFocusChangeListener, final Handler mFocusChangeHandler) {
            if (mOnAudioFocusChangeListener == null) {
                throw new IllegalArgumentException("OnAudioFocusChangeListener must not be null");
            }
            if (mFocusChangeHandler != null) {
                this.mOnAudioFocusChangeListener = mOnAudioFocusChangeListener;
                this.mFocusChangeHandler = mFocusChangeHandler;
                return this;
            }
            throw new IllegalArgumentException("Handler must not be null");
        }
        
        public Builder setWillPauseWhenDucked(final boolean mPauseOnDuck) {
            this.mPauseOnDuck = mPauseOnDuck;
            return this;
        }
    }
    
    private static class OnAudioFocusChangeListenerHandlerCompat implements Handler$Callback, AudioManager$OnAudioFocusChangeListener
    {
        private static final int FOCUS_CHANGE = 2782386;
        private final Handler mHandler;
        private final AudioManager$OnAudioFocusChangeListener mListener;
        
        OnAudioFocusChangeListenerHandlerCompat(final AudioManager$OnAudioFocusChangeListener mListener, final Handler handler) {
            this.mListener = mListener;
            this.mHandler = new Handler(handler.getLooper(), (Handler$Callback)this);
        }
        
        public boolean handleMessage(final Message message) {
            if (message.what == 2782386) {
                this.mListener.onAudioFocusChange(message.arg1);
                return true;
            }
            return false;
        }
        
        public void onAudioFocusChange(final int n) {
            final Handler mHandler = this.mHandler;
            mHandler.sendMessage(Message.obtain(mHandler, 2782386, n, 0));
        }
    }
}

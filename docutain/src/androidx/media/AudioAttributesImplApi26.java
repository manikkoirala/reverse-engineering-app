// 
// Decompiled by Procyon v0.6.0
// 

package androidx.media;

import android.media.AudioAttributes;

public class AudioAttributesImplApi26 extends AudioAttributesImplApi21
{
    public AudioAttributesImplApi26() {
    }
    
    AudioAttributesImplApi26(final AudioAttributes audioAttributes) {
        super(audioAttributes, -1);
    }
    
    @Override
    public int getVolumeControlStream() {
        return this.mAudioAttributes.getVolumeControlStream();
    }
    
    static class Builder extends AudioAttributesImplApi21.Builder
    {
        Builder() {
        }
        
        Builder(final Object o) {
            super(o);
        }
        
        @Override
        public AudioAttributesImpl build() {
            return new AudioAttributesImplApi26(this.mFwkBuilder.build());
        }
        
        public Builder setUsage(final int usage) {
            this.mFwkBuilder.setUsage(usage);
            return this;
        }
    }
}

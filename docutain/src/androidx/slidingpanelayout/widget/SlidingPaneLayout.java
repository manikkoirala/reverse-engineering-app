// 
// Decompiled by Procyon v0.6.0
// 

package androidx.slidingpanelayout.widget;

import android.widget.FrameLayout;
import android.os.Parcel;
import android.os.Parcelable$ClassLoaderCreator;
import android.os.Parcelable$Creator;
import androidx.customview.view.AbsSavedState;
import android.content.res.TypedArray;
import android.graphics.Paint;
import android.view.ViewParent;
import android.view.accessibility.AccessibilityEvent;
import androidx.core.view.accessibility.AccessibilityNodeInfoCompat;
import android.os.Parcelable;
import android.view.MotionEvent;
import android.util.Log;
import android.view.ViewGroup$MarginLayoutParams;
import android.graphics.Canvas;
import java.util.Iterator;
import android.view.ViewGroup$LayoutParams;
import java.util.Collection;
import java.util.Arrays;
import android.view.View$MeasureSpec;
import androidx.core.view.WindowInsetsCompat;
import androidx.core.graphics.Insets;
import android.content.ContextWrapper;
import android.app.Activity;
import androidx.core.content.ContextCompat;
import androidx.window.layout.WindowInfoTracker$_CC;
import androidx.core.view.AccessibilityDelegateCompat;
import androidx.core.view.ViewCompat;
import androidx.transition.Transition;
import androidx.transition.TransitionManager;
import android.animation.TimeInterpolator;
import androidx.core.view.animation.PathInterpolatorCompat;
import androidx.transition.ChangeBounds;
import java.util.concurrent.CopyOnWriteArrayList;
import android.util.AttributeSet;
import android.content.Context;
import android.os.Build$VERSION;
import android.graphics.Rect;
import android.view.View;
import android.graphics.drawable.Drawable;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.lang.reflect.Method;
import androidx.window.layout.FoldingFeature;
import androidx.customview.widget.ViewDragHelper;
import androidx.customview.widget.Openable;
import android.view.ViewGroup;

public class SlidingPaneLayout extends ViewGroup implements Openable
{
    private static final String ACCESSIBILITY_CLASS_NAME = "androidx.slidingpanelayout.widget.SlidingPaneLayout";
    public static final int LOCK_MODE_LOCKED = 3;
    public static final int LOCK_MODE_LOCKED_CLOSED = 2;
    public static final int LOCK_MODE_LOCKED_OPEN = 1;
    public static final int LOCK_MODE_UNLOCKED = 0;
    private static final int MIN_FLING_VELOCITY = 400;
    private static final String TAG = "SlidingPaneLayout";
    private static boolean sEdgeSizeUsingSystemGestureInsets;
    private boolean mCanSlide;
    private int mCoveredFadeColor;
    private boolean mDisplayListReflectionLoaded;
    final ViewDragHelper mDragHelper;
    private boolean mFirstLayout;
    FoldingFeature mFoldingFeature;
    private FoldingFeatureObserver mFoldingFeatureObserver;
    private Method mGetDisplayList;
    private float mInitialMotionX;
    private float mInitialMotionY;
    boolean mIsUnableToDrag;
    private int mLockMode;
    private FoldingFeatureObserver.OnFoldingFeatureChangeListener mOnFoldingFeatureChangeListener;
    private PanelSlideListener mPanelSlideListener;
    private final List<PanelSlideListener> mPanelSlideListeners;
    private int mParallaxBy;
    private float mParallaxOffset;
    final ArrayList<DisableLayerRunnable> mPostedRunnables;
    boolean mPreservedOpenState;
    private Field mRecreateDisplayList;
    private Drawable mShadowDrawableLeft;
    private Drawable mShadowDrawableRight;
    float mSlideOffset;
    int mSlideRange;
    View mSlideableView;
    private int mSliderFadeColor;
    private final Rect mTmpRect;
    
    static {
        SlidingPaneLayout.sEdgeSizeUsingSystemGestureInsets = (Build$VERSION.SDK_INT >= 29);
    }
    
    public SlidingPaneLayout(final Context context) {
        this(context, null);
    }
    
    public SlidingPaneLayout(final Context context, final AttributeSet set) {
        this(context, set, 0);
    }
    
    public SlidingPaneLayout(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
        this.mSliderFadeColor = 0;
        this.mSlideOffset = 1.0f;
        this.mPanelSlideListeners = new CopyOnWriteArrayList<PanelSlideListener>();
        this.mFirstLayout = true;
        this.mTmpRect = new Rect();
        this.mPostedRunnables = new ArrayList<DisableLayerRunnable>();
        this.mOnFoldingFeatureChangeListener = new FoldingFeatureObserver.OnFoldingFeatureChangeListener() {
            final SlidingPaneLayout this$0;
            
            @Override
            public void onFoldingFeatureChange(final FoldingFeature mFoldingFeature) {
                this.this$0.mFoldingFeature = mFoldingFeature;
                final ChangeBounds changeBounds = new ChangeBounds();
                changeBounds.setDuration(300L);
                changeBounds.setInterpolator((TimeInterpolator)PathInterpolatorCompat.create(0.2f, 0.0f, 0.0f, 1.0f));
                TransitionManager.beginDelayedTransition(this.this$0, changeBounds);
                this.this$0.requestLayout();
            }
        };
        final float density = context.getResources().getDisplayMetrics().density;
        this.setWillNotDraw(false);
        ViewCompat.setAccessibilityDelegate((View)this, new AccessibilityDelegate());
        ViewCompat.setImportantForAccessibility((View)this, 1);
        (this.mDragHelper = ViewDragHelper.create(this, 0.5f, (ViewDragHelper.Callback)new DragHelperCallback())).setMinVelocity(density * 400.0f);
        this.setFoldingFeatureObserver(new FoldingFeatureObserver(WindowInfoTracker$_CC.getOrCreate(context), ContextCompat.getMainExecutor(context)));
    }
    
    private boolean closePane(final int n) {
        if (!this.mCanSlide) {
            this.mPreservedOpenState = false;
        }
        if (!this.mFirstLayout && !this.smoothSlideTo(1.0f, n)) {
            return false;
        }
        this.mPreservedOpenState = false;
        return true;
    }
    
    private static Activity getActivityOrNull(Context baseContext) {
        while (baseContext instanceof ContextWrapper) {
            if (baseContext instanceof Activity) {
                return (Activity)baseContext;
            }
            baseContext = ((ContextWrapper)baseContext).getBaseContext();
        }
        return null;
    }
    
    private static Rect getFoldBoundsInView(final FoldingFeature foldingFeature, final View view) {
        final int[] array = new int[2];
        view.getLocationInWindow(array);
        final int n = array[0];
        final Rect rect = new Rect(n, array[1], view.getWidth() + n, array[1] + view.getWidth());
        final Rect rect2 = new Rect(foldingFeature.getBounds());
        final boolean intersect = rect2.intersect(rect);
        if ((rect2.width() == 0 && rect2.height() == 0) || !intersect) {
            return null;
        }
        rect2.offset(-array[0], -array[1]);
        return rect2;
    }
    
    private static int getMinimumWidth(final View view) {
        if (view instanceof TouchBlocker) {
            return ViewCompat.getMinimumWidth(((TouchBlocker)view).getChildAt(0));
        }
        return ViewCompat.getMinimumWidth(view);
    }
    
    private Insets getSystemGestureInsets() {
        if (SlidingPaneLayout.sEdgeSizeUsingSystemGestureInsets) {
            final WindowInsetsCompat rootWindowInsets = ViewCompat.getRootWindowInsets((View)this);
            if (rootWindowInsets != null) {
                return rootWindowInsets.getSystemGestureInsets();
            }
        }
        return null;
    }
    
    private static int measureChildHeight(final View view, int n, final int n2) {
        final LayoutParams layoutParams = (LayoutParams)view.getLayoutParams();
        if (layoutParams.width == 0 && layoutParams.weight > 0.0f) {
            n = getChildMeasureSpec(n, n2, layoutParams.height);
        }
        else {
            n = View$MeasureSpec.makeMeasureSpec(view.getMeasuredHeight(), 1073741824);
        }
        return n;
    }
    
    private boolean openPane(final int n) {
        if (!this.mCanSlide) {
            this.mPreservedOpenState = true;
        }
        return (this.mFirstLayout || this.smoothSlideTo(0.0f, n)) && (this.mPreservedOpenState = true);
    }
    
    private void parallaxOtherViews(final float mParallaxOffset) {
        final boolean layoutRtlSupport = this.isLayoutRtlSupport();
        for (int childCount = this.getChildCount(), i = 0; i < childCount; ++i) {
            final View child = this.getChildAt(i);
            if (child != this.mSlideableView) {
                final float mParallaxOffset2 = this.mParallaxOffset;
                final int mParallaxBy = this.mParallaxBy;
                final int n = (int)((1.0f - mParallaxOffset2) * mParallaxBy);
                this.mParallaxOffset = mParallaxOffset;
                int n2 = n - (int)((1.0f - mParallaxOffset) * mParallaxBy);
                if (layoutRtlSupport) {
                    n2 = -n2;
                }
                child.offsetLeftAndRight(n2);
            }
        }
    }
    
    private void setFoldingFeatureObserver(final FoldingFeatureObserver mFoldingFeatureObserver) {
        (this.mFoldingFeatureObserver = mFoldingFeatureObserver).setOnFoldingFeatureChangeListener(this.mOnFoldingFeatureChangeListener);
    }
    
    private ArrayList<Rect> splitViewPositions() {
        final FoldingFeature mFoldingFeature = this.mFoldingFeature;
        if (mFoldingFeature != null) {
            if (mFoldingFeature.isSeparating()) {
                if (this.mFoldingFeature.getBounds().left == 0) {
                    return null;
                }
                if (this.mFoldingFeature.getBounds().top == 0) {
                    final Rect foldBoundsInView = getFoldBoundsInView(this.mFoldingFeature, (View)this);
                    if (foldBoundsInView == null) {
                        return null;
                    }
                    final Rect rect = new Rect(this.getPaddingLeft(), this.getPaddingTop(), Math.max(this.getPaddingLeft(), foldBoundsInView.left), this.getHeight() - this.getPaddingBottom());
                    final int a = this.getWidth() - this.getPaddingRight();
                    return new ArrayList<Rect>(Arrays.asList(rect, new Rect(Math.min(a, foldBoundsInView.right), this.getPaddingTop(), a, this.getHeight() - this.getPaddingBottom())));
                }
            }
        }
        return null;
    }
    
    private static boolean viewIsOpaque(final View view) {
        final boolean opaque = view.isOpaque();
        boolean b = true;
        if (opaque) {
            return true;
        }
        if (Build$VERSION.SDK_INT >= 18) {
            return false;
        }
        final Drawable background = view.getBackground();
        if (background != null) {
            if (background.getOpacity() != -1) {
                b = false;
            }
            return b;
        }
        return false;
    }
    
    public void addPanelSlideListener(final PanelSlideListener panelSlideListener) {
        this.mPanelSlideListeners.add(panelSlideListener);
    }
    
    public void addView(final View view, final int n, final ViewGroup$LayoutParams viewGroup$LayoutParams) {
        if (this.getChildCount() == 1) {
            super.addView((View)new TouchBlocker(view), n, viewGroup$LayoutParams);
            return;
        }
        super.addView(view, n, viewGroup$LayoutParams);
    }
    
    protected boolean canScroll(final View view, final boolean b, int n, final int n2, final int n3) {
        final boolean b2 = view instanceof ViewGroup;
        final boolean b3 = true;
        if (b2) {
            final ViewGroup viewGroup = (ViewGroup)view;
            final int scrollX = view.getScrollX();
            final int scrollY = view.getScrollY();
            for (int i = viewGroup.getChildCount() - 1; i >= 0; --i) {
                final View child = viewGroup.getChildAt(i);
                final int n4 = n2 + scrollX;
                if (n4 >= child.getLeft() && n4 < child.getRight()) {
                    final int n5 = n3 + scrollY;
                    if (n5 >= child.getTop() && n5 < child.getBottom() && this.canScroll(child, true, n, n4 - child.getLeft(), n5 - child.getTop())) {
                        return true;
                    }
                }
            }
        }
        if (b) {
            if (!this.isLayoutRtlSupport()) {
                n = -n;
            }
            if (view.canScrollHorizontally(n)) {
                return b3;
            }
        }
        return false;
    }
    
    @Deprecated
    public boolean canSlide() {
        return this.mCanSlide;
    }
    
    protected boolean checkLayoutParams(final ViewGroup$LayoutParams viewGroup$LayoutParams) {
        return viewGroup$LayoutParams instanceof LayoutParams && super.checkLayoutParams(viewGroup$LayoutParams);
    }
    
    public void close() {
        this.closePane();
    }
    
    public boolean closePane() {
        return this.closePane(0);
    }
    
    public void computeScroll() {
        if (this.mDragHelper.continueSettling(true)) {
            if (!this.mCanSlide) {
                this.mDragHelper.abort();
                return;
            }
            ViewCompat.postInvalidateOnAnimation((View)this);
        }
    }
    
    void dispatchOnPanelClosed(final View view) {
        final Iterator<PanelSlideListener> iterator = this.mPanelSlideListeners.iterator();
        while (iterator.hasNext()) {
            iterator.next().onPanelClosed(view);
        }
        this.sendAccessibilityEvent(32);
    }
    
    void dispatchOnPanelOpened(final View view) {
        final Iterator<PanelSlideListener> iterator = this.mPanelSlideListeners.iterator();
        while (iterator.hasNext()) {
            iterator.next().onPanelOpened(view);
        }
        this.sendAccessibilityEvent(32);
    }
    
    void dispatchOnPanelSlide(final View view) {
        final Iterator<PanelSlideListener> iterator = this.mPanelSlideListeners.iterator();
        while (iterator.hasNext()) {
            iterator.next().onPanelSlide(view, this.mSlideOffset);
        }
    }
    
    public void draw(final Canvas canvas) {
        super.draw(canvas);
        Drawable drawable;
        if (this.isLayoutRtlSupport()) {
            drawable = this.mShadowDrawableRight;
        }
        else {
            drawable = this.mShadowDrawableLeft;
        }
        View child;
        if (this.getChildCount() > 1) {
            child = this.getChildAt(1);
        }
        else {
            child = null;
        }
        if (child != null) {
            if (drawable != null) {
                final int top = child.getTop();
                final int bottom = child.getBottom();
                final int intrinsicWidth = drawable.getIntrinsicWidth();
                int right;
                int n;
                if (this.isLayoutRtlSupport()) {
                    right = child.getRight();
                    n = intrinsicWidth + right;
                }
                else {
                    final int left;
                    final int n2 = (left = child.getLeft()) - intrinsicWidth;
                    n = left;
                    right = n2;
                }
                drawable.setBounds(right, top, n, bottom);
                drawable.draw(canvas);
            }
        }
    }
    
    protected boolean drawChild(final Canvas canvas, final View view, final long n) {
        if (this.isLayoutRtlSupport() ^ this.isOpen()) {
            this.mDragHelper.setEdgeTrackingEnabled(1);
            final Insets systemGestureInsets = this.getSystemGestureInsets();
            if (systemGestureInsets != null) {
                final ViewDragHelper mDragHelper = this.mDragHelper;
                mDragHelper.setEdgeSize(Math.max(mDragHelper.getDefaultEdgeSize(), systemGestureInsets.left));
            }
        }
        else {
            this.mDragHelper.setEdgeTrackingEnabled(2);
            final Insets systemGestureInsets2 = this.getSystemGestureInsets();
            if (systemGestureInsets2 != null) {
                final ViewDragHelper mDragHelper2 = this.mDragHelper;
                mDragHelper2.setEdgeSize(Math.max(mDragHelper2.getDefaultEdgeSize(), systemGestureInsets2.right));
            }
        }
        final LayoutParams layoutParams = (LayoutParams)view.getLayoutParams();
        final int save = canvas.save();
        if (this.mCanSlide && !layoutParams.slideable && this.mSlideableView != null) {
            canvas.getClipBounds(this.mTmpRect);
            if (this.isLayoutRtlSupport()) {
                final Rect mTmpRect = this.mTmpRect;
                mTmpRect.left = Math.max(mTmpRect.left, this.mSlideableView.getRight());
            }
            else {
                final Rect mTmpRect2 = this.mTmpRect;
                mTmpRect2.right = Math.min(mTmpRect2.right, this.mSlideableView.getLeft());
            }
            canvas.clipRect(this.mTmpRect);
        }
        final boolean drawChild = super.drawChild(canvas, view, n);
        canvas.restoreToCount(save);
        return drawChild;
    }
    
    protected ViewGroup$LayoutParams generateDefaultLayoutParams() {
        return (ViewGroup$LayoutParams)new LayoutParams();
    }
    
    public ViewGroup$LayoutParams generateLayoutParams(final AttributeSet set) {
        return (ViewGroup$LayoutParams)new LayoutParams(this.getContext(), set);
    }
    
    protected ViewGroup$LayoutParams generateLayoutParams(final ViewGroup$LayoutParams viewGroup$LayoutParams) {
        LayoutParams layoutParams;
        if (viewGroup$LayoutParams instanceof ViewGroup$MarginLayoutParams) {
            layoutParams = new LayoutParams((ViewGroup$MarginLayoutParams)viewGroup$LayoutParams);
        }
        else {
            layoutParams = new LayoutParams(viewGroup$LayoutParams);
        }
        return (ViewGroup$LayoutParams)layoutParams;
    }
    
    @Deprecated
    public int getCoveredFadeColor() {
        return this.mCoveredFadeColor;
    }
    
    public final int getLockMode() {
        return this.mLockMode;
    }
    
    public int getParallaxDistance() {
        return this.mParallaxBy;
    }
    
    @Deprecated
    public int getSliderFadeColor() {
        return this.mSliderFadeColor;
    }
    
    void invalidateChildRegion(final View view) {
        if (Build$VERSION.SDK_INT >= 17) {
            ViewCompat.setLayerPaint(view, ((LayoutParams)view.getLayoutParams()).dimPaint);
            return;
        }
        Label_0172: {
            if (Build$VERSION.SDK_INT >= 16) {
                if (!this.mDisplayListReflectionLoaded) {
                    try {
                        final Class[] array = null;
                        this.mGetDisplayList = View.class.getDeclaredMethod("getDisplayList", (Class<?>[])null);
                    }
                    catch (final NoSuchMethodException ex) {
                        Log.e("SlidingPaneLayout", "Couldn't fetch getDisplayList method; dimming won't work right.", (Throwable)ex);
                    }
                    try {
                        (this.mRecreateDisplayList = View.class.getDeclaredField("mRecreateDisplayList")).setAccessible(true);
                    }
                    catch (final NoSuchFieldException ex2) {
                        Log.e("SlidingPaneLayout", "Couldn't fetch mRecreateDisplayList field; dimming will be slow.", (Throwable)ex2);
                    }
                    this.mDisplayListReflectionLoaded = true;
                }
                if (this.mGetDisplayList != null) {
                    final Field mRecreateDisplayList = this.mRecreateDisplayList;
                    if (mRecreateDisplayList != null) {
                        try {
                            mRecreateDisplayList.setBoolean(view, true);
                            final Method mGetDisplayList = this.mGetDisplayList;
                            final Object[] array2 = null;
                            mGetDisplayList.invoke(view, (Object[])null);
                        }
                        catch (final Exception ex3) {
                            Log.e("SlidingPaneLayout", "Error refreshing display list state", (Throwable)ex3);
                        }
                        break Label_0172;
                    }
                }
                view.invalidate();
                return;
            }
        }
        ViewCompat.postInvalidateOnAnimation((View)this, view.getLeft(), view.getTop(), view.getRight(), view.getBottom());
    }
    
    boolean isDimmed(final View view) {
        final boolean b = false;
        if (view == null) {
            return false;
        }
        final LayoutParams layoutParams = (LayoutParams)view.getLayoutParams();
        boolean b2 = b;
        if (this.mCanSlide) {
            b2 = b;
            if (layoutParams.dimWhenOffset) {
                b2 = b;
                if (this.mSlideOffset > 0.0f) {
                    b2 = true;
                }
            }
        }
        return b2;
    }
    
    boolean isLayoutRtlSupport() {
        final int layoutDirection = ViewCompat.getLayoutDirection((View)this);
        boolean b = true;
        if (layoutDirection != 1) {
            b = false;
        }
        return b;
    }
    
    public boolean isOpen() {
        return !this.mCanSlide || this.mSlideOffset == 0.0f;
    }
    
    public boolean isSlideable() {
        return this.mCanSlide;
    }
    
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        this.mFirstLayout = true;
        if (this.mFoldingFeatureObserver != null) {
            final Activity activityOrNull = getActivityOrNull(this.getContext());
            if (activityOrNull != null) {
                this.mFoldingFeatureObserver.registerLayoutStateChangeCallback(activityOrNull);
            }
        }
    }
    
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        this.mFirstLayout = true;
        final FoldingFeatureObserver mFoldingFeatureObserver = this.mFoldingFeatureObserver;
        if (mFoldingFeatureObserver != null) {
            mFoldingFeatureObserver.unregisterLayoutStateChangeCallback();
        }
        for (int i = 0; i < this.mPostedRunnables.size(); ++i) {
            this.mPostedRunnables.get(i).run();
        }
        this.mPostedRunnables.clear();
    }
    
    public boolean onInterceptTouchEvent(final MotionEvent motionEvent) {
        final int actionMasked = motionEvent.getActionMasked();
        final boolean mCanSlide = this.mCanSlide;
        final boolean b = true;
        if (!mCanSlide && actionMasked == 0 && this.getChildCount() > 1) {
            final View child = this.getChildAt(1);
            if (child != null) {
                this.mPreservedOpenState = this.mDragHelper.isViewUnder(child, (int)motionEvent.getX(), (int)motionEvent.getY());
            }
        }
        if (!this.mCanSlide || (this.mIsUnableToDrag && actionMasked != 0)) {
            this.mDragHelper.cancel();
            return super.onInterceptTouchEvent(motionEvent);
        }
        if (actionMasked != 3 && actionMasked != 1) {
            boolean b2 = false;
            Label_0245: {
                if (actionMasked != 0) {
                    if (actionMasked == 2) {
                        final float x = motionEvent.getX();
                        final float y = motionEvent.getY();
                        final float abs = Math.abs(x - this.mInitialMotionX);
                        final float abs2 = Math.abs(y - this.mInitialMotionY);
                        if (abs > this.mDragHelper.getTouchSlop() && abs2 > abs) {
                            this.mDragHelper.cancel();
                            this.mIsUnableToDrag = true;
                            return false;
                        }
                    }
                }
                else {
                    this.mIsUnableToDrag = false;
                    final float x2 = motionEvent.getX();
                    final float y2 = motionEvent.getY();
                    this.mInitialMotionX = x2;
                    this.mInitialMotionY = y2;
                    if (this.mDragHelper.isViewUnder(this.mSlideableView, (int)x2, (int)y2) && this.isDimmed(this.mSlideableView)) {
                        b2 = true;
                        break Label_0245;
                    }
                }
                b2 = false;
            }
            boolean b3 = b;
            if (!this.mDragHelper.shouldInterceptTouchEvent(motionEvent)) {
                b3 = (b2 && b);
            }
            return b3;
        }
        this.mDragHelper.cancel();
        return false;
    }
    
    protected void onLayout(final boolean b, int a, int mParallaxBy, int a2, int n) {
        final boolean layoutRtlSupport = this.isLayoutRtlSupport();
        final int n2 = a2 - a;
        if (layoutRtlSupport) {
            a = this.getPaddingRight();
        }
        else {
            a = this.getPaddingLeft();
        }
        if (layoutRtlSupport) {
            n = this.getPaddingLeft();
        }
        else {
            n = this.getPaddingRight();
        }
        final int paddingTop = this.getPaddingTop();
        final int childCount = this.getChildCount();
        if (this.mFirstLayout) {
            float mSlideOffset;
            if (this.mCanSlide && this.mPreservedOpenState) {
                mSlideOffset = 0.0f;
            }
            else {
                mSlideOffset = 1.0f;
            }
            this.mSlideOffset = mSlideOffset;
        }
        mParallaxBy = a;
        for (int i = 0; i < childCount; ++i) {
            final View child = this.getChildAt(i);
            if (child.getVisibility() != 8) {
                final LayoutParams layoutParams = (LayoutParams)child.getLayoutParams();
                final int measuredWidth = child.getMeasuredWidth();
                Label_0327: {
                    if (layoutParams.slideable) {
                        final int leftMargin = layoutParams.leftMargin;
                        a2 = layoutParams.rightMargin;
                        final int b2 = n2 - n;
                        final int mSlideRange = Math.min(a, b2) - mParallaxBy - (leftMargin + a2);
                        this.mSlideRange = mSlideRange;
                        if (layoutRtlSupport) {
                            a2 = layoutParams.rightMargin;
                        }
                        else {
                            a2 = layoutParams.leftMargin;
                        }
                        layoutParams.dimWhenOffset = (mParallaxBy + a2 + mSlideRange + measuredWidth / 2 > b2);
                        final int n3 = (int)(mSlideRange * this.mSlideOffset);
                        this.mSlideOffset = n3 / (float)this.mSlideRange;
                        mParallaxBy += a2 + n3;
                        a2 = 0;
                    }
                    else {
                        if (this.mCanSlide) {
                            mParallaxBy = this.mParallaxBy;
                            if (mParallaxBy != 0) {
                                a2 = (int)((1.0f - this.mSlideOffset) * mParallaxBy);
                                mParallaxBy = a;
                                break Label_0327;
                            }
                        }
                        mParallaxBy = a;
                        a2 = 0;
                    }
                }
                int n4;
                if (layoutRtlSupport) {
                    a2 += n2 - mParallaxBy;
                    n4 = a2 - measuredWidth;
                }
                else {
                    n4 = mParallaxBy - a2;
                    a2 = n4 + measuredWidth;
                }
                child.layout(n4, paddingTop, a2, child.getMeasuredHeight() + paddingTop);
                final FoldingFeature mFoldingFeature = this.mFoldingFeature;
                if (mFoldingFeature != null && mFoldingFeature.getOrientation() == FoldingFeature.Orientation.VERTICAL && this.mFoldingFeature.isSeparating()) {
                    a2 = this.mFoldingFeature.getBounds().width();
                }
                else {
                    a2 = 0;
                }
                a += child.getWidth() + Math.abs(a2);
            }
        }
        if (this.mFirstLayout) {
            if (this.mCanSlide && this.mParallaxBy != 0) {
                this.parallaxOtherViews(this.mSlideOffset);
            }
            this.updateObscuredViewsVisibility(this.mSlideableView);
        }
        this.mFirstLayout = false;
    }
    
    protected void onMeasure(int index, int leftMargin) {
        final int mode = View$MeasureSpec.getMode(index);
        final int size = View$MeasureSpec.getSize(index);
        final int mode2 = View$MeasureSpec.getMode(leftMargin);
        index = View$MeasureSpec.getSize(leftMargin);
        int n;
        if (mode2 != Integer.MIN_VALUE) {
            if (mode2 != 1073741824) {
                index = 0;
                n = 0;
            }
            else {
                index = (n = index - this.getPaddingTop() - this.getPaddingBottom());
            }
        }
        else {
            n = index - this.getPaddingTop() - this.getPaddingBottom();
            index = 0;
        }
        final int max = Math.max(size - this.getPaddingLeft() - this.getPaddingRight(), 0);
        final int childCount = this.getChildCount();
        if (childCount > 2) {
            Log.e("SlidingPaneLayout", "onMeasure: More than two child views are not supported.");
        }
        this.mSlideableView = null;
        int b = max;
        int i = 0;
        int n2 = 0;
        float n3 = 0.0f;
        while (i < childCount) {
            final View child = this.getChildAt(i);
            final LayoutParams layoutParams = (LayoutParams)child.getLayoutParams();
            Label_0486: {
                if (child.getVisibility() == 8) {
                    layoutParams.dimWhenOffset = false;
                }
                else {
                    float n4 = n3;
                    if (layoutParams.weight > 0.0f) {
                        n3 = (n4 = n3 + layoutParams.weight);
                        if (layoutParams.width == 0) {
                            break Label_0486;
                        }
                    }
                    final int max2 = Math.max(max - (layoutParams.leftMargin + layoutParams.rightMargin), 0);
                    int n6;
                    if (layoutParams.width == -2) {
                        int n5;
                        if (mode == 0) {
                            n5 = mode;
                        }
                        else {
                            n5 = Integer.MIN_VALUE;
                        }
                        n6 = View$MeasureSpec.makeMeasureSpec(max2, n5);
                    }
                    else if (layoutParams.width == -1) {
                        n6 = View$MeasureSpec.makeMeasureSpec(max2, mode);
                    }
                    else {
                        n6 = View$MeasureSpec.makeMeasureSpec(layoutParams.width, 1073741824);
                    }
                    child.measure(n6, getChildMeasureSpec(leftMargin, this.getPaddingTop() + this.getPaddingBottom(), layoutParams.height));
                    final int measuredWidth = child.getMeasuredWidth();
                    final int measuredHeight = child.getMeasuredHeight();
                    int min = index;
                    if (measuredHeight > index) {
                        if (mode2 == Integer.MIN_VALUE) {
                            min = Math.min(measuredHeight, n);
                        }
                        else {
                            min = index;
                            if (mode2 == 0) {
                                min = measuredHeight;
                            }
                        }
                    }
                    final int n7 = b - measuredWidth;
                    if (i == 0) {
                        index = min;
                        n3 = n4;
                        b = n7;
                    }
                    else {
                        int slideable;
                        if (n7 < 0) {
                            slideable = 1;
                        }
                        else {
                            slideable = 0;
                        }
                        layoutParams.slideable = (slideable != 0);
                        final boolean b2 = (n2 | slideable) != 0x0;
                        index = min;
                        n2 = (b2 ? 1 : 0);
                        n3 = n4;
                        b = n7;
                        if (layoutParams.slideable) {
                            this.mSlideableView = child;
                            b = n7;
                            n3 = n4;
                            n2 = (b2 ? 1 : 0);
                            index = min;
                        }
                    }
                }
            }
            ++i;
        }
        int n8 = 0;
        Label_0786: {
            if (n2 == 0) {
                n8 = index;
                if (n3 <= 0.0f) {
                    break Label_0786;
                }
            }
            int n9 = 0;
            while (true) {
                n8 = index;
                if (n9 >= childCount) {
                    break;
                }
                final View child2 = this.getChildAt(n9);
                int n10 = 0;
                Label_0777: {
                    if (child2.getVisibility() == 8) {
                        n10 = index;
                    }
                    else {
                        final LayoutParams layoutParams2 = (LayoutParams)child2.getLayoutParams();
                        int measuredWidth2;
                        if (layoutParams2.width == 0 && layoutParams2.weight > 0.0f) {
                            measuredWidth2 = 0;
                        }
                        else {
                            measuredWidth2 = child2.getMeasuredWidth();
                        }
                        int n11;
                        int n12;
                        if (n2 != 0) {
                            n11 = max - (layoutParams2.leftMargin + layoutParams2.rightMargin);
                            n12 = View$MeasureSpec.makeMeasureSpec(n11, 1073741824);
                        }
                        else if (layoutParams2.weight > 0.0f) {
                            n11 = measuredWidth2 + (int)(layoutParams2.weight * Math.max(0, b) / n3);
                            n12 = View$MeasureSpec.makeMeasureSpec(n11, 1073741824);
                        }
                        else {
                            n11 = measuredWidth2;
                            n12 = 0;
                        }
                        final int measureChildHeight = measureChildHeight(child2, leftMargin, this.getPaddingTop() + this.getPaddingBottom());
                        n10 = index;
                        if (measuredWidth2 != n11) {
                            child2.measure(n12, measureChildHeight);
                            final int measuredHeight2 = child2.getMeasuredHeight();
                            if (measuredHeight2 > (n10 = index)) {
                                if (mode2 == Integer.MIN_VALUE) {
                                    index = Math.min(measuredHeight2, n);
                                }
                                else {
                                    n10 = index;
                                    if (mode2 != 0) {
                                        break Label_0777;
                                    }
                                    index = measuredHeight2;
                                }
                                n10 = index;
                            }
                        }
                    }
                }
                ++n9;
                index = n10;
            }
        }
        final ArrayList<Rect> splitViewPositions = this.splitViewPositions();
        boolean mCanSlide = n2 != 0;
        if (splitViewPositions != null && !(mCanSlide = (n2 != 0))) {
            index = 0;
            while (true) {
                mCanSlide = (n2 != 0);
                if (index >= childCount) {
                    break;
                }
                final View child3 = this.getChildAt(index);
                if (child3.getVisibility() != 8) {
                    final Rect rect = splitViewPositions.get(index);
                    final LayoutParams layoutParams3 = (LayoutParams)child3.getLayoutParams();
                    leftMargin = layoutParams3.leftMargin;
                    final int rightMargin = layoutParams3.rightMargin;
                    final int measureSpec = View$MeasureSpec.makeMeasureSpec(child3.getMeasuredHeight(), 1073741824);
                    child3.measure(View$MeasureSpec.makeMeasureSpec(rect.width(), Integer.MIN_VALUE), measureSpec);
                    if ((child3.getMeasuredWidthAndState() & 0x1000000) != 0x1 && (getMinimumWidth(child3) == 0 || rect.width() >= getMinimumWidth(child3))) {
                        child3.measure(View$MeasureSpec.makeMeasureSpec(rect.width(), 1073741824), measureSpec);
                    }
                    else {
                        child3.measure(View$MeasureSpec.makeMeasureSpec(max - (leftMargin + rightMargin), 1073741824), measureSpec);
                        if (index != 0) {
                            layoutParams3.slideable = true;
                            this.mSlideableView = child3;
                            n2 = (true ? 1 : 0);
                        }
                    }
                }
                ++index;
            }
        }
        this.setMeasuredDimension(size, n8 + this.getPaddingTop() + this.getPaddingBottom());
        this.mCanSlide = mCanSlide;
        if (this.mDragHelper.getViewDragState() != 0 && !mCanSlide) {
            this.mDragHelper.abort();
        }
    }
    
    void onPanelDragged(int n) {
        if (this.mSlideableView == null) {
            this.mSlideOffset = 0.0f;
            return;
        }
        final boolean layoutRtlSupport = this.isLayoutRtlSupport();
        final LayoutParams layoutParams = (LayoutParams)this.mSlideableView.getLayoutParams();
        final int width = this.mSlideableView.getWidth();
        int n2 = n;
        if (layoutRtlSupport) {
            n2 = this.getWidth() - n - width;
        }
        if (layoutRtlSupport) {
            n = this.getPaddingRight();
        }
        else {
            n = this.getPaddingLeft();
        }
        int n3;
        if (layoutRtlSupport) {
            n3 = layoutParams.rightMargin;
        }
        else {
            n3 = layoutParams.leftMargin;
        }
        final float mSlideOffset = (n2 - (n + n3)) / (float)this.mSlideRange;
        this.mSlideOffset = mSlideOffset;
        if (this.mParallaxBy != 0) {
            this.parallaxOtherViews(mSlideOffset);
        }
        this.dispatchOnPanelSlide(this.mSlideableView);
    }
    
    protected void onRestoreInstanceState(final Parcelable parcelable) {
        if (!(parcelable instanceof SavedState)) {
            super.onRestoreInstanceState(parcelable);
            return;
        }
        final SavedState savedState = (SavedState)parcelable;
        super.onRestoreInstanceState(savedState.getSuperState());
        if (savedState.isOpen) {
            this.openPane();
        }
        else {
            this.closePane();
        }
        this.mPreservedOpenState = savedState.isOpen;
        this.setLockMode(savedState.mLockMode);
    }
    
    protected Parcelable onSaveInstanceState() {
        final SavedState savedState = new SavedState(super.onSaveInstanceState());
        boolean isOpen;
        if (this.isSlideable()) {
            isOpen = this.isOpen();
        }
        else {
            isOpen = this.mPreservedOpenState;
        }
        savedState.isOpen = isOpen;
        savedState.mLockMode = this.mLockMode;
        return (Parcelable)savedState;
    }
    
    protected void onSizeChanged(final int n, final int n2, final int n3, final int n4) {
        super.onSizeChanged(n, n2, n3, n4);
        if (n != n3) {
            this.mFirstLayout = true;
        }
    }
    
    public boolean onTouchEvent(final MotionEvent motionEvent) {
        if (!this.mCanSlide) {
            return super.onTouchEvent(motionEvent);
        }
        this.mDragHelper.processTouchEvent(motionEvent);
        final int actionMasked = motionEvent.getActionMasked();
        if (actionMasked != 0) {
            if (actionMasked == 1) {
                if (this.isDimmed(this.mSlideableView)) {
                    final float x = motionEvent.getX();
                    final float y = motionEvent.getY();
                    final float n = x - this.mInitialMotionX;
                    final float n2 = y - this.mInitialMotionY;
                    final int touchSlop = this.mDragHelper.getTouchSlop();
                    if (n * n + n2 * n2 < touchSlop * touchSlop && this.mDragHelper.isViewUnder(this.mSlideableView, (int)x, (int)y)) {
                        this.closePane(0);
                    }
                }
            }
        }
        else {
            final float x2 = motionEvent.getX();
            final float y2 = motionEvent.getY();
            this.mInitialMotionX = x2;
            this.mInitialMotionY = y2;
        }
        return true;
    }
    
    public void open() {
        this.openPane();
    }
    
    public boolean openPane() {
        return this.openPane(0);
    }
    
    public void removePanelSlideListener(final PanelSlideListener panelSlideListener) {
        this.mPanelSlideListeners.remove(panelSlideListener);
    }
    
    public void removeView(final View view) {
        if (view.getParent() instanceof TouchBlocker) {
            super.removeView((View)view.getParent());
            return;
        }
        super.removeView(view);
    }
    
    public void requestChildFocus(final View view, final View view2) {
        super.requestChildFocus(view, view2);
        if (!this.isInTouchMode() && !this.mCanSlide) {
            this.mPreservedOpenState = (view == this.mSlideableView);
        }
    }
    
    void setAllChildrenVisible() {
        for (int childCount = this.getChildCount(), i = 0; i < childCount; ++i) {
            final View child = this.getChildAt(i);
            if (child.getVisibility() == 4) {
                child.setVisibility(0);
            }
        }
    }
    
    @Deprecated
    public void setCoveredFadeColor(final int mCoveredFadeColor) {
        this.mCoveredFadeColor = mCoveredFadeColor;
    }
    
    public final void setLockMode(final int mLockMode) {
        this.mLockMode = mLockMode;
    }
    
    @Deprecated
    public void setPanelSlideListener(final PanelSlideListener mPanelSlideListener) {
        final PanelSlideListener mPanelSlideListener2 = this.mPanelSlideListener;
        if (mPanelSlideListener2 != null) {
            this.removePanelSlideListener(mPanelSlideListener2);
        }
        if (mPanelSlideListener != null) {
            this.addPanelSlideListener(mPanelSlideListener);
        }
        this.mPanelSlideListener = mPanelSlideListener;
    }
    
    public void setParallaxDistance(final int mParallaxBy) {
        this.mParallaxBy = mParallaxBy;
        this.requestLayout();
    }
    
    @Deprecated
    public void setShadowDrawable(final Drawable shadowDrawableLeft) {
        this.setShadowDrawableLeft(shadowDrawableLeft);
    }
    
    public void setShadowDrawableLeft(final Drawable mShadowDrawableLeft) {
        this.mShadowDrawableLeft = mShadowDrawableLeft;
    }
    
    public void setShadowDrawableRight(final Drawable mShadowDrawableRight) {
        this.mShadowDrawableRight = mShadowDrawableRight;
    }
    
    @Deprecated
    public void setShadowResource(final int n) {
        this.setShadowDrawableLeft(this.getResources().getDrawable(n));
    }
    
    public void setShadowResourceLeft(final int n) {
        this.setShadowDrawableLeft(ContextCompat.getDrawable(this.getContext(), n));
    }
    
    public void setShadowResourceRight(final int n) {
        this.setShadowDrawableRight(ContextCompat.getDrawable(this.getContext(), n));
    }
    
    @Deprecated
    public void setSliderFadeColor(final int mSliderFadeColor) {
        this.mSliderFadeColor = mSliderFadeColor;
    }
    
    @Deprecated
    public void smoothSlideClosed() {
        this.closePane();
    }
    
    @Deprecated
    public void smoothSlideOpen() {
        this.openPane();
    }
    
    boolean smoothSlideTo(final float n, int rightMargin) {
        if (!this.mCanSlide) {
            return false;
        }
        final boolean layoutRtlSupport = this.isLayoutRtlSupport();
        final LayoutParams layoutParams = (LayoutParams)this.mSlideableView.getLayoutParams();
        if (layoutRtlSupport) {
            final int paddingRight = this.getPaddingRight();
            rightMargin = layoutParams.rightMargin;
            rightMargin = (int)(this.getWidth() - (paddingRight + rightMargin + n * this.mSlideRange + this.mSlideableView.getWidth()));
        }
        else {
            rightMargin = (int)(this.getPaddingLeft() + layoutParams.leftMargin + n * this.mSlideRange);
        }
        final ViewDragHelper mDragHelper = this.mDragHelper;
        final View mSlideableView = this.mSlideableView;
        if (mDragHelper.smoothSlideViewTo(mSlideableView, rightMargin, mSlideableView.getTop())) {
            this.setAllChildrenVisible();
            ViewCompat.postInvalidateOnAnimation((View)this);
            return true;
        }
        return false;
    }
    
    void updateObscuredViewsVisibility(final View view) {
        final boolean layoutRtlSupport = this.isLayoutRtlSupport();
        int paddingLeft;
        if (layoutRtlSupport) {
            paddingLeft = this.getWidth() - this.getPaddingRight();
        }
        else {
            paddingLeft = this.getPaddingLeft();
        }
        int paddingLeft2;
        if (layoutRtlSupport) {
            paddingLeft2 = this.getPaddingLeft();
        }
        else {
            paddingLeft2 = this.getWidth() - this.getPaddingRight();
        }
        final int paddingTop = this.getPaddingTop();
        final int height = this.getHeight();
        final int paddingBottom = this.getPaddingBottom();
        int left;
        int right;
        int top;
        int bottom;
        if (view != null && viewIsOpaque(view)) {
            left = view.getLeft();
            right = view.getRight();
            top = view.getTop();
            bottom = view.getBottom();
        }
        else {
            left = 0;
            right = 0;
            top = 0;
            bottom = 0;
        }
        for (int childCount = this.getChildCount(), i = 0; i < childCount; ++i) {
            final View child = this.getChildAt(i);
            if (child == view) {
                break;
            }
            if (child.getVisibility() != 8) {
                int a;
                if (layoutRtlSupport) {
                    a = paddingLeft2;
                }
                else {
                    a = paddingLeft;
                }
                final int max = Math.max(a, child.getLeft());
                final int max2 = Math.max(paddingTop, child.getTop());
                int a2;
                if (layoutRtlSupport) {
                    a2 = paddingLeft;
                }
                else {
                    a2 = paddingLeft2;
                }
                final int min = Math.min(a2, child.getRight());
                final int min2 = Math.min(height - paddingBottom, child.getBottom());
                int visibility;
                if (max >= left && max2 >= top && min <= right && min2 <= bottom) {
                    visibility = 4;
                }
                else {
                    visibility = 0;
                }
                child.setVisibility(visibility);
            }
        }
    }
    
    class AccessibilityDelegate extends AccessibilityDelegateCompat
    {
        private final Rect mTmpRect;
        final SlidingPaneLayout this$0;
        
        AccessibilityDelegate(final SlidingPaneLayout this$0) {
            this.this$0 = this$0;
            this.mTmpRect = new Rect();
        }
        
        private void copyNodeInfoNoChildren(final AccessibilityNodeInfoCompat accessibilityNodeInfoCompat, final AccessibilityNodeInfoCompat accessibilityNodeInfoCompat2) {
            final Rect mTmpRect = this.mTmpRect;
            accessibilityNodeInfoCompat2.getBoundsInScreen(mTmpRect);
            accessibilityNodeInfoCompat.setBoundsInScreen(mTmpRect);
            accessibilityNodeInfoCompat.setVisibleToUser(accessibilityNodeInfoCompat2.isVisibleToUser());
            accessibilityNodeInfoCompat.setPackageName(accessibilityNodeInfoCompat2.getPackageName());
            accessibilityNodeInfoCompat.setClassName(accessibilityNodeInfoCompat2.getClassName());
            accessibilityNodeInfoCompat.setContentDescription(accessibilityNodeInfoCompat2.getContentDescription());
            accessibilityNodeInfoCompat.setEnabled(accessibilityNodeInfoCompat2.isEnabled());
            accessibilityNodeInfoCompat.setClickable(accessibilityNodeInfoCompat2.isClickable());
            accessibilityNodeInfoCompat.setFocusable(accessibilityNodeInfoCompat2.isFocusable());
            accessibilityNodeInfoCompat.setFocused(accessibilityNodeInfoCompat2.isFocused());
            accessibilityNodeInfoCompat.setAccessibilityFocused(accessibilityNodeInfoCompat2.isAccessibilityFocused());
            accessibilityNodeInfoCompat.setSelected(accessibilityNodeInfoCompat2.isSelected());
            accessibilityNodeInfoCompat.setLongClickable(accessibilityNodeInfoCompat2.isLongClickable());
            accessibilityNodeInfoCompat.addAction(accessibilityNodeInfoCompat2.getActions());
            accessibilityNodeInfoCompat.setMovementGranularities(accessibilityNodeInfoCompat2.getMovementGranularities());
        }
        
        public boolean filter(final View view) {
            return this.this$0.isDimmed(view);
        }
        
        @Override
        public void onInitializeAccessibilityEvent(final View view, final AccessibilityEvent accessibilityEvent) {
            super.onInitializeAccessibilityEvent(view, accessibilityEvent);
            accessibilityEvent.setClassName((CharSequence)"androidx.slidingpanelayout.widget.SlidingPaneLayout");
        }
        
        @Override
        public void onInitializeAccessibilityNodeInfo(View child, final AccessibilityNodeInfoCompat accessibilityNodeInfoCompat) {
            final AccessibilityNodeInfoCompat obtain = AccessibilityNodeInfoCompat.obtain(accessibilityNodeInfoCompat);
            super.onInitializeAccessibilityNodeInfo(child, obtain);
            this.copyNodeInfoNoChildren(accessibilityNodeInfoCompat, obtain);
            obtain.recycle();
            accessibilityNodeInfoCompat.setClassName("androidx.slidingpanelayout.widget.SlidingPaneLayout");
            accessibilityNodeInfoCompat.setSource(child);
            final ViewParent parentForAccessibility = ViewCompat.getParentForAccessibility(child);
            if (parentForAccessibility instanceof View) {
                accessibilityNodeInfoCompat.setParent((View)parentForAccessibility);
            }
            for (int childCount = this.this$0.getChildCount(), i = 0; i < childCount; ++i) {
                child = this.this$0.getChildAt(i);
                if (!this.filter(child) && child.getVisibility() == 0) {
                    ViewCompat.setImportantForAccessibility(child, 1);
                    accessibilityNodeInfoCompat.addChild(child);
                }
            }
        }
        
        @Override
        public boolean onRequestSendAccessibilityEvent(final ViewGroup viewGroup, final View view, final AccessibilityEvent accessibilityEvent) {
            return !this.filter(view) && super.onRequestSendAccessibilityEvent(viewGroup, view, accessibilityEvent);
        }
    }
    
    private class DisableLayerRunnable implements Runnable
    {
        final View mChildView;
        final SlidingPaneLayout this$0;
        
        DisableLayerRunnable(final SlidingPaneLayout this$0, final View mChildView) {
            this.this$0 = this$0;
            this.mChildView = mChildView;
        }
        
        @Override
        public void run() {
            if (this.mChildView.getParent() == this.this$0) {
                this.mChildView.setLayerType(0, (Paint)null);
                this.this$0.invalidateChildRegion(this.mChildView);
            }
            this.this$0.mPostedRunnables.remove(this);
        }
    }
    
    private class DragHelperCallback extends Callback
    {
        final SlidingPaneLayout this$0;
        
        DragHelperCallback(final SlidingPaneLayout this$0) {
            this.this$0 = this$0;
        }
        
        private boolean isDraggable() {
            return !this.this$0.mIsUnableToDrag && this.this$0.getLockMode() != 3 && (!this.this$0.isOpen() || this.this$0.getLockMode() != 1) && (this.this$0.isOpen() || this.this$0.getLockMode() != 2);
        }
        
        @Override
        public int clampViewPositionHorizontal(final View view, int n, int mSlideRange) {
            final LayoutParams layoutParams = (LayoutParams)this.this$0.mSlideableView.getLayoutParams();
            if (this.this$0.isLayoutRtlSupport()) {
                mSlideRange = this.this$0.getWidth() - (this.this$0.getPaddingRight() + layoutParams.rightMargin + this.this$0.mSlideableView.getWidth());
                n = Math.max(Math.min(n, mSlideRange), mSlideRange - this.this$0.mSlideRange);
            }
            else {
                final int b = this.this$0.getPaddingLeft() + layoutParams.leftMargin;
                mSlideRange = this.this$0.mSlideRange;
                n = Math.min(Math.max(n, b), mSlideRange + b);
            }
            return n;
        }
        
        @Override
        public int clampViewPositionVertical(final View view, final int n, final int n2) {
            return view.getTop();
        }
        
        @Override
        public int getViewHorizontalDragRange(final View view) {
            return this.this$0.mSlideRange;
        }
        
        @Override
        public void onEdgeDragStarted(final int n, final int n2) {
            if (!this.isDraggable()) {
                return;
            }
            this.this$0.mDragHelper.captureChildView(this.this$0.mSlideableView, n2);
        }
        
        @Override
        public void onEdgeTouched(final int n, final int n2) {
            if (!this.isDraggable()) {
                return;
            }
            this.this$0.mDragHelper.captureChildView(this.this$0.mSlideableView, n2);
        }
        
        @Override
        public void onViewCaptured(final View view, final int n) {
            this.this$0.setAllChildrenVisible();
        }
        
        @Override
        public void onViewDragStateChanged(final int n) {
            if (this.this$0.mDragHelper.getViewDragState() == 0) {
                if (this.this$0.mSlideOffset == 1.0f) {
                    final SlidingPaneLayout this$0 = this.this$0;
                    this$0.updateObscuredViewsVisibility(this$0.mSlideableView);
                    final SlidingPaneLayout this$2 = this.this$0;
                    this$2.dispatchOnPanelClosed(this$2.mSlideableView);
                    this.this$0.mPreservedOpenState = false;
                }
                else {
                    final SlidingPaneLayout this$3 = this.this$0;
                    this$3.dispatchOnPanelOpened(this$3.mSlideableView);
                    this.this$0.mPreservedOpenState = true;
                }
            }
        }
        
        @Override
        public void onViewPositionChanged(final View view, final int n, final int n2, final int n3, final int n4) {
            this.this$0.onPanelDragged(n);
            this.this$0.invalidate();
        }
        
        @Override
        public void onViewReleased(final View view, final float n, final float n2) {
            final LayoutParams layoutParams = (LayoutParams)view.getLayoutParams();
            int n5 = 0;
            Label_0176: {
                if (this.this$0.isLayoutRtlSupport()) {
                    final int n3 = this.this$0.getPaddingRight() + layoutParams.rightMargin;
                    int n4 = 0;
                    Label_0079: {
                        if (n >= 0.0f) {
                            n4 = n3;
                            if (n != 0.0f) {
                                break Label_0079;
                            }
                            n4 = n3;
                            if (this.this$0.mSlideOffset <= 0.5f) {
                                break Label_0079;
                            }
                        }
                        n4 = n3 + this.this$0.mSlideRange;
                    }
                    n5 = this.this$0.getWidth() - n4 - this.this$0.mSlideableView.getWidth();
                }
                else {
                    final int n6 = layoutParams.leftMargin + this.this$0.getPaddingLeft();
                    final float n7 = fcmpl(n, 0.0f);
                    if (n7 <= 0) {
                        n5 = n6;
                        if (n7 != 0) {
                            break Label_0176;
                        }
                        n5 = n6;
                        if (this.this$0.mSlideOffset <= 0.5f) {
                            break Label_0176;
                        }
                    }
                    n5 = n6 + this.this$0.mSlideRange;
                }
            }
            this.this$0.mDragHelper.settleCapturedViewAt(n5, view.getTop());
            this.this$0.invalidate();
        }
        
        @Override
        public boolean tryCaptureView(final View view, final int n) {
            return this.isDraggable() && ((LayoutParams)view.getLayoutParams()).slideable;
        }
    }
    
    public static class LayoutParams extends ViewGroup$MarginLayoutParams
    {
        private static final int[] ATTRS;
        Paint dimPaint;
        boolean dimWhenOffset;
        boolean slideable;
        public float weight;
        
        static {
            ATTRS = new int[] { 16843137 };
        }
        
        public LayoutParams() {
            super(-1, -1);
            this.weight = 0.0f;
        }
        
        public LayoutParams(final int n, final int n2) {
            super(n, n2);
            this.weight = 0.0f;
        }
        
        public LayoutParams(final Context context, final AttributeSet set) {
            super(context, set);
            this.weight = 0.0f;
            final TypedArray obtainStyledAttributes = context.obtainStyledAttributes(set, LayoutParams.ATTRS);
            this.weight = obtainStyledAttributes.getFloat(0, 0.0f);
            obtainStyledAttributes.recycle();
        }
        
        public LayoutParams(final ViewGroup$LayoutParams viewGroup$LayoutParams) {
            super(viewGroup$LayoutParams);
            this.weight = 0.0f;
        }
        
        public LayoutParams(final ViewGroup$MarginLayoutParams viewGroup$MarginLayoutParams) {
            super(viewGroup$MarginLayoutParams);
            this.weight = 0.0f;
        }
        
        public LayoutParams(final LayoutParams layoutParams) {
            super((ViewGroup$MarginLayoutParams)layoutParams);
            this.weight = 0.0f;
            this.weight = layoutParams.weight;
        }
    }
    
    public interface PanelSlideListener
    {
        void onPanelClosed(final View p0);
        
        void onPanelOpened(final View p0);
        
        void onPanelSlide(final View p0, final float p1);
    }
    
    static class SavedState extends AbsSavedState
    {
        public static final Parcelable$Creator<SavedState> CREATOR;
        boolean isOpen;
        int mLockMode;
        
        static {
            CREATOR = (Parcelable$Creator)new Parcelable$ClassLoaderCreator<SavedState>() {
                public SavedState createFromParcel(final Parcel parcel) {
                    return new SavedState(parcel, null);
                }
                
                public SavedState createFromParcel(final Parcel parcel, final ClassLoader classLoader) {
                    return new SavedState(parcel, null);
                }
                
                public SavedState[] newArray(final int n) {
                    return new SavedState[n];
                }
            };
        }
        
        SavedState(final Parcel parcel, final ClassLoader classLoader) {
            super(parcel, classLoader);
            this.isOpen = (parcel.readInt() != 0);
            this.mLockMode = parcel.readInt();
        }
        
        SavedState(final Parcelable parcelable) {
            super(parcelable);
        }
        
        @Override
        public void writeToParcel(final Parcel parcel, final int n) {
            super.writeToParcel(parcel, n);
            parcel.writeInt((int)(this.isOpen ? 1 : 0));
            parcel.writeInt(this.mLockMode);
        }
    }
    
    public static class SimplePanelSlideListener implements PanelSlideListener
    {
        @Override
        public void onPanelClosed(final View view) {
        }
        
        @Override
        public void onPanelOpened(final View view) {
        }
        
        @Override
        public void onPanelSlide(final View view, final float n) {
        }
    }
    
    private static class TouchBlocker extends FrameLayout
    {
        TouchBlocker(final View view) {
            super(view.getContext());
            this.addView(view);
        }
        
        public boolean onGenericMotionEvent(final MotionEvent motionEvent) {
            return true;
        }
        
        public boolean onTouchEvent(final MotionEvent motionEvent) {
            return true;
        }
    }
}

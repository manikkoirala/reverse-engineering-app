// 
// Decompiled by Procyon v0.6.0
// 

package androidx.slidingpanelayout.widget;

import kotlinx.coroutines.CoroutineStart;
import kotlinx.coroutines.BuildersKt;
import kotlin.coroutines.Continuation;
import kotlin.jvm.functions.Function2;
import kotlinx.coroutines.CoroutineScopeKt;
import kotlinx.coroutines.ExecutorsKt;
import kotlin.coroutines.CoroutineContext;
import java.util.concurrent.CancellationException;
import kotlinx.coroutines.Job$DefaultImpls;
import android.app.Activity;
import java.util.Iterator;
import androidx.window.layout.DisplayFeature;
import androidx.window.layout.FoldingFeature;
import androidx.window.layout.WindowLayoutInfo;
import kotlin.jvm.internal.Intrinsics;
import androidx.window.layout.WindowInfoTracker;
import kotlinx.coroutines.Job;
import java.util.concurrent.Executor;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000>\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\b\u0000\u0018\u00002\u00020\u0001:\u0001\u0015B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\u0012\u0010\u000b\u001a\u0004\u0018\u00010\f2\u0006\u0010\r\u001a\u00020\u000eH\u0002J\u000e\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u0012J\u000e\u0010\u0013\u001a\u00020\u00102\u0006\u0010\t\u001a\u00020\nJ\u0006\u0010\u0014\u001a\u00020\u0010R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\u0007\u001a\u0004\u0018\u00010\bX\u0082\u000e¢\u0006\u0002\n\u0000R\u0010\u0010\t\u001a\u0004\u0018\u00010\nX\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u0016" }, d2 = { "Landroidx/slidingpanelayout/widget/FoldingFeatureObserver;", "", "windowInfoTracker", "Landroidx/window/layout/WindowInfoTracker;", "executor", "Ljava/util/concurrent/Executor;", "(Landroidx/window/layout/WindowInfoTracker;Ljava/util/concurrent/Executor;)V", "job", "Lkotlinx/coroutines/Job;", "onFoldingFeatureChangeListener", "Landroidx/slidingpanelayout/widget/FoldingFeatureObserver$OnFoldingFeatureChangeListener;", "getFoldingFeature", "Landroidx/window/layout/FoldingFeature;", "windowLayoutInfo", "Landroidx/window/layout/WindowLayoutInfo;", "registerLayoutStateChangeCallback", "", "activity", "Landroid/app/Activity;", "setOnFoldingFeatureChangeListener", "unregisterLayoutStateChangeCallback", "OnFoldingFeatureChangeListener", "slidingpanelayout_release" }, k = 1, mv = { 1, 6, 0 }, xi = 48)
public final class FoldingFeatureObserver
{
    private final Executor executor;
    private Job job;
    private OnFoldingFeatureChangeListener onFoldingFeatureChangeListener;
    private final WindowInfoTracker windowInfoTracker;
    
    public FoldingFeatureObserver(final WindowInfoTracker windowInfoTracker, final Executor executor) {
        Intrinsics.checkNotNullParameter((Object)windowInfoTracker, "windowInfoTracker");
        Intrinsics.checkNotNullParameter((Object)executor, "executor");
        this.windowInfoTracker = windowInfoTracker;
        this.executor = executor;
    }
    
    private final FoldingFeature getFoldingFeature(final WindowLayoutInfo windowLayoutInfo) {
        final Iterator iterator = windowLayoutInfo.getDisplayFeatures().iterator();
        while (true) {
            Object next;
            do {
                final boolean hasNext = iterator.hasNext();
                FoldingFeature foldingFeature = null;
                if (!hasNext) {
                    next = null;
                    if (next instanceof FoldingFeature) {
                        foldingFeature = (FoldingFeature)next;
                    }
                    return foldingFeature;
                }
                next = iterator.next();
            } while (!(((DisplayFeature)next) instanceof FoldingFeature));
            continue;
        }
    }
    
    public final void registerLayoutStateChangeCallback(final Activity activity) {
        Intrinsics.checkNotNullParameter((Object)activity, "activity");
        final Job job = this.job;
        if (job != null) {
            Job$DefaultImpls.cancel$default(job, (CancellationException)null, 1, (Object)null);
        }
        this.job = BuildersKt.launch$default(CoroutineScopeKt.CoroutineScope((CoroutineContext)ExecutorsKt.from(this.executor)), (CoroutineContext)null, (CoroutineStart)null, (Function2)new FoldingFeatureObserver$registerLayoutStateChangeCallback.FoldingFeatureObserver$registerLayoutStateChangeCallback$1(this, activity, (Continuation)null), 3, (Object)null);
    }
    
    public final void setOnFoldingFeatureChangeListener(final OnFoldingFeatureChangeListener onFoldingFeatureChangeListener) {
        Intrinsics.checkNotNullParameter((Object)onFoldingFeatureChangeListener, "onFoldingFeatureChangeListener");
        this.onFoldingFeatureChangeListener = onFoldingFeatureChangeListener;
    }
    
    public final void unregisterLayoutStateChangeCallback() {
        final Job job = this.job;
        if (job != null) {
            Job$DefaultImpls.cancel$default(job, (CancellationException)null, 1, (Object)null);
        }
    }
    
    @Metadata(d1 = { "\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b`\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&¨\u0006\u0006" }, d2 = { "Landroidx/slidingpanelayout/widget/FoldingFeatureObserver$OnFoldingFeatureChangeListener;", "", "onFoldingFeatureChange", "", "foldingFeature", "Landroidx/window/layout/FoldingFeature;", "slidingpanelayout_release" }, k = 1, mv = { 1, 6, 0 }, xi = 48)
    public interface OnFoldingFeatureChangeListener
    {
        void onFoldingFeatureChange(final FoldingFeature p0);
    }
}

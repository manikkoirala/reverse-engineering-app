// 
// Decompiled by Procyon v0.6.0
// 

package androidx.appcompat.app;

import java.util.ArrayDeque;
import java.util.Queue;
import java.util.concurrent.Executor;
import android.content.ComponentName;
import android.os.Build$VERSION;
import android.content.Context;

class AppLocalesStorageHelper
{
    static final String APPLICATION_LOCALES_RECORD_FILE = "androidx.appcompat.app.AppCompatDelegate.application_locales_record_file";
    static final String APP_LOCALES_META_DATA_HOLDER_SERVICE_NAME = "androidx.appcompat.app.AppLocalesMetadataHolderService";
    static final String LOCALE_RECORD_ATTRIBUTE_TAG = "application_locales";
    static final String LOCALE_RECORD_FILE_TAG = "locales";
    static final String TAG = "AppLocalesStorageHelper";
    
    private AppLocalesStorageHelper() {
    }
    
    static void persistLocales(final Context p0, final String p1) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: ldc             ""
        //     3: invokevirtual   java/lang/String.equals:(Ljava/lang/Object;)Z
        //     6: ifeq            17
        //     9: aload_0        
        //    10: ldc             "androidx.appcompat.app.AppCompatDelegate.application_locales_record_file"
        //    12: invokevirtual   android/content/Context.deleteFile:(Ljava/lang/String;)Z
        //    15: pop            
        //    16: return         
        //    17: aload_0        
        //    18: ldc             "androidx.appcompat.app.AppCompatDelegate.application_locales_record_file"
        //    20: iconst_0       
        //    21: invokevirtual   android/content/Context.openFileOutput:(Ljava/lang/String;I)Ljava/io/FileOutputStream;
        //    24: astore_0       
        //    25: invokestatic    android/util/Xml.newSerializer:()Lorg/xmlpull/v1/XmlSerializer;
        //    28: astore_2       
        //    29: aload_2        
        //    30: aload_0        
        //    31: aconst_null    
        //    32: invokeinterface org/xmlpull/v1/XmlSerializer.setOutput:(Ljava/io/OutputStream;Ljava/lang/String;)V
        //    37: aload_2        
        //    38: ldc             "UTF-8"
        //    40: iconst_1       
        //    41: invokestatic    java/lang/Boolean.valueOf:(Z)Ljava/lang/Boolean;
        //    44: invokeinterface org/xmlpull/v1/XmlSerializer.startDocument:(Ljava/lang/String;Ljava/lang/Boolean;)V
        //    49: aload_2        
        //    50: aconst_null    
        //    51: ldc             "locales"
        //    53: invokeinterface org/xmlpull/v1/XmlSerializer.startTag:(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;
        //    58: pop            
        //    59: aload_2        
        //    60: aconst_null    
        //    61: ldc             "application_locales"
        //    63: aload_1        
        //    64: invokeinterface org/xmlpull/v1/XmlSerializer.attribute:(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;
        //    69: pop            
        //    70: aload_2        
        //    71: aconst_null    
        //    72: ldc             "locales"
        //    74: invokeinterface org/xmlpull/v1/XmlSerializer.endTag:(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;
        //    79: pop            
        //    80: aload_2        
        //    81: invokeinterface org/xmlpull/v1/XmlSerializer.endDocument:()V
        //    86: new             Ljava/lang/StringBuilder;
        //    89: astore_2       
        //    90: aload_2        
        //    91: invokespecial   java/lang/StringBuilder.<init>:()V
        //    94: aload_2        
        //    95: ldc             "Storing App Locales : app-locales: "
        //    97: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   100: pop            
        //   101: aload_2        
        //   102: aload_1        
        //   103: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   106: pop            
        //   107: aload_2        
        //   108: ldc             " persisted successfully."
        //   110: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   113: pop            
        //   114: ldc             "AppLocalesStorageHelper"
        //   116: aload_2        
        //   117: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   120: invokestatic    android/util/Log.d:(Ljava/lang/String;Ljava/lang/String;)I
        //   123: pop            
        //   124: aload_0        
        //   125: ifnull          179
        //   128: aload_0        
        //   129: invokevirtual   java/io/FileOutputStream.close:()V
        //   132: goto            179
        //   135: astore_1       
        //   136: goto            180
        //   139: astore_2       
        //   140: new             Ljava/lang/StringBuilder;
        //   143: astore_3       
        //   144: aload_3        
        //   145: invokespecial   java/lang/StringBuilder.<init>:()V
        //   148: aload_3        
        //   149: ldc             "Storing App Locales : Failed to persist app-locales: "
        //   151: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   154: pop            
        //   155: aload_3        
        //   156: aload_1        
        //   157: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   160: pop            
        //   161: ldc             "AppLocalesStorageHelper"
        //   163: aload_3        
        //   164: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   167: aload_2        
        //   168: invokestatic    android/util/Log.w:(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //   171: pop            
        //   172: aload_0        
        //   173: ifnull          179
        //   176: goto            128
        //   179: return         
        //   180: aload_0        
        //   181: ifnull          188
        //   184: aload_0        
        //   185: invokevirtual   java/io/FileOutputStream.close:()V
        //   188: aload_1        
        //   189: athrow         
        //   190: astore_0       
        //   191: ldc             "AppLocalesStorageHelper"
        //   193: ldc             "Storing App Locales : FileNotFoundException: Cannot open file %s for writing "
        //   195: iconst_1       
        //   196: anewarray       Ljava/lang/Object;
        //   199: dup            
        //   200: iconst_0       
        //   201: ldc             "androidx.appcompat.app.AppCompatDelegate.application_locales_record_file"
        //   203: aastore        
        //   204: invokestatic    java/lang/String.format:(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
        //   207: invokestatic    android/util/Log.w:(Ljava/lang/String;Ljava/lang/String;)I
        //   210: pop            
        //   211: return         
        //   212: astore_0       
        //   213: goto            179
        //   216: astore_0       
        //   217: goto            188
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                           
        //  -----  -----  -----  -----  -------------------------------
        //  17     25     190    212    Ljava/io/FileNotFoundException;
        //  29     124    139    179    Ljava/lang/Exception;
        //  29     124    135    190    Any
        //  128    132    212    216    Ljava/io/IOException;
        //  140    172    135    190    Any
        //  184    188    216    220    Ljava/io/IOException;
        // 
        // The error that occurred was:
        // 
        // java.lang.IndexOutOfBoundsException: Index 117 out of bounds for length 117
        //     at java.base/jdk.internal.util.Preconditions.outOfBounds(Preconditions.java:64)
        //     at java.base/jdk.internal.util.Preconditions.outOfBoundsCheckIndex(Preconditions.java:70)
        //     at java.base/jdk.internal.util.Preconditions.checkIndex(Preconditions.java:266)
        //     at java.base/java.util.Objects.checkIndex(Objects.java:361)
        //     at java.base/java.util.ArrayList.get(ArrayList.java:427)
        //     at com.strobel.decompiler.ast.AstBuilder.convertToAst(AstBuilder.java:3362)
        //     at com.strobel.decompiler.ast.AstBuilder.build(AstBuilder.java:112)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:203)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    static String readLocales(final Context p0) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     2: astore          4
        //     4: aload_0        
        //     5: ldc             "androidx.appcompat.app.AppCompatDelegate.application_locales_record_file"
        //     7: invokevirtual   android/content/Context.openFileInput:(Ljava/lang/String;)Ljava/io/FileInputStream;
        //    10: astore          6
        //    12: invokestatic    android/util/Xml.newPullParser:()Lorg/xmlpull/v1/XmlPullParser;
        //    15: astore          5
        //    17: aload           5
        //    19: aload           6
        //    21: ldc             "UTF-8"
        //    23: invokeinterface org/xmlpull/v1/XmlPullParser.setInput:(Ljava/io/InputStream;Ljava/lang/String;)V
        //    28: aload           5
        //    30: invokeinterface org/xmlpull/v1/XmlPullParser.getDepth:()I
        //    35: istore_2       
        //    36: aload           5
        //    38: invokeinterface org/xmlpull/v1/XmlPullParser.next:()I
        //    43: istore_1       
        //    44: aload           4
        //    46: astore_3       
        //    47: iload_1        
        //    48: iconst_1       
        //    49: if_icmpeq       110
        //    52: iload_1        
        //    53: iconst_3       
        //    54: if_icmpne       71
        //    57: aload           4
        //    59: astore_3       
        //    60: aload           5
        //    62: invokeinterface org/xmlpull/v1/XmlPullParser.getDepth:()I
        //    67: iload_2        
        //    68: if_icmple       110
        //    71: iload_1        
        //    72: iconst_3       
        //    73: if_icmpeq       36
        //    76: iload_1        
        //    77: iconst_4       
        //    78: if_icmpne       84
        //    81: goto            36
        //    84: aload           5
        //    86: invokeinterface org/xmlpull/v1/XmlPullParser.getName:()Ljava/lang/String;
        //    91: ldc             "locales"
        //    93: invokevirtual   java/lang/String.equals:(Ljava/lang/Object;)Z
        //    96: ifeq            36
        //    99: aload           5
        //   101: aconst_null    
        //   102: ldc             "application_locales"
        //   104: invokeinterface org/xmlpull/v1/XmlPullParser.getAttributeValue:(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
        //   109: astore_3       
        //   110: aload_3        
        //   111: astore          5
        //   113: aload           6
        //   115: ifnull          165
        //   118: aload           6
        //   120: invokevirtual   java/io/FileInputStream.close:()V
        //   123: aload_3        
        //   124: astore          5
        //   126: goto            165
        //   129: astore          4
        //   131: aload_3        
        //   132: astore          5
        //   134: goto            165
        //   137: astore_0       
        //   138: goto            218
        //   141: astore_3       
        //   142: ldc             "AppLocalesStorageHelper"
        //   144: ldc             "Reading app Locales : Unable to parse through file :androidx.appcompat.app.AppCompatDelegate.application_locales_record_file"
        //   146: invokestatic    android/util/Log.w:(Ljava/lang/String;Ljava/lang/String;)I
        //   149: pop            
        //   150: aload           4
        //   152: astore          5
        //   154: aload           6
        //   156: ifnull          165
        //   159: aload           4
        //   161: astore_3       
        //   162: goto            118
        //   165: aload           5
        //   167: invokevirtual   java/lang/String.isEmpty:()Z
        //   170: ifne            208
        //   173: new             Ljava/lang/StringBuilder;
        //   176: dup            
        //   177: invokespecial   java/lang/StringBuilder.<init>:()V
        //   180: astore_0       
        //   181: aload_0        
        //   182: ldc             "Reading app Locales : Locales read from file: androidx.appcompat.app.AppCompatDelegate.application_locales_record_file , appLocales: "
        //   184: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   187: pop            
        //   188: aload_0        
        //   189: aload           5
        //   191: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   194: pop            
        //   195: ldc             "AppLocalesStorageHelper"
        //   197: aload_0        
        //   198: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   201: invokestatic    android/util/Log.d:(Ljava/lang/String;Ljava/lang/String;)I
        //   204: pop            
        //   205: goto            215
        //   208: aload_0        
        //   209: ldc             "androidx.appcompat.app.AppCompatDelegate.application_locales_record_file"
        //   211: invokevirtual   android/content/Context.deleteFile:(Ljava/lang/String;)Z
        //   214: pop            
        //   215: aload           5
        //   217: areturn        
        //   218: aload           6
        //   220: ifnull          228
        //   223: aload           6
        //   225: invokevirtual   java/io/FileInputStream.close:()V
        //   228: aload_0        
        //   229: athrow         
        //   230: astore_0       
        //   231: ldc             "AppLocalesStorageHelper"
        //   233: ldc             "Reading app Locales : Locales record file not found: androidx.appcompat.app.AppCompatDelegate.application_locales_record_file"
        //   235: invokestatic    android/util/Log.w:(Ljava/lang/String;Ljava/lang/String;)I
        //   238: pop            
        //   239: ldc             ""
        //   241: areturn        
        //   242: astore_3       
        //   243: goto            228
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                                   
        //  -----  -----  -----  -----  ---------------------------------------
        //  4      12     230    242    Ljava/io/FileNotFoundException;
        //  12     36     141    165    Lorg/xmlpull/v1/XmlPullParserException;
        //  12     36     141    165    Ljava/io/IOException;
        //  12     36     137    230    Any
        //  36     44     141    165    Lorg/xmlpull/v1/XmlPullParserException;
        //  36     44     141    165    Ljava/io/IOException;
        //  36     44     137    230    Any
        //  60     71     141    165    Lorg/xmlpull/v1/XmlPullParserException;
        //  60     71     141    165    Ljava/io/IOException;
        //  60     71     137    230    Any
        //  84     110    141    165    Lorg/xmlpull/v1/XmlPullParserException;
        //  84     110    141    165    Ljava/io/IOException;
        //  84     110    137    230    Any
        //  118    123    129    137    Ljava/io/IOException;
        //  142    150    137    230    Any
        //  223    228    242    246    Ljava/io/IOException;
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0228:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2604)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:206)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    static void syncLocalesToFramework(final Context context) {
        if (Build$VERSION.SDK_INT >= 33) {
            final ComponentName componentName = new ComponentName(context, "androidx.appcompat.app.AppLocalesMetadataHolderService");
            if (context.getPackageManager().getComponentEnabledSetting(componentName) != 1) {
                if (AppCompatDelegate.getApplicationLocales().isEmpty()) {
                    final String locales = readLocales(context);
                    final Object systemService = context.getSystemService("locale");
                    if (systemService != null) {
                        AppCompatDelegate.Api33Impl.localeManagerSetApplicationLocales(systemService, AppCompatDelegate.Api24Impl.localeListForLanguageTags(locales));
                    }
                }
                context.getPackageManager().setComponentEnabledSetting(componentName, 1, 1);
            }
        }
    }
    
    static class SerialExecutor implements Executor
    {
        Runnable mActive;
        final Executor mExecutor;
        private final Object mLock;
        final Queue<Runnable> mTasks;
        
        SerialExecutor(final Executor mExecutor) {
            this.mLock = new Object();
            this.mTasks = new ArrayDeque<Runnable>();
            this.mExecutor = mExecutor;
        }
        
        @Override
        public void execute(final Runnable runnable) {
            synchronized (this.mLock) {
                this.mTasks.add(new AppLocalesStorageHelper$SerialExecutor$$ExternalSyntheticLambda0(this, runnable));
                if (this.mActive == null) {
                    this.scheduleNext();
                }
            }
        }
        
        protected void scheduleNext() {
            synchronized (this.mLock) {
                final Runnable mActive = this.mTasks.poll();
                this.mActive = mActive;
                if (mActive != null) {
                    this.mExecutor.execute(mActive);
                }
            }
        }
    }
    
    static class ThreadPerTaskExecutor implements Executor
    {
        @Override
        public void execute(final Runnable target) {
            new Thread(target).start();
        }
    }
}

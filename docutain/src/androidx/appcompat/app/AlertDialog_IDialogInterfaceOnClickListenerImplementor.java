// 
// Decompiled by Procyon v0.6.0
// 

package androidx.appcompat.app;

import android.content.DialogInterface;
import mono.android.TypeManager;
import mono.android.Runtime;
import java.util.ArrayList;
import android.content.DialogInterface$OnClickListener;
import mono.android.IGCUserPeer;

public class AlertDialog_IDialogInterfaceOnClickListenerImplementor implements IGCUserPeer, DialogInterface$OnClickListener
{
    public static final String __md_methods = "n_onClick:(Landroid/content/DialogInterface;I)V:GetOnClick_Landroid_content_DialogInterface_IHandler:Android.Content.IDialogInterfaceOnClickListenerInvoker, Mono.Android, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null\n";
    private ArrayList refList;
    
    static {
        Runtime.register("AndroidX.AppCompat.App.AlertDialog+IDialogInterfaceOnClickListenerImplementor, Xamarin.AndroidX.AppCompat", (Class)AlertDialog_IDialogInterfaceOnClickListenerImplementor.class, "n_onClick:(Landroid/content/DialogInterface;I)V:GetOnClick_Landroid_content_DialogInterface_IHandler:Android.Content.IDialogInterfaceOnClickListenerInvoker, Mono.Android, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null\n");
    }
    
    public AlertDialog_IDialogInterfaceOnClickListenerImplementor() {
        if (this.getClass() == AlertDialog_IDialogInterfaceOnClickListenerImplementor.class) {
            TypeManager.Activate("AndroidX.AppCompat.App.AlertDialog+IDialogInterfaceOnClickListenerImplementor, Xamarin.AndroidX.AppCompat", "", (Object)this, new Object[0]);
        }
    }
    
    private native void n_onClick(final DialogInterface p0, final int p1);
    
    public void monodroidAddReference(final Object e) {
        if (this.refList == null) {
            this.refList = new ArrayList();
        }
        this.refList.add(e);
    }
    
    public void monodroidClearReferences() {
        final ArrayList refList = this.refList;
        if (refList != null) {
            refList.clear();
        }
    }
    
    public void onClick(final DialogInterface dialogInterface, final int n) {
        this.n_onClick(dialogInterface, n);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.appcompat.app;

import java.util.Calendar;
import android.util.Log;
import androidx.core.content.PermissionChecker;
import android.location.Location;
import android.location.LocationManager;
import android.content.Context;

class TwilightManager
{
    private static final int SUNRISE = 6;
    private static final int SUNSET = 22;
    private static final String TAG = "TwilightManager";
    private static TwilightManager sInstance;
    private final Context mContext;
    private final LocationManager mLocationManager;
    private final TwilightState mTwilightState;
    
    TwilightManager(final Context mContext, final LocationManager mLocationManager) {
        this.mTwilightState = new TwilightState();
        this.mContext = mContext;
        this.mLocationManager = mLocationManager;
    }
    
    static TwilightManager getInstance(Context applicationContext) {
        if (TwilightManager.sInstance == null) {
            applicationContext = applicationContext.getApplicationContext();
            TwilightManager.sInstance = new TwilightManager(applicationContext, (LocationManager)applicationContext.getSystemService("location"));
        }
        return TwilightManager.sInstance;
    }
    
    private Location getLastKnownLocation() {
        final int checkSelfPermission = PermissionChecker.checkSelfPermission(this.mContext, "android.permission.ACCESS_COARSE_LOCATION");
        Location lastKnownLocationForProvider = null;
        Location lastKnownLocationForProvider2;
        if (checkSelfPermission == 0) {
            lastKnownLocationForProvider2 = this.getLastKnownLocationForProvider("network");
        }
        else {
            lastKnownLocationForProvider2 = null;
        }
        if (PermissionChecker.checkSelfPermission(this.mContext, "android.permission.ACCESS_FINE_LOCATION") == 0) {
            lastKnownLocationForProvider = this.getLastKnownLocationForProvider("gps");
        }
        if (lastKnownLocationForProvider != null && lastKnownLocationForProvider2 != null) {
            Location location = lastKnownLocationForProvider2;
            if (lastKnownLocationForProvider.getTime() > lastKnownLocationForProvider2.getTime()) {
                location = lastKnownLocationForProvider;
            }
            return location;
        }
        if (lastKnownLocationForProvider != null) {
            lastKnownLocationForProvider2 = lastKnownLocationForProvider;
        }
        return lastKnownLocationForProvider2;
    }
    
    private Location getLastKnownLocationForProvider(final String s) {
        try {
            if (this.mLocationManager.isProviderEnabled(s)) {
                return this.mLocationManager.getLastKnownLocation(s);
            }
        }
        catch (final Exception ex) {
            Log.d("TwilightManager", "Failed to get last known location", (Throwable)ex);
        }
        return null;
    }
    
    private boolean isStateValid() {
        return this.mTwilightState.nextUpdate > System.currentTimeMillis();
    }
    
    static void setInstance(final TwilightManager sInstance) {
        TwilightManager.sInstance = sInstance;
    }
    
    private void updateState(final Location location) {
        final TwilightState mTwilightState = this.mTwilightState;
        final long currentTimeMillis = System.currentTimeMillis();
        final TwilightCalculator instance = TwilightCalculator.getInstance();
        instance.calculateTwilight(currentTimeMillis - 86400000L, location.getLatitude(), location.getLongitude());
        instance.calculateTwilight(currentTimeMillis, location.getLatitude(), location.getLongitude());
        final boolean isNight = instance.state == 1;
        final long sunrise = instance.sunrise;
        final long sunset = instance.sunset;
        instance.calculateTwilight(currentTimeMillis + 86400000L, location.getLatitude(), location.getLongitude());
        final long sunrise2 = instance.sunrise;
        long nextUpdate;
        if (sunrise != -1L && sunset != -1L) {
            long n;
            if (currentTimeMillis > sunset) {
                n = sunrise2 + 0L;
            }
            else if (currentTimeMillis > sunrise) {
                n = sunset + 0L;
            }
            else {
                n = sunrise + 0L;
            }
            nextUpdate = n + 60000L;
        }
        else {
            nextUpdate = 43200000L + currentTimeMillis;
        }
        mTwilightState.isNight = isNight;
        mTwilightState.nextUpdate = nextUpdate;
    }
    
    boolean isNight() {
        final TwilightState mTwilightState = this.mTwilightState;
        if (this.isStateValid()) {
            return mTwilightState.isNight;
        }
        final Location lastKnownLocation = this.getLastKnownLocation();
        if (lastKnownLocation != null) {
            this.updateState(lastKnownLocation);
            return mTwilightState.isNight;
        }
        Log.i("TwilightManager", "Could not get last known location. This is probably because the app does not have any location permissions. Falling back to hardcoded sunrise/sunset values.");
        final int value = Calendar.getInstance().get(11);
        return value < 6 || value >= 22;
    }
    
    private static class TwilightState
    {
        boolean isNight;
        long nextUpdate;
        
        TwilightState() {
        }
    }
}

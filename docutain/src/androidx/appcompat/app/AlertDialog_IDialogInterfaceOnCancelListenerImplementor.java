// 
// Decompiled by Procyon v0.6.0
// 

package androidx.appcompat.app;

import android.content.DialogInterface;
import mono.android.TypeManager;
import mono.android.Runtime;
import java.util.ArrayList;
import android.content.DialogInterface$OnCancelListener;
import mono.android.IGCUserPeer;

public class AlertDialog_IDialogInterfaceOnCancelListenerImplementor implements IGCUserPeer, DialogInterface$OnCancelListener
{
    public static final String __md_methods = "n_onCancel:(Landroid/content/DialogInterface;)V:GetOnCancel_Landroid_content_DialogInterface_Handler:Android.Content.IDialogInterfaceOnCancelListenerInvoker, Mono.Android, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null\n";
    private ArrayList refList;
    
    static {
        Runtime.register("AndroidX.AppCompat.App.AlertDialog+IDialogInterfaceOnCancelListenerImplementor, Xamarin.AndroidX.AppCompat", (Class)AlertDialog_IDialogInterfaceOnCancelListenerImplementor.class, "n_onCancel:(Landroid/content/DialogInterface;)V:GetOnCancel_Landroid_content_DialogInterface_Handler:Android.Content.IDialogInterfaceOnCancelListenerInvoker, Mono.Android, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null\n");
    }
    
    public AlertDialog_IDialogInterfaceOnCancelListenerImplementor() {
        if (this.getClass() == AlertDialog_IDialogInterfaceOnCancelListenerImplementor.class) {
            TypeManager.Activate("AndroidX.AppCompat.App.AlertDialog+IDialogInterfaceOnCancelListenerImplementor, Xamarin.AndroidX.AppCompat", "", (Object)this, new Object[0]);
        }
    }
    
    private native void n_onCancel(final DialogInterface p0);
    
    public void monodroidAddReference(final Object e) {
        if (this.refList == null) {
            this.refList = new ArrayList();
        }
        this.refList.add(e);
    }
    
    public void monodroidClearReferences() {
        final ArrayList refList = this.refList;
        if (refList != null) {
            refList.clear();
        }
    }
    
    public void onCancel(final DialogInterface dialogInterface) {
        this.n_onCancel(dialogInterface);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.appcompat.app;

import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;
import android.app.LocaleManager;
import android.os.LocaleList;
import androidx.appcompat.view.ActionMode;
import androidx.appcompat.widget.Toolbar;
import android.window.OnBackInvokedDispatcher;
import android.os.Bundle;
import android.content.res.Configuration;
import android.view.MenuInflater;
import android.util.AttributeSet;
import android.view.ViewGroup$LayoutParams;
import android.view.View;
import java.util.Objects;
import androidx.appcompat.widget.VectorEnabledTintResources;
import android.content.pm.ServiceInfo;
import android.content.pm.PackageManager$NameNotFoundException;
import android.util.Log;
import androidx.core.os.BuildCompat;
import android.view.Window;
import android.content.Context;
import android.app.Dialog;
import android.app.Activity;
import java.util.Iterator;
import java.util.concurrent.Executor;
import androidx.core.os.LocaleListCompat;
import java.lang.ref.WeakReference;
import androidx.collection.ArraySet;

public abstract class AppCompatDelegate
{
    static final boolean DEBUG = false;
    public static final int FEATURE_ACTION_MODE_OVERLAY = 10;
    public static final int FEATURE_SUPPORT_ACTION_BAR = 108;
    public static final int FEATURE_SUPPORT_ACTION_BAR_OVERLAY = 109;
    @Deprecated
    public static final int MODE_NIGHT_AUTO = 0;
    public static final int MODE_NIGHT_AUTO_BATTERY = 3;
    @Deprecated
    public static final int MODE_NIGHT_AUTO_TIME = 0;
    public static final int MODE_NIGHT_FOLLOW_SYSTEM = -1;
    public static final int MODE_NIGHT_NO = 1;
    public static final int MODE_NIGHT_UNSPECIFIED = -100;
    public static final int MODE_NIGHT_YES = 2;
    static final String TAG = "AppCompatDelegate";
    private static final ArraySet<WeakReference<AppCompatDelegate>> sActivityDelegates;
    private static final Object sActivityDelegatesLock;
    private static final Object sAppLocalesStorageSyncLock;
    private static int sDefaultNightMode;
    private static Boolean sIsAutoStoreLocalesOptedIn;
    private static boolean sIsFrameworkSyncChecked;
    private static LocaleListCompat sRequestedAppLocales;
    static AppLocalesStorageHelper.SerialExecutor sSerialExecutorForLocalesStorage;
    private static LocaleListCompat sStoredAppLocales;
    
    static {
        AppCompatDelegate.sSerialExecutorForLocalesStorage = new AppLocalesStorageHelper.SerialExecutor(new AppLocalesStorageHelper.ThreadPerTaskExecutor());
        AppCompatDelegate.sDefaultNightMode = -100;
        AppCompatDelegate.sRequestedAppLocales = null;
        AppCompatDelegate.sStoredAppLocales = null;
        AppCompatDelegate.sIsAutoStoreLocalesOptedIn = null;
        AppCompatDelegate.sIsFrameworkSyncChecked = false;
        sActivityDelegates = new ArraySet<WeakReference<AppCompatDelegate>>();
        sActivityDelegatesLock = new Object();
        sAppLocalesStorageSyncLock = new Object();
    }
    
    AppCompatDelegate() {
    }
    
    static void addActiveDelegate(final AppCompatDelegate referent) {
        synchronized (AppCompatDelegate.sActivityDelegatesLock) {
            removeDelegateFromActives(referent);
            AppCompatDelegate.sActivityDelegates.add(new WeakReference<AppCompatDelegate>(referent));
        }
    }
    
    private static void applyDayNightToActiveDelegates() {
        synchronized (AppCompatDelegate.sActivityDelegatesLock) {
            final Iterator<WeakReference<AppCompatDelegate>> iterator = AppCompatDelegate.sActivityDelegates.iterator();
            while (iterator.hasNext()) {
                final AppCompatDelegate appCompatDelegate = iterator.next().get();
                if (appCompatDelegate != null) {
                    appCompatDelegate.applyDayNight();
                }
            }
        }
    }
    
    private static void applyLocalesToActiveDelegates() {
        final Iterator<WeakReference<AppCompatDelegate>> iterator = AppCompatDelegate.sActivityDelegates.iterator();
        while (iterator.hasNext()) {
            final AppCompatDelegate appCompatDelegate = iterator.next().get();
            if (appCompatDelegate != null) {
                appCompatDelegate.applyAppLocales();
            }
        }
    }
    
    public static AppCompatDelegate create(final Activity activity, final AppCompatCallback appCompatCallback) {
        return new AppCompatDelegateImpl(activity, appCompatCallback);
    }
    
    public static AppCompatDelegate create(final Dialog dialog, final AppCompatCallback appCompatCallback) {
        return new AppCompatDelegateImpl(dialog, appCompatCallback);
    }
    
    public static AppCompatDelegate create(final Context context, final Activity activity, final AppCompatCallback appCompatCallback) {
        return new AppCompatDelegateImpl(context, activity, appCompatCallback);
    }
    
    public static AppCompatDelegate create(final Context context, final Window window, final AppCompatCallback appCompatCallback) {
        return new AppCompatDelegateImpl(context, window, appCompatCallback);
    }
    
    public static LocaleListCompat getApplicationLocales() {
        if (BuildCompat.isAtLeastT()) {
            final Object localeManagerForApplication = getLocaleManagerForApplication();
            if (localeManagerForApplication != null) {
                return LocaleListCompat.wrap(Api33Impl.localeManagerGetApplicationLocales(localeManagerForApplication));
            }
        }
        else {
            final LocaleListCompat sRequestedAppLocales = AppCompatDelegate.sRequestedAppLocales;
            if (sRequestedAppLocales != null) {
                return sRequestedAppLocales;
            }
        }
        return LocaleListCompat.getEmptyLocaleList();
    }
    
    public static int getDefaultNightMode() {
        return AppCompatDelegate.sDefaultNightMode;
    }
    
    static Object getLocaleManagerForApplication() {
        final Iterator<WeakReference<AppCompatDelegate>> iterator = AppCompatDelegate.sActivityDelegates.iterator();
        while (iterator.hasNext()) {
            final AppCompatDelegate appCompatDelegate = iterator.next().get();
            if (appCompatDelegate != null) {
                final Context contextForDelegate = appCompatDelegate.getContextForDelegate();
                if (contextForDelegate != null) {
                    return contextForDelegate.getSystemService("locale");
                }
                continue;
            }
        }
        return null;
    }
    
    static LocaleListCompat getRequestedAppLocales() {
        return AppCompatDelegate.sRequestedAppLocales;
    }
    
    static LocaleListCompat getStoredAppLocales() {
        return AppCompatDelegate.sStoredAppLocales;
    }
    
    static boolean isAutoStorageOptedIn(final Context context) {
        if (AppCompatDelegate.sIsAutoStoreLocalesOptedIn == null) {
            try {
                final ServiceInfo serviceInfo = AppLocalesMetadataHolderService.getServiceInfo(context);
                if (serviceInfo.metaData != null) {
                    AppCompatDelegate.sIsAutoStoreLocalesOptedIn = serviceInfo.metaData.getBoolean("autoStoreLocales");
                }
            }
            catch (final PackageManager$NameNotFoundException ex) {
                Log.d("AppCompatDelegate", "Checking for metadata for AppLocalesMetadataHolderService : Service not found");
                AppCompatDelegate.sIsAutoStoreLocalesOptedIn = false;
            }
        }
        return AppCompatDelegate.sIsAutoStoreLocalesOptedIn;
    }
    
    public static boolean isCompatVectorFromResourcesEnabled() {
        return VectorEnabledTintResources.isCompatVectorFromResourcesEnabled();
    }
    
    static void removeActivityDelegate(final AppCompatDelegate appCompatDelegate) {
        synchronized (AppCompatDelegate.sActivityDelegatesLock) {
            removeDelegateFromActives(appCompatDelegate);
        }
    }
    
    private static void removeDelegateFromActives(final AppCompatDelegate appCompatDelegate) {
        synchronized (AppCompatDelegate.sActivityDelegatesLock) {
            final Iterator<WeakReference<AppCompatDelegate>> iterator = AppCompatDelegate.sActivityDelegates.iterator();
            while (iterator.hasNext()) {
                final AppCompatDelegate appCompatDelegate2 = iterator.next().get();
                if (appCompatDelegate2 == appCompatDelegate || appCompatDelegate2 == null) {
                    iterator.remove();
                }
            }
        }
    }
    
    static void resetStaticRequestedAndStoredLocales() {
        AppCompatDelegate.sRequestedAppLocales = null;
        AppCompatDelegate.sStoredAppLocales = null;
    }
    
    public static void setApplicationLocales(final LocaleListCompat localeListCompat) {
        Objects.requireNonNull(localeListCompat);
        if (BuildCompat.isAtLeastT()) {
            final Object localeManagerForApplication = getLocaleManagerForApplication();
            if (localeManagerForApplication != null) {
                Api33Impl.localeManagerSetApplicationLocales(localeManagerForApplication, Api24Impl.localeListForLanguageTags(localeListCompat.toLanguageTags()));
            }
        }
        else if (!localeListCompat.equals(AppCompatDelegate.sRequestedAppLocales)) {
            synchronized (AppCompatDelegate.sActivityDelegatesLock) {
                AppCompatDelegate.sRequestedAppLocales = localeListCompat;
                applyLocalesToActiveDelegates();
            }
        }
    }
    
    public static void setCompatVectorFromResourcesEnabled(final boolean compatVectorFromResourcesEnabled) {
        VectorEnabledTintResources.setCompatVectorFromResourcesEnabled(compatVectorFromResourcesEnabled);
    }
    
    public static void setDefaultNightMode(final int sDefaultNightMode) {
        if (sDefaultNightMode != -1 && sDefaultNightMode != 0 && sDefaultNightMode != 1 && sDefaultNightMode != 2 && sDefaultNightMode != 3) {
            Log.d("AppCompatDelegate", "setDefaultNightMode() called with an unknown mode");
        }
        else if (AppCompatDelegate.sDefaultNightMode != sDefaultNightMode) {
            AppCompatDelegate.sDefaultNightMode = sDefaultNightMode;
            applyDayNightToActiveDelegates();
        }
    }
    
    static void setIsAutoStoreLocalesOptedIn(final boolean b) {
        AppCompatDelegate.sIsAutoStoreLocalesOptedIn = b;
    }
    
    static void syncRequestedAndStoredLocales(final Context context) {
        if (!isAutoStorageOptedIn(context)) {
            return;
        }
        if (BuildCompat.isAtLeastT()) {
            if (!AppCompatDelegate.sIsFrameworkSyncChecked) {
                AppCompatDelegate.sSerialExecutorForLocalesStorage.execute(new AppCompatDelegate$$ExternalSyntheticLambda1(context));
            }
            return;
        }
        synchronized (AppCompatDelegate.sAppLocalesStorageSyncLock) {
            final LocaleListCompat sRequestedAppLocales = AppCompatDelegate.sRequestedAppLocales;
            if (sRequestedAppLocales == null) {
                if (AppCompatDelegate.sStoredAppLocales == null) {
                    AppCompatDelegate.sStoredAppLocales = LocaleListCompat.forLanguageTags(AppLocalesStorageHelper.readLocales(context));
                }
                if (AppCompatDelegate.sStoredAppLocales.isEmpty()) {
                    return;
                }
                AppCompatDelegate.sRequestedAppLocales = AppCompatDelegate.sStoredAppLocales;
            }
            else if (!sRequestedAppLocales.equals(AppCompatDelegate.sStoredAppLocales)) {
                AppLocalesStorageHelper.persistLocales(context, (AppCompatDelegate.sStoredAppLocales = AppCompatDelegate.sRequestedAppLocales).toLanguageTags());
            }
        }
    }
    
    public abstract void addContentView(final View p0, final ViewGroup$LayoutParams p1);
    
    boolean applyAppLocales() {
        return false;
    }
    
    public abstract boolean applyDayNight();
    
    void asyncExecuteSyncRequestedAndStoredLocales(final Context context) {
        AppCompatDelegate.sSerialExecutorForLocalesStorage.execute(new AppCompatDelegate$$ExternalSyntheticLambda0(context));
    }
    
    @Deprecated
    public void attachBaseContext(final Context context) {
    }
    
    public Context attachBaseContext2(final Context context) {
        this.attachBaseContext(context);
        return context;
    }
    
    public abstract View createView(final View p0, final String p1, final Context p2, final AttributeSet p3);
    
    public abstract <T extends View> T findViewById(final int p0);
    
    public Context getContextForDelegate() {
        return null;
    }
    
    public abstract ActionBarDrawerToggle.Delegate getDrawerToggleDelegate();
    
    public int getLocalNightMode() {
        return -100;
    }
    
    public abstract MenuInflater getMenuInflater();
    
    public abstract ActionBar getSupportActionBar();
    
    public abstract boolean hasWindowFeature(final int p0);
    
    public abstract void installViewFactory();
    
    public abstract void invalidateOptionsMenu();
    
    public abstract boolean isHandleNativeActionModesEnabled();
    
    public abstract void onConfigurationChanged(final Configuration p0);
    
    public abstract void onCreate(final Bundle p0);
    
    public abstract void onDestroy();
    
    public abstract void onPostCreate(final Bundle p0);
    
    public abstract void onPostResume();
    
    public abstract void onSaveInstanceState(final Bundle p0);
    
    public abstract void onStart();
    
    public abstract void onStop();
    
    public abstract boolean requestWindowFeature(final int p0);
    
    public abstract void setContentView(final int p0);
    
    public abstract void setContentView(final View p0);
    
    public abstract void setContentView(final View p0, final ViewGroup$LayoutParams p1);
    
    public abstract void setHandleNativeActionModesEnabled(final boolean p0);
    
    public abstract void setLocalNightMode(final int p0);
    
    public void setOnBackInvokedDispatcher(final OnBackInvokedDispatcher onBackInvokedDispatcher) {
    }
    
    public abstract void setSupportActionBar(final Toolbar p0);
    
    public void setTheme(final int n) {
    }
    
    public abstract void setTitle(final CharSequence p0);
    
    public abstract ActionMode startSupportActionMode(final ActionMode.Callback p0);
    
    static class Api24Impl
    {
        private Api24Impl() {
        }
        
        static LocaleList localeListForLanguageTags(final String s) {
            return LocaleList.forLanguageTags(s);
        }
    }
    
    static class Api33Impl
    {
        private Api33Impl() {
        }
        
        static LocaleList localeManagerGetApplicationLocales(final Object o) {
            return ((LocaleManager)o).getApplicationLocales();
        }
        
        static void localeManagerSetApplicationLocales(final Object o, final LocaleList applicationLocales) {
            ((LocaleManager)o).setApplicationLocales(applicationLocales);
        }
    }
    
    @Retention(RetentionPolicy.SOURCE)
    public @interface NightMode {
    }
}

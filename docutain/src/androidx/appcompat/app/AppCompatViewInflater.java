// 
// Decompiled by Procyon v0.6.0
// 

package androidx.appcompat.app;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import androidx.appcompat.widget.TintContextWrapper;
import androidx.appcompat.widget.AppCompatToggleButton;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.AppCompatSpinner;
import androidx.appcompat.widget.AppCompatSeekBar;
import androidx.appcompat.widget.AppCompatRatingBar;
import androidx.appcompat.widget.AppCompatRadioButton;
import androidx.appcompat.widget.AppCompatMultiAutoCompleteTextView;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatImageButton;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatCheckedTextView;
import androidx.appcompat.widget.AppCompatCheckBox;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatAutoCompleteTextView;
import androidx.appcompat.view.ContextThemeWrapper;
import android.util.Log;
import androidx.appcompat.R;
import android.view.InflateException;
import android.view.View$OnClickListener;
import android.content.ContextWrapper;
import android.content.res.TypedArray;
import androidx.core.view.ViewCompat;
import android.os.Build$VERSION;
import android.util.AttributeSet;
import android.content.Context;
import android.view.View;
import java.lang.reflect.Constructor;
import androidx.collection.SimpleArrayMap;

public class AppCompatViewInflater
{
    private static final String LOG_TAG = "AppCompatViewInflater";
    private static final int[] sAccessibilityHeading;
    private static final int[] sAccessibilityPaneTitle;
    private static final String[] sClassPrefixList;
    private static final SimpleArrayMap<String, Constructor<? extends View>> sConstructorMap;
    private static final Class<?>[] sConstructorSignature;
    private static final int[] sOnClickAttrs;
    private static final int[] sScreenReaderFocusable;
    private final Object[] mConstructorArgs;
    
    static {
        sConstructorSignature = new Class[] { Context.class, AttributeSet.class };
        sOnClickAttrs = new int[] { 16843375 };
        sAccessibilityHeading = new int[] { 16844160 };
        sAccessibilityPaneTitle = new int[] { 16844156 };
        sScreenReaderFocusable = new int[] { 16844148 };
        sClassPrefixList = new String[] { "android.widget.", "android.view.", "android.webkit." };
        sConstructorMap = new SimpleArrayMap<String, Constructor<? extends View>>();
    }
    
    public AppCompatViewInflater() {
        this.mConstructorArgs = new Object[2];
    }
    
    private void backportAccessibilityAttributes(final Context context, final View view, final AttributeSet set) {
        if (Build$VERSION.SDK_INT >= 19) {
            if (Build$VERSION.SDK_INT <= 28) {
                final TypedArray obtainStyledAttributes = context.obtainStyledAttributes(set, AppCompatViewInflater.sAccessibilityHeading);
                if (obtainStyledAttributes.hasValue(0)) {
                    ViewCompat.setAccessibilityHeading(view, obtainStyledAttributes.getBoolean(0, false));
                }
                obtainStyledAttributes.recycle();
                final TypedArray obtainStyledAttributes2 = context.obtainStyledAttributes(set, AppCompatViewInflater.sAccessibilityPaneTitle);
                if (obtainStyledAttributes2.hasValue(0)) {
                    ViewCompat.setAccessibilityPaneTitle(view, obtainStyledAttributes2.getString(0));
                }
                obtainStyledAttributes2.recycle();
                final TypedArray obtainStyledAttributes3 = context.obtainStyledAttributes(set, AppCompatViewInflater.sScreenReaderFocusable);
                if (obtainStyledAttributes3.hasValue(0)) {
                    ViewCompat.setScreenReaderFocusable(view, obtainStyledAttributes3.getBoolean(0, false));
                }
                obtainStyledAttributes3.recycle();
            }
        }
    }
    
    private void checkOnClickListener(final View view, final AttributeSet set) {
        final Context context = view.getContext();
        if (context instanceof ContextWrapper) {
            if (Build$VERSION.SDK_INT < 15 || ViewCompat.hasOnClickListeners(view)) {
                final TypedArray obtainStyledAttributes = context.obtainStyledAttributes(set, AppCompatViewInflater.sOnClickAttrs);
                final String string = obtainStyledAttributes.getString(0);
                if (string != null) {
                    view.setOnClickListener((View$OnClickListener)new DeclaredOnClickListener(view, string));
                }
                obtainStyledAttributes.recycle();
            }
        }
    }
    
    private View createViewByPrefix(final Context context, final String str, String string) throws ClassNotFoundException, InflateException {
        final SimpleArrayMap<String, Constructor<? extends View>> sConstructorMap = AppCompatViewInflater.sConstructorMap;
        Label_0095: {
            Constructor<? extends View> constructor;
            if ((constructor = sConstructorMap.get(str)) != null) {
                break Label_0095;
            }
            Label_0062: {
                if (string == null) {
                    break Label_0062;
                }
                try {
                    final StringBuilder sb = new StringBuilder();
                    sb.append(string);
                    sb.append(str);
                    string = sb.toString();
                    while (true) {
                        constructor = Class.forName(string, false, context.getClassLoader()).asSubclass(View.class).getConstructor(AppCompatViewInflater.sConstructorSignature);
                        sConstructorMap.put(str, constructor);
                        constructor.setAccessible(true);
                        return (View)constructor.newInstance(this.mConstructorArgs);
                        string = str;
                        continue;
                    }
                }
                catch (final Exception ex) {
                    return null;
                }
            }
        }
    }
    
    private View createViewFromTag(final Context context, final String s, final AttributeSet set) {
        String attributeValue = s;
        if (s.equals("view")) {
            attributeValue = set.getAttributeValue((String)null, "class");
        }
        try {
            final Object[] mConstructorArgs = this.mConstructorArgs;
            mConstructorArgs[0] = context;
            mConstructorArgs[1] = set;
            if (-1 != attributeValue.indexOf(46)) {
                return this.createViewByPrefix(context, attributeValue, null);
            }
            int n = 0;
            while (true) {
                final String[] sClassPrefixList = AppCompatViewInflater.sClassPrefixList;
                if (n >= sClassPrefixList.length) {
                    return null;
                }
                final View viewByPrefix = this.createViewByPrefix(context, attributeValue, sClassPrefixList[n]);
                if (viewByPrefix != null) {
                    return viewByPrefix;
                }
                ++n;
            }
        }
        catch (final Exception ex) {
            return null;
        }
        finally {
            final Object[] mConstructorArgs2 = this.mConstructorArgs;
            mConstructorArgs2[1] = (mConstructorArgs2[0] = null);
        }
    }
    
    private static Context themifyContext(final Context context, final AttributeSet set, final boolean b, final boolean b2) {
        final TypedArray obtainStyledAttributes = context.obtainStyledAttributes(set, R.styleable.View, 0, 0);
        int resourceId;
        if (b) {
            resourceId = obtainStyledAttributes.getResourceId(R.styleable.View_android_theme, 0);
        }
        else {
            resourceId = 0;
        }
        int n = resourceId;
        if (b2 && (n = resourceId) == 0) {
            final int resourceId2 = obtainStyledAttributes.getResourceId(R.styleable.View_theme, 0);
            if ((n = resourceId2) != 0) {
                Log.i("AppCompatViewInflater", "app:theme is now deprecated. Please move to using android:theme instead.");
                n = resourceId2;
            }
        }
        obtainStyledAttributes.recycle();
        Object o = context;
        if (n != 0) {
            if (context instanceof ContextThemeWrapper) {
                o = context;
                if (((ContextThemeWrapper)context).getThemeResId() == n) {
                    return (Context)o;
                }
            }
            o = new ContextThemeWrapper(context, n);
        }
        return (Context)o;
    }
    
    private void verifyNotNull(final View view, final String str) {
        if (view != null) {
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append(this.getClass().getName());
        sb.append(" asked to inflate view for <");
        sb.append(str);
        sb.append(">, but returned null");
        throw new IllegalStateException(sb.toString());
    }
    
    protected AppCompatAutoCompleteTextView createAutoCompleteTextView(final Context context, final AttributeSet set) {
        return new AppCompatAutoCompleteTextView(context, set);
    }
    
    protected AppCompatButton createButton(final Context context, final AttributeSet set) {
        return new AppCompatButton(context, set);
    }
    
    protected AppCompatCheckBox createCheckBox(final Context context, final AttributeSet set) {
        return new AppCompatCheckBox(context, set);
    }
    
    protected AppCompatCheckedTextView createCheckedTextView(final Context context, final AttributeSet set) {
        return new AppCompatCheckedTextView(context, set);
    }
    
    protected AppCompatEditText createEditText(final Context context, final AttributeSet set) {
        return new AppCompatEditText(context, set);
    }
    
    protected AppCompatImageButton createImageButton(final Context context, final AttributeSet set) {
        return new AppCompatImageButton(context, set);
    }
    
    protected AppCompatImageView createImageView(final Context context, final AttributeSet set) {
        return new AppCompatImageView(context, set);
    }
    
    protected AppCompatMultiAutoCompleteTextView createMultiAutoCompleteTextView(final Context context, final AttributeSet set) {
        return new AppCompatMultiAutoCompleteTextView(context, set);
    }
    
    protected AppCompatRadioButton createRadioButton(final Context context, final AttributeSet set) {
        return new AppCompatRadioButton(context, set);
    }
    
    protected AppCompatRatingBar createRatingBar(final Context context, final AttributeSet set) {
        return new AppCompatRatingBar(context, set);
    }
    
    protected AppCompatSeekBar createSeekBar(final Context context, final AttributeSet set) {
        return new AppCompatSeekBar(context, set);
    }
    
    protected AppCompatSpinner createSpinner(final Context context, final AttributeSet set) {
        return new AppCompatSpinner(context, set);
    }
    
    protected AppCompatTextView createTextView(final Context context, final AttributeSet set) {
        return new AppCompatTextView(context, set);
    }
    
    protected AppCompatToggleButton createToggleButton(final Context context, final AttributeSet set) {
        return new AppCompatToggleButton(context, set);
    }
    
    protected View createView(final Context context, final String s, final AttributeSet set) {
        return null;
    }
    
    public final View createView(View o, final String s, final Context context, final AttributeSet set, final boolean b, final boolean b2, final boolean b3, final boolean b4) {
        Context context2;
        if (b && o != null) {
            context2 = ((View)o).getContext();
        }
        else {
            context2 = context;
        }
        Context themifyContext = null;
        Label_0046: {
            if (!b2) {
                themifyContext = context2;
                if (!b3) {
                    break Label_0046;
                }
            }
            themifyContext = themifyContext(context2, set, b2, b3);
        }
        Context wrap = themifyContext;
        if (b4) {
            wrap = TintContextWrapper.wrap(themifyContext);
        }
        s.hashCode();
        int n = -1;
        switch (s) {
            case "Button": {
                n = 13;
                break;
            }
            case "EditText": {
                n = 12;
                break;
            }
            case "CheckBox": {
                n = 11;
                break;
            }
            case "AutoCompleteTextView": {
                n = 10;
                break;
            }
            case "ImageView": {
                n = 9;
                break;
            }
            case "ToggleButton": {
                n = 8;
                break;
            }
            case "RadioButton": {
                n = 7;
                break;
            }
            case "Spinner": {
                n = 6;
                break;
            }
            case "SeekBar": {
                n = 5;
                break;
            }
            case "ImageButton": {
                n = 4;
                break;
            }
            case "TextView": {
                n = 3;
                break;
            }
            case "MultiAutoCompleteTextView": {
                n = 2;
                break;
            }
            case "CheckedTextView": {
                n = 1;
                break;
            }
            case "RatingBar": {
                n = 0;
                break;
            }
            default:
                break;
        }
        switch (n) {
            default: {
                o = this.createView(wrap, s, set);
                break;
            }
            case 13: {
                o = this.createButton(wrap, set);
                this.verifyNotNull((View)o, s);
                break;
            }
            case 12: {
                o = this.createEditText(wrap, set);
                this.verifyNotNull((View)o, s);
                break;
            }
            case 11: {
                o = this.createCheckBox(wrap, set);
                this.verifyNotNull((View)o, s);
                break;
            }
            case 10: {
                o = this.createAutoCompleteTextView(wrap, set);
                this.verifyNotNull((View)o, s);
                break;
            }
            case 9: {
                o = this.createImageView(wrap, set);
                this.verifyNotNull((View)o, s);
                break;
            }
            case 8: {
                o = this.createToggleButton(wrap, set);
                this.verifyNotNull((View)o, s);
                break;
            }
            case 7: {
                o = this.createRadioButton(wrap, set);
                this.verifyNotNull((View)o, s);
                break;
            }
            case 6: {
                o = this.createSpinner(wrap, set);
                this.verifyNotNull((View)o, s);
                break;
            }
            case 5: {
                o = this.createSeekBar(wrap, set);
                this.verifyNotNull((View)o, s);
                break;
            }
            case 4: {
                o = this.createImageButton(wrap, set);
                this.verifyNotNull((View)o, s);
                break;
            }
            case 3: {
                o = this.createTextView(wrap, set);
                this.verifyNotNull((View)o, s);
                break;
            }
            case 2: {
                o = this.createMultiAutoCompleteTextView(wrap, set);
                this.verifyNotNull((View)o, s);
                break;
            }
            case 1: {
                o = this.createCheckedTextView(wrap, set);
                this.verifyNotNull((View)o, s);
                break;
            }
            case 0: {
                o = this.createRatingBar(wrap, set);
                this.verifyNotNull((View)o, s);
                break;
            }
        }
        Object viewFromTag = o;
        if (o == null) {
            viewFromTag = o;
            if (context != wrap) {
                viewFromTag = this.createViewFromTag(wrap, s, set);
            }
        }
        if (viewFromTag != null) {
            this.checkOnClickListener((View)viewFromTag, set);
            this.backportAccessibilityAttributes(wrap, (View)viewFromTag, set);
        }
        return (View)viewFromTag;
    }
    
    private static class DeclaredOnClickListener implements View$OnClickListener
    {
        private final View mHostView;
        private final String mMethodName;
        private Context mResolvedContext;
        private Method mResolvedMethod;
        
        public DeclaredOnClickListener(final View mHostView, final String mMethodName) {
            this.mHostView = mHostView;
            this.mMethodName = mMethodName;
        }
        
        private void resolveMethod(Context baseContext) {
            while (baseContext != null) {
                try {
                    if (!baseContext.isRestricted()) {
                        final Method method = baseContext.getClass().getMethod(this.mMethodName, View.class);
                        if (method != null) {
                            this.mResolvedMethod = method;
                            this.mResolvedContext = baseContext;
                            return;
                        }
                    }
                }
                catch (final NoSuchMethodException ex) {}
                if (baseContext instanceof ContextWrapper) {
                    baseContext = ((ContextWrapper)baseContext).getBaseContext();
                }
                else {
                    baseContext = null;
                }
            }
            final int id = this.mHostView.getId();
            String string;
            if (id == -1) {
                string = "";
            }
            else {
                final StringBuilder sb = new StringBuilder();
                sb.append(" with id '");
                sb.append(this.mHostView.getContext().getResources().getResourceEntryName(id));
                sb.append("'");
                string = sb.toString();
            }
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("Could not find method ");
            sb2.append(this.mMethodName);
            sb2.append("(View) in a parent or ancestor Context for android:onClick attribute defined on view ");
            sb2.append(this.mHostView.getClass());
            sb2.append(string);
            throw new IllegalStateException(sb2.toString());
        }
        
        public void onClick(final View view) {
            if (this.mResolvedMethod == null) {
                this.resolveMethod(this.mHostView.getContext());
            }
            try {
                this.mResolvedMethod.invoke(this.mResolvedContext, view);
            }
            catch (final InvocationTargetException cause) {
                throw new IllegalStateException("Could not execute method for android:onClick", cause);
            }
            catch (final IllegalAccessException cause2) {
                throw new IllegalStateException("Could not execute non-public method for android:onClick", cause2);
            }
        }
    }
}

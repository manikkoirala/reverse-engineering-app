// 
// Decompiled by Procyon v0.6.0
// 

package androidx.appcompat.view;

import androidx.appcompat.R;
import android.content.res.AssetManager;
import android.os.Build$VERSION;
import android.content.Context;
import android.content.res.Resources$Theme;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.content.res.Configuration;
import android.content.ContextWrapper;

public class ContextThemeWrapper extends ContextWrapper
{
    private static Configuration sEmptyConfig;
    private LayoutInflater mInflater;
    private Configuration mOverrideConfiguration;
    private Resources mResources;
    private Resources$Theme mTheme;
    private int mThemeResource;
    
    public ContextThemeWrapper() {
        super((Context)null);
    }
    
    public ContextThemeWrapper(final Context context, final int mThemeResource) {
        super(context);
        this.mThemeResource = mThemeResource;
    }
    
    public ContextThemeWrapper(final Context context, final Resources$Theme mTheme) {
        super(context);
        this.mTheme = mTheme;
    }
    
    private Resources getResourcesInternal() {
        if (this.mResources == null) {
            if (this.mOverrideConfiguration != null && (Build$VERSION.SDK_INT < 26 || !isEmptyConfiguration(this.mOverrideConfiguration))) {
                if (Build$VERSION.SDK_INT >= 17) {
                    this.mResources = Api17Impl.createConfigurationContext(this, this.mOverrideConfiguration).getResources();
                }
                else {
                    final Resources resources = super.getResources();
                    final Configuration configuration = new Configuration(resources.getConfiguration());
                    configuration.updateFrom(this.mOverrideConfiguration);
                    this.mResources = new Resources(resources.getAssets(), resources.getDisplayMetrics(), configuration);
                }
            }
            else {
                this.mResources = super.getResources();
            }
        }
        return this.mResources;
    }
    
    private void initializeTheme() {
        final boolean b = this.mTheme == null;
        if (b) {
            this.mTheme = this.getResources().newTheme();
            final Resources$Theme theme = this.getBaseContext().getTheme();
            if (theme != null) {
                this.mTheme.setTo(theme);
            }
        }
        this.onApplyThemeResource(this.mTheme, this.mThemeResource, b);
    }
    
    private static boolean isEmptyConfiguration(final Configuration configuration) {
        if (configuration == null) {
            return true;
        }
        if (ContextThemeWrapper.sEmptyConfig == null) {
            final Configuration sEmptyConfig = new Configuration();
            sEmptyConfig.fontScale = 0.0f;
            ContextThemeWrapper.sEmptyConfig = sEmptyConfig;
        }
        return configuration.equals(ContextThemeWrapper.sEmptyConfig);
    }
    
    public void applyOverrideConfiguration(final Configuration configuration) {
        if (this.mResources != null) {
            throw new IllegalStateException("getResources() or getAssets() has already been called");
        }
        if (this.mOverrideConfiguration == null) {
            this.mOverrideConfiguration = new Configuration(configuration);
            return;
        }
        throw new IllegalStateException("Override configuration has already been set");
    }
    
    protected void attachBaseContext(final Context context) {
        super.attachBaseContext(context);
    }
    
    public AssetManager getAssets() {
        return this.getResources().getAssets();
    }
    
    public Resources getResources() {
        return this.getResourcesInternal();
    }
    
    public Object getSystemService(final String anObject) {
        if ("layout_inflater".equals(anObject)) {
            if (this.mInflater == null) {
                this.mInflater = LayoutInflater.from(this.getBaseContext()).cloneInContext((Context)this);
            }
            return this.mInflater;
        }
        return this.getBaseContext().getSystemService(anObject);
    }
    
    public Resources$Theme getTheme() {
        final Resources$Theme mTheme = this.mTheme;
        if (mTheme != null) {
            return mTheme;
        }
        if (this.mThemeResource == 0) {
            this.mThemeResource = R.style.Theme_AppCompat_Light;
        }
        this.initializeTheme();
        return this.mTheme;
    }
    
    public int getThemeResId() {
        return this.mThemeResource;
    }
    
    protected void onApplyThemeResource(final Resources$Theme resources$Theme, final int n, final boolean b) {
        resources$Theme.applyStyle(n, true);
    }
    
    public void setTheme(final int mThemeResource) {
        if (this.mThemeResource != mThemeResource) {
            this.mThemeResource = mThemeResource;
            this.initializeTheme();
        }
    }
    
    static class Api17Impl
    {
        private Api17Impl() {
        }
        
        static Context createConfigurationContext(final ContextThemeWrapper contextThemeWrapper, final Configuration configuration) {
            return contextThemeWrapper.createConfigurationContext(configuration);
        }
    }
}

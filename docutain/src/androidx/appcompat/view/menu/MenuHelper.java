// 
// Decompiled by Procyon v0.6.0
// 

package androidx.appcompat.view.menu;

interface MenuHelper
{
    void dismiss();
    
    void setPresenterCallback(final MenuPresenter.Callback p0);
}

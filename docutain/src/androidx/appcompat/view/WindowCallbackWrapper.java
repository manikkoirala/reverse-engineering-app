// 
// Decompiled by Procyon v0.6.0
// 

package androidx.appcompat.view;

import android.view.ActionMode$Callback;
import android.view.WindowManager$LayoutParams;
import android.view.SearchEvent;
import android.view.KeyboardShortcutGroup;
import java.util.List;
import android.view.MenuItem;
import android.view.View;
import android.view.Menu;
import android.view.ActionMode;
import android.view.accessibility.AccessibilityEvent;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.Window$Callback;

public class WindowCallbackWrapper implements Window$Callback
{
    final Window$Callback mWrapped;
    
    public WindowCallbackWrapper(final Window$Callback mWrapped) {
        if (mWrapped != null) {
            this.mWrapped = mWrapped;
            return;
        }
        throw new IllegalArgumentException("Window callback may not be null");
    }
    
    public boolean dispatchGenericMotionEvent(final MotionEvent motionEvent) {
        return this.mWrapped.dispatchGenericMotionEvent(motionEvent);
    }
    
    public boolean dispatchKeyEvent(final KeyEvent keyEvent) {
        return this.mWrapped.dispatchKeyEvent(keyEvent);
    }
    
    public boolean dispatchKeyShortcutEvent(final KeyEvent keyEvent) {
        return this.mWrapped.dispatchKeyShortcutEvent(keyEvent);
    }
    
    public boolean dispatchPopulateAccessibilityEvent(final AccessibilityEvent accessibilityEvent) {
        return this.mWrapped.dispatchPopulateAccessibilityEvent(accessibilityEvent);
    }
    
    public boolean dispatchTouchEvent(final MotionEvent motionEvent) {
        return this.mWrapped.dispatchTouchEvent(motionEvent);
    }
    
    public boolean dispatchTrackballEvent(final MotionEvent motionEvent) {
        return this.mWrapped.dispatchTrackballEvent(motionEvent);
    }
    
    public final Window$Callback getWrapped() {
        return this.mWrapped;
    }
    
    public void onActionModeFinished(final ActionMode actionMode) {
        this.mWrapped.onActionModeFinished(actionMode);
    }
    
    public void onActionModeStarted(final ActionMode actionMode) {
        this.mWrapped.onActionModeStarted(actionMode);
    }
    
    public void onAttachedToWindow() {
        this.mWrapped.onAttachedToWindow();
    }
    
    public void onContentChanged() {
        this.mWrapped.onContentChanged();
    }
    
    public boolean onCreatePanelMenu(final int n, final Menu menu) {
        return this.mWrapped.onCreatePanelMenu(n, menu);
    }
    
    public View onCreatePanelView(final int n) {
        return this.mWrapped.onCreatePanelView(n);
    }
    
    public void onDetachedFromWindow() {
        this.mWrapped.onDetachedFromWindow();
    }
    
    public boolean onMenuItemSelected(final int n, final MenuItem menuItem) {
        return this.mWrapped.onMenuItemSelected(n, menuItem);
    }
    
    public boolean onMenuOpened(final int n, final Menu menu) {
        return this.mWrapped.onMenuOpened(n, menu);
    }
    
    public void onPanelClosed(final int n, final Menu menu) {
        this.mWrapped.onPanelClosed(n, menu);
    }
    
    public void onPointerCaptureChanged(final boolean b) {
        Api26Impl.onPointerCaptureChanged(this.mWrapped, b);
    }
    
    public boolean onPreparePanel(final int n, final View view, final Menu menu) {
        return this.mWrapped.onPreparePanel(n, view, menu);
    }
    
    public void onProvideKeyboardShortcuts(final List<KeyboardShortcutGroup> list, final Menu menu, final int n) {
        Api24Impl.onProvideKeyboardShortcuts(this.mWrapped, list, menu, n);
    }
    
    public boolean onSearchRequested() {
        return this.mWrapped.onSearchRequested();
    }
    
    public boolean onSearchRequested(final SearchEvent searchEvent) {
        return Api23Impl.onSearchRequested(this.mWrapped, searchEvent);
    }
    
    public void onWindowAttributesChanged(final WindowManager$LayoutParams windowManager$LayoutParams) {
        this.mWrapped.onWindowAttributesChanged(windowManager$LayoutParams);
    }
    
    public void onWindowFocusChanged(final boolean b) {
        this.mWrapped.onWindowFocusChanged(b);
    }
    
    public ActionMode onWindowStartingActionMode(final ActionMode$Callback actionMode$Callback) {
        return this.mWrapped.onWindowStartingActionMode(actionMode$Callback);
    }
    
    public ActionMode onWindowStartingActionMode(final ActionMode$Callback actionMode$Callback, final int n) {
        return Api23Impl.onWindowStartingActionMode(this.mWrapped, actionMode$Callback, n);
    }
    
    static class Api23Impl
    {
        private Api23Impl() {
        }
        
        static boolean onSearchRequested(final Window$Callback window$Callback, final SearchEvent searchEvent) {
            return window$Callback.onSearchRequested(searchEvent);
        }
        
        static ActionMode onWindowStartingActionMode(final Window$Callback window$Callback, final ActionMode$Callback actionMode$Callback, final int n) {
            return window$Callback.onWindowStartingActionMode(actionMode$Callback, n);
        }
    }
    
    static class Api24Impl
    {
        private Api24Impl() {
        }
        
        static void onProvideKeyboardShortcuts(final Window$Callback window$Callback, final List<KeyboardShortcutGroup> list, final Menu menu, final int n) {
            window$Callback.onProvideKeyboardShortcuts((List)list, menu, n);
        }
    }
    
    static class Api26Impl
    {
        private Api26Impl() {
        }
        
        static void onPointerCaptureChanged(final Window$Callback window$Callback, final boolean b) {
            window$Callback.onPointerCaptureChanged(b);
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.appcompat.resources;

import android.content.res.TypedArray;
import org.xmlpull.v1.XmlPullParserException;
import java.io.IOException;
import android.graphics.drawable.Drawable;
import android.content.res.Resources$Theme;
import android.util.AttributeSet;
import org.xmlpull.v1.XmlPullParser;
import android.animation.ObjectAnimator;
import android.util.TypedValue;
import android.content.res.Resources;

public final class Compatibility
{
    private Compatibility() {
    }
    
    public static class Api15Impl
    {
        private Api15Impl() {
        }
        
        public static void getValueForDensity(final Resources resources, final int n, final int n2, final TypedValue typedValue, final boolean b) {
            resources.getValueForDensity(n, n2, typedValue, b);
        }
    }
    
    public static class Api18Impl
    {
        private Api18Impl() {
        }
        
        public static void setAutoCancel(final ObjectAnimator objectAnimator, final boolean autoCancel) {
            objectAnimator.setAutoCancel(autoCancel);
        }
    }
    
    public static class Api21Impl
    {
        private Api21Impl() {
        }
        
        public static Drawable createFromXmlInner(final Resources resources, final XmlPullParser xmlPullParser, final AttributeSet set, final Resources$Theme resources$Theme) throws IOException, XmlPullParserException {
            return Drawable.createFromXmlInner(resources, xmlPullParser, set, resources$Theme);
        }
        
        public static int getChangingConfigurations(final TypedArray typedArray) {
            return typedArray.getChangingConfigurations();
        }
        
        public static void inflate(final Drawable drawable, final Resources resources, final XmlPullParser xmlPullParser, final AttributeSet set, final Resources$Theme resources$Theme) throws IOException, XmlPullParserException {
            drawable.inflate(resources, xmlPullParser, set, resources$Theme);
        }
    }
}

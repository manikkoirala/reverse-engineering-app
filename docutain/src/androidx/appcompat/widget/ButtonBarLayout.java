// 
// Decompiled by Procyon v0.6.0
// 

package androidx.appcompat.widget;

import android.widget.LinearLayout$LayoutParams;
import android.view.View$MeasureSpec;
import android.content.res.TypedArray;
import android.view.View;
import androidx.core.view.ViewCompat;
import androidx.appcompat.R;
import android.util.AttributeSet;
import android.content.Context;
import android.widget.LinearLayout;

public class ButtonBarLayout extends LinearLayout
{
    private static final int PEEK_BUTTON_DP = 16;
    private boolean mAllowStacking;
    private int mLastWidthSize;
    private boolean mStacked;
    
    public ButtonBarLayout(final Context context, final AttributeSet set) {
        super(context, set);
        this.mLastWidthSize = -1;
        final TypedArray obtainStyledAttributes = context.obtainStyledAttributes(set, R.styleable.ButtonBarLayout);
        ViewCompat.saveAttributeDataForStyleable((View)this, context, R.styleable.ButtonBarLayout, set, obtainStyledAttributes, 0, 0);
        this.mAllowStacking = obtainStyledAttributes.getBoolean(R.styleable.ButtonBarLayout_allowStacking, true);
        obtainStyledAttributes.recycle();
        if (this.getOrientation() == 1) {
            this.setStacked(this.mAllowStacking);
        }
    }
    
    private int getNextVisibleChildIndex(int i) {
        while (i < this.getChildCount()) {
            if (this.getChildAt(i).getVisibility() == 0) {
                return i;
            }
            ++i;
        }
        return -1;
    }
    
    private boolean isStacked() {
        return this.mStacked;
    }
    
    private void setStacked(final boolean mStacked) {
        if (this.mStacked != mStacked && (!mStacked || this.mAllowStacking)) {
            this.setOrientation((int)((this.mStacked = mStacked) ? 1 : 0));
            int gravity;
            if (mStacked) {
                gravity = 8388613;
            }
            else {
                gravity = 80;
            }
            this.setGravity(gravity);
            final View viewById = this.findViewById(R.id.spacer);
            if (viewById != null) {
                int visibility;
                if (mStacked) {
                    visibility = 8;
                }
                else {
                    visibility = 4;
                }
                viewById.setVisibility(visibility);
            }
            for (int i = this.getChildCount() - 2; i >= 0; --i) {
                this.bringChildToFront(this.getChildAt(i));
            }
        }
    }
    
    protected void onMeasure(final int n, final int n2) {
        final int size = View$MeasureSpec.getSize(n);
        final boolean mAllowStacking = this.mAllowStacking;
        final int n3 = 0;
        if (mAllowStacking) {
            if (size > this.mLastWidthSize && this.isStacked()) {
                this.setStacked(false);
            }
            this.mLastWidthSize = size;
        }
        int measureSpec;
        boolean b;
        if (!this.isStacked() && View$MeasureSpec.getMode(n) == 1073741824) {
            measureSpec = View$MeasureSpec.makeMeasureSpec(size, Integer.MIN_VALUE);
            b = true;
        }
        else {
            measureSpec = n;
            b = false;
        }
        super.onMeasure(measureSpec, n2);
        boolean b2 = b;
        if (this.mAllowStacking) {
            b2 = b;
            if (!this.isStacked()) {
                final boolean b3 = (this.getMeasuredWidthAndState() & 0xFF000000) == 0x1000000;
                b2 = b;
                if (b3) {
                    this.setStacked(true);
                    b2 = true;
                }
            }
        }
        if (b2) {
            super.onMeasure(n, n2);
        }
        final int nextVisibleChildIndex = this.getNextVisibleChildIndex(0);
        int minimumHeight = n3;
        if (nextVisibleChildIndex >= 0) {
            final View child = this.getChildAt(nextVisibleChildIndex);
            final LinearLayout$LayoutParams linearLayout$LayoutParams = (LinearLayout$LayoutParams)child.getLayoutParams();
            final int n4 = this.getPaddingTop() + child.getMeasuredHeight() + linearLayout$LayoutParams.topMargin + linearLayout$LayoutParams.bottomMargin + 0;
            if (this.isStacked()) {
                final int nextVisibleChildIndex2 = this.getNextVisibleChildIndex(nextVisibleChildIndex + 1);
                minimumHeight = n4;
                if (nextVisibleChildIndex2 >= 0) {
                    minimumHeight = n4 + (this.getChildAt(nextVisibleChildIndex2).getPaddingTop() + (int)(this.getResources().getDisplayMetrics().density * 16.0f));
                }
            }
            else {
                minimumHeight = n4 + this.getPaddingBottom();
            }
        }
        if (ViewCompat.getMinimumHeight((View)this) != minimumHeight) {
            this.setMinimumHeight(minimumHeight);
            if (n2 == 0) {
                super.onMeasure(n, n2);
            }
        }
    }
    
    public void setAllowStacking(final boolean mAllowStacking) {
        if (this.mAllowStacking != mAllowStacking) {
            this.mAllowStacking = mAllowStacking;
            if (!mAllowStacking && this.isStacked()) {
                this.setStacked(false);
            }
            this.requestLayout();
        }
    }
}

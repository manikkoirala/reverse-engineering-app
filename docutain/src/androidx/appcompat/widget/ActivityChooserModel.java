// 
// Decompiled by Procyon v0.6.0
// 

package androidx.appcompat.widget;

import java.math.BigDecimal;
import android.content.ComponentName;
import java.util.Collections;
import java.util.Collection;
import android.os.AsyncTask;
import android.content.pm.ResolveInfo;
import android.text.TextUtils;
import java.util.ArrayList;
import java.util.HashMap;
import android.content.Intent;
import android.content.Context;
import java.util.List;
import java.util.Map;
import android.database.DataSetObservable;

class ActivityChooserModel extends DataSetObservable
{
    static final String ATTRIBUTE_ACTIVITY = "activity";
    static final String ATTRIBUTE_TIME = "time";
    static final String ATTRIBUTE_WEIGHT = "weight";
    static final boolean DEBUG = false;
    private static final int DEFAULT_ACTIVITY_INFLATION = 5;
    private static final float DEFAULT_HISTORICAL_RECORD_WEIGHT = 1.0f;
    public static final String DEFAULT_HISTORY_FILE_NAME = "activity_choser_model_history.xml";
    public static final int DEFAULT_HISTORY_MAX_LENGTH = 50;
    private static final String HISTORY_FILE_EXTENSION = ".xml";
    private static final int INVALID_INDEX = -1;
    static final String LOG_TAG = "ActivityChooserModel";
    static final String TAG_HISTORICAL_RECORD = "historical-record";
    static final String TAG_HISTORICAL_RECORDS = "historical-records";
    private static final Map<String, ActivityChooserModel> sDataModelRegistry;
    private static final Object sRegistryLock;
    private final List<ActivityResolveInfo> mActivities;
    private OnChooseActivityListener mActivityChoserModelPolicy;
    private ActivitySorter mActivitySorter;
    boolean mCanReadHistoricalData;
    final Context mContext;
    private final List<HistoricalRecord> mHistoricalRecords;
    private boolean mHistoricalRecordsChanged;
    final String mHistoryFileName;
    private int mHistoryMaxSize;
    private final Object mInstanceLock;
    private Intent mIntent;
    private boolean mReadShareHistoryCalled;
    private boolean mReloadActivities;
    
    static {
        sRegistryLock = new Object();
        sDataModelRegistry = new HashMap<String, ActivityChooserModel>();
    }
    
    private ActivityChooserModel(final Context context, final String s) {
        this.mInstanceLock = new Object();
        this.mActivities = new ArrayList<ActivityResolveInfo>();
        this.mHistoricalRecords = new ArrayList<HistoricalRecord>();
        this.mActivitySorter = (ActivitySorter)new DefaultSorter();
        this.mHistoryMaxSize = 50;
        this.mCanReadHistoricalData = true;
        this.mReadShareHistoryCalled = false;
        this.mHistoricalRecordsChanged = true;
        this.mReloadActivities = false;
        this.mContext = context.getApplicationContext();
        if (!TextUtils.isEmpty((CharSequence)s) && !s.endsWith(".xml")) {
            final StringBuilder sb = new StringBuilder();
            sb.append(s);
            sb.append(".xml");
            this.mHistoryFileName = sb.toString();
        }
        else {
            this.mHistoryFileName = s;
        }
    }
    
    private boolean addHistoricalRecord(final HistoricalRecord historicalRecord) {
        final boolean add = this.mHistoricalRecords.add(historicalRecord);
        if (add) {
            this.mHistoricalRecordsChanged = true;
            this.pruneExcessiveHistoricalRecordsIfNeeded();
            this.persistHistoricalDataIfNeeded();
            this.sortActivitiesIfNeeded();
            this.notifyChanged();
        }
        return add;
    }
    
    private void ensureConsistentState() {
        final boolean loadActivitiesIfNeeded = this.loadActivitiesIfNeeded();
        final boolean historicalDataIfNeeded = this.readHistoricalDataIfNeeded();
        this.pruneExcessiveHistoricalRecordsIfNeeded();
        if (loadActivitiesIfNeeded | historicalDataIfNeeded) {
            this.sortActivitiesIfNeeded();
            this.notifyChanged();
        }
    }
    
    public static ActivityChooserModel get(final Context context, final String s) {
        synchronized (ActivityChooserModel.sRegistryLock) {
            final Map<String, ActivityChooserModel> sDataModelRegistry = ActivityChooserModel.sDataModelRegistry;
            ActivityChooserModel activityChooserModel;
            if ((activityChooserModel = sDataModelRegistry.get(s)) == null) {
                activityChooserModel = new ActivityChooserModel(context, s);
                sDataModelRegistry.put(s, activityChooserModel);
            }
            return activityChooserModel;
        }
    }
    
    private boolean loadActivitiesIfNeeded() {
        final boolean mReloadActivities = this.mReloadActivities;
        int i = 0;
        if (mReloadActivities && this.mIntent != null) {
            this.mReloadActivities = false;
            this.mActivities.clear();
            for (List queryIntentActivities = this.mContext.getPackageManager().queryIntentActivities(this.mIntent, 0); i < queryIntentActivities.size(); ++i) {
                this.mActivities.add(new ActivityResolveInfo((ResolveInfo)queryIntentActivities.get(i)));
            }
            return true;
        }
        return false;
    }
    
    private void persistHistoricalDataIfNeeded() {
        if (!this.mReadShareHistoryCalled) {
            throw new IllegalStateException("No preceding call to #readHistoricalData");
        }
        if (!this.mHistoricalRecordsChanged) {
            return;
        }
        this.mHistoricalRecordsChanged = false;
        if (!TextUtils.isEmpty((CharSequence)this.mHistoryFileName)) {
            new PersistHistoryAsyncTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new Object[] { new ArrayList(this.mHistoricalRecords), this.mHistoryFileName });
        }
    }
    
    private void pruneExcessiveHistoricalRecordsIfNeeded() {
        final int n = this.mHistoricalRecords.size() - this.mHistoryMaxSize;
        if (n <= 0) {
            return;
        }
        this.mHistoricalRecordsChanged = true;
        for (int i = 0; i < n; ++i) {
            final HistoricalRecord historicalRecord = this.mHistoricalRecords.remove(0);
        }
    }
    
    private boolean readHistoricalDataIfNeeded() {
        if (this.mCanReadHistoricalData && this.mHistoricalRecordsChanged && !TextUtils.isEmpty((CharSequence)this.mHistoryFileName)) {
            this.mCanReadHistoricalData = false;
            this.mReadShareHistoryCalled = true;
            this.readHistoricalDataImpl();
            return true;
        }
        return false;
    }
    
    private void readHistoricalDataImpl() {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: getfield        androidx/appcompat/widget/ActivityChooserModel.mContext:Landroid/content/Context;
        //     4: aload_0        
        //     5: getfield        androidx/appcompat/widget/ActivityChooserModel.mHistoryFileName:Ljava/lang/String;
        //     8: invokevirtual   android/content/Context.openFileInput:(Ljava/lang/String;)Ljava/io/FileInputStream;
        //    11: astore          5
        //    13: invokestatic    android/util/Xml.newPullParser:()Lorg/xmlpull/v1/XmlPullParser;
        //    16: astore          7
        //    18: aload           7
        //    20: aload           5
        //    22: ldc_w           "UTF-8"
        //    25: invokeinterface org/xmlpull/v1/XmlPullParser.setInput:(Ljava/io/InputStream;Ljava/lang/String;)V
        //    30: iconst_0       
        //    31: istore_2       
        //    32: iload_2        
        //    33: iconst_1       
        //    34: if_icmpeq       53
        //    37: iload_2        
        //    38: iconst_2       
        //    39: if_icmpeq       53
        //    42: aload           7
        //    44: invokeinterface org/xmlpull/v1/XmlPullParser.next:()I
        //    49: istore_2       
        //    50: goto            32
        //    53: ldc             "historical-records"
        //    55: aload           7
        //    57: invokeinterface org/xmlpull/v1/XmlPullParser.getName:()Ljava/lang/String;
        //    62: invokevirtual   java/lang/String.equals:(Ljava/lang/Object;)Z
        //    65: ifeq            218
        //    68: aload_0        
        //    69: getfield        androidx/appcompat/widget/ActivityChooserModel.mHistoricalRecords:Ljava/util/List;
        //    72: astore          9
        //    74: aload           9
        //    76: invokeinterface java/util/List.clear:()V
        //    81: aload           7
        //    83: invokeinterface org/xmlpull/v1/XmlPullParser.next:()I
        //    88: istore_2       
        //    89: iload_2        
        //    90: iconst_1       
        //    91: if_icmpne       107
        //    94: aload           5
        //    96: ifnull          353
        //    99: aload           5
        //   101: invokevirtual   java/io/FileInputStream.close:()V
        //   104: goto            353
        //   107: iload_2        
        //   108: iconst_3       
        //   109: if_icmpeq       81
        //   112: iload_2        
        //   113: iconst_4       
        //   114: if_icmpne       120
        //   117: goto            81
        //   120: ldc             "historical-record"
        //   122: aload           7
        //   124: invokeinterface org/xmlpull/v1/XmlPullParser.getName:()Ljava/lang/String;
        //   129: invokevirtual   java/lang/String.equals:(Ljava/lang/Object;)Z
        //   132: ifeq            202
        //   135: aload           7
        //   137: aconst_null    
        //   138: ldc             "activity"
        //   140: invokeinterface org/xmlpull/v1/XmlPullParser.getAttributeValue:(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
        //   145: astore          8
        //   147: aload           7
        //   149: aconst_null    
        //   150: ldc             "time"
        //   152: invokeinterface org/xmlpull/v1/XmlPullParser.getAttributeValue:(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
        //   157: invokestatic    java/lang/Long.parseLong:(Ljava/lang/String;)J
        //   160: lstore_3       
        //   161: aload           7
        //   163: aconst_null    
        //   164: ldc             "weight"
        //   166: invokeinterface org/xmlpull/v1/XmlPullParser.getAttributeValue:(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
        //   171: invokestatic    java/lang/Float.parseFloat:(Ljava/lang/String;)F
        //   174: fstore_1       
        //   175: new             Landroidx/appcompat/widget/ActivityChooserModel$HistoricalRecord;
        //   178: astore          6
        //   180: aload           6
        //   182: aload           8
        //   184: lload_3        
        //   185: fload_1        
        //   186: invokespecial   androidx/appcompat/widget/ActivityChooserModel$HistoricalRecord.<init>:(Ljava/lang/String;JF)V
        //   189: aload           9
        //   191: aload           6
        //   193: invokeinterface java/util/List.add:(Ljava/lang/Object;)Z
        //   198: pop            
        //   199: goto            81
        //   202: new             Lorg/xmlpull/v1/XmlPullParserException;
        //   205: astore          6
        //   207: aload           6
        //   209: ldc_w           "Share records file not well-formed."
        //   212: invokespecial   org/xmlpull/v1/XmlPullParserException.<init>:(Ljava/lang/String;)V
        //   215: aload           6
        //   217: athrow         
        //   218: new             Lorg/xmlpull/v1/XmlPullParserException;
        //   221: astore          6
        //   223: aload           6
        //   225: ldc_w           "Share records file does not start with historical-records tag."
        //   228: invokespecial   org/xmlpull/v1/XmlPullParserException.<init>:(Ljava/lang/String;)V
        //   231: aload           6
        //   233: athrow         
        //   234: astore          6
        //   236: goto            354
        //   239: astore          6
        //   241: getstatic       androidx/appcompat/widget/ActivityChooserModel.LOG_TAG:Ljava/lang/String;
        //   244: astore          8
        //   246: new             Ljava/lang/StringBuilder;
        //   249: astore          7
        //   251: aload           7
        //   253: invokespecial   java/lang/StringBuilder.<init>:()V
        //   256: aload           7
        //   258: ldc_w           "Error reading historical recrod file: "
        //   261: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   264: pop            
        //   265: aload           7
        //   267: aload_0        
        //   268: getfield        androidx/appcompat/widget/ActivityChooserModel.mHistoryFileName:Ljava/lang/String;
        //   271: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   274: pop            
        //   275: aload           8
        //   277: aload           7
        //   279: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   282: aload           6
        //   284: invokestatic    android/util/Log.e:(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //   287: pop            
        //   288: aload           5
        //   290: ifnull          353
        //   293: goto            99
        //   296: astore          7
        //   298: getstatic       androidx/appcompat/widget/ActivityChooserModel.LOG_TAG:Ljava/lang/String;
        //   301: astore          8
        //   303: new             Ljava/lang/StringBuilder;
        //   306: astore          6
        //   308: aload           6
        //   310: invokespecial   java/lang/StringBuilder.<init>:()V
        //   313: aload           6
        //   315: ldc_w           "Error reading historical recrod file: "
        //   318: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   321: pop            
        //   322: aload           6
        //   324: aload_0        
        //   325: getfield        androidx/appcompat/widget/ActivityChooserModel.mHistoryFileName:Ljava/lang/String;
        //   328: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   331: pop            
        //   332: aload           8
        //   334: aload           6
        //   336: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   339: aload           7
        //   341: invokestatic    android/util/Log.e:(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //   344: pop            
        //   345: aload           5
        //   347: ifnull          353
        //   350: goto            99
        //   353: return         
        //   354: aload           5
        //   356: ifnull          364
        //   359: aload           5
        //   361: invokevirtual   java/io/FileInputStream.close:()V
        //   364: aload           6
        //   366: athrow         
        //   367: astore          5
        //   369: return         
        //   370: astore          5
        //   372: goto            353
        //   375: astore          5
        //   377: goto            364
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                                   
        //  -----  -----  -----  -----  ---------------------------------------
        //  0      13     367    370    Ljava/io/FileNotFoundException;
        //  13     30     296    353    Lorg/xmlpull/v1/XmlPullParserException;
        //  13     30     239    296    Ljava/io/IOException;
        //  13     30     234    367    Any
        //  42     50     296    353    Lorg/xmlpull/v1/XmlPullParserException;
        //  42     50     239    296    Ljava/io/IOException;
        //  42     50     234    367    Any
        //  53     81     296    353    Lorg/xmlpull/v1/XmlPullParserException;
        //  53     81     239    296    Ljava/io/IOException;
        //  53     81     234    367    Any
        //  81     89     296    353    Lorg/xmlpull/v1/XmlPullParserException;
        //  81     89     239    296    Ljava/io/IOException;
        //  81     89     234    367    Any
        //  99     104    370    375    Ljava/io/IOException;
        //  120    199    296    353    Lorg/xmlpull/v1/XmlPullParserException;
        //  120    199    239    296    Ljava/io/IOException;
        //  120    199    234    367    Any
        //  202    218    296    353    Lorg/xmlpull/v1/XmlPullParserException;
        //  202    218    239    296    Ljava/io/IOException;
        //  202    218    234    367    Any
        //  218    234    296    353    Lorg/xmlpull/v1/XmlPullParserException;
        //  218    234    239    296    Ljava/io/IOException;
        //  218    234    234    367    Any
        //  241    288    234    367    Any
        //  298    345    234    367    Any
        //  359    364    375    380    Ljava/io/IOException;
        // 
        // The error that occurred was:
        // 
        // java.lang.IndexOutOfBoundsException: Index 165 out of bounds for length 165
        //     at java.base/jdk.internal.util.Preconditions.outOfBounds(Preconditions.java:64)
        //     at java.base/jdk.internal.util.Preconditions.outOfBoundsCheckIndex(Preconditions.java:70)
        //     at java.base/jdk.internal.util.Preconditions.checkIndex(Preconditions.java:266)
        //     at java.base/java.util.Objects.checkIndex(Objects.java:361)
        //     at java.base/java.util.ArrayList.get(ArrayList.java:427)
        //     at com.strobel.decompiler.ast.AstBuilder.convertToAst(AstBuilder.java:3362)
        //     at com.strobel.decompiler.ast.AstBuilder.build(AstBuilder.java:112)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:203)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    private boolean sortActivitiesIfNeeded() {
        if (this.mActivitySorter != null && this.mIntent != null && !this.mActivities.isEmpty() && !this.mHistoricalRecords.isEmpty()) {
            this.mActivitySorter.sort(this.mIntent, this.mActivities, Collections.unmodifiableList((List<? extends HistoricalRecord>)this.mHistoricalRecords));
            return true;
        }
        return false;
    }
    
    public Intent chooseActivity(final int n) {
        synchronized (this.mInstanceLock) {
            if (this.mIntent == null) {
                return null;
            }
            this.ensureConsistentState();
            final ActivityResolveInfo activityResolveInfo = this.mActivities.get(n);
            final ComponentName component = new ComponentName(activityResolveInfo.resolveInfo.activityInfo.packageName, activityResolveInfo.resolveInfo.activityInfo.name);
            final Intent intent = new Intent(this.mIntent);
            intent.setComponent(component);
            if (this.mActivityChoserModelPolicy != null && this.mActivityChoserModelPolicy.onChooseActivity(this, new Intent(intent))) {
                return null;
            }
            this.addHistoricalRecord(new HistoricalRecord(component, System.currentTimeMillis(), 1.0f));
            return intent;
        }
    }
    
    public ResolveInfo getActivity(final int n) {
        synchronized (this.mInstanceLock) {
            this.ensureConsistentState();
            return this.mActivities.get(n).resolveInfo;
        }
    }
    
    public int getActivityCount() {
        synchronized (this.mInstanceLock) {
            this.ensureConsistentState();
            return this.mActivities.size();
        }
    }
    
    public int getActivityIndex(final ResolveInfo resolveInfo) {
        synchronized (this.mInstanceLock) {
            this.ensureConsistentState();
            final List<ActivityResolveInfo> mActivities = this.mActivities;
            for (int size = mActivities.size(), i = 0; i < size; ++i) {
                if (((ActivityResolveInfo)mActivities.get(i)).resolveInfo == resolveInfo) {
                    return i;
                }
            }
            return -1;
        }
    }
    
    public ResolveInfo getDefaultActivity() {
        synchronized (this.mInstanceLock) {
            this.ensureConsistentState();
            if (!this.mActivities.isEmpty()) {
                return this.mActivities.get(0).resolveInfo;
            }
            return null;
        }
    }
    
    public int getHistoryMaxSize() {
        synchronized (this.mInstanceLock) {
            return this.mHistoryMaxSize;
        }
    }
    
    public int getHistorySize() {
        synchronized (this.mInstanceLock) {
            this.ensureConsistentState();
            return this.mHistoricalRecords.size();
        }
    }
    
    public Intent getIntent() {
        synchronized (this.mInstanceLock) {
            return this.mIntent;
        }
    }
    
    public void setActivitySorter(final ActivitySorter mActivitySorter) {
        synchronized (this.mInstanceLock) {
            if (this.mActivitySorter == mActivitySorter) {
                return;
            }
            this.mActivitySorter = mActivitySorter;
            if (this.sortActivitiesIfNeeded()) {
                this.notifyChanged();
            }
        }
    }
    
    public void setDefaultActivity(final int n) {
        synchronized (this.mInstanceLock) {
            this.ensureConsistentState();
            final ActivityResolveInfo activityResolveInfo = this.mActivities.get(n);
            final ActivityResolveInfo activityResolveInfo2 = this.mActivities.get(0);
            float n2;
            if (activityResolveInfo2 != null) {
                n2 = activityResolveInfo2.weight - activityResolveInfo.weight + 5.0f;
            }
            else {
                n2 = 1.0f;
            }
            this.addHistoricalRecord(new HistoricalRecord(new ComponentName(activityResolveInfo.resolveInfo.activityInfo.packageName, activityResolveInfo.resolveInfo.activityInfo.name), System.currentTimeMillis(), n2));
        }
    }
    
    public void setHistoryMaxSize(final int mHistoryMaxSize) {
        synchronized (this.mInstanceLock) {
            if (this.mHistoryMaxSize == mHistoryMaxSize) {
                return;
            }
            this.mHistoryMaxSize = mHistoryMaxSize;
            this.pruneExcessiveHistoricalRecordsIfNeeded();
            if (this.sortActivitiesIfNeeded()) {
                this.notifyChanged();
            }
        }
    }
    
    public void setIntent(final Intent mIntent) {
        synchronized (this.mInstanceLock) {
            if (this.mIntent == mIntent) {
                return;
            }
            this.mIntent = mIntent;
            this.mReloadActivities = true;
            this.ensureConsistentState();
        }
    }
    
    public void setOnChooseActivityListener(final OnChooseActivityListener mActivityChoserModelPolicy) {
        synchronized (this.mInstanceLock) {
            this.mActivityChoserModelPolicy = mActivityChoserModelPolicy;
        }
    }
    
    public interface ActivityChooserModelClient
    {
        void setActivityChooserModel(final ActivityChooserModel p0);
    }
    
    public static final class ActivityResolveInfo implements Comparable<ActivityResolveInfo>
    {
        public final ResolveInfo resolveInfo;
        public float weight;
        
        public ActivityResolveInfo(final ResolveInfo resolveInfo) {
            this.resolveInfo = resolveInfo;
        }
        
        @Override
        public int compareTo(final ActivityResolveInfo activityResolveInfo) {
            return Float.floatToIntBits(activityResolveInfo.weight) - Float.floatToIntBits(this.weight);
        }
        
        @Override
        public boolean equals(final Object o) {
            return this == o || (o != null && this.getClass() == o.getClass() && Float.floatToIntBits(this.weight) == Float.floatToIntBits(((ActivityResolveInfo)o).weight));
        }
        
        @Override
        public int hashCode() {
            return Float.floatToIntBits(this.weight) + 31;
        }
        
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder();
            sb.append("[");
            sb.append("resolveInfo:");
            sb.append(this.resolveInfo.toString());
            sb.append("; weight:");
            sb.append(new BigDecimal(this.weight));
            sb.append("]");
            return sb.toString();
        }
    }
    
    public interface ActivitySorter
    {
        void sort(final Intent p0, final List<ActivityResolveInfo> p1, final List<HistoricalRecord> p2);
    }
    
    private static final class DefaultSorter implements ActivitySorter
    {
        private static final float WEIGHT_DECAY_COEFFICIENT = 0.95f;
        private final Map<ComponentName, ActivityResolveInfo> mPackageNameToActivityMap;
        
        DefaultSorter() {
            this.mPackageNameToActivityMap = new HashMap<ComponentName, ActivityResolveInfo>();
        }
        
        @Override
        public void sort(final Intent intent, final List<ActivityResolveInfo> list, final List<HistoricalRecord> list2) {
            final Map<ComponentName, ActivityResolveInfo> mPackageNameToActivityMap = this.mPackageNameToActivityMap;
            mPackageNameToActivityMap.clear();
            for (int size = list.size(), i = 0; i < size; ++i) {
                final ActivityResolveInfo activityResolveInfo = list.get(i);
                activityResolveInfo.weight = 0.0f;
                mPackageNameToActivityMap.put(new ComponentName(activityResolveInfo.resolveInfo.activityInfo.packageName, activityResolveInfo.resolveInfo.activityInfo.name), activityResolveInfo);
            }
            int j = list2.size() - 1;
            float n = 1.0f;
            while (j >= 0) {
                final HistoricalRecord historicalRecord = list2.get(j);
                final ActivityResolveInfo activityResolveInfo2 = mPackageNameToActivityMap.get(historicalRecord.activity);
                float n2 = n;
                if (activityResolveInfo2 != null) {
                    activityResolveInfo2.weight += historicalRecord.weight * n;
                    n2 = n * 0.95f;
                }
                --j;
                n = n2;
            }
            Collections.sort((List<Comparable>)list);
        }
    }
    
    public static final class HistoricalRecord
    {
        public final ComponentName activity;
        public final long time;
        public final float weight;
        
        public HistoricalRecord(final ComponentName activity, final long time, final float weight) {
            this.activity = activity;
            this.time = time;
            this.weight = weight;
        }
        
        public HistoricalRecord(final String s, final long n, final float n2) {
            this(ComponentName.unflattenFromString(s), n, n2);
        }
        
        @Override
        public boolean equals(final Object o) {
            if (this == o) {
                return true;
            }
            if (o == null) {
                return false;
            }
            if (this.getClass() != o.getClass()) {
                return false;
            }
            final HistoricalRecord historicalRecord = (HistoricalRecord)o;
            final ComponentName activity = this.activity;
            if (activity == null) {
                if (historicalRecord.activity != null) {
                    return false;
                }
            }
            else if (!activity.equals((Object)historicalRecord.activity)) {
                return false;
            }
            return this.time == historicalRecord.time && Float.floatToIntBits(this.weight) == Float.floatToIntBits(historicalRecord.weight);
        }
        
        @Override
        public int hashCode() {
            final ComponentName activity = this.activity;
            int hashCode;
            if (activity == null) {
                hashCode = 0;
            }
            else {
                hashCode = activity.hashCode();
            }
            final long time = this.time;
            return ((hashCode + 31) * 31 + (int)(time ^ time >>> 32)) * 31 + Float.floatToIntBits(this.weight);
        }
        
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder();
            sb.append("[");
            sb.append("; activity:");
            sb.append(this.activity);
            sb.append("; time:");
            sb.append(this.time);
            sb.append("; weight:");
            sb.append(new BigDecimal(this.weight));
            sb.append("]");
            return sb.toString();
        }
    }
    
    public interface OnChooseActivityListener
    {
        boolean onChooseActivity(final ActivityChooserModel p0, final Intent p1);
    }
    
    private final class PersistHistoryAsyncTask extends AsyncTask<Object, Void, Void>
    {
        final ActivityChooserModel this$0;
        
        PersistHistoryAsyncTask(final ActivityChooserModel this$0) {
            this.this$0 = this$0;
        }
        
        public Void doInBackground(final Object... p0) {
            // 
            // This method could not be decompiled.
            // 
            // Original Bytecode:
            // 
            //     1: iconst_0       
            //     2: aaload         
            //     3: checkcast       Ljava/util/List;
            //     6: astore          5
            //     8: aload_1        
            //     9: iconst_1       
            //    10: aaload         
            //    11: checkcast       Ljava/lang/String;
            //    14: astore          4
            //    16: aload_0        
            //    17: getfield        androidx/appcompat/widget/ActivityChooserModel$PersistHistoryAsyncTask.this$0:Landroidx/appcompat/widget/ActivityChooserModel;
            //    20: getfield        androidx/appcompat/widget/ActivityChooserModel.mContext:Landroid/content/Context;
            //    23: aload           4
            //    25: iconst_0       
            //    26: invokevirtual   android/content/Context.openFileOutput:(Ljava/lang/String;I)Ljava/io/FileOutputStream;
            //    29: astore_1       
            //    30: invokestatic    android/util/Xml.newSerializer:()Lorg/xmlpull/v1/XmlSerializer;
            //    33: astore          6
            //    35: aload           6
            //    37: aload_1        
            //    38: aconst_null    
            //    39: invokeinterface org/xmlpull/v1/XmlSerializer.setOutput:(Ljava/io/OutputStream;Ljava/lang/String;)V
            //    44: aload           6
            //    46: ldc             "UTF-8"
            //    48: iconst_1       
            //    49: invokestatic    java/lang/Boolean.valueOf:(Z)Ljava/lang/Boolean;
            //    52: invokeinterface org/xmlpull/v1/XmlSerializer.startDocument:(Ljava/lang/String;Ljava/lang/Boolean;)V
            //    57: aload           6
            //    59: aconst_null    
            //    60: ldc             "historical-records"
            //    62: invokeinterface org/xmlpull/v1/XmlSerializer.startTag:(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;
            //    67: pop            
            //    68: aload           5
            //    70: invokeinterface java/util/List.size:()I
            //    75: istore_3       
            //    76: iconst_0       
            //    77: istore_2       
            //    78: iload_2        
            //    79: iload_3        
            //    80: if_icmpge       181
            //    83: aload           5
            //    85: iconst_0       
            //    86: invokeinterface java/util/List.remove:(I)Ljava/lang/Object;
            //    91: checkcast       Landroidx/appcompat/widget/ActivityChooserModel$HistoricalRecord;
            //    94: astore          4
            //    96: aload           6
            //    98: aconst_null    
            //    99: ldc             "historical-record"
            //   101: invokeinterface org/xmlpull/v1/XmlSerializer.startTag:(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;
            //   106: pop            
            //   107: aload           6
            //   109: aconst_null    
            //   110: ldc             "activity"
            //   112: aload           4
            //   114: getfield        androidx/appcompat/widget/ActivityChooserModel$HistoricalRecord.activity:Landroid/content/ComponentName;
            //   117: invokevirtual   android/content/ComponentName.flattenToString:()Ljava/lang/String;
            //   120: invokeinterface org/xmlpull/v1/XmlSerializer.attribute:(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;
            //   125: pop            
            //   126: aload           6
            //   128: aconst_null    
            //   129: ldc             "time"
            //   131: aload           4
            //   133: getfield        androidx/appcompat/widget/ActivityChooserModel$HistoricalRecord.time:J
            //   136: invokestatic    java/lang/String.valueOf:(J)Ljava/lang/String;
            //   139: invokeinterface org/xmlpull/v1/XmlSerializer.attribute:(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;
            //   144: pop            
            //   145: aload           6
            //   147: aconst_null    
            //   148: ldc             "weight"
            //   150: aload           4
            //   152: getfield        androidx/appcompat/widget/ActivityChooserModel$HistoricalRecord.weight:F
            //   155: invokestatic    java/lang/String.valueOf:(F)Ljava/lang/String;
            //   158: invokeinterface org/xmlpull/v1/XmlSerializer.attribute:(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;
            //   163: pop            
            //   164: aload           6
            //   166: aconst_null    
            //   167: ldc             "historical-record"
            //   169: invokeinterface org/xmlpull/v1/XmlSerializer.endTag:(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;
            //   174: pop            
            //   175: iinc            2, 1
            //   178: goto            78
            //   181: aload           6
            //   183: aconst_null    
            //   184: ldc             "historical-records"
            //   186: invokeinterface org/xmlpull/v1/XmlSerializer.endTag:(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;
            //   191: pop            
            //   192: aload           6
            //   194: invokeinterface org/xmlpull/v1/XmlSerializer.endDocument:()V
            //   199: aload_0        
            //   200: getfield        androidx/appcompat/widget/ActivityChooserModel$PersistHistoryAsyncTask.this$0:Landroidx/appcompat/widget/ActivityChooserModel;
            //   203: iconst_1       
            //   204: putfield        androidx/appcompat/widget/ActivityChooserModel.mCanReadHistoricalData:Z
            //   207: aload_1        
            //   208: ifnull          421
            //   211: aload_1        
            //   212: invokevirtual   java/io/FileOutputStream.close:()V
            //   215: goto            421
            //   218: astore          4
            //   220: goto            423
            //   223: astore          6
            //   225: getstatic       androidx/appcompat/widget/ActivityChooserModel.LOG_TAG:Ljava/lang/String;
            //   228: astore          4
            //   230: new             Ljava/lang/StringBuilder;
            //   233: astore          5
            //   235: aload           5
            //   237: invokespecial   java/lang/StringBuilder.<init>:()V
            //   240: aload           5
            //   242: ldc             "Error writing historical record file: "
            //   244: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //   247: pop            
            //   248: aload           5
            //   250: aload_0        
            //   251: getfield        androidx/appcompat/widget/ActivityChooserModel$PersistHistoryAsyncTask.this$0:Landroidx/appcompat/widget/ActivityChooserModel;
            //   254: getfield        androidx/appcompat/widget/ActivityChooserModel.mHistoryFileName:Ljava/lang/String;
            //   257: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //   260: pop            
            //   261: aload           4
            //   263: aload           5
            //   265: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
            //   268: aload           6
            //   270: invokestatic    android/util/Log.e:(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
            //   273: pop            
            //   274: aload_0        
            //   275: getfield        androidx/appcompat/widget/ActivityChooserModel$PersistHistoryAsyncTask.this$0:Landroidx/appcompat/widget/ActivityChooserModel;
            //   278: iconst_1       
            //   279: putfield        androidx/appcompat/widget/ActivityChooserModel.mCanReadHistoricalData:Z
            //   282: aload_1        
            //   283: ifnull          421
            //   286: goto            211
            //   289: astore          4
            //   291: getstatic       androidx/appcompat/widget/ActivityChooserModel.LOG_TAG:Ljava/lang/String;
            //   294: astore          6
            //   296: new             Ljava/lang/StringBuilder;
            //   299: astore          5
            //   301: aload           5
            //   303: invokespecial   java/lang/StringBuilder.<init>:()V
            //   306: aload           5
            //   308: ldc             "Error writing historical record file: "
            //   310: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //   313: pop            
            //   314: aload           5
            //   316: aload_0        
            //   317: getfield        androidx/appcompat/widget/ActivityChooserModel$PersistHistoryAsyncTask.this$0:Landroidx/appcompat/widget/ActivityChooserModel;
            //   320: getfield        androidx/appcompat/widget/ActivityChooserModel.mHistoryFileName:Ljava/lang/String;
            //   323: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //   326: pop            
            //   327: aload           6
            //   329: aload           5
            //   331: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
            //   334: aload           4
            //   336: invokestatic    android/util/Log.e:(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
            //   339: pop            
            //   340: aload_0        
            //   341: getfield        androidx/appcompat/widget/ActivityChooserModel$PersistHistoryAsyncTask.this$0:Landroidx/appcompat/widget/ActivityChooserModel;
            //   344: iconst_1       
            //   345: putfield        androidx/appcompat/widget/ActivityChooserModel.mCanReadHistoricalData:Z
            //   348: aload_1        
            //   349: ifnull          421
            //   352: goto            211
            //   355: astore          5
            //   357: getstatic       androidx/appcompat/widget/ActivityChooserModel.LOG_TAG:Ljava/lang/String;
            //   360: astore          4
            //   362: new             Ljava/lang/StringBuilder;
            //   365: astore          6
            //   367: aload           6
            //   369: invokespecial   java/lang/StringBuilder.<init>:()V
            //   372: aload           6
            //   374: ldc             "Error writing historical record file: "
            //   376: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //   379: pop            
            //   380: aload           6
            //   382: aload_0        
            //   383: getfield        androidx/appcompat/widget/ActivityChooserModel$PersistHistoryAsyncTask.this$0:Landroidx/appcompat/widget/ActivityChooserModel;
            //   386: getfield        androidx/appcompat/widget/ActivityChooserModel.mHistoryFileName:Ljava/lang/String;
            //   389: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //   392: pop            
            //   393: aload           4
            //   395: aload           6
            //   397: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
            //   400: aload           5
            //   402: invokestatic    android/util/Log.e:(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
            //   405: pop            
            //   406: aload_0        
            //   407: getfield        androidx/appcompat/widget/ActivityChooserModel$PersistHistoryAsyncTask.this$0:Landroidx/appcompat/widget/ActivityChooserModel;
            //   410: iconst_1       
            //   411: putfield        androidx/appcompat/widget/ActivityChooserModel.mCanReadHistoricalData:Z
            //   414: aload_1        
            //   415: ifnull          421
            //   418: goto            211
            //   421: aconst_null    
            //   422: areturn        
            //   423: aload_0        
            //   424: getfield        androidx/appcompat/widget/ActivityChooserModel$PersistHistoryAsyncTask.this$0:Landroidx/appcompat/widget/ActivityChooserModel;
            //   427: iconst_1       
            //   428: putfield        androidx/appcompat/widget/ActivityChooserModel.mCanReadHistoricalData:Z
            //   431: aload_1        
            //   432: ifnull          439
            //   435: aload_1        
            //   436: invokevirtual   java/io/FileOutputStream.close:()V
            //   439: aload           4
            //   441: athrow         
            //   442: astore_1       
            //   443: getstatic       androidx/appcompat/widget/ActivityChooserModel.LOG_TAG:Ljava/lang/String;
            //   446: astore          5
            //   448: new             Ljava/lang/StringBuilder;
            //   451: dup            
            //   452: invokespecial   java/lang/StringBuilder.<init>:()V
            //   455: astore          6
            //   457: aload           6
            //   459: ldc             "Error writing historical record file: "
            //   461: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //   464: pop            
            //   465: aload           6
            //   467: aload           4
            //   469: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //   472: pop            
            //   473: aload           5
            //   475: aload           6
            //   477: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
            //   480: aload_1        
            //   481: invokestatic    android/util/Log.e:(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
            //   484: pop            
            //   485: aconst_null    
            //   486: areturn        
            //   487: astore_1       
            //   488: goto            421
            //   491: astore_1       
            //   492: goto            439
            //    Exceptions:
            //  Try           Handler
            //  Start  End    Start  End    Type                                
            //  -----  -----  -----  -----  ------------------------------------
            //  16     30     442    487    Ljava/io/FileNotFoundException;
            //  35     76     355    421    Ljava/lang/IllegalArgumentException;
            //  35     76     289    355    Ljava/lang/IllegalStateException;
            //  35     76     223    289    Ljava/io/IOException;
            //  35     76     218    442    Any
            //  83     175    355    421    Ljava/lang/IllegalArgumentException;
            //  83     175    289    355    Ljava/lang/IllegalStateException;
            //  83     175    223    289    Ljava/io/IOException;
            //  83     175    218    442    Any
            //  181    199    355    421    Ljava/lang/IllegalArgumentException;
            //  181    199    289    355    Ljava/lang/IllegalStateException;
            //  181    199    223    289    Ljava/io/IOException;
            //  181    199    218    442    Any
            //  211    215    487    491    Ljava/io/IOException;
            //  225    274    218    442    Any
            //  291    340    218    442    Any
            //  357    406    218    442    Any
            //  435    439    491    495    Ljava/io/IOException;
            // 
            // The error that occurred was:
            // 
            // java.lang.IndexOutOfBoundsException: Index 229 out of bounds for length 229
            //     at java.base/jdk.internal.util.Preconditions.outOfBounds(Preconditions.java:64)
            //     at java.base/jdk.internal.util.Preconditions.outOfBoundsCheckIndex(Preconditions.java:70)
            //     at java.base/jdk.internal.util.Preconditions.checkIndex(Preconditions.java:266)
            //     at java.base/java.util.Objects.checkIndex(Objects.java:361)
            //     at java.base/java.util.ArrayList.get(ArrayList.java:427)
            //     at com.strobel.decompiler.ast.AstBuilder.convertToAst(AstBuilder.java:3362)
            //     at com.strobel.decompiler.ast.AstBuilder.build(AstBuilder.java:112)
            //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:203)
            //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:662)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
            //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
            //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
            //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
            //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
            //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
            // 
            throw new IllegalStateException("An error occurred while decompiling this method.");
        }
    }
}

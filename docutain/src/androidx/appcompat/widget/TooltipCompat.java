// 
// Decompiled by Procyon v0.6.0
// 

package androidx.appcompat.widget;

import android.os.Build$VERSION;
import android.view.View;

public class TooltipCompat
{
    private TooltipCompat() {
    }
    
    public static void setTooltipText(final View view, final CharSequence charSequence) {
        if (Build$VERSION.SDK_INT >= 26) {
            Api26Impl.setTooltipText(view, charSequence);
        }
        else {
            TooltipCompatHandler.setTooltipText(view, charSequence);
        }
    }
    
    static class Api26Impl
    {
        private Api26Impl() {
        }
        
        static void setTooltipText(final View view, final CharSequence tooltipText) {
            view.setTooltipText(tooltipText);
        }
    }
}

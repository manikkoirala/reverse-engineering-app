// 
// Decompiled by Procyon v0.6.0
// 

package androidx.appcompat.widget;

import android.view.View;
import mono.android.TypeManager;
import mono.android.Runtime;
import java.util.ArrayList;
import android.view.View$OnClickListener;
import mono.android.IGCUserPeer;

public class Toolbar_NavigationOnClickEventDispatcher implements IGCUserPeer, View$OnClickListener
{
    public static final String __md_methods = "n_onClick:(Landroid/view/View;)V:GetOnClick_Landroid_view_View_Handler:Android.Views.View/IOnClickListenerInvoker, Mono.Android, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null\n";
    private ArrayList refList;
    
    static {
        Runtime.register("AndroidX.AppCompat.Widget.Toolbar+NavigationOnClickEventDispatcher, Xamarin.AndroidX.AppCompat", (Class)Toolbar_NavigationOnClickEventDispatcher.class, "n_onClick:(Landroid/view/View;)V:GetOnClick_Landroid_view_View_Handler:Android.Views.View/IOnClickListenerInvoker, Mono.Android, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null\n");
    }
    
    public Toolbar_NavigationOnClickEventDispatcher() {
        if (this.getClass() == Toolbar_NavigationOnClickEventDispatcher.class) {
            TypeManager.Activate("AndroidX.AppCompat.Widget.Toolbar+NavigationOnClickEventDispatcher, Xamarin.AndroidX.AppCompat", "", (Object)this, new Object[0]);
        }
    }
    
    public Toolbar_NavigationOnClickEventDispatcher(final Toolbar toolbar) {
        if (this.getClass() == Toolbar_NavigationOnClickEventDispatcher.class) {
            TypeManager.Activate("AndroidX.AppCompat.Widget.Toolbar+NavigationOnClickEventDispatcher, Xamarin.AndroidX.AppCompat", "AndroidX.AppCompat.Widget.Toolbar, Xamarin.AndroidX.AppCompat", (Object)this, new Object[] { toolbar });
        }
    }
    
    private native void n_onClick(final View p0);
    
    public void monodroidAddReference(final Object e) {
        if (this.refList == null) {
            this.refList = new ArrayList();
        }
        this.refList.add(e);
    }
    
    public void monodroidClearReferences() {
        final ArrayList refList = this.refList;
        if (refList != null) {
            refList.clear();
        }
    }
    
    public void onClick(final View view) {
        this.n_onClick(view);
    }
}

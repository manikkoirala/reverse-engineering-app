// 
// Decompiled by Procyon v0.6.0
// 

package androidx.appcompat.widget;

import android.content.res.TypedArray;
import android.text.method.TransformationMethod;
import androidx.appcompat.R;
import android.util.AttributeSet;
import android.text.InputFilter;
import android.widget.TextView;
import androidx.emoji2.viewsintegration.EmojiTextViewHelper;

class AppCompatEmojiTextHelper
{
    private final EmojiTextViewHelper mEmojiTextViewHelper;
    private final TextView mView;
    
    AppCompatEmojiTextHelper(final TextView mView) {
        this.mView = mView;
        this.mEmojiTextViewHelper = new EmojiTextViewHelper(mView, false);
    }
    
    InputFilter[] getFilters(final InputFilter[] array) {
        return this.mEmojiTextViewHelper.getFilters(array);
    }
    
    public boolean isEnabled() {
        return this.mEmojiTextViewHelper.isEnabled();
    }
    
    void loadFromAttributes(AttributeSet obtainStyledAttributes, final int n) {
        obtainStyledAttributes = (AttributeSet)this.mView.getContext().obtainStyledAttributes(obtainStyledAttributes, R.styleable.AppCompatTextView, n, 0);
        try {
            final boolean hasValue = ((TypedArray)obtainStyledAttributes).hasValue(R.styleable.AppCompatTextView_emojiCompatEnabled);
            boolean boolean1 = true;
            if (hasValue) {
                boolean1 = ((TypedArray)obtainStyledAttributes).getBoolean(R.styleable.AppCompatTextView_emojiCompatEnabled, true);
            }
            ((TypedArray)obtainStyledAttributes).recycle();
            this.setEnabled(boolean1);
        }
        finally {
            ((TypedArray)obtainStyledAttributes).recycle();
        }
    }
    
    void setAllCaps(final boolean allCaps) {
        this.mEmojiTextViewHelper.setAllCaps(allCaps);
    }
    
    void setEnabled(final boolean enabled) {
        this.mEmojiTextViewHelper.setEnabled(enabled);
    }
    
    public TransformationMethod wrapTransformationMethod(final TransformationMethod transformationMethod) {
        return this.mEmojiTextViewHelper.wrapTransformationMethod(transformationMethod);
    }
}

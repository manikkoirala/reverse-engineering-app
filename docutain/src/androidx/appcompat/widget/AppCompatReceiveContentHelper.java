// 
// Decompiled by Procyon v0.6.0
// 

package androidx.appcompat.widget;

import android.text.Selection;
import android.text.Spannable;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.ClipData;
import androidx.core.view.ContentInfoCompat;
import android.content.ClipboardManager;
import android.app.Activity;
import android.widget.TextView;
import android.util.Log;
import androidx.core.view.ViewCompat;
import android.os.Build$VERSION;
import android.view.DragEvent;
import android.view.View;

final class AppCompatReceiveContentHelper
{
    private static final String LOG_TAG = "ReceiveContent";
    
    private AppCompatReceiveContentHelper() {
    }
    
    static boolean maybeHandleDragEventViaPerformReceiveContent(final View obj, final DragEvent dragEvent) {
        if (Build$VERSION.SDK_INT < 31 && Build$VERSION.SDK_INT >= 24 && dragEvent.getLocalState() == null) {
            if (ViewCompat.getOnReceiveContentMimeTypes(obj) != null) {
                final Activity tryGetActivity = tryGetActivity(obj);
                if (tryGetActivity == null) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Can't handle drop: no activity: view=");
                    sb.append(obj);
                    Log.i("ReceiveContent", sb.toString());
                    return false;
                }
                if (dragEvent.getAction() == 1) {
                    return obj instanceof TextView ^ true;
                }
                if (dragEvent.getAction() == 3) {
                    boolean b;
                    if (obj instanceof TextView) {
                        b = OnDropApi24Impl.onDropForTextView(dragEvent, (TextView)obj, tryGetActivity);
                    }
                    else {
                        b = OnDropApi24Impl.onDropForView(dragEvent, obj, tryGetActivity);
                    }
                    return b;
                }
            }
        }
        return false;
    }
    
    static boolean maybeHandleMenuActionViaPerformReceiveContent(final TextView textView, int flags) {
        final int sdk_INT = Build$VERSION.SDK_INT;
        final int n = 0;
        if (sdk_INT < 31 && ViewCompat.getOnReceiveContentMimeTypes((View)textView) != null && (flags == 16908322 || flags == 16908337)) {
            final ClipboardManager clipboardManager = (ClipboardManager)textView.getContext().getSystemService("clipboard");
            ClipData primaryClip;
            if (clipboardManager == null) {
                primaryClip = null;
            }
            else {
                primaryClip = clipboardManager.getPrimaryClip();
            }
            if (primaryClip != null && primaryClip.getItemCount() > 0) {
                final ContentInfoCompat.Builder builder = new ContentInfoCompat.Builder(primaryClip, 1);
                if (flags == 16908322) {
                    flags = n;
                }
                else {
                    flags = 1;
                }
                ViewCompat.performReceiveContent((View)textView, builder.setFlags(flags).build());
            }
            return true;
        }
        return false;
    }
    
    static Activity tryGetActivity(final View view) {
        for (Context context = view.getContext(); context instanceof ContextWrapper; context = ((ContextWrapper)context).getBaseContext()) {
            if (context instanceof Activity) {
                return (Activity)context;
            }
        }
        return null;
    }
    
    private static final class OnDropApi24Impl
    {
        static boolean onDropForTextView(final DragEvent dragEvent, final TextView textView, final Activity activity) {
            activity.requestDragAndDropPermissions(dragEvent);
            final int offsetForPosition = textView.getOffsetForPosition(dragEvent.getX(), dragEvent.getY());
            textView.beginBatchEdit();
            try {
                Selection.setSelection((Spannable)textView.getText(), offsetForPosition);
                ViewCompat.performReceiveContent((View)textView, new ContentInfoCompat.Builder(dragEvent.getClipData(), 3).build());
                return true;
            }
            finally {
                textView.endBatchEdit();
            }
        }
        
        static boolean onDropForView(final DragEvent dragEvent, final View view, final Activity activity) {
            activity.requestDragAndDropPermissions(dragEvent);
            ViewCompat.performReceiveContent(view, new ContentInfoCompat.Builder(dragEvent.getClipData(), 3).build());
            return true;
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.appcompat.widget;

import android.view.textclassifier.TextClassificationManager;
import androidx.core.util.Preconditions;
import android.widget.TextView;
import android.view.textclassifier.TextClassifier;

final class AppCompatTextClassifierHelper
{
    private TextClassifier mTextClassifier;
    private TextView mTextView;
    
    AppCompatTextClassifierHelper(final TextView textView) {
        this.mTextView = Preconditions.checkNotNull(textView);
    }
    
    public TextClassifier getTextClassifier() {
        TextClassifier textClassifier;
        if ((textClassifier = this.mTextClassifier) == null) {
            textClassifier = Api26Impl.getTextClassifier(this.mTextView);
        }
        return textClassifier;
    }
    
    public void setTextClassifier(final TextClassifier mTextClassifier) {
        this.mTextClassifier = mTextClassifier;
    }
    
    private static final class Api26Impl
    {
        static TextClassifier getTextClassifier(final TextView textView) {
            final TextClassificationManager textClassificationManager = (TextClassificationManager)textView.getContext().getSystemService((Class)TextClassificationManager.class);
            if (textClassificationManager != null) {
                return textClassificationManager.getTextClassifier();
            }
            return TextClassifier.NO_OP;
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.appcompat.widget;

public interface EmojiCompatConfigurationView
{
    boolean isEmojiCompatEnabled();
    
    void setEmojiCompatEnabled(final boolean p0);
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.appcompat.widget;

import android.content.res.Resources$NotFoundException;
import androidx.appcompat.content.res.AppCompatResources;
import android.view.View;
import androidx.core.view.ViewCompat;
import androidx.appcompat.R;
import android.util.AttributeSet;
import android.graphics.drawable.Drawable;
import androidx.core.graphics.drawable.DrawableCompat;
import androidx.core.widget.CheckedTextViewCompat;
import android.widget.CheckedTextView;
import android.graphics.PorterDuff$Mode;
import android.content.res.ColorStateList;

class AppCompatCheckedTextViewHelper
{
    private ColorStateList mCheckMarkTintList;
    private PorterDuff$Mode mCheckMarkTintMode;
    private boolean mHasCheckMarkTint;
    private boolean mHasCheckMarkTintMode;
    private boolean mSkipNextApply;
    private final CheckedTextView mView;
    
    AppCompatCheckedTextViewHelper(final CheckedTextView mView) {
        this.mCheckMarkTintList = null;
        this.mCheckMarkTintMode = null;
        this.mHasCheckMarkTint = false;
        this.mHasCheckMarkTintMode = false;
        this.mView = mView;
    }
    
    void applyCheckMarkTint() {
        final Drawable checkMarkDrawable = CheckedTextViewCompat.getCheckMarkDrawable(this.mView);
        if (checkMarkDrawable != null && (this.mHasCheckMarkTint || this.mHasCheckMarkTintMode)) {
            final Drawable mutate = DrawableCompat.wrap(checkMarkDrawable).mutate();
            if (this.mHasCheckMarkTint) {
                DrawableCompat.setTintList(mutate, this.mCheckMarkTintList);
            }
            if (this.mHasCheckMarkTintMode) {
                DrawableCompat.setTintMode(mutate, this.mCheckMarkTintMode);
            }
            if (mutate.isStateful()) {
                mutate.setState(this.mView.getDrawableState());
            }
            this.mView.setCheckMarkDrawable(mutate);
        }
    }
    
    ColorStateList getSupportCheckMarkTintList() {
        return this.mCheckMarkTintList;
    }
    
    PorterDuff$Mode getSupportCheckMarkTintMode() {
        return this.mCheckMarkTintMode;
    }
    
    void loadFromAttributes(final AttributeSet set, int n) {
        final TintTypedArray obtainStyledAttributes = TintTypedArray.obtainStyledAttributes(this.mView.getContext(), set, R.styleable.CheckedTextView, n, 0);
        final CheckedTextView mView = this.mView;
        ViewCompat.saveAttributeDataForStyleable((View)mView, mView.getContext(), R.styleable.CheckedTextView, set, obtainStyledAttributes.getWrappedTypeArray(), n, 0);
        try {
            Label_0091: {
                if (obtainStyledAttributes.hasValue(R.styleable.CheckedTextView_checkMarkCompat)) {
                    n = obtainStyledAttributes.getResourceId(R.styleable.CheckedTextView_checkMarkCompat, 0);
                    if (n != 0) {
                        try {
                            final CheckedTextView mView2 = this.mView;
                            mView2.setCheckMarkDrawable(AppCompatResources.getDrawable(mView2.getContext(), n));
                            n = 1;
                            break Label_0091;
                        }
                        catch (final Resources$NotFoundException ex) {}
                    }
                }
                n = 0;
            }
            if (n == 0 && obtainStyledAttributes.hasValue(R.styleable.CheckedTextView_android_checkMark)) {
                n = obtainStyledAttributes.getResourceId(R.styleable.CheckedTextView_android_checkMark, 0);
                if (n != 0) {
                    final CheckedTextView mView3 = this.mView;
                    mView3.setCheckMarkDrawable(AppCompatResources.getDrawable(mView3.getContext(), n));
                }
            }
            if (obtainStyledAttributes.hasValue(R.styleable.CheckedTextView_checkMarkTint)) {
                CheckedTextViewCompat.setCheckMarkTintList(this.mView, obtainStyledAttributes.getColorStateList(R.styleable.CheckedTextView_checkMarkTint));
            }
            if (obtainStyledAttributes.hasValue(R.styleable.CheckedTextView_checkMarkTintMode)) {
                CheckedTextViewCompat.setCheckMarkTintMode(this.mView, DrawableUtils.parseTintMode(obtainStyledAttributes.getInt(R.styleable.CheckedTextView_checkMarkTintMode, -1), null));
            }
        }
        finally {
            obtainStyledAttributes.recycle();
        }
    }
    
    void onSetCheckMarkDrawable() {
        if (this.mSkipNextApply) {
            this.mSkipNextApply = false;
            return;
        }
        this.mSkipNextApply = true;
        this.applyCheckMarkTint();
    }
    
    void setSupportCheckMarkTintList(final ColorStateList mCheckMarkTintList) {
        this.mCheckMarkTintList = mCheckMarkTintList;
        this.mHasCheckMarkTint = true;
        this.applyCheckMarkTint();
    }
    
    void setSupportCheckMarkTintMode(final PorterDuff$Mode mCheckMarkTintMode) {
        this.mCheckMarkTintMode = mCheckMarkTintMode;
        this.mHasCheckMarkTintMode = true;
        this.applyCheckMarkTint();
    }
}

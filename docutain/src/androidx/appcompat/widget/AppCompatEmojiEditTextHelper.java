// 
// Decompiled by Procyon v0.6.0
// 

package androidx.appcompat.widget;

import android.content.res.TypedArray;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputConnection;
import androidx.appcompat.R;
import android.util.AttributeSet;
import android.text.method.NumberKeyListener;
import android.text.method.KeyListener;
import android.widget.EditText;
import androidx.emoji2.viewsintegration.EmojiEditTextHelper;

class AppCompatEmojiEditTextHelper
{
    private final EmojiEditTextHelper mEmojiEditTextHelper;
    private final EditText mView;
    
    AppCompatEmojiEditTextHelper(final EditText mView) {
        this.mView = mView;
        this.mEmojiEditTextHelper = new EmojiEditTextHelper(mView, false);
    }
    
    KeyListener getKeyListener(final KeyListener keyListener) {
        KeyListener keyListener2 = keyListener;
        if (this.isEmojiCapableKeyListener(keyListener)) {
            keyListener2 = this.mEmojiEditTextHelper.getKeyListener(keyListener);
        }
        return keyListener2;
    }
    
    boolean isEmojiCapableKeyListener(final KeyListener keyListener) {
        return keyListener instanceof NumberKeyListener ^ true;
    }
    
    boolean isEnabled() {
        return this.mEmojiEditTextHelper.isEnabled();
    }
    
    void loadFromAttributes(AttributeSet obtainStyledAttributes, final int n) {
        obtainStyledAttributes = (AttributeSet)this.mView.getContext().obtainStyledAttributes(obtainStyledAttributes, R.styleable.AppCompatTextView, n, 0);
        try {
            final boolean hasValue = ((TypedArray)obtainStyledAttributes).hasValue(R.styleable.AppCompatTextView_emojiCompatEnabled);
            boolean boolean1 = true;
            if (hasValue) {
                boolean1 = ((TypedArray)obtainStyledAttributes).getBoolean(R.styleable.AppCompatTextView_emojiCompatEnabled, true);
            }
            ((TypedArray)obtainStyledAttributes).recycle();
            this.setEnabled(boolean1);
        }
        finally {
            ((TypedArray)obtainStyledAttributes).recycle();
        }
    }
    
    InputConnection onCreateInputConnection(final InputConnection inputConnection, final EditorInfo editorInfo) {
        return this.mEmojiEditTextHelper.onCreateInputConnection(inputConnection, editorInfo);
    }
    
    void setEnabled(final boolean enabled) {
        this.mEmojiEditTextHelper.setEnabled(enabled);
    }
}

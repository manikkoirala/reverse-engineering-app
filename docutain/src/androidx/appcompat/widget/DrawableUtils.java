// 
// Decompiled by Procyon v0.6.0
// 

package androidx.appcompat.widget;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Field;
import android.graphics.PorterDuff$Mode;
import android.graphics.Insets;
import androidx.core.graphics.drawable.DrawableCompat;
import android.graphics.drawable.Drawable$ConstantState;
import android.graphics.drawable.ScaleDrawable;
import androidx.appcompat.graphics.drawable.DrawableWrapperCompat;
import androidx.core.graphics.drawable.WrappedDrawable;
import android.graphics.drawable.DrawableContainer$DrawableContainerState;
import android.graphics.drawable.DrawableContainer;
import android.graphics.drawable.LayerDrawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.InsetDrawable;
import android.os.Build$VERSION;
import android.graphics.drawable.Drawable;
import android.graphics.Rect;

public class DrawableUtils
{
    private static final int[] CHECKED_STATE_SET;
    private static final int[] EMPTY_STATE_SET;
    public static final Rect INSETS_NONE;
    
    static {
        CHECKED_STATE_SET = new int[] { 16842912 };
        EMPTY_STATE_SET = new int[0];
        INSETS_NONE = new Rect();
    }
    
    private DrawableUtils() {
    }
    
    public static boolean canSafelyMutateDrawable(final Drawable drawable) {
        if (Build$VERSION.SDK_INT >= 17) {
            return true;
        }
        if (Build$VERSION.SDK_INT < 15 && drawable instanceof InsetDrawable) {
            return false;
        }
        if (Build$VERSION.SDK_INT < 15 && drawable instanceof GradientDrawable) {
            return false;
        }
        if (Build$VERSION.SDK_INT < 17 && drawable instanceof LayerDrawable) {
            return false;
        }
        if (drawable instanceof DrawableContainer) {
            final Drawable$ConstantState constantState = drawable.getConstantState();
            if (constantState instanceof DrawableContainer$DrawableContainerState) {
                final Drawable[] children = ((DrawableContainer$DrawableContainerState)constantState).getChildren();
                for (int length = children.length, i = 0; i < length; ++i) {
                    if (!canSafelyMutateDrawable(children[i])) {
                        return false;
                    }
                }
            }
        }
        else {
            if (drawable instanceof WrappedDrawable) {
                return canSafelyMutateDrawable(((WrappedDrawable)drawable).getWrappedDrawable());
            }
            if (drawable instanceof DrawableWrapperCompat) {
                return canSafelyMutateDrawable(((DrawableWrapperCompat)drawable).getDrawable());
            }
            if (drawable instanceof ScaleDrawable) {
                return canSafelyMutateDrawable(((ScaleDrawable)drawable).getDrawable());
            }
        }
        return true;
    }
    
    static void fixDrawable(final Drawable drawable) {
        final String name = drawable.getClass().getName();
        if (Build$VERSION.SDK_INT == 21 && "android.graphics.drawable.VectorDrawable".equals(name)) {
            forceDrawableStateChange(drawable);
        }
        else if (Build$VERSION.SDK_INT >= 29 && Build$VERSION.SDK_INT < 31 && "android.graphics.drawable.ColorStateListDrawable".equals(name)) {
            forceDrawableStateChange(drawable);
        }
    }
    
    private static void forceDrawableStateChange(final Drawable drawable) {
        final int[] state = drawable.getState();
        if (state != null && state.length != 0) {
            drawable.setState(DrawableUtils.EMPTY_STATE_SET);
        }
        else {
            drawable.setState(DrawableUtils.CHECKED_STATE_SET);
        }
        drawable.setState(state);
    }
    
    public static Rect getOpticalBounds(final Drawable drawable) {
        if (Build$VERSION.SDK_INT >= 29) {
            final Insets opticalInsets = Api29Impl.getOpticalInsets(drawable);
            return new Rect(opticalInsets.left, opticalInsets.top, opticalInsets.right, opticalInsets.bottom);
        }
        if (Build$VERSION.SDK_INT >= 18) {
            return Api18Impl.getOpticalInsets(DrawableCompat.unwrap(drawable));
        }
        return DrawableUtils.INSETS_NONE;
    }
    
    public static PorterDuff$Mode parseTintMode(final int n, final PorterDuff$Mode porterDuff$Mode) {
        if (n == 3) {
            return PorterDuff$Mode.SRC_OVER;
        }
        if (n == 5) {
            return PorterDuff$Mode.SRC_IN;
        }
        if (n == 9) {
            return PorterDuff$Mode.SRC_ATOP;
        }
        switch (n) {
            default: {
                return porterDuff$Mode;
            }
            case 16: {
                return PorterDuff$Mode.ADD;
            }
            case 15: {
                return PorterDuff$Mode.SCREEN;
            }
            case 14: {
                return PorterDuff$Mode.MULTIPLY;
            }
        }
    }
    
    static class Api18Impl
    {
        private static final Field sBottom;
        private static final Method sGetOpticalInsets;
        private static final Field sLeft;
        private static final boolean sReflectionSuccessful;
        private static final Field sRight;
        private static final Field sTop;
        
        static {
            Method method;
            try {
                final Class<?> forName = Class.forName("android.graphics.Insets");
                method = Drawable.class.getMethod("getOpticalInsets", (Class<?>[])new Class[0]);
                try {
                    final Field field = forName.getField("left");
                    try {
                        final Field field2 = forName.getField("top");
                        try {
                            final Field field3 = forName.getField("right");
                            try {
                                final Field field4 = forName.getField("bottom");
                                final boolean b = true;
                            }
                            catch (final NoSuchMethodException | ClassNotFoundException | NoSuchFieldException ex) {}
                        }
                        catch (final NoSuchMethodException | ClassNotFoundException | NoSuchFieldException ex2) {
                            final Field field3 = null;
                        }
                    }
                    catch (final NoSuchFieldException ex3) {}
                    catch (final ClassNotFoundException ex4) {}
                    catch (final NoSuchMethodException ex5) {}
                }
                catch (final NoSuchFieldException ex6) {}
                catch (final ClassNotFoundException ex7) {}
                catch (final NoSuchMethodException ex8) {}
            }
            catch (final NoSuchFieldException ex9) {
                method = null;
            }
            catch (final ClassNotFoundException ex10) {
                method = null;
            }
            catch (final NoSuchMethodException ex11) {
                method = null;
            }
            final Field field = null;
            final Field field2 = null;
            final Field field3 = null;
            final Field field4 = null;
            final boolean b = false;
            if (b) {
                sGetOpticalInsets = method;
                sLeft = field;
                sTop = field2;
                sRight = field3;
                sBottom = field4;
                sReflectionSuccessful = true;
            }
            else {
                sGetOpticalInsets = null;
                sLeft = null;
                sTop = null;
                sRight = null;
                sBottom = null;
                sReflectionSuccessful = false;
            }
        }
        
        private Api18Impl() {
        }
        
        static Rect getOpticalInsets(final Drawable obj) {
            Label_0068: {
                if (Build$VERSION.SDK_INT >= 29 || !Api18Impl.sReflectionSuccessful) {
                    break Label_0068;
                }
                try {
                    final Object invoke = Api18Impl.sGetOpticalInsets.invoke(obj, new Object[0]);
                    if (invoke != null) {
                        return new Rect(Api18Impl.sLeft.getInt(invoke), Api18Impl.sTop.getInt(invoke), Api18Impl.sRight.getInt(invoke), Api18Impl.sBottom.getInt(invoke));
                    }
                    return DrawableUtils.INSETS_NONE;
                }
                catch (final IllegalAccessException | InvocationTargetException ex) {
                    return DrawableUtils.INSETS_NONE;
                }
            }
        }
    }
    
    static class Api29Impl
    {
        private Api29Impl() {
        }
        
        static Insets getOpticalInsets(final Drawable drawable) {
            return drawable.getOpticalInsets();
        }
    }
}

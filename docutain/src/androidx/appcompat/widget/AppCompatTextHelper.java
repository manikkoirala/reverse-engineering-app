// 
// Decompiled by Procyon v0.6.0
// 

package androidx.appcompat.widget;

import android.os.LocaleList;
import java.util.Locale;
import androidx.core.view.inputmethod.EditorInfoCompat;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputConnection;
import androidx.core.widget.TextViewCompat;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import androidx.core.view.ViewCompat;
import android.util.AttributeSet;
import android.graphics.PorterDuff$Mode;
import android.content.res.Resources$NotFoundException;
import androidx.core.content.res.ResourcesCompat;
import java.lang.ref.WeakReference;
import androidx.appcompat.R;
import android.os.Build$VERSION;
import android.content.res.ColorStateList;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.widget.TextView;
import android.graphics.Typeface;

class AppCompatTextHelper
{
    private static final int MONOSPACE = 3;
    private static final int SANS = 1;
    private static final int SERIF = 2;
    private static final int TEXT_FONT_WEIGHT_UNSPECIFIED = -1;
    private boolean mAsyncFontPending;
    private final AppCompatTextViewAutoSizeHelper mAutoSizeTextHelper;
    private TintInfo mDrawableBottomTint;
    private TintInfo mDrawableEndTint;
    private TintInfo mDrawableLeftTint;
    private TintInfo mDrawableRightTint;
    private TintInfo mDrawableStartTint;
    private TintInfo mDrawableTint;
    private TintInfo mDrawableTopTint;
    private Typeface mFontTypeface;
    private int mFontWeight;
    private int mStyle;
    private final TextView mView;
    
    AppCompatTextHelper(final TextView mView) {
        this.mStyle = 0;
        this.mFontWeight = -1;
        this.mView = mView;
        this.mAutoSizeTextHelper = new AppCompatTextViewAutoSizeHelper(mView);
    }
    
    private void applyCompoundDrawableTint(final Drawable drawable, final TintInfo tintInfo) {
        if (drawable != null && tintInfo != null) {
            AppCompatDrawableManager.tintDrawable(drawable, tintInfo, this.mView.getDrawableState());
        }
    }
    
    private static TintInfo createTintInfo(final Context context, final AppCompatDrawableManager appCompatDrawableManager, final int n) {
        final ColorStateList tintList = appCompatDrawableManager.getTintList(context, n);
        if (tintList != null) {
            final TintInfo tintInfo = new TintInfo();
            tintInfo.mHasTintList = true;
            tintInfo.mTintList = tintList;
            return tintInfo;
        }
        return null;
    }
    
    private void setCompoundDrawables(Drawable drawable, Drawable drawable2, Drawable drawable3, Drawable drawable4, Drawable drawable5, Drawable drawable6) {
        if (Build$VERSION.SDK_INT >= 17 && (drawable5 != null || drawable6 != null)) {
            final Drawable[] compoundDrawablesRelative = Api17Impl.getCompoundDrawablesRelative(this.mView);
            final TextView mView = this.mView;
            if (drawable5 == null) {
                drawable5 = compoundDrawablesRelative[0];
            }
            if (drawable2 == null) {
                drawable2 = compoundDrawablesRelative[1];
            }
            if (drawable6 == null) {
                drawable6 = compoundDrawablesRelative[2];
            }
            if (drawable4 == null) {
                drawable4 = compoundDrawablesRelative[3];
            }
            Api17Impl.setCompoundDrawablesRelativeWithIntrinsicBounds(mView, drawable5, drawable2, drawable6, drawable4);
        }
        else if (drawable != null || drawable2 != null || drawable3 != null || drawable4 != null) {
            if (Build$VERSION.SDK_INT >= 17) {
                final Drawable[] compoundDrawablesRelative2 = Api17Impl.getCompoundDrawablesRelative(this.mView);
                drawable5 = compoundDrawablesRelative2[0];
                if (drawable5 != null || compoundDrawablesRelative2[2] != null) {
                    final TextView mView2 = this.mView;
                    if (drawable2 == null) {
                        drawable2 = compoundDrawablesRelative2[1];
                    }
                    drawable3 = compoundDrawablesRelative2[2];
                    if (drawable4 == null) {
                        drawable4 = compoundDrawablesRelative2[3];
                    }
                    Api17Impl.setCompoundDrawablesRelativeWithIntrinsicBounds(mView2, drawable5, drawable2, drawable3, drawable4);
                    return;
                }
            }
            final Drawable[] compoundDrawables = this.mView.getCompoundDrawables();
            final TextView mView3 = this.mView;
            if (drawable == null) {
                drawable = compoundDrawables[0];
            }
            if (drawable2 == null) {
                drawable2 = compoundDrawables[1];
            }
            if (drawable3 == null) {
                drawable3 = compoundDrawables[2];
            }
            if (drawable4 == null) {
                drawable4 = compoundDrawables[3];
            }
            mView3.setCompoundDrawablesWithIntrinsicBounds(drawable, drawable2, drawable3, drawable4);
        }
    }
    
    private void setCompoundTints() {
        final TintInfo mDrawableTint = this.mDrawableTint;
        this.mDrawableLeftTint = mDrawableTint;
        this.mDrawableTopTint = mDrawableTint;
        this.mDrawableRightTint = mDrawableTint;
        this.mDrawableBottomTint = mDrawableTint;
        this.mDrawableStartTint = mDrawableTint;
        this.mDrawableEndTint = mDrawableTint;
    }
    
    private void setTextSizeInternal(final int n, final float n2) {
        this.mAutoSizeTextHelper.setTextSizeInternal(n, n2);
    }
    
    private void updateTypefaceAndStyle(final Context context, final TintTypedArray tintTypedArray) {
        this.mStyle = tintTypedArray.getInt(R.styleable.TextAppearance_android_textStyle, this.mStyle);
        final int sdk_INT = Build$VERSION.SDK_INT;
        final boolean b = false;
        if (sdk_INT >= 28 && (this.mFontWeight = tintTypedArray.getInt(R.styleable.TextAppearance_android_textFontWeight, -1)) != -1) {
            this.mStyle = ((this.mStyle & 0x2) | 0x0);
        }
        if (!tintTypedArray.hasValue(R.styleable.TextAppearance_android_fontFamily) && !tintTypedArray.hasValue(R.styleable.TextAppearance_fontFamily)) {
            if (tintTypedArray.hasValue(R.styleable.TextAppearance_android_typeface)) {
                this.mAsyncFontPending = false;
                final int int1 = tintTypedArray.getInt(R.styleable.TextAppearance_android_typeface, 1);
                if (int1 != 1) {
                    if (int1 != 2) {
                        if (int1 == 3) {
                            this.mFontTypeface = Typeface.MONOSPACE;
                        }
                    }
                    else {
                        this.mFontTypeface = Typeface.SERIF;
                    }
                }
                else {
                    this.mFontTypeface = Typeface.SANS_SERIF;
                }
            }
            return;
        }
        this.mFontTypeface = null;
        int n;
        if (tintTypedArray.hasValue(R.styleable.TextAppearance_fontFamily)) {
            n = R.styleable.TextAppearance_fontFamily;
        }
        else {
            n = R.styleable.TextAppearance_android_fontFamily;
        }
        final int mFontWeight = this.mFontWeight;
        final int mStyle = this.mStyle;
        if (!context.isRestricted()) {
            final ResourcesCompat.FontCallback fontCallback = new ResourcesCompat.FontCallback(this, mFontWeight, mStyle, new WeakReference((T)this.mView)) {
                final AppCompatTextHelper this$0;
                final int val$fontWeight;
                final int val$style;
                final WeakReference val$textViewWeak;
                
                @Override
                public void onFontRetrievalFailed(final int n) {
                }
                
                @Override
                public void onFontRetrieved(final Typeface typeface) {
                    Typeface create = typeface;
                    if (Build$VERSION.SDK_INT >= 28) {
                        final int val$fontWeight = this.val$fontWeight;
                        create = typeface;
                        if (val$fontWeight != -1) {
                            create = Api28Impl.create(typeface, val$fontWeight, (this.val$style & 0x2) != 0x0);
                        }
                    }
                    this.this$0.onAsyncTypefaceReceived(this.val$textViewWeak, create);
                }
            };
            try {
                final Typeface font = tintTypedArray.getFont(n, this.mStyle, fontCallback);
                if (font != null) {
                    if (Build$VERSION.SDK_INT >= 28 && this.mFontWeight != -1) {
                        this.mFontTypeface = Api28Impl.create(Typeface.create(font, 0), this.mFontWeight, (this.mStyle & 0x2) != 0x0);
                    }
                    else {
                        this.mFontTypeface = font;
                    }
                }
                this.mAsyncFontPending = (this.mFontTypeface == null);
            }
            catch (final UnsupportedOperationException | Resources$NotFoundException ex) {}
        }
        if (this.mFontTypeface == null) {
            final String string = tintTypedArray.getString(n);
            if (string != null) {
                if (Build$VERSION.SDK_INT >= 28 && this.mFontWeight != -1) {
                    final Typeface create = Typeface.create(string, 0);
                    final int mFontWeight2 = this.mFontWeight;
                    boolean b2 = b;
                    if ((this.mStyle & 0x2) != 0x0) {
                        b2 = true;
                    }
                    this.mFontTypeface = Api28Impl.create(create, mFontWeight2, b2);
                }
                else {
                    this.mFontTypeface = Typeface.create(string, this.mStyle);
                }
            }
        }
    }
    
    void applyCompoundDrawablesTints() {
        if (this.mDrawableLeftTint != null || this.mDrawableTopTint != null || this.mDrawableRightTint != null || this.mDrawableBottomTint != null) {
            final Drawable[] compoundDrawables = this.mView.getCompoundDrawables();
            this.applyCompoundDrawableTint(compoundDrawables[0], this.mDrawableLeftTint);
            this.applyCompoundDrawableTint(compoundDrawables[1], this.mDrawableTopTint);
            this.applyCompoundDrawableTint(compoundDrawables[2], this.mDrawableRightTint);
            this.applyCompoundDrawableTint(compoundDrawables[3], this.mDrawableBottomTint);
        }
        if (Build$VERSION.SDK_INT >= 17 && (this.mDrawableStartTint != null || this.mDrawableEndTint != null)) {
            final Drawable[] compoundDrawablesRelative = Api17Impl.getCompoundDrawablesRelative(this.mView);
            this.applyCompoundDrawableTint(compoundDrawablesRelative[0], this.mDrawableStartTint);
            this.applyCompoundDrawableTint(compoundDrawablesRelative[2], this.mDrawableEndTint);
        }
    }
    
    void autoSizeText() {
        this.mAutoSizeTextHelper.autoSizeText();
    }
    
    int getAutoSizeMaxTextSize() {
        return this.mAutoSizeTextHelper.getAutoSizeMaxTextSize();
    }
    
    int getAutoSizeMinTextSize() {
        return this.mAutoSizeTextHelper.getAutoSizeMinTextSize();
    }
    
    int getAutoSizeStepGranularity() {
        return this.mAutoSizeTextHelper.getAutoSizeStepGranularity();
    }
    
    int[] getAutoSizeTextAvailableSizes() {
        return this.mAutoSizeTextHelper.getAutoSizeTextAvailableSizes();
    }
    
    int getAutoSizeTextType() {
        return this.mAutoSizeTextHelper.getAutoSizeTextType();
    }
    
    ColorStateList getCompoundDrawableTintList() {
        final TintInfo mDrawableTint = this.mDrawableTint;
        ColorStateList mTintList;
        if (mDrawableTint != null) {
            mTintList = mDrawableTint.mTintList;
        }
        else {
            mTintList = null;
        }
        return mTintList;
    }
    
    PorterDuff$Mode getCompoundDrawableTintMode() {
        final TintInfo mDrawableTint = this.mDrawableTint;
        PorterDuff$Mode mTintMode;
        if (mDrawableTint != null) {
            mTintMode = mDrawableTint.mTintMode;
        }
        else {
            mTintMode = null;
        }
        return mTintMode;
    }
    
    boolean isAutoSizeEnabled() {
        return this.mAutoSizeTextHelper.isAutoSizeEnabled();
    }
    
    void loadFromAttributes(final AttributeSet set, int n) {
        final Context context = this.mView.getContext();
        final AppCompatDrawableManager value = AppCompatDrawableManager.get();
        final TintTypedArray obtainStyledAttributes = TintTypedArray.obtainStyledAttributes(context, set, R.styleable.AppCompatTextHelper, n, 0);
        final TextView mView = this.mView;
        ViewCompat.saveAttributeDataForStyleable((View)mView, mView.getContext(), R.styleable.AppCompatTextHelper, set, obtainStyledAttributes.getWrappedTypeArray(), n, 0);
        final int resourceId = obtainStyledAttributes.getResourceId(R.styleable.AppCompatTextHelper_android_textAppearance, -1);
        if (obtainStyledAttributes.hasValue(R.styleable.AppCompatTextHelper_android_drawableLeft)) {
            this.mDrawableLeftTint = createTintInfo(context, value, obtainStyledAttributes.getResourceId(R.styleable.AppCompatTextHelper_android_drawableLeft, 0));
        }
        if (obtainStyledAttributes.hasValue(R.styleable.AppCompatTextHelper_android_drawableTop)) {
            this.mDrawableTopTint = createTintInfo(context, value, obtainStyledAttributes.getResourceId(R.styleable.AppCompatTextHelper_android_drawableTop, 0));
        }
        if (obtainStyledAttributes.hasValue(R.styleable.AppCompatTextHelper_android_drawableRight)) {
            this.mDrawableRightTint = createTintInfo(context, value, obtainStyledAttributes.getResourceId(R.styleable.AppCompatTextHelper_android_drawableRight, 0));
        }
        if (obtainStyledAttributes.hasValue(R.styleable.AppCompatTextHelper_android_drawableBottom)) {
            this.mDrawableBottomTint = createTintInfo(context, value, obtainStyledAttributes.getResourceId(R.styleable.AppCompatTextHelper_android_drawableBottom, 0));
        }
        if (Build$VERSION.SDK_INT >= 17) {
            if (obtainStyledAttributes.hasValue(R.styleable.AppCompatTextHelper_android_drawableStart)) {
                this.mDrawableStartTint = createTintInfo(context, value, obtainStyledAttributes.getResourceId(R.styleable.AppCompatTextHelper_android_drawableStart, 0));
            }
            if (obtainStyledAttributes.hasValue(R.styleable.AppCompatTextHelper_android_drawableEnd)) {
                this.mDrawableEndTint = createTintInfo(context, value, obtainStyledAttributes.getResourceId(R.styleable.AppCompatTextHelper_android_drawableEnd, 0));
            }
        }
        obtainStyledAttributes.recycle();
        final boolean b = this.mView.getTransformationMethod() instanceof PasswordTransformationMethod;
        boolean allCaps;
        boolean b2;
        ColorStateList list;
        String s;
        String string;
        ColorStateList list2;
        ColorStateList colorStateList3;
        if (resourceId != -1) {
            final TintTypedArray obtainStyledAttributes2 = TintTypedArray.obtainStyledAttributes(context, resourceId, R.styleable.TextAppearance);
            if (!b && obtainStyledAttributes2.hasValue(R.styleable.TextAppearance_textAllCaps)) {
                allCaps = obtainStyledAttributes2.getBoolean(R.styleable.TextAppearance_textAllCaps, false);
                b2 = true;
            }
            else {
                allCaps = false;
                b2 = false;
            }
            this.updateTypefaceAndStyle(context, obtainStyledAttributes2);
            ColorStateList colorStateList;
            ColorStateList colorStateList2;
            if (Build$VERSION.SDK_INT < 23) {
                if (obtainStyledAttributes2.hasValue(R.styleable.TextAppearance_android_textColor)) {
                    colorStateList = obtainStyledAttributes2.getColorStateList(R.styleable.TextAppearance_android_textColor);
                }
                else {
                    colorStateList = null;
                }
                if (obtainStyledAttributes2.hasValue(R.styleable.TextAppearance_android_textColorHint)) {
                    list = obtainStyledAttributes2.getColorStateList(R.styleable.TextAppearance_android_textColorHint);
                }
                else {
                    list = null;
                }
                if (obtainStyledAttributes2.hasValue(R.styleable.TextAppearance_android_textColorLink)) {
                    colorStateList2 = obtainStyledAttributes2.getColorStateList(R.styleable.TextAppearance_android_textColorLink);
                }
                else {
                    colorStateList2 = null;
                }
            }
            else {
                list = null;
                colorStateList2 = null;
                colorStateList = null;
            }
            if (obtainStyledAttributes2.hasValue(R.styleable.TextAppearance_textLocale)) {
                s = obtainStyledAttributes2.getString(R.styleable.TextAppearance_textLocale);
            }
            else {
                s = null;
            }
            if (Build$VERSION.SDK_INT >= 26 && obtainStyledAttributes2.hasValue(R.styleable.TextAppearance_fontVariationSettings)) {
                string = obtainStyledAttributes2.getString(R.styleable.TextAppearance_fontVariationSettings);
            }
            else {
                string = null;
            }
            obtainStyledAttributes2.recycle();
            list2 = colorStateList2;
            colorStateList3 = colorStateList;
        }
        else {
            list = null;
            string = null;
            allCaps = false;
            b2 = false;
            list2 = null;
            s = null;
            colorStateList3 = null;
        }
        final TintTypedArray obtainStyledAttributes3 = TintTypedArray.obtainStyledAttributes(context, set, R.styleable.TextAppearance, n, 0);
        if (!b && obtainStyledAttributes3.hasValue(R.styleable.TextAppearance_textAllCaps)) {
            allCaps = obtainStyledAttributes3.getBoolean(R.styleable.TextAppearance_textAllCaps, false);
            b2 = true;
        }
        ColorStateList hintTextColor = list;
        ColorStateList colorStateList4 = list2;
        ColorStateList textColor = colorStateList3;
        if (Build$VERSION.SDK_INT < 23) {
            if (obtainStyledAttributes3.hasValue(R.styleable.TextAppearance_android_textColor)) {
                colorStateList3 = obtainStyledAttributes3.getColorStateList(R.styleable.TextAppearance_android_textColor);
            }
            if (obtainStyledAttributes3.hasValue(R.styleable.TextAppearance_android_textColorHint)) {
                list = obtainStyledAttributes3.getColorStateList(R.styleable.TextAppearance_android_textColorHint);
            }
            hintTextColor = list;
            colorStateList4 = list2;
            textColor = colorStateList3;
            if (obtainStyledAttributes3.hasValue(R.styleable.TextAppearance_android_textColorLink)) {
                colorStateList4 = obtainStyledAttributes3.getColorStateList(R.styleable.TextAppearance_android_textColorLink);
                textColor = colorStateList3;
                hintTextColor = list;
            }
        }
        if (obtainStyledAttributes3.hasValue(R.styleable.TextAppearance_textLocale)) {
            s = obtainStyledAttributes3.getString(R.styleable.TextAppearance_textLocale);
        }
        String string2 = string;
        if (Build$VERSION.SDK_INT >= 26) {
            string2 = string;
            if (obtainStyledAttributes3.hasValue(R.styleable.TextAppearance_fontVariationSettings)) {
                string2 = obtainStyledAttributes3.getString(R.styleable.TextAppearance_fontVariationSettings);
            }
        }
        if (Build$VERSION.SDK_INT >= 28 && obtainStyledAttributes3.hasValue(R.styleable.TextAppearance_android_textSize) && obtainStyledAttributes3.getDimensionPixelSize(R.styleable.TextAppearance_android_textSize, -1) == 0) {
            this.mView.setTextSize(0, 0.0f);
        }
        this.updateTypefaceAndStyle(context, obtainStyledAttributes3);
        obtainStyledAttributes3.recycle();
        if (textColor != null) {
            this.mView.setTextColor(textColor);
        }
        if (hintTextColor != null) {
            this.mView.setHintTextColor(hintTextColor);
        }
        if (colorStateList4 != null) {
            this.mView.setLinkTextColor(colorStateList4);
        }
        if (!b && b2) {
            this.setAllCaps(allCaps);
        }
        final Typeface mFontTypeface = this.mFontTypeface;
        if (mFontTypeface != null) {
            if (this.mFontWeight == -1) {
                this.mView.setTypeface(mFontTypeface, this.mStyle);
            }
            else {
                this.mView.setTypeface(mFontTypeface);
            }
        }
        if (string2 != null) {
            Api26Impl.setFontVariationSettings(this.mView, string2);
        }
        if (s != null) {
            if (Build$VERSION.SDK_INT >= 24) {
                Api24Impl.setTextLocales(this.mView, Api24Impl.forLanguageTags(s));
            }
            else if (Build$VERSION.SDK_INT >= 21) {
                Api17Impl.setTextLocale(this.mView, Api21Impl.forLanguageTag(s.split(",")[0]));
            }
        }
        this.mAutoSizeTextHelper.loadFromAttributes(set, n);
        if (ViewUtils.SDK_LEVEL_SUPPORTS_AUTOSIZE && this.mAutoSizeTextHelper.getAutoSizeTextType() != 0) {
            final int[] autoSizeTextAvailableSizes = this.mAutoSizeTextHelper.getAutoSizeTextAvailableSizes();
            if (autoSizeTextAvailableSizes.length > 0) {
                if (Api26Impl.getAutoSizeStepGranularity(this.mView) != -1.0f) {
                    Api26Impl.setAutoSizeTextTypeUniformWithConfiguration(this.mView, this.mAutoSizeTextHelper.getAutoSizeMinTextSize(), this.mAutoSizeTextHelper.getAutoSizeMaxTextSize(), this.mAutoSizeTextHelper.getAutoSizeStepGranularity(), 0);
                }
                else {
                    Api26Impl.setAutoSizeTextTypeUniformWithPresetSizes(this.mView, autoSizeTextAvailableSizes, 0);
                }
            }
        }
        final TintTypedArray obtainStyledAttributes4 = TintTypedArray.obtainStyledAttributes(context, set, R.styleable.AppCompatTextView);
        n = obtainStyledAttributes4.getResourceId(R.styleable.AppCompatTextView_drawableLeftCompat, -1);
        Drawable drawable;
        if (n != -1) {
            drawable = value.getDrawable(context, n);
        }
        else {
            drawable = null;
        }
        n = obtainStyledAttributes4.getResourceId(R.styleable.AppCompatTextView_drawableTopCompat, -1);
        Drawable drawable2;
        if (n != -1) {
            drawable2 = value.getDrawable(context, n);
        }
        else {
            drawable2 = null;
        }
        n = obtainStyledAttributes4.getResourceId(R.styleable.AppCompatTextView_drawableRightCompat, -1);
        Drawable drawable3;
        if (n != -1) {
            drawable3 = value.getDrawable(context, n);
        }
        else {
            drawable3 = null;
        }
        n = obtainStyledAttributes4.getResourceId(R.styleable.AppCompatTextView_drawableBottomCompat, -1);
        Drawable drawable4;
        if (n != -1) {
            drawable4 = value.getDrawable(context, n);
        }
        else {
            drawable4 = null;
        }
        n = obtainStyledAttributes4.getResourceId(R.styleable.AppCompatTextView_drawableStartCompat, -1);
        Drawable drawable5;
        if (n != -1) {
            drawable5 = value.getDrawable(context, n);
        }
        else {
            drawable5 = null;
        }
        n = obtainStyledAttributes4.getResourceId(R.styleable.AppCompatTextView_drawableEndCompat, -1);
        Drawable drawable6;
        if (n != -1) {
            drawable6 = value.getDrawable(context, n);
        }
        else {
            drawable6 = null;
        }
        this.setCompoundDrawables(drawable, drawable2, drawable3, drawable4, drawable5, drawable6);
        if (obtainStyledAttributes4.hasValue(R.styleable.AppCompatTextView_drawableTint)) {
            TextViewCompat.setCompoundDrawableTintList(this.mView, obtainStyledAttributes4.getColorStateList(R.styleable.AppCompatTextView_drawableTint));
        }
        if (obtainStyledAttributes4.hasValue(R.styleable.AppCompatTextView_drawableTintMode)) {
            TextViewCompat.setCompoundDrawableTintMode(this.mView, DrawableUtils.parseTintMode(obtainStyledAttributes4.getInt(R.styleable.AppCompatTextView_drawableTintMode, -1), null));
        }
        final int dimensionPixelSize = obtainStyledAttributes4.getDimensionPixelSize(R.styleable.AppCompatTextView_firstBaselineToTopHeight, -1);
        final int dimensionPixelSize2 = obtainStyledAttributes4.getDimensionPixelSize(R.styleable.AppCompatTextView_lastBaselineToBottomHeight, -1);
        n = obtainStyledAttributes4.getDimensionPixelSize(R.styleable.AppCompatTextView_lineHeight, -1);
        obtainStyledAttributes4.recycle();
        if (dimensionPixelSize != -1) {
            TextViewCompat.setFirstBaselineToTopHeight(this.mView, dimensionPixelSize);
        }
        if (dimensionPixelSize2 != -1) {
            TextViewCompat.setLastBaselineToBottomHeight(this.mView, dimensionPixelSize2);
        }
        if (n != -1) {
            TextViewCompat.setLineHeight(this.mView, n);
        }
    }
    
    void onAsyncTypefaceReceived(final WeakReference<TextView> weakReference, final Typeface mFontTypeface) {
        if (this.mAsyncFontPending) {
            this.mFontTypeface = mFontTypeface;
            final TextView textView = weakReference.get();
            if (textView != null) {
                if (ViewCompat.isAttachedToWindow((View)textView)) {
                    textView.post((Runnable)new Runnable(this, textView, mFontTypeface, this.mStyle) {
                        final AppCompatTextHelper this$0;
                        final int val$style;
                        final TextView val$textView;
                        final Typeface val$typeface;
                        
                        @Override
                        public void run() {
                            this.val$textView.setTypeface(this.val$typeface, this.val$style);
                        }
                    });
                }
                else {
                    textView.setTypeface(mFontTypeface, this.mStyle);
                }
            }
        }
    }
    
    void onLayout(final boolean b, final int n, final int n2, final int n3, final int n4) {
        if (!ViewUtils.SDK_LEVEL_SUPPORTS_AUTOSIZE) {
            this.autoSizeText();
        }
    }
    
    void onSetCompoundDrawables() {
        this.applyCompoundDrawablesTints();
    }
    
    void onSetTextAppearance(final Context context, final int n) {
        final TintTypedArray obtainStyledAttributes = TintTypedArray.obtainStyledAttributes(context, n, R.styleable.TextAppearance);
        if (obtainStyledAttributes.hasValue(R.styleable.TextAppearance_textAllCaps)) {
            this.setAllCaps(obtainStyledAttributes.getBoolean(R.styleable.TextAppearance_textAllCaps, false));
        }
        if (Build$VERSION.SDK_INT < 23) {
            if (obtainStyledAttributes.hasValue(R.styleable.TextAppearance_android_textColor)) {
                final ColorStateList colorStateList = obtainStyledAttributes.getColorStateList(R.styleable.TextAppearance_android_textColor);
                if (colorStateList != null) {
                    this.mView.setTextColor(colorStateList);
                }
            }
            if (obtainStyledAttributes.hasValue(R.styleable.TextAppearance_android_textColorLink)) {
                final ColorStateList colorStateList2 = obtainStyledAttributes.getColorStateList(R.styleable.TextAppearance_android_textColorLink);
                if (colorStateList2 != null) {
                    this.mView.setLinkTextColor(colorStateList2);
                }
            }
            if (obtainStyledAttributes.hasValue(R.styleable.TextAppearance_android_textColorHint)) {
                final ColorStateList colorStateList3 = obtainStyledAttributes.getColorStateList(R.styleable.TextAppearance_android_textColorHint);
                if (colorStateList3 != null) {
                    this.mView.setHintTextColor(colorStateList3);
                }
            }
        }
        if (obtainStyledAttributes.hasValue(R.styleable.TextAppearance_android_textSize) && obtainStyledAttributes.getDimensionPixelSize(R.styleable.TextAppearance_android_textSize, -1) == 0) {
            this.mView.setTextSize(0, 0.0f);
        }
        this.updateTypefaceAndStyle(context, obtainStyledAttributes);
        if (Build$VERSION.SDK_INT >= 26 && obtainStyledAttributes.hasValue(R.styleable.TextAppearance_fontVariationSettings)) {
            final String string = obtainStyledAttributes.getString(R.styleable.TextAppearance_fontVariationSettings);
            if (string != null) {
                Api26Impl.setFontVariationSettings(this.mView, string);
            }
        }
        obtainStyledAttributes.recycle();
        final Typeface mFontTypeface = this.mFontTypeface;
        if (mFontTypeface != null) {
            this.mView.setTypeface(mFontTypeface, this.mStyle);
        }
    }
    
    void populateSurroundingTextIfNeeded(final TextView textView, final InputConnection inputConnection, final EditorInfo editorInfo) {
        if (Build$VERSION.SDK_INT < 30 && inputConnection != null) {
            EditorInfoCompat.setInitialSurroundingText(editorInfo, textView.getText());
        }
    }
    
    void setAllCaps(final boolean allCaps) {
        this.mView.setAllCaps(allCaps);
    }
    
    void setAutoSizeTextTypeUniformWithConfiguration(final int n, final int n2, final int n3, final int n4) throws IllegalArgumentException {
        this.mAutoSizeTextHelper.setAutoSizeTextTypeUniformWithConfiguration(n, n2, n3, n4);
    }
    
    void setAutoSizeTextTypeUniformWithPresetSizes(final int[] array, final int n) throws IllegalArgumentException {
        this.mAutoSizeTextHelper.setAutoSizeTextTypeUniformWithPresetSizes(array, n);
    }
    
    void setAutoSizeTextTypeWithDefaults(final int autoSizeTextTypeWithDefaults) {
        this.mAutoSizeTextHelper.setAutoSizeTextTypeWithDefaults(autoSizeTextTypeWithDefaults);
    }
    
    void setCompoundDrawableTintList(final ColorStateList mTintList) {
        if (this.mDrawableTint == null) {
            this.mDrawableTint = new TintInfo();
        }
        this.mDrawableTint.mTintList = mTintList;
        this.mDrawableTint.mHasTintList = (mTintList != null);
        this.setCompoundTints();
    }
    
    void setCompoundDrawableTintMode(final PorterDuff$Mode mTintMode) {
        if (this.mDrawableTint == null) {
            this.mDrawableTint = new TintInfo();
        }
        this.mDrawableTint.mTintMode = mTintMode;
        this.mDrawableTint.mHasTintMode = (mTintMode != null);
        this.setCompoundTints();
    }
    
    void setTextSize(final int n, final float n2) {
        if (!ViewUtils.SDK_LEVEL_SUPPORTS_AUTOSIZE && !this.isAutoSizeEnabled()) {
            this.setTextSizeInternal(n, n2);
        }
    }
    
    static class Api17Impl
    {
        private Api17Impl() {
        }
        
        static Drawable[] getCompoundDrawablesRelative(final TextView textView) {
            return textView.getCompoundDrawablesRelative();
        }
        
        static void setCompoundDrawablesRelativeWithIntrinsicBounds(final TextView textView, final Drawable drawable, final Drawable drawable2, final Drawable drawable3, final Drawable drawable4) {
            textView.setCompoundDrawablesRelativeWithIntrinsicBounds(drawable, drawable2, drawable3, drawable4);
        }
        
        static void setTextLocale(final TextView textView, final Locale textLocale) {
            textView.setTextLocale(textLocale);
        }
    }
    
    static class Api21Impl
    {
        private Api21Impl() {
        }
        
        static Locale forLanguageTag(final String languageTag) {
            return Locale.forLanguageTag(languageTag);
        }
    }
    
    static class Api24Impl
    {
        private Api24Impl() {
        }
        
        static LocaleList forLanguageTags(final String s) {
            return LocaleList.forLanguageTags(s);
        }
        
        static void setTextLocales(final TextView textView, final LocaleList textLocales) {
            textView.setTextLocales(textLocales);
        }
    }
    
    static class Api26Impl
    {
        private Api26Impl() {
        }
        
        static int getAutoSizeStepGranularity(final TextView textView) {
            return textView.getAutoSizeStepGranularity();
        }
        
        static void setAutoSizeTextTypeUniformWithConfiguration(final TextView textView, final int n, final int n2, final int n3, final int n4) {
            textView.setAutoSizeTextTypeUniformWithConfiguration(n, n2, n3, n4);
        }
        
        static void setAutoSizeTextTypeUniformWithPresetSizes(final TextView textView, final int[] array, final int n) {
            textView.setAutoSizeTextTypeUniformWithPresetSizes(array, n);
        }
        
        static boolean setFontVariationSettings(final TextView textView, final String fontVariationSettings) {
            return textView.setFontVariationSettings(fontVariationSettings);
        }
    }
    
    static class Api28Impl
    {
        private Api28Impl() {
        }
        
        static Typeface create(final Typeface typeface, final int n, final boolean b) {
            return Typeface.create(typeface, n, b);
        }
    }
}

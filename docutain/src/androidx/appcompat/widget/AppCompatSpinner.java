// 
// Decompiled by Procyon v0.6.0
// 

package androidx.appcompat.widget;

import android.os.Parcel;
import android.os.Parcelable$Creator;
import android.view.View$BaseSavedState;
import android.widget.PopupWindow$OnDismissListener;
import androidx.core.view.ViewCompat;
import android.widget.AdapterView;
import android.widget.AdapterView$OnItemClickListener;
import android.database.DataSetObserver;
import android.widget.ListView;
import android.util.Log;
import android.content.DialogInterface;
import androidx.appcompat.app.AlertDialog;
import android.content.DialogInterface$OnClickListener;
import androidx.core.util.ObjectsCompat;
import android.widget.ThemedSpinnerAdapter;
import androidx.appcompat.content.res.AppCompatResources;
import android.widget.ListAdapter;
import android.widget.Adapter;
import android.view.MotionEvent;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver$OnGlobalLayoutListener;
import android.os.Parcelable;
import android.graphics.PorterDuff$Mode;
import android.content.res.ColorStateList;
import android.os.Build$VERSION;
import android.view.View;
import android.view.ViewGroup$LayoutParams;
import android.view.ViewGroup;
import android.view.View$MeasureSpec;
import android.graphics.drawable.Drawable;
import android.content.res.Resources$Theme;
import androidx.appcompat.R;
import android.util.AttributeSet;
import android.graphics.Rect;
import android.widget.SpinnerAdapter;
import android.content.Context;
import androidx.core.view.TintableBackgroundView;
import android.widget.Spinner;

public class AppCompatSpinner extends Spinner implements TintableBackgroundView
{
    private static final int[] ATTRS_ANDROID_SPINNERMODE;
    private static final int MAX_ITEMS_MEASURED = 15;
    private static final int MODE_DIALOG = 0;
    private static final int MODE_DROPDOWN = 1;
    private static final int MODE_THEME = -1;
    private static final String TAG = "AppCompatSpinner";
    private final AppCompatBackgroundHelper mBackgroundTintHelper;
    int mDropDownWidth;
    private ForwardingListener mForwardingListener;
    private SpinnerPopup mPopup;
    private final Context mPopupContext;
    private final boolean mPopupSet;
    private SpinnerAdapter mTempAdapter;
    final Rect mTempRect;
    
    static {
        ATTRS_ANDROID_SPINNERMODE = new int[] { 16843505 };
    }
    
    public AppCompatSpinner(final Context context) {
        this(context, null);
    }
    
    public AppCompatSpinner(final Context context, final int n) {
        this(context, null, R.attr.spinnerStyle, n);
    }
    
    public AppCompatSpinner(final Context context, final AttributeSet set) {
        this(context, set, R.attr.spinnerStyle);
    }
    
    public AppCompatSpinner(final Context context, final AttributeSet set, final int n) {
        this(context, set, n, -1);
    }
    
    public AppCompatSpinner(final Context context, final AttributeSet set, final int n, final int n2) {
        this(context, set, n, n2, null);
    }
    
    public AppCompatSpinner(final Context p0, final AttributeSet p1, final int p2, final int p3, final Resources$Theme p4) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: aload_1        
        //     2: aload_2        
        //     3: iload_3        
        //     4: invokespecial   android/widget/Spinner.<init>:(Landroid/content/Context;Landroid/util/AttributeSet;I)V
        //     7: aload_0        
        //     8: new             Landroid/graphics/Rect;
        //    11: dup            
        //    12: invokespecial   android/graphics/Rect.<init>:()V
        //    15: putfield        androidx/appcompat/widget/AppCompatSpinner.mTempRect:Landroid/graphics/Rect;
        //    18: aload_0        
        //    19: aload_0        
        //    20: invokevirtual   androidx/appcompat/widget/AppCompatSpinner.getContext:()Landroid/content/Context;
        //    23: invokestatic    androidx/appcompat/widget/ThemeUtils.checkAppCompatTheme:(Landroid/view/View;Landroid/content/Context;)V
        //    26: aload_1        
        //    27: aload_2        
        //    28: getstatic       androidx/appcompat/R$styleable.Spinner:[I
        //    31: iload_3        
        //    32: iconst_0       
        //    33: invokestatic    androidx/appcompat/widget/TintTypedArray.obtainStyledAttributes:(Landroid/content/Context;Landroid/util/AttributeSet;[III)Landroidx/appcompat/widget/TintTypedArray;
        //    36: astore          10
        //    38: aload_0        
        //    39: new             Landroidx/appcompat/widget/AppCompatBackgroundHelper;
        //    42: dup            
        //    43: aload_0        
        //    44: invokespecial   androidx/appcompat/widget/AppCompatBackgroundHelper.<init>:(Landroid/view/View;)V
        //    47: putfield        androidx/appcompat/widget/AppCompatSpinner.mBackgroundTintHelper:Landroidx/appcompat/widget/AppCompatBackgroundHelper;
        //    50: aload           5
        //    52: ifnull          72
        //    55: aload_0        
        //    56: new             Landroidx/appcompat/view/ContextThemeWrapper;
        //    59: dup            
        //    60: aload_1        
        //    61: aload           5
        //    63: invokespecial   androidx/appcompat/view/ContextThemeWrapper.<init>:(Landroid/content/Context;Landroid/content/res/Resources$Theme;)V
        //    66: putfield        androidx/appcompat/widget/AppCompatSpinner.mPopupContext:Landroid/content/Context;
        //    69: goto            110
        //    72: aload           10
        //    74: getstatic       androidx/appcompat/R$styleable.Spinner_popupTheme:I
        //    77: iconst_0       
        //    78: invokevirtual   androidx/appcompat/widget/TintTypedArray.getResourceId:(II)I
        //    81: istore          6
        //    83: iload           6
        //    85: ifeq            105
        //    88: aload_0        
        //    89: new             Landroidx/appcompat/view/ContextThemeWrapper;
        //    92: dup            
        //    93: aload_1        
        //    94: iload           6
        //    96: invokespecial   androidx/appcompat/view/ContextThemeWrapper.<init>:(Landroid/content/Context;I)V
        //    99: putfield        androidx/appcompat/widget/AppCompatSpinner.mPopupContext:Landroid/content/Context;
        //   102: goto            110
        //   105: aload_0        
        //   106: aload_1        
        //   107: putfield        androidx/appcompat/widget/AppCompatSpinner.mPopupContext:Landroid/content/Context;
        //   110: aconst_null    
        //   111: astore          8
        //   113: iload           4
        //   115: istore          7
        //   117: iload           4
        //   119: iconst_m1      
        //   120: if_icmpne       249
        //   123: aload_1        
        //   124: aload_2        
        //   125: getstatic       androidx/appcompat/widget/AppCompatSpinner.ATTRS_ANDROID_SPINNERMODE:[I
        //   128: iload_3        
        //   129: iconst_0       
        //   130: invokevirtual   android/content/Context.obtainStyledAttributes:(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;
        //   133: astore          5
        //   135: iload           4
        //   137: istore          6
        //   139: aload           5
        //   141: astore          8
        //   143: aload           5
        //   145: iconst_0       
        //   146: invokevirtual   android/content/res/TypedArray.hasValue:(I)Z
        //   149: ifeq            165
        //   152: aload           5
        //   154: astore          8
        //   156: aload           5
        //   158: iconst_0       
        //   159: iconst_0       
        //   160: invokevirtual   android/content/res/TypedArray.getInt:(II)I
        //   163: istore          6
        //   165: iload           6
        //   167: istore          7
        //   169: aload           5
        //   171: ifnull          249
        //   174: iload           6
        //   176: istore          4
        //   178: aload           5
        //   180: invokevirtual   android/content/res/TypedArray.recycle:()V
        //   183: iload           4
        //   185: istore          7
        //   187: goto            249
        //   190: astore          9
        //   192: goto            207
        //   195: astore_2       
        //   196: aload           8
        //   198: astore_1       
        //   199: goto            239
        //   202: astore          9
        //   204: aconst_null    
        //   205: astore          5
        //   207: aload           5
        //   209: astore          8
        //   211: ldc             "AppCompatSpinner"
        //   213: ldc             "Could not read android:spinnerMode"
        //   215: aload           9
        //   217: invokestatic    android/util/Log.i:(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //   220: pop            
        //   221: iload           4
        //   223: istore          7
        //   225: aload           5
        //   227: ifnull          249
        //   230: goto            178
        //   233: astore_1       
        //   234: aload_1        
        //   235: astore_2       
        //   236: aload           8
        //   238: astore_1       
        //   239: aload_1        
        //   240: ifnull          247
        //   243: aload_1        
        //   244: invokevirtual   android/content/res/TypedArray.recycle:()V
        //   247: aload_2        
        //   248: athrow         
        //   249: iload           7
        //   251: ifeq            363
        //   254: iload           7
        //   256: iconst_1       
        //   257: if_icmpeq       263
        //   260: goto            394
        //   263: new             Landroidx/appcompat/widget/AppCompatSpinner$DropdownPopup;
        //   266: dup            
        //   267: aload_0        
        //   268: aload_0        
        //   269: getfield        androidx/appcompat/widget/AppCompatSpinner.mPopupContext:Landroid/content/Context;
        //   272: aload_2        
        //   273: iload_3        
        //   274: invokespecial   androidx/appcompat/widget/AppCompatSpinner$DropdownPopup.<init>:(Landroidx/appcompat/widget/AppCompatSpinner;Landroid/content/Context;Landroid/util/AttributeSet;I)V
        //   277: astore          5
        //   279: aload_0        
        //   280: getfield        androidx/appcompat/widget/AppCompatSpinner.mPopupContext:Landroid/content/Context;
        //   283: aload_2        
        //   284: getstatic       androidx/appcompat/R$styleable.Spinner:[I
        //   287: iload_3        
        //   288: iconst_0       
        //   289: invokestatic    androidx/appcompat/widget/TintTypedArray.obtainStyledAttributes:(Landroid/content/Context;Landroid/util/AttributeSet;[III)Landroidx/appcompat/widget/TintTypedArray;
        //   292: astore          8
        //   294: aload_0        
        //   295: aload           8
        //   297: getstatic       androidx/appcompat/R$styleable.Spinner_android_dropDownWidth:I
        //   300: bipush          -2
        //   302: invokevirtual   androidx/appcompat/widget/TintTypedArray.getLayoutDimension:(II)I
        //   305: putfield        androidx/appcompat/widget/AppCompatSpinner.mDropDownWidth:I
        //   308: aload           5
        //   310: aload           8
        //   312: getstatic       androidx/appcompat/R$styleable.Spinner_android_popupBackground:I
        //   315: invokevirtual   androidx/appcompat/widget/TintTypedArray.getDrawable:(I)Landroid/graphics/drawable/Drawable;
        //   318: invokevirtual   androidx/appcompat/widget/AppCompatSpinner$DropdownPopup.setBackgroundDrawable:(Landroid/graphics/drawable/Drawable;)V
        //   321: aload           5
        //   323: aload           10
        //   325: getstatic       androidx/appcompat/R$styleable.Spinner_android_prompt:I
        //   328: invokevirtual   androidx/appcompat/widget/TintTypedArray.getString:(I)Ljava/lang/String;
        //   331: invokevirtual   androidx/appcompat/widget/AppCompatSpinner$DropdownPopup.setPromptText:(Ljava/lang/CharSequence;)V
        //   334: aload           8
        //   336: invokevirtual   androidx/appcompat/widget/TintTypedArray.recycle:()V
        //   339: aload_0        
        //   340: aload           5
        //   342: putfield        androidx/appcompat/widget/AppCompatSpinner.mPopup:Landroidx/appcompat/widget/AppCompatSpinner$SpinnerPopup;
        //   345: aload_0        
        //   346: new             Landroidx/appcompat/widget/AppCompatSpinner$1;
        //   349: dup            
        //   350: aload_0        
        //   351: aload_0        
        //   352: aload           5
        //   354: invokespecial   androidx/appcompat/widget/AppCompatSpinner$1.<init>:(Landroidx/appcompat/widget/AppCompatSpinner;Landroid/view/View;Landroidx/appcompat/widget/AppCompatSpinner$DropdownPopup;)V
        //   357: putfield        androidx/appcompat/widget/AppCompatSpinner.mForwardingListener:Landroidx/appcompat/widget/ForwardingListener;
        //   360: goto            394
        //   363: new             Landroidx/appcompat/widget/AppCompatSpinner$DialogPopup;
        //   366: dup            
        //   367: aload_0        
        //   368: invokespecial   androidx/appcompat/widget/AppCompatSpinner$DialogPopup.<init>:(Landroidx/appcompat/widget/AppCompatSpinner;)V
        //   371: astore          5
        //   373: aload_0        
        //   374: aload           5
        //   376: putfield        androidx/appcompat/widget/AppCompatSpinner.mPopup:Landroidx/appcompat/widget/AppCompatSpinner$SpinnerPopup;
        //   379: aload           5
        //   381: aload           10
        //   383: getstatic       androidx/appcompat/R$styleable.Spinner_android_prompt:I
        //   386: invokevirtual   androidx/appcompat/widget/TintTypedArray.getString:(I)Ljava/lang/String;
        //   389: invokeinterface androidx/appcompat/widget/AppCompatSpinner$SpinnerPopup.setPromptText:(Ljava/lang/CharSequence;)V
        //   394: aload           10
        //   396: getstatic       androidx/appcompat/R$styleable.Spinner_android_entries:I
        //   399: invokevirtual   androidx/appcompat/widget/TintTypedArray.getTextArray:(I)[Ljava/lang/CharSequence;
        //   402: astore          5
        //   404: aload           5
        //   406: ifnull          434
        //   409: new             Landroid/widget/ArrayAdapter;
        //   412: dup            
        //   413: aload_1        
        //   414: ldc             17367048
        //   416: aload           5
        //   418: invokespecial   android/widget/ArrayAdapter.<init>:(Landroid/content/Context;I[Ljava/lang/Object;)V
        //   421: astore_1       
        //   422: aload_1        
        //   423: getstatic       androidx/appcompat/R$layout.support_simple_spinner_dropdown_item:I
        //   426: invokevirtual   android/widget/ArrayAdapter.setDropDownViewResource:(I)V
        //   429: aload_0        
        //   430: aload_1        
        //   431: invokevirtual   androidx/appcompat/widget/AppCompatSpinner.setAdapter:(Landroid/widget/SpinnerAdapter;)V
        //   434: aload           10
        //   436: invokevirtual   androidx/appcompat/widget/TintTypedArray.recycle:()V
        //   439: aload_0        
        //   440: iconst_1       
        //   441: putfield        androidx/appcompat/widget/AppCompatSpinner.mPopupSet:Z
        //   444: aload_0        
        //   445: getfield        androidx/appcompat/widget/AppCompatSpinner.mTempAdapter:Landroid/widget/SpinnerAdapter;
        //   448: astore_1       
        //   449: aload_1        
        //   450: ifnull          463
        //   453: aload_0        
        //   454: aload_1        
        //   455: invokevirtual   androidx/appcompat/widget/AppCompatSpinner.setAdapter:(Landroid/widget/SpinnerAdapter;)V
        //   458: aload_0        
        //   459: aconst_null    
        //   460: putfield        androidx/appcompat/widget/AppCompatSpinner.mTempAdapter:Landroid/widget/SpinnerAdapter;
        //   463: aload_0        
        //   464: getfield        androidx/appcompat/widget/AppCompatSpinner.mBackgroundTintHelper:Landroidx/appcompat/widget/AppCompatBackgroundHelper;
        //   467: aload_2        
        //   468: iload_3        
        //   469: invokevirtual   androidx/appcompat/widget/AppCompatBackgroundHelper.loadFromAttributes:(Landroid/util/AttributeSet;I)V
        //   472: return         
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                 
        //  -----  -----  -----  -----  ---------------------
        //  123    135    202    207    Ljava/lang/Exception;
        //  123    135    195    202    Any
        //  143    152    190    195    Ljava/lang/Exception;
        //  143    152    233    239    Any
        //  156    165    190    195    Ljava/lang/Exception;
        //  156    165    233    239    Any
        //  211    221    233    239    Any
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0165:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2604)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:206)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createConstructor(AstBuilder.java:799)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:635)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    int compatMeasureContentWidth(final SpinnerAdapter spinnerAdapter, final Drawable drawable) {
        int n = 0;
        if (spinnerAdapter == null) {
            return 0;
        }
        final int measureSpec = View$MeasureSpec.makeMeasureSpec(this.getMeasuredWidth(), 0);
        final int measureSpec2 = View$MeasureSpec.makeMeasureSpec(this.getMeasuredHeight(), 0);
        final int max = Math.max(0, this.getSelectedItemPosition());
        final int min = Math.min(spinnerAdapter.getCount(), max + 15);
        int i = Math.max(0, max - (15 - (min - max)));
        View view = null;
        int max2 = 0;
        while (i < min) {
            final int itemViewType = spinnerAdapter.getItemViewType(i);
            int n2;
            if (itemViewType != (n2 = n)) {
                view = null;
                n2 = itemViewType;
            }
            view = spinnerAdapter.getView(i, view, (ViewGroup)this);
            if (view.getLayoutParams() == null) {
                view.setLayoutParams(new ViewGroup$LayoutParams(-2, -2));
            }
            view.measure(measureSpec, measureSpec2);
            max2 = Math.max(max2, view.getMeasuredWidth());
            ++i;
            n = n2;
        }
        int n3 = max2;
        if (drawable != null) {
            drawable.getPadding(this.mTempRect);
            n3 = max2 + (this.mTempRect.left + this.mTempRect.right);
        }
        return n3;
    }
    
    protected void drawableStateChanged() {
        super.drawableStateChanged();
        final AppCompatBackgroundHelper mBackgroundTintHelper = this.mBackgroundTintHelper;
        if (mBackgroundTintHelper != null) {
            mBackgroundTintHelper.applySupportBackgroundTint();
        }
    }
    
    public int getDropDownHorizontalOffset() {
        final SpinnerPopup mPopup = this.mPopup;
        if (mPopup != null) {
            return mPopup.getHorizontalOffset();
        }
        if (Build$VERSION.SDK_INT >= 16) {
            return super.getDropDownHorizontalOffset();
        }
        return 0;
    }
    
    public int getDropDownVerticalOffset() {
        final SpinnerPopup mPopup = this.mPopup;
        if (mPopup != null) {
            return mPopup.getVerticalOffset();
        }
        if (Build$VERSION.SDK_INT >= 16) {
            return super.getDropDownVerticalOffset();
        }
        return 0;
    }
    
    public int getDropDownWidth() {
        if (this.mPopup != null) {
            return this.mDropDownWidth;
        }
        if (Build$VERSION.SDK_INT >= 16) {
            return super.getDropDownWidth();
        }
        return 0;
    }
    
    final SpinnerPopup getInternalPopup() {
        return this.mPopup;
    }
    
    public Drawable getPopupBackground() {
        final SpinnerPopup mPopup = this.mPopup;
        if (mPopup != null) {
            return mPopup.getBackground();
        }
        if (Build$VERSION.SDK_INT >= 16) {
            return super.getPopupBackground();
        }
        return null;
    }
    
    public Context getPopupContext() {
        return this.mPopupContext;
    }
    
    public CharSequence getPrompt() {
        final SpinnerPopup mPopup = this.mPopup;
        CharSequence charSequence;
        if (mPopup != null) {
            charSequence = mPopup.getHintText();
        }
        else {
            charSequence = super.getPrompt();
        }
        return charSequence;
    }
    
    public ColorStateList getSupportBackgroundTintList() {
        final AppCompatBackgroundHelper mBackgroundTintHelper = this.mBackgroundTintHelper;
        ColorStateList supportBackgroundTintList;
        if (mBackgroundTintHelper != null) {
            supportBackgroundTintList = mBackgroundTintHelper.getSupportBackgroundTintList();
        }
        else {
            supportBackgroundTintList = null;
        }
        return supportBackgroundTintList;
    }
    
    public PorterDuff$Mode getSupportBackgroundTintMode() {
        final AppCompatBackgroundHelper mBackgroundTintHelper = this.mBackgroundTintHelper;
        PorterDuff$Mode supportBackgroundTintMode;
        if (mBackgroundTintHelper != null) {
            supportBackgroundTintMode = mBackgroundTintHelper.getSupportBackgroundTintMode();
        }
        else {
            supportBackgroundTintMode = null;
        }
        return supportBackgroundTintMode;
    }
    
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        final SpinnerPopup mPopup = this.mPopup;
        if (mPopup != null && mPopup.isShowing()) {
            this.mPopup.dismiss();
        }
    }
    
    protected void onMeasure(final int n, final int n2) {
        super.onMeasure(n, n2);
        if (this.mPopup != null && View$MeasureSpec.getMode(n) == Integer.MIN_VALUE) {
            this.setMeasuredDimension(Math.min(Math.max(this.getMeasuredWidth(), this.compatMeasureContentWidth(this.getAdapter(), this.getBackground())), View$MeasureSpec.getSize(n)), this.getMeasuredHeight());
        }
    }
    
    public void onRestoreInstanceState(final Parcelable parcelable) {
        final SavedState savedState = (SavedState)parcelable;
        super.onRestoreInstanceState(savedState.getSuperState());
        if (savedState.mShowDropdown) {
            final ViewTreeObserver viewTreeObserver = this.getViewTreeObserver();
            if (viewTreeObserver != null) {
                viewTreeObserver.addOnGlobalLayoutListener((ViewTreeObserver$OnGlobalLayoutListener)new ViewTreeObserver$OnGlobalLayoutListener(this) {
                    final AppCompatSpinner this$0;
                    
                    public void onGlobalLayout() {
                        if (!this.this$0.getInternalPopup().isShowing()) {
                            this.this$0.showPopup();
                        }
                        final ViewTreeObserver viewTreeObserver = this.this$0.getViewTreeObserver();
                        if (viewTreeObserver != null) {
                            if (Build$VERSION.SDK_INT >= 16) {
                                Api16Impl.removeOnGlobalLayoutListener(viewTreeObserver, (ViewTreeObserver$OnGlobalLayoutListener)this);
                            }
                            else {
                                viewTreeObserver.removeGlobalOnLayoutListener((ViewTreeObserver$OnGlobalLayoutListener)this);
                            }
                        }
                    }
                });
            }
        }
    }
    
    public Parcelable onSaveInstanceState() {
        final SavedState savedState = new SavedState(super.onSaveInstanceState());
        final SpinnerPopup mPopup = this.mPopup;
        savedState.mShowDropdown = (mPopup != null && mPopup.isShowing());
        return (Parcelable)savedState;
    }
    
    public boolean onTouchEvent(final MotionEvent motionEvent) {
        final ForwardingListener mForwardingListener = this.mForwardingListener;
        return (mForwardingListener != null && mForwardingListener.onTouch((View)this, motionEvent)) || super.onTouchEvent(motionEvent);
    }
    
    public boolean performClick() {
        final SpinnerPopup mPopup = this.mPopup;
        if (mPopup != null) {
            if (!mPopup.isShowing()) {
                this.showPopup();
            }
            return true;
        }
        return super.performClick();
    }
    
    public void setAdapter(final SpinnerAdapter spinnerAdapter) {
        if (!this.mPopupSet) {
            this.mTempAdapter = spinnerAdapter;
            return;
        }
        super.setAdapter(spinnerAdapter);
        if (this.mPopup != null) {
            Context context;
            if ((context = this.mPopupContext) == null) {
                context = this.getContext();
            }
            this.mPopup.setAdapter((ListAdapter)new DropDownAdapter(spinnerAdapter, context.getTheme()));
        }
    }
    
    public void setBackgroundDrawable(final Drawable backgroundDrawable) {
        super.setBackgroundDrawable(backgroundDrawable);
        final AppCompatBackgroundHelper mBackgroundTintHelper = this.mBackgroundTintHelper;
        if (mBackgroundTintHelper != null) {
            mBackgroundTintHelper.onSetBackgroundDrawable(backgroundDrawable);
        }
    }
    
    public void setBackgroundResource(final int backgroundResource) {
        super.setBackgroundResource(backgroundResource);
        final AppCompatBackgroundHelper mBackgroundTintHelper = this.mBackgroundTintHelper;
        if (mBackgroundTintHelper != null) {
            mBackgroundTintHelper.onSetBackgroundResource(backgroundResource);
        }
    }
    
    public void setDropDownHorizontalOffset(final int dropDownHorizontalOffset) {
        final SpinnerPopup mPopup = this.mPopup;
        if (mPopup != null) {
            mPopup.setHorizontalOriginalOffset(dropDownHorizontalOffset);
            this.mPopup.setHorizontalOffset(dropDownHorizontalOffset);
        }
        else if (Build$VERSION.SDK_INT >= 16) {
            super.setDropDownHorizontalOffset(dropDownHorizontalOffset);
        }
    }
    
    public void setDropDownVerticalOffset(final int n) {
        final SpinnerPopup mPopup = this.mPopup;
        if (mPopup != null) {
            mPopup.setVerticalOffset(n);
        }
        else if (Build$VERSION.SDK_INT >= 16) {
            super.setDropDownVerticalOffset(n);
        }
    }
    
    public void setDropDownWidth(final int n) {
        if (this.mPopup != null) {
            this.mDropDownWidth = n;
        }
        else if (Build$VERSION.SDK_INT >= 16) {
            super.setDropDownWidth(n);
        }
    }
    
    public void setPopupBackgroundDrawable(final Drawable drawable) {
        final SpinnerPopup mPopup = this.mPopup;
        if (mPopup != null) {
            mPopup.setBackgroundDrawable(drawable);
        }
        else if (Build$VERSION.SDK_INT >= 16) {
            super.setPopupBackgroundDrawable(drawable);
        }
    }
    
    public void setPopupBackgroundResource(final int n) {
        this.setPopupBackgroundDrawable(AppCompatResources.getDrawable(this.getPopupContext(), n));
    }
    
    public void setPrompt(final CharSequence charSequence) {
        final SpinnerPopup mPopup = this.mPopup;
        if (mPopup != null) {
            mPopup.setPromptText(charSequence);
        }
        else {
            super.setPrompt(charSequence);
        }
    }
    
    public void setSupportBackgroundTintList(final ColorStateList supportBackgroundTintList) {
        final AppCompatBackgroundHelper mBackgroundTintHelper = this.mBackgroundTintHelper;
        if (mBackgroundTintHelper != null) {
            mBackgroundTintHelper.setSupportBackgroundTintList(supportBackgroundTintList);
        }
    }
    
    public void setSupportBackgroundTintMode(final PorterDuff$Mode supportBackgroundTintMode) {
        final AppCompatBackgroundHelper mBackgroundTintHelper = this.mBackgroundTintHelper;
        if (mBackgroundTintHelper != null) {
            mBackgroundTintHelper.setSupportBackgroundTintMode(supportBackgroundTintMode);
        }
    }
    
    void showPopup() {
        if (Build$VERSION.SDK_INT >= 17) {
            this.mPopup.show(Api17Impl.getTextDirection((View)this), Api17Impl.getTextAlignment((View)this));
        }
        else {
            this.mPopup.show(-1, -1);
        }
    }
    
    private static final class Api16Impl
    {
        static void removeOnGlobalLayoutListener(final ViewTreeObserver viewTreeObserver, final ViewTreeObserver$OnGlobalLayoutListener viewTreeObserver$OnGlobalLayoutListener) {
            viewTreeObserver.removeOnGlobalLayoutListener(viewTreeObserver$OnGlobalLayoutListener);
        }
    }
    
    private static final class Api17Impl
    {
        static int getTextAlignment(final View view) {
            return view.getTextAlignment();
        }
        
        static int getTextDirection(final View view) {
            return view.getTextDirection();
        }
        
        static void setTextAlignment(final View view, final int textAlignment) {
            view.setTextAlignment(textAlignment);
        }
        
        static void setTextDirection(final View view, final int textDirection) {
            view.setTextDirection(textDirection);
        }
    }
    
    private static final class Api23Impl
    {
        static void setDropDownViewTheme(final ThemedSpinnerAdapter themedSpinnerAdapter, final Resources$Theme dropDownViewTheme) {
            if (!ObjectsCompat.equals(themedSpinnerAdapter.getDropDownViewTheme(), dropDownViewTheme)) {
                themedSpinnerAdapter.setDropDownViewTheme(dropDownViewTheme);
            }
        }
    }
    
    class DialogPopup implements SpinnerPopup, DialogInterface$OnClickListener
    {
        private ListAdapter mListAdapter;
        AlertDialog mPopup;
        private CharSequence mPrompt;
        final AppCompatSpinner this$0;
        
        DialogPopup(final AppCompatSpinner this$0) {
            this.this$0 = this$0;
        }
        
        @Override
        public void dismiss() {
            final AlertDialog mPopup = this.mPopup;
            if (mPopup != null) {
                mPopup.dismiss();
                this.mPopup = null;
            }
        }
        
        @Override
        public Drawable getBackground() {
            return null;
        }
        
        @Override
        public CharSequence getHintText() {
            return this.mPrompt;
        }
        
        @Override
        public int getHorizontalOffset() {
            return 0;
        }
        
        @Override
        public int getHorizontalOriginalOffset() {
            return 0;
        }
        
        @Override
        public int getVerticalOffset() {
            return 0;
        }
        
        @Override
        public boolean isShowing() {
            final AlertDialog mPopup = this.mPopup;
            return mPopup != null && mPopup.isShowing();
        }
        
        public void onClick(final DialogInterface dialogInterface, final int selection) {
            this.this$0.setSelection(selection);
            if (this.this$0.getOnItemClickListener() != null) {
                this.this$0.performItemClick((View)null, selection, this.mListAdapter.getItemId(selection));
            }
            this.dismiss();
        }
        
        @Override
        public void setAdapter(final ListAdapter mListAdapter) {
            this.mListAdapter = mListAdapter;
        }
        
        @Override
        public void setBackgroundDrawable(final Drawable drawable) {
            Log.e("AppCompatSpinner", "Cannot set popup background for MODE_DIALOG, ignoring");
        }
        
        @Override
        public void setHorizontalOffset(final int n) {
            Log.e("AppCompatSpinner", "Cannot set horizontal offset for MODE_DIALOG, ignoring");
        }
        
        @Override
        public void setHorizontalOriginalOffset(final int n) {
            Log.e("AppCompatSpinner", "Cannot set horizontal (original) offset for MODE_DIALOG, ignoring");
        }
        
        @Override
        public void setPromptText(final CharSequence mPrompt) {
            this.mPrompt = mPrompt;
        }
        
        @Override
        public void setVerticalOffset(final int n) {
            Log.e("AppCompatSpinner", "Cannot set vertical offset for MODE_DIALOG, ignoring");
        }
        
        @Override
        public void show(final int n, final int n2) {
            if (this.mListAdapter == null) {
                return;
            }
            final AlertDialog.Builder builder = new AlertDialog.Builder(this.this$0.getPopupContext());
            final CharSequence mPrompt = this.mPrompt;
            if (mPrompt != null) {
                builder.setTitle(mPrompt);
            }
            final AlertDialog create = builder.setSingleChoiceItems(this.mListAdapter, this.this$0.getSelectedItemPosition(), (DialogInterface$OnClickListener)this).create();
            this.mPopup = create;
            final ListView listView = create.getListView();
            if (Build$VERSION.SDK_INT >= 17) {
                Api17Impl.setTextDirection((View)listView, n);
                Api17Impl.setTextAlignment((View)listView, n2);
            }
            this.mPopup.show();
        }
    }
    
    interface SpinnerPopup
    {
        void dismiss();
        
        Drawable getBackground();
        
        CharSequence getHintText();
        
        int getHorizontalOffset();
        
        int getHorizontalOriginalOffset();
        
        int getVerticalOffset();
        
        boolean isShowing();
        
        void setAdapter(final ListAdapter p0);
        
        void setBackgroundDrawable(final Drawable p0);
        
        void setHorizontalOffset(final int p0);
        
        void setHorizontalOriginalOffset(final int p0);
        
        void setPromptText(final CharSequence p0);
        
        void setVerticalOffset(final int p0);
        
        void show(final int p0, final int p1);
    }
    
    private static class DropDownAdapter implements ListAdapter, SpinnerAdapter
    {
        private SpinnerAdapter mAdapter;
        private ListAdapter mListAdapter;
        
        public DropDownAdapter(final SpinnerAdapter mAdapter, final Resources$Theme dropDownViewTheme) {
            this.mAdapter = mAdapter;
            if (mAdapter instanceof ListAdapter) {
                this.mListAdapter = (ListAdapter)mAdapter;
            }
            if (dropDownViewTheme != null) {
                if (Build$VERSION.SDK_INT >= 23 && mAdapter instanceof ThemedSpinnerAdapter) {
                    Api23Impl.setDropDownViewTheme((ThemedSpinnerAdapter)mAdapter, dropDownViewTheme);
                }
                else if (mAdapter instanceof androidx.appcompat.widget.ThemedSpinnerAdapter) {
                    final androidx.appcompat.widget.ThemedSpinnerAdapter themedSpinnerAdapter = (androidx.appcompat.widget.ThemedSpinnerAdapter)mAdapter;
                    if (themedSpinnerAdapter.getDropDownViewTheme() == null) {
                        themedSpinnerAdapter.setDropDownViewTheme(dropDownViewTheme);
                    }
                }
            }
        }
        
        public boolean areAllItemsEnabled() {
            final ListAdapter mListAdapter = this.mListAdapter;
            return mListAdapter == null || mListAdapter.areAllItemsEnabled();
        }
        
        public int getCount() {
            final SpinnerAdapter mAdapter = this.mAdapter;
            int count;
            if (mAdapter == null) {
                count = 0;
            }
            else {
                count = mAdapter.getCount();
            }
            return count;
        }
        
        public View getDropDownView(final int n, View dropDownView, final ViewGroup viewGroup) {
            final SpinnerAdapter mAdapter = this.mAdapter;
            if (mAdapter == null) {
                dropDownView = null;
            }
            else {
                dropDownView = mAdapter.getDropDownView(n, dropDownView, viewGroup);
            }
            return dropDownView;
        }
        
        public Object getItem(final int n) {
            final SpinnerAdapter mAdapter = this.mAdapter;
            Object item;
            if (mAdapter == null) {
                item = null;
            }
            else {
                item = mAdapter.getItem(n);
            }
            return item;
        }
        
        public long getItemId(final int n) {
            final SpinnerAdapter mAdapter = this.mAdapter;
            long itemId;
            if (mAdapter == null) {
                itemId = -1L;
            }
            else {
                itemId = mAdapter.getItemId(n);
            }
            return itemId;
        }
        
        public int getItemViewType(final int n) {
            return 0;
        }
        
        public View getView(final int n, final View view, final ViewGroup viewGroup) {
            return this.getDropDownView(n, view, viewGroup);
        }
        
        public int getViewTypeCount() {
            return 1;
        }
        
        public boolean hasStableIds() {
            final SpinnerAdapter mAdapter = this.mAdapter;
            return mAdapter != null && mAdapter.hasStableIds();
        }
        
        public boolean isEmpty() {
            return this.getCount() == 0;
        }
        
        public boolean isEnabled(final int n) {
            final ListAdapter mListAdapter = this.mListAdapter;
            return mListAdapter == null || mListAdapter.isEnabled(n);
        }
        
        public void registerDataSetObserver(final DataSetObserver dataSetObserver) {
            final SpinnerAdapter mAdapter = this.mAdapter;
            if (mAdapter != null) {
                mAdapter.registerDataSetObserver(dataSetObserver);
            }
        }
        
        public void unregisterDataSetObserver(final DataSetObserver dataSetObserver) {
            final SpinnerAdapter mAdapter = this.mAdapter;
            if (mAdapter != null) {
                mAdapter.unregisterDataSetObserver(dataSetObserver);
            }
        }
    }
    
    class DropdownPopup extends ListPopupWindow implements SpinnerPopup
    {
        ListAdapter mAdapter;
        private CharSequence mHintText;
        private int mOriginalHorizontalOffset;
        private final Rect mVisibleRect;
        final AppCompatSpinner this$0;
        
        public DropdownPopup(final AppCompatSpinner appCompatSpinner, final Context context, final AttributeSet set, final int n) {
            this.this$0 = appCompatSpinner;
            super(context, set, n);
            this.mVisibleRect = new Rect();
            this.setAnchorView((View)appCompatSpinner);
            this.setModal(true);
            this.setPromptPosition(0);
            this.setOnItemClickListener((AdapterView$OnItemClickListener)new AdapterView$OnItemClickListener(this, appCompatSpinner) {
                final DropdownPopup this$1;
                final AppCompatSpinner val$this$0;
                
                public void onItemClick(final AdapterView<?> adapterView, final View view, final int selection, final long n) {
                    this.this$1.this$0.setSelection(selection);
                    if (this.this$1.this$0.getOnItemClickListener() != null) {
                        this.this$1.this$0.performItemClick(view, selection, this.this$1.mAdapter.getItemId(selection));
                    }
                    this.this$1.dismiss();
                }
            });
        }
        
        void computeContentWidth() {
            final Drawable background = this.getBackground();
            int right = 0;
            if (background != null) {
                background.getPadding(this.this$0.mTempRect);
                if (ViewUtils.isLayoutRtl((View)this.this$0)) {
                    right = this.this$0.mTempRect.right;
                }
                else {
                    right = -this.this$0.mTempRect.left;
                }
            }
            else {
                final Rect mTempRect = this.this$0.mTempRect;
                this.this$0.mTempRect.right = 0;
                mTempRect.left = 0;
            }
            final int paddingLeft = this.this$0.getPaddingLeft();
            final int paddingRight = this.this$0.getPaddingRight();
            final int width = this.this$0.getWidth();
            if (this.this$0.mDropDownWidth == -2) {
                final int compatMeasureContentWidth = this.this$0.compatMeasureContentWidth((SpinnerAdapter)this.mAdapter, this.getBackground());
                final int n = this.this$0.getContext().getResources().getDisplayMetrics().widthPixels - this.this$0.mTempRect.left - this.this$0.mTempRect.right;
                int a;
                if ((a = compatMeasureContentWidth) > n) {
                    a = n;
                }
                this.setContentWidth(Math.max(a, width - paddingLeft - paddingRight));
            }
            else if (this.this$0.mDropDownWidth == -1) {
                this.setContentWidth(width - paddingLeft - paddingRight);
            }
            else {
                this.setContentWidth(this.this$0.mDropDownWidth);
            }
            int horizontalOffset;
            if (ViewUtils.isLayoutRtl((View)this.this$0)) {
                horizontalOffset = right + (width - paddingRight - this.getWidth() - this.getHorizontalOriginalOffset());
            }
            else {
                horizontalOffset = right + (paddingLeft + this.getHorizontalOriginalOffset());
            }
            this.setHorizontalOffset(horizontalOffset);
        }
        
        @Override
        public CharSequence getHintText() {
            return this.mHintText;
        }
        
        @Override
        public int getHorizontalOriginalOffset() {
            return this.mOriginalHorizontalOffset;
        }
        
        boolean isVisibleToUser(final View view) {
            return ViewCompat.isAttachedToWindow(view) && view.getGlobalVisibleRect(this.mVisibleRect);
        }
        
        @Override
        public void setAdapter(final ListAdapter listAdapter) {
            super.setAdapter(listAdapter);
            this.mAdapter = listAdapter;
        }
        
        @Override
        public void setHorizontalOriginalOffset(final int mOriginalHorizontalOffset) {
            this.mOriginalHorizontalOffset = mOriginalHorizontalOffset;
        }
        
        @Override
        public void setPromptText(final CharSequence mHintText) {
            this.mHintText = mHintText;
        }
        
        @Override
        public void show(final int n, final int n2) {
            final boolean showing = this.isShowing();
            this.computeContentWidth();
            this.setInputMethodMode(2);
            super.show();
            final ListView listView = this.getListView();
            listView.setChoiceMode(1);
            if (Build$VERSION.SDK_INT >= 17) {
                Api17Impl.setTextDirection((View)listView, n);
                Api17Impl.setTextAlignment((View)listView, n2);
            }
            this.setSelection(this.this$0.getSelectedItemPosition());
            if (showing) {
                return;
            }
            final ViewTreeObserver viewTreeObserver = this.this$0.getViewTreeObserver();
            if (viewTreeObserver != null) {
                final ViewTreeObserver$OnGlobalLayoutListener viewTreeObserver$OnGlobalLayoutListener = (ViewTreeObserver$OnGlobalLayoutListener)new ViewTreeObserver$OnGlobalLayoutListener(this) {
                    final DropdownPopup this$1;
                    
                    public void onGlobalLayout() {
                        final DropdownPopup this$1 = this.this$1;
                        if (!this$1.isVisibleToUser((View)this$1.this$0)) {
                            this.this$1.dismiss();
                        }
                        else {
                            this.this$1.computeContentWidth();
                            this.this$1.show();
                        }
                    }
                };
                viewTreeObserver.addOnGlobalLayoutListener((ViewTreeObserver$OnGlobalLayoutListener)viewTreeObserver$OnGlobalLayoutListener);
                this.setOnDismissListener((PopupWindow$OnDismissListener)new PopupWindow$OnDismissListener(this, viewTreeObserver$OnGlobalLayoutListener) {
                    final DropdownPopup this$1;
                    final ViewTreeObserver$OnGlobalLayoutListener val$layoutListener;
                    
                    public void onDismiss() {
                        final ViewTreeObserver viewTreeObserver = this.this$1.this$0.getViewTreeObserver();
                        if (viewTreeObserver != null) {
                            viewTreeObserver.removeGlobalOnLayoutListener(this.val$layoutListener);
                        }
                    }
                });
            }
        }
    }
    
    static class SavedState extends View$BaseSavedState
    {
        public static final Parcelable$Creator<SavedState> CREATOR;
        boolean mShowDropdown;
        
        static {
            CREATOR = (Parcelable$Creator)new Parcelable$Creator<SavedState>() {
                public SavedState createFromParcel(final Parcel parcel) {
                    return new SavedState(parcel);
                }
                
                public SavedState[] newArray(final int n) {
                    return new SavedState[n];
                }
            };
        }
        
        SavedState(final Parcel parcel) {
            super(parcel);
            this.mShowDropdown = (parcel.readByte() != 0);
        }
        
        SavedState(final Parcelable parcelable) {
            super(parcelable);
        }
        
        public void writeToParcel(final Parcel parcel, final int n) {
            super.writeToParcel(parcel, n);
            parcel.writeByte((byte)(byte)(this.mShowDropdown ? 1 : 0));
        }
    }
}

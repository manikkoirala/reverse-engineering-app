// 
// Decompiled by Procyon v0.6.0
// 

package androidx.appcompat.widget;

import androidx.appcompat.content.res.AppCompatResources;
import android.graphics.drawable.Drawable;
import android.view.inputmethod.InputConnection;
import android.view.inputmethod.EditorInfo;
import android.graphics.PorterDuff$Mode;
import android.content.res.ColorStateList;
import androidx.core.widget.TextViewCompat;
import android.view.ActionMode$Callback;
import android.widget.TextView;
import android.view.View;
import androidx.appcompat.R;
import android.util.AttributeSet;
import android.content.Context;
import androidx.core.widget.TintableCompoundDrawablesView;
import androidx.core.view.TintableBackgroundView;
import androidx.core.widget.TintableCheckedTextView;
import android.widget.CheckedTextView;

public class AppCompatCheckedTextView extends CheckedTextView implements TintableCheckedTextView, TintableBackgroundView, EmojiCompatConfigurationView, TintableCompoundDrawablesView
{
    private AppCompatEmojiTextHelper mAppCompatEmojiTextHelper;
    private final AppCompatBackgroundHelper mBackgroundTintHelper;
    private final AppCompatCheckedTextViewHelper mCheckedHelper;
    private final AppCompatTextHelper mTextHelper;
    
    public AppCompatCheckedTextView(final Context context) {
        this(context, null);
    }
    
    public AppCompatCheckedTextView(final Context context, final AttributeSet set) {
        this(context, set, R.attr.checkedTextViewStyle);
    }
    
    public AppCompatCheckedTextView(final Context context, final AttributeSet set, final int n) {
        super(TintContextWrapper.wrap(context), set, n);
        ThemeUtils.checkAppCompatTheme((View)this, this.getContext());
        final AppCompatTextHelper mTextHelper = new AppCompatTextHelper((TextView)this);
        (this.mTextHelper = mTextHelper).loadFromAttributes(set, n);
        mTextHelper.applyCompoundDrawablesTints();
        (this.mBackgroundTintHelper = new AppCompatBackgroundHelper((View)this)).loadFromAttributes(set, n);
        (this.mCheckedHelper = new AppCompatCheckedTextViewHelper(this)).loadFromAttributes(set, n);
        this.getEmojiTextViewHelper().loadFromAttributes(set, n);
    }
    
    private AppCompatEmojiTextHelper getEmojiTextViewHelper() {
        if (this.mAppCompatEmojiTextHelper == null) {
            this.mAppCompatEmojiTextHelper = new AppCompatEmojiTextHelper((TextView)this);
        }
        return this.mAppCompatEmojiTextHelper;
    }
    
    protected void drawableStateChanged() {
        super.drawableStateChanged();
        final AppCompatTextHelper mTextHelper = this.mTextHelper;
        if (mTextHelper != null) {
            mTextHelper.applyCompoundDrawablesTints();
        }
        final AppCompatBackgroundHelper mBackgroundTintHelper = this.mBackgroundTintHelper;
        if (mBackgroundTintHelper != null) {
            mBackgroundTintHelper.applySupportBackgroundTint();
        }
        final AppCompatCheckedTextViewHelper mCheckedHelper = this.mCheckedHelper;
        if (mCheckedHelper != null) {
            mCheckedHelper.applyCheckMarkTint();
        }
    }
    
    public ActionMode$Callback getCustomSelectionActionModeCallback() {
        return TextViewCompat.unwrapCustomSelectionActionModeCallback(super.getCustomSelectionActionModeCallback());
    }
    
    public ColorStateList getSupportBackgroundTintList() {
        final AppCompatBackgroundHelper mBackgroundTintHelper = this.mBackgroundTintHelper;
        ColorStateList supportBackgroundTintList;
        if (mBackgroundTintHelper != null) {
            supportBackgroundTintList = mBackgroundTintHelper.getSupportBackgroundTintList();
        }
        else {
            supportBackgroundTintList = null;
        }
        return supportBackgroundTintList;
    }
    
    public PorterDuff$Mode getSupportBackgroundTintMode() {
        final AppCompatBackgroundHelper mBackgroundTintHelper = this.mBackgroundTintHelper;
        PorterDuff$Mode supportBackgroundTintMode;
        if (mBackgroundTintHelper != null) {
            supportBackgroundTintMode = mBackgroundTintHelper.getSupportBackgroundTintMode();
        }
        else {
            supportBackgroundTintMode = null;
        }
        return supportBackgroundTintMode;
    }
    
    public ColorStateList getSupportCheckMarkTintList() {
        final AppCompatCheckedTextViewHelper mCheckedHelper = this.mCheckedHelper;
        ColorStateList supportCheckMarkTintList;
        if (mCheckedHelper != null) {
            supportCheckMarkTintList = mCheckedHelper.getSupportCheckMarkTintList();
        }
        else {
            supportCheckMarkTintList = null;
        }
        return supportCheckMarkTintList;
    }
    
    public PorterDuff$Mode getSupportCheckMarkTintMode() {
        final AppCompatCheckedTextViewHelper mCheckedHelper = this.mCheckedHelper;
        PorterDuff$Mode supportCheckMarkTintMode;
        if (mCheckedHelper != null) {
            supportCheckMarkTintMode = mCheckedHelper.getSupportCheckMarkTintMode();
        }
        else {
            supportCheckMarkTintMode = null;
        }
        return supportCheckMarkTintMode;
    }
    
    public ColorStateList getSupportCompoundDrawablesTintList() {
        return this.mTextHelper.getCompoundDrawableTintList();
    }
    
    public PorterDuff$Mode getSupportCompoundDrawablesTintMode() {
        return this.mTextHelper.getCompoundDrawableTintMode();
    }
    
    public boolean isEmojiCompatEnabled() {
        return this.getEmojiTextViewHelper().isEnabled();
    }
    
    public InputConnection onCreateInputConnection(final EditorInfo editorInfo) {
        return AppCompatHintHelper.onCreateInputConnection(super.onCreateInputConnection(editorInfo), editorInfo, (View)this);
    }
    
    public void setAllCaps(final boolean b) {
        super.setAllCaps(b);
        this.getEmojiTextViewHelper().setAllCaps(b);
    }
    
    public void setBackgroundDrawable(final Drawable backgroundDrawable) {
        super.setBackgroundDrawable(backgroundDrawable);
        final AppCompatBackgroundHelper mBackgroundTintHelper = this.mBackgroundTintHelper;
        if (mBackgroundTintHelper != null) {
            mBackgroundTintHelper.onSetBackgroundDrawable(backgroundDrawable);
        }
    }
    
    public void setBackgroundResource(final int backgroundResource) {
        super.setBackgroundResource(backgroundResource);
        final AppCompatBackgroundHelper mBackgroundTintHelper = this.mBackgroundTintHelper;
        if (mBackgroundTintHelper != null) {
            mBackgroundTintHelper.onSetBackgroundResource(backgroundResource);
        }
    }
    
    public void setCheckMarkDrawable(final int n) {
        this.setCheckMarkDrawable(AppCompatResources.getDrawable(this.getContext(), n));
    }
    
    public void setCheckMarkDrawable(final Drawable checkMarkDrawable) {
        super.setCheckMarkDrawable(checkMarkDrawable);
        final AppCompatCheckedTextViewHelper mCheckedHelper = this.mCheckedHelper;
        if (mCheckedHelper != null) {
            mCheckedHelper.onSetCheckMarkDrawable();
        }
    }
    
    public void setCompoundDrawables(final Drawable drawable, final Drawable drawable2, final Drawable drawable3, final Drawable drawable4) {
        super.setCompoundDrawables(drawable, drawable2, drawable3, drawable4);
        final AppCompatTextHelper mTextHelper = this.mTextHelper;
        if (mTextHelper != null) {
            mTextHelper.onSetCompoundDrawables();
        }
    }
    
    public void setCompoundDrawablesRelative(final Drawable drawable, final Drawable drawable2, final Drawable drawable3, final Drawable drawable4) {
        super.setCompoundDrawablesRelative(drawable, drawable2, drawable3, drawable4);
        final AppCompatTextHelper mTextHelper = this.mTextHelper;
        if (mTextHelper != null) {
            mTextHelper.onSetCompoundDrawables();
        }
    }
    
    public void setCustomSelectionActionModeCallback(final ActionMode$Callback actionMode$Callback) {
        super.setCustomSelectionActionModeCallback(TextViewCompat.wrapCustomSelectionActionModeCallback((TextView)this, actionMode$Callback));
    }
    
    public void setEmojiCompatEnabled(final boolean enabled) {
        this.getEmojiTextViewHelper().setEnabled(enabled);
    }
    
    public void setSupportBackgroundTintList(final ColorStateList supportBackgroundTintList) {
        final AppCompatBackgroundHelper mBackgroundTintHelper = this.mBackgroundTintHelper;
        if (mBackgroundTintHelper != null) {
            mBackgroundTintHelper.setSupportBackgroundTintList(supportBackgroundTintList);
        }
    }
    
    public void setSupportBackgroundTintMode(final PorterDuff$Mode supportBackgroundTintMode) {
        final AppCompatBackgroundHelper mBackgroundTintHelper = this.mBackgroundTintHelper;
        if (mBackgroundTintHelper != null) {
            mBackgroundTintHelper.setSupportBackgroundTintMode(supportBackgroundTintMode);
        }
    }
    
    public void setSupportCheckMarkTintList(final ColorStateList supportCheckMarkTintList) {
        final AppCompatCheckedTextViewHelper mCheckedHelper = this.mCheckedHelper;
        if (mCheckedHelper != null) {
            mCheckedHelper.setSupportCheckMarkTintList(supportCheckMarkTintList);
        }
    }
    
    public void setSupportCheckMarkTintMode(final PorterDuff$Mode supportCheckMarkTintMode) {
        final AppCompatCheckedTextViewHelper mCheckedHelper = this.mCheckedHelper;
        if (mCheckedHelper != null) {
            mCheckedHelper.setSupportCheckMarkTintMode(supportCheckMarkTintMode);
        }
    }
    
    public void setSupportCompoundDrawablesTintList(final ColorStateList compoundDrawableTintList) {
        this.mTextHelper.setCompoundDrawableTintList(compoundDrawableTintList);
        this.mTextHelper.applyCompoundDrawablesTints();
    }
    
    public void setSupportCompoundDrawablesTintMode(final PorterDuff$Mode compoundDrawableTintMode) {
        this.mTextHelper.setCompoundDrawableTintMode(compoundDrawableTintMode);
        this.mTextHelper.applyCompoundDrawablesTints();
    }
    
    public void setTextAppearance(final Context context, final int n) {
        super.setTextAppearance(context, n);
        final AppCompatTextHelper mTextHelper = this.mTextHelper;
        if (mTextHelper != null) {
            mTextHelper.onSetTextAppearance(context, n);
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.appcompat.widget;

import android.widget.ListView;
import androidx.appcompat.view.SupportMenuInflater;
import android.view.MenuInflater;
import android.view.Menu;
import androidx.appcompat.view.menu.ShowableListMenu;
import android.widget.PopupWindow$OnDismissListener;
import android.view.MenuItem;
import androidx.appcompat.R;
import androidx.appcompat.view.menu.MenuPopupHelper;
import androidx.appcompat.view.menu.MenuBuilder;
import android.view.View$OnTouchListener;
import android.content.Context;
import android.view.View;

public class PopupMenu
{
    private final View mAnchor;
    private final Context mContext;
    private View$OnTouchListener mDragListener;
    private final MenuBuilder mMenu;
    OnMenuItemClickListener mMenuItemClickListener;
    OnDismissListener mOnDismissListener;
    final MenuPopupHelper mPopup;
    
    public PopupMenu(final Context context, final View view) {
        this(context, view, 0);
    }
    
    public PopupMenu(final Context context, final View view, final int n) {
        this(context, view, n, R.attr.popupMenuStyle, 0);
    }
    
    public PopupMenu(final Context mContext, final View mAnchor, final int gravity, final int n, final int n2) {
        this.mContext = mContext;
        this.mAnchor = mAnchor;
        final MenuBuilder mMenu = new MenuBuilder(mContext);
        (this.mMenu = mMenu).setCallback((MenuBuilder.Callback)new MenuBuilder.Callback(this) {
            final PopupMenu this$0;
            
            @Override
            public boolean onMenuItemSelected(final MenuBuilder menuBuilder, final MenuItem menuItem) {
                return this.this$0.mMenuItemClickListener != null && this.this$0.mMenuItemClickListener.onMenuItemClick(menuItem);
            }
            
            @Override
            public void onMenuModeChange(final MenuBuilder menuBuilder) {
            }
        });
        final MenuPopupHelper mPopup = new MenuPopupHelper(mContext, mMenu, mAnchor, false, n, n2);
        (this.mPopup = mPopup).setGravity(gravity);
        mPopup.setOnDismissListener((PopupWindow$OnDismissListener)new PopupWindow$OnDismissListener(this) {
            final PopupMenu this$0;
            
            public void onDismiss() {
                if (this.this$0.mOnDismissListener != null) {
                    this.this$0.mOnDismissListener.onDismiss(this.this$0);
                }
            }
        });
    }
    
    public void dismiss() {
        this.mPopup.dismiss();
    }
    
    public View$OnTouchListener getDragToOpenListener() {
        if (this.mDragListener == null) {
            this.mDragListener = (View$OnTouchListener)new ForwardingListener(this, this.mAnchor) {
                final PopupMenu this$0;
                
                @Override
                public ShowableListMenu getPopup() {
                    return this.this$0.mPopup.getPopup();
                }
                
                @Override
                protected boolean onForwardingStarted() {
                    this.this$0.show();
                    return true;
                }
                
                @Override
                protected boolean onForwardingStopped() {
                    this.this$0.dismiss();
                    return true;
                }
            };
        }
        return this.mDragListener;
    }
    
    public int getGravity() {
        return this.mPopup.getGravity();
    }
    
    public Menu getMenu() {
        return (Menu)this.mMenu;
    }
    
    public MenuInflater getMenuInflater() {
        return new SupportMenuInflater(this.mContext);
    }
    
    ListView getMenuListView() {
        if (!this.mPopup.isShowing()) {
            return null;
        }
        return this.mPopup.getListView();
    }
    
    public void inflate(final int n) {
        this.getMenuInflater().inflate(n, (Menu)this.mMenu);
    }
    
    public void setForceShowIcon(final boolean forceShowIcon) {
        this.mPopup.setForceShowIcon(forceShowIcon);
    }
    
    public void setGravity(final int gravity) {
        this.mPopup.setGravity(gravity);
    }
    
    public void setOnDismissListener(final OnDismissListener mOnDismissListener) {
        this.mOnDismissListener = mOnDismissListener;
    }
    
    public void setOnMenuItemClickListener(final OnMenuItemClickListener mMenuItemClickListener) {
        this.mMenuItemClickListener = mMenuItemClickListener;
    }
    
    public void show() {
        this.mPopup.show();
    }
    
    public interface OnDismissListener
    {
        void onDismiss(final PopupMenu p0);
    }
    
    public interface OnMenuItemClickListener
    {
        boolean onMenuItemClick(final MenuItem p0);
    }
}

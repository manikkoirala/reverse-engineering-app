// 
// Decompiled by Procyon v0.6.0
// 

package androidx.appcompat.content.res;

import androidx.appcompat.widget.ResourceManagerInternal;
import android.graphics.drawable.Drawable;
import androidx.core.content.ContextCompat;
import android.content.res.ColorStateList;
import android.content.Context;

public final class AppCompatResources
{
    private AppCompatResources() {
    }
    
    public static ColorStateList getColorStateList(final Context context, final int n) {
        return ContextCompat.getColorStateList(context, n);
    }
    
    public static Drawable getDrawable(final Context context, final int n) {
        return ResourceManagerInternal.get().getDrawable(context, n);
    }
}

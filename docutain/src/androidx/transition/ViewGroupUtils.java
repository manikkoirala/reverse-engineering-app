// 
// Decompiled by Procyon v0.6.0
// 

package androidx.transition;

import android.os.Build$VERSION;
import android.view.ViewGroup;
import java.lang.reflect.Method;

class ViewGroupUtils
{
    private static Method sGetChildDrawingOrderMethod;
    private static boolean sGetChildDrawingOrderMethodFetched = false;
    private static boolean sTryHiddenSuppressLayout = true;
    
    private ViewGroupUtils() {
    }
    
    static int getChildDrawingOrder(final ViewGroup p0, final int p1) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     3: bipush          29
        //     5: if_icmplt       14
        //     8: aload_0        
        //     9: iload_1        
        //    10: invokevirtual   android/view/ViewGroup.getChildDrawingOrder:(I)I
        //    13: ireturn        
        //    14: getstatic       androidx/transition/ViewGroupUtils.sGetChildDrawingOrderMethodFetched:Z
        //    17: ifne            57
        //    20: ldc             Landroid/view/ViewGroup;.class
        //    22: ldc             "getChildDrawingOrder"
        //    24: iconst_2       
        //    25: anewarray       Ljava/lang/Class;
        //    28: dup            
        //    29: iconst_0       
        //    30: getstatic       java/lang/Integer.TYPE:Ljava/lang/Class;
        //    33: aastore        
        //    34: dup            
        //    35: iconst_1       
        //    36: getstatic       java/lang/Integer.TYPE:Ljava/lang/Class;
        //    39: aastore        
        //    40: invokevirtual   java/lang/Class.getDeclaredMethod:(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
        //    43: astore_3       
        //    44: aload_3        
        //    45: putstatic       androidx/transition/ViewGroupUtils.sGetChildDrawingOrderMethod:Ljava/lang/reflect/Method;
        //    48: aload_3        
        //    49: iconst_1       
        //    50: invokevirtual   java/lang/reflect/Method.setAccessible:(Z)V
        //    53: iconst_1       
        //    54: putstatic       androidx/transition/ViewGroupUtils.sGetChildDrawingOrderMethodFetched:Z
        //    57: getstatic       androidx/transition/ViewGroupUtils.sGetChildDrawingOrderMethod:Ljava/lang/reflect/Method;
        //    60: astore_3       
        //    61: aload_3        
        //    62: ifnull          100
        //    65: aload_3        
        //    66: aload_0        
        //    67: iconst_2       
        //    68: anewarray       Ljava/lang/Object;
        //    71: dup            
        //    72: iconst_0       
        //    73: aload_0        
        //    74: invokevirtual   android/view/ViewGroup.getChildCount:()I
        //    77: invokestatic    java/lang/Integer.valueOf:(I)Ljava/lang/Integer;
        //    80: aastore        
        //    81: dup            
        //    82: iconst_1       
        //    83: iload_1        
        //    84: invokestatic    java/lang/Integer.valueOf:(I)Ljava/lang/Integer;
        //    87: aastore        
        //    88: invokevirtual   java/lang/reflect/Method.invoke:(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
        //    91: checkcast       Ljava/lang/Integer;
        //    94: invokevirtual   java/lang/Integer.intValue:()I
        //    97: istore_2       
        //    98: iload_2        
        //    99: ireturn        
        //   100: iload_1        
        //   101: ireturn        
        //   102: astore_3       
        //   103: goto            53
        //   106: astore_0       
        //   107: goto            100
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                                         
        //  -----  -----  -----  -----  ---------------------------------------------
        //  20     53     102    106    Ljava/lang/NoSuchMethodException;
        //  65     98     106    110    Ljava/lang/IllegalAccessException;
        //  65     98     106    110    Ljava/lang/reflect/InvocationTargetException;
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0100:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2604)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:206)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    static ViewGroupOverlayImpl getOverlay(final ViewGroup viewGroup) {
        if (Build$VERSION.SDK_INT >= 18) {
            return new ViewGroupOverlayApi18(viewGroup);
        }
        return ViewGroupOverlayApi14.createFrom(viewGroup);
    }
    
    private static void hiddenSuppressLayout(final ViewGroup viewGroup, final boolean b) {
        if (ViewGroupUtils.sTryHiddenSuppressLayout) {
            try {
                viewGroup.suppressLayout(b);
            }
            catch (final NoSuchMethodError noSuchMethodError) {
                ViewGroupUtils.sTryHiddenSuppressLayout = false;
            }
        }
    }
    
    static void suppressLayout(final ViewGroup viewGroup, final boolean b) {
        if (Build$VERSION.SDK_INT >= 29) {
            viewGroup.suppressLayout(b);
        }
        else if (Build$VERSION.SDK_INT >= 18) {
            hiddenSuppressLayout(viewGroup, b);
        }
        else {
            ViewGroupUtilsApi14.suppressLayout(viewGroup, b);
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.transition;

public class TransitionListenerAdapter implements TransitionListener
{
    @Override
    public void onTransitionCancel(final Transition transition) {
    }
    
    @Override
    public void onTransitionEnd(final Transition transition) {
    }
    
    @Override
    public void onTransitionPause(final Transition transition) {
    }
    
    @Override
    public void onTransitionResume(final Transition transition) {
    }
    
    @Override
    public void onTransitionStart(final Transition transition) {
    }
}

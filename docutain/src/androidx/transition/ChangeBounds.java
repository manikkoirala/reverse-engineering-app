// 
// Decompiled by Procyon v0.6.0
// 

package androidx.transition;

import java.util.Map;
import android.animation.PropertyValuesHolder;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.Canvas;
import android.graphics.Bitmap;
import android.graphics.Bitmap$Config;
import android.animation.TypeEvaluator;
import android.animation.ObjectAnimator;
import android.animation.Animator$AnimatorListener;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.Animator;
import android.view.ViewGroup;
import androidx.core.view.ViewCompat;
import android.content.res.TypedArray;
import org.xmlpull.v1.XmlPullParser;
import androidx.core.content.res.TypedArrayUtils;
import android.content.res.XmlResourceParser;
import android.util.AttributeSet;
import android.content.Context;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.graphics.PointF;
import android.view.View;
import android.util.Property;

public class ChangeBounds extends Transition
{
    private static final Property<View, PointF> BOTTOM_RIGHT_ONLY_PROPERTY;
    private static final Property<ViewBounds, PointF> BOTTOM_RIGHT_PROPERTY;
    private static final Property<Drawable, PointF> DRAWABLE_ORIGIN_PROPERTY;
    private static final Property<View, PointF> POSITION_PROPERTY;
    private static final String PROPNAME_BOUNDS = "android:changeBounds:bounds";
    private static final String PROPNAME_CLIP = "android:changeBounds:clip";
    private static final String PROPNAME_PARENT = "android:changeBounds:parent";
    private static final String PROPNAME_WINDOW_X = "android:changeBounds:windowX";
    private static final String PROPNAME_WINDOW_Y = "android:changeBounds:windowY";
    private static final Property<View, PointF> TOP_LEFT_ONLY_PROPERTY;
    private static final Property<ViewBounds, PointF> TOP_LEFT_PROPERTY;
    private static RectEvaluator sRectEvaluator;
    private static final String[] sTransitionProperties;
    private boolean mReparent;
    private boolean mResizeClip;
    private int[] mTempLocation;
    
    static {
        sTransitionProperties = new String[] { "android:changeBounds:bounds", "android:changeBounds:clip", "android:changeBounds:parent", "android:changeBounds:windowX", "android:changeBounds:windowY" };
        DRAWABLE_ORIGIN_PROPERTY = new Property<Drawable, PointF>("boundsOrigin") {
            private Rect mBounds = new Rect();
            
            public PointF get(final Drawable drawable) {
                drawable.copyBounds(this.mBounds);
                return new PointF((float)this.mBounds.left, (float)this.mBounds.top);
            }
            
            public void set(final Drawable drawable, final PointF pointF) {
                drawable.copyBounds(this.mBounds);
                this.mBounds.offsetTo(Math.round(pointF.x), Math.round(pointF.y));
                drawable.setBounds(this.mBounds);
            }
        };
        TOP_LEFT_PROPERTY = new Property<ViewBounds, PointF>("topLeft") {
            public PointF get(final ViewBounds viewBounds) {
                return null;
            }
            
            public void set(final ViewBounds viewBounds, final PointF topLeft) {
                viewBounds.setTopLeft(topLeft);
            }
        };
        BOTTOM_RIGHT_PROPERTY = new Property<ViewBounds, PointF>("bottomRight") {
            public PointF get(final ViewBounds viewBounds) {
                return null;
            }
            
            public void set(final ViewBounds viewBounds, final PointF bottomRight) {
                viewBounds.setBottomRight(bottomRight);
            }
        };
        BOTTOM_RIGHT_ONLY_PROPERTY = new Property<View, PointF>("bottomRight") {
            public PointF get(final View view) {
                return null;
            }
            
            public void set(final View view, final PointF pointF) {
                ViewUtils.setLeftTopRightBottom(view, view.getLeft(), view.getTop(), Math.round(pointF.x), Math.round(pointF.y));
            }
        };
        TOP_LEFT_ONLY_PROPERTY = new Property<View, PointF>("topLeft") {
            public PointF get(final View view) {
                return null;
            }
            
            public void set(final View view, final PointF pointF) {
                ViewUtils.setLeftTopRightBottom(view, Math.round(pointF.x), Math.round(pointF.y), view.getRight(), view.getBottom());
            }
        };
        POSITION_PROPERTY = new Property<View, PointF>("position") {
            public PointF get(final View view) {
                return null;
            }
            
            public void set(final View view, final PointF pointF) {
                final int round = Math.round(pointF.x);
                final int round2 = Math.round(pointF.y);
                ViewUtils.setLeftTopRightBottom(view, round, round2, view.getWidth() + round, view.getHeight() + round2);
            }
        };
        ChangeBounds.sRectEvaluator = new RectEvaluator();
    }
    
    public ChangeBounds() {
        this.mTempLocation = new int[2];
        this.mResizeClip = false;
        this.mReparent = false;
    }
    
    public ChangeBounds(final Context context, final AttributeSet set) {
        super(context, set);
        this.mTempLocation = new int[2];
        this.mResizeClip = false;
        this.mReparent = false;
        final TypedArray obtainStyledAttributes = context.obtainStyledAttributes(set, Styleable.CHANGE_BOUNDS);
        final boolean namedBoolean = TypedArrayUtils.getNamedBoolean(obtainStyledAttributes, (XmlPullParser)set, "resizeClip", 0, false);
        obtainStyledAttributes.recycle();
        this.setResizeClip(namedBoolean);
    }
    
    private void captureValues(final TransitionValues transitionValues) {
        final View view = transitionValues.view;
        if (ViewCompat.isLaidOut(view) || view.getWidth() != 0 || view.getHeight() != 0) {
            transitionValues.values.put("android:changeBounds:bounds", new Rect(view.getLeft(), view.getTop(), view.getRight(), view.getBottom()));
            transitionValues.values.put("android:changeBounds:parent", transitionValues.view.getParent());
            if (this.mReparent) {
                transitionValues.view.getLocationInWindow(this.mTempLocation);
                transitionValues.values.put("android:changeBounds:windowX", this.mTempLocation[0]);
                transitionValues.values.put("android:changeBounds:windowY", this.mTempLocation[1]);
            }
            if (this.mResizeClip) {
                transitionValues.values.put("android:changeBounds:clip", ViewCompat.getClipBounds(view));
            }
        }
    }
    
    private boolean parentMatches(final View view, final View view2) {
        final boolean mReparent = this.mReparent;
        boolean b = true;
        if (mReparent) {
            final TransitionValues matchedTransitionValues = this.getMatchedTransitionValues(view, true);
            b = (((matchedTransitionValues != null) ? (view2 == matchedTransitionValues.view) : (view == view2)) && b);
        }
        return b;
    }
    
    @Override
    public void captureEndValues(final TransitionValues transitionValues) {
        this.captureValues(transitionValues);
    }
    
    @Override
    public void captureStartValues(final TransitionValues transitionValues) {
        this.captureValues(transitionValues);
    }
    
    @Override
    public Animator createAnimator(final ViewGroup viewGroup, final TransitionValues transitionValues, final TransitionValues transitionValues2) {
        if (transitionValues == null || transitionValues2 == null) {
            return null;
        }
        final Map<String, Object> values = transitionValues.values;
        final Map<String, Object> values2 = transitionValues2.values;
        final ViewGroup viewGroup2 = values.get("android:changeBounds:parent");
        final ViewGroup viewGroup3 = values2.get("android:changeBounds:parent");
        if (viewGroup2 != null && viewGroup3 != null) {
            final View view = transitionValues2.view;
            if (this.parentMatches((View)viewGroup2, (View)viewGroup3)) {
                final Rect rect = transitionValues.values.get("android:changeBounds:bounds");
                final Rect rect2 = transitionValues2.values.get("android:changeBounds:bounds");
                final int left = rect.left;
                final int left2 = rect2.left;
                final int top = rect.top;
                final int top2 = rect2.top;
                final int right = rect.right;
                final int right2 = rect2.right;
                final int bottom = rect.bottom;
                final int bottom2 = rect2.bottom;
                final int a = right - left;
                final int a2 = bottom - top;
                final int b = right2 - left2;
                final int b2 = bottom2 - top2;
                Rect rect3 = transitionValues.values.get("android:changeBounds:clip");
                final Rect rect4 = transitionValues2.values.get("android:changeBounds:clip");
                int n2 = 0;
                Label_0299: {
                    if ((a != 0 && a2 != 0) || (b != 0 && b2 != 0)) {
                        int n;
                        if (left == left2 && top == top2) {
                            n = 0;
                        }
                        else {
                            n = 1;
                        }
                        if (right == right2) {
                            n2 = n;
                            if (bottom == bottom2) {
                                break Label_0299;
                            }
                        }
                        n2 = n + 1;
                    }
                    else {
                        n2 = 0;
                    }
                }
                int n3 = 0;
                Label_0335: {
                    if (rect3 == null || rect3.equals((Object)rect4)) {
                        n3 = n2;
                        if (rect3 != null) {
                            break Label_0335;
                        }
                        n3 = n2;
                        if (rect4 == null) {
                            break Label_0335;
                        }
                    }
                    n3 = n2 + 1;
                }
                if (n3 > 0) {
                    Object o;
                    if (!this.mResizeClip) {
                        ViewUtils.setLeftTopRightBottom(view, left, top, right, bottom);
                        if (n3 == 2) {
                            if (a == b && a2 == b2) {
                                o = ObjectAnimatorUtils.ofPointF(view, ChangeBounds.POSITION_PROPERTY, this.getPathMotion().getPath((float)left, (float)top, (float)left2, (float)top2));
                            }
                            else {
                                final ViewBounds viewBounds = new ViewBounds(view);
                                final ObjectAnimator ofPointF = ObjectAnimatorUtils.ofPointF(viewBounds, ChangeBounds.TOP_LEFT_PROPERTY, this.getPathMotion().getPath((float)left, (float)top, (float)left2, (float)top2));
                                final ObjectAnimator ofPointF2 = ObjectAnimatorUtils.ofPointF(viewBounds, ChangeBounds.BOTTOM_RIGHT_PROPERTY, this.getPathMotion().getPath((float)right, (float)bottom, (float)right2, (float)bottom2));
                                o = new AnimatorSet();
                                ((AnimatorSet)o).playTogether(new Animator[] { (Animator)ofPointF, (Animator)ofPointF2 });
                                ((AnimatorSet)o).addListener((Animator$AnimatorListener)new AnimatorListenerAdapter(this, viewBounds) {
                                    private ViewBounds mViewBounds = viewBounds;
                                    final ChangeBounds this$0;
                                    final ViewBounds val$viewBounds;
                                });
                            }
                        }
                        else if (left == left2 && top == top2) {
                            o = ObjectAnimatorUtils.ofPointF(view, ChangeBounds.BOTTOM_RIGHT_ONLY_PROPERTY, this.getPathMotion().getPath((float)right, (float)bottom, (float)right2, (float)bottom2));
                        }
                        else {
                            o = ObjectAnimatorUtils.ofPointF(view, ChangeBounds.TOP_LEFT_ONLY_PROPERTY, this.getPathMotion().getPath((float)left, (float)top, (float)left2, (float)top2));
                        }
                    }
                    else {
                        ViewUtils.setLeftTopRightBottom(view, left, top, Math.max(a, b) + left, Math.max(a2, b2) + top);
                        Object ofPointF3;
                        if (left == left2 && top == top2) {
                            ofPointF3 = null;
                        }
                        else {
                            ofPointF3 = ObjectAnimatorUtils.ofPointF(view, ChangeBounds.POSITION_PROPERTY, this.getPathMotion().getPath((float)left, (float)top, (float)left2, (float)top2));
                        }
                        if (rect3 == null) {
                            rect3 = new Rect(0, 0, a, a2);
                        }
                        Rect rect5;
                        if (rect4 == null) {
                            rect5 = new Rect(0, 0, b, b2);
                        }
                        else {
                            rect5 = rect4;
                        }
                        ObjectAnimator ofObject;
                        if (!rect3.equals((Object)rect5)) {
                            ViewCompat.setClipBounds(view, rect3);
                            ofObject = ObjectAnimator.ofObject((Object)view, "clipBounds", (TypeEvaluator)ChangeBounds.sRectEvaluator, new Object[] { rect3, rect5 });
                            ofObject.addListener((Animator$AnimatorListener)new AnimatorListenerAdapter(this, view, rect4, left2, top2, right2, bottom2) {
                                private boolean mIsCanceled;
                                final ChangeBounds this$0;
                                final int val$endBottom;
                                final int val$endLeft;
                                final int val$endRight;
                                final int val$endTop;
                                final Rect val$finalClip;
                                final View val$view;
                                
                                public void onAnimationCancel(final Animator animator) {
                                    this.mIsCanceled = true;
                                }
                                
                                public void onAnimationEnd(final Animator animator) {
                                    if (!this.mIsCanceled) {
                                        ViewCompat.setClipBounds(this.val$view, this.val$finalClip);
                                        ViewUtils.setLeftTopRightBottom(this.val$view, this.val$endLeft, this.val$endTop, this.val$endRight, this.val$endBottom);
                                    }
                                }
                            });
                        }
                        else {
                            ofObject = null;
                        }
                        o = TransitionUtils.mergeAnimators((Animator)ofPointF3, (Animator)ofObject);
                    }
                    if (view.getParent() instanceof ViewGroup) {
                        final ViewGroup viewGroup4 = (ViewGroup)view.getParent();
                        ViewGroupUtils.suppressLayout(viewGroup4, true);
                        this.addListener((TransitionListener)new TransitionListenerAdapter(this, viewGroup4) {
                            boolean mCanceled = false;
                            final ChangeBounds this$0;
                            final ViewGroup val$parent;
                            
                            @Override
                            public void onTransitionCancel(final Transition transition) {
                                ViewGroupUtils.suppressLayout(this.val$parent, false);
                                this.mCanceled = true;
                            }
                            
                            @Override
                            public void onTransitionEnd(final Transition transition) {
                                if (!this.mCanceled) {
                                    ViewGroupUtils.suppressLayout(this.val$parent, false);
                                }
                                transition.removeListener((TransitionListener)this);
                            }
                            
                            @Override
                            public void onTransitionPause(final Transition transition) {
                                ViewGroupUtils.suppressLayout(this.val$parent, false);
                            }
                            
                            @Override
                            public void onTransitionResume(final Transition transition) {
                                ViewGroupUtils.suppressLayout(this.val$parent, true);
                            }
                        });
                    }
                    return (Animator)o;
                }
            }
            else {
                final int intValue = transitionValues.values.get("android:changeBounds:windowX");
                final int intValue2 = transitionValues.values.get("android:changeBounds:windowY");
                final int intValue3 = transitionValues2.values.get("android:changeBounds:windowX");
                final int intValue4 = transitionValues2.values.get("android:changeBounds:windowY");
                if (intValue != intValue3 || intValue2 != intValue4) {
                    viewGroup.getLocationInWindow(this.mTempLocation);
                    final Bitmap bitmap = Bitmap.createBitmap(view.getWidth(), view.getHeight(), Bitmap$Config.ARGB_8888);
                    view.draw(new Canvas(bitmap));
                    final BitmapDrawable bitmapDrawable = new BitmapDrawable(bitmap);
                    final float transitionAlpha = ViewUtils.getTransitionAlpha(view);
                    ViewUtils.setTransitionAlpha(view, 0.0f);
                    ViewUtils.getOverlay((View)viewGroup).add((Drawable)bitmapDrawable);
                    final PathMotion pathMotion = this.getPathMotion();
                    final int[] mTempLocation = this.mTempLocation;
                    final int n4 = mTempLocation[0];
                    final float n5 = (float)(intValue - n4);
                    final int n6 = mTempLocation[1];
                    final ObjectAnimator ofPropertyValuesHolder = ObjectAnimator.ofPropertyValuesHolder((Object)bitmapDrawable, new PropertyValuesHolder[] { PropertyValuesHolderUtils.ofPointF(ChangeBounds.DRAWABLE_ORIGIN_PROPERTY, pathMotion.getPath(n5, (float)(intValue2 - n6), (float)(intValue3 - n4), (float)(intValue4 - n6))) });
                    ofPropertyValuesHolder.addListener((Animator$AnimatorListener)new AnimatorListenerAdapter(this, viewGroup, bitmapDrawable, view, transitionAlpha) {
                        final ChangeBounds this$0;
                        final BitmapDrawable val$drawable;
                        final ViewGroup val$sceneRoot;
                        final float val$transitionAlpha;
                        final View val$view;
                        
                        public void onAnimationEnd(final Animator animator) {
                            ViewUtils.getOverlay((View)this.val$sceneRoot).remove((Drawable)this.val$drawable);
                            ViewUtils.setTransitionAlpha(this.val$view, this.val$transitionAlpha);
                        }
                    });
                    return (Animator)ofPropertyValuesHolder;
                }
            }
            return null;
        }
        return null;
    }
    
    public boolean getResizeClip() {
        return this.mResizeClip;
    }
    
    @Override
    public String[] getTransitionProperties() {
        return ChangeBounds.sTransitionProperties;
    }
    
    public void setResizeClip(final boolean mResizeClip) {
        this.mResizeClip = mResizeClip;
    }
    
    private static class ViewBounds
    {
        private int mBottom;
        private int mBottomRightCalls;
        private int mLeft;
        private int mRight;
        private int mTop;
        private int mTopLeftCalls;
        private View mView;
        
        ViewBounds(final View mView) {
            this.mView = mView;
        }
        
        private void setLeftTopRightBottom() {
            ViewUtils.setLeftTopRightBottom(this.mView, this.mLeft, this.mTop, this.mRight, this.mBottom);
            this.mTopLeftCalls = 0;
            this.mBottomRightCalls = 0;
        }
        
        void setBottomRight(final PointF pointF) {
            this.mRight = Math.round(pointF.x);
            this.mBottom = Math.round(pointF.y);
            final int mBottomRightCalls = this.mBottomRightCalls + 1;
            this.mBottomRightCalls = mBottomRightCalls;
            if (this.mTopLeftCalls == mBottomRightCalls) {
                this.setLeftTopRightBottom();
            }
        }
        
        void setTopLeft(final PointF pointF) {
            this.mLeft = Math.round(pointF.x);
            this.mTop = Math.round(pointF.y);
            final int mTopLeftCalls = this.mTopLeftCalls + 1;
            this.mTopLeftCalls = mTopLeftCalls;
            if (mTopLeftCalls == this.mBottomRightCalls) {
                this.setLeftTopRightBottom();
            }
        }
    }
}

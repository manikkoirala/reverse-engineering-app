// 
// Decompiled by Procyon v0.6.0
// 

package androidx.transition;

import android.view.ViewParent;
import java.lang.reflect.InvocationTargetException;
import android.graphics.Matrix;
import android.util.Log;
import android.view.View;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

class ViewUtilsBase
{
    private static final String TAG = "ViewUtilsBase";
    private static final int VISIBILITY_MASK = 12;
    private static boolean sSetFrameFetched;
    private static Method sSetFrameMethod;
    private static Field sViewFlagsField;
    private static boolean sViewFlagsFieldFetched;
    private float[] mMatrixValues;
    
    private void fetchSetFrame() {
        if (!ViewUtilsBase.sSetFrameFetched) {
            try {
                (ViewUtilsBase.sSetFrameMethod = View.class.getDeclaredMethod("setFrame", Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE)).setAccessible(true);
            }
            catch (final NoSuchMethodException ex) {
                Log.i("ViewUtilsBase", "Failed to retrieve setFrame method", (Throwable)ex);
            }
            ViewUtilsBase.sSetFrameFetched = true;
        }
    }
    
    public void clearNonTransitionAlpha(final View view) {
        if (view.getVisibility() == 0) {
            view.setTag(R.id.save_non_transition_alpha, (Object)null);
        }
    }
    
    public float getTransitionAlpha(final View view) {
        final Float n = (Float)view.getTag(R.id.save_non_transition_alpha);
        if (n != null) {
            return view.getAlpha() / n;
        }
        return view.getAlpha();
    }
    
    public void saveNonTransitionAlpha(final View view) {
        if (view.getTag(R.id.save_non_transition_alpha) == null) {
            view.setTag(R.id.save_non_transition_alpha, (Object)view.getAlpha());
        }
    }
    
    public void setAnimationMatrix(final View view, final Matrix matrix) {
        if (matrix != null && !matrix.isIdentity()) {
            float[] mMatrixValues;
            if ((mMatrixValues = this.mMatrixValues) == null) {
                mMatrixValues = new float[9];
                this.mMatrixValues = mMatrixValues;
            }
            matrix.getValues(mMatrixValues);
            final float n = mMatrixValues[3];
            final float n2 = (float)Math.sqrt(1.0f - n * n);
            int n3;
            if (mMatrixValues[0] < 0.0f) {
                n3 = -1;
            }
            else {
                n3 = 1;
            }
            final float n4 = n2 * n3;
            final float rotation = (float)Math.toDegrees(Math.atan2(n, n4));
            final float scaleX = mMatrixValues[0] / n4;
            final float scaleY = mMatrixValues[4] / n4;
            final float translationX = mMatrixValues[2];
            final float translationY = mMatrixValues[5];
            view.setPivotX(0.0f);
            view.setPivotY(0.0f);
            view.setTranslationX(translationX);
            view.setTranslationY(translationY);
            view.setRotation(rotation);
            view.setScaleX(scaleX);
            view.setScaleY(scaleY);
        }
        else {
            view.setPivotX((float)(view.getWidth() / 2));
            view.setPivotY((float)(view.getHeight() / 2));
            view.setTranslationX(0.0f);
            view.setTranslationY(0.0f);
            view.setScaleX(1.0f);
            view.setScaleY(1.0f);
            view.setRotation(0.0f);
        }
    }
    
    public void setLeftTopRightBottom(final View obj, final int i, final int j, final int k, final int l) {
        this.fetchSetFrame();
        final Method sSetFrameMethod = ViewUtilsBase.sSetFrameMethod;
        if (sSetFrameMethod == null) {
            goto Label_0071;
        }
        try {
            sSetFrameMethod.invoke(obj, i, j, k, l);
            goto Label_0071;
        }
        catch (final InvocationTargetException ex) {
            throw new RuntimeException(ex.getCause());
        }
        catch (final IllegalAccessException ex2) {
            goto Label_0071;
        }
    }
    
    public void setTransitionAlpha(final View view, final float alpha) {
        final Float n = (Float)view.getTag(R.id.save_non_transition_alpha);
        if (n != null) {
            view.setAlpha(n * alpha);
        }
        else {
            view.setAlpha(alpha);
        }
    }
    
    public void setTransitionVisibility(final View view, final int n) {
        if (!ViewUtilsBase.sViewFlagsFieldFetched) {
            try {
                (ViewUtilsBase.sViewFlagsField = View.class.getDeclaredField("mViewFlags")).setAccessible(true);
            }
            catch (final NoSuchFieldException ex) {
                Log.i("ViewUtilsBase", "fetchViewFlagsField: ");
            }
            ViewUtilsBase.sViewFlagsFieldFetched = true;
        }
        final Field sViewFlagsField = ViewUtilsBase.sViewFlagsField;
        if (sViewFlagsField == null) {
            return;
        }
        try {
            ViewUtilsBase.sViewFlagsField.setInt(view, n | (sViewFlagsField.getInt(view) & 0xFFFFFFF3));
        }
        catch (final IllegalAccessException ex2) {}
    }
    
    public void transformMatrixToGlobal(final View view, final Matrix matrix) {
        final ViewParent parent = view.getParent();
        if (parent instanceof View) {
            final View view2 = (View)parent;
            this.transformMatrixToGlobal(view2, matrix);
            matrix.preTranslate((float)(-view2.getScrollX()), (float)(-view2.getScrollY()));
        }
        matrix.preTranslate((float)view.getLeft(), (float)view.getTop());
        final Matrix matrix2 = view.getMatrix();
        if (!matrix2.isIdentity()) {
            matrix.preConcat(matrix2);
        }
    }
    
    public void transformMatrixToLocal(final View view, final Matrix matrix) {
        final ViewParent parent = view.getParent();
        if (parent instanceof View) {
            final View view2 = (View)parent;
            this.transformMatrixToLocal(view2, matrix);
            matrix.postTranslate((float)view2.getScrollX(), (float)view2.getScrollY());
        }
        matrix.postTranslate((float)(-view.getLeft()), (float)(-view.getTop()));
        final Matrix matrix2 = view.getMatrix();
        if (!matrix2.isIdentity()) {
            final Matrix matrix3 = new Matrix();
            if (matrix2.invert(matrix3)) {
                matrix.postConcat(matrix3);
            }
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.transition;

import java.lang.reflect.InvocationTargetException;
import android.os.Build$VERSION;
import android.graphics.Canvas;
import java.lang.reflect.Method;

class CanvasUtils
{
    private static Method sInorderBarrierMethod;
    private static boolean sOrderMethodsFetched;
    private static Method sReorderBarrierMethod;
    
    private CanvasUtils() {
    }
    
    static void enableZ(final Canvas obj, final boolean b) {
        if (Build$VERSION.SDK_INT < 21) {
            goto Label_0163;
        }
        if (Build$VERSION.SDK_INT >= 29) {
            if (b) {
                ((Canvas)obj).enableZ();
                goto Label_0163;
            }
            ((Canvas)obj).disableZ();
            goto Label_0163;
        }
        else {
            if (Build$VERSION.SDK_INT == 28) {
                goto Label_0164;
            }
            Label_0097: {
                if (CanvasUtils.sOrderMethodsFetched) {
                    break Label_0097;
                }
                try {
                    (CanvasUtils.sReorderBarrierMethod = Canvas.class.getDeclaredMethod("insertReorderBarrier", (Class<?>[])new Class[0])).setAccessible(true);
                    (CanvasUtils.sInorderBarrierMethod = Canvas.class.getDeclaredMethod("insertInorderBarrier", (Class<?>[])new Class[0])).setAccessible(true);
                    CanvasUtils.sOrderMethodsFetched = true;
                    if (!b) {
                        goto Label_0126;
                    }
                    try {
                        final Method sReorderBarrierMethod = CanvasUtils.sReorderBarrierMethod;
                        if (sReorderBarrierMethod != null) {
                            sReorderBarrierMethod.invoke(obj, new Object[0]);
                            goto Label_0126;
                        }
                        goto Label_0126;
                    }
                    catch (final InvocationTargetException obj) {}
                    catch (final IllegalAccessException obj) {}
                }
                catch (final NoSuchMethodException ex) {}
            }
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.transition;

import androidx.collection.ArrayMap;
import androidx.collection.LongSparseArray;
import android.view.View;
import android.util.SparseArray;

class TransitionValuesMaps
{
    final SparseArray<View> mIdValues;
    final LongSparseArray<View> mItemIdValues;
    final ArrayMap<String, View> mNameValues;
    final ArrayMap<View, TransitionValues> mViewValues;
    
    TransitionValuesMaps() {
        this.mViewValues = new ArrayMap<View, TransitionValues>();
        this.mIdValues = (SparseArray<View>)new SparseArray();
        this.mItemIdValues = new LongSparseArray<View>();
        this.mNameValues = new ArrayMap<String, View>();
    }
}

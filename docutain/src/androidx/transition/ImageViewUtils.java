// 
// Decompiled by Procyon v0.6.0
// 

package androidx.transition;

import android.graphics.drawable.Drawable;
import android.os.Build$VERSION;
import android.graphics.Matrix;
import android.widget.ImageView;
import java.lang.reflect.Field;

class ImageViewUtils
{
    private static Field sDrawMatrixField;
    private static boolean sDrawMatrixFieldFetched = false;
    private static boolean sTryHiddenAnimateTransform = true;
    
    private ImageViewUtils() {
    }
    
    static void animateTransform(final ImageView imageView, final Matrix matrix) {
        if (Build$VERSION.SDK_INT >= 29) {
            imageView.animateTransform(matrix);
        }
        else if (matrix == null) {
            final Drawable drawable = imageView.getDrawable();
            if (drawable != null) {
                drawable.setBounds(0, 0, imageView.getWidth() - imageView.getPaddingLeft() - imageView.getPaddingRight(), imageView.getHeight() - imageView.getPaddingTop() - imageView.getPaddingBottom());
                imageView.invalidate();
            }
        }
        else if (Build$VERSION.SDK_INT >= 21) {
            hiddenAnimateTransform(imageView, matrix);
        }
        else {
            final Drawable drawable2 = imageView.getDrawable();
            if (drawable2 != null) {
                drawable2.setBounds(0, 0, drawable2.getIntrinsicWidth(), drawable2.getIntrinsicHeight());
                Matrix matrix2 = null;
                final Matrix matrix3 = null;
                fetchDrawMatrixField();
                final Field sDrawMatrixField = ImageViewUtils.sDrawMatrixField;
                Label_0175: {
                    if (sDrawMatrixField != null) {
                        matrix2 = matrix3;
                        try {
                            final Matrix matrix4 = (Matrix)sDrawMatrixField.get(imageView);
                            if (matrix4 == null) {
                                try {
                                    final Matrix value = matrix2 = new Matrix();
                                    ImageViewUtils.sDrawMatrixField.set(imageView, value);
                                    matrix2 = value;
                                    break Label_0175;
                                }
                                catch (final IllegalAccessException ex) {}
                            }
                            matrix2 = matrix4;
                        }
                        catch (final IllegalAccessException ex2) {}
                    }
                }
                if (matrix2 != null) {
                    matrix2.set(matrix);
                }
                imageView.invalidate();
            }
        }
    }
    
    private static void fetchDrawMatrixField() {
        if (ImageViewUtils.sDrawMatrixFieldFetched) {
            return;
        }
        while (true) {
            try {
                (ImageViewUtils.sDrawMatrixField = ImageView.class.getDeclaredField("mDrawMatrix")).setAccessible(true);
                ImageViewUtils.sDrawMatrixFieldFetched = true;
            }
            catch (final NoSuchFieldException ex) {
                continue;
            }
            break;
        }
    }
    
    private static void hiddenAnimateTransform(final ImageView imageView, final Matrix matrix) {
        if (ImageViewUtils.sTryHiddenAnimateTransform) {
            try {
                imageView.animateTransform(matrix);
            }
            catch (final NoSuchMethodError noSuchMethodError) {
                ImageViewUtils.sTryHiddenAnimateTransform = false;
            }
        }
    }
}

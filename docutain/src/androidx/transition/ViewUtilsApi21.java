// 
// Decompiled by Procyon v0.6.0
// 

package androidx.transition;

import android.graphics.Matrix;
import android.view.View;

class ViewUtilsApi21 extends ViewUtilsApi19
{
    private static boolean sTryHiddenSetAnimationMatrix = true;
    private static boolean sTryHiddenTransformMatrixToGlobal = true;
    private static boolean sTryHiddenTransformMatrixToLocal = true;
    
    @Override
    public void setAnimationMatrix(final View view, final Matrix animationMatrix) {
        if (ViewUtilsApi21.sTryHiddenSetAnimationMatrix) {
            try {
                view.setAnimationMatrix(animationMatrix);
            }
            catch (final NoSuchMethodError noSuchMethodError) {
                ViewUtilsApi21.sTryHiddenSetAnimationMatrix = false;
            }
        }
    }
    
    @Override
    public void transformMatrixToGlobal(final View view, final Matrix matrix) {
        if (ViewUtilsApi21.sTryHiddenTransformMatrixToGlobal) {
            try {
                view.transformMatrixToGlobal(matrix);
            }
            catch (final NoSuchMethodError noSuchMethodError) {
                ViewUtilsApi21.sTryHiddenTransformMatrixToGlobal = false;
            }
        }
    }
    
    @Override
    public void transformMatrixToLocal(final View view, final Matrix matrix) {
        if (ViewUtilsApi21.sTryHiddenTransformMatrixToLocal) {
            try {
                view.transformMatrixToLocal(matrix);
            }
            catch (final NoSuchMethodError noSuchMethodError) {
                ViewUtilsApi21.sTryHiddenTransformMatrixToLocal = false;
            }
        }
    }
}

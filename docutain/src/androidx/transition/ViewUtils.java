// 
// Decompiled by Procyon v0.6.0
// 

package androidx.transition;

import android.graphics.Matrix;
import androidx.core.view.ViewCompat;
import android.os.Build$VERSION;
import android.graphics.Rect;
import android.view.View;
import android.util.Property;

class ViewUtils
{
    static final Property<View, Rect> CLIP_BOUNDS;
    private static final ViewUtilsBase IMPL;
    private static final String TAG = "ViewUtils";
    static final Property<View, Float> TRANSITION_ALPHA;
    
    static {
        if (Build$VERSION.SDK_INT >= 29) {
            IMPL = new ViewUtilsApi29();
        }
        else if (Build$VERSION.SDK_INT >= 23) {
            IMPL = new ViewUtilsApi23();
        }
        else if (Build$VERSION.SDK_INT >= 22) {
            IMPL = new ViewUtilsApi22();
        }
        else if (Build$VERSION.SDK_INT >= 21) {
            IMPL = new ViewUtilsApi21();
        }
        else if (Build$VERSION.SDK_INT >= 19) {
            IMPL = new ViewUtilsApi19();
        }
        else {
            IMPL = new ViewUtilsBase();
        }
        TRANSITION_ALPHA = new Property<View, Float>("translationAlpha") {
            public Float get(final View view) {
                return ViewUtils.getTransitionAlpha(view);
            }
            
            public void set(final View view, final Float n) {
                ViewUtils.setTransitionAlpha(view, n);
            }
        };
        CLIP_BOUNDS = new Property<View, Rect>("clipBounds") {
            public Rect get(final View view) {
                return ViewCompat.getClipBounds(view);
            }
            
            public void set(final View view, final Rect rect) {
                ViewCompat.setClipBounds(view, rect);
            }
        };
    }
    
    private ViewUtils() {
    }
    
    static void clearNonTransitionAlpha(final View view) {
        ViewUtils.IMPL.clearNonTransitionAlpha(view);
    }
    
    static ViewOverlayImpl getOverlay(final View view) {
        if (Build$VERSION.SDK_INT >= 18) {
            return new ViewOverlayApi18(view);
        }
        return ViewOverlayApi14.createFrom(view);
    }
    
    static float getTransitionAlpha(final View view) {
        return ViewUtils.IMPL.getTransitionAlpha(view);
    }
    
    static WindowIdImpl getWindowId(final View view) {
        if (Build$VERSION.SDK_INT >= 18) {
            return new WindowIdApi18(view);
        }
        return new WindowIdApi14(view.getWindowToken());
    }
    
    static void saveNonTransitionAlpha(final View view) {
        ViewUtils.IMPL.saveNonTransitionAlpha(view);
    }
    
    static void setAnimationMatrix(final View view, final Matrix matrix) {
        ViewUtils.IMPL.setAnimationMatrix(view, matrix);
    }
    
    static void setLeftTopRightBottom(final View view, final int n, final int n2, final int n3, final int n4) {
        ViewUtils.IMPL.setLeftTopRightBottom(view, n, n2, n3, n4);
    }
    
    static void setTransitionAlpha(final View view, final float n) {
        ViewUtils.IMPL.setTransitionAlpha(view, n);
    }
    
    static void setTransitionVisibility(final View view, final int n) {
        ViewUtils.IMPL.setTransitionVisibility(view, n);
    }
    
    static void transformMatrixToGlobal(final View view, final Matrix matrix) {
        ViewUtils.IMPL.transformMatrixToGlobal(view, matrix);
    }
    
    static void transformMatrixToLocal(final View view, final Matrix matrix) {
        ViewUtils.IMPL.transformMatrixToLocal(view, matrix);
    }
}

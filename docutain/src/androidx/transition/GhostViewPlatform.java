// 
// Decompiled by Procyon v0.6.0
// 

package androidx.transition;

import android.util.Log;
import java.lang.reflect.InvocationTargetException;
import android.graphics.Matrix;
import android.view.ViewGroup;
import android.view.View;
import java.lang.reflect.Method;

class GhostViewPlatform implements GhostView
{
    private static final String TAG = "GhostViewApi21";
    private static Method sAddGhostMethod;
    private static boolean sAddGhostMethodFetched;
    private static Class<?> sGhostViewClass;
    private static boolean sGhostViewClassFetched;
    private static Method sRemoveGhostMethod;
    private static boolean sRemoveGhostMethodFetched;
    private final View mGhostView;
    
    private GhostViewPlatform(final View mGhostView) {
        this.mGhostView = mGhostView;
    }
    
    static GhostView addGhost(final View view, final ViewGroup viewGroup, final Matrix matrix) {
        fetchAddGhostMethod();
        final Method sAddGhostMethod = GhostViewPlatform.sAddGhostMethod;
        if (sAddGhostMethod == null) {
            goto Label_0058;
        }
        try {
            return new GhostViewPlatform((View)sAddGhostMethod.invoke(null, view, viewGroup, matrix));
        }
        catch (final InvocationTargetException ex) {
            throw new RuntimeException(ex.getCause());
        }
        catch (final IllegalAccessException ex2) {
            goto Label_0058;
        }
    }
    
    private static void fetchAddGhostMethod() {
        if (!GhostViewPlatform.sAddGhostMethodFetched) {
            try {
                fetchGhostViewClass();
                (GhostViewPlatform.sAddGhostMethod = GhostViewPlatform.sGhostViewClass.getDeclaredMethod("addGhost", View.class, ViewGroup.class, Matrix.class)).setAccessible(true);
            }
            catch (final NoSuchMethodException ex) {
                Log.i("GhostViewApi21", "Failed to retrieve addGhost method", (Throwable)ex);
            }
            GhostViewPlatform.sAddGhostMethodFetched = true;
        }
    }
    
    private static void fetchGhostViewClass() {
        if (!GhostViewPlatform.sGhostViewClassFetched) {
            try {
                GhostViewPlatform.sGhostViewClass = Class.forName("android.view.GhostView");
            }
            catch (final ClassNotFoundException ex) {
                Log.i("GhostViewApi21", "Failed to retrieve GhostView class", (Throwable)ex);
            }
            GhostViewPlatform.sGhostViewClassFetched = true;
        }
    }
    
    private static void fetchRemoveGhostMethod() {
        if (!GhostViewPlatform.sRemoveGhostMethodFetched) {
            try {
                fetchGhostViewClass();
                (GhostViewPlatform.sRemoveGhostMethod = GhostViewPlatform.sGhostViewClass.getDeclaredMethod("removeGhost", View.class)).setAccessible(true);
            }
            catch (final NoSuchMethodException ex) {
                Log.i("GhostViewApi21", "Failed to retrieve removeGhost method", (Throwable)ex);
            }
            GhostViewPlatform.sRemoveGhostMethodFetched = true;
        }
    }
    
    static void removeGhost(final View view) {
        fetchRemoveGhostMethod();
        final Method sRemoveGhostMethod = GhostViewPlatform.sRemoveGhostMethod;
        if (sRemoveGhostMethod == null) {
            goto Label_0041;
        }
        try {
            sRemoveGhostMethod.invoke(null, view);
            goto Label_0041;
        }
        catch (final InvocationTargetException ex) {
            throw new RuntimeException(ex.getCause());
        }
        catch (final IllegalAccessException ex2) {
            goto Label_0041;
        }
    }
    
    @Override
    public void reserveEndViewTransition(final ViewGroup viewGroup, final View view) {
    }
    
    @Override
    public void setVisibility(final int visibility) {
        this.mGhostView.setVisibility(visibility);
    }
}

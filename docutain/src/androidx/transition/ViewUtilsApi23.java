// 
// Decompiled by Procyon v0.6.0
// 

package androidx.transition;

import android.os.Build$VERSION;
import android.view.View;

class ViewUtilsApi23 extends ViewUtilsApi22
{
    private static boolean sTryHiddenSetTransitionVisibility = true;
    
    @Override
    public void setTransitionVisibility(final View view, final int transitionVisibility) {
        if (Build$VERSION.SDK_INT == 28) {
            super.setTransitionVisibility(view, transitionVisibility);
        }
        else if (ViewUtilsApi23.sTryHiddenSetTransitionVisibility) {
            try {
                view.setTransitionVisibility(transitionVisibility);
            }
            catch (final NoSuchMethodError noSuchMethodError) {
                ViewUtilsApi23.sTryHiddenSetTransitionVisibility = false;
            }
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.transition;

import android.animation.AnimatorListenerAdapter;
import android.animation.Animator$AnimatorListener;
import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.animation.Animator;
import android.animation.TimeInterpolator;
import android.view.View;

class TranslationAnimationCreator
{
    private TranslationAnimationCreator() {
    }
    
    static Animator createAnimation(final View view, final TransitionValues transitionValues, final int n, final int n2, float translationY, final float n3, final float n4, final float n5, final TimeInterpolator interpolator, final Transition transition) {
        final float translationX = view.getTranslationX();
        final float translationY2 = view.getTranslationY();
        final int[] array = (int[])transitionValues.view.getTag(R.id.transition_position);
        float translationX2;
        if (array != null) {
            translationX2 = array[0] - n + translationX;
            translationY = array[1] - n2 + translationY2;
        }
        else {
            translationX2 = translationY;
            translationY = n3;
        }
        final int round = Math.round(translationX2 - translationX);
        final int round2 = Math.round(translationY - translationY2);
        view.setTranslationX(translationX2);
        view.setTranslationY(translationY);
        if (translationX2 == n4 && translationY == n5) {
            return null;
        }
        final ObjectAnimator ofPropertyValuesHolder = ObjectAnimator.ofPropertyValuesHolder((Object)view, new PropertyValuesHolder[] { PropertyValuesHolder.ofFloat(View.TRANSLATION_X, new float[] { translationX2, n4 }), PropertyValuesHolder.ofFloat(View.TRANSLATION_Y, new float[] { translationY, n5 }) });
        final TransitionPositionListener transitionPositionListener = new TransitionPositionListener(view, transitionValues.view, n + round, n2 + round2, translationX, translationY2);
        transition.addListener((Transition.TransitionListener)transitionPositionListener);
        ofPropertyValuesHolder.addListener((Animator$AnimatorListener)transitionPositionListener);
        AnimatorUtils.addPauseListener((Animator)ofPropertyValuesHolder, transitionPositionListener);
        ofPropertyValuesHolder.setInterpolator(interpolator);
        return (Animator)ofPropertyValuesHolder;
    }
    
    private static class TransitionPositionListener extends AnimatorListenerAdapter implements TransitionListener
    {
        private final View mMovingView;
        private float mPausedX;
        private float mPausedY;
        private final int mStartX;
        private final int mStartY;
        private final float mTerminalX;
        private final float mTerminalY;
        private int[] mTransitionPosition;
        private final View mViewInHierarchy;
        
        TransitionPositionListener(final View mMovingView, final View mViewInHierarchy, final int n, final int n2, final float mTerminalX, final float mTerminalY) {
            this.mMovingView = mMovingView;
            this.mViewInHierarchy = mViewInHierarchy;
            this.mStartX = n - Math.round(mMovingView.getTranslationX());
            this.mStartY = n2 - Math.round(mMovingView.getTranslationY());
            this.mTerminalX = mTerminalX;
            this.mTerminalY = mTerminalY;
            final int[] mTransitionPosition = (int[])mViewInHierarchy.getTag(R.id.transition_position);
            this.mTransitionPosition = mTransitionPosition;
            if (mTransitionPosition != null) {
                mViewInHierarchy.setTag(R.id.transition_position, (Object)null);
            }
        }
        
        public void onAnimationCancel(final Animator animator) {
            if (this.mTransitionPosition == null) {
                this.mTransitionPosition = new int[2];
            }
            this.mTransitionPosition[0] = Math.round(this.mStartX + this.mMovingView.getTranslationX());
            this.mTransitionPosition[1] = Math.round(this.mStartY + this.mMovingView.getTranslationY());
            this.mViewInHierarchy.setTag(R.id.transition_position, (Object)this.mTransitionPosition);
        }
        
        public void onAnimationPause(final Animator animator) {
            this.mPausedX = this.mMovingView.getTranslationX();
            this.mPausedY = this.mMovingView.getTranslationY();
            this.mMovingView.setTranslationX(this.mTerminalX);
            this.mMovingView.setTranslationY(this.mTerminalY);
        }
        
        public void onAnimationResume(final Animator animator) {
            this.mMovingView.setTranslationX(this.mPausedX);
            this.mMovingView.setTranslationY(this.mPausedY);
        }
        
        public void onTransitionCancel(final Transition transition) {
        }
        
        public void onTransitionEnd(final Transition transition) {
            this.mMovingView.setTranslationX(this.mTerminalX);
            this.mMovingView.setTranslationY(this.mTerminalY);
            transition.removeListener((Transition.TransitionListener)this);
        }
        
        public void onTransitionPause(final Transition transition) {
        }
        
        public void onTransitionResume(final Transition transition) {
        }
        
        public void onTransitionStart(final Transition transition) {
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.transition;

import android.graphics.drawable.Drawable;

interface ViewOverlayImpl
{
    void add(final Drawable p0);
    
    void remove(final Drawable p0);
}

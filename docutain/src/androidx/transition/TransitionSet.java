// 
// Decompiled by Procyon v0.6.0
// 

package androidx.transition;

import android.util.AndroidRuntimeException;
import android.animation.TimeInterpolator;
import android.view.ViewGroup;
import android.view.View;
import java.util.Iterator;
import android.content.res.TypedArray;
import org.xmlpull.v1.XmlPullParser;
import androidx.core.content.res.TypedArrayUtils;
import android.content.res.XmlResourceParser;
import android.util.AttributeSet;
import android.content.Context;
import java.util.ArrayList;

public class TransitionSet extends Transition
{
    private static final int FLAG_CHANGE_EPICENTER = 8;
    private static final int FLAG_CHANGE_INTERPOLATOR = 1;
    private static final int FLAG_CHANGE_PATH_MOTION = 4;
    private static final int FLAG_CHANGE_PROPAGATION = 2;
    public static final int ORDERING_SEQUENTIAL = 1;
    public static final int ORDERING_TOGETHER = 0;
    private int mChangeFlags;
    int mCurrentListeners;
    private boolean mPlayTogether;
    boolean mStarted;
    private ArrayList<Transition> mTransitions;
    
    public TransitionSet() {
        this.mTransitions = new ArrayList<Transition>();
        this.mPlayTogether = true;
        this.mStarted = false;
        this.mChangeFlags = 0;
    }
    
    public TransitionSet(final Context context, final AttributeSet set) {
        super(context, set);
        this.mTransitions = new ArrayList<Transition>();
        this.mPlayTogether = true;
        this.mStarted = false;
        this.mChangeFlags = 0;
        final TypedArray obtainStyledAttributes = context.obtainStyledAttributes(set, Styleable.TRANSITION_SET);
        this.setOrdering(TypedArrayUtils.getNamedInt(obtainStyledAttributes, (XmlPullParser)set, "transitionOrdering", 0, 0));
        obtainStyledAttributes.recycle();
    }
    
    private void addTransitionInternal(final Transition e) {
        this.mTransitions.add(e);
        e.mParent = this;
    }
    
    private void setupStartEndListeners() {
        final TransitionSetListener transitionSetListener = new TransitionSetListener(this);
        final Iterator<Transition> iterator = this.mTransitions.iterator();
        while (iterator.hasNext()) {
            iterator.next().addListener((TransitionListener)transitionSetListener);
        }
        this.mCurrentListeners = this.mTransitions.size();
    }
    
    @Override
    public TransitionSet addListener(final TransitionListener transitionListener) {
        return (TransitionSet)super.addListener(transitionListener);
    }
    
    @Override
    public TransitionSet addTarget(final int n) {
        for (int i = 0; i < this.mTransitions.size(); ++i) {
            this.mTransitions.get(i).addTarget(n);
        }
        return (TransitionSet)super.addTarget(n);
    }
    
    @Override
    public TransitionSet addTarget(final View view) {
        for (int i = 0; i < this.mTransitions.size(); ++i) {
            this.mTransitions.get(i).addTarget(view);
        }
        return (TransitionSet)super.addTarget(view);
    }
    
    @Override
    public TransitionSet addTarget(final Class<?> clazz) {
        for (int i = 0; i < this.mTransitions.size(); ++i) {
            this.mTransitions.get(i).addTarget(clazz);
        }
        return (TransitionSet)super.addTarget(clazz);
    }
    
    @Override
    public TransitionSet addTarget(final String s) {
        for (int i = 0; i < this.mTransitions.size(); ++i) {
            this.mTransitions.get(i).addTarget(s);
        }
        return (TransitionSet)super.addTarget(s);
    }
    
    public TransitionSet addTransition(final Transition transition) {
        this.addTransitionInternal(transition);
        if (this.mDuration >= 0L) {
            transition.setDuration(this.mDuration);
        }
        if ((this.mChangeFlags & 0x1) != 0x0) {
            transition.setInterpolator(this.getInterpolator());
        }
        if ((this.mChangeFlags & 0x2) != 0x0) {
            transition.setPropagation(this.getPropagation());
        }
        if ((this.mChangeFlags & 0x4) != 0x0) {
            transition.setPathMotion(this.getPathMotion());
        }
        if ((this.mChangeFlags & 0x8) != 0x0) {
            transition.setEpicenterCallback(this.getEpicenterCallback());
        }
        return this;
    }
    
    @Override
    protected void cancel() {
        super.cancel();
        for (int size = this.mTransitions.size(), i = 0; i < size; ++i) {
            this.mTransitions.get(i).cancel();
        }
    }
    
    @Override
    public void captureEndValues(final TransitionValues transitionValues) {
        if (this.isValidTarget(transitionValues.view)) {
            for (final Transition e : this.mTransitions) {
                if (e.isValidTarget(transitionValues.view)) {
                    e.captureEndValues(transitionValues);
                    transitionValues.mTargetedTransitions.add(e);
                }
            }
        }
    }
    
    @Override
    void capturePropagationValues(final TransitionValues transitionValues) {
        super.capturePropagationValues(transitionValues);
        for (int size = this.mTransitions.size(), i = 0; i < size; ++i) {
            this.mTransitions.get(i).capturePropagationValues(transitionValues);
        }
    }
    
    @Override
    public void captureStartValues(final TransitionValues transitionValues) {
        if (this.isValidTarget(transitionValues.view)) {
            for (final Transition e : this.mTransitions) {
                if (e.isValidTarget(transitionValues.view)) {
                    e.captureStartValues(transitionValues);
                    transitionValues.mTargetedTransitions.add(e);
                }
            }
        }
    }
    
    @Override
    public Transition clone() {
        final TransitionSet set = (TransitionSet)super.clone();
        set.mTransitions = new ArrayList<Transition>();
        for (int size = this.mTransitions.size(), i = 0; i < size; ++i) {
            set.addTransitionInternal(this.mTransitions.get(i).clone());
        }
        return set;
    }
    
    @Override
    protected void createAnimators(final ViewGroup viewGroup, final TransitionValuesMaps transitionValuesMaps, final TransitionValuesMaps transitionValuesMaps2, final ArrayList<TransitionValues> list, final ArrayList<TransitionValues> list2) {
        final long startDelay = this.getStartDelay();
        for (int size = this.mTransitions.size(), i = 0; i < size; ++i) {
            final Transition transition = this.mTransitions.get(i);
            if (startDelay > 0L && (this.mPlayTogether || i == 0)) {
                final long startDelay2 = transition.getStartDelay();
                if (startDelay2 > 0L) {
                    transition.setStartDelay(startDelay2 + startDelay);
                }
                else {
                    transition.setStartDelay(startDelay);
                }
            }
            transition.createAnimators(viewGroup, transitionValuesMaps, transitionValuesMaps2, list, list2);
        }
    }
    
    @Override
    public Transition excludeTarget(final int n, final boolean b) {
        for (int i = 0; i < this.mTransitions.size(); ++i) {
            this.mTransitions.get(i).excludeTarget(n, b);
        }
        return super.excludeTarget(n, b);
    }
    
    @Override
    public Transition excludeTarget(final View view, final boolean b) {
        for (int i = 0; i < this.mTransitions.size(); ++i) {
            this.mTransitions.get(i).excludeTarget(view, b);
        }
        return super.excludeTarget(view, b);
    }
    
    @Override
    public Transition excludeTarget(final Class<?> clazz, final boolean b) {
        for (int i = 0; i < this.mTransitions.size(); ++i) {
            this.mTransitions.get(i).excludeTarget(clazz, b);
        }
        return super.excludeTarget(clazz, b);
    }
    
    @Override
    public Transition excludeTarget(final String s, final boolean b) {
        for (int i = 0; i < this.mTransitions.size(); ++i) {
            this.mTransitions.get(i).excludeTarget(s, b);
        }
        return super.excludeTarget(s, b);
    }
    
    @Override
    void forceToEnd(final ViewGroup viewGroup) {
        super.forceToEnd(viewGroup);
        for (int size = this.mTransitions.size(), i = 0; i < size; ++i) {
            this.mTransitions.get(i).forceToEnd(viewGroup);
        }
    }
    
    public int getOrdering() {
        return (this.mPlayTogether ^ true) ? 1 : 0;
    }
    
    public Transition getTransitionAt(final int index) {
        if (index >= 0 && index < this.mTransitions.size()) {
            return this.mTransitions.get(index);
        }
        return null;
    }
    
    public int getTransitionCount() {
        return this.mTransitions.size();
    }
    
    @Override
    public void pause(final View view) {
        super.pause(view);
        for (int size = this.mTransitions.size(), i = 0; i < size; ++i) {
            this.mTransitions.get(i).pause(view);
        }
    }
    
    @Override
    public TransitionSet removeListener(final TransitionListener transitionListener) {
        return (TransitionSet)super.removeListener(transitionListener);
    }
    
    @Override
    public TransitionSet removeTarget(final int n) {
        for (int i = 0; i < this.mTransitions.size(); ++i) {
            this.mTransitions.get(i).removeTarget(n);
        }
        return (TransitionSet)super.removeTarget(n);
    }
    
    @Override
    public TransitionSet removeTarget(final View view) {
        for (int i = 0; i < this.mTransitions.size(); ++i) {
            this.mTransitions.get(i).removeTarget(view);
        }
        return (TransitionSet)super.removeTarget(view);
    }
    
    @Override
    public TransitionSet removeTarget(final Class<?> clazz) {
        for (int i = 0; i < this.mTransitions.size(); ++i) {
            this.mTransitions.get(i).removeTarget(clazz);
        }
        return (TransitionSet)super.removeTarget(clazz);
    }
    
    @Override
    public TransitionSet removeTarget(final String s) {
        for (int i = 0; i < this.mTransitions.size(); ++i) {
            this.mTransitions.get(i).removeTarget(s);
        }
        return (TransitionSet)super.removeTarget(s);
    }
    
    public TransitionSet removeTransition(final Transition o) {
        this.mTransitions.remove(o);
        o.mParent = null;
        return this;
    }
    
    @Override
    public void resume(final View view) {
        super.resume(view);
        for (int size = this.mTransitions.size(), i = 0; i < size; ++i) {
            this.mTransitions.get(i).resume(view);
        }
    }
    
    @Override
    protected void runAnimators() {
        if (this.mTransitions.isEmpty()) {
            this.start();
            this.end();
            return;
        }
        this.setupStartEndListeners();
        if (!this.mPlayTogether) {
            for (int i = 1; i < this.mTransitions.size(); ++i) {
                this.mTransitions.get(i - 1).addListener((TransitionListener)new TransitionListenerAdapter(this, this.mTransitions.get(i)) {
                    final TransitionSet this$0;
                    final Transition val$nextTransition;
                    
                    @Override
                    public void onTransitionEnd(final Transition transition) {
                        this.val$nextTransition.runAnimators();
                        transition.removeListener((TransitionListener)this);
                    }
                });
            }
            final Transition transition = this.mTransitions.get(0);
            if (transition != null) {
                transition.runAnimators();
            }
        }
        else {
            final Iterator<Transition> iterator = this.mTransitions.iterator();
            while (iterator.hasNext()) {
                iterator.next().runAnimators();
            }
        }
    }
    
    @Override
    void setCanRemoveViews(final boolean b) {
        super.setCanRemoveViews(b);
        for (int size = this.mTransitions.size(), i = 0; i < size; ++i) {
            this.mTransitions.get(i).setCanRemoveViews(b);
        }
    }
    
    @Override
    public TransitionSet setDuration(final long n) {
        super.setDuration(n);
        if (this.mDuration >= 0L) {
            final ArrayList<Transition> mTransitions = this.mTransitions;
            if (mTransitions != null) {
                for (int size = mTransitions.size(), i = 0; i < size; ++i) {
                    this.mTransitions.get(i).setDuration(n);
                }
            }
        }
        return this;
    }
    
    @Override
    public void setEpicenterCallback(final EpicenterCallback epicenterCallback) {
        super.setEpicenterCallback(epicenterCallback);
        this.mChangeFlags |= 0x8;
        for (int size = this.mTransitions.size(), i = 0; i < size; ++i) {
            this.mTransitions.get(i).setEpicenterCallback(epicenterCallback);
        }
    }
    
    @Override
    public TransitionSet setInterpolator(final TimeInterpolator timeInterpolator) {
        this.mChangeFlags |= 0x1;
        final ArrayList<Transition> mTransitions = this.mTransitions;
        if (mTransitions != null) {
            for (int size = mTransitions.size(), i = 0; i < size; ++i) {
                this.mTransitions.get(i).setInterpolator(timeInterpolator);
            }
        }
        return (TransitionSet)super.setInterpolator(timeInterpolator);
    }
    
    public TransitionSet setOrdering(final int i) {
        if (i != 0) {
            if (i != 1) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Invalid parameter for TransitionSet ordering: ");
                sb.append(i);
                throw new AndroidRuntimeException(sb.toString());
            }
            this.mPlayTogether = false;
        }
        else {
            this.mPlayTogether = true;
        }
        return this;
    }
    
    @Override
    public void setPathMotion(final PathMotion pathMotion) {
        super.setPathMotion(pathMotion);
        this.mChangeFlags |= 0x4;
        if (this.mTransitions != null) {
            for (int i = 0; i < this.mTransitions.size(); ++i) {
                this.mTransitions.get(i).setPathMotion(pathMotion);
            }
        }
    }
    
    @Override
    public void setPropagation(final TransitionPropagation transitionPropagation) {
        super.setPropagation(transitionPropagation);
        this.mChangeFlags |= 0x2;
        for (int size = this.mTransitions.size(), i = 0; i < size; ++i) {
            this.mTransitions.get(i).setPropagation(transitionPropagation);
        }
    }
    
    @Override
    public TransitionSet setStartDelay(final long startDelay) {
        return (TransitionSet)super.setStartDelay(startDelay);
    }
    
    @Override
    String toString(final String str) {
        String str2 = super.toString(str);
        for (int i = 0; i < this.mTransitions.size(); ++i) {
            final StringBuilder sb = new StringBuilder();
            sb.append(str2);
            sb.append("\n");
            final Transition transition = this.mTransitions.get(i);
            final StringBuilder sb2 = new StringBuilder();
            sb2.append(str);
            sb2.append("  ");
            sb.append(transition.toString(sb2.toString()));
            str2 = sb.toString();
        }
        return str2;
    }
    
    static class TransitionSetListener extends TransitionListenerAdapter
    {
        TransitionSet mTransitionSet;
        
        TransitionSetListener(final TransitionSet mTransitionSet) {
            this.mTransitionSet = mTransitionSet;
        }
        
        @Override
        public void onTransitionEnd(final Transition transition) {
            final TransitionSet mTransitionSet = this.mTransitionSet;
            --mTransitionSet.mCurrentListeners;
            if (this.mTransitionSet.mCurrentListeners == 0) {
                this.mTransitionSet.mStarted = false;
                this.mTransitionSet.end();
            }
            transition.removeListener((TransitionListener)this);
        }
        
        @Override
        public void onTransitionStart(final Transition transition) {
            if (!this.mTransitionSet.mStarted) {
                this.mTransitionSet.start();
                this.mTransitionSet.mStarted = true;
            }
        }
    }
}

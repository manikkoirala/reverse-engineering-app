// 
// Decompiled by Procyon v0.6.0
// 

package androidx.transition;

import java.lang.reflect.InvocationTargetException;
import android.view.ViewParent;
import android.view.MotionEvent;
import android.graphics.Canvas;
import androidx.core.view.ViewCompat;
import android.graphics.drawable.Drawable$Callback;
import android.graphics.Rect;
import java.util.ArrayList;
import java.lang.reflect.Method;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.ViewGroup;
import android.content.Context;

class ViewOverlayApi14 implements ViewOverlayImpl
{
    protected OverlayViewGroup mOverlayViewGroup;
    
    ViewOverlayApi14(final Context context, final ViewGroup viewGroup, final View view) {
        this.mOverlayViewGroup = new OverlayViewGroup(context, viewGroup, view, this);
    }
    
    static ViewOverlayApi14 createFrom(final View view) {
        final ViewGroup contentView = getContentView(view);
        if (contentView != null) {
            for (int childCount = contentView.getChildCount(), i = 0; i < childCount; ++i) {
                final View child = contentView.getChildAt(i);
                if (child instanceof OverlayViewGroup) {
                    return ((OverlayViewGroup)child).mViewOverlay;
                }
            }
            return new ViewGroupOverlayApi14(contentView.getContext(), contentView, view);
        }
        return null;
    }
    
    static ViewGroup getContentView(View view) {
        while (view != null) {
            if (view.getId() == 16908290 && view instanceof ViewGroup) {
                return (ViewGroup)view;
            }
            if (!(view.getParent() instanceof ViewGroup)) {
                continue;
            }
            view = (View)view.getParent();
        }
        return null;
    }
    
    @Override
    public void add(final Drawable drawable) {
        this.mOverlayViewGroup.add(drawable);
    }
    
    @Override
    public void remove(final Drawable drawable) {
        this.mOverlayViewGroup.remove(drawable);
    }
    
    static class OverlayViewGroup extends ViewGroup
    {
        static Method sInvalidateChildInParentFastMethod;
        private boolean mDisposed;
        ArrayList<Drawable> mDrawables;
        ViewGroup mHostView;
        View mRequestingView;
        ViewOverlayApi14 mViewOverlay;
        
        static {
            try {
                OverlayViewGroup.sInvalidateChildInParentFastMethod = ViewGroup.class.getDeclaredMethod("invalidateChildInParentFast", Integer.TYPE, Integer.TYPE, Rect.class);
            }
            catch (final NoSuchMethodException ex) {}
        }
        
        OverlayViewGroup(final Context context, final ViewGroup mHostView, final View mRequestingView, final ViewOverlayApi14 mViewOverlay) {
            super(context);
            this.mDrawables = null;
            this.mHostView = mHostView;
            this.mRequestingView = mRequestingView;
            this.setRight(mHostView.getWidth());
            this.setBottom(mHostView.getHeight());
            mHostView.addView((View)this);
            this.mViewOverlay = mViewOverlay;
        }
        
        private void assertNotDisposed() {
            if (!this.mDisposed) {
                return;
            }
            throw new IllegalStateException("This overlay was disposed already. Please use a new one via ViewGroupUtils.getOverlay()");
        }
        
        private void disposeIfEmpty() {
            if (this.getChildCount() == 0) {
                final ArrayList<Drawable> mDrawables = this.mDrawables;
                if (mDrawables == null || mDrawables.size() == 0) {
                    this.mDisposed = true;
                    this.mHostView.removeView((View)this);
                }
            }
        }
        
        private void getOffset(final int[] array) {
            final int[] array2 = new int[2];
            final int[] array3 = new int[2];
            this.mHostView.getLocationOnScreen(array2);
            this.mRequestingView.getLocationOnScreen(array3);
            array[0] = array3[0] - array2[0];
            array[1] = array3[1] - array2[1];
        }
        
        public void add(final Drawable drawable) {
            this.assertNotDisposed();
            if (this.mDrawables == null) {
                this.mDrawables = new ArrayList<Drawable>();
            }
            if (!this.mDrawables.contains(drawable)) {
                this.mDrawables.add(drawable);
                this.invalidate(drawable.getBounds());
                drawable.setCallback((Drawable$Callback)this);
            }
        }
        
        public void add(final View view) {
            this.assertNotDisposed();
            if (view.getParent() instanceof ViewGroup) {
                final ViewGroup viewGroup = (ViewGroup)view.getParent();
                if (viewGroup != this.mHostView && viewGroup.getParent() != null && ViewCompat.isAttachedToWindow((View)viewGroup)) {
                    final int[] array = new int[2];
                    final int[] array2 = new int[2];
                    viewGroup.getLocationOnScreen(array);
                    this.mHostView.getLocationOnScreen(array2);
                    ViewCompat.offsetLeftAndRight(view, array[0] - array2[0]);
                    ViewCompat.offsetTopAndBottom(view, array[1] - array2[1]);
                }
                viewGroup.removeView(view);
                if (view.getParent() != null) {
                    viewGroup.removeView(view);
                }
            }
            super.addView(view);
        }
        
        protected void dispatchDraw(final Canvas canvas) {
            final int[] array = new int[2];
            final int[] array2 = new int[2];
            this.mHostView.getLocationOnScreen(array);
            this.mRequestingView.getLocationOnScreen(array2);
            int i = 0;
            canvas.translate((float)(array2[0] - array[0]), (float)(array2[1] - array[1]));
            canvas.clipRect(new Rect(0, 0, this.mRequestingView.getWidth(), this.mRequestingView.getHeight()));
            super.dispatchDraw(canvas);
            final ArrayList<Drawable> mDrawables = this.mDrawables;
            int size;
            if (mDrawables == null) {
                size = 0;
            }
            else {
                size = mDrawables.size();
            }
            while (i < size) {
                this.mDrawables.get(i).draw(canvas);
                ++i;
            }
        }
        
        public boolean dispatchTouchEvent(final MotionEvent motionEvent) {
            return false;
        }
        
        public ViewParent invalidateChildInParent(final int[] array, final Rect rect) {
            if (this.mHostView != null) {
                rect.offset(array[0], array[1]);
                if (this.mHostView != null) {
                    array[1] = (array[0] = 0);
                    final int[] array2 = new int[2];
                    this.getOffset(array2);
                    rect.offset(array2[0], array2[1]);
                    return super.invalidateChildInParent(array, rect);
                }
                this.invalidate(rect);
            }
            return null;
        }
        
        protected ViewParent invalidateChildInParentFast(final int i, final int j, final Rect rect) {
            if (this.mHostView != null && OverlayViewGroup.sInvalidateChildInParentFastMethod != null) {
                try {
                    this.getOffset(new int[2]);
                    OverlayViewGroup.sInvalidateChildInParentFastMethod.invoke(this.mHostView, i, j, rect);
                }
                catch (final InvocationTargetException ex) {
                    ex.printStackTrace();
                }
                catch (final IllegalAccessException ex2) {
                    ex2.printStackTrace();
                }
            }
            return null;
        }
        
        public void invalidateDrawable(final Drawable drawable) {
            this.invalidate(drawable.getBounds());
        }
        
        protected void onLayout(final boolean b, final int n, final int n2, final int n3, final int n4) {
        }
        
        public void remove(final Drawable o) {
            final ArrayList<Drawable> mDrawables = this.mDrawables;
            if (mDrawables != null) {
                mDrawables.remove(o);
                this.invalidate(o.getBounds());
                o.setCallback((Drawable$Callback)null);
                this.disposeIfEmpty();
            }
        }
        
        public void remove(final View view) {
            super.removeView(view);
            this.disposeIfEmpty();
        }
        
        protected boolean verifyDrawable(final Drawable o) {
            if (!super.verifyDrawable(o)) {
                final ArrayList<Drawable> mDrawables = this.mDrawables;
                if (mDrawables == null || !mDrawables.contains(o)) {
                    return false;
                }
            }
            return true;
        }
    }
}

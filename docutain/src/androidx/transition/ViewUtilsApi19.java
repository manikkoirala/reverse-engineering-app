// 
// Decompiled by Procyon v0.6.0
// 

package androidx.transition;

import android.view.View;

class ViewUtilsApi19 extends ViewUtilsBase
{
    private static boolean sTryHiddenTransitionAlpha = true;
    
    @Override
    public void clearNonTransitionAlpha(final View view) {
    }
    
    @Override
    public float getTransitionAlpha(final View view) {
        if (ViewUtilsApi19.sTryHiddenTransitionAlpha) {
            try {
                return view.getTransitionAlpha();
            }
            catch (final NoSuchMethodError noSuchMethodError) {
                ViewUtilsApi19.sTryHiddenTransitionAlpha = false;
            }
        }
        return view.getAlpha();
    }
    
    @Override
    public void saveNonTransitionAlpha(final View view) {
    }
    
    @Override
    public void setTransitionAlpha(final View view, final float n) {
        if (ViewUtilsApi19.sTryHiddenTransitionAlpha) {
            try {
                view.setTransitionAlpha(n);
                return;
            }
            catch (final NoSuchMethodError noSuchMethodError) {
                ViewUtilsApi19.sTryHiddenTransitionAlpha = false;
            }
        }
        view.setAlpha(n);
    }
}

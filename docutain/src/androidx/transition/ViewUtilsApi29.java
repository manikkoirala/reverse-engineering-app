// 
// Decompiled by Procyon v0.6.0
// 

package androidx.transition;

import android.graphics.Matrix;
import android.view.View;

class ViewUtilsApi29 extends ViewUtilsApi23
{
    @Override
    public float getTransitionAlpha(final View view) {
        return view.getTransitionAlpha();
    }
    
    @Override
    public void setAnimationMatrix(final View view, final Matrix animationMatrix) {
        view.setAnimationMatrix(animationMatrix);
    }
    
    @Override
    public void setLeftTopRightBottom(final View view, final int n, final int n2, final int n3, final int n4) {
        view.setLeftTopRightBottom(n, n2, n3, n4);
    }
    
    @Override
    public void setTransitionAlpha(final View view, final float transitionAlpha) {
        view.setTransitionAlpha(transitionAlpha);
    }
    
    @Override
    public void setTransitionVisibility(final View view, final int transitionVisibility) {
        view.setTransitionVisibility(transitionVisibility);
    }
    
    @Override
    public void transformMatrixToGlobal(final View view, final Matrix matrix) {
        view.transformMatrixToGlobal(matrix);
    }
    
    @Override
    public void transformMatrixToLocal(final View view, final Matrix matrix) {
        view.transformMatrixToLocal(matrix);
    }
}

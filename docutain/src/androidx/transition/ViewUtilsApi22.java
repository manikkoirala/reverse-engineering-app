// 
// Decompiled by Procyon v0.6.0
// 

package androidx.transition;

import android.view.View;

class ViewUtilsApi22 extends ViewUtilsApi21
{
    private static boolean sTryHiddenSetLeftTopRightBottom = true;
    
    @Override
    public void setLeftTopRightBottom(final View view, final int n, final int n2, final int n3, final int n4) {
        if (ViewUtilsApi22.sTryHiddenSetLeftTopRightBottom) {
            try {
                view.setLeftTopRightBottom(n, n2, n3, n4);
            }
            catch (final NoSuchMethodError noSuchMethodError) {
                ViewUtilsApi22.sTryHiddenSetLeftTopRightBottom = false;
            }
        }
    }
}

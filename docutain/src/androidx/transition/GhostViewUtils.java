// 
// Decompiled by Procyon v0.6.0
// 

package androidx.transition;

import android.os.Build$VERSION;
import android.graphics.Matrix;
import android.view.ViewGroup;
import android.view.View;

class GhostViewUtils
{
    private GhostViewUtils() {
    }
    
    static GhostView addGhost(final View view, final ViewGroup viewGroup, final Matrix matrix) {
        if (Build$VERSION.SDK_INT == 28) {
            return GhostViewPlatform.addGhost(view, viewGroup, matrix);
        }
        return GhostViewPort.addGhost(view, viewGroup, matrix);
    }
    
    static void removeGhost(final View view) {
        if (Build$VERSION.SDK_INT == 28) {
            GhostViewPlatform.removeGhost(view);
        }
        else {
            GhostViewPort.removeGhost(view);
        }
    }
}

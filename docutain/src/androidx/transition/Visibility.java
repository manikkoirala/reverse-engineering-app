// 
// Decompiled by Procyon v0.6.0
// 

package androidx.transition;

import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;
import android.animation.AnimatorListenerAdapter;
import android.animation.Animator$AnimatorListener;
import android.view.View;
import android.animation.Animator;
import android.view.ViewGroup;
import android.content.res.TypedArray;
import org.xmlpull.v1.XmlPullParser;
import androidx.core.content.res.TypedArrayUtils;
import android.content.res.XmlResourceParser;
import android.util.AttributeSet;
import android.content.Context;

public abstract class Visibility extends Transition
{
    public static final int MODE_IN = 1;
    public static final int MODE_OUT = 2;
    private static final String PROPNAME_PARENT = "android:visibility:parent";
    private static final String PROPNAME_SCREEN_LOCATION = "android:visibility:screenLocation";
    static final String PROPNAME_VISIBILITY = "android:visibility:visibility";
    private static final String[] sTransitionProperties;
    private int mMode;
    
    static {
        sTransitionProperties = new String[] { "android:visibility:visibility", "android:visibility:parent" };
    }
    
    public Visibility() {
        this.mMode = 3;
    }
    
    public Visibility(final Context context, final AttributeSet set) {
        super(context, set);
        this.mMode = 3;
        final TypedArray obtainStyledAttributes = context.obtainStyledAttributes(set, Styleable.VISIBILITY_TRANSITION);
        final int namedInt = TypedArrayUtils.getNamedInt(obtainStyledAttributes, (XmlPullParser)set, "transitionVisibilityMode", 0, 0);
        obtainStyledAttributes.recycle();
        if (namedInt != 0) {
            this.setMode(namedInt);
        }
    }
    
    private void captureValues(final TransitionValues transitionValues) {
        transitionValues.values.put("android:visibility:visibility", transitionValues.view.getVisibility());
        transitionValues.values.put("android:visibility:parent", transitionValues.view.getParent());
        final int[] array = new int[2];
        transitionValues.view.getLocationOnScreen(array);
        transitionValues.values.put("android:visibility:screenLocation", array);
    }
    
    private VisibilityInfo getVisibilityChangeInfo(final TransitionValues transitionValues, final TransitionValues transitionValues2) {
        final VisibilityInfo visibilityInfo = new VisibilityInfo();
        visibilityInfo.mVisibilityChange = false;
        visibilityInfo.mFadeIn = false;
        if (transitionValues != null && transitionValues.values.containsKey("android:visibility:visibility")) {
            visibilityInfo.mStartVisibility = transitionValues.values.get("android:visibility:visibility");
            visibilityInfo.mStartParent = transitionValues.values.get("android:visibility:parent");
        }
        else {
            visibilityInfo.mStartVisibility = -1;
            visibilityInfo.mStartParent = null;
        }
        if (transitionValues2 != null && transitionValues2.values.containsKey("android:visibility:visibility")) {
            visibilityInfo.mEndVisibility = transitionValues2.values.get("android:visibility:visibility");
            visibilityInfo.mEndParent = transitionValues2.values.get("android:visibility:parent");
        }
        else {
            visibilityInfo.mEndVisibility = -1;
            visibilityInfo.mEndParent = null;
        }
        if (transitionValues != null && transitionValues2 != null) {
            if (visibilityInfo.mStartVisibility == visibilityInfo.mEndVisibility && visibilityInfo.mStartParent == visibilityInfo.mEndParent) {
                return visibilityInfo;
            }
            if (visibilityInfo.mStartVisibility != visibilityInfo.mEndVisibility) {
                if (visibilityInfo.mStartVisibility == 0) {
                    visibilityInfo.mFadeIn = false;
                    visibilityInfo.mVisibilityChange = true;
                }
                else if (visibilityInfo.mEndVisibility == 0) {
                    visibilityInfo.mFadeIn = true;
                    visibilityInfo.mVisibilityChange = true;
                }
            }
            else if (visibilityInfo.mEndParent == null) {
                visibilityInfo.mFadeIn = false;
                visibilityInfo.mVisibilityChange = true;
            }
            else if (visibilityInfo.mStartParent == null) {
                visibilityInfo.mFadeIn = true;
                visibilityInfo.mVisibilityChange = true;
            }
        }
        else if (transitionValues == null && visibilityInfo.mEndVisibility == 0) {
            visibilityInfo.mFadeIn = true;
            visibilityInfo.mVisibilityChange = true;
        }
        else if (transitionValues2 == null && visibilityInfo.mStartVisibility == 0) {
            visibilityInfo.mFadeIn = false;
            visibilityInfo.mVisibilityChange = true;
        }
        return visibilityInfo;
    }
    
    @Override
    public void captureEndValues(final TransitionValues transitionValues) {
        this.captureValues(transitionValues);
    }
    
    @Override
    public void captureStartValues(final TransitionValues transitionValues) {
        this.captureValues(transitionValues);
    }
    
    @Override
    public Animator createAnimator(final ViewGroup viewGroup, final TransitionValues transitionValues, final TransitionValues transitionValues2) {
        final VisibilityInfo visibilityChangeInfo = this.getVisibilityChangeInfo(transitionValues, transitionValues2);
        if (!visibilityChangeInfo.mVisibilityChange || (visibilityChangeInfo.mStartParent == null && visibilityChangeInfo.mEndParent == null)) {
            return null;
        }
        if (visibilityChangeInfo.mFadeIn) {
            return this.onAppear(viewGroup, transitionValues, visibilityChangeInfo.mStartVisibility, transitionValues2, visibilityChangeInfo.mEndVisibility);
        }
        return this.onDisappear(viewGroup, transitionValues, visibilityChangeInfo.mStartVisibility, transitionValues2, visibilityChangeInfo.mEndVisibility);
    }
    
    public int getMode() {
        return this.mMode;
    }
    
    @Override
    public String[] getTransitionProperties() {
        return Visibility.sTransitionProperties;
    }
    
    @Override
    public boolean isTransitionRequired(final TransitionValues transitionValues, final TransitionValues transitionValues2) {
        final boolean b = false;
        if (transitionValues == null && transitionValues2 == null) {
            return false;
        }
        if (transitionValues != null && transitionValues2 != null && transitionValues2.values.containsKey("android:visibility:visibility") != transitionValues.values.containsKey("android:visibility:visibility")) {
            return false;
        }
        final VisibilityInfo visibilityChangeInfo = this.getVisibilityChangeInfo(transitionValues, transitionValues2);
        boolean b2 = b;
        if (visibilityChangeInfo.mVisibilityChange) {
            if (visibilityChangeInfo.mStartVisibility != 0) {
                b2 = b;
                if (visibilityChangeInfo.mEndVisibility != 0) {
                    return b2;
                }
            }
            b2 = true;
        }
        return b2;
    }
    
    public boolean isVisible(final TransitionValues transitionValues) {
        final boolean b = false;
        if (transitionValues == null) {
            return false;
        }
        final int intValue = transitionValues.values.get("android:visibility:visibility");
        final View view = transitionValues.values.get("android:visibility:parent");
        boolean b2 = b;
        if (intValue == 0) {
            b2 = b;
            if (view != null) {
                b2 = true;
            }
        }
        return b2;
    }
    
    public Animator onAppear(final ViewGroup viewGroup, final View view, final TransitionValues transitionValues, final TransitionValues transitionValues2) {
        return null;
    }
    
    public Animator onAppear(final ViewGroup viewGroup, final TransitionValues transitionValues, final int n, final TransitionValues transitionValues2, final int n2) {
        if ((this.mMode & 0x1) == 0x1 && transitionValues2 != null) {
            if (transitionValues == null) {
                final View view = (View)transitionValues2.view.getParent();
                if (this.getVisibilityChangeInfo(this.getMatchedTransitionValues(view, false), this.getTransitionValues(view, false)).mVisibilityChange) {
                    return null;
                }
            }
            return this.onAppear(viewGroup, transitionValues2.view, transitionValues, transitionValues2);
        }
        return null;
    }
    
    public Animator onDisappear(final ViewGroup viewGroup, final View view, final TransitionValues transitionValues, final TransitionValues transitionValues2) {
        return null;
    }
    
    public Animator onDisappear(final ViewGroup viewGroup, final TransitionValues transitionValues, int n, final TransitionValues transitionValues2, int n2) {
        if ((this.mMode & 0x2) != 0x2) {
            return null;
        }
        if (transitionValues == null) {
            return null;
        }
        final View view = transitionValues.view;
        View view2;
        if (transitionValues2 != null) {
            view2 = transitionValues2.view;
        }
        else {
            view2 = null;
        }
        View view3 = (View)view.getTag(R.id.save_overlay_view);
        View view4 = null;
        Label_0287: {
            if (view3 != null) {
                view4 = null;
                n = 1;
            }
            else {
                View view5 = null;
                Label_0133: {
                    Label_0125: {
                        if (view2 != null && view2.getParent() != null) {
                            if (n2 != 4) {
                                if (view != view2) {
                                    break Label_0125;
                                }
                            }
                            view5 = view2;
                            n = 0;
                            view2 = null;
                            break Label_0133;
                        }
                        if (view2 != null) {
                            view5 = null;
                            n = 0;
                            break Label_0133;
                        }
                    }
                    view2 = null;
                    view5 = null;
                    n = 1;
                }
                View copyViewImage = view2;
                Label_0277: {
                    if (n != 0) {
                        if (view.getParent() != null) {
                            copyViewImage = view2;
                            if (!(view.getParent() instanceof View)) {
                                break Label_0277;
                            }
                            final View view6 = (View)view.getParent();
                            if (!this.getVisibilityChangeInfo(this.getTransitionValues(view6, true), this.getMatchedTransitionValues(view6, true)).mVisibilityChange) {
                                copyViewImage = TransitionUtils.copyViewImage(viewGroup, view, view6);
                                break Label_0277;
                            }
                            n = view6.getId();
                            copyViewImage = view2;
                            if (view6.getParent() != null) {
                                break Label_0277;
                            }
                            copyViewImage = view2;
                            if (n == -1) {
                                break Label_0277;
                            }
                            copyViewImage = view2;
                            if (viewGroup.findViewById(n) == null) {
                                break Label_0277;
                            }
                            copyViewImage = view2;
                            if (!this.mCanRemoveViews) {
                                break Label_0277;
                            }
                        }
                        view4 = view5;
                        n = 0;
                        view3 = view;
                        break Label_0287;
                    }
                }
                n = 0;
                view4 = view5;
                view3 = copyViewImage;
            }
        }
        if (view3 != null) {
            if (n == 0) {
                final int[] array = transitionValues.values.get("android:visibility:screenLocation");
                n2 = array[0];
                final int n3 = array[1];
                final int[] array2 = new int[2];
                viewGroup.getLocationOnScreen(array2);
                view3.offsetLeftAndRight(n2 - array2[0] - view3.getLeft());
                view3.offsetTopAndBottom(n3 - array2[1] - view3.getTop());
                ViewGroupUtils.getOverlay(viewGroup).add(view3);
            }
            final Animator onDisappear = this.onDisappear(viewGroup, view3, transitionValues, transitionValues2);
            if (n == 0) {
                if (onDisappear == null) {
                    ViewGroupUtils.getOverlay(viewGroup).remove(view3);
                }
                else {
                    view.setTag(R.id.save_overlay_view, (Object)view3);
                    this.addListener((TransitionListener)new TransitionListenerAdapter(this, viewGroup, view3, view) {
                        final Visibility this$0;
                        final View val$finalOverlayView;
                        final ViewGroup val$overlayHost;
                        final View val$startView;
                        
                        @Override
                        public void onTransitionEnd(final Transition transition) {
                            this.val$startView.setTag(R.id.save_overlay_view, (Object)null);
                            ViewGroupUtils.getOverlay(this.val$overlayHost).remove(this.val$finalOverlayView);
                            transition.removeListener((TransitionListener)this);
                        }
                        
                        @Override
                        public void onTransitionPause(final Transition transition) {
                            ViewGroupUtils.getOverlay(this.val$overlayHost).remove(this.val$finalOverlayView);
                        }
                        
                        @Override
                        public void onTransitionResume(final Transition transition) {
                            if (this.val$finalOverlayView.getParent() == null) {
                                ViewGroupUtils.getOverlay(this.val$overlayHost).add(this.val$finalOverlayView);
                            }
                            else {
                                this.this$0.cancel();
                            }
                        }
                    });
                }
            }
            return onDisappear;
        }
        if (view4 != null) {
            n = view4.getVisibility();
            ViewUtils.setTransitionVisibility(view4, 0);
            final Animator onDisappear2 = this.onDisappear(viewGroup, view4, transitionValues, transitionValues2);
            if (onDisappear2 != null) {
                final DisappearListener disappearListener = new DisappearListener(view4, n2, true);
                onDisappear2.addListener((Animator$AnimatorListener)disappearListener);
                AnimatorUtils.addPauseListener(onDisappear2, disappearListener);
                this.addListener((TransitionListener)disappearListener);
            }
            else {
                ViewUtils.setTransitionVisibility(view4, n);
            }
            return onDisappear2;
        }
        return null;
    }
    
    public void setMode(final int mMode) {
        if ((mMode & 0xFFFFFFFC) == 0x0) {
            this.mMode = mMode;
            return;
        }
        throw new IllegalArgumentException("Only MODE_IN and MODE_OUT flags are allowed");
    }
    
    private static class DisappearListener extends AnimatorListenerAdapter implements TransitionListener, AnimatorPauseListenerCompat
    {
        boolean mCanceled;
        private final int mFinalVisibility;
        private boolean mLayoutSuppressed;
        private final ViewGroup mParent;
        private final boolean mSuppressLayout;
        private final View mView;
        
        DisappearListener(final View mView, final int mFinalVisibility, final boolean mSuppressLayout) {
            this.mCanceled = false;
            this.mView = mView;
            this.mFinalVisibility = mFinalVisibility;
            this.mParent = (ViewGroup)mView.getParent();
            this.mSuppressLayout = mSuppressLayout;
            this.suppressLayout(true);
        }
        
        private void hideViewWhenNotCanceled() {
            if (!this.mCanceled) {
                ViewUtils.setTransitionVisibility(this.mView, this.mFinalVisibility);
                final ViewGroup mParent = this.mParent;
                if (mParent != null) {
                    mParent.invalidate();
                }
            }
            this.suppressLayout(false);
        }
        
        private void suppressLayout(final boolean mLayoutSuppressed) {
            if (this.mSuppressLayout && this.mLayoutSuppressed != mLayoutSuppressed) {
                final ViewGroup mParent = this.mParent;
                if (mParent != null) {
                    ViewGroupUtils.suppressLayout(mParent, this.mLayoutSuppressed = mLayoutSuppressed);
                }
            }
        }
        
        public void onAnimationCancel(final Animator animator) {
            this.mCanceled = true;
        }
        
        public void onAnimationEnd(final Animator animator) {
            this.hideViewWhenNotCanceled();
        }
        
        public void onAnimationPause(final Animator animator) {
            if (!this.mCanceled) {
                ViewUtils.setTransitionVisibility(this.mView, this.mFinalVisibility);
            }
        }
        
        public void onAnimationRepeat(final Animator animator) {
        }
        
        public void onAnimationResume(final Animator animator) {
            if (!this.mCanceled) {
                ViewUtils.setTransitionVisibility(this.mView, 0);
            }
        }
        
        public void onAnimationStart(final Animator animator) {
        }
        
        public void onTransitionCancel(final Transition transition) {
        }
        
        public void onTransitionEnd(final Transition transition) {
            this.hideViewWhenNotCanceled();
            transition.removeListener((TransitionListener)this);
        }
        
        public void onTransitionPause(final Transition transition) {
            this.suppressLayout(false);
        }
        
        public void onTransitionResume(final Transition transition) {
            this.suppressLayout(true);
        }
        
        public void onTransitionStart(final Transition transition) {
        }
    }
    
    @Retention(RetentionPolicy.SOURCE)
    public @interface Mode {
    }
    
    private static class VisibilityInfo
    {
        ViewGroup mEndParent;
        int mEndVisibility;
        boolean mFadeIn;
        ViewGroup mStartParent;
        int mStartVisibility;
        boolean mVisibilityChange;
        
        VisibilityInfo() {
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.transition;

import android.os.Build$VERSION;
import android.view.ViewParent;
import java.util.ArrayList;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

class GhostViewHolder extends FrameLayout
{
    private boolean mAttached;
    private ViewGroup mParent;
    
    GhostViewHolder(final ViewGroup mParent) {
        super(mParent.getContext());
        this.setClipChildren(false);
        (this.mParent = mParent).setTag(R.id.ghost_view_holder, (Object)this);
        ViewGroupUtils.getOverlay(this.mParent).add((View)this);
        this.mAttached = true;
    }
    
    static GhostViewHolder getHolder(final ViewGroup viewGroup) {
        return (GhostViewHolder)viewGroup.getTag(R.id.ghost_view_holder);
    }
    
    private int getInsertIndex(final ArrayList<View> list) {
        final ArrayList list2 = new ArrayList();
        int n = this.getChildCount() - 1;
        int i = 0;
        while (i <= n) {
            final int n2 = (i + n) / 2;
            getParents(((GhostViewPort)this.getChildAt(n2)).mView, list2);
            if (isOnTop(list, list2)) {
                i = n2 + 1;
            }
            else {
                n = n2 - 1;
            }
            list2.clear();
        }
        return i;
    }
    
    private static void getParents(final View e, final ArrayList<View> list) {
        final ViewParent parent = e.getParent();
        if (parent instanceof ViewGroup) {
            getParents((View)parent, list);
        }
        list.add(e);
    }
    
    private static boolean isOnTop(final View view, final View view2) {
        final ViewGroup viewGroup = (ViewGroup)view.getParent();
        final int childCount = viewGroup.getChildCount();
        final int sdk_INT = Build$VERSION.SDK_INT;
        final boolean b = false;
        boolean b2 = false;
        if (sdk_INT >= 21 && view.getZ() != view2.getZ()) {
            if (view.getZ() > view2.getZ()) {
                b2 = true;
            }
            return b2;
        }
        for (int i = 0; i < childCount; ++i) {
            final View child = viewGroup.getChildAt(ViewGroupUtils.getChildDrawingOrder(viewGroup, i));
            if (child == view) {
                return b;
            }
            if (child == view2) {
                break;
            }
        }
        return true;
    }
    
    private static boolean isOnTop(final ArrayList<View> list, final ArrayList<View> list2) {
        final boolean empty = list.isEmpty();
        boolean b2;
        final boolean b = b2 = true;
        if (!empty) {
            b2 = b;
            if (!list2.isEmpty()) {
                if (list.get(0) != list2.get(0)) {
                    b2 = b;
                }
                else {
                    final int min = Math.min(list.size(), list2.size());
                    for (int i = 1; i < min; ++i) {
                        final View view = list.get(i);
                        final View view2 = list2.get(i);
                        if (view != view2) {
                            return isOnTop(view, view2);
                        }
                    }
                    b2 = (list2.size() == min && b);
                }
            }
        }
        return b2;
    }
    
    void addGhostView(final GhostViewPort ghostViewPort) {
        final ArrayList list = new ArrayList();
        getParents(ghostViewPort.mView, list);
        final int insertIndex = this.getInsertIndex(list);
        if (insertIndex >= 0 && insertIndex < this.getChildCount()) {
            this.addView((View)ghostViewPort, insertIndex);
        }
        else {
            this.addView((View)ghostViewPort);
        }
    }
    
    public void onViewAdded(final View view) {
        if (this.mAttached) {
            super.onViewAdded(view);
            return;
        }
        throw new IllegalStateException("This GhostViewHolder is detached!");
    }
    
    public void onViewRemoved(final View view) {
        super.onViewRemoved(view);
        if ((this.getChildCount() == 1 && this.getChildAt(0) == view) || this.getChildCount() == 0) {
            this.mParent.setTag(R.id.ghost_view_holder, (Object)null);
            ViewGroupUtils.getOverlay(this.mParent).remove((View)this);
            this.mAttached = false;
        }
    }
    
    void popToOverlayTop() {
        if (this.mAttached) {
            ViewGroupUtils.getOverlay(this.mParent).remove((View)this);
            ViewGroupUtils.getOverlay(this.mParent).add((View)this);
            return;
        }
        throw new IllegalStateException("This GhostViewHolder is detached!");
    }
}

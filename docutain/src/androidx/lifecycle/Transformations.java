// 
// Decompiled by Procyon v0.6.0
// 

package androidx.lifecycle;

import kotlin.DeprecationLevel;
import kotlin.Deprecated;
import androidx.arch.core.util.Function;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Ref$BooleanRef;
import kotlin.jvm.internal.Intrinsics;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000\u001c\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u001a\u001e\u0010\u0000\u001a\b\u0012\u0004\u0012\u0002H\u00020\u0001\"\u0004\b\u0000\u0010\u0002*\b\u0012\u0004\u0012\u0002H\u00020\u0001H\u0007\u001aB\u0010\u0003\u001a\b\u0012\u0004\u0012\u0002H\u00040\u0001\"\u0004\b\u0000\u0010\u0002\"\u0004\b\u0001\u0010\u0004*\b\u0012\u0004\u0012\u0002H\u00020\u00012\u001c\u0010\u0005\u001a\u0018\u0012\t\u0012\u0007H\u0002¢\u0006\u0002\b\u0007\u0012\t\u0012\u0007H\u0004¢\u0006\u0002\b\u00070\u0006H\u0007\u001a8\u0010\u0003\u001a\b\u0012\u0004\u0012\u0002H\u00040\u0001\"\u0004\b\u0000\u0010\u0002\"\u0004\b\u0001\u0010\u0004*\b\u0012\u0004\u0012\u0002H\u00020\u00012\u0012\u0010\b\u001a\u000e\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00040\tH\u0007\u001aJ\u0010\n\u001a\b\u0012\u0004\u0012\u0002H\u00040\u0001\"\u0004\b\u0000\u0010\u0002\"\u0004\b\u0001\u0010\u0004*\b\u0012\u0004\u0012\u0002H\u00020\u00012$\u0010\u0005\u001a \u0012\t\u0012\u0007H\u0002¢\u0006\u0002\b\u0007\u0012\u0011\u0012\u000f\u0012\u0004\u0012\u0002H\u0004\u0018\u00010\u0001¢\u0006\u0002\b\u00070\u0006H\u0007\u001a>\u0010\n\u001a\b\u0012\u0004\u0012\u0002H\u00040\u0001\"\u0004\b\u0000\u0010\u0002\"\u0004\b\u0001\u0010\u0004*\b\u0012\u0004\u0012\u0002H\u00020\u00012\u0018\u0010\u000b\u001a\u0014\u0012\u0004\u0012\u0002H\u0002\u0012\n\u0012\b\u0012\u0004\u0012\u0002H\u00040\u00010\tH\u0007¨\u0006\f" }, d2 = { "distinctUntilChanged", "Landroidx/lifecycle/LiveData;", "X", "map", "Y", "transform", "Lkotlin/Function1;", "Lkotlin/jvm/JvmSuppressWildcards;", "mapFunction", "Landroidx/arch/core/util/Function;", "switchMap", "switchMapFunction", "lifecycle-livedata_release" }, k = 2, mv = { 1, 8, 0 }, xi = 48)
public final class Transformations
{
    public static final <X> LiveData<X> distinctUntilChanged(final LiveData<X> liveData) {
        Intrinsics.checkNotNullParameter((Object)liveData, "<this>");
        final MediatorLiveData mediatorLiveData = new MediatorLiveData();
        final Ref$BooleanRef ref$BooleanRef = new Ref$BooleanRef();
        ref$BooleanRef.element = true;
        if (liveData.isInitialized()) {
            mediatorLiveData.setValue(liveData.getValue());
            ref$BooleanRef.element = false;
        }
        mediatorLiveData.addSource(liveData, (Observer<? super X>)new Transformations$sam$androidx_lifecycle_Observer.Transformations$sam$androidx_lifecycle_Observer$0((Function1)new Transformations$distinctUntilChanged.Transformations$distinctUntilChanged$1(mediatorLiveData, ref$BooleanRef)));
        return mediatorLiveData;
    }
    
    public static final <X, Y> LiveData<Y> map(final LiveData<X> liveData, final Function1<X, Y> function1) {
        Intrinsics.checkNotNullParameter((Object)liveData, "<this>");
        Intrinsics.checkNotNullParameter((Object)function1, "transform");
        final MediatorLiveData mediatorLiveData = new MediatorLiveData();
        mediatorLiveData.addSource(liveData, (Observer<? super X>)new Transformations$sam$androidx_lifecycle_Observer.Transformations$sam$androidx_lifecycle_Observer$0((Function1)new Transformations$map.Transformations$map$1(mediatorLiveData, (Function1)function1)));
        return mediatorLiveData;
    }
    
    public static final <X, Y> LiveData<Y> switchMap(final LiveData<X> liveData, final Function1<X, LiveData<Y>> function1) {
        Intrinsics.checkNotNullParameter((Object)liveData, "<this>");
        Intrinsics.checkNotNullParameter((Object)function1, "transform");
        final MediatorLiveData mediatorLiveData = new MediatorLiveData();
        mediatorLiveData.addSource(liveData, (Observer<? super X>)new Transformations$switchMap.Transformations$switchMap$1((Function1)function1, mediatorLiveData));
        return mediatorLiveData;
    }
}

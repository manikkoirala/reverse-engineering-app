// 
// Decompiled by Procyon v0.6.0
// 

package androidx.lifecycle;

import java.util.Collection;
import kotlinx.coroutines.MainCoroutineDispatcher;
import kotlinx.coroutines.Dispatchers;
import kotlin.coroutines.CoroutineContext;
import kotlin.jvm.internal.Intrinsics;
import java.util.ArrayDeque;
import java.util.Queue;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0007\b\u0000\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\b\u0010\n\u001a\u00020\u0004H\u0007J\u0018\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\tH\u0007J\b\u0010\u0010\u001a\u00020\fH\u0007J\u0010\u0010\u0011\u001a\u00020\f2\u0006\u0010\u000f\u001a\u00020\tH\u0003J\b\u0010\u0012\u001a\u00020\fH\u0007J\b\u0010\u0013\u001a\u00020\fH\u0007J\b\u0010\u0014\u001a\u00020\fH\u0007R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004X\u0082\u000e¢\u0006\u0002\n\u0000R\u0014\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\t0\bX\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u0015" }, d2 = { "Landroidx/lifecycle/DispatchQueue;", "", "()V", "finished", "", "isDraining", "paused", "queue", "Ljava/util/Queue;", "Ljava/lang/Runnable;", "canRun", "dispatchAndEnqueue", "", "context", "Lkotlin/coroutines/CoroutineContext;", "runnable", "drainQueue", "enqueue", "finish", "pause", "resume", "lifecycle-common" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
public final class DispatchQueue
{
    private boolean finished;
    private boolean isDraining;
    private boolean paused;
    private final Queue<Runnable> queue;
    
    public DispatchQueue() {
        this.paused = true;
        this.queue = new ArrayDeque<Runnable>();
    }
    
    private static final void dispatchAndEnqueue$lambda$2$lambda$1(final DispatchQueue dispatchQueue, final Runnable runnable) {
        Intrinsics.checkNotNullParameter((Object)dispatchQueue, "this$0");
        Intrinsics.checkNotNullParameter((Object)runnable, "$runnable");
        dispatchQueue.enqueue(runnable);
    }
    
    private final void enqueue(final Runnable runnable) {
        if (this.queue.offer(runnable)) {
            this.drainQueue();
            return;
        }
        throw new IllegalStateException("cannot enqueue any more runnables".toString());
    }
    
    public final boolean canRun() {
        return this.finished || !this.paused;
    }
    
    public final void dispatchAndEnqueue(final CoroutineContext coroutineContext, final Runnable runnable) {
        Intrinsics.checkNotNullParameter((Object)coroutineContext, "context");
        Intrinsics.checkNotNullParameter((Object)runnable, "runnable");
        final MainCoroutineDispatcher immediate = Dispatchers.getMain().getImmediate();
        if (!immediate.isDispatchNeeded(coroutineContext) && !this.canRun()) {
            this.enqueue(runnable);
        }
        else {
            immediate.dispatch(coroutineContext, (Runnable)new DispatchQueue$$ExternalSyntheticLambda0(this, runnable));
        }
    }
    
    public final void drainQueue() {
        if (this.isDraining) {
            return;
        }
        try {
            this.isDraining = true;
            while ((this.queue.isEmpty() ^ true) && this.canRun()) {
                final Runnable runnable = this.queue.poll();
                if (runnable != null) {
                    runnable.run();
                }
            }
        }
        finally {
            this.isDraining = false;
        }
    }
    
    public final void finish() {
        this.finished = true;
        this.drainQueue();
    }
    
    public final void pause() {
        this.paused = true;
    }
    
    public final void resume() {
        if (!this.paused) {
            return;
        }
        if (this.finished ^ true) {
            this.paused = false;
            this.drainQueue();
            return;
        }
        throw new IllegalStateException("Cannot resume a finished dispatcher".toString());
    }
}

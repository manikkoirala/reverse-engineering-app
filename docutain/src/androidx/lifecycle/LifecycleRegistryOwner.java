// 
// Decompiled by Procyon v0.6.0
// 

package androidx.lifecycle;

@Deprecated
public interface LifecycleRegistryOwner extends LifecycleOwner
{
    LifecycleRegistry getLifecycle();
}

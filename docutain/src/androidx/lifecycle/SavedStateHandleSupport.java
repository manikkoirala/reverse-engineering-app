// 
// Decompiled by Procyon v0.6.0
// 

package androidx.lifecycle;

import kotlin.reflect.KClass;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Reflection;
import androidx.lifecycle.viewmodel.InitializerViewModelFactoryBuilder;
import androidx.savedstate.SavedStateRegistry;
import kotlin.jvm.internal.Intrinsics;
import androidx.savedstate.SavedStateRegistryOwner;
import android.os.Bundle;
import androidx.lifecycle.viewmodel.CreationExtras;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000F\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u001a*\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u00062\u0006\u0010\u0015\u001a\u00020\t2\u0006\u0010\u0016\u001a\u00020\u00042\b\u0010\u0017\u001a\u0004\u0018\u00010\u0002H\u0002\u001a\f\u0010\u0012\u001a\u00020\u0013*\u00020\u0018H\u0007\u001a\u001f\u0010\u0019\u001a\u00020\u001a\"\f\b\u0000\u0010\u001b*\u00020\u0006*\u00020\t*\u0002H\u001bH\u0007¢\u0006\u0002\u0010\u001c\"\u0016\u0010\u0000\u001a\b\u0012\u0004\u0012\u00020\u00020\u00018\u0006X\u0087\u0004¢\u0006\u0002\n\u0000\"\u000e\u0010\u0003\u001a\u00020\u0004X\u0082T¢\u0006\u0002\n\u0000\"\u0016\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00060\u00018\u0006X\u0087\u0004¢\u0006\u0002\n\u0000\"\u000e\u0010\u0007\u001a\u00020\u0004X\u0082T¢\u0006\u0002\n\u0000\"\u0016\u0010\b\u001a\b\u0012\u0004\u0012\u00020\t0\u00018\u0006X\u0087\u0004¢\u0006\u0002\n\u0000\"\u0018\u0010\n\u001a\u00020\u000b*\u00020\u00068@X\u0080\u0004¢\u0006\u0006\u001a\u0004\b\f\u0010\r\"\u0018\u0010\u000e\u001a\u00020\u000f*\u00020\t8@X\u0080\u0004¢\u0006\u0006\u001a\u0004\b\u0010\u0010\u0011¨\u0006\u001d" }, d2 = { "DEFAULT_ARGS_KEY", "Landroidx/lifecycle/viewmodel/CreationExtras$Key;", "Landroid/os/Bundle;", "SAVED_STATE_KEY", "", "SAVED_STATE_REGISTRY_OWNER_KEY", "Landroidx/savedstate/SavedStateRegistryOwner;", "VIEWMODEL_KEY", "VIEW_MODEL_STORE_OWNER_KEY", "Landroidx/lifecycle/ViewModelStoreOwner;", "savedStateHandlesProvider", "Landroidx/lifecycle/SavedStateHandlesProvider;", "getSavedStateHandlesProvider", "(Landroidx/savedstate/SavedStateRegistryOwner;)Landroidx/lifecycle/SavedStateHandlesProvider;", "savedStateHandlesVM", "Landroidx/lifecycle/SavedStateHandlesVM;", "getSavedStateHandlesVM", "(Landroidx/lifecycle/ViewModelStoreOwner;)Landroidx/lifecycle/SavedStateHandlesVM;", "createSavedStateHandle", "Landroidx/lifecycle/SavedStateHandle;", "savedStateRegistryOwner", "viewModelStoreOwner", "key", "defaultArgs", "Landroidx/lifecycle/viewmodel/CreationExtras;", "enableSavedStateHandles", "", "T", "(Landroidx/savedstate/SavedStateRegistryOwner;)V", "lifecycle-viewmodel-savedstate_release" }, k = 2, mv = { 1, 8, 0 }, xi = 48)
public final class SavedStateHandleSupport
{
    public static final CreationExtras.Key<Bundle> DEFAULT_ARGS_KEY;
    private static final String SAVED_STATE_KEY = "androidx.lifecycle.internal.SavedStateHandlesProvider";
    public static final CreationExtras.Key<SavedStateRegistryOwner> SAVED_STATE_REGISTRY_OWNER_KEY;
    private static final String VIEWMODEL_KEY = "androidx.lifecycle.internal.SavedStateHandlesVM";
    public static final CreationExtras.Key<ViewModelStoreOwner> VIEW_MODEL_STORE_OWNER_KEY;
    
    static {
        SAVED_STATE_REGISTRY_OWNER_KEY = (CreationExtras.Key)new SavedStateHandleSupport$SAVED_STATE_REGISTRY_OWNER_KEY.SavedStateHandleSupport$SAVED_STATE_REGISTRY_OWNER_KEY$1();
        VIEW_MODEL_STORE_OWNER_KEY = (CreationExtras.Key)new SavedStateHandleSupport$VIEW_MODEL_STORE_OWNER_KEY.SavedStateHandleSupport$VIEW_MODEL_STORE_OWNER_KEY$1();
        DEFAULT_ARGS_KEY = (CreationExtras.Key)new SavedStateHandleSupport$DEFAULT_ARGS_KEY.SavedStateHandleSupport$DEFAULT_ARGS_KEY$1();
    }
    
    public static final SavedStateHandle createSavedStateHandle(final CreationExtras creationExtras) {
        Intrinsics.checkNotNullParameter((Object)creationExtras, "<this>");
        final SavedStateRegistryOwner savedStateRegistryOwner = creationExtras.get(SavedStateHandleSupport.SAVED_STATE_REGISTRY_OWNER_KEY);
        if (savedStateRegistryOwner == null) {
            throw new IllegalArgumentException("CreationExtras must have a value by `SAVED_STATE_REGISTRY_OWNER_KEY`");
        }
        final ViewModelStoreOwner viewModelStoreOwner = creationExtras.get(SavedStateHandleSupport.VIEW_MODEL_STORE_OWNER_KEY);
        if (viewModelStoreOwner == null) {
            throw new IllegalArgumentException("CreationExtras must have a value by `VIEW_MODEL_STORE_OWNER_KEY`");
        }
        final Bundle bundle = creationExtras.get(SavedStateHandleSupport.DEFAULT_ARGS_KEY);
        final String s = creationExtras.get(ViewModelProvider.NewInstanceFactory.VIEW_MODEL_KEY);
        if (s != null) {
            return createSavedStateHandle(savedStateRegistryOwner, viewModelStoreOwner, s, bundle);
        }
        throw new IllegalArgumentException("CreationExtras must have a value by `VIEW_MODEL_KEY`");
    }
    
    private static final SavedStateHandle createSavedStateHandle(final SavedStateRegistryOwner savedStateRegistryOwner, final ViewModelStoreOwner viewModelStoreOwner, final String s, final Bundle bundle) {
        final SavedStateHandlesProvider savedStateHandlesProvider = getSavedStateHandlesProvider(savedStateRegistryOwner);
        final SavedStateHandlesVM savedStateHandlesVM = getSavedStateHandlesVM(viewModelStoreOwner);
        SavedStateHandle handle;
        if ((handle = savedStateHandlesVM.getHandles().get(s)) == null) {
            handle = SavedStateHandle.Companion.createHandle(savedStateHandlesProvider.consumeRestoredStateForKey(s), bundle);
            savedStateHandlesVM.getHandles().put(s, handle);
        }
        return handle;
    }
    
    public static final <T extends SavedStateRegistryOwner & ViewModelStoreOwner> void enableSavedStateHandles(final T t) {
        Intrinsics.checkNotNullParameter((Object)t, "<this>");
        final Lifecycle.State currentState = t.getLifecycle().getCurrentState();
        if (currentState == Lifecycle.State.INITIALIZED || currentState == Lifecycle.State.CREATED) {
            if (t.getSavedStateRegistry().getSavedStateProvider("androidx.lifecycle.internal.SavedStateHandlesProvider") == null) {
                final SavedStateHandlesProvider savedStateHandlesProvider = new SavedStateHandlesProvider(t.getSavedStateRegistry(), t);
                t.getSavedStateRegistry().registerSavedStateProvider("androidx.lifecycle.internal.SavedStateHandlesProvider", (SavedStateRegistry.SavedStateProvider)savedStateHandlesProvider);
                t.getLifecycle().addObserver(new SavedStateHandleAttacher(savedStateHandlesProvider));
            }
            return;
        }
        throw new IllegalArgumentException("Failed requirement.".toString());
    }
    
    public static final SavedStateHandlesProvider getSavedStateHandlesProvider(final SavedStateRegistryOwner savedStateRegistryOwner) {
        Intrinsics.checkNotNullParameter((Object)savedStateRegistryOwner, "<this>");
        final SavedStateRegistry.SavedStateProvider savedStateProvider = savedStateRegistryOwner.getSavedStateRegistry().getSavedStateProvider("androidx.lifecycle.internal.SavedStateHandlesProvider");
        SavedStateHandlesProvider savedStateHandlesProvider;
        if (savedStateProvider instanceof SavedStateHandlesProvider) {
            savedStateHandlesProvider = (SavedStateHandlesProvider)savedStateProvider;
        }
        else {
            savedStateHandlesProvider = null;
        }
        if (savedStateHandlesProvider != null) {
            return savedStateHandlesProvider;
        }
        throw new IllegalStateException("enableSavedStateHandles() wasn't called prior to createSavedStateHandle() call");
    }
    
    public static final SavedStateHandlesVM getSavedStateHandlesVM(final ViewModelStoreOwner viewModelStoreOwner) {
        Intrinsics.checkNotNullParameter((Object)viewModelStoreOwner, "<this>");
        final InitializerViewModelFactoryBuilder initializerViewModelFactoryBuilder = new InitializerViewModelFactoryBuilder();
        initializerViewModelFactoryBuilder.addInitializer((kotlin.reflect.KClass<ViewModel>)Reflection.getOrCreateKotlinClass((Class)SavedStateHandlesVM.class), (kotlin.jvm.functions.Function1<? super CreationExtras, ? extends ViewModel>)SavedStateHandleSupport$savedStateHandlesVM$1.SavedStateHandleSupport$savedStateHandlesVM$1$1.INSTANCE);
        return new ViewModelProvider(viewModelStoreOwner, initializerViewModelFactoryBuilder.build()).get("androidx.lifecycle.internal.SavedStateHandlesVM", SavedStateHandlesVM.class);
    }
}

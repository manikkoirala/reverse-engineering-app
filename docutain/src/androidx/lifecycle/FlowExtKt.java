// 
// Decompiled by Procyon v0.6.0
// 

package androidx.lifecycle;

import kotlinx.coroutines.flow.FlowKt;
import kotlin.coroutines.Continuation;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.internal.Intrinsics;
import kotlinx.coroutines.flow.Flow;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u001a.\u0010\u0000\u001a\b\u0012\u0004\u0012\u0002H\u00020\u0001\"\u0004\b\u0000\u0010\u0002*\b\u0012\u0004\u0012\u0002H\u00020\u00012\u0006\u0010\u0003\u001a\u00020\u00042\b\b\u0002\u0010\u0005\u001a\u00020\u0006¨\u0006\u0007" }, d2 = { "flowWithLifecycle", "Lkotlinx/coroutines/flow/Flow;", "T", "lifecycle", "Landroidx/lifecycle/Lifecycle;", "minActiveState", "Landroidx/lifecycle/Lifecycle$State;", "lifecycle-runtime-ktx_release" }, k = 2, mv = { 1, 8, 0 }, xi = 48)
public final class FlowExtKt
{
    public static final <T> Flow<T> flowWithLifecycle(final Flow<? extends T> flow, final Lifecycle lifecycle, final Lifecycle.State state) {
        Intrinsics.checkNotNullParameter((Object)flow, "<this>");
        Intrinsics.checkNotNullParameter((Object)lifecycle, "lifecycle");
        Intrinsics.checkNotNullParameter((Object)state, "minActiveState");
        return (Flow<T>)FlowKt.callbackFlow((Function2)new FlowExtKt$flowWithLifecycle.FlowExtKt$flowWithLifecycle$1(lifecycle, state, (Flow)flow, (Continuation)null));
    }
}

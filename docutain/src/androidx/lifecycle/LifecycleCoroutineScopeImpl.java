// 
// Decompiled by Procyon v0.6.0
// 

package androidx.lifecycle;

import kotlinx.coroutines.CoroutineStart;
import kotlinx.coroutines.BuildersKt;
import kotlin.coroutines.Continuation;
import kotlin.jvm.functions.Function2;
import kotlinx.coroutines.Dispatchers;
import kotlinx.coroutines.CoroutineScope;
import java.util.concurrent.CancellationException;
import kotlinx.coroutines.JobKt;
import kotlin.jvm.internal.Intrinsics;
import kotlin.coroutines.CoroutineContext;
import kotlin.Metadata;

@Metadata(d1 = { "\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0000\u0018\u00002\u00020\u00012\u00020\u0002B\u0015\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006¢\u0006\u0002\u0010\u0007J\u0018\u0010\f\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u0011H\u0016J\u0006\u0010\u0012\u001a\u00020\rR\u0014\u0010\u0005\u001a\u00020\u0006X\u0096\u0004¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010\tR\u0014\u0010\u0003\u001a\u00020\u0004X\u0090\u0004¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000b¨\u0006\u0013" }, d2 = { "Landroidx/lifecycle/LifecycleCoroutineScopeImpl;", "Landroidx/lifecycle/LifecycleCoroutineScope;", "Landroidx/lifecycle/LifecycleEventObserver;", "lifecycle", "Landroidx/lifecycle/Lifecycle;", "coroutineContext", "Lkotlin/coroutines/CoroutineContext;", "(Landroidx/lifecycle/Lifecycle;Lkotlin/coroutines/CoroutineContext;)V", "getCoroutineContext", "()Lkotlin/coroutines/CoroutineContext;", "getLifecycle$lifecycle_common", "()Landroidx/lifecycle/Lifecycle;", "onStateChanged", "", "source", "Landroidx/lifecycle/LifecycleOwner;", "event", "Landroidx/lifecycle/Lifecycle$Event;", "register", "lifecycle-common" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
public final class LifecycleCoroutineScopeImpl extends LifecycleCoroutineScope implements LifecycleEventObserver
{
    private final CoroutineContext coroutineContext;
    private final Lifecycle lifecycle;
    
    public LifecycleCoroutineScopeImpl(final Lifecycle lifecycle, final CoroutineContext coroutineContext) {
        Intrinsics.checkNotNullParameter((Object)lifecycle, "lifecycle");
        Intrinsics.checkNotNullParameter((Object)coroutineContext, "coroutineContext");
        this.lifecycle = lifecycle;
        this.coroutineContext = coroutineContext;
        if (this.getLifecycle$lifecycle_common().getCurrentState() == Lifecycle.State.DESTROYED) {
            JobKt.cancel$default(this.getCoroutineContext(), (CancellationException)null, 1, (Object)null);
        }
    }
    
    public CoroutineContext getCoroutineContext() {
        return this.coroutineContext;
    }
    
    @Override
    public Lifecycle getLifecycle$lifecycle_common() {
        return this.lifecycle;
    }
    
    @Override
    public void onStateChanged(final LifecycleOwner lifecycleOwner, final Lifecycle.Event event) {
        Intrinsics.checkNotNullParameter((Object)lifecycleOwner, "source");
        Intrinsics.checkNotNullParameter((Object)event, "event");
        if (this.getLifecycle$lifecycle_common().getCurrentState().compareTo((Lifecycle.State)Lifecycle.State.DESTROYED) <= 0) {
            this.getLifecycle$lifecycle_common().removeObserver(this);
            JobKt.cancel$default(this.getCoroutineContext(), (CancellationException)null, 1, (Object)null);
        }
    }
    
    public final void register() {
        BuildersKt.launch$default((CoroutineScope)this, (CoroutineContext)Dispatchers.getMain().getImmediate(), (CoroutineStart)null, (Function2)new LifecycleCoroutineScopeImpl$register.LifecycleCoroutineScopeImpl$register$1(this, (Continuation)null), 2, (Object)null);
    }
}

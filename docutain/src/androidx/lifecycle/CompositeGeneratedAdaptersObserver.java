// 
// Decompiled by Procyon v0.6.0
// 

package androidx.lifecycle;

import kotlin.jvm.internal.Intrinsics;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0011\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0000\u0018\u00002\u00020\u0001B\u0013\u0012\f\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003¢\u0006\u0002\u0010\u0005J\u0018\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\fH\u0016R\u0016\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003X\u0082\u0004¢\u0006\u0004\n\u0002\u0010\u0006¨\u0006\r" }, d2 = { "Landroidx/lifecycle/CompositeGeneratedAdaptersObserver;", "Landroidx/lifecycle/LifecycleEventObserver;", "generatedAdapters", "", "Landroidx/lifecycle/GeneratedAdapter;", "([Landroidx/lifecycle/GeneratedAdapter;)V", "[Landroidx/lifecycle/GeneratedAdapter;", "onStateChanged", "", "source", "Landroidx/lifecycle/LifecycleOwner;", "event", "Landroidx/lifecycle/Lifecycle$Event;", "lifecycle-common" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
public final class CompositeGeneratedAdaptersObserver implements LifecycleEventObserver
{
    private final GeneratedAdapter[] generatedAdapters;
    
    public CompositeGeneratedAdaptersObserver(final GeneratedAdapter[] generatedAdapters) {
        Intrinsics.checkNotNullParameter((Object)generatedAdapters, "generatedAdapters");
        this.generatedAdapters = generatedAdapters;
    }
    
    @Override
    public void onStateChanged(final LifecycleOwner lifecycleOwner, final Lifecycle.Event event) {
        Intrinsics.checkNotNullParameter((Object)lifecycleOwner, "source");
        Intrinsics.checkNotNullParameter((Object)event, "event");
        final MethodCallsLogger methodCallsLogger = new MethodCallsLogger();
        final GeneratedAdapter[] generatedAdapters = this.generatedAdapters;
        final int length = generatedAdapters.length;
        final int n = 0;
        for (int i = 0; i < length; ++i) {
            generatedAdapters[i].callMethods(lifecycleOwner, event, false, methodCallsLogger);
        }
        final GeneratedAdapter[] generatedAdapters2 = this.generatedAdapters;
        for (int length2 = generatedAdapters2.length, j = n; j < length2; ++j) {
            generatedAdapters2[j].callMethods(lifecycleOwner, event, true, methodCallsLogger);
        }
    }
}

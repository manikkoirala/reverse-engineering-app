// 
// Decompiled by Procyon v0.6.0
// 

package androidx.lifecycle;

import java.io.Serializable;
import androidx.lifecycle.viewmodel.InitializerViewModelFactory;
import java.util.Arrays;
import androidx.lifecycle.viewmodel.ViewModelInitializer;
import kotlin.jvm.JvmStatic;
import java.lang.reflect.InvocationTargetException;
import android.app.Application;
import androidx.lifecycle.viewmodel.MutableCreationExtras;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import androidx.lifecycle.viewmodel.CreationExtras;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000>\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0006\b\u0016\u0018\u00002\u00020\u0001:\u0004\u0016\u0017\u0018\u0019B\u000f\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004B\u0017\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006¢\u0006\u0002\u0010\u0007B!\b\u0007\u0012\u0006\u0010\b\u001a\u00020\t\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\b\b\u0002\u0010\n\u001a\u00020\u000b¢\u0006\u0002\u0010\fJ&\u0010\r\u001a\u0002H\u000e\"\b\b\u0000\u0010\u000e*\u00020\u000f2\f\u0010\u0010\u001a\b\u0012\u0004\u0012\u0002H\u000e0\u0011H\u0097\u0002¢\u0006\u0002\u0010\u0012J.\u0010\r\u001a\u0002H\u000e\"\b\b\u0000\u0010\u000e*\u00020\u000f2\u0006\u0010\u0013\u001a\u00020\u00142\f\u0010\u0010\u001a\b\u0012\u0004\u0012\u0002H\u000e0\u0011H\u0097\u0002¢\u0006\u0002\u0010\u0015R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u001a" }, d2 = { "Landroidx/lifecycle/ViewModelProvider;", "", "owner", "Landroidx/lifecycle/ViewModelStoreOwner;", "(Landroidx/lifecycle/ViewModelStoreOwner;)V", "factory", "Landroidx/lifecycle/ViewModelProvider$Factory;", "(Landroidx/lifecycle/ViewModelStoreOwner;Landroidx/lifecycle/ViewModelProvider$Factory;)V", "store", "Landroidx/lifecycle/ViewModelStore;", "defaultCreationExtras", "Landroidx/lifecycle/viewmodel/CreationExtras;", "(Landroidx/lifecycle/ViewModelStore;Landroidx/lifecycle/ViewModelProvider$Factory;Landroidx/lifecycle/viewmodel/CreationExtras;)V", "get", "T", "Landroidx/lifecycle/ViewModel;", "modelClass", "Ljava/lang/Class;", "(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;", "key", "", "(Ljava/lang/String;Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;", "AndroidViewModelFactory", "Factory", "NewInstanceFactory", "OnRequeryFactory", "lifecycle-viewmodel_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
public class ViewModelProvider
{
    private final CreationExtras defaultCreationExtras;
    private final Factory factory;
    private final ViewModelStore store;
    
    public ViewModelProvider(final ViewModelStore viewModelStore, final Factory factory) {
        Intrinsics.checkNotNullParameter((Object)viewModelStore, "store");
        Intrinsics.checkNotNullParameter((Object)factory, "factory");
        this(viewModelStore, factory, null, 4, null);
    }
    
    public ViewModelProvider(final ViewModelStore store, final Factory factory, final CreationExtras defaultCreationExtras) {
        Intrinsics.checkNotNullParameter((Object)store, "store");
        Intrinsics.checkNotNullParameter((Object)factory, "factory");
        Intrinsics.checkNotNullParameter((Object)defaultCreationExtras, "defaultCreationExtras");
        this.store = store;
        this.factory = factory;
        this.defaultCreationExtras = defaultCreationExtras;
    }
    
    public ViewModelProvider(final ViewModelStoreOwner viewModelStoreOwner) {
        Intrinsics.checkNotNullParameter((Object)viewModelStoreOwner, "owner");
        this(viewModelStoreOwner.getViewModelStore(), AndroidViewModelFactory.Companion.defaultFactory$lifecycle_viewmodel_release(viewModelStoreOwner), ViewModelProviderGetKt.defaultCreationExtras(viewModelStoreOwner));
    }
    
    public ViewModelProvider(final ViewModelStoreOwner viewModelStoreOwner, final Factory factory) {
        Intrinsics.checkNotNullParameter((Object)viewModelStoreOwner, "owner");
        Intrinsics.checkNotNullParameter((Object)factory, "factory");
        this(viewModelStoreOwner.getViewModelStore(), factory, ViewModelProviderGetKt.defaultCreationExtras(viewModelStoreOwner));
    }
    
    public <T extends ViewModel> T get(final Class<T> clazz) {
        Intrinsics.checkNotNullParameter((Object)clazz, "modelClass");
        final String canonicalName = clazz.getCanonicalName();
        if (canonicalName != null) {
            final StringBuilder sb = new StringBuilder();
            sb.append("androidx.lifecycle.ViewModelProvider.DefaultKey:");
            sb.append(canonicalName);
            return this.get(sb.toString(), clazz);
        }
        throw new IllegalArgumentException("Local and anonymous classes can not be ViewModels");
    }
    
    public <T extends ViewModel> T get(final String s, Class<T> o) {
        Intrinsics.checkNotNullParameter((Object)s, "key");
        Intrinsics.checkNotNullParameter(o, "modelClass");
        final ViewModel value = this.store.get(s);
        if (((Class)o).isInstance(value)) {
            final Factory factory = this.factory;
            OnRequeryFactory onRequeryFactory;
            if (factory instanceof OnRequeryFactory) {
                onRequeryFactory = (OnRequeryFactory)factory;
            }
            else {
                onRequeryFactory = null;
            }
            if (onRequeryFactory != null) {
                Intrinsics.checkNotNull((Object)value);
                onRequeryFactory.onRequery(value);
            }
            Intrinsics.checkNotNull((Object)value, "null cannot be cast to non-null type T of androidx.lifecycle.ViewModelProvider.get");
            return (T)value;
        }
        final MutableCreationExtras mutableCreationExtras = new MutableCreationExtras(this.defaultCreationExtras);
        mutableCreationExtras.set(NewInstanceFactory.VIEW_MODEL_KEY, s);
        try {
            o = this.factory.create((Class<ViewModel>)o, mutableCreationExtras);
        }
        catch (final AbstractMethodError abstractMethodError) {
            o = this.factory.create((Class<Class>)o);
        }
        this.store.put(s, (ViewModel)o);
        return (T)o;
    }
    
    @Metadata(d1 = { "\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\b\u0016\u0018\u0000 \u00142\u00020\u0001:\u0001\u0014B\u0007\b\u0016¢\u0006\u0002\u0010\u0002B\u000f\b\u0016\u0012\u0006\u0010\u0003\u001a\u00020\u0004¢\u0006\u0002\u0010\u0005B\u0019\b\u0002\u0012\b\u0010\u0003\u001a\u0004\u0018\u00010\u0004\u0012\u0006\u0010\u0006\u001a\u00020\u0007¢\u0006\u0002\u0010\bJ%\u0010\t\u001a\u0002H\n\"\b\b\u0000\u0010\n*\u00020\u000b2\f\u0010\f\u001a\b\u0012\u0004\u0012\u0002H\n0\rH\u0016¢\u0006\u0002\u0010\u000eJ-\u0010\t\u001a\u0002H\n\"\b\b\u0000\u0010\n*\u00020\u000b2\f\u0010\f\u001a\b\u0012\u0004\u0012\u0002H\n0\r2\u0006\u0010\u000f\u001a\u00020\u0004H\u0002¢\u0006\u0002\u0010\u0010J-\u0010\t\u001a\u0002H\n\"\b\b\u0000\u0010\n*\u00020\u000b2\f\u0010\f\u001a\b\u0012\u0004\u0012\u0002H\n0\r2\u0006\u0010\u0011\u001a\u00020\u0012H\u0016¢\u0006\u0002\u0010\u0013R\u0010\u0010\u0003\u001a\u0004\u0018\u00010\u0004X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u0015" }, d2 = { "Landroidx/lifecycle/ViewModelProvider$AndroidViewModelFactory;", "Landroidx/lifecycle/ViewModelProvider$NewInstanceFactory;", "()V", "application", "Landroid/app/Application;", "(Landroid/app/Application;)V", "unused", "", "(Landroid/app/Application;I)V", "create", "T", "Landroidx/lifecycle/ViewModel;", "modelClass", "Ljava/lang/Class;", "(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;", "app", "(Ljava/lang/Class;Landroid/app/Application;)Landroidx/lifecycle/ViewModel;", "extras", "Landroidx/lifecycle/viewmodel/CreationExtras;", "(Ljava/lang/Class;Landroidx/lifecycle/viewmodel/CreationExtras;)Landroidx/lifecycle/ViewModel;", "Companion", "lifecycle-viewmodel_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
    public static class AndroidViewModelFactory extends NewInstanceFactory
    {
        public static final CreationExtras.Key<Application> APPLICATION_KEY;
        public static final Companion Companion;
        public static final String DEFAULT_KEY = "androidx.lifecycle.ViewModelProvider.DefaultKey";
        private static AndroidViewModelFactory sInstance;
        private final Application application;
        
        static {
            Companion = new Companion(null);
            APPLICATION_KEY = ApplicationKeyImpl.INSTANCE;
        }
        
        public AndroidViewModelFactory() {
            this(null, 0);
        }
        
        public AndroidViewModelFactory(final Application application) {
            Intrinsics.checkNotNullParameter((Object)application, "application");
            this(application, 0);
        }
        
        private AndroidViewModelFactory(final Application application, final int n) {
            this.application = application;
        }
        
        public static final /* synthetic */ AndroidViewModelFactory access$getSInstance$cp() {
            return AndroidViewModelFactory.sInstance;
        }
        
        public static final /* synthetic */ void access$setSInstance$cp(final AndroidViewModelFactory sInstance) {
            AndroidViewModelFactory.sInstance = sInstance;
        }
        
        private final <T extends ViewModel> T create(Class<T> create, final Application application) {
            if (AndroidViewModel.class.isAssignableFrom((Class<?>)create)) {
                try {
                    final ViewModel viewModel = (T)((Class<Class<Class<?>>>)create).getConstructor(Application.class).newInstance(application);
                    Intrinsics.checkNotNullExpressionValue((Object)viewModel, "{\n                try {\n\u2026          }\n            }");
                    create = (Serializable)viewModel;
                    return (T)create;
                }
                catch (final InvocationTargetException ex) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Cannot create an instance of ");
                    sb.append(create);
                    throw new RuntimeException(sb.toString(), ex);
                }
                catch (final InstantiationException ex2) {
                    final StringBuilder sb2 = new StringBuilder();
                    sb2.append("Cannot create an instance of ");
                    sb2.append(create);
                    throw new RuntimeException(sb2.toString(), ex2);
                }
                catch (final IllegalAccessException ex3) {
                    final StringBuilder sb3 = new StringBuilder();
                    sb3.append("Cannot create an instance of ");
                    sb3.append(create);
                    throw new RuntimeException(sb3.toString(), ex3);
                }
                catch (final NoSuchMethodException ex4) {
                    final StringBuilder sb4 = new StringBuilder();
                    sb4.append("Cannot create an instance of ");
                    sb4.append(create);
                    throw new RuntimeException(sb4.toString(), ex4);
                }
            }
            create = super.create((Class<Class<Class<?>>>)create);
            return (T)create;
        }
        
        @JvmStatic
        public static final AndroidViewModelFactory getInstance(final Application application) {
            return AndroidViewModelFactory.Companion.getInstance(application);
        }
        
        @Override
        public <T extends ViewModel> T create(final Class<T> clazz) {
            Intrinsics.checkNotNullParameter((Object)clazz, "modelClass");
            final Application application = this.application;
            if (application != null) {
                return this.create(clazz, application);
            }
            throw new UnsupportedOperationException("AndroidViewModelFactory constructed with empty constructor works only with create(modelClass: Class<T>, extras: CreationExtras).");
        }
        
        @Override
        public <T extends ViewModel> T create(final Class<T> clazz, final CreationExtras creationExtras) {
            Intrinsics.checkNotNullParameter((Object)clazz, "modelClass");
            Intrinsics.checkNotNullParameter((Object)creationExtras, "extras");
            ViewModel viewModel;
            if (this.application != null) {
                viewModel = this.create(clazz);
            }
            else {
                final Application application = creationExtras.get(AndroidViewModelFactory.APPLICATION_KEY);
                if (application != null) {
                    viewModel = this.create(clazz, application);
                }
                else {
                    if (AndroidViewModel.class.isAssignableFrom(clazz)) {
                        throw new IllegalArgumentException("CreationExtras must have an application by `APPLICATION_KEY`");
                    }
                    viewModel = super.create(clazz);
                }
            }
            return (T)viewModel;
        }
        
        @Metadata(d1 = { "\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001:\u0001\u0011B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0015\u0010\n\u001a\u00020\u000b2\u0006\u0010\f\u001a\u00020\rH\u0000¢\u0006\u0002\b\u000eJ\u0010\u0010\u000f\u001a\u00020\t2\u0006\u0010\u0010\u001a\u00020\u0005H\u0007R\u0016\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u00048\u0006X\u0087\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0080T¢\u0006\u0002\n\u0000R\u0010\u0010\b\u001a\u0004\u0018\u00010\tX\u0082\u000e¢\u0006\u0002\n\u0000¨\u0006\u0012" }, d2 = { "Landroidx/lifecycle/ViewModelProvider$AndroidViewModelFactory$Companion;", "", "()V", "APPLICATION_KEY", "Landroidx/lifecycle/viewmodel/CreationExtras$Key;", "Landroid/app/Application;", "DEFAULT_KEY", "", "sInstance", "Landroidx/lifecycle/ViewModelProvider$AndroidViewModelFactory;", "defaultFactory", "Landroidx/lifecycle/ViewModelProvider$Factory;", "owner", "Landroidx/lifecycle/ViewModelStoreOwner;", "defaultFactory$lifecycle_viewmodel_release", "getInstance", "application", "ApplicationKeyImpl", "lifecycle-viewmodel_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
        public static final class Companion
        {
            private Companion() {
            }
            
            public final Factory defaultFactory$lifecycle_viewmodel_release(final ViewModelStoreOwner viewModelStoreOwner) {
                Intrinsics.checkNotNullParameter((Object)viewModelStoreOwner, "owner");
                Factory defaultViewModelProviderFactory;
                if (viewModelStoreOwner instanceof HasDefaultViewModelProviderFactory) {
                    defaultViewModelProviderFactory = ((HasDefaultViewModelProviderFactory)viewModelStoreOwner).getDefaultViewModelProviderFactory();
                }
                else {
                    defaultViewModelProviderFactory = NewInstanceFactory.Companion.getInstance();
                }
                return defaultViewModelProviderFactory;
            }
            
            @JvmStatic
            public final AndroidViewModelFactory getInstance(final Application application) {
                Intrinsics.checkNotNullParameter((Object)application, "application");
                if (AndroidViewModelFactory.access$getSInstance$cp() == null) {
                    AndroidViewModelFactory.access$setSInstance$cp(new AndroidViewModelFactory(application));
                }
                final AndroidViewModelFactory access$getSInstance$cp = AndroidViewModelFactory.access$getSInstance$cp();
                Intrinsics.checkNotNull((Object)access$getSInstance$cp);
                return access$getSInstance$cp;
            }
            
            @Metadata(d1 = { "\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u00c2\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0003¨\u0006\u0004" }, d2 = { "Landroidx/lifecycle/ViewModelProvider$AndroidViewModelFactory$Companion$ApplicationKeyImpl;", "Landroidx/lifecycle/viewmodel/CreationExtras$Key;", "Landroid/app/Application;", "()V", "lifecycle-viewmodel_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
            private static final class ApplicationKeyImpl implements Key<Application>
            {
                public static final ApplicationKeyImpl INSTANCE;
                
                static {
                    INSTANCE = new ApplicationKeyImpl();
                }
            }
        }
    }
    
    @Metadata(d1 = { "\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\b\u0016\u0018\u0000 \t2\u00020\u0001:\u0001\tB\u0005¢\u0006\u0002\u0010\u0002J%\u0010\u0003\u001a\u0002H\u0004\"\b\b\u0000\u0010\u0004*\u00020\u00052\f\u0010\u0006\u001a\b\u0012\u0004\u0012\u0002H\u00040\u0007H\u0016¢\u0006\u0002\u0010\b¨\u0006\n" }, d2 = { "Landroidx/lifecycle/ViewModelProvider$NewInstanceFactory;", "Landroidx/lifecycle/ViewModelProvider$Factory;", "()V", "create", "T", "Landroidx/lifecycle/ViewModel;", "modelClass", "Ljava/lang/Class;", "(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;", "Companion", "lifecycle-viewmodel_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
    public static class NewInstanceFactory implements Factory
    {
        public static final Companion Companion;
        public static final CreationExtras.Key<String> VIEW_MODEL_KEY;
        private static NewInstanceFactory sInstance;
        
        static {
            Companion = new Companion(null);
            VIEW_MODEL_KEY = ViewModelKeyImpl.INSTANCE;
        }
        
        public static final /* synthetic */ NewInstanceFactory access$getSInstance$cp() {
            return NewInstanceFactory.sInstance;
        }
        
        public static final /* synthetic */ void access$setSInstance$cp(final NewInstanceFactory sInstance) {
            NewInstanceFactory.sInstance = sInstance;
        }
        
        public static final NewInstanceFactory getInstance() {
            return NewInstanceFactory.Companion.getInstance();
        }
        
        @Override
        public <T extends ViewModel> T create(final Class<T> obj) {
            Intrinsics.checkNotNullParameter((Object)obj, "modelClass");
            try {
                final ViewModel instance = obj.getDeclaredConstructor((Class<?>[])new Class[0]).newInstance(new Object[0]);
                Intrinsics.checkNotNullExpressionValue((Object)instance, "{\n                modelC\u2026wInstance()\n            }");
                return (T)instance;
            }
            catch (final IllegalAccessException ex) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Cannot create an instance of ");
                sb.append(obj);
                throw new RuntimeException(sb.toString(), ex);
            }
            catch (final InstantiationException ex2) {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("Cannot create an instance of ");
                sb2.append(obj);
                throw new RuntimeException(sb2.toString(), ex2);
            }
            catch (final NoSuchMethodException ex3) {
                final StringBuilder sb3 = new StringBuilder();
                sb3.append("Cannot create an instance of ");
                sb3.append(obj);
                throw new RuntimeException(sb3.toString(), ex3);
            }
        }
        
        @Metadata(d1 = { "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\b\u0086\u0003\u0018\u00002\u00020\u0001:\u0001\fB\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u0016\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u00048\u0006X\u0087\u0004¢\u0006\u0002\n\u0000R\u001a\u0010\u0006\u001a\u00020\u00078GX\u0087\u0004¢\u0006\f\u0012\u0004\b\b\u0010\u0002\u001a\u0004\b\t\u0010\nR\u0010\u0010\u000b\u001a\u0004\u0018\u00010\u0007X\u0082\u000e¢\u0006\u0002\n\u0000¨\u0006\r" }, d2 = { "Landroidx/lifecycle/ViewModelProvider$NewInstanceFactory$Companion;", "", "()V", "VIEW_MODEL_KEY", "Landroidx/lifecycle/viewmodel/CreationExtras$Key;", "", "instance", "Landroidx/lifecycle/ViewModelProvider$NewInstanceFactory;", "getInstance$annotations", "getInstance", "()Landroidx/lifecycle/ViewModelProvider$NewInstanceFactory;", "sInstance", "ViewModelKeyImpl", "lifecycle-viewmodel_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
        public static final class Companion
        {
            private Companion() {
            }
            
            public final NewInstanceFactory getInstance() {
                if (NewInstanceFactory.access$getSInstance$cp() == null) {
                    NewInstanceFactory.access$setSInstance$cp(new NewInstanceFactory());
                }
                final NewInstanceFactory access$getSInstance$cp = NewInstanceFactory.access$getSInstance$cp();
                Intrinsics.checkNotNull((Object)access$getSInstance$cp);
                return access$getSInstance$cp;
            }
            
            @Metadata(d1 = { "\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\b\u00c2\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0003¨\u0006\u0004" }, d2 = { "Landroidx/lifecycle/ViewModelProvider$NewInstanceFactory$Companion$ViewModelKeyImpl;", "Landroidx/lifecycle/viewmodel/CreationExtras$Key;", "", "()V", "lifecycle-viewmodel_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
            private static final class ViewModelKeyImpl implements Key<String>
            {
                public static final ViewModelKeyImpl INSTANCE;
                
                static {
                    INSTANCE = new ViewModelKeyImpl();
                }
            }
        }
    }
    
    @Metadata(d1 = { "\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bf\u0018\u0000 \u000b2\u00020\u0001:\u0001\u000bJ%\u0010\u0002\u001a\u0002H\u0003\"\b\b\u0000\u0010\u0003*\u00020\u00042\f\u0010\u0005\u001a\b\u0012\u0004\u0012\u0002H\u00030\u0006H\u0016¢\u0006\u0002\u0010\u0007J-\u0010\u0002\u001a\u0002H\u0003\"\b\b\u0000\u0010\u0003*\u00020\u00042\f\u0010\u0005\u001a\b\u0012\u0004\u0012\u0002H\u00030\u00062\u0006\u0010\b\u001a\u00020\tH\u0016¢\u0006\u0002\u0010\n\u00f8\u0001\u0000\u0082\u0002\u0006\n\u0004\b!0\u0001¨\u0006\f\u00c0\u0006\u0001" }, d2 = { "Landroidx/lifecycle/ViewModelProvider$Factory;", "", "create", "T", "Landroidx/lifecycle/ViewModel;", "modelClass", "Ljava/lang/Class;", "(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;", "extras", "Landroidx/lifecycle/viewmodel/CreationExtras;", "(Ljava/lang/Class;Landroidx/lifecycle/viewmodel/CreationExtras;)Landroidx/lifecycle/ViewModel;", "Companion", "lifecycle-viewmodel_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
    public interface Factory
    {
        public static final Companion Companion = Factory.Companion.$$INSTANCE;
        
         <T extends ViewModel> T create(final Class<T> p0);
        
         <T extends ViewModel> T create(final Class<T> p0, final CreationExtras p1);
        
        @Metadata(d1 = { "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0011\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J)\u0010\u0003\u001a\u00020\u00042\u001a\u0010\u0005\u001a\u000e\u0012\n\b\u0001\u0012\u0006\u0012\u0002\b\u00030\u00070\u0006\"\u0006\u0012\u0002\b\u00030\u0007H\u0007¢\u0006\u0002\u0010\b¨\u0006\t" }, d2 = { "Landroidx/lifecycle/ViewModelProvider$Factory$Companion;", "", "()V", "from", "Landroidx/lifecycle/ViewModelProvider$Factory;", "initializers", "", "Landroidx/lifecycle/viewmodel/ViewModelInitializer;", "([Landroidx/lifecycle/viewmodel/ViewModelInitializer;)Landroidx/lifecycle/ViewModelProvider$Factory;", "lifecycle-viewmodel_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
        public static final class Companion
        {
            static final Companion $$INSTANCE;
            
            static {
                $$INSTANCE = new Companion();
            }
            
            private Companion() {
            }
            
            @JvmStatic
            public final Factory from(final ViewModelInitializer<?>... original) {
                Intrinsics.checkNotNullParameter((Object)original, "initializers");
                return (Factory)new InitializerViewModelFactory((ViewModelInitializer<?>[])Arrays.copyOf(original, original.length));
            }
        }
    }
    
    @Metadata(d1 = { "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0017\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0016¨\u0006\u0007" }, d2 = { "Landroidx/lifecycle/ViewModelProvider$OnRequeryFactory;", "", "()V", "onRequery", "", "viewModel", "Landroidx/lifecycle/ViewModel;", "lifecycle-viewmodel_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
    public static class OnRequeryFactory
    {
        public void onRequery(final ViewModel viewModel) {
            Intrinsics.checkNotNullParameter((Object)viewModel, "viewModel");
        }
    }
}

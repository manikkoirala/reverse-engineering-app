// 
// Decompiled by Procyon v0.6.0
// 

package androidx.lifecycle;

import kotlin.Metadata;

@Metadata(d1 = { "\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\bg\u0018\u00002\u00020\u0001J*\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u00072\u0006\u0010\b\u001a\u00020\t2\b\u0010\n\u001a\u0004\u0018\u00010\u000bH&\u00f8\u0001\u0000\u0082\u0002\u0006\n\u0004\b!0\u0001¨\u0006\f\u00c0\u0006\u0001" }, d2 = { "Landroidx/lifecycle/GeneratedAdapter;", "", "callMethods", "", "source", "Landroidx/lifecycle/LifecycleOwner;", "event", "Landroidx/lifecycle/Lifecycle$Event;", "onAny", "", "logger", "Landroidx/lifecycle/MethodCallsLogger;", "lifecycle-common" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
public interface GeneratedAdapter
{
    void callMethods(final LifecycleOwner p0, final Lifecycle.Event p1, final boolean p2, final MethodCallsLogger p3);
}

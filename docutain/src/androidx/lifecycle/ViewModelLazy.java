// 
// Decompiled by Procyon v0.6.0
// 

package androidx.lifecycle;

import kotlin.jvm.JvmClassMappingKt;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import kotlin.reflect.KClass;
import androidx.lifecycle.viewmodel.CreationExtras;
import kotlin.jvm.functions.Function0;
import kotlin.Metadata;
import kotlin.Lazy;

@Metadata(d1 = { "\u00004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\u000b\n\u0000\u0018\u0000*\b\b\u0000\u0010\u0001*\u00020\u00022\b\u0012\u0004\u0012\u0002H\u00010\u0003BA\b\u0007\u0012\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00028\u00000\u0005\u0012\f\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\b0\u0007\u0012\f\u0010\t\u001a\b\u0012\u0004\u0012\u00020\n0\u0007\u0012\u000e\b\u0002\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\f0\u0007¢\u0006\u0002\u0010\rJ\b\u0010\u0013\u001a\u00020\u0014H\u0016R\u0012\u0010\u000e\u001a\u0004\u0018\u00018\u0000X\u0082\u000e¢\u0006\u0004\n\u0002\u0010\u000fR\u0014\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\f0\u0007X\u0082\u0004¢\u0006\u0002\n\u0000R\u0014\u0010\t\u001a\b\u0012\u0004\u0012\u00020\n0\u0007X\u0082\u0004¢\u0006\u0002\n\u0000R\u0014\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\b0\u0007X\u0082\u0004¢\u0006\u0002\n\u0000R\u0014\u0010\u0010\u001a\u00028\u00008VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u0011\u0010\u0012R\u0014\u0010\u0004\u001a\b\u0012\u0004\u0012\u00028\u00000\u0005X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u0015" }, d2 = { "Landroidx/lifecycle/ViewModelLazy;", "VM", "Landroidx/lifecycle/ViewModel;", "Lkotlin/Lazy;", "viewModelClass", "Lkotlin/reflect/KClass;", "storeProducer", "Lkotlin/Function0;", "Landroidx/lifecycle/ViewModelStore;", "factoryProducer", "Landroidx/lifecycle/ViewModelProvider$Factory;", "extrasProducer", "Landroidx/lifecycle/viewmodel/CreationExtras;", "(Lkotlin/reflect/KClass;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)V", "cached", "Landroidx/lifecycle/ViewModel;", "value", "getValue", "()Landroidx/lifecycle/ViewModel;", "isInitialized", "", "lifecycle-viewmodel_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
public final class ViewModelLazy<VM extends ViewModel> implements Lazy<VM>
{
    private VM cached;
    private final Function0<CreationExtras> extrasProducer;
    private final Function0<ViewModelProvider.Factory> factoryProducer;
    private final Function0<ViewModelStore> storeProducer;
    private final KClass<VM> viewModelClass;
    
    public ViewModelLazy(final KClass<VM> kClass, final Function0<? extends ViewModelStore> function0, final Function0<? extends ViewModelProvider.Factory> function2) {
        Intrinsics.checkNotNullParameter((Object)kClass, "viewModelClass");
        Intrinsics.checkNotNullParameter((Object)function0, "storeProducer");
        Intrinsics.checkNotNullParameter((Object)function2, "factoryProducer");
        this(kClass, function0, function2, null, 8, null);
    }
    
    public ViewModelLazy(final KClass<VM> viewModelClass, final Function0<? extends ViewModelStore> storeProducer, final Function0<? extends ViewModelProvider.Factory> factoryProducer, final Function0<? extends CreationExtras> extrasProducer) {
        Intrinsics.checkNotNullParameter((Object)viewModelClass, "viewModelClass");
        Intrinsics.checkNotNullParameter((Object)storeProducer, "storeProducer");
        Intrinsics.checkNotNullParameter((Object)factoryProducer, "factoryProducer");
        Intrinsics.checkNotNullParameter((Object)extrasProducer, "extrasProducer");
        this.viewModelClass = viewModelClass;
        this.storeProducer = (Function0<ViewModelStore>)storeProducer;
        this.factoryProducer = (Function0<ViewModelProvider.Factory>)factoryProducer;
        this.extrasProducer = (Function0<CreationExtras>)extrasProducer;
    }
    
    public VM getValue() {
        ViewModel cached;
        if ((cached = this.cached) == null) {
            cached = new ViewModelProvider((ViewModelStore)this.storeProducer.invoke(), (ViewModelProvider.Factory)this.factoryProducer.invoke(), (CreationExtras)this.extrasProducer.invoke()).get((Class<VM>)JvmClassMappingKt.getJavaClass((KClass)this.viewModelClass));
            this.cached = (VM)cached;
        }
        return (VM)cached;
    }
    
    public boolean isInitialized() {
        return this.cached != null;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.lifecycle;

import java.util.concurrent.CancellationException;
import kotlinx.coroutines.Job$DefaultImpls;
import kotlin.jvm.internal.Intrinsics;
import kotlinx.coroutines.Job;
import kotlin.Metadata;

@Metadata(d1 = { "\u00002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\b\u0001\u0018\u00002\u00020\u0001B%\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\t¢\u0006\u0002\u0010\nJ\b\u0010\r\u001a\u00020\u000eH\u0007J\u0011\u0010\u000f\u001a\u00020\u000e2\u0006\u0010\b\u001a\u00020\tH\u0082\bR\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\fX\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u0010" }, d2 = { "Landroidx/lifecycle/LifecycleController;", "", "lifecycle", "Landroidx/lifecycle/Lifecycle;", "minState", "Landroidx/lifecycle/Lifecycle$State;", "dispatchQueue", "Landroidx/lifecycle/DispatchQueue;", "parentJob", "Lkotlinx/coroutines/Job;", "(Landroidx/lifecycle/Lifecycle;Landroidx/lifecycle/Lifecycle$State;Landroidx/lifecycle/DispatchQueue;Lkotlinx/coroutines/Job;)V", "observer", "Landroidx/lifecycle/LifecycleEventObserver;", "finish", "", "handleDestroy", "lifecycle-common" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
public final class LifecycleController
{
    private final DispatchQueue dispatchQueue;
    private final Lifecycle lifecycle;
    private final Lifecycle.State minState;
    private final LifecycleEventObserver observer;
    
    public LifecycleController(final Lifecycle lifecycle, final Lifecycle.State minState, final DispatchQueue dispatchQueue, final Job job) {
        Intrinsics.checkNotNullParameter((Object)lifecycle, "lifecycle");
        Intrinsics.checkNotNullParameter((Object)minState, "minState");
        Intrinsics.checkNotNullParameter((Object)dispatchQueue, "dispatchQueue");
        Intrinsics.checkNotNullParameter((Object)job, "parentJob");
        this.lifecycle = lifecycle;
        this.minState = minState;
        this.dispatchQueue = dispatchQueue;
        final LifecycleController$$ExternalSyntheticLambda0 observer = new LifecycleController$$ExternalSyntheticLambda0(this, job);
        this.observer = observer;
        if (lifecycle.getCurrentState() == Lifecycle.State.DESTROYED) {
            Job$DefaultImpls.cancel$default(job, (CancellationException)null, 1, (Object)null);
            this.finish();
        }
        else {
            lifecycle.addObserver(observer);
        }
    }
    
    private final void handleDestroy(final Job job) {
        Job$DefaultImpls.cancel$default(job, (CancellationException)null, 1, (Object)null);
        this.finish();
    }
    
    private static final void observer$lambda$0(final LifecycleController lifecycleController, final Job job, final LifecycleOwner lifecycleOwner, final Lifecycle.Event event) {
        Intrinsics.checkNotNullParameter((Object)lifecycleController, "this$0");
        Intrinsics.checkNotNullParameter((Object)job, "$parentJob");
        Intrinsics.checkNotNullParameter((Object)lifecycleOwner, "source");
        Intrinsics.checkNotNullParameter((Object)event, "<anonymous parameter 1>");
        if (lifecycleOwner.getLifecycle().getCurrentState() == Lifecycle.State.DESTROYED) {
            Job$DefaultImpls.cancel$default(job, (CancellationException)null, 1, (Object)null);
            lifecycleController.finish();
        }
        else if (lifecycleOwner.getLifecycle().getCurrentState().compareTo((Lifecycle.State)lifecycleController.minState) < 0) {
            lifecycleController.dispatchQueue.pause();
        }
        else {
            lifecycleController.dispatchQueue.resume();
        }
    }
    
    public final void finish() {
        this.lifecycle.removeObserver(this.observer);
        this.dispatchQueue.finish();
    }
}

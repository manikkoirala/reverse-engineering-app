// 
// Decompiled by Procyon v0.6.0
// 

package androidx.lifecycle;

import kotlin.Deprecated;
import androidx.arch.core.internal.SafeIterableMap;
import androidx.arch.core.executor.ArchTaskExecutor;
import kotlin.jvm.JvmStatic;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.DefaultConstructorMarker;
import java.util.ArrayList;
import androidx.arch.core.internal.FastSafeIterableMap;
import java.lang.ref.WeakReference;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000`\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\n\b\u0016\u0018\u0000 42\u00020\u0001:\u000245B\u000f\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004B\u0017\b\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006¢\u0006\u0002\u0010\u0007J\u0010\u0010!\u001a\u00020\"2\u0006\u0010#\u001a\u00020\u001cH\u0016J\u0010\u0010$\u001a\u00020\"2\u0006\u0010\u0014\u001a\u00020\u0003H\u0002J\u0010\u0010%\u001a\u00020\u000b2\u0006\u0010#\u001a\u00020\u001cH\u0002J\u0010\u0010&\u001a\u00020\"2\u0006\u0010'\u001a\u00020(H\u0003J\u0010\u0010)\u001a\u00020\"2\u0006\u0010\u0014\u001a\u00020\u0003H\u0002J\u0010\u0010*\u001a\u00020\"2\u0006\u0010+\u001a\u00020,H\u0016J\u0010\u0010-\u001a\u00020\"2\u0006\u0010\n\u001a\u00020\u000bH\u0017J\u0010\u0010.\u001a\u00020\"2\u0006\u0010/\u001a\u00020\u000bH\u0002J\b\u00100\u001a\u00020\"H\u0002J\u0010\u00101\u001a\u00020\"2\u0006\u0010\n\u001a\u00020\u000bH\u0002J\u0010\u00102\u001a\u00020\"2\u0006\u0010#\u001a\u00020\u001cH\u0016J\b\u00103\u001a\u00020\"H\u0002R\u000e\u0010\b\u001a\u00020\tX\u0082\u000e¢\u0006\u0002\n\u0000R$\u0010\f\u001a\u00020\u000b2\u0006\u0010\n\u001a\u00020\u000b8V@VX\u0096\u000e¢\u0006\f\u001a\u0004\b\r\u0010\u000e\"\u0004\b\u000f\u0010\u0010R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0006X\u0082\u000e¢\u0006\u0002\n\u0000R\u0014\u0010\u0012\u001a\u00020\u00068BX\u0082\u0004¢\u0006\u0006\u001a\u0004\b\u0012\u0010\u0013R\u0014\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\u00030\u0015X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0016\u001a\u00020\u0006X\u0082\u000e¢\u0006\u0002\n\u0000R\u0014\u0010\u0017\u001a\u00020\t8VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u0018\u0010\u0019R\u001a\u0010\u001a\u001a\u000e\u0012\u0004\u0012\u00020\u001c\u0012\u0004\u0012\u00020\u001d0\u001bX\u0082\u000e¢\u0006\u0002\n\u0000R\u001e\u0010\u001e\u001a\u0012\u0012\u0004\u0012\u00020\u000b0\u001fj\b\u0012\u0004\u0012\u00020\u000b` X\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u000e¢\u0006\u0002\n\u0000¨\u00066" }, d2 = { "Landroidx/lifecycle/LifecycleRegistry;", "Landroidx/lifecycle/Lifecycle;", "provider", "Landroidx/lifecycle/LifecycleOwner;", "(Landroidx/lifecycle/LifecycleOwner;)V", "enforceMainThread", "", "(Landroidx/lifecycle/LifecycleOwner;Z)V", "addingObserverCounter", "", "state", "Landroidx/lifecycle/Lifecycle$State;", "currentState", "getCurrentState", "()Landroidx/lifecycle/Lifecycle$State;", "setCurrentState", "(Landroidx/lifecycle/Lifecycle$State;)V", "handlingEvent", "isSynced", "()Z", "lifecycleOwner", "Ljava/lang/ref/WeakReference;", "newEventOccurred", "observerCount", "getObserverCount", "()I", "observerMap", "Landroidx/arch/core/internal/FastSafeIterableMap;", "Landroidx/lifecycle/LifecycleObserver;", "Landroidx/lifecycle/LifecycleRegistry$ObserverWithState;", "parentStates", "Ljava/util/ArrayList;", "Lkotlin/collections/ArrayList;", "addObserver", "", "observer", "backwardPass", "calculateTargetState", "enforceMainThreadIfNeeded", "methodName", "", "forwardPass", "handleLifecycleEvent", "event", "Landroidx/lifecycle/Lifecycle$Event;", "markState", "moveToState", "next", "popParentState", "pushParentState", "removeObserver", "sync", "Companion", "ObserverWithState", "lifecycle-runtime_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
public class LifecycleRegistry extends Lifecycle
{
    public static final Companion Companion;
    private int addingObserverCounter;
    private final boolean enforceMainThread;
    private boolean handlingEvent;
    private final WeakReference<LifecycleOwner> lifecycleOwner;
    private boolean newEventOccurred;
    private FastSafeIterableMap<LifecycleObserver, ObserverWithState> observerMap;
    private ArrayList<State> parentStates;
    private State state;
    
    static {
        Companion = new Companion(null);
    }
    
    public LifecycleRegistry(final LifecycleOwner lifecycleOwner) {
        Intrinsics.checkNotNullParameter((Object)lifecycleOwner, "provider");
        this(lifecycleOwner, true);
    }
    
    private LifecycleRegistry(final LifecycleOwner referent, final boolean enforceMainThread) {
        this.enforceMainThread = enforceMainThread;
        this.observerMap = new FastSafeIterableMap<LifecycleObserver, ObserverWithState>();
        this.state = State.INITIALIZED;
        this.parentStates = new ArrayList<State>();
        this.lifecycleOwner = new WeakReference<LifecycleOwner>(referent);
    }
    
    private final void backwardPass(final LifecycleOwner lifecycleOwner) {
        final Iterator<Map.Entry<Object, Object>> descendingIterator = this.observerMap.descendingIterator();
        Intrinsics.checkNotNullExpressionValue((Object)descendingIterator, "observerMap.descendingIterator()");
        while (descendingIterator.hasNext() && !this.newEventOccurred) {
            final Map.Entry<LifecycleObserver, V> entry = descendingIterator.next();
            Intrinsics.checkNotNullExpressionValue((Object)entry, "next()");
            final LifecycleObserver lifecycleObserver = entry.getKey();
            final ObserverWithState observerWithState = (ObserverWithState)entry.getValue();
            while (observerWithState.getState().compareTo((State)this.state) > 0 && !this.newEventOccurred && this.observerMap.contains(lifecycleObserver)) {
                final Event down = Event.Companion.downFrom(observerWithState.getState());
                if (down == null) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("no event down from ");
                    sb.append(observerWithState.getState());
                    throw new IllegalStateException(sb.toString());
                }
                this.pushParentState(down.getTargetState());
                observerWithState.dispatchEvent(lifecycleOwner, down);
                this.popParentState();
            }
        }
    }
    
    private final State calculateTargetState(final LifecycleObserver lifecycleObserver) {
        final Map.Entry<LifecycleObserver, ObserverWithState> ceil = this.observerMap.ceil(lifecycleObserver);
        State state = null;
        State state2 = null;
        Label_0039: {
            if (ceil != null) {
                final ObserverWithState observerWithState = ceil.getValue();
                if (observerWithState != null) {
                    state2 = observerWithState.getState();
                    break Label_0039;
                }
            }
            state2 = null;
        }
        if (this.parentStates.isEmpty() ^ true) {
            final ArrayList<State> parentStates = this.parentStates;
            state = parentStates.get(parentStates.size() - 1);
        }
        final Companion companion = LifecycleRegistry.Companion;
        return companion.min$lifecycle_runtime_release(companion.min$lifecycle_runtime_release(this.state, state2), state);
    }
    
    @JvmStatic
    public static final LifecycleRegistry createUnsafe(final LifecycleOwner lifecycleOwner) {
        return LifecycleRegistry.Companion.createUnsafe(lifecycleOwner);
    }
    
    private final void enforceMainThreadIfNeeded(final String str) {
        if (this.enforceMainThread && !ArchTaskExecutor.getInstance().isMainThread()) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Method ");
            sb.append(str);
            sb.append(" must be called on the main thread");
            throw new IllegalStateException(sb.toString().toString());
        }
    }
    
    private final void forwardPass(final LifecycleOwner lifecycleOwner) {
        final SafeIterableMap.IteratorWithAdditions iteratorWithAdditions = this.observerMap.iteratorWithAdditions();
        Intrinsics.checkNotNullExpressionValue((Object)iteratorWithAdditions, "observerMap.iteratorWithAdditions()");
        final Iterator iterator = iteratorWithAdditions;
        while (iterator.hasNext() && !this.newEventOccurred) {
            final Map.Entry<LifecycleObserver, V> entry = iterator.next();
            final LifecycleObserver lifecycleObserver = entry.getKey();
            final ObserverWithState observerWithState = (ObserverWithState)entry.getValue();
            while (observerWithState.getState().compareTo((State)this.state) < 0 && !this.newEventOccurred && this.observerMap.contains(lifecycleObserver)) {
                this.pushParentState(observerWithState.getState());
                final Event up = Event.Companion.upFrom(observerWithState.getState());
                if (up == null) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("no event up from ");
                    sb.append(observerWithState.getState());
                    throw new IllegalStateException(sb.toString());
                }
                observerWithState.dispatchEvent(lifecycleOwner, up);
                this.popParentState();
            }
        }
    }
    
    private final boolean isSynced() {
        final int size = this.observerMap.size();
        boolean b = true;
        if (size == 0) {
            return true;
        }
        final Map.Entry<LifecycleObserver, ObserverWithState> eldest = this.observerMap.eldest();
        Intrinsics.checkNotNull((Object)eldest);
        final State state = eldest.getValue().getState();
        final Map.Entry<LifecycleObserver, ObserverWithState> newest = this.observerMap.newest();
        Intrinsics.checkNotNull((Object)newest);
        final State state2 = newest.getValue().getState();
        if (state != state2 || this.state != state2) {
            b = false;
        }
        return b;
    }
    
    @JvmStatic
    public static final State min$lifecycle_runtime_release(final State state, final State state2) {
        return LifecycleRegistry.Companion.min$lifecycle_runtime_release(state, state2);
    }
    
    private final void moveToState(final State state) {
        final State state2 = this.state;
        if (state2 == state) {
            return;
        }
        if (state2 == State.INITIALIZED && state == State.DESTROYED) {
            final StringBuilder sb = new StringBuilder();
            sb.append("no event down from ");
            sb.append(this.state);
            sb.append(" in component ");
            sb.append(this.lifecycleOwner.get());
            throw new IllegalStateException(sb.toString().toString());
        }
        this.state = state;
        if (!this.handlingEvent && this.addingObserverCounter == 0) {
            this.handlingEvent = true;
            this.sync();
            this.handlingEvent = false;
            if (this.state == State.DESTROYED) {
                this.observerMap = new FastSafeIterableMap<LifecycleObserver, ObserverWithState>();
            }
            return;
        }
        this.newEventOccurred = true;
    }
    
    private final void popParentState() {
        final ArrayList<State> parentStates = this.parentStates;
        parentStates.remove(parentStates.size() - 1);
    }
    
    private final void pushParentState(final State e) {
        this.parentStates.add(e);
    }
    
    private final void sync() {
        final LifecycleOwner lifecycleOwner = this.lifecycleOwner.get();
        if (lifecycleOwner != null) {
            while (!this.isSynced()) {
                this.newEventOccurred = false;
                final State state = this.state;
                final Map.Entry<LifecycleObserver, ObserverWithState> eldest = this.observerMap.eldest();
                Intrinsics.checkNotNull((Object)eldest);
                if (state.compareTo((State)eldest.getValue().getState()) < 0) {
                    this.backwardPass(lifecycleOwner);
                }
                final Map.Entry<LifecycleObserver, ObserverWithState> newest = this.observerMap.newest();
                if (!this.newEventOccurred && newest != null && this.state.compareTo((State)newest.getValue().getState()) > 0) {
                    this.forwardPass(lifecycleOwner);
                }
            }
            this.newEventOccurred = false;
            return;
        }
        throw new IllegalStateException("LifecycleOwner of this LifecycleRegistry is already garbage collected. It is too late to change lifecycle state.");
    }
    
    @Override
    public void addObserver(final LifecycleObserver lifecycleObserver) {
        Intrinsics.checkNotNullParameter((Object)lifecycleObserver, "observer");
        this.enforceMainThreadIfNeeded("addObserver");
        State state;
        if (this.state == State.DESTROYED) {
            state = State.DESTROYED;
        }
        else {
            state = State.INITIALIZED;
        }
        final ObserverWithState observerWithState = new ObserverWithState(lifecycleObserver, state);
        if (this.observerMap.putIfAbsent(lifecycleObserver, observerWithState) != null) {
            return;
        }
        final LifecycleOwner lifecycleOwner = this.lifecycleOwner.get();
        if (lifecycleOwner == null) {
            return;
        }
        final boolean b = this.addingObserverCounter != 0 || this.handlingEvent;
        State state2 = this.calculateTargetState(lifecycleObserver);
        ++this.addingObserverCounter;
        while (observerWithState.getState().compareTo((State)state2) < 0 && this.observerMap.contains(lifecycleObserver)) {
            this.pushParentState(observerWithState.getState());
            final Event up = Event.Companion.upFrom(observerWithState.getState());
            if (up == null) {
                final StringBuilder sb = new StringBuilder();
                sb.append("no event up from ");
                sb.append(observerWithState.getState());
                throw new IllegalStateException(sb.toString());
            }
            observerWithState.dispatchEvent(lifecycleOwner, up);
            this.popParentState();
            state2 = this.calculateTargetState(lifecycleObserver);
        }
        if (!b) {
            this.sync();
        }
        --this.addingObserverCounter;
    }
    
    @Override
    public State getCurrentState() {
        return this.state;
    }
    
    public int getObserverCount() {
        this.enforceMainThreadIfNeeded("getObserverCount");
        return this.observerMap.size();
    }
    
    public void handleLifecycleEvent(final Event event) {
        Intrinsics.checkNotNullParameter((Object)event, "event");
        this.enforceMainThreadIfNeeded("handleLifecycleEvent");
        this.moveToState(event.getTargetState());
    }
    
    @Deprecated(message = "Override [currentState].")
    public void markState(final State currentState) {
        Intrinsics.checkNotNullParameter((Object)currentState, "state");
        this.enforceMainThreadIfNeeded("markState");
        this.setCurrentState(currentState);
    }
    
    @Override
    public void removeObserver(final LifecycleObserver lifecycleObserver) {
        Intrinsics.checkNotNullParameter((Object)lifecycleObserver, "observer");
        this.enforceMainThreadIfNeeded("removeObserver");
        this.observerMap.remove(lifecycleObserver);
    }
    
    public void setCurrentState(final State state) {
        Intrinsics.checkNotNullParameter((Object)state, "state");
        this.enforceMainThreadIfNeeded("setCurrentState");
        this.moveToState(state);
    }
    
    @Metadata(d1 = { "\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0007J\u001f\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\b2\b\u0010\n\u001a\u0004\u0018\u00010\bH\u0001¢\u0006\u0002\b\u000b¨\u0006\f" }, d2 = { "Landroidx/lifecycle/LifecycleRegistry$Companion;", "", "()V", "createUnsafe", "Landroidx/lifecycle/LifecycleRegistry;", "owner", "Landroidx/lifecycle/LifecycleOwner;", "min", "Landroidx/lifecycle/Lifecycle$State;", "state1", "state2", "min$lifecycle_runtime_release", "lifecycle-runtime_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
    public static final class Companion
    {
        private Companion() {
        }
        
        @JvmStatic
        public final LifecycleRegistry createUnsafe(final LifecycleOwner lifecycleOwner) {
            Intrinsics.checkNotNullParameter((Object)lifecycleOwner, "owner");
            return new LifecycleRegistry(lifecycleOwner, false, null);
        }
        
        @JvmStatic
        public final State min$lifecycle_runtime_release(final State state, final State state2) {
            Intrinsics.checkNotNullParameter((Object)state, "state1");
            State state3 = state;
            if (state2 != null) {
                state3 = state;
                if (state2.compareTo((State)state) < 0) {
                    state3 = state2;
                }
            }
            return state3;
        }
    }
    
    @Metadata(d1 = { "\u00002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0000\u0018\u00002\u00020\u0001B\u0017\u0012\b\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\u0018\u0010\u0012\u001a\u00020\u00132\b\u0010\u0014\u001a\u0004\u0018\u00010\u00152\u0006\u0010\u0016\u001a\u00020\u0017R\u001a\u0010\u0007\u001a\u00020\bX\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\t\u0010\n\"\u0004\b\u000b\u0010\fR\u001a\u0010\r\u001a\u00020\u0005X\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u000e\u0010\u000f\"\u0004\b\u0010\u0010\u0011¨\u0006\u0018" }, d2 = { "Landroidx/lifecycle/LifecycleRegistry$ObserverWithState;", "", "observer", "Landroidx/lifecycle/LifecycleObserver;", "initialState", "Landroidx/lifecycle/Lifecycle$State;", "(Landroidx/lifecycle/LifecycleObserver;Landroidx/lifecycle/Lifecycle$State;)V", "lifecycleObserver", "Landroidx/lifecycle/LifecycleEventObserver;", "getLifecycleObserver", "()Landroidx/lifecycle/LifecycleEventObserver;", "setLifecycleObserver", "(Landroidx/lifecycle/LifecycleEventObserver;)V", "state", "getState", "()Landroidx/lifecycle/Lifecycle$State;", "setState", "(Landroidx/lifecycle/Lifecycle$State;)V", "dispatchEvent", "", "owner", "Landroidx/lifecycle/LifecycleOwner;", "event", "Landroidx/lifecycle/Lifecycle$Event;", "lifecycle-runtime_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
    public static final class ObserverWithState
    {
        private LifecycleEventObserver lifecycleObserver;
        private State state;
        
        public ObserverWithState(final LifecycleObserver lifecycleObserver, final State state) {
            Intrinsics.checkNotNullParameter((Object)state, "initialState");
            Intrinsics.checkNotNull((Object)lifecycleObserver);
            this.lifecycleObserver = Lifecycling.lifecycleEventObserver(lifecycleObserver);
            this.state = state;
        }
        
        public final void dispatchEvent(final LifecycleOwner lifecycleOwner, final Event event) {
            Intrinsics.checkNotNullParameter((Object)event, "event");
            final State targetState = event.getTargetState();
            this.state = LifecycleRegistry.Companion.min$lifecycle_runtime_release(this.state, targetState);
            final LifecycleEventObserver lifecycleObserver = this.lifecycleObserver;
            Intrinsics.checkNotNull((Object)lifecycleOwner);
            lifecycleObserver.onStateChanged(lifecycleOwner, event);
            this.state = targetState;
        }
        
        public final LifecycleEventObserver getLifecycleObserver() {
            return this.lifecycleObserver;
        }
        
        public final State getState() {
            return this.state;
        }
        
        public final void setLifecycleObserver(final LifecycleEventObserver lifecycleObserver) {
            Intrinsics.checkNotNullParameter((Object)lifecycleObserver, "<set-?>");
            this.lifecycleObserver = lifecycleObserver;
        }
        
        public final void setState(final State state) {
            Intrinsics.checkNotNullParameter((Object)state, "<set-?>");
            this.state = state;
        }
    }
}

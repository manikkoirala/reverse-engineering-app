// 
// Decompiled by Procyon v0.6.0
// 

package androidx.lifecycle;

import java.util.Iterator;
import java.util.Map;
import kotlin.LazyKt;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.Intrinsics;
import kotlin.Lazy;
import android.os.Bundle;
import kotlin.Metadata;
import androidx.savedstate.SavedStateRegistry;

@Metadata(d1 = { "\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\b\u0000\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\u0010\u0010\u0011\u001a\u0004\u0018\u00010\n2\u0006\u0010\u0012\u001a\u00020\u0013J\u0006\u0010\u0014\u001a\u00020\u0015J\b\u0010\u0016\u001a\u00020\nH\u0016R\u000e\u0010\u0007\u001a\u00020\bX\u0082\u000e¢\u0006\u0002\n\u0000R\u0010\u0010\t\u001a\u0004\u0018\u00010\nX\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004¢\u0006\u0002\n\u0000R\u001b\u0010\u000b\u001a\u00020\f8BX\u0082\u0084\u0002¢\u0006\f\n\u0004\b\u000f\u0010\u0010\u001a\u0004\b\r\u0010\u000e¨\u0006\u0017" }, d2 = { "Landroidx/lifecycle/SavedStateHandlesProvider;", "Landroidx/savedstate/SavedStateRegistry$SavedStateProvider;", "savedStateRegistry", "Landroidx/savedstate/SavedStateRegistry;", "viewModelStoreOwner", "Landroidx/lifecycle/ViewModelStoreOwner;", "(Landroidx/savedstate/SavedStateRegistry;Landroidx/lifecycle/ViewModelStoreOwner;)V", "restored", "", "restoredState", "Landroid/os/Bundle;", "viewModel", "Landroidx/lifecycle/SavedStateHandlesVM;", "getViewModel", "()Landroidx/lifecycle/SavedStateHandlesVM;", "viewModel$delegate", "Lkotlin/Lazy;", "consumeRestoredStateForKey", "key", "", "performRestore", "", "saveState", "lifecycle-viewmodel-savedstate_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
public final class SavedStateHandlesProvider implements SavedStateProvider
{
    private boolean restored;
    private Bundle restoredState;
    private final SavedStateRegistry savedStateRegistry;
    private final Lazy viewModel$delegate;
    
    public SavedStateHandlesProvider(final SavedStateRegistry savedStateRegistry, final ViewModelStoreOwner viewModelStoreOwner) {
        Intrinsics.checkNotNullParameter((Object)savedStateRegistry, "savedStateRegistry");
        Intrinsics.checkNotNullParameter((Object)viewModelStoreOwner, "viewModelStoreOwner");
        this.savedStateRegistry = savedStateRegistry;
        this.viewModel$delegate = LazyKt.lazy((Function0)new SavedStateHandlesProvider$viewModel.SavedStateHandlesProvider$viewModel$2(viewModelStoreOwner));
    }
    
    private final SavedStateHandlesVM getViewModel() {
        return (SavedStateHandlesVM)this.viewModel$delegate.getValue();
    }
    
    public final Bundle consumeRestoredStateForKey(final String s) {
        Intrinsics.checkNotNullParameter((Object)s, "key");
        this.performRestore();
        final Bundle restoredState = this.restoredState;
        Bundle bundle;
        if (restoredState != null) {
            bundle = restoredState.getBundle(s);
        }
        else {
            bundle = null;
        }
        final Bundle restoredState2 = this.restoredState;
        if (restoredState2 != null) {
            restoredState2.remove(s);
        }
        final Bundle restoredState3 = this.restoredState;
        boolean b = true;
        if (restoredState3 == null || !restoredState3.isEmpty()) {
            b = false;
        }
        if (b) {
            this.restoredState = null;
        }
        return bundle;
    }
    
    public final void performRestore() {
        if (!this.restored) {
            final Bundle consumeRestoredStateForKey = this.savedStateRegistry.consumeRestoredStateForKey("androidx.lifecycle.internal.SavedStateHandlesProvider");
            final Bundle restoredState = new Bundle();
            final Bundle restoredState2 = this.restoredState;
            if (restoredState2 != null) {
                restoredState.putAll(restoredState2);
            }
            if (consumeRestoredStateForKey != null) {
                restoredState.putAll(consumeRestoredStateForKey);
            }
            this.restoredState = restoredState;
            this.restored = true;
            this.getViewModel();
        }
    }
    
    @Override
    public Bundle saveState() {
        final Bundle bundle = new Bundle();
        final Bundle restoredState = this.restoredState;
        if (restoredState != null) {
            bundle.putAll(restoredState);
        }
        for (final Map.Entry<String, V> entry : this.getViewModel().getHandles().entrySet()) {
            final String s = entry.getKey();
            final Bundle saveState = ((SavedStateHandle)entry.getValue()).savedStateProvider().saveState();
            if (!Intrinsics.areEqual((Object)saveState, (Object)Bundle.EMPTY)) {
                bundle.putBundle(s, saveState);
            }
        }
        this.restored = false;
        return bundle;
    }
}

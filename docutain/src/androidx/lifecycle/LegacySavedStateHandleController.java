// 
// Decompiled by Procyon v0.6.0
// 

package androidx.lifecycle;

import java.util.Iterator;
import java.util.Collection;
import androidx.savedstate.SavedStateRegistryOwner;
import android.os.Bundle;
import kotlin.jvm.JvmStatic;
import kotlin.jvm.internal.Intrinsics;
import androidx.savedstate.SavedStateRegistry;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000:\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\b\u00c0\u0002\u0018\u00002\u00020\u0001:\u0001\u0013B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J \u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\fH\u0007J,\u0010\r\u001a\u00020\u000e2\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\f2\b\u0010\u000f\u001a\u0004\u0018\u00010\u00042\b\u0010\u0010\u001a\u0004\u0018\u00010\u0011H\u0007J\u0018\u0010\u0012\u001a\u00020\u00062\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\fH\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0086T¢\u0006\u0002\n\u0000¨\u0006\u0014" }, d2 = { "Landroidx/lifecycle/LegacySavedStateHandleController;", "", "()V", "TAG_SAVED_STATE_HANDLE_CONTROLLER", "", "attachHandleIfNeeded", "", "viewModel", "Landroidx/lifecycle/ViewModel;", "registry", "Landroidx/savedstate/SavedStateRegistry;", "lifecycle", "Landroidx/lifecycle/Lifecycle;", "create", "Landroidx/lifecycle/SavedStateHandleController;", "key", "defaultArgs", "Landroid/os/Bundle;", "tryToAddRecreator", "OnRecreation", "lifecycle-viewmodel-savedstate_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
public final class LegacySavedStateHandleController
{
    public static final LegacySavedStateHandleController INSTANCE;
    public static final String TAG_SAVED_STATE_HANDLE_CONTROLLER = "androidx.lifecycle.savedstate.vm.tag";
    
    static {
        INSTANCE = new LegacySavedStateHandleController();
    }
    
    private LegacySavedStateHandleController() {
    }
    
    @JvmStatic
    public static final void attachHandleIfNeeded(final ViewModel viewModel, final SavedStateRegistry savedStateRegistry, final Lifecycle lifecycle) {
        Intrinsics.checkNotNullParameter((Object)viewModel, "viewModel");
        Intrinsics.checkNotNullParameter((Object)savedStateRegistry, "registry");
        Intrinsics.checkNotNullParameter((Object)lifecycle, "lifecycle");
        final SavedStateHandleController savedStateHandleController = viewModel.getTag("androidx.lifecycle.savedstate.vm.tag");
        if (savedStateHandleController != null && !savedStateHandleController.isAttached()) {
            savedStateHandleController.attachToLifecycle(savedStateRegistry, lifecycle);
            LegacySavedStateHandleController.INSTANCE.tryToAddRecreator(savedStateRegistry, lifecycle);
        }
    }
    
    @JvmStatic
    public static final SavedStateHandleController create(final SavedStateRegistry savedStateRegistry, final Lifecycle lifecycle, final String s, final Bundle bundle) {
        Intrinsics.checkNotNullParameter((Object)savedStateRegistry, "registry");
        Intrinsics.checkNotNullParameter((Object)lifecycle, "lifecycle");
        Intrinsics.checkNotNull((Object)s);
        final SavedStateHandleController savedStateHandleController = new SavedStateHandleController(s, SavedStateHandle.Companion.createHandle(savedStateRegistry.consumeRestoredStateForKey(s), bundle));
        savedStateHandleController.attachToLifecycle(savedStateRegistry, lifecycle);
        LegacySavedStateHandleController.INSTANCE.tryToAddRecreator(savedStateRegistry, lifecycle);
        return savedStateHandleController;
    }
    
    private final void tryToAddRecreator(final SavedStateRegistry savedStateRegistry, final Lifecycle lifecycle) {
        final Lifecycle.State currentState = lifecycle.getCurrentState();
        if (currentState != Lifecycle.State.INITIALIZED && !currentState.isAtLeast(Lifecycle.State.STARTED)) {
            lifecycle.addObserver((LifecycleObserver)new LegacySavedStateHandleController$tryToAddRecreator.LegacySavedStateHandleController$tryToAddRecreator$1(lifecycle, savedStateRegistry));
        }
        else {
            savedStateRegistry.runOnNextRecreation((Class<? extends SavedStateRegistry.AutoRecreated>)OnRecreation.class);
        }
    }
    
    @Metadata(d1 = { "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0000\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0016¨\u0006\u0007" }, d2 = { "Landroidx/lifecycle/LegacySavedStateHandleController$OnRecreation;", "Landroidx/savedstate/SavedStateRegistry$AutoRecreated;", "()V", "onRecreated", "", "owner", "Landroidx/savedstate/SavedStateRegistryOwner;", "lifecycle-viewmodel-savedstate_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
    public static final class OnRecreation implements AutoRecreated
    {
        @Override
        public void onRecreated(final SavedStateRegistryOwner savedStateRegistryOwner) {
            Intrinsics.checkNotNullParameter((Object)savedStateRegistryOwner, "owner");
            if (savedStateRegistryOwner instanceof ViewModelStoreOwner) {
                final ViewModelStore viewModelStore = ((ViewModelStoreOwner)savedStateRegistryOwner).getViewModelStore();
                final SavedStateRegistry savedStateRegistry = savedStateRegistryOwner.getSavedStateRegistry();
                final Iterator<String> iterator = viewModelStore.keys().iterator();
                while (iterator.hasNext()) {
                    final ViewModel value = viewModelStore.get(iterator.next());
                    Intrinsics.checkNotNull((Object)value);
                    LegacySavedStateHandleController.attachHandleIfNeeded(value, savedStateRegistry, savedStateRegistryOwner.getLifecycle());
                }
                if (viewModelStore.keys().isEmpty() ^ true) {
                    savedStateRegistry.runOnNextRecreation((Class<? extends SavedStateRegistry.AutoRecreated>)OnRecreation.class);
                }
                return;
            }
            throw new IllegalStateException("Internal error: OnRecreation should be registered only on components that implement ViewModelStoreOwner".toString());
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.lifecycle;

public class MutableLiveData<T> extends LiveData<T>
{
    public MutableLiveData() {
    }
    
    public MutableLiveData(final T t) {
        super(t);
    }
    
    public void postValue(final T t) {
        super.postValue(t);
    }
    
    public void setValue(final T value) {
        super.setValue(value);
    }
}

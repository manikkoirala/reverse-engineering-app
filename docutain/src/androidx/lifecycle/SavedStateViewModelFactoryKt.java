// 
// Decompiled by Procyon v0.6.0
// 

package androidx.lifecycle;

import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.Collection;
import kotlin.collections.ArraysKt;
import kotlin.jvm.internal.Intrinsics;
import java.lang.reflect.Constructor;
import kotlin.collections.CollectionsKt;
import android.app.Application;
import java.util.List;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000*\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0011\n\u0002\u0010\u0000\n\u0002\b\u0002\u001a6\u0010\u0004\u001a\n\u0012\u0004\u0012\u0002H\u0006\u0018\u00010\u0005\"\u0004\b\u0000\u0010\u00062\f\u0010\u0007\u001a\b\u0012\u0004\u0012\u0002H\u00060\u00022\u0010\u0010\b\u001a\f\u0012\b\u0012\u0006\u0012\u0002\b\u00030\u00020\u0001H\u0000\u001aI\u0010\t\u001a\u0002H\u0006\"\n\b\u0000\u0010\u0006*\u0004\u0018\u00010\n2\f\u0010\u0007\u001a\b\u0012\u0004\u0012\u0002H\u00060\u00022\f\u0010\u000b\u001a\b\u0012\u0004\u0012\u0002H\u00060\u00052\u0012\u0010\f\u001a\n\u0012\u0006\b\u0001\u0012\u00020\u000e0\r\"\u00020\u000eH\u0000¢\u0006\u0002\u0010\u000f\"\u0018\u0010\u0000\u001a\f\u0012\b\u0012\u0006\u0012\u0002\b\u00030\u00020\u0001X\u0082\u0004¢\u0006\u0002\n\u0000\"\u0018\u0010\u0003\u001a\f\u0012\b\u0012\u0006\u0012\u0002\b\u00030\u00020\u0001X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u0010" }, d2 = { "ANDROID_VIEWMODEL_SIGNATURE", "", "Ljava/lang/Class;", "VIEWMODEL_SIGNATURE", "findMatchingConstructor", "Ljava/lang/reflect/Constructor;", "T", "modelClass", "signature", "newInstance", "Landroidx/lifecycle/ViewModel;", "constructor", "params", "", "", "(Ljava/lang/Class;Ljava/lang/reflect/Constructor;[Ljava/lang/Object;)Landroidx/lifecycle/ViewModel;", "lifecycle-viewmodel-savedstate_release" }, k = 2, mv = { 1, 8, 0 }, xi = 48)
public final class SavedStateViewModelFactoryKt
{
    private static final List<Class<?>> ANDROID_VIEWMODEL_SIGNATURE;
    private static final List<Class<?>> VIEWMODEL_SIGNATURE;
    
    static {
        ANDROID_VIEWMODEL_SIGNATURE = CollectionsKt.listOf((Object[])new Class[] { Application.class, SavedStateHandle.class });
        VIEWMODEL_SIGNATURE = CollectionsKt.listOf((Object)SavedStateHandle.class);
    }
    
    public static final <T> Constructor<T> findMatchingConstructor(final Class<T> clazz, final List<? extends Class<?>> obj) {
        Intrinsics.checkNotNullParameter((Object)clazz, "modelClass");
        Intrinsics.checkNotNullParameter((Object)obj, "signature");
        final Constructor[] constructors = clazz.getConstructors();
        Intrinsics.checkNotNullExpressionValue((Object)constructors, "modelClass.constructors");
        for (final Constructor constructor : constructors) {
            final Class[] parameterTypes = constructor.getParameterTypes();
            Intrinsics.checkNotNullExpressionValue((Object)parameterTypes, "constructor.parameterTypes");
            final List list = ArraysKt.toList((Object[])parameterTypes);
            if (Intrinsics.areEqual((Object)obj, (Object)list)) {
                Intrinsics.checkNotNull((Object)constructor, "null cannot be cast to non-null type java.lang.reflect.Constructor<T of androidx.lifecycle.SavedStateViewModelFactoryKt.findMatchingConstructor>");
                return constructor;
            }
            if (obj.size() == list.size() && list.containsAll(obj)) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Class ");
                sb.append(clazz.getSimpleName());
                sb.append(" must have parameters in the proper order: ");
                sb.append(obj);
                throw new UnsupportedOperationException(sb.toString());
            }
        }
        return null;
    }
    
    public static final <T extends ViewModel> T newInstance(final Class<T> obj, final Constructor<T> constructor, final Object... original) {
        Intrinsics.checkNotNullParameter((Object)obj, "modelClass");
        Intrinsics.checkNotNullParameter((Object)constructor, "constructor");
        Intrinsics.checkNotNullParameter((Object)original, "params");
        try {
            return constructor.newInstance(Arrays.copyOf(original, original.length));
        }
        catch (final InvocationTargetException ex) {
            final StringBuilder sb = new StringBuilder();
            sb.append("An exception happened in constructor of ");
            sb.append(obj);
            throw new RuntimeException(sb.toString(), ex.getCause());
        }
        catch (final InstantiationException ex2) {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("A ");
            sb2.append(obj);
            sb2.append(" cannot be instantiated.");
            throw new RuntimeException(sb2.toString(), ex2);
        }
        catch (final IllegalAccessException ex3) {
            final StringBuilder sb3 = new StringBuilder();
            sb3.append("Failed to access ");
            sb3.append(obj);
            throw new RuntimeException(sb3.toString(), ex3);
        }
    }
}

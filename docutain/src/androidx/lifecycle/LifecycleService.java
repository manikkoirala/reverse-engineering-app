// 
// Decompiled by Procyon v0.6.0
// 

package androidx.lifecycle;

import kotlin.Deprecated;
import kotlin.jvm.internal.Intrinsics;
import android.os.IBinder;
import android.content.Intent;
import kotlin.Metadata;
import android.app.Service;

@Metadata(d1 = { "\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0003\b\u0016\u0018\u00002\u00020\u00012\u00020\u0002B\u0005¢\u0006\u0002\u0010\u0003J\u0012\u0010\n\u001a\u0004\u0018\u00010\u000b2\u0006\u0010\f\u001a\u00020\rH\u0017J\b\u0010\u000e\u001a\u00020\u000fH\u0017J\b\u0010\u0010\u001a\u00020\u000fH\u0017J\u001a\u0010\u0011\u001a\u00020\u000f2\b\u0010\f\u001a\u0004\u0018\u00010\r2\u0006\u0010\u0012\u001a\u00020\u0013H\u0017J\"\u0010\u0014\u001a\u00020\u00132\b\u0010\f\u001a\u0004\u0018\u00010\r2\u0006\u0010\u0015\u001a\u00020\u00132\u0006\u0010\u0012\u001a\u00020\u0013H\u0017R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004¢\u0006\u0002\n\u0000R\u0014\u0010\u0006\u001a\u00020\u00078VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\b\u0010\t¨\u0006\u0016" }, d2 = { "Landroidx/lifecycle/LifecycleService;", "Landroid/app/Service;", "Landroidx/lifecycle/LifecycleOwner;", "()V", "dispatcher", "Landroidx/lifecycle/ServiceLifecycleDispatcher;", "lifecycle", "Landroidx/lifecycle/Lifecycle;", "getLifecycle", "()Landroidx/lifecycle/Lifecycle;", "onBind", "Landroid/os/IBinder;", "intent", "Landroid/content/Intent;", "onCreate", "", "onDestroy", "onStart", "startId", "", "onStartCommand", "flags", "lifecycle-service_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
public class LifecycleService extends Service implements LifecycleOwner
{
    private final ServiceLifecycleDispatcher dispatcher;
    
    public LifecycleService() {
        this.dispatcher = new ServiceLifecycleDispatcher(this);
    }
    
    public Lifecycle getLifecycle() {
        return this.dispatcher.getLifecycle();
    }
    
    public IBinder onBind(final Intent intent) {
        Intrinsics.checkNotNullParameter((Object)intent, "intent");
        this.dispatcher.onServicePreSuperOnBind();
        return null;
    }
    
    public void onCreate() {
        this.dispatcher.onServicePreSuperOnCreate();
        super.onCreate();
    }
    
    public void onDestroy() {
        this.dispatcher.onServicePreSuperOnDestroy();
        super.onDestroy();
    }
    
    @Deprecated(message = "Deprecated in Java")
    public void onStart(final Intent intent, final int n) {
        this.dispatcher.onServicePreSuperOnStart();
        super.onStart(intent, n);
    }
    
    public int onStartCommand(final Intent intent, final int n, final int n2) {
        return super.onStartCommand(intent, n, n2);
    }
}

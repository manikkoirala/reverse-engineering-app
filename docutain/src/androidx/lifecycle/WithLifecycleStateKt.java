// 
// Decompiled by Procyon v0.6.0
// 

package androidx.lifecycle;

import kotlin.jvm.internal.InlineMarker;
import kotlinx.coroutines.MainCoroutineDispatcher;
import kotlinx.coroutines.Dispatchers;
import kotlin.coroutines.jvm.internal.DebugProbesKt;
import kotlin.jvm.functions.Function1;
import kotlin.coroutines.EmptyCoroutineContext;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CancellableContinuation;
import kotlinx.coroutines.CancellableContinuationImpl;
import kotlin.coroutines.intrinsics.IntrinsicsKt;
import kotlin.coroutines.Continuation;
import kotlin.jvm.functions.Function0;
import kotlinx.coroutines.CoroutineDispatcher;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000,\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\b\u001aA\u0010\u0000\u001a\u0002H\u0001\"\u0004\b\u0000\u0010\u0001*\u00020\u00022\u0006\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b2\f\u0010\t\u001a\b\u0012\u0004\u0012\u0002H\u00010\nH\u0081@\u00f8\u0001\u0000¢\u0006\u0002\u0010\u000b\u001a+\u0010\f\u001a\u0002H\u0001\"\u0004\b\u0000\u0010\u0001*\u00020\u00022\u000e\b\u0004\u0010\t\u001a\b\u0012\u0004\u0012\u0002H\u00010\nH\u0086H\u00f8\u0001\u0000¢\u0006\u0002\u0010\r\u001a+\u0010\f\u001a\u0002H\u0001\"\u0004\b\u0000\u0010\u0001*\u00020\u000e2\u000e\b\u0004\u0010\t\u001a\b\u0012\u0004\u0012\u0002H\u00010\nH\u0086H\u00f8\u0001\u0000¢\u0006\u0002\u0010\u000f\u001a+\u0010\u0010\u001a\u0002H\u0001\"\u0004\b\u0000\u0010\u0001*\u00020\u00022\u000e\b\u0004\u0010\t\u001a\b\u0012\u0004\u0012\u0002H\u00010\nH\u0086H\u00f8\u0001\u0000¢\u0006\u0002\u0010\r\u001a+\u0010\u0010\u001a\u0002H\u0001\"\u0004\b\u0000\u0010\u0001*\u00020\u000e2\u000e\b\u0004\u0010\t\u001a\b\u0012\u0004\u0012\u0002H\u00010\nH\u0086H\u00f8\u0001\u0000¢\u0006\u0002\u0010\u000f\u001a+\u0010\u0011\u001a\u0002H\u0001\"\u0004\b\u0000\u0010\u0001*\u00020\u00022\u000e\b\u0004\u0010\t\u001a\b\u0012\u0004\u0012\u0002H\u00010\nH\u0086H\u00f8\u0001\u0000¢\u0006\u0002\u0010\r\u001a+\u0010\u0011\u001a\u0002H\u0001\"\u0004\b\u0000\u0010\u0001*\u00020\u000e2\u000e\b\u0004\u0010\t\u001a\b\u0012\u0004\u0012\u0002H\u00010\nH\u0086H\u00f8\u0001\u0000¢\u0006\u0002\u0010\u000f\u001a3\u0010\u0012\u001a\u0002H\u0001\"\u0004\b\u0000\u0010\u0001*\u00020\u00022\u0006\u0010\u0003\u001a\u00020\u00042\u000e\b\u0004\u0010\t\u001a\b\u0012\u0004\u0012\u0002H\u00010\nH\u0086H\u00f8\u0001\u0000¢\u0006\u0002\u0010\u0013\u001a3\u0010\u0012\u001a\u0002H\u0001\"\u0004\b\u0000\u0010\u0001*\u00020\u000e2\u0006\u0010\u0003\u001a\u00020\u00042\u000e\b\u0004\u0010\t\u001a\b\u0012\u0004\u0012\u0002H\u00010\nH\u0086H\u00f8\u0001\u0000¢\u0006\u0002\u0010\u0014\u001a3\u0010\u0015\u001a\u0002H\u0001\"\u0004\b\u0000\u0010\u0001*\u00020\u00022\u0006\u0010\u0003\u001a\u00020\u00042\u000e\b\u0004\u0010\t\u001a\b\u0012\u0004\u0012\u0002H\u00010\nH\u0081H\u00f8\u0001\u0000¢\u0006\u0002\u0010\u0013\u0082\u0002\u0004\n\u0002\b\u0019¨\u0006\u0016" }, d2 = { "suspendWithStateAtLeastUnchecked", "R", "Landroidx/lifecycle/Lifecycle;", "state", "Landroidx/lifecycle/Lifecycle$State;", "dispatchNeeded", "", "lifecycleDispatcher", "Lkotlinx/coroutines/CoroutineDispatcher;", "block", "Lkotlin/Function0;", "(Landroidx/lifecycle/Lifecycle;Landroidx/lifecycle/Lifecycle$State;ZLkotlinx/coroutines/CoroutineDispatcher;Lkotlin/jvm/functions/Function0;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "withCreated", "(Landroidx/lifecycle/Lifecycle;Lkotlin/jvm/functions/Function0;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "Landroidx/lifecycle/LifecycleOwner;", "(Landroidx/lifecycle/LifecycleOwner;Lkotlin/jvm/functions/Function0;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "withResumed", "withStarted", "withStateAtLeast", "(Landroidx/lifecycle/Lifecycle;Landroidx/lifecycle/Lifecycle$State;Lkotlin/jvm/functions/Function0;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "(Landroidx/lifecycle/LifecycleOwner;Landroidx/lifecycle/Lifecycle$State;Lkotlin/jvm/functions/Function0;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "withStateAtLeastUnchecked", "lifecycle-runtime-ktx_release" }, k = 2, mv = { 1, 8, 0 }, xi = 48)
public final class WithLifecycleStateKt
{
    public static final <R> Object suspendWithStateAtLeastUnchecked(final Lifecycle lifecycle, final Lifecycle.State state, final boolean b, final CoroutineDispatcher coroutineDispatcher, final Function0<? extends R> function0, final Continuation<? super R> continuation) {
        final CancellableContinuationImpl cancellableContinuationImpl = new CancellableContinuationImpl(IntrinsicsKt.intercepted((Continuation)continuation), 1);
        cancellableContinuationImpl.initCancellability();
        final CancellableContinuation cancellableContinuation = (CancellableContinuation)cancellableContinuationImpl;
        final WithLifecycleStateKt$suspendWithStateAtLeastUnchecked$2$observer.WithLifecycleStateKt$suspendWithStateAtLeastUnchecked$2$observer$1 withLifecycleStateKt$suspendWithStateAtLeastUnchecked$2$observer$1 = new WithLifecycleStateKt$suspendWithStateAtLeastUnchecked$2$observer.WithLifecycleStateKt$suspendWithStateAtLeastUnchecked$2$observer$1(state, lifecycle, cancellableContinuation, (Function0)function0);
        if (b) {
            coroutineDispatcher.dispatch((CoroutineContext)EmptyCoroutineContext.INSTANCE, (Runnable)new WithLifecycleStateKt$suspendWithStateAtLeastUnchecked$2.WithLifecycleStateKt$suspendWithStateAtLeastUnchecked$2$1(lifecycle, withLifecycleStateKt$suspendWithStateAtLeastUnchecked$2$observer$1));
        }
        else {
            lifecycle.addObserver((LifecycleObserver)withLifecycleStateKt$suspendWithStateAtLeastUnchecked$2$observer$1);
        }
        cancellableContinuation.invokeOnCancellation((Function1)new WithLifecycleStateKt$suspendWithStateAtLeastUnchecked$2.WithLifecycleStateKt$suspendWithStateAtLeastUnchecked$2$2(coroutineDispatcher, lifecycle, withLifecycleStateKt$suspendWithStateAtLeastUnchecked$2$observer$1));
        final Object result = cancellableContinuationImpl.getResult();
        if (result == IntrinsicsKt.getCOROUTINE_SUSPENDED()) {
            DebugProbesKt.probeCoroutineSuspended((Continuation)continuation);
        }
        return result;
    }
    
    public static final <R> Object withCreated(final Lifecycle lifecycle, final Function0<? extends R> function0, final Continuation<? super R> continuation) {
        final Lifecycle.State created = Lifecycle.State.CREATED;
        final MainCoroutineDispatcher immediate = Dispatchers.getMain().getImmediate();
        final boolean dispatchNeeded = immediate.isDispatchNeeded(continuation.getContext());
        if (!dispatchNeeded) {
            if (lifecycle.getCurrentState() == Lifecycle.State.DESTROYED) {
                throw new LifecycleDestroyedException();
            }
            if (lifecycle.getCurrentState().compareTo((Lifecycle.State)created) >= 0) {
                return function0.invoke();
            }
        }
        return suspendWithStateAtLeastUnchecked(lifecycle, created, dispatchNeeded, (CoroutineDispatcher)immediate, (kotlin.jvm.functions.Function0<?>)new WithLifecycleStateKt$withStateAtLeastUnchecked.WithLifecycleStateKt$withStateAtLeastUnchecked$2((Function0)function0), (kotlin.coroutines.Continuation<? super Object>)continuation);
    }
    
    public static final <R> Object withCreated(final LifecycleOwner lifecycleOwner, final Function0<? extends R> function0, final Continuation<? super R> continuation) {
        final Lifecycle lifecycle = lifecycleOwner.getLifecycle();
        final Lifecycle.State created = Lifecycle.State.CREATED;
        final MainCoroutineDispatcher immediate = Dispatchers.getMain().getImmediate();
        final boolean dispatchNeeded = immediate.isDispatchNeeded(continuation.getContext());
        if (!dispatchNeeded) {
            if (lifecycle.getCurrentState() == Lifecycle.State.DESTROYED) {
                throw new LifecycleDestroyedException();
            }
            if (lifecycle.getCurrentState().compareTo((Lifecycle.State)created) >= 0) {
                return function0.invoke();
            }
        }
        return suspendWithStateAtLeastUnchecked(lifecycle, created, dispatchNeeded, (CoroutineDispatcher)immediate, (kotlin.jvm.functions.Function0<?>)new WithLifecycleStateKt$withStateAtLeastUnchecked.WithLifecycleStateKt$withStateAtLeastUnchecked$2((Function0)function0), (kotlin.coroutines.Continuation<? super Object>)continuation);
    }
    
    private static final <R> Object withCreated$$forInline(final Lifecycle lifecycle, final Function0<? extends R> function0, final Continuation<? super R> continuation) {
        final Lifecycle.State created = Lifecycle.State.CREATED;
        Dispatchers.getMain().getImmediate();
        InlineMarker.mark(3);
        throw new NullPointerException();
    }
    
    private static final <R> Object withCreated$$forInline(final LifecycleOwner lifecycleOwner, final Function0<? extends R> function0, final Continuation<? super R> continuation) {
        lifecycleOwner.getLifecycle();
        final Lifecycle.State created = Lifecycle.State.CREATED;
        Dispatchers.getMain().getImmediate();
        InlineMarker.mark(3);
        throw new NullPointerException();
    }
    
    public static final <R> Object withResumed(final Lifecycle lifecycle, final Function0<? extends R> function0, final Continuation<? super R> continuation) {
        final Lifecycle.State resumed = Lifecycle.State.RESUMED;
        final MainCoroutineDispatcher immediate = Dispatchers.getMain().getImmediate();
        final boolean dispatchNeeded = immediate.isDispatchNeeded(continuation.getContext());
        if (!dispatchNeeded) {
            if (lifecycle.getCurrentState() == Lifecycle.State.DESTROYED) {
                throw new LifecycleDestroyedException();
            }
            if (lifecycle.getCurrentState().compareTo((Lifecycle.State)resumed) >= 0) {
                return function0.invoke();
            }
        }
        return suspendWithStateAtLeastUnchecked(lifecycle, resumed, dispatchNeeded, (CoroutineDispatcher)immediate, (kotlin.jvm.functions.Function0<?>)new WithLifecycleStateKt$withStateAtLeastUnchecked.WithLifecycleStateKt$withStateAtLeastUnchecked$2((Function0)function0), (kotlin.coroutines.Continuation<? super Object>)continuation);
    }
    
    public static final <R> Object withResumed(final LifecycleOwner lifecycleOwner, final Function0<? extends R> function0, final Continuation<? super R> continuation) {
        final Lifecycle lifecycle = lifecycleOwner.getLifecycle();
        final Lifecycle.State resumed = Lifecycle.State.RESUMED;
        final MainCoroutineDispatcher immediate = Dispatchers.getMain().getImmediate();
        final boolean dispatchNeeded = immediate.isDispatchNeeded(continuation.getContext());
        if (!dispatchNeeded) {
            if (lifecycle.getCurrentState() == Lifecycle.State.DESTROYED) {
                throw new LifecycleDestroyedException();
            }
            if (lifecycle.getCurrentState().compareTo((Lifecycle.State)resumed) >= 0) {
                return function0.invoke();
            }
        }
        return suspendWithStateAtLeastUnchecked(lifecycle, resumed, dispatchNeeded, (CoroutineDispatcher)immediate, (kotlin.jvm.functions.Function0<?>)new WithLifecycleStateKt$withStateAtLeastUnchecked.WithLifecycleStateKt$withStateAtLeastUnchecked$2((Function0)function0), (kotlin.coroutines.Continuation<? super Object>)continuation);
    }
    
    private static final <R> Object withResumed$$forInline(final Lifecycle lifecycle, final Function0<? extends R> function0, final Continuation<? super R> continuation) {
        final Lifecycle.State resumed = Lifecycle.State.RESUMED;
        Dispatchers.getMain().getImmediate();
        InlineMarker.mark(3);
        throw new NullPointerException();
    }
    
    private static final <R> Object withResumed$$forInline(final LifecycleOwner lifecycleOwner, final Function0<? extends R> function0, final Continuation<? super R> continuation) {
        lifecycleOwner.getLifecycle();
        final Lifecycle.State resumed = Lifecycle.State.RESUMED;
        Dispatchers.getMain().getImmediate();
        InlineMarker.mark(3);
        throw new NullPointerException();
    }
    
    public static final <R> Object withStarted(final Lifecycle lifecycle, final Function0<? extends R> function0, final Continuation<? super R> continuation) {
        final Lifecycle.State started = Lifecycle.State.STARTED;
        final MainCoroutineDispatcher immediate = Dispatchers.getMain().getImmediate();
        final boolean dispatchNeeded = immediate.isDispatchNeeded(continuation.getContext());
        if (!dispatchNeeded) {
            if (lifecycle.getCurrentState() == Lifecycle.State.DESTROYED) {
                throw new LifecycleDestroyedException();
            }
            if (lifecycle.getCurrentState().compareTo((Lifecycle.State)started) >= 0) {
                return function0.invoke();
            }
        }
        return suspendWithStateAtLeastUnchecked(lifecycle, started, dispatchNeeded, (CoroutineDispatcher)immediate, (kotlin.jvm.functions.Function0<?>)new WithLifecycleStateKt$withStateAtLeastUnchecked.WithLifecycleStateKt$withStateAtLeastUnchecked$2((Function0)function0), (kotlin.coroutines.Continuation<? super Object>)continuation);
    }
    
    public static final <R> Object withStarted(final LifecycleOwner lifecycleOwner, final Function0<? extends R> function0, final Continuation<? super R> continuation) {
        final Lifecycle lifecycle = lifecycleOwner.getLifecycle();
        final Lifecycle.State started = Lifecycle.State.STARTED;
        final MainCoroutineDispatcher immediate = Dispatchers.getMain().getImmediate();
        final boolean dispatchNeeded = immediate.isDispatchNeeded(continuation.getContext());
        if (!dispatchNeeded) {
            if (lifecycle.getCurrentState() == Lifecycle.State.DESTROYED) {
                throw new LifecycleDestroyedException();
            }
            if (lifecycle.getCurrentState().compareTo((Lifecycle.State)started) >= 0) {
                return function0.invoke();
            }
        }
        return suspendWithStateAtLeastUnchecked(lifecycle, started, dispatchNeeded, (CoroutineDispatcher)immediate, (kotlin.jvm.functions.Function0<?>)new WithLifecycleStateKt$withStateAtLeastUnchecked.WithLifecycleStateKt$withStateAtLeastUnchecked$2((Function0)function0), (kotlin.coroutines.Continuation<? super Object>)continuation);
    }
    
    private static final <R> Object withStarted$$forInline(final Lifecycle lifecycle, final Function0<? extends R> function0, final Continuation<? super R> continuation) {
        final Lifecycle.State started = Lifecycle.State.STARTED;
        Dispatchers.getMain().getImmediate();
        InlineMarker.mark(3);
        throw new NullPointerException();
    }
    
    private static final <R> Object withStarted$$forInline(final LifecycleOwner lifecycleOwner, final Function0<? extends R> function0, final Continuation<? super R> continuation) {
        lifecycleOwner.getLifecycle();
        final Lifecycle.State started = Lifecycle.State.STARTED;
        Dispatchers.getMain().getImmediate();
        InlineMarker.mark(3);
        throw new NullPointerException();
    }
    
    public static final <R> Object withStateAtLeast(final Lifecycle lifecycle, final Lifecycle.State obj, final Function0<? extends R> function0, final Continuation<? super R> continuation) {
        if (obj.compareTo((Lifecycle.State)Lifecycle.State.CREATED) >= 0) {
            final MainCoroutineDispatcher immediate = Dispatchers.getMain().getImmediate();
            final boolean dispatchNeeded = immediate.isDispatchNeeded(continuation.getContext());
            if (!dispatchNeeded) {
                if (lifecycle.getCurrentState() == Lifecycle.State.DESTROYED) {
                    throw new LifecycleDestroyedException();
                }
                if (lifecycle.getCurrentState().compareTo((Lifecycle.State)obj) >= 0) {
                    return function0.invoke();
                }
            }
            return suspendWithStateAtLeastUnchecked(lifecycle, obj, dispatchNeeded, (CoroutineDispatcher)immediate, (kotlin.jvm.functions.Function0<?>)new WithLifecycleStateKt$withStateAtLeastUnchecked.WithLifecycleStateKt$withStateAtLeastUnchecked$2((Function0)function0), (kotlin.coroutines.Continuation<? super Object>)continuation);
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("target state must be CREATED or greater, found ");
        sb.append(obj);
        throw new IllegalArgumentException(sb.toString().toString());
    }
    
    public static final <R> Object withStateAtLeast(final LifecycleOwner lifecycleOwner, final Lifecycle.State obj, final Function0<? extends R> function0, final Continuation<? super R> continuation) {
        final Lifecycle lifecycle = lifecycleOwner.getLifecycle();
        if (obj.compareTo((Lifecycle.State)Lifecycle.State.CREATED) >= 0) {
            final MainCoroutineDispatcher immediate = Dispatchers.getMain().getImmediate();
            final boolean dispatchNeeded = immediate.isDispatchNeeded(continuation.getContext());
            if (!dispatchNeeded) {
                if (lifecycle.getCurrentState() == Lifecycle.State.DESTROYED) {
                    throw new LifecycleDestroyedException();
                }
                if (lifecycle.getCurrentState().compareTo((Lifecycle.State)obj) >= 0) {
                    return function0.invoke();
                }
            }
            return suspendWithStateAtLeastUnchecked(lifecycle, obj, dispatchNeeded, (CoroutineDispatcher)immediate, (kotlin.jvm.functions.Function0<?>)new WithLifecycleStateKt$withStateAtLeastUnchecked.WithLifecycleStateKt$withStateAtLeastUnchecked$2((Function0)function0), (kotlin.coroutines.Continuation<? super Object>)continuation);
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("target state must be CREATED or greater, found ");
        sb.append(obj);
        throw new IllegalArgumentException(sb.toString().toString());
    }
    
    private static final <R> Object withStateAtLeast$$forInline(final Lifecycle lifecycle, final Lifecycle.State obj, final Function0<? extends R> function0, final Continuation<? super R> continuation) {
        if (obj.compareTo((Lifecycle.State)Lifecycle.State.CREATED) < 0) {
            final StringBuilder sb = new StringBuilder();
            sb.append("target state must be CREATED or greater, found ");
            sb.append(obj);
            throw new IllegalArgumentException(sb.toString().toString());
        }
        Dispatchers.getMain().getImmediate();
        InlineMarker.mark(3);
        throw new NullPointerException();
    }
    
    private static final <R> Object withStateAtLeast$$forInline(final LifecycleOwner lifecycleOwner, final Lifecycle.State obj, final Function0<? extends R> function0, final Continuation<? super R> continuation) {
        lifecycleOwner.getLifecycle();
        if (obj.compareTo((Lifecycle.State)Lifecycle.State.CREATED) < 0) {
            final StringBuilder sb = new StringBuilder();
            sb.append("target state must be CREATED or greater, found ");
            sb.append(obj);
            throw new IllegalArgumentException(sb.toString().toString());
        }
        Dispatchers.getMain().getImmediate();
        InlineMarker.mark(3);
        throw new NullPointerException();
    }
    
    public static final <R> Object withStateAtLeastUnchecked(final Lifecycle lifecycle, final Lifecycle.State state, final Function0<? extends R> function0, final Continuation<? super R> continuation) {
        final MainCoroutineDispatcher immediate = Dispatchers.getMain().getImmediate();
        final boolean dispatchNeeded = immediate.isDispatchNeeded(continuation.getContext());
        if (!dispatchNeeded) {
            if (lifecycle.getCurrentState() == Lifecycle.State.DESTROYED) {
                throw new LifecycleDestroyedException();
            }
            if (lifecycle.getCurrentState().compareTo((Lifecycle.State)state) >= 0) {
                return function0.invoke();
            }
        }
        return suspendWithStateAtLeastUnchecked(lifecycle, state, dispatchNeeded, (CoroutineDispatcher)immediate, (kotlin.jvm.functions.Function0<?>)new WithLifecycleStateKt$withStateAtLeastUnchecked.WithLifecycleStateKt$withStateAtLeastUnchecked$2((Function0)function0), (kotlin.coroutines.Continuation<? super Object>)continuation);
    }
    
    private static final <R> Object withStateAtLeastUnchecked$$forInline(final Lifecycle lifecycle, final Lifecycle.State state, final Function0<? extends R> function0, final Continuation<? super R> continuation) {
        Dispatchers.getMain().getImmediate();
        InlineMarker.mark(3);
        throw new NullPointerException();
    }
}

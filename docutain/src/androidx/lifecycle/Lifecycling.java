// 
// Decompiled by Procyon v0.6.0
// 

package androidx.lifecycle;

import java.util.ArrayList;
import java.util.Collection;
import kotlin.collections.CollectionsKt;
import kotlin.jvm.JvmStatic;
import kotlin.text.StringsKt;
import java.lang.reflect.InvocationTargetException;
import kotlin.jvm.internal.Intrinsics;
import java.util.HashMap;
import java.lang.reflect.Constructor;
import java.util.List;
import java.util.Map;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000D\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010%\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u00c7\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J \u0010\r\u001a\u00020\f2\u000e\u0010\u000e\u001a\n\u0012\u0006\b\u0001\u0012\u00020\f0\u000b2\u0006\u0010\u000f\u001a\u00020\u0001H\u0002J\u001e\u0010\u0010\u001a\f\u0012\u0006\b\u0001\u0012\u00020\f\u0018\u00010\u000b2\n\u0010\u0011\u001a\u0006\u0012\u0002\b\u00030\bH\u0002J\u0010\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u0013H\u0007J\u0014\u0010\u0015\u001a\u00020\u00042\n\u0010\u0011\u001a\u0006\u0012\u0002\b\u00030\bH\u0002J\u0016\u0010\u0016\u001a\u00020\u00172\f\u0010\u0011\u001a\b\u0012\u0002\b\u0003\u0018\u00010\bH\u0002J\u0010\u0010\u0018\u001a\u00020\u00192\u0006\u0010\u000f\u001a\u00020\u0001H\u0007J\u0014\u0010\u001a\u001a\u00020\u00042\n\u0010\u0011\u001a\u0006\u0012\u0002\b\u00030\bH\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082T¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082T¢\u0006\u0002\n\u0000R\u001e\u0010\u0006\u001a\u0012\u0012\b\u0012\u0006\u0012\u0002\b\u00030\b\u0012\u0004\u0012\u00020\u00040\u0007X\u0082\u0004¢\u0006\u0002\n\u0000R,\u0010\t\u001a \u0012\b\u0012\u0006\u0012\u0002\b\u00030\b\u0012\u0012\u0012\u0010\u0012\f\u0012\n\u0012\u0006\b\u0001\u0012\u00020\f0\u000b0\n0\u0007X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u001b" }, d2 = { "Landroidx/lifecycle/Lifecycling;", "", "()V", "GENERATED_CALLBACK", "", "REFLECTIVE_CALLBACK", "callbackCache", "", "Ljava/lang/Class;", "classToAdapters", "", "Ljava/lang/reflect/Constructor;", "Landroidx/lifecycle/GeneratedAdapter;", "createGeneratedAdapter", "constructor", "object", "generatedConstructor", "klass", "getAdapterName", "", "className", "getObserverConstructorType", "isLifecycleParent", "", "lifecycleEventObserver", "Landroidx/lifecycle/LifecycleEventObserver;", "resolveObserverCallbackType", "lifecycle-common" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
public final class Lifecycling
{
    private static final int GENERATED_CALLBACK = 2;
    public static final Lifecycling INSTANCE;
    private static final int REFLECTIVE_CALLBACK = 1;
    private static final Map<Class<?>, Integer> callbackCache;
    private static final Map<Class<?>, List<Constructor<? extends GeneratedAdapter>>> classToAdapters;
    
    static {
        INSTANCE = new Lifecycling();
        callbackCache = new HashMap<Class<?>, Integer>();
        classToAdapters = new HashMap<Class<?>, List<Constructor<? extends GeneratedAdapter>>>();
    }
    
    private Lifecycling() {
    }
    
    private final GeneratedAdapter createGeneratedAdapter(final Constructor<? extends GeneratedAdapter> constructor, final Object o) {
        try {
            final GeneratedAdapter instance = (GeneratedAdapter)constructor.newInstance(o);
            Intrinsics.checkNotNullExpressionValue((Object)instance, "{\n            constructo\u2026tance(`object`)\n        }");
            return instance;
        }
        catch (final InvocationTargetException ex) {
            throw new RuntimeException(ex);
        }
        catch (final InstantiationException ex2) {
            throw new RuntimeException(ex2);
        }
        catch (final IllegalAccessException ex3) {
            throw new RuntimeException(ex3);
        }
    }
    
    private final Constructor<? extends GeneratedAdapter> generatedConstructor(final Class<?> clazz) {
        Constructor<?> declaredConstructor;
        try {
            final Package package1 = clazz.getPackage();
            String s = clazz.getCanonicalName();
            String name;
            if (package1 != null) {
                name = package1.getName();
            }
            else {
                name = "";
            }
            Intrinsics.checkNotNullExpressionValue((Object)name, "fullPackage");
            if (name.length() != 0) {
                Intrinsics.checkNotNullExpressionValue((Object)s, "name");
                s = s.substring(name.length() + 1);
                Intrinsics.checkNotNullExpressionValue((Object)s, "this as java.lang.String).substring(startIndex)");
            }
            Intrinsics.checkNotNullExpressionValue((Object)s, "if (fullPackage.isEmpty(\u2026g(fullPackage.length + 1)");
            final String adapterName = getAdapterName(s);
            String string;
            if (name.length() == 0) {
                string = adapterName;
            }
            else {
                final StringBuilder sb = new StringBuilder();
                sb.append(name);
                sb.append('.');
                sb.append(adapterName);
                string = sb.toString();
            }
            final Class<?> forName = Class.forName(string);
            Intrinsics.checkNotNull((Object)forName, "null cannot be cast to non-null type java.lang.Class<out androidx.lifecycle.GeneratedAdapter>");
            final Constructor<?> constructor = declaredConstructor = forName.getDeclaredConstructor(clazz);
            if (!constructor.isAccessible()) {
                constructor.setAccessible(true);
                declaredConstructor = constructor;
            }
        }
        catch (final NoSuchMethodException ex) {
            throw new RuntimeException(ex);
        }
        catch (final ClassNotFoundException ex2) {
            declaredConstructor = null;
        }
        return (Constructor<? extends GeneratedAdapter>)declaredConstructor;
    }
    
    @JvmStatic
    public static final String getAdapterName(final String s) {
        Intrinsics.checkNotNullParameter((Object)s, "className");
        final StringBuilder sb = new StringBuilder();
        sb.append(StringsKt.replace$default(s, ".", "_", false, 4, (Object)null));
        sb.append("_LifecycleAdapter");
        return sb.toString();
    }
    
    private final int getObserverConstructorType(final Class<?> clazz) {
        final Map<Class<?>, Integer> callbackCache = Lifecycling.callbackCache;
        final Integer n = callbackCache.get(clazz);
        if (n != null) {
            return n;
        }
        final int resolveObserverCallbackType = this.resolveObserverCallbackType(clazz);
        callbackCache.put(clazz, resolveObserverCallbackType);
        return resolveObserverCallbackType;
    }
    
    private final boolean isLifecycleParent(final Class<?> clazz) {
        return clazz != null && LifecycleObserver.class.isAssignableFrom(clazz);
    }
    
    @JvmStatic
    public static final LifecycleEventObserver lifecycleEventObserver(final Object o) {
        Intrinsics.checkNotNullParameter(o, "object");
        final boolean b = o instanceof LifecycleEventObserver;
        final boolean b2 = o instanceof DefaultLifecycleObserver;
        if (b && b2) {
            return new DefaultLifecycleObserverAdapter((DefaultLifecycleObserver)o, (LifecycleEventObserver)o);
        }
        if (b2) {
            return new DefaultLifecycleObserverAdapter((DefaultLifecycleObserver)o, null);
        }
        if (b) {
            return (LifecycleEventObserver)o;
        }
        final Class<?> class1 = o.getClass();
        final Lifecycling instance = Lifecycling.INSTANCE;
        if (instance.getObserverConstructorType(class1) != 2) {
            return new ReflectiveGenericLifecycleObserver(o);
        }
        final List<Constructor<? extends GeneratedAdapter>> value = Lifecycling.classToAdapters.get(class1);
        Intrinsics.checkNotNull((Object)value);
        final List<Constructor<? extends GeneratedAdapter>> list = value;
        final int size = list.size();
        int i = 0;
        if (size == 1) {
            return new SingleGeneratedAdapterObserver(instance.createGeneratedAdapter((Constructor<? extends GeneratedAdapter>)list.get(0), o));
        }
        final int size2 = list.size();
        final GeneratedAdapter[] array = new GeneratedAdapter[size2];
        while (i < size2) {
            array[i] = Lifecycling.INSTANCE.createGeneratedAdapter(list.get(i), o);
            ++i;
        }
        return new CompositeGeneratedAdaptersObserver(array);
    }
    
    private final int resolveObserverCallbackType(final Class<?> clazz) {
        if (clazz.getCanonicalName() == null) {
            return 1;
        }
        final Constructor<? extends GeneratedAdapter> generatedConstructor = this.generatedConstructor(clazz);
        if (generatedConstructor != null) {
            Lifecycling.classToAdapters.put(clazz, CollectionsKt.listOf((Object)generatedConstructor));
            return 2;
        }
        if (ClassesInfoCache.sInstance.hasLifecycleMethods(clazz)) {
            return 1;
        }
        final Class<?> superclass = clazz.getSuperclass();
        List list = null;
        if (this.isLifecycleParent(superclass)) {
            Intrinsics.checkNotNullExpressionValue((Object)superclass, "superclass");
            if (this.getObserverConstructorType(superclass) == 1) {
                return 1;
            }
            final List<Constructor<? extends GeneratedAdapter>> value = Lifecycling.classToAdapters.get(superclass);
            Intrinsics.checkNotNull((Object)value);
            list = new ArrayList(value);
        }
        final Class<?>[] interfaces = clazz.getInterfaces();
        Intrinsics.checkNotNullExpressionValue((Object)interfaces, "klass.interfaces");
        List list2;
        for (int i = 0; i < interfaces.length; ++i, list = list2) {
            final Class<?> clazz2 = interfaces[i];
            if (!this.isLifecycleParent(clazz2)) {
                list2 = list;
            }
            else {
                Intrinsics.checkNotNullExpressionValue((Object)clazz2, "intrface");
                if (this.getObserverConstructorType(clazz2) == 1) {
                    return 1;
                }
                if ((list2 = list) == null) {
                    list2 = new ArrayList();
                }
                final List<Constructor<? extends GeneratedAdapter>> value2 = Lifecycling.classToAdapters.get(clazz2);
                Intrinsics.checkNotNull((Object)value2);
                list2.addAll(value2);
            }
        }
        if (list != null) {
            Lifecycling.classToAdapters.put(clazz, list);
            return 2;
        }
        return 1;
    }
}

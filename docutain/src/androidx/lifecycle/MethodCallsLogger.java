// 
// Decompiled by Procyon v0.6.0
// 

package androidx.lifecycle;

import kotlin.jvm.internal.Intrinsics;
import java.util.HashMap;
import java.util.Map;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010%\n\u0002\u0010\u000e\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0003\b\u0017\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\u0018\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\u00052\u0006\u0010\n\u001a\u00020\u0006H\u0017R\u001a\u0010\u0003\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u000b" }, d2 = { "Landroidx/lifecycle/MethodCallsLogger;", "", "()V", "calledMethods", "", "", "", "approveCall", "", "name", "type", "lifecycle-common" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
public class MethodCallsLogger
{
    private final Map<String, Integer> calledMethods;
    
    public MethodCallsLogger() {
        this.calledMethods = new HashMap<String, Integer>();
    }
    
    public boolean approveCall(final String s, final int n) {
        Intrinsics.checkNotNullParameter((Object)s, "name");
        final Integer n2 = this.calledMethods.get(s);
        boolean b = false;
        int intValue;
        if (n2 != null) {
            intValue = n2;
        }
        else {
            intValue = 0;
        }
        if ((intValue & n) != 0x0) {
            b = true;
        }
        this.calledMethods.put(s, n | intValue);
        return b ^ true;
    }
}

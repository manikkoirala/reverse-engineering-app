// 
// Decompiled by Procyon v0.6.0
// 

package androidx.lifecycle.viewmodel;

import kotlin.Metadata;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;

@Retention(RetentionPolicy.RUNTIME)
@Metadata(d1 = { "\u0000\n\n\u0002\u0018\u0002\n\u0002\u0010\u001b\n\u0000\b\u0087\u0002\u0018\u00002\u00020\u0001B\u0000¨\u0006\u0002" }, d2 = { "Landroidx/lifecycle/viewmodel/ViewModelFactoryDsl;", "", "lifecycle-viewmodel_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
public @interface ViewModelFactoryDsl {
}

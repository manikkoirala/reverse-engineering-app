// 
// Decompiled by Procyon v0.6.0
// 

package androidx.lifecycle.viewmodel;

import java.util.Map;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\u0018\u00002\u00020\u0001B\u000f\u0012\b\b\u0002\u0010\u0002\u001a\u00020\u0001¢\u0006\u0002\u0010\u0003J$\u0010\u0004\u001a\u0004\u0018\u0001H\u0005\"\u0004\b\u0000\u0010\u00052\f\u0010\u0006\u001a\b\u0012\u0004\u0012\u0002H\u00050\u0007H\u0096\u0002¢\u0006\u0002\u0010\bJ*\u0010\t\u001a\u00020\n\"\u0004\b\u0000\u0010\u00052\f\u0010\u0006\u001a\b\u0012\u0004\u0012\u0002H\u00050\u00072\u0006\u0010\u000b\u001a\u0002H\u0005H\u0086\u0002¢\u0006\u0002\u0010\f¨\u0006\r" }, d2 = { "Landroidx/lifecycle/viewmodel/MutableCreationExtras;", "Landroidx/lifecycle/viewmodel/CreationExtras;", "initialExtras", "(Landroidx/lifecycle/viewmodel/CreationExtras;)V", "get", "T", "key", "Landroidx/lifecycle/viewmodel/CreationExtras$Key;", "(Landroidx/lifecycle/viewmodel/CreationExtras$Key;)Ljava/lang/Object;", "set", "", "t", "(Landroidx/lifecycle/viewmodel/CreationExtras$Key;Ljava/lang/Object;)V", "lifecycle-viewmodel_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
public final class MutableCreationExtras extends CreationExtras
{
    public MutableCreationExtras() {
        this(null, 1, null);
    }
    
    public MutableCreationExtras(final CreationExtras creationExtras) {
        Intrinsics.checkNotNullParameter((Object)creationExtras, "initialExtras");
        this.getMap$lifecycle_viewmodel_release().putAll(creationExtras.getMap$lifecycle_viewmodel_release());
    }
    
    @Override
    public <T> T get(final Key<T> key) {
        Intrinsics.checkNotNullParameter((Object)key, "key");
        return (T)this.getMap$lifecycle_viewmodel_release().get(key);
    }
    
    public final <T> void set(final Key<T> key, final T t) {
        Intrinsics.checkNotNullParameter((Object)key, "key");
        this.getMap$lifecycle_viewmodel_release().put((Key<?>)key, t);
    }
}

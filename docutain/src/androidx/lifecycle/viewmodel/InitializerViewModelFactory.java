// 
// Decompiled by Procyon v0.6.0
// 

package androidx.lifecycle.viewmodel;

import androidx.lifecycle.ViewModelProvider$Factory$_CC;
import androidx.lifecycle.ViewModel;
import kotlin.jvm.internal.Intrinsics;
import kotlin.Metadata;
import androidx.lifecycle.ViewModelProvider;

@Metadata(d1 = { "\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0011\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0000\u0018\u00002\u00020\u0001B!\u0012\u001a\u0010\u0002\u001a\u000e\u0012\n\b\u0001\u0012\u0006\u0012\u0002\b\u00030\u00040\u0003\"\u0006\u0012\u0002\b\u00030\u0004¢\u0006\u0002\u0010\u0005J-\u0010\u0007\u001a\u0002H\b\"\b\b\u0000\u0010\b*\u00020\t2\f\u0010\n\u001a\b\u0012\u0004\u0012\u0002H\b0\u000b2\u0006\u0010\f\u001a\u00020\rH\u0016¢\u0006\u0002\u0010\u000eR\u001c\u0010\u0002\u001a\u000e\u0012\n\b\u0001\u0012\u0006\u0012\u0002\b\u00030\u00040\u0003X\u0082\u0004¢\u0006\u0004\n\u0002\u0010\u0006¨\u0006\u000f" }, d2 = { "Landroidx/lifecycle/viewmodel/InitializerViewModelFactory;", "Landroidx/lifecycle/ViewModelProvider$Factory;", "initializers", "", "Landroidx/lifecycle/viewmodel/ViewModelInitializer;", "([Landroidx/lifecycle/viewmodel/ViewModelInitializer;)V", "[Landroidx/lifecycle/viewmodel/ViewModelInitializer;", "create", "T", "Landroidx/lifecycle/ViewModel;", "modelClass", "Ljava/lang/Class;", "extras", "Landroidx/lifecycle/viewmodel/CreationExtras;", "(Ljava/lang/Class;Landroidx/lifecycle/viewmodel/CreationExtras;)Landroidx/lifecycle/ViewModel;", "lifecycle-viewmodel_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
public final class InitializerViewModelFactory implements Factory
{
    private final ViewModelInitializer<?>[] initializers;
    
    public InitializerViewModelFactory(final ViewModelInitializer<?>... initializers) {
        Intrinsics.checkNotNullParameter((Object)initializers, "initializers");
        this.initializers = initializers;
    }
    
    @Override
    public <T extends ViewModel> T create(final Class<T> clazz, final CreationExtras creationExtras) {
        Intrinsics.checkNotNullParameter((Object)clazz, "modelClass");
        Intrinsics.checkNotNullParameter((Object)creationExtras, "extras");
        final ViewModelInitializer<?>[] initializers = this.initializers;
        final int length = initializers.length;
        int i = 0;
        ViewModel viewModel = null;
        while (i < length) {
            final ViewModelInitializer<?> viewModelInitializer = initializers[i];
            if (Intrinsics.areEqual((Object)viewModelInitializer.getClazz$lifecycle_viewmodel_release(), (Object)clazz)) {
                final Object invoke = viewModelInitializer.getInitializer$lifecycle_viewmodel_release().invoke((Object)creationExtras);
                if (invoke instanceof ViewModel) {
                    viewModel = (T)invoke;
                }
                else {
                    viewModel = null;
                }
            }
            ++i;
        }
        if (viewModel != null) {
            return (T)viewModel;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("No initializer set for given class ");
        sb.append(clazz.getName());
        throw new IllegalArgumentException(sb.toString());
    }
}

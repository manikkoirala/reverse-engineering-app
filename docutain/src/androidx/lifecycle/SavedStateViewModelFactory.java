// 
// Decompiled by Procyon v0.6.0
// 

package androidx.lifecycle;

import java.lang.reflect.Constructor;
import java.util.List;
import androidx.lifecycle.viewmodel.CreationExtras;
import kotlin.jvm.internal.Intrinsics;
import androidx.savedstate.SavedStateRegistryOwner;
import androidx.savedstate.SavedStateRegistry;
import android.os.Bundle;
import android.app.Application;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000Z\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u00012\u00020\u0002B\u0007\b\u0016¢\u0006\u0002\u0010\u0003B\u0019\b\u0016\u0012\b\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007¢\u0006\u0002\u0010\bB#\b\u0017\u0012\b\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\b\u0010\t\u001a\u0004\u0018\u00010\n¢\u0006\u0002\u0010\u000bJ%\u0010\u0011\u001a\u0002H\u0012\"\b\b\u0000\u0010\u0012*\u00020\u00132\f\u0010\u0014\u001a\b\u0012\u0004\u0012\u0002H\u00120\u0015H\u0016¢\u0006\u0002\u0010\u0016J-\u0010\u0011\u001a\u0002H\u0012\"\b\b\u0000\u0010\u0012*\u00020\u00132\f\u0010\u0014\u001a\b\u0012\u0004\u0012\u0002H\u00120\u00152\u0006\u0010\u0017\u001a\u00020\u0018H\u0016¢\u0006\u0002\u0010\u0019J+\u0010\u0011\u001a\u0002H\u0012\"\b\b\u0000\u0010\u0012*\u00020\u00132\u0006\u0010\u001a\u001a\u00020\u001b2\f\u0010\u0014\u001a\b\u0012\u0004\u0012\u0002H\u00120\u0015¢\u0006\u0002\u0010\u001cJ\u0010\u0010\u001d\u001a\u00020\u001e2\u0006\u0010\u001f\u001a\u00020\u0013H\u0017R\u0010\u0010\u0004\u001a\u0004\u0018\u00010\u0005X\u0082\u000e¢\u0006\u0002\n\u0000R\u0010\u0010\t\u001a\u0004\u0018\u00010\nX\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\u0002X\u0082\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\r\u001a\u0004\u0018\u00010\u000eX\u0082\u000e¢\u0006\u0002\n\u0000R\u0010\u0010\u000f\u001a\u0004\u0018\u00010\u0010X\u0082\u000e¢\u0006\u0002\n\u0000¨\u0006 " }, d2 = { "Landroidx/lifecycle/SavedStateViewModelFactory;", "Landroidx/lifecycle/ViewModelProvider$OnRequeryFactory;", "Landroidx/lifecycle/ViewModelProvider$Factory;", "()V", "application", "Landroid/app/Application;", "owner", "Landroidx/savedstate/SavedStateRegistryOwner;", "(Landroid/app/Application;Landroidx/savedstate/SavedStateRegistryOwner;)V", "defaultArgs", "Landroid/os/Bundle;", "(Landroid/app/Application;Landroidx/savedstate/SavedStateRegistryOwner;Landroid/os/Bundle;)V", "factory", "lifecycle", "Landroidx/lifecycle/Lifecycle;", "savedStateRegistry", "Landroidx/savedstate/SavedStateRegistry;", "create", "T", "Landroidx/lifecycle/ViewModel;", "modelClass", "Ljava/lang/Class;", "(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;", "extras", "Landroidx/lifecycle/viewmodel/CreationExtras;", "(Ljava/lang/Class;Landroidx/lifecycle/viewmodel/CreationExtras;)Landroidx/lifecycle/ViewModel;", "key", "", "(Ljava/lang/String;Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;", "onRequery", "", "viewModel", "lifecycle-viewmodel-savedstate_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
public final class SavedStateViewModelFactory extends OnRequeryFactory implements Factory
{
    private Application application;
    private Bundle defaultArgs;
    private final Factory factory;
    private Lifecycle lifecycle;
    private SavedStateRegistry savedStateRegistry;
    
    public SavedStateViewModelFactory() {
        this.factory = new ViewModelProvider.AndroidViewModelFactory();
    }
    
    public SavedStateViewModelFactory(final Application application, final SavedStateRegistryOwner savedStateRegistryOwner) {
        Intrinsics.checkNotNullParameter((Object)savedStateRegistryOwner, "owner");
        this(application, savedStateRegistryOwner, null);
    }
    
    public SavedStateViewModelFactory(final Application application, final SavedStateRegistryOwner savedStateRegistryOwner, final Bundle defaultArgs) {
        Intrinsics.checkNotNullParameter((Object)savedStateRegistryOwner, "owner");
        this.savedStateRegistry = savedStateRegistryOwner.getSavedStateRegistry();
        this.lifecycle = savedStateRegistryOwner.getLifecycle();
        this.defaultArgs = defaultArgs;
        this.application = application;
        Factory factory;
        if (application != null) {
            factory = AndroidViewModelFactory.Companion.getInstance(application);
        }
        else {
            factory = new ViewModelProvider.AndroidViewModelFactory();
        }
        this.factory = factory;
    }
    
    @Override
    public <T extends ViewModel> T create(final Class<T> clazz) {
        Intrinsics.checkNotNullParameter((Object)clazz, "modelClass");
        final String canonicalName = clazz.getCanonicalName();
        if (canonicalName != null) {
            return this.create(canonicalName, clazz);
        }
        throw new IllegalArgumentException("Local and anonymous classes can not be ViewModels");
    }
    
    @Override
    public <T extends ViewModel> T create(final Class<T> clazz, final CreationExtras creationExtras) {
        Intrinsics.checkNotNullParameter((Object)clazz, "modelClass");
        Intrinsics.checkNotNullParameter((Object)creationExtras, "extras");
        final String s = creationExtras.get(NewInstanceFactory.VIEW_MODEL_KEY);
        if (s != null) {
            ViewModel viewModel;
            if (creationExtras.get(SavedStateHandleSupport.SAVED_STATE_REGISTRY_OWNER_KEY) != null && creationExtras.get(SavedStateHandleSupport.VIEW_MODEL_STORE_OWNER_KEY) != null) {
                final Application application = creationExtras.get(AndroidViewModelFactory.APPLICATION_KEY);
                final boolean assignable = AndroidViewModel.class.isAssignableFrom(clazz);
                Constructor<T> constructor;
                if (assignable && application != null) {
                    constructor = SavedStateViewModelFactoryKt.findMatchingConstructor(clazz, SavedStateViewModelFactoryKt.access$getANDROID_VIEWMODEL_SIGNATURE$p());
                }
                else {
                    constructor = SavedStateViewModelFactoryKt.findMatchingConstructor(clazz, SavedStateViewModelFactoryKt.access$getVIEWMODEL_SIGNATURE$p());
                }
                if (constructor == null) {
                    return this.factory.create(clazz, creationExtras);
                }
                if (assignable && application != null) {
                    viewModel = SavedStateViewModelFactoryKt.newInstance(clazz, constructor, application, SavedStateHandleSupport.createSavedStateHandle(creationExtras));
                }
                else {
                    viewModel = SavedStateViewModelFactoryKt.newInstance(clazz, constructor, SavedStateHandleSupport.createSavedStateHandle(creationExtras));
                }
            }
            else {
                if (this.lifecycle == null) {
                    throw new IllegalStateException("SAVED_STATE_REGISTRY_OWNER_KEY andVIEW_MODEL_STORE_OWNER_KEY must be provided in the creation extras tosuccessfully create a ViewModel.");
                }
                viewModel = this.create(s, clazz);
            }
            return (T)viewModel;
        }
        throw new IllegalStateException("VIEW_MODEL_KEY must always be provided by ViewModelProvider");
    }
    
    public final <T extends ViewModel> T create(final String s, final Class<T> clazz) {
        Intrinsics.checkNotNullParameter((Object)s, "key");
        Intrinsics.checkNotNullParameter((Object)clazz, "modelClass");
        final Lifecycle lifecycle = this.lifecycle;
        if (lifecycle == null) {
            throw new UnsupportedOperationException("SavedStateViewModelFactory constructed with empty constructor supports only calls to create(modelClass: Class<T>, extras: CreationExtras).");
        }
        final boolean assignable = AndroidViewModel.class.isAssignableFrom(clazz);
        Constructor<T> constructor;
        if (assignable && this.application != null) {
            constructor = SavedStateViewModelFactoryKt.findMatchingConstructor(clazz, SavedStateViewModelFactoryKt.access$getANDROID_VIEWMODEL_SIGNATURE$p());
        }
        else {
            constructor = SavedStateViewModelFactoryKt.findMatchingConstructor(clazz, SavedStateViewModelFactoryKt.access$getVIEWMODEL_SIGNATURE$p());
        }
        if (constructor == null) {
            ViewModel viewModel;
            if (this.application != null) {
                viewModel = this.factory.create(clazz);
            }
            else {
                viewModel = NewInstanceFactory.Companion.getInstance().create(clazz);
            }
            return (T)viewModel;
        }
        final SavedStateRegistry savedStateRegistry = this.savedStateRegistry;
        Intrinsics.checkNotNull((Object)savedStateRegistry);
        final SavedStateHandleController create = LegacySavedStateHandleController.create(savedStateRegistry, lifecycle, s, this.defaultArgs);
        ViewModel viewModel2 = null;
        Label_0188: {
            if (assignable) {
                final Application application = this.application;
                if (application != null) {
                    Intrinsics.checkNotNull((Object)application);
                    viewModel2 = SavedStateViewModelFactoryKt.newInstance(clazz, constructor, application, create.getHandle());
                    break Label_0188;
                }
            }
            viewModel2 = SavedStateViewModelFactoryKt.newInstance(clazz, constructor, create.getHandle());
        }
        viewModel2.setTagIfAbsent("androidx.lifecycle.savedstate.vm.tag", create);
        return (T)viewModel2;
    }
    
    @Override
    public void onRequery(final ViewModel viewModel) {
        Intrinsics.checkNotNullParameter((Object)viewModel, "viewModel");
        if (this.lifecycle != null) {
            final SavedStateRegistry savedStateRegistry = this.savedStateRegistry;
            Intrinsics.checkNotNull((Object)savedStateRegistry);
            final Lifecycle lifecycle = this.lifecycle;
            Intrinsics.checkNotNull((Object)lifecycle);
            LegacySavedStateHandleController.attachHandleIfNeeded(viewModel, savedStateRegistry, lifecycle);
        }
    }
}

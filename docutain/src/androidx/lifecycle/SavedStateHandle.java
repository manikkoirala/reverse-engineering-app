// 
// Decompiled by Procyon v0.6.0
// 

package androidx.lifecycle;

import java.util.HashMap;
import kotlin.collections.SetsKt;
import kotlinx.coroutines.flow.FlowKt;
import kotlinx.coroutines.flow.StateFlowKt;
import kotlinx.coroutines.flow.StateFlow;
import java.util.Set;
import java.util.Iterator;
import androidx.core.os.BundleKt;
import kotlin.TuplesKt;
import kotlin.Pair;
import kotlin.collections.MapsKt;
import kotlin.jvm.JvmStatic;
import kotlin.jvm.internal.Intrinsics;
import java.util.LinkedHashMap;
import android.util.SparseArray;
import java.io.Serializable;
import android.os.Parcelable;
import java.util.ArrayList;
import android.os.Binder;
import android.util.SizeF;
import android.util.Size;
import android.os.Build$VERSION;
import kotlin.jvm.internal.DefaultConstructorMarker;
import android.os.Bundle;
import androidx.savedstate.SavedStateRegistry;
import kotlinx.coroutines.flow.MutableStateFlow;
import java.util.Map;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000X\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010$\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010%\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\"\n\u0002\b\t\u0018\u0000 *2\u00020\u0001:\u0002*+B\u001d\b\u0016\u0012\u0014\u0010\u0002\u001a\u0010\u0012\u0004\u0012\u00020\u0004\u0012\u0006\u0012\u0004\u0018\u00010\u00010\u0003¢\u0006\u0002\u0010\u0005B\u0007\b\u0016¢\u0006\u0002\u0010\u0006J\u0010\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u0004H\u0007J\u0011\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0012\u001a\u00020\u0004H\u0087\u0002J\u001e\u0010\u0015\u001a\u0004\u0018\u0001H\u0016\"\u0004\b\u0000\u0010\u00162\u0006\u0010\u0012\u001a\u00020\u0004H\u0087\u0002¢\u0006\u0002\u0010\u0017J\u001c\u0010\u0018\u001a\b\u0012\u0004\u0012\u0002H\u00160\u0019\"\u0004\b\u0000\u0010\u00162\u0006\u0010\u0012\u001a\u00020\u0004H\u0007J)\u0010\u0018\u001a\b\u0012\u0004\u0012\u0002H\u00160\u0019\"\u0004\b\u0000\u0010\u00162\u0006\u0010\u0012\u001a\u00020\u00042\u0006\u0010\u001a\u001a\u0002H\u0016H\u0007¢\u0006\u0002\u0010\u001bJ1\u0010\u001c\u001a\b\u0012\u0004\u0012\u0002H\u00160\u0019\"\u0004\b\u0000\u0010\u00162\u0006\u0010\u0012\u001a\u00020\u00042\u0006\u0010\u001d\u001a\u00020\u00142\u0006\u0010\u001a\u001a\u0002H\u0016H\u0002¢\u0006\u0002\u0010\u001eJ)\u0010\u001f\u001a\b\u0012\u0004\u0012\u0002H\u00160 \"\u0004\b\u0000\u0010\u00162\u0006\u0010\u0012\u001a\u00020\u00042\u0006\u0010\u001a\u001a\u0002H\u0016H\u0007¢\u0006\u0002\u0010!J\u000e\u0010\"\u001a\b\u0012\u0004\u0012\u00020\u00040#H\u0007J\u001d\u0010$\u001a\u0004\u0018\u0001H\u0016\"\u0004\b\u0000\u0010\u00162\u0006\u0010\u0012\u001a\u00020\u0004H\u0007¢\u0006\u0002\u0010\u0017J\b\u0010\r\u001a\u00020\u000eH\u0007J&\u0010%\u001a\u00020\u0011\"\u0004\b\u0000\u0010\u00162\u0006\u0010\u0012\u001a\u00020\u00042\b\u0010&\u001a\u0004\u0018\u0001H\u0016H\u0087\u0002¢\u0006\u0002\u0010'J\u0018\u0010(\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u00042\u0006\u0010)\u001a\u00020\u000eH\u0007R\"\u0010\u0007\u001a\u0016\u0012\u0004\u0012\u00020\u0004\u0012\f\u0012\n\u0012\u0006\u0012\u0004\u0018\u00010\u00010\t0\bX\u0082\u0004¢\u0006\u0002\n\u0000R\u001e\u0010\n\u001a\u0012\u0012\u0004\u0012\u00020\u0004\u0012\b\u0012\u0006\u0012\u0002\b\u00030\u000b0\bX\u0082\u0004¢\u0006\u0002\n\u0000R\u001c\u0010\f\u001a\u0010\u0012\u0004\u0012\u00020\u0004\u0012\u0006\u0012\u0004\u0018\u00010\u00010\bX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082\u0004¢\u0006\u0002\n\u0000R\u001a\u0010\u000f\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u000e0\bX\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006," }, d2 = { "Landroidx/lifecycle/SavedStateHandle;", "", "initialState", "", "", "(Ljava/util/Map;)V", "()V", "flows", "", "Lkotlinx/coroutines/flow/MutableStateFlow;", "liveDatas", "Landroidx/lifecycle/SavedStateHandle$SavingStateLiveData;", "regular", "savedStateProvider", "Landroidx/savedstate/SavedStateRegistry$SavedStateProvider;", "savedStateProviders", "clearSavedStateProvider", "", "key", "contains", "", "get", "T", "(Ljava/lang/String;)Ljava/lang/Object;", "getLiveData", "Landroidx/lifecycle/MutableLiveData;", "initialValue", "(Ljava/lang/String;Ljava/lang/Object;)Landroidx/lifecycle/MutableLiveData;", "getLiveDataInternal", "hasInitialValue", "(Ljava/lang/String;ZLjava/lang/Object;)Landroidx/lifecycle/MutableLiveData;", "getStateFlow", "Lkotlinx/coroutines/flow/StateFlow;", "(Ljava/lang/String;Ljava/lang/Object;)Lkotlinx/coroutines/flow/StateFlow;", "keys", "", "remove", "set", "value", "(Ljava/lang/String;Ljava/lang/Object;)V", "setSavedStateProvider", "provider", "Companion", "SavingStateLiveData", "lifecycle-viewmodel-savedstate_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
public final class SavedStateHandle
{
    private static final Class<?>[] ACCEPTABLE_CLASSES;
    public static final Companion Companion;
    private static final String KEYS = "keys";
    private static final String VALUES = "values";
    private final Map<String, MutableStateFlow<Object>> flows;
    private final Map<String, SavingStateLiveData<?>> liveDatas;
    private final Map<String, Object> regular;
    private final SavedStateRegistry.SavedStateProvider savedStateProvider;
    private final Map<String, SavedStateRegistry.SavedStateProvider> savedStateProviders;
    
    static {
        Companion = new Companion(null);
        final Class<Boolean> type = Boolean.TYPE;
        final Class<Double> type2 = Double.TYPE;
        final Class<Integer> type3 = Integer.TYPE;
        final Class<Long> type4 = Long.TYPE;
        final Class<Byte> type5 = Byte.TYPE;
        final Class<Character> type6 = Character.TYPE;
        final Class<Float> type7 = Float.TYPE;
        final Class<Short> type8 = Short.TYPE;
        Serializable type9;
        if (Build$VERSION.SDK_INT >= 21) {
            type9 = Size.class;
        }
        else {
            type9 = Integer.TYPE;
        }
        Serializable type10;
        if (Build$VERSION.SDK_INT >= 21) {
            type10 = SizeF.class;
        }
        else {
            type10 = Integer.TYPE;
        }
        ACCEPTABLE_CLASSES = new Class[] { type, boolean[].class, type2, double[].class, type3, int[].class, type4, long[].class, String.class, String[].class, Binder.class, Bundle.class, type5, byte[].class, type6, char[].class, CharSequence.class, CharSequence[].class, ArrayList.class, type7, float[].class, Parcelable.class, Parcelable[].class, Serializable.class, type8, short[].class, SparseArray.class, (Class)type9, (Class)type10 };
    }
    
    public SavedStateHandle() {
        this.regular = new LinkedHashMap<String, Object>();
        this.savedStateProviders = new LinkedHashMap<String, SavedStateRegistry.SavedStateProvider>();
        this.liveDatas = new LinkedHashMap<String, SavingStateLiveData<?>>();
        this.flows = new LinkedHashMap<String, MutableStateFlow<Object>>();
        this.savedStateProvider = new SavedStateHandle$$ExternalSyntheticLambda0(this);
    }
    
    public SavedStateHandle(final Map<String, ?> map) {
        Intrinsics.checkNotNullParameter((Object)map, "initialState");
        final Map regular = new LinkedHashMap();
        this.regular = regular;
        this.savedStateProviders = new LinkedHashMap<String, SavedStateRegistry.SavedStateProvider>();
        this.liveDatas = new LinkedHashMap<String, SavingStateLiveData<?>>();
        this.flows = new LinkedHashMap<String, MutableStateFlow<Object>>();
        this.savedStateProvider = new SavedStateHandle$$ExternalSyntheticLambda0(this);
        regular.putAll(map);
    }
    
    public static final /* synthetic */ Class[] access$getACCEPTABLE_CLASSES$cp() {
        return SavedStateHandle.ACCEPTABLE_CLASSES;
    }
    
    public static final /* synthetic */ Map access$getFlows$p(final SavedStateHandle savedStateHandle) {
        return savedStateHandle.flows;
    }
    
    public static final /* synthetic */ Map access$getRegular$p(final SavedStateHandle savedStateHandle) {
        return savedStateHandle.regular;
    }
    
    @JvmStatic
    public static final SavedStateHandle createHandle(final Bundle bundle, final Bundle bundle2) {
        return SavedStateHandle.Companion.createHandle(bundle, bundle2);
    }
    
    private final <T> MutableLiveData<T> getLiveDataInternal(final String s, final boolean b, final T t) {
        final SavingStateLiveData<?> value = this.liveDatas.get(s);
        MutableLiveData mutableLiveData;
        if (value instanceof MutableLiveData) {
            mutableLiveData = value;
        }
        else {
            mutableLiveData = null;
        }
        if (mutableLiveData != null) {
            return mutableLiveData;
        }
        SavingStateLiveData<T> savingStateLiveData;
        if (this.regular.containsKey(s)) {
            savingStateLiveData = new SavingStateLiveData<T>(this, s, (T)this.regular.get(s));
        }
        else if (b) {
            this.regular.put(s, t);
            savingStateLiveData = new SavingStateLiveData<T>(this, s, t);
        }
        else {
            savingStateLiveData = new SavingStateLiveData<T>(this, s);
        }
        this.liveDatas.put(s, savingStateLiveData);
        return savingStateLiveData;
    }
    
    private static final Bundle savedStateProvider$lambda$0(final SavedStateHandle savedStateHandle) {
        Intrinsics.checkNotNullParameter((Object)savedStateHandle, "this$0");
        for (final Map.Entry<String, V> entry : MapsKt.toMap((Map)savedStateHandle.savedStateProviders).entrySet()) {
            savedStateHandle.set(entry.getKey(), ((SavedStateRegistry.SavedStateProvider)entry.getValue()).saveState());
        }
        final Set<String> keySet = savedStateHandle.regular.keySet();
        final ArrayList list = new ArrayList(keySet.size());
        final ArrayList list2 = new ArrayList<Object>(list.size());
        for (final String e : keySet) {
            list.add((Object)e);
            list2.add(savedStateHandle.regular.get(e));
        }
        return BundleKt.bundleOf(TuplesKt.to((Object)"keys", (Object)list), TuplesKt.to((Object)"values", (Object)list2));
    }
    
    public final void clearSavedStateProvider(final String s) {
        Intrinsics.checkNotNullParameter((Object)s, "key");
        this.savedStateProviders.remove(s);
    }
    
    public final boolean contains(final String s) {
        Intrinsics.checkNotNullParameter((Object)s, "key");
        return this.regular.containsKey(s);
    }
    
    public final <T> T get(String value) {
        Intrinsics.checkNotNullParameter((Object)value, "key");
        try {
            value = this.regular.get(value);
        }
        catch (final ClassCastException ex) {
            this.remove(value);
            value = null;
        }
        return (T)value;
    }
    
    public final <T> MutableLiveData<T> getLiveData(final String s) {
        Intrinsics.checkNotNullParameter((Object)s, "key");
        final MutableLiveData<Object> liveDataInternal = this.getLiveDataInternal(s, false, (Object)null);
        Intrinsics.checkNotNull((Object)liveDataInternal, "null cannot be cast to non-null type androidx.lifecycle.MutableLiveData<T of androidx.lifecycle.SavedStateHandle.getLiveData>");
        return (MutableLiveData<T>)liveDataInternal;
    }
    
    public final <T> MutableLiveData<T> getLiveData(final String s, final T t) {
        Intrinsics.checkNotNullParameter((Object)s, "key");
        return this.getLiveDataInternal(s, true, t);
    }
    
    public final <T> StateFlow<T> getStateFlow(final String s, final T t) {
        Intrinsics.checkNotNullParameter((Object)s, "key");
        final Map<String, MutableStateFlow<Object>> flows = this.flows;
        MutableStateFlow mutableStateFlow;
        if ((mutableStateFlow = flows.get(s)) == null) {
            if (!this.regular.containsKey(s)) {
                this.regular.put(s, t);
            }
            mutableStateFlow = StateFlowKt.MutableStateFlow(this.regular.get(s));
            this.flows.put(s, (MutableStateFlow<Object>)mutableStateFlow);
            flows.put(s, (MutableStateFlow<Object>)mutableStateFlow);
        }
        final StateFlow stateFlow = FlowKt.asStateFlow((MutableStateFlow)mutableStateFlow);
        Intrinsics.checkNotNull((Object)stateFlow, "null cannot be cast to non-null type kotlinx.coroutines.flow.StateFlow<T of androidx.lifecycle.SavedStateHandle.getStateFlow>");
        return (StateFlow<T>)stateFlow;
    }
    
    public final Set<String> keys() {
        return SetsKt.plus(SetsKt.plus((Set)this.regular.keySet(), (Iterable)this.savedStateProviders.keySet()), (Iterable)this.liveDatas.keySet());
    }
    
    public final <T> T remove(final String s) {
        Intrinsics.checkNotNullParameter((Object)s, "key");
        final Object remove = this.regular.remove(s);
        final SavingStateLiveData savingStateLiveData = this.liveDatas.remove(s);
        if (savingStateLiveData != null) {
            savingStateLiveData.detach();
        }
        this.flows.remove(s);
        return (T)remove;
    }
    
    public final SavedStateRegistry.SavedStateProvider savedStateProvider() {
        return this.savedStateProvider;
    }
    
    public final <T> void set(final String s, final T t) {
        Intrinsics.checkNotNullParameter((Object)s, "key");
        if (SavedStateHandle.Companion.validateValue(t)) {
            final SavingStateLiveData<?> value = this.liveDatas.get(s);
            MutableLiveData mutableLiveData;
            if (value instanceof MutableLiveData) {
                mutableLiveData = value;
            }
            else {
                mutableLiveData = null;
            }
            if (mutableLiveData != null) {
                mutableLiveData.setValue(t);
            }
            else {
                this.regular.put(s, t);
            }
            final MutableStateFlow mutableStateFlow = this.flows.get(s);
            if (mutableStateFlow != null) {
                mutableStateFlow.setValue((Object)t);
            }
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Can't put value with type ");
        Intrinsics.checkNotNull((Object)t);
        sb.append(t.getClass());
        sb.append(" into saved state");
        throw new IllegalArgumentException(sb.toString());
    }
    
    public final void setSavedStateProvider(final String s, final SavedStateRegistry.SavedStateProvider savedStateProvider) {
        Intrinsics.checkNotNullParameter((Object)s, "key");
        Intrinsics.checkNotNullParameter((Object)savedStateProvider, "provider");
        this.savedStateProviders.put(s, savedStateProvider);
    }
    
    @Metadata(d1 = { "\u00006\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0011\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u001c\u0010\n\u001a\u00020\u000b2\b\u0010\f\u001a\u0004\u0018\u00010\r2\b\u0010\u000e\u001a\u0004\u0018\u00010\rH\u0007J\u0012\u0010\u000f\u001a\u00020\u00102\b\u0010\u0011\u001a\u0004\u0018\u00010\u0001H\u0007R \u0010\u0003\u001a\u0012\u0012\u000e\u0012\f\u0012\u0006\b\u0001\u0012\u00020\u0001\u0018\u00010\u00050\u0004X\u0082\u0004¢\u0006\u0004\n\u0002\u0010\u0006R\u000e\u0010\u0007\u001a\u00020\bX\u0082T¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\bX\u0082T¢\u0006\u0002\n\u0000¨\u0006\u0012" }, d2 = { "Landroidx/lifecycle/SavedStateHandle$Companion;", "", "()V", "ACCEPTABLE_CLASSES", "", "Ljava/lang/Class;", "[Ljava/lang/Class;", "KEYS", "", "VALUES", "createHandle", "Landroidx/lifecycle/SavedStateHandle;", "restoredState", "Landroid/os/Bundle;", "defaultState", "validateValue", "", "value", "lifecycle-viewmodel-savedstate_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
    public static final class Companion
    {
        private Companion() {
        }
        
        @JvmStatic
        public final SavedStateHandle createHandle(final Bundle bundle, final Bundle bundle2) {
            if (bundle == null) {
                SavedStateHandle savedStateHandle;
                if (bundle2 == null) {
                    savedStateHandle = new SavedStateHandle();
                }
                else {
                    final Map map = new HashMap();
                    for (final String s : bundle2.keySet()) {
                        Intrinsics.checkNotNullExpressionValue((Object)s, "key");
                        map.put(s, bundle2.get(s));
                    }
                    savedStateHandle = new SavedStateHandle(map);
                }
                return savedStateHandle;
            }
            final ArrayList parcelableArrayList = bundle.getParcelableArrayList("keys");
            final ArrayList parcelableArrayList2 = bundle.getParcelableArrayList("values");
            final int n = 0;
            if (parcelableArrayList != null && parcelableArrayList2 != null && parcelableArrayList.size() == parcelableArrayList2.size()) {
                final Map map2 = new LinkedHashMap();
                for (int size = parcelableArrayList.size(), i = n; i < size; ++i) {
                    final Object value = parcelableArrayList.get(i);
                    Intrinsics.checkNotNull(value, "null cannot be cast to non-null type kotlin.String");
                    map2.put(value, parcelableArrayList2.get(i));
                }
                return new SavedStateHandle(map2);
            }
            throw new IllegalStateException("Invalid bundle passed as restored state".toString());
        }
        
        public final boolean validateValue(final Object o) {
            if (o == null) {
                return true;
            }
            for (final Class clazz : SavedStateHandle.access$getACCEPTABLE_CLASSES$cp()) {
                Intrinsics.checkNotNull((Object)clazz);
                if (clazz.isInstance(o)) {
                    return true;
                }
            }
            return false;
        }
    }
    
    @Metadata(d1 = { "\u0000\"\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0002\b\u0003\b\u0000\u0018\u0000*\u0004\b\u0000\u0010\u00012\b\u0012\u0004\u0012\u0002H\u00010\u0002B!\b\u0016\u0012\b\u0010\u0003\u001a\u0004\u0018\u00010\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00028\u0000¢\u0006\u0002\u0010\bB\u0019\b\u0016\u0012\b\u0010\u0003\u001a\u0004\u0018\u00010\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006¢\u0006\u0002\u0010\tJ\u0006\u0010\n\u001a\u00020\u000bJ\u0015\u0010\f\u001a\u00020\u000b2\u0006\u0010\u0007\u001a\u00028\u0000H\u0016¢\u0006\u0002\u0010\rR\u0010\u0010\u0003\u001a\u0004\u0018\u00010\u0004X\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u000e¢\u0006\u0002\n\u0000¨\u0006\u000e" }, d2 = { "Landroidx/lifecycle/SavedStateHandle$SavingStateLiveData;", "T", "Landroidx/lifecycle/MutableLiveData;", "handle", "Landroidx/lifecycle/SavedStateHandle;", "key", "", "value", "(Landroidx/lifecycle/SavedStateHandle;Ljava/lang/String;Ljava/lang/Object;)V", "(Landroidx/lifecycle/SavedStateHandle;Ljava/lang/String;)V", "detach", "", "setValue", "(Ljava/lang/Object;)V", "lifecycle-viewmodel-savedstate_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
    public static final class SavingStateLiveData<T> extends MutableLiveData<T>
    {
        private SavedStateHandle handle;
        private String key;
        
        public SavingStateLiveData(final SavedStateHandle handle, final String key) {
            Intrinsics.checkNotNullParameter((Object)key, "key");
            this.key = key;
            this.handle = handle;
        }
        
        public SavingStateLiveData(final SavedStateHandle handle, final String key, final T t) {
            Intrinsics.checkNotNullParameter((Object)key, "key");
            super(t);
            this.key = key;
            this.handle = handle;
        }
        
        public final void detach() {
            this.handle = null;
        }
        
        @Override
        public void setValue(final T t) {
            final SavedStateHandle handle = this.handle;
            if (handle != null) {
                SavedStateHandle.access$getRegular$p(handle).put(this.key, t);
                final MutableStateFlow mutableStateFlow = SavedStateHandle.access$getFlows$p(handle).get(this.key);
                if (mutableStateFlow != null) {
                    mutableStateFlow.setValue((Object)t);
                }
            }
            super.setValue(t);
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.lifecycle;

import java.util.HashSet;
import java.util.Collection;
import java.util.Set;
import kotlin.jvm.internal.Intrinsics;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010%\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0010\"\n\u0002\b\u0003\b\u0016\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\u0006\u0010\u0007\u001a\u00020\bJ\u0013\u0010\t\u001a\u0004\u0018\u00010\u00062\u0006\u0010\n\u001a\u00020\u0005H\u0087\u0002J\u000e\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\u00050\fH\u0007J\u0018\u0010\r\u001a\u00020\b2\u0006\u0010\n\u001a\u00020\u00052\u0006\u0010\u000e\u001a\u00020\u0006H\u0007R\u001a\u0010\u0003\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u000f" }, d2 = { "Landroidx/lifecycle/ViewModelStore;", "", "()V", "map", "", "", "Landroidx/lifecycle/ViewModel;", "clear", "", "get", "key", "keys", "", "put", "viewModel", "lifecycle-viewmodel_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
public class ViewModelStore
{
    private final Map<String, ViewModel> map;
    
    public ViewModelStore() {
        this.map = new LinkedHashMap<String, ViewModel>();
    }
    
    public final void clear() {
        final Iterator<ViewModel> iterator = this.map.values().iterator();
        while (iterator.hasNext()) {
            iterator.next().clear();
        }
        this.map.clear();
    }
    
    public final ViewModel get(final String s) {
        Intrinsics.checkNotNullParameter((Object)s, "key");
        return this.map.get(s);
    }
    
    public final Set<String> keys() {
        return new HashSet<String>(this.map.keySet());
    }
    
    public final void put(final String s, final ViewModel viewModel) {
        Intrinsics.checkNotNullParameter((Object)s, "key");
        Intrinsics.checkNotNullParameter((Object)viewModel, "viewModel");
        final ViewModel viewModel2 = this.map.put(s, viewModel);
        if (viewModel2 != null) {
            viewModel2.onCleared();
        }
    }
}

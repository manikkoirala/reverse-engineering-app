// 
// Decompiled by Procyon v0.6.0
// 

package androidx.lifecycle;

import kotlin.jvm.JvmStatic;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import java.util.concurrent.atomic.AtomicReference;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\b&\u0018\u00002\u00020\u0001:\u0002\u0012\u0013B\u0005¢\u0006\u0002\u0010\u0002J\u0010\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0010H'J\u0010\u0010\u0011\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0010H'R\u0014\u0010\u0003\u001a\u00020\u00048gX¦\u0004¢\u0006\u0006\u001a\u0004\b\u0005\u0010\u0006R$\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00010\b8\u0006@\u0006X\u0087\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\t\u0010\n\"\u0004\b\u000b\u0010\f¨\u0006\u0014" }, d2 = { "Landroidx/lifecycle/Lifecycle;", "", "()V", "currentState", "Landroidx/lifecycle/Lifecycle$State;", "getCurrentState", "()Landroidx/lifecycle/Lifecycle$State;", "internalScopeRef", "Ljava/util/concurrent/atomic/AtomicReference;", "getInternalScopeRef", "()Ljava/util/concurrent/atomic/AtomicReference;", "setInternalScopeRef", "(Ljava/util/concurrent/atomic/AtomicReference;)V", "addObserver", "", "observer", "Landroidx/lifecycle/LifecycleObserver;", "removeObserver", "Event", "State", "lifecycle-common" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
public abstract class Lifecycle
{
    private AtomicReference<Object> internalScopeRef;
    
    public Lifecycle() {
        this.internalScopeRef = new AtomicReference<Object>();
    }
    
    public abstract void addObserver(final LifecycleObserver p0);
    
    public abstract State getCurrentState();
    
    public final AtomicReference<Object> getInternalScopeRef() {
        return this.internalScopeRef;
    }
    
    public abstract void removeObserver(final LifecycleObserver p0);
    
    public final void setInternalScopeRef(final AtomicReference<Object> internalScopeRef) {
        Intrinsics.checkNotNullParameter((Object)internalScopeRef, "<set-?>");
        this.internalScopeRef = internalScopeRef;
    }
    
    @Metadata(d1 = { "\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u000b\b\u0086\u0001\u0018\u0000 \u000e2\b\u0012\u0004\u0012\u00020\u00000\u0001:\u0001\u000eB\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u0011\u0010\u0003\u001a\u00020\u00048F¢\u0006\u0006\u001a\u0004\b\u0005\u0010\u0006j\u0002\b\u0007j\u0002\b\bj\u0002\b\tj\u0002\b\nj\u0002\b\u000bj\u0002\b\fj\u0002\b\r¨\u0006\u000f" }, d2 = { "Landroidx/lifecycle/Lifecycle$Event;", "", "(Ljava/lang/String;I)V", "targetState", "Landroidx/lifecycle/Lifecycle$State;", "getTargetState", "()Landroidx/lifecycle/Lifecycle$State;", "ON_CREATE", "ON_START", "ON_RESUME", "ON_PAUSE", "ON_STOP", "ON_DESTROY", "ON_ANY", "Companion", "lifecycle-common" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
    public enum Event
    {
        private static final Event[] $VALUES;
        public static final Companion Companion;
        
        ON_ANY, 
        ON_CREATE, 
        ON_DESTROY, 
        ON_PAUSE, 
        ON_RESUME, 
        ON_START, 
        ON_STOP;
        
        private static final /* synthetic */ Event[] $values() {
            return new Event[] { Event.ON_CREATE, Event.ON_START, Event.ON_RESUME, Event.ON_PAUSE, Event.ON_STOP, Event.ON_DESTROY, Event.ON_ANY };
        }
        
        static {
            $VALUES = $values();
            Companion = new Companion(null);
        }
        
        @JvmStatic
        public static final Event downFrom(final State state) {
            return Event.Companion.downFrom(state);
        }
        
        @JvmStatic
        public static final Event downTo(final State state) {
            return Event.Companion.downTo(state);
        }
        
        @JvmStatic
        public static final Event upFrom(final State state) {
            return Event.Companion.upFrom(state);
        }
        
        @JvmStatic
        public static final Event upTo(final State state) {
            return Event.Companion.upTo(state);
        }
        
        public final State getTargetState() {
            switch (WhenMappings.$EnumSwitchMapping$0[this.ordinal()]) {
                default: {
                    final StringBuilder sb = new StringBuilder();
                    sb.append(this);
                    sb.append(" has no target state");
                    throw new IllegalArgumentException(sb.toString());
                }
                case 6: {
                    return State.DESTROYED;
                }
                case 5: {
                    return State.RESUMED;
                }
                case 3:
                case 4: {
                    return State.STARTED;
                }
                case 1:
                case 2: {
                    return State.CREATED;
                }
            }
        }
        
        @Metadata(d1 = { "\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0012\u0010\u0003\u001a\u0004\u0018\u00010\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0007J\u0012\u0010\u0007\u001a\u0004\u0018\u00010\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0007J\u0012\u0010\b\u001a\u0004\u0018\u00010\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0007J\u0012\u0010\t\u001a\u0004\u0018\u00010\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0007¨\u0006\n" }, d2 = { "Landroidx/lifecycle/Lifecycle$Event$Companion;", "", "()V", "downFrom", "Landroidx/lifecycle/Lifecycle$Event;", "state", "Landroidx/lifecycle/Lifecycle$State;", "downTo", "upFrom", "upTo", "lifecycle-common" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
        public static final class Companion
        {
            private Companion() {
            }
            
            @JvmStatic
            public final Event downFrom(final State state) {
                Intrinsics.checkNotNullParameter((Object)state, "state");
                final int n = WhenMappings.$EnumSwitchMapping$0[state.ordinal()];
                Event event;
                if (n != 1) {
                    if (n != 2) {
                        if (n != 3) {
                            event = null;
                        }
                        else {
                            event = Event.ON_PAUSE;
                        }
                    }
                    else {
                        event = Event.ON_STOP;
                    }
                }
                else {
                    event = Event.ON_DESTROY;
                }
                return event;
            }
            
            @JvmStatic
            public final Event downTo(final State state) {
                Intrinsics.checkNotNullParameter((Object)state, "state");
                final int n = WhenMappings.$EnumSwitchMapping$0[state.ordinal()];
                Event event;
                if (n != 1) {
                    if (n != 2) {
                        if (n != 4) {
                            event = null;
                        }
                        else {
                            event = Event.ON_DESTROY;
                        }
                    }
                    else {
                        event = Event.ON_PAUSE;
                    }
                }
                else {
                    event = Event.ON_STOP;
                }
                return event;
            }
            
            @JvmStatic
            public final Event upFrom(final State state) {
                Intrinsics.checkNotNullParameter((Object)state, "state");
                final int n = WhenMappings.$EnumSwitchMapping$0[state.ordinal()];
                Event event;
                if (n != 1) {
                    if (n != 2) {
                        if (n != 5) {
                            event = null;
                        }
                        else {
                            event = Event.ON_CREATE;
                        }
                    }
                    else {
                        event = Event.ON_RESUME;
                    }
                }
                else {
                    event = Event.ON_START;
                }
                return event;
            }
            
            @JvmStatic
            public final Event upTo(final State state) {
                Intrinsics.checkNotNullParameter((Object)state, "state");
                final int n = WhenMappings.$EnumSwitchMapping$0[state.ordinal()];
                Event event;
                if (n != 1) {
                    if (n != 2) {
                        if (n != 3) {
                            event = null;
                        }
                        else {
                            event = Event.ON_RESUME;
                        }
                    }
                    else {
                        event = Event.ON_START;
                    }
                }
                else {
                    event = Event.ON_CREATE;
                }
                return event;
            }
        }
    }
    
    @Metadata(d1 = { "\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0007\b\u0086\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u000e\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0000j\u0002\b\u0006j\u0002\b\u0007j\u0002\b\bj\u0002\b\tj\u0002\b\n¨\u0006\u000b" }, d2 = { "Landroidx/lifecycle/Lifecycle$State;", "", "(Ljava/lang/String;I)V", "isAtLeast", "", "state", "DESTROYED", "INITIALIZED", "CREATED", "STARTED", "RESUMED", "lifecycle-common" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
    public enum State
    {
        private static final State[] $VALUES;
        
        CREATED, 
        DESTROYED, 
        INITIALIZED, 
        RESUMED, 
        STARTED;
        
        private static final /* synthetic */ State[] $values() {
            return new State[] { State.DESTROYED, State.INITIALIZED, State.CREATED, State.STARTED, State.RESUMED };
        }
        
        static {
            $VALUES = $values();
        }
        
        public final boolean isAtLeast(final State state) {
            Intrinsics.checkNotNullParameter((Object)state, "state");
            return this.compareTo((State)state) >= 0;
        }
    }
}

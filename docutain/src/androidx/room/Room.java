// 
// Decompiled by Procyon v0.6.0
// 

package androidx.room;

import kotlin.jvm.JvmStatic;
import kotlin.text.StringsKt;
import kotlin.jvm.internal.Intrinsics;
import android.content.Context;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0007\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J8\u0010\u0007\u001a\b\u0012\u0004\u0012\u0002H\t0\b\"\b\b\u0000\u0010\t*\u00020\n2\u0006\u0010\u000b\u001a\u00020\f2\f\u0010\r\u001a\b\u0012\u0004\u0012\u0002H\t0\u000e2\b\u0010\u000f\u001a\u0004\u0018\u00010\u0004H\u0007J/\u0010\u0010\u001a\u0002H\t\"\u0004\b\u0000\u0010\t\"\u0004\b\u0001\u0010\u00112\f\u0010\r\u001a\b\u0012\u0004\u0012\u0002H\u00110\u000e2\u0006\u0010\u0012\u001a\u00020\u0004H\u0007¢\u0006\u0002\u0010\u0013J.\u0010\u0014\u001a\b\u0012\u0004\u0012\u0002H\t0\b\"\b\b\u0000\u0010\t*\u00020\n2\u0006\u0010\u000b\u001a\u00020\f2\f\u0010\r\u001a\b\u0012\u0004\u0012\u0002H\t0\u000eH\u0007R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082T¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0080T¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004X\u0086T¢\u0006\u0002\n\u0000¨\u0006\u0015" }, d2 = { "Landroidx/room/Room;", "", "()V", "CURSOR_CONV_SUFFIX", "", "LOG_TAG", "MASTER_TABLE_NAME", "databaseBuilder", "Landroidx/room/RoomDatabase$Builder;", "T", "Landroidx/room/RoomDatabase;", "context", "Landroid/content/Context;", "klass", "Ljava/lang/Class;", "name", "getGeneratedImplementation", "C", "suffix", "(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Object;", "inMemoryDatabaseBuilder", "room-runtime_release" }, k = 1, mv = { 1, 7, 1 }, xi = 48)
public final class Room
{
    private static final String CURSOR_CONV_SUFFIX = "_CursorConverter";
    public static final Room INSTANCE;
    public static final String LOG_TAG = "ROOM";
    public static final String MASTER_TABLE_NAME = "room_master_table";
    
    static {
        INSTANCE = new Room();
    }
    
    private Room() {
    }
    
    @JvmStatic
    public static final <T extends RoomDatabase> RoomDatabase.Builder<T> databaseBuilder(final Context context, final Class<T> clazz, final String s) {
        Intrinsics.checkNotNullParameter((Object)context, "context");
        Intrinsics.checkNotNullParameter((Object)clazz, "klass");
        final CharSequence charSequence = s;
        if ((charSequence == null || StringsKt.isBlank(charSequence)) ^ true) {
            return (RoomDatabase.Builder<T>)new RoomDatabase.Builder(context, (Class<RoomDatabase>)clazz, s);
        }
        throw new IllegalArgumentException("Cannot build a database with null or empty name. If you are trying to create an in memory database, use Room.inMemoryDatabaseBuilder".toString());
    }
    
    @JvmStatic
    public static final <T, C> T getGeneratedImplementation(final Class<C> clazz, String string) {
        Intrinsics.checkNotNullParameter((Object)clazz, "klass");
        Intrinsics.checkNotNullParameter((Object)string, "suffix");
        final Package package1 = clazz.getPackage();
        Intrinsics.checkNotNull((Object)package1);
        final String name = package1.getName();
        String s = clazz.getCanonicalName();
        Intrinsics.checkNotNull((Object)s);
        Intrinsics.checkNotNullExpressionValue((Object)name, "fullPackage");
        final int length = name.length();
        final int n = 0;
        if (length != 0) {
            s = s.substring(name.length() + 1);
            Intrinsics.checkNotNullExpressionValue((Object)s, "this as java.lang.String).substring(startIndex)");
        }
        final StringBuilder sb = new StringBuilder();
        sb.append(StringsKt.replace$default(s, '.', '_', false, 4, (Object)null));
        sb.append(string);
        final String string2 = sb.toString();
        int n2 = n;
        try {
            if (name.length() == 0) {
                n2 = 1;
            }
            if (n2 != 0) {
                string = string2;
            }
            else {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append(name);
                sb2.append('.');
                sb2.append(string2);
                string = sb2.toString();
            }
            final Class<?> forName = Class.forName(string, true, clazz.getClassLoader());
            Intrinsics.checkNotNull((Object)forName, "null cannot be cast to non-null type java.lang.Class<T of androidx.room.Room.getGeneratedImplementation>");
            return (T)forName.newInstance();
        }
        catch (final InstantiationException ex) {
            final StringBuilder sb3 = new StringBuilder();
            sb3.append("Failed to create an instance of ");
            sb3.append(clazz);
            sb3.append(".canonicalName");
            throw new RuntimeException(sb3.toString());
        }
        catch (final IllegalAccessException ex2) {
            final StringBuilder sb4 = new StringBuilder();
            sb4.append("Cannot access the constructor ");
            sb4.append(clazz);
            sb4.append(".canonicalName");
            throw new RuntimeException(sb4.toString());
        }
        catch (final ClassNotFoundException ex3) {
            final StringBuilder sb5 = new StringBuilder();
            sb5.append("Cannot find implementation for ");
            sb5.append(clazz.getCanonicalName());
            sb5.append(". ");
            sb5.append(string2);
            sb5.append(" does not exist");
            throw new RuntimeException(sb5.toString());
        }
    }
    
    @JvmStatic
    public static final <T extends RoomDatabase> RoomDatabase.Builder<T> inMemoryDatabaseBuilder(final Context context, final Class<T> clazz) {
        Intrinsics.checkNotNullParameter((Object)context, "context");
        Intrinsics.checkNotNullParameter((Object)clazz, "klass");
        return (RoomDatabase.Builder<T>)new RoomDatabase.Builder(context, (Class<RoomDatabase>)clazz, null);
    }
}

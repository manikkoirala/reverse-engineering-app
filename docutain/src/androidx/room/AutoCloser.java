// 
// Decompiled by Procyon v0.6.0
// 

package androidx.room;

import kotlin.jvm.functions.Function1;
import java.io.IOException;
import kotlin.Unit;
import android.os.SystemClock;
import android.os.Looper;
import kotlin.jvm.internal.Intrinsics;
import java.util.concurrent.TimeUnit;
import kotlin.jvm.internal.DefaultConstructorMarker;
import android.os.Handler;
import java.util.concurrent.Executor;
import androidx.sqlite.db.SupportSQLiteOpenHelper;
import androidx.sqlite.db.SupportSQLiteDatabase;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000Z\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u000e\n\u0002\u0010\b\n\u0002\b\u0007\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0007\b\u0000\u0018\u0000 ?2\u00020\u0001:\u0001?B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007¢\u0006\u0002\u0010\bJ\u0006\u00103\u001a\u000204J\u0006\u00105\u001a\u000204J%\u00106\u001a\u0002H7\"\u0004\b\u0000\u001072\u0012\u00108\u001a\u000e\u0012\u0004\u0012\u00020\r\u0012\u0004\u0012\u0002H709¢\u0006\u0002\u0010:J\u0006\u0010;\u001a\u00020\rJ\u000e\u0010<\u001a\u0002042\u0006\u0010\u0012\u001a\u00020\u0013J\u000e\u0010=\u001a\u0002042\u0006\u0010>\u001a\u00020\u000bR\u000e\u0010\t\u001a\u00020\u0003X\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004¢\u0006\u0002\n\u0000R \u0010\f\u001a\u0004\u0018\u00010\r8\u0000@\u0000X\u0081\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u000e\u0010\u000f\"\u0004\b\u0010\u0010\u0011R\u001a\u0010\u0012\u001a\u00020\u0013X\u0086.¢\u0006\u000e\n\u0000\u001a\u0004\b\u0014\u0010\u0015\"\u0004\b\u0016\u0010\u0017R\u000e\u0010\u0018\u001a\u00020\u000bX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0019\u001a\u00020\u0007X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u001a\u001a\u00020\u001bX\u0082\u0004¢\u0006\u0002\n\u0000R\u0011\u0010\u001c\u001a\u00020\u001d8F¢\u0006\u0006\u001a\u0004\b\u001c\u0010\u001eR\u001e\u0010\u001f\u001a\u00020\u00038\u0000@\u0000X\u0081\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b \u0010!\"\u0004\b\"\u0010#R\u000e\u0010$\u001a\u00020\u0001X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010%\u001a\u00020\u001dX\u0082\u000e¢\u0006\u0002\n\u0000R\u001c\u0010&\u001a\u0004\u0018\u00010\u000bX\u0080\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b'\u0010(\"\u0004\b)\u0010*R\u001e\u0010+\u001a\u00020,8\u0000@\u0000X\u0081\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b-\u0010.\"\u0004\b/\u00100R\u0014\u00101\u001a\u00020,8AX\u0080\u0004¢\u0006\u0006\u001a\u0004\b2\u0010.¨\u0006@" }, d2 = { "Landroidx/room/AutoCloser;", "", "autoCloseTimeoutAmount", "", "autoCloseTimeUnit", "Ljava/util/concurrent/TimeUnit;", "autoCloseExecutor", "Ljava/util/concurrent/Executor;", "(JLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/Executor;)V", "autoCloseTimeoutInMs", "autoCloser", "Ljava/lang/Runnable;", "delegateDatabase", "Landroidx/sqlite/db/SupportSQLiteDatabase;", "getDelegateDatabase$room_runtime_release", "()Landroidx/sqlite/db/SupportSQLiteDatabase;", "setDelegateDatabase$room_runtime_release", "(Landroidx/sqlite/db/SupportSQLiteDatabase;)V", "delegateOpenHelper", "Landroidx/sqlite/db/SupportSQLiteOpenHelper;", "getDelegateOpenHelper", "()Landroidx/sqlite/db/SupportSQLiteOpenHelper;", "setDelegateOpenHelper", "(Landroidx/sqlite/db/SupportSQLiteOpenHelper;)V", "executeAutoCloser", "executor", "handler", "Landroid/os/Handler;", "isActive", "", "()Z", "lastDecrementRefCountTimeStamp", "getLastDecrementRefCountTimeStamp$room_runtime_release", "()J", "setLastDecrementRefCountTimeStamp$room_runtime_release", "(J)V", "lock", "manuallyClosed", "onAutoCloseCallback", "getOnAutoCloseCallback$room_runtime_release", "()Ljava/lang/Runnable;", "setOnAutoCloseCallback$room_runtime_release", "(Ljava/lang/Runnable;)V", "refCount", "", "getRefCount$room_runtime_release", "()I", "setRefCount$room_runtime_release", "(I)V", "refCountForTest", "getRefCountForTest$room_runtime_release", "closeDatabaseIfOpen", "", "decrementCountAndScheduleClose", "executeRefCountingFunction", "V", "block", "Lkotlin/Function1;", "(Lkotlin/jvm/functions/Function1;)Ljava/lang/Object;", "incrementCountAndEnsureDbIsOpen", "init", "setAutoCloseCallback", "onAutoClose", "Companion", "room-runtime_release" }, k = 1, mv = { 1, 7, 1 }, xi = 48)
public final class AutoCloser
{
    public static final Companion Companion;
    public static final String autoCloseBug = "https://issuetracker.google.com/issues/new?component=413107&template=1096568";
    private long autoCloseTimeoutInMs;
    private final Runnable autoCloser;
    private SupportSQLiteDatabase delegateDatabase;
    public SupportSQLiteOpenHelper delegateOpenHelper;
    private final Runnable executeAutoCloser;
    private final Executor executor;
    private final Handler handler;
    private long lastDecrementRefCountTimeStamp;
    private final Object lock;
    private boolean manuallyClosed;
    private Runnable onAutoCloseCallback;
    private int refCount;
    
    static {
        Companion = new Companion(null);
    }
    
    public AutoCloser(final long duration, final TimeUnit timeUnit, final Executor executor) {
        Intrinsics.checkNotNullParameter((Object)timeUnit, "autoCloseTimeUnit");
        Intrinsics.checkNotNullParameter((Object)executor, "autoCloseExecutor");
        this.handler = new Handler(Looper.getMainLooper());
        this.lock = new Object();
        this.autoCloseTimeoutInMs = timeUnit.toMillis(duration);
        this.executor = executor;
        this.lastDecrementRefCountTimeStamp = SystemClock.uptimeMillis();
        this.executeAutoCloser = new AutoCloser$$ExternalSyntheticLambda0(this);
        this.autoCloser = new AutoCloser$$ExternalSyntheticLambda1(this);
    }
    
    private static final void autoCloser$lambda$3(final AutoCloser autoCloser) {
        Intrinsics.checkNotNullParameter((Object)autoCloser, "this$0");
        synchronized (autoCloser.lock) {
            if (SystemClock.uptimeMillis() - autoCloser.lastDecrementRefCountTimeStamp < autoCloser.autoCloseTimeoutInMs) {
                return;
            }
            if (autoCloser.refCount != 0) {
                return;
            }
            final Runnable onAutoCloseCallback = autoCloser.onAutoCloseCallback;
            Unit instance;
            if (onAutoCloseCallback != null) {
                onAutoCloseCallback.run();
                instance = Unit.INSTANCE;
            }
            else {
                instance = null;
            }
            if (instance != null) {
                final SupportSQLiteDatabase delegateDatabase = autoCloser.delegateDatabase;
                if (delegateDatabase != null && delegateDatabase.isOpen()) {
                    delegateDatabase.close();
                }
                autoCloser.delegateDatabase = null;
                final Unit instance2 = Unit.INSTANCE;
                return;
            }
            throw new IllegalStateException("onAutoCloseCallback is null but it should have been set before use. Please file a bug against Room at: https://issuetracker.google.com/issues/new?component=413107&template=1096568".toString());
        }
    }
    
    private static final void executeAutoCloser$lambda$0(final AutoCloser autoCloser) {
        Intrinsics.checkNotNullParameter((Object)autoCloser, "this$0");
        autoCloser.executor.execute(autoCloser.autoCloser);
    }
    
    public final void closeDatabaseIfOpen() throws IOException {
        synchronized (this.lock) {
            this.manuallyClosed = true;
            final SupportSQLiteDatabase delegateDatabase = this.delegateDatabase;
            if (delegateDatabase != null) {
                delegateDatabase.close();
            }
            this.delegateDatabase = null;
            final Unit instance = Unit.INSTANCE;
        }
    }
    
    public final void decrementCountAndScheduleClose() {
        synchronized (this.lock) {
            final int refCount = this.refCount;
            if (refCount > 0) {
                if ((this.refCount = refCount - 1) == 0) {
                    if (this.delegateDatabase == null) {
                        return;
                    }
                    this.handler.postDelayed(this.executeAutoCloser, this.autoCloseTimeoutInMs);
                }
                final Unit instance = Unit.INSTANCE;
                return;
            }
            throw new IllegalStateException("ref count is 0 or lower but we're supposed to decrement".toString());
        }
    }
    
    public final <V> V executeRefCountingFunction(final Function1<? super SupportSQLiteDatabase, ? extends V> function1) {
        Intrinsics.checkNotNullParameter((Object)function1, "block");
        try {
            return (V)function1.invoke((Object)this.incrementCountAndEnsureDbIsOpen());
        }
        finally {
            this.decrementCountAndScheduleClose();
        }
    }
    
    public final SupportSQLiteDatabase getDelegateDatabase$room_runtime_release() {
        return this.delegateDatabase;
    }
    
    public final SupportSQLiteOpenHelper getDelegateOpenHelper() {
        final SupportSQLiteOpenHelper delegateOpenHelper = this.delegateOpenHelper;
        if (delegateOpenHelper != null) {
            return delegateOpenHelper;
        }
        Intrinsics.throwUninitializedPropertyAccessException("delegateOpenHelper");
        return null;
    }
    
    public final long getLastDecrementRefCountTimeStamp$room_runtime_release() {
        return this.lastDecrementRefCountTimeStamp;
    }
    
    public final Runnable getOnAutoCloseCallback$room_runtime_release() {
        return this.onAutoCloseCallback;
    }
    
    public final int getRefCount$room_runtime_release() {
        return this.refCount;
    }
    
    public final int getRefCountForTest$room_runtime_release() {
        synchronized (this.lock) {
            return this.refCount;
        }
    }
    
    public final SupportSQLiteDatabase incrementCountAndEnsureDbIsOpen() {
        synchronized (this.lock) {
            this.handler.removeCallbacks(this.executeAutoCloser);
            ++this.refCount;
            if (!(this.manuallyClosed ^ true)) {
                throw new IllegalStateException("Attempting to open already closed database.".toString());
            }
            final SupportSQLiteDatabase delegateDatabase = this.delegateDatabase;
            if (delegateDatabase != null && delegateDatabase.isOpen()) {
                return delegateDatabase;
            }
            return this.delegateDatabase = this.getDelegateOpenHelper().getWritableDatabase();
        }
    }
    
    public final void init(final SupportSQLiteOpenHelper delegateOpenHelper) {
        Intrinsics.checkNotNullParameter((Object)delegateOpenHelper, "delegateOpenHelper");
        this.setDelegateOpenHelper(delegateOpenHelper);
    }
    
    public final boolean isActive() {
        return this.manuallyClosed ^ true;
    }
    
    public final void setAutoCloseCallback(final Runnable onAutoCloseCallback) {
        Intrinsics.checkNotNullParameter((Object)onAutoCloseCallback, "onAutoClose");
        this.onAutoCloseCallback = onAutoCloseCallback;
    }
    
    public final void setDelegateDatabase$room_runtime_release(final SupportSQLiteDatabase delegateDatabase) {
        this.delegateDatabase = delegateDatabase;
    }
    
    public final void setDelegateOpenHelper(final SupportSQLiteOpenHelper delegateOpenHelper) {
        Intrinsics.checkNotNullParameter((Object)delegateOpenHelper, "<set-?>");
        this.delegateOpenHelper = delegateOpenHelper;
    }
    
    public final void setLastDecrementRefCountTimeStamp$room_runtime_release(final long lastDecrementRefCountTimeStamp) {
        this.lastDecrementRefCountTimeStamp = lastDecrementRefCountTimeStamp;
    }
    
    public final void setOnAutoCloseCallback$room_runtime_release(final Runnable onAutoCloseCallback) {
        this.onAutoCloseCallback = onAutoCloseCallback;
    }
    
    public final void setRefCount$room_runtime_release(final int refCount) {
        this.refCount = refCount;
    }
    
    @Metadata(d1 = { "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0086T¢\u0006\u0002\n\u0000¨\u0006\u0005" }, d2 = { "Landroidx/room/AutoCloser$Companion;", "", "()V", "autoCloseBug", "", "room-runtime_release" }, k = 1, mv = { 1, 7, 1 }, xi = 48)
    public static final class Companion
    {
        private Companion() {
        }
    }
}

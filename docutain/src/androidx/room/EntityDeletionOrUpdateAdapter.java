// 
// Decompiled by Procyon v0.6.0
// 

package androidx.room;

import java.util.Iterator;
import androidx.sqlite.db.SupportSQLiteStatement;
import kotlin.jvm.internal.Intrinsics;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000<\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u0011\n\u0000\n\u0002\u0010\u001c\n\u0000\b'\u0018\u0000*\u0004\b\u0000\u0010\u00012\u00020\u0002B\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004¢\u0006\u0002\u0010\u0005J\u001d\u0010\u0006\u001a\u00020\u00072\u0006\u0010\b\u001a\u00020\t2\u0006\u0010\n\u001a\u00028\u0000H$¢\u0006\u0002\u0010\u000bJ\b\u0010\f\u001a\u00020\rH$J\u0013\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\n\u001a\u00028\u0000¢\u0006\u0002\u0010\u0010J\u001b\u0010\u0011\u001a\u00020\u000f2\u000e\u0010\u0012\u001a\n\u0012\u0006\b\u0001\u0012\u00028\u00000\u0013¢\u0006\u0002\u0010\u0014J\u0014\u0010\u0011\u001a\u00020\u000f2\f\u0010\u0012\u001a\b\u0012\u0004\u0012\u00028\u00000\u0015¨\u0006\u0016" }, d2 = { "Landroidx/room/EntityDeletionOrUpdateAdapter;", "T", "Landroidx/room/SharedSQLiteStatement;", "database", "Landroidx/room/RoomDatabase;", "(Landroidx/room/RoomDatabase;)V", "bind", "", "statement", "Landroidx/sqlite/db/SupportSQLiteStatement;", "entity", "(Landroidx/sqlite/db/SupportSQLiteStatement;Ljava/lang/Object;)V", "createQuery", "", "handle", "", "(Ljava/lang/Object;)I", "handleMultiple", "entities", "", "([Ljava/lang/Object;)I", "", "room-runtime_release" }, k = 1, mv = { 1, 7, 1 }, xi = 48)
public abstract class EntityDeletionOrUpdateAdapter<T> extends SharedSQLiteStatement
{
    public EntityDeletionOrUpdateAdapter(final RoomDatabase roomDatabase) {
        Intrinsics.checkNotNullParameter((Object)roomDatabase, "database");
        super(roomDatabase);
    }
    
    protected abstract void bind(final SupportSQLiteStatement p0, final T p1);
    
    @Override
    protected abstract String createQuery();
    
    public final int handle(final T t) {
        final SupportSQLiteStatement acquire = this.acquire();
        try {
            this.bind(acquire, t);
            return acquire.executeUpdateDelete();
        }
        finally {
            this.release(acquire);
        }
    }
    
    public final int handleMultiple(final Iterable<? extends T> iterable) {
        Intrinsics.checkNotNullParameter((Object)iterable, "entities");
        final SupportSQLiteStatement acquire = this.acquire();
        int n = 0;
        try {
            final Iterator<? extends T> iterator = iterable.iterator();
            while (iterator.hasNext()) {
                this.bind(acquire, iterator.next());
                n += acquire.executeUpdateDelete();
            }
            return n;
        }
        finally {
            this.release(acquire);
        }
    }
    
    public final int handleMultiple(final T[] array) {
        Intrinsics.checkNotNullParameter((Object)array, "entities");
        final SupportSQLiteStatement acquire = this.acquire();
        try {
            final int length = array.length;
            int i = 0;
            int n = 0;
            while (i < length) {
                this.bind(acquire, array[i]);
                n += acquire.executeUpdateDelete();
                ++i;
            }
            return n;
        }
        finally {
            this.release(acquire);
        }
    }
}

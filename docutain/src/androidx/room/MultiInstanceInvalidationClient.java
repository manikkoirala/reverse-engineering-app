// 
// Decompiled by Procyon v0.6.0
// 

package androidx.room;

import android.os.RemoteException;
import android.util.Log;
import java.util.Set;
import java.util.Collection;
import kotlin.jvm.internal.Intrinsics;
import android.content.Intent;
import java.util.concurrent.atomic.AtomicBoolean;
import android.content.ServiceConnection;
import java.util.concurrent.Executor;
import android.content.Context;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000h\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u000b\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0000\b\u0000\u0018\u00002\u00020\u0001B-\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b¢\u0006\u0002\u0010\fJ\u0006\u00109\u001a\u00020:R\u0016\u0010\r\u001a\n \u000e*\u0004\u0018\u00010\u00030\u0003X\u0082\u0004¢\u0006\u0002\n\u0000R\u0011\u0010\u000f\u001a\u00020\u0010¢\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\u0012R\u001a\u0010\u0013\u001a\u00020\u0014X\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0015\u0010\u0016\"\u0004\b\u0017\u0010\u0018R\u0011\u0010\n\u001a\u00020\u000b¢\u0006\b\n\u0000\u001a\u0004\b\u0019\u0010\u001aR\u0011\u0010\b\u001a\u00020\t¢\u0006\b\n\u0000\u001a\u0004\b\u001b\u0010\u001cR\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u001d\u0010\u001eR\u001a\u0010\u001f\u001a\u00020 X\u0086.¢\u0006\u000e\n\u0000\u001a\u0004\b!\u0010\"\"\u0004\b#\u0010$R\u0011\u0010%\u001a\u00020&¢\u0006\b\n\u0000\u001a\u0004\b'\u0010(R\u001c\u0010)\u001a\u0004\u0018\u00010*X\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b+\u0010,\"\u0004\b-\u0010.R\u0011\u0010/\u001a\u000200¢\u0006\b\n\u0000\u001a\u0004\b1\u00102R\u0011\u00103\u001a\u00020&¢\u0006\b\n\u0000\u001a\u0004\b4\u0010(R\u0011\u00105\u001a\u000206¢\u0006\b\n\u0000\u001a\u0004\b7\u00108¨\u0006;" }, d2 = { "Landroidx/room/MultiInstanceInvalidationClient;", "", "context", "Landroid/content/Context;", "name", "", "serviceIntent", "Landroid/content/Intent;", "invalidationTracker", "Landroidx/room/InvalidationTracker;", "executor", "Ljava/util/concurrent/Executor;", "(Landroid/content/Context;Ljava/lang/String;Landroid/content/Intent;Landroidx/room/InvalidationTracker;Ljava/util/concurrent/Executor;)V", "appContext", "kotlin.jvm.PlatformType", "callback", "Landroidx/room/IMultiInstanceInvalidationCallback;", "getCallback", "()Landroidx/room/IMultiInstanceInvalidationCallback;", "clientId", "", "getClientId", "()I", "setClientId", "(I)V", "getExecutor", "()Ljava/util/concurrent/Executor;", "getInvalidationTracker", "()Landroidx/room/InvalidationTracker;", "getName", "()Ljava/lang/String;", "observer", "Landroidx/room/InvalidationTracker$Observer;", "getObserver", "()Landroidx/room/InvalidationTracker$Observer;", "setObserver", "(Landroidx/room/InvalidationTracker$Observer;)V", "removeObserverRunnable", "Ljava/lang/Runnable;", "getRemoveObserverRunnable", "()Ljava/lang/Runnable;", "service", "Landroidx/room/IMultiInstanceInvalidationService;", "getService", "()Landroidx/room/IMultiInstanceInvalidationService;", "setService", "(Landroidx/room/IMultiInstanceInvalidationService;)V", "serviceConnection", "Landroid/content/ServiceConnection;", "getServiceConnection", "()Landroid/content/ServiceConnection;", "setUpRunnable", "getSetUpRunnable", "stopped", "Ljava/util/concurrent/atomic/AtomicBoolean;", "getStopped", "()Ljava/util/concurrent/atomic/AtomicBoolean;", "stop", "", "room-runtime_release" }, k = 1, mv = { 1, 7, 1 }, xi = 48)
public final class MultiInstanceInvalidationClient
{
    private final Context appContext;
    private final IMultiInstanceInvalidationCallback callback;
    private int clientId;
    private final Executor executor;
    private final InvalidationTracker invalidationTracker;
    private final String name;
    public InvalidationTracker.Observer observer;
    private final Runnable removeObserverRunnable;
    private IMultiInstanceInvalidationService service;
    private final ServiceConnection serviceConnection;
    private final Runnable setUpRunnable;
    private final AtomicBoolean stopped;
    
    public MultiInstanceInvalidationClient(Context applicationContext, final String name, final Intent intent, final InvalidationTracker invalidationTracker, final Executor executor) {
        Intrinsics.checkNotNullParameter((Object)applicationContext, "context");
        Intrinsics.checkNotNullParameter((Object)name, "name");
        Intrinsics.checkNotNullParameter((Object)intent, "serviceIntent");
        Intrinsics.checkNotNullParameter((Object)invalidationTracker, "invalidationTracker");
        Intrinsics.checkNotNullParameter((Object)executor, "executor");
        this.name = name;
        this.invalidationTracker = invalidationTracker;
        this.executor = executor;
        applicationContext = applicationContext.getApplicationContext();
        this.appContext = applicationContext;
        this.callback = (IMultiInstanceInvalidationCallback)new MultiInstanceInvalidationClient$callback.MultiInstanceInvalidationClient$callback$1(this);
        this.stopped = new AtomicBoolean(false);
        final ServiceConnection serviceConnection = (ServiceConnection)new MultiInstanceInvalidationClient$serviceConnection.MultiInstanceInvalidationClient$serviceConnection$1(this);
        this.serviceConnection = serviceConnection;
        this.setUpRunnable = new MultiInstanceInvalidationClient$$ExternalSyntheticLambda0(this);
        this.removeObserverRunnable = new MultiInstanceInvalidationClient$$ExternalSyntheticLambda1(this);
        final String[] array = invalidationTracker.getTableIdLookup$room_runtime_release().keySet().toArray(new String[0]);
        Intrinsics.checkNotNull((Object)array, "null cannot be cast to non-null type kotlin.Array<T of kotlin.collections.ArraysKt__ArraysJVMKt.toTypedArray>");
        this.setObserver(new InvalidationTracker.Observer(this, (String[])array) {
            final MultiInstanceInvalidationClient this$0;
            
            @Override
            public boolean isRemote$room_runtime_release() {
                return true;
            }
            
            @Override
            public void onInvalidated(final Set<String> set) {
                Intrinsics.checkNotNullParameter((Object)set, "tables");
                if (this.this$0.getStopped().get()) {
                    return;
                }
                try {
                    final IMultiInstanceInvalidationService service = this.this$0.getService();
                    if (service != null) {
                        final int clientId = this.this$0.getClientId();
                        final String[] array = set.toArray(new String[0]);
                        Intrinsics.checkNotNull((Object)array, "null cannot be cast to non-null type kotlin.Array<T of kotlin.collections.ArraysKt__ArraysJVMKt.toTypedArray>");
                        service.broadcastInvalidation(clientId, array);
                    }
                }
                catch (final RemoteException ex) {
                    Log.w("ROOM", "Cannot broadcast invalidation", (Throwable)ex);
                }
            }
        });
        applicationContext.bindService(intent, serviceConnection, 1);
    }
    
    private static final void removeObserverRunnable$lambda$2(final MultiInstanceInvalidationClient multiInstanceInvalidationClient) {
        Intrinsics.checkNotNullParameter((Object)multiInstanceInvalidationClient, "this$0");
        multiInstanceInvalidationClient.invalidationTracker.removeObserver(multiInstanceInvalidationClient.getObserver());
    }
    
    private static final void setUpRunnable$lambda$1(final MultiInstanceInvalidationClient multiInstanceInvalidationClient) {
        Intrinsics.checkNotNullParameter((Object)multiInstanceInvalidationClient, "this$0");
        try {
            final IMultiInstanceInvalidationService service = multiInstanceInvalidationClient.service;
            if (service != null) {
                multiInstanceInvalidationClient.clientId = service.registerCallback(multiInstanceInvalidationClient.callback, multiInstanceInvalidationClient.name);
                multiInstanceInvalidationClient.invalidationTracker.addObserver(multiInstanceInvalidationClient.getObserver());
            }
        }
        catch (final RemoteException ex) {
            Log.w("ROOM", "Cannot register multi-instance invalidation callback", (Throwable)ex);
        }
    }
    
    public final IMultiInstanceInvalidationCallback getCallback() {
        return this.callback;
    }
    
    public final int getClientId() {
        return this.clientId;
    }
    
    public final Executor getExecutor() {
        return this.executor;
    }
    
    public final InvalidationTracker getInvalidationTracker() {
        return this.invalidationTracker;
    }
    
    public final String getName() {
        return this.name;
    }
    
    public final InvalidationTracker.Observer getObserver() {
        final InvalidationTracker.Observer observer = this.observer;
        if (observer != null) {
            return observer;
        }
        Intrinsics.throwUninitializedPropertyAccessException("observer");
        return null;
    }
    
    public final Runnable getRemoveObserverRunnable() {
        return this.removeObserverRunnable;
    }
    
    public final IMultiInstanceInvalidationService getService() {
        return this.service;
    }
    
    public final ServiceConnection getServiceConnection() {
        return this.serviceConnection;
    }
    
    public final Runnable getSetUpRunnable() {
        return this.setUpRunnable;
    }
    
    public final AtomicBoolean getStopped() {
        return this.stopped;
    }
    
    public final void setClientId(final int clientId) {
        this.clientId = clientId;
    }
    
    public final void setObserver(final InvalidationTracker.Observer observer) {
        Intrinsics.checkNotNullParameter((Object)observer, "<set-?>");
        this.observer = observer;
    }
    
    public final void setService(final IMultiInstanceInvalidationService service) {
        this.service = service;
    }
    
    public final void stop() {
        if (this.stopped.compareAndSet(false, true)) {
            this.invalidationTracker.removeObserver(this.getObserver());
            try {
                final IMultiInstanceInvalidationService service = this.service;
                if (service != null) {
                    service.unregisterCallback(this.callback, this.clientId);
                }
            }
            catch (final RemoteException ex) {
                Log.w("ROOM", "Cannot unregister multi-instance invalidation callback", (Throwable)ex);
            }
            this.appContext.unbindService(this.serviceConnection);
        }
    }
}

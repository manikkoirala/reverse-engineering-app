// 
// Decompiled by Procyon v0.6.0
// 

package androidx.room;

import android.util.Log;
import java.util.TreeMap;
import androidx.sqlite.db.SupportSQLiteCompat;
import android.app.ActivityManager;
import androidx.sqlite.db.framework.FrameworkSQLiteOpenHelperFactory;
import androidx.arch.core.executor.ArchTaskExecutor;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.ArrayList;
import android.content.Intent;
import java.io.InputStream;
import java.io.File;
import android.content.Context;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.Callable;
import androidx.sqlite.db.SimpleSQLiteQuery;
import android.os.Looper;
import java.util.Iterator;
import java.util.BitSet;
import kotlin.collections.MapsKt;
import kotlin.collections.SetsKt;
import java.util.Set;
import kotlin.collections.CollectionsKt;
import androidx.room.migration.Migration;
import androidx.sqlite.db.SupportSQLiteStatement;
import java.util.concurrent.locks.Lock;
import kotlin.ReplaceWith;
import kotlin.jvm.functions.Function1;
import android.database.Cursor;
import android.os.CancellationSignal;
import androidx.sqlite.db.SupportSQLiteQuery;
import android.os.Build$VERSION;
import kotlin.Deprecated;
import kotlin.jvm.internal.Intrinsics;
import java.util.Collections;
import java.util.LinkedHashMap;
import kotlin.jvm.internal.DefaultConstructorMarker;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import androidx.sqlite.db.SupportSQLiteDatabase;
import java.util.List;
import java.util.concurrent.Executor;
import androidx.sqlite.db.SupportSQLiteOpenHelper;
import androidx.room.migration.AutoMigrationSpec;
import java.util.Map;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000\u00c4\u0001\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010%\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0007\n\u0002\u0010\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\"\n\u0002\b\r\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0011\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\r\b&\u0018\u0000 p2\u00020\u0001:\u0007nopqrstB\u0005¢\u0006\u0002\u0010\u0002J\b\u0010:\u001a\u00020;H\u0017J\b\u0010<\u001a\u00020;H\u0017J\b\u0010=\u001a\u00020;H\u0017J\b\u0010>\u001a\u00020;H'J\b\u0010?\u001a\u00020;H\u0016J\u0010\u0010@\u001a\u00020A2\u0006\u0010B\u001a\u00020\u0010H\u0016J\b\u0010C\u001a\u00020\u0018H$J\u0010\u0010D\u001a\u00020\u00132\u0006\u0010E\u001a\u00020FH$J\b\u0010G\u001a\u00020;H\u0017J*\u0010H\u001a\b\u0012\u0004\u0012\u00020I0#2\u001a\u0010\u0007\u001a\u0016\u0012\f\u0012\n\u0012\u0006\b\u0001\u0012\u00020\n0\t\u0012\u0004\u0012\u00020\n0JH\u0017J\r\u0010K\u001a\u00020LH\u0000¢\u0006\u0002\bMJ\u0016\u0010N\u001a\u0010\u0012\f\u0012\n\u0012\u0006\b\u0001\u0012\u00020\n0\t0OH\u0017J\"\u0010P\u001a\u001c\u0012\b\u0012\u0006\u0012\u0002\b\u00030\t\u0012\u000e\u0012\f\u0012\b\u0012\u0006\u0012\u0002\b\u00030\t0#0JH\u0015J#\u0010Q\u001a\u0004\u0018\u0001HR\"\u0004\b\u0000\u0010R2\f\u0010S\u001a\b\u0012\u0004\u0012\u0002HR0\tH\u0016¢\u0006\u0002\u0010TJ\b\u0010U\u001a\u00020\u0004H\u0016J\u0010\u0010V\u001a\u00020;2\u0006\u0010W\u001a\u00020FH\u0017J\b\u0010X\u001a\u00020;H\u0002J\b\u0010Y\u001a\u00020;H\u0002J\u0010\u0010Z\u001a\u00020;2\u0006\u0010[\u001a\u00020'H\u0014J\u001c\u0010\\\u001a\u00020]2\u0006\u0010\\\u001a\u00020^2\n\b\u0002\u0010_\u001a\u0004\u0018\u00010`H\u0017J)\u0010\\\u001a\u00020]2\u0006\u0010\\\u001a\u00020\u00102\u0012\u0010a\u001a\u000e\u0012\b\b\u0001\u0012\u0004\u0018\u00010\u0001\u0018\u00010bH\u0016¢\u0006\u0002\u0010cJ\u0010\u0010d\u001a\u00020;2\u0006\u0010e\u001a\u00020fH\u0016J!\u0010d\u001a\u0002Hg\"\u0004\b\u0000\u0010g2\f\u0010e\u001a\b\u0012\u0004\u0012\u0002Hg0hH\u0016¢\u0006\u0002\u0010iJ\b\u0010j\u001a\u00020;H\u0017J+\u0010k\u001a\u0004\u0018\u0001HR\"\u0004\b\u0000\u0010R2\f\u0010l\u001a\b\u0012\u0004\u0012\u0002HR0\t2\u0006\u0010)\u001a\u00020\u0013H\u0002¢\u0006\u0002\u0010mR\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u000e¢\u0006\u0002\n\u0000R\u0010\u0010\u0005\u001a\u0004\u0018\u00010\u0006X\u0082\u000e¢\u0006\u0002\n\u0000R2\u0010\u0007\u001a\u0016\u0012\f\u0012\n\u0012\u0006\b\u0001\u0012\u00020\n0\t\u0012\u0004\u0012\u00020\n0\b8\u0004@\u0004X\u0085\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u000b\u0010\f\"\u0004\b\r\u0010\u000eR\u001f\u0010\u000f\u001a\u000e\u0012\u0004\u0012\u00020\u0010\u0012\u0004\u0012\u00020\u00010\b8G¢\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\fR\u000e\u0010\u0012\u001a\u00020\u0013X\u0082.¢\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\u0015X\u0082.¢\u0006\u0002\n\u0000R\u000e\u0010\u0016\u001a\u00020\u0015X\u0082.¢\u0006\u0002\n\u0000R\u0014\u0010\u0017\u001a\u00020\u0018X\u0096\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0019\u0010\u001aR\u0014\u0010\u001b\u001a\u00020\u00048@X\u0080\u0004¢\u0006\u0006\u001a\u0004\b\u001c\u0010\u001dR\u001a\u0010\u001e\u001a\u00020\u00048VX\u0096\u0004¢\u0006\f\u0012\u0004\b\u001f\u0010\u0002\u001a\u0004\b\u001e\u0010\u001dR\u0017\u0010 \u001a\u00020\u00048G¢\u0006\f\u0012\u0004\b!\u0010\u0002\u001a\u0004\b \u0010\u001dR \u0010\"\u001a\n\u0012\u0004\u0012\u00020$\u0018\u00010#8\u0004@\u0004X\u0085\u000e¢\u0006\b\n\u0000\u0012\u0004\b%\u0010\u0002R\u001a\u0010&\u001a\u0004\u0018\u00010'8\u0004@\u0004X\u0085\u000e¢\u0006\b\n\u0000\u0012\u0004\b(\u0010\u0002R\u0014\u0010)\u001a\u00020\u00138VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b*\u0010+R\u0014\u0010,\u001a\u00020\u00158VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b-\u0010.R\u000e\u0010/\u001a\u000200X\u0082\u0004¢\u0006\u0002\n\u0000R\u0019\u00101\u001a\b\u0012\u0004\u0012\u000203028G¢\u0006\b\n\u0000\u001a\u0004\b4\u00105R\u0014\u00106\u001a\u00020\u00158VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b7\u0010.R\u001e\u00108\u001a\u0012\u0012\b\u0012\u0006\u0012\u0002\b\u00030\t\u0012\u0004\u0012\u00020\u00010\bX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u00109\u001a\u00020\u0004X\u0082\u000e¢\u0006\u0002\n\u0000¨\u0006u" }, d2 = { "Landroidx/room/RoomDatabase;", "", "()V", "allowMainThreadQueries", "", "autoCloser", "Landroidx/room/AutoCloser;", "autoMigrationSpecs", "", "Ljava/lang/Class;", "Landroidx/room/migration/AutoMigrationSpec;", "getAutoMigrationSpecs", "()Ljava/util/Map;", "setAutoMigrationSpecs", "(Ljava/util/Map;)V", "backingFieldMap", "", "getBackingFieldMap", "internalOpenHelper", "Landroidx/sqlite/db/SupportSQLiteOpenHelper;", "internalQueryExecutor", "Ljava/util/concurrent/Executor;", "internalTransactionExecutor", "invalidationTracker", "Landroidx/room/InvalidationTracker;", "getInvalidationTracker", "()Landroidx/room/InvalidationTracker;", "isMainThread", "isMainThread$room_runtime_release", "()Z", "isOpen", "isOpen$annotations", "isOpenInternal", "isOpenInternal$annotations", "mCallbacks", "", "Landroidx/room/RoomDatabase$Callback;", "getMCallbacks$annotations", "mDatabase", "Landroidx/sqlite/db/SupportSQLiteDatabase;", "getMDatabase$annotations", "openHelper", "getOpenHelper", "()Landroidx/sqlite/db/SupportSQLiteOpenHelper;", "queryExecutor", "getQueryExecutor", "()Ljava/util/concurrent/Executor;", "readWriteLock", "Ljava/util/concurrent/locks/ReentrantReadWriteLock;", "suspendingTransactionId", "Ljava/lang/ThreadLocal;", "", "getSuspendingTransactionId", "()Ljava/lang/ThreadLocal;", "transactionExecutor", "getTransactionExecutor", "typeConverters", "writeAheadLoggingEnabled", "assertNotMainThread", "", "assertNotSuspendingTransaction", "beginTransaction", "clearAllTables", "close", "compileStatement", "Landroidx/sqlite/db/SupportSQLiteStatement;", "sql", "createInvalidationTracker", "createOpenHelper", "config", "Landroidx/room/DatabaseConfiguration;", "endTransaction", "getAutoMigrations", "Landroidx/room/migration/Migration;", "", "getCloseLock", "Ljava/util/concurrent/locks/Lock;", "getCloseLock$room_runtime_release", "getRequiredAutoMigrationSpecs", "", "getRequiredTypeConverters", "getTypeConverter", "T", "klass", "(Ljava/lang/Class;)Ljava/lang/Object;", "inTransaction", "init", "configuration", "internalBeginTransaction", "internalEndTransaction", "internalInitInvalidationTracker", "db", "query", "Landroid/database/Cursor;", "Landroidx/sqlite/db/SupportSQLiteQuery;", "signal", "Landroid/os/CancellationSignal;", "args", "", "(Ljava/lang/String;[Ljava/lang/Object;)Landroid/database/Cursor;", "runInTransaction", "body", "Ljava/lang/Runnable;", "V", "Ljava/util/concurrent/Callable;", "(Ljava/util/concurrent/Callable;)Ljava/lang/Object;", "setTransactionSuccessful", "unwrapOpenHelper", "clazz", "(Ljava/lang/Class;Landroidx/sqlite/db/SupportSQLiteOpenHelper;)Ljava/lang/Object;", "Builder", "Callback", "Companion", "JournalMode", "MigrationContainer", "PrepackagedDatabaseCallback", "QueryCallback", "room-runtime_release" }, k = 1, mv = { 1, 7, 1 }, xi = 48)
public abstract class RoomDatabase
{
    public static final Companion Companion;
    public static final int MAX_BIND_PARAMETER_CNT = 999;
    private boolean allowMainThreadQueries;
    private AutoCloser autoCloser;
    private Map<Class<? extends AutoMigrationSpec>, AutoMigrationSpec> autoMigrationSpecs;
    private final Map<String, Object> backingFieldMap;
    private SupportSQLiteOpenHelper internalOpenHelper;
    private Executor internalQueryExecutor;
    private Executor internalTransactionExecutor;
    private final InvalidationTracker invalidationTracker;
    protected List<? extends Callback> mCallbacks;
    protected volatile SupportSQLiteDatabase mDatabase;
    private final ReentrantReadWriteLock readWriteLock;
    private final ThreadLocal<Integer> suspendingTransactionId;
    private final Map<Class<?>, Object> typeConverters;
    private boolean writeAheadLoggingEnabled;
    
    static {
        Companion = new Companion(null);
    }
    
    public RoomDatabase() {
        this.invalidationTracker = this.createInvalidationTracker();
        this.autoMigrationSpecs = new LinkedHashMap<Class<? extends AutoMigrationSpec>, AutoMigrationSpec>();
        this.readWriteLock = new ReentrantReadWriteLock();
        this.suspendingTransactionId = new ThreadLocal<Integer>();
        final Map<String, Object> synchronizedMap = Collections.synchronizedMap(new LinkedHashMap<String, Object>());
        Intrinsics.checkNotNullExpressionValue((Object)synchronizedMap, "synchronizedMap(mutableMapOf())");
        this.backingFieldMap = synchronizedMap;
        this.typeConverters = new LinkedHashMap<Class<?>, Object>();
    }
    
    private final void internalBeginTransaction() {
        this.assertNotMainThread();
        final SupportSQLiteDatabase writableDatabase = this.getOpenHelper().getWritableDatabase();
        this.getInvalidationTracker().syncTriggers$room_runtime_release(writableDatabase);
        if (Build$VERSION.SDK_INT >= 16 && writableDatabase.isWriteAheadLoggingEnabled()) {
            writableDatabase.beginTransactionNonExclusive();
        }
        else {
            writableDatabase.beginTransaction();
        }
    }
    
    private final void internalEndTransaction() {
        this.getOpenHelper().getWritableDatabase().endTransaction();
        if (!this.inTransaction()) {
            this.getInvalidationTracker().refreshVersionsAsync();
        }
    }
    
    public static /* synthetic */ Cursor query$default(final RoomDatabase roomDatabase, final SupportSQLiteQuery supportSQLiteQuery, CancellationSignal cancellationSignal, final int n, final Object o) {
        if (o == null) {
            if ((n & 0x2) != 0x0) {
                cancellationSignal = null;
            }
            return roomDatabase.query(supportSQLiteQuery, cancellationSignal);
        }
        throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: query");
    }
    
    private final <T> T unwrapOpenHelper(final Class<T> clazz, final SupportSQLiteOpenHelper supportSQLiteOpenHelper) {
        if (clazz.isInstance(supportSQLiteOpenHelper)) {
            return (T)supportSQLiteOpenHelper;
        }
        Object unwrapOpenHelper;
        if (supportSQLiteOpenHelper instanceof DelegatingOpenHelper) {
            unwrapOpenHelper = this.unwrapOpenHelper((Class<Object>)clazz, ((DelegatingOpenHelper)supportSQLiteOpenHelper).getDelegate());
        }
        else {
            unwrapOpenHelper = null;
        }
        return (T)unwrapOpenHelper;
    }
    
    public void assertNotMainThread() {
        if (this.allowMainThreadQueries) {
            return;
        }
        if (this.isMainThread$room_runtime_release() ^ true) {
            return;
        }
        throw new IllegalStateException("Cannot access database on the main thread since it may potentially lock the UI for a long period of time.".toString());
    }
    
    public void assertNotSuspendingTransaction() {
        if (this.inTransaction() || this.suspendingTransactionId.get() == null) {
            return;
        }
        throw new IllegalStateException("Cannot access database on a different coroutine context inherited from a suspending transaction.".toString());
    }
    
    @Deprecated(message = "beginTransaction() is deprecated", replaceWith = @ReplaceWith(expression = "runInTransaction(Runnable)", imports = {}))
    public void beginTransaction() {
        this.assertNotMainThread();
        final AutoCloser autoCloser = this.autoCloser;
        if (autoCloser == null) {
            this.internalBeginTransaction();
        }
        else {
            autoCloser.executeRefCountingFunction((kotlin.jvm.functions.Function1<? super SupportSQLiteDatabase, ?>)new RoomDatabase$beginTransaction.RoomDatabase$beginTransaction$1(this));
        }
    }
    
    public abstract void clearAllTables();
    
    public void close() {
        if (this.isOpen()) {
            final ReentrantReadWriteLock.WriteLock writeLock = this.readWriteLock.writeLock();
            Intrinsics.checkNotNullExpressionValue((Object)writeLock, "readWriteLock.writeLock()");
            final Lock lock = writeLock;
            lock.lock();
            try {
                this.getInvalidationTracker().stopMultiInstanceInvalidation$room_runtime_release();
                this.getOpenHelper().close();
            }
            finally {
                lock.unlock();
            }
        }
    }
    
    public SupportSQLiteStatement compileStatement(final String s) {
        Intrinsics.checkNotNullParameter((Object)s, "sql");
        this.assertNotMainThread();
        this.assertNotSuspendingTransaction();
        return this.getOpenHelper().getWritableDatabase().compileStatement(s);
    }
    
    protected abstract InvalidationTracker createInvalidationTracker();
    
    protected abstract SupportSQLiteOpenHelper createOpenHelper(final DatabaseConfiguration p0);
    
    @Deprecated(message = "endTransaction() is deprecated", replaceWith = @ReplaceWith(expression = "runInTransaction(Runnable)", imports = {}))
    public void endTransaction() {
        final AutoCloser autoCloser = this.autoCloser;
        if (autoCloser == null) {
            this.internalEndTransaction();
        }
        else {
            autoCloser.executeRefCountingFunction((kotlin.jvm.functions.Function1<? super SupportSQLiteDatabase, ?>)new RoomDatabase$endTransaction.RoomDatabase$endTransaction$1(this));
        }
    }
    
    protected final Map<Class<? extends AutoMigrationSpec>, AutoMigrationSpec> getAutoMigrationSpecs() {
        return this.autoMigrationSpecs;
    }
    
    public List<Migration> getAutoMigrations(final Map<Class<? extends AutoMigrationSpec>, AutoMigrationSpec> map) {
        Intrinsics.checkNotNullParameter((Object)map, "autoMigrationSpecs");
        return CollectionsKt.emptyList();
    }
    
    public final Map<String, Object> getBackingFieldMap() {
        return this.backingFieldMap;
    }
    
    public final Lock getCloseLock$room_runtime_release() {
        final ReentrantReadWriteLock.ReadLock lock = this.readWriteLock.readLock();
        Intrinsics.checkNotNullExpressionValue((Object)lock, "readWriteLock.readLock()");
        return lock;
    }
    
    public InvalidationTracker getInvalidationTracker() {
        return this.invalidationTracker;
    }
    
    public SupportSQLiteOpenHelper getOpenHelper() {
        SupportSQLiteOpenHelper internalOpenHelper;
        if ((internalOpenHelper = this.internalOpenHelper) == null) {
            Intrinsics.throwUninitializedPropertyAccessException("internalOpenHelper");
            internalOpenHelper = null;
        }
        return internalOpenHelper;
    }
    
    public Executor getQueryExecutor() {
        Executor internalQueryExecutor;
        if ((internalQueryExecutor = this.internalQueryExecutor) == null) {
            Intrinsics.throwUninitializedPropertyAccessException("internalQueryExecutor");
            internalQueryExecutor = null;
        }
        return internalQueryExecutor;
    }
    
    public Set<Class<? extends AutoMigrationSpec>> getRequiredAutoMigrationSpecs() {
        return SetsKt.emptySet();
    }
    
    protected Map<Class<?>, List<Class<?>>> getRequiredTypeConverters() {
        return MapsKt.emptyMap();
    }
    
    public final ThreadLocal<Integer> getSuspendingTransactionId() {
        return this.suspendingTransactionId;
    }
    
    public Executor getTransactionExecutor() {
        Executor internalTransactionExecutor;
        if ((internalTransactionExecutor = this.internalTransactionExecutor) == null) {
            Intrinsics.throwUninitializedPropertyAccessException("internalTransactionExecutor");
            internalTransactionExecutor = null;
        }
        return internalTransactionExecutor;
    }
    
    public <T> T getTypeConverter(final Class<T> clazz) {
        Intrinsics.checkNotNullParameter((Object)clazz, "klass");
        return (T)this.typeConverters.get(clazz);
    }
    
    public boolean inTransaction() {
        return this.getOpenHelper().getWritableDatabase().inTransaction();
    }
    
    public void init(final DatabaseConfiguration databaseConfiguration) {
        Intrinsics.checkNotNullParameter((Object)databaseConfiguration, "configuration");
        this.internalOpenHelper = this.createOpenHelper(databaseConfiguration);
        final Set<Class<? extends AutoMigrationSpec>> requiredAutoMigrationSpecs = this.getRequiredAutoMigrationSpecs();
        final BitSet set = new BitSet();
        final Iterator<Class<? extends AutoMigrationSpec>> iterator = requiredAutoMigrationSpecs.iterator();
        while (true) {
            final boolean hasNext = iterator.hasNext();
            boolean b = true;
            final int n = -1;
            if (!hasNext) {
                int bitIndex = databaseConfiguration.autoMigrationSpecs.size() - 1;
                if (bitIndex >= 0) {
                    while (true) {
                        final int n2 = bitIndex - 1;
                        if (!set.get(bitIndex)) {
                            throw new IllegalArgumentException("Unexpected auto migration specs found. Annotate AutoMigrationSpec implementation with @ProvidedAutoMigrationSpec annotation or remove this spec from the builder.".toString());
                        }
                        if (n2 < 0) {
                            break;
                        }
                        bitIndex = n2;
                    }
                }
                for (final Migration migration : this.getAutoMigrations(this.autoMigrationSpecs)) {
                    if (!databaseConfiguration.migrationContainer.contains(migration.startVersion, migration.endVersion)) {
                        databaseConfiguration.migrationContainer.addMigrations(migration);
                    }
                }
                final SQLiteCopyOpenHelper sqLiteCopyOpenHelper = this.unwrapOpenHelper(SQLiteCopyOpenHelper.class, this.getOpenHelper());
                if (sqLiteCopyOpenHelper != null) {
                    sqLiteCopyOpenHelper.setDatabaseConfiguration(databaseConfiguration);
                }
                final AutoClosingRoomOpenHelper autoClosingRoomOpenHelper = this.unwrapOpenHelper(AutoClosingRoomOpenHelper.class, this.getOpenHelper());
                if (autoClosingRoomOpenHelper != null) {
                    this.autoCloser = autoClosingRoomOpenHelper.autoCloser;
                    this.getInvalidationTracker().setAutoCloser$room_runtime_release(autoClosingRoomOpenHelper.autoCloser);
                }
                boolean b2;
                if (Build$VERSION.SDK_INT >= 16) {
                    b2 = (databaseConfiguration.journalMode == JournalMode.WRITE_AHEAD_LOGGING);
                    this.getOpenHelper().setWriteAheadLoggingEnabled(b2);
                }
                else {
                    b2 = false;
                }
                this.mCallbacks = (List<? extends Callback>)databaseConfiguration.callbacks;
                this.internalQueryExecutor = databaseConfiguration.queryExecutor;
                this.internalTransactionExecutor = new TransactionExecutor(databaseConfiguration.transactionExecutor);
                this.allowMainThreadQueries = databaseConfiguration.allowMainThreadQueries;
                this.writeAheadLoggingEnabled = b2;
                if (databaseConfiguration.multiInstanceInvalidationServiceIntent != null) {
                    if (databaseConfiguration.name == null) {
                        throw new IllegalArgumentException("Required value was null.".toString());
                    }
                    this.getInvalidationTracker().startMultiInstanceInvalidation$room_runtime_release(databaseConfiguration.context, databaseConfiguration.name, databaseConfiguration.multiInstanceInvalidationServiceIntent);
                }
                final Map<Class<?>, List<Class<?>>> requiredTypeConverters = this.getRequiredTypeConverters();
                final BitSet set2 = new BitSet();
                for (final Map.Entry<Class, V> entry : requiredTypeConverters.entrySet()) {
                    final Class clazz = entry.getKey();
                    for (final Class obj : (List)entry.getValue()) {
                        int bitIndex2 = databaseConfiguration.typeConverters.size() - 1;
                        Label_0742: {
                            if (bitIndex2 >= 0) {
                                while (true) {
                                    final int n3 = bitIndex2 - 1;
                                    if (obj.isAssignableFrom(databaseConfiguration.typeConverters.get(bitIndex2).getClass())) {
                                        set2.set(bitIndex2);
                                        break Label_0742;
                                    }
                                    if (n3 < 0) {
                                        break;
                                    }
                                    bitIndex2 = n3;
                                }
                            }
                            bitIndex2 = -1;
                        }
                        if (bitIndex2 < 0) {
                            final StringBuilder sb = new StringBuilder();
                            sb.append("A required type converter (");
                            sb.append(obj);
                            sb.append(") for ");
                            sb.append(clazz.getCanonicalName());
                            sb.append(" is missing in the database configuration.");
                            throw new IllegalArgumentException(sb.toString().toString());
                        }
                        this.typeConverters.put(obj, databaseConfiguration.typeConverters.get(bitIndex2));
                    }
                }
                int bitIndex3 = databaseConfiguration.typeConverters.size() - 1;
                if (bitIndex3 >= 0) {
                    while (true) {
                        final int n4 = bitIndex3 - 1;
                        if (!set2.get(bitIndex3)) {
                            final Object value = databaseConfiguration.typeConverters.get(bitIndex3);
                            final StringBuilder sb2 = new StringBuilder();
                            sb2.append("Unexpected type converter ");
                            sb2.append(value);
                            sb2.append(". Annotate TypeConverter class with @ProvidedTypeConverter annotation or remove this converter from the builder.");
                            throw new IllegalArgumentException(sb2.toString());
                        }
                        if (n4 < 0) {
                            break;
                        }
                        bitIndex3 = n4;
                    }
                }
                return;
            }
            final Class clazz2 = iterator.next();
            final int n5 = databaseConfiguration.autoMigrationSpecs.size() - 1;
            int bitIndex4 = n;
            if (n5 >= 0) {
                bitIndex4 = n5;
                while (true) {
                    final int n6 = bitIndex4 - 1;
                    if (clazz2.isAssignableFrom(databaseConfiguration.autoMigrationSpecs.get(bitIndex4).getClass())) {
                        set.set(bitIndex4);
                        break;
                    }
                    if (n6 < 0) {
                        bitIndex4 = n;
                        break;
                    }
                    bitIndex4 = n6;
                }
            }
            if (bitIndex4 < 0) {
                b = false;
            }
            if (!b) {
                final StringBuilder sb3 = new StringBuilder();
                sb3.append("A required auto migration spec (");
                sb3.append(clazz2.getCanonicalName());
                sb3.append(") is missing in the database configuration.");
                throw new IllegalArgumentException(sb3.toString().toString());
            }
            this.autoMigrationSpecs.put(clazz2, databaseConfiguration.autoMigrationSpecs.get(bitIndex4));
        }
    }
    
    protected void internalInitInvalidationTracker(final SupportSQLiteDatabase supportSQLiteDatabase) {
        Intrinsics.checkNotNullParameter((Object)supportSQLiteDatabase, "db");
        this.getInvalidationTracker().internalInit$room_runtime_release(supportSQLiteDatabase);
    }
    
    public final boolean isMainThread$room_runtime_release() {
        return Looper.getMainLooper().getThread() == Thread.currentThread();
    }
    
    public boolean isOpen() {
        final AutoCloser autoCloser = this.autoCloser;
        boolean b;
        if (autoCloser != null) {
            b = autoCloser.isActive();
        }
        else {
            final SupportSQLiteDatabase mDatabase = this.mDatabase;
            if (mDatabase == null) {
                final Boolean value = null;
                return Intrinsics.areEqual((Object)value, (Object)true);
            }
            b = mDatabase.isOpen();
        }
        final Boolean value = b;
        return Intrinsics.areEqual((Object)value, (Object)true);
    }
    
    public final boolean isOpenInternal() {
        final SupportSQLiteDatabase mDatabase = this.mDatabase;
        boolean b = true;
        if (mDatabase == null || !mDatabase.isOpen()) {
            b = false;
        }
        return b;
    }
    
    public final Cursor query(final SupportSQLiteQuery supportSQLiteQuery) {
        Intrinsics.checkNotNullParameter((Object)supportSQLiteQuery, "query");
        return query$default(this, supportSQLiteQuery, null, 2, null);
    }
    
    public Cursor query(final SupportSQLiteQuery supportSQLiteQuery, final CancellationSignal cancellationSignal) {
        Intrinsics.checkNotNullParameter((Object)supportSQLiteQuery, "query");
        this.assertNotMainThread();
        this.assertNotSuspendingTransaction();
        Cursor cursor;
        if (cancellationSignal != null && Build$VERSION.SDK_INT >= 16) {
            cursor = this.getOpenHelper().getWritableDatabase().query(supportSQLiteQuery, cancellationSignal);
        }
        else {
            cursor = this.getOpenHelper().getWritableDatabase().query(supportSQLiteQuery);
        }
        return cursor;
    }
    
    public Cursor query(final String s, final Object[] array) {
        Intrinsics.checkNotNullParameter((Object)s, "query");
        return this.getOpenHelper().getWritableDatabase().query(new SimpleSQLiteQuery(s, array));
    }
    
    public <V> V runInTransaction(final Callable<V> callable) {
        Intrinsics.checkNotNullParameter((Object)callable, "body");
        this.beginTransaction();
        try {
            final V call = callable.call();
            this.setTransactionSuccessful();
            return call;
        }
        finally {
            this.endTransaction();
        }
    }
    
    public void runInTransaction(final Runnable runnable) {
        Intrinsics.checkNotNullParameter((Object)runnable, "body");
        this.beginTransaction();
        try {
            runnable.run();
            this.setTransactionSuccessful();
        }
        finally {
            this.endTransaction();
        }
    }
    
    protected final void setAutoMigrationSpecs(final Map<Class<? extends AutoMigrationSpec>, AutoMigrationSpec> autoMigrationSpecs) {
        Intrinsics.checkNotNullParameter((Object)autoMigrationSpecs, "<set-?>");
        this.autoMigrationSpecs = autoMigrationSpecs;
    }
    
    @Deprecated(message = "setTransactionSuccessful() is deprecated", replaceWith = @ReplaceWith(expression = "runInTransaction(Runnable)", imports = {}))
    public void setTransactionSuccessful() {
        this.getOpenHelper().getWritableDatabase().setTransactionSuccessful();
    }
    
    @Metadata(d1 = { "\u0000¦\u0001\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010#\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0010\u0011\n\u0002\u0018\u0002\n\u0002\b\u000f\n\u0002\u0010\u0015\n\u0002\b\u000b\b\u0016\u0018\u0000*\b\b\u0000\u0010\u0001*\u00020\u00022\u00020\u0003B'\b\u0000\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\f\u0010\u0006\u001a\b\u0012\u0004\u0012\u00028\u00000\u0007\u0012\b\u0010\b\u001a\u0004\u0018\u00010\t¢\u0006\u0002\u0010\nJ\u0016\u00103\u001a\b\u0012\u0004\u0012\u00028\u00000\u00002\u0006\u00104\u001a\u00020\u0014H\u0016J\u0016\u00105\u001a\b\u0012\u0004\u0012\u00028\u00000\u00002\u0006\u00106\u001a\u00020\u0016H\u0016J'\u00107\u001a\b\u0012\u0004\u0012\u00028\u00000\u00002\u0012\u00108\u001a\n\u0012\u0006\b\u0001\u0012\u00020:09\"\u00020:H\u0016¢\u0006\u0002\u0010;J\u0016\u0010<\u001a\b\u0012\u0004\u0012\u00028\u00000\u00002\u0006\u0010=\u001a\u00020\u0003H\u0016J\u000e\u0010\r\u001a\b\u0012\u0004\u0012\u00028\u00000\u0000H\u0016J\r\u0010>\u001a\u00028\u0000H\u0016¢\u0006\u0002\u0010?J\u0016\u0010@\u001a\b\u0012\u0004\u0012\u00028\u00000\u00002\u0006\u0010A\u001a\u00020\tH\u0016J\u001e\u0010@\u001a\b\u0012\u0004\u0012\u00028\u00000\u00002\u0006\u0010A\u001a\u00020\t2\u0006\u00106\u001a\u00020*H\u0017J\u0016\u0010B\u001a\b\u0012\u0004\u0012\u00028\u00000\u00002\u0006\u0010C\u001a\u00020\u0019H\u0016J\u001e\u0010B\u001a\b\u0012\u0004\u0012\u00028\u00000\u00002\u0006\u0010C\u001a\u00020\u00192\u0006\u00106\u001a\u00020*H\u0017J\u001c\u0010D\u001a\b\u0012\u0004\u0012\u00028\u00000\u00002\f\u0010E\u001a\b\u0012\u0004\u0012\u00020\u001c0\u001bH\u0017J$\u0010D\u001a\b\u0012\u0004\u0012\u00028\u00000\u00002\f\u0010E\u001a\b\u0012\u0004\u0012\u00020\u001c0\u001b2\u0006\u00106\u001a\u00020*H\u0017J\u000e\u0010F\u001a\b\u0012\u0004\u0012\u00028\u00000\u0000H\u0016J\u000e\u0010G\u001a\b\u0012\u0004\u0012\u00028\u00000\u0000H\u0016J\u001a\u0010H\u001a\b\u0012\u0004\u0012\u00028\u00000\u00002\n\u0010I\u001a\u00020J\"\u00020%H\u0016J\u000e\u0010K\u001a\b\u0012\u0004\u0012\u00028\u00000\u0000H\u0016J\u0018\u0010L\u001a\b\u0012\u0004\u0012\u00028\u00000\u00002\b\u0010\u001d\u001a\u0004\u0018\u00010\u001eH\u0016J \u0010M\u001a\b\u0012\u0004\u0012\u00028\u00000\u00002\b\b\u0001\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u000e\u001a\u00020\u000fH\u0017J\u0016\u0010N\u001a\b\u0012\u0004\u0012\u00028\u00000\u00002\u0006\u0010\u001f\u001a\u00020 H\u0016J\u0016\u0010O\u001a\b\u0012\u0004\u0012\u00028\u00000\u00002\u0006\u0010P\u001a\u00020(H\u0017J\u001e\u0010Q\u001a\b\u0012\u0004\u0012\u00028\u00000\u00002\u0006\u0010+\u001a\u00020,2\u0006\u0010R\u001a\u00020.H\u0016J\u0016\u0010S\u001a\b\u0012\u0004\u0012\u00028\u00000\u00002\u0006\u0010R\u001a\u00020.H\u0016J\u0016\u0010T\u001a\b\u0012\u0004\u0012\u00028\u00000\u00002\u0006\u0010R\u001a\u00020.H\u0016R\u000e\u0010\u000b\u001a\u00020\fX\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\fX\u0082\u000e¢\u0006\u0002\n\u0000R\u0010\u0010\u000e\u001a\u0004\u0018\u00010\u000fX\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0011X\u0082\u000e¢\u0006\u0002\n\u0000R\u0014\u0010\u0012\u001a\b\u0012\u0004\u0012\u00020\u00140\u0013X\u0082\u000e¢\u0006\u0002\n\u0000R\u0014\u0010\u0015\u001a\b\u0012\u0004\u0012\u00020\u00160\u0013X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\u0017\u001a\u0004\u0018\u00010\tX\u0082\u000e¢\u0006\u0002\n\u0000R\u0010\u0010\u0018\u001a\u0004\u0018\u00010\u0019X\u0082\u000e¢\u0006\u0002\n\u0000R\u0016\u0010\u001a\u001a\n\u0012\u0004\u0012\u00020\u001c\u0018\u00010\u001bX\u0082\u000e¢\u0006\u0002\n\u0000R\u0010\u0010\u001d\u001a\u0004\u0018\u00010\u001eX\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u001f\u001a\u00020 X\u0082\u000e¢\u0006\u0002\n\u0000R\u0014\u0010\u0006\u001a\b\u0012\u0004\u0012\u00028\u00000\u0007X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010!\u001a\u00020\"X\u0082\u0004¢\u0006\u0002\n\u0000R\u0016\u0010#\u001a\n\u0012\u0004\u0012\u00020%\u0018\u00010$X\u0082\u000e¢\u0006\u0002\n\u0000R\u0014\u0010&\u001a\b\u0012\u0004\u0012\u00020%0$X\u0082\u000e¢\u0006\u0002\n\u0000R\u0010\u0010'\u001a\u0004\u0018\u00010(X\u0082\u000e¢\u0006\u0002\n\u0000R\u0010\u0010\b\u001a\u0004\u0018\u00010\tX\u0082\u0004¢\u0006\u0002\n\u0000R\u0010\u0010)\u001a\u0004\u0018\u00010*X\u0082\u000e¢\u0006\u0002\n\u0000R\u0010\u0010+\u001a\u0004\u0018\u00010,X\u0082\u000e¢\u0006\u0002\n\u0000R\u0010\u0010-\u001a\u0004\u0018\u00010.X\u0082\u000e¢\u0006\u0002\n\u0000R\u0010\u0010/\u001a\u0004\u0018\u00010.X\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u00100\u001a\u00020\fX\u0082\u000e¢\u0006\u0002\n\u0000R\u0010\u00101\u001a\u0004\u0018\u00010.X\u0082\u000e¢\u0006\u0002\n\u0000R\u0014\u00102\u001a\b\u0012\u0004\u0012\u00020\u00030\u0013X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006U" }, d2 = { "Landroidx/room/RoomDatabase$Builder;", "T", "Landroidx/room/RoomDatabase;", "", "context", "Landroid/content/Context;", "klass", "Ljava/lang/Class;", "name", "", "(Landroid/content/Context;Ljava/lang/Class;Ljava/lang/String;)V", "allowDestructiveMigrationOnDowngrade", "", "allowMainThreadQueries", "autoCloseTimeUnit", "Ljava/util/concurrent/TimeUnit;", "autoCloseTimeout", "", "autoMigrationSpecs", "", "Landroidx/room/migration/AutoMigrationSpec;", "callbacks", "Landroidx/room/RoomDatabase$Callback;", "copyFromAssetPath", "copyFromFile", "Ljava/io/File;", "copyFromInputStream", "Ljava/util/concurrent/Callable;", "Ljava/io/InputStream;", "factory", "Landroidx/sqlite/db/SupportSQLiteOpenHelper$Factory;", "journalMode", "Landroidx/room/RoomDatabase$JournalMode;", "migrationContainer", "Landroidx/room/RoomDatabase$MigrationContainer;", "migrationStartAndEndVersions", "", "", "migrationsNotRequiredFrom", "multiInstanceInvalidationIntent", "Landroid/content/Intent;", "prepackagedDatabaseCallback", "Landroidx/room/RoomDatabase$PrepackagedDatabaseCallback;", "queryCallback", "Landroidx/room/RoomDatabase$QueryCallback;", "queryCallbackExecutor", "Ljava/util/concurrent/Executor;", "queryExecutor", "requireMigration", "transactionExecutor", "typeConverters", "addAutoMigrationSpec", "autoMigrationSpec", "addCallback", "callback", "addMigrations", "migrations", "", "Landroidx/room/migration/Migration;", "([Landroidx/room/migration/Migration;)Landroidx/room/RoomDatabase$Builder;", "addTypeConverter", "typeConverter", "build", "()Landroidx/room/RoomDatabase;", "createFromAsset", "databaseFilePath", "createFromFile", "databaseFile", "createFromInputStream", "inputStreamCallable", "enableMultiInstanceInvalidation", "fallbackToDestructiveMigration", "fallbackToDestructiveMigrationFrom", "startVersions", "", "fallbackToDestructiveMigrationOnDowngrade", "openHelperFactory", "setAutoCloseTimeout", "setJournalMode", "setMultiInstanceInvalidationServiceIntent", "invalidationServiceIntent", "setQueryCallback", "executor", "setQueryExecutor", "setTransactionExecutor", "room-runtime_release" }, k = 1, mv = { 1, 7, 1 }, xi = 48)
    public static class Builder<T extends RoomDatabase>
    {
        private boolean allowDestructiveMigrationOnDowngrade;
        private boolean allowMainThreadQueries;
        private TimeUnit autoCloseTimeUnit;
        private long autoCloseTimeout;
        private List<AutoMigrationSpec> autoMigrationSpecs;
        private final List<Callback> callbacks;
        private final Context context;
        private String copyFromAssetPath;
        private File copyFromFile;
        private Callable<InputStream> copyFromInputStream;
        private SupportSQLiteOpenHelper.Factory factory;
        private JournalMode journalMode;
        private final Class<T> klass;
        private final MigrationContainer migrationContainer;
        private Set<Integer> migrationStartAndEndVersions;
        private Set<Integer> migrationsNotRequiredFrom;
        private Intent multiInstanceInvalidationIntent;
        private final String name;
        private PrepackagedDatabaseCallback prepackagedDatabaseCallback;
        private QueryCallback queryCallback;
        private Executor queryCallbackExecutor;
        private Executor queryExecutor;
        private boolean requireMigration;
        private Executor transactionExecutor;
        private final List<Object> typeConverters;
        
        public Builder(final Context context, final Class<T> klass, final String name) {
            Intrinsics.checkNotNullParameter((Object)context, "context");
            Intrinsics.checkNotNullParameter((Object)klass, "klass");
            this.context = context;
            this.klass = klass;
            this.name = name;
            this.callbacks = new ArrayList<Callback>();
            this.typeConverters = new ArrayList<Object>();
            this.autoMigrationSpecs = new ArrayList<AutoMigrationSpec>();
            this.journalMode = JournalMode.AUTOMATIC;
            this.requireMigration = true;
            this.autoCloseTimeout = -1L;
            this.migrationContainer = new MigrationContainer();
            this.migrationsNotRequiredFrom = new LinkedHashSet<Integer>();
        }
        
        public Builder<T> addAutoMigrationSpec(final AutoMigrationSpec autoMigrationSpec) {
            Intrinsics.checkNotNullParameter((Object)autoMigrationSpec, "autoMigrationSpec");
            final Builder builder = this;
            this.autoMigrationSpecs.add(autoMigrationSpec);
            return this;
        }
        
        public Builder<T> addCallback(final Callback callback) {
            Intrinsics.checkNotNullParameter((Object)callback, "callback");
            final Builder builder = this;
            this.callbacks.add(callback);
            return this;
        }
        
        public Builder<T> addMigrations(final Migration... original) {
            Intrinsics.checkNotNullParameter((Object)original, "migrations");
            final Builder builder = this;
            if (this.migrationStartAndEndVersions == null) {
                this.migrationStartAndEndVersions = new HashSet<Integer>();
            }
            for (int i = 0; i < original.length; ++i) {
                final Migration migration = original[i];
                final Set<Integer> migrationStartAndEndVersions = this.migrationStartAndEndVersions;
                Intrinsics.checkNotNull((Object)migrationStartAndEndVersions);
                migrationStartAndEndVersions.add(migration.startVersion);
                final Set<Integer> migrationStartAndEndVersions2 = this.migrationStartAndEndVersions;
                Intrinsics.checkNotNull((Object)migrationStartAndEndVersions2);
                migrationStartAndEndVersions2.add(migration.endVersion);
            }
            this.migrationContainer.addMigrations((Migration[])Arrays.copyOf(original, original.length));
            return this;
        }
        
        public Builder<T> addTypeConverter(final Object o) {
            Intrinsics.checkNotNullParameter(o, "typeConverter");
            final Builder builder = this;
            this.typeConverters.add(o);
            return this;
        }
        
        public Builder<T> allowMainThreadQueries() {
            final Builder builder = this;
            this.allowMainThreadQueries = true;
            return this;
        }
        
        public T build() {
            final Executor queryExecutor = this.queryExecutor;
            if (queryExecutor == null && this.transactionExecutor == null) {
                final Executor ioThreadExecutor = ArchTaskExecutor.getIOThreadExecutor();
                this.transactionExecutor = ioThreadExecutor;
                this.queryExecutor = ioThreadExecutor;
            }
            else if (queryExecutor != null && this.transactionExecutor == null) {
                this.transactionExecutor = queryExecutor;
            }
            else if (queryExecutor == null) {
                this.queryExecutor = this.transactionExecutor;
            }
            final Set<Integer> migrationStartAndEndVersions = this.migrationStartAndEndVersions;
            final int n = 1;
            if (migrationStartAndEndVersions != null) {
                Intrinsics.checkNotNull((Object)migrationStartAndEndVersions);
                final Iterator<Integer> iterator = migrationStartAndEndVersions.iterator();
                while (iterator.hasNext()) {
                    final int intValue = iterator.next().intValue();
                    if (this.migrationsNotRequiredFrom.contains(intValue) ^ true) {
                        continue;
                    }
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Inconsistency detected. A Migration was supplied to addMigration(Migration... migrations) that has a start or end version equal to a start version supplied to fallbackToDestructiveMigrationFrom(int... startVersions). Start version: ");
                    sb.append(intValue);
                    throw new IllegalArgumentException(sb.toString().toString());
                }
            }
            SupportSQLiteOpenHelper.Factory factory;
            if ((factory = this.factory) == null) {
                factory = new FrameworkSQLiteOpenHelperFactory();
            }
            SupportSQLiteOpenHelper.Factory factory3 = null;
            Label_0497: {
                if (factory != null) {
                    SupportSQLiteOpenHelper.Factory factory2 = factory;
                    if (this.autoCloseTimeout > 0L) {
                        if (this.name == null) {
                            throw new IllegalArgumentException("Cannot create auto-closing database for an in-memory database.".toString());
                        }
                        final long autoCloseTimeout = this.autoCloseTimeout;
                        final TimeUnit autoCloseTimeUnit = this.autoCloseTimeUnit;
                        if (autoCloseTimeUnit == null) {
                            throw new IllegalArgumentException("Required value was null.".toString());
                        }
                        final Executor queryExecutor2 = this.queryExecutor;
                        if (queryExecutor2 == null) {
                            throw new IllegalArgumentException("Required value was null.".toString());
                        }
                        factory2 = new AutoClosingRoomOpenHelperFactory(factory, new AutoCloser(autoCloseTimeout, autoCloseTimeUnit, queryExecutor2));
                    }
                    final String copyFromAssetPath = this.copyFromAssetPath;
                    if (copyFromAssetPath == null && this.copyFromFile == null) {
                        factory3 = factory2;
                        if (this.copyFromInputStream == null) {
                            break Label_0497;
                        }
                    }
                    if (this.name == null) {
                        throw new IllegalArgumentException("Cannot create from asset or file for an in-memory database.".toString());
                    }
                    int n2;
                    if (copyFromAssetPath == null) {
                        n2 = 0;
                    }
                    else {
                        n2 = 1;
                    }
                    final File copyFromFile = this.copyFromFile;
                    int n3;
                    if (copyFromFile == null) {
                        n3 = 0;
                    }
                    else {
                        n3 = 1;
                    }
                    final Callable<InputStream> copyFromInputStream = this.copyFromInputStream;
                    int n4;
                    if (copyFromInputStream == null) {
                        n4 = 0;
                    }
                    else {
                        n4 = 1;
                    }
                    int n5;
                    if (n2 + n3 + n4 == 1) {
                        n5 = n;
                    }
                    else {
                        n5 = 0;
                    }
                    if (n5 == 0) {
                        throw new IllegalArgumentException("More than one of createFromAsset(), createFromInputStream(), and createFromFile() were called on this Builder, but the database can only be created using one of the three configurations.".toString());
                    }
                    factory3 = new SQLiteCopyOpenHelperFactory(copyFromAssetPath, copyFromFile, copyFromInputStream, factory2);
                }
                else {
                    factory3 = null;
                }
            }
            if (factory3 == null) {
                throw new IllegalArgumentException("Required value was null.".toString());
            }
            final QueryCallback queryCallback = this.queryCallback;
            SupportSQLiteOpenHelper.Factory factory4 = factory3;
            if (queryCallback != null) {
                final Executor queryCallbackExecutor = this.queryCallbackExecutor;
                if (queryCallbackExecutor == null) {
                    throw new IllegalArgumentException("Required value was null.".toString());
                }
                if (queryCallback == null) {
                    throw new IllegalArgumentException("Required value was null.".toString());
                }
                factory4 = new QueryInterceptorOpenHelperFactory(factory3, queryCallbackExecutor, queryCallback);
            }
            final Context context = this.context;
            final String name = this.name;
            final MigrationContainer migrationContainer = this.migrationContainer;
            final List<Callback> callbacks = this.callbacks;
            final boolean allowMainThreadQueries = this.allowMainThreadQueries;
            final JournalMode resolve$room_runtime_release = this.journalMode.resolve$room_runtime_release(context);
            final Executor queryExecutor3 = this.queryExecutor;
            if (queryExecutor3 == null) {
                throw new IllegalArgumentException("Required value was null.".toString());
            }
            final Executor transactionExecutor = this.transactionExecutor;
            if (transactionExecutor != null) {
                final DatabaseConfiguration databaseConfiguration = new DatabaseConfiguration(context, name, factory4, migrationContainer, callbacks, allowMainThreadQueries, resolve$room_runtime_release, queryExecutor3, transactionExecutor, this.multiInstanceInvalidationIntent, this.requireMigration, this.allowDestructiveMigrationOnDowngrade, this.migrationsNotRequiredFrom, this.copyFromAssetPath, this.copyFromFile, this.copyFromInputStream, this.prepackagedDatabaseCallback, this.typeConverters, this.autoMigrationSpecs);
                final RoomDatabase roomDatabase = Room.getGeneratedImplementation(this.klass, "_Impl");
                roomDatabase.init(databaseConfiguration);
                return (T)roomDatabase;
            }
            throw new IllegalArgumentException("Required value was null.".toString());
        }
        
        public Builder<T> createFromAsset(final String copyFromAssetPath) {
            Intrinsics.checkNotNullParameter((Object)copyFromAssetPath, "databaseFilePath");
            final Builder builder = this;
            this.copyFromAssetPath = copyFromAssetPath;
            return this;
        }
        
        public Builder<T> createFromAsset(final String copyFromAssetPath, final PrepackagedDatabaseCallback prepackagedDatabaseCallback) {
            Intrinsics.checkNotNullParameter((Object)copyFromAssetPath, "databaseFilePath");
            Intrinsics.checkNotNullParameter((Object)prepackagedDatabaseCallback, "callback");
            final Builder builder = this;
            this.prepackagedDatabaseCallback = prepackagedDatabaseCallback;
            this.copyFromAssetPath = copyFromAssetPath;
            return this;
        }
        
        public Builder<T> createFromFile(final File copyFromFile) {
            Intrinsics.checkNotNullParameter((Object)copyFromFile, "databaseFile");
            final Builder builder = this;
            this.copyFromFile = copyFromFile;
            return this;
        }
        
        public Builder<T> createFromFile(final File copyFromFile, final PrepackagedDatabaseCallback prepackagedDatabaseCallback) {
            Intrinsics.checkNotNullParameter((Object)copyFromFile, "databaseFile");
            Intrinsics.checkNotNullParameter((Object)prepackagedDatabaseCallback, "callback");
            final Builder builder = this;
            this.prepackagedDatabaseCallback = prepackagedDatabaseCallback;
            this.copyFromFile = copyFromFile;
            return this;
        }
        
        public Builder<T> createFromInputStream(final Callable<InputStream> copyFromInputStream) {
            Intrinsics.checkNotNullParameter((Object)copyFromInputStream, "inputStreamCallable");
            final Builder builder = this;
            this.copyFromInputStream = copyFromInputStream;
            return this;
        }
        
        public Builder<T> createFromInputStream(final Callable<InputStream> copyFromInputStream, final PrepackagedDatabaseCallback prepackagedDatabaseCallback) {
            Intrinsics.checkNotNullParameter((Object)copyFromInputStream, "inputStreamCallable");
            Intrinsics.checkNotNullParameter((Object)prepackagedDatabaseCallback, "callback");
            final Builder builder = this;
            this.prepackagedDatabaseCallback = prepackagedDatabaseCallback;
            this.copyFromInputStream = copyFromInputStream;
            return this;
        }
        
        public Builder<T> enableMultiInstanceInvalidation() {
            final Builder builder = this;
            Intent multiInstanceInvalidationIntent;
            if (this.name != null) {
                multiInstanceInvalidationIntent = new Intent(this.context, (Class)MultiInstanceInvalidationService.class);
            }
            else {
                multiInstanceInvalidationIntent = null;
                final Intent intent = null;
            }
            this.multiInstanceInvalidationIntent = multiInstanceInvalidationIntent;
            return this;
        }
        
        public Builder<T> fallbackToDestructiveMigration() {
            final Builder builder = this;
            this.requireMigration = false;
            this.allowDestructiveMigrationOnDowngrade = true;
            return this;
        }
        
        public Builder<T> fallbackToDestructiveMigrationFrom(final int... array) {
            Intrinsics.checkNotNullParameter((Object)array, "startVersions");
            final Builder builder = this;
            for (int length = array.length, i = 0; i < length; ++i) {
                this.migrationsNotRequiredFrom.add(array[i]);
            }
            return this;
        }
        
        public Builder<T> fallbackToDestructiveMigrationOnDowngrade() {
            final Builder builder = this;
            this.requireMigration = true;
            this.allowDestructiveMigrationOnDowngrade = true;
            return this;
        }
        
        public Builder<T> openHelperFactory(final SupportSQLiteOpenHelper.Factory factory) {
            final Builder builder = this;
            this.factory = factory;
            return this;
        }
        
        @ExperimentalRoomApi
        public Builder<T> setAutoCloseTimeout(final long autoCloseTimeout, final TimeUnit autoCloseTimeUnit) {
            Intrinsics.checkNotNullParameter((Object)autoCloseTimeUnit, "autoCloseTimeUnit");
            final Builder builder = this;
            if (autoCloseTimeout >= 0L) {
                this.autoCloseTimeout = autoCloseTimeout;
                this.autoCloseTimeUnit = autoCloseTimeUnit;
                return this;
            }
            throw new IllegalArgumentException("autoCloseTimeout must be >= 0".toString());
        }
        
        public Builder<T> setJournalMode(final JournalMode journalMode) {
            Intrinsics.checkNotNullParameter((Object)journalMode, "journalMode");
            final Builder builder = this;
            this.journalMode = journalMode;
            return this;
        }
        
        @ExperimentalRoomApi
        public Builder<T> setMultiInstanceInvalidationServiceIntent(Intent multiInstanceInvalidationIntent) {
            Intrinsics.checkNotNullParameter((Object)multiInstanceInvalidationIntent, "invalidationServiceIntent");
            final Builder builder = this;
            if (this.name == null) {
                multiInstanceInvalidationIntent = null;
            }
            this.multiInstanceInvalidationIntent = multiInstanceInvalidationIntent;
            return this;
        }
        
        public Builder<T> setQueryCallback(final QueryCallback queryCallback, final Executor queryCallbackExecutor) {
            Intrinsics.checkNotNullParameter((Object)queryCallback, "queryCallback");
            Intrinsics.checkNotNullParameter((Object)queryCallbackExecutor, "executor");
            final Builder builder = this;
            this.queryCallback = queryCallback;
            this.queryCallbackExecutor = queryCallbackExecutor;
            return this;
        }
        
        public Builder<T> setQueryExecutor(final Executor queryExecutor) {
            Intrinsics.checkNotNullParameter((Object)queryExecutor, "executor");
            final Builder builder = this;
            this.queryExecutor = queryExecutor;
            return this;
        }
        
        public Builder<T> setTransactionExecutor(final Executor transactionExecutor) {
            Intrinsics.checkNotNullParameter((Object)transactionExecutor, "executor");
            final Builder builder = this;
            this.transactionExecutor = transactionExecutor;
            return this;
        }
    }
    
    @Metadata(d1 = { "\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\b&\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0016J\u0010\u0010\u0007\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0016J\u0010\u0010\b\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0016¨\u0006\t" }, d2 = { "Landroidx/room/RoomDatabase$Callback;", "", "()V", "onCreate", "", "db", "Landroidx/sqlite/db/SupportSQLiteDatabase;", "onDestructiveMigration", "onOpen", "room-runtime_release" }, k = 1, mv = { 1, 7, 1 }, xi = 48)
    public abstract static class Callback
    {
        public void onCreate(final SupportSQLiteDatabase supportSQLiteDatabase) {
            Intrinsics.checkNotNullParameter((Object)supportSQLiteDatabase, "db");
        }
        
        public void onDestructiveMigration(final SupportSQLiteDatabase supportSQLiteDatabase) {
            Intrinsics.checkNotNullParameter((Object)supportSQLiteDatabase, "db");
        }
        
        public void onOpen(final SupportSQLiteDatabase supportSQLiteDatabase) {
            Intrinsics.checkNotNullParameter((Object)supportSQLiteDatabase, "db");
        }
    }
    
    @Metadata(d1 = { "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u0010\u0010\u0003\u001a\u00020\u00048\u0006X\u0087T¢\u0006\u0002\n\u0000¨\u0006\u0005" }, d2 = { "Landroidx/room/RoomDatabase$Companion;", "", "()V", "MAX_BIND_PARAMETER_CNT", "", "room-runtime_release" }, k = 1, mv = { 1, 7, 1 }, xi = 48)
    public static final class Companion
    {
        private Companion() {
        }
    }
    
    @Metadata(d1 = { "\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0002J\u0015\u0010\u0007\u001a\u00020\u00002\u0006\u0010\b\u001a\u00020\tH\u0000¢\u0006\u0002\b\nj\u0002\b\u000bj\u0002\b\fj\u0002\b\r¨\u0006\u000e" }, d2 = { "Landroidx/room/RoomDatabase$JournalMode;", "", "(Ljava/lang/String;I)V", "isLowRamDevice", "", "activityManager", "Landroid/app/ActivityManager;", "resolve", "context", "Landroid/content/Context;", "resolve$room_runtime_release", "AUTOMATIC", "TRUNCATE", "WRITE_AHEAD_LOGGING", "room-runtime_release" }, k = 1, mv = { 1, 7, 1 }, xi = 48)
    public enum JournalMode
    {
        private static final JournalMode[] $VALUES;
        
        AUTOMATIC, 
        TRUNCATE, 
        WRITE_AHEAD_LOGGING;
        
        private static final /* synthetic */ JournalMode[] $values() {
            return new JournalMode[] { JournalMode.AUTOMATIC, JournalMode.TRUNCATE, JournalMode.WRITE_AHEAD_LOGGING };
        }
        
        static {
            $VALUES = $values();
        }
        
        private final boolean isLowRamDevice(final ActivityManager activityManager) {
            return Build$VERSION.SDK_INT >= 19 && SupportSQLiteCompat.Api19Impl.isLowRamDevice(activityManager);
        }
        
        public final JournalMode resolve$room_runtime_release(final Context context) {
            Intrinsics.checkNotNullParameter((Object)context, "context");
            if (this != JournalMode.AUTOMATIC) {
                return this;
            }
            if (Build$VERSION.SDK_INT >= 16) {
                final Object systemService = context.getSystemService("activity");
                Intrinsics.checkNotNull(systemService, "null cannot be cast to non-null type android.app.ActivityManager");
                if (!this.isLowRamDevice((ActivityManager)systemService)) {
                    return JournalMode.WRITE_AHEAD_LOGGING;
                }
            }
            return JournalMode.TRUNCATE;
        }
    }
    
    @Metadata(d1 = { "\u0000H\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010%\n\u0002\u0010\b\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u0011\n\u0000\n\u0002\u0010 \n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0007\n\u0002\u0010!\n\u0002\b\u0002\n\u0002\u0010$\n\u0000\b\u0016\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\u0010\u0010\b\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u0007H\u0002J!\u0010\u000b\u001a\u00020\t2\u0012\u0010\u0003\u001a\n\u0012\u0006\b\u0001\u0012\u00020\u00070\f\"\u00020\u0007H\u0016¢\u0006\u0002\u0010\rJ\u0016\u0010\u000b\u001a\u00020\t2\f\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00070\u000eH\u0016J\u0016\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u00052\u0006\u0010\u0012\u001a\u00020\u0005J \u0010\u0013\u001a\n\u0012\u0004\u0012\u00020\u0007\u0018\u00010\u000e2\u0006\u0010\u0014\u001a\u00020\u00052\u0006\u0010\u0015\u001a\u00020\u0005H\u0016J6\u0010\u0016\u001a\n\u0012\u0004\u0012\u00020\u0007\u0018\u00010\u000e2\f\u0010\u0017\u001a\b\u0012\u0004\u0012\u00020\u00070\u00182\u0006\u0010\u0019\u001a\u00020\u00102\u0006\u0010\u0014\u001a\u00020\u00052\u0006\u0010\u0015\u001a\u00020\u0005H\u0002J \u0010\u001a\u001a\u001a\u0012\u0004\u0012\u00020\u0005\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00070\u001b0\u001bH\u0016R&\u0010\u0003\u001a\u001a\u0012\u0004\u0012\u00020\u0005\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00070\u00060\u0004X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u001c" }, d2 = { "Landroidx/room/RoomDatabase$MigrationContainer;", "", "()V", "migrations", "", "", "Ljava/util/TreeMap;", "Landroidx/room/migration/Migration;", "addMigration", "", "migration", "addMigrations", "", "([Landroidx/room/migration/Migration;)V", "", "contains", "", "startVersion", "endVersion", "findMigrationPath", "start", "end", "findUpMigrationPath", "result", "", "upgrade", "getMigrations", "", "room-runtime_release" }, k = 1, mv = { 1, 7, 1 }, xi = 48)
    public static class MigrationContainer
    {
        private final Map<Integer, TreeMap<Integer, Migration>> migrations;
        
        public MigrationContainer() {
            this.migrations = new LinkedHashMap<Integer, TreeMap<Integer, Migration>>();
        }
        
        private final void addMigration(final Migration obj) {
            final int startVersion = obj.startVersion;
            final int endVersion = obj.endVersion;
            final Map<Integer, TreeMap<Integer, Migration>> migrations = this.migrations;
            final Integer value = startVersion;
            TreeMap value2;
            if ((value2 = migrations.get(value)) == null) {
                value2 = new TreeMap();
                migrations.put(value, value2);
            }
            final TreeMap treeMap = value2;
            final Map map = treeMap;
            if (map.containsKey(endVersion)) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Overriding migration ");
                sb.append(treeMap.get(endVersion));
                sb.append(" with ");
                sb.append(obj);
                Log.w("ROOM", sb.toString());
            }
            map.put(endVersion, obj);
        }
        
        private final List<Migration> findUpMigrationPath(final List<Migration> list, final boolean b, int intValue, final int n) {
            int i;
        Label_0232:
            do {
                final int n2 = 1;
                if (!(b ? (intValue < n) : (intValue > n))) {
                    return list;
                }
                final TreeMap treeMap = this.migrations.get(intValue);
                if (treeMap == null) {
                    return null;
                }
                Set keySet;
                if (b) {
                    keySet = treeMap.descendingKeySet();
                }
                else {
                    keySet = treeMap.keySet();
                }
                for (final Integer key : keySet) {
                    boolean b2 = false;
                    Label_0188: {
                        Label_0185: {
                            if (b) {
                                Intrinsics.checkNotNullExpressionValue((Object)key, "targetVersion");
                                final int intValue2 = key;
                                if (intValue + 1 > intValue2 || intValue2 > n) {
                                    break Label_0185;
                                }
                            }
                            else {
                                Intrinsics.checkNotNullExpressionValue((Object)key, "targetVersion");
                                final int intValue3 = key;
                                if (n > intValue3 || intValue3 >= intValue) {
                                    break Label_0185;
                                }
                            }
                            b2 = true;
                            break Label_0188;
                        }
                        b2 = false;
                    }
                    if (b2) {
                        final Object value = treeMap.get(key);
                        Intrinsics.checkNotNull(value);
                        list.add((Migration)value);
                        intValue = key;
                        i = n2;
                        continue Label_0232;
                    }
                }
                i = 0;
            } while (i != 0);
            return null;
        }
        
        public void addMigrations(final List<? extends Migration> list) {
            Intrinsics.checkNotNullParameter((Object)list, "migrations");
            final Iterator iterator = list.iterator();
            while (iterator.hasNext()) {
                this.addMigration((Migration)iterator.next());
            }
        }
        
        public void addMigrations(final Migration... array) {
            Intrinsics.checkNotNullParameter((Object)array, "migrations");
            for (int length = array.length, i = 0; i < length; ++i) {
                this.addMigration(array[i]);
            }
        }
        
        public final boolean contains(final int n, final int i) {
            final Map<Integer, Map<Integer, Migration>> migrations = this.getMigrations();
            if (migrations.containsKey(n)) {
                Map emptyMap;
                if ((emptyMap = migrations.get(n)) == null) {
                    emptyMap = MapsKt.emptyMap();
                }
                return emptyMap.containsKey(i);
            }
            return false;
        }
        
        public List<Migration> findMigrationPath(final int n, final int n2) {
            if (n == n2) {
                return CollectionsKt.emptyList();
            }
            return this.findUpMigrationPath(new ArrayList<Migration>(), n2 > n, n, n2);
        }
        
        public Map<Integer, Map<Integer, Migration>> getMigrations() {
            return (Map<Integer, Map<Integer, Migration>>)this.migrations;
        }
    }
    
    @Metadata(d1 = { "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b&\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0016¨\u0006\u0007" }, d2 = { "Landroidx/room/RoomDatabase$PrepackagedDatabaseCallback;", "", "()V", "onOpenPrepackagedDatabase", "", "db", "Landroidx/sqlite/db/SupportSQLiteDatabase;", "room-runtime_release" }, k = 1, mv = { 1, 7, 1 }, xi = 48)
    public abstract static class PrepackagedDatabaseCallback
    {
        public void onOpenPrepackagedDatabase(final SupportSQLiteDatabase supportSQLiteDatabase) {
            Intrinsics.checkNotNullParameter((Object)supportSQLiteDatabase, "db");
        }
    }
    
    @Metadata(d1 = { "\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010 \n\u0000\bf\u0018\u00002\u00020\u0001J \u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u000e\u0010\u0006\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00010\u0007H&\u00f8\u0001\u0000\u0082\u0002\u0006\n\u0004\b!0\u0001¨\u0006\b\u00c0\u0006\u0001" }, d2 = { "Landroidx/room/RoomDatabase$QueryCallback;", "", "onQuery", "", "sqlQuery", "", "bindArgs", "", "room-runtime_release" }, k = 1, mv = { 1, 7, 1 }, xi = 48)
    public interface QueryCallback
    {
        void onQuery(final String p0, final List<?> p1);
    }
}

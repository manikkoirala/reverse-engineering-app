// 
// Decompiled by Procyon v0.6.0
// 

package androidx.room;

import java.lang.ref.WeakReference;
import kotlin.text.StringsKt;
import android.os.Build$VERSION;
import java.util.concurrent.locks.Lock;
import android.database.sqlite.SQLiteException;
import android.content.Intent;
import android.content.Context;
import android.util.Log;
import kotlin.Deprecated;
import androidx.lifecycle.LiveData;
import java.util.concurrent.Callable;
import kotlin.collections.CollectionsKt;
import java.util.List;
import java.util.ArrayList;
import androidx.sqlite.db.SupportSQLiteDatabase;
import java.util.Collection;
import kotlin.collections.SetsKt;
import kotlin.Unit;
import java.util.Arrays;
import java.util.Iterator;
import kotlin.collections.MapsKt;
import java.util.Locale;
import java.util.LinkedHashMap;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.DefaultConstructorMarker;
import java.util.Set;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;
import androidx.arch.core.internal.SafeIterableMap;
import androidx.sqlite.db.SupportSQLiteStatement;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000¬\u0001\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0011\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010$\n\u0000\n\u0002\u0010\"\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\b\n\u0002\b\b\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u000f\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0010\b\u0016\u0018\u0000 e2\u00020\u0001:\u0005efghiB#\b\u0017\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0012\u0010\u0004\u001a\n\u0012\u0006\b\u0001\u0012\u00020\u00060\u0005\"\u00020\u0006¢\u0006\u0002\u0010\u0007BV\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0012\u0010\b\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00060\t\u0012\u001d\u0010\n\u001a\u0019\u0012\u0004\u0012\u00020\u0006\u0012\u000f\u0012\r\u0012\u0004\u0012\u00020\u00060\u000b¢\u0006\u0002\b\f0\t\u0012\u0012\u0010\u0004\u001a\n\u0012\u0006\b\u0001\u0012\u00020\u00060\u0005\"\u00020\u0006¢\u0006\u0002\u0010\rJ\u0010\u00108\u001a\u0002092\u0006\u0010:\u001a\u00020\"H\u0017J\u0010\u0010;\u001a\u0002092\u0006\u0010:\u001a\u00020\"H\u0017J7\u0010<\u001a\b\u0012\u0004\u0012\u0002H>0=\"\u0004\b\u0000\u0010>2\u000e\u0010\u0004\u001a\n\u0012\u0006\b\u0001\u0012\u00020\u00060\u00052\f\u0010?\u001a\b\u0012\u0004\u0012\u0002H>0@H\u0017¢\u0006\u0002\u0010AJ?\u0010<\u001a\b\u0012\u0004\u0012\u0002H>0=\"\u0004\b\u0000\u0010>2\u000e\u0010\u0004\u001a\n\u0012\u0006\b\u0001\u0012\u00020\u00060\u00052\u0006\u0010B\u001a\u00020\u00192\f\u0010?\u001a\b\u0012\u0004\u0012\u0002H>0@H\u0017¢\u0006\u0002\u0010CJ\r\u0010D\u001a\u00020\u0019H\u0000¢\u0006\u0002\bEJ\u0015\u0010F\u001a\u0002092\u0006\u0010\u0002\u001a\u00020GH\u0000¢\u0006\u0002\bHJ!\u0010I\u001a\u0002092\u0012\u0010J\u001a\n\u0012\u0006\b\u0001\u0012\u00020\u00060\u0005\"\u00020\u0006H\u0007¢\u0006\u0002\u0010KJ\b\u0010L\u001a\u000209H\u0002J\b\u0010M\u001a\u000209H\u0016J\b\u0010N\u001a\u000209H\u0017J\u0010\u0010O\u001a\u0002092\u0006\u0010:\u001a\u00020\"H\u0017J%\u0010P\u001a\n\u0012\u0006\b\u0001\u0012\u00020\u00060\u00052\u000e\u0010Q\u001a\n\u0012\u0006\b\u0001\u0012\u00020\u00060\u0005H\u0002¢\u0006\u0002\u0010RJ\u0015\u0010S\u001a\u0002092\u0006\u0010\u000e\u001a\u00020\u000fH\u0000¢\u0006\u0002\bTJ%\u0010U\u001a\u0002092\u0006\u0010V\u001a\u00020W2\u0006\u0010X\u001a\u00020\u00062\u0006\u0010Y\u001a\u00020ZH\u0000¢\u0006\u0002\b[J\u0018\u0010\\\u001a\u0002092\u0006\u0010]\u001a\u00020G2\u0006\u0010^\u001a\u000200H\u0002J\r\u0010_\u001a\u000209H\u0000¢\u0006\u0002\b`J\u0018\u0010a\u001a\u0002092\u0006\u0010]\u001a\u00020G2\u0006\u0010^\u001a\u000200H\u0002J\r\u0010b\u001a\u000209H\u0000¢\u0006\u0002\bcJ\u0015\u0010b\u001a\u0002092\u0006\u0010\u0002\u001a\u00020GH\u0000¢\u0006\u0002\bcJ%\u0010d\u001a\n\u0012\u0006\b\u0001\u0012\u00020\u00060\u00052\u000e\u0010\u0004\u001a\n\u0012\u0006\b\u0001\u0012\u00020\u00060\u0005H\u0002¢\u0006\u0002\u0010RR\u0010\u0010\u000e\u001a\u0004\u0018\u00010\u000fX\u0082\u000e¢\u0006\u0002\n\u0000R\u001c\u0010\u0010\u001a\u0004\u0018\u00010\u0011X\u0080\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0012\u0010\u0013\"\u0004\b\u0014\u0010\u0015R\u0014\u0010\u0002\u001a\u00020\u0003X\u0080\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0016\u0010\u0017R\u000e\u0010\u0018\u001a\u00020\u0019X\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u001a\u001a\u00020\u001bX\u0082\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\u001c\u001a\u0004\u0018\u00010\u001dX\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u001e\u001a\u00020\u001fX\u0082\u0004¢\u0006\u0002\n\u0000R\"\u0010 \u001a\u000e\u0012\u0004\u0012\u00020\"\u0012\u0004\u0012\u00020#0!8\u0000X\u0081\u0004¢\u0006\b\n\u0000\u001a\u0004\b$\u0010%R\u0016\u0010&\u001a\u00020'8GX\u0087\u0004¢\u0006\b\n\u0000\u001a\u0004\b(\u0010)R\u0016\u0010*\u001a\u00020+8\u0006X\u0087\u0004¢\u0006\b\n\u0000\u0012\u0004\b,\u0010-R\u001a\u0010\b\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00060\tX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010.\u001a\u00020\u0001X\u0082\u0004¢\u0006\u0002\n\u0000R \u0010/\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u0002000\tX\u0080\u0004¢\u0006\b\n\u0000\u001a\u0004\b1\u00102R\u001e\u00103\u001a\n\u0012\u0006\b\u0001\u0012\u00020\u00060\u0005X\u0080\u0004¢\u0006\n\n\u0002\u00106\u001a\u0004\b4\u00105R\u000e\u00107\u001a\u00020\u0001X\u0082\u0004¢\u0006\u0002\n\u0000R%\u0010\n\u001a\u0019\u0012\u0004\u0012\u00020\u0006\u0012\u000f\u0012\r\u0012\u0004\u0012\u00020\u00060\u000b¢\u0006\u0002\b\f0\tX\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006j" }, d2 = { "Landroidx/room/InvalidationTracker;", "", "database", "Landroidx/room/RoomDatabase;", "tableNames", "", "", "(Landroidx/room/RoomDatabase;[Ljava/lang/String;)V", "shadowTablesMap", "", "viewTables", "", "Lkotlin/jvm/JvmSuppressWildcards;", "(Landroidx/room/RoomDatabase;Ljava/util/Map;Ljava/util/Map;[Ljava/lang/String;)V", "autoCloser", "Landroidx/room/AutoCloser;", "cleanupStatement", "Landroidx/sqlite/db/SupportSQLiteStatement;", "getCleanupStatement$room_runtime_release", "()Landroidx/sqlite/db/SupportSQLiteStatement;", "setCleanupStatement$room_runtime_release", "(Landroidx/sqlite/db/SupportSQLiteStatement;)V", "getDatabase$room_runtime_release", "()Landroidx/room/RoomDatabase;", "initialized", "", "invalidationLiveDataContainer", "Landroidx/room/InvalidationLiveDataContainer;", "multiInstanceInvalidationClient", "Landroidx/room/MultiInstanceInvalidationClient;", "observedTableTracker", "Landroidx/room/InvalidationTracker$ObservedTableTracker;", "observerMap", "Landroidx/arch/core/internal/SafeIterableMap;", "Landroidx/room/InvalidationTracker$Observer;", "Landroidx/room/InvalidationTracker$ObserverWrapper;", "getObserverMap$room_runtime_release", "()Landroidx/arch/core/internal/SafeIterableMap;", "pendingRefresh", "Ljava/util/concurrent/atomic/AtomicBoolean;", "getPendingRefresh", "()Ljava/util/concurrent/atomic/AtomicBoolean;", "refreshRunnable", "Ljava/lang/Runnable;", "getRefreshRunnable$annotations", "()V", "syncTriggersLock", "tableIdLookup", "", "getTableIdLookup$room_runtime_release", "()Ljava/util/Map;", "tablesNames", "getTablesNames$room_runtime_release", "()[Ljava/lang/String;", "[Ljava/lang/String;", "trackerLock", "addObserver", "", "observer", "addWeakObserver", "createLiveData", "Landroidx/lifecycle/LiveData;", "T", "computeFunction", "Ljava/util/concurrent/Callable;", "([Ljava/lang/String;Ljava/util/concurrent/Callable;)Landroidx/lifecycle/LiveData;", "inTransaction", "([Ljava/lang/String;ZLjava/util/concurrent/Callable;)Landroidx/lifecycle/LiveData;", "ensureInitialization", "ensureInitialization$room_runtime_release", "internalInit", "Landroidx/sqlite/db/SupportSQLiteDatabase;", "internalInit$room_runtime_release", "notifyObserversByTableNames", "tables", "([Ljava/lang/String;)V", "onAutoCloseCallback", "refreshVersionsAsync", "refreshVersionsSync", "removeObserver", "resolveViews", "names", "([Ljava/lang/String;)[Ljava/lang/String;", "setAutoCloser", "setAutoCloser$room_runtime_release", "startMultiInstanceInvalidation", "context", "Landroid/content/Context;", "name", "serviceIntent", "Landroid/content/Intent;", "startMultiInstanceInvalidation$room_runtime_release", "startTrackingTable", "db", "tableId", "stopMultiInstanceInvalidation", "stopMultiInstanceInvalidation$room_runtime_release", "stopTrackingTable", "syncTriggers", "syncTriggers$room_runtime_release", "validateAndResolveTableNames", "Companion", "ObservedTableTracker", "Observer", "ObserverWrapper", "WeakObserver", "room-runtime_release" }, k = 1, mv = { 1, 7, 1 }, xi = 48)
public class InvalidationTracker
{
    private static final String CREATE_TRACKING_TABLE_SQL = "CREATE TEMP TABLE room_table_modification_log (table_id INTEGER PRIMARY KEY, invalidated INTEGER NOT NULL DEFAULT 0)";
    public static final Companion Companion;
    private static final String INVALIDATED_COLUMN_NAME = "invalidated";
    public static final String RESET_UPDATED_TABLES_SQL = "UPDATE room_table_modification_log SET invalidated = 0 WHERE invalidated = 1";
    public static final String SELECT_UPDATED_TABLES_SQL = "SELECT * FROM room_table_modification_log WHERE invalidated = 1;";
    private static final String TABLE_ID_COLUMN_NAME = "table_id";
    private static final String[] TRIGGERS;
    private static final String UPDATE_TABLE_NAME = "room_table_modification_log";
    private AutoCloser autoCloser;
    private volatile SupportSQLiteStatement cleanupStatement;
    private final RoomDatabase database;
    private volatile boolean initialized;
    private final InvalidationLiveDataContainer invalidationLiveDataContainer;
    private MultiInstanceInvalidationClient multiInstanceInvalidationClient;
    private final ObservedTableTracker observedTableTracker;
    private final SafeIterableMap<Observer, ObserverWrapper> observerMap;
    private final AtomicBoolean pendingRefresh;
    public final Runnable refreshRunnable;
    private final Map<String, String> shadowTablesMap;
    private final Object syncTriggersLock;
    private final Map<String, Integer> tableIdLookup;
    private final String[] tablesNames;
    private final Object trackerLock;
    private final Map<String, Set<String>> viewTables;
    
    static {
        Companion = new Companion(null);
        TRIGGERS = new String[] { "UPDATE", "DELETE", "INSERT" };
    }
    
    public InvalidationTracker(final RoomDatabase database, final Map<String, String> shadowTablesMap, final Map<String, Set<String>> viewTables, final String... array) {
        Intrinsics.checkNotNullParameter((Object)database, "database");
        Intrinsics.checkNotNullParameter((Object)shadowTablesMap, "shadowTablesMap");
        Intrinsics.checkNotNullParameter((Object)viewTables, "viewTables");
        Intrinsics.checkNotNullParameter((Object)array, "tableNames");
        this.database = database;
        this.shadowTablesMap = shadowTablesMap;
        this.viewTables = viewTables;
        int i = 0;
        this.pendingRefresh = new AtomicBoolean(false);
        this.observedTableTracker = new ObservedTableTracker(array.length);
        this.invalidationLiveDataContainer = new InvalidationLiveDataContainer(database);
        this.observerMap = new SafeIterableMap<Observer, ObserverWrapper>();
        this.syncTriggersLock = new Object();
        this.trackerLock = new Object();
        this.tableIdLookup = new LinkedHashMap<String, Integer>();
        final int length = array.length;
        final String[] tablesNames = new String[length];
        while (i < length) {
            final String s = array[i];
            final Locale us = Locale.US;
            Intrinsics.checkNotNullExpressionValue((Object)us, "US");
            final String lowerCase = s.toLowerCase(us);
            Intrinsics.checkNotNullExpressionValue((Object)lowerCase, "this as java.lang.String).toLowerCase(locale)");
            this.tableIdLookup.put(lowerCase, i);
            final String s2 = this.shadowTablesMap.get(array[i]);
            String lowerCase2;
            if (s2 != null) {
                final Locale us2 = Locale.US;
                Intrinsics.checkNotNullExpressionValue((Object)us2, "US");
                lowerCase2 = s2.toLowerCase(us2);
                Intrinsics.checkNotNullExpressionValue((Object)lowerCase2, "this as java.lang.String).toLowerCase(locale)");
            }
            else {
                lowerCase2 = null;
            }
            if (lowerCase2 == null) {
                lowerCase2 = lowerCase;
            }
            tablesNames[i] = lowerCase2;
            ++i;
        }
        this.tablesNames = tablesNames;
        for (final Map.Entry<K, String> entry : this.shadowTablesMap.entrySet()) {
            final String s3 = entry.getValue();
            final Locale us3 = Locale.US;
            Intrinsics.checkNotNullExpressionValue((Object)us3, "US");
            final String lowerCase3 = s3.toLowerCase(us3);
            Intrinsics.checkNotNullExpressionValue((Object)lowerCase3, "this as java.lang.String).toLowerCase(locale)");
            if (this.tableIdLookup.containsKey(lowerCase3)) {
                final String s4 = (String)entry.getKey();
                final Locale us4 = Locale.US;
                Intrinsics.checkNotNullExpressionValue((Object)us4, "US");
                final String lowerCase4 = s4.toLowerCase(us4);
                Intrinsics.checkNotNullExpressionValue((Object)lowerCase4, "this as java.lang.String).toLowerCase(locale)");
                final Map<String, Integer> tableIdLookup = this.tableIdLookup;
                tableIdLookup.put(lowerCase4, (Integer)MapsKt.getValue((Map)tableIdLookup, (Object)lowerCase3));
            }
        }
        this.refreshRunnable = (Runnable)new InvalidationTracker$refreshRunnable.InvalidationTracker$refreshRunnable$1(this);
    }
    
    public InvalidationTracker(final RoomDatabase roomDatabase, final String... original) {
        Intrinsics.checkNotNullParameter((Object)roomDatabase, "database");
        Intrinsics.checkNotNullParameter((Object)original, "tableNames");
        this(roomDatabase, MapsKt.emptyMap(), MapsKt.emptyMap(), (String[])Arrays.copyOf(original, original.length));
    }
    
    private final void onAutoCloseCallback() {
        synchronized (this.trackerLock) {
            this.initialized = false;
            this.observedTableTracker.resetTriggerState();
            final SupportSQLiteStatement cleanupStatement = this.cleanupStatement;
            if (cleanupStatement != null) {
                cleanupStatement.close();
                final Unit instance = Unit.INSTANCE;
            }
        }
    }
    
    private final String[] resolveViews(final String[] array) {
        final Set setBuilder = SetsKt.createSetBuilder();
        for (final String s : array) {
            final Map<String, Set<String>> viewTables = this.viewTables;
            final Locale us = Locale.US;
            Intrinsics.checkNotNullExpressionValue((Object)us, "US");
            final String lowerCase = s.toLowerCase(us);
            Intrinsics.checkNotNullExpressionValue((Object)lowerCase, "this as java.lang.String).toLowerCase(locale)");
            if (viewTables.containsKey(lowerCase)) {
                final Map<String, Set<String>> viewTables2 = this.viewTables;
                final Locale us2 = Locale.US;
                Intrinsics.checkNotNullExpressionValue((Object)us2, "US");
                final String lowerCase2 = s.toLowerCase(us2);
                Intrinsics.checkNotNullExpressionValue((Object)lowerCase2, "this as java.lang.String).toLowerCase(locale)");
                final Set<String> value = viewTables2.get(lowerCase2);
                Intrinsics.checkNotNull((Object)value);
                setBuilder.addAll(value);
            }
            else {
                setBuilder.add(s);
            }
        }
        final String[] array2 = SetsKt.build(setBuilder).toArray(new String[0]);
        Intrinsics.checkNotNull((Object)array2, "null cannot be cast to non-null type kotlin.Array<T of kotlin.collections.ArraysKt__ArraysJVMKt.toTypedArray>");
        return array2;
    }
    
    private final void startTrackingTable(final SupportSQLiteDatabase supportSQLiteDatabase, final int n) {
        final StringBuilder sb = new StringBuilder();
        sb.append("INSERT OR IGNORE INTO room_table_modification_log VALUES(");
        sb.append(n);
        sb.append(", 0)");
        supportSQLiteDatabase.execSQL(sb.toString());
        final String str = this.tablesNames[n];
        for (final String str2 : InvalidationTracker.TRIGGERS) {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("CREATE TEMP TRIGGER IF NOT EXISTS ");
            sb2.append(InvalidationTracker.Companion.getTriggerName$room_runtime_release(str, str2));
            sb2.append(" AFTER ");
            sb2.append(str2);
            sb2.append(" ON `");
            sb2.append(str);
            sb2.append("` BEGIN UPDATE ");
            sb2.append("room_table_modification_log");
            sb2.append(" SET ");
            sb2.append("invalidated");
            sb2.append(" = 1");
            sb2.append(" WHERE ");
            sb2.append("table_id");
            sb2.append(" = ");
            sb2.append(n);
            sb2.append(" AND ");
            sb2.append("invalidated");
            sb2.append(" = 0");
            sb2.append("; END");
            final String string = sb2.toString();
            Intrinsics.checkNotNullExpressionValue((Object)string, "StringBuilder().apply(builderAction).toString()");
            supportSQLiteDatabase.execSQL(string);
        }
    }
    
    private final void stopTrackingTable(final SupportSQLiteDatabase supportSQLiteDatabase, int i) {
        final String s = this.tablesNames[i];
        final String[] triggers = InvalidationTracker.TRIGGERS;
        int length;
        String s2;
        StringBuilder sb;
        String string;
        for (length = triggers.length, i = 0; i < length; ++i) {
            s2 = triggers[i];
            sb = new StringBuilder();
            sb.append("DROP TRIGGER IF EXISTS ");
            sb.append(InvalidationTracker.Companion.getTriggerName$room_runtime_release(s, s2));
            string = sb.toString();
            Intrinsics.checkNotNullExpressionValue((Object)string, "StringBuilder().apply(builderAction).toString()");
            supportSQLiteDatabase.execSQL(string);
        }
    }
    
    private final String[] validateAndResolveTableNames(final String[] array) {
        final String[] resolveViews = this.resolveViews(array);
        for (final String str : resolveViews) {
            final Map<String, Integer> tableIdLookup = this.tableIdLookup;
            final Locale us = Locale.US;
            Intrinsics.checkNotNullExpressionValue((Object)us, "US");
            final String lowerCase = str.toLowerCase(us);
            Intrinsics.checkNotNullExpressionValue((Object)lowerCase, "this as java.lang.String).toLowerCase(locale)");
            if (!tableIdLookup.containsKey(lowerCase)) {
                final StringBuilder sb = new StringBuilder();
                sb.append("There is no table with name ");
                sb.append(str);
                throw new IllegalArgumentException(sb.toString().toString());
            }
        }
        return resolveViews;
    }
    
    public void addObserver(final Observer observer) {
        Intrinsics.checkNotNullParameter((Object)observer, "observer");
        final String[] resolveViews = this.resolveViews(observer.getTables$room_runtime_release());
        final Collection collection = new ArrayList(resolveViews.length);
        for (final String str : resolveViews) {
            final Map<String, Integer> tableIdLookup = this.tableIdLookup;
            final Locale us = Locale.US;
            Intrinsics.checkNotNullExpressionValue((Object)us, "US");
            final String lowerCase = str.toLowerCase(us);
            Intrinsics.checkNotNullExpressionValue((Object)lowerCase, "this as java.lang.String).toLowerCase(locale)");
            final Integer n = tableIdLookup.get(lowerCase);
            if (n == null) {
                final StringBuilder sb = new StringBuilder();
                sb.append("There is no table with name ");
                sb.append(str);
                throw new IllegalArgumentException(sb.toString());
            }
            collection.add(n);
        }
        final int[] intArray = CollectionsKt.toIntArray((Collection)collection);
        final ObserverWrapper observerWrapper = new ObserverWrapper(observer, intArray, resolveViews);
        synchronized (this.observerMap) {
            final ObserverWrapper observerWrapper2 = this.observerMap.putIfAbsent(observer, observerWrapper);
            monitorexit(this.observerMap);
            if (observerWrapper2 == null && this.observedTableTracker.onAdded(Arrays.copyOf(intArray, intArray.length))) {
                this.syncTriggers$room_runtime_release();
            }
        }
    }
    
    public void addWeakObserver(final Observer observer) {
        Intrinsics.checkNotNullParameter((Object)observer, "observer");
        this.addObserver((Observer)new WeakObserver(this, observer));
    }
    
    @Deprecated(message = "Use [createLiveData(String[], boolean, Callable)]")
    public <T> LiveData<T> createLiveData(final String[] array, final Callable<T> callable) {
        Intrinsics.checkNotNullParameter((Object)array, "tableNames");
        Intrinsics.checkNotNullParameter((Object)callable, "computeFunction");
        return this.createLiveData(array, false, callable);
    }
    
    public <T> LiveData<T> createLiveData(final String[] array, final boolean b, final Callable<T> callable) {
        Intrinsics.checkNotNullParameter((Object)array, "tableNames");
        Intrinsics.checkNotNullParameter((Object)callable, "computeFunction");
        return this.invalidationLiveDataContainer.create(this.validateAndResolveTableNames(array), b, callable);
    }
    
    public final boolean ensureInitialization$room_runtime_release() {
        if (!this.database.isOpenInternal()) {
            return false;
        }
        if (!this.initialized) {
            this.database.getOpenHelper().getWritableDatabase();
        }
        if (!this.initialized) {
            Log.e("ROOM", "database is not initialized even though it is open");
            return false;
        }
        return true;
    }
    
    public final SupportSQLiteStatement getCleanupStatement$room_runtime_release() {
        return this.cleanupStatement;
    }
    
    public final RoomDatabase getDatabase$room_runtime_release() {
        return this.database;
    }
    
    public final SafeIterableMap<Observer, ObserverWrapper> getObserverMap$room_runtime_release() {
        return this.observerMap;
    }
    
    public final AtomicBoolean getPendingRefresh() {
        return this.pendingRefresh;
    }
    
    public final Map<String, Integer> getTableIdLookup$room_runtime_release() {
        return this.tableIdLookup;
    }
    
    public final String[] getTablesNames$room_runtime_release() {
        return this.tablesNames;
    }
    
    public final void internalInit$room_runtime_release(final SupportSQLiteDatabase supportSQLiteDatabase) {
        Intrinsics.checkNotNullParameter((Object)supportSQLiteDatabase, "database");
        synchronized (this.trackerLock) {
            if (this.initialized) {
                Log.e("ROOM", "Invalidation tracker is initialized twice :/.");
                return;
            }
            supportSQLiteDatabase.execSQL("PRAGMA temp_store = MEMORY;");
            supportSQLiteDatabase.execSQL("PRAGMA recursive_triggers='ON';");
            supportSQLiteDatabase.execSQL("CREATE TEMP TABLE room_table_modification_log (table_id INTEGER PRIMARY KEY, invalidated INTEGER NOT NULL DEFAULT 0)");
            this.syncTriggers$room_runtime_release(supportSQLiteDatabase);
            this.cleanupStatement = supportSQLiteDatabase.compileStatement("UPDATE room_table_modification_log SET invalidated = 0 WHERE invalidated = 1");
            this.initialized = true;
            final Unit instance = Unit.INSTANCE;
        }
    }
    
    public final void notifyObserversByTableNames(final String... array) {
        Intrinsics.checkNotNullParameter((Object)array, "tables");
        synchronized (this.observerMap) {
            for (final Map.Entry<Observer, V> entry : this.observerMap) {
                Intrinsics.checkNotNullExpressionValue((Object)entry, "(observer, wrapper)");
                final Observer observer = entry.getKey();
                final ObserverWrapper observerWrapper = (ObserverWrapper)entry.getValue();
                if (!observer.isRemote$room_runtime_release()) {
                    observerWrapper.notifyByTableNames$room_runtime_release(array);
                }
            }
            final Unit instance = Unit.INSTANCE;
        }
    }
    
    public void refreshVersionsAsync() {
        if (this.pendingRefresh.compareAndSet(false, true)) {
            final AutoCloser autoCloser = this.autoCloser;
            if (autoCloser != null) {
                autoCloser.incrementCountAndEnsureDbIsOpen();
            }
            this.database.getQueryExecutor().execute(this.refreshRunnable);
        }
    }
    
    public void refreshVersionsSync() {
        final AutoCloser autoCloser = this.autoCloser;
        if (autoCloser != null) {
            autoCloser.incrementCountAndEnsureDbIsOpen();
        }
        this.syncTriggers$room_runtime_release();
        this.refreshRunnable.run();
    }
    
    public void removeObserver(final Observer observer) {
        Intrinsics.checkNotNullParameter((Object)observer, "observer");
        Object original = this.observerMap;
        synchronized (original) {
            final ObserverWrapper observerWrapper = this.observerMap.remove(observer);
            monitorexit(original);
            if (observerWrapper != null) {
                final ObservedTableTracker observedTableTracker = this.observedTableTracker;
                original = observerWrapper.getTableIds$room_runtime_release();
                if (observedTableTracker.onRemoved(Arrays.copyOf((int[])original, ((int[])original).length))) {
                    this.syncTriggers$room_runtime_release();
                }
            }
        }
    }
    
    public final void setAutoCloser$room_runtime_release(final AutoCloser autoCloser) {
        Intrinsics.checkNotNullParameter((Object)autoCloser, "autoCloser");
        (this.autoCloser = autoCloser).setAutoCloseCallback(new InvalidationTracker$$ExternalSyntheticLambda0(this));
    }
    
    public final void setCleanupStatement$room_runtime_release(final SupportSQLiteStatement cleanupStatement) {
        this.cleanupStatement = cleanupStatement;
    }
    
    public final void startMultiInstanceInvalidation$room_runtime_release(final Context context, final String s, final Intent intent) {
        Intrinsics.checkNotNullParameter((Object)context, "context");
        Intrinsics.checkNotNullParameter((Object)s, "name");
        Intrinsics.checkNotNullParameter((Object)intent, "serviceIntent");
        this.multiInstanceInvalidationClient = new MultiInstanceInvalidationClient(context, s, intent, this, this.database.getQueryExecutor());
    }
    
    public final void stopMultiInstanceInvalidation$room_runtime_release() {
        final MultiInstanceInvalidationClient multiInstanceInvalidationClient = this.multiInstanceInvalidationClient;
        if (multiInstanceInvalidationClient != null) {
            multiInstanceInvalidationClient.stop();
        }
        this.multiInstanceInvalidationClient = null;
    }
    
    public final void syncTriggers$room_runtime_release() {
        if (!this.database.isOpenInternal()) {
            return;
        }
        this.syncTriggers$room_runtime_release(this.database.getOpenHelper().getWritableDatabase());
    }
    
    public final void syncTriggers$room_runtime_release(SupportSQLiteDatabase instance) {
        Intrinsics.checkNotNullParameter((Object)instance, "database");
        if (instance.inTransaction()) {
            return;
        }
        try {
            final Lock closeLock$room_runtime_release = this.database.getCloseLock$room_runtime_release();
            closeLock$room_runtime_release.lock();
            try {
                synchronized (this.syncTriggersLock) {
                    final int[] tablesToSync = this.observedTableTracker.getTablesToSync();
                    if (tablesToSync == null) {
                        return;
                    }
                    InvalidationTracker.Companion.beginTransactionInternal$room_runtime_release(instance);
                    try {
                        for (int length = tablesToSync.length, i = 0, n = 0; i < length; ++i, ++n) {
                            final int n2 = tablesToSync[i];
                            if (n2 != 1) {
                                if (n2 == 2) {
                                    this.stopTrackingTable(instance, n);
                                }
                            }
                            else {
                                this.startTrackingTable(instance, n);
                            }
                        }
                        instance.setTransactionSuccessful();
                        instance.endTransaction();
                        instance = (SupportSQLiteDatabase)Unit.INSTANCE;
                    }
                    finally {
                        instance.endTransaction();
                    }
                }
            }
            finally {
                closeLock$room_runtime_release.unlock();
            }
        }
        catch (final SQLiteException ex) {
            Log.e("ROOM", "Cannot run invalidation tracker. Is the db closed?", (Throwable)ex);
        }
        catch (final IllegalStateException ex2) {
            Log.e("ROOM", "Cannot run invalidation tracker. Is the db closed?", (Throwable)ex2);
        }
    }
    
    @Metadata(d1 = { "\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0007\n\u0002\u0010\u0011\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0015\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u0012H\u0000¢\u0006\u0002\b\u0013J\u001d\u0010\u0014\u001a\u00020\u00042\u0006\u0010\u0015\u001a\u00020\u00042\u0006\u0010\u0016\u001a\u00020\u0004H\u0000¢\u0006\u0002\b\u0017R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082T¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082T¢\u0006\u0002\n\u0000R\u0016\u0010\u0006\u001a\u00020\u00048\u0000X\u0081T¢\u0006\b\n\u0000\u0012\u0004\b\u0007\u0010\u0002R\u0016\u0010\b\u001a\u00020\u00048\u0000X\u0081T¢\u0006\b\n\u0000\u0012\u0004\b\t\u0010\u0002R\u000e\u0010\n\u001a\u00020\u0004X\u0082T¢\u0006\u0002\n\u0000R\u0016\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\u00040\fX\u0082\u0004¢\u0006\u0004\n\u0002\u0010\rR\u000e\u0010\u000e\u001a\u00020\u0004X\u0082T¢\u0006\u0002\n\u0000¨\u0006\u0018" }, d2 = { "Landroidx/room/InvalidationTracker$Companion;", "", "()V", "CREATE_TRACKING_TABLE_SQL", "", "INVALIDATED_COLUMN_NAME", "RESET_UPDATED_TABLES_SQL", "getRESET_UPDATED_TABLES_SQL$room_runtime_release$annotations", "SELECT_UPDATED_TABLES_SQL", "getSELECT_UPDATED_TABLES_SQL$room_runtime_release$annotations", "TABLE_ID_COLUMN_NAME", "TRIGGERS", "", "[Ljava/lang/String;", "UPDATE_TABLE_NAME", "beginTransactionInternal", "", "database", "Landroidx/sqlite/db/SupportSQLiteDatabase;", "beginTransactionInternal$room_runtime_release", "getTriggerName", "tableName", "triggerType", "getTriggerName$room_runtime_release", "room-runtime_release" }, k = 1, mv = { 1, 7, 1 }, xi = 48)
    public static final class Companion
    {
        private Companion() {
        }
        
        public final void beginTransactionInternal$room_runtime_release(final SupportSQLiteDatabase supportSQLiteDatabase) {
            Intrinsics.checkNotNullParameter((Object)supportSQLiteDatabase, "database");
            if (Build$VERSION.SDK_INT >= 16 && supportSQLiteDatabase.isWriteAheadLoggingEnabled()) {
                supportSQLiteDatabase.beginTransactionNonExclusive();
            }
            else {
                supportSQLiteDatabase.beginTransaction();
            }
        }
        
        public final String getTriggerName$room_runtime_release(final String str, final String str2) {
            Intrinsics.checkNotNullParameter((Object)str, "tableName");
            Intrinsics.checkNotNullParameter((Object)str2, "triggerType");
            final StringBuilder sb = new StringBuilder();
            sb.append("`room_table_modification_trigger_");
            sb.append(str);
            sb.append('_');
            sb.append(str2);
            sb.append('`');
            return sb.toString();
        }
    }
    
    @Metadata(d1 = { "\u00008\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0010\u0016\n\u0002\b\u0003\n\u0002\u0010\u0015\n\u0000\n\u0002\u0010\u0018\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0002\b\u0002\b\u0000\u0018\u0000 \u00192\u00020\u0001:\u0001\u0019B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\n\u0010\u0013\u001a\u0004\u0018\u00010\u0010H\u0007J\u0012\u0010\u0014\u001a\u00020\u00062\n\u0010\u0015\u001a\u00020\u0010\"\u00020\u0003J\u0012\u0010\u0016\u001a\u00020\u00062\n\u0010\u0015\u001a\u00020\u0010\"\u00020\u0003J\u0006\u0010\u0017\u001a\u00020\u0018R\u001a\u0010\u0005\u001a\u00020\u0006X\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0007\u0010\b\"\u0004\b\t\u0010\nR\u0011\u0010\u000b\u001a\u00020\f¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000eR\u000e\u0010\u000f\u001a\u00020\u0010X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0012X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u001a" }, d2 = { "Landroidx/room/InvalidationTracker$ObservedTableTracker;", "", "tableCount", "", "(I)V", "needsSync", "", "getNeedsSync", "()Z", "setNeedsSync", "(Z)V", "tableObservers", "", "getTableObservers", "()[J", "triggerStateChanges", "", "triggerStates", "", "getTablesToSync", "onAdded", "tableIds", "onRemoved", "resetTriggerState", "", "Companion", "room-runtime_release" }, k = 1, mv = { 1, 7, 1 }, xi = 48)
    public static final class ObservedTableTracker
    {
        public static final int ADD = 1;
        public static final Companion Companion;
        public static final int NO_OP = 0;
        public static final int REMOVE = 2;
        private boolean needsSync;
        private final long[] tableObservers;
        private final int[] triggerStateChanges;
        private final boolean[] triggerStates;
        
        static {
            Companion = new Companion(null);
        }
        
        public ObservedTableTracker(final int n) {
            this.tableObservers = new long[n];
            this.triggerStates = new boolean[n];
            this.triggerStateChanges = new int[n];
        }
        
        public final boolean getNeedsSync() {
            return this.needsSync;
        }
        
        public final long[] getTableObservers() {
            return this.tableObservers;
        }
        
        public final int[] getTablesToSync() {
            synchronized (this) {
                if (!this.needsSync) {
                    return null;
                }
                final long[] tableObservers = this.tableObservers;
                for (int length = tableObservers.length, i = 0, n = 0; i < length; ++i, ++n) {
                    final long n2 = tableObservers[i];
                    int n3 = 1;
                    final boolean b = n2 > 0L;
                    final boolean[] triggerStates = this.triggerStates;
                    if (b != triggerStates[n]) {
                        final int[] triggerStateChanges = this.triggerStateChanges;
                        if (!b) {
                            n3 = 2;
                        }
                        triggerStateChanges[n] = n3;
                    }
                    else {
                        this.triggerStateChanges[n] = 0;
                    }
                    triggerStates[n] = b;
                }
                this.needsSync = false;
                return this.triggerStateChanges.clone();
            }
        }
        
        public final boolean onAdded(final int... array) {
            Intrinsics.checkNotNullParameter((Object)array, "tableIds");
            synchronized (this) {
                final int length = array.length;
                int i = 0;
                boolean b = false;
                while (i < length) {
                    final int n = array[i];
                    final long[] tableObservers = this.tableObservers;
                    final long n2 = tableObservers[n];
                    tableObservers[n] = 1L + n2;
                    if (n2 == 0L) {
                        this.needsSync = true;
                        b = true;
                    }
                    ++i;
                }
                final Unit instance = Unit.INSTANCE;
                return b;
            }
        }
        
        public final boolean onRemoved(final int... array) {
            Intrinsics.checkNotNullParameter((Object)array, "tableIds");
            synchronized (this) {
                final int length = array.length;
                int i = 0;
                boolean b = false;
                while (i < length) {
                    if (this.tableObservers[array[i]]-- == 1L) {
                        this.needsSync = true;
                        b = true;
                    }
                    ++i;
                }
                final Unit instance = Unit.INSTANCE;
                return b;
            }
        }
        
        public final void resetTriggerState() {
            synchronized (this) {
                Arrays.fill(this.triggerStates, false);
                this.needsSync = true;
                final Unit instance = Unit.INSTANCE;
            }
        }
        
        public final void setNeedsSync(final boolean needsSync) {
            this.needsSync = needsSync;
        }
        
        @Metadata(d1 = { "\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\b\u0080\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0086T¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0086T¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004X\u0086T¢\u0006\u0002\n\u0000¨\u0006\u0007" }, d2 = { "Landroidx/room/InvalidationTracker$ObservedTableTracker$Companion;", "", "()V", "ADD", "", "NO_OP", "REMOVE", "room-runtime_release" }, k = 1, mv = { 1, 7, 1 }, xi = 48)
        public static final class Companion
        {
            private Companion() {
            }
        }
    }
    
    @Metadata(d1 = { "\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0011\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0002\b\u0006\n\u0002\u0010\u0002\n\u0002\u0010\"\n\u0000\b&\u0018\u00002\u00020\u0001B#\b\u0014\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0012\u0010\u0004\u001a\n\u0012\u0006\b\u0001\u0012\u00020\u00030\u0005\"\u00020\u0003¢\u0006\u0002\u0010\u0006B\u0015\u0012\u000e\u0010\u0007\u001a\n\u0012\u0006\b\u0001\u0012\u00020\u00030\u0005¢\u0006\u0002\u0010\bJ\u0016\u0010\u0010\u001a\u00020\u00112\f\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00030\u0012H&R\u0014\u0010\t\u001a\u00020\n8PX\u0090\u0004¢\u0006\u0006\u001a\u0004\b\u000b\u0010\fR\u001e\u0010\u0007\u001a\n\u0012\u0006\b\u0001\u0012\u00020\u00030\u0005X\u0080\u0004¢\u0006\n\n\u0002\u0010\u000f\u001a\u0004\b\r\u0010\u000e¨\u0006\u0013" }, d2 = { "Landroidx/room/InvalidationTracker$Observer;", "", "firstTable", "", "rest", "", "(Ljava/lang/String;[Ljava/lang/String;)V", "tables", "([Ljava/lang/String;)V", "isRemote", "", "isRemote$room_runtime_release", "()Z", "getTables$room_runtime_release", "()[Ljava/lang/String;", "[Ljava/lang/String;", "onInvalidated", "", "", "room-runtime_release" }, k = 1, mv = { 1, 7, 1 }, xi = 48)
    public abstract static class Observer
    {
        private final String[] tables;
        
        protected Observer(final String s, final String... array) {
            Intrinsics.checkNotNullParameter((Object)s, "firstTable");
            Intrinsics.checkNotNullParameter((Object)array, "rest");
            final List listBuilder = CollectionsKt.createListBuilder();
            CollectionsKt.addAll((Collection)listBuilder, (Object[])array);
            listBuilder.add(s);
            final String[] array2 = CollectionsKt.build(listBuilder).toArray(new String[0]);
            Intrinsics.checkNotNull((Object)array2, "null cannot be cast to non-null type kotlin.Array<T of kotlin.collections.ArraysKt__ArraysJVMKt.toTypedArray>");
            this(array2);
        }
        
        public Observer(final String[] tables) {
            Intrinsics.checkNotNullParameter((Object)tables, "tables");
            this.tables = tables;
        }
        
        public final String[] getTables$room_runtime_release() {
            return this.tables;
        }
        
        public boolean isRemote$room_runtime_release() {
            return false;
        }
        
        public abstract void onInvalidated(final Set<String> p0);
    }
    
    @Metadata(d1 = { "\u00008\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0015\n\u0000\n\u0002\u0010\u0011\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0010\"\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0006\b\u0000\u0018\u00002\u00020\u0001B%\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u000e\u0010\u0006\u001a\n\u0012\u0006\b\u0001\u0012\u00020\b0\u0007¢\u0006\u0002\u0010\tJ\u001d\u0010\u0011\u001a\u00020\u00122\u000e\u0010\u0013\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00140\rH\u0000¢\u0006\u0002\b\u0015J\u001f\u0010\u0016\u001a\u00020\u00122\u000e\u0010\u0017\u001a\n\u0012\u0006\b\u0001\u0012\u00020\b0\u0007H\u0000¢\u0006\u0004\b\u0018\u0010\u0019R\u0014\u0010\u0002\u001a\u00020\u0003X\u0080\u0004¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000bR\u0014\u0010\f\u001a\b\u0012\u0004\u0012\u00020\b0\rX\u0082\u0004¢\u0006\u0002\n\u0000R\u0014\u0010\u0004\u001a\u00020\u0005X\u0080\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\u000fR\u0018\u0010\u0006\u001a\n\u0012\u0006\b\u0001\u0012\u00020\b0\u0007X\u0082\u0004¢\u0006\u0004\n\u0002\u0010\u0010¨\u0006\u001a" }, d2 = { "Landroidx/room/InvalidationTracker$ObserverWrapper;", "", "observer", "Landroidx/room/InvalidationTracker$Observer;", "tableIds", "", "tableNames", "", "", "(Landroidx/room/InvalidationTracker$Observer;[I[Ljava/lang/String;)V", "getObserver$room_runtime_release", "()Landroidx/room/InvalidationTracker$Observer;", "singleTableSet", "", "getTableIds$room_runtime_release", "()[I", "[Ljava/lang/String;", "notifyByTableInvalidStatus", "", "invalidatedTablesIds", "", "notifyByTableInvalidStatus$room_runtime_release", "notifyByTableNames", "tables", "notifyByTableNames$room_runtime_release", "([Ljava/lang/String;)V", "room-runtime_release" }, k = 1, mv = { 1, 7, 1 }, xi = 48)
    public static final class ObserverWrapper
    {
        private final Observer observer;
        private final Set<String> singleTableSet;
        private final int[] tableIds;
        private final String[] tableNames;
        
        public ObserverWrapper(final Observer observer, final int[] tableIds, final String[] tableNames) {
            Intrinsics.checkNotNullParameter((Object)observer, "observer");
            Intrinsics.checkNotNullParameter((Object)tableIds, "tableIds");
            Intrinsics.checkNotNullParameter((Object)tableNames, "tableNames");
            this.observer = observer;
            this.tableIds = tableIds;
            this.tableNames = tableNames;
            final int length = tableNames.length;
            final int n = 1;
            Set singleTableSet;
            if (length == 0 ^ true) {
                singleTableSet = SetsKt.setOf((Object)tableNames[0]);
            }
            else {
                singleTableSet = SetsKt.emptySet();
            }
            this.singleTableSet = singleTableSet;
            int n2;
            if (tableIds.length == tableNames.length) {
                n2 = n;
            }
            else {
                n2 = 0;
            }
            if (n2 != 0) {
                return;
            }
            throw new IllegalStateException("Check failed.".toString());
        }
        
        public final Observer getObserver$room_runtime_release() {
            return this.observer;
        }
        
        public final int[] getTableIds$room_runtime_release() {
            return this.tableIds;
        }
        
        public final void notifyByTableInvalidStatus$room_runtime_release(final Set<Integer> set) {
            Intrinsics.checkNotNullParameter((Object)set, "invalidatedTablesIds");
            final int[] tableIds = this.tableIds;
            final int length = tableIds.length;
            Set<String> set2;
            if (length != 0) {
                int i = 0;
                if (length != 1) {
                    final Set setBuilder = SetsKt.createSetBuilder();
                    final int[] tableIds2 = this.tableIds;
                    for (int length2 = tableIds2.length, n = 0; i < length2; ++i, ++n) {
                        if (set.contains(tableIds2[i])) {
                            setBuilder.add(this.tableNames[n]);
                        }
                    }
                    set2 = SetsKt.build(setBuilder);
                }
                else if (set.contains(tableIds[0])) {
                    set2 = this.singleTableSet;
                }
                else {
                    set2 = SetsKt.emptySet();
                }
            }
            else {
                set2 = SetsKt.emptySet();
            }
            if (set2.isEmpty() ^ true) {
                this.observer.onInvalidated(set2);
            }
        }
        
        public final void notifyByTableNames$room_runtime_release(final String[] array) {
            Intrinsics.checkNotNullParameter((Object)array, "tables");
            final int length = this.tableNames.length;
            Set<String> set;
            if (length != 0) {
                final int n = 0;
                if (length != 1) {
                    final Set setBuilder = SetsKt.createSetBuilder();
                    for (final String s : array) {
                        for (final String s2 : this.tableNames) {
                            if (StringsKt.equals(s2, s, true)) {
                                setBuilder.add(s2);
                            }
                        }
                    }
                    set = SetsKt.build(setBuilder);
                }
                else {
                    final int length4 = array.length;
                    int n2 = 0;
                    int n3;
                    while (true) {
                        n3 = n;
                        if (n2 >= length4) {
                            break;
                        }
                        if (StringsKt.equals(array[n2], this.tableNames[0], true)) {
                            n3 = 1;
                            break;
                        }
                        ++n2;
                    }
                    if (n3 != 0) {
                        set = this.singleTableSet;
                    }
                    else {
                        set = SetsKt.emptySet();
                    }
                }
            }
            else {
                set = SetsKt.emptySet();
            }
            if (set.isEmpty() ^ true) {
                this.observer.onInvalidated(set);
            }
        }
    }
    
    @Metadata(d1 = { "\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\"\n\u0002\u0010\u000e\n\u0000\b\u0000\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0001¢\u0006\u0002\u0010\u0005J\u0016\u0010\f\u001a\u00020\r2\f\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\u00100\u000fH\u0016R\u0017\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00010\u0007¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010\tR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000b¨\u0006\u0011" }, d2 = { "Landroidx/room/InvalidationTracker$WeakObserver;", "Landroidx/room/InvalidationTracker$Observer;", "tracker", "Landroidx/room/InvalidationTracker;", "delegate", "(Landroidx/room/InvalidationTracker;Landroidx/room/InvalidationTracker$Observer;)V", "delegateRef", "Ljava/lang/ref/WeakReference;", "getDelegateRef", "()Ljava/lang/ref/WeakReference;", "getTracker", "()Landroidx/room/InvalidationTracker;", "onInvalidated", "", "tables", "", "", "room-runtime_release" }, k = 1, mv = { 1, 7, 1 }, xi = 48)
    public static final class WeakObserver extends Observer
    {
        private final WeakReference<Observer> delegateRef;
        private final InvalidationTracker tracker;
        
        public WeakObserver(final InvalidationTracker tracker, final Observer referent) {
            Intrinsics.checkNotNullParameter((Object)tracker, "tracker");
            Intrinsics.checkNotNullParameter((Object)referent, "delegate");
            super(referent.getTables$room_runtime_release());
            this.tracker = tracker;
            this.delegateRef = new WeakReference<Observer>(referent);
        }
        
        public final WeakReference<Observer> getDelegateRef() {
            return this.delegateRef;
        }
        
        public final InvalidationTracker getTracker() {
            return this.tracker;
        }
        
        @Override
        public void onInvalidated(final Set<String> set) {
            Intrinsics.checkNotNullParameter((Object)set, "tables");
            final Observer observer = this.delegateRef.get();
            if (observer == null) {
                this.tracker.removeObserver((Observer)this);
            }
            else {
                observer.onInvalidated(set);
            }
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.room;

import kotlin.jvm.internal.Intrinsics;
import kotlin.Metadata;
import androidx.sqlite.db.SupportSQLiteOpenHelper;

@Metadata(d1 = { "\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0000\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0001\u0012\u0006\u0010\u0003\u001a\u00020\u0004¢\u0006\u0002\u0010\u0005J\u0010\u0010\u0006\u001a\u00020\u00072\u0006\u0010\b\u001a\u00020\tH\u0016R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0001X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\n" }, d2 = { "Landroidx/room/AutoClosingRoomOpenHelperFactory;", "Landroidx/sqlite/db/SupportSQLiteOpenHelper$Factory;", "delegate", "autoCloser", "Landroidx/room/AutoCloser;", "(Landroidx/sqlite/db/SupportSQLiteOpenHelper$Factory;Landroidx/room/AutoCloser;)V", "create", "Landroidx/room/AutoClosingRoomOpenHelper;", "configuration", "Landroidx/sqlite/db/SupportSQLiteOpenHelper$Configuration;", "room-runtime_release" }, k = 1, mv = { 1, 7, 1 }, xi = 48)
public final class AutoClosingRoomOpenHelperFactory implements Factory
{
    private final AutoCloser autoCloser;
    private final Factory delegate;
    
    public AutoClosingRoomOpenHelperFactory(final Factory delegate, final AutoCloser autoCloser) {
        Intrinsics.checkNotNullParameter((Object)delegate, "delegate");
        Intrinsics.checkNotNullParameter((Object)autoCloser, "autoCloser");
        this.delegate = delegate;
        this.autoCloser = autoCloser;
    }
    
    public AutoClosingRoomOpenHelper create(final Configuration configuration) {
        Intrinsics.checkNotNullParameter((Object)configuration, "configuration");
        return new AutoClosingRoomOpenHelper(this.delegate.create(configuration), this.autoCloser);
    }
}

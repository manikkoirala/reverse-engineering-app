// 
// Decompiled by Procyon v0.6.0
// 

package androidx.room;

import kotlin.collections.IntIterator;
import java.util.NoSuchElementException;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.ranges.IntRange;
import kotlin.jvm.JvmStatic;
import java.util.Set;
import kotlin.jvm.internal.Ref$ObjectRef;
import java.util.Collection;
import kotlin.collections.SetsKt;
import java.util.Locale;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.functions.Function3;
import java.util.ArrayList;
import java.util.Iterator;
import kotlin.collections.CollectionsKt;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import java.util.List;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000L\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0000\n\u0002\u0010!\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0011\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0015\n\u0002\b\u0007\b\u00c7\u0002\u0018\u00002\u00020\u0001:\u0003\u001b\u001c\u001dB\u0007\b\u0002¢\u0006\u0002\u0010\u0002JV\u0010\u0003\u001a\u00020\u0004\"\u0004\b\u0000\u0010\u00052\u0012\u0010\u0006\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u0002H\u00050\u00070\u00072\u000e\b\u0002\u0010\b\u001a\b\u0012\u0004\u0012\u0002H\u00050\t2\b\b\u0002\u0010\n\u001a\u00020\u000b2\u0018\u0010\f\u001a\u0014\u0012\n\u0012\b\u0012\u0004\u0012\u0002H\u00050\u0007\u0012\u0004\u0012\u00020\u00040\rH\u0002JO\u0010\u000e\u001a\u00020\u00042\f\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u000f0\u00072\f\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u00120\u00112$\u0010\u0013\u001a \u0012\u0004\u0012\u00020\u000b\u0012\u0004\u0012\u00020\u000b\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u000f0\u0007\u0012\u0004\u0012\u00020\u00040\u0014H\u0002¢\u0006\u0002\u0010\u0015J5\u0010\u0016\u001a\b\u0012\u0004\u0012\u00020\u00170\u00112\f\u0010\u0018\u001a\b\u0012\u0004\u0012\u00020\u00120\u00112\u0012\u0010\u0019\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00120\u00110\u0011H\u0007¢\u0006\u0002\u0010\u001a¨\u0006\u001e" }, d2 = { "Landroidx/room/AmbiguousColumnResolver;", "", "()V", "dfs", "", "T", "content", "", "current", "", "depth", "", "block", "Lkotlin/Function1;", "rabinKarpSearch", "Landroidx/room/AmbiguousColumnResolver$ResultColumn;", "pattern", "", "", "onHashMatch", "Lkotlin/Function3;", "(Ljava/util/List;[Ljava/lang/String;Lkotlin/jvm/functions/Function3;)V", "resolve", "", "resultColumns", "mappings", "([Ljava/lang/String;[[Ljava/lang/String;)[[I", "Match", "ResultColumn", "Solution", "room-common" }, k = 1, mv = { 1, 7, 1 }, xi = 48)
public final class AmbiguousColumnResolver
{
    public static final AmbiguousColumnResolver INSTANCE;
    
    static {
        INSTANCE = new AmbiguousColumnResolver();
    }
    
    private AmbiguousColumnResolver() {
    }
    
    private final <T> void dfs(final List<? extends List<? extends T>> list, final List<T> list2, final int n, final Function1<? super List<? extends T>, Unit> function1) {
        if (n == list.size()) {
            function1.invoke((Object)CollectionsKt.toList((Iterable)list2));
            return;
        }
        final Iterator iterator = list.get(n).iterator();
        while (iterator.hasNext()) {
            list2.add(iterator.next());
            AmbiguousColumnResolver.INSTANCE.dfs(list, (List<Object>)list2, n + 1, (kotlin.jvm.functions.Function1<? super List<?>, Unit>)function1);
            CollectionsKt.removeLast((List)list2);
        }
    }
    
    static /* synthetic */ void dfs$default(final AmbiguousColumnResolver ambiguousColumnResolver, final List list, List list2, int n, final Function1 function1, final int n2, final Object o) {
        if ((n2 & 0x2) != 0x0) {
            list2 = new ArrayList();
        }
        if ((n2 & 0x4) != 0x0) {
            n = 0;
        }
        ambiguousColumnResolver.dfs(list, (List<Object>)list2, n, (kotlin.jvm.functions.Function1<? super List<?>, Unit>)function1);
    }
    
    private final void rabinKarpSearch(final List<ResultColumn> list, final String[] array, final Function3<? super Integer, ? super Integer, ? super List<ResultColumn>, Unit> function3) {
        final int length = array.length;
        final int n = 0;
        int i = 0;
        int n2 = 0;
        while (i < length) {
            n2 += array[i].hashCode();
            ++i;
        }
        final int length2 = array.length;
        final Iterator iterator = list.subList(0, length2).iterator();
        int n3 = 0;
        int j;
        int n4;
        int k;
        while (true) {
            j = n;
            n4 = n3;
            k = length2;
            if (!iterator.hasNext()) {
                break;
            }
            n3 += ((ResultColumn)iterator.next()).getName().hashCode();
        }
        while (true) {
            if (n2 == n4) {
                function3.invoke((Object)j, (Object)k, (Object)list.subList(j, k));
            }
            ++j;
            if (++k > list.size()) {
                break;
            }
            n4 = n4 - list.get(j - 1).getName().hashCode() + ((ResultColumn)list.get(k - 1)).getName().hashCode();
        }
    }
    
    @JvmStatic
    public static final int[][] resolve(final String[] array, final String[][] array2) {
        Intrinsics.checkNotNullParameter((Object)array, "resultColumns");
        Intrinsics.checkNotNullParameter((Object)array2, "mappings");
        final int length = array.length;
        int n = 0;
        boolean b;
        while (true) {
            b = true;
            if (n >= length) {
                break;
            }
            String substring;
            final String s = substring = array[n];
            if (s.charAt(0) == '`') {
                substring = s;
                if (s.charAt(s.length() - 1) == '`') {
                    substring = s.substring(1, s.length() - 1);
                    Intrinsics.checkNotNullExpressionValue((Object)substring, "this as java.lang.String\u2026ing(startIndex, endIndex)");
                }
            }
            final Locale us = Locale.US;
            Intrinsics.checkNotNullExpressionValue((Object)us, "US");
            final String lowerCase = substring.toLowerCase(us);
            Intrinsics.checkNotNullExpressionValue((Object)lowerCase, "this as java.lang.String).toLowerCase(locale)");
            array[n] = lowerCase;
            ++n;
        }
        final Object[] array3 = array2;
        for (int length2 = array3.length, i = 0; i < length2; ++i) {
            for (int length3 = array2[i].length, j = 0; j < length3; ++j) {
                final String[] array4 = array2[i];
                final String s2 = array4[j];
                final Locale us2 = Locale.US;
                Intrinsics.checkNotNullExpressionValue((Object)us2, "US");
                final String lowerCase2 = s2.toLowerCase(us2);
                Intrinsics.checkNotNullExpressionValue((Object)lowerCase2, "this as java.lang.String).toLowerCase(locale)");
                array4[j] = lowerCase2;
            }
        }
        final Set setBuilder = SetsKt.createSetBuilder();
        for (int length4 = array3.length, k = 0; k < length4; ++k) {
            CollectionsKt.addAll((Collection)setBuilder, (Object[])array3[k]);
        }
        final Set build = SetsKt.build(setBuilder);
        final List listBuilder = CollectionsKt.createListBuilder();
        for (int length5 = array.length, l = 0, n2 = 0; l < length5; ++l, ++n2) {
            final String s3 = array[l];
            if (build.contains(s3)) {
                listBuilder.add(new ResultColumn(s3, n2));
            }
        }
        final List build2 = CollectionsKt.build(listBuilder);
        final int length6 = array3.length;
        final ArrayList list = new ArrayList<List>(length6);
        for (int n3 = 0; n3 < length6; ++n3) {
            list.add(new ArrayList());
        }
        final List list2 = list;
        for (int length7 = array3.length, n4 = 0, n5 = 0; n4 < length7; ++n4, ++n5) {
            final String[] array5 = (String[])array3[n4];
            AmbiguousColumnResolver.INSTANCE.rabinKarpSearch(build2, array5, (Function3<? super Integer, ? super Integer, ? super List<ResultColumn>, Unit>)new AmbiguousColumnResolver$resolve$1.AmbiguousColumnResolver$resolve$1$1(array5, list2, n5));
            if (((List)list2.get(n5)).isEmpty()) {
                final Collection collection = new ArrayList(array5.length);
                for (final String str : array5) {
                    final List listBuilder2 = CollectionsKt.createListBuilder();
                    for (final ResultColumn resultColumn : build2) {
                        if (Intrinsics.areEqual((Object)str, (Object)resultColumn.getName())) {
                            listBuilder2.add(resultColumn.getIndex());
                        }
                    }
                    final List build3 = CollectionsKt.build(listBuilder2);
                    if (!(build3.isEmpty() ^ true)) {
                        final StringBuilder sb = new StringBuilder();
                        sb.append("Column ");
                        sb.append(str);
                        sb.append(" not found in result");
                        throw new IllegalStateException(sb.toString().toString());
                    }
                    collection.add(build3);
                }
                dfs$default(AmbiguousColumnResolver.INSTANCE, (List)collection, null, 0, (Function1)new AmbiguousColumnResolver$resolve$1.AmbiguousColumnResolver$resolve$1$2(list2, n5), 6, null);
            }
        }
        final Iterable iterable = list2;
        int n7 = 0;
        Label_0745: {
            if (iterable instanceof Collection && ((Collection)iterable).isEmpty()) {
                n7 = (b ? 1 : 0);
            }
            else {
                final Iterator iterator2 = iterable.iterator();
                do {
                    n7 = (b ? 1 : 0);
                    if (iterator2.hasNext()) {
                        continue;
                    }
                    break Label_0745;
                } while (((Collection)iterator2.next()).isEmpty() ^ true);
                n7 = 0;
            }
        }
        if (n7 != 0) {
            final Ref$ObjectRef ref$ObjectRef = new Ref$ObjectRef();
            ref$ObjectRef.element = Solution.Companion.getNO_SOLUTION();
            dfs$default(AmbiguousColumnResolver.INSTANCE, list2, null, 0, (Function1)new AmbiguousColumnResolver$resolve.AmbiguousColumnResolver$resolve$4(ref$ObjectRef), 6, null);
            final Iterable iterable2 = ((Solution)ref$ObjectRef.element).getMatches();
            final Collection collection2 = new ArrayList(CollectionsKt.collectionSizeOrDefault(iterable2, 10));
            final Iterator iterator3 = iterable2.iterator();
            while (iterator3.hasNext()) {
                collection2.add(CollectionsKt.toIntArray((Collection)((Match)iterator3.next()).getResultIndices()));
            }
            final int[][] array6 = ((Collection)(List<E>)collection2).toArray(new int[0][]);
            Intrinsics.checkNotNull((Object)array6, "null cannot be cast to non-null type kotlin.Array<T of kotlin.collections.ArraysKt__ArraysJVMKt.toTypedArray>");
            return array6;
        }
        throw new IllegalStateException("Failed to find matches for all mappings".toString());
    }
    
    @Metadata(d1 = { "\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0010\b\n\u0002\b\u0006\b\u0002\u0018\u00002\u00020\u0001B\u001b\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005¢\u0006\u0002\u0010\u0007R\u0017\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010\tR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000b¨\u0006\f" }, d2 = { "Landroidx/room/AmbiguousColumnResolver$Match;", "", "resultRange", "Lkotlin/ranges/IntRange;", "resultIndices", "", "", "(Lkotlin/ranges/IntRange;Ljava/util/List;)V", "getResultIndices", "()Ljava/util/List;", "getResultRange", "()Lkotlin/ranges/IntRange;", "room-common" }, k = 1, mv = { 1, 7, 1 }, xi = 48)
    private static final class Match
    {
        private final List<Integer> resultIndices;
        private final IntRange resultRange;
        
        public Match(final IntRange resultRange, final List<Integer> resultIndices) {
            Intrinsics.checkNotNullParameter((Object)resultRange, "resultRange");
            Intrinsics.checkNotNullParameter((Object)resultIndices, "resultIndices");
            this.resultRange = resultRange;
            this.resultIndices = resultIndices;
        }
        
        public final List<Integer> getResultIndices() {
            return this.resultIndices;
        }
        
        public final IntRange getResultRange() {
            return this.resultRange;
        }
    }
    
    @Metadata(d1 = { "\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0002\b\t\n\u0002\u0010\u000b\n\u0002\b\u0004\b\u0082\b\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\t\u0010\u000b\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\f\u001a\u00020\u0005H\u00c6\u0003J\u001d\u0010\r\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u0005H\u00c6\u0001J\u0013\u0010\u000e\u001a\u00020\u000f2\b\u0010\u0010\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u0011\u001a\u00020\u0005H\u00d6\u0001J\t\u0010\u0012\u001a\u00020\u0003H\u00d6\u0001R\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\n¨\u0006\u0013" }, d2 = { "Landroidx/room/AmbiguousColumnResolver$ResultColumn;", "", "name", "", "index", "", "(Ljava/lang/String;I)V", "getIndex", "()I", "getName", "()Ljava/lang/String;", "component1", "component2", "copy", "equals", "", "other", "hashCode", "toString", "room-common" }, k = 1, mv = { 1, 7, 1 }, xi = 48)
    private static final class ResultColumn
    {
        private final int index;
        private final String name;
        
        public ResultColumn(final String name, final int index) {
            Intrinsics.checkNotNullParameter((Object)name, "name");
            this.name = name;
            this.index = index;
        }
        
        public final String component1() {
            return this.name;
        }
        
        public final int component2() {
            return this.index;
        }
        
        public final ResultColumn copy(final String s, final int n) {
            Intrinsics.checkNotNullParameter((Object)s, "name");
            return new ResultColumn(s, n);
        }
        
        @Override
        public boolean equals(final Object o) {
            if (this == o) {
                return true;
            }
            if (!(o instanceof ResultColumn)) {
                return false;
            }
            final ResultColumn resultColumn = (ResultColumn)o;
            return Intrinsics.areEqual((Object)this.name, (Object)resultColumn.name) && this.index == resultColumn.index;
        }
        
        public final int getIndex() {
            return this.index;
        }
        
        public final String getName() {
            return this.name;
        }
        
        @Override
        public int hashCode() {
            return this.name.hashCode() * 31 + this.index;
        }
        
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder();
            sb.append("ResultColumn(name=");
            sb.append(this.name);
            sb.append(", index=");
            sb.append(this.index);
            sb.append(')');
            return sb.toString();
        }
    }
    
    @Metadata(d1 = { "\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u000f\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u000b\b\u0002\u0018\u0000 \u00102\b\u0012\u0004\u0012\u00020\u00000\u0001:\u0001\u0010B#\u0012\f\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0006¢\u0006\u0002\u0010\bJ\u0011\u0010\u000e\u001a\u00020\u00062\u0006\u0010\u000f\u001a\u00020\u0000H\u0096\u0002R\u0011\u0010\u0005\u001a\u00020\u0006¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0017\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0011\u0010\u0007\u001a\u00020\u0006¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\n¨\u0006\u0011" }, d2 = { "Landroidx/room/AmbiguousColumnResolver$Solution;", "", "matches", "", "Landroidx/room/AmbiguousColumnResolver$Match;", "coverageOffset", "", "overlaps", "(Ljava/util/List;II)V", "getCoverageOffset", "()I", "getMatches", "()Ljava/util/List;", "getOverlaps", "compareTo", "other", "Companion", "room-common" }, k = 1, mv = { 1, 7, 1 }, xi = 48)
    private static final class Solution implements Comparable<Solution>
    {
        public static final Companion Companion;
        private static final Solution NO_SOLUTION;
        private final int coverageOffset;
        private final List<Match> matches;
        private final int overlaps;
        
        static {
            Companion = new Companion(null);
            NO_SOLUTION = new Solution(CollectionsKt.emptyList(), Integer.MAX_VALUE, Integer.MAX_VALUE);
        }
        
        public Solution(final List<Match> matches, final int coverageOffset, final int overlaps) {
            Intrinsics.checkNotNullParameter((Object)matches, "matches");
            this.matches = matches;
            this.coverageOffset = coverageOffset;
            this.overlaps = overlaps;
        }
        
        public static final /* synthetic */ Solution access$getNO_SOLUTION$cp() {
            return Solution.NO_SOLUTION;
        }
        
        @Override
        public int compareTo(final Solution solution) {
            Intrinsics.checkNotNullParameter((Object)solution, "other");
            final int compare = Intrinsics.compare(this.overlaps, solution.overlaps);
            if (compare != 0) {
                return compare;
            }
            return Intrinsics.compare(this.coverageOffset, solution.coverageOffset);
        }
        
        public final int getCoverageOffset() {
            return this.coverageOffset;
        }
        
        public final List<Match> getMatches() {
            return this.matches;
        }
        
        public final int getOverlaps() {
            return this.overlaps;
        }
        
        @Metadata(d1 = { "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0014\u0010\u0007\u001a\u00020\u00042\f\u0010\b\u001a\b\u0012\u0004\u0012\u00020\n0\tR\u0011\u0010\u0003\u001a\u00020\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006¨\u0006\u000b" }, d2 = { "Landroidx/room/AmbiguousColumnResolver$Solution$Companion;", "", "()V", "NO_SOLUTION", "Landroidx/room/AmbiguousColumnResolver$Solution;", "getNO_SOLUTION", "()Landroidx/room/AmbiguousColumnResolver$Solution;", "build", "matches", "", "Landroidx/room/AmbiguousColumnResolver$Match;", "room-common" }, k = 1, mv = { 1, 7, 1 }, xi = 48)
        public static final class Companion
        {
            private Companion() {
            }
            
            public final Solution build(final List<Match> list) {
                Intrinsics.checkNotNullParameter((Object)list, "matches");
                final Iterable iterable = list;
                final Iterator iterator = iterable.iterator();
                final int n = 0;
                int n2 = 0;
                while (iterator.hasNext()) {
                    final Match match = (Match)iterator.next();
                    n2 += match.getResultRange().getLast() - match.getResultRange().getFirst() + 1 - match.getResultIndices().size();
                }
                final Iterator iterator2 = iterable.iterator();
                if (!iterator2.hasNext()) {
                    throw new NoSuchElementException();
                }
                int first = ((Match)iterator2.next()).getResultRange().getFirst();
                while (iterator2.hasNext()) {
                    final int first2 = ((Match)iterator2.next()).getResultRange().getFirst();
                    if (first > first2) {
                        first = first2;
                    }
                }
                final Iterator iterator3 = iterable.iterator();
                if (iterator3.hasNext()) {
                    int last = ((Match)iterator3.next()).getResultRange().getLast();
                    while (iterator3.hasNext()) {
                        final int last2 = ((Match)iterator3.next()).getResultRange().getLast();
                        if (last < last2) {
                            last = last2;
                        }
                    }
                    final Iterable iterable2 = (Iterable)new IntRange(first, last);
                    int n3;
                    if (iterable2 instanceof Collection && ((Collection)iterable2).isEmpty()) {
                        n3 = n;
                    }
                    else {
                        final Iterator iterator4 = iterable2.iterator();
                        n3 = 0;
                    Label_0292:
                        while (iterator4.hasNext()) {
                            final int nextInt = ((IntIterator)iterator4).nextInt();
                            final Iterator iterator5 = iterable.iterator();
                            int n4 = 0;
                            while (true) {
                                while (iterator5.hasNext()) {
                                    int n5 = n4;
                                    if (((Match)iterator5.next()).getResultRange().contains(nextInt)) {
                                        n5 = n4 + 1;
                                    }
                                    if ((n4 = n5) > 1) {
                                        final boolean b = true;
                                        if (!b) {
                                            continue Label_0292;
                                        }
                                        final int n6 = n3 + 1;
                                        if ((n3 = n6) < 0) {
                                            CollectionsKt.throwCountOverflow();
                                            n3 = n6;
                                            continue Label_0292;
                                        }
                                        continue Label_0292;
                                    }
                                }
                                final boolean b = false;
                                continue;
                            }
                        }
                    }
                    return new Solution(list, n2, n3);
                }
                throw new NoSuchElementException();
            }
            
            public final Solution getNO_SOLUTION() {
                return Solution.access$getNO_SOLUTION$cp();
            }
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.room;

import java.util.Arrays;
import java.util.Collection;
import java.util.ArrayList;
import kotlin.jvm.internal.Intrinsics;
import java.util.concurrent.Executor;
import java.util.List;
import kotlin.Metadata;
import androidx.sqlite.db.SupportSQLiteStatement;

@Metadata(d1 = { "\u0000J\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010!\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0012\n\u0000\n\u0002\u0010\u0006\n\u0000\n\u0002\u0010\t\n\u0002\b\f\b\u0000\u0018\u00002\u00020\u0001B%\u0012\u0006\u0010\u0002\u001a\u00020\u0001\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\b¢\u0006\u0002\u0010\tJ\u0018\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u0012H\u0016J\u0018\u0010\u0013\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u0014H\u0016J\u0018\u0010\u0015\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u0016H\u0016J\u0010\u0010\u0017\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0010H\u0016J\u0018\u0010\u0018\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u0004H\u0016J\b\u0010\u0019\u001a\u00020\u000eH\u0016J\t\u0010\u001a\u001a\u00020\u000eH\u0096\u0001J\b\u0010\u001b\u001a\u00020\u000eH\u0016J\b\u0010\u001c\u001a\u00020\u0016H\u0016J\b\u0010\u001d\u001a\u00020\u0010H\u0016J\u001a\u0010\u001e\u001a\u00020\u000e2\u0006\u0010\u001f\u001a\u00020\u00102\b\u0010\u0011\u001a\u0004\u0018\u00010\fH\u0002J\b\u0010 \u001a\u00020\u0016H\u0016J\n\u0010!\u001a\u0004\u0018\u00010\u0004H\u0016R\u0016\u0010\n\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\f0\u000bX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0001X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\bX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\"" }, d2 = { "Landroidx/room/QueryInterceptorStatement;", "Landroidx/sqlite/db/SupportSQLiteStatement;", "delegate", "sqlStatement", "", "queryCallbackExecutor", "Ljava/util/concurrent/Executor;", "queryCallback", "Landroidx/room/RoomDatabase$QueryCallback;", "(Landroidx/sqlite/db/SupportSQLiteStatement;Ljava/lang/String;Ljava/util/concurrent/Executor;Landroidx/room/RoomDatabase$QueryCallback;)V", "bindArgsCache", "", "", "bindBlob", "", "index", "", "value", "", "bindDouble", "", "bindLong", "", "bindNull", "bindString", "clearBindings", "close", "execute", "executeInsert", "executeUpdateDelete", "saveArgsToCache", "bindIndex", "simpleQueryForLong", "simpleQueryForString", "room-runtime_release" }, k = 1, mv = { 1, 7, 1 }, xi = 48)
public final class QueryInterceptorStatement implements SupportSQLiteStatement
{
    private final List<Object> bindArgsCache;
    private final SupportSQLiteStatement delegate;
    private final RoomDatabase.QueryCallback queryCallback;
    private final Executor queryCallbackExecutor;
    private final String sqlStatement;
    
    public QueryInterceptorStatement(final SupportSQLiteStatement delegate, final String sqlStatement, final Executor queryCallbackExecutor, final RoomDatabase.QueryCallback queryCallback) {
        Intrinsics.checkNotNullParameter((Object)delegate, "delegate");
        Intrinsics.checkNotNullParameter((Object)sqlStatement, "sqlStatement");
        Intrinsics.checkNotNullParameter((Object)queryCallbackExecutor, "queryCallbackExecutor");
        Intrinsics.checkNotNullParameter((Object)queryCallback, "queryCallback");
        this.delegate = delegate;
        this.sqlStatement = sqlStatement;
        this.queryCallbackExecutor = queryCallbackExecutor;
        this.queryCallback = queryCallback;
        this.bindArgsCache = new ArrayList<Object>();
    }
    
    private static final void execute$lambda$0(final QueryInterceptorStatement queryInterceptorStatement) {
        Intrinsics.checkNotNullParameter((Object)queryInterceptorStatement, "this$0");
        queryInterceptorStatement.queryCallback.onQuery(queryInterceptorStatement.sqlStatement, queryInterceptorStatement.bindArgsCache);
    }
    
    private static final void executeInsert$lambda$2(final QueryInterceptorStatement queryInterceptorStatement) {
        Intrinsics.checkNotNullParameter((Object)queryInterceptorStatement, "this$0");
        queryInterceptorStatement.queryCallback.onQuery(queryInterceptorStatement.sqlStatement, queryInterceptorStatement.bindArgsCache);
    }
    
    private static final void executeUpdateDelete$lambda$1(final QueryInterceptorStatement queryInterceptorStatement) {
        Intrinsics.checkNotNullParameter((Object)queryInterceptorStatement, "this$0");
        queryInterceptorStatement.queryCallback.onQuery(queryInterceptorStatement.sqlStatement, queryInterceptorStatement.bindArgsCache);
    }
    
    private final void saveArgsToCache(int i, final Object o) {
        final int n = i - 1;
        if (n >= this.bindArgsCache.size()) {
            int size;
            for (size = this.bindArgsCache.size(), i = 0; i < n - size + 1; ++i) {
                this.bindArgsCache.add(null);
            }
        }
        this.bindArgsCache.set(n, o);
    }
    
    private static final void simpleQueryForLong$lambda$3(final QueryInterceptorStatement queryInterceptorStatement) {
        Intrinsics.checkNotNullParameter((Object)queryInterceptorStatement, "this$0");
        queryInterceptorStatement.queryCallback.onQuery(queryInterceptorStatement.sqlStatement, queryInterceptorStatement.bindArgsCache);
    }
    
    private static final void simpleQueryForString$lambda$4(final QueryInterceptorStatement queryInterceptorStatement) {
        Intrinsics.checkNotNullParameter((Object)queryInterceptorStatement, "this$0");
        queryInterceptorStatement.queryCallback.onQuery(queryInterceptorStatement.sqlStatement, queryInterceptorStatement.bindArgsCache);
    }
    
    @Override
    public void bindBlob(final int n, final byte[] array) {
        Intrinsics.checkNotNullParameter((Object)array, "value");
        this.saveArgsToCache(n, array);
        this.delegate.bindBlob(n, array);
    }
    
    @Override
    public void bindDouble(final int n, final double d) {
        this.saveArgsToCache(n, d);
        this.delegate.bindDouble(n, d);
    }
    
    @Override
    public void bindLong(final int n, final long l) {
        this.saveArgsToCache(n, l);
        this.delegate.bindLong(n, l);
    }
    
    @Override
    public void bindNull(final int n) {
        final Object[] array = this.bindArgsCache.toArray(new Object[0]);
        Intrinsics.checkNotNull((Object)array, "null cannot be cast to non-null type kotlin.Array<T of kotlin.collections.ArraysKt__ArraysJVMKt.toTypedArray>");
        this.saveArgsToCache(n, Arrays.copyOf(array, array.length));
        this.delegate.bindNull(n);
    }
    
    @Override
    public void bindString(final int n, final String s) {
        Intrinsics.checkNotNullParameter((Object)s, "value");
        this.saveArgsToCache(n, s);
        this.delegate.bindString(n, s);
    }
    
    @Override
    public void clearBindings() {
        this.bindArgsCache.clear();
        this.delegate.clearBindings();
    }
    
    @Override
    public void close() {
        this.delegate.close();
    }
    
    @Override
    public void execute() {
        this.queryCallbackExecutor.execute(new QueryInterceptorStatement$$ExternalSyntheticLambda1(this));
        this.delegate.execute();
    }
    
    @Override
    public long executeInsert() {
        this.queryCallbackExecutor.execute(new QueryInterceptorStatement$$ExternalSyntheticLambda2(this));
        return this.delegate.executeInsert();
    }
    
    @Override
    public int executeUpdateDelete() {
        this.queryCallbackExecutor.execute(new QueryInterceptorStatement$$ExternalSyntheticLambda0(this));
        return this.delegate.executeUpdateDelete();
    }
    
    @Override
    public long simpleQueryForLong() {
        this.queryCallbackExecutor.execute(new QueryInterceptorStatement$$ExternalSyntheticLambda4(this));
        return this.delegate.simpleQueryForLong();
    }
    
    @Override
    public String simpleQueryForString() {
        this.queryCallbackExecutor.execute(new QueryInterceptorStatement$$ExternalSyntheticLambda3(this));
        return this.delegate.simpleQueryForString();
    }
}

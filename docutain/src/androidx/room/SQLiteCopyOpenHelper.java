// 
// Decompiled by Procyon v0.6.0
// 

package androidx.room;

import android.util.Log;
import androidx.sqlite.util.ProcessLock;
import androidx.sqlite.db.SupportSQLiteDatabase;
import kotlin.io.CloseableKt;
import kotlin.Unit;
import java.io.Closeable;
import kotlin.ranges.RangesKt;
import androidx.sqlite.db.framework.FrameworkSQLiteOpenHelperFactory;
import androidx.room.util.DBUtil;
import java.nio.channels.FileChannel;
import java.io.IOException;
import androidx.room.util.FileUtil;
import java.io.FileOutputStream;
import java.nio.channels.ReadableByteChannel;
import java.io.FileInputStream;
import java.nio.channels.Channels;
import kotlin.jvm.internal.Intrinsics;
import java.io.InputStream;
import java.util.concurrent.Callable;
import java.io.File;
import android.content.Context;
import kotlin.Metadata;
import androidx.sqlite.db.SupportSQLiteOpenHelper;

@Metadata(d1 = { "\u0000R\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0002\b\u000b\b\u0000\u0018\u00002\u00020\u00012\u00020\u0002BA\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\b\u0010\u0005\u001a\u0004\u0018\u00010\u0006\u0012\b\u0010\u0007\u001a\u0004\u0018\u00010\b\u0012\u000e\u0010\t\u001a\n\u0012\u0004\u0012\u00020\u000b\u0018\u00010\n\u0012\u0006\u0010\f\u001a\u00020\r\u0012\u0006\u0010\u000e\u001a\u00020\u0001¢\u0006\u0002\u0010\u000fJ\b\u0010\u001f\u001a\u00020 H\u0016J\u0018\u0010!\u001a\u00020 2\u0006\u0010\"\u001a\u00020\b2\u0006\u0010#\u001a\u00020\u001cH\u0002J\u0010\u0010$\u001a\u00020\u00012\u0006\u0010%\u001a\u00020\bH\u0002J\u0018\u0010&\u001a\u00020 2\u0006\u0010%\u001a\u00020\b2\u0006\u0010#\u001a\u00020\u001cH\u0002J\u000e\u0010'\u001a\u00020 2\u0006\u0010\u0010\u001a\u00020\u0011J\u0010\u0010(\u001a\u00020 2\u0006\u0010)\u001a\u00020\u001cH\u0017J\u0010\u0010*\u001a\u00020 2\u0006\u0010#\u001a\u00020\u001cH\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\u0005\u001a\u0004\u0018\u00010\u0006X\u0082\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\u0007\u001a\u0004\u0018\u00010\bX\u0082\u0004¢\u0006\u0002\n\u0000R\u0016\u0010\t\u001a\n\u0012\u0004\u0012\u00020\u000b\u0018\u00010\nX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0011X\u0082.¢\u0006\u0002\n\u0000R\u0016\u0010\u0012\u001a\u0004\u0018\u00010\u00068VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u0013\u0010\u0014R\u000e\u0010\f\u001a\u00020\rX\u0082\u0004¢\u0006\u0002\n\u0000R\u0014\u0010\u000e\u001a\u00020\u0001X\u0096\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0015\u0010\u0016R\u0014\u0010\u0017\u001a\u00020\u00188VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u0019\u0010\u001aR\u000e\u0010\u001b\u001a\u00020\u001cX\u0082\u000e¢\u0006\u0002\n\u0000R\u0014\u0010\u001d\u001a\u00020\u00188VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u001e\u0010\u001a¨\u0006+" }, d2 = { "Landroidx/room/SQLiteCopyOpenHelper;", "Landroidx/sqlite/db/SupportSQLiteOpenHelper;", "Landroidx/room/DelegatingOpenHelper;", "context", "Landroid/content/Context;", "copyFromAssetPath", "", "copyFromFile", "Ljava/io/File;", "copyFromInputStream", "Ljava/util/concurrent/Callable;", "Ljava/io/InputStream;", "databaseVersion", "", "delegate", "(Landroid/content/Context;Ljava/lang/String;Ljava/io/File;Ljava/util/concurrent/Callable;ILandroidx/sqlite/db/SupportSQLiteOpenHelper;)V", "databaseConfiguration", "Landroidx/room/DatabaseConfiguration;", "databaseName", "getDatabaseName", "()Ljava/lang/String;", "getDelegate", "()Landroidx/sqlite/db/SupportSQLiteOpenHelper;", "readableDatabase", "Landroidx/sqlite/db/SupportSQLiteDatabase;", "getReadableDatabase", "()Landroidx/sqlite/db/SupportSQLiteDatabase;", "verified", "", "writableDatabase", "getWritableDatabase", "close", "", "copyDatabaseFile", "destinationFile", "writable", "createFrameworkOpenHelper", "databaseFile", "dispatchOnOpenPrepackagedDatabase", "setDatabaseConfiguration", "setWriteAheadLoggingEnabled", "enabled", "verifyDatabaseFile", "room-runtime_release" }, k = 1, mv = { 1, 7, 1 }, xi = 48)
public final class SQLiteCopyOpenHelper implements SupportSQLiteOpenHelper, DelegatingOpenHelper
{
    private final Context context;
    private final String copyFromAssetPath;
    private final File copyFromFile;
    private final Callable<InputStream> copyFromInputStream;
    private DatabaseConfiguration databaseConfiguration;
    private final int databaseVersion;
    private final SupportSQLiteOpenHelper delegate;
    private boolean verified;
    
    public SQLiteCopyOpenHelper(final Context context, final String copyFromAssetPath, final File copyFromFile, final Callable<InputStream> copyFromInputStream, final int databaseVersion, final SupportSQLiteOpenHelper delegate) {
        Intrinsics.checkNotNullParameter((Object)context, "context");
        Intrinsics.checkNotNullParameter((Object)delegate, "delegate");
        this.context = context;
        this.copyFromAssetPath = copyFromAssetPath;
        this.copyFromFile = copyFromFile;
        this.copyFromInputStream = copyFromInputStream;
        this.databaseVersion = databaseVersion;
        this.delegate = delegate;
    }
    
    private final void copyDatabaseFile(final File dest, final boolean b) throws IOException {
        Label_0100: {
            if (this.copyFromAssetPath != null) {
                final ReadableByteChannel readableByteChannel = Channels.newChannel(this.context.getAssets().open(this.copyFromAssetPath));
                Intrinsics.checkNotNullExpressionValue((Object)readableByteChannel, "newChannel(context.assets.open(copyFromAssetPath))");
                break Label_0100;
            }
            if (this.copyFromFile != null) {
                final FileChannel channel = new FileInputStream(this.copyFromFile).getChannel();
                Intrinsics.checkNotNullExpressionValue((Object)channel, "FileInputStream(copyFromFile).channel");
                final ReadableByteChannel readableByteChannel = channel;
                break Label_0100;
            }
            final Callable<InputStream> copyFromInputStream = this.copyFromInputStream;
            if (copyFromInputStream == null) {
                throw new IllegalStateException("copyFromAssetPath, copyFromFile and copyFromInputStream are all null!");
            }
            try {
                final ReadableByteChannel readableByteChannel = Channels.newChannel(copyFromInputStream.call());
                Intrinsics.checkNotNullExpressionValue((Object)readableByteChannel, "newChannel(inputStream)");
                final File tempFile = File.createTempFile("room-copy-helper", ".tmp", this.context.getCacheDir());
                tempFile.deleteOnExit();
                final FileChannel channel2 = new FileOutputStream(tempFile).getChannel();
                Intrinsics.checkNotNullExpressionValue((Object)channel2, "output");
                FileUtil.copy(readableByteChannel, channel2);
                final File parentFile = dest.getParentFile();
                if (parentFile != null && !parentFile.exists() && !parentFile.mkdirs()) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Failed to create directories for ");
                    sb.append(dest.getAbsolutePath());
                    throw new IOException(sb.toString());
                }
                Intrinsics.checkNotNullExpressionValue((Object)tempFile, "intermediateFile");
                this.dispatchOnOpenPrepackagedDatabase(tempFile, b);
                if (tempFile.renameTo(dest)) {
                    return;
                }
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("Failed to move intermediate file (");
                sb2.append(tempFile.getAbsolutePath());
                sb2.append(") to destination (");
                sb2.append(dest.getAbsolutePath());
                sb2.append(").");
                throw new IOException(sb2.toString());
            }
            catch (final Exception ex) {
                throw new IOException("inputStreamCallable exception on call", ex);
            }
        }
        throw new IllegalStateException("copyFromAssetPath, copyFromFile and copyFromInputStream are all null!");
    }
    
    private final SupportSQLiteOpenHelper createFrameworkOpenHelper(final File file) {
        try {
            final int version = DBUtil.readVersion(file);
            return new FrameworkSQLiteOpenHelperFactory().create(Configuration.Companion.builder(this.context).name(file.getAbsolutePath()).callback((Callback)new SQLiteCopyOpenHelper$createFrameworkOpenHelper$configuration.SQLiteCopyOpenHelper$createFrameworkOpenHelper$configuration$1(version, RangesKt.coerceAtLeast(version, 1))).build());
        }
        catch (final IOException ex) {
            throw new RuntimeException("Malformed database file, unable to read version.", ex);
        }
    }
    
    private final void dispatchOnOpenPrepackagedDatabase(final File file, final boolean b) {
        DatabaseConfiguration databaseConfiguration;
        if ((databaseConfiguration = this.databaseConfiguration) == null) {
            Intrinsics.throwUninitializedPropertyAccessException("databaseConfiguration");
            databaseConfiguration = null;
        }
        if (databaseConfiguration.prepackagedDatabaseCallback == null) {
            return;
        }
        final Closeable closeable = this.createFrameworkOpenHelper(file);
        try {
            final SupportSQLiteOpenHelper supportSQLiteOpenHelper = (SupportSQLiteOpenHelper)closeable;
            SupportSQLiteDatabase supportSQLiteDatabase;
            if (b) {
                supportSQLiteDatabase = supportSQLiteOpenHelper.getWritableDatabase();
            }
            else {
                supportSQLiteDatabase = supportSQLiteOpenHelper.getReadableDatabase();
            }
            DatabaseConfiguration databaseConfiguration2;
            if ((databaseConfiguration2 = this.databaseConfiguration) == null) {
                Intrinsics.throwUninitializedPropertyAccessException("databaseConfiguration");
                databaseConfiguration2 = null;
            }
            final RoomDatabase.PrepackagedDatabaseCallback prepackagedDatabaseCallback = databaseConfiguration2.prepackagedDatabaseCallback;
            Intrinsics.checkNotNull((Object)prepackagedDatabaseCallback);
            prepackagedDatabaseCallback.onOpenPrepackagedDatabase(supportSQLiteDatabase);
            final Unit instance = Unit.INSTANCE;
            CloseableKt.closeFinally(closeable, (Throwable)null);
        }
        finally {
            try {}
            finally {
                final Throwable t;
                CloseableKt.closeFinally(closeable, t);
            }
        }
    }
    
    private final void verifyDatabaseFile(final boolean b) {
        final String databaseName = this.getDatabaseName();
        if (databaseName != null) {
            final File databasePath = this.context.getDatabasePath(databaseName);
            final DatabaseConfiguration databaseConfiguration = this.databaseConfiguration;
            final DatabaseConfiguration databaseConfiguration2 = null;
            DatabaseConfiguration databaseConfiguration3;
            if ((databaseConfiguration3 = databaseConfiguration) == null) {
                Intrinsics.throwUninitializedPropertyAccessException("databaseConfiguration");
                databaseConfiguration3 = null;
            }
            final ProcessLock processLock = new ProcessLock(databaseName, this.context.getFilesDir(), databaseConfiguration3.multiInstanceInvalidation);
            try {
                ProcessLock.lock$default(processLock, false, 1, null);
                if (!databasePath.exists()) {
                    try {
                        Intrinsics.checkNotNullExpressionValue((Object)databasePath, "databaseFile");
                        this.copyDatabaseFile(databasePath, b);
                        return;
                    }
                    catch (final IOException ex) {
                        throw new RuntimeException("Unable to copy database file.", ex);
                    }
                }
                try {
                    Intrinsics.checkNotNullExpressionValue((Object)databasePath, "databaseFile");
                    final int version = DBUtil.readVersion(databasePath);
                    if (version == this.databaseVersion) {
                        return;
                    }
                    DatabaseConfiguration databaseConfiguration4 = this.databaseConfiguration;
                    if (databaseConfiguration4 == null) {
                        Intrinsics.throwUninitializedPropertyAccessException("databaseConfiguration");
                        databaseConfiguration4 = databaseConfiguration2;
                    }
                    if (databaseConfiguration4.isMigrationRequired(version, this.databaseVersion)) {
                        return;
                    }
                    if (this.context.deleteDatabase(databaseName)) {
                        try {
                            this.copyDatabaseFile(databasePath, b);
                        }
                        catch (final IOException ex2) {
                            Log.w("ROOM", "Unable to copy database file.", (Throwable)ex2);
                        }
                    }
                    else {
                        final StringBuilder sb = new StringBuilder();
                        sb.append("Failed to delete database file (");
                        sb.append(databaseName);
                        sb.append(") for a copy destructive migration.");
                        Log.w("ROOM", sb.toString());
                    }
                    return;
                }
                catch (final IOException ex3) {
                    Log.w("ROOM", "Unable to read database version.", (Throwable)ex3);
                    return;
                }
            }
            finally {
                processLock.unlock();
            }
        }
        throw new IllegalStateException("Required value was null.".toString());
    }
    
    @Override
    public void close() {
        synchronized (this) {
            this.getDelegate().close();
            this.verified = false;
        }
    }
    
    @Override
    public String getDatabaseName() {
        return this.getDelegate().getDatabaseName();
    }
    
    @Override
    public SupportSQLiteOpenHelper getDelegate() {
        return this.delegate;
    }
    
    @Override
    public SupportSQLiteDatabase getReadableDatabase() {
        if (!this.verified) {
            this.verifyDatabaseFile(false);
            this.verified = true;
        }
        return this.getDelegate().getReadableDatabase();
    }
    
    @Override
    public SupportSQLiteDatabase getWritableDatabase() {
        if (!this.verified) {
            this.verifyDatabaseFile(true);
            this.verified = true;
        }
        return this.getDelegate().getWritableDatabase();
    }
    
    public final void setDatabaseConfiguration(final DatabaseConfiguration databaseConfiguration) {
        Intrinsics.checkNotNullParameter((Object)databaseConfiguration, "databaseConfiguration");
        this.databaseConfiguration = databaseConfiguration;
    }
    
    @Override
    public void setWriteAheadLoggingEnabled(final boolean writeAheadLoggingEnabled) {
        this.getDelegate().setWriteAheadLoggingEnabled(writeAheadLoggingEnabled);
    }
}

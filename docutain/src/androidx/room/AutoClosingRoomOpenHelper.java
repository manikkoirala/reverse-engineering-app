// 
// Decompiled by Procyon v0.6.0
// 

package androidx.room;

import android.content.ContentResolver;
import android.database.DataSetObserver;
import android.database.ContentObserver;
import androidx.sqlite.db.SupportSQLiteCompat;
import android.net.Uri;
import android.os.Bundle;
import kotlin.Deprecated;
import android.database.CharArrayBuffer;
import java.util.Iterator;
import kotlin.collections.CollectionsKt;
import java.util.ArrayList;
import kotlin.Unit;
import java.util.Locale;
import android.os.CancellationSignal;
import android.database.Cursor;
import androidx.sqlite.db.SupportSQLiteQuery;
import android.content.ContentValues;
import android.util.Pair;
import java.util.List;
import android.database.SQLException;
import androidx.sqlite.db.SupportSQLiteDatabase$_CC;
import kotlin.jvm.functions.Function1;
import androidx.sqlite.db.SupportSQLiteStatement;
import java.io.IOException;
import android.database.sqlite.SQLiteTransactionListener;
import androidx.sqlite.db.SupportSQLiteDatabase;
import kotlin.jvm.internal.Intrinsics;
import kotlin.Metadata;
import androidx.sqlite.db.SupportSQLiteOpenHelper;

@Metadata(d1 = { "\u0000>\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0004\b\u0000\u0018\u00002\u00020\u00012\u00020\u0002:\u0003\u001a\u001b\u001cB\u0015\u0012\u0006\u0010\u0003\u001a\u00020\u0001\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\b\u0010\u0015\u001a\u00020\u0016H\u0016J\u0011\u0010\u0017\u001a\u00020\u00162\u0006\u0010\u0018\u001a\u00020\u0019H\u0097\u0001R\u0010\u0010\u0004\u001a\u00020\u00058\u0000X\u0081\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\bX\u0082\u0004¢\u0006\u0002\n\u0000R\u0014\u0010\t\u001a\u0004\u0018\u00010\nX\u0096\u0005¢\u0006\u0006\u001a\u0004\b\u000b\u0010\fR\u0014\u0010\u0003\u001a\u00020\u0001X\u0096\u0004¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000eR\u0014\u0010\u000f\u001a\u00020\u00108WX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u0011\u0010\u0012R\u0014\u0010\u0013\u001a\u00020\u00108WX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u0014\u0010\u0012¨\u0006\u001d" }, d2 = { "Landroidx/room/AutoClosingRoomOpenHelper;", "Landroidx/sqlite/db/SupportSQLiteOpenHelper;", "Landroidx/room/DelegatingOpenHelper;", "delegate", "autoCloser", "Landroidx/room/AutoCloser;", "(Landroidx/sqlite/db/SupportSQLiteOpenHelper;Landroidx/room/AutoCloser;)V", "autoClosingDb", "Landroidx/room/AutoClosingRoomOpenHelper$AutoClosingSupportSQLiteDatabase;", "databaseName", "", "getDatabaseName", "()Ljava/lang/String;", "getDelegate", "()Landroidx/sqlite/db/SupportSQLiteOpenHelper;", "readableDatabase", "Landroidx/sqlite/db/SupportSQLiteDatabase;", "getReadableDatabase", "()Landroidx/sqlite/db/SupportSQLiteDatabase;", "writableDatabase", "getWritableDatabase", "close", "", "setWriteAheadLoggingEnabled", "enabled", "", "AutoClosingSupportSQLiteDatabase", "AutoClosingSupportSqliteStatement", "KeepAliveCursor", "room-runtime_release" }, k = 1, mv = { 1, 7, 1 }, xi = 48)
public final class AutoClosingRoomOpenHelper implements SupportSQLiteOpenHelper, DelegatingOpenHelper
{
    public final AutoCloser autoCloser;
    private final AutoClosingSupportSQLiteDatabase autoClosingDb;
    private final SupportSQLiteOpenHelper delegate;
    
    public AutoClosingRoomOpenHelper(final SupportSQLiteOpenHelper delegate, final AutoCloser autoCloser) {
        Intrinsics.checkNotNullParameter((Object)delegate, "delegate");
        Intrinsics.checkNotNullParameter((Object)autoCloser, "autoCloser");
        this.delegate = delegate;
        (this.autoCloser = autoCloser).init(this.getDelegate());
        this.autoClosingDb = new AutoClosingSupportSQLiteDatabase(autoCloser);
    }
    
    @Override
    public void close() {
        this.autoClosingDb.close();
    }
    
    @Override
    public String getDatabaseName() {
        return this.delegate.getDatabaseName();
    }
    
    @Override
    public SupportSQLiteOpenHelper getDelegate() {
        return this.delegate;
    }
    
    @Override
    public SupportSQLiteDatabase getReadableDatabase() {
        this.autoClosingDb.pokeOpen();
        return this.autoClosingDb;
    }
    
    @Override
    public SupportSQLiteDatabase getWritableDatabase() {
        this.autoClosingDb.pokeOpen();
        return this.autoClosingDb;
    }
    
    @Override
    public void setWriteAheadLoggingEnabled(final boolean writeAheadLoggingEnabled) {
        this.delegate.setWriteAheadLoggingEnabled(writeAheadLoggingEnabled);
    }
    
    @Metadata(d1 = { "\u0000\u0080\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0006\n\u0002\u0010\t\n\u0002\b\u000b\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0011\n\u0002\u0010\u0000\n\u0002\b\u000b\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\t\b\u0000\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\b\u0010$\u001a\u00020%H\u0016J\b\u0010&\u001a\u00020%H\u0016J\u0010\u0010'\u001a\u00020%2\u0006\u0010(\u001a\u00020)H\u0016J\u0010\u0010*\u001a\u00020%2\u0006\u0010(\u001a\u00020)H\u0016J\b\u0010+\u001a\u00020%H\u0016J\u0010\u0010,\u001a\u00020-2\u0006\u0010.\u001a\u00020\bH\u0016J3\u0010/\u001a\u00020\u001f2\u0006\u00100\u001a\u00020\b2\b\u00101\u001a\u0004\u0018\u00010\b2\u0012\u00102\u001a\u000e\u0012\b\b\u0001\u0012\u0004\u0018\u000104\u0018\u000103H\u0016¢\u0006\u0002\u00105J\b\u00106\u001a\u00020%H\u0016J\b\u00107\u001a\u00020\fH\u0016J\b\u00108\u001a\u00020%H\u0016J\u0010\u00109\u001a\u00020%2\u0006\u0010.\u001a\u00020\bH\u0016J'\u00109\u001a\u00020%2\u0006\u0010.\u001a\u00020\b2\u0010\u0010:\u001a\f\u0012\b\b\u0001\u0012\u0004\u0018\u00010403H\u0016¢\u0006\u0002\u0010;J\b\u0010<\u001a\u00020\fH\u0016J \u0010=\u001a\u00020\u00132\u0006\u00100\u001a\u00020\b2\u0006\u0010>\u001a\u00020\u001f2\u0006\u0010?\u001a\u00020@H\u0016J\u0010\u0010A\u001a\u00020\f2\u0006\u0010B\u001a\u00020\u001fH\u0016J\u0006\u0010C\u001a\u00020%J\u0010\u0010D\u001a\u00020E2\u0006\u0010D\u001a\u00020FH\u0016J\u001a\u0010D\u001a\u00020E2\u0006\u0010D\u001a\u00020F2\b\u0010G\u001a\u0004\u0018\u00010HH\u0017J\u0010\u0010D\u001a\u00020E2\u0006\u0010D\u001a\u00020\bH\u0016J'\u0010D\u001a\u00020E2\u0006\u0010D\u001a\u00020\b2\u0010\u0010:\u001a\f\u0012\b\b\u0001\u0012\u0004\u0018\u00010403H\u0016¢\u0006\u0002\u0010IJ\u0010\u0010J\u001a\u00020%2\u0006\u0010K\u001a\u00020\fH\u0017J\u0010\u0010L\u001a\u00020%2\u0006\u0010M\u001a\u00020NH\u0016J\u0010\u0010O\u001a\u00020%2\u0006\u0010P\u001a\u00020\u001fH\u0016J\u0010\u0010Q\u001a\u00020\u00132\u0006\u0010\u0016\u001a\u00020\u0013H\u0016J\b\u0010R\u001a\u00020%H\u0016JC\u0010S\u001a\u00020\u001f2\u0006\u00100\u001a\u00020\b2\u0006\u0010>\u001a\u00020\u001f2\u0006\u0010?\u001a\u00020@2\b\u00101\u001a\u0004\u0018\u00010\b2\u0012\u00102\u001a\u000e\u0012\b\b\u0001\u0012\u0004\u0018\u000104\u0018\u000103H\u0016¢\u0006\u0002\u0010TJ\b\u0010U\u001a\u00020\fH\u0016J\u0010\u0010U\u001a\u00020\f2\u0006\u0010V\u001a\u00020\u0013H\u0016R(\u0010\u0005\u001a\u0016\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\b\u0012\u0004\u0012\u00020\b0\u0007\u0018\u00010\u00068VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\t\u0010\nR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004¢\u0006\u0002\n\u0000R\u0014\u0010\u000b\u001a\u00020\f8VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u000b\u0010\rR\u0014\u0010\u000e\u001a\u00020\f8VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u000e\u0010\rR\u0014\u0010\u000f\u001a\u00020\f8VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u000f\u0010\rR\u0014\u0010\u0010\u001a\u00020\f8VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u0010\u0010\rR\u0014\u0010\u0011\u001a\u00020\f8WX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u0011\u0010\rR\u0014\u0010\u0012\u001a\u00020\u00138VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u0014\u0010\u0015R$\u0010\u0017\u001a\u00020\u00132\u0006\u0010\u0016\u001a\u00020\u00138V@VX\u0096\u000e¢\u0006\f\u001a\u0004\b\u0018\u0010\u0015\"\u0004\b\u0019\u0010\u001aR\u0016\u0010\u001b\u001a\u0004\u0018\u00010\b8VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u001c\u0010\u001dR$\u0010\u001e\u001a\u00020\u001f2\u0006\u0010\u001e\u001a\u00020\u001f8V@VX\u0096\u000e¢\u0006\f\u001a\u0004\b \u0010!\"\u0004\b\"\u0010#¨\u0006W" }, d2 = { "Landroidx/room/AutoClosingRoomOpenHelper$AutoClosingSupportSQLiteDatabase;", "Landroidx/sqlite/db/SupportSQLiteDatabase;", "autoCloser", "Landroidx/room/AutoCloser;", "(Landroidx/room/AutoCloser;)V", "attachedDbs", "", "Landroid/util/Pair;", "", "getAttachedDbs", "()Ljava/util/List;", "isDatabaseIntegrityOk", "", "()Z", "isDbLockedByCurrentThread", "isOpen", "isReadOnly", "isWriteAheadLoggingEnabled", "maximumSize", "", "getMaximumSize", "()J", "numBytes", "pageSize", "getPageSize", "setPageSize", "(J)V", "path", "getPath", "()Ljava/lang/String;", "version", "", "getVersion", "()I", "setVersion", "(I)V", "beginTransaction", "", "beginTransactionNonExclusive", "beginTransactionWithListener", "transactionListener", "Landroid/database/sqlite/SQLiteTransactionListener;", "beginTransactionWithListenerNonExclusive", "close", "compileStatement", "Landroidx/sqlite/db/SupportSQLiteStatement;", "sql", "delete", "table", "whereClause", "whereArgs", "", "", "(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I", "disableWriteAheadLogging", "enableWriteAheadLogging", "endTransaction", "execSQL", "bindArgs", "(Ljava/lang/String;[Ljava/lang/Object;)V", "inTransaction", "insert", "conflictAlgorithm", "values", "Landroid/content/ContentValues;", "needUpgrade", "newVersion", "pokeOpen", "query", "Landroid/database/Cursor;", "Landroidx/sqlite/db/SupportSQLiteQuery;", "cancellationSignal", "Landroid/os/CancellationSignal;", "(Ljava/lang/String;[Ljava/lang/Object;)Landroid/database/Cursor;", "setForeignKeyConstraintsEnabled", "enabled", "setLocale", "locale", "Ljava/util/Locale;", "setMaxSqlCacheSize", "cacheSize", "setMaximumSize", "setTransactionSuccessful", "update", "(Ljava/lang/String;ILandroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/Object;)I", "yieldIfContendedSafely", "sleepAfterYieldDelayMillis", "room-runtime_release" }, k = 1, mv = { 1, 7, 1 }, xi = 48)
    public static final class AutoClosingSupportSQLiteDatabase implements SupportSQLiteDatabase
    {
        private final AutoCloser autoCloser;
        
        public AutoClosingSupportSQLiteDatabase(final AutoCloser autoCloser) {
            Intrinsics.checkNotNullParameter((Object)autoCloser, "autoCloser");
            this.autoCloser = autoCloser;
        }
        
        @Override
        public void beginTransaction() {
            final SupportSQLiteDatabase incrementCountAndEnsureDbIsOpen = this.autoCloser.incrementCountAndEnsureDbIsOpen();
            try {
                incrementCountAndEnsureDbIsOpen.beginTransaction();
            }
            finally {
                this.autoCloser.decrementCountAndScheduleClose();
            }
        }
        
        @Override
        public void beginTransactionNonExclusive() {
            final SupportSQLiteDatabase incrementCountAndEnsureDbIsOpen = this.autoCloser.incrementCountAndEnsureDbIsOpen();
            try {
                incrementCountAndEnsureDbIsOpen.beginTransactionNonExclusive();
            }
            finally {
                this.autoCloser.decrementCountAndScheduleClose();
            }
        }
        
        @Override
        public void beginTransactionWithListener(final SQLiteTransactionListener sqLiteTransactionListener) {
            Intrinsics.checkNotNullParameter((Object)sqLiteTransactionListener, "transactionListener");
            final SupportSQLiteDatabase incrementCountAndEnsureDbIsOpen = this.autoCloser.incrementCountAndEnsureDbIsOpen();
            try {
                incrementCountAndEnsureDbIsOpen.beginTransactionWithListener(sqLiteTransactionListener);
            }
            finally {
                this.autoCloser.decrementCountAndScheduleClose();
            }
        }
        
        @Override
        public void beginTransactionWithListenerNonExclusive(final SQLiteTransactionListener sqLiteTransactionListener) {
            Intrinsics.checkNotNullParameter((Object)sqLiteTransactionListener, "transactionListener");
            final SupportSQLiteDatabase incrementCountAndEnsureDbIsOpen = this.autoCloser.incrementCountAndEnsureDbIsOpen();
            try {
                incrementCountAndEnsureDbIsOpen.beginTransactionWithListenerNonExclusive(sqLiteTransactionListener);
            }
            finally {
                this.autoCloser.decrementCountAndScheduleClose();
            }
        }
        
        @Override
        public void close() throws IOException {
            this.autoCloser.closeDatabaseIfOpen();
        }
        
        @Override
        public SupportSQLiteStatement compileStatement(final String s) {
            Intrinsics.checkNotNullParameter((Object)s, "sql");
            return new AutoClosingSupportSqliteStatement(s, this.autoCloser);
        }
        
        @Override
        public int delete(final String s, final String s2, final Object[] array) {
            Intrinsics.checkNotNullParameter((Object)s, "table");
            return this.autoCloser.executeRefCountingFunction((kotlin.jvm.functions.Function1<? super SupportSQLiteDatabase, ? extends Number>)new AutoClosingRoomOpenHelper$AutoClosingSupportSQLiteDatabase$delete.AutoClosingRoomOpenHelper$AutoClosingSupportSQLiteDatabase$delete$1(s, s2, array)).intValue();
        }
        
        @Override
        public void disableWriteAheadLogging() {
            throw new UnsupportedOperationException("Enable/disable write ahead logging on the OpenHelper instead of on the database directly.");
        }
        
        @Override
        public boolean enableWriteAheadLogging() {
            throw new UnsupportedOperationException("Enable/disable write ahead logging on the OpenHelper instead of on the database directly.");
        }
        
        @Override
        public void endTransaction() {
            if (this.autoCloser.getDelegateDatabase$room_runtime_release() != null) {
                try {
                    final SupportSQLiteDatabase delegateDatabase$room_runtime_release = this.autoCloser.getDelegateDatabase$room_runtime_release();
                    Intrinsics.checkNotNull((Object)delegateDatabase$room_runtime_release);
                    delegateDatabase$room_runtime_release.endTransaction();
                    return;
                }
                finally {
                    this.autoCloser.decrementCountAndScheduleClose();
                }
            }
            throw new IllegalStateException("End transaction called but delegateDb is null".toString());
        }
        
        @Override
        public void execSQL(final String s) throws SQLException {
            Intrinsics.checkNotNullParameter((Object)s, "sql");
            this.autoCloser.executeRefCountingFunction((kotlin.jvm.functions.Function1<? super SupportSQLiteDatabase, ?>)new AutoClosingRoomOpenHelper$AutoClosingSupportSQLiteDatabase$execSQL.AutoClosingRoomOpenHelper$AutoClosingSupportSQLiteDatabase$execSQL$1(s));
        }
        
        @Override
        public void execSQL(final String s, final Object[] array) throws SQLException {
            Intrinsics.checkNotNullParameter((Object)s, "sql");
            Intrinsics.checkNotNullParameter((Object)array, "bindArgs");
            this.autoCloser.executeRefCountingFunction((kotlin.jvm.functions.Function1<? super SupportSQLiteDatabase, ?>)new AutoClosingRoomOpenHelper$AutoClosingSupportSQLiteDatabase$execSQL.AutoClosingRoomOpenHelper$AutoClosingSupportSQLiteDatabase$execSQL$2(s, array));
        }
        
        @Override
        public List<Pair<String, String>> getAttachedDbs() {
            return this.autoCloser.executeRefCountingFunction((kotlin.jvm.functions.Function1<? super SupportSQLiteDatabase, ? extends List<Pair<String, String>>>)AutoClosingRoomOpenHelper$AutoClosingSupportSQLiteDatabase$attachedDbs.AutoClosingRoomOpenHelper$AutoClosingSupportSQLiteDatabase$attachedDbs$1.INSTANCE);
        }
        
        @Override
        public long getMaximumSize() {
            return this.autoCloser.executeRefCountingFunction((kotlin.jvm.functions.Function1<? super SupportSQLiteDatabase, ? extends Number>)AutoClosingRoomOpenHelper$AutoClosingSupportSQLiteDatabase$maximumSize.AutoClosingRoomOpenHelper$AutoClosingSupportSQLiteDatabase$maximumSize$1.INSTANCE).longValue();
        }
        
        @Override
        public long getPageSize() {
            return this.autoCloser.executeRefCountingFunction((kotlin.jvm.functions.Function1<? super SupportSQLiteDatabase, ? extends Number>)AutoClosingRoomOpenHelper$AutoClosingSupportSQLiteDatabase$pageSize.AutoClosingRoomOpenHelper$AutoClosingSupportSQLiteDatabase$pageSize$1.INSTANCE).longValue();
        }
        
        @Override
        public String getPath() {
            return this.autoCloser.executeRefCountingFunction((kotlin.jvm.functions.Function1<? super SupportSQLiteDatabase, ? extends String>)AutoClosingRoomOpenHelper$AutoClosingSupportSQLiteDatabase$path.AutoClosingRoomOpenHelper$AutoClosingSupportSQLiteDatabase$path$1.INSTANCE);
        }
        
        @Override
        public int getVersion() {
            return this.autoCloser.executeRefCountingFunction((kotlin.jvm.functions.Function1<? super SupportSQLiteDatabase, ? extends Number>)AutoClosingRoomOpenHelper$AutoClosingSupportSQLiteDatabase$version.AutoClosingRoomOpenHelper$AutoClosingSupportSQLiteDatabase$version$1.INSTANCE).intValue();
        }
        
        @Override
        public boolean inTransaction() {
            return this.autoCloser.getDelegateDatabase$room_runtime_release() != null && this.autoCloser.executeRefCountingFunction((kotlin.jvm.functions.Function1<? super SupportSQLiteDatabase, ? extends Boolean>)AutoClosingRoomOpenHelper$AutoClosingSupportSQLiteDatabase$inTransaction.AutoClosingRoomOpenHelper$AutoClosingSupportSQLiteDatabase$inTransaction$1.INSTANCE);
        }
        
        @Override
        public long insert(final String s, final int n, final ContentValues contentValues) throws SQLException {
            Intrinsics.checkNotNullParameter((Object)s, "table");
            Intrinsics.checkNotNullParameter((Object)contentValues, "values");
            return this.autoCloser.executeRefCountingFunction((kotlin.jvm.functions.Function1<? super SupportSQLiteDatabase, ? extends Number>)new AutoClosingRoomOpenHelper$AutoClosingSupportSQLiteDatabase$insert.AutoClosingRoomOpenHelper$AutoClosingSupportSQLiteDatabase$insert$1(s, n, contentValues)).longValue();
        }
        
        @Override
        public boolean isDatabaseIntegrityOk() {
            return this.autoCloser.executeRefCountingFunction((kotlin.jvm.functions.Function1<? super SupportSQLiteDatabase, ? extends Boolean>)AutoClosingRoomOpenHelper$AutoClosingSupportSQLiteDatabase$isDatabaseIntegrityOk.AutoClosingRoomOpenHelper$AutoClosingSupportSQLiteDatabase$isDatabaseIntegrityOk$1.INSTANCE);
        }
        
        @Override
        public boolean isDbLockedByCurrentThread() {
            return this.autoCloser.getDelegateDatabase$room_runtime_release() != null && this.autoCloser.executeRefCountingFunction((kotlin.jvm.functions.Function1<? super SupportSQLiteDatabase, ? extends Boolean>)AutoClosingRoomOpenHelper$AutoClosingSupportSQLiteDatabase$isDbLockedByCurrentThread.AutoClosingRoomOpenHelper$AutoClosingSupportSQLiteDatabase$isDbLockedByCurrentThread$1.INSTANCE);
        }
        
        @Override
        public boolean isOpen() {
            final SupportSQLiteDatabase delegateDatabase$room_runtime_release = this.autoCloser.getDelegateDatabase$room_runtime_release();
            return delegateDatabase$room_runtime_release != null && delegateDatabase$room_runtime_release.isOpen();
        }
        
        @Override
        public boolean isReadOnly() {
            return this.autoCloser.executeRefCountingFunction((kotlin.jvm.functions.Function1<? super SupportSQLiteDatabase, ? extends Boolean>)AutoClosingRoomOpenHelper$AutoClosingSupportSQLiteDatabase$isReadOnly.AutoClosingRoomOpenHelper$AutoClosingSupportSQLiteDatabase$isReadOnly$1.INSTANCE);
        }
        
        @Override
        public boolean isWriteAheadLoggingEnabled() {
            return this.autoCloser.executeRefCountingFunction((kotlin.jvm.functions.Function1<? super SupportSQLiteDatabase, ? extends Boolean>)AutoClosingRoomOpenHelper$AutoClosingSupportSQLiteDatabase$isWriteAheadLoggingEnabled.AutoClosingRoomOpenHelper$AutoClosingSupportSQLiteDatabase$isWriteAheadLoggingEnabled$1.INSTANCE);
        }
        
        @Override
        public boolean needUpgrade(final int n) {
            return this.autoCloser.executeRefCountingFunction((kotlin.jvm.functions.Function1<? super SupportSQLiteDatabase, ? extends Boolean>)new AutoClosingRoomOpenHelper$AutoClosingSupportSQLiteDatabase$needUpgrade.AutoClosingRoomOpenHelper$AutoClosingSupportSQLiteDatabase$needUpgrade$1(n));
        }
        
        public final void pokeOpen() {
            this.autoCloser.executeRefCountingFunction((kotlin.jvm.functions.Function1<? super SupportSQLiteDatabase, ?>)AutoClosingRoomOpenHelper$AutoClosingSupportSQLiteDatabase$pokeOpen.AutoClosingRoomOpenHelper$AutoClosingSupportSQLiteDatabase$pokeOpen$1.INSTANCE);
        }
        
        @Override
        public Cursor query(final SupportSQLiteQuery supportSQLiteQuery) {
            Intrinsics.checkNotNullParameter((Object)supportSQLiteQuery, "query");
            try {
                return (Cursor)new KeepAliveCursor(this.autoCloser.incrementCountAndEnsureDbIsOpen().query(supportSQLiteQuery), this.autoCloser);
            }
            finally {
                this.autoCloser.decrementCountAndScheduleClose();
            }
        }
        
        @Override
        public Cursor query(final SupportSQLiteQuery supportSQLiteQuery, final CancellationSignal cancellationSignal) {
            Intrinsics.checkNotNullParameter((Object)supportSQLiteQuery, "query");
            try {
                return (Cursor)new KeepAliveCursor(this.autoCloser.incrementCountAndEnsureDbIsOpen().query(supportSQLiteQuery, cancellationSignal), this.autoCloser);
            }
            finally {
                this.autoCloser.decrementCountAndScheduleClose();
            }
        }
        
        @Override
        public Cursor query(final String s) {
            Intrinsics.checkNotNullParameter((Object)s, "query");
            try {
                return (Cursor)new KeepAliveCursor(this.autoCloser.incrementCountAndEnsureDbIsOpen().query(s), this.autoCloser);
            }
            finally {
                this.autoCloser.decrementCountAndScheduleClose();
            }
        }
        
        @Override
        public Cursor query(final String s, final Object[] array) {
            Intrinsics.checkNotNullParameter((Object)s, "query");
            Intrinsics.checkNotNullParameter((Object)array, "bindArgs");
            try {
                return (Cursor)new KeepAliveCursor(this.autoCloser.incrementCountAndEnsureDbIsOpen().query(s, array), this.autoCloser);
            }
            finally {
                this.autoCloser.decrementCountAndScheduleClose();
            }
        }
        
        @Override
        public void setForeignKeyConstraintsEnabled(final boolean b) {
            this.autoCloser.executeRefCountingFunction((kotlin.jvm.functions.Function1<? super SupportSQLiteDatabase, ?>)new AutoClosingRoomOpenHelper$AutoClosingSupportSQLiteDatabase$setForeignKeyConstraintsEnabled.AutoClosingRoomOpenHelper$AutoClosingSupportSQLiteDatabase$setForeignKeyConstraintsEnabled$1(b));
        }
        
        @Override
        public void setLocale(final Locale locale) {
            Intrinsics.checkNotNullParameter((Object)locale, "locale");
            this.autoCloser.executeRefCountingFunction((kotlin.jvm.functions.Function1<? super SupportSQLiteDatabase, ?>)new AutoClosingRoomOpenHelper$AutoClosingSupportSQLiteDatabase$setLocale.AutoClosingRoomOpenHelper$AutoClosingSupportSQLiteDatabase$setLocale$1(locale));
        }
        
        @Override
        public void setMaxSqlCacheSize(final int n) {
            this.autoCloser.executeRefCountingFunction((kotlin.jvm.functions.Function1<? super SupportSQLiteDatabase, ?>)new AutoClosingRoomOpenHelper$AutoClosingSupportSQLiteDatabase$setMaxSqlCacheSize.AutoClosingRoomOpenHelper$AutoClosingSupportSQLiteDatabase$setMaxSqlCacheSize$1(n));
        }
        
        @Override
        public long setMaximumSize(final long n) {
            return this.autoCloser.executeRefCountingFunction((kotlin.jvm.functions.Function1<? super SupportSQLiteDatabase, ? extends Number>)new AutoClosingRoomOpenHelper$AutoClosingSupportSQLiteDatabase$setMaximumSize.AutoClosingRoomOpenHelper$AutoClosingSupportSQLiteDatabase$setMaximumSize$1(n)).longValue();
        }
        
        @Override
        public void setPageSize(final long n) {
            this.autoCloser.executeRefCountingFunction((kotlin.jvm.functions.Function1<? super SupportSQLiteDatabase, ?>)new AutoClosingRoomOpenHelper$AutoClosingSupportSQLiteDatabase$pageSize.AutoClosingRoomOpenHelper$AutoClosingSupportSQLiteDatabase$pageSize$2(n));
        }
        
        @Override
        public void setTransactionSuccessful() {
            final SupportSQLiteDatabase delegateDatabase$room_runtime_release = this.autoCloser.getDelegateDatabase$room_runtime_release();
            Unit instance;
            if (delegateDatabase$room_runtime_release != null) {
                delegateDatabase$room_runtime_release.setTransactionSuccessful();
                instance = Unit.INSTANCE;
            }
            else {
                instance = null;
            }
            if (instance != null) {
                return;
            }
            throw new IllegalStateException("setTransactionSuccessful called but delegateDb is null".toString());
        }
        
        @Override
        public void setVersion(final int n) {
            this.autoCloser.executeRefCountingFunction((kotlin.jvm.functions.Function1<? super SupportSQLiteDatabase, ?>)new AutoClosingRoomOpenHelper$AutoClosingSupportSQLiteDatabase$version.AutoClosingRoomOpenHelper$AutoClosingSupportSQLiteDatabase$version$2(n));
        }
        
        @Override
        public int update(final String s, final int n, final ContentValues contentValues, final String s2, final Object[] array) {
            Intrinsics.checkNotNullParameter((Object)s, "table");
            Intrinsics.checkNotNullParameter((Object)contentValues, "values");
            return this.autoCloser.executeRefCountingFunction((kotlin.jvm.functions.Function1<? super SupportSQLiteDatabase, ? extends Number>)new AutoClosingRoomOpenHelper$AutoClosingSupportSQLiteDatabase$update.AutoClosingRoomOpenHelper$AutoClosingSupportSQLiteDatabase$update$1(s, n, contentValues, s2, array)).intValue();
        }
        
        @Override
        public boolean yieldIfContendedSafely() {
            return this.autoCloser.executeRefCountingFunction((kotlin.jvm.functions.Function1<? super SupportSQLiteDatabase, ? extends Boolean>)AutoClosingRoomOpenHelper$AutoClosingSupportSQLiteDatabase$yieldIfContendedSafely.AutoClosingRoomOpenHelper$AutoClosingSupportSQLiteDatabase$yieldIfContendedSafely$1.INSTANCE);
        }
        
        @Override
        public boolean yieldIfContendedSafely(final long n) {
            return this.autoCloser.executeRefCountingFunction((kotlin.jvm.functions.Function1<? super SupportSQLiteDatabase, ? extends Boolean>)AutoClosingRoomOpenHelper$AutoClosingSupportSQLiteDatabase$yieldIfContendedSafely.AutoClosingRoomOpenHelper$AutoClosingSupportSQLiteDatabase$yieldIfContendedSafely$2.INSTANCE);
        }
    }
    
    @Metadata(d1 = { "\u0000N\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0012\n\u0000\n\u0002\u0010\u0006\n\u0000\n\u0002\u0010\t\n\u0002\b\u000b\n\u0002\u0018\u0002\n\u0002\b\u0007\b\u0002\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\u0018\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0010H\u0016J\u0018\u0010\u0011\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0012H\u0016J\u0018\u0010\u0013\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0014H\u0016J\u0010\u0010\u0015\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u000eH\u0016J\u0018\u0010\u0016\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0003H\u0016J\b\u0010\u0017\u001a\u00020\fH\u0016J\b\u0010\u0018\u001a\u00020\fH\u0016J\u0010\u0010\u0019\u001a\u00020\f2\u0006\u0010\u001a\u001a\u00020\u0001H\u0002J\b\u0010\u001b\u001a\u00020\fH\u0016J\b\u0010\u001c\u001a\u00020\u0014H\u0016J'\u0010\u001d\u001a\u0002H\u001e\"\u0004\b\u0000\u0010\u001e2\u0012\u0010\u001f\u001a\u000e\u0012\u0004\u0012\u00020\u0001\u0012\u0004\u0012\u0002H\u001e0 H\u0002¢\u0006\u0002\u0010!J\b\u0010\"\u001a\u00020\u000eH\u0016J\u001a\u0010#\u001a\u00020\f2\u0006\u0010$\u001a\u00020\u000e2\b\u0010\u000f\u001a\u0004\u0018\u00010\tH\u0002J\b\u0010%\u001a\u00020\u0014H\u0016J\n\u0010&\u001a\u0004\u0018\u00010\u0003H\u0016R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004¢\u0006\u0002\n\u0000R\"\u0010\u0007\u001a\u0016\u0012\u0006\u0012\u0004\u0018\u00010\t0\bj\n\u0012\u0006\u0012\u0004\u0018\u00010\t`\nX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006'" }, d2 = { "Landroidx/room/AutoClosingRoomOpenHelper$AutoClosingSupportSqliteStatement;", "Landroidx/sqlite/db/SupportSQLiteStatement;", "sql", "", "autoCloser", "Landroidx/room/AutoCloser;", "(Ljava/lang/String;Landroidx/room/AutoCloser;)V", "binds", "Ljava/util/ArrayList;", "", "Lkotlin/collections/ArrayList;", "bindBlob", "", "index", "", "value", "", "bindDouble", "", "bindLong", "", "bindNull", "bindString", "clearBindings", "close", "doBinds", "supportSQLiteStatement", "execute", "executeInsert", "executeSqliteStatementWithRefCount", "T", "block", "Lkotlin/Function1;", "(Lkotlin/jvm/functions/Function1;)Ljava/lang/Object;", "executeUpdateDelete", "saveBinds", "bindIndex", "simpleQueryForLong", "simpleQueryForString", "room-runtime_release" }, k = 1, mv = { 1, 7, 1 }, xi = 48)
    private static final class AutoClosingSupportSqliteStatement implements SupportSQLiteStatement
    {
        private final AutoCloser autoCloser;
        private final ArrayList<Object> binds;
        private final String sql;
        
        public AutoClosingSupportSqliteStatement(final String sql, final AutoCloser autoCloser) {
            Intrinsics.checkNotNullParameter((Object)sql, "sql");
            Intrinsics.checkNotNullParameter((Object)autoCloser, "autoCloser");
            this.sql = sql;
            this.autoCloser = autoCloser;
            this.binds = new ArrayList<Object>();
        }
        
        private final void doBinds(final SupportSQLiteStatement supportSQLiteStatement) {
            final Iterator iterator = this.binds.iterator();
            int index = 0;
            while (iterator.hasNext()) {
                iterator.next();
                final int n = index + 1;
                if (index < 0) {
                    CollectionsKt.throwIndexOverflow();
                }
                final Object value = this.binds.get(index);
                if (value == null) {
                    supportSQLiteStatement.bindNull(n);
                }
                else if (value instanceof Long) {
                    supportSQLiteStatement.bindLong(n, ((Number)value).longValue());
                }
                else if (value instanceof Double) {
                    supportSQLiteStatement.bindDouble(n, ((Number)value).doubleValue());
                }
                else if (value instanceof String) {
                    supportSQLiteStatement.bindString(n, (String)value);
                }
                else if (value instanceof byte[]) {
                    supportSQLiteStatement.bindBlob(n, (byte[])value);
                }
                index = n;
            }
        }
        
        private final <T> T executeSqliteStatementWithRefCount(final Function1<? super SupportSQLiteStatement, ? extends T> function1) {
            return this.autoCloser.executeRefCountingFunction((kotlin.jvm.functions.Function1<? super SupportSQLiteDatabase, ? extends T>)new AutoClosingRoomOpenHelper$AutoClosingSupportSqliteStatement$executeSqliteStatementWithRefCount.AutoClosingRoomOpenHelper$AutoClosingSupportSqliteStatement$executeSqliteStatementWithRefCount$1(this, (Function1)function1));
        }
        
        private final void saveBinds(int size, final Object element) {
            final int index = size - 1;
            if (index >= this.binds.size()) {
                size = this.binds.size();
                if (size <= index) {
                    while (true) {
                        this.binds.add(null);
                        if (size == index) {
                            break;
                        }
                        ++size;
                    }
                }
            }
            this.binds.set(index, element);
        }
        
        @Override
        public void bindBlob(final int n, final byte[] array) {
            Intrinsics.checkNotNullParameter((Object)array, "value");
            this.saveBinds(n, array);
        }
        
        @Override
        public void bindDouble(final int n, final double d) {
            this.saveBinds(n, d);
        }
        
        @Override
        public void bindLong(final int n, final long l) {
            this.saveBinds(n, l);
        }
        
        @Override
        public void bindNull(final int n) {
            this.saveBinds(n, null);
        }
        
        @Override
        public void bindString(final int n, final String s) {
            Intrinsics.checkNotNullParameter((Object)s, "value");
            this.saveBinds(n, s);
        }
        
        @Override
        public void clearBindings() {
            this.binds.clear();
        }
        
        @Override
        public void close() throws IOException {
        }
        
        @Override
        public void execute() {
            this.executeSqliteStatementWithRefCount((kotlin.jvm.functions.Function1<? super SupportSQLiteStatement, ?>)AutoClosingRoomOpenHelper$AutoClosingSupportSqliteStatement$execute.AutoClosingRoomOpenHelper$AutoClosingSupportSqliteStatement$execute$1.INSTANCE);
        }
        
        @Override
        public long executeInsert() {
            return this.executeSqliteStatementWithRefCount((kotlin.jvm.functions.Function1<? super SupportSQLiteStatement, ? extends Number>)AutoClosingRoomOpenHelper$AutoClosingSupportSqliteStatement$executeInsert.AutoClosingRoomOpenHelper$AutoClosingSupportSqliteStatement$executeInsert$1.INSTANCE).longValue();
        }
        
        @Override
        public int executeUpdateDelete() {
            return this.executeSqliteStatementWithRefCount((kotlin.jvm.functions.Function1<? super SupportSQLiteStatement, ? extends Number>)AutoClosingRoomOpenHelper$AutoClosingSupportSqliteStatement$executeUpdateDelete.AutoClosingRoomOpenHelper$AutoClosingSupportSqliteStatement$executeUpdateDelete$1.INSTANCE).intValue();
        }
        
        @Override
        public long simpleQueryForLong() {
            return this.executeSqliteStatementWithRefCount((kotlin.jvm.functions.Function1<? super SupportSQLiteStatement, ? extends Number>)AutoClosingRoomOpenHelper$AutoClosingSupportSqliteStatement$simpleQueryForLong.AutoClosingRoomOpenHelper$AutoClosingSupportSqliteStatement$simpleQueryForLong$1.INSTANCE).longValue();
        }
        
        @Override
        public String simpleQueryForString() {
            return this.executeSqliteStatementWithRefCount((kotlin.jvm.functions.Function1<? super SupportSQLiteStatement, ? extends String>)AutoClosingRoomOpenHelper$AutoClosingSupportSqliteStatement$simpleQueryForString.AutoClosingRoomOpenHelper$AutoClosingSupportSqliteStatement$simpleQueryForString$1.INSTANCE);
        }
    }
    
    @Metadata(d1 = { "\u0000\u0090\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0012\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\u0011\n\u0002\b\u0003\n\u0002\u0010\u0006\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0007\n\u0002\b\u0002\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\b\u0002\n\u0002\u0010\n\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\r\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0006\b\u0002\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0001\u0012\u0006\u0010\u0003\u001a\u00020\u0004¢\u0006\u0002\u0010\u0005J\b\u0010\u0006\u001a\u00020\u0007H\u0016J!\u0010\b\u001a\u00020\u00072\u0006\u0010\t\u001a\u00020\n2\u000e\u0010\u000b\u001a\n \r*\u0004\u0018\u00010\f0\fH\u0096\u0001J\t\u0010\u000e\u001a\u00020\u0007H\u0097\u0001J\u0019\u0010\u000f\u001a\n \r*\u0004\u0018\u00010\u00100\u00102\u0006\u0010\t\u001a\u00020\nH\u0096\u0001J\t\u0010\u0011\u001a\u00020\nH\u0096\u0001J\u0019\u0010\u0012\u001a\u00020\n2\u000e\u0010\t\u001a\n \r*\u0004\u0018\u00010\u00130\u0013H\u0096\u0001J\u0019\u0010\u0014\u001a\u00020\n2\u000e\u0010\t\u001a\n \r*\u0004\u0018\u00010\u00130\u0013H\u0096\u0001J\u0019\u0010\u0015\u001a\n \r*\u0004\u0018\u00010\u00130\u00132\u0006\u0010\t\u001a\u00020\nH\u0096\u0001J4\u0010\u0016\u001a(\u0012\f\u0012\n \r*\u0004\u0018\u00010\u00130\u0013 \r*\u0014\u0012\u000e\b\u0001\u0012\n \r*\u0004\u0018\u00010\u00130\u0013\u0018\u00010\u00170\u0017H\u0096\u0001¢\u0006\u0002\u0010\u0018J\t\u0010\u0019\u001a\u00020\nH\u0096\u0001J\u0011\u0010\u001a\u001a\u00020\u001b2\u0006\u0010\t\u001a\u00020\nH\u0096\u0001J\u0011\u0010\u001c\u001a\n \r*\u0004\u0018\u00010\u001d0\u001dH\u0096\u0001J\u0011\u0010\u001e\u001a\u00020\u001f2\u0006\u0010\t\u001a\u00020\nH\u0096\u0001J\u0011\u0010 \u001a\u00020\n2\u0006\u0010\t\u001a\u00020\nH\u0096\u0001J\u0011\u0010!\u001a\u00020\"2\u0006\u0010\t\u001a\u00020\nH\u0096\u0001J\b\u0010#\u001a\u00020$H\u0017J\u000e\u0010%\u001a\b\u0012\u0004\u0012\u00020$0&H\u0017J\t\u0010'\u001a\u00020\nH\u0096\u0001J\u0011\u0010(\u001a\u00020)2\u0006\u0010\t\u001a\u00020\nH\u0096\u0001J\u0019\u0010*\u001a\n \r*\u0004\u0018\u00010\u00130\u00132\u0006\u0010\t\u001a\u00020\nH\u0096\u0001J\u0011\u0010+\u001a\u00020\n2\u0006\u0010\t\u001a\u00020\nH\u0096\u0001J\t\u0010,\u001a\u00020-H\u0096\u0001J\t\u0010.\u001a\u00020-H\u0096\u0001J\t\u0010/\u001a\u00020-H\u0096\u0001J\t\u00100\u001a\u00020-H\u0096\u0001J\t\u00101\u001a\u00020-H\u0096\u0001J\t\u00102\u001a\u00020-H\u0096\u0001J\u0011\u00103\u001a\u00020-2\u0006\u0010\t\u001a\u00020\nH\u0096\u0001J\u0011\u00104\u001a\u00020-2\u0006\u0010\t\u001a\u00020\nH\u0096\u0001J\t\u00105\u001a\u00020-H\u0096\u0001J\t\u00106\u001a\u00020-H\u0096\u0001J\t\u00107\u001a\u00020-H\u0096\u0001J\u0011\u00108\u001a\u00020-2\u0006\u0010\t\u001a\u00020\nH\u0096\u0001J\t\u00109\u001a\u00020-H\u0096\u0001J\u0019\u0010:\u001a\u00020\u00072\u000e\u0010\t\u001a\n \r*\u0004\u0018\u00010;0;H\u0096\u0001J\u0019\u0010<\u001a\u00020\u00072\u000e\u0010\t\u001a\n \r*\u0004\u0018\u00010=0=H\u0096\u0001J\t\u0010>\u001a\u00020-H\u0097\u0001J!\u0010?\u001a\n \r*\u0004\u0018\u00010\u001d0\u001d2\u000e\u0010\t\u001a\n \r*\u0004\u0018\u00010\u001d0\u001dH\u0096\u0001J\u0010\u0010@\u001a\u00020\u00072\u0006\u0010A\u001a\u00020\u001dH\u0017J)\u0010B\u001a\u00020\u00072\u000e\u0010\t\u001a\n \r*\u0004\u0018\u00010C0C2\u000e\u0010\u000b\u001a\n \r*\u0004\u0018\u00010$0$H\u0096\u0001J\u001e\u0010D\u001a\u00020\u00072\u0006\u0010E\u001a\u00020C2\f\u0010F\u001a\b\u0012\u0004\u0012\u00020$0&H\u0017J\u0019\u0010G\u001a\u00020\u00072\u000e\u0010\t\u001a\n \r*\u0004\u0018\u00010;0;H\u0096\u0001J\u0019\u0010H\u001a\u00020\u00072\u000e\u0010\t\u001a\n \r*\u0004\u0018\u00010=0=H\u0096\u0001R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0001X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006I" }, d2 = { "Landroidx/room/AutoClosingRoomOpenHelper$KeepAliveCursor;", "Landroid/database/Cursor;", "delegate", "autoCloser", "Landroidx/room/AutoCloser;", "(Landroid/database/Cursor;Landroidx/room/AutoCloser;)V", "close", "", "copyStringToBuffer", "p0", "", "p1", "Landroid/database/CharArrayBuffer;", "kotlin.jvm.PlatformType", "deactivate", "getBlob", "", "getColumnCount", "getColumnIndex", "", "getColumnIndexOrThrow", "getColumnName", "getColumnNames", "", "()[Ljava/lang/String;", "getCount", "getDouble", "", "getExtras", "Landroid/os/Bundle;", "getFloat", "", "getInt", "getLong", "", "getNotificationUri", "Landroid/net/Uri;", "getNotificationUris", "", "getPosition", "getShort", "", "getString", "getType", "getWantsAllOnMoveCalls", "", "isAfterLast", "isBeforeFirst", "isClosed", "isFirst", "isLast", "isNull", "move", "moveToFirst", "moveToLast", "moveToNext", "moveToPosition", "moveToPrevious", "registerContentObserver", "Landroid/database/ContentObserver;", "registerDataSetObserver", "Landroid/database/DataSetObserver;", "requery", "respond", "setExtras", "extras", "setNotificationUri", "Landroid/content/ContentResolver;", "setNotificationUris", "cr", "uris", "unregisterContentObserver", "unregisterDataSetObserver", "room-runtime_release" }, k = 1, mv = { 1, 7, 1 }, xi = 48)
    private static final class KeepAliveCursor implements Cursor
    {
        private final AutoCloser autoCloser;
        private final Cursor delegate;
        
        public KeepAliveCursor(final Cursor delegate, final AutoCloser autoCloser) {
            Intrinsics.checkNotNullParameter((Object)delegate, "delegate");
            Intrinsics.checkNotNullParameter((Object)autoCloser, "autoCloser");
            this.delegate = delegate;
            this.autoCloser = autoCloser;
        }
        
        public void close() {
            this.delegate.close();
            this.autoCloser.decrementCountAndScheduleClose();
        }
        
        public void copyStringToBuffer(final int n, final CharArrayBuffer charArrayBuffer) {
            this.delegate.copyStringToBuffer(n, charArrayBuffer);
        }
        
        @Deprecated(message = "Deprecated in Java")
        public void deactivate() {
            this.delegate.deactivate();
        }
        
        public byte[] getBlob(final int n) {
            return this.delegate.getBlob(n);
        }
        
        public int getColumnCount() {
            return this.delegate.getColumnCount();
        }
        
        public int getColumnIndex(final String s) {
            return this.delegate.getColumnIndex(s);
        }
        
        public int getColumnIndexOrThrow(final String s) {
            return this.delegate.getColumnIndexOrThrow(s);
        }
        
        public String getColumnName(final int n) {
            return this.delegate.getColumnName(n);
        }
        
        public String[] getColumnNames() {
            return this.delegate.getColumnNames();
        }
        
        public int getCount() {
            return this.delegate.getCount();
        }
        
        public double getDouble(final int n) {
            return this.delegate.getDouble(n);
        }
        
        public Bundle getExtras() {
            return this.delegate.getExtras();
        }
        
        public float getFloat(final int n) {
            return this.delegate.getFloat(n);
        }
        
        public int getInt(final int n) {
            return this.delegate.getInt(n);
        }
        
        public long getLong(final int n) {
            return this.delegate.getLong(n);
        }
        
        public Uri getNotificationUri() {
            return SupportSQLiteCompat.Api19Impl.getNotificationUri(this.delegate);
        }
        
        public List<Uri> getNotificationUris() {
            return SupportSQLiteCompat.Api29Impl.getNotificationUris(this.delegate);
        }
        
        public int getPosition() {
            return this.delegate.getPosition();
        }
        
        public short getShort(final int n) {
            return this.delegate.getShort(n);
        }
        
        public String getString(final int n) {
            return this.delegate.getString(n);
        }
        
        public int getType(final int n) {
            return this.delegate.getType(n);
        }
        
        public boolean getWantsAllOnMoveCalls() {
            return this.delegate.getWantsAllOnMoveCalls();
        }
        
        public boolean isAfterLast() {
            return this.delegate.isAfterLast();
        }
        
        public boolean isBeforeFirst() {
            return this.delegate.isBeforeFirst();
        }
        
        public boolean isClosed() {
            return this.delegate.isClosed();
        }
        
        public boolean isFirst() {
            return this.delegate.isFirst();
        }
        
        public boolean isLast() {
            return this.delegate.isLast();
        }
        
        public boolean isNull(final int n) {
            return this.delegate.isNull(n);
        }
        
        public boolean move(final int n) {
            return this.delegate.move(n);
        }
        
        public boolean moveToFirst() {
            return this.delegate.moveToFirst();
        }
        
        public boolean moveToLast() {
            return this.delegate.moveToLast();
        }
        
        public boolean moveToNext() {
            return this.delegate.moveToNext();
        }
        
        public boolean moveToPosition(final int n) {
            return this.delegate.moveToPosition(n);
        }
        
        public boolean moveToPrevious() {
            return this.delegate.moveToPrevious();
        }
        
        public void registerContentObserver(final ContentObserver contentObserver) {
            this.delegate.registerContentObserver(contentObserver);
        }
        
        public void registerDataSetObserver(final DataSetObserver dataSetObserver) {
            this.delegate.registerDataSetObserver(dataSetObserver);
        }
        
        @Deprecated(message = "Deprecated in Java")
        public boolean requery() {
            return this.delegate.requery();
        }
        
        public Bundle respond(final Bundle bundle) {
            return this.delegate.respond(bundle);
        }
        
        public void setExtras(final Bundle bundle) {
            Intrinsics.checkNotNullParameter((Object)bundle, "extras");
            SupportSQLiteCompat.Api23Impl.setExtras(this.delegate, bundle);
        }
        
        public void setNotificationUri(final ContentResolver contentResolver, final Uri uri) {
            this.delegate.setNotificationUri(contentResolver, uri);
        }
        
        public void setNotificationUris(final ContentResolver contentResolver, final List<? extends Uri> list) {
            Intrinsics.checkNotNullParameter((Object)contentResolver, "cr");
            Intrinsics.checkNotNullParameter((Object)list, "uris");
            SupportSQLiteCompat.Api29Impl.setNotificationUris(this.delegate, contentResolver, list);
        }
        
        public void unregisterContentObserver(final ContentObserver contentObserver) {
            this.delegate.unregisterContentObserver(contentObserver);
        }
        
        public void unregisterDataSetObserver(final DataSetObserver dataSetObserver) {
            this.delegate.unregisterDataSetObserver(dataSetObserver);
        }
    }
}

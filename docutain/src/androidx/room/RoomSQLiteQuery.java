// 
// Decompiled by Procyon v0.6.0
// 

package androidx.room;

import java.util.Iterator;
import kotlin.annotation.AnnotationRetention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;
import kotlin.Unit;
import java.util.Map;
import java.util.Arrays;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.JvmStatic;
import kotlin.jvm.internal.DefaultConstructorMarker;
import java.util.TreeMap;
import kotlin.Metadata;
import androidx.sqlite.db.SupportSQLiteProgram;
import androidx.sqlite.db.SupportSQLiteQuery;

@Metadata(d1 = { "\u0000X\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0006\n\u0002\u0010\u0015\n\u0002\b\u0003\n\u0002\u0010\u0011\n\u0002\u0010\u0012\n\u0002\b\u0004\n\u0002\u0010\u0013\n\u0002\b\u0002\n\u0002\u0010\u0016\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0007\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0010\u0006\n\u0000\n\u0002\u0010\t\n\u0002\b\u000e\b\u0007\u0018\u0000 62\u00020\u00012\u00020\u0002:\u000256B\u000f\b\u0002\u0012\u0006\u0010\u0003\u001a\u00020\u0004¢\u0006\u0002\u0010\u0005J\u0018\u0010\"\u001a\u00020#2\u0006\u0010$\u001a\u00020\u00042\u0006\u0010%\u001a\u00020\u0010H\u0016J\u0018\u0010&\u001a\u00020#2\u0006\u0010$\u001a\u00020\u00042\u0006\u0010%\u001a\u00020'H\u0016J\u0018\u0010(\u001a\u00020#2\u0006\u0010$\u001a\u00020\u00042\u0006\u0010%\u001a\u00020)H\u0016J\u0010\u0010*\u001a\u00020#2\u0006\u0010$\u001a\u00020\u0004H\u0016J\u0018\u0010+\u001a\u00020#2\u0006\u0010$\u001a\u00020\u00042\u0006\u0010%\u001a\u00020\u001bH\u0016J\u0010\u0010,\u001a\u00020#2\u0006\u0010-\u001a\u00020\u0002H\u0016J\b\u0010.\u001a\u00020#H\u0016J\b\u0010/\u001a\u00020#H\u0016J\u000e\u00100\u001a\u00020#2\u0006\u00101\u001a\u00020\u0000J\u0016\u00102\u001a\u00020#2\u0006\u0010\u001a\u001a\u00020\u001b2\u0006\u00103\u001a\u00020\u0004J\u0006\u00104\u001a\u00020#R\u001e\u0010\u0007\u001a\u00020\u00042\u0006\u0010\u0006\u001a\u00020\u0004@RX\u0096\u000e¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010\tR\u0014\u0010\n\u001a\u00020\u000bX\u0082\u0004¢\u0006\b\n\u0000\u0012\u0004\b\f\u0010\rR \u0010\u000e\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00100\u000f8\u0006X\u0087\u0004¢\u0006\n\n\u0002\u0010\u0012\u0012\u0004\b\u0011\u0010\rR\u0016\u0010\u0003\u001a\u00020\u00048\u0006X\u0087\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0013\u0010\tR\u0016\u0010\u0014\u001a\u00020\u00158\u0006X\u0087\u0004¢\u0006\b\n\u0000\u0012\u0004\b\u0016\u0010\rR\u0016\u0010\u0017\u001a\u00020\u00188\u0006X\u0087\u0004¢\u0006\b\n\u0000\u0012\u0004\b\u0019\u0010\rR\u0010\u0010\u001a\u001a\u0004\u0018\u00010\u001bX\u0082\u000e¢\u0006\u0002\n\u0000R\u0014\u0010\u001c\u001a\u00020\u001b8VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u001d\u0010\u001eR \u0010\u001f\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u001b0\u000f8\u0006X\u0087\u0004¢\u0006\n\n\u0002\u0010!\u0012\u0004\b \u0010\r¨\u00067" }, d2 = { "Landroidx/room/RoomSQLiteQuery;", "Landroidx/sqlite/db/SupportSQLiteQuery;", "Landroidx/sqlite/db/SupportSQLiteProgram;", "capacity", "", "(I)V", "<set-?>", "argCount", "getArgCount", "()I", "bindingTypes", "", "getBindingTypes$annotations", "()V", "blobBindings", "", "", "getBlobBindings$annotations", "[[B", "getCapacity", "doubleBindings", "", "getDoubleBindings$annotations", "longBindings", "", "getLongBindings$annotations", "query", "", "sql", "getSql", "()Ljava/lang/String;", "stringBindings", "getStringBindings$annotations", "[Ljava/lang/String;", "bindBlob", "", "index", "value", "bindDouble", "", "bindLong", "", "bindNull", "bindString", "bindTo", "statement", "clearBindings", "close", "copyArgumentsFrom", "other", "init", "initArgCount", "release", "Binding", "Companion", "room-runtime_release" }, k = 1, mv = { 1, 7, 1 }, xi = 48)
public final class RoomSQLiteQuery implements SupportSQLiteQuery, SupportSQLiteProgram
{
    private static final int BLOB = 5;
    public static final Companion Companion;
    public static final int DESIRED_POOL_SIZE = 10;
    private static final int DOUBLE = 3;
    private static final int LONG = 2;
    private static final int NULL = 1;
    public static final int POOL_LIMIT = 15;
    private static final int STRING = 4;
    public static final TreeMap<Integer, RoomSQLiteQuery> queryPool;
    private int argCount;
    private final int[] bindingTypes;
    public final byte[][] blobBindings;
    private final int capacity;
    public final double[] doubleBindings;
    public final long[] longBindings;
    private volatile String query;
    public final String[] stringBindings;
    
    static {
        Companion = new Companion(null);
        queryPool = new TreeMap<Integer, RoomSQLiteQuery>();
    }
    
    private RoomSQLiteQuery(int capacity) {
        this.capacity = capacity;
        ++capacity;
        this.bindingTypes = new int[capacity];
        this.longBindings = new long[capacity];
        this.doubleBindings = new double[capacity];
        this.stringBindings = new String[capacity];
        this.blobBindings = new byte[capacity][];
    }
    
    @JvmStatic
    public static final RoomSQLiteQuery acquire(final String s, final int n) {
        return RoomSQLiteQuery.Companion.acquire(s, n);
    }
    
    @JvmStatic
    public static final RoomSQLiteQuery copyFrom(final SupportSQLiteQuery supportSQLiteQuery) {
        return RoomSQLiteQuery.Companion.copyFrom(supportSQLiteQuery);
    }
    
    @Override
    public void bindBlob(final int n, final byte[] array) {
        Intrinsics.checkNotNullParameter((Object)array, "value");
        this.bindingTypes[n] = 5;
        this.blobBindings[n] = array;
    }
    
    @Override
    public void bindDouble(final int n, final double n2) {
        this.bindingTypes[n] = 3;
        this.doubleBindings[n] = n2;
    }
    
    @Override
    public void bindLong(final int n, final long n2) {
        this.bindingTypes[n] = 2;
        this.longBindings[n] = n2;
    }
    
    @Override
    public void bindNull(final int n) {
        this.bindingTypes[n] = 1;
    }
    
    @Override
    public void bindString(final int n, final String s) {
        Intrinsics.checkNotNullParameter((Object)s, "value");
        this.bindingTypes[n] = 4;
        this.stringBindings[n] = s;
    }
    
    @Override
    public void bindTo(final SupportSQLiteProgram supportSQLiteProgram) {
        Intrinsics.checkNotNullParameter((Object)supportSQLiteProgram, "statement");
        final int argCount = this.getArgCount();
        if (1 <= argCount) {
            int n = 1;
            while (true) {
                final int n2 = this.bindingTypes[n];
                if (n2 != 1) {
                    if (n2 != 2) {
                        if (n2 != 3) {
                            if (n2 != 4) {
                                if (n2 == 5) {
                                    final byte[] array = this.blobBindings[n];
                                    if (array == null) {
                                        throw new IllegalArgumentException("Required value was null.".toString());
                                    }
                                    supportSQLiteProgram.bindBlob(n, array);
                                }
                            }
                            else {
                                final String s = this.stringBindings[n];
                                if (s == null) {
                                    throw new IllegalArgumentException("Required value was null.".toString());
                                }
                                supportSQLiteProgram.bindString(n, s);
                            }
                        }
                        else {
                            supportSQLiteProgram.bindDouble(n, this.doubleBindings[n]);
                        }
                    }
                    else {
                        supportSQLiteProgram.bindLong(n, this.longBindings[n]);
                    }
                }
                else {
                    supportSQLiteProgram.bindNull(n);
                }
                if (n == argCount) {
                    break;
                }
                ++n;
            }
        }
    }
    
    @Override
    public void clearBindings() {
        Arrays.fill(this.bindingTypes, 1);
        Arrays.fill(this.stringBindings, null);
        Arrays.fill(this.blobBindings, null);
        this.query = null;
    }
    
    @Override
    public void close() {
    }
    
    public final void copyArgumentsFrom(final RoomSQLiteQuery roomSQLiteQuery) {
        Intrinsics.checkNotNullParameter((Object)roomSQLiteQuery, "other");
        final int n = roomSQLiteQuery.getArgCount() + 1;
        System.arraycopy(roomSQLiteQuery.bindingTypes, 0, this.bindingTypes, 0, n);
        System.arraycopy(roomSQLiteQuery.longBindings, 0, this.longBindings, 0, n);
        System.arraycopy(roomSQLiteQuery.stringBindings, 0, this.stringBindings, 0, n);
        System.arraycopy(roomSQLiteQuery.blobBindings, 0, this.blobBindings, 0, n);
        System.arraycopy(roomSQLiteQuery.doubleBindings, 0, this.doubleBindings, 0, n);
    }
    
    @Override
    public int getArgCount() {
        return this.argCount;
    }
    
    public final int getCapacity() {
        return this.capacity;
    }
    
    @Override
    public String getSql() {
        final String query = this.query;
        if (query != null) {
            return query;
        }
        throw new IllegalStateException("Required value was null.".toString());
    }
    
    public final void init(final String query, final int argCount) {
        Intrinsics.checkNotNullParameter((Object)query, "query");
        this.query = query;
        this.argCount = argCount;
    }
    
    public final void release() {
        final TreeMap<Integer, RoomSQLiteQuery> queryPool = RoomSQLiteQuery.queryPool;
        synchronized (queryPool) {
            queryPool.put(this.capacity, this);
            RoomSQLiteQuery.Companion.prunePoolLocked$room_runtime_release();
            final Unit instance = Unit.INSTANCE;
        }
    }
    
    @Retention(RetentionPolicy.SOURCE)
    @Metadata(d1 = { "\u0000\n\n\u0002\u0018\u0002\n\u0002\u0010\u001b\n\u0000\b\u0081\u0002\u0018\u00002\u00020\u0001B\u0000¨\u0006\u0002" }, d2 = { "Landroidx/room/RoomSQLiteQuery$Binding;", "", "room-runtime_release" }, k = 1, mv = { 1, 7, 1 }, xi = 48)
    @kotlin.annotation.Retention(AnnotationRetention.SOURCE)
    public @interface Binding {
    }
    
    @Metadata(d1 = { "\u00006\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0018\u0010\u0011\u001a\u00020\u000f2\u0006\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u0004H\u0007J\u0010\u0010\u0015\u001a\u00020\u000f2\u0006\u0010\u0016\u001a\u00020\u0017H\u0007J\r\u0010\u0018\u001a\u00020\u0019H\u0000¢\u0006\u0002\b\u001aR\u000e\u0010\u0003\u001a\u00020\u0004X\u0082T¢\u0006\u0002\n\u0000R\u0016\u0010\u0005\u001a\u00020\u00048\u0006X\u0087T¢\u0006\b\n\u0000\u0012\u0004\b\u0006\u0010\u0002R\u000e\u0010\u0007\u001a\u00020\u0004X\u0082T¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0004X\u0082T¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\u0004X\u0082T¢\u0006\u0002\n\u0000R\u0016\u0010\n\u001a\u00020\u00048\u0006X\u0087T¢\u0006\b\n\u0000\u0012\u0004\b\u000b\u0010\u0002R\u000e\u0010\f\u001a\u00020\u0004X\u0082T¢\u0006\u0002\n\u0000R\"\u0010\r\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u000f0\u000e8\u0006X\u0087\u0004¢\u0006\b\n\u0000\u0012\u0004\b\u0010\u0010\u0002¨\u0006\u001b" }, d2 = { "Landroidx/room/RoomSQLiteQuery$Companion;", "", "()V", "BLOB", "", "DESIRED_POOL_SIZE", "getDESIRED_POOL_SIZE$annotations", "DOUBLE", "LONG", "NULL", "POOL_LIMIT", "getPOOL_LIMIT$annotations", "STRING", "queryPool", "Ljava/util/TreeMap;", "Landroidx/room/RoomSQLiteQuery;", "getQueryPool$annotations", "acquire", "query", "", "argumentCount", "copyFrom", "supportSQLiteQuery", "Landroidx/sqlite/db/SupportSQLiteQuery;", "prunePoolLocked", "", "prunePoolLocked$room_runtime_release", "room-runtime_release" }, k = 1, mv = { 1, 7, 1 }, xi = 48)
    public static final class Companion
    {
        private Companion() {
        }
        
        @JvmStatic
        public final RoomSQLiteQuery acquire(final String s, final int i) {
            Intrinsics.checkNotNullParameter((Object)s, "query");
            Object queryPool = RoomSQLiteQuery.queryPool;
            synchronized (queryPool) {
                final Map.Entry<Integer, RoomSQLiteQuery> ceilingEntry = RoomSQLiteQuery.queryPool.ceilingEntry(i);
                if (ceilingEntry != null) {
                    RoomSQLiteQuery.queryPool.remove(ceilingEntry.getKey());
                    final RoomSQLiteQuery roomSQLiteQuery = ceilingEntry.getValue();
                    roomSQLiteQuery.init(s, i);
                    Intrinsics.checkNotNullExpressionValue((Object)roomSQLiteQuery, "sqliteQuery");
                    return roomSQLiteQuery;
                }
                final Unit instance = Unit.INSTANCE;
                monitorexit(queryPool);
                queryPool = new RoomSQLiteQuery(i, null);
                ((RoomSQLiteQuery)queryPool).init(s, i);
                return (RoomSQLiteQuery)queryPool;
            }
        }
        
        @JvmStatic
        public final RoomSQLiteQuery copyFrom(final SupportSQLiteQuery supportSQLiteQuery) {
            Intrinsics.checkNotNullParameter((Object)supportSQLiteQuery, "supportSQLiteQuery");
            final RoomSQLiteQuery acquire = this.acquire(supportSQLiteQuery.getSql(), supportSQLiteQuery.getArgCount());
            supportSQLiteQuery.bindTo((SupportSQLiteProgram)new RoomSQLiteQuery$Companion$copyFrom.RoomSQLiteQuery$Companion$copyFrom$1(acquire));
            return acquire;
        }
        
        public final void prunePoolLocked$room_runtime_release() {
            if (RoomSQLiteQuery.queryPool.size() > 15) {
                int i = RoomSQLiteQuery.queryPool.size() - 10;
                final Iterator<Integer> iterator = RoomSQLiteQuery.queryPool.descendingKeySet().iterator();
                Intrinsics.checkNotNullExpressionValue((Object)iterator, "queryPool.descendingKeySet().iterator()");
                while (i > 0) {
                    iterator.next();
                    iterator.remove();
                    --i;
                }
            }
        }
    }
}

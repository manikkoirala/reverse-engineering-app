// 
// Decompiled by Procyon v0.6.0
// 

package androidx.room;

import kotlin.jvm.internal.Intrinsics;
import java.io.InputStream;
import java.util.concurrent.Callable;
import java.io.File;
import kotlin.Metadata;
import androidx.sqlite.db.SupportSQLiteOpenHelper;

@Metadata(d1 = { "\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0000\u0018\u00002\u00020\u0001B1\u0012\b\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\b\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\u000e\u0010\u0006\u001a\n\u0012\u0004\u0012\u00020\b\u0018\u00010\u0007\u0012\u0006\u0010\t\u001a\u00020\u0001¢\u0006\u0002\u0010\nJ\u0010\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u000eH\u0016R\u0010\u0010\u0002\u001a\u0004\u0018\u00010\u0003X\u0082\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\u0004\u001a\u0004\u0018\u00010\u0005X\u0082\u0004¢\u0006\u0002\n\u0000R\u0016\u0010\u0006\u001a\n\u0012\u0004\u0012\u00020\b\u0018\u00010\u0007X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\u0001X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u000f" }, d2 = { "Landroidx/room/SQLiteCopyOpenHelperFactory;", "Landroidx/sqlite/db/SupportSQLiteOpenHelper$Factory;", "mCopyFromAssetPath", "", "mCopyFromFile", "Ljava/io/File;", "mCopyFromInputStream", "Ljava/util/concurrent/Callable;", "Ljava/io/InputStream;", "mDelegate", "(Ljava/lang/String;Ljava/io/File;Ljava/util/concurrent/Callable;Landroidx/sqlite/db/SupportSQLiteOpenHelper$Factory;)V", "create", "Landroidx/sqlite/db/SupportSQLiteOpenHelper;", "configuration", "Landroidx/sqlite/db/SupportSQLiteOpenHelper$Configuration;", "room-runtime_release" }, k = 1, mv = { 1, 7, 1 }, xi = 48)
public final class SQLiteCopyOpenHelperFactory implements Factory
{
    private final String mCopyFromAssetPath;
    private final File mCopyFromFile;
    private final Callable<InputStream> mCopyFromInputStream;
    private final Factory mDelegate;
    
    public SQLiteCopyOpenHelperFactory(final String mCopyFromAssetPath, final File mCopyFromFile, final Callable<InputStream> mCopyFromInputStream, final Factory mDelegate) {
        Intrinsics.checkNotNullParameter((Object)mDelegate, "mDelegate");
        this.mCopyFromAssetPath = mCopyFromAssetPath;
        this.mCopyFromFile = mCopyFromFile;
        this.mCopyFromInputStream = mCopyFromInputStream;
        this.mDelegate = mDelegate;
    }
    
    @Override
    public SupportSQLiteOpenHelper create(final Configuration configuration) {
        Intrinsics.checkNotNullParameter((Object)configuration, "configuration");
        return new SQLiteCopyOpenHelper(configuration.context, this.mCopyFromAssetPath, this.mCopyFromFile, this.mCopyFromInputStream, configuration.callback.version, this.mDelegate.create(configuration));
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.room;

import java.util.concurrent.Executor;
import kotlin.jvm.internal.Intrinsics;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.Callable;
import kotlin.Metadata;
import androidx.lifecycle.LiveData;

@Metadata(d1 = { "\u0000X\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0011\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\u0002\n\u0002\b\u0002\b\u0001\u0018\u0000*\u0004\b\u0000\u0010\u00012\b\u0012\u0004\u0012\u0002H\u00010\u0002B;\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\b\u0012\f\u0010\t\u001a\b\u0012\u0004\u0012\u00028\u00000\n\u0012\u000e\u0010\u000b\u001a\n\u0012\u0006\b\u0001\u0012\u00020\r0\f¢\u0006\u0002\u0010\u000eJ\b\u0010+\u001a\u00020,H\u0014J\b\u0010-\u001a\u00020,H\u0014R\u0017\u0010\t\u001a\b\u0012\u0004\u0012\u00028\u00000\n¢\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u0010R\u0011\u0010\u0011\u001a\u00020\u0012¢\u0006\b\n\u0000\u001a\u0004\b\u0013\u0010\u0014R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004¢\u0006\u0002\n\u0000R\u0011\u0010\u0003\u001a\u00020\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0015\u0010\u0016R\u0011\u0010\u0007\u001a\u00020\b¢\u0006\b\n\u0000\u001a\u0004\b\u0017\u0010\u0018R\u0011\u0010\u0019\u001a\u00020\u0012¢\u0006\b\n\u0000\u001a\u0004\b\u001a\u0010\u0014R\u0011\u0010\u001b\u001a\u00020\u001c¢\u0006\b\n\u0000\u001a\u0004\b\u001d\u0010\u001eR\u0011\u0010\u001f\u001a\u00020 ¢\u0006\b\n\u0000\u001a\u0004\b!\u0010\"R\u0011\u0010#\u001a\u00020$8F¢\u0006\u0006\u001a\u0004\b%\u0010&R\u0011\u0010'\u001a\u00020\u001c¢\u0006\b\n\u0000\u001a\u0004\b(\u0010\u001eR\u0011\u0010)\u001a\u00020\u0012¢\u0006\b\n\u0000\u001a\u0004\b*\u0010\u0014¨\u0006." }, d2 = { "Landroidx/room/RoomTrackingLiveData;", "T", "Landroidx/lifecycle/LiveData;", "database", "Landroidx/room/RoomDatabase;", "container", "Landroidx/room/InvalidationLiveDataContainer;", "inTransaction", "", "computeFunction", "Ljava/util/concurrent/Callable;", "tableNames", "", "", "(Landroidx/room/RoomDatabase;Landroidx/room/InvalidationLiveDataContainer;ZLjava/util/concurrent/Callable;[Ljava/lang/String;)V", "getComputeFunction", "()Ljava/util/concurrent/Callable;", "computing", "Ljava/util/concurrent/atomic/AtomicBoolean;", "getComputing", "()Ljava/util/concurrent/atomic/AtomicBoolean;", "getDatabase", "()Landroidx/room/RoomDatabase;", "getInTransaction", "()Z", "invalid", "getInvalid", "invalidationRunnable", "Ljava/lang/Runnable;", "getInvalidationRunnable", "()Ljava/lang/Runnable;", "observer", "Landroidx/room/InvalidationTracker$Observer;", "getObserver", "()Landroidx/room/InvalidationTracker$Observer;", "queryExecutor", "Ljava/util/concurrent/Executor;", "getQueryExecutor", "()Ljava/util/concurrent/Executor;", "refreshRunnable", "getRefreshRunnable", "registeredObserver", "getRegisteredObserver", "onActive", "", "onInactive", "room-runtime_release" }, k = 1, mv = { 1, 7, 1 }, xi = 48)
public final class RoomTrackingLiveData<T> extends LiveData<T>
{
    private final Callable<T> computeFunction;
    private final AtomicBoolean computing;
    private final InvalidationLiveDataContainer container;
    private final RoomDatabase database;
    private final boolean inTransaction;
    private final AtomicBoolean invalid;
    private final Runnable invalidationRunnable;
    private final InvalidationTracker.Observer observer;
    private final Runnable refreshRunnable;
    private final AtomicBoolean registeredObserver;
    
    public RoomTrackingLiveData(final RoomDatabase database, final InvalidationLiveDataContainer container, final boolean inTransaction, final Callable<T> computeFunction, final String[] array) {
        Intrinsics.checkNotNullParameter((Object)database, "database");
        Intrinsics.checkNotNullParameter((Object)container, "container");
        Intrinsics.checkNotNullParameter((Object)computeFunction, "computeFunction");
        Intrinsics.checkNotNullParameter((Object)array, "tableNames");
        this.database = database;
        this.container = container;
        this.inTransaction = inTransaction;
        this.computeFunction = computeFunction;
        this.observer = (InvalidationTracker.Observer)new RoomTrackingLiveData$observer.RoomTrackingLiveData$observer$1(array, this);
        this.invalid = new AtomicBoolean(true);
        this.computing = new AtomicBoolean(false);
        this.registeredObserver = new AtomicBoolean(false);
        this.refreshRunnable = new RoomTrackingLiveData$$ExternalSyntheticLambda0(this);
        this.invalidationRunnable = new RoomTrackingLiveData$$ExternalSyntheticLambda1(this);
    }
    
    private static final void invalidationRunnable$lambda$1(final RoomTrackingLiveData roomTrackingLiveData) {
        Intrinsics.checkNotNullParameter((Object)roomTrackingLiveData, "this$0");
        final boolean hasActiveObservers = roomTrackingLiveData.hasActiveObservers();
        if (roomTrackingLiveData.invalid.compareAndSet(false, true) && hasActiveObservers) {
            roomTrackingLiveData.getQueryExecutor().execute(roomTrackingLiveData.refreshRunnable);
        }
    }
    
    private static final void refreshRunnable$lambda$0(final RoomTrackingLiveData roomTrackingLiveData) {
        Intrinsics.checkNotNullParameter((Object)roomTrackingLiveData, "this$0");
        if (roomTrackingLiveData.registeredObserver.compareAndSet(false, true)) {
            roomTrackingLiveData.database.getInvalidationTracker().addWeakObserver(roomTrackingLiveData.observer);
        }
        int n;
        do {
            if (roomTrackingLiveData.computing.compareAndSet(false, true)) {
                Object call = null;
                n = 0;
                try {
                    while (roomTrackingLiveData.invalid.compareAndSet(true, false)) {
                        try {
                            call = roomTrackingLiveData.computeFunction.call();
                            n = 1;
                            continue;
                        }
                        catch (final Exception ex) {
                            throw new RuntimeException("Exception while computing database live data.", ex);
                        }
                        break;
                    }
                    if (n == 0) {
                        continue;
                    }
                    roomTrackingLiveData.postValue(call);
                    continue;
                }
                finally {
                    roomTrackingLiveData.computing.set(false);
                }
            }
            n = 0;
        } while (n != 0 && roomTrackingLiveData.invalid.get());
    }
    
    public final Callable<T> getComputeFunction() {
        return this.computeFunction;
    }
    
    public final AtomicBoolean getComputing() {
        return this.computing;
    }
    
    public final RoomDatabase getDatabase() {
        return this.database;
    }
    
    public final boolean getInTransaction() {
        return this.inTransaction;
    }
    
    public final AtomicBoolean getInvalid() {
        return this.invalid;
    }
    
    public final Runnable getInvalidationRunnable() {
        return this.invalidationRunnable;
    }
    
    public final InvalidationTracker.Observer getObserver() {
        return this.observer;
    }
    
    public final Executor getQueryExecutor() {
        Executor executor;
        if (this.inTransaction) {
            executor = this.database.getTransactionExecutor();
        }
        else {
            executor = this.database.getQueryExecutor();
        }
        return executor;
    }
    
    public final Runnable getRefreshRunnable() {
        return this.refreshRunnable;
    }
    
    public final AtomicBoolean getRegisteredObserver() {
        return this.registeredObserver;
    }
    
    @Override
    protected void onActive() {
        super.onActive();
        final InvalidationLiveDataContainer container = this.container;
        Intrinsics.checkNotNull((Object)this, "null cannot be cast to non-null type androidx.lifecycle.LiveData<kotlin.Any>");
        container.onActive(this);
        this.getQueryExecutor().execute(this.refreshRunnable);
    }
    
    @Override
    protected void onInactive() {
        super.onInactive();
        final InvalidationLiveDataContainer container = this.container;
        Intrinsics.checkNotNull((Object)this, "null cannot be cast to non-null type androidx.lifecycle.LiveData<kotlin.Any>");
        container.onInactive(this);
    }
}

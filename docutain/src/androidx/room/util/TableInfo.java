// 
// Decompiled by Procyon v0.6.0
// 

package androidx.room.util;

import java.util.Collection;
import androidx.room.Index;
import java.util.ArrayList;
import java.util.List;
import kotlin.annotation.AnnotationRetention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;
import android.os.Build$VERSION;
import kotlin.text.StringsKt;
import java.util.Locale;
import kotlin.Deprecated;
import kotlin.jvm.JvmStatic;
import androidx.sqlite.db.SupportSQLiteDatabase;
import kotlin.collections.SetsKt;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.DefaultConstructorMarker;
import java.util.Set;
import java.util.Map;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000>\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\"\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\b\b\u0007\u0018\u0000 \u00152\u00020\u0001:\u0006\u0014\u0015\u0016\u0017\u0018\u0019B1\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0012\u0010\u0004\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00060\u0005\u0012\f\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\t0\b¢\u0006\u0002\u0010\nBA\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0012\u0010\u0004\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00060\u0005\u0012\f\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\t0\b\u0012\u0010\b\u0002\u0010\u000b\u001a\n\u0012\u0004\u0012\u00020\f\u0018\u00010\b¢\u0006\u0002\u0010\rJ\u0013\u0010\u000e\u001a\u00020\u000f2\b\u0010\u0010\u001a\u0004\u0018\u00010\u0001H\u0096\u0002J\b\u0010\u0011\u001a\u00020\u0012H\u0016J\b\u0010\u0013\u001a\u00020\u0003H\u0016R\u001c\u0010\u0004\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00060\u00058\u0006X\u0087\u0004¢\u0006\u0002\n\u0000R\u0016\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\t0\b8\u0006X\u0087\u0004¢\u0006\u0002\n\u0000R\u0018\u0010\u000b\u001a\n\u0012\u0004\u0012\u00020\f\u0018\u00010\b8\u0006X\u0087\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\u0002\u001a\u00020\u00038\u0006X\u0087\u0004¢\u0006\u0002\n\u0000¨\u0006\u001a" }, d2 = { "Landroidx/room/util/TableInfo;", "", "name", "", "columns", "", "Landroidx/room/util/TableInfo$Column;", "foreignKeys", "", "Landroidx/room/util/TableInfo$ForeignKey;", "(Ljava/lang/String;Ljava/util/Map;Ljava/util/Set;)V", "indices", "Landroidx/room/util/TableInfo$Index;", "(Ljava/lang/String;Ljava/util/Map;Ljava/util/Set;Ljava/util/Set;)V", "equals", "", "other", "hashCode", "", "toString", "Column", "Companion", "CreatedFrom", "ForeignKey", "ForeignKeyWithSequence", "Index", "room-runtime_release" }, k = 1, mv = { 1, 7, 1 }, xi = 48)
public final class TableInfo
{
    public static final int CREATED_FROM_DATABASE = 2;
    public static final int CREATED_FROM_ENTITY = 1;
    public static final int CREATED_FROM_UNKNOWN = 0;
    public static final Companion Companion;
    public final Map<String, Column> columns;
    public final Set<ForeignKey> foreignKeys;
    public final Set<Index> indices;
    public final String name;
    
    static {
        Companion = new Companion(null);
    }
    
    public TableInfo(final String s, final Map<String, Column> map, final Set<ForeignKey> set) {
        Intrinsics.checkNotNullParameter((Object)s, "name");
        Intrinsics.checkNotNullParameter((Object)map, "columns");
        Intrinsics.checkNotNullParameter((Object)set, "foreignKeys");
        this(s, map, set, SetsKt.emptySet());
    }
    
    public TableInfo(final String name, final Map<String, Column> columns, final Set<ForeignKey> foreignKeys, final Set<Index> indices) {
        Intrinsics.checkNotNullParameter((Object)name, "name");
        Intrinsics.checkNotNullParameter((Object)columns, "columns");
        Intrinsics.checkNotNullParameter((Object)foreignKeys, "foreignKeys");
        this.name = name;
        this.columns = columns;
        this.foreignKeys = foreignKeys;
        this.indices = indices;
    }
    
    @JvmStatic
    public static final TableInfo read(final SupportSQLiteDatabase supportSQLiteDatabase, final String s) {
        return TableInfo.Companion.read(supportSQLiteDatabase, s);
    }
    
    @Override
    public boolean equals(final Object o) {
        final boolean b = true;
        if (this == o) {
            return true;
        }
        if (!(o instanceof TableInfo)) {
            return false;
        }
        final String name = this.name;
        final TableInfo tableInfo = (TableInfo)o;
        if (!Intrinsics.areEqual((Object)name, (Object)tableInfo.name)) {
            return false;
        }
        if (!Intrinsics.areEqual((Object)this.columns, (Object)tableInfo.columns)) {
            return false;
        }
        if (!Intrinsics.areEqual((Object)this.foreignKeys, (Object)tableInfo.foreignKeys)) {
            return false;
        }
        final Set<Index> indices = this.indices;
        boolean equal = b;
        if (indices != null) {
            final Set<Index> indices2 = tableInfo.indices;
            if (indices2 == null) {
                equal = b;
            }
            else {
                equal = Intrinsics.areEqual((Object)indices, (Object)indices2);
            }
        }
        return equal;
    }
    
    @Override
    public int hashCode() {
        return (this.name.hashCode() * 31 + this.columns.hashCode()) * 31 + this.foreignKeys.hashCode();
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("TableInfo{name='");
        sb.append(this.name);
        sb.append("', columns=");
        sb.append(this.columns);
        sb.append(", foreignKeys=");
        sb.append(this.foreignKeys);
        sb.append(", indices=");
        sb.append(this.indices);
        sb.append('}');
        return sb.toString();
    }
    
    @Metadata(d1 = { "\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\b\n\u0002\b\u0010\u0018\u0000 \u00172\u00020\u0001:\u0001\u0017B'\b\u0017\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\b¢\u0006\u0002\u0010\tB7\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\b\u0012\b\u0010\n\u001a\u0004\u0018\u00010\u0003\u0012\u0006\u0010\u000b\u001a\u00020\b¢\u0006\u0002\u0010\fJ\u0013\u0010\u0012\u001a\u00020\u00062\b\u0010\u0013\u001a\u0004\u0018\u00010\u0001H\u0096\u0002J\u0012\u0010\u0014\u001a\u00020\b2\b\u0010\u0004\u001a\u0004\u0018\u00010\u0003H\u0003J\b\u0010\u0015\u001a\u00020\bH\u0016J\b\u0010\u0016\u001a\u00020\u0003H\u0016R\u0016\u0010\r\u001a\u00020\b8\u0006X\u0087\u0004¢\u0006\b\n\u0000\u0012\u0004\b\u000e\u0010\u000fR\u0010\u0010\u000b\u001a\u00020\b8\u0006X\u0087\u0004¢\u0006\u0002\n\u0000R\u0012\u0010\n\u001a\u0004\u0018\u00010\u00038\u0006X\u0087\u0004¢\u0006\u0002\n\u0000R\u0011\u0010\u0010\u001a\u00020\u00068F¢\u0006\u0006\u001a\u0004\b\u0010\u0010\u0011R\u0010\u0010\u0002\u001a\u00020\u00038\u0006X\u0087\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\u0005\u001a\u00020\u00068\u0006X\u0087\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\u0007\u001a\u00020\b8\u0006X\u0087\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\u0004\u001a\u00020\u00038\u0006X\u0087\u0004¢\u0006\u0002\n\u0000¨\u0006\u0018" }, d2 = { "Landroidx/room/util/TableInfo$Column;", "", "name", "", "type", "notNull", "", "primaryKeyPosition", "", "(Ljava/lang/String;Ljava/lang/String;ZI)V", "defaultValue", "createdFrom", "(Ljava/lang/String;Ljava/lang/String;ZILjava/lang/String;I)V", "affinity", "getAffinity$annotations", "()V", "isPrimaryKey", "()Z", "equals", "other", "findAffinity", "hashCode", "toString", "Companion", "room-runtime_release" }, k = 1, mv = { 1, 7, 1 }, xi = 48)
    public static final class Column
    {
        public static final Companion Companion;
        public final int affinity;
        public final int createdFrom;
        public final String defaultValue;
        public final String name;
        public final boolean notNull;
        public final int primaryKeyPosition;
        public final String type;
        
        static {
            Companion = new Companion(null);
        }
        
        @Deprecated(message = "Use {@link Column#Column(String, String, boolean, int, String, int)} instead.")
        public Column(final String s, final String s2, final boolean b, final int n) {
            Intrinsics.checkNotNullParameter((Object)s, "name");
            Intrinsics.checkNotNullParameter((Object)s2, "type");
            this(s, s2, b, n, null, 0);
        }
        
        public Column(final String name, final String type, final boolean notNull, final int primaryKeyPosition, final String defaultValue, final int createdFrom) {
            Intrinsics.checkNotNullParameter((Object)name, "name");
            Intrinsics.checkNotNullParameter((Object)type, "type");
            this.name = name;
            this.type = type;
            this.notNull = notNull;
            this.primaryKeyPosition = primaryKeyPosition;
            this.defaultValue = defaultValue;
            this.createdFrom = createdFrom;
            this.affinity = this.findAffinity(type);
        }
        
        @JvmStatic
        public static final boolean defaultValueEquals(final String s, final String s2) {
            return Column.Companion.defaultValueEquals(s, s2);
        }
        
        private final int findAffinity(String upperCase) {
            if (upperCase == null) {
                return 5;
            }
            final Locale us = Locale.US;
            Intrinsics.checkNotNullExpressionValue((Object)us, "US");
            upperCase = upperCase.toUpperCase(us);
            Intrinsics.checkNotNullExpressionValue((Object)upperCase, "this as java.lang.String).toUpperCase(locale)");
            final CharSequence charSequence = upperCase;
            if (StringsKt.contains$default(charSequence, (CharSequence)"INT", false, 2, (Object)null)) {
                return 3;
            }
            if (StringsKt.contains$default(charSequence, (CharSequence)"CHAR", false, 2, (Object)null) || StringsKt.contains$default(charSequence, (CharSequence)"CLOB", false, 2, (Object)null) || StringsKt.contains$default(charSequence, (CharSequence)"TEXT", false, 2, (Object)null)) {
                return 2;
            }
            if (StringsKt.contains$default(charSequence, (CharSequence)"BLOB", false, 2, (Object)null)) {
                return 5;
            }
            if (!StringsKt.contains$default(charSequence, (CharSequence)"REAL", false, 2, (Object)null) && !StringsKt.contains$default(charSequence, (CharSequence)"FLOA", false, 2, (Object)null) && !StringsKt.contains$default(charSequence, (CharSequence)"DOUB", false, 2, (Object)null)) {
                return 1;
            }
            return 4;
        }
        
        @Override
        public boolean equals(final Object o) {
            boolean b = true;
            if (this == o) {
                return true;
            }
            if (!(o instanceof Column)) {
                return false;
            }
            if (Build$VERSION.SDK_INT >= 20) {
                if (this.primaryKeyPosition != ((Column)o).primaryKeyPosition) {
                    return false;
                }
            }
            else if (this.isPrimaryKey() != ((Column)o).isPrimaryKey()) {
                return false;
            }
            final String name = this.name;
            final Column column = (Column)o;
            if (!Intrinsics.areEqual((Object)name, (Object)column.name)) {
                return false;
            }
            if (this.notNull != column.notNull) {
                return false;
            }
            if (this.createdFrom == 1 && column.createdFrom == 2) {
                final String defaultValue = this.defaultValue;
                if (defaultValue != null && !Column.Companion.defaultValueEquals(defaultValue, column.defaultValue)) {
                    return false;
                }
            }
            if (this.createdFrom == 2 && column.createdFrom == 1) {
                final String defaultValue2 = column.defaultValue;
                if (defaultValue2 != null && !Column.Companion.defaultValueEquals(defaultValue2, this.defaultValue)) {
                    return false;
                }
            }
            final int createdFrom = this.createdFrom;
            if (createdFrom != 0 && createdFrom == column.createdFrom) {
                final String defaultValue3 = this.defaultValue;
                if ((defaultValue3 != null) ? (!Column.Companion.defaultValueEquals(defaultValue3, column.defaultValue)) : (column.defaultValue != null)) {
                    return false;
                }
            }
            if (this.affinity != column.affinity) {
                b = false;
            }
            return b;
        }
        
        @Override
        public int hashCode() {
            final int hashCode = this.name.hashCode();
            final int affinity = this.affinity;
            int n;
            if (this.notNull) {
                n = 1231;
            }
            else {
                n = 1237;
            }
            return ((hashCode * 31 + affinity) * 31 + n) * 31 + this.primaryKeyPosition;
        }
        
        public final boolean isPrimaryKey() {
            return this.primaryKeyPosition > 0;
        }
        
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder();
            sb.append("Column{name='");
            sb.append(this.name);
            sb.append("', type='");
            sb.append(this.type);
            sb.append("', affinity='");
            sb.append(this.affinity);
            sb.append("', notNull=");
            sb.append(this.notNull);
            sb.append(", primaryKeyPosition=");
            sb.append(this.primaryKeyPosition);
            sb.append(", defaultValue='");
            String defaultValue;
            if ((defaultValue = this.defaultValue) == null) {
                defaultValue = "undefined";
            }
            sb.append(defaultValue);
            sb.append("'}");
            return sb.toString();
        }
        
        @Metadata(d1 = { "\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0002J\u001a\u0010\u0007\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\b\u0010\b\u001a\u0004\u0018\u00010\u0006H\u0007¨\u0006\t" }, d2 = { "Landroidx/room/util/TableInfo$Column$Companion;", "", "()V", "containsSurroundingParenthesis", "", "current", "", "defaultValueEquals", "other", "room-runtime_release" }, k = 1, mv = { 1, 7, 1 }, xi = 48)
        public static final class Companion
        {
            private Companion() {
            }
            
            private final boolean containsSurroundingParenthesis(final String s) {
                final CharSequence charSequence = s;
                final int length = charSequence.length();
                boolean b = true;
                if (length == 0) {
                    return false;
                }
                int i = 0;
                int n = 0;
                int n3;
                for (int n2 = 0; i < charSequence.length(); ++i, ++n2, n = n3) {
                    final char char1 = charSequence.charAt(i);
                    if (n2 == 0 && char1 != '(') {
                        return false;
                    }
                    if (char1 == '(') {
                        n3 = n + 1;
                    }
                    else {
                        n3 = n;
                        if (char1 == ')') {
                            n3 = --n;
                            if (n == 0) {
                                n3 = n;
                                if (n2 != s.length() - 1) {
                                    return false;
                                }
                            }
                        }
                    }
                }
                if (n != 0) {
                    b = false;
                }
                return b;
            }
            
            @JvmStatic
            public final boolean defaultValueEquals(String substring, final String s) {
                Intrinsics.checkNotNullParameter((Object)substring, "current");
                if (Intrinsics.areEqual((Object)substring, (Object)s)) {
                    return true;
                }
                if (this.containsSurroundingParenthesis(substring)) {
                    substring = substring.substring(1, substring.length() - 1);
                    Intrinsics.checkNotNullExpressionValue((Object)substring, "this as java.lang.String\u2026ing(startIndex, endIndex)");
                    return Intrinsics.areEqual((Object)StringsKt.trim((CharSequence)substring).toString(), (Object)s);
                }
                return false;
            }
        }
    }
    
    @Metadata(d1 = { "\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0018\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\fH\u0007R\u000e\u0010\u0003\u001a\u00020\u0004X\u0086T¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0086T¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004X\u0086T¢\u0006\u0002\n\u0000¨\u0006\r" }, d2 = { "Landroidx/room/util/TableInfo$Companion;", "", "()V", "CREATED_FROM_DATABASE", "", "CREATED_FROM_ENTITY", "CREATED_FROM_UNKNOWN", "read", "Landroidx/room/util/TableInfo;", "database", "Landroidx/sqlite/db/SupportSQLiteDatabase;", "tableName", "", "room-runtime_release" }, k = 1, mv = { 1, 7, 1 }, xi = 48)
    public static final class Companion
    {
        private Companion() {
        }
        
        @JvmStatic
        public final TableInfo read(final SupportSQLiteDatabase supportSQLiteDatabase, final String s) {
            Intrinsics.checkNotNullParameter((Object)supportSQLiteDatabase, "database");
            Intrinsics.checkNotNullParameter((Object)s, "tableName");
            return TableInfoKt.readTableInfo(supportSQLiteDatabase, s);
        }
    }
    
    @Retention(RetentionPolicy.SOURCE)
    @Metadata(d1 = { "\u0000\n\n\u0002\u0018\u0002\n\u0002\u0010\u001b\n\u0000\b\u0081\u0002\u0018\u00002\u00020\u0001B\u0000¨\u0006\u0002" }, d2 = { "Landroidx/room/util/TableInfo$CreatedFrom;", "", "room-runtime_release" }, k = 1, mv = { 1, 7, 1 }, xi = 48)
    @kotlin.annotation.Retention(AnnotationRetention.SOURCE)
    public @interface CreatedFrom {
    }
    
    @Metadata(d1 = { "\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010 \n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\b\u0007\u0018\u00002\u00020\u0001B9\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0003\u0012\f\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00030\u0007\u0012\f\u0010\b\u001a\b\u0012\u0004\u0012\u00020\u00030\u0007¢\u0006\u0002\u0010\tJ\u0013\u0010\n\u001a\u00020\u000b2\b\u0010\f\u001a\u0004\u0018\u00010\u0001H\u0096\u0002J\b\u0010\r\u001a\u00020\u000eH\u0016J\b\u0010\u000f\u001a\u00020\u0003H\u0016R\u0016\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00030\u00078\u0006X\u0087\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\u0004\u001a\u00020\u00038\u0006X\u0087\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\u0005\u001a\u00020\u00038\u0006X\u0087\u0004¢\u0006\u0002\n\u0000R\u0016\u0010\b\u001a\b\u0012\u0004\u0012\u00020\u00030\u00078\u0006X\u0087\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\u0002\u001a\u00020\u00038\u0006X\u0087\u0004¢\u0006\u0002\n\u0000¨\u0006\u0010" }, d2 = { "Landroidx/room/util/TableInfo$ForeignKey;", "", "referenceTable", "", "onDelete", "onUpdate", "columnNames", "", "referenceColumnNames", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;)V", "equals", "", "other", "hashCode", "", "toString", "room-runtime_release" }, k = 1, mv = { 1, 7, 1 }, xi = 48)
    public static final class ForeignKey
    {
        public final List<String> columnNames;
        public final String onDelete;
        public final String onUpdate;
        public final List<String> referenceColumnNames;
        public final String referenceTable;
        
        public ForeignKey(final String referenceTable, final String onDelete, final String onUpdate, final List<String> columnNames, final List<String> referenceColumnNames) {
            Intrinsics.checkNotNullParameter((Object)referenceTable, "referenceTable");
            Intrinsics.checkNotNullParameter((Object)onDelete, "onDelete");
            Intrinsics.checkNotNullParameter((Object)onUpdate, "onUpdate");
            Intrinsics.checkNotNullParameter((Object)columnNames, "columnNames");
            Intrinsics.checkNotNullParameter((Object)referenceColumnNames, "referenceColumnNames");
            this.referenceTable = referenceTable;
            this.onDelete = onDelete;
            this.onUpdate = onUpdate;
            this.columnNames = columnNames;
            this.referenceColumnNames = referenceColumnNames;
        }
        
        @Override
        public boolean equals(final Object o) {
            if (this == o) {
                return true;
            }
            final boolean b = o instanceof ForeignKey;
            boolean equal = false;
            if (!b) {
                return false;
            }
            final String referenceTable = this.referenceTable;
            final ForeignKey foreignKey = (ForeignKey)o;
            if (!Intrinsics.areEqual((Object)referenceTable, (Object)foreignKey.referenceTable)) {
                return false;
            }
            if (!Intrinsics.areEqual((Object)this.onDelete, (Object)foreignKey.onDelete)) {
                return false;
            }
            if (!Intrinsics.areEqual((Object)this.onUpdate, (Object)foreignKey.onUpdate)) {
                return false;
            }
            if (Intrinsics.areEqual((Object)this.columnNames, (Object)foreignKey.columnNames)) {
                equal = Intrinsics.areEqual((Object)this.referenceColumnNames, (Object)foreignKey.referenceColumnNames);
            }
            return equal;
        }
        
        @Override
        public int hashCode() {
            return (((this.referenceTable.hashCode() * 31 + this.onDelete.hashCode()) * 31 + this.onUpdate.hashCode()) * 31 + this.columnNames.hashCode()) * 31 + this.referenceColumnNames.hashCode();
        }
        
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder();
            sb.append("ForeignKey{referenceTable='");
            sb.append(this.referenceTable);
            sb.append("', onDelete='");
            sb.append(this.onDelete);
            sb.append(" +', onUpdate='");
            sb.append(this.onUpdate);
            sb.append("', columnNames=");
            sb.append(this.columnNames);
            sb.append(", referenceColumnNames=");
            sb.append(this.referenceColumnNames);
            sb.append('}');
            return sb.toString();
        }
    }
    
    @Metadata(d1 = { "\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u000f\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u000b\b\u0000\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B%\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0006¢\u0006\u0002\u0010\bJ\u0011\u0010\u000f\u001a\u00020\u00032\u0006\u0010\u0010\u001a\u00020\u0000H\u0096\u0002R\u0011\u0010\u0005\u001a\u00020\u0006¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0011\u0010\u0004\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\fR\u0011\u0010\u0007\u001a\u00020\u0006¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\n¨\u0006\u0011" }, d2 = { "Landroidx/room/util/TableInfo$ForeignKeyWithSequence;", "", "id", "", "sequence", "from", "", "to", "(IILjava/lang/String;Ljava/lang/String;)V", "getFrom", "()Ljava/lang/String;", "getId", "()I", "getSequence", "getTo", "compareTo", "other", "room-runtime_release" }, k = 1, mv = { 1, 7, 1 }, xi = 48)
    public static final class ForeignKeyWithSequence implements Comparable<ForeignKeyWithSequence>
    {
        private final String from;
        private final int id;
        private final int sequence;
        private final String to;
        
        public ForeignKeyWithSequence(final int id, final int sequence, final String from, final String to) {
            Intrinsics.checkNotNullParameter((Object)from, "from");
            Intrinsics.checkNotNullParameter((Object)to, "to");
            this.id = id;
            this.sequence = sequence;
            this.from = from;
            this.to = to;
        }
        
        @Override
        public int compareTo(final ForeignKeyWithSequence foreignKeyWithSequence) {
            Intrinsics.checkNotNullParameter((Object)foreignKeyWithSequence, "other");
            int n;
            if ((n = this.id - foreignKeyWithSequence.id) == 0) {
                n = this.sequence - foreignKeyWithSequence.sequence;
            }
            return n;
        }
        
        public final String getFrom() {
            return this.from;
        }
        
        public final int getId() {
            return this.id;
        }
        
        public final int getSequence() {
            return this.sequence;
        }
        
        public final String getTo() {
            return this.to;
        }
    }
    
    @Metadata(d1 = { "\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010 \n\u0002\b\u0006\n\u0002\u0010\b\n\u0002\b\u0003\b\u0007\u0018\u0000 \u00102\u00020\u0001:\u0001\u0010B%\b\u0017\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\f\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00030\u0007¢\u0006\u0002\u0010\bB1\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\f\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00030\u0007\u0012\f\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u00030\u0007¢\u0006\u0002\u0010\nJ\u0013\u0010\u000b\u001a\u00020\u00052\b\u0010\f\u001a\u0004\u0018\u00010\u0001H\u0096\u0002J\b\u0010\r\u001a\u00020\u000eH\u0016J\b\u0010\u000f\u001a\u00020\u0003H\u0016R\u0016\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00030\u00078\u0006X\u0087\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\u0002\u001a\u00020\u00038\u0006X\u0087\u0004¢\u0006\u0002\n\u0000R\u0018\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u00030\u00078\u0006@\u0006X\u0087\u000e¢\u0006\u0002\n\u0000R\u0010\u0010\u0004\u001a\u00020\u00058\u0006X\u0087\u0004¢\u0006\u0002\n\u0000¨\u0006\u0011" }, d2 = { "Landroidx/room/util/TableInfo$Index;", "", "name", "", "unique", "", "columns", "", "(Ljava/lang/String;ZLjava/util/List;)V", "orders", "(Ljava/lang/String;ZLjava/util/List;Ljava/util/List;)V", "equals", "other", "hashCode", "", "toString", "Companion", "room-runtime_release" }, k = 1, mv = { 1, 7, 1 }, xi = 48)
    public static final class Index
    {
        public static final Companion Companion;
        public static final String DEFAULT_PREFIX = "index_";
        public final List<String> columns;
        public final String name;
        public List<String> orders;
        public final boolean unique;
        
        static {
            Companion = new Companion(null);
        }
        
        @Deprecated(message = "Use {@link #Index(String, boolean, List, List)}")
        public Index(final String s, final boolean b, final List<String> list) {
            Intrinsics.checkNotNullParameter((Object)s, "name");
            Intrinsics.checkNotNullParameter((Object)list, "columns");
            final int size = list.size();
            final ArrayList list2 = new ArrayList<String>(size);
            for (int i = 0; i < size; ++i) {
                list2.add(androidx.room.Index.Order.ASC.name());
            }
            this(s, b, list, (List<String>)list2);
        }
        
        public Index(final String name, final boolean unique, final List<String> columns, final List<String> orders) {
            Intrinsics.checkNotNullParameter((Object)name, "name");
            Intrinsics.checkNotNullParameter((Object)columns, "columns");
            Intrinsics.checkNotNullParameter((Object)orders, "orders");
            this.name = name;
            this.unique = unique;
            this.columns = columns;
            this.orders = orders;
            Collection collection;
            if ((collection = orders).isEmpty()) {
                final int size = columns.size();
                final ArrayList list = new ArrayList<String>(size);
                for (int i = 0; i < size; ++i) {
                    list.add(androidx.room.Index.Order.ASC.name());
                }
                collection = list;
            }
            this.orders = (List<String>)collection;
        }
        
        @Override
        public boolean equals(final Object o) {
            if (this == o) {
                return true;
            }
            if (!(o instanceof Index)) {
                return false;
            }
            final boolean unique = this.unique;
            final Index index = (Index)o;
            if (unique != index.unique) {
                return false;
            }
            if (!Intrinsics.areEqual((Object)this.columns, (Object)index.columns)) {
                return false;
            }
            if (!Intrinsics.areEqual((Object)this.orders, (Object)index.orders)) {
                return false;
            }
            boolean b;
            if (StringsKt.startsWith$default(this.name, "index_", false, 2, (Object)null)) {
                b = StringsKt.startsWith$default(index.name, "index_", false, 2, (Object)null);
            }
            else {
                b = Intrinsics.areEqual((Object)this.name, (Object)index.name);
            }
            return b;
        }
        
        @Override
        public int hashCode() {
            int hashCode;
            if (StringsKt.startsWith$default(this.name, "index_", false, 2, (Object)null)) {
                hashCode = -1184239155;
            }
            else {
                hashCode = this.name.hashCode();
            }
            return ((hashCode * 31 + (this.unique ? 1 : 0)) * 31 + this.columns.hashCode()) * 31 + this.orders.hashCode();
        }
        
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder();
            sb.append("Index{name='");
            sb.append(this.name);
            sb.append("', unique=");
            sb.append(this.unique);
            sb.append(", columns=");
            sb.append(this.columns);
            sb.append(", orders=");
            sb.append(this.orders);
            sb.append("'}");
            return sb.toString();
        }
        
        @Metadata(d1 = { "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0086T¢\u0006\u0002\n\u0000¨\u0006\u0005" }, d2 = { "Landroidx/room/util/TableInfo$Index$Companion;", "", "()V", "DEFAULT_PREFIX", "", "room-runtime_release" }, k = 1, mv = { 1, 7, 1 }, xi = 48)
        public static final class Companion
        {
            private Companion() {
            }
        }
    }
}

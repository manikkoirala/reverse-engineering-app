// 
// Decompiled by Procyon v0.6.0
// 

package androidx.room.util;

import java.util.Iterator;
import kotlin.collections.CollectionsKt;
import java.util.Collection;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.List;
import kotlin.text.StringsKt;
import java.io.Closeable;
import kotlin.io.CloseableKt;
import kotlin.Unit;
import android.database.Cursor;
import android.os.Build$VERSION;
import kotlin.collections.SetsKt;
import androidx.sqlite.db.SupportSQLiteDatabase;
import kotlin.jvm.JvmStatic;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.DefaultConstructorMarker;
import java.util.Set;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\"\n\u0002\b\u0005\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\b\u0007\u0018\u0000 \u00102\u00020\u0001:\u0001\u0010B%\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0003¢\u0006\u0002\u0010\u0007B)\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u0005\u0012\f\u0010\b\u001a\b\u0012\u0004\u0012\u00020\u00030\u0005¢\u0006\u0002\u0010\tJ\u0013\u0010\n\u001a\u00020\u000b2\b\u0010\f\u001a\u0004\u0018\u00010\u0001H\u0096\u0002J\b\u0010\r\u001a\u00020\u000eH\u0016J\b\u0010\u000f\u001a\u00020\u0003H\u0016R\u0016\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u00058\u0006X\u0087\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\u0002\u001a\u00020\u00038\u0006X\u0087\u0004¢\u0006\u0002\n\u0000R\u0016\u0010\b\u001a\b\u0012\u0004\u0012\u00020\u00030\u00058\u0006X\u0087\u0004¢\u0006\u0002\n\u0000¨\u0006\u0011" }, d2 = { "Landroidx/room/util/FtsTableInfo;", "", "name", "", "columns", "", "createSql", "(Ljava/lang/String;Ljava/util/Set;Ljava/lang/String;)V", "options", "(Ljava/lang/String;Ljava/util/Set;Ljava/util/Set;)V", "equals", "", "other", "hashCode", "", "toString", "Companion", "room-runtime_release" }, k = 1, mv = { 1, 7, 1 }, xi = 48)
public final class FtsTableInfo
{
    public static final Companion Companion;
    private static final String[] FTS_OPTIONS;
    public final Set<String> columns;
    public final String name;
    public final Set<String> options;
    
    static {
        Companion = new Companion(null);
        FTS_OPTIONS = new String[] { "tokenize=", "compress=", "content=", "languageid=", "matchinfo=", "notindexed=", "order=", "prefix=", "uncompress=" };
    }
    
    public FtsTableInfo(final String s, final Set<String> set, final String s2) {
        Intrinsics.checkNotNullParameter((Object)s, "name");
        Intrinsics.checkNotNullParameter((Object)set, "columns");
        Intrinsics.checkNotNullParameter((Object)s2, "createSql");
        this(s, set, FtsTableInfo.Companion.parseOptions(s2));
    }
    
    public FtsTableInfo(final String name, final Set<String> columns, final Set<String> options) {
        Intrinsics.checkNotNullParameter((Object)name, "name");
        Intrinsics.checkNotNullParameter((Object)columns, "columns");
        Intrinsics.checkNotNullParameter((Object)options, "options");
        this.name = name;
        this.columns = columns;
        this.options = options;
    }
    
    public static final /* synthetic */ String[] access$getFTS_OPTIONS$cp() {
        return FtsTableInfo.FTS_OPTIONS;
    }
    
    @JvmStatic
    public static final Set<String> parseOptions(final String s) {
        return FtsTableInfo.Companion.parseOptions(s);
    }
    
    @JvmStatic
    public static final FtsTableInfo read(final SupportSQLiteDatabase supportSQLiteDatabase, final String s) {
        return FtsTableInfo.Companion.read(supportSQLiteDatabase, s);
    }
    
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof FtsTableInfo)) {
            return false;
        }
        final String name = this.name;
        final FtsTableInfo ftsTableInfo = (FtsTableInfo)o;
        return Intrinsics.areEqual((Object)name, (Object)ftsTableInfo.name) && Intrinsics.areEqual((Object)this.columns, (Object)ftsTableInfo.columns) && Intrinsics.areEqual((Object)this.options, (Object)ftsTableInfo.options);
    }
    
    @Override
    public int hashCode() {
        return (this.name.hashCode() * 31 + this.columns.hashCode()) * 31 + this.options.hashCode();
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("FtsTableInfo{name='");
        sb.append(this.name);
        sb.append("', columns=");
        sb.append(this.columns);
        sb.append(", options=");
        sb.append(this.options);
        sb.append("'}");
        return sb.toString();
    }
    
    @Metadata(d1 = { "\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0011\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\"\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0016\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00050\b2\u0006\u0010\t\u001a\u00020\u0005H\u0007J\u0018\u0010\n\u001a\u00020\u000b2\u0006\u0010\f\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u0005H\u0007J\u001e\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u00050\b2\u0006\u0010\f\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u0005H\u0002J\u001e\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u00050\b2\u0006\u0010\f\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u0005H\u0002R\u0016\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004X\u0082\u0004¢\u0006\u0004\n\u0002\u0010\u0006¨\u0006\u0011" }, d2 = { "Landroidx/room/util/FtsTableInfo$Companion;", "", "()V", "FTS_OPTIONS", "", "", "[Ljava/lang/String;", "parseOptions", "", "createStatement", "read", "Landroidx/room/util/FtsTableInfo;", "database", "Landroidx/sqlite/db/SupportSQLiteDatabase;", "tableName", "readColumns", "readOptions", "room-runtime_release" }, k = 1, mv = { 1, 7, 1 }, xi = 48)
    public static final class Companion
    {
        private Companion() {
        }
        
        private final Set<String> readColumns(SupportSQLiteDatabase query, String string) {
            final Set setBuilder = SetsKt.createSetBuilder();
            final StringBuilder sb = new StringBuilder();
            sb.append("PRAGMA table_info(`");
            sb.append(string);
            sb.append("`)");
            query = (SupportSQLiteDatabase)query.query(sb.toString());
            Label_0211: {
                if (Build$VERSION.SDK_INT > 15) {
                    query = query;
                    try {
                        final Cursor cursor = (Cursor)query;
                        if (cursor.getColumnCount() > 0) {
                            final int columnIndex = cursor.getColumnIndex("name");
                            while (cursor.moveToNext()) {
                                final String string2 = cursor.getString(columnIndex);
                                Intrinsics.checkNotNullExpressionValue((Object)string2, "cursor.getString(nameIndex)");
                                setBuilder.add(string2);
                            }
                        }
                        final Unit instance = Unit.INSTANCE;
                        CloseableKt.closeFinally((Closeable)query, (Throwable)null);
                        break Label_0211;
                    }
                    finally {
                        try {}
                        finally {
                            CloseableKt.closeFinally((Closeable)query, (Throwable)string);
                        }
                    }
                }
                try {
                    if (((Cursor)query).getColumnCount() > 0) {
                        final int columnIndex2 = ((Cursor)query).getColumnIndex("name");
                        while (((Cursor)query).moveToNext()) {
                            string = ((Cursor)query).getString(columnIndex2);
                            Intrinsics.checkNotNullExpressionValue((Object)string, "cursor.getString(nameIndex)");
                            setBuilder.add(string);
                        }
                    }
                    final Unit instance2 = Unit.INSTANCE;
                    ((Cursor)query).close();
                    return SetsKt.build(setBuilder);
                }
                finally {
                    ((Cursor)query).close();
                }
            }
        }
        
        private final Set<String> readOptions(final SupportSQLiteDatabase supportSQLiteDatabase, String query) {
            final StringBuilder sb = new StringBuilder();
            sb.append("SELECT * FROM sqlite_master WHERE `name` = '");
            sb.append(query);
            sb.append('\'');
            query = (String)supportSQLiteDatabase.query(sb.toString());
            final int sdk_INT = Build$VERSION.SDK_INT;
            String string = "";
            Label_0147: {
                if (sdk_INT > 15) {
                    query = query;
                    try {
                        final Cursor cursor = (Cursor)query;
                        if (cursor.moveToFirst()) {
                            cursor.getString(cursor.getColumnIndexOrThrow("sql"));
                        }
                        CloseableKt.closeFinally((Closeable)query, (Throwable)null);
                        break Label_0147;
                    }
                    finally {
                        try {}
                        finally {
                            final Throwable t;
                            CloseableKt.closeFinally((Closeable)query, t);
                        }
                    }
                }
                try {
                    if (((Cursor)query).moveToFirst()) {
                        string = ((Cursor)query).getString(((Cursor)query).getColumnIndexOrThrow("sql"));
                    }
                    ((Cursor)query).close();
                    Intrinsics.checkNotNullExpressionValue((Object)string, "sql");
                    return this.parseOptions(string);
                }
                finally {
                    ((Cursor)query).close();
                }
            }
        }
        
        @JvmStatic
        public final Set<String> parseOptions(final String s) {
            Intrinsics.checkNotNullParameter((Object)s, "createStatement");
            final CharSequence charSequence = s;
            if (charSequence.length() == 0) {
                return SetsKt.emptySet();
            }
            final String substring = s.substring(StringsKt.indexOf$default(charSequence, '(', 0, false, 6, (Object)null) + 1, StringsKt.lastIndexOf$default(charSequence, ')', 0, false, 6, (Object)null));
            Intrinsics.checkNotNullExpressionValue((Object)substring, "this as java.lang.String\u2026ing(startIndex, endIndex)");
            final List list = new ArrayList();
            final ArrayDeque arrayDeque = new ArrayDeque();
            final CharSequence charSequence2 = substring;
            int n = -1;
            int n2;
            for (int i = 0, endIndex = 0; i < charSequence2.length(); ++i, ++endIndex, n = n2) {
                final char char1 = charSequence2.charAt(i);
                if (char1 == '\'' || char1 == '\"' || char1 == '`') {
                    if (arrayDeque.isEmpty()) {
                        arrayDeque.push(char1);
                        n2 = n;
                    }
                    else {
                        final Character c = (Character)arrayDeque.peek();
                        if (c == null) {
                            n2 = n;
                        }
                        else {
                            n2 = n;
                            if (c == char1) {
                                arrayDeque.pop();
                                n2 = n;
                            }
                        }
                    }
                }
                else if (char1 == '[') {
                    n2 = n;
                    if (arrayDeque.isEmpty()) {
                        arrayDeque.push(char1);
                        n2 = n;
                    }
                }
                else if (char1 == ']') {
                    n2 = n;
                    if (!arrayDeque.isEmpty()) {
                        final Character c2 = (Character)arrayDeque.peek();
                        if (c2 == null) {
                            n2 = n;
                        }
                        else {
                            n2 = n;
                            if (c2 == '[') {
                                arrayDeque.pop();
                                n2 = n;
                            }
                        }
                    }
                }
                else {
                    n2 = n;
                    if (char1 == ',') {
                        n2 = n;
                        if (arrayDeque.isEmpty()) {
                            final String substring2 = substring.substring(n + 1, endIndex);
                            Intrinsics.checkNotNullExpressionValue((Object)substring2, "this as java.lang.String\u2026ing(startIndex, endIndex)");
                            final CharSequence charSequence3 = substring2;
                            int n3 = charSequence3.length() - 1;
                            int j = 0;
                            int n4 = 0;
                            while (j <= n3) {
                                int n5;
                                if (n4 == 0) {
                                    n5 = j;
                                }
                                else {
                                    n5 = n3;
                                }
                                final boolean b = Intrinsics.compare((int)charSequence3.charAt(n5), 32) <= 0;
                                if (n4 == 0) {
                                    if (!b) {
                                        n4 = 1;
                                    }
                                    else {
                                        ++j;
                                    }
                                }
                                else {
                                    if (!b) {
                                        break;
                                    }
                                    --n3;
                                }
                            }
                            list.add(charSequence3.subSequence(j, n3 + 1).toString());
                            n2 = endIndex;
                        }
                    }
                }
            }
            final String substring3 = substring.substring(n + 1);
            Intrinsics.checkNotNullExpressionValue((Object)substring3, "this as java.lang.String).substring(startIndex)");
            list.add(StringsKt.trim((CharSequence)substring3).toString());
            final Iterable iterable = list;
            final Collection collection = new ArrayList();
        Label_0609:
            for (final Object next : iterable) {
                final String s2 = (String)next;
                final String[] access$getFTS_OPTIONS$cp = FtsTableInfo.access$getFTS_OPTIONS$cp();
                final int length = access$getFTS_OPTIONS$cp.length;
                int k = 0;
                while (true) {
                    while (k < length) {
                        if (StringsKt.startsWith$default(s2, access$getFTS_OPTIONS$cp[k], false, 2, (Object)null)) {
                            final boolean b2 = true;
                            if (b2) {
                                collection.add(next);
                                continue Label_0609;
                            }
                            continue Label_0609;
                        }
                        else {
                            ++k;
                        }
                    }
                    final boolean b2 = false;
                    continue;
                }
            }
            return CollectionsKt.toSet((Iterable)collection);
        }
        
        @JvmStatic
        public final FtsTableInfo read(final SupportSQLiteDatabase supportSQLiteDatabase, final String s) {
            Intrinsics.checkNotNullParameter((Object)supportSQLiteDatabase, "database");
            Intrinsics.checkNotNullParameter((Object)s, "tableName");
            return new FtsTableInfo(s, this.readColumns(supportSQLiteDatabase, s), this.readOptions(supportSQLiteDatabase, s));
        }
    }
}

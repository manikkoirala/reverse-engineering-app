// 
// Decompiled by Procyon v0.6.0
// 

package androidx.room.util;

import java.nio.ByteBuffer;
import kotlin.jvm.internal.Intrinsics;
import java.util.UUID;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000\u0010\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0012\n\u0002\b\u0003\u001a\u000e\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003\u001a\u000e\u0010\u0004\u001a\u00020\u00032\u0006\u0010\u0005\u001a\u00020\u0001¨\u0006\u0006" }, d2 = { "convertByteToUUID", "Ljava/util/UUID;", "bytes", "", "convertUUIDToByte", "uuid", "room-runtime_release" }, k = 2, mv = { 1, 7, 1 }, xi = 48)
public final class UUIDUtil
{
    public static final UUID convertByteToUUID(final byte[] array) {
        Intrinsics.checkNotNullParameter((Object)array, "bytes");
        final ByteBuffer wrap = ByteBuffer.wrap(array);
        return new UUID(wrap.getLong(), wrap.getLong());
    }
    
    public static final byte[] convertUUIDToByte(final UUID uuid) {
        Intrinsics.checkNotNullParameter((Object)uuid, "uuid");
        final ByteBuffer wrap = ByteBuffer.wrap(new byte[16]);
        wrap.putLong(uuid.getMostSignificantBits());
        wrap.putLong(uuid.getLeastSignificantBits());
        final byte[] array = wrap.array();
        Intrinsics.checkNotNullExpressionValue((Object)array, "buffer.array()");
        return array;
    }
}

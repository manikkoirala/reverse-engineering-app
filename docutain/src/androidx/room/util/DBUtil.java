// 
// Decompiled by Procyon v0.6.0
// 

package androidx.room.util;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.io.FileInputStream;
import java.io.File;
import android.database.AbstractWindowedCursor;
import kotlin.Deprecated;
import androidx.sqlite.db.SupportSQLiteQuery;
import androidx.room.RoomDatabase;
import java.util.LinkedHashMap;
import java.util.Map;
import android.database.sqlite.SQLiteConstraintException;
import java.util.Iterator;
import java.util.List;
import kotlin.text.StringsKt;
import kotlin.io.CloseableKt;
import kotlin.Unit;
import android.database.Cursor;
import java.io.Closeable;
import kotlin.collections.CollectionsKt;
import kotlin.jvm.internal.Intrinsics;
import androidx.sqlite.db.SupportSQLiteDatabase;
import androidx.sqlite.db.SupportSQLiteCompat;
import android.os.Build$VERSION;
import android.os.CancellationSignal;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000D\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0000\u001a\b\u0010\u0000\u001a\u0004\u0018\u00010\u0001\u001a\u000e\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005\u001a\u0016\u0010\u0006\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0007\u001a\u00020\b\u001a\u0010\u0010\t\u001a\u00020\b2\u0006\u0010\n\u001a\u00020\u000bH\u0002\u001a \u0010\f\u001a\u00020\u000b2\u0006\u0010\u0004\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u0011H\u0007\u001a(\u0010\f\u001a\u00020\u000b2\u0006\u0010\u0004\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u00112\b\u0010\u0012\u001a\u0004\u0018\u00010\u0001\u001a\u000e\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u0016¨\u0006\u0017" }, d2 = { "createCancellationSignal", "Landroid/os/CancellationSignal;", "dropFtsSyncTriggers", "", "db", "Landroidx/sqlite/db/SupportSQLiteDatabase;", "foreignKeyCheck", "tableName", "", "processForeignKeyCheckFailure", "cursor", "Landroid/database/Cursor;", "query", "Landroidx/room/RoomDatabase;", "sqLiteQuery", "Landroidx/sqlite/db/SupportSQLiteQuery;", "maybeCopy", "", "signal", "readVersion", "", "databaseFile", "Ljava/io/File;", "room-runtime_release" }, k = 2, mv = { 1, 7, 1 }, xi = 48)
public final class DBUtil
{
    public static final CancellationSignal createCancellationSignal() {
        CancellationSignal cancellationSignal;
        if (Build$VERSION.SDK_INT >= 16) {
            cancellationSignal = SupportSQLiteCompat.Api16Impl.createCancellationSignal();
        }
        else {
            cancellationSignal = null;
            final CancellationSignal cancellationSignal2 = null;
        }
        return cancellationSignal;
    }
    
    public static final void dropFtsSyncTriggers(final SupportSQLiteDatabase supportSQLiteDatabase) {
        Intrinsics.checkNotNullParameter((Object)supportSQLiteDatabase, "db");
        final List listBuilder = CollectionsKt.createListBuilder();
        Object query = supportSQLiteDatabase.query("SELECT name FROM sqlite_master WHERE type = 'trigger'");
        Label_0122: {
            if (Build$VERSION.SDK_INT > 15) {
                query = query;
                try {
                    final Cursor cursor = (Cursor)query;
                    while (cursor.moveToNext()) {
                        listBuilder.add(cursor.getString(0));
                    }
                    final Unit instance = Unit.INSTANCE;
                    CloseableKt.closeFinally((Closeable)query, (Throwable)null);
                    break Label_0122;
                }
                finally {
                    try {}
                    finally {
                        CloseableKt.closeFinally((Closeable)query, (Throwable)supportSQLiteDatabase);
                    }
                }
            }
            try {
                while (((Cursor)query).moveToNext()) {
                    listBuilder.add(((Cursor)query).getString(0));
                }
                final Unit instance2 = Unit.INSTANCE;
                ((Cursor)query).close();
                for (final String str : CollectionsKt.build(listBuilder)) {
                    Intrinsics.checkNotNullExpressionValue((Object)str, "triggerName");
                    if (StringsKt.startsWith$default(str, "room_fts_content_sync_", false, 2, (Object)null)) {
                        query = new StringBuilder();
                        ((StringBuilder)query).append("DROP TRIGGER IF EXISTS ");
                        ((StringBuilder)query).append(str);
                        supportSQLiteDatabase.execSQL(((StringBuilder)query).toString());
                    }
                }
            }
            finally {
                ((Cursor)query).close();
            }
        }
    }
    
    public static final void foreignKeyCheck(SupportSQLiteDatabase query, String processForeignKeyCheckFailure) {
        Intrinsics.checkNotNullParameter((Object)query, "db");
        Intrinsics.checkNotNullParameter((Object)processForeignKeyCheckFailure, "tableName");
        final StringBuilder sb = new StringBuilder();
        sb.append("PRAGMA foreign_key_check(`");
        sb.append(processForeignKeyCheckFailure);
        sb.append("`)");
        query = (SupportSQLiteDatabase)query.query(sb.toString());
        if (Build$VERSION.SDK_INT > 15) {
            query = query;
            try {
                final Cursor cursor = (Cursor)query;
                if (cursor.getCount() <= 0) {
                    final Unit instance = Unit.INSTANCE;
                    CloseableKt.closeFinally((Closeable)query, (Throwable)null);
                    return;
                }
                throw new SQLiteConstraintException(processForeignKeyCheckFailure(cursor));
            }
            finally {
                try {}
                finally {
                    CloseableKt.closeFinally((Closeable)query, (Throwable)processForeignKeyCheckFailure);
                }
            }
        }
        try {
            if (((Cursor)query).getCount() <= 0) {
                final Unit instance2 = Unit.INSTANCE;
                return;
            }
            processForeignKeyCheckFailure = processForeignKeyCheckFailure((Cursor)query);
            throw new SQLiteConstraintException(processForeignKeyCheckFailure);
        }
        finally {
            ((Cursor)query).close();
        }
    }
    
    private static final String processForeignKeyCheckFailure(final Cursor cursor) {
        final StringBuilder sb = new StringBuilder();
        final int count = cursor.getCount();
        final Map map = new LinkedHashMap();
        while (cursor.moveToNext()) {
            if (cursor.isFirst()) {
                sb.append("Foreign key violation(s) detected in '");
                sb.append(cursor.getString(0));
                sb.append("'.\n");
            }
            final String string = cursor.getString(3);
            if (!map.containsKey(string)) {
                Intrinsics.checkNotNullExpressionValue((Object)string, "constraintIndex");
                final String string2 = cursor.getString(2);
                Intrinsics.checkNotNullExpressionValue((Object)string2, "cursor.getString(2)");
                map.put(string, string2);
            }
        }
        sb.append("Number of different violations discovered: ");
        sb.append(map.keySet().size());
        sb.append("\n");
        sb.append("Number of rows in violation: ");
        sb.append(count);
        sb.append("\n");
        sb.append("Violation(s) detected in the following constraint(s):\n");
        for (final Map.Entry<String, V> entry : map.entrySet()) {
            final String str = entry.getKey();
            final String str2 = (String)entry.getValue();
            sb.append("\tParent Table = ");
            sb.append(str2);
            sb.append(", Foreign Key Constraint Index = ");
            sb.append(str);
            sb.append("\n");
        }
        final String string3 = sb.toString();
        Intrinsics.checkNotNullExpressionValue((Object)string3, "StringBuilder().apply(builderAction).toString()");
        return string3;
    }
    
    @Deprecated(message = "This is only used in the generated code and shouldn't be called directly.")
    public static final Cursor query(final RoomDatabase roomDatabase, final SupportSQLiteQuery supportSQLiteQuery, final boolean b) {
        Intrinsics.checkNotNullParameter((Object)roomDatabase, "db");
        Intrinsics.checkNotNullParameter((Object)supportSQLiteQuery, "sqLiteQuery");
        return query(roomDatabase, supportSQLiteQuery, b, null);
    }
    
    public static final Cursor query(final RoomDatabase roomDatabase, final SupportSQLiteQuery supportSQLiteQuery, final boolean b, final CancellationSignal cancellationSignal) {
        Intrinsics.checkNotNullParameter((Object)roomDatabase, "db");
        Intrinsics.checkNotNullParameter((Object)supportSQLiteQuery, "sqLiteQuery");
        Cursor cursor2;
        final Cursor cursor = cursor2 = roomDatabase.query(supportSQLiteQuery, cancellationSignal);
        if (b) {
            cursor2 = cursor;
            if (cursor instanceof AbstractWindowedCursor) {
                final AbstractWindowedCursor abstractWindowedCursor = (AbstractWindowedCursor)cursor;
                final int count = abstractWindowedCursor.getCount();
                int numRows;
                if (abstractWindowedCursor.hasWindow()) {
                    numRows = abstractWindowedCursor.getWindow().getNumRows();
                }
                else {
                    numRows = count;
                }
                if (Build$VERSION.SDK_INT >= 23) {
                    cursor2 = cursor;
                    if (numRows >= count) {
                        return cursor2;
                    }
                }
                cursor2 = CursorUtil.copyAndClose(cursor);
            }
        }
        return cursor2;
    }
    
    public static final int readVersion(File file) throws IOException {
        Intrinsics.checkNotNullParameter((Object)file, "databaseFile");
        file = (File)new FileInputStream(file).getChannel();
        try {
            final FileChannel fileChannel = (FileChannel)file;
            final ByteBuffer allocate = ByteBuffer.allocate(4);
            fileChannel.tryLock(60L, 4L, true);
            fileChannel.position(60L);
            if (fileChannel.read(allocate) == 4) {
                allocate.rewind();
                final int int1 = allocate.getInt();
                CloseableKt.closeFinally((Closeable)file, (Throwable)null);
                return int1;
            }
            throw new IOException("Bad database header, unable to read 4 bytes at offset 60");
        }
        finally {
            try {}
            finally {
                final Throwable t;
                CloseableKt.closeFinally((Closeable)file, t);
            }
        }
    }
}

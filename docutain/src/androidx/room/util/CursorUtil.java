// 
// Decompiled by Procyon v0.6.0
// 

package androidx.room.util;

import kotlin.jvm.internal.InlineMarker;
import android.util.Log;
import kotlin.jvm.functions.Function1;
import kotlin.collections.ArraysKt;
import kotlin.text.StringsKt;
import kotlin.io.CloseableKt;
import android.database.MatrixCursor;
import java.io.Closeable;
import android.os.Build$VERSION;
import kotlin.jvm.internal.Intrinsics;
import android.database.Cursor;
import kotlin.Metadata;

@Metadata(d1 = { "\u00000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0011\n\u0002\b\u0005\n\u0002\u0010\u0015\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\u001a\u000e\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0001\u001a\u0018\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00012\u0006\u0010\u0006\u001a\u00020\u0007H\u0002\u001a#\u0010\u0003\u001a\u00020\u00042\f\u0010\b\u001a\b\u0012\u0004\u0012\u00020\u00070\t2\u0006\u0010\u0006\u001a\u00020\u0007H\u0007¢\u0006\u0002\u0010\n\u001a\u0016\u0010\u000b\u001a\u00020\u00042\u0006\u0010\u0002\u001a\u00020\u00012\u0006\u0010\u0006\u001a\u00020\u0007\u001a\u0016\u0010\f\u001a\u00020\u00042\u0006\u0010\u0002\u001a\u00020\u00012\u0006\u0010\u0006\u001a\u00020\u0007\u001a)\u0010\r\u001a\u00020\u00012\u0006\u0010\u0005\u001a\u00020\u00012\f\u0010\b\u001a\b\u0012\u0004\u0012\u00020\u00070\t2\u0006\u0010\u000e\u001a\u00020\u000f¢\u0006\u0002\u0010\u0010\u001a/\u0010\u0011\u001a\u0002H\u0012\"\u0004\b\u0000\u0010\u0012*\u00020\u00012\u0012\u0010\u0013\u001a\u000e\u0012\u0004\u0012\u00020\u0001\u0012\u0004\u0012\u0002H\u00120\u0014H\u0086\b\u00f8\u0001\u0000¢\u0006\u0002\u0010\u0015\u0082\u0002\u0007\n\u0005\b\u009920\u0001¨\u0006\u0016" }, d2 = { "copyAndClose", "Landroid/database/Cursor;", "c", "findColumnIndexBySuffix", "", "cursor", "name", "", "columnNames", "", "([Ljava/lang/String;Ljava/lang/String;)I", "getColumnIndex", "getColumnIndexOrThrow", "wrapMappedColumns", "mapping", "", "(Landroid/database/Cursor;[Ljava/lang/String;[I)Landroid/database/Cursor;", "useCursor", "R", "block", "Lkotlin/Function1;", "(Landroid/database/Cursor;Lkotlin/jvm/functions/Function1;)Ljava/lang/Object;", "room-runtime_release" }, k = 2, mv = { 1, 7, 1 }, xi = 48)
public final class CursorUtil
{
    public static final Cursor copyAndClose(Cursor cursor) {
        Intrinsics.checkNotNullParameter((Object)cursor, "c");
        Label_0436: {
            if (Build$VERSION.SDK_INT > 15) {
                final Closeable closeable = (Closeable)cursor;
                try {
                    final Cursor cursor2 = (Cursor)closeable;
                    final MatrixCursor matrixCursor = new MatrixCursor(cursor2.getColumnNames(), cursor2.getCount());
                    while (cursor2.moveToNext()) {
                        final Object[] array = new Object[cursor2.getColumnCount()];
                        for (int columnCount = cursor.getColumnCount(), i = 0; i < columnCount; ++i) {
                            final int type = cursor2.getType(i);
                            if (type != 0) {
                                if (type != 1) {
                                    if (type != 2) {
                                        if (type != 3) {
                                            if (type != 4) {
                                                throw new IllegalStateException();
                                            }
                                            array[i] = cursor2.getBlob(i);
                                        }
                                        else {
                                            array[i] = cursor2.getString(i);
                                        }
                                    }
                                    else {
                                        array[i] = cursor2.getDouble(i);
                                    }
                                }
                                else {
                                    array[i] = cursor2.getLong(i);
                                }
                            }
                            else {
                                array[i] = null;
                            }
                        }
                        matrixCursor.addRow(array);
                    }
                    CloseableKt.closeFinally(closeable, (Throwable)null);
                    break Label_0436;
                }
                finally {
                    try {}
                    finally {
                        CloseableKt.closeFinally(closeable, (Throwable)cursor);
                    }
                }
            }
            try {
                final Object o = new MatrixCursor(cursor.getColumnNames(), cursor.getCount());
                while (cursor.moveToNext()) {
                    final Object[] array2 = new Object[cursor.getColumnCount()];
                    for (int columnCount2 = cursor.getColumnCount(), j = 0; j < columnCount2; ++j) {
                        final int type2 = cursor.getType(j);
                        if (type2 != 0) {
                            if (type2 != 1) {
                                if (type2 != 2) {
                                    if (type2 != 3) {
                                        if (type2 != 4) {
                                            throw new IllegalStateException();
                                        }
                                        array2[j] = cursor.getBlob(j);
                                    }
                                    else {
                                        array2[j] = cursor.getString(j);
                                    }
                                }
                                else {
                                    array2[j] = cursor.getDouble(j);
                                }
                            }
                            else {
                                array2[j] = cursor.getLong(j);
                            }
                        }
                        else {
                            array2[j] = null;
                        }
                    }
                    ((MatrixCursor)o).addRow(array2);
                }
                cursor.close();
                cursor = (Cursor)o;
                return cursor;
            }
            finally {
                cursor.close();
            }
        }
    }
    
    private static final int findColumnIndexBySuffix(final Cursor cursor, final String s) {
        if (Build$VERSION.SDK_INT > 25) {
            return -1;
        }
        if (s.length() == 0) {
            return -1;
        }
        final String[] columnNames = cursor.getColumnNames();
        Intrinsics.checkNotNullExpressionValue((Object)columnNames, "columnNames");
        return findColumnIndexBySuffix(columnNames, s);
    }
    
    public static final int findColumnIndexBySuffix(final String[] array, final String s) {
        Intrinsics.checkNotNullParameter((Object)array, "columnNames");
        Intrinsics.checkNotNullParameter((Object)s, "name");
        final StringBuilder sb = new StringBuilder();
        sb.append('.');
        sb.append(s);
        final String string = sb.toString();
        final StringBuilder sb2 = new StringBuilder();
        sb2.append('.');
        sb2.append(s);
        sb2.append('`');
        final String string2 = sb2.toString();
        for (int length = array.length, i = 0, n = 0; i < length; ++i, ++n) {
            final String s2 = array[i];
            if (s2.length() >= s.length() + 2) {
                if (StringsKt.endsWith$default(s2, string, false, 2, (Object)null)) {
                    return n;
                }
                if (s2.charAt(0) == '`' && StringsKt.endsWith$default(s2, string2, false, 2, (Object)null)) {
                    return n;
                }
            }
        }
        return -1;
    }
    
    public static final int getColumnIndex(final Cursor cursor, final String str) {
        Intrinsics.checkNotNullParameter((Object)cursor, "c");
        Intrinsics.checkNotNullParameter((Object)str, "name");
        final int columnIndex = cursor.getColumnIndex(str);
        if (columnIndex >= 0) {
            return columnIndex;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append('`');
        sb.append(str);
        sb.append('`');
        int n = cursor.getColumnIndex(sb.toString());
        if (n < 0) {
            n = findColumnIndexBySuffix(cursor, str);
        }
        return n;
    }
    
    public static final int getColumnIndexOrThrow(final Cursor cursor, final String str) {
        Intrinsics.checkNotNullParameter((Object)cursor, "c");
        Intrinsics.checkNotNullParameter((Object)str, "name");
        final int columnIndex = getColumnIndex(cursor, str);
        if (columnIndex >= 0) {
            return columnIndex;
        }
        String joinToString$default;
        try {
            final String[] columnNames = cursor.getColumnNames();
            Intrinsics.checkNotNullExpressionValue((Object)columnNames, "c.columnNames");
            joinToString$default = ArraysKt.joinToString$default((Object[])columnNames, (CharSequence)null, (CharSequence)null, (CharSequence)null, 0, (CharSequence)null, (Function1)null, 63, (Object)null);
        }
        catch (final Exception ex) {
            Log.d("RoomCursorUtil", "Cannot collect column names for debug purposes", (Throwable)ex);
            joinToString$default = "unknown";
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("column '");
        sb.append(str);
        sb.append("' does not exist. Available columns: ");
        sb.append(joinToString$default);
        throw new IllegalArgumentException(sb.toString());
    }
    
    public static final <R> R useCursor(Cursor cursor, final Function1<? super Cursor, ? extends R> function1) {
        Intrinsics.checkNotNullParameter((Object)cursor, "<this>");
        Intrinsics.checkNotNullParameter((Object)function1, "block");
        if (Build$VERSION.SDK_INT > 15) {
            cursor = cursor;
            try {
                final Object invoke = function1.invoke((Object)cursor);
                InlineMarker.finallyStart(1);
                CloseableKt.closeFinally((Closeable)cursor, (Throwable)null);
                InlineMarker.finallyEnd(1);
                return (R)invoke;
            }
            finally {
                try {}
                finally {
                    InlineMarker.finallyStart(1);
                    CloseableKt.closeFinally((Closeable)cursor, (Throwable)function1);
                    InlineMarker.finallyEnd(1);
                }
            }
        }
        try {
            return (R)function1.invoke((Object)cursor);
        }
        finally {
            InlineMarker.finallyStart(1);
            cursor.close();
            InlineMarker.finallyEnd(1);
        }
    }
    
    public static final Cursor wrapMappedColumns(final Cursor cursor, final String[] array, final int[] array2) {
        Intrinsics.checkNotNullParameter((Object)cursor, "cursor");
        Intrinsics.checkNotNullParameter((Object)array, "columnNames");
        Intrinsics.checkNotNullParameter((Object)array2, "mapping");
        if (array.length == array2.length) {
            return (Cursor)new CursorUtil$wrapMappedColumns.CursorUtil$wrapMappedColumns$2(cursor, array, array2);
        }
        throw new IllegalStateException("Expected columnNames.length == mapping.length".toString());
    }
}

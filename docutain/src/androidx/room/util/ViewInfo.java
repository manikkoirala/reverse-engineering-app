// 
// Decompiled by Procyon v0.6.0
// 

package androidx.room.util;

import kotlin.io.CloseableKt;
import android.database.Cursor;
import java.io.Closeable;
import android.os.Build$VERSION;
import kotlin.jvm.JvmStatic;
import androidx.sqlite.db.SupportSQLiteDatabase;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\b\u0007\u0018\u0000 \f2\u00020\u0001:\u0001\fB\u0017\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\b\u0010\u0004\u001a\u0004\u0018\u00010\u0003¢\u0006\u0002\u0010\u0005J\u0013\u0010\u0006\u001a\u00020\u00072\b\u0010\b\u001a\u0004\u0018\u00010\u0001H\u0096\u0002J\b\u0010\t\u001a\u00020\nH\u0016J\b\u0010\u000b\u001a\u00020\u0003H\u0016R\u0010\u0010\u0002\u001a\u00020\u00038\u0006X\u0087\u0004¢\u0006\u0002\n\u0000R\u0012\u0010\u0004\u001a\u0004\u0018\u00010\u00038\u0006X\u0087\u0004¢\u0006\u0002\n\u0000¨\u0006\r" }, d2 = { "Landroidx/room/util/ViewInfo;", "", "name", "", "sql", "(Ljava/lang/String;Ljava/lang/String;)V", "equals", "", "other", "hashCode", "", "toString", "Companion", "room-runtime_release" }, k = 1, mv = { 1, 7, 1 }, xi = 48)
public final class ViewInfo
{
    public static final Companion Companion;
    public final String name;
    public final String sql;
    
    static {
        Companion = new Companion(null);
    }
    
    public ViewInfo(final String name, final String sql) {
        Intrinsics.checkNotNullParameter((Object)name, "name");
        this.name = name;
        this.sql = sql;
    }
    
    @JvmStatic
    public static final ViewInfo read(final SupportSQLiteDatabase supportSQLiteDatabase, final String s) {
        return ViewInfo.Companion.read(supportSQLiteDatabase, s);
    }
    
    @Override
    public boolean equals(final Object o) {
        final boolean b = true;
        if (this == o) {
            return true;
        }
        if (!(o instanceof ViewInfo)) {
            return false;
        }
        final String name = this.name;
        final ViewInfo viewInfo = (ViewInfo)o;
        if (Intrinsics.areEqual((Object)name, (Object)viewInfo.name)) {
            final String sql = this.sql;
            final String sql2 = viewInfo.sql;
            boolean equal;
            if (sql != null) {
                equal = Intrinsics.areEqual((Object)sql, (Object)sql2);
            }
            else {
                equal = (sql2 == null);
            }
            if (equal) {
                return b;
            }
        }
        return false;
    }
    
    @Override
    public int hashCode() {
        final int hashCode = this.name.hashCode();
        final String sql = this.sql;
        int hashCode2;
        if (sql != null) {
            hashCode2 = sql.hashCode();
        }
        else {
            hashCode2 = 0;
        }
        return hashCode * 31 + hashCode2;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("ViewInfo{name='");
        sb.append(this.name);
        sb.append("', sql='");
        sb.append(this.sql);
        sb.append("'}");
        return sb.toString();
    }
    
    @Metadata(d1 = { "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0018\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bH\u0007¨\u0006\t" }, d2 = { "Landroidx/room/util/ViewInfo$Companion;", "", "()V", "read", "Landroidx/room/util/ViewInfo;", "database", "Landroidx/sqlite/db/SupportSQLiteDatabase;", "viewName", "", "room-runtime_release" }, k = 1, mv = { 1, 7, 1 }, xi = 48)
    public static final class Companion
    {
        private Companion() {
        }
        
        @JvmStatic
        public final ViewInfo read(SupportSQLiteDatabase supportSQLiteDatabase, String string) {
            Intrinsics.checkNotNullParameter((Object)supportSQLiteDatabase, "database");
            Intrinsics.checkNotNullParameter((Object)string, "viewName");
            final StringBuilder sb = new StringBuilder();
            sb.append("SELECT name, sql FROM sqlite_master WHERE type = 'view' AND name = '");
            sb.append(string);
            sb.append('\'');
            Object query = supportSQLiteDatabase.query(sb.toString());
            if (Build$VERSION.SDK_INT > 15) {
                query = query;
                try {
                    final Cursor cursor = (Cursor)query;
                    if (cursor.moveToFirst()) {
                        string = cursor.getString(0);
                        Intrinsics.checkNotNullExpressionValue((Object)string, "cursor.getString(0)");
                        final ViewInfo viewInfo = new ViewInfo(string, cursor.getString(1));
                    }
                    else {
                        final ViewInfo viewInfo2 = new ViewInfo(string, null);
                    }
                    CloseableKt.closeFinally((Closeable)query, (Throwable)null);
                    return (ViewInfo)supportSQLiteDatabase;
                }
                finally {
                    try {}
                    finally {
                        CloseableKt.closeFinally((Closeable)query, (Throwable)supportSQLiteDatabase);
                    }
                }
            }
            try {
                if (((Cursor)query).moveToFirst()) {
                    final String string2 = ((Cursor)query).getString(0);
                    Intrinsics.checkNotNullExpressionValue((Object)string2, "cursor.getString(0)");
                    supportSQLiteDatabase = (SupportSQLiteDatabase)new ViewInfo(string2, ((Cursor)query).getString(1));
                }
                else {
                    supportSQLiteDatabase = (SupportSQLiteDatabase)new ViewInfo(string, null);
                }
                return (ViewInfo)supportSQLiteDatabase;
            }
            finally {
                ((Cursor)query).close();
            }
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.room.util;

import java.util.Iterator;
import android.util.Log;
import java.util.ArrayList;
import java.util.Collection;
import kotlin.text.StringsKt;
import kotlin.jvm.functions.Function1;
import kotlin.collections.CollectionsKt;
import java.util.List;
import kotlin.jvm.internal.Intrinsics;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000*\n\u0000\n\u0002\u0010\u0011\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\b\u0003\u001a\u0016\u0010\u0006\u001a\u00020\u00072\u0006\u0010\b\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000b\u001a\u0018\u0010\f\u001a\u0004\u0018\u00010\u00022\u000e\u0010\r\u001a\n\u0012\u0004\u0012\u00020\u000b\u0018\u00010\u000e\u001a\u0006\u0010\u000f\u001a\u00020\t\u001a\u0018\u0010\u0010\u001a\n\u0012\u0004\u0012\u00020\u000b\u0018\u00010\u000e2\b\u0010\r\u001a\u0004\u0018\u00010\u0002\" \u0010\u0000\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00020\u00018\u0006X\u0087\u0004¢\u0006\n\n\u0002\u0010\u0005\u0012\u0004\b\u0003\u0010\u0004¨\u0006\u0011" }, d2 = { "EMPTY_STRING_ARRAY", "", "", "getEMPTY_STRING_ARRAY$annotations", "()V", "[Ljava/lang/String;", "appendPlaceholders", "", "builder", "Ljava/lang/StringBuilder;", "count", "", "joinIntoString", "input", "", "newStringBuilder", "splitToIntList", "room-runtime_release" }, k = 2, mv = { 1, 7, 1 }, xi = 48)
public final class StringUtil
{
    public static final String[] EMPTY_STRING_ARRAY;
    
    static {
        EMPTY_STRING_ARRAY = new String[0];
    }
    
    public static final void appendPlaceholders(final StringBuilder sb, final int n) {
        Intrinsics.checkNotNullParameter((Object)sb, "builder");
        for (int i = 0; i < n; ++i) {
            sb.append("?");
            if (i < n - 1) {
                sb.append(",");
            }
        }
    }
    
    public static final String joinIntoString(final List<Integer> list) {
        String joinToString$default;
        if (list != null) {
            joinToString$default = CollectionsKt.joinToString$default((Iterable)list, (CharSequence)",", (CharSequence)null, (CharSequence)null, 0, (CharSequence)null, (Function1)null, 62, (Object)null);
        }
        else {
            joinToString$default = null;
        }
        return joinToString$default;
    }
    
    public static final StringBuilder newStringBuilder() {
        return new StringBuilder();
    }
    
    public static final List<Integer> splitToIntList(String s) {
        List<Integer> list = null;
        if (s != null) {
            final List split$default = StringsKt.split$default((CharSequence)s, new char[] { ',' }, false, 0, 6, (Object)null);
            list = list;
            if (split$default != null) {
                final Iterable iterable = split$default;
                final Collection collection = new ArrayList();
                final Iterator iterator = iterable.iterator();
                while (iterator.hasNext()) {
                    s = (String)iterator.next();
                    Integer value;
                    try {
                        value = Integer.parseInt(s);
                    }
                    catch (final NumberFormatException ex) {
                        Log.e("ROOM", "Malformed integer list", (Throwable)ex);
                        final Integer n = null;
                        value = null;
                    }
                    if (value != null) {
                        collection.add(value);
                    }
                }
                list = (List<Integer>)collection;
            }
        }
        return list;
    }
}

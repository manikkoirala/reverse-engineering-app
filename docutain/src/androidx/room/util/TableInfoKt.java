// 
// Decompiled by Procyon v0.6.0
// 

package androidx.room.util;

import java.util.TreeMap;
import java.util.Iterator;
import java.util.Collection;
import java.util.ArrayList;
import kotlin.collections.SetsKt;
import java.util.Set;
import kotlin.collections.CollectionsKt;
import java.util.List;
import kotlin.jvm.internal.Intrinsics;
import java.io.Closeable;
import kotlin.io.CloseableKt;
import kotlin.collections.MapsKt;
import android.database.Cursor;
import android.os.Build$VERSION;
import java.util.Map;
import androidx.sqlite.db.SupportSQLiteDatabase;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000H\n\u0000\n\u0002\u0010$\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\"\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\u001a$\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0002H\u0002\u001a\u0016\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\t0\b2\u0006\u0010\n\u001a\u00020\u000bH\u0002\u001a\u001e\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u000e0\r2\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0002H\u0002\u001a\"\u0010\u000f\u001a\u0004\u0018\u00010\u00102\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0011\u001a\u00020\u00022\u0006\u0010\u0012\u001a\u00020\u0013H\u0002\u001a \u0010\u0014\u001a\n\u0012\u0004\u0012\u00020\u0010\u0018\u00010\r2\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0002H\u0002\u001a\u0018\u0010\u0015\u001a\u00020\u00162\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0002H\u0000¨\u0006\u0017" }, d2 = { "readColumns", "", "", "Landroidx/room/util/TableInfo$Column;", "database", "Landroidx/sqlite/db/SupportSQLiteDatabase;", "tableName", "readForeignKeyFieldMappings", "", "Landroidx/room/util/TableInfo$ForeignKeyWithSequence;", "cursor", "Landroid/database/Cursor;", "readForeignKeys", "", "Landroidx/room/util/TableInfo$ForeignKey;", "readIndex", "Landroidx/room/util/TableInfo$Index;", "name", "unique", "", "readIndices", "readTableInfo", "Landroidx/room/util/TableInfo;", "room-runtime_release" }, k = 2, mv = { 1, 7, 1 }, xi = 48)
public final class TableInfoKt
{
    private static final Map<String, TableInfo.Column> readColumns(SupportSQLiteDatabase query, final String str) {
        final StringBuilder sb = new StringBuilder();
        sb.append("PRAGMA table_info(`");
        sb.append(str);
        sb.append("`)");
        query = (SupportSQLiteDatabase)query.query(sb.toString());
        if (Build$VERSION.SDK_INT > 15) {
            query = query;
            try {
                final Cursor cursor = (Cursor)query;
                if (cursor.getColumnCount() <= 0) {
                    final Map emptyMap = MapsKt.emptyMap();
                    CloseableKt.closeFinally((Closeable)query, (Throwable)null);
                    return emptyMap;
                }
                final int columnIndex = cursor.getColumnIndex("name");
                final int columnIndex2 = cursor.getColumnIndex("type");
                final int columnIndex3 = cursor.getColumnIndex("notnull");
                final int columnIndex4 = cursor.getColumnIndex("pk");
                final int columnIndex5 = cursor.getColumnIndex("dflt_value");
                final Map mapBuilder = MapsKt.createMapBuilder();
                while (cursor.moveToNext()) {
                    final String string = cursor.getString(columnIndex);
                    final String string2 = cursor.getString(columnIndex2);
                    final boolean b = cursor.getInt(columnIndex3) != 0;
                    final int int1 = cursor.getInt(columnIndex4);
                    final String string3 = cursor.getString(columnIndex5);
                    Intrinsics.checkNotNullExpressionValue((Object)string, "name");
                    Intrinsics.checkNotNullExpressionValue((Object)string2, "type");
                    mapBuilder.put(string, new TableInfo.Column(string, string2, b, int1, string3, 2));
                }
                final Map build = MapsKt.build(mapBuilder);
                CloseableKt.closeFinally((Closeable)query, (Throwable)null);
                return build;
            }
            finally {
                try {}
                finally {
                    final Throwable t;
                    CloseableKt.closeFinally((Closeable)query, t);
                }
            }
        }
        try {
            if (((Cursor)query).getColumnCount() <= 0) {
                return MapsKt.emptyMap();
            }
            final int columnIndex6 = ((Cursor)query).getColumnIndex("name");
            final int columnIndex7 = ((Cursor)query).getColumnIndex("type");
            final int columnIndex8 = ((Cursor)query).getColumnIndex("notnull");
            final int columnIndex9 = ((Cursor)query).getColumnIndex("pk");
            final int columnIndex10 = ((Cursor)query).getColumnIndex("dflt_value");
            final Map mapBuilder2 = MapsKt.createMapBuilder();
            while (((Cursor)query).moveToNext()) {
                final String string4 = ((Cursor)query).getString(columnIndex6);
                final String string5 = ((Cursor)query).getString(columnIndex7);
                final boolean b2 = ((Cursor)query).getInt(columnIndex8) != 0;
                final int int2 = ((Cursor)query).getInt(columnIndex9);
                final String string6 = ((Cursor)query).getString(columnIndex10);
                Intrinsics.checkNotNullExpressionValue((Object)string4, "name");
                Intrinsics.checkNotNullExpressionValue((Object)string5, "type");
                mapBuilder2.put(string4, new TableInfo.Column(string4, string5, b2, int2, string6, 2));
            }
            return MapsKt.build(mapBuilder2);
        }
        finally {
            ((Cursor)query).close();
        }
    }
    
    private static final List<TableInfo.ForeignKeyWithSequence> readForeignKeyFieldMappings(final Cursor cursor) {
        final int columnIndex = cursor.getColumnIndex("id");
        final int columnIndex2 = cursor.getColumnIndex("seq");
        final int columnIndex3 = cursor.getColumnIndex("from");
        final int columnIndex4 = cursor.getColumnIndex("to");
        final List listBuilder = CollectionsKt.createListBuilder();
        while (cursor.moveToNext()) {
            final int int1 = cursor.getInt(columnIndex);
            final int int2 = cursor.getInt(columnIndex2);
            final String string = cursor.getString(columnIndex3);
            Intrinsics.checkNotNullExpressionValue((Object)string, "cursor.getString(fromColumnIndex)");
            final String string2 = cursor.getString(columnIndex4);
            Intrinsics.checkNotNullExpressionValue((Object)string2, "cursor.getString(toColumnIndex)");
            listBuilder.add(new TableInfo.ForeignKeyWithSequence(int1, int2, string, string2));
        }
        return CollectionsKt.sorted((Iterable)CollectionsKt.build(listBuilder));
    }
    
    private static final Set<TableInfo.ForeignKey> readForeignKeys(SupportSQLiteDatabase query, final String str) {
        final StringBuilder sb = new StringBuilder();
        sb.append("PRAGMA foreign_key_list(`");
        sb.append(str);
        sb.append("`)");
        query = (SupportSQLiteDatabase)query.query(sb.toString());
        if (Build$VERSION.SDK_INT > 15) {
            query = query;
            try {
                final Cursor cursor = (Cursor)query;
                final int columnIndex = cursor.getColumnIndex("id");
                final int columnIndex2 = cursor.getColumnIndex("seq");
                final int columnIndex3 = cursor.getColumnIndex("table");
                final int columnIndex4 = cursor.getColumnIndex("on_delete");
                final int columnIndex5 = cursor.getColumnIndex("on_update");
                final List<TableInfo.ForeignKeyWithSequence> foreignKeyFieldMappings = readForeignKeyFieldMappings(cursor);
                cursor.moveToPosition(-1);
                final Set setBuilder = SetsKt.createSetBuilder();
                while (cursor.moveToNext()) {
                    if (cursor.getInt(columnIndex2) != 0) {
                        continue;
                    }
                    final int int1 = cursor.getInt(columnIndex);
                    final List list = new ArrayList();
                    final List list2 = new ArrayList();
                    final Iterable iterable = foreignKeyFieldMappings;
                    final Collection collection = new ArrayList();
                    for (final Object next : iterable) {
                        if (((TableInfo.ForeignKeyWithSequence)next).getId() == int1) {
                            collection.add(next);
                        }
                    }
                    for (final TableInfo.ForeignKeyWithSequence foreignKeyWithSequence : collection) {
                        list.add(foreignKeyWithSequence.getFrom());
                        list2.add(foreignKeyWithSequence.getTo());
                    }
                    final String string = cursor.getString(columnIndex3);
                    Intrinsics.checkNotNullExpressionValue((Object)string, "cursor.getString(tableColumnIndex)");
                    final String string2 = cursor.getString(columnIndex4);
                    Intrinsics.checkNotNullExpressionValue((Object)string2, "cursor.getString(onDeleteColumnIndex)");
                    final String string3 = cursor.getString(columnIndex5);
                    Intrinsics.checkNotNullExpressionValue((Object)string3, "cursor.getString(onUpdateColumnIndex)");
                    setBuilder.add(new TableInfo.ForeignKey(string, string2, string3, list, list2));
                }
                final Set build = SetsKt.build(setBuilder);
                CloseableKt.closeFinally((Closeable)query, (Throwable)null);
                return build;
            }
            finally {
                try {}
                finally {
                    final Throwable t;
                    CloseableKt.closeFinally((Closeable)query, t);
                }
            }
        }
        try {
            final int columnIndex6 = ((Cursor)query).getColumnIndex("id");
            final int columnIndex7 = ((Cursor)query).getColumnIndex("seq");
            final int columnIndex8 = ((Cursor)query).getColumnIndex("table");
            final int columnIndex9 = ((Cursor)query).getColumnIndex("on_delete");
            final int columnIndex10 = ((Cursor)query).getColumnIndex("on_update");
            final List<TableInfo.ForeignKeyWithSequence> foreignKeyFieldMappings2 = readForeignKeyFieldMappings((Cursor)query);
            ((Cursor)query).moveToPosition(-1);
            final Set setBuilder2 = SetsKt.createSetBuilder();
            while (((Cursor)query).moveToNext()) {
                if (((Cursor)query).getInt(columnIndex7) != 0) {
                    continue;
                }
                final int int2 = ((Cursor)query).getInt(columnIndex6);
                final List list3 = new ArrayList();
                final List list4 = new ArrayList();
                final Iterable iterable2 = foreignKeyFieldMappings2;
                final Collection collection2 = new ArrayList();
                for (final Object next2 : iterable2) {
                    if (((TableInfo.ForeignKeyWithSequence)next2).getId() == int2) {
                        collection2.add(next2);
                    }
                }
                for (final TableInfo.ForeignKeyWithSequence foreignKeyWithSequence2 : collection2) {
                    list3.add(foreignKeyWithSequence2.getFrom());
                    list4.add(foreignKeyWithSequence2.getTo());
                }
                final String string4 = ((Cursor)query).getString(columnIndex8);
                Intrinsics.checkNotNullExpressionValue((Object)string4, "cursor.getString(tableColumnIndex)");
                final String string5 = ((Cursor)query).getString(columnIndex9);
                Intrinsics.checkNotNullExpressionValue((Object)string5, "cursor.getString(onDeleteColumnIndex)");
                final String string6 = ((Cursor)query).getString(columnIndex10);
                Intrinsics.checkNotNullExpressionValue((Object)string6, "cursor.getString(onUpdateColumnIndex)");
                setBuilder2.add(new TableInfo.ForeignKey(string4, string5, string6, list3, list4));
            }
            return SetsKt.build(setBuilder2);
        }
        finally {
            ((Cursor)query).close();
        }
    }
    
    private static final TableInfo.Index readIndex(final SupportSQLiteDatabase supportSQLiteDatabase, final String str, final boolean b) {
        final StringBuilder sb = new StringBuilder();
        sb.append("PRAGMA index_xinfo(`");
        sb.append(str);
        sb.append("`)");
        Cursor query = supportSQLiteDatabase.query(sb.toString());
        final int sdk_INT = Build$VERSION.SDK_INT;
        final String s = "DESC";
        final String s2 = "ASC";
        if (sdk_INT > 15) {
            final Closeable closeable = (Closeable)query;
            try {
                query = (Cursor)closeable;
                final int columnIndex = query.getColumnIndex("seqno");
                final int columnIndex2 = query.getColumnIndex("cid");
                final int columnIndex3 = query.getColumnIndex("name");
                final int columnIndex4 = query.getColumnIndex("desc");
                if (columnIndex != -1 && columnIndex2 != -1 && columnIndex3 != -1 && columnIndex4 != -1) {
                    final TreeMap treeMap = new TreeMap();
                    final TreeMap<Object, Object> treeMap2 = new TreeMap<Object, Object>();
                    while (query.moveToNext()) {
                        if (query.getInt(columnIndex2) < 0) {
                            continue;
                        }
                        final int int1 = query.getInt(columnIndex);
                        final String string = query.getString(columnIndex3);
                        String s3;
                        if (query.getInt(columnIndex4) > 0) {
                            s3 = s;
                        }
                        else {
                            s3 = s2;
                        }
                        final Map map = treeMap;
                        Intrinsics.checkNotNullExpressionValue((Object)string, "columnName");
                        map.put(int1, string);
                        ((Map<Integer, String>)treeMap2).put(int1, s3);
                    }
                    final Collection values = treeMap.values();
                    Intrinsics.checkNotNullExpressionValue((Object)values, "columnsMap.values");
                    final List list = CollectionsKt.toList((Iterable)values);
                    final Collection<Object> values2 = treeMap2.values();
                    Intrinsics.checkNotNullExpressionValue((Object)values2, "ordersMap.values");
                    final TableInfo.Index index = new TableInfo.Index(str, b, list, CollectionsKt.toList((Iterable)values2));
                    CloseableKt.closeFinally(closeable, (Throwable)null);
                    return;
                }
                CloseableKt.closeFinally(closeable, (Throwable)null);
                return null;
            }
            finally {
                try {}
                finally {
                    CloseableKt.closeFinally(closeable, (Throwable)str);
                }
            }
        }
        try {
            final int columnIndex5 = query.getColumnIndex("seqno");
            final int columnIndex6 = query.getColumnIndex("cid");
            final int columnIndex7 = query.getColumnIndex("name");
            final int columnIndex8 = query.getColumnIndex("desc");
            if (columnIndex5 != -1 && columnIndex6 != -1 && columnIndex7 != -1 && columnIndex8 != -1) {
                final TreeMap treeMap3 = new TreeMap();
                final TreeMap<Object, Object> treeMap4 = new TreeMap<Object, Object>();
                while (query.moveToNext()) {
                    if (query.getInt(columnIndex6) < 0) {
                        continue;
                    }
                    final int int2 = query.getInt(columnIndex5);
                    final String string2 = query.getString(columnIndex7);
                    String s4;
                    if (query.getInt(columnIndex8) > 0) {
                        s4 = "DESC";
                    }
                    else {
                        s4 = "ASC";
                    }
                    final Map map2 = treeMap3;
                    Intrinsics.checkNotNullExpressionValue((Object)string2, "columnName");
                    map2.put(int2, string2);
                    ((Map<Integer, String>)treeMap4).put(int2, s4);
                }
                final Collection values3 = treeMap3.values();
                Intrinsics.checkNotNullExpressionValue((Object)values3, "columnsMap.values");
                final List list2 = CollectionsKt.toList((Iterable)values3);
                final Collection<Object> values4 = treeMap4.values();
                Intrinsics.checkNotNullExpressionValue((Object)values4, "ordersMap.values");
                return new TableInfo.Index(str, b, list2, CollectionsKt.toList((Iterable)values4));
            }
            return null;
        }
        finally {
            query.close();
        }
    }
    
    private static final Set<TableInfo.Index> readIndices(final SupportSQLiteDatabase supportSQLiteDatabase, String query) {
        final StringBuilder sb = new StringBuilder();
        sb.append("PRAGMA index_list(`");
        sb.append(query);
        sb.append("`)");
        query = (String)supportSQLiteDatabase.query(sb.toString());
        if (Build$VERSION.SDK_INT > 15) {
            query = query;
            try {
                final Cursor cursor = (Cursor)query;
                final int columnIndex = cursor.getColumnIndex("name");
                final int columnIndex2 = cursor.getColumnIndex("origin");
                final int columnIndex3 = cursor.getColumnIndex("unique");
                if (columnIndex != -1 && columnIndex2 != -1 && columnIndex3 != -1) {
                    final Set setBuilder = SetsKt.createSetBuilder();
                    while (cursor.moveToNext()) {
                        if (!Intrinsics.areEqual((Object)"c", (Object)cursor.getString(columnIndex2))) {
                            continue;
                        }
                        final String string = cursor.getString(columnIndex);
                        final boolean b = cursor.getInt(columnIndex3) == 1;
                        Intrinsics.checkNotNullExpressionValue((Object)string, "name");
                        final TableInfo.Index index = readIndex(supportSQLiteDatabase, string, b);
                        if (index == null) {
                            CloseableKt.closeFinally((Closeable)query, (Throwable)null);
                            return null;
                        }
                        setBuilder.add(index);
                    }
                    final Set build = SetsKt.build(setBuilder);
                    CloseableKt.closeFinally((Closeable)query, (Throwable)null);
                    return build;
                }
                CloseableKt.closeFinally((Closeable)query, (Throwable)null);
                return null;
            }
            finally {
                try {}
                finally {
                    CloseableKt.closeFinally((Closeable)query, (Throwable)supportSQLiteDatabase);
                }
            }
        }
        try {
            final int columnIndex4 = ((Cursor)query).getColumnIndex("name");
            final int columnIndex5 = ((Cursor)query).getColumnIndex("origin");
            final int columnIndex6 = ((Cursor)query).getColumnIndex("unique");
            if (columnIndex4 != -1 && columnIndex5 != -1 && columnIndex6 != -1) {
                final Set setBuilder2 = SetsKt.createSetBuilder();
                while (((Cursor)query).moveToNext()) {
                    if (!Intrinsics.areEqual((Object)"c", (Object)((Cursor)query).getString(columnIndex5))) {
                        continue;
                    }
                    final String string2 = ((Cursor)query).getString(columnIndex4);
                    final boolean b2 = ((Cursor)query).getInt(columnIndex6) == 1;
                    Intrinsics.checkNotNullExpressionValue((Object)string2, "name");
                    final TableInfo.Index index2 = readIndex(supportSQLiteDatabase, string2, b2);
                    if (index2 == null) {
                        return null;
                    }
                    setBuilder2.add(index2);
                }
                return SetsKt.build(setBuilder2);
            }
            return null;
        }
        finally {
            ((Cursor)query).close();
        }
    }
    
    public static final TableInfo readTableInfo(final SupportSQLiteDatabase supportSQLiteDatabase, final String s) {
        Intrinsics.checkNotNullParameter((Object)supportSQLiteDatabase, "database");
        Intrinsics.checkNotNullParameter((Object)s, "tableName");
        return new TableInfo(s, readColumns(supportSQLiteDatabase, s), readForeignKeys(supportSQLiteDatabase, s), readIndices(supportSQLiteDatabase, s));
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.room.util;

import java.io.IOException;
import java.io.OutputStream;
import java.io.InputStream;
import java.nio.channels.WritableByteChannel;
import java.nio.channels.Channels;
import android.os.Build$VERSION;
import kotlin.jvm.internal.Intrinsics;
import java.nio.channels.FileChannel;
import java.nio.channels.ReadableByteChannel;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000\u0014\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u001a\u0018\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\u0007¨\u0006\u0006" }, d2 = { "copy", "", "input", "Ljava/nio/channels/ReadableByteChannel;", "output", "Ljava/nio/channels/FileChannel;", "room-runtime_release" }, k = 2, mv = { 1, 7, 1 }, xi = 48)
public final class FileUtil
{
    public static final void copy(final ReadableByteChannel ch, final FileChannel fileChannel) throws IOException {
        Intrinsics.checkNotNullParameter((Object)ch, "input");
        Intrinsics.checkNotNullParameter((Object)fileChannel, "output");
        try {
            if (Build$VERSION.SDK_INT > 23) {
                fileChannel.transferFrom(ch, 0L, Long.MAX_VALUE);
            }
            else {
                final InputStream inputStream = Channels.newInputStream(ch);
                final OutputStream outputStream = Channels.newOutputStream(fileChannel);
                final byte[] array = new byte[4096];
                while (true) {
                    final int read = inputStream.read(array);
                    if (read <= 0) {
                        break;
                    }
                    outputStream.write(array, 0, read);
                }
            }
            fileChannel.force(false);
        }
        finally {
            ch.close();
            fileChannel.close();
        }
    }
}

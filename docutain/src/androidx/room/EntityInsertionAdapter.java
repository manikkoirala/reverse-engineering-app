// 
// Decompiled by Procyon v0.6.0
// 

package androidx.room;

import java.util.List;
import kotlin.jvm.internal.ArrayIteratorKt;
import kotlin.collections.CollectionsKt;
import java.util.Collection;
import java.util.Iterator;
import androidx.sqlite.db.SupportSQLiteStatement;
import kotlin.jvm.internal.Intrinsics;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000L\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0011\n\u0000\n\u0002\u0010\u001c\n\u0000\n\u0002\u0010\t\n\u0002\b\u0002\n\u0002\u0010\u0016\n\u0000\n\u0002\u0010\u001e\n\u0002\b\u0004\n\u0002\u0010 \n\u0002\b\u0002\b'\u0018\u0000*\u0004\b\u0000\u0010\u00012\u00020\u0002B\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004¢\u0006\u0002\u0010\u0005J\u001f\u0010\u0006\u001a\u00020\u00072\b\u0010\b\u001a\u0004\u0018\u00010\t2\u0006\u0010\n\u001a\u00028\u0000H$¢\u0006\u0002\u0010\u000bJ\u0013\u0010\f\u001a\u00020\u00072\u0006\u0010\n\u001a\u00028\u0000¢\u0006\u0002\u0010\rJ\u001b\u0010\f\u001a\u00020\u00072\u000e\u0010\u000e\u001a\n\u0012\u0006\b\u0001\u0012\u00028\u00000\u000f¢\u0006\u0002\u0010\u0010J\u0014\u0010\f\u001a\u00020\u00072\f\u0010\u000e\u001a\b\u0012\u0004\u0012\u00028\u00000\u0011J\u0013\u0010\u0012\u001a\u00020\u00132\u0006\u0010\n\u001a\u00028\u0000¢\u0006\u0002\u0010\u0014J\u001b\u0010\u0015\u001a\u00020\u00162\u000e\u0010\u000e\u001a\n\u0012\u0006\b\u0001\u0012\u00028\u00000\u000f¢\u0006\u0002\u0010\u0017J\u0014\u0010\u0015\u001a\u00020\u00162\f\u0010\u000e\u001a\b\u0012\u0004\u0012\u00028\u00000\u0018J#\u0010\u0019\u001a\n\u0012\u0006\b\u0001\u0012\u00020\u00130\u000f2\u000e\u0010\u000e\u001a\n\u0012\u0006\b\u0001\u0012\u00028\u00000\u000f¢\u0006\u0002\u0010\u001aJ!\u0010\u0019\u001a\n\u0012\u0006\b\u0001\u0012\u00020\u00130\u000f2\f\u0010\u000e\u001a\b\u0012\u0004\u0012\u00028\u00000\u0018¢\u0006\u0002\u0010\u001bJ!\u0010\u001c\u001a\b\u0012\u0004\u0012\u00020\u00130\u001d2\u000e\u0010\u000e\u001a\n\u0012\u0006\b\u0001\u0012\u00028\u00000\u000f¢\u0006\u0002\u0010\u001eJ\u001a\u0010\u001c\u001a\b\u0012\u0004\u0012\u00020\u00130\u001d2\f\u0010\u000e\u001a\b\u0012\u0004\u0012\u00028\u00000\u0018¨\u0006\u001f" }, d2 = { "Landroidx/room/EntityInsertionAdapter;", "T", "Landroidx/room/SharedSQLiteStatement;", "database", "Landroidx/room/RoomDatabase;", "(Landroidx/room/RoomDatabase;)V", "bind", "", "statement", "Landroidx/sqlite/db/SupportSQLiteStatement;", "entity", "(Landroidx/sqlite/db/SupportSQLiteStatement;Ljava/lang/Object;)V", "insert", "(Ljava/lang/Object;)V", "entities", "", "([Ljava/lang/Object;)V", "", "insertAndReturnId", "", "(Ljava/lang/Object;)J", "insertAndReturnIdsArray", "", "([Ljava/lang/Object;)[J", "", "insertAndReturnIdsArrayBox", "([Ljava/lang/Object;)[Ljava/lang/Long;", "(Ljava/util/Collection;)[Ljava/lang/Long;", "insertAndReturnIdsList", "", "([Ljava/lang/Object;)Ljava/util/List;", "room-runtime_release" }, k = 1, mv = { 1, 7, 1 }, xi = 48)
public abstract class EntityInsertionAdapter<T> extends SharedSQLiteStatement
{
    public EntityInsertionAdapter(final RoomDatabase roomDatabase) {
        Intrinsics.checkNotNullParameter((Object)roomDatabase, "database");
        super(roomDatabase);
    }
    
    protected abstract void bind(final SupportSQLiteStatement p0, final T p1);
    
    public final void insert(final Iterable<? extends T> iterable) {
        Intrinsics.checkNotNullParameter((Object)iterable, "entities");
        final SupportSQLiteStatement acquire = this.acquire();
        try {
            final Iterator<? extends T> iterator = iterable.iterator();
            while (iterator.hasNext()) {
                this.bind(acquire, iterator.next());
                acquire.executeInsert();
            }
        }
        finally {
            this.release(acquire);
        }
    }
    
    public final void insert(final T t) {
        final SupportSQLiteStatement acquire = this.acquire();
        try {
            this.bind(acquire, t);
            acquire.executeInsert();
        }
        finally {
            this.release(acquire);
        }
    }
    
    public final void insert(final T[] array) {
        Intrinsics.checkNotNullParameter((Object)array, "entities");
        final SupportSQLiteStatement acquire = this.acquire();
        int i = 0;
        try {
            while (i < array.length) {
                this.bind(acquire, array[i]);
                acquire.executeInsert();
                ++i;
            }
        }
        finally {
            this.release(acquire);
        }
    }
    
    public final long insertAndReturnId(final T t) {
        final SupportSQLiteStatement acquire = this.acquire();
        try {
            this.bind(acquire, t);
            return acquire.executeInsert();
        }
        finally {
            this.release(acquire);
        }
    }
    
    public final long[] insertAndReturnIdsArray(final Collection<? extends T> collection) {
        Intrinsics.checkNotNullParameter((Object)collection, "entities");
        final SupportSQLiteStatement acquire = this.acquire();
        try {
            final long[] array = new long[collection.size()];
            final Iterable iterable = collection;
            int n = 0;
            for (final Object next : iterable) {
                if (n < 0) {
                    CollectionsKt.throwIndexOverflow();
                }
                this.bind(acquire, (T)next);
                array[n] = acquire.executeInsert();
                ++n;
            }
            return array;
        }
        finally {
            this.release(acquire);
        }
    }
    
    public final long[] insertAndReturnIdsArray(final T[] array) {
        Intrinsics.checkNotNullParameter((Object)array, "entities");
        final SupportSQLiteStatement acquire = this.acquire();
        try {
            final long[] array2 = new long[array.length];
            for (int length = array.length, i = 0, n = 0; i < length; ++i, ++n) {
                this.bind(acquire, array[i]);
                array2[n] = acquire.executeInsert();
            }
            return array2;
        }
        finally {
            this.release(acquire);
        }
    }
    
    public final Long[] insertAndReturnIdsArrayBox(final Collection<? extends T> collection) {
        Intrinsics.checkNotNullParameter((Object)collection, "entities");
        final SupportSQLiteStatement acquire = this.acquire();
        final Iterator<? extends T> iterator = collection.iterator();
        int i = 0;
        try {
            final int size = collection.size();
            final Long[] array = new Long[size];
            while (i < size) {
                this.bind(acquire, iterator.next());
                array[i] = acquire.executeInsert();
                ++i;
            }
            return array;
        }
        finally {
            this.release(acquire);
        }
    }
    
    public final Long[] insertAndReturnIdsArrayBox(final T[] array) {
        Intrinsics.checkNotNullParameter((Object)array, "entities");
        final SupportSQLiteStatement acquire = this.acquire();
        final Iterator iterator = ArrayIteratorKt.iterator((Object[])array);
        int i = 0;
        try {
            final int length = array.length;
            final Long[] array2 = new Long[length];
            while (i < length) {
                this.bind(acquire, (T)iterator.next());
                array2[i] = acquire.executeInsert();
                ++i;
            }
            return array2;
        }
        finally {
            this.release(acquire);
        }
    }
    
    public final List<Long> insertAndReturnIdsList(final Collection<? extends T> collection) {
        Intrinsics.checkNotNullParameter((Object)collection, "entities");
        final SupportSQLiteStatement acquire = this.acquire();
        try {
            final List listBuilder = CollectionsKt.createListBuilder();
            final Iterator iterator = collection.iterator();
            while (iterator.hasNext()) {
                this.bind(acquire, iterator.next());
                listBuilder.add(acquire.executeInsert());
            }
            return CollectionsKt.build(listBuilder);
        }
        finally {
            this.release(acquire);
        }
    }
    
    public final List<Long> insertAndReturnIdsList(final T[] array) {
        Intrinsics.checkNotNullParameter((Object)array, "entities");
        final SupportSQLiteStatement acquire = this.acquire();
        try {
            final List listBuilder = CollectionsKt.createListBuilder();
            for (int i = 0; i < array.length; ++i) {
                this.bind(acquire, array[i]);
                listBuilder.add(acquire.executeInsert());
            }
            return CollectionsKt.build(listBuilder);
        }
        finally {
            this.release(acquire);
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.room;

import kotlin.collections.CollectionsKt;
import java.util.List;
import java.util.Collection;
import java.util.Iterator;
import kotlin.text.StringsKt;
import android.os.Build$VERSION;
import android.database.sqlite.SQLiteConstraintException;
import kotlin.jvm.internal.Intrinsics;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000R\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u0011\n\u0000\n\u0002\u0010\u001c\n\u0000\n\u0002\u0010\t\n\u0002\b\u0002\n\u0002\u0010\u0016\n\u0000\n\u0002\u0010\u001e\n\u0002\b\u0004\n\u0002\u0010 \n\u0002\b\u0002\b\u0007\u0018\u0000*\u0004\b\u0000\u0010\u00012\u00020\u0002B!\u0012\f\u0010\u0003\u001a\b\u0012\u0004\u0012\u00028\u00000\u0004\u0012\f\u0010\u0005\u001a\b\u0012\u0004\u0012\u00028\u00000\u0006¢\u0006\u0002\u0010\u0007J\u0010\u0010\b\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000bH\u0002J\u0013\u0010\f\u001a\u00020\t2\u0006\u0010\r\u001a\u00028\u0000¢\u0006\u0002\u0010\u000eJ\u001b\u0010\f\u001a\u00020\t2\u000e\u0010\u000f\u001a\n\u0012\u0006\b\u0001\u0012\u00028\u00000\u0010¢\u0006\u0002\u0010\u0011J\u0014\u0010\f\u001a\u00020\t2\f\u0010\u000f\u001a\b\u0012\u0004\u0012\u00028\u00000\u0012J\u0013\u0010\u0013\u001a\u00020\u00142\u0006\u0010\r\u001a\u00028\u0000¢\u0006\u0002\u0010\u0015J\u001b\u0010\u0016\u001a\u00020\u00172\u000e\u0010\u000f\u001a\n\u0012\u0006\b\u0001\u0012\u00028\u00000\u0010¢\u0006\u0002\u0010\u0018J\u0014\u0010\u0016\u001a\u00020\u00172\f\u0010\u000f\u001a\b\u0012\u0004\u0012\u00028\u00000\u0019J#\u0010\u001a\u001a\n\u0012\u0006\b\u0001\u0012\u00020\u00140\u00102\u000e\u0010\u000f\u001a\n\u0012\u0006\b\u0001\u0012\u00028\u00000\u0010¢\u0006\u0002\u0010\u001bJ!\u0010\u001a\u001a\n\u0012\u0006\b\u0001\u0012\u00020\u00140\u00102\f\u0010\u000f\u001a\b\u0012\u0004\u0012\u00028\u00000\u0019¢\u0006\u0002\u0010\u001cJ!\u0010\u001d\u001a\b\u0012\u0004\u0012\u00020\u00140\u001e2\u000e\u0010\u000f\u001a\n\u0012\u0006\b\u0001\u0012\u00028\u00000\u0010¢\u0006\u0002\u0010\u001fJ\u001a\u0010\u001d\u001a\b\u0012\u0004\u0012\u00020\u00140\u001e2\f\u0010\u000f\u001a\b\u0012\u0004\u0012\u00028\u00000\u0019R\u0014\u0010\u0003\u001a\b\u0012\u0004\u0012\u00028\u00000\u0004X\u0082\u0004¢\u0006\u0002\n\u0000R\u0014\u0010\u0005\u001a\b\u0012\u0004\u0012\u00028\u00000\u0006X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006 " }, d2 = { "Landroidx/room/EntityUpsertionAdapter;", "T", "", "insertionAdapter", "Landroidx/room/EntityInsertionAdapter;", "updateAdapter", "Landroidx/room/EntityDeletionOrUpdateAdapter;", "(Landroidx/room/EntityInsertionAdapter;Landroidx/room/EntityDeletionOrUpdateAdapter;)V", "checkUniquenessException", "", "ex", "Landroid/database/sqlite/SQLiteConstraintException;", "upsert", "entity", "(Ljava/lang/Object;)V", "entities", "", "([Ljava/lang/Object;)V", "", "upsertAndReturnId", "", "(Ljava/lang/Object;)J", "upsertAndReturnIdsArray", "", "([Ljava/lang/Object;)[J", "", "upsertAndReturnIdsArrayBox", "([Ljava/lang/Object;)[Ljava/lang/Long;", "(Ljava/util/Collection;)[Ljava/lang/Long;", "upsertAndReturnIdsList", "", "([Ljava/lang/Object;)Ljava/util/List;", "room-runtime_release" }, k = 1, mv = { 1, 7, 1 }, xi = 48)
public final class EntityUpsertionAdapter<T>
{
    private final EntityInsertionAdapter<T> insertionAdapter;
    private final EntityDeletionOrUpdateAdapter<T> updateAdapter;
    
    public EntityUpsertionAdapter(final EntityInsertionAdapter<T> insertionAdapter, final EntityDeletionOrUpdateAdapter<T> updateAdapter) {
        Intrinsics.checkNotNullParameter((Object)insertionAdapter, "insertionAdapter");
        Intrinsics.checkNotNullParameter((Object)updateAdapter, "updateAdapter");
        this.insertionAdapter = insertionAdapter;
        this.updateAdapter = updateAdapter;
    }
    
    private final void checkUniquenessException(final SQLiteConstraintException ex) {
        final String message = ex.getMessage();
        if (message != null) {
            if (Build$VERSION.SDK_INT <= 19) {
                if (!StringsKt.contains((CharSequence)message, (CharSequence)"unique", true)) {
                    throw ex;
                }
            }
            else if (!StringsKt.contains((CharSequence)message, (CharSequence)"1555", true)) {
                throw ex;
            }
            return;
        }
        throw ex;
    }
    
    public final void upsert(Iterable<? extends T> next) {
        Intrinsics.checkNotNullParameter((Object)next, "entities");
        final Iterator<? extends T> iterator = ((Iterable<? extends T>)next).iterator();
        while (iterator.hasNext()) {
            next = (T)iterator.next();
            try {
                this.insertionAdapter.insert(next);
            }
            catch (final SQLiteConstraintException ex) {
                this.checkUniquenessException(ex);
                this.updateAdapter.handle(next);
            }
        }
    }
    
    public final void upsert(final T t) {
        try {
            this.insertionAdapter.insert(t);
        }
        catch (final SQLiteConstraintException ex) {
            this.checkUniquenessException(ex);
            this.updateAdapter.handle(t);
        }
    }
    
    public final void upsert(final T[] array) {
        Intrinsics.checkNotNullParameter((Object)array, "entities");
        for (final T t : array) {
            try {
                this.insertionAdapter.insert(t);
            }
            catch (final SQLiteConstraintException ex) {
                this.checkUniquenessException(ex);
                this.updateAdapter.handle(t);
            }
        }
    }
    
    public final long upsertAndReturnId(final T t) {
        long insertAndReturnId;
        try {
            insertAndReturnId = this.insertionAdapter.insertAndReturnId(t);
        }
        catch (final SQLiteConstraintException ex) {
            this.checkUniquenessException(ex);
            this.updateAdapter.handle(t);
            insertAndReturnId = -1L;
        }
        return insertAndReturnId;
    }
    
    public final long[] upsertAndReturnIdsArray(final Collection<? extends T> collection) {
        Intrinsics.checkNotNullParameter((Object)collection, "entities");
        final Iterator iterator = collection.iterator();
        final int size = collection.size();
        final long[] array = new long[size];
        for (int i = 0; i < size; ++i) {
            final Object next = iterator.next();
            long insertAndReturnId;
            try {
                insertAndReturnId = this.insertionAdapter.insertAndReturnId((T)next);
            }
            catch (final SQLiteConstraintException ex) {
                this.checkUniquenessException(ex);
                this.updateAdapter.handle((T)next);
                insertAndReturnId = -1L;
            }
            array[i] = insertAndReturnId;
        }
        return array;
    }
    
    public final long[] upsertAndReturnIdsArray(final T[] array) {
        Intrinsics.checkNotNullParameter((Object)array, "entities");
        final int length = array.length;
        final long[] array2 = new long[length];
        for (int i = 0; i < length; ++i) {
            long insertAndReturnId;
            try {
                insertAndReturnId = this.insertionAdapter.insertAndReturnId(array[i]);
            }
            catch (final SQLiteConstraintException ex) {
                this.checkUniquenessException(ex);
                this.updateAdapter.handle(array[i]);
                insertAndReturnId = -1L;
            }
            array2[i] = insertAndReturnId;
        }
        return array2;
    }
    
    public final Long[] upsertAndReturnIdsArrayBox(final Collection<? extends T> collection) {
        Intrinsics.checkNotNullParameter((Object)collection, "entities");
        final Iterator<? extends T> iterator = collection.iterator();
        final int size = collection.size();
        final Long[] array = new Long[size];
        for (int i = 0; i < size; ++i) {
            final T next = (T)iterator.next();
            long insertAndReturnId;
            try {
                insertAndReturnId = this.insertionAdapter.insertAndReturnId(next);
            }
            catch (final SQLiteConstraintException ex) {
                this.checkUniquenessException(ex);
                this.updateAdapter.handle(next);
                insertAndReturnId = -1L;
            }
            array[i] = insertAndReturnId;
        }
        return array;
    }
    
    public final Long[] upsertAndReturnIdsArrayBox(final T[] array) {
        Intrinsics.checkNotNullParameter((Object)array, "entities");
        final int length = array.length;
        final Long[] array2 = new Long[length];
        for (int i = 0; i < length; ++i) {
            long insertAndReturnId;
            try {
                insertAndReturnId = this.insertionAdapter.insertAndReturnId(array[i]);
            }
            catch (final SQLiteConstraintException ex) {
                this.checkUniquenessException(ex);
                this.updateAdapter.handle(array[i]);
                insertAndReturnId = -1L;
            }
            array2[i] = insertAndReturnId;
        }
        return array2;
    }
    
    public final List<Long> upsertAndReturnIdsList(Collection<? extends T> next) {
        Intrinsics.checkNotNullParameter((Object)next, "entities");
        final List listBuilder = CollectionsKt.createListBuilder();
        final Iterator iterator = ((Iterable)next).iterator();
        while (iterator.hasNext()) {
            next = (T)iterator.next();
            try {
                listBuilder.add(this.insertionAdapter.insertAndReturnId(next));
            }
            catch (final SQLiteConstraintException ex) {
                this.checkUniquenessException(ex);
                this.updateAdapter.handle(next);
                listBuilder.add(-1L);
            }
        }
        return CollectionsKt.build(listBuilder);
    }
    
    public final List<Long> upsertAndReturnIdsList(final T[] array) {
        Intrinsics.checkNotNullParameter((Object)array, "entities");
        final List listBuilder = CollectionsKt.createListBuilder();
        for (final T t : array) {
            try {
                listBuilder.add(this.insertionAdapter.insertAndReturnId(t));
            }
            catch (final SQLiteConstraintException ex) {
                this.checkUniquenessException(ex);
                this.updateAdapter.handle(t);
                listBuilder.add(-1L);
            }
        }
        return CollectionsKt.build(listBuilder);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.room;

import android.os.Parcel;
import android.os.Binder;
import android.os.IBinder;
import android.os.RemoteException;
import android.os.IInterface;

public interface IMultiInstanceInvalidationService extends IInterface
{
    public static final String DESCRIPTOR = "androidx.room.IMultiInstanceInvalidationService";
    
    void broadcastInvalidation(final int p0, final String[] p1) throws RemoteException;
    
    int registerCallback(final IMultiInstanceInvalidationCallback p0, final String p1) throws RemoteException;
    
    void unregisterCallback(final IMultiInstanceInvalidationCallback p0, final int p1) throws RemoteException;
    
    public static class Default implements IMultiInstanceInvalidationService
    {
        public IBinder asBinder() {
            return null;
        }
        
        @Override
        public void broadcastInvalidation(final int n, final String[] array) throws RemoteException {
        }
        
        @Override
        public int registerCallback(final IMultiInstanceInvalidationCallback multiInstanceInvalidationCallback, final String s) throws RemoteException {
            return 0;
        }
        
        @Override
        public void unregisterCallback(final IMultiInstanceInvalidationCallback multiInstanceInvalidationCallback, final int n) throws RemoteException {
        }
    }
    
    public abstract static class Stub extends Binder implements IMultiInstanceInvalidationService
    {
        static final int TRANSACTION_broadcastInvalidation = 3;
        static final int TRANSACTION_registerCallback = 1;
        static final int TRANSACTION_unregisterCallback = 2;
        
        public Stub() {
            this.attachInterface((IInterface)this, "androidx.room.IMultiInstanceInvalidationService");
        }
        
        public static IMultiInstanceInvalidationService asInterface(final IBinder binder) {
            if (binder == null) {
                return null;
            }
            final IInterface queryLocalInterface = binder.queryLocalInterface("androidx.room.IMultiInstanceInvalidationService");
            if (queryLocalInterface != null && queryLocalInterface instanceof IMultiInstanceInvalidationService) {
                return (IMultiInstanceInvalidationService)queryLocalInterface;
            }
            return new Proxy(binder);
        }
        
        public IBinder asBinder() {
            return (IBinder)this;
        }
        
        public boolean onTransact(int registerCallback, final Parcel parcel, final Parcel parcel2, final int n) throws RemoteException {
            if (registerCallback >= 1 && registerCallback <= 16777215) {
                parcel.enforceInterface("androidx.room.IMultiInstanceInvalidationService");
            }
            if (registerCallback != 1598968902) {
                if (registerCallback != 1) {
                    if (registerCallback != 2) {
                        if (registerCallback != 3) {
                            return super.onTransact(registerCallback, parcel, parcel2, n);
                        }
                        this.broadcastInvalidation(parcel.readInt(), parcel.createStringArray());
                    }
                    else {
                        this.unregisterCallback(IMultiInstanceInvalidationCallback.Stub.asInterface(parcel.readStrongBinder()), parcel.readInt());
                        parcel2.writeNoException();
                    }
                }
                else {
                    registerCallback = this.registerCallback(IMultiInstanceInvalidationCallback.Stub.asInterface(parcel.readStrongBinder()), parcel.readString());
                    parcel2.writeNoException();
                    parcel2.writeInt(registerCallback);
                }
                return true;
            }
            parcel2.writeString("androidx.room.IMultiInstanceInvalidationService");
            return true;
        }
        
        private static class Proxy implements IMultiInstanceInvalidationService
        {
            private IBinder mRemote;
            
            Proxy(final IBinder mRemote) {
                this.mRemote = mRemote;
            }
            
            public IBinder asBinder() {
                return this.mRemote;
            }
            
            @Override
            public void broadcastInvalidation(final int n, final String[] array) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("androidx.room.IMultiInstanceInvalidationService");
                    obtain.writeInt(n);
                    obtain.writeStringArray(array);
                    this.mRemote.transact(3, obtain, (Parcel)null, 1);
                }
                finally {
                    obtain.recycle();
                }
            }
            
            public String getInterfaceDescriptor() {
                return "androidx.room.IMultiInstanceInvalidationService";
            }
            
            @Override
            public int registerCallback(final IMultiInstanceInvalidationCallback multiInstanceInvalidationCallback, final String s) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                final Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("androidx.room.IMultiInstanceInvalidationService");
                    obtain.writeStrongInterface((IInterface)multiInstanceInvalidationCallback);
                    obtain.writeString(s);
                    this.mRemote.transact(1, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readInt();
                }
                finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
            
            @Override
            public void unregisterCallback(final IMultiInstanceInvalidationCallback multiInstanceInvalidationCallback, final int n) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                final Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("androidx.room.IMultiInstanceInvalidationService");
                    obtain.writeStrongInterface((IInterface)multiInstanceInvalidationCallback);
                    obtain.writeInt(n);
                    this.mRemote.transact(2, obtain, obtain2, 0);
                    obtain2.readException();
                }
                finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
        }
    }
}

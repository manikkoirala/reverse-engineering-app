// 
// Decompiled by Procyon v0.6.0
// 

package androidx.room;

import kotlin.annotation.AnnotationTarget;
import kotlin.Metadata;
import java.lang.annotation.ElementType;
import java.lang.annotation.Target;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;

@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.TYPE, ElementType.METHOD })
@Metadata(d1 = { "\u0000\n\n\u0002\u0018\u0002\n\u0002\u0010\u001b\n\u0000\b\u0087\u0002\u0018\u00002\u00020\u0001B\u0000¨\u0006\u0002" }, d2 = { "Landroidx/room/ExperimentalRoomApi;", "", "room-runtime_release" }, k = 1, mv = { 1, 7, 1 }, xi = 48)
@kotlin.annotation.Target(allowedTargets = { AnnotationTarget.CLASS, AnnotationTarget.FUNCTION })
public @interface ExperimentalRoomApi {
}

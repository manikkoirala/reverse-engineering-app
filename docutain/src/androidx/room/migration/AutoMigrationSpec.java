// 
// Decompiled by Procyon v0.6.0
// 

package androidx.room.migration;

import androidx.sqlite.db.SupportSQLiteDatabase;

public interface AutoMigrationSpec
{
    void onPostMigrate(final SupportSQLiteDatabase p0);
}

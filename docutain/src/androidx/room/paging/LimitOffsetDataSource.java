// 
// Decompiled by Procyon v0.6.0
// 

package androidx.room.paging;

import androidx.paging.PositionalDataSource$LoadRangeCallback;
import androidx.paging.PositionalDataSource$LoadRangeParams;
import java.util.Collections;
import androidx.paging.PositionalDataSource$LoadInitialCallback;
import androidx.paging.PositionalDataSource$LoadInitialParams;
import java.util.List;
import android.database.Cursor;
import androidx.sqlite.db.SupportSQLiteQuery;
import java.util.Set;
import androidx.room.RoomSQLiteQuery;
import java.util.concurrent.atomic.AtomicBoolean;
import androidx.room.InvalidationTracker;
import androidx.room.RoomDatabase;
import androidx.paging.PositionalDataSource;

public abstract class LimitOffsetDataSource<T> extends PositionalDataSource<T>
{
    private final String mCountQuery;
    private final RoomDatabase mDb;
    private final boolean mInTransaction;
    private final String mLimitOffsetQuery;
    private final InvalidationTracker.Observer mObserver;
    private final AtomicBoolean mRegisteredObserver;
    private final RoomSQLiteQuery mSourceQuery;
    
    protected LimitOffsetDataSource(final RoomDatabase mDb, final RoomSQLiteQuery mSourceQuery, final boolean mInTransaction, final boolean b, final String... array) {
        this.mRegisteredObserver = new AtomicBoolean(false);
        this.mDb = mDb;
        this.mSourceQuery = mSourceQuery;
        this.mInTransaction = mInTransaction;
        final StringBuilder sb = new StringBuilder();
        sb.append("SELECT COUNT(*) FROM ( ");
        sb.append(mSourceQuery.getSql());
        sb.append(" )");
        this.mCountQuery = sb.toString();
        final StringBuilder sb2 = new StringBuilder();
        sb2.append("SELECT * FROM ( ");
        sb2.append(mSourceQuery.getSql());
        sb2.append(" ) LIMIT ? OFFSET ?");
        this.mLimitOffsetQuery = sb2.toString();
        this.mObserver = new InvalidationTracker.Observer(this, array) {
            final LimitOffsetDataSource this$0;
            
            @Override
            public void onInvalidated(final Set<String> set) {
                this.this$0.invalidate();
            }
        };
        if (b) {
            this.registerObserverIfNecessary();
        }
    }
    
    protected LimitOffsetDataSource(final RoomDatabase roomDatabase, final RoomSQLiteQuery roomSQLiteQuery, final boolean b, final String... array) {
        this(roomDatabase, roomSQLiteQuery, b, true, array);
    }
    
    protected LimitOffsetDataSource(final RoomDatabase roomDatabase, final SupportSQLiteQuery supportSQLiteQuery, final boolean b, final boolean b2, final String... array) {
        this(roomDatabase, RoomSQLiteQuery.copyFrom(supportSQLiteQuery), b, b2, array);
    }
    
    protected LimitOffsetDataSource(final RoomDatabase roomDatabase, final SupportSQLiteQuery supportSQLiteQuery, final boolean b, final String... array) {
        this(roomDatabase, RoomSQLiteQuery.copyFrom(supportSQLiteQuery), b, array);
    }
    
    private RoomSQLiteQuery getSQLiteQuery(final int n, final int n2) {
        final RoomSQLiteQuery acquire = RoomSQLiteQuery.acquire(this.mLimitOffsetQuery, this.mSourceQuery.getArgCount() + 2);
        acquire.copyArgumentsFrom(this.mSourceQuery);
        acquire.bindLong(acquire.getArgCount() - 1, n2);
        acquire.bindLong(acquire.getArgCount(), n);
        return acquire;
    }
    
    private void registerObserverIfNecessary() {
        if (this.mRegisteredObserver.compareAndSet(false, true)) {
            this.mDb.getInvalidationTracker().addWeakObserver(this.mObserver);
        }
    }
    
    protected abstract List<T> convertRows(final Cursor p0);
    
    public int countItems() {
        this.registerObserverIfNecessary();
        final RoomSQLiteQuery acquire = RoomSQLiteQuery.acquire(this.mCountQuery, this.mSourceQuery.getArgCount());
        acquire.copyArgumentsFrom(this.mSourceQuery);
        final Cursor query = this.mDb.query(acquire);
        try {
            if (query.moveToFirst()) {
                return query.getInt(0);
            }
            return 0;
        }
        finally {
            query.close();
            acquire.release();
        }
    }
    
    public boolean isInvalid() {
        this.registerObserverIfNecessary();
        this.mDb.getInvalidationTracker().refreshVersionsSync();
        return super.isInvalid();
    }
    
    public void loadInitial(PositionalDataSource$LoadInitialParams query, PositionalDataSource$LoadInitialCallback<T> positionalDataSource$LoadInitialParams) {
        this.registerObserverIfNecessary();
        final List<Object> emptyList = Collections.emptyList();
        this.mDb.beginTransaction();
        final List list = null;
        List list2 = null;
        final PositionalDataSource$LoadInitialParams positionalDataSource$LoadInitialParams2 = null;
        RoomSQLiteQuery roomSQLiteQuery = null;
        List list4 = null;
        Label_0165: {
            try {
                final int countItems = this.countItems();
                int computeInitialLoadPosition = 0;
                RoomSQLiteQuery sqLiteQuery = null;
                List<Object> list3 = null;
                Label_0120: {
                    if (countItems != 0) {
                        computeInitialLoadPosition = computeInitialLoadPosition(query, countItems);
                        sqLiteQuery = this.getSQLiteQuery(computeInitialLoadPosition, computeInitialLoadSize(query, computeInitialLoadPosition, countItems));
                        query = positionalDataSource$LoadInitialParams2;
                        try {
                            final Cursor cursor = (Cursor)(query = (PositionalDataSource$LoadInitialParams)this.mDb.query(sqLiteQuery));
                            final Object convertRows = this.convertRows(cursor);
                            query = (PositionalDataSource$LoadInitialParams)cursor;
                            this.mDb.setTransactionSuccessful();
                            query = (PositionalDataSource$LoadInitialParams)convertRows;
                            break Label_0120;
                        }
                        finally {
                            positionalDataSource$LoadInitialParams = query;
                            list3 = list2;
                            break Label_0165;
                        }
                    }
                    computeInitialLoadPosition = 0;
                    sqLiteQuery = null;
                    list2 = list;
                    list3 = emptyList;
                }
                if (list2 != null) {
                    ((Cursor)list2).close();
                }
                this.mDb.endTransaction();
                if (sqLiteQuery != null) {
                    sqLiteQuery.release();
                }
                ((PositionalDataSource$LoadInitialCallback)positionalDataSource$LoadInitialParams).onResult((List)list3, computeInitialLoadPosition, countItems);
                return;
            }
            finally {
                roomSQLiteQuery = null;
                list4 = list2;
            }
        }
        if (list4 != null) {
            ((Cursor)list4).close();
        }
        this.mDb.endTransaction();
        if (roomSQLiteQuery != null) {
            roomSQLiteQuery.release();
        }
    }
    
    public List<T> loadRange(final int n, final int n2) {
        final RoomSQLiteQuery sqLiteQuery = this.getSQLiteQuery(n, n2);
        if (this.mInTransaction) {
            this.mDb.beginTransaction();
            Cursor query = null;
            try {
                final Cursor cursor = query = this.mDb.query(sqLiteQuery);
                final List<T> convertRows = this.convertRows(cursor);
                query = cursor;
                this.mDb.setTransactionSuccessful();
                return convertRows;
            }
            finally {
                if (query != null) {
                    query.close();
                }
                this.mDb.endTransaction();
                sqLiteQuery.release();
            }
        }
        final Cursor query2 = this.mDb.query(sqLiteQuery);
        try {
            return this.convertRows(query2);
        }
        finally {
            query2.close();
            sqLiteQuery.release();
        }
    }
    
    public void loadRange(final PositionalDataSource$LoadRangeParams positionalDataSource$LoadRangeParams, final PositionalDataSource$LoadRangeCallback<T> positionalDataSource$LoadRangeCallback) {
        positionalDataSource$LoadRangeCallback.onResult((List)this.loadRange(positionalDataSource$LoadRangeParams.startPosition, positionalDataSource$LoadRangeParams.loadSize));
    }
}

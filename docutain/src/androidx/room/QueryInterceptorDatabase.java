// 
// Decompiled by Procyon v0.6.0
// 

package androidx.room;

import java.util.Locale;
import android.os.CancellationSignal;
import androidx.sqlite.db.SupportSQLiteProgram;
import android.database.Cursor;
import android.content.ContentValues;
import android.util.Pair;
import java.util.Collection;
import java.util.ArrayList;
import androidx.sqlite.db.SupportSQLiteStatement;
import android.database.sqlite.SQLiteTransactionListener;
import kotlin.collections.ArraysKt;
import kotlin.collections.CollectionsKt;
import kotlin.jvm.internal.Intrinsics;
import java.util.List;
import androidx.sqlite.db.SupportSQLiteQuery;
import java.util.concurrent.Executor;
import kotlin.Metadata;
import androidx.sqlite.db.SupportSQLiteDatabase;

@Metadata(d1 = { "\u0000\u0088\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0007\n\u0002\u0010\t\n\u0002\b\n\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0011\n\u0002\u0010\u0000\n\u0002\b\f\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\n\b\u0000\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0001\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006¢\u0006\u0002\u0010\u0007J\b\u0010'\u001a\u00020(H\u0016J\b\u0010)\u001a\u00020(H\u0016J\u0010\u0010*\u001a\u00020(2\u0006\u0010+\u001a\u00020,H\u0016J\u0010\u0010-\u001a\u00020(2\u0006\u0010+\u001a\u00020,H\u0016J\t\u0010.\u001a\u00020(H\u0096\u0001J\u0010\u0010/\u001a\u0002002\u0006\u00101\u001a\u00020\u000bH\u0016J4\u00102\u001a\u00020\"2\u0006\u00103\u001a\u00020\u000b2\b\u00104\u001a\u0004\u0018\u00010\u000b2\u0012\u00105\u001a\u000e\u0012\b\b\u0001\u0012\u0004\u0018\u000107\u0018\u000106H\u0096\u0001¢\u0006\u0002\u00108J\t\u00109\u001a\u00020(H\u0097\u0001J\t\u0010:\u001a\u00020\u000fH\u0096\u0001J\b\u0010;\u001a\u00020(H\u0016J,\u0010<\u001a\u00020(2\u0006\u00101\u001a\u00020\u000b2\u0014\b\u0001\u0010=\u001a\u000e\u0012\b\b\u0001\u0012\u0004\u0018\u000107\u0018\u000106H\u0096\u0001¢\u0006\u0002\u0010>J\u0010\u0010?\u001a\u00020(2\u0006\u00101\u001a\u00020\u000bH\u0016J'\u0010?\u001a\u00020(2\u0006\u00101\u001a\u00020\u000b2\u0010\u0010=\u001a\f\u0012\b\b\u0001\u0012\u0004\u0018\u00010706H\u0016¢\u0006\u0002\u0010>J\t\u0010@\u001a\u00020\u000fH\u0096\u0001J!\u0010A\u001a\u00020\u00172\u0006\u00103\u001a\u00020\u000b2\u0006\u0010B\u001a\u00020\"2\u0006\u0010C\u001a\u00020DH\u0096\u0001J\u0011\u0010E\u001a\u00020\u000f2\u0006\u0010F\u001a\u00020\"H\u0096\u0001J\u0010\u0010G\u001a\u00020H2\u0006\u0010G\u001a\u00020IH\u0016J\u001a\u0010G\u001a\u00020H2\u0006\u0010G\u001a\u00020I2\b\u0010J\u001a\u0004\u0018\u00010KH\u0016J\u0010\u0010G\u001a\u00020H2\u0006\u0010G\u001a\u00020\u000bH\u0016J'\u0010G\u001a\u00020H2\u0006\u0010G\u001a\u00020\u000b2\u0010\u0010=\u001a\f\u0012\b\b\u0001\u0012\u0004\u0018\u00010706H\u0016¢\u0006\u0002\u0010LJ\u0011\u0010M\u001a\u00020(2\u0006\u0010N\u001a\u00020\u000fH\u0097\u0001J\u0011\u0010O\u001a\u00020(2\u0006\u0010P\u001a\u00020QH\u0096\u0001J\u0011\u0010R\u001a\u00020(2\u0006\u0010S\u001a\u00020\"H\u0096\u0001J\u0011\u0010T\u001a\u00020\u00172\u0006\u0010U\u001a\u00020\u0017H\u0096\u0001J\b\u0010V\u001a\u00020(H\u0016JD\u0010W\u001a\u00020\"2\u0006\u00103\u001a\u00020\u000b2\u0006\u0010B\u001a\u00020\"2\u0006\u0010C\u001a\u00020D2\b\u00104\u001a\u0004\u0018\u00010\u000b2\u0012\u00105\u001a\u000e\u0012\b\b\u0001\u0012\u0004\u0018\u000107\u0018\u000106H\u0096\u0001¢\u0006\u0002\u0010XJ\t\u0010Y\u001a\u00020\u000fH\u0096\u0001J\u0011\u0010Y\u001a\u00020\u000f2\u0006\u0010Z\u001a\u00020\u0017H\u0096\u0001R(\u0010\b\u001a\u0016\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u000b\u0012\u0004\u0012\u00020\u000b0\n\u0018\u00010\t8VX\u0096\u0005¢\u0006\u0006\u001a\u0004\b\f\u0010\rR\u000e\u0010\u0002\u001a\u00020\u0001X\u0082\u0004¢\u0006\u0002\n\u0000R\u0012\u0010\u000e\u001a\u00020\u000fX\u0096\u0005¢\u0006\u0006\u001a\u0004\b\u000e\u0010\u0010R\u0012\u0010\u0011\u001a\u00020\u000fX\u0096\u0005¢\u0006\u0006\u001a\u0004\b\u0011\u0010\u0010R\u0014\u0010\u0012\u001a\u00020\u000f8VX\u0096\u0005¢\u0006\u0006\u001a\u0004\b\u0012\u0010\u0010R\u0012\u0010\u0013\u001a\u00020\u000fX\u0096\u0005¢\u0006\u0006\u001a\u0004\b\u0013\u0010\u0010R\u0012\u0010\u0014\u001a\u00020\u000fX\u0096\u0005¢\u0006\u0006\u001a\u0004\b\u0014\u0010\u0010R\u0014\u0010\u0015\u001a\u00020\u000f8WX\u0096\u0005¢\u0006\u0006\u001a\u0004\b\u0015\u0010\u0010R\u0012\u0010\u0016\u001a\u00020\u0017X\u0096\u0005¢\u0006\u0006\u001a\u0004\b\u0018\u0010\u0019R\u0018\u0010\u001a\u001a\u00020\u0017X\u0096\u000f¢\u0006\f\u001a\u0004\b\u001b\u0010\u0019\"\u0004\b\u001c\u0010\u001dR\u0014\u0010\u001e\u001a\u0004\u0018\u00010\u000bX\u0096\u0005¢\u0006\u0006\u001a\u0004\b\u001f\u0010 R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004¢\u0006\u0002\n\u0000R\u0018\u0010!\u001a\u00020\"X\u0096\u000f¢\u0006\f\u001a\u0004\b#\u0010$\"\u0004\b%\u0010&¨\u0006[" }, d2 = { "Landroidx/room/QueryInterceptorDatabase;", "Landroidx/sqlite/db/SupportSQLiteDatabase;", "delegate", "queryCallbackExecutor", "Ljava/util/concurrent/Executor;", "queryCallback", "Landroidx/room/RoomDatabase$QueryCallback;", "(Landroidx/sqlite/db/SupportSQLiteDatabase;Ljava/util/concurrent/Executor;Landroidx/room/RoomDatabase$QueryCallback;)V", "attachedDbs", "", "Landroid/util/Pair;", "", "getAttachedDbs", "()Ljava/util/List;", "isDatabaseIntegrityOk", "", "()Z", "isDbLockedByCurrentThread", "isExecPerConnectionSQLSupported", "isOpen", "isReadOnly", "isWriteAheadLoggingEnabled", "maximumSize", "", "getMaximumSize", "()J", "pageSize", "getPageSize", "setPageSize", "(J)V", "path", "getPath", "()Ljava/lang/String;", "version", "", "getVersion", "()I", "setVersion", "(I)V", "beginTransaction", "", "beginTransactionNonExclusive", "beginTransactionWithListener", "transactionListener", "Landroid/database/sqlite/SQLiteTransactionListener;", "beginTransactionWithListenerNonExclusive", "close", "compileStatement", "Landroidx/sqlite/db/SupportSQLiteStatement;", "sql", "delete", "table", "whereClause", "whereArgs", "", "", "(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I", "disableWriteAheadLogging", "enableWriteAheadLogging", "endTransaction", "execPerConnectionSQL", "bindArgs", "(Ljava/lang/String;[Ljava/lang/Object;)V", "execSQL", "inTransaction", "insert", "conflictAlgorithm", "values", "Landroid/content/ContentValues;", "needUpgrade", "newVersion", "query", "Landroid/database/Cursor;", "Landroidx/sqlite/db/SupportSQLiteQuery;", "cancellationSignal", "Landroid/os/CancellationSignal;", "(Ljava/lang/String;[Ljava/lang/Object;)Landroid/database/Cursor;", "setForeignKeyConstraintsEnabled", "enabled", "setLocale", "locale", "Ljava/util/Locale;", "setMaxSqlCacheSize", "cacheSize", "setMaximumSize", "numBytes", "setTransactionSuccessful", "update", "(Ljava/lang/String;ILandroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/Object;)I", "yieldIfContendedSafely", "sleepAfterYieldDelayMillis", "room-runtime_release" }, k = 1, mv = { 1, 7, 1 }, xi = 48)
public final class QueryInterceptorDatabase implements SupportSQLiteDatabase
{
    private final SupportSQLiteDatabase delegate;
    private final RoomDatabase.QueryCallback queryCallback;
    private final Executor queryCallbackExecutor;
    
    public QueryInterceptorDatabase(final SupportSQLiteDatabase delegate, final Executor queryCallbackExecutor, final RoomDatabase.QueryCallback queryCallback) {
        Intrinsics.checkNotNullParameter((Object)delegate, "delegate");
        Intrinsics.checkNotNullParameter((Object)queryCallbackExecutor, "queryCallbackExecutor");
        Intrinsics.checkNotNullParameter((Object)queryCallback, "queryCallback");
        this.delegate = delegate;
        this.queryCallbackExecutor = queryCallbackExecutor;
        this.queryCallback = queryCallback;
    }
    
    private static final void beginTransaction$lambda$0(final QueryInterceptorDatabase queryInterceptorDatabase) {
        Intrinsics.checkNotNullParameter((Object)queryInterceptorDatabase, "this$0");
        queryInterceptorDatabase.queryCallback.onQuery("BEGIN EXCLUSIVE TRANSACTION", CollectionsKt.emptyList());
    }
    
    private static final void beginTransactionNonExclusive$lambda$1(final QueryInterceptorDatabase queryInterceptorDatabase) {
        Intrinsics.checkNotNullParameter((Object)queryInterceptorDatabase, "this$0");
        queryInterceptorDatabase.queryCallback.onQuery("BEGIN DEFERRED TRANSACTION", CollectionsKt.emptyList());
    }
    
    private static final void beginTransactionWithListener$lambda$2(final QueryInterceptorDatabase queryInterceptorDatabase) {
        Intrinsics.checkNotNullParameter((Object)queryInterceptorDatabase, "this$0");
        queryInterceptorDatabase.queryCallback.onQuery("BEGIN EXCLUSIVE TRANSACTION", CollectionsKt.emptyList());
    }
    
    private static final void beginTransactionWithListenerNonExclusive$lambda$3(final QueryInterceptorDatabase queryInterceptorDatabase) {
        Intrinsics.checkNotNullParameter((Object)queryInterceptorDatabase, "this$0");
        queryInterceptorDatabase.queryCallback.onQuery("BEGIN DEFERRED TRANSACTION", CollectionsKt.emptyList());
    }
    
    private static final void endTransaction$lambda$4(final QueryInterceptorDatabase queryInterceptorDatabase) {
        Intrinsics.checkNotNullParameter((Object)queryInterceptorDatabase, "this$0");
        queryInterceptorDatabase.queryCallback.onQuery("END TRANSACTION", CollectionsKt.emptyList());
    }
    
    private static final void execSQL$lambda$10(final QueryInterceptorDatabase queryInterceptorDatabase, final String s) {
        Intrinsics.checkNotNullParameter((Object)queryInterceptorDatabase, "this$0");
        Intrinsics.checkNotNullParameter((Object)s, "$sql");
        queryInterceptorDatabase.queryCallback.onQuery(s, CollectionsKt.emptyList());
    }
    
    private static final void execSQL$lambda$11(final QueryInterceptorDatabase queryInterceptorDatabase, final String s, final List list) {
        Intrinsics.checkNotNullParameter((Object)queryInterceptorDatabase, "this$0");
        Intrinsics.checkNotNullParameter((Object)s, "$sql");
        Intrinsics.checkNotNullParameter((Object)list, "$inputArguments");
        queryInterceptorDatabase.queryCallback.onQuery(s, list);
    }
    
    private static final void query$lambda$6(final QueryInterceptorDatabase queryInterceptorDatabase, final String s) {
        Intrinsics.checkNotNullParameter((Object)queryInterceptorDatabase, "this$0");
        Intrinsics.checkNotNullParameter((Object)s, "$query");
        queryInterceptorDatabase.queryCallback.onQuery(s, CollectionsKt.emptyList());
    }
    
    private static final void query$lambda$7(final QueryInterceptorDatabase queryInterceptorDatabase, final String s, final Object[] array) {
        Intrinsics.checkNotNullParameter((Object)queryInterceptorDatabase, "this$0");
        Intrinsics.checkNotNullParameter((Object)s, "$query");
        Intrinsics.checkNotNullParameter((Object)array, "$bindArgs");
        queryInterceptorDatabase.queryCallback.onQuery(s, ArraysKt.toList(array));
    }
    
    private static final void query$lambda$8(final QueryInterceptorDatabase queryInterceptorDatabase, final SupportSQLiteQuery supportSQLiteQuery, final QueryInterceptorProgram queryInterceptorProgram) {
        Intrinsics.checkNotNullParameter((Object)queryInterceptorDatabase, "this$0");
        Intrinsics.checkNotNullParameter((Object)supportSQLiteQuery, "$query");
        Intrinsics.checkNotNullParameter((Object)queryInterceptorProgram, "$queryInterceptorProgram");
        queryInterceptorDatabase.queryCallback.onQuery(supportSQLiteQuery.getSql(), queryInterceptorProgram.getBindArgsCache$room_runtime_release());
    }
    
    private static final void query$lambda$9(final QueryInterceptorDatabase queryInterceptorDatabase, final SupportSQLiteQuery supportSQLiteQuery, final QueryInterceptorProgram queryInterceptorProgram) {
        Intrinsics.checkNotNullParameter((Object)queryInterceptorDatabase, "this$0");
        Intrinsics.checkNotNullParameter((Object)supportSQLiteQuery, "$query");
        Intrinsics.checkNotNullParameter((Object)queryInterceptorProgram, "$queryInterceptorProgram");
        queryInterceptorDatabase.queryCallback.onQuery(supportSQLiteQuery.getSql(), queryInterceptorProgram.getBindArgsCache$room_runtime_release());
    }
    
    private static final void setTransactionSuccessful$lambda$5(final QueryInterceptorDatabase queryInterceptorDatabase) {
        Intrinsics.checkNotNullParameter((Object)queryInterceptorDatabase, "this$0");
        queryInterceptorDatabase.queryCallback.onQuery("TRANSACTION SUCCESSFUL", CollectionsKt.emptyList());
    }
    
    @Override
    public void beginTransaction() {
        this.queryCallbackExecutor.execute(new QueryInterceptorDatabase$$ExternalSyntheticLambda6(this));
        this.delegate.beginTransaction();
    }
    
    @Override
    public void beginTransactionNonExclusive() {
        this.queryCallbackExecutor.execute(new QueryInterceptorDatabase$$ExternalSyntheticLambda0(this));
        this.delegate.beginTransactionNonExclusive();
    }
    
    @Override
    public void beginTransactionWithListener(final SQLiteTransactionListener sqLiteTransactionListener) {
        Intrinsics.checkNotNullParameter((Object)sqLiteTransactionListener, "transactionListener");
        this.queryCallbackExecutor.execute(new QueryInterceptorDatabase$$ExternalSyntheticLambda11(this));
        this.delegate.beginTransactionWithListener(sqLiteTransactionListener);
    }
    
    @Override
    public void beginTransactionWithListenerNonExclusive(final SQLiteTransactionListener sqLiteTransactionListener) {
        Intrinsics.checkNotNullParameter((Object)sqLiteTransactionListener, "transactionListener");
        this.queryCallbackExecutor.execute(new QueryInterceptorDatabase$$ExternalSyntheticLambda4(this));
        this.delegate.beginTransactionWithListenerNonExclusive(sqLiteTransactionListener);
    }
    
    @Override
    public void close() {
        this.delegate.close();
    }
    
    @Override
    public SupportSQLiteStatement compileStatement(final String s) {
        Intrinsics.checkNotNullParameter((Object)s, "sql");
        return new QueryInterceptorStatement(this.delegate.compileStatement(s), s, this.queryCallbackExecutor, this.queryCallback);
    }
    
    @Override
    public int delete(final String s, final String s2, final Object[] array) {
        Intrinsics.checkNotNullParameter((Object)s, "table");
        return this.delegate.delete(s, s2, array);
    }
    
    @Override
    public void disableWriteAheadLogging() {
        this.delegate.disableWriteAheadLogging();
    }
    
    @Override
    public boolean enableWriteAheadLogging() {
        return this.delegate.enableWriteAheadLogging();
    }
    
    @Override
    public void endTransaction() {
        this.queryCallbackExecutor.execute(new QueryInterceptorDatabase$$ExternalSyntheticLambda8(this));
        this.delegate.endTransaction();
    }
    
    @Override
    public void execPerConnectionSQL(final String s, final Object[] array) {
        Intrinsics.checkNotNullParameter((Object)s, "sql");
        this.delegate.execPerConnectionSQL(s, array);
    }
    
    @Override
    public void execSQL(final String s) {
        Intrinsics.checkNotNullParameter((Object)s, "sql");
        this.queryCallbackExecutor.execute(new QueryInterceptorDatabase$$ExternalSyntheticLambda10(this, s));
        this.delegate.execSQL(s);
    }
    
    @Override
    public void execSQL(final String s, final Object[] array) {
        Intrinsics.checkNotNullParameter((Object)s, "sql");
        Intrinsics.checkNotNullParameter((Object)array, "bindArgs");
        final List list = new ArrayList();
        list.addAll(CollectionsKt.listOf((Object)array));
        this.queryCallbackExecutor.execute(new QueryInterceptorDatabase$$ExternalSyntheticLambda9(this, s, list));
        this.delegate.execSQL(s, new List[] { list });
    }
    
    @Override
    public List<Pair<String, String>> getAttachedDbs() {
        return this.delegate.getAttachedDbs();
    }
    
    @Override
    public long getMaximumSize() {
        return this.delegate.getMaximumSize();
    }
    
    @Override
    public long getPageSize() {
        return this.delegate.getPageSize();
    }
    
    @Override
    public String getPath() {
        return this.delegate.getPath();
    }
    
    @Override
    public int getVersion() {
        return this.delegate.getVersion();
    }
    
    @Override
    public boolean inTransaction() {
        return this.delegate.inTransaction();
    }
    
    @Override
    public long insert(final String s, final int n, final ContentValues contentValues) {
        Intrinsics.checkNotNullParameter((Object)s, "table");
        Intrinsics.checkNotNullParameter((Object)contentValues, "values");
        return this.delegate.insert(s, n, contentValues);
    }
    
    @Override
    public boolean isDatabaseIntegrityOk() {
        return this.delegate.isDatabaseIntegrityOk();
    }
    
    @Override
    public boolean isDbLockedByCurrentThread() {
        return this.delegate.isDbLockedByCurrentThread();
    }
    
    @Override
    public boolean isExecPerConnectionSQLSupported() {
        return this.delegate.isExecPerConnectionSQLSupported();
    }
    
    @Override
    public boolean isOpen() {
        return this.delegate.isOpen();
    }
    
    @Override
    public boolean isReadOnly() {
        return this.delegate.isReadOnly();
    }
    
    @Override
    public boolean isWriteAheadLoggingEnabled() {
        return this.delegate.isWriteAheadLoggingEnabled();
    }
    
    @Override
    public boolean needUpgrade(final int n) {
        return this.delegate.needUpgrade(n);
    }
    
    @Override
    public Cursor query(final SupportSQLiteQuery supportSQLiteQuery) {
        Intrinsics.checkNotNullParameter((Object)supportSQLiteQuery, "query");
        final QueryInterceptorProgram queryInterceptorProgram = new QueryInterceptorProgram();
        supportSQLiteQuery.bindTo(queryInterceptorProgram);
        this.queryCallbackExecutor.execute(new QueryInterceptorDatabase$$ExternalSyntheticLambda5(this, supportSQLiteQuery, queryInterceptorProgram));
        return this.delegate.query(supportSQLiteQuery);
    }
    
    @Override
    public Cursor query(final SupportSQLiteQuery supportSQLiteQuery, final CancellationSignal cancellationSignal) {
        Intrinsics.checkNotNullParameter((Object)supportSQLiteQuery, "query");
        final QueryInterceptorProgram queryInterceptorProgram = new QueryInterceptorProgram();
        supportSQLiteQuery.bindTo(queryInterceptorProgram);
        this.queryCallbackExecutor.execute(new QueryInterceptorDatabase$$ExternalSyntheticLambda1(this, supportSQLiteQuery, queryInterceptorProgram));
        return this.delegate.query(supportSQLiteQuery);
    }
    
    @Override
    public Cursor query(final String s) {
        Intrinsics.checkNotNullParameter((Object)s, "query");
        this.queryCallbackExecutor.execute(new QueryInterceptorDatabase$$ExternalSyntheticLambda7(this, s));
        return this.delegate.query(s);
    }
    
    @Override
    public Cursor query(final String s, final Object[] array) {
        Intrinsics.checkNotNullParameter((Object)s, "query");
        Intrinsics.checkNotNullParameter((Object)array, "bindArgs");
        this.queryCallbackExecutor.execute(new QueryInterceptorDatabase$$ExternalSyntheticLambda2(this, s, array));
        return this.delegate.query(s, array);
    }
    
    @Override
    public void setForeignKeyConstraintsEnabled(final boolean foreignKeyConstraintsEnabled) {
        this.delegate.setForeignKeyConstraintsEnabled(foreignKeyConstraintsEnabled);
    }
    
    @Override
    public void setLocale(final Locale locale) {
        Intrinsics.checkNotNullParameter((Object)locale, "locale");
        this.delegate.setLocale(locale);
    }
    
    @Override
    public void setMaxSqlCacheSize(final int maxSqlCacheSize) {
        this.delegate.setMaxSqlCacheSize(maxSqlCacheSize);
    }
    
    @Override
    public long setMaximumSize(final long maximumSize) {
        return this.delegate.setMaximumSize(maximumSize);
    }
    
    @Override
    public void setPageSize(final long pageSize) {
        this.delegate.setPageSize(pageSize);
    }
    
    @Override
    public void setTransactionSuccessful() {
        this.queryCallbackExecutor.execute(new QueryInterceptorDatabase$$ExternalSyntheticLambda3(this));
        this.delegate.setTransactionSuccessful();
    }
    
    @Override
    public void setVersion(final int version) {
        this.delegate.setVersion(version);
    }
    
    @Override
    public int update(final String s, final int n, final ContentValues contentValues, final String s2, final Object[] array) {
        Intrinsics.checkNotNullParameter((Object)s, "table");
        Intrinsics.checkNotNullParameter((Object)contentValues, "values");
        return this.delegate.update(s, n, contentValues, s2, array);
    }
    
    @Override
    public boolean yieldIfContendedSafely() {
        return this.delegate.yieldIfContendedSafely();
    }
    
    @Override
    public boolean yieldIfContendedSafely(final long n) {
        return this.delegate.yieldIfContendedSafely(n);
    }
}

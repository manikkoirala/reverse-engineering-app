// 
// Decompiled by Procyon v0.6.0
// 

package androidx.room;

import kotlin.Deprecated;
import java.util.Iterator;
import java.util.List;
import androidx.room.migration.Migration;
import kotlin.io.CloseableKt;
import android.database.Cursor;
import java.io.Closeable;
import android.os.Build$VERSION;
import androidx.sqlite.db.SimpleSQLiteQuery;
import androidx.sqlite.db.SupportSQLiteQuery;
import androidx.sqlite.db.SupportSQLiteDatabase;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.Metadata;
import androidx.sqlite.db.SupportSQLiteOpenHelper;

@Metadata(d1 = { "\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\b\n\u0002\b\b\b\u0017\u0018\u0000 \u00192\u00020\u0001:\u0003\u0019\u001a\u001bB\u001f\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007¢\u0006\u0002\u0010\bB%\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\t\u001a\u00020\u0007\u0012\u0006\u0010\u0006\u001a\u00020\u0007¢\u0006\u0002\u0010\nJ\u0010\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u000eH\u0002J\u0010\u0010\u000f\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u000eH\u0002J\u0010\u0010\u0010\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u000eH\u0016J\u0010\u0010\u0011\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u000eH\u0016J \u0010\u0012\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u000e2\u0006\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u0014H\u0016J\u0010\u0010\u0016\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u000eH\u0016J \u0010\u0017\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u000e2\u0006\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u0014H\u0016J\u0010\u0010\u0018\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u000eH\u0002R\u0010\u0010\u0002\u001a\u0004\u0018\u00010\u0003X\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\u0007X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u001c" }, d2 = { "Landroidx/room/RoomOpenHelper;", "Landroidx/sqlite/db/SupportSQLiteOpenHelper$Callback;", "configuration", "Landroidx/room/DatabaseConfiguration;", "delegate", "Landroidx/room/RoomOpenHelper$Delegate;", "legacyHash", "", "(Landroidx/room/DatabaseConfiguration;Landroidx/room/RoomOpenHelper$Delegate;Ljava/lang/String;)V", "identityHash", "(Landroidx/room/DatabaseConfiguration;Landroidx/room/RoomOpenHelper$Delegate;Ljava/lang/String;Ljava/lang/String;)V", "checkIdentity", "", "db", "Landroidx/sqlite/db/SupportSQLiteDatabase;", "createMasterTableIfNotExists", "onConfigure", "onCreate", "onDowngrade", "oldVersion", "", "newVersion", "onOpen", "onUpgrade", "updateIdentity", "Companion", "Delegate", "ValidationResult", "room-runtime_release" }, k = 1, mv = { 1, 7, 1 }, xi = 48)
public class RoomOpenHelper extends Callback
{
    public static final Companion Companion;
    private DatabaseConfiguration configuration;
    private final Delegate delegate;
    private final String identityHash;
    private final String legacyHash;
    
    static {
        Companion = new Companion(null);
    }
    
    public RoomOpenHelper(final DatabaseConfiguration databaseConfiguration, final Delegate delegate, final String s) {
        Intrinsics.checkNotNullParameter((Object)databaseConfiguration, "configuration");
        Intrinsics.checkNotNullParameter((Object)delegate, "delegate");
        Intrinsics.checkNotNullParameter((Object)s, "legacyHash");
        this(databaseConfiguration, delegate, "", s);
    }
    
    public RoomOpenHelper(final DatabaseConfiguration configuration, final Delegate delegate, final String identityHash, final String legacyHash) {
        Intrinsics.checkNotNullParameter((Object)configuration, "configuration");
        Intrinsics.checkNotNullParameter((Object)delegate, "delegate");
        Intrinsics.checkNotNullParameter((Object)identityHash, "identityHash");
        Intrinsics.checkNotNullParameter((Object)legacyHash, "legacyHash");
        super(delegate.version);
        this.configuration = configuration;
        this.delegate = delegate;
        this.identityHash = identityHash;
        this.legacyHash = legacyHash;
    }
    
    private final void checkIdentity(SupportSQLiteDatabase string) {
        if (RoomOpenHelper.Companion.hasRoomMasterTable$room_runtime_release(string)) {
            Object query = string.query(new SimpleSQLiteQuery("SELECT identity_hash FROM room_master_table WHERE id = 42 LIMIT 1"));
            final int sdk_INT = Build$VERSION.SDK_INT;
            string = null;
            Label_0131: {
                if (sdk_INT > 15) {
                    query = query;
                    try {
                        final Cursor cursor = (Cursor)query;
                        if (cursor.moveToFirst()) {
                            cursor.getString(0);
                        }
                        else {
                            final String s = null;
                        }
                        CloseableKt.closeFinally((Closeable)query, (Throwable)null);
                        break Label_0131;
                    }
                    finally {
                        try {}
                        finally {
                            CloseableKt.closeFinally((Closeable)query, (Throwable)string);
                        }
                    }
                }
                try {
                    if (((Cursor)query).moveToFirst()) {
                        string = (SupportSQLiteDatabase)((Cursor)query).getString(0);
                    }
                    else {
                        final String s2 = null;
                    }
                    ((Cursor)query).close();
                    if (Intrinsics.areEqual((Object)this.identityHash, (Object)string)) {
                        return;
                    }
                    if (Intrinsics.areEqual((Object)this.legacyHash, (Object)string)) {
                        return;
                    }
                    query = new StringBuilder();
                    ((StringBuilder)query).append("Room cannot verify the data integrity. Looks like you've changed schema but forgot to update the version number. You can simply fix this by increasing the version number. Expected identity hash: ");
                    ((StringBuilder)query).append(this.identityHash);
                    ((StringBuilder)query).append(", found: ");
                    ((StringBuilder)query).append((String)string);
                    throw new IllegalStateException(((StringBuilder)query).toString());
                }
                finally {
                    ((Cursor)query).close();
                }
            }
        }
        final ValidationResult onValidateSchema = this.delegate.onValidateSchema(string);
        if (!onValidateSchema.isValid) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Pre-packaged database has an invalid schema: ");
            sb.append(onValidateSchema.expectedFoundMsg);
            throw new IllegalStateException(sb.toString());
        }
        this.delegate.onPostMigrate(string);
        this.updateIdentity(string);
    }
    
    private final void createMasterTableIfNotExists(final SupportSQLiteDatabase supportSQLiteDatabase) {
        supportSQLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
    }
    
    private final void updateIdentity(final SupportSQLiteDatabase supportSQLiteDatabase) {
        this.createMasterTableIfNotExists(supportSQLiteDatabase);
        supportSQLiteDatabase.execSQL(RoomMasterTable.createInsertQuery(this.identityHash));
    }
    
    @Override
    public void onConfigure(final SupportSQLiteDatabase supportSQLiteDatabase) {
        Intrinsics.checkNotNullParameter((Object)supportSQLiteDatabase, "db");
        super.onConfigure(supportSQLiteDatabase);
    }
    
    @Override
    public void onCreate(final SupportSQLiteDatabase supportSQLiteDatabase) {
        Intrinsics.checkNotNullParameter((Object)supportSQLiteDatabase, "db");
        final boolean hasEmptySchema$room_runtime_release = RoomOpenHelper.Companion.hasEmptySchema$room_runtime_release(supportSQLiteDatabase);
        this.delegate.createAllTables(supportSQLiteDatabase);
        if (!hasEmptySchema$room_runtime_release) {
            final ValidationResult onValidateSchema = this.delegate.onValidateSchema(supportSQLiteDatabase);
            if (!onValidateSchema.isValid) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Pre-packaged database has an invalid schema: ");
                sb.append(onValidateSchema.expectedFoundMsg);
                throw new IllegalStateException(sb.toString());
            }
        }
        this.updateIdentity(supportSQLiteDatabase);
        this.delegate.onCreate(supportSQLiteDatabase);
    }
    
    @Override
    public void onDowngrade(final SupportSQLiteDatabase supportSQLiteDatabase, final int n, final int n2) {
        Intrinsics.checkNotNullParameter((Object)supportSQLiteDatabase, "db");
        this.onUpgrade(supportSQLiteDatabase, n, n2);
    }
    
    @Override
    public void onOpen(final SupportSQLiteDatabase supportSQLiteDatabase) {
        Intrinsics.checkNotNullParameter((Object)supportSQLiteDatabase, "db");
        super.onOpen(supportSQLiteDatabase);
        this.checkIdentity(supportSQLiteDatabase);
        this.delegate.onOpen(supportSQLiteDatabase);
        this.configuration = null;
    }
    
    @Override
    public void onUpgrade(final SupportSQLiteDatabase supportSQLiteDatabase, final int i, final int j) {
        Intrinsics.checkNotNullParameter((Object)supportSQLiteDatabase, "db");
        final DatabaseConfiguration configuration = this.configuration;
        int n = 0;
        if (configuration != null) {
            final List<Migration> migrationPath = configuration.migrationContainer.findMigrationPath(i, j);
            n = n;
            if (migrationPath != null) {
                this.delegate.onPreMigrate(supportSQLiteDatabase);
                final Iterator iterator = migrationPath.iterator();
                while (iterator.hasNext()) {
                    ((Migration)iterator.next()).migrate(supportSQLiteDatabase);
                }
                final ValidationResult onValidateSchema = this.delegate.onValidateSchema(supportSQLiteDatabase);
                if (!onValidateSchema.isValid) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Migration didn't properly handle: ");
                    sb.append(onValidateSchema.expectedFoundMsg);
                    throw new IllegalStateException(sb.toString());
                }
                this.delegate.onPostMigrate(supportSQLiteDatabase);
                this.updateIdentity(supportSQLiteDatabase);
                n = 1;
            }
        }
        if (n == 0) {
            final DatabaseConfiguration configuration2 = this.configuration;
            if (configuration2 == null || configuration2.isMigrationRequired(i, j)) {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("A migration from ");
                sb2.append(i);
                sb2.append(" to ");
                sb2.append(j);
                sb2.append(" was required but not found. Please provide the necessary Migration path via RoomDatabase.Builder.addMigration(Migration ...) or allow for destructive migrations via one of the RoomDatabase.Builder.fallbackToDestructiveMigration* methods.");
                throw new IllegalStateException(sb2.toString());
            }
            this.delegate.dropAllTables(supportSQLiteDatabase);
            this.delegate.createAllTables(supportSQLiteDatabase);
        }
    }
    
    @Metadata(d1 = { "\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0015\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0000¢\u0006\u0002\b\u0007J\u0015\u0010\b\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0000¢\u0006\u0002\b\t¨\u0006\n" }, d2 = { "Landroidx/room/RoomOpenHelper$Companion;", "", "()V", "hasEmptySchema", "", "db", "Landroidx/sqlite/db/SupportSQLiteDatabase;", "hasEmptySchema$room_runtime_release", "hasRoomMasterTable", "hasRoomMasterTable$room_runtime_release", "room-runtime_release" }, k = 1, mv = { 1, 7, 1 }, xi = 48)
    public static final class Companion
    {
        private Companion() {
        }
        
        public final boolean hasEmptySchema$room_runtime_release(SupportSQLiteDatabase supportSQLiteDatabase) {
            Intrinsics.checkNotNullParameter((Object)supportSQLiteDatabase, "db");
            final Cursor query = supportSQLiteDatabase.query("SELECT count(*) FROM sqlite_master WHERE name != 'android_metadata'");
            final int sdk_INT = Build$VERSION.SDK_INT;
            boolean b = true;
            final boolean b2 = true;
            if (sdk_INT > 15) {
                supportSQLiteDatabase = (SupportSQLiteDatabase)query;
                try {
                    final Cursor cursor = (Cursor)supportSQLiteDatabase;
                    final boolean b3 = cursor.moveToFirst() && cursor.getInt(0) == 0 && b2;
                    CloseableKt.closeFinally((Closeable)supportSQLiteDatabase, (Throwable)null);
                    return b3;
                }
                finally {
                    try {}
                    finally {
                        final Throwable t;
                        CloseableKt.closeFinally((Closeable)supportSQLiteDatabase, t);
                    }
                }
            }
            try {
                if (!query.moveToFirst() || query.getInt(0) != 0) {
                    b = false;
                }
                return b;
            }
            finally {
                query.close();
            }
        }
        
        public final boolean hasRoomMasterTable$room_runtime_release(SupportSQLiteDatabase supportSQLiteDatabase) {
            Intrinsics.checkNotNullParameter((Object)supportSQLiteDatabase, "db");
            final Cursor query = supportSQLiteDatabase.query("SELECT 1 FROM sqlite_master WHERE type = 'table' AND name='room_master_table'");
            final int sdk_INT = Build$VERSION.SDK_INT;
            boolean b = true;
            final boolean b2 = true;
            if (sdk_INT > 15) {
                supportSQLiteDatabase = (SupportSQLiteDatabase)query;
                try {
                    final Cursor cursor = (Cursor)supportSQLiteDatabase;
                    final boolean b3 = cursor.moveToFirst() && cursor.getInt(0) != 0 && b2;
                    CloseableKt.closeFinally((Closeable)supportSQLiteDatabase, (Throwable)null);
                    return b3;
                }
                finally {
                    try {}
                    finally {
                        final Throwable t;
                        CloseableKt.closeFinally((Closeable)supportSQLiteDatabase, t);
                    }
                }
            }
            try {
                if (!query.moveToFirst() || query.getInt(0) == 0) {
                    b = false;
                }
                return b;
            }
            finally {
                query.close();
            }
        }
    }
    
    @Metadata(d1 = { "\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0003\b'\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u0010\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bH&J\u0010\u0010\t\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bH&J\u0010\u0010\n\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bH&J\u0010\u0010\u000b\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bH&J\u0010\u0010\f\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bH\u0016J\u0010\u0010\r\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bH\u0016J\u0010\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\bH\u0016J\u0010\u0010\u0011\u001a\u00020\u00062\u0006\u0010\u0010\u001a\u00020\bH\u0015R\u0010\u0010\u0002\u001a\u00020\u00038\u0006X\u0087\u0004¢\u0006\u0002\n\u0000¨\u0006\u0012" }, d2 = { "Landroidx/room/RoomOpenHelper$Delegate;", "", "version", "", "(I)V", "createAllTables", "", "database", "Landroidx/sqlite/db/SupportSQLiteDatabase;", "dropAllTables", "onCreate", "onOpen", "onPostMigrate", "onPreMigrate", "onValidateSchema", "Landroidx/room/RoomOpenHelper$ValidationResult;", "db", "validateMigration", "room-runtime_release" }, k = 1, mv = { 1, 7, 1 }, xi = 48)
    public abstract static class Delegate
    {
        public final int version;
        
        public Delegate(final int version) {
            this.version = version;
        }
        
        public abstract void createAllTables(final SupportSQLiteDatabase p0);
        
        public abstract void dropAllTables(final SupportSQLiteDatabase p0);
        
        public abstract void onCreate(final SupportSQLiteDatabase p0);
        
        public abstract void onOpen(final SupportSQLiteDatabase p0);
        
        public void onPostMigrate(final SupportSQLiteDatabase supportSQLiteDatabase) {
            Intrinsics.checkNotNullParameter((Object)supportSQLiteDatabase, "database");
        }
        
        public void onPreMigrate(final SupportSQLiteDatabase supportSQLiteDatabase) {
            Intrinsics.checkNotNullParameter((Object)supportSQLiteDatabase, "database");
        }
        
        public ValidationResult onValidateSchema(final SupportSQLiteDatabase supportSQLiteDatabase) {
            Intrinsics.checkNotNullParameter((Object)supportSQLiteDatabase, "db");
            this.validateMigration(supportSQLiteDatabase);
            return new ValidationResult(true, null);
        }
        
        @Deprecated(message = "Use [onValidateSchema(SupportSQLiteDatabase)]")
        protected void validateMigration(final SupportSQLiteDatabase supportSQLiteDatabase) {
            Intrinsics.checkNotNullParameter((Object)supportSQLiteDatabase, "db");
            throw new UnsupportedOperationException("validateMigration is deprecated");
        }
    }
    
    @Metadata(d1 = { "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\b\u0017\u0018\u00002\u00020\u0001B\u0017\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\b\u0010\u0004\u001a\u0004\u0018\u00010\u0005¢\u0006\u0002\u0010\u0006R\u0012\u0010\u0004\u001a\u0004\u0018\u00010\u00058\u0006X\u0087\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\u0002\u001a\u00020\u00038\u0006X\u0087\u0004¢\u0006\u0002\n\u0000¨\u0006\u0007" }, d2 = { "Landroidx/room/RoomOpenHelper$ValidationResult;", "", "isValid", "", "expectedFoundMsg", "", "(ZLjava/lang/String;)V", "room-runtime_release" }, k = 1, mv = { 1, 7, 1 }, xi = 48)
    public static class ValidationResult
    {
        public final String expectedFoundMsg;
        public final boolean isValid;
        
        public ValidationResult(final boolean isValid, final String expectedFoundMsg) {
            this.isValid = isValid;
            this.expectedFoundMsg = expectedFoundMsg;
        }
    }
}

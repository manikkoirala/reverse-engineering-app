// 
// Decompiled by Procyon v0.6.0
// 

package androidx.savedstate;

import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.Iterator;
import java.util.ArrayList;
import android.os.Bundle;
import java.util.List;
import androidx.lifecycle.LifecycleObserver;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleOwner;
import java.lang.reflect.Constructor;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.Metadata;
import androidx.lifecycle.LifecycleEventObserver;

@Metadata(d1 = { "\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\b\u0000\u0018\u0000 \u000e2\u00020\u0001:\u0002\u000e\u000fB\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u0018\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\nH\u0016J\u0010\u0010\u000b\u001a\u00020\u00062\u0006\u0010\f\u001a\u00020\rH\u0002R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u0010" }, d2 = { "Landroidx/savedstate/Recreator;", "Landroidx/lifecycle/LifecycleEventObserver;", "owner", "Landroidx/savedstate/SavedStateRegistryOwner;", "(Landroidx/savedstate/SavedStateRegistryOwner;)V", "onStateChanged", "", "source", "Landroidx/lifecycle/LifecycleOwner;", "event", "Landroidx/lifecycle/Lifecycle$Event;", "reflectiveNew", "className", "", "Companion", "SavedStateProvider", "savedstate_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
public final class Recreator implements LifecycleEventObserver
{
    public static final String CLASSES_KEY = "classes_to_restore";
    public static final String COMPONENT_KEY = "androidx.savedstate.Restarter";
    public static final Companion Companion;
    private final SavedStateRegistryOwner owner;
    
    static {
        Companion = new Companion(null);
    }
    
    public Recreator(final SavedStateRegistryOwner owner) {
        Intrinsics.checkNotNullParameter((Object)owner, "owner");
        this.owner = owner;
    }
    
    private final void reflectiveNew(String str) {
        try {
            Object o = Class.forName(str, false, Recreator.class.getClassLoader()).asSubclass(SavedStateRegistry.AutoRecreated.class);
            Intrinsics.checkNotNullExpressionValue(o, "{\n                Class.\u2026class.java)\n            }");
            try {
                final Constructor declaredConstructor = ((Class)o).getDeclaredConstructor((Class[])new Class[0]);
                declaredConstructor.setAccessible(true);
                try {
                    o = declaredConstructor.newInstance(new Object[0]);
                    Intrinsics.checkNotNullExpressionValue(o, "{\n                constr\u2026wInstance()\n            }");
                    o = o;
                    ((SavedStateRegistry.AutoRecreated)o).onRecreated(this.owner);
                }
                catch (final Exception ex) {
                    o = new StringBuilder();
                    ((StringBuilder)o).append("Failed to instantiate ");
                    ((StringBuilder)o).append(str);
                    throw new RuntimeException(((StringBuilder)o).toString(), ex);
                }
            }
            catch (final NoSuchMethodException ex2) {
                str = (String)new StringBuilder();
                ((StringBuilder)str).append("Class ");
                ((StringBuilder)str).append(((Class)o).getSimpleName());
                ((StringBuilder)str).append(" must have default constructor in order to be automatically recreated");
                throw new IllegalStateException(((StringBuilder)str).toString(), ex2);
            }
        }
        catch (final ClassNotFoundException ex3) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Class ");
            sb.append(str);
            sb.append(" wasn't found");
            throw new RuntimeException(sb.toString(), ex3);
        }
    }
    
    @Override
    public void onStateChanged(final LifecycleOwner lifecycleOwner, final Lifecycle.Event event) {
        Intrinsics.checkNotNullParameter((Object)lifecycleOwner, "source");
        Intrinsics.checkNotNullParameter((Object)event, "event");
        if (event != Lifecycle.Event.ON_CREATE) {
            throw new AssertionError((Object)"Next event must be ON_CREATE");
        }
        lifecycleOwner.getLifecycle().removeObserver(this);
        final Bundle consumeRestoredStateForKey = this.owner.getSavedStateRegistry().consumeRestoredStateForKey("androidx.savedstate.Restarter");
        if (consumeRestoredStateForKey == null) {
            return;
        }
        final ArrayList stringArrayList = consumeRestoredStateForKey.getStringArrayList("classes_to_restore");
        if (stringArrayList != null) {
            final Iterator iterator = stringArrayList.iterator();
            while (iterator.hasNext()) {
                this.reflectiveNew((String)iterator.next());
            }
            return;
        }
        throw new IllegalStateException("Bundle with restored state for the component \"androidx.savedstate.Restarter\" must contain list of strings by the key \"classes_to_restore\"");
    }
    
    @Metadata(d1 = { "\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0086T¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0086T¢\u0006\u0002\n\u0000¨\u0006\u0006" }, d2 = { "Landroidx/savedstate/Recreator$Companion;", "", "()V", "CLASSES_KEY", "", "COMPONENT_KEY", "savedstate_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
    public static final class Companion
    {
        private Companion() {
        }
    }
    
    @Metadata(d1 = { "\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010#\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\b\u0000\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u000e\u0010\b\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u0007J\b\u0010\u000b\u001a\u00020\fH\u0016R\u0014\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\r" }, d2 = { "Landroidx/savedstate/Recreator$SavedStateProvider;", "Landroidx/savedstate/SavedStateRegistry$SavedStateProvider;", "registry", "Landroidx/savedstate/SavedStateRegistry;", "(Landroidx/savedstate/SavedStateRegistry;)V", "classes", "", "", "add", "", "className", "saveState", "Landroid/os/Bundle;", "savedstate_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
    public static final class SavedStateProvider implements SavedStateRegistry.SavedStateProvider
    {
        private final Set<String> classes;
        
        public SavedStateProvider(final SavedStateRegistry savedStateRegistry) {
            Intrinsics.checkNotNullParameter((Object)savedStateRegistry, "registry");
            this.classes = new LinkedHashSet<String>();
            savedStateRegistry.registerSavedStateProvider("androidx.savedstate.Restarter", (SavedStateRegistry.SavedStateProvider)this);
        }
        
        public final void add(final String s) {
            Intrinsics.checkNotNullParameter((Object)s, "className");
            this.classes.add(s);
        }
        
        @Override
        public Bundle saveState() {
            final Bundle bundle = new Bundle();
            bundle.putStringArrayList("classes_to_restore", new ArrayList((Collection<? extends E>)this.classes));
            return bundle;
        }
    }
}

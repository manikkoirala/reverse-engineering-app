// 
// Decompiled by Procyon v0.6.0
// 

package androidx.savedstate;

import androidx.lifecycle.LifecycleObserver;
import java.util.Iterator;
import java.util.Map;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.DefaultConstructorMarker;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleOwner;
import android.os.Bundle;
import androidx.arch.core.internal.SafeIterableMap;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000J\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0007\u0018\u0000 )2\u00020\u0001:\u0003()*B\u0007\b\u0000¢\u0006\u0002\u0010\u0002J\u0012\u0010\u0014\u001a\u0004\u0018\u00010\u00132\u0006\u0010\u0015\u001a\u00020\u0007H\u0007J\u0010\u0010\u0016\u001a\u0004\u0018\u00010\b2\u0006\u0010\u0015\u001a\u00020\u0007J\u0015\u0010\u0017\u001a\u00020\u00182\u0006\u0010\u0019\u001a\u00020\u001aH\u0001¢\u0006\u0002\b\u001bJ\u0017\u0010\u001c\u001a\u00020\u00182\b\u0010\u001d\u001a\u0004\u0018\u00010\u0013H\u0001¢\u0006\u0002\b\u001eJ\u0010\u0010\u001f\u001a\u00020\u00182\u0006\u0010 \u001a\u00020\u0013H\u0007J\u0018\u0010!\u001a\u00020\u00182\u0006\u0010\u0015\u001a\u00020\u00072\u0006\u0010\"\u001a\u00020\bH\u0007J\u0018\u0010#\u001a\u00020\u00182\u000e\u0010$\u001a\n\u0012\u0006\b\u0001\u0012\u00020&0%H\u0007J\u0010\u0010'\u001a\u00020\u00182\u0006\u0010\u0015\u001a\u00020\u0007H\u0007R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u000e¢\u0006\u0002\n\u0000R\u001a\u0010\u0005\u001a\u000e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\b0\u0006X\u0082\u0004¢\u0006\u0002\n\u0000R\u001a\u0010\t\u001a\u00020\u0004X\u0080\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\n\u0010\u000b\"\u0004\b\f\u0010\rR \u0010\u000f\u001a\u00020\u00042\u0006\u0010\u000e\u001a\u00020\u00048G@BX\u0086\u000e¢\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u000bR\u0010\u0010\u0010\u001a\u0004\u0018\u00010\u0011X\u0082\u000e¢\u0006\u0002\n\u0000R\u0010\u0010\u0012\u001a\u0004\u0018\u00010\u0013X\u0082\u000e¢\u0006\u0002\n\u0000¨\u0006+" }, d2 = { "Landroidx/savedstate/SavedStateRegistry;", "", "()V", "attached", "", "components", "Landroidx/arch/core/internal/SafeIterableMap;", "", "Landroidx/savedstate/SavedStateRegistry$SavedStateProvider;", "isAllowingSavingState", "isAllowingSavingState$savedstate_release", "()Z", "setAllowingSavingState$savedstate_release", "(Z)V", "<set-?>", "isRestored", "recreatorProvider", "Landroidx/savedstate/Recreator$SavedStateProvider;", "restoredState", "Landroid/os/Bundle;", "consumeRestoredStateForKey", "key", "getSavedStateProvider", "performAttach", "", "lifecycle", "Landroidx/lifecycle/Lifecycle;", "performAttach$savedstate_release", "performRestore", "savedState", "performRestore$savedstate_release", "performSave", "outBundle", "registerSavedStateProvider", "provider", "runOnNextRecreation", "clazz", "Ljava/lang/Class;", "Landroidx/savedstate/SavedStateRegistry$AutoRecreated;", "unregisterSavedStateProvider", "AutoRecreated", "Companion", "SavedStateProvider", "savedstate_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
public final class SavedStateRegistry
{
    private static final Companion Companion;
    @Deprecated
    private static final String SAVED_COMPONENTS_KEY = "androidx.lifecycle.BundlableSavedStateRegistry.key";
    private boolean attached;
    private final SafeIterableMap<String, SavedStateProvider> components;
    private boolean isAllowingSavingState;
    private boolean isRestored;
    private Recreator.SavedStateProvider recreatorProvider;
    private Bundle restoredState;
    
    static {
        Companion = new Companion(null);
    }
    
    public SavedStateRegistry() {
        this.components = new SafeIterableMap<String, SavedStateProvider>();
        this.isAllowingSavingState = true;
    }
    
    private static final void performAttach$lambda$4(final SavedStateRegistry savedStateRegistry, final LifecycleOwner lifecycleOwner, final Lifecycle.Event event) {
        Intrinsics.checkNotNullParameter((Object)savedStateRegistry, "this$0");
        Intrinsics.checkNotNullParameter((Object)lifecycleOwner, "<anonymous parameter 0>");
        Intrinsics.checkNotNullParameter((Object)event, "event");
        if (event == Lifecycle.Event.ON_START) {
            savedStateRegistry.isAllowingSavingState = true;
        }
        else if (event == Lifecycle.Event.ON_STOP) {
            savedStateRegistry.isAllowingSavingState = false;
        }
    }
    
    public final Bundle consumeRestoredStateForKey(final String s) {
        Intrinsics.checkNotNullParameter((Object)s, "key");
        if (!this.isRestored) {
            throw new IllegalStateException("You can consumeRestoredStateForKey only after super.onCreate of corresponding component".toString());
        }
        final Bundle restoredState = this.restoredState;
        if (restoredState != null) {
            Bundle bundle;
            if (restoredState != null) {
                bundle = restoredState.getBundle(s);
            }
            else {
                bundle = null;
            }
            final Bundle restoredState2 = this.restoredState;
            if (restoredState2 != null) {
                restoredState2.remove(s);
            }
            final Bundle restoredState3 = this.restoredState;
            int n = 0;
            if (restoredState3 != null) {
                n = n;
                if (!restoredState3.isEmpty()) {
                    n = 1;
                }
            }
            if (n == 0) {
                this.restoredState = null;
            }
            return bundle;
        }
        return null;
    }
    
    public final SavedStateProvider getSavedStateProvider(final String s) {
        Intrinsics.checkNotNullParameter((Object)s, "key");
        for (final Map.Entry<String, V> entry : this.components) {
            Intrinsics.checkNotNullExpressionValue((Object)entry, "components");
            final String s2 = entry.getKey();
            final SavedStateProvider savedStateProvider = (SavedStateProvider)entry.getValue();
            if (Intrinsics.areEqual((Object)s2, (Object)s)) {
                return savedStateProvider;
            }
        }
        return null;
    }
    
    public final boolean isAllowingSavingState$savedstate_release() {
        return this.isAllowingSavingState;
    }
    
    public final boolean isRestored() {
        return this.isRestored;
    }
    
    public final void performAttach$savedstate_release(final Lifecycle lifecycle) {
        Intrinsics.checkNotNullParameter((Object)lifecycle, "lifecycle");
        if (this.attached ^ true) {
            lifecycle.addObserver(new SavedStateRegistry$$ExternalSyntheticLambda0(this));
            this.attached = true;
            return;
        }
        throw new IllegalStateException("SavedStateRegistry was already attached.".toString());
    }
    
    public final void performRestore$savedstate_release(Bundle bundle) {
        if (!this.attached) {
            throw new IllegalStateException("You must call performAttach() before calling performRestore(Bundle).".toString());
        }
        if (this.isRestored ^ true) {
            if (bundle != null) {
                bundle = bundle.getBundle("androidx.lifecycle.BundlableSavedStateRegistry.key");
            }
            else {
                bundle = null;
            }
            this.restoredState = bundle;
            this.isRestored = true;
            return;
        }
        throw new IllegalStateException("SavedStateRegistry was already restored.".toString());
    }
    
    public final void performSave(final Bundle bundle) {
        Intrinsics.checkNotNullParameter((Object)bundle, "outBundle");
        final Bundle bundle2 = new Bundle();
        final Bundle restoredState = this.restoredState;
        if (restoredState != null) {
            bundle2.putAll(restoredState);
        }
        final SafeIterableMap.IteratorWithAdditions iteratorWithAdditions = this.components.iteratorWithAdditions();
        Intrinsics.checkNotNullExpressionValue((Object)iteratorWithAdditions, "this.components.iteratorWithAdditions()");
        final Iterator iterator = iteratorWithAdditions;
        while (iterator.hasNext()) {
            final Map.Entry<String, V> entry = iterator.next();
            bundle2.putBundle((String)entry.getKey(), ((SavedStateProvider)entry.getValue()).saveState());
        }
        if (!bundle2.isEmpty()) {
            bundle.putBundle("androidx.lifecycle.BundlableSavedStateRegistry.key", bundle2);
        }
    }
    
    public final void registerSavedStateProvider(final String s, final SavedStateProvider savedStateProvider) {
        Intrinsics.checkNotNullParameter((Object)s, "key");
        Intrinsics.checkNotNullParameter((Object)savedStateProvider, "provider");
        if (this.components.putIfAbsent(s, savedStateProvider) == null) {
            return;
        }
        throw new IllegalArgumentException("SavedStateProvider with the given key is already registered".toString());
    }
    
    public final void runOnNextRecreation(Class<? extends AutoRecreated> name) {
        Intrinsics.checkNotNullParameter((Object)name, "clazz");
        if (this.isAllowingSavingState) {
            Recreator.SavedStateProvider recreatorProvider;
            if ((recreatorProvider = this.recreatorProvider) == null) {
                recreatorProvider = new Recreator.SavedStateProvider(this);
            }
            this.recreatorProvider = recreatorProvider;
            try {
                ((Class)name).getDeclaredConstructor((Class[])new Class[0]);
                final Recreator.SavedStateProvider recreatorProvider2 = this.recreatorProvider;
                if (recreatorProvider2 != null) {
                    name = ((Class)name).getName();
                    Intrinsics.checkNotNullExpressionValue((Object)name, "clazz.name");
                    recreatorProvider2.add(name);
                }
                return;
            }
            catch (final NoSuchMethodException ex) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Class ");
                sb.append(((Class)name).getSimpleName());
                sb.append(" must have default constructor in order to be automatically recreated");
                throw new IllegalArgumentException(sb.toString(), ex);
            }
        }
        throw new IllegalStateException("Can not perform this action after onSaveInstanceState".toString());
    }
    
    public final void setAllowingSavingState$savedstate_release(final boolean isAllowingSavingState) {
        this.isAllowingSavingState = isAllowingSavingState;
    }
    
    public final void unregisterSavedStateProvider(final String s) {
        Intrinsics.checkNotNullParameter((Object)s, "key");
        this.components.remove(s);
    }
    
    @Metadata(d1 = { "\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\bf\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&\u00f8\u0001\u0000\u0082\u0002\u0006\n\u0004\b!0\u0001¨\u0006\u0006\u00c0\u0006\u0001" }, d2 = { "Landroidx/savedstate/SavedStateRegistry$AutoRecreated;", "", "onRecreated", "", "owner", "Landroidx/savedstate/SavedStateRegistryOwner;", "savedstate_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
    public interface AutoRecreated
    {
        void onRecreated(final SavedStateRegistryOwner p0);
    }
    
    @Metadata(d1 = { "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\b\u0082\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082T¢\u0006\u0002\n\u0000¨\u0006\u0005" }, d2 = { "Landroidx/savedstate/SavedStateRegistry$Companion;", "", "()V", "SAVED_COMPONENTS_KEY", "", "savedstate_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
    private static final class Companion
    {
    }
    
    @Metadata(d1 = { "\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u00e6\u0080\u0001\u0018\u00002\u00020\u0001J\b\u0010\u0002\u001a\u00020\u0003H&\u00f8\u0001\u0000\u0082\u0002\u0006\n\u0004\b!0\u0001¨\u0006\u0004\u00c0\u0006\u0001" }, d2 = { "Landroidx/savedstate/SavedStateRegistry$SavedStateProvider;", "", "saveState", "Landroid/os/Bundle;", "savedstate_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
    public interface SavedStateProvider
    {
        Bundle saveState();
    }
}

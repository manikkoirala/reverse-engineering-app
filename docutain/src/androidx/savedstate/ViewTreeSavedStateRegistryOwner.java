// 
// Decompiled by Procyon v0.6.0
// 

package androidx.savedstate;

import kotlin.sequences.SequencesKt;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import android.view.View;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\u001a\u0013\u0010\u0000\u001a\u0004\u0018\u00010\u0001*\u00020\u0002H\u0007¢\u0006\u0002\b\u0003\u001a\u001b\u0010\u0004\u001a\u00020\u0005*\u00020\u00022\b\u0010\u0006\u001a\u0004\u0018\u00010\u0001H\u0007¢\u0006\u0002\b\u0007¨\u0006\b" }, d2 = { "findViewTreeSavedStateRegistryOwner", "Landroidx/savedstate/SavedStateRegistryOwner;", "Landroid/view/View;", "get", "setViewTreeSavedStateRegistryOwner", "", "owner", "set", "savedstate_release" }, k = 2, mv = { 1, 8, 0 }, xi = 48)
public final class ViewTreeSavedStateRegistryOwner
{
    public static final SavedStateRegistryOwner get(final View view) {
        Intrinsics.checkNotNullParameter((Object)view, "<this>");
        return (SavedStateRegistryOwner)SequencesKt.firstOrNull(SequencesKt.mapNotNull(SequencesKt.generateSequence((Object)view, (Function1)ViewTreeSavedStateRegistryOwner$findViewTreeSavedStateRegistryOwner.ViewTreeSavedStateRegistryOwner$findViewTreeSavedStateRegistryOwner$1.INSTANCE), (Function1)ViewTreeSavedStateRegistryOwner$findViewTreeSavedStateRegistryOwner.ViewTreeSavedStateRegistryOwner$findViewTreeSavedStateRegistryOwner$2.INSTANCE));
    }
    
    public static final void set(final View view, final SavedStateRegistryOwner savedStateRegistryOwner) {
        Intrinsics.checkNotNullParameter((Object)view, "<this>");
        view.setTag(R.id.view_tree_saved_state_registry_owner, (Object)savedStateRegistryOwner);
    }
}

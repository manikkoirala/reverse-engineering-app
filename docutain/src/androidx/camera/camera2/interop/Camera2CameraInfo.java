// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.interop;

import java.util.Map;
import android.hardware.camera2.CameraCharacteristics$Key;
import androidx.core.util.Preconditions;
import android.hardware.camera2.CameraCharacteristics;
import androidx.camera.core.CameraInfo;
import androidx.camera.camera2.internal.Camera2CameraInfoImpl;

public final class Camera2CameraInfo
{
    private static final String TAG = "Camera2CameraInfo";
    private final Camera2CameraInfoImpl mCamera2CameraInfoImpl;
    
    public Camera2CameraInfo(final Camera2CameraInfoImpl mCamera2CameraInfoImpl) {
        this.mCamera2CameraInfoImpl = mCamera2CameraInfoImpl;
    }
    
    public static CameraCharacteristics extractCameraCharacteristics(final CameraInfo cameraInfo) {
        Preconditions.checkState(cameraInfo instanceof Camera2CameraInfoImpl, "CameraInfo does not contain any Camera2 information.");
        return ((Camera2CameraInfoImpl)cameraInfo).getCameraCharacteristicsCompat().toCameraCharacteristics();
    }
    
    public static Camera2CameraInfo from(final CameraInfo cameraInfo) {
        Preconditions.checkArgument(cameraInfo instanceof Camera2CameraInfoImpl, (Object)"CameraInfo doesn't contain Camera2 implementation.");
        return ((Camera2CameraInfoImpl)cameraInfo).getCamera2CameraInfo();
    }
    
    public <T> T getCameraCharacteristic(final CameraCharacteristics$Key<T> cameraCharacteristics$Key) {
        return this.mCamera2CameraInfoImpl.getCameraCharacteristicsCompat().get(cameraCharacteristics$Key);
    }
    
    public Map<String, CameraCharacteristics> getCameraCharacteristicsMap() {
        return this.mCamera2CameraInfoImpl.getCameraCharacteristicsMap();
    }
    
    public String getCameraId() {
        return this.mCamera2CameraInfoImpl.getCameraId();
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.interop;

import androidx.camera.core.impl.TagBundle;
import android.hardware.camera2.TotalCaptureResult;
import androidx.camera.core.impl.utils.futures.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import androidx.core.util.Preconditions;
import androidx.camera.core.CameraControl;
import java.util.Iterator;
import androidx.camera.core.impl.Config;
import java.util.concurrent.Executor;
import androidx.concurrent.futures.CallbackToFutureAdapter;
import androidx.camera.camera2.internal.Camera2CameraControlImpl;
import androidx.camera.camera2.impl.Camera2ImplConfig;

public final class Camera2CameraControl
{
    public static final String TAG_KEY = "Camera2CameraControl";
    private Camera2ImplConfig.Builder mBuilder;
    private final Camera2CameraControlImpl mCamera2CameraControlImpl;
    private final Camera2CameraControlImpl.CaptureResultListener mCaptureResultListener;
    CallbackToFutureAdapter.Completer<Void> mCompleter;
    final Executor mExecutor;
    private boolean mIsActive;
    final Object mLock;
    private boolean mPendingUpdate;
    
    public Camera2CameraControl(final Camera2CameraControlImpl mCamera2CameraControlImpl, final Executor mExecutor) {
        this.mIsActive = false;
        this.mPendingUpdate = false;
        this.mLock = new Object();
        this.mBuilder = new Camera2ImplConfig.Builder();
        this.mCaptureResultListener = new Camera2CameraControl$$ExternalSyntheticLambda4(this);
        this.mCamera2CameraControlImpl = mCamera2CameraControlImpl;
        this.mExecutor = mExecutor;
    }
    
    private void addCaptureRequestOptionsInternal(final CaptureRequestOptions captureRequestOptions) {
        synchronized (this.mLock) {
            for (final Config.Option option : captureRequestOptions.listOptions()) {
                this.mBuilder.getMutableConfig().insertOption(option, captureRequestOptions.retrieveOption(option));
            }
        }
    }
    
    private void clearCaptureRequestOptionsInternal() {
        synchronized (this.mLock) {
            this.mBuilder = new Camera2ImplConfig.Builder();
        }
    }
    
    public static Camera2CameraControl from(final CameraControl cameraControl) {
        Preconditions.checkArgument(cameraControl instanceof Camera2CameraControlImpl, (Object)"CameraControl doesn't contain Camera2 implementation.");
        return ((Camera2CameraControlImpl)cameraControl).getCamera2CameraControl();
    }
    
    private void setActiveInternal(final boolean mIsActive) {
        if (this.mIsActive == mIsActive) {
            return;
        }
        this.mIsActive = mIsActive;
        if (mIsActive) {
            if (this.mPendingUpdate) {
                this.updateSession();
            }
        }
        else {
            final CallbackToFutureAdapter.Completer<Void> mCompleter = this.mCompleter;
            if (mCompleter != null) {
                mCompleter.setException(new CameraControl.OperationCanceledException("The camera control has became inactive."));
                this.mCompleter = null;
            }
        }
    }
    
    private void updateConfig(final CallbackToFutureAdapter.Completer<Void> mCompleter) {
        this.mPendingUpdate = true;
        CallbackToFutureAdapter.Completer<Void> mCompleter2 = this.mCompleter;
        if (mCompleter2 == null) {
            mCompleter2 = null;
        }
        this.mCompleter = mCompleter;
        if (this.mIsActive) {
            this.updateSession();
        }
        if (mCompleter2 != null) {
            mCompleter2.setException(new CameraControl.OperationCanceledException("Camera2CameraControl was updated with new options."));
        }
    }
    
    private void updateSession() {
        this.mCamera2CameraControlImpl.updateSessionConfig();
        this.mPendingUpdate = false;
    }
    
    public ListenableFuture<Void> addCaptureRequestOptions(final CaptureRequestOptions captureRequestOptions) {
        this.addCaptureRequestOptionsInternal(captureRequestOptions);
        return Futures.nonCancellationPropagating((com.google.common.util.concurrent.ListenableFuture<Void>)CallbackToFutureAdapter.getFuture((CallbackToFutureAdapter.Resolver<V>)new Camera2CameraControl$$ExternalSyntheticLambda3(this)));
    }
    
    public ListenableFuture<Void> clearCaptureRequestOptions() {
        this.clearCaptureRequestOptionsInternal();
        return Futures.nonCancellationPropagating((com.google.common.util.concurrent.ListenableFuture<Void>)CallbackToFutureAdapter.getFuture((CallbackToFutureAdapter.Resolver<V>)new Camera2CameraControl$$ExternalSyntheticLambda2(this)));
    }
    
    public Camera2ImplConfig getCamera2ImplConfig() {
        synchronized (this.mLock) {
            if (this.mCompleter != null) {
                this.mBuilder.getMutableConfig().insertOption(Camera2ImplConfig.CAPTURE_REQUEST_TAG_OPTION, this.mCompleter.hashCode());
            }
            return this.mBuilder.build();
        }
    }
    
    public Camera2CameraControlImpl.CaptureResultListener getCaptureRequestListener() {
        return this.mCaptureResultListener;
    }
    
    public CaptureRequestOptions getCaptureRequestOptions() {
        synchronized (this.mLock) {
            return CaptureRequestOptions.Builder.from(this.mBuilder.build()).build();
        }
    }
    
    public void setActive(final boolean b) {
        this.mExecutor.execute(new Camera2CameraControl$$ExternalSyntheticLambda1(this, b));
    }
    
    public ListenableFuture<Void> setCaptureRequestOptions(final CaptureRequestOptions captureRequestOptions) {
        this.clearCaptureRequestOptionsInternal();
        this.addCaptureRequestOptionsInternal(captureRequestOptions);
        return Futures.nonCancellationPropagating((com.google.common.util.concurrent.ListenableFuture<Void>)CallbackToFutureAdapter.getFuture((CallbackToFutureAdapter.Resolver<V>)new Camera2CameraControl$$ExternalSyntheticLambda5(this)));
    }
}

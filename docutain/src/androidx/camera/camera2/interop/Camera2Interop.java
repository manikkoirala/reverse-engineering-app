// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.interop;

import android.hardware.camera2.CameraCaptureSession$StateCallback;
import android.hardware.camera2.CameraCaptureSession$CaptureCallback;
import android.hardware.camera2.CameraDevice$StateCallback;
import androidx.camera.core.impl.Config;
import androidx.camera.camera2.impl.Camera2ImplConfig;
import android.hardware.camera2.CaptureRequest$Key;
import androidx.camera.core.ExtendableBuilder;

public final class Camera2Interop
{
    private Camera2Interop() {
    }
    
    public static final class Extender<T>
    {
        ExtendableBuilder<T> mBaseBuilder;
        
        public Extender(final ExtendableBuilder<T> mBaseBuilder) {
            this.mBaseBuilder = mBaseBuilder;
        }
        
        public <ValueT> Extender<T> setCaptureRequestOption(final CaptureRequest$Key<ValueT> captureRequest$Key, final ValueT valueT) {
            this.mBaseBuilder.getMutableConfig().insertOption((Config.Option<ValueT>)Camera2ImplConfig.createCaptureRequestOption(captureRequest$Key), Config.OptionPriority.ALWAYS_OVERRIDE, valueT);
            return this;
        }
        
        public Extender<T> setCaptureRequestTemplate(final int i) {
            this.mBaseBuilder.getMutableConfig().insertOption(Camera2ImplConfig.TEMPLATE_TYPE_OPTION, i);
            return this;
        }
        
        public Extender<T> setDeviceStateCallback(final CameraDevice$StateCallback cameraDevice$StateCallback) {
            this.mBaseBuilder.getMutableConfig().insertOption(Camera2ImplConfig.DEVICE_STATE_CALLBACK_OPTION, cameraDevice$StateCallback);
            return this;
        }
        
        public Extender<T> setPhysicalCameraId(final String s) {
            this.mBaseBuilder.getMutableConfig().insertOption(Camera2ImplConfig.SESSION_PHYSICAL_CAMERA_ID_OPTION, s);
            return this;
        }
        
        public Extender<T> setSessionCaptureCallback(final CameraCaptureSession$CaptureCallback cameraCaptureSession$CaptureCallback) {
            this.mBaseBuilder.getMutableConfig().insertOption(Camera2ImplConfig.SESSION_CAPTURE_CALLBACK_OPTION, cameraCaptureSession$CaptureCallback);
            return this;
        }
        
        public Extender<T> setSessionStateCallback(final CameraCaptureSession$StateCallback cameraCaptureSession$StateCallback) {
            this.mBaseBuilder.getMutableConfig().insertOption(Camera2ImplConfig.SESSION_STATE_CALLBACK_OPTION, cameraCaptureSession$StateCallback);
            return this;
        }
        
        public Extender<T> setStreamUseCase(final long l) {
            this.mBaseBuilder.getMutableConfig().insertOption(Camera2ImplConfig.STREAM_USE_CASE_OPTION, l);
            return this;
        }
    }
}

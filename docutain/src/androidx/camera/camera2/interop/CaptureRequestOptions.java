// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.interop;

import androidx.camera.core.impl.MutableConfig;
import androidx.camera.core.impl.OptionsBundle;
import androidx.camera.core.impl.MutableOptionsBundle;
import androidx.camera.core.ExtendableBuilder;
import java.util.Set;
import androidx.camera.camera2.impl.Camera2ImplConfig;
import android.hardware.camera2.CaptureRequest$Key;
import androidx.camera.core.impl.ReadableConfig$_CC;
import androidx.camera.core.impl.Config;
import androidx.camera.core.impl.ReadableConfig;

public class CaptureRequestOptions implements ReadableConfig
{
    private final Config mConfig;
    
    public CaptureRequestOptions(final Config mConfig) {
        this.mConfig = mConfig;
    }
    
    public <ValueT> ValueT getCaptureRequestOption(final CaptureRequest$Key<ValueT> captureRequest$Key) {
        return this.mConfig.retrieveOption((Config.Option<ValueT>)Camera2ImplConfig.createCaptureRequestOption(captureRequest$Key), (ValueT)null);
    }
    
    public <ValueT> ValueT getCaptureRequestOption(final CaptureRequest$Key<ValueT> captureRequest$Key, final ValueT valueT) {
        return this.mConfig.retrieveOption((Config.Option<ValueT>)Camera2ImplConfig.createCaptureRequestOption(captureRequest$Key), valueT);
    }
    
    @Override
    public Config getConfig() {
        return this.mConfig;
    }
    
    public static final class Builder implements ExtendableBuilder<CaptureRequestOptions>
    {
        private final MutableOptionsBundle mMutableOptionsBundle;
        
        public Builder() {
            this.mMutableOptionsBundle = MutableOptionsBundle.create();
        }
        
        public static Builder from(final Config config) {
            final Builder builder = new Builder();
            config.findOptions("camera2.captureRequest.option.", (Config.OptionMatcher)new CaptureRequestOptions$Builder$$ExternalSyntheticLambda0(builder, config));
            return builder;
        }
        
        @Override
        public CaptureRequestOptions build() {
            return new CaptureRequestOptions(OptionsBundle.from(this.mMutableOptionsBundle));
        }
        
        public <ValueT> Builder clearCaptureRequestOption(final CaptureRequest$Key<ValueT> captureRequest$Key) {
            this.mMutableOptionsBundle.removeOption(Camera2ImplConfig.createCaptureRequestOption(captureRequest$Key));
            return this;
        }
        
        @Override
        public MutableConfig getMutableConfig() {
            return this.mMutableOptionsBundle;
        }
        
        public <ValueT> Builder setCaptureRequestOption(final CaptureRequest$Key<ValueT> captureRequest$Key, final ValueT valueT) {
            this.mMutableOptionsBundle.insertOption((Option<ValueT>)Camera2ImplConfig.createCaptureRequestOption(captureRequest$Key), valueT);
            return this;
        }
    }
}

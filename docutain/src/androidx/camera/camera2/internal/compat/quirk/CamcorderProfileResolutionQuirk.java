// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.internal.compat.quirk;

import java.util.Collection;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Arrays;
import android.graphics.SurfaceTexture;
import android.os.Build$VERSION;
import androidx.camera.core.Logger;
import android.hardware.camera2.CameraCharacteristics$Key;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.params.StreamConfigurationMap;
import androidx.camera.camera2.internal.compat.CameraCharacteristicsCompat;
import android.util.Size;
import java.util.List;
import androidx.camera.core.impl.Quirk;

public class CamcorderProfileResolutionQuirk implements Quirk
{
    private static final String TAG = "CamcorderProfileResolutionQuirk";
    private final List<Size> mSupportedResolutions;
    
    public CamcorderProfileResolutionQuirk(final CameraCharacteristicsCompat cameraCharacteristicsCompat) {
        final StreamConfigurationMap streamConfigurationMap = cameraCharacteristicsCompat.get((android.hardware.camera2.CameraCharacteristics$Key<StreamConfigurationMap>)CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP);
        if (streamConfigurationMap == null) {
            Logger.e("CamcorderProfileResolutionQuirk", "StreamConfigurationMap is null");
        }
        final int sdk_INT = Build$VERSION.SDK_INT;
        Size[] array = null;
        if (sdk_INT < 23) {
            if (streamConfigurationMap != null) {
                array = streamConfigurationMap.getOutputSizes((Class)SurfaceTexture.class);
            }
        }
        else if (streamConfigurationMap != null) {
            array = streamConfigurationMap.getOutputSizes(34);
        }
        List<Size> list;
        if (array != null) {
            list = Arrays.asList((Size[])array.clone());
        }
        else {
            list = Collections.emptyList();
        }
        this.mSupportedResolutions = list;
        final StringBuilder sb = new StringBuilder();
        sb.append("mSupportedResolutions = ");
        sb.append(list);
        Logger.d("CamcorderProfileResolutionQuirk", sb.toString());
    }
    
    static boolean load(final CameraCharacteristicsCompat cameraCharacteristicsCompat) {
        final Integer n = cameraCharacteristicsCompat.get((android.hardware.camera2.CameraCharacteristics$Key<Integer>)CameraCharacteristics.INFO_SUPPORTED_HARDWARE_LEVEL);
        return n != null && n == 2;
    }
    
    public List<Size> getSupportedResolutions() {
        return new ArrayList<Size>(this.mSupportedResolutions);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.internal.compat.quirk;

import android.util.Size;
import androidx.camera.core.impl.SurfaceConfig;
import android.os.Build$VERSION;
import java.util.Locale;
import android.os.Build;
import java.util.HashMap;
import android.util.Range;
import java.util.Map;
import androidx.camera.core.impl.Quirk;

public class ExtraCroppingQuirk implements Quirk
{
    private static final Map<String, Range<Integer>> SAMSUNG_DISTORTION_MODELS_TO_API_LEVEL_MAP;
    
    static {
        final HashMap samsung_DISTORTION_MODELS_TO_API_LEVEL_MAP = new HashMap();
        (SAMSUNG_DISTORTION_MODELS_TO_API_LEVEL_MAP = samsung_DISTORTION_MODELS_TO_API_LEVEL_MAP).put("SM-T580", null);
        final Integer value = 21;
        final Integer value2 = 26;
        samsung_DISTORTION_MODELS_TO_API_LEVEL_MAP.put("SM-J710MN", new Range((Comparable)value, (Comparable)value2));
        samsung_DISTORTION_MODELS_TO_API_LEVEL_MAP.put("SM-A320FL", null);
        samsung_DISTORTION_MODELS_TO_API_LEVEL_MAP.put("SM-G570M", null);
        samsung_DISTORTION_MODELS_TO_API_LEVEL_MAP.put("SM-G610F", null);
        samsung_DISTORTION_MODELS_TO_API_LEVEL_MAP.put("SM-G610M", new Range((Comparable)value, (Comparable)value2));
    }
    
    private static boolean isSamsungDistortion() {
        final boolean equalsIgnoreCase = "samsung".equalsIgnoreCase(Build.BRAND);
        boolean contains = true;
        if (!equalsIgnoreCase || !ExtraCroppingQuirk.SAMSUNG_DISTORTION_MODELS_TO_API_LEVEL_MAP.containsKey(Build.MODEL.toUpperCase(Locale.US))) {
            return false;
        }
        final Range range = ExtraCroppingQuirk.SAMSUNG_DISTORTION_MODELS_TO_API_LEVEL_MAP.get(Build.MODEL.toUpperCase(Locale.US));
        if (range != null) {
            contains = range.contains((Comparable)Build$VERSION.SDK_INT);
        }
        return contains;
    }
    
    static boolean load() {
        return isSamsungDistortion();
    }
    
    public Size getVerifiedResolution(final SurfaceConfig.ConfigType configType) {
        if (!isSamsungDistortion()) {
            return null;
        }
        final int n = ExtraCroppingQuirk$1.$SwitchMap$androidx$camera$core$impl$SurfaceConfig$ConfigType[configType.ordinal()];
        if (n == 1) {
            return new Size(1920, 1080);
        }
        if (n == 2) {
            return new Size(1280, 720);
        }
        if (n != 3) {
            return null;
        }
        return new Size(3264, 1836);
    }
}

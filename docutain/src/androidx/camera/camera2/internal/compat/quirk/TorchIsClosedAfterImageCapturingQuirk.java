// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.internal.compat.quirk;

import java.util.Locale;
import android.os.Build;
import java.util.Arrays;
import java.util.List;
import androidx.camera.core.impl.Quirk;

public class TorchIsClosedAfterImageCapturingQuirk implements Quirk
{
    public static final List<String> BUILD_MODELS;
    
    static {
        BUILD_MODELS = Arrays.asList("mi a1", "mi a2", "mi a2 lite", "redmi 4x", "redmi 5a", "redmi 6 pro");
    }
    
    static boolean load() {
        return TorchIsClosedAfterImageCapturingQuirk.BUILD_MODELS.contains(Build.MODEL.toLowerCase(Locale.US));
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.internal.compat.quirk;

import java.util.Locale;
import android.os.Build;
import java.util.Collection;
import java.util.HashSet;
import java.util.Arrays;
import android.util.Pair;
import java.util.Set;
import androidx.camera.core.impl.Quirk;

public class FlashAvailabilityBufferUnderflowQuirk implements Quirk
{
    private static final Set<Pair<String, String>> KNOWN_AFFECTED_MODELS;
    
    static {
        KNOWN_AFFECTED_MODELS = new HashSet<Pair<String, String>>((Collection<? extends Pair<String, String>>)Arrays.asList(new Pair((Object)"sprd", (Object)"lemp")));
    }
    
    static boolean load() {
        return FlashAvailabilityBufferUnderflowQuirk.KNOWN_AFFECTED_MODELS.contains(new Pair((Object)Build.MANUFACTURER.toLowerCase(Locale.US), (Object)Build.MODEL.toLowerCase(Locale.US)));
    }
}

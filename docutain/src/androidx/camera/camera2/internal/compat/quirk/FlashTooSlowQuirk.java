// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.internal.compat.quirk;

import android.hardware.camera2.CameraCharacteristics$Key;
import android.hardware.camera2.CameraCharacteristics;
import java.util.Locale;
import android.os.Build;
import androidx.camera.camera2.internal.compat.CameraCharacteristicsCompat;
import java.util.Arrays;
import java.util.List;

public class FlashTooSlowQuirk implements UseTorchAsFlashQuirk
{
    private static final List<String> AFFECTED_MODELS;
    
    static {
        AFFECTED_MODELS = Arrays.asList("PIXEL 3A", "PIXEL 3A XL");
    }
    
    static boolean load(final CameraCharacteristicsCompat cameraCharacteristicsCompat) {
        final boolean contains = FlashTooSlowQuirk.AFFECTED_MODELS.contains(Build.MODEL.toUpperCase(Locale.US));
        boolean b = true;
        if (!contains || cameraCharacteristicsCompat.get((android.hardware.camera2.CameraCharacteristics$Key<Integer>)CameraCharacteristics.LENS_FACING) != 1) {
            b = false;
        }
        return b;
    }
}

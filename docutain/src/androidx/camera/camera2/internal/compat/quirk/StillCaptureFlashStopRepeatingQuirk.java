// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.internal.compat.quirk;

import java.util.Locale;
import android.os.Build;
import androidx.camera.core.impl.Quirk;

public class StillCaptureFlashStopRepeatingQuirk implements Quirk
{
    static boolean load() {
        return "SAMSUNG".equals(Build.MANUFACTURER.toUpperCase(Locale.US)) && Build.MODEL.toUpperCase(Locale.US).startsWith("SM-A716");
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.internal.compat.quirk;

import android.hardware.camera2.CameraCharacteristics$Key;
import android.hardware.camera2.CameraCharacteristics;
import java.util.Locale;
import android.os.Build;
import androidx.camera.camera2.internal.compat.CameraCharacteristicsCompat;
import java.util.Arrays;
import java.util.List;

public class CameraNoResponseWhenEnablingFlashQuirk implements UseTorchAsFlashQuirk
{
    public static final List<String> AFFECTED_MODELS;
    
    static {
        AFFECTED_MODELS = Arrays.asList("SM-N9200", "SM-N9208", "SAMSUNG-SM-N920A", "SM-N920C", "SM-N920F", "SM-N920G", "SM-N920I", "SM-N920K", "SM-N920L", "SM-N920P", "SM-N920R4", "SM-N920R6", "SM-N920R7", "SM-N920S", "SM-N920T", "SM-N920V", "SM-N920W8", "SM-N920X", "SM-J510FN");
    }
    
    static boolean load(final CameraCharacteristicsCompat cameraCharacteristicsCompat) {
        final boolean contains = CameraNoResponseWhenEnablingFlashQuirk.AFFECTED_MODELS.contains(Build.MODEL.toUpperCase(Locale.US));
        boolean b = true;
        if (!contains || cameraCharacteristicsCompat.get((android.hardware.camera2.CameraCharacteristics$Key<Integer>)CameraCharacteristics.LENS_FACING) != 1) {
            b = false;
        }
        return b;
    }
}

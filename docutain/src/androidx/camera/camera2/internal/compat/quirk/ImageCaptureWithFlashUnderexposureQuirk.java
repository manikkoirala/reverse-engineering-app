// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.internal.compat.quirk;

import android.hardware.camera2.CameraCharacteristics$Key;
import android.hardware.camera2.CameraCharacteristics;
import java.util.Locale;
import android.os.Build;
import androidx.camera.camera2.internal.compat.CameraCharacteristicsCompat;
import java.util.Arrays;
import java.util.List;

public class ImageCaptureWithFlashUnderexposureQuirk implements UseTorchAsFlashQuirk
{
    public static final List<String> BUILD_MODELS;
    
    static {
        BUILD_MODELS = Arrays.asList("sm-a260f", "sm-j530f", "sm-j600g", "sm-j701f", "sm-g610f", "sm-j710mn");
    }
    
    static boolean load(final CameraCharacteristicsCompat cameraCharacteristicsCompat) {
        final boolean contains = ImageCaptureWithFlashUnderexposureQuirk.BUILD_MODELS.contains(Build.MODEL.toLowerCase(Locale.US));
        boolean b = true;
        if (!contains || cameraCharacteristicsCompat.get((android.hardware.camera2.CameraCharacteristics$Key<Integer>)CameraCharacteristics.LENS_FACING) != 1) {
            b = false;
        }
        return b;
    }
}

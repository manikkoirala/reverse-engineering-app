// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.internal.compat.quirk;

import java.util.Locale;
import android.os.Build;
import androidx.camera.camera2.internal.compat.CameraCharacteristicsCompat;
import java.util.Collection;
import java.util.HashSet;
import java.util.Arrays;
import java.util.Set;
import androidx.camera.core.internal.compat.quirk.SoftwareJpegEncodingPreferredQuirk;

public final class JpegHalCorruptImageQuirk implements SoftwareJpegEncodingPreferredQuirk
{
    private static final Set<String> KNOWN_AFFECTED_DEVICES;
    
    static {
        KNOWN_AFFECTED_DEVICES = new HashSet<String>(Arrays.asList("heroqltevzw", "heroqltetmo", "k61v1_basic_ref"));
    }
    
    static boolean load(final CameraCharacteristicsCompat cameraCharacteristicsCompat) {
        return JpegHalCorruptImageQuirk.KNOWN_AFFECTED_DEVICES.contains(Build.DEVICE.toLowerCase(Locale.US));
    }
}

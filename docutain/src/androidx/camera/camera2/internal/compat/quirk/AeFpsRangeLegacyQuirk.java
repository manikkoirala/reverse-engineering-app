// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.internal.compat.quirk;

import android.hardware.camera2.CameraCharacteristics$Key;
import android.hardware.camera2.CameraCharacteristics;
import androidx.camera.camera2.internal.compat.CameraCharacteristicsCompat;
import android.util.Range;
import androidx.camera.core.impl.Quirk;

public class AeFpsRangeLegacyQuirk implements Quirk
{
    private final Range<Integer> mAeFpsRange;
    
    public AeFpsRangeLegacyQuirk(final CameraCharacteristicsCompat cameraCharacteristicsCompat) {
        this.mAeFpsRange = this.pickSuitableFpsRange(cameraCharacteristicsCompat.get((android.hardware.camera2.CameraCharacteristics$Key<Range<Integer>[]>)CameraCharacteristics.CONTROL_AE_AVAILABLE_TARGET_FPS_RANGES));
    }
    
    private Range<Integer> getCorrectedFpsRange(final Range<Integer> range) {
        int intValue = (int)range.getUpper();
        int intValue2 = (int)range.getLower();
        if ((int)range.getUpper() >= 1000) {
            intValue = (int)range.getUpper() / 1000;
        }
        if ((int)range.getLower() >= 1000) {
            intValue2 = (int)range.getLower() / 1000;
        }
        return (Range<Integer>)new Range((Comparable)intValue2, (Comparable)intValue);
    }
    
    static boolean load(final CameraCharacteristicsCompat cameraCharacteristicsCompat) {
        final Integer n = cameraCharacteristicsCompat.get((android.hardware.camera2.CameraCharacteristics$Key<Integer>)CameraCharacteristics.INFO_SUPPORTED_HARDWARE_LEVEL);
        return n != null && n == 2;
    }
    
    private Range<Integer> pickSuitableFpsRange(final Range<Integer>[] array) {
        final Range<Integer> range = null;
        final Range<Integer> range2 = null;
        Range<Integer> range3 = range;
        if (array != null) {
            if (array.length == 0) {
                range3 = range;
            }
            else {
                final int length = array.length;
                int n = 0;
                Range<Integer> range4 = range2;
                while (true) {
                    range3 = range4;
                    if (n >= length) {
                        break;
                    }
                    final Range<Integer> correctedFpsRange = this.getCorrectedFpsRange(array[n]);
                    Range<Integer> range5 = null;
                    Label_0117: {
                        if ((int)correctedFpsRange.getUpper() != 30) {
                            range5 = range4;
                        }
                        else {
                            if (range4 != null) {
                                range5 = range4;
                                if ((int)correctedFpsRange.getLower() >= (int)range4.getLower()) {
                                    break Label_0117;
                                }
                            }
                            range5 = correctedFpsRange;
                        }
                    }
                    ++n;
                    range4 = range5;
                }
            }
        }
        return range3;
    }
    
    public Range<Integer> getRange() {
        return this.mAeFpsRange;
    }
}

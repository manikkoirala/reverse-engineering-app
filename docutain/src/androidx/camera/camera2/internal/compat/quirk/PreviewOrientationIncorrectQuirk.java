// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.internal.compat.quirk;

import android.hardware.camera2.CameraCharacteristics$Key;
import android.hardware.camera2.CameraCharacteristics;
import androidx.camera.camera2.internal.compat.CameraCharacteristicsCompat;
import androidx.camera.core.impl.Quirk;

public class PreviewOrientationIncorrectQuirk implements Quirk
{
    static boolean load(final CameraCharacteristicsCompat cameraCharacteristicsCompat) {
        final Integer n = cameraCharacteristicsCompat.get((android.hardware.camera2.CameraCharacteristics$Key<Integer>)CameraCharacteristics.INFO_SUPPORTED_HARDWARE_LEVEL);
        return n != null && n == 2;
    }
}

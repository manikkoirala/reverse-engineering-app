// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.internal.compat.quirk;

import android.hardware.camera2.CameraCharacteristics$Key;
import android.hardware.camera2.CameraCharacteristics;
import java.util.Locale;
import android.os.Build;
import androidx.camera.camera2.internal.compat.CameraCharacteristicsCompat;
import java.util.Arrays;
import java.util.List;
import androidx.camera.core.impl.Quirk;

public class ImageCaptureFailWithAutoFlashQuirk implements Quirk
{
    private static final List<String> BUILD_MODELS_FRONT_CAMERA;
    
    static {
        BUILD_MODELS_FRONT_CAMERA = Arrays.asList("sm-j700f", "sm-j710f");
    }
    
    static boolean load(final CameraCharacteristicsCompat cameraCharacteristicsCompat) {
        return ImageCaptureFailWithAutoFlashQuirk.BUILD_MODELS_FRONT_CAMERA.contains(Build.MODEL.toLowerCase(Locale.US)) && cameraCharacteristicsCompat.get((android.hardware.camera2.CameraCharacteristics$Key<Integer>)CameraCharacteristics.LENS_FACING) == 0;
    }
}

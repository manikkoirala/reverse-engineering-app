// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.internal.compat.quirk;

import android.hardware.camera2.CameraCharacteristics$Key;
import android.hardware.camera2.CameraCharacteristics;
import java.util.Locale;
import android.os.Build;
import androidx.camera.camera2.internal.compat.CameraCharacteristicsCompat;
import java.util.Arrays;
import java.util.List;

public class ImageCaptureFlashNotFireQuirk implements UseTorchAsFlashQuirk
{
    private static final List<String> BUILD_MODELS;
    private static final List<String> BUILD_MODELS_FRONT_CAMERA;
    
    static {
        BUILD_MODELS = Arrays.asList("itel w6004");
        BUILD_MODELS_FRONT_CAMERA = Arrays.asList("sm-j700f", "sm-j710f");
    }
    
    static boolean load(final CameraCharacteristicsCompat cameraCharacteristicsCompat) {
        final boolean contains = ImageCaptureFlashNotFireQuirk.BUILD_MODELS_FRONT_CAMERA.contains(Build.MODEL.toLowerCase(Locale.US));
        final boolean b = true;
        final boolean b2 = contains && cameraCharacteristicsCompat.get((android.hardware.camera2.CameraCharacteristics$Key<Integer>)CameraCharacteristics.LENS_FACING) == 0;
        final boolean contains2 = ImageCaptureFlashNotFireQuirk.BUILD_MODELS.contains(Build.MODEL.toLowerCase(Locale.US));
        boolean b3 = b;
        if (!b2) {
            b3 = (contains2 && b);
        }
        return b3;
    }
}

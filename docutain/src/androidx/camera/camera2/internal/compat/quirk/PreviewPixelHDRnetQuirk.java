// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.internal.compat.quirk;

import java.util.Locale;
import android.os.Build;
import java.util.Arrays;
import java.util.List;
import androidx.camera.core.impl.Quirk;

public class PreviewPixelHDRnetQuirk implements Quirk
{
    private static final List<String> SUPPORTED_DEVICES;
    
    static {
        SUPPORTED_DEVICES = Arrays.asList("sunfish", "bramble", "redfin", "barbet");
    }
    
    static boolean load() {
        return "Google".equals(Build.MANUFACTURER) && PreviewPixelHDRnetQuirk.SUPPORTED_DEVICES.contains(Build.DEVICE.toLowerCase(Locale.getDefault()));
    }
}

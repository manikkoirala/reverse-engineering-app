// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.internal.compat;

import android.hardware.camera2.CameraManager$AvailabilityCallback;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraDevice$StateCallback;
import java.util.concurrent.Executor;
import android.hardware.camera2.CameraCharacteristics;
import android.os.Build$VERSION;
import android.content.Context;

class CameraManagerCompatApi28Impl extends CameraManagerCompatBaseImpl
{
    CameraManagerCompatApi28Impl(final Context context) {
        super(context, null);
    }
    
    static CameraManagerCompatApi28Impl create(final Context context) {
        return new CameraManagerCompatApi28Impl(context);
    }
    
    private boolean isDndFailCase(final Throwable t) {
        return Build$VERSION.SDK_INT == 28 && isDndRuntimeException(t);
    }
    
    private static boolean isDndRuntimeException(final Throwable t) {
        if (t.getClass().equals(RuntimeException.class)) {
            final StackTraceElement[] stackTrace = t.getStackTrace();
            if (stackTrace != null) {
                if (stackTrace.length >= 0) {
                    return "_enableShutterSound".equals(stackTrace[0].getMethodName());
                }
            }
        }
        return false;
    }
    
    private void throwDndException(final Throwable t) throws CameraAccessExceptionCompat {
        throw new CameraAccessExceptionCompat(10001, t);
    }
    
    @Override
    public CameraCharacteristics getCameraCharacteristics(final String s) throws CameraAccessExceptionCompat {
        try {
            return super.getCameraCharacteristics(s);
        }
        catch (final RuntimeException ex) {
            if (this.isDndFailCase(ex)) {
                this.throwDndException(ex);
            }
            throw ex;
        }
    }
    
    @Override
    public void openCamera(final String s, final Executor executor, final CameraDevice$StateCallback cameraDevice$StateCallback) throws CameraAccessExceptionCompat {
        try {
            this.mCameraManager.openCamera(s, executor, cameraDevice$StateCallback);
        }
        catch (final RuntimeException ex) {
            if (this.isDndFailCase(ex)) {
                this.throwDndException(ex);
            }
            throw ex;
        }
        catch (final SecurityException ex2) {
            throw ex2;
        }
        catch (final IllegalArgumentException ex2) {
            throw ex2;
        }
        catch (final CameraAccessException ex3) {
            throw CameraAccessExceptionCompat.toCameraAccessExceptionCompat(ex3);
        }
    }
    
    @Override
    public void registerAvailabilityCallback(final Executor executor, final CameraManager$AvailabilityCallback cameraManager$AvailabilityCallback) {
        this.mCameraManager.registerAvailabilityCallback(executor, cameraManager$AvailabilityCallback);
    }
    
    @Override
    public void unregisterAvailabilityCallback(final CameraManager$AvailabilityCallback cameraManager$AvailabilityCallback) {
        this.mCameraManager.unregisterAvailabilityCallback(cameraManager$AvailabilityCallback);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.internal.compat;

import java.util.HashMap;
import java.util.Map;
import android.hardware.camera2.CameraManager$AvailabilityCallback;
import androidx.core.util.Preconditions;
import android.hardware.camera2.CameraDevice$StateCallback;
import java.util.concurrent.Executor;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCharacteristics;
import android.os.Handler;
import android.content.Context;
import android.hardware.camera2.CameraManager;

class CameraManagerCompatBaseImpl implements CameraManagerCompatImpl
{
    final CameraManager mCameraManager;
    final Object mObject;
    
    CameraManagerCompatBaseImpl(final Context context, final Object mObject) {
        this.mCameraManager = (CameraManager)context.getSystemService("camera");
        this.mObject = mObject;
    }
    
    static CameraManagerCompatBaseImpl create(final Context context, final Handler handler) {
        return new CameraManagerCompatBaseImpl(context, new CameraManagerCompatParamsApi21(handler));
    }
    
    @Override
    public CameraCharacteristics getCameraCharacteristics(final String s) throws CameraAccessExceptionCompat {
        try {
            return this.mCameraManager.getCameraCharacteristics(s);
        }
        catch (final CameraAccessException ex) {
            throw CameraAccessExceptionCompat.toCameraAccessExceptionCompat(ex);
        }
    }
    
    @Override
    public String[] getCameraIdList() throws CameraAccessExceptionCompat {
        try {
            return this.mCameraManager.getCameraIdList();
        }
        catch (final CameraAccessException ex) {
            throw CameraAccessExceptionCompat.toCameraAccessExceptionCompat(ex);
        }
    }
    
    @Override
    public CameraManager getCameraManager() {
        return this.mCameraManager;
    }
    
    @Override
    public void openCamera(final String s, final Executor executor, final CameraDevice$StateCallback cameraDevice$StateCallback) throws CameraAccessExceptionCompat {
        Preconditions.checkNotNull(executor);
        Preconditions.checkNotNull(cameraDevice$StateCallback);
        final CameraDeviceCompat.StateCallbackExecutorWrapper stateCallbackExecutorWrapper = new CameraDeviceCompat.StateCallbackExecutorWrapper(executor, cameraDevice$StateCallback);
        final CameraManagerCompatParamsApi21 cameraManagerCompatParamsApi21 = (CameraManagerCompatParamsApi21)this.mObject;
        try {
            this.mCameraManager.openCamera(s, (CameraDevice$StateCallback)stateCallbackExecutorWrapper, cameraManagerCompatParamsApi21.mCompatHandler);
        }
        catch (final CameraAccessException ex) {
            throw CameraAccessExceptionCompat.toCameraAccessExceptionCompat(ex);
        }
    }
    
    @Override
    public void registerAvailabilityCallback(final Executor executor, final CameraManager$AvailabilityCallback cameraManager$AvailabilityCallback) {
        if (executor != null) {
            AvailabilityCallbackExecutorWrapper availabilityCallbackExecutorWrapper = null;
            final CameraManagerCompatParamsApi21 cameraManagerCompatParamsApi21 = (CameraManagerCompatParamsApi21)this.mObject;
            if (cameraManager$AvailabilityCallback != null) {
                synchronized (cameraManagerCompatParamsApi21.mWrapperMap) {
                    if ((availabilityCallbackExecutorWrapper = cameraManagerCompatParamsApi21.mWrapperMap.get(cameraManager$AvailabilityCallback)) == null) {
                        availabilityCallbackExecutorWrapper = new CameraManagerCompat.AvailabilityCallbackExecutorWrapper(executor, cameraManager$AvailabilityCallback);
                        cameraManagerCompatParamsApi21.mWrapperMap.put(cameraManager$AvailabilityCallback, availabilityCallbackExecutorWrapper);
                    }
                }
            }
            this.mCameraManager.registerAvailabilityCallback((CameraManager$AvailabilityCallback)availabilityCallbackExecutorWrapper, cameraManagerCompatParamsApi21.mCompatHandler);
            return;
        }
        throw new IllegalArgumentException("executor was null");
    }
    
    @Override
    public void unregisterAvailabilityCallback(final CameraManager$AvailabilityCallback cameraManager$AvailabilityCallback) {
        final CameraManagerCompat.AvailabilityCallbackExecutorWrapper availabilityCallbackExecutorWrapper2;
        Label_0045: {
            if (cameraManager$AvailabilityCallback != null) {
                final CameraManagerCompatParamsApi21 cameraManagerCompatParamsApi21 = (CameraManagerCompatParamsApi21)this.mObject;
                synchronized (cameraManagerCompatParamsApi21.mWrapperMap) {
                    final AvailabilityCallbackExecutorWrapper availabilityCallbackExecutorWrapper = cameraManagerCompatParamsApi21.mWrapperMap.remove(cameraManager$AvailabilityCallback);
                    break Label_0045;
                }
            }
            availabilityCallbackExecutorWrapper2 = null;
        }
        if (availabilityCallbackExecutorWrapper2 != null) {
            availabilityCallbackExecutorWrapper2.setDisabled();
        }
        this.mCameraManager.unregisterAvailabilityCallback((CameraManager$AvailabilityCallback)availabilityCallbackExecutorWrapper2);
    }
    
    static final class CameraManagerCompatParamsApi21
    {
        final Handler mCompatHandler;
        final Map<CameraManager$AvailabilityCallback, AvailabilityCallbackExecutorWrapper> mWrapperMap;
        
        CameraManagerCompatParamsApi21(final Handler mCompatHandler) {
            this.mWrapperMap = new HashMap<CameraManager$AvailabilityCallback, AvailabilityCallbackExecutorWrapper>();
            this.mCompatHandler = mCompatHandler;
        }
    }
}

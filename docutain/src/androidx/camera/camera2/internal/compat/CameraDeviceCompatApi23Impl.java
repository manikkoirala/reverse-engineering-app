// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.internal.compat;

import androidx.camera.camera2.internal.compat.params.InputConfigurationCompat;
import android.view.Surface;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCaptureSession$StateCallback;
import java.util.List;
import android.hardware.camera2.params.InputConfiguration;
import androidx.core.util.Preconditions;
import androidx.camera.camera2.internal.compat.params.SessionConfigurationCompat;
import android.os.Handler;
import android.hardware.camera2.CameraDevice;

class CameraDeviceCompatApi23Impl extends CameraDeviceCompatBaseImpl
{
    CameraDeviceCompatApi23Impl(final CameraDevice cameraDevice, final Object o) {
        super(cameraDevice, o);
    }
    
    static CameraDeviceCompatApi23Impl create(final CameraDevice cameraDevice, final Handler handler) {
        return new CameraDeviceCompatApi23Impl(cameraDevice, new CameraDeviceCompatParamsApi21(handler));
    }
    
    @Override
    public void createCaptureSession(final SessionConfigurationCompat sessionConfigurationCompat) throws CameraAccessExceptionCompat {
        CameraDeviceCompatBaseImpl.checkPreconditions(this.mCameraDevice, sessionConfigurationCompat);
        final CameraCaptureSessionCompat.StateCallbackExecutorWrapper stateCallbackExecutorWrapper = new CameraCaptureSessionCompat.StateCallbackExecutorWrapper(sessionConfigurationCompat.getExecutor(), sessionConfigurationCompat.getStateCallback());
        final List<Surface> unpackSurfaces = CameraDeviceCompatBaseImpl.unpackSurfaces(sessionConfigurationCompat.getOutputConfigurations());
        final Handler mCompatHandler = Preconditions.checkNotNull(this.mImplParams).mCompatHandler;
        final InputConfigurationCompat inputConfiguration = sessionConfigurationCompat.getInputConfiguration();
        Label_0090: {
            if (inputConfiguration == null) {
                break Label_0090;
            }
            try {
                final InputConfiguration inputConfiguration2 = (InputConfiguration)inputConfiguration.unwrap();
                Preconditions.checkNotNull(inputConfiguration2);
                this.mCameraDevice.createReprocessableCaptureSession(inputConfiguration2, (List)unpackSurfaces, (CameraCaptureSession$StateCallback)stateCallbackExecutorWrapper, mCompatHandler);
                return;
                iftrue(Label_0112:)(sessionConfigurationCompat.getSessionType() != 1);
                Block_3: {
                    break Block_3;
                    Label_0112: {
                        this.createBaseCaptureSession(this.mCameraDevice, unpackSurfaces, stateCallbackExecutorWrapper, mCompatHandler);
                    }
                    return;
                }
                this.mCameraDevice.createConstrainedHighSpeedCaptureSession((List)unpackSurfaces, (CameraCaptureSession$StateCallback)stateCallbackExecutorWrapper, mCompatHandler);
            }
            catch (final CameraAccessException ex) {
                throw CameraAccessExceptionCompat.toCameraAccessExceptionCompat(ex);
            }
        }
    }
}

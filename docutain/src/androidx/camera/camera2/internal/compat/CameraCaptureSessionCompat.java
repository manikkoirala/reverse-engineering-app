// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.internal.compat;

import android.hardware.camera2.CameraCaptureSession$StateCallback;
import android.hardware.camera2.CaptureResult;
import android.hardware.camera2.CaptureFailure;
import android.hardware.camera2.TotalCaptureResult;
import android.view.Surface;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCaptureSession$CaptureCallback;
import java.util.concurrent.Executor;
import android.hardware.camera2.CaptureRequest;
import java.util.List;
import androidx.camera.core.impl.utils.MainThreadAsyncHandler;
import android.os.Build$VERSION;
import android.os.Handler;
import android.hardware.camera2.CameraCaptureSession;

public final class CameraCaptureSessionCompat
{
    private final CameraCaptureSessionCompatImpl mImpl;
    
    private CameraCaptureSessionCompat(final CameraCaptureSession cameraCaptureSession, final Handler handler) {
        if (Build$VERSION.SDK_INT >= 28) {
            this.mImpl = (CameraCaptureSessionCompatImpl)new CameraCaptureSessionCompatApi28Impl(cameraCaptureSession);
        }
        else {
            this.mImpl = CameraCaptureSessionCompatBaseImpl.create(cameraCaptureSession, handler);
        }
    }
    
    public static CameraCaptureSessionCompat toCameraCaptureSessionCompat(final CameraCaptureSession cameraCaptureSession) {
        return toCameraCaptureSessionCompat(cameraCaptureSession, MainThreadAsyncHandler.getInstance());
    }
    
    public static CameraCaptureSessionCompat toCameraCaptureSessionCompat(final CameraCaptureSession cameraCaptureSession, final Handler handler) {
        return new CameraCaptureSessionCompat(cameraCaptureSession, handler);
    }
    
    public int captureBurstRequests(final List<CaptureRequest> list, final Executor executor, final CameraCaptureSession$CaptureCallback cameraCaptureSession$CaptureCallback) throws CameraAccessException {
        return this.mImpl.captureBurstRequests(list, executor, cameraCaptureSession$CaptureCallback);
    }
    
    public int captureSingleRequest(final CaptureRequest captureRequest, final Executor executor, final CameraCaptureSession$CaptureCallback cameraCaptureSession$CaptureCallback) throws CameraAccessException {
        return this.mImpl.captureSingleRequest(captureRequest, executor, cameraCaptureSession$CaptureCallback);
    }
    
    public int setRepeatingBurstRequests(final List<CaptureRequest> list, final Executor executor, final CameraCaptureSession$CaptureCallback cameraCaptureSession$CaptureCallback) throws CameraAccessException {
        return this.mImpl.setRepeatingBurstRequests(list, executor, cameraCaptureSession$CaptureCallback);
    }
    
    public int setSingleRepeatingRequest(final CaptureRequest captureRequest, final Executor executor, final CameraCaptureSession$CaptureCallback cameraCaptureSession$CaptureCallback) throws CameraAccessException {
        return this.mImpl.setSingleRepeatingRequest(captureRequest, executor, cameraCaptureSession$CaptureCallback);
    }
    
    public CameraCaptureSession toCameraCaptureSession() {
        return this.mImpl.unwrap();
    }
    
    interface CameraCaptureSessionCompatImpl
    {
        int captureBurstRequests(final List<CaptureRequest> p0, final Executor p1, final CameraCaptureSession$CaptureCallback p2) throws CameraAccessException;
        
        int captureSingleRequest(final CaptureRequest p0, final Executor p1, final CameraCaptureSession$CaptureCallback p2) throws CameraAccessException;
        
        int setRepeatingBurstRequests(final List<CaptureRequest> p0, final Executor p1, final CameraCaptureSession$CaptureCallback p2) throws CameraAccessException;
        
        int setSingleRepeatingRequest(final CaptureRequest p0, final Executor p1, final CameraCaptureSession$CaptureCallback p2) throws CameraAccessException;
        
        CameraCaptureSession unwrap();
    }
    
    static final class CaptureCallbackExecutorWrapper extends CameraCaptureSession$CaptureCallback
    {
        private final Executor mExecutor;
        final CameraCaptureSession$CaptureCallback mWrappedCallback;
        
        CaptureCallbackExecutorWrapper(final Executor mExecutor, final CameraCaptureSession$CaptureCallback mWrappedCallback) {
            this.mExecutor = mExecutor;
            this.mWrappedCallback = mWrappedCallback;
        }
        
        public void onCaptureBufferLost(final CameraCaptureSession cameraCaptureSession, final CaptureRequest captureRequest, final Surface surface, final long n) {
            this.mExecutor.execute(new CameraCaptureSessionCompat$CaptureCallbackExecutorWrapper$$ExternalSyntheticLambda1(this, cameraCaptureSession, captureRequest, surface, n));
        }
        
        public void onCaptureCompleted(final CameraCaptureSession cameraCaptureSession, final CaptureRequest captureRequest, final TotalCaptureResult totalCaptureResult) {
            this.mExecutor.execute(new CameraCaptureSessionCompat$CaptureCallbackExecutorWrapper$$ExternalSyntheticLambda4(this, cameraCaptureSession, captureRequest, totalCaptureResult));
        }
        
        public void onCaptureFailed(final CameraCaptureSession cameraCaptureSession, final CaptureRequest captureRequest, final CaptureFailure captureFailure) {
            this.mExecutor.execute(new CameraCaptureSessionCompat$CaptureCallbackExecutorWrapper$$ExternalSyntheticLambda6(this, cameraCaptureSession, captureRequest, captureFailure));
        }
        
        public void onCaptureProgressed(final CameraCaptureSession cameraCaptureSession, final CaptureRequest captureRequest, final CaptureResult captureResult) {
            this.mExecutor.execute(new CameraCaptureSessionCompat$CaptureCallbackExecutorWrapper$$ExternalSyntheticLambda3(this, cameraCaptureSession, captureRequest, captureResult));
        }
        
        public void onCaptureSequenceAborted(final CameraCaptureSession cameraCaptureSession, final int n) {
            this.mExecutor.execute(new CameraCaptureSessionCompat$CaptureCallbackExecutorWrapper$$ExternalSyntheticLambda0(this, cameraCaptureSession, n));
        }
        
        public void onCaptureSequenceCompleted(final CameraCaptureSession cameraCaptureSession, final int n, final long n2) {
            this.mExecutor.execute(new CameraCaptureSessionCompat$CaptureCallbackExecutorWrapper$$ExternalSyntheticLambda2(this, cameraCaptureSession, n, n2));
        }
        
        public void onCaptureStarted(final CameraCaptureSession cameraCaptureSession, final CaptureRequest captureRequest, final long n, final long n2) {
            this.mExecutor.execute(new CameraCaptureSessionCompat$CaptureCallbackExecutorWrapper$$ExternalSyntheticLambda5(this, cameraCaptureSession, captureRequest, n, n2));
        }
    }
    
    static final class StateCallbackExecutorWrapper extends CameraCaptureSession$StateCallback
    {
        private final Executor mExecutor;
        final CameraCaptureSession$StateCallback mWrappedCallback;
        
        StateCallbackExecutorWrapper(final Executor mExecutor, final CameraCaptureSession$StateCallback mWrappedCallback) {
            this.mExecutor = mExecutor;
            this.mWrappedCallback = mWrappedCallback;
        }
        
        public void onActive(final CameraCaptureSession cameraCaptureSession) {
            this.mExecutor.execute(new CameraCaptureSessionCompat$StateCallbackExecutorWrapper$$ExternalSyntheticLambda3(this, cameraCaptureSession));
        }
        
        public void onCaptureQueueEmpty(final CameraCaptureSession cameraCaptureSession) {
            this.mExecutor.execute(new CameraCaptureSessionCompat$StateCallbackExecutorWrapper$$ExternalSyntheticLambda2(this, cameraCaptureSession));
        }
        
        public void onClosed(final CameraCaptureSession cameraCaptureSession) {
            this.mExecutor.execute(new CameraCaptureSessionCompat$StateCallbackExecutorWrapper$$ExternalSyntheticLambda1(this, cameraCaptureSession));
        }
        
        public void onConfigureFailed(final CameraCaptureSession cameraCaptureSession) {
            this.mExecutor.execute(new CameraCaptureSessionCompat$StateCallbackExecutorWrapper$$ExternalSyntheticLambda4(this, cameraCaptureSession));
        }
        
        public void onConfigured(final CameraCaptureSession cameraCaptureSession) {
            this.mExecutor.execute(new CameraCaptureSessionCompat$StateCallbackExecutorWrapper$$ExternalSyntheticLambda0(this, cameraCaptureSession));
        }
        
        public void onReady(final CameraCaptureSession cameraCaptureSession) {
            this.mExecutor.execute(new CameraCaptureSessionCompat$StateCallbackExecutorWrapper$$ExternalSyntheticLambda5(this, cameraCaptureSession));
        }
        
        public void onSurfacePrepared(final CameraCaptureSession cameraCaptureSession, final Surface surface) {
            this.mExecutor.execute(new CameraCaptureSessionCompat$StateCallbackExecutorWrapper$$ExternalSyntheticLambda6(this, cameraCaptureSession, surface));
        }
    }
}

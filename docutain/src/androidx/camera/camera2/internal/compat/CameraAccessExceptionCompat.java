// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.internal.compat;

import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;
import java.util.Collections;
import java.util.Collection;
import java.util.HashSet;
import java.util.Arrays;
import android.hardware.camera2.CameraAccessException;
import java.util.Set;

public class CameraAccessExceptionCompat extends Exception
{
    public static final int CAMERA_CHARACTERISTICS_CREATION_ERROR = 10002;
    public static final int CAMERA_DEPRECATED_HAL = 1000;
    public static final int CAMERA_DISABLED = 1;
    public static final int CAMERA_DISCONNECTED = 2;
    public static final int CAMERA_ERROR = 3;
    public static final int CAMERA_IN_USE = 4;
    public static final int CAMERA_UNAVAILABLE_DO_NOT_DISTURB = 10001;
    static final Set<Integer> COMPAT_ERRORS;
    public static final int MAX_CAMERAS_IN_USE = 5;
    static final Set<Integer> PLATFORM_ERRORS;
    private final CameraAccessException mCameraAccessException;
    private final int mReason;
    
    static {
        PLATFORM_ERRORS = Collections.unmodifiableSet((Set<? extends Integer>)new HashSet<Integer>(Arrays.asList(4, 5, 1, 2, 3)));
        COMPAT_ERRORS = Collections.unmodifiableSet((Set<? extends Integer>)new HashSet<Integer>(Arrays.asList(10001, 10002)));
    }
    
    public CameraAccessExceptionCompat(final int n) {
        super(getDefaultMessage(n));
        this.mReason = n;
        CameraAccessException mCameraAccessException;
        if (CameraAccessExceptionCompat.PLATFORM_ERRORS.contains(n)) {
            mCameraAccessException = new CameraAccessException(n);
        }
        else {
            mCameraAccessException = null;
        }
        this.mCameraAccessException = mCameraAccessException;
    }
    
    public CameraAccessExceptionCompat(final int n, final String s) {
        super(getCombinedMessage(n, s));
        this.mReason = n;
        CameraAccessException mCameraAccessException;
        if (CameraAccessExceptionCompat.PLATFORM_ERRORS.contains(n)) {
            mCameraAccessException = new CameraAccessException(n, s);
        }
        else {
            mCameraAccessException = null;
        }
        this.mCameraAccessException = mCameraAccessException;
    }
    
    public CameraAccessExceptionCompat(final int n, final String s, final Throwable cause) {
        super(getCombinedMessage(n, s), cause);
        this.mReason = n;
        CameraAccessException mCameraAccessException;
        if (CameraAccessExceptionCompat.PLATFORM_ERRORS.contains(n)) {
            mCameraAccessException = new CameraAccessException(n, s, cause);
        }
        else {
            mCameraAccessException = null;
        }
        this.mCameraAccessException = mCameraAccessException;
    }
    
    public CameraAccessExceptionCompat(final int n, final Throwable cause) {
        super(getDefaultMessage(n), cause);
        this.mReason = n;
        final boolean contains = CameraAccessExceptionCompat.PLATFORM_ERRORS.contains(n);
        CameraAccessException mCameraAccessException = null;
        if (contains) {
            mCameraAccessException = new CameraAccessException(n, (String)null, cause);
        }
        this.mCameraAccessException = mCameraAccessException;
    }
    
    private CameraAccessExceptionCompat(final CameraAccessException mCameraAccessException) {
        super(mCameraAccessException.getMessage(), mCameraAccessException.getCause());
        this.mReason = mCameraAccessException.getReason();
        this.mCameraAccessException = mCameraAccessException;
    }
    
    private static String getCombinedMessage(final int i, final String s) {
        return String.format("%s (%d): %s", getProblemString(i), i, s);
    }
    
    private static String getDefaultMessage(final int n) {
        if (n == 1) {
            return "The camera is disabled due to a device policy, and cannot be opened.";
        }
        if (n == 2) {
            return "The camera device is removable and has been disconnected from the Android device, or the camera service has shut down the connection due to a higher-priority access request for the camera device.";
        }
        if (n == 3) {
            return "The camera device is currently in the error state; no further calls to it will succeed.";
        }
        if (n == 4) {
            return "The camera device is in use already";
        }
        if (n == 5) {
            return "The system-wide limit for number of open cameras has been reached, and more camera devices cannot be opened until previous instances are closed.";
        }
        if (n == 10001) {
            return "Some API 28 devices cannot access the camera when the device is in \"Do Not Disturb\" mode. The camera will not be accessible until \"Do Not Disturb\" mode is disabled.";
        }
        if (n != 10002) {
            return null;
        }
        return "Failed to create CameraCharacteristics.";
    }
    
    private static String getProblemString(final int n) {
        String s;
        if (n != 1) {
            if (n != 2) {
                if (n != 3) {
                    if (n != 4) {
                        if (n != 5) {
                            if (n != 1000) {
                                if (n != 10001) {
                                    if (n != 10002) {
                                        s = "<UNKNOWN ERROR>";
                                    }
                                    else {
                                        s = "CAMERA_CHARACTERISTICS_CREATION_ERROR";
                                    }
                                }
                                else {
                                    s = "CAMERA_UNAVAILABLE_DO_NOT_DISTURB";
                                }
                            }
                            else {
                                s = "CAMERA_DEPRECATED_HAL";
                            }
                        }
                        else {
                            s = "MAX_CAMERAS_IN_USE";
                        }
                    }
                    else {
                        s = "CAMERA_IN_USE";
                    }
                }
                else {
                    s = "CAMERA_ERROR";
                }
            }
            else {
                s = "CAMERA_DISCONNECTED";
            }
        }
        else {
            s = "CAMERA_DISABLED";
        }
        return s;
    }
    
    public static CameraAccessExceptionCompat toCameraAccessExceptionCompat(final CameraAccessException ex) {
        if (ex != null) {
            return new CameraAccessExceptionCompat(ex);
        }
        throw new NullPointerException("cameraAccessException should not be null");
    }
    
    public final int getReason() {
        return this.mReason;
    }
    
    public CameraAccessException toCameraAccessException() {
        return this.mCameraAccessException;
    }
    
    @Retention(RetentionPolicy.SOURCE)
    public @interface AccessError {
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.internal.compat;

import android.hardware.camera2.CameraDevice$StateCallback;
import java.util.concurrent.Executor;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCharacteristics;
import android.content.Context;

class CameraManagerCompatApi29Impl extends CameraManagerCompatApi28Impl
{
    CameraManagerCompatApi29Impl(final Context context) {
        super(context);
    }
    
    @Override
    public CameraCharacteristics getCameraCharacteristics(final String s) throws CameraAccessExceptionCompat {
        try {
            return this.mCameraManager.getCameraCharacteristics(s);
        }
        catch (final CameraAccessException ex) {
            throw CameraAccessExceptionCompat.toCameraAccessExceptionCompat(ex);
        }
    }
    
    @Override
    public void openCamera(final String s, final Executor executor, final CameraDevice$StateCallback cameraDevice$StateCallback) throws CameraAccessExceptionCompat {
        try {
            this.mCameraManager.openCamera(s, executor, cameraDevice$StateCallback);
        }
        catch (final CameraAccessException ex) {
            throw CameraAccessExceptionCompat.toCameraAccessExceptionCompat(ex);
        }
    }
}

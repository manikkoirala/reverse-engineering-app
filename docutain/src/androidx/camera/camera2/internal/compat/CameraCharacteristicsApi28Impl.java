// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.internal.compat;

import java.util.Set;
import android.hardware.camera2.CameraCharacteristics;

class CameraCharacteristicsApi28Impl extends CameraCharacteristicsBaseImpl
{
    CameraCharacteristicsApi28Impl(final CameraCharacteristics cameraCharacteristics) {
        super(cameraCharacteristics);
    }
    
    @Override
    public Set<String> getPhysicalCameraIds() {
        return this.mCameraCharacteristics.getPhysicalCameraIds();
    }
}

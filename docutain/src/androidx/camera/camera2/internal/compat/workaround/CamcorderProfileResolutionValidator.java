// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.internal.compat.workaround;

import androidx.camera.core.impl.CamcorderProfileProxy;
import java.util.Collections;
import java.util.Collection;
import java.util.HashSet;
import android.util.Size;
import java.util.Set;
import androidx.camera.camera2.internal.compat.quirk.CamcorderProfileResolutionQuirk;

public class CamcorderProfileResolutionValidator
{
    private final CamcorderProfileResolutionQuirk mQuirk;
    private final Set<Size> mSupportedResolutions;
    
    public CamcorderProfileResolutionValidator(final CamcorderProfileResolutionQuirk mQuirk) {
        this.mQuirk = mQuirk;
        Set<Object> emptySet;
        if (mQuirk != null) {
            emptySet = (Set<Object>)new HashSet<Size>(mQuirk.getSupportedResolutions());
        }
        else {
            emptySet = (Set<Object>)Collections.emptySet();
        }
        this.mSupportedResolutions = (Set<Size>)emptySet;
    }
    
    public boolean hasQuirk() {
        return this.mQuirk != null;
    }
    
    public boolean hasValidVideoResolution(final CamcorderProfileProxy camcorderProfileProxy) {
        return camcorderProfileProxy != null && (this.mQuirk == null || this.mSupportedResolutions.contains(new Size(camcorderProfileProxy.getVideoFrameWidth(), camcorderProfileProxy.getVideoFrameHeight())));
    }
}

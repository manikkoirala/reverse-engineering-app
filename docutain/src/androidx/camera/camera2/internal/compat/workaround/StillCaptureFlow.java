// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.internal.compat.workaround;

import java.util.Iterator;
import android.hardware.camera2.CaptureRequest;
import java.util.List;
import androidx.camera.camera2.internal.compat.quirk.DeviceQuirks;
import androidx.camera.camera2.internal.compat.quirk.StillCaptureFlashStopRepeatingQuirk;

public class StillCaptureFlow
{
    private final boolean mShouldStopRepeatingBeforeStillCapture;
    
    public StillCaptureFlow() {
        this.mShouldStopRepeatingBeforeStillCapture = (DeviceQuirks.get(StillCaptureFlashStopRepeatingQuirk.class) != null);
    }
    
    public boolean shouldStopRepeatingBeforeCapture(final List<CaptureRequest> list, final boolean b) {
        if (this.mShouldStopRepeatingBeforeStillCapture) {
            if (b) {
                final Iterator<CaptureRequest> iterator = list.iterator();
                while (iterator.hasNext()) {
                    final int intValue = (int)iterator.next().get(CaptureRequest.CONTROL_AE_MODE);
                    if (intValue == 2 || intValue == 3) {
                        return true;
                    }
                }
            }
        }
        return false;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.internal.compat.workaround;

import androidx.camera.core.impl.SurfaceConfig;
import android.util.Size;
import androidx.camera.camera2.internal.compat.quirk.DeviceQuirks;
import androidx.camera.camera2.internal.compat.quirk.ExtraCroppingQuirk;

public class MaxPreviewSize
{
    private final ExtraCroppingQuirk mExtraCroppingQuirk;
    
    public MaxPreviewSize() {
        this(DeviceQuirks.get(ExtraCroppingQuirk.class));
    }
    
    MaxPreviewSize(final ExtraCroppingQuirk mExtraCroppingQuirk) {
        this.mExtraCroppingQuirk = mExtraCroppingQuirk;
    }
    
    public Size getMaxPreviewResolution(Size size) {
        final ExtraCroppingQuirk mExtraCroppingQuirk = this.mExtraCroppingQuirk;
        if (mExtraCroppingQuirk == null) {
            return size;
        }
        final Size verifiedResolution = mExtraCroppingQuirk.getVerifiedResolution(SurfaceConfig.ConfigType.PRIV);
        if (verifiedResolution == null) {
            return size;
        }
        if (verifiedResolution.getWidth() * verifiedResolution.getHeight() > size.getWidth() * size.getHeight()) {
            size = verifiedResolution;
        }
        return size;
    }
}

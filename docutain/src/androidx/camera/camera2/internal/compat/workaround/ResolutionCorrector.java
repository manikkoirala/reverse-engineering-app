// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.internal.compat.workaround;

import java.util.Iterator;
import java.util.ArrayList;
import android.util.Size;
import java.util.List;
import androidx.camera.core.impl.SurfaceConfig;
import androidx.camera.camera2.internal.compat.quirk.DeviceQuirks;
import androidx.camera.camera2.internal.compat.quirk.ExtraCroppingQuirk;

public class ResolutionCorrector
{
    private final ExtraCroppingQuirk mExtraCroppingQuirk;
    
    public ResolutionCorrector() {
        this(DeviceQuirks.get(ExtraCroppingQuirk.class));
    }
    
    ResolutionCorrector(final ExtraCroppingQuirk mExtraCroppingQuirk) {
        this.mExtraCroppingQuirk = mExtraCroppingQuirk;
    }
    
    public List<Size> insertOrPrioritize(final SurfaceConfig.ConfigType configType, final List<Size> list) {
        final ExtraCroppingQuirk mExtraCroppingQuirk = this.mExtraCroppingQuirk;
        if (mExtraCroppingQuirk == null) {
            return list;
        }
        final Size verifiedResolution = mExtraCroppingQuirk.getVerifiedResolution(configType);
        if (verifiedResolution == null) {
            return list;
        }
        final ArrayList list2 = new ArrayList();
        list2.add(verifiedResolution);
        for (final Size size : list) {
            if (!size.equals((Object)verifiedResolution)) {
                list2.add(size);
            }
        }
        return list2;
    }
}

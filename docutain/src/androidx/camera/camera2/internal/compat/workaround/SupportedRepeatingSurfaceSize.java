// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.internal.compat.workaround;

import java.util.ArrayList;
import androidx.camera.camera2.internal.compat.quirk.DeviceQuirks;
import androidx.camera.core.impl.utils.CompareSizesByArea;
import androidx.camera.camera2.internal.compat.quirk.RepeatingStreamConstraintForVideoRecordingQuirk;
import java.util.Comparator;
import android.util.Size;

public class SupportedRepeatingSurfaceSize
{
    private static final Size MINI_PREVIEW_SIZE_HUAWEI_MATE_9;
    private static final Comparator<Size> SIZE_COMPARATOR;
    private final RepeatingStreamConstraintForVideoRecordingQuirk mQuirk;
    
    static {
        MINI_PREVIEW_SIZE_HUAWEI_MATE_9 = new Size(320, 240);
        SIZE_COMPARATOR = new CompareSizesByArea();
    }
    
    public SupportedRepeatingSurfaceSize() {
        this.mQuirk = DeviceQuirks.get(RepeatingStreamConstraintForVideoRecordingQuirk.class);
    }
    
    public Size[] getSupportedSizes(final Size[] array) {
        Size[] array2 = array;
        if (this.mQuirk != null) {
            array2 = array;
            if (RepeatingStreamConstraintForVideoRecordingQuirk.isHuaweiMate9()) {
                final ArrayList list = new ArrayList();
                for (final Size size : array) {
                    if (SupportedRepeatingSurfaceSize.SIZE_COMPARATOR.compare(size, SupportedRepeatingSurfaceSize.MINI_PREVIEW_SIZE_HUAWEI_MATE_9) >= 0) {
                        list.add(size);
                    }
                }
                array2 = (Size[])list.toArray(new Size[0]);
            }
        }
        return array2;
    }
}

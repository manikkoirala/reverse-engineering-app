// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.internal.compat.workaround;

import android.hardware.camera2.CameraAccessException;
import androidx.camera.camera2.internal.Camera2CaptureCallbacks;
import java.util.Iterator;
import androidx.camera.core.impl.utils.futures.AsyncFunction;
import androidx.camera.core.impl.utils.executor.CameraXExecutors;
import androidx.camera.core.impl.utils.futures.FutureChain;
import java.util.Collection;
import java.util.ArrayList;
import androidx.camera.camera2.internal.SynchronizedCaptureSession;
import androidx.camera.core.impl.DeferrableSurface;
import java.util.List;
import androidx.camera.camera2.internal.compat.params.SessionConfigurationCompat;
import android.hardware.camera2.CameraDevice;
import androidx.camera.core.impl.utils.futures.Futures;
import androidx.camera.core.impl.Quirk;
import androidx.camera.camera2.internal.compat.quirk.CaptureSessionStuckQuirk;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.CameraCaptureSession;
import androidx.camera.core.impl.Quirks;
import com.google.common.util.concurrent.ListenableFuture;
import androidx.concurrent.futures.CallbackToFutureAdapter;
import android.hardware.camera2.CameraCaptureSession$CaptureCallback;

public class WaitForRepeatingRequestStart
{
    private final CameraCaptureSession$CaptureCallback mCaptureCallback;
    private final boolean mHasCaptureSessionStuckQuirk;
    private boolean mHasSubmittedRepeating;
    private final Object mLock;
    CallbackToFutureAdapter.Completer<Void> mStartStreamingCompleter;
    private final ListenableFuture<Void> mStartStreamingFuture;
    
    public WaitForRepeatingRequestStart(final Quirks quirks) {
        this.mLock = new Object();
        this.mCaptureCallback = new CameraCaptureSession$CaptureCallback() {
            final WaitForRepeatingRequestStart this$0;
            
            public void onCaptureSequenceAborted(final CameraCaptureSession cameraCaptureSession, final int n) {
                if (this.this$0.mStartStreamingCompleter != null) {
                    this.this$0.mStartStreamingCompleter.setCancelled();
                    this.this$0.mStartStreamingCompleter = null;
                }
            }
            
            public void onCaptureStarted(final CameraCaptureSession cameraCaptureSession, final CaptureRequest captureRequest, final long n, final long n2) {
                if (this.this$0.mStartStreamingCompleter != null) {
                    this.this$0.mStartStreamingCompleter.set(null);
                    this.this$0.mStartStreamingCompleter = null;
                }
            }
        };
        this.mHasCaptureSessionStuckQuirk = quirks.contains(CaptureSessionStuckQuirk.class);
        if (this.shouldWaitRepeatingSubmit()) {
            this.mStartStreamingFuture = CallbackToFutureAdapter.getFuture((CallbackToFutureAdapter.Resolver<Void>)new WaitForRepeatingRequestStart$$ExternalSyntheticLambda0(this));
        }
        else {
            this.mStartStreamingFuture = Futures.immediateFuture((Void)null);
        }
    }
    
    public ListenableFuture<Void> getStartStreamFuture() {
        return Futures.nonCancellationPropagating(this.mStartStreamingFuture);
    }
    
    public void onSessionEnd() {
        synchronized (this.mLock) {
            if (this.shouldWaitRepeatingSubmit() && !this.mHasSubmittedRepeating) {
                this.mStartStreamingFuture.cancel(true);
            }
        }
    }
    
    public ListenableFuture<Void> openCaptureSession(final CameraDevice cameraDevice, final SessionConfigurationCompat sessionConfigurationCompat, final List<DeferrableSurface> list, final List<SynchronizedCaptureSession> list2, final OpenCaptureSession openCaptureSession) {
        final ArrayList list3 = new ArrayList();
        final Iterator<SynchronizedCaptureSession> iterator = list2.iterator();
        while (iterator.hasNext()) {
            list3.add(iterator.next().getOpeningBlocker());
        }
        return (ListenableFuture<Void>)FutureChain.from(Futures.successfulAsList((Collection<? extends com.google.common.util.concurrent.ListenableFuture<?>>)list3)).transformAsync((AsyncFunction<? super List<Object>, Object>)new WaitForRepeatingRequestStart$$ExternalSyntheticLambda1(openCaptureSession, cameraDevice, sessionConfigurationCompat, list), CameraXExecutors.directExecutor());
    }
    
    public int setSingleRepeatingRequest(final CaptureRequest captureRequest, final CameraCaptureSession$CaptureCallback cameraCaptureSession$CaptureCallback, final SingleRepeatingRequest singleRepeatingRequest) throws CameraAccessException {
        final Object mLock = this.mLock;
        monitorenter(mLock);
        CameraCaptureSession$CaptureCallback comboCallback = cameraCaptureSession$CaptureCallback;
        try {
            if (this.shouldWaitRepeatingSubmit()) {
                comboCallback = Camera2CaptureCallbacks.createComboCallback(this.mCaptureCallback, cameraCaptureSession$CaptureCallback);
                this.mHasSubmittedRepeating = true;
            }
            return singleRepeatingRequest.run(captureRequest, comboCallback);
        }
        finally {
            monitorexit(mLock);
        }
    }
    
    public boolean shouldWaitRepeatingSubmit() {
        return this.mHasCaptureSessionStuckQuirk;
    }
    
    @FunctionalInterface
    public interface OpenCaptureSession
    {
        ListenableFuture<Void> run(final CameraDevice p0, final SessionConfigurationCompat p1, final List<DeferrableSurface> p2);
    }
    
    @FunctionalInterface
    public interface SingleRepeatingRequest
    {
        int run(final CaptureRequest p0, final CameraCaptureSession$CaptureCallback p1) throws CameraAccessException;
    }
}

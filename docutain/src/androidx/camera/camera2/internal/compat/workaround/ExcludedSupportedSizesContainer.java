// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.internal.compat.workaround;

import java.util.ArrayList;
import androidx.camera.camera2.internal.compat.quirk.DeviceQuirks;
import androidx.camera.camera2.internal.compat.quirk.ExcludedSupportedSizesQuirk;
import android.util.Size;
import java.util.List;

public class ExcludedSupportedSizesContainer
{
    private final String mCameraId;
    
    public ExcludedSupportedSizesContainer(final String mCameraId) {
        this.mCameraId = mCameraId;
    }
    
    public List<Size> get(final int n) {
        final ExcludedSupportedSizesQuirk excludedSupportedSizesQuirk = DeviceQuirks.get(ExcludedSupportedSizesQuirk.class);
        if (excludedSupportedSizesQuirk == null) {
            return new ArrayList<Size>();
        }
        return excludedSupportedSizesQuirk.getExcludedSizes(this.mCameraId, n);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.internal.compat.workaround;

import android.hardware.camera2.CaptureRequest$Key;
import android.hardware.camera2.CaptureRequest;
import androidx.camera.camera2.internal.compat.quirk.DeviceQuirks;
import androidx.camera.camera2.internal.compat.quirk.ImageCapturePixelHDRPlusQuirk;
import androidx.camera.camera2.impl.Camera2ImplConfig;

public class ImageCapturePixelHDRPlus
{
    public void toggleHDRPlus(final int n, final Camera2ImplConfig.Builder builder) {
        if (DeviceQuirks.get(ImageCapturePixelHDRPlusQuirk.class) == null) {
            return;
        }
        if (n != 0) {
            if (n == 1) {
                builder.setCaptureRequestOption((android.hardware.camera2.CaptureRequest$Key<Boolean>)CaptureRequest.CONTROL_ENABLE_ZSL, false);
            }
        }
        else {
            builder.setCaptureRequestOption((android.hardware.camera2.CaptureRequest$Key<Boolean>)CaptureRequest.CONTROL_ENABLE_ZSL, true);
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.internal.compat;

import android.hardware.camera2.CameraManager$AvailabilityCallback;
import android.hardware.camera2.params.OutputConfiguration;
import android.util.Size;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.CameraCaptureSession$CaptureCallback;
import android.view.Surface;
import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CameraCaptureSession$StateCallback;
import android.hardware.camera2.CameraDevice;

public final class ApiCompat
{
    private ApiCompat() {
    }
    
    public static class Api21Impl
    {
        private Api21Impl() {
        }
        
        public static void close(final CameraDevice cameraDevice) {
            cameraDevice.close();
        }
    }
    
    public static class Api23Impl
    {
        private Api23Impl() {
        }
        
        public static void onSurfacePrepared(final CameraCaptureSession$StateCallback cameraCaptureSession$StateCallback, final CameraCaptureSession cameraCaptureSession, final Surface surface) {
            cameraCaptureSession$StateCallback.onSurfacePrepared(cameraCaptureSession, surface);
        }
    }
    
    public static class Api24Impl
    {
        private Api24Impl() {
        }
        
        public static void onCaptureBufferLost(final CameraCaptureSession$CaptureCallback cameraCaptureSession$CaptureCallback, final CameraCaptureSession cameraCaptureSession, final CaptureRequest captureRequest, final Surface surface, final long n) {
            cameraCaptureSession$CaptureCallback.onCaptureBufferLost(cameraCaptureSession, captureRequest, surface, n);
        }
    }
    
    public static class Api26Impl
    {
        private Api26Impl() {
        }
        
        public static <T> OutputConfiguration newOutputConfiguration(final Size size, final Class<T> clazz) {
            return new OutputConfiguration(size, (Class)clazz);
        }
        
        public static void onCaptureQueueEmpty(final CameraCaptureSession$StateCallback cameraCaptureSession$StateCallback, final CameraCaptureSession cameraCaptureSession) {
            cameraCaptureSession$StateCallback.onCaptureQueueEmpty(cameraCaptureSession);
        }
    }
    
    public static class Api29Impl
    {
        private Api29Impl() {
        }
        
        public static void onCameraAccessPrioritiesChanged(final CameraManager$AvailabilityCallback cameraManager$AvailabilityCallback) {
            cameraManager$AvailabilityCallback.onCameraAccessPrioritiesChanged();
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.internal.compat;

import java.util.Set;
import android.os.Build$VERSION;
import java.util.HashMap;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraCharacteristics$Key;
import java.util.Map;

public class CameraCharacteristicsCompat
{
    private final CameraCharacteristicsCompatImpl mCameraCharacteristicsImpl;
    private final Map<CameraCharacteristics$Key<?>, Object> mValuesCache;
    
    private CameraCharacteristicsCompat(final CameraCharacteristics cameraCharacteristics) {
        this.mValuesCache = new HashMap<CameraCharacteristics$Key<?>, Object>();
        if (Build$VERSION.SDK_INT >= 28) {
            this.mCameraCharacteristicsImpl = (CameraCharacteristicsCompatImpl)new CameraCharacteristicsApi28Impl(cameraCharacteristics);
        }
        else {
            this.mCameraCharacteristicsImpl = (CameraCharacteristicsCompatImpl)new CameraCharacteristicsBaseImpl(cameraCharacteristics);
        }
    }
    
    private boolean isKeyNonCacheable(final CameraCharacteristics$Key<?> cameraCharacteristics$Key) {
        return cameraCharacteristics$Key.equals((Object)CameraCharacteristics.SENSOR_ORIENTATION);
    }
    
    public static CameraCharacteristicsCompat toCameraCharacteristicsCompat(final CameraCharacteristics cameraCharacteristics) {
        return new CameraCharacteristicsCompat(cameraCharacteristics);
    }
    
    public <T> T get(final CameraCharacteristics$Key<T> cameraCharacteristics$Key) {
        if (this.isKeyNonCacheable(cameraCharacteristics$Key)) {
            return this.mCameraCharacteristicsImpl.get(cameraCharacteristics$Key);
        }
        synchronized (this) {
            final Object value = this.mValuesCache.get(cameraCharacteristics$Key);
            if (value != null) {
                return (T)value;
            }
            final T value2 = this.mCameraCharacteristicsImpl.get(cameraCharacteristics$Key);
            if (value2 != null) {
                this.mValuesCache.put(cameraCharacteristics$Key, value2);
            }
            return value2;
        }
    }
    
    public Set<String> getPhysicalCameraIds() {
        return this.mCameraCharacteristicsImpl.getPhysicalCameraIds();
    }
    
    public CameraCharacteristics toCameraCharacteristics() {
        return this.mCameraCharacteristicsImpl.unwrap();
    }
    
    public interface CameraCharacteristicsCompatImpl
    {
         <T> T get(final CameraCharacteristics$Key<T> p0);
        
        Set<String> getPhysicalCameraIds();
        
        CameraCharacteristics unwrap();
    }
}

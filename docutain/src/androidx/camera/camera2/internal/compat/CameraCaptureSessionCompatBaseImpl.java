// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.internal.compat;

import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCaptureSession$CaptureCallback;
import java.util.concurrent.Executor;
import android.hardware.camera2.CaptureRequest;
import java.util.List;
import android.os.Handler;
import androidx.core.util.Preconditions;
import android.hardware.camera2.CameraCaptureSession;

class CameraCaptureSessionCompatBaseImpl implements CameraCaptureSessionCompatImpl
{
    final CameraCaptureSession mCameraCaptureSession;
    final Object mObject;
    
    CameraCaptureSessionCompatBaseImpl(final CameraCaptureSession cameraCaptureSession, final Object mObject) {
        this.mCameraCaptureSession = Preconditions.checkNotNull(cameraCaptureSession);
        this.mObject = mObject;
    }
    
    static CameraCaptureSessionCompatImpl create(final CameraCaptureSession cameraCaptureSession, final Handler handler) {
        return new CameraCaptureSessionCompatBaseImpl(cameraCaptureSession, new CameraCaptureSessionCompatParamsApi21(handler));
    }
    
    @Override
    public int captureBurstRequests(final List<CaptureRequest> list, final Executor executor, final CameraCaptureSession$CaptureCallback cameraCaptureSession$CaptureCallback) throws CameraAccessException {
        return this.mCameraCaptureSession.captureBurst((List)list, (CameraCaptureSession$CaptureCallback)new CameraCaptureSessionCompat.CaptureCallbackExecutorWrapper(executor, cameraCaptureSession$CaptureCallback), ((CameraCaptureSessionCompatParamsApi21)this.mObject).mCompatHandler);
    }
    
    @Override
    public int captureSingleRequest(final CaptureRequest captureRequest, final Executor executor, final CameraCaptureSession$CaptureCallback cameraCaptureSession$CaptureCallback) throws CameraAccessException {
        return this.mCameraCaptureSession.capture(captureRequest, (CameraCaptureSession$CaptureCallback)new CameraCaptureSessionCompat.CaptureCallbackExecutorWrapper(executor, cameraCaptureSession$CaptureCallback), ((CameraCaptureSessionCompatParamsApi21)this.mObject).mCompatHandler);
    }
    
    @Override
    public int setRepeatingBurstRequests(final List<CaptureRequest> list, final Executor executor, final CameraCaptureSession$CaptureCallback cameraCaptureSession$CaptureCallback) throws CameraAccessException {
        return this.mCameraCaptureSession.setRepeatingBurst((List)list, (CameraCaptureSession$CaptureCallback)new CameraCaptureSessionCompat.CaptureCallbackExecutorWrapper(executor, cameraCaptureSession$CaptureCallback), ((CameraCaptureSessionCompatParamsApi21)this.mObject).mCompatHandler);
    }
    
    @Override
    public int setSingleRepeatingRequest(final CaptureRequest captureRequest, final Executor executor, final CameraCaptureSession$CaptureCallback cameraCaptureSession$CaptureCallback) throws CameraAccessException {
        return this.mCameraCaptureSession.setRepeatingRequest(captureRequest, (CameraCaptureSession$CaptureCallback)new CameraCaptureSessionCompat.CaptureCallbackExecutorWrapper(executor, cameraCaptureSession$CaptureCallback), ((CameraCaptureSessionCompatParamsApi21)this.mObject).mCompatHandler);
    }
    
    @Override
    public CameraCaptureSession unwrap() {
        return this.mCameraCaptureSession;
    }
    
    private static class CameraCaptureSessionCompatParamsApi21
    {
        final Handler mCompatHandler;
        
        CameraCaptureSessionCompatParamsApi21(final Handler mCompatHandler) {
            this.mCompatHandler = mCompatHandler;
        }
    }
}

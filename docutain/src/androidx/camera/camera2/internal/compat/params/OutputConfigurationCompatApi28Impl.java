// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.internal.compat.params;

import androidx.core.util.Preconditions;
import android.hardware.camera2.params.OutputConfiguration;
import android.view.Surface;

class OutputConfigurationCompatApi28Impl extends OutputConfigurationCompatApi26Impl
{
    OutputConfigurationCompatApi28Impl(final int n, final Surface surface) {
        this(new OutputConfiguration(n, surface));
    }
    
    OutputConfigurationCompatApi28Impl(final Surface surface) {
        super(new OutputConfiguration(surface));
    }
    
    OutputConfigurationCompatApi28Impl(final Object o) {
        super(o);
    }
    
    static OutputConfigurationCompatApi28Impl wrap(final OutputConfiguration outputConfiguration) {
        return new OutputConfigurationCompatApi28Impl(outputConfiguration);
    }
    
    @Override
    public int getMaxSharedSurfaceCount() {
        return ((OutputConfiguration)this.getOutputConfiguration()).getMaxSharedSurfaceCount();
    }
    
    @Override
    public Object getOutputConfiguration() {
        Preconditions.checkArgument(this.mObject instanceof OutputConfiguration);
        return this.mObject;
    }
    
    @Override
    public String getPhysicalCameraId() {
        return null;
    }
    
    @Override
    public void removeSurface(final Surface surface) {
        ((OutputConfiguration)this.getOutputConfiguration()).removeSurface(surface);
    }
    
    @Override
    public void setPhysicalCameraId(final String physicalCameraId) {
        ((OutputConfiguration)this.getOutputConfiguration()).setPhysicalCameraId(physicalCameraId);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.internal.compat.params;

import java.util.Objects;
import java.util.Collections;
import java.util.List;
import androidx.core.util.Preconditions;
import android.hardware.camera2.params.OutputConfiguration;
import android.view.Surface;

class OutputConfigurationCompatApi24Impl extends OutputConfigurationCompatBaseImpl
{
    OutputConfigurationCompatApi24Impl(final int n, final Surface surface) {
        this(new OutputConfigurationParamsApi24(new OutputConfiguration(n, surface)));
    }
    
    OutputConfigurationCompatApi24Impl(final Surface surface) {
        this(new OutputConfigurationParamsApi24(new OutputConfiguration(surface)));
    }
    
    OutputConfigurationCompatApi24Impl(final Object o) {
        super(o);
    }
    
    static OutputConfigurationCompatApi24Impl wrap(final OutputConfiguration outputConfiguration) {
        return new OutputConfigurationCompatApi24Impl(new OutputConfigurationParamsApi24(outputConfiguration));
    }
    
    @Override
    public void enableSurfaceSharing() {
        ((OutputConfigurationParamsApi24)this.mObject).mIsShared = true;
    }
    
    @Override
    public Object getOutputConfiguration() {
        Preconditions.checkArgument(this.mObject instanceof OutputConfigurationParamsApi24);
        return ((OutputConfigurationParamsApi24)this.mObject).mOutputConfiguration;
    }
    
    @Override
    public String getPhysicalCameraId() {
        return ((OutputConfigurationParamsApi24)this.mObject).mPhysicalCameraId;
    }
    
    @Override
    public Surface getSurface() {
        return ((OutputConfiguration)this.getOutputConfiguration()).getSurface();
    }
    
    @Override
    public int getSurfaceGroupId() {
        return ((OutputConfiguration)this.getOutputConfiguration()).getSurfaceGroupId();
    }
    
    @Override
    public List<Surface> getSurfaces() {
        return Collections.singletonList(this.getSurface());
    }
    
    @Override
    boolean isSurfaceSharingEnabled() {
        return ((OutputConfigurationParamsApi24)this.mObject).mIsShared;
    }
    
    @Override
    public void setPhysicalCameraId(final String mPhysicalCameraId) {
        ((OutputConfigurationParamsApi24)this.mObject).mPhysicalCameraId = mPhysicalCameraId;
    }
    
    private static final class OutputConfigurationParamsApi24
    {
        boolean mIsShared;
        final OutputConfiguration mOutputConfiguration;
        String mPhysicalCameraId;
        
        OutputConfigurationParamsApi24(final OutputConfiguration mOutputConfiguration) {
            this.mOutputConfiguration = mOutputConfiguration;
        }
        
        @Override
        public boolean equals(final Object o) {
            final boolean b = o instanceof OutputConfigurationParamsApi24;
            final boolean b2 = false;
            if (!b) {
                return false;
            }
            final OutputConfigurationParamsApi24 outputConfigurationParamsApi24 = (OutputConfigurationParamsApi24)o;
            boolean b3 = b2;
            if (Objects.equals(this.mOutputConfiguration, outputConfigurationParamsApi24.mOutputConfiguration)) {
                b3 = b2;
                if (this.mIsShared == outputConfigurationParamsApi24.mIsShared) {
                    b3 = b2;
                    if (Objects.equals(this.mPhysicalCameraId, outputConfigurationParamsApi24.mPhysicalCameraId)) {
                        b3 = true;
                    }
                }
            }
            return b3;
        }
        
        @Override
        public int hashCode() {
            final int n = this.mOutputConfiguration.hashCode() ^ 0x1F;
            final int n2 = (this.mIsShared ? 1 : 0) ^ (n << 5) - n;
            final String mPhysicalCameraId = this.mPhysicalCameraId;
            int hashCode;
            if (mPhysicalCameraId == null) {
                hashCode = 0;
            }
            else {
                hashCode = mPhysicalCameraId.hashCode();
            }
            return hashCode ^ (n2 << 5) - n2;
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.internal.compat.params;

import java.util.Objects;
import androidx.core.util.Preconditions;
import androidx.camera.core.Logger;
import java.util.List;
import java.lang.reflect.Field;
import android.hardware.camera2.params.OutputConfiguration;
import android.view.Surface;

class OutputConfigurationCompatApi26Impl extends OutputConfigurationCompatApi24Impl
{
    private static final String MAX_SHARED_SURFACES_COUNT_FIELD = "MAX_SURFACES_COUNT";
    private static final String SURFACES_FIELD = "mSurfaces";
    
    OutputConfigurationCompatApi26Impl(final int n, final Surface surface) {
        this(new OutputConfigurationParamsApi26(new OutputConfiguration(n, surface)));
    }
    
    OutputConfigurationCompatApi26Impl(final Surface surface) {
        this(new OutputConfigurationParamsApi26(new OutputConfiguration(surface)));
    }
    
    OutputConfigurationCompatApi26Impl(final Object o) {
        super(o);
    }
    
    private static int getMaxSharedSurfaceCountApi26() throws NoSuchFieldException, IllegalAccessException {
        final Field declaredField = OutputConfiguration.class.getDeclaredField("MAX_SURFACES_COUNT");
        declaredField.setAccessible(true);
        return declaredField.getInt(null);
    }
    
    private static List<Surface> getMutableSurfaceListApi26(final OutputConfiguration obj) throws NoSuchFieldException, IllegalAccessException {
        final Field declaredField = OutputConfiguration.class.getDeclaredField("mSurfaces");
        declaredField.setAccessible(true);
        return (List)declaredField.get(obj);
    }
    
    static OutputConfigurationCompatApi26Impl wrap(final OutputConfiguration outputConfiguration) {
        return new OutputConfigurationCompatApi26Impl(new OutputConfigurationParamsApi26(outputConfiguration));
    }
    
    @Override
    public void addSurface(final Surface surface) {
        ((OutputConfiguration)this.getOutputConfiguration()).addSurface(surface);
    }
    
    @Override
    public void enableSurfaceSharing() {
        ((OutputConfiguration)this.getOutputConfiguration()).enableSurfaceSharing();
    }
    
    @Override
    public int getMaxSharedSurfaceCount() {
        try {
            return getMaxSharedSurfaceCountApi26();
        }
        catch (final IllegalAccessException ex) {}
        catch (final NoSuchFieldException ex2) {}
        final IllegalAccessException ex;
        Logger.e("OutputConfigCompat", "Unable to retrieve max shared surface count.", ex);
        return super.getMaxSharedSurfaceCount();
    }
    
    @Override
    public Object getOutputConfiguration() {
        Preconditions.checkArgument(this.mObject instanceof OutputConfigurationParamsApi26);
        return ((OutputConfigurationParamsApi26)this.mObject).mOutputConfiguration;
    }
    
    @Override
    public String getPhysicalCameraId() {
        return ((OutputConfigurationParamsApi26)this.mObject).mPhysicalCameraId;
    }
    
    @Override
    public List<Surface> getSurfaces() {
        return ((OutputConfiguration)this.getOutputConfiguration()).getSurfaces();
    }
    
    @Override
    final boolean isSurfaceSharingEnabled() {
        throw new AssertionError((Object)"isSurfaceSharingEnabled() should not be called on API >= 26");
    }
    
    @Override
    public void removeSurface(Surface ex) {
        if (this.getSurface() != ex) {
            try {
                if (getMutableSurfaceListApi26((OutputConfiguration)this.getOutputConfiguration()).remove(ex)) {
                    return;
                }
                ex = (NoSuchFieldException)new IllegalArgumentException("Surface is not part of this output configuration");
                throw ex;
            }
            catch (final NoSuchFieldException ex) {}
            catch (final IllegalAccessException ex2) {}
            Logger.e("OutputConfigCompat", "Unable to remove surface from this output configuration.", ex);
            return;
        }
        throw new IllegalArgumentException("Cannot remove surface associated with this output configuration");
    }
    
    @Override
    public void setPhysicalCameraId(final String mPhysicalCameraId) {
        ((OutputConfigurationParamsApi26)this.mObject).mPhysicalCameraId = mPhysicalCameraId;
    }
    
    private static final class OutputConfigurationParamsApi26
    {
        final OutputConfiguration mOutputConfiguration;
        String mPhysicalCameraId;
        
        OutputConfigurationParamsApi26(final OutputConfiguration mOutputConfiguration) {
            this.mOutputConfiguration = mOutputConfiguration;
        }
        
        @Override
        public boolean equals(final Object o) {
            final boolean b = o instanceof OutputConfigurationParamsApi26;
            final boolean b2 = false;
            if (!b) {
                return false;
            }
            final OutputConfigurationParamsApi26 outputConfigurationParamsApi26 = (OutputConfigurationParamsApi26)o;
            boolean b3 = b2;
            if (Objects.equals(this.mOutputConfiguration, outputConfigurationParamsApi26.mOutputConfiguration)) {
                b3 = b2;
                if (Objects.equals(this.mPhysicalCameraId, outputConfigurationParamsApi26.mPhysicalCameraId)) {
                    b3 = true;
                }
            }
            return b3;
        }
        
        @Override
        public int hashCode() {
            final int n = this.mOutputConfiguration.hashCode() ^ 0x1F;
            final String mPhysicalCameraId = this.mPhysicalCameraId;
            int hashCode;
            if (mPhysicalCameraId == null) {
                hashCode = 0;
            }
            else {
                hashCode = mPhysicalCameraId.hashCode();
            }
            return hashCode ^ (n << 5) - n;
        }
    }
}

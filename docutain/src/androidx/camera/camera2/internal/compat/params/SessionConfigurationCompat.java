// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.internal.compat.params;

import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;
import java.util.Collection;
import android.hardware.camera2.params.InputConfiguration;
import java.util.Objects;
import java.util.Collections;
import android.hardware.camera2.params.SessionConfiguration;
import android.hardware.camera2.CaptureRequest;
import java.util.Iterator;
import java.util.ArrayList;
import android.hardware.camera2.params.OutputConfiguration;
import android.os.Build$VERSION;
import android.hardware.camera2.CameraCaptureSession$StateCallback;
import java.util.concurrent.Executor;
import java.util.List;

public final class SessionConfigurationCompat
{
    public static final int SESSION_HIGH_SPEED = 1;
    public static final int SESSION_REGULAR = 0;
    private final SessionConfigurationCompatImpl mImpl;
    
    public SessionConfigurationCompat(final int n, final List<OutputConfigurationCompat> list, final Executor executor, final CameraCaptureSession$StateCallback cameraCaptureSession$StateCallback) {
        if (Build$VERSION.SDK_INT < 28) {
            this.mImpl = (SessionConfigurationCompatImpl)new SessionConfigurationCompatBaseImpl(n, list, executor, cameraCaptureSession$StateCallback);
        }
        else {
            this.mImpl = (SessionConfigurationCompatImpl)new SessionConfigurationCompatApi28Impl(n, list, executor, cameraCaptureSession$StateCallback);
        }
    }
    
    private SessionConfigurationCompat(final SessionConfigurationCompatImpl mImpl) {
        this.mImpl = mImpl;
    }
    
    public static List<OutputConfiguration> transformFromCompat(final List<OutputConfigurationCompat> list) {
        final ArrayList list2 = new ArrayList(list.size());
        final Iterator iterator = list.iterator();
        while (iterator.hasNext()) {
            list2.add(((OutputConfigurationCompat)iterator.next()).unwrap());
        }
        return list2;
    }
    
    static List<OutputConfigurationCompat> transformToCompat(final List<OutputConfiguration> list) {
        final ArrayList list2 = new ArrayList(list.size());
        final Iterator iterator = list.iterator();
        while (iterator.hasNext()) {
            list2.add(OutputConfigurationCompat.wrap(iterator.next()));
        }
        return list2;
    }
    
    public static SessionConfigurationCompat wrap(final Object o) {
        if (o == null) {
            return null;
        }
        if (Build$VERSION.SDK_INT < 28) {
            return null;
        }
        return new SessionConfigurationCompat((SessionConfigurationCompatImpl)new SessionConfigurationCompatApi28Impl(o));
    }
    
    @Override
    public boolean equals(final Object o) {
        return o instanceof SessionConfigurationCompat && this.mImpl.equals(((SessionConfigurationCompat)o).mImpl);
    }
    
    public Executor getExecutor() {
        return this.mImpl.getExecutor();
    }
    
    public InputConfigurationCompat getInputConfiguration() {
        return this.mImpl.getInputConfiguration();
    }
    
    public List<OutputConfigurationCompat> getOutputConfigurations() {
        return this.mImpl.getOutputConfigurations();
    }
    
    public CaptureRequest getSessionParameters() {
        return this.mImpl.getSessionParameters();
    }
    
    public int getSessionType() {
        return this.mImpl.getSessionType();
    }
    
    public CameraCaptureSession$StateCallback getStateCallback() {
        return this.mImpl.getStateCallback();
    }
    
    @Override
    public int hashCode() {
        return this.mImpl.hashCode();
    }
    
    public void setInputConfiguration(final InputConfigurationCompat inputConfiguration) {
        this.mImpl.setInputConfiguration(inputConfiguration);
    }
    
    public void setSessionParameters(final CaptureRequest sessionParameters) {
        this.mImpl.setSessionParameters(sessionParameters);
    }
    
    public Object unwrap() {
        return this.mImpl.getSessionConfiguration();
    }
    
    private static final class SessionConfigurationCompatApi28Impl implements SessionConfigurationCompatImpl
    {
        private final SessionConfiguration mObject;
        private final List<OutputConfigurationCompat> mOutputConfigurations;
        
        SessionConfigurationCompatApi28Impl(final int n, final List<OutputConfigurationCompat> list, final Executor executor, final CameraCaptureSession$StateCallback cameraCaptureSession$StateCallback) {
            this(new SessionConfiguration(n, (List)SessionConfigurationCompat.transformFromCompat(list), executor, cameraCaptureSession$StateCallback));
        }
        
        SessionConfigurationCompatApi28Impl(final Object o) {
            final SessionConfiguration mObject = (SessionConfiguration)o;
            this.mObject = mObject;
            this.mOutputConfigurations = (List<OutputConfigurationCompat>)Collections.unmodifiableList((List<?>)SessionConfigurationCompat.transformToCompat(mObject.getOutputConfigurations()));
        }
        
        @Override
        public boolean equals(final Object o) {
            return o instanceof SessionConfigurationCompatApi28Impl && Objects.equals(this.mObject, ((SessionConfigurationCompatApi28Impl)o).mObject);
        }
        
        @Override
        public Executor getExecutor() {
            return this.mObject.getExecutor();
        }
        
        @Override
        public InputConfigurationCompat getInputConfiguration() {
            return InputConfigurationCompat.wrap(this.mObject.getInputConfiguration());
        }
        
        @Override
        public List<OutputConfigurationCompat> getOutputConfigurations() {
            return this.mOutputConfigurations;
        }
        
        @Override
        public Object getSessionConfiguration() {
            return this.mObject;
        }
        
        @Override
        public CaptureRequest getSessionParameters() {
            return this.mObject.getSessionParameters();
        }
        
        @Override
        public int getSessionType() {
            return this.mObject.getSessionType();
        }
        
        @Override
        public CameraCaptureSession$StateCallback getStateCallback() {
            return this.mObject.getStateCallback();
        }
        
        @Override
        public int hashCode() {
            return this.mObject.hashCode();
        }
        
        @Override
        public void setInputConfiguration(final InputConfigurationCompat inputConfigurationCompat) {
            this.mObject.setInputConfiguration((InputConfiguration)inputConfigurationCompat.unwrap());
        }
        
        @Override
        public void setSessionParameters(final CaptureRequest sessionParameters) {
            this.mObject.setSessionParameters(sessionParameters);
        }
    }
    
    private static final class SessionConfigurationCompatBaseImpl implements SessionConfigurationCompatImpl
    {
        private final Executor mExecutor;
        private InputConfigurationCompat mInputConfig;
        private final List<OutputConfigurationCompat> mOutputConfigurations;
        private CaptureRequest mSessionParameters;
        private final int mSessionType;
        private final CameraCaptureSession$StateCallback mStateCallback;
        
        SessionConfigurationCompatBaseImpl(final int mSessionType, final List<OutputConfigurationCompat> c, final Executor mExecutor, final CameraCaptureSession$StateCallback mStateCallback) {
            this.mInputConfig = null;
            this.mSessionParameters = null;
            this.mSessionType = mSessionType;
            this.mOutputConfigurations = Collections.unmodifiableList((List<? extends OutputConfigurationCompat>)new ArrayList<OutputConfigurationCompat>(c));
            this.mStateCallback = mStateCallback;
            this.mExecutor = mExecutor;
        }
        
        @Override
        public boolean equals(final Object o) {
            if (this == o) {
                return true;
            }
            if (o instanceof SessionConfigurationCompatBaseImpl) {
                final SessionConfigurationCompatBaseImpl sessionConfigurationCompatBaseImpl = (SessionConfigurationCompatBaseImpl)o;
                if (Objects.equals(this.mInputConfig, sessionConfigurationCompatBaseImpl.mInputConfig) && this.mSessionType == sessionConfigurationCompatBaseImpl.mSessionType) {
                    if (this.mOutputConfigurations.size() == sessionConfigurationCompatBaseImpl.mOutputConfigurations.size()) {
                        for (int i = 0; i < this.mOutputConfigurations.size(); ++i) {
                            if (!this.mOutputConfigurations.get(i).equals(sessionConfigurationCompatBaseImpl.mOutputConfigurations.get(i))) {
                                return false;
                            }
                        }
                        return true;
                    }
                }
            }
            return false;
        }
        
        @Override
        public Executor getExecutor() {
            return this.mExecutor;
        }
        
        @Override
        public InputConfigurationCompat getInputConfiguration() {
            return this.mInputConfig;
        }
        
        @Override
        public List<OutputConfigurationCompat> getOutputConfigurations() {
            return this.mOutputConfigurations;
        }
        
        @Override
        public Object getSessionConfiguration() {
            return null;
        }
        
        @Override
        public CaptureRequest getSessionParameters() {
            return this.mSessionParameters;
        }
        
        @Override
        public int getSessionType() {
            return this.mSessionType;
        }
        
        @Override
        public CameraCaptureSession$StateCallback getStateCallback() {
            return this.mStateCallback;
        }
        
        @Override
        public int hashCode() {
            final int n = this.mOutputConfigurations.hashCode() ^ 0x1F;
            final InputConfigurationCompat mInputConfig = this.mInputConfig;
            int hashCode;
            if (mInputConfig == null) {
                hashCode = 0;
            }
            else {
                hashCode = mInputConfig.hashCode();
            }
            final int n2 = hashCode ^ (n << 5) - n;
            return this.mSessionType ^ (n2 << 5) - n2;
        }
        
        @Override
        public void setInputConfiguration(final InputConfigurationCompat mInputConfig) {
            if (this.mSessionType != 1) {
                this.mInputConfig = mInputConfig;
                return;
            }
            throw new UnsupportedOperationException("Method not supported for high speed session types");
        }
        
        @Override
        public void setSessionParameters(final CaptureRequest mSessionParameters) {
            this.mSessionParameters = mSessionParameters;
        }
    }
    
    private interface SessionConfigurationCompatImpl
    {
        Executor getExecutor();
        
        InputConfigurationCompat getInputConfiguration();
        
        List<OutputConfigurationCompat> getOutputConfigurations();
        
        Object getSessionConfiguration();
        
        CaptureRequest getSessionParameters();
        
        int getSessionType();
        
        CameraCaptureSession$StateCallback getStateCallback();
        
        void setInputConfiguration(final InputConfigurationCompat p0);
        
        void setSessionParameters(final CaptureRequest p0);
    }
    
    @Retention(RetentionPolicy.SOURCE)
    public @interface SessionMode {
    }
}

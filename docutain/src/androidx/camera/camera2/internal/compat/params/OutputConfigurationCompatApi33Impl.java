// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.internal.compat.params;

import java.util.List;
import android.hardware.camera2.params.OutputConfiguration;
import android.view.Surface;

public class OutputConfigurationCompatApi33Impl extends OutputConfigurationCompatApi28Impl
{
    OutputConfigurationCompatApi33Impl(final int n, final Surface surface) {
        this(new OutputConfiguration(n, surface));
    }
    
    OutputConfigurationCompatApi33Impl(final Surface surface) {
        super(new OutputConfiguration(surface));
    }
    
    OutputConfigurationCompatApi33Impl(final Object o) {
        super(o);
    }
    
    static OutputConfigurationCompatApi33Impl wrap(final OutputConfiguration outputConfiguration) {
        return new OutputConfigurationCompatApi33Impl(outputConfiguration);
    }
    
    @Override
    public long getStreamUseCase() {
        return ((OutputConfiguration)this.getOutputConfiguration()).getStreamUseCase();
    }
    
    @Override
    public void setStreamUseCase(final long streamUseCase) {
        if (streamUseCase == -1L) {
            return;
        }
        ((OutputConfiguration)this.getOutputConfiguration()).setStreamUseCase(streamUseCase);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.internal.compat.params;

import java.util.List;
import android.hardware.camera2.params.OutputConfiguration;
import androidx.camera.camera2.internal.compat.ApiCompat;
import android.util.Size;
import android.os.Build$VERSION;
import android.view.Surface;

public final class OutputConfigurationCompat
{
    public static final int STREAM_USE_CASE_NONE = -1;
    public static final int SURFACE_GROUP_ID_NONE = -1;
    private final OutputConfigurationCompatImpl mImpl;
    
    public OutputConfigurationCompat(final int n, final Surface surface) {
        if (Build$VERSION.SDK_INT >= 33) {
            this.mImpl = (OutputConfigurationCompatImpl)new OutputConfigurationCompatApi33Impl(n, surface);
        }
        else if (Build$VERSION.SDK_INT >= 28) {
            this.mImpl = (OutputConfigurationCompatImpl)new OutputConfigurationCompatApi28Impl(n, surface);
        }
        else if (Build$VERSION.SDK_INT >= 26) {
            this.mImpl = (OutputConfigurationCompatImpl)new OutputConfigurationCompatApi26Impl(n, surface);
        }
        else if (Build$VERSION.SDK_INT >= 24) {
            this.mImpl = (OutputConfigurationCompatImpl)new OutputConfigurationCompatApi24Impl(n, surface);
        }
        else {
            this.mImpl = (OutputConfigurationCompatImpl)new OutputConfigurationCompatBaseImpl(surface);
        }
    }
    
    public <T> OutputConfigurationCompat(final Size size, final Class<T> clazz) {
        final OutputConfiguration outputConfiguration = ApiCompat.Api26Impl.newOutputConfiguration(size, clazz);
        if (Build$VERSION.SDK_INT >= 33) {
            this.mImpl = (OutputConfigurationCompatImpl)OutputConfigurationCompatApi33Impl.wrap(outputConfiguration);
        }
        else if (Build$VERSION.SDK_INT >= 28) {
            this.mImpl = (OutputConfigurationCompatImpl)OutputConfigurationCompatApi28Impl.wrap(outputConfiguration);
        }
        else {
            this.mImpl = (OutputConfigurationCompatImpl)OutputConfigurationCompatApi26Impl.wrap(outputConfiguration);
        }
    }
    
    public OutputConfigurationCompat(final Surface surface) {
        this(-1, surface);
    }
    
    private OutputConfigurationCompat(final OutputConfigurationCompatImpl mImpl) {
        this.mImpl = mImpl;
    }
    
    public static OutputConfigurationCompat wrap(final Object o) {
        if (o == null) {
            return null;
        }
        Object o2;
        if (Build$VERSION.SDK_INT >= 33) {
            o2 = OutputConfigurationCompatApi33Impl.wrap((OutputConfiguration)o);
        }
        else if (Build$VERSION.SDK_INT >= 28) {
            o2 = OutputConfigurationCompatApi28Impl.wrap((OutputConfiguration)o);
        }
        else if (Build$VERSION.SDK_INT >= 26) {
            o2 = OutputConfigurationCompatApi26Impl.wrap((OutputConfiguration)o);
        }
        else if (Build$VERSION.SDK_INT >= 24) {
            o2 = OutputConfigurationCompatApi24Impl.wrap((OutputConfiguration)o);
        }
        else {
            o2 = null;
        }
        if (o2 == null) {
            return null;
        }
        return new OutputConfigurationCompat((OutputConfigurationCompatImpl)o2);
    }
    
    public void addSurface(final Surface surface) {
        this.mImpl.addSurface(surface);
    }
    
    public void enableSurfaceSharing() {
        this.mImpl.enableSurfaceSharing();
    }
    
    @Override
    public boolean equals(final Object o) {
        return o instanceof OutputConfigurationCompat && this.mImpl.equals(((OutputConfigurationCompat)o).mImpl);
    }
    
    public int getMaxSharedSurfaceCount() {
        return this.mImpl.getMaxSharedSurfaceCount();
    }
    
    public String getPhysicalCameraId() {
        return this.mImpl.getPhysicalCameraId();
    }
    
    public long getStreamUseCase() {
        return this.mImpl.getStreamUseCase();
    }
    
    public Surface getSurface() {
        return this.mImpl.getSurface();
    }
    
    public int getSurfaceGroupId() {
        return this.mImpl.getSurfaceGroupId();
    }
    
    public List<Surface> getSurfaces() {
        return this.mImpl.getSurfaces();
    }
    
    @Override
    public int hashCode() {
        return this.mImpl.hashCode();
    }
    
    public void removeSurface(final Surface surface) {
        this.mImpl.removeSurface(surface);
    }
    
    public void setPhysicalCameraId(final String physicalCameraId) {
        this.mImpl.setPhysicalCameraId(physicalCameraId);
    }
    
    public void setStreamUseCase(final long streamUseCase) {
        this.mImpl.setStreamUseCase(streamUseCase);
    }
    
    public Object unwrap() {
        return this.mImpl.getOutputConfiguration();
    }
    
    interface OutputConfigurationCompatImpl
    {
        void addSurface(final Surface p0);
        
        void enableSurfaceSharing();
        
        int getMaxSharedSurfaceCount();
        
        Object getOutputConfiguration();
        
        String getPhysicalCameraId();
        
        long getStreamUseCase();
        
        Surface getSurface();
        
        int getSurfaceGroupId();
        
        List<Surface> getSurfaces();
        
        void removeSurface(final Surface p0);
        
        void setPhysicalCameraId(final String p0);
        
        void setStreamUseCase(final long p0);
    }
}

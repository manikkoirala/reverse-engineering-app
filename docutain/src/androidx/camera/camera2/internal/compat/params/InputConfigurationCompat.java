// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.internal.compat.params;

import java.util.Objects;
import android.hardware.camera2.params.InputConfiguration;
import android.os.Build$VERSION;

public final class InputConfigurationCompat
{
    private final InputConfigurationCompatImpl mImpl;
    
    public InputConfigurationCompat(final int n, final int n2, final int n3) {
        if (Build$VERSION.SDK_INT >= 31) {
            this.mImpl = (InputConfigurationCompatImpl)new InputConfigurationCompatApi31Impl(n, n2, n3);
        }
        else if (Build$VERSION.SDK_INT >= 23) {
            this.mImpl = (InputConfigurationCompatImpl)new InputConfigurationCompatApi23Impl(n, n2, n3);
        }
        else {
            this.mImpl = (InputConfigurationCompatImpl)new InputConfigurationCompatBaseImpl(n, n2, n3);
        }
    }
    
    private InputConfigurationCompat(final InputConfigurationCompatImpl mImpl) {
        this.mImpl = mImpl;
    }
    
    public static InputConfigurationCompat wrap(final Object o) {
        if (o == null) {
            return null;
        }
        if (Build$VERSION.SDK_INT < 23) {
            return null;
        }
        if (Build$VERSION.SDK_INT >= 31) {
            return new InputConfigurationCompat((InputConfigurationCompatImpl)new InputConfigurationCompatApi31Impl(o));
        }
        return new InputConfigurationCompat((InputConfigurationCompatImpl)new InputConfigurationCompatApi23Impl(o));
    }
    
    @Override
    public boolean equals(final Object o) {
        return o instanceof InputConfigurationCompat && this.mImpl.equals(((InputConfigurationCompat)o).mImpl);
    }
    
    public int getFormat() {
        return this.mImpl.getFormat();
    }
    
    public int getHeight() {
        return this.mImpl.getHeight();
    }
    
    public int getWidth() {
        return this.mImpl.getWidth();
    }
    
    @Override
    public int hashCode() {
        return this.mImpl.hashCode();
    }
    
    public boolean isMultiResolution() {
        return this.mImpl.isMultiResolution();
    }
    
    @Override
    public String toString() {
        return this.mImpl.toString();
    }
    
    public Object unwrap() {
        return this.mImpl.getInputConfiguration();
    }
    
    private static class InputConfigurationCompatApi23Impl implements InputConfigurationCompatImpl
    {
        private final InputConfiguration mObject;
        
        InputConfigurationCompatApi23Impl(final int n, final int n2, final int n3) {
            this(new InputConfiguration(n, n2, n3));
        }
        
        InputConfigurationCompatApi23Impl(final Object o) {
            this.mObject = (InputConfiguration)o;
        }
        
        @Override
        public boolean equals(final Object o) {
            return o instanceof InputConfigurationCompatImpl && Objects.equals(this.mObject, ((InputConfigurationCompatImpl)o).getInputConfiguration());
        }
        
        @Override
        public int getFormat() {
            return this.mObject.getFormat();
        }
        
        @Override
        public int getHeight() {
            return this.mObject.getHeight();
        }
        
        @Override
        public Object getInputConfiguration() {
            return this.mObject;
        }
        
        @Override
        public int getWidth() {
            return this.mObject.getWidth();
        }
        
        @Override
        public int hashCode() {
            return this.mObject.hashCode();
        }
        
        @Override
        public boolean isMultiResolution() {
            return false;
        }
        
        @Override
        public String toString() {
            return this.mObject.toString();
        }
    }
    
    private static final class InputConfigurationCompatApi31Impl extends InputConfigurationCompatApi23Impl
    {
        InputConfigurationCompatApi31Impl(final int n, final int n2, final int n3) {
            super(n, n2, n3);
        }
        
        InputConfigurationCompatApi31Impl(final Object o) {
            super(o);
        }
        
        @Override
        public boolean isMultiResolution() {
            return ((InputConfiguration)((InputConfigurationCompatApi23Impl)this).getInputConfiguration()).isMultiResolution();
        }
    }
    
    static final class InputConfigurationCompatBaseImpl implements InputConfigurationCompatImpl
    {
        private final int mFormat;
        private final int mHeight;
        private final int mWidth;
        
        InputConfigurationCompatBaseImpl(final int mWidth, final int mHeight, final int mFormat) {
            this.mWidth = mWidth;
            this.mHeight = mHeight;
            this.mFormat = mFormat;
        }
        
        @Override
        public boolean equals(final Object o) {
            final boolean b = o instanceof InputConfigurationCompatBaseImpl;
            final boolean b2 = false;
            if (!b) {
                return false;
            }
            final InputConfigurationCompatBaseImpl inputConfigurationCompatBaseImpl = (InputConfigurationCompatBaseImpl)o;
            boolean b3 = b2;
            if (inputConfigurationCompatBaseImpl.getWidth() == this.mWidth) {
                b3 = b2;
                if (inputConfigurationCompatBaseImpl.getHeight() == this.mHeight) {
                    b3 = b2;
                    if (inputConfigurationCompatBaseImpl.getFormat() == this.mFormat) {
                        b3 = true;
                    }
                }
            }
            return b3;
        }
        
        @Override
        public int getFormat() {
            return this.mFormat;
        }
        
        @Override
        public int getHeight() {
            return this.mHeight;
        }
        
        @Override
        public Object getInputConfiguration() {
            return null;
        }
        
        @Override
        public int getWidth() {
            return this.mWidth;
        }
        
        @Override
        public int hashCode() {
            final int n = this.mWidth ^ 0x1F;
            final int n2 = this.mHeight ^ (n << 5) - n;
            return this.mFormat ^ (n2 << 5) - n2;
        }
        
        @Override
        public boolean isMultiResolution() {
            return false;
        }
        
        @Override
        public String toString() {
            return String.format("InputConfiguration(w:%d, h:%d, format:%d)", this.mWidth, this.mHeight, this.mFormat);
        }
    }
    
    private interface InputConfigurationCompatImpl
    {
        int getFormat();
        
        int getHeight();
        
        Object getInputConfiguration();
        
        int getWidth();
        
        boolean isMultiResolution();
    }
}

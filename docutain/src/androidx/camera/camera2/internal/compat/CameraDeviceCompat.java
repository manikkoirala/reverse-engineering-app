// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.internal.compat;

import java.util.concurrent.Executor;
import android.hardware.camera2.CameraDevice$StateCallback;
import androidx.camera.camera2.internal.compat.params.SessionConfigurationCompat;
import androidx.camera.core.impl.utils.MainThreadAsyncHandler;
import android.os.Build$VERSION;
import android.os.Handler;
import android.hardware.camera2.CameraDevice;

public final class CameraDeviceCompat
{
    public static final int SESSION_OPERATION_MODE_CONSTRAINED_HIGH_SPEED = 1;
    public static final int SESSION_OPERATION_MODE_NORMAL = 0;
    private final CameraDeviceCompatImpl mImpl;
    
    private CameraDeviceCompat(final CameraDevice cameraDevice, final Handler handler) {
        if (Build$VERSION.SDK_INT >= 28) {
            this.mImpl = (CameraDeviceCompatImpl)new CameraDeviceCompatApi28Impl(cameraDevice);
        }
        else if (Build$VERSION.SDK_INT >= 24) {
            this.mImpl = (CameraDeviceCompatImpl)CameraDeviceCompatApi24Impl.create(cameraDevice, handler);
        }
        else if (Build$VERSION.SDK_INT >= 23) {
            this.mImpl = (CameraDeviceCompatImpl)CameraDeviceCompatApi23Impl.create(cameraDevice, handler);
        }
        else {
            this.mImpl = (CameraDeviceCompatImpl)CameraDeviceCompatBaseImpl.create(cameraDevice, handler);
        }
    }
    
    public static CameraDeviceCompat toCameraDeviceCompat(final CameraDevice cameraDevice) {
        return toCameraDeviceCompat(cameraDevice, MainThreadAsyncHandler.getInstance());
    }
    
    public static CameraDeviceCompat toCameraDeviceCompat(final CameraDevice cameraDevice, final Handler handler) {
        return new CameraDeviceCompat(cameraDevice, handler);
    }
    
    public void createCaptureSession(final SessionConfigurationCompat sessionConfigurationCompat) throws CameraAccessExceptionCompat {
        this.mImpl.createCaptureSession(sessionConfigurationCompat);
    }
    
    public CameraDevice toCameraDevice() {
        return this.mImpl.unwrap();
    }
    
    interface CameraDeviceCompatImpl
    {
        void createCaptureSession(final SessionConfigurationCompat p0) throws CameraAccessExceptionCompat;
        
        CameraDevice unwrap();
    }
    
    static final class StateCallbackExecutorWrapper extends CameraDevice$StateCallback
    {
        private final Executor mExecutor;
        final CameraDevice$StateCallback mWrappedCallback;
        
        StateCallbackExecutorWrapper(final Executor mExecutor, final CameraDevice$StateCallback mWrappedCallback) {
            this.mExecutor = mExecutor;
            this.mWrappedCallback = mWrappedCallback;
        }
        
        public void onClosed(final CameraDevice cameraDevice) {
            this.mExecutor.execute(new CameraDeviceCompat$StateCallbackExecutorWrapper$$ExternalSyntheticLambda0(this, cameraDevice));
        }
        
        public void onDisconnected(final CameraDevice cameraDevice) {
            this.mExecutor.execute(new CameraDeviceCompat$StateCallbackExecutorWrapper$$ExternalSyntheticLambda1(this, cameraDevice));
        }
        
        public void onError(final CameraDevice cameraDevice, final int n) {
            this.mExecutor.execute(new CameraDeviceCompat$StateCallbackExecutorWrapper$$ExternalSyntheticLambda3(this, cameraDevice, n));
        }
        
        public void onOpened(final CameraDevice cameraDevice) {
            this.mExecutor.execute(new CameraDeviceCompat$StateCallbackExecutorWrapper$$ExternalSyntheticLambda2(this, cameraDevice));
        }
    }
}

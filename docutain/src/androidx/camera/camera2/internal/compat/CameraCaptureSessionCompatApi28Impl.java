// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.internal.compat;

import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCaptureSession$CaptureCallback;
import java.util.concurrent.Executor;
import android.hardware.camera2.CaptureRequest;
import java.util.List;
import android.hardware.camera2.CameraCaptureSession;

class CameraCaptureSessionCompatApi28Impl extends CameraCaptureSessionCompatBaseImpl
{
    CameraCaptureSessionCompatApi28Impl(final CameraCaptureSession cameraCaptureSession) {
        super(cameraCaptureSession, null);
    }
    
    @Override
    public int captureBurstRequests(final List<CaptureRequest> list, final Executor executor, final CameraCaptureSession$CaptureCallback cameraCaptureSession$CaptureCallback) throws CameraAccessException {
        return this.mCameraCaptureSession.captureBurstRequests((List)list, executor, cameraCaptureSession$CaptureCallback);
    }
    
    @Override
    public int captureSingleRequest(final CaptureRequest captureRequest, final Executor executor, final CameraCaptureSession$CaptureCallback cameraCaptureSession$CaptureCallback) throws CameraAccessException {
        return this.mCameraCaptureSession.captureSingleRequest(captureRequest, executor, cameraCaptureSession$CaptureCallback);
    }
    
    @Override
    public int setRepeatingBurstRequests(final List<CaptureRequest> list, final Executor executor, final CameraCaptureSession$CaptureCallback cameraCaptureSession$CaptureCallback) throws CameraAccessException {
        return this.mCameraCaptureSession.setRepeatingBurstRequests((List)list, executor, cameraCaptureSession$CaptureCallback);
    }
    
    @Override
    public int setSingleRepeatingRequest(final CaptureRequest captureRequest, final Executor executor, final CameraCaptureSession$CaptureCallback cameraCaptureSession$CaptureCallback) throws CameraAccessException {
        return this.mCameraCaptureSession.setSingleRepeatingRequest(captureRequest, executor, cameraCaptureSession$CaptureCallback);
    }
}

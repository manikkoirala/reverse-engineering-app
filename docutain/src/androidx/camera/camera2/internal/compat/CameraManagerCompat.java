// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.internal.compat;

import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.CameraManager$AvailabilityCallback;
import android.hardware.camera2.CameraDevice$StateCallback;
import java.util.concurrent.Executor;
import android.os.Handler;
import androidx.camera.core.impl.utils.MainThreadAsyncHandler;
import android.content.Context;
import android.util.ArrayMap;
import java.util.Map;

public final class CameraManagerCompat
{
    private final Map<String, CameraCharacteristicsCompat> mCameraCharacteristicsMap;
    private final CameraManagerCompatImpl mImpl;
    
    private CameraManagerCompat(final CameraManagerCompatImpl mImpl) {
        this.mCameraCharacteristicsMap = (Map<String, CameraCharacteristicsCompat>)new ArrayMap(4);
        this.mImpl = mImpl;
    }
    
    public static CameraManagerCompat from(final Context context) {
        return from(context, MainThreadAsyncHandler.getInstance());
    }
    
    public static CameraManagerCompat from(final Context context, final Handler handler) {
        return new CameraManagerCompat(CameraManagerCompat$CameraManagerCompatImpl$_CC.from(context, handler));
    }
    
    public static CameraManagerCompat from(final CameraManagerCompatImpl cameraManagerCompatImpl) {
        return new CameraManagerCompat(cameraManagerCompatImpl);
    }
    
    public CameraCharacteristicsCompat getCameraCharacteristicsCompat(final String s) throws CameraAccessExceptionCompat {
        synchronized (this.mCameraCharacteristicsMap) {
            CameraCharacteristicsCompat cameraCharacteristicsCompat;
            if ((cameraCharacteristicsCompat = this.mCameraCharacteristicsMap.get(s)) == null) {
                try {
                    cameraCharacteristicsCompat = CameraCharacteristicsCompat.toCameraCharacteristicsCompat(this.mImpl.getCameraCharacteristics(s));
                    this.mCameraCharacteristicsMap.put(s, cameraCharacteristicsCompat);
                }
                catch (final AssertionError assertionError) {
                    throw new CameraAccessExceptionCompat(10002, assertionError.getMessage(), assertionError);
                }
            }
            return cameraCharacteristicsCompat;
        }
    }
    
    public String[] getCameraIdList() throws CameraAccessExceptionCompat {
        return this.mImpl.getCameraIdList();
    }
    
    public void openCamera(final String s, final Executor executor, final CameraDevice$StateCallback cameraDevice$StateCallback) throws CameraAccessExceptionCompat {
        this.mImpl.openCamera(s, executor, cameraDevice$StateCallback);
    }
    
    public void registerAvailabilityCallback(final Executor executor, final CameraManager$AvailabilityCallback cameraManager$AvailabilityCallback) {
        this.mImpl.registerAvailabilityCallback(executor, cameraManager$AvailabilityCallback);
    }
    
    public void unregisterAvailabilityCallback(final CameraManager$AvailabilityCallback cameraManager$AvailabilityCallback) {
        this.mImpl.unregisterAvailabilityCallback(cameraManager$AvailabilityCallback);
    }
    
    public CameraManager unwrap() {
        return this.mImpl.getCameraManager();
    }
    
    static final class AvailabilityCallbackExecutorWrapper extends CameraManager$AvailabilityCallback
    {
        private boolean mDisabled;
        private final Executor mExecutor;
        private final Object mLock;
        final CameraManager$AvailabilityCallback mWrappedCallback;
        
        AvailabilityCallbackExecutorWrapper(final Executor mExecutor, final CameraManager$AvailabilityCallback mWrappedCallback) {
            this.mLock = new Object();
            this.mDisabled = false;
            this.mExecutor = mExecutor;
            this.mWrappedCallback = mWrappedCallback;
        }
        
        public void onCameraAccessPrioritiesChanged() {
            synchronized (this.mLock) {
                if (!this.mDisabled) {
                    this.mExecutor.execute(new CameraManagerCompat$AvailabilityCallbackExecutorWrapper$$ExternalSyntheticLambda0(this));
                }
            }
        }
        
        public void onCameraAvailable(final String s) {
            synchronized (this.mLock) {
                if (!this.mDisabled) {
                    this.mExecutor.execute(new CameraManagerCompat$AvailabilityCallbackExecutorWrapper$$ExternalSyntheticLambda2(this, s));
                }
            }
        }
        
        public void onCameraUnavailable(final String s) {
            synchronized (this.mLock) {
                if (!this.mDisabled) {
                    this.mExecutor.execute(new CameraManagerCompat$AvailabilityCallbackExecutorWrapper$$ExternalSyntheticLambda1(this, s));
                }
            }
        }
        
        void setDisabled() {
            synchronized (this.mLock) {
                this.mDisabled = true;
            }
        }
    }
    
    public interface CameraManagerCompatImpl
    {
        CameraCharacteristics getCameraCharacteristics(final String p0) throws CameraAccessExceptionCompat;
        
        String[] getCameraIdList() throws CameraAccessExceptionCompat;
        
        CameraManager getCameraManager();
        
        void openCamera(final String p0, final Executor p1, final CameraDevice$StateCallback p2) throws CameraAccessExceptionCompat;
        
        void registerAvailabilityCallback(final Executor p0, final CameraManager$AvailabilityCallback p1);
        
        void unregisterAvailabilityCallback(final CameraManager$AvailabilityCallback p0);
    }
}

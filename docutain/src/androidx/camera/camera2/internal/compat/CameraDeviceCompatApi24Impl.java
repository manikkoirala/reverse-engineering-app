// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.internal.compat;

import androidx.camera.camera2.internal.compat.params.InputConfigurationCompat;
import androidx.camera.camera2.internal.compat.params.OutputConfigurationCompat;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCaptureSession$StateCallback;
import java.util.List;
import android.hardware.camera2.params.InputConfiguration;
import androidx.core.util.Preconditions;
import androidx.camera.camera2.internal.compat.params.SessionConfigurationCompat;
import android.os.Handler;
import android.hardware.camera2.CameraDevice;

class CameraDeviceCompatApi24Impl extends CameraDeviceCompatApi23Impl
{
    CameraDeviceCompatApi24Impl(final CameraDevice cameraDevice, final Object o) {
        super(cameraDevice, o);
    }
    
    static CameraDeviceCompatApi24Impl create(final CameraDevice cameraDevice, final Handler handler) {
        return new CameraDeviceCompatApi24Impl(cameraDevice, new CameraDeviceCompatParamsApi21(handler));
    }
    
    @Override
    public void createCaptureSession(final SessionConfigurationCompat sessionConfigurationCompat) throws CameraAccessExceptionCompat {
        CameraDeviceCompatBaseImpl.checkPreconditions(this.mCameraDevice, sessionConfigurationCompat);
        final CameraCaptureSessionCompat.StateCallbackExecutorWrapper stateCallbackExecutorWrapper = new CameraCaptureSessionCompat.StateCallbackExecutorWrapper(sessionConfigurationCompat.getExecutor(), sessionConfigurationCompat.getStateCallback());
        final List<OutputConfigurationCompat> outputConfigurations = sessionConfigurationCompat.getOutputConfigurations();
        final Handler mCompatHandler = Preconditions.checkNotNull(this.mImplParams).mCompatHandler;
        final InputConfigurationCompat inputConfiguration = sessionConfigurationCompat.getInputConfiguration();
        Label_0090: {
            if (inputConfiguration == null) {
                break Label_0090;
            }
            try {
                final InputConfiguration inputConfiguration2 = (InputConfiguration)inputConfiguration.unwrap();
                Preconditions.checkNotNull(inputConfiguration2);
                this.mCameraDevice.createReprocessableCaptureSessionByConfigurations(inputConfiguration2, (List)SessionConfigurationCompat.transformFromCompat(outputConfigurations), (CameraCaptureSession$StateCallback)stateCallbackExecutorWrapper, mCompatHandler);
                return;
                while (true) {
                    this.mCameraDevice.createConstrainedHighSpeedCaptureSession((List)CameraDeviceCompatBaseImpl.unpackSurfaces(outputConfigurations), (CameraCaptureSession$StateCallback)stateCallbackExecutorWrapper, mCompatHandler);
                    return;
                    iftrue(Label_0115:)(sessionConfigurationCompat.getSessionType() != 1);
                    continue;
                }
                Label_0115: {
                    this.mCameraDevice.createCaptureSessionByOutputConfigurations((List)SessionConfigurationCompat.transformFromCompat(outputConfigurations), (CameraCaptureSession$StateCallback)stateCallbackExecutorWrapper, mCompatHandler);
                }
            }
            catch (final CameraAccessException ex) {
                throw CameraAccessExceptionCompat.toCameraAccessExceptionCompat(ex);
            }
        }
    }
}

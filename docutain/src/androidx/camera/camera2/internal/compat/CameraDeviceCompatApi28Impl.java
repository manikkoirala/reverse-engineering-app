// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.internal.compat;

import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.params.SessionConfiguration;
import androidx.camera.camera2.internal.compat.params.SessionConfigurationCompat;
import androidx.core.util.Preconditions;
import android.hardware.camera2.CameraDevice;

class CameraDeviceCompatApi28Impl extends CameraDeviceCompatApi24Impl
{
    CameraDeviceCompatApi28Impl(final CameraDevice cameraDevice) {
        super(Preconditions.checkNotNull(cameraDevice), null);
    }
    
    @Override
    public void createCaptureSession(final SessionConfigurationCompat sessionConfigurationCompat) throws CameraAccessExceptionCompat {
        final SessionConfiguration sessionConfiguration = (SessionConfiguration)sessionConfigurationCompat.unwrap();
        Preconditions.checkNotNull(sessionConfiguration);
        try {
            this.mCameraDevice.createCaptureSession(sessionConfiguration);
        }
        catch (final CameraAccessException ex) {
            throw CameraAccessExceptionCompat.toCameraAccessExceptionCompat(ex);
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.internal.compat;

import java.util.Collections;
import java.util.Set;
import android.hardware.camera2.CameraCharacteristics$Key;
import android.hardware.camera2.CameraCharacteristics;

class CameraCharacteristicsBaseImpl implements CameraCharacteristicsCompatImpl
{
    protected final CameraCharacteristics mCameraCharacteristics;
    
    CameraCharacteristicsBaseImpl(final CameraCharacteristics mCameraCharacteristics) {
        this.mCameraCharacteristics = mCameraCharacteristics;
    }
    
    @Override
    public <T> T get(final CameraCharacteristics$Key<T> cameraCharacteristics$Key) {
        return (T)this.mCameraCharacteristics.get((CameraCharacteristics$Key)cameraCharacteristics$Key);
    }
    
    @Override
    public Set<String> getPhysicalCameraIds() {
        return Collections.emptySet();
    }
    
    @Override
    public CameraCharacteristics unwrap() {
        return this.mCameraCharacteristics;
    }
}

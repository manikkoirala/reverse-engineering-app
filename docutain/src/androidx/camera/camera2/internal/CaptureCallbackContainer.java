// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.internal;

import android.hardware.camera2.CameraCaptureSession$CaptureCallback;
import androidx.camera.core.impl.CameraCaptureCallback;

final class CaptureCallbackContainer extends CameraCaptureCallback
{
    private final CameraCaptureSession$CaptureCallback mCaptureCallback;
    
    private CaptureCallbackContainer(final CameraCaptureSession$CaptureCallback mCaptureCallback) {
        if (mCaptureCallback != null) {
            this.mCaptureCallback = mCaptureCallback;
            return;
        }
        throw new NullPointerException("captureCallback is null");
    }
    
    static CaptureCallbackContainer create(final CameraCaptureSession$CaptureCallback cameraCaptureSession$CaptureCallback) {
        return new CaptureCallbackContainer(cameraCaptureSession$CaptureCallback);
    }
    
    CameraCaptureSession$CaptureCallback getCaptureCallback() {
        return this.mCaptureCallback;
    }
}

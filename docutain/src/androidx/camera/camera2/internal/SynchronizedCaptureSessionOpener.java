// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.internal;

import androidx.camera.camera2.internal.compat.workaround.ForceCloseCaptureSession;
import androidx.camera.camera2.internal.compat.workaround.WaitForRepeatingRequestStart;
import androidx.camera.camera2.internal.compat.workaround.ForceCloseDeferrableSurface;
import java.util.concurrent.ScheduledExecutorService;
import android.os.Handler;
import androidx.camera.core.impl.Quirks;
import android.view.Surface;
import com.google.common.util.concurrent.ListenableFuture;
import androidx.camera.core.impl.DeferrableSurface;
import android.hardware.camera2.CameraDevice;
import java.util.concurrent.Executor;
import androidx.camera.camera2.internal.compat.params.SessionConfigurationCompat;
import androidx.camera.camera2.internal.compat.params.OutputConfigurationCompat;
import java.util.List;

final class SynchronizedCaptureSessionOpener
{
    private final OpenerImpl mImpl;
    
    SynchronizedCaptureSessionOpener(final OpenerImpl mImpl) {
        this.mImpl = mImpl;
    }
    
    SessionConfigurationCompat createSessionConfigurationCompat(final int n, final List<OutputConfigurationCompat> list, final SynchronizedCaptureSession.StateCallback stateCallback) {
        return this.mImpl.createSessionConfigurationCompat(n, list, stateCallback);
    }
    
    public Executor getExecutor() {
        return this.mImpl.getExecutor();
    }
    
    ListenableFuture<Void> openCaptureSession(final CameraDevice cameraDevice, final SessionConfigurationCompat sessionConfigurationCompat, final List<DeferrableSurface> list) {
        return this.mImpl.openCaptureSession(cameraDevice, sessionConfigurationCompat, list);
    }
    
    ListenableFuture<List<Surface>> startWithDeferrableSurface(final List<DeferrableSurface> list, final long n) {
        return this.mImpl.startWithDeferrableSurface(list, n);
    }
    
    boolean stop() {
        return this.mImpl.stop();
    }
    
    static class Builder
    {
        private final Quirks mCameraQuirks;
        private final CaptureSessionRepository mCaptureSessionRepository;
        private final Handler mCompatHandler;
        private final Quirks mDeviceQuirks;
        private final Executor mExecutor;
        private final boolean mQuirkExist;
        private final ScheduledExecutorService mScheduledExecutorService;
        
        Builder(final Executor mExecutor, final ScheduledExecutorService mScheduledExecutorService, final Handler mCompatHandler, final CaptureSessionRepository mCaptureSessionRepository, final Quirks mCameraQuirks, final Quirks mDeviceQuirks) {
            this.mExecutor = mExecutor;
            this.mScheduledExecutorService = mScheduledExecutorService;
            this.mCompatHandler = mCompatHandler;
            this.mCaptureSessionRepository = mCaptureSessionRepository;
            this.mCameraQuirks = mCameraQuirks;
            this.mDeviceQuirks = mDeviceQuirks;
            this.mQuirkExist = (new ForceCloseDeferrableSurface(mCameraQuirks, mDeviceQuirks).shouldForceClose() || new WaitForRepeatingRequestStart(mCameraQuirks).shouldWaitRepeatingSubmit() || new ForceCloseCaptureSession(mDeviceQuirks).shouldForceClose());
        }
        
        SynchronizedCaptureSessionOpener build() {
            SynchronizedCaptureSessionBaseImpl synchronizedCaptureSessionBaseImpl;
            if (this.mQuirkExist) {
                synchronizedCaptureSessionBaseImpl = new SynchronizedCaptureSessionImpl(this.mCameraQuirks, this.mDeviceQuirks, this.mCaptureSessionRepository, this.mExecutor, this.mScheduledExecutorService, this.mCompatHandler);
            }
            else {
                synchronizedCaptureSessionBaseImpl = new SynchronizedCaptureSessionBaseImpl(this.mCaptureSessionRepository, this.mExecutor, this.mScheduledExecutorService, this.mCompatHandler);
            }
            return new SynchronizedCaptureSessionOpener((OpenerImpl)synchronizedCaptureSessionBaseImpl);
        }
    }
    
    interface OpenerImpl
    {
        SessionConfigurationCompat createSessionConfigurationCompat(final int p0, final List<OutputConfigurationCompat> p1, final SynchronizedCaptureSession.StateCallback p2);
        
        Executor getExecutor();
        
        ListenableFuture<Void> openCaptureSession(final CameraDevice p0, final SessionConfigurationCompat p1, final List<DeferrableSurface> p2);
        
        ListenableFuture<List<Surface>> startWithDeferrableSurface(final List<DeferrableSurface> p0, final long p1);
        
        boolean stop();
    }
}

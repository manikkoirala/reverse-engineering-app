// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.internal;

import androidx.camera.core.impl.utils.futures.AsyncFunction;
import androidx.camera.core.impl.utils.futures.FutureChain;
import java.util.Collection;
import androidx.camera.core.impl.utils.futures.FutureCallback;
import java.util.concurrent.CancellationException;
import androidx.camera.core.impl.utils.executor.CameraXExecutors;
import androidx.camera.core.Logger;
import androidx.camera.camera2.internal.compat.CameraDeviceCompat;
import java.util.Objects;
import androidx.camera.core.impl.DeferrableSurfaces;
import androidx.camera.core.impl.utils.futures.Futures;
import android.os.Build$VERSION;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CameraCaptureSession$StateCallback;
import androidx.camera.camera2.internal.compat.params.SessionConfigurationCompat;
import androidx.camera.camera2.internal.compat.params.OutputConfigurationCompat;
import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CameraCaptureSession$CaptureCallback;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.CameraAccessException;
import androidx.core.util.Preconditions;
import android.view.Surface;
import java.util.concurrent.ScheduledExecutorService;
import com.google.common.util.concurrent.ListenableFuture;
import androidx.concurrent.futures.CallbackToFutureAdapter;
import androidx.camera.core.impl.DeferrableSurface;
import java.util.List;
import java.util.concurrent.Executor;
import android.os.Handler;
import androidx.camera.camera2.internal.compat.CameraCaptureSessionCompat;

class SynchronizedCaptureSessionBaseImpl extends StateCallback implements SynchronizedCaptureSession, OpenerImpl
{
    private static final String TAG = "SyncCaptureSessionBase";
    CameraCaptureSessionCompat mCameraCaptureSessionCompat;
    final CaptureSessionRepository mCaptureSessionRepository;
    StateCallback mCaptureSessionStateCallback;
    private boolean mClosed;
    final Handler mCompatHandler;
    final Executor mExecutor;
    private List<DeferrableSurface> mHeldDeferrableSurfaces;
    final Object mLock;
    CallbackToFutureAdapter.Completer<Void> mOpenCaptureSessionCompleter;
    ListenableFuture<Void> mOpenCaptureSessionFuture;
    private boolean mOpenerDisabled;
    private final ScheduledExecutorService mScheduledExecutorService;
    private boolean mSessionFinished;
    private ListenableFuture<List<Surface>> mStartingSurface;
    
    SynchronizedCaptureSessionBaseImpl(final CaptureSessionRepository mCaptureSessionRepository, final Executor mExecutor, final ScheduledExecutorService mScheduledExecutorService, final Handler mCompatHandler) {
        this.mLock = new Object();
        this.mHeldDeferrableSurfaces = null;
        this.mClosed = false;
        this.mOpenerDisabled = false;
        this.mSessionFinished = false;
        this.mCaptureSessionRepository = mCaptureSessionRepository;
        this.mCompatHandler = mCompatHandler;
        this.mExecutor = mExecutor;
        this.mScheduledExecutorService = mScheduledExecutorService;
    }
    
    @Override
    public void abortCaptures() throws CameraAccessException {
        Preconditions.checkNotNull(this.mCameraCaptureSessionCompat, "Need to call openCaptureSession before using this API.");
        this.mCameraCaptureSessionCompat.toCameraCaptureSession().abortCaptures();
    }
    
    @Override
    public int captureBurstRequests(final List<CaptureRequest> list, final CameraCaptureSession$CaptureCallback cameraCaptureSession$CaptureCallback) throws CameraAccessException {
        Preconditions.checkNotNull(this.mCameraCaptureSessionCompat, "Need to call openCaptureSession before using this API.");
        return this.mCameraCaptureSessionCompat.captureBurstRequests(list, this.getExecutor(), cameraCaptureSession$CaptureCallback);
    }
    
    @Override
    public int captureBurstRequests(final List<CaptureRequest> list, final Executor executor, final CameraCaptureSession$CaptureCallback cameraCaptureSession$CaptureCallback) throws CameraAccessException {
        Preconditions.checkNotNull(this.mCameraCaptureSessionCompat, "Need to call openCaptureSession before using this API.");
        return this.mCameraCaptureSessionCompat.captureBurstRequests(list, executor, cameraCaptureSession$CaptureCallback);
    }
    
    @Override
    public int captureSingleRequest(final CaptureRequest captureRequest, final CameraCaptureSession$CaptureCallback cameraCaptureSession$CaptureCallback) throws CameraAccessException {
        Preconditions.checkNotNull(this.mCameraCaptureSessionCompat, "Need to call openCaptureSession before using this API.");
        return this.mCameraCaptureSessionCompat.captureSingleRequest(captureRequest, this.getExecutor(), cameraCaptureSession$CaptureCallback);
    }
    
    @Override
    public int captureSingleRequest(final CaptureRequest captureRequest, final Executor executor, final CameraCaptureSession$CaptureCallback cameraCaptureSession$CaptureCallback) throws CameraAccessException {
        Preconditions.checkNotNull(this.mCameraCaptureSessionCompat, "Need to call openCaptureSession before using this API.");
        return this.mCameraCaptureSessionCompat.captureSingleRequest(captureRequest, executor, cameraCaptureSession$CaptureCallback);
    }
    
    @Override
    public void close() {
        Preconditions.checkNotNull(this.mCameraCaptureSessionCompat, "Need to call openCaptureSession before using this API.");
        this.mCaptureSessionRepository.onCaptureSessionClosing(this);
        this.mCameraCaptureSessionCompat.toCameraCaptureSession().close();
        this.getExecutor().execute(new SynchronizedCaptureSessionBaseImpl$$ExternalSyntheticLambda4(this));
    }
    
    void createCaptureSessionCompat(final CameraCaptureSession cameraCaptureSession) {
        if (this.mCameraCaptureSessionCompat == null) {
            this.mCameraCaptureSessionCompat = CameraCaptureSessionCompat.toCameraCaptureSessionCompat(cameraCaptureSession, this.mCompatHandler);
        }
    }
    
    @Override
    public SessionConfigurationCompat createSessionConfigurationCompat(final int n, final List<OutputConfigurationCompat> list, final StateCallback mCaptureSessionStateCallback) {
        this.mCaptureSessionStateCallback = mCaptureSessionStateCallback;
        return new SessionConfigurationCompat(n, list, this.getExecutor(), new CameraCaptureSession$StateCallback(this) {
            final SynchronizedCaptureSessionBaseImpl this$0;
            
            public void onActive(final CameraCaptureSession cameraCaptureSession) {
                this.this$0.createCaptureSessionCompat(cameraCaptureSession);
                final SynchronizedCaptureSessionBaseImpl this$0 = this.this$0;
                this$0.onActive(this$0);
            }
            
            public void onCaptureQueueEmpty(final CameraCaptureSession cameraCaptureSession) {
                this.this$0.createCaptureSessionCompat(cameraCaptureSession);
                final SynchronizedCaptureSessionBaseImpl this$0 = this.this$0;
                this$0.onCaptureQueueEmpty(this$0);
            }
            
            public void onClosed(final CameraCaptureSession cameraCaptureSession) {
                this.this$0.createCaptureSessionCompat(cameraCaptureSession);
                final SynchronizedCaptureSessionBaseImpl this$0 = this.this$0;
                this$0.onClosed(this$0);
            }
            
            public void onConfigureFailed(final CameraCaptureSession cameraCaptureSession) {
                try {
                    this.this$0.createCaptureSessionCompat(cameraCaptureSession);
                    final SynchronizedCaptureSessionBaseImpl this$0 = this.this$0;
                    this$0.onConfigureFailed(this$0);
                    synchronized (this.this$0.mLock) {
                        Preconditions.checkNotNull(this.this$0.mOpenCaptureSessionCompleter, "OpenCaptureSession completer should not null");
                        final CallbackToFutureAdapter.Completer<Void> mOpenCaptureSessionCompleter = this.this$0.mOpenCaptureSessionCompleter;
                        this.this$0.mOpenCaptureSessionCompleter = null;
                        monitorexit(this.this$0.mLock);
                        mOpenCaptureSessionCompleter.setException(new IllegalStateException("onConfigureFailed"));
                    }
                }
                finally {
                    synchronized (this.this$0.mLock) {
                        Preconditions.checkNotNull(this.this$0.mOpenCaptureSessionCompleter, "OpenCaptureSession completer should not null");
                        final CallbackToFutureAdapter.Completer<Void> mOpenCaptureSessionCompleter2 = this.this$0.mOpenCaptureSessionCompleter;
                        this.this$0.mOpenCaptureSessionCompleter = null;
                        monitorexit(this.this$0.mLock);
                        mOpenCaptureSessionCompleter2.setException(new IllegalStateException("onConfigureFailed"));
                    }
                }
            }
            
            public void onConfigured(final CameraCaptureSession cameraCaptureSession) {
                try {
                    this.this$0.createCaptureSessionCompat(cameraCaptureSession);
                    final SynchronizedCaptureSessionBaseImpl this$0 = this.this$0;
                    this$0.onConfigured(this$0);
                    synchronized (this.this$0.mLock) {
                        Preconditions.checkNotNull(this.this$0.mOpenCaptureSessionCompleter, "OpenCaptureSession completer should not null");
                        final CallbackToFutureAdapter.Completer<Void> mOpenCaptureSessionCompleter = this.this$0.mOpenCaptureSessionCompleter;
                        this.this$0.mOpenCaptureSessionCompleter = null;
                        monitorexit(this.this$0.mLock);
                        mOpenCaptureSessionCompleter.set(null);
                    }
                }
                finally {
                    synchronized (this.this$0.mLock) {
                        Preconditions.checkNotNull(this.this$0.mOpenCaptureSessionCompleter, "OpenCaptureSession completer should not null");
                        final CallbackToFutureAdapter.Completer<Void> mOpenCaptureSessionCompleter2 = this.this$0.mOpenCaptureSessionCompleter;
                        this.this$0.mOpenCaptureSessionCompleter = null;
                        monitorexit(this.this$0.mLock);
                        mOpenCaptureSessionCompleter2.set(null);
                    }
                }
            }
            
            public void onReady(final CameraCaptureSession cameraCaptureSession) {
                this.this$0.createCaptureSessionCompat(cameraCaptureSession);
                final SynchronizedCaptureSessionBaseImpl this$0 = this.this$0;
                this$0.onReady(this$0);
            }
            
            public void onSurfacePrepared(final CameraCaptureSession cameraCaptureSession, final Surface surface) {
                this.this$0.createCaptureSessionCompat(cameraCaptureSession);
                final SynchronizedCaptureSessionBaseImpl this$0 = this.this$0;
                this$0.onSurfacePrepared(this$0, surface);
            }
        });
    }
    
    @Override
    public void finishClose() {
        this.releaseDeferrableSurfaces();
    }
    
    @Override
    public CameraDevice getDevice() {
        Preconditions.checkNotNull(this.mCameraCaptureSessionCompat);
        return this.mCameraCaptureSessionCompat.toCameraCaptureSession().getDevice();
    }
    
    @Override
    public Executor getExecutor() {
        return this.mExecutor;
    }
    
    @Override
    public Surface getInputSurface() {
        Preconditions.checkNotNull(this.mCameraCaptureSessionCompat);
        if (Build$VERSION.SDK_INT >= 23) {
            return Api23Impl.getInputSurface(this.mCameraCaptureSessionCompat.toCameraCaptureSession());
        }
        return null;
    }
    
    @Override
    public ListenableFuture<Void> getOpeningBlocker() {
        return Futures.immediateFuture((Void)null);
    }
    
    @Override
    public StateCallback getStateCallback() {
        return this;
    }
    
    void holdDeferrableSurfaces(final List<DeferrableSurface> mHeldDeferrableSurfaces) throws DeferrableSurface.SurfaceClosedException {
        synchronized (this.mLock) {
            this.releaseDeferrableSurfaces();
            DeferrableSurfaces.incrementAll(mHeldDeferrableSurfaces);
            this.mHeldDeferrableSurfaces = mHeldDeferrableSurfaces;
        }
    }
    
    boolean isCameraCaptureSessionOpen() {
        synchronized (this.mLock) {
            return this.mOpenCaptureSessionFuture != null;
        }
    }
    
    public void onActive(final SynchronizedCaptureSession synchronizedCaptureSession) {
        Objects.requireNonNull(this.mCaptureSessionStateCallback);
        this.mCaptureSessionStateCallback.onActive(synchronizedCaptureSession);
    }
    
    public void onCaptureQueueEmpty(final SynchronizedCaptureSession synchronizedCaptureSession) {
        Objects.requireNonNull(this.mCaptureSessionStateCallback);
        this.mCaptureSessionStateCallback.onCaptureQueueEmpty(synchronizedCaptureSession);
    }
    
    @Override
    public void onClosed(final SynchronizedCaptureSession synchronizedCaptureSession) {
        synchronized (this.mLock) {
            ListenableFuture mOpenCaptureSessionFuture;
            if (!this.mClosed) {
                this.mClosed = true;
                Preconditions.checkNotNull(this.mOpenCaptureSessionFuture, "Need to call openCaptureSession before using this API.");
                mOpenCaptureSessionFuture = this.mOpenCaptureSessionFuture;
            }
            else {
                mOpenCaptureSessionFuture = null;
            }
            monitorexit(this.mLock);
            this.finishClose();
            if (mOpenCaptureSessionFuture != null) {
                mOpenCaptureSessionFuture.addListener((Runnable)new SynchronizedCaptureSessionBaseImpl$$ExternalSyntheticLambda3(this, synchronizedCaptureSession), CameraXExecutors.directExecutor());
            }
        }
    }
    
    @Override
    public void onConfigureFailed(final SynchronizedCaptureSession synchronizedCaptureSession) {
        Objects.requireNonNull(this.mCaptureSessionStateCallback);
        this.finishClose();
        this.mCaptureSessionRepository.onCaptureSessionConfigureFail(this);
        this.mCaptureSessionStateCallback.onConfigureFailed(synchronizedCaptureSession);
    }
    
    public void onConfigured(final SynchronizedCaptureSession synchronizedCaptureSession) {
        Objects.requireNonNull(this.mCaptureSessionStateCallback);
        this.mCaptureSessionRepository.onCaptureSessionCreated(this);
        this.mCaptureSessionStateCallback.onConfigured(synchronizedCaptureSession);
    }
    
    public void onReady(final SynchronizedCaptureSession synchronizedCaptureSession) {
        Objects.requireNonNull(this.mCaptureSessionStateCallback);
        this.mCaptureSessionStateCallback.onReady(synchronizedCaptureSession);
    }
    
    @Override
    void onSessionFinished(final SynchronizedCaptureSession synchronizedCaptureSession) {
        synchronized (this.mLock) {
            ListenableFuture mOpenCaptureSessionFuture;
            if (!this.mSessionFinished) {
                this.mSessionFinished = true;
                Preconditions.checkNotNull(this.mOpenCaptureSessionFuture, "Need to call openCaptureSession before using this API.");
                mOpenCaptureSessionFuture = this.mOpenCaptureSessionFuture;
            }
            else {
                mOpenCaptureSessionFuture = null;
            }
            monitorexit(this.mLock);
            if (mOpenCaptureSessionFuture != null) {
                mOpenCaptureSessionFuture.addListener((Runnable)new SynchronizedCaptureSessionBaseImpl$$ExternalSyntheticLambda0(this, synchronizedCaptureSession), CameraXExecutors.directExecutor());
            }
        }
    }
    
    public void onSurfacePrepared(final SynchronizedCaptureSession synchronizedCaptureSession, final Surface surface) {
        Objects.requireNonNull(this.mCaptureSessionStateCallback);
        this.mCaptureSessionStateCallback.onSurfacePrepared(synchronizedCaptureSession, surface);
    }
    
    @Override
    public ListenableFuture<Void> openCaptureSession(final CameraDevice cameraDevice, final SessionConfigurationCompat sessionConfigurationCompat, final List<DeferrableSurface> list) {
        synchronized (this.mLock) {
            if (this.mOpenerDisabled) {
                return Futures.immediateFailedFuture(new CancellationException("Opener is disabled"));
            }
            this.mCaptureSessionRepository.onCreateCaptureSession(this);
            Futures.addCallback(this.mOpenCaptureSessionFuture = CallbackToFutureAdapter.getFuture((CallbackToFutureAdapter.Resolver<Void>)new SynchronizedCaptureSessionBaseImpl$$ExternalSyntheticLambda2(this, list, CameraDeviceCompat.toCameraDeviceCompat(cameraDevice, this.mCompatHandler), sessionConfigurationCompat)), new FutureCallback<Void>(this) {
                final SynchronizedCaptureSessionBaseImpl this$0;
                
                @Override
                public void onFailure(final Throwable t) {
                    this.this$0.finishClose();
                    this.this$0.mCaptureSessionRepository.onCaptureSessionConfigureFail(this.this$0);
                }
                
                @Override
                public void onSuccess(final Void void1) {
                }
            }, CameraXExecutors.directExecutor());
            return Futures.nonCancellationPropagating(this.mOpenCaptureSessionFuture);
        }
    }
    
    void releaseDeferrableSurfaces() {
        synchronized (this.mLock) {
            final List<DeferrableSurface> mHeldDeferrableSurfaces = this.mHeldDeferrableSurfaces;
            if (mHeldDeferrableSurfaces != null) {
                DeferrableSurfaces.decrementAll(mHeldDeferrableSurfaces);
                this.mHeldDeferrableSurfaces = null;
            }
        }
    }
    
    @Override
    public int setRepeatingBurstRequests(final List<CaptureRequest> list, final CameraCaptureSession$CaptureCallback cameraCaptureSession$CaptureCallback) throws CameraAccessException {
        Preconditions.checkNotNull(this.mCameraCaptureSessionCompat, "Need to call openCaptureSession before using this API.");
        return this.mCameraCaptureSessionCompat.setRepeatingBurstRequests(list, this.getExecutor(), cameraCaptureSession$CaptureCallback);
    }
    
    @Override
    public int setRepeatingBurstRequests(final List<CaptureRequest> list, final Executor executor, final CameraCaptureSession$CaptureCallback cameraCaptureSession$CaptureCallback) throws CameraAccessException {
        Preconditions.checkNotNull(this.mCameraCaptureSessionCompat, "Need to call openCaptureSession before using this API.");
        return this.mCameraCaptureSessionCompat.setRepeatingBurstRequests(list, executor, cameraCaptureSession$CaptureCallback);
    }
    
    @Override
    public int setSingleRepeatingRequest(final CaptureRequest captureRequest, final CameraCaptureSession$CaptureCallback cameraCaptureSession$CaptureCallback) throws CameraAccessException {
        Preconditions.checkNotNull(this.mCameraCaptureSessionCompat, "Need to call openCaptureSession before using this API.");
        return this.mCameraCaptureSessionCompat.setSingleRepeatingRequest(captureRequest, this.getExecutor(), cameraCaptureSession$CaptureCallback);
    }
    
    @Override
    public int setSingleRepeatingRequest(final CaptureRequest captureRequest, final Executor executor, final CameraCaptureSession$CaptureCallback cameraCaptureSession$CaptureCallback) throws CameraAccessException {
        Preconditions.checkNotNull(this.mCameraCaptureSessionCompat, "Need to call openCaptureSession before using this API.");
        return this.mCameraCaptureSessionCompat.setSingleRepeatingRequest(captureRequest, executor, cameraCaptureSession$CaptureCallback);
    }
    
    @Override
    public ListenableFuture<List<Surface>> startWithDeferrableSurface(final List<DeferrableSurface> list, final long n) {
        synchronized (this.mLock) {
            if (this.mOpenerDisabled) {
                return Futures.immediateFailedFuture(new CancellationException("Opener is disabled"));
            }
            final FutureChain<Object> transformAsync = FutureChain.from(DeferrableSurfaces.surfaceListWithTimeout(list, false, n, this.getExecutor(), this.mScheduledExecutorService)).transformAsync((AsyncFunction<? super List<Surface>, Object>)new SynchronizedCaptureSessionBaseImpl$$ExternalSyntheticLambda1(this, list), this.getExecutor());
            this.mStartingSurface = (ListenableFuture<List<Surface>>)transformAsync;
            return Futures.nonCancellationPropagating((com.google.common.util.concurrent.ListenableFuture<List<Surface>>)transformAsync);
        }
    }
    
    @Override
    public boolean stop() {
        final ListenableFuture listenableFuture = null;
        final ListenableFuture listenableFuture2 = null;
        final ListenableFuture listenableFuture3 = null;
        final ListenableFuture listenableFuture4 = null;
        ListenableFuture listenableFuture5 = listenableFuture3;
        try {
            final Object mLock = this.mLock;
            listenableFuture5 = listenableFuture3;
            monitorenter(mLock);
            ListenableFuture listenableFuture6 = listenableFuture;
            listenableFuture5 = listenableFuture2;
            try {
                if (!this.mOpenerDisabled) {
                    listenableFuture5 = listenableFuture2;
                    final ListenableFuture<List<Surface>> mStartingSurface = this.mStartingSurface;
                    listenableFuture6 = listenableFuture4;
                    if (mStartingSurface != null) {
                        listenableFuture6 = mStartingSurface;
                    }
                    listenableFuture5 = listenableFuture6;
                    this.mOpenerDisabled = true;
                }
                listenableFuture5 = listenableFuture6;
                final boolean b = !this.isCameraCaptureSessionOpen();
                listenableFuture5 = listenableFuture6;
                return b;
            }
            finally {
                monitorexit(mLock);
            }
        }
        finally {
            if (listenableFuture5 != null) {
                listenableFuture5.cancel(true);
            }
        }
    }
    
    @Override
    public void stopRepeating() throws CameraAccessException {
        Preconditions.checkNotNull(this.mCameraCaptureSessionCompat, "Need to call openCaptureSession before using this API.");
        this.mCameraCaptureSessionCompat.toCameraCaptureSession().stopRepeating();
    }
    
    @Override
    public CameraCaptureSessionCompat toCameraCaptureSessionCompat() {
        Preconditions.checkNotNull(this.mCameraCaptureSessionCompat);
        return this.mCameraCaptureSessionCompat;
    }
    
    private static class Api23Impl
    {
        static Surface getInputSurface(final CameraCaptureSession cameraCaptureSession) {
            return cameraCaptureSession.getInputSurface();
        }
    }
}

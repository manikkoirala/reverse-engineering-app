// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.internal;

import android.media.Image;
import android.os.Build$VERSION;
import java.util.NoSuchElementException;
import androidx.camera.core.Logger;
import android.hardware.camera2.params.InputConfiguration;
import android.view.Surface;
import androidx.camera.core.internal.compat.ImageWriterCompat;
import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CameraCaptureSession$StateCallback;
import androidx.camera.core.impl.ImmediateSurface;
import androidx.camera.core.impl.ImageReaderProxy;
import androidx.camera.core.MetadataImageReader;
import androidx.camera.core.impl.SessionConfig;
import java.util.Comparator;
import java.util.Arrays;
import androidx.camera.core.impl.utils.CompareSizesByArea;
import java.util.HashMap;
import android.hardware.camera2.CameraCharacteristics$Key;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.params.StreamConfigurationMap;
import com.google.common.util.concurrent.ListenableFuture;
import java.util.concurrent.Executor;
import androidx.camera.core.impl.utils.executor.CameraXExecutors;
import java.util.Objects;
import androidx.camera.core.ImageProxy;
import androidx.camera.core.internal.utils.RingBuffer;
import android.util.Size;
import java.util.Map;
import android.media.ImageWriter;
import androidx.camera.core.SafeCloseImageReaderProxy;
import androidx.camera.core.impl.DeferrableSurface;
import androidx.camera.core.impl.CameraCaptureCallback;
import androidx.camera.core.internal.utils.ZslRingBuffer;
import androidx.camera.camera2.internal.compat.CameraCharacteristicsCompat;

final class ZslControlImpl implements ZslControl
{
    static final int MAX_IMAGES = 9;
    static final int RING_BUFFER_CAPACITY = 3;
    private static final String TAG = "ZslControlImpl";
    private final CameraCharacteristicsCompat mCameraCharacteristicsCompat;
    final ZslRingBuffer mImageRingBuffer;
    private boolean mIsPrivateReprocessingSupported;
    private boolean mIsZslDisabledByFlashMode;
    private boolean mIsZslDisabledByUseCaseConfig;
    private CameraCaptureCallback mMetadataMatchingCaptureCallback;
    private DeferrableSurface mReprocessingImageDeferrableSurface;
    SafeCloseImageReaderProxy mReprocessingImageReader;
    ImageWriter mReprocessingImageWriter;
    private final Map<Integer, Size> mReprocessingInputSizeMap;
    
    ZslControlImpl(final CameraCharacteristicsCompat mCameraCharacteristicsCompat) {
        this.mIsZslDisabledByUseCaseConfig = false;
        this.mIsZslDisabledByFlashMode = false;
        this.mIsPrivateReprocessingSupported = false;
        this.mCameraCharacteristicsCompat = mCameraCharacteristicsCompat;
        this.mIsPrivateReprocessingSupported = ZslUtil.isCapabilitySupported(mCameraCharacteristicsCompat, 4);
        this.mReprocessingInputSizeMap = this.createReprocessingInputSizeMap(mCameraCharacteristicsCompat);
        this.mImageRingBuffer = new ZslRingBuffer(3, new ZslControlImpl$$ExternalSyntheticLambda2());
    }
    
    private void cleanup() {
        final ZslRingBuffer mImageRingBuffer = this.mImageRingBuffer;
        while (!mImageRingBuffer.isEmpty()) {
            mImageRingBuffer.dequeue().close();
        }
        final DeferrableSurface mReprocessingImageDeferrableSurface = this.mReprocessingImageDeferrableSurface;
        if (mReprocessingImageDeferrableSurface != null) {
            final SafeCloseImageReaderProxy mReprocessingImageReader = this.mReprocessingImageReader;
            if (mReprocessingImageReader != null) {
                final ListenableFuture<Void> terminationFuture = mReprocessingImageDeferrableSurface.getTerminationFuture();
                Objects.requireNonNull(mReprocessingImageReader);
                terminationFuture.addListener((Runnable)new ZslControlImpl$$ExternalSyntheticLambda1(mReprocessingImageReader), (Executor)CameraXExecutors.mainThreadExecutor());
                this.mReprocessingImageReader = null;
            }
            mReprocessingImageDeferrableSurface.close();
            this.mReprocessingImageDeferrableSurface = null;
        }
        final ImageWriter mReprocessingImageWriter = this.mReprocessingImageWriter;
        if (mReprocessingImageWriter != null) {
            mReprocessingImageWriter.close();
            this.mReprocessingImageWriter = null;
        }
    }
    
    private Map<Integer, Size> createReprocessingInputSizeMap(final CameraCharacteristicsCompat cameraCharacteristicsCompat) {
        final StreamConfigurationMap streamConfigurationMap = cameraCharacteristicsCompat.get((android.hardware.camera2.CameraCharacteristics$Key<StreamConfigurationMap>)CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP);
        if (streamConfigurationMap != null && streamConfigurationMap.getInputFormats() != null) {
            final HashMap hashMap = new HashMap();
            for (final int j : streamConfigurationMap.getInputFormats()) {
                final Size[] inputSizes = streamConfigurationMap.getInputSizes(j);
                if (inputSizes != null) {
                    Arrays.sort(inputSizes, new CompareSizesByArea(true));
                    hashMap.put(j, inputSizes[0]);
                }
            }
            return hashMap;
        }
        return new HashMap<Integer, Size>();
    }
    
    private boolean isJpegValidOutputForInputFormat(final CameraCharacteristicsCompat cameraCharacteristicsCompat, int i) {
        final StreamConfigurationMap streamConfigurationMap = cameraCharacteristicsCompat.get((android.hardware.camera2.CameraCharacteristics$Key<StreamConfigurationMap>)CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP);
        if (streamConfigurationMap == null) {
            return false;
        }
        final int[] validOutputFormatsForInput = streamConfigurationMap.getValidOutputFormatsForInput(i);
        if (validOutputFormatsForInput == null) {
            return false;
        }
        int length;
        for (length = validOutputFormatsForInput.length, i = 0; i < length; ++i) {
            if (validOutputFormatsForInput[i] == 256) {
                return true;
            }
        }
        return false;
    }
    
    @Override
    public void addZslConfig(final SessionConfig.Builder builder) {
        this.cleanup();
        if (this.mIsZslDisabledByUseCaseConfig) {
            return;
        }
        if (this.mIsPrivateReprocessingSupported && !this.mReprocessingInputSizeMap.isEmpty() && this.mReprocessingInputSizeMap.containsKey(34)) {
            if (this.isJpegValidOutputForInputFormat(this.mCameraCharacteristicsCompat, 34)) {
                final Size size = this.mReprocessingInputSizeMap.get(34);
                final MetadataImageReader metadataImageReader = new MetadataImageReader(size.getWidth(), size.getHeight(), 34, 9);
                this.mMetadataMatchingCaptureCallback = metadataImageReader.getCameraCaptureCallback();
                this.mReprocessingImageReader = new SafeCloseImageReaderProxy(metadataImageReader);
                metadataImageReader.setOnImageAvailableListener(new ZslControlImpl$$ExternalSyntheticLambda0(this), CameraXExecutors.ioExecutor());
                final ImmediateSurface mReprocessingImageDeferrableSurface = new ImmediateSurface(this.mReprocessingImageReader.getSurface(), new Size(this.mReprocessingImageReader.getWidth(), this.mReprocessingImageReader.getHeight()), 34);
                this.mReprocessingImageDeferrableSurface = mReprocessingImageDeferrableSurface;
                final SafeCloseImageReaderProxy mReprocessingImageReader = this.mReprocessingImageReader;
                final ListenableFuture<Void> terminationFuture = mReprocessingImageDeferrableSurface.getTerminationFuture();
                Objects.requireNonNull(mReprocessingImageReader);
                terminationFuture.addListener((Runnable)new ZslControlImpl$$ExternalSyntheticLambda1(mReprocessingImageReader), (Executor)CameraXExecutors.mainThreadExecutor());
                builder.addSurface(this.mReprocessingImageDeferrableSurface);
                builder.addCameraCaptureCallback(this.mMetadataMatchingCaptureCallback);
                builder.addSessionStateCallback(new CameraCaptureSession$StateCallback(this) {
                    final ZslControlImpl this$0;
                    
                    public void onConfigureFailed(final CameraCaptureSession cameraCaptureSession) {
                    }
                    
                    public void onConfigured(final CameraCaptureSession cameraCaptureSession) {
                        final Surface inputSurface = cameraCaptureSession.getInputSurface();
                        if (inputSurface != null) {
                            this.this$0.mReprocessingImageWriter = ImageWriterCompat.newInstance(inputSurface, 1);
                        }
                    }
                });
                builder.setInputConfiguration(new InputConfiguration(this.mReprocessingImageReader.getWidth(), this.mReprocessingImageReader.getHeight(), this.mReprocessingImageReader.getImageFormat()));
            }
        }
    }
    
    @Override
    public ImageProxy dequeueImageFromBuffer() {
        ImageProxy imageProxy;
        try {
            imageProxy = this.mImageRingBuffer.dequeue();
        }
        catch (final NoSuchElementException ex) {
            Logger.e("ZslControlImpl", "dequeueImageFromBuffer no such element");
            imageProxy = null;
        }
        return imageProxy;
    }
    
    @Override
    public boolean enqueueImageToImageWriter(final ImageProxy imageProxy) {
        final Image image = imageProxy.getImage();
        if (Build$VERSION.SDK_INT >= 23) {
            final ImageWriter mReprocessingImageWriter = this.mReprocessingImageWriter;
            if (mReprocessingImageWriter != null && image != null) {
                try {
                    ImageWriterCompat.queueInputImage(mReprocessingImageWriter, image);
                    return true;
                }
                catch (final IllegalStateException ex) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("enqueueImageToImageWriter throws IllegalStateException = ");
                    sb.append(ex.getMessage());
                    Logger.e("ZslControlImpl", sb.toString());
                }
            }
        }
        return false;
    }
    
    @Override
    public boolean isZslDisabledByFlashMode() {
        return this.mIsZslDisabledByFlashMode;
    }
    
    @Override
    public boolean isZslDisabledByUserCaseConfig() {
        return this.mIsZslDisabledByUseCaseConfig;
    }
    
    @Override
    public void setZslDisabledByFlashMode(final boolean mIsZslDisabledByFlashMode) {
        this.mIsZslDisabledByFlashMode = mIsZslDisabledByFlashMode;
    }
    
    @Override
    public void setZslDisabledByUserCaseConfig(final boolean mIsZslDisabledByUseCaseConfig) {
        this.mIsZslDisabledByUseCaseConfig = mIsZslDisabledByUseCaseConfig;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.internal;

import java.util.ArrayList;
import java.util.Iterator;
import android.hardware.camera2.CameraDevice;
import java.util.Collection;
import java.util.List;
import java.util.LinkedHashSet;
import java.util.concurrent.Executor;
import java.util.Set;
import android.hardware.camera2.CameraDevice$StateCallback;

class CaptureSessionRepository
{
    private final CameraDevice$StateCallback mCameraStateCallback;
    final Set<SynchronizedCaptureSession> mCaptureSessions;
    final Set<SynchronizedCaptureSession> mClosingCaptureSession;
    final Set<SynchronizedCaptureSession> mCreatingCaptureSessions;
    final Executor mExecutor;
    final Object mLock;
    
    CaptureSessionRepository(final Executor mExecutor) {
        this.mLock = new Object();
        this.mCaptureSessions = new LinkedHashSet<SynchronizedCaptureSession>();
        this.mClosingCaptureSession = new LinkedHashSet<SynchronizedCaptureSession>();
        this.mCreatingCaptureSessions = new LinkedHashSet<SynchronizedCaptureSession>();
        this.mCameraStateCallback = new CameraDevice$StateCallback() {
            final CaptureSessionRepository this$0;
            
            private void cameraClosed() {
                Object o = this.this$0.mLock;
                synchronized (o) {
                    final List<SynchronizedCaptureSession> sessionsInOrder = this.this$0.getSessionsInOrder();
                    this.this$0.mCreatingCaptureSessions.clear();
                    this.this$0.mCaptureSessions.clear();
                    this.this$0.mClosingCaptureSession.clear();
                    monitorexit(o);
                    o = sessionsInOrder.iterator();
                    while (((Iterator)o).hasNext()) {
                        ((SynchronizedCaptureSession)((Iterator)o).next()).finishClose();
                    }
                }
            }
            
            private void forceOnClosedCaptureSessions() {
                final LinkedHashSet set = new LinkedHashSet();
                synchronized (this.this$0.mLock) {
                    set.addAll(this.this$0.mCreatingCaptureSessions);
                    set.addAll(this.this$0.mCaptureSessions);
                    monitorexit(this.this$0.mLock);
                    this.this$0.mExecutor.execute(new CaptureSessionRepository$1$$ExternalSyntheticLambda0(set));
                }
            }
            
            public void onClosed(final CameraDevice cameraDevice) {
                this.cameraClosed();
            }
            
            public void onDisconnected(final CameraDevice cameraDevice) {
                this.forceOnClosedCaptureSessions();
                this.cameraClosed();
            }
            
            public void onError(final CameraDevice cameraDevice, final int n) {
                this.forceOnClosedCaptureSessions();
                this.cameraClosed();
            }
            
            public void onOpened(final CameraDevice cameraDevice) {
            }
        };
        this.mExecutor = mExecutor;
    }
    
    private void forceFinishCloseStaleSessions(final SynchronizedCaptureSession synchronizedCaptureSession) {
        for (final SynchronizedCaptureSession synchronizedCaptureSession2 : this.getSessionsInOrder()) {
            if (synchronizedCaptureSession2 == synchronizedCaptureSession) {
                break;
            }
            synchronizedCaptureSession2.finishClose();
        }
    }
    
    static void forceOnClosed(final Set<SynchronizedCaptureSession> set) {
        for (final SynchronizedCaptureSession synchronizedCaptureSession : set) {
            synchronizedCaptureSession.getStateCallback().onClosed(synchronizedCaptureSession);
        }
    }
    
    CameraDevice$StateCallback getCameraStateCallback() {
        return this.mCameraStateCallback;
    }
    
    List<SynchronizedCaptureSession> getCaptureSessions() {
        synchronized (this.mLock) {
            return new ArrayList<SynchronizedCaptureSession>(this.mCaptureSessions);
        }
    }
    
    List<SynchronizedCaptureSession> getClosingCaptureSession() {
        synchronized (this.mLock) {
            return new ArrayList<SynchronizedCaptureSession>(this.mClosingCaptureSession);
        }
    }
    
    List<SynchronizedCaptureSession> getCreatingCaptureSessions() {
        synchronized (this.mLock) {
            return new ArrayList<SynchronizedCaptureSession>(this.mCreatingCaptureSessions);
        }
    }
    
    List<SynchronizedCaptureSession> getSessionsInOrder() {
        synchronized (this.mLock) {
            final ArrayList list = new ArrayList();
            list.addAll(this.getCaptureSessions());
            list.addAll(this.getCreatingCaptureSessions());
            return list;
        }
    }
    
    void onCaptureSessionClosed(final SynchronizedCaptureSession synchronizedCaptureSession) {
        synchronized (this.mLock) {
            this.mCaptureSessions.remove(synchronizedCaptureSession);
            this.mClosingCaptureSession.remove(synchronizedCaptureSession);
        }
    }
    
    void onCaptureSessionClosing(final SynchronizedCaptureSession synchronizedCaptureSession) {
        synchronized (this.mLock) {
            this.mClosingCaptureSession.add(synchronizedCaptureSession);
        }
    }
    
    void onCaptureSessionConfigureFail(final SynchronizedCaptureSession synchronizedCaptureSession) {
        this.forceFinishCloseStaleSessions(synchronizedCaptureSession);
        synchronized (this.mLock) {
            this.mCreatingCaptureSessions.remove(synchronizedCaptureSession);
        }
    }
    
    void onCaptureSessionCreated(final SynchronizedCaptureSession synchronizedCaptureSession) {
        synchronized (this.mLock) {
            this.mCaptureSessions.add(synchronizedCaptureSession);
            this.mCreatingCaptureSessions.remove(synchronizedCaptureSession);
            monitorexit(this.mLock);
            this.forceFinishCloseStaleSessions(synchronizedCaptureSession);
        }
    }
    
    void onCreateCaptureSession(final SynchronizedCaptureSession synchronizedCaptureSession) {
        synchronized (this.mLock) {
            this.mCreatingCaptureSessions.add(synchronizedCaptureSession);
        }
    }
}

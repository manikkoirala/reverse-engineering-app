// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.internal;

import android.view.Display;
import android.graphics.Point;
import android.content.Context;
import androidx.camera.camera2.internal.compat.workaround.MaxPreviewSize;
import android.hardware.display.DisplayManager;
import android.util.Size;

public class DisplayInfoManager
{
    private static final Object INSTANCE_LOCK;
    private static final Size MAX_PREVIEW_SIZE;
    private static volatile DisplayInfoManager sInstance;
    private final DisplayManager mDisplayManager;
    private final MaxPreviewSize mMaxPreviewSize;
    private volatile Size mPreviewSize;
    
    static {
        MAX_PREVIEW_SIZE = new Size(1920, 1080);
        INSTANCE_LOCK = new Object();
    }
    
    private DisplayInfoManager(final Context context) {
        this.mPreviewSize = null;
        this.mMaxPreviewSize = new MaxPreviewSize();
        this.mDisplayManager = (DisplayManager)context.getSystemService("display");
    }
    
    private Size calculatePreviewSize() {
        final Point point = new Point();
        this.getMaxSizeDisplay().getRealSize(point);
        Size size;
        if (point.x > point.y) {
            size = new Size(point.x, point.y);
        }
        else {
            size = new Size(point.y, point.x);
        }
        final int width = size.getWidth();
        final int height = size.getHeight();
        final Size max_PREVIEW_SIZE = DisplayInfoManager.MAX_PREVIEW_SIZE;
        if (width * height > max_PREVIEW_SIZE.getWidth() * max_PREVIEW_SIZE.getHeight()) {
            size = max_PREVIEW_SIZE;
        }
        return this.mMaxPreviewSize.getMaxPreviewResolution(size);
    }
    
    public static DisplayInfoManager getInstance(final Context context) {
        if (DisplayInfoManager.sInstance == null) {
            synchronized (DisplayInfoManager.INSTANCE_LOCK) {
                if (DisplayInfoManager.sInstance == null) {
                    DisplayInfoManager.sInstance = new DisplayInfoManager(context);
                }
            }
        }
        return DisplayInfoManager.sInstance;
    }
    
    static void releaseInstance() {
        DisplayInfoManager.sInstance = null;
    }
    
    public Display getMaxSizeDisplay() {
        final Display[] displays = this.mDisplayManager.getDisplays();
        final int length = displays.length;
        int i = 0;
        if (length == 1) {
            return displays[0];
        }
        Display display = null;
        int n = -1;
        while (i < displays.length) {
            final Display display2 = displays[i];
            Display display3 = display;
            int n2 = n;
            if (display2.getState() != 1) {
                final Point point = new Point();
                display2.getRealSize(point);
                display3 = display;
                if (point.x * point.y > (n2 = n)) {
                    n2 = point.x * point.y;
                    display3 = display2;
                }
            }
            ++i;
            display = display3;
            n = n2;
        }
        if (display != null) {
            return display;
        }
        throw new IllegalArgumentException("No display can be found from the input display manager!");
    }
    
    Size getPreviewSize() {
        if (this.mPreviewSize != null) {
            return this.mPreviewSize;
        }
        return this.mPreviewSize = this.calculatePreviewSize();
    }
    
    void refresh() {
        this.mPreviewSize = this.calculatePreviewSize();
    }
}

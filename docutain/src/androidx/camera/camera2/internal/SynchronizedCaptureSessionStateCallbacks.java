// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.internal;

import androidx.camera.camera2.internal.compat.ApiCompat;
import android.hardware.camera2.CameraCaptureSession$StateCallback;
import android.view.Surface;
import java.util.Iterator;
import java.util.Arrays;
import java.util.Collection;
import java.util.ArrayList;
import java.util.List;

final class SynchronizedCaptureSessionStateCallbacks extends StateCallback
{
    private final List<StateCallback> mCallbacks;
    
    SynchronizedCaptureSessionStateCallbacks(final List<StateCallback> list) {
        (this.mCallbacks = new ArrayList<StateCallback>()).addAll(list);
    }
    
    static StateCallback createComboCallback(final StateCallback... a) {
        return new SynchronizedCaptureSessionStateCallbacks(Arrays.asList(a));
    }
    
    public void onActive(final SynchronizedCaptureSession synchronizedCaptureSession) {
        final Iterator<StateCallback> iterator = this.mCallbacks.iterator();
        while (iterator.hasNext()) {
            iterator.next().onActive(synchronizedCaptureSession);
        }
    }
    
    public void onCaptureQueueEmpty(final SynchronizedCaptureSession synchronizedCaptureSession) {
        final Iterator<StateCallback> iterator = this.mCallbacks.iterator();
        while (iterator.hasNext()) {
            iterator.next().onCaptureQueueEmpty(synchronizedCaptureSession);
        }
    }
    
    @Override
    public void onClosed(final SynchronizedCaptureSession synchronizedCaptureSession) {
        final Iterator<StateCallback> iterator = this.mCallbacks.iterator();
        while (iterator.hasNext()) {
            iterator.next().onClosed(synchronizedCaptureSession);
        }
    }
    
    @Override
    public void onConfigureFailed(final SynchronizedCaptureSession synchronizedCaptureSession) {
        final Iterator<StateCallback> iterator = this.mCallbacks.iterator();
        while (iterator.hasNext()) {
            iterator.next().onConfigureFailed(synchronizedCaptureSession);
        }
    }
    
    public void onConfigured(final SynchronizedCaptureSession synchronizedCaptureSession) {
        final Iterator<StateCallback> iterator = this.mCallbacks.iterator();
        while (iterator.hasNext()) {
            iterator.next().onConfigured(synchronizedCaptureSession);
        }
    }
    
    public void onReady(final SynchronizedCaptureSession synchronizedCaptureSession) {
        final Iterator<StateCallback> iterator = this.mCallbacks.iterator();
        while (iterator.hasNext()) {
            iterator.next().onReady(synchronizedCaptureSession);
        }
    }
    
    @Override
    void onSessionFinished(final SynchronizedCaptureSession synchronizedCaptureSession) {
        final Iterator<StateCallback> iterator = this.mCallbacks.iterator();
        while (iterator.hasNext()) {
            iterator.next().onSessionFinished(synchronizedCaptureSession);
        }
    }
    
    public void onSurfacePrepared(final SynchronizedCaptureSession synchronizedCaptureSession, final Surface surface) {
        final Iterator<StateCallback> iterator = this.mCallbacks.iterator();
        while (iterator.hasNext()) {
            iterator.next().onSurfacePrepared(synchronizedCaptureSession, surface);
        }
    }
    
    static class Adapter extends StateCallback
    {
        private final CameraCaptureSession$StateCallback mCameraCaptureSessionStateCallback;
        
        Adapter(final CameraCaptureSession$StateCallback mCameraCaptureSessionStateCallback) {
            this.mCameraCaptureSessionStateCallback = mCameraCaptureSessionStateCallback;
        }
        
        Adapter(final List<CameraCaptureSession$StateCallback> list) {
            this(CameraCaptureSessionStateCallbacks.createComboCallback(list));
        }
        
        public void onActive(final SynchronizedCaptureSession synchronizedCaptureSession) {
            this.mCameraCaptureSessionStateCallback.onActive(synchronizedCaptureSession.toCameraCaptureSessionCompat().toCameraCaptureSession());
        }
        
        public void onCaptureQueueEmpty(final SynchronizedCaptureSession synchronizedCaptureSession) {
            ApiCompat.Api26Impl.onCaptureQueueEmpty(this.mCameraCaptureSessionStateCallback, synchronizedCaptureSession.toCameraCaptureSessionCompat().toCameraCaptureSession());
        }
        
        @Override
        public void onClosed(final SynchronizedCaptureSession synchronizedCaptureSession) {
            this.mCameraCaptureSessionStateCallback.onClosed(synchronizedCaptureSession.toCameraCaptureSessionCompat().toCameraCaptureSession());
        }
        
        @Override
        public void onConfigureFailed(final SynchronizedCaptureSession synchronizedCaptureSession) {
            this.mCameraCaptureSessionStateCallback.onConfigureFailed(synchronizedCaptureSession.toCameraCaptureSessionCompat().toCameraCaptureSession());
        }
        
        public void onConfigured(final SynchronizedCaptureSession synchronizedCaptureSession) {
            this.mCameraCaptureSessionStateCallback.onConfigured(synchronizedCaptureSession.toCameraCaptureSessionCompat().toCameraCaptureSession());
        }
        
        public void onReady(final SynchronizedCaptureSession synchronizedCaptureSession) {
            this.mCameraCaptureSessionStateCallback.onReady(synchronizedCaptureSession.toCameraCaptureSessionCompat().toCameraCaptureSession());
        }
        
        @Override
        void onSessionFinished(final SynchronizedCaptureSession synchronizedCaptureSession) {
        }
        
        public void onSurfacePrepared(final SynchronizedCaptureSession synchronizedCaptureSession, final Surface surface) {
            ApiCompat.Api23Impl.onSurfacePrepared(this.mCameraCaptureSessionStateCallback, synchronizedCaptureSession.toCameraCaptureSessionCompat().toCameraCaptureSession(), surface);
        }
    }
}

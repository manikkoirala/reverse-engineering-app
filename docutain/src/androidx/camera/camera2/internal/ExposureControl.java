// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.internal;

import android.util.Range;
import androidx.camera.core.impl.utils.futures.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import android.hardware.camera2.CaptureRequest$Key;
import android.hardware.camera2.CaptureRequest;
import androidx.camera.camera2.impl.Camera2ImplConfig;
import androidx.core.util.Preconditions;
import android.hardware.camera2.CaptureResult;
import android.hardware.camera2.TotalCaptureResult;
import androidx.camera.core.ExposureState;
import androidx.camera.core.CameraControl;
import androidx.camera.camera2.internal.compat.CameraCharacteristicsCompat;
import androidx.concurrent.futures.CallbackToFutureAdapter;
import java.util.concurrent.Executor;

public class ExposureControl
{
    private static final int DEFAULT_EXPOSURE_COMPENSATION = 0;
    private final Camera2CameraControlImpl mCameraControl;
    private final Executor mExecutor;
    private final ExposureStateImpl mExposureStateImpl;
    private boolean mIsActive;
    private Camera2CameraControlImpl.CaptureResultListener mRunningCaptureResultListener;
    private CallbackToFutureAdapter.Completer<Integer> mRunningCompleter;
    
    ExposureControl(final Camera2CameraControlImpl mCameraControl, final CameraCharacteristicsCompat cameraCharacteristicsCompat, final Executor mExecutor) {
        this.mIsActive = false;
        this.mCameraControl = mCameraControl;
        this.mExposureStateImpl = new ExposureStateImpl(cameraCharacteristicsCompat, 0);
        this.mExecutor = mExecutor;
    }
    
    private void clearRunningTask() {
        final CallbackToFutureAdapter.Completer<Integer> mRunningCompleter = this.mRunningCompleter;
        if (mRunningCompleter != null) {
            mRunningCompleter.setException(new CameraControl.OperationCanceledException("Cancelled by another setExposureCompensationIndex()"));
            this.mRunningCompleter = null;
        }
        final Camera2CameraControlImpl.CaptureResultListener mRunningCaptureResultListener = this.mRunningCaptureResultListener;
        if (mRunningCaptureResultListener != null) {
            this.mCameraControl.removeCaptureResultListener(mRunningCaptureResultListener);
            this.mRunningCaptureResultListener = null;
        }
    }
    
    static ExposureState getDefaultExposureState(final CameraCharacteristicsCompat cameraCharacteristicsCompat) {
        return new ExposureStateImpl(cameraCharacteristicsCompat, 0);
    }
    
    ExposureState getExposureState() {
        return this.mExposureStateImpl;
    }
    
    void setActive(final boolean mIsActive) {
        if (mIsActive == this.mIsActive) {
            return;
        }
        if (!(this.mIsActive = mIsActive)) {
            this.mExposureStateImpl.setExposureCompensationIndex(0);
            this.clearRunningTask();
        }
    }
    
    void setCaptureRequestOption(final Camera2ImplConfig.Builder builder) {
        builder.setCaptureRequestOption((android.hardware.camera2.CaptureRequest$Key<Integer>)CaptureRequest.CONTROL_AE_EXPOSURE_COMPENSATION, this.mExposureStateImpl.getExposureCompensationIndex());
    }
    
    ListenableFuture<Integer> setExposureCompensationIndex(final int exposureCompensationIndex) {
        if (!this.mExposureStateImpl.isExposureCompensationSupported()) {
            return Futures.immediateFailedFuture(new IllegalArgumentException("ExposureCompensation is not supported"));
        }
        final Range<Integer> exposureCompensationRange = this.mExposureStateImpl.getExposureCompensationRange();
        if (!exposureCompensationRange.contains((Comparable)exposureCompensationIndex)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Requested ExposureCompensation ");
            sb.append(exposureCompensationIndex);
            sb.append(" is not within valid range [");
            sb.append(exposureCompensationRange.getUpper());
            sb.append("..");
            sb.append(exposureCompensationRange.getLower());
            sb.append("]");
            return (ListenableFuture<Integer>)Futures.immediateFailedFuture(new IllegalArgumentException(sb.toString()));
        }
        this.mExposureStateImpl.setExposureCompensationIndex(exposureCompensationIndex);
        return Futures.nonCancellationPropagating((com.google.common.util.concurrent.ListenableFuture<Integer>)CallbackToFutureAdapter.getFuture((CallbackToFutureAdapter.Resolver<V>)new ExposureControl$$ExternalSyntheticLambda1(this, exposureCompensationIndex)));
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.internal;

import android.media.CamcorderProfile;
import androidx.camera.core.impl.CamcorderProfileProxy;
import androidx.camera.camera2.internal.compat.quirk.CameraQuirks;
import androidx.camera.camera2.internal.compat.quirk.CamcorderProfileResolutionQuirk;
import androidx.camera.core.Logger;
import androidx.camera.camera2.internal.compat.CameraCharacteristicsCompat;
import androidx.camera.camera2.internal.compat.workaround.CamcorderProfileResolutionValidator;
import androidx.camera.core.impl.CamcorderProfileProvider;

public class Camera2CamcorderProfileProvider implements CamcorderProfileProvider
{
    private static final String TAG = "Camera2CamcorderProfileProvider";
    private final CamcorderProfileResolutionValidator mCamcorderProfileResolutionValidator;
    private final int mCameraId;
    private final boolean mHasValidCameraId;
    
    public Camera2CamcorderProfileProvider(final String s, final CameraCharacteristicsCompat cameraCharacteristicsCompat) {
        int int1;
        boolean mHasValidCameraId;
        try {
            int1 = Integer.parseInt(s);
            mHasValidCameraId = true;
        }
        catch (final NumberFormatException ex) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Camera id is not an integer: ");
            sb.append(s);
            sb.append(", unable to create CamcorderProfileProvider");
            Logger.w("Camera2CamcorderProfileProvider", sb.toString());
            mHasValidCameraId = false;
            int1 = -1;
        }
        this.mHasValidCameraId = mHasValidCameraId;
        this.mCameraId = int1;
        this.mCamcorderProfileResolutionValidator = new CamcorderProfileResolutionValidator(CameraQuirks.get(s, cameraCharacteristicsCompat).get(CamcorderProfileResolutionQuirk.class));
    }
    
    private CamcorderProfileProxy getProfileInternal(final int i) {
        CamcorderProfileProxy fromCamcorderProfile = null;
        CamcorderProfile value;
        try {
            value = CamcorderProfile.get(this.mCameraId, i);
        }
        catch (final RuntimeException ex) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Unable to get CamcorderProfile by quality: ");
            sb.append(i);
            Logger.w("Camera2CamcorderProfileProvider", sb.toString(), ex);
            value = null;
        }
        if (value != null) {
            fromCamcorderProfile = CamcorderProfileProxy.fromCamcorderProfile(value);
        }
        return fromCamcorderProfile;
    }
    
    @Override
    public CamcorderProfileProxy get(final int n) {
        if (!this.mHasValidCameraId) {
            return null;
        }
        if (!CamcorderProfile.hasProfile(this.mCameraId, n)) {
            return null;
        }
        final CamcorderProfileProxy profileInternal = this.getProfileInternal(n);
        if (!this.mCamcorderProfileResolutionValidator.hasValidVideoResolution(profileInternal)) {
            return null;
        }
        return profileInternal;
    }
    
    @Override
    public boolean hasProfile(final int n) {
        return this.mHasValidCameraId && CamcorderProfile.hasProfile(this.mCameraId, n) && (!this.mCamcorderProfileResolutionValidator.hasQuirk() || this.mCamcorderProfileResolutionValidator.hasValidVideoResolution(this.getProfileInternal(n)));
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.internal;

import android.hardware.camera2.CaptureFailure;
import androidx.camera.core.impl.CameraCaptureFailure;

public final class Camera2CameraCaptureFailure extends CameraCaptureFailure
{
    private final CaptureFailure mCaptureFailure;
    
    public Camera2CameraCaptureFailure(final Reason reason, final CaptureFailure mCaptureFailure) {
        super(reason);
        this.mCaptureFailure = mCaptureFailure;
    }
    
    public CaptureFailure getCaptureFailure() {
        return this.mCaptureFailure;
    }
}

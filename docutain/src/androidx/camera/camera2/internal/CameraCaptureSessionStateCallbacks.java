// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.internal;

import android.view.Surface;
import androidx.camera.camera2.internal.compat.ApiCompat;
import android.hardware.camera2.CameraCaptureSession;
import java.util.Iterator;
import java.util.ArrayList;
import java.util.Arrays;
import android.hardware.camera2.CameraCaptureSession$StateCallback;
import java.util.List;

public final class CameraCaptureSessionStateCallbacks
{
    private CameraCaptureSessionStateCallbacks() {
    }
    
    public static CameraCaptureSession$StateCallback createComboCallback(final List<CameraCaptureSession$StateCallback> list) {
        if (list.isEmpty()) {
            return createNoOpCallback();
        }
        if (list.size() == 1) {
            return list.get(0);
        }
        return new ComboSessionStateCallback(list);
    }
    
    public static CameraCaptureSession$StateCallback createComboCallback(final CameraCaptureSession$StateCallback... a) {
        return createComboCallback(Arrays.asList(a));
    }
    
    public static CameraCaptureSession$StateCallback createNoOpCallback() {
        return new NoOpSessionStateCallback();
    }
    
    static final class ComboSessionStateCallback extends CameraCaptureSession$StateCallback
    {
        private final List<CameraCaptureSession$StateCallback> mCallbacks;
        
        ComboSessionStateCallback(final List<CameraCaptureSession$StateCallback> list) {
            this.mCallbacks = new ArrayList<CameraCaptureSession$StateCallback>();
            for (final CameraCaptureSession$StateCallback cameraCaptureSession$StateCallback : list) {
                if (!(cameraCaptureSession$StateCallback instanceof NoOpSessionStateCallback)) {
                    this.mCallbacks.add(cameraCaptureSession$StateCallback);
                }
            }
        }
        
        public void onActive(final CameraCaptureSession cameraCaptureSession) {
            final Iterator<CameraCaptureSession$StateCallback> iterator = this.mCallbacks.iterator();
            while (iterator.hasNext()) {
                iterator.next().onActive(cameraCaptureSession);
            }
        }
        
        public void onCaptureQueueEmpty(final CameraCaptureSession cameraCaptureSession) {
            final Iterator<CameraCaptureSession$StateCallback> iterator = this.mCallbacks.iterator();
            while (iterator.hasNext()) {
                ApiCompat.Api26Impl.onCaptureQueueEmpty(iterator.next(), cameraCaptureSession);
            }
        }
        
        public void onClosed(final CameraCaptureSession cameraCaptureSession) {
            final Iterator<CameraCaptureSession$StateCallback> iterator = this.mCallbacks.iterator();
            while (iterator.hasNext()) {
                iterator.next().onClosed(cameraCaptureSession);
            }
        }
        
        public void onConfigureFailed(final CameraCaptureSession cameraCaptureSession) {
            final Iterator<CameraCaptureSession$StateCallback> iterator = this.mCallbacks.iterator();
            while (iterator.hasNext()) {
                iterator.next().onConfigureFailed(cameraCaptureSession);
            }
        }
        
        public void onConfigured(final CameraCaptureSession cameraCaptureSession) {
            final Iterator<CameraCaptureSession$StateCallback> iterator = this.mCallbacks.iterator();
            while (iterator.hasNext()) {
                iterator.next().onConfigured(cameraCaptureSession);
            }
        }
        
        public void onReady(final CameraCaptureSession cameraCaptureSession) {
            final Iterator<CameraCaptureSession$StateCallback> iterator = this.mCallbacks.iterator();
            while (iterator.hasNext()) {
                iterator.next().onReady(cameraCaptureSession);
            }
        }
        
        public void onSurfacePrepared(final CameraCaptureSession cameraCaptureSession, final Surface surface) {
            final Iterator<CameraCaptureSession$StateCallback> iterator = this.mCallbacks.iterator();
            while (iterator.hasNext()) {
                ApiCompat.Api23Impl.onSurfacePrepared(iterator.next(), cameraCaptureSession, surface);
            }
        }
    }
    
    static final class NoOpSessionStateCallback extends CameraCaptureSession$StateCallback
    {
        public void onActive(final CameraCaptureSession cameraCaptureSession) {
        }
        
        public void onCaptureQueueEmpty(final CameraCaptureSession cameraCaptureSession) {
        }
        
        public void onClosed(final CameraCaptureSession cameraCaptureSession) {
        }
        
        public void onConfigureFailed(final CameraCaptureSession cameraCaptureSession) {
        }
        
        public void onConfigured(final CameraCaptureSession cameraCaptureSession) {
        }
        
        public void onReady(final CameraCaptureSession cameraCaptureSession) {
        }
        
        public void onSurfacePrepared(final CameraCaptureSession cameraCaptureSession, final Surface surface) {
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.internal;

import android.os.SystemClock;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.ScheduledFuture;
import java.util.Locale;
import androidx.camera.camera2.internal.compat.ApiCompat;
import java.util.Arrays;
import java.util.concurrent.TimeoutException;
import android.hardware.camera2.CameraAccessException;
import java.util.concurrent.CancellationException;
import androidx.camera.core.impl.utils.futures.FutureCallback;
import androidx.camera.core.impl.Config;
import androidx.camera.camera2.impl.Camera2ImplConfig;
import androidx.camera.core.Camera$_CC;
import java.util.concurrent.ExecutionException;
import androidx.camera.core.impl.Observable;
import java.util.LinkedHashSet;
import androidx.camera.core.impl.CameraInfoInternal;
import androidx.camera.core.CameraInfo;
import androidx.camera.core.impl.CameraInternal$_CC;
import androidx.camera.core.CameraControl;
import android.os.Build$VERSION;
import java.util.concurrent.RejectedExecutionException;
import androidx.camera.core.impl.UseCaseConfig;
import android.util.Size;
import android.text.TextUtils;
import android.util.Rational;
import androidx.camera.core.Preview;
import androidx.camera.core.CameraState;
import androidx.camera.core.UseCase;
import androidx.camera.core.impl.utils.futures.Futures;
import java.util.Collection;
import java.util.ArrayList;
import android.hardware.camera2.CameraDevice$StateCallback;
import androidx.camera.core.impl.ImmediateSurface;
import android.view.Surface;
import android.graphics.SurfaceTexture;
import androidx.core.util.Preconditions;
import java.util.List;
import java.util.Iterator;
import androidx.camera.core.impl.DeferrableSurface;
import androidx.camera.core.impl.CaptureConfig;
import androidx.camera.core.impl.SessionConfig;
import androidx.camera.core.Logger;
import androidx.camera.core.CameraUnavailableException;
import androidx.camera.camera2.internal.compat.CameraAccessExceptionCompat;
import android.hardware.camera2.CameraManager$AvailabilityCallback;
import androidx.camera.core.Camera;
import androidx.camera.camera2.internal.compat.quirk.DeviceQuirks;
import androidx.camera.core.impl.CameraControlInternal;
import androidx.camera.core.impl.utils.executor.CameraXExecutors;
import androidx.camera.core.impl.CameraConfigs;
import java.util.HashSet;
import java.util.LinkedHashMap;
import android.os.Handler;
import androidx.concurrent.futures.CallbackToFutureAdapter;
import androidx.camera.core.impl.UseCaseAttachState;
import androidx.camera.core.impl.SessionProcessor;
import java.util.concurrent.ScheduledExecutorService;
import com.google.common.util.concurrent.ListenableFuture;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import androidx.camera.core.impl.LiveDataObservable;
import java.util.concurrent.Executor;
import java.util.Set;
import androidx.camera.core.impl.CameraStateRegistry;
import androidx.camera.camera2.internal.compat.CameraManagerCompat;
import android.hardware.camera2.CameraDevice;
import androidx.camera.core.impl.CameraConfig;
import androidx.camera.core.impl.CameraInternal;

final class Camera2CameraImpl implements CameraInternal
{
    private static final int ERROR_NONE = 0;
    private static final String TAG = "Camera2CameraImpl";
    private final CameraAvailability mCameraAvailability;
    private CameraConfig mCameraConfig;
    private final Camera2CameraControlImpl mCameraControlInternal;
    CameraDevice mCameraDevice;
    int mCameraDeviceError;
    final Camera2CameraInfoImpl mCameraInfoInternal;
    private final CameraManagerCompat mCameraManager;
    private final CameraStateMachine mCameraStateMachine;
    private final CameraStateRegistry mCameraStateRegistry;
    CaptureSessionInterface mCaptureSession;
    private final SynchronizedCaptureSessionOpener.Builder mCaptureSessionOpenerBuilder;
    private final CaptureSessionRepository mCaptureSessionRepository;
    final Set<CaptureSession> mConfiguringForClose;
    private final DisplayInfoManager mDisplayInfoManager;
    private final Executor mExecutor;
    boolean mIsActiveResumingMode;
    final Object mLock;
    private MeteringRepeatingSession mMeteringRepeatingSession;
    private final Set<String> mNotifyStateAttachedSet;
    private final LiveDataObservable<State> mObservableState;
    final AtomicInteger mReleaseRequestCount;
    final Map<CaptureSessionInterface, ListenableFuture<Void>> mReleasedCaptureSessions;
    private final ScheduledExecutorService mScheduledExecutorService;
    private SessionProcessor mSessionProcessor;
    volatile InternalState mState;
    private final StateCallback mStateCallback;
    private final UseCaseAttachState mUseCaseAttachState;
    ListenableFuture<Void> mUserReleaseFuture;
    CallbackToFutureAdapter.Completer<Void> mUserReleaseNotifier;
    
    Camera2CameraImpl(final CameraManagerCompat mCameraManager, final String s, final Camera2CameraInfoImpl mCameraInfoInternal, final CameraStateRegistry mCameraStateRegistry, Executor sequentialExecutor, final Handler handler, final DisplayInfoManager mDisplayInfoManager) throws CameraUnavailableException {
        this.mState = InternalState.INITIALIZED;
        final LiveDataObservable mObservableState = new LiveDataObservable();
        this.mObservableState = mObservableState;
        this.mCameraDeviceError = 0;
        this.mReleaseRequestCount = new AtomicInteger(0);
        this.mReleasedCaptureSessions = new LinkedHashMap<CaptureSessionInterface, ListenableFuture<Void>>();
        this.mConfiguringForClose = new HashSet<CaptureSession>();
        this.mNotifyStateAttachedSet = new HashSet<String>();
        this.mCameraConfig = CameraConfigs.emptyConfig();
        this.mLock = new Object();
        this.mIsActiveResumingMode = false;
        this.mCameraManager = mCameraManager;
        this.mCameraStateRegistry = mCameraStateRegistry;
        final ScheduledExecutorService handlerExecutor = CameraXExecutors.newHandlerExecutor(handler);
        this.mScheduledExecutorService = handlerExecutor;
        sequentialExecutor = CameraXExecutors.newSequentialExecutor(sequentialExecutor);
        this.mExecutor = sequentialExecutor;
        this.mStateCallback = new StateCallback(sequentialExecutor, handlerExecutor);
        this.mUseCaseAttachState = new UseCaseAttachState(s);
        mObservableState.postValue(State.CLOSED);
        final CameraStateMachine mCameraStateMachine = new CameraStateMachine(mCameraStateRegistry);
        this.mCameraStateMachine = mCameraStateMachine;
        final CaptureSessionRepository mCaptureSessionRepository = new CaptureSessionRepository(sequentialExecutor);
        this.mCaptureSessionRepository = mCaptureSessionRepository;
        this.mDisplayInfoManager = mDisplayInfoManager;
        this.mCaptureSession = this.newCaptureSession();
        try {
            final Camera2CameraControlImpl mCameraControlInternal = new Camera2CameraControlImpl(mCameraManager.getCameraCharacteristicsCompat(s), handlerExecutor, sequentialExecutor, new ControlUpdateListenerInternal(), mCameraInfoInternal.getCameraQuirks());
            this.mCameraControlInternal = mCameraControlInternal;
            (this.mCameraInfoInternal = mCameraInfoInternal).linkWithCameraControl(mCameraControlInternal);
            mCameraInfoInternal.setCameraStateSource(mCameraStateMachine.getStateLiveData());
            this.mCaptureSessionOpenerBuilder = new SynchronizedCaptureSessionOpener.Builder(sequentialExecutor, handlerExecutor, handler, mCaptureSessionRepository, mCameraInfoInternal.getCameraQuirks(), DeviceQuirks.getAll());
            final CameraAvailability mCameraAvailability = new CameraAvailability(s);
            mCameraStateRegistry.registerCamera(this, sequentialExecutor, (CameraStateRegistry.OnOpenAvailableListener)(this.mCameraAvailability = mCameraAvailability));
            mCameraManager.registerAvailabilityCallback(sequentialExecutor, mCameraAvailability);
        }
        catch (final CameraAccessExceptionCompat cameraAccessExceptionCompat) {
            throw CameraUnavailableExceptionHelper.createFrom(cameraAccessExceptionCompat);
        }
    }
    
    private void addMeteringRepeating() {
        if (this.mMeteringRepeatingSession != null) {
            final UseCaseAttachState mUseCaseAttachState = this.mUseCaseAttachState;
            final StringBuilder sb = new StringBuilder();
            sb.append(this.mMeteringRepeatingSession.getName());
            sb.append(this.mMeteringRepeatingSession.hashCode());
            mUseCaseAttachState.setUseCaseAttached(sb.toString(), this.mMeteringRepeatingSession.getSessionConfig(), this.mMeteringRepeatingSession.getUseCaseConfig());
            final UseCaseAttachState mUseCaseAttachState2 = this.mUseCaseAttachState;
            final StringBuilder sb2 = new StringBuilder();
            sb2.append(this.mMeteringRepeatingSession.getName());
            sb2.append(this.mMeteringRepeatingSession.hashCode());
            mUseCaseAttachState2.setUseCaseActive(sb2.toString(), this.mMeteringRepeatingSession.getSessionConfig(), this.mMeteringRepeatingSession.getUseCaseConfig());
        }
    }
    
    private void addOrRemoveMeteringRepeatingUseCase() {
        final SessionConfig build = this.mUseCaseAttachState.getAttachedBuilder().build();
        final CaptureConfig repeatingCaptureConfig = build.getRepeatingCaptureConfig();
        final int size = repeatingCaptureConfig.getSurfaces().size();
        final int size2 = build.getSurfaces().size();
        if (!build.getSurfaces().isEmpty()) {
            if (repeatingCaptureConfig.getSurfaces().isEmpty()) {
                if (this.mMeteringRepeatingSession == null) {
                    this.mMeteringRepeatingSession = new MeteringRepeatingSession(this.mCameraInfoInternal.getCameraCharacteristicsCompat(), this.mDisplayInfoManager);
                }
                this.addMeteringRepeating();
            }
            else if (size2 == 1 && size == 1) {
                this.removeMeteringRepeating();
            }
            else if (size >= 2) {
                this.removeMeteringRepeating();
            }
            else {
                final StringBuilder sb = new StringBuilder();
                sb.append("mMeteringRepeating is ATTACHED, SessionConfig Surfaces: ");
                sb.append(size2);
                sb.append(", CaptureConfig Surfaces: ");
                sb.append(size);
                Logger.d("Camera2CameraImpl", sb.toString());
            }
        }
    }
    
    private boolean checkAndAttachRepeatingSurface(final CaptureConfig.Builder builder) {
        if (!builder.getSurfaces().isEmpty()) {
            Logger.w("Camera2CameraImpl", "The capture config builder already has surface inside.");
            return false;
        }
        final Iterator<SessionConfig> iterator = this.mUseCaseAttachState.getActiveAndAttachedSessionConfigs().iterator();
        while (iterator.hasNext()) {
            final List<DeferrableSurface> surfaces = iterator.next().getRepeatingCaptureConfig().getSurfaces();
            if (!surfaces.isEmpty()) {
                final Iterator iterator2 = surfaces.iterator();
                while (iterator2.hasNext()) {
                    builder.addSurface((DeferrableSurface)iterator2.next());
                }
            }
        }
        if (builder.getSurfaces().isEmpty()) {
            Logger.w("Camera2CameraImpl", "Unable to find a repeating surface to attach to CaptureConfig");
            return false;
        }
        return true;
    }
    
    private void closeInternal() {
        this.debugLog("Closing camera.");
        final int n = Camera2CameraImpl$3.$SwitchMap$androidx$camera$camera2$internal$Camera2CameraImpl$InternalState[this.mState.ordinal()];
        boolean b = false;
        if (n != 2) {
            if (n != 4) {
                if (n != 5 && n != 6) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("close() ignored due to being in state: ");
                    sb.append(this.mState);
                    this.debugLog(sb.toString());
                }
                else {
                    final boolean cancelScheduledReopen = this.mStateCallback.cancelScheduledReopen();
                    this.setState(InternalState.CLOSING);
                    if (cancelScheduledReopen) {
                        Preconditions.checkState(this.isSessionCloseComplete());
                        this.finishClose();
                    }
                }
            }
            else {
                this.setState(InternalState.CLOSING);
                this.closeCamera(false);
            }
        }
        else {
            if (this.mCameraDevice == null) {
                b = true;
            }
            Preconditions.checkState(b);
            this.setState(InternalState.INITIALIZED);
        }
    }
    
    private void configAndClose(final boolean b) {
        final CaptureSession captureSession = new CaptureSession();
        this.mConfiguringForClose.add(captureSession);
        this.resetCaptureSession(b);
        final SurfaceTexture surfaceTexture = new SurfaceTexture(0);
        surfaceTexture.setDefaultBufferSize(640, 480);
        final Surface surface = new Surface(surfaceTexture);
        final Camera2CameraImpl$$ExternalSyntheticLambda14 camera2CameraImpl$$ExternalSyntheticLambda14 = new Camera2CameraImpl$$ExternalSyntheticLambda14(surface, surfaceTexture);
        final SessionConfig.Builder builder = new SessionConfig.Builder();
        final ImmediateSurface immediateSurface = new ImmediateSurface(surface);
        builder.addNonRepeatingSurface(immediateSurface);
        builder.setTemplateType(1);
        this.debugLog("Start configAndClose.");
        captureSession.open(builder.build(), Preconditions.checkNotNull(this.mCameraDevice), this.mCaptureSessionOpenerBuilder.build()).addListener((Runnable)new Camera2CameraImpl$$ExternalSyntheticLambda15(this, captureSession, immediateSurface, camera2CameraImpl$$ExternalSyntheticLambda14), this.mExecutor);
    }
    
    private CameraDevice$StateCallback createDeviceStateCallback() {
        final ArrayList list = new ArrayList((Collection<? extends E>)this.mUseCaseAttachState.getAttachedBuilder().build().getDeviceStateCallbacks());
        list.add(this.mCaptureSessionRepository.getCameraStateCallback());
        list.add(this.mStateCallback);
        return CameraDeviceStateCallbacks.createComboCallback(list);
    }
    
    private void debugLog(final String s, final Throwable t) {
        Logger.d("Camera2CameraImpl", String.format("{%s} %s", this.toString(), s), t);
    }
    
    static String getErrorMessage(final int n) {
        if (n == 0) {
            return "ERROR_NONE";
        }
        if (n == 1) {
            return "ERROR_CAMERA_IN_USE";
        }
        if (n == 2) {
            return "ERROR_MAX_CAMERAS_IN_USE";
        }
        if (n == 3) {
            return "ERROR_CAMERA_DISABLED";
        }
        if (n == 4) {
            return "ERROR_CAMERA_DEVICE";
        }
        if (n != 5) {
            return "UNKNOWN ERROR";
        }
        return "ERROR_CAMERA_SERVICE";
    }
    
    private ListenableFuture<Void> getOrCreateUserReleaseFuture() {
        if (this.mUserReleaseFuture == null) {
            if (this.mState != InternalState.RELEASED) {
                this.mUserReleaseFuture = CallbackToFutureAdapter.getFuture((CallbackToFutureAdapter.Resolver<Void>)new Camera2CameraImpl$$ExternalSyntheticLambda2(this));
            }
            else {
                this.mUserReleaseFuture = Futures.immediateFuture((Void)null);
            }
        }
        return this.mUserReleaseFuture;
    }
    
    static String getUseCaseId(final UseCase useCase) {
        final StringBuilder sb = new StringBuilder();
        sb.append(useCase.getName());
        sb.append(useCase.hashCode());
        return sb.toString();
    }
    
    private boolean isLegacyDevice() {
        return ((Camera2CameraInfoImpl)this.getCameraInfoInternal()).getSupportedHardwareLevel() == 2;
    }
    
    private CaptureSessionInterface newCaptureSession() {
        synchronized (this.mLock) {
            if (this.mSessionProcessor == null) {
                return new CaptureSession();
            }
            return new ProcessingCaptureSession(this.mSessionProcessor, this.mCameraInfoInternal, this.mExecutor, this.mScheduledExecutorService);
        }
    }
    
    private void notifyStateAttachedToUseCases(final List<UseCase> list) {
        for (final UseCase useCase : list) {
            final String useCaseId = getUseCaseId(useCase);
            if (this.mNotifyStateAttachedSet.contains(useCaseId)) {
                continue;
            }
            this.mNotifyStateAttachedSet.add(useCaseId);
            useCase.onStateAttached();
        }
    }
    
    private void notifyStateDetachedToUseCases(final List<UseCase> list) {
        for (final UseCase useCase : list) {
            final String useCaseId = getUseCaseId(useCase);
            if (!this.mNotifyStateAttachedSet.contains(useCaseId)) {
                continue;
            }
            useCase.onStateDetached();
            this.mNotifyStateAttachedSet.remove(useCaseId);
        }
    }
    
    private void openCameraDevice(final boolean b) {
        if (!b) {
            this.mStateCallback.resetReopenMonitor();
        }
        this.mStateCallback.cancelScheduledReopen();
        this.debugLog("Opening camera.");
        this.setState(InternalState.OPENING);
        try {
            this.mCameraManager.openCamera(this.mCameraInfoInternal.getCameraId(), this.mExecutor, this.createDeviceStateCallback());
        }
        catch (final SecurityException ex) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Unable to open camera due to ");
            sb.append(ex.getMessage());
            this.debugLog(sb.toString());
            this.setState(InternalState.REOPENING);
            this.mStateCallback.scheduleCameraReopen();
        }
        catch (final CameraAccessExceptionCompat cameraAccessExceptionCompat) {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("Unable to open camera due to ");
            sb2.append(cameraAccessExceptionCompat.getMessage());
            this.debugLog(sb2.toString());
            if (cameraAccessExceptionCompat.getReason() == 10001) {
                this.setState(InternalState.INITIALIZED, CameraState.StateError.create(7, cameraAccessExceptionCompat));
            }
        }
    }
    
    private void openInternal() {
        final int n = Camera2CameraImpl$3.$SwitchMap$androidx$camera$camera2$internal$Camera2CameraImpl$InternalState[this.mState.ordinal()];
        boolean b = false;
        if (n != 1 && n != 2) {
            if (n != 3) {
                final StringBuilder sb = new StringBuilder();
                sb.append("open() ignored due to being in state: ");
                sb.append(this.mState);
                this.debugLog(sb.toString());
            }
            else {
                this.setState(InternalState.REOPENING);
                if (!this.isSessionCloseComplete() && this.mCameraDeviceError == 0) {
                    if (this.mCameraDevice != null) {
                        b = true;
                    }
                    Preconditions.checkState(b, "Camera Device should be open if session close is not complete");
                    this.setState(InternalState.OPENED);
                    this.openCaptureSession();
                }
            }
        }
        else {
            this.tryForceOpenCameraDevice(false);
        }
    }
    
    private ListenableFuture<Void> releaseInternal() {
        final ListenableFuture<Void> orCreateUserReleaseFuture = this.getOrCreateUserReleaseFuture();
        final int n = Camera2CameraImpl$3.$SwitchMap$androidx$camera$camera2$internal$Camera2CameraImpl$InternalState[this.mState.ordinal()];
        boolean b = false;
        switch (n) {
            default: {
                final StringBuilder sb = new StringBuilder();
                sb.append("release() ignored due to being in state: ");
                sb.append(this.mState);
                this.debugLog(sb.toString());
                break;
            }
            case 4: {
                this.setState(InternalState.RELEASING);
                this.closeCamera(false);
                break;
            }
            case 3:
            case 5:
            case 6:
            case 7: {
                final boolean cancelScheduledReopen = this.mStateCallback.cancelScheduledReopen();
                this.setState(InternalState.RELEASING);
                if (cancelScheduledReopen) {
                    Preconditions.checkState(this.isSessionCloseComplete());
                    this.finishClose();
                    break;
                }
                break;
            }
            case 1:
            case 2: {
                if (this.mCameraDevice == null) {
                    b = true;
                }
                Preconditions.checkState(b);
                this.setState(InternalState.RELEASING);
                Preconditions.checkState(this.isSessionCloseComplete());
                this.finishClose();
                break;
            }
        }
        return orCreateUserReleaseFuture;
    }
    
    private void removeMeteringRepeating() {
        if (this.mMeteringRepeatingSession != null) {
            final UseCaseAttachState mUseCaseAttachState = this.mUseCaseAttachState;
            final StringBuilder sb = new StringBuilder();
            sb.append(this.mMeteringRepeatingSession.getName());
            sb.append(this.mMeteringRepeatingSession.hashCode());
            mUseCaseAttachState.setUseCaseDetached(sb.toString());
            final UseCaseAttachState mUseCaseAttachState2 = this.mUseCaseAttachState;
            final StringBuilder sb2 = new StringBuilder();
            sb2.append(this.mMeteringRepeatingSession.getName());
            sb2.append(this.mMeteringRepeatingSession.hashCode());
            mUseCaseAttachState2.setUseCaseInactive(sb2.toString());
            this.mMeteringRepeatingSession.clear();
            this.mMeteringRepeatingSession = null;
        }
    }
    
    private Collection<UseCaseInfo> toUseCaseInfos(final Collection<UseCase> collection) {
        final ArrayList list = new ArrayList();
        final Iterator<UseCase> iterator = collection.iterator();
        while (iterator.hasNext()) {
            list.add(UseCaseInfo.from(iterator.next()));
        }
        return list;
    }
    
    private void tryAttachUseCases(final Collection<UseCaseInfo> collection) {
        final boolean empty = this.mUseCaseAttachState.getAttachedSessionConfigs().isEmpty();
        final ArrayList list = new ArrayList();
        final Iterator<UseCaseInfo> iterator = collection.iterator();
        Rational previewAspectRatio = null;
        while (iterator.hasNext()) {
            final UseCaseInfo useCaseInfo = iterator.next();
            if (!this.mUseCaseAttachState.isUseCaseAttached(useCaseInfo.getUseCaseId())) {
                this.mUseCaseAttachState.setUseCaseAttached(useCaseInfo.getUseCaseId(), useCaseInfo.getSessionConfig(), useCaseInfo.getUseCaseConfig());
                list.add(useCaseInfo.getUseCaseId());
                if (useCaseInfo.getUseCaseType() != Preview.class) {
                    continue;
                }
                final Size surfaceResolution = useCaseInfo.getSurfaceResolution();
                if (surfaceResolution == null) {
                    continue;
                }
                previewAspectRatio = new Rational(surfaceResolution.getWidth(), surfaceResolution.getHeight());
            }
        }
        if (list.isEmpty()) {
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Use cases [");
        sb.append(TextUtils.join((CharSequence)", ", (Iterable)list));
        sb.append("] now ATTACHED");
        this.debugLog(sb.toString());
        if (empty) {
            this.mCameraControlInternal.setActive(true);
            this.mCameraControlInternal.incrementUseCount();
        }
        this.addOrRemoveMeteringRepeatingUseCase();
        this.updateZslDisabledByUseCaseConfigStatus();
        this.updateCaptureSessionConfig();
        this.resetCaptureSession(false);
        if (this.mState == InternalState.OPENED) {
            this.openCaptureSession();
        }
        else {
            this.openInternal();
        }
        if (previewAspectRatio != null) {
            this.mCameraControlInternal.setPreviewAspectRatio(previewAspectRatio);
        }
    }
    
    private void tryDetachUseCases(final Collection<UseCaseInfo> collection) {
        final ArrayList list = new ArrayList();
        final Iterator<UseCaseInfo> iterator = collection.iterator();
        boolean b = false;
        while (iterator.hasNext()) {
            final UseCaseInfo useCaseInfo = iterator.next();
            if (this.mUseCaseAttachState.isUseCaseAttached(useCaseInfo.getUseCaseId())) {
                this.mUseCaseAttachState.removeUseCase(useCaseInfo.getUseCaseId());
                list.add(useCaseInfo.getUseCaseId());
                if (useCaseInfo.getUseCaseType() != Preview.class) {
                    continue;
                }
                b = true;
            }
        }
        if (list.isEmpty()) {
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Use cases [");
        sb.append(TextUtils.join((CharSequence)", ", (Iterable)list));
        sb.append("] now DETACHED for camera");
        this.debugLog(sb.toString());
        if (b) {
            this.mCameraControlInternal.setPreviewAspectRatio(null);
        }
        this.addOrRemoveMeteringRepeatingUseCase();
        if (this.mUseCaseAttachState.getAttachedUseCaseConfigs().isEmpty()) {
            this.mCameraControlInternal.setZslDisabledByUserCaseConfig(false);
        }
        else {
            this.updateZslDisabledByUseCaseConfigStatus();
        }
        if (this.mUseCaseAttachState.getAttachedSessionConfigs().isEmpty()) {
            this.mCameraControlInternal.decrementUseCount();
            this.resetCaptureSession(false);
            this.mCameraControlInternal.setActive(false);
            this.mCaptureSession = this.newCaptureSession();
            this.closeInternal();
        }
        else {
            this.updateCaptureSessionConfig();
            this.resetCaptureSession(false);
            if (this.mState == InternalState.OPENED) {
                this.openCaptureSession();
            }
        }
    }
    
    private void updateZslDisabledByUseCaseConfigStatus() {
        final Iterator<UseCaseConfig<?>> iterator = this.mUseCaseAttachState.getAttachedUseCaseConfigs().iterator();
        boolean zslDisabledByUserCaseConfig = false;
        while (iterator.hasNext()) {
            zslDisabledByUserCaseConfig |= iterator.next().isZslDisabled(false);
        }
        this.mCameraControlInternal.setZslDisabledByUserCaseConfig(zslDisabledByUserCaseConfig);
    }
    
    @Override
    public void attachUseCases(final Collection<UseCase> c) {
        final ArrayList c2 = new ArrayList((Collection<? extends E>)c);
        if (c2.isEmpty()) {
            return;
        }
        this.mCameraControlInternal.incrementUseCount();
        this.notifyStateAttachedToUseCases(new ArrayList<UseCase>(c2));
        final ArrayList list = new ArrayList(this.toUseCaseInfos(c2));
        try {
            this.mExecutor.execute(new Camera2CameraImpl$$ExternalSyntheticLambda13(this, list));
        }
        catch (final RejectedExecutionException ex) {
            this.debugLog("Unable to attach use cases.", ex);
            this.mCameraControlInternal.decrementUseCount();
        }
    }
    
    @Override
    public void close() {
        this.mExecutor.execute(new Camera2CameraImpl$$ExternalSyntheticLambda12(this));
    }
    
    void closeCamera(final boolean b) {
        final boolean b2 = this.mState == InternalState.CLOSING || this.mState == InternalState.RELEASING || (this.mState == InternalState.REOPENING && this.mCameraDeviceError != 0);
        final StringBuilder sb = new StringBuilder();
        sb.append("closeCamera should only be called in a CLOSING, RELEASING or REOPENING (with error) state. Current state: ");
        sb.append(this.mState);
        sb.append(" (error: ");
        sb.append(getErrorMessage(this.mCameraDeviceError));
        sb.append(")");
        Preconditions.checkState(b2, sb.toString());
        if (Build$VERSION.SDK_INT > 23 && Build$VERSION.SDK_INT < 29 && this.isLegacyDevice() && this.mCameraDeviceError == 0) {
            this.configAndClose(b);
        }
        else {
            this.resetCaptureSession(b);
        }
        this.mCaptureSession.cancelIssuedCaptureRequests();
    }
    
    void debugLog(final String s) {
        this.debugLog(s, null);
    }
    
    @Override
    public void detachUseCases(final Collection<UseCase> c) {
        final ArrayList c2 = new ArrayList((Collection<? extends E>)c);
        if (c2.isEmpty()) {
            return;
        }
        final ArrayList list = new ArrayList(this.toUseCaseInfos(c2));
        this.notifyStateDetachedToUseCases(new ArrayList<UseCase>(c2));
        this.mExecutor.execute(new Camera2CameraImpl$$ExternalSyntheticLambda11(this, list));
    }
    
    SessionConfig findSessionConfigForSurface(final DeferrableSurface deferrableSurface) {
        for (final SessionConfig sessionConfig : this.mUseCaseAttachState.getAttachedSessionConfigs()) {
            if (sessionConfig.getSurfaces().contains(deferrableSurface)) {
                return sessionConfig;
            }
        }
        return null;
    }
    
    void finishClose() {
        Preconditions.checkState(this.mState == InternalState.RELEASING || this.mState == InternalState.CLOSING);
        Preconditions.checkState(this.mReleasedCaptureSessions.isEmpty());
        this.mCameraDevice = null;
        if (this.mState == InternalState.CLOSING) {
            this.setState(InternalState.INITIALIZED);
        }
        else {
            this.mCameraManager.unregisterAvailabilityCallback(this.mCameraAvailability);
            this.setState(InternalState.RELEASED);
            final CallbackToFutureAdapter.Completer<Void> mUserReleaseNotifier = this.mUserReleaseNotifier;
            if (mUserReleaseNotifier != null) {
                mUserReleaseNotifier.set(null);
                this.mUserReleaseNotifier = null;
            }
        }
    }
    
    public CameraAvailability getCameraAvailability() {
        return this.mCameraAvailability;
    }
    
    @Override
    public CameraControlInternal getCameraControlInternal() {
        return this.mCameraControlInternal;
    }
    
    @Override
    public CameraInfoInternal getCameraInfoInternal() {
        return this.mCameraInfoInternal;
    }
    
    @Override
    public Observable<State> getCameraState() {
        return this.mObservableState;
    }
    
    @Override
    public CameraConfig getExtendedConfig() {
        return this.mCameraConfig;
    }
    
    boolean isSessionCloseComplete() {
        return this.mReleasedCaptureSessions.isEmpty() && this.mConfiguringForClose.isEmpty();
    }
    
    boolean isUseCaseAttached(UseCase cause) {
        try {
            cause = (ExecutionException)new Camera2CameraImpl$$ExternalSyntheticLambda9(this, getUseCaseId((UseCase)cause));
            return (boolean)CallbackToFutureAdapter.getFuture((CallbackToFutureAdapter.Resolver<Object>)cause).get();
        }
        catch (final ExecutionException cause) {}
        catch (final InterruptedException ex) {}
        throw new RuntimeException("Unable to check if use case is attached.", cause);
    }
    
    @Override
    public void onUseCaseActive(final UseCase useCase) {
        Preconditions.checkNotNull(useCase);
        this.mExecutor.execute(new Camera2CameraImpl$$ExternalSyntheticLambda5(this, getUseCaseId(useCase), useCase.getSessionConfig(), useCase.getCurrentConfig()));
    }
    
    @Override
    public void onUseCaseInactive(final UseCase useCase) {
        Preconditions.checkNotNull(useCase);
        this.mExecutor.execute(new Camera2CameraImpl$$ExternalSyntheticLambda0(this, getUseCaseId(useCase)));
    }
    
    @Override
    public void onUseCaseReset(final UseCase useCase) {
        Preconditions.checkNotNull(useCase);
        this.mExecutor.execute(new Camera2CameraImpl$$ExternalSyntheticLambda16(this, getUseCaseId(useCase), useCase.getSessionConfig(), useCase.getCurrentConfig()));
    }
    
    @Override
    public void onUseCaseUpdated(final UseCase useCase) {
        Preconditions.checkNotNull(useCase);
        this.mExecutor.execute(new Camera2CameraImpl$$ExternalSyntheticLambda8(this, getUseCaseId(useCase), useCase.getSessionConfig(), useCase.getCurrentConfig()));
    }
    
    @Override
    public void open() {
        this.mExecutor.execute(new Camera2CameraImpl$$ExternalSyntheticLambda10(this));
    }
    
    void openCaptureSession() {
        Preconditions.checkState(this.mState == InternalState.OPENED);
        final SessionConfig.ValidatingBuilder attachedBuilder = this.mUseCaseAttachState.getAttachedBuilder();
        if (!attachedBuilder.isValid()) {
            this.debugLog("Unable to create capture session due to conflicting configurations");
            return;
        }
        if (!attachedBuilder.build().getImplementationOptions().containsOption((Config.Option<?>)Camera2ImplConfig.STREAM_USE_CASE_OPTION)) {
            attachedBuilder.addImplementationOption(Camera2ImplConfig.STREAM_USE_CASE_OPTION, StreamUseCaseUtil.getStreamUseCaseFromUseCaseConfigs(this.mUseCaseAttachState.getAttachedUseCaseConfigs(), this.mUseCaseAttachState.getAttachedSessionConfigs()));
        }
        Futures.addCallback(this.mCaptureSession.open(attachedBuilder.build(), Preconditions.checkNotNull(this.mCameraDevice), this.mCaptureSessionOpenerBuilder.build()), new FutureCallback<Void>(this) {
            final Camera2CameraImpl this$0;
            
            @Override
            public void onFailure(final Throwable t) {
                if (t instanceof DeferrableSurface.SurfaceClosedException) {
                    final SessionConfig sessionConfigForSurface = this.this$0.findSessionConfigForSurface(((DeferrableSurface.SurfaceClosedException)t).getDeferrableSurface());
                    if (sessionConfigForSurface != null) {
                        this.this$0.postSurfaceClosedError(sessionConfigForSurface);
                    }
                    return;
                }
                if (t instanceof CancellationException) {
                    this.this$0.debugLog("Unable to configure camera cancelled");
                    return;
                }
                if (this.this$0.mState == InternalState.OPENED) {
                    this.this$0.setState(InternalState.OPENED, CameraState.StateError.create(4, t));
                }
                if (t instanceof CameraAccessException) {
                    final Camera2CameraImpl this$0 = this.this$0;
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Unable to configure camera due to ");
                    sb.append(t.getMessage());
                    this$0.debugLog(sb.toString());
                }
                else if (t instanceof TimeoutException) {
                    final StringBuilder sb2 = new StringBuilder();
                    sb2.append("Unable to configure camera ");
                    sb2.append(this.this$0.mCameraInfoInternal.getCameraId());
                    sb2.append(", timeout!");
                    Logger.e("Camera2CameraImpl", sb2.toString());
                }
            }
            
            @Override
            public void onSuccess(final Void void1) {
            }
        }, this.mExecutor);
    }
    
    void postSurfaceClosedError(final SessionConfig sessionConfig) {
        final ScheduledExecutorService mainThreadExecutor = CameraXExecutors.mainThreadExecutor();
        final List<SessionConfig.ErrorListener> errorListeners = sessionConfig.getErrorListeners();
        if (!errorListeners.isEmpty()) {
            final SessionConfig.ErrorListener errorListener = errorListeners.get(0);
            this.debugLog("Posting surface closed", new Throwable());
            mainThreadExecutor.execute(new Camera2CameraImpl$$ExternalSyntheticLambda6(errorListener, sessionConfig));
        }
    }
    
    @Override
    public ListenableFuture<Void> release() {
        return CallbackToFutureAdapter.getFuture((CallbackToFutureAdapter.Resolver<Void>)new Camera2CameraImpl$$ExternalSyntheticLambda4(this));
    }
    
    void releaseNoOpSession(final CaptureSession captureSession, final DeferrableSurface deferrableSurface, final Runnable runnable) {
        this.mConfiguringForClose.remove(captureSession);
        final ListenableFuture<Void> releaseSession = this.releaseSession(captureSession, false);
        deferrableSurface.close();
        Futures.successfulAsList((Collection<? extends com.google.common.util.concurrent.ListenableFuture<?>>)Arrays.asList(releaseSession, deferrableSurface.getTerminationFuture())).addListener(runnable, CameraXExecutors.directExecutor());
    }
    
    ListenableFuture<Void> releaseSession(final CaptureSessionInterface captureSessionInterface, final boolean b) {
        captureSessionInterface.close();
        final ListenableFuture<Void> release = captureSessionInterface.release(b);
        final StringBuilder sb = new StringBuilder();
        sb.append("Releasing session in state ");
        sb.append(this.mState.name());
        this.debugLog(sb.toString());
        this.mReleasedCaptureSessions.put(captureSessionInterface, release);
        Futures.addCallback((com.google.common.util.concurrent.ListenableFuture<Object>)release, (FutureCallback<? super Object>)new FutureCallback<Void>(this, captureSessionInterface) {
            final Camera2CameraImpl this$0;
            final CaptureSessionInterface val$captureSession;
            
            @Override
            public void onFailure(final Throwable t) {
            }
            
            @Override
            public void onSuccess(final Void void1) {
                this.this$0.mReleasedCaptureSessions.remove(this.val$captureSession);
                final int n = Camera2CameraImpl$3.$SwitchMap$androidx$camera$camera2$internal$Camera2CameraImpl$InternalState[this.this$0.mState.ordinal()];
                if (n != 3) {
                    if (n != 6) {
                        if (n != 7) {
                            return;
                        }
                    }
                    else if (this.this$0.mCameraDeviceError == 0) {
                        return;
                    }
                }
                if (this.this$0.isSessionCloseComplete() && this.this$0.mCameraDevice != null) {
                    ApiCompat.Api21Impl.close(this.this$0.mCameraDevice);
                    this.this$0.mCameraDevice = null;
                }
            }
        }, CameraXExecutors.directExecutor());
        return release;
    }
    
    void resetCaptureSession(final boolean b) {
        Preconditions.checkState(this.mCaptureSession != null);
        this.debugLog("Resetting Capture Session");
        final CaptureSessionInterface mCaptureSession = this.mCaptureSession;
        final SessionConfig sessionConfig = mCaptureSession.getSessionConfig();
        final List<CaptureConfig> captureConfigs = mCaptureSession.getCaptureConfigs();
        (this.mCaptureSession = this.newCaptureSession()).setSessionConfig(sessionConfig);
        this.mCaptureSession.issueCaptureRequests(captureConfigs);
        this.releaseSession(mCaptureSession, b);
    }
    
    @Override
    public void setActiveResumingMode(final boolean b) {
        this.mExecutor.execute(new Camera2CameraImpl$$ExternalSyntheticLambda1(this, b));
    }
    
    @Override
    public void setExtendedConfig(final CameraConfig cameraConfig) {
        CameraConfig emptyConfig = cameraConfig;
        if (cameraConfig == null) {
            emptyConfig = CameraConfigs.emptyConfig();
        }
        final SessionProcessor sessionProcessor = emptyConfig.getSessionProcessor(null);
        this.mCameraConfig = emptyConfig;
        synchronized (this.mLock) {
            this.mSessionProcessor = sessionProcessor;
        }
    }
    
    void setState(final InternalState internalState) {
        this.setState(internalState, null);
    }
    
    void setState(final InternalState internalState, final CameraState.StateError stateError) {
        this.setState(internalState, stateError, true);
    }
    
    void setState(final InternalState obj, final CameraState.StateError stateError, final boolean b) {
        final StringBuilder sb = new StringBuilder();
        sb.append("Transitioning camera internal state: ");
        sb.append(this.mState);
        sb.append(" --> ");
        sb.append(obj);
        this.debugLog(sb.toString());
        this.mState = obj;
        State state = null;
        switch (Camera2CameraImpl$3.$SwitchMap$androidx$camera$camera2$internal$Camera2CameraImpl$InternalState[obj.ordinal()]) {
            default: {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("Unknown state: ");
                sb2.append(obj);
                throw new IllegalStateException(sb2.toString());
            }
            case 8: {
                state = State.RELEASED;
                break;
            }
            case 7: {
                state = State.RELEASING;
                break;
            }
            case 5:
            case 6: {
                state = State.OPENING;
                break;
            }
            case 4: {
                state = State.OPEN;
                break;
            }
            case 3: {
                state = State.CLOSING;
                break;
            }
            case 2: {
                state = State.PENDING_OPEN;
                break;
            }
            case 1: {
                state = State.CLOSED;
                break;
            }
        }
        this.mCameraStateRegistry.markCameraState(this, state, b);
        this.mObservableState.postValue(state);
        this.mCameraStateMachine.updateState(state, stateError);
    }
    
    void submitCaptureRequests(final List<CaptureConfig> list) {
        final ArrayList list2 = new ArrayList();
        for (final CaptureConfig captureConfig : list) {
            final CaptureConfig.Builder from = CaptureConfig.Builder.from(captureConfig);
            if (captureConfig.getTemplateType() == 5 && captureConfig.getCameraCaptureResult() != null) {
                from.setCameraCaptureResult(captureConfig.getCameraCaptureResult());
            }
            if (captureConfig.getSurfaces().isEmpty() && captureConfig.isUseRepeatingSurface() && !this.checkAndAttachRepeatingSurface(from)) {
                continue;
            }
            list2.add(from.build());
        }
        this.debugLog("Issue capture request");
        this.mCaptureSession.issueCaptureRequests(list2);
    }
    
    @Override
    public String toString() {
        return String.format(Locale.US, "Camera@%x[id=%s]", this.hashCode(), this.mCameraInfoInternal.getCameraId());
    }
    
    void tryForceOpenCameraDevice(final boolean b) {
        this.debugLog("Attempting to force open the camera.");
        if (!this.mCameraStateRegistry.tryOpenCamera(this)) {
            this.debugLog("No cameras available. Waiting for available camera before opening camera.");
            this.setState(InternalState.PENDING_OPEN);
            return;
        }
        this.openCameraDevice(b);
    }
    
    void tryOpenCameraDevice(final boolean b) {
        this.debugLog("Attempting to open the camera.");
        if (!this.mCameraAvailability.isCameraAvailable() || !this.mCameraStateRegistry.tryOpenCamera(this)) {
            this.debugLog("No cameras available. Waiting for available camera before opening camera.");
            this.setState(InternalState.PENDING_OPEN);
            return;
        }
        this.openCameraDevice(b);
    }
    
    void updateCaptureSessionConfig() {
        final SessionConfig.ValidatingBuilder activeAndAttachedBuilder = this.mUseCaseAttachState.getActiveAndAttachedBuilder();
        if (activeAndAttachedBuilder.isValid()) {
            this.mCameraControlInternal.setTemplate(activeAndAttachedBuilder.build().getTemplateType());
            activeAndAttachedBuilder.add(this.mCameraControlInternal.getSessionConfig());
            this.mCaptureSession.setSessionConfig(activeAndAttachedBuilder.build());
        }
        else {
            this.mCameraControlInternal.resetTemplate();
            this.mCaptureSession.setSessionConfig(this.mCameraControlInternal.getSessionConfig());
        }
    }
    
    final class CameraAvailability extends CameraManager$AvailabilityCallback implements OnOpenAvailableListener
    {
        private boolean mCameraAvailable;
        private final String mCameraId;
        final Camera2CameraImpl this$0;
        
        CameraAvailability(final Camera2CameraImpl this$0, final String mCameraId) {
            this.this$0 = this$0;
            this.mCameraAvailable = true;
            this.mCameraId = mCameraId;
        }
        
        boolean isCameraAvailable() {
            return this.mCameraAvailable;
        }
        
        public void onCameraAvailable(final String anObject) {
            if (!this.mCameraId.equals(anObject)) {
                return;
            }
            this.mCameraAvailable = true;
            if (this.this$0.mState == InternalState.PENDING_OPEN) {
                this.this$0.tryOpenCameraDevice(false);
            }
        }
        
        public void onCameraUnavailable(final String anObject) {
            if (!this.mCameraId.equals(anObject)) {
                return;
            }
            this.mCameraAvailable = false;
        }
        
        public void onOpenAvailable() {
            if (this.this$0.mState == InternalState.PENDING_OPEN) {
                this.this$0.tryOpenCameraDevice(false);
            }
        }
    }
    
    final class ControlUpdateListenerInternal implements ControlUpdateCallback
    {
        final Camera2CameraImpl this$0;
        
        ControlUpdateListenerInternal(final Camera2CameraImpl this$0) {
            this.this$0 = this$0;
        }
        
        @Override
        public void onCameraControlCaptureRequests(final List<CaptureConfig> list) {
            this.this$0.submitCaptureRequests(Preconditions.checkNotNull(list));
        }
        
        @Override
        public void onCameraControlUpdateSessionConfig() {
            this.this$0.updateCaptureSessionConfig();
        }
    }
    
    enum InternalState
    {
        private static final InternalState[] $VALUES;
        
        CLOSING, 
        INITIALIZED, 
        OPENED, 
        OPENING, 
        PENDING_OPEN, 
        RELEASED, 
        RELEASING, 
        REOPENING;
    }
    
    final class StateCallback extends CameraDevice$StateCallback
    {
        private final CameraReopenMonitor mCameraReopenMonitor;
        private final Executor mExecutor;
        ScheduledFuture<?> mScheduledReopenHandle;
        private ScheduledReopen mScheduledReopenRunnable;
        private final ScheduledExecutorService mScheduler;
        final Camera2CameraImpl this$0;
        
        StateCallback(final Camera2CameraImpl this$0, final Executor mExecutor, final ScheduledExecutorService mScheduler) {
            this.this$0 = this$0;
            this.mCameraReopenMonitor = new CameraReopenMonitor();
            this.mExecutor = mExecutor;
            this.mScheduler = mScheduler;
        }
        
        private void handleErrorOnOpen(final CameraDevice cameraDevice, int n) {
            final boolean b = this.this$0.mState == InternalState.OPENING || this.this$0.mState == InternalState.OPENED || this.this$0.mState == InternalState.REOPENING;
            final StringBuilder sb = new StringBuilder();
            sb.append("Attempt to handle open error from non open state: ");
            sb.append(this.this$0.mState);
            Preconditions.checkState(b, sb.toString());
            if (n != 1 && n != 2 && n != 4) {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("Error observed on open (or opening) camera device ");
                sb2.append(cameraDevice.getId());
                sb2.append(": ");
                sb2.append(Camera2CameraImpl.getErrorMessage(n));
                sb2.append(" closing camera.");
                Logger.e("Camera2CameraImpl", sb2.toString());
                if (n == 3) {
                    n = 5;
                }
                else {
                    n = 6;
                }
                this.this$0.setState(InternalState.CLOSING, CameraState.StateError.create(n));
                this.this$0.closeCamera(false);
            }
            else {
                Logger.d("Camera2CameraImpl", String.format("Attempt to reopen camera[%s] after error[%s]", cameraDevice.getId(), Camera2CameraImpl.getErrorMessage(n)));
                this.reopenCameraAfterError(n);
            }
        }
        
        private void reopenCameraAfterError(final int n) {
            final int mCameraDeviceError = this.this$0.mCameraDeviceError;
            int n2 = 1;
            Preconditions.checkState(mCameraDeviceError != 0, "Can only reopen camera device after error if the camera device is actually in an error state.");
            if (n != 1) {
                if (n != 2) {
                    n2 = 3;
                }
            }
            else {
                n2 = 2;
            }
            this.this$0.setState(InternalState.REOPENING, CameraState.StateError.create(n2));
            this.this$0.closeCamera(false);
        }
        
        boolean cancelScheduledReopen() {
            final ScheduledFuture<?> mScheduledReopenHandle = this.mScheduledReopenHandle;
            boolean b = false;
            if (mScheduledReopenHandle != null) {
                final Camera2CameraImpl this$0 = this.this$0;
                final StringBuilder sb = new StringBuilder();
                sb.append("Cancelling scheduled re-open: ");
                sb.append(this.mScheduledReopenRunnable);
                this$0.debugLog(sb.toString());
                this.mScheduledReopenRunnable.cancel();
                this.mScheduledReopenRunnable = null;
                this.mScheduledReopenHandle.cancel(false);
                this.mScheduledReopenHandle = null;
                b = true;
            }
            return b;
        }
        
        public void onClosed(final CameraDevice obj) {
            this.this$0.debugLog("CameraDevice.onClosed()");
            final boolean b = this.this$0.mCameraDevice == null;
            final StringBuilder sb = new StringBuilder();
            sb.append("Unexpected onClose callback on camera device: ");
            sb.append(obj);
            Preconditions.checkState(b, sb.toString());
            final int n = Camera2CameraImpl$3.$SwitchMap$androidx$camera$camera2$internal$Camera2CameraImpl$InternalState[this.this$0.mState.ordinal()];
            if (n != 3) {
                if (n != 6) {
                    if (n != 7) {
                        final StringBuilder sb2 = new StringBuilder();
                        sb2.append("Camera closed while in state: ");
                        sb2.append(this.this$0.mState);
                        throw new IllegalStateException(sb2.toString());
                    }
                }
                else {
                    if (this.this$0.mCameraDeviceError != 0) {
                        final Camera2CameraImpl this$0 = this.this$0;
                        final StringBuilder sb3 = new StringBuilder();
                        sb3.append("Camera closed due to error: ");
                        sb3.append(Camera2CameraImpl.getErrorMessage(this.this$0.mCameraDeviceError));
                        this$0.debugLog(sb3.toString());
                        this.scheduleCameraReopen();
                        return;
                    }
                    this.this$0.tryOpenCameraDevice(false);
                    return;
                }
            }
            Preconditions.checkState(this.this$0.isSessionCloseComplete());
            this.this$0.finishClose();
        }
        
        public void onDisconnected(final CameraDevice cameraDevice) {
            this.this$0.debugLog("CameraDevice.onDisconnected()");
            this.onError(cameraDevice, 1);
        }
        
        public void onError(final CameraDevice mCameraDevice, final int mCameraDeviceError) {
            this.this$0.mCameraDevice = mCameraDevice;
            this.this$0.mCameraDeviceError = mCameraDeviceError;
            final int n = Camera2CameraImpl$3.$SwitchMap$androidx$camera$camera2$internal$Camera2CameraImpl$InternalState[this.this$0.mState.ordinal()];
            if (n != 3) {
                if (n == 4 || n == 5 || n == 6) {
                    Logger.d("Camera2CameraImpl", String.format("CameraDevice.onError(): %s failed with %s while in %s state. Will attempt recovering from error.", mCameraDevice.getId(), Camera2CameraImpl.getErrorMessage(mCameraDeviceError), this.this$0.mState.name()));
                    this.handleErrorOnOpen(mCameraDevice, mCameraDeviceError);
                    return;
                }
                if (n != 7) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("onError() should not be possible from state: ");
                    sb.append(this.this$0.mState);
                    throw new IllegalStateException(sb.toString());
                }
            }
            Logger.e("Camera2CameraImpl", String.format("CameraDevice.onError(): %s failed with %s while in %s state. Will finish closing camera.", mCameraDevice.getId(), Camera2CameraImpl.getErrorMessage(mCameraDeviceError), this.this$0.mState.name()));
            this.this$0.closeCamera(false);
        }
        
        public void onOpened(final CameraDevice mCameraDevice) {
            this.this$0.debugLog("CameraDevice.onOpened()");
            this.this$0.mCameraDevice = mCameraDevice;
            this.this$0.mCameraDeviceError = 0;
            this.resetReopenMonitor();
            final int n = Camera2CameraImpl$3.$SwitchMap$androidx$camera$camera2$internal$Camera2CameraImpl$InternalState[this.this$0.mState.ordinal()];
            if (n != 3) {
                if (n == 5 || n == 6) {
                    this.this$0.setState(InternalState.OPENED);
                    this.this$0.openCaptureSession();
                    return;
                }
                if (n != 7) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("onOpened() should not be possible from state: ");
                    sb.append(this.this$0.mState);
                    throw new IllegalStateException(sb.toString());
                }
            }
            Preconditions.checkState(this.this$0.isSessionCloseComplete());
            this.this$0.mCameraDevice.close();
            this.this$0.mCameraDevice = null;
        }
        
        void resetReopenMonitor() {
            this.mCameraReopenMonitor.reset();
        }
        
        void scheduleCameraReopen() {
            final ScheduledReopen mScheduledReopenRunnable = this.mScheduledReopenRunnable;
            final boolean b = true;
            Preconditions.checkState(mScheduledReopenRunnable == null);
            Preconditions.checkState(this.mScheduledReopenHandle == null && b);
            if (this.mCameraReopenMonitor.canScheduleCameraReopen()) {
                this.mScheduledReopenRunnable = new ScheduledReopen(this.mExecutor);
                final Camera2CameraImpl this$0 = this.this$0;
                final StringBuilder sb = new StringBuilder();
                sb.append("Attempting camera re-open in ");
                sb.append(this.mCameraReopenMonitor.getReopenDelayMs());
                sb.append("ms: ");
                sb.append(this.mScheduledReopenRunnable);
                sb.append(" activeResuming = ");
                sb.append(this.this$0.mIsActiveResumingMode);
                this$0.debugLog(sb.toString());
                this.mScheduledReopenHandle = this.mScheduler.schedule(this.mScheduledReopenRunnable, this.mCameraReopenMonitor.getReopenDelayMs(), TimeUnit.MILLISECONDS);
            }
            else {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("Camera reopening attempted for ");
                sb2.append(this.mCameraReopenMonitor.getReopenLimitMs());
                sb2.append("ms without success.");
                Logger.e("Camera2CameraImpl", sb2.toString());
                this.this$0.setState(InternalState.PENDING_OPEN, null, false);
            }
        }
        
        boolean shouldActiveResume() {
            final boolean mIsActiveResumingMode = this.this$0.mIsActiveResumingMode;
            final boolean b = true;
            if (mIsActiveResumingMode) {
                boolean b2 = b;
                if (this.this$0.mCameraDeviceError == 1) {
                    return b2;
                }
                if (this.this$0.mCameraDeviceError == 2) {
                    b2 = b;
                    return b2;
                }
            }
            return false;
        }
        
        class CameraReopenMonitor
        {
            static final int ACTIVE_REOPEN_DELAY_BASE_MS = 1000;
            static final int ACTIVE_REOPEN_LIMIT_MS = 1800000;
            static final int INVALID_TIME = -1;
            static final int REOPEN_DELAY_MS = 700;
            static final int REOPEN_LIMIT_MS = 10000;
            private long mFirstReopenTime;
            final StateCallback this$1;
            
            CameraReopenMonitor(final StateCallback this$1) {
                this.this$1 = this$1;
                this.mFirstReopenTime = -1L;
            }
            
            boolean canScheduleCameraReopen() {
                if (this.getElapsedTime() >= this.getReopenLimitMs()) {
                    this.reset();
                    return false;
                }
                return true;
            }
            
            long getElapsedTime() {
                final long uptimeMillis = SystemClock.uptimeMillis();
                if (this.mFirstReopenTime == -1L) {
                    this.mFirstReopenTime = uptimeMillis;
                }
                return uptimeMillis - this.mFirstReopenTime;
            }
            
            int getReopenDelayMs() {
                if (!this.this$1.shouldActiveResume()) {
                    return 700;
                }
                final long elapsedTime = this.getElapsedTime();
                if (elapsedTime <= 120000L) {
                    return 1000;
                }
                if (elapsedTime <= 300000L) {
                    return 2000;
                }
                return 4000;
            }
            
            int getReopenLimitMs() {
                if (!this.this$1.shouldActiveResume()) {
                    return 10000;
                }
                return 1800000;
            }
            
            void reset() {
                this.mFirstReopenTime = -1L;
            }
        }
        
        class ScheduledReopen implements Runnable
        {
            private boolean mCancelled;
            private Executor mExecutor;
            final StateCallback this$1;
            
            ScheduledReopen(final StateCallback this$1, final Executor mExecutor) {
                this.this$1 = this$1;
                this.mCancelled = false;
                this.mExecutor = mExecutor;
            }
            
            void cancel() {
                this.mCancelled = true;
            }
            
            @Override
            public void run() {
                this.mExecutor.execute(new Camera2CameraImpl$StateCallback$ScheduledReopen$$ExternalSyntheticLambda0(this));
            }
        }
    }
    
    abstract static class UseCaseInfo
    {
        static UseCaseInfo create(final String s, final Class<?> clazz, final SessionConfig sessionConfig, final UseCaseConfig<?> useCaseConfig, final Size size) {
            return (UseCaseInfo)new AutoValue_Camera2CameraImpl_UseCaseInfo(s, clazz, sessionConfig, useCaseConfig, size);
        }
        
        static UseCaseInfo from(final UseCase useCase) {
            return create(Camera2CameraImpl.getUseCaseId(useCase), useCase.getClass(), useCase.getSessionConfig(), useCase.getCurrentConfig(), useCase.getAttachedSurfaceResolution());
        }
        
        abstract SessionConfig getSessionConfig();
        
        abstract Size getSurfaceResolution();
        
        abstract UseCaseConfig<?> getUseCaseConfig();
        
        abstract String getUseCaseId();
        
        abstract Class<?> getUseCaseType();
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.internal;

import android.media.CamcorderProfile;

interface CamcorderProfileHelper
{
    CamcorderProfile get(final int p0, final int p1);
    
    boolean hasProfile(final int p0, final int p1);
}

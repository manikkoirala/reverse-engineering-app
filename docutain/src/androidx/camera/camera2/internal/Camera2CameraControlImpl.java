// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.internal;

import android.hardware.camera2.CameraCaptureSession;
import java.util.Collection;
import java.util.concurrent.RejectedExecutionException;
import androidx.camera.core.impl.CameraCaptureFailure;
import androidx.camera.core.impl.CameraCaptureResult;
import android.util.ArrayMap;
import java.util.HashSet;
import java.util.Set;
import java.util.Map;
import androidx.camera.core.impl.utils.futures.AsyncFunction;
import androidx.camera.core.impl.utils.futures.FutureChain;
import androidx.camera.core.FocusMeteringResult;
import androidx.camera.core.FocusMeteringAction;
import android.util.Rational;
import androidx.camera.core.Logger;
import java.util.Iterator;
import androidx.core.util.Preconditions;
import android.graphics.Rect;
import java.util.Collections;
import android.hardware.camera2.CaptureRequest$Key;
import android.hardware.camera2.CaptureRequest;
import androidx.camera.camera2.impl.Camera2ImplConfig;
import androidx.camera.core.impl.CaptureConfig;
import androidx.camera.core.CameraControl;
import androidx.camera.core.impl.utils.executor.CameraXExecutors;
import androidx.camera.camera2.interop.CaptureRequestOptions;
import androidx.camera.core.impl.Config;
import androidx.concurrent.futures.CallbackToFutureAdapter;
import androidx.camera.core.impl.TagBundle;
import android.hardware.camera2.TotalCaptureResult;
import android.hardware.camera2.CameraCharacteristics$Key;
import android.hardware.camera2.CameraCharacteristics;
import android.os.Build$VERSION;
import androidx.camera.core.impl.CameraCaptureCallback;
import android.hardware.camera2.CameraCaptureSession$CaptureCallback;
import androidx.camera.core.impl.utils.futures.Futures;
import java.util.List;
import androidx.camera.core.impl.Quirks;
import androidx.camera.core.impl.Quirk;
import java.util.ArrayList;
import java.util.concurrent.ScheduledExecutorService;
import androidx.camera.core.impl.SessionConfig;
import java.util.concurrent.atomic.AtomicLong;
import com.google.common.util.concurrent.ListenableFuture;
import java.util.concurrent.Executor;
import androidx.camera.camera2.internal.compat.CameraCharacteristicsCompat;
import androidx.camera.camera2.interop.Camera2CameraControl;
import androidx.camera.camera2.internal.compat.workaround.AutoFlashAEModeDisabler;
import androidx.camera.camera2.internal.compat.workaround.AeFpsRange;
import androidx.camera.core.impl.CameraControlInternal;

public class Camera2CameraControlImpl implements CameraControlInternal
{
    private static final int DEFAULT_TEMPLATE = 1;
    private static final String TAG = "Camera2CameraControlImp";
    static final String TAG_SESSION_UPDATE_ID = "CameraControlSessionUpdateId";
    private final AeFpsRange mAeFpsRange;
    private final AutoFlashAEModeDisabler mAutoFlashAEModeDisabler;
    private final Camera2CameraControl mCamera2CameraControl;
    private final Camera2CapturePipeline mCamera2CapturePipeline;
    private final CameraCaptureCallbackSet mCameraCaptureCallbackSet;
    private final CameraCharacteristicsCompat mCameraCharacteristics;
    private final ControlUpdateCallback mControlUpdateCallback;
    private long mCurrentSessionUpdateId;
    final Executor mExecutor;
    private final ExposureControl mExposureControl;
    private volatile int mFlashMode;
    private volatile ListenableFuture<Void> mFlashModeChangeSessionUpdateFuture;
    private final FocusMeteringControl mFocusMeteringControl;
    private volatile boolean mIsTorchOn;
    private final Object mLock;
    private final AtomicLong mNextSessionUpdateId;
    final CameraControlSessionCallback mSessionCallback;
    private final SessionConfig.Builder mSessionConfigBuilder;
    private int mTemplate;
    private final TorchControl mTorchControl;
    private int mUseCount;
    private final ZoomControl mZoomControl;
    ZslControl mZslControl;
    
    Camera2CameraControlImpl(final CameraCharacteristicsCompat cameraCharacteristicsCompat, final ScheduledExecutorService scheduledExecutorService, final Executor executor, final ControlUpdateCallback controlUpdateCallback) {
        this(cameraCharacteristicsCompat, scheduledExecutorService, executor, controlUpdateCallback, new Quirks(new ArrayList<Quirk>()));
    }
    
    Camera2CameraControlImpl(final CameraCharacteristicsCompat mCameraCharacteristics, final ScheduledExecutorService scheduledExecutorService, final Executor mExecutor, final ControlUpdateCallback mControlUpdateCallback, final Quirks quirks) {
        this.mLock = new Object();
        final SessionConfig.Builder mSessionConfigBuilder = new SessionConfig.Builder();
        this.mSessionConfigBuilder = mSessionConfigBuilder;
        this.mUseCount = 0;
        this.mIsTorchOn = false;
        this.mFlashMode = 2;
        this.mNextSessionUpdateId = new AtomicLong(0L);
        this.mFlashModeChangeSessionUpdateFuture = Futures.immediateFuture((Void)null);
        this.mTemplate = 1;
        this.mCurrentSessionUpdateId = 0L;
        final CameraCaptureCallbackSet mCameraCaptureCallbackSet = new CameraCaptureCallbackSet();
        this.mCameraCaptureCallbackSet = mCameraCaptureCallbackSet;
        this.mCameraCharacteristics = mCameraCharacteristics;
        this.mControlUpdateCallback = mControlUpdateCallback;
        this.mExecutor = mExecutor;
        final CameraControlSessionCallback mSessionCallback = new CameraControlSessionCallback(mExecutor);
        this.mSessionCallback = mSessionCallback;
        mSessionConfigBuilder.setTemplateType(this.mTemplate);
        mSessionConfigBuilder.addRepeatingCameraCaptureCallback(CaptureCallbackContainer.create(mSessionCallback));
        mSessionConfigBuilder.addRepeatingCameraCaptureCallback(mCameraCaptureCallbackSet);
        this.mExposureControl = new ExposureControl(this, mCameraCharacteristics, mExecutor);
        this.mFocusMeteringControl = new FocusMeteringControl(this, scheduledExecutorService, mExecutor, quirks);
        this.mZoomControl = new ZoomControl(this, mCameraCharacteristics, mExecutor);
        this.mTorchControl = new TorchControl(this, mCameraCharacteristics, mExecutor);
        if (Build$VERSION.SDK_INT >= 23) {
            this.mZslControl = new ZslControlImpl(mCameraCharacteristics);
        }
        else {
            this.mZslControl = new ZslControlNoOpImpl();
        }
        this.mAeFpsRange = new AeFpsRange(quirks);
        this.mAutoFlashAEModeDisabler = new AutoFlashAEModeDisabler(quirks);
        this.mCamera2CameraControl = new Camera2CameraControl(this, mExecutor);
        this.mCamera2CapturePipeline = new Camera2CapturePipeline(this, mCameraCharacteristics, quirks, mExecutor);
        mExecutor.execute(new Camera2CameraControlImpl$$ExternalSyntheticLambda9(this));
    }
    
    private int getSupportedAwbMode(final int n) {
        final int[] array = this.mCameraCharacteristics.get((android.hardware.camera2.CameraCharacteristics$Key<int[]>)CameraCharacteristics.CONTROL_AWB_AVAILABLE_MODES);
        if (array == null) {
            return 0;
        }
        if (this.isModeInList(n, array)) {
            return n;
        }
        if (this.isModeInList(1, array)) {
            return 1;
        }
        return 0;
    }
    
    private boolean isControlInUse() {
        return this.getUseCount() > 0;
    }
    
    private boolean isModeInList(final int n, final int[] array) {
        for (int length = array.length, i = 0; i < length; ++i) {
            if (n == array[i]) {
                return true;
            }
        }
        return false;
    }
    
    static boolean isSessionUpdated(final TotalCaptureResult totalCaptureResult, final long n) {
        if (totalCaptureResult.getRequest() == null) {
            return false;
        }
        final Object tag = totalCaptureResult.getRequest().getTag();
        if (tag instanceof TagBundle) {
            final Long n2 = (Long)((TagBundle)tag).getTag("CameraControlSessionUpdateId");
            if (n2 == null) {
                return false;
            }
            if (n2 >= n) {
                return true;
            }
        }
        return false;
    }
    
    private ListenableFuture<Void> waitForSessionUpdateId(final long n) {
        return CallbackToFutureAdapter.getFuture((CallbackToFutureAdapter.Resolver<Void>)new Camera2CameraControlImpl$$ExternalSyntheticLambda5(this, n));
    }
    
    void addCaptureResultListener(final CaptureResultListener captureResultListener) {
        this.mSessionCallback.addListener(captureResultListener);
    }
    
    @Override
    public void addInteropConfig(final Config config) {
        this.mCamera2CameraControl.addCaptureRequestOptions(CaptureRequestOptions.Builder.from(config).build()).addListener((Runnable)new Camera2CameraControlImpl$$ExternalSyntheticLambda10(), CameraXExecutors.directExecutor());
    }
    
    void addSessionCameraCaptureCallback(final Executor executor, final CameraCaptureCallback cameraCaptureCallback) {
        this.mExecutor.execute(new Camera2CameraControlImpl$$ExternalSyntheticLambda4(this, executor, cameraCaptureCallback));
    }
    
    @Override
    public void addZslConfig(final SessionConfig.Builder builder) {
        this.mZslControl.addZslConfig(builder);
    }
    
    @Override
    public ListenableFuture<Void> cancelFocusAndMetering() {
        if (!this.isControlInUse()) {
            return Futures.immediateFailedFuture(new CameraControl.OperationCanceledException("Camera is not active."));
        }
        return Futures.nonCancellationPropagating(this.mFocusMeteringControl.cancelFocusAndMetering());
    }
    
    @Override
    public void clearInteropConfig() {
        this.mCamera2CameraControl.clearCaptureRequestOptions().addListener((Runnable)new Camera2CameraControlImpl$$ExternalSyntheticLambda3(), CameraXExecutors.directExecutor());
    }
    
    void decrementUseCount() {
        synchronized (this.mLock) {
            final int mUseCount = this.mUseCount;
            if (mUseCount != 0) {
                this.mUseCount = mUseCount - 1;
                return;
            }
            throw new IllegalStateException("Decrementing use count occurs more times than incrementing");
        }
    }
    
    @Override
    public ListenableFuture<Void> enableTorch(final boolean b) {
        if (!this.isControlInUse()) {
            return Futures.immediateFailedFuture(new CameraControl.OperationCanceledException("Camera is not active."));
        }
        return Futures.nonCancellationPropagating(this.mTorchControl.enableTorch(b));
    }
    
    void enableTorchInternal(final boolean mIsTorchOn) {
        if (!(this.mIsTorchOn = mIsTorchOn)) {
            final CaptureConfig.Builder builder = new CaptureConfig.Builder();
            builder.setTemplateType(this.mTemplate);
            builder.setUseRepeatingSurface(true);
            final Camera2ImplConfig.Builder builder2 = new Camera2ImplConfig.Builder();
            builder2.setCaptureRequestOption((android.hardware.camera2.CaptureRequest$Key<Integer>)CaptureRequest.CONTROL_AE_MODE, this.getSupportedAeMode(1));
            builder2.setCaptureRequestOption((android.hardware.camera2.CaptureRequest$Key<Integer>)CaptureRequest.FLASH_MODE, 0);
            builder.addImplementationOptions(builder2.build());
            this.submitCaptureRequestsInternal(Collections.singletonList(builder.build()));
        }
        this.updateSessionConfigSynchronous();
    }
    
    public Camera2CameraControl getCamera2CameraControl() {
        return this.mCamera2CameraControl;
    }
    
    Rect getCropSensorRegion() {
        return this.mZoomControl.getCropSensorRegion();
    }
    
    long getCurrentSessionUpdateId() {
        return this.mCurrentSessionUpdateId;
    }
    
    public ExposureControl getExposureControl() {
        return this.mExposureControl;
    }
    
    @Override
    public int getFlashMode() {
        return this.mFlashMode;
    }
    
    public FocusMeteringControl getFocusMeteringControl() {
        return this.mFocusMeteringControl;
    }
    
    @Override
    public Config getInteropConfig() {
        return this.mCamera2CameraControl.getCamera2ImplConfig();
    }
    
    int getMaxAeRegionCount() {
        final Integer n = this.mCameraCharacteristics.get((android.hardware.camera2.CameraCharacteristics$Key<Integer>)CameraCharacteristics.CONTROL_MAX_REGIONS_AE);
        int intValue;
        if (n == null) {
            intValue = 0;
        }
        else {
            intValue = n;
        }
        return intValue;
    }
    
    int getMaxAfRegionCount() {
        final Integer n = this.mCameraCharacteristics.get((android.hardware.camera2.CameraCharacteristics$Key<Integer>)CameraCharacteristics.CONTROL_MAX_REGIONS_AF);
        int intValue;
        if (n == null) {
            intValue = 0;
        }
        else {
            intValue = n;
        }
        return intValue;
    }
    
    int getMaxAwbRegionCount() {
        final Integer n = this.mCameraCharacteristics.get((android.hardware.camera2.CameraCharacteristics$Key<Integer>)CameraCharacteristics.CONTROL_MAX_REGIONS_AWB);
        int intValue;
        if (n == null) {
            intValue = 0;
        }
        else {
            intValue = n;
        }
        return intValue;
    }
    
    @Override
    public Rect getSensorRect() {
        return Preconditions.checkNotNull((Rect)this.mCameraCharacteristics.get((android.hardware.camera2.CameraCharacteristics$Key<T>)CameraCharacteristics.SENSOR_INFO_ACTIVE_ARRAY_SIZE));
    }
    
    @Override
    public SessionConfig getSessionConfig() {
        this.mSessionConfigBuilder.setTemplateType(this.mTemplate);
        this.mSessionConfigBuilder.setImplementationOptions(this.getSessionOptions());
        final Object captureRequestTag = this.mCamera2CameraControl.getCamera2ImplConfig().getCaptureRequestTag(null);
        if (captureRequestTag != null && captureRequestTag instanceof Integer) {
            this.mSessionConfigBuilder.addTag("Camera2CameraControl", captureRequestTag);
        }
        this.mSessionConfigBuilder.addTag("CameraControlSessionUpdateId", this.mCurrentSessionUpdateId);
        return this.mSessionConfigBuilder.build();
    }
    
    Config getSessionOptions() {
        final Camera2ImplConfig.Builder captureRequestOption = new Camera2ImplConfig.Builder();
        captureRequestOption.setCaptureRequestOption((android.hardware.camera2.CaptureRequest$Key<Integer>)CaptureRequest.CONTROL_MODE, 1);
        this.mFocusMeteringControl.addFocusMeteringOptions(captureRequestOption);
        this.mAeFpsRange.addAeFpsRangeOptions(captureRequestOption);
        this.mZoomControl.addZoomOption(captureRequestOption);
        int correctedAeMode = 0;
        Label_0105: {
            if (this.mIsTorchOn) {
                captureRequestOption.setCaptureRequestOption((android.hardware.camera2.CaptureRequest$Key<Integer>)CaptureRequest.FLASH_MODE, 2);
            }
            else {
                final int mFlashMode = this.mFlashMode;
                if (mFlashMode == 0) {
                    correctedAeMode = this.mAutoFlashAEModeDisabler.getCorrectedAeMode(2);
                    break Label_0105;
                }
                if (mFlashMode == 1) {
                    correctedAeMode = 3;
                    break Label_0105;
                }
            }
            correctedAeMode = 1;
        }
        captureRequestOption.setCaptureRequestOption((android.hardware.camera2.CaptureRequest$Key<Integer>)CaptureRequest.CONTROL_AE_MODE, this.getSupportedAeMode(correctedAeMode));
        captureRequestOption.setCaptureRequestOption((android.hardware.camera2.CaptureRequest$Key<Integer>)CaptureRequest.CONTROL_AWB_MODE, this.getSupportedAwbMode(1));
        this.mExposureControl.setCaptureRequestOption(captureRequestOption);
        final Camera2ImplConfig camera2ImplConfig = this.mCamera2CameraControl.getCamera2ImplConfig();
        for (final Config.Option<ValueT> option : camera2ImplConfig.listOptions()) {
            captureRequestOption.getMutableConfig().insertOption((Config.Option<Object>)option, Config.OptionPriority.ALWAYS_OVERRIDE, camera2ImplConfig.retrieveOption((Config.Option<ValueT>)option));
        }
        return captureRequestOption.build();
    }
    
    int getSupportedAeMode(final int n) {
        final int[] array = this.mCameraCharacteristics.get((android.hardware.camera2.CameraCharacteristics$Key<int[]>)CameraCharacteristics.CONTROL_AE_AVAILABLE_MODES);
        if (array == null) {
            return 0;
        }
        if (this.isModeInList(n, array)) {
            return n;
        }
        if (this.isModeInList(1, array)) {
            return 1;
        }
        return 0;
    }
    
    int getSupportedAfMode(final int n) {
        final int[] array = this.mCameraCharacteristics.get((android.hardware.camera2.CameraCharacteristics$Key<int[]>)CameraCharacteristics.CONTROL_AF_AVAILABLE_MODES);
        if (array == null) {
            return 0;
        }
        if (this.isModeInList(n, array)) {
            return n;
        }
        if (this.isModeInList(4, array)) {
            return 4;
        }
        if (this.isModeInList(1, array)) {
            return 1;
        }
        return 0;
    }
    
    public TorchControl getTorchControl() {
        return this.mTorchControl;
    }
    
    int getUseCount() {
        synchronized (this.mLock) {
            return this.mUseCount;
        }
    }
    
    public ZoomControl getZoomControl() {
        return this.mZoomControl;
    }
    
    public ZslControl getZslControl() {
        return this.mZslControl;
    }
    
    void incrementUseCount() {
        synchronized (this.mLock) {
            ++this.mUseCount;
        }
    }
    
    boolean isTorchOn() {
        return this.mIsTorchOn;
    }
    
    @Override
    public boolean isZslDisabledByByUserCaseConfig() {
        return this.mZslControl.isZslDisabledByUserCaseConfig();
    }
    
    void removeCaptureResultListener(final CaptureResultListener captureResultListener) {
        this.mSessionCallback.removeListener(captureResultListener);
    }
    
    void removeSessionCameraCaptureCallback(final CameraCaptureCallback cameraCaptureCallback) {
        this.mExecutor.execute(new Camera2CameraControlImpl$$ExternalSyntheticLambda0(this, cameraCaptureCallback));
    }
    
    void resetTemplate() {
        this.setTemplate(1);
    }
    
    void setActive(final boolean active) {
        this.mFocusMeteringControl.setActive(active);
        this.mZoomControl.setActive(active);
        this.mTorchControl.setActive(active);
        this.mExposureControl.setActive(active);
        this.mCamera2CameraControl.setActive(active);
    }
    
    @Override
    public ListenableFuture<Integer> setExposureCompensationIndex(final int exposureCompensationIndex) {
        if (!this.isControlInUse()) {
            return Futures.immediateFailedFuture(new CameraControl.OperationCanceledException("Camera is not active."));
        }
        return this.mExposureControl.setExposureCompensationIndex(exposureCompensationIndex);
    }
    
    @Override
    public void setFlashMode(int mFlashMode) {
        if (!this.isControlInUse()) {
            Logger.w("Camera2CameraControlImp", "Camera is not active.");
            return;
        }
        this.mFlashMode = mFlashMode;
        final ZslControl mZslControl = this.mZslControl;
        mFlashMode = this.mFlashMode;
        boolean zslDisabledByFlashMode = true;
        if (mFlashMode != 1) {
            zslDisabledByFlashMode = (this.mFlashMode == 0 && zslDisabledByFlashMode);
        }
        mZslControl.setZslDisabledByFlashMode(zslDisabledByFlashMode);
        this.mFlashModeChangeSessionUpdateFuture = this.updateSessionConfigAsync();
    }
    
    @Override
    public ListenableFuture<Void> setLinearZoom(final float linearZoom) {
        if (!this.isControlInUse()) {
            return Futures.immediateFailedFuture(new CameraControl.OperationCanceledException("Camera is not active."));
        }
        return Futures.nonCancellationPropagating(this.mZoomControl.setLinearZoom(linearZoom));
    }
    
    public void setPreviewAspectRatio(final Rational previewAspectRatio) {
        this.mFocusMeteringControl.setPreviewAspectRatio(previewAspectRatio);
    }
    
    void setTemplate(final int n) {
        this.mTemplate = n;
        this.mFocusMeteringControl.setTemplate(n);
        this.mCamera2CapturePipeline.setTemplate(this.mTemplate);
    }
    
    @Override
    public ListenableFuture<Void> setZoomRatio(final float zoomRatio) {
        if (!this.isControlInUse()) {
            return Futures.immediateFailedFuture(new CameraControl.OperationCanceledException("Camera is not active."));
        }
        return Futures.nonCancellationPropagating(this.mZoomControl.setZoomRatio(zoomRatio));
    }
    
    @Override
    public void setZslDisabledByUserCaseConfig(final boolean zslDisabledByUserCaseConfig) {
        this.mZslControl.setZslDisabledByUserCaseConfig(zslDisabledByUserCaseConfig);
    }
    
    @Override
    public ListenableFuture<FocusMeteringResult> startFocusAndMetering(final FocusMeteringAction focusMeteringAction) {
        if (!this.isControlInUse()) {
            return Futures.immediateFailedFuture(new CameraControl.OperationCanceledException("Camera is not active."));
        }
        return Futures.nonCancellationPropagating(this.mFocusMeteringControl.startFocusAndMetering(focusMeteringAction));
    }
    
    void submitCaptureRequestsInternal(final List<CaptureConfig> list) {
        this.mControlUpdateCallback.onCameraControlCaptureRequests(list);
    }
    
    @Override
    public ListenableFuture<List<Void>> submitStillCaptureRequests(final List<CaptureConfig> list, final int n, final int n2) {
        if (!this.isControlInUse()) {
            Logger.w("Camera2CameraControlImp", "Camera is not active.");
            return Futures.immediateFailedFuture(new CameraControl.OperationCanceledException("Camera is not active."));
        }
        return (ListenableFuture<List<Void>>)FutureChain.from((com.google.common.util.concurrent.ListenableFuture<Object>)Futures.nonCancellationPropagating((com.google.common.util.concurrent.ListenableFuture<V>)this.mFlashModeChangeSessionUpdateFuture)).transformAsync((AsyncFunction<? super Object, Object>)new Camera2CameraControlImpl$$ExternalSyntheticLambda7(this, list, n, this.getFlashMode(), n2), this.mExecutor);
    }
    
    public void updateSessionConfig() {
        this.mExecutor.execute(new Camera2CameraControlImpl$$ExternalSyntheticLambda1(this));
    }
    
    ListenableFuture<Void> updateSessionConfigAsync() {
        return Futures.nonCancellationPropagating((com.google.common.util.concurrent.ListenableFuture<Void>)CallbackToFutureAdapter.getFuture((CallbackToFutureAdapter.Resolver<V>)new Camera2CameraControlImpl$$ExternalSyntheticLambda2(this)));
    }
    
    long updateSessionConfigSynchronous() {
        this.mCurrentSessionUpdateId = this.mNextSessionUpdateId.getAndIncrement();
        this.mControlUpdateCallback.onCameraControlUpdateSessionConfig();
        return this.mCurrentSessionUpdateId;
    }
    
    static final class CameraCaptureCallbackSet extends CameraCaptureCallback
    {
        Map<CameraCaptureCallback, Executor> mCallbackExecutors;
        Set<CameraCaptureCallback> mCallbacks;
        
        CameraCaptureCallbackSet() {
            this.mCallbacks = new HashSet<CameraCaptureCallback>();
            this.mCallbackExecutors = (Map<CameraCaptureCallback, Executor>)new ArrayMap();
        }
        
        void addCaptureCallback(final Executor executor, final CameraCaptureCallback cameraCaptureCallback) {
            this.mCallbacks.add(cameraCaptureCallback);
            this.mCallbackExecutors.put(cameraCaptureCallback, executor);
        }
        
        @Override
        public void onCaptureCancelled() {
            for (final CameraCaptureCallback cameraCaptureCallback : this.mCallbacks) {
                try {
                    this.mCallbackExecutors.get(cameraCaptureCallback).execute(new Camera2CameraControlImpl$CameraCaptureCallbackSet$$ExternalSyntheticLambda1(cameraCaptureCallback));
                }
                catch (final RejectedExecutionException ex) {
                    Logger.e("Camera2CameraControlImp", "Executor rejected to invoke onCaptureCancelled.", ex);
                }
            }
        }
        
        @Override
        public void onCaptureCompleted(final CameraCaptureResult cameraCaptureResult) {
            for (final CameraCaptureCallback cameraCaptureCallback : this.mCallbacks) {
                try {
                    this.mCallbackExecutors.get(cameraCaptureCallback).execute(new Camera2CameraControlImpl$CameraCaptureCallbackSet$$ExternalSyntheticLambda2(cameraCaptureCallback, cameraCaptureResult));
                }
                catch (final RejectedExecutionException ex) {
                    Logger.e("Camera2CameraControlImp", "Executor rejected to invoke onCaptureCompleted.", ex);
                }
            }
        }
        
        @Override
        public void onCaptureFailed(final CameraCaptureFailure cameraCaptureFailure) {
            for (final CameraCaptureCallback cameraCaptureCallback : this.mCallbacks) {
                try {
                    this.mCallbackExecutors.get(cameraCaptureCallback).execute(new Camera2CameraControlImpl$CameraCaptureCallbackSet$$ExternalSyntheticLambda0(cameraCaptureCallback, cameraCaptureFailure));
                }
                catch (final RejectedExecutionException ex) {
                    Logger.e("Camera2CameraControlImp", "Executor rejected to invoke onCaptureFailed.", ex);
                }
            }
        }
        
        void removeCaptureCallback(final CameraCaptureCallback cameraCaptureCallback) {
            this.mCallbacks.remove(cameraCaptureCallback);
            this.mCallbackExecutors.remove(cameraCaptureCallback);
        }
    }
    
    static final class CameraControlSessionCallback extends CameraCaptureSession$CaptureCallback
    {
        private final Executor mExecutor;
        final Set<CaptureResultListener> mResultListeners;
        
        CameraControlSessionCallback(final Executor mExecutor) {
            this.mResultListeners = new HashSet<CaptureResultListener>();
            this.mExecutor = mExecutor;
        }
        
        void addListener(final CaptureResultListener captureResultListener) {
            this.mResultListeners.add(captureResultListener);
        }
        
        public void onCaptureCompleted(final CameraCaptureSession cameraCaptureSession, final CaptureRequest captureRequest, final TotalCaptureResult totalCaptureResult) {
            this.mExecutor.execute(new Camera2CameraControlImpl$CameraControlSessionCallback$$ExternalSyntheticLambda0(this, totalCaptureResult));
        }
        
        void removeListener(final CaptureResultListener captureResultListener) {
            this.mResultListeners.remove(captureResultListener);
        }
    }
    
    public interface CaptureResultListener
    {
        boolean onCaptureResult(final TotalCaptureResult p0);
    }
}

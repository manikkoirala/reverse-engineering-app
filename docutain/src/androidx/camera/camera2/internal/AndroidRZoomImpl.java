// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.internal;

import androidx.camera.core.CameraControl;
import android.hardware.camera2.TotalCaptureResult;
import androidx.core.util.Preconditions;
import android.graphics.Rect;
import android.hardware.camera2.CaptureRequest$Key;
import android.hardware.camera2.CaptureRequest;
import androidx.camera.camera2.impl.Camera2ImplConfig;
import android.hardware.camera2.CameraCharacteristics$Key;
import android.hardware.camera2.CameraCharacteristics;
import android.util.Range;
import androidx.concurrent.futures.CallbackToFutureAdapter;
import androidx.camera.camera2.internal.compat.CameraCharacteristicsCompat;

final class AndroidRZoomImpl implements ZoomImpl
{
    public static final float DEFAULT_ZOOM_RATIO = 1.0f;
    private final CameraCharacteristicsCompat mCameraCharacteristics;
    private float mCurrentZoomRatio;
    private float mPendingZoomRatio;
    private CallbackToFutureAdapter.Completer<Void> mPendingZoomRatioCompleter;
    private final Range<Float> mZoomRatioRange;
    
    AndroidRZoomImpl(final CameraCharacteristicsCompat mCameraCharacteristics) {
        this.mCurrentZoomRatio = 1.0f;
        this.mPendingZoomRatio = 1.0f;
        this.mCameraCharacteristics = mCameraCharacteristics;
        this.mZoomRatioRange = (Range<Float>)mCameraCharacteristics.get((android.hardware.camera2.CameraCharacteristics$Key<Range>)CameraCharacteristics.CONTROL_ZOOM_RATIO_RANGE);
    }
    
    @Override
    public void addRequestOption(final Camera2ImplConfig.Builder builder) {
        builder.setCaptureRequestOption((android.hardware.camera2.CaptureRequest$Key<Float>)CaptureRequest.CONTROL_ZOOM_RATIO, this.mCurrentZoomRatio);
    }
    
    @Override
    public Rect getCropSensorRegion() {
        return Preconditions.checkNotNull((Rect)this.mCameraCharacteristics.get((android.hardware.camera2.CameraCharacteristics$Key<T>)CameraCharacteristics.SENSOR_INFO_ACTIVE_ARRAY_SIZE));
    }
    
    @Override
    public float getMaxZoom() {
        return (float)this.mZoomRatioRange.getUpper();
    }
    
    @Override
    public float getMinZoom() {
        return (float)this.mZoomRatioRange.getLower();
    }
    
    @Override
    public void onCaptureResult(final TotalCaptureResult totalCaptureResult) {
        if (this.mPendingZoomRatioCompleter != null) {
            final CaptureRequest request = totalCaptureResult.getRequest();
            Float n;
            if (request == null) {
                n = null;
            }
            else {
                n = (Float)request.get(CaptureRequest.CONTROL_ZOOM_RATIO);
            }
            if (n == null) {
                return;
            }
            if (this.mPendingZoomRatio == n) {
                this.mPendingZoomRatioCompleter.set(null);
                this.mPendingZoomRatioCompleter = null;
            }
        }
    }
    
    @Override
    public void resetZoom() {
        this.mCurrentZoomRatio = 1.0f;
        final CallbackToFutureAdapter.Completer<Void> mPendingZoomRatioCompleter = this.mPendingZoomRatioCompleter;
        if (mPendingZoomRatioCompleter != null) {
            mPendingZoomRatioCompleter.setException(new CameraControl.OperationCanceledException("Camera is not active."));
            this.mPendingZoomRatioCompleter = null;
        }
    }
    
    @Override
    public void setZoomRatio(final float mCurrentZoomRatio, final CallbackToFutureAdapter.Completer<Void> mPendingZoomRatioCompleter) {
        this.mCurrentZoomRatio = mCurrentZoomRatio;
        final CallbackToFutureAdapter.Completer<Void> mPendingZoomRatioCompleter2 = this.mPendingZoomRatioCompleter;
        if (mPendingZoomRatioCompleter2 != null) {
            mPendingZoomRatioCompleter2.setException(new CameraControl.OperationCanceledException("There is a new zoomRatio being set"));
        }
        this.mPendingZoomRatio = this.mCurrentZoomRatio;
        this.mPendingZoomRatioCompleter = mPendingZoomRatioCompleter;
    }
}

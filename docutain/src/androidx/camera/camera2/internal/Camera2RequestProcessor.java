// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.internal;

import androidx.camera.core.impl.CameraCaptureFailure;
import android.hardware.camera2.CaptureFailure;
import androidx.camera.core.impl.CameraCaptureResult;
import android.hardware.camera2.CaptureResult;
import android.hardware.camera2.TotalCaptureResult;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.CameraCaptureSession;
import androidx.camera.core.impl.CaptureConfig;
import java.util.Arrays;
import androidx.camera.core.impl.TagBundle;
import androidx.camera.core.impl.CameraCaptureCallback;
import android.hardware.camera2.CameraCaptureSession$CaptureCallback;
import java.util.concurrent.ExecutionException;
import android.view.Surface;
import androidx.camera.core.Logger;
import androidx.camera.core.impl.DeferrableSurface;
import java.util.Iterator;
import java.util.Collections;
import java.util.Collection;
import java.util.ArrayList;
import androidx.core.util.Preconditions;
import androidx.camera.core.impl.SessionConfig;
import androidx.camera.core.impl.SessionProcessorSurface;
import java.util.List;
import androidx.camera.core.impl.RequestProcessor;

public class Camera2RequestProcessor implements RequestProcessor
{
    private static final String TAG = "Camera2RequestProcessor";
    private final CaptureSession mCaptureSession;
    private volatile boolean mIsClosed;
    private final List<SessionProcessorSurface> mProcessorSurfaces;
    private volatile SessionConfig mSessionConfig;
    
    public Camera2RequestProcessor(final CaptureSession mCaptureSession, final List<SessionProcessorSurface> c) {
        boolean b = false;
        this.mIsClosed = false;
        if (mCaptureSession.mState == CaptureSession.State.OPENED) {
            b = true;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("CaptureSession state must be OPENED. Current state:");
        sb.append(mCaptureSession.mState);
        Preconditions.checkArgument(b, (Object)sb.toString());
        this.mCaptureSession = mCaptureSession;
        this.mProcessorSurfaces = Collections.unmodifiableList((List<? extends SessionProcessorSurface>)new ArrayList<SessionProcessorSurface>(c));
    }
    
    private boolean areRequestsValid(final List<Request> list) {
        final Iterator<Request> iterator = list.iterator();
        while (iterator.hasNext()) {
            if (!this.isRequestValid(iterator.next())) {
                return false;
            }
        }
        return true;
    }
    
    private DeferrableSurface findSurface(final int n) {
        for (final SessionProcessorSurface sessionProcessorSurface : this.mProcessorSurfaces) {
            if (sessionProcessorSurface.getOutputConfigId() == n) {
                return sessionProcessorSurface;
            }
        }
        return null;
    }
    
    private boolean isRequestValid(final Request request) {
        if (request.getTargetOutputConfigIds().isEmpty()) {
            Logger.e("Camera2RequestProcessor", "Unable to submit the RequestProcessor.Request: empty targetOutputConfigIds");
            return false;
        }
        for (final Integer obj : request.getTargetOutputConfigIds()) {
            if (this.findSurface(obj) == null) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Unable to submit the RequestProcessor.Request: targetOutputConfigId(");
                sb.append(obj);
                sb.append(") is not a valid id");
                Logger.e("Camera2RequestProcessor", sb.toString());
                return false;
            }
        }
        return true;
    }
    
    @Override
    public void abortCaptures() {
        if (this.mIsClosed) {
            return;
        }
        this.mCaptureSession.abortCaptures();
    }
    
    public void close() {
        this.mIsClosed = true;
    }
    
    int findOutputConfigId(final Surface surface) {
        for (final SessionProcessorSurface sessionProcessorSurface : this.mProcessorSurfaces) {
            try {
                if (sessionProcessorSurface.getSurface().get() == surface) {
                    return sessionProcessorSurface.getOutputConfigId();
                }
                continue;
            }
            catch (final InterruptedException | ExecutionException ex) {
                continue;
            }
            break;
        }
        return -1;
    }
    
    @Override
    public int setRepeating(final Request request, final Callback callback) {
        if (!this.mIsClosed && this.isRequestValid(request)) {
            final SessionConfig.Builder builder = new SessionConfig.Builder();
            builder.setTemplateType(request.getTemplateId());
            builder.setImplementationOptions(request.getParameters());
            builder.addCameraCaptureCallback(CaptureCallbackContainer.create(new Camera2CallbackWrapper(request, callback, true)));
            if (this.mSessionConfig != null) {
                final Iterator<CameraCaptureCallback> iterator = this.mSessionConfig.getRepeatingCameraCaptureCallbacks().iterator();
                while (iterator.hasNext()) {
                    builder.addCameraCaptureCallback(iterator.next());
                }
                final TagBundle tagBundle = this.mSessionConfig.getRepeatingCaptureConfig().getTagBundle();
                for (final String s : tagBundle.listKeys()) {
                    builder.addTag(s, tagBundle.getTag(s));
                }
            }
            final Iterator<Integer> iterator3 = request.getTargetOutputConfigIds().iterator();
            while (iterator3.hasNext()) {
                builder.addSurface(this.findSurface(iterator3.next()));
            }
            return this.mCaptureSession.issueRepeatingCaptureRequests(builder.build());
        }
        return -1;
    }
    
    @Override
    public void stopRepeating() {
        if (this.mIsClosed) {
            return;
        }
        this.mCaptureSession.stopRepeating();
    }
    
    @Override
    public int submit(final Request request, final Callback callback) {
        return this.submit(Arrays.asList(request), callback);
    }
    
    @Override
    public int submit(final List<Request> list, final Callback callback) {
        if (!this.mIsClosed && this.areRequestsValid(list)) {
            final ArrayList list2 = new ArrayList();
            boolean b = true;
            for (final Request request : list) {
                final CaptureConfig.Builder builder = new CaptureConfig.Builder();
                builder.setTemplateType(request.getTemplateId());
                builder.setImplementationOptions(request.getParameters());
                builder.addCameraCaptureCallback(CaptureCallbackContainer.create(new Camera2CallbackWrapper(request, callback, b)));
                b = false;
                final Iterator<Integer> iterator2 = request.getTargetOutputConfigIds().iterator();
                while (iterator2.hasNext()) {
                    builder.addSurface(this.findSurface(iterator2.next()));
                }
                list2.add(builder.build());
            }
            return this.mCaptureSession.issueBurstCaptureRequest(list2);
        }
        return -1;
    }
    
    public void updateSessionConfig(final SessionConfig mSessionConfig) {
        this.mSessionConfig = mSessionConfig;
    }
    
    private class Camera2CallbackWrapper extends CameraCaptureSession$CaptureCallback
    {
        private final Callback mCallback;
        private final boolean mInvokeSequenceCallback;
        private final Request mRequest;
        final Camera2RequestProcessor this$0;
        
        Camera2CallbackWrapper(final Camera2RequestProcessor this$0, final Request mRequest, final Callback mCallback, final boolean mInvokeSequenceCallback) {
            this.this$0 = this$0;
            this.mCallback = mCallback;
            this.mRequest = mRequest;
            this.mInvokeSequenceCallback = mInvokeSequenceCallback;
        }
        
        public void onCaptureBufferLost(final CameraCaptureSession cameraCaptureSession, final CaptureRequest captureRequest, final Surface surface, final long n) {
            this.mCallback.onCaptureBufferLost(this.mRequest, n, this.this$0.findOutputConfigId(surface));
        }
        
        public void onCaptureCompleted(final CameraCaptureSession cameraCaptureSession, final CaptureRequest captureRequest, final TotalCaptureResult totalCaptureResult) {
            this.mCallback.onCaptureCompleted(this.mRequest, new Camera2CameraCaptureResult((CaptureResult)totalCaptureResult));
        }
        
        public void onCaptureFailed(final CameraCaptureSession cameraCaptureSession, final CaptureRequest captureRequest, final CaptureFailure captureFailure) {
            this.mCallback.onCaptureFailed(this.mRequest, new Camera2CameraCaptureFailure(CameraCaptureFailure.Reason.ERROR, captureFailure));
        }
        
        public void onCaptureProgressed(final CameraCaptureSession cameraCaptureSession, final CaptureRequest captureRequest, final CaptureResult captureResult) {
            this.mCallback.onCaptureProgressed(this.mRequest, new Camera2CameraCaptureResult(captureResult));
        }
        
        public void onCaptureSequenceAborted(final CameraCaptureSession cameraCaptureSession, final int n) {
            if (this.mInvokeSequenceCallback) {
                this.mCallback.onCaptureSequenceAborted(n);
            }
        }
        
        public void onCaptureSequenceCompleted(final CameraCaptureSession cameraCaptureSession, final int n, final long n2) {
            if (this.mInvokeSequenceCallback) {
                this.mCallback.onCaptureSequenceCompleted(n, n2);
            }
        }
        
        public void onCaptureStarted(final CameraCaptureSession cameraCaptureSession, final CaptureRequest captureRequest, final long n, final long n2) {
            this.mCallback.onCaptureStarted(this.mRequest, n2, n);
        }
    }
}

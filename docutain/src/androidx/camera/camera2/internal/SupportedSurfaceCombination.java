// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.internal;

import androidx.camera.core.impl.AttachedSurfaceInfo;
import androidx.camera.core.impl.SurfaceConfig;
import androidx.camera.core.impl.utils.CameraOrientationUtil;
import java.util.Collections;
import androidx.camera.core.impl.UseCaseConfig;
import androidx.camera.core.impl.utils.AspectRatioUtil;
import androidx.camera.core.Logger;
import androidx.camera.camera2.internal.compat.workaround.TargetAspectRatio;
import android.util.Rational;
import android.media.MediaRecorder;
import androidx.camera.core.internal.utils.SizeUtil;
import android.media.CamcorderProfile;
import android.util.Pair;
import androidx.camera.core.impl.ImageOutputConfig;
import java.util.Iterator;
import java.util.Collection;
import java.util.Comparator;
import java.util.Arrays;
import androidx.camera.core.impl.utils.CompareSizesByArea;
import android.graphics.SurfaceTexture;
import android.os.Build$VERSION;
import android.hardware.camera2.params.StreamConfigurationMap;
import androidx.camera.core.CameraUnavailableException;
import androidx.camera.camera2.internal.compat.CameraAccessExceptionCompat;
import android.hardware.camera2.CameraCharacteristics$Key;
import android.hardware.camera2.CameraCharacteristics;
import androidx.core.util.Preconditions;
import java.util.HashMap;
import java.util.ArrayList;
import androidx.camera.camera2.internal.compat.CameraManagerCompat;
import android.content.Context;
import androidx.camera.core.impl.SurfaceSizeDefinition;
import androidx.camera.core.impl.SurfaceCombination;
import androidx.camera.camera2.internal.compat.workaround.ResolutionCorrector;
import androidx.camera.camera2.internal.compat.workaround.ExtraSupportedSurfaceCombinationsContainer;
import androidx.camera.camera2.internal.compat.workaround.ExcludedSupportedSizesContainer;
import android.util.Size;
import java.util.List;
import java.util.Map;
import androidx.camera.camera2.internal.compat.CameraCharacteristicsCompat;

final class SupportedSurfaceCombination
{
    private static final String TAG = "SupportedSurfaceCombination";
    private final CamcorderProfileHelper mCamcorderProfileHelper;
    private final String mCameraId;
    private final CameraCharacteristicsCompat mCharacteristics;
    private final DisplayInfoManager mDisplayInfoManager;
    private final Map<Integer, List<Size>> mExcludedSizeListCache;
    private final ExcludedSupportedSizesContainer mExcludedSupportedSizesContainer;
    private final ExtraSupportedSurfaceCombinationsContainer mExtraSupportedSurfaceCombinationsContainer;
    private final int mHardwareLevel;
    private boolean mIsBurstCaptureSupported;
    private boolean mIsRawSupported;
    private final boolean mIsSensorLandscapeResolution;
    private final Map<Integer, Size> mMaxSizeCache;
    private Map<Integer, Size[]> mOutputSizesCache;
    private final ResolutionCorrector mResolutionCorrector;
    private final List<SurfaceCombination> mSurfaceCombinations;
    SurfaceSizeDefinition mSurfaceSizeDefinition;
    
    SupportedSurfaceCombination(final Context context, final String s, final CameraManagerCompat cameraManagerCompat, final CamcorderProfileHelper camcorderProfileHelper) throws CameraUnavailableException {
        this.mSurfaceCombinations = new ArrayList<SurfaceCombination>();
        this.mMaxSizeCache = new HashMap<Integer, Size>();
        this.mExcludedSizeListCache = new HashMap<Integer, List<Size>>();
        final int n = 0;
        this.mIsRawSupported = false;
        this.mIsBurstCaptureSupported = false;
        this.mOutputSizesCache = new HashMap<Integer, Size[]>();
        this.mResolutionCorrector = new ResolutionCorrector();
        final String mCameraId = Preconditions.checkNotNull(s);
        this.mCameraId = mCameraId;
        this.mCamcorderProfileHelper = Preconditions.checkNotNull(camcorderProfileHelper);
        this.mExcludedSupportedSizesContainer = new ExcludedSupportedSizesContainer(s);
        this.mExtraSupportedSurfaceCombinationsContainer = new ExtraSupportedSurfaceCombinationsContainer();
        this.mDisplayInfoManager = DisplayInfoManager.getInstance(context);
        try {
            final CameraCharacteristicsCompat cameraCharacteristicsCompat = cameraManagerCompat.getCameraCharacteristicsCompat(mCameraId);
            this.mCharacteristics = cameraCharacteristicsCompat;
            final Integer n2 = cameraCharacteristicsCompat.get((android.hardware.camera2.CameraCharacteristics$Key<Integer>)CameraCharacteristics.INFO_SUPPORTED_HARDWARE_LEVEL);
            int intValue;
            if (n2 != null) {
                intValue = n2;
            }
            else {
                intValue = 2;
            }
            this.mHardwareLevel = intValue;
            this.mIsSensorLandscapeResolution = this.isSensorLandscapeResolution();
            final int[] array = cameraCharacteristicsCompat.get((android.hardware.camera2.CameraCharacteristics$Key<int[]>)CameraCharacteristics.REQUEST_AVAILABLE_CAPABILITIES);
            if (array != null) {
                for (int length = array.length, i = n; i < length; ++i) {
                    final int n3 = array[i];
                    if (n3 == 3) {
                        this.mIsRawSupported = true;
                    }
                    else if (n3 == 6) {
                        this.mIsBurstCaptureSupported = true;
                    }
                }
            }
            this.generateSupportedCombinationList();
            this.generateSurfaceSizeDefinition();
            this.checkCustomization();
        }
        catch (final CameraAccessExceptionCompat cameraAccessExceptionCompat) {
            throw CameraUnavailableExceptionHelper.createFrom(cameraAccessExceptionCompat);
        }
    }
    
    private void checkCustomization() {
    }
    
    private Size[] doGetAllOutputSizesByFormat(final int i) {
        final StreamConfigurationMap streamConfigurationMap = this.mCharacteristics.get((android.hardware.camera2.CameraCharacteristics$Key<StreamConfigurationMap>)CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP);
        if (streamConfigurationMap == null) {
            throw new IllegalArgumentException("Can not retrieve SCALER_STREAM_CONFIGURATION_MAP");
        }
        Size[] array;
        if (Build$VERSION.SDK_INT < 23 && i == 34) {
            array = streamConfigurationMap.getOutputSizes((Class)SurfaceTexture.class);
        }
        else {
            array = streamConfigurationMap.getOutputSizes(i);
        }
        if (array != null) {
            final Size[] excludeProblematicSizes = this.excludeProblematicSizes(array, i);
            Arrays.sort(excludeProblematicSizes, new CompareSizesByArea(true));
            return excludeProblematicSizes;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Can not get supported output size for the format: ");
        sb.append(i);
        throw new IllegalArgumentException(sb.toString());
    }
    
    private Size[] excludeProblematicSizes(final Size[] a, final int n) {
        final List<Size> fetchExcludedSizes = this.fetchExcludedSizes(n);
        final ArrayList list = new ArrayList((Collection<? extends E>)Arrays.asList(a));
        list.removeAll(fetchExcludedSizes);
        return (Size[])list.toArray(new Size[0]);
    }
    
    private List<Size> fetchExcludedSizes(final int n) {
        List<Size> value;
        if ((value = this.mExcludedSizeListCache.get(n)) == null) {
            value = this.mExcludedSupportedSizesContainer.get(n);
            this.mExcludedSizeListCache.put(n, value);
        }
        return value;
    }
    
    private Size fetchMaxSize(final int n) {
        final Size size = this.mMaxSizeCache.get(n);
        if (size != null) {
            return size;
        }
        final Size maxOutputSizeByFormat = this.getMaxOutputSizeByFormat(n);
        this.mMaxSizeCache.put(n, maxOutputSizeByFormat);
        return maxOutputSizeByFormat;
    }
    
    private Size flipSizeByRotation(final Size size, final int n) {
        Size size2 = size;
        if (size != null) {
            size2 = size;
            if (this.isRotationNeeded(n)) {
                size2 = new Size(size.getHeight(), size.getWidth());
            }
        }
        return size2;
    }
    
    private void generateSupportedCombinationList() {
        this.mSurfaceCombinations.addAll(GuaranteedConfigurationsUtil.generateSupportedCombinationList(this.mHardwareLevel, this.mIsRawSupported, this.mIsBurstCaptureSupported));
        this.mSurfaceCombinations.addAll(this.mExtraSupportedSurfaceCombinationsContainer.get(this.mCameraId, this.mHardwareLevel));
    }
    
    private void generateSurfaceSizeDefinition() {
        this.mSurfaceSizeDefinition = SurfaceSizeDefinition.create(new Size(640, 480), this.mDisplayInfoManager.getPreviewSize(), this.getRecordSize());
    }
    
    private Size[] getAllOutputSizesByFormat(final int n) {
        Size[] doGetAllOutputSizesByFormat;
        if ((doGetAllOutputSizesByFormat = this.mOutputSizesCache.get(n)) == null) {
            doGetAllOutputSizesByFormat = this.doGetAllOutputSizesByFormat(n);
            this.mOutputSizesCache.put(n, doGetAllOutputSizesByFormat);
        }
        return doGetAllOutputSizesByFormat;
    }
    
    private List<List<Size>> getAllPossibleSizeArrangements(final List<List<Size>> list) {
        final Iterator<List> iterator = list.iterator();
        int n = 1;
        while (iterator.hasNext()) {
            n *= iterator.next().size();
        }
        if (n != 0) {
            final ArrayList list2 = new ArrayList();
            for (int i = 0; i < n; ++i) {
                list2.add(new ArrayList());
            }
            int n2 = n / list.get(0).size();
            int n3 = n;
            int n4;
            for (int j = 0; j < list.size(); ++j, n2 = n4) {
                final List list3 = list.get(j);
                for (int k = 0; k < n; ++k) {
                    ((List<Size>)list2.get(k)).add((Size)list3.get(k % n3 / n2));
                }
                n4 = n2;
                if (j < list.size() - 1) {
                    n4 = n2 / list.get(j + 1).size();
                    n3 = n2;
                }
            }
            return list2;
        }
        throw new IllegalArgumentException("Failed to find supported resolutions.");
    }
    
    private Size[] getCustomizedSupportSizesFromConfig(final int n, final ImageOutputConfig imageOutputConfig) {
        final Size[] array = null;
        final List<Pair<Integer, Size[]>> supportedResolutions = imageOutputConfig.getSupportedResolutions(null);
        Size[] array2 = array;
        Label_0072: {
            if (supportedResolutions != null) {
                final Iterator<Pair<Integer, Size[]>> iterator = supportedResolutions.iterator();
                Pair pair;
                do {
                    array2 = array;
                    if (!iterator.hasNext()) {
                        break Label_0072;
                    }
                    pair = iterator.next();
                } while ((int)pair.first != n);
                array2 = (Size[])pair.second;
            }
        }
        Size[] excludeProblematicSizes;
        if ((excludeProblematicSizes = array2) != null) {
            excludeProblematicSizes = this.excludeProblematicSizes(array2, n);
            Arrays.sort(excludeProblematicSizes, new CompareSizesByArea(true));
        }
        return excludeProblematicSizes;
    }
    
    private Size getRecordSize() {
        try {
            final int int1 = Integer.parseInt(this.mCameraId);
            CamcorderProfile value = null;
            if (this.mCamcorderProfileHelper.hasProfile(int1, 1)) {
                value = this.mCamcorderProfileHelper.get(int1, 1);
            }
            if (value != null) {
                return new Size(value.videoFrameWidth, value.videoFrameHeight);
            }
            return this.getRecordSizeByHasProfile(int1);
        }
        catch (final NumberFormatException ex) {
            return this.getRecordSizeFromStreamConfigurationMap();
        }
    }
    
    private Size getRecordSizeByHasProfile(final int n) {
        Size resolution_480P = SizeUtil.RESOLUTION_480P;
        CamcorderProfile camcorderProfile;
        if (this.mCamcorderProfileHelper.hasProfile(n, 10)) {
            camcorderProfile = this.mCamcorderProfileHelper.get(n, 10);
        }
        else if (this.mCamcorderProfileHelper.hasProfile(n, 8)) {
            camcorderProfile = this.mCamcorderProfileHelper.get(n, 8);
        }
        else if (this.mCamcorderProfileHelper.hasProfile(n, 12)) {
            camcorderProfile = this.mCamcorderProfileHelper.get(n, 12);
        }
        else if (this.mCamcorderProfileHelper.hasProfile(n, 6)) {
            camcorderProfile = this.mCamcorderProfileHelper.get(n, 6);
        }
        else if (this.mCamcorderProfileHelper.hasProfile(n, 5)) {
            camcorderProfile = this.mCamcorderProfileHelper.get(n, 5);
        }
        else if (this.mCamcorderProfileHelper.hasProfile(n, 4)) {
            camcorderProfile = this.mCamcorderProfileHelper.get(n, 4);
        }
        else {
            camcorderProfile = null;
        }
        if (camcorderProfile != null) {
            resolution_480P = new Size(camcorderProfile.videoFrameWidth, camcorderProfile.videoFrameHeight);
        }
        return resolution_480P;
    }
    
    private Size getRecordSizeFromStreamConfigurationMap() {
        final StreamConfigurationMap streamConfigurationMap = this.mCharacteristics.get((android.hardware.camera2.CameraCharacteristics$Key<StreamConfigurationMap>)CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP);
        if (streamConfigurationMap == null) {
            throw new IllegalArgumentException("Can not retrieve SCALER_STREAM_CONFIGURATION_MAP");
        }
        final Size[] outputSizes = streamConfigurationMap.getOutputSizes((Class)MediaRecorder.class);
        if (outputSizes == null) {
            return SizeUtil.RESOLUTION_480P;
        }
        Arrays.sort(outputSizes, new CompareSizesByArea(true));
        for (final Size size : outputSizes) {
            if (size.getWidth() <= SizeUtil.RESOLUTION_1080P.getWidth() && size.getHeight() <= SizeUtil.RESOLUTION_1080P.getHeight()) {
                return size;
            }
        }
        return SizeUtil.RESOLUTION_480P;
    }
    
    private Rational getTargetAspectRatio(final ImageOutputConfig imageOutputConfig) {
        final int value = new TargetAspectRatio().get(this.mCameraId, this.mCharacteristics);
        final Rational rational = null;
        Rational rational2;
        if (value != 0) {
            if (value != 1) {
                if (value != 2) {
                    if (value != 3) {
                        rational2 = rational;
                    }
                    else {
                        final Size targetSize = this.getTargetSize(imageOutputConfig);
                        if (imageOutputConfig.hasTargetAspectRatio()) {
                            final int targetAspectRatio = imageOutputConfig.getTargetAspectRatio();
                            if (targetAspectRatio != 0) {
                                if (targetAspectRatio != 1) {
                                    final StringBuilder sb = new StringBuilder();
                                    sb.append("Undefined target aspect ratio: ");
                                    sb.append(targetAspectRatio);
                                    Logger.e("SupportedSurfaceCombination", sb.toString());
                                    rational2 = rational;
                                }
                                else if (this.mIsSensorLandscapeResolution) {
                                    rational2 = AspectRatioUtil.ASPECT_RATIO_16_9;
                                }
                                else {
                                    rational2 = AspectRatioUtil.ASPECT_RATIO_9_16;
                                }
                            }
                            else if (this.mIsSensorLandscapeResolution) {
                                rational2 = AspectRatioUtil.ASPECT_RATIO_4_3;
                            }
                            else {
                                rational2 = AspectRatioUtil.ASPECT_RATIO_3_4;
                            }
                        }
                        else {
                            rational2 = rational;
                            if (targetSize != null) {
                                rational2 = new Rational(targetSize.getWidth(), targetSize.getHeight());
                            }
                        }
                    }
                }
                else {
                    final Size fetchMaxSize = this.fetchMaxSize(256);
                    rational2 = new Rational(fetchMaxSize.getWidth(), fetchMaxSize.getHeight());
                }
            }
            else if (this.mIsSensorLandscapeResolution) {
                rational2 = AspectRatioUtil.ASPECT_RATIO_16_9;
            }
            else {
                rational2 = AspectRatioUtil.ASPECT_RATIO_9_16;
            }
        }
        else if (this.mIsSensorLandscapeResolution) {
            rational2 = AspectRatioUtil.ASPECT_RATIO_4_3;
        }
        else {
            rational2 = AspectRatioUtil.ASPECT_RATIO_3_4;
        }
        return rational2;
    }
    
    private Size getTargetSize(final ImageOutputConfig imageOutputConfig) {
        return this.flipSizeByRotation(imageOutputConfig.getTargetResolution(null), imageOutputConfig.getTargetRotation(0));
    }
    
    private List<Integer> getUseCasesPriorityOrder(final List<UseCaseConfig<?>> list) {
        final ArrayList list2 = new ArrayList();
        final ArrayList list3 = new ArrayList();
        final Iterator iterator = list.iterator();
        while (iterator.hasNext()) {
            final int surfaceOccupancyPriority = ((UseCaseConfig)iterator.next()).getSurfaceOccupancyPriority(0);
            if (!list3.contains(surfaceOccupancyPriority)) {
                list3.add(surfaceOccupancyPriority);
            }
        }
        Collections.sort((List<Comparable>)list3);
        Collections.reverse(list3);
        for (final int intValue : list3) {
            for (final UseCaseConfig useCaseConfig : list) {
                if (intValue == useCaseConfig.getSurfaceOccupancyPriority(0)) {
                    list2.add(list.indexOf(useCaseConfig));
                }
            }
        }
        return list2;
    }
    
    private Map<Rational, List<Size>> groupSizesByAspectRatio(final List<Size> list) {
        final HashMap hashMap = new HashMap();
        hashMap.put(AspectRatioUtil.ASPECT_RATIO_4_3, new ArrayList());
        hashMap.put(AspectRatioUtil.ASPECT_RATIO_16_9, new ArrayList());
        for (final Size o : list) {
            Rational rational = null;
            for (final Rational rational2 : hashMap.keySet()) {
                if (AspectRatioUtil.hasMatchingAspectRatio(o, rational2)) {
                    final List list2 = (List)hashMap.get(rational2);
                    if (!list2.contains(o)) {
                        list2.add(o);
                    }
                    rational = rational2;
                }
            }
            if (rational == null) {
                hashMap.put(new Rational(o.getWidth(), o.getHeight()), new ArrayList(Collections.singleton(o)));
            }
        }
        return hashMap;
    }
    
    private boolean isRotationNeeded(int n) {
        final Integer n2 = this.mCharacteristics.get((android.hardware.camera2.CameraCharacteristics$Key<Integer>)CameraCharacteristics.SENSOR_ORIENTATION);
        Preconditions.checkNotNull(n2, "Camera HAL in bad state, unable to retrieve the SENSOR_ORIENTATION");
        final int surfaceRotationToDegrees = CameraOrientationUtil.surfaceRotationToDegrees(n);
        final Integer n3 = this.mCharacteristics.get((android.hardware.camera2.CameraCharacteristics$Key<Integer>)CameraCharacteristics.LENS_FACING);
        Preconditions.checkNotNull(n3, "Camera HAL in bad state, unable to retrieve the LENS_FACING");
        n = n3;
        final boolean b = false;
        n = CameraOrientationUtil.getRelativeImageRotation(surfaceRotationToDegrees, n2, 1 == n);
        if (n != 90) {
            final boolean b2 = b;
            if (n != 270) {
                return b2;
            }
        }
        return true;
    }
    
    private boolean isSensorLandscapeResolution() {
        final Size size = this.mCharacteristics.get((android.hardware.camera2.CameraCharacteristics$Key<Size>)CameraCharacteristics.SENSOR_INFO_PIXEL_ARRAY_SIZE);
        boolean b = true;
        if (size != null) {
            b = (size.getWidth() >= size.getHeight() && b);
        }
        return b;
    }
    
    private void refreshPreviewSize() {
        this.mDisplayInfoManager.refresh();
        if (this.mSurfaceSizeDefinition == null) {
            this.generateSurfaceSizeDefinition();
        }
        else {
            this.mSurfaceSizeDefinition = SurfaceSizeDefinition.create(this.mSurfaceSizeDefinition.getAnalysisSize(), this.mDisplayInfoManager.getPreviewSize(), this.mSurfaceSizeDefinition.getRecordSize());
        }
    }
    
    private void removeSupportedSizesByTargetSize(final List<Size> list, final Size size) {
        if (list != null) {
            if (!list.isEmpty()) {
                final ArrayList list2 = new ArrayList();
                int i = 0;
                int n = -1;
                while (i < list.size()) {
                    final Size size2 = list.get(i);
                    if (size2.getWidth() < size.getWidth() || size2.getHeight() < size.getHeight()) {
                        break;
                    }
                    if (n >= 0) {
                        list2.add(list.get(n));
                    }
                    n = i;
                    ++i;
                }
                list.removeAll(list2);
            }
        }
    }
    
    boolean checkSupported(final List<SurfaceConfig> list) {
        final Iterator<SurfaceCombination> iterator = this.mSurfaceCombinations.iterator();
        boolean supported = false;
        while (iterator.hasNext()) {
            final boolean b = supported = iterator.next().isSupported(list);
            if (b) {
                supported = b;
                break;
            }
        }
        return supported;
    }
    
    String getCameraId() {
        return this.mCameraId;
    }
    
    Size getMaxOutputSizeByFormat(final int n) {
        return Collections.max((Collection<? extends Size>)Arrays.asList(this.getAllOutputSizesByFormat(n)), (Comparator<? super Size>)new CompareSizesByArea());
    }
    
    Map<UseCaseConfig<?>, Size> getSuggestedResolutions(final List<AttachedSurfaceInfo> list, final List<UseCaseConfig<?>> list2) {
        this.refreshPreviewSize();
        final ArrayList list3 = new ArrayList();
        final Iterator<AttachedSurfaceInfo> iterator = list.iterator();
        while (iterator.hasNext()) {
            list3.add(iterator.next().getSurfaceConfig());
        }
        final Iterator iterator2 = list2.iterator();
        while (iterator2.hasNext()) {
            list3.add(SurfaceConfig.transformSurfaceConfig(((UseCaseConfig<?>)iterator2.next()).getInputFormat(), new Size(640, 480), this.mSurfaceSizeDefinition));
        }
        if (!this.checkSupported(list3)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("No supported surface combination is found for camera device - Id : ");
            sb.append(this.mCameraId);
            sb.append(".  May be attempting to bind too many use cases. Existing surfaces: ");
            sb.append(list);
            sb.append(" New configs: ");
            sb.append(list2);
            throw new IllegalArgumentException(sb.toString());
        }
        final List<Integer> useCasesPriorityOrder = this.getUseCasesPriorityOrder(list2);
        final ArrayList list4 = new ArrayList();
        final Iterator<Integer> iterator3 = useCasesPriorityOrder.iterator();
        while (iterator3.hasNext()) {
            list4.add(this.getSupportedOutputSizes((UseCaseConfig<?>)list2.get(iterator3.next())));
        }
        final List<List<Size>> allPossibleSizeArrangements = this.getAllPossibleSizeArrangements(list4);
        final Map<UseCaseConfig<?>, Size> map = null;
        final Iterator<List> iterator4 = allPossibleSizeArrangements.iterator();
    Label_0467:
        while (true) {
            ArrayList<SurfaceConfig> list5;
            List list6;
            do {
                final Object o = map;
                if (iterator4.hasNext()) {
                    list6 = iterator4.next();
                    list5 = new ArrayList<SurfaceConfig>();
                    final Iterator<AttachedSurfaceInfo> iterator5 = list.iterator();
                    while (iterator5.hasNext()) {
                        list5.add(iterator5.next().getSurfaceConfig());
                    }
                    for (int i = 0; i < list6.size(); ++i) {
                        list5.add(SurfaceConfig.transformSurfaceConfig(((UseCaseConfig<?>)list2.get((int)useCasesPriorityOrder.get(i))).getInputFormat(), (Size)list6.get(i), this.mSurfaceSizeDefinition));
                    }
                }
                else {
                    if (o != null) {
                        return (Map<UseCaseConfig<?>, Size>)o;
                    }
                    final StringBuilder sb2 = new StringBuilder();
                    sb2.append("No supported surface combination is found for camera device - Id : ");
                    sb2.append(this.mCameraId);
                    sb2.append(" and Hardware level: ");
                    sb2.append(this.mHardwareLevel);
                    sb2.append(". May be the specified resolution is too large and not supported. Existing surfaces: ");
                    sb2.append(list);
                    sb2.append(" New configs: ");
                    sb2.append(list2);
                    throw new IllegalArgumentException(sb2.toString());
                }
            } while (!this.checkSupported(list5));
            final HashMap<UseCaseConfig, Size> hashMap = new HashMap<UseCaseConfig, Size>();
            final Iterator iterator6 = list2.iterator();
            while (true) {
                final Object o = hashMap;
                if (!iterator6.hasNext()) {
                    continue Label_0467;
                }
                final UseCaseConfig useCaseConfig = (UseCaseConfig)iterator6.next();
                hashMap.put(useCaseConfig, (Size)list6.get(useCasesPriorityOrder.indexOf(list2.indexOf(useCaseConfig))));
            }
            break;
        }
    }
    
    List<Size> getSupportedOutputSizes(final UseCaseConfig<?> useCaseConfig) {
        final int inputFormat = useCaseConfig.getInputFormat();
        final ImageOutputConfig imageOutputConfig = (ImageOutputConfig)useCaseConfig;
        Size[] a;
        if ((a = this.getCustomizedSupportSizesFromConfig(inputFormat, imageOutputConfig)) == null) {
            a = this.getAllOutputSizesByFormat(inputFormat);
        }
        final ArrayList<Size> list = new ArrayList<Size>();
        final Size maxResolution = imageOutputConfig.getMaxResolution(null);
        final Size maxOutputSizeByFormat = this.getMaxOutputSizeByFormat(inputFormat);
        Size size = null;
        Label_0090: {
            if (maxResolution != null) {
                size = maxResolution;
                if (SizeUtil.getArea(maxOutputSizeByFormat) >= SizeUtil.getArea(maxResolution)) {
                    break Label_0090;
                }
            }
            size = maxOutputSizeByFormat;
        }
        Arrays.sort(a, new CompareSizesByArea(true));
        final Size targetSize = this.getTargetSize(imageOutputConfig);
        final Size resolution_VGA = SizeUtil.RESOLUTION_VGA;
        final int area = SizeUtil.getArea(SizeUtil.RESOLUTION_VGA);
        Size resolution_ZERO;
        if (SizeUtil.getArea(size) < area) {
            resolution_ZERO = SizeUtil.RESOLUTION_ZERO;
        }
        else {
            resolution_ZERO = resolution_VGA;
            if (targetSize != null) {
                resolution_ZERO = resolution_VGA;
                if (SizeUtil.getArea(targetSize) < area) {
                    resolution_ZERO = targetSize;
                }
            }
        }
        for (final Size size2 : a) {
            if (SizeUtil.getArea(size2) <= SizeUtil.getArea(size) && SizeUtil.getArea(size2) >= SizeUtil.getArea(resolution_ZERO) && !list.contains(size2)) {
                list.add(size2);
            }
        }
        if (!list.isEmpty()) {
            final Rational targetAspectRatio = this.getTargetAspectRatio(imageOutputConfig);
            Size defaultResolution;
            if ((defaultResolution = targetSize) == null) {
                defaultResolution = imageOutputConfig.getDefaultResolution(null);
            }
            final ArrayList list2 = new ArrayList<Size>();
            new HashMap();
            if (targetAspectRatio == null) {
                list2.addAll(list);
                if (defaultResolution != null) {
                    this.removeSupportedSizesByTargetSize(list2, defaultResolution);
                }
            }
            else {
                final Map<Rational, List<Size>> groupSizesByAspectRatio = this.groupSizesByAspectRatio(list);
                if (defaultResolution != null) {
                    final Iterator<Rational> iterator = groupSizesByAspectRatio.keySet().iterator();
                    while (iterator.hasNext()) {
                        this.removeSupportedSizesByTargetSize(groupSizesByAspectRatio.get(iterator.next()), defaultResolution);
                    }
                }
                final ArrayList list3 = new ArrayList<Object>(groupSizesByAspectRatio.keySet());
                Collections.sort((List<E>)list3, (Comparator<? super E>)new AspectRatioUtil.CompareAspectRatiosByDistanceToTargetRatio(targetAspectRatio));
                final Iterator<Object> iterator2 = list3.iterator();
                while (iterator2.hasNext()) {
                    for (final Size size3 : groupSizesByAspectRatio.get(iterator2.next())) {
                        if (!list2.contains(size3)) {
                            list2.add(size3);
                        }
                    }
                }
            }
            return this.mResolutionCorrector.insertOrPrioritize(SurfaceConfig.getConfigType(useCaseConfig.getInputFormat()), list2);
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Can not get supported output size under supported maximum for the format: ");
        sb.append(inputFormat);
        throw new IllegalArgumentException(sb.toString());
    }
    
    boolean isBurstCaptureSupported() {
        return this.mIsBurstCaptureSupported;
    }
    
    boolean isRawSupported() {
        return this.mIsRawSupported;
    }
    
    SurfaceConfig transformSurfaceConfig(final int n, final Size size) {
        return SurfaceConfig.transformSurfaceConfig(n, size, this.mSurfaceSizeDefinition);
    }
}

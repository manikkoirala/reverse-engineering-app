// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.internal;

import android.hardware.camera2.CameraDevice;
import java.util.Iterator;
import java.util.ArrayList;
import java.util.Arrays;
import android.hardware.camera2.CameraDevice$StateCallback;
import java.util.List;

public final class CameraDeviceStateCallbacks
{
    private CameraDeviceStateCallbacks() {
    }
    
    public static CameraDevice$StateCallback createComboCallback(final List<CameraDevice$StateCallback> list) {
        if (list.isEmpty()) {
            return createNoOpCallback();
        }
        if (list.size() == 1) {
            return list.get(0);
        }
        return new ComboDeviceStateCallback(list);
    }
    
    public static CameraDevice$StateCallback createComboCallback(final CameraDevice$StateCallback... a) {
        return createComboCallback(Arrays.asList(a));
    }
    
    public static CameraDevice$StateCallback createNoOpCallback() {
        return new NoOpDeviceStateCallback();
    }
    
    private static final class ComboDeviceStateCallback extends CameraDevice$StateCallback
    {
        private final List<CameraDevice$StateCallback> mCallbacks;
        
        ComboDeviceStateCallback(final List<CameraDevice$StateCallback> list) {
            this.mCallbacks = new ArrayList<CameraDevice$StateCallback>();
            for (final CameraDevice$StateCallback cameraDevice$StateCallback : list) {
                if (!(cameraDevice$StateCallback instanceof NoOpDeviceStateCallback)) {
                    this.mCallbacks.add(cameraDevice$StateCallback);
                }
            }
        }
        
        public void onClosed(final CameraDevice cameraDevice) {
            final Iterator<CameraDevice$StateCallback> iterator = this.mCallbacks.iterator();
            while (iterator.hasNext()) {
                iterator.next().onClosed(cameraDevice);
            }
        }
        
        public void onDisconnected(final CameraDevice cameraDevice) {
            final Iterator<CameraDevice$StateCallback> iterator = this.mCallbacks.iterator();
            while (iterator.hasNext()) {
                iterator.next().onDisconnected(cameraDevice);
            }
        }
        
        public void onError(final CameraDevice cameraDevice, final int n) {
            final Iterator<CameraDevice$StateCallback> iterator = this.mCallbacks.iterator();
            while (iterator.hasNext()) {
                iterator.next().onError(cameraDevice, n);
            }
        }
        
        public void onOpened(final CameraDevice cameraDevice) {
            final Iterator<CameraDevice$StateCallback> iterator = this.mCallbacks.iterator();
            while (iterator.hasNext()) {
                iterator.next().onOpened(cameraDevice);
            }
        }
    }
    
    static final class NoOpDeviceStateCallback extends CameraDevice$StateCallback
    {
        public void onClosed(final CameraDevice cameraDevice) {
        }
        
        public void onDisconnected(final CameraDevice cameraDevice) {
        }
        
        public void onError(final CameraDevice cameraDevice, final int n) {
        }
        
        public void onOpened(final CameraDevice cameraDevice) {
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.internal;

import android.view.Surface;
import androidx.camera.core.impl.utils.futures.Futures;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCaptureSession$CaptureCallback;
import android.hardware.camera2.CaptureRequest;
import androidx.camera.camera2.internal.compat.params.SessionConfigurationCompat;
import android.hardware.camera2.CameraDevice;
import androidx.camera.core.Logger;
import android.os.Handler;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.Executor;
import androidx.camera.core.impl.Quirks;
import androidx.camera.camera2.internal.compat.workaround.WaitForRepeatingRequestStart;
import com.google.common.util.concurrent.ListenableFuture;
import androidx.camera.camera2.internal.compat.workaround.ForceCloseCaptureSession;
import androidx.camera.core.impl.DeferrableSurface;
import java.util.List;
import androidx.camera.camera2.internal.compat.workaround.ForceCloseDeferrableSurface;

class SynchronizedCaptureSessionImpl extends SynchronizedCaptureSessionBaseImpl
{
    private static final String TAG = "SyncCaptureSessionImpl";
    private final ForceCloseDeferrableSurface mCloseSurfaceQuirk;
    private List<DeferrableSurface> mDeferrableSurfaces;
    private final ForceCloseCaptureSession mForceCloseSessionQuirk;
    private final Object mObjectLock;
    ListenableFuture<Void> mOpeningCaptureSession;
    private final WaitForRepeatingRequestStart mWaitForOtherSessionCompleteQuirk;
    
    SynchronizedCaptureSessionImpl(final Quirks quirks, final Quirks quirks2, final CaptureSessionRepository captureSessionRepository, final Executor executor, final ScheduledExecutorService scheduledExecutorService, final Handler handler) {
        super(captureSessionRepository, executor, scheduledExecutorService, handler);
        this.mObjectLock = new Object();
        this.mCloseSurfaceQuirk = new ForceCloseDeferrableSurface(quirks, quirks2);
        this.mWaitForOtherSessionCompleteQuirk = new WaitForRepeatingRequestStart(quirks);
        this.mForceCloseSessionQuirk = new ForceCloseCaptureSession(quirks2);
    }
    
    @Override
    public void close() {
        this.debugLog("Session call close()");
        this.mWaitForOtherSessionCompleteQuirk.onSessionEnd();
        this.mWaitForOtherSessionCompleteQuirk.getStartStreamFuture().addListener((Runnable)new SynchronizedCaptureSessionImpl$$ExternalSyntheticLambda0(this), this.getExecutor());
    }
    
    void debugLog(final String str) {
        final StringBuilder sb = new StringBuilder();
        sb.append("[");
        sb.append(this);
        sb.append("] ");
        sb.append(str);
        Logger.d("SyncCaptureSessionImpl", sb.toString());
    }
    
    @Override
    public ListenableFuture<Void> getOpeningBlocker() {
        return this.mWaitForOtherSessionCompleteQuirk.getStartStreamFuture();
    }
    
    @Override
    public void onClosed(final SynchronizedCaptureSession synchronizedCaptureSession) {
        synchronized (this.mObjectLock) {
            this.mCloseSurfaceQuirk.onSessionEnd(this.mDeferrableSurfaces);
            monitorexit(this.mObjectLock);
            this.debugLog("onClosed()");
            super.onClosed(synchronizedCaptureSession);
        }
    }
    
    @Override
    public void onConfigured(final SynchronizedCaptureSession synchronizedCaptureSession) {
        this.debugLog("Session onConfigured()");
        this.mForceCloseSessionQuirk.onSessionConfigured(synchronizedCaptureSession, this.mCaptureSessionRepository.getCreatingCaptureSessions(), this.mCaptureSessionRepository.getCaptureSessions(), (ForceCloseCaptureSession.OnConfigured)new SynchronizedCaptureSessionImpl$$ExternalSyntheticLambda2(this));
    }
    
    @Override
    public ListenableFuture<Void> openCaptureSession(final CameraDevice cameraDevice, final SessionConfigurationCompat sessionConfigurationCompat, final List<DeferrableSurface> list) {
        synchronized (this.mObjectLock) {
            final ListenableFuture<Void> openCaptureSession = this.mWaitForOtherSessionCompleteQuirk.openCaptureSession(cameraDevice, sessionConfigurationCompat, list, this.mCaptureSessionRepository.getClosingCaptureSession(), (WaitForRepeatingRequestStart.OpenCaptureSession)new SynchronizedCaptureSessionImpl$$ExternalSyntheticLambda1(this));
            this.mOpeningCaptureSession = openCaptureSession;
            return Futures.nonCancellationPropagating(openCaptureSession);
        }
    }
    
    @Override
    public int setSingleRepeatingRequest(final CaptureRequest captureRequest, final CameraCaptureSession$CaptureCallback cameraCaptureSession$CaptureCallback) throws CameraAccessException {
        return this.mWaitForOtherSessionCompleteQuirk.setSingleRepeatingRequest(captureRequest, cameraCaptureSession$CaptureCallback, (WaitForRepeatingRequestStart.SingleRepeatingRequest)new SynchronizedCaptureSessionImpl$$ExternalSyntheticLambda3(this));
    }
    
    @Override
    public ListenableFuture<List<Surface>> startWithDeferrableSurface(final List<DeferrableSurface> mDeferrableSurfaces, final long n) {
        synchronized (this.mObjectLock) {
            this.mDeferrableSurfaces = mDeferrableSurfaces;
            return super.startWithDeferrableSurface(mDeferrableSurfaces, n);
        }
    }
    
    @Override
    public boolean stop() {
        synchronized (this.mObjectLock) {
            if (this.isCameraCaptureSessionOpen()) {
                this.mCloseSurfaceQuirk.onSessionEnd(this.mDeferrableSurfaces);
            }
            else {
                final ListenableFuture<Void> mOpeningCaptureSession = this.mOpeningCaptureSession;
                if (mOpeningCaptureSession != null) {
                    mOpeningCaptureSession.cancel(true);
                }
            }
            return super.stop();
        }
    }
}

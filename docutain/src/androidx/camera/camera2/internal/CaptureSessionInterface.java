// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.internal;

import com.google.common.util.concurrent.ListenableFuture;
import android.hardware.camera2.CameraDevice;
import androidx.camera.core.impl.SessionConfig;
import androidx.camera.core.impl.CaptureConfig;
import java.util.List;

interface CaptureSessionInterface
{
    void cancelIssuedCaptureRequests();
    
    void close();
    
    List<CaptureConfig> getCaptureConfigs();
    
    SessionConfig getSessionConfig();
    
    void issueCaptureRequests(final List<CaptureConfig> p0);
    
    ListenableFuture<Void> open(final SessionConfig p0, final CameraDevice p1, final SynchronizedCaptureSessionOpener p2);
    
    ListenableFuture<Void> release(final boolean p0);
    
    void setSessionConfig(final SessionConfig p0);
}

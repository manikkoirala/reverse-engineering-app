// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.internal;

import androidx.camera.camera2.internal.compat.CameraCaptureSessionCompat;
import com.google.common.util.concurrent.ListenableFuture;
import android.view.Surface;
import android.hardware.camera2.CameraDevice;
import java.util.concurrent.Executor;
import android.hardware.camera2.CameraCaptureSession$CaptureCallback;
import android.hardware.camera2.CaptureRequest;
import java.util.List;
import android.hardware.camera2.CameraAccessException;

public interface SynchronizedCaptureSession
{
    void abortCaptures() throws CameraAccessException;
    
    int captureBurstRequests(final List<CaptureRequest> p0, final CameraCaptureSession$CaptureCallback p1) throws CameraAccessException;
    
    int captureBurstRequests(final List<CaptureRequest> p0, final Executor p1, final CameraCaptureSession$CaptureCallback p2) throws CameraAccessException;
    
    int captureSingleRequest(final CaptureRequest p0, final CameraCaptureSession$CaptureCallback p1) throws CameraAccessException;
    
    int captureSingleRequest(final CaptureRequest p0, final Executor p1, final CameraCaptureSession$CaptureCallback p2) throws CameraAccessException;
    
    void close();
    
    void finishClose();
    
    CameraDevice getDevice();
    
    Surface getInputSurface();
    
    ListenableFuture<Void> getOpeningBlocker();
    
    StateCallback getStateCallback();
    
    int setRepeatingBurstRequests(final List<CaptureRequest> p0, final CameraCaptureSession$CaptureCallback p1) throws CameraAccessException;
    
    int setRepeatingBurstRequests(final List<CaptureRequest> p0, final Executor p1, final CameraCaptureSession$CaptureCallback p2) throws CameraAccessException;
    
    int setSingleRepeatingRequest(final CaptureRequest p0, final CameraCaptureSession$CaptureCallback p1) throws CameraAccessException;
    
    int setSingleRepeatingRequest(final CaptureRequest p0, final Executor p1, final CameraCaptureSession$CaptureCallback p2) throws CameraAccessException;
    
    void stopRepeating() throws CameraAccessException;
    
    CameraCaptureSessionCompat toCameraCaptureSessionCompat();
    
    public abstract static class StateCallback
    {
        void onActive(final SynchronizedCaptureSession synchronizedCaptureSession) {
        }
        
        void onCaptureQueueEmpty(final SynchronizedCaptureSession synchronizedCaptureSession) {
        }
        
        public void onClosed(final SynchronizedCaptureSession synchronizedCaptureSession) {
        }
        
        public void onConfigureFailed(final SynchronizedCaptureSession synchronizedCaptureSession) {
        }
        
        void onConfigured(final SynchronizedCaptureSession synchronizedCaptureSession) {
        }
        
        void onReady(final SynchronizedCaptureSession synchronizedCaptureSession) {
        }
        
        void onSessionFinished(final SynchronizedCaptureSession synchronizedCaptureSession) {
        }
        
        void onSurfacePrepared(final SynchronizedCaptureSession synchronizedCaptureSession, final Surface surface) {
        }
    }
}

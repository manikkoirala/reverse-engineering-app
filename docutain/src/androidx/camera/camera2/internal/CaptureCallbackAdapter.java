// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.internal;

import androidx.camera.core.impl.CameraCaptureFailure;
import android.hardware.camera2.CaptureFailure;
import androidx.camera.core.impl.CameraCaptureResult;
import android.hardware.camera2.CaptureResult;
import androidx.core.util.Preconditions;
import androidx.camera.core.impl.TagBundle;
import android.hardware.camera2.TotalCaptureResult;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.CameraCaptureSession;
import androidx.camera.core.impl.CameraCaptureCallback;
import android.hardware.camera2.CameraCaptureSession$CaptureCallback;

final class CaptureCallbackAdapter extends CameraCaptureSession$CaptureCallback
{
    private final CameraCaptureCallback mCameraCaptureCallback;
    
    CaptureCallbackAdapter(final CameraCaptureCallback mCameraCaptureCallback) {
        if (mCameraCaptureCallback != null) {
            this.mCameraCaptureCallback = mCameraCaptureCallback;
            return;
        }
        throw new NullPointerException("cameraCaptureCallback is null");
    }
    
    public void onCaptureCompleted(final CameraCaptureSession cameraCaptureSession, final CaptureRequest captureRequest, final TotalCaptureResult totalCaptureResult) {
        super.onCaptureCompleted(cameraCaptureSession, captureRequest, totalCaptureResult);
        final Object tag = captureRequest.getTag();
        TagBundle emptyBundle;
        if (tag != null) {
            Preconditions.checkArgument(tag instanceof TagBundle, (Object)"The tagBundle object from the CaptureResult is not a TagBundle object.");
            emptyBundle = (TagBundle)tag;
        }
        else {
            emptyBundle = TagBundle.emptyBundle();
        }
        this.mCameraCaptureCallback.onCaptureCompleted(new Camera2CameraCaptureResult(emptyBundle, (CaptureResult)totalCaptureResult));
    }
    
    public void onCaptureFailed(final CameraCaptureSession cameraCaptureSession, final CaptureRequest captureRequest, final CaptureFailure captureFailure) {
        super.onCaptureFailed(cameraCaptureSession, captureRequest, captureFailure);
        this.mCameraCaptureCallback.onCaptureFailed(new CameraCaptureFailure(CameraCaptureFailure.Reason.ERROR));
    }
}

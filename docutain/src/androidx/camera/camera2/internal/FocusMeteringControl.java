// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.internal;

import androidx.camera.core.impl.CameraControlInternal;
import androidx.camera.core.impl.CameraCaptureFailure;
import androidx.camera.core.impl.CameraCaptureCallback;
import android.hardware.camera2.CaptureResult;
import android.hardware.camera2.TotalCaptureResult;
import com.google.common.util.concurrent.ListenableFuture;
import androidx.camera.core.impl.Config;
import android.os.Build$VERSION;
import androidx.camera.core.impl.CaptureConfig;
import android.hardware.camera2.CaptureRequest$Key;
import android.hardware.camera2.CaptureRequest;
import androidx.camera.camera2.impl.Camera2ImplConfig;
import java.util.Iterator;
import java.util.Collections;
import java.util.ArrayList;
import java.util.List;
import android.graphics.PointF;
import androidx.camera.core.MeteringPoint;
import android.graphics.Rect;
import androidx.camera.core.CameraControl;
import java.util.concurrent.TimeUnit;
import androidx.camera.core.impl.CameraCaptureResult;
import androidx.camera.core.FocusMeteringAction;
import androidx.camera.core.impl.Quirks;
import java.util.concurrent.ScheduledExecutorService;
import androidx.camera.core.FocusMeteringResult;
import androidx.concurrent.futures.CallbackToFutureAdapter;
import android.util.Rational;
import androidx.camera.camera2.internal.compat.workaround.MeteringRegionCorrection;
import java.util.concurrent.Executor;
import java.util.concurrent.ScheduledFuture;
import android.hardware.camera2.params.MeteringRectangle;

class FocusMeteringControl
{
    static final long AUTO_FOCUS_TIMEOUT_DURATION = 5000L;
    private static final MeteringRectangle[] EMPTY_RECTANGLES;
    private MeteringRectangle[] mAeRects;
    private MeteringRectangle[] mAfRects;
    private ScheduledFuture<?> mAutoCancelHandle;
    private ScheduledFuture<?> mAutoFocusTimeoutHandle;
    private MeteringRectangle[] mAwbRects;
    private final Camera2CameraControlImpl mCameraControl;
    Integer mCurrentAfState;
    final Executor mExecutor;
    long mFocusTimeoutCounter;
    private volatile boolean mIsActive;
    boolean mIsAutoFocusCompleted;
    boolean mIsFocusSuccessful;
    private boolean mIsInAfAutoMode;
    private final MeteringRegionCorrection mMeteringRegionCorrection;
    private volatile Rational mPreviewAspectRatio;
    CallbackToFutureAdapter.Completer<FocusMeteringResult> mRunningActionCompleter;
    CallbackToFutureAdapter.Completer<Void> mRunningCancelCompleter;
    private final ScheduledExecutorService mScheduler;
    private Camera2CameraControlImpl.CaptureResultListener mSessionListenerForCancel;
    private Camera2CameraControlImpl.CaptureResultListener mSessionListenerForFocus;
    private int mTemplate;
    
    static {
        EMPTY_RECTANGLES = new MeteringRectangle[0];
    }
    
    FocusMeteringControl(final Camera2CameraControlImpl mCameraControl, final ScheduledExecutorService mScheduler, final Executor mExecutor, final Quirks quirks) {
        this.mIsActive = false;
        this.mPreviewAspectRatio = null;
        this.mIsInAfAutoMode = false;
        this.mCurrentAfState = 0;
        this.mFocusTimeoutCounter = 0L;
        this.mIsAutoFocusCompleted = false;
        this.mIsFocusSuccessful = false;
        this.mTemplate = 1;
        this.mSessionListenerForFocus = null;
        this.mSessionListenerForCancel = null;
        final MeteringRectangle[] empty_RECTANGLES = FocusMeteringControl.EMPTY_RECTANGLES;
        this.mAfRects = empty_RECTANGLES;
        this.mAeRects = empty_RECTANGLES;
        this.mAwbRects = empty_RECTANGLES;
        this.mRunningActionCompleter = null;
        this.mRunningCancelCompleter = null;
        this.mCameraControl = mCameraControl;
        this.mExecutor = mExecutor;
        this.mScheduler = mScheduler;
        this.mMeteringRegionCorrection = new MeteringRegionCorrection(quirks);
    }
    
    private void clearAutoFocusTimeoutHandle() {
        final ScheduledFuture<?> mAutoFocusTimeoutHandle = this.mAutoFocusTimeoutHandle;
        if (mAutoFocusTimeoutHandle != null) {
            mAutoFocusTimeoutHandle.cancel(true);
            this.mAutoFocusTimeoutHandle = null;
        }
    }
    
    private void completeCancelFuture() {
        final CallbackToFutureAdapter.Completer<Void> mRunningCancelCompleter = this.mRunningCancelCompleter;
        if (mRunningCancelCompleter != null) {
            mRunningCancelCompleter.set(null);
            this.mRunningCancelCompleter = null;
        }
    }
    
    private void disableAutoCancel() {
        final ScheduledFuture<?> mAutoCancelHandle = this.mAutoCancelHandle;
        if (mAutoCancelHandle != null) {
            mAutoCancelHandle.cancel(true);
            this.mAutoCancelHandle = null;
        }
    }
    
    private void executeMeteringAction(final MeteringRectangle[] mAfRects, final MeteringRectangle[] mAeRects, final MeteringRectangle[] mAwbRects, final FocusMeteringAction focusMeteringAction, final long n) {
        this.mCameraControl.removeCaptureResultListener(this.mSessionListenerForFocus);
        this.disableAutoCancel();
        this.clearAutoFocusTimeoutHandle();
        this.mAfRects = mAfRects;
        this.mAeRects = mAeRects;
        this.mAwbRects = mAwbRects;
        long n2;
        if (this.shouldTriggerAF()) {
            this.mIsInAfAutoMode = true;
            this.mIsAutoFocusCompleted = false;
            this.mIsFocusSuccessful = false;
            n2 = this.mCameraControl.updateSessionConfigSynchronous();
            this.triggerAf(null, true);
        }
        else {
            this.mIsInAfAutoMode = false;
            this.mIsAutoFocusCompleted = true;
            this.mIsFocusSuccessful = false;
            n2 = this.mCameraControl.updateSessionConfigSynchronous();
        }
        this.mCurrentAfState = 0;
        final FocusMeteringControl$$ExternalSyntheticLambda6 mSessionListenerForFocus = new FocusMeteringControl$$ExternalSyntheticLambda6(this, this.isAfModeSupported(), n2);
        this.mSessionListenerForFocus = mSessionListenerForFocus;
        this.mCameraControl.addCaptureResultListener((Camera2CameraControlImpl.CaptureResultListener)mSessionListenerForFocus);
        final long mFocusTimeoutCounter = this.mFocusTimeoutCounter + 1L;
        this.mFocusTimeoutCounter = mFocusTimeoutCounter;
        this.mAutoFocusTimeoutHandle = this.mScheduler.schedule(new FocusMeteringControl$$ExternalSyntheticLambda7(this, mFocusTimeoutCounter), n, TimeUnit.MILLISECONDS);
        if (focusMeteringAction.isAutoCancelEnabled()) {
            this.mAutoCancelHandle = this.mScheduler.schedule(new FocusMeteringControl$$ExternalSyntheticLambda8(this, mFocusTimeoutCounter), focusMeteringAction.getAutoCancelDurationInMillis(), TimeUnit.MILLISECONDS);
        }
    }
    
    private void failActionFuture(final String s) {
        this.mCameraControl.removeCaptureResultListener(this.mSessionListenerForFocus);
        final CallbackToFutureAdapter.Completer<FocusMeteringResult> mRunningActionCompleter = this.mRunningActionCompleter;
        if (mRunningActionCompleter != null) {
            mRunningActionCompleter.setException(new CameraControl.OperationCanceledException(s));
            this.mRunningActionCompleter = null;
        }
    }
    
    private void failCancelFuture(final String s) {
        this.mCameraControl.removeCaptureResultListener(this.mSessionListenerForCancel);
        final CallbackToFutureAdapter.Completer<Void> mRunningCancelCompleter = this.mRunningCancelCompleter;
        if (mRunningCancelCompleter != null) {
            mRunningCancelCompleter.setException(new CameraControl.OperationCanceledException(s));
            this.mRunningCancelCompleter = null;
        }
    }
    
    private Rational getDefaultAspectRatio() {
        if (this.mPreviewAspectRatio != null) {
            return this.mPreviewAspectRatio;
        }
        final Rect cropSensorRegion = this.mCameraControl.getCropSensorRegion();
        return new Rational(cropSensorRegion.width(), cropSensorRegion.height());
    }
    
    private static PointF getFovAdjustedPoint(final MeteringPoint meteringPoint, final Rational rational, Rational surfaceAspectRatio, final int n, final MeteringRegionCorrection meteringRegionCorrection) {
        if (meteringPoint.getSurfaceAspectRatio() != null) {
            surfaceAspectRatio = meteringPoint.getSurfaceAspectRatio();
        }
        final PointF correctedPoint = meteringRegionCorrection.getCorrectedPoint(meteringPoint, n);
        if (!surfaceAspectRatio.equals((Object)rational)) {
            if (surfaceAspectRatio.compareTo(rational) > 0) {
                final float n2 = (float)(surfaceAspectRatio.doubleValue() / rational.doubleValue());
                correctedPoint.y = ((float)((n2 - 1.0) / 2.0) + correctedPoint.y) * (1.0f / n2);
            }
            else {
                final float n3 = (float)(rational.doubleValue() / surfaceAspectRatio.doubleValue());
                correctedPoint.x = ((float)((n3 - 1.0) / 2.0) + correctedPoint.x) * (1.0f / n3);
            }
        }
        return correctedPoint;
    }
    
    private static MeteringRectangle getMeteringRect(final MeteringPoint meteringPoint, final PointF pointF, final Rect rect) {
        final int n = (int)(rect.left + pointF.x * rect.width());
        final int n2 = (int)(rect.top + pointF.y * rect.height());
        final int n3 = (int)(meteringPoint.getSize() * rect.width());
        final int n4 = (int)(meteringPoint.getSize() * rect.height());
        final int n5 = n3 / 2;
        final int n6 = n4 / 2;
        final Rect rect2 = new Rect(n - n5, n2 - n6, n + n5, n2 + n6);
        rect2.left = rangeLimit(rect2.left, rect.right, rect.left);
        rect2.right = rangeLimit(rect2.right, rect.right, rect.left);
        rect2.top = rangeLimit(rect2.top, rect.bottom, rect.top);
        rect2.bottom = rangeLimit(rect2.bottom, rect.bottom, rect.top);
        return new MeteringRectangle(rect2, 1000);
    }
    
    private List<MeteringRectangle> getMeteringRectangles(final List<MeteringPoint> list, final int n, final Rational rational, final Rect rect, final int n2) {
        if (!list.isEmpty() && n != 0) {
            final ArrayList list2 = new ArrayList();
            final Rational rational2 = new Rational(rect.width(), rect.height());
            for (final MeteringPoint meteringPoint : list) {
                if (list2.size() == n) {
                    break;
                }
                if (!isValid(meteringPoint)) {
                    continue;
                }
                final MeteringRectangle meteringRect = getMeteringRect(meteringPoint, getFovAdjustedPoint(meteringPoint, rational2, rational, n2, this.mMeteringRegionCorrection), rect);
                if (meteringRect.getWidth() == 0) {
                    continue;
                }
                if (meteringRect.getHeight() == 0) {
                    continue;
                }
                list2.add(meteringRect);
            }
            return (List<MeteringRectangle>)Collections.unmodifiableList((List<?>)list2);
        }
        return Collections.emptyList();
    }
    
    private boolean isAfModeSupported() {
        final Camera2CameraControlImpl mCameraControl = this.mCameraControl;
        boolean b = true;
        if (mCameraControl.getSupportedAfMode(1) != 1) {
            b = false;
        }
        return b;
    }
    
    private static boolean isValid(final MeteringPoint meteringPoint) {
        return meteringPoint.getX() >= 0.0f && meteringPoint.getX() <= 1.0f && meteringPoint.getY() >= 0.0f && meteringPoint.getY() <= 1.0f;
    }
    
    private static int rangeLimit(final int a, final int b, final int b2) {
        return Math.min(Math.max(a, b2), b);
    }
    
    private boolean shouldTriggerAF() {
        return this.mAfRects.length > 0;
    }
    
    void addFocusMeteringOptions(final Camera2ImplConfig.Builder builder) {
        int defaultAfMode;
        if (this.mIsInAfAutoMode) {
            defaultAfMode = 1;
        }
        else {
            defaultAfMode = this.getDefaultAfMode();
        }
        builder.setCaptureRequestOption((android.hardware.camera2.CaptureRequest$Key<Integer>)CaptureRequest.CONTROL_AF_MODE, this.mCameraControl.getSupportedAfMode(defaultAfMode));
        if (this.mAfRects.length != 0) {
            builder.setCaptureRequestOption((android.hardware.camera2.CaptureRequest$Key<MeteringRectangle[]>)CaptureRequest.CONTROL_AF_REGIONS, this.mAfRects);
        }
        if (this.mAeRects.length != 0) {
            builder.setCaptureRequestOption((android.hardware.camera2.CaptureRequest$Key<MeteringRectangle[]>)CaptureRequest.CONTROL_AE_REGIONS, this.mAeRects);
        }
        if (this.mAwbRects.length != 0) {
            builder.setCaptureRequestOption((android.hardware.camera2.CaptureRequest$Key<MeteringRectangle[]>)CaptureRequest.CONTROL_AWB_REGIONS, this.mAwbRects);
        }
    }
    
    void cancelAfAeTrigger(final boolean b, final boolean b2) {
        if (!this.mIsActive) {
            return;
        }
        final CaptureConfig.Builder builder = new CaptureConfig.Builder();
        builder.setUseRepeatingSurface(true);
        builder.setTemplateType(this.mTemplate);
        final Camera2ImplConfig.Builder builder2 = new Camera2ImplConfig.Builder();
        if (b) {
            builder2.setCaptureRequestOption((android.hardware.camera2.CaptureRequest$Key<Integer>)CaptureRequest.CONTROL_AF_TRIGGER, 2);
        }
        if (Build$VERSION.SDK_INT >= 23 && b2) {
            builder2.setCaptureRequestOption((android.hardware.camera2.CaptureRequest$Key<Integer>)CaptureRequest.CONTROL_AE_PRECAPTURE_TRIGGER, 2);
        }
        builder.addImplementationOptions(builder2.build());
        this.mCameraControl.submitCaptureRequestsInternal(Collections.singletonList(builder.build()));
    }
    
    ListenableFuture<Void> cancelFocusAndMetering() {
        return CallbackToFutureAdapter.getFuture((CallbackToFutureAdapter.Resolver<Void>)new FocusMeteringControl$$ExternalSyntheticLambda3(this));
    }
    
    void cancelFocusAndMeteringInternal(final CallbackToFutureAdapter.Completer<Void> mRunningCancelCompleter) {
        this.failCancelFuture("Cancelled by another cancelFocusAndMetering()");
        this.failActionFuture("Cancelled by cancelFocusAndMetering()");
        this.mRunningCancelCompleter = mRunningCancelCompleter;
        this.disableAutoCancel();
        this.clearAutoFocusTimeoutHandle();
        if (this.shouldTriggerAF()) {
            this.cancelAfAeTrigger(true, false);
        }
        final MeteringRectangle[] empty_RECTANGLES = FocusMeteringControl.EMPTY_RECTANGLES;
        this.mAfRects = empty_RECTANGLES;
        this.mAeRects = empty_RECTANGLES;
        this.mAwbRects = empty_RECTANGLES;
        this.mIsInAfAutoMode = false;
        final long updateSessionConfigSynchronous = this.mCameraControl.updateSessionConfigSynchronous();
        if (this.mRunningCancelCompleter != null) {
            final FocusMeteringControl$$ExternalSyntheticLambda9 mSessionListenerForCancel = new FocusMeteringControl$$ExternalSyntheticLambda9(this, this.mCameraControl.getSupportedAfMode(this.getDefaultAfMode()), updateSessionConfigSynchronous);
            this.mSessionListenerForCancel = mSessionListenerForCancel;
            this.mCameraControl.addCaptureResultListener((Camera2CameraControlImpl.CaptureResultListener)mSessionListenerForCancel);
        }
    }
    
    void cancelFocusAndMeteringWithoutAsyncResult() {
        this.cancelFocusAndMeteringInternal(null);
    }
    
    void completeActionFuture(final boolean b) {
        this.clearAutoFocusTimeoutHandle();
        final CallbackToFutureAdapter.Completer<FocusMeteringResult> mRunningActionCompleter = this.mRunningActionCompleter;
        if (mRunningActionCompleter != null) {
            mRunningActionCompleter.set(FocusMeteringResult.create(b));
            this.mRunningActionCompleter = null;
        }
    }
    
    int getDefaultAfMode() {
        if (this.mTemplate != 3) {
            return 4;
        }
        return 3;
    }
    
    boolean isFocusMeteringSupported(final FocusMeteringAction focusMeteringAction) {
        final Rect cropSensorRegion = this.mCameraControl.getCropSensorRegion();
        final Rational defaultAspectRatio = this.getDefaultAspectRatio();
        final List<MeteringRectangle> meteringRectangles = this.getMeteringRectangles(focusMeteringAction.getMeteringPointsAf(), this.mCameraControl.getMaxAfRegionCount(), defaultAspectRatio, cropSensorRegion, 1);
        final List<MeteringRectangle> meteringRectangles2 = this.getMeteringRectangles(focusMeteringAction.getMeteringPointsAe(), this.mCameraControl.getMaxAeRegionCount(), defaultAspectRatio, cropSensorRegion, 2);
        final List<MeteringRectangle> meteringRectangles3 = this.getMeteringRectangles(focusMeteringAction.getMeteringPointsAwb(), this.mCameraControl.getMaxAwbRegionCount(), defaultAspectRatio, cropSensorRegion, 4);
        return !meteringRectangles.isEmpty() || !meteringRectangles2.isEmpty() || !meteringRectangles3.isEmpty();
    }
    
    void setActive(final boolean mIsActive) {
        if (mIsActive == this.mIsActive) {
            return;
        }
        if (!(this.mIsActive = mIsActive)) {
            this.cancelFocusAndMeteringWithoutAsyncResult();
        }
    }
    
    public void setPreviewAspectRatio(final Rational mPreviewAspectRatio) {
        this.mPreviewAspectRatio = mPreviewAspectRatio;
    }
    
    void setTemplate(final int mTemplate) {
        this.mTemplate = mTemplate;
    }
    
    ListenableFuture<FocusMeteringResult> startFocusAndMetering(final FocusMeteringAction focusMeteringAction) {
        return this.startFocusAndMetering(focusMeteringAction, 5000L);
    }
    
    ListenableFuture<FocusMeteringResult> startFocusAndMetering(final FocusMeteringAction focusMeteringAction, final long n) {
        return CallbackToFutureAdapter.getFuture((CallbackToFutureAdapter.Resolver<FocusMeteringResult>)new FocusMeteringControl$$ExternalSyntheticLambda1(this, focusMeteringAction, n));
    }
    
    void startFocusAndMeteringInternal(final CallbackToFutureAdapter.Completer<FocusMeteringResult> mRunningActionCompleter, final FocusMeteringAction focusMeteringAction, final long n) {
        if (!this.mIsActive) {
            mRunningActionCompleter.setException(new CameraControl.OperationCanceledException("Camera is not active."));
            return;
        }
        final Rect cropSensorRegion = this.mCameraControl.getCropSensorRegion();
        final Rational defaultAspectRatio = this.getDefaultAspectRatio();
        final List<MeteringRectangle> meteringRectangles = this.getMeteringRectangles(focusMeteringAction.getMeteringPointsAf(), this.mCameraControl.getMaxAfRegionCount(), defaultAspectRatio, cropSensorRegion, 1);
        final List<MeteringRectangle> meteringRectangles2 = this.getMeteringRectangles(focusMeteringAction.getMeteringPointsAe(), this.mCameraControl.getMaxAeRegionCount(), defaultAspectRatio, cropSensorRegion, 2);
        final List<MeteringRectangle> meteringRectangles3 = this.getMeteringRectangles(focusMeteringAction.getMeteringPointsAwb(), this.mCameraControl.getMaxAwbRegionCount(), defaultAspectRatio, cropSensorRegion, 4);
        if (meteringRectangles.isEmpty() && meteringRectangles2.isEmpty() && meteringRectangles3.isEmpty()) {
            mRunningActionCompleter.setException(new IllegalArgumentException("None of the specified AF/AE/AWB MeteringPoints is supported on this camera."));
            return;
        }
        this.failActionFuture("Cancelled by another startFocusAndMetering()");
        this.failCancelFuture("Cancelled by another startFocusAndMetering()");
        this.disableAutoCancel();
        this.mRunningActionCompleter = mRunningActionCompleter;
        final MeteringRectangle[] empty_RECTANGLES = FocusMeteringControl.EMPTY_RECTANGLES;
        this.executeMeteringAction(meteringRectangles.toArray(empty_RECTANGLES), meteringRectangles2.toArray(empty_RECTANGLES), meteringRectangles3.toArray(empty_RECTANGLES), focusMeteringAction, n);
    }
    
    void triggerAePrecapture(final CallbackToFutureAdapter.Completer<Void> completer) {
        if (!this.mIsActive) {
            if (completer != null) {
                completer.setException(new CameraControl.OperationCanceledException("Camera is not active."));
            }
            return;
        }
        final CaptureConfig.Builder builder = new CaptureConfig.Builder();
        builder.setTemplateType(this.mTemplate);
        builder.setUseRepeatingSurface(true);
        final Camera2ImplConfig.Builder builder2 = new Camera2ImplConfig.Builder();
        builder2.setCaptureRequestOption((android.hardware.camera2.CaptureRequest$Key<Integer>)CaptureRequest.CONTROL_AE_PRECAPTURE_TRIGGER, 1);
        builder.addImplementationOptions(builder2.build());
        builder.addCameraCaptureCallback(new CameraCaptureCallback(this, completer) {
            final FocusMeteringControl this$0;
            final CallbackToFutureAdapter.Completer val$completer;
            
            @Override
            public void onCaptureCancelled() {
                final CallbackToFutureAdapter.Completer val$completer = this.val$completer;
                if (val$completer != null) {
                    val$completer.setException(new CameraControl.OperationCanceledException("Camera is closed"));
                }
            }
            
            @Override
            public void onCaptureCompleted(final CameraCaptureResult cameraCaptureResult) {
                final CallbackToFutureAdapter.Completer val$completer = this.val$completer;
                if (val$completer != null) {
                    val$completer.set(null);
                }
            }
            
            @Override
            public void onCaptureFailed(final CameraCaptureFailure cameraCaptureFailure) {
                final CallbackToFutureAdapter.Completer val$completer = this.val$completer;
                if (val$completer != null) {
                    val$completer.setException(new CameraControlInternal.CameraControlException(cameraCaptureFailure));
                }
            }
        });
        this.mCameraControl.submitCaptureRequestsInternal(Collections.singletonList(builder.build()));
    }
    
    void triggerAf(final CallbackToFutureAdapter.Completer<CameraCaptureResult> completer, final boolean b) {
        if (!this.mIsActive) {
            if (completer != null) {
                completer.setException(new CameraControl.OperationCanceledException("Camera is not active."));
            }
            return;
        }
        final CaptureConfig.Builder builder = new CaptureConfig.Builder();
        builder.setTemplateType(this.mTemplate);
        builder.setUseRepeatingSurface(true);
        final Camera2ImplConfig.Builder builder2 = new Camera2ImplConfig.Builder();
        builder2.setCaptureRequestOption((android.hardware.camera2.CaptureRequest$Key<Integer>)CaptureRequest.CONTROL_AF_TRIGGER, 1);
        if (b) {
            builder2.setCaptureRequestOption((android.hardware.camera2.CaptureRequest$Key<Integer>)CaptureRequest.CONTROL_AE_MODE, this.mCameraControl.getSupportedAeMode(1));
        }
        builder.addImplementationOptions(builder2.build());
        builder.addCameraCaptureCallback(new CameraCaptureCallback(this, completer) {
            final FocusMeteringControl this$0;
            final CallbackToFutureAdapter.Completer val$completer;
            
            @Override
            public void onCaptureCancelled() {
                final CallbackToFutureAdapter.Completer val$completer = this.val$completer;
                if (val$completer != null) {
                    val$completer.setException(new CameraControl.OperationCanceledException("Camera is closed"));
                }
            }
            
            @Override
            public void onCaptureCompleted(final CameraCaptureResult cameraCaptureResult) {
                final CallbackToFutureAdapter.Completer val$completer = this.val$completer;
                if (val$completer != null) {
                    val$completer.set(cameraCaptureResult);
                }
            }
            
            @Override
            public void onCaptureFailed(final CameraCaptureFailure cameraCaptureFailure) {
                final CallbackToFutureAdapter.Completer val$completer = this.val$completer;
                if (val$completer != null) {
                    val$completer.setException(new CameraControlInternal.CameraControlException(cameraCaptureFailure));
                }
            }
        });
        this.mCameraControl.submitCaptureRequestsInternal(Collections.singletonList(builder.build()));
    }
}

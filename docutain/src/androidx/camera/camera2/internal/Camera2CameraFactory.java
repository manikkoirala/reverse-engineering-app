// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.internal;

import androidx.camera.core.CameraUnavailableException;
import androidx.camera.core.impl.CameraInternal;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.Set;
import androidx.camera.camera2.internal.compat.CameraAccessExceptionCompat;
import android.hardware.camera2.CameraCharacteristics$Key;
import android.hardware.camera2.CameraCharacteristics;
import android.os.Build;
import java.util.Iterator;
import androidx.camera.core.Logger;
import java.util.ArrayList;
import androidx.camera.core.InitializationException;
import java.util.HashMap;
import androidx.camera.core.CameraSelector;
import android.content.Context;
import androidx.camera.core.impl.CameraThreadConfig;
import androidx.camera.core.impl.CameraStateRegistry;
import androidx.camera.camera2.internal.compat.CameraManagerCompat;
import java.util.Map;
import java.util.List;
import androidx.camera.core.impl.CameraFactory;

public final class Camera2CameraFactory implements CameraFactory
{
    private static final int DEFAULT_ALLOWED_CONCURRENT_OPEN_CAMERAS = 1;
    private static final String TAG = "Camera2CameraFactory";
    private final List<String> mAvailableCameraIds;
    private final Map<String, Camera2CameraInfoImpl> mCameraInfos;
    private final CameraManagerCompat mCameraManager;
    private final CameraStateRegistry mCameraStateRegistry;
    private final DisplayInfoManager mDisplayInfoManager;
    private final CameraThreadConfig mThreadConfig;
    
    public Camera2CameraFactory(final Context context, final CameraThreadConfig mThreadConfig, final CameraSelector cameraSelector) throws InitializationException {
        this.mCameraInfos = new HashMap<String, Camera2CameraInfoImpl>();
        this.mThreadConfig = mThreadConfig;
        this.mCameraStateRegistry = new CameraStateRegistry(1);
        this.mCameraManager = CameraManagerCompat.from(context, mThreadConfig.getSchedulerHandler());
        this.mDisplayInfoManager = DisplayInfoManager.getInstance(context);
        this.mAvailableCameraIds = this.getBackwardCompatibleCameraIds(CameraSelectionOptimizer.getSelectedAvailableCameraIds(this, cameraSelector));
    }
    
    private List<String> getBackwardCompatibleCameraIds(final List<String> list) throws InitializationException {
        final ArrayList list2 = new ArrayList();
        for (final String str : list) {
            if (!str.equals("0") && !str.equals("1")) {
                if (this.isBackwardCompatible(str)) {
                    list2.add(str);
                }
                else {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Camera ");
                    sb.append(str);
                    sb.append(" is filtered out because its capabilities do not contain REQUEST_AVAILABLE_CAPABILITIES_BACKWARD_COMPATIBLE.");
                    Logger.d("Camera2CameraFactory", sb.toString());
                }
            }
            else {
                list2.add(str);
            }
        }
        return list2;
    }
    
    private boolean isBackwardCompatible(final String s) throws InitializationException {
        if ("robolectric".equals(Build.FINGERPRINT)) {
            return true;
        }
        try {
            final int[] array = this.mCameraManager.getCameraCharacteristicsCompat(s).get((android.hardware.camera2.CameraCharacteristics$Key<int[]>)CameraCharacteristics.REQUEST_AVAILABLE_CAPABILITIES);
            if (array != null) {
                for (int length = array.length, i = 0; i < length; ++i) {
                    if (array[i] == 0) {
                        return true;
                    }
                }
            }
            return false;
        }
        catch (final CameraAccessExceptionCompat cameraAccessExceptionCompat) {
            throw new InitializationException(CameraUnavailableExceptionHelper.createFrom(cameraAccessExceptionCompat));
        }
    }
    
    @Override
    public Set<String> getAvailableCameraIds() {
        return new LinkedHashSet<String>(this.mAvailableCameraIds);
    }
    
    @Override
    public CameraInternal getCamera(final String s) throws CameraUnavailableException {
        if (this.mAvailableCameraIds.contains(s)) {
            return new Camera2CameraImpl(this.mCameraManager, s, this.getCameraInfo(s), this.mCameraStateRegistry, this.mThreadConfig.getCameraExecutor(), this.mThreadConfig.getSchedulerHandler(), this.mDisplayInfoManager);
        }
        throw new IllegalArgumentException("The given camera id is not on the available camera id list.");
    }
    
    Camera2CameraInfoImpl getCameraInfo(final String s) throws CameraUnavailableException {
        try {
            Camera2CameraInfoImpl camera2CameraInfoImpl;
            if ((camera2CameraInfoImpl = this.mCameraInfos.get(s)) == null) {
                camera2CameraInfoImpl = new Camera2CameraInfoImpl(s, this.mCameraManager);
                this.mCameraInfos.put(s, camera2CameraInfoImpl);
            }
            return camera2CameraInfoImpl;
        }
        catch (final CameraAccessExceptionCompat cameraAccessExceptionCompat) {
            throw CameraUnavailableExceptionHelper.createFrom(cameraAccessExceptionCompat);
        }
    }
    
    @Override
    public CameraManagerCompat getCameraManager() {
        return this.mCameraManager;
    }
}

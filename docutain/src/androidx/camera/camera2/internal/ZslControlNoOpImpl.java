// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.internal;

import androidx.camera.core.ImageProxy;
import androidx.camera.core.impl.SessionConfig;

public class ZslControlNoOpImpl implements ZslControl
{
    @Override
    public void addZslConfig(final SessionConfig.Builder builder) {
    }
    
    @Override
    public ImageProxy dequeueImageFromBuffer() {
        return null;
    }
    
    @Override
    public boolean enqueueImageToImageWriter(final ImageProxy imageProxy) {
        return false;
    }
    
    @Override
    public boolean isZslDisabledByFlashMode() {
        return false;
    }
    
    @Override
    public boolean isZslDisabledByUserCaseConfig() {
        return false;
    }
    
    @Override
    public void setZslDisabledByFlashMode(final boolean b) {
    }
    
    @Override
    public void setZslDisabledByUserCaseConfig(final boolean b) {
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.internal;

import androidx.camera.core.CameraUnavailableException;
import androidx.camera.camera2.internal.compat.CameraAccessExceptionCompat;

public final class CameraUnavailableExceptionHelper
{
    private CameraUnavailableExceptionHelper() {
    }
    
    public static CameraUnavailableException createFrom(final CameraAccessExceptionCompat cameraAccessExceptionCompat) {
        final int reason = cameraAccessExceptionCompat.getReason();
        int n = 5;
        if (reason != 1) {
            if (reason != 2) {
                if (reason != 3) {
                    if (reason != 4) {
                        if (reason != 5) {
                            if (reason != 10001) {
                                n = 0;
                            }
                            else {
                                n = 6;
                            }
                        }
                    }
                    else {
                        n = 4;
                    }
                }
                else {
                    n = 3;
                }
            }
            else {
                n = 2;
            }
        }
        else {
            n = 1;
        }
        return new CameraUnavailableException(n, cameraAccessExceptionCompat);
    }
}

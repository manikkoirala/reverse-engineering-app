// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.internal;

import androidx.lifecycle.Observer;
import androidx.lifecycle.MediatorLiveData;
import android.os.Build$VERSION;
import androidx.camera.core.FocusMeteringAction;
import androidx.camera.camera2.internal.compat.workaround.FlashAvailabilityChecker;
import androidx.camera.core.impl.Timebase;
import androidx.camera.core.impl.utils.CameraOrientationUtil;
import android.hardware.camera2.CameraCharacteristics$Key;
import androidx.camera.core.ExposureState;
import androidx.lifecycle.LiveData;
import androidx.camera.core.impl.CameraInfoInternal$_CC;
import androidx.camera.core.CameraSelector;
import java.util.Iterator;
import java.util.Objects;
import java.util.LinkedHashMap;
import android.hardware.camera2.CameraCharacteristics;
import java.util.Map;
import java.util.ArrayList;
import androidx.camera.core.Logger;
import androidx.camera.camera2.internal.compat.CameraAccessExceptionCompat;
import androidx.camera.camera2.internal.compat.quirk.CameraQuirks;
import androidx.core.util.Preconditions;
import androidx.camera.core.ZoomState;
import androidx.camera.core.CameraState;
import androidx.camera.core.impl.Quirks;
import androidx.camera.camera2.internal.compat.CameraManagerCompat;
import androidx.camera.camera2.internal.compat.CameraCharacteristicsCompat;
import java.util.concurrent.Executor;
import androidx.camera.core.impl.CameraCaptureCallback;
import android.util.Pair;
import java.util.List;
import androidx.camera.camera2.interop.Camera2CameraInfo;
import androidx.camera.core.impl.CamcorderProfileProvider;
import androidx.camera.core.impl.CameraInfoInternal;

public final class Camera2CameraInfoImpl implements CameraInfoInternal
{
    private static final String TAG = "Camera2CameraInfo";
    private final CamcorderProfileProvider mCamera2CamcorderProfileProvider;
    private Camera2CameraControlImpl mCamera2CameraControlImpl;
    private final Camera2CameraInfo mCamera2CameraInfo;
    private List<Pair<CameraCaptureCallback, Executor>> mCameraCaptureCallbacks;
    private final CameraCharacteristicsCompat mCameraCharacteristicsCompat;
    private final String mCameraId;
    private final CameraManagerCompat mCameraManager;
    private final Quirks mCameraQuirks;
    private final RedirectableLiveData<CameraState> mCameraStateLiveData;
    private final Object mLock;
    private RedirectableLiveData<Integer> mRedirectTorchStateLiveData;
    private RedirectableLiveData<ZoomState> mRedirectZoomStateLiveData;
    
    Camera2CameraInfoImpl(final String s, final CameraManagerCompat mCameraManager) throws CameraAccessExceptionCompat {
        this.mLock = new Object();
        this.mRedirectTorchStateLiveData = null;
        this.mRedirectZoomStateLiveData = null;
        this.mCameraCaptureCallbacks = null;
        final String mCameraId = Preconditions.checkNotNull(s);
        this.mCameraId = mCameraId;
        this.mCameraManager = mCameraManager;
        final CameraCharacteristicsCompat cameraCharacteristicsCompat = mCameraManager.getCameraCharacteristicsCompat(mCameraId);
        this.mCameraCharacteristicsCompat = cameraCharacteristicsCompat;
        this.mCamera2CameraInfo = new Camera2CameraInfo(this);
        this.mCameraQuirks = CameraQuirks.get(s, cameraCharacteristicsCompat);
        this.mCamera2CamcorderProfileProvider = new Camera2CamcorderProfileProvider(s, cameraCharacteristicsCompat);
        this.mCameraStateLiveData = new RedirectableLiveData<CameraState>(CameraState.create(CameraState.Type.CLOSED));
    }
    
    private void logDeviceInfo() {
        this.logDeviceLevel();
    }
    
    private void logDeviceLevel() {
        final int supportedHardwareLevel = this.getSupportedHardwareLevel();
        String string;
        if (supportedHardwareLevel != 0) {
            if (supportedHardwareLevel != 1) {
                if (supportedHardwareLevel != 2) {
                    if (supportedHardwareLevel != 3) {
                        if (supportedHardwareLevel != 4) {
                            final StringBuilder sb = new StringBuilder();
                            sb.append("Unknown value: ");
                            sb.append(supportedHardwareLevel);
                            string = sb.toString();
                        }
                        else {
                            string = "INFO_SUPPORTED_HARDWARE_LEVEL_EXTERNAL";
                        }
                    }
                    else {
                        string = "INFO_SUPPORTED_HARDWARE_LEVEL_3";
                    }
                }
                else {
                    string = "INFO_SUPPORTED_HARDWARE_LEVEL_LEGACY";
                }
            }
            else {
                string = "INFO_SUPPORTED_HARDWARE_LEVEL_FULL";
            }
        }
        else {
            string = "INFO_SUPPORTED_HARDWARE_LEVEL_LIMITED";
        }
        final StringBuilder sb2 = new StringBuilder();
        sb2.append("Device Level: ");
        sb2.append(string);
        Logger.i("Camera2CameraInfo", sb2.toString());
    }
    
    @Override
    public void addSessionCaptureCallback(final Executor executor, final CameraCaptureCallback cameraCaptureCallback) {
        synchronized (this.mLock) {
            final Camera2CameraControlImpl mCamera2CameraControlImpl = this.mCamera2CameraControlImpl;
            if (mCamera2CameraControlImpl == null) {
                if (this.mCameraCaptureCallbacks == null) {
                    this.mCameraCaptureCallbacks = new ArrayList<Pair<CameraCaptureCallback, Executor>>();
                }
                this.mCameraCaptureCallbacks.add((Pair<CameraCaptureCallback, Executor>)new Pair((Object)cameraCaptureCallback, (Object)executor));
                return;
            }
            mCamera2CameraControlImpl.addSessionCameraCaptureCallback(executor, cameraCaptureCallback);
        }
    }
    
    @Override
    public CamcorderProfileProvider getCamcorderProfileProvider() {
        return this.mCamera2CamcorderProfileProvider;
    }
    
    public Camera2CameraInfo getCamera2CameraInfo() {
        return this.mCamera2CameraInfo;
    }
    
    public CameraCharacteristicsCompat getCameraCharacteristicsCompat() {
        return this.mCameraCharacteristicsCompat;
    }
    
    public Map<String, CameraCharacteristics> getCameraCharacteristicsMap() {
        final LinkedHashMap linkedHashMap = new LinkedHashMap();
        linkedHashMap.put(this.mCameraId, this.mCameraCharacteristicsCompat.toCameraCharacteristics());
        for (final String str : this.mCameraCharacteristicsCompat.getPhysicalCameraIds()) {
            if (Objects.equals(str, this.mCameraId)) {
                continue;
            }
            try {
                linkedHashMap.put(str, this.mCameraManager.getCameraCharacteristicsCompat(str).toCameraCharacteristics());
            }
            catch (final CameraAccessExceptionCompat cameraAccessExceptionCompat) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Failed to get CameraCharacteristics for cameraId ");
                sb.append(str);
                Logger.e("Camera2CameraInfo", sb.toString(), cameraAccessExceptionCompat);
            }
        }
        return linkedHashMap;
    }
    
    @Override
    public String getCameraId() {
        return this.mCameraId;
    }
    
    @Override
    public Quirks getCameraQuirks() {
        return this.mCameraQuirks;
    }
    
    @Override
    public LiveData<CameraState> getCameraState() {
        return this.mCameraStateLiveData;
    }
    
    @Override
    public ExposureState getExposureState() {
        synchronized (this.mLock) {
            final Camera2CameraControlImpl mCamera2CameraControlImpl = this.mCamera2CameraControlImpl;
            if (mCamera2CameraControlImpl == null) {
                return ExposureControl.getDefaultExposureState(this.mCameraCharacteristicsCompat);
            }
            return mCamera2CameraControlImpl.getExposureControl().getExposureState();
        }
    }
    
    @Override
    public String getImplementationType() {
        String s;
        if (this.getSupportedHardwareLevel() == 2) {
            s = "androidx.camera.camera2.legacy";
        }
        else {
            s = "androidx.camera.camera2";
        }
        return s;
    }
    
    @Override
    public Integer getLensFacing() {
        final Integer n = this.mCameraCharacteristicsCompat.get((android.hardware.camera2.CameraCharacteristics$Key<Integer>)CameraCharacteristics.LENS_FACING);
        Preconditions.checkNotNull(n);
        final int intValue = n;
        if (intValue == 0) {
            return 0;
        }
        if (intValue != 1) {
            return null;
        }
        return 1;
    }
    
    int getSensorOrientation() {
        final Integer n = this.mCameraCharacteristicsCompat.get((android.hardware.camera2.CameraCharacteristics$Key<Integer>)CameraCharacteristics.SENSOR_ORIENTATION);
        Preconditions.checkNotNull(n);
        return n;
    }
    
    @Override
    public int getSensorRotationDegrees() {
        return this.getSensorRotationDegrees(0);
    }
    
    @Override
    public int getSensorRotationDegrees(int surfaceRotationToDegrees) {
        final int sensorOrientation = this.getSensorOrientation();
        surfaceRotationToDegrees = CameraOrientationUtil.surfaceRotationToDegrees(surfaceRotationToDegrees);
        final Integer lensFacing = this.getLensFacing();
        boolean b = true;
        if (lensFacing == null || 1 != lensFacing) {
            b = false;
        }
        return CameraOrientationUtil.getRelativeImageRotation(surfaceRotationToDegrees, sensorOrientation, b);
    }
    
    int getSupportedHardwareLevel() {
        final Integer n = this.mCameraCharacteristicsCompat.get((android.hardware.camera2.CameraCharacteristics$Key<Integer>)CameraCharacteristics.INFO_SUPPORTED_HARDWARE_LEVEL);
        Preconditions.checkNotNull(n);
        return n;
    }
    
    @Override
    public Timebase getTimebase() {
        final Integer n = this.mCameraCharacteristicsCompat.get((android.hardware.camera2.CameraCharacteristics$Key<Integer>)CameraCharacteristics.SENSOR_INFO_TIMESTAMP_SOURCE);
        Preconditions.checkNotNull(n);
        if (n != 1) {
            return Timebase.UPTIME;
        }
        return Timebase.REALTIME;
    }
    
    @Override
    public LiveData<Integer> getTorchState() {
        synchronized (this.mLock) {
            final Camera2CameraControlImpl mCamera2CameraControlImpl = this.mCamera2CameraControlImpl;
            if (mCamera2CameraControlImpl == null) {
                if (this.mRedirectTorchStateLiveData == null) {
                    this.mRedirectTorchStateLiveData = new RedirectableLiveData<Integer>(0);
                }
                return this.mRedirectTorchStateLiveData;
            }
            final RedirectableLiveData<Integer> mRedirectTorchStateLiveData = this.mRedirectTorchStateLiveData;
            if (mRedirectTorchStateLiveData != null) {
                return mRedirectTorchStateLiveData;
            }
            return mCamera2CameraControlImpl.getTorchControl().getTorchState();
        }
    }
    
    @Override
    public LiveData<ZoomState> getZoomState() {
        synchronized (this.mLock) {
            final Camera2CameraControlImpl mCamera2CameraControlImpl = this.mCamera2CameraControlImpl;
            if (mCamera2CameraControlImpl == null) {
                if (this.mRedirectZoomStateLiveData == null) {
                    this.mRedirectZoomStateLiveData = new RedirectableLiveData<ZoomState>(ZoomControl.getDefaultZoomState(this.mCameraCharacteristicsCompat));
                }
                return this.mRedirectZoomStateLiveData;
            }
            final RedirectableLiveData<ZoomState> mRedirectZoomStateLiveData = this.mRedirectZoomStateLiveData;
            if (mRedirectZoomStateLiveData != null) {
                return mRedirectZoomStateLiveData;
            }
            return mCamera2CameraControlImpl.getZoomControl().getZoomState();
        }
    }
    
    @Override
    public boolean hasFlashUnit() {
        return FlashAvailabilityChecker.isFlashAvailable(this.mCameraCharacteristicsCompat);
    }
    
    @Override
    public boolean isFocusMeteringSupported(final FocusMeteringAction focusMeteringAction) {
        synchronized (this.mLock) {
            final Camera2CameraControlImpl mCamera2CameraControlImpl = this.mCamera2CameraControlImpl;
            return mCamera2CameraControlImpl != null && mCamera2CameraControlImpl.getFocusMeteringControl().isFocusMeteringSupported(focusMeteringAction);
        }
    }
    
    @Override
    public boolean isPrivateReprocessingSupported() {
        return ZslUtil.isCapabilitySupported(this.mCameraCharacteristicsCompat, 4);
    }
    
    @Override
    public boolean isZslSupported() {
        return Build$VERSION.SDK_INT >= 23 && this.isPrivateReprocessingSupported();
    }
    
    void linkWithCameraControl(final Camera2CameraControlImpl mCamera2CameraControlImpl) {
        synchronized (this.mLock) {
            this.mCamera2CameraControlImpl = mCamera2CameraControlImpl;
            final RedirectableLiveData<ZoomState> mRedirectZoomStateLiveData = this.mRedirectZoomStateLiveData;
            if (mRedirectZoomStateLiveData != null) {
                mRedirectZoomStateLiveData.redirectTo(mCamera2CameraControlImpl.getZoomControl().getZoomState());
            }
            final RedirectableLiveData<Integer> mRedirectTorchStateLiveData = this.mRedirectTorchStateLiveData;
            if (mRedirectTorchStateLiveData != null) {
                mRedirectTorchStateLiveData.redirectTo(this.mCamera2CameraControlImpl.getTorchControl().getTorchState());
            }
            final List<Pair<CameraCaptureCallback, Executor>> mCameraCaptureCallbacks = this.mCameraCaptureCallbacks;
            if (mCameraCaptureCallbacks != null) {
                for (final Pair pair : mCameraCaptureCallbacks) {
                    this.mCamera2CameraControlImpl.addSessionCameraCaptureCallback((Executor)pair.second, (CameraCaptureCallback)pair.first);
                }
                this.mCameraCaptureCallbacks = null;
            }
            monitorexit(this.mLock);
            this.logDeviceInfo();
        }
    }
    
    @Override
    public void removeSessionCaptureCallback(final CameraCaptureCallback cameraCaptureCallback) {
        synchronized (this.mLock) {
            final Camera2CameraControlImpl mCamera2CameraControlImpl = this.mCamera2CameraControlImpl;
            if (mCamera2CameraControlImpl != null) {
                mCamera2CameraControlImpl.removeSessionCameraCaptureCallback(cameraCaptureCallback);
                return;
            }
            final List<Pair<CameraCaptureCallback, Executor>> mCameraCaptureCallbacks = this.mCameraCaptureCallbacks;
            if (mCameraCaptureCallbacks == null) {
                return;
            }
            final Iterator<Pair<CameraCaptureCallback, Executor>> iterator = mCameraCaptureCallbacks.iterator();
            while (iterator.hasNext()) {
                if (iterator.next().first == cameraCaptureCallback) {
                    iterator.remove();
                }
            }
        }
    }
    
    void setCameraStateSource(final LiveData<CameraState> liveData) {
        this.mCameraStateLiveData.redirectTo(liveData);
    }
    
    static class RedirectableLiveData<T> extends MediatorLiveData<T>
    {
        private final T mInitialValue;
        private LiveData<T> mLiveDataSource;
        
        RedirectableLiveData(final T mInitialValue) {
            this.mInitialValue = mInitialValue;
        }
        
        @Override
        public <S> void addSource(final LiveData<S> liveData, final Observer<? super S> observer) {
            throw new UnsupportedOperationException();
        }
        
        @Override
        public T getValue() {
            final LiveData<T> mLiveDataSource = this.mLiveDataSource;
            T t;
            if (mLiveDataSource == null) {
                t = this.mInitialValue;
            }
            else {
                t = mLiveDataSource.getValue();
            }
            return t;
        }
        
        void redirectTo(final LiveData<T> mLiveDataSource) {
            final LiveData<T> mLiveDataSource2 = this.mLiveDataSource;
            if (mLiveDataSource2 != null) {
                super.removeSource(mLiveDataSource2);
            }
            super.addSource(this.mLiveDataSource = mLiveDataSource, new Camera2CameraInfoImpl$RedirectableLiveData$$ExternalSyntheticLambda0(this));
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.internal;

import androidx.camera.core.impl.Config;
import androidx.camera.camera2.impl.Camera2ImplConfig;
import androidx.camera.core.impl.CameraCaptureCallback;
import java.util.Collection;
import androidx.camera.core.impl.OptionsBundle;
import androidx.camera.core.impl.UseCaseConfig;
import androidx.camera.core.impl.CaptureConfig;

class Camera2CaptureOptionUnpacker implements OptionUnpacker
{
    static final Camera2CaptureOptionUnpacker INSTANCE;
    
    static {
        INSTANCE = new Camera2CaptureOptionUnpacker();
    }
    
    @Override
    public void unpack(final UseCaseConfig<?> useCaseConfig, final Builder builder) {
        final CaptureConfig defaultCaptureConfig = useCaseConfig.getDefaultCaptureConfig(null);
        Config implementationOptions = OptionsBundle.emptyBundle();
        int n = CaptureConfig.defaultEmptyCaptureConfig().getTemplateType();
        if (defaultCaptureConfig != null) {
            n = defaultCaptureConfig.getTemplateType();
            builder.addAllCameraCaptureCallbacks(defaultCaptureConfig.getCameraCaptureCallbacks());
            implementationOptions = defaultCaptureConfig.getImplementationOptions();
        }
        builder.setImplementationOptions(implementationOptions);
        final Camera2ImplConfig camera2ImplConfig = new Camera2ImplConfig(useCaseConfig);
        builder.setTemplateType(camera2ImplConfig.getCaptureRequestTemplate(n));
        builder.addCameraCaptureCallback(CaptureCallbackContainer.create(camera2ImplConfig.getSessionCaptureCallback(Camera2CaptureCallbacks.createNoOpCallback())));
        builder.addImplementationOptions(camera2ImplConfig.getCaptureRequestOptions());
    }
}

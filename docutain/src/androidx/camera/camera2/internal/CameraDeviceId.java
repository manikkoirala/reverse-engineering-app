// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.internal;

abstract class CameraDeviceId
{
    public static CameraDeviceId create(final String s, final String s2, final String s3, final String s4) {
        return new AutoValue_CameraDeviceId(s.toLowerCase(), s2.toLowerCase(), s3.toLowerCase(), s4.toLowerCase());
    }
    
    public abstract String getBrand();
    
    public abstract String getCameraId();
    
    public abstract String getDevice();
    
    public abstract String getModel();
}

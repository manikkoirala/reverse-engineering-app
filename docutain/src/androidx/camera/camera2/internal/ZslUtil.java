// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.internal;

import android.hardware.camera2.CameraCharacteristics$Key;
import android.hardware.camera2.CameraCharacteristics;
import androidx.camera.camera2.internal.compat.CameraCharacteristicsCompat;

final class ZslUtil
{
    private ZslUtil() {
    }
    
    public static boolean isCapabilitySupported(final CameraCharacteristicsCompat cameraCharacteristicsCompat, final int n) {
        final int[] array = cameraCharacteristicsCompat.get((android.hardware.camera2.CameraCharacteristics$Key<int[]>)CameraCharacteristics.REQUEST_AVAILABLE_CAPABILITIES);
        if (array != null) {
            for (int length = array.length, i = 0; i < length; ++i) {
                if (array[i] == n) {
                    return true;
                }
            }
        }
        return false;
    }
}

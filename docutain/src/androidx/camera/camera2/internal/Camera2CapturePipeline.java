// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.internal;

import androidx.camera.core.ImageProxy;
import androidx.camera.core.impl.CameraCaptureResults;
import androidx.camera.core.impl.CameraCaptureFailure;
import androidx.camera.core.ImageCaptureException;
import androidx.camera.core.impl.CameraCaptureCallback;
import java.util.Objects;
import androidx.camera.core.impl.utils.futures.AsyncFunction;
import androidx.camera.core.impl.Config;
import android.hardware.camera2.CaptureRequest$Key;
import android.hardware.camera2.CaptureRequest;
import androidx.camera.camera2.impl.Camera2ImplConfig;
import java.util.Iterator;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;
import androidx.camera.core.impl.CameraCaptureResult;
import androidx.arch.core.util.Function;
import androidx.camera.core.impl.utils.executor.CameraXExecutors;
import androidx.camera.core.impl.utils.futures.FutureChain;
import androidx.concurrent.futures.CallbackToFutureAdapter;
import androidx.camera.core.impl.utils.futures.Futures;
import androidx.camera.camera2.internal.compat.workaround.OverrideAeModeForStillCapture;
import androidx.camera.core.impl.CaptureConfig;
import java.util.List;
import com.google.common.util.concurrent.ListenableFuture;
import androidx.camera.core.Logger;
import android.hardware.camera2.CaptureResult;
import android.hardware.camera2.TotalCaptureResult;
import android.hardware.camera2.CameraCharacteristics$Key;
import android.hardware.camera2.CameraCharacteristics;
import androidx.camera.camera2.internal.compat.CameraCharacteristicsCompat;
import java.util.Collection;
import java.util.Collections;
import java.util.EnumSet;
import androidx.camera.camera2.internal.compat.workaround.UseTorchAsFlash;
import java.util.concurrent.Executor;
import androidx.camera.core.impl.Quirks;
import androidx.camera.core.impl.CameraCaptureMetaData;
import java.util.Set;

class Camera2CapturePipeline
{
    private static final Set<CameraCaptureMetaData.AeState> AE_CONVERGED_STATE_SET;
    private static final Set<CameraCaptureMetaData.AeState> AE_TORCH_AS_FLASH_CONVERGED_STATE_SET;
    private static final Set<CameraCaptureMetaData.AfState> AF_CONVERGED_STATE_SET;
    private static final Set<CameraCaptureMetaData.AwbState> AWB_CONVERGED_STATE_SET;
    private static final String TAG = "Camera2CapturePipeline";
    private final Camera2CameraControlImpl mCameraControl;
    private final Quirks mCameraQuirk;
    private final Executor mExecutor;
    private final boolean mIsLegacyDevice;
    private int mTemplate;
    private final UseTorchAsFlash mUseTorchAsFlash;
    
    static {
        AF_CONVERGED_STATE_SET = Collections.unmodifiableSet((Set<? extends CameraCaptureMetaData.AfState>)EnumSet.of(CameraCaptureMetaData.AfState.PASSIVE_FOCUSED, CameraCaptureMetaData.AfState.PASSIVE_NOT_FOCUSED, CameraCaptureMetaData.AfState.LOCKED_FOCUSED, CameraCaptureMetaData.AfState.LOCKED_NOT_FOCUSED));
        AWB_CONVERGED_STATE_SET = Collections.unmodifiableSet((Set<? extends CameraCaptureMetaData.AwbState>)EnumSet.of(CameraCaptureMetaData.AwbState.CONVERGED, CameraCaptureMetaData.AwbState.UNKNOWN));
        final EnumSet<CameraCaptureMetaData.AeState> copy = EnumSet.copyOf((Collection<CameraCaptureMetaData.AeState>)(AE_CONVERGED_STATE_SET = Collections.unmodifiableSet((Set<? extends CameraCaptureMetaData.AeState>)EnumSet.of(CameraCaptureMetaData.AeState.CONVERGED, CameraCaptureMetaData.AeState.FLASH_REQUIRED, CameraCaptureMetaData.AeState.UNKNOWN))));
        copy.remove(CameraCaptureMetaData.AeState.FLASH_REQUIRED);
        copy.remove(CameraCaptureMetaData.AeState.UNKNOWN);
        AE_TORCH_AS_FLASH_CONVERGED_STATE_SET = Collections.unmodifiableSet((Set<?>)copy);
    }
    
    Camera2CapturePipeline(final Camera2CameraControlImpl mCameraControl, final CameraCharacteristicsCompat cameraCharacteristicsCompat, final Quirks mCameraQuirk, final Executor mExecutor) {
        boolean mIsLegacyDevice = true;
        this.mTemplate = 1;
        this.mCameraControl = mCameraControl;
        final Integer n = cameraCharacteristicsCompat.get((android.hardware.camera2.CameraCharacteristics$Key<Integer>)CameraCharacteristics.INFO_SUPPORTED_HARDWARE_LEVEL);
        if (n == null || n != 2) {
            mIsLegacyDevice = false;
        }
        this.mIsLegacyDevice = mIsLegacyDevice;
        this.mExecutor = mExecutor;
        this.mCameraQuirk = mCameraQuirk;
        this.mUseTorchAsFlash = new UseTorchAsFlash(mCameraQuirk);
    }
    
    static boolean is3AConverged(final TotalCaptureResult totalCaptureResult, final boolean b) {
        final boolean b2 = false;
        if (totalCaptureResult == null) {
            return false;
        }
        final Camera2CameraCaptureResult camera2CameraCaptureResult = new Camera2CameraCaptureResult((CaptureResult)totalCaptureResult);
        final boolean b3 = camera2CameraCaptureResult.getAfMode() == CameraCaptureMetaData.AfMode.OFF || camera2CameraCaptureResult.getAfMode() == CameraCaptureMetaData.AfMode.UNKNOWN || Camera2CapturePipeline.AF_CONVERGED_STATE_SET.contains(camera2CameraCaptureResult.getAfState());
        final boolean b4 = (int)totalCaptureResult.get(CaptureResult.CONTROL_AE_MODE) == 0;
        boolean b5 = false;
        Label_0150: {
            Label_0122: {
                if (b) {
                    if (b4) {
                        break Label_0122;
                    }
                    if (Camera2CapturePipeline.AE_TORCH_AS_FLASH_CONVERGED_STATE_SET.contains(camera2CameraCaptureResult.getAeState())) {
                        break Label_0122;
                    }
                }
                else {
                    if (b4) {
                        break Label_0122;
                    }
                    if (Camera2CapturePipeline.AE_CONVERGED_STATE_SET.contains(camera2CameraCaptureResult.getAeState())) {
                        break Label_0122;
                    }
                }
                b5 = false;
                break Label_0150;
            }
            b5 = true;
        }
        final boolean b6 = (int)totalCaptureResult.get(CaptureResult.CONTROL_AWB_MODE) == 0 || Camera2CapturePipeline.AWB_CONVERGED_STATE_SET.contains(camera2CameraCaptureResult.getAwbState());
        final StringBuilder sb = new StringBuilder();
        sb.append("checkCaptureResult, AE=");
        sb.append(camera2CameraCaptureResult.getAeState());
        sb.append(" AF =");
        sb.append(camera2CameraCaptureResult.getAfState());
        sb.append(" AWB=");
        sb.append(camera2CameraCaptureResult.getAwbState());
        Logger.d("Camera2CapturePipeline", sb.toString());
        boolean b7 = b2;
        if (b3) {
            b7 = b2;
            if (b5) {
                b7 = b2;
                if (b6) {
                    b7 = true;
                }
            }
        }
        return b7;
    }
    
    static boolean isFlashRequired(final int detailMessage, final TotalCaptureResult totalCaptureResult) {
        final boolean b = false;
        if (detailMessage == 0) {
            Integer n;
            if (totalCaptureResult != null) {
                n = (Integer)totalCaptureResult.get(CaptureResult.CONTROL_AE_STATE);
            }
            else {
                n = null;
            }
            boolean b2 = b;
            if (n != null) {
                b2 = b;
                if (n == 4) {
                    b2 = true;
                }
            }
            return b2;
        }
        if (detailMessage == 1) {
            return true;
        }
        if (detailMessage == 2) {
            return false;
        }
        throw new AssertionError(detailMessage);
    }
    
    private boolean isTorchAsFlash(final int n) {
        final boolean shouldUseTorchAsFlash = this.mUseTorchAsFlash.shouldUseTorchAsFlash();
        boolean b2;
        final boolean b = b2 = true;
        if (!shouldUseTorchAsFlash) {
            b2 = b;
            if (this.mTemplate != 3) {
                b2 = (n == 1 && b);
            }
        }
        return b2;
    }
    
    static ListenableFuture<TotalCaptureResult> waitForResult(final long n, final Camera2CameraControlImpl camera2CameraControlImpl, final Checker checker) {
        final ResultListener resultListener = new ResultListener(n, checker);
        camera2CameraControlImpl.addCaptureResultListener((Camera2CameraControlImpl.CaptureResultListener)resultListener);
        return resultListener.getFuture();
    }
    
    public void setTemplate(final int mTemplate) {
        this.mTemplate = mTemplate;
    }
    
    public ListenableFuture<List<Void>> submitStillCaptures(final List<CaptureConfig> list, final int n, final int n2, final int n3) {
        final OverrideAeModeForStillCapture overrideAeModeForStillCapture = new OverrideAeModeForStillCapture(this.mCameraQuirk);
        final Pipeline pipeline = new Pipeline(this.mTemplate, this.mExecutor, this.mCameraControl, this.mIsLegacyDevice, overrideAeModeForStillCapture);
        if (n == 0) {
            pipeline.addTask(new AfTask(this.mCameraControl));
        }
        if (this.isTorchAsFlash(n3)) {
            pipeline.addTask(new TorchTask(this.mCameraControl, n2, this.mExecutor));
        }
        else {
            pipeline.addTask(new AePreCaptureTask(this.mCameraControl, n2, overrideAeModeForStillCapture));
        }
        return Futures.nonCancellationPropagating(pipeline.executeCapture(list, n2));
    }
    
    static class AePreCaptureTask implements PipelineTask
    {
        private final Camera2CameraControlImpl mCameraControl;
        private final int mFlashMode;
        private boolean mIsExecuted;
        private final OverrideAeModeForStillCapture mOverrideAeModeForStillCapture;
        
        AePreCaptureTask(final Camera2CameraControlImpl mCameraControl, final int mFlashMode, final OverrideAeModeForStillCapture mOverrideAeModeForStillCapture) {
            this.mIsExecuted = false;
            this.mCameraControl = mCameraControl;
            this.mFlashMode = mFlashMode;
            this.mOverrideAeModeForStillCapture = mOverrideAeModeForStillCapture;
        }
        
        @Override
        public boolean isCaptureResultNeeded() {
            return this.mFlashMode == 0;
        }
        
        @Override
        public void postCapture() {
            if (this.mIsExecuted) {
                Logger.d("Camera2CapturePipeline", "cancel TriggerAePreCapture");
                this.mCameraControl.getFocusMeteringControl().cancelAfAeTrigger(false, true);
                this.mOverrideAeModeForStillCapture.onAePrecaptureFinished();
            }
        }
        
        @Override
        public ListenableFuture<Boolean> preCapture(final TotalCaptureResult totalCaptureResult) {
            if (Camera2CapturePipeline.isFlashRequired(this.mFlashMode, totalCaptureResult)) {
                Logger.d("Camera2CapturePipeline", "Trigger AE");
                this.mIsExecuted = true;
                return (ListenableFuture<Boolean>)FutureChain.from((com.google.common.util.concurrent.ListenableFuture<Object>)CallbackToFutureAdapter.getFuture((CallbackToFutureAdapter.Resolver<V>)new Camera2CapturePipeline$AePreCaptureTask$$ExternalSyntheticLambda0(this))).transform((Function<? super Object, Object>)new Camera2CapturePipeline$AePreCaptureTask$$ExternalSyntheticLambda1(), CameraXExecutors.directExecutor());
            }
            return Futures.immediateFuture(false);
        }
    }
    
    interface PipelineTask
    {
        boolean isCaptureResultNeeded();
        
        void postCapture();
        
        ListenableFuture<Boolean> preCapture(final TotalCaptureResult p0);
    }
    
    static class AfTask implements PipelineTask
    {
        private final Camera2CameraControlImpl mCameraControl;
        private boolean mIsExecuted;
        
        AfTask(final Camera2CameraControlImpl mCameraControl) {
            this.mIsExecuted = false;
            this.mCameraControl = mCameraControl;
        }
        
        @Override
        public boolean isCaptureResultNeeded() {
            return true;
        }
        
        @Override
        public void postCapture() {
            if (this.mIsExecuted) {
                Logger.d("Camera2CapturePipeline", "cancel TriggerAF");
                this.mCameraControl.getFocusMeteringControl().cancelAfAeTrigger(true, false);
            }
        }
        
        @Override
        public ListenableFuture<Boolean> preCapture(final TotalCaptureResult totalCaptureResult) {
            final com.google.common.util.concurrent.ListenableFuture<Boolean> immediateFuture = Futures.immediateFuture(true);
            if (totalCaptureResult == null) {
                return immediateFuture;
            }
            final Integer n = (Integer)totalCaptureResult.get(CaptureResult.CONTROL_AF_MODE);
            if (n == null) {
                return immediateFuture;
            }
            final int intValue = n;
            if (intValue == 1 || intValue == 2) {
                Logger.d("Camera2CapturePipeline", "TriggerAf? AF mode auto");
                final Integer n2 = (Integer)totalCaptureResult.get(CaptureResult.CONTROL_AF_STATE);
                if (n2 != null && n2 == 0) {
                    Logger.d("Camera2CapturePipeline", "Trigger AF");
                    this.mIsExecuted = true;
                    this.mCameraControl.getFocusMeteringControl().triggerAf(null, false);
                }
            }
            return immediateFuture;
        }
    }
    
    static class Pipeline
    {
        private static final long CHECK_3A_TIMEOUT_IN_NS;
        private static final long CHECK_3A_WITH_FLASH_TIMEOUT_IN_NS;
        private final Camera2CameraControlImpl mCameraControl;
        private final Executor mExecutor;
        private final boolean mIsLegacyDevice;
        private final OverrideAeModeForStillCapture mOverrideAeModeForStillCapture;
        private final PipelineTask mPipelineSubTask;
        final List<PipelineTask> mTasks;
        private final int mTemplate;
        private long mTimeout3A;
        
        static {
            CHECK_3A_TIMEOUT_IN_NS = TimeUnit.SECONDS.toNanos(1L);
            CHECK_3A_WITH_FLASH_TIMEOUT_IN_NS = TimeUnit.SECONDS.toNanos(5L);
        }
        
        Pipeline(final int mTemplate, final Executor mExecutor, final Camera2CameraControlImpl mCameraControl, final boolean mIsLegacyDevice, final OverrideAeModeForStillCapture mOverrideAeModeForStillCapture) {
            this.mTimeout3A = Pipeline.CHECK_3A_TIMEOUT_IN_NS;
            this.mTasks = new ArrayList<PipelineTask>();
            this.mPipelineSubTask = new PipelineTask() {
                final Pipeline this$0;
                
                @Override
                public boolean isCaptureResultNeeded() {
                    final Iterator<PipelineTask> iterator = this.this$0.mTasks.iterator();
                    while (iterator.hasNext()) {
                        if (iterator.next().isCaptureResultNeeded()) {
                            return true;
                        }
                    }
                    return false;
                }
                
                @Override
                public void postCapture() {
                    final Iterator<PipelineTask> iterator = this.this$0.mTasks.iterator();
                    while (iterator.hasNext()) {
                        iterator.next().postCapture();
                    }
                }
                
                @Override
                public ListenableFuture<Boolean> preCapture(final TotalCaptureResult totalCaptureResult) {
                    final ArrayList list = new ArrayList();
                    final Iterator<PipelineTask> iterator = this.this$0.mTasks.iterator();
                    while (iterator.hasNext()) {
                        list.add(iterator.next().preCapture(totalCaptureResult));
                    }
                    return (ListenableFuture<Boolean>)Futures.transform(Futures.allAsList((Collection<? extends com.google.common.util.concurrent.ListenableFuture<?>>)list), (Function<? super List<Object>, ?>)new Camera2CapturePipeline$Pipeline$1$$ExternalSyntheticLambda0(), CameraXExecutors.directExecutor());
                }
            };
            this.mTemplate = mTemplate;
            this.mExecutor = mExecutor;
            this.mCameraControl = mCameraControl;
            this.mIsLegacyDevice = mIsLegacyDevice;
            this.mOverrideAeModeForStillCapture = mOverrideAeModeForStillCapture;
        }
        
        private void applyAeModeQuirk(final CaptureConfig.Builder builder) {
            final Camera2ImplConfig.Builder builder2 = new Camera2ImplConfig.Builder();
            builder2.setCaptureRequestOption((android.hardware.camera2.CaptureRequest$Key<Integer>)CaptureRequest.CONTROL_AE_MODE, 3);
            builder.addImplementationOptions(builder2.build());
        }
        
        private void applyStillCaptureTemplate(final CaptureConfig.Builder builder, final CaptureConfig captureConfig) {
            int templateType;
            if (this.mTemplate == 3 && !this.mIsLegacyDevice) {
                templateType = 4;
            }
            else if (captureConfig.getTemplateType() != -1 && captureConfig.getTemplateType() != 5) {
                templateType = -1;
            }
            else {
                templateType = 2;
            }
            if (templateType != -1) {
                builder.setTemplateType(templateType);
            }
        }
        
        private void setTimeout3A(final long mTimeout3A) {
            this.mTimeout3A = mTimeout3A;
        }
        
        void addTask(final PipelineTask pipelineTask) {
            this.mTasks.add(pipelineTask);
        }
        
        ListenableFuture<List<Void>> executeCapture(final List<CaptureConfig> list, final int n) {
            Object o = Futures.immediateFuture((V)null);
            if (!this.mTasks.isEmpty()) {
                ListenableFuture<TotalCaptureResult> listenableFuture;
                if (this.mPipelineSubTask.isCaptureResultNeeded()) {
                    listenableFuture = Camera2CapturePipeline.waitForResult(0L, this.mCameraControl, null);
                }
                else {
                    listenableFuture = (ListenableFuture<TotalCaptureResult>)Futures.immediateFuture((V)null);
                }
                o = FutureChain.from((com.google.common.util.concurrent.ListenableFuture<Object>)listenableFuture).transformAsync((AsyncFunction<? super Object, Object>)new Camera2CapturePipeline$Pipeline$$ExternalSyntheticLambda2(this, n), this.mExecutor).transformAsync((AsyncFunction<? super Object, Object>)new Camera2CapturePipeline$Pipeline$$ExternalSyntheticLambda3(this), this.mExecutor);
            }
            final FutureChain<Object> transformAsync = FutureChain.from((com.google.common.util.concurrent.ListenableFuture<Object>)o).transformAsync((AsyncFunction<? super Object, Object>)new Camera2CapturePipeline$Pipeline$$ExternalSyntheticLambda4(this, list, n), this.mExecutor);
            final PipelineTask mPipelineSubTask = this.mPipelineSubTask;
            Objects.requireNonNull(mPipelineSubTask);
            ((ListenableFuture)transformAsync).addListener(new Camera2CapturePipeline$Pipeline$$ExternalSyntheticLambda5(mPipelineSubTask), this.mExecutor);
            return (ListenableFuture<List<Void>>)transformAsync;
        }
        
        ListenableFuture<List<Void>> submitConfigsInternal(final List<CaptureConfig> list, final int n) {
            final ArrayList list2 = new ArrayList();
            final ArrayList list3 = new ArrayList();
            for (final CaptureConfig captureConfig : list) {
                final CaptureConfig.Builder from = CaptureConfig.Builder.from(captureConfig);
                CameraCaptureResult retrieveCameraCaptureResult;
                final CameraCaptureResult cameraCaptureResult = retrieveCameraCaptureResult = null;
                if (captureConfig.getTemplateType() == 5) {
                    retrieveCameraCaptureResult = cameraCaptureResult;
                    if (!this.mCameraControl.getZslControl().isZslDisabledByFlashMode()) {
                        retrieveCameraCaptureResult = cameraCaptureResult;
                        if (!this.mCameraControl.getZslControl().isZslDisabledByUserCaseConfig()) {
                            final ImageProxy dequeueImageFromBuffer = this.mCameraControl.getZslControl().dequeueImageFromBuffer();
                            final boolean b = dequeueImageFromBuffer != null && this.mCameraControl.getZslControl().enqueueImageToImageWriter(dequeueImageFromBuffer);
                            retrieveCameraCaptureResult = cameraCaptureResult;
                            if (b) {
                                retrieveCameraCaptureResult = CameraCaptureResults.retrieveCameraCaptureResult(dequeueImageFromBuffer.getImageInfo());
                            }
                        }
                    }
                }
                if (retrieveCameraCaptureResult != null) {
                    from.setCameraCaptureResult(retrieveCameraCaptureResult);
                }
                else {
                    this.applyStillCaptureTemplate(from, captureConfig);
                }
                if (this.mOverrideAeModeForStillCapture.shouldSetAeModeAlwaysFlash(n)) {
                    this.applyAeModeQuirk(from);
                }
                list2.add(CallbackToFutureAdapter.getFuture((CallbackToFutureAdapter.Resolver<Object>)new Camera2CapturePipeline$Pipeline$$ExternalSyntheticLambda0(this, from)));
                list3.add(from.build());
            }
            this.mCameraControl.submitCaptureRequestsInternal(list3);
            return (ListenableFuture<List<Void>>)Futures.allAsList((Collection<? extends com.google.common.util.concurrent.ListenableFuture<?>>)list2);
        }
    }
    
    static class ResultListener implements CaptureResultListener
    {
        static final long NO_TIMEOUT = 0L;
        private final Checker mChecker;
        private CallbackToFutureAdapter.Completer<TotalCaptureResult> mCompleter;
        private final ListenableFuture<TotalCaptureResult> mFuture;
        private final long mTimeLimitNs;
        private volatile Long mTimestampOfFirstUpdateNs;
        
        ResultListener(final long mTimeLimitNs, final Checker mChecker) {
            this.mFuture = CallbackToFutureAdapter.getFuture((CallbackToFutureAdapter.Resolver<TotalCaptureResult>)new Camera2CapturePipeline$ResultListener$$ExternalSyntheticLambda0(this));
            this.mTimestampOfFirstUpdateNs = null;
            this.mTimeLimitNs = mTimeLimitNs;
            this.mChecker = mChecker;
        }
        
        public ListenableFuture<TotalCaptureResult> getFuture() {
            return this.mFuture;
        }
        
        @Override
        public boolean onCaptureResult(final TotalCaptureResult totalCaptureResult) {
            final Long n = (Long)totalCaptureResult.get(CaptureResult.SENSOR_TIMESTAMP);
            if (n != null && this.mTimestampOfFirstUpdateNs == null) {
                this.mTimestampOfFirstUpdateNs = n;
            }
            final Long mTimestampOfFirstUpdateNs = this.mTimestampOfFirstUpdateNs;
            if (0L != this.mTimeLimitNs && mTimestampOfFirstUpdateNs != null && n != null && n - mTimestampOfFirstUpdateNs > this.mTimeLimitNs) {
                this.mCompleter.set(null);
                final StringBuilder sb = new StringBuilder();
                sb.append("Wait for capture result timeout, current:");
                sb.append(n);
                sb.append(" first: ");
                sb.append(mTimestampOfFirstUpdateNs);
                Logger.d("Camera2CapturePipeline", sb.toString());
                return true;
            }
            final Checker mChecker = this.mChecker;
            if (mChecker != null && !mChecker.check(totalCaptureResult)) {
                return false;
            }
            this.mCompleter.set(totalCaptureResult);
            return true;
        }
        
        interface Checker
        {
            boolean check(final TotalCaptureResult p0);
        }
    }
    
    static class TorchTask implements PipelineTask
    {
        private static final long CHECK_3A_WITH_TORCH_TIMEOUT_IN_NS;
        private final Camera2CameraControlImpl mCameraControl;
        private final Executor mExecutor;
        private final int mFlashMode;
        private boolean mIsExecuted;
        
        static {
            CHECK_3A_WITH_TORCH_TIMEOUT_IN_NS = TimeUnit.SECONDS.toNanos(2L);
        }
        
        TorchTask(final Camera2CameraControlImpl mCameraControl, final int mFlashMode, final Executor mExecutor) {
            this.mIsExecuted = false;
            this.mCameraControl = mCameraControl;
            this.mFlashMode = mFlashMode;
            this.mExecutor = mExecutor;
        }
        
        @Override
        public boolean isCaptureResultNeeded() {
            return this.mFlashMode == 0;
        }
        
        @Override
        public void postCapture() {
            if (this.mIsExecuted) {
                this.mCameraControl.getTorchControl().enableTorchInternal(null, false);
                Logger.d("Camera2CapturePipeline", "Turn off torch");
            }
        }
        
        @Override
        public ListenableFuture<Boolean> preCapture(final TotalCaptureResult totalCaptureResult) {
            if (Camera2CapturePipeline.isFlashRequired(this.mFlashMode, totalCaptureResult)) {
                if (!this.mCameraControl.isTorchOn()) {
                    Logger.d("Camera2CapturePipeline", "Turn on torch");
                    this.mIsExecuted = true;
                    return (ListenableFuture<Boolean>)FutureChain.from((com.google.common.util.concurrent.ListenableFuture<Object>)CallbackToFutureAdapter.getFuture((CallbackToFutureAdapter.Resolver<V>)new Camera2CapturePipeline$TorchTask$$ExternalSyntheticLambda1(this))).transformAsync((AsyncFunction<? super Object, Object>)new Camera2CapturePipeline$TorchTask$$ExternalSyntheticLambda2(this), this.mExecutor).transform((Function<? super Object, Object>)new Camera2CapturePipeline$TorchTask$$ExternalSyntheticLambda3(), CameraXExecutors.directExecutor());
                }
                Logger.d("Camera2CapturePipeline", "Torch already on, not turn on");
            }
            return Futures.immediateFuture(false);
        }
    }
}

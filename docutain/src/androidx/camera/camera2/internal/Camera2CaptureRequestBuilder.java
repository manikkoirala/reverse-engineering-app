// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.internal;

import java.util.ArrayList;
import android.hardware.camera2.CameraAccessException;
import androidx.camera.core.impl.CameraCaptureResult;
import java.util.List;
import android.hardware.camera2.TotalCaptureResult;
import android.os.Build$VERSION;
import android.hardware.camera2.CaptureRequest;
import android.view.Surface;
import androidx.camera.core.impl.DeferrableSurface;
import java.util.Map;
import android.hardware.camera2.CameraDevice;
import androidx.camera.core.impl.CaptureConfig;
import java.util.Iterator;
import androidx.camera.core.Logger;
import android.hardware.camera2.CaptureRequest$Key;
import androidx.camera.camera2.interop.CaptureRequestOptions;
import androidx.camera.core.impl.Config;
import android.hardware.camera2.CaptureRequest$Builder;

class Camera2CaptureRequestBuilder
{
    private static final String TAG = "Camera2CaptureRequestBuilder";
    
    private Camera2CaptureRequestBuilder() {
    }
    
    private static void applyImplementationOptionToCaptureBuilder(final CaptureRequest$Builder captureRequest$Builder, Config build) {
        build = CaptureRequestOptions.Builder.from(build).build();
        for (final Config.Option option : ((CaptureRequestOptions)build).listOptions()) {
            final CaptureRequest$Key obj = (CaptureRequest$Key)option.getToken();
            try {
                captureRequest$Builder.set(obj, ((CaptureRequestOptions)build).retrieveOption(option));
            }
            catch (final IllegalArgumentException ex) {
                final StringBuilder sb = new StringBuilder();
                sb.append("CaptureRequest.Key is not supported: ");
                sb.append(obj);
                Logger.e("Camera2CaptureRequestBuilder", sb.toString());
            }
        }
    }
    
    public static CaptureRequest build(final CaptureConfig captureConfig, final CameraDevice cameraDevice, final Map<DeferrableSurface, Surface> map) throws CameraAccessException {
        if (cameraDevice == null) {
            return null;
        }
        final List<Surface> configuredSurfaces = getConfiguredSurfaces(captureConfig.getSurfaces(), map);
        if (configuredSurfaces.isEmpty()) {
            return null;
        }
        final CameraCaptureResult cameraCaptureResult = captureConfig.getCameraCaptureResult();
        CaptureRequest$Builder captureRequest$Builder;
        if (Build$VERSION.SDK_INT >= 23 && captureConfig.getTemplateType() == 5 && cameraCaptureResult != null && cameraCaptureResult.getCaptureResult() instanceof TotalCaptureResult) {
            Logger.d("Camera2CaptureRequestBuilder", "createReprocessCaptureRequest");
            captureRequest$Builder = Api23Impl.createReprocessCaptureRequest(cameraDevice, (TotalCaptureResult)cameraCaptureResult.getCaptureResult());
        }
        else {
            Logger.d("Camera2CaptureRequestBuilder", "createCaptureRequest");
            captureRequest$Builder = cameraDevice.createCaptureRequest(captureConfig.getTemplateType());
        }
        applyImplementationOptionToCaptureBuilder(captureRequest$Builder, captureConfig.getImplementationOptions());
        if (captureConfig.getImplementationOptions().containsOption((Config.Option<?>)CaptureConfig.OPTION_ROTATION)) {
            captureRequest$Builder.set(CaptureRequest.JPEG_ORIENTATION, (Object)captureConfig.getImplementationOptions().retrieveOption(CaptureConfig.OPTION_ROTATION));
        }
        if (captureConfig.getImplementationOptions().containsOption((Config.Option<?>)CaptureConfig.OPTION_JPEG_QUALITY)) {
            captureRequest$Builder.set(CaptureRequest.JPEG_QUALITY, (Object)captureConfig.getImplementationOptions().retrieveOption(CaptureConfig.OPTION_JPEG_QUALITY).byteValue());
        }
        final Iterator<Surface> iterator = configuredSurfaces.iterator();
        while (iterator.hasNext()) {
            captureRequest$Builder.addTarget((Surface)iterator.next());
        }
        captureRequest$Builder.setTag((Object)captureConfig.getTagBundle());
        return captureRequest$Builder.build();
    }
    
    public static CaptureRequest buildWithoutTarget(final CaptureConfig captureConfig, final CameraDevice cameraDevice) throws CameraAccessException {
        if (cameraDevice == null) {
            return null;
        }
        final CaptureRequest$Builder captureRequest = cameraDevice.createCaptureRequest(captureConfig.getTemplateType());
        applyImplementationOptionToCaptureBuilder(captureRequest, captureConfig.getImplementationOptions());
        return captureRequest.build();
    }
    
    private static List<Surface> getConfiguredSurfaces(final List<DeferrableSurface> list, final Map<DeferrableSurface, Surface> map) {
        final ArrayList list2 = new ArrayList();
        final Iterator<DeferrableSurface> iterator = list.iterator();
        while (iterator.hasNext()) {
            final Surface surface = map.get(iterator.next());
            if (surface == null) {
                throw new IllegalArgumentException("DeferrableSurface not in configuredSurfaceMap");
            }
            list2.add(surface);
        }
        return list2;
    }
    
    static class Api23Impl
    {
        private Api23Impl() {
        }
        
        static CaptureRequest$Builder createReprocessCaptureRequest(final CameraDevice cameraDevice, final TotalCaptureResult totalCaptureResult) throws CameraAccessException {
            return cameraDevice.createReprocessCaptureRequest(totalCaptureResult);
        }
    }
}

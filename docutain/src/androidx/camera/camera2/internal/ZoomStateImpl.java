// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.internal;

import androidx.core.math.MathUtils;
import androidx.camera.core.ZoomState;

class ZoomStateImpl implements ZoomState
{
    private float mLinearZoom;
    private final float mMaxZoomRatio;
    private final float mMinZoomRatio;
    private float mZoomRatio;
    
    ZoomStateImpl(final float mMaxZoomRatio, final float mMinZoomRatio) {
        this.mMaxZoomRatio = mMaxZoomRatio;
        this.mMinZoomRatio = mMinZoomRatio;
    }
    
    private float getPercentageByRatio(float n) {
        final float mMaxZoomRatio = this.mMaxZoomRatio;
        final float mMinZoomRatio = this.mMinZoomRatio;
        if (mMaxZoomRatio == mMinZoomRatio) {
            return 0.0f;
        }
        if (n == mMaxZoomRatio) {
            return 1.0f;
        }
        if (n == mMinZoomRatio) {
            return 0.0f;
        }
        n = 1.0f / n;
        final float n2 = 1.0f / mMaxZoomRatio;
        final float n3 = 1.0f / mMinZoomRatio;
        return (n - n3) / (n2 - n3);
    }
    
    private float getRatioByPercentage(final float n) {
        if (n == 1.0f) {
            return this.mMaxZoomRatio;
        }
        if (n == 0.0f) {
            return this.mMinZoomRatio;
        }
        final float mMaxZoomRatio = this.mMaxZoomRatio;
        final double n2 = 1.0f / mMaxZoomRatio;
        final float mMinZoomRatio = this.mMinZoomRatio;
        final double n3 = 1.0f / mMinZoomRatio;
        return (float)MathUtils.clamp(1.0 / (n3 + (n2 - n3) * n), mMinZoomRatio, mMaxZoomRatio);
    }
    
    @Override
    public float getLinearZoom() {
        return this.mLinearZoom;
    }
    
    @Override
    public float getMaxZoomRatio() {
        return this.mMaxZoomRatio;
    }
    
    @Override
    public float getMinZoomRatio() {
        return this.mMinZoomRatio;
    }
    
    @Override
    public float getZoomRatio() {
        return this.mZoomRatio;
    }
    
    void setLinearZoom(final float n) throws IllegalArgumentException {
        if (n <= 1.0f && n >= 0.0f) {
            this.mLinearZoom = n;
            this.mZoomRatio = this.getRatioByPercentage(n);
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Requested linearZoom ");
        sb.append(n);
        sb.append(" is not within valid range [0..1]");
        throw new IllegalArgumentException(sb.toString());
    }
    
    void setZoomRatio(final float n) throws IllegalArgumentException {
        if (n <= this.mMaxZoomRatio && n >= this.mMinZoomRatio) {
            this.mZoomRatio = n;
            this.mLinearZoom = this.getPercentageByRatio(n);
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Requested zoomRatio ");
        sb.append(n);
        sb.append(" is not within valid range [");
        sb.append(this.mMinZoomRatio);
        sb.append(" , ");
        sb.append(this.mMaxZoomRatio);
        sb.append("]");
        throw new IllegalArgumentException(sb.toString());
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2;

import androidx.camera.camera2.internal.Camera2UseCaseConfigFactory;
import androidx.camera.core.CameraUnavailableException;
import androidx.camera.core.InitializationException;
import androidx.camera.camera2.internal.Camera2DeviceSurfaceManager;
import java.util.Set;
import android.content.Context;
import androidx.camera.core.impl.UseCaseConfigFactory;
import androidx.camera.core.impl.CameraDeviceSurfaceManager;
import androidx.camera.core.impl.CameraFactory;
import androidx.camera.core.CameraXConfig;

public final class Camera2Config
{
    private Camera2Config() {
    }
    
    public static CameraXConfig defaultConfig() {
        return new CameraXConfig.Builder().setCameraFactoryProvider(new Camera2Config$$ExternalSyntheticLambda0()).setDeviceSurfaceManagerProvider(new Camera2Config$$ExternalSyntheticLambda1()).setUseCaseConfigFactoryProvider(new Camera2Config$$ExternalSyntheticLambda2()).build();
    }
    
    public static final class DefaultProvider implements Provider
    {
        @Override
        public CameraXConfig getCameraXConfig() {
            return Camera2Config.defaultConfig();
        }
    }
}

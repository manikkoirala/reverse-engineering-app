// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.impl;

import java.util.Iterator;
import androidx.camera.core.impl.MutableConfig;
import androidx.camera.core.impl.OptionsBundle;
import androidx.camera.core.impl.MutableOptionsBundle;
import androidx.camera.core.ExtendableBuilder;
import android.hardware.camera2.CaptureRequest$Key;
import android.hardware.camera2.CameraCaptureSession$StateCallback;
import android.hardware.camera2.CameraCaptureSession$CaptureCallback;
import android.hardware.camera2.CameraDevice$StateCallback;
import androidx.camera.core.impl.Config;
import androidx.camera.camera2.interop.CaptureRequestOptions;

public final class Camera2ImplConfig extends CaptureRequestOptions
{
    public static final Option<CameraEventCallbacks> CAMERA_EVENT_CALLBACK_OPTION;
    public static final String CAPTURE_REQUEST_ID_STEM = "camera2.captureRequest.option.";
    public static final Option<Object> CAPTURE_REQUEST_TAG_OPTION;
    public static final Option<CameraDevice$StateCallback> DEVICE_STATE_CALLBACK_OPTION;
    public static final Option<CameraCaptureSession$CaptureCallback> SESSION_CAPTURE_CALLBACK_OPTION;
    public static final Option<String> SESSION_PHYSICAL_CAMERA_ID_OPTION;
    public static final Option<CameraCaptureSession$StateCallback> SESSION_STATE_CALLBACK_OPTION;
    public static final Option<Long> STREAM_USE_CASE_OPTION;
    public static final Option<Integer> TEMPLATE_TYPE_OPTION;
    
    static {
        TEMPLATE_TYPE_OPTION = (Option)Config.Option.create("camera2.captureRequest.templateType", Integer.TYPE);
        STREAM_USE_CASE_OPTION = (Option)Config.Option.create("camera2.cameraCaptureSession.streamUseCase", Long.TYPE);
        DEVICE_STATE_CALLBACK_OPTION = (Option)Config.Option.create("camera2.cameraDevice.stateCallback", CameraDevice$StateCallback.class);
        SESSION_STATE_CALLBACK_OPTION = (Option)Config.Option.create("camera2.cameraCaptureSession.stateCallback", CameraCaptureSession$StateCallback.class);
        SESSION_CAPTURE_CALLBACK_OPTION = (Option)Config.Option.create("camera2.cameraCaptureSession.captureCallback", CameraCaptureSession$CaptureCallback.class);
        CAMERA_EVENT_CALLBACK_OPTION = (Option)Config.Option.create("camera2.cameraEvent.callback", CameraEventCallbacks.class);
        CAPTURE_REQUEST_TAG_OPTION = (Option)Config.Option.create("camera2.captureRequest.tag", Object.class);
        SESSION_PHYSICAL_CAMERA_ID_OPTION = (Option)Config.Option.create("camera2.cameraCaptureSession.physicalCameraId", String.class);
    }
    
    public Camera2ImplConfig(final Config config) {
        super(config);
    }
    
    public static Option<Object> createCaptureRequestOption(final CaptureRequest$Key<?> captureRequest$Key) {
        final StringBuilder sb = new StringBuilder();
        sb.append("camera2.captureRequest.option.");
        sb.append(captureRequest$Key.getName());
        return Config.Option.create(sb.toString(), Object.class, captureRequest$Key);
    }
    
    public CameraEventCallbacks getCameraEventCallback(final CameraEventCallbacks cameraEventCallbacks) {
        return this.getConfig().retrieveOption(Camera2ImplConfig.CAMERA_EVENT_CALLBACK_OPTION, cameraEventCallbacks);
    }
    
    public CaptureRequestOptions getCaptureRequestOptions() {
        return CaptureRequestOptions.Builder.from(this.getConfig()).build();
    }
    
    public Object getCaptureRequestTag(final Object o) {
        return this.getConfig().retrieveOption(Camera2ImplConfig.CAPTURE_REQUEST_TAG_OPTION, o);
    }
    
    public int getCaptureRequestTemplate(final int i) {
        return this.getConfig().retrieveOption(Camera2ImplConfig.TEMPLATE_TYPE_OPTION, i);
    }
    
    public CameraDevice$StateCallback getDeviceStateCallback(final CameraDevice$StateCallback cameraDevice$StateCallback) {
        return this.getConfig().retrieveOption(Camera2ImplConfig.DEVICE_STATE_CALLBACK_OPTION, cameraDevice$StateCallback);
    }
    
    public String getPhysicalCameraId(final String s) {
        return this.getConfig().retrieveOption(Camera2ImplConfig.SESSION_PHYSICAL_CAMERA_ID_OPTION, s);
    }
    
    public CameraCaptureSession$CaptureCallback getSessionCaptureCallback(final CameraCaptureSession$CaptureCallback cameraCaptureSession$CaptureCallback) {
        return this.getConfig().retrieveOption(Camera2ImplConfig.SESSION_CAPTURE_CALLBACK_OPTION, cameraCaptureSession$CaptureCallback);
    }
    
    public CameraCaptureSession$StateCallback getSessionStateCallback(final CameraCaptureSession$StateCallback cameraCaptureSession$StateCallback) {
        return this.getConfig().retrieveOption(Camera2ImplConfig.SESSION_STATE_CALLBACK_OPTION, cameraCaptureSession$StateCallback);
    }
    
    public long getStreamUseCase(final long l) {
        return this.getConfig().retrieveOption(Camera2ImplConfig.STREAM_USE_CASE_OPTION, l);
    }
    
    public static final class Builder implements ExtendableBuilder<Camera2ImplConfig>
    {
        private final MutableOptionsBundle mMutableOptionsBundle;
        
        public Builder() {
            this.mMutableOptionsBundle = MutableOptionsBundle.create();
        }
        
        @Override
        public Camera2ImplConfig build() {
            return new Camera2ImplConfig(OptionsBundle.from(this.mMutableOptionsBundle));
        }
        
        @Override
        public MutableConfig getMutableConfig() {
            return this.mMutableOptionsBundle;
        }
        
        public Builder insertAllOptions(final Config config) {
            for (final Config.Option<ValueT> option : config.listOptions()) {
                this.mMutableOptionsBundle.insertOption((Option<Object>)option, config.retrieveOption((Config.Option<ValueT>)option));
            }
            return this;
        }
        
        public <ValueT> Builder setCaptureRequestOption(final CaptureRequest$Key<ValueT> captureRequest$Key, final ValueT valueT) {
            this.mMutableOptionsBundle.insertOption((Option<ValueT>)Camera2ImplConfig.createCaptureRequestOption(captureRequest$Key), valueT);
            return this;
        }
        
        public <ValueT> Builder setCaptureRequestOptionWithPriority(final CaptureRequest$Key<ValueT> captureRequest$Key, final ValueT valueT, final OptionPriority optionPriority) {
            this.mMutableOptionsBundle.insertOption((Option<ValueT>)Camera2ImplConfig.createCaptureRequestOption(captureRequest$Key), optionPriority, valueT);
            return this;
        }
    }
    
    public static final class Extender<T>
    {
        ExtendableBuilder<T> mBaseBuilder;
        
        public Extender(final ExtendableBuilder<T> mBaseBuilder) {
            this.mBaseBuilder = mBaseBuilder;
        }
        
        public Extender<T> setCameraEventCallback(final CameraEventCallbacks cameraEventCallbacks) {
            this.mBaseBuilder.getMutableConfig().insertOption(Camera2ImplConfig.CAMERA_EVENT_CALLBACK_OPTION, cameraEventCallbacks);
            return this;
        }
    }
}

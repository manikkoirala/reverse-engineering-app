// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.impl;

import androidx.camera.core.impl.CaptureConfig;
import java.util.Iterator;
import java.util.ArrayList;
import java.util.List;
import java.util.Arrays;
import androidx.camera.core.impl.MultiValueSet;

public final class CameraEventCallbacks extends MultiValueSet<CameraEventCallback>
{
    public CameraEventCallbacks(final CameraEventCallback... a) {
        this.addAll(Arrays.asList(a));
    }
    
    public static CameraEventCallbacks createEmptyCallback() {
        return new CameraEventCallbacks(new CameraEventCallback[0]);
    }
    
    @Override
    public MultiValueSet<CameraEventCallback> clone() {
        final CameraEventCallbacks emptyCallback = createEmptyCallback();
        emptyCallback.addAll(this.getAllItems());
        return emptyCallback;
    }
    
    public ComboCameraEventCallback createComboCallback() {
        return new ComboCameraEventCallback(this.getAllItems());
    }
    
    public static final class ComboCameraEventCallback
    {
        private final List<CameraEventCallback> mCallbacks;
        
        ComboCameraEventCallback(final List<CameraEventCallback> list) {
            this.mCallbacks = new ArrayList<CameraEventCallback>();
            final Iterator<CameraEventCallback> iterator = list.iterator();
            while (iterator.hasNext()) {
                this.mCallbacks.add(iterator.next());
            }
        }
        
        public List<CameraEventCallback> getCallbacks() {
            return this.mCallbacks;
        }
        
        public void onDeInitSession() {
            final Iterator<CameraEventCallback> iterator = this.mCallbacks.iterator();
            while (iterator.hasNext()) {
                iterator.next().onDeInitSession();
            }
        }
        
        public List<CaptureConfig> onDisableSession() {
            final ArrayList list = new ArrayList();
            final Iterator<CameraEventCallback> iterator = this.mCallbacks.iterator();
            while (iterator.hasNext()) {
                final CaptureConfig onDisableSession = iterator.next().onDisableSession();
                if (onDisableSession != null) {
                    list.add(onDisableSession);
                }
            }
            return list;
        }
        
        public List<CaptureConfig> onEnableSession() {
            final ArrayList list = new ArrayList();
            final Iterator<CameraEventCallback> iterator = this.mCallbacks.iterator();
            while (iterator.hasNext()) {
                final CaptureConfig onEnableSession = iterator.next().onEnableSession();
                if (onEnableSession != null) {
                    list.add(onEnableSession);
                }
            }
            return list;
        }
        
        public List<CaptureConfig> onInitSession() {
            final ArrayList list = new ArrayList();
            final Iterator<CameraEventCallback> iterator = this.mCallbacks.iterator();
            while (iterator.hasNext()) {
                final CaptureConfig onInitSession = iterator.next().onInitSession();
                if (onInitSession != null) {
                    list.add(onInitSession);
                }
            }
            return list;
        }
        
        public List<CaptureConfig> onRepeating() {
            final ArrayList list = new ArrayList();
            final Iterator<CameraEventCallback> iterator = this.mCallbacks.iterator();
            while (iterator.hasNext()) {
                final CaptureConfig onRepeating = iterator.next().onRepeating();
                if (onRepeating != null) {
                    list.add(onRepeating);
                }
            }
            return list;
        }
    }
}

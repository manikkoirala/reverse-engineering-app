// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.impl;

import androidx.camera.camera2.internal.Camera2CameraCaptureResult;
import android.hardware.camera2.CaptureResult;
import androidx.camera.core.impl.CameraCaptureResult;
import androidx.camera.camera2.internal.Camera2CameraCaptureFailure;
import android.hardware.camera2.CaptureFailure;
import androidx.camera.core.impl.CameraCaptureFailure;

public final class Camera2CameraCaptureResultConverter
{
    private Camera2CameraCaptureResultConverter() {
    }
    
    public static CaptureFailure getCaptureFailure(final CameraCaptureFailure cameraCaptureFailure) {
        if (cameraCaptureFailure instanceof Camera2CameraCaptureFailure) {
            return ((Camera2CameraCaptureFailure)cameraCaptureFailure).getCaptureFailure();
        }
        return null;
    }
    
    public static CaptureResult getCaptureResult(final CameraCaptureResult cameraCaptureResult) {
        if (cameraCaptureResult instanceof Camera2CameraCaptureResult) {
            return ((Camera2CameraCaptureResult)cameraCaptureResult).getCaptureResult();
        }
        return null;
    }
}

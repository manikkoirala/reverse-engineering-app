// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.lifecycle;

import androidx.lifecycle.LifecycleOwner;
import androidx.camera.core.internal.CameraUseCaseAdapter;

final class AutoValue_LifecycleCameraRepository_Key extends Key
{
    private final CameraUseCaseAdapter.CameraId cameraId;
    private final LifecycleOwner lifecycleOwner;
    
    AutoValue_LifecycleCameraRepository_Key(final LifecycleOwner lifecycleOwner, final CameraUseCaseAdapter.CameraId cameraId) {
        if (lifecycleOwner == null) {
            throw new NullPointerException("Null lifecycleOwner");
        }
        this.lifecycleOwner = lifecycleOwner;
        if (cameraId != null) {
            this.cameraId = cameraId;
            return;
        }
        throw new NullPointerException("Null cameraId");
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o == this) {
            return true;
        }
        if (o instanceof Key) {
            final Key key = (Key)o;
            if (!this.lifecycleOwner.equals(key.getLifecycleOwner()) || !this.cameraId.equals(key.getCameraId())) {
                b = false;
            }
            return b;
        }
        return false;
    }
    
    @Override
    public CameraUseCaseAdapter.CameraId getCameraId() {
        return this.cameraId;
    }
    
    @Override
    public LifecycleOwner getLifecycleOwner() {
        return this.lifecycleOwner;
    }
    
    @Override
    public int hashCode() {
        return (this.lifecycleOwner.hashCode() ^ 0xF4243) * 1000003 ^ this.cameraId.hashCode();
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("Key{lifecycleOwner=");
        sb.append(this.lifecycleOwner);
        sb.append(", cameraId=");
        sb.append(this.cameraId);
        sb.append("}");
        return sb.toString();
    }
}

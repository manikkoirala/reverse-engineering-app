// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.lifecycle;

import java.util.ArrayList;
import android.os.Build$VERSION;
import androidx.lifecycle.OnLifecycleEvent;
import java.util.Collections;
import java.util.List;
import androidx.camera.core.impl.CameraConfig;
import androidx.camera.core.impl.CameraInternal;
import java.util.LinkedHashSet;
import androidx.camera.core.CameraInfo;
import androidx.camera.core.CameraControl;
import androidx.camera.core.UseCase;
import java.util.Collection;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleOwner;
import androidx.camera.core.internal.CameraUseCaseAdapter;
import androidx.camera.core.Camera;
import androidx.lifecycle.LifecycleObserver;

final class LifecycleCamera implements LifecycleObserver, Camera
{
    private final CameraUseCaseAdapter mCameraUseCaseAdapter;
    private volatile boolean mIsActive;
    private final LifecycleOwner mLifecycleOwner;
    private final Object mLock;
    private boolean mReleased;
    private boolean mSuspended;
    
    LifecycleCamera(final LifecycleOwner mLifecycleOwner, final CameraUseCaseAdapter mCameraUseCaseAdapter) {
        this.mLock = new Object();
        this.mIsActive = false;
        this.mSuspended = false;
        this.mReleased = false;
        this.mLifecycleOwner = mLifecycleOwner;
        this.mCameraUseCaseAdapter = mCameraUseCaseAdapter;
        if (mLifecycleOwner.getLifecycle().getCurrentState().isAtLeast(Lifecycle.State.STARTED)) {
            mCameraUseCaseAdapter.attachUseCases();
        }
        else {
            mCameraUseCaseAdapter.detachUseCases();
        }
        mLifecycleOwner.getLifecycle().addObserver(this);
    }
    
    void bind(final Collection<UseCase> collection) throws CameraUseCaseAdapter.CameraException {
        synchronized (this.mLock) {
            this.mCameraUseCaseAdapter.addUseCases(collection);
        }
    }
    
    @Override
    public CameraControl getCameraControl() {
        return this.mCameraUseCaseAdapter.getCameraControl();
    }
    
    @Override
    public CameraInfo getCameraInfo() {
        return this.mCameraUseCaseAdapter.getCameraInfo();
    }
    
    @Override
    public LinkedHashSet<CameraInternal> getCameraInternals() {
        return this.mCameraUseCaseAdapter.getCameraInternals();
    }
    
    public CameraUseCaseAdapter getCameraUseCaseAdapter() {
        return this.mCameraUseCaseAdapter;
    }
    
    @Override
    public CameraConfig getExtendedConfig() {
        return this.mCameraUseCaseAdapter.getExtendedConfig();
    }
    
    public LifecycleOwner getLifecycleOwner() {
        synchronized (this.mLock) {
            return this.mLifecycleOwner;
        }
    }
    
    public List<UseCase> getUseCases() {
        synchronized (this.mLock) {
            return Collections.unmodifiableList((List<? extends UseCase>)this.mCameraUseCaseAdapter.getUseCases());
        }
    }
    
    public boolean isActive() {
        synchronized (this.mLock) {
            return this.mIsActive;
        }
    }
    
    public boolean isBound(final UseCase useCase) {
        synchronized (this.mLock) {
            return this.mCameraUseCaseAdapter.getUseCases().contains(useCase);
        }
    }
    
    @Override
    public boolean isUseCasesCombinationSupported(final UseCase... array) {
        return this.mCameraUseCaseAdapter.isUseCasesCombinationSupported(array);
    }
    
    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    public void onDestroy(final LifecycleOwner lifecycleOwner) {
        synchronized (this.mLock) {
            final CameraUseCaseAdapter mCameraUseCaseAdapter = this.mCameraUseCaseAdapter;
            mCameraUseCaseAdapter.removeUseCases(mCameraUseCaseAdapter.getUseCases());
        }
    }
    
    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    public void onPause(final LifecycleOwner lifecycleOwner) {
        if (Build$VERSION.SDK_INT >= 24) {
            this.mCameraUseCaseAdapter.setActiveResumingMode(false);
        }
    }
    
    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    public void onResume(final LifecycleOwner lifecycleOwner) {
        if (Build$VERSION.SDK_INT >= 24) {
            this.mCameraUseCaseAdapter.setActiveResumingMode(true);
        }
    }
    
    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    public void onStart(final LifecycleOwner lifecycleOwner) {
        synchronized (this.mLock) {
            if (!this.mSuspended && !this.mReleased) {
                this.mCameraUseCaseAdapter.attachUseCases();
                this.mIsActive = true;
            }
        }
    }
    
    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    public void onStop(final LifecycleOwner lifecycleOwner) {
        synchronized (this.mLock) {
            if (!this.mSuspended && !this.mReleased) {
                this.mCameraUseCaseAdapter.detachUseCases();
                this.mIsActive = false;
            }
        }
    }
    
    void release() {
        synchronized (this.mLock) {
            this.mReleased = true;
            this.mIsActive = false;
            this.mLifecycleOwner.getLifecycle().removeObserver(this);
        }
    }
    
    @Override
    public void setExtendedConfig(final CameraConfig extendedConfig) {
        this.mCameraUseCaseAdapter.setExtendedConfig(extendedConfig);
    }
    
    public void suspend() {
        synchronized (this.mLock) {
            if (this.mSuspended) {
                return;
            }
            this.onStop(this.mLifecycleOwner);
            this.mSuspended = true;
        }
    }
    
    void unbind(final Collection<UseCase> c) {
        synchronized (this.mLock) {
            final ArrayList list = new ArrayList(c);
            list.retainAll(this.mCameraUseCaseAdapter.getUseCases());
            this.mCameraUseCaseAdapter.removeUseCases(list);
        }
    }
    
    void unbindAll() {
        synchronized (this.mLock) {
            final CameraUseCaseAdapter mCameraUseCaseAdapter = this.mCameraUseCaseAdapter;
            mCameraUseCaseAdapter.removeUseCases(mCameraUseCaseAdapter.getUseCases());
        }
    }
    
    public void unsuspend() {
        synchronized (this.mLock) {
            if (!this.mSuspended) {
                return;
            }
            this.mSuspended = false;
            if (this.mLifecycleOwner.getLifecycle().getCurrentState().isAtLeast(Lifecycle.State.STARTED)) {
                this.onStart(this.mLifecycleOwner);
            }
        }
    }
}

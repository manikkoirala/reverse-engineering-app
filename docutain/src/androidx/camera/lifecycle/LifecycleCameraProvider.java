// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.lifecycle;

import androidx.camera.core.UseCase;
import androidx.camera.core.CameraProvider;

interface LifecycleCameraProvider extends CameraProvider
{
    boolean isBound(final UseCase p0);
    
    void unbind(final UseCase... p0);
    
    void unbindAll();
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core;

import android.media.ImageReader;
import androidx.camera.core.impl.ImageReaderProxy;

public final class ImageReaderProxys
{
    private ImageReaderProxys() {
    }
    
    public static ImageReaderProxy createIsolatedReader(final int n, final int n2, final int n3, final int n4) {
        return new AndroidImageReaderProxy(ImageReader.newInstance(n, n2, n3, n4));
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core;

import androidx.camera.core.impl.utils.ExifData;
import android.graphics.Matrix;
import androidx.camera.core.impl.TagBundle;

public abstract class ImmutableImageInfo implements ImageInfo
{
    public static ImageInfo create(final TagBundle tagBundle, final long n, final int n2, final Matrix matrix) {
        return new AutoValue_ImmutableImageInfo(tagBundle, n, n2, matrix);
    }
    
    @Override
    public abstract int getRotationDegrees();
    
    @Override
    public abstract Matrix getSensorToBufferTransformMatrix();
    
    @Override
    public abstract TagBundle getTagBundle();
    
    @Override
    public abstract long getTimestamp();
    
    @Override
    public void populateExifData(final ExifData.Builder builder) {
        builder.setOrientationDegrees(this.getRotationDegrees());
    }
}

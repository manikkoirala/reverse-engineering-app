// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core;

import android.os.Build$VERSION;
import android.util.Log;

public final class Logger
{
    static final int DEFAULT_MIN_LOG_LEVEL = 3;
    private static final int MAX_TAG_LENGTH = 23;
    private static int sMinLogLevel = 3;
    
    private Logger() {
    }
    
    public static void d(String truncateTag, final String s) {
        truncateTag = truncateTag(truncateTag);
        if (isLogLevelEnabled(truncateTag, 3)) {
            Log.d(truncateTag, s);
        }
    }
    
    public static void d(String truncateTag, final String s, final Throwable t) {
        truncateTag = truncateTag(truncateTag);
        if (isLogLevelEnabled(truncateTag, 3)) {
            Log.d(truncateTag, s, t);
        }
    }
    
    public static void e(String truncateTag, final String s) {
        truncateTag = truncateTag(truncateTag);
        if (isLogLevelEnabled(truncateTag, 6)) {
            Log.e(truncateTag, s);
        }
    }
    
    public static void e(String truncateTag, final String s, final Throwable t) {
        truncateTag = truncateTag(truncateTag);
        if (isLogLevelEnabled(truncateTag, 6)) {
            Log.e(truncateTag, s, t);
        }
    }
    
    static int getMinLogLevel() {
        return Logger.sMinLogLevel;
    }
    
    public static void i(String truncateTag, final String s) {
        truncateTag = truncateTag(truncateTag);
        if (isLogLevelEnabled(truncateTag, 4)) {
            Log.i(truncateTag, s);
        }
    }
    
    public static void i(String truncateTag, final String s, final Throwable t) {
        truncateTag = truncateTag(truncateTag);
        if (isLogLevelEnabled(truncateTag, 4)) {
            Log.i(truncateTag, s, t);
        }
    }
    
    public static boolean isDebugEnabled(final String s) {
        return isLogLevelEnabled(truncateTag(s), 3);
    }
    
    public static boolean isErrorEnabled(final String s) {
        return isLogLevelEnabled(truncateTag(s), 6);
    }
    
    public static boolean isInfoEnabled(final String s) {
        return isLogLevelEnabled(truncateTag(s), 4);
    }
    
    private static boolean isLogLevelEnabled(final String s, final int n) {
        return Logger.sMinLogLevel <= n || Log.isLoggable(s, n);
    }
    
    public static boolean isWarnEnabled(final String s) {
        return isLogLevelEnabled(truncateTag(s), 5);
    }
    
    static void resetMinLogLevel() {
        Logger.sMinLogLevel = 3;
    }
    
    static void setMinLogLevel(final int sMinLogLevel) {
        Logger.sMinLogLevel = sMinLogLevel;
    }
    
    private static String truncateTag(final String s) {
        String substring = s;
        if (Build$VERSION.SDK_INT <= 25) {
            substring = s;
            if (23 < s.length()) {
                substring = s.substring(0, 23);
            }
        }
        return substring;
    }
    
    public static void w(String truncateTag, final String s) {
        truncateTag = truncateTag(truncateTag);
        if (isLogLevelEnabled(truncateTag, 5)) {
            Log.w(truncateTag, s);
        }
    }
    
    public static void w(String truncateTag, final String s, final Throwable t) {
        truncateTag = truncateTag(truncateTag);
        if (isLogLevelEnabled(truncateTag, 5)) {
            Log.w(truncateTag, s, t);
        }
    }
}

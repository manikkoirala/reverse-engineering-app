// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core;

import java.nio.ByteBuffer;
import android.media.Image;
import android.graphics.Rect;

public interface ImageProxy extends AutoCloseable
{
    void close();
    
    Rect getCropRect();
    
    int getFormat();
    
    int getHeight();
    
    Image getImage();
    
    ImageInfo getImageInfo();
    
    PlaneProxy[] getPlanes();
    
    int getWidth();
    
    void setCropRect(final Rect p0);
    
    public interface PlaneProxy
    {
        ByteBuffer getBuffer();
        
        int getPixelStride();
        
        int getRowStride();
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core;

import java.util.concurrent.Executors;
import java.util.Collection;
import androidx.core.util.Preconditions;
import java.util.Iterator;
import androidx.camera.core.impl.CaptureStage;
import androidx.camera.core.impl.CaptureBundle;
import android.view.Surface;
import androidx.arch.core.util.Function;
import androidx.camera.core.impl.CameraCaptureCallback;
import androidx.camera.core.impl.utils.executor.CameraXExecutors;
import android.util.Size;
import android.media.ImageReader;
import androidx.camera.core.impl.utils.futures.Futures;
import java.util.ArrayList;
import java.util.Collections;
import androidx.camera.core.impl.ImageProxyBundle;
import java.util.concurrent.Executor;
import com.google.common.util.concurrent.ListenableFuture;
import androidx.concurrent.futures.CallbackToFutureAdapter;
import androidx.camera.core.impl.utils.futures.FutureCallback;
import androidx.camera.core.impl.CaptureProcessor;
import java.util.List;
import androidx.camera.core.impl.ImageReaderProxy;

class ProcessingImageReader implements ImageReaderProxy
{
    private static final int EXIF_MAX_SIZE_BYTES = 64000;
    private static final String TAG = "ProcessingImageReader";
    private final List<Integer> mCaptureIdList;
    final CaptureProcessor mCaptureProcessor;
    private FutureCallback<List<ImageProxy>> mCaptureStageReadyCallback;
    CallbackToFutureAdapter.Completer<Void> mCloseCompleter;
    private ListenableFuture<Void> mCloseFuture;
    boolean mClosed;
    Executor mErrorCallbackExecutor;
    Executor mExecutor;
    private OnImageAvailableListener mImageProcessedListener;
    final ImageReaderProxy mInputImageReader;
    OnImageAvailableListener mListener;
    final Object mLock;
    OnProcessingErrorCallback mOnProcessingErrorCallback;
    final ImageReaderProxy mOutputImageReader;
    final Executor mPostProcessExecutor;
    boolean mProcessing;
    SettableImageProxyBundle mSettableImageProxyBundle;
    private ListenableFuture<List<ImageProxy>> mSettableImageProxyFutureList;
    private String mTagBundleKey;
    private OnImageAvailableListener mTransformedListener;
    private final ListenableFuture<Void> mUnderlyingCaptureProcessorCloseFuture;
    
    ProcessingImageReader(final Builder builder) {
        this.mLock = new Object();
        this.mTransformedListener = new OnImageAvailableListener() {
            final ProcessingImageReader this$0;
            
            @Override
            public void onImageAvailable(final ImageReaderProxy imageReaderProxy) {
                this.this$0.imageIncoming(imageReaderProxy);
            }
        };
        this.mImageProcessedListener = new OnImageAvailableListener() {
            final ProcessingImageReader this$0;
            
            @Override
            public void onImageAvailable(final ImageReaderProxy imageReaderProxy) {
                synchronized (this.this$0.mLock) {
                    final OnImageAvailableListener mListener = this.this$0.mListener;
                    final Executor mExecutor = this.this$0.mExecutor;
                    this.this$0.mSettableImageProxyBundle.reset();
                    this.this$0.setupSettableImageProxyBundleCallbacks();
                    monitorexit(this.this$0.mLock);
                    if (mListener != null) {
                        if (mExecutor != null) {
                            mExecutor.execute(new ProcessingImageReader$2$$ExternalSyntheticLambda0(this, mListener));
                        }
                        else {
                            mListener.onImageAvailable(this.this$0);
                        }
                    }
                }
            }
        };
        this.mCaptureStageReadyCallback = new FutureCallback<List<ImageProxy>>() {
            final ProcessingImageReader this$0;
            
            @Override
            public void onFailure(final Throwable t) {
            }
            
            @Override
            public void onSuccess(List<ImageProxy> mOnProcessingErrorCallback) {
                synchronized (this.this$0.mLock) {
                    if (this.this$0.mClosed) {
                        return;
                    }
                    this.this$0.mProcessing = true;
                    final SettableImageProxyBundle mSettableImageProxyBundle = this.this$0.mSettableImageProxyBundle;
                    mOnProcessingErrorCallback = this.this$0.mOnProcessingErrorCallback;
                    final Executor mErrorCallbackExecutor = this.this$0.mErrorCallbackExecutor;
                    monitorexit(this.this$0.mLock);
                    try {
                        this.this$0.mCaptureProcessor.process(mSettableImageProxyBundle);
                    }
                    catch (final Exception ex) {
                        final Object mLock = this.this$0.mLock;
                        synchronized (this.this$0.mLock) {
                            this.this$0.mSettableImageProxyBundle.reset();
                            if (mOnProcessingErrorCallback != null && mErrorCallbackExecutor != null) {
                                mErrorCallbackExecutor.execute(new ProcessingImageReader$3$$ExternalSyntheticLambda0(mOnProcessingErrorCallback, ex));
                            }
                            monitorexit(this.this$0.mLock);
                            synchronized (this.this$0.mLock) {
                                this.this$0.mProcessing = false;
                                monitorexit(this.this$0.mLock);
                                this.this$0.closeAndCompleteFutureIfNecessary();
                            }
                        }
                    }
                }
            }
        };
        this.mClosed = false;
        this.mProcessing = false;
        this.mTagBundleKey = new String();
        this.mSettableImageProxyBundle = new SettableImageProxyBundle(Collections.emptyList(), this.mTagBundleKey);
        this.mCaptureIdList = new ArrayList<Integer>();
        this.mSettableImageProxyFutureList = (ListenableFuture<List<ImageProxy>>)Futures.immediateFuture(new ArrayList());
        if (builder.mInputImageReader.getMaxImages() >= builder.mCaptureBundle.getCaptureStages().size()) {
            final ImageReaderProxy mInputImageReader = builder.mInputImageReader;
            this.mInputImageReader = mInputImageReader;
            final int width = mInputImageReader.getWidth();
            final int height = mInputImageReader.getHeight();
            int n = width;
            int n2 = height;
            if (builder.mOutputFormat == 256) {
                n = (int)(width * height * 1.5f) + 64000;
                n2 = 1;
            }
            final AndroidImageReaderProxy mOutputImageReader = new AndroidImageReaderProxy(ImageReader.newInstance(n, n2, builder.mOutputFormat, mInputImageReader.getMaxImages()));
            this.mOutputImageReader = mOutputImageReader;
            this.mPostProcessExecutor = builder.mPostProcessExecutor;
            final CaptureProcessor mCaptureProcessor = builder.mCaptureProcessor;
            (this.mCaptureProcessor = mCaptureProcessor).onOutputSurface(mOutputImageReader.getSurface(), builder.mOutputFormat);
            mCaptureProcessor.onResolutionUpdate(new Size(mInputImageReader.getWidth(), mInputImageReader.getHeight()));
            this.mUnderlyingCaptureProcessorCloseFuture = mCaptureProcessor.getCloseFuture();
            this.setCaptureBundle(builder.mCaptureBundle);
            return;
        }
        throw new IllegalArgumentException("MetadataImageReader is smaller than CaptureBundle.");
    }
    
    private void cancelSettableImageProxyBundleFutureList() {
        synchronized (this.mLock) {
            if (!this.mSettableImageProxyFutureList.isDone()) {
                this.mSettableImageProxyFutureList.cancel(true);
            }
            this.mSettableImageProxyBundle.reset();
        }
    }
    
    @Override
    public ImageProxy acquireLatestImage() {
        synchronized (this.mLock) {
            return this.mOutputImageReader.acquireLatestImage();
        }
    }
    
    @Override
    public ImageProxy acquireNextImage() {
        synchronized (this.mLock) {
            return this.mOutputImageReader.acquireNextImage();
        }
    }
    
    @Override
    public void clearOnImageAvailableListener() {
        synchronized (this.mLock) {
            this.mListener = null;
            this.mExecutor = null;
            this.mInputImageReader.clearOnImageAvailableListener();
            this.mOutputImageReader.clearOnImageAvailableListener();
            if (!this.mProcessing) {
                this.mSettableImageProxyBundle.close();
            }
        }
    }
    
    @Override
    public void close() {
        synchronized (this.mLock) {
            if (this.mClosed) {
                return;
            }
            this.mInputImageReader.clearOnImageAvailableListener();
            this.mOutputImageReader.clearOnImageAvailableListener();
            this.mClosed = true;
            monitorexit(this.mLock);
            this.mCaptureProcessor.close();
            this.closeAndCompleteFutureIfNecessary();
        }
    }
    
    void closeAndCompleteFutureIfNecessary() {
        synchronized (this.mLock) {
            final boolean mClosed = this.mClosed;
            final boolean mProcessing = this.mProcessing;
            final CallbackToFutureAdapter.Completer<Void> mCloseCompleter = this.mCloseCompleter;
            if (mClosed && !mProcessing) {
                this.mInputImageReader.close();
                this.mSettableImageProxyBundle.close();
                this.mOutputImageReader.close();
            }
            monitorexit(this.mLock);
            if (mClosed && !mProcessing) {
                this.mUnderlyingCaptureProcessorCloseFuture.addListener((Runnable)new ProcessingImageReader$$ExternalSyntheticLambda2(this, mCloseCompleter), CameraXExecutors.directExecutor());
            }
        }
    }
    
    CameraCaptureCallback getCameraCaptureCallback() {
        synchronized (this.mLock) {
            final ImageReaderProxy mInputImageReader = this.mInputImageReader;
            if (mInputImageReader instanceof MetadataImageReader) {
                return ((MetadataImageReader)mInputImageReader).getCameraCaptureCallback();
            }
            return new CameraCaptureCallback(this) {
                final ProcessingImageReader this$0;
            };
        }
    }
    
    ListenableFuture<Void> getCloseFuture() {
        synchronized (this.mLock) {
            com.google.common.util.concurrent.ListenableFuture<Void> listenableFuture;
            if (this.mClosed && !this.mProcessing) {
                listenableFuture = Futures.transform(this.mUnderlyingCaptureProcessorCloseFuture, (Function<? super Void, ? extends Void>)new ProcessingImageReader$$ExternalSyntheticLambda0(), CameraXExecutors.directExecutor());
            }
            else {
                if (this.mCloseFuture == null) {
                    this.mCloseFuture = CallbackToFutureAdapter.getFuture((CallbackToFutureAdapter.Resolver<Void>)new ProcessingImageReader$$ExternalSyntheticLambda1(this));
                }
                listenableFuture = Futures.nonCancellationPropagating(this.mCloseFuture);
            }
            return listenableFuture;
        }
    }
    
    @Override
    public int getHeight() {
        synchronized (this.mLock) {
            return this.mInputImageReader.getHeight();
        }
    }
    
    @Override
    public int getImageFormat() {
        synchronized (this.mLock) {
            return this.mOutputImageReader.getImageFormat();
        }
    }
    
    @Override
    public int getMaxImages() {
        synchronized (this.mLock) {
            return this.mInputImageReader.getMaxImages();
        }
    }
    
    @Override
    public Surface getSurface() {
        synchronized (this.mLock) {
            return this.mInputImageReader.getSurface();
        }
    }
    
    public String getTagBundleKey() {
        return this.mTagBundleKey;
    }
    
    @Override
    public int getWidth() {
        synchronized (this.mLock) {
            return this.mInputImageReader.getWidth();
        }
    }
    
    void imageIncoming(final ImageReaderProxy imageReaderProxy) {
        synchronized (this.mLock) {
            if (this.mClosed) {
                return;
            }
            try {
                try {
                    final ImageProxy acquireNextImage = imageReaderProxy.acquireNextImage();
                    if (acquireNextImage == null) {
                        return;
                    }
                    final Integer obj = (Integer)acquireNextImage.getImageInfo().getTagBundle().getTag(this.mTagBundleKey);
                    if (!this.mCaptureIdList.contains(obj)) {
                        final StringBuilder sb = new StringBuilder();
                        sb.append("ImageProxyBundle does not contain this id: ");
                        sb.append(obj);
                        Logger.w("ProcessingImageReader", sb.toString());
                        acquireNextImage.close();
                        return;
                    }
                    this.mSettableImageProxyBundle.addImageProxy(acquireNextImage);
                }
                finally {}
            }
            catch (final IllegalStateException ex) {
                Logger.e("ProcessingImageReader", "Failed to acquire latest image.", ex);
            }
        }
    }
    
    public void setCaptureBundle(final CaptureBundle captureBundle) {
        synchronized (this.mLock) {
            if (this.mClosed) {
                return;
            }
            this.cancelSettableImageProxyBundleFutureList();
            if (captureBundle.getCaptureStages() != null) {
                if (this.mInputImageReader.getMaxImages() < captureBundle.getCaptureStages().size()) {
                    throw new IllegalArgumentException("CaptureBundle is larger than InputImageReader.");
                }
                this.mCaptureIdList.clear();
                for (final CaptureStage captureStage : captureBundle.getCaptureStages()) {
                    if (captureStage != null) {
                        this.mCaptureIdList.add(captureStage.getId());
                    }
                }
            }
            final String string = Integer.toString(captureBundle.hashCode());
            this.mTagBundleKey = string;
            this.mSettableImageProxyBundle = new SettableImageProxyBundle(this.mCaptureIdList, string);
            this.setupSettableImageProxyBundleCallbacks();
        }
    }
    
    @Override
    public void setOnImageAvailableListener(final OnImageAvailableListener onImageAvailableListener, final Executor executor) {
        synchronized (this.mLock) {
            this.mListener = Preconditions.checkNotNull(onImageAvailableListener);
            this.mExecutor = Preconditions.checkNotNull(executor);
            this.mInputImageReader.setOnImageAvailableListener(this.mTransformedListener, executor);
            this.mOutputImageReader.setOnImageAvailableListener(this.mImageProcessedListener, executor);
        }
    }
    
    public void setOnProcessingErrorCallback(final Executor mErrorCallbackExecutor, final OnProcessingErrorCallback mOnProcessingErrorCallback) {
        synchronized (this.mLock) {
            this.mErrorCallbackExecutor = mErrorCallbackExecutor;
            this.mOnProcessingErrorCallback = mOnProcessingErrorCallback;
        }
    }
    
    void setupSettableImageProxyBundleCallbacks() {
        final ArrayList list = new ArrayList();
        final Iterator<Integer> iterator = this.mCaptureIdList.iterator();
        while (iterator.hasNext()) {
            list.add(this.mSettableImageProxyBundle.getImageProxy(iterator.next()));
        }
        this.mSettableImageProxyFutureList = (ListenableFuture<List<ImageProxy>>)Futures.allAsList((Collection<? extends com.google.common.util.concurrent.ListenableFuture<?>>)list);
        Futures.addCallback(Futures.allAsList((Collection<? extends com.google.common.util.concurrent.ListenableFuture<?>>)list), (FutureCallback<? super List<Object>>)this.mCaptureStageReadyCallback, this.mPostProcessExecutor);
    }
    
    static final class Builder
    {
        protected final CaptureBundle mCaptureBundle;
        protected final CaptureProcessor mCaptureProcessor;
        protected final ImageReaderProxy mInputImageReader;
        protected int mOutputFormat;
        protected Executor mPostProcessExecutor;
        
        Builder(final int n, final int n2, final int n3, final int n4, final CaptureBundle captureBundle, final CaptureProcessor captureProcessor) {
            this(new MetadataImageReader(n, n2, n3, n4), captureBundle, captureProcessor);
        }
        
        Builder(final ImageReaderProxy mInputImageReader, final CaptureBundle mCaptureBundle, final CaptureProcessor mCaptureProcessor) {
            this.mPostProcessExecutor = Executors.newSingleThreadExecutor();
            this.mInputImageReader = mInputImageReader;
            this.mCaptureBundle = mCaptureBundle;
            this.mCaptureProcessor = mCaptureProcessor;
            this.mOutputFormat = mInputImageReader.getImageFormat();
        }
        
        ProcessingImageReader build() {
            return new ProcessingImageReader(this);
        }
        
        Builder setOutputFormat(final int mOutputFormat) {
            this.mOutputFormat = mOutputFormat;
            return this;
        }
        
        Builder setPostProcessExecutor(final Executor mPostProcessExecutor) {
            this.mPostProcessExecutor = mPostProcessExecutor;
            return this;
        }
    }
    
    interface OnProcessingErrorCallback
    {
        void notifyProcessingError(final String p0, final Throwable p1);
    }
}

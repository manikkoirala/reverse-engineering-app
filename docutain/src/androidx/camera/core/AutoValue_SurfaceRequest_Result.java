// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core;

import android.view.Surface;

final class AutoValue_SurfaceRequest_Result extends Result
{
    private final int resultCode;
    private final Surface surface;
    
    AutoValue_SurfaceRequest_Result(final int resultCode, final Surface surface) {
        this.resultCode = resultCode;
        if (surface != null) {
            this.surface = surface;
            return;
        }
        throw new NullPointerException("Null surface");
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o == this) {
            return true;
        }
        if (o instanceof Result) {
            final Result result = (Result)o;
            if (this.resultCode != result.getResultCode() || !this.surface.equals(result.getSurface())) {
                b = false;
            }
            return b;
        }
        return false;
    }
    
    @Override
    public int getResultCode() {
        return this.resultCode;
    }
    
    @Override
    public Surface getSurface() {
        return this.surface;
    }
    
    @Override
    public int hashCode() {
        return (this.resultCode ^ 0xF4243) * 1000003 ^ this.surface.hashCode();
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("Result{resultCode=");
        sb.append(this.resultCode);
        sb.append(", surface=");
        sb.append(this.surface);
        sb.append("}");
        return sb.toString();
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core;

public class ImageCaptureException extends Exception
{
    private final int mImageCaptureError;
    
    public ImageCaptureException(final int mImageCaptureError, final String message, final Throwable cause) {
        super(message, cause);
        this.mImageCaptureError = mImageCaptureError;
    }
    
    public int getImageCaptureError() {
        return this.mImageCaptureError;
    }
}

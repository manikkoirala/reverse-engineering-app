// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core;

public class InitializationException extends Exception
{
    public InitializationException(final String message) {
        super(message);
    }
    
    public InitializationException(final String message, final Throwable cause) {
        super(message, cause);
    }
    
    public InitializationException(final Throwable cause) {
        super(cause);
    }
}

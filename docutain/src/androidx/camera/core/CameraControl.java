// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core;

import com.google.common.util.concurrent.ListenableFuture;

public interface CameraControl
{
    ListenableFuture<Void> cancelFocusAndMetering();
    
    ListenableFuture<Void> enableTorch(final boolean p0);
    
    ListenableFuture<Integer> setExposureCompensationIndex(final int p0);
    
    ListenableFuture<Void> setLinearZoom(final float p0);
    
    ListenableFuture<Void> setZoomRatio(final float p0);
    
    ListenableFuture<FocusMeteringResult> startFocusAndMetering(final FocusMeteringAction p0);
    
    public static final class OperationCanceledException extends Exception
    {
        public OperationCanceledException(final String message) {
            super(message);
        }
        
        public OperationCanceledException(final String message, final Throwable cause) {
            super(message, cause);
        }
    }
}

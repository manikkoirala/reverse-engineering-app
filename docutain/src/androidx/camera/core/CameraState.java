// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core;

public abstract class CameraState
{
    public static final int ERROR_CAMERA_DISABLED = 5;
    public static final int ERROR_CAMERA_FATAL_ERROR = 6;
    public static final int ERROR_CAMERA_IN_USE = 2;
    public static final int ERROR_DO_NOT_DISTURB_MODE_ENABLED = 7;
    public static final int ERROR_MAX_CAMERAS_IN_USE = 1;
    public static final int ERROR_OTHER_RECOVERABLE_ERROR = 3;
    public static final int ERROR_STREAM_CONFIG = 4;
    
    public static CameraState create(final Type type) {
        return create(type, null);
    }
    
    public static CameraState create(final Type type, final StateError stateError) {
        return new AutoValue_CameraState(type, stateError);
    }
    
    public abstract StateError getError();
    
    public abstract Type getType();
    
    public enum ErrorType
    {
        private static final ErrorType[] $VALUES;
        
        CRITICAL, 
        RECOVERABLE;
    }
    
    public abstract static class StateError
    {
        public static StateError create(final int n) {
            return create(n, null);
        }
        
        public static StateError create(final int n, final Throwable t) {
            return (StateError)new AutoValue_CameraState_StateError(n, t);
        }
        
        public abstract Throwable getCause();
        
        public abstract int getCode();
        
        public ErrorType getType() {
            final int code = this.getCode();
            if (code != 2 && code != 1 && code != 3) {
                return ErrorType.CRITICAL;
            }
            return ErrorType.RECOVERABLE;
        }
    }
    
    public enum Type
    {
        private static final Type[] $VALUES;
        
        CLOSED, 
        CLOSING, 
        OPEN, 
        OPENING, 
        PENDING_OPEN;
    }
}

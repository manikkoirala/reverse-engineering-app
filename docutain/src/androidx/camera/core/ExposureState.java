// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core;

import android.util.Rational;
import android.util.Range;

public interface ExposureState
{
    int getExposureCompensationIndex();
    
    Range<Integer> getExposureCompensationRange();
    
    Rational getExposureCompensationStep();
    
    boolean isExposureCompensationSupported();
}

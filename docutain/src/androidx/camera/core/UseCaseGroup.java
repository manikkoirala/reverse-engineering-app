// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core;

import androidx.core.util.Preconditions;
import java.util.ArrayList;
import java.util.List;

public final class UseCaseGroup
{
    private final List<CameraEffect> mEffects;
    private final List<UseCase> mUseCases;
    private final ViewPort mViewPort;
    
    UseCaseGroup(final ViewPort mViewPort, final List<UseCase> mUseCases, final List<CameraEffect> mEffects) {
        this.mViewPort = mViewPort;
        this.mUseCases = mUseCases;
        this.mEffects = mEffects;
    }
    
    public List<CameraEffect> getEffects() {
        return this.mEffects;
    }
    
    public List<UseCase> getUseCases() {
        return this.mUseCases;
    }
    
    public ViewPort getViewPort() {
        return this.mViewPort;
    }
    
    public static final class Builder
    {
        private final List<CameraEffect> mEffects;
        private final List<UseCase> mUseCases;
        private ViewPort mViewPort;
        
        public Builder() {
            this.mUseCases = new ArrayList<UseCase>();
            this.mEffects = new ArrayList<CameraEffect>();
        }
        
        public Builder addEffect(final CameraEffect cameraEffect) {
            this.mEffects.add(cameraEffect);
            return this;
        }
        
        public Builder addUseCase(final UseCase useCase) {
            this.mUseCases.add(useCase);
            return this;
        }
        
        public UseCaseGroup build() {
            Preconditions.checkArgument(this.mUseCases.isEmpty() ^ true, (Object)"UseCase must not be empty.");
            return new UseCaseGroup(this.mViewPort, this.mUseCases, this.mEffects);
        }
        
        public Builder setViewPort(final ViewPort mViewPort) {
            this.mViewPort = mViewPort;
            return this;
        }
    }
}

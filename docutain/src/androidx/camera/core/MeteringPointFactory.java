// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core;

import android.graphics.PointF;
import android.util.Rational;

public abstract class MeteringPointFactory
{
    private Rational mSurfaceAspectRatio;
    
    public MeteringPointFactory() {
        this(null);
    }
    
    public MeteringPointFactory(final Rational mSurfaceAspectRatio) {
        this.mSurfaceAspectRatio = mSurfaceAspectRatio;
    }
    
    public static float getDefaultPointSize() {
        return 0.15f;
    }
    
    protected abstract PointF convertPoint(final float p0, final float p1);
    
    public final MeteringPoint createPoint(final float n, final float n2) {
        return this.createPoint(n, n2, getDefaultPointSize());
    }
    
    public final MeteringPoint createPoint(final float n, final float n2, final float n3) {
        final PointF convertPoint = this.convertPoint(n, n2);
        return new MeteringPoint(convertPoint.x, convertPoint.y, n3, this.mSurfaceAspectRatio);
    }
}

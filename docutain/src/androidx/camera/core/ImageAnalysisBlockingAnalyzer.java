// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core;

import androidx.camera.core.impl.utils.futures.Futures;
import androidx.camera.core.impl.utils.executor.CameraXExecutors;
import androidx.camera.core.impl.utils.futures.FutureCallback;
import androidx.camera.core.impl.ImageReaderProxy;

final class ImageAnalysisBlockingAnalyzer extends ImageAnalysisAbstractAnalyzer
{
    @Override
    ImageProxy acquireImage(final ImageReaderProxy imageReaderProxy) {
        return imageReaderProxy.acquireNextImage();
    }
    
    @Override
    void clearCache() {
    }
    
    @Override
    void onValidImageAvailable(final ImageProxy imageProxy) {
        Futures.addCallback(this.analyzeImage(imageProxy), new FutureCallback<Void>(this, imageProxy) {
            final ImageAnalysisBlockingAnalyzer this$0;
            final ImageProxy val$imageProxy;
            
            @Override
            public void onFailure(final Throwable t) {
                this.val$imageProxy.close();
            }
            
            @Override
            public void onSuccess(final Void void1) {
            }
        }, CameraXExecutors.directExecutor());
    }
}

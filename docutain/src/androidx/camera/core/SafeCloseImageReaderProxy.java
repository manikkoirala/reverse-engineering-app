// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core;

import java.util.concurrent.Executor;
import android.view.Surface;
import androidx.camera.core.impl.ImageReaderProxy;

public class SafeCloseImageReaderProxy implements ImageReaderProxy
{
    private ForwardingImageProxy.OnImageCloseListener mForwardingImageCloseListener;
    private final ForwardingImageProxy.OnImageCloseListener mImageCloseListener;
    private final ImageReaderProxy mImageReaderProxy;
    private boolean mIsClosed;
    private final Object mLock;
    private int mOutstandingImages;
    private final Surface mSurface;
    
    public SafeCloseImageReaderProxy(final ImageReaderProxy mImageReaderProxy) {
        this.mLock = new Object();
        this.mOutstandingImages = 0;
        this.mIsClosed = false;
        this.mImageCloseListener = new SafeCloseImageReaderProxy$$ExternalSyntheticLambda0(this);
        this.mImageReaderProxy = mImageReaderProxy;
        this.mSurface = mImageReaderProxy.getSurface();
    }
    
    private ImageProxy wrapImageProxy(final ImageProxy imageProxy) {
        if (imageProxy != null) {
            ++this.mOutstandingImages;
            final SingleCloseImageProxy singleCloseImageProxy = new SingleCloseImageProxy(imageProxy);
            singleCloseImageProxy.addOnImageCloseListener(this.mImageCloseListener);
            return singleCloseImageProxy;
        }
        return null;
    }
    
    @Override
    public ImageProxy acquireLatestImage() {
        synchronized (this.mLock) {
            return this.wrapImageProxy(this.mImageReaderProxy.acquireLatestImage());
        }
    }
    
    @Override
    public ImageProxy acquireNextImage() {
        synchronized (this.mLock) {
            return this.wrapImageProxy(this.mImageReaderProxy.acquireNextImage());
        }
    }
    
    @Override
    public void clearOnImageAvailableListener() {
        synchronized (this.mLock) {
            this.mImageReaderProxy.clearOnImageAvailableListener();
        }
    }
    
    @Override
    public void close() {
        synchronized (this.mLock) {
            final Surface mSurface = this.mSurface;
            if (mSurface != null) {
                mSurface.release();
            }
            this.mImageReaderProxy.close();
        }
    }
    
    public int getCapacity() {
        synchronized (this.mLock) {
            final int maxImages = this.mImageReaderProxy.getMaxImages();
            final int mOutstandingImages = this.mOutstandingImages;
            monitorexit(this.mLock);
            return maxImages - mOutstandingImages;
        }
    }
    
    @Override
    public int getHeight() {
        synchronized (this.mLock) {
            return this.mImageReaderProxy.getHeight();
        }
    }
    
    @Override
    public int getImageFormat() {
        synchronized (this.mLock) {
            return this.mImageReaderProxy.getImageFormat();
        }
    }
    
    @Override
    public int getMaxImages() {
        synchronized (this.mLock) {
            return this.mImageReaderProxy.getMaxImages();
        }
    }
    
    @Override
    public Surface getSurface() {
        synchronized (this.mLock) {
            return this.mImageReaderProxy.getSurface();
        }
    }
    
    @Override
    public int getWidth() {
        synchronized (this.mLock) {
            return this.mImageReaderProxy.getWidth();
        }
    }
    
    public void safeClose() {
        synchronized (this.mLock) {
            this.mIsClosed = true;
            this.mImageReaderProxy.clearOnImageAvailableListener();
            if (this.mOutstandingImages == 0) {
                this.close();
            }
        }
    }
    
    @Override
    public void setOnImageAvailableListener(final OnImageAvailableListener onImageAvailableListener, final Executor executor) {
        synchronized (this.mLock) {
            this.mImageReaderProxy.setOnImageAvailableListener((OnImageAvailableListener)new SafeCloseImageReaderProxy$$ExternalSyntheticLambda1(this, onImageAvailableListener), executor);
        }
    }
    
    public void setOnImageCloseListener(final ForwardingImageProxy.OnImageCloseListener mForwardingImageCloseListener) {
        synchronized (this.mLock) {
            this.mForwardingImageCloseListener = mForwardingImageCloseListener;
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core;

import android.util.Size;
import java.nio.ByteBuffer;

final class ImageProxyDownsampler
{
    private ImageProxyDownsampler() {
    }
    
    private static ImageProxy.PlaneProxy createPlaneProxy(final int n, final int n2, final byte[] array) {
        return new ImageProxy.PlaneProxy(array, n, n2) {
            final ByteBuffer mBuffer = ByteBuffer.wrap(array);
            final byte[] val$data;
            final int val$pixelStride;
            final int val$rowStride;
            
            @Override
            public ByteBuffer getBuffer() {
                return this.mBuffer;
            }
            
            @Override
            public int getPixelStride() {
                return this.val$pixelStride;
            }
            
            @Override
            public int getRowStride() {
                return this.val$rowStride;
            }
        };
    }
    
    static ForwardingImageProxy downsample(final ImageProxy imageProxy, final int n, final int n2, final DownsamplingMethod downsamplingMethod) {
        if (imageProxy.getFormat() != 35) {
            throw new UnsupportedOperationException("Only YUV_420_888 format is currently supported.");
        }
        if (imageProxy.getWidth() < n || imageProxy.getHeight() < n2) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Downsampled dimension ");
            sb.append(new Size(n, n2));
            sb.append(" is not <= original dimension ");
            sb.append(new Size(imageProxy.getWidth(), imageProxy.getHeight()));
            sb.append(".");
            throw new IllegalArgumentException(sb.toString());
        }
        if (imageProxy.getWidth() == n && imageProxy.getHeight() == n2) {
            return new ForwardingImageProxyImpl(imageProxy, imageProxy.getPlanes(), n, n2);
        }
        final int[] array = new int[3];
        final int width = imageProxy.getWidth();
        int i = 0;
        array[0] = width;
        array[1] = imageProxy.getWidth() / 2;
        array[2] = imageProxy.getWidth() / 2;
        final int[] array2 = { imageProxy.getHeight(), imageProxy.getHeight() / 2, imageProxy.getHeight() / 2 };
        final int[] array3 = { n, 0, 0 };
        array3[2] = (array3[1] = n / 2);
        final int[] array4 = { n2, 0, 0 };
        array4[2] = (array4[1] = n2 / 2);
        final ImageProxy.PlaneProxy[] array5 = new ImageProxy.PlaneProxy[3];
        while (i < 3) {
            final ImageProxy.PlaneProxy planeProxy = imageProxy.getPlanes()[i];
            final ByteBuffer buffer = planeProxy.getBuffer();
            final byte[] array6 = new byte[array3[i] * array4[i]];
            final int n3 = ImageProxyDownsampler$2.$SwitchMap$androidx$camera$core$ImageProxyDownsampler$DownsamplingMethod[downsamplingMethod.ordinal()];
            if (n3 != 1) {
                if (n3 == 2) {
                    resizeAveraging(buffer, array[i], planeProxy.getPixelStride(), planeProxy.getRowStride(), array2[i], array6, array3[i], array4[i]);
                }
            }
            else {
                resizeNearestNeighbor(buffer, array[i], planeProxy.getPixelStride(), planeProxy.getRowStride(), array2[i], array6, array3[i], array4[i]);
            }
            array5[i] = createPlaneProxy(array3[i], 1, array6);
            ++i;
        }
        return new ForwardingImageProxyImpl(imageProxy, array5, n, n2);
    }
    
    private static void resizeAveraging(final ByteBuffer byteBuffer, int i, final int n, final int n2, final int n3, final byte[] array, final int n4, final int n5) {
        final float n6 = i / (float)n4;
        final float n7 = n3 / (float)n5;
        final byte[] dst = new byte[n2];
        final byte[] dst2 = new byte[n2];
        final int[] array2 = new int[n4];
        for (i = 0; i < n4; ++i) {
            array2[i] = (int)(i * n6) * n;
        }
        synchronized (byteBuffer) {
            byteBuffer.rewind();
            int a;
            int n8;
            int min;
            int min2;
            int j;
            int n9;
            for (i = 0; i < n5; ++i) {
                a = (int)(i * n7);
                n8 = n3 - 1;
                min = Math.min(a, n8);
                min2 = Math.min(a + 1, n8);
                byteBuffer.position(min * n2);
                byteBuffer.get(dst, 0, Math.min(n2, byteBuffer.remaining()));
                byteBuffer.position(min2 * n2);
                byteBuffer.get(dst2, 0, Math.min(n2, byteBuffer.remaining()));
                for (j = 0; j < n4; ++j) {
                    n9 = array2[j];
                    array[i * n4 + j] = (byte)(((dst[n9] & 0xFF) + (dst[n9 + n] & 0xFF) + (dst2[n9] & 0xFF) + (dst2[n9 + n] & 0xFF)) / 4 & 0xFF);
                }
            }
        }
    }
    
    private static void resizeNearestNeighbor(final ByteBuffer byteBuffer, int i, int j, final int a, final int n, final byte[] array, final int n2, final int n3) {
        final float n4 = i / (float)n2;
        final float n5 = n / (float)n3;
        final byte[] dst = new byte[a];
        final int[] array2 = new int[n2];
        for (i = 0; i < n2; ++i) {
            array2[i] = (int)(i * n4) * j;
        }
        synchronized (byteBuffer) {
            byteBuffer.rewind();
            for (i = 0; i < n3; ++i) {
                byteBuffer.position(Math.min((int)(i * n5), n - 1) * a);
                byteBuffer.get(dst, 0, Math.min(a, byteBuffer.remaining()));
                for (j = 0; j < n2; ++j) {
                    array[i * n2 + j] = dst[array2[j]];
                }
            }
        }
    }
    
    enum DownsamplingMethod
    {
        private static final DownsamplingMethod[] $VALUES;
        
        AVERAGING, 
        NEAREST_NEIGHBOR;
    }
    
    private static final class ForwardingImageProxyImpl extends ForwardingImageProxy
    {
        private final int mDownsampledHeight;
        private final PlaneProxy[] mDownsampledPlanes;
        private final int mDownsampledWidth;
        
        ForwardingImageProxyImpl(final ImageProxy imageProxy, final PlaneProxy[] mDownsampledPlanes, final int mDownsampledWidth, final int mDownsampledHeight) {
            super(imageProxy);
            this.mDownsampledPlanes = mDownsampledPlanes;
            this.mDownsampledWidth = mDownsampledWidth;
            this.mDownsampledHeight = mDownsampledHeight;
        }
        
        @Override
        public int getHeight() {
            return this.mDownsampledHeight;
        }
        
        @Override
        public PlaneProxy[] getPlanes() {
            return this.mDownsampledPlanes;
        }
        
        @Override
        public int getWidth() {
            return this.mDownsampledWidth;
        }
    }
}

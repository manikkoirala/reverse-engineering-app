// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core;

import java.util.List;

public interface ImageProcessor
{
    Response process(final Request p0);
    
    public interface Request
    {
        List<ImageProxy> getInputImages();
        
        int getOutputFormat();
    }
    
    public interface Response
    {
        ImageProxy getOutputImage();
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core;

import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;

public class CameraUnavailableException extends Exception
{
    public static final int CAMERA_DISABLED = 1;
    public static final int CAMERA_DISCONNECTED = 2;
    public static final int CAMERA_ERROR = 3;
    public static final int CAMERA_IN_USE = 4;
    public static final int CAMERA_MAX_IN_USE = 5;
    public static final int CAMERA_UNAVAILABLE_DO_NOT_DISTURB = 6;
    public static final int CAMERA_UNKNOWN_ERROR = 0;
    private final int mReason;
    
    public CameraUnavailableException(final int mReason) {
        this.mReason = mReason;
    }
    
    public CameraUnavailableException(final int mReason, final String message) {
        super(message);
        this.mReason = mReason;
    }
    
    public CameraUnavailableException(final int mReason, final String message, final Throwable cause) {
        super(message, cause);
        this.mReason = mReason;
    }
    
    public CameraUnavailableException(final int mReason, final Throwable cause) {
        super(cause);
        this.mReason = mReason;
    }
    
    public int getReason() {
        return this.mReason;
    }
    
    @Retention(RetentionPolicy.SOURCE)
    public @interface Reason {
    }
}

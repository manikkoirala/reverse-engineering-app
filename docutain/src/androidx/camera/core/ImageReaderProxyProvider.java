// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core;

import androidx.camera.core.impl.ImageReaderProxy;

public interface ImageReaderProxyProvider
{
    ImageReaderProxy newInstance(final int p0, final int p1, final int p2, final int p3, final long p4);
}

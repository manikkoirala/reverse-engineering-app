// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core;

import java.util.List;
import androidx.camera.core.impl.Identifier;

public interface CameraFilter
{
    public static final Identifier DEFAULT_ID = Identifier.create(new Object());
    
    List<CameraInfo> filter(final List<CameraInfo> p0);
    
    Identifier getIdentifier();
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core;

import androidx.camera.core.impl.utils.ExifData;
import androidx.camera.core.impl.TagBundle;
import android.graphics.Matrix;

public interface ImageInfo
{
    int getRotationDegrees();
    
    Matrix getSensorToBufferTransformMatrix();
    
    TagBundle getTagBundle();
    
    long getTimestamp();
    
    void populateExifData(final ExifData.Builder p0);
}

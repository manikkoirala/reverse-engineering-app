// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core;

import androidx.camera.core.impl.DeferrableSurface;
import androidx.camera.core.internal.utils.UseCaseConfigUtil;
import java.util.Iterator;
import androidx.camera.core.internal.TargetConfig;
import androidx.camera.core.impl.MutableOptionsBundle;
import androidx.camera.core.impl.CameraInfoInternal;
import androidx.camera.core.impl.Config;
import java.util.Objects;
import androidx.camera.core.impl.UseCaseConfigFactory;
import androidx.core.util.Preconditions;
import androidx.camera.core.impl.CameraControlInternal;
import androidx.camera.core.impl.ImageOutputConfig;
import java.util.HashSet;
import android.graphics.Rect;
import java.util.Set;
import android.graphics.Matrix;
import androidx.camera.core.impl.UseCaseConfig;
import androidx.camera.core.impl.CameraInternal;
import androidx.camera.core.impl.SessionConfig;
import android.util.Size;

public abstract class UseCase
{
    private Size mAttachedResolution;
    private SessionConfig mAttachedSessionConfig;
    private CameraInternal mCamera;
    private UseCaseConfig<?> mCameraConfig;
    private final Object mCameraLock;
    private UseCaseConfig<?> mCurrentConfig;
    private UseCaseConfig<?> mExtendedConfig;
    private Matrix mSensorToBufferTransformMatrix;
    private State mState;
    private final Set<StateChangeCallback> mStateChangeCallbacks;
    private UseCaseConfig<?> mUseCaseConfig;
    private Rect mViewPortCropRect;
    
    protected UseCase(final UseCaseConfig<?> useCaseConfig) {
        this.mStateChangeCallbacks = new HashSet<StateChangeCallback>();
        this.mCameraLock = new Object();
        this.mState = State.INACTIVE;
        this.mSensorToBufferTransformMatrix = new Matrix();
        this.mAttachedSessionConfig = SessionConfig.defaultEmptySessionConfig();
        this.mUseCaseConfig = useCaseConfig;
        this.mCurrentConfig = useCaseConfig;
    }
    
    private void addStateChangeCallback(final StateChangeCallback stateChangeCallback) {
        this.mStateChangeCallbacks.add(stateChangeCallback);
    }
    
    private void removeStateChangeCallback(final StateChangeCallback stateChangeCallback) {
        this.mStateChangeCallbacks.remove(stateChangeCallback);
    }
    
    protected int getAppTargetRotation() {
        return ((ImageOutputConfig)this.mCurrentConfig).getAppTargetRotation(-1);
    }
    
    public Size getAttachedSurfaceResolution() {
        return this.mAttachedResolution;
    }
    
    public CameraInternal getCamera() {
        synchronized (this.mCameraLock) {
            return this.mCamera;
        }
    }
    
    protected CameraControlInternal getCameraControl() {
        synchronized (this.mCameraLock) {
            final CameraInternal mCamera = this.mCamera;
            if (mCamera == null) {
                return CameraControlInternal.DEFAULT_EMPTY_INSTANCE;
            }
            return mCamera.getCameraControlInternal();
        }
    }
    
    protected String getCameraId() {
        final CameraInternal camera = this.getCamera();
        final StringBuilder sb = new StringBuilder();
        sb.append("No camera attached to use case: ");
        sb.append(this);
        return Preconditions.checkNotNull(camera, sb.toString()).getCameraInfoInternal().getCameraId();
    }
    
    public UseCaseConfig<?> getCurrentConfig() {
        return this.mCurrentConfig;
    }
    
    public abstract UseCaseConfig<?> getDefaultConfig(final boolean p0, final UseCaseConfigFactory p1);
    
    public int getImageFormat() {
        return this.mCurrentConfig.getInputFormat();
    }
    
    public String getName() {
        final UseCaseConfig<?> mCurrentConfig = this.mCurrentConfig;
        final StringBuilder sb = new StringBuilder();
        sb.append("<UnknownUseCase-");
        sb.append(this.hashCode());
        sb.append(">");
        return Objects.requireNonNull(mCurrentConfig.getTargetName(sb.toString()));
    }
    
    protected int getRelativeRotation(final CameraInternal cameraInternal) {
        return cameraInternal.getCameraInfoInternal().getSensorRotationDegrees(this.getTargetRotationInternal());
    }
    
    public ResolutionInfo getResolutionInfo() {
        return this.getResolutionInfoInternal();
    }
    
    protected ResolutionInfo getResolutionInfoInternal() {
        final CameraInternal camera = this.getCamera();
        final Size attachedSurfaceResolution = this.getAttachedSurfaceResolution();
        if (camera != null && attachedSurfaceResolution != null) {
            Rect viewPortCropRect;
            if ((viewPortCropRect = this.getViewPortCropRect()) == null) {
                viewPortCropRect = new Rect(0, 0, attachedSurfaceResolution.getWidth(), attachedSurfaceResolution.getHeight());
            }
            return ResolutionInfo.create(attachedSurfaceResolution, viewPortCropRect, this.getRelativeRotation(camera));
        }
        return null;
    }
    
    public Matrix getSensorToBufferTransformMatrix() {
        return this.mSensorToBufferTransformMatrix;
    }
    
    public SessionConfig getSessionConfig() {
        return this.mAttachedSessionConfig;
    }
    
    protected int getTargetRotationInternal() {
        return ((ImageOutputConfig)this.mCurrentConfig).getTargetRotation(0);
    }
    
    public abstract UseCaseConfig.Builder<?, ?, ?> getUseCaseConfigBuilder(final Config p0);
    
    public Rect getViewPortCropRect() {
        return this.mViewPortCropRect;
    }
    
    protected boolean isCurrentCamera(final String a) {
        return this.getCamera() != null && Objects.equals(a, this.getCameraId());
    }
    
    public UseCaseConfig<?> mergeConfigs(final CameraInfoInternal cameraInfoInternal, final UseCaseConfig<?> useCaseConfig, final UseCaseConfig<?> useCaseConfig2) {
        MutableOptionsBundle mutableOptionsBundle;
        if (useCaseConfig2 != null) {
            mutableOptionsBundle = MutableOptionsBundle.from(useCaseConfig2);
            mutableOptionsBundle.removeOption(TargetConfig.OPTION_TARGET_NAME);
        }
        else {
            mutableOptionsBundle = MutableOptionsBundle.create();
        }
        for (final Config.Option option : this.mUseCaseConfig.listOptions()) {
            mutableOptionsBundle.insertOption(option, this.mUseCaseConfig.getOptionPriority(option), (Object)this.mUseCaseConfig.retrieveOption((Config.Option<ValueT>)option));
        }
        if (useCaseConfig != null) {
            for (final Config.Option option2 : useCaseConfig.listOptions()) {
                if (option2.getId().equals(TargetConfig.OPTION_TARGET_NAME.getId())) {
                    continue;
                }
                mutableOptionsBundle.insertOption(option2, useCaseConfig.getOptionPriority(option2), (ValueT)useCaseConfig.retrieveOption((Config.Option<ValueT>)option2));
            }
        }
        if (mutableOptionsBundle.containsOption(ImageOutputConfig.OPTION_TARGET_RESOLUTION) && mutableOptionsBundle.containsOption(ImageOutputConfig.OPTION_TARGET_ASPECT_RATIO)) {
            mutableOptionsBundle.removeOption(ImageOutputConfig.OPTION_TARGET_ASPECT_RATIO);
        }
        return this.onMergeConfig(cameraInfoInternal, this.getUseCaseConfigBuilder(mutableOptionsBundle));
    }
    
    protected final void notifyActive() {
        this.mState = State.ACTIVE;
        this.notifyState();
    }
    
    protected final void notifyInactive() {
        this.mState = State.INACTIVE;
        this.notifyState();
    }
    
    protected final void notifyReset() {
        final Iterator<StateChangeCallback> iterator = this.mStateChangeCallbacks.iterator();
        while (iterator.hasNext()) {
            iterator.next().onUseCaseReset(this);
        }
    }
    
    public final void notifyState() {
        final int n = UseCase$1.$SwitchMap$androidx$camera$core$UseCase$State[this.mState.ordinal()];
        if (n != 1) {
            if (n == 2) {
                final Iterator<StateChangeCallback> iterator = this.mStateChangeCallbacks.iterator();
                while (iterator.hasNext()) {
                    iterator.next().onUseCaseActive(this);
                }
            }
        }
        else {
            final Iterator<StateChangeCallback> iterator2 = this.mStateChangeCallbacks.iterator();
            while (iterator2.hasNext()) {
                iterator2.next().onUseCaseInactive(this);
            }
        }
    }
    
    protected final void notifyUpdated() {
        final Iterator<StateChangeCallback> iterator = this.mStateChangeCallbacks.iterator();
        while (iterator.hasNext()) {
            iterator.next().onUseCaseUpdated(this);
        }
    }
    
    public void onAttach(final CameraInternal mCamera, final UseCaseConfig<?> mExtendedConfig, final UseCaseConfig<?> mCameraConfig) {
        synchronized (this.mCameraLock) {
            this.addStateChangeCallback((StateChangeCallback)(this.mCamera = mCamera));
            monitorexit(this.mCameraLock);
            this.mExtendedConfig = mExtendedConfig;
            this.mCameraConfig = mCameraConfig;
            final UseCaseConfig<?> mergeConfigs = this.mergeConfigs(mCamera.getCameraInfoInternal(), this.mExtendedConfig, this.mCameraConfig);
            this.mCurrentConfig = mergeConfigs;
            final EventCallback useCaseEventCallback = mergeConfigs.getUseCaseEventCallback(null);
            if (useCaseEventCallback != null) {
                useCaseEventCallback.onAttach(mCamera.getCameraInfoInternal());
            }
            this.onAttached();
        }
    }
    
    public void onAttached() {
    }
    
    protected void onCameraControlReady() {
    }
    
    public void onDetach(final CameraInternal cameraInternal) {
        this.onDetached();
        final EventCallback useCaseEventCallback = this.mCurrentConfig.getUseCaseEventCallback(null);
        if (useCaseEventCallback != null) {
            useCaseEventCallback.onDetach();
        }
        synchronized (this.mCameraLock) {
            Preconditions.checkArgument(cameraInternal == this.mCamera);
            this.removeStateChangeCallback((StateChangeCallback)this.mCamera);
            this.mCamera = null;
            monitorexit(this.mCameraLock);
            this.mAttachedResolution = null;
            this.mViewPortCropRect = null;
            this.mCurrentConfig = this.mUseCaseConfig;
            this.mExtendedConfig = null;
            this.mCameraConfig = null;
        }
    }
    
    public void onDetached() {
    }
    
    protected UseCaseConfig<?> onMergeConfig(final CameraInfoInternal cameraInfoInternal, final UseCaseConfig.Builder<?, ?, ?> builder) {
        return (UseCaseConfig<?>)builder.getUseCaseConfig();
    }
    
    public void onStateAttached() {
        this.onCameraControlReady();
    }
    
    public void onStateDetached() {
    }
    
    protected abstract Size onSuggestedResolutionUpdated(final Size p0);
    
    public void setSensorToBufferTransformMatrix(final Matrix matrix) {
        this.mSensorToBufferTransformMatrix = new Matrix(matrix);
    }
    
    protected boolean setTargetRotationInternal(final int n) {
        final int targetRotation = ((ImageOutputConfig)this.getCurrentConfig()).getTargetRotation(-1);
        if (targetRotation != -1 && targetRotation == n) {
            return false;
        }
        final UseCaseConfig.Builder<?, ?, ?> useCaseConfigBuilder = this.getUseCaseConfigBuilder(this.mUseCaseConfig);
        UseCaseConfigUtil.updateTargetRotationAndRelatedConfigs(useCaseConfigBuilder, n);
        this.mUseCaseConfig = (UseCaseConfig<?>)useCaseConfigBuilder.getUseCaseConfig();
        final CameraInternal camera = this.getCamera();
        if (camera == null) {
            this.mCurrentConfig = this.mUseCaseConfig;
        }
        else {
            this.mCurrentConfig = this.mergeConfigs(camera.getCameraInfoInternal(), this.mExtendedConfig, this.mCameraConfig);
        }
        return true;
    }
    
    public void setViewPortCropRect(final Rect mViewPortCropRect) {
        this.mViewPortCropRect = mViewPortCropRect;
    }
    
    protected void updateSessionConfig(final SessionConfig mAttachedSessionConfig) {
        this.mAttachedSessionConfig = mAttachedSessionConfig;
        for (final DeferrableSurface deferrableSurface : mAttachedSessionConfig.getSurfaces()) {
            if (deferrableSurface.getContainerClass() == null) {
                deferrableSurface.setContainerClass(this.getClass());
            }
        }
    }
    
    public void updateSuggestedResolution(final Size size) {
        this.mAttachedResolution = this.onSuggestedResolutionUpdated(size);
    }
    
    public interface EventCallback
    {
        void onAttach(final CameraInfo p0);
        
        void onDetach();
    }
    
    enum State
    {
        private static final State[] $VALUES;
        
        ACTIVE, 
        INACTIVE;
    }
    
    public interface StateChangeCallback
    {
        void onUseCaseActive(final UseCase p0);
        
        void onUseCaseInactive(final UseCase p0);
        
        void onUseCaseReset(final UseCase p0);
        
        void onUseCaseUpdated(final UseCase p0);
    }
}

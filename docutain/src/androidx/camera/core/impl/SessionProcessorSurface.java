// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl;

import androidx.camera.core.impl.utils.futures.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import android.view.Surface;

public final class SessionProcessorSurface extends DeferrableSurface
{
    private final int mOutputConfigId;
    private final Surface mSurface;
    
    public SessionProcessorSurface(final Surface mSurface, final int mOutputConfigId) {
        this.mSurface = mSurface;
        this.mOutputConfigId = mOutputConfigId;
    }
    
    public int getOutputConfigId() {
        return this.mOutputConfigId;
    }
    
    public ListenableFuture<Surface> provideSurface() {
        return Futures.immediateFuture(this.mSurface);
    }
}

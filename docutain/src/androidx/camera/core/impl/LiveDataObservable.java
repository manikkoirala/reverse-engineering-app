// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl;

import java.util.concurrent.atomic.AtomicBoolean;
import android.os.SystemClock;
import androidx.core.util.Preconditions;
import androidx.lifecycle.Observer;
import androidx.lifecycle.LiveData;
import androidx.concurrent.futures.CallbackToFutureAdapter;
import com.google.common.util.concurrent.ListenableFuture;
import androidx.camera.core.impl.utils.executor.CameraXExecutors;
import java.util.concurrent.Executor;
import java.util.HashMap;
import java.util.Map;
import androidx.lifecycle.MutableLiveData;

public final class LiveDataObservable<T> implements Observable<T>
{
    final MutableLiveData<Result<T>> mLiveData;
    private final Map<Observer<? super T>, LiveDataObserverAdapter<T>> mObservers;
    
    public LiveDataObservable() {
        this.mLiveData = new MutableLiveData<Result<T>>();
        this.mObservers = new HashMap<Observer<? super T>, LiveDataObserverAdapter<T>>();
    }
    
    @Override
    public void addObserver(final Executor executor, final Observer<? super T> observer) {
        synchronized (this.mObservers) {
            final LiveDataObserverAdapter liveDataObserverAdapter = this.mObservers.get(observer);
            if (liveDataObserverAdapter != null) {
                liveDataObserverAdapter.disable();
            }
            final LiveDataObserverAdapter liveDataObserverAdapter2 = new LiveDataObserverAdapter(executor, (Observer<? super Object>)observer);
            this.mObservers.put(observer, (LiveDataObserverAdapter<T>)liveDataObserverAdapter2);
            CameraXExecutors.mainThreadExecutor().execute(new LiveDataObservable$$ExternalSyntheticLambda3(this, (LiveDataObserverAdapter)liveDataObserverAdapter, liveDataObserverAdapter2));
        }
    }
    
    @Override
    public ListenableFuture<T> fetchData() {
        return CallbackToFutureAdapter.getFuture((CallbackToFutureAdapter.Resolver<T>)new LiveDataObservable$$ExternalSyntheticLambda0(this));
    }
    
    public LiveData<Result<T>> getLiveData() {
        return this.mLiveData;
    }
    
    public void postError(final Throwable t) {
        this.mLiveData.postValue(Result.fromError(t));
    }
    
    public void postValue(final T t) {
        this.mLiveData.postValue(Result.fromValue(t));
    }
    
    @Override
    public void removeObserver(final Observer<? super T> observer) {
        synchronized (this.mObservers) {
            final LiveDataObserverAdapter liveDataObserverAdapter = this.mObservers.remove(observer);
            if (liveDataObserverAdapter != null) {
                liveDataObserverAdapter.disable();
                CameraXExecutors.mainThreadExecutor().execute(new LiveDataObservable$$ExternalSyntheticLambda2(this, (LiveDataObserverAdapter)liveDataObserverAdapter));
            }
        }
    }
    
    private static final class LiveDataObserverAdapter<T> implements Observer<Result<T>>
    {
        final AtomicBoolean mActive;
        final Executor mExecutor;
        final Observable.Observer<? super T> mObserver;
        
        LiveDataObserverAdapter(final Executor mExecutor, final Observable.Observer<? super T> mObserver) {
            this.mActive = new AtomicBoolean(true);
            this.mExecutor = mExecutor;
            this.mObserver = mObserver;
        }
        
        void disable() {
            this.mActive.set(false);
        }
        
        @Override
        public void onChanged(final Result<T> result) {
            this.mExecutor.execute(new LiveDataObservable$LiveDataObserverAdapter$$ExternalSyntheticLambda0(this, result));
        }
    }
    
    public static final class Result<T>
    {
        private final Throwable mError;
        private final T mValue;
        
        private Result(final T mValue, final Throwable mError) {
            this.mValue = mValue;
            this.mError = mError;
        }
        
        static <T> Result<T> fromError(final Throwable t) {
            return new Result<T>(null, Preconditions.checkNotNull(t));
        }
        
        static <T> Result<T> fromValue(final T t) {
            return new Result<T>(t, null);
        }
        
        public boolean completedSuccessfully() {
            return this.mError == null;
        }
        
        public Throwable getError() {
            return this.mError;
        }
        
        public T getValue() {
            if (this.completedSuccessfully()) {
                return this.mValue;
            }
            throw new IllegalStateException("Result contains an error. Does not contain a value.");
        }
        
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder();
            sb.append("[Result: <");
            String str;
            if (this.completedSuccessfully()) {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("Value: ");
                sb2.append(this.mValue);
                str = sb2.toString();
            }
            else {
                final StringBuilder sb3 = new StringBuilder();
                sb3.append("Error: ");
                sb3.append(this.mError);
                str = sb3.toString();
            }
            sb.append(str);
            sb.append(">]");
            return sb.toString();
        }
    }
}

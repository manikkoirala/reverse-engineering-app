// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl;

import androidx.camera.core.InitializationException;
import java.util.Set;
import android.content.Context;
import android.util.Size;
import java.util.Map;
import java.util.List;

public interface CameraDeviceSurfaceManager
{
    boolean checkSupported(final String p0, final List<SurfaceConfig> p1);
    
    Map<UseCaseConfig<?>, Size> getSuggestedResolutions(final String p0, final List<AttachedSurfaceInfo> p1, final List<UseCaseConfig<?>> p2);
    
    SurfaceConfig transformSurfaceConfig(final String p0, final int p1, final Size p2);
    
    public interface Provider
    {
        CameraDeviceSurfaceManager newInstance(final Context p0, final Object p1, final Set<String> p2) throws InitializationException;
    }
}

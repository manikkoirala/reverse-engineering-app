// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl;

import java.util.List;

final class AutoValue_SessionConfig_OutputConfig extends OutputConfig
{
    private final String physicalCameraId;
    private final List<DeferrableSurface> sharedSurfaces;
    private final DeferrableSurface surface;
    private final int surfaceGroupId;
    
    private AutoValue_SessionConfig_OutputConfig(final DeferrableSurface surface, final List<DeferrableSurface> sharedSurfaces, final String physicalCameraId, final int surfaceGroupId) {
        this.surface = surface;
        this.sharedSurfaces = sharedSurfaces;
        this.physicalCameraId = physicalCameraId;
        this.surfaceGroupId = surfaceGroupId;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o == this) {
            return true;
        }
        if (o instanceof OutputConfig) {
            final OutputConfig outputConfig = (OutputConfig)o;
            if (this.surface.equals(outputConfig.getSurface()) && this.sharedSurfaces.equals(outputConfig.getSharedSurfaces())) {
                final String physicalCameraId = this.physicalCameraId;
                if (physicalCameraId == null) {
                    if (outputConfig.getPhysicalCameraId() != null) {
                        return false;
                    }
                }
                else if (!physicalCameraId.equals(outputConfig.getPhysicalCameraId())) {
                    return false;
                }
                if (this.surfaceGroupId == outputConfig.getSurfaceGroupId()) {
                    return b;
                }
            }
            b = false;
            return b;
        }
        return false;
    }
    
    @Override
    public String getPhysicalCameraId() {
        return this.physicalCameraId;
    }
    
    @Override
    public List<DeferrableSurface> getSharedSurfaces() {
        return this.sharedSurfaces;
    }
    
    @Override
    public DeferrableSurface getSurface() {
        return this.surface;
    }
    
    @Override
    public int getSurfaceGroupId() {
        return this.surfaceGroupId;
    }
    
    @Override
    public int hashCode() {
        final int hashCode = this.surface.hashCode();
        final int hashCode2 = this.sharedSurfaces.hashCode();
        final String physicalCameraId = this.physicalCameraId;
        int hashCode3;
        if (physicalCameraId == null) {
            hashCode3 = 0;
        }
        else {
            hashCode3 = physicalCameraId.hashCode();
        }
        return (((hashCode ^ 0xF4243) * 1000003 ^ hashCode2) * 1000003 ^ hashCode3) * 1000003 ^ this.surfaceGroupId;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("OutputConfig{surface=");
        sb.append(this.surface);
        sb.append(", sharedSurfaces=");
        sb.append(this.sharedSurfaces);
        sb.append(", physicalCameraId=");
        sb.append(this.physicalCameraId);
        sb.append(", surfaceGroupId=");
        sb.append(this.surfaceGroupId);
        sb.append("}");
        return sb.toString();
    }
    
    static final class Builder extends OutputConfig.Builder
    {
        private String physicalCameraId;
        private List<DeferrableSurface> sharedSurfaces;
        private DeferrableSurface surface;
        private Integer surfaceGroupId;
        
        @Override
        public OutputConfig build() {
            final DeferrableSurface surface = this.surface;
            String string = "";
            if (surface == null) {
                final StringBuilder sb = new StringBuilder();
                sb.append("");
                sb.append(" surface");
                string = sb.toString();
            }
            String string2 = string;
            if (this.sharedSurfaces == null) {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append(string);
                sb2.append(" sharedSurfaces");
                string2 = sb2.toString();
            }
            String string3 = string2;
            if (this.surfaceGroupId == null) {
                final StringBuilder sb3 = new StringBuilder();
                sb3.append(string2);
                sb3.append(" surfaceGroupId");
                string3 = sb3.toString();
            }
            if (string3.isEmpty()) {
                return new AutoValue_SessionConfig_OutputConfig(this.surface, this.sharedSurfaces, this.physicalCameraId, this.surfaceGroupId, null);
            }
            final StringBuilder sb4 = new StringBuilder();
            sb4.append("Missing required properties:");
            sb4.append(string3);
            throw new IllegalStateException(sb4.toString());
        }
        
        @Override
        public OutputConfig.Builder setPhysicalCameraId(final String physicalCameraId) {
            this.physicalCameraId = physicalCameraId;
            return this;
        }
        
        @Override
        public OutputConfig.Builder setSharedSurfaces(final List<DeferrableSurface> sharedSurfaces) {
            if (sharedSurfaces != null) {
                this.sharedSurfaces = sharedSurfaces;
                return this;
            }
            throw new NullPointerException("Null sharedSurfaces");
        }
        
        @Override
        public OutputConfig.Builder setSurface(final DeferrableSurface surface) {
            if (surface != null) {
                this.surface = surface;
                return this;
            }
            throw new NullPointerException("Null surface");
        }
        
        @Override
        public OutputConfig.Builder setSurfaceGroupId(final int i) {
            this.surfaceGroupId = i;
            return this;
        }
    }
}

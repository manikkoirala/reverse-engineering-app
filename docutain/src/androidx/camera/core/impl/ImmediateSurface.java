// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl;

import androidx.camera.core.impl.utils.futures.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import android.util.Size;
import android.view.Surface;

public final class ImmediateSurface extends DeferrableSurface
{
    private final Surface mSurface;
    
    public ImmediateSurface(final Surface mSurface) {
        this.mSurface = mSurface;
    }
    
    public ImmediateSurface(final Surface mSurface, final Size size, final int n) {
        super(size, n);
        this.mSurface = mSurface;
    }
    
    public ListenableFuture<Surface> provideSurface() {
        return Futures.immediateFuture(this.mSurface);
    }
}

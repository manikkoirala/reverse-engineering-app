// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl.utils;

import java.io.EOFException;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.DataInputStream;
import java.nio.ByteOrder;
import java.io.DataInput;
import java.io.InputStream;

final class ByteOrderedDataInputStream extends InputStream implements DataInput
{
    private static final ByteOrder BIG_ENDIAN;
    private static final ByteOrder LITTLE_ENDIAN;
    private ByteOrder mByteOrder;
    private final DataInputStream mDataInputStream;
    final int mLength;
    int mPosition;
    
    static {
        LITTLE_ENDIAN = ByteOrder.LITTLE_ENDIAN;
        BIG_ENDIAN = ByteOrder.BIG_ENDIAN;
    }
    
    ByteOrderedDataInputStream(final InputStream inputStream) throws IOException {
        this(inputStream, ByteOrder.BIG_ENDIAN);
    }
    
    ByteOrderedDataInputStream(final InputStream in, final ByteOrder mByteOrder) throws IOException {
        this.mByteOrder = ByteOrder.BIG_ENDIAN;
        final DataInputStream mDataInputStream = new DataInputStream(in);
        this.mDataInputStream = mDataInputStream;
        final int available = mDataInputStream.available();
        this.mLength = available;
        this.mPosition = 0;
        mDataInputStream.mark(available);
        this.mByteOrder = mByteOrder;
    }
    
    ByteOrderedDataInputStream(final byte[] buf) throws IOException {
        this(new ByteArrayInputStream(buf));
    }
    
    @Override
    public int available() throws IOException {
        return this.mDataInputStream.available();
    }
    
    public int getLength() {
        return this.mLength;
    }
    
    @Override
    public void mark(final int readlimit) {
        synchronized (this.mDataInputStream) {
            this.mDataInputStream.mark(readlimit);
        }
    }
    
    public int peek() {
        return this.mPosition;
    }
    
    @Override
    public int read() throws IOException {
        ++this.mPosition;
        return this.mDataInputStream.read();
    }
    
    @Override
    public int read(final byte[] b, int read, final int len) throws IOException {
        read = this.mDataInputStream.read(b, read, len);
        this.mPosition += read;
        return read;
    }
    
    @Override
    public boolean readBoolean() throws IOException {
        ++this.mPosition;
        return this.mDataInputStream.readBoolean();
    }
    
    @Override
    public byte readByte() throws IOException {
        final int mPosition = this.mPosition + 1;
        this.mPosition = mPosition;
        if (mPosition > this.mLength) {
            throw new EOFException();
        }
        final int read = this.mDataInputStream.read();
        if (read >= 0) {
            return (byte)read;
        }
        throw new EOFException();
    }
    
    @Override
    public char readChar() throws IOException {
        this.mPosition += 2;
        return this.mDataInputStream.readChar();
    }
    
    @Override
    public double readDouble() throws IOException {
        return Double.longBitsToDouble(this.readLong());
    }
    
    @Override
    public float readFloat() throws IOException {
        return Float.intBitsToFloat(this.readInt());
    }
    
    @Override
    public void readFully(final byte[] b) throws IOException {
        final int mPosition = this.mPosition + b.length;
        this.mPosition = mPosition;
        if (mPosition > this.mLength) {
            throw new EOFException();
        }
        if (this.mDataInputStream.read(b, 0, b.length) == b.length) {
            return;
        }
        throw new IOException("Couldn't read up to the length of buffer");
    }
    
    @Override
    public void readFully(final byte[] b, final int off, final int len) throws IOException {
        final int mPosition = this.mPosition + len;
        this.mPosition = mPosition;
        if (mPosition > this.mLength) {
            throw new EOFException();
        }
        if (this.mDataInputStream.read(b, off, len) == len) {
            return;
        }
        throw new IOException("Couldn't read up to the length of buffer");
    }
    
    @Override
    public int readInt() throws IOException {
        final int mPosition = this.mPosition + 4;
        this.mPosition = mPosition;
        if (mPosition > this.mLength) {
            throw new EOFException();
        }
        final int read = this.mDataInputStream.read();
        final int read2 = this.mDataInputStream.read();
        final int read3 = this.mDataInputStream.read();
        final int read4 = this.mDataInputStream.read();
        if ((read | read2 | read3 | read4) < 0) {
            throw new EOFException();
        }
        final ByteOrder mByteOrder = this.mByteOrder;
        if (mByteOrder == ByteOrderedDataInputStream.LITTLE_ENDIAN) {
            return (read4 << 24) + (read3 << 16) + (read2 << 8) + read;
        }
        if (mByteOrder == ByteOrderedDataInputStream.BIG_ENDIAN) {
            return (read << 24) + (read2 << 16) + (read3 << 8) + read4;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Invalid byte order: ");
        sb.append(this.mByteOrder);
        throw new IOException(sb.toString());
    }
    
    @Override
    public String readLine() {
        throw new UnsupportedOperationException("readLine() not implemented.");
    }
    
    @Override
    public long readLong() throws IOException {
        final int mPosition = this.mPosition + 8;
        this.mPosition = mPosition;
        if (mPosition > this.mLength) {
            throw new EOFException();
        }
        final int read = this.mDataInputStream.read();
        final int read2 = this.mDataInputStream.read();
        final int read3 = this.mDataInputStream.read();
        final int read4 = this.mDataInputStream.read();
        final int read5 = this.mDataInputStream.read();
        final int read6 = this.mDataInputStream.read();
        final int read7 = this.mDataInputStream.read();
        final int read8 = this.mDataInputStream.read();
        if ((read | read2 | read3 | read4 | read5 | read6 | read7 | read8) < 0) {
            throw new EOFException();
        }
        final ByteOrder mByteOrder = this.mByteOrder;
        if (mByteOrder == ByteOrderedDataInputStream.LITTLE_ENDIAN) {
            return ((long)read8 << 56) + ((long)read7 << 48) + ((long)read6 << 40) + ((long)read5 << 32) + ((long)read4 << 24) + ((long)read3 << 16) + ((long)read2 << 8) + read;
        }
        if (mByteOrder == ByteOrderedDataInputStream.BIG_ENDIAN) {
            return ((long)read << 56) + ((long)read2 << 48) + ((long)read3 << 40) + ((long)read4 << 32) + ((long)read5 << 24) + ((long)read6 << 16) + ((long)read7 << 8) + read8;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Invalid byte order: ");
        sb.append(this.mByteOrder);
        throw new IOException(sb.toString());
    }
    
    @Override
    public short readShort() throws IOException {
        final int mPosition = this.mPosition + 2;
        this.mPosition = mPosition;
        if (mPosition > this.mLength) {
            throw new EOFException();
        }
        final int read = this.mDataInputStream.read();
        final int read2 = this.mDataInputStream.read();
        if ((read | read2) < 0) {
            throw new EOFException();
        }
        final ByteOrder mByteOrder = this.mByteOrder;
        if (mByteOrder == ByteOrderedDataInputStream.LITTLE_ENDIAN) {
            return (short)((read2 << 8) + read);
        }
        if (mByteOrder == ByteOrderedDataInputStream.BIG_ENDIAN) {
            return (short)((read << 8) + read2);
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Invalid byte order: ");
        sb.append(this.mByteOrder);
        throw new IOException(sb.toString());
    }
    
    @Override
    public String readUTF() throws IOException {
        this.mPosition += 2;
        return this.mDataInputStream.readUTF();
    }
    
    @Override
    public int readUnsignedByte() throws IOException {
        ++this.mPosition;
        return this.mDataInputStream.readUnsignedByte();
    }
    
    public long readUnsignedInt() throws IOException {
        return (long)this.readInt() & 0xFFFFFFFFL;
    }
    
    @Override
    public int readUnsignedShort() throws IOException {
        final int mPosition = this.mPosition + 2;
        this.mPosition = mPosition;
        if (mPosition > this.mLength) {
            throw new EOFException();
        }
        final int read = this.mDataInputStream.read();
        final int read2 = this.mDataInputStream.read();
        if ((read | read2) < 0) {
            throw new EOFException();
        }
        final ByteOrder mByteOrder = this.mByteOrder;
        if (mByteOrder == ByteOrderedDataInputStream.LITTLE_ENDIAN) {
            return (read2 << 8) + read;
        }
        if (mByteOrder == ByteOrderedDataInputStream.BIG_ENDIAN) {
            return (read << 8) + read2;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Invalid byte order: ");
        sb.append(this.mByteOrder);
        throw new IOException(sb.toString());
    }
    
    public void seek(long n) throws IOException {
        final int mPosition = this.mPosition;
        if (mPosition > n) {
            this.mPosition = 0;
            this.mDataInputStream.reset();
            this.mDataInputStream.mark(this.mLength);
        }
        else {
            n -= mPosition;
        }
        final int n2 = (int)n;
        if (this.skipBytes(n2) == n2) {
            return;
        }
        throw new IOException("Couldn't seek up to the byteCount");
    }
    
    public void setByteOrder(final ByteOrder mByteOrder) {
        this.mByteOrder = mByteOrder;
    }
    
    @Override
    public int skipBytes(int i) throws IOException {
        int min;
        for (min = Math.min(i, this.mLength - this.mPosition), i = 0; i < min; i += this.mDataInputStream.skipBytes(min - i)) {}
        this.mPosition += i;
        return i;
    }
}

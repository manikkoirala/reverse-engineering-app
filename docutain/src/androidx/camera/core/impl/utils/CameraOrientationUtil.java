// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl.utils;

import androidx.camera.core.Logger;

public final class CameraOrientationUtil
{
    private static final String TAG = "CameraOrientationUtil";
    
    private CameraOrientationUtil() {
    }
    
    public static int degreesToSurfaceRotation(int i) {
        if (i != 0) {
            if (i != 90) {
                if (i != 180) {
                    if (i != 270) {
                        final StringBuilder sb = new StringBuilder();
                        sb.append("Invalid sensor rotation: ");
                        sb.append(i);
                        throw new IllegalStateException(sb.toString());
                    }
                    i = 3;
                }
                else {
                    i = 2;
                }
            }
            else {
                i = 1;
            }
        }
        else {
            i = 0;
        }
        return i;
    }
    
    public static int getRelativeImageRotation(final int i, final int j, final boolean b) {
        int k;
        if (b) {
            k = (j - i + 360) % 360;
        }
        else {
            k = (j + i) % 360;
        }
        if (Logger.isDebugEnabled("CameraOrientationUtil")) {
            Logger.d("CameraOrientationUtil", String.format("getRelativeImageRotation: destRotationDegrees=%s, sourceRotationDegrees=%s, isOppositeFacing=%s, result=%s", i, j, b, k));
        }
        return k;
    }
    
    public static int surfaceRotationToDegrees(int i) {
        if (i != 0) {
            if (i != 1) {
                if (i != 2) {
                    if (i != 3) {
                        final StringBuilder sb = new StringBuilder();
                        sb.append("Unsupported surface rotation: ");
                        sb.append(i);
                        throw new IllegalArgumentException(sb.toString());
                    }
                    i = 270;
                }
                else {
                    i = 180;
                }
            }
            else {
                i = 90;
            }
        }
        else {
            i = 0;
        }
        return i;
    }
}

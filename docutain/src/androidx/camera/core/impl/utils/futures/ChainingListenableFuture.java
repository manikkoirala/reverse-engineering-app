// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl.utils.futures;

import java.lang.reflect.UndeclaredThrowableException;
import androidx.camera.core.impl.utils.executor.CameraXExecutors;
import java.util.concurrent.CancellationException;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import androidx.core.util.Preconditions;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.BlockingQueue;
import com.google.common.util.concurrent.ListenableFuture;

class ChainingListenableFuture<I, O> extends FutureChain<O> implements Runnable
{
    private AsyncFunction<? super I, ? extends O> mFunction;
    private ListenableFuture<? extends I> mInputFuture;
    private final BlockingQueue<Boolean> mMayInterruptIfRunningChannel;
    private final CountDownLatch mOutputCreated;
    volatile ListenableFuture<? extends O> mOutputFuture;
    
    ChainingListenableFuture(final AsyncFunction<? super I, ? extends O> asyncFunction, final ListenableFuture<? extends I> listenableFuture) {
        this.mMayInterruptIfRunningChannel = new LinkedBlockingQueue<Boolean>(1);
        this.mOutputCreated = new CountDownLatch(1);
        this.mFunction = Preconditions.checkNotNull(asyncFunction);
        this.mInputFuture = Preconditions.checkNotNull(listenableFuture);
    }
    
    private void cancel(final Future<?> future, final boolean b) {
        if (future != null) {
            future.cancel(b);
        }
    }
    
    private <E> void putUninterruptibly(final BlockingQueue<E> blockingQueue, final E e) {
        boolean b = false;
        while (true) {
            try {
                blockingQueue.put(e);
            }
            catch (final InterruptedException ex) {
                b = true;
                continue;
            }
            finally {
                if (b) {
                    Thread.currentThread().interrupt();
                }
            }
            break;
        }
    }
    
    private <E> E takeUninterruptibly(final BlockingQueue<E> blockingQueue) {
        boolean b = false;
        try {
            return blockingQueue.take();
        }
        catch (final InterruptedException ex) {
            b = true;
            return blockingQueue.take();
        }
        finally {
            if (b) {
                Thread.currentThread().interrupt();
            }
        }
    }
    
    @Override
    public boolean cancel(final boolean b) {
        if (super.cancel(b)) {
            this.putUninterruptibly(this.mMayInterruptIfRunningChannel, b);
            this.cancel((Future<?>)this.mInputFuture, b);
            this.cancel((Future<?>)this.mOutputFuture, b);
            return true;
        }
        return false;
    }
    
    @Override
    public O get() throws InterruptedException, ExecutionException {
        if (!this.isDone()) {
            final ListenableFuture<? extends I> mInputFuture = this.mInputFuture;
            if (mInputFuture != null) {
                mInputFuture.get();
            }
            this.mOutputCreated.await();
            final ListenableFuture<? extends O> mOutputFuture = this.mOutputFuture;
            if (mOutputFuture != null) {
                mOutputFuture.get();
            }
        }
        return super.get();
    }
    
    @Override
    public O get(long nanoTime, final TimeUnit sourceUnit) throws TimeoutException, ExecutionException, InterruptedException {
        long n = nanoTime;
        TimeUnit timeUnit = sourceUnit;
        if (!this.isDone()) {
            long convert = nanoTime;
            TimeUnit nanoseconds;
            if ((nanoseconds = sourceUnit) != TimeUnit.NANOSECONDS) {
                convert = TimeUnit.NANOSECONDS.convert(nanoTime, sourceUnit);
                nanoseconds = TimeUnit.NANOSECONDS;
            }
            final ListenableFuture<? extends I> mInputFuture = this.mInputFuture;
            nanoTime = convert;
            if (mInputFuture != null) {
                nanoTime = System.nanoTime();
                mInputFuture.get(convert, nanoseconds);
                nanoTime = convert - Math.max(0L, System.nanoTime() - nanoTime);
            }
            final long nanoTime2 = System.nanoTime();
            if (!this.mOutputCreated.await(nanoTime, nanoseconds)) {
                throw new TimeoutException();
            }
            nanoTime -= Math.max(0L, System.nanoTime() - nanoTime2);
            final ListenableFuture<? extends O> mOutputFuture = this.mOutputFuture;
            n = nanoTime;
            timeUnit = nanoseconds;
            if (mOutputFuture != null) {
                mOutputFuture.get(nanoTime, nanoseconds);
                n = nanoTime;
                timeUnit = nanoseconds;
            }
        }
        return super.get(n, timeUnit);
    }
    
    @Override
    public void run() {
        try {
            Label_0152: {
                try {
                    try {
                        final com.google.common.util.concurrent.ListenableFuture<? extends O> apply = this.mFunction.apply((Object)Futures.getUninterruptibly((Future<Object>)this.mInputFuture));
                        this.mOutputFuture = apply;
                        if (this.isCancelled()) {
                            apply.cancel((boolean)this.takeUninterruptibly(this.mMayInterruptIfRunningChannel));
                            this.mOutputFuture = null;
                            this.mFunction = null;
                            this.mInputFuture = null;
                            this.mOutputCreated.countDown();
                            return;
                        }
                        apply.addListener((Runnable)new Runnable(this, apply) {
                            final ChainingListenableFuture this$0;
                            final ListenableFuture val$outputFuture;
                            
                            @Override
                            public void run() {
                                while (true) {
                                    try {
                                        try {
                                            this.this$0.set(Futures.getUninterruptibly((Future<V>)this.val$outputFuture));
                                            this.this$0.mOutputFuture = null;
                                        }
                                        finally {}
                                    }
                                    catch (final ExecutionException ex) {
                                        this.this$0.setException(ex.getCause());
                                        continue;
                                    }
                                    catch (final CancellationException ex2) {
                                        this.this$0.cancel(false);
                                        this.this$0.mOutputFuture = null;
                                        return;
                                    }
                                    break;
                                }
                                this.this$0.mOutputFuture = null;
                            }
                        }, CameraXExecutors.directExecutor());
                        break Label_0152;
                    }
                    finally {}
                }
                catch (final Error exception) {
                    this.setException(exception);
                }
                catch (final Exception ex) {}
                catch (final UndeclaredThrowableException ex2) {
                    final Exception exception2;
                    this.setException(exception2);
                    this.mFunction = null;
                    this.mInputFuture = null;
                    this.mOutputCreated.countDown();
                    return;
                    final UndeclaredThrowableException ex3;
                    this.setException(ex3.getCause());
                }
            }
        }
        catch (final ExecutionException ex4) {}
        catch (final CancellationException ex5) {}
        return;
        this.mFunction = null;
        this.mInputFuture = null;
        this.mOutputCreated.countDown();
    }
}

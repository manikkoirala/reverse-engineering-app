// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl.utils.futures;

import java.util.concurrent.CancellationException;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.Collection;
import androidx.camera.core.impl.utils.executor.CameraXExecutors;
import java.util.Iterator;
import java.util.ArrayList;
import androidx.core.util.Preconditions;
import java.util.concurrent.Executor;
import androidx.concurrent.futures.CallbackToFutureAdapter;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.List;
import com.google.common.util.concurrent.ListenableFuture;

class ListFuture<V> implements ListenableFuture<List<V>>
{
    private final boolean mAllMustSucceed;
    List<? extends ListenableFuture<? extends V>> mFutures;
    private final AtomicInteger mRemaining;
    private final ListenableFuture<List<V>> mResult;
    CallbackToFutureAdapter.Completer<List<V>> mResultNotifier;
    List<V> mValues;
    
    ListFuture(final List<? extends ListenableFuture<? extends V>> list, final boolean mAllMustSucceed, final Executor executor) {
        this.mFutures = Preconditions.checkNotNull(list);
        this.mValues = new ArrayList<V>(list.size());
        this.mAllMustSucceed = mAllMustSucceed;
        this.mRemaining = new AtomicInteger(list.size());
        this.mResult = CallbackToFutureAdapter.getFuture((CallbackToFutureAdapter.Resolver<List<V>>)new CallbackToFutureAdapter.Resolver<List<V>>(this) {
            final ListFuture this$0;
            
            @Override
            public Object attachCompleter(final Completer<List<V>> mResultNotifier) {
                Preconditions.checkState(this.this$0.mResultNotifier == null, "The result can only set once!");
                this.this$0.mResultNotifier = mResultNotifier;
                final StringBuilder sb = new StringBuilder();
                sb.append("ListFuture[");
                sb.append(this);
                sb.append("]");
                return sb.toString();
            }
        });
        this.init(executor);
    }
    
    private void callAllGets() throws InterruptedException {
        final List<? extends ListenableFuture<? extends V>> mFutures = this.mFutures;
        Label_0076: {
            if (mFutures != null && !this.isDone()) {
                for (final ListenableFuture listenableFuture : mFutures) {
                    while (!listenableFuture.isDone()) {
                        try {
                            listenableFuture.get();
                            continue;
                        }
                        catch (final InterruptedException listenableFuture) {
                            throw listenableFuture;
                        }
                        catch (final Error listenableFuture) {
                            throw listenableFuture;
                        }
                        finally {
                            if (this.mAllMustSucceed) {
                                return;
                            }
                            continue;
                        }
                        break Label_0076;
                    }
                }
            }
        }
    }
    
    private void init(final Executor executor) {
        this.addListener(new Runnable(this) {
            final ListFuture this$0;
            
            @Override
            public void run() {
                this.this$0.mValues = null;
                this.this$0.mFutures = null;
            }
        }, CameraXExecutors.directExecutor());
        if (this.mFutures.isEmpty()) {
            this.mResultNotifier.set(new ArrayList<V>((Collection<? extends V>)this.mValues));
            return;
        }
        final int n = 0;
        for (int i = 0; i < this.mFutures.size(); ++i) {
            this.mValues.add(null);
        }
        final List<? extends ListenableFuture<? extends V>> mFutures = this.mFutures;
        for (int j = n; j < mFutures.size(); ++j) {
            final ListenableFuture listenableFuture = mFutures.get(j);
            listenableFuture.addListener((Runnable)new Runnable(this, j, listenableFuture) {
                final ListFuture this$0;
                final int val$index;
                final ListenableFuture val$listenable;
                
                @Override
                public void run() {
                    this.this$0.setOneValue(this.val$index, (Future<? extends V>)this.val$listenable);
                }
            }, executor);
        }
    }
    
    public void addListener(final Runnable runnable, final Executor executor) {
        this.mResult.addListener(runnable, executor);
    }
    
    public boolean cancel(final boolean b) {
        final List<? extends ListenableFuture<? extends V>> mFutures = this.mFutures;
        if (mFutures != null) {
            final Iterator<? extends ListenableFuture<? extends V>> iterator = mFutures.iterator();
            while (iterator.hasNext()) {
                ((ListenableFuture)iterator.next()).cancel(b);
            }
        }
        return this.mResult.cancel(b);
    }
    
    public List<V> get() throws InterruptedException, ExecutionException {
        this.callAllGets();
        return (List)this.mResult.get();
    }
    
    public List<V> get(final long n, final TimeUnit timeUnit) throws InterruptedException, ExecutionException, TimeoutException {
        return (List)this.mResult.get(n, timeUnit);
    }
    
    public boolean isCancelled() {
        return this.mResult.isCancelled();
    }
    
    public boolean isDone() {
        return this.mResult.isDone();
    }
    
    void setOneValue(int n, final Future<? extends V> future) {
        final List<V> mValues = this.mValues;
        if (!this.isDone()) {
            if (mValues != null) {
                final boolean b = true;
                final boolean b2 = true;
                final boolean b3 = true;
                boolean b4 = true;
                final boolean b5 = true;
                final boolean b6 = true;
                try {
                Block_26_Outer:
                    while (true) {
                        try {
                            Preconditions.checkState(future.isDone(), "Tried to set value from future which is not done");
                            mValues.set(n, Futures.getUninterruptibly((Future<V>)future));
                            n = this.mRemaining.decrementAndGet();
                            b4 = (n >= 0 && b6);
                            Preconditions.checkState(b4, "Less than 0 remaining futures");
                            if (n != 0) {
                                return;
                            }
                            final List<V> mValues2 = this.mValues;
                            if (mValues2 != null) {
                                final CallbackToFutureAdapter.Completer<List<V>> completer = this.mResultNotifier;
                                final ArrayList list = new ArrayList(mValues2);
                                completer.set((ArrayList)list);
                                return;
                            }
                            Label_0126: {
                                Preconditions.checkState(this.isDone());
                            }
                            return;
                        }
                        finally {
                            n = this.mRemaining.decrementAndGet();
                            Preconditions.checkState(n >= 0 && b5, "Less than 0 remaining futures");
                            if (n == 0) {
                                final List<V> mValues3 = this.mValues;
                                if (mValues3 != null) {
                                    this.mResultNotifier.set(new ArrayList<V>((Collection<? extends V>)mValues3));
                                }
                                else {
                                    Preconditions.checkState(this.isDone());
                                }
                            }
                        Block_23_Outer:
                            while (true) {
                                while (true) {
                                    Block_22: {
                                        final List<V> mValues4;
                                        Block_13: {
                                            Label_0389: {
                                                break Label_0389;
                                                mValues4 = this.mValues;
                                                iftrue(Label_0126:)(mValues4 == null);
                                                break Block_13;
                                                while (true) {
                                                    final List<V> mValues5 = this.mValues;
                                                    iftrue(Label_0126:)(mValues5 == null);
                                                    Block_18: {
                                                        break Block_18;
                                                        Label_0426: {
                                                            return;
                                                        }
                                                    }
                                                    CallbackToFutureAdapter.Completer<List<V>> completer = this.mResultNotifier;
                                                    ArrayList list = new ArrayList(mValues5);
                                                    continue Block_26_Outer;
                                                    final ExecutionException ex;
                                                    this.mResultNotifier.setException(ex.getCause());
                                                    n = this.mRemaining.decrementAndGet();
                                                    b4 = (n >= 0 && b3);
                                                    Preconditions.checkState(b4, "Less than 0 remaining futures");
                                                    iftrue(Label_0426:)(n != 0);
                                                    break Block_22;
                                                    completer = this.mResultNotifier;
                                                    final List<V> mValues6;
                                                    list = new ArrayList(mValues6);
                                                    continue Block_26_Outer;
                                                    Label_0387: {
                                                        b4 = false;
                                                    }
                                                    break Label_0389;
                                                    final RuntimeException exception;
                                                    this.mResultNotifier.setException(exception);
                                                    n = this.mRemaining.decrementAndGet();
                                                    b4 = (n >= 0 && b2);
                                                    Preconditions.checkState(b4, "Less than 0 remaining futures");
                                                    iftrue(Label_0426:)(n != 0);
                                                    continue Block_23_Outer;
                                                }
                                            }
                                            Preconditions.checkState(b4, "Less than 0 remaining futures");
                                            iftrue(Label_0426:)(n != 0);
                                            while (true) {
                                                Block_27: {
                                                    break Block_27;
                                                    final CallbackToFutureAdapter.Completer<List<V>> completer = this.mResultNotifier;
                                                    final List<V> mValues7;
                                                    final ArrayList list = new ArrayList(mValues7);
                                                    continue Block_26_Outer;
                                                    b4 = b;
                                                    break Block_23_Outer;
                                                }
                                                final List<V> mValues7 = this.mValues;
                                                iftrue(Label_0126:)(mValues7 == null);
                                                continue;
                                            }
                                            b4 = false;
                                            break Block_23_Outer;
                                        }
                                        CallbackToFutureAdapter.Completer<List<V>> completer = this.mResultNotifier;
                                        ArrayList list = new ArrayList(mValues4);
                                        continue Block_26_Outer;
                                    }
                                    final List<V> mValues6 = this.mValues;
                                    iftrue(Label_0126:)(mValues6 == null);
                                    continue;
                                }
                                this.cancel(false);
                                n = this.mRemaining.decrementAndGet();
                                iftrue(Label_0387:)(n < 0);
                                continue Block_23_Outer;
                            }
                            Preconditions.checkState(b4, "Less than 0 remaining futures");
                            iftrue(Label_0426:)(n != 0);
                        }
                        break;
                    }
                }
                catch (final Error error) {}
                catch (final RuntimeException ex2) {}
                catch (final ExecutionException ex3) {}
                catch (final CancellationException ex4) {}
            }
        }
        Preconditions.checkState(this.mAllMustSucceed, "Future was done before all dependencies completed");
    }
}

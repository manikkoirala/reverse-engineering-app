// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl.utils.futures;

public interface FutureCallback<V>
{
    void onFailure(final Throwable p0);
    
    void onSuccess(final V p0);
}

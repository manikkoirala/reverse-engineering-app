// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl.utils.futures;

import androidx.arch.core.util.Function;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import androidx.core.util.Preconditions;
import androidx.concurrent.futures.CallbackToFutureAdapter;
import com.google.common.util.concurrent.ListenableFuture;

public class FutureChain<V> implements ListenableFuture<V>
{
    CallbackToFutureAdapter.Completer<V> mCompleter;
    private final ListenableFuture<V> mDelegate;
    
    FutureChain() {
        this.mDelegate = CallbackToFutureAdapter.getFuture((CallbackToFutureAdapter.Resolver<V>)new CallbackToFutureAdapter.Resolver<V>(this) {
            final FutureChain this$0;
            
            @Override
            public Object attachCompleter(final Completer<V> mCompleter) {
                Preconditions.checkState(this.this$0.mCompleter == null, "The result can only set once!");
                this.this$0.mCompleter = mCompleter;
                final StringBuilder sb = new StringBuilder();
                sb.append("FutureChain[");
                sb.append(this.this$0);
                sb.append("]");
                return sb.toString();
            }
        });
    }
    
    FutureChain(final ListenableFuture<V> listenableFuture) {
        this.mDelegate = Preconditions.checkNotNull(listenableFuture);
    }
    
    public static <V> FutureChain<V> from(final ListenableFuture<V> listenableFuture) {
        FutureChain futureChain;
        if (listenableFuture instanceof FutureChain) {
            futureChain = (FutureChain)listenableFuture;
        }
        else {
            futureChain = new FutureChain((ListenableFuture<V>)listenableFuture);
        }
        return futureChain;
    }
    
    public final void addCallback(final FutureCallback<? super V> futureCallback, final Executor executor) {
        Futures.addCallback((com.google.common.util.concurrent.ListenableFuture<Object>)this, (FutureCallback<? super Object>)futureCallback, executor);
    }
    
    public void addListener(final Runnable runnable, final Executor executor) {
        this.mDelegate.addListener(runnable, executor);
    }
    
    public boolean cancel(final boolean b) {
        return this.mDelegate.cancel(b);
    }
    
    public V get() throws InterruptedException, ExecutionException {
        return (V)this.mDelegate.get();
    }
    
    public V get(final long n, final TimeUnit timeUnit) throws InterruptedException, ExecutionException, TimeoutException {
        return (V)this.mDelegate.get(n, timeUnit);
    }
    
    public boolean isCancelled() {
        return this.mDelegate.isCancelled();
    }
    
    public boolean isDone() {
        return this.mDelegate.isDone();
    }
    
    boolean set(final V v) {
        final CallbackToFutureAdapter.Completer<V> mCompleter = this.mCompleter;
        return mCompleter != null && mCompleter.set(v);
    }
    
    boolean setException(final Throwable exception) {
        final CallbackToFutureAdapter.Completer<V> mCompleter = this.mCompleter;
        return mCompleter != null && mCompleter.setException(exception);
    }
    
    public final <T> FutureChain<T> transform(final Function<? super V, T> function, final Executor executor) {
        return (FutureChain)Futures.transform((com.google.common.util.concurrent.ListenableFuture<Object>)this, (Function<? super Object, ?>)function, executor);
    }
    
    public final <T> FutureChain<T> transformAsync(final AsyncFunction<? super V, T> asyncFunction, final Executor executor) {
        return (FutureChain)Futures.transformAsync((com.google.common.util.concurrent.ListenableFuture<Object>)this, (AsyncFunction<? super Object, ?>)asyncFunction, executor);
    }
}

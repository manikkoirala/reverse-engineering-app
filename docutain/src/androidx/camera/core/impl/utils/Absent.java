// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl.utils;

import androidx.core.util.Supplier;
import androidx.core.util.Preconditions;

final class Absent<T> extends Optional<T>
{
    static final Absent<Object> sInstance;
    private static final long serialVersionUID = 0L;
    
    static {
        sInstance = new Absent<Object>();
    }
    
    private Absent() {
    }
    
    private Object readResolve() {
        return Absent.sInstance;
    }
    
    static <T> Optional<T> withType() {
        return (Optional<T>)Absent.sInstance;
    }
    
    @Override
    public boolean equals(final Object o) {
        return o == this;
    }
    
    @Override
    public T get() {
        throw new IllegalStateException("Optional.get() cannot be called on an absent value");
    }
    
    @Override
    public int hashCode() {
        return 2040732332;
    }
    
    @Override
    public boolean isPresent() {
        return false;
    }
    
    @Override
    public Optional<T> or(final Optional<? extends T> optional) {
        return Preconditions.checkNotNull((Optional<T>)optional);
    }
    
    @Override
    public T or(final Supplier<? extends T> supplier) {
        return Preconditions.checkNotNull((T)supplier.get(), "use Optional.orNull() instead of a Supplier that returns null");
    }
    
    @Override
    public T or(final T t) {
        return Preconditions.checkNotNull(t, "use Optional.orNull() instead of Optional.or(null)");
    }
    
    @Override
    public T orNull() {
        return null;
    }
    
    @Override
    public String toString() {
        return "Optional.absent()";
    }
}

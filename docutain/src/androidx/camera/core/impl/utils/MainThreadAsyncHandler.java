// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl.utils;

import androidx.core.os.HandlerCompat;
import android.os.Looper;
import android.os.Handler;

public final class MainThreadAsyncHandler
{
    private static volatile Handler sHandler;
    
    private MainThreadAsyncHandler() {
    }
    
    public static Handler getInstance() {
        if (MainThreadAsyncHandler.sHandler != null) {
            return MainThreadAsyncHandler.sHandler;
        }
        synchronized (MainThreadAsyncHandler.class) {
            if (MainThreadAsyncHandler.sHandler == null) {
                MainThreadAsyncHandler.sHandler = HandlerCompat.createAsync(Looper.getMainLooper());
            }
            return MainThreadAsyncHandler.sHandler;
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl.utils;

import androidx.core.util.Supplier;
import androidx.core.util.Preconditions;
import java.io.Serializable;

public abstract class Optional<T> implements Serializable
{
    private static final long serialVersionUID = 0L;
    
    Optional() {
    }
    
    public static <T> Optional<T> absent() {
        return Absent.withType();
    }
    
    public static <T> Optional<T> fromNullable(final T t) {
        Optional<Object> absent;
        if (t == null) {
            absent = absent();
        }
        else {
            absent = new Present<Object>(t);
        }
        return (Optional<T>)absent;
    }
    
    public static <T> Optional<T> of(final T t) {
        return new Present<T>(Preconditions.checkNotNull(t));
    }
    
    @Override
    public abstract boolean equals(final Object p0);
    
    public abstract T get();
    
    @Override
    public abstract int hashCode();
    
    public abstract boolean isPresent();
    
    public abstract Optional<T> or(final Optional<? extends T> p0);
    
    public abstract T or(final Supplier<? extends T> p0);
    
    public abstract T or(final T p0);
    
    public abstract T orNull();
    
    @Override
    public abstract String toString();
}

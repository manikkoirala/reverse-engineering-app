// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl.utils;

import android.content.ContextWrapper;
import android.app.Application;
import android.os.Build$VERSION;
import android.content.Context;

public final class ContextUtil
{
    private ContextUtil() {
    }
    
    public static Context getApplicationContext(final Context context) {
        final Context applicationContext = context.getApplicationContext();
        if (Build$VERSION.SDK_INT >= 30) {
            final String attributionTag = Api30Impl.getAttributionTag(context);
            if (attributionTag != null) {
                return Api30Impl.createAttributionContext(applicationContext, attributionTag);
            }
        }
        return applicationContext;
    }
    
    public static Application getApplicationFromContext(Context context) {
        Application application;
        for (context = getApplicationContext(context); context instanceof ContextWrapper; context = getBaseContext((ContextWrapper)context)) {
            if (context instanceof Application) {
                application = (Application)context;
                return application;
            }
        }
        application = null;
        return application;
    }
    
    public static Context getBaseContext(final ContextWrapper contextWrapper) {
        final Context baseContext = contextWrapper.getBaseContext();
        if (Build$VERSION.SDK_INT >= 30) {
            final String attributionTag = Api30Impl.getAttributionTag((Context)contextWrapper);
            if (attributionTag != null) {
                return Api30Impl.createAttributionContext(baseContext, attributionTag);
            }
        }
        return baseContext;
    }
    
    private static class Api30Impl
    {
        static Context createAttributionContext(final Context context, final String s) {
            return context.createAttributionContext(s);
        }
        
        static String getAttributionTag(final Context context) {
            return context.getAttributionTag();
        }
    }
}

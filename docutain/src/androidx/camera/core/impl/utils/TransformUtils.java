// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl.utils;

import androidx.core.util.Preconditions;
import java.util.Locale;
import android.util.Size;
import android.graphics.Rect;
import android.graphics.Matrix$ScaleToFit;
import android.graphics.Matrix;
import android.graphics.RectF;

public class TransformUtils
{
    public static final RectF NORMALIZED_RECT;
    
    static {
        NORMALIZED_RECT = new RectF(-1.0f, -1.0f, 1.0f, 1.0f);
    }
    
    private TransformUtils() {
    }
    
    public static Matrix getExifTransform(int n, int n2, final int n3) {
        final Matrix matrix = new Matrix();
        final float n4 = (float)n2;
        final float n5 = (float)n3;
        RectF rectF = new RectF(0.0f, 0.0f, n4, n5);
        final RectF normalized_RECT = TransformUtils.NORMALIZED_RECT;
        matrix.setRectToRect(rectF, normalized_RECT, Matrix$ScaleToFit.FILL);
        n2 = 1;
        Label_0199: {
            switch (n) {
                case 8: {
                    matrix.postRotate(270.0f);
                    n = n2;
                    break Label_0199;
                }
                case 7: {
                    matrix.postScale(-1.0f, 1.0f);
                    matrix.postRotate(90.0f);
                    n = n2;
                    break Label_0199;
                }
                case 6: {
                    matrix.postRotate(90.0f);
                    n = n2;
                    break Label_0199;
                }
                case 5: {
                    matrix.postScale(-1.0f, 1.0f);
                    matrix.postRotate(270.0f);
                    n = n2;
                    break Label_0199;
                }
                case 4: {
                    matrix.postScale(1.0f, -1.0f);
                    break;
                }
                case 3: {
                    matrix.postRotate(180.0f);
                    break;
                }
                case 2: {
                    matrix.postScale(-1.0f, 1.0f);
                    break;
                }
            }
            n = 0;
        }
        if (n != 0) {
            rectF = new RectF(0.0f, 0.0f, n5, n4);
        }
        final Matrix matrix2 = new Matrix();
        matrix2.setRectToRect(normalized_RECT, rectF, Matrix$ScaleToFit.FILL);
        matrix.postConcat(matrix2);
        return matrix;
    }
    
    public static Matrix getNormalizedToBuffer(final Rect rect) {
        return getNormalizedToBuffer(new RectF(rect));
    }
    
    public static Matrix getNormalizedToBuffer(final RectF rectF) {
        final Matrix matrix = new Matrix();
        matrix.setRectToRect(TransformUtils.NORMALIZED_RECT, rectF, Matrix$ScaleToFit.FILL);
        return matrix;
    }
    
    public static Matrix getRectToRect(final RectF rectF, final RectF rectF2, final int n) {
        return getRectToRect(rectF, rectF2, n, false);
    }
    
    public static Matrix getRectToRect(final RectF rectF, final RectF rectF2, final int n, final boolean b) {
        final Matrix matrix = new Matrix();
        matrix.setRectToRect(rectF, TransformUtils.NORMALIZED_RECT, Matrix$ScaleToFit.FILL);
        matrix.postRotate((float)n);
        if (b) {
            matrix.postScale(-1.0f, 1.0f);
        }
        matrix.postConcat(getNormalizedToBuffer(rectF2));
        return matrix;
    }
    
    public static boolean hasCropping(final Rect rect, final Size size) {
        return rect.left != 0 || rect.top != 0 || rect.width() != size.getWidth() || rect.height() != size.getHeight();
    }
    
    public static boolean is90or270(final int i) {
        if (i == 90 || i == 270) {
            return true;
        }
        if (i != 0 && i != 180) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Invalid rotation degrees: ");
            sb.append(i);
            throw new IllegalArgumentException(sb.toString());
        }
        return false;
    }
    
    public static boolean isAspectRatioMatchingWithRoundingError(final Size size, final boolean b, final Size size2, final boolean b2) {
        float n2;
        float n;
        if (b) {
            n = (n2 = size.getWidth() / (float)size.getHeight());
        }
        else {
            n = (size.getWidth() + 1.0f) / (size.getHeight() - 1.0f);
            n2 = (size.getWidth() - 1.0f) / (size.getHeight() + 1.0f);
        }
        float n4;
        float n3;
        if (b2) {
            n3 = (n4 = size2.getWidth() / (float)size2.getHeight());
        }
        else {
            n4 = (size2.getWidth() + 1.0f) / (size2.getHeight() - 1.0f);
            n3 = (size2.getWidth() - 1.0f) / (size2.getHeight() + 1.0f);
        }
        return n >= n3 && n4 >= n2;
    }
    
    public static float max(final float a, final float b, final float a2, final float b2) {
        return Math.max(Math.max(a, b), Math.max(a2, b2));
    }
    
    public static float min(final float a, final float b, final float a2, final float b2) {
        return Math.min(Math.min(a, b), Math.min(a2, b2));
    }
    
    public static Size rectToSize(final Rect rect) {
        return new Size(rect.width(), rect.height());
    }
    
    public static String rectToString(final Rect rect) {
        return String.format(Locale.US, "%s(%dx%d)", rect, rect.width(), rect.height());
    }
    
    public static float[] rectToVertices(final RectF rectF) {
        return new float[] { rectF.left, rectF.top, rectF.right, rectF.top, rectF.right, rectF.bottom, rectF.left, rectF.bottom };
    }
    
    public static Size reverseSize(final Size size) {
        return new Size(size.getHeight(), size.getWidth());
    }
    
    public static Size rotateSize(final Size size, final int i) {
        final boolean b = i % 90 == 0;
        final StringBuilder sb = new StringBuilder();
        sb.append("Invalid rotation degrees: ");
        sb.append(i);
        Preconditions.checkArgument(b, (Object)sb.toString());
        Size reverseSize = size;
        if (is90or270(within360(i))) {
            reverseSize = reverseSize(size);
        }
        return reverseSize;
    }
    
    public static Rect sizeToRect(final Size size) {
        return sizeToRect(size, 0, 0);
    }
    
    public static Rect sizeToRect(final Size size, final int n, final int n2) {
        return new Rect(n, n2, size.getWidth() + n, size.getHeight() + n2);
    }
    
    public static RectF sizeToRectF(final Size size) {
        return sizeToRectF(size, 0, 0);
    }
    
    public static RectF sizeToRectF(final Size size, final int n, final int n2) {
        return new RectF((float)n, (float)n2, (float)(n + size.getWidth()), (float)(n2 + size.getHeight()));
    }
    
    public static float[] sizeToVertices(final Size size) {
        return new float[] { 0.0f, 0.0f, (float)size.getWidth(), 0.0f, (float)size.getWidth(), (float)size.getHeight(), 0.0f, (float)size.getHeight() };
    }
    
    public static Matrix updateSensorToBufferTransform(Matrix matrix, final Rect rect) {
        matrix = new Matrix(matrix);
        matrix.postTranslate((float)(-rect.left), (float)(-rect.top));
        return matrix;
    }
    
    public static RectF verticesToRect(final float[] array) {
        return new RectF(min(array[0], array[2], array[4], array[6]), min(array[1], array[3], array[5], array[7]), max(array[0], array[2], array[4], array[6]), max(array[1], array[3], array[5], array[7]));
    }
    
    public static int within360(final int n) {
        return (n % 360 + 360) % 360;
    }
}

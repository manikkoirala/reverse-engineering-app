// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl.utils;

import java.util.Locale;
import android.opengl.Matrix;

public final class MatrixExt
{
    private static final float[] sTemp;
    
    static {
        sTemp = new float[16];
    }
    
    private MatrixExt() {
    }
    
    private static void denormalize(final float[] array, final float n, final float n2) {
        Matrix.translateM(array, 0, -n, -n2, 0.0f);
    }
    
    private static void normalize(final float[] array, final float n, final float n2) {
        Matrix.translateM(array, 0, n, n2, 0.0f);
    }
    
    public static void postRotate(final float[] array, final float n, final float n2, final float n3) {
        final float[] sTemp = MatrixExt.sTemp;
        synchronized (sTemp) {
            Matrix.setIdentityM(sTemp, 0);
            normalize(sTemp, n2, n3);
            Matrix.rotateM(sTemp, 0, n, 0.0f, 0.0f, 1.0f);
            denormalize(sTemp, n2, n3);
            Matrix.multiplyMM(array, 0, sTemp, 0, array, 0);
        }
    }
    
    public static void preRotate(final float[] array, final float n, final float n2, final float n3) {
        normalize(array, n2, n3);
        Matrix.rotateM(array, 0, n, 0.0f, 0.0f, 1.0f);
        denormalize(array, n2, n3);
    }
    
    public static void setRotate(final float[] array, final float n, final float n2, final float n3) {
        Matrix.setIdentityM(array, 0);
        preRotate(array, n, n2, n3);
    }
    
    public static String toString(final float[] array, final int n) {
        return String.format(Locale.US, "Matrix:\n%2.1f %2.1f %2.1f %2.1f\n%2.1f %2.1f %2.1f %2.1f\n%2.1f %2.1f %2.1f %2.1f\n%2.1f %2.1f %2.1f %2.1f", array[n], array[n + 4], array[n + 8], array[n + 12], array[n + 1], array[n + 5], array[n + 9], array[n + 13], array[n + 2], array[n + 6], array[n + 10], array[n + 14], array[n + 3], array[n + 7], array[n + 11], array[n + 15]);
    }
}

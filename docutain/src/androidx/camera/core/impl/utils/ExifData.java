// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl.utils;

import androidx.camera.core.impl.CameraCaptureMetaData;
import java.util.concurrent.TimeUnit;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.Iterator;
import android.util.Pair;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.regex.Pattern;
import java.util.Locale;
import androidx.camera.core.Logger;
import androidx.camera.core.ImageProxy;
import android.os.Build;
import androidx.core.util.Preconditions;
import java.util.Collection;
import java.util.Arrays;
import java.nio.ByteOrder;
import java.util.Map;
import java.util.List;
import java.util.HashSet;

public class ExifData
{
    private static final boolean DEBUG = false;
    static final ExifTag[] EXIF_POINTER_TAGS;
    static final ExifTag[][] EXIF_TAGS;
    private static final ExifTag[] IFD_EXIF_TAGS;
    static final String[] IFD_FORMAT_NAMES;
    private static final ExifTag[] IFD_GPS_TAGS;
    private static final ExifTag[] IFD_INTEROPERABILITY_TAGS;
    private static final ExifTag[] IFD_TIFF_TAGS;
    static final int IFD_TYPE_EXIF = 1;
    static final int IFD_TYPE_GPS = 2;
    static final int IFD_TYPE_INTEROPERABILITY = 3;
    static final int IFD_TYPE_PRIMARY = 0;
    private static final int MM_IN_MICRONS = 1000;
    private static final String TAG = "ExifData";
    static final String TAG_EXIF_IFD_POINTER = "ExifIFDPointer";
    static final String TAG_GPS_INFO_IFD_POINTER = "GPSInfoIFDPointer";
    static final String TAG_INTEROPERABILITY_IFD_POINTER = "InteroperabilityIFDPointer";
    static final String TAG_SUB_IFD_POINTER = "SubIFDPointer";
    static final HashSet<String> sTagSetForCompatibility;
    private final List<Map<String, ExifAttribute>> mAttributes;
    private final ByteOrder mByteOrder;
    
    static {
        IFD_FORMAT_NAMES = new String[] { "", "BYTE", "STRING", "USHORT", "ULONG", "URATIONAL", "SBYTE", "UNDEFINED", "SSHORT", "SLONG", "SRATIONAL", "SINGLE", "DOUBLE", "IFD" };
        final ExifTag[] array = IFD_TIFF_TAGS = new ExifTag[] { new ExifTag("ImageWidth", 256, 3, 4), new ExifTag("ImageLength", 257, 3, 4), new ExifTag("Make", 271, 2), new ExifTag("Model", 272, 2), new ExifTag("Orientation", 274, 3), new ExifTag("XResolution", 282, 5), new ExifTag("YResolution", 283, 5), new ExifTag("ResolutionUnit", 296, 3), new ExifTag("Software", 305, 2), new ExifTag("DateTime", 306, 2), new ExifTag("YCbCrPositioning", 531, 3), new ExifTag("SubIFDPointer", 330, 4), new ExifTag("ExifIFDPointer", 34665, 4), new ExifTag("GPSInfoIFDPointer", 34853, 4) };
        final ExifTag[] array2 = IFD_EXIF_TAGS = new ExifTag[] { new ExifTag("ExposureTime", 33434, 5), new ExifTag("FNumber", 33437, 5), new ExifTag("ExposureProgram", 34850, 3), new ExifTag("PhotographicSensitivity", 34855, 3), new ExifTag("SensitivityType", 34864, 3), new ExifTag("ExifVersion", 36864, 2), new ExifTag("DateTimeOriginal", 36867, 2), new ExifTag("DateTimeDigitized", 36868, 2), new ExifTag("ComponentsConfiguration", 37121, 7), new ExifTag("ShutterSpeedValue", 37377, 10), new ExifTag("ApertureValue", 37378, 5), new ExifTag("BrightnessValue", 37379, 10), new ExifTag("ExposureBiasValue", 37380, 10), new ExifTag("MaxApertureValue", 37381, 5), new ExifTag("MeteringMode", 37383, 3), new ExifTag("LightSource", 37384, 3), new ExifTag("Flash", 37385, 3), new ExifTag("FocalLength", 37386, 5), new ExifTag("SubSecTime", 37520, 2), new ExifTag("SubSecTimeOriginal", 37521, 2), new ExifTag("SubSecTimeDigitized", 37522, 2), new ExifTag("FlashpixVersion", 40960, 7), new ExifTag("ColorSpace", 40961, 3), new ExifTag("PixelXDimension", 40962, 3, 4), new ExifTag("PixelYDimension", 40963, 3, 4), new ExifTag("InteroperabilityIFDPointer", 40965, 4), new ExifTag("FocalPlaneResolutionUnit", 41488, 3), new ExifTag("SensingMethod", 41495, 3), new ExifTag("FileSource", 41728, 7), new ExifTag("SceneType", 41729, 7), new ExifTag("CustomRendered", 41985, 3), new ExifTag("ExposureMode", 41986, 3), new ExifTag("WhiteBalance", 41987, 3), new ExifTag("SceneCaptureType", 41990, 3), new ExifTag("Contrast", 41992, 3), new ExifTag("Saturation", 41993, 3), new ExifTag("Sharpness", 41994, 3) };
        final ExifTag[] array3 = IFD_GPS_TAGS = new ExifTag[] { new ExifTag("GPSVersionID", 0, 1), new ExifTag("GPSLatitudeRef", 1, 2), new ExifTag("GPSLatitude", 2, 5, 10), new ExifTag("GPSLongitudeRef", 3, 2), new ExifTag("GPSLongitude", 4, 5, 10), new ExifTag("GPSAltitudeRef", 5, 1), new ExifTag("GPSAltitude", 6, 5), new ExifTag("GPSTimeStamp", 7, 5), new ExifTag("GPSSpeedRef", 12, 2), new ExifTag("GPSTrackRef", 14, 2), new ExifTag("GPSImgDirectionRef", 16, 2), new ExifTag("GPSDestBearingRef", 23, 2), new ExifTag("GPSDestDistanceRef", 25, 2) };
        EXIF_POINTER_TAGS = new ExifTag[] { new ExifTag("SubIFDPointer", 330, 4), new ExifTag("ExifIFDPointer", 34665, 4), new ExifTag("GPSInfoIFDPointer", 34853, 4), new ExifTag("InteroperabilityIFDPointer", 40965, 4) };
        EXIF_TAGS = new ExifTag[][] { array, array2, array3, IFD_INTEROPERABILITY_TAGS = new ExifTag[] { new ExifTag("InteroperabilityIndex", 1, 2) } };
        sTagSetForCompatibility = new HashSet<String>(Arrays.asList("FNumber", "ExposureTime", "GPSTimeStamp"));
    }
    
    ExifData(final ByteOrder mByteOrder, final List<Map<String, ExifAttribute>> mAttributes) {
        Preconditions.checkState(mAttributes.size() == ExifData.EXIF_TAGS.length, "Malformed attributes list. Number of IFDs mismatch.");
        this.mByteOrder = mByteOrder;
        this.mAttributes = mAttributes;
    }
    
    public static Builder builderForDevice() {
        return new Builder(ByteOrder.BIG_ENDIAN).setAttribute("Orientation", String.valueOf(1)).setAttribute("XResolution", "72/1").setAttribute("YResolution", "72/1").setAttribute("ResolutionUnit", String.valueOf(2)).setAttribute("YCbCrPositioning", String.valueOf(1)).setAttribute("Make", Build.MANUFACTURER).setAttribute("Model", Build.MODEL);
    }
    
    public static ExifData create(final ImageProxy imageProxy, final int orientationDegrees) {
        final Builder builderForDevice = builderForDevice();
        imageProxy.getImageInfo().populateExifData(builderForDevice);
        builderForDevice.setOrientationDegrees(orientationDegrees);
        return builderForDevice.setImageWidth(imageProxy.getWidth()).setImageHeight(imageProxy.getHeight()).build();
    }
    
    private ExifAttribute getExifAttribute(final String anObject) {
        String s = anObject;
        if ("ISOSpeedRatings".equals(anObject)) {
            s = "PhotographicSensitivity";
        }
        for (int i = 0; i < ExifData.EXIF_TAGS.length; ++i) {
            final ExifAttribute exifAttribute = (ExifAttribute)this.mAttributes.get(i).get(s);
            if (exifAttribute != null) {
                return exifAttribute;
            }
        }
        return null;
    }
    
    public String getAttribute(String string) {
        final ExifAttribute exifAttribute = this.getExifAttribute(string);
        Label_0247: {
            if (exifAttribute == null) {
                break Label_0247;
            }
            if (!ExifData.sTagSetForCompatibility.contains(string)) {
                return exifAttribute.getStringValue(this.mByteOrder);
            }
            if (string.equals("GPSTimeStamp")) {
                if (exifAttribute.format != 5 && exifAttribute.format != 10) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("GPS Timestamp format is not rational. format=");
                    sb.append(exifAttribute.format);
                    Logger.w("ExifData", sb.toString());
                    return null;
                }
                final LongRational[] a = (LongRational[])exifAttribute.getValue(this.mByteOrder);
                if (a != null && a.length == 3) {
                    return String.format(Locale.US, "%02d:%02d:%02d", (int)(a[0].getNumerator() / (float)a[0].getDenominator()), (int)(a[1].getNumerator() / (float)a[1].getDenominator()), (int)(a[2].getNumerator() / (float)a[2].getDenominator()));
                }
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("Invalid GPS Timestamp array. array=");
                sb2.append(Arrays.toString(a));
                Logger.w("ExifData", sb2.toString());
                return null;
            }
            try {
                string = Double.toString(exifAttribute.getDoubleValue(this.mByteOrder));
                return string;
                return null;
            }
            catch (final NumberFormatException ex) {
                return null;
            }
        }
    }
    
    Map<String, ExifAttribute> getAttributes(final int i) {
        final int length = ExifData.EXIF_TAGS.length;
        final StringBuilder sb = new StringBuilder();
        sb.append("Invalid IFD index: ");
        sb.append(i);
        sb.append(". Index should be between [0, EXIF_TAGS.length] ");
        Preconditions.checkArgumentInRange(i, 0, length, sb.toString());
        return this.mAttributes.get(i);
    }
    
    public ByteOrder getByteOrder() {
        return this.mByteOrder;
    }
    
    public static final class Builder
    {
        private static final Pattern DATETIME_PRIMARY_FORMAT_PATTERN;
        private static final Pattern DATETIME_SECONDARY_FORMAT_PATTERN;
        private static final int DATETIME_VALUE_STRING_LENGTH = 19;
        private static final Pattern GPS_TIMESTAMP_PATTERN;
        static final List<HashMap<String, ExifTag>> sExifTagMapsForWriting;
        final List<Map<String, ExifAttribute>> mAttributes;
        private final ByteOrder mByteOrder;
        
        static {
            GPS_TIMESTAMP_PATTERN = Pattern.compile("^(\\d{2}):(\\d{2}):(\\d{2})$");
            DATETIME_PRIMARY_FORMAT_PATTERN = Pattern.compile("^(\\d{4}):(\\d{2}):(\\d{2})\\s(\\d{2}):(\\d{2}):(\\d{2})$");
            DATETIME_SECONDARY_FORMAT_PATTERN = Pattern.compile("^(\\d{4})-(\\d{2})-(\\d{2})\\s(\\d{2}):(\\d{2}):(\\d{2})$");
            sExifTagMapsForWriting = Collections.list((Enumeration<Object>)new Enumeration<HashMap<String, ExifTag>>() {
                int mIfdIndex = 0;
                
                @Override
                public boolean hasMoreElements() {
                    return this.mIfdIndex < ExifData.EXIF_TAGS.length;
                }
                
                @Override
                public HashMap<String, ExifTag> nextElement() {
                    final HashMap hashMap = new HashMap();
                    for (final ExifTag value : ExifData.EXIF_TAGS[this.mIfdIndex]) {
                        hashMap.put(value.name, value);
                    }
                    ++this.mIfdIndex;
                    return hashMap;
                }
            });
        }
        
        Builder(final ByteOrder mByteOrder) {
            this.mAttributes = (List<Map<String, ExifAttribute>>)Collections.list((Enumeration<Object>)new Enumeration<Map<String, ExifAttribute>>() {
                int mIfdIndex = 0;
                final Builder this$0;
                
                @Override
                public boolean hasMoreElements() {
                    return this.mIfdIndex < ExifData.EXIF_TAGS.length;
                }
                
                @Override
                public Map<String, ExifAttribute> nextElement() {
                    ++this.mIfdIndex;
                    return new HashMap<String, ExifAttribute>();
                }
            });
            this.mByteOrder = mByteOrder;
        }
        
        private static Pair<Integer, Integer> guessDataFormat(final String s) {
            final boolean contains = s.contains(",");
            int i = 1;
            final Integer value = 2;
            final Integer value2 = -1;
            if (contains) {
                final String[] split = s.split(",", -1);
                Pair guessDataFormat;
                final Pair<Integer, Integer> pair = (Pair<Integer, Integer>)(guessDataFormat = guessDataFormat(split[0]));
                if ((int)pair.first == 2) {
                    return pair;
                }
                while (i < split.length) {
                    final Pair<Integer, Integer> guessDataFormat2 = guessDataFormat(split[i]);
                    int intValue;
                    if (!((Integer)guessDataFormat2.first).equals(guessDataFormat.first) && !((Integer)guessDataFormat2.second).equals(guessDataFormat.first)) {
                        intValue = -1;
                    }
                    else {
                        intValue = (int)guessDataFormat.first;
                    }
                    int intValue2;
                    if ((int)guessDataFormat.second != -1 && (((Integer)guessDataFormat2.first).equals(guessDataFormat.second) || ((Integer)guessDataFormat2.second).equals(guessDataFormat.second))) {
                        intValue2 = (int)guessDataFormat.second;
                    }
                    else {
                        intValue2 = -1;
                    }
                    if (intValue == -1 && intValue2 == -1) {
                        return (Pair<Integer, Integer>)new Pair((Object)value, (Object)value2);
                    }
                    if (intValue == -1) {
                        guessDataFormat = new Pair((Object)intValue2, (Object)value2);
                    }
                    else if (intValue2 == -1) {
                        guessDataFormat = new Pair((Object)intValue, (Object)value2);
                    }
                    ++i;
                }
                return (Pair<Integer, Integer>)guessDataFormat;
            }
            else {
                Label_0413: {
                    if (!s.contains("/")) {
                        break Label_0413;
                    }
                    final String[] split2 = s.split("/", -1);
                    Label_0401: {
                        if (split2.length != 2) {
                            break Label_0401;
                        }
                        try {
                            final long n = (long)Double.parseDouble(split2[0]);
                            final long n2 = (long)Double.parseDouble(split2[1]);
                            if (n < 0L || n2 < 0L) {
                                return (Pair<Integer, Integer>)new Pair((Object)10, (Object)value2);
                            }
                            if (n <= 2147483647L && n2 <= 2147483647L) {
                                return (Pair<Integer, Integer>)new Pair((Object)10, (Object)5);
                            }
                            return (Pair<Integer, Integer>)new Pair((Object)5, (Object)value2);
                            return (Pair<Integer, Integer>)new Pair((Object)value, (Object)value2);
                            try {
                                final long long1 = Long.parseLong(s);
                                final long n3 = lcmp(long1, 0L);
                                if (n3 >= 0 && long1 <= 65535L) {
                                    return (Pair<Integer, Integer>)new Pair((Object)3, (Object)4);
                                }
                                if (n3 < 0) {
                                    return (Pair<Integer, Integer>)new Pair((Object)9, (Object)value2);
                                }
                                return (Pair<Integer, Integer>)new Pair((Object)4, (Object)value2);
                            }
                            catch (final NumberFormatException ex) {
                                try {
                                    Double.parseDouble(s);
                                    return (Pair<Integer, Integer>)new Pair((Object)12, (Object)value2);
                                }
                                catch (final NumberFormatException ex2) {
                                    return (Pair<Integer, Integer>)new Pair((Object)value, (Object)value2);
                                }
                            }
                        }
                        catch (final NumberFormatException ex3) {
                            return (Pair<Integer, Integer>)new Pair((Object)value, (Object)value2);
                        }
                    }
                }
            }
        }
        
        private void setAttributeIfMissing(final String s, final String s2, final List<Map<String, ExifAttribute>> list) {
            final Iterator<Map<String, ExifAttribute>> iterator = list.iterator();
            while (iterator.hasNext()) {
                if (iterator.next().containsKey(s)) {
                    return;
                }
            }
            this.setAttributeInternal(s, s2, list);
        }
        
        private void setAttributeInternal(String anObject, String regex, final List<Map<String, ExifAttribute>> list) {
            final String str = regex;
            String replaceAll = null;
            Label_0162: {
                if (!"DateTime".equals(anObject) && !"DateTimeOriginal".equals(anObject)) {
                    replaceAll = str;
                    if (!"DateTimeDigitized".equals(anObject)) {
                        break Label_0162;
                    }
                }
                if ((replaceAll = str) != null) {
                    final boolean find = Builder.DATETIME_PRIMARY_FORMAT_PATTERN.matcher(str).find();
                    final boolean find2 = Builder.DATETIME_SECONDARY_FORMAT_PATTERN.matcher(str).find();
                    if (regex.length() != 19 || (!find && !find2)) {
                        final StringBuilder sb = new StringBuilder();
                        sb.append("Invalid value for ");
                        sb.append(anObject);
                        sb.append(" : ");
                        sb.append(str);
                        Logger.w("ExifData", sb.toString());
                        return;
                    }
                    replaceAll = str;
                    if (find2) {
                        replaceAll = str.replaceAll("-", ":");
                    }
                }
            }
            String s = anObject;
            if ("ISOSpeedRatings".equals(anObject)) {
                s = "PhotographicSensitivity";
            }
            if ((anObject = replaceAll) != null) {
                anObject = replaceAll;
                if (ExifData.sTagSetForCompatibility.contains(s)) {
                    if (s.equals("GPSTimeStamp")) {
                        final Matcher matcher = Builder.GPS_TIMESTAMP_PATTERN.matcher(replaceAll);
                        if (!matcher.find()) {
                            final StringBuilder sb2 = new StringBuilder();
                            sb2.append("Invalid value for ");
                            sb2.append(s);
                            sb2.append(" : ");
                            sb2.append(replaceAll);
                            Logger.w("ExifData", sb2.toString());
                            return;
                        }
                        final StringBuilder sb3 = new StringBuilder();
                        sb3.append(Integer.parseInt(Preconditions.checkNotNull(matcher.group(1))));
                        sb3.append("/1,");
                        sb3.append(Integer.parseInt(Preconditions.checkNotNull(matcher.group(2))));
                        sb3.append("/1,");
                        sb3.append(Integer.parseInt(Preconditions.checkNotNull(matcher.group(3))));
                        sb3.append("/1");
                        anObject = sb3.toString();
                    }
                    else {
                        try {
                            anObject = new LongRational(Double.parseDouble(replaceAll)).toString();
                        }
                        catch (final NumberFormatException ex) {
                            final StringBuilder sb4 = new StringBuilder();
                            sb4.append("Invalid value for ");
                            sb4.append(s);
                            sb4.append(" : ");
                            sb4.append(replaceAll);
                            Logger.w("ExifData", sb4.toString(), ex);
                            return;
                        }
                    }
                }
            }
            int i = 0;
            Builder builder = this;
            while (i < ExifData.EXIF_TAGS.length) {
                final ExifTag exifTag = (ExifTag)Builder.sExifTagMapsForWriting.get(i).get(s);
                Builder builder2 = builder;
                Label_1335: {
                    Label_1332: {
                        if (exifTag != null) {
                            if (anObject == null) {
                                list.get(i).remove(s);
                                builder2 = builder;
                            }
                            else {
                                final Pair<Integer, Integer> guessDataFormat = guessDataFormat(anObject);
                                int n;
                                if (exifTag.primaryFormat != (int)guessDataFormat.first && exifTag.primaryFormat != (int)guessDataFormat.second) {
                                    if (exifTag.secondaryFormat != -1 && (exifTag.secondaryFormat == (int)guessDataFormat.first || exifTag.secondaryFormat == (int)guessDataFormat.second)) {
                                        n = exifTag.secondaryFormat;
                                    }
                                    else {
                                        if (exifTag.primaryFormat != 1 && exifTag.primaryFormat != 7) {
                                            builder2 = builder;
                                            if (exifTag.primaryFormat != 2) {
                                                break Label_1332;
                                            }
                                        }
                                        n = exifTag.primaryFormat;
                                    }
                                }
                                else {
                                    n = exifTag.primaryFormat;
                                }
                                final String s2 = "/";
                                switch (n) {
                                    default: {
                                        builder2 = builder;
                                        break;
                                    }
                                    case 12: {
                                        final String[] split = anObject.split(",", -1);
                                        final double[] array = new double[split.length];
                                        for (int j = 0; j < split.length; ++j) {
                                            array[j] = Double.parseDouble(split[j]);
                                        }
                                        list.get(i).put(s, ExifAttribute.createDouble(array, builder.mByteOrder));
                                        builder2 = builder;
                                        break;
                                    }
                                    case 10: {
                                        final String[] split2 = anObject.split(",", -1);
                                        final LongRational[] array2 = new LongRational[split2.length];
                                        int k = 0;
                                        regex = s2;
                                        while (k < split2.length) {
                                            final String[] split3 = split2[k].split(regex, -1);
                                            array2[k] = new LongRational((long)Double.parseDouble(split3[0]), (long)Double.parseDouble(split3[1]));
                                            ++k;
                                        }
                                        final Map map = list.get(i);
                                        builder2 = this;
                                        map.put(s, ExifAttribute.createSRational(array2, builder2.mByteOrder));
                                        break;
                                    }
                                    case 9: {
                                        final String[] split4 = anObject.split(",", -1);
                                        final int[] array3 = new int[split4.length];
                                        for (int l = 0; l < split4.length; ++l) {
                                            array3[l] = Integer.parseInt(split4[l]);
                                        }
                                        list.get(i).put(s, ExifAttribute.createSLong(array3, builder.mByteOrder));
                                        builder2 = builder;
                                        break;
                                    }
                                    case 5: {
                                        final String regex2 = "/";
                                        final String[] split5 = anObject.split(",", -1);
                                        final LongRational[] array4 = new LongRational[split5.length];
                                        for (int n2 = 0; n2 < split5.length; ++n2) {
                                            final String[] split6 = split5[n2].split(regex2, -1);
                                            array4[n2] = new LongRational((long)Double.parseDouble(split6[0]), (long)Double.parseDouble(split6[1]));
                                        }
                                        list.get(i).put(s, ExifAttribute.createURational(array4, builder.mByteOrder));
                                        break Label_1335;
                                    }
                                    case 4: {
                                        final String[] split7 = anObject.split(",", -1);
                                        final long[] array5 = new long[split7.length];
                                        for (int n3 = 0; n3 < split7.length; ++n3) {
                                            array5[n3] = Long.parseLong(split7[n3]);
                                        }
                                        list.get(i).put(s, ExifAttribute.createULong(array5, builder.mByteOrder));
                                        break Label_1335;
                                    }
                                    case 3: {
                                        final String[] split8 = anObject.split(",", -1);
                                        final int[] array6 = new int[split8.length];
                                        for (int n4 = 0; n4 < split8.length; ++n4) {
                                            array6[n4] = Integer.parseInt(split8[n4]);
                                        }
                                        list.get(i).put(s, ExifAttribute.createUShort(array6, builder.mByteOrder));
                                        break Label_1335;
                                    }
                                    case 2:
                                    case 7: {
                                        list.get(i).put(s, ExifAttribute.createString(anObject));
                                        break Label_1335;
                                    }
                                    case 1: {
                                        list.get(i).put(s, ExifAttribute.createByte(anObject));
                                        break Label_1335;
                                    }
                                }
                            }
                        }
                    }
                    builder = builder2;
                }
                ++i;
            }
        }
        
        public ExifData build() {
            final ArrayList<Object> list = Collections.list((Enumeration<Object>)new Enumeration<Map<String, ExifAttribute>>(this) {
                final Enumeration<Map<String, ExifAttribute>> mMapEnumeration = Collections.enumeration(this$0.mAttributes);
                final Builder this$0;
                
                @Override
                public boolean hasMoreElements() {
                    return this.mMapEnumeration.hasMoreElements();
                }
                
                @Override
                public Map<String, ExifAttribute> nextElement() {
                    return new HashMap<String, ExifAttribute>(this.mMapEnumeration.nextElement());
                }
            });
            if (!((Map)list.get(1)).isEmpty()) {
                this.setAttributeIfMissing("ExposureProgram", String.valueOf(0), (List<Map<String, ExifAttribute>>)list);
                this.setAttributeIfMissing("ExifVersion", "0230", (List<Map<String, ExifAttribute>>)list);
                this.setAttributeIfMissing("ComponentsConfiguration", "1,2,3,0", (List<Map<String, ExifAttribute>>)list);
                this.setAttributeIfMissing("MeteringMode", String.valueOf(0), (List<Map<String, ExifAttribute>>)list);
                this.setAttributeIfMissing("LightSource", String.valueOf(0), (List<Map<String, ExifAttribute>>)list);
                this.setAttributeIfMissing("FlashpixVersion", "0100", (List<Map<String, ExifAttribute>>)list);
                this.setAttributeIfMissing("FocalPlaneResolutionUnit", String.valueOf(2), (List<Map<String, ExifAttribute>>)list);
                this.setAttributeIfMissing("FileSource", String.valueOf(3), (List<Map<String, ExifAttribute>>)list);
                this.setAttributeIfMissing("SceneType", String.valueOf(1), (List<Map<String, ExifAttribute>>)list);
                this.setAttributeIfMissing("CustomRendered", String.valueOf(0), (List<Map<String, ExifAttribute>>)list);
                this.setAttributeIfMissing("SceneCaptureType", String.valueOf(0), (List<Map<String, ExifAttribute>>)list);
                this.setAttributeIfMissing("Contrast", String.valueOf(0), (List<Map<String, ExifAttribute>>)list);
                this.setAttributeIfMissing("Saturation", String.valueOf(0), (List<Map<String, ExifAttribute>>)list);
                this.setAttributeIfMissing("Sharpness", String.valueOf(0), (List<Map<String, ExifAttribute>>)list);
            }
            if (!((Map)list.get(2)).isEmpty()) {
                this.setAttributeIfMissing("GPSVersionID", "2300", (List<Map<String, ExifAttribute>>)list);
                this.setAttributeIfMissing("GPSSpeedRef", "K", (List<Map<String, ExifAttribute>>)list);
                this.setAttributeIfMissing("GPSTrackRef", "T", (List<Map<String, ExifAttribute>>)list);
                this.setAttributeIfMissing("GPSImgDirectionRef", "T", (List<Map<String, ExifAttribute>>)list);
                this.setAttributeIfMissing("GPSDestBearingRef", "T", (List<Map<String, ExifAttribute>>)list);
                this.setAttributeIfMissing("GPSDestDistanceRef", "K", (List<Map<String, ExifAttribute>>)list);
            }
            return new ExifData(this.mByteOrder, (List<Map<String, ExifAttribute>>)list);
        }
        
        public Builder removeAttribute(final String s) {
            this.setAttributeInternal(s, null, this.mAttributes);
            return this;
        }
        
        public Builder setAttribute(final String s, final String s2) {
            this.setAttributeInternal(s, s2, this.mAttributes);
            return this;
        }
        
        public Builder setExposureTimeNanos(final long n) {
            return this.setAttribute("ExposureTime", String.valueOf(n / (double)TimeUnit.SECONDS.toNanos(1L)));
        }
        
        public Builder setFlashState(final CameraCaptureMetaData.FlashState obj) {
            if (obj == CameraCaptureMetaData.FlashState.UNKNOWN) {
                return this;
            }
            final int n = ExifData$1.$SwitchMap$androidx$camera$core$impl$CameraCaptureMetaData$FlashState[obj.ordinal()];
            int i;
            if (n != 1) {
                if (n != 2) {
                    if (n != 3) {
                        final StringBuilder sb = new StringBuilder();
                        sb.append("Unknown flash state: ");
                        sb.append(obj);
                        Logger.w("ExifData", sb.toString());
                        return this;
                    }
                    i = 1;
                }
                else {
                    i = 32;
                }
            }
            else {
                i = 0;
            }
            if ((i & 0x1) == 0x1) {
                this.setAttribute("LightSource", String.valueOf(4));
            }
            return this.setAttribute("Flash", String.valueOf(i));
        }
        
        public Builder setFocalLength(final float n) {
            return this.setAttribute("FocalLength", new LongRational((long)(n * 1000.0f), 1000L).toString());
        }
        
        public Builder setImageHeight(final int i) {
            return this.setAttribute("ImageLength", String.valueOf(i));
        }
        
        public Builder setImageWidth(final int i) {
            return this.setAttribute("ImageWidth", String.valueOf(i));
        }
        
        public Builder setIso(final int b) {
            return this.setAttribute("SensitivityType", String.valueOf(3)).setAttribute("PhotographicSensitivity", String.valueOf(Math.min(65535, b)));
        }
        
        public Builder setLensFNumber(final float f) {
            return this.setAttribute("FNumber", String.valueOf(f));
        }
        
        public Builder setOrientationDegrees(int n) {
            if (n != 0) {
                if (n != 90) {
                    if (n != 180) {
                        if (n != 270) {
                            final StringBuilder sb = new StringBuilder();
                            sb.append("Unexpected orientation value: ");
                            sb.append(n);
                            sb.append(". Must be one of 0, 90, 180, 270.");
                            Logger.w("ExifData", sb.toString());
                            n = 0;
                        }
                        else {
                            n = 8;
                        }
                    }
                    else {
                        n = 3;
                    }
                }
                else {
                    n = 6;
                }
            }
            else {
                n = 1;
            }
            return this.setAttribute("Orientation", String.valueOf(n));
        }
        
        public Builder setWhiteBalanceMode(final WhiteBalanceMode whiteBalanceMode) {
            final int n = ExifData$1.$SwitchMap$androidx$camera$core$impl$utils$ExifData$WhiteBalanceMode[whiteBalanceMode.ordinal()];
            String s;
            if (n != 1) {
                if (n != 2) {
                    s = null;
                }
                else {
                    s = String.valueOf(1);
                }
            }
            else {
                s = String.valueOf(0);
            }
            return this.setAttribute("WhiteBalance", s);
        }
    }
    
    public enum WhiteBalanceMode
    {
        private static final WhiteBalanceMode[] $VALUES;
        
        AUTO, 
        MANUAL;
    }
}

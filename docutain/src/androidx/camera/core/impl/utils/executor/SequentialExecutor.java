// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl.utils.executor;

import androidx.camera.core.Logger;
import java.util.concurrent.RejectedExecutionException;
import androidx.core.util.Preconditions;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.concurrent.Executor;

final class SequentialExecutor implements Executor
{
    private static final String TAG = "SequentialExecutor";
    private final Executor mExecutor;
    final Deque<Runnable> mQueue;
    private final QueueWorker mWorker;
    long mWorkerRunCount;
    WorkerRunningState mWorkerRunningState;
    
    SequentialExecutor(final Executor executor) {
        this.mQueue = new ArrayDeque<Runnable>();
        this.mWorker = new QueueWorker();
        this.mWorkerRunningState = WorkerRunningState.IDLE;
        this.mWorkerRunCount = 0L;
        this.mExecutor = Preconditions.checkNotNull(executor);
    }
    
    @Override
    public void execute(final Runnable error) {
        Preconditions.checkNotNull(error);
        synchronized (this.mQueue) {
            if (this.mWorkerRunningState != WorkerRunningState.RUNNING) {
                if (this.mWorkerRunningState != WorkerRunningState.QUEUED) {
                    final long mWorkerRunCount = this.mWorkerRunCount;
                    final Runnable runnable = new Runnable(this, error) {
                        final SequentialExecutor this$0;
                        final Runnable val$task;
                        
                        @Override
                        public void run() {
                            this.val$task.run();
                        }
                    };
                    this.mQueue.add(runnable);
                    this.mWorkerRunningState = WorkerRunningState.QUEUING;
                    monitorexit(this.mQueue);
                    final int n = 1;
                    boolean b = true;
                    try {
                        this.mExecutor.execute(this.mWorker);
                        if (this.mWorkerRunningState == WorkerRunningState.QUEUING) {
                            b = false;
                        }
                        if (b) {
                            return;
                        }
                        synchronized (this.mQueue) {
                            if (this.mWorkerRunCount == mWorkerRunCount && this.mWorkerRunningState == WorkerRunningState.QUEUING) {
                                this.mWorkerRunningState = WorkerRunningState.QUEUED;
                            }
                            return;
                        }
                    }
                    catch (final Error error) {}
                    catch (final RuntimeException ex) {}
                    final Deque<Runnable> mQueue = this.mQueue;
                    synchronized (this.mQueue) {
                        if ((this.mWorkerRunningState == WorkerRunningState.IDLE || this.mWorkerRunningState == WorkerRunningState.QUEUING) && this.mQueue.removeLastOccurrence(runnable)) {
                            final int n2 = n;
                        }
                        else {
                            final int n2 = 0;
                        }
                        int n2;
                        if (error instanceof RejectedExecutionException && n2 == 0) {
                            return;
                        }
                        throw error;
                    }
                }
            }
            this.mQueue.add((Runnable)error);
        }
    }
    
    final class QueueWorker implements Runnable
    {
        final SequentialExecutor this$0;
        
        QueueWorker(final SequentialExecutor this$0) {
            this.this$0 = this$0;
        }
        
        private void workOnQueue() {
            int n = 0;
            int n2 = 0;
            while (true) {
                int n3 = n2;
                try {
                    Object mQueue = this.this$0.mQueue;
                    n3 = n2;
                    monitorenter(mQueue);
                    int n4 = n;
                    Label_0085: {
                        if (n != 0) {
                            break Label_0085;
                        }
                        try {
                            if (this.this$0.mWorkerRunningState == WorkerRunningState.RUNNING) {
                                monitorexit(mQueue);
                                return;
                            }
                            final SequentialExecutor this$0 = this.this$0;
                            ++this$0.mWorkerRunCount;
                            this.this$0.mWorkerRunningState = WorkerRunningState.RUNNING;
                            n4 = 1;
                            final Runnable obj = this.this$0.mQueue.poll();
                            if (obj == null) {
                                this.this$0.mWorkerRunningState = WorkerRunningState.IDLE;
                                monitorexit(mQueue);
                                return;
                            }
                            monitorexit(mQueue);
                            n3 = n2;
                            n2 = (n3 = (n2 | (Thread.interrupted() ? 1 : 0)));
                            try {
                                obj.run();
                                n = n4;
                            }
                            catch (final RuntimeException ex) {
                                n3 = n2;
                                n3 = n2;
                                mQueue = new StringBuilder();
                                n3 = n2;
                                ((StringBuilder)mQueue).append("Exception while executing runnable ");
                                n3 = n2;
                                ((StringBuilder)mQueue).append(obj);
                                n3 = n2;
                                Logger.e("SequentialExecutor", ((StringBuilder)mQueue).toString(), ex);
                                n = n4;
                            }
                            continue;
                        }
                        finally {
                            monitorexit(mQueue);
                            n3 = n2;
                        }
                    }
                }
                finally {
                    if (n3 != 0) {
                        Thread.currentThread().interrupt();
                    }
                }
            }
        }
        
        @Override
        public void run() {
            try {
                this.workOnQueue();
            }
            catch (final Error error) {
                synchronized (this.this$0.mQueue) {
                    this.this$0.mWorkerRunningState = WorkerRunningState.IDLE;
                    monitorexit(this.this$0.mQueue);
                    throw error;
                }
            }
        }
    }
    
    enum WorkerRunningState
    {
        private static final WorkerRunningState[] $VALUES;
        
        IDLE, 
        QUEUED, 
        QUEUING, 
        RUNNING;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl.utils.executor;

import android.os.Handler;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.Executor;

public final class CameraXExecutors
{
    private CameraXExecutors() {
    }
    
    public static Executor directExecutor() {
        return DirectExecutor.getInstance();
    }
    
    public static Executor highPriorityExecutor() {
        return HighPriorityExecutor.getInstance();
    }
    
    public static Executor ioExecutor() {
        return IoExecutor.getInstance();
    }
    
    public static boolean isSequentialExecutor(final Executor executor) {
        return executor instanceof SequentialExecutor;
    }
    
    public static ScheduledExecutorService mainThreadExecutor() {
        return MainThreadExecutor.getInstance();
    }
    
    public static ScheduledExecutorService myLooperExecutor() {
        return HandlerScheduledExecutorService.currentThreadExecutor();
    }
    
    public static ScheduledExecutorService newHandlerExecutor(final Handler handler) {
        return new HandlerScheduledExecutorService(handler);
    }
    
    public static Executor newSequentialExecutor(final Executor executor) {
        return new SequentialExecutor(executor);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl.utils.executor;

import java.util.concurrent.Executors;
import java.util.Locale;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executor;

final class IoExecutor implements Executor
{
    private static volatile Executor sExecutor;
    private final ExecutorService mIoService;
    
    IoExecutor() {
        this.mIoService = Executors.newFixedThreadPool(2, new ThreadFactory() {
            private static final String THREAD_NAME_STEM = "CameraX-camerax_io_%d";
            private final AtomicInteger mThreadId = new AtomicInteger(0);
            final IoExecutor this$0;
            
            @Override
            public Thread newThread(final Runnable target) {
                final Thread thread = new Thread(target);
                thread.setName(String.format(Locale.US, "CameraX-camerax_io_%d", this.mThreadId.getAndIncrement()));
                return thread;
            }
        });
    }
    
    static Executor getInstance() {
        if (IoExecutor.sExecutor != null) {
            return IoExecutor.sExecutor;
        }
        synchronized (IoExecutor.class) {
            if (IoExecutor.sExecutor == null) {
                IoExecutor.sExecutor = new IoExecutor();
            }
            return IoExecutor.sExecutor;
        }
    }
    
    @Override
    public void execute(final Runnable runnable) {
        this.mIoService.execute(runnable);
    }
}

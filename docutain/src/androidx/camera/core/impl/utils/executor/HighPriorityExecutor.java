// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl.utils.executor;

import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executor;

final class HighPriorityExecutor implements Executor
{
    private static volatile Executor sExecutor;
    private final ExecutorService mHighPriorityService;
    
    HighPriorityExecutor() {
        this.mHighPriorityService = Executors.newSingleThreadExecutor(new ThreadFactory() {
            private static final String THREAD_NAME = "CameraX-camerax_high_priority";
            final HighPriorityExecutor this$0;
            
            @Override
            public Thread newThread(final Runnable target) {
                final Thread thread = new Thread(target);
                thread.setPriority(10);
                thread.setName("CameraX-camerax_high_priority");
                return thread;
            }
        });
    }
    
    static Executor getInstance() {
        if (HighPriorityExecutor.sExecutor != null) {
            return HighPriorityExecutor.sExecutor;
        }
        synchronized (HighPriorityExecutor.class) {
            if (HighPriorityExecutor.sExecutor == null) {
                HighPriorityExecutor.sExecutor = new HighPriorityExecutor();
            }
            return HighPriorityExecutor.sExecutor;
        }
    }
    
    @Override
    public void execute(final Runnable runnable) {
        this.mHighPriorityService.execute(runnable);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl.utils.executor;

import android.os.Handler;
import android.os.Looper;
import java.util.concurrent.ScheduledExecutorService;

final class MainThreadExecutor
{
    private static volatile ScheduledExecutorService sInstance;
    
    private MainThreadExecutor() {
    }
    
    static ScheduledExecutorService getInstance() {
        if (MainThreadExecutor.sInstance != null) {
            return MainThreadExecutor.sInstance;
        }
        synchronized (MainThreadExecutor.class) {
            if (MainThreadExecutor.sInstance == null) {
                MainThreadExecutor.sInstance = new HandlerScheduledExecutorService(new Handler(Looper.getMainLooper()));
            }
            return MainThreadExecutor.sInstance;
        }
    }
}

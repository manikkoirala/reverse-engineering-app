// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl.utils;

class ExifTag
{
    public final String name;
    public final int number;
    public final int primaryFormat;
    public final int secondaryFormat;
    
    ExifTag(final String name, final int number, final int primaryFormat) {
        this.name = name;
        this.number = number;
        this.primaryFormat = primaryFormat;
        this.secondaryFormat = -1;
    }
    
    ExifTag(final String name, final int number, final int primaryFormat, final int secondaryFormat) {
        this.name = name;
        this.number = number;
        this.primaryFormat = primaryFormat;
        this.secondaryFormat = secondaryFormat;
    }
    
    boolean isFormatCompatible(final int n) {
        final int primaryFormat = this.primaryFormat;
        boolean b2;
        final boolean b = b2 = true;
        if (primaryFormat != 7) {
            if (n == 7) {
                b2 = b;
            }
            else {
                b2 = b;
                if (primaryFormat != n) {
                    final int secondaryFormat = this.secondaryFormat;
                    if (secondaryFormat == n) {
                        b2 = b;
                    }
                    else {
                        if ((primaryFormat == 4 || secondaryFormat == 4) && n == 3) {
                            return true;
                        }
                        if ((primaryFormat == 9 || secondaryFormat == 9) && n == 8) {
                            return true;
                        }
                        b2 = ((primaryFormat == 12 || secondaryFormat == 12) && n == 11 && b);
                    }
                }
            }
        }
        return b2;
    }
}

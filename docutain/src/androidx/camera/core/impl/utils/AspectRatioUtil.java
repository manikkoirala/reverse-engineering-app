// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl.utils;

import java.util.Comparator;
import androidx.core.util.Preconditions;
import androidx.camera.core.internal.utils.SizeUtil;
import android.util.Size;
import android.util.Rational;

public final class AspectRatioUtil
{
    private static final int ALIGN16 = 16;
    public static final Rational ASPECT_RATIO_16_9;
    public static final Rational ASPECT_RATIO_3_4;
    public static final Rational ASPECT_RATIO_4_3;
    public static final Rational ASPECT_RATIO_9_16;
    
    static {
        ASPECT_RATIO_4_3 = new Rational(4, 3);
        ASPECT_RATIO_3_4 = new Rational(3, 4);
        ASPECT_RATIO_16_9 = new Rational(16, 9);
        ASPECT_RATIO_9_16 = new Rational(9, 16);
    }
    
    private AspectRatioUtil() {
    }
    
    public static boolean hasMatchingAspectRatio(final Size size, final Rational rational) {
        boolean possibleMod16FromAspectRatio = false;
        if (rational != null) {
            if (rational.equals((Object)new Rational(size.getWidth(), size.getHeight()))) {
                possibleMod16FromAspectRatio = true;
            }
            else if (SizeUtil.getArea(size) >= SizeUtil.getArea(SizeUtil.RESOLUTION_VGA)) {
                possibleMod16FromAspectRatio = isPossibleMod16FromAspectRatio(size, rational);
            }
        }
        return possibleMod16FromAspectRatio;
    }
    
    private static boolean isPossibleMod16FromAspectRatio(final Size size, final Rational rational) {
        final int width = size.getWidth();
        final int height = size.getHeight();
        final Rational rational2 = new Rational(rational.getDenominator(), rational.getNumerator());
        final int n = width % 16;
        boolean b = false;
        if (n == 0 && height % 16 == 0) {
            if (ratioIntersectsMod16Segment(Math.max(0, height - 16), width, rational) || ratioIntersectsMod16Segment(Math.max(0, width - 16), height, rational2)) {
                b = true;
            }
            return b;
        }
        if (n == 0) {
            return ratioIntersectsMod16Segment(height, width, rational);
        }
        return height % 16 == 0 && ratioIntersectsMod16Segment(width, height, rational2);
    }
    
    private static boolean ratioIntersectsMod16Segment(final int n, final int n2, final Rational rational) {
        final boolean b = true;
        Preconditions.checkArgument(n2 % 16 == 0);
        final double n3 = n * rational.getNumerator() / (double)rational.getDenominator();
        return n3 > Math.max(0, n2 - 16) && n3 < n2 + 16 && b;
    }
    
    public static final class CompareAspectRatiosByDistanceToTargetRatio implements Comparator<Rational>
    {
        private Rational mTargetRatio;
        
        public CompareAspectRatiosByDistanceToTargetRatio(final Rational mTargetRatio) {
            this.mTargetRatio = mTargetRatio;
        }
        
        @Override
        public int compare(final Rational rational, final Rational rational2) {
            if (rational.equals((Object)rational2)) {
                return 0;
            }
            return (int)Math.signum(Math.abs(rational.floatValue() - this.mTargetRatio.floatValue()) - Math.abs(rational2.floatValue() - this.mTargetRatio.floatValue()));
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl.utils;

import androidx.core.util.Preconditions;
import android.util.CloseGuard;
import android.os.Build$VERSION;

public final class CloseGuardHelper
{
    private final CloseGuardImpl mImpl;
    
    private CloseGuardHelper(final CloseGuardImpl mImpl) {
        this.mImpl = mImpl;
    }
    
    public static CloseGuardHelper create() {
        if (Build$VERSION.SDK_INT >= 30) {
            return new CloseGuardHelper((CloseGuardImpl)new CloseGuardApi30Impl());
        }
        return new CloseGuardHelper((CloseGuardImpl)new CloseGuardNoOpImpl());
    }
    
    public void close() {
        this.mImpl.close();
    }
    
    public void open(final String s) {
        this.mImpl.open(s);
    }
    
    public void warnIfOpen() {
        this.mImpl.warnIfOpen();
    }
    
    static final class CloseGuardApi30Impl implements CloseGuardImpl
    {
        private final CloseGuard mPlatformImpl;
        
        CloseGuardApi30Impl() {
            this.mPlatformImpl = new CloseGuard();
        }
        
        @Override
        public void close() {
            this.mPlatformImpl.close();
        }
        
        @Override
        public void open(final String s) {
            this.mPlatformImpl.open(s);
        }
        
        @Override
        public void warnIfOpen() {
            this.mPlatformImpl.warnIfOpen();
        }
    }
    
    private interface CloseGuardImpl
    {
        void close();
        
        void open(final String p0);
        
        void warnIfOpen();
    }
    
    static final class CloseGuardNoOpImpl implements CloseGuardImpl
    {
        @Override
        public void close() {
        }
        
        @Override
        public void open(final String s) {
            Preconditions.checkNotNull(s, "CloseMethodName must not be null.");
        }
        
        @Override
        public void warnIfOpen() {
        }
    }
}

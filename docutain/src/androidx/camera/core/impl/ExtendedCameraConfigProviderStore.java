// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl;

import java.util.HashMap;
import java.util.Map;

public final class ExtendedCameraConfigProviderStore
{
    private static final Map<Object, CameraConfigProvider> CAMERA_CONFIG_PROVIDERS;
    private static final Object LOCK;
    
    static {
        LOCK = new Object();
        CAMERA_CONFIG_PROVIDERS = new HashMap<Object, CameraConfigProvider>();
    }
    
    private ExtendedCameraConfigProviderStore() {
    }
    
    public static void addConfig(final Object o, final CameraConfigProvider cameraConfigProvider) {
        synchronized (ExtendedCameraConfigProviderStore.LOCK) {
            ExtendedCameraConfigProviderStore.CAMERA_CONFIG_PROVIDERS.put(o, cameraConfigProvider);
        }
    }
    
    public static CameraConfigProvider getConfigProvider(final Object o) {
        synchronized (ExtendedCameraConfigProviderStore.LOCK) {
            final CameraConfigProvider cameraConfigProvider = ExtendedCameraConfigProviderStore.CAMERA_CONFIG_PROVIDERS.get(o);
            monitorexit(ExtendedCameraConfigProviderStore.LOCK);
            CameraConfigProvider empty = cameraConfigProvider;
            if (cameraConfigProvider == null) {
                empty = CameraConfigProvider.EMPTY;
            }
            return empty;
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl;

import java.util.Collections;
import androidx.camera.core.Logger;
import java.util.Iterator;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

public final class UseCaseAttachState
{
    private static final String TAG = "UseCaseAttachState";
    private final Map<String, UseCaseAttachInfo> mAttachedUseCasesToInfoMap;
    private final String mCameraId;
    
    public UseCaseAttachState(final String mCameraId) {
        this.mAttachedUseCasesToInfoMap = new LinkedHashMap<String, UseCaseAttachInfo>();
        this.mCameraId = mCameraId;
    }
    
    private UseCaseAttachInfo getOrCreateUseCaseAttachInfo(final String s, final SessionConfig sessionConfig, final UseCaseConfig<?> useCaseConfig) {
        UseCaseAttachInfo useCaseAttachInfo;
        if ((useCaseAttachInfo = this.mAttachedUseCasesToInfoMap.get(s)) == null) {
            useCaseAttachInfo = new UseCaseAttachInfo(sessionConfig, useCaseConfig);
            this.mAttachedUseCasesToInfoMap.put(s, useCaseAttachInfo);
        }
        return useCaseAttachInfo;
    }
    
    private Collection<SessionConfig> getSessionConfigs(final AttachStateFilter attachStateFilter) {
        final ArrayList list = new ArrayList();
        for (final Map.Entry<K, UseCaseAttachInfo> entry : this.mAttachedUseCasesToInfoMap.entrySet()) {
            if (attachStateFilter == null || attachStateFilter.filter(entry.getValue())) {
                list.add(entry.getValue().getSessionConfig());
            }
        }
        return list;
    }
    
    private Collection<UseCaseConfig<?>> getUseCaseConfigs(final AttachStateFilter attachStateFilter) {
        final ArrayList list = new ArrayList();
        for (final Map.Entry<K, UseCaseAttachInfo> entry : this.mAttachedUseCasesToInfoMap.entrySet()) {
            if (attachStateFilter == null || attachStateFilter.filter(entry.getValue())) {
                list.add(entry.getValue().getUseCaseConfig());
            }
        }
        return list;
    }
    
    public SessionConfig.ValidatingBuilder getActiveAndAttachedBuilder() {
        final SessionConfig.ValidatingBuilder validatingBuilder = new SessionConfig.ValidatingBuilder();
        final ArrayList obj = new ArrayList();
        for (final Map.Entry<K, UseCaseAttachInfo> entry : this.mAttachedUseCasesToInfoMap.entrySet()) {
            final UseCaseAttachInfo useCaseAttachInfo = entry.getValue();
            if (useCaseAttachInfo.getActive() && useCaseAttachInfo.getAttached()) {
                final String s = (String)entry.getKey();
                validatingBuilder.add(useCaseAttachInfo.getSessionConfig());
                obj.add(s);
            }
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Active and attached use case: ");
        sb.append(obj);
        sb.append(" for camera: ");
        sb.append(this.mCameraId);
        Logger.d("UseCaseAttachState", sb.toString());
        return validatingBuilder;
    }
    
    public Collection<SessionConfig> getActiveAndAttachedSessionConfigs() {
        return Collections.unmodifiableCollection((Collection<? extends SessionConfig>)this.getSessionConfigs((AttachStateFilter)new UseCaseAttachState$$ExternalSyntheticLambda2()));
    }
    
    public SessionConfig.ValidatingBuilder getAttachedBuilder() {
        final SessionConfig.ValidatingBuilder validatingBuilder = new SessionConfig.ValidatingBuilder();
        final ArrayList obj = new ArrayList();
        for (final Map.Entry<K, UseCaseAttachInfo> entry : this.mAttachedUseCasesToInfoMap.entrySet()) {
            final UseCaseAttachInfo useCaseAttachInfo = entry.getValue();
            if (useCaseAttachInfo.getAttached()) {
                validatingBuilder.add(useCaseAttachInfo.getSessionConfig());
                obj.add(entry.getKey());
            }
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("All use case: ");
        sb.append(obj);
        sb.append(" for camera: ");
        sb.append(this.mCameraId);
        Logger.d("UseCaseAttachState", sb.toString());
        return validatingBuilder;
    }
    
    public Collection<SessionConfig> getAttachedSessionConfigs() {
        return Collections.unmodifiableCollection((Collection<? extends SessionConfig>)this.getSessionConfigs((AttachStateFilter)new UseCaseAttachState$$ExternalSyntheticLambda0()));
    }
    
    public Collection<UseCaseConfig<?>> getAttachedUseCaseConfigs() {
        return Collections.unmodifiableCollection((Collection<? extends UseCaseConfig<?>>)this.getUseCaseConfigs((AttachStateFilter)new UseCaseAttachState$$ExternalSyntheticLambda1()));
    }
    
    public boolean isUseCaseAttached(final String s) {
        return this.mAttachedUseCasesToInfoMap.containsKey(s) && this.mAttachedUseCasesToInfoMap.get(s).getAttached();
    }
    
    public void removeUseCase(final String s) {
        this.mAttachedUseCasesToInfoMap.remove(s);
    }
    
    public void setUseCaseActive(final String s, final SessionConfig sessionConfig, final UseCaseConfig<?> useCaseConfig) {
        this.getOrCreateUseCaseAttachInfo(s, sessionConfig, useCaseConfig).setActive(true);
    }
    
    public void setUseCaseAttached(final String s, final SessionConfig sessionConfig, final UseCaseConfig<?> useCaseConfig) {
        this.getOrCreateUseCaseAttachInfo(s, sessionConfig, useCaseConfig).setAttached(true);
    }
    
    public void setUseCaseDetached(final String s) {
        if (!this.mAttachedUseCasesToInfoMap.containsKey(s)) {
            return;
        }
        final UseCaseAttachInfo useCaseAttachInfo = this.mAttachedUseCasesToInfoMap.get(s);
        useCaseAttachInfo.setAttached(false);
        if (!useCaseAttachInfo.getActive()) {
            this.mAttachedUseCasesToInfoMap.remove(s);
        }
    }
    
    public void setUseCaseInactive(final String s) {
        if (!this.mAttachedUseCasesToInfoMap.containsKey(s)) {
            return;
        }
        final UseCaseAttachInfo useCaseAttachInfo = this.mAttachedUseCasesToInfoMap.get(s);
        useCaseAttachInfo.setActive(false);
        if (!useCaseAttachInfo.getAttached()) {
            this.mAttachedUseCasesToInfoMap.remove(s);
        }
    }
    
    public void updateUseCase(final String s, final SessionConfig sessionConfig, final UseCaseConfig<?> useCaseConfig) {
        if (!this.mAttachedUseCasesToInfoMap.containsKey(s)) {
            return;
        }
        final UseCaseAttachInfo useCaseAttachInfo = new UseCaseAttachInfo(sessionConfig, useCaseConfig);
        final UseCaseAttachInfo useCaseAttachInfo2 = this.mAttachedUseCasesToInfoMap.get(s);
        useCaseAttachInfo.setAttached(useCaseAttachInfo2.getAttached());
        useCaseAttachInfo.setActive(useCaseAttachInfo2.getActive());
        this.mAttachedUseCasesToInfoMap.put(s, useCaseAttachInfo);
    }
    
    private interface AttachStateFilter
    {
        boolean filter(final UseCaseAttachInfo p0);
    }
    
    private static final class UseCaseAttachInfo
    {
        private boolean mActive;
        private boolean mAttached;
        private final SessionConfig mSessionConfig;
        private final UseCaseConfig<?> mUseCaseConfig;
        
        UseCaseAttachInfo(final SessionConfig mSessionConfig, final UseCaseConfig<?> mUseCaseConfig) {
            this.mAttached = false;
            this.mActive = false;
            this.mSessionConfig = mSessionConfig;
            this.mUseCaseConfig = mUseCaseConfig;
        }
        
        boolean getActive() {
            return this.mActive;
        }
        
        boolean getAttached() {
            return this.mAttached;
        }
        
        SessionConfig getSessionConfig() {
            return this.mSessionConfig;
        }
        
        UseCaseConfig<?> getUseCaseConfig() {
            return this.mUseCaseConfig;
        }
        
        void setActive(final boolean mActive) {
            this.mActive = mActive;
        }
        
        void setAttached(final boolean mAttached) {
            this.mAttached = mAttached;
        }
    }
}

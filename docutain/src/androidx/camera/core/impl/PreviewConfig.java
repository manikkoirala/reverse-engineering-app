// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl;

import androidx.camera.core.internal.UseCaseEventConfig;
import androidx.camera.core.internal.UseCaseEventConfig$_CC;
import androidx.camera.core.UseCase;
import android.util.Range;
import androidx.camera.core.internal.TargetConfig;
import androidx.camera.core.internal.TargetConfig$_CC;
import java.util.List;
import java.util.Set;
import android.util.Size;
import androidx.camera.core.CameraSelector;
import androidx.camera.core.internal.ThreadConfig$_CC;
import java.util.concurrent.Executor;
import androidx.camera.core.internal.ThreadConfig;
import androidx.camera.core.Preview;

public final class PreviewConfig implements UseCaseConfig<Preview>, ImageOutputConfig, ThreadConfig
{
    public static final Option<ImageInfoProcessor> IMAGE_INFO_PROCESSOR;
    public static final Option<CaptureProcessor> OPTION_PREVIEW_CAPTURE_PROCESSOR;
    public static final Option<Boolean> OPTION_RGBA8888_SURFACE_REQUIRED;
    private final OptionsBundle mConfig;
    
    static {
        IMAGE_INFO_PROCESSOR = (Option)Config.Option.create("camerax.core.preview.imageInfoProcessor", ImageInfoProcessor.class);
        OPTION_PREVIEW_CAPTURE_PROCESSOR = (Option)Config.Option.create("camerax.core.preview.captureProcessor", CaptureProcessor.class);
        OPTION_RGBA8888_SURFACE_REQUIRED = (Option)Config.Option.create("camerax.core.preview.isRgba8888SurfaceRequired", Boolean.class);
    }
    
    public PreviewConfig(final OptionsBundle mConfig) {
        this.mConfig = mConfig;
    }
    
    public CaptureProcessor getCaptureProcessor() {
        return (CaptureProcessor)this.retrieveOption(PreviewConfig.OPTION_PREVIEW_CAPTURE_PROCESSOR);
    }
    
    public CaptureProcessor getCaptureProcessor(final CaptureProcessor captureProcessor) {
        return (CaptureProcessor)this.retrieveOption(PreviewConfig.OPTION_PREVIEW_CAPTURE_PROCESSOR, captureProcessor);
    }
    
    @Override
    public Config getConfig() {
        return this.mConfig;
    }
    
    ImageInfoProcessor getImageInfoProcessor() {
        return (ImageInfoProcessor)this.retrieveOption(PreviewConfig.IMAGE_INFO_PROCESSOR);
    }
    
    public ImageInfoProcessor getImageInfoProcessor(final ImageInfoProcessor imageInfoProcessor) {
        return (ImageInfoProcessor)this.retrieveOption(PreviewConfig.IMAGE_INFO_PROCESSOR, imageInfoProcessor);
    }
    
    @Override
    public int getInputFormat() {
        return (int)this.retrieveOption(PreviewConfig.OPTION_INPUT_FORMAT);
    }
    
    public boolean isRgba8888SurfaceRequired(final boolean b) {
        return (boolean)this.retrieveOption(PreviewConfig.OPTION_RGBA8888_SURFACE_REQUIRED, b);
    }
    
    @Override
    public /* synthetic */ Object retrieveOption(final Option option) {
        return ReadableConfig$_CC.$default$retrieveOption(this, option);
    }
    
    @Override
    public /* synthetic */ Object retrieveOption(final Option option, final Object o) {
        return ReadableConfig$_CC.$default$retrieveOption(this, option, o);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl;

import java.util.Collection;
import java.util.Collections;
import java.util.Set;
import java.util.Iterator;
import android.util.ArrayMap;
import java.util.Map;
import java.util.TreeMap;
import java.util.Comparator;

public class OptionsBundle implements Config
{
    private static final OptionsBundle EMPTY_BUNDLE;
    protected static final Comparator<Option<?>> ID_COMPARE;
    protected final TreeMap<Option<?>, Map<OptionPriority, Object>> mOptions;
    
    static {
        EMPTY_BUNDLE = new OptionsBundle(new TreeMap<Option<?>, Map<OptionPriority, Object>>(ID_COMPARE = new OptionsBundle$$ExternalSyntheticLambda0()));
    }
    
    OptionsBundle(final TreeMap<Option<?>, Map<OptionPriority, Object>> mOptions) {
        this.mOptions = mOptions;
    }
    
    public static OptionsBundle emptyBundle() {
        return OptionsBundle.EMPTY_BUNDLE;
    }
    
    public static OptionsBundle from(final Config config) {
        if (OptionsBundle.class.equals(config.getClass())) {
            return (OptionsBundle)config;
        }
        final TreeMap treeMap = new TreeMap((Comparator<? super K>)OptionsBundle.ID_COMPARE);
        for (final Option<?> key : config.listOptions()) {
            final Set<OptionPriority> priorities = config.getPriorities(key);
            final ArrayMap value = new ArrayMap();
            for (final OptionPriority optionPriority : priorities) {
                ((Map<OptionPriority, Object>)value).put(optionPriority, config.retrieveOptionWithPriority(key, optionPriority));
            }
            treeMap.put(key, value);
        }
        return new OptionsBundle(treeMap);
    }
    
    @Override
    public boolean containsOption(final Option<?> key) {
        return this.mOptions.containsKey(key);
    }
    
    @Override
    public void findOptions(final String prefix, final OptionMatcher optionMatcher) {
        for (final Map.Entry<Option, V> entry : this.mOptions.tailMap(Option.create(prefix, Void.class)).entrySet()) {
            if (!entry.getKey().getId().startsWith(prefix)) {
                break;
            }
            if (!optionMatcher.onOptionMatched(entry.getKey())) {
                break;
            }
        }
    }
    
    @Override
    public OptionPriority getOptionPriority(final Option<?> option) {
        final Map map = this.mOptions.get(option);
        if (map != null) {
            return (OptionPriority)Collections.min((Collection<?>)map.keySet());
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Option does not exist: ");
        sb.append(option);
        throw new IllegalArgumentException(sb.toString());
    }
    
    @Override
    public Set<OptionPriority> getPriorities(final Option<?> key) {
        final Map map = this.mOptions.get(key);
        if (map == null) {
            return Collections.emptySet();
        }
        return (Set<OptionPriority>)Collections.unmodifiableSet((Set<?>)map.keySet());
    }
    
    @Override
    public Set<Option<?>> listOptions() {
        return Collections.unmodifiableSet((Set<? extends Option<?>>)this.mOptions.keySet());
    }
    
    @Override
    public <ValueT> ValueT retrieveOption(final Option<ValueT> option) {
        final Map map = this.mOptions.get(option);
        if (map != null) {
            return (ValueT)map.get(Collections.min((Collection<?>)map.keySet()));
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Option does not exist: ");
        sb.append(option);
        throw new IllegalArgumentException(sb.toString());
    }
    
    @Override
    public <ValueT> ValueT retrieveOption(final Option<ValueT> option, final ValueT valueT) {
        try {
            return this.retrieveOption(option);
        }
        catch (final IllegalArgumentException ex) {
            return valueT;
        }
    }
    
    @Override
    public <ValueT> ValueT retrieveOptionWithPriority(final Option<ValueT> obj, final OptionPriority obj2) {
        final Map map = this.mOptions.get(obj);
        if (map == null) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Option does not exist: ");
            sb.append(obj);
            throw new IllegalArgumentException(sb.toString());
        }
        if (map.containsKey(obj2)) {
            return (ValueT)map.get(obj2);
        }
        final StringBuilder sb2 = new StringBuilder();
        sb2.append("Option does not exist: ");
        sb2.append(obj);
        sb2.append(" with priority=");
        sb2.append(obj2);
        throw new IllegalArgumentException(sb2.toString());
    }
}

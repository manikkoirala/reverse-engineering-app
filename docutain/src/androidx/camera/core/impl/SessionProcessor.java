// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl;

import android.hardware.camera2.CaptureResult$Key;
import java.util.Map;
import androidx.camera.core.CameraInfo;

public interface SessionProcessor
{
    void abortCapture(final int p0);
    
    void deInitSession();
    
    SessionConfig initSession(final CameraInfo p0, final OutputSurface p1, final OutputSurface p2, final OutputSurface p3);
    
    void onCaptureSessionEnd();
    
    void onCaptureSessionStart(final RequestProcessor p0);
    
    void setParameters(final Config p0);
    
    int startCapture(final CaptureCallback p0);
    
    int startRepeating(final CaptureCallback p0);
    
    void stopRepeating();
    
    public interface CaptureCallback
    {
        void onCaptureCompleted(final long p0, final int p1, final Map<CaptureResult$Key, Object> p2);
        
        void onCaptureFailed(final int p0);
        
        void onCaptureProcessStarted(final int p0);
        
        void onCaptureSequenceAborted(final int p0);
        
        void onCaptureSequenceCompleted(final int p0);
        
        void onCaptureStarted(final int p0, final long p1);
    }
}

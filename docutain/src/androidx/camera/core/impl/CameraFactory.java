// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl;

import androidx.camera.core.InitializationException;
import androidx.camera.core.CameraSelector;
import android.content.Context;
import androidx.camera.core.CameraUnavailableException;
import java.util.Set;

public interface CameraFactory
{
    Set<String> getAvailableCameraIds();
    
    CameraInternal getCamera(final String p0) throws CameraUnavailableException;
    
    Object getCameraManager();
    
    public interface Provider
    {
        CameraFactory newInstance(final Context p0, final CameraThreadConfig p1, final CameraSelector p2) throws InitializationException;
    }
}

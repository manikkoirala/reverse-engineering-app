// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl;

public class CameraCaptureFailure
{
    private final Reason mReason;
    
    public CameraCaptureFailure(final Reason mReason) {
        this.mReason = mReason;
    }
    
    public Reason getReason() {
        return this.mReason;
    }
    
    public enum Reason
    {
        private static final Reason[] $VALUES;
        
        ERROR;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl;

import androidx.camera.core.internal.CameraCaptureResultImageInfo;
import androidx.camera.core.ImageInfo;

public final class CameraCaptureResults
{
    private CameraCaptureResults() {
    }
    
    public static CameraCaptureResult retrieveCameraCaptureResult(final ImageInfo imageInfo) {
        if (imageInfo instanceof CameraCaptureResultImageInfo) {
            return ((CameraCaptureResultImageInfo)imageInfo).getCameraCaptureResult();
        }
        return null;
    }
}

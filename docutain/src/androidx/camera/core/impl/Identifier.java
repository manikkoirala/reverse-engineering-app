// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl;

public abstract class Identifier
{
    public static Identifier create(final Object o) {
        return new AutoValue_Identifier(o);
    }
    
    public abstract Object getValue();
}

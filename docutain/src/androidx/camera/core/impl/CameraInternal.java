// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl;

import com.google.common.util.concurrent.ListenableFuture;
import java.util.LinkedHashSet;
import androidx.camera.core.CameraInfo;
import androidx.camera.core.CameraControl;
import java.util.Collection;
import androidx.camera.core.UseCase;
import androidx.camera.core.Camera;

public interface CameraInternal extends Camera, StateChangeCallback
{
    void attachUseCases(final Collection<UseCase> p0);
    
    void close();
    
    void detachUseCases(final Collection<UseCase> p0);
    
    CameraControl getCameraControl();
    
    CameraControlInternal getCameraControlInternal();
    
    CameraInfo getCameraInfo();
    
    CameraInfoInternal getCameraInfoInternal();
    
    LinkedHashSet<CameraInternal> getCameraInternals();
    
    Observable<State> getCameraState();
    
    CameraConfig getExtendedConfig();
    
    void open();
    
    ListenableFuture<Void> release();
    
    void setActiveResumingMode(final boolean p0);
    
    void setExtendedConfig(final CameraConfig p0);
    
    public enum State
    {
        private static final State[] $VALUES;
        
        CLOSED(false), 
        CLOSING(true), 
        OPEN(true), 
        OPENING(true), 
        PENDING_OPEN(false), 
        RELEASED(false), 
        RELEASING(true);
        
        private final boolean mHoldsCameraSlot;
        
        private State(final boolean mHoldsCameraSlot) {
            this.mHoldsCameraSlot = mHoldsCameraSlot;
        }
        
        boolean holdsCameraSlot() {
            return this.mHoldsCameraSlot;
        }
    }
}

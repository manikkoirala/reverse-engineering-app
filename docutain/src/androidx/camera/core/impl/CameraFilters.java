// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl;

import java.util.Collections;
import java.util.List;
import androidx.camera.core.CameraFilter;

public class CameraFilters
{
    public static final CameraFilter ANY;
    public static final CameraFilter NONE;
    
    static {
        ANY = new CameraFilters$$ExternalSyntheticLambda0();
        NONE = new CameraFilters$$ExternalSyntheticLambda1();
    }
    
    private CameraFilters() {
    }
}

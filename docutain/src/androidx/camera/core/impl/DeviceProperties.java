// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl;

import android.os.Build$VERSION;
import android.os.Build;

public abstract class DeviceProperties
{
    public static DeviceProperties create() {
        return create(Build.MANUFACTURER, Build.MODEL, Build$VERSION.SDK_INT);
    }
    
    public static DeviceProperties create(final String s, final String s2, final int n) {
        return new AutoValue_DeviceProperties(s, s2, n);
    }
    
    public abstract String manufacturer();
    
    public abstract String model();
    
    public abstract int sdkVersion();
}

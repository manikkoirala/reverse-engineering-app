// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl;

import androidx.camera.core.internal.utils.SizeUtil;
import android.util.Size;

public abstract class SurfaceConfig
{
    SurfaceConfig() {
    }
    
    public static SurfaceConfig create(final ConfigType configType, final ConfigSize configSize) {
        return new AutoValue_SurfaceConfig(configType, configSize);
    }
    
    public static ConfigType getConfigType(final int n) {
        if (n == 35) {
            return ConfigType.YUV;
        }
        if (n == 256) {
            return ConfigType.JPEG;
        }
        if (n == 32) {
            return ConfigType.RAW;
        }
        return ConfigType.PRIV;
    }
    
    public static SurfaceConfig transformSurfaceConfig(int area, final Size size, final SurfaceSizeDefinition surfaceSizeDefinition) {
        final ConfigType configType = getConfigType(area);
        final ConfigSize not_SUPPORT = ConfigSize.NOT_SUPPORT;
        area = SizeUtil.getArea(size);
        ConfigSize configSize;
        if (area <= SizeUtil.getArea(surfaceSizeDefinition.getAnalysisSize())) {
            configSize = ConfigSize.VGA;
        }
        else if (area <= SizeUtil.getArea(surfaceSizeDefinition.getPreviewSize())) {
            configSize = ConfigSize.PREVIEW;
        }
        else if (area <= SizeUtil.getArea(surfaceSizeDefinition.getRecordSize())) {
            configSize = ConfigSize.RECORD;
        }
        else {
            configSize = ConfigSize.MAXIMUM;
        }
        return create(configType, configSize);
    }
    
    public abstract ConfigSize getConfigSize();
    
    public abstract ConfigType getConfigType();
    
    public final boolean isSupported(final SurfaceConfig surfaceConfig) {
        final ConfigType configType = surfaceConfig.getConfigType();
        return surfaceConfig.getConfigSize().getId() <= this.getConfigSize().getId() && configType == this.getConfigType();
    }
    
    public enum ConfigSize
    {
        private static final ConfigSize[] $VALUES;
        
        MAXIMUM(3), 
        NOT_SUPPORT(4), 
        PREVIEW(1), 
        RECORD(2), 
        VGA(0);
        
        final int mId;
        
        private ConfigSize(final int mId) {
            this.mId = mId;
        }
        
        int getId() {
            return this.mId;
        }
    }
    
    public enum ConfigType
    {
        private static final ConfigType[] $VALUES;
        
        JPEG, 
        PRIV, 
        RAW, 
        YUV;
    }
}

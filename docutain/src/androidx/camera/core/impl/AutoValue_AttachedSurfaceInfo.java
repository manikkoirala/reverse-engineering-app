// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl;

import android.util.Range;
import android.util.Size;

final class AutoValue_AttachedSurfaceInfo extends AttachedSurfaceInfo
{
    private final int imageFormat;
    private final Size size;
    private final SurfaceConfig surfaceConfig;
    private final Range<Integer> targetFrameRate;
    
    AutoValue_AttachedSurfaceInfo(final SurfaceConfig surfaceConfig, final int imageFormat, final Size size, final Range<Integer> targetFrameRate) {
        if (surfaceConfig == null) {
            throw new NullPointerException("Null surfaceConfig");
        }
        this.surfaceConfig = surfaceConfig;
        this.imageFormat = imageFormat;
        if (size != null) {
            this.size = size;
            this.targetFrameRate = targetFrameRate;
            return;
        }
        throw new NullPointerException("Null size");
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o == this) {
            return true;
        }
        if (o instanceof AttachedSurfaceInfo) {
            final AttachedSurfaceInfo attachedSurfaceInfo = (AttachedSurfaceInfo)o;
            if (this.surfaceConfig.equals(attachedSurfaceInfo.getSurfaceConfig()) && this.imageFormat == attachedSurfaceInfo.getImageFormat() && this.size.equals((Object)attachedSurfaceInfo.getSize())) {
                final Range<Integer> targetFrameRate = this.targetFrameRate;
                if (targetFrameRate == null) {
                    if (attachedSurfaceInfo.getTargetFrameRate() == null) {
                        return b;
                    }
                }
                else if (targetFrameRate.equals((Object)attachedSurfaceInfo.getTargetFrameRate())) {
                    return b;
                }
            }
            b = false;
            return b;
        }
        return false;
    }
    
    @Override
    public int getImageFormat() {
        return this.imageFormat;
    }
    
    @Override
    public Size getSize() {
        return this.size;
    }
    
    @Override
    public SurfaceConfig getSurfaceConfig() {
        return this.surfaceConfig;
    }
    
    @Override
    public Range<Integer> getTargetFrameRate() {
        return this.targetFrameRate;
    }
    
    @Override
    public int hashCode() {
        final int hashCode = this.surfaceConfig.hashCode();
        final int imageFormat = this.imageFormat;
        final int hashCode2 = this.size.hashCode();
        final Range<Integer> targetFrameRate = this.targetFrameRate;
        int hashCode3;
        if (targetFrameRate == null) {
            hashCode3 = 0;
        }
        else {
            hashCode3 = targetFrameRate.hashCode();
        }
        return (((hashCode ^ 0xF4243) * 1000003 ^ imageFormat) * 1000003 ^ hashCode2) * 1000003 ^ hashCode3;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("AttachedSurfaceInfo{surfaceConfig=");
        sb.append(this.surfaceConfig);
        sb.append(", imageFormat=");
        sb.append(this.imageFormat);
        sb.append(", size=");
        sb.append(this.size);
        sb.append(", targetFrameRate=");
        sb.append(this.targetFrameRate);
        sb.append("}");
        return sb.toString();
    }
}

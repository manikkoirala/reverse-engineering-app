// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl;

import java.util.List;

public interface RequestProcessor
{
    void abortCaptures();
    
    int setRepeating(final Request p0, final Callback p1);
    
    void stopRepeating();
    
    int submit(final Request p0, final Callback p1);
    
    int submit(final List<Request> p0, final Callback p1);
    
    public interface Callback
    {
        void onCaptureBufferLost(final Request p0, final long p1, final int p2);
        
        void onCaptureCompleted(final Request p0, final CameraCaptureResult p1);
        
        void onCaptureFailed(final Request p0, final CameraCaptureFailure p1);
        
        void onCaptureProgressed(final Request p0, final CameraCaptureResult p1);
        
        void onCaptureSequenceAborted(final int p0);
        
        void onCaptureSequenceCompleted(final int p0, final long p1);
        
        void onCaptureStarted(final Request p0, final long p1, final long p2);
    }
    
    public interface Request
    {
        Config getParameters();
        
        List<Integer> getTargetOutputConfigIds();
        
        int getTemplateId();
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl;

import java.util.concurrent.Executor;
import android.view.Surface;
import androidx.camera.core.ImageProxy;

public interface ImageReaderProxy
{
    ImageProxy acquireLatestImage();
    
    ImageProxy acquireNextImage();
    
    void clearOnImageAvailableListener();
    
    void close();
    
    int getHeight();
    
    int getImageFormat();
    
    int getMaxImages();
    
    Surface getSurface();
    
    int getWidth();
    
    void setOnImageAvailableListener(final OnImageAvailableListener p0, final Executor p1);
    
    public interface OnImageAvailableListener
    {
        void onImageAvailable(final ImageReaderProxy p0);
    }
}

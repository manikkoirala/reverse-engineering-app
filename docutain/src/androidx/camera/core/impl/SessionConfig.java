// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl;

import androidx.camera.core.Logger;
import java.util.Arrays;
import androidx.camera.core.internal.compat.workaround.SurfaceSorter;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.Iterator;
import java.util.ArrayList;
import java.util.Collections;
import android.hardware.camera2.CameraCaptureSession$StateCallback;
import android.hardware.camera2.params.InputConfiguration;
import android.hardware.camera2.CameraDevice$StateCallback;
import java.util.List;

public final class SessionConfig
{
    private final List<CameraDevice$StateCallback> mDeviceStateCallbacks;
    private final List<ErrorListener> mErrorListeners;
    private InputConfiguration mInputConfiguration;
    private final List<OutputConfig> mOutputConfigs;
    private final CaptureConfig mRepeatingCaptureConfig;
    private final List<CameraCaptureSession$StateCallback> mSessionStateCallbacks;
    private final List<CameraCaptureCallback> mSingleCameraCaptureCallbacks;
    
    SessionConfig(final List<OutputConfig> mOutputConfigs, final List<CameraDevice$StateCallback> list, final List<CameraCaptureSession$StateCallback> list2, final List<CameraCaptureCallback> list3, final List<ErrorListener> list4, final CaptureConfig mRepeatingCaptureConfig, final InputConfiguration mInputConfiguration) {
        this.mOutputConfigs = mOutputConfigs;
        this.mDeviceStateCallbacks = Collections.unmodifiableList((List<? extends CameraDevice$StateCallback>)list);
        this.mSessionStateCallbacks = Collections.unmodifiableList((List<? extends CameraCaptureSession$StateCallback>)list2);
        this.mSingleCameraCaptureCallbacks = Collections.unmodifiableList((List<? extends CameraCaptureCallback>)list3);
        this.mErrorListeners = Collections.unmodifiableList((List<? extends ErrorListener>)list4);
        this.mRepeatingCaptureConfig = mRepeatingCaptureConfig;
        this.mInputConfiguration = mInputConfiguration;
    }
    
    public static SessionConfig defaultEmptySessionConfig() {
        return new SessionConfig(new ArrayList<OutputConfig>(), new ArrayList<CameraDevice$StateCallback>(0), new ArrayList<CameraCaptureSession$StateCallback>(0), new ArrayList<CameraCaptureCallback>(0), new ArrayList<ErrorListener>(0), new CaptureConfig.Builder().build(), null);
    }
    
    public List<CameraDevice$StateCallback> getDeviceStateCallbacks() {
        return this.mDeviceStateCallbacks;
    }
    
    public List<ErrorListener> getErrorListeners() {
        return this.mErrorListeners;
    }
    
    public Config getImplementationOptions() {
        return this.mRepeatingCaptureConfig.getImplementationOptions();
    }
    
    public InputConfiguration getInputConfiguration() {
        return this.mInputConfiguration;
    }
    
    public List<OutputConfig> getOutputConfigs() {
        return this.mOutputConfigs;
    }
    
    public List<CameraCaptureCallback> getRepeatingCameraCaptureCallbacks() {
        return this.mRepeatingCaptureConfig.getCameraCaptureCallbacks();
    }
    
    public CaptureConfig getRepeatingCaptureConfig() {
        return this.mRepeatingCaptureConfig;
    }
    
    public List<CameraCaptureSession$StateCallback> getSessionStateCallbacks() {
        return this.mSessionStateCallbacks;
    }
    
    public List<CameraCaptureCallback> getSingleCameraCaptureCallbacks() {
        return this.mSingleCameraCaptureCallbacks;
    }
    
    public List<DeferrableSurface> getSurfaces() {
        final ArrayList list = new ArrayList();
        for (final OutputConfig outputConfig : this.mOutputConfigs) {
            list.add(outputConfig.getSurface());
            final Iterator<DeferrableSurface> iterator2 = outputConfig.getSharedSurfaces().iterator();
            while (iterator2.hasNext()) {
                list.add(iterator2.next());
            }
        }
        return (List<DeferrableSurface>)Collections.unmodifiableList((List<?>)list);
    }
    
    public int getTemplateType() {
        return this.mRepeatingCaptureConfig.getTemplateType();
    }
    
    static class BaseBuilder
    {
        final CaptureConfig.Builder mCaptureConfigBuilder;
        final List<CameraDevice$StateCallback> mDeviceStateCallbacks;
        final List<ErrorListener> mErrorListeners;
        InputConfiguration mInputConfiguration;
        final Set<OutputConfig> mOutputConfigs;
        final List<CameraCaptureSession$StateCallback> mSessionStateCallbacks;
        final List<CameraCaptureCallback> mSingleCameraCaptureCallbacks;
        
        BaseBuilder() {
            this.mOutputConfigs = new LinkedHashSet<OutputConfig>();
            this.mCaptureConfigBuilder = new CaptureConfig.Builder();
            this.mDeviceStateCallbacks = new ArrayList<CameraDevice$StateCallback>();
            this.mSessionStateCallbacks = new ArrayList<CameraCaptureSession$StateCallback>();
            this.mErrorListeners = new ArrayList<ErrorListener>();
            this.mSingleCameraCaptureCallbacks = new ArrayList<CameraCaptureCallback>();
        }
    }
    
    public static class Builder extends BaseBuilder
    {
        public static Builder createFrom(final UseCaseConfig<?> useCaseConfig) {
            final OptionUnpacker sessionOptionUnpacker = useCaseConfig.getSessionOptionUnpacker(null);
            if (sessionOptionUnpacker != null) {
                final Builder builder = new Builder();
                sessionOptionUnpacker.unpack(useCaseConfig, builder);
                return builder;
            }
            final StringBuilder sb = new StringBuilder();
            sb.append("Implementation is missing option unpacker for ");
            sb.append(useCaseConfig.getTargetName(useCaseConfig.toString()));
            throw new IllegalStateException(sb.toString());
        }
        
        public Builder addAllCameraCaptureCallbacks(final Collection<CameraCaptureCallback> collection) {
            for (final CameraCaptureCallback cameraCaptureCallback : collection) {
                this.mCaptureConfigBuilder.addCameraCaptureCallback(cameraCaptureCallback);
                if (!this.mSingleCameraCaptureCallbacks.contains(cameraCaptureCallback)) {
                    this.mSingleCameraCaptureCallbacks.add(cameraCaptureCallback);
                }
            }
            return this;
        }
        
        public Builder addAllDeviceStateCallbacks(final Collection<CameraDevice$StateCallback> collection) {
            final Iterator<CameraDevice$StateCallback> iterator = collection.iterator();
            while (iterator.hasNext()) {
                this.addDeviceStateCallback(iterator.next());
            }
            return this;
        }
        
        public Builder addAllRepeatingCameraCaptureCallbacks(final Collection<CameraCaptureCallback> collection) {
            this.mCaptureConfigBuilder.addAllCameraCaptureCallbacks(collection);
            return this;
        }
        
        public Builder addAllSessionStateCallbacks(final List<CameraCaptureSession$StateCallback> list) {
            final Iterator<CameraCaptureSession$StateCallback> iterator = list.iterator();
            while (iterator.hasNext()) {
                this.addSessionStateCallback(iterator.next());
            }
            return this;
        }
        
        public Builder addCameraCaptureCallback(final CameraCaptureCallback cameraCaptureCallback) {
            this.mCaptureConfigBuilder.addCameraCaptureCallback(cameraCaptureCallback);
            if (!this.mSingleCameraCaptureCallbacks.contains(cameraCaptureCallback)) {
                this.mSingleCameraCaptureCallbacks.add(cameraCaptureCallback);
            }
            return this;
        }
        
        public Builder addDeviceStateCallback(final CameraDevice$StateCallback cameraDevice$StateCallback) {
            if (this.mDeviceStateCallbacks.contains(cameraDevice$StateCallback)) {
                return this;
            }
            this.mDeviceStateCallbacks.add(cameraDevice$StateCallback);
            return this;
        }
        
        public Builder addErrorListener(final ErrorListener errorListener) {
            this.mErrorListeners.add(errorListener);
            return this;
        }
        
        public Builder addImplementationOptions(final Config config) {
            this.mCaptureConfigBuilder.addImplementationOptions(config);
            return this;
        }
        
        public Builder addNonRepeatingSurface(final DeferrableSurface deferrableSurface) {
            this.mOutputConfigs.add(OutputConfig.builder(deferrableSurface).build());
            return this;
        }
        
        public Builder addOutputConfig(final OutputConfig outputConfig) {
            this.mOutputConfigs.add(outputConfig);
            this.mCaptureConfigBuilder.addSurface(outputConfig.getSurface());
            final Iterator<DeferrableSurface> iterator = outputConfig.getSharedSurfaces().iterator();
            while (iterator.hasNext()) {
                this.mCaptureConfigBuilder.addSurface(iterator.next());
            }
            return this;
        }
        
        public Builder addRepeatingCameraCaptureCallback(final CameraCaptureCallback cameraCaptureCallback) {
            this.mCaptureConfigBuilder.addCameraCaptureCallback(cameraCaptureCallback);
            return this;
        }
        
        public Builder addSessionStateCallback(final CameraCaptureSession$StateCallback cameraCaptureSession$StateCallback) {
            if (this.mSessionStateCallbacks.contains(cameraCaptureSession$StateCallback)) {
                return this;
            }
            this.mSessionStateCallbacks.add(cameraCaptureSession$StateCallback);
            return this;
        }
        
        public Builder addSurface(final DeferrableSurface deferrableSurface) {
            this.mOutputConfigs.add(OutputConfig.builder(deferrableSurface).build());
            this.mCaptureConfigBuilder.addSurface(deferrableSurface);
            return this;
        }
        
        public Builder addTag(final String s, final Object o) {
            this.mCaptureConfigBuilder.addTag(s, o);
            return this;
        }
        
        public SessionConfig build() {
            return new SessionConfig(new ArrayList<OutputConfig>((Collection<? extends OutputConfig>)this.mOutputConfigs), this.mDeviceStateCallbacks, this.mSessionStateCallbacks, this.mSingleCameraCaptureCallbacks, this.mErrorListeners, this.mCaptureConfigBuilder.build(), this.mInputConfiguration);
        }
        
        public Builder clearSurfaces() {
            this.mOutputConfigs.clear();
            this.mCaptureConfigBuilder.clearSurfaces();
            return this;
        }
        
        public List<CameraCaptureCallback> getSingleCameraCaptureCallbacks() {
            return Collections.unmodifiableList((List<? extends CameraCaptureCallback>)this.mSingleCameraCaptureCallbacks);
        }
        
        public boolean removeCameraCaptureCallback(final CameraCaptureCallback cameraCaptureCallback) {
            final boolean removeCameraCaptureCallback = this.mCaptureConfigBuilder.removeCameraCaptureCallback(cameraCaptureCallback);
            final boolean remove = this.mSingleCameraCaptureCallbacks.remove(cameraCaptureCallback);
            return removeCameraCaptureCallback || remove;
        }
        
        public Builder removeSurface(final DeferrableSurface obj) {
            while (true) {
                for (final Object o : this.mOutputConfigs) {
                    if (((OutputConfig)o).getSurface().equals(obj)) {
                        if (o != null) {
                            this.mOutputConfigs.remove(o);
                        }
                        this.mCaptureConfigBuilder.removeSurface(obj);
                        return this;
                    }
                }
                Object o = null;
                continue;
            }
        }
        
        public Builder setImplementationOptions(final Config implementationOptions) {
            this.mCaptureConfigBuilder.setImplementationOptions(implementationOptions);
            return this;
        }
        
        public Builder setInputConfiguration(final InputConfiguration mInputConfiguration) {
            this.mInputConfiguration = mInputConfiguration;
            return this;
        }
        
        public Builder setTemplateType(final int templateType) {
            this.mCaptureConfigBuilder.setTemplateType(templateType);
            return this;
        }
    }
    
    public interface ErrorListener
    {
        void onError(final SessionConfig p0, final SessionError p1);
    }
    
    public interface OptionUnpacker
    {
        void unpack(final UseCaseConfig<?> p0, final SessionConfig.Builder p1);
    }
    
    public abstract static class OutputConfig
    {
        public static final int SURFACE_GROUP_ID_NONE = -1;
        
        public static Builder builder(final DeferrableSurface surface) {
            return new AutoValue_SessionConfig_OutputConfig.Builder().setSurface(surface).setSharedSurfaces(Collections.emptyList()).setPhysicalCameraId(null).setSurfaceGroupId(-1);
        }
        
        public abstract String getPhysicalCameraId();
        
        public abstract List<DeferrableSurface> getSharedSurfaces();
        
        public abstract DeferrableSurface getSurface();
        
        public abstract int getSurfaceGroupId();
        
        public abstract static class Builder
        {
            public abstract OutputConfig build();
            
            public abstract Builder setPhysicalCameraId(final String p0);
            
            public abstract Builder setSharedSurfaces(final List<DeferrableSurface> p0);
            
            public abstract Builder setSurface(final DeferrableSurface p0);
            
            public abstract Builder setSurfaceGroupId(final int p0);
        }
    }
    
    public enum SessionError
    {
        private static final SessionError[] $VALUES;
        
        SESSION_ERROR_SURFACE_NEEDS_RESET, 
        SESSION_ERROR_UNKNOWN;
    }
    
    public static final class ValidatingBuilder extends BaseBuilder
    {
        private static final List<Integer> SUPPORTED_TEMPLATE_PRIORITY;
        private static final String TAG = "ValidatingBuilder";
        private final SurfaceSorter mSurfaceSorter;
        private boolean mTemplateSet;
        private boolean mValid;
        
        static {
            SUPPORTED_TEMPLATE_PRIORITY = Arrays.asList(1, 5, 3);
        }
        
        public ValidatingBuilder() {
            this.mSurfaceSorter = new SurfaceSorter();
            this.mValid = true;
            this.mTemplateSet = false;
        }
        
        private List<DeferrableSurface> getSurfaces() {
            final ArrayList list = new ArrayList();
            for (final OutputConfig outputConfig : this.mOutputConfigs) {
                list.add(outputConfig.getSurface());
                final Iterator<DeferrableSurface> iterator2 = outputConfig.getSharedSurfaces().iterator();
                while (iterator2.hasNext()) {
                    list.add(iterator2.next());
                }
            }
            return list;
        }
        
        private int selectTemplateType(int i, final int j) {
            final List<Integer> supported_TEMPLATE_PRIORITY = ValidatingBuilder.SUPPORTED_TEMPLATE_PRIORITY;
            if (supported_TEMPLATE_PRIORITY.indexOf(i) < supported_TEMPLATE_PRIORITY.indexOf(j)) {
                i = j;
            }
            return i;
        }
        
        public void add(final SessionConfig sessionConfig) {
            final CaptureConfig repeatingCaptureConfig = sessionConfig.getRepeatingCaptureConfig();
            if (repeatingCaptureConfig.getTemplateType() != -1) {
                this.mTemplateSet = true;
                this.mCaptureConfigBuilder.setTemplateType(this.selectTemplateType(repeatingCaptureConfig.getTemplateType(), this.mCaptureConfigBuilder.getTemplateType()));
            }
            this.mCaptureConfigBuilder.addAllTags(sessionConfig.getRepeatingCaptureConfig().getTagBundle());
            this.mDeviceStateCallbacks.addAll(sessionConfig.getDeviceStateCallbacks());
            this.mSessionStateCallbacks.addAll(sessionConfig.getSessionStateCallbacks());
            this.mCaptureConfigBuilder.addAllCameraCaptureCallbacks(sessionConfig.getRepeatingCameraCaptureCallbacks());
            this.mSingleCameraCaptureCallbacks.addAll(sessionConfig.getSingleCameraCaptureCallbacks());
            this.mErrorListeners.addAll((Collection<? extends ErrorListener>)sessionConfig.getErrorListeners());
            if (sessionConfig.getInputConfiguration() != null) {
                this.mInputConfiguration = sessionConfig.getInputConfiguration();
            }
            this.mOutputConfigs.addAll((Collection<? extends OutputConfig>)sessionConfig.getOutputConfigs());
            this.mCaptureConfigBuilder.getSurfaces().addAll(repeatingCaptureConfig.getSurfaces());
            if (!this.getSurfaces().containsAll(this.mCaptureConfigBuilder.getSurfaces())) {
                Logger.d("ValidatingBuilder", "Invalid configuration due to capture request surfaces are not a subset of surfaces");
                this.mValid = false;
            }
            this.mCaptureConfigBuilder.addImplementationOptions(repeatingCaptureConfig.getImplementationOptions());
        }
        
        public <T> void addImplementationOption(final Config.Option<T> option, final T t) {
            this.mCaptureConfigBuilder.addImplementationOption(option, t);
        }
        
        public SessionConfig build() {
            if (this.mValid) {
                final ArrayList list = new ArrayList((Collection<? extends E>)this.mOutputConfigs);
                this.mSurfaceSorter.sort(list);
                return new SessionConfig(list, this.mDeviceStateCallbacks, this.mSessionStateCallbacks, this.mSingleCameraCaptureCallbacks, this.mErrorListeners, this.mCaptureConfigBuilder.build(), this.mInputConfiguration);
            }
            throw new IllegalArgumentException("Unsupported session configuration combination");
        }
        
        public void clearSurfaces() {
            this.mOutputConfigs.clear();
            this.mCaptureConfigBuilder.clearSurfaces();
        }
        
        public boolean isValid() {
            return this.mTemplateSet && this.mValid;
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl;

import android.util.Size;
import android.view.Surface;

public abstract class OutputSurface
{
    public static OutputSurface create(final Surface surface, final Size size, final int n) {
        return new AutoValue_OutputSurface(surface, size, n);
    }
    
    public abstract int getImageFormat();
    
    public abstract Size getSize();
    
    public abstract Surface getSurface();
}

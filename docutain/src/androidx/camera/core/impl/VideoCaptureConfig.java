// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl;

import androidx.camera.core.internal.UseCaseEventConfig;
import androidx.camera.core.internal.UseCaseEventConfig$_CC;
import androidx.camera.core.UseCase;
import android.util.Range;
import androidx.camera.core.internal.TargetConfig;
import androidx.camera.core.internal.TargetConfig$_CC;
import java.util.List;
import java.util.Set;
import android.util.Size;
import androidx.camera.core.CameraSelector;
import androidx.camera.core.internal.ThreadConfig$_CC;
import java.util.concurrent.Executor;
import androidx.camera.core.internal.ThreadConfig;
import androidx.camera.core.VideoCapture;

public final class VideoCaptureConfig implements UseCaseConfig<VideoCapture>, ImageOutputConfig, ThreadConfig
{
    public static final Option<Integer> OPTION_AUDIO_BIT_RATE;
    public static final Option<Integer> OPTION_AUDIO_CHANNEL_COUNT;
    public static final Option<Integer> OPTION_AUDIO_MIN_BUFFER_SIZE;
    public static final Option<Integer> OPTION_AUDIO_SAMPLE_RATE;
    public static final Option<Integer> OPTION_BIT_RATE;
    public static final Option<Integer> OPTION_INTRA_FRAME_INTERVAL;
    public static final Option<Integer> OPTION_VIDEO_FRAME_RATE;
    private final OptionsBundle mConfig;
    
    static {
        OPTION_VIDEO_FRAME_RATE = (Option)Config.Option.create("camerax.core.videoCapture.recordingFrameRate", Integer.TYPE);
        OPTION_BIT_RATE = (Option)Config.Option.create("camerax.core.videoCapture.bitRate", Integer.TYPE);
        OPTION_INTRA_FRAME_INTERVAL = (Option)Config.Option.create("camerax.core.videoCapture.intraFrameInterval", Integer.TYPE);
        OPTION_AUDIO_BIT_RATE = (Option)Config.Option.create("camerax.core.videoCapture.audioBitRate", Integer.TYPE);
        OPTION_AUDIO_SAMPLE_RATE = (Option)Config.Option.create("camerax.core.videoCapture.audioSampleRate", Integer.TYPE);
        OPTION_AUDIO_CHANNEL_COUNT = (Option)Config.Option.create("camerax.core.videoCapture.audioChannelCount", Integer.TYPE);
        OPTION_AUDIO_MIN_BUFFER_SIZE = (Option)Config.Option.create("camerax.core.videoCapture.audioMinBufferSize", Integer.TYPE);
    }
    
    public VideoCaptureConfig(final OptionsBundle mConfig) {
        this.mConfig = mConfig;
    }
    
    public int getAudioBitRate() {
        return (int)this.retrieveOption(VideoCaptureConfig.OPTION_AUDIO_BIT_RATE);
    }
    
    public int getAudioBitRate(final int i) {
        return (int)this.retrieveOption(VideoCaptureConfig.OPTION_AUDIO_BIT_RATE, i);
    }
    
    public int getAudioChannelCount() {
        return (int)this.retrieveOption(VideoCaptureConfig.OPTION_AUDIO_CHANNEL_COUNT);
    }
    
    public int getAudioChannelCount(final int i) {
        return (int)this.retrieveOption(VideoCaptureConfig.OPTION_AUDIO_CHANNEL_COUNT, i);
    }
    
    public int getAudioMinBufferSize() {
        return (int)this.retrieveOption(VideoCaptureConfig.OPTION_AUDIO_MIN_BUFFER_SIZE);
    }
    
    public int getAudioMinBufferSize(final int i) {
        return (int)this.retrieveOption(VideoCaptureConfig.OPTION_AUDIO_MIN_BUFFER_SIZE, i);
    }
    
    public int getAudioSampleRate() {
        return (int)this.retrieveOption(VideoCaptureConfig.OPTION_AUDIO_SAMPLE_RATE);
    }
    
    public int getAudioSampleRate(final int i) {
        return (int)this.retrieveOption(VideoCaptureConfig.OPTION_AUDIO_SAMPLE_RATE, i);
    }
    
    public int getBitRate() {
        return (int)this.retrieveOption(VideoCaptureConfig.OPTION_BIT_RATE);
    }
    
    public int getBitRate(final int i) {
        return (int)this.retrieveOption(VideoCaptureConfig.OPTION_BIT_RATE, i);
    }
    
    @Override
    public Config getConfig() {
        return this.mConfig;
    }
    
    public int getIFrameInterval() {
        return (int)this.retrieveOption(VideoCaptureConfig.OPTION_INTRA_FRAME_INTERVAL);
    }
    
    public int getIFrameInterval(final int i) {
        return (int)this.retrieveOption(VideoCaptureConfig.OPTION_INTRA_FRAME_INTERVAL, i);
    }
    
    @Override
    public int getInputFormat() {
        return 34;
    }
    
    public int getVideoFrameRate() {
        return (int)this.retrieveOption(VideoCaptureConfig.OPTION_VIDEO_FRAME_RATE);
    }
    
    public int getVideoFrameRate(final int i) {
        return (int)this.retrieveOption(VideoCaptureConfig.OPTION_VIDEO_FRAME_RATE, i);
    }
    
    @Override
    public /* synthetic */ Object retrieveOption(final Option option) {
        return ReadableConfig$_CC.$default$retrieveOption(this, option);
    }
    
    @Override
    public /* synthetic */ Object retrieveOption(final Option option, final Object o) {
        return ReadableConfig$_CC.$default$retrieveOption(this, option, o);
    }
}

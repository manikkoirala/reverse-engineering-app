// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl;

import java.util.Iterator;
import android.util.ArrayMap;
import java.util.Map;

public class MutableTagBundle extends TagBundle
{
    private MutableTagBundle(final Map<String, Object> map) {
        super(map);
    }
    
    public static MutableTagBundle create() {
        return new MutableTagBundle((Map<String, Object>)new ArrayMap());
    }
    
    public static MutableTagBundle from(final TagBundle tagBundle) {
        final ArrayMap arrayMap = new ArrayMap();
        for (final String s : tagBundle.listKeys()) {
            ((Map<String, Object>)arrayMap).put(s, tagBundle.getTag(s));
        }
        return new MutableTagBundle((Map<String, Object>)arrayMap);
    }
    
    public void addTagBundle(final TagBundle tagBundle) {
        if (this.mTagMap != null && tagBundle.mTagMap != null) {
            this.mTagMap.putAll(tagBundle.mTagMap);
        }
    }
    
    public void putTag(final String s, final Object o) {
        this.mTagMap.put(s, o);
    }
}

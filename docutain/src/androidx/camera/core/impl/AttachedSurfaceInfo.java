// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl;

import android.util.Range;
import android.util.Size;

public abstract class AttachedSurfaceInfo
{
    AttachedSurfaceInfo() {
    }
    
    public static AttachedSurfaceInfo create(final SurfaceConfig surfaceConfig, final int n, final Size size, final Range<Integer> range) {
        return new AutoValue_AttachedSurfaceInfo(surfaceConfig, n, size, range);
    }
    
    public abstract int getImageFormat();
    
    public abstract Size getSize();
    
    public abstract SurfaceConfig getSurfaceConfig();
    
    public abstract Range<Integer> getTargetFrameRate();
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl;

import java.util.Collections;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.HashSet;
import java.util.Set;

public abstract class MultiValueSet<C>
{
    private Set<C> mSet;
    
    public MultiValueSet() {
        this.mSet = new HashSet<C>();
    }
    
    public void addAll(final List<C> list) {
        this.mSet.addAll((Collection<? extends C>)list);
    }
    
    public abstract MultiValueSet<C> clone();
    
    public List<C> getAllItems() {
        return Collections.unmodifiableList((List<? extends C>)new ArrayList<C>((Collection<? extends C>)this.mSet));
    }
}

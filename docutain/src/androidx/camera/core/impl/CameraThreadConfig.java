// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl;

import android.os.Handler;
import java.util.concurrent.Executor;

public abstract class CameraThreadConfig
{
    public static CameraThreadConfig create(final Executor executor, final Handler handler) {
        return new AutoValue_CameraThreadConfig(executor, handler);
    }
    
    public abstract Executor getCameraExecutor();
    
    public abstract Handler getSchedulerHandler();
}

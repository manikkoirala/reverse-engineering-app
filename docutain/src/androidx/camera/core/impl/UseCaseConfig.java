// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl;

import androidx.camera.core.ExtendableBuilder;
import android.util.Range;
import androidx.camera.core.CameraSelector;
import androidx.camera.core.internal.UseCaseEventConfig;
import androidx.camera.core.internal.TargetConfig;
import androidx.camera.core.UseCase;

public interface UseCaseConfig<T extends UseCase> extends TargetConfig<T>, UseCaseEventConfig, ImageInputConfig
{
    public static final Option<CameraSelector> OPTION_CAMERA_SELECTOR = Config.Option.create("camerax.core.useCase.cameraSelector", CameraSelector.class);
    public static final Option<CaptureConfig.OptionUnpacker> OPTION_CAPTURE_CONFIG_UNPACKER = Config.Option.create("camerax.core.useCase.captureConfigUnpacker", CaptureConfig.OptionUnpacker.class);
    public static final Option<CaptureConfig> OPTION_DEFAULT_CAPTURE_CONFIG = Config.Option.create("camerax.core.useCase.defaultCaptureConfig", CaptureConfig.class);
    public static final Option<SessionConfig> OPTION_DEFAULT_SESSION_CONFIG = Config.Option.create("camerax.core.useCase.defaultSessionConfig", SessionConfig.class);
    public static final Option<SessionConfig.OptionUnpacker> OPTION_SESSION_CONFIG_UNPACKER = Config.Option.create("camerax.core.useCase.sessionConfigUnpacker", SessionConfig.OptionUnpacker.class);
    public static final Option<Integer> OPTION_SURFACE_OCCUPANCY_PRIORITY = Config.Option.create("camerax.core.useCase.surfaceOccupancyPriority", Integer.TYPE);
    public static final Option<Range<Integer>> OPTION_TARGET_FRAME_RATE = Config.Option.create("camerax.core.useCase.targetFrameRate", CameraSelector.class);
    public static final Option<Boolean> OPTION_ZSL_DISABLED = Config.Option.create("camerax.core.useCase.zslDisabled", Boolean.TYPE);
    
    CameraSelector getCameraSelector();
    
    CameraSelector getCameraSelector(final CameraSelector p0);
    
    CaptureConfig.OptionUnpacker getCaptureOptionUnpacker();
    
    CaptureConfig.OptionUnpacker getCaptureOptionUnpacker(final CaptureConfig.OptionUnpacker p0);
    
    CaptureConfig getDefaultCaptureConfig();
    
    CaptureConfig getDefaultCaptureConfig(final CaptureConfig p0);
    
    SessionConfig getDefaultSessionConfig();
    
    SessionConfig getDefaultSessionConfig(final SessionConfig p0);
    
    SessionConfig.OptionUnpacker getSessionOptionUnpacker();
    
    SessionConfig.OptionUnpacker getSessionOptionUnpacker(final SessionConfig.OptionUnpacker p0);
    
    int getSurfaceOccupancyPriority();
    
    int getSurfaceOccupancyPriority(final int p0);
    
    Range<Integer> getTargetFramerate();
    
    Range<Integer> getTargetFramerate(final Range<Integer> p0);
    
    boolean isZslDisabled(final boolean p0);
    
    public interface Builder<T extends UseCase, C extends UseCaseConfig<T>, B> extends TargetConfig.Builder<T, B>, ExtendableBuilder<T>, UseCaseEventConfig.Builder<B>
    {
        C getUseCaseConfig();
        
        B setCameraSelector(final CameraSelector p0);
        
        B setCaptureOptionUnpacker(final CaptureConfig.OptionUnpacker p0);
        
        B setDefaultCaptureConfig(final CaptureConfig p0);
        
        B setDefaultSessionConfig(final SessionConfig p0);
        
        B setSessionOptionUnpacker(final SessionConfig.OptionUnpacker p0);
        
        B setSurfaceOccupancyPriority(final int p0);
        
        B setZslDisabled(final boolean p0);
    }
}

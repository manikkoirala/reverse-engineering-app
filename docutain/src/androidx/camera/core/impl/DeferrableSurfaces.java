// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.concurrent.ScheduledFuture;
import android.view.Surface;
import androidx.camera.core.impl.utils.futures.FutureCallback;
import java.util.concurrent.TimeUnit;
import java.util.Collection;
import androidx.camera.core.impl.utils.futures.Futures;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.Executor;
import java.util.concurrent.TimeoutException;
import androidx.concurrent.futures.CallbackToFutureAdapter;
import com.google.common.util.concurrent.ListenableFuture;
import java.util.Iterator;
import java.util.List;

public final class DeferrableSurfaces
{
    private DeferrableSurfaces() {
    }
    
    public static void decrementAll(final List<DeferrableSurface> list) {
        final Iterator<DeferrableSurface> iterator = list.iterator();
        while (iterator.hasNext()) {
            iterator.next().decrementUseCount();
        }
    }
    
    public static void incrementAll(final List<DeferrableSurface> list) throws DeferrableSurface.SurfaceClosedException {
        if (!list.isEmpty()) {
            int n = 0;
            while (true) {
                int n2 = n;
                try {
                    ((DeferrableSurface)list.get(n)).incrementUseCount();
                    final int n3 = n2 = n + 1;
                    final int size = list.size();
                    n = n3;
                    if (n3 < size) {
                        continue;
                    }
                }
                catch (final DeferrableSurface.SurfaceClosedException ex) {
                    for (int i = n2 - 1; i >= 0; --i) {
                        ((DeferrableSurface)list.get(i)).decrementUseCount();
                    }
                    throw ex;
                }
                break;
            }
        }
    }
    
    public static ListenableFuture<List<Surface>> surfaceListWithTimeout(final Collection<DeferrableSurface> collection, final boolean b, final long n, final Executor executor, final ScheduledExecutorService scheduledExecutorService) {
        final ArrayList list = new ArrayList();
        final Iterator<DeferrableSurface> iterator = collection.iterator();
        while (iterator.hasNext()) {
            list.add(Futures.nonCancellationPropagating(iterator.next().getSurface()));
        }
        return CallbackToFutureAdapter.getFuture((CallbackToFutureAdapter.Resolver<List<Surface>>)new DeferrableSurfaces$$ExternalSyntheticLambda3(list, scheduledExecutorService, executor, n, b));
    }
    
    public static boolean tryIncrementAll(final List<DeferrableSurface> list) {
        try {
            incrementAll(list);
            return true;
        }
        catch (final DeferrableSurface.SurfaceClosedException ex) {
            return false;
        }
    }
}

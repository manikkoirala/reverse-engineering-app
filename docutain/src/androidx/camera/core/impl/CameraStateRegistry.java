// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl;

import java.util.concurrent.RejectedExecutionException;
import java.util.Objects;
import java.util.concurrent.Executor;
import androidx.core.util.Preconditions;
import java.util.Iterator;
import java.util.Locale;
import androidx.camera.core.Logger;
import java.util.HashMap;
import androidx.camera.core.Camera;
import java.util.Map;

public final class CameraStateRegistry
{
    private static final String TAG = "CameraStateRegistry";
    private int mAvailableCameras;
    private final Map<Camera, CameraRegistration> mCameraStates;
    private final StringBuilder mDebugString;
    private final Object mLock;
    private final int mMaxAllowedOpenedCameras;
    
    public CameraStateRegistry(final int n) {
        this.mDebugString = new StringBuilder();
        this.mLock = new Object();
        this.mCameraStates = new HashMap<Camera, CameraRegistration>();
        this.mMaxAllowedOpenedCameras = n;
        synchronized ("mLock") {
            this.mAvailableCameras = n;
        }
    }
    
    private static boolean isOpen(final CameraInternal.State state) {
        return state != null && state.holdsCameraSlot();
    }
    
    private void recalculateAvailableCameras() {
        if (Logger.isDebugEnabled("CameraStateRegistry")) {
            this.mDebugString.setLength(0);
            this.mDebugString.append("Recalculating open cameras:\n");
            this.mDebugString.append(String.format(Locale.US, "%-45s%-22s\n", "Camera", "State"));
            this.mDebugString.append("-------------------------------------------------------------------\n");
        }
        final Iterator<Map.Entry<Camera, CameraRegistration>> iterator = this.mCameraStates.entrySet().iterator();
        int i = 0;
        while (iterator.hasNext()) {
            final Map.Entry<K, CameraRegistration> entry = (Map.Entry<K, CameraRegistration>)iterator.next();
            if (Logger.isDebugEnabled("CameraStateRegistry")) {
                String string;
                if (entry.getValue().getState() != null) {
                    string = entry.getValue().getState().toString();
                }
                else {
                    string = "UNKNOWN";
                }
                this.mDebugString.append(String.format(Locale.US, "%-45s%-22s\n", entry.getKey().toString(), string));
            }
            if (isOpen(entry.getValue().getState())) {
                ++i;
            }
        }
        if (Logger.isDebugEnabled("CameraStateRegistry")) {
            this.mDebugString.append("-------------------------------------------------------------------\n");
            this.mDebugString.append(String.format(Locale.US, "Open count: %d (Max allowed: %d)", i, this.mMaxAllowedOpenedCameras));
            Logger.d("CameraStateRegistry", this.mDebugString.toString());
        }
        this.mAvailableCameras = Math.max(this.mMaxAllowedOpenedCameras - i, 0);
    }
    
    private CameraInternal.State unregisterCamera(final Camera camera) {
        final CameraRegistration cameraRegistration = this.mCameraStates.remove(camera);
        if (cameraRegistration != null) {
            this.recalculateAvailableCameras();
            return cameraRegistration.getState();
        }
        return null;
    }
    
    private CameraInternal.State updateAndVerifyState(final Camera camera, final CameraInternal.State state) {
        final CameraInternal.State setState = Preconditions.checkNotNull(this.mCameraStates.get(camera), "Cannot update state of camera which has not yet been registered. Register with CameraStateRegistry.registerCamera()").setState(state);
        if (state == CameraInternal.State.OPENING) {
            Preconditions.checkState(isOpen(state) || setState == CameraInternal.State.OPENING, "Cannot mark camera as opening until camera was successful at calling CameraStateRegistry.tryOpenCamera()");
        }
        if (setState != state) {
            this.recalculateAvailableCameras();
        }
        return setState;
    }
    
    public boolean isCameraClosing() {
        synchronized (this.mLock) {
            final Iterator<Map.Entry<Camera, CameraRegistration>> iterator = this.mCameraStates.entrySet().iterator();
            while (iterator.hasNext()) {
                if (((Map.Entry<K, CameraRegistration>)iterator.next()).getValue().getState() == CameraInternal.State.CLOSING) {
                    return true;
                }
            }
            return false;
        }
    }
    
    public void markCameraState(final Camera camera, final CameraInternal.State state) {
        this.markCameraState(camera, state, true);
    }
    
    public void markCameraState(final Camera camera, final CameraInternal.State state, final boolean b) {
        synchronized (this.mLock) {
            final int mAvailableCameras = this.mAvailableCameras;
            CameraInternal.State state2;
            if (state == CameraInternal.State.RELEASED) {
                state2 = this.unregisterCamera(camera);
            }
            else {
                state2 = this.updateAndVerifyState(camera, state);
            }
            if (state2 == state) {
                return;
            }
            Map<Camera, CameraRegistration> map;
            if (mAvailableCameras < 1 && this.mAvailableCameras > 0) {
                final HashMap hashMap = new HashMap();
                final Iterator<Map.Entry<Camera, CameraRegistration>> iterator = this.mCameraStates.entrySet().iterator();
                while (true) {
                    map = hashMap;
                    if (!iterator.hasNext()) {
                        break;
                    }
                    final Map.Entry<K, CameraRegistration> entry = iterator.next();
                    if (entry.getValue().getState() != CameraInternal.State.PENDING_OPEN) {
                        continue;
                    }
                    hashMap.put(entry.getKey(), entry.getValue());
                }
            }
            else if (state == CameraInternal.State.PENDING_OPEN && this.mAvailableCameras > 0) {
                map = new HashMap<Camera, CameraRegistration>();
                map.put(camera, this.mCameraStates.get(camera));
            }
            else {
                map = null;
            }
            if (map != null && !b) {
                map.remove(camera);
            }
            monitorexit(this.mLock);
            if (map != null) {
                final Iterator<CameraRegistration> iterator2 = map.values().iterator();
                while (iterator2.hasNext()) {
                    iterator2.next().notifyListener();
                }
            }
        }
    }
    
    public void registerCamera(final Camera obj, final Executor executor, final OnOpenAvailableListener onOpenAvailableListener) {
        synchronized (this.mLock) {
            final boolean b = !this.mCameraStates.containsKey(obj);
            final StringBuilder sb = new StringBuilder();
            sb.append("Camera is already registered: ");
            sb.append(obj);
            Preconditions.checkState(b, sb.toString());
            this.mCameraStates.put(obj, new CameraRegistration(null, executor, onOpenAvailableListener));
        }
    }
    
    public boolean tryOpenCamera(final Camera camera) {
        synchronized (this.mLock) {
            final CameraRegistration cameraRegistration = Preconditions.checkNotNull(this.mCameraStates.get(camera), "Camera must first be registered with registerCamera()");
            if (Logger.isDebugEnabled("CameraStateRegistry")) {
                this.mDebugString.setLength(0);
                this.mDebugString.append(String.format(Locale.US, "tryOpenCamera(%s) [Available Cameras: %d, Already Open: %b (Previous state: %s)]", camera, this.mAvailableCameras, isOpen(cameraRegistration.getState()), cameraRegistration.getState()));
            }
            boolean b;
            if (this.mAvailableCameras <= 0 && !isOpen(cameraRegistration.getState())) {
                b = false;
            }
            else {
                cameraRegistration.setState(CameraInternal.State.OPENING);
                b = true;
            }
            if (Logger.isDebugEnabled("CameraStateRegistry")) {
                final StringBuilder mDebugString = this.mDebugString;
                final Locale us = Locale.US;
                String s;
                if (b) {
                    s = "SUCCESS";
                }
                else {
                    s = "FAIL";
                }
                mDebugString.append(String.format(us, " --> %s", s));
                Logger.d("CameraStateRegistry", this.mDebugString.toString());
            }
            if (b) {
                this.recalculateAvailableCameras();
            }
            return b;
        }
    }
    
    private static class CameraRegistration
    {
        private final OnOpenAvailableListener mCameraAvailableListener;
        private final Executor mNotifyExecutor;
        private CameraInternal.State mState;
        
        CameraRegistration(final CameraInternal.State mState, final Executor mNotifyExecutor, final OnOpenAvailableListener mCameraAvailableListener) {
            this.mState = mState;
            this.mNotifyExecutor = mNotifyExecutor;
            this.mCameraAvailableListener = mCameraAvailableListener;
        }
        
        CameraInternal.State getState() {
            return this.mState;
        }
        
        void notifyListener() {
            try {
                final Executor mNotifyExecutor = this.mNotifyExecutor;
                final OnOpenAvailableListener mCameraAvailableListener = this.mCameraAvailableListener;
                Objects.requireNonNull(mCameraAvailableListener);
                mNotifyExecutor.execute(new CameraStateRegistry$CameraRegistration$$ExternalSyntheticLambda0(mCameraAvailableListener));
            }
            catch (final RejectedExecutionException ex) {
                Logger.e("CameraStateRegistry", "Unable to notify camera.", ex);
            }
        }
        
        CameraInternal.State setState(final CameraInternal.State mState) {
            final CameraInternal.State mState2 = this.mState;
            this.mState = mState;
            return mState2;
        }
    }
    
    public interface OnOpenAvailableListener
    {
        void onOpenAvailable();
    }
}

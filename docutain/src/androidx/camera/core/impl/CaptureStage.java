// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl;

public interface CaptureStage
{
    CaptureConfig getCaptureConfig();
    
    int getId();
    
    public static final class DefaultCaptureStage implements CaptureStage
    {
        private final CaptureConfig mCaptureConfig;
        
        public DefaultCaptureStage() {
            this.mCaptureConfig = new CaptureConfig.Builder().build();
        }
        
        @Override
        public CaptureConfig getCaptureConfig() {
            return this.mCaptureConfig;
        }
        
        @Override
        public int getId() {
            return 0;
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl;

public class MutableStateObservable<T> extends StateObservable<T>
{
    private MutableStateObservable(final Object o, final boolean b) {
        super(o, b);
    }
    
    public static <T> MutableStateObservable<T> withInitialError(final Throwable t) {
        return new MutableStateObservable<T>(t, true);
    }
    
    public static <T> MutableStateObservable<T> withInitialState(final T t) {
        return new MutableStateObservable<T>(t, false);
    }
    
    public void setError(final Throwable t) {
        this.updateStateAsError(t);
    }
    
    public void setState(final T t) {
        this.updateState(t);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl;

import android.util.Size;
import android.view.Surface;
import com.google.common.util.concurrent.ListenableFuture;

public interface CaptureProcessor
{
    void close();
    
    ListenableFuture<Void> getCloseFuture();
    
    void onOutputSurface(final Surface p0, final int p1);
    
    void onResolutionUpdate(final Size p0);
    
    void process(final ImageProxyBundle p0);
}

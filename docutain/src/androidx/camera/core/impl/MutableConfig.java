// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl;

public interface MutableConfig extends Config
{
     <ValueT> void insertOption(final Option<ValueT> p0, final OptionPriority p1, final ValueT p2);
    
     <ValueT> void insertOption(final Option<ValueT> p0, final ValueT p1);
    
     <ValueT> ValueT removeOption(final Option<ValueT> p0);
}

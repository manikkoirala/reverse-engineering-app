// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl;

import java.util.Set;

public interface ReadableConfig extends Config
{
    boolean containsOption(final Option<?> p0);
    
    void findOptions(final String p0, final OptionMatcher p1);
    
    Config getConfig();
    
    OptionPriority getOptionPriority(final Option<?> p0);
    
    Set<OptionPriority> getPriorities(final Option<?> p0);
    
    Set<Option<?>> listOptions();
    
     <ValueT> ValueT retrieveOption(final Option<ValueT> p0);
    
     <ValueT> ValueT retrieveOption(final Option<ValueT> p0, final ValueT p1);
    
     <ValueT> ValueT retrieveOptionWithPriority(final Option<ValueT> p0, final OptionPriority p1);
}

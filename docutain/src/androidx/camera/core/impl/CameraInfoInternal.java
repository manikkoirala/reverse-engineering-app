// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl;

import androidx.camera.core.CameraSelector;
import java.util.concurrent.Executor;
import androidx.camera.core.CameraInfo;

public interface CameraInfoInternal extends CameraInfo
{
    void addSessionCaptureCallback(final Executor p0, final CameraCaptureCallback p1);
    
    CamcorderProfileProvider getCamcorderProfileProvider();
    
    String getCameraId();
    
    Quirks getCameraQuirks();
    
    CameraSelector getCameraSelector();
    
    Integer getLensFacing();
    
    Timebase getTimebase();
    
    void removeSessionCaptureCallback(final CameraCaptureCallback p0);
}

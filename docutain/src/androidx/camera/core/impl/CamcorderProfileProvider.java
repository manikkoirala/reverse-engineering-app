// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl;

public interface CamcorderProfileProvider
{
    public static final CamcorderProfileProvider EMPTY = new CamcorderProfileProvider() {
        @Override
        public CamcorderProfileProxy get(final int n) {
            return null;
        }
        
        @Override
        public boolean hasProfile(final int n) {
            return false;
        }
    };
    
    CamcorderProfileProxy get(final int p0);
    
    boolean hasProfile(final int p0);
}

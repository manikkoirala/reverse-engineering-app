// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl;

import java.util.Iterator;
import java.util.ArrayList;
import java.util.List;

public final class SurfaceCombination
{
    private final List<SurfaceConfig> mSurfaceConfigList;
    
    public SurfaceCombination() {
        this.mSurfaceConfigList = new ArrayList<SurfaceConfig>();
    }
    
    private static void generateArrangements(final List<int[]> list, final int n, final int[] array, final int n2) {
        if (n2 >= array.length) {
            list.add(array.clone());
            return;
        }
        int i = 0;
    Label_0024:
        while (i < n) {
            while (true) {
                for (int j = 0; j < n2; ++j) {
                    if (i == array[j]) {
                        final boolean b = true;
                        if (!b) {
                            array[n2] = i;
                            generateArrangements(list, n, array, n2 + 1);
                        }
                        ++i;
                        continue Label_0024;
                    }
                }
                final boolean b = false;
                continue;
            }
        }
    }
    
    private List<int[]> getElementsArrangements(final int n) {
        final ArrayList list = new ArrayList();
        generateArrangements(list, n, new int[n], 0);
        return list;
    }
    
    public boolean addSurfaceConfig(final SurfaceConfig surfaceConfig) {
        return this.mSurfaceConfigList.add(surfaceConfig);
    }
    
    public List<SurfaceConfig> getSurfaceConfigList() {
        return this.mSurfaceConfigList;
    }
    
    public boolean isSupported(final List<SurfaceConfig> list) {
        final boolean empty = list.isEmpty();
        boolean b = true;
        if (empty) {
            return true;
        }
        if (list.size() > this.mSurfaceConfigList.size()) {
            return false;
        }
        for (final int[] array : this.getElementsArrangements(this.mSurfaceConfigList.size())) {
            int n = 0;
            int n2 = 1;
            int n3;
            while (true) {
                n3 = n2;
                if (n >= this.mSurfaceConfigList.size()) {
                    break;
                }
                int n4 = n2;
                if (array[n] < list.size()) {
                    final boolean b2 = (n2 & (this.mSurfaceConfigList.get(n).isSupported(list.get(array[n])) ? 1 : 0)) != 0x0;
                    if ((n4 = (b2 ? 1 : 0)) == 0) {
                        n3 = (b2 ? 1 : 0);
                        break;
                    }
                }
                ++n;
                n2 = n4;
            }
            if (n3 != 0) {
                return b;
            }
        }
        b = false;
        return b;
    }
    
    public boolean removeSurfaceConfig(final SurfaceConfig surfaceConfig) {
        return this.mSurfaceConfigList.remove(surfaceConfig);
    }
}

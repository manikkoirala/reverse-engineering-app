// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl;

import java.util.Set;
import java.util.Iterator;
import android.util.Pair;
import android.util.ArrayMap;
import java.util.Map;

public class TagBundle
{
    private static final String CAMERAX_USER_TAG_PREFIX = "android.hardware.camera2.CaptureRequest.setTag.CX";
    private static final TagBundle EMPTY_TAGBUNDLE;
    private static final String USER_TAG_PREFIX = "android.hardware.camera2.CaptureRequest.setTag.";
    protected final Map<String, Object> mTagMap;
    
    static {
        EMPTY_TAGBUNDLE = new TagBundle((Map<String, Object>)new ArrayMap());
    }
    
    protected TagBundle(final Map<String, Object> mTagMap) {
        this.mTagMap = mTagMap;
    }
    
    public static TagBundle create(final Pair<String, Object> pair) {
        final ArrayMap arrayMap = new ArrayMap();
        ((Map<Object, Object>)arrayMap).put(pair.first, pair.second);
        return new TagBundle((Map<String, Object>)arrayMap);
    }
    
    public static TagBundle emptyBundle() {
        return TagBundle.EMPTY_TAGBUNDLE;
    }
    
    public static TagBundle from(final TagBundle tagBundle) {
        final ArrayMap arrayMap = new ArrayMap();
        for (final String s : tagBundle.listKeys()) {
            ((Map<String, Object>)arrayMap).put(s, tagBundle.getTag(s));
        }
        return new TagBundle((Map<String, Object>)arrayMap);
    }
    
    public Object getTag(final String s) {
        return this.mTagMap.get(s);
    }
    
    public Set<String> listKeys() {
        return this.mTagMap.keySet();
    }
    
    @Override
    public final String toString() {
        return "android.hardware.camera2.CaptureRequest.setTag.CX";
    }
}

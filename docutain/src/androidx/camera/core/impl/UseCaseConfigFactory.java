// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl;

import androidx.camera.core.InitializationException;
import android.content.Context;

public interface UseCaseConfigFactory
{
    public static final UseCaseConfigFactory EMPTY_INSTANCE = new UseCaseConfigFactory() {
        @Override
        public Config getConfig(final CaptureType captureType, final int n) {
            return null;
        }
    };
    
    Config getConfig(final CaptureType p0, final int p1);
    
    public enum CaptureType
    {
        private static final CaptureType[] $VALUES;
        
        IMAGE_ANALYSIS, 
        IMAGE_CAPTURE, 
        PREVIEW, 
        VIDEO_CAPTURE;
    }
    
    public interface Provider
    {
        UseCaseConfigFactory newInstance(final Context p0) throws InitializationException;
    }
}

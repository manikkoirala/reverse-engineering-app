// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl;

import java.util.List;

public interface CaptureBundle
{
    List<CaptureStage> getCaptureStages();
}

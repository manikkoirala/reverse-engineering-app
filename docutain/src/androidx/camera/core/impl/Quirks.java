// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl;

import java.util.Iterator;
import java.util.Collection;
import java.util.ArrayList;
import java.util.List;

public class Quirks
{
    private final List<Quirk> mQuirks;
    
    public Quirks(final List<Quirk> c) {
        this.mQuirks = new ArrayList<Quirk>(c);
    }
    
    public boolean contains(final Class<? extends Quirk> clazz) {
        final Iterator<Quirk> iterator = this.mQuirks.iterator();
        while (iterator.hasNext()) {
            if (clazz.isAssignableFrom(iterator.next().getClass())) {
                return true;
            }
        }
        return false;
    }
    
    public <T extends Quirk> T get(final Class<T> clazz) {
        for (final Quirk quirk : this.mQuirks) {
            if (quirk.getClass() == clazz) {
                return (T)quirk;
            }
        }
        return null;
    }
}

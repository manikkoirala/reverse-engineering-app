// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl;

public final class ImageFormatConstants
{
    public static final int INTERNAL_DEFINED_IMAGE_FORMAT_JPEG = 33;
    public static final int INTERNAL_DEFINED_IMAGE_FORMAT_PRIVATE = 34;
    
    private ImageFormatConstants() {
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl;

import android.media.CamcorderProfile;

public abstract class CamcorderProfileProxy
{
    public static int CODEC_PROFILE_NONE = -1;
    
    public static CamcorderProfileProxy create(final int n, final int n2, final int n3, final int n4, final int n5, final int n6, final int n7, final int n8, final int n9, final int n10, final int n11, final int n12) {
        return new AutoValue_CamcorderProfileProxy(n, n2, n3, n4, n5, n6, n7, n8, n9, n10, n11, n12);
    }
    
    public static CamcorderProfileProxy fromCamcorderProfile(final CamcorderProfile camcorderProfile) {
        return new AutoValue_CamcorderProfileProxy(camcorderProfile.duration, camcorderProfile.quality, camcorderProfile.fileFormat, camcorderProfile.videoCodec, camcorderProfile.videoBitRate, camcorderProfile.videoFrameRate, camcorderProfile.videoFrameWidth, camcorderProfile.videoFrameHeight, camcorderProfile.audioCodec, camcorderProfile.audioBitRate, camcorderProfile.audioSampleRate, camcorderProfile.audioChannels);
    }
    
    public abstract int getAudioBitRate();
    
    public abstract int getAudioChannels();
    
    public abstract int getAudioCodec();
    
    public String getAudioCodecMimeType() {
        switch (this.getAudioCodec()) {
            default: {
                return null;
            }
            case 7: {
                return "audio/opus";
            }
            case 6: {
                return "audio/vorbis";
            }
            case 3:
            case 4:
            case 5: {
                return "audio/mp4a-latm";
            }
            case 2: {
                return "audio/amr-wb";
            }
            case 1: {
                return "audio/3gpp";
            }
        }
    }
    
    public abstract int getAudioSampleRate();
    
    public abstract int getDuration();
    
    public abstract int getFileFormat();
    
    public abstract int getQuality();
    
    public int getRequiredAudioProfile() {
        final int audioCodec = this.getAudioCodec();
        if (audioCodec == 3) {
            return 2;
        }
        if (audioCodec == 4) {
            return 5;
        }
        if (audioCodec != 5) {
            return CamcorderProfileProxy.CODEC_PROFILE_NONE;
        }
        return 39;
    }
    
    public abstract int getVideoBitRate();
    
    public abstract int getVideoCodec();
    
    public String getVideoCodecMimeType() {
        final int videoCodec = this.getVideoCodec();
        if (videoCodec == 1) {
            return "video/3gpp";
        }
        if (videoCodec == 2) {
            return "video/avc";
        }
        if (videoCodec == 3) {
            return "video/mp4v-es";
        }
        if (videoCodec == 4) {
            return "video/x-vnd.on2.vp8";
        }
        if (videoCodec != 5) {
            return null;
        }
        return "video/hevc";
    }
    
    public abstract int getVideoFrameHeight();
    
    public abstract int getVideoFrameRate();
    
    public abstract int getVideoFrameWidth();
}

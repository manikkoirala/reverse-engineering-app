// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl;

import android.os.Handler;
import java.util.concurrent.Executor;

final class AutoValue_CameraThreadConfig extends CameraThreadConfig
{
    private final Executor cameraExecutor;
    private final Handler schedulerHandler;
    
    AutoValue_CameraThreadConfig(final Executor cameraExecutor, final Handler schedulerHandler) {
        if (cameraExecutor == null) {
            throw new NullPointerException("Null cameraExecutor");
        }
        this.cameraExecutor = cameraExecutor;
        if (schedulerHandler != null) {
            this.schedulerHandler = schedulerHandler;
            return;
        }
        throw new NullPointerException("Null schedulerHandler");
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o == this) {
            return true;
        }
        if (o instanceof CameraThreadConfig) {
            final CameraThreadConfig cameraThreadConfig = (CameraThreadConfig)o;
            if (!this.cameraExecutor.equals(cameraThreadConfig.getCameraExecutor()) || !this.schedulerHandler.equals(cameraThreadConfig.getSchedulerHandler())) {
                b = false;
            }
            return b;
        }
        return false;
    }
    
    @Override
    public Executor getCameraExecutor() {
        return this.cameraExecutor;
    }
    
    @Override
    public Handler getSchedulerHandler() {
        return this.schedulerHandler;
    }
    
    @Override
    public int hashCode() {
        return (this.cameraExecutor.hashCode() ^ 0xF4243) * 1000003 ^ this.schedulerHandler.hashCode();
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("CameraThreadConfig{cameraExecutor=");
        sb.append(this.cameraExecutor);
        sb.append(", schedulerHandler=");
        sb.append(this.schedulerHandler);
        sb.append("}");
        return sb.toString();
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl;

import android.util.Size;

public abstract class SurfaceSizeDefinition
{
    SurfaceSizeDefinition() {
    }
    
    public static SurfaceSizeDefinition create(final Size size, final Size size2, final Size size3) {
        return new AutoValue_SurfaceSizeDefinition(size, size2, size3);
    }
    
    public abstract Size getAnalysisSize();
    
    public abstract Size getPreviewSize();
    
    public abstract Size getRecordSize();
}

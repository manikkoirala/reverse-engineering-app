// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import androidx.camera.core.impl.utils.futures.Futures;
import com.google.common.util.concurrent.ListenableFuture;

public final class ConstantObservable<T> implements Observable<T>
{
    private static final ConstantObservable<Object> NULL_OBSERVABLE;
    private static final String TAG = "ConstantObservable";
    private final ListenableFuture<T> mValueFuture;
    
    static {
        NULL_OBSERVABLE = new ConstantObservable<Object>(null);
    }
    
    private ConstantObservable(final T t) {
        this.mValueFuture = Futures.immediateFuture(t);
    }
    
    public static <U> Observable<U> withValue(final U u) {
        if (u == null) {
            return (Observable<U>)ConstantObservable.NULL_OBSERVABLE;
        }
        return new ConstantObservable<U>(u);
    }
    
    @Override
    public void addObserver(final Executor executor, final Observer<? super T> observer) {
        this.mValueFuture.addListener((Runnable)new ConstantObservable$$ExternalSyntheticLambda0(this, observer), executor);
    }
    
    @Override
    public ListenableFuture<T> fetchData() {
        return this.mValueFuture;
    }
    
    @Override
    public void removeObserver(final Observer<? super T> observer) {
    }
}

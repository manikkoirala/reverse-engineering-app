// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl;

public interface ConfigProvider<C extends Config>
{
    C getConfig();
}

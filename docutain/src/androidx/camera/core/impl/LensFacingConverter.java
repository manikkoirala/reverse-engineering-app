// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl;

public class LensFacingConverter
{
    private LensFacingConverter() {
    }
    
    public static String nameOf(final int i) {
        if (i == 0) {
            return "FRONT";
        }
        if (i == 1) {
            return "BACK";
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Unknown lens facing ");
        sb.append(i);
        throw new IllegalArgumentException(sb.toString());
    }
    
    public static int valueOf(final String str) {
        if (str == null) {
            throw new NullPointerException("name cannot be null");
        }
        str.hashCode();
        if (str.equals("BACK")) {
            return 1;
        }
        if (str.equals("FRONT")) {
            return 0;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Unknown len facing name ");
        sb.append(str);
        throw new IllegalArgumentException(sb.toString());
    }
    
    public static Integer[] values() {
        return new Integer[] { 0, 1 };
    }
}

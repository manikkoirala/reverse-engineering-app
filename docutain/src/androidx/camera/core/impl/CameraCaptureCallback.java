// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl;

public abstract class CameraCaptureCallback
{
    public void onCaptureCancelled() {
    }
    
    public void onCaptureCompleted(final CameraCaptureResult cameraCaptureResult) {
    }
    
    public void onCaptureFailed(final CameraCaptureFailure cameraCaptureFailure) {
    }
}

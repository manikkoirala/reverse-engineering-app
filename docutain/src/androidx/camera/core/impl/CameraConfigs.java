// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl;

import java.util.Set;

public class CameraConfigs
{
    private static final CameraConfig EMPTY_CONFIG;
    
    static {
        EMPTY_CONFIG = new EmptyCameraConfig();
    }
    
    private CameraConfigs() {
    }
    
    public static CameraConfig emptyConfig() {
        return CameraConfigs.EMPTY_CONFIG;
    }
    
    static final class EmptyCameraConfig implements CameraConfig
    {
        private final Identifier mIdentifier;
        
        EmptyCameraConfig() {
            this.mIdentifier = Identifier.create(new Object());
        }
        
        @Override
        public Identifier getCompatibilityId() {
            return this.mIdentifier;
        }
        
        @Override
        public Config getConfig() {
            return OptionsBundle.emptyBundle();
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl;

import androidx.camera.core.impl.utils.futures.Futures;
import android.view.Surface;
import androidx.camera.core.impl.utils.executor.CameraXExecutors;
import android.util.Log;
import androidx.camera.core.Logger;
import com.google.common.util.concurrent.ListenableFuture;
import androidx.concurrent.futures.CallbackToFutureAdapter;
import java.util.concurrent.atomic.AtomicInteger;
import android.util.Size;

public abstract class DeferrableSurface
{
    private static final boolean DEBUG;
    public static final Size SIZE_UNDEFINED;
    private static final String TAG = "DeferrableSurface";
    private static final AtomicInteger TOTAL_COUNT;
    private static final AtomicInteger USED_COUNT;
    private boolean mClosed;
    Class<?> mContainerClass;
    private final Object mLock;
    private final Size mPrescribedSize;
    private final int mPrescribedStreamFormat;
    private CallbackToFutureAdapter.Completer<Void> mTerminationCompleter;
    private final ListenableFuture<Void> mTerminationFuture;
    private int mUseCount;
    
    static {
        SIZE_UNDEFINED = new Size(0, 0);
        DEBUG = Logger.isDebugEnabled("DeferrableSurface");
        USED_COUNT = new AtomicInteger(0);
        TOTAL_COUNT = new AtomicInteger(0);
    }
    
    public DeferrableSurface() {
        this(DeferrableSurface.SIZE_UNDEFINED, 0);
    }
    
    public DeferrableSurface(final Size mPrescribedSize, final int mPrescribedStreamFormat) {
        this.mLock = new Object();
        this.mUseCount = 0;
        this.mClosed = false;
        this.mPrescribedSize = mPrescribedSize;
        this.mPrescribedStreamFormat = mPrescribedStreamFormat;
        final com.google.common.util.concurrent.ListenableFuture<Void> future = CallbackToFutureAdapter.getFuture((CallbackToFutureAdapter.Resolver<Void>)new DeferrableSurface$$ExternalSyntheticLambda0(this));
        this.mTerminationFuture = future;
        if (Logger.isDebugEnabled("DeferrableSurface")) {
            this.printGlobalDebugCounts("Surface created", DeferrableSurface.TOTAL_COUNT.incrementAndGet(), DeferrableSurface.USED_COUNT.get());
            future.addListener((Runnable)new DeferrableSurface$$ExternalSyntheticLambda1(this, Log.getStackTraceString((Throwable)new Exception())), CameraXExecutors.directExecutor());
        }
    }
    
    private void printGlobalDebugCounts(final String str, final int i, final int j) {
        if (!DeferrableSurface.DEBUG && Logger.isDebugEnabled("DeferrableSurface")) {
            Logger.d("DeferrableSurface", "DeferrableSurface usage statistics may be inaccurate since debug logging was not enabled at static initialization time. App restart may be required to enable accurate usage statistics.");
        }
        final StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append("[total_surfaces=");
        sb.append(i);
        sb.append(", used_surfaces=");
        sb.append(j);
        sb.append("](");
        sb.append(this);
        sb.append("}");
        Logger.d("DeferrableSurface", sb.toString());
    }
    
    public void close() {
        synchronized (this.mLock) {
            Object o;
            if (!this.mClosed) {
                this.mClosed = true;
                Object mTerminationCompleter;
                if (this.mUseCount == 0) {
                    mTerminationCompleter = this.mTerminationCompleter;
                    this.mTerminationCompleter = null;
                }
                else {
                    mTerminationCompleter = null;
                }
                o = mTerminationCompleter;
                if (Logger.isDebugEnabled("DeferrableSurface")) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("surface closed,  useCount=");
                    sb.append(this.mUseCount);
                    sb.append(" closed=true ");
                    sb.append(this);
                    Logger.d("DeferrableSurface", sb.toString());
                    o = mTerminationCompleter;
                }
            }
            else {
                o = null;
            }
            monitorexit(this.mLock);
            if (o != null) {
                ((CallbackToFutureAdapter.Completer<Object>)o).set(null);
            }
        }
    }
    
    public void decrementUseCount() {
        synchronized (this.mLock) {
            int mUseCount = this.mUseCount;
            if (mUseCount != 0) {
                --mUseCount;
                this.mUseCount = mUseCount;
                Object mTerminationCompleter;
                if (mUseCount == 0 && this.mClosed) {
                    mTerminationCompleter = this.mTerminationCompleter;
                    this.mTerminationCompleter = null;
                }
                else {
                    mTerminationCompleter = null;
                }
                if (Logger.isDebugEnabled("DeferrableSurface")) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("use count-1,  useCount=");
                    sb.append(this.mUseCount);
                    sb.append(" closed=");
                    sb.append(this.mClosed);
                    sb.append(" ");
                    sb.append(this);
                    Logger.d("DeferrableSurface", sb.toString());
                    if (this.mUseCount == 0) {
                        this.printGlobalDebugCounts("Surface no longer in use", DeferrableSurface.TOTAL_COUNT.get(), DeferrableSurface.USED_COUNT.decrementAndGet());
                    }
                }
                monitorexit(this.mLock);
                if (mTerminationCompleter != null) {
                    ((CallbackToFutureAdapter.Completer<Object>)mTerminationCompleter).set(null);
                }
                return;
            }
            throw new IllegalStateException("Decrementing use count occurs more times than incrementing");
        }
    }
    
    public Class<?> getContainerClass() {
        return this.mContainerClass;
    }
    
    public Size getPrescribedSize() {
        return this.mPrescribedSize;
    }
    
    public int getPrescribedStreamFormat() {
        return this.mPrescribedStreamFormat;
    }
    
    public final ListenableFuture<Surface> getSurface() {
        synchronized (this.mLock) {
            if (this.mClosed) {
                return Futures.immediateFailedFuture(new SurfaceClosedException("DeferrableSurface already closed.", this));
            }
            return this.provideSurface();
        }
    }
    
    public ListenableFuture<Void> getTerminationFuture() {
        return Futures.nonCancellationPropagating(this.mTerminationFuture);
    }
    
    public int getUseCount() {
        synchronized (this.mLock) {
            return this.mUseCount;
        }
    }
    
    public void incrementUseCount() throws SurfaceClosedException {
        synchronized (this.mLock) {
            final int mUseCount = this.mUseCount;
            if (mUseCount == 0 && this.mClosed) {
                throw new SurfaceClosedException("Cannot begin use on a closed surface.", this);
            }
            this.mUseCount = mUseCount + 1;
            if (Logger.isDebugEnabled("DeferrableSurface")) {
                if (this.mUseCount == 1) {
                    this.printGlobalDebugCounts("New surface in use", DeferrableSurface.TOTAL_COUNT.get(), DeferrableSurface.USED_COUNT.incrementAndGet());
                }
                final StringBuilder sb = new StringBuilder();
                sb.append("use count+1, useCount=");
                sb.append(this.mUseCount);
                sb.append(" ");
                sb.append(this);
                Logger.d("DeferrableSurface", sb.toString());
            }
        }
    }
    
    protected abstract ListenableFuture<Surface> provideSurface();
    
    public void setContainerClass(final Class<?> mContainerClass) {
        this.mContainerClass = mContainerClass;
    }
    
    public static final class SurfaceClosedException extends Exception
    {
        DeferrableSurface mDeferrableSurface;
        
        public SurfaceClosedException(final String message, final DeferrableSurface mDeferrableSurface) {
            super(message);
            this.mDeferrableSurface = mDeferrableSurface;
        }
        
        public DeferrableSurface getDeferrableSurface() {
            return this.mDeferrableSurface;
        }
    }
    
    public static final class SurfaceUnavailableException extends Exception
    {
        public SurfaceUnavailableException(final String message) {
            super(message);
        }
    }
}

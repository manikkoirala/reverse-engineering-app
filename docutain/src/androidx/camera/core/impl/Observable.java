// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl;

import com.google.common.util.concurrent.ListenableFuture;
import java.util.concurrent.Executor;

public interface Observable<T>
{
    void addObserver(final Executor p0, final Observer<? super T> p1);
    
    ListenableFuture<T> fetchData();
    
    void removeObserver(final Observer<? super T> p0);
    
    public interface Observer<T>
    {
        void onError(final Throwable p0);
        
        void onNewData(final T p0);
    }
}

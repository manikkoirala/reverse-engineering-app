// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl;

import androidx.camera.core.ImageInfo;

public interface ImageInfoProcessor
{
    CaptureStage getCaptureStage();
    
    boolean process(final ImageInfo p0);
}

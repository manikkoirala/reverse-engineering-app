// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl;

import androidx.camera.core.impl.utils.ExifData;
import android.hardware.camera2.CaptureResult;

public interface CameraCaptureResult
{
    CameraCaptureMetaData.AeState getAeState();
    
    CameraCaptureMetaData.AfMode getAfMode();
    
    CameraCaptureMetaData.AfState getAfState();
    
    CameraCaptureMetaData.AwbState getAwbState();
    
    CaptureResult getCaptureResult();
    
    CameraCaptureMetaData.FlashState getFlashState();
    
    TagBundle getTagBundle();
    
    long getTimestamp();
    
    void populateExifData(final ExifData.Builder p0);
    
    public static final class EmptyCameraCaptureResult implements CameraCaptureResult
    {
        public static CameraCaptureResult create() {
            return new EmptyCameraCaptureResult();
        }
        
        @Override
        public CameraCaptureMetaData.AeState getAeState() {
            return CameraCaptureMetaData.AeState.UNKNOWN;
        }
        
        @Override
        public CameraCaptureMetaData.AfMode getAfMode() {
            return CameraCaptureMetaData.AfMode.UNKNOWN;
        }
        
        @Override
        public CameraCaptureMetaData.AfState getAfState() {
            return CameraCaptureMetaData.AfState.UNKNOWN;
        }
        
        @Override
        public CameraCaptureMetaData.AwbState getAwbState() {
            return CameraCaptureMetaData.AwbState.UNKNOWN;
        }
        
        @Override
        public CameraCaptureMetaData.FlashState getFlashState() {
            return CameraCaptureMetaData.FlashState.UNKNOWN;
        }
        
        @Override
        public TagBundle getTagBundle() {
            return TagBundle.emptyBundle();
        }
        
        @Override
        public long getTimestamp() {
            return -1L;
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl;

import androidx.core.util.Preconditions;
import androidx.camera.core.InitializationException;
import androidx.camera.core.CameraUnavailableException;
import androidx.camera.core.Logger;
import java.util.LinkedHashSet;
import java.util.Iterator;
import androidx.camera.core.impl.utils.executor.CameraXExecutors;
import java.util.Collection;
import androidx.camera.core.impl.utils.futures.Futures;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Set;
import com.google.common.util.concurrent.ListenableFuture;
import androidx.concurrent.futures.CallbackToFutureAdapter;
import java.util.Map;

public final class CameraRepository
{
    private static final String TAG = "CameraRepository";
    private final Map<String, CameraInternal> mCameras;
    private final Object mCamerasLock;
    private CallbackToFutureAdapter.Completer<Void> mDeinitCompleter;
    private ListenableFuture<Void> mDeinitFuture;
    private final Set<CameraInternal> mReleasingCameras;
    
    public CameraRepository() {
        this.mCamerasLock = new Object();
        this.mCameras = new LinkedHashMap<String, CameraInternal>();
        this.mReleasingCameras = new HashSet<CameraInternal>();
    }
    
    public ListenableFuture<Void> deinit() {
        synchronized (this.mCamerasLock) {
            if (this.mCameras.isEmpty()) {
                ListenableFuture<Void> listenableFuture;
                if ((listenableFuture = this.mDeinitFuture) == null) {
                    listenableFuture = Futures.immediateFuture((Void)null);
                }
                return listenableFuture;
            }
            ListenableFuture<Void> mDeinitFuture;
            if ((mDeinitFuture = this.mDeinitFuture) == null) {
                mDeinitFuture = CallbackToFutureAdapter.getFuture((CallbackToFutureAdapter.Resolver<Void>)new CameraRepository$$ExternalSyntheticLambda0(this));
                this.mDeinitFuture = mDeinitFuture;
            }
            this.mReleasingCameras.addAll(this.mCameras.values());
            for (final CameraInternal cameraInternal : this.mCameras.values()) {
                cameraInternal.release().addListener((Runnable)new CameraRepository$$ExternalSyntheticLambda1(this, cameraInternal), CameraXExecutors.directExecutor());
            }
            this.mCameras.clear();
            return mDeinitFuture;
        }
    }
    
    public CameraInternal getCamera(final String str) {
        synchronized (this.mCamerasLock) {
            final CameraInternal cameraInternal = this.mCameras.get(str);
            if (cameraInternal != null) {
                return cameraInternal;
            }
            final StringBuilder sb = new StringBuilder();
            sb.append("Invalid camera: ");
            sb.append(str);
            throw new IllegalArgumentException(sb.toString());
        }
    }
    
    Set<String> getCameraIds() {
        synchronized (this.mCamerasLock) {
            return new LinkedHashSet<String>(this.mCameras.keySet());
        }
    }
    
    public LinkedHashSet<CameraInternal> getCameras() {
        synchronized (this.mCamerasLock) {
            return new LinkedHashSet<CameraInternal>(this.mCameras.values());
        }
    }
    
    public void init(final CameraFactory cameraFactory) throws InitializationException {
        final Object mCamerasLock = this.mCamerasLock;
        monitorenter(mCamerasLock);
        try {
            try {
                for (final String str : cameraFactory.getAvailableCameraIds()) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Added camera: ");
                    sb.append(str);
                    Logger.d("CameraRepository", sb.toString());
                    this.mCameras.put(str, cameraFactory.getCamera(str));
                }
                monitorexit(mCamerasLock);
            }
            finally {
                monitorexit(mCamerasLock);
            }
        }
        catch (final CameraUnavailableException ex) {}
    }
}

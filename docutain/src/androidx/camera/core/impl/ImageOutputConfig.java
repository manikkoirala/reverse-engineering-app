// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl;

import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;
import androidx.camera.core.AspectRatio;
import android.util.Pair;
import java.util.List;
import android.util.Size;

public interface ImageOutputConfig extends ReadableConfig
{
    public static final int INVALID_ROTATION = -1;
    public static final Option<Integer> OPTION_APP_TARGET_ROTATION = Config.Option.create("camerax.core.imageOutput.appTargetRotation", Integer.TYPE);
    public static final Option<Size> OPTION_DEFAULT_RESOLUTION = Config.Option.create("camerax.core.imageOutput.defaultResolution", Size.class);
    public static final Option<Size> OPTION_MAX_RESOLUTION = Config.Option.create("camerax.core.imageOutput.maxResolution", Size.class);
    public static final Option<List<Pair<Integer, Size[]>>> OPTION_SUPPORTED_RESOLUTIONS = Config.Option.create("camerax.core.imageOutput.supportedResolutions", List.class);
    public static final Option<Integer> OPTION_TARGET_ASPECT_RATIO = Config.Option.create("camerax.core.imageOutput.targetAspectRatio", AspectRatio.class);
    public static final Option<Size> OPTION_TARGET_RESOLUTION = Config.Option.create("camerax.core.imageOutput.targetResolution", Size.class);
    public static final Option<Integer> OPTION_TARGET_ROTATION = Config.Option.create("camerax.core.imageOutput.targetRotation", Integer.TYPE);
    public static final int ROTATION_NOT_SPECIFIED = -1;
    
    int getAppTargetRotation(final int p0);
    
    Size getDefaultResolution();
    
    Size getDefaultResolution(final Size p0);
    
    Size getMaxResolution();
    
    Size getMaxResolution(final Size p0);
    
    List<Pair<Integer, Size[]>> getSupportedResolutions();
    
    List<Pair<Integer, Size[]>> getSupportedResolutions(final List<Pair<Integer, Size[]>> p0);
    
    int getTargetAspectRatio();
    
    Size getTargetResolution();
    
    Size getTargetResolution(final Size p0);
    
    int getTargetRotation();
    
    int getTargetRotation(final int p0);
    
    boolean hasTargetAspectRatio();
    
    public interface Builder<B>
    {
        B setDefaultResolution(final Size p0);
        
        B setMaxResolution(final Size p0);
        
        B setSupportedResolutions(final List<Pair<Integer, Size[]>> p0);
        
        B setTargetAspectRatio(final int p0);
        
        B setTargetResolution(final Size p0);
        
        B setTargetRotation(final int p0);
    }
    
    @Retention(RetentionPolicy.SOURCE)
    public @interface OptionalRotationValue {
    }
    
    @Retention(RetentionPolicy.SOURCE)
    public @interface RotationDegreesValue {
    }
    
    @Retention(RetentionPolicy.SOURCE)
    public @interface RotationValue {
    }
}

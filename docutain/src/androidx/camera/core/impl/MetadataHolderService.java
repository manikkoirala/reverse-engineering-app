// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl;

import android.os.IBinder;
import android.content.Intent;
import android.app.Service;

public class MetadataHolderService extends Service
{
    private MetadataHolderService() {
    }
    
    public IBinder onBind(final Intent intent) {
        throw new UnsupportedOperationException();
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl;

import java.util.Set;

public interface Config
{
    boolean containsOption(final Option<?> p0);
    
    void findOptions(final String p0, final OptionMatcher p1);
    
    OptionPriority getOptionPriority(final Option<?> p0);
    
    Set<OptionPriority> getPriorities(final Option<?> p0);
    
    Set<Option<?>> listOptions();
    
     <ValueT> ValueT retrieveOption(final Option<ValueT> p0);
    
     <ValueT> ValueT retrieveOption(final Option<ValueT> p0, final ValueT p1);
    
     <ValueT> ValueT retrieveOptionWithPriority(final Option<ValueT> p0, final OptionPriority p1);
    
    public abstract static class Option<T>
    {
        Option() {
        }
        
        public static <T> Option<T> create(final String s, final Class<?> clazz) {
            return create(s, clazz, null);
        }
        
        public static <T> Option<T> create(final String s, final Class<?> clazz, final Object o) {
            return (Option<T>)new AutoValue_Config_Option(s, (Class<Object>)clazz, o);
        }
        
        public abstract String getId();
        
        public abstract Object getToken();
        
        public abstract Class<T> getValueClass();
    }
    
    public interface OptionMatcher
    {
        boolean onOptionMatched(final Option<?> p0);
    }
    
    public enum OptionPriority
    {
        private static final OptionPriority[] $VALUES;
        
        ALWAYS_OVERRIDE, 
        OPTIONAL, 
        REQUIRED;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl;

import java.util.Collections;
import java.util.List;
import androidx.camera.core.FocusMeteringResult;
import androidx.camera.core.FocusMeteringAction;
import android.graphics.Rect;
import androidx.camera.core.impl.utils.futures.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import androidx.camera.core.CameraControl;

public interface CameraControlInternal extends CameraControl
{
    public static final CameraControlInternal DEFAULT_EMPTY_INSTANCE = new CameraControlInternal() {
        @Override
        public void addInteropConfig(final Config config) {
        }
        
        @Override
        public void addZslConfig(final SessionConfig.Builder builder) {
        }
        
        @Override
        public ListenableFuture<Void> cancelFocusAndMetering() {
            return Futures.immediateFuture((Void)null);
        }
        
        @Override
        public void clearInteropConfig() {
        }
        
        @Override
        public ListenableFuture<Void> enableTorch(final boolean b) {
            return Futures.immediateFuture((Void)null);
        }
        
        @Override
        public int getFlashMode() {
            return 2;
        }
        
        @Override
        public Config getInteropConfig() {
            return null;
        }
        
        @Override
        public Rect getSensorRect() {
            return new Rect();
        }
        
        @Override
        public SessionConfig getSessionConfig() {
            return SessionConfig.defaultEmptySessionConfig();
        }
        
        @Override
        public boolean isZslDisabledByByUserCaseConfig() {
            return false;
        }
        
        @Override
        public ListenableFuture<Integer> setExposureCompensationIndex(final int n) {
            return Futures.immediateFuture(0);
        }
        
        @Override
        public void setFlashMode(final int n) {
        }
        
        @Override
        public ListenableFuture<Void> setLinearZoom(final float n) {
            return Futures.immediateFuture((Void)null);
        }
        
        @Override
        public ListenableFuture<Void> setZoomRatio(final float n) {
            return Futures.immediateFuture((Void)null);
        }
        
        @Override
        public void setZslDisabledByUserCaseConfig(final boolean b) {
        }
        
        @Override
        public ListenableFuture<FocusMeteringResult> startFocusAndMetering(final FocusMeteringAction focusMeteringAction) {
            return Futures.immediateFuture(FocusMeteringResult.emptyInstance());
        }
        
        @Override
        public ListenableFuture<List<Void>> submitStillCaptureRequests(final List<CaptureConfig> list, final int n, final int n2) {
            return Futures.immediateFuture(Collections.emptyList());
        }
    };
    
    void addInteropConfig(final Config p0);
    
    void addZslConfig(final SessionConfig.Builder p0);
    
    void clearInteropConfig();
    
    int getFlashMode();
    
    Config getInteropConfig();
    
    Rect getSensorRect();
    
    SessionConfig getSessionConfig();
    
    boolean isZslDisabledByByUserCaseConfig();
    
    void setFlashMode(final int p0);
    
    void setZslDisabledByUserCaseConfig(final boolean p0);
    
    ListenableFuture<List<Void>> submitStillCaptureRequests(final List<CaptureConfig> p0, final int p1, final int p2);
    
    public static final class CameraControlException extends Exception
    {
        private CameraCaptureFailure mCameraCaptureFailure;
        
        public CameraControlException(final CameraCaptureFailure mCameraCaptureFailure) {
            this.mCameraCaptureFailure = mCameraCaptureFailure;
        }
        
        public CameraControlException(final CameraCaptureFailure mCameraCaptureFailure, final Throwable cause) {
            super(cause);
            this.mCameraCaptureFailure = mCameraCaptureFailure;
        }
        
        public CameraCaptureFailure getCameraCaptureFailure() {
            return this.mCameraCaptureFailure;
        }
    }
    
    public interface ControlUpdateCallback
    {
        void onCameraControlCaptureRequests(final List<CaptureConfig> p0);
        
        void onCameraControlUpdateSessionConfig();
    }
}

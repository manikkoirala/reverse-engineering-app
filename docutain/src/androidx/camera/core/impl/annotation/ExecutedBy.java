// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Target;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;

@Retention(RetentionPolicy.SOURCE)
@Target({ ElementType.METHOD })
public @interface ExecutedBy {
    String value();
}

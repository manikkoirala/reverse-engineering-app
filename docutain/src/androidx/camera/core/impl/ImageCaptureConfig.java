// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl;

import androidx.camera.core.internal.UseCaseEventConfig;
import androidx.camera.core.internal.UseCaseEventConfig$_CC;
import androidx.camera.core.UseCase;
import android.util.Range;
import androidx.camera.core.internal.TargetConfig;
import androidx.camera.core.internal.TargetConfig$_CC;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Executor;
import android.util.Size;
import androidx.camera.core.CameraSelector;
import androidx.camera.core.ImageReaderProxyProvider;
import androidx.camera.core.internal.IoConfig;
import androidx.camera.core.ImageCapture;

public final class ImageCaptureConfig implements UseCaseConfig<ImageCapture>, ImageOutputConfig, IoConfig
{
    public static final Option<Integer> OPTION_BUFFER_FORMAT;
    public static final Option<CaptureBundle> OPTION_CAPTURE_BUNDLE;
    public static final Option<CaptureProcessor> OPTION_CAPTURE_PROCESSOR;
    public static final Option<Integer> OPTION_FLASH_MODE;
    public static final Option<Integer> OPTION_FLASH_TYPE;
    public static final Option<Integer> OPTION_IMAGE_CAPTURE_MODE;
    public static final Option<ImageReaderProxyProvider> OPTION_IMAGE_READER_PROXY_PROVIDER;
    public static final Option<Integer> OPTION_JPEG_COMPRESSION_QUALITY;
    public static final Option<Integer> OPTION_MAX_CAPTURE_STAGES;
    public static final Option<Boolean> OPTION_USE_SOFTWARE_JPEG_ENCODER;
    private final OptionsBundle mConfig;
    
    static {
        OPTION_IMAGE_CAPTURE_MODE = (Option)Config.Option.create("camerax.core.imageCapture.captureMode", Integer.TYPE);
        OPTION_FLASH_MODE = (Option)Config.Option.create("camerax.core.imageCapture.flashMode", Integer.TYPE);
        OPTION_CAPTURE_BUNDLE = (Option)Config.Option.create("camerax.core.imageCapture.captureBundle", CaptureBundle.class);
        OPTION_CAPTURE_PROCESSOR = (Option)Config.Option.create("camerax.core.imageCapture.captureProcessor", CaptureProcessor.class);
        OPTION_BUFFER_FORMAT = (Option)Config.Option.create("camerax.core.imageCapture.bufferFormat", Integer.class);
        OPTION_MAX_CAPTURE_STAGES = (Option)Config.Option.create("camerax.core.imageCapture.maxCaptureStages", Integer.class);
        OPTION_IMAGE_READER_PROXY_PROVIDER = (Option)Config.Option.create("camerax.core.imageCapture.imageReaderProxyProvider", ImageReaderProxyProvider.class);
        OPTION_USE_SOFTWARE_JPEG_ENCODER = (Option)Config.Option.create("camerax.core.imageCapture.useSoftwareJpegEncoder", Boolean.TYPE);
        OPTION_FLASH_TYPE = (Option)Config.Option.create("camerax.core.imageCapture.flashType", Integer.TYPE);
        OPTION_JPEG_COMPRESSION_QUALITY = (Option)Config.Option.create("camerax.core.imageCapture.jpegCompressionQuality", Integer.TYPE);
    }
    
    public ImageCaptureConfig(final OptionsBundle mConfig) {
        this.mConfig = mConfig;
    }
    
    @Override
    public /* synthetic */ boolean containsOption(final Option option) {
        return ReadableConfig$_CC.$default$containsOption(this, option);
    }
    
    public Integer getBufferFormat() {
        return (Integer)this.retrieveOption(ImageCaptureConfig.OPTION_BUFFER_FORMAT);
    }
    
    public Integer getBufferFormat(final Integer n) {
        return (Integer)this.retrieveOption(ImageCaptureConfig.OPTION_BUFFER_FORMAT, n);
    }
    
    public CaptureBundle getCaptureBundle() {
        return (CaptureBundle)this.retrieveOption(ImageCaptureConfig.OPTION_CAPTURE_BUNDLE);
    }
    
    public CaptureBundle getCaptureBundle(final CaptureBundle captureBundle) {
        return (CaptureBundle)this.retrieveOption(ImageCaptureConfig.OPTION_CAPTURE_BUNDLE, captureBundle);
    }
    
    public int getCaptureMode() {
        return (int)this.retrieveOption(ImageCaptureConfig.OPTION_IMAGE_CAPTURE_MODE);
    }
    
    public CaptureProcessor getCaptureProcessor() {
        return (CaptureProcessor)this.retrieveOption(ImageCaptureConfig.OPTION_CAPTURE_PROCESSOR);
    }
    
    public CaptureProcessor getCaptureProcessor(final CaptureProcessor captureProcessor) {
        return (CaptureProcessor)this.retrieveOption(ImageCaptureConfig.OPTION_CAPTURE_PROCESSOR, captureProcessor);
    }
    
    @Override
    public Config getConfig() {
        return this.mConfig;
    }
    
    public int getFlashMode() {
        return (int)this.retrieveOption(ImageCaptureConfig.OPTION_FLASH_MODE);
    }
    
    public int getFlashMode(final int i) {
        return (int)this.retrieveOption(ImageCaptureConfig.OPTION_FLASH_MODE, i);
    }
    
    public int getFlashType() {
        return (int)this.retrieveOption(ImageCaptureConfig.OPTION_FLASH_TYPE);
    }
    
    public int getFlashType(final int i) {
        return (int)this.retrieveOption(ImageCaptureConfig.OPTION_FLASH_TYPE, i);
    }
    
    public ImageReaderProxyProvider getImageReaderProxyProvider() {
        return (ImageReaderProxyProvider)this.retrieveOption(ImageCaptureConfig.OPTION_IMAGE_READER_PROXY_PROVIDER, null);
    }
    
    @Override
    public int getInputFormat() {
        return (int)this.retrieveOption(ImageCaptureConfig.OPTION_INPUT_FORMAT);
    }
    
    @Override
    public Executor getIoExecutor() {
        return (Executor)this.retrieveOption(ImageCaptureConfig.OPTION_IO_EXECUTOR);
    }
    
    @Override
    public Executor getIoExecutor(final Executor executor) {
        return (Executor)this.retrieveOption(ImageCaptureConfig.OPTION_IO_EXECUTOR, executor);
    }
    
    public int getJpegQuality() {
        return (int)this.retrieveOption(ImageCaptureConfig.OPTION_JPEG_COMPRESSION_QUALITY);
    }
    
    public int getJpegQuality(final int i) {
        return (int)this.retrieveOption(ImageCaptureConfig.OPTION_JPEG_COMPRESSION_QUALITY, i);
    }
    
    public int getMaxCaptureStages() {
        return (int)this.retrieveOption(ImageCaptureConfig.OPTION_MAX_CAPTURE_STAGES);
    }
    
    public int getMaxCaptureStages(final int i) {
        return (int)this.retrieveOption(ImageCaptureConfig.OPTION_MAX_CAPTURE_STAGES, i);
    }
    
    public boolean hasCaptureMode() {
        return this.containsOption(ImageCaptureConfig.OPTION_IMAGE_CAPTURE_MODE);
    }
    
    public boolean isSoftwareJpegEncoderRequested() {
        return (boolean)this.retrieveOption(ImageCaptureConfig.OPTION_USE_SOFTWARE_JPEG_ENCODER, false);
    }
    
    @Override
    public /* synthetic */ Object retrieveOption(final Option option) {
        return ReadableConfig$_CC.$default$retrieveOption(this, option);
    }
    
    @Override
    public /* synthetic */ Object retrieveOption(final Option option, final Object o) {
        return ReadableConfig$_CC.$default$retrieveOption(this, option, o);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl;

import android.view.Surface;
import android.util.Size;

final class AutoValue_OutputSurface extends OutputSurface
{
    private final int imageFormat;
    private final Size size;
    private final Surface surface;
    
    AutoValue_OutputSurface(final Surface surface, final Size size, final int imageFormat) {
        if (surface == null) {
            throw new NullPointerException("Null surface");
        }
        this.surface = surface;
        if (size != null) {
            this.size = size;
            this.imageFormat = imageFormat;
            return;
        }
        throw new NullPointerException("Null size");
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o == this) {
            return true;
        }
        if (o instanceof OutputSurface) {
            final OutputSurface outputSurface = (OutputSurface)o;
            if (!this.surface.equals(outputSurface.getSurface()) || !this.size.equals((Object)outputSurface.getSize()) || this.imageFormat != outputSurface.getImageFormat()) {
                b = false;
            }
            return b;
        }
        return false;
    }
    
    @Override
    public int getImageFormat() {
        return this.imageFormat;
    }
    
    @Override
    public Size getSize() {
        return this.size;
    }
    
    @Override
    public Surface getSurface() {
        return this.surface;
    }
    
    @Override
    public int hashCode() {
        return ((this.surface.hashCode() ^ 0xF4243) * 1000003 ^ this.size.hashCode()) * 1000003 ^ this.imageFormat;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("OutputSurface{surface=");
        sb.append(this.surface);
        sb.append(", size=");
        sb.append(this.size);
        sb.append(", imageFormat=");
        sb.append(this.imageFormat);
        sb.append("}");
        return sb.toString();
    }
}

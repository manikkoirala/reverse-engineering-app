// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl;

import android.content.Context;
import androidx.camera.core.CameraInfo;

public interface CameraConfigProvider
{
    public static final CameraConfigProvider EMPTY = new CameraConfigProvider$$ExternalSyntheticLambda0();
    
    CameraConfig getConfig(final CameraInfo p0, final Context p1);
}

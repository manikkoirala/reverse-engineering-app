// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core;

import android.util.Size;
import android.graphics.Rect;

public final class SettableImageProxy extends ForwardingImageProxy
{
    private Rect mCropRect;
    private final int mHeight;
    private final ImageInfo mImageInfo;
    private final Object mLock;
    private final int mWidth;
    
    public SettableImageProxy(final ImageProxy imageProxy, final Size size, final ImageInfo mImageInfo) {
        super(imageProxy);
        this.mLock = new Object();
        if (size == null) {
            this.mWidth = super.getWidth();
            this.mHeight = super.getHeight();
        }
        else {
            this.mWidth = size.getWidth();
            this.mHeight = size.getHeight();
        }
        this.mImageInfo = mImageInfo;
    }
    
    SettableImageProxy(final ImageProxy imageProxy, final ImageInfo imageInfo) {
        this(imageProxy, null, imageInfo);
    }
    
    @Override
    public Rect getCropRect() {
        synchronized (this.mLock) {
            if (this.mCropRect == null) {
                return new Rect(0, 0, this.getWidth(), this.getHeight());
            }
            return new Rect(this.mCropRect);
        }
    }
    
    @Override
    public int getHeight() {
        return this.mHeight;
    }
    
    @Override
    public ImageInfo getImageInfo() {
        return this.mImageInfo;
    }
    
    @Override
    public int getWidth() {
        return this.mWidth;
    }
    
    @Override
    public void setCropRect(final Rect rect) {
        Rect mCropRect = rect;
        if (rect != null) {
            mCropRect = new Rect(rect);
            if (!mCropRect.intersect(0, 0, this.getWidth(), this.getHeight())) {
                mCropRect.setEmpty();
            }
        }
        synchronized (this.mLock) {
            this.mCropRect = mCropRect;
        }
    }
}

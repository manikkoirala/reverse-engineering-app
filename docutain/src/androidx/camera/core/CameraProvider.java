// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core;

import java.util.List;

public interface CameraProvider
{
    List<CameraInfo> getAvailableCameraInfos();
    
    boolean hasCamera(final CameraSelector p0) throws CameraInfoUnavailableException;
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core;

import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;

public class AspectRatio
{
    public static final int RATIO_16_9 = 1;
    public static final int RATIO_4_3 = 0;
    
    private AspectRatio() {
    }
    
    @Retention(RetentionPolicy.SOURCE)
    public @interface Ratio {
    }
}

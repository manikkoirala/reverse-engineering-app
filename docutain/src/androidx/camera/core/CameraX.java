// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core;

import android.content.pm.ServiceInfo;
import android.os.SystemClock;
import androidx.camera.core.impl.CameraValidator;
import androidx.camera.core.impl.CameraThreadConfig;
import androidx.concurrent.futures.CallbackToFutureAdapter;
import androidx.core.util.Preconditions;
import android.content.pm.PackageManager;
import android.app.Application;
import android.content.pm.PackageManager$NameNotFoundException;
import java.lang.reflect.InvocationTargetException;
import android.content.ComponentName;
import androidx.camera.core.impl.MetadataHolderService;
import androidx.camera.core.impl.utils.ContextUtil;
import androidx.camera.core.impl.Config;
import androidx.core.os.HandlerCompat;
import androidx.camera.core.impl.utils.futures.Futures;
import androidx.camera.core.impl.CameraDeviceSurfaceManager;
import android.os.HandlerThread;
import android.os.Handler;
import com.google.common.util.concurrent.ListenableFuture;
import androidx.camera.core.impl.UseCaseConfigFactory;
import androidx.camera.core.impl.CameraRepository;
import androidx.camera.core.impl.CameraFactory;
import java.util.concurrent.Executor;
import android.content.Context;
import android.util.SparseArray;

public final class CameraX
{
    private static final Object MIN_LOG_LEVEL_LOCK;
    private static final long RETRY_SLEEP_MILLIS = 500L;
    private static final String RETRY_TOKEN = "retry_token";
    private static final String TAG = "CameraX";
    private static final long WAIT_INITIALIZED_TIMEOUT_MILLIS = 3000L;
    private static final SparseArray<Integer> sMinLogLevelReferenceCountMap;
    private Context mAppContext;
    private final Executor mCameraExecutor;
    private CameraFactory mCameraFactory;
    final CameraRepository mCameraRepository;
    private final CameraXConfig mCameraXConfig;
    private UseCaseConfigFactory mDefaultConfigFactory;
    private final ListenableFuture<Void> mInitInternalFuture;
    private InternalInitState mInitState;
    private final Object mInitializeLock;
    private final Integer mMinLogLevel;
    private final Handler mSchedulerHandler;
    private final HandlerThread mSchedulerThread;
    private ListenableFuture<Void> mShutdownInternalFuture;
    private CameraDeviceSurfaceManager mSurfaceManager;
    
    static {
        MIN_LOG_LEVEL_LOCK = new Object();
        sMinLogLevelReferenceCountMap = new SparseArray();
    }
    
    public CameraX(final Context context, CameraXConfig.Provider configProvider) {
        this.mCameraRepository = new CameraRepository();
        this.mInitializeLock = new Object();
        this.mInitState = InternalInitState.UNINITIALIZED;
        this.mShutdownInternalFuture = Futures.immediateFuture((Void)null);
        if (configProvider != null) {
            this.mCameraXConfig = configProvider.getCameraXConfig();
        }
        else {
            configProvider = getConfigProvider(context);
            if (configProvider == null) {
                throw new IllegalStateException("CameraX is not configured properly. The most likely cause is you did not include a default implementation in your build such as 'camera-camera2'.");
            }
            this.mCameraXConfig = configProvider.getCameraXConfig();
        }
        final Executor cameraExecutor = this.mCameraXConfig.getCameraExecutor(null);
        final Handler schedulerHandler = this.mCameraXConfig.getSchedulerHandler(null);
        Executor mCameraExecutor = cameraExecutor;
        if (cameraExecutor == null) {
            mCameraExecutor = new CameraExecutor();
        }
        this.mCameraExecutor = mCameraExecutor;
        if (schedulerHandler == null) {
            final HandlerThread mSchedulerThread = new HandlerThread("CameraX-scheduler", 10);
            (this.mSchedulerThread = mSchedulerThread).start();
            this.mSchedulerHandler = HandlerCompat.createAsync(mSchedulerThread.getLooper());
        }
        else {
            this.mSchedulerThread = null;
            this.mSchedulerHandler = schedulerHandler;
        }
        increaseMinLogLevelReference(this.mMinLogLevel = (Integer)this.mCameraXConfig.retrieveOption(CameraXConfig.OPTION_MIN_LOGGING_LEVEL, null));
        this.mInitInternalFuture = this.initInternal(context);
    }
    
    private static void decreaseMinLogLevelReference(final Integer n) {
        final Object min_LOG_LEVEL_LOCK = CameraX.MIN_LOG_LEVEL_LOCK;
        monitorenter(min_LOG_LEVEL_LOCK);
        Label_0013: {
            if (n != null) {
                break Label_0013;
            }
            try {
                monitorexit(min_LOG_LEVEL_LOCK);
                return;
                while (true) {
                    while (true) {
                        updateOrResetMinLogLevel();
                        return;
                        final SparseArray<Integer> sMinLogLevelReferenceCountMap;
                        sMinLogLevelReferenceCountMap.remove((int)n);
                        continue;
                        Label_0049: {
                            final int i;
                            sMinLogLevelReferenceCountMap.put((int)n, (Object)i);
                        }
                        continue;
                    }
                    final SparseArray<Integer> sMinLogLevelReferenceCountMap = CameraX.sMinLogLevelReferenceCountMap;
                    final int i = (int)sMinLogLevelReferenceCountMap.get((int)n) - 1;
                    iftrue(Label_0049:)(i != 0);
                    continue;
                }
            }
            finally {
                monitorexit(min_LOG_LEVEL_LOCK);
            }
        }
    }
    
    private static CameraXConfig.Provider getConfigProvider(Context className) {
        final Application applicationFromContext = ContextUtil.getApplicationFromContext((Context)className);
        final boolean b = applicationFromContext instanceof CameraXConfig.Provider;
        final NullPointerException ex = null;
        if (b) {
            className = (NullPointerException)applicationFromContext;
        }
        else {
            try {
                final Context applicationContext = ContextUtil.getApplicationContext((Context)className);
                final PackageManager packageManager = applicationContext.getPackageManager();
                className = (NullPointerException)new ComponentName(applicationContext, (Class)MetadataHolderService.class);
                className = (NullPointerException)packageManager.getServiceInfo((ComponentName)className, 640);
                if (((ServiceInfo)className).metaData != null) {
                    className = (NullPointerException)((ServiceInfo)className).metaData.getString("androidx.camera.core.impl.MetadataHolderService.DEFAULT_CONFIG_PROVIDER");
                }
                else {
                    className = null;
                }
                if (className == null) {
                    Logger.e("CameraX", "No default CameraXConfig.Provider specified in meta-data. The most likely cause is you did not include a default implementation in your build such as 'camera-camera2'.");
                    return null;
                }
                className = (NullPointerException)Class.forName((String)className).getDeclaredConstructor((Class<?>[])new Class[0]).newInstance(new Object[0]);
                return (CameraXConfig.Provider)className;
            }
            catch (final NullPointerException className) {}
            catch (final IllegalAccessException className) {}
            catch (final NoSuchMethodException className) {}
            catch (final InvocationTargetException className) {}
            catch (final InstantiationException className) {}
            catch (final ClassNotFoundException className) {}
            catch (final PackageManager$NameNotFoundException ex2) {}
            Logger.e("CameraX", "Failed to retrieve default CameraXConfig.Provider from meta-data", className);
            className = ex;
        }
        return (CameraXConfig.Provider)className;
    }
    
    private static void increaseMinLogLevelReference(final Integer n) {
        final Object min_LOG_LEVEL_LOCK = CameraX.MIN_LOG_LEVEL_LOCK;
        monitorenter(min_LOG_LEVEL_LOCK);
        Label_0013: {
            if (n != null) {
                break Label_0013;
            }
            try {
                monitorexit(min_LOG_LEVEL_LOCK);
                return;
                SparseArray<Integer> sMinLogLevelReferenceCountMap = null;
                int i = 0;
            Label_0066:
                while (true) {
                    i = 1 + (int)sMinLogLevelReferenceCountMap.get((int)n);
                    break Label_0066;
                    Preconditions.checkArgumentInRange(n, 3, 6, "minLogLevel");
                    sMinLogLevelReferenceCountMap = CameraX.sMinLogLevelReferenceCountMap;
                    final Object value = sMinLogLevelReferenceCountMap.get((int)n);
                    i = 1;
                    iftrue(Label_0066:)(value == null);
                    continue;
                }
                sMinLogLevelReferenceCountMap.put((int)n, (Object)i);
                updateOrResetMinLogLevel();
            }
            finally {
                monitorexit(min_LOG_LEVEL_LOCK);
            }
        }
    }
    
    private void initAndRetryRecursively(final Executor executor, final long n, final Context context, final CallbackToFutureAdapter.Completer<Void> completer) {
        executor.execute(new CameraX$$ExternalSyntheticLambda3(this, context, executor, completer, n));
    }
    
    private ListenableFuture<Void> initInternal(final Context context) {
        synchronized (this.mInitializeLock) {
            Preconditions.checkState(this.mInitState == InternalInitState.UNINITIALIZED, "CameraX.initInternal() should only be called once per instance");
            this.mInitState = InternalInitState.INITIALIZING;
            return CallbackToFutureAdapter.getFuture((CallbackToFutureAdapter.Resolver<Void>)new CameraX$$ExternalSyntheticLambda4(this, context));
        }
    }
    
    private void setStateToInitialized() {
        synchronized (this.mInitializeLock) {
            this.mInitState = InternalInitState.INITIALIZED;
        }
    }
    
    private ListenableFuture<Void> shutdownInternal() {
        synchronized (this.mInitializeLock) {
            this.mSchedulerHandler.removeCallbacksAndMessages((Object)"retry_token");
            final int n = CameraX$1.$SwitchMap$androidx$camera$core$CameraX$InternalInitState[this.mInitState.ordinal()];
            if (n == 1) {
                this.mInitState = InternalInitState.SHUTDOWN;
                return Futures.immediateFuture((Void)null);
            }
            if (n != 2) {
                if (n == 3 || n == 4) {
                    this.mInitState = InternalInitState.SHUTDOWN;
                    decreaseMinLogLevelReference(this.mMinLogLevel);
                    this.mShutdownInternalFuture = CallbackToFutureAdapter.getFuture((CallbackToFutureAdapter.Resolver<Void>)new CameraX$$ExternalSyntheticLambda0(this));
                }
                return this.mShutdownInternalFuture;
            }
            throw new IllegalStateException("CameraX could not be shutdown when it is initializing.");
        }
    }
    
    private static void updateOrResetMinLogLevel() {
        final SparseArray<Integer> sMinLogLevelReferenceCountMap = CameraX.sMinLogLevelReferenceCountMap;
        if (sMinLogLevelReferenceCountMap.size() == 0) {
            Logger.resetMinLogLevel();
            return;
        }
        if (sMinLogLevelReferenceCountMap.get(3) != null) {
            Logger.setMinLogLevel(3);
        }
        else if (sMinLogLevelReferenceCountMap.get(4) != null) {
            Logger.setMinLogLevel(4);
        }
        else if (sMinLogLevelReferenceCountMap.get(5) != null) {
            Logger.setMinLogLevel(5);
        }
        else if (sMinLogLevelReferenceCountMap.get(6) != null) {
            Logger.setMinLogLevel(6);
        }
    }
    
    public CameraDeviceSurfaceManager getCameraDeviceSurfaceManager() {
        final CameraDeviceSurfaceManager mSurfaceManager = this.mSurfaceManager;
        if (mSurfaceManager != null) {
            return mSurfaceManager;
        }
        throw new IllegalStateException("CameraX not initialized yet.");
    }
    
    public CameraFactory getCameraFactory() {
        final CameraFactory mCameraFactory = this.mCameraFactory;
        if (mCameraFactory != null) {
            return mCameraFactory;
        }
        throw new IllegalStateException("CameraX not initialized yet.");
    }
    
    public CameraRepository getCameraRepository() {
        return this.mCameraRepository;
    }
    
    public UseCaseConfigFactory getDefaultConfigFactory() {
        final UseCaseConfigFactory mDefaultConfigFactory = this.mDefaultConfigFactory;
        if (mDefaultConfigFactory != null) {
            return mDefaultConfigFactory;
        }
        throw new IllegalStateException("CameraX not initialized yet.");
    }
    
    public ListenableFuture<Void> getInitializeFuture() {
        return this.mInitInternalFuture;
    }
    
    boolean isInitialized() {
        synchronized (this.mInitializeLock) {
            return this.mInitState == InternalInitState.INITIALIZED;
        }
    }
    
    public ListenableFuture<Void> shutdown() {
        return this.shutdownInternal();
    }
    
    private enum InternalInitState
    {
        private static final InternalInitState[] $VALUES;
        
        INITIALIZED, 
        INITIALIZING, 
        INITIALIZING_ERROR, 
        SHUTDOWN, 
        UNINITIALIZED;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core;

import java.util.Collections;
import java.util.Collection;
import java.util.ArrayList;
import java.util.Arrays;
import androidx.camera.core.impl.CaptureBundle;
import androidx.camera.core.impl.CaptureStage;
import java.util.List;

public final class CaptureBundles
{
    private CaptureBundles() {
    }
    
    static CaptureBundle createCaptureBundle(final List<CaptureStage> list) {
        return new CaptureBundleImpl(list);
    }
    
    static CaptureBundle createCaptureBundle(final CaptureStage... a) {
        return new CaptureBundleImpl(Arrays.asList(a));
    }
    
    public static CaptureBundle singleDefaultCaptureBundle() {
        return createCaptureBundle(new CaptureStage.DefaultCaptureStage());
    }
    
    static final class CaptureBundleImpl implements CaptureBundle
    {
        final List<CaptureStage> mCaptureStageList;
        
        CaptureBundleImpl(final List<CaptureStage> c) {
            if (c != null && !c.isEmpty()) {
                this.mCaptureStageList = Collections.unmodifiableList((List<? extends CaptureStage>)new ArrayList<CaptureStage>(c));
                return;
            }
            throw new IllegalArgumentException("Cannot set an empty CaptureStage list.");
        }
        
        @Override
        public List<CaptureStage> getCaptureStages() {
            return this.mCaptureStageList;
        }
    }
}

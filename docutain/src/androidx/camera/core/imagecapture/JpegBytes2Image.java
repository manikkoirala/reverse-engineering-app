// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.imagecapture;

import androidx.camera.core.ImageCaptureException;
import androidx.camera.core.impl.utils.Exif;
import java.util.Objects;
import androidx.camera.core.impl.ImageReaderProxy;
import androidx.camera.core.ImageProcessingUtil;
import androidx.camera.core.SafeCloseImageReaderProxy;
import androidx.camera.core.ImageReaderProxys;
import androidx.camera.core.ImageProxy;
import androidx.camera.core.processing.Packet;
import androidx.camera.core.processing.Operation;

public class JpegBytes2Image implements Operation<Packet<byte[]>, Packet<ImageProxy>>
{
    private static final int MAX_IMAGES = 2;
    
    @Override
    public Packet<ImageProxy> apply(final Packet<byte[]> packet) throws ImageCaptureException {
        final SafeCloseImageReaderProxy safeCloseImageReaderProxy = new SafeCloseImageReaderProxy(ImageReaderProxys.createIsolatedReader(packet.getSize().getWidth(), packet.getSize().getHeight(), 256, 2));
        final ImageProxy convertJpegBytesToImage = ImageProcessingUtil.convertJpegBytesToImage(safeCloseImageReaderProxy, packet.getData());
        safeCloseImageReaderProxy.safeClose();
        return Packet.of(Objects.requireNonNull(convertJpegBytesToImage), Objects.requireNonNull(packet.getExif()), packet.getCropRect(), packet.getRotationDegrees(), packet.getSensorToBufferTransform(), packet.getCameraCaptureResult());
    }
}

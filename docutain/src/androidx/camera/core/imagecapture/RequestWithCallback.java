// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.imagecapture;

import androidx.camera.core.ImageProxy;
import androidx.camera.core.ImageCapture;
import androidx.camera.core.impl.utils.Threads;
import androidx.camera.core.ImageCaptureException;
import androidx.core.util.Preconditions;
import com.google.common.util.concurrent.ListenableFuture;
import androidx.concurrent.futures.CallbackToFutureAdapter;

class RequestWithCallback implements TakePictureCallback
{
    private CallbackToFutureAdapter.Completer<Void> mCaptureCompleter;
    private final ListenableFuture<Void> mCaptureFuture;
    private boolean mIsAborted;
    private boolean mIsComplete;
    private final TakePictureRequest mTakePictureRequest;
    
    RequestWithCallback(final TakePictureRequest mTakePictureRequest) {
        this.mIsComplete = false;
        this.mIsAborted = false;
        this.mTakePictureRequest = mTakePictureRequest;
        this.mCaptureFuture = CallbackToFutureAdapter.getFuture((CallbackToFutureAdapter.Resolver<Void>)new RequestWithCallback$$ExternalSyntheticLambda0(this));
    }
    
    private void checkOnImageCaptured() {
        Preconditions.checkState(this.mCaptureFuture.isDone(), "onImageCaptured() must be called before onFinalResult()");
    }
    
    private void markComplete() {
        Preconditions.checkState(this.mIsComplete ^ true, "The callback can only complete once.");
        this.mIsComplete = true;
    }
    
    private void onFailure(final ImageCaptureException ex) {
        Threads.checkMainThread();
        this.mTakePictureRequest.onError(ex);
    }
    
    void abort(final ImageCaptureException ex) {
        Threads.checkMainThread();
        this.mIsAborted = true;
        this.mCaptureCompleter.set(null);
        this.onFailure(ex);
    }
    
    ListenableFuture<Void> getCaptureFuture() {
        Threads.checkMainThread();
        return this.mCaptureFuture;
    }
    
    @Override
    public boolean isAborted() {
        return this.mIsAborted;
    }
    
    @Override
    public void onCaptureFailure(final ImageCaptureException ex) {
        Threads.checkMainThread();
        if (this.mIsAborted) {
            return;
        }
        this.markComplete();
        this.mCaptureCompleter.set(null);
        this.onFailure(ex);
    }
    
    @Override
    public void onFinalResult(final ImageCapture.OutputFileResults outputFileResults) {
        Threads.checkMainThread();
        if (this.mIsAborted) {
            return;
        }
        this.checkOnImageCaptured();
        this.markComplete();
        this.mTakePictureRequest.onResult(outputFileResults);
    }
    
    @Override
    public void onFinalResult(final ImageProxy imageProxy) {
        Threads.checkMainThread();
        if (this.mIsAborted) {
            return;
        }
        this.checkOnImageCaptured();
        this.markComplete();
        this.mTakePictureRequest.onResult(imageProxy);
    }
    
    @Override
    public void onImageCaptured() {
        Threads.checkMainThread();
        if (this.mIsAborted) {
            return;
        }
        this.mCaptureCompleter.set(null);
    }
    
    @Override
    public void onProcessFailure(final ImageCaptureException ex) {
        Threads.checkMainThread();
        if (this.mIsAborted) {
            return;
        }
        this.checkOnImageCaptured();
        this.markComplete();
        this.onFailure(ex);
    }
}

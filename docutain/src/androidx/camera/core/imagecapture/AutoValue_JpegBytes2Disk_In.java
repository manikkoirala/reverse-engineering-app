// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.imagecapture;

import androidx.camera.core.processing.Packet;
import androidx.camera.core.ImageCapture;

final class AutoValue_JpegBytes2Disk_In extends In
{
    private final ImageCapture.OutputFileOptions outputFileOptions;
    private final Packet<byte[]> packet;
    
    AutoValue_JpegBytes2Disk_In(final Packet<byte[]> packet, final ImageCapture.OutputFileOptions outputFileOptions) {
        if (packet == null) {
            throw new NullPointerException("Null packet");
        }
        this.packet = packet;
        if (outputFileOptions != null) {
            this.outputFileOptions = outputFileOptions;
            return;
        }
        throw new NullPointerException("Null outputFileOptions");
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o == this) {
            return true;
        }
        if (o instanceof In) {
            final In in = (In)o;
            if (!this.packet.equals(in.getPacket()) || !this.outputFileOptions.equals(in.getOutputFileOptions())) {
                b = false;
            }
            return b;
        }
        return false;
    }
    
    @Override
    ImageCapture.OutputFileOptions getOutputFileOptions() {
        return this.outputFileOptions;
    }
    
    @Override
    Packet<byte[]> getPacket() {
        return this.packet;
    }
    
    @Override
    public int hashCode() {
        return (this.packet.hashCode() ^ 0xF4243) * 1000003 ^ this.outputFileOptions.hashCode();
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("In{packet=");
        sb.append(this.packet);
        sb.append(", outputFileOptions=");
        sb.append(this.outputFileOptions);
        sb.append("}");
        return sb.toString();
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.imagecapture;

import androidx.camera.core.ImageProxy;
import java.util.Objects;
import androidx.camera.core.ImageCaptureException;
import androidx.core.util.Preconditions;
import androidx.camera.core.impl.CameraCaptureCallback;
import java.util.List;
import android.graphics.Matrix;
import android.graphics.Rect;
import androidx.camera.core.ImageCapture;
import java.util.concurrent.Executor;

public abstract class TakePictureRequest
{
    public static TakePictureRequest of(final Executor executor, final ImageCapture.OnImageCapturedCallback onImageCapturedCallback, final ImageCapture.OnImageSavedCallback onImageSavedCallback, final ImageCapture.OutputFileOptions outputFileOptions, final Rect rect, final Matrix matrix, final int n, final int n2, final int n3, final List<CameraCaptureCallback> list) {
        final int n4 = 1;
        Preconditions.checkArgument(onImageSavedCallback == null == (outputFileOptions == null), (Object)"onDiskCallback and outputFileOptions should be both null or both non-null.");
        final boolean b = onImageSavedCallback == null;
        int n5;
        if (onImageCapturedCallback == null) {
            n5 = n4;
        }
        else {
            n5 = 0;
        }
        Preconditions.checkArgument((boolean)((n5 ^ (b ? 1 : 0)) != 0x0), (Object)"One and only one on-disk or in-memory callback should be present.");
        return new AutoValue_TakePictureRequest(executor, onImageCapturedCallback, onImageSavedCallback, outputFileOptions, rect, matrix, n, n2, n3, list);
    }
    
    abstract Executor getAppExecutor();
    
    abstract int getCaptureMode();
    
    abstract Rect getCropRect();
    
    abstract ImageCapture.OnImageCapturedCallback getInMemoryCallback();
    
    abstract int getJpegQuality();
    
    abstract ImageCapture.OnImageSavedCallback getOnDiskCallback();
    
    abstract ImageCapture.OutputFileOptions getOutputFileOptions();
    
    abstract int getRotationDegrees();
    
    abstract Matrix getSensorToBufferTransform();
    
    abstract List<CameraCaptureCallback> getSessionConfigCameraCaptureCallbacks();
    
    void onError(final ImageCaptureException ex) {
        this.getAppExecutor().execute(new TakePictureRequest$$ExternalSyntheticLambda1(this, ex));
    }
    
    void onResult(final ImageCapture.OutputFileResults outputFileResults) {
        this.getAppExecutor().execute(new TakePictureRequest$$ExternalSyntheticLambda0(this, outputFileResults));
    }
    
    void onResult(final ImageProxy imageProxy) {
        this.getAppExecutor().execute(new TakePictureRequest$$ExternalSyntheticLambda2(this, imageProxy));
    }
}

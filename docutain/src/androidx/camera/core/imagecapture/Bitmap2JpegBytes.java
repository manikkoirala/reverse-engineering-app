// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.imagecapture;

import androidx.camera.core.ImageCaptureException;
import java.util.Objects;
import androidx.camera.core.impl.utils.Exif;
import java.io.OutputStream;
import android.graphics.Bitmap$CompressFormat;
import android.graphics.Bitmap;
import java.io.ByteArrayOutputStream;
import androidx.camera.core.processing.Packet;
import androidx.camera.core.processing.Operation;

class Bitmap2JpegBytes implements Operation<In, Packet<byte[]>>
{
    @Override
    public Packet<byte[]> apply(final In in) throws ImageCaptureException {
        final Packet<Bitmap> packet = in.getPacket();
        final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        packet.getData().compress(Bitmap$CompressFormat.JPEG, in.getJpegQuality(), (OutputStream)byteArrayOutputStream);
        ((Bitmap)packet.getData()).recycle();
        return Packet.of(byteArrayOutputStream.toByteArray(), Objects.requireNonNull(packet.getExif()), 256, packet.getSize(), packet.getCropRect(), packet.getRotationDegrees(), packet.getSensorToBufferTransform(), packet.getCameraCaptureResult());
    }
    
    abstract static class In
    {
        static In of(final Packet<Bitmap> packet, final int n) {
            return (In)new AutoValue_Bitmap2JpegBytes_In(packet, n);
        }
        
        abstract int getJpegQuality();
        
        abstract Packet<Bitmap> getPacket();
    }
}

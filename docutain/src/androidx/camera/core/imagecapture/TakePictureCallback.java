// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.imagecapture;

import androidx.camera.core.ImageProxy;
import androidx.camera.core.ImageCapture;
import androidx.camera.core.ImageCaptureException;

interface TakePictureCallback
{
    boolean isAborted();
    
    void onCaptureFailure(final ImageCaptureException p0);
    
    void onFinalResult(final ImageCapture.OutputFileResults p0);
    
    void onFinalResult(final ImageProxy p0);
    
    void onImageCaptured();
    
    void onProcessFailure(final ImageCaptureException p0);
}

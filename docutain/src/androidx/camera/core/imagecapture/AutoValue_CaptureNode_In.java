// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.imagecapture;

import android.util.Size;
import androidx.camera.core.processing.Edge;

final class AutoValue_CaptureNode_In extends In
{
    private final int format;
    private final Edge<ProcessingRequest> requestEdge;
    private final Size size;
    
    AutoValue_CaptureNode_In(final Size size, final int format, final Edge<ProcessingRequest> requestEdge) {
        if (size == null) {
            throw new NullPointerException("Null size");
        }
        this.size = size;
        this.format = format;
        if (requestEdge != null) {
            this.requestEdge = requestEdge;
            return;
        }
        throw new NullPointerException("Null requestEdge");
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o == this) {
            return true;
        }
        if (o instanceof In) {
            final In in = (In)o;
            if (!this.size.equals((Object)in.getSize()) || this.format != in.getFormat() || !this.requestEdge.equals(in.getRequestEdge())) {
                b = false;
            }
            return b;
        }
        return false;
    }
    
    @Override
    int getFormat() {
        return this.format;
    }
    
    @Override
    Edge<ProcessingRequest> getRequestEdge() {
        return this.requestEdge;
    }
    
    @Override
    Size getSize() {
        return this.size;
    }
    
    @Override
    public int hashCode() {
        return ((this.size.hashCode() ^ 0xF4243) * 1000003 ^ this.format) * 1000003 ^ this.requestEdge.hashCode();
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("In{size=");
        sb.append(this.size);
        sb.append(", format=");
        sb.append(this.format);
        sb.append(", requestEdge=");
        sb.append(this.requestEdge);
        sb.append("}");
        return sb.toString();
    }
}

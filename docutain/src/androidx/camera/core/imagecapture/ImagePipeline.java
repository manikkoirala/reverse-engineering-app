// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.imagecapture;

import androidx.camera.core.ForwardingImageProxy;
import androidx.camera.core.impl.utils.TransformUtils;
import androidx.camera.core.impl.SessionConfig;
import androidx.core.util.Pair;
import androidx.camera.core.CaptureBundles;
import java.util.Iterator;
import androidx.camera.core.impl.CameraCaptureCallback;
import java.util.Collection;
import androidx.camera.core.impl.CaptureStage;
import java.util.List;
import java.util.ArrayList;
import androidx.camera.core.impl.CaptureBundle;
import java.util.Objects;
import androidx.camera.core.impl.utils.executor.CameraXExecutors;
import java.util.concurrent.Executor;
import androidx.camera.core.impl.UseCaseConfig;
import androidx.camera.core.impl.utils.Threads;
import android.util.Size;
import androidx.camera.core.impl.ImageCaptureConfig;
import androidx.camera.core.impl.CaptureConfig;
import androidx.camera.core.internal.compat.workaround.ExifRotationAvailability;

public class ImagePipeline
{
    static final ExifRotationAvailability EXIF_ROTATION_AVAILABILITY;
    static final byte JPEG_QUALITY_MAX_QUALITY = 100;
    static final byte JPEG_QUALITY_MIN_LATENCY = 95;
    private final SingleBundlingNode mBundlingNode;
    private final CaptureConfig mCaptureConfig;
    private final CaptureNode mCaptureNode;
    private final CaptureNode.In mPipelineIn;
    private final ProcessingNode mProcessingNode;
    private final ImageCaptureConfig mUseCaseConfig;
    
    static {
        EXIF_ROTATION_AVAILABILITY = new ExifRotationAvailability();
    }
    
    public ImagePipeline(final ImageCaptureConfig mUseCaseConfig, final Size size) {
        Threads.checkMainThread();
        this.mUseCaseConfig = mUseCaseConfig;
        this.mCaptureConfig = CaptureConfig.Builder.createFrom(mUseCaseConfig).build();
        final CaptureNode mCaptureNode = new CaptureNode();
        this.mCaptureNode = mCaptureNode;
        final SingleBundlingNode mBundlingNode = new SingleBundlingNode();
        this.mBundlingNode = mBundlingNode;
        final ProcessingNode mProcessingNode = new ProcessingNode(Objects.requireNonNull(mUseCaseConfig.getIoExecutor(CameraXExecutors.ioExecutor())));
        this.mProcessingNode = mProcessingNode;
        final CaptureNode.In of = CaptureNode.In.of(size, mUseCaseConfig.getInputFormat());
        this.mPipelineIn = of;
        mProcessingNode.transform(mBundlingNode.transform(mCaptureNode.transform(of)));
    }
    
    private CameraRequest createCameraRequest(final CaptureBundle captureBundle, final TakePictureRequest takePictureRequest, final TakePictureCallback takePictureCallback) {
        final ArrayList list = new ArrayList();
        final int hashCode = captureBundle.hashCode();
        for (final CaptureStage captureStage : Objects.requireNonNull(captureBundle.getCaptureStages())) {
            final CaptureConfig.Builder builder = new CaptureConfig.Builder();
            builder.setTemplateType(this.mCaptureConfig.getTemplateType());
            builder.addImplementationOptions(this.mCaptureConfig.getImplementationOptions());
            builder.addAllCameraCaptureCallbacks(takePictureRequest.getSessionConfigCameraCaptureCallbacks());
            builder.addSurface(this.mPipelineIn.getSurface());
            if (this.mPipelineIn.getFormat() == 256) {
                if (ImagePipeline.EXIF_ROTATION_AVAILABILITY.isRotationOptionSupported()) {
                    builder.addImplementationOption(CaptureConfig.OPTION_ROTATION, takePictureRequest.getRotationDegrees());
                }
                builder.addImplementationOption(CaptureConfig.OPTION_JPEG_QUALITY, this.getCameraRequestJpegQuality(takePictureRequest));
            }
            builder.addImplementationOptions(captureStage.getCaptureConfig().getImplementationOptions());
            builder.addTag(String.valueOf(hashCode), captureStage.getId());
            builder.addCameraCaptureCallback(this.mPipelineIn.getCameraCaptureCallback());
            list.add(builder.build());
        }
        return new CameraRequest(list, takePictureCallback);
    }
    
    private CaptureBundle createCaptureBundle() {
        return Objects.requireNonNull(this.mUseCaseConfig.getCaptureBundle(CaptureBundles.singleDefaultCaptureBundle()));
    }
    
    private ProcessingRequest createProcessingRequest(final CaptureBundle captureBundle, final TakePictureRequest takePictureRequest, final TakePictureCallback takePictureCallback) {
        return new ProcessingRequest(captureBundle, takePictureRequest.getOutputFileOptions(), takePictureRequest.getCropRect(), takePictureRequest.getRotationDegrees(), takePictureRequest.getJpegQuality(), takePictureRequest.getSensorToBufferTransform(), takePictureCallback);
    }
    
    public void close() {
        Threads.checkMainThread();
        this.mCaptureNode.release();
        this.mBundlingNode.release();
        this.mProcessingNode.release();
    }
    
    Pair<CameraRequest, ProcessingRequest> createRequests(final TakePictureRequest takePictureRequest, final TakePictureCallback takePictureCallback) {
        Threads.checkMainThread();
        final CaptureBundle captureBundle = this.createCaptureBundle();
        return new Pair<CameraRequest, ProcessingRequest>(this.createCameraRequest(captureBundle, takePictureRequest, takePictureCallback), this.createProcessingRequest(captureBundle, takePictureRequest, takePictureCallback));
    }
    
    public SessionConfig.Builder createSessionConfigBuilder() {
        final SessionConfig.Builder from = SessionConfig.Builder.createFrom(this.mUseCaseConfig);
        from.addNonRepeatingSurface(this.mPipelineIn.getSurface());
        return from;
    }
    
    int getCameraRequestJpegQuality(final TakePictureRequest takePictureRequest) {
        final boolean b = takePictureRequest.getOnDiskCallback() != null;
        final boolean hasCropping = TransformUtils.hasCropping(takePictureRequest.getCropRect(), this.mPipelineIn.getSize());
        if (!b || !hasCropping) {
            return takePictureRequest.getJpegQuality();
        }
        if (takePictureRequest.getCaptureMode() == 0) {
            return 100;
        }
        return 95;
    }
    
    public int getCapacity() {
        Threads.checkMainThread();
        return this.mCaptureNode.getCapacity();
    }
    
    CaptureNode getCaptureNode() {
        return this.mCaptureNode;
    }
    
    void postProcess(final ProcessingRequest processingRequest) {
        Threads.checkMainThread();
        this.mPipelineIn.getRequestEdge().accept(processingRequest);
    }
    
    public void setOnImageCloseListener(final ForwardingImageProxy.OnImageCloseListener onImageCloseListener) {
        Threads.checkMainThread();
        this.mCaptureNode.setOnImageCloseListener(onImageCloseListener);
    }
}

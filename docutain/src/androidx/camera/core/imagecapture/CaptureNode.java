// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.imagecapture;

import androidx.camera.core.impl.ImmediateSurface;
import androidx.camera.core.processing.Edge;
import androidx.camera.core.impl.DeferrableSurface;
import androidx.camera.core.impl.CameraCaptureCallback;
import android.util.Size;
import androidx.core.util.Consumer;
import java.util.concurrent.Executor;
import androidx.camera.core.impl.utils.executor.CameraXExecutors;
import android.view.Surface;
import androidx.camera.core.MetadataImageReader;
import androidx.camera.core.ForwardingImageProxy;
import java.util.Iterator;
import java.util.Collection;
import androidx.camera.core.impl.ImageReaderProxy;
import androidx.camera.core.impl.utils.Threads;
import androidx.core.util.Preconditions;
import java.util.Objects;
import java.util.HashSet;
import androidx.camera.core.SafeCloseImageReaderProxy;
import androidx.camera.core.ImageProxy;
import java.util.Set;
import androidx.camera.core.processing.Node;

class CaptureNode implements Node<In, Out>
{
    static final int MAX_IMAGES = 4;
    private ProcessingRequest mCurrentRequest;
    private In mInputEdge;
    private Out mOutputEdge;
    private final Set<ImageProxy> mPendingImages;
    private final Set<Integer> mPendingStageIds;
    SafeCloseImageReaderProxy mSafeCloseImageReaderProxy;
    
    CaptureNode() {
        this.mPendingStageIds = new HashSet<Integer>();
        this.mPendingImages = new HashSet<ImageProxy>();
        this.mCurrentRequest = null;
    }
    
    private void matchAndPropagateImage(final ImageProxy imageProxy) {
        final int intValue = Objects.requireNonNull(imageProxy.getImageInfo().getTagBundle().getTag(this.mCurrentRequest.getTagBundleKey()));
        final boolean contains = this.mPendingStageIds.contains(intValue);
        final StringBuilder sb = new StringBuilder();
        sb.append("Received an unexpected stage id");
        sb.append(intValue);
        Preconditions.checkState(contains, sb.toString());
        this.mPendingStageIds.remove(intValue);
        if (this.mPendingStageIds.isEmpty()) {
            this.mCurrentRequest.onImageCaptured();
            this.mCurrentRequest = null;
        }
        this.mOutputEdge.getImageEdge().accept(imageProxy);
    }
    
    public int getCapacity() {
        Threads.checkMainThread();
        Preconditions.checkState(this.mSafeCloseImageReaderProxy != null, "The ImageReader is not initialized.");
        return this.mSafeCloseImageReaderProxy.getCapacity();
    }
    
    In getInputEdge() {
        return this.mInputEdge;
    }
    
    void onImageProxyAvailable(final ImageProxy imageProxy) {
        Threads.checkMainThread();
        if (this.mCurrentRequest == null) {
            this.mPendingImages.add(imageProxy);
        }
        else {
            this.matchAndPropagateImage(imageProxy);
        }
    }
    
    void onRequestAvailable(final ProcessingRequest mCurrentRequest) {
        Threads.checkMainThread();
        final int capacity = this.getCapacity();
        final boolean b = true;
        Preconditions.checkState(capacity > 0, "Too many acquire images. Close image to be able to process next.");
        boolean b2 = b;
        if (this.mCurrentRequest != null) {
            b2 = (this.mPendingStageIds.isEmpty() && b);
        }
        Preconditions.checkState(b2, "The previous request is not complete");
        this.mCurrentRequest = mCurrentRequest;
        this.mPendingStageIds.addAll(mCurrentRequest.getStageIds());
        this.mOutputEdge.getRequestEdge().accept(mCurrentRequest);
        final Iterator<ImageProxy> iterator = this.mPendingImages.iterator();
        while (iterator.hasNext()) {
            this.matchAndPropagateImage(iterator.next());
        }
        this.mPendingImages.clear();
    }
    
    @Override
    public void release() {
        Threads.checkMainThread();
        final SafeCloseImageReaderProxy mSafeCloseImageReaderProxy = this.mSafeCloseImageReaderProxy;
        if (mSafeCloseImageReaderProxy != null) {
            mSafeCloseImageReaderProxy.safeClose();
        }
        final In mInputEdge = this.mInputEdge;
        if (mInputEdge != null) {
            mInputEdge.closeSurface();
        }
    }
    
    public void setOnImageCloseListener(final ForwardingImageProxy.OnImageCloseListener onImageCloseListener) {
        Threads.checkMainThread();
        Preconditions.checkState(this.mSafeCloseImageReaderProxy != null, "The ImageReader is not initialized.");
        this.mSafeCloseImageReaderProxy.setOnImageCloseListener(onImageCloseListener);
    }
    
    @Override
    public Out transform(final In mInputEdge) {
        this.mInputEdge = mInputEdge;
        final Size size = mInputEdge.getSize();
        final MetadataImageReader metadataImageReader = new MetadataImageReader(size.getWidth(), size.getHeight(), mInputEdge.getFormat(), 4);
        this.mSafeCloseImageReaderProxy = new SafeCloseImageReaderProxy(metadataImageReader);
        mInputEdge.setCameraCaptureCallback(metadataImageReader.getCameraCaptureCallback());
        mInputEdge.setSurface(Objects.requireNonNull(metadataImageReader.getSurface()));
        metadataImageReader.setOnImageAvailableListener(new CaptureNode$$ExternalSyntheticLambda0(this), CameraXExecutors.mainThreadExecutor());
        mInputEdge.getRequestEdge().setListener(new CaptureNode$$ExternalSyntheticLambda1(this));
        return this.mOutputEdge = Out.of(mInputEdge.getFormat());
    }
    
    abstract static class In
    {
        private CameraCaptureCallback mCameraCaptureCallback;
        private DeferrableSurface mSurface;
        
        static In of(final Size size, final int n) {
            return (In)new AutoValue_CaptureNode_In(size, n, new Edge<ProcessingRequest>());
        }
        
        void closeSurface() {
            this.mSurface.close();
        }
        
        CameraCaptureCallback getCameraCaptureCallback() {
            return this.mCameraCaptureCallback;
        }
        
        abstract int getFormat();
        
        abstract Edge<ProcessingRequest> getRequestEdge();
        
        abstract Size getSize();
        
        DeferrableSurface getSurface() {
            return this.mSurface;
        }
        
        void setCameraCaptureCallback(final CameraCaptureCallback mCameraCaptureCallback) {
            this.mCameraCaptureCallback = mCameraCaptureCallback;
        }
        
        void setSurface(final Surface surface) {
            Preconditions.checkState(this.mSurface == null, "The surface is already set.");
            this.mSurface = new ImmediateSurface(surface);
        }
    }
    
    abstract static class Out
    {
        static Out of(final int n) {
            return (Out)new AutoValue_CaptureNode_Out(new Edge<ImageProxy>(), new Edge<ProcessingRequest>(), n);
        }
        
        abstract int getFormat();
        
        abstract Edge<ImageProxy> getImageEdge();
        
        abstract Edge<ProcessingRequest> getRequestEdge();
    }
}

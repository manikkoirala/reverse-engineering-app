// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.imagecapture;

import androidx.camera.core.impl.CameraCaptureResult;
import androidx.core.util.Preconditions;
import androidx.camera.core.internal.CameraCaptureResultImageInfo;
import java.io.IOException;
import androidx.camera.core.ImageCaptureException;
import androidx.camera.core.impl.utils.Exif;
import android.graphics.Rect;
import android.graphics.RectF;
import androidx.camera.core.impl.utils.TransformUtils;
import android.graphics.Matrix;
import android.util.Size;
import androidx.camera.core.ImageProxy;
import androidx.camera.core.processing.Packet;
import androidx.camera.core.processing.Operation;

final class ProcessingInput2Packet implements Operation<ProcessingNode.InputPacket, Packet<ImageProxy>>
{
    private static Matrix getHalTransform(int n, final Size size, final int n2) {
        n -= n2;
        Size size2;
        if (TransformUtils.is90or270(TransformUtils.within360(n))) {
            size2 = new Size(size.getHeight(), size.getWidth());
        }
        else {
            size2 = size;
        }
        return TransformUtils.getRectToRect(new RectF(0.0f, 0.0f, (float)size2.getWidth(), (float)size2.getHeight()), new RectF(0.0f, 0.0f, (float)size.getWidth(), (float)size.getHeight()), n);
    }
    
    private static Rect getUpdatedCropRect(final Rect rect, final Matrix matrix) {
        final RectF rectF = new RectF(rect);
        matrix.mapRect(rectF);
        final Rect rect2 = new Rect();
        rectF.round(rect2);
        return rect2;
    }
    
    private static Matrix getUpdatedTransform(Matrix matrix, final Matrix matrix2) {
        matrix = new Matrix(matrix);
        matrix.postConcat(matrix2);
        return matrix;
    }
    
    private static boolean isSizeMatch(final Exif exif, final ImageProxy imageProxy) {
        return exif.getWidth() == imageProxy.getWidth() && exif.getHeight() == imageProxy.getHeight();
    }
    
    @Override
    public Packet<ImageProxy> apply(final ProcessingNode.InputPacket inputPacket) throws ImageCaptureException {
        final ImageProxy imageProxy = inputPacket.getImageProxy();
        final ProcessingRequest processingRequest = inputPacket.getProcessingRequest();
        Exif fromImageProxy = null;
        Label_0067: {
            if (imageProxy.getFormat() == 256) {
                try {
                    fromImageProxy = Exif.createFromImageProxy(imageProxy);
                    imageProxy.getPlanes()[0].getBuffer().rewind();
                    break Label_0067;
                }
                catch (final IOException ex) {
                    throw new ImageCaptureException(1, "Failed to extract EXIF data.", ex);
                }
            }
            fromImageProxy = null;
        }
        final CameraCaptureResult cameraCaptureResult = ((CameraCaptureResultImageInfo)imageProxy.getImageInfo()).getCameraCaptureResult();
        Rect rect = processingRequest.getCropRect();
        Matrix matrix = processingRequest.getSensorToBufferTransform();
        int n = processingRequest.getRotationDegrees();
        if (ImagePipeline.EXIF_ROTATION_AVAILABILITY.shouldUseExifOrientation(imageProxy)) {
            Preconditions.checkNotNull(fromImageProxy, "The image must have JPEG exif.");
            Preconditions.checkState(isSizeMatch(fromImageProxy, imageProxy), "Exif size does not match image size.");
            final Matrix halTransform = getHalTransform(processingRequest.getRotationDegrees(), new Size(fromImageProxy.getWidth(), fromImageProxy.getHeight()), fromImageProxy.getRotation());
            rect = getUpdatedCropRect(processingRequest.getCropRect(), halTransform);
            matrix = getUpdatedTransform(processingRequest.getSensorToBufferTransform(), halTransform);
            n = fromImageProxy.getRotation();
        }
        return Packet.of(imageProxy, fromImageProxy, rect, n, matrix, cameraCaptureResult);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.imagecapture;

import androidx.camera.core.processing.Node;

public interface BundlingNode extends Node<CaptureNode.Out, ProcessingNode.In>
{
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.imagecapture;

import androidx.camera.core.impl.utils.Threads;
import androidx.camera.core.ImageCaptureException;
import androidx.camera.core.impl.CaptureConfig;
import java.util.List;

public final class CameraRequest
{
    private final TakePictureCallback mCallback;
    private final List<CaptureConfig> mCaptureConfigs;
    
    public CameraRequest(final List<CaptureConfig> mCaptureConfigs, final TakePictureCallback mCallback) {
        this.mCaptureConfigs = mCaptureConfigs;
        this.mCallback = mCallback;
    }
    
    List<CaptureConfig> getCaptureConfigs() {
        return this.mCaptureConfigs;
    }
    
    void onCaptureFailure(final ImageCaptureException ex) {
        Threads.checkMainThread();
        this.mCallback.onCaptureFailure(ex);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.imagecapture;

import androidx.core.util.Consumer;
import java.util.Objects;
import androidx.core.util.Preconditions;
import androidx.camera.core.impl.utils.Threads;
import androidx.camera.core.ImageProxy;

class SingleBundlingNode implements BundlingNode
{
    private ProcessingNode.In mOutputEdge;
    private ProcessingRequest mPendingRequest;
    
    private void matchImageWithRequest(final ImageProxy imageProxy) {
        Threads.checkMainThread();
        final ProcessingRequest mPendingRequest = this.mPendingRequest;
        final boolean b = true;
        Preconditions.checkState(mPendingRequest != null);
        Preconditions.checkState(Objects.requireNonNull(imageProxy.getImageInfo().getTagBundle().getTag(this.mPendingRequest.getTagBundleKey())) == (int)this.mPendingRequest.getStageIds().get(0) && b);
        this.mOutputEdge.getEdge().accept(ProcessingNode.InputPacket.of(this.mPendingRequest, imageProxy));
        this.mPendingRequest = null;
    }
    
    private void trackIncomingRequest(final ProcessingRequest mPendingRequest) {
        Threads.checkMainThread();
        final int size = mPendingRequest.getStageIds().size();
        final boolean b = false;
        Preconditions.checkState(size == 1, "Cannot handle multi-image capture.");
        boolean b2 = b;
        if (this.mPendingRequest == null) {
            b2 = true;
        }
        Preconditions.checkState(b2, "Already has an existing request.");
        this.mPendingRequest = mPendingRequest;
    }
    
    @Override
    public void release() {
    }
    
    @Override
    public ProcessingNode.In transform(final CaptureNode.Out out) {
        out.getImageEdge().setListener(new SingleBundlingNode$$ExternalSyntheticLambda0(this));
        out.getRequestEdge().setListener(new SingleBundlingNode$$ExternalSyntheticLambda1(this));
        return this.mOutputEdge = ProcessingNode.In.of(out.getFormat());
    }
}

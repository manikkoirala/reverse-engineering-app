// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.imagecapture;

import androidx.camera.core.ImageProxy;
import androidx.core.util.Pair;
import java.util.Objects;
import android.util.Log;
import java.util.Iterator;
import androidx.core.util.Preconditions;
import java.util.concurrent.Executor;
import androidx.camera.core.impl.utils.futures.Futures;
import androidx.camera.core.impl.utils.executor.CameraXExecutors;
import androidx.camera.core.ImageCaptureException;
import androidx.camera.core.impl.utils.futures.FutureCallback;
import androidx.camera.core.impl.utils.Threads;
import java.util.ArrayDeque;
import java.util.Deque;
import androidx.camera.core.ForwardingImageProxy;

public class TakePictureManager implements OnImageCloseListener
{
    private static final String TAG = "TakePictureManager";
    final ImageCaptureControl mImageCaptureControl;
    final ImagePipeline mImagePipeline;
    RequestWithCallback mInFlightRequest;
    final Deque<TakePictureRequest> mNewRequests;
    boolean mPaused;
    
    public TakePictureManager(final ImageCaptureControl mImageCaptureControl, final ImagePipeline mImagePipeline) {
        this.mNewRequests = new ArrayDeque<TakePictureRequest>();
        this.mPaused = false;
        Threads.checkMainThread();
        this.mImageCaptureControl = mImageCaptureControl;
        (this.mImagePipeline = mImagePipeline).setOnImageCloseListener(this);
    }
    
    private void submitCameraRequest(final CameraRequest cameraRequest, final Runnable runnable) {
        Threads.checkMainThread();
        this.mImageCaptureControl.lockFlashMode();
        Futures.addCallback(this.mImageCaptureControl.submitStillCaptureRequests(cameraRequest.getCaptureConfigs()), new FutureCallback<Void>(this, runnable, cameraRequest) {
            final TakePictureManager this$0;
            final CameraRequest val$cameraRequest;
            final Runnable val$successRunnable;
            
            @Override
            public void onFailure(final Throwable t) {
                if (t instanceof ImageCaptureException) {
                    this.val$cameraRequest.onCaptureFailure((ImageCaptureException)t);
                }
                else {
                    this.val$cameraRequest.onCaptureFailure(new ImageCaptureException(2, "Failed to submit capture request", t));
                }
                this.this$0.mImageCaptureControl.unlockFlashMode();
            }
            
            @Override
            public void onSuccess(final Void void1) {
                this.val$successRunnable.run();
                this.this$0.mImageCaptureControl.unlockFlashMode();
            }
        }, CameraXExecutors.mainThreadExecutor());
    }
    
    private void trackCurrentRequest(final RequestWithCallback mInFlightRequest) {
        Preconditions.checkState(this.hasInFlightRequest() ^ true);
        this.mInFlightRequest = mInFlightRequest;
        mInFlightRequest.getCaptureFuture().addListener((Runnable)new TakePictureManager$$ExternalSyntheticLambda1(this), CameraXExecutors.directExecutor());
    }
    
    public void abortRequests() {
        Threads.checkMainThread();
        final ImageCaptureException ex = new ImageCaptureException(3, "Camera is closed.", null);
        final Iterator<TakePictureRequest> iterator = this.mNewRequests.iterator();
        while (iterator.hasNext()) {
            iterator.next().onError(ex);
        }
        this.mNewRequests.clear();
        final RequestWithCallback mInFlightRequest = this.mInFlightRequest;
        if (mInFlightRequest != null) {
            mInFlightRequest.abort(ex);
        }
    }
    
    boolean hasInFlightRequest() {
        return this.mInFlightRequest != null;
    }
    
    void issueNextRequest() {
        Threads.checkMainThread();
        Log.d("TakePictureManager", "Issue the next TakePictureRequest.");
        if (this.hasInFlightRequest()) {
            Log.d("TakePictureManager", "There is already a request in-flight.");
            return;
        }
        if (this.mPaused) {
            Log.d("TakePictureManager", "The class is paused.");
            return;
        }
        if (this.mImagePipeline.getCapacity() == 0) {
            Log.d("TakePictureManager", "Too many acquire images. Close image to be able to process next.");
            return;
        }
        final TakePictureRequest takePictureRequest = this.mNewRequests.poll();
        if (takePictureRequest == null) {
            Log.d("TakePictureManager", "No new request.");
            return;
        }
        final RequestWithCallback requestWithCallback = new RequestWithCallback(takePictureRequest);
        this.trackCurrentRequest(requestWithCallback);
        final Pair<CameraRequest, ProcessingRequest> requests = this.mImagePipeline.createRequests(takePictureRequest, requestWithCallback);
        this.submitCameraRequest(Objects.requireNonNull(requests.first), new TakePictureManager$$ExternalSyntheticLambda0(this, Objects.requireNonNull(requests.second)));
    }
    
    public void offerRequest(final TakePictureRequest takePictureRequest) {
        Threads.checkMainThread();
        this.mNewRequests.offer(takePictureRequest);
        this.issueNextRequest();
    }
    
    @Override
    public void onImageClose(final ImageProxy imageProxy) {
        CameraXExecutors.mainThreadExecutor().execute(new TakePictureManager$$ExternalSyntheticLambda2(this));
    }
    
    public void pause() {
        Threads.checkMainThread();
        this.mPaused = true;
    }
    
    public void resume() {
        Threads.checkMainThread();
        this.mPaused = false;
        this.issueNextRequest();
    }
}

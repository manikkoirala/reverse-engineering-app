// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.imagecapture;

import androidx.camera.core.processing.Edge;
import androidx.core.util.Consumer;
import java.util.Objects;
import androidx.camera.core.impl.utils.executor.CameraXExecutors;
import androidx.camera.core.ImageCaptureException;
import androidx.camera.core.ImageCapture;
import android.graphics.Bitmap;
import androidx.camera.core.ImageProxy;
import java.util.concurrent.Executor;
import androidx.camera.core.processing.Packet;
import androidx.camera.core.processing.Operation;
import androidx.camera.core.processing.Node;

public class ProcessingNode implements Node<In, Void>
{
    private Operation<Bitmap2JpegBytes.In, Packet<byte[]>> mBitmap2JpegBytes;
    private final Executor mBlockingExecutor;
    private Operation<Image2JpegBytes.In, Packet<byte[]>> mImage2JpegBytes;
    private Operation<InputPacket, Packet<ImageProxy>> mInput2Packet;
    private Operation<Packet<byte[]>, Packet<Bitmap>> mJpegBytes2CroppedBitmap;
    private Operation<JpegBytes2Disk.In, ImageCapture.OutputFileResults> mJpegBytes2Disk;
    private Operation<Packet<byte[]>, Packet<ImageProxy>> mJpegBytes2Image;
    private Operation<Packet<ImageProxy>, ImageProxy> mJpegImage2Result;
    
    ProcessingNode(final Executor mBlockingExecutor) {
        this.mBlockingExecutor = mBlockingExecutor;
    }
    
    private static void sendError(final ProcessingRequest processingRequest, final ImageCaptureException ex) {
        CameraXExecutors.mainThreadExecutor().execute(new ProcessingNode$$ExternalSyntheticLambda1(processingRequest, ex));
    }
    
    void injectJpegBytes2CroppedBitmapForTesting(final Operation<Packet<byte[]>, Packet<Bitmap>> mJpegBytes2CroppedBitmap) {
        this.mJpegBytes2CroppedBitmap = mJpegBytes2CroppedBitmap;
    }
    
    ImageProxy processInMemoryCapture(final InputPacket inputPacket) throws ImageCaptureException {
        final ProcessingRequest processingRequest = inputPacket.getProcessingRequest();
        Packet packet2;
        final Packet packet = packet2 = this.mInput2Packet.apply(inputPacket);
        if (packet.getFormat() == 35) {
            packet2 = this.mJpegBytes2Image.apply(this.mImage2JpegBytes.apply(Image2JpegBytes.In.of(packet, processingRequest.getJpegQuality())));
        }
        return this.mJpegImage2Result.apply(packet2);
    }
    
    void processInputPacket(final InputPacket inputPacket) {
        final ProcessingRequest processingRequest = inputPacket.getProcessingRequest();
        try {
            if (inputPacket.getProcessingRequest().isInMemoryCapture()) {
                CameraXExecutors.mainThreadExecutor().execute(new ProcessingNode$$ExternalSyntheticLambda3(processingRequest, this.processInMemoryCapture(inputPacket)));
            }
            else {
                CameraXExecutors.mainThreadExecutor().execute(new ProcessingNode$$ExternalSyntheticLambda4(processingRequest, this.processOnDiskCapture(inputPacket)));
            }
        }
        catch (final RuntimeException ex) {
            sendError(processingRequest, new ImageCaptureException(0, "Processing failed.", ex));
        }
        catch (final ImageCaptureException ex2) {
            sendError(processingRequest, ex2);
        }
    }
    
    ImageCapture.OutputFileResults processOnDiskCapture(final InputPacket inputPacket) throws ImageCaptureException {
        final ProcessingRequest processingRequest = inputPacket.getProcessingRequest();
        Packet packet2;
        final Packet packet = packet2 = this.mImage2JpegBytes.apply(Image2JpegBytes.In.of(this.mInput2Packet.apply(inputPacket), processingRequest.getJpegQuality()));
        if (packet.hasCropping()) {
            packet2 = this.mBitmap2JpegBytes.apply(Bitmap2JpegBytes.In.of(this.mJpegBytes2CroppedBitmap.apply(packet), processingRequest.getJpegQuality()));
        }
        return this.mJpegBytes2Disk.apply(JpegBytes2Disk.In.of(packet2, Objects.requireNonNull(processingRequest.getOutputFileOptions())));
    }
    
    @Override
    public void release() {
    }
    
    @Override
    public Void transform(final In in) {
        in.getEdge().setListener(new ProcessingNode$$ExternalSyntheticLambda0(this));
        this.mInput2Packet = new ProcessingInput2Packet();
        this.mImage2JpegBytes = new Image2JpegBytes();
        this.mJpegBytes2CroppedBitmap = new JpegBytes2CroppedBitmap();
        this.mBitmap2JpegBytes = new Bitmap2JpegBytes();
        this.mJpegBytes2Disk = new JpegBytes2Disk();
        this.mJpegImage2Result = new JpegImage2Result();
        if (in.getFormat() == 35) {
            this.mJpegBytes2Image = new JpegBytes2Image();
        }
        return null;
    }
    
    abstract static class In
    {
        static In of(final int n) {
            return (In)new AutoValue_ProcessingNode_In(new Edge<InputPacket>(), n);
        }
        
        abstract Edge<InputPacket> getEdge();
        
        abstract int getFormat();
    }
    
    abstract static class InputPacket
    {
        static InputPacket of(final ProcessingRequest processingRequest, final ImageProxy imageProxy) {
            return (InputPacket)new AutoValue_ProcessingNode_InputPacket(processingRequest, imageProxy);
        }
        
        abstract ImageProxy getImageProxy();
        
        abstract ProcessingRequest getProcessingRequest();
    }
}

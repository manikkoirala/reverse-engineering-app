// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.imagecapture;

import androidx.camera.core.processing.Edge;

final class AutoValue_ProcessingNode_In extends In
{
    private final Edge<InputPacket> edge;
    private final int format;
    
    AutoValue_ProcessingNode_In(final Edge<InputPacket> edge, final int format) {
        if (edge != null) {
            this.edge = edge;
            this.format = format;
            return;
        }
        throw new NullPointerException("Null edge");
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o == this) {
            return true;
        }
        if (o instanceof In) {
            final In in = (In)o;
            if (!this.edge.equals(in.getEdge()) || this.format != in.getFormat()) {
                b = false;
            }
            return b;
        }
        return false;
    }
    
    @Override
    Edge<InputPacket> getEdge() {
        return this.edge;
    }
    
    @Override
    int getFormat() {
        return this.format;
    }
    
    @Override
    public int hashCode() {
        return (this.edge.hashCode() ^ 0xF4243) * 1000003 ^ this.format;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("In{edge=");
        sb.append(this.edge);
        sb.append(", format=");
        sb.append(this.format);
        sb.append("}");
        return sb.toString();
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.imagecapture;

import com.google.common.util.concurrent.ListenableFuture;
import androidx.camera.core.impl.CaptureConfig;
import java.util.List;

public interface ImageCaptureControl
{
    void lockFlashMode();
    
    ListenableFuture<Void> submitStillCaptureRequests(final List<CaptureConfig> p0);
    
    void unlockFlashMode();
}

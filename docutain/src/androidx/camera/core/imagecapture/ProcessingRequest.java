// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.imagecapture;

import androidx.camera.core.ImageCaptureException;
import androidx.camera.core.ImageProxy;
import java.util.Iterator;
import java.util.Objects;
import androidx.camera.core.impl.CaptureStage;
import java.util.ArrayList;
import androidx.camera.core.impl.CaptureBundle;
import java.util.List;
import android.graphics.Matrix;
import androidx.camera.core.ImageCapture;
import android.graphics.Rect;

class ProcessingRequest
{
    private final TakePictureCallback mCallback;
    private final Rect mCropRect;
    private final int mJpegQuality;
    private final ImageCapture.OutputFileOptions mOutputFileOptions;
    private final int mRotationDegrees;
    private final Matrix mSensorToBufferTransform;
    private final List<Integer> mStageIds;
    private final String mTagBundleKey;
    
    ProcessingRequest(final CaptureBundle captureBundle, final ImageCapture.OutputFileOptions mOutputFileOptions, final Rect mCropRect, final int mRotationDegrees, final int mJpegQuality, final Matrix mSensorToBufferTransform, final TakePictureCallback mCallback) {
        this.mOutputFileOptions = mOutputFileOptions;
        this.mJpegQuality = mJpegQuality;
        this.mRotationDegrees = mRotationDegrees;
        this.mCropRect = mCropRect;
        this.mSensorToBufferTransform = mSensorToBufferTransform;
        this.mCallback = mCallback;
        this.mTagBundleKey = String.valueOf(captureBundle.hashCode());
        this.mStageIds = new ArrayList<Integer>();
        final Iterator<CaptureStage> iterator = Objects.requireNonNull(captureBundle.getCaptureStages()).iterator();
        while (iterator.hasNext()) {
            this.mStageIds.add(iterator.next().getId());
        }
    }
    
    Rect getCropRect() {
        return this.mCropRect;
    }
    
    int getJpegQuality() {
        return this.mJpegQuality;
    }
    
    ImageCapture.OutputFileOptions getOutputFileOptions() {
        return this.mOutputFileOptions;
    }
    
    int getRotationDegrees() {
        return this.mRotationDegrees;
    }
    
    Matrix getSensorToBufferTransform() {
        return this.mSensorToBufferTransform;
    }
    
    List<Integer> getStageIds() {
        return this.mStageIds;
    }
    
    String getTagBundleKey() {
        return this.mTagBundleKey;
    }
    
    boolean isAborted() {
        return this.mCallback.isAborted();
    }
    
    boolean isInMemoryCapture() {
        return this.getOutputFileOptions() == null;
    }
    
    void onFinalResult(final ImageCapture.OutputFileResults outputFileResults) {
        this.mCallback.onFinalResult(outputFileResults);
    }
    
    void onFinalResult(final ImageProxy imageProxy) {
        this.mCallback.onFinalResult(imageProxy);
    }
    
    void onImageCaptured() {
        this.mCallback.onImageCaptured();
    }
    
    void onProcessFailure(final ImageCaptureException ex) {
        this.mCallback.onProcessFailure(ex);
    }
}

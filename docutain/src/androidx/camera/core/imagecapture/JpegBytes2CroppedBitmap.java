// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.imagecapture;

import androidx.camera.core.impl.utils.TransformUtils;
import java.util.Objects;
import androidx.camera.core.impl.utils.Exif;
import java.io.IOException;
import androidx.camera.core.ImageCaptureException;
import android.graphics.BitmapFactory$Options;
import android.graphics.BitmapRegionDecoder;
import android.graphics.Rect;
import android.graphics.Bitmap;
import androidx.camera.core.processing.Packet;
import androidx.camera.core.processing.Operation;

final class JpegBytes2CroppedBitmap implements Operation<Packet<byte[]>, Packet<Bitmap>>
{
    private Bitmap createCroppedBitmap(final byte[] array, final Rect rect) throws ImageCaptureException {
        try {
            return BitmapRegionDecoder.newInstance(array, 0, array.length, false).decodeRegion(rect, new BitmapFactory$Options());
        }
        catch (final IOException ex) {
            throw new ImageCaptureException(1, "Failed to decode JPEG.", ex);
        }
    }
    
    @Override
    public Packet<Bitmap> apply(final Packet<byte[]> packet) throws ImageCaptureException {
        final Rect cropRect = packet.getCropRect();
        final Bitmap croppedBitmap = this.createCroppedBitmap(packet.getData(), cropRect);
        return Packet.of(croppedBitmap, Objects.requireNonNull(packet.getExif()), new Rect(0, 0, croppedBitmap.getWidth(), croppedBitmap.getHeight()), packet.getRotationDegrees(), TransformUtils.updateSensorToBufferTransform(packet.getSensorToBufferTransform(), cropRect), packet.getCameraCaptureResult());
    }
}

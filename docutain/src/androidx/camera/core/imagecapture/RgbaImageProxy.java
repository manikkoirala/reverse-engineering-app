// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.imagecapture;

import java.util.Objects;
import android.media.Image;
import androidx.camera.core.impl.utils.ExifData;
import androidx.camera.core.impl.TagBundle;
import androidx.core.util.Preconditions;
import java.nio.ByteBuffer;
import androidx.camera.core.processing.Packet;
import androidx.camera.core.internal.utils.ImageUtil;
import android.graphics.Matrix;
import android.graphics.Bitmap;
import androidx.camera.core.ImageInfo;
import android.graphics.Rect;
import androidx.camera.core.ImageProxy;

public final class RgbaImageProxy implements ImageProxy
{
    private final Rect mCropRect;
    private final int mHeight;
    private final ImageInfo mImageInfo;
    private final Object mLock;
    PlaneProxy[] mPlaneProxy;
    private final int mWidth;
    
    RgbaImageProxy(final Bitmap bitmap, final Rect rect, final int n, final Matrix matrix, final long n2) {
        this(ImageUtil.createDirectByteBuffer(bitmap), 4, bitmap.getWidth(), bitmap.getHeight(), rect, n, matrix, n2);
    }
    
    public RgbaImageProxy(final Packet<Bitmap> packet) {
        this((Bitmap)packet.getData(), packet.getCropRect(), packet.getRotationDegrees(), packet.getSensorToBufferTransform(), packet.getCameraCaptureResult().getTimestamp());
    }
    
    public RgbaImageProxy(final ByteBuffer byteBuffer, final int n, final int mWidth, final int mHeight, final Rect mCropRect, final int n2, final Matrix matrix, final long n3) {
        this.mLock = new Object();
        this.mWidth = mWidth;
        this.mHeight = mHeight;
        this.mCropRect = mCropRect;
        this.mImageInfo = createImageInfo(n3, n2, matrix);
        byteBuffer.rewind();
        this.mPlaneProxy = new PlaneProxy[] { createPlaneProxy(byteBuffer, mWidth * n, n) };
    }
    
    private void checkNotClosed() {
        synchronized (this.mLock) {
            Preconditions.checkState(this.mPlaneProxy != null, "The image is closed.");
        }
    }
    
    private static ImageInfo createImageInfo(final long n, final int n2, final Matrix matrix) {
        return new ImageInfo(n, n2, matrix) {
            final int val$rotationDegrees;
            final Matrix val$sensorToBuffer;
            final long val$timestamp;
            
            @Override
            public int getRotationDegrees() {
                return this.val$rotationDegrees;
            }
            
            @Override
            public Matrix getSensorToBufferTransformMatrix() {
                return new Matrix(this.val$sensorToBuffer);
            }
            
            @Override
            public TagBundle getTagBundle() {
                throw new UnsupportedOperationException("Custom ImageProxy does not contain TagBundle");
            }
            
            @Override
            public long getTimestamp() {
                return this.val$timestamp;
            }
            
            @Override
            public void populateExifData(final ExifData.Builder builder) {
                throw new UnsupportedOperationException("Custom ImageProxy does not contain Exif data.");
            }
        };
    }
    
    private static PlaneProxy createPlaneProxy(final ByteBuffer byteBuffer, final int n, final int n2) {
        return new PlaneProxy(n, n2, byteBuffer) {
            final ByteBuffer val$byteBuffer;
            final int val$pixelStride;
            final int val$rowStride;
            
            @Override
            public ByteBuffer getBuffer() {
                return this.val$byteBuffer;
            }
            
            @Override
            public int getPixelStride() {
                return this.val$pixelStride;
            }
            
            @Override
            public int getRowStride() {
                return this.val$rowStride;
            }
        };
    }
    
    @Override
    public void close() {
        synchronized (this.mLock) {
            this.checkNotClosed();
            this.mPlaneProxy = null;
        }
    }
    
    public Bitmap createBitmap() {
        synchronized (this.mLock) {
            this.checkNotClosed();
            return ImageUtil.createBitmapFromPlane(this.getPlanes(), this.getWidth(), this.getHeight());
        }
    }
    
    @Override
    public Rect getCropRect() {
        synchronized (this.mLock) {
            this.checkNotClosed();
            return this.mCropRect;
        }
    }
    
    @Override
    public int getFormat() {
        synchronized (this.mLock) {
            this.checkNotClosed();
            return 1;
        }
    }
    
    @Override
    public int getHeight() {
        synchronized (this.mLock) {
            this.checkNotClosed();
            return this.mHeight;
        }
    }
    
    @Override
    public Image getImage() {
        synchronized (this.mLock) {
            this.checkNotClosed();
            return null;
        }
    }
    
    @Override
    public ImageInfo getImageInfo() {
        synchronized (this.mLock) {
            this.checkNotClosed();
            return this.mImageInfo;
        }
    }
    
    @Override
    public PlaneProxy[] getPlanes() {
        synchronized (this.mLock) {
            this.checkNotClosed();
            return Objects.requireNonNull(this.mPlaneProxy);
        }
    }
    
    @Override
    public int getWidth() {
        synchronized (this.mLock) {
            this.checkNotClosed();
            return this.mWidth;
        }
    }
    
    @Override
    public void setCropRect(final Rect rect) {
        synchronized (this.mLock) {
            this.checkNotClosed();
            if (rect != null) {
                this.mCropRect.set(rect);
            }
        }
    }
}

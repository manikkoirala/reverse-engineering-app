// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.imagecapture;

import androidx.camera.core.impl.utils.TransformUtils;
import android.graphics.Rect;
import android.util.Size;
import java.io.OutputStream;
import androidx.camera.core.impl.utils.ExifOutputStream;
import androidx.camera.core.impl.utils.ExifData;
import androidx.camera.core.internal.ByteBufferOutputStream;
import android.graphics.YuvImage;
import java.util.Objects;
import androidx.camera.core.internal.utils.ImageUtil;
import androidx.camera.core.ImageProxy;
import java.io.IOException;
import androidx.camera.core.ImageCaptureException;
import java.io.InputStream;
import java.io.ByteArrayInputStream;
import androidx.camera.core.impl.utils.Exif;
import java.nio.ByteBuffer;
import androidx.camera.core.processing.Packet;
import androidx.camera.core.processing.Operation;

final class Image2JpegBytes implements Operation<In, Packet<byte[]>>
{
    private static byte[] byteBufferToByteArray(final ByteBuffer byteBuffer) {
        final int position = byteBuffer.position();
        final byte[] dst = new byte[position];
        byteBuffer.rewind();
        byteBuffer.get(dst, 0, position);
        return dst;
    }
    
    private static Exif extractExif(final byte[] buf) throws ImageCaptureException {
        try {
            return Exif.createFromInputStream(new ByteArrayInputStream(buf));
        }
        catch (final IOException ex) {
            throw new ImageCaptureException(0, "Failed to extract Exif from YUV-generated JPEG", ex);
        }
    }
    
    private Packet<byte[]> processJpegImage(final In in) {
        final Packet<ImageProxy> packet = in.getPacket();
        return Packet.of(ImageUtil.jpegImageToJpegByteArray(packet.getData()), Objects.requireNonNull(packet.getExif()), 256, packet.getSize(), packet.getCropRect(), packet.getRotationDegrees(), packet.getSensorToBufferTransform(), packet.getCameraCaptureResult());
    }
    
    private Packet<byte[]> processYuvImage(final In in) throws ImageCaptureException {
        final Packet<ImageProxy> packet = in.getPacket();
        final ImageProxy imageProxy = packet.getData();
        final Rect cropRect = packet.getCropRect();
        final YuvImage yuvImage = new YuvImage(ImageUtil.yuv_420_888toNv21(imageProxy), 17, imageProxy.getWidth(), imageProxy.getHeight(), (int[])null);
        final ByteBuffer allocateDirect = ByteBuffer.allocateDirect(cropRect.width() * cropRect.height() * 2);
        yuvImage.compressToJpeg(cropRect, in.getJpegQuality(), (OutputStream)new ExifOutputStream(new ByteBufferOutputStream(allocateDirect), ExifData.create(imageProxy, packet.getRotationDegrees())));
        final byte[] byteBufferToByteArray = byteBufferToByteArray(allocateDirect);
        return Packet.of(byteBufferToByteArray, extractExif(byteBufferToByteArray), 256, new Size(cropRect.width(), cropRect.height()), new Rect(0, 0, cropRect.width(), cropRect.height()), packet.getRotationDegrees(), TransformUtils.updateSensorToBufferTransform(packet.getSensorToBufferTransform(), cropRect), packet.getCameraCaptureResult());
    }
    
    @Override
    public Packet<byte[]> apply(final In in) throws ImageCaptureException {
        try {
            final int format = in.getPacket().getFormat();
            Packet<byte[]> packet;
            if (format != 35) {
                if (format != 256) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Unexpected format: ");
                    sb.append(format);
                    throw new IllegalArgumentException(sb.toString());
                }
                packet = this.processJpegImage(in);
            }
            else {
                packet = this.processYuvImage(in);
            }
            return packet;
        }
        finally {
            in.getPacket().getData().close();
        }
    }
    
    abstract static class In
    {
        static In of(final Packet<ImageProxy> packet, final int n) {
            return (In)new AutoValue_Image2JpegBytes_In(packet, n);
        }
        
        abstract int getJpegQuality();
        
        abstract Packet<ImageProxy> getPacket();
    }
}

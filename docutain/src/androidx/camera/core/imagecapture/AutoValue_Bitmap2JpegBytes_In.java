// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.imagecapture;

import android.graphics.Bitmap;
import androidx.camera.core.processing.Packet;

final class AutoValue_Bitmap2JpegBytes_In extends In
{
    private final int jpegQuality;
    private final Packet<Bitmap> packet;
    
    AutoValue_Bitmap2JpegBytes_In(final Packet<Bitmap> packet, final int jpegQuality) {
        if (packet != null) {
            this.packet = packet;
            this.jpegQuality = jpegQuality;
            return;
        }
        throw new NullPointerException("Null packet");
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o == this) {
            return true;
        }
        if (o instanceof In) {
            final In in = (In)o;
            if (!this.packet.equals(in.getPacket()) || this.jpegQuality != in.getJpegQuality()) {
                b = false;
            }
            return b;
        }
        return false;
    }
    
    @Override
    int getJpegQuality() {
        return this.jpegQuality;
    }
    
    @Override
    Packet<Bitmap> getPacket() {
        return this.packet;
    }
    
    @Override
    public int hashCode() {
        return (this.packet.hashCode() ^ 0xF4243) * 1000003 ^ this.jpegQuality;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("In{packet=");
        sb.append(this.packet);
        sb.append(", jpegQuality=");
        sb.append(this.jpegQuality);
        sb.append("}");
        return sb.toString();
    }
}

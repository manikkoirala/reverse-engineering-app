// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core;

import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;
import android.view.Surface;
import androidx.core.util.Consumer;
import java.util.concurrent.Executor;
import android.util.Size;

public interface SurfaceOutput
{
    void close();
    
    int getFormat();
    
    int getRotationDegrees();
    
    Size getSize();
    
    Surface getSurface(final Executor p0, final Consumer<Event> p1);
    
    int getTargets();
    
    void updateTransformMatrix(final float[] p0, final float[] p1);
    
    public abstract static class Event
    {
        public static final int EVENT_REQUEST_CLOSE = 0;
        
        public static Event of(final int n, final SurfaceOutput surfaceOutput) {
            return (Event)new AutoValue_SurfaceOutput_Event(n, surfaceOutput);
        }
        
        public abstract int getEventCode();
        
        public abstract SurfaceOutput getSurfaceOutput();
        
        @Retention(RetentionPolicy.SOURCE)
        public @interface EventCode {
        }
    }
    
    public enum GlTransformOptions
    {
        private static final GlTransformOptions[] $VALUES;
        
        APPLY_CROP_ROTATE_AND_MIRRORING, 
        USE_SURFACE_TEXTURE_TRANSFORM;
    }
}

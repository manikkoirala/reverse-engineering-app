// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core;

import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;
import androidx.core.util.Preconditions;
import java.util.concurrent.Executor;

public class CameraEffect
{
    public static final int IMAGE_CAPTURE = 4;
    public static final int PREVIEW = 1;
    public static final int VIDEO_CAPTURE = 2;
    private final ImageProcessor mImageProcessor;
    private final Executor mProcessorExecutor;
    private final SurfaceProcessor mSurfaceProcessor;
    private final int mTargets;
    
    protected CameraEffect(final int mTargets, final Executor mProcessorExecutor, final ImageProcessor mImageProcessor) {
        this.mTargets = mTargets;
        this.mProcessorExecutor = mProcessorExecutor;
        this.mSurfaceProcessor = null;
        this.mImageProcessor = mImageProcessor;
    }
    
    protected CameraEffect(final int mTargets, final Executor mProcessorExecutor, final SurfaceProcessor mSurfaceProcessor) {
        this.mTargets = mTargets;
        this.mProcessorExecutor = mProcessorExecutor;
        this.mSurfaceProcessor = mSurfaceProcessor;
        this.mImageProcessor = null;
    }
    
    public ImageProcessor getImageProcessor() {
        return this.mImageProcessor;
    }
    
    public Executor getProcessorExecutor() {
        return this.mProcessorExecutor;
    }
    
    public SurfaceProcessor getSurfaceProcessor() {
        return this.mSurfaceProcessor;
    }
    
    public int getTargets() {
        return this.mTargets;
    }
    
    public static class Builder
    {
        private ImageProcessor mImageProcessor;
        private Executor mProcessorExecutor;
        private SurfaceProcessor mSurfaceProcessor;
        private final int mTargets;
        
        public Builder(final int mTargets) {
            this.mTargets = mTargets;
        }
        
        public CameraEffect build() {
            final Executor mProcessorExecutor = this.mProcessorExecutor;
            boolean b = true;
            Preconditions.checkState(mProcessorExecutor != null, "Must have a executor");
            final boolean b2 = this.mImageProcessor != null;
            if (this.mSurfaceProcessor == null) {
                b = false;
            }
            Preconditions.checkState(b2 ^ b, "Must have one and only one processor");
            final SurfaceProcessor mSurfaceProcessor = this.mSurfaceProcessor;
            if (mSurfaceProcessor != null) {
                return new CameraEffect(this.mTargets, this.mProcessorExecutor, mSurfaceProcessor);
            }
            return new CameraEffect(this.mTargets, this.mProcessorExecutor, this.mImageProcessor);
        }
        
        public Builder setImageProcessor(final Executor mProcessorExecutor, final ImageProcessor mImageProcessor) {
            this.mProcessorExecutor = mProcessorExecutor;
            this.mImageProcessor = mImageProcessor;
            return this;
        }
        
        public Builder setSurfaceProcessor(final Executor mProcessorExecutor, final SurfaceProcessor mSurfaceProcessor) {
            this.mProcessorExecutor = mProcessorExecutor;
            this.mSurfaceProcessor = mSurfaceProcessor;
            return this;
        }
    }
    
    @Retention(RetentionPolicy.SOURCE)
    public @interface Targets {
    }
}

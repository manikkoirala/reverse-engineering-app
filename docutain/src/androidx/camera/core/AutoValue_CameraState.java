// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core;

final class AutoValue_CameraState extends CameraState
{
    private final StateError error;
    private final Type type;
    
    AutoValue_CameraState(final Type type, final StateError error) {
        if (type != null) {
            this.type = type;
            this.error = error;
            return;
        }
        throw new NullPointerException("Null type");
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o == this) {
            return true;
        }
        if (o instanceof CameraState) {
            final CameraState cameraState = (CameraState)o;
            if (this.type.equals(cameraState.getType())) {
                final StateError error = this.error;
                if (error == null) {
                    if (cameraState.getError() == null) {
                        return b;
                    }
                }
                else if (error.equals(cameraState.getError())) {
                    return b;
                }
            }
            b = false;
            return b;
        }
        return false;
    }
    
    @Override
    public StateError getError() {
        return this.error;
    }
    
    @Override
    public Type getType() {
        return this.type;
    }
    
    @Override
    public int hashCode() {
        final int hashCode = this.type.hashCode();
        final StateError error = this.error;
        int hashCode2;
        if (error == null) {
            hashCode2 = 0;
        }
        else {
            hashCode2 = error.hashCode();
        }
        return (hashCode ^ 0xF4243) * 1000003 ^ hashCode2;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("CameraState{type=");
        sb.append(this.type);
        sb.append(", error=");
        sb.append(this.error);
        sb.append("}");
        return sb.toString();
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core;

import android.graphics.Rect;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;
import java.util.concurrent.ExecutionException;
import androidx.core.util.Consumer;
import java.util.concurrent.CancellationException;
import androidx.camera.core.impl.utils.futures.Futures;
import androidx.camera.core.impl.utils.executor.CameraXExecutors;
import androidx.camera.core.impl.utils.futures.FutureCallback;
import androidx.core.util.Preconditions;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.Executor;
import android.view.Surface;
import com.google.common.util.concurrent.ListenableFuture;
import android.util.Size;
import androidx.concurrent.futures.CallbackToFutureAdapter;
import androidx.camera.core.impl.DeferrableSurface;
import android.util.Range;
import androidx.camera.core.impl.CameraInternal;

public final class SurfaceRequest
{
    private final CameraInternal mCamera;
    private final Range<Integer> mExpectedFrameRate;
    private final DeferrableSurface mInternalDeferrableSurface;
    private final Object mLock;
    private final boolean mRGBA8888Required;
    private final CallbackToFutureAdapter.Completer<Void> mRequestCancellationCompleter;
    private final Size mResolution;
    private final ListenableFuture<Void> mSessionStatusFuture;
    private final CallbackToFutureAdapter.Completer<Surface> mSurfaceCompleter;
    final ListenableFuture<Surface> mSurfaceFuture;
    private TransformationInfo mTransformationInfo;
    private Executor mTransformationInfoExecutor;
    private TransformationInfoListener mTransformationInfoListener;
    
    public SurfaceRequest(final Size size, final CameraInternal cameraInternal, final boolean b) {
        this(size, cameraInternal, b, null);
    }
    
    public SurfaceRequest(final Size size, final CameraInternal mCamera, final boolean mrgba8888Required, final Range<Integer> mExpectedFrameRate) {
        this.mLock = new Object();
        this.mResolution = size;
        this.mCamera = mCamera;
        this.mRGBA8888Required = mrgba8888Required;
        this.mExpectedFrameRate = mExpectedFrameRate;
        final StringBuilder sb = new StringBuilder();
        sb.append("SurfaceRequest[size: ");
        sb.append(size);
        sb.append(", id: ");
        sb.append(this.hashCode());
        sb.append("]");
        final String string = sb.toString();
        final AtomicReference atomicReference = new AtomicReference(null);
        final com.google.common.util.concurrent.ListenableFuture<Object> future = CallbackToFutureAdapter.getFuture((CallbackToFutureAdapter.Resolver<Object>)new SurfaceRequest$$ExternalSyntheticLambda0(atomicReference, string));
        final CallbackToFutureAdapter.Completer mRequestCancellationCompleter = (CallbackToFutureAdapter.Completer)Preconditions.checkNotNull((CallbackToFutureAdapter.Completer)atomicReference.get());
        this.mRequestCancellationCompleter = mRequestCancellationCompleter;
        final AtomicReference atomicReference2 = new AtomicReference(null);
        Futures.addCallback(this.mSessionStatusFuture = CallbackToFutureAdapter.getFuture((CallbackToFutureAdapter.Resolver<Void>)new SurfaceRequest$$ExternalSyntheticLambda1(atomicReference2, string)), new FutureCallback<Void>(this, mRequestCancellationCompleter, future) {
            final SurfaceRequest this$0;
            final CallbackToFutureAdapter.Completer val$requestCancellationCompleter;
            final ListenableFuture val$requestCancellationFuture;
            
            @Override
            public void onFailure(final Throwable t) {
                if (t instanceof RequestCancelledException) {
                    Preconditions.checkState(this.val$requestCancellationFuture.cancel(false));
                }
                else {
                    Preconditions.checkState(this.val$requestCancellationCompleter.set(null));
                }
            }
            
            @Override
            public void onSuccess(final Void void1) {
                Preconditions.checkState(this.val$requestCancellationCompleter.set(null));
            }
        }, CameraXExecutors.directExecutor());
        final CallbackToFutureAdapter.Completer completer = (CallbackToFutureAdapter.Completer)Preconditions.checkNotNull((CallbackToFutureAdapter.Completer)atomicReference2.get());
        final AtomicReference atomicReference3 = new AtomicReference(null);
        final com.google.common.util.concurrent.ListenableFuture<Surface> future2 = CallbackToFutureAdapter.getFuture((CallbackToFutureAdapter.Resolver<Surface>)new SurfaceRequest$$ExternalSyntheticLambda2(atomicReference3, string));
        this.mSurfaceFuture = future2;
        this.mSurfaceCompleter = (CallbackToFutureAdapter.Completer<Surface>)Preconditions.checkNotNull(atomicReference3.get());
        final DeferrableSurface mInternalDeferrableSurface = new DeferrableSurface(this, size, 34) {
            final SurfaceRequest this$0;
            
            @Override
            protected ListenableFuture<Surface> provideSurface() {
                return this.this$0.mSurfaceFuture;
            }
        };
        this.mInternalDeferrableSurface = mInternalDeferrableSurface;
        final ListenableFuture<Void> terminationFuture = mInternalDeferrableSurface.getTerminationFuture();
        Futures.addCallback((com.google.common.util.concurrent.ListenableFuture<Object>)future2, (FutureCallback<? super Object>)new FutureCallback<Surface>(this, terminationFuture, completer, string) {
            final SurfaceRequest this$0;
            final CallbackToFutureAdapter.Completer val$sessionStatusCompleter;
            final String val$surfaceRequestString;
            final ListenableFuture val$terminationFuture;
            
            @Override
            public void onFailure(final Throwable t) {
                if (t instanceof CancellationException) {
                    final CallbackToFutureAdapter.Completer val$sessionStatusCompleter = this.val$sessionStatusCompleter;
                    final StringBuilder sb = new StringBuilder();
                    sb.append(this.val$surfaceRequestString);
                    sb.append(" cancelled.");
                    Preconditions.checkState(val$sessionStatusCompleter.setException(new RequestCancelledException(sb.toString(), t)));
                }
                else {
                    this.val$sessionStatusCompleter.set(null);
                }
            }
            
            @Override
            public void onSuccess(final Surface surface) {
                Futures.propagate((com.google.common.util.concurrent.ListenableFuture<Object>)this.val$terminationFuture, this.val$sessionStatusCompleter);
            }
        }, CameraXExecutors.directExecutor());
        terminationFuture.addListener((Runnable)new SurfaceRequest$$ExternalSyntheticLambda3(this), CameraXExecutors.directExecutor());
    }
    
    public void addRequestCancellationListener(final Executor executor, final Runnable runnable) {
        this.mRequestCancellationCompleter.addCancellationListener(runnable, executor);
    }
    
    public void clearTransformationInfoListener() {
        synchronized (this.mLock) {
            this.mTransformationInfoListener = null;
            this.mTransformationInfoExecutor = null;
        }
    }
    
    public CameraInternal getCamera() {
        return this.mCamera;
    }
    
    public DeferrableSurface getDeferrableSurface() {
        return this.mInternalDeferrableSurface;
    }
    
    public Range<Integer> getExpectedFrameRate() {
        return this.mExpectedFrameRate;
    }
    
    public Size getResolution() {
        return this.mResolution;
    }
    
    public boolean isRGBA8888Required() {
        return this.mRGBA8888Required;
    }
    
    public void provideSurface(final Surface surface, final Executor executor, final Consumer<Result> consumer) {
        if (!this.mSurfaceCompleter.set(surface) && !this.mSurfaceFuture.isCancelled()) {
            Preconditions.checkState(this.mSurfaceFuture.isDone());
            try {
                this.mSurfaceFuture.get();
                executor.execute(new SurfaceRequest$$ExternalSyntheticLambda4(consumer, surface));
            }
            catch (final InterruptedException | ExecutionException ex) {
                executor.execute(new SurfaceRequest$$ExternalSyntheticLambda5(consumer, surface));
            }
        }
        else {
            Futures.addCallback(this.mSessionStatusFuture, new FutureCallback<Void>(this, consumer, surface) {
                final SurfaceRequest this$0;
                final Consumer val$resultListener;
                final Surface val$surface;
                
                @Override
                public void onFailure(final Throwable obj) {
                    final boolean b = obj instanceof RequestCancelledException;
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Camera surface session should only fail with request cancellation. Instead failed due to:\n");
                    sb.append(obj);
                    Preconditions.checkState(b, sb.toString());
                    this.val$resultListener.accept(Result.of(1, this.val$surface));
                }
                
                @Override
                public void onSuccess(final Void void1) {
                    this.val$resultListener.accept(Result.of(0, this.val$surface));
                }
            }, executor);
        }
    }
    
    public void setTransformationInfoListener(final Executor mTransformationInfoExecutor, final TransformationInfoListener mTransformationInfoListener) {
        synchronized (this.mLock) {
            this.mTransformationInfoListener = mTransformationInfoListener;
            this.mTransformationInfoExecutor = mTransformationInfoExecutor;
            final TransformationInfo mTransformationInfo = this.mTransformationInfo;
            monitorexit(this.mLock);
            if (mTransformationInfo != null) {
                mTransformationInfoExecutor.execute(new SurfaceRequest$$ExternalSyntheticLambda6(mTransformationInfoListener, mTransformationInfo));
            }
        }
    }
    
    public void updateTransformationInfo(final TransformationInfo mTransformationInfo) {
        synchronized (this.mLock) {
            this.mTransformationInfo = mTransformationInfo;
            final TransformationInfoListener mTransformationInfoListener = this.mTransformationInfoListener;
            final Executor mTransformationInfoExecutor = this.mTransformationInfoExecutor;
            monitorexit(this.mLock);
            if (mTransformationInfoListener != null && mTransformationInfoExecutor != null) {
                mTransformationInfoExecutor.execute(new SurfaceRequest$$ExternalSyntheticLambda7(mTransformationInfoListener, mTransformationInfo));
            }
        }
    }
    
    public boolean willNotProvideSurface() {
        return this.mSurfaceCompleter.setException(new DeferrableSurface.SurfaceUnavailableException("Surface request will not complete."));
    }
    
    private static final class RequestCancelledException extends RuntimeException
    {
        RequestCancelledException(final String message, final Throwable cause) {
            super(message, cause);
        }
    }
    
    public abstract static class Result
    {
        public static final int RESULT_INVALID_SURFACE = 2;
        public static final int RESULT_REQUEST_CANCELLED = 1;
        public static final int RESULT_SURFACE_ALREADY_PROVIDED = 3;
        public static final int RESULT_SURFACE_USED_SUCCESSFULLY = 0;
        public static final int RESULT_WILL_NOT_PROVIDE_SURFACE = 4;
        
        Result() {
        }
        
        static Result of(final int n, final Surface surface) {
            return (Result)new AutoValue_SurfaceRequest_Result(n, surface);
        }
        
        public abstract int getResultCode();
        
        public abstract Surface getSurface();
        
        @Retention(RetentionPolicy.SOURCE)
        public @interface ResultCode {
        }
    }
    
    public abstract static class TransformationInfo
    {
        TransformationInfo() {
        }
        
        public static TransformationInfo of(final Rect rect, final int n, final int n2) {
            return (TransformationInfo)new AutoValue_SurfaceRequest_TransformationInfo(rect, n, n2);
        }
        
        public abstract Rect getCropRect();
        
        public abstract int getRotationDegrees();
        
        public abstract int getTargetRotation();
    }
    
    public interface TransformationInfoListener
    {
        void onTransformationInfoUpdate(final TransformationInfo p0);
    }
}

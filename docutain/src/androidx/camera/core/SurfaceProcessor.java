// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core;

public interface SurfaceProcessor
{
    void onInputSurface(final SurfaceRequest p0);
    
    void onOutputSurface(final SurfaceOutput p0);
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core;

import android.graphics.Rect;
import android.util.Size;

public abstract class ResolutionInfo
{
    ResolutionInfo() {
    }
    
    static ResolutionInfo create(final Size size, final Rect rect, final int n) {
        return new AutoValue_ResolutionInfo(size, rect, n);
    }
    
    public abstract Rect getCropRect();
    
    public abstract Size getResolution();
    
    public abstract int getRotationDegrees();
}

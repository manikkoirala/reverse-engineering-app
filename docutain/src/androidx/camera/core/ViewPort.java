// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core;

import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;
import androidx.core.util.Preconditions;
import android.util.Rational;

public final class ViewPort
{
    public static final int FILL_CENTER = 1;
    public static final int FILL_END = 2;
    public static final int FILL_START = 0;
    public static final int FIT = 3;
    private Rational mAspectRatio;
    private int mLayoutDirection;
    private int mRotation;
    private int mScaleType;
    
    ViewPort(final int mScaleType, final Rational mAspectRatio, final int mRotation, final int mLayoutDirection) {
        this.mScaleType = mScaleType;
        this.mAspectRatio = mAspectRatio;
        this.mRotation = mRotation;
        this.mLayoutDirection = mLayoutDirection;
    }
    
    public Rational getAspectRatio() {
        return this.mAspectRatio;
    }
    
    public int getLayoutDirection() {
        return this.mLayoutDirection;
    }
    
    public int getRotation() {
        return this.mRotation;
    }
    
    public int getScaleType() {
        return this.mScaleType;
    }
    
    public static final class Builder
    {
        private static final int DEFAULT_LAYOUT_DIRECTION = 0;
        private static final int DEFAULT_SCALE_TYPE = 1;
        private final Rational mAspectRatio;
        private int mLayoutDirection;
        private final int mRotation;
        private int mScaleType;
        
        public Builder(final Rational mAspectRatio, final int mRotation) {
            this.mScaleType = 1;
            this.mLayoutDirection = 0;
            this.mAspectRatio = mAspectRatio;
            this.mRotation = mRotation;
        }
        
        public ViewPort build() {
            Preconditions.checkNotNull(this.mAspectRatio, "The crop aspect ratio must be set.");
            return new ViewPort(this.mScaleType, this.mAspectRatio, this.mRotation, this.mLayoutDirection);
        }
        
        public Builder setLayoutDirection(final int mLayoutDirection) {
            this.mLayoutDirection = mLayoutDirection;
            return this;
        }
        
        public Builder setScaleType(final int mScaleType) {
            this.mScaleType = mScaleType;
            return this;
        }
    }
    
    @Retention(RetentionPolicy.SOURCE)
    public @interface LayoutDirection {
    }
    
    @Retention(RetentionPolicy.SOURCE)
    public @interface ScaleType {
    }
}

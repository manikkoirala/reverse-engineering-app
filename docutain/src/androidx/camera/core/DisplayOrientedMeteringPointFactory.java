// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core;

import android.graphics.PointF;
import androidx.camera.core.impl.CameraInfoInternal;
import android.view.Display;

public final class DisplayOrientedMeteringPointFactory extends MeteringPointFactory
{
    private final CameraInfo mCameraInfo;
    private final Display mDisplay;
    private final float mHeight;
    private final float mWidth;
    
    public DisplayOrientedMeteringPointFactory(final Display mDisplay, final CameraInfo mCameraInfo, final float mWidth, final float mHeight) {
        this.mWidth = mWidth;
        this.mHeight = mHeight;
        this.mDisplay = mDisplay;
        this.mCameraInfo = mCameraInfo;
    }
    
    private Integer getLensFacing() {
        final CameraInfo mCameraInfo = this.mCameraInfo;
        if (mCameraInfo instanceof CameraInfoInternal) {
            return ((CameraInfoInternal)mCameraInfo).getLensFacing();
        }
        return null;
    }
    
    private int getRelativeCameraOrientation(final boolean b) {
        int sensorRotationDegrees;
        try {
            sensorRotationDegrees = this.mCameraInfo.getSensorRotationDegrees(this.mDisplay.getRotation());
            if (b) {
                sensorRotationDegrees = (360 - sensorRotationDegrees) % 360;
            }
        }
        catch (final Exception ex) {
            sensorRotationDegrees = 0;
        }
        return sensorRotationDegrees;
    }
    
    @Override
    protected PointF convertPoint(float n, final float n2) {
        final float mWidth = this.mWidth;
        final float mHeight = this.mHeight;
        final Integer lensFacing = this.getLensFacing();
        final boolean b = lensFacing != null && lensFacing == 0;
        final int relativeCameraOrientation = this.getRelativeCameraOrientation(b);
        float n3 = mWidth;
        float n4 = mHeight;
        float n5 = n;
        float n6 = n2;
        if (relativeCameraOrientation != 90) {
            if (relativeCameraOrientation == 270) {
                n3 = mWidth;
                n4 = mHeight;
                n5 = n;
                n6 = n2;
            }
            else {
                n4 = mWidth;
                n3 = mHeight;
                n6 = n;
                n5 = n2;
            }
        }
        n = n6;
        Label_0155: {
            if (relativeCameraOrientation != 90) {
                if (relativeCameraOrientation != 180) {
                    if (relativeCameraOrientation != 270) {
                        break Label_0155;
                    }
                    n6 = n4 - n6;
                    break Label_0155;
                }
                else {
                    n = n4 - n6;
                }
            }
            n5 = n3 - n5;
            n6 = n;
        }
        n = n6;
        if (b) {
            n = n4 - n6;
        }
        return new PointF(n / n4, n5 / n3);
    }
}

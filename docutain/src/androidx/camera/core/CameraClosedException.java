// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core;

final class CameraClosedException extends RuntimeException
{
    CameraClosedException(final String message) {
        super(message);
    }
    
    CameraClosedException(final String message, final Throwable cause) {
        super(message, cause);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core;

final class AutoValue_CameraState_StateError extends StateError
{
    private final Throwable cause;
    private final int code;
    
    AutoValue_CameraState_StateError(final int code, final Throwable cause) {
        this.code = code;
        this.cause = cause;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o == this) {
            return true;
        }
        if (o instanceof StateError) {
            final StateError stateError = (StateError)o;
            if (this.code == stateError.getCode()) {
                final Throwable cause = this.cause;
                if (cause == null) {
                    if (stateError.getCause() == null) {
                        return b;
                    }
                }
                else if (cause.equals(stateError.getCause())) {
                    return b;
                }
            }
            b = false;
            return b;
        }
        return false;
    }
    
    @Override
    public Throwable getCause() {
        return this.cause;
    }
    
    @Override
    public int getCode() {
        return this.code;
    }
    
    @Override
    public int hashCode() {
        final int code = this.code;
        final Throwable cause = this.cause;
        int hashCode;
        if (cause == null) {
            hashCode = 0;
        }
        else {
            hashCode = cause.hashCode();
        }
        return (code ^ 0xF4243) * 1000003 ^ hashCode;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("StateError{code=");
        sb.append(this.code);
        sb.append(", cause=");
        sb.append(this.cause);
        sb.append("}");
        return sb.toString();
    }
}

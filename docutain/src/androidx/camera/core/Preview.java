// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core;

import androidx.camera.core.impl.ConfigProvider;
import java.util.UUID;
import android.util.Pair;
import java.util.List;
import androidx.camera.core.impl.CaptureConfig;
import androidx.camera.core.impl.OptionsBundle;
import androidx.camera.core.impl.MutableConfig;
import androidx.camera.core.internal.TargetConfig;
import androidx.camera.core.impl.MutableOptionsBundle;
import androidx.camera.core.internal.ThreadConfig;
import androidx.camera.core.impl.ImageOutputConfig;
import androidx.camera.core.impl.ImageInputConfig;
import androidx.camera.core.impl.CameraInfoInternal;
import androidx.camera.core.impl.Config;
import androidx.camera.core.impl.Config$_CC;
import androidx.camera.core.impl.UseCaseConfigFactory;
import androidx.camera.core.internal.CameraCaptureResultImageInfo;
import androidx.camera.core.impl.CameraCaptureResult;
import androidx.camera.core.impl.CameraCaptureCallback;
import androidx.camera.core.impl.ImageInfoProcessor;
import android.os.Handler;
import android.os.HandlerThread;
import androidx.camera.core.impl.CaptureStage;
import androidx.camera.core.impl.CaptureProcessor;
import androidx.camera.core.impl.CameraInternal;
import androidx.camera.core.processing.SurfaceEdge;
import java.util.Collections;
import androidx.camera.core.processing.SettableSurface;
import java.util.Objects;
import android.graphics.Rect;
import android.graphics.Matrix;
import androidx.core.util.Preconditions;
import androidx.camera.core.impl.utils.Threads;
import androidx.camera.core.impl.SessionConfig;
import androidx.camera.core.impl.UseCaseConfig;
import androidx.camera.core.impl.PreviewConfig;
import androidx.camera.core.impl.utils.executor.CameraXExecutors;
import android.util.Size;
import androidx.camera.core.processing.SurfaceProcessorInternal;
import androidx.camera.core.impl.DeferrableSurface;
import androidx.camera.core.processing.SurfaceProcessorNode;
import java.util.concurrent.Executor;

public final class Preview extends UseCase
{
    public static final Defaults DEFAULT_CONFIG;
    private static final Executor DEFAULT_SURFACE_PROVIDER_EXECUTOR;
    private static final String TAG = "Preview";
    SurfaceRequest mCurrentSurfaceRequest;
    private SurfaceProcessorNode mNode;
    private DeferrableSurface mSessionDeferrableSurface;
    private SurfaceProcessorInternal mSurfaceProcessor;
    private SurfaceProvider mSurfaceProvider;
    private Executor mSurfaceProviderExecutor;
    private Size mSurfaceSize;
    
    static {
        DEFAULT_CONFIG = new Defaults();
        DEFAULT_SURFACE_PROVIDER_EXECUTOR = CameraXExecutors.mainThreadExecutor();
    }
    
    Preview(final PreviewConfig previewConfig) {
        super(previewConfig);
        this.mSurfaceProviderExecutor = Preview.DEFAULT_SURFACE_PROVIDER_EXECUTOR;
    }
    
    private void addCameraSurfaceAndErrorListener(final SessionConfig.Builder builder, final String s, final PreviewConfig previewConfig, final Size size) {
        if (this.mSurfaceProvider != null) {
            builder.addSurface(this.mSessionDeferrableSurface);
        }
        builder.addErrorListener(new Preview$$ExternalSyntheticLambda1(this, s, previewConfig, size));
    }
    
    private void clearPipeline() {
        final DeferrableSurface mSessionDeferrableSurface = this.mSessionDeferrableSurface;
        if (mSessionDeferrableSurface != null) {
            mSessionDeferrableSurface.close();
            this.mSessionDeferrableSurface = null;
        }
        final SurfaceProcessorNode mNode = this.mNode;
        if (mNode != null) {
            mNode.release();
            this.mNode = null;
        }
        this.mCurrentSurfaceRequest = null;
    }
    
    private SessionConfig.Builder createPipelineWithNode(final String s, final PreviewConfig previewConfig, final Size size) {
        Threads.checkMainThread();
        Preconditions.checkNotNull(this.mSurfaceProcessor);
        final CameraInternal camera = this.getCamera();
        Preconditions.checkNotNull(camera);
        this.clearPipeline();
        this.mNode = new SurfaceProcessorNode(camera, SurfaceOutput.GlTransformOptions.USE_SURFACE_TEXTURE_TRANSFORM, this.mSurfaceProcessor);
        final SettableSurface settableSurface = new SettableSurface(1, size, 34, new Matrix(), true, Objects.requireNonNull(this.getCropRect(size)), this.getRelativeRotation(camera), false);
        final SettableSurface settableSurface2 = this.mNode.transform(SurfaceEdge.create(Collections.singletonList(settableSurface))).getSurfaces().get(0);
        this.mSessionDeferrableSurface = settableSurface;
        this.mCurrentSurfaceRequest = settableSurface2.createSurfaceRequest(camera);
        if (this.mSurfaceProvider != null) {
            this.sendSurfaceRequest();
        }
        final SessionConfig.Builder from = SessionConfig.Builder.createFrom(previewConfig);
        this.addCameraSurfaceAndErrorListener(from, s, previewConfig, size);
        return from;
    }
    
    private Rect getCropRect(final Size size) {
        if (this.getViewPortCropRect() != null) {
            return this.getViewPortCropRect();
        }
        if (size != null) {
            return new Rect(0, 0, size.getWidth(), size.getHeight());
        }
        return null;
    }
    
    private void sendSurfaceRequest() {
        this.mSurfaceProviderExecutor.execute(new Preview$$ExternalSyntheticLambda0(Preconditions.checkNotNull(this.mSurfaceProvider), Preconditions.checkNotNull(this.mCurrentSurfaceRequest)));
        this.sendTransformationInfoIfReady();
    }
    
    private void sendTransformationInfoIfReady() {
        final CameraInternal camera = this.getCamera();
        final SurfaceProvider mSurfaceProvider = this.mSurfaceProvider;
        final Rect cropRect = this.getCropRect(this.mSurfaceSize);
        final SurfaceRequest mCurrentSurfaceRequest = this.mCurrentSurfaceRequest;
        if (camera != null && mSurfaceProvider != null && cropRect != null && mCurrentSurfaceRequest != null) {
            mCurrentSurfaceRequest.updateTransformationInfo(SurfaceRequest.TransformationInfo.of(cropRect, this.getRelativeRotation(camera), this.getAppTargetRotation()));
        }
    }
    
    private void updateConfigAndOutput(final String s, final PreviewConfig previewConfig, final Size size) {
        this.updateSessionConfig(this.createPipeline(s, previewConfig, size).build());
    }
    
    SessionConfig.Builder createPipeline(final String s, final PreviewConfig previewConfig, final Size size) {
        if (this.mSurfaceProcessor != null) {
            return this.createPipelineWithNode(s, previewConfig, size);
        }
        Threads.checkMainThread();
        final SessionConfig.Builder from = SessionConfig.Builder.createFrom(previewConfig);
        final CaptureProcessor captureProcessor = previewConfig.getCaptureProcessor(null);
        this.clearPipeline();
        final SurfaceRequest mCurrentSurfaceRequest = new SurfaceRequest(size, this.getCamera(), previewConfig.isRgba8888SurfaceRequired(false));
        this.mCurrentSurfaceRequest = mCurrentSurfaceRequest;
        if (this.mSurfaceProvider != null) {
            this.sendSurfaceRequest();
        }
        if (captureProcessor != null) {
            final CaptureStage.DefaultCaptureStage defaultCaptureStage = new CaptureStage.DefaultCaptureStage();
            final HandlerThread handlerThread = new HandlerThread("CameraX-preview_processing");
            handlerThread.start();
            final String string = Integer.toString(defaultCaptureStage.hashCode());
            final ProcessingSurface mSessionDeferrableSurface = new ProcessingSurface(size.getWidth(), size.getHeight(), previewConfig.getInputFormat(), new Handler(handlerThread.getLooper()), defaultCaptureStage, captureProcessor, mCurrentSurfaceRequest.getDeferrableSurface(), string);
            from.addCameraCaptureCallback(mSessionDeferrableSurface.getCameraCaptureCallback());
            mSessionDeferrableSurface.getTerminationFuture().addListener((Runnable)new Preview$$ExternalSyntheticLambda2(handlerThread), CameraXExecutors.directExecutor());
            this.mSessionDeferrableSurface = mSessionDeferrableSurface;
            from.addTag(string, defaultCaptureStage.getId());
        }
        else {
            final ImageInfoProcessor imageInfoProcessor = previewConfig.getImageInfoProcessor(null);
            if (imageInfoProcessor != null) {
                from.addCameraCaptureCallback(new CameraCaptureCallback(this, imageInfoProcessor) {
                    final Preview this$0;
                    final ImageInfoProcessor val$processor;
                    
                    @Override
                    public void onCaptureCompleted(final CameraCaptureResult cameraCaptureResult) {
                        super.onCaptureCompleted(cameraCaptureResult);
                        if (this.val$processor.process(new CameraCaptureResultImageInfo(cameraCaptureResult))) {
                            this.this$0.notifyUpdated();
                        }
                    }
                });
            }
            this.mSessionDeferrableSurface = mCurrentSurfaceRequest.getDeferrableSurface();
        }
        this.addCameraSurfaceAndErrorListener(from, s, previewConfig, size);
        return from;
    }
    
    @Override
    public UseCaseConfig<?> getDefaultConfig(final boolean b, final UseCaseConfigFactory useCaseConfigFactory) {
        Config config2;
        final Config config = config2 = useCaseConfigFactory.getConfig(UseCaseConfigFactory.CaptureType.PREVIEW, 1);
        if (b) {
            config2 = Config$_CC.mergeConfigs(config, Preview.DEFAULT_CONFIG.getConfig());
        }
        Object useCaseConfig;
        if (config2 == null) {
            useCaseConfig = null;
        }
        else {
            useCaseConfig = this.getUseCaseConfigBuilder(config2).getUseCaseConfig();
        }
        return (UseCaseConfig<?>)useCaseConfig;
    }
    
    public SurfaceProcessorInternal getProcessor() {
        return this.mSurfaceProcessor;
    }
    
    @Override
    public ResolutionInfo getResolutionInfo() {
        return super.getResolutionInfo();
    }
    
    public int getTargetRotation() {
        return this.getTargetRotationInternal();
    }
    
    @Override
    public UseCaseConfig.Builder<?, ?, ?> getUseCaseConfigBuilder(final Config config) {
        return Builder.fromConfig(config);
    }
    
    @Override
    public void onDetached() {
        this.clearPipeline();
    }
    
    @Override
    protected UseCaseConfig<?> onMergeConfig(final CameraInfoInternal cameraInfoInternal, final UseCaseConfig.Builder<?, ?, ?> builder) {
        if (builder.getMutableConfig().retrieveOption(PreviewConfig.OPTION_PREVIEW_CAPTURE_PROCESSOR, null) != null) {
            builder.getMutableConfig().insertOption(ImageInputConfig.OPTION_INPUT_FORMAT, 35);
        }
        else {
            builder.getMutableConfig().insertOption(ImageInputConfig.OPTION_INPUT_FORMAT, 34);
        }
        return (UseCaseConfig<?>)builder.getUseCaseConfig();
    }
    
    @Override
    protected Size onSuggestedResolutionUpdated(final Size mSurfaceSize) {
        this.mSurfaceSize = mSurfaceSize;
        this.updateConfigAndOutput(this.getCameraId(), (PreviewConfig)this.getCurrentConfig(), this.mSurfaceSize);
        return mSurfaceSize;
    }
    
    public void setProcessor(final SurfaceProcessorInternal mSurfaceProcessor) {
        this.mSurfaceProcessor = mSurfaceProcessor;
    }
    
    public void setSurfaceProvider(final SurfaceProvider surfaceProvider) {
        this.setSurfaceProvider(Preview.DEFAULT_SURFACE_PROVIDER_EXECUTOR, surfaceProvider);
    }
    
    public void setSurfaceProvider(final Executor mSurfaceProviderExecutor, final SurfaceProvider mSurfaceProvider) {
        Threads.checkMainThread();
        if (mSurfaceProvider == null) {
            this.mSurfaceProvider = null;
            this.notifyInactive();
        }
        else {
            this.mSurfaceProvider = mSurfaceProvider;
            this.mSurfaceProviderExecutor = mSurfaceProviderExecutor;
            this.notifyActive();
            if (this.getAttachedSurfaceResolution() != null) {
                this.updateConfigAndOutput(this.getCameraId(), (PreviewConfig)this.getCurrentConfig(), this.getAttachedSurfaceResolution());
                this.notifyReset();
            }
        }
    }
    
    public void setTargetRotation(final int targetRotationInternal) {
        if (this.setTargetRotationInternal(targetRotationInternal)) {
            this.sendTransformationInfoIfReady();
        }
    }
    
    @Override
    public void setViewPortCropRect(final Rect viewPortCropRect) {
        super.setViewPortCropRect(viewPortCropRect);
        this.sendTransformationInfoIfReady();
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("Preview:");
        sb.append(this.getName());
        return sb.toString();
    }
    
    public static final class Builder implements UseCaseConfig.Builder<Preview, PreviewConfig, Builder>, ImageOutputConfig.Builder<Builder>, ThreadConfig.Builder<Builder>
    {
        private final MutableOptionsBundle mMutableConfig;
        
        public Builder() {
            this(MutableOptionsBundle.create());
        }
        
        private Builder(final MutableOptionsBundle mMutableConfig) {
            this.mMutableConfig = mMutableConfig;
            final Class<?> obj = mMutableConfig.retrieveOption(TargetConfig.OPTION_TARGET_CLASS, null);
            if (obj != null && !obj.equals(Preview.class)) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Invalid target class configuration for ");
                sb.append(this);
                sb.append(": ");
                sb.append(obj);
                throw new IllegalArgumentException(sb.toString());
            }
            this.setTargetClass(Preview.class);
        }
        
        static Builder fromConfig(final Config config) {
            return new Builder(MutableOptionsBundle.from(config));
        }
        
        public static Builder fromConfig(final PreviewConfig previewConfig) {
            return new Builder(MutableOptionsBundle.from(previewConfig));
        }
        
        @Override
        public Preview build() {
            if (this.getMutableConfig().retrieveOption(PreviewConfig.OPTION_TARGET_ASPECT_RATIO, (Object)null) != null && this.getMutableConfig().retrieveOption(PreviewConfig.OPTION_TARGET_RESOLUTION, (Object)null) != null) {
                throw new IllegalArgumentException("Cannot use both setTargetResolution and setTargetAspectRatio on the same config.");
            }
            return new Preview(this.getUseCaseConfig());
        }
        
        @Override
        public MutableConfig getMutableConfig() {
            return this.mMutableConfig;
        }
        
        public PreviewConfig getUseCaseConfig() {
            return new PreviewConfig(OptionsBundle.from(this.mMutableConfig));
        }
        
        public Builder setBackgroundExecutor(final Executor executor) {
            this.getMutableConfig().insertOption(PreviewConfig.OPTION_BACKGROUND_EXECUTOR, executor);
            return this;
        }
        
        public Builder setCameraSelector(final CameraSelector cameraSelector) {
            this.getMutableConfig().insertOption(UseCaseConfig.OPTION_CAMERA_SELECTOR, cameraSelector);
            return this;
        }
        
        public Builder setCaptureOptionUnpacker(final CaptureConfig.OptionUnpacker optionUnpacker) {
            this.getMutableConfig().insertOption(PreviewConfig.OPTION_CAPTURE_CONFIG_UNPACKER, optionUnpacker);
            return this;
        }
        
        public Builder setCaptureProcessor(final CaptureProcessor captureProcessor) {
            this.getMutableConfig().insertOption(PreviewConfig.OPTION_PREVIEW_CAPTURE_PROCESSOR, captureProcessor);
            return this;
        }
        
        public Builder setDefaultCaptureConfig(final CaptureConfig captureConfig) {
            this.getMutableConfig().insertOption(PreviewConfig.OPTION_DEFAULT_CAPTURE_CONFIG, captureConfig);
            return this;
        }
        
        public Builder setDefaultResolution(final Size size) {
            this.getMutableConfig().insertOption(PreviewConfig.OPTION_DEFAULT_RESOLUTION, size);
            return this;
        }
        
        public Builder setDefaultSessionConfig(final SessionConfig sessionConfig) {
            this.getMutableConfig().insertOption(PreviewConfig.OPTION_DEFAULT_SESSION_CONFIG, sessionConfig);
            return this;
        }
        
        public Builder setImageInfoProcessor(final ImageInfoProcessor imageInfoProcessor) {
            this.getMutableConfig().insertOption(PreviewConfig.IMAGE_INFO_PROCESSOR, imageInfoProcessor);
            return this;
        }
        
        public Builder setIsRgba8888SurfaceRequired(final boolean b) {
            this.getMutableConfig().insertOption(PreviewConfig.OPTION_RGBA8888_SURFACE_REQUIRED, b);
            return this;
        }
        
        public Builder setMaxResolution(final Size size) {
            this.getMutableConfig().insertOption(PreviewConfig.OPTION_MAX_RESOLUTION, size);
            return this;
        }
        
        public Builder setSessionOptionUnpacker(final SessionConfig.OptionUnpacker optionUnpacker) {
            this.getMutableConfig().insertOption(PreviewConfig.OPTION_SESSION_CONFIG_UNPACKER, optionUnpacker);
            return this;
        }
        
        public Builder setSupportedResolutions(final List<Pair<Integer, Size[]>> list) {
            this.getMutableConfig().insertOption(PreviewConfig.OPTION_SUPPORTED_RESOLUTIONS, list);
            return this;
        }
        
        public Builder setSurfaceOccupancyPriority(final int i) {
            this.getMutableConfig().insertOption(PreviewConfig.OPTION_SURFACE_OCCUPANCY_PRIORITY, i);
            return this;
        }
        
        public Builder setTargetAspectRatio(final int i) {
            this.getMutableConfig().insertOption(PreviewConfig.OPTION_TARGET_ASPECT_RATIO, i);
            return this;
        }
        
        public Builder setTargetClass(final Class<Preview> clazz) {
            this.getMutableConfig().insertOption(PreviewConfig.OPTION_TARGET_CLASS, clazz);
            if (this.getMutableConfig().retrieveOption(PreviewConfig.OPTION_TARGET_NAME, (Object)null) == null) {
                final StringBuilder sb = new StringBuilder();
                sb.append(clazz.getCanonicalName());
                sb.append("-");
                sb.append(UUID.randomUUID());
                this.setTargetName(sb.toString());
            }
            return this;
        }
        
        public Builder setTargetName(final String s) {
            this.getMutableConfig().insertOption(PreviewConfig.OPTION_TARGET_NAME, s);
            return this;
        }
        
        public Builder setTargetResolution(final Size size) {
            this.getMutableConfig().insertOption(ImageOutputConfig.OPTION_TARGET_RESOLUTION, size);
            return this;
        }
        
        public Builder setTargetRotation(final int n) {
            this.getMutableConfig().insertOption(PreviewConfig.OPTION_TARGET_ROTATION, n);
            this.getMutableConfig().insertOption(ImageOutputConfig.OPTION_APP_TARGET_ROTATION, n);
            return this;
        }
        
        public Builder setUseCaseEventCallback(final EventCallback eventCallback) {
            this.getMutableConfig().insertOption(PreviewConfig.OPTION_USE_CASE_EVENT_CALLBACK, eventCallback);
            return this;
        }
        
        public Builder setZslDisabled(final boolean b) {
            this.getMutableConfig().insertOption(UseCaseConfig.OPTION_ZSL_DISABLED, b);
            return this;
        }
    }
    
    public static final class Defaults implements ConfigProvider<PreviewConfig>
    {
        private static final int DEFAULT_ASPECT_RATIO = 0;
        private static final PreviewConfig DEFAULT_CONFIG;
        private static final int DEFAULT_SURFACE_OCCUPANCY_PRIORITY = 2;
        
        static {
            DEFAULT_CONFIG = new Builder().setSurfaceOccupancyPriority(2).setTargetAspectRatio(0).getUseCaseConfig();
        }
        
        @Override
        public PreviewConfig getConfig() {
            return Defaults.DEFAULT_CONFIG;
        }
    }
    
    public interface SurfaceProvider
    {
        void onSurfaceRequested(final SurfaceRequest p0);
    }
}

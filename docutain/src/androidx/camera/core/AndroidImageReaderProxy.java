// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core;

import androidx.camera.core.impl.utils.MainThreadAsyncHandler;
import java.util.concurrent.Executor;
import android.view.Surface;
import android.os.Handler;
import android.media.ImageReader$OnImageAvailableListener;
import android.media.Image;
import android.media.ImageReader;
import androidx.camera.core.impl.ImageReaderProxy;

class AndroidImageReaderProxy implements ImageReaderProxy
{
    private final ImageReader mImageReader;
    private boolean mIsImageAvailableListenerCleared;
    private final Object mLock;
    
    AndroidImageReaderProxy(final ImageReader mImageReader) {
        this.mLock = new Object();
        this.mIsImageAvailableListenerCleared = true;
        this.mImageReader = mImageReader;
    }
    
    private boolean isImageReaderContextNotInitializedException(final RuntimeException ex) {
        return "ImageReaderContext is not initialized".equals(ex.getMessage());
    }
    
    @Override
    public ImageProxy acquireLatestImage() {
        final Object mLock = this.mLock;
        monitorenter(mLock);
        try {
            try {
                final Image acquireLatestImage = this.mImageReader.acquireLatestImage();
            }
            finally {
                monitorexit(mLock);
                Block_7: {
                    Image acquireLatestImage = null;
                    while (true) {
                        iftrue(Label_0041:)(acquireLatestImage != null);
                        break Block_7;
                        acquireLatestImage = null;
                        continue;
                    }
                    final AndroidImageProxy androidImageProxy;
                    Label_0041: {
                        androidImageProxy = new AndroidImageProxy(acquireLatestImage);
                    }
                    monitorexit(mLock);
                    return androidImageProxy;
                }
                monitorexit(mLock);
                return null;
            }
        }
        catch (final RuntimeException ex) {}
    }
    
    @Override
    public ImageProxy acquireNextImage() {
        final Object mLock = this.mLock;
        monitorenter(mLock);
        try {
            try {
                final Image acquireNextImage = this.mImageReader.acquireNextImage();
            }
            finally {
                monitorexit(mLock);
                final Image acquireNextImage = null;
                iftrue(Label_0041:)(acquireNextImage != null);
                Block_7: {
                    break Block_7;
                    final AndroidImageProxy androidImageProxy;
                    Label_0041: {
                        androidImageProxy = new AndroidImageProxy(acquireNextImage);
                    }
                    monitorexit(mLock);
                    return androidImageProxy;
                }
                monitorexit(mLock);
                return null;
            }
        }
        catch (final RuntimeException ex) {}
    }
    
    @Override
    public void clearOnImageAvailableListener() {
        synchronized (this.mLock) {
            this.mIsImageAvailableListenerCleared = true;
            this.mImageReader.setOnImageAvailableListener((ImageReader$OnImageAvailableListener)null, (Handler)null);
        }
    }
    
    @Override
    public void close() {
        synchronized (this.mLock) {
            this.mImageReader.close();
        }
    }
    
    @Override
    public int getHeight() {
        synchronized (this.mLock) {
            return this.mImageReader.getHeight();
        }
    }
    
    @Override
    public int getImageFormat() {
        synchronized (this.mLock) {
            return this.mImageReader.getImageFormat();
        }
    }
    
    @Override
    public int getMaxImages() {
        synchronized (this.mLock) {
            return this.mImageReader.getMaxImages();
        }
    }
    
    @Override
    public Surface getSurface() {
        synchronized (this.mLock) {
            return this.mImageReader.getSurface();
        }
    }
    
    @Override
    public int getWidth() {
        synchronized (this.mLock) {
            return this.mImageReader.getWidth();
        }
    }
    
    @Override
    public void setOnImageAvailableListener(final OnImageAvailableListener onImageAvailableListener, final Executor executor) {
        synchronized (this.mLock) {
            this.mIsImageAvailableListenerCleared = false;
            this.mImageReader.setOnImageAvailableListener((ImageReader$OnImageAvailableListener)new AndroidImageReaderProxy$$ExternalSyntheticLambda1(this, executor, onImageAvailableListener), MainThreadAsyncHandler.getInstance());
        }
    }
}

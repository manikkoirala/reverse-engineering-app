// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core;

public interface ZoomState
{
    float getLinearZoom();
    
    float getMaxZoomRatio();
    
    float getMinZoomRatio();
    
    float getZoomRatio();
}

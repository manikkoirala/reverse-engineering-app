// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.processing;

import androidx.camera.core.ImageProxy;
import java.util.List;
import androidx.camera.core.ImageProcessor;

public class ImageProcessorRequest implements Request
{
    private final List<ImageProxy> mImageProxies;
    private final int mOutputFormat;
    
    public ImageProcessorRequest(final List<ImageProxy> mImageProxies, final int mOutputFormat) {
        this.mImageProxies = mImageProxies;
        this.mOutputFormat = mOutputFormat;
    }
    
    @Override
    public List<ImageProxy> getInputImages() {
        return this.mImageProxies;
    }
    
    @Override
    public int getOutputFormat() {
        return this.mOutputFormat;
    }
}

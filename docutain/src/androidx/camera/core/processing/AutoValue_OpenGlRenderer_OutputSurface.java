// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.processing;

import android.opengl.EGLSurface;

final class AutoValue_OpenGlRenderer_OutputSurface extends OutputSurface
{
    private final EGLSurface eglSurface;
    private final int height;
    private final int width;
    
    AutoValue_OpenGlRenderer_OutputSurface(final EGLSurface eglSurface, final int width, final int height) {
        if (eglSurface != null) {
            this.eglSurface = eglSurface;
            this.width = width;
            this.height = height;
            return;
        }
        throw new NullPointerException("Null eglSurface");
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o == this) {
            return true;
        }
        if (o instanceof OutputSurface) {
            final OutputSurface outputSurface = (OutputSurface)o;
            if (!this.eglSurface.equals((Object)outputSurface.getEglSurface()) || this.width != outputSurface.getWidth() || this.height != outputSurface.getHeight()) {
                b = false;
            }
            return b;
        }
        return false;
    }
    
    @Override
    EGLSurface getEglSurface() {
        return this.eglSurface;
    }
    
    @Override
    int getHeight() {
        return this.height;
    }
    
    @Override
    int getWidth() {
        return this.width;
    }
    
    @Override
    public int hashCode() {
        return ((this.eglSurface.hashCode() ^ 0xF4243) * 1000003 ^ this.width) * 1000003 ^ this.height;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("OutputSurface{eglSurface=");
        sb.append(this.eglSurface);
        sb.append(", width=");
        sb.append(this.width);
        sb.append(", height=");
        sb.append(this.height);
        sb.append("}");
        return sb.toString();
    }
}

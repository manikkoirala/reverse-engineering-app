// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.processing;

public interface ShaderProvider
{
    public static final ShaderProvider DEFAULT = new ShaderProvider() {};
    
    String createFragmentShader(final String p0, final String p1);
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.processing;

public interface Node<I, O>
{
    void release();
    
    O transform(final I p0);
}

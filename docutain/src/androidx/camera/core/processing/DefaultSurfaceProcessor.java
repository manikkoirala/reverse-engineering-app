// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.processing;

import com.google.common.util.concurrent.ListenableFuture;
import androidx.core.util.Consumer;
import androidx.camera.core.SurfaceRequest;
import android.graphics.SurfaceTexture;
import java.util.concurrent.ExecutionException;
import androidx.concurrent.futures.CallbackToFutureAdapter;
import java.util.Iterator;
import androidx.camera.core.impl.utils.executor.CameraXExecutors;
import java.util.LinkedHashMap;
import android.view.Surface;
import androidx.camera.core.SurfaceOutput;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;
import android.os.HandlerThread;
import android.os.Handler;
import java.util.concurrent.Executor;
import android.graphics.SurfaceTexture$OnFrameAvailableListener;

public class DefaultSurfaceProcessor implements SurfaceProcessorInternal, SurfaceTexture$OnFrameAvailableListener
{
    private final Executor mGlExecutor;
    final Handler mGlHandler;
    private final OpenGlRenderer mGlRenderer;
    final HandlerThread mGlThread;
    private int mInputSurfaceCount;
    private final AtomicBoolean mIsReleased;
    final Map<SurfaceOutput, Surface> mOutputSurfaces;
    private final float[] mSurfaceOutputMatrix;
    private final float[] mTextureMatrix;
    
    public DefaultSurfaceProcessor() {
        this(ShaderProvider.DEFAULT);
    }
    
    public DefaultSurfaceProcessor(final ShaderProvider shaderProvider) {
        this.mIsReleased = new AtomicBoolean(false);
        this.mTextureMatrix = new float[16];
        this.mSurfaceOutputMatrix = new float[16];
        this.mOutputSurfaces = new LinkedHashMap<SurfaceOutput, Surface>();
        this.mInputSurfaceCount = 0;
        final HandlerThread mGlThread = new HandlerThread("GL Thread");
        (this.mGlThread = mGlThread).start();
        final Handler mGlHandler = new Handler(mGlThread.getLooper());
        this.mGlHandler = mGlHandler;
        this.mGlExecutor = CameraXExecutors.newHandlerExecutor(mGlHandler);
        this.mGlRenderer = new OpenGlRenderer();
        try {
            this.initGlRenderer(shaderProvider);
        }
        catch (final RuntimeException ex) {
            this.release();
            throw ex;
        }
    }
    
    private void checkReadyToRelease() {
        if (this.mIsReleased.get() && this.mInputSurfaceCount == 0) {
            final Iterator<SurfaceOutput> iterator = this.mOutputSurfaces.keySet().iterator();
            while (iterator.hasNext()) {
                iterator.next().close();
            }
            this.mOutputSurfaces.clear();
            this.mGlRenderer.release();
            this.mGlThread.quit();
        }
    }
    
    private void initGlRenderer(ShaderProvider future) {
        future = (InterruptedException)CallbackToFutureAdapter.getFuture((CallbackToFutureAdapter.Resolver<Object>)new DefaultSurfaceProcessor$$ExternalSyntheticLambda3(this, (ShaderProvider)future));
        try {
            ((ListenableFuture)future).get();
            return;
        }
        catch (final InterruptedException future) {}
        catch (final ExecutionException ex) {}
        Throwable cause = future;
        if (future instanceof ExecutionException) {
            cause = future.getCause();
        }
        if (cause instanceof RuntimeException) {
            throw (RuntimeException)cause;
        }
        throw new IllegalStateException("Failed to create DefaultSurfaceProcessor", cause);
    }
    
    public void onFrameAvailable(final SurfaceTexture surfaceTexture) {
        if (this.mIsReleased.get()) {
            return;
        }
        surfaceTexture.updateTexImage();
        surfaceTexture.getTransformMatrix(this.mTextureMatrix);
        for (final Map.Entry<K, Surface> entry : this.mOutputSurfaces.entrySet()) {
            final Surface outputSurface = entry.getValue();
            final SurfaceOutput surfaceOutput = (SurfaceOutput)entry.getKey();
            this.mGlRenderer.setOutputSurface(outputSurface);
            surfaceOutput.updateTransformMatrix(this.mSurfaceOutputMatrix, this.mTextureMatrix);
            this.mGlRenderer.render(surfaceTexture.getTimestamp(), this.mSurfaceOutputMatrix);
        }
    }
    
    public void onInputSurface(final SurfaceRequest surfaceRequest) {
        if (this.mIsReleased.get()) {
            surfaceRequest.willNotProvideSurface();
            return;
        }
        this.mGlExecutor.execute(new DefaultSurfaceProcessor$$ExternalSyntheticLambda2(this, surfaceRequest));
    }
    
    public void onOutputSurface(final SurfaceOutput surfaceOutput) {
        if (this.mIsReleased.get()) {
            surfaceOutput.close();
            return;
        }
        this.mGlExecutor.execute(new DefaultSurfaceProcessor$$ExternalSyntheticLambda1(this, surfaceOutput));
    }
    
    @Override
    public void release() {
        if (this.mIsReleased.getAndSet(true)) {
            return;
        }
        this.mGlExecutor.execute(new DefaultSurfaceProcessor$$ExternalSyntheticLambda4(this));
    }
}

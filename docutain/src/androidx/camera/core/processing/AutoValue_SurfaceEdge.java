// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.processing;

import java.util.List;

final class AutoValue_SurfaceEdge extends SurfaceEdge
{
    private final List<SettableSurface> surfaces;
    
    AutoValue_SurfaceEdge(final List<SettableSurface> surfaces) {
        if (surfaces != null) {
            this.surfaces = surfaces;
            return;
        }
        throw new NullPointerException("Null surfaces");
    }
    
    @Override
    public boolean equals(final Object o) {
        return o == this || (o instanceof SurfaceEdge && this.surfaces.equals(((SurfaceEdge)o).getSurfaces()));
    }
    
    @Override
    public List<SettableSurface> getSurfaces() {
        return this.surfaces;
    }
    
    @Override
    public int hashCode() {
        return this.surfaces.hashCode() ^ 0xF4243;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("SurfaceEdge{surfaces=");
        sb.append(this.surfaces);
        sb.append("}");
        return sb.toString();
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.processing;

import androidx.camera.core.impl.utils.TransformUtils;
import androidx.core.util.Preconditions;
import androidx.camera.core.ImageProxy;
import android.util.Size;
import androidx.camera.core.impl.CameraCaptureResult;
import android.graphics.Matrix;
import android.graphics.Rect;
import androidx.camera.core.impl.utils.Exif;
import android.graphics.Bitmap;

public abstract class Packet<T>
{
    public static Packet<Bitmap> of(final Bitmap bitmap, final Exif exif, final Rect rect, final int n, final Matrix matrix, final CameraCaptureResult cameraCaptureResult) {
        return new AutoValue_Packet<Bitmap>(bitmap, exif, 42, new Size(bitmap.getWidth(), bitmap.getHeight()), rect, n, matrix, cameraCaptureResult);
    }
    
    public static Packet<ImageProxy> of(final ImageProxy imageProxy, final Exif exif, final Rect rect, final int n, final Matrix matrix, final CameraCaptureResult cameraCaptureResult) {
        if (imageProxy.getFormat() == 256) {
            Preconditions.checkNotNull(exif, "JPEG image must have Exif.");
        }
        return new AutoValue_Packet<ImageProxy>(imageProxy, exif, imageProxy.getFormat(), new Size(imageProxy.getWidth(), imageProxy.getHeight()), rect, n, matrix, cameraCaptureResult);
    }
    
    public static Packet<byte[]> of(final byte[] array, final Exif exif, final int n, final Size size, final Rect rect, final int n2, final Matrix matrix, final CameraCaptureResult cameraCaptureResult) {
        return new AutoValue_Packet<byte[]>(array, exif, n, size, rect, n2, matrix, cameraCaptureResult);
    }
    
    public abstract CameraCaptureResult getCameraCaptureResult();
    
    public abstract Rect getCropRect();
    
    public abstract T getData();
    
    public abstract Exif getExif();
    
    public abstract int getFormat();
    
    public abstract int getRotationDegrees();
    
    public abstract Matrix getSensorToBufferTransform();
    
    public abstract Size getSize();
    
    public boolean hasCropping() {
        return TransformUtils.hasCropping(this.getCropRect(), this.getSize());
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.processing;

import androidx.camera.core.ImageCaptureException;
import java.util.concurrent.ExecutionException;
import androidx.concurrent.futures.CallbackToFutureAdapter;
import java.util.Objects;
import androidx.core.util.Preconditions;
import androidx.camera.core.CameraEffect;
import androidx.camera.core.ImageProcessor;
import java.util.concurrent.Executor;

public class InternalImageProcessor
{
    private final Executor mExecutor;
    private final ImageProcessor mImageProcessor;
    
    public InternalImageProcessor(final CameraEffect cameraEffect) {
        Preconditions.checkArgument(cameraEffect.getTargets() == 4);
        this.mExecutor = cameraEffect.getProcessorExecutor();
        this.mImageProcessor = Objects.requireNonNull(cameraEffect.getImageProcessor());
    }
    
    public ImageProcessor.Response safeProcess(ImageProcessor.Request ex) throws ImageCaptureException {
        try {
            ex = (InterruptedException)CallbackToFutureAdapter.getFuture((CallbackToFutureAdapter.Resolver<Object>)new InternalImageProcessor$$ExternalSyntheticLambda1(this, (ImageProcessor.Request)ex)).get();
            return (ImageProcessor.Response)ex;
        }
        catch (final InterruptedException ex) {}
        catch (final ExecutionException ex2) {}
        Throwable cause = ex;
        if (ex.getCause() != null) {
            cause = ex.getCause();
        }
        throw new ImageCaptureException(0, "Failed to invoke ImageProcessor.", cause);
    }
}

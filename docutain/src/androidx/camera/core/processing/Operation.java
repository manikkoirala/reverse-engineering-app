// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.processing;

import androidx.camera.core.ImageCaptureException;

public interface Operation<I, O>
{
    O apply(final I p0) throws ImageCaptureException;
}

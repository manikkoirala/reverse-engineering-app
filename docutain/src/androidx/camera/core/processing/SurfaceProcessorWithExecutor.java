// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.processing;

import androidx.camera.core.SurfaceOutput;
import androidx.camera.core.SurfaceRequest;
import androidx.core.util.Preconditions;
import androidx.camera.core.SurfaceProcessor;
import java.util.concurrent.Executor;

public class SurfaceProcessorWithExecutor implements SurfaceProcessorInternal
{
    private final Executor mExecutor;
    private final SurfaceProcessor mSurfaceProcessor;
    
    public SurfaceProcessorWithExecutor(final SurfaceProcessor mSurfaceProcessor, final Executor mExecutor) {
        Preconditions.checkState(mSurfaceProcessor instanceof SurfaceProcessorInternal ^ true, "SurfaceProcessorInternal should always be thread safe. Do not wrap.");
        this.mSurfaceProcessor = mSurfaceProcessor;
        this.mExecutor = mExecutor;
    }
    
    public Executor getExecutor() {
        return this.mExecutor;
    }
    
    public SurfaceProcessor getProcessor() {
        return this.mSurfaceProcessor;
    }
    
    @Override
    public void onInputSurface(final SurfaceRequest surfaceRequest) {
        this.mExecutor.execute(new SurfaceProcessorWithExecutor$$ExternalSyntheticLambda0(this, surfaceRequest));
    }
    
    @Override
    public void onOutputSurface(final SurfaceOutput surfaceOutput) {
        this.mExecutor.execute(new SurfaceProcessorWithExecutor$$ExternalSyntheticLambda1(this, surfaceOutput));
    }
    
    @Override
    public void release() {
    }
}

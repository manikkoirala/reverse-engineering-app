// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.processing;

import java.util.concurrent.RejectedExecutionException;
import androidx.camera.core.Logger;
import java.util.concurrent.atomic.AtomicReference;
import android.graphics.RectF;
import androidx.camera.core.impl.utils.TransformUtils;
import androidx.camera.core.impl.utils.MatrixExt;
import android.opengl.Matrix;
import android.view.Surface;
import android.util.Size;
import android.graphics.Rect;
import java.util.concurrent.Executor;
import androidx.core.util.Consumer;
import androidx.concurrent.futures.CallbackToFutureAdapter;
import com.google.common.util.concurrent.ListenableFuture;
import androidx.camera.core.SurfaceOutput;

final class SurfaceOutputImpl implements SurfaceOutput
{
    private static final String TAG = "SurfaceOutputImpl";
    private final ListenableFuture<Void> mCloseFuture;
    private CallbackToFutureAdapter.Completer<Void> mCloseFutureCompleter;
    private Consumer<Event> mEventListener;
    private Executor mExecutor;
    private final int mFormat;
    private final float[] mGlTransform;
    private final GlTransformOptions mGlTransformOptions;
    private boolean mHasPendingCloseRequest;
    private final Rect mInputCropRect;
    private final Size mInputSize;
    private boolean mIsClosed;
    private final Object mLock;
    private final boolean mMirroring;
    private final int mRotationDegrees;
    private final Size mSize;
    private final Surface mSurface;
    private final int mTargets;
    
    SurfaceOutputImpl(final Surface mSurface, final int mTargets, final int mFormat, final Size mSize, final GlTransformOptions mGlTransformOptions, final Size mInputSize, final Rect rect, final int mRotationDegrees, final boolean mMirroring) {
        this.mLock = new Object();
        this.mGlTransform = new float[16];
        this.mHasPendingCloseRequest = false;
        this.mIsClosed = false;
        this.mSurface = mSurface;
        this.mTargets = mTargets;
        this.mFormat = mFormat;
        this.mSize = mSize;
        this.mGlTransformOptions = mGlTransformOptions;
        this.mInputSize = mInputSize;
        this.mInputCropRect = new Rect(rect);
        this.mMirroring = mMirroring;
        if (mGlTransformOptions == GlTransformOptions.APPLY_CROP_ROTATE_AND_MIRRORING) {
            this.mRotationDegrees = mRotationDegrees;
            this.calculateGlTransform();
        }
        else {
            this.mRotationDegrees = 0;
        }
        this.mCloseFuture = CallbackToFutureAdapter.getFuture((CallbackToFutureAdapter.Resolver<Void>)new SurfaceOutputImpl$$ExternalSyntheticLambda0(this));
    }
    
    private void calculateGlTransform() {
        Matrix.setIdentityM(this.mGlTransform, 0);
        Matrix.translateM(this.mGlTransform, 0, 0.0f, 1.0f, 0.0f);
        Matrix.scaleM(this.mGlTransform, 0, 1.0f, -1.0f, 1.0f);
        MatrixExt.preRotate(this.mGlTransform, (float)this.mRotationDegrees, 0.5f, 0.5f);
        if (this.mMirroring) {
            Matrix.translateM(this.mGlTransform, 0, 1.0f, 0.0f, 0.0f);
            Matrix.scaleM(this.mGlTransform, 0, -1.0f, 1.0f, 1.0f);
        }
        final Size rotateSize = TransformUtils.rotateSize(this.mInputSize, this.mRotationDegrees);
        final android.graphics.Matrix rectToRect = TransformUtils.getRectToRect(TransformUtils.sizeToRectF(this.mInputSize), TransformUtils.sizeToRectF(rotateSize), this.mRotationDegrees, this.mMirroring);
        final RectF rectF = new RectF(this.mInputCropRect);
        rectToRect.mapRect(rectF);
        final float n = rectF.left / rotateSize.getWidth();
        final float n2 = (rotateSize.getHeight() - rectF.height() - rectF.top) / rotateSize.getHeight();
        final float n3 = rectF.width() / rotateSize.getWidth();
        final float n4 = rectF.height() / rotateSize.getHeight();
        Matrix.translateM(this.mGlTransform, 0, n, n2, 0.0f);
        Matrix.scaleM(this.mGlTransform, 0, n3, n4, 1.0f);
    }
    
    @Override
    public void close() {
        synchronized (this.mLock) {
            if (!this.mIsClosed) {
                this.mIsClosed = true;
            }
            monitorexit(this.mLock);
            this.mCloseFutureCompleter.set(null);
        }
    }
    
    public ListenableFuture<Void> getCloseFuture() {
        return this.mCloseFuture;
    }
    
    @Override
    public int getFormat() {
        return this.mFormat;
    }
    
    @Override
    public int getRotationDegrees() {
        return this.mRotationDegrees;
    }
    
    @Override
    public Size getSize() {
        return this.mSize;
    }
    
    @Override
    public Surface getSurface(final Executor mExecutor, final Consumer<Event> mEventListener) {
        synchronized (this.mLock) {
            this.mExecutor = mExecutor;
            this.mEventListener = mEventListener;
            final boolean mHasPendingCloseRequest = this.mHasPendingCloseRequest;
            monitorexit(this.mLock);
            if (mHasPendingCloseRequest) {
                this.requestClose();
            }
            return this.mSurface;
        }
    }
    
    @Override
    public int getTargets() {
        return this.mTargets;
    }
    
    public boolean isClosed() {
        synchronized (this.mLock) {
            return this.mIsClosed;
        }
    }
    
    public void requestClose() {
        final AtomicReference atomicReference = new AtomicReference();
        Object mLock = this.mLock;
        synchronized (mLock) {
            Executor mExecutor = null;
            Label_0066: {
                Label_0064: {
                    if (this.mExecutor != null) {
                        final Consumer<Event> mEventListener = this.mEventListener;
                        if (mEventListener != null) {
                            if (!this.mIsClosed) {
                                atomicReference.set(mEventListener);
                                mExecutor = this.mExecutor;
                                this.mHasPendingCloseRequest = false;
                                break Label_0066;
                            }
                            break Label_0064;
                        }
                    }
                    this.mHasPendingCloseRequest = true;
                }
                mExecutor = null;
            }
            monitorexit(mLock);
            if (mExecutor != null) {
                try {
                    mLock = new SurfaceOutputImpl$$ExternalSyntheticLambda1(this, atomicReference);
                    mExecutor.execute((Runnable)mLock);
                }
                catch (final RejectedExecutionException ex) {
                    Logger.d("SurfaceOutputImpl", "Processor executor closed. Close request not posted.", ex);
                }
            }
        }
    }
    
    @Override
    public void updateTransformMatrix(final float[] array, final float[] array2) {
        final int n = SurfaceOutputImpl$1.$SwitchMap$androidx$camera$core$SurfaceOutput$GlTransformOptions[this.mGlTransformOptions.ordinal()];
        if (n != 1) {
            if (n != 2) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Unknown GlTransformOptions: ");
                sb.append(this.mGlTransformOptions);
                throw new AssertionError((Object)sb.toString());
            }
            System.arraycopy(this.mGlTransform, 0, array, 0, 16);
        }
        else {
            System.arraycopy(array2, 0, array, 0, 16);
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.processing;

import kotlin.jvm.internal.Intrinsics;
import androidx.core.util.Consumer;

public class Edge<T> implements Consumer<T>
{
    private Consumer<T> mListener;
    
    @Override
    public void accept(final T t) {
        Intrinsics.checkNotNull((Object)this.mListener, "Listener is not set.");
        this.mListener.accept(t);
    }
    
    public void setListener(final Consumer<T> mListener) {
        this.mListener = mListener;
    }
}

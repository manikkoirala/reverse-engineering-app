// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.processing;

import java.util.Collections;
import androidx.camera.core.impl.utils.Threads;
import java.util.Iterator;
import java.util.concurrent.Executor;
import androidx.camera.core.impl.utils.futures.Futures;
import androidx.camera.core.impl.utils.executor.CameraXExecutors;
import androidx.core.util.Preconditions;
import androidx.camera.core.impl.utils.futures.FutureCallback;
import androidx.camera.core.SurfaceRequest;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Matrix;
import android.util.Size;
import androidx.camera.core.impl.utils.TransformUtils;
import androidx.camera.core.SurfaceOutput;
import androidx.camera.core.impl.CameraInternal;

public class SurfaceProcessorNode implements Node<SurfaceEdge, SurfaceEdge>
{
    final CameraInternal mCameraInternal;
    private final SurfaceOutput.GlTransformOptions mGlTransformOptions;
    private SurfaceEdge mInputEdge;
    private SurfaceEdge mOutputEdge;
    final SurfaceProcessorInternal mSurfaceProcessor;
    
    public SurfaceProcessorNode(final CameraInternal mCameraInternal, final SurfaceOutput.GlTransformOptions mGlTransformOptions, final SurfaceProcessorInternal mSurfaceProcessor) {
        this.mCameraInternal = mCameraInternal;
        this.mGlTransformOptions = mGlTransformOptions;
        this.mSurfaceProcessor = mSurfaceProcessor;
    }
    
    private SettableSurface createOutputSurface(SettableSurface o) {
        final int n = SurfaceProcessorNode$2.$SwitchMap$androidx$camera$core$SurfaceOutput$GlTransformOptions[this.mGlTransformOptions.ordinal()];
        if (n != 1) {
            if (n != 2) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Unknown GlTransformOptions: ");
                sb.append(this.mGlTransformOptions);
                throw new AssertionError((Object)sb.toString());
            }
            o = new SettableSurface(((SettableSurface)o).getTargets(), ((SettableSurface)o).getSize(), ((SettableSurface)o).getFormat(), ((SettableSurface)o).getSensorToBufferTransform(), false, ((SettableSurface)o).getCropRect(), ((SettableSurface)o).getRotationDegrees(), ((SettableSurface)o).getMirroring());
        }
        else {
            final Size size = ((SettableSurface)o).getSize();
            final Rect cropRect = ((SettableSurface)o).getCropRect();
            final int rotationDegrees = ((SettableSurface)o).getRotationDegrees();
            final boolean mirroring = ((SettableSurface)o).getMirroring();
            Size rectToSize;
            if (TransformUtils.is90or270(rotationDegrees)) {
                rectToSize = new Size(cropRect.height(), cropRect.width());
            }
            else {
                rectToSize = TransformUtils.rectToSize(cropRect);
            }
            final Matrix matrix = new Matrix(((SettableSurface)o).getSensorToBufferTransform());
            matrix.postConcat(TransformUtils.getRectToRect(TransformUtils.sizeToRectF(size), new RectF(cropRect), rotationDegrees, mirroring));
            o = new SettableSurface(((SettableSurface)o).getTargets(), rectToSize, ((SettableSurface)o).getFormat(), matrix, false, TransformUtils.sizeToRect(rectToSize), 0, false);
        }
        return (SettableSurface)o;
    }
    
    private void sendSurfacesToProcessorWhenReady(final SettableSurface settableSurface, final SettableSurface settableSurface2) {
        Futures.addCallback(settableSurface2.createSurfaceOutputFuture(this.mGlTransformOptions, settableSurface.getSize(), settableSurface.getCropRect(), settableSurface.getRotationDegrees(), settableSurface.getMirroring()), new FutureCallback<SurfaceOutput>(this, settableSurface.createSurfaceRequest(this.mCameraInternal), settableSurface, settableSurface2) {
            final SurfaceProcessorNode this$0;
            final SettableSurface val$input;
            final SettableSurface val$output;
            final SurfaceRequest val$surfaceRequest;
            
            @Override
            public void onFailure(final Throwable t) {
                this.val$surfaceRequest.willNotProvideSurface();
            }
            
            @Override
            public void onSuccess(final SurfaceOutput surfaceOutput) {
                Preconditions.checkNotNull(surfaceOutput);
                this.this$0.mSurfaceProcessor.onOutputSurface(surfaceOutput);
                this.this$0.mSurfaceProcessor.onInputSurface(this.val$surfaceRequest);
                this.this$0.setupSurfaceUpdatePipeline(this.val$input, this.val$surfaceRequest, this.val$output, surfaceOutput);
            }
        }, CameraXExecutors.mainThreadExecutor());
    }
    
    @Override
    public void release() {
        this.mSurfaceProcessor.release();
        CameraXExecutors.mainThreadExecutor().execute(new SurfaceProcessorNode$$ExternalSyntheticLambda1(this));
    }
    
    void setupSurfaceUpdatePipeline(final SettableSurface settableSurface, final SurfaceRequest surfaceRequest, final SettableSurface settableSurface2, final SurfaceOutput surfaceOutput) {
        surfaceRequest.setTransformationInfoListener(CameraXExecutors.mainThreadExecutor(), (SurfaceRequest.TransformationInfoListener)new SurfaceProcessorNode$$ExternalSyntheticLambda0(surfaceOutput, settableSurface, settableSurface2));
    }
    
    @Override
    public SurfaceEdge transform(SurfaceEdge create) {
        Threads.checkMainThread();
        final int size = create.getSurfaces().size();
        boolean b = true;
        if (size != 1) {
            b = false;
        }
        Preconditions.checkArgument(b, (Object)"Multiple input stream not supported yet.");
        this.mInputEdge = create;
        final SettableSurface settableSurface = create.getSurfaces().get(0);
        final SettableSurface outputSurface = this.createOutputSurface(settableSurface);
        this.sendSurfacesToProcessorWhenReady(settableSurface, outputSurface);
        create = SurfaceEdge.create(Collections.singletonList(outputSurface));
        return this.mOutputEdge = create;
    }
}

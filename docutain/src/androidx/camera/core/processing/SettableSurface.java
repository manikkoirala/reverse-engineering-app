// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.processing;

import android.util.Range;
import androidx.camera.core.impl.CameraInternal;
import java.util.concurrent.Executor;
import androidx.camera.core.impl.utils.futures.AsyncFunction;
import androidx.camera.core.impl.utils.futures.Futures;
import androidx.core.util.Preconditions;
import androidx.camera.core.impl.utils.Threads;
import androidx.camera.core.SurfaceOutput;
import androidx.camera.core.impl.utils.executor.CameraXExecutors;
import android.util.Size;
import com.google.common.util.concurrent.ListenableFuture;
import android.graphics.Matrix;
import androidx.camera.core.SurfaceRequest;
import android.graphics.Rect;
import android.view.Surface;
import androidx.concurrent.futures.CallbackToFutureAdapter;
import androidx.camera.core.impl.DeferrableSurface;

public class SettableSurface extends DeferrableSurface
{
    CallbackToFutureAdapter.Completer<Surface> mCompleter;
    private SurfaceOutputImpl mConsumerToNotify;
    private final Rect mCropRect;
    private boolean mHasConsumer;
    private final boolean mHasEmbeddedTransform;
    private boolean mHasProvider;
    private final boolean mMirroring;
    private SurfaceRequest mProviderSurfaceRequest;
    private int mRotationDegrees;
    private final Matrix mSensorToBufferTransform;
    private final ListenableFuture<Surface> mSurfaceFuture;
    private final int mTargets;
    
    public SettableSurface(final int mTargets, final Size size, final int n, final Matrix mSensorToBufferTransform, final boolean mHasEmbeddedTransform, final Rect mCropRect, final int mRotationDegrees, final boolean mMirroring) {
        super(size, n);
        this.mHasProvider = false;
        this.mHasConsumer = false;
        this.mTargets = mTargets;
        this.mSensorToBufferTransform = mSensorToBufferTransform;
        this.mHasEmbeddedTransform = mHasEmbeddedTransform;
        this.mCropRect = mCropRect;
        this.mRotationDegrees = mRotationDegrees;
        this.mMirroring = mMirroring;
        this.mSurfaceFuture = CallbackToFutureAdapter.getFuture((CallbackToFutureAdapter.Resolver<Surface>)new SettableSurface$$ExternalSyntheticLambda0(this, size));
    }
    
    private void notifyTransformationInfoUpdate() {
        final SurfaceRequest mProviderSurfaceRequest = this.mProviderSurfaceRequest;
        if (mProviderSurfaceRequest != null) {
            mProviderSurfaceRequest.updateTransformationInfo(SurfaceRequest.TransformationInfo.of(this.mCropRect, this.mRotationDegrees, -1));
        }
    }
    
    @Override
    public final void close() {
        super.close();
        CameraXExecutors.mainThreadExecutor().execute(new SettableSurface$$ExternalSyntheticLambda2(this));
    }
    
    public ListenableFuture<SurfaceOutput> createSurfaceOutputFuture(final SurfaceOutput.GlTransformOptions glTransformOptions, final Size size, final Rect rect, final int n, final boolean b) {
        Threads.checkMainThread();
        Preconditions.checkState(this.mHasConsumer ^ true, "Consumer can only be linked once.");
        this.mHasConsumer = true;
        return Futures.transformAsync(this.getSurface(), (AsyncFunction<? super Surface, ? extends SurfaceOutput>)new SettableSurface$$ExternalSyntheticLambda4(this, glTransformOptions, size, rect, n, b), (Executor)CameraXExecutors.mainThreadExecutor());
    }
    
    public SurfaceRequest createSurfaceRequest(final CameraInternal cameraInternal) {
        return this.createSurfaceRequest(cameraInternal, null);
    }
    
    public SurfaceRequest createSurfaceRequest(final CameraInternal cameraInternal, final Range<Integer> range) {
        Threads.checkMainThread();
        final SurfaceRequest mProviderSurfaceRequest = new SurfaceRequest(this.getSize(), cameraInternal, true, range);
        try {
            this.setProvider(mProviderSurfaceRequest.getDeferrableSurface());
            this.mProviderSurfaceRequest = mProviderSurfaceRequest;
            this.notifyTransformationInfoUpdate();
            return mProviderSurfaceRequest;
        }
        catch (final SurfaceClosedException cause) {
            throw new AssertionError("Surface is somehow already closed", cause);
        }
    }
    
    public Rect getCropRect() {
        return this.mCropRect;
    }
    
    public int getFormat() {
        return this.getPrescribedStreamFormat();
    }
    
    public boolean getMirroring() {
        return this.mMirroring;
    }
    
    public int getRotationDegrees() {
        return this.mRotationDegrees;
    }
    
    public Matrix getSensorToBufferTransform() {
        return this.mSensorToBufferTransform;
    }
    
    public Size getSize() {
        return this.getPrescribedSize();
    }
    
    public int getTargets() {
        return this.mTargets;
    }
    
    public boolean hasEmbeddedTransform() {
        return this.mHasEmbeddedTransform;
    }
    
    @Override
    protected ListenableFuture<Surface> provideSurface() {
        return this.mSurfaceFuture;
    }
    
    public void setProvider(final DeferrableSurface deferrableSurface) throws SurfaceClosedException {
        Threads.checkMainThread();
        this.setProvider(deferrableSurface.getSurface());
        deferrableSurface.incrementUseCount();
        this.getTerminationFuture().addListener((Runnable)new SettableSurface$$ExternalSyntheticLambda3(deferrableSurface), CameraXExecutors.directExecutor());
    }
    
    public void setProvider(final ListenableFuture<Surface> listenableFuture) {
        Threads.checkMainThread();
        Preconditions.checkState(this.mHasProvider ^ true, "Provider can only be linked once.");
        this.mHasProvider = true;
        Futures.propagate(listenableFuture, this.mCompleter);
    }
    
    public void setRotationDegrees(final int mRotationDegrees) {
        Threads.checkMainThread();
        if (this.mRotationDegrees == mRotationDegrees) {
            return;
        }
        this.mRotationDegrees = mRotationDegrees;
        this.notifyTransformationInfoUpdate();
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.processing;

import java.util.List;

public abstract class SurfaceEdge
{
    public static SurfaceEdge create(final List<SettableSurface> list) {
        return new AutoValue_SurfaceEdge(list);
    }
    
    public abstract List<SettableSurface> getSurfaces();
}

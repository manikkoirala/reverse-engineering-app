// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core;

public final class CameraInfoUnavailableException extends Exception
{
    public CameraInfoUnavailableException(final String message) {
        super(message);
    }
    
    public CameraInfoUnavailableException(final String message, final Throwable cause) {
        super(message, cause);
    }
}

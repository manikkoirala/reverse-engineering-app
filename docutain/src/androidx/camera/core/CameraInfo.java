// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core;

import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;
import androidx.lifecycle.LiveData;

public interface CameraInfo
{
    public static final String IMPLEMENTATION_TYPE_CAMERA2 = "androidx.camera.camera2";
    public static final String IMPLEMENTATION_TYPE_CAMERA2_LEGACY = "androidx.camera.camera2.legacy";
    public static final String IMPLEMENTATION_TYPE_FAKE = "androidx.camera.fake";
    public static final String IMPLEMENTATION_TYPE_UNKNOWN = "<unknown>";
    
    CameraSelector getCameraSelector();
    
    LiveData<CameraState> getCameraState();
    
    ExposureState getExposureState();
    
    String getImplementationType();
    
    int getSensorRotationDegrees();
    
    int getSensorRotationDegrees(final int p0);
    
    LiveData<Integer> getTorchState();
    
    LiveData<ZoomState> getZoomState();
    
    boolean hasFlashUnit();
    
    boolean isFocusMeteringSupported(final FocusMeteringAction p0);
    
    boolean isPrivateReprocessingSupported();
    
    boolean isZslSupported();
    
    @Retention(RetentionPolicy.SOURCE)
    public @interface ImplementationType {
    }
}

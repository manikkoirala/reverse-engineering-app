// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core;

import java.nio.ByteBuffer;
import android.graphics.Rect;
import android.media.Image$Plane;
import android.graphics.Matrix;
import androidx.camera.core.impl.TagBundle;
import android.media.Image;

final class AndroidImageProxy implements ImageProxy
{
    private final Image mImage;
    private final ImageInfo mImageInfo;
    private final PlaneProxy[] mPlanes;
    
    AndroidImageProxy(final Image mImage) {
        this.mImage = mImage;
        final Image$Plane[] planes = mImage.getPlanes();
        if (planes != null) {
            this.mPlanes = new PlaneProxy[planes.length];
            for (int i = 0; i < planes.length; ++i) {
                this.mPlanes[i] = new PlaneProxy(planes[i]);
            }
        }
        else {
            this.mPlanes = new PlaneProxy[0];
        }
        this.mImageInfo = ImmutableImageInfo.create(TagBundle.emptyBundle(), mImage.getTimestamp(), 0, new Matrix());
    }
    
    @Override
    public void close() {
        this.mImage.close();
    }
    
    @Override
    public Rect getCropRect() {
        return this.mImage.getCropRect();
    }
    
    @Override
    public int getFormat() {
        return this.mImage.getFormat();
    }
    
    @Override
    public int getHeight() {
        return this.mImage.getHeight();
    }
    
    @Override
    public Image getImage() {
        return this.mImage;
    }
    
    @Override
    public ImageInfo getImageInfo() {
        return this.mImageInfo;
    }
    
    @Override
    public ImageProxy.PlaneProxy[] getPlanes() {
        return this.mPlanes;
    }
    
    @Override
    public int getWidth() {
        return this.mImage.getWidth();
    }
    
    @Override
    public void setCropRect(final Rect cropRect) {
        this.mImage.setCropRect(cropRect);
    }
    
    private static final class PlaneProxy implements ImageProxy.PlaneProxy
    {
        private final Image$Plane mPlane;
        
        PlaneProxy(final Image$Plane mPlane) {
            this.mPlane = mPlane;
        }
        
        @Override
        public ByteBuffer getBuffer() {
            return this.mPlane.getBuffer();
        }
        
        @Override
        public int getPixelStride() {
            return this.mPlane.getPixelStride();
        }
        
        @Override
        public int getRowStride() {
            return this.mPlane.getRowStride();
        }
    }
}

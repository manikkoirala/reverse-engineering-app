// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core;

import android.util.Rational;

public class MeteringPoint
{
    private float mNormalizedX;
    private float mNormalizedY;
    private float mSize;
    private Rational mSurfaceAspectRatio;
    
    MeteringPoint(final float mNormalizedX, final float mNormalizedY, final float mSize, final Rational mSurfaceAspectRatio) {
        this.mNormalizedX = mNormalizedX;
        this.mNormalizedY = mNormalizedY;
        this.mSize = mSize;
        this.mSurfaceAspectRatio = mSurfaceAspectRatio;
    }
    
    public float getSize() {
        return this.mSize;
    }
    
    public Rational getSurfaceAspectRatio() {
        return this.mSurfaceAspectRatio;
    }
    
    public float getX() {
        return this.mNormalizedX;
    }
    
    public float getY() {
        return this.mNormalizedY;
    }
}

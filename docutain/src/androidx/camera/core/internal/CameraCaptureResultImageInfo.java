// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.internal;

import androidx.camera.core.impl.utils.ExifData;
import androidx.camera.core.impl.TagBundle;
import android.graphics.Matrix;
import androidx.camera.core.impl.CameraCaptureResult;
import androidx.camera.core.ImageInfo;

public final class CameraCaptureResultImageInfo implements ImageInfo
{
    private final CameraCaptureResult mCameraCaptureResult;
    
    public CameraCaptureResultImageInfo(final CameraCaptureResult mCameraCaptureResult) {
        this.mCameraCaptureResult = mCameraCaptureResult;
    }
    
    public CameraCaptureResult getCameraCaptureResult() {
        return this.mCameraCaptureResult;
    }
    
    @Override
    public int getRotationDegrees() {
        return 0;
    }
    
    @Override
    public Matrix getSensorToBufferTransformMatrix() {
        return new Matrix();
    }
    
    @Override
    public TagBundle getTagBundle() {
        return this.mCameraCaptureResult.getTagBundle();
    }
    
    @Override
    public long getTimestamp() {
        return this.mCameraCaptureResult.getTimestamp();
    }
    
    @Override
    public void populateExifData(final ExifData.Builder builder) {
        this.mCameraCaptureResult.populateExifData(builder);
    }
}

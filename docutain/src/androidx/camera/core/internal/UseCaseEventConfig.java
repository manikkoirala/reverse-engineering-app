// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.internal;

import androidx.camera.core.UseCase;
import androidx.camera.core.impl.Config;
import androidx.camera.core.impl.ReadableConfig;

public interface UseCaseEventConfig extends ReadableConfig
{
    public static final Option<UseCase.EventCallback> OPTION_USE_CASE_EVENT_CALLBACK = Config.Option.create("camerax.core.useCaseEventCallback", UseCase.EventCallback.class);
    
    UseCase.EventCallback getUseCaseEventCallback();
    
    UseCase.EventCallback getUseCaseEventCallback(final UseCase.EventCallback p0);
    
    public interface Builder<B>
    {
        B setUseCaseEventCallback(final UseCase.EventCallback p0);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.internal;

import java.util.concurrent.Executor;
import androidx.camera.core.impl.Config;
import androidx.camera.core.impl.ReadableConfig;

public interface IoConfig extends ReadableConfig
{
    public static final Option<Executor> OPTION_IO_EXECUTOR = Config.Option.create("camerax.core.io.ioExecutor", Executor.class);
    
    Executor getIoExecutor();
    
    Executor getIoExecutor(final Executor p0);
    
    public interface Builder<B>
    {
        B setIoExecutor(final Executor p0);
    }
}

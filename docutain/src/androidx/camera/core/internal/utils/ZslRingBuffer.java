// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.internal.utils;

import androidx.camera.core.impl.CameraCaptureResult;
import androidx.camera.core.impl.CameraCaptureMetaData;
import androidx.camera.core.impl.CameraCaptureResults;
import androidx.camera.core.ImageInfo;
import androidx.camera.core.ImageProxy;

public final class ZslRingBuffer extends ArrayRingBuffer<ImageProxy>
{
    public ZslRingBuffer(final int n, final OnRemoveCallback<ImageProxy> onRemoveCallback) {
        super(n, onRemoveCallback);
    }
    
    private boolean isValidZslFrame(final ImageInfo imageInfo) {
        final CameraCaptureResult retrieveCameraCaptureResult = CameraCaptureResults.retrieveCameraCaptureResult(imageInfo);
        return (retrieveCameraCaptureResult.getAfState() == CameraCaptureMetaData.AfState.LOCKED_FOCUSED || retrieveCameraCaptureResult.getAfState() == CameraCaptureMetaData.AfState.PASSIVE_FOCUSED) && retrieveCameraCaptureResult.getAeState() == CameraCaptureMetaData.AeState.CONVERGED && retrieveCameraCaptureResult.getAwbState() == CameraCaptureMetaData.AwbState.CONVERGED;
    }
    
    @Override
    public void enqueue(final ImageProxy imageProxy) {
        if (this.isValidZslFrame(imageProxy.getImageInfo())) {
            super.enqueue(imageProxy);
        }
        else {
            this.mOnRemoveCallback.onRemove((T)imageProxy);
        }
    }
}

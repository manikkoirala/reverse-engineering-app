// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.internal.utils;

import androidx.camera.core.Logger;
import androidx.core.util.Preconditions;
import android.database.Cursor;
import android.net.Uri;
import android.content.ContentResolver;

public final class VideoUtil
{
    private static final String TAG = "VideoUtil";
    
    private VideoUtil() {
    }
    
    public static String getAbsolutePathFromUri(ContentResolver contentResolver, final Uri uri) {
        Object string = null;
        Object query = null;
        try {
            try {
                contentResolver = (ContentResolver)(string = (query = contentResolver.query(uri, new String[] { "_data" }, (String)null, (String[])null, (String)null)));
                contentResolver = (ContentResolver)Preconditions.checkNotNull((Cursor)contentResolver);
                try {
                    final int columnIndexOrThrow = ((Cursor)contentResolver).getColumnIndexOrThrow("_data");
                    ((Cursor)contentResolver).moveToFirst();
                    string = ((Cursor)contentResolver).getString(columnIndexOrThrow);
                    if (contentResolver != null) {
                        ((Cursor)contentResolver).close();
                    }
                    return (String)string;
                }
                catch (final RuntimeException string) {
                    query = contentResolver;
                }
                finally {
                    string = contentResolver;
                }
            }
            catch (final RuntimeException ex) {}
            string = query;
            Logger.e("VideoUtil", String.format("Failed in getting absolute path for Uri %s with Exception %s", uri.toString(), ((Throwable)contentResolver).toString()));
            if (query != null) {
                ((Cursor)query).close();
            }
            return "";
        }
        finally {}
        if (string != null) {
            ((Cursor)string).close();
        }
    }
}

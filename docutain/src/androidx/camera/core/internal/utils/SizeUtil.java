// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.internal.utils;

import android.util.Size;

public final class SizeUtil
{
    public static final Size RESOLUTION_1080P;
    public static final Size RESOLUTION_480P;
    public static final Size RESOLUTION_VGA;
    public static final Size RESOLUTION_ZERO;
    
    static {
        RESOLUTION_ZERO = new Size(0, 0);
        RESOLUTION_VGA = new Size(640, 480);
        RESOLUTION_480P = new Size(720, 480);
        RESOLUTION_1080P = new Size(1920, 1080);
    }
    
    private SizeUtil() {
    }
    
    public static int getArea(final Size size) {
        return size.getWidth() * size.getHeight();
    }
}

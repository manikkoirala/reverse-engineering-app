// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.internal.utils;

import android.util.Size;
import androidx.camera.core.impl.utils.CameraOrientationUtil;
import androidx.camera.core.impl.ImageOutputConfig;
import androidx.camera.core.impl.UseCaseConfig;

public final class UseCaseConfigUtil
{
    private UseCaseConfigUtil() {
    }
    
    public static void updateTargetRotationAndRelatedConfigs(final UseCaseConfig.Builder<?, ?, ?> builder, final int targetRotation) {
        final ImageOutputConfig imageOutputConfig = (ImageOutputConfig)builder.getUseCaseConfig();
        final int targetRotation2 = imageOutputConfig.getTargetRotation(-1);
        if (targetRotation2 == -1 || targetRotation2 != targetRotation) {
            ((ImageOutputConfig.Builder)builder).setTargetRotation(targetRotation);
        }
        if (targetRotation2 != -1 && targetRotation != -1) {
            if (targetRotation2 != targetRotation) {
                if (Math.abs(CameraOrientationUtil.surfaceRotationToDegrees(targetRotation) - CameraOrientationUtil.surfaceRotationToDegrees(targetRotation2)) % 180 == 90) {
                    final Size targetResolution = imageOutputConfig.getTargetResolution(null);
                    if (targetResolution != null) {
                        ((ImageOutputConfig.Builder)builder).setTargetResolution(new Size(targetResolution.getHeight(), targetResolution.getWidth()));
                    }
                }
            }
        }
    }
}

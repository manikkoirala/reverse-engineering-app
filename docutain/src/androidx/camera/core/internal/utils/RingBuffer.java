// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.internal.utils;

public interface RingBuffer<T>
{
    T dequeue();
    
    void enqueue(final T p0);
    
    int getMaxCapacity();
    
    boolean isEmpty();
    
    public interface OnRemoveCallback<T>
    {
        void onRemove(final T p0);
    }
}

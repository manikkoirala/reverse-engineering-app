// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.internal.compat;

import java.lang.reflect.InvocationTargetException;
import androidx.core.util.Preconditions;
import android.os.Build$VERSION;
import android.util.Log;
import android.view.Surface;
import android.media.ImageWriter;
import java.lang.reflect.Method;

final class ImageWriterCompatApi26Impl
{
    private static final String TAG = "ImageWriterCompatApi26";
    private static Method sNewInstanceMethod;
    
    static {
        try {
            ImageWriterCompatApi26Impl.sNewInstanceMethod = ImageWriter.class.getMethod("newInstance", Surface.class, Integer.TYPE, Integer.TYPE);
        }
        catch (final NoSuchMethodException ex) {
            Log.i("ImageWriterCompatApi26", "Unable to initialize via reflection.", (Throwable)ex);
        }
    }
    
    private ImageWriterCompatApi26Impl() {
    }
    
    static ImageWriter newInstance(Surface ex, final int i, final int j) {
        final int sdk_INT = Build$VERSION.SDK_INT;
        Throwable cause = null;
        if (sdk_INT >= 26) {
            try {
                ex = (InvocationTargetException)Preconditions.checkNotNull(ImageWriterCompatApi26Impl.sNewInstanceMethod.invoke(null, ex, i, j));
                return (ImageWriter)ex;
            }
            catch (final InvocationTargetException ex) {}
            catch (final IllegalAccessException ex2) {}
            cause = ex;
        }
        throw new RuntimeException("Unable to invoke newInstance(Surface, int, int) via reflection.", cause);
    }
}

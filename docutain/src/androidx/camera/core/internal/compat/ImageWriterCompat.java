// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.internal.compat;

import android.view.Surface;
import android.media.Image;
import android.os.Build$VERSION;
import android.media.ImageWriter;

public final class ImageWriterCompat
{
    private ImageWriterCompat() {
    }
    
    public static void close(final ImageWriter imageWriter) {
        if (Build$VERSION.SDK_INT >= 23) {
            ImageWriterCompatApi23Impl.close(imageWriter);
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Unable to call close() on API ");
        sb.append(Build$VERSION.SDK_INT);
        sb.append(". Version 23 or higher required.");
        throw new RuntimeException(sb.toString());
    }
    
    public static Image dequeueInputImage(final ImageWriter imageWriter) {
        if (Build$VERSION.SDK_INT >= 23) {
            return ImageWriterCompatApi23Impl.dequeueInputImage(imageWriter);
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Unable to call dequeueInputImage() on API ");
        sb.append(Build$VERSION.SDK_INT);
        sb.append(". Version 23 or higher required.");
        throw new RuntimeException(sb.toString());
    }
    
    public static ImageWriter newInstance(final Surface surface, final int n) {
        if (Build$VERSION.SDK_INT >= 23) {
            return ImageWriterCompatApi23Impl.newInstance(surface, n);
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Unable to call newInstance(Surface, int) on API ");
        sb.append(Build$VERSION.SDK_INT);
        sb.append(". Version 23 or higher required.");
        throw new RuntimeException(sb.toString());
    }
    
    public static ImageWriter newInstance(final Surface surface, final int n, final int n2) {
        if (Build$VERSION.SDK_INT >= 29) {
            return ImageWriterCompatApi29Impl.newInstance(surface, n, n2);
        }
        if (Build$VERSION.SDK_INT >= 26) {
            return ImageWriterCompatApi26Impl.newInstance(surface, n, n2);
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Unable to call newInstance(Surface, int, int) on API ");
        sb.append(Build$VERSION.SDK_INT);
        sb.append(". Version 26 or higher required.");
        throw new RuntimeException(sb.toString());
    }
    
    public static void queueInputImage(final ImageWriter imageWriter, final Image image) {
        if (Build$VERSION.SDK_INT >= 23) {
            ImageWriterCompatApi23Impl.queueInputImage(imageWriter, image);
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Unable to call queueInputImage() on API ");
        sb.append(Build$VERSION.SDK_INT);
        sb.append(". Version 23 or higher required.");
        throw new RuntimeException(sb.toString());
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.internal.compat;

import android.media.ImageWriter;
import android.view.Surface;

final class ImageWriterCompatApi29Impl
{
    private ImageWriterCompatApi29Impl() {
    }
    
    static ImageWriter newInstance(final Surface surface, final int n, final int n2) {
        return ImageWriter.newInstance(surface, n, n2);
    }
}

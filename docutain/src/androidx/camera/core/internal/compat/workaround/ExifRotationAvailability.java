// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.internal.compat.workaround;

import androidx.camera.core.ImageProxy;
import androidx.camera.core.impl.Config;
import androidx.camera.core.impl.CaptureConfig;
import androidx.camera.core.internal.compat.quirk.DeviceQuirks;
import androidx.camera.core.internal.compat.quirk.ImageCaptureRotationOptionQuirk;

public class ExifRotationAvailability
{
    public boolean isRotationOptionSupported() {
        final ImageCaptureRotationOptionQuirk imageCaptureRotationOptionQuirk = DeviceQuirks.get(ImageCaptureRotationOptionQuirk.class);
        return imageCaptureRotationOptionQuirk == null || imageCaptureRotationOptionQuirk.isSupported(CaptureConfig.OPTION_ROTATION);
    }
    
    public boolean shouldUseExifOrientation(final ImageProxy imageProxy) {
        return this.isRotationOptionSupported() && imageProxy.getFormat() == 256;
    }
}

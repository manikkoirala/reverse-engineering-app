// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.internal.compat;

import android.view.Surface;
import android.media.Image;
import android.media.ImageWriter;

final class ImageWriterCompatApi23Impl
{
    private ImageWriterCompatApi23Impl() {
    }
    
    static void close(final ImageWriter imageWriter) {
        imageWriter.close();
    }
    
    static Image dequeueInputImage(final ImageWriter imageWriter) {
        return imageWriter.dequeueInputImage();
    }
    
    static ImageWriter newInstance(final Surface surface, final int n) {
        return ImageWriter.newInstance(surface, n);
    }
    
    static void queueInputImage(final ImageWriter imageWriter, final Image image) {
        imageWriter.queueInputImage(image);
    }
}

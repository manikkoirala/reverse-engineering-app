// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.internal;

import androidx.camera.core.impl.Config;
import androidx.camera.core.impl.ReadableConfig;

public interface TargetConfig<T> extends ReadableConfig
{
    public static final Option<Class<?>> OPTION_TARGET_CLASS = Config.Option.create("camerax.core.target.class", Class.class);
    public static final Option<String> OPTION_TARGET_NAME = Config.Option.create("camerax.core.target.name", String.class);
    
    Class<T> getTargetClass();
    
    Class<T> getTargetClass(final Class<T> p0);
    
    String getTargetName();
    
    String getTargetName(final String p0);
    
    public interface Builder<T, B>
    {
        B setTargetClass(final Class<T> p0);
        
        B setTargetName(final String p0);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.internal;

import androidx.camera.core.impl.ImageProxyBundle;
import android.util.Size;
import androidx.camera.core.internal.compat.ImageWriterCompat;
import androidx.core.util.Preconditions;
import android.view.Surface;
import androidx.camera.core.impl.utils.futures.Futures;
import androidx.camera.core.Logger;
import android.media.ImageWriter;
import com.google.common.util.concurrent.ListenableFuture;
import androidx.concurrent.futures.CallbackToFutureAdapter;
import android.graphics.Rect;
import androidx.camera.core.impl.CaptureProcessor;

public class YuvToJpegProcessor implements CaptureProcessor
{
    private static final String TAG = "YuvToJpegProcessor";
    private static final Rect UNINITIALIZED_RECT;
    CallbackToFutureAdapter.Completer<Void> mCloseCompleter;
    private ListenableFuture<Void> mCloseFuture;
    private boolean mClosed;
    private Rect mImageRect;
    private ImageWriter mImageWriter;
    private final Object mLock;
    private final int mMaxImages;
    private int mProcessingImages;
    private int mQuality;
    private int mRotationDegrees;
    
    static {
        UNINITIALIZED_RECT = new Rect(0, 0, 0, 0);
    }
    
    public YuvToJpegProcessor(final int mQuality, final int mMaxImages) {
        this.mLock = new Object();
        this.mRotationDegrees = 0;
        this.mClosed = false;
        this.mProcessingImages = 0;
        this.mImageRect = YuvToJpegProcessor.UNINITIALIZED_RECT;
        this.mQuality = mQuality;
        this.mMaxImages = mMaxImages;
    }
    
    @Override
    public void close() {
        synchronized (this.mLock) {
            if (this.mClosed) {
                return;
            }
            this.mClosed = true;
            Object mCloseCompleter;
            if (this.mProcessingImages == 0 && this.mImageWriter != null) {
                Logger.d("YuvToJpegProcessor", "No processing in progress. Closing immediately.");
                this.mImageWriter.close();
                mCloseCompleter = this.mCloseCompleter;
            }
            else {
                Logger.d("YuvToJpegProcessor", "close() called while processing. Will close after completion.");
                mCloseCompleter = null;
            }
            monitorexit(this.mLock);
            if (mCloseCompleter != null) {
                ((CallbackToFutureAdapter.Completer<Object>)mCloseCompleter).set(null);
            }
        }
    }
    
    @Override
    public ListenableFuture<Void> getCloseFuture() {
        synchronized (this.mLock) {
            com.google.common.util.concurrent.ListenableFuture<Void> listenableFuture;
            if (this.mClosed && this.mProcessingImages == 0) {
                listenableFuture = Futures.immediateFuture((Void)null);
            }
            else {
                if (this.mCloseFuture == null) {
                    this.mCloseFuture = CallbackToFutureAdapter.getFuture((CallbackToFutureAdapter.Resolver<Void>)new YuvToJpegProcessor$$ExternalSyntheticLambda0(this));
                }
                listenableFuture = Futures.nonCancellationPropagating(this.mCloseFuture);
            }
            return listenableFuture;
        }
    }
    
    @Override
    public void onOutputSurface(final Surface surface, final int n) {
        Preconditions.checkState(n == 256, "YuvToJpegProcessor only supports JPEG output format.");
        synchronized (this.mLock) {
            if (!this.mClosed) {
                if (this.mImageWriter != null) {
                    throw new IllegalStateException("Output surface already set.");
                }
                this.mImageWriter = ImageWriterCompat.newInstance(surface, this.mMaxImages, n);
            }
            else {
                Logger.w("YuvToJpegProcessor", "Cannot set output surface. Processor is closed.");
            }
        }
    }
    
    @Override
    public void onResolutionUpdate(final Size size) {
        synchronized (this.mLock) {
            this.mImageRect = new Rect(0, 0, size.getWidth(), size.getHeight());
        }
    }
    
    @Override
    public void process(final ImageProxyBundle p0) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: invokeinterface androidx/camera/core/impl/ImageProxyBundle.getCaptureIds:()Ljava/util/List;
        //     6: astore          10
        //     8: aload           10
        //    10: invokeinterface java/util/List.size:()I
        //    15: istore_2       
        //    16: iconst_0       
        //    17: istore          6
        //    19: iconst_0       
        //    20: istore          5
        //    22: iconst_0       
        //    23: istore          4
        //    25: iconst_0       
        //    26: istore          7
        //    28: iload_2        
        //    29: iconst_1       
        //    30: if_icmpne       39
        //    33: iconst_1       
        //    34: istore          9
        //    36: goto            42
        //    39: iconst_0       
        //    40: istore          9
        //    42: new             Ljava/lang/StringBuilder;
        //    45: dup            
        //    46: invokespecial   java/lang/StringBuilder.<init>:()V
        //    49: astore          11
        //    51: aload           11
        //    53: ldc             "Processing image bundle have single capture id, but found "
        //    55: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    58: pop            
        //    59: aload           11
        //    61: aload           10
        //    63: invokeinterface java/util/List.size:()I
        //    68: invokevirtual   java/lang/StringBuilder.append:(I)Ljava/lang/StringBuilder;
        //    71: pop            
        //    72: iload           9
        //    74: aload           11
        //    76: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //    79: invokestatic    androidx/core/util/Preconditions.checkArgument:(ZLjava/lang/Object;)V
        //    82: aload_1        
        //    83: aload           10
        //    85: iconst_0       
        //    86: invokeinterface java/util/List.get:(I)Ljava/lang/Object;
        //    91: checkcast       Ljava/lang/Integer;
        //    94: invokevirtual   java/lang/Integer.intValue:()I
        //    97: invokeinterface androidx/camera/core/impl/ImageProxyBundle.getImageProxy:(I)Lcom/google/common/util/concurrent/ListenableFuture;
        //   102: astore_1       
        //   103: aload_1        
        //   104: invokeinterface com/google/common/util/concurrent/ListenableFuture.isDone:()Z
        //   109: invokestatic    androidx/core/util/Preconditions.checkArgument:(Z)V
        //   112: aload_0        
        //   113: getfield        androidx/camera/core/internal/YuvToJpegProcessor.mLock:Ljava/lang/Object;
        //   116: astore          11
        //   118: aload           11
        //   120: monitorenter   
        //   121: aload_0        
        //   122: getfield        androidx/camera/core/internal/YuvToJpegProcessor.mImageWriter:Landroid/media/ImageWriter;
        //   125: astore          15
        //   127: aload_0        
        //   128: getfield        androidx/camera/core/internal/YuvToJpegProcessor.mClosed:Z
        //   131: ifne            139
        //   134: iconst_1       
        //   135: istore_2       
        //   136: goto            141
        //   139: iconst_0       
        //   140: istore_2       
        //   141: aload_0        
        //   142: getfield        androidx/camera/core/internal/YuvToJpegProcessor.mImageRect:Landroid/graphics/Rect;
        //   145: astore          10
        //   147: iload_2        
        //   148: ifeq            161
        //   151: aload_0        
        //   152: aload_0        
        //   153: getfield        androidx/camera/core/internal/YuvToJpegProcessor.mProcessingImages:I
        //   156: iconst_1       
        //   157: iadd           
        //   158: putfield        androidx/camera/core/internal/YuvToJpegProcessor.mProcessingImages:I
        //   161: aload_0        
        //   162: getfield        androidx/camera/core/internal/YuvToJpegProcessor.mQuality:I
        //   165: istore_3       
        //   166: aload_0        
        //   167: getfield        androidx/camera/core/internal/YuvToJpegProcessor.mRotationDegrees:I
        //   170: istore          8
        //   172: aload           11
        //   174: monitorexit    
        //   175: aload_1        
        //   176: invokeinterface com/google/common/util/concurrent/ListenableFuture.get:()Ljava/lang/Object;
        //   181: checkcast       Landroidx/camera/core/ImageProxy;
        //   184: astore          13
        //   186: iload_2        
        //   187: ifne            299
        //   190: ldc             "YuvToJpegProcessor"
        //   192: ldc             "Image enqueued for processing on closed processor."
        //   194: invokestatic    androidx/camera/core/Logger.w:(Ljava/lang/String;Ljava/lang/String;)V
        //   197: aload           13
        //   199: invokeinterface androidx/camera/core/ImageProxy.close:()V
        //   204: aload_0        
        //   205: getfield        androidx/camera/core/internal/YuvToJpegProcessor.mLock:Ljava/lang/Object;
        //   208: astore_1       
        //   209: aload_1        
        //   210: monitorenter   
        //   211: iload           7
        //   213: istore_3       
        //   214: iload_2        
        //   215: ifeq            257
        //   218: aload_0        
        //   219: getfield        androidx/camera/core/internal/YuvToJpegProcessor.mProcessingImages:I
        //   222: istore_2       
        //   223: aload_0        
        //   224: iload_2        
        //   225: iconst_1       
        //   226: isub           
        //   227: putfield        androidx/camera/core/internal/YuvToJpegProcessor.mProcessingImages:I
        //   230: iload           7
        //   232: istore_3       
        //   233: iload_2        
        //   234: ifne            257
        //   237: iload           7
        //   239: istore_3       
        //   240: aload_0        
        //   241: getfield        androidx/camera/core/internal/YuvToJpegProcessor.mClosed:Z
        //   244: ifeq            257
        //   247: iconst_1       
        //   248: istore_3       
        //   249: goto            257
        //   252: astore          10
        //   254: goto            294
        //   257: aload_0        
        //   258: getfield        androidx/camera/core/internal/YuvToJpegProcessor.mCloseCompleter:Landroidx/concurrent/futures/CallbackToFutureAdapter$Completer;
        //   261: astore          10
        //   263: aload_1        
        //   264: monitorexit    
        //   265: iload_3        
        //   266: ifeq            293
        //   269: aload           15
        //   271: invokevirtual   android/media/ImageWriter.close:()V
        //   274: ldc             "YuvToJpegProcessor"
        //   276: ldc             "Closed after completion of last image processed."
        //   278: invokestatic    androidx/camera/core/Logger.d:(Ljava/lang/String;Ljava/lang/String;)V
        //   281: aload           10
        //   283: ifnull          293
        //   286: aload           10
        //   288: aconst_null    
        //   289: invokevirtual   androidx/concurrent/futures/CallbackToFutureAdapter$Completer.set:(Ljava/lang/Object;)Z
        //   292: pop            
        //   293: return         
        //   294: aload_1        
        //   295: monitorexit    
        //   296: aload           10
        //   298: athrow         
        //   299: aload           15
        //   301: invokevirtual   android/media/ImageWriter.dequeueInputImage:()Landroid/media/Image;
        //   304: astore          11
        //   306: aload           13
        //   308: astore          14
        //   310: aload           11
        //   312: astore          12
        //   314: aload_1        
        //   315: invokeinterface com/google/common/util/concurrent/ListenableFuture.get:()Ljava/lang/Object;
        //   320: checkcast       Landroidx/camera/core/ImageProxy;
        //   323: astore_1       
        //   324: aload_1        
        //   325: invokeinterface androidx/camera/core/ImageProxy.getFormat:()I
        //   330: bipush          35
        //   332: if_icmpne       341
        //   335: iconst_1       
        //   336: istore          9
        //   338: goto            344
        //   341: iconst_0       
        //   342: istore          9
        //   344: iload           9
        //   346: ldc             "Input image is not expected YUV_420_888 image format"
        //   348: invokestatic    androidx/core/util/Preconditions.checkState:(ZLjava/lang/String;)V
        //   351: aload_1        
        //   352: invokestatic    androidx/camera/core/internal/utils/ImageUtil.yuv_420_888toNv21:(Landroidx/camera/core/ImageProxy;)[B
        //   355: astore          13
        //   357: new             Landroid/graphics/YuvImage;
        //   360: astore          12
        //   362: aload           12
        //   364: aload           13
        //   366: bipush          17
        //   368: aload_1        
        //   369: invokeinterface androidx/camera/core/ImageProxy.getWidth:()I
        //   374: aload_1        
        //   375: invokeinterface androidx/camera/core/ImageProxy.getHeight:()I
        //   380: aconst_null    
        //   381: invokespecial   android/graphics/YuvImage.<init>:([BIII[I)V
        //   384: aload           11
        //   386: invokevirtual   android/media/Image.getPlanes:()[Landroid/media/Image$Plane;
        //   389: iconst_0       
        //   390: aaload         
        //   391: invokevirtual   android/media/Image$Plane.getBuffer:()Ljava/nio/ByteBuffer;
        //   394: astore          13
        //   396: aload           13
        //   398: invokevirtual   java/nio/ByteBuffer.position:()I
        //   401: istore          7
        //   403: new             Landroidx/camera/core/impl/utils/ExifOutputStream;
        //   406: astore          14
        //   408: new             Landroidx/camera/core/internal/ByteBufferOutputStream;
        //   411: astore          16
        //   413: aload           16
        //   415: aload           13
        //   417: invokespecial   androidx/camera/core/internal/ByteBufferOutputStream.<init>:(Ljava/nio/ByteBuffer;)V
        //   420: aload           14
        //   422: aload           16
        //   424: aload_1        
        //   425: iload           8
        //   427: invokestatic    androidx/camera/core/impl/utils/ExifData.create:(Landroidx/camera/core/ImageProxy;I)Landroidx/camera/core/impl/utils/ExifData;
        //   430: invokespecial   androidx/camera/core/impl/utils/ExifOutputStream.<init>:(Ljava/io/OutputStream;Landroidx/camera/core/impl/utils/ExifData;)V
        //   433: aload           12
        //   435: aload           10
        //   437: iload_3        
        //   438: aload           14
        //   440: invokevirtual   android/graphics/YuvImage.compressToJpeg:(Landroid/graphics/Rect;ILjava/io/OutputStream;)Z
        //   443: pop            
        //   444: aload_1        
        //   445: invokeinterface androidx/camera/core/ImageProxy.close:()V
        //   450: aload           13
        //   452: aload           13
        //   454: invokevirtual   java/nio/ByteBuffer.position:()I
        //   457: invokevirtual   java/nio/ByteBuffer.limit:(I)Ljava/nio/Buffer;
        //   460: pop            
        //   461: aload           13
        //   463: iload           7
        //   465: invokevirtual   java/nio/ByteBuffer.position:(I)Ljava/nio/Buffer;
        //   468: pop            
        //   469: aload           15
        //   471: aload           11
        //   473: invokevirtual   android/media/ImageWriter.queueInputImage:(Landroid/media/Image;)V
        //   476: aload_0        
        //   477: getfield        androidx/camera/core/internal/YuvToJpegProcessor.mLock:Ljava/lang/Object;
        //   480: astore          10
        //   482: aload           10
        //   484: monitorenter   
        //   485: iload           6
        //   487: istore_3       
        //   488: iload_2        
        //   489: ifeq            530
        //   492: aload_0        
        //   493: getfield        androidx/camera/core/internal/YuvToJpegProcessor.mProcessingImages:I
        //   496: istore_2       
        //   497: aload_0        
        //   498: iload_2        
        //   499: iconst_1       
        //   500: isub           
        //   501: putfield        androidx/camera/core/internal/YuvToJpegProcessor.mProcessingImages:I
        //   504: iload           6
        //   506: istore_3       
        //   507: iload_2        
        //   508: ifne            530
        //   511: iload           6
        //   513: istore_3       
        //   514: aload_0        
        //   515: getfield        androidx/camera/core/internal/YuvToJpegProcessor.mClosed:Z
        //   518: ifeq            530
        //   521: iconst_1       
        //   522: istore_3       
        //   523: goto            530
        //   526: astore_1       
        //   527: goto            567
        //   530: aload_0        
        //   531: getfield        androidx/camera/core/internal/YuvToJpegProcessor.mCloseCompleter:Landroidx/concurrent/futures/CallbackToFutureAdapter$Completer;
        //   534: astore_1       
        //   535: aload           10
        //   537: monitorexit    
        //   538: iload_3        
        //   539: ifeq            1002
        //   542: aload           15
        //   544: invokevirtual   android/media/ImageWriter.close:()V
        //   547: ldc             "YuvToJpegProcessor"
        //   549: ldc             "Closed after completion of last image processed."
        //   551: invokestatic    androidx/camera/core/Logger.d:(Ljava/lang/String;Ljava/lang/String;)V
        //   554: aload_1        
        //   555: ifnull          1002
        //   558: aload_1        
        //   559: aconst_null    
        //   560: invokevirtual   androidx/concurrent/futures/CallbackToFutureAdapter$Completer.set:(Ljava/lang/Object;)Z
        //   563: pop            
        //   564: goto            1002
        //   567: aload           10
        //   569: monitorexit    
        //   570: aload_1        
        //   571: athrow         
        //   572: astore_1       
        //   573: aconst_null    
        //   574: astore          10
        //   576: aload           11
        //   578: astore          12
        //   580: goto            772
        //   583: astore          10
        //   585: aconst_null    
        //   586: astore_1       
        //   587: goto            661
        //   590: astore          10
        //   592: aload_1        
        //   593: astore          12
        //   595: aload           10
        //   597: astore_1       
        //   598: aload           12
        //   600: astore          10
        //   602: aload           11
        //   604: astore          12
        //   606: goto            772
        //   609: astore          10
        //   611: goto            661
        //   614: astore          10
        //   616: aload           13
        //   618: astore_1       
        //   619: goto            661
        //   622: astore_1       
        //   623: aconst_null    
        //   624: astore          12
        //   626: aload           13
        //   628: astore          10
        //   630: goto            772
        //   633: astore          10
        //   635: aconst_null    
        //   636: astore          11
        //   638: aload           13
        //   640: astore_1       
        //   641: goto            661
        //   644: astore_1       
        //   645: aconst_null    
        //   646: astore          10
        //   648: aconst_null    
        //   649: astore          12
        //   651: goto            772
        //   654: astore          10
        //   656: aconst_null    
        //   657: astore_1       
        //   658: aconst_null    
        //   659: astore          11
        //   661: aload           11
        //   663: astore          12
        //   665: iload_2        
        //   666: ifeq            892
        //   669: aload_1        
        //   670: astore          14
        //   672: aload           11
        //   674: astore          12
        //   676: ldc             "YuvToJpegProcessor"
        //   678: ldc_w           "Failed to process YUV -> JPEG"
        //   681: aload           10
        //   683: invokestatic    androidx/camera/core/Logger.e:(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
        //   686: aload_1        
        //   687: astore          14
        //   689: aload           11
        //   691: astore          12
        //   693: aload           15
        //   695: invokevirtual   android/media/ImageWriter.dequeueInputImage:()Landroid/media/Image;
        //   698: astore          10
        //   700: aload_1        
        //   701: astore          14
        //   703: aload           10
        //   705: astore          12
        //   707: aload           10
        //   709: invokevirtual   android/media/Image.getPlanes:()[Landroid/media/Image$Plane;
        //   712: iconst_0       
        //   713: aaload         
        //   714: invokevirtual   android/media/Image$Plane.getBuffer:()Ljava/nio/ByteBuffer;
        //   717: astore          11
        //   719: aload_1        
        //   720: astore          14
        //   722: aload           10
        //   724: astore          12
        //   726: aload           11
        //   728: invokevirtual   java/nio/ByteBuffer.rewind:()Ljava/nio/Buffer;
        //   731: pop            
        //   732: aload_1        
        //   733: astore          14
        //   735: aload           10
        //   737: astore          12
        //   739: aload           11
        //   741: iconst_0       
        //   742: invokevirtual   java/nio/ByteBuffer.limit:(I)Ljava/nio/Buffer;
        //   745: pop            
        //   746: aload_1        
        //   747: astore          14
        //   749: aload           10
        //   751: astore          12
        //   753: aload           15
        //   755: aload           10
        //   757: invokevirtual   android/media/ImageWriter.queueInputImage:(Landroid/media/Image;)V
        //   760: aload           10
        //   762: astore          12
        //   764: goto            892
        //   767: astore_1       
        //   768: aload           14
        //   770: astore          10
        //   772: aload_0        
        //   773: getfield        androidx/camera/core/internal/YuvToJpegProcessor.mLock:Ljava/lang/Object;
        //   776: astore          11
        //   778: aload           11
        //   780: monitorenter   
        //   781: iload           5
        //   783: istore_3       
        //   784: iload_2        
        //   785: ifeq            826
        //   788: aload_0        
        //   789: getfield        androidx/camera/core/internal/YuvToJpegProcessor.mProcessingImages:I
        //   792: istore_2       
        //   793: aload_0        
        //   794: iload_2        
        //   795: iconst_1       
        //   796: isub           
        //   797: putfield        androidx/camera/core/internal/YuvToJpegProcessor.mProcessingImages:I
        //   800: iload           5
        //   802: istore_3       
        //   803: iload_2        
        //   804: ifne            826
        //   807: iload           5
        //   809: istore_3       
        //   810: aload_0        
        //   811: getfield        androidx/camera/core/internal/YuvToJpegProcessor.mClosed:Z
        //   814: ifeq            826
        //   817: iconst_1       
        //   818: istore_3       
        //   819: goto            826
        //   822: astore_1       
        //   823: goto            887
        //   826: aload_0        
        //   827: getfield        androidx/camera/core/internal/YuvToJpegProcessor.mCloseCompleter:Landroidx/concurrent/futures/CallbackToFutureAdapter$Completer;
        //   830: astore          13
        //   832: aload           11
        //   834: monitorexit    
        //   835: aload           12
        //   837: ifnull          845
        //   840: aload           12
        //   842: invokevirtual   android/media/Image.close:()V
        //   845: aload           10
        //   847: ifnull          857
        //   850: aload           10
        //   852: invokeinterface androidx/camera/core/ImageProxy.close:()V
        //   857: iload_3        
        //   858: ifeq            885
        //   861: aload           15
        //   863: invokevirtual   android/media/ImageWriter.close:()V
        //   866: ldc             "YuvToJpegProcessor"
        //   868: ldc             "Closed after completion of last image processed."
        //   870: invokestatic    androidx/camera/core/Logger.d:(Ljava/lang/String;Ljava/lang/String;)V
        //   873: aload           13
        //   875: ifnull          885
        //   878: aload           13
        //   880: aconst_null    
        //   881: invokevirtual   androidx/concurrent/futures/CallbackToFutureAdapter$Completer.set:(Ljava/lang/Object;)Z
        //   884: pop            
        //   885: aload_1        
        //   886: athrow         
        //   887: aload           11
        //   889: monitorexit    
        //   890: aload_1        
        //   891: athrow         
        //   892: aload_0        
        //   893: getfield        androidx/camera/core/internal/YuvToJpegProcessor.mLock:Ljava/lang/Object;
        //   896: astore          11
        //   898: aload           11
        //   900: monitorenter   
        //   901: iload           4
        //   903: istore_3       
        //   904: iload_2        
        //   905: ifeq            946
        //   908: aload_0        
        //   909: getfield        androidx/camera/core/internal/YuvToJpegProcessor.mProcessingImages:I
        //   912: istore_2       
        //   913: aload_0        
        //   914: iload_2        
        //   915: iconst_1       
        //   916: isub           
        //   917: putfield        androidx/camera/core/internal/YuvToJpegProcessor.mProcessingImages:I
        //   920: iload           4
        //   922: istore_3       
        //   923: iload_2        
        //   924: ifne            946
        //   927: iload           4
        //   929: istore_3       
        //   930: aload_0        
        //   931: getfield        androidx/camera/core/internal/YuvToJpegProcessor.mClosed:Z
        //   934: ifeq            946
        //   937: iconst_1       
        //   938: istore_3       
        //   939: goto            946
        //   942: astore_1       
        //   943: goto            1003
        //   946: aload_0        
        //   947: getfield        androidx/camera/core/internal/YuvToJpegProcessor.mCloseCompleter:Landroidx/concurrent/futures/CallbackToFutureAdapter$Completer;
        //   950: astore          10
        //   952: aload           11
        //   954: monitorexit    
        //   955: aload           12
        //   957: ifnull          965
        //   960: aload           12
        //   962: invokevirtual   android/media/Image.close:()V
        //   965: aload_1        
        //   966: ifnull          975
        //   969: aload_1        
        //   970: invokeinterface androidx/camera/core/ImageProxy.close:()V
        //   975: iload_3        
        //   976: ifeq            1002
        //   979: aload           15
        //   981: invokevirtual   android/media/ImageWriter.close:()V
        //   984: ldc             "YuvToJpegProcessor"
        //   986: ldc             "Closed after completion of last image processed."
        //   988: invokestatic    androidx/camera/core/Logger.d:(Ljava/lang/String;Ljava/lang/String;)V
        //   991: aload           10
        //   993: ifnull          1002
        //   996: aload           10
        //   998: astore_1       
        //   999: goto            558
        //  1002: return         
        //  1003: aload           11
        //  1005: monitorexit    
        //  1006: aload_1        
        //  1007: athrow         
        //  1008: astore_1       
        //  1009: aload           11
        //  1011: monitorexit    
        //  1012: aload_1        
        //  1013: athrow         
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                 
        //  -----  -----  -----  -----  ---------------------
        //  121    134    1008   1014   Any
        //  141    147    1008   1014   Any
        //  151    161    1008   1014   Any
        //  161    175    1008   1014   Any
        //  175    186    654    661    Ljava/lang/Exception;
        //  175    186    644    654    Any
        //  190    204    633    644    Ljava/lang/Exception;
        //  190    204    622    633    Any
        //  218    230    252    299    Any
        //  240    247    252    299    Any
        //  257    265    252    299    Any
        //  294    296    252    299    Any
        //  299    306    633    644    Ljava/lang/Exception;
        //  299    306    622    633    Any
        //  314    324    614    622    Ljava/lang/Exception;
        //  314    324    767    772    Any
        //  324    335    609    614    Ljava/lang/Exception;
        //  324    335    590    609    Any
        //  344    450    609    614    Ljava/lang/Exception;
        //  344    450    590    609    Any
        //  450    476    583    590    Ljava/lang/Exception;
        //  450    476    572    583    Any
        //  492    504    526    572    Any
        //  514    521    526    572    Any
        //  530    538    526    572    Any
        //  567    570    526    572    Any
        //  676    686    767    772    Any
        //  693    700    767    772    Any
        //  707    719    767    772    Any
        //  726    732    767    772    Any
        //  739    746    767    772    Any
        //  753    760    767    772    Any
        //  788    800    822    892    Any
        //  810    817    822    892    Any
        //  826    835    822    892    Any
        //  887    890    822    892    Any
        //  908    920    942    1008   Any
        //  930    937    942    1008   Any
        //  946    955    942    1008   Any
        //  1003   1006   942    1008   Any
        //  1009   1012   1008   1014   Any
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0341:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2604)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:206)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public void setJpegQuality(final int mQuality) {
        synchronized (this.mLock) {
            this.mQuality = mQuality;
        }
    }
    
    public void setRotationDegrees(final int mRotationDegrees) {
        synchronized (this.mLock) {
            this.mRotationDegrees = mRotationDegrees;
        }
    }
}

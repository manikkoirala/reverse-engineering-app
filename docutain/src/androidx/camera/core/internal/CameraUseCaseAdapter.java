// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.internal;

import java.util.Arrays;
import androidx.camera.core.CameraInfo;
import androidx.camera.core.CameraControl;
import androidx.camera.core.processing.SurfaceProcessorWithExecutor;
import java.util.Objects;
import androidx.camera.core.SurfaceProcessor;
import androidx.camera.core.processing.SurfaceProcessorInternal;
import androidx.core.util.Consumer;
import androidx.camera.core.impl.utils.executor.CameraXExecutors;
import androidx.camera.core.SurfaceRequest;
import android.graphics.SurfaceTexture;
import android.view.Surface;
import androidx.camera.core.Logger;
import androidx.camera.core.Preview;
import androidx.camera.core.ImageCapture;
import androidx.camera.core.impl.UseCaseConfig;
import androidx.camera.core.impl.AttachedSurfaceInfo;
import android.util.Range;
import java.util.HashMap;
import java.util.Map;
import androidx.camera.core.impl.CameraInfoInternal;
import android.graphics.Matrix$ScaleToFit;
import android.graphics.RectF;
import androidx.core.util.Preconditions;
import android.graphics.Matrix;
import android.util.Size;
import android.graphics.Rect;
import java.util.Iterator;
import androidx.camera.core.impl.CameraControlInternal;
import java.util.Collection;
import androidx.camera.core.impl.CameraConfigs;
import java.util.Collections;
import java.util.ArrayList;
import androidx.camera.core.ViewPort;
import androidx.camera.core.impl.UseCaseConfigFactory;
import androidx.camera.core.impl.Config;
import androidx.camera.core.UseCase;
import androidx.camera.core.CameraEffect;
import java.util.List;
import java.util.LinkedHashSet;
import androidx.camera.core.impl.CameraInternal;
import androidx.camera.core.impl.CameraDeviceSurfaceManager;
import androidx.camera.core.impl.CameraConfig;
import androidx.camera.core.Camera;

public final class CameraUseCaseAdapter implements Camera
{
    private static final String TAG = "CameraUseCaseAdapter";
    private boolean mAttached;
    private CameraConfig mCameraConfig;
    private final CameraDeviceSurfaceManager mCameraDeviceSurfaceManager;
    private CameraInternal mCameraInternal;
    private final LinkedHashSet<CameraInternal> mCameraInternals;
    private List<CameraEffect> mEffects;
    private List<UseCase> mExtraUseCases;
    private final CameraId mId;
    private Config mInteropConfig;
    private final Object mLock;
    private final UseCaseConfigFactory mUseCaseConfigFactory;
    private final List<UseCase> mUseCases;
    private ViewPort mViewPort;
    
    public CameraUseCaseAdapter(final LinkedHashSet<CameraInternal> c, final CameraDeviceSurfaceManager mCameraDeviceSurfaceManager, final UseCaseConfigFactory mUseCaseConfigFactory) {
        this.mUseCases = new ArrayList<UseCase>();
        this.mEffects = Collections.emptyList();
        this.mCameraConfig = CameraConfigs.emptyConfig();
        this.mLock = new Object();
        this.mAttached = true;
        this.mInteropConfig = null;
        this.mExtraUseCases = new ArrayList<UseCase>();
        this.mCameraInternal = c.iterator().next();
        final LinkedHashSet mCameraInternals = new LinkedHashSet((Collection<? extends E>)c);
        this.mCameraInternals = mCameraInternals;
        this.mId = new CameraId(mCameraInternals);
        this.mCameraDeviceSurfaceManager = mCameraDeviceSurfaceManager;
        this.mUseCaseConfigFactory = mUseCaseConfigFactory;
    }
    
    private void cacheInteropConfig() {
        synchronized (this.mLock) {
            final CameraControlInternal cameraControlInternal = this.mCameraInternal.getCameraControlInternal();
            this.mInteropConfig = cameraControlInternal.getInteropConfig();
            cameraControlInternal.clearInteropConfig();
        }
    }
    
    private List<UseCase> calculateRequiredExtraUseCases(final List<UseCase> list, final List<UseCase> c) {
        final ArrayList list2 = new ArrayList((Collection<? extends E>)c);
        final boolean extraPreviewRequired = this.isExtraPreviewRequired(list);
        final boolean extraImageCaptureRequired = this.isExtraImageCaptureRequired(list);
        final Iterator<Object> iterator = (Iterator<Object>)c.iterator();
        Object o = null;
        Object o2 = null;
        while (iterator.hasNext()) {
            final UseCase useCase = iterator.next();
            if (this.isPreview(useCase)) {
                o = useCase;
            }
            else {
                if (!this.isImageCapture(useCase)) {
                    continue;
                }
                o2 = useCase;
            }
        }
        if (extraPreviewRequired && o == null) {
            list2.add(this.createExtraPreview());
        }
        else if (!extraPreviewRequired && o != null) {
            list2.remove(o);
        }
        if (extraImageCaptureRequired && o2 == null) {
            list2.add(this.createExtraImageCapture());
        }
        else if (!extraImageCaptureRequired && o2 != null) {
            list2.remove(o2);
        }
        return list2;
    }
    
    private static Matrix calculateSensorToBufferTransformMatrix(final Rect rect, final Size size) {
        Preconditions.checkArgument(rect.width() > 0 && rect.height() > 0, (Object)"Cannot compute viewport crop rects zero sized sensor rect.");
        final RectF rectF = new RectF(rect);
        final Matrix matrix = new Matrix();
        matrix.setRectToRect(new RectF(0.0f, 0.0f, (float)size.getWidth(), (float)size.getHeight()), rectF, Matrix$ScaleToFit.CENTER);
        matrix.invert(matrix);
        return matrix;
    }
    
    private Map<UseCase, Size> calculateSuggestedResolutions(final CameraInfoInternal cameraInfoInternal, final List<UseCase> list, final List<UseCase> list2, final Map<UseCase, ConfigPair> map) {
        final ArrayList list3 = new ArrayList();
        final String cameraId = cameraInfoInternal.getCameraId();
        final HashMap hashMap = new HashMap();
        for (final UseCase useCase : list2) {
            list3.add(AttachedSurfaceInfo.create(this.mCameraDeviceSurfaceManager.transformSurfaceConfig(cameraId, useCase.getImageFormat(), useCase.getAttachedSurfaceResolution()), useCase.getImageFormat(), useCase.getAttachedSurfaceResolution(), useCase.getCurrentConfig().getTargetFramerate(null)));
            hashMap.put(useCase, useCase.getAttachedSurfaceResolution());
        }
        if (!list.isEmpty()) {
            final HashMap hashMap2 = new HashMap();
            for (final UseCase useCase2 : list) {
                final ConfigPair configPair = map.get(useCase2);
                hashMap2.put(useCase2.mergeConfigs(cameraInfoInternal, configPair.mExtendedConfig, configPair.mCameraConfig), useCase2);
            }
            final Map<UseCaseConfig<?>, Size> suggestedResolutions = this.mCameraDeviceSurfaceManager.getSuggestedResolutions(cameraId, list3, new ArrayList<UseCaseConfig<?>>(hashMap2.keySet()));
            for (final Map.Entry<K, UseCase> entry : hashMap2.entrySet()) {
                hashMap.put(entry.getValue(), suggestedResolutions.get(entry.getKey()));
            }
        }
        return hashMap;
    }
    
    private ImageCapture createExtraImageCapture() {
        return new ImageCapture.Builder().setTargetName("ImageCapture-Extra").build();
    }
    
    private Preview createExtraPreview() {
        final Preview build = new Preview.Builder().setTargetName("Preview-Extra").build();
        build.setSurfaceProvider((Preview.SurfaceProvider)new CameraUseCaseAdapter$$ExternalSyntheticLambda1());
        return build;
    }
    
    private void detachUnnecessaryUseCases(final List<UseCase> list) {
        synchronized (this.mLock) {
            if (!list.isEmpty()) {
                this.mCameraInternal.detachUseCases(list);
                for (final UseCase obj : list) {
                    if (this.mUseCases.contains(obj)) {
                        obj.onDetach(this.mCameraInternal);
                    }
                    else {
                        final StringBuilder sb = new StringBuilder();
                        sb.append("Attempting to detach non-attached UseCase: ");
                        sb.append(obj);
                        Logger.e("CameraUseCaseAdapter", sb.toString());
                    }
                }
                this.mUseCases.removeAll(list);
            }
        }
    }
    
    public static CameraId generateCameraId(final LinkedHashSet<CameraInternal> set) {
        return new CameraId(set);
    }
    
    private Map<UseCase, ConfigPair> getConfigs(final List<UseCase> list, final UseCaseConfigFactory useCaseConfigFactory, final UseCaseConfigFactory useCaseConfigFactory2) {
        final HashMap hashMap = new HashMap();
        for (final UseCase useCase : list) {
            hashMap.put(useCase, new ConfigPair(useCase.getDefaultConfig(false, useCaseConfigFactory), useCase.getDefaultConfig(true, useCaseConfigFactory2)));
        }
        return hashMap;
    }
    
    private boolean isCoexistingPreviewImageCaptureRequired() {
        synchronized (this.mLock) {
            final int useCaseCombinationRequiredRule = this.mCameraConfig.getUseCaseCombinationRequiredRule();
            boolean b = true;
            if (useCaseCombinationRequiredRule != 1) {
                b = false;
            }
            return b;
        }
    }
    
    private boolean isExtraImageCaptureRequired(final List<UseCase> list) {
        final Iterator<UseCase> iterator = list.iterator();
        final boolean b = false;
        boolean b2 = false;
        boolean b3 = false;
        while (iterator.hasNext()) {
            final UseCase useCase = iterator.next();
            if (this.isPreview(useCase)) {
                b2 = true;
            }
            else {
                if (!this.isImageCapture(useCase)) {
                    continue;
                }
                b3 = true;
            }
        }
        boolean b4 = b;
        if (b2) {
            b4 = b;
            if (!b3) {
                b4 = true;
            }
        }
        return b4;
    }
    
    private boolean isExtraPreviewRequired(final List<UseCase> list) {
        final Iterator<UseCase> iterator = list.iterator();
        final boolean b = false;
        boolean b2 = false;
        boolean b3 = false;
        while (iterator.hasNext()) {
            final UseCase useCase = iterator.next();
            if (this.isPreview(useCase)) {
                b3 = true;
            }
            else {
                if (!this.isImageCapture(useCase)) {
                    continue;
                }
                b2 = true;
            }
        }
        boolean b4 = b;
        if (b2) {
            b4 = b;
            if (!b3) {
                b4 = true;
            }
        }
        return b4;
    }
    
    private boolean isImageCapture(final UseCase useCase) {
        return useCase instanceof ImageCapture;
    }
    
    private boolean isPreview(final UseCase useCase) {
        return useCase instanceof Preview;
    }
    
    private void restoreInteropConfig() {
        synchronized (this.mLock) {
            if (this.mInteropConfig != null) {
                this.mCameraInternal.getCameraControlInternal().addInteropConfig(this.mInteropConfig);
            }
        }
    }
    
    static void updateEffects(final List<CameraEffect> list, final Collection<UseCase> collection) {
        final HashMap hashMap = new HashMap();
        for (final CameraEffect cameraEffect : list) {
            hashMap.put(cameraEffect.getTargets(), cameraEffect);
        }
        for (final UseCase useCase : collection) {
            if (useCase instanceof Preview) {
                final Preview preview = (Preview)useCase;
                final CameraEffect cameraEffect2 = (CameraEffect)hashMap.get(1);
                if (cameraEffect2 == null) {
                    preview.setProcessor(null);
                }
                else {
                    preview.setProcessor(new SurfaceProcessorWithExecutor(Objects.requireNonNull(cameraEffect2.getSurfaceProcessor()), cameraEffect2.getProcessorExecutor()));
                }
            }
        }
    }
    
    private void updateViewPort(final Map<UseCase, Size> map, final Collection<UseCase> collection) {
        synchronized (this.mLock) {
            if (this.mViewPort != null) {
                final Integer lensFacing = this.mCameraInternal.getCameraInfoInternal().getLensFacing();
                boolean b = true;
                if (lensFacing == null) {
                    Logger.w("CameraUseCaseAdapter", "The lens facing is null, probably an external.");
                    b = true;
                }
                else if (lensFacing != 0) {
                    b = false;
                }
                final Map<UseCase, Rect> calculateViewPortRects = ViewPorts.calculateViewPortRects(this.mCameraInternal.getCameraControlInternal().getSensorRect(), b, this.mViewPort.getAspectRatio(), this.mCameraInternal.getCameraInfoInternal().getSensorRotationDegrees(this.mViewPort.getRotation()), this.mViewPort.getScaleType(), this.mViewPort.getLayoutDirection(), map);
                for (final UseCase useCase : collection) {
                    useCase.setViewPortCropRect(Preconditions.checkNotNull(calculateViewPortRects.get(useCase)));
                    useCase.setSensorToBufferTransformMatrix(calculateSensorToBufferTransformMatrix(this.mCameraInternal.getCameraControlInternal().getSensorRect(), map.get(useCase)));
                }
            }
        }
    }
    
    public void addUseCases(final Collection<UseCase> collection) throws CameraException {
        synchronized (this.mLock) {
            final ArrayList list = new ArrayList();
            for (final UseCase useCase : collection) {
                if (this.mUseCases.contains(useCase)) {
                    Logger.d("CameraUseCaseAdapter", "Attempting to attach already attached UseCase");
                }
                else {
                    list.add(useCase);
                }
            }
            final ArrayList list2 = new ArrayList(this.mUseCases);
            Object o = Collections.emptyList();
            List<Object> emptyList = (List<Object>)Collections.emptyList();
            if (this.isCoexistingPreviewImageCaptureRequired()) {
                list2.removeAll(this.mExtraUseCases);
                list2.addAll(list);
                o = this.calculateRequiredExtraUseCases(list2, new ArrayList<UseCase>(this.mExtraUseCases));
                final ArrayList list3 = new ArrayList((Collection)o);
                list3.removeAll(this.mExtraUseCases);
                list.addAll(list3);
                emptyList = (List<Object>)new ArrayList<UseCase>(this.mExtraUseCases);
                emptyList.removeAll((Collection<?>)o);
            }
            final Map<UseCase, ConfigPair> configs = this.getConfigs(list, this.mCameraConfig.getUseCaseConfigFactory(), this.mUseCaseConfigFactory);
            try {
                final ArrayList list4 = new ArrayList<UseCase>(this.mUseCases);
                list4.removeAll(emptyList);
                final Map<UseCase, Size> calculateSuggestedResolutions = this.calculateSuggestedResolutions(this.mCameraInternal.getCameraInfoInternal(), list, list4, configs);
                this.updateViewPort(calculateSuggestedResolutions, collection);
                updateEffects(this.mEffects, collection);
                this.mExtraUseCases = (List<UseCase>)o;
                this.detachUnnecessaryUseCases((List<UseCase>)emptyList);
                for (final UseCase useCase2 : list) {
                    final ConfigPair configPair = configs.get(useCase2);
                    useCase2.onAttach(this.mCameraInternal, configPair.mExtendedConfig, configPair.mCameraConfig);
                    useCase2.updateSuggestedResolution(Preconditions.checkNotNull(calculateSuggestedResolutions.get(useCase2)));
                }
                this.mUseCases.addAll(list);
                if (this.mAttached) {
                    this.mCameraInternal.attachUseCases(list);
                }
                final Iterator iterator3 = list.iterator();
                while (iterator3.hasNext()) {
                    ((UseCase)iterator3.next()).notifyState();
                }
            }
            catch (final IllegalArgumentException ex) {
                throw new CameraException(ex.getMessage());
            }
        }
    }
    
    public void attachUseCases() {
        synchronized (this.mLock) {
            if (!this.mAttached) {
                this.mCameraInternal.attachUseCases(this.mUseCases);
                this.restoreInteropConfig();
                final Iterator<UseCase> iterator = this.mUseCases.iterator();
                while (iterator.hasNext()) {
                    iterator.next().notifyState();
                }
                this.mAttached = true;
            }
        }
    }
    
    public void detachUseCases() {
        synchronized (this.mLock) {
            if (this.mAttached) {
                this.mCameraInternal.detachUseCases(new ArrayList<UseCase>(this.mUseCases));
                this.cacheInteropConfig();
                this.mAttached = false;
            }
        }
    }
    
    @Override
    public CameraControl getCameraControl() {
        return this.mCameraInternal.getCameraControlInternal();
    }
    
    public CameraId getCameraId() {
        return this.mId;
    }
    
    @Override
    public CameraInfo getCameraInfo() {
        return this.mCameraInternal.getCameraInfoInternal();
    }
    
    @Override
    public LinkedHashSet<CameraInternal> getCameraInternals() {
        return this.mCameraInternals;
    }
    
    @Override
    public CameraConfig getExtendedConfig() {
        synchronized (this.mLock) {
            return this.mCameraConfig;
        }
    }
    
    public List<UseCase> getUseCases() {
        synchronized (this.mLock) {
            return new ArrayList<UseCase>(this.mUseCases);
        }
    }
    
    public boolean isEquivalent(final CameraUseCaseAdapter cameraUseCaseAdapter) {
        return this.mId.equals(cameraUseCaseAdapter.getCameraId());
    }
    
    @Override
    public boolean isUseCasesCombinationSupported(final UseCase... array) {
        final Object mLock = this.mLock;
        monitorenter(mLock);
        try {
            try {
                this.calculateSuggestedResolutions(this.mCameraInternal.getCameraInfoInternal(), Arrays.asList(array), Collections.emptyList(), this.getConfigs(Arrays.asList(array), this.mCameraConfig.getUseCaseConfigFactory(), this.mUseCaseConfigFactory));
                monitorexit(mLock);
                return true;
            }
            finally {
                monitorexit(mLock);
            }
        }
        catch (final IllegalArgumentException ex) {}
    }
    
    public void removeUseCases(final Collection<UseCase> c) {
        synchronized (this.mLock) {
            this.detachUnnecessaryUseCases(new ArrayList<UseCase>(c));
            if (this.isCoexistingPreviewImageCaptureRequired()) {
                this.mExtraUseCases.removeAll(c);
                try {
                    this.addUseCases((Collection<UseCase>)Collections.emptyList());
                }
                catch (final CameraException ex) {
                    throw new IllegalArgumentException("Failed to add extra fake Preview or ImageCapture use case!");
                }
            }
        }
    }
    
    public void setActiveResumingMode(final boolean activeResumingMode) {
        this.mCameraInternal.setActiveResumingMode(activeResumingMode);
    }
    
    public void setEffects(final List<CameraEffect> mEffects) {
        synchronized (this.mLock) {
            this.mEffects = mEffects;
        }
    }
    
    @Override
    public void setExtendedConfig(final CameraConfig cameraConfig) {
        final Object mLock = this.mLock;
        monitorenter(mLock);
        CameraConfig emptyConfig = cameraConfig;
        Label_0017: {
            if (cameraConfig != null) {
                break Label_0017;
            }
            try {
                emptyConfig = CameraConfigs.emptyConfig();
                if (!this.mUseCases.isEmpty() && !this.mCameraConfig.getCompatibilityId().equals(emptyConfig.getCompatibilityId())) {
                    throw new IllegalStateException("Need to unbind all use cases before binding with extension enabled");
                }
                this.mCameraConfig = emptyConfig;
                this.mCameraInternal.setExtendedConfig(emptyConfig);
            }
            finally {
                monitorexit(mLock);
            }
        }
    }
    
    public void setViewPort(final ViewPort mViewPort) {
        synchronized (this.mLock) {
            this.mViewPort = mViewPort;
        }
    }
    
    public static final class CameraException extends Exception
    {
        public CameraException() {
        }
        
        public CameraException(final String message) {
            super(message);
        }
        
        public CameraException(final Throwable cause) {
            super(cause);
        }
    }
    
    public static final class CameraId
    {
        private final List<String> mIds;
        
        CameraId(final LinkedHashSet<CameraInternal> set) {
            this.mIds = new ArrayList<String>();
            final Iterator<Object> iterator = set.iterator();
            while (iterator.hasNext()) {
                this.mIds.add(iterator.next().getCameraInfoInternal().getCameraId());
            }
        }
        
        @Override
        public boolean equals(final Object o) {
            return o instanceof CameraId && this.mIds.equals(((CameraId)o).mIds);
        }
        
        @Override
        public int hashCode() {
            return this.mIds.hashCode() * 53;
        }
    }
    
    private static class ConfigPair
    {
        UseCaseConfig<?> mCameraConfig;
        UseCaseConfig<?> mExtendedConfig;
        
        ConfigPair(final UseCaseConfig<?> mExtendedConfig, final UseCaseConfig<?> mCameraConfig) {
            this.mExtendedConfig = mExtendedConfig;
            this.mCameraConfig = mCameraConfig;
        }
    }
}

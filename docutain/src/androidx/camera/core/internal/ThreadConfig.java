// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.internal;

import java.util.concurrent.Executor;
import androidx.camera.core.impl.Config;
import androidx.camera.core.impl.ReadableConfig;

public interface ThreadConfig extends ReadableConfig
{
    public static final Option<Executor> OPTION_BACKGROUND_EXECUTOR = Config.Option.create("camerax.core.thread.backgroundExecutor", Executor.class);
    
    Executor getBackgroundExecutor();
    
    Executor getBackgroundExecutor(final Executor p0);
    
    public interface Builder<B>
    {
        B setBackgroundExecutor(final Executor p0);
    }
}

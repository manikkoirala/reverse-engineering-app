// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core;

import java.util.Iterator;
import java.util.Collection;
import android.media.Image;
import android.graphics.Rect;
import java.util.HashSet;
import java.util.Set;

public abstract class ForwardingImageProxy implements ImageProxy
{
    protected final ImageProxy mImage;
    private final Object mLock;
    private final Set<OnImageCloseListener> mOnImageCloseListeners;
    
    protected ForwardingImageProxy(final ImageProxy mImage) {
        this.mLock = new Object();
        this.mOnImageCloseListeners = new HashSet<OnImageCloseListener>();
        this.mImage = mImage;
    }
    
    public void addOnImageCloseListener(final OnImageCloseListener onImageCloseListener) {
        synchronized (this.mLock) {
            this.mOnImageCloseListeners.add(onImageCloseListener);
        }
    }
    
    @Override
    public void close() {
        this.mImage.close();
        this.notifyOnImageCloseListeners();
    }
    
    @Override
    public Rect getCropRect() {
        return this.mImage.getCropRect();
    }
    
    @Override
    public int getFormat() {
        return this.mImage.getFormat();
    }
    
    @Override
    public int getHeight() {
        return this.mImage.getHeight();
    }
    
    @Override
    public Image getImage() {
        return this.mImage.getImage();
    }
    
    @Override
    public ImageInfo getImageInfo() {
        return this.mImage.getImageInfo();
    }
    
    @Override
    public PlaneProxy[] getPlanes() {
        return this.mImage.getPlanes();
    }
    
    @Override
    public int getWidth() {
        return this.mImage.getWidth();
    }
    
    protected void notifyOnImageCloseListeners() {
        Object o = this.mLock;
        synchronized (o) {
            final HashSet set = new HashSet(this.mOnImageCloseListeners);
            monitorexit(o);
            o = set.iterator();
            while (((Iterator)o).hasNext()) {
                ((OnImageCloseListener)((Iterator)o).next()).onImageClose(this);
            }
        }
    }
    
    @Override
    public void setCropRect(final Rect cropRect) {
        this.mImage.setCropRect(cropRect);
    }
    
    public interface OnImageCloseListener
    {
        void onImageClose(final ImageProxy p0);
    }
}

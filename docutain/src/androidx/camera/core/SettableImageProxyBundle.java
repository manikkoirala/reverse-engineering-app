// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core;

import java.util.Collections;
import java.util.Iterator;
import java.util.ArrayList;
import com.google.common.util.concurrent.ListenableFuture;
import androidx.concurrent.futures.CallbackToFutureAdapter;
import android.util.SparseArray;
import java.util.List;
import androidx.camera.core.impl.ImageProxyBundle;

final class SettableImageProxyBundle implements ImageProxyBundle
{
    private final List<Integer> mCaptureIdList;
    private boolean mClosed;
    final SparseArray<CallbackToFutureAdapter.Completer<ImageProxy>> mCompleters;
    private final SparseArray<ListenableFuture<ImageProxy>> mFutureResults;
    final Object mLock;
    private final List<ImageProxy> mOwnedImageProxies;
    private String mTagBundleKey;
    
    SettableImageProxyBundle(final List<Integer> mCaptureIdList, final String mTagBundleKey) {
        this.mLock = new Object();
        this.mCompleters = (SparseArray<CallbackToFutureAdapter.Completer<ImageProxy>>)new SparseArray();
        this.mFutureResults = (SparseArray<ListenableFuture<ImageProxy>>)new SparseArray();
        this.mOwnedImageProxies = new ArrayList<ImageProxy>();
        this.mClosed = false;
        this.mCaptureIdList = mCaptureIdList;
        this.mTagBundleKey = mTagBundleKey;
        this.setup();
    }
    
    private void setup() {
        synchronized (this.mLock) {
            for (final int intValue : this.mCaptureIdList) {
                this.mFutureResults.put(intValue, (Object)CallbackToFutureAdapter.getFuture((CallbackToFutureAdapter.Resolver<Object>)new CallbackToFutureAdapter.Resolver<ImageProxy>(this, intValue) {
                    final SettableImageProxyBundle this$0;
                    final int val$captureId;
                    
                    @Override
                    public Object attachCompleter(final Completer<ImageProxy> completer) {
                        synchronized (this.this$0.mLock) {
                            this.this$0.mCompleters.put(this.val$captureId, (Object)completer);
                            monitorexit(this.this$0.mLock);
                            final StringBuilder sb = new StringBuilder();
                            sb.append("getImageProxy(id: ");
                            sb.append(this.val$captureId);
                            sb.append(")");
                            return sb.toString();
                        }
                    }
                }));
            }
        }
    }
    
    void addImageProxy(final ImageProxy imageProxy) {
        synchronized (this.mLock) {
            if (this.mClosed) {
                return;
            }
            final Integer obj = (Integer)imageProxy.getImageInfo().getTagBundle().getTag(this.mTagBundleKey);
            if (obj == null) {
                throw new IllegalArgumentException("CaptureId is null.");
            }
            final CallbackToFutureAdapter.Completer completer = (CallbackToFutureAdapter.Completer)this.mCompleters.get((int)obj);
            if (completer != null) {
                this.mOwnedImageProxies.add(imageProxy);
                completer.set(imageProxy);
                return;
            }
            final StringBuilder sb = new StringBuilder();
            sb.append("ImageProxyBundle does not contain this id: ");
            sb.append(obj);
            throw new IllegalArgumentException(sb.toString());
        }
    }
    
    void close() {
        synchronized (this.mLock) {
            if (this.mClosed) {
                return;
            }
            final Iterator<ImageProxy> iterator = this.mOwnedImageProxies.iterator();
            while (iterator.hasNext()) {
                iterator.next().close();
            }
            this.mOwnedImageProxies.clear();
            this.mFutureResults.clear();
            this.mCompleters.clear();
            this.mClosed = true;
        }
    }
    
    @Override
    public List<Integer> getCaptureIds() {
        return Collections.unmodifiableList((List<? extends Integer>)this.mCaptureIdList);
    }
    
    @Override
    public ListenableFuture<ImageProxy> getImageProxy(final int i) {
        synchronized (this.mLock) {
            if (this.mClosed) {
                throw new IllegalStateException("ImageProxyBundle already closed.");
            }
            final ListenableFuture listenableFuture = (ListenableFuture)this.mFutureResults.get(i);
            if (listenableFuture != null) {
                return (ListenableFuture<ImageProxy>)listenableFuture;
            }
            final StringBuilder sb = new StringBuilder();
            sb.append("ImageProxyBundle does not contain this id: ");
            sb.append(i);
            throw new IllegalArgumentException(sb.toString());
        }
    }
    
    void reset() {
        synchronized (this.mLock) {
            if (this.mClosed) {
                return;
            }
            final Iterator<ImageProxy> iterator = this.mOwnedImageProxies.iterator();
            while (iterator.hasNext()) {
                iterator.next().close();
            }
            this.mOwnedImageProxies.clear();
            this.mFutureResults.clear();
            this.mCompleters.clear();
            this.setup();
        }
    }
}

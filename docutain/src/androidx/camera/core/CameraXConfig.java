// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core;

import java.util.UUID;
import androidx.camera.core.impl.MutableConfig;
import androidx.camera.core.impl.MutableOptionsBundle;
import androidx.camera.core.internal.TargetConfig$_CC;
import java.util.Set;
import androidx.camera.core.impl.ReadableConfig;
import androidx.camera.core.impl.ReadableConfig$_CC;
import androidx.camera.core.impl.OptionsBundle;
import androidx.camera.core.impl.UseCaseConfigFactory;
import android.os.Handler;
import androidx.camera.core.impl.CameraDeviceSurfaceManager;
import androidx.camera.core.impl.CameraFactory;
import java.util.concurrent.Executor;
import androidx.camera.core.impl.Config;
import androidx.camera.core.internal.TargetConfig;

public final class CameraXConfig implements TargetConfig<CameraX>
{
    static final Option<CameraSelector> OPTION_AVAILABLE_CAMERAS_LIMITER;
    static final Option<Executor> OPTION_CAMERA_EXECUTOR;
    static final Option<CameraFactory.Provider> OPTION_CAMERA_FACTORY_PROVIDER;
    static final Option<CameraDeviceSurfaceManager.Provider> OPTION_DEVICE_SURFACE_MANAGER_PROVIDER;
    static final Option<Integer> OPTION_MIN_LOGGING_LEVEL;
    static final Option<Handler> OPTION_SCHEDULER_HANDLER;
    static final Option<UseCaseConfigFactory.Provider> OPTION_USECASE_CONFIG_FACTORY_PROVIDER;
    private final OptionsBundle mConfig;
    
    static {
        OPTION_CAMERA_FACTORY_PROVIDER = (Option)Config.Option.create("camerax.core.appConfig.cameraFactoryProvider", CameraFactory.Provider.class);
        OPTION_DEVICE_SURFACE_MANAGER_PROVIDER = (Option)Config.Option.create("camerax.core.appConfig.deviceSurfaceManagerProvider", CameraDeviceSurfaceManager.Provider.class);
        OPTION_USECASE_CONFIG_FACTORY_PROVIDER = (Option)Config.Option.create("camerax.core.appConfig.useCaseConfigFactoryProvider", UseCaseConfigFactory.Provider.class);
        OPTION_CAMERA_EXECUTOR = (Option)Config.Option.create("camerax.core.appConfig.cameraExecutor", Executor.class);
        OPTION_SCHEDULER_HANDLER = (Option)Config.Option.create("camerax.core.appConfig.schedulerHandler", Handler.class);
        OPTION_MIN_LOGGING_LEVEL = (Option)Config.Option.create("camerax.core.appConfig.minimumLoggingLevel", Integer.TYPE);
        OPTION_AVAILABLE_CAMERAS_LIMITER = (Option)Config.Option.create("camerax.core.appConfig.availableCamerasLimiter", CameraSelector.class);
    }
    
    CameraXConfig(final OptionsBundle mConfig) {
        this.mConfig = mConfig;
    }
    
    public CameraSelector getAvailableCamerasLimiter(final CameraSelector cameraSelector) {
        return this.mConfig.retrieveOption(CameraXConfig.OPTION_AVAILABLE_CAMERAS_LIMITER, cameraSelector);
    }
    
    public Executor getCameraExecutor(final Executor executor) {
        return this.mConfig.retrieveOption(CameraXConfig.OPTION_CAMERA_EXECUTOR, executor);
    }
    
    public CameraFactory.Provider getCameraFactoryProvider(final CameraFactory.Provider provider) {
        return this.mConfig.retrieveOption(CameraXConfig.OPTION_CAMERA_FACTORY_PROVIDER, provider);
    }
    
    @Override
    public Config getConfig() {
        return this.mConfig;
    }
    
    public CameraDeviceSurfaceManager.Provider getDeviceSurfaceManagerProvider(final CameraDeviceSurfaceManager.Provider provider) {
        return this.mConfig.retrieveOption(CameraXConfig.OPTION_DEVICE_SURFACE_MANAGER_PROVIDER, provider);
    }
    
    public int getMinimumLoggingLevel() {
        return this.mConfig.retrieveOption(CameraXConfig.OPTION_MIN_LOGGING_LEVEL, 3);
    }
    
    public Handler getSchedulerHandler(final Handler handler) {
        return this.mConfig.retrieveOption(CameraXConfig.OPTION_SCHEDULER_HANDLER, handler);
    }
    
    public UseCaseConfigFactory.Provider getUseCaseConfigFactoryProvider(final UseCaseConfigFactory.Provider provider) {
        return this.mConfig.retrieveOption(CameraXConfig.OPTION_USECASE_CONFIG_FACTORY_PROVIDER, provider);
    }
    
    public static final class Builder implements TargetConfig.Builder<CameraX, Builder>
    {
        private final MutableOptionsBundle mMutableConfig;
        
        public Builder() {
            this(MutableOptionsBundle.create());
        }
        
        private Builder(final MutableOptionsBundle mMutableConfig) {
            this.mMutableConfig = mMutableConfig;
            final Class<?> obj = mMutableConfig.retrieveOption(TargetConfig.OPTION_TARGET_CLASS, null);
            if (obj != null && !obj.equals(CameraX.class)) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Invalid target class configuration for ");
                sb.append(this);
                sb.append(": ");
                sb.append(obj);
                throw new IllegalArgumentException(sb.toString());
            }
            this.setTargetClass(CameraX.class);
        }
        
        public static Builder fromConfig(final CameraXConfig cameraXConfig) {
            return new Builder(MutableOptionsBundle.from(cameraXConfig));
        }
        
        private MutableConfig getMutableConfig() {
            return this.mMutableConfig;
        }
        
        public CameraXConfig build() {
            return new CameraXConfig(OptionsBundle.from(this.mMutableConfig));
        }
        
        public Builder setAvailableCamerasLimiter(final CameraSelector cameraSelector) {
            this.getMutableConfig().insertOption(CameraXConfig.OPTION_AVAILABLE_CAMERAS_LIMITER, cameraSelector);
            return this;
        }
        
        public Builder setCameraExecutor(final Executor executor) {
            this.getMutableConfig().insertOption(CameraXConfig.OPTION_CAMERA_EXECUTOR, executor);
            return this;
        }
        
        public Builder setCameraFactoryProvider(final CameraFactory.Provider provider) {
            this.getMutableConfig().insertOption(CameraXConfig.OPTION_CAMERA_FACTORY_PROVIDER, provider);
            return this;
        }
        
        public Builder setDeviceSurfaceManagerProvider(final CameraDeviceSurfaceManager.Provider provider) {
            this.getMutableConfig().insertOption(CameraXConfig.OPTION_DEVICE_SURFACE_MANAGER_PROVIDER, provider);
            return this;
        }
        
        public Builder setMinimumLoggingLevel(final int i) {
            this.getMutableConfig().insertOption(CameraXConfig.OPTION_MIN_LOGGING_LEVEL, i);
            return this;
        }
        
        public Builder setSchedulerHandler(final Handler handler) {
            this.getMutableConfig().insertOption(CameraXConfig.OPTION_SCHEDULER_HANDLER, handler);
            return this;
        }
        
        public Builder setTargetClass(final Class<CameraX> clazz) {
            this.getMutableConfig().insertOption(TargetConfig.OPTION_TARGET_CLASS, clazz);
            if (this.getMutableConfig().retrieveOption(TargetConfig.OPTION_TARGET_NAME, null) == null) {
                final StringBuilder sb = new StringBuilder();
                sb.append(clazz.getCanonicalName());
                sb.append("-");
                sb.append(UUID.randomUUID());
                this.setTargetName(sb.toString());
            }
            return this;
        }
        
        public Builder setTargetName(final String s) {
            this.getMutableConfig().insertOption(TargetConfig.OPTION_TARGET_NAME, s);
            return this;
        }
        
        public Builder setUseCaseConfigFactoryProvider(final UseCaseConfigFactory.Provider provider) {
            this.getMutableConfig().insertOption(CameraXConfig.OPTION_USECASE_CONFIG_FACTORY_PROVIDER, provider);
            return this;
        }
    }
    
    public interface Provider
    {
        CameraXConfig getCameraXConfig();
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core;

import androidx.camera.core.impl.ReadableConfig;
import android.net.Uri;
import java.io.OutputStream;
import java.io.File;
import android.content.ContentValues;
import android.content.ContentResolver;
import android.location.Location;
import java.util.Locale;
import java.util.ArrayDeque;
import java.util.Deque;
import java.nio.ByteBuffer;
import java.util.concurrent.RejectedExecutionException;
import java.io.IOException;
import java.io.InputStream;
import androidx.camera.core.impl.utils.Exif;
import java.io.ByteArrayInputStream;
import android.graphics.Matrix;
import java.util.concurrent.atomic.AtomicBoolean;
import androidx.camera.core.impl.ConfigProvider;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;
import java.util.UUID;
import androidx.camera.core.impl.OptionsBundle;
import androidx.camera.core.internal.TargetConfig;
import androidx.camera.core.impl.MutableOptionsBundle;
import androidx.camera.core.internal.IoConfig;
import androidx.camera.core.impl.ImageOutputConfig;
import android.os.Looper;
import androidx.arch.core.util.Function;
import androidx.camera.core.impl.utils.CameraOrientationUtil;
import androidx.camera.core.impl.Quirk;
import androidx.camera.core.internal.compat.quirk.SoftwareJpegEncodingPreferredQuirk;
import androidx.camera.core.impl.CameraInfoInternal;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.ThreadFactory;
import androidx.camera.core.impl.utils.futures.FutureCallback;
import java.util.Collections;
import java.util.Collection;
import java.util.ArrayList;
import androidx.camera.core.impl.Config$_CC;
import androidx.camera.core.impl.UseCaseConfigFactory;
import androidx.camera.camera2.internal.ZslControlImpl$$ExternalSyntheticLambda1;
import androidx.camera.core.impl.ImmediateSurface;
import android.view.Surface;
import androidx.camera.core.impl.TagBundle;
import androidx.camera.core.impl.MutableTagBundle;
import android.media.ImageReader;
import java.util.concurrent.CancellationException;
import androidx.camera.core.imagecapture.TakePictureRequest;
import androidx.concurrent.futures.CallbackToFutureAdapter;
import androidx.camera.core.internal.YuvToJpegProcessor;
import androidx.camera.core.impl.SessionProcessor;
import androidx.camera.core.impl.ImageInputConfig;
import java.util.Iterator;
import android.util.Pair;
import androidx.camera.core.impl.utils.TransformUtils;
import androidx.camera.core.impl.CameraInternal;
import androidx.camera.core.impl.CaptureStage;
import androidx.camera.core.impl.MutableConfig;
import android.os.Build$VERSION;
import java.util.Objects;
import androidx.camera.core.internal.utils.ImageUtil;
import android.util.Size;
import android.graphics.Rect;
import androidx.camera.core.impl.utils.Threads;
import android.util.Log;
import androidx.core.util.Preconditions;
import androidx.camera.core.impl.utils.executor.CameraXExecutors;
import androidx.camera.core.impl.Config;
import java.util.List;
import androidx.camera.core.impl.utils.futures.Futures;
import androidx.camera.core.impl.UseCaseConfig;
import androidx.camera.core.impl.ImageCaptureConfig;
import androidx.camera.core.imagecapture.TakePictureManager;
import androidx.camera.core.impl.SessionConfig;
import androidx.camera.core.impl.CameraCaptureCallback;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.Executor;
import com.google.common.util.concurrent.ListenableFuture;
import androidx.camera.core.imagecapture.ImagePipeline;
import androidx.camera.core.imagecapture.ImageCaptureControl;
import java.util.concurrent.ExecutorService;
import androidx.camera.core.impl.DeferrableSurface;
import android.util.Rational;
import androidx.camera.core.impl.ImageReaderProxy;
import androidx.camera.core.impl.CaptureProcessor;
import androidx.camera.core.impl.CaptureConfig;
import androidx.camera.core.impl.CaptureBundle;
import androidx.camera.core.internal.compat.workaround.ExifRotationAvailability;

public final class ImageCapture extends UseCase
{
    public static final int CAPTURE_MODE_MAXIMIZE_QUALITY = 0;
    public static final int CAPTURE_MODE_MINIMIZE_LATENCY = 1;
    public static final int CAPTURE_MODE_ZERO_SHUTTER_LAG = 2;
    private static final int DEFAULT_CAPTURE_MODE = 1;
    public static final Defaults DEFAULT_CONFIG;
    private static final int DEFAULT_FLASH_MODE = 2;
    public static final int ERROR_CAMERA_CLOSED = 3;
    public static final int ERROR_CAPTURE_FAILED = 2;
    public static final int ERROR_FILE_IO = 1;
    public static final int ERROR_INVALID_CAMERA = 4;
    public static final int ERROR_UNKNOWN = 0;
    static final ExifRotationAvailability EXIF_ROTATION_AVAILABILITY;
    public static final int FLASH_MODE_AUTO = 0;
    public static final int FLASH_MODE_OFF = 2;
    public static final int FLASH_MODE_ON = 1;
    private static final int FLASH_MODE_UNKNOWN = -1;
    public static final int FLASH_TYPE_ONE_SHOT_FLASH = 0;
    public static final int FLASH_TYPE_USE_TORCH_AS_FLASH = 1;
    private static final byte JPEG_QUALITY_MAXIMIZE_QUALITY_MODE = 100;
    private static final byte JPEG_QUALITY_MINIMIZE_LATENCY_MODE = 95;
    private static final int MAX_IMAGES = 2;
    private static final String TAG = "ImageCapture";
    private CaptureBundle mCaptureBundle;
    private CaptureConfig mCaptureConfig;
    private final int mCaptureMode;
    private CaptureProcessor mCaptureProcessor;
    private final ImageReaderProxy.OnImageAvailableListener mClosingListener;
    private Rational mCropAspectRatio;
    private DeferrableSurface mDeferrableSurface;
    private ExecutorService mExecutor;
    private int mFlashMode;
    private final int mFlashType;
    private final ImageCaptureControl mImageCaptureControl;
    private ImageCaptureRequestProcessor mImageCaptureRequestProcessor;
    private ImagePipeline mImagePipeline;
    SafeCloseImageReaderProxy mImageReader;
    private ListenableFuture<Void> mImageReaderCloseFuture;
    final Executor mIoExecutor;
    private final AtomicReference<Integer> mLockedFlashMode;
    private int mMaxCaptureStages;
    private CameraCaptureCallback mMetadataMatchingCaptureCallback;
    ProcessingImageReader mProcessingImageReader;
    final Executor mSequentialIoExecutor;
    SessionConfig.Builder mSessionConfigBuilder;
    private TakePictureManager mTakePictureManager;
    boolean mUseProcessingPipeline;
    private boolean mUseSoftwareJpeg;
    
    static {
        DEFAULT_CONFIG = new Defaults();
        EXIF_ROTATION_AVAILABILITY = new ExifRotationAvailability();
    }
    
    ImageCapture(ImageCaptureConfig imageCaptureConfig) {
        super(imageCaptureConfig);
        this.mUseProcessingPipeline = false;
        this.mClosingListener = new ImageCapture$$ExternalSyntheticLambda0();
        this.mLockedFlashMode = new AtomicReference<Integer>(null);
        this.mFlashMode = -1;
        this.mCropAspectRatio = null;
        this.mUseSoftwareJpeg = false;
        this.mImageReaderCloseFuture = Futures.immediateFuture((Void)null);
        this.mImageCaptureControl = new ImageCaptureControl() {
            final ImageCapture this$0;
            
            @Override
            public void lockFlashMode() {
                this.this$0.lockFlashMode();
            }
            
            @Override
            public ListenableFuture<Void> submitStillCaptureRequests(final List<CaptureConfig> list) {
                return this.this$0.submitStillCaptureRequest(list);
            }
            
            @Override
            public void unlockFlashMode() {
                this.this$0.unlockFlashMode();
            }
        };
        imageCaptureConfig = (ImageCaptureConfig)this.getCurrentConfig();
        if (imageCaptureConfig.containsOption(ImageCaptureConfig.OPTION_IMAGE_CAPTURE_MODE)) {
            this.mCaptureMode = imageCaptureConfig.getCaptureMode();
        }
        else {
            this.mCaptureMode = 1;
        }
        this.mFlashType = imageCaptureConfig.getFlashType(0);
        final Executor mIoExecutor = Preconditions.checkNotNull(imageCaptureConfig.getIoExecutor(CameraXExecutors.ioExecutor()));
        this.mIoExecutor = mIoExecutor;
        this.mSequentialIoExecutor = CameraXExecutors.newSequentialExecutor(mIoExecutor);
    }
    
    private void abortImageCaptureRequests() {
        if (this.mImageCaptureRequestProcessor != null) {
            this.mImageCaptureRequestProcessor.cancelRequests(new CameraClosedException("Camera is closed."));
        }
    }
    
    private void clearPipelineWithNode() {
        Log.d("ImageCapture", "clearPipelineWithNode");
        Threads.checkMainThread();
        this.mImagePipeline.close();
        this.mImagePipeline = null;
        this.mTakePictureManager.abortRequests();
        this.mTakePictureManager = null;
    }
    
    static Rect computeDispatchCropRect(final Rect rect, final Rational rational, final int n, final Size size, final int n2) {
        if (rect != null) {
            return ImageUtil.computeCropRectFromDispatchInfo(rect, n, size, n2);
        }
        if (rational != null) {
            Rational rational2 = rational;
            if (n2 % 180 != 0) {
                rational2 = new Rational(rational.getDenominator(), rational.getNumerator());
            }
            if (ImageUtil.isAspectRatioValid(size, rational2)) {
                return Objects.requireNonNull(ImageUtil.computeCropRectFromAspectRatio(size, rational2));
            }
        }
        return new Rect(0, 0, size.getWidth(), size.getHeight());
    }
    
    private SessionConfig.Builder createPipelineWithNode(final String s, final ImageCaptureConfig imageCaptureConfig, final Size size) {
        Threads.checkMainThread();
        final boolean b = false;
        Log.d("ImageCapture", String.format("createPipelineWithNode(cameraId: %s, resolution: %s)", s, size));
        Preconditions.checkState(this.mImagePipeline == null);
        this.mImagePipeline = new ImagePipeline(imageCaptureConfig, size);
        boolean b2 = b;
        if (this.mTakePictureManager == null) {
            b2 = true;
        }
        Preconditions.checkState(b2);
        this.mTakePictureManager = new TakePictureManager(this.mImageCaptureControl, this.mImagePipeline);
        final SessionConfig.Builder sessionConfigBuilder = this.mImagePipeline.createSessionConfigBuilder();
        if (Build$VERSION.SDK_INT >= 23 && this.getCaptureMode() == 2) {
            this.getCameraControl().addZslConfig(sessionConfigBuilder);
        }
        sessionConfigBuilder.addErrorListener(new ImageCapture$$ExternalSyntheticLambda6(this, s));
        return sessionConfigBuilder;
    }
    
    static boolean enforceSoftwareJpegConstraints(final MutableConfig mutableConfig) {
        final Boolean true = Boolean.TRUE;
        final Config.Option<Boolean> option_USE_SOFTWARE_JPEG_ENCODER = ImageCaptureConfig.OPTION_USE_SOFTWARE_JPEG_ENCODER;
        boolean b = false;
        final boolean b2 = false;
        final Boolean value = false;
        if (true.equals(mutableConfig.retrieveOption(option_USE_SOFTWARE_JPEG_ENCODER, value))) {
            boolean b3 = true;
            if (Build$VERSION.SDK_INT < 26) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Software JPEG only supported on API 26+, but current API level is ");
                sb.append(Build$VERSION.SDK_INT);
                Logger.w("ImageCapture", sb.toString());
                b3 = false;
            }
            final Integer n = mutableConfig.retrieveOption(ImageCaptureConfig.OPTION_BUFFER_FORMAT, null);
            if (n != null && n != 256) {
                Logger.w("ImageCapture", "Software JPEG cannot be used with non-JPEG output buffer format.");
                b3 = b2;
            }
            if (!(b = b3)) {
                Logger.w("ImageCapture", "Unable to support software JPEG. Disabling.");
                mutableConfig.insertOption(ImageCaptureConfig.OPTION_USE_SOFTWARE_JPEG_ENCODER, value);
                b = b3;
            }
        }
        return b;
    }
    
    private CaptureBundle getCaptureBundle(final CaptureBundle captureBundle) {
        final List<CaptureStage> captureStages = this.mCaptureBundle.getCaptureStages();
        CaptureBundle captureBundle2 = captureBundle;
        if (captureStages != null) {
            if (captureStages.isEmpty()) {
                captureBundle2 = captureBundle;
            }
            else {
                captureBundle2 = CaptureBundles.createCaptureBundle(captureStages);
            }
        }
        return captureBundle2;
    }
    
    private int getCaptureStageSize(final ImageCaptureConfig imageCaptureConfig) {
        final CaptureBundle captureBundle = imageCaptureConfig.getCaptureBundle(null);
        if (captureBundle == null) {
            return 1;
        }
        final List<CaptureStage> captureStages = captureBundle.getCaptureStages();
        if (captureStages == null) {
            return 1;
        }
        return captureStages.size();
    }
    
    static int getError(final Throwable t) {
        if (t instanceof CameraClosedException) {
            return 3;
        }
        if (t instanceof ImageCaptureException) {
            return ((ImageCaptureException)t).getImageCaptureError();
        }
        return 0;
    }
    
    private int getJpegQualityForImageCaptureRequest(final CameraInternal cameraInternal, final boolean b) {
        int n;
        if (b) {
            final int relativeRotation = this.getRelativeRotation(cameraInternal);
            final Size size = Objects.requireNonNull(this.getAttachedSurfaceResolution());
            final Rect computeDispatchCropRect = computeDispatchCropRect(this.getViewPortCropRect(), this.mCropAspectRatio, relativeRotation, size, relativeRotation);
            if (ImageUtil.shouldCropImage(size.getWidth(), size.getHeight(), computeDispatchCropRect.width(), computeDispatchCropRect.height())) {
                if (this.mCaptureMode == 0) {
                    n = 100;
                }
                else {
                    n = 95;
                }
            }
            else {
                n = this.getJpegQualityInternal();
            }
        }
        else {
            n = this.getJpegQualityInternal();
        }
        return n;
    }
    
    private int getJpegQualityInternal() {
        final ImageCaptureConfig imageCaptureConfig = (ImageCaptureConfig)this.getCurrentConfig();
        if (imageCaptureConfig.containsOption(ImageCaptureConfig.OPTION_JPEG_COMPRESSION_QUALITY)) {
            return imageCaptureConfig.getJpegQuality();
        }
        final int mCaptureMode = this.mCaptureMode;
        if (mCaptureMode == 0) {
            return 100;
        }
        if (mCaptureMode != 1 && mCaptureMode != 2) {
            final StringBuilder sb = new StringBuilder();
            sb.append("CaptureMode ");
            sb.append(this.mCaptureMode);
            sb.append(" is invalid");
            throw new IllegalStateException(sb.toString());
        }
        return 95;
    }
    
    private Rect getTakePictureCropRect() {
        final Rect viewPortCropRect = this.getViewPortCropRect();
        final Size size = Objects.requireNonNull(this.getAttachedSurfaceResolution());
        if (viewPortCropRect != null) {
            return viewPortCropRect;
        }
        if (ImageUtil.isAspectRatioValid(this.mCropAspectRatio)) {
            final int relativeRotation = this.getRelativeRotation(Objects.requireNonNull(this.getCamera()));
            Rational mCropAspectRatio = new Rational(this.mCropAspectRatio.getDenominator(), this.mCropAspectRatio.getNumerator());
            if (!TransformUtils.is90or270(relativeRotation)) {
                mCropAspectRatio = this.mCropAspectRatio;
            }
            return Objects.requireNonNull(ImageUtil.computeCropRectFromAspectRatio(size, mCropAspectRatio));
        }
        return new Rect(0, 0, size.getWidth(), size.getHeight());
    }
    
    private static boolean isImageFormatSupported(final List<Pair<Integer, Size[]>> list, final int i) {
        if (list == null) {
            return false;
        }
        final Iterator<Pair<Integer, Size[]>> iterator = list.iterator();
        while (iterator.hasNext()) {
            if (((Integer)iterator.next().first).equals(i)) {
                return true;
            }
        }
        return false;
    }
    
    private boolean isNodeEnabled() {
        Threads.checkMainThread();
        final ImageCaptureConfig imageCaptureConfig = (ImageCaptureConfig)this.getCurrentConfig();
        return imageCaptureConfig.getImageReaderProxyProvider() == null && !this.isSessionProcessorEnabledInCurrentCamera() && this.mCaptureProcessor == null && this.getCaptureStageSize(imageCaptureConfig) <= 1 && Objects.requireNonNull(imageCaptureConfig.retrieveOption(ImageInputConfig.OPTION_INPUT_FORMAT, 256)) == 256 && this.mUseProcessingPipeline;
    }
    
    private boolean isSessionProcessorEnabledInCurrentCamera() {
        final CameraInternal camera = this.getCamera();
        boolean b = false;
        if (camera == null) {
            return false;
        }
        if (this.getCamera().getExtendedConfig().getSessionProcessor(null) != null) {
            b = true;
        }
        return b;
    }
    
    private void sendImageCaptureRequest(final Executor executor, final OnImageCapturedCallback onImageCapturedCallback, final boolean b) {
        final CameraInternal camera = this.getCamera();
        if (camera == null) {
            executor.execute(new ImageCapture$$ExternalSyntheticLambda12(this, onImageCapturedCallback));
            return;
        }
        final ImageCaptureRequestProcessor mImageCaptureRequestProcessor = this.mImageCaptureRequestProcessor;
        if (mImageCaptureRequestProcessor == null) {
            executor.execute(new ImageCapture$$ExternalSyntheticLambda13(onImageCapturedCallback));
            return;
        }
        mImageCaptureRequestProcessor.sendRequest(new ImageCaptureRequest(this.getRelativeRotation(camera), this.getJpegQualityForImageCaptureRequest(camera, b), this.mCropAspectRatio, this.getViewPortCropRect(), this.getSensorToBufferTransformMatrix(), executor, onImageCapturedCallback));
    }
    
    private void sendInvalidCameraError(final Executor executor, final OnImageCapturedCallback onImageCapturedCallback, final OnImageSavedCallback onImageSavedCallback) {
        final StringBuilder sb = new StringBuilder();
        sb.append("Not bound to a valid Camera [");
        sb.append(this);
        sb.append("]");
        final ImageCaptureException ex = new ImageCaptureException(4, sb.toString(), null);
        if (onImageCapturedCallback != null) {
            onImageCapturedCallback.onError(ex);
        }
        else {
            if (onImageSavedCallback == null) {
                throw new IllegalArgumentException("Must have either in-memory or on-disk callback.");
            }
            onImageSavedCallback.onError(ex);
        }
    }
    
    private ListenableFuture<ImageProxy> takePictureInternal(final ImageCaptureRequest imageCaptureRequest) {
        return CallbackToFutureAdapter.getFuture((CallbackToFutureAdapter.Resolver<ImageProxy>)new ImageCapture$$ExternalSyntheticLambda5(this, imageCaptureRequest));
    }
    
    private void takePictureWithNode(final Executor executor, final OnImageCapturedCallback onImageCapturedCallback, final OnImageSavedCallback onImageSavedCallback, final OutputFileOptions outputFileOptions) {
        Threads.checkMainThread();
        Log.d("ImageCapture", "takePictureWithNode");
        final CameraInternal camera = this.getCamera();
        if (camera == null) {
            this.sendInvalidCameraError(executor, onImageCapturedCallback, onImageSavedCallback);
            return;
        }
        this.mTakePictureManager.offerRequest(TakePictureRequest.of(executor, onImageCapturedCallback, onImageSavedCallback, outputFileOptions, this.getTakePictureCropRect(), this.getSensorToBufferTransformMatrix(), this.getRelativeRotation(camera), this.getJpegQualityInternal(), this.getCaptureMode(), this.mSessionConfigBuilder.getSingleCameraCaptureCallbacks()));
    }
    
    private void trySetFlashModeToCameraControl() {
        synchronized (this.mLockedFlashMode) {
            if (this.mLockedFlashMode.get() != null) {
                return;
            }
            this.getCameraControl().setFlashMode(this.getFlashMode());
        }
    }
    
    void clearPipeline() {
        Threads.checkMainThread();
        if (this.isNodeEnabled()) {
            this.clearPipelineWithNode();
            return;
        }
        final ImageCaptureRequestProcessor mImageCaptureRequestProcessor = this.mImageCaptureRequestProcessor;
        if (mImageCaptureRequestProcessor != null) {
            mImageCaptureRequestProcessor.cancelRequests(new CancellationException("Request is canceled."));
            this.mImageCaptureRequestProcessor = null;
        }
        final DeferrableSurface mDeferrableSurface = this.mDeferrableSurface;
        this.mDeferrableSurface = null;
        this.mImageReader = null;
        this.mProcessingImageReader = null;
        this.mImageReaderCloseFuture = Futures.immediateFuture((Void)null);
        if (mDeferrableSurface != null) {
            mDeferrableSurface.close();
        }
    }
    
    SessionConfig.Builder createPipeline(final String s, final ImageCaptureConfig imageCaptureConfig, final Size size) {
        Threads.checkMainThread();
        if (this.isNodeEnabled()) {
            return this.createPipelineWithNode(s, imageCaptureConfig, size);
        }
        final SessionConfig.Builder from = SessionConfig.Builder.createFrom(imageCaptureConfig);
        if (Build$VERSION.SDK_INT >= 23 && this.getCaptureMode() == 2) {
            this.getCameraControl().addZslConfig(from);
        }
        YuvToJpegProcessor yuvToJpegProcessor = null;
        Label_0643: {
            if (imageCaptureConfig.getImageReaderProxyProvider() != null) {
                this.mImageReader = new SafeCloseImageReaderProxy(imageCaptureConfig.getImageReaderProxyProvider().newInstance(size.getWidth(), size.getHeight(), this.getImageFormat(), 2, 0L));
                this.mMetadataMatchingCaptureCallback = new CameraCaptureCallback(this) {
                    final ImageCapture this$0;
                };
            }
            else {
                if (this.isSessionProcessorEnabledInCurrentCamera()) {
                    ImageReaderProxy build;
                    if (this.getImageFormat() == 256) {
                        build = new AndroidImageReaderProxy(ImageReader.newInstance(size.getWidth(), size.getHeight(), this.getImageFormat(), 2));
                        yuvToJpegProcessor = null;
                    }
                    else {
                        if (this.getImageFormat() != 35) {
                            final StringBuilder sb = new StringBuilder();
                            sb.append("Unsupported image format:");
                            sb.append(this.getImageFormat());
                            throw new IllegalArgumentException(sb.toString());
                        }
                        if (Build$VERSION.SDK_INT < 26) {
                            throw new UnsupportedOperationException("Does not support API level < 26");
                        }
                        yuvToJpegProcessor = new YuvToJpegProcessor(this.getJpegQualityInternal(), 2);
                        final ModifiableImageReaderProxy modifiableImageReaderProxy = new ModifiableImageReaderProxy(ImageReader.newInstance(size.getWidth(), size.getHeight(), 35, 2));
                        final CaptureBundle singleDefaultCaptureBundle = CaptureBundles.singleDefaultCaptureBundle();
                        build = new ProcessingImageReader.Builder(modifiableImageReaderProxy, singleDefaultCaptureBundle, yuvToJpegProcessor).setPostProcessExecutor(this.mExecutor).setOutputFormat(256).build();
                        final MutableTagBundle create = MutableTagBundle.create();
                        create.putTag(((ProcessingImageReader)build).getTagBundleKey(), singleDefaultCaptureBundle.getCaptureStages().get(0).getId());
                        modifiableImageReaderProxy.setImageTagBundle(create);
                    }
                    this.mMetadataMatchingCaptureCallback = new CameraCaptureCallback(this) {
                        final ImageCapture this$0;
                    };
                    this.mImageReader = new SafeCloseImageReaderProxy(build);
                    break Label_0643;
                }
                CaptureProcessor mCaptureProcessor = this.mCaptureProcessor;
                if (mCaptureProcessor != null || this.mUseSoftwareJpeg) {
                    final int imageFormat = this.getImageFormat();
                    int imageFormat2 = this.getImageFormat();
                    if (this.mUseSoftwareJpeg) {
                        if (Build$VERSION.SDK_INT < 26) {
                            throw new IllegalStateException("Software JPEG only supported on API 26+");
                        }
                        Logger.i("ImageCapture", "Using software JPEG encoder.");
                        if (this.mCaptureProcessor != null) {
                            yuvToJpegProcessor = new YuvToJpegProcessor(this.getJpegQualityInternal(), this.mMaxCaptureStages);
                            mCaptureProcessor = new CaptureProcessorPipeline(this.mCaptureProcessor, this.mMaxCaptureStages, yuvToJpegProcessor, this.mExecutor);
                        }
                        else {
                            yuvToJpegProcessor = (YuvToJpegProcessor)(mCaptureProcessor = new YuvToJpegProcessor(this.getJpegQualityInternal(), this.mMaxCaptureStages));
                        }
                        imageFormat2 = 256;
                    }
                    else {
                        yuvToJpegProcessor = null;
                    }
                    final ProcessingImageReader build2 = new ProcessingImageReader.Builder(size.getWidth(), size.getHeight(), imageFormat, this.mMaxCaptureStages, this.getCaptureBundle(CaptureBundles.singleDefaultCaptureBundle()), mCaptureProcessor).setPostProcessExecutor(this.mExecutor).setOutputFormat(imageFormat2).build();
                    this.mProcessingImageReader = build2;
                    this.mMetadataMatchingCaptureCallback = build2.getCameraCaptureCallback();
                    this.mImageReader = new SafeCloseImageReaderProxy(this.mProcessingImageReader);
                    break Label_0643;
                }
                final MetadataImageReader metadataImageReader = new MetadataImageReader(size.getWidth(), size.getHeight(), this.getImageFormat(), 2);
                this.mMetadataMatchingCaptureCallback = metadataImageReader.getCameraCaptureCallback();
                this.mImageReader = new SafeCloseImageReaderProxy(metadataImageReader);
            }
            yuvToJpegProcessor = null;
        }
        final ImageCaptureRequestProcessor mImageCaptureRequestProcessor = this.mImageCaptureRequestProcessor;
        if (mImageCaptureRequestProcessor != null) {
            mImageCaptureRequestProcessor.cancelRequests(new CancellationException("Request is canceled."));
        }
        final ImageCapture$$ExternalSyntheticLambda2 imageCapture$$ExternalSyntheticLambda2 = new ImageCapture$$ExternalSyntheticLambda2(this);
        Object o;
        if (yuvToJpegProcessor == null) {
            o = null;
        }
        else {
            o = new ImageCapture$$ExternalSyntheticLambda3(yuvToJpegProcessor);
        }
        this.mImageCaptureRequestProcessor = new ImageCaptureRequestProcessor(2, (ImageCaptor)imageCapture$$ExternalSyntheticLambda2, (RequestProcessCallback)o);
        this.mImageReader.setOnImageAvailableListener(this.mClosingListener, CameraXExecutors.mainThreadExecutor());
        final DeferrableSurface mDeferrableSurface = this.mDeferrableSurface;
        if (mDeferrableSurface != null) {
            mDeferrableSurface.close();
        }
        this.mDeferrableSurface = new ImmediateSurface(Objects.requireNonNull(this.mImageReader.getSurface()), new Size(this.mImageReader.getWidth(), this.mImageReader.getHeight()), this.getImageFormat());
        final ProcessingImageReader mProcessingImageReader = this.mProcessingImageReader;
        ListenableFuture<Void> mImageReaderCloseFuture;
        if (mProcessingImageReader != null) {
            mImageReaderCloseFuture = mProcessingImageReader.getCloseFuture();
        }
        else {
            mImageReaderCloseFuture = Futures.immediateFuture((Void)null);
        }
        this.mImageReaderCloseFuture = mImageReaderCloseFuture;
        final ListenableFuture<Void> terminationFuture = this.mDeferrableSurface.getTerminationFuture();
        final SafeCloseImageReaderProxy mImageReader = this.mImageReader;
        Objects.requireNonNull(mImageReader);
        terminationFuture.addListener((Runnable)new ZslControlImpl$$ExternalSyntheticLambda1(mImageReader), (Executor)CameraXExecutors.mainThreadExecutor());
        from.addNonRepeatingSurface(this.mDeferrableSurface);
        from.addErrorListener(new ImageCapture$$ExternalSyntheticLambda4(this, s, imageCaptureConfig, size));
        return from;
    }
    
    public int getCaptureMode() {
        return this.mCaptureMode;
    }
    
    @Override
    public UseCaseConfig<?> getDefaultConfig(final boolean b, final UseCaseConfigFactory useCaseConfigFactory) {
        Config config2;
        final Config config = config2 = useCaseConfigFactory.getConfig(UseCaseConfigFactory.CaptureType.IMAGE_CAPTURE, this.getCaptureMode());
        if (b) {
            config2 = Config$_CC.mergeConfigs(config, ImageCapture.DEFAULT_CONFIG.getConfig());
        }
        Object useCaseConfig;
        if (config2 == null) {
            useCaseConfig = null;
        }
        else {
            useCaseConfig = this.getUseCaseConfigBuilder(config2).getUseCaseConfig();
        }
        return (UseCaseConfig<?>)useCaseConfig;
    }
    
    public int getFlashMode() {
        synchronized (this.mLockedFlashMode) {
            int n = this.mFlashMode;
            if (n == -1) {
                n = ((ImageCaptureConfig)this.getCurrentConfig()).getFlashMode(2);
            }
            return n;
        }
    }
    
    public int getJpegQuality() {
        return this.getJpegQualityInternal();
    }
    
    @Override
    public ResolutionInfo getResolutionInfo() {
        return super.getResolutionInfo();
    }
    
    @Override
    protected ResolutionInfo getResolutionInfoInternal() {
        final CameraInternal camera = this.getCamera();
        final Size attachedSurfaceResolution = this.getAttachedSurfaceResolution();
        if (camera != null && attachedSurfaceResolution != null) {
            final Rect viewPortCropRect = this.getViewPortCropRect();
            final Rational mCropAspectRatio = this.mCropAspectRatio;
            Rect computeCropRectFromAspectRatio;
            if ((computeCropRectFromAspectRatio = viewPortCropRect) == null) {
                if (mCropAspectRatio != null) {
                    computeCropRectFromAspectRatio = ImageUtil.computeCropRectFromAspectRatio(attachedSurfaceResolution, mCropAspectRatio);
                }
                else {
                    computeCropRectFromAspectRatio = new Rect(0, 0, attachedSurfaceResolution.getWidth(), attachedSurfaceResolution.getHeight());
                }
            }
            return ResolutionInfo.create(attachedSurfaceResolution, Objects.requireNonNull(computeCropRectFromAspectRatio), this.getRelativeRotation(camera));
        }
        return null;
    }
    
    public int getTargetRotation() {
        return this.getTargetRotationInternal();
    }
    
    @Override
    public UseCaseConfig.Builder<?, ?, ?> getUseCaseConfigBuilder(final Config config) {
        return Builder.fromConfig(config);
    }
    
    boolean isProcessingPipelineEnabled() {
        return this.mImagePipeline != null && this.mTakePictureManager != null;
    }
    
    ListenableFuture<Void> issueTakePicture(final ImageCaptureRequest imageCaptureRequest) {
        Logger.d("ImageCapture", "issueTakePicture");
        final ArrayList list = new ArrayList();
        CaptureBundle captureBundle;
        String tagBundleKey;
        if (this.mProcessingImageReader != null) {
            captureBundle = this.getCaptureBundle(CaptureBundles.singleDefaultCaptureBundle());
            if (captureBundle == null) {
                return Futures.immediateFailedFuture(new IllegalArgumentException("ImageCapture cannot set empty CaptureBundle."));
            }
            final List<CaptureStage> captureStages = captureBundle.getCaptureStages();
            if (captureStages == null) {
                return Futures.immediateFailedFuture(new IllegalArgumentException("ImageCapture has CaptureBundle with null capture stages"));
            }
            if (this.mCaptureProcessor == null && captureStages.size() > 1) {
                return Futures.immediateFailedFuture(new IllegalArgumentException("No CaptureProcessor can be found to process the images captured for multiple CaptureStages."));
            }
            if (captureStages.size() > this.mMaxCaptureStages) {
                return Futures.immediateFailedFuture(new IllegalArgumentException("ImageCapture has CaptureStages > Max CaptureStage size"));
            }
            this.mProcessingImageReader.setCaptureBundle(captureBundle);
            this.mProcessingImageReader.setOnProcessingErrorCallback(CameraXExecutors.directExecutor(), (ProcessingImageReader.OnProcessingErrorCallback)new ImageCapture$$ExternalSyntheticLambda7(imageCaptureRequest));
            tagBundleKey = this.mProcessingImageReader.getTagBundleKey();
        }
        else {
            captureBundle = this.getCaptureBundle(CaptureBundles.singleDefaultCaptureBundle());
            if (captureBundle == null) {
                return Futures.immediateFailedFuture(new IllegalArgumentException("ImageCapture cannot set empty CaptureBundle."));
            }
            final List<CaptureStage> captureStages2 = captureBundle.getCaptureStages();
            if (captureStages2 == null) {
                return Futures.immediateFailedFuture(new IllegalArgumentException("ImageCapture has CaptureBundle with null capture stages"));
            }
            if (captureStages2.size() > 1) {
                return Futures.immediateFailedFuture(new IllegalArgumentException("ImageCapture have no CaptureProcess set with CaptureBundle size > 1."));
            }
            tagBundleKey = null;
        }
        for (final CaptureStage captureStage : captureBundle.getCaptureStages()) {
            final CaptureConfig.Builder builder = new CaptureConfig.Builder();
            builder.setTemplateType(this.mCaptureConfig.getTemplateType());
            builder.addImplementationOptions(this.mCaptureConfig.getImplementationOptions());
            builder.addAllCameraCaptureCallbacks(this.mSessionConfigBuilder.getSingleCameraCaptureCallbacks());
            builder.addSurface(this.mDeferrableSurface);
            if (this.getImageFormat() == 256) {
                if (ImageCapture.EXIF_ROTATION_AVAILABILITY.isRotationOptionSupported()) {
                    builder.addImplementationOption(CaptureConfig.OPTION_ROTATION, imageCaptureRequest.mRotationDegrees);
                }
                builder.addImplementationOption(CaptureConfig.OPTION_JPEG_QUALITY, imageCaptureRequest.mJpegQuality);
            }
            builder.addImplementationOptions(captureStage.getCaptureConfig().getImplementationOptions());
            if (tagBundleKey != null) {
                builder.addTag(tagBundleKey, captureStage.getId());
            }
            builder.addCameraCaptureCallback(this.mMetadataMatchingCaptureCallback);
            list.add(builder.build());
        }
        return this.submitStillCaptureRequest(list);
    }
    
    void lockFlashMode() {
        synchronized (this.mLockedFlashMode) {
            if (this.mLockedFlashMode.get() != null) {
                return;
            }
            this.mLockedFlashMode.set(this.getFlashMode());
        }
    }
    
    @Override
    public void onAttached() {
        final ImageCaptureConfig imageCaptureConfig = (ImageCaptureConfig)this.getCurrentConfig();
        this.mCaptureConfig = CaptureConfig.Builder.createFrom(imageCaptureConfig).build();
        this.mCaptureProcessor = imageCaptureConfig.getCaptureProcessor(null);
        this.mMaxCaptureStages = imageCaptureConfig.getMaxCaptureStages(2);
        this.mCaptureBundle = imageCaptureConfig.getCaptureBundle(CaptureBundles.singleDefaultCaptureBundle());
        this.mUseSoftwareJpeg = imageCaptureConfig.isSoftwareJpegEncoderRequested();
        Preconditions.checkNotNull(this.getCamera(), "Attached camera cannot be null");
        this.mExecutor = Executors.newFixedThreadPool(1, new ThreadFactory(this) {
            private final AtomicInteger mId = new AtomicInteger(0);
            final ImageCapture this$0;
            
            @Override
            public Thread newThread(final Runnable target) {
                final StringBuilder sb = new StringBuilder();
                sb.append("CameraX-image_capture_");
                sb.append(this.mId.getAndIncrement());
                return new Thread(target, sb.toString());
            }
        });
    }
    
    @Override
    protected void onCameraControlReady() {
        this.trySetFlashModeToCameraControl();
    }
    
    @Override
    public void onDetached() {
        final ListenableFuture<Void> mImageReaderCloseFuture = this.mImageReaderCloseFuture;
        this.abortImageCaptureRequests();
        this.clearPipeline();
        this.mUseSoftwareJpeg = false;
        final ExecutorService mExecutor = this.mExecutor;
        Objects.requireNonNull(mExecutor);
        mImageReaderCloseFuture.addListener((Runnable)new ImageCapture$$ExternalSyntheticLambda9(mExecutor), CameraXExecutors.directExecutor());
    }
    
    @Override
    protected UseCaseConfig<?> onMergeConfig(final CameraInfoInternal cameraInfoInternal, final UseCaseConfig.Builder<?, ?, ?> builder) {
        final CaptureProcessor retrieveOption = ((ReadableConfig)builder.getUseCaseConfig()).retrieveOption(ImageCaptureConfig.OPTION_CAPTURE_PROCESSOR, null);
        final boolean b = true;
        final Boolean value = true;
        if (retrieveOption != null && Build$VERSION.SDK_INT >= 29) {
            Logger.i("ImageCapture", "Requesting software JPEG due to a CaptureProcessor is set.");
            builder.getMutableConfig().insertOption(ImageCaptureConfig.OPTION_USE_SOFTWARE_JPEG_ENCODER, value);
        }
        else if (cameraInfoInternal.getCameraQuirks().contains(SoftwareJpegEncodingPreferredQuirk.class)) {
            if (Boolean.FALSE.equals(builder.getMutableConfig().retrieveOption(ImageCaptureConfig.OPTION_USE_SOFTWARE_JPEG_ENCODER, value))) {
                Logger.w("ImageCapture", "Device quirk suggests software JPEG encoder, but it has been explicitly disabled.");
            }
            else {
                Logger.i("ImageCapture", "Requesting software JPEG due to device quirk.");
                builder.getMutableConfig().insertOption(ImageCaptureConfig.OPTION_USE_SOFTWARE_JPEG_ENCODER, value);
            }
        }
        final boolean enforceSoftwareJpegConstraints = enforceSoftwareJpegConstraints(builder.getMutableConfig());
        final Integer n = builder.getMutableConfig().retrieveOption(ImageCaptureConfig.OPTION_BUFFER_FORMAT, null);
        int intValue = 35;
        if (n != null) {
            Preconditions.checkArgument(builder.getMutableConfig().retrieveOption(ImageCaptureConfig.OPTION_CAPTURE_PROCESSOR, null) == null, (Object)"Cannot set buffer format with CaptureProcessor defined.");
            final MutableConfig mutableConfig = builder.getMutableConfig();
            final Config.Option<Integer> option_INPUT_FORMAT = ImageInputConfig.OPTION_INPUT_FORMAT;
            if (!enforceSoftwareJpegConstraints) {
                intValue = n;
            }
            mutableConfig.insertOption(option_INPUT_FORMAT, intValue);
        }
        else if (builder.getMutableConfig().retrieveOption(ImageCaptureConfig.OPTION_CAPTURE_PROCESSOR, null) == null && !enforceSoftwareJpegConstraints) {
            final List list = builder.getMutableConfig().retrieveOption(ImageCaptureConfig.OPTION_SUPPORTED_RESOLUTIONS, (List)null);
            if (list == null) {
                builder.getMutableConfig().insertOption(ImageInputConfig.OPTION_INPUT_FORMAT, 256);
            }
            else if (isImageFormatSupported(list, 256)) {
                builder.getMutableConfig().insertOption(ImageInputConfig.OPTION_INPUT_FORMAT, 256);
            }
            else if (isImageFormatSupported(list, 35)) {
                builder.getMutableConfig().insertOption(ImageInputConfig.OPTION_INPUT_FORMAT, 35);
            }
        }
        else {
            builder.getMutableConfig().insertOption(ImageInputConfig.OPTION_INPUT_FORMAT, 35);
        }
        final Integer n2 = builder.getMutableConfig().retrieveOption(ImageCaptureConfig.OPTION_MAX_CAPTURE_STAGES, 2);
        Preconditions.checkNotNull(n2, "Maximum outstanding image count must be at least 1");
        Preconditions.checkArgument(n2 >= 1 && b, (Object)"Maximum outstanding image count must be at least 1");
        return (UseCaseConfig<?>)builder.getUseCaseConfig();
    }
    
    @Override
    public void onStateDetached() {
        this.abortImageCaptureRequests();
    }
    
    @Override
    protected Size onSuggestedResolutionUpdated(final Size size) {
        final SessionConfig.Builder pipeline = this.createPipeline(this.getCameraId(), (ImageCaptureConfig)this.getCurrentConfig(), size);
        this.mSessionConfigBuilder = pipeline;
        this.updateSessionConfig(pipeline.build());
        this.notifyActive();
        return size;
    }
    
    public void setCropAspectRatio(final Rational mCropAspectRatio) {
        this.mCropAspectRatio = mCropAspectRatio;
    }
    
    public void setFlashMode(final int n) {
        if (n != 0 && n != 1 && n != 2) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Invalid flash mode: ");
            sb.append(n);
            throw new IllegalArgumentException(sb.toString());
        }
        synchronized (this.mLockedFlashMode) {
            this.mFlashMode = n;
            this.trySetFlashModeToCameraControl();
        }
    }
    
    public void setTargetRotation(final int targetRotationInternal) {
        final int targetRotation = this.getTargetRotation();
        if (this.setTargetRotationInternal(targetRotationInternal) && this.mCropAspectRatio != null) {
            this.mCropAspectRatio = ImageUtil.getRotatedAspectRatio(Math.abs(CameraOrientationUtil.surfaceRotationToDegrees(targetRotationInternal) - CameraOrientationUtil.surfaceRotationToDegrees(targetRotation)), this.mCropAspectRatio);
        }
    }
    
    ListenableFuture<Void> submitStillCaptureRequest(final List<CaptureConfig> list) {
        Threads.checkMainThread();
        return Futures.transform(this.getCameraControl().submitStillCaptureRequests(list, this.mCaptureMode, this.mFlashType), (Function<? super List<Void>, ? extends Void>)new ImageCapture$$ExternalSyntheticLambda8(), CameraXExecutors.directExecutor());
    }
    
    public void takePicture(final OutputFileOptions outputFileOptions, final Executor executor, final OnImageSavedCallback onImageSavedCallback) {
        if (Looper.getMainLooper() != Looper.myLooper()) {
            CameraXExecutors.mainThreadExecutor().execute(new ImageCapture$$ExternalSyntheticLambda1(this, outputFileOptions, executor, onImageSavedCallback));
            return;
        }
        if (this.isNodeEnabled()) {
            this.takePictureWithNode(executor, null, onImageSavedCallback, outputFileOptions);
            return;
        }
        this.sendImageCaptureRequest(CameraXExecutors.mainThreadExecutor(), (OnImageCapturedCallback)new OnImageCapturedCallback(this, outputFileOptions, this.getJpegQualityInternal(), executor, new ImageSaver.OnImageSavedCallback(this, onImageSavedCallback) {
            final ImageCapture this$0;
            final ImageCapture.OnImageSavedCallback val$imageSavedCallback;
            
            @Override
            public void onError(final SaveError saveError, final String s, final Throwable t) {
                boolean b;
                if (saveError == SaveError.FILE_IO_FAILED) {
                    b = true;
                }
                else {
                    b = false;
                }
                this.val$imageSavedCallback.onError(new ImageCaptureException((int)(b ? 1 : 0), s, t));
            }
            
            @Override
            public void onImageSaved(final OutputFileResults outputFileResults) {
                this.val$imageSavedCallback.onImageSaved(outputFileResults);
            }
        }, onImageSavedCallback) {
            final ImageCapture this$0;
            final Executor val$executor;
            final OnImageSavedCallback val$imageSavedCallback;
            final ImageSaver.OnImageSavedCallback val$imageSavedCallbackWrapper;
            final OutputFileOptions val$outputFileOptions;
            final int val$outputJpegQuality;
            
            @Override
            public void onCaptureSuccess(final ImageProxy imageProxy) {
                this.this$0.mIoExecutor.execute(new ImageSaver(imageProxy, this.val$outputFileOptions, imageProxy.getImageInfo().getRotationDegrees(), this.val$outputJpegQuality, this.val$executor, this.this$0.mSequentialIoExecutor, this.val$imageSavedCallbackWrapper));
            }
            
            @Override
            public void onError(final ImageCaptureException ex) {
                this.val$imageSavedCallback.onError(ex);
            }
        }, true);
    }
    
    public void takePicture(final Executor executor, final OnImageCapturedCallback onImageCapturedCallback) {
        if (Looper.getMainLooper() != Looper.myLooper()) {
            CameraXExecutors.mainThreadExecutor().execute(new ImageCapture$$ExternalSyntheticLambda14(this, executor, onImageCapturedCallback));
            return;
        }
        if (this.isNodeEnabled()) {
            this.takePictureWithNode(executor, onImageCapturedCallback, null, null);
            return;
        }
        this.sendImageCaptureRequest(executor, onImageCapturedCallback, false);
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("ImageCapture:");
        sb.append(this.getName());
        return sb.toString();
    }
    
    void unlockFlashMode() {
        synchronized (this.mLockedFlashMode) {
            final Integer n = this.mLockedFlashMode.getAndSet(null);
            if (n == null) {
                return;
            }
            if (n != this.getFlashMode()) {
                this.trySetFlashModeToCameraControl();
            }
        }
    }
    
    public static final class Builder implements UseCaseConfig.Builder<ImageCapture, ImageCaptureConfig, Builder>, ImageOutputConfig.Builder<Builder>, IoConfig.Builder<Builder>
    {
        private final MutableOptionsBundle mMutableConfig;
        
        public Builder() {
            this(MutableOptionsBundle.create());
        }
        
        private Builder(final MutableOptionsBundle mMutableConfig) {
            this.mMutableConfig = mMutableConfig;
            final Class<?> obj = mMutableConfig.retrieveOption(TargetConfig.OPTION_TARGET_CLASS, null);
            if (obj != null && !obj.equals(ImageCapture.class)) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Invalid target class configuration for ");
                sb.append(this);
                sb.append(": ");
                sb.append(obj);
                throw new IllegalArgumentException(sb.toString());
            }
            this.setTargetClass(ImageCapture.class);
        }
        
        public static Builder fromConfig(final Config config) {
            return new Builder(MutableOptionsBundle.from(config));
        }
        
        static Builder fromConfig(final ImageCaptureConfig imageCaptureConfig) {
            return new Builder(MutableOptionsBundle.from(imageCaptureConfig));
        }
        
        @Override
        public ImageCapture build() {
            if (this.getMutableConfig().retrieveOption(ImageCaptureConfig.OPTION_TARGET_ASPECT_RATIO, (Object)null) != null && this.getMutableConfig().retrieveOption(ImageCaptureConfig.OPTION_TARGET_RESOLUTION, (Object)null) != null) {
                throw new IllegalArgumentException("Cannot use both setTargetResolution and setTargetAspectRatio on the same config.");
            }
            final Integer n = this.getMutableConfig().retrieveOption(ImageCaptureConfig.OPTION_BUFFER_FORMAT, null);
            final boolean b = false;
            if (n != null) {
                Preconditions.checkArgument(this.getMutableConfig().retrieveOption(ImageCaptureConfig.OPTION_CAPTURE_PROCESSOR, null) == null, (Object)"Cannot set buffer format with CaptureProcessor defined.");
                this.getMutableConfig().insertOption(ImageInputConfig.OPTION_INPUT_FORMAT, n);
            }
            else if (this.getMutableConfig().retrieveOption(ImageCaptureConfig.OPTION_CAPTURE_PROCESSOR, null) != null) {
                this.getMutableConfig().insertOption(ImageInputConfig.OPTION_INPUT_FORMAT, 35);
            }
            else {
                this.getMutableConfig().insertOption(ImageInputConfig.OPTION_INPUT_FORMAT, 256);
            }
            final ImageCapture imageCapture = new ImageCapture(this.getUseCaseConfig());
            final Size size = this.getMutableConfig().retrieveOption(ImageCaptureConfig.OPTION_TARGET_RESOLUTION, (Size)null);
            if (size != null) {
                imageCapture.setCropAspectRatio(new Rational(size.getWidth(), size.getHeight()));
            }
            final Integer n2 = this.getMutableConfig().retrieveOption(ImageCaptureConfig.OPTION_MAX_CAPTURE_STAGES, 2);
            Preconditions.checkNotNull(n2, "Maximum outstanding image count must be at least 1");
            boolean b2 = b;
            if (n2 >= 1) {
                b2 = true;
            }
            Preconditions.checkArgument(b2, (Object)"Maximum outstanding image count must be at least 1");
            Preconditions.checkNotNull((Object)this.getMutableConfig().retrieveOption(ImageCaptureConfig.OPTION_IO_EXECUTOR, (T)CameraXExecutors.ioExecutor()), "The IO executor can't be null");
            if (this.getMutableConfig().containsOption((Config.Option<?>)ImageCaptureConfig.OPTION_FLASH_MODE)) {
                final Integer obj = this.getMutableConfig().retrieveOption(ImageCaptureConfig.OPTION_FLASH_MODE);
                if (obj != null) {
                    if (obj == 0 || obj == 1) {
                        return imageCapture;
                    }
                    if (obj == 2) {
                        return imageCapture;
                    }
                }
                final StringBuilder sb = new StringBuilder();
                sb.append("The flash mode is not allowed to set: ");
                sb.append(obj);
                throw new IllegalArgumentException(sb.toString());
            }
            return imageCapture;
        }
        
        @Override
        public MutableConfig getMutableConfig() {
            return this.mMutableConfig;
        }
        
        public ImageCaptureConfig getUseCaseConfig() {
            return new ImageCaptureConfig(OptionsBundle.from(this.mMutableConfig));
        }
        
        public Builder setBufferFormat(final int i) {
            this.getMutableConfig().insertOption(ImageCaptureConfig.OPTION_BUFFER_FORMAT, i);
            return this;
        }
        
        public Builder setCameraSelector(final CameraSelector cameraSelector) {
            this.getMutableConfig().insertOption(UseCaseConfig.OPTION_CAMERA_SELECTOR, cameraSelector);
            return this;
        }
        
        public Builder setCaptureBundle(final CaptureBundle captureBundle) {
            this.getMutableConfig().insertOption(ImageCaptureConfig.OPTION_CAPTURE_BUNDLE, captureBundle);
            return this;
        }
        
        public Builder setCaptureMode(final int i) {
            this.getMutableConfig().insertOption(ImageCaptureConfig.OPTION_IMAGE_CAPTURE_MODE, i);
            return this;
        }
        
        public Builder setCaptureOptionUnpacker(final CaptureConfig.OptionUnpacker optionUnpacker) {
            this.getMutableConfig().insertOption(ImageCaptureConfig.OPTION_CAPTURE_CONFIG_UNPACKER, optionUnpacker);
            return this;
        }
        
        public Builder setCaptureProcessor(final CaptureProcessor captureProcessor) {
            this.getMutableConfig().insertOption(ImageCaptureConfig.OPTION_CAPTURE_PROCESSOR, captureProcessor);
            return this;
        }
        
        public Builder setDefaultCaptureConfig(final CaptureConfig captureConfig) {
            this.getMutableConfig().insertOption(ImageCaptureConfig.OPTION_DEFAULT_CAPTURE_CONFIG, captureConfig);
            return this;
        }
        
        public Builder setDefaultResolution(final Size size) {
            this.getMutableConfig().insertOption(ImageOutputConfig.OPTION_DEFAULT_RESOLUTION, size);
            return this;
        }
        
        public Builder setDefaultSessionConfig(final SessionConfig sessionConfig) {
            this.getMutableConfig().insertOption(ImageCaptureConfig.OPTION_DEFAULT_SESSION_CONFIG, sessionConfig);
            return this;
        }
        
        public Builder setFlashMode(final int i) {
            this.getMutableConfig().insertOption(ImageCaptureConfig.OPTION_FLASH_MODE, i);
            return this;
        }
        
        public Builder setFlashType(final int i) {
            this.getMutableConfig().insertOption(ImageCaptureConfig.OPTION_FLASH_TYPE, i);
            return this;
        }
        
        public Builder setImageReaderProxyProvider(final ImageReaderProxyProvider imageReaderProxyProvider) {
            this.getMutableConfig().insertOption(ImageCaptureConfig.OPTION_IMAGE_READER_PROXY_PROVIDER, imageReaderProxyProvider);
            return this;
        }
        
        public Builder setIoExecutor(final Executor executor) {
            this.getMutableConfig().insertOption(ImageCaptureConfig.OPTION_IO_EXECUTOR, executor);
            return this;
        }
        
        public Builder setJpegQuality(final int i) {
            Preconditions.checkArgumentInRange(i, 1, 100, "jpegQuality");
            this.getMutableConfig().insertOption(ImageCaptureConfig.OPTION_JPEG_COMPRESSION_QUALITY, i);
            return this;
        }
        
        public Builder setMaxCaptureStages(final int i) {
            this.getMutableConfig().insertOption(ImageCaptureConfig.OPTION_MAX_CAPTURE_STAGES, i);
            return this;
        }
        
        public Builder setMaxResolution(final Size size) {
            this.getMutableConfig().insertOption(ImageCaptureConfig.OPTION_MAX_RESOLUTION, size);
            return this;
        }
        
        public Builder setSessionOptionUnpacker(final SessionConfig.OptionUnpacker optionUnpacker) {
            this.getMutableConfig().insertOption(ImageCaptureConfig.OPTION_SESSION_CONFIG_UNPACKER, optionUnpacker);
            return this;
        }
        
        public Builder setSoftwareJpegEncoderRequested(final boolean b) {
            this.getMutableConfig().insertOption(ImageCaptureConfig.OPTION_USE_SOFTWARE_JPEG_ENCODER, b);
            return this;
        }
        
        public Builder setSupportedResolutions(final List<Pair<Integer, Size[]>> list) {
            this.getMutableConfig().insertOption(ImageCaptureConfig.OPTION_SUPPORTED_RESOLUTIONS, list);
            return this;
        }
        
        public Builder setSurfaceOccupancyPriority(final int i) {
            this.getMutableConfig().insertOption(ImageCaptureConfig.OPTION_SURFACE_OCCUPANCY_PRIORITY, i);
            return this;
        }
        
        public Builder setTargetAspectRatio(final int i) {
            this.getMutableConfig().insertOption(ImageCaptureConfig.OPTION_TARGET_ASPECT_RATIO, i);
            return this;
        }
        
        public Builder setTargetClass(final Class<ImageCapture> clazz) {
            this.getMutableConfig().insertOption(ImageCaptureConfig.OPTION_TARGET_CLASS, clazz);
            if (this.getMutableConfig().retrieveOption(ImageCaptureConfig.OPTION_TARGET_NAME, (Object)null) == null) {
                final StringBuilder sb = new StringBuilder();
                sb.append(clazz.getCanonicalName());
                sb.append("-");
                sb.append(UUID.randomUUID());
                this.setTargetName(sb.toString());
            }
            return this;
        }
        
        public Builder setTargetName(final String s) {
            this.getMutableConfig().insertOption(ImageCaptureConfig.OPTION_TARGET_NAME, s);
            return this;
        }
        
        public Builder setTargetResolution(final Size size) {
            this.getMutableConfig().insertOption(ImageCaptureConfig.OPTION_TARGET_RESOLUTION, size);
            return this;
        }
        
        public Builder setTargetRotation(final int i) {
            this.getMutableConfig().insertOption(ImageCaptureConfig.OPTION_TARGET_ROTATION, i);
            return this;
        }
        
        public Builder setUseCaseEventCallback(final EventCallback eventCallback) {
            this.getMutableConfig().insertOption(ImageCaptureConfig.OPTION_USE_CASE_EVENT_CALLBACK, eventCallback);
            return this;
        }
        
        public Builder setZslDisabled(final boolean b) {
            this.getMutableConfig().insertOption(UseCaseConfig.OPTION_ZSL_DISABLED, b);
            return this;
        }
    }
    
    @Retention(RetentionPolicy.SOURCE)
    public @interface CaptureMode {
    }
    
    public static final class Defaults implements ConfigProvider<ImageCaptureConfig>
    {
        private static final int DEFAULT_ASPECT_RATIO = 0;
        private static final ImageCaptureConfig DEFAULT_CONFIG;
        private static final int DEFAULT_SURFACE_OCCUPANCY_PRIORITY = 4;
        
        static {
            DEFAULT_CONFIG = new ImageCapture.Builder().setSurfaceOccupancyPriority(4).setTargetAspectRatio(0).getUseCaseConfig();
        }
        
        @Override
        public ImageCaptureConfig getConfig() {
            return Defaults.DEFAULT_CONFIG;
        }
    }
    
    @Retention(RetentionPolicy.SOURCE)
    public @interface FlashMode {
    }
    
    @Retention(RetentionPolicy.SOURCE)
    public @interface FlashType {
    }
    
    @Retention(RetentionPolicy.SOURCE)
    public @interface ImageCaptureError {
    }
    
    static class ImageCaptureRequest
    {
        private final OnImageCapturedCallback mCallback;
        AtomicBoolean mDispatched;
        final int mJpegQuality;
        private final Executor mListenerExecutor;
        final int mRotationDegrees;
        private final Matrix mSensorToBufferTransformMatrix;
        private final Rational mTargetRatio;
        private final Rect mViewPortCropRect;
        
        ImageCaptureRequest(final int mRotationDegrees, final int mJpegQuality, final Rational mTargetRatio, final Rect mViewPortCropRect, final Matrix mSensorToBufferTransformMatrix, final Executor mListenerExecutor, final OnImageCapturedCallback mCallback) {
            boolean b = false;
            this.mDispatched = new AtomicBoolean(false);
            this.mRotationDegrees = mRotationDegrees;
            this.mJpegQuality = mJpegQuality;
            if (mTargetRatio != null) {
                Preconditions.checkArgument(mTargetRatio.isZero() ^ true, (Object)"Target ratio cannot be zero");
                if (mTargetRatio.floatValue() > 0.0f) {
                    b = true;
                }
                Preconditions.checkArgument(b, (Object)"Target ratio must be positive");
            }
            this.mTargetRatio = mTargetRatio;
            this.mViewPortCropRect = mViewPortCropRect;
            this.mSensorToBufferTransformMatrix = mSensorToBufferTransformMatrix;
            this.mListenerExecutor = mListenerExecutor;
            this.mCallback = mCallback;
        }
        
        void dispatchImage(final ImageProxy imageProxy) {
            if (!this.mDispatched.compareAndSet(false, true)) {
                imageProxy.close();
                return;
            }
            Size size = null;
            int n = 0;
            Label_0155: {
                if (ImageCapture.EXIF_ROTATION_AVAILABILITY.shouldUseExifOrientation(imageProxy)) {
                    try {
                        final ByteBuffer buffer = imageProxy.getPlanes()[0].getBuffer();
                        buffer.rewind();
                        final byte[] array = new byte[buffer.capacity()];
                        buffer.get(array);
                        final Exif fromInputStream = Exif.createFromInputStream(new ByteArrayInputStream(array));
                        buffer.rewind();
                        size = new Size(fromInputStream.getWidth(), fromInputStream.getHeight());
                        n = fromInputStream.getRotation();
                        break Label_0155;
                    }
                    catch (final IOException ex) {
                        this.notifyCallbackError(1, "Unable to parse JPEG exif", ex);
                        imageProxy.close();
                        return;
                    }
                }
                size = new Size(imageProxy.getWidth(), imageProxy.getHeight());
                n = this.mRotationDegrees;
            }
            final SettableImageProxy settableImageProxy = new SettableImageProxy(imageProxy, size, ImmutableImageInfo.create(imageProxy.getImageInfo().getTagBundle(), imageProxy.getImageInfo().getTimestamp(), n, this.mSensorToBufferTransformMatrix));
            settableImageProxy.setCropRect(ImageCapture.computeDispatchCropRect(this.mViewPortCropRect, this.mTargetRatio, this.mRotationDegrees, size, n));
            try {
                this.mListenerExecutor.execute(new ImageCapture$ImageCaptureRequest$$ExternalSyntheticLambda1(this, settableImageProxy));
            }
            catch (final RejectedExecutionException ex2) {
                Logger.e("ImageCapture", "Unable to post to the supplied executor.");
                imageProxy.close();
            }
        }
        
        void notifyCallbackError(final int n, final String s, final Throwable t) {
            if (!this.mDispatched.compareAndSet(false, true)) {
                return;
            }
            try {
                this.mListenerExecutor.execute(new ImageCapture$ImageCaptureRequest$$ExternalSyntheticLambda0(this, n, s, t));
            }
            catch (final RejectedExecutionException ex) {
                Logger.e("ImageCapture", "Unable to post to the supplied executor.");
            }
        }
    }
    
    static class ImageCaptureRequestProcessor implements OnImageCloseListener
    {
        ImageCaptureRequest mCurrentRequest;
        ListenableFuture<ImageProxy> mCurrentRequestFuture;
        private final ImageCaptor mImageCaptor;
        final Object mLock;
        private final int mMaxImages;
        int mOutstandingImages;
        private final Deque<ImageCaptureRequest> mPendingRequests;
        private final RequestProcessCallback mRequestProcessCallback;
        
        ImageCaptureRequestProcessor(final int n, final ImageCaptor imageCaptor) {
            this(n, imageCaptor, null);
        }
        
        ImageCaptureRequestProcessor(final int mMaxImages, final ImageCaptor mImageCaptor, final RequestProcessCallback mRequestProcessCallback) {
            this.mPendingRequests = new ArrayDeque<ImageCaptureRequest>();
            this.mCurrentRequest = null;
            this.mCurrentRequestFuture = null;
            this.mOutstandingImages = 0;
            this.mLock = new Object();
            this.mMaxImages = mMaxImages;
            this.mImageCaptor = mImageCaptor;
            this.mRequestProcessCallback = mRequestProcessCallback;
        }
        
        public void cancelRequests(final Throwable t) {
            Object o = this.mLock;
            synchronized (o) {
                final ImageCaptureRequest mCurrentRequest = this.mCurrentRequest;
                this.mCurrentRequest = null;
                final ListenableFuture<ImageProxy> mCurrentRequestFuture = this.mCurrentRequestFuture;
                this.mCurrentRequestFuture = null;
                final ArrayList list = new ArrayList(this.mPendingRequests);
                this.mPendingRequests.clear();
                monitorexit(o);
                if (mCurrentRequest != null && mCurrentRequestFuture != null) {
                    mCurrentRequest.notifyCallbackError(ImageCapture.getError(t), t.getMessage(), t);
                    mCurrentRequestFuture.cancel(true);
                }
                o = list.iterator();
                while (((Iterator)o).hasNext()) {
                    ((ImageCaptureRequest)((Iterator)o).next()).notifyCallbackError(ImageCapture.getError(t), t.getMessage(), t);
                }
            }
        }
        
        @Override
        public void onImageClose(final ImageProxy imageProxy) {
            synchronized (this.mLock) {
                --this.mOutstandingImages;
                CameraXExecutors.mainThreadExecutor().execute(new ImageCapture$ImageCaptureRequestProcessor$$ExternalSyntheticLambda0(this));
            }
        }
        
        void processNextRequest() {
            synchronized (this.mLock) {
                if (this.mCurrentRequest != null) {
                    return;
                }
                if (this.mOutstandingImages >= this.mMaxImages) {
                    Logger.w("ImageCapture", "Too many acquire images. Close image to be able to process next.");
                    return;
                }
                final ImageCaptureRequest mCurrentRequest = this.mPendingRequests.poll();
                if (mCurrentRequest == null) {
                    return;
                }
                this.mCurrentRequest = mCurrentRequest;
                final RequestProcessCallback mRequestProcessCallback = this.mRequestProcessCallback;
                if (mRequestProcessCallback != null) {
                    mRequestProcessCallback.onPreProcessRequest(mCurrentRequest);
                }
                Futures.addCallback(this.mCurrentRequestFuture = this.mImageCaptor.capture(mCurrentRequest), new FutureCallback<ImageProxy>(this, mCurrentRequest) {
                    final ImageCaptureRequestProcessor this$0;
                    final ImageCaptureRequest val$imageCaptureRequest;
                    
                    @Override
                    public void onFailure(final Throwable t) {
                        synchronized (this.this$0.mLock) {
                            if (!(t instanceof CancellationException)) {
                                final ImageCaptureRequest val$imageCaptureRequest = this.val$imageCaptureRequest;
                                final int error = ImageCapture.getError(t);
                                String message;
                                if (t != null) {
                                    message = t.getMessage();
                                }
                                else {
                                    message = "Unknown error";
                                }
                                val$imageCaptureRequest.notifyCallbackError(error, message, t);
                            }
                            this.this$0.mCurrentRequest = null;
                            this.this$0.mCurrentRequestFuture = null;
                            this.this$0.processNextRequest();
                        }
                    }
                    
                    @Override
                    public void onSuccess(final ImageProxy imageProxy) {
                        synchronized (this.this$0.mLock) {
                            Preconditions.checkNotNull(imageProxy);
                            final SingleCloseImageProxy singleCloseImageProxy = new SingleCloseImageProxy(imageProxy);
                            singleCloseImageProxy.addOnImageCloseListener((ForwardingImageProxy.OnImageCloseListener)this.this$0);
                            final ImageCaptureRequestProcessor this$0 = this.this$0;
                            ++this$0.mOutstandingImages;
                            this.val$imageCaptureRequest.dispatchImage(singleCloseImageProxy);
                            this.this$0.mCurrentRequest = null;
                            this.this$0.mCurrentRequestFuture = null;
                            this.this$0.processNextRequest();
                        }
                    }
                }, CameraXExecutors.mainThreadExecutor());
            }
        }
        
        public List<ImageCaptureRequest> pullOutUnfinishedRequests() {
            synchronized (this.mLock) {
                final ArrayList list = new ArrayList(this.mPendingRequests);
                this.mPendingRequests.clear();
                final ImageCaptureRequest mCurrentRequest = this.mCurrentRequest;
                this.mCurrentRequest = null;
                if (mCurrentRequest != null) {
                    final ListenableFuture<ImageProxy> mCurrentRequestFuture = this.mCurrentRequestFuture;
                    if (mCurrentRequestFuture != null && mCurrentRequestFuture.cancel(true)) {
                        list.add(0, mCurrentRequest);
                    }
                }
                return list;
            }
        }
        
        public void sendRequest(final ImageCaptureRequest imageCaptureRequest) {
            synchronized (this.mLock) {
                this.mPendingRequests.offer(imageCaptureRequest);
                final Locale us = Locale.US;
                int i;
                if (this.mCurrentRequest != null) {
                    i = 1;
                }
                else {
                    i = 0;
                }
                Logger.d("ImageCapture", String.format(us, "Send image capture request [current, pending] = [%d, %d]", i, this.mPendingRequests.size()));
                this.processNextRequest();
            }
        }
        
        interface ImageCaptor
        {
            ListenableFuture<ImageProxy> capture(final ImageCaptureRequest p0);
        }
        
        interface RequestProcessCallback
        {
            void onPreProcessRequest(final ImageCaptureRequest p0);
        }
    }
    
    public static final class Metadata
    {
        private boolean mIsReversedHorizontal;
        private boolean mIsReversedHorizontalSet;
        private boolean mIsReversedVertical;
        private Location mLocation;
        
        public Metadata() {
            this.mIsReversedHorizontalSet = false;
        }
        
        public Location getLocation() {
            return this.mLocation;
        }
        
        public boolean isReversedHorizontal() {
            return this.mIsReversedHorizontal;
        }
        
        public boolean isReversedHorizontalSet() {
            return this.mIsReversedHorizontalSet;
        }
        
        public boolean isReversedVertical() {
            return this.mIsReversedVertical;
        }
        
        public void setLocation(final Location mLocation) {
            this.mLocation = mLocation;
        }
        
        public void setReversedHorizontal(final boolean mIsReversedHorizontal) {
            this.mIsReversedHorizontal = mIsReversedHorizontal;
            this.mIsReversedHorizontalSet = true;
        }
        
        public void setReversedVertical(final boolean mIsReversedVertical) {
            this.mIsReversedVertical = mIsReversedVertical;
        }
    }
    
    public abstract static class OnImageCapturedCallback
    {
        public void onCaptureSuccess(final ImageProxy imageProxy) {
        }
        
        public void onError(final ImageCaptureException ex) {
        }
    }
    
    public interface OnImageSavedCallback
    {
        void onError(final ImageCaptureException p0);
        
        void onImageSaved(final OutputFileResults p0);
    }
    
    public static final class OutputFileOptions
    {
        private final ContentResolver mContentResolver;
        private final ContentValues mContentValues;
        private final File mFile;
        private final Metadata mMetadata;
        private final OutputStream mOutputStream;
        private final Uri mSaveCollection;
        
        OutputFileOptions(final File mFile, final ContentResolver mContentResolver, final Uri mSaveCollection, final ContentValues mContentValues, final OutputStream mOutputStream, final Metadata metadata) {
            this.mFile = mFile;
            this.mContentResolver = mContentResolver;
            this.mSaveCollection = mSaveCollection;
            this.mContentValues = mContentValues;
            this.mOutputStream = mOutputStream;
            Metadata mMetadata = metadata;
            if (metadata == null) {
                mMetadata = new Metadata();
            }
            this.mMetadata = mMetadata;
        }
        
        public ContentResolver getContentResolver() {
            return this.mContentResolver;
        }
        
        public ContentValues getContentValues() {
            return this.mContentValues;
        }
        
        public File getFile() {
            return this.mFile;
        }
        
        public Metadata getMetadata() {
            return this.mMetadata;
        }
        
        public OutputStream getOutputStream() {
            return this.mOutputStream;
        }
        
        public Uri getSaveCollection() {
            return this.mSaveCollection;
        }
        
        public static final class Builder
        {
            private ContentResolver mContentResolver;
            private ContentValues mContentValues;
            private File mFile;
            private Metadata mMetadata;
            private OutputStream mOutputStream;
            private Uri mSaveCollection;
            
            public Builder(final ContentResolver mContentResolver, final Uri mSaveCollection, final ContentValues mContentValues) {
                this.mContentResolver = mContentResolver;
                this.mSaveCollection = mSaveCollection;
                this.mContentValues = mContentValues;
            }
            
            public Builder(final File mFile) {
                this.mFile = mFile;
            }
            
            public Builder(final OutputStream mOutputStream) {
                this.mOutputStream = mOutputStream;
            }
            
            public OutputFileOptions build() {
                return new OutputFileOptions(this.mFile, this.mContentResolver, this.mSaveCollection, this.mContentValues, this.mOutputStream, this.mMetadata);
            }
            
            public Builder setMetadata(final Metadata mMetadata) {
                this.mMetadata = mMetadata;
                return this;
            }
        }
    }
    
    public static class OutputFileResults
    {
        private final Uri mSavedUri;
        
        public OutputFileResults(final Uri mSavedUri) {
            this.mSavedUri = mSavedUri;
        }
        
        public Uri getSavedUri() {
            return this.mSavedUri;
        }
    }
}

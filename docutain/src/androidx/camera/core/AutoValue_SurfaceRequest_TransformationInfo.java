// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core;

import android.graphics.Rect;

final class AutoValue_SurfaceRequest_TransformationInfo extends TransformationInfo
{
    private final Rect cropRect;
    private final int rotationDegrees;
    private final int targetRotation;
    
    AutoValue_SurfaceRequest_TransformationInfo(final Rect cropRect, final int rotationDegrees, final int targetRotation) {
        if (cropRect != null) {
            this.cropRect = cropRect;
            this.rotationDegrees = rotationDegrees;
            this.targetRotation = targetRotation;
            return;
        }
        throw new NullPointerException("Null cropRect");
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o == this) {
            return true;
        }
        if (o instanceof TransformationInfo) {
            final TransformationInfo transformationInfo = (TransformationInfo)o;
            if (!this.cropRect.equals((Object)transformationInfo.getCropRect()) || this.rotationDegrees != transformationInfo.getRotationDegrees() || this.targetRotation != transformationInfo.getTargetRotation()) {
                b = false;
            }
            return b;
        }
        return false;
    }
    
    @Override
    public Rect getCropRect() {
        return this.cropRect;
    }
    
    @Override
    public int getRotationDegrees() {
        return this.rotationDegrees;
    }
    
    @Override
    public int getTargetRotation() {
        return this.targetRotation;
    }
    
    @Override
    public int hashCode() {
        return ((this.cropRect.hashCode() ^ 0xF4243) * 1000003 ^ this.rotationDegrees) * 1000003 ^ this.targetRotation;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("TransformationInfo{cropRect=");
        sb.append(this.cropRect);
        sb.append(", rotationDegrees=");
        sb.append(this.rotationDegrees);
        sb.append(", targetRotation=");
        sb.append(this.targetRotation);
        sb.append("}");
        return sb.toString();
    }
}

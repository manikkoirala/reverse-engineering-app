// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core;

import androidx.camera.core.impl.ReadableConfig;
import androidx.camera.core.impl.ConfigProvider;
import java.util.UUID;
import android.util.Pair;
import java.util.List;
import androidx.camera.core.impl.CaptureConfig;
import androidx.camera.core.impl.OptionsBundle;
import androidx.camera.core.impl.MutableConfig;
import androidx.camera.core.internal.TargetConfig;
import androidx.camera.core.impl.MutableOptionsBundle;
import androidx.camera.core.internal.ThreadConfig;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;
import android.graphics.Rect;
import android.graphics.Matrix;
import androidx.camera.core.impl.ImageOutputConfig;
import androidx.camera.core.impl.Quirk;
import androidx.camera.core.internal.compat.quirk.OnePixelShiftQuirk;
import androidx.camera.core.impl.CameraInfoInternal;
import androidx.camera.core.impl.Config;
import androidx.camera.core.impl.Config$_CC;
import androidx.camera.core.impl.UseCaseConfigFactory;
import androidx.camera.core.impl.ImmediateSurface;
import androidx.camera.core.impl.ImageReaderProxy;
import androidx.core.util.Preconditions;
import androidx.camera.core.impl.SessionConfig;
import android.util.Size;
import androidx.camera.core.impl.utils.Threads;
import java.util.concurrent.Executor;
import androidx.camera.core.impl.CameraInternal;
import androidx.camera.core.impl.utils.executor.CameraXExecutors;
import androidx.camera.core.impl.UseCaseConfig;
import androidx.camera.core.impl.ImageAnalysisConfig;
import androidx.camera.core.impl.DeferrableSurface;

public final class ImageAnalysis extends UseCase
{
    public static final int COORDINATE_SYSTEM_ORIGINAL = 0;
    private static final int DEFAULT_BACKPRESSURE_STRATEGY = 0;
    public static final Defaults DEFAULT_CONFIG;
    private static final int DEFAULT_IMAGE_QUEUE_DEPTH = 6;
    private static final Boolean DEFAULT_ONE_PIXEL_SHIFT_ENABLED;
    private static final int DEFAULT_OUTPUT_IMAGE_FORMAT = 1;
    private static final boolean DEFAULT_OUTPUT_IMAGE_ROTATION_ENABLED = false;
    private static final int NON_BLOCKING_IMAGE_DEPTH = 4;
    public static final int OUTPUT_IMAGE_FORMAT_RGBA_8888 = 2;
    public static final int OUTPUT_IMAGE_FORMAT_YUV_420_888 = 1;
    public static final int STRATEGY_BLOCK_PRODUCER = 1;
    public static final int STRATEGY_KEEP_ONLY_LATEST = 0;
    private static final String TAG = "ImageAnalysis";
    private final Object mAnalysisLock;
    private DeferrableSurface mDeferrableSurface;
    final ImageAnalysisAbstractAnalyzer mImageAnalysisAbstractAnalyzer;
    private Analyzer mSubscribedAnalyzer;
    
    static {
        DEFAULT_CONFIG = new Defaults();
        DEFAULT_ONE_PIXEL_SHIFT_ENABLED = null;
    }
    
    ImageAnalysis(final ImageAnalysisConfig imageAnalysisConfig) {
        super(imageAnalysisConfig);
        this.mAnalysisLock = new Object();
        if (((ImageAnalysisConfig)this.getCurrentConfig()).getBackpressureStrategy(0) == 1) {
            this.mImageAnalysisAbstractAnalyzer = new ImageAnalysisBlockingAnalyzer();
        }
        else {
            this.mImageAnalysisAbstractAnalyzer = new ImageAnalysisNonBlockingAnalyzer(imageAnalysisConfig.getBackgroundExecutor(CameraXExecutors.highPriorityExecutor()));
        }
        this.mImageAnalysisAbstractAnalyzer.setOutputImageFormat(this.getOutputImageFormat());
        this.mImageAnalysisAbstractAnalyzer.setOutputImageRotationEnabled(this.isOutputImageRotationEnabled());
    }
    
    private boolean isFlipWH(final CameraInternal cameraInternal) {
        final boolean outputImageRotationEnabled = this.isOutputImageRotationEnabled();
        boolean b = false;
        if (outputImageRotationEnabled) {
            b = b;
            if (this.getRelativeRotation(cameraInternal) % 180 != 0) {
                b = true;
            }
        }
        return b;
    }
    
    private void tryUpdateRelativeRotation() {
        final CameraInternal camera = this.getCamera();
        if (camera != null) {
            this.mImageAnalysisAbstractAnalyzer.setRelativeRotation(this.getRelativeRotation(camera));
        }
    }
    
    public void clearAnalyzer() {
        synchronized (this.mAnalysisLock) {
            this.mImageAnalysisAbstractAnalyzer.setAnalyzer(null, null);
            if (this.mSubscribedAnalyzer != null) {
                this.notifyInactive();
            }
            this.mSubscribedAnalyzer = null;
        }
    }
    
    void clearPipeline() {
        Threads.checkMainThread();
        final DeferrableSurface mDeferrableSurface = this.mDeferrableSurface;
        if (mDeferrableSurface != null) {
            mDeferrableSurface.close();
            this.mDeferrableSurface = null;
        }
    }
    
    SessionConfig.Builder createPipeline(final String s, final ImageAnalysisConfig imageAnalysisConfig, final Size size) {
        Threads.checkMainThread();
        final Executor executor = Preconditions.checkNotNull(imageAnalysisConfig.getBackgroundExecutor(CameraXExecutors.highPriorityExecutor()));
        final int backpressureStrategy = this.getBackpressureStrategy();
        final int n = 1;
        int imageQueueDepth;
        if (backpressureStrategy == 1) {
            imageQueueDepth = this.getImageQueueDepth();
        }
        else {
            imageQueueDepth = 4;
        }
        SafeCloseImageReaderProxy safeCloseImageReaderProxy;
        if (imageAnalysisConfig.getImageReaderProxyProvider() != null) {
            safeCloseImageReaderProxy = new SafeCloseImageReaderProxy(imageAnalysisConfig.getImageReaderProxyProvider().newInstance(size.getWidth(), size.getHeight(), this.getImageFormat(), imageQueueDepth, 0L));
        }
        else {
            safeCloseImageReaderProxy = new SafeCloseImageReaderProxy(ImageReaderProxys.createIsolatedReader(size.getWidth(), size.getHeight(), this.getImageFormat(), imageQueueDepth));
        }
        final boolean b = this.getCamera() != null && this.isFlipWH(this.getCamera());
        int n2;
        if (b) {
            n2 = size.getHeight();
        }
        else {
            n2 = size.getWidth();
        }
        int n3;
        if (b) {
            n3 = size.getWidth();
        }
        else {
            n3 = size.getHeight();
        }
        int n4;
        if (this.getOutputImageFormat() == 2) {
            n4 = 1;
        }
        else {
            n4 = 35;
        }
        final boolean b2 = this.getImageFormat() == 35 && this.getOutputImageFormat() == 2;
        int n5 = 0;
        Label_0275: {
            if (this.getImageFormat() == 35) {
                if (this.getCamera() != null) {
                    n5 = n;
                    if (this.getRelativeRotation(this.getCamera()) != 0) {
                        break Label_0275;
                    }
                }
                if (Boolean.TRUE.equals(this.getOnePixelShiftEnabled())) {
                    n5 = n;
                    break Label_0275;
                }
            }
            n5 = 0;
        }
        SafeCloseImageReaderProxy processedImageReaderProxy;
        if (!b2 && n5 == 0) {
            processedImageReaderProxy = null;
        }
        else {
            processedImageReaderProxy = new SafeCloseImageReaderProxy(ImageReaderProxys.createIsolatedReader(n2, n3, n4, safeCloseImageReaderProxy.getMaxImages()));
        }
        if (processedImageReaderProxy != null) {
            this.mImageAnalysisAbstractAnalyzer.setProcessedImageReaderProxy(processedImageReaderProxy);
        }
        this.tryUpdateRelativeRotation();
        safeCloseImageReaderProxy.setOnImageAvailableListener(this.mImageAnalysisAbstractAnalyzer, executor);
        final SessionConfig.Builder from = SessionConfig.Builder.createFrom(imageAnalysisConfig);
        final DeferrableSurface mDeferrableSurface = this.mDeferrableSurface;
        if (mDeferrableSurface != null) {
            mDeferrableSurface.close();
        }
        final ImmediateSurface mDeferrableSurface2 = new ImmediateSurface(safeCloseImageReaderProxy.getSurface(), size, this.getImageFormat());
        this.mDeferrableSurface = mDeferrableSurface2;
        mDeferrableSurface2.getTerminationFuture().addListener((Runnable)new ImageAnalysis$$ExternalSyntheticLambda0(safeCloseImageReaderProxy, processedImageReaderProxy), (Executor)CameraXExecutors.mainThreadExecutor());
        from.addSurface(this.mDeferrableSurface);
        from.addErrorListener(new ImageAnalysis$$ExternalSyntheticLambda1(this, s, imageAnalysisConfig, size));
        return from;
    }
    
    public Executor getBackgroundExecutor() {
        return ((ImageAnalysisConfig)this.getCurrentConfig()).getBackgroundExecutor(null);
    }
    
    public int getBackpressureStrategy() {
        return ((ImageAnalysisConfig)this.getCurrentConfig()).getBackpressureStrategy(0);
    }
    
    @Override
    public UseCaseConfig<?> getDefaultConfig(final boolean b, final UseCaseConfigFactory useCaseConfigFactory) {
        Config config2;
        final Config config = config2 = useCaseConfigFactory.getConfig(UseCaseConfigFactory.CaptureType.IMAGE_ANALYSIS, 1);
        if (b) {
            config2 = Config$_CC.mergeConfigs(config, ImageAnalysis.DEFAULT_CONFIG.getConfig());
        }
        Object useCaseConfig;
        if (config2 == null) {
            useCaseConfig = null;
        }
        else {
            useCaseConfig = this.getUseCaseConfigBuilder(config2).getUseCaseConfig();
        }
        return (UseCaseConfig<?>)useCaseConfig;
    }
    
    public int getImageQueueDepth() {
        return ((ImageAnalysisConfig)this.getCurrentConfig()).getImageQueueDepth(6);
    }
    
    public Boolean getOnePixelShiftEnabled() {
        return ((ImageAnalysisConfig)this.getCurrentConfig()).getOnePixelShiftEnabled(ImageAnalysis.DEFAULT_ONE_PIXEL_SHIFT_ENABLED);
    }
    
    public int getOutputImageFormat() {
        return ((ImageAnalysisConfig)this.getCurrentConfig()).getOutputImageFormat(1);
    }
    
    @Override
    public ResolutionInfo getResolutionInfo() {
        return super.getResolutionInfo();
    }
    
    public int getTargetRotation() {
        return this.getTargetRotationInternal();
    }
    
    @Override
    public UseCaseConfig.Builder<?, ?, ?> getUseCaseConfigBuilder(final Config config) {
        return Builder.fromConfig(config);
    }
    
    public boolean isOutputImageRotationEnabled() {
        return ((ImageAnalysisConfig)this.getCurrentConfig()).isOutputImageRotationEnabled(false);
    }
    
    @Override
    public void onAttached() {
        this.mImageAnalysisAbstractAnalyzer.attach();
    }
    
    @Override
    public void onDetached() {
        this.clearPipeline();
        this.mImageAnalysisAbstractAnalyzer.detach();
    }
    
    @Override
    protected UseCaseConfig<?> onMergeConfig(final CameraInfoInternal cameraInfoInternal, final UseCaseConfig.Builder<?, ?, ?> builder) {
        final Boolean onePixelShiftEnabled = this.getOnePixelShiftEnabled();
        boolean onePixelShiftEnabled2 = cameraInfoInternal.getCameraQuirks().contains(OnePixelShiftQuirk.class);
        final ImageAnalysisAbstractAnalyzer mImageAnalysisAbstractAnalyzer = this.mImageAnalysisAbstractAnalyzer;
        if (onePixelShiftEnabled != null) {
            onePixelShiftEnabled2 = onePixelShiftEnabled;
        }
        mImageAnalysisAbstractAnalyzer.setOnePixelShiftEnabled(onePixelShiftEnabled2);
        synchronized (this.mAnalysisLock) {
            final Analyzer mSubscribedAnalyzer = this.mSubscribedAnalyzer;
            Size defaultTargetResolution;
            if (mSubscribedAnalyzer != null) {
                defaultTargetResolution = mSubscribedAnalyzer.getDefaultTargetResolution();
            }
            else {
                defaultTargetResolution = null;
            }
            monitorexit(this.mAnalysisLock);
            if (defaultTargetResolution != null && !((ReadableConfig)builder.getUseCaseConfig()).containsOption(ImageOutputConfig.OPTION_TARGET_RESOLUTION)) {
                builder.getMutableConfig().insertOption(ImageOutputConfig.OPTION_TARGET_RESOLUTION, defaultTargetResolution);
            }
            return (UseCaseConfig<?>)builder.getUseCaseConfig();
        }
    }
    
    @Override
    protected Size onSuggestedResolutionUpdated(final Size size) {
        this.updateSessionConfig(this.createPipeline(this.getCameraId(), (ImageAnalysisConfig)this.getCurrentConfig(), size).build());
        return size;
    }
    
    public void setAnalyzer(final Executor executor, final Analyzer mSubscribedAnalyzer) {
        synchronized (this.mAnalysisLock) {
            this.mImageAnalysisAbstractAnalyzer.setAnalyzer(executor, new ImageAnalysis$$ExternalSyntheticLambda2(mSubscribedAnalyzer));
            if (this.mSubscribedAnalyzer == null) {
                this.notifyActive();
            }
            this.mSubscribedAnalyzer = mSubscribedAnalyzer;
        }
    }
    
    @Override
    public void setSensorToBufferTransformMatrix(final Matrix matrix) {
        super.setSensorToBufferTransformMatrix(matrix);
        this.mImageAnalysisAbstractAnalyzer.setSensorToBufferTransformMatrix(matrix);
    }
    
    public void setTargetRotation(final int targetRotationInternal) {
        if (this.setTargetRotationInternal(targetRotationInternal)) {
            this.tryUpdateRelativeRotation();
        }
    }
    
    @Override
    public void setViewPortCropRect(final Rect rect) {
        super.setViewPortCropRect(rect);
        this.mImageAnalysisAbstractAnalyzer.setViewPortCropRect(rect);
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("ImageAnalysis:");
        sb.append(this.getName());
        return sb.toString();
    }
    
    public interface Analyzer
    {
        void analyze(final ImageProxy p0);
        
        Size getDefaultTargetResolution();
        
        int getTargetCoordinateSystem();
        
        void updateTransform(final Matrix p0);
    }
    
    @Retention(RetentionPolicy.SOURCE)
    public @interface BackpressureStrategy {
    }
    
    public static final class Builder implements ImageOutputConfig.Builder<Builder>, ThreadConfig.Builder<Builder>, UseCaseConfig.Builder<ImageAnalysis, ImageAnalysisConfig, Builder>
    {
        private final MutableOptionsBundle mMutableConfig;
        
        public Builder() {
            this(MutableOptionsBundle.create());
        }
        
        private Builder(final MutableOptionsBundle mMutableConfig) {
            this.mMutableConfig = mMutableConfig;
            final Class<?> obj = mMutableConfig.retrieveOption(TargetConfig.OPTION_TARGET_CLASS, null);
            if (obj != null && !obj.equals(ImageAnalysis.class)) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Invalid target class configuration for ");
                sb.append(this);
                sb.append(": ");
                sb.append(obj);
                throw new IllegalArgumentException(sb.toString());
            }
            this.setTargetClass(ImageAnalysis.class);
        }
        
        static Builder fromConfig(final Config config) {
            return new Builder(MutableOptionsBundle.from(config));
        }
        
        public static Builder fromConfig(final ImageAnalysisConfig imageAnalysisConfig) {
            return new Builder(MutableOptionsBundle.from(imageAnalysisConfig));
        }
        
        @Override
        public ImageAnalysis build() {
            if (this.getMutableConfig().retrieveOption(ImageOutputConfig.OPTION_TARGET_ASPECT_RATIO, null) != null && this.getMutableConfig().retrieveOption(ImageOutputConfig.OPTION_TARGET_RESOLUTION, null) != null) {
                throw new IllegalArgumentException("Cannot use both setTargetResolution and setTargetAspectRatio on the same config.");
            }
            return new ImageAnalysis(this.getUseCaseConfig());
        }
        
        @Override
        public MutableConfig getMutableConfig() {
            return this.mMutableConfig;
        }
        
        public ImageAnalysisConfig getUseCaseConfig() {
            return new ImageAnalysisConfig(OptionsBundle.from(this.mMutableConfig));
        }
        
        public Builder setBackgroundExecutor(final Executor executor) {
            this.getMutableConfig().insertOption(ThreadConfig.OPTION_BACKGROUND_EXECUTOR, executor);
            return this;
        }
        
        public Builder setBackpressureStrategy(final int i) {
            this.getMutableConfig().insertOption(ImageAnalysisConfig.OPTION_BACKPRESSURE_STRATEGY, i);
            return this;
        }
        
        public Builder setCameraSelector(final CameraSelector cameraSelector) {
            this.getMutableConfig().insertOption(UseCaseConfig.OPTION_CAMERA_SELECTOR, cameraSelector);
            return this;
        }
        
        public Builder setCaptureOptionUnpacker(final CaptureConfig.OptionUnpacker optionUnpacker) {
            this.getMutableConfig().insertOption(UseCaseConfig.OPTION_CAPTURE_CONFIG_UNPACKER, optionUnpacker);
            return this;
        }
        
        public Builder setDefaultCaptureConfig(final CaptureConfig captureConfig) {
            this.getMutableConfig().insertOption(UseCaseConfig.OPTION_DEFAULT_CAPTURE_CONFIG, captureConfig);
            return this;
        }
        
        public Builder setDefaultResolution(final Size size) {
            this.getMutableConfig().insertOption(ImageOutputConfig.OPTION_DEFAULT_RESOLUTION, size);
            return this;
        }
        
        public Builder setDefaultSessionConfig(final SessionConfig sessionConfig) {
            this.getMutableConfig().insertOption(UseCaseConfig.OPTION_DEFAULT_SESSION_CONFIG, sessionConfig);
            return this;
        }
        
        public Builder setImageQueueDepth(final int i) {
            this.getMutableConfig().insertOption(ImageAnalysisConfig.OPTION_IMAGE_QUEUE_DEPTH, i);
            return this;
        }
        
        public Builder setImageReaderProxyProvider(final ImageReaderProxyProvider imageReaderProxyProvider) {
            this.getMutableConfig().insertOption(ImageAnalysisConfig.OPTION_IMAGE_READER_PROXY_PROVIDER, imageReaderProxyProvider);
            return this;
        }
        
        public Builder setMaxResolution(final Size size) {
            this.getMutableConfig().insertOption(ImageOutputConfig.OPTION_MAX_RESOLUTION, size);
            return this;
        }
        
        public Builder setOnePixelShiftEnabled(final boolean b) {
            this.getMutableConfig().insertOption(ImageAnalysisConfig.OPTION_ONE_PIXEL_SHIFT_ENABLED, b);
            return this;
        }
        
        public Builder setOutputImageFormat(final int i) {
            this.getMutableConfig().insertOption(ImageAnalysisConfig.OPTION_OUTPUT_IMAGE_FORMAT, i);
            return this;
        }
        
        public Builder setOutputImageRotationEnabled(final boolean b) {
            this.getMutableConfig().insertOption(ImageAnalysisConfig.OPTION_OUTPUT_IMAGE_ROTATION_ENABLED, b);
            return this;
        }
        
        public Builder setSessionOptionUnpacker(final SessionConfig.OptionUnpacker optionUnpacker) {
            this.getMutableConfig().insertOption(UseCaseConfig.OPTION_SESSION_CONFIG_UNPACKER, optionUnpacker);
            return this;
        }
        
        public Builder setSupportedResolutions(final List<Pair<Integer, Size[]>> list) {
            this.getMutableConfig().insertOption(ImageOutputConfig.OPTION_SUPPORTED_RESOLUTIONS, list);
            return this;
        }
        
        public Builder setSurfaceOccupancyPriority(final int i) {
            this.getMutableConfig().insertOption(UseCaseConfig.OPTION_SURFACE_OCCUPANCY_PRIORITY, i);
            return this;
        }
        
        public Builder setTargetAspectRatio(final int i) {
            this.getMutableConfig().insertOption(ImageOutputConfig.OPTION_TARGET_ASPECT_RATIO, i);
            return this;
        }
        
        public Builder setTargetClass(final Class<ImageAnalysis> clazz) {
            this.getMutableConfig().insertOption(UseCaseConfig.OPTION_TARGET_CLASS, clazz);
            if (this.getMutableConfig().retrieveOption(UseCaseConfig.OPTION_TARGET_NAME, (Object)null) == null) {
                final StringBuilder sb = new StringBuilder();
                sb.append(clazz.getCanonicalName());
                sb.append("-");
                sb.append(UUID.randomUUID());
                this.setTargetName(sb.toString());
            }
            return this;
        }
        
        public Builder setTargetName(final String s) {
            this.getMutableConfig().insertOption(UseCaseConfig.OPTION_TARGET_NAME, s);
            return this;
        }
        
        public Builder setTargetResolution(final Size size) {
            this.getMutableConfig().insertOption(ImageOutputConfig.OPTION_TARGET_RESOLUTION, size);
            return this;
        }
        
        public Builder setTargetRotation(final int i) {
            this.getMutableConfig().insertOption(ImageOutputConfig.OPTION_TARGET_ROTATION, i);
            return this;
        }
        
        public Builder setUseCaseEventCallback(final EventCallback eventCallback) {
            this.getMutableConfig().insertOption(UseCaseConfig.OPTION_USE_CASE_EVENT_CALLBACK, eventCallback);
            return this;
        }
        
        public Builder setZslDisabled(final boolean b) {
            this.getMutableConfig().insertOption(UseCaseConfig.OPTION_ZSL_DISABLED, b);
            return this;
        }
    }
    
    public static final class Defaults implements ConfigProvider<ImageAnalysisConfig>
    {
        private static final int DEFAULT_ASPECT_RATIO = 0;
        private static final ImageAnalysisConfig DEFAULT_CONFIG;
        private static final int DEFAULT_SURFACE_OCCUPANCY_PRIORITY = 1;
        private static final Size DEFAULT_TARGET_RESOLUTION;
        
        static {
            DEFAULT_CONFIG = new Builder().setDefaultResolution(DEFAULT_TARGET_RESOLUTION = new Size(640, 480)).setSurfaceOccupancyPriority(1).setTargetAspectRatio(0).getUseCaseConfig();
        }
        
        @Override
        public ImageAnalysisConfig getConfig() {
            return Defaults.DEFAULT_CONFIG;
        }
    }
    
    @Retention(RetentionPolicy.SOURCE)
    public @interface OutputImageFormat {
    }
}

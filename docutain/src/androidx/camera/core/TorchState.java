// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core;

import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;

public class TorchState
{
    public static final int OFF = 0;
    public static final int ON = 1;
    
    private TorchState() {
    }
    
    @Retention(RetentionPolicy.SOURCE)
    public @interface State {
    }
}

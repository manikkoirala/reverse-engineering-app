// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core;

import java.util.concurrent.ExecutionException;
import androidx.camera.core.impl.ImageProxyBundle;
import java.util.Collections;
import androidx.core.util.Preconditions;
import android.media.ImageReader;
import android.util.Size;
import android.view.Surface;
import java.util.concurrent.RejectedExecutionException;
import androidx.arch.core.util.Function;
import androidx.camera.core.impl.utils.executor.CameraXExecutors;
import java.util.Collection;
import androidx.camera.core.impl.utils.futures.Futures;
import java.util.ArrayList;
import java.util.List;
import androidx.camera.core.impl.ImageReaderProxy;
import java.util.concurrent.Executor;
import com.google.common.util.concurrent.ListenableFuture;
import androidx.concurrent.futures.CallbackToFutureAdapter;
import androidx.camera.core.impl.CaptureProcessor;

class CaptureProcessorPipeline implements CaptureProcessor
{
    private static final String TAG = "CaptureProcessorPipeline";
    CallbackToFutureAdapter.Completer<Void> mCloseCompleter;
    private ListenableFuture<Void> mCloseFuture;
    private boolean mClosed;
    final Executor mExecutor;
    private ImageReaderProxy mIntermediateImageReader;
    private final Object mLock;
    private final int mMaxImages;
    private final CaptureProcessor mPostCaptureProcessor;
    private final CaptureProcessor mPreCaptureProcessor;
    private boolean mProcessing;
    private ImageInfo mSourceImageInfo;
    private final ListenableFuture<List<Void>> mUnderlyingCaptureProcessorsCloseFuture;
    
    CaptureProcessorPipeline(final CaptureProcessor mPreCaptureProcessor, final int mMaxImages, final CaptureProcessor mPostCaptureProcessor, final Executor mExecutor) {
        this.mIntermediateImageReader = null;
        this.mSourceImageInfo = null;
        this.mLock = new Object();
        this.mClosed = false;
        this.mProcessing = false;
        this.mPreCaptureProcessor = mPreCaptureProcessor;
        this.mPostCaptureProcessor = mPostCaptureProcessor;
        final ArrayList list = new ArrayList();
        list.add(mPreCaptureProcessor.getCloseFuture());
        list.add(mPostCaptureProcessor.getCloseFuture());
        this.mUnderlyingCaptureProcessorsCloseFuture = (ListenableFuture<List<Void>>)Futures.allAsList((Collection<? extends com.google.common.util.concurrent.ListenableFuture<?>>)list);
        this.mExecutor = mExecutor;
        this.mMaxImages = mMaxImages;
    }
    
    private void closeAndCompleteFutureIfNecessary() {
        synchronized (this.mLock) {
            final boolean mClosed = this.mClosed;
            final boolean mProcessing = this.mProcessing;
            final CallbackToFutureAdapter.Completer<Void> mCloseCompleter = this.mCloseCompleter;
            if (mClosed && !mProcessing) {
                this.mIntermediateImageReader.close();
            }
            monitorexit(this.mLock);
            if (mClosed && !mProcessing && mCloseCompleter != null) {
                this.mUnderlyingCaptureProcessorsCloseFuture.addListener((Runnable)new CaptureProcessorPipeline$$ExternalSyntheticLambda4(mCloseCompleter), CameraXExecutors.directExecutor());
            }
        }
    }
    
    @Override
    public void close() {
        synchronized (this.mLock) {
            if (this.mClosed) {
                return;
            }
            this.mClosed = true;
            monitorexit(this.mLock);
            this.mPreCaptureProcessor.close();
            this.mPostCaptureProcessor.close();
            this.closeAndCompleteFutureIfNecessary();
        }
    }
    
    @Override
    public ListenableFuture<Void> getCloseFuture() {
        synchronized (this.mLock) {
            com.google.common.util.concurrent.ListenableFuture<Void> listenableFuture;
            if (this.mClosed && !this.mProcessing) {
                listenableFuture = Futures.transform(this.mUnderlyingCaptureProcessorsCloseFuture, (Function<? super List<Void>, ? extends Void>)new CaptureProcessorPipeline$$ExternalSyntheticLambda1(), CameraXExecutors.directExecutor());
            }
            else {
                if (this.mCloseFuture == null) {
                    this.mCloseFuture = CallbackToFutureAdapter.getFuture((CallbackToFutureAdapter.Resolver<Void>)new CaptureProcessorPipeline$$ExternalSyntheticLambda2(this));
                }
                listenableFuture = Futures.nonCancellationPropagating(this.mCloseFuture);
            }
            return listenableFuture;
        }
    }
    
    @Override
    public void onOutputSurface(final Surface surface, final int n) {
        this.mPostCaptureProcessor.onOutputSurface(surface, n);
    }
    
    @Override
    public void onResolutionUpdate(final Size size) {
        final AndroidImageReaderProxy mIntermediateImageReader = new AndroidImageReaderProxy(ImageReader.newInstance(size.getWidth(), size.getHeight(), 35, this.mMaxImages));
        this.mIntermediateImageReader = mIntermediateImageReader;
        this.mPreCaptureProcessor.onOutputSurface(mIntermediateImageReader.getSurface(), 35);
        this.mPreCaptureProcessor.onResolutionUpdate(size);
        this.mPostCaptureProcessor.onResolutionUpdate(size);
        this.mIntermediateImageReader.setOnImageAvailableListener((ImageReaderProxy.OnImageAvailableListener)new CaptureProcessorPipeline$$ExternalSyntheticLambda0(this), CameraXExecutors.directExecutor());
    }
    
    void postProcess(final ImageProxy imageProxy) {
        Object mLock = this.mLock;
        synchronized (mLock) {
            final boolean mClosed = this.mClosed;
            monitorexit(mLock);
            if (!mClosed) {
                final Size size = new Size(imageProxy.getWidth(), imageProxy.getHeight());
                Preconditions.checkNotNull(this.mSourceImageInfo);
                mLock = this.mSourceImageInfo.getTagBundle().listKeys().iterator().next();
                final int intValue = (int)this.mSourceImageInfo.getTagBundle().getTag((String)mLock);
                final SettableImageProxy settableImageProxy = new SettableImageProxy(imageProxy, size, this.mSourceImageInfo);
                this.mSourceImageInfo = null;
                mLock = new SettableImageProxyBundle(Collections.singletonList(intValue), (String)mLock);
                ((SettableImageProxyBundle)mLock).addImageProxy(settableImageProxy);
                try {
                    this.mPostCaptureProcessor.process((ImageProxyBundle)mLock);
                }
                catch (final Exception mLock) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Post processing image failed! ");
                    sb.append(((Throwable)mLock).getMessage());
                    Logger.e("CaptureProcessorPipeline", sb.toString());
                }
            }
            synchronized (this.mLock) {
                this.mProcessing = false;
                monitorexit(this.mLock);
                this.closeAndCompleteFutureIfNecessary();
            }
        }
    }
    
    @Override
    public void process(final ImageProxyBundle imageProxyBundle) {
        Object o = this.mLock;
        synchronized (o) {
            if (this.mClosed) {
                return;
            }
            this.mProcessing = true;
            monitorexit(o);
            o = imageProxyBundle.getImageProxy(imageProxyBundle.getCaptureIds().get(0));
            Preconditions.checkArgument(((ListenableFuture)o).isDone());
            try {
                this.mSourceImageInfo = ((ImageProxy)((ListenableFuture)o).get()).getImageInfo();
                this.mPreCaptureProcessor.process(imageProxyBundle);
            }
            catch (final ExecutionException | InterruptedException ex) {
                throw new IllegalArgumentException("Can not successfully extract ImageProxy from the ImageProxyBundle.");
            }
        }
    }
}

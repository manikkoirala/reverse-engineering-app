// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core;

public final class FocusMeteringResult
{
    private boolean mIsFocusSuccessful;
    
    private FocusMeteringResult(final boolean mIsFocusSuccessful) {
        this.mIsFocusSuccessful = mIsFocusSuccessful;
    }
    
    public static FocusMeteringResult create(final boolean b) {
        return new FocusMeteringResult(b);
    }
    
    public static FocusMeteringResult emptyInstance() {
        return new FocusMeteringResult(false);
    }
    
    public boolean isFocusSuccessful() {
        return this.mIsFocusSuccessful;
    }
}

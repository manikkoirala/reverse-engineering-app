// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.view.video;

import androidx.camera.core.VideoCapture;
import java.io.File;
import androidx.core.util.Preconditions;
import android.os.Build$VERSION;
import android.os.ParcelFileDescriptor;
import android.content.ContentValues;
import android.net.Uri;
import android.content.ContentResolver;

public abstract class OutputFileOptions
{
    private static final Metadata EMPTY_METADATA;
    
    static {
        EMPTY_METADATA = Metadata.builder().build();
    }
    
    OutputFileOptions() {
    }
    
    public static Builder builder(final ContentResolver contentResolver, final Uri saveCollection, final ContentValues contentValues) {
        return new AutoValue_OutputFileOptions.Builder().setMetadata(OutputFileOptions.EMPTY_METADATA).setContentResolver(contentResolver).setSaveCollection(saveCollection).setContentValues(contentValues);
    }
    
    public static Builder builder(final ParcelFileDescriptor fileDescriptor) {
        Preconditions.checkArgument(Build$VERSION.SDK_INT >= 26, (Object)"Using a ParcelFileDescriptor to record a video is only supported for Android 8.0 or above.");
        return new AutoValue_OutputFileOptions.Builder().setMetadata(OutputFileOptions.EMPTY_METADATA).setFileDescriptor(fileDescriptor);
    }
    
    public static Builder builder(final File file) {
        return new AutoValue_OutputFileOptions.Builder().setMetadata(OutputFileOptions.EMPTY_METADATA).setFile(file);
    }
    
    private boolean isSavingToFile() {
        return this.getFile() != null;
    }
    
    private boolean isSavingToFileDescriptor() {
        return this.getFileDescriptor() != null;
    }
    
    private boolean isSavingToMediaStore() {
        return this.getSaveCollection() != null && this.getContentResolver() != null && this.getContentValues() != null;
    }
    
    abstract ContentResolver getContentResolver();
    
    abstract ContentValues getContentValues();
    
    abstract File getFile();
    
    abstract ParcelFileDescriptor getFileDescriptor();
    
    public abstract Metadata getMetadata();
    
    abstract Uri getSaveCollection();
    
    public VideoCapture.OutputFileOptions toVideoCaptureOutputFileOptions() {
        VideoCapture.OutputFileOptions.Builder builder;
        if (this.isSavingToFile()) {
            builder = new VideoCapture.OutputFileOptions.Builder(Preconditions.checkNotNull(this.getFile()));
        }
        else if (this.isSavingToFileDescriptor()) {
            builder = new VideoCapture.OutputFileOptions.Builder(Preconditions.checkNotNull(this.getFileDescriptor()).getFileDescriptor());
        }
        else {
            Preconditions.checkState(this.isSavingToMediaStore());
            builder = new VideoCapture.OutputFileOptions.Builder(Preconditions.checkNotNull(this.getContentResolver()), Preconditions.checkNotNull(this.getSaveCollection()), Preconditions.checkNotNull(this.getContentValues()));
        }
        final VideoCapture.Metadata metadata = new VideoCapture.Metadata();
        metadata.location = this.getMetadata().getLocation();
        builder.setMetadata(metadata);
        return builder.build();
    }
    
    public abstract static class Builder
    {
        Builder() {
        }
        
        public abstract OutputFileOptions build();
        
        abstract Builder setContentResolver(final ContentResolver p0);
        
        abstract Builder setContentValues(final ContentValues p0);
        
        abstract Builder setFile(final File p0);
        
        abstract Builder setFileDescriptor(final ParcelFileDescriptor p0);
        
        public abstract Builder setMetadata(final Metadata p0);
        
        abstract Builder setSaveCollection(final Uri p0);
    }
}

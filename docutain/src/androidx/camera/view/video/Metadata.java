// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.view.video;

import android.location.Location;

public abstract class Metadata
{
    Metadata() {
    }
    
    public static Builder builder() {
        return (Builder)new AutoValue_Metadata.Builder();
    }
    
    public abstract Location getLocation();
    
    public abstract static class Builder
    {
        Builder() {
        }
        
        public abstract Metadata build();
        
        public abstract Builder setLocation(final Location p0);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.view.video;

import android.net.Uri;

final class AutoValue_OutputFileResults extends OutputFileResults
{
    private final Uri savedUri;
    
    AutoValue_OutputFileResults(final Uri savedUri) {
        this.savedUri = savedUri;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean equals = true;
        if (o == this) {
            return true;
        }
        if (o instanceof OutputFileResults) {
            final OutputFileResults outputFileResults = (OutputFileResults)o;
            final Uri savedUri = this.savedUri;
            final Uri savedUri2 = outputFileResults.getSavedUri();
            if (savedUri == null) {
                if (savedUri2 != null) {
                    equals = false;
                }
            }
            else {
                equals = savedUri.equals((Object)savedUri2);
            }
            return equals;
        }
        return false;
    }
    
    @Override
    public Uri getSavedUri() {
        return this.savedUri;
    }
    
    @Override
    public int hashCode() {
        final Uri savedUri = this.savedUri;
        int hashCode;
        if (savedUri == null) {
            hashCode = 0;
        }
        else {
            hashCode = savedUri.hashCode();
        }
        return hashCode ^ 0xF4243;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("OutputFileResults{savedUri=");
        sb.append(this.savedUri);
        sb.append("}");
        return sb.toString();
    }
}

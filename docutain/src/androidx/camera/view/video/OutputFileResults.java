// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.view.video;

import android.net.Uri;

public abstract class OutputFileResults
{
    OutputFileResults() {
    }
    
    public static OutputFileResults create(final Uri uri) {
        return new AutoValue_OutputFileResults(uri);
    }
    
    public abstract Uri getSavedUri();
}

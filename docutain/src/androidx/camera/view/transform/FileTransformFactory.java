// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.view.transform;

import android.graphics.Matrix;
import androidx.camera.core.impl.utils.TransformUtils;
import android.graphics.Rect;
import androidx.camera.core.impl.utils.Exif;
import java.io.FileInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import android.net.Uri;
import android.content.ContentResolver;

public final class FileTransformFactory
{
    private boolean mUsingExifOrientation;
    
    public OutputTransform getOutputTransform(ContentResolver openInputStream, final Uri uri) throws IOException {
        openInputStream = (ContentResolver)openInputStream.openInputStream(uri);
        try {
            final OutputTransform outputTransform = this.getOutputTransform((InputStream)openInputStream);
            if (openInputStream != null) {
                ((InputStream)openInputStream).close();
            }
            return outputTransform;
        }
        finally {
            if (openInputStream != null) {
                try {
                    ((InputStream)openInputStream).close();
                }
                finally {
                    final Throwable exception;
                    ((Throwable)uri).addSuppressed(exception);
                }
            }
        }
    }
    
    public OutputTransform getOutputTransform(File file) throws IOException {
        file = (File)new FileInputStream(file);
        try {
            final OutputTransform outputTransform = this.getOutputTransform((InputStream)file);
            ((InputStream)file).close();
            return outputTransform;
        }
        finally {
            try {
                ((InputStream)file).close();
            }
            finally {
                final Throwable t;
                final Throwable exception;
                t.addSuppressed(exception);
            }
        }
    }
    
    public OutputTransform getOutputTransform(final InputStream inputStream) throws IOException {
        final Exif fromInputStream = Exif.createFromInputStream(inputStream);
        final Rect rect = new Rect(0, 0, fromInputStream.getWidth(), fromInputStream.getHeight());
        final Matrix normalizedToBuffer = TransformUtils.getNormalizedToBuffer(rect);
        if (this.mUsingExifOrientation) {
            normalizedToBuffer.postConcat(TransformUtils.getExifTransform(fromInputStream.getOrientation(), fromInputStream.getWidth(), fromInputStream.getHeight()));
        }
        return new OutputTransform(normalizedToBuffer, TransformUtils.rectToSize(rect));
    }
    
    public boolean isUsingExifOrientation() {
        return this.mUsingExifOrientation;
    }
    
    public void setUsingExifOrientation(final boolean mUsingExifOrientation) {
        this.mUsingExifOrientation = mUsingExifOrientation;
    }
}

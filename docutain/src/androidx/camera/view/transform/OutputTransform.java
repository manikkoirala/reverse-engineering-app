// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.view.transform;

import android.util.Size;
import android.graphics.Matrix;

public final class OutputTransform
{
    final Matrix mMatrix;
    final Size mViewPortSize;
    
    public OutputTransform(final Matrix mMatrix, final Size mViewPortSize) {
        this.mMatrix = mMatrix;
        this.mViewPortSize = mViewPortSize;
    }
    
    public Matrix getMatrix() {
        return this.mMatrix;
    }
    
    Size getViewPortSize() {
        return this.mViewPortSize;
    }
}

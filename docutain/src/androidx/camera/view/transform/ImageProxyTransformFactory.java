// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.view.transform;

import android.graphics.Matrix;
import androidx.camera.core.impl.utils.TransformUtils;
import android.graphics.RectF;
import androidx.camera.core.ImageProxy;

public final class ImageProxyTransformFactory
{
    private boolean mUsingCropRect;
    private boolean mUsingRotationDegrees;
    
    private RectF getCropRect(final ImageProxy imageProxy) {
        if (this.mUsingCropRect) {
            return new RectF(imageProxy.getCropRect());
        }
        return new RectF(0.0f, 0.0f, (float)imageProxy.getWidth(), (float)imageProxy.getHeight());
    }
    
    static RectF getRotatedCropRect(final RectF rectF, final int n) {
        if (TransformUtils.is90or270(n)) {
            return new RectF(0.0f, 0.0f, rectF.height(), rectF.width());
        }
        return new RectF(0.0f, 0.0f, rectF.width(), rectF.height());
    }
    
    private int getRotationDegrees(final ImageProxy imageProxy) {
        if (this.mUsingRotationDegrees) {
            return imageProxy.getImageInfo().getRotationDegrees();
        }
        return 0;
    }
    
    public OutputTransform getOutputTransform(final ImageProxy imageProxy) {
        final int rotationDegrees = this.getRotationDegrees(imageProxy);
        final RectF cropRect = this.getCropRect(imageProxy);
        final Matrix rectToRect = TransformUtils.getRectToRect(cropRect, getRotatedCropRect(cropRect, rotationDegrees), rotationDegrees);
        rectToRect.preConcat(TransformUtils.getNormalizedToBuffer(imageProxy.getCropRect()));
        return new OutputTransform(rectToRect, TransformUtils.rectToSize(imageProxy.getCropRect()));
    }
    
    public boolean isUsingCropRect() {
        return this.mUsingCropRect;
    }
    
    public boolean isUsingRotationDegrees() {
        return this.mUsingRotationDegrees;
    }
    
    public void setUsingCropRect(final boolean mUsingCropRect) {
        this.mUsingCropRect = mUsingCropRect;
    }
    
    public void setUsingRotationDegrees(final boolean mUsingRotationDegrees) {
        this.mUsingRotationDegrees = mUsingRotationDegrees;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.view.transform;

import android.graphics.RectF;
import android.graphics.PointF;
import androidx.core.util.Preconditions;
import androidx.camera.core.Logger;
import androidx.camera.core.impl.utils.TransformUtils;
import android.graphics.Matrix;

public final class CoordinateTransform
{
    private static final String MISMATCH_MSG = "The source viewport (%s) does not match the target viewport (%s). Please make sure they are associated with the same Viewport.";
    private static final String TAG = "CoordinateTransform";
    private final Matrix mMatrix;
    
    public CoordinateTransform(final OutputTransform outputTransform, final OutputTransform outputTransform2) {
        if (!TransformUtils.isAspectRatioMatchingWithRoundingError(outputTransform.getViewPortSize(), false, outputTransform2.getViewPortSize(), false)) {
            Logger.w("CoordinateTransform", String.format("The source viewport (%s) does not match the target viewport (%s). Please make sure they are associated with the same Viewport.", outputTransform.getViewPortSize(), outputTransform2.getViewPortSize()));
        }
        final Matrix mMatrix = new Matrix();
        this.mMatrix = mMatrix;
        Preconditions.checkState(outputTransform.getMatrix().invert(mMatrix), "The source transform cannot be inverted");
        mMatrix.postConcat(outputTransform2.getMatrix());
    }
    
    public void mapPoint(final PointF pointF) {
        final float[] array = { pointF.x, pointF.y };
        this.mMatrix.mapPoints(array);
        pointF.x = array[0];
        pointF.y = array[1];
    }
    
    public void mapPoints(final float[] array) {
        this.mMatrix.mapPoints(array);
    }
    
    public void mapRect(final RectF rectF) {
        this.mMatrix.mapRect(rectF);
    }
    
    public void transform(final Matrix matrix) {
        matrix.set(this.mMatrix);
    }
}

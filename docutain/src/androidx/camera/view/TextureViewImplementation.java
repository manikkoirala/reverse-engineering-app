// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.view;

import androidx.core.util.Consumer;
import java.util.Objects;
import androidx.camera.core.impl.utils.executor.CameraXExecutors;
import android.view.Surface;
import androidx.camera.core.impl.utils.futures.Futures;
import androidx.core.content.ContextCompat;
import androidx.camera.core.impl.utils.futures.FutureCallback;
import androidx.camera.core.Logger;
import android.view.TextureView$SurfaceTextureListener;
import android.view.ViewGroup$LayoutParams;
import android.widget.FrameLayout$LayoutParams;
import androidx.core.util.Preconditions;
import android.graphics.Bitmap;
import android.view.View;
import android.widget.FrameLayout;
import android.view.TextureView;
import androidx.camera.core.SurfaceRequest;
import com.google.common.util.concurrent.ListenableFuture;
import androidx.concurrent.futures.CallbackToFutureAdapter;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.Executor;
import android.graphics.SurfaceTexture;

final class TextureViewImplementation extends PreviewViewImplementation
{
    private static final String TAG = "TextureViewImpl";
    SurfaceTexture mDetachedSurfaceTexture;
    Executor mFrameUpdateExecutor;
    boolean mIsSurfaceTextureDetachedFromView;
    AtomicReference<CallbackToFutureAdapter.Completer<Void>> mNextFrameCompleter;
    PreviewView.OnFrameUpdateListener mOnFrameUpdateListener;
    OnSurfaceNotInUseListener mOnSurfaceNotInUseListener;
    ListenableFuture<SurfaceRequest.Result> mSurfaceReleaseFuture;
    SurfaceRequest mSurfaceRequest;
    SurfaceTexture mSurfaceTexture;
    TextureView mTextureView;
    
    TextureViewImplementation(final FrameLayout frameLayout, final PreviewTransformation previewTransformation) {
        super(frameLayout, previewTransformation);
        this.mIsSurfaceTextureDetachedFromView = false;
        this.mNextFrameCompleter = new AtomicReference<CallbackToFutureAdapter.Completer<Void>>();
    }
    
    private void notifySurfaceNotInUse() {
        final OnSurfaceNotInUseListener mOnSurfaceNotInUseListener = this.mOnSurfaceNotInUseListener;
        if (mOnSurfaceNotInUseListener != null) {
            mOnSurfaceNotInUseListener.onSurfaceNotInUse();
            this.mOnSurfaceNotInUseListener = null;
        }
    }
    
    private void reattachSurfaceTexture() {
        if (this.mIsSurfaceTextureDetachedFromView && this.mDetachedSurfaceTexture != null) {
            final SurfaceTexture surfaceTexture = this.mTextureView.getSurfaceTexture();
            final SurfaceTexture mDetachedSurfaceTexture = this.mDetachedSurfaceTexture;
            if (surfaceTexture != mDetachedSurfaceTexture) {
                this.mTextureView.setSurfaceTexture(mDetachedSurfaceTexture);
                this.mDetachedSurfaceTexture = null;
                this.mIsSurfaceTextureDetachedFromView = false;
            }
        }
    }
    
    @Override
    View getPreview() {
        return (View)this.mTextureView;
    }
    
    @Override
    Bitmap getPreviewBitmap() {
        final TextureView mTextureView = this.mTextureView;
        if (mTextureView != null && mTextureView.isAvailable()) {
            return this.mTextureView.getBitmap();
        }
        return null;
    }
    
    public void initializePreview() {
        Preconditions.checkNotNull(this.mParent);
        Preconditions.checkNotNull(this.mResolution);
        (this.mTextureView = new TextureView(this.mParent.getContext())).setLayoutParams((ViewGroup$LayoutParams)new FrameLayout$LayoutParams(this.mResolution.getWidth(), this.mResolution.getHeight()));
        this.mTextureView.setSurfaceTextureListener((TextureView$SurfaceTextureListener)new TextureView$SurfaceTextureListener(this) {
            final TextureViewImplementation this$0;
            
            public void onSurfaceTextureAvailable(final SurfaceTexture mSurfaceTexture, final int i, final int j) {
                final StringBuilder sb = new StringBuilder();
                sb.append("SurfaceTexture available. Size: ");
                sb.append(i);
                sb.append("x");
                sb.append(j);
                Logger.d("TextureViewImpl", sb.toString());
                this.this$0.mSurfaceTexture = mSurfaceTexture;
                if (this.this$0.mSurfaceReleaseFuture != null) {
                    Preconditions.checkNotNull(this.this$0.mSurfaceRequest);
                    final StringBuilder sb2 = new StringBuilder();
                    sb2.append("Surface invalidated ");
                    sb2.append(this.this$0.mSurfaceRequest);
                    Logger.d("TextureViewImpl", sb2.toString());
                    this.this$0.mSurfaceRequest.getDeferrableSurface().close();
                }
                else {
                    this.this$0.tryToProvidePreviewSurface();
                }
            }
            
            public boolean onSurfaceTextureDestroyed(final SurfaceTexture mDetachedSurfaceTexture) {
                this.this$0.mSurfaceTexture = null;
                if (this.this$0.mSurfaceReleaseFuture != null) {
                    Futures.addCallback(this.this$0.mSurfaceReleaseFuture, new FutureCallback<SurfaceRequest.Result>(this, mDetachedSurfaceTexture) {
                        final TextureViewImplementation$1 this$1;
                        final SurfaceTexture val$surfaceTexture;
                        
                        @Override
                        public void onFailure(final Throwable cause) {
                            throw new IllegalStateException("SurfaceReleaseFuture did not complete nicely.", cause);
                        }
                        
                        @Override
                        public void onSuccess(final SurfaceRequest.Result result) {
                            Preconditions.checkState(result.getResultCode() != 3, "Unexpected result from SurfaceRequest. Surface was provided twice.");
                            Logger.d("TextureViewImpl", "SurfaceTexture about to manually be destroyed");
                            this.val$surfaceTexture.release();
                            if (this.this$1.this$0.mDetachedSurfaceTexture != null) {
                                this.this$1.this$0.mDetachedSurfaceTexture = null;
                            }
                        }
                    }, ContextCompat.getMainExecutor(this.this$0.mTextureView.getContext()));
                    this.this$0.mDetachedSurfaceTexture = mDetachedSurfaceTexture;
                    return false;
                }
                Logger.d("TextureViewImpl", "SurfaceTexture about to be destroyed");
                return true;
            }
            
            public void onSurfaceTextureSizeChanged(final SurfaceTexture surfaceTexture, final int i, final int j) {
                final StringBuilder sb = new StringBuilder();
                sb.append("SurfaceTexture size changed: ");
                sb.append(i);
                sb.append("x");
                sb.append(j);
                Logger.d("TextureViewImpl", sb.toString());
            }
            
            public void onSurfaceTextureUpdated(final SurfaceTexture surfaceTexture) {
                final CallbackToFutureAdapter.Completer completer = this.this$0.mNextFrameCompleter.getAndSet(null);
                if (completer != null) {
                    completer.set(null);
                }
                final PreviewView.OnFrameUpdateListener mOnFrameUpdateListener = this.this$0.mOnFrameUpdateListener;
                final Executor mFrameUpdateExecutor = this.this$0.mFrameUpdateExecutor;
                if (mOnFrameUpdateListener != null && mFrameUpdateExecutor != null) {
                    mFrameUpdateExecutor.execute(new TextureViewImplementation$1$$ExternalSyntheticLambda0(mOnFrameUpdateListener, surfaceTexture));
                }
            }
        });
        this.mParent.removeAllViews();
        this.mParent.addView((View)this.mTextureView);
    }
    
    @Override
    void onAttachedToWindow() {
        this.reattachSurfaceTexture();
    }
    
    @Override
    void onDetachedFromWindow() {
        this.mIsSurfaceTextureDetachedFromView = true;
    }
    
    @Override
    void onSurfaceRequested(final SurfaceRequest mSurfaceRequest, final OnSurfaceNotInUseListener mOnSurfaceNotInUseListener) {
        this.mResolution = mSurfaceRequest.getResolution();
        this.mOnSurfaceNotInUseListener = mOnSurfaceNotInUseListener;
        this.initializePreview();
        final SurfaceRequest mSurfaceRequest2 = this.mSurfaceRequest;
        if (mSurfaceRequest2 != null) {
            mSurfaceRequest2.willNotProvideSurface();
        }
        (this.mSurfaceRequest = mSurfaceRequest).addRequestCancellationListener(ContextCompat.getMainExecutor(this.mTextureView.getContext()), new TextureViewImplementation$$ExternalSyntheticLambda4(this, mSurfaceRequest));
        this.tryToProvidePreviewSurface();
    }
    
    @Override
    void setFrameUpdateListener(final Executor mFrameUpdateExecutor, final PreviewView.OnFrameUpdateListener mOnFrameUpdateListener) {
        this.mOnFrameUpdateListener = mOnFrameUpdateListener;
        this.mFrameUpdateExecutor = mFrameUpdateExecutor;
    }
    
    void tryToProvidePreviewSurface() {
        if (this.mResolution != null) {
            final SurfaceTexture mSurfaceTexture = this.mSurfaceTexture;
            if (mSurfaceTexture != null) {
                if (this.mSurfaceRequest != null) {
                    mSurfaceTexture.setDefaultBufferSize(this.mResolution.getWidth(), this.mResolution.getHeight());
                    final Surface surface = new Surface(this.mSurfaceTexture);
                    final SurfaceRequest mSurfaceRequest = this.mSurfaceRequest;
                    final com.google.common.util.concurrent.ListenableFuture<SurfaceRequest.Result> future = CallbackToFutureAdapter.getFuture((CallbackToFutureAdapter.Resolver<SurfaceRequest.Result>)new TextureViewImplementation$$ExternalSyntheticLambda0(this, surface));
                    (this.mSurfaceReleaseFuture = future).addListener((Runnable)new TextureViewImplementation$$ExternalSyntheticLambda1(this, surface, future, mSurfaceRequest), ContextCompat.getMainExecutor(this.mTextureView.getContext()));
                    this.onSurfaceProvided();
                }
            }
        }
    }
    
    @Override
    ListenableFuture<Void> waitForNextFrame() {
        return CallbackToFutureAdapter.getFuture((CallbackToFutureAdapter.Resolver<Void>)new TextureViewImplementation$$ExternalSyntheticLambda2(this));
    }
}

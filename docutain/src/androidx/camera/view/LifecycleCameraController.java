// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.view;

import androidx.camera.core.UseCaseGroup;
import android.util.Log;
import androidx.camera.core.Camera;
import androidx.camera.core.impl.utils.Threads;
import android.content.Context;
import androidx.lifecycle.LifecycleOwner;

public final class LifecycleCameraController extends CameraController
{
    private static final String TAG = "CamLifecycleController";
    private LifecycleOwner mLifecycleOwner;
    
    public LifecycleCameraController(final Context context) {
        super(context);
    }
    
    public void bindToLifecycle(final LifecycleOwner mLifecycleOwner) {
        Threads.checkMainThread();
        this.mLifecycleOwner = mLifecycleOwner;
        this.startCameraAndTrackStates();
    }
    
    void shutDownForTests() {
        if (this.mCameraProvider != null) {
            this.mCameraProvider.unbindAll();
            this.mCameraProvider.shutdown();
        }
    }
    
    @Override
    Camera startCamera() {
        if (this.mLifecycleOwner == null) {
            Log.d("CamLifecycleController", "Lifecycle is not set.");
            return null;
        }
        if (this.mCameraProvider == null) {
            Log.d("CamLifecycleController", "CameraProvider is not ready.");
            return null;
        }
        final UseCaseGroup useCaseGroup = this.createUseCaseGroup();
        if (useCaseGroup == null) {
            return null;
        }
        return this.mCameraProvider.bindToLifecycle(this.mLifecycleOwner, this.mCameraSelector, useCaseGroup);
    }
    
    public void unbind() {
        Threads.checkMainThread();
        this.mLifecycleOwner = null;
        this.mCamera = null;
        if (this.mCameraProvider != null) {
            this.mCameraProvider.unbindAll();
        }
    }
}

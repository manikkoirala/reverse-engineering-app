// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.view;

import java.util.Iterator;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.Executor;
import java.util.Collection;
import java.util.ArrayList;
import java.util.HashMap;
import android.content.Context;
import android.view.OrientationEventListener;
import java.util.Map;

public final class RotationProvider
{
    boolean mIgnoreCanDetectForTest;
    final Map<Listener, ListenerWrapper> mListeners;
    final Object mLock;
    final OrientationEventListener mOrientationListener;
    
    public RotationProvider(final Context context) {
        this.mLock = new Object();
        this.mListeners = new HashMap<Listener, ListenerWrapper>();
        this.mIgnoreCanDetectForTest = false;
        this.mOrientationListener = new OrientationEventListener(this, context) {
            private static final int INVALID_SURFACE_ROTATION = -1;
            private int mRotation = -1;
            final RotationProvider this$0;
            
            public void onOrientationChanged(int orientationToSurfaceRotation) {
                if (orientationToSurfaceRotation == -1) {
                    return;
                }
                orientationToSurfaceRotation = RotationProvider.orientationToSurfaceRotation(orientationToSurfaceRotation);
                if (this.mRotation != orientationToSurfaceRotation) {
                    this.mRotation = orientationToSurfaceRotation;
                    Object o = this.this$0.mLock;
                    synchronized (o) {
                        final ArrayList list = new ArrayList(this.this$0.mListeners.values());
                        monitorexit(o);
                        if (!list.isEmpty()) {
                            o = list.iterator();
                            while (((Iterator)o).hasNext()) {
                                ((ListenerWrapper)((Iterator)o).next()).onRotationChanged(orientationToSurfaceRotation);
                            }
                        }
                    }
                }
            }
        };
    }
    
    static int orientationToSurfaceRotation(final int n) {
        if (n >= 315 || n < 45) {
            return 0;
        }
        if (n >= 225) {
            return 1;
        }
        if (n >= 135) {
            return 2;
        }
        return 3;
    }
    
    public boolean addListener(final Executor executor, final Listener listener) {
        synchronized (this.mLock) {
            if (!this.mOrientationListener.canDetectOrientation() && !this.mIgnoreCanDetectForTest) {
                return false;
            }
            this.mListeners.put(listener, new ListenerWrapper(listener, executor));
            this.mOrientationListener.enable();
            return true;
        }
    }
    
    public void removeListener(final Listener listener) {
        synchronized (this.mLock) {
            final ListenerWrapper listenerWrapper = this.mListeners.get(listener);
            if (listenerWrapper != null) {
                listenerWrapper.disable();
                this.mListeners.remove(listener);
            }
            if (this.mListeners.isEmpty()) {
                this.mOrientationListener.disable();
            }
        }
    }
    
    public interface Listener
    {
        void onRotationChanged(final int p0);
    }
    
    private static class ListenerWrapper
    {
        private final AtomicBoolean mEnabled;
        private final Executor mExecutor;
        private final Listener mListener;
        
        ListenerWrapper(final Listener mListener, final Executor mExecutor) {
            this.mListener = mListener;
            this.mExecutor = mExecutor;
            this.mEnabled = new AtomicBoolean(true);
        }
        
        void disable() {
            this.mEnabled.set(false);
        }
        
        void onRotationChanged(final int n) {
            this.mExecutor.execute(new RotationProvider$ListenerWrapper$$ExternalSyntheticLambda0(this, n));
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.view;

import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;
import android.util.Size;
import android.graphics.Matrix;
import androidx.camera.view.transform.OutputTransform;
import androidx.camera.view.video.OutputFileResults;
import androidx.camera.view.video.OnVideoSavedCallback;
import androidx.camera.view.video.OutputFileOptions;
import androidx.camera.core.FocusMeteringResult;
import androidx.camera.core.impl.utils.futures.FutureCallback;
import androidx.camera.core.FocusMeteringAction;
import androidx.camera.core.MeteringPointFactory;
import androidx.camera.core.CameraInfoUnavailableException;
import androidx.core.util.Preconditions;
import androidx.lifecycle.LiveData;
import androidx.camera.core.CameraInfo;
import androidx.camera.core.CameraControl;
import java.util.Iterator;
import androidx.camera.core.UseCaseGroup;
import androidx.camera.core.UseCase;
import androidx.camera.core.impl.utils.Threads;
import androidx.camera.core.Logger;
import androidx.camera.core.impl.ImageOutputConfig;
import java.util.Objects;
import android.os.Build$VERSION;
import androidx.arch.core.util.Function;
import androidx.camera.core.impl.utils.futures.Futures;
import androidx.camera.core.impl.utils.executor.CameraXExecutors;
import java.util.Collections;
import androidx.camera.core.ZoomState;
import androidx.camera.core.ViewPort;
import java.util.concurrent.atomic.AtomicBoolean;
import androidx.camera.core.VideoCapture;
import androidx.lifecycle.MutableLiveData;
import android.view.Display;
import androidx.camera.core.Preview;
import com.google.common.util.concurrent.ListenableFuture;
import androidx.camera.core.ImageCapture;
import androidx.camera.core.CameraEffect;
import java.util.List;
import androidx.camera.core.CameraSelector;
import androidx.camera.lifecycle.ProcessCameraProvider;
import androidx.camera.core.Camera;
import android.content.Context;
import java.util.concurrent.Executor;
import androidx.camera.core.ImageAnalysis;

public abstract class CameraController
{
    private static final float AE_SIZE = 0.25f;
    private static final float AF_SIZE = 0.16666667f;
    private static final String CAMERA_NOT_ATTACHED = "Use cases not attached to camera.";
    private static final String CAMERA_NOT_INITIALIZED = "Camera not initialized.";
    public static final int COORDINATE_SYSTEM_VIEW_REFERENCED = 1;
    public static final int IMAGE_ANALYSIS = 2;
    public static final int IMAGE_CAPTURE = 1;
    private static final String IMAGE_CAPTURE_DISABLED = "ImageCapture disabled.";
    private static final String PREVIEW_VIEW_NOT_ATTACHED = "PreviewView not attached to CameraController.";
    private static final String TAG = "CameraController";
    public static final int TAP_TO_FOCUS_FAILED = 4;
    public static final int TAP_TO_FOCUS_FOCUSED = 2;
    public static final int TAP_TO_FOCUS_NOT_FOCUSED = 3;
    public static final int TAP_TO_FOCUS_NOT_STARTED = 0;
    public static final int TAP_TO_FOCUS_STARTED = 1;
    public static final int VIDEO_CAPTURE = 4;
    private static final String VIDEO_CAPTURE_DISABLED = "VideoCapture disabled.";
    private ImageAnalysis.Analyzer mAnalysisAnalyzer;
    private Executor mAnalysisBackgroundExecutor;
    private Executor mAnalysisExecutor;
    private final Context mAppContext;
    Camera mCamera;
    ProcessCameraProvider mCameraProvider;
    CameraSelector mCameraSelector;
    final RotationProvider.Listener mDeviceRotationListener;
    private List<CameraEffect> mEffects;
    private int mEnabledUseCases;
    ImageAnalysis mImageAnalysis;
    OutputSize mImageAnalysisTargetSize;
    ImageCapture mImageCapture;
    Executor mImageCaptureIoExecutor;
    OutputSize mImageCaptureTargetSize;
    private final ListenableFuture<Void> mInitializationFuture;
    private boolean mPinchToZoomEnabled;
    Preview mPreview;
    Display mPreviewDisplay;
    OutputSize mPreviewTargetSize;
    private final RotationProvider mRotationProvider;
    Preview.SurfaceProvider mSurfaceProvider;
    private boolean mTapToFocusEnabled;
    final MutableLiveData<Integer> mTapToFocusState;
    private final ForwardingLiveData<Integer> mTorchState;
    VideoCapture mVideoCapture;
    OutputSize mVideoCaptureOutputSize;
    final AtomicBoolean mVideoIsRecording;
    ViewPort mViewPort;
    private final ForwardingLiveData<ZoomState> mZoomState;
    
    CameraController(Context applicationContext) {
        this.mCameraSelector = CameraSelector.DEFAULT_BACK_CAMERA;
        this.mEnabledUseCases = 3;
        this.mVideoIsRecording = new AtomicBoolean(false);
        this.mPinchToZoomEnabled = true;
        this.mTapToFocusEnabled = true;
        this.mZoomState = new ForwardingLiveData<ZoomState>();
        this.mTorchState = new ForwardingLiveData<Integer>();
        this.mTapToFocusState = new MutableLiveData<Integer>(0);
        this.mEffects = Collections.emptyList();
        applicationContext = getApplicationContext(applicationContext);
        this.mAppContext = applicationContext;
        this.mPreview = new Preview.Builder().build();
        this.mImageCapture = new ImageCapture.Builder().build();
        this.mImageAnalysis = new ImageAnalysis.Builder().build();
        this.mVideoCapture = new VideoCapture.Builder().build();
        this.mInitializationFuture = Futures.transform(ProcessCameraProvider.getInstance(applicationContext), (Function<? super ProcessCameraProvider, ? extends Void>)new CameraController$$ExternalSyntheticLambda1(this), (Executor)CameraXExecutors.mainThreadExecutor());
        this.mRotationProvider = new RotationProvider(applicationContext);
        this.mDeviceRotationListener = new CameraController$$ExternalSyntheticLambda2(this);
    }
    
    private static Context getApplicationContext(final Context context) {
        final Context applicationContext = context.getApplicationContext();
        if (Build$VERSION.SDK_INT >= 30) {
            final String attributionTag = Api30Impl.getAttributionTag(context);
            if (attributionTag != null) {
                return Api30Impl.createAttributionContext(applicationContext, attributionTag);
            }
        }
        return applicationContext;
    }
    
    private boolean isCameraAttached() {
        return this.mCamera != null;
    }
    
    private boolean isCameraInitialized() {
        return this.mCameraProvider != null;
    }
    
    private boolean isOutputSizeEqual(final OutputSize outputSize, final OutputSize obj) {
        boolean b = true;
        if (outputSize == obj) {
            return true;
        }
        if (outputSize == null || !outputSize.equals(obj)) {
            b = false;
        }
        return b;
    }
    
    private boolean isPreviewViewAttached() {
        return this.mSurfaceProvider != null && this.mViewPort != null && this.mPreviewDisplay != null;
    }
    
    private boolean isUseCaseEnabled(final int n) {
        return (n & this.mEnabledUseCases) != 0x0;
    }
    
    private void restartCameraIfAnalyzerResolutionChanged(final ImageAnalysis.Analyzer analyzer, final ImageAnalysis.Analyzer analyzer2) {
        final Object o = null;
        Object defaultTargetResolution;
        if (analyzer == null) {
            defaultTargetResolution = null;
        }
        else {
            defaultTargetResolution = analyzer.getDefaultTargetResolution();
        }
        Object defaultTargetResolution2;
        if (analyzer2 == null) {
            defaultTargetResolution2 = o;
        }
        else {
            defaultTargetResolution2 = analyzer2.getDefaultTargetResolution();
        }
        if (!Objects.equals(defaultTargetResolution, defaultTargetResolution2)) {
            this.unbindImageAnalysisAndRecreate(this.mImageAnalysis.getBackpressureStrategy(), this.mImageAnalysis.getImageQueueDepth());
            this.startCameraAndTrackStates();
        }
    }
    
    private void setTargetOutputSize(final ImageOutputConfig.Builder<?> builder, final OutputSize obj) {
        if (obj == null) {
            return;
        }
        if (obj.getResolution() != null) {
            builder.setTargetResolution(obj.getResolution());
        }
        else if (obj.getAspectRatio() != -1) {
            builder.setTargetAspectRatio(obj.getAspectRatio());
        }
        else {
            final StringBuilder sb = new StringBuilder();
            sb.append("Invalid target surface size. ");
            sb.append(obj);
            Logger.e("CameraController", sb.toString());
        }
    }
    
    private float speedUpZoomBy2X(final float n) {
        if (n > 1.0f) {
            return (n - 1.0f) * 2.0f + 1.0f;
        }
        return 1.0f - (1.0f - n) * 2.0f;
    }
    
    private void startListeningToRotationEvents() {
        this.mRotationProvider.addListener(CameraXExecutors.mainThreadExecutor(), this.mDeviceRotationListener);
    }
    
    private void stopListeningToRotationEvents() {
        this.mRotationProvider.removeListener(this.mDeviceRotationListener);
    }
    
    private void unbindImageAnalysisAndRecreate(final int backpressureStrategy, final int imageQueueDepth) {
        Threads.checkMainThread();
        if (this.isCameraInitialized()) {
            this.mCameraProvider.unbind(this.mImageAnalysis);
        }
        final ImageAnalysis.Builder setImageQueueDepth = new ImageAnalysis.Builder().setBackpressureStrategy(backpressureStrategy).setImageQueueDepth(imageQueueDepth);
        this.setTargetOutputSize(setImageQueueDepth, this.mImageAnalysisTargetSize);
        final Executor mAnalysisBackgroundExecutor = this.mAnalysisBackgroundExecutor;
        if (mAnalysisBackgroundExecutor != null) {
            setImageQueueDepth.setBackgroundExecutor(mAnalysisBackgroundExecutor);
        }
        final ImageAnalysis build = setImageQueueDepth.build();
        this.mImageAnalysis = build;
        final Executor mAnalysisExecutor = this.mAnalysisExecutor;
        if (mAnalysisExecutor != null) {
            final ImageAnalysis.Analyzer mAnalysisAnalyzer = this.mAnalysisAnalyzer;
            if (mAnalysisAnalyzer != null) {
                build.setAnalyzer(mAnalysisExecutor, mAnalysisAnalyzer);
            }
        }
    }
    
    private void unbindImageCaptureAndRecreate(final int captureMode) {
        if (this.isCameraInitialized()) {
            this.mCameraProvider.unbind(this.mImageCapture);
        }
        final ImageCapture.Builder setCaptureMode = new ImageCapture.Builder().setCaptureMode(captureMode);
        this.setTargetOutputSize(setCaptureMode, this.mImageCaptureTargetSize);
        final Executor mImageCaptureIoExecutor = this.mImageCaptureIoExecutor;
        if (mImageCaptureIoExecutor != null) {
            setCaptureMode.setIoExecutor(mImageCaptureIoExecutor);
        }
        this.mImageCapture = setCaptureMode.build();
    }
    
    private void unbindPreviewAndRecreate() {
        if (this.isCameraInitialized()) {
            this.mCameraProvider.unbind(this.mPreview);
        }
        final Preview.Builder builder = new Preview.Builder();
        this.setTargetOutputSize(builder, this.mPreviewTargetSize);
        this.mPreview = builder.build();
    }
    
    private void unbindVideoAndRecreate() {
        if (this.isCameraInitialized()) {
            this.mCameraProvider.unbind(this.mVideoCapture);
        }
        final VideoCapture.Builder builder = new VideoCapture.Builder();
        this.setTargetOutputSize(builder, this.mVideoCaptureOutputSize);
        this.mVideoCapture = builder.build();
    }
    
    void attachPreviewSurface(final Preview.SurfaceProvider surfaceProvider, final ViewPort mViewPort, final Display mPreviewDisplay) {
        Threads.checkMainThread();
        if (this.mSurfaceProvider != surfaceProvider) {
            this.mSurfaceProvider = surfaceProvider;
            this.mPreview.setSurfaceProvider(surfaceProvider);
        }
        this.mViewPort = mViewPort;
        this.mPreviewDisplay = mPreviewDisplay;
        this.startListeningToRotationEvents();
        this.startCameraAndTrackStates();
    }
    
    public void clearImageAnalysisAnalyzer() {
        Threads.checkMainThread();
        final ImageAnalysis.Analyzer mAnalysisAnalyzer = this.mAnalysisAnalyzer;
        this.mAnalysisExecutor = null;
        this.mAnalysisAnalyzer = null;
        this.mImageAnalysis.clearAnalyzer();
        this.restartCameraIfAnalyzerResolutionChanged(mAnalysisAnalyzer, null);
    }
    
    void clearPreviewSurface() {
        Threads.checkMainThread();
        final ProcessCameraProvider mCameraProvider = this.mCameraProvider;
        if (mCameraProvider != null) {
            mCameraProvider.unbind(this.mPreview, this.mImageCapture, this.mImageAnalysis, this.mVideoCapture);
        }
        this.mPreview.setSurfaceProvider(null);
        this.mCamera = null;
        this.mSurfaceProvider = null;
        this.mViewPort = null;
        this.mPreviewDisplay = null;
        this.stopListeningToRotationEvents();
    }
    
    protected UseCaseGroup createUseCaseGroup() {
        if (!this.isCameraInitialized()) {
            Logger.d("CameraController", "Camera not initialized.");
            return null;
        }
        if (!this.isPreviewViewAttached()) {
            Logger.d("CameraController", "PreviewView not attached to CameraController.");
            return null;
        }
        final UseCaseGroup.Builder addUseCase = new UseCaseGroup.Builder().addUseCase(this.mPreview);
        if (this.isImageCaptureEnabled()) {
            addUseCase.addUseCase(this.mImageCapture);
        }
        else {
            this.mCameraProvider.unbind(this.mImageCapture);
        }
        if (this.isImageAnalysisEnabled()) {
            addUseCase.addUseCase(this.mImageAnalysis);
        }
        else {
            this.mCameraProvider.unbind(this.mImageAnalysis);
        }
        if (this.isVideoCaptureEnabled()) {
            addUseCase.addUseCase(this.mVideoCapture);
        }
        else {
            this.mCameraProvider.unbind(this.mVideoCapture);
        }
        addUseCase.setViewPort(this.mViewPort);
        final Iterator<CameraEffect> iterator = this.mEffects.iterator();
        while (iterator.hasNext()) {
            addUseCase.addEffect(iterator.next());
        }
        return addUseCase.build();
    }
    
    public ListenableFuture<Void> enableTorch(final boolean b) {
        Threads.checkMainThread();
        if (!this.isCameraAttached()) {
            Logger.w("CameraController", "Use cases not attached to camera.");
            return Futures.immediateFuture((Void)null);
        }
        return this.mCamera.getCameraControl().enableTorch(b);
    }
    
    public CameraControl getCameraControl() {
        Threads.checkMainThread();
        final Camera mCamera = this.mCamera;
        CameraControl cameraControl;
        if (mCamera == null) {
            cameraControl = null;
        }
        else {
            cameraControl = mCamera.getCameraControl();
        }
        return cameraControl;
    }
    
    public CameraInfo getCameraInfo() {
        Threads.checkMainThread();
        final Camera mCamera = this.mCamera;
        CameraInfo cameraInfo;
        if (mCamera == null) {
            cameraInfo = null;
        }
        else {
            cameraInfo = mCamera.getCameraInfo();
        }
        return cameraInfo;
    }
    
    public CameraSelector getCameraSelector() {
        Threads.checkMainThread();
        return this.mCameraSelector;
    }
    
    public Executor getImageAnalysisBackgroundExecutor() {
        Threads.checkMainThread();
        return this.mAnalysisBackgroundExecutor;
    }
    
    public int getImageAnalysisBackpressureStrategy() {
        Threads.checkMainThread();
        return this.mImageAnalysis.getBackpressureStrategy();
    }
    
    public int getImageAnalysisImageQueueDepth() {
        Threads.checkMainThread();
        return this.mImageAnalysis.getImageQueueDepth();
    }
    
    public OutputSize getImageAnalysisTargetSize() {
        Threads.checkMainThread();
        return this.mImageAnalysisTargetSize;
    }
    
    public int getImageCaptureFlashMode() {
        Threads.checkMainThread();
        return this.mImageCapture.getFlashMode();
    }
    
    public Executor getImageCaptureIoExecutor() {
        Threads.checkMainThread();
        return this.mImageCaptureIoExecutor;
    }
    
    public int getImageCaptureMode() {
        Threads.checkMainThread();
        return this.mImageCapture.getCaptureMode();
    }
    
    public OutputSize getImageCaptureTargetSize() {
        Threads.checkMainThread();
        return this.mImageCaptureTargetSize;
    }
    
    public ListenableFuture<Void> getInitializationFuture() {
        return this.mInitializationFuture;
    }
    
    public OutputSize getPreviewTargetSize() {
        Threads.checkMainThread();
        return this.mPreviewTargetSize;
    }
    
    public LiveData<Integer> getTapToFocusState() {
        Threads.checkMainThread();
        return this.mTapToFocusState;
    }
    
    public LiveData<Integer> getTorchState() {
        Threads.checkMainThread();
        return this.mTorchState;
    }
    
    public OutputSize getVideoCaptureTargetSize() {
        Threads.checkMainThread();
        return this.mVideoCaptureOutputSize;
    }
    
    public LiveData<ZoomState> getZoomState() {
        Threads.checkMainThread();
        return this.mZoomState;
    }
    
    public boolean hasCamera(final CameraSelector cameraSelector) {
        Threads.checkMainThread();
        Preconditions.checkNotNull(cameraSelector);
        final ProcessCameraProvider mCameraProvider = this.mCameraProvider;
        if (mCameraProvider != null) {
            try {
                return mCameraProvider.hasCamera(cameraSelector);
            }
            catch (final CameraInfoUnavailableException ex) {
                Logger.w("CameraController", "Failed to check camera availability", ex);
                return false;
            }
        }
        throw new IllegalStateException("Camera not initialized. Please wait for the initialization future to finish. See #getInitializationFuture().");
    }
    
    public boolean isImageAnalysisEnabled() {
        Threads.checkMainThread();
        return this.isUseCaseEnabled(2);
    }
    
    public boolean isImageCaptureEnabled() {
        Threads.checkMainThread();
        return this.isUseCaseEnabled(1);
    }
    
    public boolean isPinchToZoomEnabled() {
        Threads.checkMainThread();
        return this.mPinchToZoomEnabled;
    }
    
    public boolean isRecording() {
        Threads.checkMainThread();
        return this.mVideoIsRecording.get();
    }
    
    public boolean isTapToFocusEnabled() {
        Threads.checkMainThread();
        return this.mTapToFocusEnabled;
    }
    
    public boolean isVideoCaptureEnabled() {
        Threads.checkMainThread();
        return this.isUseCaseEnabled(4);
    }
    
    void onPinchToZoom(final float f) {
        if (!this.isCameraAttached()) {
            Logger.w("CameraController", "Use cases not attached to camera.");
            return;
        }
        if (!this.mPinchToZoomEnabled) {
            Logger.d("CameraController", "Pinch to zoom disabled.");
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Pinch to zoom with scale: ");
        sb.append(f);
        Logger.d("CameraController", sb.toString());
        final ZoomState zoomState = this.getZoomState().getValue();
        if (zoomState == null) {
            return;
        }
        this.setZoomRatio(Math.min(Math.max(zoomState.getZoomRatio() * this.speedUpZoomBy2X(f), zoomState.getMinZoomRatio()), zoomState.getMaxZoomRatio()));
    }
    
    void onTapToFocus(final MeteringPointFactory meteringPointFactory, final float f, final float f2) {
        if (!this.isCameraAttached()) {
            Logger.w("CameraController", "Use cases not attached to camera.");
            return;
        }
        if (!this.mTapToFocusEnabled) {
            Logger.d("CameraController", "Tap to focus disabled. ");
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Tap to focus started: ");
        sb.append(f);
        sb.append(", ");
        sb.append(f2);
        Logger.d("CameraController", sb.toString());
        this.mTapToFocusState.postValue(1);
        Futures.addCallback(this.mCamera.getCameraControl().startFocusAndMetering(new FocusMeteringAction.Builder(meteringPointFactory.createPoint(f, f2, 0.16666667f), 1).addPoint(meteringPointFactory.createPoint(f, f2, 0.25f), 2).build()), new FutureCallback<FocusMeteringResult>(this) {
            final CameraController this$0;
            
            @Override
            public void onFailure(final Throwable t) {
                if (t instanceof CameraControl.OperationCanceledException) {
                    Logger.d("CameraController", "Tap-to-focus is canceled by new action.");
                    return;
                }
                Logger.d("CameraController", "Tap to focus failed.", t);
                this.this$0.mTapToFocusState.postValue(4);
            }
            
            @Override
            public void onSuccess(final FocusMeteringResult focusMeteringResult) {
                if (focusMeteringResult == null) {
                    return;
                }
                final StringBuilder sb = new StringBuilder();
                sb.append("Tap to focus onSuccess: ");
                sb.append(focusMeteringResult.isFocusSuccessful());
                Logger.d("CameraController", sb.toString());
                final MutableLiveData<Integer> mTapToFocusState = this.this$0.mTapToFocusState;
                int i;
                if (focusMeteringResult.isFocusSuccessful()) {
                    i = 2;
                }
                else {
                    i = 3;
                }
                mTapToFocusState.postValue(i);
            }
        }, CameraXExecutors.directExecutor());
    }
    
    public void setCameraSelector(final CameraSelector mCameraSelector) {
        Threads.checkMainThread();
        final CameraSelector mCameraSelector2 = this.mCameraSelector;
        if (mCameraSelector2 == mCameraSelector) {
            return;
        }
        this.mCameraSelector = mCameraSelector;
        final ProcessCameraProvider mCameraProvider = this.mCameraProvider;
        if (mCameraProvider == null) {
            return;
        }
        mCameraProvider.unbind(this.mPreview, this.mImageCapture, this.mImageAnalysis, this.mVideoCapture);
        this.startCameraAndTrackStates(new CameraController$$ExternalSyntheticLambda0(this, mCameraSelector2));
    }
    
    public void setEffects(final List<CameraEffect> list) {
        if (Objects.equals(this.mEffects, list)) {
            return;
        }
        final ProcessCameraProvider mCameraProvider = this.mCameraProvider;
        if (mCameraProvider != null) {
            mCameraProvider.unbindAll();
        }
        this.mEffects = list;
        this.startCameraAndTrackStates();
    }
    
    public void setEnabledUseCases(final int mEnabledUseCases) {
        Threads.checkMainThread();
        final int mEnabledUseCases2 = this.mEnabledUseCases;
        if (mEnabledUseCases == mEnabledUseCases2) {
            return;
        }
        this.mEnabledUseCases = mEnabledUseCases;
        if (!this.isVideoCaptureEnabled()) {
            this.stopRecording();
        }
        this.startCameraAndTrackStates(new CameraController$$ExternalSyntheticLambda3(this, mEnabledUseCases2));
    }
    
    public void setImageAnalysisAnalyzer(final Executor mAnalysisExecutor, final ImageAnalysis.Analyzer mAnalysisAnalyzer) {
        Threads.checkMainThread();
        final ImageAnalysis.Analyzer mAnalysisAnalyzer2 = this.mAnalysisAnalyzer;
        if (mAnalysisAnalyzer2 == mAnalysisAnalyzer && this.mAnalysisExecutor == mAnalysisExecutor) {
            return;
        }
        this.mAnalysisExecutor = mAnalysisExecutor;
        this.mAnalysisAnalyzer = mAnalysisAnalyzer;
        this.mImageAnalysis.setAnalyzer(mAnalysisExecutor, mAnalysisAnalyzer);
        this.restartCameraIfAnalyzerResolutionChanged(mAnalysisAnalyzer2, mAnalysisAnalyzer);
    }
    
    public void setImageAnalysisBackgroundExecutor(final Executor mAnalysisBackgroundExecutor) {
        Threads.checkMainThread();
        if (this.mAnalysisBackgroundExecutor == mAnalysisBackgroundExecutor) {
            return;
        }
        this.mAnalysisBackgroundExecutor = mAnalysisBackgroundExecutor;
        this.unbindImageAnalysisAndRecreate(this.mImageAnalysis.getBackpressureStrategy(), this.mImageAnalysis.getImageQueueDepth());
        this.startCameraAndTrackStates();
    }
    
    public void setImageAnalysisBackpressureStrategy(final int n) {
        Threads.checkMainThread();
        if (this.mImageAnalysis.getBackpressureStrategy() == n) {
            return;
        }
        this.unbindImageAnalysisAndRecreate(n, this.mImageAnalysis.getImageQueueDepth());
        this.startCameraAndTrackStates();
    }
    
    public void setImageAnalysisImageQueueDepth(final int n) {
        Threads.checkMainThread();
        if (this.mImageAnalysis.getImageQueueDepth() == n) {
            return;
        }
        this.unbindImageAnalysisAndRecreate(this.mImageAnalysis.getBackpressureStrategy(), n);
        this.startCameraAndTrackStates();
    }
    
    public void setImageAnalysisTargetSize(final OutputSize mImageAnalysisTargetSize) {
        Threads.checkMainThread();
        if (this.isOutputSizeEqual(this.mImageAnalysisTargetSize, mImageAnalysisTargetSize)) {
            return;
        }
        this.mImageAnalysisTargetSize = mImageAnalysisTargetSize;
        this.unbindImageAnalysisAndRecreate(this.mImageAnalysis.getBackpressureStrategy(), this.mImageAnalysis.getImageQueueDepth());
        this.startCameraAndTrackStates();
    }
    
    public void setImageCaptureFlashMode(final int flashMode) {
        Threads.checkMainThread();
        this.mImageCapture.setFlashMode(flashMode);
    }
    
    public void setImageCaptureIoExecutor(final Executor mImageCaptureIoExecutor) {
        Threads.checkMainThread();
        if (this.mImageCaptureIoExecutor == mImageCaptureIoExecutor) {
            return;
        }
        this.mImageCaptureIoExecutor = mImageCaptureIoExecutor;
        this.unbindImageCaptureAndRecreate(this.mImageCapture.getCaptureMode());
        this.startCameraAndTrackStates();
    }
    
    public void setImageCaptureMode(final int n) {
        Threads.checkMainThread();
        if (this.mImageCapture.getCaptureMode() == n) {
            return;
        }
        this.unbindImageCaptureAndRecreate(n);
        this.startCameraAndTrackStates();
    }
    
    public void setImageCaptureTargetSize(final OutputSize mImageCaptureTargetSize) {
        Threads.checkMainThread();
        if (this.isOutputSizeEqual(this.mImageCaptureTargetSize, mImageCaptureTargetSize)) {
            return;
        }
        this.mImageCaptureTargetSize = mImageCaptureTargetSize;
        this.unbindImageCaptureAndRecreate(this.getImageCaptureMode());
        this.startCameraAndTrackStates();
    }
    
    public ListenableFuture<Void> setLinearZoom(final float linearZoom) {
        Threads.checkMainThread();
        if (!this.isCameraAttached()) {
            Logger.w("CameraController", "Use cases not attached to camera.");
            return Futures.immediateFuture((Void)null);
        }
        return this.mCamera.getCameraControl().setLinearZoom(linearZoom);
    }
    
    public void setPinchToZoomEnabled(final boolean mPinchToZoomEnabled) {
        Threads.checkMainThread();
        this.mPinchToZoomEnabled = mPinchToZoomEnabled;
    }
    
    public void setPreviewTargetSize(final OutputSize mPreviewTargetSize) {
        Threads.checkMainThread();
        if (this.isOutputSizeEqual(this.mPreviewTargetSize, mPreviewTargetSize)) {
            return;
        }
        this.mPreviewTargetSize = mPreviewTargetSize;
        this.unbindPreviewAndRecreate();
        this.startCameraAndTrackStates();
    }
    
    public void setTapToFocusEnabled(final boolean mTapToFocusEnabled) {
        Threads.checkMainThread();
        this.mTapToFocusEnabled = mTapToFocusEnabled;
    }
    
    public void setVideoCaptureTargetSize(final OutputSize mVideoCaptureOutputSize) {
        Threads.checkMainThread();
        if (this.isOutputSizeEqual(this.mVideoCaptureOutputSize, mVideoCaptureOutputSize)) {
            return;
        }
        this.mVideoCaptureOutputSize = mVideoCaptureOutputSize;
        this.unbindVideoAndRecreate();
        this.startCameraAndTrackStates();
    }
    
    public ListenableFuture<Void> setZoomRatio(final float zoomRatio) {
        Threads.checkMainThread();
        if (!this.isCameraAttached()) {
            Logger.w("CameraController", "Use cases not attached to camera.");
            return Futures.immediateFuture((Void)null);
        }
        return this.mCamera.getCameraControl().setZoomRatio(zoomRatio);
    }
    
    abstract Camera startCamera();
    
    void startCameraAndTrackStates() {
        this.startCameraAndTrackStates(null);
    }
    
    void startCameraAndTrackStates(final Runnable runnable) {
        try {
            this.mCamera = this.startCamera();
            if (!this.isCameraAttached()) {
                Logger.d("CameraController", "Use cases not attached to camera.");
                return;
            }
            this.mZoomState.setSource(this.mCamera.getCameraInfo().getZoomState());
            this.mTorchState.setSource(this.mCamera.getCameraInfo().getTorchState());
        }
        catch (final IllegalArgumentException cause) {
            if (runnable != null) {
                runnable.run();
            }
            throw new IllegalStateException("The selected camera does not support the enabled use cases. Please disable use case and/or select a different camera. e.g. #setVideoCaptureEnabled(false)", cause);
        }
    }
    
    public void startRecording(final OutputFileOptions outputFileOptions, final Executor executor, final OnVideoSavedCallback onVideoSavedCallback) {
        Threads.checkMainThread();
        Preconditions.checkState(this.isCameraInitialized(), "Camera not initialized.");
        Preconditions.checkState(this.isVideoCaptureEnabled(), "VideoCapture disabled.");
        this.mVideoCapture.startRecording(outputFileOptions.toVideoCaptureOutputFileOptions(), executor, (VideoCapture.OnVideoSavedCallback)new VideoCapture.OnVideoSavedCallback(this, onVideoSavedCallback) {
            final CameraController this$0;
            final androidx.camera.view.video.OnVideoSavedCallback val$callback;
            
            @Override
            public void onError(final int n, final String s, final Throwable t) {
                this.this$0.mVideoIsRecording.set(false);
                this.val$callback.onError(n, s, t);
            }
            
            @Override
            public void onVideoSaved(final OutputFileResults outputFileResults) {
                this.this$0.mVideoIsRecording.set(false);
                this.val$callback.onVideoSaved(androidx.camera.view.video.OutputFileResults.create(outputFileResults.getSavedUri()));
            }
        });
        this.mVideoIsRecording.set(true);
    }
    
    public void stopRecording() {
        Threads.checkMainThread();
        if (this.mVideoIsRecording.get()) {
            this.mVideoCapture.stopRecording();
        }
    }
    
    public void takePicture(final ImageCapture.OutputFileOptions outputFileOptions, final Executor executor, final ImageCapture.OnImageSavedCallback onImageSavedCallback) {
        Threads.checkMainThread();
        Preconditions.checkState(this.isCameraInitialized(), "Camera not initialized.");
        Preconditions.checkState(this.isImageCaptureEnabled(), "ImageCapture disabled.");
        this.updateMirroringFlagInOutputFileOptions(outputFileOptions);
        this.mImageCapture.takePicture(outputFileOptions, executor, onImageSavedCallback);
    }
    
    public void takePicture(final Executor executor, final ImageCapture.OnImageCapturedCallback onImageCapturedCallback) {
        Threads.checkMainThread();
        Preconditions.checkState(this.isCameraInitialized(), "Camera not initialized.");
        Preconditions.checkState(this.isImageCaptureEnabled(), "ImageCapture disabled.");
        this.mImageCapture.takePicture(executor, onImageCapturedCallback);
    }
    
    void updateMirroringFlagInOutputFileOptions(final ImageCapture.OutputFileOptions outputFileOptions) {
        if (this.mCameraSelector.getLensFacing() != null && !outputFileOptions.getMetadata().isReversedHorizontalSet()) {
            outputFileOptions.getMetadata().setReversedHorizontal(this.mCameraSelector.getLensFacing() == 0);
        }
    }
    
    void updatePreviewViewTransform(final OutputTransform outputTransform) {
        Threads.checkMainThread();
        final ImageAnalysis.Analyzer mAnalysisAnalyzer = this.mAnalysisAnalyzer;
        if (mAnalysisAnalyzer == null) {
            return;
        }
        if (outputTransform == null) {
            mAnalysisAnalyzer.updateTransform(null);
        }
        else if (mAnalysisAnalyzer.getTargetCoordinateSystem() == 1) {
            this.mAnalysisAnalyzer.updateTransform(outputTransform.getMatrix());
        }
    }
    
    private static class Api30Impl
    {
        static Context createAttributionContext(final Context context, final String s) {
            return context.createAttributionContext(s);
        }
        
        static String getAttributionTag(final Context context) {
            return context.getAttributionTag();
        }
    }
    
    public static final class OutputSize
    {
        public static final int UNASSIGNED_ASPECT_RATIO = -1;
        private final int mAspectRatio;
        private final Size mResolution;
        
        public OutputSize(final int mAspectRatio) {
            Preconditions.checkArgument(mAspectRatio != -1);
            this.mAspectRatio = mAspectRatio;
            this.mResolution = null;
        }
        
        public OutputSize(final Size mResolution) {
            Preconditions.checkNotNull(mResolution);
            this.mAspectRatio = -1;
            this.mResolution = mResolution;
        }
        
        public int getAspectRatio() {
            return this.mAspectRatio;
        }
        
        public Size getResolution() {
            return this.mResolution;
        }
        
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder();
            sb.append("aspect ratio: ");
            sb.append(this.mAspectRatio);
            sb.append(" resolution: ");
            sb.append(this.mResolution);
            return sb.toString();
        }
        
        @Retention(RetentionPolicy.SOURCE)
        public @interface OutputAspectRatio {
        }
    }
    
    @Retention(RetentionPolicy.SOURCE)
    public @interface TapToFocusStates {
    }
    
    @Retention(RetentionPolicy.SOURCE)
    public @interface UseCases {
    }
}

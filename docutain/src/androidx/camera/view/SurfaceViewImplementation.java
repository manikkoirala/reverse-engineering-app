// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.view;

import android.view.SurfaceHolder;
import android.view.Surface;
import androidx.core.util.Consumer;
import android.util.Size;
import android.view.PixelCopy;
import android.os.Handler;
import androidx.camera.core.impl.utils.futures.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import java.util.concurrent.Executor;
import androidx.core.content.ContextCompat;
import androidx.camera.core.SurfaceRequest;
import android.view.SurfaceHolder$Callback;
import android.view.ViewGroup$LayoutParams;
import android.widget.FrameLayout$LayoutParams;
import androidx.core.util.Preconditions;
import android.view.PixelCopy$OnPixelCopyFinishedListener;
import android.graphics.Bitmap$Config;
import android.graphics.Bitmap;
import android.view.View;
import androidx.camera.core.Logger;
import android.widget.FrameLayout;
import android.view.SurfaceView;

final class SurfaceViewImplementation extends PreviewViewImplementation
{
    private static final String TAG = "SurfaceViewImpl";
    private OnSurfaceNotInUseListener mOnSurfaceNotInUseListener;
    final SurfaceRequestCallback mSurfaceRequestCallback;
    SurfaceView mSurfaceView;
    
    SurfaceViewImplementation(final FrameLayout frameLayout, final PreviewTransformation previewTransformation) {
        super(frameLayout, previewTransformation);
        this.mSurfaceRequestCallback = new SurfaceRequestCallback();
    }
    
    @Override
    View getPreview() {
        return (View)this.mSurfaceView;
    }
    
    @Override
    Bitmap getPreviewBitmap() {
        final SurfaceView mSurfaceView = this.mSurfaceView;
        if (mSurfaceView != null && mSurfaceView.getHolder().getSurface() != null && this.mSurfaceView.getHolder().getSurface().isValid()) {
            final Bitmap bitmap = Bitmap.createBitmap(this.mSurfaceView.getWidth(), this.mSurfaceView.getHeight(), Bitmap$Config.ARGB_8888);
            Api24Impl.pixelCopyRequest(this.mSurfaceView, bitmap, (PixelCopy$OnPixelCopyFinishedListener)new SurfaceViewImplementation$$ExternalSyntheticLambda2(), this.mSurfaceView.getHandler());
            return bitmap;
        }
        return null;
    }
    
    @Override
    void initializePreview() {
        Preconditions.checkNotNull(this.mParent);
        Preconditions.checkNotNull(this.mResolution);
        (this.mSurfaceView = new SurfaceView(this.mParent.getContext())).setLayoutParams((ViewGroup$LayoutParams)new FrameLayout$LayoutParams(this.mResolution.getWidth(), this.mResolution.getHeight()));
        this.mParent.removeAllViews();
        this.mParent.addView((View)this.mSurfaceView);
        this.mSurfaceView.getHolder().addCallback((SurfaceHolder$Callback)this.mSurfaceRequestCallback);
    }
    
    void notifySurfaceNotInUse() {
        final OnSurfaceNotInUseListener mOnSurfaceNotInUseListener = this.mOnSurfaceNotInUseListener;
        if (mOnSurfaceNotInUseListener != null) {
            mOnSurfaceNotInUseListener.onSurfaceNotInUse();
            this.mOnSurfaceNotInUseListener = null;
        }
    }
    
    @Override
    void onAttachedToWindow() {
    }
    
    @Override
    void onDetachedFromWindow() {
    }
    
    @Override
    void onSurfaceRequested(final SurfaceRequest surfaceRequest, final OnSurfaceNotInUseListener mOnSurfaceNotInUseListener) {
        this.mResolution = surfaceRequest.getResolution();
        this.mOnSurfaceNotInUseListener = mOnSurfaceNotInUseListener;
        this.initializePreview();
        surfaceRequest.addRequestCancellationListener(ContextCompat.getMainExecutor(this.mSurfaceView.getContext()), new SurfaceViewImplementation$$ExternalSyntheticLambda0(this));
        this.mSurfaceView.post((Runnable)new SurfaceViewImplementation$$ExternalSyntheticLambda1(this, surfaceRequest));
    }
    
    @Override
    void setFrameUpdateListener(final Executor executor, final PreviewView.OnFrameUpdateListener onFrameUpdateListener) {
        throw new IllegalArgumentException("SurfaceView doesn't support frame update listener");
    }
    
    @Override
    ListenableFuture<Void> waitForNextFrame() {
        return Futures.immediateFuture((Void)null);
    }
    
    private static class Api24Impl
    {
        static void pixelCopyRequest(final SurfaceView surfaceView, final Bitmap bitmap, final PixelCopy$OnPixelCopyFinishedListener pixelCopy$OnPixelCopyFinishedListener, final Handler handler) {
            PixelCopy.request(surfaceView, bitmap, pixelCopy$OnPixelCopyFinishedListener, handler);
        }
    }
    
    class SurfaceRequestCallback implements SurfaceHolder$Callback
    {
        private Size mCurrentSurfaceSize;
        private SurfaceRequest mSurfaceRequest;
        private Size mTargetSize;
        private boolean mWasSurfaceProvided;
        final SurfaceViewImplementation this$0;
        
        SurfaceRequestCallback(final SurfaceViewImplementation this$0) {
            this.this$0 = this$0;
            this.mWasSurfaceProvided = false;
        }
        
        private boolean canProvideSurface() {
            if (!this.mWasSurfaceProvided && this.mSurfaceRequest != null) {
                final Size mTargetSize = this.mTargetSize;
                if (mTargetSize != null && mTargetSize.equals((Object)this.mCurrentSurfaceSize)) {
                    return true;
                }
            }
            return false;
        }
        
        private void cancelPreviousRequest() {
            if (this.mSurfaceRequest != null) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Request canceled: ");
                sb.append(this.mSurfaceRequest);
                Logger.d("SurfaceViewImpl", sb.toString());
                this.mSurfaceRequest.willNotProvideSurface();
            }
        }
        
        private void invalidateSurface() {
            if (this.mSurfaceRequest != null) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Surface invalidated ");
                sb.append(this.mSurfaceRequest);
                Logger.d("SurfaceViewImpl", sb.toString());
                this.mSurfaceRequest.getDeferrableSurface().close();
            }
        }
        
        private boolean tryToComplete() {
            final Surface surface = this.this$0.mSurfaceView.getHolder().getSurface();
            if (this.canProvideSurface()) {
                Logger.d("SurfaceViewImpl", "Surface set on Preview.");
                this.mSurfaceRequest.provideSurface(surface, ContextCompat.getMainExecutor(this.this$0.mSurfaceView.getContext()), new SurfaceViewImplementation$SurfaceRequestCallback$$ExternalSyntheticLambda0(this));
                this.mWasSurfaceProvided = true;
                this.this$0.onSurfaceProvided();
                return true;
            }
            return false;
        }
        
        void setSurfaceRequest(final SurfaceRequest mSurfaceRequest) {
            this.cancelPreviousRequest();
            this.mSurfaceRequest = mSurfaceRequest;
            final Size resolution = mSurfaceRequest.getResolution();
            this.mTargetSize = resolution;
            this.mWasSurfaceProvided = false;
            if (!this.tryToComplete()) {
                Logger.d("SurfaceViewImpl", "Wait for new Surface creation.");
                this.this$0.mSurfaceView.getHolder().setFixedSize(resolution.getWidth(), resolution.getHeight());
            }
        }
        
        public void surfaceChanged(final SurfaceHolder surfaceHolder, final int n, final int i, final int j) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Surface changed. Size: ");
            sb.append(i);
            sb.append("x");
            sb.append(j);
            Logger.d("SurfaceViewImpl", sb.toString());
            this.mCurrentSurfaceSize = new Size(i, j);
            this.tryToComplete();
        }
        
        public void surfaceCreated(final SurfaceHolder surfaceHolder) {
            Logger.d("SurfaceViewImpl", "Surface created.");
        }
        
        public void surfaceDestroyed(final SurfaceHolder surfaceHolder) {
            Logger.d("SurfaceViewImpl", "Surface destroyed.");
            if (this.mWasSurfaceProvided) {
                this.invalidateSurface();
            }
            else {
                this.cancelPreviousRequest();
            }
            this.mWasSurfaceProvided = false;
            this.mSurfaceRequest = null;
            this.mCurrentSurfaceSize = null;
            this.mTargetSize = null;
        }
    }
}

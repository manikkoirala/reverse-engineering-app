// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.view;

public final class R
{
    public static final class attr
    {
        public static final int implementationMode = 2130969183;
        public static final int scaleType = 2130969970;
    }
    
    public static final class id
    {
        public static final int compatible = 2131362014;
        public static final int fillCenter = 2131362101;
        public static final int fillEnd = 2131362102;
        public static final int fillStart = 2131362103;
        public static final int fitCenter = 2131362114;
        public static final int fitEnd = 2131362115;
        public static final int fitStart = 2131362116;
        public static final int performance = 2131362320;
    }
    
    public static final class styleable
    {
        public static final int[] PreviewView;
        public static final int PreviewView_implementationMode = 0;
        public static final int PreviewView_scaleType = 1;
        
        static {
            PreviewView = new int[] { 2130969183, 2130969970 };
        }
    }
}

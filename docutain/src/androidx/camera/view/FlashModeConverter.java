// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.view;

final class FlashModeConverter
{
    private FlashModeConverter() {
    }
    
    public static String nameOf(final int i) {
        if (i == 0) {
            return "AUTO";
        }
        if (i == 1) {
            return "ON";
        }
        if (i == 2) {
            return "OFF";
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Unknown flash mode ");
        sb.append(i);
        throw new IllegalArgumentException(sb.toString());
    }
    
    public static int valueOf(final String str) {
        if (str == null) {
            throw new NullPointerException("name cannot be null");
        }
        str.hashCode();
        int n = -1;
        switch (str) {
            case "AUTO": {
                n = 2;
                break;
            }
            case "OFF": {
                n = 1;
                break;
            }
            case "ON": {
                n = 0;
                break;
            }
            default:
                break;
        }
        switch (n) {
            default: {
                final StringBuilder sb = new StringBuilder();
                sb.append("Unknown flash mode name ");
                sb.append(str);
                throw new IllegalArgumentException(sb.toString());
            }
            case 2: {
                return 0;
            }
            case 1: {
                return 2;
            }
            case 0: {
                return 1;
            }
        }
    }
}

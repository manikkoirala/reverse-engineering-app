// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.view.internal.compat.quirk;

import java.util.ArrayList;
import androidx.camera.core.impl.Quirk;
import java.util.List;

public class DeviceQuirksLoader
{
    private DeviceQuirksLoader() {
    }
    
    static List<Quirk> loadQuirks() {
        final ArrayList list = new ArrayList();
        if (SurfaceViewStretchedQuirk.load()) {
            list.add(new SurfaceViewStretchedQuirk());
        }
        if (SurfaceViewNotCroppedByParentQuirk.load()) {
            list.add(new SurfaceViewNotCroppedByParentQuirk());
        }
        return list;
    }
}

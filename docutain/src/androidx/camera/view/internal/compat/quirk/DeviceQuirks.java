// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.view.internal.compat.quirk;

import androidx.camera.core.impl.Quirk;
import androidx.camera.core.impl.Quirks;

public class DeviceQuirks
{
    private static final Quirks QUIRKS;
    
    static {
        QUIRKS = new Quirks(DeviceQuirksLoader.loadQuirks());
    }
    
    private DeviceQuirks() {
    }
    
    public static <T extends Quirk> T get(final Class<T> clazz) {
        return DeviceQuirks.QUIRKS.get(clazz);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.view;

import com.google.common.util.concurrent.ListenableFuture;
import java.util.concurrent.Executor;
import androidx.camera.core.SurfaceRequest;
import android.view.View;
import android.graphics.Bitmap;
import android.util.Size;
import android.widget.FrameLayout;

abstract class PreviewViewImplementation
{
    FrameLayout mParent;
    private final PreviewTransformation mPreviewTransform;
    Size mResolution;
    private boolean mWasSurfaceProvided;
    
    PreviewViewImplementation(final FrameLayout mParent, final PreviewTransformation mPreviewTransform) {
        this.mWasSurfaceProvided = false;
        this.mParent = mParent;
        this.mPreviewTransform = mPreviewTransform;
    }
    
    Bitmap getBitmap() {
        final Bitmap previewBitmap = this.getPreviewBitmap();
        if (previewBitmap == null) {
            return null;
        }
        return this.mPreviewTransform.createTransformedBitmap(previewBitmap, new Size(this.mParent.getWidth(), this.mParent.getHeight()), this.mParent.getLayoutDirection());
    }
    
    abstract View getPreview();
    
    abstract Bitmap getPreviewBitmap();
    
    abstract void initializePreview();
    
    abstract void onAttachedToWindow();
    
    abstract void onDetachedFromWindow();
    
    void onSurfaceProvided() {
        this.mWasSurfaceProvided = true;
        this.redrawPreview();
    }
    
    abstract void onSurfaceRequested(final SurfaceRequest p0, final OnSurfaceNotInUseListener p1);
    
    void redrawPreview() {
        final View preview = this.getPreview();
        if (preview != null) {
            if (this.mWasSurfaceProvided) {
                this.mPreviewTransform.transformView(new Size(this.mParent.getWidth(), this.mParent.getHeight()), this.mParent.getLayoutDirection(), preview);
            }
        }
    }
    
    void setFrameUpdateListener(final Executor executor, final PreviewView.OnFrameUpdateListener onFrameUpdateListener) {
    }
    
    abstract ListenableFuture<Void> waitForNextFrame();
    
    interface OnSurfaceNotInUseListener
    {
        void onSurfaceNotInUse();
    }
}

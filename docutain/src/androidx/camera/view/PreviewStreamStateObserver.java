// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.view;

import androidx.camera.core.Logger;
import androidx.camera.core.impl.CameraCaptureResult;
import androidx.concurrent.futures.CallbackToFutureAdapter;
import androidx.camera.core.impl.utils.futures.Futures;
import java.util.Iterator;
import androidx.camera.core.impl.utils.futures.FutureCallback;
import androidx.arch.core.util.Function;
import androidx.camera.core.impl.utils.futures.AsyncFunction;
import androidx.camera.core.impl.utils.executor.CameraXExecutors;
import androidx.camera.core.impl.utils.futures.FutureChain;
import androidx.camera.core.impl.CameraCaptureCallback;
import java.util.List;
import java.util.ArrayList;
import androidx.camera.core.CameraInfo;
import androidx.lifecycle.MutableLiveData;
import com.google.common.util.concurrent.ListenableFuture;
import androidx.camera.core.impl.CameraInfoInternal;
import androidx.camera.core.impl.CameraInternal;
import androidx.camera.core.impl.Observable;

final class PreviewStreamStateObserver implements Observer<CameraInternal.State>
{
    private static final String TAG = "StreamStateObserver";
    private final CameraInfoInternal mCameraInfoInternal;
    ListenableFuture<Void> mFlowFuture;
    private boolean mHasStartedPreviewStreamFlow;
    private PreviewView.StreamState mPreviewStreamState;
    private final MutableLiveData<PreviewView.StreamState> mPreviewStreamStateLiveData;
    private final PreviewViewImplementation mPreviewViewImplementation;
    
    PreviewStreamStateObserver(final CameraInfoInternal mCameraInfoInternal, final MutableLiveData<PreviewView.StreamState> mPreviewStreamStateLiveData, final PreviewViewImplementation mPreviewViewImplementation) {
        this.mHasStartedPreviewStreamFlow = false;
        this.mCameraInfoInternal = mCameraInfoInternal;
        this.mPreviewStreamStateLiveData = mPreviewStreamStateLiveData;
        this.mPreviewViewImplementation = mPreviewViewImplementation;
        synchronized (this) {
            this.mPreviewStreamState = mPreviewStreamStateLiveData.getValue();
        }
    }
    
    private void cancelFlow() {
        final ListenableFuture<Void> mFlowFuture = this.mFlowFuture;
        if (mFlowFuture != null) {
            mFlowFuture.cancel(false);
            this.mFlowFuture = null;
        }
    }
    
    private void startPreviewStreamStateFlow(final CameraInfo cameraInfo) {
        this.updatePreviewStreamState(PreviewView.StreamState.IDLE);
        final ArrayList list = new ArrayList();
        Futures.addCallback(this.mFlowFuture = (ListenableFuture<Void>)FutureChain.from(this.waitForCaptureResult(cameraInfo, list)).transformAsync((AsyncFunction<? super Void, Object>)new PreviewStreamStateObserver$$ExternalSyntheticLambda0(this), CameraXExecutors.directExecutor()).transform((Function<? super Object, Object>)new PreviewStreamStateObserver$$ExternalSyntheticLambda1(this), CameraXExecutors.directExecutor()), new FutureCallback<Void>(this, list, cameraInfo) {
            final PreviewStreamStateObserver this$0;
            final List val$callbacksToClear;
            final CameraInfo val$cameraInfo;
            
            @Override
            public void onFailure(final Throwable t) {
                this.this$0.mFlowFuture = null;
                if (!this.val$callbacksToClear.isEmpty()) {
                    final Iterator iterator = this.val$callbacksToClear.iterator();
                    while (iterator.hasNext()) {
                        ((CameraInfoInternal)this.val$cameraInfo).removeSessionCaptureCallback((CameraCaptureCallback)iterator.next());
                    }
                    this.val$callbacksToClear.clear();
                }
            }
            
            @Override
            public void onSuccess(final Void void1) {
                this.this$0.mFlowFuture = null;
            }
        }, CameraXExecutors.directExecutor());
    }
    
    private ListenableFuture<Void> waitForCaptureResult(final CameraInfo cameraInfo, final List<CameraCaptureCallback> list) {
        return CallbackToFutureAdapter.getFuture((CallbackToFutureAdapter.Resolver<Void>)new PreviewStreamStateObserver$$ExternalSyntheticLambda2(this, cameraInfo, list));
    }
    
    void clear() {
        this.cancelFlow();
    }
    
    @Override
    public void onError(final Throwable t) {
        this.clear();
        this.updatePreviewStreamState(PreviewView.StreamState.IDLE);
    }
    
    public void onNewData(final CameraInternal.State state) {
        if (state != CameraInternal.State.CLOSING && state != CameraInternal.State.CLOSED && state != CameraInternal.State.RELEASING && state != CameraInternal.State.RELEASED) {
            if ((state == CameraInternal.State.OPENING || state == CameraInternal.State.OPEN || state == CameraInternal.State.PENDING_OPEN) && !this.mHasStartedPreviewStreamFlow) {
                this.startPreviewStreamStateFlow(this.mCameraInfoInternal);
                this.mHasStartedPreviewStreamFlow = true;
            }
        }
        else {
            this.updatePreviewStreamState(PreviewView.StreamState.IDLE);
            if (this.mHasStartedPreviewStreamFlow) {
                this.mHasStartedPreviewStreamFlow = false;
                this.cancelFlow();
            }
        }
    }
    
    void updatePreviewStreamState(final PreviewView.StreamState obj) {
        synchronized (this) {
            if (this.mPreviewStreamState.equals(obj)) {
                return;
            }
            this.mPreviewStreamState = obj;
            monitorexit(this);
            final StringBuilder sb = new StringBuilder();
            sb.append("Update Preview stream state to ");
            sb.append(obj);
            Logger.d("StreamStateObserver", sb.toString());
            this.mPreviewStreamStateLiveData.postValue(obj);
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.view;

import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;

@Retention(RetentionPolicy.CLASS)
public @interface TransformExperimental {
}

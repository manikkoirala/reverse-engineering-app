// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.view;

import androidx.lifecycle.Observer;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;

final class ForwardingLiveData<T> extends MediatorLiveData<T>
{
    private LiveData<T> mLiveDataSource;
    
    @Override
    public T getValue() {
        final LiveData<T> mLiveDataSource = this.mLiveDataSource;
        T value;
        if (mLiveDataSource == null) {
            value = null;
        }
        else {
            value = mLiveDataSource.getValue();
        }
        return value;
    }
    
    void setSource(final LiveData<T> mLiveDataSource) {
        final LiveData<T> mLiveDataSource2 = this.mLiveDataSource;
        if (mLiveDataSource2 != null) {
            super.removeSource(mLiveDataSource2);
        }
        super.addSource(this.mLiveDataSource = mLiveDataSource, new ForwardingLiveData$$ExternalSyntheticLambda0(this));
    }
}

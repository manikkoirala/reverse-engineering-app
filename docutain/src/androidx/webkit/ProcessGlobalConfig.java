// 
// Decompiled by Procyon v0.6.0
// 

package androidx.webkit;

import java.io.File;
import android.content.Context;
import java.lang.reflect.Field;
import androidx.camera.view.PreviewView$1$$ExternalSyntheticBackportWithForwarding0;
import androidx.webkit.internal.ApiHelperForP;
import androidx.webkit.internal.WebViewFeatureInternal;
import java.util.HashMap;
import java.util.concurrent.atomic.AtomicReference;

public class ProcessGlobalConfig
{
    private static boolean sApplyCalled;
    private static final Object sLock;
    private static final AtomicReference<HashMap<String, Object>> sProcessGlobalConfig;
    String mCacheDirectoryBasePath;
    String mDataDirectoryBasePath;
    String mDataDirectorySuffix;
    
    static {
        sProcessGlobalConfig = new AtomicReference<HashMap<String, Object>>();
        sLock = new Object();
        ProcessGlobalConfig.sApplyCalled = false;
    }
    
    public static void apply(final ProcessGlobalConfig processGlobalConfig) {
        Object sLock = ProcessGlobalConfig.sLock;
        synchronized (sLock) {
            if (ProcessGlobalConfig.sApplyCalled) {
                throw new IllegalStateException("ProcessGlobalConfig#apply was called more than once, which is an illegal operation. The configuration settings provided by ProcessGlobalConfig take effect only once, when WebView is first loaded into the current process. Every process should only ever create a single instance of ProcessGlobalConfig and apply it once, before any calls to android.webkit APIs, such as during early app startup.");
            }
            ProcessGlobalConfig.sApplyCalled = true;
            monitorexit(sLock);
            sLock = new HashMap<String, String>();
            if (webViewCurrentlyLoaded()) {
                throw new IllegalStateException("WebView has already been loaded in the current process, so any attempt to apply the settings in ProcessGlobalConfig will have no effect. ProcessGlobalConfig#apply needs to be called before any calls to android.webkit APIs, such as during early app startup.");
            }
            if (processGlobalConfig.mDataDirectorySuffix != null) {
                if (WebViewFeatureInternal.STARTUP_FEATURE_SET_DATA_DIRECTORY_SUFFIX.isSupportedByFramework()) {
                    ApiHelperForP.setDataDirectorySuffix(processGlobalConfig.mDataDirectorySuffix);
                }
                else {
                    ((HashMap<String, String>)sLock).put("DATA_DIRECTORY_SUFFIX", processGlobalConfig.mDataDirectorySuffix);
                }
            }
            final String mDataDirectoryBasePath = processGlobalConfig.mDataDirectoryBasePath;
            if (mDataDirectoryBasePath != null) {
                ((HashMap<String, String>)sLock).put("DATA_DIRECTORY_BASE_PATH", mDataDirectoryBasePath);
            }
            final String mCacheDirectoryBasePath = processGlobalConfig.mCacheDirectoryBasePath;
            if (mCacheDirectoryBasePath != null) {
                ((HashMap<String, String>)sLock).put("CACHE_DIRECTORY_BASE_PATH", mCacheDirectoryBasePath);
            }
            if (PreviewView$1$$ExternalSyntheticBackportWithForwarding0.m(ProcessGlobalConfig.sProcessGlobalConfig, null, sLock)) {
                return;
            }
            throw new RuntimeException("Attempting to set ProcessGlobalConfig#sProcessGlobalConfig when it was already set");
        }
    }
    
    private static boolean webViewCurrentlyLoaded() {
        boolean b = false;
        try {
            final Field declaredField = Class.forName("android.webkit.WebViewFactory").getDeclaredField("sProviderInstance");
            declaredField.setAccessible(true);
            if (declaredField.get(null) != null) {
                b = true;
            }
            return b;
        }
        catch (final Exception ex) {
            return b;
        }
    }
    
    public ProcessGlobalConfig setDataDirectorySuffix(final Context context, final String s) {
        if (!WebViewFeatureInternal.STARTUP_FEATURE_SET_DATA_DIRECTORY_SUFFIX.isSupported(context)) {
            throw WebViewFeatureInternal.getUnsupportedOperationException();
        }
        if (s.equals("")) {
            throw new IllegalArgumentException("Suffix cannot be an empty string");
        }
        if (s.indexOf(File.separatorChar) < 0) {
            this.mDataDirectorySuffix = s;
            return this;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Suffix ");
        sb.append(s);
        sb.append(" contains a path separator");
        throw new IllegalArgumentException(sb.toString());
    }
    
    public ProcessGlobalConfig setDirectoryBasePaths(final Context context, final File file, final File file2) {
        if (!WebViewFeatureInternal.STARTUP_FEATURE_SET_DIRECTORY_BASE_PATH.isSupported(context)) {
            throw WebViewFeatureInternal.getUnsupportedOperationException();
        }
        if (!file.isAbsolute()) {
            throw new IllegalArgumentException("dataDirectoryBasePath must be a non-empty absolute path");
        }
        if (file2.isAbsolute()) {
            this.mDataDirectoryBasePath = file.getAbsolutePath();
            this.mCacheDirectoryBasePath = file2.getAbsolutePath();
            return this;
        }
        throw new IllegalArgumentException("cacheDirectoryBasePath must be a non-empty absolute path");
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.webkit;

import android.os.Handler;
import java.lang.reflect.InvocationHandler;
import android.webkit.WebMessagePort;

public abstract class WebMessagePortCompat
{
    public abstract void close();
    
    public abstract WebMessagePort getFrameworkPort();
    
    public abstract InvocationHandler getInvocationHandler();
    
    public abstract void postMessage(final WebMessageCompat p0);
    
    public abstract void setWebMessageCallback(final Handler p0, final WebMessageCallbackCompat p1);
    
    public abstract void setWebMessageCallback(final WebMessageCallbackCompat p0);
    
    public abstract static class WebMessageCallbackCompat
    {
        public void onMessage(final WebMessagePortCompat webMessagePortCompat, final WebMessageCompat webMessageCompat) {
        }
    }
}

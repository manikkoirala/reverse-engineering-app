// 
// Decompiled by Procyon v0.6.0
// 

package androidx.webkit;

import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;

public abstract class WebResourceErrorCompat
{
    public abstract CharSequence getDescription();
    
    public abstract int getErrorCode();
    
    @Retention(RetentionPolicy.SOURCE)
    public @interface NetErrorCode {
    }
}

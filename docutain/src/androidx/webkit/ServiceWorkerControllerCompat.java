// 
// Decompiled by Procyon v0.6.0
// 

package androidx.webkit;

import androidx.webkit.internal.ServiceWorkerControllerImpl;

public abstract class ServiceWorkerControllerCompat
{
    public static ServiceWorkerControllerCompat getInstance() {
        return LAZY_HOLDER.INSTANCE;
    }
    
    public abstract ServiceWorkerWebSettingsCompat getServiceWorkerWebSettings();
    
    public abstract void setServiceWorkerClient(final ServiceWorkerClientCompat p0);
    
    private static class LAZY_HOLDER
    {
        static final ServiceWorkerControllerCompat INSTANCE;
        
        static {
            INSTANCE = new ServiceWorkerControllerImpl();
        }
    }
}

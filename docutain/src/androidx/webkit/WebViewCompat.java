// 
// Decompiled by Procyon v0.6.0
// 

package androidx.webkit;

import java.util.concurrent.Executor;
import java.util.HashSet;
import java.util.List;
import java.util.Collection;
import java.util.ArrayList;
import android.webkit.ValueCallback;
import androidx.webkit.internal.WebMessageAdapter;
import androidx.webkit.internal.WebViewRenderProcessClientFrameworkAdapter;
import androidx.webkit.internal.WebViewRenderProcessImpl;
import androidx.webkit.internal.ApiHelperForQ;
import android.webkit.WebViewClient;
import android.webkit.WebChromeClient;
import androidx.webkit.internal.ApiHelperForOMR1;
import androidx.webkit.internal.WebViewProviderAdapter;
import android.content.pm.PackageManager$NameNotFoundException;
import androidx.webkit.internal.WebViewGlueCommunicator;
import androidx.webkit.internal.WebViewProviderFactory;
import android.content.Context;
import androidx.webkit.internal.ApiHelperForO;
import android.content.pm.PackageInfo;
import androidx.webkit.internal.ApiFeature;
import androidx.webkit.internal.WebMessagePortImpl;
import androidx.webkit.internal.ApiHelperForM;
import org.chromium.support_lib_boundary.WebViewProviderBoundaryInterface;
import java.lang.reflect.Method;
import java.lang.reflect.InvocationTargetException;
import android.os.Looper;
import androidx.webkit.internal.ApiHelperForP;
import android.os.Build$VERSION;
import androidx.webkit.internal.WebViewFeatureInternal;
import java.util.Set;
import android.webkit.WebView;
import android.net.Uri;

public class WebViewCompat
{
    private static final Uri EMPTY_URI;
    private static final Uri WILDCARD_URI;
    
    static {
        WILDCARD_URI = Uri.parse("*");
        EMPTY_URI = Uri.parse("");
    }
    
    private WebViewCompat() {
    }
    
    public static ScriptHandler addDocumentStartJavaScript(final WebView webView, final String s, final Set<String> set) {
        if (WebViewFeatureInternal.DOCUMENT_START_SCRIPT.isSupportedByWebView()) {
            return getProvider(webView).addDocumentStartJavaScript(s, set.toArray(new String[0]));
        }
        throw WebViewFeatureInternal.getUnsupportedOperationException();
    }
    
    public static void addWebMessageListener(final WebView webView, final String s, final Set<String> set, final WebMessageListener webMessageListener) {
        if (WebViewFeatureInternal.WEB_MESSAGE_LISTENER.isSupportedByWebView()) {
            getProvider(webView).addWebMessageListener(s, set.toArray(new String[0]), webMessageListener);
            return;
        }
        throw WebViewFeatureInternal.getUnsupportedOperationException();
    }
    
    private static void checkThread(final WebView obj) {
        if (Build$VERSION.SDK_INT >= 28) {
            final Looper webViewLooper = ApiHelperForP.getWebViewLooper(obj);
            if (webViewLooper == Looper.myLooper()) {
                return;
            }
            final StringBuilder sb = new StringBuilder();
            sb.append("A WebView method was called on thread '");
            sb.append(Thread.currentThread().getName());
            sb.append("'. All WebView methods must be called on the same thread. (Expected Looper ");
            sb.append(webViewLooper);
            sb.append(" called on ");
            sb.append(Looper.myLooper());
            sb.append(", FYI main Looper is ");
            sb.append(Looper.getMainLooper());
            sb.append(")");
            throw new RuntimeException(sb.toString());
        }
        try {
            final Method declaredMethod = WebView.class.getDeclaredMethod("checkThread", (Class<?>[])new Class[0]);
            declaredMethod.setAccessible(true);
            declaredMethod.invoke(obj, new Object[0]);
        }
        catch (final InvocationTargetException cause) {
            throw new RuntimeException(cause);
        }
        catch (final IllegalAccessException cause2) {
            throw new RuntimeException(cause2);
        }
        catch (final NoSuchMethodException cause3) {
            throw new RuntimeException(cause3);
        }
    }
    
    private static WebViewProviderBoundaryInterface createProvider(final WebView webView) {
        return getFactory().createWebView(webView);
    }
    
    public static WebMessagePortCompat[] createWebMessageChannel(final WebView webView) {
        final ApiFeature.M create_WEB_MESSAGE_CHANNEL = WebViewFeatureInternal.CREATE_WEB_MESSAGE_CHANNEL;
        if (create_WEB_MESSAGE_CHANNEL.isSupportedByFramework()) {
            return WebMessagePortImpl.portsToCompat(ApiHelperForM.createWebMessageChannel(webView));
        }
        if (create_WEB_MESSAGE_CHANNEL.isSupportedByWebView()) {
            return getProvider(webView).createWebMessageChannel();
        }
        throw WebViewFeatureInternal.getUnsupportedOperationException();
    }
    
    public static PackageInfo getCurrentLoadedWebViewPackage() {
        if (Build$VERSION.SDK_INT < 21) {
            return null;
        }
        if (Build$VERSION.SDK_INT >= 26) {
            return ApiHelperForO.getCurrentWebViewPackage();
        }
        try {
            return getLoadedWebViewPackageInfo();
        }
        catch (final ClassNotFoundException | IllegalAccessException | InvocationTargetException | NoSuchMethodException ex) {
            return null;
        }
    }
    
    public static PackageInfo getCurrentWebViewPackage(final Context context) {
        if (Build$VERSION.SDK_INT < 21) {
            return null;
        }
        final PackageInfo currentLoadedWebViewPackage = getCurrentLoadedWebViewPackage();
        if (currentLoadedWebViewPackage != null) {
            return currentLoadedWebViewPackage;
        }
        return getNotYetLoadedWebViewPackageInfo(context);
    }
    
    private static WebViewProviderFactory getFactory() {
        return WebViewGlueCommunicator.getFactory();
    }
    
    private static PackageInfo getLoadedWebViewPackageInfo() throws ClassNotFoundException, NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        return (PackageInfo)Class.forName("android.webkit.WebViewFactory").getMethod("getLoadedPackageInfo", (Class<?>[])new Class[0]).invoke(null, new Object[0]);
    }
    
    private static PackageInfo getNotYetLoadedWebViewPackageInfo(final Context context) {
        try {
            String s;
            if (Build$VERSION.SDK_INT >= 21 && Build$VERSION.SDK_INT <= 23) {
                s = (String)Class.forName("android.webkit.WebViewFactory").getMethod("getWebViewPackageName", (Class<?>[])new Class[0]).invoke(null, new Object[0]);
            }
            else {
                s = (String)Class.forName("android.webkit.WebViewUpdateService").getMethod("getCurrentWebViewPackageName", (Class<?>[])new Class[0]).invoke(null, new Object[0]);
            }
            if (s == null) {
                return null;
            }
            return context.getPackageManager().getPackageInfo(s, 0);
        }
        catch (final ClassNotFoundException | IllegalAccessException | InvocationTargetException | NoSuchMethodException | PackageManager$NameNotFoundException ex) {
            return null;
        }
    }
    
    private static WebViewProviderAdapter getProvider(final WebView webView) {
        return new WebViewProviderAdapter(createProvider(webView));
    }
    
    public static Uri getSafeBrowsingPrivacyPolicyUrl() {
        final ApiFeature.O_MR1 safe_BROWSING_PRIVACY_POLICY_URL = WebViewFeatureInternal.SAFE_BROWSING_PRIVACY_POLICY_URL;
        if (safe_BROWSING_PRIVACY_POLICY_URL.isSupportedByFramework()) {
            return ApiHelperForOMR1.getSafeBrowsingPrivacyPolicyUrl();
        }
        if (safe_BROWSING_PRIVACY_POLICY_URL.isSupportedByWebView()) {
            return getFactory().getStatics().getSafeBrowsingPrivacyPolicyUrl();
        }
        throw WebViewFeatureInternal.getUnsupportedOperationException();
    }
    
    public static String getVariationsHeader() {
        if (WebViewFeatureInternal.GET_VARIATIONS_HEADER.isSupportedByWebView()) {
            return getFactory().getStatics().getVariationsHeader();
        }
        throw WebViewFeatureInternal.getUnsupportedOperationException();
    }
    
    public static WebChromeClient getWebChromeClient(final WebView webView) {
        final ApiFeature.O get_WEB_CHROME_CLIENT = WebViewFeatureInternal.GET_WEB_CHROME_CLIENT;
        if (get_WEB_CHROME_CLIENT.isSupportedByFramework()) {
            return ApiHelperForO.getWebChromeClient(webView);
        }
        if (get_WEB_CHROME_CLIENT.isSupportedByWebView()) {
            return getProvider(webView).getWebChromeClient();
        }
        throw WebViewFeatureInternal.getUnsupportedOperationException();
    }
    
    public static WebViewClient getWebViewClient(final WebView webView) {
        final ApiFeature.O get_WEB_VIEW_CLIENT = WebViewFeatureInternal.GET_WEB_VIEW_CLIENT;
        if (get_WEB_VIEW_CLIENT.isSupportedByFramework()) {
            return ApiHelperForO.getWebViewClient(webView);
        }
        if (get_WEB_VIEW_CLIENT.isSupportedByWebView()) {
            return getProvider(webView).getWebViewClient();
        }
        throw WebViewFeatureInternal.getUnsupportedOperationException();
    }
    
    public static WebViewRenderProcess getWebViewRenderProcess(final WebView webView) {
        final ApiFeature.Q get_WEB_VIEW_RENDERER = WebViewFeatureInternal.GET_WEB_VIEW_RENDERER;
        if (get_WEB_VIEW_RENDERER.isSupportedByFramework()) {
            final android.webkit.WebViewRenderProcess webViewRenderProcess = ApiHelperForQ.getWebViewRenderProcess(webView);
            WebViewRenderProcessImpl forFrameworkObject;
            if (webViewRenderProcess != null) {
                forFrameworkObject = WebViewRenderProcessImpl.forFrameworkObject(webViewRenderProcess);
            }
            else {
                forFrameworkObject = null;
            }
            return forFrameworkObject;
        }
        if (get_WEB_VIEW_RENDERER.isSupportedByWebView()) {
            return getProvider(webView).getWebViewRenderProcess();
        }
        throw WebViewFeatureInternal.getUnsupportedOperationException();
    }
    
    public static WebViewRenderProcessClient getWebViewRenderProcessClient(final WebView webView) {
        final ApiFeature.Q web_VIEW_RENDERER_CLIENT_BASIC_USAGE = WebViewFeatureInternal.WEB_VIEW_RENDERER_CLIENT_BASIC_USAGE;
        if (web_VIEW_RENDERER_CLIENT_BASIC_USAGE.isSupportedByFramework()) {
            final android.webkit.WebViewRenderProcessClient webViewRenderProcessClient = ApiHelperForQ.getWebViewRenderProcessClient(webView);
            if (webViewRenderProcessClient != null && webViewRenderProcessClient instanceof WebViewRenderProcessClientFrameworkAdapter) {
                return ((WebViewRenderProcessClientFrameworkAdapter)webViewRenderProcessClient).getFrameworkRenderProcessClient();
            }
            return null;
        }
        else {
            if (web_VIEW_RENDERER_CLIENT_BASIC_USAGE.isSupportedByWebView()) {
                return getProvider(webView).getWebViewRenderProcessClient();
            }
            throw WebViewFeatureInternal.getUnsupportedOperationException();
        }
    }
    
    public static boolean isMultiProcessEnabled() {
        if (WebViewFeatureInternal.MULTI_PROCESS.isSupportedByWebView()) {
            return getFactory().getStatics().isMultiProcessEnabled();
        }
        throw WebViewFeatureInternal.getUnsupportedOperationException();
    }
    
    public static void postVisualStateCallback(final WebView webView, final long n, final VisualStateCallback visualStateCallback) {
        final ApiFeature.M visual_STATE_CALLBACK = WebViewFeatureInternal.VISUAL_STATE_CALLBACK;
        if (visual_STATE_CALLBACK.isSupportedByFramework()) {
            ApiHelperForM.postVisualStateCallback(webView, n, visualStateCallback);
        }
        else {
            if (!visual_STATE_CALLBACK.isSupportedByWebView()) {
                throw WebViewFeatureInternal.getUnsupportedOperationException();
            }
            checkThread(webView);
            getProvider(webView).insertVisualStateCallback(n, visualStateCallback);
        }
    }
    
    public static void postWebMessage(final WebView webView, final WebMessageCompat webMessageCompat, final Uri uri) {
        Uri empty_URI = uri;
        if (WebViewCompat.WILDCARD_URI.equals((Object)uri)) {
            empty_URI = WebViewCompat.EMPTY_URI;
        }
        final ApiFeature.M post_WEB_MESSAGE = WebViewFeatureInternal.POST_WEB_MESSAGE;
        if (post_WEB_MESSAGE.isSupportedByFramework() && webMessageCompat.getType() == 0) {
            ApiHelperForM.postWebMessage(webView, WebMessagePortImpl.compatToFrameworkMessage(webMessageCompat), empty_URI);
        }
        else {
            if (!post_WEB_MESSAGE.isSupportedByWebView() || !WebMessageAdapter.isMessagePayloadTypeSupportedByWebView(webMessageCompat.getType())) {
                throw WebViewFeatureInternal.getUnsupportedOperationException();
            }
            getProvider(webView).postWebMessage(webMessageCompat, empty_URI);
        }
    }
    
    public static void removeWebMessageListener(final WebView webView, final String s) {
        if (WebViewFeatureInternal.WEB_MESSAGE_LISTENER.isSupportedByWebView()) {
            getProvider(webView).removeWebMessageListener(s);
            return;
        }
        throw WebViewFeatureInternal.getUnsupportedOperationException();
    }
    
    public static void setSafeBrowsingAllowlist(final Set<String> c, final ValueCallback<Boolean> valueCallback) {
        final ApiFeature.O_MR1 safe_BROWSING_ALLOWLIST_PREFERRED_TO_PREFERRED = WebViewFeatureInternal.SAFE_BROWSING_ALLOWLIST_PREFERRED_TO_PREFERRED;
        final ApiFeature.O_MR1 safe_BROWSING_ALLOWLIST_PREFERRED_TO_DEPRECATED = WebViewFeatureInternal.SAFE_BROWSING_ALLOWLIST_PREFERRED_TO_DEPRECATED;
        if (safe_BROWSING_ALLOWLIST_PREFERRED_TO_PREFERRED.isSupportedByWebView()) {
            getFactory().getStatics().setSafeBrowsingAllowlist((Set)c, (ValueCallback)valueCallback);
            return;
        }
        final ArrayList list = new ArrayList((Collection<? extends E>)c);
        if (safe_BROWSING_ALLOWLIST_PREFERRED_TO_DEPRECATED.isSupportedByFramework()) {
            ApiHelperForOMR1.setSafeBrowsingWhitelist(list, valueCallback);
        }
        else {
            if (!safe_BROWSING_ALLOWLIST_PREFERRED_TO_DEPRECATED.isSupportedByWebView()) {
                throw WebViewFeatureInternal.getUnsupportedOperationException();
            }
            getFactory().getStatics().setSafeBrowsingWhitelist((List)list, (ValueCallback)valueCallback);
        }
    }
    
    @Deprecated
    public static void setSafeBrowsingWhitelist(final List<String> c, final ValueCallback<Boolean> valueCallback) {
        setSafeBrowsingAllowlist(new HashSet<String>(c), valueCallback);
    }
    
    public static void setWebViewRenderProcessClient(final WebView webView, final WebViewRenderProcessClient webViewRenderProcessClient) {
        final ApiFeature.Q web_VIEW_RENDERER_CLIENT_BASIC_USAGE = WebViewFeatureInternal.WEB_VIEW_RENDERER_CLIENT_BASIC_USAGE;
        if (web_VIEW_RENDERER_CLIENT_BASIC_USAGE.isSupportedByFramework()) {
            ApiHelperForQ.setWebViewRenderProcessClient(webView, webViewRenderProcessClient);
        }
        else {
            if (!web_VIEW_RENDERER_CLIENT_BASIC_USAGE.isSupportedByWebView()) {
                throw WebViewFeatureInternal.getUnsupportedOperationException();
            }
            getProvider(webView).setWebViewRenderProcessClient(null, webViewRenderProcessClient);
        }
    }
    
    public static void setWebViewRenderProcessClient(final WebView webView, final Executor executor, final WebViewRenderProcessClient webViewRenderProcessClient) {
        final ApiFeature.Q web_VIEW_RENDERER_CLIENT_BASIC_USAGE = WebViewFeatureInternal.WEB_VIEW_RENDERER_CLIENT_BASIC_USAGE;
        if (web_VIEW_RENDERER_CLIENT_BASIC_USAGE.isSupportedByFramework()) {
            ApiHelperForQ.setWebViewRenderProcessClient(webView, executor, webViewRenderProcessClient);
        }
        else {
            if (!web_VIEW_RENDERER_CLIENT_BASIC_USAGE.isSupportedByWebView()) {
                throw WebViewFeatureInternal.getUnsupportedOperationException();
            }
            getProvider(webView).setWebViewRenderProcessClient(executor, webViewRenderProcessClient);
        }
    }
    
    public static void startSafeBrowsing(final Context context, final ValueCallback<Boolean> valueCallback) {
        final ApiFeature.O_MR1 start_SAFE_BROWSING = WebViewFeatureInternal.START_SAFE_BROWSING;
        if (start_SAFE_BROWSING.isSupportedByFramework()) {
            ApiHelperForOMR1.startSafeBrowsing(context, valueCallback);
        }
        else {
            if (!start_SAFE_BROWSING.isSupportedByWebView()) {
                throw WebViewFeatureInternal.getUnsupportedOperationException();
            }
            getFactory().getStatics().initSafeBrowsing(context, (ValueCallback)valueCallback);
        }
    }
    
    public interface VisualStateCallback
    {
        void onComplete(final long p0);
    }
    
    public interface WebMessageListener
    {
        void onPostMessage(final WebView p0, final WebMessageCompat p1, final Uri p2, final boolean p3, final JavaScriptReplyProxy p4);
    }
}

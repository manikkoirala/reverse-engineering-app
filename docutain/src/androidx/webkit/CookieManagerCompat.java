// 
// Decompiled by Procyon v0.6.0
// 

package androidx.webkit;

import androidx.webkit.internal.WebViewFeatureInternal;
import java.util.List;
import androidx.webkit.internal.WebViewGlueCommunicator;
import androidx.webkit.internal.CookieManagerAdapter;
import android.webkit.CookieManager;

public class CookieManagerCompat
{
    private CookieManagerCompat() {
    }
    
    private static CookieManagerAdapter getAdapter(final CookieManager cookieManager) {
        return WebViewGlueCommunicator.getCompatConverter().convertCookieManager(cookieManager);
    }
    
    public static List<String> getCookieInfo(final CookieManager cookieManager, final String s) {
        if (WebViewFeatureInternal.GET_COOKIE_INFO.isSupportedByWebView()) {
            return getAdapter(cookieManager).getCookieInfo(s);
        }
        throw WebViewFeatureInternal.getUnsupportedOperationException();
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.webkit.internal;

import android.webkit.WebResourceResponse;
import android.webkit.WebResourceRequest;
import androidx.webkit.ServiceWorkerClientCompat;
import android.webkit.ServiceWorkerClient;

public class FrameworkServiceWorkerClient extends ServiceWorkerClient
{
    private final ServiceWorkerClientCompat mImpl;
    
    public FrameworkServiceWorkerClient(final ServiceWorkerClientCompat mImpl) {
        this.mImpl = mImpl;
    }
    
    public WebResourceResponse shouldInterceptRequest(final WebResourceRequest webResourceRequest) {
        return this.mImpl.shouldInterceptRequest(webResourceRequest);
    }
}

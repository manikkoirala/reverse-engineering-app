// 
// Decompiled by Procyon v0.6.0
// 

package androidx.webkit.internal;

import androidx.webkit.WebViewRenderProcess;
import java.lang.reflect.InvocationHandler;
import android.webkit.WebView;
import androidx.webkit.WebViewRenderProcessClient;
import java.util.concurrent.Executor;
import org.chromium.support_lib_boundary.WebViewRendererClientBoundaryInterface;

public class WebViewRenderProcessClientAdapter implements WebViewRendererClientBoundaryInterface
{
    private static final String[] sSupportedFeatures;
    private final Executor mExecutor;
    private final WebViewRenderProcessClient mWebViewRenderProcessClient;
    
    static {
        sSupportedFeatures = new String[] { "WEB_VIEW_RENDERER_CLIENT_BASIC_USAGE" };
    }
    
    public WebViewRenderProcessClientAdapter(final Executor mExecutor, final WebViewRenderProcessClient mWebViewRenderProcessClient) {
        this.mExecutor = mExecutor;
        this.mWebViewRenderProcessClient = mWebViewRenderProcessClient;
    }
    
    public final String[] getSupportedFeatures() {
        return WebViewRenderProcessClientAdapter.sSupportedFeatures;
    }
    
    public WebViewRenderProcessClient getWebViewRenderProcessClient() {
        return this.mWebViewRenderProcessClient;
    }
    
    public final void onRendererResponsive(final WebView webView, final InvocationHandler invocationHandler) {
        final WebViewRenderProcessImpl forInvocationHandler = WebViewRenderProcessImpl.forInvocationHandler(invocationHandler);
        final WebViewRenderProcessClient mWebViewRenderProcessClient = this.mWebViewRenderProcessClient;
        final Executor mExecutor = this.mExecutor;
        if (mExecutor == null) {
            mWebViewRenderProcessClient.onRenderProcessResponsive(webView, forInvocationHandler);
        }
        else {
            mExecutor.execute(new Runnable(this, mWebViewRenderProcessClient, webView, forInvocationHandler) {
                final WebViewRenderProcessClientAdapter this$0;
                final WebViewRenderProcessClient val$client;
                final WebViewRenderProcess val$rendererObject;
                final WebView val$view;
                
                @Override
                public void run() {
                    this.val$client.onRenderProcessResponsive(this.val$view, this.val$rendererObject);
                }
            });
        }
    }
    
    public final void onRendererUnresponsive(final WebView webView, final InvocationHandler invocationHandler) {
        final WebViewRenderProcessImpl forInvocationHandler = WebViewRenderProcessImpl.forInvocationHandler(invocationHandler);
        final WebViewRenderProcessClient mWebViewRenderProcessClient = this.mWebViewRenderProcessClient;
        final Executor mExecutor = this.mExecutor;
        if (mExecutor == null) {
            mWebViewRenderProcessClient.onRenderProcessUnresponsive(webView, forInvocationHandler);
        }
        else {
            mExecutor.execute(new Runnable(this, mWebViewRenderProcessClient, webView, forInvocationHandler) {
                final WebViewRenderProcessClientAdapter this$0;
                final WebViewRenderProcessClient val$client;
                final WebViewRenderProcess val$rendererObject;
                final WebView val$view;
                
                @Override
                public void run() {
                    this.val$client.onRenderProcessUnresponsive(this.val$view, this.val$rendererObject);
                }
            });
        }
    }
}

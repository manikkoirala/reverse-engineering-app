// 
// Decompiled by Procyon v0.6.0
// 

package androidx.webkit.internal;

import java.util.List;
import org.chromium.support_lib_boundary.WebViewCookieManagerBoundaryInterface;

public class CookieManagerAdapter
{
    private final WebViewCookieManagerBoundaryInterface mBoundaryInterface;
    
    public CookieManagerAdapter(final WebViewCookieManagerBoundaryInterface mBoundaryInterface) {
        this.mBoundaryInterface = mBoundaryInterface;
    }
    
    public List<String> getCookieInfo(final String s) {
        return this.mBoundaryInterface.getCookieInfo(s);
    }
}

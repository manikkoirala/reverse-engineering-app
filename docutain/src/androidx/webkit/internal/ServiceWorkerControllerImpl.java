// 
// Decompiled by Procyon v0.6.0
// 

package androidx.webkit.internal;

import org.chromium.support_lib_boundary.util.BoundaryInterfaceReflectionUtil;
import java.lang.reflect.InvocationHandler;
import android.webkit.ServiceWorkerClient;
import androidx.webkit.ServiceWorkerClientCompat;
import androidx.webkit.ServiceWorkerWebSettingsCompat;
import android.webkit.ServiceWorkerController;
import org.chromium.support_lib_boundary.ServiceWorkerControllerBoundaryInterface;
import androidx.webkit.ServiceWorkerControllerCompat;

public class ServiceWorkerControllerImpl extends ServiceWorkerControllerCompat
{
    private ServiceWorkerControllerBoundaryInterface mBoundaryInterface;
    private ServiceWorkerController mFrameworksImpl;
    private final ServiceWorkerWebSettingsCompat mWebSettings;
    
    public ServiceWorkerControllerImpl() {
        final ApiFeature.N service_WORKER_BASIC_USAGE = WebViewFeatureInternal.SERVICE_WORKER_BASIC_USAGE;
        if (service_WORKER_BASIC_USAGE.isSupportedByFramework()) {
            this.mFrameworksImpl = ApiHelperForN.getServiceWorkerControllerInstance();
            this.mBoundaryInterface = null;
            this.mWebSettings = ApiHelperForN.getServiceWorkerWebSettingsImpl(this.getFrameworksImpl());
        }
        else {
            if (!service_WORKER_BASIC_USAGE.isSupportedByWebView()) {
                throw WebViewFeatureInternal.getUnsupportedOperationException();
            }
            this.mFrameworksImpl = null;
            final ServiceWorkerControllerBoundaryInterface serviceWorkerController = WebViewGlueCommunicator.getFactory().getServiceWorkerController();
            this.mBoundaryInterface = serviceWorkerController;
            this.mWebSettings = new ServiceWorkerWebSettingsImpl(serviceWorkerController.getServiceWorkerWebSettings());
        }
    }
    
    private ServiceWorkerControllerBoundaryInterface getBoundaryInterface() {
        if (this.mBoundaryInterface == null) {
            this.mBoundaryInterface = WebViewGlueCommunicator.getFactory().getServiceWorkerController();
        }
        return this.mBoundaryInterface;
    }
    
    private ServiceWorkerController getFrameworksImpl() {
        if (this.mFrameworksImpl == null) {
            this.mFrameworksImpl = ApiHelperForN.getServiceWorkerControllerInstance();
        }
        return this.mFrameworksImpl;
    }
    
    @Override
    public ServiceWorkerWebSettingsCompat getServiceWorkerWebSettings() {
        return this.mWebSettings;
    }
    
    @Override
    public void setServiceWorkerClient(final ServiceWorkerClientCompat serviceWorkerClientCompat) {
        final ApiFeature.N service_WORKER_BASIC_USAGE = WebViewFeatureInternal.SERVICE_WORKER_BASIC_USAGE;
        if (service_WORKER_BASIC_USAGE.isSupportedByFramework()) {
            if (serviceWorkerClientCompat == null) {
                ApiHelperForN.setServiceWorkerClient(this.getFrameworksImpl(), null);
            }
            else {
                ApiHelperForN.setServiceWorkerClientCompat(this.getFrameworksImpl(), serviceWorkerClientCompat);
            }
        }
        else {
            if (!service_WORKER_BASIC_USAGE.isSupportedByWebView()) {
                throw WebViewFeatureInternal.getUnsupportedOperationException();
            }
            if (serviceWorkerClientCompat == null) {
                this.getBoundaryInterface().setServiceWorkerClient((InvocationHandler)null);
            }
            else {
                this.getBoundaryInterface().setServiceWorkerClient(BoundaryInterfaceReflectionUtil.createInvocationHandlerFor((Object)new ServiceWorkerClientAdapter(serviceWorkerClientCompat)));
            }
        }
    }
}

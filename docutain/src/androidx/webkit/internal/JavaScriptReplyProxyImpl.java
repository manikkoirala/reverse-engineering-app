// 
// Decompiled by Procyon v0.6.0
// 

package androidx.webkit.internal;

import java.util.concurrent.Callable;
import org.chromium.support_lib_boundary.util.BoundaryInterfaceReflectionUtil;
import java.lang.reflect.InvocationHandler;
import org.chromium.support_lib_boundary.JsReplyProxyBoundaryInterface;
import androidx.webkit.JavaScriptReplyProxy;

public class JavaScriptReplyProxyImpl extends JavaScriptReplyProxy
{
    private JsReplyProxyBoundaryInterface mBoundaryInterface;
    
    public JavaScriptReplyProxyImpl(final JsReplyProxyBoundaryInterface mBoundaryInterface) {
        this.mBoundaryInterface = mBoundaryInterface;
    }
    
    public static JavaScriptReplyProxyImpl forInvocationHandler(final InvocationHandler invocationHandler) {
        final JsReplyProxyBoundaryInterface jsReplyProxyBoundaryInterface = (JsReplyProxyBoundaryInterface)BoundaryInterfaceReflectionUtil.castToSuppLibClass((Class)JsReplyProxyBoundaryInterface.class, invocationHandler);
        return (JavaScriptReplyProxyImpl)jsReplyProxyBoundaryInterface.getOrCreatePeer((Callable)new Callable<Object>(jsReplyProxyBoundaryInterface) {
            final JsReplyProxyBoundaryInterface val$boundaryInterface;
            
            @Override
            public Object call() {
                return new JavaScriptReplyProxyImpl(this.val$boundaryInterface);
            }
        });
    }
    
    @Override
    public void postMessage(final String s) {
        if (WebViewFeatureInternal.WEB_MESSAGE_LISTENER.isSupportedByWebView()) {
            this.mBoundaryInterface.postMessage(s);
            return;
        }
        throw WebViewFeatureInternal.getUnsupportedOperationException();
    }
}

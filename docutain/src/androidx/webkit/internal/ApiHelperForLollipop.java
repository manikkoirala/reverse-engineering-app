// 
// Decompiled by Procyon v0.6.0
// 

package androidx.webkit.internal;

import android.net.Uri;
import android.webkit.WebResourceRequest;

public class ApiHelperForLollipop
{
    private ApiHelperForLollipop() {
    }
    
    public static Uri getUrl(final WebResourceRequest webResourceRequest) {
        return webResourceRequest.getUrl();
    }
    
    public static boolean isForMainFrame(final WebResourceRequest webResourceRequest) {
        return webResourceRequest.isForMainFrame();
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.webkit.internal;

import java.lang.reflect.Proxy;
import org.chromium.support_lib_boundary.util.BoundaryInterfaceReflectionUtil;
import java.lang.reflect.InvocationHandler;
import android.webkit.SafeBrowsingResponse;
import org.chromium.support_lib_boundary.SafeBrowsingResponseBoundaryInterface;
import androidx.webkit.SafeBrowsingResponseCompat;

public class SafeBrowsingResponseImpl extends SafeBrowsingResponseCompat
{
    private SafeBrowsingResponseBoundaryInterface mBoundaryInterface;
    private SafeBrowsingResponse mFrameworksImpl;
    
    public SafeBrowsingResponseImpl(final SafeBrowsingResponse mFrameworksImpl) {
        this.mFrameworksImpl = mFrameworksImpl;
    }
    
    public SafeBrowsingResponseImpl(final InvocationHandler invocationHandler) {
        this.mBoundaryInterface = (SafeBrowsingResponseBoundaryInterface)BoundaryInterfaceReflectionUtil.castToSuppLibClass((Class)SafeBrowsingResponseBoundaryInterface.class, invocationHandler);
    }
    
    private SafeBrowsingResponseBoundaryInterface getBoundaryInterface() {
        if (this.mBoundaryInterface == null) {
            this.mBoundaryInterface = (SafeBrowsingResponseBoundaryInterface)BoundaryInterfaceReflectionUtil.castToSuppLibClass((Class)SafeBrowsingResponseBoundaryInterface.class, WebViewGlueCommunicator.getCompatConverter().convertSafeBrowsingResponse(this.mFrameworksImpl));
        }
        return this.mBoundaryInterface;
    }
    
    private SafeBrowsingResponse getFrameworksImpl() {
        if (this.mFrameworksImpl == null) {
            this.mFrameworksImpl = WebViewGlueCommunicator.getCompatConverter().convertSafeBrowsingResponse(Proxy.getInvocationHandler(this.mBoundaryInterface));
        }
        return this.mFrameworksImpl;
    }
    
    @Override
    public void backToSafety(final boolean b) {
        final ApiFeature.O_MR1 safe_BROWSING_RESPONSE_BACK_TO_SAFETY = WebViewFeatureInternal.SAFE_BROWSING_RESPONSE_BACK_TO_SAFETY;
        if (safe_BROWSING_RESPONSE_BACK_TO_SAFETY.isSupportedByFramework()) {
            ApiHelperForOMR1.backToSafety(this.getFrameworksImpl(), b);
        }
        else {
            if (!safe_BROWSING_RESPONSE_BACK_TO_SAFETY.isSupportedByWebView()) {
                throw WebViewFeatureInternal.getUnsupportedOperationException();
            }
            this.getBoundaryInterface().backToSafety(b);
        }
    }
    
    @Override
    public void proceed(final boolean b) {
        final ApiFeature.O_MR1 safe_BROWSING_RESPONSE_PROCEED = WebViewFeatureInternal.SAFE_BROWSING_RESPONSE_PROCEED;
        if (safe_BROWSING_RESPONSE_PROCEED.isSupportedByFramework()) {
            ApiHelperForOMR1.proceed(this.getFrameworksImpl(), b);
        }
        else {
            if (!safe_BROWSING_RESPONSE_PROCEED.isSupportedByWebView()) {
                throw WebViewFeatureInternal.getUnsupportedOperationException();
            }
            this.getBoundaryInterface().proceed(b);
        }
    }
    
    @Override
    public void showInterstitial(final boolean b) {
        final ApiFeature.O_MR1 safe_BROWSING_RESPONSE_SHOW_INTERSTITIAL = WebViewFeatureInternal.SAFE_BROWSING_RESPONSE_SHOW_INTERSTITIAL;
        if (safe_BROWSING_RESPONSE_SHOW_INTERSTITIAL.isSupportedByFramework()) {
            ApiHelperForOMR1.showInterstitial(this.getFrameworksImpl(), b);
        }
        else {
            if (!safe_BROWSING_RESPONSE_SHOW_INTERSTITIAL.isSupportedByWebView()) {
                throw WebViewFeatureInternal.getUnsupportedOperationException();
            }
            this.getBoundaryInterface().showInterstitial(b);
        }
    }
}

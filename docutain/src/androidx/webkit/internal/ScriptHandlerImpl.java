// 
// Decompiled by Procyon v0.6.0
// 

package androidx.webkit.internal;

import org.chromium.support_lib_boundary.util.BoundaryInterfaceReflectionUtil;
import java.lang.reflect.InvocationHandler;
import org.chromium.support_lib_boundary.ScriptHandlerBoundaryInterface;
import androidx.webkit.ScriptHandler;

public class ScriptHandlerImpl extends ScriptHandler
{
    private ScriptHandlerBoundaryInterface mBoundaryInterface;
    
    private ScriptHandlerImpl(final ScriptHandlerBoundaryInterface mBoundaryInterface) {
        this.mBoundaryInterface = mBoundaryInterface;
    }
    
    public static ScriptHandlerImpl toScriptHandler(final InvocationHandler invocationHandler) {
        return new ScriptHandlerImpl((ScriptHandlerBoundaryInterface)BoundaryInterfaceReflectionUtil.castToSuppLibClass((Class)ScriptHandlerBoundaryInterface.class, invocationHandler));
    }
    
    @Override
    public void remove() {
        this.mBoundaryInterface.remove();
    }
}

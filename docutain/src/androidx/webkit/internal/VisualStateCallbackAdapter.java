// 
// Decompiled by Procyon v0.6.0
// 

package androidx.webkit.internal;

import androidx.webkit.WebViewCompat;
import org.chromium.support_lib_boundary.VisualStateCallbackBoundaryInterface;

public class VisualStateCallbackAdapter implements VisualStateCallbackBoundaryInterface
{
    private final WebViewCompat.VisualStateCallback mVisualStateCallback;
    
    public VisualStateCallbackAdapter(final WebViewCompat.VisualStateCallback mVisualStateCallback) {
        this.mVisualStateCallback = mVisualStateCallback;
    }
    
    public void onComplete(final long n) {
        this.mVisualStateCallback.onComplete(n);
    }
}

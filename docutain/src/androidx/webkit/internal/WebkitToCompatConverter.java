// 
// Decompiled by Procyon v0.6.0
// 

package androidx.webkit.internal;

import org.chromium.support_lib_boundary.WebResourceRequestBoundaryInterface;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceError;
import android.webkit.WebMessagePort;
import org.chromium.support_lib_boundary.WebSettingsBoundaryInterface;
import android.webkit.WebSettings;
import android.webkit.ServiceWorkerWebSettings;
import android.webkit.SafeBrowsingResponse;
import java.lang.reflect.InvocationHandler;
import org.chromium.support_lib_boundary.util.BoundaryInterfaceReflectionUtil;
import org.chromium.support_lib_boundary.WebViewCookieManagerBoundaryInterface;
import android.webkit.CookieManager;
import org.chromium.support_lib_boundary.WebkitToCompatConverterBoundaryInterface;

public class WebkitToCompatConverter
{
    private final WebkitToCompatConverterBoundaryInterface mImpl;
    
    public WebkitToCompatConverter(final WebkitToCompatConverterBoundaryInterface mImpl) {
        this.mImpl = mImpl;
    }
    
    public CookieManagerAdapter convertCookieManager(final CookieManager cookieManager) {
        return new CookieManagerAdapter((WebViewCookieManagerBoundaryInterface)BoundaryInterfaceReflectionUtil.castToSuppLibClass((Class)WebViewCookieManagerBoundaryInterface.class, this.mImpl.convertCookieManager((Object)cookieManager)));
    }
    
    public SafeBrowsingResponse convertSafeBrowsingResponse(final InvocationHandler invocationHandler) {
        return (SafeBrowsingResponse)this.mImpl.convertSafeBrowsingResponse(invocationHandler);
    }
    
    public InvocationHandler convertSafeBrowsingResponse(final SafeBrowsingResponse safeBrowsingResponse) {
        return this.mImpl.convertSafeBrowsingResponse((Object)safeBrowsingResponse);
    }
    
    public ServiceWorkerWebSettings convertServiceWorkerSettings(final InvocationHandler invocationHandler) {
        return (ServiceWorkerWebSettings)this.mImpl.convertServiceWorkerSettings(invocationHandler);
    }
    
    public InvocationHandler convertServiceWorkerSettings(final ServiceWorkerWebSettings serviceWorkerWebSettings) {
        return this.mImpl.convertServiceWorkerSettings((Object)serviceWorkerWebSettings);
    }
    
    public WebSettingsAdapter convertSettings(final WebSettings webSettings) {
        return new WebSettingsAdapter((WebSettingsBoundaryInterface)BoundaryInterfaceReflectionUtil.castToSuppLibClass((Class)WebSettingsBoundaryInterface.class, this.mImpl.convertSettings(webSettings)));
    }
    
    public WebMessagePort convertWebMessagePort(final InvocationHandler invocationHandler) {
        return (WebMessagePort)this.mImpl.convertWebMessagePort(invocationHandler);
    }
    
    public InvocationHandler convertWebMessagePort(final WebMessagePort webMessagePort) {
        return this.mImpl.convertWebMessagePort((Object)webMessagePort);
    }
    
    public WebResourceError convertWebResourceError(final InvocationHandler invocationHandler) {
        return (WebResourceError)this.mImpl.convertWebResourceError(invocationHandler);
    }
    
    public InvocationHandler convertWebResourceError(final WebResourceError webResourceError) {
        return this.mImpl.convertWebResourceError((Object)webResourceError);
    }
    
    public WebResourceRequestAdapter convertWebResourceRequest(final WebResourceRequest webResourceRequest) {
        return new WebResourceRequestAdapter((WebResourceRequestBoundaryInterface)BoundaryInterfaceReflectionUtil.castToSuppLibClass((Class)WebResourceRequestBoundaryInterface.class, this.mImpl.convertWebResourceRequest(webResourceRequest)));
    }
}

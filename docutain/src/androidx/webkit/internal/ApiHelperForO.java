// 
// Decompiled by Procyon v0.6.0
// 

package androidx.webkit.internal;

import android.webkit.WebViewClient;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.content.pm.PackageInfo;

public class ApiHelperForO
{
    private ApiHelperForO() {
    }
    
    public static PackageInfo getCurrentWebViewPackage() {
        return WebView.getCurrentWebViewPackage();
    }
    
    public static boolean getSafeBrowsingEnabled(final WebSettings webSettings) {
        return webSettings.getSafeBrowsingEnabled();
    }
    
    public static WebChromeClient getWebChromeClient(final WebView webView) {
        return webView.getWebChromeClient();
    }
    
    public static WebViewClient getWebViewClient(final WebView webView) {
        return webView.getWebViewClient();
    }
    
    public static void setSafeBrowsingEnabled(final WebSettings webSettings, final boolean safeBrowsingEnabled) {
        webSettings.setSafeBrowsingEnabled(safeBrowsingEnabled);
    }
}

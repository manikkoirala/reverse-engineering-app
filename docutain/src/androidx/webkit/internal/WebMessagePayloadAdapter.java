// 
// Decompiled by Procyon v0.6.0
// 

package androidx.webkit.internal;

import androidx.webkit.WebMessageCompat;
import org.chromium.support_lib_boundary.WebMessagePayloadBoundaryInterface;

public class WebMessagePayloadAdapter implements WebMessagePayloadBoundaryInterface
{
    private final WebMessageCompat mMessageCompat;
    
    public WebMessagePayloadAdapter(final WebMessageCompat mMessageCompat) {
        this.mMessageCompat = mMessageCompat;
    }
    
    public byte[] getAsArrayBuffer() {
        return this.mMessageCompat.getArrayBuffer();
    }
    
    public String getAsString() {
        return this.mMessageCompat.getData();
    }
    
    public String[] getSupportedFeatures() {
        return new String[0];
    }
    
    public int getType() {
        final int type = this.mMessageCompat.getType();
        if (type == 0) {
            return 0;
        }
        if (type == 1) {
            return 1;
        }
        throw WebViewFeatureInternal.getUnsupportedOperationException();
    }
}

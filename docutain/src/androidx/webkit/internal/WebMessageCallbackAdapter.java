// 
// Decompiled by Procyon v0.6.0
// 

package androidx.webkit.internal;

import androidx.webkit.WebMessageCompat;
import org.chromium.support_lib_boundary.util.BoundaryInterfaceReflectionUtil;
import org.chromium.support_lib_boundary.WebMessageBoundaryInterface;
import java.lang.reflect.InvocationHandler;
import androidx.webkit.WebMessagePortCompat;
import org.chromium.support_lib_boundary.WebMessageCallbackBoundaryInterface;

public class WebMessageCallbackAdapter implements WebMessageCallbackBoundaryInterface
{
    private final WebMessagePortCompat.WebMessageCallbackCompat mImpl;
    
    public WebMessageCallbackAdapter(final WebMessagePortCompat.WebMessageCallbackCompat mImpl) {
        this.mImpl = mImpl;
    }
    
    public String[] getSupportedFeatures() {
        return new String[] { "WEB_MESSAGE_CALLBACK_ON_MESSAGE" };
    }
    
    public void onMessage(final InvocationHandler invocationHandler, final InvocationHandler invocationHandler2) {
        final WebMessageCompat webMessageCompatFromBoundaryInterface = WebMessageAdapter.webMessageCompatFromBoundaryInterface((WebMessageBoundaryInterface)BoundaryInterfaceReflectionUtil.castToSuppLibClass((Class)WebMessageBoundaryInterface.class, invocationHandler2));
        if (webMessageCompatFromBoundaryInterface != null) {
            this.mImpl.onMessage(new WebMessagePortImpl(invocationHandler), webMessageCompatFromBoundaryInterface);
        }
    }
}

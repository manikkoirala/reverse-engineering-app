// 
// Decompiled by Procyon v0.6.0
// 

package androidx.webkit.internal;

import java.util.concurrent.Executor;
import java.io.OutputStream;
import java.util.Collection;
import android.webkit.TracingConfig$Builder;
import androidx.webkit.TracingConfig;
import android.os.Looper;
import android.webkit.WebView;
import android.webkit.TracingController;

public class ApiHelperForP
{
    private ApiHelperForP() {
    }
    
    public static TracingController getTracingControllerInstance() {
        return TracingController.getInstance();
    }
    
    public static ClassLoader getWebViewClassLoader() {
        return WebView.getWebViewClassLoader();
    }
    
    public static Looper getWebViewLooper(final WebView webView) {
        return webView.getWebViewLooper();
    }
    
    public static boolean isTracing(final TracingController tracingController) {
        return tracingController.isTracing();
    }
    
    public static void setDataDirectorySuffix(final String dataDirectorySuffix) {
        WebView.setDataDirectorySuffix(dataDirectorySuffix);
    }
    
    public static void start(final TracingController tracingController, final TracingConfig tracingConfig) {
        tracingController.start(new TracingConfig$Builder().addCategories(new int[] { tracingConfig.getPredefinedCategories() }).addCategories((Collection)tracingConfig.getCustomIncludedCategories()).setTracingMode(tracingConfig.getTracingMode()).build());
    }
    
    public static boolean stop(final TracingController tracingController, final OutputStream outputStream, final Executor executor) {
        return tracingController.stop(outputStream, executor);
    }
}

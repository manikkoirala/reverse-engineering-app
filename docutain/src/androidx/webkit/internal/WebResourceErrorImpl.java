// 
// Decompiled by Procyon v0.6.0
// 

package androidx.webkit.internal;

import java.lang.reflect.Proxy;
import org.chromium.support_lib_boundary.util.BoundaryInterfaceReflectionUtil;
import java.lang.reflect.InvocationHandler;
import android.webkit.WebResourceError;
import org.chromium.support_lib_boundary.WebResourceErrorBoundaryInterface;
import androidx.webkit.WebResourceErrorCompat;

public class WebResourceErrorImpl extends WebResourceErrorCompat
{
    private WebResourceErrorBoundaryInterface mBoundaryInterface;
    private WebResourceError mFrameworksImpl;
    
    public WebResourceErrorImpl(final WebResourceError mFrameworksImpl) {
        this.mFrameworksImpl = mFrameworksImpl;
    }
    
    public WebResourceErrorImpl(final InvocationHandler invocationHandler) {
        this.mBoundaryInterface = (WebResourceErrorBoundaryInterface)BoundaryInterfaceReflectionUtil.castToSuppLibClass((Class)WebResourceErrorBoundaryInterface.class, invocationHandler);
    }
    
    private WebResourceErrorBoundaryInterface getBoundaryInterface() {
        if (this.mBoundaryInterface == null) {
            this.mBoundaryInterface = (WebResourceErrorBoundaryInterface)BoundaryInterfaceReflectionUtil.castToSuppLibClass((Class)WebResourceErrorBoundaryInterface.class, WebViewGlueCommunicator.getCompatConverter().convertWebResourceError(this.mFrameworksImpl));
        }
        return this.mBoundaryInterface;
    }
    
    private WebResourceError getFrameworksImpl() {
        if (this.mFrameworksImpl == null) {
            this.mFrameworksImpl = WebViewGlueCommunicator.getCompatConverter().convertWebResourceError(Proxy.getInvocationHandler(this.mBoundaryInterface));
        }
        return this.mFrameworksImpl;
    }
    
    @Override
    public CharSequence getDescription() {
        final ApiFeature.M web_RESOURCE_ERROR_GET_DESCRIPTION = WebViewFeatureInternal.WEB_RESOURCE_ERROR_GET_DESCRIPTION;
        if (web_RESOURCE_ERROR_GET_DESCRIPTION.isSupportedByFramework()) {
            return ApiHelperForM.getDescription(this.getFrameworksImpl());
        }
        if (web_RESOURCE_ERROR_GET_DESCRIPTION.isSupportedByWebView()) {
            return this.getBoundaryInterface().getDescription();
        }
        throw WebViewFeatureInternal.getUnsupportedOperationException();
    }
    
    @Override
    public int getErrorCode() {
        final ApiFeature.M web_RESOURCE_ERROR_GET_CODE = WebViewFeatureInternal.WEB_RESOURCE_ERROR_GET_CODE;
        if (web_RESOURCE_ERROR_GET_CODE.isSupportedByFramework()) {
            return ApiHelperForM.getErrorCode(this.getFrameworksImpl());
        }
        if (web_RESOURCE_ERROR_GET_CODE.isSupportedByWebView()) {
            return this.getBoundaryInterface().getErrorCode();
        }
        throw WebViewFeatureInternal.getUnsupportedOperationException();
    }
}

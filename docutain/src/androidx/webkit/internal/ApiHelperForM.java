// 
// Decompiled by Procyon v0.6.0
// 

package androidx.webkit.internal;

import android.os.Handler;
import android.webkit.WebMessagePort$WebMessageCallback;
import androidx.webkit.WebMessagePortCompat;
import android.net.Uri;
import android.webkit.WebView$VisualStateCallback;
import androidx.webkit.WebViewCompat;
import android.webkit.WebSettings;
import android.webkit.WebResourceError;
import android.webkit.WebView;
import android.webkit.WebMessage;
import androidx.webkit.WebMessageCompat;
import android.webkit.WebMessagePort;

public class ApiHelperForM
{
    private ApiHelperForM() {
    }
    
    public static void close(final WebMessagePort webMessagePort) {
        webMessagePort.close();
    }
    
    public static WebMessage createWebMessage(final WebMessageCompat webMessageCompat) {
        return new WebMessage(webMessageCompat.getData(), WebMessagePortImpl.compatToPorts(webMessageCompat.getPorts()));
    }
    
    public static WebMessagePort[] createWebMessageChannel(final WebView webView) {
        return webView.createWebMessageChannel();
    }
    
    public static WebMessageCompat createWebMessageCompat(final WebMessage webMessage) {
        return new WebMessageCompat(webMessage.getData(), WebMessagePortImpl.portsToCompat(webMessage.getPorts()));
    }
    
    public static CharSequence getDescription(final WebResourceError webResourceError) {
        return webResourceError.getDescription();
    }
    
    public static int getErrorCode(final WebResourceError webResourceError) {
        return webResourceError.getErrorCode();
    }
    
    public static boolean getOffscreenPreRaster(final WebSettings webSettings) {
        return webSettings.getOffscreenPreRaster();
    }
    
    public static void postMessage(final WebMessagePort webMessagePort, final WebMessage webMessage) {
        webMessagePort.postMessage(webMessage);
    }
    
    public static void postVisualStateCallback(final WebView webView, final long n, final WebViewCompat.VisualStateCallback visualStateCallback) {
        webView.postVisualStateCallback(n, (WebView$VisualStateCallback)new WebView$VisualStateCallback(visualStateCallback) {
            final WebViewCompat.VisualStateCallback val$callback;
            
            public void onComplete(final long n) {
                this.val$callback.onComplete(n);
            }
        });
    }
    
    public static void postWebMessage(final WebView webView, final WebMessage webMessage, final Uri uri) {
        webView.postWebMessage(webMessage, uri);
    }
    
    public static void setOffscreenPreRaster(final WebSettings webSettings, final boolean offscreenPreRaster) {
        webSettings.setOffscreenPreRaster(offscreenPreRaster);
    }
    
    public static void setWebMessageCallback(final WebMessagePort webMessagePort, final WebMessagePortCompat.WebMessageCallbackCompat webMessageCallbackCompat) {
        webMessagePort.setWebMessageCallback((WebMessagePort$WebMessageCallback)new WebMessagePort$WebMessageCallback(webMessageCallbackCompat) {
            final WebMessagePortCompat.WebMessageCallbackCompat val$callback;
            
            public void onMessage(final WebMessagePort webMessagePort, final WebMessage webMessage) {
                this.val$callback.onMessage(new WebMessagePortImpl(webMessagePort), WebMessagePortImpl.frameworkMessageToCompat(webMessage));
            }
        });
    }
    
    public static void setWebMessageCallback(final WebMessagePort webMessagePort, final WebMessagePortCompat.WebMessageCallbackCompat webMessageCallbackCompat, final Handler handler) {
        webMessagePort.setWebMessageCallback((WebMessagePort$WebMessageCallback)new WebMessagePort$WebMessageCallback(webMessageCallbackCompat) {
            final WebMessagePortCompat.WebMessageCallbackCompat val$callback;
            
            public void onMessage(final WebMessagePort webMessagePort, final WebMessage webMessage) {
                this.val$callback.onMessage(new WebMessagePortImpl(webMessagePort), WebMessagePortImpl.frameworkMessageToCompat(webMessage));
            }
        }, handler);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.webkit.internal;

import java.util.concurrent.Executor;
import androidx.webkit.ProxyConfig;
import java.util.List;
import org.chromium.support_lib_boundary.ProxyControllerBoundaryInterface;
import androidx.webkit.ProxyController;

public class ProxyControllerImpl extends ProxyController
{
    private ProxyControllerBoundaryInterface mBoundaryInterface;
    
    private ProxyControllerBoundaryInterface getBoundaryInterface() {
        if (this.mBoundaryInterface == null) {
            this.mBoundaryInterface = WebViewGlueCommunicator.getFactory().getProxyController();
        }
        return this.mBoundaryInterface;
    }
    
    public static String[][] proxyRulesToStringArray(final List<ProxyConfig.ProxyRule> list) {
        final String[][] array = new String[list.size()][2];
        for (int i = 0; i < list.size(); ++i) {
            array[i][0] = ((ProxyConfig.ProxyRule)list.get(i)).getSchemeFilter();
            array[i][1] = ((ProxyConfig.ProxyRule)list.get(i)).getUrl();
        }
        return array;
    }
    
    @Override
    public void clearProxyOverride(final Executor executor, final Runnable runnable) {
        if (WebViewFeatureInternal.PROXY_OVERRIDE.isSupportedByWebView()) {
            this.getBoundaryInterface().clearProxyOverride(runnable, executor);
            return;
        }
        throw WebViewFeatureInternal.getUnsupportedOperationException();
    }
    
    @Override
    public void setProxyOverride(final ProxyConfig proxyConfig, final Executor executor, final Runnable runnable) {
        final ApiFeature.NoFramework proxy_OVERRIDE = WebViewFeatureInternal.PROXY_OVERRIDE;
        final ApiFeature.NoFramework proxy_OVERRIDE_REVERSE_BYPASS = WebViewFeatureInternal.PROXY_OVERRIDE_REVERSE_BYPASS;
        final String[][] proxyRulesToStringArray = proxyRulesToStringArray(proxyConfig.getProxyRules());
        final String[] array = proxyConfig.getBypassRules().toArray(new String[0]);
        if (proxy_OVERRIDE.isSupportedByWebView() && !proxyConfig.isReverseBypassEnabled()) {
            this.getBoundaryInterface().setProxyOverride(proxyRulesToStringArray, array, runnable, executor);
        }
        else {
            if (!proxy_OVERRIDE.isSupportedByWebView() || !proxy_OVERRIDE_REVERSE_BYPASS.isSupportedByWebView()) {
                throw WebViewFeatureInternal.getUnsupportedOperationException();
            }
            this.getBoundaryInterface().setProxyOverride(proxyRulesToStringArray, array, runnable, executor, proxyConfig.isReverseBypassEnabled());
        }
    }
}

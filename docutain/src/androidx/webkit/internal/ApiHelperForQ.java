// 
// Decompiled by Procyon v0.6.0
// 

package androidx.webkit.internal;

import java.util.concurrent.Executor;
import android.webkit.WebViewRenderProcessClient;
import android.webkit.WebViewRenderProcess;
import android.webkit.WebView;
import android.webkit.WebSettings;

public class ApiHelperForQ
{
    private ApiHelperForQ() {
    }
    
    @Deprecated
    public static int getForceDark(final WebSettings webSettings) {
        return webSettings.getForceDark();
    }
    
    public static WebViewRenderProcess getWebViewRenderProcess(final WebView webView) {
        return webView.getWebViewRenderProcess();
    }
    
    public static WebViewRenderProcessClient getWebViewRenderProcessClient(final WebView webView) {
        return webView.getWebViewRenderProcessClient();
    }
    
    @Deprecated
    public static void setForceDark(final WebSettings webSettings, final int forceDark) {
        webSettings.setForceDark(forceDark);
    }
    
    public static void setWebViewRenderProcessClient(final WebView webView, final androidx.webkit.WebViewRenderProcessClient webViewRenderProcessClient) {
        WebViewRenderProcessClientFrameworkAdapter webViewRenderProcessClient2;
        if (webViewRenderProcessClient != null) {
            webViewRenderProcessClient2 = new WebViewRenderProcessClientFrameworkAdapter(webViewRenderProcessClient);
        }
        else {
            webViewRenderProcessClient2 = null;
        }
        webView.setWebViewRenderProcessClient((WebViewRenderProcessClient)webViewRenderProcessClient2);
    }
    
    public static void setWebViewRenderProcessClient(final WebView webView, final Executor executor, final androidx.webkit.WebViewRenderProcessClient webViewRenderProcessClient) {
        WebViewRenderProcessClientFrameworkAdapter webViewRenderProcessClientFrameworkAdapter;
        if (webViewRenderProcessClient != null) {
            webViewRenderProcessClientFrameworkAdapter = new WebViewRenderProcessClientFrameworkAdapter(webViewRenderProcessClient);
        }
        else {
            webViewRenderProcessClientFrameworkAdapter = null;
        }
        webView.setWebViewRenderProcessClient(executor, (WebViewRenderProcessClient)webViewRenderProcessClientFrameworkAdapter);
    }
    
    public static boolean terminate(final WebViewRenderProcess webViewRenderProcess) {
        return webViewRenderProcess.terminate();
    }
}

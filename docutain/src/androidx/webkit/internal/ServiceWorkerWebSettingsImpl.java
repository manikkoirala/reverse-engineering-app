// 
// Decompiled by Procyon v0.6.0
// 

package androidx.webkit.internal;

import java.util.Set;
import java.lang.reflect.Proxy;
import org.chromium.support_lib_boundary.util.BoundaryInterfaceReflectionUtil;
import java.lang.reflect.InvocationHandler;
import android.webkit.ServiceWorkerWebSettings;
import org.chromium.support_lib_boundary.ServiceWorkerWebSettingsBoundaryInterface;
import androidx.webkit.ServiceWorkerWebSettingsCompat;

public class ServiceWorkerWebSettingsImpl extends ServiceWorkerWebSettingsCompat
{
    private ServiceWorkerWebSettingsBoundaryInterface mBoundaryInterface;
    private ServiceWorkerWebSettings mFrameworksImpl;
    
    public ServiceWorkerWebSettingsImpl(final ServiceWorkerWebSettings mFrameworksImpl) {
        this.mFrameworksImpl = mFrameworksImpl;
    }
    
    public ServiceWorkerWebSettingsImpl(final InvocationHandler invocationHandler) {
        this.mBoundaryInterface = (ServiceWorkerWebSettingsBoundaryInterface)BoundaryInterfaceReflectionUtil.castToSuppLibClass((Class)ServiceWorkerWebSettingsBoundaryInterface.class, invocationHandler);
    }
    
    private ServiceWorkerWebSettingsBoundaryInterface getBoundaryInterface() {
        if (this.mBoundaryInterface == null) {
            this.mBoundaryInterface = (ServiceWorkerWebSettingsBoundaryInterface)BoundaryInterfaceReflectionUtil.castToSuppLibClass((Class)ServiceWorkerWebSettingsBoundaryInterface.class, WebViewGlueCommunicator.getCompatConverter().convertServiceWorkerSettings(this.mFrameworksImpl));
        }
        return this.mBoundaryInterface;
    }
    
    private ServiceWorkerWebSettings getFrameworksImpl() {
        if (this.mFrameworksImpl == null) {
            this.mFrameworksImpl = WebViewGlueCommunicator.getCompatConverter().convertServiceWorkerSettings(Proxy.getInvocationHandler(this.mBoundaryInterface));
        }
        return this.mFrameworksImpl;
    }
    
    @Override
    public boolean getAllowContentAccess() {
        final ApiFeature.N service_WORKER_CONTENT_ACCESS = WebViewFeatureInternal.SERVICE_WORKER_CONTENT_ACCESS;
        if (service_WORKER_CONTENT_ACCESS.isSupportedByFramework()) {
            return ApiHelperForN.getAllowContentAccess(this.getFrameworksImpl());
        }
        if (service_WORKER_CONTENT_ACCESS.isSupportedByWebView()) {
            return this.getBoundaryInterface().getAllowContentAccess();
        }
        throw WebViewFeatureInternal.getUnsupportedOperationException();
    }
    
    @Override
    public boolean getAllowFileAccess() {
        final ApiFeature.N service_WORKER_FILE_ACCESS = WebViewFeatureInternal.SERVICE_WORKER_FILE_ACCESS;
        if (service_WORKER_FILE_ACCESS.isSupportedByFramework()) {
            return ApiHelperForN.getAllowFileAccess(this.getFrameworksImpl());
        }
        if (service_WORKER_FILE_ACCESS.isSupportedByWebView()) {
            return this.getBoundaryInterface().getAllowFileAccess();
        }
        throw WebViewFeatureInternal.getUnsupportedOperationException();
    }
    
    @Override
    public boolean getBlockNetworkLoads() {
        final ApiFeature.N service_WORKER_BLOCK_NETWORK_LOADS = WebViewFeatureInternal.SERVICE_WORKER_BLOCK_NETWORK_LOADS;
        if (service_WORKER_BLOCK_NETWORK_LOADS.isSupportedByFramework()) {
            return ApiHelperForN.getBlockNetworkLoads(this.getFrameworksImpl());
        }
        if (service_WORKER_BLOCK_NETWORK_LOADS.isSupportedByWebView()) {
            return this.getBoundaryInterface().getBlockNetworkLoads();
        }
        throw WebViewFeatureInternal.getUnsupportedOperationException();
    }
    
    @Override
    public int getCacheMode() {
        final ApiFeature.N service_WORKER_CACHE_MODE = WebViewFeatureInternal.SERVICE_WORKER_CACHE_MODE;
        if (service_WORKER_CACHE_MODE.isSupportedByFramework()) {
            return ApiHelperForN.getCacheMode(this.getFrameworksImpl());
        }
        if (service_WORKER_CACHE_MODE.isSupportedByWebView()) {
            return this.getBoundaryInterface().getCacheMode();
        }
        throw WebViewFeatureInternal.getUnsupportedOperationException();
    }
    
    @Override
    public Set<String> getRequestedWithHeaderOriginAllowList() {
        if (WebViewFeatureInternal.REQUESTED_WITH_HEADER_ALLOW_LIST.isSupportedByWebView()) {
            return this.getBoundaryInterface().getRequestedWithHeaderOriginAllowList();
        }
        throw WebViewFeatureInternal.getUnsupportedOperationException();
    }
    
    @Override
    public void setAllowContentAccess(final boolean allowContentAccess) {
        final ApiFeature.N service_WORKER_CONTENT_ACCESS = WebViewFeatureInternal.SERVICE_WORKER_CONTENT_ACCESS;
        if (service_WORKER_CONTENT_ACCESS.isSupportedByFramework()) {
            ApiHelperForN.setAllowContentAccess(this.getFrameworksImpl(), allowContentAccess);
        }
        else {
            if (!service_WORKER_CONTENT_ACCESS.isSupportedByWebView()) {
                throw WebViewFeatureInternal.getUnsupportedOperationException();
            }
            this.getBoundaryInterface().setAllowContentAccess(allowContentAccess);
        }
    }
    
    @Override
    public void setAllowFileAccess(final boolean allowFileAccess) {
        final ApiFeature.N service_WORKER_FILE_ACCESS = WebViewFeatureInternal.SERVICE_WORKER_FILE_ACCESS;
        if (service_WORKER_FILE_ACCESS.isSupportedByFramework()) {
            ApiHelperForN.setAllowFileAccess(this.getFrameworksImpl(), allowFileAccess);
        }
        else {
            if (!service_WORKER_FILE_ACCESS.isSupportedByWebView()) {
                throw WebViewFeatureInternal.getUnsupportedOperationException();
            }
            this.getBoundaryInterface().setAllowFileAccess(allowFileAccess);
        }
    }
    
    @Override
    public void setBlockNetworkLoads(final boolean blockNetworkLoads) {
        final ApiFeature.N service_WORKER_BLOCK_NETWORK_LOADS = WebViewFeatureInternal.SERVICE_WORKER_BLOCK_NETWORK_LOADS;
        if (service_WORKER_BLOCK_NETWORK_LOADS.isSupportedByFramework()) {
            ApiHelperForN.setBlockNetworkLoads(this.getFrameworksImpl(), blockNetworkLoads);
        }
        else {
            if (!service_WORKER_BLOCK_NETWORK_LOADS.isSupportedByWebView()) {
                throw WebViewFeatureInternal.getUnsupportedOperationException();
            }
            this.getBoundaryInterface().setBlockNetworkLoads(blockNetworkLoads);
        }
    }
    
    @Override
    public void setCacheMode(final int cacheMode) {
        final ApiFeature.N service_WORKER_CACHE_MODE = WebViewFeatureInternal.SERVICE_WORKER_CACHE_MODE;
        if (service_WORKER_CACHE_MODE.isSupportedByFramework()) {
            ApiHelperForN.setCacheMode(this.getFrameworksImpl(), cacheMode);
        }
        else {
            if (!service_WORKER_CACHE_MODE.isSupportedByWebView()) {
                throw WebViewFeatureInternal.getUnsupportedOperationException();
            }
            this.getBoundaryInterface().setCacheMode(cacheMode);
        }
    }
    
    @Override
    public void setRequestedWithHeaderOriginAllowList(final Set<String> requestedWithHeaderOriginAllowList) {
        if (WebViewFeatureInternal.REQUESTED_WITH_HEADER_ALLOW_LIST.isSupportedByWebView()) {
            this.getBoundaryInterface().setRequestedWithHeaderOriginAllowList((Set)requestedWithHeaderOriginAllowList);
            return;
        }
        throw WebViewFeatureInternal.getUnsupportedOperationException();
    }
}

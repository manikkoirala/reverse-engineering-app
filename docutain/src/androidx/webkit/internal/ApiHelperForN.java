// 
// Decompiled by Procyon v0.6.0
// 

package androidx.webkit.internal;

import androidx.webkit.ServiceWorkerClientCompat;
import android.webkit.ServiceWorkerClient;
import android.webkit.WebResourceRequest;
import android.webkit.ServiceWorkerController;
import android.webkit.WebSettings;
import java.io.File;
import android.content.Context;
import android.webkit.ServiceWorkerWebSettings;

public class ApiHelperForN
{
    private ApiHelperForN() {
    }
    
    public static boolean getAllowContentAccess(final ServiceWorkerWebSettings serviceWorkerWebSettings) {
        return serviceWorkerWebSettings.getAllowContentAccess();
    }
    
    public static boolean getAllowFileAccess(final ServiceWorkerWebSettings serviceWorkerWebSettings) {
        return serviceWorkerWebSettings.getAllowFileAccess();
    }
    
    public static boolean getBlockNetworkLoads(final ServiceWorkerWebSettings serviceWorkerWebSettings) {
        return serviceWorkerWebSettings.getBlockNetworkLoads();
    }
    
    public static int getCacheMode(final ServiceWorkerWebSettings serviceWorkerWebSettings) {
        return serviceWorkerWebSettings.getCacheMode();
    }
    
    public static File getDataDir(final Context context) {
        return context.getDataDir();
    }
    
    public static int getDisabledActionModeMenuItems(final WebSettings webSettings) {
        return webSettings.getDisabledActionModeMenuItems();
    }
    
    public static ServiceWorkerController getServiceWorkerControllerInstance() {
        return ServiceWorkerController.getInstance();
    }
    
    public static ServiceWorkerWebSettings getServiceWorkerWebSettings(final ServiceWorkerController serviceWorkerController) {
        return serviceWorkerController.getServiceWorkerWebSettings();
    }
    
    public static ServiceWorkerWebSettingsImpl getServiceWorkerWebSettingsImpl(final ServiceWorkerController serviceWorkerController) {
        return new ServiceWorkerWebSettingsImpl(getServiceWorkerWebSettings(serviceWorkerController));
    }
    
    public static boolean isRedirect(final WebResourceRequest webResourceRequest) {
        return webResourceRequest.isRedirect();
    }
    
    public static void setAllowContentAccess(final ServiceWorkerWebSettings serviceWorkerWebSettings, final boolean allowContentAccess) {
        serviceWorkerWebSettings.setAllowContentAccess(allowContentAccess);
    }
    
    public static void setAllowFileAccess(final ServiceWorkerWebSettings serviceWorkerWebSettings, final boolean allowFileAccess) {
        serviceWorkerWebSettings.setAllowFileAccess(allowFileAccess);
    }
    
    public static void setBlockNetworkLoads(final ServiceWorkerWebSettings serviceWorkerWebSettings, final boolean blockNetworkLoads) {
        serviceWorkerWebSettings.setBlockNetworkLoads(blockNetworkLoads);
    }
    
    public static void setCacheMode(final ServiceWorkerWebSettings serviceWorkerWebSettings, final int cacheMode) {
        serviceWorkerWebSettings.setCacheMode(cacheMode);
    }
    
    public static void setDisabledActionModeMenuItems(final WebSettings webSettings, final int disabledActionModeMenuItems) {
        webSettings.setDisabledActionModeMenuItems(disabledActionModeMenuItems);
    }
    
    public static void setServiceWorkerClient(final ServiceWorkerController serviceWorkerController, final ServiceWorkerClient serviceWorkerClient) {
        serviceWorkerController.setServiceWorkerClient(serviceWorkerClient);
    }
    
    public static void setServiceWorkerClientCompat(final ServiceWorkerController serviceWorkerController, final ServiceWorkerClientCompat serviceWorkerClientCompat) {
        serviceWorkerController.setServiceWorkerClient((ServiceWorkerClient)new FrameworkServiceWorkerClient(serviceWorkerClientCompat));
    }
}

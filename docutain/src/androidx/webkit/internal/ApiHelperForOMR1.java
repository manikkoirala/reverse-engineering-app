// 
// Decompiled by Procyon v0.6.0
// 

package androidx.webkit.internal;

import android.content.Context;
import android.webkit.ValueCallback;
import java.util.List;
import android.webkit.WebView;
import android.net.Uri;
import android.webkit.SafeBrowsingResponse;

public class ApiHelperForOMR1
{
    private ApiHelperForOMR1() {
    }
    
    public static void backToSafety(final SafeBrowsingResponse safeBrowsingResponse, final boolean b) {
        safeBrowsingResponse.backToSafety(b);
    }
    
    public static Uri getSafeBrowsingPrivacyPolicyUrl() {
        return WebView.getSafeBrowsingPrivacyPolicyUrl();
    }
    
    public static void proceed(final SafeBrowsingResponse safeBrowsingResponse, final boolean b) {
        safeBrowsingResponse.proceed(b);
    }
    
    public static void setSafeBrowsingWhitelist(final List<String> list, final ValueCallback<Boolean> valueCallback) {
        WebView.setSafeBrowsingWhitelist((List)list, (ValueCallback)valueCallback);
    }
    
    public static void showInterstitial(final SafeBrowsingResponse safeBrowsingResponse, final boolean b) {
        safeBrowsingResponse.showInterstitial(b);
    }
    
    public static void startSafeBrowsing(final Context context, final ValueCallback<Boolean> valueCallback) {
        WebView.startSafeBrowsing(context, (ValueCallback)valueCallback);
    }
}

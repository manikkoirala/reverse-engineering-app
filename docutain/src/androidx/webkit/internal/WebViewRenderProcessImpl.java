// 
// Decompiled by Procyon v0.6.0
// 

package androidx.webkit.internal;

import java.util.concurrent.Callable;
import org.chromium.support_lib_boundary.util.BoundaryInterfaceReflectionUtil;
import java.lang.reflect.InvocationHandler;
import java.lang.ref.WeakReference;
import org.chromium.support_lib_boundary.WebViewRendererBoundaryInterface;
import java.util.WeakHashMap;
import androidx.webkit.WebViewRenderProcess;

public class WebViewRenderProcessImpl extends WebViewRenderProcess
{
    private static final WeakHashMap<android.webkit.WebViewRenderProcess, WebViewRenderProcessImpl> sFrameworkMap;
    private WebViewRendererBoundaryInterface mBoundaryInterface;
    private WeakReference<android.webkit.WebViewRenderProcess> mFrameworkObject;
    
    static {
        sFrameworkMap = new WeakHashMap<android.webkit.WebViewRenderProcess, WebViewRenderProcessImpl>();
    }
    
    public WebViewRenderProcessImpl(final android.webkit.WebViewRenderProcess referent) {
        this.mFrameworkObject = new WeakReference<android.webkit.WebViewRenderProcess>(referent);
    }
    
    public WebViewRenderProcessImpl(final WebViewRendererBoundaryInterface mBoundaryInterface) {
        this.mBoundaryInterface = mBoundaryInterface;
    }
    
    public static WebViewRenderProcessImpl forFrameworkObject(final android.webkit.WebViewRenderProcess webViewRenderProcess) {
        final WeakHashMap<android.webkit.WebViewRenderProcess, WebViewRenderProcessImpl> sFrameworkMap = WebViewRenderProcessImpl.sFrameworkMap;
        final WebViewRenderProcessImpl webViewRenderProcessImpl = sFrameworkMap.get(webViewRenderProcess);
        if (webViewRenderProcessImpl != null) {
            return webViewRenderProcessImpl;
        }
        final WebViewRenderProcessImpl value = new WebViewRenderProcessImpl(webViewRenderProcess);
        sFrameworkMap.put(webViewRenderProcess, value);
        return value;
    }
    
    public static WebViewRenderProcessImpl forInvocationHandler(final InvocationHandler invocationHandler) {
        final WebViewRendererBoundaryInterface webViewRendererBoundaryInterface = (WebViewRendererBoundaryInterface)BoundaryInterfaceReflectionUtil.castToSuppLibClass((Class)WebViewRendererBoundaryInterface.class, invocationHandler);
        return (WebViewRenderProcessImpl)webViewRendererBoundaryInterface.getOrCreatePeer((Callable)new Callable<Object>(webViewRendererBoundaryInterface) {
            final WebViewRendererBoundaryInterface val$boundaryInterface;
            
            @Override
            public Object call() {
                return new WebViewRenderProcessImpl(this.val$boundaryInterface);
            }
        });
    }
    
    @Override
    public boolean terminate() {
        final ApiFeature.Q web_VIEW_RENDERER_TERMINATE = WebViewFeatureInternal.WEB_VIEW_RENDERER_TERMINATE;
        if (web_VIEW_RENDERER_TERMINATE.isSupportedByFramework()) {
            final android.webkit.WebViewRenderProcess webViewRenderProcess = this.mFrameworkObject.get();
            return webViewRenderProcess != null && ApiHelperForQ.terminate(webViewRenderProcess);
        }
        if (web_VIEW_RENDERER_TERMINATE.isSupportedByWebView()) {
            return this.mBoundaryInterface.terminate();
        }
        throw WebViewFeatureInternal.getUnsupportedOperationException();
    }
}

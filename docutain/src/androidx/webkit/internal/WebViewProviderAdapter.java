// 
// Decompiled by Procyon v0.6.0
// 

package androidx.webkit.internal;

import java.util.concurrent.Executor;
import android.net.Uri;
import androidx.webkit.WebMessageCompat;
import androidx.webkit.WebViewRenderProcessClient;
import androidx.webkit.WebViewRenderProcess;
import android.webkit.WebViewClient;
import android.webkit.WebChromeClient;
import java.lang.reflect.InvocationHandler;
import androidx.webkit.WebMessagePortCompat;
import org.chromium.support_lib_boundary.util.BoundaryInterfaceReflectionUtil;
import androidx.webkit.WebViewCompat;
import org.chromium.support_lib_boundary.WebViewProviderBoundaryInterface;

public class WebViewProviderAdapter
{
    WebViewProviderBoundaryInterface mImpl;
    
    public WebViewProviderAdapter(final WebViewProviderBoundaryInterface mImpl) {
        this.mImpl = mImpl;
    }
    
    public ScriptHandlerImpl addDocumentStartJavaScript(final String s, final String[] array) {
        return ScriptHandlerImpl.toScriptHandler(this.mImpl.addDocumentStartJavaScript(s, array));
    }
    
    public void addWebMessageListener(final String s, final String[] array, final WebViewCompat.WebMessageListener webMessageListener) {
        this.mImpl.addWebMessageListener(s, array, BoundaryInterfaceReflectionUtil.createInvocationHandlerFor((Object)new WebMessageListenerAdapter(webMessageListener)));
    }
    
    public WebMessagePortCompat[] createWebMessageChannel() {
        final InvocationHandler[] webMessageChannel = this.mImpl.createWebMessageChannel();
        final WebMessagePortCompat[] array = new WebMessagePortCompat[webMessageChannel.length];
        for (int i = 0; i < webMessageChannel.length; ++i) {
            array[i] = new WebMessagePortImpl(webMessageChannel[i]);
        }
        return array;
    }
    
    public WebChromeClient getWebChromeClient() {
        return this.mImpl.getWebChromeClient();
    }
    
    public WebViewClient getWebViewClient() {
        return this.mImpl.getWebViewClient();
    }
    
    public WebViewRenderProcess getWebViewRenderProcess() {
        return WebViewRenderProcessImpl.forInvocationHandler(this.mImpl.getWebViewRenderer());
    }
    
    public WebViewRenderProcessClient getWebViewRenderProcessClient() {
        final InvocationHandler webViewRendererClient = this.mImpl.getWebViewRendererClient();
        if (webViewRendererClient == null) {
            return null;
        }
        return ((WebViewRenderProcessClientAdapter)BoundaryInterfaceReflectionUtil.getDelegateFromInvocationHandler(webViewRendererClient)).getWebViewRenderProcessClient();
    }
    
    public void insertVisualStateCallback(final long n, final WebViewCompat.VisualStateCallback visualStateCallback) {
        this.mImpl.insertVisualStateCallback(n, BoundaryInterfaceReflectionUtil.createInvocationHandlerFor((Object)new VisualStateCallbackAdapter(visualStateCallback)));
    }
    
    public void postWebMessage(final WebMessageCompat webMessageCompat, final Uri uri) {
        this.mImpl.postMessageToMainFrame(BoundaryInterfaceReflectionUtil.createInvocationHandlerFor((Object)new WebMessageAdapter(webMessageCompat)), uri);
    }
    
    public void removeWebMessageListener(final String s) {
        this.mImpl.removeWebMessageListener(s);
    }
    
    public void setWebViewRenderProcessClient(final Executor executor, final WebViewRenderProcessClient webViewRenderProcessClient) {
        InvocationHandler invocationHandler;
        if (webViewRenderProcessClient != null) {
            invocationHandler = BoundaryInterfaceReflectionUtil.createInvocationHandlerFor((Object)new WebViewRenderProcessClientAdapter(executor, webViewRenderProcessClient));
        }
        else {
            invocationHandler = null;
        }
        this.mImpl.setWebViewRendererClient(invocationHandler);
    }
}

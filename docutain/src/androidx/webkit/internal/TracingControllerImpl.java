// 
// Decompiled by Procyon v0.6.0
// 

package androidx.webkit.internal;

import java.util.concurrent.Executor;
import java.io.OutputStream;
import java.util.Collection;
import androidx.webkit.TracingConfig;
import org.chromium.support_lib_boundary.TracingControllerBoundaryInterface;
import androidx.webkit.TracingController;

public class TracingControllerImpl extends TracingController
{
    private TracingControllerBoundaryInterface mBoundaryInterface;
    private android.webkit.TracingController mFrameworksImpl;
    
    public TracingControllerImpl() {
        final ApiFeature.P tracing_CONTROLLER_BASIC_USAGE = WebViewFeatureInternal.TRACING_CONTROLLER_BASIC_USAGE;
        if (tracing_CONTROLLER_BASIC_USAGE.isSupportedByFramework()) {
            this.mFrameworksImpl = ApiHelperForP.getTracingControllerInstance();
            this.mBoundaryInterface = null;
        }
        else {
            if (!tracing_CONTROLLER_BASIC_USAGE.isSupportedByWebView()) {
                throw WebViewFeatureInternal.getUnsupportedOperationException();
            }
            this.mFrameworksImpl = null;
            this.mBoundaryInterface = WebViewGlueCommunicator.getFactory().getTracingController();
        }
    }
    
    private TracingControllerBoundaryInterface getBoundaryInterface() {
        if (this.mBoundaryInterface == null) {
            this.mBoundaryInterface = WebViewGlueCommunicator.getFactory().getTracingController();
        }
        return this.mBoundaryInterface;
    }
    
    private android.webkit.TracingController getFrameworksImpl() {
        if (this.mFrameworksImpl == null) {
            this.mFrameworksImpl = ApiHelperForP.getTracingControllerInstance();
        }
        return this.mFrameworksImpl;
    }
    
    @Override
    public boolean isTracing() {
        final ApiFeature.P tracing_CONTROLLER_BASIC_USAGE = WebViewFeatureInternal.TRACING_CONTROLLER_BASIC_USAGE;
        if (tracing_CONTROLLER_BASIC_USAGE.isSupportedByFramework()) {
            return ApiHelperForP.isTracing(this.getFrameworksImpl());
        }
        if (tracing_CONTROLLER_BASIC_USAGE.isSupportedByWebView()) {
            return this.getBoundaryInterface().isTracing();
        }
        throw WebViewFeatureInternal.getUnsupportedOperationException();
    }
    
    @Override
    public void start(final TracingConfig tracingConfig) {
        if (tracingConfig != null) {
            final ApiFeature.P tracing_CONTROLLER_BASIC_USAGE = WebViewFeatureInternal.TRACING_CONTROLLER_BASIC_USAGE;
            if (tracing_CONTROLLER_BASIC_USAGE.isSupportedByFramework()) {
                ApiHelperForP.start(this.getFrameworksImpl(), tracingConfig);
            }
            else {
                if (!tracing_CONTROLLER_BASIC_USAGE.isSupportedByWebView()) {
                    throw WebViewFeatureInternal.getUnsupportedOperationException();
                }
                this.getBoundaryInterface().start(tracingConfig.getPredefinedCategories(), (Collection)tracingConfig.getCustomIncludedCategories(), tracingConfig.getTracingMode());
            }
            return;
        }
        throw new IllegalArgumentException("Tracing config must be non null");
    }
    
    @Override
    public boolean stop(final OutputStream outputStream, final Executor executor) {
        final ApiFeature.P tracing_CONTROLLER_BASIC_USAGE = WebViewFeatureInternal.TRACING_CONTROLLER_BASIC_USAGE;
        if (tracing_CONTROLLER_BASIC_USAGE.isSupportedByFramework()) {
            return ApiHelperForP.stop(this.getFrameworksImpl(), outputStream, executor);
        }
        if (tracing_CONTROLLER_BASIC_USAGE.isSupportedByWebView()) {
            return this.getBoundaryInterface().stop(outputStream, executor);
        }
        throw WebViewFeatureInternal.getUnsupportedOperationException();
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.webkit.internal;

import android.webkit.WebResourceResponse;
import android.webkit.WebResourceRequest;
import androidx.webkit.ServiceWorkerClientCompat;
import org.chromium.support_lib_boundary.ServiceWorkerClientBoundaryInterface;

public class ServiceWorkerClientAdapter implements ServiceWorkerClientBoundaryInterface
{
    private final ServiceWorkerClientCompat mClient;
    
    public ServiceWorkerClientAdapter(final ServiceWorkerClientCompat mClient) {
        this.mClient = mClient;
    }
    
    public String[] getSupportedFeatures() {
        return new String[] { "SERVICE_WORKER_SHOULD_INTERCEPT_REQUEST" };
    }
    
    public WebResourceResponse shouldInterceptRequest(final WebResourceRequest webResourceRequest) {
        return this.mClient.shouldInterceptRequest(webResourceRequest);
    }
}

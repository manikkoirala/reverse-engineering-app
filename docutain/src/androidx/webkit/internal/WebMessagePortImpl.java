// 
// Decompiled by Procyon v0.6.0
// 

package androidx.webkit.internal;

import android.os.Handler;
import java.lang.reflect.Proxy;
import android.webkit.WebMessage;
import androidx.webkit.WebMessageCompat;
import org.chromium.support_lib_boundary.util.BoundaryInterfaceReflectionUtil;
import java.lang.reflect.InvocationHandler;
import android.webkit.WebMessagePort;
import org.chromium.support_lib_boundary.WebMessagePortBoundaryInterface;
import androidx.webkit.WebMessagePortCompat;

public class WebMessagePortImpl extends WebMessagePortCompat
{
    private WebMessagePortBoundaryInterface mBoundaryInterface;
    private WebMessagePort mFrameworksImpl;
    
    public WebMessagePortImpl(final WebMessagePort mFrameworksImpl) {
        this.mFrameworksImpl = mFrameworksImpl;
    }
    
    public WebMessagePortImpl(final InvocationHandler invocationHandler) {
        this.mBoundaryInterface = (WebMessagePortBoundaryInterface)BoundaryInterfaceReflectionUtil.castToSuppLibClass((Class)WebMessagePortBoundaryInterface.class, invocationHandler);
    }
    
    public static WebMessage compatToFrameworkMessage(final WebMessageCompat webMessageCompat) {
        return ApiHelperForM.createWebMessage(webMessageCompat);
    }
    
    public static WebMessagePort[] compatToPorts(final WebMessagePortCompat[] array) {
        if (array == null) {
            return null;
        }
        final int length = array.length;
        final WebMessagePort[] array2 = new WebMessagePort[length];
        for (int i = 0; i < length; ++i) {
            array2[i] = array[i].getFrameworkPort();
        }
        return array2;
    }
    
    public static WebMessageCompat frameworkMessageToCompat(final WebMessage webMessage) {
        return ApiHelperForM.createWebMessageCompat(webMessage);
    }
    
    private WebMessagePortBoundaryInterface getBoundaryInterface() {
        if (this.mBoundaryInterface == null) {
            this.mBoundaryInterface = (WebMessagePortBoundaryInterface)BoundaryInterfaceReflectionUtil.castToSuppLibClass((Class)WebMessagePortBoundaryInterface.class, WebViewGlueCommunicator.getCompatConverter().convertWebMessagePort(this.mFrameworksImpl));
        }
        return this.mBoundaryInterface;
    }
    
    private WebMessagePort getFrameworksImpl() {
        if (this.mFrameworksImpl == null) {
            this.mFrameworksImpl = WebViewGlueCommunicator.getCompatConverter().convertWebMessagePort(Proxy.getInvocationHandler(this.mBoundaryInterface));
        }
        return this.mFrameworksImpl;
    }
    
    public static WebMessagePortCompat[] portsToCompat(final WebMessagePort[] array) {
        if (array == null) {
            return null;
        }
        final WebMessagePortCompat[] array2 = new WebMessagePortCompat[array.length];
        for (int i = 0; i < array.length; ++i) {
            array2[i] = new WebMessagePortImpl(array[i]);
        }
        return array2;
    }
    
    @Override
    public void close() {
        final ApiFeature.M web_MESSAGE_PORT_CLOSE = WebViewFeatureInternal.WEB_MESSAGE_PORT_CLOSE;
        if (web_MESSAGE_PORT_CLOSE.isSupportedByFramework()) {
            ApiHelperForM.close(this.getFrameworksImpl());
        }
        else {
            if (!web_MESSAGE_PORT_CLOSE.isSupportedByWebView()) {
                throw WebViewFeatureInternal.getUnsupportedOperationException();
            }
            this.getBoundaryInterface().close();
        }
    }
    
    @Override
    public WebMessagePort getFrameworkPort() {
        return this.getFrameworksImpl();
    }
    
    @Override
    public InvocationHandler getInvocationHandler() {
        return Proxy.getInvocationHandler(this.getBoundaryInterface());
    }
    
    @Override
    public void postMessage(final WebMessageCompat webMessageCompat) {
        final ApiFeature.M web_MESSAGE_PORT_POST_MESSAGE = WebViewFeatureInternal.WEB_MESSAGE_PORT_POST_MESSAGE;
        if (web_MESSAGE_PORT_POST_MESSAGE.isSupportedByFramework() && webMessageCompat.getType() == 0) {
            ApiHelperForM.postMessage(this.getFrameworksImpl(), compatToFrameworkMessage(webMessageCompat));
        }
        else {
            if (!web_MESSAGE_PORT_POST_MESSAGE.isSupportedByWebView() || !WebMessageAdapter.isMessagePayloadTypeSupportedByWebView(webMessageCompat.getType())) {
                throw WebViewFeatureInternal.getUnsupportedOperationException();
            }
            this.getBoundaryInterface().postMessage(BoundaryInterfaceReflectionUtil.createInvocationHandlerFor((Object)new WebMessageAdapter(webMessageCompat)));
        }
    }
    
    @Override
    public void setWebMessageCallback(final Handler handler, final WebMessageCallbackCompat webMessageCallbackCompat) {
        final ApiFeature.M create_WEB_MESSAGE_CHANNEL = WebViewFeatureInternal.CREATE_WEB_MESSAGE_CHANNEL;
        if (create_WEB_MESSAGE_CHANNEL.isSupportedByWebView()) {
            this.getBoundaryInterface().setWebMessageCallback(BoundaryInterfaceReflectionUtil.createInvocationHandlerFor((Object)new WebMessageCallbackAdapter(webMessageCallbackCompat)), handler);
        }
        else {
            if (!create_WEB_MESSAGE_CHANNEL.isSupportedByFramework()) {
                throw WebViewFeatureInternal.getUnsupportedOperationException();
            }
            ApiHelperForM.setWebMessageCallback(this.getFrameworksImpl(), webMessageCallbackCompat, handler);
        }
    }
    
    @Override
    public void setWebMessageCallback(final WebMessageCallbackCompat webMessageCallbackCompat) {
        final ApiFeature.M web_MESSAGE_PORT_SET_MESSAGE_CALLBACK = WebViewFeatureInternal.WEB_MESSAGE_PORT_SET_MESSAGE_CALLBACK;
        if (web_MESSAGE_PORT_SET_MESSAGE_CALLBACK.isSupportedByWebView()) {
            this.getBoundaryInterface().setWebMessageCallback(BoundaryInterfaceReflectionUtil.createInvocationHandlerFor((Object)new WebMessageCallbackAdapter(webMessageCallbackCompat)));
        }
        else {
            if (!web_MESSAGE_PORT_SET_MESSAGE_CALLBACK.isSupportedByFramework()) {
                throw WebViewFeatureInternal.getUnsupportedOperationException();
            }
            ApiHelperForM.setWebMessageCallback(this.getFrameworksImpl(), webMessageCallbackCompat);
        }
    }
}

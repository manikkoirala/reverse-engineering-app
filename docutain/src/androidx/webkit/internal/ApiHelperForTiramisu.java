// 
// Decompiled by Procyon v0.6.0
// 

package androidx.webkit.internal;

import android.content.pm.PackageManager$NameNotFoundException;
import android.content.pm.ServiceInfo;
import android.content.pm.PackageManager$ComponentInfoFlags;
import android.content.ComponentName;
import android.content.pm.PackageManager;

public class ApiHelperForTiramisu
{
    private ApiHelperForTiramisu() {
    }
    
    static ServiceInfo getServiceInfo(final PackageManager packageManager, final ComponentName componentName, final PackageManager$ComponentInfoFlags packageManager$ComponentInfoFlags) throws PackageManager$NameNotFoundException {
        return packageManager.getServiceInfo(componentName, packageManager$ComponentInfoFlags);
    }
    
    static PackageManager$ComponentInfoFlags of(final long n) {
        return PackageManager$ComponentInfoFlags.of(n);
    }
}

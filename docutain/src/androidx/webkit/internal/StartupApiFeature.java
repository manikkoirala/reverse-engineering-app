// 
// Decompiled by Procyon v0.6.0
// 

package androidx.webkit.internal;

import java.util.Collections;
import android.content.pm.ServiceInfo;
import android.content.pm.PackageManager$ComponentInfoFlags;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager$NameNotFoundException;
import android.os.Build$VERSION;
import android.content.ComponentName;
import androidx.webkit.WebViewCompat;
import android.os.Bundle;
import android.content.Context;
import java.util.HashSet;
import java.util.Set;

public abstract class StartupApiFeature
{
    static final boolean $assertionsDisabled = false;
    public static final String METADATA_HOLDER_SERVICE_NAME = "org.chromium.android_webview.services.StartupFeatureMetadataHolder";
    private static final Set<StartupApiFeature> sValues;
    private final String mInternalFeatureValue;
    private final String mPublicFeatureValue;
    
    static {
        sValues = new HashSet<StartupApiFeature>();
    }
    
    StartupApiFeature(final String mPublicFeatureValue, final String mInternalFeatureValue) {
        this.mPublicFeatureValue = mPublicFeatureValue;
        this.mInternalFeatureValue = mInternalFeatureValue;
        StartupApiFeature.sValues.add(this);
    }
    
    private static Bundle getMetaDataFromWebViewManifestOrNull(final Context context) {
        final PackageInfo currentWebViewPackage = WebViewCompat.getCurrentWebViewPackage(context);
        if (currentWebViewPackage == null) {
            return null;
        }
        final ComponentName componentName = new ComponentName(currentWebViewPackage.packageName, "org.chromium.android_webview.services.StartupFeatureMetadataHolder");
        if (Build$VERSION.SDK_INT >= 33) {
            final PackageManager$ComponentInfoFlags of = ApiHelperForTiramisu.of(640L);
            try {
                return ApiHelperForTiramisu.getServiceInfo(context.getPackageManager(), componentName, of).metaData;
            }
            catch (final PackageManager$NameNotFoundException ex) {
                return null;
            }
        }
        int n = 128;
        if (Build$VERSION.SDK_INT >= 24) {
            n = 640;
        }
        try {
            return getServiceInfo(context, componentName, n).metaData;
        }
        catch (final PackageManager$NameNotFoundException ex2) {
            return null;
        }
    }
    
    private static ServiceInfo getServiceInfo(final Context context, final ComponentName componentName, final int n) throws PackageManager$NameNotFoundException {
        return context.getPackageManager().getServiceInfo(componentName, n);
    }
    
    public static Set<StartupApiFeature> values() {
        return Collections.unmodifiableSet((Set<? extends StartupApiFeature>)StartupApiFeature.sValues);
    }
    
    public String getPublicFeatureName() {
        return this.mPublicFeatureValue;
    }
    
    public boolean isSupported(final Context context) {
        return this.isSupportedByFramework() || this.isSupportedByWebView(context);
    }
    
    public abstract boolean isSupportedByFramework();
    
    public boolean isSupportedByWebView(final Context context) {
        final Bundle metaDataFromWebViewManifestOrNull = getMetaDataFromWebViewManifestOrNull(context);
        return metaDataFromWebViewManifestOrNull != null && metaDataFromWebViewManifestOrNull.containsKey(this.mInternalFeatureValue);
    }
    
    public static class NoFramework extends StartupApiFeature
    {
        NoFramework(final String s, final String s2) {
            super(s, s2);
        }
        
        @Override
        public final boolean isSupportedByFramework() {
            return false;
        }
    }
    
    public static class P extends StartupApiFeature
    {
        P(final String s, final String s2) {
            super(s, s2);
        }
        
        @Override
        public final boolean isSupportedByFramework() {
            return Build$VERSION.SDK_INT >= 28;
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.webkit.internal;

public interface ConditionallySupportedFeature
{
    String getPublicFeatureName();
    
    boolean isSupported();
}

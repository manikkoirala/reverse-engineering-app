// 
// Decompiled by Procyon v0.6.0
// 

package androidx.webkit.internal;

import androidx.webkit.WebMessageCompat;
import androidx.webkit.JavaScriptReplyProxy;
import org.chromium.support_lib_boundary.util.BoundaryInterfaceReflectionUtil;
import org.chromium.support_lib_boundary.WebMessageBoundaryInterface;
import android.net.Uri;
import java.lang.reflect.InvocationHandler;
import android.webkit.WebView;
import androidx.webkit.WebViewCompat;
import org.chromium.support_lib_boundary.WebMessageListenerBoundaryInterface;

public class WebMessageListenerAdapter implements WebMessageListenerBoundaryInterface
{
    private WebViewCompat.WebMessageListener mWebMessageListener;
    
    public WebMessageListenerAdapter(final WebViewCompat.WebMessageListener mWebMessageListener) {
        this.mWebMessageListener = mWebMessageListener;
    }
    
    public String[] getSupportedFeatures() {
        return new String[] { "WEB_MESSAGE_LISTENER" };
    }
    
    public void onPostMessage(final WebView webView, final InvocationHandler invocationHandler, final Uri uri, final boolean b, final InvocationHandler invocationHandler2) {
        final WebMessageCompat webMessageCompatFromBoundaryInterface = WebMessageAdapter.webMessageCompatFromBoundaryInterface((WebMessageBoundaryInterface)BoundaryInterfaceReflectionUtil.castToSuppLibClass((Class)WebMessageBoundaryInterface.class, invocationHandler));
        if (webMessageCompatFromBoundaryInterface != null) {
            this.mWebMessageListener.onPostMessage(webView, webMessageCompatFromBoundaryInterface, uri, b, JavaScriptReplyProxyImpl.forInvocationHandler(invocationHandler2));
        }
    }
}

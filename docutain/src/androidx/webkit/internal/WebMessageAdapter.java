// 
// Decompiled by Procyon v0.6.0
// 

package androidx.webkit.internal;

import org.chromium.support_lib_boundary.util.BoundaryInterfaceReflectionUtil;
import org.chromium.support_lib_boundary.WebMessagePayloadBoundaryInterface;
import androidx.webkit.WebMessagePortCompat;
import java.lang.reflect.InvocationHandler;
import androidx.webkit.WebMessageCompat;
import org.chromium.support_lib_boundary.WebMessageBoundaryInterface;

public class WebMessageAdapter implements WebMessageBoundaryInterface
{
    private static final String[] sFeatures;
    private WebMessageCompat mWebMessageCompat;
    
    static {
        sFeatures = new String[] { "WEB_MESSAGE_GET_MESSAGE_PAYLOAD" };
    }
    
    public WebMessageAdapter(final WebMessageCompat mWebMessageCompat) {
        this.mWebMessageCompat = mWebMessageCompat;
    }
    
    public static boolean isMessagePayloadTypeSupportedByWebView(final int n) {
        boolean b = true;
        if (n != 0) {
            b = (n == 1 && WebViewFeatureInternal.WEB_MESSAGE_GET_MESSAGE_PAYLOAD.isSupportedByWebView() && b);
        }
        return b;
    }
    
    private static WebMessagePortCompat[] toWebMessagePortCompats(final InvocationHandler[] array) {
        final WebMessagePortCompat[] array2 = new WebMessagePortCompat[array.length];
        for (int i = 0; i < array.length; ++i) {
            array2[i] = new WebMessagePortImpl(array[i]);
        }
        return array2;
    }
    
    public static WebMessageCompat webMessageCompatFromBoundaryInterface(final WebMessageBoundaryInterface webMessageBoundaryInterface) {
        final WebMessagePortCompat[] webMessagePortCompats = toWebMessagePortCompats(webMessageBoundaryInterface.getPorts());
        if (!WebViewFeatureInternal.WEB_MESSAGE_GET_MESSAGE_PAYLOAD.isSupportedByWebView()) {
            return new WebMessageCompat(webMessageBoundaryInterface.getData(), webMessagePortCompats);
        }
        final WebMessagePayloadBoundaryInterface webMessagePayloadBoundaryInterface = (WebMessagePayloadBoundaryInterface)BoundaryInterfaceReflectionUtil.castToSuppLibClass((Class)WebMessagePayloadBoundaryInterface.class, webMessageBoundaryInterface.getMessagePayload());
        final int type = webMessagePayloadBoundaryInterface.getType();
        if (type == 0) {
            return new WebMessageCompat(webMessagePayloadBoundaryInterface.getAsString(), webMessagePortCompats);
        }
        if (type != 1) {
            return null;
        }
        return new WebMessageCompat(webMessagePayloadBoundaryInterface.getAsArrayBuffer(), webMessagePortCompats);
    }
    
    @Deprecated
    public String getData() {
        return this.mWebMessageCompat.getData();
    }
    
    public InvocationHandler getMessagePayload() {
        return BoundaryInterfaceReflectionUtil.createInvocationHandlerFor((Object)new WebMessagePayloadAdapter(this.mWebMessageCompat));
    }
    
    public InvocationHandler[] getPorts() {
        final WebMessagePortCompat[] ports = this.mWebMessageCompat.getPorts();
        if (ports == null) {
            return null;
        }
        final InvocationHandler[] array = new InvocationHandler[ports.length];
        for (int i = 0; i < ports.length; ++i) {
            array[i] = ports[i].getInvocationHandler();
        }
        return array;
    }
    
    public String[] getSupportedFeatures() {
        return WebMessageAdapter.sFeatures;
    }
}

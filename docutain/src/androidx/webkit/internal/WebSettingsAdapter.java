// 
// Decompiled by Procyon v0.6.0
// 

package androidx.webkit.internal;

import java.util.Set;
import org.chromium.support_lib_boundary.WebSettingsBoundaryInterface;

public class WebSettingsAdapter
{
    private final WebSettingsBoundaryInterface mBoundaryInterface;
    
    public WebSettingsAdapter(final WebSettingsBoundaryInterface mBoundaryInterface) {
        this.mBoundaryInterface = mBoundaryInterface;
    }
    
    public int getDisabledActionModeMenuItems() {
        return this.mBoundaryInterface.getDisabledActionModeMenuItems();
    }
    
    public boolean getEnterpriseAuthenticationAppLinkPolicyEnabled() {
        return this.mBoundaryInterface.getEnterpriseAuthenticationAppLinkPolicyEnabled();
    }
    
    public int getForceDark() {
        return this.mBoundaryInterface.getForceDark();
    }
    
    public int getForceDarkStrategy() {
        return this.mBoundaryInterface.getForceDarkBehavior();
    }
    
    public boolean getOffscreenPreRaster() {
        return this.mBoundaryInterface.getOffscreenPreRaster();
    }
    
    public Set<String> getRequestedWithHeaderOriginAllowList() {
        return this.mBoundaryInterface.getRequestedWithHeaderOriginAllowList();
    }
    
    public boolean getSafeBrowsingEnabled() {
        return this.mBoundaryInterface.getSafeBrowsingEnabled();
    }
    
    public boolean isAlgorithmicDarkeningAllowed() {
        return this.mBoundaryInterface.isAlgorithmicDarkeningAllowed();
    }
    
    public void setAlgorithmicDarkeningAllowed(final boolean algorithmicDarkeningAllowed) {
        this.mBoundaryInterface.setAlgorithmicDarkeningAllowed(algorithmicDarkeningAllowed);
    }
    
    public void setDisabledActionModeMenuItems(final int disabledActionModeMenuItems) {
        this.mBoundaryInterface.setDisabledActionModeMenuItems(disabledActionModeMenuItems);
    }
    
    public void setEnterpriseAuthenticationAppLinkPolicyEnabled(final boolean enterpriseAuthenticationAppLinkPolicyEnabled) {
        this.mBoundaryInterface.setEnterpriseAuthenticationAppLinkPolicyEnabled(enterpriseAuthenticationAppLinkPolicyEnabled);
    }
    
    public void setForceDark(final int forceDark) {
        this.mBoundaryInterface.setForceDark(forceDark);
    }
    
    public void setForceDarkStrategy(final int forceDarkBehavior) {
        this.mBoundaryInterface.setForceDarkBehavior(forceDarkBehavior);
    }
    
    public void setOffscreenPreRaster(final boolean offscreenPreRaster) {
        this.mBoundaryInterface.setOffscreenPreRaster(offscreenPreRaster);
    }
    
    public void setRequestedWithHeaderOriginAllowList(final Set<String> requestedWithHeaderOriginAllowList) {
        this.mBoundaryInterface.setRequestedWithHeaderOriginAllowList((Set)requestedWithHeaderOriginAllowList);
    }
    
    public void setSafeBrowsingEnabled(final boolean safeBrowsingEnabled) {
        this.mBoundaryInterface.setSafeBrowsingEnabled(safeBrowsingEnabled);
    }
    
    public void setWillSuppressErrorPage(final boolean willSuppressErrorPage) {
        this.mBoundaryInterface.setWillSuppressErrorPage(willSuppressErrorPage);
    }
    
    public boolean willSuppressErrorPage() {
        return this.mBoundaryInterface.getWillSuppressErrorPage();
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.webkit.internal;

import android.content.res.Resources$NotFoundException;
import java.io.FileNotFoundException;
import java.io.FileInputStream;
import java.util.zip.GZIPInputStream;
import java.io.InputStream;
import java.net.URLConnection;
import android.util.TypedValue;
import android.os.Build$VERSION;
import java.io.IOException;
import java.io.File;
import android.content.Context;

public class AssetHelper
{
    public static final String DEFAULT_MIME_TYPE = "text/plain";
    private Context mContext;
    
    public AssetHelper(final Context mContext) {
        this.mContext = mContext;
    }
    
    public static String getCanonicalDirPath(final File file) throws IOException {
        String s;
        final String str = s = file.getCanonicalPath();
        if (!str.endsWith("/")) {
            final StringBuilder sb = new StringBuilder();
            sb.append(str);
            sb.append("/");
            s = sb.toString();
        }
        return s;
    }
    
    public static File getCanonicalFileIfChild(final File parent, final String child) throws IOException {
        final String canonicalDirPath = getCanonicalDirPath(parent);
        final String canonicalPath = new File(parent, child).getCanonicalPath();
        if (canonicalPath.startsWith(canonicalDirPath)) {
            return new File(canonicalPath);
        }
        return null;
    }
    
    public static File getDataDir(final Context context) {
        if (Build$VERSION.SDK_INT >= 24) {
            return ApiHelperForN.getDataDir(context);
        }
        return context.getCacheDir().getParentFile();
    }
    
    private int getFieldId(final String s, final String s2) {
        return this.mContext.getResources().getIdentifier(s2, s, this.mContext.getPackageName());
    }
    
    private int getValueType(final int n) {
        final TypedValue typedValue = new TypedValue();
        this.mContext.getResources().getValue(n, typedValue, true);
        return typedValue.type;
    }
    
    public static String guessMimeType(String guessContentTypeFromName) {
        if ((guessContentTypeFromName = URLConnection.guessContentTypeFromName(guessContentTypeFromName)) == null) {
            guessContentTypeFromName = "text/plain";
        }
        return guessContentTypeFromName;
    }
    
    private static InputStream handleSvgzStream(final String s, final InputStream in) throws IOException {
        InputStream inputStream = in;
        if (s.endsWith(".svgz")) {
            inputStream = new GZIPInputStream(in);
        }
        return inputStream;
    }
    
    public static InputStream openFile(final File file) throws FileNotFoundException, IOException {
        return handleSvgzStream(file.getPath(), new FileInputStream(file));
    }
    
    private static String removeLeadingSlash(final String s) {
        String substring = s;
        if (s.length() > 1) {
            substring = s;
            if (s.charAt(0) == '/') {
                substring = s.substring(1);
            }
        }
        return substring;
    }
    
    public InputStream openAsset(String removeLeadingSlash) throws IOException {
        removeLeadingSlash = removeLeadingSlash(removeLeadingSlash);
        return handleSvgzStream(removeLeadingSlash, this.mContext.getAssets().open(removeLeadingSlash, 2));
    }
    
    public InputStream openResource(String substring) throws Resources$NotFoundException, IOException {
        final String removeLeadingSlash = removeLeadingSlash(substring);
        final String[] split = removeLeadingSlash.split("/", -1);
        if (split.length != 2) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Incorrect resource path: ");
            sb.append(removeLeadingSlash);
            throw new IllegalArgumentException(sb.toString());
        }
        final String s = split[0];
        final String s2 = split[1];
        final int lastIndex = s2.lastIndexOf(46);
        substring = s2;
        if (lastIndex != -1) {
            substring = s2.substring(0, lastIndex);
        }
        final int fieldId = this.getFieldId(s, substring);
        final int valueType = this.getValueType(fieldId);
        if (valueType == 3) {
            return handleSvgzStream(removeLeadingSlash, this.mContext.getResources().openRawResource(fieldId));
        }
        throw new IOException(String.format("Expected %s resource to be of TYPE_STRING but was %d", removeLeadingSlash, valueType));
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.webkit.internal;

import java.util.Iterator;
import java.util.HashSet;
import java.util.Collection;
import android.content.Context;
import java.util.regex.Matcher;
import android.content.pm.PackageInfo;
import androidx.webkit.WebViewCompat;
import android.os.Build$VERSION;
import java.util.regex.Pattern;

public class WebViewFeatureInternal
{
    public static final ApiFeature.T ALGORITHMIC_DARKENING;
    public static final ApiFeature.M CREATE_WEB_MESSAGE_CHANNEL;
    public static final ApiFeature.N DISABLED_ACTION_MODE_MENU_ITEMS;
    public static final ApiFeature.NoFramework DOCUMENT_START_SCRIPT;
    public static final ApiFeature.NoFramework ENTERPRISE_AUTHENTICATION_APP_LINK_POLICY;
    public static final ApiFeature.Q FORCE_DARK;
    public static final ApiFeature.NoFramework FORCE_DARK_STRATEGY;
    public static final ApiFeature.NoFramework GET_COOKIE_INFO;
    public static final ApiFeature.NoFramework GET_VARIATIONS_HEADER;
    public static final ApiFeature.O GET_WEB_CHROME_CLIENT;
    public static final ApiFeature.O GET_WEB_VIEW_CLIENT;
    public static final ApiFeature.Q GET_WEB_VIEW_RENDERER;
    public static final ApiFeature.NoFramework MULTI_PROCESS;
    public static final ApiFeature.M OFF_SCREEN_PRERASTER;
    public static final ApiFeature.M POST_WEB_MESSAGE;
    public static final ApiFeature.NoFramework PROXY_OVERRIDE;
    public static final ApiFeature.NoFramework PROXY_OVERRIDE_REVERSE_BYPASS;
    public static final ApiFeature.M RECEIVE_HTTP_ERROR;
    public static final ApiFeature.M RECEIVE_WEB_RESOURCE_ERROR;
    public static final ApiFeature.NoFramework REQUESTED_WITH_HEADER_ALLOW_LIST;
    @Deprecated
    public static final ApiFeature.O_MR1 SAFE_BROWSING_ALLOWLIST_DEPRECATED_TO_DEPRECATED;
    @Deprecated
    public static final ApiFeature.O_MR1 SAFE_BROWSING_ALLOWLIST_DEPRECATED_TO_PREFERRED;
    public static final ApiFeature.O_MR1 SAFE_BROWSING_ALLOWLIST_PREFERRED_TO_DEPRECATED;
    public static final ApiFeature.O_MR1 SAFE_BROWSING_ALLOWLIST_PREFERRED_TO_PREFERRED;
    public static final ApiFeature.O SAFE_BROWSING_ENABLE;
    public static final ApiFeature.O_MR1 SAFE_BROWSING_HIT;
    public static final ApiFeature.O_MR1 SAFE_BROWSING_PRIVACY_POLICY_URL;
    public static final ApiFeature.O_MR1 SAFE_BROWSING_RESPONSE_BACK_TO_SAFETY;
    public static final ApiFeature.O_MR1 SAFE_BROWSING_RESPONSE_PROCEED;
    public static final ApiFeature.O_MR1 SAFE_BROWSING_RESPONSE_SHOW_INTERSTITIAL;
    public static final ApiFeature.N SERVICE_WORKER_BASIC_USAGE;
    public static final ApiFeature.N SERVICE_WORKER_BLOCK_NETWORK_LOADS;
    public static final ApiFeature.N SERVICE_WORKER_CACHE_MODE;
    public static final ApiFeature.N SERVICE_WORKER_CONTENT_ACCESS;
    public static final ApiFeature.N SERVICE_WORKER_FILE_ACCESS;
    public static final ApiFeature.N SERVICE_WORKER_SHOULD_INTERCEPT_REQUEST;
    public static final ApiFeature.N SHOULD_OVERRIDE_WITH_REDIRECTS;
    public static final StartupApiFeature.P STARTUP_FEATURE_SET_DATA_DIRECTORY_SUFFIX;
    public static final StartupApiFeature.NoFramework STARTUP_FEATURE_SET_DIRECTORY_BASE_PATH;
    public static final ApiFeature.O_MR1 START_SAFE_BROWSING;
    public static final ApiFeature.NoFramework SUPPRESS_ERROR_PAGE;
    public static final ApiFeature.P TRACING_CONTROLLER_BASIC_USAGE;
    public static final ApiFeature.M VISUAL_STATE_CALLBACK;
    public static final ApiFeature.M WEB_MESSAGE_CALLBACK_ON_MESSAGE;
    public static final ApiFeature.NoFramework WEB_MESSAGE_GET_MESSAGE_PAYLOAD;
    public static final ApiFeature.NoFramework WEB_MESSAGE_LISTENER;
    public static final ApiFeature.M WEB_MESSAGE_PORT_CLOSE;
    public static final ApiFeature.M WEB_MESSAGE_PORT_POST_MESSAGE;
    public static final ApiFeature.M WEB_MESSAGE_PORT_SET_MESSAGE_CALLBACK;
    public static final ApiFeature.M WEB_RESOURCE_ERROR_GET_CODE;
    public static final ApiFeature.M WEB_RESOURCE_ERROR_GET_DESCRIPTION;
    public static final ApiFeature.N WEB_RESOURCE_REQUEST_IS_REDIRECT;
    public static final ApiFeature.Q WEB_VIEW_RENDERER_CLIENT_BASIC_USAGE;
    public static final ApiFeature.Q WEB_VIEW_RENDERER_TERMINATE;
    
    static {
        VISUAL_STATE_CALLBACK = new ApiFeature.M("VISUAL_STATE_CALLBACK", "VISUAL_STATE_CALLBACK");
        OFF_SCREEN_PRERASTER = new ApiFeature.M("OFF_SCREEN_PRERASTER", "OFF_SCREEN_PRERASTER");
        SAFE_BROWSING_ENABLE = new ApiFeature.O("SAFE_BROWSING_ENABLE", "SAFE_BROWSING_ENABLE");
        DISABLED_ACTION_MODE_MENU_ITEMS = new ApiFeature.N("DISABLED_ACTION_MODE_MENU_ITEMS", "DISABLED_ACTION_MODE_MENU_ITEMS");
        START_SAFE_BROWSING = new ApiFeature.O_MR1("START_SAFE_BROWSING", "START_SAFE_BROWSING");
        SAFE_BROWSING_ALLOWLIST_DEPRECATED_TO_DEPRECATED = new ApiFeature.O_MR1("SAFE_BROWSING_WHITELIST", "SAFE_BROWSING_WHITELIST");
        SAFE_BROWSING_ALLOWLIST_DEPRECATED_TO_PREFERRED = new ApiFeature.O_MR1("SAFE_BROWSING_WHITELIST", "SAFE_BROWSING_ALLOWLIST");
        SAFE_BROWSING_ALLOWLIST_PREFERRED_TO_DEPRECATED = new ApiFeature.O_MR1("SAFE_BROWSING_ALLOWLIST", "SAFE_BROWSING_WHITELIST");
        SAFE_BROWSING_ALLOWLIST_PREFERRED_TO_PREFERRED = new ApiFeature.O_MR1("SAFE_BROWSING_ALLOWLIST", "SAFE_BROWSING_ALLOWLIST");
        SAFE_BROWSING_PRIVACY_POLICY_URL = new ApiFeature.O_MR1("SAFE_BROWSING_PRIVACY_POLICY_URL", "SAFE_BROWSING_PRIVACY_POLICY_URL");
        SERVICE_WORKER_BASIC_USAGE = new ApiFeature.N("SERVICE_WORKER_BASIC_USAGE", "SERVICE_WORKER_BASIC_USAGE");
        SERVICE_WORKER_CACHE_MODE = new ApiFeature.N("SERVICE_WORKER_CACHE_MODE", "SERVICE_WORKER_CACHE_MODE");
        SERVICE_WORKER_CONTENT_ACCESS = new ApiFeature.N("SERVICE_WORKER_CONTENT_ACCESS", "SERVICE_WORKER_CONTENT_ACCESS");
        SERVICE_WORKER_FILE_ACCESS = new ApiFeature.N("SERVICE_WORKER_FILE_ACCESS", "SERVICE_WORKER_FILE_ACCESS");
        SERVICE_WORKER_BLOCK_NETWORK_LOADS = new ApiFeature.N("SERVICE_WORKER_BLOCK_NETWORK_LOADS", "SERVICE_WORKER_BLOCK_NETWORK_LOADS");
        SERVICE_WORKER_SHOULD_INTERCEPT_REQUEST = new ApiFeature.N("SERVICE_WORKER_SHOULD_INTERCEPT_REQUEST", "SERVICE_WORKER_SHOULD_INTERCEPT_REQUEST");
        RECEIVE_WEB_RESOURCE_ERROR = new ApiFeature.M("RECEIVE_WEB_RESOURCE_ERROR", "RECEIVE_WEB_RESOURCE_ERROR");
        RECEIVE_HTTP_ERROR = new ApiFeature.M("RECEIVE_HTTP_ERROR", "RECEIVE_HTTP_ERROR");
        SHOULD_OVERRIDE_WITH_REDIRECTS = new ApiFeature.N("SHOULD_OVERRIDE_WITH_REDIRECTS", "SHOULD_OVERRIDE_WITH_REDIRECTS");
        SAFE_BROWSING_HIT = new ApiFeature.O_MR1("SAFE_BROWSING_HIT", "SAFE_BROWSING_HIT");
        WEB_RESOURCE_REQUEST_IS_REDIRECT = new ApiFeature.N("WEB_RESOURCE_REQUEST_IS_REDIRECT", "WEB_RESOURCE_REQUEST_IS_REDIRECT");
        WEB_RESOURCE_ERROR_GET_DESCRIPTION = new ApiFeature.M("WEB_RESOURCE_ERROR_GET_DESCRIPTION", "WEB_RESOURCE_ERROR_GET_DESCRIPTION");
        WEB_RESOURCE_ERROR_GET_CODE = new ApiFeature.M("WEB_RESOURCE_ERROR_GET_CODE", "WEB_RESOURCE_ERROR_GET_CODE");
        SAFE_BROWSING_RESPONSE_BACK_TO_SAFETY = new ApiFeature.O_MR1("SAFE_BROWSING_RESPONSE_BACK_TO_SAFETY", "SAFE_BROWSING_RESPONSE_BACK_TO_SAFETY");
        SAFE_BROWSING_RESPONSE_PROCEED = new ApiFeature.O_MR1("SAFE_BROWSING_RESPONSE_PROCEED", "SAFE_BROWSING_RESPONSE_PROCEED");
        SAFE_BROWSING_RESPONSE_SHOW_INTERSTITIAL = new ApiFeature.O_MR1("SAFE_BROWSING_RESPONSE_SHOW_INTERSTITIAL", "SAFE_BROWSING_RESPONSE_SHOW_INTERSTITIAL");
        WEB_MESSAGE_PORT_POST_MESSAGE = new ApiFeature.M("WEB_MESSAGE_PORT_POST_MESSAGE", "WEB_MESSAGE_PORT_POST_MESSAGE");
        WEB_MESSAGE_PORT_CLOSE = new ApiFeature.M("WEB_MESSAGE_PORT_CLOSE", "WEB_MESSAGE_PORT_CLOSE");
        WEB_MESSAGE_GET_MESSAGE_PAYLOAD = new ApiFeature.NoFramework("WEB_MESSAGE_GET_MESSAGE_PAYLOAD", "WEB_MESSAGE_GET_MESSAGE_PAYLOAD");
        WEB_MESSAGE_PORT_SET_MESSAGE_CALLBACK = new ApiFeature.M("WEB_MESSAGE_PORT_SET_MESSAGE_CALLBACK", "WEB_MESSAGE_PORT_SET_MESSAGE_CALLBACK");
        CREATE_WEB_MESSAGE_CHANNEL = new ApiFeature.M("CREATE_WEB_MESSAGE_CHANNEL", "CREATE_WEB_MESSAGE_CHANNEL");
        POST_WEB_MESSAGE = new ApiFeature.M("POST_WEB_MESSAGE", "POST_WEB_MESSAGE");
        WEB_MESSAGE_CALLBACK_ON_MESSAGE = new ApiFeature.M("WEB_MESSAGE_CALLBACK_ON_MESSAGE", "WEB_MESSAGE_CALLBACK_ON_MESSAGE");
        GET_WEB_VIEW_CLIENT = new ApiFeature.O("GET_WEB_VIEW_CLIENT", "GET_WEB_VIEW_CLIENT");
        GET_WEB_CHROME_CLIENT = new ApiFeature.O("GET_WEB_CHROME_CLIENT", "GET_WEB_CHROME_CLIENT");
        GET_WEB_VIEW_RENDERER = new ApiFeature.Q("GET_WEB_VIEW_RENDERER", "GET_WEB_VIEW_RENDERER");
        WEB_VIEW_RENDERER_TERMINATE = new ApiFeature.Q("WEB_VIEW_RENDERER_TERMINATE", "WEB_VIEW_RENDERER_TERMINATE");
        TRACING_CONTROLLER_BASIC_USAGE = new ApiFeature.P("TRACING_CONTROLLER_BASIC_USAGE", "TRACING_CONTROLLER_BASIC_USAGE");
        STARTUP_FEATURE_SET_DATA_DIRECTORY_SUFFIX = new StartupApiFeature.P("STARTUP_FEATURE_SET_DATA_DIRECTORY_SUFFIX", "STARTUP_FEATURE_SET_DATA_DIRECTORY_SUFFIX");
        STARTUP_FEATURE_SET_DIRECTORY_BASE_PATH = new StartupApiFeature.NoFramework("STARTUP_FEATURE_SET_DIRECTORY_BASE_PATHS", "STARTUP_FEATURE_SET_DIRECTORY_BASE_PATH");
        WEB_VIEW_RENDERER_CLIENT_BASIC_USAGE = new ApiFeature.Q("WEB_VIEW_RENDERER_CLIENT_BASIC_USAGE", "WEB_VIEW_RENDERER_CLIENT_BASIC_USAGE");
        ALGORITHMIC_DARKENING = new ApiFeature.T("ALGORITHMIC_DARKENING") {
            private final Pattern mVersionPattern = Pattern.compile("\\A\\d+");
            
            @Override
            public boolean isSupportedByWebView() {
                final boolean supportedByWebView = super.isSupportedByWebView();
                if (!supportedByWebView || Build$VERSION.SDK_INT >= 29) {
                    return supportedByWebView;
                }
                final PackageInfo currentLoadedWebViewPackage = WebViewCompat.getCurrentLoadedWebViewPackage();
                final boolean b = false;
                if (currentLoadedWebViewPackage == null) {
                    return false;
                }
                final Matcher matcher = this.mVersionPattern.matcher(currentLoadedWebViewPackage.versionName);
                boolean b2 = b;
                if (matcher.find()) {
                    b2 = b;
                    if (Integer.parseInt(currentLoadedWebViewPackage.versionName.substring(matcher.start(), matcher.end())) >= 105) {
                        b2 = true;
                    }
                }
                return b2;
            }
        };
        PROXY_OVERRIDE = new ApiFeature.NoFramework("PROXY_OVERRIDE", "PROXY_OVERRIDE:3");
        SUPPRESS_ERROR_PAGE = new ApiFeature.NoFramework("SUPPRESS_ERROR_PAGE", "SUPPRESS_ERROR_PAGE");
        MULTI_PROCESS = new ApiFeature.NoFramework("MULTI_PROCESS", "MULTI_PROCESS_QUERY");
        FORCE_DARK = new ApiFeature.Q("FORCE_DARK", "FORCE_DARK");
        FORCE_DARK_STRATEGY = new ApiFeature.NoFramework("FORCE_DARK_STRATEGY", "FORCE_DARK_BEHAVIOR");
        WEB_MESSAGE_LISTENER = new ApiFeature.NoFramework("WEB_MESSAGE_LISTENER", "WEB_MESSAGE_LISTENER");
        DOCUMENT_START_SCRIPT = new ApiFeature.NoFramework("DOCUMENT_START_SCRIPT", "DOCUMENT_START_SCRIPT:1");
        PROXY_OVERRIDE_REVERSE_BYPASS = new ApiFeature.NoFramework("PROXY_OVERRIDE_REVERSE_BYPASS", "PROXY_OVERRIDE_REVERSE_BYPASS");
        GET_VARIATIONS_HEADER = new ApiFeature.NoFramework("GET_VARIATIONS_HEADER", "GET_VARIATIONS_HEADER");
        ENTERPRISE_AUTHENTICATION_APP_LINK_POLICY = new ApiFeature.NoFramework("ENTERPRISE_AUTHENTICATION_APP_LINK_POLICY", "ENTERPRISE_AUTHENTICATION_APP_LINK_POLICY");
        GET_COOKIE_INFO = new ApiFeature.NoFramework("GET_COOKIE_INFO", "GET_COOKIE_INFO");
        REQUESTED_WITH_HEADER_ALLOW_LIST = new ApiFeature.NoFramework("REQUESTED_WITH_HEADER_ALLOW_LIST", "REQUESTED_WITH_HEADER_ALLOW_LIST");
    }
    
    private WebViewFeatureInternal() {
    }
    
    public static UnsupportedOperationException getUnsupportedOperationException() {
        return new UnsupportedOperationException("This method is not supported by the current version of the framework and the current WebView APK");
    }
    
    public static boolean isStartupFeatureSupported(final String s, final Context context) {
        return isStartupFeatureSupported(s, StartupApiFeature.values(), context);
    }
    
    public static boolean isStartupFeatureSupported(final String s, final Collection<StartupApiFeature> collection, final Context context) {
        final HashSet set = new HashSet();
        for (final StartupApiFeature startupApiFeature : collection) {
            if (startupApiFeature.getPublicFeatureName().equals(s)) {
                set.add(startupApiFeature);
            }
        }
        if (!set.isEmpty()) {
            final Iterator iterator2 = set.iterator();
            while (iterator2.hasNext()) {
                if (((StartupApiFeature)iterator2.next()).isSupported(context)) {
                    return true;
                }
            }
            return false;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Unknown feature ");
        sb.append(s);
        throw new RuntimeException(sb.toString());
    }
    
    public static boolean isSupported(final String s) {
        return isSupported(s, ApiFeature.values());
    }
    
    public static <T extends ConditionallySupportedFeature> boolean isSupported(final String s, final Collection<T> collection) {
        final HashSet set = new HashSet();
        for (final ConditionallySupportedFeature conditionallySupportedFeature : collection) {
            if (conditionallySupportedFeature.getPublicFeatureName().equals(s)) {
                set.add(conditionallySupportedFeature);
            }
        }
        if (!set.isEmpty()) {
            final Iterator iterator2 = set.iterator();
            while (iterator2.hasNext()) {
                if (((ConditionallySupportedFeature)iterator2.next()).isSupported()) {
                    return true;
                }
            }
            return false;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Unknown feature ");
        sb.append(s);
        throw new RuntimeException(sb.toString());
    }
}

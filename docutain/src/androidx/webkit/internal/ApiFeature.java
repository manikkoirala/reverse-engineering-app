// 
// Decompiled by Procyon v0.6.0
// 

package androidx.webkit.internal;

import android.os.Build$VERSION;
import java.util.Arrays;
import java.util.Collection;
import org.chromium.support_lib_boundary.util.BoundaryInterfaceReflectionUtil;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public abstract class ApiFeature implements ConditionallySupportedFeature
{
    private static final Set<ApiFeature> sValues;
    private final String mInternalFeatureValue;
    private final String mPublicFeatureValue;
    
    static {
        sValues = new HashSet<ApiFeature>();
    }
    
    ApiFeature(final String mPublicFeatureValue, final String mInternalFeatureValue) {
        this.mPublicFeatureValue = mPublicFeatureValue;
        this.mInternalFeatureValue = mInternalFeatureValue;
        ApiFeature.sValues.add(this);
    }
    
    public static Set<String> getWebViewApkFeaturesForTesting() {
        return LAZY_HOLDER.WEBVIEW_APK_FEATURES;
    }
    
    public static Set<ApiFeature> values() {
        return Collections.unmodifiableSet((Set<? extends ApiFeature>)ApiFeature.sValues);
    }
    
    @Override
    public String getPublicFeatureName() {
        return this.mPublicFeatureValue;
    }
    
    @Override
    public boolean isSupported() {
        return this.isSupportedByFramework() || this.isSupportedByWebView();
    }
    
    public abstract boolean isSupportedByFramework();
    
    public boolean isSupportedByWebView() {
        return BoundaryInterfaceReflectionUtil.containsFeature((Collection)LAZY_HOLDER.WEBVIEW_APK_FEATURES, this.mInternalFeatureValue);
    }
    
    private static class LAZY_HOLDER
    {
        static final Set<String> WEBVIEW_APK_FEATURES;
        
        static {
            WEBVIEW_APK_FEATURES = new HashSet<String>(Arrays.asList(WebViewGlueCommunicator.getFactory().getWebViewFeatures()));
        }
    }
    
    public static class M extends ApiFeature
    {
        M(final String s, final String s2) {
            super(s, s2);
        }
        
        @Override
        public final boolean isSupportedByFramework() {
            return Build$VERSION.SDK_INT >= 23;
        }
    }
    
    public static class N extends ApiFeature
    {
        N(final String s, final String s2) {
            super(s, s2);
        }
        
        @Override
        public final boolean isSupportedByFramework() {
            return Build$VERSION.SDK_INT >= 24;
        }
    }
    
    public static class NoFramework extends ApiFeature
    {
        NoFramework(final String s, final String s2) {
            super(s, s2);
        }
        
        @Override
        public final boolean isSupportedByFramework() {
            return false;
        }
    }
    
    public static class O extends ApiFeature
    {
        O(final String s, final String s2) {
            super(s, s2);
        }
        
        @Override
        public final boolean isSupportedByFramework() {
            return Build$VERSION.SDK_INT >= 26;
        }
    }
    
    public static class O_MR1 extends ApiFeature
    {
        O_MR1(final String s, final String s2) {
            super(s, s2);
        }
        
        @Override
        public final boolean isSupportedByFramework() {
            return Build$VERSION.SDK_INT >= 27;
        }
    }
    
    public static class P extends ApiFeature
    {
        P(final String s, final String s2) {
            super(s, s2);
        }
        
        @Override
        public final boolean isSupportedByFramework() {
            return Build$VERSION.SDK_INT >= 28;
        }
    }
    
    public static class Q extends ApiFeature
    {
        Q(final String s, final String s2) {
            super(s, s2);
        }
        
        @Override
        public final boolean isSupportedByFramework() {
            return Build$VERSION.SDK_INT >= 29;
        }
    }
    
    public static class T extends ApiFeature
    {
        T(final String s, final String s2) {
            super(s, s2);
        }
        
        @Override
        public final boolean isSupportedByFramework() {
            return Build$VERSION.SDK_INT >= 33;
        }
    }
}

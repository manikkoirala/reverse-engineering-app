// 
// Decompiled by Procyon v0.6.0
// 

package androidx.webkit.internal;

import android.webkit.WebViewRenderProcess;
import android.webkit.WebView;
import android.webkit.WebViewRenderProcessClient;

public class WebViewRenderProcessClientFrameworkAdapter extends WebViewRenderProcessClient
{
    private androidx.webkit.WebViewRenderProcessClient mWebViewRenderProcessClient;
    
    public WebViewRenderProcessClientFrameworkAdapter(final androidx.webkit.WebViewRenderProcessClient mWebViewRenderProcessClient) {
        this.mWebViewRenderProcessClient = mWebViewRenderProcessClient;
    }
    
    public androidx.webkit.WebViewRenderProcessClient getFrameworkRenderProcessClient() {
        return this.mWebViewRenderProcessClient;
    }
    
    public void onRenderProcessResponsive(final WebView webView, final WebViewRenderProcess webViewRenderProcess) {
        this.mWebViewRenderProcessClient.onRenderProcessResponsive(webView, WebViewRenderProcessImpl.forFrameworkObject(webViewRenderProcess));
    }
    
    public void onRenderProcessUnresponsive(final WebView webView, final WebViewRenderProcess webViewRenderProcess) {
        this.mWebViewRenderProcessClient.onRenderProcessUnresponsive(webView, WebViewRenderProcessImpl.forFrameworkObject(webViewRenderProcess));
    }
}

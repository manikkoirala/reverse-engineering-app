// 
// Decompiled by Procyon v0.6.0
// 

package androidx.webkit;

public abstract class SafeBrowsingResponseCompat
{
    public abstract void backToSafety(final boolean p0);
    
    public abstract void proceed(final boolean p0);
    
    public abstract void showInterstitial(final boolean p0);
}

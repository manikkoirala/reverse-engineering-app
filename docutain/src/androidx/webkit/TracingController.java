// 
// Decompiled by Procyon v0.6.0
// 

package androidx.webkit;

import androidx.webkit.internal.TracingControllerImpl;
import java.util.concurrent.Executor;
import java.io.OutputStream;

public abstract class TracingController
{
    public static TracingController getInstance() {
        return LAZY_HOLDER.INSTANCE;
    }
    
    public abstract boolean isTracing();
    
    public abstract void start(final TracingConfig p0);
    
    public abstract boolean stop(final OutputStream p0, final Executor p1);
    
    private static class LAZY_HOLDER
    {
        static final TracingController INSTANCE;
        
        static {
            INSTANCE = new TracingControllerImpl();
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.webkit;

import androidx.webkit.internal.ApiFeature;
import androidx.webkit.internal.ApiHelperForN;
import androidx.webkit.internal.WebViewFeatureInternal;
import androidx.webkit.internal.WebViewGlueCommunicator;
import androidx.webkit.internal.WebResourceRequestAdapter;
import android.webkit.WebResourceRequest;

public class WebResourceRequestCompat
{
    private WebResourceRequestCompat() {
    }
    
    private static WebResourceRequestAdapter getAdapter(final WebResourceRequest webResourceRequest) {
        return WebViewGlueCommunicator.getCompatConverter().convertWebResourceRequest(webResourceRequest);
    }
    
    public static boolean isRedirect(final WebResourceRequest webResourceRequest) {
        final ApiFeature.N web_RESOURCE_REQUEST_IS_REDIRECT = WebViewFeatureInternal.WEB_RESOURCE_REQUEST_IS_REDIRECT;
        if (web_RESOURCE_REQUEST_IS_REDIRECT.isSupportedByFramework()) {
            return ApiHelperForN.isRedirect(webResourceRequest);
        }
        if (web_RESOURCE_REQUEST_IS_REDIRECT.isSupportedByWebView()) {
            return getAdapter(webResourceRequest).isRedirect();
        }
        throw WebViewFeatureInternal.getUnsupportedOperationException();
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.webkit;

import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;
import java.util.Objects;

public class WebMessageCompat
{
    public static final int TYPE_ARRAY_BUFFER = 1;
    public static final int TYPE_STRING = 0;
    private final byte[] mArrayBuffer;
    private final WebMessagePortCompat[] mPorts;
    private final String mString;
    private final int mType;
    
    public WebMessageCompat(final String s) {
        this(s, null);
    }
    
    public WebMessageCompat(final String mString, final WebMessagePortCompat[] mPorts) {
        this.mString = mString;
        this.mArrayBuffer = null;
        this.mPorts = mPorts;
        this.mType = 0;
    }
    
    public WebMessageCompat(final byte[] array) {
        this(array, null);
    }
    
    public WebMessageCompat(final byte[] array, final WebMessagePortCompat[] mPorts) {
        Objects.requireNonNull(array);
        this.mArrayBuffer = array;
        this.mString = null;
        this.mPorts = mPorts;
        this.mType = 1;
    }
    
    public byte[] getArrayBuffer() {
        return this.mArrayBuffer;
    }
    
    public String getData() {
        return this.mString;
    }
    
    public WebMessagePortCompat[] getPorts() {
        return this.mPorts;
    }
    
    public int getType() {
        return this.mType;
    }
    
    @Retention(RetentionPolicy.SOURCE)
    public @interface Type {
    }
}

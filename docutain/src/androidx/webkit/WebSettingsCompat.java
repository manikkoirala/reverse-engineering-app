// 
// Decompiled by Procyon v0.6.0
// 

package androidx.webkit;

import java.lang.annotation.ElementType;
import java.lang.annotation.Target;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;
import androidx.webkit.internal.ApiHelperForO;
import java.util.Set;
import androidx.webkit.internal.ApiHelperForM;
import androidx.webkit.internal.ApiHelperForQ;
import androidx.webkit.internal.ApiFeature;
import androidx.webkit.internal.ApiHelperForN;
import androidx.webkit.internal.WebViewFeatureInternal;
import androidx.webkit.internal.WebViewGlueCommunicator;
import androidx.webkit.internal.WebSettingsAdapter;
import android.webkit.WebSettings;

public class WebSettingsCompat
{
    @Deprecated
    public static final int DARK_STRATEGY_PREFER_WEB_THEME_OVER_USER_AGENT_DARKENING = 2;
    @Deprecated
    public static final int DARK_STRATEGY_USER_AGENT_DARKENING_ONLY = 0;
    @Deprecated
    public static final int DARK_STRATEGY_WEB_THEME_DARKENING_ONLY = 1;
    @Deprecated
    public static final int FORCE_DARK_AUTO = 1;
    @Deprecated
    public static final int FORCE_DARK_OFF = 0;
    @Deprecated
    public static final int FORCE_DARK_ON = 2;
    
    private WebSettingsCompat() {
    }
    
    private static WebSettingsAdapter getAdapter(final WebSettings webSettings) {
        return WebViewGlueCommunicator.getCompatConverter().convertSettings(webSettings);
    }
    
    public static int getDisabledActionModeMenuItems(final WebSettings webSettings) {
        final ApiFeature.N disabled_ACTION_MODE_MENU_ITEMS = WebViewFeatureInternal.DISABLED_ACTION_MODE_MENU_ITEMS;
        if (disabled_ACTION_MODE_MENU_ITEMS.isSupportedByFramework()) {
            return ApiHelperForN.getDisabledActionModeMenuItems(webSettings);
        }
        if (disabled_ACTION_MODE_MENU_ITEMS.isSupportedByWebView()) {
            return getAdapter(webSettings).getDisabledActionModeMenuItems();
        }
        throw WebViewFeatureInternal.getUnsupportedOperationException();
    }
    
    public static boolean getEnterpriseAuthenticationAppLinkPolicyEnabled(final WebSettings webSettings) {
        if (WebViewFeatureInternal.ENTERPRISE_AUTHENTICATION_APP_LINK_POLICY.isSupportedByWebView()) {
            return getAdapter(webSettings).getEnterpriseAuthenticationAppLinkPolicyEnabled();
        }
        throw WebViewFeatureInternal.getUnsupportedOperationException();
    }
    
    @Deprecated
    public static int getForceDark(final WebSettings webSettings) {
        final ApiFeature.Q force_DARK = WebViewFeatureInternal.FORCE_DARK;
        if (force_DARK.isSupportedByFramework()) {
            return ApiHelperForQ.getForceDark(webSettings);
        }
        if (force_DARK.isSupportedByWebView()) {
            return getAdapter(webSettings).getForceDark();
        }
        throw WebViewFeatureInternal.getUnsupportedOperationException();
    }
    
    @Deprecated
    public static int getForceDarkStrategy(final WebSettings webSettings) {
        if (WebViewFeatureInternal.FORCE_DARK_STRATEGY.isSupportedByWebView()) {
            return getAdapter(webSettings).getForceDark();
        }
        throw WebViewFeatureInternal.getUnsupportedOperationException();
    }
    
    public static boolean getOffscreenPreRaster(final WebSettings webSettings) {
        final ApiFeature.M off_SCREEN_PRERASTER = WebViewFeatureInternal.OFF_SCREEN_PRERASTER;
        if (off_SCREEN_PRERASTER.isSupportedByFramework()) {
            return ApiHelperForM.getOffscreenPreRaster(webSettings);
        }
        if (off_SCREEN_PRERASTER.isSupportedByWebView()) {
            return getAdapter(webSettings).getOffscreenPreRaster();
        }
        throw WebViewFeatureInternal.getUnsupportedOperationException();
    }
    
    public static Set<String> getRequestedWithHeaderOriginAllowList(final WebSettings webSettings) {
        if (WebViewFeatureInternal.REQUESTED_WITH_HEADER_ALLOW_LIST.isSupportedByWebView()) {
            return getAdapter(webSettings).getRequestedWithHeaderOriginAllowList();
        }
        throw WebViewFeatureInternal.getUnsupportedOperationException();
    }
    
    public static boolean getSafeBrowsingEnabled(final WebSettings webSettings) {
        final ApiFeature.O safe_BROWSING_ENABLE = WebViewFeatureInternal.SAFE_BROWSING_ENABLE;
        if (safe_BROWSING_ENABLE.isSupportedByFramework()) {
            return ApiHelperForO.getSafeBrowsingEnabled(webSettings);
        }
        if (safe_BROWSING_ENABLE.isSupportedByWebView()) {
            return getAdapter(webSettings).getSafeBrowsingEnabled();
        }
        throw WebViewFeatureInternal.getUnsupportedOperationException();
    }
    
    public static boolean isAlgorithmicDarkeningAllowed(final WebSettings webSettings) {
        if (WebViewFeatureInternal.ALGORITHMIC_DARKENING.isSupportedByWebView()) {
            return getAdapter(webSettings).isAlgorithmicDarkeningAllowed();
        }
        throw WebViewFeatureInternal.getUnsupportedOperationException();
    }
    
    public static void setAlgorithmicDarkeningAllowed(final WebSettings webSettings, final boolean algorithmicDarkeningAllowed) {
        if (WebViewFeatureInternal.ALGORITHMIC_DARKENING.isSupportedByWebView()) {
            getAdapter(webSettings).setAlgorithmicDarkeningAllowed(algorithmicDarkeningAllowed);
            return;
        }
        throw WebViewFeatureInternal.getUnsupportedOperationException();
    }
    
    public static void setDisabledActionModeMenuItems(final WebSettings webSettings, final int disabledActionModeMenuItems) {
        final ApiFeature.N disabled_ACTION_MODE_MENU_ITEMS = WebViewFeatureInternal.DISABLED_ACTION_MODE_MENU_ITEMS;
        if (disabled_ACTION_MODE_MENU_ITEMS.isSupportedByFramework()) {
            ApiHelperForN.setDisabledActionModeMenuItems(webSettings, disabledActionModeMenuItems);
        }
        else {
            if (!disabled_ACTION_MODE_MENU_ITEMS.isSupportedByWebView()) {
                throw WebViewFeatureInternal.getUnsupportedOperationException();
            }
            getAdapter(webSettings).setDisabledActionModeMenuItems(disabledActionModeMenuItems);
        }
    }
    
    public static void setEnterpriseAuthenticationAppLinkPolicyEnabled(final WebSettings webSettings, final boolean enterpriseAuthenticationAppLinkPolicyEnabled) {
        if (WebViewFeatureInternal.ENTERPRISE_AUTHENTICATION_APP_LINK_POLICY.isSupportedByWebView()) {
            getAdapter(webSettings).setEnterpriseAuthenticationAppLinkPolicyEnabled(enterpriseAuthenticationAppLinkPolicyEnabled);
            return;
        }
        throw WebViewFeatureInternal.getUnsupportedOperationException();
    }
    
    @Deprecated
    public static void setForceDark(final WebSettings webSettings, final int forceDark) {
        final ApiFeature.Q force_DARK = WebViewFeatureInternal.FORCE_DARK;
        if (force_DARK.isSupportedByFramework()) {
            ApiHelperForQ.setForceDark(webSettings, forceDark);
        }
        else {
            if (!force_DARK.isSupportedByWebView()) {
                throw WebViewFeatureInternal.getUnsupportedOperationException();
            }
            getAdapter(webSettings).setForceDark(forceDark);
        }
    }
    
    @Deprecated
    public static void setForceDarkStrategy(final WebSettings webSettings, final int forceDarkStrategy) {
        if (WebViewFeatureInternal.FORCE_DARK_STRATEGY.isSupportedByWebView()) {
            getAdapter(webSettings).setForceDarkStrategy(forceDarkStrategy);
            return;
        }
        throw WebViewFeatureInternal.getUnsupportedOperationException();
    }
    
    public static void setOffscreenPreRaster(final WebSettings webSettings, final boolean offscreenPreRaster) {
        final ApiFeature.M off_SCREEN_PRERASTER = WebViewFeatureInternal.OFF_SCREEN_PRERASTER;
        if (off_SCREEN_PRERASTER.isSupportedByFramework()) {
            ApiHelperForM.setOffscreenPreRaster(webSettings, offscreenPreRaster);
        }
        else {
            if (!off_SCREEN_PRERASTER.isSupportedByWebView()) {
                throw WebViewFeatureInternal.getUnsupportedOperationException();
            }
            getAdapter(webSettings).setOffscreenPreRaster(offscreenPreRaster);
        }
    }
    
    public static void setRequestedWithHeaderOriginAllowList(final WebSettings webSettings, final Set<String> requestedWithHeaderOriginAllowList) {
        if (WebViewFeatureInternal.REQUESTED_WITH_HEADER_ALLOW_LIST.isSupportedByWebView()) {
            getAdapter(webSettings).setRequestedWithHeaderOriginAllowList(requestedWithHeaderOriginAllowList);
            return;
        }
        throw WebViewFeatureInternal.getUnsupportedOperationException();
    }
    
    public static void setSafeBrowsingEnabled(final WebSettings webSettings, final boolean safeBrowsingEnabled) {
        final ApiFeature.O safe_BROWSING_ENABLE = WebViewFeatureInternal.SAFE_BROWSING_ENABLE;
        if (safe_BROWSING_ENABLE.isSupportedByFramework()) {
            ApiHelperForO.setSafeBrowsingEnabled(webSettings, safeBrowsingEnabled);
        }
        else {
            if (!safe_BROWSING_ENABLE.isSupportedByWebView()) {
                throw WebViewFeatureInternal.getUnsupportedOperationException();
            }
            getAdapter(webSettings).setSafeBrowsingEnabled(safeBrowsingEnabled);
        }
    }
    
    public static void setWillSuppressErrorPage(final WebSettings webSettings, final boolean willSuppressErrorPage) {
        if (WebViewFeatureInternal.SUPPRESS_ERROR_PAGE.isSupportedByWebView()) {
            getAdapter(webSettings).setWillSuppressErrorPage(willSuppressErrorPage);
            return;
        }
        throw WebViewFeatureInternal.getUnsupportedOperationException();
    }
    
    public static boolean willSuppressErrorPage(final WebSettings webSettings) {
        if (WebViewFeatureInternal.SUPPRESS_ERROR_PAGE.isSupportedByWebView()) {
            return getAdapter(webSettings).willSuppressErrorPage();
        }
        throw WebViewFeatureInternal.getUnsupportedOperationException();
    }
    
    @Retention(RetentionPolicy.SOURCE)
    @Target({ ElementType.PARAMETER, ElementType.METHOD })
    public @interface ForceDark {
    }
    
    @Retention(RetentionPolicy.SOURCE)
    @Target({ ElementType.PARAMETER, ElementType.METHOD })
    public @interface ForceDarkStrategy {
    }
    
    @Retention(RetentionPolicy.SOURCE)
    @Target({ ElementType.PARAMETER, ElementType.METHOD })
    public @interface MenuItemFlags {
    }
}

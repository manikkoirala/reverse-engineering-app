// 
// Decompiled by Procyon v0.6.0
// 

package androidx.webkit;

import android.database.Cursor;
import java.io.FileNotFoundException;
import android.os.ParcelFileDescriptor;
import android.content.ContentValues;
import android.net.Uri;
import android.os.Bundle;
import androidx.webkit.internal.WebViewGlueCommunicator;
import org.chromium.support_lib_boundary.DropDataContentProviderBoundaryInterface;
import android.content.ContentProvider;

public final class DropDataContentProvider extends ContentProvider
{
    DropDataContentProviderBoundaryInterface mImpl;
    
    private DropDataContentProviderBoundaryInterface getDropImpl() {
        if (this.mImpl == null) {
            (this.mImpl = WebViewGlueCommunicator.getFactory().getDropDataProvider()).onCreate();
        }
        return this.mImpl;
    }
    
    public Bundle call(final String s, final String s2, final Bundle bundle) {
        return this.getDropImpl().call(s, s2, bundle);
    }
    
    public int delete(final Uri uri, final String s, final String[] array) {
        throw new UnsupportedOperationException("delete method is not supported.");
    }
    
    public String getType(final Uri uri) {
        return this.getDropImpl().getType(uri);
    }
    
    public Uri insert(final Uri uri, final ContentValues contentValues) {
        throw new UnsupportedOperationException("Insert method is not supported.");
    }
    
    public boolean onCreate() {
        return true;
    }
    
    public ParcelFileDescriptor openFile(final Uri uri, final String s) throws FileNotFoundException {
        return this.getDropImpl().openFile((ContentProvider)this, uri);
    }
    
    public Cursor query(final Uri uri, final String[] array, final String s, final String[] array2, final String s2) {
        return this.getDropImpl().query(uri, array, s, array2, s2);
    }
    
    public int update(final Uri uri, final ContentValues contentValues, final String s, final String[] array) {
        throw new UnsupportedOperationException("update method is not supported.");
    }
}

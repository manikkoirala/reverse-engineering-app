// 
// Decompiled by Procyon v0.6.0
// 

package androidx.webkit;

import android.webkit.WebResourceResponse;
import android.webkit.WebResourceRequest;

public abstract class ServiceWorkerClientCompat
{
    public abstract WebResourceResponse shouldInterceptRequest(final WebResourceRequest p0);
}

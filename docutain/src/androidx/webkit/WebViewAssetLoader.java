// 
// Decompiled by Procyon v0.6.0
// 

package androidx.webkit;

import android.content.res.Resources$NotFoundException;
import java.io.File;
import java.util.ArrayList;
import androidx.core.util.Pair;
import java.io.IOException;
import java.io.InputStream;
import android.util.Log;
import android.content.Context;
import androidx.webkit.internal.AssetHelper;
import java.util.Iterator;
import android.webkit.WebResourceResponse;
import android.net.Uri;
import java.util.List;

public final class WebViewAssetLoader
{
    public static final String DEFAULT_DOMAIN = "appassets.androidplatform.net";
    private static final String TAG = "WebViewAssetLoader";
    private final List<PathMatcher> mMatchers;
    
    WebViewAssetLoader(final List<PathMatcher> mMatchers) {
        this.mMatchers = mMatchers;
    }
    
    public WebResourceResponse shouldInterceptRequest(final Uri uri) {
        for (final PathMatcher pathMatcher : this.mMatchers) {
            final PathHandler match = pathMatcher.match(uri);
            if (match == null) {
                continue;
            }
            final WebResourceResponse handle = match.handle(pathMatcher.getSuffixPath(uri.getPath()));
            if (handle == null) {
                continue;
            }
            return handle;
        }
        return null;
    }
    
    public static final class AssetsPathHandler implements PathHandler
    {
        private AssetHelper mAssetHelper;
        
        public AssetsPathHandler(final Context context) {
            this.mAssetHelper = new AssetHelper(context);
        }
        
        AssetsPathHandler(final AssetHelper mAssetHelper) {
            this.mAssetHelper = mAssetHelper;
        }
        
        @Override
        public WebResourceResponse handle(final String str) {
            try {
                return new WebResourceResponse(AssetHelper.guessMimeType(str), (String)null, this.mAssetHelper.openAsset(str));
            }
            catch (final IOException ex) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Error opening asset path: ");
                sb.append(str);
                Log.e("WebViewAssetLoader", sb.toString(), (Throwable)ex);
                return new WebResourceResponse((String)null, (String)null, (InputStream)null);
            }
        }
    }
    
    public static final class Builder
    {
        private String mDomain;
        private final List<Pair<String, PathHandler>> mHandlerList;
        private boolean mHttpAllowed;
        
        public Builder() {
            this.mDomain = "appassets.androidplatform.net";
            this.mHandlerList = new ArrayList<Pair<String, PathHandler>>();
        }
        
        public Builder addPathHandler(final String s, final PathHandler pathHandler) {
            this.mHandlerList.add(Pair.create(s, pathHandler));
            return this;
        }
        
        public WebViewAssetLoader build() {
            final ArrayList list = new ArrayList();
            for (final Pair pair : this.mHandlerList) {
                list.add(new PathMatcher(this.mDomain, (String)pair.first, this.mHttpAllowed, (PathHandler)pair.second));
            }
            return new WebViewAssetLoader(list);
        }
        
        public Builder setDomain(final String mDomain) {
            this.mDomain = mDomain;
            return this;
        }
        
        public Builder setHttpAllowed(final boolean mHttpAllowed) {
            this.mHttpAllowed = mHttpAllowed;
            return this;
        }
    }
    
    public static final class InternalStoragePathHandler implements PathHandler
    {
        private static final String[] FORBIDDEN_DATA_DIRS;
        private final File mDirectory;
        
        static {
            FORBIDDEN_DATA_DIRS = new String[] { "app_webview/", "databases/", "lib/", "shared_prefs/", "code_cache/" };
        }
        
        public InternalStoragePathHandler(final Context context, final File obj) {
            try {
                this.mDirectory = new File(AssetHelper.getCanonicalDirPath(obj));
                if (this.isAllowedInternalStorageDir(context)) {
                    return;
                }
                final StringBuilder sb = new StringBuilder();
                sb.append("The given directory \"");
                sb.append(obj);
                sb.append("\" doesn't exist under an allowed app internal storage directory");
                throw new IllegalArgumentException(sb.toString());
            }
            catch (final IOException cause) {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("Failed to resolve the canonical path for the given directory: ");
                sb2.append(obj.getPath());
                throw new IllegalArgumentException(sb2.toString(), cause);
            }
        }
        
        private boolean isAllowedInternalStorageDir(final Context context) throws IOException {
            final String canonicalDirPath = AssetHelper.getCanonicalDirPath(this.mDirectory);
            final String canonicalDirPath2 = AssetHelper.getCanonicalDirPath(context.getCacheDir());
            final String canonicalDirPath3 = AssetHelper.getCanonicalDirPath(AssetHelper.getDataDir(context));
            if (!canonicalDirPath.startsWith(canonicalDirPath2) && !canonicalDirPath.startsWith(canonicalDirPath3)) {
                return false;
            }
            if (!canonicalDirPath.equals(canonicalDirPath2) && !canonicalDirPath.equals(canonicalDirPath3)) {
                for (final String str : InternalStoragePathHandler.FORBIDDEN_DATA_DIRS) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append(canonicalDirPath3);
                    sb.append(str);
                    if (canonicalDirPath.startsWith(sb.toString())) {
                        return false;
                    }
                }
                return true;
            }
            return false;
        }
        
        @Override
        public WebResourceResponse handle(final String str) {
            try {
                final File canonicalFileIfChild = AssetHelper.getCanonicalFileIfChild(this.mDirectory, str);
                if (canonicalFileIfChild != null) {
                    return new WebResourceResponse(AssetHelper.guessMimeType(str), (String)null, AssetHelper.openFile(canonicalFileIfChild));
                }
                Log.e("WebViewAssetLoader", String.format("The requested file: %s is outside the mounted directory: %s", str, this.mDirectory));
            }
            catch (final IOException ex) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Error opening the requested path: ");
                sb.append(str);
                Log.e("WebViewAssetLoader", sb.toString(), (Throwable)ex);
            }
            return new WebResourceResponse((String)null, (String)null, (InputStream)null);
        }
    }
    
    public interface PathHandler
    {
        WebResourceResponse handle(final String p0);
    }
    
    static class PathMatcher
    {
        static final String HTTPS_SCHEME = "https";
        static final String HTTP_SCHEME = "http";
        final String mAuthority;
        final PathHandler mHandler;
        final boolean mHttpEnabled;
        final String mPath;
        
        PathMatcher(final String mAuthority, final String mPath, final boolean mHttpEnabled, final PathHandler mHandler) {
            if (mPath.isEmpty() || mPath.charAt(0) != '/') {
                throw new IllegalArgumentException("Path should start with a slash '/'.");
            }
            if (mPath.endsWith("/")) {
                this.mAuthority = mAuthority;
                this.mPath = mPath;
                this.mHttpEnabled = mHttpEnabled;
                this.mHandler = mHandler;
                return;
            }
            throw new IllegalArgumentException("Path should end with a slash '/'");
        }
        
        public String getSuffixPath(final String s) {
            return s.replaceFirst(this.mPath, "");
        }
        
        public PathHandler match(final Uri uri) {
            if (uri.getScheme().equals("http") && !this.mHttpEnabled) {
                return null;
            }
            if (!uri.getScheme().equals("http") && !uri.getScheme().equals("https")) {
                return null;
            }
            if (!uri.getAuthority().equals(this.mAuthority)) {
                return null;
            }
            if (!uri.getPath().startsWith(this.mPath)) {
                return null;
            }
            return this.mHandler;
        }
    }
    
    public static final class ResourcesPathHandler implements PathHandler
    {
        private AssetHelper mAssetHelper;
        
        public ResourcesPathHandler(final Context context) {
            this.mAssetHelper = new AssetHelper(context);
        }
        
        ResourcesPathHandler(final AssetHelper mAssetHelper) {
            this.mAssetHelper = mAssetHelper;
        }
        
        @Override
        public WebResourceResponse handle(final String s) {
            try {
                return new WebResourceResponse(AssetHelper.guessMimeType(s), (String)null, this.mAssetHelper.openResource(s));
            }
            catch (final IOException ex) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Error opening resource from the path: ");
                sb.append(s);
                Log.e("WebViewAssetLoader", sb.toString(), (Throwable)ex);
            }
            catch (final Resources$NotFoundException ex2) {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("Resource not found from the path: ");
                sb2.append(s);
                Log.e("WebViewAssetLoader", sb2.toString(), (Throwable)ex2);
            }
            return new WebResourceResponse((String)null, (String)null, (InputStream)null);
        }
    }
}

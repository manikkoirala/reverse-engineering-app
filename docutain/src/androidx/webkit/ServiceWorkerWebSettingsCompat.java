// 
// Decompiled by Procyon v0.6.0
// 

package androidx.webkit;

import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;
import java.util.Set;

public abstract class ServiceWorkerWebSettingsCompat
{
    public abstract boolean getAllowContentAccess();
    
    public abstract boolean getAllowFileAccess();
    
    public abstract boolean getBlockNetworkLoads();
    
    public abstract int getCacheMode();
    
    public abstract Set<String> getRequestedWithHeaderOriginAllowList();
    
    public abstract void setAllowContentAccess(final boolean p0);
    
    public abstract void setAllowFileAccess(final boolean p0);
    
    public abstract void setBlockNetworkLoads(final boolean p0);
    
    public abstract void setCacheMode(final int p0);
    
    public abstract void setRequestedWithHeaderOriginAllowList(final Set<String> p0);
    
    @Retention(RetentionPolicy.SOURCE)
    public @interface CacheMode {
    }
}

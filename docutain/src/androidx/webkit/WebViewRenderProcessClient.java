// 
// Decompiled by Procyon v0.6.0
// 

package androidx.webkit;

import android.webkit.WebView;

public abstract class WebViewRenderProcessClient
{
    public abstract void onRenderProcessResponsive(final WebView p0, final WebViewRenderProcess p1);
    
    public abstract void onRenderProcessUnresponsive(final WebView p0, final WebViewRenderProcess p1);
}

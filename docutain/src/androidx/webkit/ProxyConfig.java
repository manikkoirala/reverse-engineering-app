// 
// Decompiled by Procyon v0.6.0
// 

package androidx.webkit;

import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public final class ProxyConfig
{
    private static final String BYPASS_RULE_REMOVE_IMPLICIT = "<-loopback>";
    private static final String BYPASS_RULE_SIMPLE_NAMES = "<local>";
    private static final String DIRECT = "direct://";
    public static final String MATCH_ALL_SCHEMES = "*";
    public static final String MATCH_HTTP = "http";
    public static final String MATCH_HTTPS = "https";
    private List<String> mBypassRules;
    private List<ProxyRule> mProxyRules;
    private boolean mReverseBypass;
    
    public ProxyConfig(final List<ProxyRule> mProxyRules, final List<String> mBypassRules, final boolean mReverseBypass) {
        this.mProxyRules = mProxyRules;
        this.mBypassRules = mBypassRules;
        this.mReverseBypass = mReverseBypass;
    }
    
    public List<String> getBypassRules() {
        return Collections.unmodifiableList((List<? extends String>)this.mBypassRules);
    }
    
    public List<ProxyRule> getProxyRules() {
        return Collections.unmodifiableList((List<? extends ProxyRule>)this.mProxyRules);
    }
    
    public boolean isReverseBypassEnabled() {
        return this.mReverseBypass;
    }
    
    public static final class Builder
    {
        private List<String> mBypassRules;
        private List<ProxyRule> mProxyRules;
        private boolean mReverseBypass;
        
        public Builder() {
            this.mReverseBypass = false;
            this.mProxyRules = new ArrayList<ProxyRule>();
            this.mBypassRules = new ArrayList<String>();
        }
        
        public Builder(final ProxyConfig proxyConfig) {
            this.mReverseBypass = false;
            this.mProxyRules = proxyConfig.getProxyRules();
            this.mBypassRules = proxyConfig.getBypassRules();
            this.mReverseBypass = proxyConfig.isReverseBypassEnabled();
        }
        
        private List<String> bypassRules() {
            return this.mBypassRules;
        }
        
        private List<ProxyRule> proxyRules() {
            return this.mProxyRules;
        }
        
        private boolean reverseBypass() {
            return this.mReverseBypass;
        }
        
        public Builder addBypassRule(final String s) {
            this.mBypassRules.add(s);
            return this;
        }
        
        public Builder addDirect() {
            return this.addDirect("*");
        }
        
        public Builder addDirect(final String s) {
            this.mProxyRules.add(new ProxyRule(s, "direct://"));
            return this;
        }
        
        public Builder addProxyRule(final String s) {
            this.mProxyRules.add(new ProxyRule(s));
            return this;
        }
        
        public Builder addProxyRule(final String s, final String s2) {
            this.mProxyRules.add(new ProxyRule(s2, s));
            return this;
        }
        
        public ProxyConfig build() {
            return new ProxyConfig(this.proxyRules(), this.bypassRules(), this.reverseBypass());
        }
        
        public Builder bypassSimpleHostnames() {
            return this.addBypassRule("<local>");
        }
        
        public Builder removeImplicitRules() {
            return this.addBypassRule("<-loopback>");
        }
        
        public Builder setReverseBypassEnabled(final boolean mReverseBypass) {
            this.mReverseBypass = mReverseBypass;
            return this;
        }
    }
    
    public static final class ProxyRule
    {
        private String mSchemeFilter;
        private String mUrl;
        
        public ProxyRule(final String s) {
            this("*", s);
        }
        
        public ProxyRule(final String mSchemeFilter, final String mUrl) {
            this.mSchemeFilter = mSchemeFilter;
            this.mUrl = mUrl;
        }
        
        public String getSchemeFilter() {
            return this.mSchemeFilter;
        }
        
        public String getUrl() {
            return this.mUrl;
        }
    }
    
    @Retention(RetentionPolicy.SOURCE)
    public @interface ProxyScheme {
    }
}

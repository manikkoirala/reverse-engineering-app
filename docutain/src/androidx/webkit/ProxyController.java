// 
// Decompiled by Procyon v0.6.0
// 

package androidx.webkit;

import androidx.webkit.internal.ProxyControllerImpl;
import java.util.concurrent.Executor;

public abstract class ProxyController
{
    public static ProxyController getInstance() {
        if (WebViewFeature.isFeatureSupported("PROXY_OVERRIDE")) {
            return LAZY_HOLDER.INSTANCE;
        }
        throw new UnsupportedOperationException("Proxy override not supported");
    }
    
    public abstract void clearProxyOverride(final Executor p0, final Runnable p1);
    
    public abstract void setProxyOverride(final ProxyConfig p0, final Executor p1, final Runnable p2);
    
    private static class LAZY_HOLDER
    {
        static final ProxyController INSTANCE;
        
        static {
            INSTANCE = new ProxyControllerImpl();
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.webkit;

import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;
import android.app.PendingIntent;
import androidx.webkit.internal.WebViewFeatureInternal;
import androidx.webkit.internal.SafeBrowsingResponseImpl;
import android.webkit.SafeBrowsingResponse;
import android.webkit.WebResourceResponse;
import java.lang.reflect.InvocationHandler;
import androidx.webkit.internal.ApiHelperForLollipop;
import androidx.webkit.internal.WebResourceErrorImpl;
import android.os.Build$VERSION;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import org.chromium.support_lib_boundary.WebViewClientBoundaryInterface;
import android.webkit.WebViewClient;

public class WebViewClientCompat extends WebViewClient implements WebViewClientBoundaryInterface
{
    private static final String[] sSupportedFeatures;
    
    static {
        sSupportedFeatures = new String[] { "VISUAL_STATE_CALLBACK", "RECEIVE_WEB_RESOURCE_ERROR", "RECEIVE_HTTP_ERROR", "SHOULD_OVERRIDE_WITH_REDIRECTS", "SAFE_BROWSING_HIT" };
    }
    
    public final String[] getSupportedFeatures() {
        return WebViewClientCompat.sSupportedFeatures;
    }
    
    public void onPageCommitVisible(final WebView webView, final String s) {
    }
    
    public final void onReceivedError(final WebView webView, final WebResourceRequest webResourceRequest, final WebResourceError webResourceError) {
        if (Build$VERSION.SDK_INT < 23) {
            return;
        }
        this.onReceivedError(webView, webResourceRequest, new WebResourceErrorImpl(webResourceError));
    }
    
    public void onReceivedError(final WebView webView, final WebResourceRequest webResourceRequest, final WebResourceErrorCompat webResourceErrorCompat) {
        if (Build$VERSION.SDK_INT < 21) {
            return;
        }
        if (WebViewFeature.isFeatureSupported("WEB_RESOURCE_ERROR_GET_CODE")) {
            if (WebViewFeature.isFeatureSupported("WEB_RESOURCE_ERROR_GET_DESCRIPTION")) {
                if (ApiHelperForLollipop.isForMainFrame(webResourceRequest)) {
                    this.onReceivedError(webView, webResourceErrorCompat.getErrorCode(), webResourceErrorCompat.getDescription().toString(), ApiHelperForLollipop.getUrl(webResourceRequest).toString());
                }
            }
        }
    }
    
    public final void onReceivedError(final WebView webView, final WebResourceRequest webResourceRequest, final InvocationHandler invocationHandler) {
        this.onReceivedError(webView, webResourceRequest, new WebResourceErrorImpl(invocationHandler));
    }
    
    public void onReceivedHttpError(final WebView webView, final WebResourceRequest webResourceRequest, final WebResourceResponse webResourceResponse) {
    }
    
    public final void onSafeBrowsingHit(final WebView webView, final WebResourceRequest webResourceRequest, final int n, final SafeBrowsingResponse safeBrowsingResponse) {
        this.onSafeBrowsingHit(webView, webResourceRequest, n, new SafeBrowsingResponseImpl(safeBrowsingResponse));
    }
    
    public void onSafeBrowsingHit(final WebView webView, final WebResourceRequest webResourceRequest, final int n, final SafeBrowsingResponseCompat safeBrowsingResponseCompat) {
        if (WebViewFeature.isFeatureSupported("SAFE_BROWSING_RESPONSE_SHOW_INTERSTITIAL")) {
            safeBrowsingResponseCompat.showInterstitial(true);
            return;
        }
        throw WebViewFeatureInternal.getUnsupportedOperationException();
    }
    
    public final void onSafeBrowsingHit(final WebView webView, final WebResourceRequest webResourceRequest, final int n, final InvocationHandler invocationHandler) {
        this.onSafeBrowsingHit(webView, webResourceRequest, n, new SafeBrowsingResponseImpl(invocationHandler));
    }
    
    public boolean onWebAuthnIntent(final WebView webView, final PendingIntent pendingIntent, final InvocationHandler invocationHandler) {
        return false;
    }
    
    public boolean shouldOverrideUrlLoading(final WebView webView, final WebResourceRequest webResourceRequest) {
        return Build$VERSION.SDK_INT >= 21 && this.shouldOverrideUrlLoading(webView, ApiHelperForLollipop.getUrl(webResourceRequest).toString());
    }
    
    @Retention(RetentionPolicy.SOURCE)
    public @interface SafeBrowsingThreat {
    }
}

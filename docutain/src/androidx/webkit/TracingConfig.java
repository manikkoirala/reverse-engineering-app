// 
// Decompiled by Procyon v0.6.0
// 

package androidx.webkit;

import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;
import java.util.Arrays;
import java.util.Collection;
import java.util.ArrayList;
import java.util.List;

public class TracingConfig
{
    public static final int CATEGORIES_ALL = 1;
    public static final int CATEGORIES_ANDROID_WEBVIEW = 2;
    public static final int CATEGORIES_FRAME_VIEWER = 64;
    public static final int CATEGORIES_INPUT_LATENCY = 8;
    public static final int CATEGORIES_JAVASCRIPT_AND_RENDERING = 32;
    public static final int CATEGORIES_NONE = 0;
    public static final int CATEGORIES_RENDERING = 16;
    public static final int CATEGORIES_WEB_DEVELOPER = 4;
    public static final int RECORD_CONTINUOUSLY = 1;
    public static final int RECORD_UNTIL_FULL = 0;
    private final List<String> mCustomIncludedCategories;
    private int mPredefinedCategories;
    private int mTracingMode;
    
    public TracingConfig(final int mPredefinedCategories, final List<String> list, final int mTracingMode) {
        final ArrayList mCustomIncludedCategories = new ArrayList();
        this.mCustomIncludedCategories = mCustomIncludedCategories;
        this.mPredefinedCategories = mPredefinedCategories;
        mCustomIncludedCategories.addAll(list);
        this.mTracingMode = mTracingMode;
    }
    
    public List<String> getCustomIncludedCategories() {
        return this.mCustomIncludedCategories;
    }
    
    public int getPredefinedCategories() {
        return this.mPredefinedCategories;
    }
    
    public int getTracingMode() {
        return this.mTracingMode;
    }
    
    public static class Builder
    {
        private final List<String> mCustomIncludedCategories;
        private int mPredefinedCategories;
        private int mTracingMode;
        
        public Builder() {
            this.mPredefinedCategories = 0;
            this.mCustomIncludedCategories = new ArrayList<String>();
            this.mTracingMode = 1;
        }
        
        public Builder addCategories(final Collection<String> collection) {
            this.mCustomIncludedCategories.addAll(collection);
            return this;
        }
        
        public Builder addCategories(final int... array) {
            for (int length = array.length, i = 0; i < length; ++i) {
                this.mPredefinedCategories |= array[i];
            }
            return this;
        }
        
        public Builder addCategories(final String... a) {
            this.mCustomIncludedCategories.addAll(Arrays.asList(a));
            return this;
        }
        
        public TracingConfig build() {
            return new TracingConfig(this.mPredefinedCategories, this.mCustomIncludedCategories, this.mTracingMode);
        }
        
        public Builder setTracingMode(final int mTracingMode) {
            this.mTracingMode = mTracingMode;
            return this;
        }
    }
    
    @Retention(RetentionPolicy.SOURCE)
    public @interface PredefinedCategories {
    }
    
    @Retention(RetentionPolicy.SOURCE)
    public @interface TracingMode {
    }
}

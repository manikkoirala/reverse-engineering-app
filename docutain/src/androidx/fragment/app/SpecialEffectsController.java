// 
// Decompiled by Procyon v0.6.0
// 

package androidx.fragment.app;

import android.view.ViewParent;
import java.util.LinkedHashSet;
import java.util.Set;
import androidx.fragment.R;
import java.util.ListIterator;
import kotlin.collections.CollectionsKt;
import java.util.Collection;
import androidx.core.view.ViewCompat;
import android.util.Log;
import kotlin.jvm.JvmStatic;
import java.util.Iterator;
import android.view.View;
import kotlin.Unit;
import androidx.core.os.CancellationSignal;
import java.util.ArrayList;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.DefaultConstructorMarker;
import java.util.List;
import android.view.ViewGroup;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000T\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u000b\b \u0018\u0000 *2\u00020\u0001:\u0003*+,B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J \u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u0015H\u0002J\u0016\u0010\u0016\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0014\u001a\u00020\u0015J\u000e\u0010\u0017\u001a\u00020\u000f2\u0006\u0010\u0014\u001a\u00020\u0015J\u000e\u0010\u0018\u001a\u00020\u000f2\u0006\u0010\u0014\u001a\u00020\u0015J\u000e\u0010\u0019\u001a\u00020\u000f2\u0006\u0010\u0014\u001a\u00020\u0015J#\u0010\u001a\u001a\u00020\u000f2\u0011\u0010\u001b\u001a\r\u0012\t\u0012\u00070\f¢\u0006\u0002\b\u001d0\u001c2\u0006\u0010\u001e\u001a\u00020\bH&J\u0006\u0010\u001f\u001a\u00020\u000fJ\u0012\u0010 \u001a\u0004\u0018\u00010\f2\u0006\u0010!\u001a\u00020\"H\u0002J\u0012\u0010#\u001a\u0004\u0018\u00010\f2\u0006\u0010!\u001a\u00020\"H\u0002J\u0006\u0010$\u001a\u00020\u000fJ\u0006\u0010%\u001a\u00020\u000fJ\u0010\u0010&\u001a\u0004\u0018\u00010\u00132\u0006\u0010\u0014\u001a\u00020\u0015J\u0006\u0010'\u001a\u00020\u000fJ\b\u0010(\u001a\u00020\u000fH\u0002J\u000e\u0010)\u001a\u00020\u000f2\u0006\u0010\u001e\u001a\u00020\bR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006R\u000e\u0010\u0007\u001a\u00020\bX\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\bX\u0082\u000e¢\u0006\u0002\n\u0000R\u0014\u0010\n\u001a\b\u0012\u0004\u0012\u00020\f0\u000bX\u0082\u0004¢\u0006\u0002\n\u0000R\u0014\u0010\r\u001a\b\u0012\u0004\u0012\u00020\f0\u000bX\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006-" }, d2 = { "Landroidx/fragment/app/SpecialEffectsController;", "", "container", "Landroid/view/ViewGroup;", "(Landroid/view/ViewGroup;)V", "getContainer", "()Landroid/view/ViewGroup;", "isContainerPostponed", "", "operationDirectionIsPop", "pendingOperations", "", "Landroidx/fragment/app/SpecialEffectsController$Operation;", "runningOperations", "enqueue", "", "finalState", "Landroidx/fragment/app/SpecialEffectsController$Operation$State;", "lifecycleImpact", "Landroidx/fragment/app/SpecialEffectsController$Operation$LifecycleImpact;", "fragmentStateManager", "Landroidx/fragment/app/FragmentStateManager;", "enqueueAdd", "enqueueHide", "enqueueRemove", "enqueueShow", "executeOperations", "operations", "", "Lkotlin/jvm/JvmSuppressWildcards;", "isPop", "executePendingOperations", "findPendingOperation", "fragment", "Landroidx/fragment/app/Fragment;", "findRunningOperation", "forceCompleteAllOperations", "forcePostponedExecutePendingOperations", "getAwaitingCompletionLifecycleImpact", "markPostponedState", "updateFinalState", "updateOperationDirection", "Companion", "FragmentStateManagerOperation", "Operation", "fragment_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
public abstract class SpecialEffectsController
{
    public static final Companion Companion;
    private final ViewGroup container;
    private boolean isContainerPostponed;
    private boolean operationDirectionIsPop;
    private final List<Operation> pendingOperations;
    private final List<Operation> runningOperations;
    
    static {
        Companion = new Companion(null);
    }
    
    public SpecialEffectsController(final ViewGroup container) {
        Intrinsics.checkNotNullParameter((Object)container, "container");
        this.container = container;
        this.pendingOperations = new ArrayList<Operation>();
        this.runningOperations = new ArrayList<Operation>();
    }
    
    private final void enqueue(final State state, final LifecycleImpact lifecycleImpact, final FragmentStateManager fragmentStateManager) {
        synchronized (this.pendingOperations) {
            final CancellationSignal cancellationSignal = new CancellationSignal();
            final Fragment fragment = fragmentStateManager.getFragment();
            Intrinsics.checkNotNullExpressionValue((Object)fragment, "fragmentStateManager.fragment");
            final Operation pendingOperation = this.findPendingOperation(fragment);
            if (pendingOperation != null) {
                pendingOperation.mergeWith(state, lifecycleImpact);
                return;
            }
            final FragmentStateManagerOperation fragmentStateManagerOperation = new FragmentStateManagerOperation(state, lifecycleImpact, fragmentStateManager, cancellationSignal);
            this.pendingOperations.add((Operation)fragmentStateManagerOperation);
            ((Operation)fragmentStateManagerOperation).addCompletionListener(new SpecialEffectsController$$ExternalSyntheticLambda0(this, fragmentStateManagerOperation));
            ((Operation)fragmentStateManagerOperation).addCompletionListener(new SpecialEffectsController$$ExternalSyntheticLambda1(this, fragmentStateManagerOperation));
            final Unit instance = Unit.INSTANCE;
        }
    }
    
    private static final void enqueue$lambda$4$lambda$2(final SpecialEffectsController specialEffectsController, final FragmentStateManagerOperation fragmentStateManagerOperation) {
        Intrinsics.checkNotNullParameter((Object)specialEffectsController, "this$0");
        Intrinsics.checkNotNullParameter((Object)fragmentStateManagerOperation, "$operation");
        if (specialEffectsController.pendingOperations.contains(fragmentStateManagerOperation)) {
            final State finalState = ((Operation)fragmentStateManagerOperation).getFinalState();
            final View mView = ((Operation)fragmentStateManagerOperation).getFragment().mView;
            Intrinsics.checkNotNullExpressionValue((Object)mView, "operation.fragment.mView");
            finalState.applyState(mView);
        }
    }
    
    private static final void enqueue$lambda$4$lambda$3(final SpecialEffectsController specialEffectsController, final FragmentStateManagerOperation fragmentStateManagerOperation) {
        Intrinsics.checkNotNullParameter((Object)specialEffectsController, "this$0");
        Intrinsics.checkNotNullParameter((Object)fragmentStateManagerOperation, "$operation");
        specialEffectsController.pendingOperations.remove(fragmentStateManagerOperation);
        specialEffectsController.runningOperations.remove(fragmentStateManagerOperation);
    }
    
    private final Operation findPendingOperation(final Fragment fragment) {
        for (final Object next : this.pendingOperations) {
            final Operation operation = (Operation)next;
            if (Intrinsics.areEqual((Object)operation.getFragment(), (Object)fragment) && !operation.isCanceled()) {
                return (Operation)next;
            }
        }
        return null;
    }
    
    private final Operation findRunningOperation(final Fragment fragment) {
        for (final Object next : this.runningOperations) {
            final Operation operation = (Operation)next;
            if (Intrinsics.areEqual((Object)operation.getFragment(), (Object)fragment) && !operation.isCanceled()) {
                return (Operation)next;
            }
        }
        return null;
    }
    
    @JvmStatic
    public static final SpecialEffectsController getOrCreateController(final ViewGroup viewGroup, final FragmentManager fragmentManager) {
        return SpecialEffectsController.Companion.getOrCreateController(viewGroup, fragmentManager);
    }
    
    @JvmStatic
    public static final SpecialEffectsController getOrCreateController(final ViewGroup viewGroup, final SpecialEffectsControllerFactory specialEffectsControllerFactory) {
        return SpecialEffectsController.Companion.getOrCreateController(viewGroup, specialEffectsControllerFactory);
    }
    
    private final void updateFinalState() {
        for (final Operation operation : this.pendingOperations) {
            if (operation.getLifecycleImpact() == LifecycleImpact.ADDING) {
                final View requireView = operation.getFragment().requireView();
                Intrinsics.checkNotNullExpressionValue((Object)requireView, "fragment.requireView()");
                operation.mergeWith(State.Companion.from(requireView.getVisibility()), LifecycleImpact.NONE);
            }
        }
    }
    
    public final void enqueueAdd(final State state, final FragmentStateManager fragmentStateManager) {
        Intrinsics.checkNotNullParameter((Object)state, "finalState");
        Intrinsics.checkNotNullParameter((Object)fragmentStateManager, "fragmentStateManager");
        if (FragmentManager.isLoggingEnabled(2)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("SpecialEffectsController: Enqueuing add operation for fragment ");
            sb.append(fragmentStateManager.getFragment());
            Log.v("FragmentManager", sb.toString());
        }
        this.enqueue(state, LifecycleImpact.ADDING, fragmentStateManager);
    }
    
    public final void enqueueHide(final FragmentStateManager fragmentStateManager) {
        Intrinsics.checkNotNullParameter((Object)fragmentStateManager, "fragmentStateManager");
        if (FragmentManager.isLoggingEnabled(2)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("SpecialEffectsController: Enqueuing hide operation for fragment ");
            sb.append(fragmentStateManager.getFragment());
            Log.v("FragmentManager", sb.toString());
        }
        this.enqueue(State.GONE, LifecycleImpact.NONE, fragmentStateManager);
    }
    
    public final void enqueueRemove(final FragmentStateManager fragmentStateManager) {
        Intrinsics.checkNotNullParameter((Object)fragmentStateManager, "fragmentStateManager");
        if (FragmentManager.isLoggingEnabled(2)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("SpecialEffectsController: Enqueuing remove operation for fragment ");
            sb.append(fragmentStateManager.getFragment());
            Log.v("FragmentManager", sb.toString());
        }
        this.enqueue(State.REMOVED, LifecycleImpact.REMOVING, fragmentStateManager);
    }
    
    public final void enqueueShow(final FragmentStateManager fragmentStateManager) {
        Intrinsics.checkNotNullParameter((Object)fragmentStateManager, "fragmentStateManager");
        if (FragmentManager.isLoggingEnabled(2)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("SpecialEffectsController: Enqueuing show operation for fragment ");
            sb.append(fragmentStateManager.getFragment());
            Log.v("FragmentManager", sb.toString());
        }
        this.enqueue(State.VISIBLE, LifecycleImpact.NONE, fragmentStateManager);
    }
    
    public abstract void executeOperations(final List<Operation> p0, final boolean p1);
    
    public final void executePendingOperations() {
        if (this.isContainerPostponed) {
            return;
        }
        if (!ViewCompat.isAttachedToWindow((View)this.container)) {
            this.forceCompleteAllOperations();
            this.operationDirectionIsPop = false;
            return;
        }
        synchronized (this.pendingOperations) {
            if (this.pendingOperations.isEmpty() ^ true) {
                final List mutableList = CollectionsKt.toMutableList((Collection)this.runningOperations);
                this.runningOperations.clear();
                for (final Operation obj : mutableList) {
                    if (FragmentManager.isLoggingEnabled(2)) {
                        final StringBuilder sb = new StringBuilder();
                        sb.append("SpecialEffectsController: Cancelling operation ");
                        sb.append(obj);
                        Log.v("FragmentManager", sb.toString());
                    }
                    obj.cancel();
                    if (!obj.isComplete()) {
                        this.runningOperations.add(obj);
                    }
                }
                this.updateFinalState();
                final List mutableList2 = CollectionsKt.toMutableList((Collection)this.pendingOperations);
                this.pendingOperations.clear();
                this.runningOperations.addAll(mutableList2);
                if (FragmentManager.isLoggingEnabled(2)) {
                    Log.v("FragmentManager", "SpecialEffectsController: Executing pending operations");
                }
                final Iterator iterator2 = mutableList2.iterator();
                while (iterator2.hasNext()) {
                    ((Operation)iterator2.next()).onStart();
                }
                this.executeOperations(mutableList2, this.operationDirectionIsPop);
                this.operationDirectionIsPop = false;
                if (FragmentManager.isLoggingEnabled(2)) {
                    Log.v("FragmentManager", "SpecialEffectsController: Finished executing pending operations");
                }
            }
            final Unit instance = Unit.INSTANCE;
        }
    }
    
    public final void forceCompleteAllOperations() {
        if (FragmentManager.isLoggingEnabled(2)) {
            Log.v("FragmentManager", "SpecialEffectsController: Forcing all operations to complete");
        }
        final boolean attachedToWindow = ViewCompat.isAttachedToWindow((View)this.container);
        synchronized (this.pendingOperations) {
            this.updateFinalState();
            final Iterator<Operation> iterator = this.pendingOperations.iterator();
            while (iterator.hasNext()) {
                iterator.next().onStart();
            }
            for (final Operation obj : CollectionsKt.toMutableList((Collection)this.runningOperations)) {
                if (FragmentManager.isLoggingEnabled(2)) {
                    String string;
                    if (attachedToWindow) {
                        string = "";
                    }
                    else {
                        final StringBuilder sb = new StringBuilder();
                        sb.append("Container ");
                        sb.append(this.container);
                        sb.append(" is not attached to window. ");
                        string = sb.toString();
                    }
                    final StringBuilder sb2 = new StringBuilder();
                    sb2.append("SpecialEffectsController: ");
                    sb2.append(string);
                    sb2.append("Cancelling running operation ");
                    sb2.append(obj);
                    Log.v("FragmentManager", sb2.toString());
                }
                obj.cancel();
            }
            for (final Operation obj2 : CollectionsKt.toMutableList((Collection)this.pendingOperations)) {
                if (FragmentManager.isLoggingEnabled(2)) {
                    String string2;
                    if (attachedToWindow) {
                        string2 = "";
                    }
                    else {
                        final StringBuilder sb3 = new StringBuilder();
                        sb3.append("Container ");
                        sb3.append(this.container);
                        sb3.append(" is not attached to window. ");
                        string2 = sb3.toString();
                    }
                    final StringBuilder sb4 = new StringBuilder();
                    sb4.append("SpecialEffectsController: ");
                    sb4.append(string2);
                    sb4.append("Cancelling pending operation ");
                    sb4.append(obj2);
                    Log.v("FragmentManager", sb4.toString());
                }
                obj2.cancel();
            }
            final Unit instance = Unit.INSTANCE;
        }
    }
    
    public final void forcePostponedExecutePendingOperations() {
        if (this.isContainerPostponed) {
            if (FragmentManager.isLoggingEnabled(2)) {
                Log.v("FragmentManager", "SpecialEffectsController: Forcing postponed operations");
            }
            this.isContainerPostponed = false;
            this.executePendingOperations();
        }
    }
    
    public final LifecycleImpact getAwaitingCompletionLifecycleImpact(final FragmentStateManager fragmentStateManager) {
        Intrinsics.checkNotNullParameter((Object)fragmentStateManager, "fragmentStateManager");
        final Fragment fragment = fragmentStateManager.getFragment();
        Intrinsics.checkNotNullExpressionValue((Object)fragment, "fragmentStateManager.fragment");
        final Operation pendingOperation = this.findPendingOperation(fragment);
        Enum<LifecycleImpact> lifecycleImpact = null;
        Enum<LifecycleImpact> lifecycleImpact2;
        if (pendingOperation != null) {
            lifecycleImpact2 = pendingOperation.getLifecycleImpact();
        }
        else {
            lifecycleImpact2 = null;
        }
        final Operation runningOperation = this.findRunningOperation(fragment);
        if (runningOperation != null) {
            lifecycleImpact = runningOperation.getLifecycleImpact();
        }
        int n;
        if (lifecycleImpact2 == null) {
            n = -1;
        }
        else {
            n = WhenMappings.$EnumSwitchMapping$0[lifecycleImpact2.ordinal()];
        }
        if (n == -1 || n == 1) {
            lifecycleImpact2 = lifecycleImpact;
        }
        return (LifecycleImpact)lifecycleImpact2;
    }
    
    public final ViewGroup getContainer() {
        return this.container;
    }
    
    public final void markPostponedState() {
        synchronized (this.pendingOperations) {
            this.updateFinalState();
            final List<Operation> pendingOperations = this.pendingOperations;
            final ListIterator<Operation> listIterator = pendingOperations.listIterator(pendingOperations.size());
            while (true) {
                Operation operation;
                State operationState;
                do {
                    final boolean hasPrevious = listIterator.hasPrevious();
                    boolean postponed = false;
                    final Fragment fragment = null;
                    if (!hasPrevious) {
                        final Operation previous = null;
                        final Operation operation2 = previous;
                        Fragment fragment2 = fragment;
                        if (operation2 != null) {
                            fragment2 = operation2.getFragment();
                        }
                        if (fragment2 != null) {
                            postponed = fragment2.isPostponed();
                        }
                        this.isContainerPostponed = postponed;
                        final Unit instance = Unit.INSTANCE;
                        return;
                    }
                    final Operation previous = listIterator.previous();
                    operation = previous;
                    final State.Companion companion = State.Companion;
                    final View mView = operation.getFragment().mView;
                    Intrinsics.checkNotNullExpressionValue((Object)mView, "operation.fragment.mView");
                    operationState = companion.asOperationState(mView);
                } while (operation.getFinalState() != State.VISIBLE || operationState == State.VISIBLE);
                continue;
            }
        }
    }
    
    public final void updateOperationDirection(final boolean operationDirectionIsPop) {
        this.operationDirectionIsPop = operationDirectionIsPop;
    }
    
    @Metadata(d1 = { "\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0018\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bH\u0007J\u0018\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\t\u001a\u00020\nH\u0007¨\u0006\u000b" }, d2 = { "Landroidx/fragment/app/SpecialEffectsController$Companion;", "", "()V", "getOrCreateController", "Landroidx/fragment/app/SpecialEffectsController;", "container", "Landroid/view/ViewGroup;", "fragmentManager", "Landroidx/fragment/app/FragmentManager;", "factory", "Landroidx/fragment/app/SpecialEffectsControllerFactory;", "fragment_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
    public static final class Companion
    {
        private Companion() {
        }
        
        @JvmStatic
        public final SpecialEffectsController getOrCreateController(final ViewGroup viewGroup, final FragmentManager fragmentManager) {
            Intrinsics.checkNotNullParameter((Object)viewGroup, "container");
            Intrinsics.checkNotNullParameter((Object)fragmentManager, "fragmentManager");
            final SpecialEffectsControllerFactory specialEffectsControllerFactory = fragmentManager.getSpecialEffectsControllerFactory();
            Intrinsics.checkNotNullExpressionValue((Object)specialEffectsControllerFactory, "fragmentManager.specialEffectsControllerFactory");
            return this.getOrCreateController(viewGroup, specialEffectsControllerFactory);
        }
        
        @JvmStatic
        public final SpecialEffectsController getOrCreateController(final ViewGroup viewGroup, final SpecialEffectsControllerFactory specialEffectsControllerFactory) {
            Intrinsics.checkNotNullParameter((Object)viewGroup, "container");
            Intrinsics.checkNotNullParameter((Object)specialEffectsControllerFactory, "factory");
            final Object tag = viewGroup.getTag(R.id.special_effects_controller_view_tag);
            if (tag instanceof SpecialEffectsController) {
                return (SpecialEffectsController)tag;
            }
            final SpecialEffectsController controller = specialEffectsControllerFactory.createController(viewGroup);
            Intrinsics.checkNotNullExpressionValue((Object)controller, "factory.createController(container)");
            viewGroup.setTag(R.id.special_effects_controller_view_tag, (Object)controller);
            return controller;
        }
    }
    
    @Metadata(d1 = { "\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\b\u0002\u0018\u00002\u00020\u0001B%\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\t¢\u0006\u0002\u0010\nJ\b\u0010\u000b\u001a\u00020\fH\u0016J\b\u0010\r\u001a\u00020\fH\u0016R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u000e" }, d2 = { "Landroidx/fragment/app/SpecialEffectsController$FragmentStateManagerOperation;", "Landroidx/fragment/app/SpecialEffectsController$Operation;", "finalState", "Landroidx/fragment/app/SpecialEffectsController$Operation$State;", "lifecycleImpact", "Landroidx/fragment/app/SpecialEffectsController$Operation$LifecycleImpact;", "fragmentStateManager", "Landroidx/fragment/app/FragmentStateManager;", "cancellationSignal", "Landroidx/core/os/CancellationSignal;", "(Landroidx/fragment/app/SpecialEffectsController$Operation$State;Landroidx/fragment/app/SpecialEffectsController$Operation$LifecycleImpact;Landroidx/fragment/app/FragmentStateManager;Landroidx/core/os/CancellationSignal;)V", "complete", "", "onStart", "fragment_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
    private static final class FragmentStateManagerOperation extends Operation
    {
        private final FragmentStateManager fragmentStateManager;
        
        public FragmentStateManagerOperation(final State state, final LifecycleImpact lifecycleImpact, final FragmentStateManager fragmentStateManager, final CancellationSignal cancellationSignal) {
            Intrinsics.checkNotNullParameter((Object)state, "finalState");
            Intrinsics.checkNotNullParameter((Object)lifecycleImpact, "lifecycleImpact");
            Intrinsics.checkNotNullParameter((Object)fragmentStateManager, "fragmentStateManager");
            Intrinsics.checkNotNullParameter((Object)cancellationSignal, "cancellationSignal");
            final Fragment fragment = fragmentStateManager.getFragment();
            Intrinsics.checkNotNullExpressionValue((Object)fragment, "fragmentStateManager.fragment");
            super(state, lifecycleImpact, fragment, cancellationSignal);
            this.fragmentStateManager = fragmentStateManager;
        }
        
        @Override
        public void complete() {
            super.complete();
            this.fragmentStateManager.moveToExpectedState();
        }
        
        @Override
        public void onStart() {
            if (((Operation)this).getLifecycleImpact() == LifecycleImpact.ADDING) {
                final Fragment fragment = this.fragmentStateManager.getFragment();
                Intrinsics.checkNotNullExpressionValue((Object)fragment, "fragmentStateManager.fragment");
                final View focus = fragment.mView.findFocus();
                if (focus != null) {
                    fragment.setFocusedView(focus);
                    if (FragmentManager.isLoggingEnabled(2)) {
                        final StringBuilder sb = new StringBuilder();
                        sb.append("requestFocus: Saved focused view ");
                        sb.append(focus);
                        sb.append(" for Fragment ");
                        sb.append(fragment);
                        Log.v("FragmentManager", sb.toString());
                    }
                }
                final View requireView = ((Operation)this).getFragment().requireView();
                Intrinsics.checkNotNullExpressionValue((Object)requireView, "this.fragment.requireView()");
                if (requireView.getParent() == null) {
                    this.fragmentStateManager.addViewToContainer();
                    requireView.setAlpha(0.0f);
                }
                if (requireView.getAlpha() == 0.0f && requireView.getVisibility() == 0) {
                    requireView.setVisibility(4);
                }
                requireView.setAlpha(fragment.getPostOnViewCreatedAlpha());
            }
            else if (((Operation)this).getLifecycleImpact() == LifecycleImpact.REMOVING) {
                final Fragment fragment2 = this.fragmentStateManager.getFragment();
                Intrinsics.checkNotNullExpressionValue((Object)fragment2, "fragmentStateManager.fragment");
                final View requireView2 = fragment2.requireView();
                Intrinsics.checkNotNullExpressionValue((Object)requireView2, "fragment.requireView()");
                if (FragmentManager.isLoggingEnabled(2)) {
                    final StringBuilder sb2 = new StringBuilder();
                    sb2.append("Clearing focus ");
                    sb2.append(requireView2.findFocus());
                    sb2.append(" on view ");
                    sb2.append(requireView2);
                    sb2.append(" for Fragment ");
                    sb2.append(fragment2);
                    Log.v("FragmentManager", sb2.toString());
                }
                requireView2.clearFocus();
            }
        }
    }
    
    @Metadata(d1 = { "\u0000N\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\u000b\n\u0002\b\b\n\u0002\u0010#\n\u0000\n\u0002\u0010\u0002\n\u0002\b\t\n\u0002\u0010\u000e\n\u0002\b\u0003\b\u0010\u0018\u00002\u00020\u0001:\u0002+,B%\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\t¢\u0006\u0002\u0010\nJ\u000e\u0010\u001f\u001a\u00020 2\u0006\u0010!\u001a\u00020\rJ\u0006\u0010\"\u001a\u00020 J\b\u0010#\u001a\u00020 H\u0017J\u000e\u0010$\u001a\u00020 2\u0006\u0010%\u001a\u00020\tJ\u000e\u0010&\u001a\u00020 2\u0006\u0010%\u001a\u00020\tJ\u0016\u0010'\u001a\u00020 2\u0006\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005J\b\u0010(\u001a\u00020 H\u0016J\b\u0010)\u001a\u00020*H\u0016R\u0014\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\r0\fX\u0082\u0004¢\u0006\u0002\n\u0000R\u001a\u0010\u0002\u001a\u00020\u0003X\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u000e\u0010\u000f\"\u0004\b\u0010\u0010\u0011R\u0011\u0010\u0006\u001a\u00020\u0007¢\u0006\b\n\u0000\u001a\u0004\b\u0012\u0010\u0013R\u001e\u0010\u0016\u001a\u00020\u00152\u0006\u0010\u0014\u001a\u00020\u0015@BX\u0086\u000e¢\u0006\b\n\u0000\u001a\u0004\b\u0016\u0010\u0017R\u001e\u0010\u0018\u001a\u00020\u00152\u0006\u0010\u0014\u001a\u00020\u0015@BX\u0086\u000e¢\u0006\b\n\u0000\u001a\u0004\b\u0018\u0010\u0017R\u001a\u0010\u0004\u001a\u00020\u0005X\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0019\u0010\u001a\"\u0004\b\u001b\u0010\u001cR\u0014\u0010\u001d\u001a\b\u0012\u0004\u0012\u00020\t0\u001eX\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006-" }, d2 = { "Landroidx/fragment/app/SpecialEffectsController$Operation;", "", "finalState", "Landroidx/fragment/app/SpecialEffectsController$Operation$State;", "lifecycleImpact", "Landroidx/fragment/app/SpecialEffectsController$Operation$LifecycleImpact;", "fragment", "Landroidx/fragment/app/Fragment;", "cancellationSignal", "Landroidx/core/os/CancellationSignal;", "(Landroidx/fragment/app/SpecialEffectsController$Operation$State;Landroidx/fragment/app/SpecialEffectsController$Operation$LifecycleImpact;Landroidx/fragment/app/Fragment;Landroidx/core/os/CancellationSignal;)V", "completionListeners", "", "Ljava/lang/Runnable;", "getFinalState", "()Landroidx/fragment/app/SpecialEffectsController$Operation$State;", "setFinalState", "(Landroidx/fragment/app/SpecialEffectsController$Operation$State;)V", "getFragment", "()Landroidx/fragment/app/Fragment;", "<set-?>", "", "isCanceled", "()Z", "isComplete", "getLifecycleImpact", "()Landroidx/fragment/app/SpecialEffectsController$Operation$LifecycleImpact;", "setLifecycleImpact", "(Landroidx/fragment/app/SpecialEffectsController$Operation$LifecycleImpact;)V", "specialEffectsSignals", "", "addCompletionListener", "", "listener", "cancel", "complete", "completeSpecialEffect", "signal", "markStartedSpecialEffect", "mergeWith", "onStart", "toString", "", "LifecycleImpact", "State", "fragment_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
    public static class Operation
    {
        private final List<Runnable> completionListeners;
        private State finalState;
        private final Fragment fragment;
        private boolean isCanceled;
        private boolean isComplete;
        private LifecycleImpact lifecycleImpact;
        private final Set<CancellationSignal> specialEffectsSignals;
        
        public Operation(final State finalState, final LifecycleImpact lifecycleImpact, final Fragment fragment, final CancellationSignal cancellationSignal) {
            Intrinsics.checkNotNullParameter((Object)finalState, "finalState");
            Intrinsics.checkNotNullParameter((Object)lifecycleImpact, "lifecycleImpact");
            Intrinsics.checkNotNullParameter((Object)fragment, "fragment");
            Intrinsics.checkNotNullParameter((Object)cancellationSignal, "cancellationSignal");
            this.finalState = finalState;
            this.lifecycleImpact = lifecycleImpact;
            this.fragment = fragment;
            this.completionListeners = new ArrayList<Runnable>();
            this.specialEffectsSignals = new LinkedHashSet<CancellationSignal>();
            cancellationSignal.setOnCancelListener((CancellationSignal.OnCancelListener)new SpecialEffectsController$Operation$$ExternalSyntheticLambda0(this));
        }
        
        private static final void _init_$lambda$0(final Operation operation) {
            Intrinsics.checkNotNullParameter((Object)operation, "this$0");
            operation.cancel();
        }
        
        public final void addCompletionListener(final Runnable runnable) {
            Intrinsics.checkNotNullParameter((Object)runnable, "listener");
            this.completionListeners.add(runnable);
        }
        
        public final void cancel() {
            if (this.isCanceled) {
                return;
            }
            this.isCanceled = true;
            if (this.specialEffectsSignals.isEmpty()) {
                this.complete();
            }
            else {
                final Iterator iterator = CollectionsKt.toMutableSet((Iterable)this.specialEffectsSignals).iterator();
                while (iterator.hasNext()) {
                    ((CancellationSignal)iterator.next()).cancel();
                }
            }
        }
        
        public void complete() {
            if (this.isComplete) {
                return;
            }
            if (FragmentManager.isLoggingEnabled(2)) {
                final StringBuilder sb = new StringBuilder();
                sb.append("SpecialEffectsController: ");
                sb.append(this);
                sb.append(" has called complete.");
                Log.v("FragmentManager", sb.toString());
            }
            this.isComplete = true;
            final Iterator iterator = this.completionListeners.iterator();
            while (iterator.hasNext()) {
                ((Runnable)iterator.next()).run();
            }
        }
        
        public final void completeSpecialEffect(final CancellationSignal cancellationSignal) {
            Intrinsics.checkNotNullParameter((Object)cancellationSignal, "signal");
            if (this.specialEffectsSignals.remove(cancellationSignal) && this.specialEffectsSignals.isEmpty()) {
                this.complete();
            }
        }
        
        public final State getFinalState() {
            return this.finalState;
        }
        
        public final Fragment getFragment() {
            return this.fragment;
        }
        
        public final LifecycleImpact getLifecycleImpact() {
            return this.lifecycleImpact;
        }
        
        public final boolean isCanceled() {
            return this.isCanceled;
        }
        
        public final boolean isComplete() {
            return this.isComplete;
        }
        
        public final void markStartedSpecialEffect(final CancellationSignal cancellationSignal) {
            Intrinsics.checkNotNullParameter((Object)cancellationSignal, "signal");
            this.onStart();
            this.specialEffectsSignals.add(cancellationSignal);
        }
        
        public final void mergeWith(final State state, final LifecycleImpact lifecycleImpact) {
            Intrinsics.checkNotNullParameter((Object)state, "finalState");
            Intrinsics.checkNotNullParameter((Object)lifecycleImpact, "lifecycleImpact");
            final int n = WhenMappings.$EnumSwitchMapping$0[lifecycleImpact.ordinal()];
            if (n != 1) {
                if (n != 2) {
                    if (n == 3) {
                        if (this.finalState != State.REMOVED) {
                            if (FragmentManager.isLoggingEnabled(2)) {
                                final StringBuilder sb = new StringBuilder();
                                sb.append("SpecialEffectsController: For fragment ");
                                sb.append(this.fragment);
                                sb.append(" mFinalState = ");
                                sb.append(this.finalState);
                                sb.append(" -> ");
                                sb.append(state);
                                sb.append('.');
                                Log.v("FragmentManager", sb.toString());
                            }
                            this.finalState = state;
                        }
                    }
                }
                else {
                    if (FragmentManager.isLoggingEnabled(2)) {
                        final StringBuilder sb2 = new StringBuilder();
                        sb2.append("SpecialEffectsController: For fragment ");
                        sb2.append(this.fragment);
                        sb2.append(" mFinalState = ");
                        sb2.append(this.finalState);
                        sb2.append(" -> REMOVED. mLifecycleImpact  = ");
                        sb2.append(this.lifecycleImpact);
                        sb2.append(" to REMOVING.");
                        Log.v("FragmentManager", sb2.toString());
                    }
                    this.finalState = State.REMOVED;
                    this.lifecycleImpact = LifecycleImpact.REMOVING;
                }
            }
            else if (this.finalState == State.REMOVED) {
                if (FragmentManager.isLoggingEnabled(2)) {
                    final StringBuilder sb3 = new StringBuilder();
                    sb3.append("SpecialEffectsController: For fragment ");
                    sb3.append(this.fragment);
                    sb3.append(" mFinalState = REMOVED -> VISIBLE. mLifecycleImpact = ");
                    sb3.append(this.lifecycleImpact);
                    sb3.append(" to ADDING.");
                    Log.v("FragmentManager", sb3.toString());
                }
                this.finalState = State.VISIBLE;
                this.lifecycleImpact = LifecycleImpact.ADDING;
            }
        }
        
        public void onStart() {
        }
        
        public final void setFinalState(final State finalState) {
            Intrinsics.checkNotNullParameter((Object)finalState, "<set-?>");
            this.finalState = finalState;
        }
        
        public final void setLifecycleImpact(final LifecycleImpact lifecycleImpact) {
            Intrinsics.checkNotNullParameter((Object)lifecycleImpact, "<set-?>");
            this.lifecycleImpact = lifecycleImpact;
        }
        
        @Override
        public String toString() {
            final String hexString = Integer.toHexString(System.identityHashCode(this));
            final StringBuilder sb = new StringBuilder();
            sb.append("Operation {");
            sb.append(hexString);
            sb.append("} {finalState = ");
            sb.append(this.finalState);
            sb.append(" lifecycleImpact = ");
            sb.append(this.lifecycleImpact);
            sb.append(" fragment = ");
            sb.append(this.fragment);
            sb.append('}');
            return sb.toString();
        }
        
        @Metadata(d1 = { "\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0005\b\u0080\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002j\u0002\b\u0003j\u0002\b\u0004j\u0002\b\u0005¨\u0006\u0006" }, d2 = { "Landroidx/fragment/app/SpecialEffectsController$Operation$LifecycleImpact;", "", "(Ljava/lang/String;I)V", "NONE", "ADDING", "REMOVING", "fragment_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
        public enum LifecycleImpact
        {
            private static final LifecycleImpact[] $VALUES;
            
            ADDING, 
            NONE, 
            REMOVING;
            
            private static final /* synthetic */ LifecycleImpact[] $values() {
                return new LifecycleImpact[] { LifecycleImpact.NONE, LifecycleImpact.ADDING, LifecycleImpact.REMOVING };
            }
            
            static {
                $VALUES = $values();
            }
        }
        
        @Metadata(d1 = { "\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\b\u0080\u0001\u0018\u0000 \u000b2\b\u0012\u0004\u0012\u00020\u00000\u0001:\u0001\u000bB\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u000e\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006j\u0002\b\u0007j\u0002\b\bj\u0002\b\tj\u0002\b\n¨\u0006\f" }, d2 = { "Landroidx/fragment/app/SpecialEffectsController$Operation$State;", "", "(Ljava/lang/String;I)V", "applyState", "", "view", "Landroid/view/View;", "REMOVED", "VISIBLE", "GONE", "INVISIBLE", "Companion", "fragment_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
        public enum State
        {
            private static final State[] $VALUES;
            public static final Companion Companion;
            
            GONE, 
            INVISIBLE, 
            REMOVED, 
            VISIBLE;
            
            private static final /* synthetic */ State[] $values() {
                return new State[] { State.REMOVED, State.VISIBLE, State.GONE, State.INVISIBLE };
            }
            
            static {
                $VALUES = $values();
                Companion = new Companion(null);
            }
            
            @JvmStatic
            public static final State from(final int n) {
                return State.Companion.from(n);
            }
            
            public final void applyState(final View view) {
                Intrinsics.checkNotNullParameter((Object)view, "view");
                final int n = WhenMappings.$EnumSwitchMapping$0[this.ordinal()];
                if (n != 1) {
                    if (n != 2) {
                        if (n != 3) {
                            if (n == 4) {
                                if (FragmentManager.isLoggingEnabled(2)) {
                                    final StringBuilder sb = new StringBuilder();
                                    sb.append("SpecialEffectsController: Setting view ");
                                    sb.append(view);
                                    sb.append(" to INVISIBLE");
                                    Log.v("FragmentManager", sb.toString());
                                }
                                view.setVisibility(4);
                            }
                        }
                        else {
                            if (FragmentManager.isLoggingEnabled(2)) {
                                final StringBuilder sb2 = new StringBuilder();
                                sb2.append("SpecialEffectsController: Setting view ");
                                sb2.append(view);
                                sb2.append(" to GONE");
                                Log.v("FragmentManager", sb2.toString());
                            }
                            view.setVisibility(8);
                        }
                    }
                    else {
                        if (FragmentManager.isLoggingEnabled(2)) {
                            final StringBuilder sb3 = new StringBuilder();
                            sb3.append("SpecialEffectsController: Setting view ");
                            sb3.append(view);
                            sb3.append(" to VISIBLE");
                            Log.v("FragmentManager", sb3.toString());
                        }
                        view.setVisibility(0);
                    }
                }
                else {
                    final ViewParent parent = view.getParent();
                    ViewGroup obj;
                    if (parent instanceof ViewGroup) {
                        obj = (ViewGroup)parent;
                    }
                    else {
                        obj = null;
                    }
                    if (obj != null) {
                        if (FragmentManager.isLoggingEnabled(2)) {
                            final StringBuilder sb4 = new StringBuilder();
                            sb4.append("SpecialEffectsController: Removing view ");
                            sb4.append(view);
                            sb4.append(" from container ");
                            sb4.append(obj);
                            Log.v("FragmentManager", sb4.toString());
                        }
                        obj.removeView(view);
                    }
                }
            }
            
            @Metadata(d1 = { "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0007J\n\u0010\u0007\u001a\u00020\u0004*\u00020\b¨\u0006\t" }, d2 = { "Landroidx/fragment/app/SpecialEffectsController$Operation$State$Companion;", "", "()V", "from", "Landroidx/fragment/app/SpecialEffectsController$Operation$State;", "visibility", "", "asOperationState", "Landroid/view/View;", "fragment_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
            public static final class Companion
            {
                private Companion() {
                }
                
                public final State asOperationState(final View view) {
                    Intrinsics.checkNotNullParameter((Object)view, "<this>");
                    State state;
                    if (view.getAlpha() == 0.0f && view.getVisibility() == 0) {
                        state = State.INVISIBLE;
                    }
                    else {
                        state = this.from(view.getVisibility());
                    }
                    return state;
                }
                
                @JvmStatic
                public final State from(final int i) {
                    State state;
                    if (i != 0) {
                        if (i != 4) {
                            if (i != 8) {
                                final StringBuilder sb = new StringBuilder();
                                sb.append("Unknown visibility ");
                                sb.append(i);
                                throw new IllegalArgumentException(sb.toString());
                            }
                            state = State.GONE;
                        }
                        else {
                            state = State.INVISIBLE;
                        }
                    }
                    else {
                        state = State.VISIBLE;
                    }
                    return state;
                }
            }
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.fragment.app;

import androidx.core.view.OneShotPreDrawListener;
import android.view.animation.Transformation;
import android.view.ViewGroup;
import android.view.View;
import android.view.animation.AnimationSet;
import android.content.res.TypedArray;
import android.animation.Animator;
import android.view.animation.Animation;
import android.animation.AnimatorInflater;
import android.content.res.Resources$NotFoundException;
import android.view.animation.AnimationUtils;
import androidx.fragment.R;
import android.content.Context;

class FragmentAnim
{
    private FragmentAnim() {
    }
    
    private static int getNextAnim(final Fragment fragment, final boolean b, final boolean b2) {
        if (b2) {
            if (b) {
                return fragment.getPopEnterAnim();
            }
            return fragment.getPopExitAnim();
        }
        else {
            if (b) {
                return fragment.getEnterAnim();
            }
            return fragment.getExitAnim();
        }
    }
    
    static AnimationOrAnimator loadAnimation(final Context context, final Fragment fragment, boolean equals, final boolean b) {
        final int nextTransition = fragment.getNextTransition();
        final int nextAnim = getNextAnim(fragment, equals, b);
        final boolean b2 = false;
        fragment.setAnimations(0, 0, 0, 0);
        if (fragment.mContainer != null && fragment.mContainer.getTag(R.id.visible_removing_fragment_view_tag) != null) {
            fragment.mContainer.setTag(R.id.visible_removing_fragment_view_tag, (Object)null);
        }
        if (fragment.mContainer != null && fragment.mContainer.getLayoutTransition() != null) {
            return null;
        }
        final Animation onCreateAnimation = fragment.onCreateAnimation(nextTransition, equals, nextAnim);
        if (onCreateAnimation != null) {
            return new AnimationOrAnimator(onCreateAnimation);
        }
        final Animator onCreateAnimator = fragment.onCreateAnimator(nextTransition, equals, nextAnim);
        if (onCreateAnimator != null) {
            return new AnimationOrAnimator(onCreateAnimator);
        }
        int transitToAnimResourceId;
        if ((transitToAnimResourceId = nextAnim) == 0) {
            transitToAnimResourceId = nextAnim;
            if (nextTransition != 0) {
                transitToAnimResourceId = transitToAnimResourceId(context, nextTransition, equals);
            }
        }
        if (transitToAnimResourceId != 0) {
            equals = "anim".equals(context.getResources().getResourceTypeName(transitToAnimResourceId));
            int n = b2 ? 1 : 0;
            if (equals) {
                try {
                    final Animation loadAnimation = AnimationUtils.loadAnimation(context, transitToAnimResourceId);
                    if (loadAnimation != null) {
                        return new AnimationOrAnimator(loadAnimation);
                    }
                    n = 1;
                }
                catch (final RuntimeException ex) {
                    n = (b2 ? 1 : 0);
                }
                catch (final Resources$NotFoundException ex2) {
                    throw ex2;
                }
            }
            if (n == 0) {
                try {
                    final Animator loadAnimator = AnimatorInflater.loadAnimator(context, transitToAnimResourceId);
                    if (loadAnimator != null) {
                        return new AnimationOrAnimator(loadAnimator);
                    }
                }
                catch (final RuntimeException ex3) {
                    if (equals) {
                        throw ex3;
                    }
                    final Animation loadAnimation2 = AnimationUtils.loadAnimation(context, transitToAnimResourceId);
                    if (loadAnimation2 != null) {
                        return new AnimationOrAnimator(loadAnimation2);
                    }
                }
            }
        }
        return null;
    }
    
    private static int toActivityTransitResId(final Context context, int resourceId) {
        final TypedArray obtainStyledAttributes = context.obtainStyledAttributes(16973825, new int[] { resourceId });
        resourceId = obtainStyledAttributes.getResourceId(0, -1);
        obtainStyledAttributes.recycle();
        return resourceId;
    }
    
    private static int transitToAnimResourceId(final Context context, int n, final boolean b) {
        if (n != 4097) {
            if (n != 8194) {
                if (n != 8197) {
                    if (n != 4099) {
                        if (n != 4100) {
                            n = -1;
                        }
                        else if (b) {
                            n = toActivityTransitResId(context, 16842936);
                        }
                        else {
                            n = toActivityTransitResId(context, 16842937);
                        }
                    }
                    else if (b) {
                        n = R.animator.fragment_fade_enter;
                    }
                    else {
                        n = R.animator.fragment_fade_exit;
                    }
                }
                else if (b) {
                    n = toActivityTransitResId(context, 16842938);
                }
                else {
                    n = toActivityTransitResId(context, 16842939);
                }
            }
            else if (b) {
                n = R.animator.fragment_close_enter;
            }
            else {
                n = R.animator.fragment_close_exit;
            }
        }
        else if (b) {
            n = R.animator.fragment_open_enter;
        }
        else {
            n = R.animator.fragment_open_exit;
        }
        return n;
    }
    
    static class AnimationOrAnimator
    {
        public final Animation animation;
        public final Animator animator;
        
        AnimationOrAnimator(final Animator animator) {
            this.animation = null;
            this.animator = animator;
            if (animator != null) {
                return;
            }
            throw new IllegalStateException("Animator cannot be null");
        }
        
        AnimationOrAnimator(final Animation animation) {
            this.animation = animation;
            this.animator = null;
            if (animation != null) {
                return;
            }
            throw new IllegalStateException("Animation cannot be null");
        }
    }
    
    static class EndViewTransitionAnimation extends AnimationSet implements Runnable
    {
        private boolean mAnimating;
        private final View mChild;
        private boolean mEnded;
        private final ViewGroup mParent;
        private boolean mTransitionEnded;
        
        EndViewTransitionAnimation(final Animation animation, final ViewGroup mParent, final View mChild) {
            super(false);
            this.mAnimating = true;
            this.mParent = mParent;
            this.mChild = mChild;
            this.addAnimation(animation);
            mParent.post((Runnable)this);
        }
        
        public boolean getTransformation(final long n, final Transformation transformation) {
            this.mAnimating = true;
            if (this.mEnded) {
                return this.mTransitionEnded ^ true;
            }
            if (!super.getTransformation(n, transformation)) {
                this.mEnded = true;
                OneShotPreDrawListener.add((View)this.mParent, this);
            }
            return true;
        }
        
        public boolean getTransformation(final long n, final Transformation transformation, final float n2) {
            this.mAnimating = true;
            if (this.mEnded) {
                return this.mTransitionEnded ^ true;
            }
            if (!super.getTransformation(n, transformation, n2)) {
                this.mEnded = true;
                OneShotPreDrawListener.add((View)this.mParent, this);
            }
            return true;
        }
        
        public void run() {
            if (!this.mEnded && this.mAnimating) {
                this.mAnimating = false;
                this.mParent.post((Runnable)this);
            }
            else {
                this.mParent.endViewTransition(this.mChild);
                this.mTransitionEnded = true;
            }
        }
    }
}

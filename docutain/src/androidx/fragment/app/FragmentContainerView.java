// 
// Decompiled by Procyon v0.6.0
// 

package androidx.fragment.app;

import android.os.Build$VERSION;
import android.animation.LayoutTransition;
import java.util.Collection;
import java.util.Iterator;
import android.graphics.Canvas;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;
import android.view.WindowInsets;
import android.view.ViewGroup$LayoutParams;
import android.view.ViewGroup;
import android.os.Bundle;
import android.content.res.TypedArray;
import androidx.fragment.R;
import kotlin.jvm.internal.DefaultConstructorMarker;
import android.util.AttributeSet;
import java.util.ArrayList;
import kotlin.jvm.internal.Intrinsics;
import android.content.Context;
import android.view.View;
import java.util.List;
import android.view.View$OnApplyWindowInsetsListener;
import kotlin.Metadata;
import android.widget.FrameLayout;

@Metadata(d1 = { "\u0000v\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\r\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u0001:\u0001<B\u000f\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004B#\b\u0017\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\b\u0010\u0005\u001a\u0004\u0018\u00010\u0006\u0012\b\b\u0002\u0010\u0007\u001a\u00020\b¢\u0006\u0002\u0010\tB\u001f\b\u0010\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\n\u001a\u00020\u000b¢\u0006\u0002\u0010\fJ\u0010\u0010\u0015\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\u0011H\u0002J\"\u0010\u0018\u001a\u00020\u00162\u0006\u0010\u0019\u001a\u00020\u00112\u0006\u0010\u001a\u001a\u00020\b2\b\u0010\u001b\u001a\u0004\u0018\u00010\u001cH\u0016J\u0010\u0010\u001d\u001a\u00020\u001e2\u0006\u0010\u001f\u001a\u00020\u001eH\u0017J\u0010\u0010 \u001a\u00020\u00162\u0006\u0010!\u001a\u00020\"H\u0014J \u0010#\u001a\u00020\u00132\u0006\u0010!\u001a\u00020\"2\u0006\u0010\u0019\u001a\u00020\u00112\u0006\u0010$\u001a\u00020%H\u0014J\u0010\u0010&\u001a\u00020\u00162\u0006\u0010'\u001a\u00020\u0011H\u0016J\u0017\u0010(\u001a\u0002H)\"\n\b\u0000\u0010)*\u0004\u0018\u00010*¢\u0006\u0002\u0010+J\u0010\u0010,\u001a\u00020\u001e2\u0006\u0010\u001f\u001a\u00020\u001eH\u0017J\b\u0010-\u001a\u00020\u0016H\u0016J\u0010\u0010.\u001a\u00020\u00162\u0006\u0010'\u001a\u00020\u0011H\u0016J\u0010\u0010/\u001a\u00020\u00162\u0006\u0010\u001a\u001a\u00020\bH\u0016J\u0010\u00100\u001a\u00020\u00162\u0006\u0010'\u001a\u00020\u0011H\u0016J\u0018\u00101\u001a\u00020\u00162\u0006\u00102\u001a\u00020\b2\u0006\u00103\u001a\u00020\bH\u0016J\u0018\u00104\u001a\u00020\u00162\u0006\u00102\u001a\u00020\b2\u0006\u00103\u001a\u00020\bH\u0016J\u0010\u00105\u001a\u00020\u00162\u0006\u0010\u0012\u001a\u00020\u0013H\u0001J\u0012\u00106\u001a\u00020\u00162\b\u00107\u001a\u0004\u0018\u000108H\u0016J\u0010\u00109\u001a\u00020\u00162\u0006\u0010:\u001a\u00020\u000eH\u0016J\u0010\u0010;\u001a\u00020\u00162\u0006\u0010'\u001a\u00020\u0011H\u0016R\u0010\u0010\r\u001a\u0004\u0018\u00010\u000eX\u0082\u000e¢\u0006\u0002\n\u0000R\u0014\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u00110\u0010X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0013X\u0082\u000e¢\u0006\u0002\n\u0000R\u0014\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\u00110\u0010X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006=" }, d2 = { "Landroidx/fragment/app/FragmentContainerView;", "Landroid/widget/FrameLayout;", "context", "Landroid/content/Context;", "(Landroid/content/Context;)V", "attrs", "Landroid/util/AttributeSet;", "defStyleAttr", "", "(Landroid/content/Context;Landroid/util/AttributeSet;I)V", "fm", "Landroidx/fragment/app/FragmentManager;", "(Landroid/content/Context;Landroid/util/AttributeSet;Landroidx/fragment/app/FragmentManager;)V", "applyWindowInsetsListener", "Landroid/view/View$OnApplyWindowInsetsListener;", "disappearingFragmentChildren", "", "Landroid/view/View;", "drawDisappearingViewsFirst", "", "transitioningFragmentViews", "addDisappearingFragmentView", "", "v", "addView", "child", "index", "params", "Landroid/view/ViewGroup$LayoutParams;", "dispatchApplyWindowInsets", "Landroid/view/WindowInsets;", "insets", "dispatchDraw", "canvas", "Landroid/graphics/Canvas;", "drawChild", "drawingTime", "", "endViewTransition", "view", "getFragment", "F", "Landroidx/fragment/app/Fragment;", "()Landroidx/fragment/app/Fragment;", "onApplyWindowInsets", "removeAllViewsInLayout", "removeView", "removeViewAt", "removeViewInLayout", "removeViews", "start", "count", "removeViewsInLayout", "setDrawDisappearingViewsLast", "setLayoutTransition", "transition", "Landroid/animation/LayoutTransition;", "setOnApplyWindowInsetsListener", "listener", "startViewTransition", "Api20Impl", "fragment_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
public final class FragmentContainerView extends FrameLayout
{
    private View$OnApplyWindowInsetsListener applyWindowInsetsListener;
    private final List<View> disappearingFragmentChildren;
    private boolean drawDisappearingViewsFirst;
    private final List<View> transitioningFragmentViews;
    
    public FragmentContainerView(final Context context) {
        Intrinsics.checkNotNullParameter((Object)context, "context");
        super(context);
        this.disappearingFragmentChildren = new ArrayList<View>();
        this.transitioningFragmentViews = new ArrayList<View>();
        this.drawDisappearingViewsFirst = true;
    }
    
    public FragmentContainerView(final Context context, final AttributeSet set) {
        Intrinsics.checkNotNullParameter((Object)context, "context");
        this(context, set, 0, 4, null);
    }
    
    public FragmentContainerView(final Context context, final AttributeSet set, final int n) {
        Intrinsics.checkNotNullParameter((Object)context, "context");
        super(context, set, n);
        this.disappearingFragmentChildren = new ArrayList<View>();
        this.transitioningFragmentViews = new ArrayList<View>();
        this.drawDisappearingViewsFirst = true;
        if (set != null) {
            final String classAttribute = set.getClassAttribute();
            final int[] fragmentContainerView = R.styleable.FragmentContainerView;
            Intrinsics.checkNotNullExpressionValue((Object)fragmentContainerView, "FragmentContainerView");
            final TypedArray obtainStyledAttributes = context.obtainStyledAttributes(set, fragmentContainerView, 0, 0);
            String string;
            String str;
            if (classAttribute == null) {
                string = obtainStyledAttributes.getString(R.styleable.FragmentContainerView_android_name);
                str = "android:name";
            }
            else {
                str = "class";
                string = classAttribute;
            }
            obtainStyledAttributes.recycle();
            if (string != null) {
                if (!this.isInEditMode()) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("FragmentContainerView must be within a FragmentActivity to use ");
                    sb.append(str);
                    sb.append("=\"");
                    sb.append(string);
                    sb.append('\"');
                    throw new UnsupportedOperationException(sb.toString());
                }
            }
        }
    }
    
    public FragmentContainerView(final Context context, final AttributeSet set, final FragmentManager fragmentManager) {
        Intrinsics.checkNotNullParameter((Object)context, "context");
        Intrinsics.checkNotNullParameter((Object)set, "attrs");
        Intrinsics.checkNotNullParameter((Object)fragmentManager, "fm");
        super(context, set);
        this.disappearingFragmentChildren = new ArrayList<View>();
        this.transitioningFragmentViews = new ArrayList<View>();
        this.drawDisappearingViewsFirst = true;
        final String classAttribute = set.getClassAttribute();
        final int[] fragmentContainerView = R.styleable.FragmentContainerView;
        Intrinsics.checkNotNullExpressionValue((Object)fragmentContainerView, "FragmentContainerView");
        final TypedArray obtainStyledAttributes = context.obtainStyledAttributes(set, fragmentContainerView, 0, 0);
        String string = classAttribute;
        if (classAttribute == null) {
            string = obtainStyledAttributes.getString(R.styleable.FragmentContainerView_android_name);
        }
        final String string2 = obtainStyledAttributes.getString(R.styleable.FragmentContainerView_android_tag);
        obtainStyledAttributes.recycle();
        final int id = this.getId();
        final Fragment fragmentById = fragmentManager.findFragmentById(id);
        if (string != null && fragmentById == null) {
            if (id == -1) {
                String string3;
                if (string2 != null) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append(" with tag ");
                    sb.append(string2);
                    string3 = sb.toString();
                }
                else {
                    string3 = "";
                }
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("FragmentContainerView must have an android:id to add Fragment ");
                sb2.append(string);
                sb2.append(string3);
                throw new IllegalStateException(sb2.toString());
            }
            final Fragment instantiate = fragmentManager.getFragmentFactory().instantiate(context.getClassLoader(), string);
            Intrinsics.checkNotNullExpressionValue((Object)instantiate, "fm.fragmentFactory.insta\u2026ontext.classLoader, name)");
            instantiate.onInflate(context, set, null);
            fragmentManager.beginTransaction().setReorderingAllowed(true).add((ViewGroup)this, instantiate, string2).commitNowAllowingStateLoss();
        }
        fragmentManager.onContainerAvailable(this);
    }
    
    private final void addDisappearingFragmentView(final View view) {
        if (this.transitioningFragmentViews.contains(view)) {
            this.disappearingFragmentChildren.add(view);
        }
    }
    
    public void addView(final View obj, final int n, final ViewGroup$LayoutParams viewGroup$LayoutParams) {
        Intrinsics.checkNotNullParameter((Object)obj, "child");
        if (FragmentManager.getViewFragment(obj) != null) {
            super.addView(obj, n, viewGroup$LayoutParams);
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Views added to a FragmentContainerView must be associated with a Fragment. View ");
        sb.append(obj);
        sb.append(" is not associated with a Fragment.");
        throw new IllegalStateException(sb.toString().toString());
    }
    
    public WindowInsets dispatchApplyWindowInsets(final WindowInsets windowInsets) {
        Intrinsics.checkNotNullParameter((Object)windowInsets, "insets");
        final WindowInsetsCompat windowInsetsCompat = WindowInsetsCompat.toWindowInsetsCompat(windowInsets);
        Intrinsics.checkNotNullExpressionValue((Object)windowInsetsCompat, "toWindowInsetsCompat(insets)");
        WindowInsetsCompat windowInsetsCompat2;
        if (this.applyWindowInsetsListener != null) {
            final Api20Impl instance = Api20Impl.INSTANCE;
            final View$OnApplyWindowInsetsListener applyWindowInsetsListener = this.applyWindowInsetsListener;
            Intrinsics.checkNotNull((Object)applyWindowInsetsListener);
            windowInsetsCompat2 = WindowInsetsCompat.toWindowInsetsCompat(instance.onApplyWindowInsets(applyWindowInsetsListener, (View)this, windowInsets));
        }
        else {
            windowInsetsCompat2 = ViewCompat.onApplyWindowInsets((View)this, windowInsetsCompat);
        }
        Intrinsics.checkNotNullExpressionValue((Object)windowInsetsCompat2, "if (applyWindowInsetsLis\u2026, insetsCompat)\n        }");
        if (!windowInsetsCompat2.isConsumed()) {
            for (int i = 0; i < this.getChildCount(); ++i) {
                ViewCompat.dispatchApplyWindowInsets(this.getChildAt(i), windowInsetsCompat2);
            }
        }
        return windowInsets;
    }
    
    protected void dispatchDraw(final Canvas canvas) {
        Intrinsics.checkNotNullParameter((Object)canvas, "canvas");
        if (this.drawDisappearingViewsFirst) {
            final Iterator iterator = this.disappearingFragmentChildren.iterator();
            while (iterator.hasNext()) {
                super.drawChild(canvas, (View)iterator.next(), this.getDrawingTime());
            }
        }
        super.dispatchDraw(canvas);
    }
    
    protected boolean drawChild(final Canvas canvas, final View view, final long n) {
        Intrinsics.checkNotNullParameter((Object)canvas, "canvas");
        Intrinsics.checkNotNullParameter((Object)view, "child");
        return (!this.drawDisappearingViewsFirst || !(this.disappearingFragmentChildren.isEmpty() ^ true) || !this.disappearingFragmentChildren.contains(view)) && super.drawChild(canvas, view, n);
    }
    
    public void endViewTransition(final View view) {
        Intrinsics.checkNotNullParameter((Object)view, "view");
        this.transitioningFragmentViews.remove(view);
        if (this.disappearingFragmentChildren.remove(view)) {
            this.drawDisappearingViewsFirst = true;
        }
        super.endViewTransition(view);
    }
    
    public final <F extends Fragment> F getFragment() {
        return (F)FragmentManager.findFragmentManager((View)this).findFragmentById(this.getId());
    }
    
    public WindowInsets onApplyWindowInsets(final WindowInsets windowInsets) {
        Intrinsics.checkNotNullParameter((Object)windowInsets, "insets");
        return windowInsets;
    }
    
    public void removeAllViewsInLayout() {
        for (int n = this.getChildCount() - 1; -1 < n; --n) {
            final View child = this.getChildAt(n);
            Intrinsics.checkNotNullExpressionValue((Object)child, "view");
            this.addDisappearingFragmentView(child);
        }
        super.removeAllViewsInLayout();
    }
    
    public void removeView(final View view) {
        Intrinsics.checkNotNullParameter((Object)view, "view");
        this.addDisappearingFragmentView(view);
        super.removeView(view);
    }
    
    public void removeViewAt(final int n) {
        final View child = this.getChildAt(n);
        Intrinsics.checkNotNullExpressionValue((Object)child, "view");
        this.addDisappearingFragmentView(child);
        super.removeViewAt(n);
    }
    
    public void removeViewInLayout(final View view) {
        Intrinsics.checkNotNullParameter((Object)view, "view");
        this.addDisappearingFragmentView(view);
        super.removeViewInLayout(view);
    }
    
    public void removeViews(final int n, final int n2) {
        for (int i = n; i < n + n2; ++i) {
            final View child = this.getChildAt(i);
            Intrinsics.checkNotNullExpressionValue((Object)child, "view");
            this.addDisappearingFragmentView(child);
        }
        super.removeViews(n, n2);
    }
    
    public void removeViewsInLayout(final int n, final int n2) {
        for (int i = n; i < n + n2; ++i) {
            final View child = this.getChildAt(i);
            Intrinsics.checkNotNullExpressionValue((Object)child, "view");
            this.addDisappearingFragmentView(child);
        }
        super.removeViewsInLayout(n, n2);
    }
    
    public final void setDrawDisappearingViewsLast(final boolean drawDisappearingViewsFirst) {
        this.drawDisappearingViewsFirst = drawDisappearingViewsFirst;
    }
    
    public void setLayoutTransition(final LayoutTransition layoutTransition) {
        if (Build$VERSION.SDK_INT < 18) {
            super.setLayoutTransition(layoutTransition);
            return;
        }
        throw new UnsupportedOperationException("FragmentContainerView does not support Layout Transitions or animateLayoutChanges=\"true\".");
    }
    
    public void setOnApplyWindowInsetsListener(final View$OnApplyWindowInsetsListener applyWindowInsetsListener) {
        Intrinsics.checkNotNullParameter((Object)applyWindowInsetsListener, "listener");
        this.applyWindowInsetsListener = applyWindowInsetsListener;
    }
    
    public void startViewTransition(final View view) {
        Intrinsics.checkNotNullParameter((Object)view, "view");
        if (view.getParent() == this) {
            this.transitioningFragmentViews.add(view);
        }
        super.startViewTransition(view);
    }
    
    @Metadata(d1 = { "\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u00c1\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u001e\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\u0004¨\u0006\n" }, d2 = { "Landroidx/fragment/app/FragmentContainerView$Api20Impl;", "", "()V", "onApplyWindowInsets", "Landroid/view/WindowInsets;", "onApplyWindowInsetsListener", "Landroid/view/View$OnApplyWindowInsetsListener;", "v", "Landroid/view/View;", "insets", "fragment_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
    public static final class Api20Impl
    {
        public static final Api20Impl INSTANCE;
        
        static {
            INSTANCE = new Api20Impl();
        }
        
        private Api20Impl() {
        }
        
        public final WindowInsets onApplyWindowInsets(final View$OnApplyWindowInsetsListener view$OnApplyWindowInsetsListener, final View view, final WindowInsets windowInsets) {
            Intrinsics.checkNotNullParameter((Object)view$OnApplyWindowInsetsListener, "onApplyWindowInsetsListener");
            Intrinsics.checkNotNullParameter((Object)view, "v");
            Intrinsics.checkNotNullParameter((Object)windowInsets, "insets");
            final WindowInsets onApplyWindowInsets = view$OnApplyWindowInsetsListener.onApplyWindowInsets(view, windowInsets);
            Intrinsics.checkNotNullExpressionValue((Object)onApplyWindowInsets, "onApplyWindowInsetsListe\u2026lyWindowInsets(v, insets)");
            return onApplyWindowInsets;
        }
    }
}

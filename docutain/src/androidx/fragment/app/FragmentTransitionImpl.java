// 
// Decompiled by Procyon v0.6.0
// 

package androidx.fragment.app;

import androidx.core.view.OneShotPreDrawListener;
import java.util.Map;
import androidx.core.os.CancellationSignal;
import android.view.ViewParent;
import android.graphics.RectF;
import android.graphics.Rect;
import java.util.ArrayList;
import android.view.ViewGroup;
import androidx.core.view.ViewCompat;
import android.view.View;
import java.util.List;

public abstract class FragmentTransitionImpl
{
    protected static void bfsAddViewChildren(final List<View> list, View view) {
        final int size = list.size();
        if (containedBeforeIndex(list, view, size)) {
            return;
        }
        if (ViewCompat.getTransitionName(view) != null) {
            list.add(view);
        }
        for (int i = size; i < list.size(); ++i) {
            view = (View)list.get(i);
            if (view instanceof ViewGroup) {
                final ViewGroup viewGroup = (ViewGroup)view;
                for (int childCount = viewGroup.getChildCount(), j = 0; j < childCount; ++j) {
                    final View child = viewGroup.getChildAt(j);
                    if (!containedBeforeIndex(list, child, size) && ViewCompat.getTransitionName(child) != null) {
                        list.add(child);
                    }
                }
            }
        }
    }
    
    private static boolean containedBeforeIndex(final List<View> list, final View view, final int n) {
        for (int i = 0; i < n; ++i) {
            if (list.get(i) == view) {
                return true;
            }
        }
        return false;
    }
    
    protected static boolean isNullOrEmpty(final List list) {
        return list == null || list.isEmpty();
    }
    
    public abstract void addTarget(final Object p0, final View p1);
    
    public abstract void addTargets(final Object p0, final ArrayList<View> p1);
    
    public abstract void beginDelayedTransition(final ViewGroup p0, final Object p1);
    
    public abstract boolean canHandle(final Object p0);
    
    public abstract Object cloneTransition(final Object p0);
    
    protected void getBoundsOnScreen(final View view, final Rect rect) {
        if (!ViewCompat.isAttachedToWindow(view)) {
            return;
        }
        final RectF rectF = new RectF();
        rectF.set(0.0f, 0.0f, (float)view.getWidth(), (float)view.getHeight());
        view.getMatrix().mapRect(rectF);
        rectF.offset((float)view.getLeft(), (float)view.getTop());
        View view2;
        for (ViewParent viewParent = view.getParent(); viewParent instanceof View; viewParent = view2.getParent()) {
            view2 = (View)viewParent;
            rectF.offset((float)(-view2.getScrollX()), (float)(-view2.getScrollY()));
            view2.getMatrix().mapRect(rectF);
            rectF.offset((float)view2.getLeft(), (float)view2.getTop());
        }
        final int[] array = new int[2];
        view.getRootView().getLocationOnScreen(array);
        rectF.offset((float)array[0], (float)array[1]);
        rect.set(Math.round(rectF.left), Math.round(rectF.top), Math.round(rectF.right), Math.round(rectF.bottom));
    }
    
    public abstract Object mergeTransitionsInSequence(final Object p0, final Object p1, final Object p2);
    
    public abstract Object mergeTransitionsTogether(final Object p0, final Object p1, final Object p2);
    
    ArrayList<String> prepareSetNameOverridesReordered(final ArrayList<View> list) {
        final ArrayList list2 = new ArrayList();
        for (int size = list.size(), i = 0; i < size; ++i) {
            final View view = list.get(i);
            list2.add(ViewCompat.getTransitionName(view));
            ViewCompat.setTransitionName(view, null);
        }
        return list2;
    }
    
    public abstract void removeTarget(final Object p0, final View p1);
    
    public abstract void replaceTargets(final Object p0, final ArrayList<View> p1, final ArrayList<View> p2);
    
    public abstract void scheduleHideFragmentView(final Object p0, final View p1, final ArrayList<View> p2);
    
    public abstract void scheduleRemoveTargets(final Object p0, final Object p1, final ArrayList<View> p2, final Object p3, final ArrayList<View> p4, final Object p5, final ArrayList<View> p6);
    
    public abstract void setEpicenter(final Object p0, final Rect p1);
    
    public abstract void setEpicenter(final Object p0, final View p1);
    
    public void setListenerForTransitionEnd(final Fragment fragment, final Object o, final CancellationSignal cancellationSignal, final Runnable runnable) {
        runnable.run();
    }
    
    void setNameOverridesReordered(final View view, final ArrayList<View> list, final ArrayList<View> list2, final ArrayList<String> list3, final Map<String, String> map) {
        final int size = list2.size();
        final ArrayList list4 = new ArrayList();
        for (int i = 0; i < size; ++i) {
            final View view2 = list.get(i);
            final String transitionName = ViewCompat.getTransitionName(view2);
            list4.add(transitionName);
            if (transitionName != null) {
                ViewCompat.setTransitionName(view2, null);
                final String s = map.get(transitionName);
                for (int j = 0; j < size; ++j) {
                    if (s.equals(list3.get(j))) {
                        ViewCompat.setTransitionName((View)list2.get(j), transitionName);
                        break;
                    }
                }
            }
        }
        OneShotPreDrawListener.add(view, new Runnable(this, size, list2, list3, list, list4) {
            final FragmentTransitionImpl this$0;
            final ArrayList val$inNames;
            final int val$numSharedElements;
            final ArrayList val$outNames;
            final ArrayList val$sharedElementsIn;
            final ArrayList val$sharedElementsOut;
            
            @Override
            public void run() {
                for (int i = 0; i < this.val$numSharedElements; ++i) {
                    ViewCompat.setTransitionName((View)this.val$sharedElementsIn.get(i), (String)this.val$inNames.get(i));
                    ViewCompat.setTransitionName((View)this.val$sharedElementsOut.get(i), (String)this.val$outNames.get(i));
                }
            }
        });
    }
    
    public abstract void setSharedElementTargets(final Object p0, final View p1, final ArrayList<View> p2);
    
    public abstract void swapSharedElementTargets(final Object p0, final ArrayList<View> p1, final ArrayList<View> p2);
    
    public abstract Object wrapTransitionInSet(final Object p0);
}

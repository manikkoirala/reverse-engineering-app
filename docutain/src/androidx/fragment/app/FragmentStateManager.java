// 
// Decompiled by Procyon v0.6.0
// 

package androidx.fragment.app;

import android.util.SparseArray;
import android.os.Parcelable;
import androidx.lifecycle.LifecycleOwner;
import java.util.Iterator;
import android.app.Activity;
import androidx.lifecycle.ViewModelStoreOwner;
import android.view.LayoutInflater;
import android.view.View$OnAttachStateChangeListener;
import androidx.core.view.ViewCompat;
import androidx.fragment.R;
import android.content.res.Resources$NotFoundException;
import android.view.ViewGroup;
import androidx.fragment.app.strictmode.FragmentStrictMode;
import android.view.ViewParent;
import android.view.View;
import android.util.Log;
import android.os.Bundle;

class FragmentStateManager
{
    static final String ARGUMENTS_KEY = "arguments";
    static final String CHILD_FRAGMENT_MANAGER_KEY = "childFragmentManager";
    static final String FRAGMENT_STATE_KEY = "state";
    static final String REGISTRY_STATE_KEY = "registryState";
    static final String SAVED_INSTANCE_STATE_KEY = "savedInstanceState";
    private static final String TAG = "FragmentManager";
    static final String VIEW_REGISTRY_STATE_KEY = "viewRegistryState";
    static final String VIEW_STATE_KEY = "viewState";
    private final FragmentLifecycleCallbacksDispatcher mDispatcher;
    private final Fragment mFragment;
    private int mFragmentManagerState;
    private final FragmentStore mFragmentStore;
    private boolean mMovingToState;
    
    FragmentStateManager(final FragmentLifecycleCallbacksDispatcher mDispatcher, final FragmentStore mFragmentStore, final Fragment mFragment) {
        this.mMovingToState = false;
        this.mFragmentManagerState = -1;
        this.mDispatcher = mDispatcher;
        this.mFragmentStore = mFragmentStore;
        this.mFragment = mFragment;
    }
    
    FragmentStateManager(final FragmentLifecycleCallbacksDispatcher mDispatcher, final FragmentStore mFragmentStore, final Fragment mFragment, final Bundle mSavedFragmentState) {
        this.mMovingToState = false;
        this.mFragmentManagerState = -1;
        this.mDispatcher = mDispatcher;
        this.mFragmentStore = mFragmentStore;
        this.mFragment = mFragment;
        mFragment.mSavedViewState = null;
        mFragment.mSavedViewRegistryState = null;
        mFragment.mBackStackNesting = 0;
        mFragment.mInLayout = false;
        mFragment.mAdded = false;
        String mWho;
        if (mFragment.mTarget != null) {
            mWho = mFragment.mTarget.mWho;
        }
        else {
            mWho = null;
        }
        mFragment.mTargetWho = mWho;
        mFragment.mTarget = null;
        mFragment.mSavedFragmentState = mSavedFragmentState;
        mFragment.mArguments = mSavedFragmentState.getBundle("arguments");
    }
    
    FragmentStateManager(final FragmentLifecycleCallbacksDispatcher mDispatcher, final FragmentStore mFragmentStore, final ClassLoader classLoader, final FragmentFactory fragmentFactory, final Bundle mSavedFragmentState) {
        this.mMovingToState = false;
        this.mFragmentManagerState = -1;
        this.mDispatcher = mDispatcher;
        this.mFragmentStore = mFragmentStore;
        final Fragment instantiate = ((FragmentState)mSavedFragmentState.getParcelable("state")).instantiate(fragmentFactory, classLoader);
        this.mFragment = instantiate;
        instantiate.mSavedFragmentState = mSavedFragmentState;
        final Bundle bundle = mSavedFragmentState.getBundle("arguments");
        if (bundle != null) {
            bundle.setClassLoader(classLoader);
        }
        instantiate.setArguments(bundle);
        if (FragmentManager.isLoggingEnabled(2)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Instantiated fragment ");
            sb.append(instantiate);
            Log.v("FragmentManager", sb.toString());
        }
    }
    
    private boolean isFragmentViewChild(final View view) {
        if (view == this.mFragment.mView) {
            return true;
        }
        for (ViewParent viewParent = view.getParent(); viewParent != null; viewParent = viewParent.getParent()) {
            if (viewParent == this.mFragment.mView) {
                return true;
            }
        }
        return false;
    }
    
    void activityCreated() {
        if (FragmentManager.isLoggingEnabled(3)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("moveto ACTIVITY_CREATED: ");
            sb.append(this.mFragment);
            Log.d("FragmentManager", sb.toString());
        }
        Bundle bundle = null;
        if (this.mFragment.mSavedFragmentState != null) {
            bundle = this.mFragment.mSavedFragmentState.getBundle("savedInstanceState");
        }
        this.mFragment.performActivityCreated(bundle);
        this.mDispatcher.dispatchOnFragmentActivityCreated(this.mFragment, bundle, false);
    }
    
    void addViewToContainer() {
        final Fragment viewFragment = FragmentManager.findViewFragment((View)this.mFragment.mContainer);
        final Fragment parentFragment = this.mFragment.getParentFragment();
        if (viewFragment != null && !viewFragment.equals(parentFragment)) {
            final Fragment mFragment = this.mFragment;
            FragmentStrictMode.onWrongNestedHierarchy(mFragment, viewFragment, mFragment.mContainerId);
        }
        this.mFragment.mContainer.addView(this.mFragment.mView, this.mFragmentStore.findFragmentIndexInContainer(this.mFragment));
    }
    
    void attach() {
        if (FragmentManager.isLoggingEnabled(3)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("moveto ATTACHED: ");
            sb.append(this.mFragment);
            Log.d("FragmentManager", sb.toString());
        }
        final Fragment mTarget = this.mFragment.mTarget;
        FragmentStateManager fragmentStateManager = null;
        if (mTarget != null) {
            fragmentStateManager = this.mFragmentStore.getFragmentStateManager(this.mFragment.mTarget.mWho);
            if (fragmentStateManager == null) {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("Fragment ");
                sb2.append(this.mFragment);
                sb2.append(" declared target fragment ");
                sb2.append(this.mFragment.mTarget);
                sb2.append(" that does not belong to this FragmentManager!");
                throw new IllegalStateException(sb2.toString());
            }
            final Fragment mFragment = this.mFragment;
            mFragment.mTargetWho = mFragment.mTarget.mWho;
            this.mFragment.mTarget = null;
        }
        else if (this.mFragment.mTargetWho != null) {
            fragmentStateManager = this.mFragmentStore.getFragmentStateManager(this.mFragment.mTargetWho);
            if (fragmentStateManager == null) {
                final StringBuilder sb3 = new StringBuilder();
                sb3.append("Fragment ");
                sb3.append(this.mFragment);
                sb3.append(" declared target fragment ");
                sb3.append(this.mFragment.mTargetWho);
                sb3.append(" that does not belong to this FragmentManager!");
                throw new IllegalStateException(sb3.toString());
            }
        }
        if (fragmentStateManager != null) {
            fragmentStateManager.moveToExpectedState();
        }
        final Fragment mFragment2 = this.mFragment;
        mFragment2.mHost = mFragment2.mFragmentManager.getHost();
        final Fragment mFragment3 = this.mFragment;
        mFragment3.mParentFragment = mFragment3.mFragmentManager.getParent();
        this.mDispatcher.dispatchOnFragmentPreAttached(this.mFragment, false);
        this.mFragment.performAttach();
        this.mDispatcher.dispatchOnFragmentAttached(this.mFragment, false);
    }
    
    int computeExpectedState() {
        if (this.mFragment.mFragmentManager == null) {
            return this.mFragment.mState;
        }
        final int mFragmentManagerState = this.mFragmentManagerState;
        final int n = FragmentStateManager$2.$SwitchMap$androidx$lifecycle$Lifecycle$State[this.mFragment.mMaxState.ordinal()];
        int n2 = mFragmentManagerState;
        if (n != 1) {
            if (n != 2) {
                if (n != 3) {
                    if (n != 4) {
                        n2 = Math.min(mFragmentManagerState, -1);
                    }
                    else {
                        n2 = Math.min(mFragmentManagerState, 0);
                    }
                }
                else {
                    n2 = Math.min(mFragmentManagerState, 1);
                }
            }
            else {
                n2 = Math.min(mFragmentManagerState, 5);
            }
        }
        int a = n2;
        if (this.mFragment.mFromLayout) {
            if (this.mFragment.mInLayout) {
                final int a2 = a = Math.max(this.mFragmentManagerState, 2);
                if (this.mFragment.mView != null) {
                    a = a2;
                    if (this.mFragment.mView.getParent() == null) {
                        a = Math.min(a2, 2);
                    }
                }
            }
            else if (this.mFragmentManagerState < 4) {
                a = Math.min(n2, this.mFragment.mState);
            }
            else {
                a = Math.min(n2, 1);
            }
        }
        int min = a;
        if (!this.mFragment.mAdded) {
            min = Math.min(a, 1);
        }
        Enum<SpecialEffectsController.Operation.LifecycleImpact> awaitingCompletionLifecycleImpact = null;
        if (this.mFragment.mContainer != null) {
            awaitingCompletionLifecycleImpact = SpecialEffectsController.getOrCreateController(this.mFragment.mContainer, this.mFragment.getParentFragmentManager()).getAwaitingCompletionLifecycleImpact(this);
        }
        int a3;
        if (awaitingCompletionLifecycleImpact == SpecialEffectsController.Operation.LifecycleImpact.ADDING) {
            a3 = Math.min(min, 6);
        }
        else if (awaitingCompletionLifecycleImpact == SpecialEffectsController.Operation.LifecycleImpact.REMOVING) {
            a3 = Math.max(min, 3);
        }
        else {
            a3 = min;
            if (this.mFragment.mRemoving) {
                if (this.mFragment.isInBackStack()) {
                    a3 = Math.min(min, 1);
                }
                else {
                    a3 = Math.min(min, -1);
                }
            }
        }
        int min2 = a3;
        if (this.mFragment.mDeferStart) {
            min2 = a3;
            if (this.mFragment.mState < 5) {
                min2 = Math.min(a3, 4);
            }
        }
        if (FragmentManager.isLoggingEnabled(2)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("computeExpectedState() of ");
            sb.append(min2);
            sb.append(" for ");
            sb.append(this.mFragment);
            Log.v("FragmentManager", sb.toString());
        }
        return min2;
    }
    
    void create() {
        if (FragmentManager.isLoggingEnabled(3)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("moveto CREATED: ");
            sb.append(this.mFragment);
            Log.d("FragmentManager", sb.toString());
        }
        Bundle bundle = null;
        if (this.mFragment.mSavedFragmentState != null) {
            bundle = this.mFragment.mSavedFragmentState.getBundle("savedInstanceState");
        }
        if (!this.mFragment.mIsCreated) {
            this.mDispatcher.dispatchOnFragmentPreCreated(this.mFragment, bundle, false);
            this.mFragment.performCreate(bundle);
            this.mDispatcher.dispatchOnFragmentCreated(this.mFragment, bundle, false);
        }
        else {
            this.mFragment.mState = 1;
            this.mFragment.restoreChildFragmentState();
        }
    }
    
    void createView() {
        if (this.mFragment.mFromLayout) {
            return;
        }
        if (FragmentManager.isLoggingEnabled(3)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("moveto CREATE_VIEW: ");
            sb.append(this.mFragment);
            Log.d("FragmentManager", sb.toString());
        }
        final Bundle mSavedFragmentState = this.mFragment.mSavedFragmentState;
        ViewGroup mContainer = null;
        Bundle bundle;
        if (mSavedFragmentState != null) {
            bundle = this.mFragment.mSavedFragmentState.getBundle("savedInstanceState");
        }
        else {
            bundle = null;
        }
        final LayoutInflater performGetLayoutInflater = this.mFragment.performGetLayoutInflater(bundle);
        if (this.mFragment.mContainer != null) {
            mContainer = this.mFragment.mContainer;
        }
        else if (this.mFragment.mContainerId != 0) {
            if (this.mFragment.mContainerId == -1) {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("Cannot create fragment ");
                sb2.append(this.mFragment);
                sb2.append(" for a container view with no id");
                throw new IllegalArgumentException(sb2.toString());
            }
            final ViewGroup viewGroup = (ViewGroup)this.mFragment.mFragmentManager.getContainer().onFindViewById(this.mFragment.mContainerId);
            if (viewGroup == null) {
                if (!this.mFragment.mRestored) {
                    String resourceName;
                    try {
                        resourceName = this.mFragment.getResources().getResourceName(this.mFragment.mContainerId);
                    }
                    catch (final Resources$NotFoundException ex) {
                        resourceName = "unknown";
                    }
                    final StringBuilder sb3 = new StringBuilder();
                    sb3.append("No view found for id 0x");
                    sb3.append(Integer.toHexString(this.mFragment.mContainerId));
                    sb3.append(" (");
                    sb3.append(resourceName);
                    sb3.append(") for fragment ");
                    sb3.append(this.mFragment);
                    throw new IllegalArgumentException(sb3.toString());
                }
                mContainer = viewGroup;
            }
            else {
                mContainer = viewGroup;
                if (!(viewGroup instanceof FragmentContainerView)) {
                    FragmentStrictMode.onWrongFragmentContainer(this.mFragment, viewGroup);
                    mContainer = viewGroup;
                }
            }
        }
        this.mFragment.mContainer = mContainer;
        this.mFragment.performCreateView(performGetLayoutInflater, mContainer, bundle);
        if (this.mFragment.mView != null) {
            if (FragmentManager.isLoggingEnabled(3)) {
                final StringBuilder sb4 = new StringBuilder();
                sb4.append("moveto VIEW_CREATED: ");
                sb4.append(this.mFragment);
                Log.d("FragmentManager", sb4.toString());
            }
            this.mFragment.mView.setSaveFromParentEnabled(false);
            this.mFragment.mView.setTag(R.id.fragment_container_view_tag, (Object)this.mFragment);
            if (mContainer != null) {
                this.addViewToContainer();
            }
            if (this.mFragment.mHidden) {
                this.mFragment.mView.setVisibility(8);
            }
            if (ViewCompat.isAttachedToWindow(this.mFragment.mView)) {
                ViewCompat.requestApplyInsets(this.mFragment.mView);
            }
            else {
                final View mView = this.mFragment.mView;
                mView.addOnAttachStateChangeListener((View$OnAttachStateChangeListener)new View$OnAttachStateChangeListener(this, mView) {
                    final FragmentStateManager this$0;
                    final View val$fragmentView;
                    
                    public void onViewAttachedToWindow(final View view) {
                        this.val$fragmentView.removeOnAttachStateChangeListener((View$OnAttachStateChangeListener)this);
                        ViewCompat.requestApplyInsets(this.val$fragmentView);
                    }
                    
                    public void onViewDetachedFromWindow(final View view) {
                    }
                });
            }
            this.mFragment.performViewCreated();
            final FragmentLifecycleCallbacksDispatcher mDispatcher = this.mDispatcher;
            final Fragment mFragment = this.mFragment;
            mDispatcher.dispatchOnFragmentViewCreated(mFragment, mFragment.mView, bundle, false);
            final int visibility = this.mFragment.mView.getVisibility();
            this.mFragment.setPostOnViewCreatedAlpha(this.mFragment.mView.getAlpha());
            if (this.mFragment.mContainer != null && visibility == 0) {
                final View focus = this.mFragment.mView.findFocus();
                if (focus != null) {
                    this.mFragment.setFocusedView(focus);
                    if (FragmentManager.isLoggingEnabled(2)) {
                        final StringBuilder sb5 = new StringBuilder();
                        sb5.append("requestFocus: Saved focused view ");
                        sb5.append(focus);
                        sb5.append(" for Fragment ");
                        sb5.append(this.mFragment);
                        Log.v("FragmentManager", sb5.toString());
                    }
                }
                this.mFragment.mView.setAlpha(0.0f);
            }
        }
        this.mFragment.mState = 2;
    }
    
    void destroy() {
        if (FragmentManager.isLoggingEnabled(3)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("movefrom CREATED: ");
            sb.append(this.mFragment);
            Log.d("FragmentManager", sb.toString());
        }
        final boolean mRemoving = this.mFragment.mRemoving;
        boolean cleared = true;
        final boolean b = mRemoving && !this.mFragment.isInBackStack();
        if (b && !this.mFragment.mBeingSaved) {
            this.mFragmentStore.setSavedState(this.mFragment.mWho, null);
        }
        if (b || this.mFragmentStore.getNonConfig().shouldDestroy(this.mFragment)) {
            final FragmentHostCallback<?> mHost = this.mFragment.mHost;
            if (mHost instanceof ViewModelStoreOwner) {
                cleared = this.mFragmentStore.getNonConfig().isCleared();
            }
            else if (mHost.getContext() instanceof Activity) {
                cleared = (true ^ ((Activity)mHost.getContext()).isChangingConfigurations());
            }
            if ((b && !this.mFragment.mBeingSaved) || cleared) {
                this.mFragmentStore.getNonConfig().clearNonConfigState(this.mFragment);
            }
            this.mFragment.performDestroy();
            this.mDispatcher.dispatchOnFragmentDestroyed(this.mFragment, false);
            for (final FragmentStateManager fragmentStateManager : this.mFragmentStore.getActiveFragmentStateManagers()) {
                if (fragmentStateManager != null) {
                    final Fragment fragment = fragmentStateManager.getFragment();
                    if (!this.mFragment.mWho.equals(fragment.mTargetWho)) {
                        continue;
                    }
                    fragment.mTarget = this.mFragment;
                    fragment.mTargetWho = null;
                }
            }
            if (this.mFragment.mTargetWho != null) {
                final Fragment mFragment = this.mFragment;
                mFragment.mTarget = this.mFragmentStore.findActiveFragment(mFragment.mTargetWho);
            }
            this.mFragmentStore.makeInactive(this);
        }
        else {
            if (this.mFragment.mTargetWho != null) {
                final Fragment activeFragment = this.mFragmentStore.findActiveFragment(this.mFragment.mTargetWho);
                if (activeFragment != null && activeFragment.mRetainInstance) {
                    this.mFragment.mTarget = activeFragment;
                }
            }
            this.mFragment.mState = 0;
        }
    }
    
    void destroyFragmentView() {
        if (FragmentManager.isLoggingEnabled(3)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("movefrom CREATE_VIEW: ");
            sb.append(this.mFragment);
            Log.d("FragmentManager", sb.toString());
        }
        if (this.mFragment.mContainer != null && this.mFragment.mView != null) {
            this.mFragment.mContainer.removeView(this.mFragment.mView);
        }
        this.mFragment.performDestroyView();
        this.mDispatcher.dispatchOnFragmentViewDestroyed(this.mFragment, false);
        this.mFragment.mContainer = null;
        this.mFragment.mView = null;
        this.mFragment.mViewLifecycleOwner = null;
        this.mFragment.mViewLifecycleOwnerLiveData.setValue(null);
        this.mFragment.mInLayout = false;
    }
    
    void detach() {
        if (FragmentManager.isLoggingEnabled(3)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("movefrom ATTACHED: ");
            sb.append(this.mFragment);
            Log.d("FragmentManager", sb.toString());
        }
        this.mFragment.performDetach();
        final FragmentLifecycleCallbacksDispatcher mDispatcher = this.mDispatcher;
        final Fragment mFragment = this.mFragment;
        final int n = 0;
        mDispatcher.dispatchOnFragmentDetached(mFragment, false);
        this.mFragment.mState = -1;
        this.mFragment.mHost = null;
        this.mFragment.mParentFragment = null;
        this.mFragment.mFragmentManager = null;
        int n2 = n;
        if (this.mFragment.mRemoving) {
            n2 = n;
            if (!this.mFragment.isInBackStack()) {
                n2 = 1;
            }
        }
        if (n2 != 0 || this.mFragmentStore.getNonConfig().shouldDestroy(this.mFragment)) {
            if (FragmentManager.isLoggingEnabled(3)) {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("initState called for fragment: ");
                sb2.append(this.mFragment);
                Log.d("FragmentManager", sb2.toString());
            }
            this.mFragment.initState();
        }
    }
    
    void ensureInflatedView() {
        if (this.mFragment.mFromLayout && this.mFragment.mInLayout && !this.mFragment.mPerformedCreateView) {
            if (FragmentManager.isLoggingEnabled(3)) {
                final StringBuilder sb = new StringBuilder();
                sb.append("moveto CREATE_VIEW: ");
                sb.append(this.mFragment);
                Log.d("FragmentManager", sb.toString());
            }
            Bundle bundle;
            if (this.mFragment.mSavedFragmentState != null) {
                bundle = this.mFragment.mSavedFragmentState.getBundle("savedInstanceState");
            }
            else {
                bundle = null;
            }
            final Fragment mFragment = this.mFragment;
            mFragment.performCreateView(mFragment.performGetLayoutInflater(bundle), null, bundle);
            if (this.mFragment.mView != null) {
                this.mFragment.mView.setSaveFromParentEnabled(false);
                this.mFragment.mView.setTag(R.id.fragment_container_view_tag, (Object)this.mFragment);
                if (this.mFragment.mHidden) {
                    this.mFragment.mView.setVisibility(8);
                }
                this.mFragment.performViewCreated();
                final FragmentLifecycleCallbacksDispatcher mDispatcher = this.mDispatcher;
                final Fragment mFragment2 = this.mFragment;
                mDispatcher.dispatchOnFragmentViewCreated(mFragment2, mFragment2.mView, bundle, false);
                this.mFragment.mState = 2;
            }
        }
    }
    
    Fragment getFragment() {
        return this.mFragment;
    }
    
    void moveToExpectedState() {
        if (this.mMovingToState) {
            if (FragmentManager.isLoggingEnabled(2)) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Ignoring re-entrant call to moveToExpectedState() for ");
                sb.append(this.getFragment());
                Log.v("FragmentManager", sb.toString());
            }
            return;
        }
        try {
            this.mMovingToState = true;
            boolean b = false;
            while (true) {
                final int computeExpectedState = this.computeExpectedState();
                if (computeExpectedState == this.mFragment.mState) {
                    break;
                }
                if (computeExpectedState > this.mFragment.mState) {
                    switch (this.mFragment.mState + 1) {
                        case 7: {
                            this.resume();
                            break;
                        }
                        case 6: {
                            this.mFragment.mState = 6;
                            break;
                        }
                        case 5: {
                            this.start();
                            break;
                        }
                        case 4: {
                            if (this.mFragment.mView != null && this.mFragment.mContainer != null) {
                                SpecialEffectsController.getOrCreateController(this.mFragment.mContainer, this.mFragment.getParentFragmentManager()).enqueueAdd(SpecialEffectsController.Operation.State.from(this.mFragment.mView.getVisibility()), this);
                            }
                            this.mFragment.mState = 4;
                            break;
                        }
                        case 3: {
                            this.activityCreated();
                            break;
                        }
                        case 2: {
                            this.ensureInflatedView();
                            this.createView();
                            break;
                        }
                        case 1: {
                            this.create();
                            break;
                        }
                        case 0: {
                            this.attach();
                            break;
                        }
                    }
                }
                else {
                    switch (this.mFragment.mState - 1) {
                        case 6: {
                            this.pause();
                            break;
                        }
                        case 5: {
                            this.mFragment.mState = 5;
                            break;
                        }
                        case 4: {
                            this.stop();
                            break;
                        }
                        case 3: {
                            if (FragmentManager.isLoggingEnabled(3)) {
                                final StringBuilder sb2 = new StringBuilder();
                                sb2.append("movefrom ACTIVITY_CREATED: ");
                                sb2.append(this.mFragment);
                                Log.d("FragmentManager", sb2.toString());
                            }
                            if (this.mFragment.mBeingSaved) {
                                this.mFragmentStore.setSavedState(this.mFragment.mWho, this.saveState());
                            }
                            else if (this.mFragment.mView != null && this.mFragment.mSavedViewState == null) {
                                this.saveViewState();
                            }
                            if (this.mFragment.mView != null && this.mFragment.mContainer != null) {
                                SpecialEffectsController.getOrCreateController(this.mFragment.mContainer, this.mFragment.getParentFragmentManager()).enqueueRemove(this);
                            }
                            this.mFragment.mState = 3;
                            break;
                        }
                        case 2: {
                            this.mFragment.mInLayout = false;
                            this.mFragment.mState = 2;
                            break;
                        }
                        case 1: {
                            this.destroyFragmentView();
                            this.mFragment.mState = 1;
                            break;
                        }
                        case 0: {
                            if (this.mFragment.mBeingSaved && this.mFragmentStore.getSavedState(this.mFragment.mWho) == null) {
                                this.mFragmentStore.setSavedState(this.mFragment.mWho, this.saveState());
                            }
                            this.destroy();
                            break;
                        }
                        case -1: {
                            this.detach();
                            break;
                        }
                    }
                }
                b = true;
            }
            if (!b && this.mFragment.mState == -1 && this.mFragment.mRemoving && !this.mFragment.isInBackStack() && !this.mFragment.mBeingSaved) {
                if (FragmentManager.isLoggingEnabled(3)) {
                    final StringBuilder sb3 = new StringBuilder();
                    sb3.append("Cleaning up state of never attached fragment: ");
                    sb3.append(this.mFragment);
                    Log.d("FragmentManager", sb3.toString());
                }
                this.mFragmentStore.getNonConfig().clearNonConfigState(this.mFragment);
                this.mFragmentStore.makeInactive(this);
                if (FragmentManager.isLoggingEnabled(3)) {
                    final StringBuilder sb4 = new StringBuilder();
                    sb4.append("initState called for fragment: ");
                    sb4.append(this.mFragment);
                    Log.d("FragmentManager", sb4.toString());
                }
                this.mFragment.initState();
            }
            if (this.mFragment.mHiddenChanged) {
                if (this.mFragment.mView != null && this.mFragment.mContainer != null) {
                    final SpecialEffectsController orCreateController = SpecialEffectsController.getOrCreateController(this.mFragment.mContainer, this.mFragment.getParentFragmentManager());
                    if (this.mFragment.mHidden) {
                        orCreateController.enqueueHide(this);
                    }
                    else {
                        orCreateController.enqueueShow(this);
                    }
                }
                if (this.mFragment.mFragmentManager != null) {
                    this.mFragment.mFragmentManager.invalidateMenuForFragment(this.mFragment);
                }
                this.mFragment.mHiddenChanged = false;
                final Fragment mFragment = this.mFragment;
                mFragment.onHiddenChanged(mFragment.mHidden);
                this.mFragment.mChildFragmentManager.dispatchOnHiddenChanged();
            }
        }
        finally {
            this.mMovingToState = false;
        }
    }
    
    void pause() {
        if (FragmentManager.isLoggingEnabled(3)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("movefrom RESUMED: ");
            sb.append(this.mFragment);
            Log.d("FragmentManager", sb.toString());
        }
        this.mFragment.performPause();
        this.mDispatcher.dispatchOnFragmentPaused(this.mFragment, false);
    }
    
    void restoreState(final ClassLoader classLoader) {
        if (this.mFragment.mSavedFragmentState == null) {
            return;
        }
        this.mFragment.mSavedFragmentState.setClassLoader(classLoader);
        if (this.mFragment.mSavedFragmentState.getBundle("savedInstanceState") == null) {
            this.mFragment.mSavedFragmentState.putBundle("savedInstanceState", new Bundle());
        }
        final Fragment mFragment = this.mFragment;
        mFragment.mSavedViewState = (SparseArray<Parcelable>)mFragment.mSavedFragmentState.getSparseParcelableArray("viewState");
        final Fragment mFragment2 = this.mFragment;
        mFragment2.mSavedViewRegistryState = mFragment2.mSavedFragmentState.getBundle("viewRegistryState");
        final FragmentState fragmentState = (FragmentState)this.mFragment.mSavedFragmentState.getParcelable("state");
        if (fragmentState != null) {
            this.mFragment.mTargetWho = fragmentState.mTargetWho;
            this.mFragment.mTargetRequestCode = fragmentState.mTargetRequestCode;
            if (this.mFragment.mSavedUserVisibleHint != null) {
                final Fragment mFragment3 = this.mFragment;
                mFragment3.mUserVisibleHint = mFragment3.mSavedUserVisibleHint;
                this.mFragment.mSavedUserVisibleHint = null;
            }
            else {
                this.mFragment.mUserVisibleHint = fragmentState.mUserVisibleHint;
            }
        }
        if (!this.mFragment.mUserVisibleHint) {
            this.mFragment.mDeferStart = true;
        }
    }
    
    void resume() {
        if (FragmentManager.isLoggingEnabled(3)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("moveto RESUMED: ");
            sb.append(this.mFragment);
            Log.d("FragmentManager", sb.toString());
        }
        final View focusedView = this.mFragment.getFocusedView();
        if (focusedView != null && this.isFragmentViewChild(focusedView)) {
            final boolean requestFocus = focusedView.requestFocus();
            if (FragmentManager.isLoggingEnabled(2)) {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("requestFocus: Restoring focused view ");
                sb2.append(focusedView);
                sb2.append(" ");
                String str;
                if (requestFocus) {
                    str = "succeeded";
                }
                else {
                    str = "failed";
                }
                sb2.append(str);
                sb2.append(" on Fragment ");
                sb2.append(this.mFragment);
                sb2.append(" resulting in focused view ");
                sb2.append(this.mFragment.mView.findFocus());
                Log.v("FragmentManager", sb2.toString());
            }
        }
        this.mFragment.setFocusedView(null);
        this.mFragment.performResume();
        this.mDispatcher.dispatchOnFragmentResumed(this.mFragment, false);
        this.mFragmentStore.setSavedState(this.mFragment.mWho, null);
        this.mFragment.mSavedFragmentState = null;
        this.mFragment.mSavedViewState = null;
        this.mFragment.mSavedViewRegistryState = null;
    }
    
    Fragment.SavedState saveInstanceState() {
        if (this.mFragment.mState > -1) {
            return new Fragment.SavedState(this.saveState());
        }
        return null;
    }
    
    Bundle saveState() {
        final Bundle bundle = new Bundle();
        if (this.mFragment.mState == -1 && this.mFragment.mSavedFragmentState != null) {
            bundle.putAll(this.mFragment.mSavedFragmentState);
        }
        bundle.putParcelable("state", (Parcelable)new FragmentState(this.mFragment));
        if (this.mFragment.mState > -1) {
            final Bundle bundle2 = new Bundle();
            this.mFragment.performSaveInstanceState(bundle2);
            if (!bundle2.isEmpty()) {
                bundle.putBundle("savedInstanceState", bundle2);
            }
            this.mDispatcher.dispatchOnFragmentSaveInstanceState(this.mFragment, bundle2, false);
            final Bundle bundle3 = new Bundle();
            this.mFragment.mSavedStateRegistryController.performSave(bundle3);
            if (!bundle3.isEmpty()) {
                bundle.putBundle("registryState", bundle3);
            }
            final Bundle saveAllStateInternal = this.mFragment.mChildFragmentManager.saveAllStateInternal();
            if (!saveAllStateInternal.isEmpty()) {
                bundle.putBundle("childFragmentManager", saveAllStateInternal);
            }
            if (this.mFragment.mView != null) {
                this.saveViewState();
            }
            if (this.mFragment.mSavedViewState != null) {
                bundle.putSparseParcelableArray("viewState", (SparseArray)this.mFragment.mSavedViewState);
            }
            if (this.mFragment.mSavedViewRegistryState != null) {
                bundle.putBundle("viewRegistryState", this.mFragment.mSavedViewRegistryState);
            }
        }
        if (this.mFragment.mArguments != null) {
            bundle.putBundle("arguments", this.mFragment.mArguments);
        }
        return bundle;
    }
    
    void saveViewState() {
        if (this.mFragment.mView == null) {
            return;
        }
        if (FragmentManager.isLoggingEnabled(2)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Saving view state for fragment ");
            sb.append(this.mFragment);
            sb.append(" with view ");
            sb.append(this.mFragment.mView);
            Log.v("FragmentManager", sb.toString());
        }
        final SparseArray mSavedViewState = new SparseArray();
        this.mFragment.mView.saveHierarchyState(mSavedViewState);
        if (mSavedViewState.size() > 0) {
            this.mFragment.mSavedViewState = (SparseArray<Parcelable>)mSavedViewState;
        }
        final Bundle mSavedViewRegistryState = new Bundle();
        this.mFragment.mViewLifecycleOwner.performSave(mSavedViewRegistryState);
        if (!mSavedViewRegistryState.isEmpty()) {
            this.mFragment.mSavedViewRegistryState = mSavedViewRegistryState;
        }
    }
    
    void setFragmentManagerState(final int mFragmentManagerState) {
        this.mFragmentManagerState = mFragmentManagerState;
    }
    
    void start() {
        if (FragmentManager.isLoggingEnabled(3)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("moveto STARTED: ");
            sb.append(this.mFragment);
            Log.d("FragmentManager", sb.toString());
        }
        this.mFragment.performStart();
        this.mDispatcher.dispatchOnFragmentStarted(this.mFragment, false);
    }
    
    void stop() {
        if (FragmentManager.isLoggingEnabled(3)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("movefrom STARTED: ");
            sb.append(this.mFragment);
            Log.d("FragmentManager", sb.toString());
        }
        this.mFragment.performStop();
        this.mDispatcher.dispatchOnFragmentStopped(this.mFragment, false);
    }
}

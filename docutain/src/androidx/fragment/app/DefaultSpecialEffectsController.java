// 
// Decompiled by Procyon v0.6.0
// 

package androidx.fragment.app;

import java.util.ListIterator;
import kotlin.Pair;
import androidx.core.view.OneShotPreDrawListener;
import androidx.core.app.SharedElementCallback;
import kotlin.TuplesKt;
import java.util.LinkedHashMap;
import java.util.Iterator;
import android.content.Context;
import android.view.animation.Animation$AnimationListener;
import android.view.animation.Animation;
import androidx.core.os.CancellationSignal;
import android.animation.Animator$AnimatorListener;
import android.util.Log;
import java.util.Set;
import kotlin.collections.CollectionsKt;
import kotlin.jvm.functions.Function1;
import java.util.Collection;
import androidx.core.view.ViewCompat;
import java.util.Map;
import androidx.core.view.ViewGroupCompat;
import kotlin.jvm.internal.Intrinsics;
import android.view.ViewGroup;
import android.animation.Animator;
import java.util.ArrayList;
import java.util.List;
import android.graphics.Rect;
import android.view.View;
import androidx.collection.ArrayMap;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000v\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010 \n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010%\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010!\n\u0002\b\u0002\n\u0002\u0010$\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u001e\n\u0002\b\u0004\b\u0000\u0018\u00002\u00020\u0001:\u0003*+,B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u0010\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bH\u0002J(\u0010\t\u001a\u00020\u00062\u0016\u0010\n\u001a\u0012\u0012\u0004\u0012\u00020\f0\u000bj\b\u0012\u0004\u0012\u00020\f`\r2\u0006\u0010\u000e\u001a\u00020\fH\u0002J\u001e\u0010\u000f\u001a\u00020\u00062\f\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\b0\u00112\u0006\u0010\u0012\u001a\u00020\u0013H\u0016J$\u0010\u0014\u001a\u00020\u00062\u0012\u0010\u0015\u001a\u000e\u0012\u0004\u0012\u00020\u0017\u0012\u0004\u0012\u00020\f0\u00162\u0006\u0010\u000e\u001a\u00020\fH\u0002J@\u0010\u0018\u001a\u00020\u00062\f\u0010\u0019\u001a\b\u0012\u0004\u0012\u00020\u001a0\u00112\f\u0010\u001b\u001a\b\u0012\u0004\u0012\u00020\b0\u001c2\u0006\u0010\u001d\u001a\u00020\u00132\u0012\u0010\u001e\u001a\u000e\u0012\u0004\u0012\u00020\b\u0012\u0004\u0012\u00020\u00130\u001fH\u0002JL\u0010 \u001a\u000e\u0012\u0004\u0012\u00020\b\u0012\u0004\u0012\u00020\u00130\u001f2\f\u0010!\u001a\b\u0012\u0004\u0012\u00020\"0\u00112\f\u0010\u001b\u001a\b\u0012\u0004\u0012\u00020\b0\u001c2\u0006\u0010\u0012\u001a\u00020\u00132\b\u0010#\u001a\u0004\u0018\u00010\b2\b\u0010$\u001a\u0004\u0018\u00010\bH\u0002J\u0016\u0010%\u001a\u00020\u00062\f\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\b0\u0011H\u0002J&\u0010&\u001a\u00020\u0006*\u000e\u0012\u0004\u0012\u00020\u0017\u0012\u0004\u0012\u00020\f0'2\f\u0010(\u001a\b\u0012\u0004\u0012\u00020\u00170)H\u0002¨\u0006-" }, d2 = { "Landroidx/fragment/app/DefaultSpecialEffectsController;", "Landroidx/fragment/app/SpecialEffectsController;", "container", "Landroid/view/ViewGroup;", "(Landroid/view/ViewGroup;)V", "applyContainerChanges", "", "operation", "Landroidx/fragment/app/SpecialEffectsController$Operation;", "captureTransitioningViews", "transitioningViews", "Ljava/util/ArrayList;", "Landroid/view/View;", "Lkotlin/collections/ArrayList;", "view", "executeOperations", "operations", "", "isPop", "", "findNamedViews", "namedViews", "", "", "startAnimations", "animationInfos", "Landroidx/fragment/app/DefaultSpecialEffectsController$AnimationInfo;", "awaitingContainerChanges", "", "startedAnyTransition", "startedTransitions", "", "startTransitions", "transitionInfos", "Landroidx/fragment/app/DefaultSpecialEffectsController$TransitionInfo;", "firstOut", "lastIn", "syncAnimations", "retainMatchingViews", "Landroidx/collection/ArrayMap;", "names", "", "AnimationInfo", "SpecialEffectsInfo", "TransitionInfo", "fragment_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
public final class DefaultSpecialEffectsController extends SpecialEffectsController
{
    public DefaultSpecialEffectsController(final ViewGroup viewGroup) {
        Intrinsics.checkNotNullParameter((Object)viewGroup, "container");
        super(viewGroup);
    }
    
    private final void applyContainerChanges(final Operation operation) {
        final View mView = operation.getFragment().mView;
        final Operation.State finalState = operation.getFinalState();
        Intrinsics.checkNotNullExpressionValue((Object)mView, "view");
        finalState.applyState(mView);
    }
    
    private final void captureTransitioningViews(final ArrayList<View> list, View child) {
        if (child instanceof ViewGroup) {
            final ViewGroup viewGroup = (ViewGroup)child;
            if (ViewGroupCompat.isTransitionGroup(viewGroup)) {
                if (!list.contains(child)) {
                    list.add(child);
                }
            }
            else {
                for (int childCount = viewGroup.getChildCount(), i = 0; i < childCount; ++i) {
                    child = viewGroup.getChildAt(i);
                    if (child.getVisibility() == 0) {
                        Intrinsics.checkNotNullExpressionValue((Object)child, "child");
                        this.captureTransitioningViews(list, child);
                    }
                }
            }
        }
        else if (!list.contains(child)) {
            list.add(child);
        }
    }
    
    private static final void executeOperations$lambda$2(final List list, final Operation operation, final DefaultSpecialEffectsController defaultSpecialEffectsController) {
        Intrinsics.checkNotNullParameter((Object)list, "$awaitingContainerChanges");
        Intrinsics.checkNotNullParameter((Object)operation, "$operation");
        Intrinsics.checkNotNullParameter((Object)defaultSpecialEffectsController, "this$0");
        if (list.contains(operation)) {
            list.remove(operation);
            defaultSpecialEffectsController.applyContainerChanges(operation);
        }
    }
    
    private final void findNamedViews(final Map<String, View> map, final View view) {
        final String transitionName = ViewCompat.getTransitionName(view);
        if (transitionName != null) {
            map.put(transitionName, view);
        }
        if (view instanceof ViewGroup) {
            final ViewGroup viewGroup = (ViewGroup)view;
            for (int childCount = viewGroup.getChildCount(), i = 0; i < childCount; ++i) {
                final View child = viewGroup.getChildAt(i);
                if (child.getVisibility() == 0) {
                    Intrinsics.checkNotNullExpressionValue((Object)child, "child");
                    this.findNamedViews(map, child);
                }
            }
        }
    }
    
    private final void retainMatchingViews(final ArrayMap<String, View> arrayMap, final Collection<String> collection) {
        final Set<Map.Entry<String, View>> entrySet = arrayMap.entrySet();
        Intrinsics.checkNotNullExpressionValue((Object)entrySet, "entries");
        CollectionsKt.retainAll((Iterable)entrySet, (Function1)new DefaultSpecialEffectsController$retainMatchingViews.DefaultSpecialEffectsController$retainMatchingViews$1((Collection)collection));
    }
    
    private final void startAnimations(final List<AnimationInfo> list, final List<Operation> list2, final boolean b, final Map<Operation, Boolean> map) {
        final Context context = this.getContainer().getContext();
        final List list3 = new ArrayList();
        final Iterator<AnimationInfo> iterator = list.iterator();
        boolean b2 = false;
        while (iterator.hasNext()) {
            final AnimationInfo animationInfo = iterator.next();
            if (((SpecialEffectsInfo)animationInfo).isVisibilityUnchanged()) {
                ((SpecialEffectsInfo)animationInfo).completeSpecialEffect();
            }
            else {
                Intrinsics.checkNotNullExpressionValue((Object)context, "context");
                final FragmentAnim.AnimationOrAnimator animation = animationInfo.getAnimation(context);
                if (animation == null) {
                    ((SpecialEffectsInfo)animationInfo).completeSpecialEffect();
                }
                else {
                    final Animator animator = animation.animator;
                    if (animator == null) {
                        list3.add(animationInfo);
                    }
                    else {
                        final Operation operation = ((SpecialEffectsInfo)animationInfo).getOperation();
                        final Fragment fragment = operation.getFragment();
                        if (Intrinsics.areEqual((Object)map.get(operation), (Object)true)) {
                            if (FragmentManager.isLoggingEnabled(2)) {
                                final StringBuilder sb = new StringBuilder();
                                sb.append("Ignoring Animator set on ");
                                sb.append(fragment);
                                sb.append(" as this Fragment was involved in a Transition.");
                                Log.v("FragmentManager", sb.toString());
                            }
                            ((SpecialEffectsInfo)animationInfo).completeSpecialEffect();
                        }
                        else {
                            final boolean b3 = operation.getFinalState() == State.GONE;
                            if (b3) {
                                list2.remove(operation);
                            }
                            final View mView = fragment.mView;
                            this.getContainer().startViewTransition(mView);
                            animator.addListener((Animator$AnimatorListener)new DefaultSpecialEffectsController$startAnimations.DefaultSpecialEffectsController$startAnimations$1(this, mView, b3, operation, animationInfo));
                            animator.setTarget((Object)mView);
                            animator.start();
                            if (FragmentManager.isLoggingEnabled(2)) {
                                final StringBuilder sb2 = new StringBuilder();
                                sb2.append("Animator from operation ");
                                sb2.append(operation);
                                sb2.append(" has started.");
                                Log.v("FragmentManager", sb2.toString());
                            }
                            ((SpecialEffectsInfo)animationInfo).getSignal().setOnCancelListener((CancellationSignal.OnCancelListener)new DefaultSpecialEffectsController$$ExternalSyntheticLambda5(animator, operation));
                            b2 = true;
                        }
                    }
                }
            }
        }
        for (final AnimationInfo animationInfo2 : list3) {
            final Operation operation2 = ((SpecialEffectsInfo)animationInfo2).getOperation();
            final Fragment fragment2 = operation2.getFragment();
            if (b) {
                if (FragmentManager.isLoggingEnabled(2)) {
                    final StringBuilder sb3 = new StringBuilder();
                    sb3.append("Ignoring Animation set on ");
                    sb3.append(fragment2);
                    sb3.append(" as Animations cannot run alongside Transitions.");
                    Log.v("FragmentManager", sb3.toString());
                }
                ((SpecialEffectsInfo)animationInfo2).completeSpecialEffect();
            }
            else if (b2) {
                if (FragmentManager.isLoggingEnabled(2)) {
                    final StringBuilder sb4 = new StringBuilder();
                    sb4.append("Ignoring Animation set on ");
                    sb4.append(fragment2);
                    sb4.append(" as Animations cannot run alongside Animators.");
                    Log.v("FragmentManager", sb4.toString());
                }
                ((SpecialEffectsInfo)animationInfo2).completeSpecialEffect();
            }
            else {
                final View mView2 = fragment2.mView;
                Intrinsics.checkNotNullExpressionValue((Object)context, "context");
                final FragmentAnim.AnimationOrAnimator animation2 = animationInfo2.getAnimation(context);
                if (animation2 == null) {
                    throw new IllegalStateException("Required value was null.".toString());
                }
                final Animation animation3 = animation2.animation;
                if (animation3 == null) {
                    throw new IllegalStateException("Required value was null.".toString());
                }
                if (operation2.getFinalState() != State.REMOVED) {
                    mView2.startAnimation(animation3);
                    ((SpecialEffectsInfo)animationInfo2).completeSpecialEffect();
                }
                else {
                    this.getContainer().startViewTransition(mView2);
                    final Animation animation4 = (Animation)new FragmentAnim.EndViewTransitionAnimation(animation3, this.getContainer(), mView2);
                    animation4.setAnimationListener((Animation$AnimationListener)new DefaultSpecialEffectsController$startAnimations.DefaultSpecialEffectsController$startAnimations$3(operation2, this, mView2, animationInfo2));
                    mView2.startAnimation(animation4);
                    if (FragmentManager.isLoggingEnabled(2)) {
                        final StringBuilder sb5 = new StringBuilder();
                        sb5.append("Animation from operation ");
                        sb5.append(operation2);
                        sb5.append(" has started.");
                        Log.v("FragmentManager", sb5.toString());
                    }
                }
                ((SpecialEffectsInfo)animationInfo2).getSignal().setOnCancelListener((CancellationSignal.OnCancelListener)new DefaultSpecialEffectsController$$ExternalSyntheticLambda6(mView2, this, animationInfo2, operation2));
            }
        }
    }
    
    private static final void startAnimations$lambda$3(final Animator animator, final Operation obj) {
        Intrinsics.checkNotNullParameter((Object)obj, "$operation");
        animator.end();
        if (FragmentManager.isLoggingEnabled(2)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Animator from operation ");
            sb.append(obj);
            sb.append(" has been canceled.");
            Log.v("FragmentManager", sb.toString());
        }
    }
    
    private static final void startAnimations$lambda$4(final View view, final DefaultSpecialEffectsController defaultSpecialEffectsController, final AnimationInfo animationInfo, final Operation obj) {
        Intrinsics.checkNotNullParameter((Object)defaultSpecialEffectsController, "this$0");
        Intrinsics.checkNotNullParameter((Object)animationInfo, "$animationInfo");
        Intrinsics.checkNotNullParameter((Object)obj, "$operation");
        view.clearAnimation();
        defaultSpecialEffectsController.getContainer().endViewTransition(view);
        ((SpecialEffectsInfo)animationInfo).completeSpecialEffect();
        if (FragmentManager.isLoggingEnabled(2)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Animation from operation ");
            sb.append(obj);
            sb.append(" has been cancelled.");
            Log.v("FragmentManager", sb.toString());
        }
    }
    
    private final Map<Operation, Boolean> startTransitions(final List<TransitionInfo> list, final List<Operation> list2, final boolean b, final Operation obj, final Operation obj2) {
        final Map map = new LinkedHashMap();
        final Iterable iterable = list;
        final Collection collection = new ArrayList();
        for (final Object next : iterable) {
            if (!((SpecialEffectsInfo)next).isVisibilityUnchanged()) {
                collection.add(next);
            }
        }
        final Iterable iterable2 = collection;
        final Collection collection2 = new ArrayList();
        for (final Object next2 : iterable2) {
            if (((TransitionInfo)next2).getHandlingImpl() != null) {
                collection2.add(next2);
            }
        }
        final Iterator iterator3 = collection2.iterator();
        FragmentTransitionImpl fragmentTransitionImpl = null;
        while (iterator3.hasNext()) {
            final TransitionInfo transitionInfo = (TransitionInfo)iterator3.next();
            final FragmentTransitionImpl handlingImpl = transitionInfo.getHandlingImpl();
            if (fragmentTransitionImpl != null && handlingImpl != fragmentTransitionImpl) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Mixing framework transitions and AndroidX transitions is not allowed. Fragment ");
                sb.append(((SpecialEffectsInfo)transitionInfo).getOperation().getFragment());
                sb.append(" returned Transition ");
                sb.append(transitionInfo.getTransition());
                sb.append(" which uses a different Transition type than other Fragments.");
                throw new IllegalArgumentException(sb.toString().toString());
            }
            fragmentTransitionImpl = handlingImpl;
        }
        if (fragmentTransitionImpl == null) {
            for (final TransitionInfo transitionInfo2 : list) {
                map.put(((SpecialEffectsInfo)transitionInfo2).getOperation(), false);
                ((SpecialEffectsInfo)transitionInfo2).completeSpecialEffect();
            }
            return map;
        }
        final View view = new View(this.getContainer().getContext());
        final Rect rect = new Rect();
        final ArrayList<View> list3 = new ArrayList<View>();
        final ArrayList list4 = new ArrayList();
        final ArrayMap arrayMap = new ArrayMap();
        final Iterator<TransitionInfo> iterator5 = list.iterator();
        View view2 = null;
        Object o = null;
        boolean b2 = false;
        while (iterator5.hasNext()) {
            final TransitionInfo transitionInfo3 = iterator5.next();
            if (transitionInfo3.hasSharedElementTransition() && obj != null && obj2 != null) {
                final Object wrapTransitionInSet = fragmentTransitionImpl.wrapTransitionInSet(fragmentTransitionImpl.cloneTransition(transitionInfo3.getSharedElementTransition()));
                final ArrayList<String> sharedElementSourceNames = obj2.getFragment().getSharedElementSourceNames();
                Intrinsics.checkNotNullExpressionValue((Object)sharedElementSourceNames, "lastIn.fragment.sharedElementSourceNames");
                final ArrayList<String> sharedElementSourceNames2 = obj.getFragment().getSharedElementSourceNames();
                Intrinsics.checkNotNullExpressionValue((Object)sharedElementSourceNames2, "firstOut.fragment.sharedElementSourceNames");
                final ArrayList<String> sharedElementTargetNames = obj.getFragment().getSharedElementTargetNames();
                Intrinsics.checkNotNullExpressionValue((Object)sharedElementTargetNames, "firstOut.fragment.sharedElementTargetNames");
                for (int size = sharedElementTargetNames.size(), i = 0; i < size; ++i) {
                    final int index = sharedElementSourceNames.indexOf(sharedElementTargetNames.get(i));
                    if (index != -1) {
                        sharedElementSourceNames.set(index, sharedElementSourceNames2.get(i));
                    }
                }
                final ArrayList<String> sharedElementTargetNames2 = obj2.getFragment().getSharedElementTargetNames();
                Intrinsics.checkNotNullExpressionValue((Object)sharedElementTargetNames2, "lastIn.fragment.sharedElementTargetNames");
                Pair pair;
                if (!b) {
                    pair = TuplesKt.to((Object)obj.getFragment().getExitTransitionCallback(), (Object)obj2.getFragment().getEnterTransitionCallback());
                }
                else {
                    pair = TuplesKt.to((Object)obj.getFragment().getEnterTransitionCallback(), (Object)obj2.getFragment().getExitTransitionCallback());
                }
                final SharedElementCallback sharedElementCallback = (SharedElementCallback)pair.component1();
                final SharedElementCallback sharedElementCallback2 = (SharedElementCallback)pair.component2();
                final int size2 = sharedElementSourceNames.size();
                int j = 0;
                o = wrapTransitionInSet;
                while (j < size2) {
                    arrayMap.put((String)sharedElementSourceNames.get(j), (String)sharedElementTargetNames2.get(j));
                    ++j;
                }
                if (FragmentManager.isLoggingEnabled(2)) {
                    Log.v("FragmentManager", ">>> entering view names <<<");
                    for (final String str : sharedElementTargetNames2) {
                        final StringBuilder sb2 = new StringBuilder();
                        sb2.append("Name: ");
                        sb2.append(str);
                        Log.v("FragmentManager", sb2.toString());
                    }
                    Log.v("FragmentManager", ">>> exiting view names <<<");
                    for (final String str2 : sharedElementSourceNames) {
                        final StringBuilder sb3 = new StringBuilder();
                        sb3.append("Name: ");
                        sb3.append(str2);
                        Log.v("FragmentManager", sb3.toString());
                    }
                }
                final ArrayMap arrayMap2 = new ArrayMap();
                final Map map2 = arrayMap2;
                final View mView = obj.getFragment().mView;
                Intrinsics.checkNotNullExpressionValue((Object)mView, "firstOut.fragment.mView");
                this.findNamedViews(map2, mView);
                final Collection collection3 = sharedElementSourceNames;
                arrayMap2.retainAll(collection3);
                if (sharedElementCallback != null) {
                    if (FragmentManager.isLoggingEnabled(2)) {
                        final StringBuilder sb4 = new StringBuilder();
                        sb4.append("Executing exit callback for operation ");
                        sb4.append(obj);
                        Log.v("FragmentManager", sb4.toString());
                    }
                    sharedElementCallback.onMapSharedElements(sharedElementSourceNames, map2);
                    int index2 = sharedElementSourceNames.size() - 1;
                    if (index2 >= 0) {
                        while (true) {
                            final int n = index2 - 1;
                            final String s = sharedElementSourceNames.get(index2);
                            final View view3 = (View)arrayMap2.get(s);
                            if (view3 == null) {
                                arrayMap.remove(s);
                            }
                            else if (!Intrinsics.areEqual((Object)s, (Object)ViewCompat.getTransitionName(view3))) {
                                arrayMap.put(ViewCompat.getTransitionName(view3), (String)arrayMap.remove(s));
                            }
                            if (n < 0) {
                                break;
                            }
                            index2 = n;
                        }
                    }
                }
                else {
                    arrayMap.retainAll(arrayMap2.keySet());
                }
                final ArrayMap arrayMap3 = new ArrayMap();
                final Map map3 = arrayMap3;
                final View mView2 = obj2.getFragment().mView;
                Intrinsics.checkNotNullExpressionValue((Object)mView2, "lastIn.fragment.mView");
                this.findNamedViews(map3, mView2);
                final Collection collection4 = sharedElementTargetNames2;
                arrayMap3.retainAll(collection4);
                arrayMap3.retainAll(arrayMap.values());
                if (sharedElementCallback2 != null) {
                    if (FragmentManager.isLoggingEnabled(2)) {
                        final StringBuilder sb5 = new StringBuilder();
                        sb5.append("Executing enter callback for operation ");
                        sb5.append(obj2);
                        Log.v("FragmentManager", sb5.toString());
                    }
                    sharedElementCallback2.onMapSharedElements(sharedElementTargetNames2, map3);
                    int index3 = sharedElementTargetNames2.size() - 1;
                    if (index3 >= 0) {
                        while (true) {
                            final int n2 = index3 - 1;
                            final String s2 = sharedElementTargetNames2.get(index3);
                            final View view4 = (View)arrayMap3.get(s2);
                            if (view4 == null) {
                                Intrinsics.checkNotNullExpressionValue((Object)s2, "name");
                                final String keyForValue = FragmentTransition.findKeyForValue(arrayMap, s2);
                                if (keyForValue != null) {
                                    arrayMap.remove(keyForValue);
                                }
                            }
                            else if (!Intrinsics.areEqual((Object)s2, (Object)ViewCompat.getTransitionName(view4))) {
                                Intrinsics.checkNotNullExpressionValue((Object)s2, "name");
                                final String keyForValue2 = FragmentTransition.findKeyForValue(arrayMap, s2);
                                if (keyForValue2 != null) {
                                    arrayMap.put(keyForValue2, ViewCompat.getTransitionName(view4));
                                }
                            }
                            if (n2 < 0) {
                                break;
                            }
                            index3 = n2;
                        }
                    }
                }
                else {
                    FragmentTransition.retainValues(arrayMap, arrayMap3);
                }
                final Set keySet = arrayMap.keySet();
                Intrinsics.checkNotNullExpressionValue((Object)keySet, "sharedElementNameMapping.keys");
                this.retainMatchingViews(arrayMap2, keySet);
                final Collection values = arrayMap.values();
                Intrinsics.checkNotNullExpressionValue((Object)values, "sharedElementNameMapping.values");
                this.retainMatchingViews(arrayMap3, values);
                if (arrayMap.isEmpty()) {
                    list3.clear();
                    list4.clear();
                    o = null;
                }
                else {
                    FragmentTransition.callSharedElementStartEnd(obj2.getFragment(), obj.getFragment(), b, arrayMap2, true);
                    OneShotPreDrawListener.add((View)this.getContainer(), new DefaultSpecialEffectsController$$ExternalSyntheticLambda1(obj2, obj, b, arrayMap3));
                    list3.addAll(arrayMap2.values());
                    if (collection3.isEmpty() ^ true) {
                        view2 = (View)arrayMap2.get(sharedElementSourceNames.get(0));
                        fragmentTransitionImpl.setEpicenter(o, view2);
                    }
                    list4.addAll(arrayMap3.values());
                    if (collection4.isEmpty() ^ true) {
                        final View view5 = (View)arrayMap3.get(sharedElementTargetNames2.get(0));
                        if (view5 != null) {
                            OneShotPreDrawListener.add((View)this.getContainer(), new DefaultSpecialEffectsController$$ExternalSyntheticLambda2(fragmentTransitionImpl, view5, rect));
                            b2 = true;
                        }
                    }
                    fragmentTransitionImpl.setSharedElementTargets(o, view, list3);
                    fragmentTransitionImpl.scheduleRemoveTargets(o, null, null, null, null, o, list4);
                    map.put(obj, true);
                    map.put(obj2, true);
                }
            }
        }
        final ArrayList<View> list5 = list3;
        final View view6 = view;
        final ArrayList list6 = new ArrayList();
        final Iterator<TransitionInfo> iterator8 = list.iterator();
        Object mergeTransitionsTogether = null;
        Object mergeTransitionsTogether2 = null;
        final View view7 = view2;
        final Object o2 = o;
        final Iterator<TransitionInfo> iterator9 = iterator8;
        final ArrayList<View> list7 = list5;
        final View view8 = view6;
        while (iterator9.hasNext()) {
            final TransitionInfo transitionInfo4 = iterator9.next();
            if (((SpecialEffectsInfo)transitionInfo4).isVisibilityUnchanged()) {
                map.put(((SpecialEffectsInfo)transitionInfo4).getOperation(), false);
                ((SpecialEffectsInfo)transitionInfo4).completeSpecialEffect();
            }
            else {
                final Object cloneTransition = fragmentTransitionImpl.cloneTransition(transitionInfo4.getTransition());
                final Operation operation = ((SpecialEffectsInfo)transitionInfo4).getOperation();
                final boolean b3 = o2 != null && (operation == obj || operation == obj2);
                if (cloneTransition == null) {
                    if (b3) {
                        continue;
                    }
                    map.put(operation, false);
                    ((SpecialEffectsInfo)transitionInfo4).completeSpecialEffect();
                }
                else {
                    final ArrayList list8 = new ArrayList();
                    final View mView3 = operation.getFragment().mView;
                    Intrinsics.checkNotNullExpressionValue((Object)mView3, "operation.fragment.mView");
                    this.captureTransitioningViews(list8, mView3);
                    if (b3) {
                        if (operation == obj) {
                            list8.removeAll(CollectionsKt.toSet((Iterable)list7));
                        }
                        else {
                            list8.removeAll(CollectionsKt.toSet((Iterable)list4));
                        }
                    }
                    if (list8.isEmpty()) {
                        fragmentTransitionImpl.addTarget(cloneTransition, view8);
                    }
                    else {
                        fragmentTransitionImpl.addTargets(cloneTransition, list8);
                        fragmentTransitionImpl.scheduleRemoveTargets(cloneTransition, cloneTransition, list8, null, null, null, null);
                        if (operation.getFinalState() == State.GONE) {
                            list2.remove(operation);
                            final ArrayList list9 = new ArrayList<View>(list8);
                            list9.remove(operation.getFragment().mView);
                            fragmentTransitionImpl.scheduleHideFragmentView(cloneTransition, operation.getFragment().mView, (ArrayList<View>)list9);
                            OneShotPreDrawListener.add((View)this.getContainer(), new DefaultSpecialEffectsController$$ExternalSyntheticLambda3(list8));
                        }
                    }
                    if (operation.getFinalState() == State.VISIBLE) {
                        list6.addAll(list8);
                        if (b2) {
                            fragmentTransitionImpl.setEpicenter(cloneTransition, rect);
                        }
                    }
                    else {
                        fragmentTransitionImpl.setEpicenter(cloneTransition, view7);
                    }
                    map.put(operation, true);
                    if (transitionInfo4.isOverlapAllowed()) {
                        mergeTransitionsTogether2 = fragmentTransitionImpl.mergeTransitionsTogether(mergeTransitionsTogether2, cloneTransition, null);
                    }
                    else {
                        mergeTransitionsTogether = fragmentTransitionImpl.mergeTransitionsTogether(mergeTransitionsTogether, cloneTransition, null);
                    }
                }
            }
        }
        final Object mergeTransitionsInSequence = fragmentTransitionImpl.mergeTransitionsInSequence(mergeTransitionsTogether2, mergeTransitionsTogether, o2);
        if (mergeTransitionsInSequence == null) {
            return map;
        }
        final Collection collection5 = new ArrayList();
        for (final Object next3 : iterable) {
            if (!((SpecialEffectsInfo)next3).isVisibilityUnchanged()) {
                collection5.add(next3);
            }
        }
        for (final TransitionInfo transitionInfo5 : collection5) {
            final Object transition = transitionInfo5.getTransition();
            final Operation operation2 = ((SpecialEffectsInfo)transitionInfo5).getOperation();
            final boolean b4 = o2 != null && (operation2 == obj || operation2 == obj2);
            if (transition != null || b4) {
                if (!ViewCompat.isLaidOut((View)this.getContainer())) {
                    if (FragmentManager.isLoggingEnabled(2)) {
                        final StringBuilder sb6 = new StringBuilder();
                        sb6.append("SpecialEffectsController: Container ");
                        sb6.append(this.getContainer());
                        sb6.append(" has not been laid out. Completing operation ");
                        sb6.append(operation2);
                        Log.v("FragmentManager", sb6.toString());
                    }
                    ((SpecialEffectsInfo)transitionInfo5).completeSpecialEffect();
                }
                else {
                    fragmentTransitionImpl.setListenerForTransitionEnd(((SpecialEffectsInfo)transitionInfo5).getOperation().getFragment(), mergeTransitionsInSequence, ((SpecialEffectsInfo)transitionInfo5).getSignal(), new DefaultSpecialEffectsController$$ExternalSyntheticLambda4(transitionInfo5, operation2));
                }
            }
        }
        if (!ViewCompat.isLaidOut((View)this.getContainer())) {
            return map;
        }
        final List list10 = list6;
        FragmentTransition.setViewVisibility(list10, 4);
        final ArrayList<String> prepareSetNameOverridesReordered = fragmentTransitionImpl.prepareSetNameOverridesReordered(list4);
        if (FragmentManager.isLoggingEnabled(2)) {
            Log.v("FragmentManager", ">>>>> Beginning transition <<<<<");
            Log.v("FragmentManager", ">>>>> SharedElementFirstOutViews <<<<<");
            for (final View next4 : list7) {
                Intrinsics.checkNotNullExpressionValue((Object)next4, "sharedElementFirstOutViews");
                final View obj3 = next4;
                final StringBuilder sb7 = new StringBuilder();
                sb7.append("View: ");
                sb7.append(obj3);
                sb7.append(" Name: ");
                sb7.append(ViewCompat.getTransitionName(obj3));
                Log.v("FragmentManager", sb7.toString());
            }
            Log.v("FragmentManager", ">>>>> SharedElementLastInViews <<<<<");
            for (final Object next5 : list4) {
                Intrinsics.checkNotNullExpressionValue(next5, "sharedElementLastInViews");
                final View obj4 = (View)next5;
                final StringBuilder sb8 = new StringBuilder();
                sb8.append("View: ");
                sb8.append(obj4);
                sb8.append(" Name: ");
                sb8.append(ViewCompat.getTransitionName(obj4));
                Log.v("FragmentManager", sb8.toString());
            }
        }
        fragmentTransitionImpl.beginDelayedTransition(this.getContainer(), mergeTransitionsInSequence);
        fragmentTransitionImpl.setNameOverridesReordered((View)this.getContainer(), list7, list4, prepareSetNameOverridesReordered, arrayMap);
        FragmentTransition.setViewVisibility(list10, 0);
        fragmentTransitionImpl.swapSharedElementTargets(o2, list7, list4);
        return map;
    }
    
    private static final void startTransitions$lambda$10(final FragmentTransitionImpl fragmentTransitionImpl, final View view, final Rect rect) {
        Intrinsics.checkNotNullParameter((Object)fragmentTransitionImpl, "$impl");
        Intrinsics.checkNotNullParameter((Object)rect, "$lastInEpicenterRect");
        fragmentTransitionImpl.getBoundsOnScreen(view, rect);
    }
    
    private static final void startTransitions$lambda$11(final ArrayList list) {
        Intrinsics.checkNotNullParameter((Object)list, "$transitioningViews");
        FragmentTransition.setViewVisibility(list, 4);
    }
    
    private static final void startTransitions$lambda$14$lambda$13(final TransitionInfo transitionInfo, final Operation obj) {
        Intrinsics.checkNotNullParameter((Object)transitionInfo, "$transitionInfo");
        Intrinsics.checkNotNullParameter((Object)obj, "$operation");
        ((SpecialEffectsInfo)transitionInfo).completeSpecialEffect();
        if (FragmentManager.isLoggingEnabled(2)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Transition for operation ");
            sb.append(obj);
            sb.append(" has completed");
            Log.v("FragmentManager", sb.toString());
        }
    }
    
    private static final void startTransitions$lambda$9(final Operation operation, final Operation operation2, final boolean b, final ArrayMap arrayMap) {
        Intrinsics.checkNotNullParameter((Object)arrayMap, "$lastInViews");
        FragmentTransition.callSharedElementStartEnd(operation.getFragment(), operation2.getFragment(), b, arrayMap, false);
    }
    
    private final void syncAnimations(final List<? extends Operation> list) {
        final Fragment fragment = ((Operation)CollectionsKt.last((List)list)).getFragment();
        for (final Operation operation : list) {
            operation.getFragment().mAnimationInfo.mEnterAnim = fragment.mAnimationInfo.mEnterAnim;
            operation.getFragment().mAnimationInfo.mExitAnim = fragment.mAnimationInfo.mExitAnim;
            operation.getFragment().mAnimationInfo.mPopEnterAnim = fragment.mAnimationInfo.mPopEnterAnim;
            operation.getFragment().mAnimationInfo.mPopExitAnim = fragment.mAnimationInfo.mPopExitAnim;
        }
    }
    
    @Override
    public void executeOperations(final List<? extends Operation> list, final boolean b) {
        Intrinsics.checkNotNullParameter((Object)list, "operations");
        final Iterator iterator = list.iterator();
        while (true) {
            Operation.State.Companion companion;
            View mView;
            Operation operation;
            do {
                final boolean hasNext = iterator.hasNext();
                final Operation operation2 = null;
                if (!hasNext) {
                    final Object next = null;
                    final Operation operation3 = (Operation)next;
                    final ListIterator listIterator = list.listIterator(list.size());
                    Operation.State.Companion companion2;
                    View mView2;
                    Operation operation4;
                    Object previous;
                    do {
                        previous = operation2;
                        if (!listIterator.hasPrevious()) {
                            break;
                        }
                        previous = listIterator.previous();
                        operation4 = (Operation)previous;
                        companion2 = State.Companion;
                        mView2 = operation4.getFragment().mView;
                        Intrinsics.checkNotNullExpressionValue((Object)mView2, "operation.fragment.mView");
                    } while (companion2.asOperationState(mView2) == State.VISIBLE || operation4.getFinalState() != State.VISIBLE);
                    final Operation operation5 = (Operation)previous;
                    if (FragmentManager.isLoggingEnabled(2)) {
                        final StringBuilder sb = new StringBuilder();
                        sb.append("Executing operations from ");
                        sb.append(operation3);
                        sb.append(" to ");
                        sb.append(operation5);
                        Log.v("FragmentManager", sb.toString());
                    }
                    final List list2 = new ArrayList();
                    final List list3 = new ArrayList();
                    final List mutableList = CollectionsKt.toMutableList((Collection)list);
                    this.syncAnimations(list);
                    for (final Operation operation6 : list) {
                        final CancellationSignal cancellationSignal = new CancellationSignal();
                        operation6.markStartedSpecialEffect(cancellationSignal);
                        list2.add(new AnimationInfo(operation6, cancellationSignal, b));
                        final CancellationSignal cancellationSignal2 = new CancellationSignal();
                        operation6.markStartedSpecialEffect(cancellationSignal2);
                        list3.add(new TransitionInfo(operation6, cancellationSignal2, b, b ? (operation6 == operation3) : (operation6 == operation5)));
                        operation6.addCompletionListener(new DefaultSpecialEffectsController$$ExternalSyntheticLambda0(mutableList, operation6, this));
                    }
                    final Map<Operation, Boolean> startTransitions = this.startTransitions(list3, mutableList, b, operation3, operation5);
                    this.startAnimations(list2, mutableList, startTransitions.containsValue(true), startTransitions);
                    final Iterator iterator3 = mutableList.iterator();
                    while (iterator3.hasNext()) {
                        this.applyContainerChanges((Operation)iterator3.next());
                    }
                    mutableList.clear();
                    if (FragmentManager.isLoggingEnabled(2)) {
                        final StringBuilder sb2 = new StringBuilder();
                        sb2.append("Completed executing operations from ");
                        sb2.append(operation3);
                        sb2.append(" to ");
                        sb2.append(operation5);
                        Log.v("FragmentManager", sb2.toString());
                    }
                    return;
                }
                final Object next = iterator.next();
                operation = (Operation)next;
                companion = State.Companion;
                mView = operation.getFragment().mView;
                Intrinsics.checkNotNullExpressionValue((Object)mView, "operation.fragment.mView");
            } while (companion.asOperationState(mView) != State.VISIBLE || operation.getFinalState() == State.VISIBLE);
            continue;
        }
    }
    
    @Metadata(d1 = { "\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\b\u0002\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007¢\u0006\u0002\u0010\bJ\u0010\u0010\f\u001a\u0004\u0018\u00010\n2\u0006\u0010\r\u001a\u00020\u000eR\u0010\u0010\t\u001a\u0004\u0018\u00010\nX\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u0007X\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u000f" }, d2 = { "Landroidx/fragment/app/DefaultSpecialEffectsController$AnimationInfo;", "Landroidx/fragment/app/DefaultSpecialEffectsController$SpecialEffectsInfo;", "operation", "Landroidx/fragment/app/SpecialEffectsController$Operation;", "signal", "Landroidx/core/os/CancellationSignal;", "isPop", "", "(Landroidx/fragment/app/SpecialEffectsController$Operation;Landroidx/core/os/CancellationSignal;Z)V", "animation", "Landroidx/fragment/app/FragmentAnim$AnimationOrAnimator;", "isAnimLoaded", "getAnimation", "context", "Landroid/content/Context;", "fragment_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
    private static final class AnimationInfo extends SpecialEffectsInfo
    {
        private FragmentAnim.AnimationOrAnimator animation;
        private boolean isAnimLoaded;
        private final boolean isPop;
        
        public AnimationInfo(final Operation operation, final CancellationSignal cancellationSignal, final boolean isPop) {
            Intrinsics.checkNotNullParameter((Object)operation, "operation");
            Intrinsics.checkNotNullParameter((Object)cancellationSignal, "signal");
            super(operation, cancellationSignal);
            this.isPop = isPop;
        }
        
        public final FragmentAnim.AnimationOrAnimator getAnimation(final Context context) {
            Intrinsics.checkNotNullParameter((Object)context, "context");
            Object animation;
            if (this.isAnimLoaded) {
                animation = this.animation;
            }
            else {
                animation = FragmentAnim.loadAnimation(context, ((SpecialEffectsInfo)this).getOperation().getFragment(), ((SpecialEffectsInfo)this).getOperation().getFinalState() == State.VISIBLE, this.isPop);
                this.animation = (FragmentAnim.AnimationOrAnimator)animation;
                this.isAnimLoaded = true;
            }
            return (FragmentAnim.AnimationOrAnimator)animation;
        }
    }
    
    @Metadata(d1 = { "\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0006\n\u0002\u0010\u0002\n\u0000\b\u0012\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\u0006\u0010\u000e\u001a\u00020\u000fR\u0011\u0010\u0007\u001a\u00020\b8F¢\u0006\u0006\u001a\u0004\b\u0007\u0010\tR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000bR\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\r¨\u0006\u0010" }, d2 = { "Landroidx/fragment/app/DefaultSpecialEffectsController$SpecialEffectsInfo;", "", "operation", "Landroidx/fragment/app/SpecialEffectsController$Operation;", "signal", "Landroidx/core/os/CancellationSignal;", "(Landroidx/fragment/app/SpecialEffectsController$Operation;Landroidx/core/os/CancellationSignal;)V", "isVisibilityUnchanged", "", "()Z", "getOperation", "()Landroidx/fragment/app/SpecialEffectsController$Operation;", "getSignal", "()Landroidx/core/os/CancellationSignal;", "completeSpecialEffect", "", "fragment_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
    private static class SpecialEffectsInfo
    {
        private final Operation operation;
        private final CancellationSignal signal;
        
        public SpecialEffectsInfo(final Operation operation, final CancellationSignal signal) {
            Intrinsics.checkNotNullParameter((Object)operation, "operation");
            Intrinsics.checkNotNullParameter((Object)signal, "signal");
            this.operation = operation;
            this.signal = signal;
        }
        
        public final void completeSpecialEffect() {
            this.operation.completeSpecialEffect(this.signal);
        }
        
        public final Operation getOperation() {
            return this.operation;
        }
        
        public final CancellationSignal getSignal() {
            return this.signal;
        }
        
        public final boolean isVisibilityUnchanged() {
            final Operation.State.Companion companion = State.Companion;
            final View mView = this.operation.getFragment().mView;
            Intrinsics.checkNotNullExpressionValue((Object)mView, "operation.fragment.mView");
            final Operation.State operationState = companion.asOperationState(mView);
            final Operation.State finalState = this.operation.getFinalState();
            return operationState == finalState || (operationState != State.VISIBLE && finalState != State.VISIBLE);
        }
    }
    
    @Metadata(d1 = { "\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0000\n\u0002\b\u0006\b\u0002\u0018\u00002\u00020\u0001B%\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\u0007¢\u0006\u0002\u0010\tJ\u0014\u0010\f\u001a\u0004\u0018\u00010\u000b2\b\u0010\u0014\u001a\u0004\u0018\u00010\u0011H\u0002J\u0006\u0010\u0016\u001a\u00020\u0007R\u0013\u0010\n\u001a\u0004\u0018\u00010\u000b8F¢\u0006\u0006\u001a\u0004\b\f\u0010\rR\u0011\u0010\u000e\u001a\u00020\u0007¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\u000fR\u0013\u0010\u0010\u001a\u0004\u0018\u00010\u0011¢\u0006\b\n\u0000\u001a\u0004\b\u0012\u0010\u0013R\u0013\u0010\u0014\u001a\u0004\u0018\u00010\u0011¢\u0006\b\n\u0000\u001a\u0004\b\u0015\u0010\u0013¨\u0006\u0017" }, d2 = { "Landroidx/fragment/app/DefaultSpecialEffectsController$TransitionInfo;", "Landroidx/fragment/app/DefaultSpecialEffectsController$SpecialEffectsInfo;", "operation", "Landroidx/fragment/app/SpecialEffectsController$Operation;", "signal", "Landroidx/core/os/CancellationSignal;", "isPop", "", "providesSharedElementTransition", "(Landroidx/fragment/app/SpecialEffectsController$Operation;Landroidx/core/os/CancellationSignal;ZZ)V", "handlingImpl", "Landroidx/fragment/app/FragmentTransitionImpl;", "getHandlingImpl", "()Landroidx/fragment/app/FragmentTransitionImpl;", "isOverlapAllowed", "()Z", "sharedElementTransition", "", "getSharedElementTransition", "()Ljava/lang/Object;", "transition", "getTransition", "hasSharedElementTransition", "fragment_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
    private static final class TransitionInfo extends SpecialEffectsInfo
    {
        private final boolean isOverlapAllowed;
        private final Object sharedElementTransition;
        private final Object transition;
        
        public TransitionInfo(final Operation operation, final CancellationSignal cancellationSignal, final boolean b, final boolean b2) {
            Intrinsics.checkNotNullParameter((Object)operation, "operation");
            Intrinsics.checkNotNullParameter((Object)cancellationSignal, "signal");
            super(operation, cancellationSignal);
            Object transition;
            if (operation.getFinalState() == State.VISIBLE) {
                final Fragment fragment = operation.getFragment();
                if (b) {
                    transition = fragment.getReenterTransition();
                }
                else {
                    transition = fragment.getEnterTransition();
                }
            }
            else {
                final Fragment fragment2 = operation.getFragment();
                if (b) {
                    transition = fragment2.getReturnTransition();
                }
                else {
                    transition = fragment2.getExitTransition();
                }
            }
            this.transition = transition;
            boolean isOverlapAllowed;
            if (operation.getFinalState() == State.VISIBLE) {
                if (b) {
                    isOverlapAllowed = operation.getFragment().getAllowReturnTransitionOverlap();
                }
                else {
                    isOverlapAllowed = operation.getFragment().getAllowEnterTransitionOverlap();
                }
            }
            else {
                isOverlapAllowed = true;
            }
            this.isOverlapAllowed = isOverlapAllowed;
            Object sharedElementTransition;
            if (b2) {
                if (b) {
                    sharedElementTransition = operation.getFragment().getSharedElementReturnTransition();
                }
                else {
                    sharedElementTransition = operation.getFragment().getSharedElementEnterTransition();
                }
            }
            else {
                sharedElementTransition = null;
            }
            this.sharedElementTransition = sharedElementTransition;
        }
        
        private final FragmentTransitionImpl getHandlingImpl(final Object obj) {
            if (obj == null) {
                return null;
            }
            if (FragmentTransition.PLATFORM_IMPL != null && FragmentTransition.PLATFORM_IMPL.canHandle(obj)) {
                return FragmentTransition.PLATFORM_IMPL;
            }
            if (FragmentTransition.SUPPORT_IMPL != null && FragmentTransition.SUPPORT_IMPL.canHandle(obj)) {
                return FragmentTransition.SUPPORT_IMPL;
            }
            final StringBuilder sb = new StringBuilder();
            sb.append("Transition ");
            sb.append(obj);
            sb.append(" for fragment ");
            sb.append(((SpecialEffectsInfo)this).getOperation().getFragment());
            sb.append(" is not a valid framework Transition or AndroidX Transition");
            throw new IllegalArgumentException(sb.toString());
        }
        
        public final FragmentTransitionImpl getHandlingImpl() {
            final FragmentTransitionImpl handlingImpl = this.getHandlingImpl(this.transition);
            final FragmentTransitionImpl handlingImpl2 = this.getHandlingImpl(this.sharedElementTransition);
            if (handlingImpl == null || handlingImpl2 == null || handlingImpl == handlingImpl2) {
                FragmentTransitionImpl fragmentTransitionImpl;
                if ((fragmentTransitionImpl = handlingImpl) == null) {
                    fragmentTransitionImpl = handlingImpl2;
                }
                return fragmentTransitionImpl;
            }
            final StringBuilder sb = new StringBuilder();
            sb.append("Mixing framework transitions and AndroidX transitions is not allowed. Fragment ");
            sb.append(((SpecialEffectsInfo)this).getOperation().getFragment());
            sb.append(" returned Transition ");
            sb.append(this.transition);
            sb.append(" which uses a different Transition  type than its shared element transition ");
            sb.append(this.sharedElementTransition);
            throw new IllegalArgumentException(sb.toString().toString());
        }
        
        public final Object getSharedElementTransition() {
            return this.sharedElementTransition;
        }
        
        public final Object getTransition() {
            return this.transition;
        }
        
        public final boolean hasSharedElementTransition() {
            return this.sharedElementTransition != null;
        }
        
        public final boolean isOverlapAllowed() {
            return this.isOverlapAllowed;
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.fragment.app;

import androidx.lifecycle.Lifecycle;
import java.lang.reflect.Modifier;
import androidx.fragment.app.strictmode.FragmentStrictMode;
import androidx.core.view.ViewCompat;
import android.view.View;
import android.view.ViewGroup;
import android.os.Bundle;
import java.util.Iterator;
import java.util.Collection;
import java.util.ArrayList;

public abstract class FragmentTransaction
{
    static final int OP_ADD = 1;
    static final int OP_ATTACH = 7;
    static final int OP_DETACH = 6;
    static final int OP_HIDE = 4;
    static final int OP_NULL = 0;
    static final int OP_REMOVE = 3;
    static final int OP_REPLACE = 2;
    static final int OP_SET_MAX_LIFECYCLE = 10;
    static final int OP_SET_PRIMARY_NAV = 8;
    static final int OP_SHOW = 5;
    static final int OP_UNSET_PRIMARY_NAV = 9;
    public static final int TRANSIT_ENTER_MASK = 4096;
    public static final int TRANSIT_EXIT_MASK = 8192;
    public static final int TRANSIT_FRAGMENT_CLOSE = 8194;
    public static final int TRANSIT_FRAGMENT_FADE = 4099;
    public static final int TRANSIT_FRAGMENT_MATCH_ACTIVITY_CLOSE = 8197;
    public static final int TRANSIT_FRAGMENT_MATCH_ACTIVITY_OPEN = 4100;
    public static final int TRANSIT_FRAGMENT_OPEN = 4097;
    public static final int TRANSIT_NONE = 0;
    public static final int TRANSIT_UNSET = -1;
    boolean mAddToBackStack;
    boolean mAllowAddToBackStack;
    int mBreadCrumbShortTitleRes;
    CharSequence mBreadCrumbShortTitleText;
    int mBreadCrumbTitleRes;
    CharSequence mBreadCrumbTitleText;
    private final ClassLoader mClassLoader;
    ArrayList<Runnable> mCommitRunnables;
    int mEnterAnim;
    int mExitAnim;
    private final FragmentFactory mFragmentFactory;
    String mName;
    ArrayList<Op> mOps;
    int mPopEnterAnim;
    int mPopExitAnim;
    boolean mReorderingAllowed;
    ArrayList<String> mSharedElementSourceNames;
    ArrayList<String> mSharedElementTargetNames;
    int mTransition;
    
    @Deprecated
    public FragmentTransaction() {
        this.mOps = new ArrayList<Op>();
        this.mAllowAddToBackStack = true;
        this.mReorderingAllowed = false;
        this.mFragmentFactory = null;
        this.mClassLoader = null;
    }
    
    FragmentTransaction(final FragmentFactory mFragmentFactory, final ClassLoader mClassLoader) {
        this.mOps = new ArrayList<Op>();
        this.mAllowAddToBackStack = true;
        this.mReorderingAllowed = false;
        this.mFragmentFactory = mFragmentFactory;
        this.mClassLoader = mClassLoader;
    }
    
    FragmentTransaction(final FragmentFactory fragmentFactory, final ClassLoader classLoader, final FragmentTransaction fragmentTransaction) {
        this(fragmentFactory, classLoader);
        final Iterator<Op> iterator = fragmentTransaction.mOps.iterator();
        while (iterator.hasNext()) {
            this.mOps.add(new Op(iterator.next()));
        }
        this.mEnterAnim = fragmentTransaction.mEnterAnim;
        this.mExitAnim = fragmentTransaction.mExitAnim;
        this.mPopEnterAnim = fragmentTransaction.mPopEnterAnim;
        this.mPopExitAnim = fragmentTransaction.mPopExitAnim;
        this.mTransition = fragmentTransaction.mTransition;
        this.mAddToBackStack = fragmentTransaction.mAddToBackStack;
        this.mAllowAddToBackStack = fragmentTransaction.mAllowAddToBackStack;
        this.mName = fragmentTransaction.mName;
        this.mBreadCrumbShortTitleRes = fragmentTransaction.mBreadCrumbShortTitleRes;
        this.mBreadCrumbShortTitleText = fragmentTransaction.mBreadCrumbShortTitleText;
        this.mBreadCrumbTitleRes = fragmentTransaction.mBreadCrumbTitleRes;
        this.mBreadCrumbTitleText = fragmentTransaction.mBreadCrumbTitleText;
        if (fragmentTransaction.mSharedElementSourceNames != null) {
            (this.mSharedElementSourceNames = new ArrayList<String>()).addAll(fragmentTransaction.mSharedElementSourceNames);
        }
        if (fragmentTransaction.mSharedElementTargetNames != null) {
            (this.mSharedElementTargetNames = new ArrayList<String>()).addAll(fragmentTransaction.mSharedElementTargetNames);
        }
        this.mReorderingAllowed = fragmentTransaction.mReorderingAllowed;
    }
    
    private Fragment createFragment(final Class<? extends Fragment> clazz, final Bundle arguments) {
        final FragmentFactory mFragmentFactory = this.mFragmentFactory;
        if (mFragmentFactory == null) {
            throw new IllegalStateException("Creating a Fragment requires that this FragmentTransaction was built with FragmentManager.beginTransaction()");
        }
        final ClassLoader mClassLoader = this.mClassLoader;
        if (mClassLoader != null) {
            final Fragment instantiate = mFragmentFactory.instantiate(mClassLoader, clazz.getName());
            if (arguments != null) {
                instantiate.setArguments(arguments);
            }
            return instantiate;
        }
        throw new IllegalStateException("The FragmentManager must be attached to itshost to create a Fragment");
    }
    
    public FragmentTransaction add(final int n, final Fragment fragment) {
        this.doAddOp(n, fragment, null, 1);
        return this;
    }
    
    public FragmentTransaction add(final int n, final Fragment fragment, final String s) {
        this.doAddOp(n, fragment, s, 1);
        return this;
    }
    
    public final FragmentTransaction add(final int n, final Class<? extends Fragment> clazz, final Bundle bundle) {
        return this.add(n, this.createFragment(clazz, bundle));
    }
    
    public final FragmentTransaction add(final int n, final Class<? extends Fragment> clazz, final Bundle bundle, final String s) {
        return this.add(n, this.createFragment(clazz, bundle), s);
    }
    
    FragmentTransaction add(final ViewGroup mContainer, final Fragment fragment, final String s) {
        fragment.mContainer = mContainer;
        return this.add(mContainer.getId(), fragment, s);
    }
    
    public FragmentTransaction add(final Fragment fragment, final String s) {
        this.doAddOp(0, fragment, s, 1);
        return this;
    }
    
    public final FragmentTransaction add(final Class<? extends Fragment> clazz, final Bundle bundle, final String s) {
        return this.add(this.createFragment(clazz, bundle), s);
    }
    
    void addOp(final Op e) {
        this.mOps.add(e);
        e.mEnterAnim = this.mEnterAnim;
        e.mExitAnim = this.mExitAnim;
        e.mPopEnterAnim = this.mPopEnterAnim;
        e.mPopExitAnim = this.mPopExitAnim;
    }
    
    public FragmentTransaction addSharedElement(final View view, final String e) {
        if (FragmentTransition.supportsTransition()) {
            final String transitionName = ViewCompat.getTransitionName(view);
            if (transitionName == null) {
                throw new IllegalArgumentException("Unique transitionNames are required for all sharedElements");
            }
            if (this.mSharedElementSourceNames == null) {
                this.mSharedElementSourceNames = new ArrayList<String>();
                this.mSharedElementTargetNames = new ArrayList<String>();
            }
            else {
                if (this.mSharedElementTargetNames.contains(e)) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("A shared element with the target name '");
                    sb.append(e);
                    sb.append("' has already been added to the transaction.");
                    throw new IllegalArgumentException(sb.toString());
                }
                if (this.mSharedElementSourceNames.contains(transitionName)) {
                    final StringBuilder sb2 = new StringBuilder();
                    sb2.append("A shared element with the source name '");
                    sb2.append(transitionName);
                    sb2.append("' has already been added to the transaction.");
                    throw new IllegalArgumentException(sb2.toString());
                }
            }
            this.mSharedElementSourceNames.add(transitionName);
            this.mSharedElementTargetNames.add(e);
        }
        return this;
    }
    
    public FragmentTransaction addToBackStack(final String mName) {
        if (this.mAllowAddToBackStack) {
            this.mAddToBackStack = true;
            this.mName = mName;
            return this;
        }
        throw new IllegalStateException("This FragmentTransaction is not allowed to be added to the back stack.");
    }
    
    public FragmentTransaction attach(final Fragment fragment) {
        this.addOp(new Op(7, fragment));
        return this;
    }
    
    public abstract int commit();
    
    public abstract int commitAllowingStateLoss();
    
    public abstract void commitNow();
    
    public abstract void commitNowAllowingStateLoss();
    
    public FragmentTransaction detach(final Fragment fragment) {
        this.addOp(new Op(6, fragment));
        return this;
    }
    
    public FragmentTransaction disallowAddToBackStack() {
        if (!this.mAddToBackStack) {
            this.mAllowAddToBackStack = false;
            return this;
        }
        throw new IllegalStateException("This transaction is already being added to the back stack");
    }
    
    void doAddOp(final int mContainerId, final Fragment obj, final String str, final int n) {
        if (obj.mPreviousWho != null) {
            FragmentStrictMode.onFragmentReuse(obj, obj.mPreviousWho);
        }
        final Class<? extends Fragment> class1 = obj.getClass();
        final int modifiers = class1.getModifiers();
        if (!class1.isAnonymousClass() && Modifier.isPublic(modifiers) && (!class1.isMemberClass() || Modifier.isStatic(modifiers))) {
            if (str != null) {
                if (obj.mTag != null && !str.equals(obj.mTag)) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Can't change tag of fragment ");
                    sb.append(obj);
                    sb.append(": was ");
                    sb.append(obj.mTag);
                    sb.append(" now ");
                    sb.append(str);
                    throw new IllegalStateException(sb.toString());
                }
                obj.mTag = str;
            }
            if (mContainerId != 0) {
                if (mContainerId == -1) {
                    final StringBuilder sb2 = new StringBuilder();
                    sb2.append("Can't add fragment ");
                    sb2.append(obj);
                    sb2.append(" with tag ");
                    sb2.append(str);
                    sb2.append(" to container view with no id");
                    throw new IllegalArgumentException(sb2.toString());
                }
                if (obj.mFragmentId != 0 && obj.mFragmentId != mContainerId) {
                    final StringBuilder sb3 = new StringBuilder();
                    sb3.append("Can't change container ID of fragment ");
                    sb3.append(obj);
                    sb3.append(": was ");
                    sb3.append(obj.mFragmentId);
                    sb3.append(" now ");
                    sb3.append(mContainerId);
                    throw new IllegalStateException(sb3.toString());
                }
                obj.mFragmentId = mContainerId;
                obj.mContainerId = mContainerId;
            }
            this.addOp(new Op(n, obj));
            return;
        }
        final StringBuilder sb4 = new StringBuilder();
        sb4.append("Fragment ");
        sb4.append(class1.getCanonicalName());
        sb4.append(" must be a public static class to be  properly recreated from instance state.");
        throw new IllegalStateException(sb4.toString());
    }
    
    public FragmentTransaction hide(final Fragment fragment) {
        this.addOp(new Op(4, fragment));
        return this;
    }
    
    public boolean isAddToBackStackAllowed() {
        return this.mAllowAddToBackStack;
    }
    
    public boolean isEmpty() {
        return this.mOps.isEmpty();
    }
    
    public FragmentTransaction remove(final Fragment fragment) {
        this.addOp(new Op(3, fragment));
        return this;
    }
    
    public FragmentTransaction replace(final int n, final Fragment fragment) {
        return this.replace(n, fragment, null);
    }
    
    public FragmentTransaction replace(final int n, final Fragment fragment, final String s) {
        if (n != 0) {
            this.doAddOp(n, fragment, s, 2);
            return this;
        }
        throw new IllegalArgumentException("Must use non-zero containerViewId");
    }
    
    public final FragmentTransaction replace(final int n, final Class<? extends Fragment> clazz, final Bundle bundle) {
        return this.replace(n, clazz, bundle, null);
    }
    
    public final FragmentTransaction replace(final int n, final Class<? extends Fragment> clazz, final Bundle bundle, final String s) {
        return this.replace(n, this.createFragment(clazz, bundle), s);
    }
    
    public FragmentTransaction runOnCommit(final Runnable e) {
        this.disallowAddToBackStack();
        if (this.mCommitRunnables == null) {
            this.mCommitRunnables = new ArrayList<Runnable>();
        }
        this.mCommitRunnables.add(e);
        return this;
    }
    
    @Deprecated
    public FragmentTransaction setAllowOptimization(final boolean reorderingAllowed) {
        return this.setReorderingAllowed(reorderingAllowed);
    }
    
    @Deprecated
    public FragmentTransaction setBreadCrumbShortTitle(final int mBreadCrumbShortTitleRes) {
        this.mBreadCrumbShortTitleRes = mBreadCrumbShortTitleRes;
        this.mBreadCrumbShortTitleText = null;
        return this;
    }
    
    @Deprecated
    public FragmentTransaction setBreadCrumbShortTitle(final CharSequence mBreadCrumbShortTitleText) {
        this.mBreadCrumbShortTitleRes = 0;
        this.mBreadCrumbShortTitleText = mBreadCrumbShortTitleText;
        return this;
    }
    
    @Deprecated
    public FragmentTransaction setBreadCrumbTitle(final int mBreadCrumbTitleRes) {
        this.mBreadCrumbTitleRes = mBreadCrumbTitleRes;
        this.mBreadCrumbTitleText = null;
        return this;
    }
    
    @Deprecated
    public FragmentTransaction setBreadCrumbTitle(final CharSequence mBreadCrumbTitleText) {
        this.mBreadCrumbTitleRes = 0;
        this.mBreadCrumbTitleText = mBreadCrumbTitleText;
        return this;
    }
    
    public FragmentTransaction setCustomAnimations(final int n, final int n2) {
        return this.setCustomAnimations(n, n2, 0, 0);
    }
    
    public FragmentTransaction setCustomAnimations(final int mEnterAnim, final int mExitAnim, final int mPopEnterAnim, final int mPopExitAnim) {
        this.mEnterAnim = mEnterAnim;
        this.mExitAnim = mExitAnim;
        this.mPopEnterAnim = mPopEnterAnim;
        this.mPopExitAnim = mPopExitAnim;
        return this;
    }
    
    public FragmentTransaction setMaxLifecycle(final Fragment fragment, final Lifecycle.State state) {
        this.addOp(new Op(10, fragment, state));
        return this;
    }
    
    public FragmentTransaction setPrimaryNavigationFragment(final Fragment fragment) {
        this.addOp(new Op(8, fragment));
        return this;
    }
    
    public FragmentTransaction setReorderingAllowed(final boolean mReorderingAllowed) {
        this.mReorderingAllowed = mReorderingAllowed;
        return this;
    }
    
    public FragmentTransaction setTransition(final int mTransition) {
        this.mTransition = mTransition;
        return this;
    }
    
    @Deprecated
    public FragmentTransaction setTransitionStyle(final int n) {
        return this;
    }
    
    public FragmentTransaction show(final Fragment fragment) {
        this.addOp(new Op(5, fragment));
        return this;
    }
    
    static final class Op
    {
        int mCmd;
        Lifecycle.State mCurrentMaxState;
        int mEnterAnim;
        int mExitAnim;
        Fragment mFragment;
        boolean mFromExpandedOp;
        Lifecycle.State mOldMaxState;
        int mPopEnterAnim;
        int mPopExitAnim;
        
        Op() {
        }
        
        Op(final int mCmd, final Fragment mFragment) {
            this.mCmd = mCmd;
            this.mFragment = mFragment;
            this.mFromExpandedOp = false;
            this.mOldMaxState = Lifecycle.State.RESUMED;
            this.mCurrentMaxState = Lifecycle.State.RESUMED;
        }
        
        Op(final int mCmd, final Fragment mFragment, final Lifecycle.State mCurrentMaxState) {
            this.mCmd = mCmd;
            this.mFragment = mFragment;
            this.mFromExpandedOp = false;
            this.mOldMaxState = mFragment.mMaxState;
            this.mCurrentMaxState = mCurrentMaxState;
        }
        
        Op(final int mCmd, final Fragment mFragment, final boolean mFromExpandedOp) {
            this.mCmd = mCmd;
            this.mFragment = mFragment;
            this.mFromExpandedOp = mFromExpandedOp;
            this.mOldMaxState = Lifecycle.State.RESUMED;
            this.mCurrentMaxState = Lifecycle.State.RESUMED;
        }
        
        Op(final Op op) {
            this.mCmd = op.mCmd;
            this.mFragment = op.mFragment;
            this.mFromExpandedOp = op.mFromExpandedOp;
            this.mEnterAnim = op.mEnterAnim;
            this.mExitAnim = op.mExitAnim;
            this.mPopEnterAnim = op.mPopEnterAnim;
            this.mPopExitAnim = op.mPopExitAnim;
            this.mOldMaxState = op.mOldMaxState;
            this.mCurrentMaxState = op.mCurrentMaxState;
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.fragment.app;

import android.os.Bundle;
import androidx.lifecycle.LifecycleOwner;
import androidx.savedstate.SavedStateRegistry;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.SavedStateViewModelFactory;
import android.content.Context;
import androidx.lifecycle.SavedStateHandleSupport;
import androidx.lifecycle.viewmodel.MutableCreationExtras;
import android.app.Application;
import android.content.ContextWrapper;
import androidx.lifecycle.viewmodel.CreationExtras;
import androidx.lifecycle.ViewModelStore;
import androidx.savedstate.SavedStateRegistryController;
import androidx.lifecycle.LifecycleRegistry;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelStoreOwner;
import androidx.savedstate.SavedStateRegistryOwner;
import androidx.lifecycle.HasDefaultViewModelProviderFactory;

class FragmentViewLifecycleOwner implements HasDefaultViewModelProviderFactory, SavedStateRegistryOwner, ViewModelStoreOwner
{
    private ViewModelProvider.Factory mDefaultFactory;
    private final Fragment mFragment;
    private LifecycleRegistry mLifecycleRegistry;
    private final Runnable mRestoreViewSavedStateRunnable;
    private SavedStateRegistryController mSavedStateRegistryController;
    private final ViewModelStore mViewModelStore;
    
    FragmentViewLifecycleOwner(final Fragment mFragment, final ViewModelStore mViewModelStore, final Runnable mRestoreViewSavedStateRunnable) {
        this.mLifecycleRegistry = null;
        this.mSavedStateRegistryController = null;
        this.mFragment = mFragment;
        this.mViewModelStore = mViewModelStore;
        this.mRestoreViewSavedStateRunnable = mRestoreViewSavedStateRunnable;
    }
    
    @Override
    public CreationExtras getDefaultViewModelCreationExtras() {
        while (true) {
            for (Context context = this.mFragment.requireContext().getApplicationContext(); context instanceof ContextWrapper; context = ((ContextWrapper)context).getBaseContext()) {
                if (context instanceof Application) {
                    final Application application = (Application)context;
                    final MutableCreationExtras mutableCreationExtras = new MutableCreationExtras();
                    if (application != null) {
                        mutableCreationExtras.set(ViewModelProvider.AndroidViewModelFactory.APPLICATION_KEY, application);
                    }
                    mutableCreationExtras.set(SavedStateHandleSupport.SAVED_STATE_REGISTRY_OWNER_KEY, this.mFragment);
                    mutableCreationExtras.set(SavedStateHandleSupport.VIEW_MODEL_STORE_OWNER_KEY, this);
                    if (this.mFragment.getArguments() != null) {
                        mutableCreationExtras.set(SavedStateHandleSupport.DEFAULT_ARGS_KEY, this.mFragment.getArguments());
                    }
                    return mutableCreationExtras;
                }
            }
            final Application application = null;
            continue;
        }
    }
    
    @Override
    public ViewModelProvider.Factory getDefaultViewModelProviderFactory() {
        final ViewModelProvider.Factory defaultViewModelProviderFactory = this.mFragment.getDefaultViewModelProviderFactory();
        if (!defaultViewModelProviderFactory.equals(this.mFragment.mDefaultFactory)) {
            return this.mDefaultFactory = defaultViewModelProviderFactory;
        }
        if (this.mDefaultFactory == null) {
            final Application application = null;
            Context context = this.mFragment.requireContext().getApplicationContext();
            Application application2;
            while (true) {
                application2 = application;
                if (!(context instanceof ContextWrapper)) {
                    break;
                }
                if (context instanceof Application) {
                    application2 = (Application)context;
                    break;
                }
                context = ((ContextWrapper)context).getBaseContext();
            }
            final Fragment mFragment = this.mFragment;
            this.mDefaultFactory = new SavedStateViewModelFactory(application2, mFragment, mFragment.getArguments());
        }
        return this.mDefaultFactory;
    }
    
    @Override
    public Lifecycle getLifecycle() {
        this.initialize();
        return this.mLifecycleRegistry;
    }
    
    @Override
    public SavedStateRegistry getSavedStateRegistry() {
        this.initialize();
        return this.mSavedStateRegistryController.getSavedStateRegistry();
    }
    
    @Override
    public ViewModelStore getViewModelStore() {
        this.initialize();
        return this.mViewModelStore;
    }
    
    void handleLifecycleEvent(final Lifecycle.Event event) {
        this.mLifecycleRegistry.handleLifecycleEvent(event);
    }
    
    void initialize() {
        if (this.mLifecycleRegistry == null) {
            this.mLifecycleRegistry = new LifecycleRegistry(this);
            (this.mSavedStateRegistryController = SavedStateRegistryController.create(this)).performAttach();
            this.mRestoreViewSavedStateRunnable.run();
        }
    }
    
    boolean isInitialized() {
        return this.mLifecycleRegistry != null;
    }
    
    void performRestore(final Bundle bundle) {
        this.mSavedStateRegistryController.performRestore(bundle);
    }
    
    void performSave(final Bundle bundle) {
        this.mSavedStateRegistryController.performSave(bundle);
    }
    
    void setCurrentState(final Lifecycle.State currentState) {
        this.mLifecycleRegistry.setCurrentState(currentState);
    }
}

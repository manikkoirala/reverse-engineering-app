// 
// Decompiled by Procyon v0.6.0
// 

package androidx.fragment.app;

public interface FragmentOnAttachListener
{
    void onAttachFragment(final FragmentManager p0, final Fragment p1);
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.fragment.app;

import androidx.lifecycle.Lifecycle;
import java.util.ArrayList;
import java.io.Writer;
import java.io.PrintWriter;
import android.util.Log;

final class BackStackRecord extends FragmentTransaction implements BackStackEntry, OpGenerator
{
    private static final String TAG = "FragmentManager";
    boolean mBeingSaved;
    boolean mCommitted;
    int mIndex;
    final FragmentManager mManager;
    
    BackStackRecord(final BackStackRecord backStackRecord) {
        final FragmentFactory fragmentFactory = backStackRecord.mManager.getFragmentFactory();
        ClassLoader classLoader;
        if (backStackRecord.mManager.getHost() != null) {
            classLoader = backStackRecord.mManager.getHost().getContext().getClassLoader();
        }
        else {
            classLoader = null;
        }
        super(fragmentFactory, classLoader, backStackRecord);
        this.mIndex = -1;
        this.mBeingSaved = false;
        this.mManager = backStackRecord.mManager;
        this.mCommitted = backStackRecord.mCommitted;
        this.mIndex = backStackRecord.mIndex;
        this.mBeingSaved = backStackRecord.mBeingSaved;
    }
    
    BackStackRecord(final FragmentManager mManager) {
        final FragmentFactory fragmentFactory = mManager.getFragmentFactory();
        ClassLoader classLoader;
        if (mManager.getHost() != null) {
            classLoader = mManager.getHost().getContext().getClassLoader();
        }
        else {
            classLoader = null;
        }
        super(fragmentFactory, classLoader);
        this.mIndex = -1;
        this.mBeingSaved = false;
        this.mManager = mManager;
    }
    
    void bumpBackStackNesting(final int i) {
        if (!this.mAddToBackStack) {
            return;
        }
        if (FragmentManager.isLoggingEnabled(2)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Bump nesting in ");
            sb.append(this);
            sb.append(" by ");
            sb.append(i);
            Log.v("FragmentManager", sb.toString());
        }
        for (int size = this.mOps.size(), j = 0; j < size; ++j) {
            final Op op = this.mOps.get(j);
            if (op.mFragment != null) {
                final Fragment mFragment = op.mFragment;
                mFragment.mBackStackNesting += i;
                if (FragmentManager.isLoggingEnabled(2)) {
                    final StringBuilder sb2 = new StringBuilder();
                    sb2.append("Bump nesting of ");
                    sb2.append(op.mFragment);
                    sb2.append(" to ");
                    sb2.append(op.mFragment.mBackStackNesting);
                    Log.v("FragmentManager", sb2.toString());
                }
            }
        }
    }
    
    void collapseOps() {
        int n;
        for (int i = this.mOps.size() - 1; i >= 0; i = n - 1) {
            final Op op = this.mOps.get(i);
            if (!op.mFromExpandedOp) {
                n = i;
            }
            else if (op.mCmd == 8) {
                op.mFromExpandedOp = false;
                this.mOps.remove(i - 1);
                n = i - 1;
            }
            else {
                final int mContainerId = op.mFragment.mContainerId;
                op.mCmd = 2;
                op.mFromExpandedOp = false;
                int n2 = i - 1;
                while (true) {
                    n = i;
                    if (n2 < 0) {
                        break;
                    }
                    final Op op2 = this.mOps.get(n2);
                    int n3 = i;
                    if (op2.mFromExpandedOp) {
                        n3 = i;
                        if (op2.mFragment.mContainerId == mContainerId) {
                            this.mOps.remove(n2);
                            n3 = i - 1;
                        }
                    }
                    --n2;
                    i = n3;
                }
            }
        }
    }
    
    @Override
    public int commit() {
        return this.commitInternal(false);
    }
    
    @Override
    public int commitAllowingStateLoss() {
        return this.commitInternal(true);
    }
    
    int commitInternal(final boolean b) {
        if (!this.mCommitted) {
            if (FragmentManager.isLoggingEnabled(2)) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Commit: ");
                sb.append(this);
                Log.v("FragmentManager", sb.toString());
                final PrintWriter printWriter = new PrintWriter(new LogWriter("FragmentManager"));
                this.dump("  ", printWriter);
                printWriter.close();
            }
            this.mCommitted = true;
            if (this.mAddToBackStack) {
                this.mIndex = this.mManager.allocBackStackIndex();
            }
            else {
                this.mIndex = -1;
            }
            this.mManager.enqueueAction((FragmentManager.OpGenerator)this, b);
            return this.mIndex;
        }
        throw new IllegalStateException("commit already called");
    }
    
    @Override
    public void commitNow() {
        this.disallowAddToBackStack();
        this.mManager.execSingleAction((FragmentManager.OpGenerator)this, false);
    }
    
    @Override
    public void commitNowAllowingStateLoss() {
        this.disallowAddToBackStack();
        this.mManager.execSingleAction((FragmentManager.OpGenerator)this, true);
    }
    
    @Override
    public FragmentTransaction detach(final Fragment fragment) {
        if (fragment.mFragmentManager != null && fragment.mFragmentManager != this.mManager) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Cannot detach Fragment attached to a different FragmentManager. Fragment ");
            sb.append(fragment.toString());
            sb.append(" is already attached to a FragmentManager.");
            throw new IllegalStateException(sb.toString());
        }
        return super.detach(fragment);
    }
    
    @Override
    void doAddOp(final int n, final Fragment fragment, final String s, final int n2) {
        super.doAddOp(n, fragment, s, n2);
        fragment.mFragmentManager = this.mManager;
    }
    
    public void dump(final String s, final PrintWriter printWriter) {
        this.dump(s, printWriter, true);
    }
    
    public void dump(final String s, final PrintWriter printWriter, final boolean b) {
        if (b) {
            printWriter.print(s);
            printWriter.print("mName=");
            printWriter.print(this.mName);
            printWriter.print(" mIndex=");
            printWriter.print(this.mIndex);
            printWriter.print(" mCommitted=");
            printWriter.println(this.mCommitted);
            if (this.mTransition != 0) {
                printWriter.print(s);
                printWriter.print("mTransition=#");
                printWriter.print(Integer.toHexString(this.mTransition));
            }
            if (this.mEnterAnim != 0 || this.mExitAnim != 0) {
                printWriter.print(s);
                printWriter.print("mEnterAnim=#");
                printWriter.print(Integer.toHexString(this.mEnterAnim));
                printWriter.print(" mExitAnim=#");
                printWriter.println(Integer.toHexString(this.mExitAnim));
            }
            if (this.mPopEnterAnim != 0 || this.mPopExitAnim != 0) {
                printWriter.print(s);
                printWriter.print("mPopEnterAnim=#");
                printWriter.print(Integer.toHexString(this.mPopEnterAnim));
                printWriter.print(" mPopExitAnim=#");
                printWriter.println(Integer.toHexString(this.mPopExitAnim));
            }
            if (this.mBreadCrumbTitleRes != 0 || this.mBreadCrumbTitleText != null) {
                printWriter.print(s);
                printWriter.print("mBreadCrumbTitleRes=#");
                printWriter.print(Integer.toHexString(this.mBreadCrumbTitleRes));
                printWriter.print(" mBreadCrumbTitleText=");
                printWriter.println(this.mBreadCrumbTitleText);
            }
            if (this.mBreadCrumbShortTitleRes != 0 || this.mBreadCrumbShortTitleText != null) {
                printWriter.print(s);
                printWriter.print("mBreadCrumbShortTitleRes=#");
                printWriter.print(Integer.toHexString(this.mBreadCrumbShortTitleRes));
                printWriter.print(" mBreadCrumbShortTitleText=");
                printWriter.println(this.mBreadCrumbShortTitleText);
            }
        }
        if (!this.mOps.isEmpty()) {
            printWriter.print(s);
            printWriter.println("Operations:");
            for (int size = this.mOps.size(), i = 0; i < size; ++i) {
                final Op op = this.mOps.get(i);
                String string = null;
                switch (op.mCmd) {
                    default: {
                        final StringBuilder sb = new StringBuilder();
                        sb.append("cmd=");
                        sb.append(op.mCmd);
                        string = sb.toString();
                        break;
                    }
                    case 10: {
                        string = "OP_SET_MAX_LIFECYCLE";
                        break;
                    }
                    case 9: {
                        string = "UNSET_PRIMARY_NAV";
                        break;
                    }
                    case 8: {
                        string = "SET_PRIMARY_NAV";
                        break;
                    }
                    case 7: {
                        string = "ATTACH";
                        break;
                    }
                    case 6: {
                        string = "DETACH";
                        break;
                    }
                    case 5: {
                        string = "SHOW";
                        break;
                    }
                    case 4: {
                        string = "HIDE";
                        break;
                    }
                    case 3: {
                        string = "REMOVE";
                        break;
                    }
                    case 2: {
                        string = "REPLACE";
                        break;
                    }
                    case 1: {
                        string = "ADD";
                        break;
                    }
                    case 0: {
                        string = "NULL";
                        break;
                    }
                }
                printWriter.print(s);
                printWriter.print("  Op #");
                printWriter.print(i);
                printWriter.print(": ");
                printWriter.print(string);
                printWriter.print(" ");
                printWriter.println(op.mFragment);
                if (b) {
                    if (op.mEnterAnim != 0 || op.mExitAnim != 0) {
                        printWriter.print(s);
                        printWriter.print("enterAnim=#");
                        printWriter.print(Integer.toHexString(op.mEnterAnim));
                        printWriter.print(" exitAnim=#");
                        printWriter.println(Integer.toHexString(op.mExitAnim));
                    }
                    if (op.mPopEnterAnim != 0 || op.mPopExitAnim != 0) {
                        printWriter.print(s);
                        printWriter.print("popEnterAnim=#");
                        printWriter.print(Integer.toHexString(op.mPopEnterAnim));
                        printWriter.print(" popExitAnim=#");
                        printWriter.println(Integer.toHexString(op.mPopExitAnim));
                    }
                }
            }
        }
    }
    
    void executeOps() {
        for (int size = this.mOps.size(), i = 0; i < size; ++i) {
            final Op op = this.mOps.get(i);
            final Fragment mFragment = op.mFragment;
            if (mFragment != null) {
                mFragment.mBeingSaved = this.mBeingSaved;
                mFragment.setPopDirection(false);
                mFragment.setNextTransition(this.mTransition);
                mFragment.setSharedElementNames(this.mSharedElementSourceNames, this.mSharedElementTargetNames);
            }
            switch (op.mCmd) {
                default: {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Unknown cmd: ");
                    sb.append(op.mCmd);
                    throw new IllegalArgumentException(sb.toString());
                }
                case 10: {
                    this.mManager.setMaxLifecycle(mFragment, op.mCurrentMaxState);
                    break;
                }
                case 9: {
                    this.mManager.setPrimaryNavigationFragment(null);
                    break;
                }
                case 8: {
                    this.mManager.setPrimaryNavigationFragment(mFragment);
                    break;
                }
                case 7: {
                    mFragment.setAnimations(op.mEnterAnim, op.mExitAnim, op.mPopEnterAnim, op.mPopExitAnim);
                    this.mManager.setExitAnimationOrder(mFragment, false);
                    this.mManager.attachFragment(mFragment);
                    break;
                }
                case 6: {
                    mFragment.setAnimations(op.mEnterAnim, op.mExitAnim, op.mPopEnterAnim, op.mPopExitAnim);
                    this.mManager.detachFragment(mFragment);
                    break;
                }
                case 5: {
                    mFragment.setAnimations(op.mEnterAnim, op.mExitAnim, op.mPopEnterAnim, op.mPopExitAnim);
                    this.mManager.setExitAnimationOrder(mFragment, false);
                    this.mManager.showFragment(mFragment);
                    break;
                }
                case 4: {
                    mFragment.setAnimations(op.mEnterAnim, op.mExitAnim, op.mPopEnterAnim, op.mPopExitAnim);
                    this.mManager.hideFragment(mFragment);
                    break;
                }
                case 3: {
                    mFragment.setAnimations(op.mEnterAnim, op.mExitAnim, op.mPopEnterAnim, op.mPopExitAnim);
                    this.mManager.removeFragment(mFragment);
                    break;
                }
                case 1: {
                    mFragment.setAnimations(op.mEnterAnim, op.mExitAnim, op.mPopEnterAnim, op.mPopExitAnim);
                    this.mManager.setExitAnimationOrder(mFragment, false);
                    this.mManager.addFragment(mFragment);
                    break;
                }
            }
        }
    }
    
    void executePopOps() {
        for (int i = this.mOps.size() - 1; i >= 0; --i) {
            final Op op = this.mOps.get(i);
            final Fragment mFragment = op.mFragment;
            if (mFragment != null) {
                mFragment.mBeingSaved = this.mBeingSaved;
                mFragment.setPopDirection(true);
                mFragment.setNextTransition(FragmentManager.reverseTransit(this.mTransition));
                mFragment.setSharedElementNames(this.mSharedElementTargetNames, this.mSharedElementSourceNames);
            }
            switch (op.mCmd) {
                default: {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Unknown cmd: ");
                    sb.append(op.mCmd);
                    throw new IllegalArgumentException(sb.toString());
                }
                case 10: {
                    this.mManager.setMaxLifecycle(mFragment, op.mOldMaxState);
                    break;
                }
                case 9: {
                    this.mManager.setPrimaryNavigationFragment(mFragment);
                    break;
                }
                case 8: {
                    this.mManager.setPrimaryNavigationFragment(null);
                    break;
                }
                case 7: {
                    mFragment.setAnimations(op.mEnterAnim, op.mExitAnim, op.mPopEnterAnim, op.mPopExitAnim);
                    this.mManager.setExitAnimationOrder(mFragment, true);
                    this.mManager.detachFragment(mFragment);
                    break;
                }
                case 6: {
                    mFragment.setAnimations(op.mEnterAnim, op.mExitAnim, op.mPopEnterAnim, op.mPopExitAnim);
                    this.mManager.attachFragment(mFragment);
                    break;
                }
                case 5: {
                    mFragment.setAnimations(op.mEnterAnim, op.mExitAnim, op.mPopEnterAnim, op.mPopExitAnim);
                    this.mManager.setExitAnimationOrder(mFragment, true);
                    this.mManager.hideFragment(mFragment);
                    break;
                }
                case 4: {
                    mFragment.setAnimations(op.mEnterAnim, op.mExitAnim, op.mPopEnterAnim, op.mPopExitAnim);
                    this.mManager.showFragment(mFragment);
                    break;
                }
                case 3: {
                    mFragment.setAnimations(op.mEnterAnim, op.mExitAnim, op.mPopEnterAnim, op.mPopExitAnim);
                    this.mManager.addFragment(mFragment);
                    break;
                }
                case 1: {
                    mFragment.setAnimations(op.mEnterAnim, op.mExitAnim, op.mPopEnterAnim, op.mPopExitAnim);
                    this.mManager.setExitAnimationOrder(mFragment, true);
                    this.mManager.removeFragment(mFragment);
                    break;
                }
            }
        }
    }
    
    Fragment expandOps(final ArrayList<Fragment> list, Fragment mFragment) {
        int i = 0;
        Fragment fragment = mFragment;
        while (i < this.mOps.size()) {
            final Op op = this.mOps.get(i);
            final int mCmd = op.mCmd;
            int n = 0;
            Label_0452: {
                if (mCmd != 1) {
                    if (mCmd != 2) {
                        if (mCmd != 3 && mCmd != 6) {
                            if (mCmd != 7) {
                                if (mCmd != 8) {
                                    mFragment = fragment;
                                    n = i;
                                    break Label_0452;
                                }
                                this.mOps.add(i, new Op(9, fragment, true));
                                op.mFromExpandedOp = true;
                                n = i + 1;
                                mFragment = op.mFragment;
                                break Label_0452;
                            }
                        }
                        else {
                            list.remove(op.mFragment);
                            mFragment = fragment;
                            n = i;
                            if (op.mFragment == fragment) {
                                this.mOps.add(i, new Op(9, op.mFragment));
                                n = i + 1;
                                mFragment = null;
                            }
                            break Label_0452;
                        }
                    }
                    else {
                        final Fragment mFragment2 = op.mFragment;
                        final int mContainerId = mFragment2.mContainerId;
                        int j = list.size() - 1;
                        int n2 = 0;
                        n = i;
                        mFragment = fragment;
                        while (j >= 0) {
                            final Fragment o = list.get(j);
                            Fragment fragment2 = mFragment;
                            int index = n;
                            int n3 = n2;
                            if (o.mContainerId == mContainerId) {
                                if (o == mFragment2) {
                                    n3 = 1;
                                    fragment2 = mFragment;
                                    index = n;
                                }
                                else {
                                    fragment2 = mFragment;
                                    index = n;
                                    if (o == mFragment) {
                                        this.mOps.add(n, new Op(9, o, true));
                                        index = n + 1;
                                        fragment2 = null;
                                    }
                                    final Op element = new Op(3, o, true);
                                    element.mEnterAnim = op.mEnterAnim;
                                    element.mPopEnterAnim = op.mPopEnterAnim;
                                    element.mExitAnim = op.mExitAnim;
                                    element.mPopExitAnim = op.mPopExitAnim;
                                    this.mOps.add(index, element);
                                    list.remove(o);
                                    ++index;
                                    n3 = n2;
                                }
                            }
                            --j;
                            mFragment = fragment2;
                            n = index;
                            n2 = n3;
                        }
                        if (n2 != 0) {
                            this.mOps.remove(n);
                            --n;
                            break Label_0452;
                        }
                        op.mCmd = 1;
                        op.mFromExpandedOp = true;
                        list.add(mFragment2);
                        break Label_0452;
                    }
                }
                list.add(op.mFragment);
                n = i;
                mFragment = fragment;
            }
            i = n + 1;
            fragment = mFragment;
        }
        return fragment;
    }
    
    @Override
    public boolean generateOps(final ArrayList<BackStackRecord> list, final ArrayList<Boolean> list2) {
        if (FragmentManager.isLoggingEnabled(2)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Run: ");
            sb.append(this);
            Log.v("FragmentManager", sb.toString());
        }
        list.add(this);
        list2.add(false);
        if (this.mAddToBackStack) {
            this.mManager.addBackStackState(this);
        }
        return true;
    }
    
    @Override
    public CharSequence getBreadCrumbShortTitle() {
        if (this.mBreadCrumbShortTitleRes != 0) {
            return this.mManager.getHost().getContext().getText(this.mBreadCrumbShortTitleRes);
        }
        return this.mBreadCrumbShortTitleText;
    }
    
    @Override
    public int getBreadCrumbShortTitleRes() {
        return this.mBreadCrumbShortTitleRes;
    }
    
    @Override
    public CharSequence getBreadCrumbTitle() {
        if (this.mBreadCrumbTitleRes != 0) {
            return this.mManager.getHost().getContext().getText(this.mBreadCrumbTitleRes);
        }
        return this.mBreadCrumbTitleText;
    }
    
    @Override
    public int getBreadCrumbTitleRes() {
        return this.mBreadCrumbTitleRes;
    }
    
    @Override
    public int getId() {
        return this.mIndex;
    }
    
    @Override
    public String getName() {
        return this.mName;
    }
    
    @Override
    public FragmentTransaction hide(final Fragment fragment) {
        if (fragment.mFragmentManager != null && fragment.mFragmentManager != this.mManager) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Cannot hide Fragment attached to a different FragmentManager. Fragment ");
            sb.append(fragment.toString());
            sb.append(" is already attached to a FragmentManager.");
            throw new IllegalStateException(sb.toString());
        }
        return super.hide(fragment);
    }
    
    @Override
    public boolean isEmpty() {
        return this.mOps.isEmpty();
    }
    
    @Override
    public FragmentTransaction remove(final Fragment fragment) {
        if (fragment.mFragmentManager != null && fragment.mFragmentManager != this.mManager) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Cannot remove Fragment attached to a different FragmentManager. Fragment ");
            sb.append(fragment.toString());
            sb.append(" is already attached to a FragmentManager.");
            throw new IllegalStateException(sb.toString());
        }
        return super.remove(fragment);
    }
    
    public void runOnCommitRunnables() {
        if (this.mCommitRunnables != null) {
            for (int i = 0; i < this.mCommitRunnables.size(); ++i) {
                this.mCommitRunnables.get(i).run();
            }
            this.mCommitRunnables = null;
        }
    }
    
    @Override
    public FragmentTransaction setMaxLifecycle(final Fragment fragment, final Lifecycle.State state) {
        if (fragment.mFragmentManager != this.mManager) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Cannot setMaxLifecycle for Fragment not attached to FragmentManager ");
            sb.append(this.mManager);
            throw new IllegalArgumentException(sb.toString());
        }
        if (state == Lifecycle.State.INITIALIZED && fragment.mState > -1) {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("Cannot set maximum Lifecycle to ");
            sb2.append(state);
            sb2.append(" after the Fragment has been created");
            throw new IllegalArgumentException(sb2.toString());
        }
        if (state != Lifecycle.State.DESTROYED) {
            return super.setMaxLifecycle(fragment, state);
        }
        final StringBuilder sb3 = new StringBuilder();
        sb3.append("Cannot set maximum Lifecycle to ");
        sb3.append(state);
        sb3.append(". Use remove() to remove the fragment from the FragmentManager and trigger its destruction.");
        throw new IllegalArgumentException(sb3.toString());
    }
    
    @Override
    public FragmentTransaction setPrimaryNavigationFragment(final Fragment primaryNavigationFragment) {
        if (primaryNavigationFragment != null && primaryNavigationFragment.mFragmentManager != null && primaryNavigationFragment.mFragmentManager != this.mManager) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Cannot setPrimaryNavigation for Fragment attached to a different FragmentManager. Fragment ");
            sb.append(primaryNavigationFragment.toString());
            sb.append(" is already attached to a FragmentManager.");
            throw new IllegalStateException(sb.toString());
        }
        return super.setPrimaryNavigationFragment(primaryNavigationFragment);
    }
    
    @Override
    public FragmentTransaction show(final Fragment fragment) {
        if (fragment.mFragmentManager != null && fragment.mFragmentManager != this.mManager) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Cannot show Fragment attached to a different FragmentManager. Fragment ");
            sb.append(fragment.toString());
            sb.append(" is already attached to a FragmentManager.");
            throw new IllegalStateException(sb.toString());
        }
        return super.show(fragment);
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder(128);
        sb.append("BackStackEntry{");
        sb.append(Integer.toHexString(System.identityHashCode(this)));
        if (this.mIndex >= 0) {
            sb.append(" #");
            sb.append(this.mIndex);
        }
        if (this.mName != null) {
            sb.append(" ");
            sb.append(this.mName);
        }
        sb.append("}");
        return sb.toString();
    }
    
    Fragment trackAddedFragmentsInPop(final ArrayList<Fragment> list, Fragment mFragment) {
        for (int i = this.mOps.size() - 1; i >= 0; --i) {
            final Op op = this.mOps.get(i);
            final int mCmd = op.mCmd;
            Label_0127: {
                if (mCmd != 1) {
                    if (mCmd != 3) {
                        switch (mCmd) {
                            default: {
                                continue;
                            }
                            case 10: {
                                op.mCurrentMaxState = op.mOldMaxState;
                                continue;
                            }
                            case 9: {
                                mFragment = op.mFragment;
                                continue;
                            }
                            case 8: {
                                mFragment = null;
                                continue;
                            }
                            case 6: {
                                break;
                            }
                            case 7: {
                                break Label_0127;
                            }
                        }
                    }
                    list.add(op.mFragment);
                    continue;
                }
            }
            list.remove(op.mFragment);
        }
        return mFragment;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.fragment.app;

import android.view.Window;
import android.view.LayoutInflater;
import androidx.lifecycle.ViewModelStore;
import androidx.activity.OnBackPressedDispatcher;
import androidx.activity.result.ActivityResultRegistry;
import androidx.core.app.PictureInPictureModeChangedInfo;
import androidx.core.app.MultiWindowModeChangedInfo;
import androidx.core.view.MenuProvider;
import androidx.core.view.MenuHost;
import androidx.savedstate.SavedStateRegistryOwner;
import androidx.activity.result.ActivityResultRegistryOwner;
import androidx.activity.OnBackPressedDispatcherOwner;
import androidx.lifecycle.ViewModelStoreOwner;
import androidx.core.app.OnPictureInPictureModeChangedProvider;
import androidx.core.app.OnMultiWindowModeChangedProvider;
import androidx.core.content.OnTrimMemoryProvider;
import androidx.core.content.OnConfigurationChangedProvider;
import android.content.IntentSender$SendIntentException;
import android.content.IntentSender;
import android.app.Activity;
import androidx.core.app.SharedElementCallback;
import android.view.MenuItem;
import android.os.Bundle;
import androidx.loader.app.LoaderManager;
import java.io.PrintWriter;
import java.io.FileDescriptor;
import android.util.AttributeSet;
import android.content.Context;
import android.view.View;
import java.util.Iterator;
import androidx.lifecycle.Lifecycle;
import androidx.activity.contextaware.OnContextAvailableListener;
import android.content.Intent;
import android.content.res.Configuration;
import androidx.core.util.Consumer;
import androidx.savedstate.SavedStateRegistry;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LifecycleRegistry;
import androidx.core.app.ActivityCompat;
import androidx.activity.ComponentActivity;

public class FragmentActivity extends ComponentActivity implements OnRequestPermissionsResultCallback, RequestPermissionsRequestCodeValidator
{
    static final String LIFECYCLE_TAG = "android:support:lifecycle";
    boolean mCreated;
    final LifecycleRegistry mFragmentLifecycleRegistry;
    final FragmentController mFragments;
    boolean mResumed;
    boolean mStopped;
    
    public FragmentActivity() {
        this.mFragments = FragmentController.createController(new HostCallbacks());
        this.mFragmentLifecycleRegistry = new LifecycleRegistry(this);
        this.mStopped = true;
        this.init();
    }
    
    public FragmentActivity(final int n) {
        super(n);
        this.mFragments = FragmentController.createController(new HostCallbacks());
        this.mFragmentLifecycleRegistry = new LifecycleRegistry(this);
        this.mStopped = true;
        this.init();
    }
    
    private void init() {
        this.getSavedStateRegistry().registerSavedStateProvider("android:support:lifecycle", (SavedStateRegistry.SavedStateProvider)new FragmentActivity$$ExternalSyntheticLambda0(this));
        this.addOnConfigurationChangedListener(new FragmentActivity$$ExternalSyntheticLambda1(this));
        this.addOnNewIntentListener(new FragmentActivity$$ExternalSyntheticLambda2(this));
        this.addOnContextAvailableListener(new FragmentActivity$$ExternalSyntheticLambda3(this));
    }
    
    private static boolean markState(final FragmentManager fragmentManager, final Lifecycle.State state) {
        final Iterator<Object> iterator = fragmentManager.getFragments().iterator();
        int n = 0;
        while (iterator.hasNext()) {
            final Fragment fragment = iterator.next();
            if (fragment == null) {
                continue;
            }
            boolean b = n != 0;
            if (fragment.getHost() != null) {
                b = ((n | (markState(fragment.getChildFragmentManager(), state) ? 1 : 0)) != 0x0);
            }
            n = (b ? 1 : 0);
            if (fragment.mViewLifecycleOwner != null) {
                n = (b ? 1 : 0);
                if (fragment.mViewLifecycleOwner.getLifecycle().getCurrentState().isAtLeast(Lifecycle.State.STARTED)) {
                    fragment.mViewLifecycleOwner.setCurrentState(state);
                    n = (true ? 1 : 0);
                }
            }
            if (!fragment.mLifecycleRegistry.getCurrentState().isAtLeast(Lifecycle.State.STARTED)) {
                continue;
            }
            fragment.mLifecycleRegistry.setCurrentState(state);
            n = (true ? 1 : 0);
        }
        return n != 0;
    }
    
    final View dispatchFragmentsOnCreateView(final View view, final String s, final Context context, final AttributeSet set) {
        return this.mFragments.onCreateView(view, s, context, set);
    }
    
    public void dump(final String s, final FileDescriptor fileDescriptor, final PrintWriter printWriter, final String[] array) {
        super.dump(s, fileDescriptor, printWriter, array);
        if (!this.shouldDumpInternalState(array)) {
            return;
        }
        printWriter.print(s);
        printWriter.print("Local FragmentActivity ");
        printWriter.print(Integer.toHexString(System.identityHashCode(this)));
        printWriter.println(" State:");
        final StringBuilder sb = new StringBuilder();
        sb.append(s);
        sb.append("  ");
        final String string = sb.toString();
        printWriter.print(string);
        printWriter.print("mCreated=");
        printWriter.print(this.mCreated);
        printWriter.print(" mResumed=");
        printWriter.print(this.mResumed);
        printWriter.print(" mStopped=");
        printWriter.print(this.mStopped);
        if (this.getApplication() != null) {
            LoaderManager.getInstance(this).dump(string, fileDescriptor, printWriter, array);
        }
        this.mFragments.getSupportFragmentManager().dump(s, fileDescriptor, printWriter, array);
    }
    
    public FragmentManager getSupportFragmentManager() {
        return this.mFragments.getSupportFragmentManager();
    }
    
    @Deprecated
    public LoaderManager getSupportLoaderManager() {
        return LoaderManager.getInstance(this);
    }
    
    void markFragmentsCreated() {
        while (markState(this.getSupportFragmentManager(), Lifecycle.State.CREATED)) {}
    }
    
    @Override
    protected void onActivityResult(final int n, final int n2, final Intent intent) {
        this.mFragments.noteStateNotSaved();
        super.onActivityResult(n, n2, intent);
    }
    
    @Deprecated
    public void onAttachFragment(final Fragment fragment) {
    }
    
    @Override
    protected void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.mFragmentLifecycleRegistry.handleLifecycleEvent(Lifecycle.Event.ON_CREATE);
        this.mFragments.dispatchCreate();
    }
    
    public View onCreateView(final View view, final String s, final Context context, final AttributeSet set) {
        final View dispatchFragmentsOnCreateView = this.dispatchFragmentsOnCreateView(view, s, context, set);
        if (dispatchFragmentsOnCreateView == null) {
            return super.onCreateView(view, s, context, set);
        }
        return dispatchFragmentsOnCreateView;
    }
    
    public View onCreateView(final String s, final Context context, final AttributeSet set) {
        final View dispatchFragmentsOnCreateView = this.dispatchFragmentsOnCreateView(null, s, context, set);
        if (dispatchFragmentsOnCreateView == null) {
            return super.onCreateView(s, context, set);
        }
        return dispatchFragmentsOnCreateView;
    }
    
    protected void onDestroy() {
        super.onDestroy();
        this.mFragments.dispatchDestroy();
        this.mFragmentLifecycleRegistry.handleLifecycleEvent(Lifecycle.Event.ON_DESTROY);
    }
    
    @Override
    public boolean onMenuItemSelected(final int n, final MenuItem menuItem) {
        return super.onMenuItemSelected(n, menuItem) || (n == 6 && this.mFragments.dispatchContextItemSelected(menuItem));
    }
    
    protected void onPause() {
        super.onPause();
        this.mResumed = false;
        this.mFragments.dispatchPause();
        this.mFragmentLifecycleRegistry.handleLifecycleEvent(Lifecycle.Event.ON_PAUSE);
    }
    
    protected void onPostResume() {
        super.onPostResume();
        this.onResumeFragments();
    }
    
    @Override
    public void onRequestPermissionsResult(final int n, final String[] array, final int[] array2) {
        this.mFragments.noteStateNotSaved();
        super.onRequestPermissionsResult(n, array, array2);
    }
    
    protected void onResume() {
        this.mFragments.noteStateNotSaved();
        super.onResume();
        this.mResumed = true;
        this.mFragments.execPendingActions();
    }
    
    protected void onResumeFragments() {
        this.mFragmentLifecycleRegistry.handleLifecycleEvent(Lifecycle.Event.ON_RESUME);
        this.mFragments.dispatchResume();
    }
    
    protected void onStart() {
        this.mFragments.noteStateNotSaved();
        super.onStart();
        this.mStopped = false;
        if (!this.mCreated) {
            this.mCreated = true;
            this.mFragments.dispatchActivityCreated();
        }
        this.mFragments.execPendingActions();
        this.mFragmentLifecycleRegistry.handleLifecycleEvent(Lifecycle.Event.ON_START);
        this.mFragments.dispatchStart();
    }
    
    public void onStateNotSaved() {
        this.mFragments.noteStateNotSaved();
    }
    
    protected void onStop() {
        super.onStop();
        this.mStopped = true;
        this.markFragmentsCreated();
        this.mFragments.dispatchStop();
        this.mFragmentLifecycleRegistry.handleLifecycleEvent(Lifecycle.Event.ON_STOP);
    }
    
    public void setEnterSharedElementCallback(final SharedElementCallback sharedElementCallback) {
        ActivityCompat.setEnterSharedElementCallback(this, sharedElementCallback);
    }
    
    public void setExitSharedElementCallback(final SharedElementCallback sharedElementCallback) {
        ActivityCompat.setExitSharedElementCallback(this, sharedElementCallback);
    }
    
    public void startActivityFromFragment(final Fragment fragment, final Intent intent, final int n) {
        this.startActivityFromFragment(fragment, intent, n, null);
    }
    
    public void startActivityFromFragment(final Fragment fragment, final Intent intent, final int n, final Bundle bundle) {
        if (n == -1) {
            ActivityCompat.startActivityForResult(this, intent, -1, bundle);
            return;
        }
        fragment.startActivityForResult(intent, n, bundle);
    }
    
    @Deprecated
    public void startIntentSenderFromFragment(final Fragment fragment, final IntentSender intentSender, final int n, final Intent intent, final int n2, final int n3, final int n4, final Bundle bundle) throws IntentSender$SendIntentException {
        if (n == -1) {
            ActivityCompat.startIntentSenderForResult(this, intentSender, n, intent, n2, n3, n4, bundle);
            return;
        }
        fragment.startIntentSenderForResult(intentSender, n, intent, n2, n3, n4, bundle);
    }
    
    public void supportFinishAfterTransition() {
        ActivityCompat.finishAfterTransition(this);
    }
    
    @Deprecated
    public void supportInvalidateOptionsMenu() {
        this.invalidateMenu();
    }
    
    public void supportPostponeEnterTransition() {
        ActivityCompat.postponeEnterTransition(this);
    }
    
    public void supportStartPostponedEnterTransition() {
        ActivityCompat.startPostponedEnterTransition(this);
    }
    
    @Deprecated
    @Override
    public final void validateRequestPermissionsRequestCode(final int n) {
    }
    
    class HostCallbacks extends FragmentHostCallback<FragmentActivity> implements OnConfigurationChangedProvider, OnTrimMemoryProvider, OnMultiWindowModeChangedProvider, OnPictureInPictureModeChangedProvider, ViewModelStoreOwner, OnBackPressedDispatcherOwner, ActivityResultRegistryOwner, SavedStateRegistryOwner, FragmentOnAttachListener, MenuHost
    {
        final FragmentActivity this$0;
        
        public HostCallbacks(final FragmentActivity this$0) {
            super(this.this$0 = this$0);
        }
        
        @Override
        public void addMenuProvider(final MenuProvider menuProvider) {
            this.this$0.addMenuProvider(menuProvider);
        }
        
        @Override
        public void addMenuProvider(final MenuProvider menuProvider, final LifecycleOwner lifecycleOwner) {
            this.this$0.addMenuProvider(menuProvider, lifecycleOwner);
        }
        
        @Override
        public void addMenuProvider(final MenuProvider menuProvider, final LifecycleOwner lifecycleOwner, final Lifecycle.State state) {
            this.this$0.addMenuProvider(menuProvider, lifecycleOwner, state);
        }
        
        @Override
        public void addOnConfigurationChangedListener(final Consumer<Configuration> consumer) {
            this.this$0.addOnConfigurationChangedListener(consumer);
        }
        
        @Override
        public void addOnMultiWindowModeChangedListener(final Consumer<MultiWindowModeChangedInfo> consumer) {
            this.this$0.addOnMultiWindowModeChangedListener(consumer);
        }
        
        @Override
        public void addOnPictureInPictureModeChangedListener(final Consumer<PictureInPictureModeChangedInfo> consumer) {
            this.this$0.addOnPictureInPictureModeChangedListener(consumer);
        }
        
        @Override
        public void addOnTrimMemoryListener(final Consumer<Integer> consumer) {
            this.this$0.addOnTrimMemoryListener(consumer);
        }
        
        @Override
        public ActivityResultRegistry getActivityResultRegistry() {
            return this.this$0.getActivityResultRegistry();
        }
        
        @Override
        public Lifecycle getLifecycle() {
            return this.this$0.mFragmentLifecycleRegistry;
        }
        
        @Override
        public OnBackPressedDispatcher getOnBackPressedDispatcher() {
            return this.this$0.getOnBackPressedDispatcher();
        }
        
        @Override
        public SavedStateRegistry getSavedStateRegistry() {
            return this.this$0.getSavedStateRegistry();
        }
        
        @Override
        public ViewModelStore getViewModelStore() {
            return this.this$0.getViewModelStore();
        }
        
        @Override
        public void invalidateMenu() {
            this.this$0.invalidateMenu();
        }
        
        @Override
        public void onAttachFragment(final FragmentManager fragmentManager, final Fragment fragment) {
            this.this$0.onAttachFragment(fragment);
        }
        
        @Override
        public void onDump(final String s, final FileDescriptor fileDescriptor, final PrintWriter printWriter, final String[] array) {
            this.this$0.dump(s, fileDescriptor, printWriter, array);
        }
        
        @Override
        public View onFindViewById(final int n) {
            return this.this$0.findViewById(n);
        }
        
        @Override
        public FragmentActivity onGetHost() {
            return this.this$0;
        }
        
        @Override
        public LayoutInflater onGetLayoutInflater() {
            return this.this$0.getLayoutInflater().cloneInContext((Context)this.this$0);
        }
        
        @Override
        public int onGetWindowAnimations() {
            final Window window = this.this$0.getWindow();
            int windowAnimations;
            if (window == null) {
                windowAnimations = 0;
            }
            else {
                windowAnimations = window.getAttributes().windowAnimations;
            }
            return windowAnimations;
        }
        
        @Override
        public boolean onHasView() {
            final Window window = this.this$0.getWindow();
            return window != null && window.peekDecorView() != null;
        }
        
        @Override
        public boolean onHasWindowAnimations() {
            return this.this$0.getWindow() != null;
        }
        
        @Override
        public boolean onShouldSaveFragmentState(final Fragment fragment) {
            return this.this$0.isFinishing() ^ true;
        }
        
        @Override
        public boolean onShouldShowRequestPermissionRationale(final String s) {
            return ActivityCompat.shouldShowRequestPermissionRationale(this.this$0, s);
        }
        
        @Override
        public void onSupportInvalidateOptionsMenu() {
            this.invalidateMenu();
        }
        
        @Override
        public void removeMenuProvider(final MenuProvider menuProvider) {
            this.this$0.removeMenuProvider(menuProvider);
        }
        
        @Override
        public void removeOnConfigurationChangedListener(final Consumer<Configuration> consumer) {
            this.this$0.removeOnConfigurationChangedListener(consumer);
        }
        
        @Override
        public void removeOnMultiWindowModeChangedListener(final Consumer<MultiWindowModeChangedInfo> consumer) {
            this.this$0.removeOnMultiWindowModeChangedListener(consumer);
        }
        
        @Override
        public void removeOnPictureInPictureModeChangedListener(final Consumer<PictureInPictureModeChangedInfo> consumer) {
            this.this$0.removeOnPictureInPictureModeChangedListener(consumer);
        }
        
        @Override
        public void removeOnTrimMemoryListener(final Consumer<Integer> consumer) {
            this.this$0.removeOnTrimMemoryListener(consumer);
        }
    }
}

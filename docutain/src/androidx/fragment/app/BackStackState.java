// 
// Decompiled by Procyon v0.6.0
// 

package androidx.fragment.app;

import java.util.Iterator;
import java.util.ArrayList;
import android.os.Bundle;
import java.util.HashMap;
import java.util.Map;
import android.os.Parcel;
import java.util.List;
import android.os.Parcelable$Creator;
import android.os.Parcelable;

class BackStackState implements Parcelable
{
    public static final Parcelable$Creator<BackStackState> CREATOR;
    final List<String> mFragments;
    final List<BackStackRecordState> mTransactions;
    
    static {
        CREATOR = (Parcelable$Creator)new Parcelable$Creator<BackStackState>() {
            public BackStackState createFromParcel(final Parcel parcel) {
                return new BackStackState(parcel);
            }
            
            public BackStackState[] newArray(final int n) {
                return new BackStackState[n];
            }
        };
    }
    
    BackStackState(final Parcel parcel) {
        this.mFragments = parcel.createStringArrayList();
        this.mTransactions = parcel.createTypedArrayList((Parcelable$Creator)BackStackRecordState.CREATOR);
    }
    
    BackStackState(final List<String> mFragments, final List<BackStackRecordState> mTransactions) {
        this.mFragments = mFragments;
        this.mTransactions = mTransactions;
    }
    
    public int describeContents() {
        return 0;
    }
    
    List<BackStackRecord> instantiate(final FragmentManager fragmentManager, final Map<String, Fragment> map) {
        final HashMap hashMap = new HashMap(this.mFragments.size());
        for (final String s : this.mFragments) {
            final Fragment value = map.get(s);
            if (value != null) {
                hashMap.put(value.mWho, value);
            }
            else {
                final Bundle setSavedState = fragmentManager.getFragmentStore().setSavedState(s, null);
                if (setSavedState == null) {
                    continue;
                }
                final ClassLoader classLoader = fragmentManager.getHost().getContext().getClassLoader();
                final Fragment instantiate = ((FragmentState)setSavedState.getParcelable("state")).instantiate(fragmentManager.getFragmentFactory(), classLoader);
                instantiate.mSavedFragmentState = setSavedState;
                if (instantiate.mSavedFragmentState.getBundle("savedInstanceState") == null) {
                    instantiate.mSavedFragmentState.putBundle("savedInstanceState", new Bundle());
                }
                final Bundle bundle = setSavedState.getBundle("arguments");
                if (bundle != null) {
                    bundle.setClassLoader(classLoader);
                }
                instantiate.setArguments(bundle);
                hashMap.put(instantiate.mWho, instantiate);
            }
        }
        final ArrayList<BackStackRecord> list = new ArrayList<BackStackRecord>();
        final Iterator<BackStackRecordState> iterator2 = this.mTransactions.iterator();
        while (iterator2.hasNext()) {
            list.add(iterator2.next().instantiate(fragmentManager, hashMap));
        }
        return list;
    }
    
    public void writeToParcel(final Parcel parcel, final int n) {
        parcel.writeStringList((List)this.mFragments);
        parcel.writeTypedList((List)this.mTransactions);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.fragment.app;

import androidx.lifecycle.LifecycleOwner;
import android.os.Bundle;

public interface FragmentResultOwner
{
    void clearFragmentResult(final String p0);
    
    void clearFragmentResultListener(final String p0);
    
    void setFragmentResult(final String p0, final Bundle p1);
    
    void setFragmentResultListener(final String p0, final LifecycleOwner p1, final FragmentResultListener p2);
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.fragment.app;

import kotlin.Deprecated;
import kotlin.jvm.internal.Intrinsics;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000\"\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\u001a3\u0010\u0000\u001a\u00020\u0001*\u00020\u00022\b\b\u0002\u0010\u0003\u001a\u00020\u00042\u0017\u0010\u0005\u001a\u0013\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\u00010\u0006¢\u0006\u0002\b\bH\u0086\b\u00f8\u0001\u0000\u001a3\u0010\t\u001a\u00020\u0001*\u00020\u00022\b\b\u0002\u0010\u0003\u001a\u00020\u00042\u0017\u0010\u0005\u001a\u0013\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\u00010\u0006¢\u0006\u0002\b\bH\u0087\b\u00f8\u0001\u0000\u001a=\u0010\n\u001a\u00020\u0001*\u00020\u00022\b\b\u0002\u0010\u000b\u001a\u00020\u00042\b\b\u0002\u0010\u0003\u001a\u00020\u00042\u0017\u0010\u0005\u001a\u0013\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\u00010\u0006¢\u0006\u0002\b\bH\u0087\b\u00f8\u0001\u0000\u0082\u0002\u0007\n\u0005\b\u009920\u0001¨\u0006\f" }, d2 = { "commit", "", "Landroidx/fragment/app/FragmentManager;", "allowStateLoss", "", "body", "Lkotlin/Function1;", "Landroidx/fragment/app/FragmentTransaction;", "Lkotlin/ExtensionFunctionType;", "commitNow", "transaction", "now", "fragment-ktx_release" }, k = 2, mv = { 1, 8, 0 }, xi = 48)
public final class FragmentManagerKt
{
    public static final void commit(final FragmentManager fragmentManager, final boolean b, final Function1<? super FragmentTransaction, Unit> function1) {
        Intrinsics.checkNotNullParameter((Object)fragmentManager, "<this>");
        Intrinsics.checkNotNullParameter((Object)function1, "body");
        final FragmentTransaction beginTransaction = fragmentManager.beginTransaction();
        Intrinsics.checkNotNullExpressionValue((Object)beginTransaction, "beginTransaction()");
        function1.invoke((Object)beginTransaction);
        if (b) {
            beginTransaction.commitAllowingStateLoss();
        }
        else {
            beginTransaction.commit();
        }
    }
    
    public static final void commitNow(final FragmentManager fragmentManager, final boolean b, final Function1<? super FragmentTransaction, Unit> function1) {
        Intrinsics.checkNotNullParameter((Object)fragmentManager, "<this>");
        Intrinsics.checkNotNullParameter((Object)function1, "body");
        final FragmentTransaction beginTransaction = fragmentManager.beginTransaction();
        Intrinsics.checkNotNullExpressionValue((Object)beginTransaction, "beginTransaction()");
        function1.invoke((Object)beginTransaction);
        if (b) {
            beginTransaction.commitNowAllowingStateLoss();
        }
        else {
            beginTransaction.commitNow();
        }
    }
    
    @Deprecated(message = "Use commit { .. } or commitNow { .. } extensions")
    public static final void transaction(final FragmentManager fragmentManager, final boolean b, final boolean b2, final Function1<? super FragmentTransaction, Unit> function1) {
        Intrinsics.checkNotNullParameter((Object)fragmentManager, "<this>");
        Intrinsics.checkNotNullParameter((Object)function1, "body");
        final FragmentTransaction beginTransaction = fragmentManager.beginTransaction();
        Intrinsics.checkNotNullExpressionValue((Object)beginTransaction, "beginTransaction()");
        function1.invoke((Object)beginTransaction);
        if (b) {
            if (b2) {
                beginTransaction.commitNowAllowingStateLoss();
            }
            else {
                beginTransaction.commitNow();
            }
        }
        else if (b2) {
            beginTransaction.commitAllowingStateLoss();
        }
        else {
            beginTransaction.commit();
        }
    }
}

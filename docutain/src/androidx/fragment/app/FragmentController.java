// 
// Decompiled by Procyon v0.6.0
// 

package androidx.fragment.app;

import java.util.ArrayList;
import androidx.lifecycle.ViewModelStoreOwner;
import androidx.collection.SimpleArrayMap;
import androidx.lifecycle.ViewModelStore;
import java.util.Map;
import java.util.Collection;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.content.Context;
import android.view.View;
import androidx.loader.app.LoaderManager;
import java.util.List;
import java.io.PrintWriter;
import java.io.FileDescriptor;
import android.view.MenuInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.content.res.Configuration;
import androidx.core.util.Preconditions;

public class FragmentController
{
    private final FragmentHostCallback<?> mHost;
    
    private FragmentController(final FragmentHostCallback<?> mHost) {
        this.mHost = mHost;
    }
    
    public static FragmentController createController(final FragmentHostCallback<?> fragmentHostCallback) {
        return new FragmentController(Preconditions.checkNotNull(fragmentHostCallback, "callbacks == null"));
    }
    
    public void attachHost(final Fragment fragment) {
        final FragmentManager mFragmentManager = this.mHost.mFragmentManager;
        final FragmentHostCallback<?> mHost = this.mHost;
        mFragmentManager.attachController(mHost, mHost, fragment);
    }
    
    public void dispatchActivityCreated() {
        this.mHost.mFragmentManager.dispatchActivityCreated();
    }
    
    @Deprecated
    public void dispatchConfigurationChanged(final Configuration configuration) {
        this.mHost.mFragmentManager.dispatchConfigurationChanged(configuration, true);
    }
    
    public boolean dispatchContextItemSelected(final MenuItem menuItem) {
        return this.mHost.mFragmentManager.dispatchContextItemSelected(menuItem);
    }
    
    public void dispatchCreate() {
        this.mHost.mFragmentManager.dispatchCreate();
    }
    
    @Deprecated
    public boolean dispatchCreateOptionsMenu(final Menu menu, final MenuInflater menuInflater) {
        return this.mHost.mFragmentManager.dispatchCreateOptionsMenu(menu, menuInflater);
    }
    
    public void dispatchDestroy() {
        this.mHost.mFragmentManager.dispatchDestroy();
    }
    
    public void dispatchDestroyView() {
        this.mHost.mFragmentManager.dispatchDestroyView();
    }
    
    @Deprecated
    public void dispatchLowMemory() {
        this.mHost.mFragmentManager.dispatchLowMemory(true);
    }
    
    @Deprecated
    public void dispatchMultiWindowModeChanged(final boolean b) {
        this.mHost.mFragmentManager.dispatchMultiWindowModeChanged(b, true);
    }
    
    @Deprecated
    public boolean dispatchOptionsItemSelected(final MenuItem menuItem) {
        return this.mHost.mFragmentManager.dispatchOptionsItemSelected(menuItem);
    }
    
    @Deprecated
    public void dispatchOptionsMenuClosed(final Menu menu) {
        this.mHost.mFragmentManager.dispatchOptionsMenuClosed(menu);
    }
    
    public void dispatchPause() {
        this.mHost.mFragmentManager.dispatchPause();
    }
    
    @Deprecated
    public void dispatchPictureInPictureModeChanged(final boolean b) {
        this.mHost.mFragmentManager.dispatchPictureInPictureModeChanged(b, true);
    }
    
    @Deprecated
    public boolean dispatchPrepareOptionsMenu(final Menu menu) {
        return this.mHost.mFragmentManager.dispatchPrepareOptionsMenu(menu);
    }
    
    @Deprecated
    public void dispatchReallyStop() {
    }
    
    public void dispatchResume() {
        this.mHost.mFragmentManager.dispatchResume();
    }
    
    public void dispatchStart() {
        this.mHost.mFragmentManager.dispatchStart();
    }
    
    public void dispatchStop() {
        this.mHost.mFragmentManager.dispatchStop();
    }
    
    @Deprecated
    public void doLoaderDestroy() {
    }
    
    @Deprecated
    public void doLoaderRetain() {
    }
    
    @Deprecated
    public void doLoaderStart() {
    }
    
    @Deprecated
    public void doLoaderStop(final boolean b) {
    }
    
    @Deprecated
    public void dumpLoaders(final String s, final FileDescriptor fileDescriptor, final PrintWriter printWriter, final String[] array) {
    }
    
    public boolean execPendingActions() {
        return this.mHost.mFragmentManager.execPendingActions(true);
    }
    
    public Fragment findFragmentByWho(final String s) {
        return this.mHost.mFragmentManager.findFragmentByWho(s);
    }
    
    public List<Fragment> getActiveFragments(final List<Fragment> list) {
        return this.mHost.mFragmentManager.getActiveFragments();
    }
    
    public int getActiveFragmentsCount() {
        return this.mHost.mFragmentManager.getActiveFragmentCount();
    }
    
    public FragmentManager getSupportFragmentManager() {
        return this.mHost.mFragmentManager;
    }
    
    @Deprecated
    public LoaderManager getSupportLoaderManager() {
        throw new UnsupportedOperationException("Loaders are managed separately from FragmentController, use LoaderManager.getInstance() to obtain a LoaderManager.");
    }
    
    public void noteStateNotSaved() {
        this.mHost.mFragmentManager.noteStateNotSaved();
    }
    
    public View onCreateView(final View view, final String s, final Context context, final AttributeSet set) {
        return this.mHost.mFragmentManager.getLayoutInflaterFactory().onCreateView(view, s, context, set);
    }
    
    @Deprecated
    public void reportLoaderStart() {
    }
    
    @Deprecated
    public void restoreAllState(final Parcelable parcelable, final FragmentManagerNonConfig fragmentManagerNonConfig) {
        this.mHost.mFragmentManager.restoreAllState(parcelable, fragmentManagerNonConfig);
    }
    
    @Deprecated
    public void restoreAllState(final Parcelable parcelable, final List<Fragment> list) {
        this.mHost.mFragmentManager.restoreAllState(parcelable, new FragmentManagerNonConfig(list, null, null));
    }
    
    @Deprecated
    public void restoreLoaderNonConfig(final SimpleArrayMap<String, LoaderManager> simpleArrayMap) {
    }
    
    @Deprecated
    public void restoreSaveState(final Parcelable parcelable) {
        final FragmentHostCallback<?> mHost = this.mHost;
        if (mHost instanceof ViewModelStoreOwner) {
            mHost.mFragmentManager.restoreSaveState(parcelable);
            return;
        }
        throw new IllegalStateException("Your FragmentHostCallback must implement ViewModelStoreOwner to call restoreSaveState(). Call restoreAllState()  if you're still using retainNestedNonConfig().");
    }
    
    @Deprecated
    public SimpleArrayMap<String, LoaderManager> retainLoaderNonConfig() {
        return null;
    }
    
    @Deprecated
    public FragmentManagerNonConfig retainNestedNonConfig() {
        return this.mHost.mFragmentManager.retainNonConfig();
    }
    
    @Deprecated
    public List<Fragment> retainNonConfig() {
        final FragmentManagerNonConfig retainNonConfig = this.mHost.mFragmentManager.retainNonConfig();
        ArrayList list;
        if (retainNonConfig != null && retainNonConfig.getFragments() != null) {
            list = new ArrayList(retainNonConfig.getFragments());
        }
        else {
            list = null;
        }
        return list;
    }
    
    @Deprecated
    public Parcelable saveAllState() {
        return this.mHost.mFragmentManager.saveAllState();
    }
}

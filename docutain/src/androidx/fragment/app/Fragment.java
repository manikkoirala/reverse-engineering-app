// 
// Decompiled by Procyon v0.6.0
// 

package androidx.fragment.app;

import android.os.Parcel;
import android.os.Parcelable$ClassLoaderCreator;
import android.os.Parcelable$Creator;
import android.content.IntentSender$SendIntentException;
import android.content.IntentSender;
import androidx.activity.result.ActivityResultRegistryOwner;
import android.os.Looper;
import java.util.concurrent.TimeUnit;
import androidx.savedstate.ViewTreeSavedStateRegistryOwner;
import androidx.lifecycle.ViewTreeViewModelStoreOwner;
import androidx.lifecycle.ViewTreeLifecycleOwner;
import androidx.lifecycle.LifecycleObserver;
import androidx.lifecycle.LifecycleEventObserver;
import android.os.Build$VERSION;
import java.util.Iterator;
import android.util.AttributeSet;
import android.view.MenuInflater;
import android.view.Menu;
import android.view.ContextMenu$ContextMenuInfo;
import android.view.ContextMenu;
import android.animation.Animator;
import android.view.animation.Animation;
import android.view.MenuItem;
import android.content.res.Configuration;
import android.app.Activity;
import android.content.Intent;
import androidx.lifecycle.ViewModelStore;
import androidx.lifecycle.LiveData;
import androidx.savedstate.SavedStateRegistry;
import android.content.res.Resources;
import androidx.core.view.LayoutInflaterCompat;
import androidx.core.app.SharedElementCallback;
import androidx.lifecycle.SavedStateViewModelFactory;
import androidx.lifecycle.viewmodel.MutableCreationExtras;
import android.app.Application;
import android.content.ContextWrapper;
import androidx.lifecycle.viewmodel.CreationExtras;
import androidx.loader.app.LoaderManager;
import java.io.PrintWriter;
import java.io.FileDescriptor;
import android.util.Log;
import androidx.core.app.ActivityOptionsCompat;
import java.util.concurrent.atomic.AtomicReference;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultRegistry;
import androidx.arch.core.util.Function;
import androidx.activity.result.contract.ActivityResultContract;
import java.lang.reflect.InvocationTargetException;
import android.content.Context;
import androidx.fragment.app.strictmode.FragmentStrictMode;
import androidx.lifecycle.SavedStateHandleSupport;
import java.util.UUID;
import androidx.lifecycle.MutableLiveData;
import android.view.View;
import android.os.Parcelable;
import android.util.SparseArray;
import androidx.savedstate.SavedStateRegistryController;
import android.os.Handler;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicInteger;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleRegistry;
import android.view.LayoutInflater;
import androidx.lifecycle.ViewModelProvider;
import android.view.ViewGroup;
import android.os.Bundle;
import androidx.activity.result.ActivityResultCaller;
import androidx.savedstate.SavedStateRegistryOwner;
import androidx.lifecycle.HasDefaultViewModelProviderFactory;
import androidx.lifecycle.ViewModelStoreOwner;
import androidx.lifecycle.LifecycleOwner;
import android.view.View$OnCreateContextMenuListener;
import android.content.ComponentCallbacks;

public class Fragment implements ComponentCallbacks, View$OnCreateContextMenuListener, LifecycleOwner, ViewModelStoreOwner, HasDefaultViewModelProviderFactory, SavedStateRegistryOwner, ActivityResultCaller
{
    static final int ACTIVITY_CREATED = 4;
    static final int ATTACHED = 0;
    static final int AWAITING_ENTER_EFFECTS = 6;
    static final int AWAITING_EXIT_EFFECTS = 3;
    static final int CREATED = 1;
    static final int INITIALIZING = -1;
    static final int RESUMED = 7;
    static final int STARTED = 5;
    static final Object USE_DEFAULT_TRANSITION;
    static final int VIEW_CREATED = 2;
    boolean mAdded;
    AnimationInfo mAnimationInfo;
    Bundle mArguments;
    int mBackStackNesting;
    boolean mBeingSaved;
    private boolean mCalled;
    FragmentManager mChildFragmentManager;
    ViewGroup mContainer;
    int mContainerId;
    private int mContentLayoutId;
    ViewModelProvider.Factory mDefaultFactory;
    boolean mDeferStart;
    boolean mDetached;
    int mFragmentId;
    FragmentManager mFragmentManager;
    boolean mFromLayout;
    boolean mHasMenu;
    boolean mHidden;
    boolean mHiddenChanged;
    FragmentHostCallback<?> mHost;
    boolean mInLayout;
    boolean mIsCreated;
    private Boolean mIsPrimaryNavigationFragment;
    LayoutInflater mLayoutInflater;
    LifecycleRegistry mLifecycleRegistry;
    Lifecycle.State mMaxState;
    boolean mMenuVisible;
    private final AtomicInteger mNextLocalRequestCode;
    private final ArrayList<OnPreAttachedListener> mOnPreAttachedListeners;
    Fragment mParentFragment;
    boolean mPerformedCreateView;
    Runnable mPostponedDurationRunnable;
    Handler mPostponedHandler;
    public String mPreviousWho;
    boolean mRemoving;
    boolean mRestored;
    boolean mRetainInstance;
    boolean mRetainInstanceChangedWhileDetached;
    Bundle mSavedFragmentState;
    private final OnPreAttachedListener mSavedStateAttachListener;
    SavedStateRegistryController mSavedStateRegistryController;
    Boolean mSavedUserVisibleHint;
    Bundle mSavedViewRegistryState;
    SparseArray<Parcelable> mSavedViewState;
    int mState;
    String mTag;
    Fragment mTarget;
    int mTargetRequestCode;
    String mTargetWho;
    boolean mUserVisibleHint;
    View mView;
    FragmentViewLifecycleOwner mViewLifecycleOwner;
    MutableLiveData<LifecycleOwner> mViewLifecycleOwnerLiveData;
    String mWho;
    
    static {
        USE_DEFAULT_TRANSITION = new Object();
    }
    
    public Fragment() {
        this.mState = -1;
        this.mWho = UUID.randomUUID().toString();
        this.mTargetWho = null;
        this.mIsPrimaryNavigationFragment = null;
        this.mChildFragmentManager = new FragmentManagerImpl();
        this.mMenuVisible = true;
        this.mUserVisibleHint = true;
        this.mPostponedDurationRunnable = new Runnable() {
            final Fragment this$0;
            
            @Override
            public void run() {
                this.this$0.startPostponedEnterTransition();
            }
        };
        this.mMaxState = Lifecycle.State.RESUMED;
        this.mViewLifecycleOwnerLiveData = new MutableLiveData<LifecycleOwner>();
        this.mNextLocalRequestCode = new AtomicInteger();
        this.mOnPreAttachedListeners = new ArrayList<OnPreAttachedListener>();
        this.mSavedStateAttachListener = (OnPreAttachedListener)new OnPreAttachedListener() {
            final Fragment this$0;
            
            @Override
            void onPreAttached() {
                this.this$0.mSavedStateRegistryController.performAttach();
                SavedStateHandleSupport.enableSavedStateHandles(this.this$0);
                Bundle bundle;
                if (this.this$0.mSavedFragmentState != null) {
                    bundle = this.this$0.mSavedFragmentState.getBundle("registryState");
                }
                else {
                    bundle = null;
                }
                this.this$0.mSavedStateRegistryController.performRestore(bundle);
            }
        };
        this.initLifecycle();
    }
    
    public Fragment(final int mContentLayoutId) {
        this();
        this.mContentLayoutId = mContentLayoutId;
    }
    
    private AnimationInfo ensureAnimationInfo() {
        if (this.mAnimationInfo == null) {
            this.mAnimationInfo = new AnimationInfo();
        }
        return this.mAnimationInfo;
    }
    
    private int getMinimumMaxLifecycleState() {
        if (this.mMaxState != Lifecycle.State.INITIALIZED && this.mParentFragment != null) {
            return Math.min(this.mMaxState.ordinal(), this.mParentFragment.getMinimumMaxLifecycleState());
        }
        return this.mMaxState.ordinal();
    }
    
    private Fragment getTargetFragment(final boolean b) {
        if (b) {
            FragmentStrictMode.onGetTargetFragmentUsage(this);
        }
        final Fragment mTarget = this.mTarget;
        if (mTarget != null) {
            return mTarget;
        }
        final FragmentManager mFragmentManager = this.mFragmentManager;
        if (mFragmentManager != null) {
            final String mTargetWho = this.mTargetWho;
            if (mTargetWho != null) {
                return mFragmentManager.findActiveFragment(mTargetWho);
            }
        }
        return null;
    }
    
    private void initLifecycle() {
        this.mLifecycleRegistry = new LifecycleRegistry(this);
        this.mSavedStateRegistryController = SavedStateRegistryController.create(this);
        this.mDefaultFactory = null;
        if (!this.mOnPreAttachedListeners.contains(this.mSavedStateAttachListener)) {
            this.registerOnPreAttachListener(this.mSavedStateAttachListener);
        }
    }
    
    @Deprecated
    public static Fragment instantiate(final Context context, final String s) {
        return instantiate(context, s, null);
    }
    
    @Deprecated
    public static Fragment instantiate(final Context context, final String s, final Bundle arguments) {
        try {
            final Fragment fragment = (Fragment)FragmentFactory.loadFragmentClass(context.getClassLoader(), s).getConstructor((Class<?>[])new Class[0]).newInstance(new Object[0]);
            if (arguments != null) {
                arguments.setClassLoader(fragment.getClass().getClassLoader());
                fragment.setArguments(arguments);
            }
            return fragment;
        }
        catch (final InvocationTargetException ex) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Unable to instantiate fragment ");
            sb.append(s);
            sb.append(": calling Fragment constructor caused an exception");
            throw new InstantiationException(sb.toString(), ex);
        }
        catch (final NoSuchMethodException ex2) {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("Unable to instantiate fragment ");
            sb2.append(s);
            sb2.append(": could not find Fragment constructor");
            throw new InstantiationException(sb2.toString(), ex2);
        }
        catch (final IllegalAccessException ex3) {
            final StringBuilder sb3 = new StringBuilder();
            sb3.append("Unable to instantiate fragment ");
            sb3.append(s);
            sb3.append(": make sure class name exists, is public, and has an empty constructor that is public");
            throw new InstantiationException(sb3.toString(), ex3);
        }
        catch (final java.lang.InstantiationException ex4) {
            final StringBuilder sb4 = new StringBuilder();
            sb4.append("Unable to instantiate fragment ");
            sb4.append(s);
            sb4.append(": make sure class name exists, is public, and has an empty constructor that is public");
            throw new InstantiationException(sb4.toString(), ex4);
        }
    }
    
    private <I, O> ActivityResultLauncher<I> prepareCallInternal(final ActivityResultContract<I, O> activityResultContract, final Function<Void, ActivityResultRegistry> function, final ActivityResultCallback<O> activityResultCallback) {
        if (this.mState <= 1) {
            final AtomicReference atomicReference = new AtomicReference();
            this.registerOnPreAttachListener((OnPreAttachedListener)new OnPreAttachedListener(this, function, atomicReference, activityResultContract, activityResultCallback) {
                final Fragment this$0;
                final ActivityResultCallback val$callback;
                final ActivityResultContract val$contract;
                final AtomicReference val$ref;
                final Function val$registryProvider;
                
                @Override
                void onPreAttached() {
                    this.val$ref.set(this.val$registryProvider.apply(null).register(this.this$0.generateActivityResultKey(), this.this$0, (ActivityResultContract<Object, Object>)this.val$contract, this.val$callback));
                }
            });
            return new ActivityResultLauncher<I>(this, atomicReference, activityResultContract) {
                final Fragment this$0;
                final ActivityResultContract val$contract;
                final AtomicReference val$ref;
                
                @Override
                public ActivityResultContract<I, ?> getContract() {
                    return this.val$contract;
                }
                
                @Override
                public void launch(final I n, final ActivityOptionsCompat activityOptionsCompat) {
                    final ActivityResultLauncher activityResultLauncher = this.val$ref.get();
                    if (activityResultLauncher != null) {
                        activityResultLauncher.launch(n, activityOptionsCompat);
                        return;
                    }
                    throw new IllegalStateException("Operation cannot be started before fragment is in created state");
                }
                
                @Override
                public void unregister() {
                    final ActivityResultLauncher activityResultLauncher = this.val$ref.getAndSet(null);
                    if (activityResultLauncher != null) {
                        activityResultLauncher.unregister();
                    }
                }
            };
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Fragment ");
        sb.append(this);
        sb.append(" is attempting to registerForActivityResult after being created. Fragments must call registerForActivityResult() before they are created (i.e. initialization, onAttach(), or onCreate()).");
        throw new IllegalStateException(sb.toString());
    }
    
    private void registerOnPreAttachListener(final OnPreAttachedListener e) {
        if (this.mState >= 0) {
            e.onPreAttached();
        }
        else {
            this.mOnPreAttachedListeners.add(e);
        }
    }
    
    private void restoreViewState() {
        if (FragmentManager.isLoggingEnabled(3)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("moveto RESTORE_VIEW_STATE: ");
            sb.append(this);
            Log.d("FragmentManager", sb.toString());
        }
        if (this.mView != null) {
            final Bundle mSavedFragmentState = this.mSavedFragmentState;
            Bundle bundle;
            if (mSavedFragmentState != null) {
                bundle = mSavedFragmentState.getBundle("savedInstanceState");
            }
            else {
                bundle = null;
            }
            this.restoreViewState(bundle);
        }
        this.mSavedFragmentState = null;
    }
    
    void callStartTransitionListener(final boolean b) {
        final AnimationInfo mAnimationInfo = this.mAnimationInfo;
        if (mAnimationInfo != null) {
            mAnimationInfo.mEnterTransitionPostponed = false;
        }
        if (this.mView != null) {
            final ViewGroup mContainer = this.mContainer;
            if (mContainer != null) {
                final FragmentManager mFragmentManager = this.mFragmentManager;
                if (mFragmentManager != null) {
                    final SpecialEffectsController orCreateController = SpecialEffectsController.getOrCreateController(mContainer, mFragmentManager);
                    orCreateController.markPostponedState();
                    if (b) {
                        this.mHost.getHandler().post((Runnable)new Runnable(this, orCreateController) {
                            final Fragment this$0;
                            final SpecialEffectsController val$controller;
                            
                            @Override
                            public void run() {
                                this.val$controller.executePendingOperations();
                            }
                        });
                    }
                    else {
                        orCreateController.executePendingOperations();
                    }
                    final Handler mPostponedHandler = this.mPostponedHandler;
                    if (mPostponedHandler != null) {
                        mPostponedHandler.removeCallbacks(this.mPostponedDurationRunnable);
                        this.mPostponedHandler = null;
                    }
                }
            }
        }
    }
    
    FragmentContainer createFragmentContainer() {
        return new FragmentContainer(this) {
            final Fragment this$0;
            
            @Override
            public View onFindViewById(final int n) {
                if (this.this$0.mView != null) {
                    return this.this$0.mView.findViewById(n);
                }
                final StringBuilder sb = new StringBuilder();
                sb.append("Fragment ");
                sb.append(this.this$0);
                sb.append(" does not have a view");
                throw new IllegalStateException(sb.toString());
            }
            
            @Override
            public boolean onHasView() {
                return this.this$0.mView != null;
            }
        };
    }
    
    public void dump(final String str, final FileDescriptor fileDescriptor, final PrintWriter printWriter, final String[] array) {
        printWriter.print(str);
        printWriter.print("mFragmentId=#");
        printWriter.print(Integer.toHexString(this.mFragmentId));
        printWriter.print(" mContainerId=#");
        printWriter.print(Integer.toHexString(this.mContainerId));
        printWriter.print(" mTag=");
        printWriter.println(this.mTag);
        printWriter.print(str);
        printWriter.print("mState=");
        printWriter.print(this.mState);
        printWriter.print(" mWho=");
        printWriter.print(this.mWho);
        printWriter.print(" mBackStackNesting=");
        printWriter.println(this.mBackStackNesting);
        printWriter.print(str);
        printWriter.print("mAdded=");
        printWriter.print(this.mAdded);
        printWriter.print(" mRemoving=");
        printWriter.print(this.mRemoving);
        printWriter.print(" mFromLayout=");
        printWriter.print(this.mFromLayout);
        printWriter.print(" mInLayout=");
        printWriter.println(this.mInLayout);
        printWriter.print(str);
        printWriter.print("mHidden=");
        printWriter.print(this.mHidden);
        printWriter.print(" mDetached=");
        printWriter.print(this.mDetached);
        printWriter.print(" mMenuVisible=");
        printWriter.print(this.mMenuVisible);
        printWriter.print(" mHasMenu=");
        printWriter.println(this.mHasMenu);
        printWriter.print(str);
        printWriter.print("mRetainInstance=");
        printWriter.print(this.mRetainInstance);
        printWriter.print(" mUserVisibleHint=");
        printWriter.println(this.mUserVisibleHint);
        if (this.mFragmentManager != null) {
            printWriter.print(str);
            printWriter.print("mFragmentManager=");
            printWriter.println(this.mFragmentManager);
        }
        if (this.mHost != null) {
            printWriter.print(str);
            printWriter.print("mHost=");
            printWriter.println(this.mHost);
        }
        if (this.mParentFragment != null) {
            printWriter.print(str);
            printWriter.print("mParentFragment=");
            printWriter.println(this.mParentFragment);
        }
        if (this.mArguments != null) {
            printWriter.print(str);
            printWriter.print("mArguments=");
            printWriter.println(this.mArguments);
        }
        if (this.mSavedFragmentState != null) {
            printWriter.print(str);
            printWriter.print("mSavedFragmentState=");
            printWriter.println(this.mSavedFragmentState);
        }
        if (this.mSavedViewState != null) {
            printWriter.print(str);
            printWriter.print("mSavedViewState=");
            printWriter.println(this.mSavedViewState);
        }
        if (this.mSavedViewRegistryState != null) {
            printWriter.print(str);
            printWriter.print("mSavedViewRegistryState=");
            printWriter.println(this.mSavedViewRegistryState);
        }
        final Fragment targetFragment = this.getTargetFragment(false);
        if (targetFragment != null) {
            printWriter.print(str);
            printWriter.print("mTarget=");
            printWriter.print(targetFragment);
            printWriter.print(" mTargetRequestCode=");
            printWriter.println(this.mTargetRequestCode);
        }
        printWriter.print(str);
        printWriter.print("mPopDirection=");
        printWriter.println(this.getPopDirection());
        if (this.getEnterAnim() != 0) {
            printWriter.print(str);
            printWriter.print("getEnterAnim=");
            printWriter.println(this.getEnterAnim());
        }
        if (this.getExitAnim() != 0) {
            printWriter.print(str);
            printWriter.print("getExitAnim=");
            printWriter.println(this.getExitAnim());
        }
        if (this.getPopEnterAnim() != 0) {
            printWriter.print(str);
            printWriter.print("getPopEnterAnim=");
            printWriter.println(this.getPopEnterAnim());
        }
        if (this.getPopExitAnim() != 0) {
            printWriter.print(str);
            printWriter.print("getPopExitAnim=");
            printWriter.println(this.getPopExitAnim());
        }
        if (this.mContainer != null) {
            printWriter.print(str);
            printWriter.print("mContainer=");
            printWriter.println(this.mContainer);
        }
        if (this.mView != null) {
            printWriter.print(str);
            printWriter.print("mView=");
            printWriter.println(this.mView);
        }
        if (this.getAnimatingAway() != null) {
            printWriter.print(str);
            printWriter.print("mAnimatingAway=");
            printWriter.println(this.getAnimatingAway());
        }
        if (this.getContext() != null) {
            LoaderManager.getInstance(this).dump(str, fileDescriptor, printWriter, array);
        }
        printWriter.print(str);
        final StringBuilder sb = new StringBuilder();
        sb.append("Child ");
        sb.append(this.mChildFragmentManager);
        sb.append(":");
        printWriter.println(sb.toString());
        final FragmentManager mChildFragmentManager = this.mChildFragmentManager;
        final StringBuilder sb2 = new StringBuilder();
        sb2.append(str);
        sb2.append("  ");
        mChildFragmentManager.dump(sb2.toString(), fileDescriptor, printWriter, array);
    }
    
    @Override
    public final boolean equals(final Object obj) {
        return super.equals(obj);
    }
    
    Fragment findFragmentByWho(final String s) {
        if (s.equals(this.mWho)) {
            return this;
        }
        return this.mChildFragmentManager.findFragmentByWho(s);
    }
    
    String generateActivityResultKey() {
        final StringBuilder sb = new StringBuilder();
        sb.append("fragment_");
        sb.append(this.mWho);
        sb.append("_rq#");
        sb.append(this.mNextLocalRequestCode.getAndIncrement());
        return sb.toString();
    }
    
    public final FragmentActivity getActivity() {
        final FragmentHostCallback<?> mHost = this.mHost;
        FragmentActivity fragmentActivity;
        if (mHost == null) {
            fragmentActivity = null;
        }
        else {
            fragmentActivity = (FragmentActivity)mHost.getActivity();
        }
        return fragmentActivity;
    }
    
    public boolean getAllowEnterTransitionOverlap() {
        final AnimationInfo mAnimationInfo = this.mAnimationInfo;
        return mAnimationInfo == null || mAnimationInfo.mAllowEnterTransitionOverlap == null || this.mAnimationInfo.mAllowEnterTransitionOverlap;
    }
    
    public boolean getAllowReturnTransitionOverlap() {
        final AnimationInfo mAnimationInfo = this.mAnimationInfo;
        return mAnimationInfo == null || mAnimationInfo.mAllowReturnTransitionOverlap == null || this.mAnimationInfo.mAllowReturnTransitionOverlap;
    }
    
    View getAnimatingAway() {
        final AnimationInfo mAnimationInfo = this.mAnimationInfo;
        if (mAnimationInfo == null) {
            return null;
        }
        return mAnimationInfo.mAnimatingAway;
    }
    
    public final Bundle getArguments() {
        return this.mArguments;
    }
    
    public final FragmentManager getChildFragmentManager() {
        if (this.mHost != null) {
            return this.mChildFragmentManager;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Fragment ");
        sb.append(this);
        sb.append(" has not been attached yet.");
        throw new IllegalStateException(sb.toString());
    }
    
    public Context getContext() {
        final FragmentHostCallback<?> mHost = this.mHost;
        Context context;
        if (mHost == null) {
            context = null;
        }
        else {
            context = mHost.getContext();
        }
        return context;
    }
    
    public CreationExtras getDefaultViewModelCreationExtras() {
        while (true) {
            for (Context context = this.requireContext().getApplicationContext(); context instanceof ContextWrapper; context = ((ContextWrapper)context).getBaseContext()) {
                if (context instanceof Application) {
                    final Application application = (Application)context;
                    if (application == null && FragmentManager.isLoggingEnabled(3)) {
                        final StringBuilder sb = new StringBuilder();
                        sb.append("Could not find Application instance from Context ");
                        sb.append(this.requireContext().getApplicationContext());
                        sb.append(", you will not be able to use AndroidViewModel with the default ViewModelProvider.Factory");
                        Log.d("FragmentManager", sb.toString());
                    }
                    final MutableCreationExtras mutableCreationExtras = new MutableCreationExtras();
                    if (application != null) {
                        mutableCreationExtras.set(ViewModelProvider.AndroidViewModelFactory.APPLICATION_KEY, application);
                    }
                    mutableCreationExtras.set(SavedStateHandleSupport.SAVED_STATE_REGISTRY_OWNER_KEY, this);
                    mutableCreationExtras.set(SavedStateHandleSupport.VIEW_MODEL_STORE_OWNER_KEY, this);
                    if (this.getArguments() != null) {
                        mutableCreationExtras.set(SavedStateHandleSupport.DEFAULT_ARGS_KEY, this.getArguments());
                    }
                    return mutableCreationExtras;
                }
            }
            final Application application = null;
            continue;
        }
    }
    
    public ViewModelProvider.Factory getDefaultViewModelProviderFactory() {
        if (this.mFragmentManager != null) {
            if (this.mDefaultFactory == null) {
                final Application application = null;
                Context context = this.requireContext().getApplicationContext();
                Application application2;
                while (true) {
                    application2 = application;
                    if (!(context instanceof ContextWrapper)) {
                        break;
                    }
                    if (context instanceof Application) {
                        application2 = (Application)context;
                        break;
                    }
                    context = ((ContextWrapper)context).getBaseContext();
                }
                if (application2 == null && FragmentManager.isLoggingEnabled(3)) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Could not find Application instance from Context ");
                    sb.append(this.requireContext().getApplicationContext());
                    sb.append(", you will need CreationExtras to use AndroidViewModel with the default ViewModelProvider.Factory");
                    Log.d("FragmentManager", sb.toString());
                }
                this.mDefaultFactory = new SavedStateViewModelFactory(application2, this, this.getArguments());
            }
            return this.mDefaultFactory;
        }
        throw new IllegalStateException("Can't access ViewModels from detached fragment");
    }
    
    int getEnterAnim() {
        final AnimationInfo mAnimationInfo = this.mAnimationInfo;
        if (mAnimationInfo == null) {
            return 0;
        }
        return mAnimationInfo.mEnterAnim;
    }
    
    public Object getEnterTransition() {
        final AnimationInfo mAnimationInfo = this.mAnimationInfo;
        if (mAnimationInfo == null) {
            return null;
        }
        return mAnimationInfo.mEnterTransition;
    }
    
    SharedElementCallback getEnterTransitionCallback() {
        final AnimationInfo mAnimationInfo = this.mAnimationInfo;
        if (mAnimationInfo == null) {
            return null;
        }
        return mAnimationInfo.mEnterTransitionCallback;
    }
    
    int getExitAnim() {
        final AnimationInfo mAnimationInfo = this.mAnimationInfo;
        if (mAnimationInfo == null) {
            return 0;
        }
        return mAnimationInfo.mExitAnim;
    }
    
    public Object getExitTransition() {
        final AnimationInfo mAnimationInfo = this.mAnimationInfo;
        if (mAnimationInfo == null) {
            return null;
        }
        return mAnimationInfo.mExitTransition;
    }
    
    SharedElementCallback getExitTransitionCallback() {
        final AnimationInfo mAnimationInfo = this.mAnimationInfo;
        if (mAnimationInfo == null) {
            return null;
        }
        return mAnimationInfo.mExitTransitionCallback;
    }
    
    View getFocusedView() {
        final AnimationInfo mAnimationInfo = this.mAnimationInfo;
        if (mAnimationInfo == null) {
            return null;
        }
        return mAnimationInfo.mFocusedView;
    }
    
    @Deprecated
    public final FragmentManager getFragmentManager() {
        return this.mFragmentManager;
    }
    
    public final Object getHost() {
        final FragmentHostCallback<?> mHost = this.mHost;
        Object onGetHost;
        if (mHost == null) {
            onGetHost = null;
        }
        else {
            onGetHost = mHost.onGetHost();
        }
        return onGetHost;
    }
    
    public final int getId() {
        return this.mFragmentId;
    }
    
    public final LayoutInflater getLayoutInflater() {
        LayoutInflater layoutInflater;
        if ((layoutInflater = this.mLayoutInflater) == null) {
            layoutInflater = this.performGetLayoutInflater(null);
        }
        return layoutInflater;
    }
    
    @Deprecated
    public LayoutInflater getLayoutInflater(final Bundle bundle) {
        final FragmentHostCallback<?> mHost = this.mHost;
        if (mHost != null) {
            final LayoutInflater onGetLayoutInflater = mHost.onGetLayoutInflater();
            LayoutInflaterCompat.setFactory2(onGetLayoutInflater, this.mChildFragmentManager.getLayoutInflaterFactory());
            return onGetLayoutInflater;
        }
        throw new IllegalStateException("onGetLayoutInflater() cannot be executed until the Fragment is attached to the FragmentManager.");
    }
    
    public Lifecycle getLifecycle() {
        return this.mLifecycleRegistry;
    }
    
    @Deprecated
    public LoaderManager getLoaderManager() {
        return LoaderManager.getInstance(this);
    }
    
    int getNextTransition() {
        final AnimationInfo mAnimationInfo = this.mAnimationInfo;
        if (mAnimationInfo == null) {
            return 0;
        }
        return mAnimationInfo.mNextTransition;
    }
    
    public final Fragment getParentFragment() {
        return this.mParentFragment;
    }
    
    public final FragmentManager getParentFragmentManager() {
        final FragmentManager mFragmentManager = this.mFragmentManager;
        if (mFragmentManager != null) {
            return mFragmentManager;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Fragment ");
        sb.append(this);
        sb.append(" not associated with a fragment manager.");
        throw new IllegalStateException(sb.toString());
    }
    
    boolean getPopDirection() {
        final AnimationInfo mAnimationInfo = this.mAnimationInfo;
        return mAnimationInfo != null && mAnimationInfo.mIsPop;
    }
    
    int getPopEnterAnim() {
        final AnimationInfo mAnimationInfo = this.mAnimationInfo;
        if (mAnimationInfo == null) {
            return 0;
        }
        return mAnimationInfo.mPopEnterAnim;
    }
    
    int getPopExitAnim() {
        final AnimationInfo mAnimationInfo = this.mAnimationInfo;
        if (mAnimationInfo == null) {
            return 0;
        }
        return mAnimationInfo.mPopExitAnim;
    }
    
    float getPostOnViewCreatedAlpha() {
        final AnimationInfo mAnimationInfo = this.mAnimationInfo;
        if (mAnimationInfo == null) {
            return 1.0f;
        }
        return mAnimationInfo.mPostOnViewCreatedAlpha;
    }
    
    public Object getReenterTransition() {
        final AnimationInfo mAnimationInfo = this.mAnimationInfo;
        if (mAnimationInfo == null) {
            return null;
        }
        Object o;
        if (mAnimationInfo.mReenterTransition == Fragment.USE_DEFAULT_TRANSITION) {
            o = this.getExitTransition();
        }
        else {
            o = this.mAnimationInfo.mReenterTransition;
        }
        return o;
    }
    
    public final Resources getResources() {
        return this.requireContext().getResources();
    }
    
    @Deprecated
    public final boolean getRetainInstance() {
        FragmentStrictMode.onGetRetainInstanceUsage(this);
        return this.mRetainInstance;
    }
    
    public Object getReturnTransition() {
        final AnimationInfo mAnimationInfo = this.mAnimationInfo;
        if (mAnimationInfo == null) {
            return null;
        }
        Object o;
        if (mAnimationInfo.mReturnTransition == Fragment.USE_DEFAULT_TRANSITION) {
            o = this.getEnterTransition();
        }
        else {
            o = this.mAnimationInfo.mReturnTransition;
        }
        return o;
    }
    
    public final SavedStateRegistry getSavedStateRegistry() {
        return this.mSavedStateRegistryController.getSavedStateRegistry();
    }
    
    public Object getSharedElementEnterTransition() {
        final AnimationInfo mAnimationInfo = this.mAnimationInfo;
        if (mAnimationInfo == null) {
            return null;
        }
        return mAnimationInfo.mSharedElementEnterTransition;
    }
    
    public Object getSharedElementReturnTransition() {
        final AnimationInfo mAnimationInfo = this.mAnimationInfo;
        if (mAnimationInfo == null) {
            return null;
        }
        Object o;
        if (mAnimationInfo.mSharedElementReturnTransition == Fragment.USE_DEFAULT_TRANSITION) {
            o = this.getSharedElementEnterTransition();
        }
        else {
            o = this.mAnimationInfo.mSharedElementReturnTransition;
        }
        return o;
    }
    
    ArrayList<String> getSharedElementSourceNames() {
        final AnimationInfo mAnimationInfo = this.mAnimationInfo;
        if (mAnimationInfo != null && mAnimationInfo.mSharedElementSourceNames != null) {
            return this.mAnimationInfo.mSharedElementSourceNames;
        }
        return new ArrayList<String>();
    }
    
    ArrayList<String> getSharedElementTargetNames() {
        final AnimationInfo mAnimationInfo = this.mAnimationInfo;
        if (mAnimationInfo != null && mAnimationInfo.mSharedElementTargetNames != null) {
            return this.mAnimationInfo.mSharedElementTargetNames;
        }
        return new ArrayList<String>();
    }
    
    public final String getString(final int n) {
        return this.getResources().getString(n);
    }
    
    public final String getString(final int n, final Object... array) {
        return this.getResources().getString(n, array);
    }
    
    public final String getTag() {
        return this.mTag;
    }
    
    @Deprecated
    public final Fragment getTargetFragment() {
        return this.getTargetFragment(true);
    }
    
    @Deprecated
    public final int getTargetRequestCode() {
        FragmentStrictMode.onGetTargetFragmentRequestCodeUsage(this);
        return this.mTargetRequestCode;
    }
    
    public final CharSequence getText(final int n) {
        return this.getResources().getText(n);
    }
    
    @Deprecated
    public boolean getUserVisibleHint() {
        return this.mUserVisibleHint;
    }
    
    public View getView() {
        return this.mView;
    }
    
    public LifecycleOwner getViewLifecycleOwner() {
        final FragmentViewLifecycleOwner mViewLifecycleOwner = this.mViewLifecycleOwner;
        if (mViewLifecycleOwner != null) {
            return mViewLifecycleOwner;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Can't access the Fragment View's LifecycleOwner for ");
        sb.append(this);
        sb.append(" when getView() is null i.e., before onCreateView() or after onDestroyView()");
        throw new IllegalStateException(sb.toString());
    }
    
    public LiveData<LifecycleOwner> getViewLifecycleOwnerLiveData() {
        return this.mViewLifecycleOwnerLiveData;
    }
    
    public ViewModelStore getViewModelStore() {
        if (this.mFragmentManager == null) {
            throw new IllegalStateException("Can't access ViewModels from detached fragment");
        }
        if (this.getMinimumMaxLifecycleState() != Lifecycle.State.INITIALIZED.ordinal()) {
            return this.mFragmentManager.getViewModelStore(this);
        }
        throw new IllegalStateException("Calling getViewModelStore() before a Fragment reaches onCreate() when using setMaxLifecycle(INITIALIZED) is not supported");
    }
    
    public final boolean hasOptionsMenu() {
        return this.mHasMenu;
    }
    
    @Override
    public final int hashCode() {
        return super.hashCode();
    }
    
    void initState() {
        this.initLifecycle();
        this.mPreviousWho = this.mWho;
        this.mWho = UUID.randomUUID().toString();
        this.mAdded = false;
        this.mRemoving = false;
        this.mFromLayout = false;
        this.mInLayout = false;
        this.mRestored = false;
        this.mBackStackNesting = 0;
        this.mFragmentManager = null;
        this.mChildFragmentManager = new FragmentManagerImpl();
        this.mHost = null;
        this.mFragmentId = 0;
        this.mContainerId = 0;
        this.mTag = null;
        this.mHidden = false;
        this.mDetached = false;
    }
    
    public final boolean isAdded() {
        return this.mHost != null && this.mAdded;
    }
    
    public final boolean isDetached() {
        return this.mDetached;
    }
    
    public final boolean isHidden() {
        if (!this.mHidden) {
            final FragmentManager mFragmentManager = this.mFragmentManager;
            if (mFragmentManager == null || !mFragmentManager.isParentHidden(this.mParentFragment)) {
                return false;
            }
        }
        return true;
    }
    
    final boolean isInBackStack() {
        return this.mBackStackNesting > 0;
    }
    
    public final boolean isInLayout() {
        return this.mInLayout;
    }
    
    public final boolean isMenuVisible() {
        if (this.mMenuVisible) {
            final FragmentManager mFragmentManager = this.mFragmentManager;
            if (mFragmentManager == null || mFragmentManager.isParentMenuVisible(this.mParentFragment)) {
                return true;
            }
        }
        return false;
    }
    
    boolean isPostponed() {
        final AnimationInfo mAnimationInfo = this.mAnimationInfo;
        return mAnimationInfo != null && mAnimationInfo.mEnterTransitionPostponed;
    }
    
    public final boolean isRemoving() {
        return this.mRemoving;
    }
    
    public final boolean isResumed() {
        return this.mState >= 7;
    }
    
    public final boolean isStateSaved() {
        final FragmentManager mFragmentManager = this.mFragmentManager;
        return mFragmentManager != null && mFragmentManager.isStateSaved();
    }
    
    public final boolean isVisible() {
        if (this.isAdded() && !this.isHidden()) {
            final View mView = this.mView;
            if (mView != null && mView.getWindowToken() != null && this.mView.getVisibility() == 0) {
                return true;
            }
        }
        return false;
    }
    
    void noteStateNotSaved() {
        this.mChildFragmentManager.noteStateNotSaved();
    }
    
    @Deprecated
    public void onActivityCreated(final Bundle bundle) {
        this.mCalled = true;
    }
    
    @Deprecated
    public void onActivityResult(final int i, final int j, final Intent obj) {
        if (FragmentManager.isLoggingEnabled(2)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Fragment ");
            sb.append(this);
            sb.append(" received the following in onActivityResult(): requestCode: ");
            sb.append(i);
            sb.append(" resultCode: ");
            sb.append(j);
            sb.append(" data: ");
            sb.append(obj);
            Log.v("FragmentManager", sb.toString());
        }
    }
    
    @Deprecated
    public void onAttach(final Activity activity) {
        this.mCalled = true;
    }
    
    public void onAttach(final Context context) {
        this.mCalled = true;
        final FragmentHostCallback<?> mHost = this.mHost;
        Activity activity;
        if (mHost == null) {
            activity = null;
        }
        else {
            activity = mHost.getActivity();
        }
        if (activity != null) {
            this.mCalled = false;
            this.onAttach(activity);
        }
    }
    
    @Deprecated
    public void onAttachFragment(final Fragment fragment) {
    }
    
    public void onConfigurationChanged(final Configuration configuration) {
        this.mCalled = true;
    }
    
    public boolean onContextItemSelected(final MenuItem menuItem) {
        return false;
    }
    
    public void onCreate(final Bundle bundle) {
        this.mCalled = true;
        this.restoreChildFragmentState();
        if (!this.mChildFragmentManager.isStateAtLeast(1)) {
            this.mChildFragmentManager.dispatchCreate();
        }
    }
    
    public Animation onCreateAnimation(final int n, final boolean b, final int n2) {
        return null;
    }
    
    public Animator onCreateAnimator(final int n, final boolean b, final int n2) {
        return null;
    }
    
    public void onCreateContextMenu(final ContextMenu contextMenu, final View view, final ContextMenu$ContextMenuInfo contextMenu$ContextMenuInfo) {
        this.requireActivity().onCreateContextMenu(contextMenu, view, contextMenu$ContextMenuInfo);
    }
    
    @Deprecated
    public void onCreateOptionsMenu(final Menu menu, final MenuInflater menuInflater) {
    }
    
    public View onCreateView(final LayoutInflater layoutInflater, final ViewGroup viewGroup, final Bundle bundle) {
        final int mContentLayoutId = this.mContentLayoutId;
        if (mContentLayoutId != 0) {
            return layoutInflater.inflate(mContentLayoutId, viewGroup, false);
        }
        return null;
    }
    
    public void onDestroy() {
        this.mCalled = true;
    }
    
    @Deprecated
    public void onDestroyOptionsMenu() {
    }
    
    public void onDestroyView() {
        this.mCalled = true;
    }
    
    public void onDetach() {
        this.mCalled = true;
    }
    
    public LayoutInflater onGetLayoutInflater(final Bundle bundle) {
        return this.getLayoutInflater(bundle);
    }
    
    public void onHiddenChanged(final boolean b) {
    }
    
    @Deprecated
    public void onInflate(final Activity activity, final AttributeSet set, final Bundle bundle) {
        this.mCalled = true;
    }
    
    public void onInflate(final Context context, final AttributeSet set, final Bundle bundle) {
        this.mCalled = true;
        final FragmentHostCallback<?> mHost = this.mHost;
        Activity activity;
        if (mHost == null) {
            activity = null;
        }
        else {
            activity = mHost.getActivity();
        }
        if (activity != null) {
            this.mCalled = false;
            this.onInflate(activity, set, bundle);
        }
    }
    
    public void onLowMemory() {
        this.mCalled = true;
    }
    
    public void onMultiWindowModeChanged(final boolean b) {
    }
    
    @Deprecated
    public boolean onOptionsItemSelected(final MenuItem menuItem) {
        return false;
    }
    
    @Deprecated
    public void onOptionsMenuClosed(final Menu menu) {
    }
    
    public void onPause() {
        this.mCalled = true;
    }
    
    public void onPictureInPictureModeChanged(final boolean b) {
    }
    
    @Deprecated
    public void onPrepareOptionsMenu(final Menu menu) {
    }
    
    public void onPrimaryNavigationFragmentChanged(final boolean b) {
    }
    
    @Deprecated
    public void onRequestPermissionsResult(final int n, final String[] array, final int[] array2) {
    }
    
    public void onResume() {
        this.mCalled = true;
    }
    
    public void onSaveInstanceState(final Bundle bundle) {
    }
    
    public void onStart() {
        this.mCalled = true;
    }
    
    public void onStop() {
        this.mCalled = true;
    }
    
    public void onViewCreated(final View view, final Bundle bundle) {
    }
    
    public void onViewStateRestored(final Bundle bundle) {
        this.mCalled = true;
    }
    
    void performActivityCreated(final Bundle bundle) {
        this.mChildFragmentManager.noteStateNotSaved();
        this.mState = 3;
        this.mCalled = false;
        this.onActivityCreated(bundle);
        if (this.mCalled) {
            this.restoreViewState();
            this.mChildFragmentManager.dispatchActivityCreated();
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Fragment ");
        sb.append(this);
        sb.append(" did not call through to super.onActivityCreated()");
        throw new SuperNotCalledException(sb.toString());
    }
    
    void performAttach() {
        final Iterator<OnPreAttachedListener> iterator = this.mOnPreAttachedListeners.iterator();
        while (iterator.hasNext()) {
            iterator.next().onPreAttached();
        }
        this.mOnPreAttachedListeners.clear();
        this.mChildFragmentManager.attachController(this.mHost, this.createFragmentContainer(), this);
        this.mState = 0;
        this.mCalled = false;
        this.onAttach(this.mHost.getContext());
        if (this.mCalled) {
            this.mFragmentManager.dispatchOnAttachFragment(this);
            this.mChildFragmentManager.dispatchAttach();
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Fragment ");
        sb.append(this);
        sb.append(" did not call through to super.onAttach()");
        throw new SuperNotCalledException(sb.toString());
    }
    
    void performConfigurationChanged(final Configuration configuration) {
        this.onConfigurationChanged(configuration);
    }
    
    boolean performContextItemSelected(final MenuItem menuItem) {
        return !this.mHidden && (this.onContextItemSelected(menuItem) || this.mChildFragmentManager.dispatchContextItemSelected(menuItem));
    }
    
    void performCreate(final Bundle bundle) {
        this.mChildFragmentManager.noteStateNotSaved();
        this.mState = 1;
        this.mCalled = false;
        if (Build$VERSION.SDK_INT >= 19) {
            this.mLifecycleRegistry.addObserver(new LifecycleEventObserver(this) {
                final Fragment this$0;
                
                @Override
                public void onStateChanged(final LifecycleOwner lifecycleOwner, final Lifecycle.Event event) {
                    if (event == Lifecycle.Event.ON_STOP && this.this$0.mView != null) {
                        Api19Impl.cancelPendingInputEvents(this.this$0.mView);
                    }
                }
            });
        }
        this.onCreate(bundle);
        this.mIsCreated = true;
        if (this.mCalled) {
            this.mLifecycleRegistry.handleLifecycleEvent(Lifecycle.Event.ON_CREATE);
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Fragment ");
        sb.append(this);
        sb.append(" did not call through to super.onCreate()");
        throw new SuperNotCalledException(sb.toString());
    }
    
    boolean performCreateOptionsMenu(final Menu menu, final MenuInflater menuInflater) {
        final boolean mHidden = this.mHidden;
        boolean b = false;
        final boolean b2 = false;
        if (!mHidden) {
            boolean b3 = b2;
            if (this.mHasMenu) {
                b3 = b2;
                if (this.mMenuVisible) {
                    b3 = true;
                    this.onCreateOptionsMenu(menu, menuInflater);
                }
            }
            b = (b3 | this.mChildFragmentManager.dispatchCreateOptionsMenu(menu, menuInflater));
        }
        return b;
    }
    
    void performCreateView(final LayoutInflater layoutInflater, final ViewGroup viewGroup, final Bundle bundle) {
        this.mChildFragmentManager.noteStateNotSaved();
        this.mPerformedCreateView = true;
        this.mViewLifecycleOwner = new FragmentViewLifecycleOwner(this, this.getViewModelStore(), new Fragment$$ExternalSyntheticLambda0(this));
        final View onCreateView = this.onCreateView(layoutInflater, viewGroup, bundle);
        this.mView = onCreateView;
        if (onCreateView != null) {
            this.mViewLifecycleOwner.initialize();
            if (FragmentManager.isLoggingEnabled(3)) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Setting ViewLifecycleOwner on View ");
                sb.append(this.mView);
                sb.append(" for Fragment ");
                sb.append(this);
                Log.d("FragmentManager", sb.toString());
            }
            ViewTreeLifecycleOwner.set(this.mView, this.mViewLifecycleOwner);
            ViewTreeViewModelStoreOwner.set(this.mView, this.mViewLifecycleOwner);
            ViewTreeSavedStateRegistryOwner.set(this.mView, this.mViewLifecycleOwner);
            this.mViewLifecycleOwnerLiveData.setValue(this.mViewLifecycleOwner);
        }
        else {
            if (this.mViewLifecycleOwner.isInitialized()) {
                throw new IllegalStateException("Called getViewLifecycleOwner() but onCreateView() returned null");
            }
            this.mViewLifecycleOwner = null;
        }
    }
    
    void performDestroy() {
        this.mChildFragmentManager.dispatchDestroy();
        this.mLifecycleRegistry.handleLifecycleEvent(Lifecycle.Event.ON_DESTROY);
        this.mState = 0;
        this.mCalled = false;
        this.mIsCreated = false;
        this.onDestroy();
        if (this.mCalled) {
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Fragment ");
        sb.append(this);
        sb.append(" did not call through to super.onDestroy()");
        throw new SuperNotCalledException(sb.toString());
    }
    
    void performDestroyView() {
        this.mChildFragmentManager.dispatchDestroyView();
        if (this.mView != null && this.mViewLifecycleOwner.getLifecycle().getCurrentState().isAtLeast(Lifecycle.State.CREATED)) {
            this.mViewLifecycleOwner.handleLifecycleEvent(Lifecycle.Event.ON_DESTROY);
        }
        this.mState = 1;
        this.mCalled = false;
        this.onDestroyView();
        if (this.mCalled) {
            LoaderManager.getInstance(this).markForRedelivery();
            this.mPerformedCreateView = false;
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Fragment ");
        sb.append(this);
        sb.append(" did not call through to super.onDestroyView()");
        throw new SuperNotCalledException(sb.toString());
    }
    
    void performDetach() {
        this.mState = -1;
        this.mCalled = false;
        this.onDetach();
        this.mLayoutInflater = null;
        if (this.mCalled) {
            if (!this.mChildFragmentManager.isDestroyed()) {
                this.mChildFragmentManager.dispatchDestroy();
                this.mChildFragmentManager = new FragmentManagerImpl();
            }
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Fragment ");
        sb.append(this);
        sb.append(" did not call through to super.onDetach()");
        throw new SuperNotCalledException(sb.toString());
    }
    
    LayoutInflater performGetLayoutInflater(final Bundle bundle) {
        return this.mLayoutInflater = this.onGetLayoutInflater(bundle);
    }
    
    void performLowMemory() {
        this.onLowMemory();
    }
    
    void performMultiWindowModeChanged(final boolean b) {
        this.onMultiWindowModeChanged(b);
    }
    
    boolean performOptionsItemSelected(final MenuItem menuItem) {
        return !this.mHidden && ((this.mHasMenu && this.mMenuVisible && this.onOptionsItemSelected(menuItem)) || this.mChildFragmentManager.dispatchOptionsItemSelected(menuItem));
    }
    
    void performOptionsMenuClosed(final Menu menu) {
        if (!this.mHidden) {
            if (this.mHasMenu && this.mMenuVisible) {
                this.onOptionsMenuClosed(menu);
            }
            this.mChildFragmentManager.dispatchOptionsMenuClosed(menu);
        }
    }
    
    void performPause() {
        this.mChildFragmentManager.dispatchPause();
        if (this.mView != null) {
            this.mViewLifecycleOwner.handleLifecycleEvent(Lifecycle.Event.ON_PAUSE);
        }
        this.mLifecycleRegistry.handleLifecycleEvent(Lifecycle.Event.ON_PAUSE);
        this.mState = 6;
        this.mCalled = false;
        this.onPause();
        if (this.mCalled) {
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Fragment ");
        sb.append(this);
        sb.append(" did not call through to super.onPause()");
        throw new SuperNotCalledException(sb.toString());
    }
    
    void performPictureInPictureModeChanged(final boolean b) {
        this.onPictureInPictureModeChanged(b);
    }
    
    boolean performPrepareOptionsMenu(final Menu menu) {
        final boolean mHidden = this.mHidden;
        boolean b = false;
        final boolean b2 = false;
        if (!mHidden) {
            boolean b3 = b2;
            if (this.mHasMenu) {
                b3 = b2;
                if (this.mMenuVisible) {
                    b3 = true;
                    this.onPrepareOptionsMenu(menu);
                }
            }
            b = (b3 | this.mChildFragmentManager.dispatchPrepareOptionsMenu(menu));
        }
        return b;
    }
    
    void performPrimaryNavigationFragmentChanged() {
        final boolean primaryNavigation = this.mFragmentManager.isPrimaryNavigation(this);
        final Boolean mIsPrimaryNavigationFragment = this.mIsPrimaryNavigationFragment;
        if (mIsPrimaryNavigationFragment == null || mIsPrimaryNavigationFragment != primaryNavigation) {
            this.mIsPrimaryNavigationFragment = primaryNavigation;
            this.onPrimaryNavigationFragmentChanged(primaryNavigation);
            this.mChildFragmentManager.dispatchPrimaryNavigationFragmentChanged();
        }
    }
    
    void performResume() {
        this.mChildFragmentManager.noteStateNotSaved();
        this.mChildFragmentManager.execPendingActions(true);
        this.mState = 7;
        this.mCalled = false;
        this.onResume();
        if (this.mCalled) {
            this.mLifecycleRegistry.handleLifecycleEvent(Lifecycle.Event.ON_RESUME);
            if (this.mView != null) {
                this.mViewLifecycleOwner.handleLifecycleEvent(Lifecycle.Event.ON_RESUME);
            }
            this.mChildFragmentManager.dispatchResume();
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Fragment ");
        sb.append(this);
        sb.append(" did not call through to super.onResume()");
        throw new SuperNotCalledException(sb.toString());
    }
    
    void performSaveInstanceState(final Bundle bundle) {
        this.onSaveInstanceState(bundle);
    }
    
    void performStart() {
        this.mChildFragmentManager.noteStateNotSaved();
        this.mChildFragmentManager.execPendingActions(true);
        this.mState = 5;
        this.mCalled = false;
        this.onStart();
        if (this.mCalled) {
            this.mLifecycleRegistry.handleLifecycleEvent(Lifecycle.Event.ON_START);
            if (this.mView != null) {
                this.mViewLifecycleOwner.handleLifecycleEvent(Lifecycle.Event.ON_START);
            }
            this.mChildFragmentManager.dispatchStart();
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Fragment ");
        sb.append(this);
        sb.append(" did not call through to super.onStart()");
        throw new SuperNotCalledException(sb.toString());
    }
    
    void performStop() {
        this.mChildFragmentManager.dispatchStop();
        if (this.mView != null) {
            this.mViewLifecycleOwner.handleLifecycleEvent(Lifecycle.Event.ON_STOP);
        }
        this.mLifecycleRegistry.handleLifecycleEvent(Lifecycle.Event.ON_STOP);
        this.mState = 4;
        this.mCalled = false;
        this.onStop();
        if (this.mCalled) {
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Fragment ");
        sb.append(this);
        sb.append(" did not call through to super.onStop()");
        throw new SuperNotCalledException(sb.toString());
    }
    
    void performViewCreated() {
        final Bundle mSavedFragmentState = this.mSavedFragmentState;
        Bundle bundle;
        if (mSavedFragmentState != null) {
            bundle = mSavedFragmentState.getBundle("savedInstanceState");
        }
        else {
            bundle = null;
        }
        this.onViewCreated(this.mView, bundle);
        this.mChildFragmentManager.dispatchViewCreated();
    }
    
    public void postponeEnterTransition() {
        this.ensureAnimationInfo().mEnterTransitionPostponed = true;
    }
    
    public final void postponeEnterTransition(final long duration, final TimeUnit timeUnit) {
        this.ensureAnimationInfo().mEnterTransitionPostponed = true;
        final Handler mPostponedHandler = this.mPostponedHandler;
        if (mPostponedHandler != null) {
            mPostponedHandler.removeCallbacks(this.mPostponedDurationRunnable);
        }
        final FragmentManager mFragmentManager = this.mFragmentManager;
        if (mFragmentManager != null) {
            this.mPostponedHandler = mFragmentManager.getHost().getHandler();
        }
        else {
            this.mPostponedHandler = new Handler(Looper.getMainLooper());
        }
        this.mPostponedHandler.removeCallbacks(this.mPostponedDurationRunnable);
        this.mPostponedHandler.postDelayed(this.mPostponedDurationRunnable, timeUnit.toMillis(duration));
    }
    
    public final <I, O> ActivityResultLauncher<I> registerForActivityResult(final ActivityResultContract<I, O> activityResultContract, final ActivityResultCallback<O> activityResultCallback) {
        return this.prepareCallInternal(activityResultContract, new Function<Void, ActivityResultRegistry>(this) {
            final Fragment this$0;
            
            @Override
            public ActivityResultRegistry apply(final Void void1) {
                if (this.this$0.mHost instanceof ActivityResultRegistryOwner) {
                    return ((ActivityResultRegistryOwner)this.this$0.mHost).getActivityResultRegistry();
                }
                return this.this$0.requireActivity().getActivityResultRegistry();
            }
        }, activityResultCallback);
    }
    
    public final <I, O> ActivityResultLauncher<I> registerForActivityResult(final ActivityResultContract<I, O> activityResultContract, final ActivityResultRegistry activityResultRegistry, final ActivityResultCallback<O> activityResultCallback) {
        return this.prepareCallInternal(activityResultContract, new Function<Void, ActivityResultRegistry>(this, activityResultRegistry) {
            final Fragment this$0;
            final ActivityResultRegistry val$registry;
            
            @Override
            public ActivityResultRegistry apply(final Void void1) {
                return this.val$registry;
            }
        }, activityResultCallback);
    }
    
    public void registerForContextMenu(final View view) {
        view.setOnCreateContextMenuListener((View$OnCreateContextMenuListener)this);
    }
    
    @Deprecated
    public final void requestPermissions(final String[] array, final int n) {
        if (this.mHost != null) {
            this.getParentFragmentManager().launchRequestPermissions(this, array, n);
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Fragment ");
        sb.append(this);
        sb.append(" not attached to Activity");
        throw new IllegalStateException(sb.toString());
    }
    
    public final FragmentActivity requireActivity() {
        final FragmentActivity activity = this.getActivity();
        if (activity != null) {
            return activity;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Fragment ");
        sb.append(this);
        sb.append(" not attached to an activity.");
        throw new IllegalStateException(sb.toString());
    }
    
    public final Bundle requireArguments() {
        final Bundle arguments = this.getArguments();
        if (arguments != null) {
            return arguments;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Fragment ");
        sb.append(this);
        sb.append(" does not have any arguments.");
        throw new IllegalStateException(sb.toString());
    }
    
    public final Context requireContext() {
        final Context context = this.getContext();
        if (context != null) {
            return context;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Fragment ");
        sb.append(this);
        sb.append(" not attached to a context.");
        throw new IllegalStateException(sb.toString());
    }
    
    @Deprecated
    public final FragmentManager requireFragmentManager() {
        return this.getParentFragmentManager();
    }
    
    public final Object requireHost() {
        final Object host = this.getHost();
        if (host != null) {
            return host;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Fragment ");
        sb.append(this);
        sb.append(" not attached to a host.");
        throw new IllegalStateException(sb.toString());
    }
    
    public final Fragment requireParentFragment() {
        final Fragment parentFragment = this.getParentFragment();
        if (parentFragment != null) {
            return parentFragment;
        }
        if (this.getContext() == null) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Fragment ");
            sb.append(this);
            sb.append(" is not attached to any Fragment or host");
            throw new IllegalStateException(sb.toString());
        }
        final StringBuilder sb2 = new StringBuilder();
        sb2.append("Fragment ");
        sb2.append(this);
        sb2.append(" is not a child Fragment, it is directly attached to ");
        sb2.append(this.getContext());
        throw new IllegalStateException(sb2.toString());
    }
    
    public final View requireView() {
        final View view = this.getView();
        if (view != null) {
            return view;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Fragment ");
        sb.append(this);
        sb.append(" did not return a View from onCreateView() or this was called before onCreateView().");
        throw new IllegalStateException(sb.toString());
    }
    
    void restoreChildFragmentState() {
        final Bundle mSavedFragmentState = this.mSavedFragmentState;
        if (mSavedFragmentState != null) {
            final Bundle bundle = mSavedFragmentState.getBundle("childFragmentManager");
            if (bundle != null) {
                this.mChildFragmentManager.restoreSaveStateInternal((Parcelable)bundle);
                this.mChildFragmentManager.dispatchCreate();
            }
        }
    }
    
    final void restoreViewState(final Bundle bundle) {
        final SparseArray<Parcelable> mSavedViewState = this.mSavedViewState;
        if (mSavedViewState != null) {
            this.mView.restoreHierarchyState((SparseArray)mSavedViewState);
            this.mSavedViewState = null;
        }
        this.mCalled = false;
        this.onViewStateRestored(bundle);
        if (this.mCalled) {
            if (this.mView != null) {
                this.mViewLifecycleOwner.handleLifecycleEvent(Lifecycle.Event.ON_CREATE);
            }
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Fragment ");
        sb.append(this);
        sb.append(" did not call through to super.onViewStateRestored()");
        throw new SuperNotCalledException(sb.toString());
    }
    
    public void setAllowEnterTransitionOverlap(final boolean b) {
        this.ensureAnimationInfo().mAllowEnterTransitionOverlap = b;
    }
    
    public void setAllowReturnTransitionOverlap(final boolean b) {
        this.ensureAnimationInfo().mAllowReturnTransitionOverlap = b;
    }
    
    void setAnimations(final int mEnterAnim, final int mExitAnim, final int mPopEnterAnim, final int mPopExitAnim) {
        if (this.mAnimationInfo == null && mEnterAnim == 0 && mExitAnim == 0 && mPopEnterAnim == 0 && mPopExitAnim == 0) {
            return;
        }
        this.ensureAnimationInfo().mEnterAnim = mEnterAnim;
        this.ensureAnimationInfo().mExitAnim = mExitAnim;
        this.ensureAnimationInfo().mPopEnterAnim = mPopEnterAnim;
        this.ensureAnimationInfo().mPopExitAnim = mPopExitAnim;
    }
    
    public void setArguments(final Bundle mArguments) {
        if (this.mFragmentManager != null && this.isStateSaved()) {
            throw new IllegalStateException("Fragment already added and state has been saved");
        }
        this.mArguments = mArguments;
    }
    
    public void setEnterSharedElementCallback(final SharedElementCallback mEnterTransitionCallback) {
        this.ensureAnimationInfo().mEnterTransitionCallback = mEnterTransitionCallback;
    }
    
    public void setEnterTransition(final Object mEnterTransition) {
        this.ensureAnimationInfo().mEnterTransition = mEnterTransition;
    }
    
    public void setExitSharedElementCallback(final SharedElementCallback mExitTransitionCallback) {
        this.ensureAnimationInfo().mExitTransitionCallback = mExitTransitionCallback;
    }
    
    public void setExitTransition(final Object mExitTransition) {
        this.ensureAnimationInfo().mExitTransition = mExitTransition;
    }
    
    void setFocusedView(final View mFocusedView) {
        this.ensureAnimationInfo().mFocusedView = mFocusedView;
    }
    
    @Deprecated
    public void setHasOptionsMenu(final boolean mHasMenu) {
        if (this.mHasMenu != mHasMenu) {
            this.mHasMenu = mHasMenu;
            if (this.isAdded() && !this.isHidden()) {
                this.mHost.onSupportInvalidateOptionsMenu();
            }
        }
    }
    
    public void setInitialSavedState(final SavedState savedState) {
        if (this.mFragmentManager == null) {
            Bundle mState;
            if (savedState != null && savedState.mState != null) {
                mState = savedState.mState;
            }
            else {
                mState = null;
            }
            this.mSavedFragmentState = mState;
            return;
        }
        throw new IllegalStateException("Fragment already added");
    }
    
    public void setMenuVisibility(final boolean mMenuVisible) {
        if (this.mMenuVisible != mMenuVisible) {
            this.mMenuVisible = mMenuVisible;
            if (this.mHasMenu && this.isAdded() && !this.isHidden()) {
                this.mHost.onSupportInvalidateOptionsMenu();
            }
        }
    }
    
    void setNextTransition(final int mNextTransition) {
        if (this.mAnimationInfo == null && mNextTransition == 0) {
            return;
        }
        this.ensureAnimationInfo();
        this.mAnimationInfo.mNextTransition = mNextTransition;
    }
    
    void setPopDirection(final boolean mIsPop) {
        if (this.mAnimationInfo == null) {
            return;
        }
        this.ensureAnimationInfo().mIsPop = mIsPop;
    }
    
    void setPostOnViewCreatedAlpha(final float mPostOnViewCreatedAlpha) {
        this.ensureAnimationInfo().mPostOnViewCreatedAlpha = mPostOnViewCreatedAlpha;
    }
    
    public void setReenterTransition(final Object mReenterTransition) {
        this.ensureAnimationInfo().mReenterTransition = mReenterTransition;
    }
    
    @Deprecated
    public void setRetainInstance(final boolean mRetainInstance) {
        FragmentStrictMode.onSetRetainInstanceUsage(this);
        this.mRetainInstance = mRetainInstance;
        final FragmentManager mFragmentManager = this.mFragmentManager;
        if (mFragmentManager != null) {
            if (mRetainInstance) {
                mFragmentManager.addRetainedFragment(this);
            }
            else {
                mFragmentManager.removeRetainedFragment(this);
            }
        }
        else {
            this.mRetainInstanceChangedWhileDetached = true;
        }
    }
    
    public void setReturnTransition(final Object mReturnTransition) {
        this.ensureAnimationInfo().mReturnTransition = mReturnTransition;
    }
    
    public void setSharedElementEnterTransition(final Object mSharedElementEnterTransition) {
        this.ensureAnimationInfo().mSharedElementEnterTransition = mSharedElementEnterTransition;
    }
    
    void setSharedElementNames(final ArrayList<String> mSharedElementSourceNames, final ArrayList<String> mSharedElementTargetNames) {
        this.ensureAnimationInfo();
        this.mAnimationInfo.mSharedElementSourceNames = mSharedElementSourceNames;
        this.mAnimationInfo.mSharedElementTargetNames = mSharedElementTargetNames;
    }
    
    public void setSharedElementReturnTransition(final Object mSharedElementReturnTransition) {
        this.ensureAnimationInfo().mSharedElementReturnTransition = mSharedElementReturnTransition;
    }
    
    @Deprecated
    public void setTargetFragment(final Fragment mTarget, final int mTargetRequestCode) {
        if (mTarget != null) {
            FragmentStrictMode.onSetTargetFragmentUsage(this, mTarget, mTargetRequestCode);
        }
        final FragmentManager mFragmentManager = this.mFragmentManager;
        FragmentManager mFragmentManager2;
        if (mTarget != null) {
            mFragmentManager2 = mTarget.mFragmentManager;
        }
        else {
            mFragmentManager2 = null;
        }
        if (mFragmentManager != null && mFragmentManager2 != null && mFragmentManager != mFragmentManager2) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Fragment ");
            sb.append(mTarget);
            sb.append(" must share the same FragmentManager to be set as a target fragment");
            throw new IllegalArgumentException(sb.toString());
        }
        for (Fragment targetFragment = mTarget; targetFragment != null; targetFragment = targetFragment.getTargetFragment(false)) {
            if (targetFragment.equals(this)) {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("Setting ");
                sb2.append(mTarget);
                sb2.append(" as the target of ");
                sb2.append(this);
                sb2.append(" would create a target cycle");
                throw new IllegalArgumentException(sb2.toString());
            }
        }
        if (mTarget == null) {
            this.mTargetWho = null;
            this.mTarget = null;
        }
        else if (this.mFragmentManager != null && mTarget.mFragmentManager != null) {
            this.mTargetWho = mTarget.mWho;
            this.mTarget = null;
        }
        else {
            this.mTargetWho = null;
            this.mTarget = mTarget;
        }
        this.mTargetRequestCode = mTargetRequestCode;
    }
    
    @Deprecated
    public void setUserVisibleHint(final boolean b) {
        FragmentStrictMode.onSetUserVisibleHint(this, b);
        if (!this.mUserVisibleHint && b && this.mState < 5 && this.mFragmentManager != null && this.isAdded() && this.mIsCreated) {
            final FragmentManager mFragmentManager = this.mFragmentManager;
            mFragmentManager.performPendingDeferredStart(mFragmentManager.createOrGetFragmentStateManager(this));
        }
        this.mUserVisibleHint = b;
        this.mDeferStart = (this.mState < 5 && !b);
        if (this.mSavedFragmentState != null) {
            this.mSavedUserVisibleHint = b;
        }
    }
    
    public boolean shouldShowRequestPermissionRationale(final String s) {
        final FragmentHostCallback<?> mHost = this.mHost;
        return mHost != null && mHost.onShouldShowRequestPermissionRationale(s);
    }
    
    public void startActivity(final Intent intent) {
        this.startActivity(intent, null);
    }
    
    public void startActivity(final Intent intent, final Bundle bundle) {
        final FragmentHostCallback<?> mHost = this.mHost;
        if (mHost != null) {
            mHost.onStartActivityFromFragment(this, intent, -1, bundle);
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Fragment ");
        sb.append(this);
        sb.append(" not attached to Activity");
        throw new IllegalStateException(sb.toString());
    }
    
    @Deprecated
    public void startActivityForResult(final Intent intent, final int n) {
        this.startActivityForResult(intent, n, null);
    }
    
    @Deprecated
    public void startActivityForResult(final Intent intent, final int n, final Bundle bundle) {
        if (this.mHost != null) {
            this.getParentFragmentManager().launchStartActivityForResult(this, intent, n, bundle);
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Fragment ");
        sb.append(this);
        sb.append(" not attached to Activity");
        throw new IllegalStateException(sb.toString());
    }
    
    @Deprecated
    public void startIntentSenderForResult(final IntentSender obj, final int i, final Intent obj2, final int n, final int n2, final int n3, final Bundle obj3) throws IntentSender$SendIntentException {
        if (this.mHost != null) {
            if (FragmentManager.isLoggingEnabled(2)) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Fragment ");
                sb.append(this);
                sb.append(" received the following in startIntentSenderForResult() requestCode: ");
                sb.append(i);
                sb.append(" IntentSender: ");
                sb.append(obj);
                sb.append(" fillInIntent: ");
                sb.append(obj2);
                sb.append(" options: ");
                sb.append(obj3);
                Log.v("FragmentManager", sb.toString());
            }
            this.getParentFragmentManager().launchStartIntentSenderForResult(this, obj, i, obj2, n, n2, n3, obj3);
            return;
        }
        final StringBuilder sb2 = new StringBuilder();
        sb2.append("Fragment ");
        sb2.append(this);
        sb2.append(" not attached to Activity");
        throw new IllegalStateException(sb2.toString());
    }
    
    public void startPostponedEnterTransition() {
        if (this.mAnimationInfo != null) {
            if (this.ensureAnimationInfo().mEnterTransitionPostponed) {
                if (this.mHost == null) {
                    this.ensureAnimationInfo().mEnterTransitionPostponed = false;
                }
                else if (Looper.myLooper() != this.mHost.getHandler().getLooper()) {
                    this.mHost.getHandler().postAtFrontOfQueue((Runnable)new Runnable(this) {
                        final Fragment this$0;
                        
                        @Override
                        public void run() {
                            this.this$0.callStartTransitionListener(false);
                        }
                    });
                }
                else {
                    this.callStartTransitionListener(true);
                }
            }
        }
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder(128);
        sb.append(this.getClass().getSimpleName());
        sb.append("{");
        sb.append(Integer.toHexString(System.identityHashCode(this)));
        sb.append("}");
        sb.append(" (");
        sb.append(this.mWho);
        if (this.mFragmentId != 0) {
            sb.append(" id=0x");
            sb.append(Integer.toHexString(this.mFragmentId));
        }
        if (this.mTag != null) {
            sb.append(" tag=");
            sb.append(this.mTag);
        }
        sb.append(")");
        return sb.toString();
    }
    
    public void unregisterForContextMenu(final View view) {
        view.setOnCreateContextMenuListener((View$OnCreateContextMenuListener)null);
    }
    
    static class AnimationInfo
    {
        Boolean mAllowEnterTransitionOverlap;
        Boolean mAllowReturnTransitionOverlap;
        View mAnimatingAway;
        int mEnterAnim;
        Object mEnterTransition;
        SharedElementCallback mEnterTransitionCallback;
        boolean mEnterTransitionPostponed;
        int mExitAnim;
        Object mExitTransition;
        SharedElementCallback mExitTransitionCallback;
        View mFocusedView;
        boolean mIsPop;
        int mNextTransition;
        int mPopEnterAnim;
        int mPopExitAnim;
        float mPostOnViewCreatedAlpha;
        Object mReenterTransition;
        Object mReturnTransition;
        Object mSharedElementEnterTransition;
        Object mSharedElementReturnTransition;
        ArrayList<String> mSharedElementSourceNames;
        ArrayList<String> mSharedElementTargetNames;
        
        AnimationInfo() {
            this.mEnterTransition = null;
            this.mReturnTransition = Fragment.USE_DEFAULT_TRANSITION;
            this.mExitTransition = null;
            this.mReenterTransition = Fragment.USE_DEFAULT_TRANSITION;
            this.mSharedElementEnterTransition = null;
            this.mSharedElementReturnTransition = Fragment.USE_DEFAULT_TRANSITION;
            this.mEnterTransitionCallback = null;
            this.mExitTransitionCallback = null;
            this.mPostOnViewCreatedAlpha = 1.0f;
            this.mFocusedView = null;
        }
    }
    
    static class Api19Impl
    {
        private Api19Impl() {
        }
        
        static void cancelPendingInputEvents(final View view) {
            view.cancelPendingInputEvents();
        }
    }
    
    public static class InstantiationException extends RuntimeException
    {
        public InstantiationException(final String message, final Exception cause) {
            super(message, cause);
        }
    }
    
    private abstract static class OnPreAttachedListener
    {
        abstract void onPreAttached();
    }
    
    public static class SavedState implements Parcelable
    {
        public static final Parcelable$Creator<SavedState> CREATOR;
        final Bundle mState;
        
        static {
            CREATOR = (Parcelable$Creator)new Parcelable$ClassLoaderCreator<SavedState>() {
                public SavedState createFromParcel(final Parcel parcel) {
                    return new SavedState(parcel, null);
                }
                
                public SavedState createFromParcel(final Parcel parcel, final ClassLoader classLoader) {
                    return new SavedState(parcel, classLoader);
                }
                
                public SavedState[] newArray(final int n) {
                    return new SavedState[n];
                }
            };
        }
        
        SavedState(final Bundle mState) {
            this.mState = mState;
        }
        
        SavedState(final Parcel parcel, final ClassLoader classLoader) {
            final Bundle bundle = parcel.readBundle();
            this.mState = bundle;
            if (classLoader != null && bundle != null) {
                bundle.setClassLoader(classLoader);
            }
        }
        
        public int describeContents() {
            return 0;
        }
        
        public void writeToParcel(final Parcel parcel, final int n) {
            parcel.writeBundle(this.mState);
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.fragment.app;

import android.os.Bundle;

public interface FragmentResultListener
{
    void onFragmentResult(final String p0, final Bundle p1);
}

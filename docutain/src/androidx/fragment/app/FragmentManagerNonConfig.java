// 
// Decompiled by Procyon v0.6.0
// 

package androidx.fragment.app;

import androidx.lifecycle.ViewModelStore;
import java.util.Collection;
import java.util.Map;

@Deprecated
public class FragmentManagerNonConfig
{
    private final Map<String, FragmentManagerNonConfig> mChildNonConfigs;
    private final Collection<Fragment> mFragments;
    private final Map<String, ViewModelStore> mViewModelStores;
    
    FragmentManagerNonConfig(final Collection<Fragment> mFragments, final Map<String, FragmentManagerNonConfig> mChildNonConfigs, final Map<String, ViewModelStore> mViewModelStores) {
        this.mFragments = mFragments;
        this.mChildNonConfigs = mChildNonConfigs;
        this.mViewModelStores = mViewModelStores;
    }
    
    Map<String, FragmentManagerNonConfig> getChildNonConfigs() {
        return this.mChildNonConfigs;
    }
    
    Collection<Fragment> getFragments() {
        return this.mFragments;
    }
    
    Map<String, ViewModelStore> getViewModelStores() {
        return this.mViewModelStores;
    }
    
    boolean isRetaining(final Fragment fragment) {
        final Collection<Fragment> mFragments = this.mFragments;
        return mFragments != null && mFragments.contains(fragment);
    }
}

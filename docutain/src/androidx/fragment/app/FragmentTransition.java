// 
// Decompiled by Procyon v0.6.0
// 

package androidx.fragment.app;

import kotlin.collections.CollectionsKt;
import java.util.LinkedHashMap;
import kotlin.jvm.JvmStatic;
import java.util.Iterator;
import androidx.core.app.SharedElementCallback;
import java.util.List;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;
import kotlin.jvm.internal.Intrinsics;
import android.view.View;
import androidx.collection.ArrayMap;
import android.os.Build$VERSION;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000F\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010 \n\u0000\n\u0002\u0010\b\n\u0002\b\u0006\b\u00c0\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J<\u0010\u0006\u001a\u00020\u00072\u0006\u0010\b\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\t2\u0006\u0010\u000b\u001a\u00020\f2\u0012\u0010\r\u001a\u000e\u0012\u0004\u0012\u00020\u000f\u0012\u0004\u0012\u00020\u00100\u000e2\u0006\u0010\u0011\u001a\u00020\fH\u0007J\n\u0010\u0012\u001a\u0004\u0018\u00010\u0004H\u0002J\u001e\u0010\u0013\u001a\u00020\u00072\f\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\u00100\u00152\u0006\u0010\u0016\u001a\u00020\u0017H\u0007J\b\u0010\u0018\u001a\u00020\fH\u0007J\"\u0010\u0019\u001a\u0004\u0018\u00010\u000f*\u000e\u0012\u0004\u0012\u00020\u000f\u0012\u0004\u0012\u00020\u000f0\u000e2\u0006\u0010\u001a\u001a\u00020\u000fH\u0007J,\u0010\u001b\u001a\u00020\u0007*\u000e\u0012\u0004\u0012\u00020\u000f\u0012\u0004\u0012\u00020\u000f0\u000e2\u0012\u0010\u001c\u001a\u000e\u0012\u0004\u0012\u00020\u000f\u0012\u0004\u0012\u00020\u00100\u000eH\u0007R\u0012\u0010\u0003\u001a\u0004\u0018\u00010\u00048\u0006X\u0087\u0004¢\u0006\u0002\n\u0000R\u0012\u0010\u0005\u001a\u0004\u0018\u00010\u00048\u0006X\u0087\u0004¢\u0006\u0002\n\u0000¨\u0006\u001d" }, d2 = { "Landroidx/fragment/app/FragmentTransition;", "", "()V", "PLATFORM_IMPL", "Landroidx/fragment/app/FragmentTransitionImpl;", "SUPPORT_IMPL", "callSharedElementStartEnd", "", "inFragment", "Landroidx/fragment/app/Fragment;", "outFragment", "isPop", "", "sharedElements", "Landroidx/collection/ArrayMap;", "", "Landroid/view/View;", "isStart", "resolveSupportImpl", "setViewVisibility", "views", "", "visibility", "", "supportsTransition", "findKeyForValue", "value", "retainValues", "namedViews", "fragment_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
public final class FragmentTransition
{
    public static final FragmentTransition INSTANCE;
    public static final FragmentTransitionImpl PLATFORM_IMPL;
    public static final FragmentTransitionImpl SUPPORT_IMPL;
    
    static {
        final FragmentTransition fragmentTransition = INSTANCE = new FragmentTransition();
        FragmentTransitionImpl platform_IMPL;
        if (Build$VERSION.SDK_INT >= 21) {
            platform_IMPL = new FragmentTransitionCompat21();
        }
        else {
            platform_IMPL = null;
        }
        PLATFORM_IMPL = platform_IMPL;
        SUPPORT_IMPL = fragmentTransition.resolveSupportImpl();
    }
    
    private FragmentTransition() {
    }
    
    @JvmStatic
    public static final void callSharedElementStartEnd(final Fragment fragment, final Fragment fragment2, final boolean b, final ArrayMap<String, View> arrayMap, final boolean b2) {
        Intrinsics.checkNotNullParameter((Object)fragment, "inFragment");
        Intrinsics.checkNotNullParameter((Object)fragment2, "outFragment");
        Intrinsics.checkNotNullParameter((Object)arrayMap, "sharedElements");
        SharedElementCallback sharedElementCallback;
        if (b) {
            sharedElementCallback = fragment2.getEnterTransitionCallback();
        }
        else {
            sharedElementCallback = fragment.getEnterTransitionCallback();
        }
        if (sharedElementCallback != null) {
            final Map map = arrayMap;
            final Collection collection = new ArrayList(map.size());
            final Iterator iterator = map.entrySet().iterator();
            while (iterator.hasNext()) {
                collection.add(((Map.Entry<K, View>)iterator.next()).getValue());
            }
            final List list = (List)collection;
            final Collection collection2 = new ArrayList(map.size());
            final Iterator iterator2 = map.entrySet().iterator();
            while (iterator2.hasNext()) {
                collection2.add(((Map.Entry<String, V>)iterator2.next()).getKey());
            }
            final List list2 = (List)collection2;
            if (b2) {
                sharedElementCallback.onSharedElementStart(list2, list, null);
            }
            else {
                sharedElementCallback.onSharedElementEnd(list2, list, null);
            }
        }
    }
    
    @JvmStatic
    public static final String findKeyForValue(final ArrayMap<String, String> arrayMap, final String s) {
        Intrinsics.checkNotNullParameter((Object)arrayMap, "<this>");
        Intrinsics.checkNotNullParameter((Object)s, "value");
        final Map map = arrayMap;
        final Map map2 = new LinkedHashMap();
        for (final Map.Entry<K, Object> entry : map.entrySet()) {
            if (Intrinsics.areEqual(entry.getValue(), (Object)s)) {
                map2.put(entry.getKey(), entry.getValue());
            }
        }
        final Collection collection = new ArrayList(map2.size());
        final Iterator iterator2 = map2.entrySet().iterator();
        while (iterator2.hasNext()) {
            collection.add(((Map.Entry<String, V>)iterator2.next()).getKey());
        }
        return (String)CollectionsKt.firstOrNull((List)collection);
    }
    
    private final FragmentTransitionImpl resolveSupportImpl() {
        FragmentTransitionImpl fragmentTransitionImpl;
        try {
            final Class<?> forName = Class.forName("androidx.transition.FragmentTransitionSupport");
            Intrinsics.checkNotNull((Object)forName, "null cannot be cast to non-null type java.lang.Class<androidx.fragment.app.FragmentTransitionImpl>");
            fragmentTransitionImpl = (FragmentTransitionImpl)forName.getDeclaredConstructor((Class<?>[])new Class[0]).newInstance(new Object[0]);
        }
        catch (final Exception ex) {
            fragmentTransitionImpl = null;
        }
        return fragmentTransitionImpl;
    }
    
    @JvmStatic
    public static final void retainValues(final ArrayMap<String, String> arrayMap, final ArrayMap<String, View> arrayMap2) {
        Intrinsics.checkNotNullParameter((Object)arrayMap, "<this>");
        Intrinsics.checkNotNullParameter((Object)arrayMap2, "namedViews");
        for (int n = arrayMap.size() - 1; -1 < n; --n) {
            if (!arrayMap2.containsKey(arrayMap.valueAt(n))) {
                arrayMap.removeAt(n);
            }
        }
    }
    
    @JvmStatic
    public static final void setViewVisibility(final List<? extends View> list, final int visibility) {
        Intrinsics.checkNotNullParameter((Object)list, "views");
        final Iterator iterator = list.iterator();
        while (iterator.hasNext()) {
            ((View)iterator.next()).setVisibility(visibility);
        }
    }
    
    @JvmStatic
    public static final boolean supportsTransition() {
        return FragmentTransition.PLATFORM_IMPL != null || FragmentTransition.SUPPORT_IMPL != null;
    }
}

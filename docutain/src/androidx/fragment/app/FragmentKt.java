// 
// Decompiled by Procyon v0.6.0
// 

package androidx.fragment.app;

import androidx.lifecycle.LifecycleOwner;
import kotlin.Unit;
import kotlin.jvm.internal.Intrinsics;
import android.os.Bundle;
import kotlin.jvm.functions.Function2;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000(\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u001a\u0012\u0010\u0000\u001a\u00020\u0001*\u00020\u00022\u0006\u0010\u0003\u001a\u00020\u0004\u001a\u0012\u0010\u0005\u001a\u00020\u0001*\u00020\u00022\u0006\u0010\u0003\u001a\u00020\u0004\u001a\u001a\u0010\u0006\u001a\u00020\u0001*\u00020\u00022\u0006\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0007\u001a\u00020\b\u001aJ\u0010\t\u001a\u00020\u0001*\u00020\u00022\u0006\u0010\u0003\u001a\u00020\u000426\u0010\n\u001a2\u0012\u0013\u0012\u00110\u0004¢\u0006\f\b\f\u0012\b\b\r\u0012\u0004\b\b(\u0003\u0012\u0013\u0012\u00110\b¢\u0006\f\b\f\u0012\b\b\r\u0012\u0004\b\b(\u000e\u0012\u0004\u0012\u00020\u00010\u000b¨\u0006\u000f" }, d2 = { "clearFragmentResult", "", "Landroidx/fragment/app/Fragment;", "requestKey", "", "clearFragmentResultListener", "setFragmentResult", "result", "Landroid/os/Bundle;", "setFragmentResultListener", "listener", "Lkotlin/Function2;", "Lkotlin/ParameterName;", "name", "bundle", "fragment-ktx_release" }, k = 2, mv = { 1, 8, 0 }, xi = 48)
public final class FragmentKt
{
    public static final void clearFragmentResult(final Fragment fragment, final String s) {
        Intrinsics.checkNotNullParameter((Object)fragment, "<this>");
        Intrinsics.checkNotNullParameter((Object)s, "requestKey");
        fragment.getParentFragmentManager().clearFragmentResult(s);
    }
    
    public static final void clearFragmentResultListener(final Fragment fragment, final String s) {
        Intrinsics.checkNotNullParameter((Object)fragment, "<this>");
        Intrinsics.checkNotNullParameter((Object)s, "requestKey");
        fragment.getParentFragmentManager().clearFragmentResultListener(s);
    }
    
    public static final void setFragmentResult(final Fragment fragment, final String s, final Bundle bundle) {
        Intrinsics.checkNotNullParameter((Object)fragment, "<this>");
        Intrinsics.checkNotNullParameter((Object)s, "requestKey");
        Intrinsics.checkNotNullParameter((Object)bundle, "result");
        fragment.getParentFragmentManager().setFragmentResult(s, bundle);
    }
    
    public static final void setFragmentResultListener(final Fragment fragment, final String s, final Function2<? super String, ? super Bundle, Unit> function2) {
        Intrinsics.checkNotNullParameter((Object)fragment, "<this>");
        Intrinsics.checkNotNullParameter((Object)s, "requestKey");
        Intrinsics.checkNotNullParameter((Object)function2, "listener");
        fragment.getParentFragmentManager().setFragmentResultListener(s, fragment, new FragmentKt$$ExternalSyntheticLambda0(function2));
    }
    
    private static final void setFragmentResultListener$lambda$0(final Function2 function2, final String s, final Bundle bundle) {
        Intrinsics.checkNotNullParameter((Object)function2, "$tmp0");
        Intrinsics.checkNotNullParameter((Object)s, "p0");
        Intrinsics.checkNotNullParameter((Object)bundle, "p1");
        function2.invoke((Object)s, (Object)bundle);
    }
}

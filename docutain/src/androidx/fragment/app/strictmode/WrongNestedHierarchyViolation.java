// 
// Decompiled by Procyon v0.6.0
// 

package androidx.fragment.app.strictmode;

import kotlin.jvm.internal.Intrinsics;
import androidx.fragment.app.Fragment;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0006\u0018\u00002\u00020\u0001B\u001f\b\u0000\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006¢\u0006\u0002\u0010\u0007R\u0011\u0010\u0005\u001a\u00020\u0006¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010\tR\u0011\u0010\u0004\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000b¨\u0006\f" }, d2 = { "Landroidx/fragment/app/strictmode/WrongNestedHierarchyViolation;", "Landroidx/fragment/app/strictmode/Violation;", "fragment", "Landroidx/fragment/app/Fragment;", "expectedParentFragment", "containerId", "", "(Landroidx/fragment/app/Fragment;Landroidx/fragment/app/Fragment;I)V", "getContainerId", "()I", "getExpectedParentFragment", "()Landroidx/fragment/app/Fragment;", "fragment_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
public final class WrongNestedHierarchyViolation extends Violation
{
    private final int containerId;
    private final Fragment expectedParentFragment;
    
    public WrongNestedHierarchyViolation(final Fragment obj, final Fragment fragment, final int n) {
        Intrinsics.checkNotNullParameter((Object)obj, "fragment");
        Intrinsics.checkNotNullParameter((Object)fragment, "expectedParentFragment");
        final StringBuilder sb = new StringBuilder();
        sb.append("Attempting to nest fragment ");
        sb.append(obj);
        sb.append(" within the view of parent fragment ");
        sb.append(fragment);
        sb.append(" via container with ID ");
        sb.append(n);
        sb.append(" without using parent's childFragmentManager");
        super(obj, sb.toString());
        this.expectedParentFragment = fragment;
        this.containerId = n;
    }
    
    public final int getContainerId() {
        return this.containerId;
    }
    
    public final Fragment getExpectedParentFragment() {
        return this.expectedParentFragment;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.fragment.app.strictmode;

import java.util.LinkedHashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import kotlin.collections.MapsKt;
import kotlin.collections.SetsKt;
import kotlin.jvm.internal.DefaultConstructorMarker;
import java.util.Map;
import kotlin.collections.CollectionsKt;
import java.util.Set;
import android.os.Handler;
import android.os.Looper;
import android.view.ViewGroup;
import kotlin.jvm.JvmStatic;
import android.util.Log;
import androidx.fragment.app.FragmentManager;
import kotlin.jvm.internal.Intrinsics;
import androidx.fragment.app.Fragment;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000X\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u00c6\u0002\u0018\u00002\u00020\u0001:\u0003123B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0012\u0010\u000b\u001a\u00020\u00062\b\u0010\f\u001a\u0004\u0018\u00010\rH\u0002J\u0018\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u00062\u0006\u0010\u0011\u001a\u00020\u0012H\u0002J\u0010\u0010\u0013\u001a\u00020\u000f2\u0006\u0010\u0011\u001a\u00020\u0012H\u0002J\u0018\u0010\u0014\u001a\u00020\u000f2\u0006\u0010\f\u001a\u00020\r2\u0006\u0010\u0015\u001a\u00020\u0004H\u0007J\u001a\u0010\u0016\u001a\u00020\u000f2\u0006\u0010\f\u001a\u00020\r2\b\u0010\u0017\u001a\u0004\u0018\u00010\u0018H\u0007J\u0010\u0010\u0019\u001a\u00020\u000f2\u0006\u0010\f\u001a\u00020\rH\u0007J\u0010\u0010\u001a\u001a\u00020\u000f2\u0006\u0010\f\u001a\u00020\rH\u0007J\u0010\u0010\u001b\u001a\u00020\u000f2\u0006\u0010\f\u001a\u00020\rH\u0007J\u0010\u0010\u001c\u001a\u00020\u000f2\u0006\u0010\u0011\u001a\u00020\u0012H\u0007J\u0010\u0010\u001d\u001a\u00020\u000f2\u0006\u0010\f\u001a\u00020\rH\u0007J \u0010\u001e\u001a\u00020\u000f2\u0006\u0010\u001f\u001a\u00020\r2\u0006\u0010 \u001a\u00020\r2\u0006\u0010!\u001a\u00020\"H\u0007J\u0018\u0010#\u001a\u00020\u000f2\u0006\u0010\f\u001a\u00020\r2\u0006\u0010$\u001a\u00020%H\u0007J\u0018\u0010&\u001a\u00020\u000f2\u0006\u0010\f\u001a\u00020\r2\u0006\u0010\u0017\u001a\u00020\u0018H\u0007J \u0010'\u001a\u00020\u000f2\u0006\u0010\f\u001a\u00020\r2\u0006\u0010(\u001a\u00020\r2\u0006\u0010)\u001a\u00020\"H\u0007J\u0018\u0010*\u001a\u00020\u000f2\u0006\u0010\f\u001a\u00020\r2\u0006\u0010+\u001a\u00020,H\u0002J0\u0010-\u001a\u00020%2\u0006\u0010\u0010\u001a\u00020\u00062\u000e\u0010.\u001a\n\u0012\u0006\b\u0001\u0012\u00020\r0/2\u000e\u00100\u001a\n\u0012\u0006\b\u0001\u0012\u00020\u00120/H\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082T¢\u0006\u0002\n\u0000R\u001a\u0010\u0005\u001a\u00020\u0006X\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0007\u0010\b\"\u0004\b\t\u0010\n¨\u00064" }, d2 = { "Landroidx/fragment/app/strictmode/FragmentStrictMode;", "", "()V", "TAG", "", "defaultPolicy", "Landroidx/fragment/app/strictmode/FragmentStrictMode$Policy;", "getDefaultPolicy", "()Landroidx/fragment/app/strictmode/FragmentStrictMode$Policy;", "setDefaultPolicy", "(Landroidx/fragment/app/strictmode/FragmentStrictMode$Policy;)V", "getNearestPolicy", "fragment", "Landroidx/fragment/app/Fragment;", "handlePolicyViolation", "", "policy", "violation", "Landroidx/fragment/app/strictmode/Violation;", "logIfDebuggingEnabled", "onFragmentReuse", "previousFragmentId", "onFragmentTagUsage", "container", "Landroid/view/ViewGroup;", "onGetRetainInstanceUsage", "onGetTargetFragmentRequestCodeUsage", "onGetTargetFragmentUsage", "onPolicyViolation", "onSetRetainInstanceUsage", "onSetTargetFragmentUsage", "violatingFragment", "targetFragment", "requestCode", "", "onSetUserVisibleHint", "isVisibleToUser", "", "onWrongFragmentContainer", "onWrongNestedHierarchy", "expectedParentFragment", "containerId", "runOnHostThread", "runnable", "Ljava/lang/Runnable;", "shouldHandlePolicyViolation", "fragmentClass", "Ljava/lang/Class;", "violationClass", "Flag", "OnViolationListener", "Policy", "fragment_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
public final class FragmentStrictMode
{
    public static final FragmentStrictMode INSTANCE;
    private static final String TAG = "FragmentStrictMode";
    private static Policy defaultPolicy;
    
    static {
        INSTANCE = new FragmentStrictMode();
        FragmentStrictMode.defaultPolicy = Policy.LAX;
    }
    
    private FragmentStrictMode() {
    }
    
    private final Policy getNearestPolicy(Fragment parentFragment) {
        while (parentFragment != null) {
            if (parentFragment.isAdded()) {
                final FragmentManager parentFragmentManager = parentFragment.getParentFragmentManager();
                Intrinsics.checkNotNullExpressionValue((Object)parentFragmentManager, "declaringFragment.parentFragmentManager");
                if (parentFragmentManager.getStrictModePolicy() != null) {
                    final Policy strictModePolicy = parentFragmentManager.getStrictModePolicy();
                    Intrinsics.checkNotNull((Object)strictModePolicy);
                    return strictModePolicy;
                }
            }
            parentFragment = parentFragment.getParentFragment();
        }
        return FragmentStrictMode.defaultPolicy;
    }
    
    private final void handlePolicyViolation(final Policy policy, final Violation violation) {
        final Fragment fragment = violation.getFragment();
        final String name = fragment.getClass().getName();
        if (policy.getFlags$fragment_release().contains(Flag.PENALTY_LOG)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Policy violation in ");
            sb.append(name);
            Log.d("FragmentStrictMode", sb.toString(), (Throwable)violation);
        }
        if (policy.getListener$fragment_release() != null) {
            this.runOnHostThread(fragment, new FragmentStrictMode$$ExternalSyntheticLambda0(policy, violation));
        }
        if (policy.getFlags$fragment_release().contains(Flag.PENALTY_DEATH)) {
            this.runOnHostThread(fragment, new FragmentStrictMode$$ExternalSyntheticLambda1(name, violation));
        }
    }
    
    private static final void handlePolicyViolation$lambda$0(final Policy policy, final Violation violation) {
        Intrinsics.checkNotNullParameter((Object)policy, "$policy");
        Intrinsics.checkNotNullParameter((Object)violation, "$violation");
        policy.getListener$fragment_release().onViolation(violation);
    }
    
    private static final void handlePolicyViolation$lambda$1(final String str, final Violation violation) {
        Intrinsics.checkNotNullParameter((Object)violation, "$violation");
        final StringBuilder sb = new StringBuilder();
        sb.append("Policy violation with PENALTY_DEATH in ");
        sb.append(str);
        Log.e("FragmentStrictMode", sb.toString(), (Throwable)violation);
        throw violation;
    }
    
    private final void logIfDebuggingEnabled(final Violation violation) {
        if (FragmentManager.isLoggingEnabled(3)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("StrictMode violation in ");
            sb.append(violation.getFragment().getClass().getName());
            Log.d("FragmentManager", sb.toString(), (Throwable)violation);
        }
    }
    
    @JvmStatic
    public static final void onFragmentReuse(final Fragment fragment, final String s) {
        Intrinsics.checkNotNullParameter((Object)fragment, "fragment");
        Intrinsics.checkNotNullParameter((Object)s, "previousFragmentId");
        final Violation violation = new FragmentReuseViolation(fragment, s);
        final FragmentStrictMode instance = FragmentStrictMode.INSTANCE;
        instance.logIfDebuggingEnabled(violation);
        final Policy nearestPolicy = instance.getNearestPolicy(fragment);
        if (nearestPolicy.getFlags$fragment_release().contains(Flag.DETECT_FRAGMENT_REUSE) && instance.shouldHandlePolicyViolation(nearestPolicy, fragment.getClass(), violation.getClass())) {
            instance.handlePolicyViolation(nearestPolicy, violation);
        }
    }
    
    @JvmStatic
    public static final void onFragmentTagUsage(final Fragment fragment, final ViewGroup viewGroup) {
        Intrinsics.checkNotNullParameter((Object)fragment, "fragment");
        final Violation violation = new FragmentTagUsageViolation(fragment, viewGroup);
        final FragmentStrictMode instance = FragmentStrictMode.INSTANCE;
        instance.logIfDebuggingEnabled(violation);
        final Policy nearestPolicy = instance.getNearestPolicy(fragment);
        if (nearestPolicy.getFlags$fragment_release().contains(Flag.DETECT_FRAGMENT_TAG_USAGE) && instance.shouldHandlePolicyViolation(nearestPolicy, fragment.getClass(), violation.getClass())) {
            instance.handlePolicyViolation(nearestPolicy, violation);
        }
    }
    
    @JvmStatic
    public static final void onGetRetainInstanceUsage(final Fragment fragment) {
        Intrinsics.checkNotNullParameter((Object)fragment, "fragment");
        final Violation violation = new GetRetainInstanceUsageViolation(fragment);
        final FragmentStrictMode instance = FragmentStrictMode.INSTANCE;
        instance.logIfDebuggingEnabled(violation);
        final Policy nearestPolicy = instance.getNearestPolicy(fragment);
        if (nearestPolicy.getFlags$fragment_release().contains(Flag.DETECT_RETAIN_INSTANCE_USAGE) && instance.shouldHandlePolicyViolation(nearestPolicy, fragment.getClass(), violation.getClass())) {
            instance.handlePolicyViolation(nearestPolicy, violation);
        }
    }
    
    @JvmStatic
    public static final void onGetTargetFragmentRequestCodeUsage(final Fragment fragment) {
        Intrinsics.checkNotNullParameter((Object)fragment, "fragment");
        final Violation violation = new GetTargetFragmentRequestCodeUsageViolation(fragment);
        final FragmentStrictMode instance = FragmentStrictMode.INSTANCE;
        instance.logIfDebuggingEnabled(violation);
        final Policy nearestPolicy = instance.getNearestPolicy(fragment);
        if (nearestPolicy.getFlags$fragment_release().contains(Flag.DETECT_TARGET_FRAGMENT_USAGE) && instance.shouldHandlePolicyViolation(nearestPolicy, fragment.getClass(), violation.getClass())) {
            instance.handlePolicyViolation(nearestPolicy, violation);
        }
    }
    
    @JvmStatic
    public static final void onGetTargetFragmentUsage(final Fragment fragment) {
        Intrinsics.checkNotNullParameter((Object)fragment, "fragment");
        final Violation violation = new GetTargetFragmentUsageViolation(fragment);
        final FragmentStrictMode instance = FragmentStrictMode.INSTANCE;
        instance.logIfDebuggingEnabled(violation);
        final Policy nearestPolicy = instance.getNearestPolicy(fragment);
        if (nearestPolicy.getFlags$fragment_release().contains(Flag.DETECT_TARGET_FRAGMENT_USAGE) && instance.shouldHandlePolicyViolation(nearestPolicy, fragment.getClass(), violation.getClass())) {
            instance.handlePolicyViolation(nearestPolicy, violation);
        }
    }
    
    @JvmStatic
    public static final void onSetRetainInstanceUsage(final Fragment fragment) {
        Intrinsics.checkNotNullParameter((Object)fragment, "fragment");
        final Violation violation = new SetRetainInstanceUsageViolation(fragment);
        final FragmentStrictMode instance = FragmentStrictMode.INSTANCE;
        instance.logIfDebuggingEnabled(violation);
        final Policy nearestPolicy = instance.getNearestPolicy(fragment);
        if (nearestPolicy.getFlags$fragment_release().contains(Flag.DETECT_RETAIN_INSTANCE_USAGE) && instance.shouldHandlePolicyViolation(nearestPolicy, fragment.getClass(), violation.getClass())) {
            instance.handlePolicyViolation(nearestPolicy, violation);
        }
    }
    
    @JvmStatic
    public static final void onSetTargetFragmentUsage(final Fragment fragment, final Fragment fragment2, final int n) {
        Intrinsics.checkNotNullParameter((Object)fragment, "violatingFragment");
        Intrinsics.checkNotNullParameter((Object)fragment2, "targetFragment");
        final Violation violation = new SetTargetFragmentUsageViolation(fragment, fragment2, n);
        final FragmentStrictMode instance = FragmentStrictMode.INSTANCE;
        instance.logIfDebuggingEnabled(violation);
        final Policy nearestPolicy = instance.getNearestPolicy(fragment);
        if (nearestPolicy.getFlags$fragment_release().contains(Flag.DETECT_TARGET_FRAGMENT_USAGE) && instance.shouldHandlePolicyViolation(nearestPolicy, fragment.getClass(), violation.getClass())) {
            instance.handlePolicyViolation(nearestPolicy, violation);
        }
    }
    
    @JvmStatic
    public static final void onSetUserVisibleHint(final Fragment fragment, final boolean b) {
        Intrinsics.checkNotNullParameter((Object)fragment, "fragment");
        final Violation violation = new SetUserVisibleHintViolation(fragment, b);
        final FragmentStrictMode instance = FragmentStrictMode.INSTANCE;
        instance.logIfDebuggingEnabled(violation);
        final Policy nearestPolicy = instance.getNearestPolicy(fragment);
        if (nearestPolicy.getFlags$fragment_release().contains(Flag.DETECT_SET_USER_VISIBLE_HINT) && instance.shouldHandlePolicyViolation(nearestPolicy, fragment.getClass(), violation.getClass())) {
            instance.handlePolicyViolation(nearestPolicy, violation);
        }
    }
    
    @JvmStatic
    public static final void onWrongFragmentContainer(final Fragment fragment, final ViewGroup viewGroup) {
        Intrinsics.checkNotNullParameter((Object)fragment, "fragment");
        Intrinsics.checkNotNullParameter((Object)viewGroup, "container");
        final Violation violation = new WrongFragmentContainerViolation(fragment, viewGroup);
        final FragmentStrictMode instance = FragmentStrictMode.INSTANCE;
        instance.logIfDebuggingEnabled(violation);
        final Policy nearestPolicy = instance.getNearestPolicy(fragment);
        if (nearestPolicy.getFlags$fragment_release().contains(Flag.DETECT_WRONG_FRAGMENT_CONTAINER) && instance.shouldHandlePolicyViolation(nearestPolicy, fragment.getClass(), violation.getClass())) {
            instance.handlePolicyViolation(nearestPolicy, violation);
        }
    }
    
    @JvmStatic
    public static final void onWrongNestedHierarchy(final Fragment fragment, final Fragment fragment2, final int n) {
        Intrinsics.checkNotNullParameter((Object)fragment, "fragment");
        Intrinsics.checkNotNullParameter((Object)fragment2, "expectedParentFragment");
        final Violation violation = new WrongNestedHierarchyViolation(fragment, fragment2, n);
        final FragmentStrictMode instance = FragmentStrictMode.INSTANCE;
        instance.logIfDebuggingEnabled(violation);
        final Policy nearestPolicy = instance.getNearestPolicy(fragment);
        if (nearestPolicy.getFlags$fragment_release().contains(Flag.DETECT_WRONG_NESTED_HIERARCHY) && instance.shouldHandlePolicyViolation(nearestPolicy, fragment.getClass(), violation.getClass())) {
            instance.handlePolicyViolation(nearestPolicy, violation);
        }
    }
    
    private final void runOnHostThread(final Fragment fragment, final Runnable runnable) {
        if (fragment.isAdded()) {
            final Handler handler = fragment.getParentFragmentManager().getHost().getHandler();
            Intrinsics.checkNotNullExpressionValue((Object)handler, "fragment.parentFragmentManager.host.handler");
            if (Intrinsics.areEqual((Object)handler.getLooper(), (Object)Looper.myLooper())) {
                runnable.run();
            }
            else {
                handler.post(runnable);
            }
        }
        else {
            runnable.run();
        }
    }
    
    private final boolean shouldHandlePolicyViolation(final Policy policy, final Class<? extends Fragment> clazz, final Class<? extends Violation> clazz2) {
        final Set set = policy.getMAllowedViolations$fragment_release().get(clazz.getName());
        return set == null || ((Intrinsics.areEqual((Object)clazz2.getSuperclass(), (Object)Violation.class) || !CollectionsKt.contains((Iterable)set, (Object)clazz2.getSuperclass())) && (set.contains(clazz2) ^ true));
    }
    
    public final Policy getDefaultPolicy() {
        return FragmentStrictMode.defaultPolicy;
    }
    
    public final void onPolicyViolation(final Violation violation) {
        Intrinsics.checkNotNullParameter((Object)violation, "violation");
        this.logIfDebuggingEnabled(violation);
        final Fragment fragment = violation.getFragment();
        final Policy nearestPolicy = this.getNearestPolicy(fragment);
        if (this.shouldHandlePolicyViolation(nearestPolicy, fragment.getClass(), violation.getClass())) {
            this.handlePolicyViolation(nearestPolicy, violation);
        }
    }
    
    public final void setDefaultPolicy(final Policy defaultPolicy) {
        Intrinsics.checkNotNullParameter((Object)defaultPolicy, "<set-?>");
        FragmentStrictMode.defaultPolicy = defaultPolicy;
    }
    
    @Metadata(d1 = { "\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u000b\b\u0080\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002j\u0002\b\u0003j\u0002\b\u0004j\u0002\b\u0005j\u0002\b\u0006j\u0002\b\u0007j\u0002\b\bj\u0002\b\tj\u0002\b\nj\u0002\b\u000b¨\u0006\f" }, d2 = { "Landroidx/fragment/app/strictmode/FragmentStrictMode$Flag;", "", "(Ljava/lang/String;I)V", "PENALTY_LOG", "PENALTY_DEATH", "DETECT_FRAGMENT_REUSE", "DETECT_FRAGMENT_TAG_USAGE", "DETECT_WRONG_NESTED_HIERARCHY", "DETECT_RETAIN_INSTANCE_USAGE", "DETECT_SET_USER_VISIBLE_HINT", "DETECT_TARGET_FRAGMENT_USAGE", "DETECT_WRONG_FRAGMENT_CONTAINER", "fragment_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
    public enum Flag
    {
        private static final Flag[] $VALUES;
        
        DETECT_FRAGMENT_REUSE, 
        DETECT_FRAGMENT_TAG_USAGE, 
        DETECT_RETAIN_INSTANCE_USAGE, 
        DETECT_SET_USER_VISIBLE_HINT, 
        DETECT_TARGET_FRAGMENT_USAGE, 
        DETECT_WRONG_FRAGMENT_CONTAINER, 
        DETECT_WRONG_NESTED_HIERARCHY, 
        PENALTY_DEATH, 
        PENALTY_LOG;
        
        private static final /* synthetic */ Flag[] $values() {
            return new Flag[] { Flag.PENALTY_LOG, Flag.PENALTY_DEATH, Flag.DETECT_FRAGMENT_REUSE, Flag.DETECT_FRAGMENT_TAG_USAGE, Flag.DETECT_WRONG_NESTED_HIERARCHY, Flag.DETECT_RETAIN_INSTANCE_USAGE, Flag.DETECT_SET_USER_VISIBLE_HINT, Flag.DETECT_TARGET_FRAGMENT_USAGE, Flag.DETECT_WRONG_FRAGMENT_CONTAINER };
        }
        
        static {
            $VALUES = $values();
        }
    }
    
    @Metadata(d1 = { "\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u00e6\u0080\u0001\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&\u00f8\u0001\u0000\u0082\u0002\u0006\n\u0004\b!0\u0001¨\u0006\u0006\u00c0\u0006\u0001" }, d2 = { "Landroidx/fragment/app/strictmode/FragmentStrictMode$OnViolationListener;", "", "onViolation", "", "violation", "Landroidx/fragment/app/strictmode/Violation;", "fragment_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
    public interface OnViolationListener
    {
        void onViolation(final Violation p0);
    }
    
    @Metadata(d1 = { "\u00002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\"\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010$\n\u0002\u0010\u000e\n\u0002\u0010#\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u000b\u0018\u0000 \u00162\u00020\u0001:\u0002\u0015\u0016BA\b\u0000\u0012\f\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003\u0012\b\u0010\u0005\u001a\u0004\u0018\u00010\u0006\u0012 \u0010\u0007\u001a\u001c\u0012\u0004\u0012\u00020\t\u0012\u0012\u0012\u0010\u0012\f\u0012\n\u0012\u0006\b\u0001\u0012\u00020\f0\u000b0\n0\b¢\u0006\u0002\u0010\rR\u001a\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003X\u0080\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\u000fR\u0016\u0010\u0005\u001a\u0004\u0018\u00010\u0006X\u0080\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\u0011R.\u0010\u0012\u001a\u001c\u0012\u0004\u0012\u00020\t\u0012\u0012\u0012\u0010\u0012\f\u0012\n\u0012\u0006\b\u0001\u0012\u00020\f0\u000b0\u00030\bX\u0080\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0013\u0010\u0014¨\u0006\u0017" }, d2 = { "Landroidx/fragment/app/strictmode/FragmentStrictMode$Policy;", "", "flags", "", "Landroidx/fragment/app/strictmode/FragmentStrictMode$Flag;", "listener", "Landroidx/fragment/app/strictmode/FragmentStrictMode$OnViolationListener;", "allowedViolations", "", "", "", "Ljava/lang/Class;", "Landroidx/fragment/app/strictmode/Violation;", "(Ljava/util/Set;Landroidx/fragment/app/strictmode/FragmentStrictMode$OnViolationListener;Ljava/util/Map;)V", "getFlags$fragment_release", "()Ljava/util/Set;", "getListener$fragment_release", "()Landroidx/fragment/app/strictmode/FragmentStrictMode$OnViolationListener;", "mAllowedViolations", "getMAllowedViolations$fragment_release", "()Ljava/util/Map;", "Builder", "Companion", "fragment_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
    public static final class Policy
    {
        public static final Companion Companion;
        public static final Policy LAX;
        private final Set<Flag> flags;
        private final OnViolationListener listener;
        private final Map<String, Set<Class<? extends Violation>>> mAllowedViolations;
        
        static {
            Companion = new Companion(null);
            LAX = new Policy(SetsKt.emptySet(), null, MapsKt.emptyMap());
        }
        
        public Policy(final Set<? extends Flag> flags, final OnViolationListener listener, final Map<String, ? extends Set<Class<? extends Violation>>> map) {
            Intrinsics.checkNotNullParameter((Object)flags, "flags");
            Intrinsics.checkNotNullParameter((Object)map, "allowedViolations");
            this.flags = (Set<Flag>)flags;
            this.listener = listener;
            final Map mAllowedViolations = new LinkedHashMap();
            for (final Map.Entry entry : map.entrySet()) {
                mAllowedViolations.put(entry.getKey(), entry.getValue());
            }
            this.mAllowedViolations = mAllowedViolations;
        }
        
        public final Set<Flag> getFlags$fragment_release() {
            return this.flags;
        }
        
        public final OnViolationListener getListener$fragment_release() {
            return this.listener;
        }
        
        public final Map<String, Set<Class<? extends Violation>>> getMAllowedViolations$fragment_release() {
            return this.mAllowedViolations;
        }
        
        @Metadata(d1 = { "\u0000@\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010#\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010%\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u000b\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J(\u0010\r\u001a\u00020\u00002\u000e\u0010\u000e\u001a\n\u0012\u0006\b\u0001\u0012\u00020\u000f0\u000b2\u000e\u0010\u0010\u001a\n\u0012\u0006\b\u0001\u0012\u00020\f0\u000bH\u0007J \u0010\r\u001a\u00020\u00002\u0006\u0010\u000e\u001a\u00020\n2\u000e\u0010\u0010\u001a\n\u0012\u0006\b\u0001\u0012\u00020\f0\u000bH\u0007J\u0006\u0010\u0011\u001a\u00020\u0012J\b\u0010\u0013\u001a\u00020\u0000H\u0007J\b\u0010\u0014\u001a\u00020\u0000H\u0007J\b\u0010\u0015\u001a\u00020\u0000H\u0007J\b\u0010\u0016\u001a\u00020\u0000H\u0007J\b\u0010\u0017\u001a\u00020\u0000H\u0007J\b\u0010\u0018\u001a\u00020\u0000H\u0007J\b\u0010\u0019\u001a\u00020\u0000H\u0007J\b\u0010\u001a\u001a\u00020\u0000H\u0007J\u0010\u0010\u001b\u001a\u00020\u00002\u0006\u0010\u0006\u001a\u00020\u0007H\u0007J\b\u0010\u001c\u001a\u00020\u0000H\u0007R\u0014\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004X\u0082\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\u0006\u001a\u0004\u0018\u00010\u0007X\u0082\u000e¢\u0006\u0002\n\u0000R(\u0010\b\u001a\u001c\u0012\u0004\u0012\u00020\n\u0012\u0012\u0012\u0010\u0012\f\u0012\n\u0012\u0006\b\u0001\u0012\u00020\f0\u000b0\u00040\tX\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u001d" }, d2 = { "Landroidx/fragment/app/strictmode/FragmentStrictMode$Policy$Builder;", "", "()V", "flags", "", "Landroidx/fragment/app/strictmode/FragmentStrictMode$Flag;", "listener", "Landroidx/fragment/app/strictmode/FragmentStrictMode$OnViolationListener;", "mAllowedViolations", "", "", "Ljava/lang/Class;", "Landroidx/fragment/app/strictmode/Violation;", "allowViolation", "fragmentClass", "Landroidx/fragment/app/Fragment;", "violationClass", "build", "Landroidx/fragment/app/strictmode/FragmentStrictMode$Policy;", "detectFragmentReuse", "detectFragmentTagUsage", "detectRetainInstanceUsage", "detectSetUserVisibleHint", "detectTargetFragmentUsage", "detectWrongFragmentContainer", "detectWrongNestedHierarchy", "penaltyDeath", "penaltyListener", "penaltyLog", "fragment_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
        public static final class Builder
        {
            private final Set<Flag> flags;
            private OnViolationListener listener;
            private final Map<String, Set<Class<? extends Violation>>> mAllowedViolations;
            
            public Builder() {
                this.flags = new LinkedHashSet<Flag>();
                this.mAllowedViolations = new LinkedHashMap<String, Set<Class<? extends Violation>>>();
            }
            
            public final Builder allowViolation(final Class<? extends Fragment> clazz, final Class<? extends Violation> clazz2) {
                Intrinsics.checkNotNullParameter((Object)clazz, "fragmentClass");
                Intrinsics.checkNotNullParameter((Object)clazz2, "violationClass");
                final String name = clazz.getName();
                Intrinsics.checkNotNullExpressionValue((Object)name, "fragmentClassString");
                return this.allowViolation(name, clazz2);
            }
            
            public final Builder allowViolation(final String s, final Class<? extends Violation> clazz) {
                Intrinsics.checkNotNullParameter((Object)s, "fragmentClass");
                Intrinsics.checkNotNullParameter((Object)clazz, "violationClass");
                Set set;
                if ((set = this.mAllowedViolations.get(s)) == null) {
                    set = new LinkedHashSet();
                }
                set.add(clazz);
                this.mAllowedViolations.put(s, set);
                return this;
            }
            
            public final Policy build() {
                if (this.listener == null && !this.flags.contains(Flag.PENALTY_DEATH)) {
                    this.penaltyLog();
                }
                return new Policy(this.flags, this.listener, this.mAllowedViolations);
            }
            
            public final Builder detectFragmentReuse() {
                this.flags.add(Flag.DETECT_FRAGMENT_REUSE);
                return this;
            }
            
            public final Builder detectFragmentTagUsage() {
                this.flags.add(Flag.DETECT_FRAGMENT_TAG_USAGE);
                return this;
            }
            
            public final Builder detectRetainInstanceUsage() {
                this.flags.add(Flag.DETECT_RETAIN_INSTANCE_USAGE);
                return this;
            }
            
            public final Builder detectSetUserVisibleHint() {
                this.flags.add(Flag.DETECT_SET_USER_VISIBLE_HINT);
                return this;
            }
            
            public final Builder detectTargetFragmentUsage() {
                this.flags.add(Flag.DETECT_TARGET_FRAGMENT_USAGE);
                return this;
            }
            
            public final Builder detectWrongFragmentContainer() {
                this.flags.add(Flag.DETECT_WRONG_FRAGMENT_CONTAINER);
                return this;
            }
            
            public final Builder detectWrongNestedHierarchy() {
                this.flags.add(Flag.DETECT_WRONG_NESTED_HIERARCHY);
                return this;
            }
            
            public final Builder penaltyDeath() {
                this.flags.add(Flag.PENALTY_DEATH);
                return this;
            }
            
            public final Builder penaltyListener(final OnViolationListener listener) {
                Intrinsics.checkNotNullParameter((Object)listener, "listener");
                this.listener = listener;
                return this;
            }
            
            public final Builder penaltyLog() {
                this.flags.add(Flag.PENALTY_LOG);
                return this;
            }
        }
        
        @Metadata(d1 = { "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\b\u0080\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u0010\u0010\u0003\u001a\u00020\u00048\u0006X\u0087\u0004¢\u0006\u0002\n\u0000¨\u0006\u0005" }, d2 = { "Landroidx/fragment/app/strictmode/FragmentStrictMode$Policy$Companion;", "", "()V", "LAX", "Landroidx/fragment/app/strictmode/FragmentStrictMode$Policy;", "fragment_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
        public static final class Companion
        {
            private Companion() {
            }
        }
    }
}

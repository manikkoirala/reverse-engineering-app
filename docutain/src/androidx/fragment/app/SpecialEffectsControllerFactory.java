// 
// Decompiled by Procyon v0.6.0
// 

package androidx.fragment.app;

import android.view.ViewGroup;

interface SpecialEffectsControllerFactory
{
    SpecialEffectsController createController(final ViewGroup p0);
}

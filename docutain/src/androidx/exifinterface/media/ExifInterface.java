// 
// Decompiled by Procyon v0.6.0
// 

package androidx.exifinterface.media;

import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;
import java.io.FilterOutputStream;
import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.DataInput;
import java.util.concurrent.TimeUnit;
import android.location.Location;
import java.util.regex.Matcher;
import android.graphics.BitmapFactory;
import android.graphics.Bitmap$Config;
import android.graphics.Bitmap;
import java.io.ByteArrayOutputStream;
import java.util.Iterator;
import java.util.Map;
import java.util.Date;
import java.text.ParsePosition;
import android.system.OsConstants;
import android.util.Pair;
import java.nio.ByteBuffer;
import java.io.EOFException;
import java.util.zip.CRC32;
import java.io.OutputStream;
import java.io.BufferedInputStream;
import java.io.Closeable;
import java.io.InputStream;
import java.io.FileInputStream;
import android.os.Build$VERSION;
import java.io.IOException;
import java.io.File;
import java.util.TimeZone;
import java.util.Locale;
import java.util.Collection;
import java.util.Arrays;
import android.util.Log;
import java.io.FileDescriptor;
import java.nio.ByteOrder;
import java.util.Set;
import android.content.res.AssetManager$AssetInputStream;
import java.util.HashSet;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Pattern;
import java.nio.charset.Charset;

public class ExifInterface
{
    public static final short ALTITUDE_ABOVE_SEA_LEVEL = 0;
    public static final short ALTITUDE_BELOW_SEA_LEVEL = 1;
    static final Charset ASCII;
    public static final int[] BITS_PER_SAMPLE_GREYSCALE_1;
    public static final int[] BITS_PER_SAMPLE_GREYSCALE_2;
    public static final int[] BITS_PER_SAMPLE_RGB;
    static final short BYTE_ALIGN_II = 18761;
    static final short BYTE_ALIGN_MM = 19789;
    public static final int COLOR_SPACE_S_RGB = 1;
    public static final int COLOR_SPACE_UNCALIBRATED = 65535;
    public static final short CONTRAST_HARD = 2;
    public static final short CONTRAST_NORMAL = 0;
    public static final short CONTRAST_SOFT = 1;
    public static final int DATA_DEFLATE_ZIP = 8;
    public static final int DATA_HUFFMAN_COMPRESSED = 2;
    public static final int DATA_JPEG = 6;
    public static final int DATA_JPEG_COMPRESSED = 7;
    public static final int DATA_LOSSY_JPEG = 34892;
    public static final int DATA_PACK_BITS_COMPRESSED = 32773;
    public static final int DATA_UNCOMPRESSED = 1;
    private static final Pattern DATETIME_PRIMARY_FORMAT_PATTERN;
    private static final Pattern DATETIME_SECONDARY_FORMAT_PATTERN;
    private static final int DATETIME_VALUE_STRING_LENGTH = 19;
    private static final boolean DEBUG;
    static final byte[] EXIF_ASCII_PREFIX;
    private static final ExifTag[] EXIF_POINTER_TAGS;
    static final ExifTag[][] EXIF_TAGS;
    public static final short EXPOSURE_MODE_AUTO = 0;
    public static final short EXPOSURE_MODE_AUTO_BRACKET = 2;
    public static final short EXPOSURE_MODE_MANUAL = 1;
    public static final short EXPOSURE_PROGRAM_ACTION = 6;
    public static final short EXPOSURE_PROGRAM_APERTURE_PRIORITY = 3;
    public static final short EXPOSURE_PROGRAM_CREATIVE = 5;
    public static final short EXPOSURE_PROGRAM_LANDSCAPE_MODE = 8;
    public static final short EXPOSURE_PROGRAM_MANUAL = 1;
    public static final short EXPOSURE_PROGRAM_NORMAL = 2;
    public static final short EXPOSURE_PROGRAM_NOT_DEFINED = 0;
    public static final short EXPOSURE_PROGRAM_PORTRAIT_MODE = 7;
    public static final short EXPOSURE_PROGRAM_SHUTTER_PRIORITY = 4;
    public static final short FILE_SOURCE_DSC = 3;
    public static final short FILE_SOURCE_OTHER = 0;
    public static final short FILE_SOURCE_REFLEX_SCANNER = 2;
    public static final short FILE_SOURCE_TRANSPARENT_SCANNER = 1;
    public static final short FLAG_FLASH_FIRED = 1;
    public static final short FLAG_FLASH_MODE_AUTO = 24;
    public static final short FLAG_FLASH_MODE_COMPULSORY_FIRING = 8;
    public static final short FLAG_FLASH_MODE_COMPULSORY_SUPPRESSION = 16;
    public static final short FLAG_FLASH_NO_FLASH_FUNCTION = 32;
    public static final short FLAG_FLASH_RED_EYE_SUPPORTED = 64;
    public static final short FLAG_FLASH_RETURN_LIGHT_DETECTED = 6;
    public static final short FLAG_FLASH_RETURN_LIGHT_NOT_DETECTED = 4;
    private static final List<Integer> FLIPPED_ROTATION_ORDER;
    public static final short FORMAT_CHUNKY = 1;
    public static final short FORMAT_PLANAR = 2;
    public static final short GAIN_CONTROL_HIGH_GAIN_DOWN = 4;
    public static final short GAIN_CONTROL_HIGH_GAIN_UP = 2;
    public static final short GAIN_CONTROL_LOW_GAIN_DOWN = 3;
    public static final short GAIN_CONTROL_LOW_GAIN_UP = 1;
    public static final short GAIN_CONTROL_NONE = 0;
    public static final String GPS_DIRECTION_MAGNETIC = "M";
    public static final String GPS_DIRECTION_TRUE = "T";
    public static final String GPS_DISTANCE_KILOMETERS = "K";
    public static final String GPS_DISTANCE_MILES = "M";
    public static final String GPS_DISTANCE_NAUTICAL_MILES = "N";
    public static final String GPS_MEASUREMENT_2D = "2";
    public static final String GPS_MEASUREMENT_3D = "3";
    public static final short GPS_MEASUREMENT_DIFFERENTIAL_CORRECTED = 1;
    public static final String GPS_MEASUREMENT_INTERRUPTED = "V";
    public static final String GPS_MEASUREMENT_IN_PROGRESS = "A";
    public static final short GPS_MEASUREMENT_NO_DIFFERENTIAL = 0;
    public static final String GPS_SPEED_KILOMETERS_PER_HOUR = "K";
    public static final String GPS_SPEED_KNOTS = "N";
    public static final String GPS_SPEED_MILES_PER_HOUR = "M";
    private static final Pattern GPS_TIMESTAMP_PATTERN;
    private static final byte[] HEIF_BRAND_HEIC;
    private static final byte[] HEIF_BRAND_MIF1;
    private static final byte[] HEIF_TYPE_FTYP;
    static final byte[] IDENTIFIER_EXIF_APP1;
    private static final byte[] IDENTIFIER_XMP_APP1;
    private static final ExifTag[] IFD_EXIF_TAGS;
    private static final int IFD_FORMAT_BYTE = 1;
    static final int[] IFD_FORMAT_BYTES_PER_FORMAT;
    private static final int IFD_FORMAT_DOUBLE = 12;
    private static final int IFD_FORMAT_IFD = 13;
    static final String[] IFD_FORMAT_NAMES;
    private static final int IFD_FORMAT_SBYTE = 6;
    private static final int IFD_FORMAT_SINGLE = 11;
    private static final int IFD_FORMAT_SLONG = 9;
    private static final int IFD_FORMAT_SRATIONAL = 10;
    private static final int IFD_FORMAT_SSHORT = 8;
    private static final int IFD_FORMAT_STRING = 2;
    private static final int IFD_FORMAT_ULONG = 4;
    private static final int IFD_FORMAT_UNDEFINED = 7;
    private static final int IFD_FORMAT_URATIONAL = 5;
    private static final int IFD_FORMAT_USHORT = 3;
    private static final ExifTag[] IFD_GPS_TAGS;
    private static final ExifTag[] IFD_INTEROPERABILITY_TAGS;
    private static final int IFD_OFFSET = 8;
    private static final ExifTag[] IFD_THUMBNAIL_TAGS;
    private static final ExifTag[] IFD_TIFF_TAGS;
    private static final int IFD_TYPE_EXIF = 1;
    private static final int IFD_TYPE_GPS = 2;
    private static final int IFD_TYPE_INTEROPERABILITY = 3;
    private static final int IFD_TYPE_ORF_CAMERA_SETTINGS = 7;
    private static final int IFD_TYPE_ORF_IMAGE_PROCESSING = 8;
    private static final int IFD_TYPE_ORF_MAKER_NOTE = 6;
    private static final int IFD_TYPE_PEF = 9;
    static final int IFD_TYPE_PREVIEW = 5;
    static final int IFD_TYPE_PRIMARY = 0;
    static final int IFD_TYPE_THUMBNAIL = 4;
    static final int IMAGE_TYPE_ARW = 1;
    static final int IMAGE_TYPE_CR2 = 2;
    static final int IMAGE_TYPE_DNG = 3;
    static final int IMAGE_TYPE_HEIF = 12;
    static final int IMAGE_TYPE_JPEG = 4;
    static final int IMAGE_TYPE_NEF = 5;
    static final int IMAGE_TYPE_NRW = 6;
    static final int IMAGE_TYPE_ORF = 7;
    static final int IMAGE_TYPE_PEF = 8;
    static final int IMAGE_TYPE_PNG = 13;
    static final int IMAGE_TYPE_RAF = 9;
    static final int IMAGE_TYPE_RW2 = 10;
    static final int IMAGE_TYPE_SRW = 11;
    static final int IMAGE_TYPE_UNKNOWN = 0;
    static final int IMAGE_TYPE_WEBP = 14;
    static final byte[] JPEG_SIGNATURE;
    public static final String LATITUDE_NORTH = "N";
    public static final String LATITUDE_SOUTH = "S";
    public static final short LIGHT_SOURCE_CLOUDY_WEATHER = 10;
    public static final short LIGHT_SOURCE_COOL_WHITE_FLUORESCENT = 14;
    public static final short LIGHT_SOURCE_D50 = 23;
    public static final short LIGHT_SOURCE_D55 = 20;
    public static final short LIGHT_SOURCE_D65 = 21;
    public static final short LIGHT_SOURCE_D75 = 22;
    public static final short LIGHT_SOURCE_DAYLIGHT = 1;
    public static final short LIGHT_SOURCE_DAYLIGHT_FLUORESCENT = 12;
    public static final short LIGHT_SOURCE_DAY_WHITE_FLUORESCENT = 13;
    public static final short LIGHT_SOURCE_FINE_WEATHER = 9;
    public static final short LIGHT_SOURCE_FLASH = 4;
    public static final short LIGHT_SOURCE_FLUORESCENT = 2;
    public static final short LIGHT_SOURCE_ISO_STUDIO_TUNGSTEN = 24;
    public static final short LIGHT_SOURCE_OTHER = 255;
    public static final short LIGHT_SOURCE_SHADE = 11;
    public static final short LIGHT_SOURCE_STANDARD_LIGHT_A = 17;
    public static final short LIGHT_SOURCE_STANDARD_LIGHT_B = 18;
    public static final short LIGHT_SOURCE_STANDARD_LIGHT_C = 19;
    public static final short LIGHT_SOURCE_TUNGSTEN = 3;
    public static final short LIGHT_SOURCE_UNKNOWN = 0;
    public static final short LIGHT_SOURCE_WARM_WHITE_FLUORESCENT = 16;
    public static final short LIGHT_SOURCE_WHITE_FLUORESCENT = 15;
    public static final String LONGITUDE_EAST = "E";
    public static final String LONGITUDE_WEST = "W";
    static final byte MARKER = -1;
    static final byte MARKER_APP1 = -31;
    private static final byte MARKER_COM = -2;
    static final byte MARKER_EOI = -39;
    private static final byte MARKER_SOF0 = -64;
    private static final byte MARKER_SOF1 = -63;
    private static final byte MARKER_SOF10 = -54;
    private static final byte MARKER_SOF11 = -53;
    private static final byte MARKER_SOF13 = -51;
    private static final byte MARKER_SOF14 = -50;
    private static final byte MARKER_SOF15 = -49;
    private static final byte MARKER_SOF2 = -62;
    private static final byte MARKER_SOF3 = -61;
    private static final byte MARKER_SOF5 = -59;
    private static final byte MARKER_SOF6 = -58;
    private static final byte MARKER_SOF7 = -57;
    private static final byte MARKER_SOF9 = -55;
    private static final byte MARKER_SOI = -40;
    private static final byte MARKER_SOS = -38;
    private static final int MAX_THUMBNAIL_SIZE = 512;
    public static final short METERING_MODE_AVERAGE = 1;
    public static final short METERING_MODE_CENTER_WEIGHT_AVERAGE = 2;
    public static final short METERING_MODE_MULTI_SPOT = 4;
    public static final short METERING_MODE_OTHER = 255;
    public static final short METERING_MODE_PARTIAL = 6;
    public static final short METERING_MODE_PATTERN = 5;
    public static final short METERING_MODE_SPOT = 3;
    public static final short METERING_MODE_UNKNOWN = 0;
    private static final Pattern NON_ZERO_TIME_PATTERN;
    private static final ExifTag[] ORF_CAMERA_SETTINGS_TAGS;
    private static final ExifTag[] ORF_IMAGE_PROCESSING_TAGS;
    private static final byte[] ORF_MAKER_NOTE_HEADER_1;
    private static final int ORF_MAKER_NOTE_HEADER_1_SIZE = 8;
    private static final byte[] ORF_MAKER_NOTE_HEADER_2;
    private static final int ORF_MAKER_NOTE_HEADER_2_SIZE = 12;
    private static final ExifTag[] ORF_MAKER_NOTE_TAGS;
    private static final short ORF_SIGNATURE_1 = 20306;
    private static final short ORF_SIGNATURE_2 = 21330;
    public static final int ORIENTATION_FLIP_HORIZONTAL = 2;
    public static final int ORIENTATION_FLIP_VERTICAL = 4;
    public static final int ORIENTATION_NORMAL = 1;
    public static final int ORIENTATION_ROTATE_180 = 3;
    public static final int ORIENTATION_ROTATE_270 = 8;
    public static final int ORIENTATION_ROTATE_90 = 6;
    public static final int ORIENTATION_TRANSPOSE = 5;
    public static final int ORIENTATION_TRANSVERSE = 7;
    public static final int ORIENTATION_UNDEFINED = 0;
    public static final int ORIGINAL_RESOLUTION_IMAGE = 0;
    private static final int PEF_MAKER_NOTE_SKIP_SIZE = 6;
    private static final String PEF_SIGNATURE = "PENTAX";
    private static final ExifTag[] PEF_TAGS;
    public static final int PHOTOMETRIC_INTERPRETATION_BLACK_IS_ZERO = 1;
    public static final int PHOTOMETRIC_INTERPRETATION_RGB = 2;
    public static final int PHOTOMETRIC_INTERPRETATION_WHITE_IS_ZERO = 0;
    public static final int PHOTOMETRIC_INTERPRETATION_YCBCR = 6;
    private static final int PNG_CHUNK_CRC_BYTE_LENGTH = 4;
    private static final int PNG_CHUNK_TYPE_BYTE_LENGTH = 4;
    private static final byte[] PNG_CHUNK_TYPE_EXIF;
    private static final byte[] PNG_CHUNK_TYPE_IEND;
    private static final byte[] PNG_CHUNK_TYPE_IHDR;
    private static final byte[] PNG_SIGNATURE;
    private static final int RAF_OFFSET_TO_JPEG_IMAGE_OFFSET = 84;
    private static final String RAF_SIGNATURE = "FUJIFILMCCD-RAW";
    public static final int REDUCED_RESOLUTION_IMAGE = 1;
    public static final short RENDERED_PROCESS_CUSTOM = 1;
    public static final short RENDERED_PROCESS_NORMAL = 0;
    public static final short RESOLUTION_UNIT_CENTIMETERS = 3;
    public static final short RESOLUTION_UNIT_INCHES = 2;
    private static final List<Integer> ROTATION_ORDER;
    private static final short RW2_SIGNATURE = 85;
    public static final short SATURATION_HIGH = 0;
    public static final short SATURATION_LOW = 0;
    public static final short SATURATION_NORMAL = 0;
    public static final short SCENE_CAPTURE_TYPE_LANDSCAPE = 1;
    public static final short SCENE_CAPTURE_TYPE_NIGHT = 3;
    public static final short SCENE_CAPTURE_TYPE_PORTRAIT = 2;
    public static final short SCENE_CAPTURE_TYPE_STANDARD = 0;
    public static final short SCENE_TYPE_DIRECTLY_PHOTOGRAPHED = 1;
    public static final short SENSITIVITY_TYPE_ISO_SPEED = 3;
    public static final short SENSITIVITY_TYPE_REI = 2;
    public static final short SENSITIVITY_TYPE_REI_AND_ISO = 6;
    public static final short SENSITIVITY_TYPE_SOS = 1;
    public static final short SENSITIVITY_TYPE_SOS_AND_ISO = 5;
    public static final short SENSITIVITY_TYPE_SOS_AND_REI = 4;
    public static final short SENSITIVITY_TYPE_SOS_AND_REI_AND_ISO = 7;
    public static final short SENSITIVITY_TYPE_UNKNOWN = 0;
    public static final short SENSOR_TYPE_COLOR_SEQUENTIAL = 5;
    public static final short SENSOR_TYPE_COLOR_SEQUENTIAL_LINEAR = 8;
    public static final short SENSOR_TYPE_NOT_DEFINED = 1;
    public static final short SENSOR_TYPE_ONE_CHIP = 2;
    public static final short SENSOR_TYPE_THREE_CHIP = 4;
    public static final short SENSOR_TYPE_TRILINEAR = 7;
    public static final short SENSOR_TYPE_TWO_CHIP = 3;
    public static final short SHARPNESS_HARD = 2;
    public static final short SHARPNESS_NORMAL = 0;
    public static final short SHARPNESS_SOFT = 1;
    private static final int SIGNATURE_CHECK_SIZE = 5000;
    private static final int SKIP_BUFFER_SIZE = 8192;
    static final byte START_CODE = 42;
    public static final int STREAM_TYPE_EXIF_DATA_ONLY = 1;
    public static final int STREAM_TYPE_FULL_IMAGE_DATA = 0;
    public static final short SUBJECT_DISTANCE_RANGE_CLOSE_VIEW = 2;
    public static final short SUBJECT_DISTANCE_RANGE_DISTANT_VIEW = 3;
    public static final short SUBJECT_DISTANCE_RANGE_MACRO = 1;
    public static final short SUBJECT_DISTANCE_RANGE_UNKNOWN = 0;
    private static final String TAG = "ExifInterface";
    public static final String TAG_APERTURE_VALUE = "ApertureValue";
    public static final String TAG_ARTIST = "Artist";
    public static final String TAG_BITS_PER_SAMPLE = "BitsPerSample";
    public static final String TAG_BODY_SERIAL_NUMBER = "BodySerialNumber";
    public static final String TAG_BRIGHTNESS_VALUE = "BrightnessValue";
    @Deprecated
    public static final String TAG_CAMARA_OWNER_NAME = "CameraOwnerName";
    public static final String TAG_CAMERA_OWNER_NAME = "CameraOwnerName";
    public static final String TAG_CFA_PATTERN = "CFAPattern";
    public static final String TAG_COLOR_SPACE = "ColorSpace";
    public static final String TAG_COMPONENTS_CONFIGURATION = "ComponentsConfiguration";
    public static final String TAG_COMPRESSED_BITS_PER_PIXEL = "CompressedBitsPerPixel";
    public static final String TAG_COMPRESSION = "Compression";
    public static final String TAG_CONTRAST = "Contrast";
    public static final String TAG_COPYRIGHT = "Copyright";
    public static final String TAG_CUSTOM_RENDERED = "CustomRendered";
    public static final String TAG_DATETIME = "DateTime";
    public static final String TAG_DATETIME_DIGITIZED = "DateTimeDigitized";
    public static final String TAG_DATETIME_ORIGINAL = "DateTimeOriginal";
    public static final String TAG_DEFAULT_CROP_SIZE = "DefaultCropSize";
    public static final String TAG_DEVICE_SETTING_DESCRIPTION = "DeviceSettingDescription";
    public static final String TAG_DIGITAL_ZOOM_RATIO = "DigitalZoomRatio";
    public static final String TAG_DNG_VERSION = "DNGVersion";
    private static final String TAG_EXIF_IFD_POINTER = "ExifIFDPointer";
    public static final String TAG_EXIF_VERSION = "ExifVersion";
    public static final String TAG_EXPOSURE_BIAS_VALUE = "ExposureBiasValue";
    public static final String TAG_EXPOSURE_INDEX = "ExposureIndex";
    public static final String TAG_EXPOSURE_MODE = "ExposureMode";
    public static final String TAG_EXPOSURE_PROGRAM = "ExposureProgram";
    public static final String TAG_EXPOSURE_TIME = "ExposureTime";
    public static final String TAG_FILE_SOURCE = "FileSource";
    public static final String TAG_FLASH = "Flash";
    public static final String TAG_FLASHPIX_VERSION = "FlashpixVersion";
    public static final String TAG_FLASH_ENERGY = "FlashEnergy";
    public static final String TAG_FOCAL_LENGTH = "FocalLength";
    public static final String TAG_FOCAL_LENGTH_IN_35MM_FILM = "FocalLengthIn35mmFilm";
    public static final String TAG_FOCAL_PLANE_RESOLUTION_UNIT = "FocalPlaneResolutionUnit";
    public static final String TAG_FOCAL_PLANE_X_RESOLUTION = "FocalPlaneXResolution";
    public static final String TAG_FOCAL_PLANE_Y_RESOLUTION = "FocalPlaneYResolution";
    public static final String TAG_F_NUMBER = "FNumber";
    public static final String TAG_GAIN_CONTROL = "GainControl";
    public static final String TAG_GAMMA = "Gamma";
    public static final String TAG_GPS_ALTITUDE = "GPSAltitude";
    public static final String TAG_GPS_ALTITUDE_REF = "GPSAltitudeRef";
    public static final String TAG_GPS_AREA_INFORMATION = "GPSAreaInformation";
    public static final String TAG_GPS_DATESTAMP = "GPSDateStamp";
    public static final String TAG_GPS_DEST_BEARING = "GPSDestBearing";
    public static final String TAG_GPS_DEST_BEARING_REF = "GPSDestBearingRef";
    public static final String TAG_GPS_DEST_DISTANCE = "GPSDestDistance";
    public static final String TAG_GPS_DEST_DISTANCE_REF = "GPSDestDistanceRef";
    public static final String TAG_GPS_DEST_LATITUDE = "GPSDestLatitude";
    public static final String TAG_GPS_DEST_LATITUDE_REF = "GPSDestLatitudeRef";
    public static final String TAG_GPS_DEST_LONGITUDE = "GPSDestLongitude";
    public static final String TAG_GPS_DEST_LONGITUDE_REF = "GPSDestLongitudeRef";
    public static final String TAG_GPS_DIFFERENTIAL = "GPSDifferential";
    public static final String TAG_GPS_DOP = "GPSDOP";
    public static final String TAG_GPS_H_POSITIONING_ERROR = "GPSHPositioningError";
    public static final String TAG_GPS_IMG_DIRECTION = "GPSImgDirection";
    public static final String TAG_GPS_IMG_DIRECTION_REF = "GPSImgDirectionRef";
    private static final String TAG_GPS_INFO_IFD_POINTER = "GPSInfoIFDPointer";
    public static final String TAG_GPS_LATITUDE = "GPSLatitude";
    public static final String TAG_GPS_LATITUDE_REF = "GPSLatitudeRef";
    public static final String TAG_GPS_LONGITUDE = "GPSLongitude";
    public static final String TAG_GPS_LONGITUDE_REF = "GPSLongitudeRef";
    public static final String TAG_GPS_MAP_DATUM = "GPSMapDatum";
    public static final String TAG_GPS_MEASURE_MODE = "GPSMeasureMode";
    public static final String TAG_GPS_PROCESSING_METHOD = "GPSProcessingMethod";
    public static final String TAG_GPS_SATELLITES = "GPSSatellites";
    public static final String TAG_GPS_SPEED = "GPSSpeed";
    public static final String TAG_GPS_SPEED_REF = "GPSSpeedRef";
    public static final String TAG_GPS_STATUS = "GPSStatus";
    public static final String TAG_GPS_TIMESTAMP = "GPSTimeStamp";
    public static final String TAG_GPS_TRACK = "GPSTrack";
    public static final String TAG_GPS_TRACK_REF = "GPSTrackRef";
    public static final String TAG_GPS_VERSION_ID = "GPSVersionID";
    public static final String TAG_IMAGE_DESCRIPTION = "ImageDescription";
    public static final String TAG_IMAGE_LENGTH = "ImageLength";
    public static final String TAG_IMAGE_UNIQUE_ID = "ImageUniqueID";
    public static final String TAG_IMAGE_WIDTH = "ImageWidth";
    private static final String TAG_INTEROPERABILITY_IFD_POINTER = "InteroperabilityIFDPointer";
    public static final String TAG_INTEROPERABILITY_INDEX = "InteroperabilityIndex";
    public static final String TAG_ISO_SPEED = "ISOSpeed";
    public static final String TAG_ISO_SPEED_LATITUDE_YYY = "ISOSpeedLatitudeyyy";
    public static final String TAG_ISO_SPEED_LATITUDE_ZZZ = "ISOSpeedLatitudezzz";
    @Deprecated
    public static final String TAG_ISO_SPEED_RATINGS = "ISOSpeedRatings";
    public static final String TAG_JPEG_INTERCHANGE_FORMAT = "JPEGInterchangeFormat";
    public static final String TAG_JPEG_INTERCHANGE_FORMAT_LENGTH = "JPEGInterchangeFormatLength";
    public static final String TAG_LENS_MAKE = "LensMake";
    public static final String TAG_LENS_MODEL = "LensModel";
    public static final String TAG_LENS_SERIAL_NUMBER = "LensSerialNumber";
    public static final String TAG_LENS_SPECIFICATION = "LensSpecification";
    public static final String TAG_LIGHT_SOURCE = "LightSource";
    public static final String TAG_MAKE = "Make";
    public static final String TAG_MAKER_NOTE = "MakerNote";
    public static final String TAG_MAX_APERTURE_VALUE = "MaxApertureValue";
    public static final String TAG_METERING_MODE = "MeteringMode";
    public static final String TAG_MODEL = "Model";
    public static final String TAG_NEW_SUBFILE_TYPE = "NewSubfileType";
    public static final String TAG_OECF = "OECF";
    public static final String TAG_OFFSET_TIME = "OffsetTime";
    public static final String TAG_OFFSET_TIME_DIGITIZED = "OffsetTimeDigitized";
    public static final String TAG_OFFSET_TIME_ORIGINAL = "OffsetTimeOriginal";
    public static final String TAG_ORF_ASPECT_FRAME = "AspectFrame";
    private static final String TAG_ORF_CAMERA_SETTINGS_IFD_POINTER = "CameraSettingsIFDPointer";
    private static final String TAG_ORF_IMAGE_PROCESSING_IFD_POINTER = "ImageProcessingIFDPointer";
    public static final String TAG_ORF_PREVIEW_IMAGE_LENGTH = "PreviewImageLength";
    public static final String TAG_ORF_PREVIEW_IMAGE_START = "PreviewImageStart";
    public static final String TAG_ORF_THUMBNAIL_IMAGE = "ThumbnailImage";
    public static final String TAG_ORIENTATION = "Orientation";
    public static final String TAG_PHOTOGRAPHIC_SENSITIVITY = "PhotographicSensitivity";
    public static final String TAG_PHOTOMETRIC_INTERPRETATION = "PhotometricInterpretation";
    public static final String TAG_PIXEL_X_DIMENSION = "PixelXDimension";
    public static final String TAG_PIXEL_Y_DIMENSION = "PixelYDimension";
    public static final String TAG_PLANAR_CONFIGURATION = "PlanarConfiguration";
    public static final String TAG_PRIMARY_CHROMATICITIES = "PrimaryChromaticities";
    private static final ExifTag TAG_RAF_IMAGE_SIZE;
    public static final String TAG_RECOMMENDED_EXPOSURE_INDEX = "RecommendedExposureIndex";
    public static final String TAG_REFERENCE_BLACK_WHITE = "ReferenceBlackWhite";
    public static final String TAG_RELATED_SOUND_FILE = "RelatedSoundFile";
    public static final String TAG_RESOLUTION_UNIT = "ResolutionUnit";
    public static final String TAG_ROWS_PER_STRIP = "RowsPerStrip";
    public static final String TAG_RW2_ISO = "ISO";
    public static final String TAG_RW2_JPG_FROM_RAW = "JpgFromRaw";
    public static final String TAG_RW2_SENSOR_BOTTOM_BORDER = "SensorBottomBorder";
    public static final String TAG_RW2_SENSOR_LEFT_BORDER = "SensorLeftBorder";
    public static final String TAG_RW2_SENSOR_RIGHT_BORDER = "SensorRightBorder";
    public static final String TAG_RW2_SENSOR_TOP_BORDER = "SensorTopBorder";
    public static final String TAG_SAMPLES_PER_PIXEL = "SamplesPerPixel";
    public static final String TAG_SATURATION = "Saturation";
    public static final String TAG_SCENE_CAPTURE_TYPE = "SceneCaptureType";
    public static final String TAG_SCENE_TYPE = "SceneType";
    public static final String TAG_SENSING_METHOD = "SensingMethod";
    public static final String TAG_SENSITIVITY_TYPE = "SensitivityType";
    public static final String TAG_SHARPNESS = "Sharpness";
    public static final String TAG_SHUTTER_SPEED_VALUE = "ShutterSpeedValue";
    public static final String TAG_SOFTWARE = "Software";
    public static final String TAG_SPATIAL_FREQUENCY_RESPONSE = "SpatialFrequencyResponse";
    public static final String TAG_SPECTRAL_SENSITIVITY = "SpectralSensitivity";
    public static final String TAG_STANDARD_OUTPUT_SENSITIVITY = "StandardOutputSensitivity";
    public static final String TAG_STRIP_BYTE_COUNTS = "StripByteCounts";
    public static final String TAG_STRIP_OFFSETS = "StripOffsets";
    public static final String TAG_SUBFILE_TYPE = "SubfileType";
    public static final String TAG_SUBJECT_AREA = "SubjectArea";
    public static final String TAG_SUBJECT_DISTANCE = "SubjectDistance";
    public static final String TAG_SUBJECT_DISTANCE_RANGE = "SubjectDistanceRange";
    public static final String TAG_SUBJECT_LOCATION = "SubjectLocation";
    public static final String TAG_SUBSEC_TIME = "SubSecTime";
    public static final String TAG_SUBSEC_TIME_DIGITIZED = "SubSecTimeDigitized";
    public static final String TAG_SUBSEC_TIME_ORIGINAL = "SubSecTimeOriginal";
    private static final String TAG_SUB_IFD_POINTER = "SubIFDPointer";
    public static final String TAG_THUMBNAIL_IMAGE_LENGTH = "ThumbnailImageLength";
    public static final String TAG_THUMBNAIL_IMAGE_WIDTH = "ThumbnailImageWidth";
    public static final String TAG_THUMBNAIL_ORIENTATION = "ThumbnailOrientation";
    public static final String TAG_TRANSFER_FUNCTION = "TransferFunction";
    public static final String TAG_USER_COMMENT = "UserComment";
    public static final String TAG_WHITE_BALANCE = "WhiteBalance";
    public static final String TAG_WHITE_POINT = "WhitePoint";
    public static final String TAG_XMP = "Xmp";
    public static final String TAG_X_RESOLUTION = "XResolution";
    public static final String TAG_Y_CB_CR_COEFFICIENTS = "YCbCrCoefficients";
    public static final String TAG_Y_CB_CR_POSITIONING = "YCbCrPositioning";
    public static final String TAG_Y_CB_CR_SUB_SAMPLING = "YCbCrSubSampling";
    public static final String TAG_Y_RESOLUTION = "YResolution";
    private static final int WEBP_CHUNK_SIZE_BYTE_LENGTH = 4;
    private static final byte[] WEBP_CHUNK_TYPE_ANIM;
    private static final byte[] WEBP_CHUNK_TYPE_ANMF;
    private static final int WEBP_CHUNK_TYPE_BYTE_LENGTH = 4;
    private static final byte[] WEBP_CHUNK_TYPE_EXIF;
    private static final byte[] WEBP_CHUNK_TYPE_VP8;
    private static final byte[] WEBP_CHUNK_TYPE_VP8L;
    private static final byte[] WEBP_CHUNK_TYPE_VP8X;
    private static final int WEBP_CHUNK_TYPE_VP8X_DEFAULT_LENGTH = 10;
    private static final int WEBP_FILE_SIZE_BYTE_LENGTH = 4;
    private static final byte[] WEBP_SIGNATURE_1;
    private static final byte[] WEBP_SIGNATURE_2;
    private static final byte WEBP_VP8L_SIGNATURE = 47;
    private static final byte[] WEBP_VP8_SIGNATURE;
    @Deprecated
    public static final int WHITEBALANCE_AUTO = 0;
    @Deprecated
    public static final int WHITEBALANCE_MANUAL = 1;
    public static final short WHITE_BALANCE_AUTO = 0;
    public static final short WHITE_BALANCE_MANUAL = 1;
    public static final short Y_CB_CR_POSITIONING_CENTERED = 1;
    public static final short Y_CB_CR_POSITIONING_CO_SITED = 2;
    private static final HashMap<Integer, Integer> sExifPointerTagMap;
    private static final HashMap<Integer, ExifTag>[] sExifTagMapsForReading;
    private static final HashMap<String, ExifTag>[] sExifTagMapsForWriting;
    private static SimpleDateFormat sFormatterPrimary;
    private static SimpleDateFormat sFormatterSecondary;
    private static final HashSet<String> sTagSetForCompatibility;
    private boolean mAreThumbnailStripsConsecutive;
    private AssetManager$AssetInputStream mAssetInputStream;
    private final HashMap<String, ExifAttribute>[] mAttributes;
    private Set<Integer> mAttributesOffsets;
    private ByteOrder mExifByteOrder;
    private String mFilename;
    private boolean mHasThumbnail;
    private boolean mHasThumbnailStrips;
    private boolean mIsExifDataOnly;
    private int mMimeType;
    private boolean mModified;
    private int mOffsetToExifData;
    private int mOrfMakerNoteOffset;
    private int mOrfThumbnailLength;
    private int mOrfThumbnailOffset;
    private FileDescriptor mSeekableFileDescriptor;
    private byte[] mThumbnailBytes;
    private int mThumbnailCompression;
    private int mThumbnailLength;
    private int mThumbnailOffset;
    private boolean mXmpIsFromSeparateMarker;
    
    static {
        final Integer value = 3;
        DEBUG = Log.isLoggable("ExifInterface", 3);
        final Integer value2 = 1;
        final Integer value3 = 2;
        final Integer value4 = 8;
        ROTATION_ORDER = Arrays.asList(value2, 6, value, value4);
        final Integer value5 = 7;
        final Integer value6 = 5;
        FLIPPED_ROTATION_ORDER = Arrays.asList(value3, value5, 4, value6);
        BITS_PER_SAMPLE_RGB = new int[] { 8, 8, 8 };
        BITS_PER_SAMPLE_GREYSCALE_1 = new int[] { 4 };
        BITS_PER_SAMPLE_GREYSCALE_2 = new int[] { 8 };
        JPEG_SIGNATURE = new byte[] { -1, -40, -1 };
        HEIF_TYPE_FTYP = new byte[] { 102, 116, 121, 112 };
        HEIF_BRAND_MIF1 = new byte[] { 109, 105, 102, 49 };
        HEIF_BRAND_HEIC = new byte[] { 104, 101, 105, 99 };
        ORF_MAKER_NOTE_HEADER_1 = new byte[] { 79, 76, 89, 77, 80, 0 };
        ORF_MAKER_NOTE_HEADER_2 = new byte[] { 79, 76, 89, 77, 80, 85, 83, 0, 73, 73 };
        PNG_SIGNATURE = new byte[] { -119, 80, 78, 71, 13, 10, 26, 10 };
        PNG_CHUNK_TYPE_EXIF = new byte[] { 101, 88, 73, 102 };
        PNG_CHUNK_TYPE_IHDR = new byte[] { 73, 72, 68, 82 };
        PNG_CHUNK_TYPE_IEND = new byte[] { 73, 69, 78, 68 };
        WEBP_SIGNATURE_1 = new byte[] { 82, 73, 70, 70 };
        WEBP_SIGNATURE_2 = new byte[] { 87, 69, 66, 80 };
        WEBP_CHUNK_TYPE_EXIF = new byte[] { 69, 88, 73, 70 };
        WEBP_VP8_SIGNATURE = new byte[] { -99, 1, 42 };
        WEBP_CHUNK_TYPE_VP8X = "VP8X".getBytes(Charset.defaultCharset());
        WEBP_CHUNK_TYPE_VP8L = "VP8L".getBytes(Charset.defaultCharset());
        WEBP_CHUNK_TYPE_VP8 = "VP8 ".getBytes(Charset.defaultCharset());
        WEBP_CHUNK_TYPE_ANIM = "ANIM".getBytes(Charset.defaultCharset());
        WEBP_CHUNK_TYPE_ANMF = "ANMF".getBytes(Charset.defaultCharset());
        IFD_FORMAT_NAMES = new String[] { "", "BYTE", "STRING", "USHORT", "ULONG", "URATIONAL", "SBYTE", "UNDEFINED", "SSHORT", "SLONG", "SRATIONAL", "SINGLE", "DOUBLE", "IFD" };
        IFD_FORMAT_BYTES_PER_FORMAT = new int[] { 0, 1, 1, 2, 4, 8, 1, 1, 2, 4, 8, 4, 8, 1 };
        EXIF_ASCII_PREFIX = new byte[] { 65, 83, 67, 73, 73, 0, 0, 0 };
        final ExifTag[] array = IFD_TIFF_TAGS = new ExifTag[] { new ExifTag("NewSubfileType", 254, 4), new ExifTag("SubfileType", 255, 4), new ExifTag("ImageWidth", 256, 3, 4), new ExifTag("ImageLength", 257, 3, 4), new ExifTag("BitsPerSample", 258, 3), new ExifTag("Compression", 259, 3), new ExifTag("PhotometricInterpretation", 262, 3), new ExifTag("ImageDescription", 270, 2), new ExifTag("Make", 271, 2), new ExifTag("Model", 272, 2), new ExifTag("StripOffsets", 273, 3, 4), new ExifTag("Orientation", 274, 3), new ExifTag("SamplesPerPixel", 277, 3), new ExifTag("RowsPerStrip", 278, 3, 4), new ExifTag("StripByteCounts", 279, 3, 4), new ExifTag("XResolution", 282, 5), new ExifTag("YResolution", 283, 5), new ExifTag("PlanarConfiguration", 284, 3), new ExifTag("ResolutionUnit", 296, 3), new ExifTag("TransferFunction", 301, 3), new ExifTag("Software", 305, 2), new ExifTag("DateTime", 306, 2), new ExifTag("Artist", 315, 2), new ExifTag("WhitePoint", 318, 5), new ExifTag("PrimaryChromaticities", 319, 5), new ExifTag("SubIFDPointer", 330, 4), new ExifTag("JPEGInterchangeFormat", 513, 4), new ExifTag("JPEGInterchangeFormatLength", 514, 4), new ExifTag("YCbCrCoefficients", 529, 5), new ExifTag("YCbCrSubSampling", 530, 3), new ExifTag("YCbCrPositioning", 531, 3), new ExifTag("ReferenceBlackWhite", 532, 5), new ExifTag("Copyright", 33432, 2), new ExifTag("ExifIFDPointer", 34665, 4), new ExifTag("GPSInfoIFDPointer", 34853, 4), new ExifTag("SensorTopBorder", 4, 4), new ExifTag("SensorLeftBorder", 5, 4), new ExifTag("SensorBottomBorder", 6, 4), new ExifTag("SensorRightBorder", 7, 4), new ExifTag("ISO", 23, 3), new ExifTag("JpgFromRaw", 46, 7), new ExifTag("Xmp", 700, 1) };
        final ExifTag[] array2 = IFD_EXIF_TAGS = new ExifTag[] { new ExifTag("ExposureTime", 33434, 5), new ExifTag("FNumber", 33437, 5), new ExifTag("ExposureProgram", 34850, 3), new ExifTag("SpectralSensitivity", 34852, 2), new ExifTag("PhotographicSensitivity", 34855, 3), new ExifTag("OECF", 34856, 7), new ExifTag("SensitivityType", 34864, 3), new ExifTag("StandardOutputSensitivity", 34865, 4), new ExifTag("RecommendedExposureIndex", 34866, 4), new ExifTag("ISOSpeed", 34867, 4), new ExifTag("ISOSpeedLatitudeyyy", 34868, 4), new ExifTag("ISOSpeedLatitudezzz", 34869, 4), new ExifTag("ExifVersion", 36864, 2), new ExifTag("DateTimeOriginal", 36867, 2), new ExifTag("DateTimeDigitized", 36868, 2), new ExifTag("OffsetTime", 36880, 2), new ExifTag("OffsetTimeOriginal", 36881, 2), new ExifTag("OffsetTimeDigitized", 36882, 2), new ExifTag("ComponentsConfiguration", 37121, 7), new ExifTag("CompressedBitsPerPixel", 37122, 5), new ExifTag("ShutterSpeedValue", 37377, 10), new ExifTag("ApertureValue", 37378, 5), new ExifTag("BrightnessValue", 37379, 10), new ExifTag("ExposureBiasValue", 37380, 10), new ExifTag("MaxApertureValue", 37381, 5), new ExifTag("SubjectDistance", 37382, 5), new ExifTag("MeteringMode", 37383, 3), new ExifTag("LightSource", 37384, 3), new ExifTag("Flash", 37385, 3), new ExifTag("FocalLength", 37386, 5), new ExifTag("SubjectArea", 37396, 3), new ExifTag("MakerNote", 37500, 7), new ExifTag("UserComment", 37510, 7), new ExifTag("SubSecTime", 37520, 2), new ExifTag("SubSecTimeOriginal", 37521, 2), new ExifTag("SubSecTimeDigitized", 37522, 2), new ExifTag("FlashpixVersion", 40960, 7), new ExifTag("ColorSpace", 40961, 3), new ExifTag("PixelXDimension", 40962, 3, 4), new ExifTag("PixelYDimension", 40963, 3, 4), new ExifTag("RelatedSoundFile", 40964, 2), new ExifTag("InteroperabilityIFDPointer", 40965, 4), new ExifTag("FlashEnergy", 41483, 5), new ExifTag("SpatialFrequencyResponse", 41484, 7), new ExifTag("FocalPlaneXResolution", 41486, 5), new ExifTag("FocalPlaneYResolution", 41487, 5), new ExifTag("FocalPlaneResolutionUnit", 41488, 3), new ExifTag("SubjectLocation", 41492, 3), new ExifTag("ExposureIndex", 41493, 5), new ExifTag("SensingMethod", 41495, 3), new ExifTag("FileSource", 41728, 7), new ExifTag("SceneType", 41729, 7), new ExifTag("CFAPattern", 41730, 7), new ExifTag("CustomRendered", 41985, 3), new ExifTag("ExposureMode", 41986, 3), new ExifTag("WhiteBalance", 41987, 3), new ExifTag("DigitalZoomRatio", 41988, 5), new ExifTag("FocalLengthIn35mmFilm", 41989, 3), new ExifTag("SceneCaptureType", 41990, 3), new ExifTag("GainControl", 41991, 3), new ExifTag("Contrast", 41992, 3), new ExifTag("Saturation", 41993, 3), new ExifTag("Sharpness", 41994, 3), new ExifTag("DeviceSettingDescription", 41995, 7), new ExifTag("SubjectDistanceRange", 41996, 3), new ExifTag("ImageUniqueID", 42016, 2), new ExifTag("CameraOwnerName", 42032, 2), new ExifTag("BodySerialNumber", 42033, 2), new ExifTag("LensSpecification", 42034, 5), new ExifTag("LensMake", 42035, 2), new ExifTag("LensModel", 42036, 2), new ExifTag("Gamma", 42240, 5), new ExifTag("DNGVersion", 50706, 1), new ExifTag("DefaultCropSize", 50720, 3, 4) };
        final ExifTag[] array3 = IFD_GPS_TAGS = new ExifTag[] { new ExifTag("GPSVersionID", 0, 1), new ExifTag("GPSLatitudeRef", 1, 2), new ExifTag("GPSLatitude", 2, 5, 10), new ExifTag("GPSLongitudeRef", 3, 2), new ExifTag("GPSLongitude", 4, 5, 10), new ExifTag("GPSAltitudeRef", 5, 1), new ExifTag("GPSAltitude", 6, 5), new ExifTag("GPSTimeStamp", 7, 5), new ExifTag("GPSSatellites", 8, 2), new ExifTag("GPSStatus", 9, 2), new ExifTag("GPSMeasureMode", 10, 2), new ExifTag("GPSDOP", 11, 5), new ExifTag("GPSSpeedRef", 12, 2), new ExifTag("GPSSpeed", 13, 5), new ExifTag("GPSTrackRef", 14, 2), new ExifTag("GPSTrack", 15, 5), new ExifTag("GPSImgDirectionRef", 16, 2), new ExifTag("GPSImgDirection", 17, 5), new ExifTag("GPSMapDatum", 18, 2), new ExifTag("GPSDestLatitudeRef", 19, 2), new ExifTag("GPSDestLatitude", 20, 5), new ExifTag("GPSDestLongitudeRef", 21, 2), new ExifTag("GPSDestLongitude", 22, 5), new ExifTag("GPSDestBearingRef", 23, 2), new ExifTag("GPSDestBearing", 24, 5), new ExifTag("GPSDestDistanceRef", 25, 2), new ExifTag("GPSDestDistance", 26, 5), new ExifTag("GPSProcessingMethod", 27, 7), new ExifTag("GPSAreaInformation", 28, 7), new ExifTag("GPSDateStamp", 29, 2), new ExifTag("GPSDifferential", 30, 3), new ExifTag("GPSHPositioningError", 31, 5) };
        final ExifTag[] array4 = IFD_INTEROPERABILITY_TAGS = new ExifTag[] { new ExifTag("InteroperabilityIndex", 1, 2) };
        final ExifTag[] array5 = IFD_THUMBNAIL_TAGS = new ExifTag[] { new ExifTag("NewSubfileType", 254, 4), new ExifTag("SubfileType", 255, 4), new ExifTag("ThumbnailImageWidth", 256, 3, 4), new ExifTag("ThumbnailImageLength", 257, 3, 4), new ExifTag("BitsPerSample", 258, 3), new ExifTag("Compression", 259, 3), new ExifTag("PhotometricInterpretation", 262, 3), new ExifTag("ImageDescription", 270, 2), new ExifTag("Make", 271, 2), new ExifTag("Model", 272, 2), new ExifTag("StripOffsets", 273, 3, 4), new ExifTag("ThumbnailOrientation", 274, 3), new ExifTag("SamplesPerPixel", 277, 3), new ExifTag("RowsPerStrip", 278, 3, 4), new ExifTag("StripByteCounts", 279, 3, 4), new ExifTag("XResolution", 282, 5), new ExifTag("YResolution", 283, 5), new ExifTag("PlanarConfiguration", 284, 3), new ExifTag("ResolutionUnit", 296, 3), new ExifTag("TransferFunction", 301, 3), new ExifTag("Software", 305, 2), new ExifTag("DateTime", 306, 2), new ExifTag("Artist", 315, 2), new ExifTag("WhitePoint", 318, 5), new ExifTag("PrimaryChromaticities", 319, 5), new ExifTag("SubIFDPointer", 330, 4), new ExifTag("JPEGInterchangeFormat", 513, 4), new ExifTag("JPEGInterchangeFormatLength", 514, 4), new ExifTag("YCbCrCoefficients", 529, 5), new ExifTag("YCbCrSubSampling", 530, 3), new ExifTag("YCbCrPositioning", 531, 3), new ExifTag("ReferenceBlackWhite", 532, 5), new ExifTag("Copyright", 33432, 2), new ExifTag("ExifIFDPointer", 34665, 4), new ExifTag("GPSInfoIFDPointer", 34853, 4), new ExifTag("DNGVersion", 50706, 1), new ExifTag("DefaultCropSize", 50720, 3, 4) };
        TAG_RAF_IMAGE_SIZE = new ExifTag("StripOffsets", 273, 3);
        final ExifTag[][] array6 = EXIF_TAGS = new ExifTag[][] { array, array2, array3, array4, array5, array, ORF_MAKER_NOTE_TAGS = new ExifTag[] { new ExifTag("ThumbnailImage", 256, 7), new ExifTag("CameraSettingsIFDPointer", 8224, 4), new ExifTag("ImageProcessingIFDPointer", 8256, 4) }, ORF_CAMERA_SETTINGS_TAGS = new ExifTag[] { new ExifTag("PreviewImageStart", 257, 4), new ExifTag("PreviewImageLength", 258, 4) }, ORF_IMAGE_PROCESSING_TAGS = new ExifTag[] { new ExifTag("AspectFrame", 4371, 3) }, PEF_TAGS = new ExifTag[] { new ExifTag("ColorSpace", 55, 3) } };
        EXIF_POINTER_TAGS = new ExifTag[] { new ExifTag("SubIFDPointer", 330, 4), new ExifTag("ExifIFDPointer", 34665, 4), new ExifTag("GPSInfoIFDPointer", 34853, 4), new ExifTag("InteroperabilityIFDPointer", 40965, 4), new ExifTag("CameraSettingsIFDPointer", 8224, 1), new ExifTag("ImageProcessingIFDPointer", 8256, 1) };
        sExifTagMapsForReading = new HashMap[array6.length];
        sExifTagMapsForWriting = new HashMap[array6.length];
        sTagSetForCompatibility = new HashSet<String>(Arrays.asList("FNumber", "DigitalZoomRatio", "ExposureTime", "SubjectDistance", "GPSTimeStamp"));
        sExifPointerTagMap = new HashMap<Integer, Integer>();
        final Charset charset = ASCII = Charset.forName("US-ASCII");
        IDENTIFIER_EXIF_APP1 = "Exif\u0000\u0000".getBytes(charset);
        IDENTIFIER_XMP_APP1 = "http://ns.adobe.com/xap/1.0/\u0000".getBytes(charset);
        (ExifInterface.sFormatterPrimary = new SimpleDateFormat("yyyy:MM:dd HH:mm:ss", Locale.US)).setTimeZone(TimeZone.getTimeZone("UTC"));
        (ExifInterface.sFormatterSecondary = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US)).setTimeZone(TimeZone.getTimeZone("UTC"));
        int n = 0;
        while (true) {
            final ExifTag[][] exif_TAGS = ExifInterface.EXIF_TAGS;
            if (n >= exif_TAGS.length) {
                break;
            }
            ExifInterface.sExifTagMapsForReading[n] = new HashMap<Integer, ExifTag>();
            ExifInterface.sExifTagMapsForWriting[n] = new HashMap<String, ExifTag>();
            for (final ExifTag exifTag : exif_TAGS[n]) {
                ExifInterface.sExifTagMapsForReading[n].put(exifTag.number, exifTag);
                ExifInterface.sExifTagMapsForWriting[n].put(exifTag.name, exifTag);
            }
            ++n;
        }
        final HashMap<Integer, Integer> sExifPointerTagMap2 = ExifInterface.sExifPointerTagMap;
        final ExifTag[] exif_POINTER_TAGS = ExifInterface.EXIF_POINTER_TAGS;
        sExifPointerTagMap2.put(exif_POINTER_TAGS[0].number, value6);
        sExifPointerTagMap2.put(exif_POINTER_TAGS[1].number, value2);
        sExifPointerTagMap2.put(exif_POINTER_TAGS[2].number, value3);
        sExifPointerTagMap2.put(exif_POINTER_TAGS[3].number, value);
        sExifPointerTagMap2.put(exif_POINTER_TAGS[4].number, value5);
        sExifPointerTagMap2.put(exif_POINTER_TAGS[5].number, value4);
        NON_ZERO_TIME_PATTERN = Pattern.compile(".*[1-9].*");
        GPS_TIMESTAMP_PATTERN = Pattern.compile("^(\\d{2}):(\\d{2}):(\\d{2})$");
        DATETIME_PRIMARY_FORMAT_PATTERN = Pattern.compile("^(\\d{4}):(\\d{2}):(\\d{2})\\s(\\d{2}):(\\d{2}):(\\d{2})$");
        DATETIME_SECONDARY_FORMAT_PATTERN = Pattern.compile("^(\\d{4})-(\\d{2})-(\\d{2})\\s(\\d{2}):(\\d{2}):(\\d{2})$");
    }
    
    public ExifInterface(final File file) throws IOException {
        final ExifTag[][] exif_TAGS = ExifInterface.EXIF_TAGS;
        this.mAttributes = new HashMap[exif_TAGS.length];
        this.mAttributesOffsets = new HashSet<Integer>(exif_TAGS.length);
        this.mExifByteOrder = ByteOrder.BIG_ENDIAN;
        if (file != null) {
            this.initForFilename(file.getAbsolutePath());
            return;
        }
        throw new NullPointerException("file cannot be null");
    }
    
    public ExifInterface(FileDescriptor dup) throws IOException {
        final ExifTag[][] exif_TAGS = ExifInterface.EXIF_TAGS;
        this.mAttributes = new HashMap[exif_TAGS.length];
        this.mAttributesOffsets = new HashSet<Integer>(exif_TAGS.length);
        this.mExifByteOrder = ByteOrder.BIG_ENDIAN;
        if (dup == null) {
            throw new NullPointerException("fileDescriptor cannot be null");
        }
        this.mAssetInputStream = null;
        this.mFilename = null;
        boolean b = false;
        Closeable closeable = null;
        Label_0101: {
            if (Build$VERSION.SDK_INT >= 21 && isSeekableFD(dup)) {
                this.mSeekableFileDescriptor = dup;
                try {
                    dup = ExifInterfaceUtils.Api21Impl.dup(dup);
                    b = true;
                    break Label_0101;
                }
                catch (final Exception cause) {
                    throw new IOException("Failed to duplicate file descriptor", cause);
                }
            }
            this.mSeekableFileDescriptor = null;
            try {
                final FileInputStream fileInputStream = new FileInputStream(dup);
                try {
                    this.loadAttributes(fileInputStream);
                    ExifInterfaceUtils.closeQuietly(fileInputStream);
                    if (b) {
                        ExifInterfaceUtils.closeFileDescriptor(dup);
                    }
                    return;
                }
                finally {}
            }
            finally {
                closeable = null;
            }
        }
        ExifInterfaceUtils.closeQuietly(closeable);
        if (b) {
            ExifInterfaceUtils.closeFileDescriptor(dup);
        }
    }
    
    public ExifInterface(final InputStream inputStream) throws IOException {
        this(inputStream, 0);
    }
    
    public ExifInterface(InputStream in, int n) throws IOException {
        final ExifTag[][] exif_TAGS = ExifInterface.EXIF_TAGS;
        this.mAttributes = new HashMap[exif_TAGS.length];
        this.mAttributesOffsets = new HashSet<Integer>(exif_TAGS.length);
        this.mExifByteOrder = ByteOrder.BIG_ENDIAN;
        if (in != null) {
            this.mFilename = null;
            if (n == 1) {
                n = 1;
            }
            else {
                n = 0;
            }
            Label_0187: {
                if (n != 0) {
                    in = new BufferedInputStream(in, ExifInterface.IDENTIFIER_EXIF_APP1.length);
                    final BufferedInputStream bufferedInputStream = (BufferedInputStream)in;
                    if (!isExifDataOnly((BufferedInputStream)in)) {
                        Log.w("ExifInterface", "Given data does not follow the structure of an Exif-only data.");
                        return;
                    }
                    this.mIsExifDataOnly = true;
                    this.mAssetInputStream = null;
                    this.mSeekableFileDescriptor = null;
                }
                else if (in instanceof AssetManager$AssetInputStream) {
                    this.mAssetInputStream = (AssetManager$AssetInputStream)in;
                    this.mSeekableFileDescriptor = null;
                }
                else {
                    if (in instanceof FileInputStream) {
                        final FileInputStream fileInputStream = (FileInputStream)in;
                        if (isSeekableFD(fileInputStream.getFD())) {
                            this.mAssetInputStream = null;
                            this.mSeekableFileDescriptor = fileInputStream.getFD();
                            break Label_0187;
                        }
                    }
                    this.mAssetInputStream = null;
                    this.mSeekableFileDescriptor = null;
                }
            }
            this.loadAttributes(in);
            return;
        }
        throw new NullPointerException("inputStream cannot be null");
    }
    
    public ExifInterface(final String s) throws IOException {
        final ExifTag[][] exif_TAGS = ExifInterface.EXIF_TAGS;
        this.mAttributes = new HashMap[exif_TAGS.length];
        this.mAttributesOffsets = new HashSet<Integer>(exif_TAGS.length);
        this.mExifByteOrder = ByteOrder.BIG_ENDIAN;
        if (s != null) {
            this.initForFilename(s);
            return;
        }
        throw new NullPointerException("filename cannot be null");
    }
    
    private void addDefaultValuesForCompatibility() {
        final String attribute = this.getAttribute("DateTimeOriginal");
        if (attribute != null && this.getAttribute("DateTime") == null) {
            this.mAttributes[0].put("DateTime", ExifAttribute.createString(attribute));
        }
        if (this.getAttribute("ImageWidth") == null) {
            this.mAttributes[0].put("ImageWidth", ExifAttribute.createULong(0L, this.mExifByteOrder));
        }
        if (this.getAttribute("ImageLength") == null) {
            this.mAttributes[0].put("ImageLength", ExifAttribute.createULong(0L, this.mExifByteOrder));
        }
        if (this.getAttribute("Orientation") == null) {
            this.mAttributes[0].put("Orientation", ExifAttribute.createULong(0L, this.mExifByteOrder));
        }
        if (this.getAttribute("LightSource") == null) {
            this.mAttributes[1].put("LightSource", ExifAttribute.createULong(0L, this.mExifByteOrder));
        }
    }
    
    private String convertDecimalDegree(double n) {
        final long lng = (long)n;
        n -= lng;
        final long lng2 = (long)(n * 60.0);
        final long round = Math.round((n - lng2 / 60.0) * 3600.0 * 1.0E7);
        final StringBuilder sb = new StringBuilder();
        sb.append(lng);
        sb.append("/1,");
        sb.append(lng2);
        sb.append("/1,");
        sb.append(round);
        sb.append("/10000000");
        return sb.toString();
    }
    
    private static double convertRationalLatLonToDouble(final String s, final String s2) {
        try {
            final String[] split = s.split(",", -1);
            final String[] split2 = split[0].split("/", -1);
            final double n = Double.parseDouble(split2[0].trim()) / Double.parseDouble(split2[1].trim());
            final String[] split3 = split[1].split("/", -1);
            final double n2 = Double.parseDouble(split3[0].trim()) / Double.parseDouble(split3[1].trim());
            final String[] split4 = split[2].split("/", -1);
            final double n3 = n + n2 / 60.0 + Double.parseDouble(split4[0].trim()) / Double.parseDouble(split4[1].trim()) / 3600.0;
            if (s2.equals("S") || s2.equals("W")) {
                return -n3;
            }
            if (!s2.equals("N") && !s2.equals("E")) {
                throw new IllegalArgumentException();
            }
            return n3;
        }
        catch (final NumberFormatException | ArrayIndexOutOfBoundsException ex) {
            throw new IllegalArgumentException();
        }
    }
    
    private void copyChunksUpToGivenChunkType(final ByteOrderedDataInputStream byteOrderedDataInputStream, final ByteOrderedDataOutputStream byteOrderedDataOutputStream, final byte[] array, final byte[] array2) throws IOException {
        byte[] b;
        do {
            b = new byte[4];
            if (byteOrderedDataInputStream.read(b) != 4) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Encountered invalid length while copying WebP chunks up tochunk type ");
                final Charset ascii = ExifInterface.ASCII;
                sb.append(new String(array, ascii));
                String string;
                if (array2 == null) {
                    string = "";
                }
                else {
                    final StringBuilder sb2 = new StringBuilder();
                    sb2.append(" or ");
                    sb2.append(new String(array2, ascii));
                    string = sb2.toString();
                }
                sb.append(string);
                throw new IOException(sb.toString());
            }
            this.copyWebPChunk(byteOrderedDataInputStream, byteOrderedDataOutputStream, b);
        } while (!Arrays.equals(b, array) && (array2 == null || !Arrays.equals(b, array2)));
    }
    
    private void copyWebPChunk(final ByteOrderedDataInputStream byteOrderedDataInputStream, final ByteOrderedDataOutputStream byteOrderedDataOutputStream, final byte[] array) throws IOException {
        final int int1 = byteOrderedDataInputStream.readInt();
        byteOrderedDataOutputStream.write(array);
        byteOrderedDataOutputStream.writeInt(int1);
        int n = int1;
        if (int1 % 2 == 1) {
            n = int1 + 1;
        }
        ExifInterfaceUtils.copy(byteOrderedDataInputStream, byteOrderedDataOutputStream, n);
    }
    
    private ExifAttribute getExifAttribute(final String anObject) {
        if (anObject != null) {
            String key = anObject;
            if ("ISOSpeedRatings".equals(anObject)) {
                if (ExifInterface.DEBUG) {
                    Log.d("ExifInterface", "getExifAttribute: Replacing TAG_ISO_SPEED_RATINGS with TAG_PHOTOGRAPHIC_SENSITIVITY.");
                }
                key = "PhotographicSensitivity";
            }
            for (int i = 0; i < ExifInterface.EXIF_TAGS.length; ++i) {
                final ExifAttribute exifAttribute = this.mAttributes[i].get(key);
                if (exifAttribute != null) {
                    return exifAttribute;
                }
            }
            return null;
        }
        throw new NullPointerException("tag shouldn't be null");
    }
    
    private void getHeifAttributes(final SeekableByteOrderedDataInputStream p0) throws IOException {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     3: bipush          28
        //     5: if_icmplt       549
        //     8: new             Landroid/media/MediaMetadataRetriever;
        //    11: dup            
        //    12: invokespecial   android/media/MediaMetadataRetriever.<init>:()V
        //    15: astore          8
        //    17: new             Landroidx/exifinterface/media/ExifInterface$1;
        //    20: astore          5
        //    22: aload           5
        //    24: aload_0        
        //    25: aload_1        
        //    26: invokespecial   androidx/exifinterface/media/ExifInterface$1.<init>:(Landroidx/exifinterface/media/ExifInterface;Landroidx/exifinterface/media/ExifInterface$SeekableByteOrderedDataInputStream;)V
        //    29: aload           8
        //    31: aload           5
        //    33: invokestatic    androidx/exifinterface/media/ExifInterfaceUtils$Api23Impl.setDataSource:(Landroid/media/MediaMetadataRetriever;Landroid/media/MediaDataSource;)V
        //    36: aload           8
        //    38: bipush          33
        //    40: invokevirtual   android/media/MediaMetadataRetriever.extractMetadata:(I)Ljava/lang/String;
        //    43: astore          10
        //    45: aload           8
        //    47: bipush          34
        //    49: invokevirtual   android/media/MediaMetadataRetriever.extractMetadata:(I)Ljava/lang/String;
        //    52: astore          9
        //    54: aload           8
        //    56: bipush          26
        //    58: invokevirtual   android/media/MediaMetadataRetriever.extractMetadata:(I)Ljava/lang/String;
        //    61: astore          6
        //    63: aload           8
        //    65: bipush          17
        //    67: invokevirtual   android/media/MediaMetadataRetriever.extractMetadata:(I)Ljava/lang/String;
        //    70: astore          5
        //    72: ldc_w           "yes"
        //    75: aload           6
        //    77: invokevirtual   java/lang/String.equals:(Ljava/lang/Object;)Z
        //    80: istore          4
        //    82: aconst_null    
        //    83: astore          7
        //    85: iload           4
        //    87: ifeq            120
        //    90: aload           8
        //    92: bipush          29
        //    94: invokevirtual   android/media/MediaMetadataRetriever.extractMetadata:(I)Ljava/lang/String;
        //    97: astore          7
        //    99: aload           8
        //   101: bipush          30
        //   103: invokevirtual   android/media/MediaMetadataRetriever.extractMetadata:(I)Ljava/lang/String;
        //   106: astore          5
        //   108: aload           8
        //   110: bipush          31
        //   112: invokevirtual   android/media/MediaMetadataRetriever.extractMetadata:(I)Ljava/lang/String;
        //   115: astore          6
        //   117: goto            167
        //   120: ldc_w           "yes"
        //   123: aload           5
        //   125: invokevirtual   java/lang/String.equals:(Ljava/lang/Object;)Z
        //   128: ifeq            161
        //   131: aload           8
        //   133: bipush          18
        //   135: invokevirtual   android/media/MediaMetadataRetriever.extractMetadata:(I)Ljava/lang/String;
        //   138: astore          7
        //   140: aload           8
        //   142: bipush          19
        //   144: invokevirtual   android/media/MediaMetadataRetriever.extractMetadata:(I)Ljava/lang/String;
        //   147: astore          5
        //   149: aload           8
        //   151: bipush          24
        //   153: invokevirtual   android/media/MediaMetadataRetriever.extractMetadata:(I)Ljava/lang/String;
        //   156: astore          6
        //   158: goto            167
        //   161: aconst_null    
        //   162: astore          5
        //   164: aconst_null    
        //   165: astore          6
        //   167: aload           7
        //   169: ifnull          197
        //   172: aload_0        
        //   173: getfield        androidx/exifinterface/media/ExifInterface.mAttributes:[Ljava/util/HashMap;
        //   176: iconst_0       
        //   177: aaload         
        //   178: ldc_w           "ImageWidth"
        //   181: aload           7
        //   183: invokestatic    java/lang/Integer.parseInt:(Ljava/lang/String;)I
        //   186: aload_0        
        //   187: getfield        androidx/exifinterface/media/ExifInterface.mExifByteOrder:Ljava/nio/ByteOrder;
        //   190: invokestatic    androidx/exifinterface/media/ExifInterface$ExifAttribute.createUShort:(ILjava/nio/ByteOrder;)Landroidx/exifinterface/media/ExifInterface$ExifAttribute;
        //   193: invokevirtual   java/util/HashMap.put:(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
        //   196: pop            
        //   197: aload           5
        //   199: ifnull          227
        //   202: aload_0        
        //   203: getfield        androidx/exifinterface/media/ExifInterface.mAttributes:[Ljava/util/HashMap;
        //   206: iconst_0       
        //   207: aaload         
        //   208: ldc_w           "ImageLength"
        //   211: aload           5
        //   213: invokestatic    java/lang/Integer.parseInt:(Ljava/lang/String;)I
        //   216: aload_0        
        //   217: getfield        androidx/exifinterface/media/ExifInterface.mExifByteOrder:Ljava/nio/ByteOrder;
        //   220: invokestatic    androidx/exifinterface/media/ExifInterface$ExifAttribute.createUShort:(ILjava/nio/ByteOrder;)Landroidx/exifinterface/media/ExifInterface$ExifAttribute;
        //   223: invokevirtual   java/util/HashMap.put:(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
        //   226: pop            
        //   227: aload           6
        //   229: ifnull          298
        //   232: iconst_1       
        //   233: istore_2       
        //   234: aload           6
        //   236: invokestatic    java/lang/Integer.parseInt:(Ljava/lang/String;)I
        //   239: istore_3       
        //   240: iload_3        
        //   241: bipush          90
        //   243: if_icmpeq       274
        //   246: iload_3        
        //   247: sipush          180
        //   250: if_icmpeq       269
        //   253: iload_3        
        //   254: sipush          270
        //   257: if_icmpeq       263
        //   260: goto            277
        //   263: bipush          8
        //   265: istore_2       
        //   266: goto            277
        //   269: iconst_3       
        //   270: istore_2       
        //   271: goto            277
        //   274: bipush          6
        //   276: istore_2       
        //   277: aload_0        
        //   278: getfield        androidx/exifinterface/media/ExifInterface.mAttributes:[Ljava/util/HashMap;
        //   281: iconst_0       
        //   282: aaload         
        //   283: ldc_w           "Orientation"
        //   286: iload_2        
        //   287: aload_0        
        //   288: getfield        androidx/exifinterface/media/ExifInterface.mExifByteOrder:Ljava/nio/ByteOrder;
        //   291: invokestatic    androidx/exifinterface/media/ExifInterface$ExifAttribute.createUShort:(ILjava/nio/ByteOrder;)Landroidx/exifinterface/media/ExifInterface$ExifAttribute;
        //   294: invokevirtual   java/util/HashMap.put:(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
        //   297: pop            
        //   298: aload           10
        //   300: ifnull          448
        //   303: aload           9
        //   305: ifnull          448
        //   308: aload           10
        //   310: invokestatic    java/lang/Integer.parseInt:(Ljava/lang/String;)I
        //   313: istore_2       
        //   314: aload           9
        //   316: invokestatic    java/lang/Integer.parseInt:(Ljava/lang/String;)I
        //   319: istore_3       
        //   320: iload_3        
        //   321: bipush          6
        //   323: if_icmple       435
        //   326: aload_1        
        //   327: iload_2        
        //   328: i2l            
        //   329: invokevirtual   androidx/exifinterface/media/ExifInterface$SeekableByteOrderedDataInputStream.seek:(J)V
        //   332: bipush          6
        //   334: newarray        B
        //   336: astore          9
        //   338: aload_1        
        //   339: aload           9
        //   341: invokevirtual   androidx/exifinterface/media/ExifInterface$SeekableByteOrderedDataInputStream.read:([B)I
        //   344: bipush          6
        //   346: if_icmpne       422
        //   349: iinc            3, -6
        //   352: aload           9
        //   354: getstatic       androidx/exifinterface/media/ExifInterface.IDENTIFIER_EXIF_APP1:[B
        //   357: invokestatic    java/util/Arrays.equals:([B[B)Z
        //   360: ifeq            409
        //   363: iload_3        
        //   364: newarray        B
        //   366: astore          9
        //   368: aload_1        
        //   369: aload           9
        //   371: invokevirtual   androidx/exifinterface/media/ExifInterface$SeekableByteOrderedDataInputStream.read:([B)I
        //   374: iload_3        
        //   375: if_icmpne       396
        //   378: aload_0        
        //   379: iload_2        
        //   380: bipush          6
        //   382: iadd           
        //   383: putfield        androidx/exifinterface/media/ExifInterface.mOffsetToExifData:I
        //   386: aload_0        
        //   387: aload           9
        //   389: iconst_0       
        //   390: invokespecial   androidx/exifinterface/media/ExifInterface.readExifSegment:([BI)V
        //   393: goto            448
        //   396: new             Ljava/io/IOException;
        //   399: astore_1       
        //   400: aload_1        
        //   401: ldc_w           "Can't read exif"
        //   404: invokespecial   java/io/IOException.<init>:(Ljava/lang/String;)V
        //   407: aload_1        
        //   408: athrow         
        //   409: new             Ljava/io/IOException;
        //   412: astore_1       
        //   413: aload_1        
        //   414: ldc_w           "Invalid identifier"
        //   417: invokespecial   java/io/IOException.<init>:(Ljava/lang/String;)V
        //   420: aload_1        
        //   421: athrow         
        //   422: new             Ljava/io/IOException;
        //   425: astore_1       
        //   426: aload_1        
        //   427: ldc_w           "Can't read identifier"
        //   430: invokespecial   java/io/IOException.<init>:(Ljava/lang/String;)V
        //   433: aload_1        
        //   434: athrow         
        //   435: new             Ljava/io/IOException;
        //   438: astore_1       
        //   439: aload_1        
        //   440: ldc_w           "Invalid exif length"
        //   443: invokespecial   java/io/IOException.<init>:(Ljava/lang/String;)V
        //   446: aload_1        
        //   447: athrow         
        //   448: getstatic       androidx/exifinterface/media/ExifInterface.DEBUG:Z
        //   451: ifeq            518
        //   454: new             Ljava/lang/StringBuilder;
        //   457: astore_1       
        //   458: aload_1        
        //   459: invokespecial   java/lang/StringBuilder.<init>:()V
        //   462: aload_1        
        //   463: ldc_w           "Heif meta: "
        //   466: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   469: pop            
        //   470: aload_1        
        //   471: aload           7
        //   473: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   476: pop            
        //   477: aload_1        
        //   478: ldc_w           "x"
        //   481: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   484: pop            
        //   485: aload_1        
        //   486: aload           5
        //   488: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   491: pop            
        //   492: aload_1        
        //   493: ldc_w           ", rotation "
        //   496: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   499: pop            
        //   500: aload_1        
        //   501: aload           6
        //   503: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   506: pop            
        //   507: ldc_w           "ExifInterface"
        //   510: aload_1        
        //   511: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   514: invokestatic    android/util/Log.d:(Ljava/lang/String;Ljava/lang/String;)I
        //   517: pop            
        //   518: aload           8
        //   520: invokevirtual   android/media/MediaMetadataRetriever.release:()V
        //   523: return         
        //   524: astore_1       
        //   525: goto            542
        //   528: astore_1       
        //   529: new             Ljava/lang/UnsupportedOperationException;
        //   532: astore_1       
        //   533: aload_1        
        //   534: ldc_w           "Failed to read EXIF from HEIF file. Given stream is either malformed or unsupported."
        //   537: invokespecial   java/lang/UnsupportedOperationException.<init>:(Ljava/lang/String;)V
        //   540: aload_1        
        //   541: athrow         
        //   542: aload           8
        //   544: invokevirtual   android/media/MediaMetadataRetriever.release:()V
        //   547: aload_1        
        //   548: athrow         
        //   549: new             Ljava/lang/UnsupportedOperationException;
        //   552: dup            
        //   553: ldc_w           "Reading EXIF from HEIF files is supported from SDK 28 and above"
        //   556: invokespecial   java/lang/UnsupportedOperationException.<init>:(Ljava/lang/String;)V
        //   559: athrow         
        //    Exceptions:
        //  throws java.io.IOException
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                        
        //  -----  -----  -----  -----  ----------------------------
        //  17     82     528    542    Ljava/lang/RuntimeException;
        //  17     82     524    528    Any
        //  90     117    528    542    Ljava/lang/RuntimeException;
        //  90     117    524    528    Any
        //  120    158    528    542    Ljava/lang/RuntimeException;
        //  120    158    524    528    Any
        //  172    197    528    542    Ljava/lang/RuntimeException;
        //  172    197    524    528    Any
        //  202    227    528    542    Ljava/lang/RuntimeException;
        //  202    227    524    528    Any
        //  234    240    528    542    Ljava/lang/RuntimeException;
        //  234    240    524    528    Any
        //  277    298    528    542    Ljava/lang/RuntimeException;
        //  277    298    524    528    Any
        //  308    320    528    542    Ljava/lang/RuntimeException;
        //  308    320    524    528    Any
        //  326    349    528    542    Ljava/lang/RuntimeException;
        //  326    349    524    528    Any
        //  352    393    528    542    Ljava/lang/RuntimeException;
        //  352    393    524    528    Any
        //  396    409    528    542    Ljava/lang/RuntimeException;
        //  396    409    524    528    Any
        //  409    422    528    542    Ljava/lang/RuntimeException;
        //  409    422    524    528    Any
        //  422    435    528    542    Ljava/lang/RuntimeException;
        //  422    435    524    528    Any
        //  435    448    528    542    Ljava/lang/RuntimeException;
        //  435    448    524    528    Any
        //  448    518    528    542    Ljava/lang/RuntimeException;
        //  448    518    524    528    Any
        //  529    542    524    528    Any
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0396:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2604)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:206)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    private void getJpegAttributes(final ByteOrderedDataInputStream obj, final int n, final int n2) throws IOException {
        final boolean debug = ExifInterface.DEBUG;
        final String s = "ExifInterface";
        if (debug) {
            final StringBuilder sb = new StringBuilder();
            sb.append("getJpegAttributes starting with: ");
            sb.append(obj);
            Log.d("ExifInterface", sb.toString());
        }
        obj.setByteOrder(ByteOrder.BIG_ENDIAN);
        final byte byte1 = obj.readByte();
        if (byte1 != -1) {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("Invalid marker: ");
            sb2.append(Integer.toHexString(byte1 & 0xFF));
            throw new IOException(sb2.toString());
        }
        if (obj.readByte() != -40) {
            final StringBuilder sb3 = new StringBuilder();
            sb3.append("Invalid marker: ");
            sb3.append(Integer.toHexString(byte1 & 0xFF));
            throw new IOException(sb3.toString());
        }
        int n3 = 2;
        while (true) {
            final byte byte2 = obj.readByte();
            if (byte2 != -1) {
                final StringBuilder sb4 = new StringBuilder();
                sb4.append("Invalid marker:");
                sb4.append(Integer.toHexString(byte2 & 0xFF));
                throw new IOException(sb4.toString());
            }
            final byte byte3 = obj.readByte();
            final boolean debug2 = ExifInterface.DEBUG;
            if (debug2) {
                final StringBuilder sb5 = new StringBuilder();
                sb5.append("Found JPEG segment indicator: ");
                sb5.append(Integer.toHexString(byte3 & 0xFF));
                Log.d(s, sb5.toString());
            }
            if (byte3 == -39 || byte3 == -38) {
                obj.setByteOrder(this.mExifByteOrder);
                return;
            }
            final int n4 = obj.readUnsignedShort() - 2;
            int n5 = n3 + 1 + 1 + 2;
            if (debug2) {
                final StringBuilder sb6 = new StringBuilder();
                sb6.append("JPEG segment: ");
                sb6.append(Integer.toHexString(byte3 & 0xFF));
                sb6.append(" (length: ");
                sb6.append(n4 + 2);
                sb6.append(")");
                Log.d(s, sb6.toString());
            }
            if (n4 < 0) {
                throw new IOException("Invalid length");
            }
            int n7 = 0;
            Label_0764: {
                int n8;
                if (byte3 != -31) {
                    if (byte3 != -2) {
                        int n6 = 0;
                        Label_0408: {
                            switch (byte3) {
                                default:
                                    Label_0415: {
                                        switch (byte3) {
                                            default: {
                                                switch (byte3) {
                                                    default: {
                                                        switch (byte3) {
                                                            default: {
                                                                n6 = n4;
                                                                break Label_0408;
                                                            }
                                                            case -51:
                                                            case -50:
                                                            case -49: {
                                                                break Label_0415;
                                                            }
                                                        }
                                                        break;
                                                    }
                                                    case -55:
                                                    case -54:
                                                    case -53: {
                                                        break Label_0415;
                                                    }
                                                }
                                                break;
                                            }
                                            case -59:
                                            case -58:
                                            case -57: {
                                                break Label_0415;
                                            }
                                        }
                                        break;
                                    }
                                case -64:
                                case -63:
                                case -62:
                                case -61: {
                                    obj.skipFully(1);
                                    final HashMap<String, ExifAttribute> hashMap = this.mAttributes[n2];
                                    String key;
                                    if (n2 != 4) {
                                        key = "ImageLength";
                                    }
                                    else {
                                        key = "ThumbnailImageLength";
                                    }
                                    hashMap.put(key, ExifAttribute.createULong(obj.readUnsignedShort(), this.mExifByteOrder));
                                    final HashMap<String, ExifAttribute> hashMap2 = this.mAttributes[n2];
                                    String key2;
                                    if (n2 != 4) {
                                        key2 = "ImageWidth";
                                    }
                                    else {
                                        key2 = "ThumbnailImageWidth";
                                    }
                                    hashMap2.put(key2, ExifAttribute.createULong(obj.readUnsignedShort(), this.mExifByteOrder));
                                    n6 = n4 - 5;
                                    break;
                                }
                            }
                        }
                        n7 = n6;
                        break Label_0764;
                    }
                    final byte[] array = new byte[n4];
                    if (obj.read(array) != n4) {
                        throw new IOException("Invalid exif");
                    }
                    if (this.getAttribute("UserComment") == null) {
                        this.mAttributes[1].put("UserComment", ExifAttribute.createString(new String(array, ExifInterface.ASCII)));
                    }
                    n8 = n5;
                }
                else {
                    final byte[] array2 = new byte[n4];
                    obj.readFully(array2);
                    final byte[] identifier_EXIF_APP1 = ExifInterface.IDENTIFIER_EXIF_APP1;
                    if (ExifInterfaceUtils.startsWith(array2, identifier_EXIF_APP1)) {
                        final byte[] copyOfRange = Arrays.copyOfRange(array2, identifier_EXIF_APP1.length, n4);
                        this.mOffsetToExifData = n + n5 + identifier_EXIF_APP1.length;
                        this.readExifSegment(copyOfRange, n2);
                        this.setThumbnailData(new ByteOrderedDataInputStream(copyOfRange));
                    }
                    else {
                        final byte[] identifier_XMP_APP1 = ExifInterface.IDENTIFIER_XMP_APP1;
                        if (ExifInterfaceUtils.startsWith(array2, identifier_XMP_APP1)) {
                            final int length = identifier_XMP_APP1.length;
                            final byte[] copyOfRange2 = Arrays.copyOfRange(array2, identifier_XMP_APP1.length, n4);
                            if (this.getAttribute("Xmp") == null) {
                                this.mAttributes[0].put("Xmp", new ExifAttribute(1, copyOfRange2.length, n5 + length, copyOfRange2));
                                this.mXmpIsFromSeparateMarker = true;
                            }
                        }
                    }
                    n8 = n5 + n4;
                }
                n7 = 0;
                n5 = n8;
            }
            if (n7 < 0) {
                throw new IOException("Invalid length");
            }
            obj.skipFully(n7);
            n3 = n5 + n7;
        }
    }
    
    private int getMimeType(final BufferedInputStream bufferedInputStream) throws IOException {
        bufferedInputStream.mark(5000);
        final byte[] b = new byte[5000];
        bufferedInputStream.read(b);
        bufferedInputStream.reset();
        if (isJpegFormat(b)) {
            return 4;
        }
        if (this.isRafFormat(b)) {
            return 9;
        }
        if (this.isHeifFormat(b)) {
            return 12;
        }
        if (this.isOrfFormat(b)) {
            return 7;
        }
        if (this.isRw2Format(b)) {
            return 10;
        }
        if (this.isPngFormat(b)) {
            return 13;
        }
        if (this.isWebpFormat(b)) {
            return 14;
        }
        return 0;
    }
    
    private void getOrfAttributes(SeekableByteOrderedDataInputStream seekableByteOrderedDataInputStream) throws IOException {
        this.getRawAttributes(seekableByteOrderedDataInputStream);
        final ExifAttribute exifAttribute = this.mAttributes[1].get("MakerNote");
        if (exifAttribute != null) {
            seekableByteOrderedDataInputStream = new SeekableByteOrderedDataInputStream(exifAttribute.bytes);
            ((ByteOrderedDataInputStream)seekableByteOrderedDataInputStream).setByteOrder(this.mExifByteOrder);
            final byte[] orf_MAKER_NOTE_HEADER_1 = ExifInterface.ORF_MAKER_NOTE_HEADER_1;
            final byte[] a = new byte[orf_MAKER_NOTE_HEADER_1.length];
            ((ByteOrderedDataInputStream)seekableByteOrderedDataInputStream).readFully(a);
            seekableByteOrderedDataInputStream.seek(0L);
            final byte[] orf_MAKER_NOTE_HEADER_2 = ExifInterface.ORF_MAKER_NOTE_HEADER_2;
            final byte[] a2 = new byte[orf_MAKER_NOTE_HEADER_2.length];
            ((ByteOrderedDataInputStream)seekableByteOrderedDataInputStream).readFully(a2);
            if (Arrays.equals(a, orf_MAKER_NOTE_HEADER_1)) {
                seekableByteOrderedDataInputStream.seek(8L);
            }
            else if (Arrays.equals(a2, orf_MAKER_NOTE_HEADER_2)) {
                seekableByteOrderedDataInputStream.seek(12L);
            }
            this.readImageFileDirectory(seekableByteOrderedDataInputStream, 6);
            final ExifAttribute value = this.mAttributes[7].get("PreviewImageStart");
            final ExifAttribute value2 = this.mAttributes[7].get("PreviewImageLength");
            if (value != null && value2 != null) {
                this.mAttributes[5].put("JPEGInterchangeFormat", value);
                this.mAttributes[5].put("JPEGInterchangeFormatLength", value2);
            }
            final ExifAttribute exifAttribute2 = this.mAttributes[8].get("AspectFrame");
            if (exifAttribute2 != null) {
                final int[] a3 = (int[])exifAttribute2.getValue(this.mExifByteOrder);
                if (a3 != null && a3.length == 4) {
                    final int n = a3[2];
                    final int n2 = a3[0];
                    if (n > n2) {
                        final int n3 = a3[3];
                        final int n4 = a3[1];
                        if (n3 > n4) {
                            final int n5 = n - n2 + 1;
                            final int n6 = n3 - n4 + 1;
                            int n7;
                            int n8;
                            if ((n7 = n5) < (n8 = n6)) {
                                final int n9 = n5 + n6;
                                n8 = n9 - n6;
                                n7 = n9 - n8;
                            }
                            final ExifAttribute uShort = ExifAttribute.createUShort(n7, this.mExifByteOrder);
                            final ExifAttribute uShort2 = ExifAttribute.createUShort(n8, this.mExifByteOrder);
                            this.mAttributes[0].put("ImageWidth", uShort);
                            this.mAttributes[0].put("ImageLength", uShort2);
                        }
                    }
                }
                else {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Invalid aspect frame values. frame=");
                    sb.append(Arrays.toString(a3));
                    Log.w("ExifInterface", sb.toString());
                }
            }
        }
    }
    
    private void getPngAttributes(final ByteOrderedDataInputStream obj) throws IOException {
        if (ExifInterface.DEBUG) {
            final StringBuilder sb = new StringBuilder();
            sb.append("getPngAttributes starting with: ");
            sb.append(obj);
            Log.d("ExifInterface", sb.toString());
        }
        obj.setByteOrder(ByteOrder.BIG_ENDIAN);
        final byte[] png_SIGNATURE = ExifInterface.PNG_SIGNATURE;
        obj.skipFully(png_SIGNATURE.length);
        int n = png_SIGNATURE.length + 0;
        try {
            while (true) {
                int int1 = obj.readInt();
                final byte[] b = new byte[4];
                Label_0356: {
                    if (obj.read(b) != 4) {
                        break Label_0356;
                    }
                    final int mOffsetToExifData = n + 4 + 4;
                    if (mOffsetToExifData == 16 && !Arrays.equals(b, ExifInterface.PNG_CHUNK_TYPE_IHDR)) {
                        throw new IOException("Encountered invalid PNG file--IHDR chunk should appearas the first chunk");
                    }
                    if (Arrays.equals(b, ExifInterface.PNG_CHUNK_TYPE_IEND)) {
                        break;
                    }
                    if (Arrays.equals(b, ExifInterface.PNG_CHUNK_TYPE_EXIF)) {
                        final byte[] array = new byte[int1];
                        if (obj.read(array) != int1) {
                            final StringBuilder sb2 = new StringBuilder();
                            sb2.append("Failed to read given length for given PNG chunk type: ");
                            sb2.append(ExifInterfaceUtils.byteArrayToHexString(b));
                            throw new IOException(sb2.toString());
                        }
                        final int int2 = obj.readInt();
                        final CRC32 crc32 = new CRC32();
                        crc32.update(b);
                        crc32.update(array);
                        if ((int)crc32.getValue() == int2) {
                            this.mOffsetToExifData = mOffsetToExifData;
                            this.readExifSegment(array, 0);
                            this.validateImages();
                            this.setThumbnailData(new ByteOrderedDataInputStream(array));
                            break;
                        }
                        final StringBuilder sb3 = new StringBuilder();
                        sb3.append("Encountered invalid CRC value for PNG-EXIF chunk.\n recorded CRC value: ");
                        sb3.append(int2);
                        sb3.append(", calculated CRC value: ");
                        sb3.append(crc32.getValue());
                        throw new IOException(sb3.toString());
                    }
                    int1 += 4;
                    try {
                        obj.skipFully(int1);
                        n = mOffsetToExifData + int1;
                        continue;
                        throw new IOException("Encountered invalid length while parsing PNG chunktype");
                    }
                    catch (final EOFException ex) {
                        throw new IOException("Encountered corrupt PNG file.");
                    }
                }
            }
        }
        catch (final EOFException ex2) {}
    }
    
    private void getRafAttributes(final ByteOrderedDataInputStream obj) throws IOException {
        final boolean debug = ExifInterface.DEBUG;
        if (debug) {
            final StringBuilder sb = new StringBuilder();
            sb.append("getRafAttributes starting with: ");
            sb.append(obj);
            Log.d("ExifInterface", sb.toString());
        }
        obj.skipFully(84);
        final byte[] array = new byte[4];
        final byte[] array2 = new byte[4];
        final byte[] array3 = new byte[4];
        obj.read(array);
        obj.read(array2);
        obj.read(array3);
        final int int1 = ByteBuffer.wrap(array).getInt();
        final int int2 = ByteBuffer.wrap(array2).getInt();
        final int int3 = ByteBuffer.wrap(array3).getInt();
        final byte[] b = new byte[int2];
        obj.skipFully(int1 - obj.position());
        obj.read(b);
        this.getJpegAttributes(new ByteOrderedDataInputStream(b), int1, 5);
        obj.skipFully(int3 - obj.position());
        obj.setByteOrder(ByteOrder.BIG_ENDIAN);
        final int int4 = obj.readInt();
        if (debug) {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("numberOfDirectoryEntry: ");
            sb2.append(int4);
            Log.d("ExifInterface", sb2.toString());
        }
        for (int i = 0; i < int4; ++i) {
            final int unsignedShort = obj.readUnsignedShort();
            final int unsignedShort2 = obj.readUnsignedShort();
            if (unsignedShort == ExifInterface.TAG_RAF_IMAGE_SIZE.number) {
                final short short1 = obj.readShort();
                final short short2 = obj.readShort();
                final ExifAttribute uShort = ExifAttribute.createUShort(short1, this.mExifByteOrder);
                final ExifAttribute uShort2 = ExifAttribute.createUShort(short2, this.mExifByteOrder);
                this.mAttributes[0].put("ImageLength", uShort);
                this.mAttributes[0].put("ImageWidth", uShort2);
                if (ExifInterface.DEBUG) {
                    final StringBuilder sb3 = new StringBuilder();
                    sb3.append("Updated to length: ");
                    sb3.append(short1);
                    sb3.append(", width: ");
                    sb3.append(short2);
                    Log.d("ExifInterface", sb3.toString());
                }
                return;
            }
            obj.skipFully(unsignedShort2);
        }
    }
    
    private void getRawAttributes(SeekableByteOrderedDataInputStream seekableByteOrderedDataInputStream) throws IOException {
        this.parseTiffHeaders((ByteOrderedDataInputStream)seekableByteOrderedDataInputStream);
        this.readImageFileDirectory(seekableByteOrderedDataInputStream, 0);
        this.updateImageSizeValues(seekableByteOrderedDataInputStream, 0);
        this.updateImageSizeValues(seekableByteOrderedDataInputStream, 5);
        this.updateImageSizeValues(seekableByteOrderedDataInputStream, 4);
        this.validateImages();
        if (this.mMimeType == 8) {
            final ExifAttribute exifAttribute = this.mAttributes[1].get("MakerNote");
            if (exifAttribute != null) {
                seekableByteOrderedDataInputStream = new SeekableByteOrderedDataInputStream(exifAttribute.bytes);
                ((ByteOrderedDataInputStream)seekableByteOrderedDataInputStream).setByteOrder(this.mExifByteOrder);
                ((ByteOrderedDataInputStream)seekableByteOrderedDataInputStream).skipFully(6);
                this.readImageFileDirectory(seekableByteOrderedDataInputStream, 9);
                final ExifAttribute value = this.mAttributes[9].get("ColorSpace");
                if (value != null) {
                    this.mAttributes[1].put("ColorSpace", value);
                }
            }
        }
    }
    
    private void getRw2Attributes(final SeekableByteOrderedDataInputStream obj) throws IOException {
        if (ExifInterface.DEBUG) {
            final StringBuilder sb = new StringBuilder();
            sb.append("getRw2Attributes starting with: ");
            sb.append(obj);
            Log.d("ExifInterface", sb.toString());
        }
        this.getRawAttributes(obj);
        final ExifAttribute exifAttribute = this.mAttributes[0].get("JpgFromRaw");
        if (exifAttribute != null) {
            this.getJpegAttributes(new ByteOrderedDataInputStream(exifAttribute.bytes), (int)exifAttribute.bytesOffset, 5);
        }
        final ExifAttribute value = this.mAttributes[0].get("ISO");
        final ExifAttribute exifAttribute2 = this.mAttributes[1].get("PhotographicSensitivity");
        if (value != null && exifAttribute2 == null) {
            this.mAttributes[1].put("PhotographicSensitivity", value);
        }
    }
    
    private void getStandaloneAttributes(final SeekableByteOrderedDataInputStream seekableByteOrderedDataInputStream) throws IOException {
        final byte[] identifier_EXIF_APP1 = ExifInterface.IDENTIFIER_EXIF_APP1;
        ((ByteOrderedDataInputStream)seekableByteOrderedDataInputStream).skipFully(identifier_EXIF_APP1.length);
        final byte[] array = new byte[((ByteOrderedDataInputStream)seekableByteOrderedDataInputStream).available()];
        ((ByteOrderedDataInputStream)seekableByteOrderedDataInputStream).readFully(array);
        this.mOffsetToExifData = identifier_EXIF_APP1.length;
        this.readExifSegment(array, 0);
    }
    
    private void getWebpAttributes(final ByteOrderedDataInputStream obj) throws IOException {
        if (ExifInterface.DEBUG) {
            final StringBuilder sb = new StringBuilder();
            sb.append("getWebpAttributes starting with: ");
            sb.append(obj);
            Log.d("ExifInterface", sb.toString());
        }
        obj.setByteOrder(ByteOrder.LITTLE_ENDIAN);
        obj.skipFully(ExifInterface.WEBP_SIGNATURE_1.length);
        final int n = obj.readInt() + 8;
        final byte[] webp_SIGNATURE_2 = ExifInterface.WEBP_SIGNATURE_2;
        obj.skipFully(webp_SIGNATURE_2.length);
        int n2 = webp_SIGNATURE_2.length + 8;
        try {
            while (true) {
                final byte[] array = new byte[4];
                if (obj.read(array) != 4) {
                    throw new IOException("Encountered invalid length while parsing WebP chunktype");
                }
                final int int1 = obj.readInt();
                final int mOffsetToExifData = n2 + 4 + 4;
                if (Arrays.equals(ExifInterface.WEBP_CHUNK_TYPE_EXIF, array)) {
                    final byte[] b = new byte[int1];
                    if (obj.read(b) == int1) {
                        this.mOffsetToExifData = mOffsetToExifData;
                        this.readExifSegment(b, 0);
                        this.setThumbnailData(new ByteOrderedDataInputStream(b));
                        break;
                    }
                    final StringBuilder sb2 = new StringBuilder();
                    sb2.append("Failed to read given length for given PNG chunk type: ");
                    sb2.append(ExifInterfaceUtils.byteArrayToHexString(array));
                    throw new IOException(sb2.toString());
                }
                else {
                    int n3 = int1;
                    if (int1 % 2 == 1) {
                        n3 = int1 + 1;
                    }
                    final int n4 = mOffsetToExifData + n3;
                    if (n4 == n) {
                        break;
                    }
                    if (n4 > n) {
                        throw new IOException("Encountered WebP file with invalid chunk size");
                    }
                    obj.skipFully(n3);
                    n2 = n4;
                }
            }
        }
        catch (final EOFException ex) {
            throw new IOException("Encountered corrupt WebP file.");
        }
    }
    
    private static Pair<Integer, Integer> guessDataFormat(final String s) {
        final boolean contains = s.contains(",");
        int i = 1;
        final Integer value = 2;
        final Integer value2 = -1;
        if (contains) {
            final String[] split = s.split(",", -1);
            Pair guessDataFormat;
            final Pair<Integer, Integer> pair = (Pair<Integer, Integer>)(guessDataFormat = guessDataFormat(split[0]));
            if ((int)pair.first == 2) {
                return pair;
            }
            while (i < split.length) {
                final Pair<Integer, Integer> guessDataFormat2 = guessDataFormat(split[i]);
                int intValue;
                if (!((Integer)guessDataFormat2.first).equals(guessDataFormat.first) && !((Integer)guessDataFormat2.second).equals(guessDataFormat.first)) {
                    intValue = -1;
                }
                else {
                    intValue = (int)guessDataFormat.first;
                }
                int intValue2;
                if ((int)guessDataFormat.second != -1 && (((Integer)guessDataFormat2.first).equals(guessDataFormat.second) || ((Integer)guessDataFormat2.second).equals(guessDataFormat.second))) {
                    intValue2 = (int)guessDataFormat.second;
                }
                else {
                    intValue2 = -1;
                }
                if (intValue == -1 && intValue2 == -1) {
                    return (Pair<Integer, Integer>)new Pair((Object)value, (Object)value2);
                }
                if (intValue == -1) {
                    guessDataFormat = new Pair((Object)intValue2, (Object)value2);
                }
                else if (intValue2 == -1) {
                    guessDataFormat = new Pair((Object)intValue, (Object)value2);
                }
                ++i;
            }
            return (Pair<Integer, Integer>)guessDataFormat;
        }
        else {
            Label_0417: {
                if (!s.contains("/")) {
                    break Label_0417;
                }
                final String[] split2 = s.split("/", -1);
                Label_0405: {
                    if (split2.length != 2) {
                        break Label_0405;
                    }
                    try {
                        final long n = (long)Double.parseDouble(split2[0]);
                        final long n2 = (long)Double.parseDouble(split2[1]);
                        if (n < 0L || n2 < 0L) {
                            return (Pair<Integer, Integer>)new Pair((Object)10, (Object)value2);
                        }
                        if (n <= 2147483647L && n2 <= 2147483647L) {
                            return (Pair<Integer, Integer>)new Pair((Object)10, (Object)5);
                        }
                        return (Pair<Integer, Integer>)new Pair((Object)5, (Object)value2);
                        return (Pair<Integer, Integer>)new Pair((Object)value, (Object)value2);
                        try {
                            final Long value3 = Long.parseLong(s);
                            if (value3 >= 0L && value3 <= 65535L) {
                                return (Pair<Integer, Integer>)new Pair((Object)3, (Object)4);
                            }
                            if (value3 < 0L) {
                                return (Pair<Integer, Integer>)new Pair((Object)9, (Object)value2);
                            }
                            return (Pair<Integer, Integer>)new Pair((Object)4, (Object)value2);
                        }
                        catch (final NumberFormatException ex) {
                            try {
                                Double.parseDouble(s);
                                return (Pair<Integer, Integer>)new Pair((Object)12, (Object)value2);
                            }
                            catch (final NumberFormatException ex2) {
                                return (Pair<Integer, Integer>)new Pair((Object)value, (Object)value2);
                            }
                        }
                    }
                    catch (final NumberFormatException ex3) {
                        return (Pair<Integer, Integer>)new Pair((Object)value, (Object)value2);
                    }
                }
            }
        }
    }
    
    private void handleThumbnailFromJfif(final ByteOrderedDataInputStream byteOrderedDataInputStream, final HashMap hashMap) throws IOException {
        final ExifAttribute exifAttribute = hashMap.get("JPEGInterchangeFormat");
        final ExifAttribute exifAttribute2 = hashMap.get("JPEGInterchangeFormatLength");
        if (exifAttribute != null && exifAttribute2 != null) {
            final int intValue = exifAttribute.getIntValue(this.mExifByteOrder);
            final int intValue2 = exifAttribute2.getIntValue(this.mExifByteOrder);
            int n = intValue;
            if (this.mMimeType == 7) {
                n = intValue + this.mOrfMakerNoteOffset;
            }
            if (n > 0 && intValue2 > 0) {
                this.mHasThumbnail = true;
                if (this.mFilename == null && this.mAssetInputStream == null && this.mSeekableFileDescriptor == null) {
                    final byte[] array = new byte[intValue2];
                    byteOrderedDataInputStream.skip(n);
                    byteOrderedDataInputStream.read(array);
                    this.mThumbnailBytes = array;
                }
                this.mThumbnailOffset = n;
                this.mThumbnailLength = intValue2;
            }
            if (ExifInterface.DEBUG) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Setting thumbnail attributes with offset: ");
                sb.append(n);
                sb.append(", length: ");
                sb.append(intValue2);
                Log.d("ExifInterface", sb.toString());
            }
        }
    }
    
    private void handleThumbnailFromStrips(final ByteOrderedDataInputStream byteOrderedDataInputStream, final HashMap hashMap) throws IOException {
        final ExifAttribute exifAttribute = hashMap.get("StripOffsets");
        final ExifAttribute exifAttribute2 = hashMap.get("StripByteCounts");
        if (exifAttribute != null && exifAttribute2 != null) {
            final long[] convertToLongArray = ExifInterfaceUtils.convertToLongArray(exifAttribute.getValue(this.mExifByteOrder));
            final long[] convertToLongArray2 = ExifInterfaceUtils.convertToLongArray(exifAttribute2.getValue(this.mExifByteOrder));
            if (convertToLongArray != null && convertToLongArray.length != 0) {
                if (convertToLongArray2 == null || convertToLongArray2.length == 0) {
                    Log.w("ExifInterface", "stripByteCounts should not be null or have zero length.");
                    return;
                }
                if (convertToLongArray.length != convertToLongArray2.length) {
                    Log.w("ExifInterface", "stripOffsets and stripByteCounts should have same length.");
                    return;
                }
                long n = 0L;
                for (int length = convertToLongArray2.length, i = 0; i < length; ++i) {
                    n += convertToLongArray2[i];
                }
                final int mThumbnailLength = (int)n;
                final byte[] mThumbnailBytes = new byte[mThumbnailLength];
                this.mAreThumbnailStripsConsecutive = true;
                this.mHasThumbnailStrips = true;
                this.mHasThumbnail = true;
                int j = 0;
                int n2 = 0;
                int n3 = 0;
                while (j < convertToLongArray.length) {
                    final int n4 = (int)convertToLongArray[j];
                    final int k = (int)convertToLongArray2[j];
                    if (j < convertToLongArray.length - 1 && n4 + k != convertToLongArray[j + 1]) {
                        this.mAreThumbnailStripsConsecutive = false;
                    }
                    final int l = n4 - n2;
                    if (l < 0) {
                        Log.d("ExifInterface", "Invalid strip offset value");
                        return;
                    }
                    final long n5 = l;
                    if (byteOrderedDataInputStream.skip(n5) != n5) {
                        final StringBuilder sb = new StringBuilder();
                        sb.append("Failed to skip ");
                        sb.append(l);
                        sb.append(" bytes.");
                        Log.d("ExifInterface", sb.toString());
                        return;
                    }
                    final byte[] b = new byte[k];
                    if (byteOrderedDataInputStream.read(b) != k) {
                        final StringBuilder sb2 = new StringBuilder();
                        sb2.append("Failed to read ");
                        sb2.append(k);
                        sb2.append(" bytes.");
                        Log.d("ExifInterface", sb2.toString());
                        return;
                    }
                    n2 = n2 + l + k;
                    System.arraycopy(b, 0, mThumbnailBytes, n3, k);
                    n3 += k;
                    ++j;
                }
                this.mThumbnailBytes = mThumbnailBytes;
                if (this.mAreThumbnailStripsConsecutive) {
                    this.mThumbnailOffset = (int)convertToLongArray[0];
                    this.mThumbnailLength = mThumbnailLength;
                }
            }
            else {
                Log.w("ExifInterface", "stripOffsets should not be null or have zero length.");
            }
        }
    }
    
    private void initForFilename(final String s) throws IOException {
        if (s == null) {
            throw new NullPointerException("filename cannot be null");
        }
        Closeable closeable = null;
        this.mAssetInputStream = null;
        this.mFilename = s;
        try {
            final FileInputStream fileInputStream = new FileInputStream(s);
            try {
                if (isSeekableFD(fileInputStream.getFD())) {
                    this.mSeekableFileDescriptor = fileInputStream.getFD();
                }
                else {
                    this.mSeekableFileDescriptor = null;
                }
                this.loadAttributes(fileInputStream);
                ExifInterfaceUtils.closeQuietly(fileInputStream);
                return;
            }
            finally {
                closeable = fileInputStream;
            }
        }
        finally {}
        ExifInterfaceUtils.closeQuietly(closeable);
    }
    
    private static boolean isExifDataOnly(final BufferedInputStream bufferedInputStream) throws IOException {
        final byte[] identifier_EXIF_APP1 = ExifInterface.IDENTIFIER_EXIF_APP1;
        bufferedInputStream.mark(identifier_EXIF_APP1.length);
        final byte[] b = new byte[identifier_EXIF_APP1.length];
        bufferedInputStream.read(b);
        bufferedInputStream.reset();
        int n = 0;
        while (true) {
            final byte[] identifier_EXIF_APP2 = ExifInterface.IDENTIFIER_EXIF_APP1;
            if (n >= identifier_EXIF_APP2.length) {
                return true;
            }
            if (b[n] != identifier_EXIF_APP2[n]) {
                return false;
            }
            ++n;
        }
    }
    
    private boolean isHeifFormat(byte[] a) throws IOException {
        final InputStream inputStream = null;
        Object o2;
        final Object o = o2 = null;
        ByteOrderedDataInputStream byteOrderedDataInputStream = null;
        InputStream inputStream2;
        try {
            try {
                o2 = o;
                byteOrderedDataInputStream = new ByteOrderedDataInputStream(a);
                try {
                    long long1 = byteOrderedDataInputStream.readInt();
                    o2 = new byte[4];
                    byteOrderedDataInputStream.read((byte[])o2);
                    if (!Arrays.equals((byte[])o2, ExifInterface.HEIF_TYPE_FTYP)) {
                        byteOrderedDataInputStream.close();
                        return false;
                    }
                    long n = 16L;
                    if (long1 == 1L) {
                        if ((long1 = byteOrderedDataInputStream.readLong()) < 16L) {
                            byteOrderedDataInputStream.close();
                            return false;
                        }
                    }
                    else {
                        n = 8L;
                    }
                    long n2 = long1;
                    if (long1 > a.length) {
                        n2 = a.length;
                    }
                    final long n3 = n2 - n;
                    if (n3 < 8L) {
                        byteOrderedDataInputStream.close();
                        return false;
                    }
                    a = new byte[4];
                    long n4 = 0L;
                    int n5 = 0;
                    int n6 = 0;
                    while (n4 < n3 / 4L) {
                        if (byteOrderedDataInputStream.read(a) != 4) {
                            byteOrderedDataInputStream.close();
                            return false;
                        }
                        int n7;
                        if (n4 == 1L) {
                            n7 = n6;
                        }
                        else {
                            int n8;
                            if (Arrays.equals(a, ExifInterface.HEIF_BRAND_MIF1)) {
                                n8 = 1;
                            }
                            else {
                                final boolean equals = Arrays.equals(a, ExifInterface.HEIF_BRAND_HEIC);
                                n8 = n5;
                                if (equals) {
                                    n6 = 1;
                                    n8 = n5;
                                }
                            }
                            n5 = n8;
                            n7 = n6;
                            if (n8 != 0) {
                                n5 = n8;
                                if ((n7 = n6) != 0) {
                                    byteOrderedDataInputStream.close();
                                    return true;
                                }
                            }
                        }
                        ++n4;
                        n6 = n7;
                    }
                    byteOrderedDataInputStream.close();
                    return false;
                }
                catch (final Exception o2) {}
                finally {
                    o2 = byteOrderedDataInputStream;
                }
            }
            finally {}
        }
        catch (final Exception byteOrderedDataInputStream) {
            inputStream2 = inputStream;
        }
        if (ExifInterface.DEBUG) {
            Log.d("ExifInterface", "Exception parsing HEIF file type box.", (Throwable)byteOrderedDataInputStream);
        }
        if (inputStream2 != null) {
            inputStream2.close();
        }
        return false;
        if (o2 != null) {
            ((ByteOrderedDataInputStream)o2).close();
        }
    }
    
    private static boolean isJpegFormat(final byte[] array) throws IOException {
        int n = 0;
        while (true) {
            final byte[] jpeg_SIGNATURE = ExifInterface.JPEG_SIGNATURE;
            if (n >= jpeg_SIGNATURE.length) {
                return true;
            }
            if (array[n] != jpeg_SIGNATURE[n]) {
                return false;
            }
            ++n;
        }
    }
    
    private boolean isOrfFormat(final byte[] array) throws IOException {
        boolean b = false;
        final InputStream inputStream = null;
        final InputStream inputStream2 = null;
        InputStream inputStream3 = null;
        try {
            final ByteOrderedDataInputStream byteOrderedDataInputStream = new ByteOrderedDataInputStream(array);
            try {
                byteOrderedDataInputStream.setByteOrder(this.mExifByteOrder = this.readByteOrder(byteOrderedDataInputStream));
                final short short1 = byteOrderedDataInputStream.readShort();
                if (short1 == 20306 || short1 == 21330) {
                    b = true;
                }
                byteOrderedDataInputStream.close();
                return b;
            }
            catch (final Exception ex) {}
        }
        catch (final Exception ex2) {
            inputStream3 = inputStream;
        }
        finally {
            inputStream3 = inputStream2;
        }
        if (inputStream3 != null) {
            inputStream3.close();
        }
        return false;
    }
    
    private boolean isPngFormat(final byte[] array) throws IOException {
        int n = 0;
        while (true) {
            final byte[] png_SIGNATURE = ExifInterface.PNG_SIGNATURE;
            if (n >= png_SIGNATURE.length) {
                return true;
            }
            if (array[n] != png_SIGNATURE[n]) {
                return false;
            }
            ++n;
        }
    }
    
    private boolean isRafFormat(final byte[] array) throws IOException {
        final byte[] bytes = "FUJIFILMCCD-RAW".getBytes(Charset.defaultCharset());
        for (int i = 0; i < bytes.length; ++i) {
            if (array[i] != bytes[i]) {
                return false;
            }
        }
        return true;
    }
    
    private boolean isRw2Format(final byte[] array) throws IOException {
        boolean b = false;
        final InputStream inputStream = null;
        final InputStream inputStream2 = null;
        InputStream inputStream3 = null;
        try {
            final ByteOrderedDataInputStream byteOrderedDataInputStream = new ByteOrderedDataInputStream(array);
            try {
                byteOrderedDataInputStream.setByteOrder(this.mExifByteOrder = this.readByteOrder(byteOrderedDataInputStream));
                if (byteOrderedDataInputStream.readShort() == 85) {
                    b = true;
                }
                byteOrderedDataInputStream.close();
                return b;
            }
            catch (final Exception ex) {}
        }
        catch (final Exception ex2) {
            inputStream3 = inputStream;
        }
        finally {
            inputStream3 = inputStream2;
        }
        if (inputStream3 != null) {
            inputStream3.close();
        }
        return false;
    }
    
    private static boolean isSeekableFD(final FileDescriptor fileDescriptor) {
        if (Build$VERSION.SDK_INT >= 21) {
            try {
                ExifInterfaceUtils.Api21Impl.lseek(fileDescriptor, 0L, OsConstants.SEEK_CUR);
                return true;
            }
            catch (final Exception ex) {
                if (ExifInterface.DEBUG) {
                    Log.d("ExifInterface", "The file descriptor for the given input is not seekable");
                }
            }
        }
        return false;
    }
    
    private boolean isSupportedDataType(final HashMap hashMap) throws IOException {
        final ExifAttribute exifAttribute = hashMap.get("BitsPerSample");
        if (exifAttribute != null) {
            final int[] a = (int[])exifAttribute.getValue(this.mExifByteOrder);
            final int[] bits_PER_SAMPLE_RGB = ExifInterface.BITS_PER_SAMPLE_RGB;
            if (Arrays.equals(bits_PER_SAMPLE_RGB, a)) {
                return true;
            }
            if (this.mMimeType == 3) {
                final ExifAttribute exifAttribute2 = hashMap.get("PhotometricInterpretation");
                if (exifAttribute2 != null) {
                    final int intValue = exifAttribute2.getIntValue(this.mExifByteOrder);
                    if ((intValue == 1 && Arrays.equals(a, ExifInterface.BITS_PER_SAMPLE_GREYSCALE_2)) || (intValue == 6 && Arrays.equals(a, bits_PER_SAMPLE_RGB))) {
                        return true;
                    }
                }
            }
        }
        if (ExifInterface.DEBUG) {
            Log.d("ExifInterface", "Unsupported data type value");
        }
        return false;
    }
    
    private static boolean isSupportedFormatForSavingAttributes(final int n) {
        return n == 4 || n == 13 || n == 14;
    }
    
    public static boolean isSupportedMimeType(String lowerCase) {
        if (lowerCase == null) {
            throw new NullPointerException("mimeType shouldn't be null");
        }
        lowerCase = lowerCase.toLowerCase(Locale.ROOT);
        lowerCase.hashCode();
        int n = -1;
        switch (lowerCase) {
            case "image/x-canon-cr2": {
                n = 14;
                break;
            }
            case "image/x-nikon-nrw": {
                n = 13;
                break;
            }
            case "image/x-nikon-nef": {
                n = 12;
                break;
            }
            case "image/x-olympus-orf": {
                n = 11;
                break;
            }
            case "image/x-pentax-pef": {
                n = 10;
                break;
            }
            case "image/png": {
                n = 9;
                break;
            }
            case "image/x-panasonic-rw2": {
                n = 8;
                break;
            }
            case "image/x-adobe-dng": {
                n = 7;
                break;
            }
            case "image/webp": {
                n = 6;
                break;
            }
            case "image/jpeg": {
                n = 5;
                break;
            }
            case "image/heif": {
                n = 4;
                break;
            }
            case "image/heic": {
                n = 3;
                break;
            }
            case "image/x-sony-arw": {
                n = 2;
                break;
            }
            case "image/x-samsung-srw": {
                n = 1;
                break;
            }
            case "image/x-fuji-raf": {
                n = 0;
                break;
            }
            default:
                break;
        }
        switch (n) {
            default: {
                return false;
            }
            case 0:
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
            case 10:
            case 11:
            case 12:
            case 13:
            case 14: {
                return true;
            }
        }
    }
    
    private boolean isThumbnail(final HashMap hashMap) throws IOException {
        final ExifAttribute exifAttribute = hashMap.get("ImageLength");
        final ExifAttribute exifAttribute2 = hashMap.get("ImageWidth");
        if (exifAttribute != null && exifAttribute2 != null) {
            final int intValue = exifAttribute.getIntValue(this.mExifByteOrder);
            final int intValue2 = exifAttribute2.getIntValue(this.mExifByteOrder);
            if (intValue <= 512 && intValue2 <= 512) {
                return true;
            }
        }
        return false;
    }
    
    private boolean isWebpFormat(final byte[] array) throws IOException {
        int n = 0;
        while (true) {
            final byte[] webp_SIGNATURE_1 = ExifInterface.WEBP_SIGNATURE_1;
            if (n < webp_SIGNATURE_1.length) {
                if (array[n] != webp_SIGNATURE_1[n]) {
                    return false;
                }
                ++n;
            }
            else {
                int n2 = 0;
                while (true) {
                    final byte[] webp_SIGNATURE_2 = ExifInterface.WEBP_SIGNATURE_2;
                    if (n2 >= webp_SIGNATURE_2.length) {
                        return true;
                    }
                    if (array[ExifInterface.WEBP_SIGNATURE_1.length + n2 + 4] != webp_SIGNATURE_2[n2]) {
                        return false;
                    }
                    ++n2;
                }
            }
        }
    }
    
    private void loadAttributes(final InputStream in) {
        if (in != null) {
            int i = 0;
            try {
                Label_0295: {
                    try {
                        while (i < ExifInterface.EXIF_TAGS.length) {
                            this.mAttributes[i] = new HashMap<String, ExifAttribute>();
                            ++i;
                        }
                        InputStream inputStream = in;
                        if (!this.mIsExifDataOnly) {
                            inputStream = new BufferedInputStream(in, 5000);
                            final BufferedInputStream bufferedInputStream = (BufferedInputStream)inputStream;
                            this.mMimeType = this.getMimeType((BufferedInputStream)inputStream);
                        }
                        if (shouldSupportSeek(this.mMimeType)) {
                            final SeekableByteOrderedDataInputStream thumbnailData = new SeekableByteOrderedDataInputStream(inputStream);
                            if (this.mIsExifDataOnly) {
                                this.getStandaloneAttributes(thumbnailData);
                            }
                            else {
                                final int mMimeType = this.mMimeType;
                                if (mMimeType == 12) {
                                    this.getHeifAttributes(thumbnailData);
                                }
                                else if (mMimeType == 7) {
                                    this.getOrfAttributes(thumbnailData);
                                }
                                else if (mMimeType == 10) {
                                    this.getRw2Attributes(thumbnailData);
                                }
                                else {
                                    this.getRawAttributes(thumbnailData);
                                }
                            }
                            thumbnailData.seek(this.mOffsetToExifData);
                            this.setThumbnailData((ByteOrderedDataInputStream)thumbnailData);
                        }
                        else {
                            final ByteOrderedDataInputStream byteOrderedDataInputStream = new ByteOrderedDataInputStream(inputStream);
                            final int mMimeType2 = this.mMimeType;
                            if (mMimeType2 == 4) {
                                this.getJpegAttributes(byteOrderedDataInputStream, 0, 0);
                            }
                            else if (mMimeType2 == 13) {
                                this.getPngAttributes(byteOrderedDataInputStream);
                            }
                            else if (mMimeType2 == 9) {
                                this.getRafAttributes(byteOrderedDataInputStream);
                            }
                            else if (mMimeType2 == 14) {
                                this.getWebpAttributes(byteOrderedDataInputStream);
                            }
                        }
                        this.addDefaultValuesForCompatibility();
                        if (ExifInterface.DEBUG) {
                            break Label_0295;
                        }
                        return;
                    }
                    finally {
                        this.addDefaultValuesForCompatibility();
                        if (ExifInterface.DEBUG) {
                            this.printAttributes();
                        }
                        while (true) {
                            this.addDefaultValuesForCompatibility();
                            final boolean debug;
                            iftrue(Label_0299:)(!debug);
                            break Label_0295;
                            Label_0299: {
                                return;
                            }
                            Log.w("ExifInterface", "Invalid image: ExifInterface got an unsupported image format file(ExifInterface supports JPEG and some RAW image formats only) or a corrupted JPEG file to ExifInterface.", (Throwable)in);
                            continue;
                        }
                        this.printAttributes();
                        return;
                        final boolean debug = ExifInterface.DEBUG;
                        iftrue(Label_0287:)(!debug);
                    }
                }
            }
            catch (final UnsupportedOperationException ex) {}
            catch (final IOException ex2) {}
        }
        throw new NullPointerException("inputstream shouldn't be null");
    }
    
    private static Long parseDateTime(String substring, final String s, final String s2) {
        Label_0235: {
            if (substring == null) {
                break Label_0235;
            }
            if (!ExifInterface.NON_ZERO_TIME_PATTERN.matcher(substring).matches()) {
                break Label_0235;
            }
            final ParsePosition parsePosition = new ParsePosition(0);
            try {
                Date date;
                if ((date = ExifInterface.sFormatterPrimary.parse(substring, parsePosition)) == null && (date = ExifInterface.sFormatterSecondary.parse(substring, parsePosition)) == null) {
                    return null;
                }
                long time;
                final long n = time = date.getTime();
                Label_0212: {
                    if (s2 != null) {
                        int n2 = 1;
                        substring = s2.substring(0, 1);
                        final int int1 = Integer.parseInt(s2.substring(1, 3));
                        final int int2 = Integer.parseInt(s2.substring(4, 6));
                        if (!"+".equals(substring)) {
                            time = n;
                            if (!"-".equals(substring)) {
                                break Label_0212;
                            }
                        }
                        time = n;
                        if (":".equals(s2.substring(3, 4))) {
                            time = n;
                            if (int1 <= 14) {
                                if (!"-".equals(substring)) {
                                    n2 = -1;
                                }
                                time = n + (int1 * 60 + int2) * 60 * 1000 * n2;
                            }
                        }
                    }
                }
                long l = time;
                if (s != null) {
                    l = time + ExifInterfaceUtils.parseSubSeconds(s);
                }
                return l;
                return null;
            }
            catch (final IllegalArgumentException ex) {
                return null;
            }
        }
    }
    
    private void parseTiffHeaders(final ByteOrderedDataInputStream byteOrderedDataInputStream) throws IOException {
        byteOrderedDataInputStream.setByteOrder(this.mExifByteOrder = this.readByteOrder(byteOrderedDataInputStream));
        final int unsignedShort = byteOrderedDataInputStream.readUnsignedShort();
        final int mMimeType = this.mMimeType;
        if (mMimeType != 7 && mMimeType != 10 && unsignedShort != 42) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Invalid start code: ");
            sb.append(Integer.toHexString(unsignedShort));
            throw new IOException(sb.toString());
        }
        int int1 = byteOrderedDataInputStream.readInt();
        if (int1 >= 8) {
            int1 -= 8;
            if (int1 > 0) {
                byteOrderedDataInputStream.skipFully(int1);
            }
            return;
        }
        final StringBuilder sb2 = new StringBuilder();
        sb2.append("Invalid first Ifd offset: ");
        sb2.append(int1);
        throw new IOException(sb2.toString());
    }
    
    private void printAttributes() {
        for (int i = 0; i < this.mAttributes.length; ++i) {
            final StringBuilder sb = new StringBuilder();
            sb.append("The size of tag group[");
            sb.append(i);
            sb.append("]: ");
            sb.append(this.mAttributes[i].size());
            Log.d("ExifInterface", sb.toString());
            for (final Map.Entry<K, ExifAttribute> entry : this.mAttributes[i].entrySet()) {
                final ExifAttribute exifAttribute = entry.getValue();
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("tagName: ");
                sb2.append((String)entry.getKey());
                sb2.append(", tagType: ");
                sb2.append(exifAttribute.toString());
                sb2.append(", tagValue: '");
                sb2.append(exifAttribute.getStringValue(this.mExifByteOrder));
                sb2.append("'");
                Log.d("ExifInterface", sb2.toString());
            }
        }
    }
    
    private ByteOrder readByteOrder(final ByteOrderedDataInputStream byteOrderedDataInputStream) throws IOException {
        final short short1 = byteOrderedDataInputStream.readShort();
        if (short1 == 18761) {
            if (ExifInterface.DEBUG) {
                Log.d("ExifInterface", "readExifSegment: Byte Align II");
            }
            return ByteOrder.LITTLE_ENDIAN;
        }
        if (short1 == 19789) {
            if (ExifInterface.DEBUG) {
                Log.d("ExifInterface", "readExifSegment: Byte Align MM");
            }
            return ByteOrder.BIG_ENDIAN;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Invalid byte order: ");
        sb.append(Integer.toHexString(short1));
        throw new IOException(sb.toString());
    }
    
    private void readExifSegment(final byte[] array, final int n) throws IOException {
        final SeekableByteOrderedDataInputStream seekableByteOrderedDataInputStream = new SeekableByteOrderedDataInputStream(array);
        this.parseTiffHeaders((ByteOrderedDataInputStream)seekableByteOrderedDataInputStream);
        this.readImageFileDirectory(seekableByteOrderedDataInputStream, n);
    }
    
    private void readImageFileDirectory(final SeekableByteOrderedDataInputStream seekableByteOrderedDataInputStream, int int1) throws IOException {
        this.mAttributesOffsets.add(seekableByteOrderedDataInputStream.mPosition);
        final short short1 = ((ByteOrderedDataInputStream)seekableByteOrderedDataInputStream).readShort();
        final boolean debug = ExifInterface.DEBUG;
        final String s = "ExifInterface";
        if (debug) {
            final StringBuilder sb = new StringBuilder();
            sb.append("numberOfDirectoryEntry: ");
            sb.append(short1);
            Log.d("ExifInterface", sb.toString());
        }
        if (short1 <= 0) {
            return;
        }
        for (short n = 0; n < short1; ++n) {
            final int unsignedShort = ((ByteOrderedDataInputStream)seekableByteOrderedDataInputStream).readUnsignedShort();
            int unsignedShort2 = ((ByteOrderedDataInputStream)seekableByteOrderedDataInputStream).readUnsignedShort();
            final int int2 = ((ByteOrderedDataInputStream)seekableByteOrderedDataInputStream).readInt();
            final long n2 = ((ByteOrderedDataInputStream)seekableByteOrderedDataInputStream).position() + 4L;
            final ExifTag exifTag = ExifInterface.sExifTagMapsForReading[int1].get(unsignedShort);
            final boolean debug2 = ExifInterface.DEBUG;
            if (debug2) {
                String name;
                if (exifTag != null) {
                    name = exifTag.name;
                }
                else {
                    name = null;
                }
                Log.d(s, String.format("ifdType: %d, tagNumber: %d, tagName: %s, dataFormat: %d, numberOfComponents: %d", int1, unsignedShort, name, unsignedShort2, int2));
            }
            int primaryFormat = 0;
            int n4 = 0;
            long lng = 0L;
            Label_0539: {
                Label_0529: {
                    Label_0266: {
                        if (exifTag == null) {
                            if (debug2) {
                                final StringBuilder sb2 = new StringBuilder();
                                sb2.append("Skip the tag entry since tag number is not defined: ");
                                sb2.append(unsignedShort);
                                Log.d(s, sb2.toString());
                            }
                        }
                        else {
                            if (unsignedShort2 > 0) {
                                final int[] ifd_FORMAT_BYTES_PER_FORMAT = ExifInterface.IFD_FORMAT_BYTES_PER_FORMAT;
                                if (unsignedShort2 < ifd_FORMAT_BYTES_PER_FORMAT.length) {
                                    if (!exifTag.isFormatCompatible(unsignedShort2)) {
                                        if (debug2) {
                                            final StringBuilder sb3 = new StringBuilder();
                                            sb3.append("Skip the tag entry since data format (");
                                            sb3.append(ExifInterface.IFD_FORMAT_NAMES[unsignedShort2]);
                                            sb3.append(") is unexpected for tag: ");
                                            sb3.append(exifTag.name);
                                            Log.d(s, sb3.toString());
                                        }
                                        break Label_0266;
                                    }
                                    else {
                                        if ((primaryFormat = unsignedShort2) == 7) {
                                            primaryFormat = exifTag.primaryFormat;
                                        }
                                        final long n3 = int2 * (long)ifd_FORMAT_BYTES_PER_FORMAT[primaryFormat];
                                        if (n3 >= 0L && n3 <= 2147483647L) {
                                            n4 = 1;
                                            lng = n3;
                                            break Label_0539;
                                        }
                                        lng = n3;
                                        unsignedShort2 = primaryFormat;
                                        if (debug2) {
                                            final StringBuilder sb4 = new StringBuilder();
                                            sb4.append("Skip the tag entry since the number of components is invalid: ");
                                            sb4.append(int2);
                                            Log.d(s, sb4.toString());
                                            lng = n3;
                                            unsignedShort2 = primaryFormat;
                                        }
                                        break Label_0529;
                                    }
                                }
                            }
                            if (debug2) {
                                final StringBuilder sb5 = new StringBuilder();
                                sb5.append("Skip the tag entry since data format is invalid: ");
                                sb5.append(unsignedShort2);
                                Log.d(s, sb5.toString());
                            }
                        }
                    }
                    lng = 0L;
                }
                final int n5 = 0;
                primaryFormat = unsignedShort2;
                n4 = n5;
            }
            if (n4 == 0) {
                seekableByteOrderedDataInputStream.seek(n2);
            }
            else {
                if (lng > 4L) {
                    final int int3 = ((ByteOrderedDataInputStream)seekableByteOrderedDataInputStream).readInt();
                    if (debug2) {
                        final StringBuilder sb6 = new StringBuilder();
                        sb6.append("seek to data offset: ");
                        sb6.append(int3);
                        Log.d(s, sb6.toString());
                    }
                    if (this.mMimeType == 7) {
                        if ("MakerNote".equals(exifTag.name)) {
                            this.mOrfMakerNoteOffset = int3;
                        }
                        else if (int1 == 6 && "ThumbnailImage".equals(exifTag.name)) {
                            this.mOrfThumbnailOffset = int3;
                            this.mOrfThumbnailLength = int2;
                            final ExifAttribute uShort = ExifAttribute.createUShort(6, this.mExifByteOrder);
                            final ExifAttribute uLong = ExifAttribute.createULong(this.mOrfThumbnailOffset, this.mExifByteOrder);
                            final ExifAttribute uLong2 = ExifAttribute.createULong(this.mOrfThumbnailLength, this.mExifByteOrder);
                            this.mAttributes[4].put("Compression", uShort);
                            this.mAttributes[4].put("JPEGInterchangeFormat", uLong);
                            this.mAttributes[4].put("JPEGInterchangeFormatLength", uLong2);
                        }
                    }
                    seekableByteOrderedDataInputStream.seek(int3);
                }
                final Integer n6 = ExifInterface.sExifPointerTagMap.get(unsignedShort);
                if (debug2) {
                    final StringBuilder sb7 = new StringBuilder();
                    sb7.append("nextIfdType: ");
                    sb7.append(n6);
                    sb7.append(" byteCount: ");
                    sb7.append(lng);
                    Log.d(s, sb7.toString());
                }
                final String s2 = s;
                if (n6 != null) {
                    long unsignedInt = -1L;
                    Label_0935: {
                        int n7;
                        if (primaryFormat != 3) {
                            if (primaryFormat == 4) {
                                unsignedInt = ((ByteOrderedDataInputStream)seekableByteOrderedDataInputStream).readUnsignedInt();
                                break Label_0935;
                            }
                            if (primaryFormat != 8) {
                                if (primaryFormat != 9 && primaryFormat != 13) {
                                    break Label_0935;
                                }
                                n7 = ((ByteOrderedDataInputStream)seekableByteOrderedDataInputStream).readInt();
                            }
                            else {
                                n7 = ((ByteOrderedDataInputStream)seekableByteOrderedDataInputStream).readShort();
                            }
                        }
                        else {
                            n7 = ((ByteOrderedDataInputStream)seekableByteOrderedDataInputStream).readUnsignedShort();
                        }
                        unsignedInt = n7;
                    }
                    if (debug2) {
                        Log.d(s2, String.format("Offset: %d, tagName: %s", unsignedInt, exifTag.name));
                    }
                    if (unsignedInt > 0L) {
                        if (!this.mAttributesOffsets.contains((int)unsignedInt)) {
                            seekableByteOrderedDataInputStream.seek(unsignedInt);
                            this.readImageFileDirectory(seekableByteOrderedDataInputStream, n6);
                        }
                        else if (debug2) {
                            final StringBuilder sb8 = new StringBuilder();
                            sb8.append("Skip jump into the IFD since it has already been read: IfdType ");
                            sb8.append(n6);
                            sb8.append(" (at ");
                            sb8.append(unsignedInt);
                            sb8.append(")");
                            Log.d(s2, sb8.toString());
                        }
                    }
                    else if (debug2) {
                        final StringBuilder sb9 = new StringBuilder();
                        sb9.append("Skip jump into the IFD since its offset is invalid: ");
                        sb9.append(unsignedInt);
                        Log.d(s2, sb9.toString());
                    }
                    seekableByteOrderedDataInputStream.seek(n2);
                }
                else {
                    final int position = ((ByteOrderedDataInputStream)seekableByteOrderedDataInputStream).position();
                    final int mOffsetToExifData = this.mOffsetToExifData;
                    final byte[] array = new byte[(int)lng];
                    ((ByteOrderedDataInputStream)seekableByteOrderedDataInputStream).readFully(array);
                    final ExifAttribute value = new ExifAttribute(primaryFormat, int2, position + mOffsetToExifData, array);
                    this.mAttributes[int1].put(exifTag.name, value);
                    if ("DNGVersion".equals(exifTag.name)) {
                        this.mMimeType = 3;
                    }
                    if ((("Make".equals(exifTag.name) || "Model".equals(exifTag.name)) && value.getStringValue(this.mExifByteOrder).contains("PENTAX")) || ("Compression".equals(exifTag.name) && value.getIntValue(this.mExifByteOrder) == 65535)) {
                        this.mMimeType = 8;
                    }
                    if (((ByteOrderedDataInputStream)seekableByteOrderedDataInputStream).position() != n2) {
                        seekableByteOrderedDataInputStream.seek(n2);
                    }
                }
            }
        }
        int1 = ((ByteOrderedDataInputStream)seekableByteOrderedDataInputStream).readInt();
        final boolean debug3 = ExifInterface.DEBUG;
        if (debug3) {
            Log.d(s, String.format("nextIfdOffset: %d", int1));
        }
        final long n8 = int1;
        if (n8 > 0L) {
            if (!this.mAttributesOffsets.contains(int1)) {
                seekableByteOrderedDataInputStream.seek(n8);
                if (this.mAttributes[4].isEmpty()) {
                    this.readImageFileDirectory(seekableByteOrderedDataInputStream, 4);
                }
                else if (this.mAttributes[5].isEmpty()) {
                    this.readImageFileDirectory(seekableByteOrderedDataInputStream, 5);
                }
            }
            else if (debug3) {
                final StringBuilder sb10 = new StringBuilder();
                sb10.append("Stop reading file since re-reading an IFD may cause an infinite loop: ");
                sb10.append(int1);
                Log.d(s, sb10.toString());
            }
        }
        else if (debug3) {
            final StringBuilder sb11 = new StringBuilder();
            sb11.append("Stop reading file since a wrong offset may cause an infinite loop: ");
            sb11.append(int1);
            Log.d(s, sb11.toString());
        }
    }
    
    private void removeAttribute(final String key) {
        for (int i = 0; i < ExifInterface.EXIF_TAGS.length; ++i) {
            this.mAttributes[i].remove(key);
        }
    }
    
    private void replaceInvalidTags(final int n, final String key, final String key2) {
        if (!this.mAttributes[n].isEmpty() && this.mAttributes[n].get(key) != null) {
            final HashMap<String, ExifAttribute> hashMap = this.mAttributes[n];
            hashMap.put(key2, hashMap.get(key));
            this.mAttributes[n].remove(key);
        }
    }
    
    private void retrieveJpegImageSize(final SeekableByteOrderedDataInputStream seekableByteOrderedDataInputStream, final int n) throws IOException {
        final ExifAttribute exifAttribute = this.mAttributes[n].get("ImageLength");
        final ExifAttribute exifAttribute2 = this.mAttributes[n].get("ImageWidth");
        if (exifAttribute == null || exifAttribute2 == null) {
            final ExifAttribute exifAttribute3 = this.mAttributes[n].get("JPEGInterchangeFormat");
            final ExifAttribute exifAttribute4 = this.mAttributes[n].get("JPEGInterchangeFormatLength");
            if (exifAttribute3 != null && exifAttribute4 != null) {
                final int intValue = exifAttribute3.getIntValue(this.mExifByteOrder);
                final int intValue2 = exifAttribute3.getIntValue(this.mExifByteOrder);
                seekableByteOrderedDataInputStream.seek(intValue);
                final byte[] b = new byte[intValue2];
                seekableByteOrderedDataInputStream.read(b);
                this.getJpegAttributes(new ByteOrderedDataInputStream(b), intValue, n);
            }
        }
    }
    
    private void saveJpegAttributes(final InputStream obj, final OutputStream obj2) throws IOException {
        if (ExifInterface.DEBUG) {
            final StringBuilder sb = new StringBuilder();
            sb.append("saveJpegAttributes starting with (inputStream: ");
            sb.append(obj);
            sb.append(", outputStream: ");
            sb.append(obj2);
            sb.append(")");
            Log.d("ExifInterface", sb.toString());
        }
        final ByteOrderedDataInputStream byteOrderedDataInputStream = new ByteOrderedDataInputStream(obj);
        final ByteOrderedDataOutputStream byteOrderedDataOutputStream = new ByteOrderedDataOutputStream(obj2, ByteOrder.BIG_ENDIAN);
        if (byteOrderedDataInputStream.readByte() != -1) {
            throw new IOException("Invalid marker");
        }
        byteOrderedDataOutputStream.writeByte(-1);
        if (byteOrderedDataInputStream.readByte() == -40) {
            byteOrderedDataOutputStream.writeByte(-40);
            ExifAttribute value;
            final ExifAttribute exifAttribute = value = null;
            if (this.getAttribute("Xmp") != null) {
                value = exifAttribute;
                if (this.mXmpIsFromSeparateMarker) {
                    value = this.mAttributes[0].remove("Xmp");
                }
            }
            byteOrderedDataOutputStream.writeByte(-1);
            byteOrderedDataOutputStream.writeByte(-31);
            this.writeExifSegment(byteOrderedDataOutputStream);
            if (value != null) {
                this.mAttributes[0].put("Xmp", value);
            }
            final byte[] array = new byte[4096];
            while (byteOrderedDataInputStream.readByte() == -1) {
                final byte byte1 = byteOrderedDataInputStream.readByte();
                if (byte1 == -39 || byte1 == -38) {
                    byteOrderedDataOutputStream.writeByte(-1);
                    byteOrderedDataOutputStream.writeByte(byte1);
                    ExifInterfaceUtils.copy(byteOrderedDataInputStream, byteOrderedDataOutputStream);
                    return;
                }
                if (byte1 != -31) {
                    byteOrderedDataOutputStream.writeByte(-1);
                    byteOrderedDataOutputStream.writeByte(byte1);
                    int i = byteOrderedDataInputStream.readUnsignedShort();
                    byteOrderedDataOutputStream.writeUnsignedShort(i);
                    i -= 2;
                    if (i < 0) {
                        throw new IOException("Invalid length");
                    }
                    while (i > 0) {
                        final int read = byteOrderedDataInputStream.read(array, 0, Math.min(i, 4096));
                        if (read < 0) {
                            break;
                        }
                        byteOrderedDataOutputStream.write(array, 0, read);
                        i -= read;
                    }
                }
                else {
                    final int n = byteOrderedDataInputStream.readUnsignedShort() - 2;
                    if (n < 0) {
                        throw new IOException("Invalid length");
                    }
                    final byte[] array2 = new byte[6];
                    if (n >= 6) {
                        if (byteOrderedDataInputStream.read(array2) != 6) {
                            throw new IOException("Invalid exif");
                        }
                        if (Arrays.equals(array2, ExifInterface.IDENTIFIER_EXIF_APP1)) {
                            byteOrderedDataInputStream.skipFully(n - 6);
                            continue;
                        }
                    }
                    byteOrderedDataOutputStream.writeByte(-1);
                    byteOrderedDataOutputStream.writeByte(byte1);
                    byteOrderedDataOutputStream.writeUnsignedShort(n + 2);
                    int j;
                    if ((j = n) >= 6) {
                        j = n - 6;
                        byteOrderedDataOutputStream.write(array2);
                    }
                    while (j > 0) {
                        final int read2 = byteOrderedDataInputStream.read(array, 0, Math.min(j, 4096));
                        if (read2 < 0) {
                            break;
                        }
                        byteOrderedDataOutputStream.write(array, 0, read2);
                        j -= read2;
                    }
                }
            }
            throw new IOException("Invalid marker");
        }
        throw new IOException("Invalid marker");
    }
    
    private void savePngAttributes(final InputStream obj, final OutputStream obj2) throws IOException {
        if (ExifInterface.DEBUG) {
            final StringBuilder sb = new StringBuilder();
            sb.append("savePngAttributes starting with (inputStream: ");
            sb.append(obj);
            sb.append(", outputStream: ");
            sb.append(obj2);
            sb.append(")");
            Log.d("ExifInterface", sb.toString());
        }
        final ByteOrderedDataInputStream byteOrderedDataInputStream = new ByteOrderedDataInputStream(obj);
        final ByteOrderedDataOutputStream byteOrderedDataOutputStream = new ByteOrderedDataOutputStream(obj2, ByteOrder.BIG_ENDIAN);
        final byte[] png_SIGNATURE = ExifInterface.PNG_SIGNATURE;
        ExifInterfaceUtils.copy(byteOrderedDataInputStream, byteOrderedDataOutputStream, png_SIGNATURE.length);
        final int mOffsetToExifData = this.mOffsetToExifData;
        if (mOffsetToExifData == 0) {
            final int int1 = byteOrderedDataInputStream.readInt();
            byteOrderedDataOutputStream.writeInt(int1);
            ExifInterfaceUtils.copy(byteOrderedDataInputStream, byteOrderedDataOutputStream, int1 + 4 + 4);
        }
        else {
            ExifInterfaceUtils.copy(byteOrderedDataInputStream, byteOrderedDataOutputStream, mOffsetToExifData - png_SIGNATURE.length - 4 - 4);
            byteOrderedDataInputStream.skipFully(byteOrderedDataInputStream.readInt() + 4 + 4);
        }
        Object o = null;
        CRC32 crc32;
        try {
            final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            try {
                o = new ByteOrderedDataOutputStream(byteArrayOutputStream, ByteOrder.BIG_ENDIAN);
                this.writeExifSegment((ByteOrderedDataOutputStream)o);
                final byte[] byteArray = ((ByteArrayOutputStream)((ByteOrderedDataOutputStream)o).mOutputStream).toByteArray();
                byteOrderedDataOutputStream.write(byteArray);
                o = new CRC32();
                ((CRC32)o).update(byteArray, 4, byteArray.length - 4);
                byteOrderedDataOutputStream.writeInt((int)((CRC32)o).getValue());
                ExifInterfaceUtils.closeQuietly(byteArrayOutputStream);
                ExifInterfaceUtils.copy(byteOrderedDataInputStream, byteOrderedDataOutputStream);
                return;
            }
            finally {}
        }
        finally {
            crc32 = (CRC32)o;
        }
        ExifInterfaceUtils.closeQuietly((Closeable)crc32);
    }
    
    private void saveWebpAttributes(final InputStream obj, OutputStream obj2) throws IOException {
        Object cause = null;
        if (ExifInterface.DEBUG) {
            cause = new StringBuilder();
            ((StringBuilder)cause).append("saveWebpAttributes starting with (inputStream: ");
            ((StringBuilder)cause).append(obj);
            ((StringBuilder)cause).append(", outputStream: ");
            ((StringBuilder)cause).append(obj2);
            ((StringBuilder)cause).append(")");
            Log.d("ExifInterface", ((StringBuilder)cause).toString());
        }
        final ByteOrderedDataInputStream byteOrderedDataInputStream = new ByteOrderedDataInputStream(obj, ByteOrder.LITTLE_ENDIAN);
        final ByteOrderedDataOutputStream out = new ByteOrderedDataOutputStream((OutputStream)obj2, ByteOrder.LITTLE_ENDIAN);
        final byte[] webp_SIGNATURE_1 = ExifInterface.WEBP_SIGNATURE_1;
        ExifInterfaceUtils.copy(byteOrderedDataInputStream, out, webp_SIGNATURE_1.length);
        final byte[] webp_SIGNATURE_2 = ExifInterface.WEBP_SIGNATURE_2;
        byteOrderedDataInputStream.skipFully(webp_SIGNATURE_2.length + 4);
        byte[] a = null;
        final Exception ex = obj2 = null;
        try {
            try {
                obj2 = ex;
                cause = new ByteArrayOutputStream();
                try {
                    obj2 = (Exception)new ByteOrderedDataOutputStream((OutputStream)cause, ByteOrder.LITTLE_ENDIAN);
                    final int mOffsetToExifData = this.mOffsetToExifData;
                    if (mOffsetToExifData != 0) {
                        ExifInterfaceUtils.copy(byteOrderedDataInputStream, (OutputStream)obj2, mOffsetToExifData - (webp_SIGNATURE_1.length + 4 + webp_SIGNATURE_2.length) - 4 - 4);
                        byteOrderedDataInputStream.skipFully(4);
                        int int1;
                        final int n = int1 = byteOrderedDataInputStream.readInt();
                        if (n % 2 != 0) {
                            int1 = n + 1;
                        }
                        byteOrderedDataInputStream.skipFully(int1);
                        this.writeExifSegment((ByteOrderedDataOutputStream)obj2);
                    }
                    else {
                        a = new byte[4];
                        if (byteOrderedDataInputStream.read(a) != 4) {
                            throw new IOException("Encountered invalid length while parsing WebP chunk type");
                        }
                        final byte[] webp_CHUNK_TYPE_VP8X = ExifInterface.WEBP_CHUNK_TYPE_VP8X;
                        final boolean equals = Arrays.equals(a, webp_CHUNK_TYPE_VP8X);
                        final int n2 = 0;
                        boolean b = true;
                        if (equals) {
                            final int int2 = byteOrderedDataInputStream.readInt();
                            int n3;
                            if (int2 % 2 == 1) {
                                n3 = int2 + 1;
                            }
                            else {
                                n3 = int2;
                            }
                            a = new byte[n3];
                            byteOrderedDataInputStream.read(a);
                            final byte b2 = (byte)(0x8 | a[0]);
                            a[0] = b2;
                            int n4 = n2;
                            if ((b2 >> 1 & 0x1) == 0x1) {
                                n4 = 1;
                            }
                            ((ByteOrderedDataOutputStream)obj2).write(webp_CHUNK_TYPE_VP8X);
                            ((ByteOrderedDataOutputStream)obj2).writeInt(int2);
                            ((ByteOrderedDataOutputStream)obj2).write(a);
                            if (n4 != 0) {
                                this.copyChunksUpToGivenChunkType(byteOrderedDataInputStream, (ByteOrderedDataOutputStream)obj2, ExifInterface.WEBP_CHUNK_TYPE_ANIM, null);
                                while (true) {
                                    a = new byte[4];
                                    obj.read(a);
                                    if (!Arrays.equals(a, ExifInterface.WEBP_CHUNK_TYPE_ANMF)) {
                                        break;
                                    }
                                    this.copyWebPChunk(byteOrderedDataInputStream, (ByteOrderedDataOutputStream)obj2, a);
                                }
                                this.writeExifSegment((ByteOrderedDataOutputStream)obj2);
                            }
                            else {
                                this.copyChunksUpToGivenChunkType(byteOrderedDataInputStream, (ByteOrderedDataOutputStream)obj2, ExifInterface.WEBP_CHUNK_TYPE_VP8, ExifInterface.WEBP_CHUNK_TYPE_VP8L);
                                this.writeExifSegment((ByteOrderedDataOutputStream)obj2);
                            }
                        }
                        else {
                            final byte[] webp_CHUNK_TYPE_VP8 = ExifInterface.WEBP_CHUNK_TYPE_VP8;
                            if (Arrays.equals(a, webp_CHUNK_TYPE_VP8) || Arrays.equals(a, ExifInterface.WEBP_CHUNK_TYPE_VP8L)) {
                                final int int3 = byteOrderedDataInputStream.readInt();
                                int n5;
                                if (int3 % 2 == 1) {
                                    n5 = int3 + 1;
                                }
                                else {
                                    n5 = int3;
                                }
                                final byte[] b3 = new byte[3];
                                int n6;
                                int n7;
                                int n8;
                                if (Arrays.equals(a, webp_CHUNK_TYPE_VP8)) {
                                    byteOrderedDataInputStream.read(b3);
                                    final byte[] array = new byte[3];
                                    if (byteOrderedDataInputStream.read(array) != 3 || !Arrays.equals(ExifInterface.WEBP_VP8_SIGNATURE, array)) {
                                        throw new IOException("Encountered error while checking VP8 signature");
                                    }
                                    n6 = byteOrderedDataInputStream.readInt();
                                    n5 -= 10;
                                    n7 = n6 << 2 >> 18;
                                    n8 = n6 << 18 >> 18;
                                    b = false;
                                }
                                else if (Arrays.equals(a, ExifInterface.WEBP_CHUNK_TYPE_VP8L)) {
                                    if (byteOrderedDataInputStream.readByte() != 47) {
                                        throw new IOException("Encountered error while checking VP8L signature");
                                    }
                                    n6 = byteOrderedDataInputStream.readInt();
                                    n8 = (n6 & 0x3FFF) + 1;
                                    n7 = ((n6 & 0xFFFC000) >>> 14) + 1;
                                    if ((n6 & 0x10000000) == 0x0) {
                                        b = false;
                                    }
                                    n5 -= 5;
                                }
                                else {
                                    n6 = 0;
                                    b = false;
                                    n8 = 0;
                                    n7 = 0;
                                }
                                ((ByteOrderedDataOutputStream)obj2).write(webp_CHUNK_TYPE_VP8X);
                                ((ByteOrderedDataOutputStream)obj2).writeInt(10);
                                final byte[] array2 = new byte[10];
                                if (b) {
                                    array2[0] |= 0x10;
                                }
                                array2[0] |= 0x8;
                                final int n9 = n8 - 1;
                                --n7;
                                array2[4] = (byte)n9;
                                array2[5] = (byte)(n9 >> 8);
                                array2[6] = (byte)(n9 >> 16);
                                array2[7] = (byte)n7;
                                array2[8] = (byte)(n7 >> 8);
                                array2[9] = (byte)(n7 >> 16);
                                ((ByteOrderedDataOutputStream)obj2).write(array2);
                                ((ByteOrderedDataOutputStream)obj2).write(a);
                                ((ByteOrderedDataOutputStream)obj2).writeInt(int3);
                                if (Arrays.equals(a, webp_CHUNK_TYPE_VP8)) {
                                    ((ByteOrderedDataOutputStream)obj2).write(b3);
                                    ((ByteOrderedDataOutputStream)obj2).write(ExifInterface.WEBP_VP8_SIGNATURE);
                                    ((ByteOrderedDataOutputStream)obj2).writeInt(n6);
                                }
                                else if (Arrays.equals(a, ExifInterface.WEBP_CHUNK_TYPE_VP8L)) {
                                    ((FilterOutputStream)obj2).write(47);
                                    ((ByteOrderedDataOutputStream)obj2).writeInt(n6);
                                }
                                ExifInterfaceUtils.copy(byteOrderedDataInputStream, (OutputStream)obj2, n5);
                                this.writeExifSegment((ByteOrderedDataOutputStream)obj2);
                            }
                        }
                    }
                    ExifInterfaceUtils.copy(byteOrderedDataInputStream, (OutputStream)obj2);
                    final int size = ((ByteArrayOutputStream)cause).size();
                    final byte[] webp_SIGNATURE_3 = ExifInterface.WEBP_SIGNATURE_2;
                    out.writeInt(size + webp_SIGNATURE_3.length);
                    out.write(webp_SIGNATURE_3);
                    ((ByteArrayOutputStream)cause).writeTo(out);
                    ExifInterfaceUtils.closeQuietly((Closeable)cause);
                    return;
                }
                catch (final Exception obj2) {}
                finally {
                    obj2 = (Exception)cause;
                }
            }
            finally {}
        }
        catch (final Exception cause) {}
        throw new IOException("Failed to save WebP file", (Throwable)cause);
        ExifInterfaceUtils.closeQuietly((Closeable)obj2);
    }
    
    private void setThumbnailData(final ByteOrderedDataInputStream byteOrderedDataInputStream) throws IOException {
        final HashMap<String, ExifAttribute> hashMap = this.mAttributes[4];
        final ExifAttribute exifAttribute = hashMap.get("Compression");
        if (exifAttribute != null) {
            final int intValue = exifAttribute.getIntValue(this.mExifByteOrder);
            if ((this.mThumbnailCompression = intValue) != 1) {
                if (intValue == 6) {
                    this.handleThumbnailFromJfif(byteOrderedDataInputStream, hashMap);
                    return;
                }
                if (intValue != 7) {
                    return;
                }
            }
            if (this.isSupportedDataType(hashMap)) {
                this.handleThumbnailFromStrips(byteOrderedDataInputStream, hashMap);
            }
        }
        else {
            this.mThumbnailCompression = 6;
            this.handleThumbnailFromJfif(byteOrderedDataInputStream, hashMap);
        }
    }
    
    private static boolean shouldSupportSeek(final int n) {
        return n != 4 && n != 9 && n != 13 && n != 14;
    }
    
    private void swapBasedOnImageSize(final int n, final int n2) throws IOException {
        if (!this.mAttributes[n].isEmpty() && !this.mAttributes[n2].isEmpty()) {
            final ExifAttribute exifAttribute = this.mAttributes[n].get("ImageLength");
            final ExifAttribute exifAttribute2 = this.mAttributes[n].get("ImageWidth");
            final ExifAttribute exifAttribute3 = this.mAttributes[n2].get("ImageLength");
            final ExifAttribute exifAttribute4 = this.mAttributes[n2].get("ImageWidth");
            if (exifAttribute != null && exifAttribute2 != null) {
                if (exifAttribute3 != null && exifAttribute4 != null) {
                    final int intValue = exifAttribute.getIntValue(this.mExifByteOrder);
                    final int intValue2 = exifAttribute2.getIntValue(this.mExifByteOrder);
                    final int intValue3 = exifAttribute3.getIntValue(this.mExifByteOrder);
                    final int intValue4 = exifAttribute4.getIntValue(this.mExifByteOrder);
                    if (intValue < intValue3 && intValue2 < intValue4) {
                        final HashMap<String, ExifAttribute>[] mAttributes = this.mAttributes;
                        final HashMap<String, ExifAttribute> hashMap = mAttributes[n];
                        mAttributes[n] = mAttributes[n2];
                        mAttributes[n2] = hashMap;
                    }
                }
                else if (ExifInterface.DEBUG) {
                    Log.d("ExifInterface", "Second image does not contain valid size information");
                }
            }
            else if (ExifInterface.DEBUG) {
                Log.d("ExifInterface", "First image does not contain valid size information");
            }
            return;
        }
        if (ExifInterface.DEBUG) {
            Log.d("ExifInterface", "Cannot perform swap since only one image data exists");
        }
    }
    
    private void updateImageSizeValues(final SeekableByteOrderedDataInputStream seekableByteOrderedDataInputStream, final int n) throws IOException {
        final ExifAttribute exifAttribute = this.mAttributes[n].get("DefaultCropSize");
        final ExifAttribute exifAttribute2 = this.mAttributes[n].get("SensorTopBorder");
        final ExifAttribute exifAttribute3 = this.mAttributes[n].get("SensorLeftBorder");
        final ExifAttribute exifAttribute4 = this.mAttributes[n].get("SensorBottomBorder");
        final ExifAttribute exifAttribute5 = this.mAttributes[n].get("SensorRightBorder");
        if (exifAttribute != null) {
            ExifAttribute value;
            ExifAttribute value2;
            if (exifAttribute.format == 5) {
                final Rational[] a = (Rational[])exifAttribute.getValue(this.mExifByteOrder);
                if (a == null || a.length != 2) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Invalid crop size values. cropSize=");
                    sb.append(Arrays.toString(a));
                    Log.w("ExifInterface", sb.toString());
                    return;
                }
                value = ExifAttribute.createURational(a[0], this.mExifByteOrder);
                value2 = ExifAttribute.createURational(a[1], this.mExifByteOrder);
            }
            else {
                final int[] a2 = (int[])exifAttribute.getValue(this.mExifByteOrder);
                if (a2 == null || a2.length != 2) {
                    final StringBuilder sb2 = new StringBuilder();
                    sb2.append("Invalid crop size values. cropSize=");
                    sb2.append(Arrays.toString(a2));
                    Log.w("ExifInterface", sb2.toString());
                    return;
                }
                value = ExifAttribute.createUShort(a2[0], this.mExifByteOrder);
                value2 = ExifAttribute.createUShort(a2[1], this.mExifByteOrder);
            }
            this.mAttributes[n].put("ImageWidth", value);
            this.mAttributes[n].put("ImageLength", value2);
        }
        else if (exifAttribute2 != null && exifAttribute3 != null && exifAttribute4 != null && exifAttribute5 != null) {
            final int intValue = exifAttribute2.getIntValue(this.mExifByteOrder);
            final int intValue2 = exifAttribute4.getIntValue(this.mExifByteOrder);
            final int intValue3 = exifAttribute5.getIntValue(this.mExifByteOrder);
            final int intValue4 = exifAttribute3.getIntValue(this.mExifByteOrder);
            if (intValue2 > intValue && intValue3 > intValue4) {
                final ExifAttribute uShort = ExifAttribute.createUShort(intValue2 - intValue, this.mExifByteOrder);
                final ExifAttribute uShort2 = ExifAttribute.createUShort(intValue3 - intValue4, this.mExifByteOrder);
                this.mAttributes[n].put("ImageLength", uShort);
                this.mAttributes[n].put("ImageWidth", uShort2);
            }
        }
        else {
            this.retrieveJpegImageSize(seekableByteOrderedDataInputStream, n);
        }
    }
    
    private void validateImages() throws IOException {
        this.swapBasedOnImageSize(0, 5);
        this.swapBasedOnImageSize(0, 4);
        this.swapBasedOnImageSize(5, 4);
        final ExifAttribute value = this.mAttributes[1].get("PixelXDimension");
        final ExifAttribute value2 = this.mAttributes[1].get("PixelYDimension");
        if (value != null && value2 != null) {
            this.mAttributes[0].put("ImageWidth", value);
            this.mAttributes[0].put("ImageLength", value2);
        }
        if (this.mAttributes[4].isEmpty() && this.isThumbnail(this.mAttributes[5])) {
            final HashMap<String, ExifAttribute>[] mAttributes = this.mAttributes;
            mAttributes[4] = mAttributes[5];
            mAttributes[5] = new HashMap<String, ExifAttribute>();
        }
        if (!this.isThumbnail(this.mAttributes[4])) {
            Log.d("ExifInterface", "No image meets the size requirements of a thumbnail image.");
        }
        this.replaceInvalidTags(0, "ThumbnailOrientation", "Orientation");
        this.replaceInvalidTags(0, "ThumbnailImageLength", "ImageLength");
        this.replaceInvalidTags(0, "ThumbnailImageWidth", "ImageWidth");
        this.replaceInvalidTags(5, "ThumbnailOrientation", "Orientation");
        this.replaceInvalidTags(5, "ThumbnailImageLength", "ImageLength");
        this.replaceInvalidTags(5, "ThumbnailImageWidth", "ImageWidth");
        this.replaceInvalidTags(4, "Orientation", "ThumbnailOrientation");
        this.replaceInvalidTags(4, "ImageLength", "ThumbnailImageLength");
        this.replaceInvalidTags(4, "ImageWidth", "ThumbnailImageWidth");
    }
    
    private int writeExifSegment(final ByteOrderedDataOutputStream byteOrderedDataOutputStream) throws IOException {
        final ExifTag[][] exif_TAGS = ExifInterface.EXIF_TAGS;
        final int[] array = new int[exif_TAGS.length];
        final int[] array2 = new int[exif_TAGS.length];
        final ExifTag[] exif_POINTER_TAGS = ExifInterface.EXIF_POINTER_TAGS;
        for (int length = exif_POINTER_TAGS.length, i = 0; i < length; ++i) {
            this.removeAttribute(exif_POINTER_TAGS[i].name);
        }
        if (this.mHasThumbnail) {
            if (this.mHasThumbnailStrips) {
                this.removeAttribute("StripOffsets");
                this.removeAttribute("StripByteCounts");
            }
            else {
                this.removeAttribute("JPEGInterchangeFormat");
                this.removeAttribute("JPEGInterchangeFormatLength");
            }
        }
        for (int j = 0; j < ExifInterface.EXIF_TAGS.length; ++j) {
            final Object[] array3 = this.mAttributes[j].entrySet().toArray();
            for (int length2 = array3.length, k = 0; k < length2; ++k) {
                final Map.Entry entry = (Map.Entry)array3[k];
                if (entry.getValue() == null) {
                    this.mAttributes[j].remove(entry.getKey());
                }
            }
        }
        if (!this.mAttributes[1].isEmpty()) {
            this.mAttributes[0].put(ExifInterface.EXIF_POINTER_TAGS[1].name, ExifAttribute.createULong(0L, this.mExifByteOrder));
        }
        if (!this.mAttributes[2].isEmpty()) {
            this.mAttributes[0].put(ExifInterface.EXIF_POINTER_TAGS[2].name, ExifAttribute.createULong(0L, this.mExifByteOrder));
        }
        if (!this.mAttributes[3].isEmpty()) {
            this.mAttributes[1].put(ExifInterface.EXIF_POINTER_TAGS[3].name, ExifAttribute.createULong(0L, this.mExifByteOrder));
        }
        if (this.mHasThumbnail) {
            if (this.mHasThumbnailStrips) {
                this.mAttributes[4].put("StripOffsets", ExifAttribute.createUShort(0, this.mExifByteOrder));
                this.mAttributes[4].put("StripByteCounts", ExifAttribute.createUShort(this.mThumbnailLength, this.mExifByteOrder));
            }
            else {
                this.mAttributes[4].put("JPEGInterchangeFormat", ExifAttribute.createULong(0L, this.mExifByteOrder));
                this.mAttributes[4].put("JPEGInterchangeFormatLength", ExifAttribute.createULong(this.mThumbnailLength, this.mExifByteOrder));
            }
        }
        for (int l = 0; l < ExifInterface.EXIF_TAGS.length; ++l) {
            final Iterator<Map.Entry<String, ExifAttribute>> iterator = this.mAttributes[l].entrySet().iterator();
            int n = 0;
            while (iterator.hasNext()) {
                final int size = ((Map.Entry<K, ExifAttribute>)iterator.next()).getValue().size();
                if (size > 4) {
                    n += size;
                }
            }
            array2[l] += n;
        }
        int mThumbnailOffset = 8;
        int n3;
        for (int n2 = 0; n2 < ExifInterface.EXIF_TAGS.length; ++n2, mThumbnailOffset = n3) {
            n3 = mThumbnailOffset;
            if (!this.mAttributes[n2].isEmpty()) {
                array[n2] = mThumbnailOffset;
                n3 = mThumbnailOffset + (this.mAttributes[n2].size() * 12 + 2 + 4 + array2[n2]);
            }
        }
        int n4 = mThumbnailOffset;
        if (this.mHasThumbnail) {
            if (this.mHasThumbnailStrips) {
                this.mAttributes[4].put("StripOffsets", ExifAttribute.createUShort(mThumbnailOffset, this.mExifByteOrder));
            }
            else {
                this.mAttributes[4].put("JPEGInterchangeFormat", ExifAttribute.createULong(mThumbnailOffset, this.mExifByteOrder));
            }
            this.mThumbnailOffset = mThumbnailOffset;
            n4 = mThumbnailOffset + this.mThumbnailLength;
        }
        int m = n4;
        if (this.mMimeType == 4) {
            m = n4 + 8;
        }
        if (ExifInterface.DEBUG) {
            for (int i2 = 0; i2 < ExifInterface.EXIF_TAGS.length; ++i2) {
                Log.d("ExifInterface", String.format("index: %d, offsets: %d, tag count: %d, data sizes: %d, total size: %d", i2, array[i2], this.mAttributes[i2].size(), array2[i2], m));
            }
        }
        if (!this.mAttributes[1].isEmpty()) {
            this.mAttributes[0].put(ExifInterface.EXIF_POINTER_TAGS[1].name, ExifAttribute.createULong(array[1], this.mExifByteOrder));
        }
        if (!this.mAttributes[2].isEmpty()) {
            this.mAttributes[0].put(ExifInterface.EXIF_POINTER_TAGS[2].name, ExifAttribute.createULong(array[2], this.mExifByteOrder));
        }
        if (!this.mAttributes[3].isEmpty()) {
            this.mAttributes[1].put(ExifInterface.EXIF_POINTER_TAGS[3].name, ExifAttribute.createULong(array[3], this.mExifByteOrder));
        }
        final int mMimeType = this.mMimeType;
        if (mMimeType != 4) {
            if (mMimeType != 13) {
                if (mMimeType == 14) {
                    byteOrderedDataOutputStream.write(ExifInterface.WEBP_CHUNK_TYPE_EXIF);
                    byteOrderedDataOutputStream.writeInt(m);
                }
            }
            else {
                byteOrderedDataOutputStream.writeInt(m);
                byteOrderedDataOutputStream.write(ExifInterface.PNG_CHUNK_TYPE_EXIF);
            }
        }
        else {
            byteOrderedDataOutputStream.writeUnsignedShort(m);
            byteOrderedDataOutputStream.write(ExifInterface.IDENTIFIER_EXIF_APP1);
        }
        short n5;
        if (this.mExifByteOrder == ByteOrder.BIG_ENDIAN) {
            n5 = 19789;
        }
        else {
            n5 = 18761;
        }
        byteOrderedDataOutputStream.writeShort(n5);
        byteOrderedDataOutputStream.setByteOrder(this.mExifByteOrder);
        byteOrderedDataOutputStream.writeUnsignedShort(42);
        byteOrderedDataOutputStream.writeUnsignedInt(8L);
        for (int n6 = 0; n6 < ExifInterface.EXIF_TAGS.length; ++n6) {
            if (!this.mAttributes[n6].isEmpty()) {
                byteOrderedDataOutputStream.writeUnsignedShort(this.mAttributes[n6].size());
                int n7 = array[n6] + 2 + this.mAttributes[n6].size() * 12 + 4;
                for (final Map.Entry<Object, V> entry2 : this.mAttributes[n6].entrySet()) {
                    final int number = ExifInterface.sExifTagMapsForWriting[n6].get(entry2.getKey()).number;
                    final ExifAttribute exifAttribute = (ExifAttribute)entry2.getValue();
                    int size2 = exifAttribute.size();
                    byteOrderedDataOutputStream.writeUnsignedShort(number);
                    byteOrderedDataOutputStream.writeUnsignedShort(exifAttribute.format);
                    byteOrderedDataOutputStream.writeInt(exifAttribute.numberOfComponents);
                    int n8;
                    if (size2 > 4) {
                        byteOrderedDataOutputStream.writeUnsignedInt(n7);
                        n8 = n7 + size2;
                    }
                    else {
                        byteOrderedDataOutputStream.write(exifAttribute.bytes);
                        n8 = n7;
                        if (size2 < 4) {
                            while (true) {
                                n8 = n7;
                                if (size2 >= 4) {
                                    break;
                                }
                                byteOrderedDataOutputStream.writeByte(0);
                                ++size2;
                            }
                        }
                    }
                    n7 = n8;
                }
                if (n6 == 0 && !this.mAttributes[4].isEmpty()) {
                    byteOrderedDataOutputStream.writeUnsignedInt(array[4]);
                }
                else {
                    byteOrderedDataOutputStream.writeUnsignedInt(0L);
                }
                final Iterator<Map.Entry<String, ExifAttribute>> iterator3 = this.mAttributes[n6].entrySet().iterator();
                while (iterator3.hasNext()) {
                    final ExifAttribute exifAttribute2 = ((Map.Entry<K, ExifAttribute>)iterator3.next()).getValue();
                    if (exifAttribute2.bytes.length > 4) {
                        byteOrderedDataOutputStream.write(exifAttribute2.bytes, 0, exifAttribute2.bytes.length);
                    }
                }
            }
        }
        if (this.mHasThumbnail) {
            byteOrderedDataOutputStream.write(this.getThumbnailBytes());
        }
        if (this.mMimeType == 14 && m % 2 == 1) {
            byteOrderedDataOutputStream.writeByte(0);
        }
        byteOrderedDataOutputStream.setByteOrder(ByteOrder.BIG_ENDIAN);
        return m;
    }
    
    public void flipHorizontally() {
        int i = 1;
        while (true) {
            switch (this.getAttributeInt("Orientation", 1)) {
                default: {
                    i = 0;
                }
                case 2: {
                    this.setAttribute("Orientation", Integer.toString(i));
                    return;
                }
                case 8: {
                    i = 7;
                    continue;
                }
                case 7: {
                    i = 8;
                    continue;
                }
                case 6: {
                    i = 5;
                    continue;
                }
                case 5: {
                    i = 6;
                    continue;
                }
                case 4: {
                    i = 3;
                    continue;
                }
                case 3: {
                    i = 4;
                    continue;
                }
                case 1: {
                    i = 2;
                    continue;
                }
            }
            break;
        }
    }
    
    public void flipVertically() {
        int i = 1;
        while (true) {
            switch (this.getAttributeInt("Orientation", 1)) {
                default: {
                    i = 0;
                }
                case 4: {
                    this.setAttribute("Orientation", Integer.toString(i));
                    return;
                }
                case 8: {
                    i = 5;
                    continue;
                }
                case 7: {
                    i = 6;
                    continue;
                }
                case 6: {
                    i = 7;
                    continue;
                }
                case 5: {
                    i = 8;
                    continue;
                }
                case 3: {
                    i = 2;
                    continue;
                }
                case 2: {
                    i = 3;
                    continue;
                }
                case 1: {
                    i = 4;
                    continue;
                }
            }
            break;
        }
    }
    
    public double getAltitude(final double n) {
        final double attributeDouble = this.getAttributeDouble("GPSAltitude", -1.0);
        int n2 = -1;
        final int attributeInt = this.getAttributeInt("GPSAltitudeRef", -1);
        if (attributeDouble >= 0.0 && attributeInt >= 0) {
            if (attributeInt != 1) {
                n2 = 1;
            }
            return attributeDouble * n2;
        }
        return n;
    }
    
    public String getAttribute(String string) {
        Label_0254: {
            if (string == null) {
                break Label_0254;
            }
            final ExifAttribute exifAttribute = this.getExifAttribute(string);
            Label_0252: {
                if (exifAttribute == null) {
                    break Label_0252;
                }
                if (!ExifInterface.sTagSetForCompatibility.contains(string)) {
                    return exifAttribute.getStringValue(this.mExifByteOrder);
                }
                if (string.equals("GPSTimeStamp")) {
                    if (exifAttribute.format != 5 && exifAttribute.format != 10) {
                        final StringBuilder sb = new StringBuilder();
                        sb.append("GPS Timestamp format is not rational. format=");
                        sb.append(exifAttribute.format);
                        Log.w("ExifInterface", sb.toString());
                        return null;
                    }
                    final Rational[] a = (Rational[])exifAttribute.getValue(this.mExifByteOrder);
                    if (a != null && a.length == 3) {
                        return String.format("%02d:%02d:%02d", (int)(a[0].numerator / (float)a[0].denominator), (int)(a[1].numerator / (float)a[1].denominator), (int)(a[2].numerator / (float)a[2].denominator));
                    }
                    final StringBuilder sb2 = new StringBuilder();
                    sb2.append("Invalid GPS Timestamp array. array=");
                    sb2.append(Arrays.toString(a));
                    Log.w("ExifInterface", sb2.toString());
                    return null;
                }
                try {
                    string = Double.toString(exifAttribute.getDoubleValue(this.mExifByteOrder));
                    return string;
                    throw new NullPointerException("tag shouldn't be null");
                    return null;
                }
                catch (final NumberFormatException ex) {
                    return null;
                }
            }
        }
    }
    
    public byte[] getAttributeBytes(final String s) {
        if (s == null) {
            throw new NullPointerException("tag shouldn't be null");
        }
        final ExifAttribute exifAttribute = this.getExifAttribute(s);
        if (exifAttribute != null) {
            return exifAttribute.bytes;
        }
        return null;
    }
    
    public double getAttributeDouble(final String s, final double n) {
        if (s != null) {
            final ExifAttribute exifAttribute = this.getExifAttribute(s);
            if (exifAttribute == null) {
                return n;
            }
            try {
                return exifAttribute.getDoubleValue(this.mExifByteOrder);
            }
            catch (final NumberFormatException ex) {
                return n;
            }
        }
        throw new NullPointerException("tag shouldn't be null");
    }
    
    public int getAttributeInt(final String s, final int n) {
        if (s != null) {
            final ExifAttribute exifAttribute = this.getExifAttribute(s);
            if (exifAttribute == null) {
                return n;
            }
            try {
                return exifAttribute.getIntValue(this.mExifByteOrder);
            }
            catch (final NumberFormatException ex) {
                return n;
            }
        }
        throw new NullPointerException("tag shouldn't be null");
    }
    
    public long[] getAttributeRange(final String s) {
        if (s == null) {
            throw new NullPointerException("tag shouldn't be null");
        }
        if (this.mModified) {
            throw new IllegalStateException("The underlying file has been modified since being parsed");
        }
        final ExifAttribute exifAttribute = this.getExifAttribute(s);
        if (exifAttribute != null) {
            return new long[] { exifAttribute.bytesOffset, exifAttribute.bytes.length };
        }
        return null;
    }
    
    public Long getDateTime() {
        return parseDateTime(this.getAttribute("DateTime"), this.getAttribute("SubSecTime"), this.getAttribute("OffsetTime"));
    }
    
    public Long getDateTimeDigitized() {
        return parseDateTime(this.getAttribute("DateTimeDigitized"), this.getAttribute("SubSecTimeDigitized"), this.getAttribute("OffsetTimeDigitized"));
    }
    
    public Long getDateTimeOriginal() {
        return parseDateTime(this.getAttribute("DateTimeOriginal"), this.getAttribute("SubSecTimeOriginal"), this.getAttribute("OffsetTimeOriginal"));
    }
    
    public Long getGpsDateTime() {
        final String attribute = this.getAttribute("GPSDateStamp");
        final String attribute2 = this.getAttribute("GPSTimeStamp");
        Label_0160: {
            if (attribute == null || attribute2 == null) {
                break Label_0160;
            }
            final Pattern non_ZERO_TIME_PATTERN = ExifInterface.NON_ZERO_TIME_PATTERN;
            if (!non_ZERO_TIME_PATTERN.matcher(attribute).matches() && !non_ZERO_TIME_PATTERN.matcher(attribute2).matches()) {
                break Label_0160;
            }
            final StringBuilder sb = new StringBuilder();
            sb.append(attribute);
            sb.append(' ');
            sb.append(attribute2);
            final String string = sb.toString();
            final ParsePosition parsePosition = new ParsePosition(0);
            try {
                Date date;
                if ((date = ExifInterface.sFormatterPrimary.parse(string, parsePosition)) == null && (date = ExifInterface.sFormatterSecondary.parse(string, parsePosition)) == null) {
                    return null;
                }
                return date.getTime();
                return null;
            }
            catch (final IllegalArgumentException ex) {
                return null;
            }
        }
    }
    
    @Deprecated
    public boolean getLatLong(final float[] array) {
        final double[] latLong = this.getLatLong();
        if (latLong == null) {
            return false;
        }
        array[0] = (float)latLong[0];
        array[1] = (float)latLong[1];
        return true;
    }
    
    public double[] getLatLong() {
        final String attribute = this.getAttribute("GPSLatitude");
        final String attribute2 = this.getAttribute("GPSLatitudeRef");
        final String attribute3 = this.getAttribute("GPSLongitude");
        final String attribute4 = this.getAttribute("GPSLongitudeRef");
        if (attribute != null && attribute2 != null && attribute3 != null && attribute4 != null) {
            try {
                return new double[] { convertRationalLatLonToDouble(attribute, attribute2), convertRationalLatLonToDouble(attribute3, attribute4) };
            }
            catch (final IllegalArgumentException ex) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Latitude/longitude values are not parsable. ");
                sb.append(String.format("latValue=%s, latRef=%s, lngValue=%s, lngRef=%s", attribute, attribute2, attribute3, attribute4));
                Log.w("ExifInterface", sb.toString());
            }
        }
        return null;
    }
    
    public int getRotationDegrees() {
        switch (this.getAttributeInt("Orientation", 1)) {
            default: {
                return 0;
            }
            case 6:
            case 7: {
                return 90;
            }
            case 5:
            case 8: {
                return 270;
            }
            case 3:
            case 4: {
                return 180;
            }
        }
    }
    
    public byte[] getThumbnail() {
        final int mThumbnailCompression = this.mThumbnailCompression;
        if (mThumbnailCompression != 6 && mThumbnailCompression != 7) {
            return null;
        }
        return this.getThumbnailBytes();
    }
    
    public Bitmap getThumbnailBitmap() {
        if (!this.mHasThumbnail) {
            return null;
        }
        if (this.mThumbnailBytes == null) {
            this.mThumbnailBytes = this.getThumbnailBytes();
        }
        final int mThumbnailCompression = this.mThumbnailCompression;
        if (mThumbnailCompression != 6 && mThumbnailCompression != 7) {
            if (mThumbnailCompression == 1) {
                final int n = this.mThumbnailBytes.length / 3;
                final int[] array = new int[n];
                for (int i = 0; i < n; ++i) {
                    final byte[] mThumbnailBytes = this.mThumbnailBytes;
                    final int n2 = i * 3;
                    array[i] = (mThumbnailBytes[n2] << 16) + 0 + (mThumbnailBytes[n2 + 1] << 8) + mThumbnailBytes[n2 + 2];
                }
                final ExifAttribute exifAttribute = this.mAttributes[4].get("ThumbnailImageLength");
                final ExifAttribute exifAttribute2 = this.mAttributes[4].get("ThumbnailImageWidth");
                if (exifAttribute != null && exifAttribute2 != null) {
                    return Bitmap.createBitmap(array, exifAttribute2.getIntValue(this.mExifByteOrder), exifAttribute.getIntValue(this.mExifByteOrder), Bitmap$Config.ARGB_8888);
                }
            }
            return null;
        }
        return BitmapFactory.decodeByteArray(this.mThumbnailBytes, 0, this.mThumbnailLength);
    }
    
    public byte[] getThumbnailBytes() {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: getfield        androidx/exifinterface/media/ExifInterface.mHasThumbnail:Z
        //     4: istore          5
        //     6: aconst_null    
        //     7: astore          8
        //     9: iload           5
        //    11: ifne            16
        //    14: aconst_null    
        //    15: areturn        
        //    16: aload_0        
        //    17: getfield        androidx/exifinterface/media/ExifInterface.mThumbnailBytes:[B
        //    20: astore          6
        //    22: aload           6
        //    24: ifnull          30
        //    27: aload           6
        //    29: areturn        
        //    30: aload_0        
        //    31: getfield        androidx/exifinterface/media/ExifInterface.mAssetInputStream:Landroid/content/res/AssetManager$AssetInputStream;
        //    34: astore          6
        //    36: aload           6
        //    38: ifnull          93
        //    41: aload           6
        //    43: invokevirtual   java/io/InputStream.markSupported:()Z
        //    46: ifeq            60
        //    49: aload           6
        //    51: invokevirtual   java/io/InputStream.reset:()V
        //    54: aconst_null    
        //    55: astore          7
        //    57: goto            196
        //    60: ldc_w           "ExifInterface"
        //    63: ldc_w           "Cannot read thumbnail from inputstream without mark/reset support"
        //    66: invokestatic    android/util/Log.d:(Ljava/lang/String;Ljava/lang/String;)I
        //    69: pop            
        //    70: aload           6
        //    72: invokestatic    androidx/exifinterface/media/ExifInterfaceUtils.closeQuietly:(Ljava/io/Closeable;)V
        //    75: aconst_null    
        //    76: areturn        
        //    77: astore          7
        //    79: aconst_null    
        //    80: astore          10
        //    82: goto            515
        //    85: astore          8
        //    87: aconst_null    
        //    88: astore          7
        //    90: goto            472
        //    93: aload_0        
        //    94: getfield        androidx/exifinterface/media/ExifInterface.mFilename:Ljava/lang/String;
        //    97: ifnull          116
        //   100: new             Ljava/io/FileInputStream;
        //   103: dup            
        //   104: aload_0        
        //   105: getfield        androidx/exifinterface/media/ExifInterface.mFilename:Ljava/lang/String;
        //   108: invokespecial   java/io/FileInputStream.<init>:(Ljava/lang/String;)V
        //   111: astore          6
        //   113: goto            54
        //   116: getstatic       android/os/Build$VERSION.SDK_INT:I
        //   119: bipush          21
        //   121: if_icmplt       190
        //   124: aload_0        
        //   125: getfield        androidx/exifinterface/media/ExifInterface.mSeekableFileDescriptor:Ljava/io/FileDescriptor;
        //   128: invokestatic    androidx/exifinterface/media/ExifInterfaceUtils$Api21Impl.dup:(Ljava/io/FileDescriptor;)Ljava/io/FileDescriptor;
        //   131: astore          6
        //   133: aload           6
        //   135: lconst_0       
        //   136: getstatic       android/system/OsConstants.SEEK_SET:I
        //   139: invokestatic    androidx/exifinterface/media/ExifInterfaceUtils$Api21Impl.lseek:(Ljava/io/FileDescriptor;JI)J
        //   142: pop2           
        //   143: new             Ljava/io/FileInputStream;
        //   146: dup            
        //   147: aload           6
        //   149: invokespecial   java/io/FileInputStream.<init>:(Ljava/io/FileDescriptor;)V
        //   152: astore          9
        //   154: aload           6
        //   156: astore          7
        //   158: aload           9
        //   160: astore          6
        //   162: goto            196
        //   165: astore          7
        //   167: aload           6
        //   169: astore          10
        //   171: aload           8
        //   173: astore          6
        //   175: goto            515
        //   178: astore          8
        //   180: aload           6
        //   182: astore          7
        //   184: aconst_null    
        //   185: astore          6
        //   187: goto            472
        //   190: aconst_null    
        //   191: astore          6
        //   193: aconst_null    
        //   194: astore          7
        //   196: aload           6
        //   198: ifnull          415
        //   201: aload           6
        //   203: astore          9
        //   205: aload           7
        //   207: astore          10
        //   209: aload           6
        //   211: aload_0        
        //   212: getfield        androidx/exifinterface/media/ExifInterface.mThumbnailOffset:I
        //   215: aload_0        
        //   216: getfield        androidx/exifinterface/media/ExifInterface.mOffsetToExifData:I
        //   219: iadd           
        //   220: i2l            
        //   221: invokevirtual   java/io/InputStream.skip:(J)J
        //   224: lstore_3       
        //   225: aload           6
        //   227: astore          9
        //   229: aload           7
        //   231: astore          10
        //   233: aload_0        
        //   234: getfield        androidx/exifinterface/media/ExifInterface.mThumbnailOffset:I
        //   237: istore_2       
        //   238: aload           6
        //   240: astore          9
        //   242: aload           7
        //   244: astore          10
        //   246: aload_0        
        //   247: getfield        androidx/exifinterface/media/ExifInterface.mOffsetToExifData:I
        //   250: istore_1       
        //   251: lload_3        
        //   252: iload_2        
        //   253: iload_1        
        //   254: iadd           
        //   255: i2l            
        //   256: lcmp           
        //   257: ifne            370
        //   260: aload           6
        //   262: astore          9
        //   264: aload           7
        //   266: astore          10
        //   268: aload_0        
        //   269: getfield        androidx/exifinterface/media/ExifInterface.mThumbnailLength:I
        //   272: newarray        B
        //   274: astore          8
        //   276: aload           6
        //   278: astore          9
        //   280: aload           7
        //   282: astore          10
        //   284: aload           6
        //   286: aload           8
        //   288: invokevirtual   java/io/InputStream.read:([B)I
        //   291: aload_0        
        //   292: getfield        androidx/exifinterface/media/ExifInterface.mThumbnailLength:I
        //   295: if_icmpne       330
        //   298: aload           6
        //   300: astore          9
        //   302: aload           7
        //   304: astore          10
        //   306: aload_0        
        //   307: aload           8
        //   309: putfield        androidx/exifinterface/media/ExifInterface.mThumbnailBytes:[B
        //   312: aload           6
        //   314: invokestatic    androidx/exifinterface/media/ExifInterfaceUtils.closeQuietly:(Ljava/io/Closeable;)V
        //   317: aload           7
        //   319: ifnull          327
        //   322: aload           7
        //   324: invokestatic    androidx/exifinterface/media/ExifInterfaceUtils.closeFileDescriptor:(Ljava/io/FileDescriptor;)V
        //   327: aload           8
        //   329: areturn        
        //   330: aload           6
        //   332: astore          9
        //   334: aload           7
        //   336: astore          10
        //   338: new             Ljava/io/IOException;
        //   341: astore          8
        //   343: aload           6
        //   345: astore          9
        //   347: aload           7
        //   349: astore          10
        //   351: aload           8
        //   353: ldc_w           "Corrupted image"
        //   356: invokespecial   java/io/IOException.<init>:(Ljava/lang/String;)V
        //   359: aload           6
        //   361: astore          9
        //   363: aload           7
        //   365: astore          10
        //   367: aload           8
        //   369: athrow         
        //   370: aload           6
        //   372: astore          9
        //   374: aload           7
        //   376: astore          10
        //   378: new             Ljava/io/IOException;
        //   381: astore          8
        //   383: aload           6
        //   385: astore          9
        //   387: aload           7
        //   389: astore          10
        //   391: aload           8
        //   393: ldc_w           "Corrupted image"
        //   396: invokespecial   java/io/IOException.<init>:(Ljava/lang/String;)V
        //   399: aload           6
        //   401: astore          9
        //   403: aload           7
        //   405: astore          10
        //   407: aload           8
        //   409: athrow         
        //   410: astore          8
        //   412: goto            472
        //   415: aload           6
        //   417: astore          9
        //   419: aload           7
        //   421: astore          10
        //   423: new             Ljava/io/FileNotFoundException;
        //   426: astore          8
        //   428: aload           6
        //   430: astore          9
        //   432: aload           7
        //   434: astore          10
        //   436: aload           8
        //   438: invokespecial   java/io/FileNotFoundException.<init>:()V
        //   441: aload           6
        //   443: astore          9
        //   445: aload           7
        //   447: astore          10
        //   449: aload           8
        //   451: athrow         
        //   452: astore          7
        //   454: aconst_null    
        //   455: astore          10
        //   457: aload           8
        //   459: astore          6
        //   461: goto            515
        //   464: astore          8
        //   466: aconst_null    
        //   467: astore          6
        //   469: aconst_null    
        //   470: astore          7
        //   472: aload           6
        //   474: astore          9
        //   476: aload           7
        //   478: astore          10
        //   480: ldc_w           "ExifInterface"
        //   483: ldc_w           "Encountered exception while getting thumbnail"
        //   486: aload           8
        //   488: invokestatic    android/util/Log.d:(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //   491: pop            
        //   492: aload           6
        //   494: invokestatic    androidx/exifinterface/media/ExifInterfaceUtils.closeQuietly:(Ljava/io/Closeable;)V
        //   497: aload           7
        //   499: ifnull          507
        //   502: aload           7
        //   504: invokestatic    androidx/exifinterface/media/ExifInterfaceUtils.closeFileDescriptor:(Ljava/io/FileDescriptor;)V
        //   507: aconst_null    
        //   508: areturn        
        //   509: astore          7
        //   511: aload           9
        //   513: astore          6
        //   515: aload           6
        //   517: invokestatic    androidx/exifinterface/media/ExifInterfaceUtils.closeQuietly:(Ljava/io/Closeable;)V
        //   520: aload           10
        //   522: ifnull          530
        //   525: aload           10
        //   527: invokestatic    androidx/exifinterface/media/ExifInterfaceUtils.closeFileDescriptor:(Ljava/io/FileDescriptor;)V
        //   530: aload           7
        //   532: athrow         
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                 
        //  -----  -----  -----  -----  ---------------------
        //  30     36     464    472    Ljava/lang/Exception;
        //  30     36     452    464    Any
        //  41     54     85     93     Ljava/lang/Exception;
        //  41     54     77     85     Any
        //  60     70     85     93     Ljava/lang/Exception;
        //  60     70     77     85     Any
        //  93     113    464    472    Ljava/lang/Exception;
        //  93     113    452    464    Any
        //  116    133    464    472    Ljava/lang/Exception;
        //  116    133    452    464    Any
        //  133    154    178    190    Ljava/lang/Exception;
        //  133    154    165    178    Any
        //  209    225    410    415    Ljava/lang/Exception;
        //  209    225    509    515    Any
        //  233    238    410    415    Ljava/lang/Exception;
        //  233    238    509    515    Any
        //  246    251    410    415    Ljava/lang/Exception;
        //  246    251    509    515    Any
        //  268    276    410    415    Ljava/lang/Exception;
        //  268    276    509    515    Any
        //  284    298    410    415    Ljava/lang/Exception;
        //  284    298    509    515    Any
        //  306    312    410    415    Ljava/lang/Exception;
        //  306    312    509    515    Any
        //  338    343    410    415    Ljava/lang/Exception;
        //  338    343    509    515    Any
        //  351    359    410    415    Ljava/lang/Exception;
        //  351    359    509    515    Any
        //  367    370    410    415    Ljava/lang/Exception;
        //  367    370    509    515    Any
        //  378    383    410    415    Ljava/lang/Exception;
        //  378    383    509    515    Any
        //  391    399    410    415    Ljava/lang/Exception;
        //  391    399    509    515    Any
        //  407    410    410    415    Ljava/lang/Exception;
        //  407    410    509    515    Any
        //  423    428    410    415    Ljava/lang/Exception;
        //  423    428    509    515    Any
        //  436    441    410    415    Ljava/lang/Exception;
        //  436    441    509    515    Any
        //  449    452    410    415    Ljava/lang/Exception;
        //  449    452    509    515    Any
        //  480    492    509    515    Any
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0327:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2604)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:206)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public long[] getThumbnailRange() {
        if (this.mModified) {
            throw new IllegalStateException("The underlying file has been modified since being parsed");
        }
        if (!this.mHasThumbnail) {
            return null;
        }
        if (this.mHasThumbnailStrips && !this.mAreThumbnailStripsConsecutive) {
            return null;
        }
        return new long[] { this.mThumbnailOffset + this.mOffsetToExifData, this.mThumbnailLength };
    }
    
    public boolean hasAttribute(final String s) {
        return this.getExifAttribute(s) != null;
    }
    
    public boolean hasThumbnail() {
        return this.mHasThumbnail;
    }
    
    public boolean isFlipped() {
        final int attributeInt = this.getAttributeInt("Orientation", 1);
        return attributeInt == 2 || attributeInt == 7 || attributeInt == 4 || attributeInt == 5;
    }
    
    public boolean isThumbnailCompressed() {
        if (!this.mHasThumbnail) {
            return false;
        }
        final int mThumbnailCompression = this.mThumbnailCompression;
        return mThumbnailCompression == 6 || mThumbnailCompression == 7;
    }
    
    public void resetOrientation() {
        this.setAttribute("Orientation", Integer.toString(1));
    }
    
    public void rotate(int n) {
        if (n % 90 == 0) {
            final int attributeInt = this.getAttributeInt("Orientation", 1);
            final List<Integer> rotation_ORDER = ExifInterface.ROTATION_ORDER;
            final boolean contains = rotation_ORDER.contains(attributeInt);
            final int n2 = 0;
            final int n3 = 0;
            final int n4 = 0;
            int i;
            if (contains) {
                final int n5 = (rotation_ORDER.indexOf(attributeInt) + n / 90) % 4;
                n = n4;
                if (n5 < 0) {
                    n = 4;
                }
                i = rotation_ORDER.get(n5 + n);
            }
            else {
                final List<Integer> flipped_ROTATION_ORDER = ExifInterface.FLIPPED_ROTATION_ORDER;
                i = n3;
                if (flipped_ROTATION_ORDER.contains(attributeInt)) {
                    final int n6 = (flipped_ROTATION_ORDER.indexOf(attributeInt) + n / 90) % 4;
                    n = n2;
                    if (n6 < 0) {
                        n = 4;
                    }
                    i = (int)flipped_ROTATION_ORDER.get(n6 + n);
                }
            }
            this.setAttribute("Orientation", Integer.toString(i));
            return;
        }
        throw new IllegalArgumentException("degree should be a multiple of 90");
    }
    
    public void saveAttributes() throws IOException {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: getfield        androidx/exifinterface/media/ExifInterface.mMimeType:I
        //     4: invokestatic    androidx/exifinterface/media/ExifInterface.isSupportedFormatForSavingAttributes:(I)Z
        //     7: ifeq            993
        //    10: aload_0        
        //    11: getfield        androidx/exifinterface/media/ExifInterface.mSeekableFileDescriptor:Ljava/io/FileDescriptor;
        //    14: ifnonnull       38
        //    17: aload_0        
        //    18: getfield        androidx/exifinterface/media/ExifInterface.mFilename:Ljava/lang/String;
        //    21: ifnull          27
        //    24: goto            38
        //    27: new             Ljava/io/IOException;
        //    30: dup            
        //    31: ldc_w           "ExifInterface does not support saving attributes for the current input."
        //    34: invokespecial   java/io/IOException.<init>:(Ljava/lang/String;)V
        //    37: athrow         
        //    38: aload_0        
        //    39: getfield        androidx/exifinterface/media/ExifInterface.mHasThumbnail:Z
        //    42: ifeq            73
        //    45: aload_0        
        //    46: getfield        androidx/exifinterface/media/ExifInterface.mHasThumbnailStrips:Z
        //    49: ifeq            73
        //    52: aload_0        
        //    53: getfield        androidx/exifinterface/media/ExifInterface.mAreThumbnailStripsConsecutive:Z
        //    56: ifeq            62
        //    59: goto            73
        //    62: new             Ljava/io/IOException;
        //    65: dup            
        //    66: ldc_w           "ExifInterface does not support saving attributes when the image file has non-consecutive thumbnail strips"
        //    69: invokespecial   java/io/IOException.<init>:(Ljava/lang/String;)V
        //    72: athrow         
        //    73: aload_0        
        //    74: iconst_1       
        //    75: putfield        androidx/exifinterface/media/ExifInterface.mModified:Z
        //    78: aload_0        
        //    79: aload_0        
        //    80: invokevirtual   androidx/exifinterface/media/ExifInterface.getThumbnail:()[B
        //    83: putfield        androidx/exifinterface/media/ExifInterface.mThumbnailBytes:[B
        //    86: aconst_null    
        //    87: astore          7
        //    89: aconst_null    
        //    90: astore          5
        //    92: aconst_null    
        //    93: astore          6
        //    95: aconst_null    
        //    96: astore          8
        //    98: ldc_w           "temp"
        //   101: ldc_w           "tmp"
        //   104: invokestatic    java/io/File.createTempFile:(Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;
        //   107: astore          13
        //   109: aload_0        
        //   110: getfield        androidx/exifinterface/media/ExifInterface.mFilename:Ljava/lang/String;
        //   113: ifnull          137
        //   116: new             Ljava/io/FileInputStream;
        //   119: astore          9
        //   121: aload           9
        //   123: aload_0        
        //   124: getfield        androidx/exifinterface/media/ExifInterface.mFilename:Ljava/lang/String;
        //   127: invokespecial   java/io/FileInputStream.<init>:(Ljava/lang/String;)V
        //   130: aload           9
        //   132: astore          5
        //   134: goto            180
        //   137: getstatic       android/os/Build$VERSION.SDK_INT:I
        //   140: bipush          21
        //   142: if_icmplt       177
        //   145: aload_0        
        //   146: getfield        androidx/exifinterface/media/ExifInterface.mSeekableFileDescriptor:Ljava/io/FileDescriptor;
        //   149: lconst_0       
        //   150: getstatic       android/system/OsConstants.SEEK_SET:I
        //   153: invokestatic    androidx/exifinterface/media/ExifInterfaceUtils$Api21Impl.lseek:(Ljava/io/FileDescriptor;JI)J
        //   156: pop2           
        //   157: new             Ljava/io/FileInputStream;
        //   160: dup            
        //   161: aload_0        
        //   162: getfield        androidx/exifinterface/media/ExifInterface.mSeekableFileDescriptor:Ljava/io/FileDescriptor;
        //   165: invokespecial   java/io/FileInputStream.<init>:(Ljava/io/FileDescriptor;)V
        //   168: astore          9
        //   170: aload           9
        //   172: astore          5
        //   174: goto            180
        //   177: aconst_null    
        //   178: astore          5
        //   180: new             Ljava/io/FileOutputStream;
        //   183: astore          6
        //   185: aload           6
        //   187: aload           13
        //   189: invokespecial   java/io/FileOutputStream.<init>:(Ljava/io/File;)V
        //   192: aload           5
        //   194: aload           6
        //   196: invokestatic    androidx/exifinterface/media/ExifInterfaceUtils.copy:(Ljava/io/InputStream;Ljava/io/OutputStream;)I
        //   199: pop            
        //   200: aload           5
        //   202: invokestatic    androidx/exifinterface/media/ExifInterfaceUtils.closeQuietly:(Ljava/io/Closeable;)V
        //   205: aload           6
        //   207: invokestatic    androidx/exifinterface/media/ExifInterfaceUtils.closeQuietly:(Ljava/io/Closeable;)V
        //   210: iconst_0       
        //   211: istore_3       
        //   212: iconst_0       
        //   213: istore          4
        //   215: iconst_0       
        //   216: istore_1       
        //   217: iconst_0       
        //   218: istore_2       
        //   219: new             Ljava/io/FileInputStream;
        //   222: astore          6
        //   224: aload           6
        //   226: aload           13
        //   228: invokespecial   java/io/FileInputStream.<init>:(Ljava/io/File;)V
        //   231: aload_0        
        //   232: getfield        androidx/exifinterface/media/ExifInterface.mFilename:Ljava/lang/String;
        //   235: ifnull          255
        //   238: new             Ljava/io/FileOutputStream;
        //   241: astore          5
        //   243: aload           5
        //   245: aload_0        
        //   246: getfield        androidx/exifinterface/media/ExifInterface.mFilename:Ljava/lang/String;
        //   249: invokespecial   java/io/FileOutputStream.<init>:(Ljava/lang/String;)V
        //   252: goto            294
        //   255: getstatic       android/os/Build$VERSION.SDK_INT:I
        //   258: bipush          21
        //   260: if_icmplt       291
        //   263: aload_0        
        //   264: getfield        androidx/exifinterface/media/ExifInterface.mSeekableFileDescriptor:Ljava/io/FileDescriptor;
        //   267: lconst_0       
        //   268: getstatic       android/system/OsConstants.SEEK_SET:I
        //   271: invokestatic    androidx/exifinterface/media/ExifInterfaceUtils$Api21Impl.lseek:(Ljava/io/FileDescriptor;JI)J
        //   274: pop2           
        //   275: new             Ljava/io/FileOutputStream;
        //   278: dup            
        //   279: aload_0        
        //   280: getfield        androidx/exifinterface/media/ExifInterface.mSeekableFileDescriptor:Ljava/io/FileDescriptor;
        //   283: invokespecial   java/io/FileOutputStream.<init>:(Ljava/io/FileDescriptor;)V
        //   286: astore          5
        //   288: goto            294
        //   291: aconst_null    
        //   292: astore          5
        //   294: new             Ljava/io/BufferedInputStream;
        //   297: astore          8
        //   299: aload           8
        //   301: aload           6
        //   303: invokespecial   java/io/BufferedInputStream.<init>:(Ljava/io/InputStream;)V
        //   306: new             Ljava/io/BufferedOutputStream;
        //   309: astore          7
        //   311: aload           7
        //   313: aload           5
        //   315: invokespecial   java/io/BufferedOutputStream.<init>:(Ljava/io/OutputStream;)V
        //   318: iload_3        
        //   319: istore_1       
        //   320: aload           8
        //   322: astore          10
        //   324: aload           7
        //   326: astore          11
        //   328: aload_0        
        //   329: getfield        androidx/exifinterface/media/ExifInterface.mMimeType:I
        //   332: istore          4
        //   334: iload           4
        //   336: iconst_4       
        //   337: if_icmpne       361
        //   340: iload_3        
        //   341: istore_1       
        //   342: aload           8
        //   344: astore          10
        //   346: aload           7
        //   348: astore          11
        //   350: aload_0        
        //   351: aload           8
        //   353: aload           7
        //   355: invokespecial   androidx/exifinterface/media/ExifInterface.saveJpegAttributes:(Ljava/io/InputStream;Ljava/io/OutputStream;)V
        //   358: goto            414
        //   361: iload           4
        //   363: bipush          13
        //   365: if_icmpne       389
        //   368: iload_3        
        //   369: istore_1       
        //   370: aload           8
        //   372: astore          10
        //   374: aload           7
        //   376: astore          11
        //   378: aload_0        
        //   379: aload           8
        //   381: aload           7
        //   383: invokespecial   androidx/exifinterface/media/ExifInterface.savePngAttributes:(Ljava/io/InputStream;Ljava/io/OutputStream;)V
        //   386: goto            414
        //   389: iload           4
        //   391: bipush          14
        //   393: if_icmpne       414
        //   396: iload_3        
        //   397: istore_1       
        //   398: aload           8
        //   400: astore          10
        //   402: aload           7
        //   404: astore          11
        //   406: aload_0        
        //   407: aload           8
        //   409: aload           7
        //   411: invokespecial   androidx/exifinterface/media/ExifInterface.saveWebpAttributes:(Ljava/io/InputStream;Ljava/io/OutputStream;)V
        //   414: aload           8
        //   416: invokestatic    androidx/exifinterface/media/ExifInterfaceUtils.closeQuietly:(Ljava/io/Closeable;)V
        //   419: aload           7
        //   421: invokestatic    androidx/exifinterface/media/ExifInterfaceUtils.closeQuietly:(Ljava/io/Closeable;)V
        //   424: aload           13
        //   426: invokevirtual   java/io/File.delete:()Z
        //   429: pop            
        //   430: aload_0        
        //   431: aconst_null    
        //   432: putfield        androidx/exifinterface/media/ExifInterface.mThumbnailBytes:[B
        //   435: return         
        //   436: astore          9
        //   438: goto            507
        //   441: astore          5
        //   443: aconst_null    
        //   444: astore          11
        //   446: iload           4
        //   448: istore_1       
        //   449: goto            866
        //   452: astore          9
        //   454: aconst_null    
        //   455: astore          7
        //   457: goto            507
        //   460: astore          9
        //   462: aconst_null    
        //   463: astore          7
        //   465: aconst_null    
        //   466: astore          8
        //   468: goto            507
        //   471: astore          5
        //   473: goto            494
        //   476: astore          5
        //   478: aconst_null    
        //   479: astore          11
        //   481: aload           7
        //   483: astore          6
        //   485: goto            870
        //   488: astore          5
        //   490: aload           8
        //   492: astore          6
        //   494: aconst_null    
        //   495: astore          7
        //   497: aconst_null    
        //   498: astore          8
        //   500: aload           5
        //   502: astore          9
        //   504: aconst_null    
        //   505: astore          5
        //   507: new             Ljava/io/FileInputStream;
        //   510: astore          12
        //   512: aload           12
        //   514: aload           13
        //   516: invokespecial   java/io/FileInputStream.<init>:(Ljava/io/File;)V
        //   519: aload           5
        //   521: astore          10
        //   523: aload           5
        //   525: astore          11
        //   527: aload_0        
        //   528: getfield        androidx/exifinterface/media/ExifInterface.mFilename:Ljava/lang/String;
        //   531: ifnonnull       611
        //   534: aload           5
        //   536: astore          6
        //   538: aload           5
        //   540: astore          10
        //   542: aload           5
        //   544: astore          11
        //   546: getstatic       android/os/Build$VERSION.SDK_INT:I
        //   549: bipush          21
        //   551: if_icmplt       636
        //   554: aload           5
        //   556: astore          10
        //   558: aload           5
        //   560: astore          11
        //   562: aload_0        
        //   563: getfield        androidx/exifinterface/media/ExifInterface.mSeekableFileDescriptor:Ljava/io/FileDescriptor;
        //   566: lconst_0       
        //   567: getstatic       android/system/OsConstants.SEEK_SET:I
        //   570: invokestatic    androidx/exifinterface/media/ExifInterfaceUtils$Api21Impl.lseek:(Ljava/io/FileDescriptor;JI)J
        //   573: pop2           
        //   574: aload           5
        //   576: astore          10
        //   578: aload           5
        //   580: astore          11
        //   582: new             Ljava/io/FileOutputStream;
        //   585: astore          6
        //   587: aload           5
        //   589: astore          10
        //   591: aload           5
        //   593: astore          11
        //   595: aload           6
        //   597: aload_0        
        //   598: getfield        androidx/exifinterface/media/ExifInterface.mSeekableFileDescriptor:Ljava/io/FileDescriptor;
        //   601: invokespecial   java/io/FileOutputStream.<init>:(Ljava/io/FileDescriptor;)V
        //   604: aload           6
        //   606: astore          5
        //   608: goto            632
        //   611: aload           5
        //   613: astore          10
        //   615: aload           5
        //   617: astore          11
        //   619: new             Ljava/io/FileOutputStream;
        //   622: dup            
        //   623: aload_0        
        //   624: getfield        androidx/exifinterface/media/ExifInterface.mFilename:Ljava/lang/String;
        //   627: invokespecial   java/io/FileOutputStream.<init>:(Ljava/lang/String;)V
        //   630: astore          5
        //   632: aload           5
        //   634: astore          6
        //   636: aload           6
        //   638: astore          10
        //   640: aload           6
        //   642: astore          11
        //   644: aload           12
        //   646: aload           6
        //   648: invokestatic    androidx/exifinterface/media/ExifInterfaceUtils.copy:(Ljava/io/InputStream;Ljava/io/OutputStream;)I
        //   651: pop            
        //   652: iload_3        
        //   653: istore_1       
        //   654: aload           8
        //   656: astore          10
        //   658: aload           7
        //   660: astore          11
        //   662: aload           12
        //   664: invokestatic    androidx/exifinterface/media/ExifInterfaceUtils.closeQuietly:(Ljava/io/Closeable;)V
        //   667: iload_3        
        //   668: istore_1       
        //   669: aload           8
        //   671: astore          10
        //   673: aload           7
        //   675: astore          11
        //   677: aload           6
        //   679: invokestatic    androidx/exifinterface/media/ExifInterfaceUtils.closeQuietly:(Ljava/io/Closeable;)V
        //   682: iload_3        
        //   683: istore_1       
        //   684: aload           8
        //   686: astore          10
        //   688: aload           7
        //   690: astore          11
        //   692: new             Ljava/io/IOException;
        //   695: astore          5
        //   697: iload_3        
        //   698: istore_1       
        //   699: aload           8
        //   701: astore          10
        //   703: aload           7
        //   705: astore          11
        //   707: aload           5
        //   709: ldc_w           "Failed to save new file"
        //   712: aload           9
        //   714: invokespecial   java/io/IOException.<init>:(Ljava/lang/String;Ljava/lang/Throwable;)V
        //   717: iload_3        
        //   718: istore_1       
        //   719: aload           8
        //   721: astore          10
        //   723: aload           7
        //   725: astore          11
        //   727: aload           5
        //   729: athrow         
        //   730: astore          9
        //   732: aload           12
        //   734: astore          6
        //   736: aload           10
        //   738: astore          5
        //   740: goto            817
        //   743: astore          9
        //   745: aload           11
        //   747: astore          5
        //   749: aload           12
        //   751: astore          6
        //   753: goto            763
        //   756: astore          9
        //   758: goto            817
        //   761: astore          9
        //   763: new             Ljava/io/IOException;
        //   766: astore          10
        //   768: new             Ljava/lang/StringBuilder;
        //   771: astore          11
        //   773: aload           11
        //   775: invokespecial   java/lang/StringBuilder.<init>:()V
        //   778: aload           11
        //   780: ldc_w           "Failed to save new file. Original file is stored in "
        //   783: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   786: pop            
        //   787: aload           11
        //   789: aload           13
        //   791: invokevirtual   java/io/File.getAbsolutePath:()Ljava/lang/String;
        //   794: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   797: pop            
        //   798: aload           10
        //   800: aload           11
        //   802: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   805: aload           9
        //   807: invokespecial   java/io/IOException.<init>:(Ljava/lang/String;Ljava/lang/Throwable;)V
        //   810: aload           10
        //   812: athrow         
        //   813: astore          9
        //   815: iconst_1       
        //   816: istore_2       
        //   817: iload_2        
        //   818: istore_1       
        //   819: aload           8
        //   821: astore          10
        //   823: aload           7
        //   825: astore          11
        //   827: aload           6
        //   829: invokestatic    androidx/exifinterface/media/ExifInterfaceUtils.closeQuietly:(Ljava/io/Closeable;)V
        //   832: iload_2        
        //   833: istore_1       
        //   834: aload           8
        //   836: astore          10
        //   838: aload           7
        //   840: astore          11
        //   842: aload           5
        //   844: invokestatic    androidx/exifinterface/media/ExifInterfaceUtils.closeQuietly:(Ljava/io/Closeable;)V
        //   847: iload_2        
        //   848: istore_1       
        //   849: aload           8
        //   851: astore          10
        //   853: aload           7
        //   855: astore          11
        //   857: aload           9
        //   859: athrow         
        //   860: astore          5
        //   862: aload           10
        //   864: astore          8
        //   866: aload           8
        //   868: astore          6
        //   870: aload           6
        //   872: invokestatic    androidx/exifinterface/media/ExifInterfaceUtils.closeQuietly:(Ljava/io/Closeable;)V
        //   875: aload           11
        //   877: invokestatic    androidx/exifinterface/media/ExifInterfaceUtils.closeQuietly:(Ljava/io/Closeable;)V
        //   880: iload_1        
        //   881: ifne            890
        //   884: aload           13
        //   886: invokevirtual   java/io/File.delete:()Z
        //   889: pop            
        //   890: aload           5
        //   892: athrow         
        //   893: astore          7
        //   895: goto            908
        //   898: astore          7
        //   900: goto            928
        //   903: astore          7
        //   905: aconst_null    
        //   906: astore          6
        //   908: aload           5
        //   910: astore          8
        //   912: aload           7
        //   914: astore          5
        //   916: aload           8
        //   918: astore          7
        //   920: goto            980
        //   923: astore          7
        //   925: aconst_null    
        //   926: astore          6
        //   928: goto            952
        //   931: astore          5
        //   933: aconst_null    
        //   934: astore          8
        //   936: aload           6
        //   938: astore          7
        //   940: aload           8
        //   942: astore          6
        //   944: goto            980
        //   947: astore          7
        //   949: aconst_null    
        //   950: astore          6
        //   952: new             Ljava/io/IOException;
        //   955: astore          8
        //   957: aload           8
        //   959: ldc_w           "Failed to copy original file to temp file"
        //   962: aload           7
        //   964: invokespecial   java/io/IOException.<init>:(Ljava/lang/String;Ljava/lang/Throwable;)V
        //   967: aload           8
        //   969: athrow         
        //   970: astore          8
        //   972: aload           5
        //   974: astore          7
        //   976: aload           8
        //   978: astore          5
        //   980: aload           7
        //   982: invokestatic    androidx/exifinterface/media/ExifInterfaceUtils.closeQuietly:(Ljava/io/Closeable;)V
        //   985: aload           6
        //   987: invokestatic    androidx/exifinterface/media/ExifInterfaceUtils.closeQuietly:(Ljava/io/Closeable;)V
        //   990: aload           5
        //   992: athrow         
        //   993: new             Ljava/io/IOException;
        //   996: dup            
        //   997: ldc_w           "ExifInterface only supports saving attributes for JPEG, PNG, and WebP formats."
        //  1000: invokespecial   java/io/IOException.<init>:(Ljava/lang/String;)V
        //  1003: athrow         
        //    Exceptions:
        //  throws java.io.IOException
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                 
        //  -----  -----  -----  -----  ---------------------
        //  98     130    947    952    Ljava/lang/Exception;
        //  98     130    931    947    Any
        //  137    170    947    952    Ljava/lang/Exception;
        //  137    170    931    947    Any
        //  180    192    923    928    Ljava/lang/Exception;
        //  180    192    903    908    Any
        //  192    200    898    903    Ljava/lang/Exception;
        //  192    200    893    898    Any
        //  219    231    488    494    Ljava/lang/Exception;
        //  219    231    476    488    Any
        //  231    252    471    476    Ljava/lang/Exception;
        //  231    252    476    488    Any
        //  255    288    471    476    Ljava/lang/Exception;
        //  255    288    476    488    Any
        //  294    306    460    471    Ljava/lang/Exception;
        //  294    306    476    488    Any
        //  306    318    452    460    Ljava/lang/Exception;
        //  306    318    441    452    Any
        //  328    334    436    441    Ljava/lang/Exception;
        //  328    334    860    866    Any
        //  350    358    436    441    Ljava/lang/Exception;
        //  350    358    860    866    Any
        //  378    386    436    441    Ljava/lang/Exception;
        //  378    386    860    866    Any
        //  406    414    436    441    Ljava/lang/Exception;
        //  406    414    860    866    Any
        //  507    519    761    763    Ljava/lang/Exception;
        //  507    519    756    761    Any
        //  527    534    743    756    Ljava/lang/Exception;
        //  527    534    730    743    Any
        //  546    554    743    756    Ljava/lang/Exception;
        //  546    554    730    743    Any
        //  562    574    743    756    Ljava/lang/Exception;
        //  562    574    730    743    Any
        //  582    587    743    756    Ljava/lang/Exception;
        //  582    587    730    743    Any
        //  595    604    743    756    Ljava/lang/Exception;
        //  595    604    730    743    Any
        //  619    632    743    756    Ljava/lang/Exception;
        //  619    632    730    743    Any
        //  644    652    743    756    Ljava/lang/Exception;
        //  644    652    730    743    Any
        //  662    667    860    866    Any
        //  677    682    860    866    Any
        //  692    697    860    866    Any
        //  707    717    860    866    Any
        //  727    730    860    866    Any
        //  763    813    813    817    Any
        //  827    832    860    866    Any
        //  842    847    860    866    Any
        //  857    860    860    866    Any
        //  952    970    970    980    Any
        // 
        // The error that occurred was:
        // 
        // java.lang.UnsupportedOperationException
        //     at java.base/java.util.Collections$1.remove(Collections.java:4820)
        //     at java.base/java.util.AbstractCollection.removeAll(AbstractCollection.java:369)
        //     at com.strobel.decompiler.ast.AstBuilder.convertLocalVariables(AstBuilder.java:3018)
        //     at com.strobel.decompiler.ast.AstBuilder.performStackAnalysis(AstBuilder.java:2501)
        //     at com.strobel.decompiler.ast.AstBuilder.build(AstBuilder.java:108)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:203)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public void setAltitude(final double a) {
        String s;
        if (a >= 0.0) {
            s = "0";
        }
        else {
            s = "1";
        }
        this.setAttribute("GPSAltitude", new Rational(Math.abs(a)).toString());
        this.setAttribute("GPSAltitudeRef", s);
    }
    
    public void setAttribute(String anObject, String s) {
        final String str = s;
        if (anObject != null) {
            String replaceAll = null;
            Label_0172: {
                if (!"DateTime".equals(anObject) && !"DateTimeOriginal".equals(anObject)) {
                    replaceAll = str;
                    if (!"DateTimeDigitized".equals(anObject)) {
                        break Label_0172;
                    }
                }
                if ((replaceAll = str) != null) {
                    final boolean find = ExifInterface.DATETIME_PRIMARY_FORMAT_PATTERN.matcher(str).find();
                    final boolean find2 = ExifInterface.DATETIME_SECONDARY_FORMAT_PATTERN.matcher(str).find();
                    if (s.length() != 19 || (!find && !find2)) {
                        final StringBuilder sb = new StringBuilder();
                        sb.append("Invalid value for ");
                        sb.append(anObject);
                        sb.append(" : ");
                        sb.append(str);
                        Log.w("ExifInterface", sb.toString());
                        return;
                    }
                    replaceAll = str;
                    if (find2) {
                        replaceAll = str.replaceAll("-", ":");
                    }
                }
            }
            s = anObject;
            if ("ISOSpeedRatings".equals(anObject)) {
                if (ExifInterface.DEBUG) {
                    Log.d("ExifInterface", "setAttribute: Replacing TAG_ISO_SPEED_RATINGS with TAG_PHOTOGRAPHIC_SENSITIVITY.");
                }
                s = "PhotographicSensitivity";
            }
            if ((anObject = replaceAll) != null) {
                anObject = replaceAll;
                if (ExifInterface.sTagSetForCompatibility.contains(s)) {
                    if (s.equals("GPSTimeStamp")) {
                        final Matcher matcher = ExifInterface.GPS_TIMESTAMP_PATTERN.matcher(replaceAll);
                        if (!matcher.find()) {
                            final StringBuilder sb2 = new StringBuilder();
                            sb2.append("Invalid value for ");
                            sb2.append(s);
                            sb2.append(" : ");
                            sb2.append(replaceAll);
                            Log.w("ExifInterface", sb2.toString());
                            return;
                        }
                        final StringBuilder sb3 = new StringBuilder();
                        sb3.append(Integer.parseInt(matcher.group(1)));
                        sb3.append("/1,");
                        sb3.append(Integer.parseInt(matcher.group(2)));
                        sb3.append("/1,");
                        sb3.append(Integer.parseInt(matcher.group(3)));
                        sb3.append("/1");
                        anObject = sb3.toString();
                    }
                    else {
                        try {
                            anObject = new Rational(Double.parseDouble(replaceAll)).toString();
                        }
                        catch (final NumberFormatException ex) {
                            final StringBuilder sb4 = new StringBuilder();
                            sb4.append("Invalid value for ");
                            sb4.append(s);
                            sb4.append(" : ");
                            sb4.append(replaceAll);
                            Log.w("ExifInterface", sb4.toString());
                            return;
                        }
                    }
                }
            }
            for (int i = 0; i < ExifInterface.EXIF_TAGS.length; ++i) {
                if (i != 4 || this.mHasThumbnail) {
                    final ExifTag exifTag = ExifInterface.sExifTagMapsForWriting[i].get(s);
                    if (exifTag != null) {
                        if (anObject == null) {
                            this.mAttributes[i].remove(s);
                        }
                        else {
                            final Pair<Integer, Integer> guessDataFormat = guessDataFormat(anObject);
                            int j;
                            if (exifTag.primaryFormat != (int)guessDataFormat.first && exifTag.primaryFormat != (int)guessDataFormat.second) {
                                if (exifTag.secondaryFormat != -1 && (exifTag.secondaryFormat == (int)guessDataFormat.first || exifTag.secondaryFormat == (int)guessDataFormat.second)) {
                                    j = exifTag.secondaryFormat;
                                }
                                else if (exifTag.primaryFormat != 1 && exifTag.primaryFormat != 7 && exifTag.primaryFormat != 2) {
                                    if (ExifInterface.DEBUG) {
                                        final StringBuilder sb5 = new StringBuilder();
                                        sb5.append("Given tag (");
                                        sb5.append(s);
                                        sb5.append(") value didn't match with one of expected formats: ");
                                        final String[] ifd_FORMAT_NAMES = ExifInterface.IFD_FORMAT_NAMES;
                                        sb5.append(ifd_FORMAT_NAMES[exifTag.primaryFormat]);
                                        final int secondaryFormat = exifTag.secondaryFormat;
                                        final String s2 = "";
                                        String string;
                                        if (secondaryFormat == -1) {
                                            string = "";
                                        }
                                        else {
                                            final StringBuilder sb6 = new StringBuilder();
                                            sb6.append(", ");
                                            sb6.append(ifd_FORMAT_NAMES[exifTag.secondaryFormat]);
                                            string = sb6.toString();
                                        }
                                        sb5.append(string);
                                        sb5.append(" (guess: ");
                                        sb5.append(ifd_FORMAT_NAMES[(int)guessDataFormat.first]);
                                        String string2;
                                        if ((int)guessDataFormat.second == -1) {
                                            string2 = s2;
                                        }
                                        else {
                                            final StringBuilder sb7 = new StringBuilder();
                                            sb7.append(", ");
                                            sb7.append(ifd_FORMAT_NAMES[(int)guessDataFormat.second]);
                                            string2 = sb7.toString();
                                        }
                                        sb5.append(string2);
                                        sb5.append(")");
                                        Log.d("ExifInterface", sb5.toString());
                                    }
                                    continue;
                                }
                                else {
                                    j = exifTag.primaryFormat;
                                }
                            }
                            else {
                                j = exifTag.primaryFormat;
                            }
                            switch (j) {
                                default: {
                                    if (ExifInterface.DEBUG) {
                                        final StringBuilder sb8 = new StringBuilder();
                                        sb8.append("Data format isn't one of expected formats: ");
                                        sb8.append(j);
                                        Log.d("ExifInterface", sb8.toString());
                                        break;
                                    }
                                    break;
                                }
                                case 12: {
                                    final String[] split = anObject.split(",", -1);
                                    final double[] array = new double[split.length];
                                    for (int k = 0; k < split.length; ++k) {
                                        array[k] = Double.parseDouble(split[k]);
                                    }
                                    this.mAttributes[i].put(s, ExifAttribute.createDouble(array, this.mExifByteOrder));
                                    break;
                                }
                                case 10: {
                                    final String[] split2 = anObject.split(",", -1);
                                    final Rational[] array2 = new Rational[split2.length];
                                    for (int l = 0; l < split2.length; ++l) {
                                        final String[] split3 = split2[l].split("/", -1);
                                        array2[l] = new Rational((long)Double.parseDouble(split3[0]), (long)Double.parseDouble(split3[1]));
                                    }
                                    this.mAttributes[i].put(s, ExifAttribute.createSRational(array2, this.mExifByteOrder));
                                    break;
                                }
                                case 9: {
                                    final String[] split4 = anObject.split(",", -1);
                                    final int[] array3 = new int[split4.length];
                                    for (int n = 0; n < split4.length; ++n) {
                                        array3[n] = Integer.parseInt(split4[n]);
                                    }
                                    this.mAttributes[i].put(s, ExifAttribute.createSLong(array3, this.mExifByteOrder));
                                    break;
                                }
                                case 5: {
                                    final String[] split5 = anObject.split(",", -1);
                                    final Rational[] array4 = new Rational[split5.length];
                                    for (int n2 = 0; n2 < split5.length; ++n2) {
                                        final String[] split6 = split5[n2].split("/", -1);
                                        array4[n2] = new Rational((long)Double.parseDouble(split6[0]), (long)Double.parseDouble(split6[1]));
                                    }
                                    this.mAttributes[i].put(s, ExifAttribute.createURational(array4, this.mExifByteOrder));
                                    break;
                                }
                                case 4: {
                                    final String[] split7 = anObject.split(",", -1);
                                    final long[] array5 = new long[split7.length];
                                    for (int n3 = 0; n3 < split7.length; ++n3) {
                                        array5[n3] = Long.parseLong(split7[n3]);
                                    }
                                    this.mAttributes[i].put(s, ExifAttribute.createULong(array5, this.mExifByteOrder));
                                    break;
                                }
                                case 3: {
                                    final String[] split8 = anObject.split(",", -1);
                                    final int[] array6 = new int[split8.length];
                                    for (int n4 = 0; n4 < split8.length; ++n4) {
                                        array6[n4] = Integer.parseInt(split8[n4]);
                                    }
                                    this.mAttributes[i].put(s, ExifAttribute.createUShort(array6, this.mExifByteOrder));
                                    break;
                                }
                                case 2:
                                case 7: {
                                    this.mAttributes[i].put(s, ExifAttribute.createString(anObject));
                                    break;
                                }
                                case 1: {
                                    this.mAttributes[i].put(s, ExifAttribute.createByte(anObject));
                                    break;
                                }
                            }
                        }
                    }
                }
            }
            return;
        }
        throw new NullPointerException("tag shouldn't be null");
    }
    
    public void setDateTime(final Long n) {
        if (n == null) {
            throw new NullPointerException("Timestamp should not be null.");
        }
        if (n >= 0L) {
            String str = Long.toString(n % 1000L);
            for (int i = str.length(); i < 3; ++i) {
                final StringBuilder sb = new StringBuilder();
                sb.append("0");
                sb.append(str);
                str = sb.toString();
            }
            this.setAttribute("DateTime", ExifInterface.sFormatterPrimary.format(new Date(n)));
            this.setAttribute("SubSecTime", str);
            return;
        }
        throw new IllegalArgumentException("Timestamp should a positive value.");
    }
    
    public void setGpsInfo(final Location location) {
        if (location == null) {
            return;
        }
        this.setAttribute("GPSProcessingMethod", location.getProvider());
        this.setLatLong(location.getLatitude(), location.getLongitude());
        this.setAltitude(location.getAltitude());
        this.setAttribute("GPSSpeedRef", "K");
        this.setAttribute("GPSSpeed", new Rational(location.getSpeed() * TimeUnit.HOURS.toSeconds(1L) / 1000.0f).toString());
        final String[] split = ExifInterface.sFormatterPrimary.format(new Date(location.getTime())).split("\\s+", -1);
        this.setAttribute("GPSDateStamp", split[0]);
        this.setAttribute("GPSTimeStamp", split[1]);
    }
    
    public void setLatLong(final double a, final double d) {
        if (a < -90.0 || a > 90.0 || Double.isNaN(a)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Latitude value ");
            sb.append(a);
            sb.append(" is not valid.");
            throw new IllegalArgumentException(sb.toString());
        }
        if (d >= -180.0 && d <= 180.0 && !Double.isNaN(d)) {
            String s;
            if (a >= 0.0) {
                s = "N";
            }
            else {
                s = "S";
            }
            this.setAttribute("GPSLatitudeRef", s);
            this.setAttribute("GPSLatitude", this.convertDecimalDegree(Math.abs(a)));
            String s2;
            if (d >= 0.0) {
                s2 = "E";
            }
            else {
                s2 = "W";
            }
            this.setAttribute("GPSLongitudeRef", s2);
            this.setAttribute("GPSLongitude", this.convertDecimalDegree(Math.abs(d)));
            return;
        }
        final StringBuilder sb2 = new StringBuilder();
        sb2.append("Longitude value ");
        sb2.append(d);
        sb2.append(" is not valid.");
        throw new IllegalArgumentException(sb2.toString());
    }
    
    private static class ByteOrderedDataInputStream extends InputStream implements DataInput
    {
        private static final ByteOrder BIG_ENDIAN;
        private static final ByteOrder LITTLE_ENDIAN;
        private ByteOrder mByteOrder;
        final DataInputStream mDataInputStream;
        int mPosition;
        private byte[] mSkipBuffer;
        
        static {
            LITTLE_ENDIAN = ByteOrder.LITTLE_ENDIAN;
            BIG_ENDIAN = ByteOrder.BIG_ENDIAN;
        }
        
        ByteOrderedDataInputStream(final InputStream inputStream) throws IOException {
            this(inputStream, ByteOrder.BIG_ENDIAN);
        }
        
        ByteOrderedDataInputStream(final InputStream in, final ByteOrder mByteOrder) throws IOException {
            this.mByteOrder = ByteOrder.BIG_ENDIAN;
            (this.mDataInputStream = new DataInputStream(in)).mark(0);
            this.mPosition = 0;
            this.mByteOrder = mByteOrder;
        }
        
        ByteOrderedDataInputStream(final byte[] buf) throws IOException {
            this(new ByteArrayInputStream(buf), ByteOrder.BIG_ENDIAN);
        }
        
        @Override
        public int available() throws IOException {
            return this.mDataInputStream.available();
        }
        
        @Override
        public void mark(final int n) {
            throw new UnsupportedOperationException("Mark is currently unsupported");
        }
        
        public int position() {
            return this.mPosition;
        }
        
        @Override
        public int read() throws IOException {
            ++this.mPosition;
            return this.mDataInputStream.read();
        }
        
        @Override
        public int read(final byte[] b, int read, final int len) throws IOException {
            read = this.mDataInputStream.read(b, read, len);
            this.mPosition += read;
            return read;
        }
        
        @Override
        public boolean readBoolean() throws IOException {
            ++this.mPosition;
            return this.mDataInputStream.readBoolean();
        }
        
        @Override
        public byte readByte() throws IOException {
            ++this.mPosition;
            final int read = this.mDataInputStream.read();
            if (read >= 0) {
                return (byte)read;
            }
            throw new EOFException();
        }
        
        @Override
        public char readChar() throws IOException {
            this.mPosition += 2;
            return this.mDataInputStream.readChar();
        }
        
        @Override
        public double readDouble() throws IOException {
            return Double.longBitsToDouble(this.readLong());
        }
        
        @Override
        public float readFloat() throws IOException {
            return Float.intBitsToFloat(this.readInt());
        }
        
        @Override
        public void readFully(final byte[] b) throws IOException {
            this.mPosition += b.length;
            this.mDataInputStream.readFully(b);
        }
        
        @Override
        public void readFully(final byte[] b, final int off, final int len) throws IOException {
            this.mPosition += len;
            this.mDataInputStream.readFully(b, off, len);
        }
        
        @Override
        public int readInt() throws IOException {
            this.mPosition += 4;
            final int read = this.mDataInputStream.read();
            final int read2 = this.mDataInputStream.read();
            final int read3 = this.mDataInputStream.read();
            final int read4 = this.mDataInputStream.read();
            if ((read | read2 | read3 | read4) < 0) {
                throw new EOFException();
            }
            final ByteOrder mByteOrder = this.mByteOrder;
            if (mByteOrder == ByteOrderedDataInputStream.LITTLE_ENDIAN) {
                return (read4 << 24) + (read3 << 16) + (read2 << 8) + read;
            }
            if (mByteOrder == ByteOrderedDataInputStream.BIG_ENDIAN) {
                return (read << 24) + (read2 << 16) + (read3 << 8) + read4;
            }
            final StringBuilder sb = new StringBuilder();
            sb.append("Invalid byte order: ");
            sb.append(this.mByteOrder);
            throw new IOException(sb.toString());
        }
        
        @Override
        public String readLine() throws IOException {
            Log.d("ExifInterface", "Currently unsupported");
            return null;
        }
        
        @Override
        public long readLong() throws IOException {
            this.mPosition += 8;
            final int read = this.mDataInputStream.read();
            final int read2 = this.mDataInputStream.read();
            final int read3 = this.mDataInputStream.read();
            final int read4 = this.mDataInputStream.read();
            final int read5 = this.mDataInputStream.read();
            final int read6 = this.mDataInputStream.read();
            final int read7 = this.mDataInputStream.read();
            final int read8 = this.mDataInputStream.read();
            if ((read | read2 | read3 | read4 | read5 | read6 | read7 | read8) < 0) {
                throw new EOFException();
            }
            final ByteOrder mByteOrder = this.mByteOrder;
            if (mByteOrder == ByteOrderedDataInputStream.LITTLE_ENDIAN) {
                return ((long)read8 << 56) + ((long)read7 << 48) + ((long)read6 << 40) + ((long)read5 << 32) + ((long)read4 << 24) + ((long)read3 << 16) + ((long)read2 << 8) + read;
            }
            if (mByteOrder == ByteOrderedDataInputStream.BIG_ENDIAN) {
                return ((long)read << 56) + ((long)read2 << 48) + ((long)read3 << 40) + ((long)read4 << 32) + ((long)read5 << 24) + ((long)read6 << 16) + ((long)read7 << 8) + read8;
            }
            final StringBuilder sb = new StringBuilder();
            sb.append("Invalid byte order: ");
            sb.append(this.mByteOrder);
            throw new IOException(sb.toString());
        }
        
        @Override
        public short readShort() throws IOException {
            this.mPosition += 2;
            final int read = this.mDataInputStream.read();
            final int read2 = this.mDataInputStream.read();
            if ((read | read2) < 0) {
                throw new EOFException();
            }
            final ByteOrder mByteOrder = this.mByteOrder;
            if (mByteOrder == ByteOrderedDataInputStream.LITTLE_ENDIAN) {
                return (short)((read2 << 8) + read);
            }
            if (mByteOrder == ByteOrderedDataInputStream.BIG_ENDIAN) {
                return (short)((read << 8) + read2);
            }
            final StringBuilder sb = new StringBuilder();
            sb.append("Invalid byte order: ");
            sb.append(this.mByteOrder);
            throw new IOException(sb.toString());
        }
        
        @Override
        public String readUTF() throws IOException {
            this.mPosition += 2;
            return this.mDataInputStream.readUTF();
        }
        
        @Override
        public int readUnsignedByte() throws IOException {
            ++this.mPosition;
            return this.mDataInputStream.readUnsignedByte();
        }
        
        public long readUnsignedInt() throws IOException {
            return (long)this.readInt() & 0xFFFFFFFFL;
        }
        
        @Override
        public int readUnsignedShort() throws IOException {
            this.mPosition += 2;
            final int read = this.mDataInputStream.read();
            final int read2 = this.mDataInputStream.read();
            if ((read | read2) < 0) {
                throw new EOFException();
            }
            final ByteOrder mByteOrder = this.mByteOrder;
            if (mByteOrder == ByteOrderedDataInputStream.LITTLE_ENDIAN) {
                return (read2 << 8) + read;
            }
            if (mByteOrder == ByteOrderedDataInputStream.BIG_ENDIAN) {
                return (read << 8) + read2;
            }
            final StringBuilder sb = new StringBuilder();
            sb.append("Invalid byte order: ");
            sb.append(this.mByteOrder);
            throw new IOException(sb.toString());
        }
        
        @Override
        public void reset() {
            throw new UnsupportedOperationException("Reset is currently unsupported");
        }
        
        public void setByteOrder(final ByteOrder mByteOrder) {
            this.mByteOrder = mByteOrder;
        }
        
        @Override
        public int skipBytes(final int n) throws IOException {
            throw new UnsupportedOperationException("skipBytes is currently unsupported");
        }
        
        public void skipFully(final int i) throws IOException {
            int j;
            int read;
            for (j = 0; j < i; j += read) {
                final DataInputStream mDataInputStream = this.mDataInputStream;
                final int b = i - j;
                if ((read = (int)mDataInputStream.skip(b)) <= 0) {
                    if (this.mSkipBuffer == null) {
                        this.mSkipBuffer = new byte[8192];
                    }
                    read = this.mDataInputStream.read(this.mSkipBuffer, 0, Math.min(8192, b));
                    if (read == -1) {
                        final StringBuilder sb = new StringBuilder();
                        sb.append("Reached EOF while skipping ");
                        sb.append(i);
                        sb.append(" bytes.");
                        throw new EOFException(sb.toString());
                    }
                }
            }
            this.mPosition += j;
        }
    }
    
    private static class ByteOrderedDataOutputStream extends FilterOutputStream
    {
        private ByteOrder mByteOrder;
        final OutputStream mOutputStream;
        
        public ByteOrderedDataOutputStream(final OutputStream outputStream, final ByteOrder mByteOrder) {
            super(outputStream);
            this.mOutputStream = outputStream;
            this.mByteOrder = mByteOrder;
        }
        
        public void setByteOrder(final ByteOrder mByteOrder) {
            this.mByteOrder = mByteOrder;
        }
        
        @Override
        public void write(final byte[] b) throws IOException {
            this.mOutputStream.write(b);
        }
        
        @Override
        public void write(final byte[] b, final int off, final int len) throws IOException {
            this.mOutputStream.write(b, off, len);
        }
        
        public void writeByte(final int n) throws IOException {
            this.mOutputStream.write(n);
        }
        
        public void writeInt(final int n) throws IOException {
            if (this.mByteOrder == ByteOrder.LITTLE_ENDIAN) {
                this.mOutputStream.write(n >>> 0 & 0xFF);
                this.mOutputStream.write(n >>> 8 & 0xFF);
                this.mOutputStream.write(n >>> 16 & 0xFF);
                this.mOutputStream.write(n >>> 24 & 0xFF);
            }
            else if (this.mByteOrder == ByteOrder.BIG_ENDIAN) {
                this.mOutputStream.write(n >>> 24 & 0xFF);
                this.mOutputStream.write(n >>> 16 & 0xFF);
                this.mOutputStream.write(n >>> 8 & 0xFF);
                this.mOutputStream.write(n >>> 0 & 0xFF);
            }
        }
        
        public void writeShort(final short n) throws IOException {
            if (this.mByteOrder == ByteOrder.LITTLE_ENDIAN) {
                this.mOutputStream.write(n >>> 0 & 0xFF);
                this.mOutputStream.write(n >>> 8 & 0xFF);
            }
            else if (this.mByteOrder == ByteOrder.BIG_ENDIAN) {
                this.mOutputStream.write(n >>> 8 & 0xFF);
                this.mOutputStream.write(n >>> 0 & 0xFF);
            }
        }
        
        public void writeUnsignedInt(final long n) throws IOException {
            this.writeInt((int)n);
        }
        
        public void writeUnsignedShort(final int n) throws IOException {
            this.writeShort((short)n);
        }
    }
    
    private static class ExifAttribute
    {
        public static final long BYTES_OFFSET_UNKNOWN = -1L;
        public final byte[] bytes;
        public final long bytesOffset;
        public final int format;
        public final int numberOfComponents;
        
        ExifAttribute(final int format, final int numberOfComponents, final long bytesOffset, final byte[] bytes) {
            this.format = format;
            this.numberOfComponents = numberOfComponents;
            this.bytesOffset = bytesOffset;
            this.bytes = bytes;
        }
        
        ExifAttribute(final int n, final int n2, final byte[] array) {
            this(n, n2, -1L, array);
        }
        
        public static ExifAttribute createByte(final String s) {
            if (s.length() == 1 && s.charAt(0) >= '0' && s.charAt(0) <= '1') {
                return new ExifAttribute(1, 1, new byte[] { (byte)(s.charAt(0) - '0') });
            }
            final byte[] bytes = s.getBytes(ExifInterface.ASCII);
            return new ExifAttribute(1, bytes.length, bytes);
        }
        
        public static ExifAttribute createDouble(final double n, final ByteOrder byteOrder) {
            return createDouble(new double[] { n }, byteOrder);
        }
        
        public static ExifAttribute createDouble(final double[] array, final ByteOrder bo) {
            final ByteBuffer wrap = ByteBuffer.wrap(new byte[ExifInterface.IFD_FORMAT_BYTES_PER_FORMAT[12] * array.length]);
            wrap.order(bo);
            for (int length = array.length, i = 0; i < length; ++i) {
                wrap.putDouble(array[i]);
            }
            return new ExifAttribute(12, array.length, wrap.array());
        }
        
        public static ExifAttribute createSLong(final int n, final ByteOrder byteOrder) {
            return createSLong(new int[] { n }, byteOrder);
        }
        
        public static ExifAttribute createSLong(final int[] array, final ByteOrder bo) {
            final ByteBuffer wrap = ByteBuffer.wrap(new byte[ExifInterface.IFD_FORMAT_BYTES_PER_FORMAT[9] * array.length]);
            wrap.order(bo);
            for (int length = array.length, i = 0; i < length; ++i) {
                wrap.putInt(array[i]);
            }
            return new ExifAttribute(9, array.length, wrap.array());
        }
        
        public static ExifAttribute createSRational(final Rational rational, final ByteOrder byteOrder) {
            return createSRational(new Rational[] { rational }, byteOrder);
        }
        
        public static ExifAttribute createSRational(final Rational[] array, final ByteOrder bo) {
            final ByteBuffer wrap = ByteBuffer.wrap(new byte[ExifInterface.IFD_FORMAT_BYTES_PER_FORMAT[10] * array.length]);
            wrap.order(bo);
            for (final Rational rational : array) {
                wrap.putInt((int)rational.numerator);
                wrap.putInt((int)rational.denominator);
            }
            return new ExifAttribute(10, array.length, wrap.array());
        }
        
        public static ExifAttribute createString(final String str) {
            final StringBuilder sb = new StringBuilder();
            sb.append(str);
            sb.append('\0');
            final byte[] bytes = sb.toString().getBytes(ExifInterface.ASCII);
            return new ExifAttribute(2, bytes.length, bytes);
        }
        
        public static ExifAttribute createULong(final long n, final ByteOrder byteOrder) {
            return createULong(new long[] { n }, byteOrder);
        }
        
        public static ExifAttribute createULong(final long[] array, final ByteOrder bo) {
            final ByteBuffer wrap = ByteBuffer.wrap(new byte[ExifInterface.IFD_FORMAT_BYTES_PER_FORMAT[4] * array.length]);
            wrap.order(bo);
            for (int length = array.length, i = 0; i < length; ++i) {
                wrap.putInt((int)array[i]);
            }
            return new ExifAttribute(4, array.length, wrap.array());
        }
        
        public static ExifAttribute createURational(final Rational rational, final ByteOrder byteOrder) {
            return createURational(new Rational[] { rational }, byteOrder);
        }
        
        public static ExifAttribute createURational(final Rational[] array, final ByteOrder bo) {
            final ByteBuffer wrap = ByteBuffer.wrap(new byte[ExifInterface.IFD_FORMAT_BYTES_PER_FORMAT[5] * array.length]);
            wrap.order(bo);
            for (final Rational rational : array) {
                wrap.putInt((int)rational.numerator);
                wrap.putInt((int)rational.denominator);
            }
            return new ExifAttribute(5, array.length, wrap.array());
        }
        
        public static ExifAttribute createUShort(final int n, final ByteOrder byteOrder) {
            return createUShort(new int[] { n }, byteOrder);
        }
        
        public static ExifAttribute createUShort(final int[] array, final ByteOrder bo) {
            final ByteBuffer wrap = ByteBuffer.wrap(new byte[ExifInterface.IFD_FORMAT_BYTES_PER_FORMAT[3] * array.length]);
            wrap.order(bo);
            for (int length = array.length, i = 0; i < length; ++i) {
                wrap.putShort((short)array[i]);
            }
            return new ExifAttribute(3, array.length, wrap.array());
        }
        
        public double getDoubleValue(final ByteOrder byteOrder) {
            final Object value = this.getValue(byteOrder);
            if (value == null) {
                throw new NumberFormatException("NULL can't be converted to a double value");
            }
            if (value instanceof String) {
                return Double.parseDouble((String)value);
            }
            if (value instanceof long[]) {
                final long[] array = (long[])value;
                if (array.length == 1) {
                    return (double)array[0];
                }
                throw new NumberFormatException("There are more than one component");
            }
            else if (value instanceof int[]) {
                final int[] array2 = (int[])value;
                if (array2.length == 1) {
                    return array2[0];
                }
                throw new NumberFormatException("There are more than one component");
            }
            else if (value instanceof double[]) {
                final double[] array3 = (double[])value;
                if (array3.length == 1) {
                    return array3[0];
                }
                throw new NumberFormatException("There are more than one component");
            }
            else {
                if (!(value instanceof Rational[])) {
                    throw new NumberFormatException("Couldn't find a double value");
                }
                final Rational[] array4 = (Rational[])value;
                if (array4.length == 1) {
                    return array4[0].calculate();
                }
                throw new NumberFormatException("There are more than one component");
            }
        }
        
        public int getIntValue(final ByteOrder byteOrder) {
            final Object value = this.getValue(byteOrder);
            if (value == null) {
                throw new NumberFormatException("NULL can't be converted to a integer value");
            }
            if (value instanceof String) {
                return Integer.parseInt((String)value);
            }
            if (value instanceof long[]) {
                final long[] array = (long[])value;
                if (array.length == 1) {
                    return (int)array[0];
                }
                throw new NumberFormatException("There are more than one component");
            }
            else {
                if (!(value instanceof int[])) {
                    throw new NumberFormatException("Couldn't find a integer value");
                }
                final int[] array2 = (int[])value;
                if (array2.length == 1) {
                    return array2[0];
                }
                throw new NumberFormatException("There are more than one component");
            }
        }
        
        public String getStringValue(final ByteOrder byteOrder) {
            final Object value = this.getValue(byteOrder);
            if (value == null) {
                return null;
            }
            if (value instanceof String) {
                return (String)value;
            }
            final StringBuilder sb = new StringBuilder();
            final boolean b = value instanceof long[];
            final int n = 0;
            int i = 0;
            final int n2 = 0;
            final int n3 = 0;
            if (b) {
                final long[] array = (long[])value;
                int n4;
                for (int j = n3; j < array.length; j = n4) {
                    sb.append(array[j]);
                    n4 = j + 1;
                    if ((j = n4) != array.length) {
                        sb.append(",");
                    }
                }
                return sb.toString();
            }
            if (value instanceof int[]) {
                final int[] array2 = (int[])value;
                int n5;
                for (int k = n; k < array2.length; k = n5) {
                    sb.append(array2[k]);
                    n5 = k + 1;
                    if ((k = n5) != array2.length) {
                        sb.append(",");
                    }
                }
                return sb.toString();
            }
            if (value instanceof double[]) {
                int n6;
                for (double[] array3 = (double[])value; i < array3.length; i = n6) {
                    sb.append(array3[i]);
                    n6 = i + 1;
                    if ((i = n6) != array3.length) {
                        sb.append(",");
                    }
                }
                return sb.toString();
            }
            if (value instanceof Rational[]) {
                final Rational[] array4 = (Rational[])value;
                int n7;
                for (int l = n2; l < array4.length; l = n7) {
                    sb.append(array4[l].numerator);
                    sb.append('/');
                    sb.append(array4[l].denominator);
                    n7 = l + 1;
                    if ((l = n7) != array4.length) {
                        sb.append(",");
                    }
                }
                return sb.toString();
            }
            return null;
        }
        
        Object getValue(final ByteOrder p0) {
            // 
            // This method could not be decompiled.
            // 
            // Original Bytecode:
            // 
            //     3: new             Landroidx/exifinterface/media/ExifInterface$ByteOrderedDataInputStream;
            //     6: astore          14
            //     8: aload           14
            //    10: aload_0        
            //    11: getfield        androidx/exifinterface/media/ExifInterface$ExifAttribute.bytes:[B
            //    14: invokespecial   androidx/exifinterface/media/ExifInterface$ByteOrderedDataInputStream.<init>:([B)V
            //    17: aload           14
            //    19: astore          13
            //    21: aload           14
            //    23: aload_1        
            //    24: invokevirtual   androidx/exifinterface/media/ExifInterface$ByteOrderedDataInputStream.setByteOrder:(Ljava/nio/ByteOrder;)V
            //    27: aload           14
            //    29: astore          13
            //    31: aload_0        
            //    32: getfield        androidx/exifinterface/media/ExifInterface$ExifAttribute.format:I
            //    35: istore          12
            //    37: iconst_0       
            //    38: istore          9
            //    40: iconst_0       
            //    41: istore          10
            //    43: iconst_0       
            //    44: istore          8
            //    46: iconst_0       
            //    47: istore          6
            //    49: iconst_0       
            //    50: istore          11
            //    52: iconst_0       
            //    53: istore_3       
            //    54: iconst_0       
            //    55: istore_2       
            //    56: iconst_0       
            //    57: istore          4
            //    59: iconst_0       
            //    60: istore          7
            //    62: iconst_1       
            //    63: istore          5
            //    65: iload           12
            //    67: tableswitch {
            //                2: 878
            //                3: 684
            //                4: 621
            //                5: 556
            //                6: 477
            //                7: 878
            //                8: 684
            //                9: 411
            //               10: 345
            //               11: 264
            //               12: 197
            //               13: 131
            //          default: 128
            //        }
            //   128: goto            993
            //   131: aload           14
            //   133: astore          13
            //   135: aload_0        
            //   136: getfield        androidx/exifinterface/media/ExifInterface$ExifAttribute.numberOfComponents:I
            //   139: newarray        D
            //   141: astore_1       
            //   142: iload           7
            //   144: istore_2       
            //   145: aload           14
            //   147: astore          13
            //   149: iload_2        
            //   150: aload_0        
            //   151: getfield        androidx/exifinterface/media/ExifInterface$ExifAttribute.numberOfComponents:I
            //   154: if_icmpge       175
            //   157: aload           14
            //   159: astore          13
            //   161: aload_1        
            //   162: iload_2        
            //   163: aload           14
            //   165: invokevirtual   androidx/exifinterface/media/ExifInterface$ByteOrderedDataInputStream.readDouble:()D
            //   168: dastore        
            //   169: iinc            2, 1
            //   172: goto            145
            //   175: aload           14
            //   177: invokevirtual   androidx/exifinterface/media/ExifInterface$ByteOrderedDataInputStream.close:()V
            //   180: goto            195
            //   183: astore          13
            //   185: ldc             "ExifInterface"
            //   187: ldc             "IOException occurred while closing InputStream"
            //   189: aload           13
            //   191: invokestatic    android/util/Log.e:(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
            //   194: pop            
            //   195: aload_1        
            //   196: areturn        
            //   197: aload           14
            //   199: astore          13
            //   201: aload_0        
            //   202: getfield        androidx/exifinterface/media/ExifInterface$ExifAttribute.numberOfComponents:I
            //   205: newarray        D
            //   207: astore_1       
            //   208: iload           9
            //   210: istore_2       
            //   211: aload           14
            //   213: astore          13
            //   215: iload_2        
            //   216: aload_0        
            //   217: getfield        androidx/exifinterface/media/ExifInterface$ExifAttribute.numberOfComponents:I
            //   220: if_icmpge       242
            //   223: aload           14
            //   225: astore          13
            //   227: aload_1        
            //   228: iload_2        
            //   229: aload           14
            //   231: invokevirtual   androidx/exifinterface/media/ExifInterface$ByteOrderedDataInputStream.readFloat:()F
            //   234: f2d            
            //   235: dastore        
            //   236: iinc            2, 1
            //   239: goto            211
            //   242: aload           14
            //   244: invokevirtual   androidx/exifinterface/media/ExifInterface$ByteOrderedDataInputStream.close:()V
            //   247: goto            262
            //   250: astore          13
            //   252: ldc             "ExifInterface"
            //   254: ldc             "IOException occurred while closing InputStream"
            //   256: aload           13
            //   258: invokestatic    android/util/Log.e:(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
            //   261: pop            
            //   262: aload_1        
            //   263: areturn        
            //   264: aload           14
            //   266: astore          13
            //   268: aload_0        
            //   269: getfield        androidx/exifinterface/media/ExifInterface$ExifAttribute.numberOfComponents:I
            //   272: anewarray       Landroidx/exifinterface/media/ExifInterface$Rational;
            //   275: astore_1       
            //   276: iload           10
            //   278: istore_2       
            //   279: aload           14
            //   281: astore          13
            //   283: iload_2        
            //   284: aload_0        
            //   285: getfield        androidx/exifinterface/media/ExifInterface$ExifAttribute.numberOfComponents:I
            //   288: if_icmpge       323
            //   291: aload           14
            //   293: astore          13
            //   295: aload_1        
            //   296: iload_2        
            //   297: new             Landroidx/exifinterface/media/ExifInterface$Rational;
            //   300: dup            
            //   301: aload           14
            //   303: invokevirtual   androidx/exifinterface/media/ExifInterface$ByteOrderedDataInputStream.readInt:()I
            //   306: i2l            
            //   307: aload           14
            //   309: invokevirtual   androidx/exifinterface/media/ExifInterface$ByteOrderedDataInputStream.readInt:()I
            //   312: i2l            
            //   313: invokespecial   androidx/exifinterface/media/ExifInterface$Rational.<init>:(JJ)V
            //   316: aastore        
            //   317: iinc            2, 1
            //   320: goto            279
            //   323: aload           14
            //   325: invokevirtual   androidx/exifinterface/media/ExifInterface$ByteOrderedDataInputStream.close:()V
            //   328: goto            343
            //   331: astore          13
            //   333: ldc             "ExifInterface"
            //   335: ldc             "IOException occurred while closing InputStream"
            //   337: aload           13
            //   339: invokestatic    android/util/Log.e:(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
            //   342: pop            
            //   343: aload_1        
            //   344: areturn        
            //   345: aload           14
            //   347: astore          13
            //   349: aload_0        
            //   350: getfield        androidx/exifinterface/media/ExifInterface$ExifAttribute.numberOfComponents:I
            //   353: newarray        I
            //   355: astore_1       
            //   356: iload           8
            //   358: istore_2       
            //   359: aload           14
            //   361: astore          13
            //   363: iload_2        
            //   364: aload_0        
            //   365: getfield        androidx/exifinterface/media/ExifInterface$ExifAttribute.numberOfComponents:I
            //   368: if_icmpge       389
            //   371: aload           14
            //   373: astore          13
            //   375: aload_1        
            //   376: iload_2        
            //   377: aload           14
            //   379: invokevirtual   androidx/exifinterface/media/ExifInterface$ByteOrderedDataInputStream.readInt:()I
            //   382: iastore        
            //   383: iinc            2, 1
            //   386: goto            359
            //   389: aload           14
            //   391: invokevirtual   androidx/exifinterface/media/ExifInterface$ByteOrderedDataInputStream.close:()V
            //   394: goto            409
            //   397: astore          13
            //   399: ldc             "ExifInterface"
            //   401: ldc             "IOException occurred while closing InputStream"
            //   403: aload           13
            //   405: invokestatic    android/util/Log.e:(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
            //   408: pop            
            //   409: aload_1        
            //   410: areturn        
            //   411: aload           14
            //   413: astore          13
            //   415: aload_0        
            //   416: getfield        androidx/exifinterface/media/ExifInterface$ExifAttribute.numberOfComponents:I
            //   419: newarray        I
            //   421: astore_1       
            //   422: iload           6
            //   424: istore_2       
            //   425: aload           14
            //   427: astore          13
            //   429: iload_2        
            //   430: aload_0        
            //   431: getfield        androidx/exifinterface/media/ExifInterface$ExifAttribute.numberOfComponents:I
            //   434: if_icmpge       455
            //   437: aload           14
            //   439: astore          13
            //   441: aload_1        
            //   442: iload_2        
            //   443: aload           14
            //   445: invokevirtual   androidx/exifinterface/media/ExifInterface$ByteOrderedDataInputStream.readShort:()S
            //   448: iastore        
            //   449: iinc            2, 1
            //   452: goto            425
            //   455: aload           14
            //   457: invokevirtual   androidx/exifinterface/media/ExifInterface$ByteOrderedDataInputStream.close:()V
            //   460: goto            475
            //   463: astore          13
            //   465: ldc             "ExifInterface"
            //   467: ldc             "IOException occurred while closing InputStream"
            //   469: aload           13
            //   471: invokestatic    android/util/Log.e:(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
            //   474: pop            
            //   475: aload_1        
            //   476: areturn        
            //   477: aload           14
            //   479: astore          13
            //   481: aload_0        
            //   482: getfield        androidx/exifinterface/media/ExifInterface$ExifAttribute.numberOfComponents:I
            //   485: anewarray       Landroidx/exifinterface/media/ExifInterface$Rational;
            //   488: astore_1       
            //   489: iload           11
            //   491: istore_2       
            //   492: aload           14
            //   494: astore          13
            //   496: iload_2        
            //   497: aload_0        
            //   498: getfield        androidx/exifinterface/media/ExifInterface$ExifAttribute.numberOfComponents:I
            //   501: if_icmpge       534
            //   504: aload           14
            //   506: astore          13
            //   508: aload_1        
            //   509: iload_2        
            //   510: new             Landroidx/exifinterface/media/ExifInterface$Rational;
            //   513: dup            
            //   514: aload           14
            //   516: invokevirtual   androidx/exifinterface/media/ExifInterface$ByteOrderedDataInputStream.readUnsignedInt:()J
            //   519: aload           14
            //   521: invokevirtual   androidx/exifinterface/media/ExifInterface$ByteOrderedDataInputStream.readUnsignedInt:()J
            //   524: invokespecial   androidx/exifinterface/media/ExifInterface$Rational.<init>:(JJ)V
            //   527: aastore        
            //   528: iinc            2, 1
            //   531: goto            492
            //   534: aload           14
            //   536: invokevirtual   androidx/exifinterface/media/ExifInterface$ByteOrderedDataInputStream.close:()V
            //   539: goto            554
            //   542: astore          13
            //   544: ldc             "ExifInterface"
            //   546: ldc             "IOException occurred while closing InputStream"
            //   548: aload           13
            //   550: invokestatic    android/util/Log.e:(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
            //   553: pop            
            //   554: aload_1        
            //   555: areturn        
            //   556: aload           14
            //   558: astore          13
            //   560: aload_0        
            //   561: getfield        androidx/exifinterface/media/ExifInterface$ExifAttribute.numberOfComponents:I
            //   564: newarray        J
            //   566: astore_1       
            //   567: iload_3        
            //   568: istore_2       
            //   569: aload           14
            //   571: astore          13
            //   573: iload_2        
            //   574: aload_0        
            //   575: getfield        androidx/exifinterface/media/ExifInterface$ExifAttribute.numberOfComponents:I
            //   578: if_icmpge       599
            //   581: aload           14
            //   583: astore          13
            //   585: aload_1        
            //   586: iload_2        
            //   587: aload           14
            //   589: invokevirtual   androidx/exifinterface/media/ExifInterface$ByteOrderedDataInputStream.readUnsignedInt:()J
            //   592: lastore        
            //   593: iinc            2, 1
            //   596: goto            569
            //   599: aload           14
            //   601: invokevirtual   androidx/exifinterface/media/ExifInterface$ByteOrderedDataInputStream.close:()V
            //   604: goto            619
            //   607: astore          13
            //   609: ldc             "ExifInterface"
            //   611: ldc             "IOException occurred while closing InputStream"
            //   613: aload           13
            //   615: invokestatic    android/util/Log.e:(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
            //   618: pop            
            //   619: aload_1        
            //   620: areturn        
            //   621: aload           14
            //   623: astore          13
            //   625: aload_0        
            //   626: getfield        androidx/exifinterface/media/ExifInterface$ExifAttribute.numberOfComponents:I
            //   629: newarray        I
            //   631: astore_1       
            //   632: aload           14
            //   634: astore          13
            //   636: iload_2        
            //   637: aload_0        
            //   638: getfield        androidx/exifinterface/media/ExifInterface$ExifAttribute.numberOfComponents:I
            //   641: if_icmpge       662
            //   644: aload           14
            //   646: astore          13
            //   648: aload_1        
            //   649: iload_2        
            //   650: aload           14
            //   652: invokevirtual   androidx/exifinterface/media/ExifInterface$ByteOrderedDataInputStream.readUnsignedShort:()I
            //   655: iastore        
            //   656: iinc            2, 1
            //   659: goto            632
            //   662: aload           14
            //   664: invokevirtual   androidx/exifinterface/media/ExifInterface$ByteOrderedDataInputStream.close:()V
            //   667: goto            682
            //   670: astore          13
            //   672: ldc             "ExifInterface"
            //   674: ldc             "IOException occurred while closing InputStream"
            //   676: aload           13
            //   678: invokestatic    android/util/Log.e:(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
            //   681: pop            
            //   682: aload_1        
            //   683: areturn        
            //   684: iload           4
            //   686: istore_2       
            //   687: aload           14
            //   689: astore          13
            //   691: aload_0        
            //   692: getfield        androidx/exifinterface/media/ExifInterface$ExifAttribute.numberOfComponents:I
            //   695: getstatic       androidx/exifinterface/media/ExifInterface.EXIF_ASCII_PREFIX:[B
            //   698: arraylength    
            //   699: if_icmplt       764
            //   702: iconst_0       
            //   703: istore_2       
            //   704: iload           5
            //   706: istore_3       
            //   707: aload           14
            //   709: astore          13
            //   711: iload_2        
            //   712: getstatic       androidx/exifinterface/media/ExifInterface.EXIF_ASCII_PREFIX:[B
            //   715: arraylength    
            //   716: if_icmpge       748
            //   719: aload           14
            //   721: astore          13
            //   723: aload_0        
            //   724: getfield        androidx/exifinterface/media/ExifInterface$ExifAttribute.bytes:[B
            //   727: iload_2        
            //   728: baload         
            //   729: getstatic       androidx/exifinterface/media/ExifInterface.EXIF_ASCII_PREFIX:[B
            //   732: iload_2        
            //   733: baload         
            //   734: if_icmpeq       742
            //   737: iconst_0       
            //   738: istore_3       
            //   739: goto            748
            //   742: iinc            2, 1
            //   745: goto            704
            //   748: iload           4
            //   750: istore_2       
            //   751: iload_3        
            //   752: ifeq            764
            //   755: aload           14
            //   757: astore          13
            //   759: getstatic       androidx/exifinterface/media/ExifInterface.EXIF_ASCII_PREFIX:[B
            //   762: arraylength    
            //   763: istore_2       
            //   764: aload           14
            //   766: astore          13
            //   768: new             Ljava/lang/StringBuilder;
            //   771: astore_1       
            //   772: aload           14
            //   774: astore          13
            //   776: aload_1        
            //   777: invokespecial   java/lang/StringBuilder.<init>:()V
            //   780: aload           14
            //   782: astore          13
            //   784: iload_2        
            //   785: aload_0        
            //   786: getfield        androidx/exifinterface/media/ExifInterface$ExifAttribute.numberOfComponents:I
            //   789: if_icmpge       847
            //   792: aload           14
            //   794: astore          13
            //   796: aload_0        
            //   797: getfield        androidx/exifinterface/media/ExifInterface$ExifAttribute.bytes:[B
            //   800: iload_2        
            //   801: baload         
            //   802: istore_3       
            //   803: iload_3        
            //   804: ifne            810
            //   807: goto            847
            //   810: iload_3        
            //   811: bipush          32
            //   813: if_icmplt       830
            //   816: aload           14
            //   818: astore          13
            //   820: aload_1        
            //   821: iload_3        
            //   822: i2c            
            //   823: invokevirtual   java/lang/StringBuilder.append:(C)Ljava/lang/StringBuilder;
            //   826: pop            
            //   827: goto            841
            //   830: aload           14
            //   832: astore          13
            //   834: aload_1        
            //   835: bipush          63
            //   837: invokevirtual   java/lang/StringBuilder.append:(C)Ljava/lang/StringBuilder;
            //   840: pop            
            //   841: iinc            2, 1
            //   844: goto            780
            //   847: aload           14
            //   849: astore          13
            //   851: aload_1        
            //   852: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
            //   855: astore_1       
            //   856: aload           14
            //   858: invokevirtual   androidx/exifinterface/media/ExifInterface$ByteOrderedDataInputStream.close:()V
            //   861: goto            876
            //   864: astore          13
            //   866: ldc             "ExifInterface"
            //   868: ldc             "IOException occurred while closing InputStream"
            //   870: aload           13
            //   872: invokestatic    android/util/Log.e:(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
            //   875: pop            
            //   876: aload_1        
            //   877: areturn        
            //   878: aload           14
            //   880: astore          13
            //   882: aload_0        
            //   883: getfield        androidx/exifinterface/media/ExifInterface$ExifAttribute.bytes:[B
            //   886: astore_1       
            //   887: aload           14
            //   889: astore          13
            //   891: aload_1        
            //   892: arraylength    
            //   893: iconst_1       
            //   894: if_icmpne       955
            //   897: aload_1        
            //   898: iconst_0       
            //   899: baload         
            //   900: istore_2       
            //   901: iload_2        
            //   902: iflt            955
            //   905: iload_2        
            //   906: iconst_1       
            //   907: if_icmpgt       955
            //   910: aload           14
            //   912: astore          13
            //   914: new             Ljava/lang/String;
            //   917: dup            
            //   918: iconst_1       
            //   919: newarray        C
            //   921: dup            
            //   922: iconst_0       
            //   923: iload_2        
            //   924: bipush          48
            //   926: iadd           
            //   927: i2c            
            //   928: castore        
            //   929: invokespecial   java/lang/String.<init>:([C)V
            //   932: astore_1       
            //   933: aload           14
            //   935: invokevirtual   androidx/exifinterface/media/ExifInterface$ByteOrderedDataInputStream.close:()V
            //   938: goto            953
            //   941: astore          13
            //   943: ldc             "ExifInterface"
            //   945: ldc             "IOException occurred while closing InputStream"
            //   947: aload           13
            //   949: invokestatic    android/util/Log.e:(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
            //   952: pop            
            //   953: aload_1        
            //   954: areturn        
            //   955: aload           14
            //   957: astore          13
            //   959: new             Ljava/lang/String;
            //   962: dup            
            //   963: aload_1        
            //   964: getstatic       androidx/exifinterface/media/ExifInterface.ASCII:Ljava/nio/charset/Charset;
            //   967: invokespecial   java/lang/String.<init>:([BLjava/nio/charset/Charset;)V
            //   970: astore_1       
            //   971: aload           14
            //   973: invokevirtual   androidx/exifinterface/media/ExifInterface$ByteOrderedDataInputStream.close:()V
            //   976: goto            991
            //   979: astore          13
            //   981: ldc             "ExifInterface"
            //   983: ldc             "IOException occurred while closing InputStream"
            //   985: aload           13
            //   987: invokestatic    android/util/Log.e:(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
            //   990: pop            
            //   991: aload_1        
            //   992: areturn        
            //   993: aload           14
            //   995: invokevirtual   androidx/exifinterface/media/ExifInterface$ByteOrderedDataInputStream.close:()V
            //   998: goto            1011
            //  1001: astore_1       
            //  1002: ldc             "ExifInterface"
            //  1004: ldc             "IOException occurred while closing InputStream"
            //  1006: aload_1        
            //  1007: invokestatic    android/util/Log.e:(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
            //  1010: pop            
            //  1011: aconst_null    
            //  1012: areturn        
            //  1013: astore          13
            //  1015: aload           14
            //  1017: astore_1       
            //  1018: aload           13
            //  1020: astore          14
            //  1022: goto            1033
            //  1025: astore_1       
            //  1026: goto            1070
            //  1029: astore          14
            //  1031: aconst_null    
            //  1032: astore_1       
            //  1033: aload_1        
            //  1034: astore          13
            //  1036: ldc             "ExifInterface"
            //  1038: ldc             "IOException occurred during reading a value"
            //  1040: aload           14
            //  1042: invokestatic    android/util/Log.w:(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
            //  1045: pop            
            //  1046: aload_1        
            //  1047: ifnull          1067
            //  1050: aload_1        
            //  1051: invokevirtual   androidx/exifinterface/media/ExifInterface$ByteOrderedDataInputStream.close:()V
            //  1054: goto            1067
            //  1057: astore_1       
            //  1058: ldc             "ExifInterface"
            //  1060: ldc             "IOException occurred while closing InputStream"
            //  1062: aload_1        
            //  1063: invokestatic    android/util/Log.e:(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
            //  1066: pop            
            //  1067: aconst_null    
            //  1068: areturn        
            //  1069: astore_1       
            //  1070: aload           13
            //  1072: ifnull          1095
            //  1075: aload           13
            //  1077: invokevirtual   androidx/exifinterface/media/ExifInterface$ByteOrderedDataInputStream.close:()V
            //  1080: goto            1095
            //  1083: astore          13
            //  1085: ldc             "ExifInterface"
            //  1087: ldc             "IOException occurred while closing InputStream"
            //  1089: aload           13
            //  1091: invokestatic    android/util/Log.e:(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
            //  1094: pop            
            //  1095: aload_1        
            //  1096: athrow         
            //    Exceptions:
            //  Try           Handler
            //  Start  End    Start  End    Type                 
            //  -----  -----  -----  -----  ---------------------
            //  3      17     1029   1033   Ljava/io/IOException;
            //  3      17     1025   1029   Any
            //  21     27     1013   1025   Ljava/io/IOException;
            //  21     27     1069   1070   Any
            //  31     37     1013   1025   Ljava/io/IOException;
            //  31     37     1069   1070   Any
            //  135    142    1013   1025   Ljava/io/IOException;
            //  135    142    1069   1070   Any
            //  149    157    1013   1025   Ljava/io/IOException;
            //  149    157    1069   1070   Any
            //  161    169    1013   1025   Ljava/io/IOException;
            //  161    169    1069   1070   Any
            //  175    180    183    195    Ljava/io/IOException;
            //  201    208    1013   1025   Ljava/io/IOException;
            //  201    208    1069   1070   Any
            //  215    223    1013   1025   Ljava/io/IOException;
            //  215    223    1069   1070   Any
            //  227    236    1013   1025   Ljava/io/IOException;
            //  227    236    1069   1070   Any
            //  242    247    250    262    Ljava/io/IOException;
            //  268    276    1013   1025   Ljava/io/IOException;
            //  268    276    1069   1070   Any
            //  283    291    1013   1025   Ljava/io/IOException;
            //  283    291    1069   1070   Any
            //  295    317    1013   1025   Ljava/io/IOException;
            //  295    317    1069   1070   Any
            //  323    328    331    343    Ljava/io/IOException;
            //  349    356    1013   1025   Ljava/io/IOException;
            //  349    356    1069   1070   Any
            //  363    371    1013   1025   Ljava/io/IOException;
            //  363    371    1069   1070   Any
            //  375    383    1013   1025   Ljava/io/IOException;
            //  375    383    1069   1070   Any
            //  389    394    397    409    Ljava/io/IOException;
            //  415    422    1013   1025   Ljava/io/IOException;
            //  415    422    1069   1070   Any
            //  429    437    1013   1025   Ljava/io/IOException;
            //  429    437    1069   1070   Any
            //  441    449    1013   1025   Ljava/io/IOException;
            //  441    449    1069   1070   Any
            //  455    460    463    475    Ljava/io/IOException;
            //  481    489    1013   1025   Ljava/io/IOException;
            //  481    489    1069   1070   Any
            //  496    504    1013   1025   Ljava/io/IOException;
            //  496    504    1069   1070   Any
            //  508    528    1013   1025   Ljava/io/IOException;
            //  508    528    1069   1070   Any
            //  534    539    542    554    Ljava/io/IOException;
            //  560    567    1013   1025   Ljava/io/IOException;
            //  560    567    1069   1070   Any
            //  573    581    1013   1025   Ljava/io/IOException;
            //  573    581    1069   1070   Any
            //  585    593    1013   1025   Ljava/io/IOException;
            //  585    593    1069   1070   Any
            //  599    604    607    619    Ljava/io/IOException;
            //  625    632    1013   1025   Ljava/io/IOException;
            //  625    632    1069   1070   Any
            //  636    644    1013   1025   Ljava/io/IOException;
            //  636    644    1069   1070   Any
            //  648    656    1013   1025   Ljava/io/IOException;
            //  648    656    1069   1070   Any
            //  662    667    670    682    Ljava/io/IOException;
            //  691    702    1013   1025   Ljava/io/IOException;
            //  691    702    1069   1070   Any
            //  711    719    1013   1025   Ljava/io/IOException;
            //  711    719    1069   1070   Any
            //  723    737    1013   1025   Ljava/io/IOException;
            //  723    737    1069   1070   Any
            //  759    764    1013   1025   Ljava/io/IOException;
            //  759    764    1069   1070   Any
            //  768    772    1013   1025   Ljava/io/IOException;
            //  768    772    1069   1070   Any
            //  776    780    1013   1025   Ljava/io/IOException;
            //  776    780    1069   1070   Any
            //  784    792    1013   1025   Ljava/io/IOException;
            //  784    792    1069   1070   Any
            //  796    803    1013   1025   Ljava/io/IOException;
            //  796    803    1069   1070   Any
            //  820    827    1013   1025   Ljava/io/IOException;
            //  820    827    1069   1070   Any
            //  834    841    1013   1025   Ljava/io/IOException;
            //  834    841    1069   1070   Any
            //  851    856    1013   1025   Ljava/io/IOException;
            //  851    856    1069   1070   Any
            //  856    861    864    876    Ljava/io/IOException;
            //  882    887    1013   1025   Ljava/io/IOException;
            //  882    887    1069   1070   Any
            //  891    897    1013   1025   Ljava/io/IOException;
            //  891    897    1069   1070   Any
            //  914    933    1013   1025   Ljava/io/IOException;
            //  914    933    1069   1070   Any
            //  933    938    941    953    Ljava/io/IOException;
            //  959    971    1013   1025   Ljava/io/IOException;
            //  959    971    1069   1070   Any
            //  971    976    979    991    Ljava/io/IOException;
            //  993    998    1001   1011   Ljava/io/IOException;
            //  1036   1046   1069   1070   Any
            //  1050   1054   1057   1067   Ljava/io/IOException;
            //  1075   1080   1083   1095   Ljava/io/IOException;
            // 
            // The error that occurred was:
            // 
            // java.lang.IllegalStateException: Expression is linked from several locations: Label_0128:
            //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
            //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2604)
            //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
            //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
            //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:206)
            //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:662)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
            //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
            //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
            //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
            //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
            //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
            // 
            throw new IllegalStateException("An error occurred while decompiling this method.");
        }
        
        public int size() {
            return ExifInterface.IFD_FORMAT_BYTES_PER_FORMAT[this.format] * this.numberOfComponents;
        }
        
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder();
            sb.append("(");
            sb.append(ExifInterface.IFD_FORMAT_NAMES[this.format]);
            sb.append(", data length:");
            sb.append(this.bytes.length);
            sb.append(")");
            return sb.toString();
        }
    }
    
    @Retention(RetentionPolicy.SOURCE)
    public @interface ExifStreamType {
    }
    
    static class ExifTag
    {
        public final String name;
        public final int number;
        public final int primaryFormat;
        public final int secondaryFormat;
        
        ExifTag(final String name, final int number, final int primaryFormat) {
            this.name = name;
            this.number = number;
            this.primaryFormat = primaryFormat;
            this.secondaryFormat = -1;
        }
        
        ExifTag(final String name, final int number, final int primaryFormat, final int secondaryFormat) {
            this.name = name;
            this.number = number;
            this.primaryFormat = primaryFormat;
            this.secondaryFormat = secondaryFormat;
        }
        
        boolean isFormatCompatible(final int n) {
            final int primaryFormat = this.primaryFormat;
            if (primaryFormat != 7) {
                if (n != 7) {
                    if (primaryFormat != n) {
                        final int secondaryFormat = this.secondaryFormat;
                        if (secondaryFormat != n) {
                            return ((primaryFormat == 4 || secondaryFormat == 4) && n == 3) || ((primaryFormat == 9 || secondaryFormat == 9) && n == 8) || ((primaryFormat == 12 || secondaryFormat == 12) && n == 11);
                        }
                    }
                }
            }
            return true;
        }
    }
    
    @Retention(RetentionPolicy.SOURCE)
    public @interface IfdType {
    }
    
    private static class Rational
    {
        public final long denominator;
        public final long numerator;
        
        Rational(final double n) {
            this((long)(n * 10000.0), 10000L);
        }
        
        Rational(final long numerator, final long denominator) {
            if (denominator == 0L) {
                this.numerator = 0L;
                this.denominator = 1L;
                return;
            }
            this.numerator = numerator;
            this.denominator = denominator;
        }
        
        public double calculate() {
            return this.numerator / (double)this.denominator;
        }
        
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder();
            sb.append(this.numerator);
            sb.append("/");
            sb.append(this.denominator);
            return sb.toString();
        }
    }
    
    private static class SeekableByteOrderedDataInputStream extends ByteOrderedDataInputStream
    {
        SeekableByteOrderedDataInputStream(final InputStream inputStream) throws IOException {
            super(inputStream);
            if (inputStream.markSupported()) {
                this.mDataInputStream.mark(Integer.MAX_VALUE);
                return;
            }
            throw new IllegalArgumentException("Cannot create SeekableByteOrderedDataInputStream with stream that does not support mark/reset");
        }
        
        SeekableByteOrderedDataInputStream(final byte[] array) throws IOException {
            super(array);
            this.mDataInputStream.mark(Integer.MAX_VALUE);
        }
        
        public void seek(long n) throws IOException {
            if (this.mPosition > n) {
                this.mPosition = 0;
                this.mDataInputStream.reset();
            }
            else {
                n -= this.mPosition;
            }
            ((ByteOrderedDataInputStream)this).skipFully((int)n);
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.drawerlayout;

public final class R
{
    public static final class attr
    {
        public static final int drawerLayoutStyle = 2130969005;
        public static final int elevation = 2130969015;
    }
    
    public static final class dimen
    {
        public static final int def_drawer_elevation = 2131165279;
    }
    
    public static final class styleable
    {
        public static final int[] DrawerLayout;
        public static final int DrawerLayout_elevation = 0;
        
        static {
            DrawerLayout = new int[] { 2130969015 };
        }
    }
}

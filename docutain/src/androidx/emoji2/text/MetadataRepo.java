// 
// Decompiled by Procyon v0.6.0
// 

package androidx.emoji2.text;

import android.util.SparseArray;
import androidx.core.util.Preconditions;
import java.nio.ByteBuffer;
import java.io.InputStream;
import java.io.IOException;
import androidx.core.os.TraceCompat;
import android.content.res.AssetManager;
import android.graphics.Typeface;
import androidx.emoji2.text.flatbuffer.MetadataList;

public final class MetadataRepo
{
    private static final int DEFAULT_ROOT_SIZE = 1024;
    private static final String S_TRACE_CREATE_REPO = "EmojiCompat.MetadataRepo.create";
    private final char[] mEmojiCharArray;
    private final MetadataList mMetadataList;
    private final Node mRootNode;
    private final Typeface mTypeface;
    
    private MetadataRepo(final Typeface mTypeface, final MetadataList mMetadataList) {
        this.mTypeface = mTypeface;
        this.mMetadataList = mMetadataList;
        this.mRootNode = new Node(1024);
        this.mEmojiCharArray = new char[mMetadataList.listLength() * 2];
        this.constructIndex(mMetadataList);
    }
    
    private void constructIndex(final MetadataList list) {
        for (int listLength = list.listLength(), i = 0; i < listLength; ++i) {
            final TypefaceEmojiRasterizer typefaceEmojiRasterizer = new TypefaceEmojiRasterizer(this, i);
            Character.toChars(typefaceEmojiRasterizer.getId(), this.mEmojiCharArray, i * 2);
            this.put(typefaceEmojiRasterizer);
        }
    }
    
    public static MetadataRepo create(final AssetManager assetManager, final String s) throws IOException {
        try {
            TraceCompat.beginSection("EmojiCompat.MetadataRepo.create");
            return new MetadataRepo(Typeface.createFromAsset(assetManager, s), MetadataListReader.read(assetManager, s));
        }
        finally {
            TraceCompat.endSection();
        }
    }
    
    public static MetadataRepo create(final Typeface typeface) {
        try {
            TraceCompat.beginSection("EmojiCompat.MetadataRepo.create");
            return new MetadataRepo(typeface, new MetadataList());
        }
        finally {
            TraceCompat.endSection();
        }
    }
    
    public static MetadataRepo create(final Typeface typeface, final InputStream inputStream) throws IOException {
        try {
            TraceCompat.beginSection("EmojiCompat.MetadataRepo.create");
            return new MetadataRepo(typeface, MetadataListReader.read(inputStream));
        }
        finally {
            TraceCompat.endSection();
        }
    }
    
    public static MetadataRepo create(final Typeface typeface, final ByteBuffer byteBuffer) throws IOException {
        try {
            TraceCompat.beginSection("EmojiCompat.MetadataRepo.create");
            return new MetadataRepo(typeface, MetadataListReader.read(byteBuffer));
        }
        finally {
            TraceCompat.endSection();
        }
    }
    
    public char[] getEmojiCharArray() {
        return this.mEmojiCharArray;
    }
    
    public MetadataList getMetadataList() {
        return this.mMetadataList;
    }
    
    int getMetadataVersion() {
        return this.mMetadataList.version();
    }
    
    Node getRootNode() {
        return this.mRootNode;
    }
    
    Typeface getTypeface() {
        return this.mTypeface;
    }
    
    void put(final TypefaceEmojiRasterizer typefaceEmojiRasterizer) {
        Preconditions.checkNotNull(typefaceEmojiRasterizer, "emoji metadata cannot be null");
        Preconditions.checkArgument(typefaceEmojiRasterizer.getCodepointsLength() > 0, (Object)"invalid metadata codepoint length");
        this.mRootNode.put(typefaceEmojiRasterizer, 0, typefaceEmojiRasterizer.getCodepointsLength() - 1);
    }
    
    static class Node
    {
        private final SparseArray<Node> mChildren;
        private TypefaceEmojiRasterizer mData;
        
        private Node() {
            this(1);
        }
        
        Node(final int n) {
            this.mChildren = (SparseArray<Node>)new SparseArray(n);
        }
        
        Node get(final int n) {
            final SparseArray<Node> mChildren = this.mChildren;
            Node node;
            if (mChildren == null) {
                node = null;
            }
            else {
                node = (Node)mChildren.get(n);
            }
            return node;
        }
        
        final TypefaceEmojiRasterizer getData() {
            return this.mData;
        }
        
        void put(final TypefaceEmojiRasterizer mData, final int n, final int n2) {
            Node value;
            if ((value = this.get(mData.getCodepointAt(n))) == null) {
                value = new Node();
                this.mChildren.put(mData.getCodepointAt(n), (Object)value);
            }
            if (n2 > n) {
                value.put(mData, n + 1, n2);
            }
            else {
                value.mData = mData;
            }
        }
    }
}

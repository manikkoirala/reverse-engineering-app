// 
// Decompiled by Procyon v0.6.0
// 

package androidx.emoji2.text;

import java.nio.ByteBuffer;
import androidx.core.graphics.TypefaceCompatUtil;
import androidx.core.os.TraceCompat;
import androidx.core.util.Preconditions;
import java.util.concurrent.ThreadPoolExecutor;
import android.database.ContentObserver;
import android.net.Uri;
import android.content.pm.PackageManager$NameNotFoundException;
import android.os.CancellationSignal;
import android.graphics.Typeface;
import androidx.core.provider.FontsContractCompat;
import android.os.SystemClock;
import java.util.concurrent.Executor;
import android.os.Handler;
import androidx.core.provider.FontRequest;
import android.content.Context;

public class FontRequestEmojiCompatConfig extends Config
{
    private static final FontProviderHelper DEFAULT_FONTS_CONTRACT;
    
    static {
        DEFAULT_FONTS_CONTRACT = new FontProviderHelper();
    }
    
    public FontRequestEmojiCompatConfig(final Context context, final FontRequest fontRequest) {
        super(new FontRequestMetadataLoader(context, fontRequest, FontRequestEmojiCompatConfig.DEFAULT_FONTS_CONTRACT));
    }
    
    public FontRequestEmojiCompatConfig(final Context context, final FontRequest fontRequest, final FontProviderHelper fontProviderHelper) {
        super(new FontRequestMetadataLoader(context, fontRequest, fontProviderHelper));
    }
    
    @Deprecated
    public FontRequestEmojiCompatConfig setHandler(final Handler handler) {
        if (handler == null) {
            return this;
        }
        this.setLoadingExecutor(ConcurrencyHelpers.convertHandlerToExecutor(handler));
        return this;
    }
    
    public FontRequestEmojiCompatConfig setLoadingExecutor(final Executor executor) {
        ((FontRequestMetadataLoader)((EmojiCompat.Config)this).getMetadataRepoLoader()).setExecutor(executor);
        return this;
    }
    
    public FontRequestEmojiCompatConfig setRetryPolicy(final RetryPolicy retryPolicy) {
        ((FontRequestMetadataLoader)((EmojiCompat.Config)this).getMetadataRepoLoader()).setRetryPolicy(retryPolicy);
        return this;
    }
    
    public static class ExponentialBackoffRetryPolicy extends RetryPolicy
    {
        private long mRetryOrigin;
        private final long mTotalMs;
        
        public ExponentialBackoffRetryPolicy(final long mTotalMs) {
            this.mTotalMs = mTotalMs;
        }
        
        @Override
        public long getRetryDelay() {
            if (this.mRetryOrigin == 0L) {
                this.mRetryOrigin = SystemClock.uptimeMillis();
                return 0L;
            }
            final long a = SystemClock.uptimeMillis() - this.mRetryOrigin;
            if (a > this.mTotalMs) {
                return -1L;
            }
            return Math.min(Math.max(a, 1000L), this.mTotalMs - a);
        }
    }
    
    public abstract static class RetryPolicy
    {
        public abstract long getRetryDelay();
    }
    
    public static class FontProviderHelper
    {
        public Typeface buildTypeface(final Context context, final FontsContractCompat.FontInfo fontInfo) throws PackageManager$NameNotFoundException {
            return FontsContractCompat.buildTypeface(context, null, new FontsContractCompat.FontInfo[] { fontInfo });
        }
        
        public FontsContractCompat.FontFamilyResult fetchFonts(final Context context, final FontRequest fontRequest) throws PackageManager$NameNotFoundException {
            return FontsContractCompat.fetchFonts(context, null, fontRequest);
        }
        
        public void registerObserver(final Context context, final Uri uri, final ContentObserver contentObserver) {
            context.getContentResolver().registerContentObserver(uri, false, contentObserver);
        }
        
        public void unregisterObserver(final Context context, final ContentObserver contentObserver) {
            context.getContentResolver().unregisterContentObserver(contentObserver);
        }
    }
    
    private static class FontRequestMetadataLoader implements MetadataRepoLoader
    {
        private static final String S_TRACE_BUILD_TYPEFACE = "EmojiCompat.FontRequestEmojiCompatConfig.buildTypeface";
        MetadataRepoLoaderCallback mCallback;
        private final Context mContext;
        private Executor mExecutor;
        private final FontProviderHelper mFontProviderHelper;
        private final Object mLock;
        private Handler mMainHandler;
        private Runnable mMainHandlerLoadCallback;
        private ThreadPoolExecutor mMyThreadPoolExecutor;
        private ContentObserver mObserver;
        private final FontRequest mRequest;
        private RetryPolicy mRetryPolicy;
        
        FontRequestMetadataLoader(final Context context, final FontRequest mRequest, final FontProviderHelper mFontProviderHelper) {
            this.mLock = new Object();
            Preconditions.checkNotNull(context, "Context cannot be null");
            Preconditions.checkNotNull(mRequest, "FontRequest cannot be null");
            this.mContext = context.getApplicationContext();
            this.mRequest = mRequest;
            this.mFontProviderHelper = mFontProviderHelper;
        }
        
        private void cleanUp() {
            synchronized (this.mLock) {
                this.mCallback = null;
                final ContentObserver mObserver = this.mObserver;
                if (mObserver != null) {
                    this.mFontProviderHelper.unregisterObserver(this.mContext, mObserver);
                    this.mObserver = null;
                }
                final Handler mMainHandler = this.mMainHandler;
                if (mMainHandler != null) {
                    mMainHandler.removeCallbacks(this.mMainHandlerLoadCallback);
                }
                this.mMainHandler = null;
                final ThreadPoolExecutor mMyThreadPoolExecutor = this.mMyThreadPoolExecutor;
                if (mMyThreadPoolExecutor != null) {
                    mMyThreadPoolExecutor.shutdown();
                }
                this.mExecutor = null;
                this.mMyThreadPoolExecutor = null;
            }
        }
        
        private FontsContractCompat.FontInfo retrieveFontInfo() {
            try {
                final FontsContractCompat.FontFamilyResult fetchFonts = this.mFontProviderHelper.fetchFonts(this.mContext, this.mRequest);
                if (fetchFonts.getStatusCode() != 0) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("fetchFonts failed (");
                    sb.append(fetchFonts.getStatusCode());
                    sb.append(")");
                    throw new RuntimeException(sb.toString());
                }
                final FontsContractCompat.FontInfo[] fonts = fetchFonts.getFonts();
                if (fonts != null && fonts.length != 0) {
                    return fonts[0];
                }
                throw new RuntimeException("fetchFonts failed (empty result)");
            }
            catch (final PackageManager$NameNotFoundException cause) {
                throw new RuntimeException("provider not found", (Throwable)cause);
            }
        }
        
        private void scheduleRetry(final Uri uri, final long n) {
            synchronized (this.mLock) {
                Handler mMainHandler;
                if ((mMainHandler = this.mMainHandler) == null) {
                    mMainHandler = ConcurrencyHelpers.mainHandlerAsync();
                    this.mMainHandler = mMainHandler;
                }
                if (this.mObserver == null) {
                    final ContentObserver mObserver = new ContentObserver(this, mMainHandler) {
                        final FontRequestMetadataLoader this$0;
                        
                        public void onChange(final boolean b, final Uri uri) {
                            this.this$0.loadInternal();
                        }
                    };
                    this.mObserver = mObserver;
                    this.mFontProviderHelper.registerObserver(this.mContext, uri, mObserver);
                }
                if (this.mMainHandlerLoadCallback == null) {
                    this.mMainHandlerLoadCallback = new FontRequestEmojiCompatConfig$FontRequestMetadataLoader$$ExternalSyntheticLambda1(this);
                }
                mMainHandler.postDelayed(this.mMainHandlerLoadCallback, n);
            }
        }
        
        void createMetadata() {
            Object o = this.mLock;
            synchronized (o) {
                if (this.mCallback == null) {
                    return;
                }
                monitorexit(o);
                try {
                    o = this.retrieveFontInfo();
                    final int resultCode = ((FontsContractCompat.FontInfo)o).getResultCode();
                    if (resultCode == 2) {
                        synchronized (this.mLock) {
                            final RetryPolicy mRetryPolicy = this.mRetryPolicy;
                            if (mRetryPolicy != null) {
                                final long retryDelay = mRetryPolicy.getRetryDelay();
                                if (retryDelay >= 0L) {
                                    this.scheduleRetry(((FontsContractCompat.FontInfo)o).getUri(), retryDelay);
                                    return;
                                }
                            }
                        }
                    }
                    if (resultCode == 0) {
                        try {
                            TraceCompat.beginSection("EmojiCompat.FontRequestEmojiCompatConfig.buildTypeface");
                            final Typeface buildTypeface = this.mFontProviderHelper.buildTypeface(this.mContext, (FontsContractCompat.FontInfo)o);
                            final ByteBuffer mmap = TypefaceCompatUtil.mmap(this.mContext, null, ((FontsContractCompat.FontInfo)o).getUri());
                            if (mmap != null && buildTypeface != null) {
                                final MetadataRepo create = MetadataRepo.create(buildTypeface, mmap);
                                TraceCompat.endSection();
                                synchronized (this.mLock) {
                                    final MetadataRepoLoaderCallback mCallback = this.mCallback;
                                    if (mCallback != null) {
                                        mCallback.onLoaded(create);
                                    }
                                    monitorexit(this.mLock);
                                    this.cleanUp();
                                    return;
                                }
                            }
                            throw new RuntimeException("Unable to open file.");
                        }
                        finally {
                            TraceCompat.endSection();
                        }
                    }
                    o = new(java.lang.RuntimeException.class)();
                    final StringBuilder sb = new StringBuilder();
                    sb.append("fetchFonts result is not OK. (");
                    sb.append(resultCode);
                    sb.append(")");
                    new RuntimeException(sb.toString());
                    throw o;
                }
                finally {
                    synchronized (this.mLock) {
                        final MetadataRepoLoaderCallback mCallback2 = this.mCallback;
                        if (mCallback2 != null) {
                            final Throwable t;
                            mCallback2.onFailed(t);
                        }
                        monitorexit(this.mLock);
                        this.cleanUp();
                    }
                }
            }
        }
        
        @Override
        public void load(final MetadataRepoLoaderCallback mCallback) {
            Preconditions.checkNotNull(mCallback, "LoaderCallback cannot be null");
            synchronized (this.mLock) {
                this.mCallback = mCallback;
                monitorexit(this.mLock);
                this.loadInternal();
            }
        }
        
        void loadInternal() {
            synchronized (this.mLock) {
                if (this.mCallback == null) {
                    return;
                }
                if (this.mExecutor == null) {
                    final ThreadPoolExecutor backgroundPriorityExecutor = ConcurrencyHelpers.createBackgroundPriorityExecutor("emojiCompat");
                    this.mMyThreadPoolExecutor = backgroundPriorityExecutor;
                    this.mExecutor = backgroundPriorityExecutor;
                }
                this.mExecutor.execute(new FontRequestEmojiCompatConfig$FontRequestMetadataLoader$$ExternalSyntheticLambda0(this));
            }
        }
        
        public void setExecutor(final Executor mExecutor) {
            synchronized (this.mLock) {
                this.mExecutor = mExecutor;
            }
        }
        
        public void setRetryPolicy(final RetryPolicy mRetryPolicy) {
            synchronized (this.mLock) {
                this.mRetryPolicy = mRetryPolicy;
            }
        }
    }
}

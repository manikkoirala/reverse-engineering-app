// 
// Decompiled by Procyon v0.6.0
// 

package androidx.emoji2.text;

import java.util.Iterator;
import java.util.Arrays;
import java.util.List;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;
import android.os.Bundle;
import android.view.inputmethod.EditorInfo;
import java.util.ArrayList;
import android.content.Context;
import android.view.KeyEvent;
import android.text.Editable;
import android.view.inputmethod.InputConnection;
import androidx.core.util.Preconditions;
import android.os.Build$VERSION;
import java.util.Collection;
import androidx.collection.ArraySet;
import android.os.Looper;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import android.os.Handler;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.Set;

public class EmojiCompat
{
    private static final Object CONFIG_LOCK;
    public static final String EDITOR_INFO_METAVERSION_KEY = "android.support.text.emoji.emojiCompat_metadataVersion";
    public static final String EDITOR_INFO_REPLACE_ALL_KEY = "android.support.text.emoji.emojiCompat_replaceAll";
    static final int EMOJI_COUNT_UNLIMITED = Integer.MAX_VALUE;
    public static final int EMOJI_FALLBACK = 2;
    public static final int EMOJI_SUPPORTED = 1;
    public static final int EMOJI_UNSUPPORTED = 0;
    private static final Object INSTANCE_LOCK;
    public static final int LOAD_STATE_DEFAULT = 3;
    public static final int LOAD_STATE_FAILED = 2;
    public static final int LOAD_STATE_LOADING = 0;
    public static final int LOAD_STATE_SUCCEEDED = 1;
    public static final int LOAD_STRATEGY_DEFAULT = 0;
    public static final int LOAD_STRATEGY_MANUAL = 1;
    private static final String NOT_INITIALIZED_ERROR_TEXT = "EmojiCompat is not initialized.\n\nYou must initialize EmojiCompat prior to referencing the EmojiCompat instance.\n\nThe most likely cause of this error is disabling the EmojiCompatInitializer\neither explicitly in AndroidManifest.xml, or by including\nandroidx.emoji2:emoji2-bundled.\n\nAutomatic initialization is typically performed by EmojiCompatInitializer. If\nyou are not expecting to initialize EmojiCompat manually in your application,\nplease check to ensure it has not been removed from your APK's manifest. You can\ndo this in Android Studio using Build > Analyze APK.\n\nIn the APK Analyzer, ensure that the startup entry for\nEmojiCompatInitializer and InitializationProvider is present in\n AndroidManifest.xml. If it is missing or contains tools:node=\"remove\", and you\nintend to use automatic configuration, verify:\n\n  1. Your application does not include emoji2-bundled\n  2. All modules do not contain an exclusion manifest rule for\n     EmojiCompatInitializer or InitializationProvider. For more information\n     about manifest exclusions see the documentation for the androidx startup\n     library.\n\nIf you intend to use emoji2-bundled, please call EmojiCompat.init. You can\nlearn more in the documentation for BundledEmojiCompatConfig.\n\nIf you intended to perform manual configuration, it is recommended that you call\nEmojiCompat.init immediately on application startup.\n\nIf you still cannot resolve this issue, please open a bug with your specific\nconfiguration to help improve error message.";
    public static final int REPLACE_STRATEGY_ALL = 1;
    public static final int REPLACE_STRATEGY_DEFAULT = 0;
    public static final int REPLACE_STRATEGY_NON_EXISTENT = 2;
    private static volatile boolean sHasDoneDefaultConfigLookup;
    private static volatile EmojiCompat sInstance;
    final int[] mEmojiAsDefaultStyleExceptions;
    private final int mEmojiSpanIndicatorColor;
    private final boolean mEmojiSpanIndicatorEnabled;
    private final GlyphChecker mGlyphChecker;
    private final CompatInternal mHelper;
    private final Set<InitCallback> mInitCallbacks;
    private final ReadWriteLock mInitLock;
    private volatile int mLoadState;
    private final Handler mMainHandler;
    private final int mMetadataLoadStrategy;
    final MetadataRepoLoader mMetadataLoader;
    final boolean mReplaceAll;
    private final SpanFactory mSpanFactory;
    final boolean mUseEmojiAsDefaultStyle;
    
    static {
        INSTANCE_LOCK = new Object();
        CONFIG_LOCK = new Object();
    }
    
    private EmojiCompat(final Config config) {
        this.mInitLock = new ReentrantReadWriteLock();
        this.mLoadState = 3;
        this.mReplaceAll = config.mReplaceAll;
        this.mUseEmojiAsDefaultStyle = config.mUseEmojiAsDefaultStyle;
        this.mEmojiAsDefaultStyleExceptions = config.mEmojiAsDefaultStyleExceptions;
        this.mEmojiSpanIndicatorEnabled = config.mEmojiSpanIndicatorEnabled;
        this.mEmojiSpanIndicatorColor = config.mEmojiSpanIndicatorColor;
        this.mMetadataLoader = config.mMetadataLoader;
        this.mMetadataLoadStrategy = config.mMetadataLoadStrategy;
        this.mGlyphChecker = config.mGlyphChecker;
        this.mMainHandler = new Handler(Looper.getMainLooper());
        final ArraySet mInitCallbacks = new ArraySet();
        this.mInitCallbacks = mInitCallbacks;
        SpanFactory mSpanFactory = config.mSpanFactory;
        if (mSpanFactory == null) {
            mSpanFactory = new DefaultSpanFactory();
        }
        this.mSpanFactory = mSpanFactory;
        if (config.mInitCallbacks != null && !config.mInitCallbacks.isEmpty()) {
            mInitCallbacks.addAll(config.mInitCallbacks);
        }
        Object mHelper;
        if (Build$VERSION.SDK_INT < 19) {
            mHelper = new CompatInternal(this);
        }
        else {
            mHelper = new CompatInternal19(this);
        }
        this.mHelper = (CompatInternal)mHelper;
        this.loadMetadata();
    }
    
    public static EmojiCompat get() {
        synchronized (EmojiCompat.INSTANCE_LOCK) {
            final EmojiCompat sInstance = EmojiCompat.sInstance;
            Preconditions.checkState(sInstance != null, "EmojiCompat is not initialized.\n\nYou must initialize EmojiCompat prior to referencing the EmojiCompat instance.\n\nThe most likely cause of this error is disabling the EmojiCompatInitializer\neither explicitly in AndroidManifest.xml, or by including\nandroidx.emoji2:emoji2-bundled.\n\nAutomatic initialization is typically performed by EmojiCompatInitializer. If\nyou are not expecting to initialize EmojiCompat manually in your application,\nplease check to ensure it has not been removed from your APK's manifest. You can\ndo this in Android Studio using Build > Analyze APK.\n\nIn the APK Analyzer, ensure that the startup entry for\nEmojiCompatInitializer and InitializationProvider is present in\n AndroidManifest.xml. If it is missing or contains tools:node=\"remove\", and you\nintend to use automatic configuration, verify:\n\n  1. Your application does not include emoji2-bundled\n  2. All modules do not contain an exclusion manifest rule for\n     EmojiCompatInitializer or InitializationProvider. For more information\n     about manifest exclusions see the documentation for the androidx startup\n     library.\n\nIf you intend to use emoji2-bundled, please call EmojiCompat.init. You can\nlearn more in the documentation for BundledEmojiCompatConfig.\n\nIf you intended to perform manual configuration, it is recommended that you call\nEmojiCompat.init immediately on application startup.\n\nIf you still cannot resolve this issue, please open a bug with your specific\nconfiguration to help improve error message.");
            return sInstance;
        }
    }
    
    public static boolean handleDeleteSurroundingText(final InputConnection inputConnection, final Editable editable, final int n, final int n2, final boolean b) {
        return Build$VERSION.SDK_INT >= 19 && EmojiProcessor.handleDeleteSurroundingText(inputConnection, editable, n, n2, b);
    }
    
    public static boolean handleOnKeyDown(final Editable editable, final int n, final KeyEvent keyEvent) {
        return Build$VERSION.SDK_INT >= 19 && EmojiProcessor.handleOnKeyDown(editable, n, keyEvent);
    }
    
    public static EmojiCompat init(final Context context) {
        return init(context, null);
    }
    
    public static EmojiCompat init(final Context context, DefaultEmojiCompatConfig.DefaultEmojiCompatConfigFactory defaultEmojiCompatConfigFactory) {
        if (EmojiCompat.sHasDoneDefaultConfigLookup) {
            return EmojiCompat.sInstance;
        }
        if (defaultEmojiCompatConfigFactory == null) {
            defaultEmojiCompatConfigFactory = new DefaultEmojiCompatConfig.DefaultEmojiCompatConfigFactory(null);
        }
        final Config create = defaultEmojiCompatConfigFactory.create(context);
        synchronized (EmojiCompat.CONFIG_LOCK) {
            if (!EmojiCompat.sHasDoneDefaultConfigLookup) {
                if (create != null) {
                    init(create);
                }
                EmojiCompat.sHasDoneDefaultConfigLookup = true;
            }
            return EmojiCompat.sInstance;
        }
    }
    
    public static EmojiCompat init(final Config config) {
        EmojiCompat emojiCompat;
        if ((emojiCompat = EmojiCompat.sInstance) == null) {
            synchronized (EmojiCompat.INSTANCE_LOCK) {
                if ((emojiCompat = EmojiCompat.sInstance) == null) {
                    emojiCompat = (EmojiCompat.sInstance = new EmojiCompat(config));
                }
            }
        }
        return emojiCompat;
    }
    
    public static boolean isConfigured() {
        return EmojiCompat.sInstance != null;
    }
    
    private boolean isInitialized() {
        final int loadState = this.getLoadState();
        boolean b = true;
        if (loadState != 1) {
            b = false;
        }
        return b;
    }
    
    private void loadMetadata() {
        this.mInitLock.writeLock().lock();
        try {
            if (this.mMetadataLoadStrategy == 0) {
                this.mLoadState = 0;
            }
            this.mInitLock.writeLock().unlock();
            if (this.getLoadState() == 0) {
                this.mHelper.loadMetadata();
            }
        }
        finally {
            this.mInitLock.writeLock().unlock();
        }
    }
    
    public static EmojiCompat reset(final Config config) {
        synchronized (EmojiCompat.INSTANCE_LOCK) {
            return EmojiCompat.sInstance = new EmojiCompat(config);
        }
    }
    
    public static EmojiCompat reset(EmojiCompat sInstance) {
        synchronized (EmojiCompat.INSTANCE_LOCK) {
            EmojiCompat.sInstance = sInstance;
            sInstance = EmojiCompat.sInstance;
            return sInstance;
        }
    }
    
    public static void skipDefaultConfigurationLookup(final boolean sHasDoneDefaultConfigLookup) {
        synchronized (EmojiCompat.CONFIG_LOCK) {
            EmojiCompat.sHasDoneDefaultConfigLookup = sHasDoneDefaultConfigLookup;
        }
    }
    
    public String getAssetSignature() {
        Preconditions.checkState(this.isInitialized(), "Not initialized yet");
        return this.mHelper.getAssetSignature();
    }
    
    public int getEmojiEnd(final CharSequence charSequence, final int n) {
        Preconditions.checkState(this.isInitialized(), "Not initialized yet");
        Preconditions.checkNotNull(charSequence, "charSequence cannot be null");
        return this.mHelper.getEmojiEnd(charSequence, n);
    }
    
    public int getEmojiMatch(final CharSequence charSequence, final int n) {
        Preconditions.checkState(this.isInitialized(), "Not initialized yet");
        Preconditions.checkNotNull(charSequence, "sequence cannot be null");
        return this.mHelper.getEmojiMatch(charSequence, n);
    }
    
    public int getEmojiSpanIndicatorColor() {
        return this.mEmojiSpanIndicatorColor;
    }
    
    public int getEmojiStart(final CharSequence charSequence, final int n) {
        Preconditions.checkState(this.isInitialized(), "Not initialized yet");
        Preconditions.checkNotNull(charSequence, "charSequence cannot be null");
        return this.mHelper.getEmojiStart(charSequence, n);
    }
    
    public int getLoadState() {
        this.mInitLock.readLock().lock();
        try {
            return this.mLoadState;
        }
        finally {
            this.mInitLock.readLock().unlock();
        }
    }
    
    @Deprecated
    public boolean hasEmojiGlyph(final CharSequence charSequence) {
        Preconditions.checkState(this.isInitialized(), "Not initialized yet");
        Preconditions.checkNotNull(charSequence, "sequence cannot be null");
        return this.mHelper.hasEmojiGlyph(charSequence);
    }
    
    @Deprecated
    public boolean hasEmojiGlyph(final CharSequence charSequence, final int n) {
        Preconditions.checkState(this.isInitialized(), "Not initialized yet");
        Preconditions.checkNotNull(charSequence, "sequence cannot be null");
        return this.mHelper.hasEmojiGlyph(charSequence, n);
    }
    
    public boolean isEmojiSpanIndicatorEnabled() {
        return this.mEmojiSpanIndicatorEnabled;
    }
    
    public void load() {
        final int mMetadataLoadStrategy = this.mMetadataLoadStrategy;
        boolean b = true;
        if (mMetadataLoadStrategy != 1) {
            b = false;
        }
        Preconditions.checkState(b, "Set metadataLoadStrategy to LOAD_STRATEGY_MANUAL to execute manual loading");
        if (this.isInitialized()) {
            return;
        }
        this.mInitLock.writeLock().lock();
        try {
            if (this.mLoadState == 0) {
                return;
            }
            this.mLoadState = 0;
            this.mInitLock.writeLock().unlock();
            this.mHelper.loadMetadata();
        }
        finally {
            this.mInitLock.writeLock().unlock();
        }
    }
    
    void onMetadataLoadFailed(final Throwable t) {
        final ArrayList list = new ArrayList();
        this.mInitLock.writeLock().lock();
        try {
            this.mLoadState = 2;
            list.addAll(this.mInitCallbacks);
            this.mInitCallbacks.clear();
            this.mInitLock.writeLock().unlock();
            this.mMainHandler.post((Runnable)new ListenerDispatcher(list, this.mLoadState, t));
        }
        finally {
            this.mInitLock.writeLock().unlock();
        }
    }
    
    void onMetadataLoadSuccess() {
        final ArrayList list = new ArrayList();
        this.mInitLock.writeLock().lock();
        try {
            this.mLoadState = 1;
            list.addAll(this.mInitCallbacks);
            this.mInitCallbacks.clear();
            this.mInitLock.writeLock().unlock();
            this.mMainHandler.post((Runnable)new ListenerDispatcher(list, this.mLoadState));
        }
        finally {
            this.mInitLock.writeLock().unlock();
        }
    }
    
    public CharSequence process(final CharSequence charSequence) {
        int length;
        if (charSequence == null) {
            length = 0;
        }
        else {
            length = charSequence.length();
        }
        return this.process(charSequence, 0, length);
    }
    
    public CharSequence process(final CharSequence charSequence, final int n, final int n2) {
        return this.process(charSequence, n, n2, Integer.MAX_VALUE);
    }
    
    public CharSequence process(final CharSequence charSequence, final int n, final int n2, final int n3) {
        return this.process(charSequence, n, n2, n3, 0);
    }
    
    public CharSequence process(final CharSequence charSequence, final int n, final int n2, final int n3, final int n4) {
        Preconditions.checkState(this.isInitialized(), "Not initialized yet");
        Preconditions.checkArgumentNonnegative(n, "start cannot be negative");
        Preconditions.checkArgumentNonnegative(n2, "end cannot be negative");
        Preconditions.checkArgumentNonnegative(n3, "maxEmojiCount cannot be negative");
        Preconditions.checkArgument(n <= n2, (Object)"start should be <= than end");
        if (charSequence == null) {
            return null;
        }
        Preconditions.checkArgument(n <= charSequence.length(), (Object)"start should be < than charSequence length");
        Preconditions.checkArgument(n2 <= charSequence.length(), (Object)"end should be < than charSequence length");
        CharSequence process = charSequence;
        if (charSequence.length() != 0) {
            if (n == n2) {
                process = charSequence;
            }
            else {
                process = this.mHelper.process(charSequence, n, n2, n3, n4 == 1 || (n4 != 2 && this.mReplaceAll));
            }
        }
        return process;
    }
    
    public void registerInitCallback(final InitCallback initCallback) {
        Preconditions.checkNotNull(initCallback, "initCallback cannot be null");
        this.mInitLock.writeLock().lock();
        try {
            if (this.mLoadState != 1 && this.mLoadState != 2) {
                this.mInitCallbacks.add(initCallback);
            }
            else {
                this.mMainHandler.post((Runnable)new ListenerDispatcher(initCallback, this.mLoadState));
            }
        }
        finally {
            this.mInitLock.writeLock().unlock();
        }
    }
    
    public void unregisterInitCallback(final InitCallback initCallback) {
        Preconditions.checkNotNull(initCallback, "initCallback cannot be null");
        this.mInitLock.writeLock().lock();
        try {
            this.mInitCallbacks.remove(initCallback);
        }
        finally {
            this.mInitLock.writeLock().unlock();
        }
    }
    
    public void updateEditorInfo(final EditorInfo editorInfo) {
        if (this.isInitialized()) {
            if (editorInfo != null) {
                if (editorInfo.extras == null) {
                    editorInfo.extras = new Bundle();
                }
                this.mHelper.updateEditorInfoAttrs(editorInfo);
            }
        }
    }
    
    @Retention(RetentionPolicy.SOURCE)
    public @interface CodepointSequenceMatchResult {
    }
    
    private static class CompatInternal
    {
        final EmojiCompat mEmojiCompat;
        
        CompatInternal(final EmojiCompat mEmojiCompat) {
            this.mEmojiCompat = mEmojiCompat;
        }
        
        String getAssetSignature() {
            return "";
        }
        
        int getEmojiEnd(final CharSequence charSequence, final int n) {
            return -1;
        }
        
        public int getEmojiMatch(final CharSequence charSequence, final int n) {
            return 0;
        }
        
        int getEmojiStart(final CharSequence charSequence, final int n) {
            return -1;
        }
        
        boolean hasEmojiGlyph(final CharSequence charSequence) {
            return false;
        }
        
        boolean hasEmojiGlyph(final CharSequence charSequence, final int n) {
            return false;
        }
        
        void loadMetadata() {
            this.mEmojiCompat.onMetadataLoadSuccess();
        }
        
        CharSequence process(final CharSequence charSequence, final int n, final int n2, final int n3, final boolean b) {
            return charSequence;
        }
        
        void updateEditorInfoAttrs(final EditorInfo editorInfo) {
        }
    }
    
    private static final class CompatInternal19 extends CompatInternal
    {
        private volatile MetadataRepo mMetadataRepo;
        private volatile EmojiProcessor mProcessor;
        
        CompatInternal19(final EmojiCompat emojiCompat) {
            super(emojiCompat);
        }
        
        @Override
        String getAssetSignature() {
            String sourceSha;
            if ((sourceSha = this.mMetadataRepo.getMetadataList().sourceSha()) == null) {
                sourceSha = "";
            }
            return sourceSha;
        }
        
        @Override
        int getEmojiEnd(final CharSequence charSequence, final int n) {
            return this.mProcessor.getEmojiEnd(charSequence, n);
        }
        
        @Override
        public int getEmojiMatch(final CharSequence charSequence, final int n) {
            return this.mProcessor.getEmojiMatch(charSequence, n);
        }
        
        @Override
        int getEmojiStart(final CharSequence charSequence, final int n) {
            return this.mProcessor.getEmojiStart(charSequence, n);
        }
        
        @Override
        boolean hasEmojiGlyph(final CharSequence charSequence) {
            final int emojiMatch = this.mProcessor.getEmojiMatch(charSequence);
            boolean b = true;
            if (emojiMatch != 1) {
                b = false;
            }
            return b;
        }
        
        @Override
        boolean hasEmojiGlyph(final CharSequence charSequence, int emojiMatch) {
            emojiMatch = this.mProcessor.getEmojiMatch(charSequence, emojiMatch);
            boolean b = true;
            if (emojiMatch != 1) {
                b = false;
            }
            return b;
        }
        
        @Override
        void loadMetadata() {
            try {
                this.mEmojiCompat.mMetadataLoader.load(new MetadataRepoLoaderCallback(this) {
                    final CompatInternal19 this$0;
                    
                    @Override
                    public void onFailed(final Throwable t) {
                        this.this$0.mEmojiCompat.onMetadataLoadFailed(t);
                    }
                    
                    @Override
                    public void onLoaded(final MetadataRepo metadataRepo) {
                        this.this$0.onMetadataLoadSuccess(metadataRepo);
                    }
                });
            }
            finally {
                final Throwable t;
                this.mEmojiCompat.onMetadataLoadFailed(t);
            }
        }
        
        void onMetadataLoadSuccess(final MetadataRepo mMetadataRepo) {
            if (mMetadataRepo == null) {
                this.mEmojiCompat.onMetadataLoadFailed(new IllegalArgumentException("metadataRepo cannot be null"));
                return;
            }
            this.mMetadataRepo = mMetadataRepo;
            this.mProcessor = new EmojiProcessor(this.mMetadataRepo, this.mEmojiCompat.mSpanFactory, this.mEmojiCompat.mGlyphChecker, this.mEmojiCompat.mUseEmojiAsDefaultStyle, this.mEmojiCompat.mEmojiAsDefaultStyleExceptions, EmojiExclusions.getEmojiExclusions());
            this.mEmojiCompat.onMetadataLoadSuccess();
        }
        
        @Override
        CharSequence process(final CharSequence charSequence, final int n, final int n2, final int n3, final boolean b) {
            return this.mProcessor.process(charSequence, n, n2, n3, b);
        }
        
        @Override
        void updateEditorInfoAttrs(final EditorInfo editorInfo) {
            editorInfo.extras.putInt("android.support.text.emoji.emojiCompat_metadataVersion", this.mMetadataRepo.getMetadataVersion());
            editorInfo.extras.putBoolean("android.support.text.emoji.emojiCompat_replaceAll", this.mEmojiCompat.mReplaceAll);
        }
    }
    
    public abstract static class MetadataRepoLoaderCallback
    {
        public abstract void onFailed(final Throwable p0);
        
        public abstract void onLoaded(final MetadataRepo p0);
    }
    
    public abstract static class Config
    {
        int[] mEmojiAsDefaultStyleExceptions;
        int mEmojiSpanIndicatorColor;
        boolean mEmojiSpanIndicatorEnabled;
        GlyphChecker mGlyphChecker;
        Set<InitCallback> mInitCallbacks;
        int mMetadataLoadStrategy;
        final MetadataRepoLoader mMetadataLoader;
        boolean mReplaceAll;
        SpanFactory mSpanFactory;
        boolean mUseEmojiAsDefaultStyle;
        
        protected Config(final MetadataRepoLoader mMetadataLoader) {
            this.mEmojiSpanIndicatorColor = -16711936;
            this.mMetadataLoadStrategy = 0;
            this.mGlyphChecker = new DefaultGlyphChecker();
            Preconditions.checkNotNull(mMetadataLoader, "metadataLoader cannot be null.");
            this.mMetadataLoader = mMetadataLoader;
        }
        
        protected final MetadataRepoLoader getMetadataRepoLoader() {
            return this.mMetadataLoader;
        }
        
        public Config registerInitCallback(final InitCallback initCallback) {
            Preconditions.checkNotNull(initCallback, "initCallback cannot be null");
            if (this.mInitCallbacks == null) {
                this.mInitCallbacks = new ArraySet<InitCallback>();
            }
            this.mInitCallbacks.add(initCallback);
            return this;
        }
        
        public Config setEmojiSpanIndicatorColor(final int mEmojiSpanIndicatorColor) {
            this.mEmojiSpanIndicatorColor = mEmojiSpanIndicatorColor;
            return this;
        }
        
        public Config setEmojiSpanIndicatorEnabled(final boolean mEmojiSpanIndicatorEnabled) {
            this.mEmojiSpanIndicatorEnabled = mEmojiSpanIndicatorEnabled;
            return this;
        }
        
        public Config setGlyphChecker(final GlyphChecker mGlyphChecker) {
            Preconditions.checkNotNull(mGlyphChecker, "GlyphChecker cannot be null");
            this.mGlyphChecker = mGlyphChecker;
            return this;
        }
        
        public Config setMetadataLoadStrategy(final int mMetadataLoadStrategy) {
            this.mMetadataLoadStrategy = mMetadataLoadStrategy;
            return this;
        }
        
        public Config setReplaceAll(final boolean mReplaceAll) {
            this.mReplaceAll = mReplaceAll;
            return this;
        }
        
        public Config setSpanFactory(final SpanFactory mSpanFactory) {
            this.mSpanFactory = mSpanFactory;
            return this;
        }
        
        public Config setUseEmojiAsDefaultStyle(final boolean b) {
            return this.setUseEmojiAsDefaultStyle(b, null);
        }
        
        public Config setUseEmojiAsDefaultStyle(final boolean mUseEmojiAsDefaultStyle, final List<Integer> list) {
            this.mUseEmojiAsDefaultStyle = mUseEmojiAsDefaultStyle;
            if (mUseEmojiAsDefaultStyle && list != null) {
                this.mEmojiAsDefaultStyleExceptions = new int[list.size()];
                int n = 0;
                final Iterator iterator = list.iterator();
                while (iterator.hasNext()) {
                    this.mEmojiAsDefaultStyleExceptions[n] = (int)iterator.next();
                    ++n;
                }
                Arrays.sort(this.mEmojiAsDefaultStyleExceptions);
            }
            else {
                this.mEmojiAsDefaultStyleExceptions = null;
            }
            return this;
        }
        
        public Config unregisterInitCallback(final InitCallback initCallback) {
            Preconditions.checkNotNull(initCallback, "initCallback cannot be null");
            final Set<InitCallback> mInitCallbacks = this.mInitCallbacks;
            if (mInitCallbacks != null) {
                mInitCallbacks.remove(initCallback);
            }
            return this;
        }
    }
    
    public static class DefaultSpanFactory implements SpanFactory
    {
        @Override
        public EmojiSpan createSpan(final TypefaceEmojiRasterizer typefaceEmojiRasterizer) {
            return new TypefaceEmojiSpan(typefaceEmojiRasterizer);
        }
    }
    
    public interface SpanFactory
    {
        EmojiSpan createSpan(final TypefaceEmojiRasterizer p0);
    }
    
    public interface GlyphChecker
    {
        boolean hasGlyph(final CharSequence p0, final int p1, final int p2, final int p3);
    }
    
    public abstract static class InitCallback
    {
        public void onFailed(final Throwable t) {
        }
        
        public void onInitialized() {
        }
    }
    
    private static class ListenerDispatcher implements Runnable
    {
        private final List<InitCallback> mInitCallbacks;
        private final int mLoadState;
        private final Throwable mThrowable;
        
        ListenerDispatcher(final InitCallback initCallback, final int n) {
            this(Arrays.asList(Preconditions.checkNotNull(initCallback, "initCallback cannot be null")), n, null);
        }
        
        ListenerDispatcher(final Collection<InitCallback> collection, final int n) {
            this(collection, n, null);
        }
        
        ListenerDispatcher(final Collection<InitCallback> c, final int mLoadState, final Throwable mThrowable) {
            Preconditions.checkNotNull(c, "initCallbacks cannot be null");
            this.mInitCallbacks = new ArrayList<InitCallback>(c);
            this.mLoadState = mLoadState;
            this.mThrowable = mThrowable;
        }
        
        @Override
        public void run() {
            final int size = this.mInitCallbacks.size();
            final int mLoadState = this.mLoadState;
            int i = 0;
            final int n = 0;
            if (mLoadState != 1) {
                for (int j = n; j < size; ++j) {
                    this.mInitCallbacks.get(j).onFailed(this.mThrowable);
                }
            }
            else {
                while (i < size) {
                    this.mInitCallbacks.get(i).onInitialized();
                    ++i;
                }
            }
        }
    }
    
    @Retention(RetentionPolicy.SOURCE)
    public @interface LoadStrategy {
    }
    
    public interface MetadataRepoLoader
    {
        void load(final MetadataRepoLoaderCallback p0);
    }
    
    @Retention(RetentionPolicy.SOURCE)
    public @interface ReplaceStrategy {
    }
}

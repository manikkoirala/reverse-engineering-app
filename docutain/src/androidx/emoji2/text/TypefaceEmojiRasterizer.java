// 
// Decompiled by Procyon v0.6.0
// 

package androidx.emoji2.text;

import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;
import android.graphics.Typeface;
import android.graphics.Paint;
import android.graphics.Canvas;
import androidx.emoji2.text.flatbuffer.MetadataItem;

public class TypefaceEmojiRasterizer
{
    static final int HAS_GLYPH_ABSENT = 1;
    static final int HAS_GLYPH_EXISTS = 2;
    static final int HAS_GLYPH_UNKNOWN = 0;
    private static final ThreadLocal<MetadataItem> sMetadataItem;
    private volatile int mCache;
    private final int mIndex;
    private final MetadataRepo mMetadataRepo;
    
    static {
        sMetadataItem = new ThreadLocal<MetadataItem>();
    }
    
    TypefaceEmojiRasterizer(final MetadataRepo mMetadataRepo, final int mIndex) {
        this.mCache = 0;
        this.mMetadataRepo = mMetadataRepo;
        this.mIndex = mIndex;
    }
    
    private MetadataItem getMetadataItem() {
        final ThreadLocal<MetadataItem> sMetadataItem = TypefaceEmojiRasterizer.sMetadataItem;
        MetadataItem value;
        if ((value = sMetadataItem.get()) == null) {
            value = new MetadataItem();
            sMetadataItem.set(value);
        }
        this.mMetadataRepo.getMetadataList().list(value, this.mIndex);
        return value;
    }
    
    public void draw(final Canvas canvas, final float n, final float n2, final Paint paint) {
        final Typeface typeface = this.mMetadataRepo.getTypeface();
        final Typeface typeface2 = paint.getTypeface();
        paint.setTypeface(typeface);
        canvas.drawText(this.mMetadataRepo.getEmojiCharArray(), this.mIndex * 2, 2, n, n2, paint);
        paint.setTypeface(typeface2);
    }
    
    public int getCodepointAt(final int n) {
        return this.getMetadataItem().codepoints(n);
    }
    
    public int getCodepointsLength() {
        return this.getMetadataItem().codepointsLength();
    }
    
    public short getCompatAdded() {
        return this.getMetadataItem().compatAdded();
    }
    
    public int getHasGlyph() {
        return this.mCache & 0x3;
    }
    
    public int getHeight() {
        return this.getMetadataItem().height();
    }
    
    public int getId() {
        return this.getMetadataItem().id();
    }
    
    public short getSdkAdded() {
        return this.getMetadataItem().sdkAdded();
    }
    
    public Typeface getTypeface() {
        return this.mMetadataRepo.getTypeface();
    }
    
    public int getWidth() {
        return this.getMetadataItem().width();
    }
    
    public boolean isDefaultEmoji() {
        return this.getMetadataItem().emojiStyle();
    }
    
    public boolean isPreferredSystemRender() {
        return (this.mCache & 0x4) > 0;
    }
    
    public void resetHasGlyphCache() {
        if (this.isPreferredSystemRender()) {
            this.mCache = 4;
        }
        else {
            this.mCache = 0;
        }
    }
    
    public void setExclusion(final boolean b) {
        final int hasGlyph = this.getHasGlyph();
        if (b) {
            this.mCache = (hasGlyph | 0x4);
        }
        else {
            this.mCache = hasGlyph;
        }
    }
    
    public void setHasGlyph(final boolean b) {
        final int n = this.mCache & 0x4;
        int mCache;
        if (b) {
            mCache = (n | 0x2);
        }
        else {
            mCache = (n | 0x1);
        }
        this.mCache = mCache;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append(super.toString());
        sb.append(", id:");
        sb.append(Integer.toHexString(this.getId()));
        sb.append(", codepoints:");
        for (int codepointsLength = this.getCodepointsLength(), i = 0; i < codepointsLength; ++i) {
            sb.append(Integer.toHexString(this.getCodepointAt(i)));
            sb.append(" ");
        }
        return sb.toString();
    }
    
    @Retention(RetentionPolicy.SOURCE)
    public @interface HasGlyph {
    }
}

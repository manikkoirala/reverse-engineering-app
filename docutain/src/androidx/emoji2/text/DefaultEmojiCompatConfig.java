// 
// Decompiled by Procyon v0.6.0
// 

package androidx.emoji2.text;

import android.util.Log;
import androidx.core.util.Preconditions;
import java.util.Iterator;
import android.content.pm.ResolveInfo;
import android.content.Intent;
import android.os.Build$VERSION;
import android.content.pm.PackageManager$NameNotFoundException;
import android.content.pm.PackageManager;
import android.content.pm.ProviderInfo;
import java.util.Collections;
import java.util.ArrayList;
import java.util.List;
import android.content.pm.Signature;
import androidx.core.provider.FontRequest;
import android.content.Context;

public final class DefaultEmojiCompatConfig
{
    private DefaultEmojiCompatConfig() {
    }
    
    public static FontRequestEmojiCompatConfig create(final Context context) {
        return (FontRequestEmojiCompatConfig)new DefaultEmojiCompatConfigFactory(null).create(context);
    }
    
    public static class DefaultEmojiCompatConfigFactory
    {
        private static final String DEFAULT_EMOJI_QUERY = "emojicompat-emoji-font";
        private static final String INTENT_LOAD_EMOJI_FONT = "androidx.content.action.LOAD_EMOJI_FONT";
        private static final String TAG = "emoji2.text.DefaultEmojiConfig";
        private final DefaultEmojiCompatConfigHelper mHelper;
        
        public DefaultEmojiCompatConfigFactory(DefaultEmojiCompatConfigHelper helperForApi) {
            if (helperForApi == null) {
                helperForApi = getHelperForApi();
            }
            this.mHelper = helperForApi;
        }
        
        private EmojiCompat.Config configOrNull(final Context context, final FontRequest fontRequest) {
            if (fontRequest == null) {
                return null;
            }
            return new FontRequestEmojiCompatConfig(context, fontRequest);
        }
        
        private List<List<byte[]>> convertToByteArray(final Signature[] array) {
            final ArrayList o = new ArrayList();
            for (int length = array.length, i = 0; i < length; ++i) {
                o.add(array[i].toByteArray());
            }
            return (List<List<byte[]>>)Collections.singletonList(o);
        }
        
        private FontRequest generateFontRequestFrom(final ProviderInfo providerInfo, final PackageManager packageManager) throws PackageManager$NameNotFoundException {
            final String authority = providerInfo.authority;
            final String packageName = providerInfo.packageName;
            return new FontRequest(authority, packageName, "emojicompat-emoji-font", this.convertToByteArray(this.mHelper.getSigningSignatures(packageManager, packageName)));
        }
        
        private static DefaultEmojiCompatConfigHelper getHelperForApi() {
            if (Build$VERSION.SDK_INT >= 28) {
                return new DefaultEmojiCompatConfigHelper_API28();
            }
            if (Build$VERSION.SDK_INT >= 19) {
                return new DefaultEmojiCompatConfigHelper_API19();
            }
            return new DefaultEmojiCompatConfigHelper();
        }
        
        private boolean hasFlagSystem(final ProviderInfo providerInfo) {
            boolean b = true;
            if (providerInfo == null || providerInfo.applicationInfo == null || (providerInfo.applicationInfo.flags & 0x1) != 0x1) {
                b = false;
            }
            return b;
        }
        
        private ProviderInfo queryDefaultInstalledContentProvider(final PackageManager packageManager) {
            final Iterator<ResolveInfo> iterator = this.mHelper.queryIntentContentProviders(packageManager, new Intent("androidx.content.action.LOAD_EMOJI_FONT"), 0).iterator();
            while (iterator.hasNext()) {
                final ProviderInfo providerInfo = this.mHelper.getProviderInfo(iterator.next());
                if (this.hasFlagSystem(providerInfo)) {
                    return providerInfo;
                }
            }
            return null;
        }
        
        public EmojiCompat.Config create(final Context context) {
            return this.configOrNull(context, this.queryForDefaultFontRequest(context));
        }
        
        FontRequest queryForDefaultFontRequest(final Context context) {
            final PackageManager packageManager = context.getPackageManager();
            Preconditions.checkNotNull(packageManager, "Package manager required to locate emoji font provider");
            final ProviderInfo queryDefaultInstalledContentProvider = this.queryDefaultInstalledContentProvider(packageManager);
            if (queryDefaultInstalledContentProvider == null) {
                return null;
            }
            try {
                return this.generateFontRequestFrom(queryDefaultInstalledContentProvider, packageManager);
            }
            catch (final PackageManager$NameNotFoundException ex) {
                Log.wtf("emoji2.text.DefaultEmojiConfig", (Throwable)ex);
                return null;
            }
        }
    }
    
    public static class DefaultEmojiCompatConfigHelper
    {
        public ProviderInfo getProviderInfo(final ResolveInfo resolveInfo) {
            throw new IllegalStateException("Unable to get provider info prior to API 19");
        }
        
        public Signature[] getSigningSignatures(final PackageManager packageManager, final String s) throws PackageManager$NameNotFoundException {
            return packageManager.getPackageInfo(s, 64).signatures;
        }
        
        public List<ResolveInfo> queryIntentContentProviders(final PackageManager packageManager, final Intent intent, final int n) {
            return Collections.emptyList();
        }
    }
    
    public static class DefaultEmojiCompatConfigHelper_API19 extends DefaultEmojiCompatConfigHelper
    {
        @Override
        public ProviderInfo getProviderInfo(final ResolveInfo resolveInfo) {
            return resolveInfo.providerInfo;
        }
        
        @Override
        public List<ResolveInfo> queryIntentContentProviders(final PackageManager packageManager, final Intent intent, final int n) {
            return packageManager.queryIntentContentProviders(intent, n);
        }
    }
    
    public static class DefaultEmojiCompatConfigHelper_API28 extends DefaultEmojiCompatConfigHelper_API19
    {
        @Override
        public Signature[] getSigningSignatures(final PackageManager packageManager, final String s) throws PackageManager$NameNotFoundException {
            return packageManager.getPackageInfo(s, 64).signatures;
        }
    }
}

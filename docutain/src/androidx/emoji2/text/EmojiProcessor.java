// 
// Decompiled by Procyon v0.6.0
// 

package androidx.emoji2.text;

import java.util.Arrays;
import android.text.TextUtils;
import android.text.SpannableString;
import android.text.Spanned;
import java.util.Iterator;
import android.text.Spannable;
import android.text.method.MetaKeyKeyListener;
import android.view.inputmethod.InputConnection;
import android.text.Selection;
import android.view.KeyEvent;
import android.text.Editable;
import java.util.Set;

final class EmojiProcessor
{
    private static final int ACTION_ADVANCE_BOTH = 1;
    private static final int ACTION_ADVANCE_END = 2;
    private static final int ACTION_FLUSH = 3;
    private static final int MAX_LOOK_AROUND_CHARACTER = 16;
    private final int[] mEmojiAsDefaultStyleExceptions;
    private EmojiCompat.GlyphChecker mGlyphChecker;
    private final MetadataRepo mMetadataRepo;
    private final EmojiCompat.SpanFactory mSpanFactory;
    private final boolean mUseEmojiAsDefaultStyle;
    
    EmojiProcessor(final MetadataRepo mMetadataRepo, final EmojiCompat.SpanFactory mSpanFactory, final EmojiCompat.GlyphChecker mGlyphChecker, final boolean mUseEmojiAsDefaultStyle, final int[] mEmojiAsDefaultStyleExceptions, final Set<int[]> set) {
        this.mSpanFactory = mSpanFactory;
        this.mMetadataRepo = mMetadataRepo;
        this.mGlyphChecker = mGlyphChecker;
        this.mUseEmojiAsDefaultStyle = mUseEmojiAsDefaultStyle;
        this.mEmojiAsDefaultStyleExceptions = mEmojiAsDefaultStyleExceptions;
        this.initExclusions(set);
    }
    
    private static boolean delete(final Editable editable, final KeyEvent keyEvent, final boolean b) {
        if (hasModifiers(keyEvent)) {
            return false;
        }
        final int selectionStart = Selection.getSelectionStart((CharSequence)editable);
        final int selectionEnd = Selection.getSelectionEnd((CharSequence)editable);
        if (hasInvalidSelection(selectionStart, selectionEnd)) {
            return false;
        }
        final EmojiSpan[] array = (EmojiSpan[])editable.getSpans(selectionStart, selectionEnd, (Class)EmojiSpan.class);
        if (array != null && array.length > 0) {
            for (final EmojiSpan emojiSpan : array) {
                final int spanStart = editable.getSpanStart((Object)emojiSpan);
                final int spanEnd = editable.getSpanEnd((Object)emojiSpan);
                if ((b && spanStart == selectionStart) || (!b && spanEnd == selectionStart) || (selectionStart > spanStart && selectionStart < spanEnd)) {
                    editable.delete(spanStart, spanEnd);
                    return true;
                }
            }
        }
        return false;
    }
    
    static boolean handleDeleteSurroundingText(final InputConnection inputConnection, final Editable editable, int a, int a2, final boolean b) {
        if (editable != null) {
            if (inputConnection != null) {
                if (a >= 0) {
                    if (a2 >= 0) {
                        final int selectionStart = Selection.getSelectionStart((CharSequence)editable);
                        final int selectionEnd = Selection.getSelectionEnd((CharSequence)editable);
                        if (hasInvalidSelection(selectionStart, selectionEnd)) {
                            return false;
                        }
                        Label_0121: {
                            if (b) {
                                a = CodepointIndexFinder.findIndexBackward((CharSequence)editable, selectionStart, Math.max(a, 0));
                                final int indexForward = CodepointIndexFinder.findIndexForward((CharSequence)editable, selectionEnd, Math.max(a2, 0));
                                if (a != -1) {
                                    a2 = a;
                                    if ((a = indexForward) != -1) {
                                        break Label_0121;
                                    }
                                }
                                return false;
                            }
                            final int max = Math.max(selectionStart - a, 0);
                            a = Math.min(selectionEnd + a2, editable.length());
                            a2 = max;
                        }
                        final EmojiSpan[] array = (EmojiSpan[])editable.getSpans(a2, a, (Class)EmojiSpan.class);
                        if (array != null && array.length > 0) {
                            for (final EmojiSpan emojiSpan : array) {
                                final int spanStart = editable.getSpanStart((Object)emojiSpan);
                                final int spanEnd = editable.getSpanEnd((Object)emojiSpan);
                                a2 = Math.min(spanStart, a2);
                                a = Math.max(spanEnd, a);
                            }
                            a2 = Math.max(a2, 0);
                            a = Math.min(a, editable.length());
                            inputConnection.beginBatchEdit();
                            editable.delete(a2, a);
                            inputConnection.endBatchEdit();
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }
    
    static boolean handleOnKeyDown(final Editable editable, final int n, final KeyEvent keyEvent) {
        boolean delete;
        if (n != 67) {
            delete = (n == 112 && delete(editable, keyEvent, true));
        }
        else {
            delete = delete(editable, keyEvent, false);
        }
        if (delete) {
            MetaKeyKeyListener.adjustMetaAfterKeypress((Spannable)editable);
            return true;
        }
        return false;
    }
    
    private boolean hasGlyph(final CharSequence charSequence, final int n, final int n2, final TypefaceEmojiRasterizer typefaceEmojiRasterizer) {
        if (typefaceEmojiRasterizer.getHasGlyph() == 0) {
            typefaceEmojiRasterizer.setHasGlyph(this.mGlyphChecker.hasGlyph(charSequence, n, n2, typefaceEmojiRasterizer.getSdkAdded()));
        }
        return typefaceEmojiRasterizer.getHasGlyph() == 2;
    }
    
    private static boolean hasInvalidSelection(final int n, final int n2) {
        return n == -1 || n2 == -1 || n != n2;
    }
    
    private static boolean hasModifiers(final KeyEvent keyEvent) {
        return KeyEvent.metaStateHasNoModifiers(keyEvent.getMetaState()) ^ true;
    }
    
    private void initExclusions(final Set<int[]> set) {
        if (set.isEmpty()) {
            return;
        }
        for (final int[] codePoints : set) {
            final String s = new String(codePoints, 0, codePoints.length);
            this.process(s, 0, s.length(), 1, true, (EmojiProcessCallback<Object>)new MarkExclusionCallback(s));
        }
    }
    
    private <T> T process(final CharSequence charSequence, int n, final int n2, final int n3, final boolean b, final EmojiProcessCallback<T> emojiProcessCallback) {
        final ProcessorSm processorSm = new ProcessorSm(this.mMetadataRepo.getRootNode(), this.mUseEmojiAsDefaultStyle, this.mEmojiAsDefaultStyleExceptions);
        int codePoint = Character.codePointAt(charSequence, n);
        int n4 = 0;
        boolean handleEmoji = true;
        int n5 = n;
        int n6 = 0;
        int n7 = 0;
    Label_0040:
        while (true) {
            n6 = n5;
            n7 = n5;
            n = codePoint;
            while (n7 < n2 && n4 < n3 && handleEmoji) {
                final int check = processorSm.check(n);
                if (check != 1) {
                    if (check != 2) {
                        if (check != 3) {
                            continue;
                        }
                        if (!b) {
                            codePoint = n;
                            n5 = n7;
                            if (this.hasGlyph(charSequence, n6, n7, processorSm.getFlushMetadata())) {
                                continue Label_0040;
                            }
                        }
                        handleEmoji = emojiProcessCallback.handleEmoji(charSequence, n6, n7, processorSm.getFlushMetadata());
                        ++n4;
                        codePoint = n;
                        n5 = n7;
                        continue Label_0040;
                    }
                    else {
                        final int index = n7 + Character.charCount(n);
                        if ((n7 = index) >= n2) {
                            continue;
                        }
                        n = Character.codePointAt(charSequence, index);
                        n7 = index;
                    }
                }
                else {
                    n6 += Character.charCount(Character.codePointAt(charSequence, n6));
                    if (n6 < n2) {
                        n = Character.codePointAt(charSequence, n6);
                    }
                    n7 = n6;
                }
            }
            break;
        }
        if (processorSm.isInFlushableState() && n4 < n3 && handleEmoji && (b || !this.hasGlyph(charSequence, n6, n7, processorSm.getCurrentMetadata()))) {
            emojiProcessCallback.handleEmoji(charSequence, n6, n7, processorSm.getCurrentMetadata());
        }
        return (T)emojiProcessCallback.getResult();
    }
    
    int getEmojiEnd(final CharSequence charSequence, final int n) {
        if (n >= 0 && n < charSequence.length()) {
            if (charSequence instanceof Spanned) {
                final Spanned spanned = (Spanned)charSequence;
                final EmojiSpan[] array = (EmojiSpan[])spanned.getSpans(n, n + 1, (Class)EmojiSpan.class);
                if (array.length > 0) {
                    return spanned.getSpanEnd((Object)array[0]);
                }
            }
            return this.process(charSequence, Math.max(0, n - 16), Math.min(charSequence.length(), n + 16), Integer.MAX_VALUE, true, (EmojiProcessCallback<EmojiProcessLookupCallback>)new EmojiProcessLookupCallback(n)).end;
        }
        return -1;
    }
    
    int getEmojiMatch(final CharSequence charSequence) {
        return this.getEmojiMatch(charSequence, this.mMetadataRepo.getMetadataVersion());
    }
    
    int getEmojiMatch(final CharSequence seq, final int n) {
        final ProcessorSm processorSm = new ProcessorSm(this.mMetadataRepo.getRootNode(), this.mUseEmojiAsDefaultStyle, this.mEmojiAsDefaultStyleExceptions);
        final int length = seq.length();
        int i = 0;
        int n2 = 0;
        int n3 = 0;
        while (i < length) {
            final int codePoint = Character.codePointAt(seq, i);
            final int check = processorSm.check(codePoint);
            TypefaceEmojiRasterizer currentMetadata = processorSm.getCurrentMetadata();
            int n4;
            int n5;
            int n6;
            if (check != 1) {
                if (check != 2) {
                    if (check != 3) {
                        n4 = i;
                        n5 = n2;
                        n6 = n3;
                    }
                    else {
                        final TypefaceEmojiRasterizer flushMetadata = processorSm.getFlushMetadata();
                        n4 = i;
                        n5 = n2;
                        n6 = n3;
                        currentMetadata = flushMetadata;
                        if (flushMetadata.getCompatAdded() <= n) {
                            n5 = n2 + 1;
                            n4 = i;
                            n6 = n3;
                            currentMetadata = flushMetadata;
                        }
                    }
                }
                else {
                    n4 = i + Character.charCount(codePoint);
                    n5 = n2;
                    n6 = n3;
                }
            }
            else {
                n4 = i + Character.charCount(codePoint);
                n6 = 0;
                n5 = n2;
            }
            i = n4;
            n2 = n5;
            n3 = n6;
            if (currentMetadata != null) {
                i = n4;
                n2 = n5;
                n3 = n6;
                if (currentMetadata.getCompatAdded() > n) {
                    continue;
                }
                n3 = n6 + 1;
                i = n4;
                n2 = n5;
            }
        }
        if (n2 != 0) {
            return 2;
        }
        if (processorSm.isInFlushableState() && processorSm.getCurrentMetadata().getCompatAdded() <= n) {
            return 1;
        }
        if (n3 == 0) {
            return 0;
        }
        return 2;
    }
    
    int getEmojiStart(final CharSequence charSequence, final int n) {
        if (n >= 0 && n < charSequence.length()) {
            if (charSequence instanceof Spanned) {
                final Spanned spanned = (Spanned)charSequence;
                final EmojiSpan[] array = (EmojiSpan[])spanned.getSpans(n, n + 1, (Class)EmojiSpan.class);
                if (array.length > 0) {
                    return spanned.getSpanStart((Object)array[0]);
                }
            }
            return this.process(charSequence, Math.max(0, n - 16), Math.min(charSequence.length(), n + 16), Integer.MAX_VALUE, true, (EmojiProcessCallback<EmojiProcessLookupCallback>)new EmojiProcessLookupCallback(n)).start;
        }
        return -1;
    }
    
    CharSequence process(final CharSequence charSequence, int min, int max, final int n, final boolean b) {
        final boolean b2 = charSequence instanceof SpannableBuilder;
        if (b2) {
            ((SpannableBuilder)charSequence).beginBatchEdit();
        }
        final UnprecomputeTextOnModificationSpannable unprecomputeTextOnModificationSpannable = null;
        Label_0085: {
            if (b2) {
                break Label_0085;
            }
            try {
                UnprecomputeTextOnModificationSpannable unprecomputeTextOnModificationSpannable2;
                if (charSequence instanceof Spannable) {
                    unprecomputeTextOnModificationSpannable2 = new UnprecomputeTextOnModificationSpannable((Spannable)charSequence);
                }
                else {
                    unprecomputeTextOnModificationSpannable2 = unprecomputeTextOnModificationSpannable;
                    if (charSequence instanceof Spanned) {
                        unprecomputeTextOnModificationSpannable2 = unprecomputeTextOnModificationSpannable;
                        if (((Spanned)charSequence).nextSpanTransition(min - 1, max + 1, (Class)EmojiSpan.class) <= max) {
                            unprecomputeTextOnModificationSpannable2 = new UnprecomputeTextOnModificationSpannable(charSequence);
                        }
                    }
                }
                int n2 = min;
                int n3 = max;
                if (unprecomputeTextOnModificationSpannable2 != null) {
                    final EmojiSpan[] array = unprecomputeTextOnModificationSpannable2.getSpans(min, max, EmojiSpan.class);
                    n2 = min;
                    n3 = max;
                    if (array != null) {
                        n2 = min;
                        n3 = max;
                        if (array.length > 0) {
                            final int length = array.length;
                            int n4 = 0;
                            while (true) {
                                n2 = min;
                                n3 = max;
                                if (n4 >= length) {
                                    break;
                                }
                                final EmojiSpan emojiSpan = array[n4];
                                final int spanStart = unprecomputeTextOnModificationSpannable2.getSpanStart(emojiSpan);
                                final int spanEnd = unprecomputeTextOnModificationSpannable2.getSpanEnd(emojiSpan);
                                if (spanStart != max) {
                                    unprecomputeTextOnModificationSpannable2.removeSpan(emojiSpan);
                                }
                                min = Math.min(spanStart, min);
                                max = Math.max(spanEnd, max);
                                ++n4;
                            }
                        }
                    }
                }
                if (n2 == n3 || n2 >= charSequence.length()) {
                    return charSequence;
                }
                if ((min = n) != Integer.MAX_VALUE) {
                    min = n;
                    if (unprecomputeTextOnModificationSpannable2 != null) {
                        min = n - unprecomputeTextOnModificationSpannable2.getSpans(0, unprecomputeTextOnModificationSpannable2.length(), EmojiSpan.class).length;
                    }
                }
                final UnprecomputeTextOnModificationSpannable unprecomputeTextOnModificationSpannable3 = this.process(charSequence, n2, n3, min, b, (EmojiProcessCallback<UnprecomputeTextOnModificationSpannable>)new EmojiProcessAddSpanCallback(unprecomputeTextOnModificationSpannable2, this.mSpanFactory));
                if (unprecomputeTextOnModificationSpannable3 != null) {
                    return (CharSequence)unprecomputeTextOnModificationSpannable3.getUnwrappedSpannable();
                }
                return charSequence;
            }
            finally {
                if (b2) {
                    ((SpannableBuilder)charSequence).endBatchEdit();
                }
            }
        }
    }
    
    private static final class CodepointIndexFinder
    {
        private static final int INVALID_INDEX = -1;
        
        static int findIndexBackward(final CharSequence charSequence, int n, int i) {
            final int length = charSequence.length();
            if (n < 0 || length < n) {
                return -1;
            }
            if (i < 0) {
                return -1;
            }
        Label_0027:
            while (true) {
                int n2 = 0;
                while (i != 0) {
                    if (--n < 0) {
                        if (n2 != 0) {
                            return -1;
                        }
                        return 0;
                    }
                    else {
                        final char char1 = charSequence.charAt(n);
                        if (n2 != 0) {
                            if (!Character.isHighSurrogate(char1)) {
                                return -1;
                            }
                            --i;
                            continue Label_0027;
                        }
                        else if (!Character.isSurrogate(char1)) {
                            --i;
                        }
                        else {
                            if (Character.isHighSurrogate(char1)) {
                                return -1;
                            }
                            n2 = 1;
                        }
                    }
                }
                return n;
            }
        }
        
        static int findIndexForward(final CharSequence charSequence, int n, int i) {
            final int length = charSequence.length();
            if (n < 0 || length < n) {
                return -1;
            }
            if (i < 0) {
                return -1;
            }
        Label_0027:
            while (true) {
                int n2 = 0;
                while (i != 0) {
                    if (n >= length) {
                        if (n2 != 0) {
                            return -1;
                        }
                        return length;
                    }
                    else {
                        final char char1 = charSequence.charAt(n);
                        if (n2 != 0) {
                            if (!Character.isLowSurrogate(char1)) {
                                return -1;
                            }
                            --i;
                            ++n;
                            continue Label_0027;
                        }
                        else if (!Character.isSurrogate(char1)) {
                            --i;
                            ++n;
                        }
                        else {
                            if (Character.isLowSurrogate(char1)) {
                                return -1;
                            }
                            ++n;
                            n2 = 1;
                        }
                    }
                }
                return n;
            }
        }
    }
    
    private static class EmojiProcessAddSpanCallback implements EmojiProcessCallback<UnprecomputeTextOnModificationSpannable>
    {
        private final EmojiCompat.SpanFactory mSpanFactory;
        public UnprecomputeTextOnModificationSpannable spannable;
        
        EmojiProcessAddSpanCallback(final UnprecomputeTextOnModificationSpannable spannable, final EmojiCompat.SpanFactory mSpanFactory) {
            this.spannable = spannable;
            this.mSpanFactory = mSpanFactory;
        }
        
        public UnprecomputeTextOnModificationSpannable getResult() {
            return this.spannable;
        }
        
        @Override
        public boolean handleEmoji(final CharSequence charSequence, final int n, final int n2, final TypefaceEmojiRasterizer typefaceEmojiRasterizer) {
            if (typefaceEmojiRasterizer.isPreferredSystemRender()) {
                return true;
            }
            if (this.spannable == null) {
                Object o;
                if (charSequence instanceof Spannable) {
                    o = charSequence;
                }
                else {
                    o = new SpannableString(charSequence);
                }
                this.spannable = new UnprecomputeTextOnModificationSpannable((Spannable)o);
            }
            this.spannable.setSpan(this.mSpanFactory.createSpan(typefaceEmojiRasterizer), n, n2, 33);
            return true;
        }
    }
    
    private interface EmojiProcessCallback<T>
    {
        T getResult();
        
        boolean handleEmoji(final CharSequence p0, final int p1, final int p2, final TypefaceEmojiRasterizer p3);
    }
    
    private static class EmojiProcessLookupCallback implements EmojiProcessCallback<EmojiProcessLookupCallback>
    {
        public int end;
        private final int mOffset;
        public int start;
        
        EmojiProcessLookupCallback(final int mOffset) {
            this.start = -1;
            this.end = -1;
            this.mOffset = mOffset;
        }
        
        public EmojiProcessLookupCallback getResult() {
            return this;
        }
        
        @Override
        public boolean handleEmoji(final CharSequence charSequence, final int start, final int end, final TypefaceEmojiRasterizer typefaceEmojiRasterizer) {
            final int mOffset = this.mOffset;
            boolean b = false;
            if (start <= mOffset && mOffset < end) {
                this.start = start;
                this.end = end;
                return false;
            }
            if (end <= mOffset) {
                b = true;
            }
            return b;
        }
    }
    
    private static class MarkExclusionCallback implements EmojiProcessCallback<MarkExclusionCallback>
    {
        private final String mExclusion;
        
        MarkExclusionCallback(final String mExclusion) {
            this.mExclusion = mExclusion;
        }
        
        public MarkExclusionCallback getResult() {
            return this;
        }
        
        @Override
        public boolean handleEmoji(final CharSequence charSequence, final int n, final int n2, final TypefaceEmojiRasterizer typefaceEmojiRasterizer) {
            if (TextUtils.equals(charSequence.subSequence(n, n2), (CharSequence)this.mExclusion)) {
                typefaceEmojiRasterizer.setExclusion(true);
                return false;
            }
            return true;
        }
    }
    
    static final class ProcessorSm
    {
        private static final int STATE_DEFAULT = 1;
        private static final int STATE_WALKING = 2;
        private int mCurrentDepth;
        private MetadataRepo.Node mCurrentNode;
        private final int[] mEmojiAsDefaultStyleExceptions;
        private MetadataRepo.Node mFlushNode;
        private int mLastCodepoint;
        private final MetadataRepo.Node mRootNode;
        private int mState;
        private final boolean mUseEmojiAsDefaultStyle;
        
        ProcessorSm(final MetadataRepo.Node node, final boolean mUseEmojiAsDefaultStyle, final int[] mEmojiAsDefaultStyleExceptions) {
            this.mState = 1;
            this.mRootNode = node;
            this.mCurrentNode = node;
            this.mUseEmojiAsDefaultStyle = mUseEmojiAsDefaultStyle;
            this.mEmojiAsDefaultStyleExceptions = mEmojiAsDefaultStyleExceptions;
        }
        
        private static boolean isEmojiStyle(final int n) {
            return n == 65039;
        }
        
        private static boolean isTextStyle(final int n) {
            return n == 65038;
        }
        
        private int reset() {
            this.mState = 1;
            this.mCurrentNode = this.mRootNode;
            this.mCurrentDepth = 0;
            return 1;
        }
        
        private boolean shouldUseEmojiPresentationStyleForSingleCodepoint() {
            if (this.mCurrentNode.getData().isDefaultEmoji()) {
                return true;
            }
            if (isEmojiStyle(this.mLastCodepoint)) {
                return true;
            }
            if (this.mUseEmojiAsDefaultStyle) {
                if (this.mEmojiAsDefaultStyleExceptions == null) {
                    return true;
                }
                if (Arrays.binarySearch(this.mEmojiAsDefaultStyleExceptions, this.mCurrentNode.getData().getCodepointAt(0)) < 0) {
                    return true;
                }
            }
            return false;
        }
        
        int check(final int mLastCodepoint) {
            final MetadataRepo.Node value = this.mCurrentNode.get(mLastCodepoint);
            final int mState = this.mState;
            int n = 3;
            Label_0175: {
                if (mState != 2) {
                    if (value == null) {
                        n = this.reset();
                        break Label_0175;
                    }
                    this.mState = 2;
                    this.mCurrentNode = value;
                    this.mCurrentDepth = 1;
                }
                else if (value != null) {
                    this.mCurrentNode = value;
                    ++this.mCurrentDepth;
                }
                else {
                    if (isTextStyle(mLastCodepoint)) {
                        n = this.reset();
                        break Label_0175;
                    }
                    if (!isEmojiStyle(mLastCodepoint)) {
                        if (this.mCurrentNode.getData() == null) {
                            n = this.reset();
                            break Label_0175;
                        }
                        if (this.mCurrentDepth != 1) {
                            this.mFlushNode = this.mCurrentNode;
                            this.reset();
                            break Label_0175;
                        }
                        if (this.shouldUseEmojiPresentationStyleForSingleCodepoint()) {
                            this.mFlushNode = this.mCurrentNode;
                            this.reset();
                            break Label_0175;
                        }
                        n = this.reset();
                        break Label_0175;
                    }
                }
                n = 2;
            }
            this.mLastCodepoint = mLastCodepoint;
            return n;
        }
        
        TypefaceEmojiRasterizer getCurrentMetadata() {
            return this.mCurrentNode.getData();
        }
        
        TypefaceEmojiRasterizer getFlushMetadata() {
            return this.mFlushNode.getData();
        }
        
        boolean isInFlushableState() {
            final int mState = this.mState;
            final boolean b = true;
            if (mState == 2 && this.mCurrentNode.getData() != null) {
                boolean b2 = b;
                if (this.mCurrentDepth > 1) {
                    return b2;
                }
                if (this.shouldUseEmojiPresentationStyleForSingleCodepoint()) {
                    b2 = b;
                    return b2;
                }
            }
            return false;
        }
    }
}

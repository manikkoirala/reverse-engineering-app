// 
// Decompiled by Procyon v0.6.0
// 

package androidx.emoji2.text;

import android.graphics.Paint;
import androidx.core.util.Preconditions;
import android.graphics.Paint$FontMetricsInt;
import android.text.style.ReplacementSpan;

public abstract class EmojiSpan extends ReplacementSpan
{
    private short mHeight;
    private final TypefaceEmojiRasterizer mRasterizer;
    private float mRatio;
    private final Paint$FontMetricsInt mTmpFontMetrics;
    private short mWidth;
    
    EmojiSpan(final TypefaceEmojiRasterizer mRasterizer) {
        this.mTmpFontMetrics = new Paint$FontMetricsInt();
        this.mWidth = -1;
        this.mHeight = -1;
        this.mRatio = 1.0f;
        Preconditions.checkNotNull(mRasterizer, "rasterizer cannot be null");
        this.mRasterizer = mRasterizer;
    }
    
    public final int getHeight() {
        return this.mHeight;
    }
    
    public final int getId() {
        return this.getTypefaceRasterizer().getId();
    }
    
    final float getRatio() {
        return this.mRatio;
    }
    
    public int getSize(final Paint paint, final CharSequence charSequence, final int n, final int n2, final Paint$FontMetricsInt paint$FontMetricsInt) {
        paint.getFontMetricsInt(this.mTmpFontMetrics);
        this.mRatio = Math.abs(this.mTmpFontMetrics.descent - this.mTmpFontMetrics.ascent) * 1.0f / this.mRasterizer.getHeight();
        this.mHeight = (short)(this.mRasterizer.getHeight() * this.mRatio);
        this.mWidth = (short)(this.mRasterizer.getWidth() * this.mRatio);
        if (paint$FontMetricsInt != null) {
            paint$FontMetricsInt.ascent = this.mTmpFontMetrics.ascent;
            paint$FontMetricsInt.descent = this.mTmpFontMetrics.descent;
            paint$FontMetricsInt.top = this.mTmpFontMetrics.top;
            paint$FontMetricsInt.bottom = this.mTmpFontMetrics.bottom;
        }
        return this.mWidth;
    }
    
    public final TypefaceEmojiRasterizer getTypefaceRasterizer() {
        return this.mRasterizer;
    }
    
    final int getWidth() {
        return this.mWidth;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.emoji2.text;

import android.text.PrecomputedText;
import androidx.core.text.PrecomputedTextCompat;
import java.util.stream.IntStream;
import android.os.Build$VERSION;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.Spannable;

class UnprecomputeTextOnModificationSpannable implements Spannable
{
    private Spannable mDelegate;
    private boolean mSafeToWrite;
    
    UnprecomputeTextOnModificationSpannable(final Spannable mDelegate) {
        this.mSafeToWrite = false;
        this.mDelegate = mDelegate;
    }
    
    UnprecomputeTextOnModificationSpannable(final Spanned spanned) {
        this.mSafeToWrite = false;
        this.mDelegate = (Spannable)new SpannableString((CharSequence)spanned);
    }
    
    UnprecomputeTextOnModificationSpannable(final CharSequence charSequence) {
        this.mSafeToWrite = false;
        this.mDelegate = (Spannable)new SpannableString(charSequence);
    }
    
    private void ensureSafeWrites() {
        final Spannable mDelegate = this.mDelegate;
        if (!this.mSafeToWrite && precomputedTextDetector().isPrecomputedText((CharSequence)mDelegate)) {
            this.mDelegate = (Spannable)new SpannableString((CharSequence)mDelegate);
        }
        this.mSafeToWrite = true;
    }
    
    static PrecomputedTextDetector precomputedTextDetector() {
        Object o;
        if (Build$VERSION.SDK_INT < 28) {
            o = new PrecomputedTextDetector();
        }
        else {
            o = new PrecomputedTextDetector_28();
        }
        return (PrecomputedTextDetector)o;
    }
    
    public char charAt(final int n) {
        return this.mDelegate.charAt(n);
    }
    
    public IntStream chars() {
        return CharSequenceHelper_API24.chars((CharSequence)this.mDelegate);
    }
    
    public IntStream codePoints() {
        return CharSequenceHelper_API24.codePoints((CharSequence)this.mDelegate);
    }
    
    public int getSpanEnd(final Object o) {
        return this.mDelegate.getSpanEnd(o);
    }
    
    public int getSpanFlags(final Object o) {
        return this.mDelegate.getSpanFlags(o);
    }
    
    public int getSpanStart(final Object o) {
        return this.mDelegate.getSpanStart(o);
    }
    
    public <T> T[] getSpans(final int n, final int n2, final Class<T> clazz) {
        return (T[])this.mDelegate.getSpans(n, n2, (Class)clazz);
    }
    
    Spannable getUnwrappedSpannable() {
        return this.mDelegate;
    }
    
    public int length() {
        return this.mDelegate.length();
    }
    
    public int nextSpanTransition(final int n, final int n2, final Class clazz) {
        return this.mDelegate.nextSpanTransition(n, n2, clazz);
    }
    
    public void removeSpan(final Object o) {
        this.ensureSafeWrites();
        this.mDelegate.removeSpan(o);
    }
    
    public void setSpan(final Object o, final int n, final int n2, final int n3) {
        this.ensureSafeWrites();
        this.mDelegate.setSpan(o, n, n2, n3);
    }
    
    public CharSequence subSequence(final int n, final int n2) {
        return this.mDelegate.subSequence(n, n2);
    }
    
    @Override
    public String toString() {
        return this.mDelegate.toString();
    }
    
    private static class CharSequenceHelper_API24
    {
        static IntStream chars(final CharSequence charSequence) {
            return charSequence.chars();
        }
        
        static IntStream codePoints(final CharSequence charSequence) {
            return charSequence.codePoints();
        }
    }
    
    static class PrecomputedTextDetector
    {
        boolean isPrecomputedText(final CharSequence charSequence) {
            return charSequence instanceof PrecomputedTextCompat;
        }
    }
    
    static class PrecomputedTextDetector_28 extends PrecomputedTextDetector
    {
        @Override
        boolean isPrecomputedText(final CharSequence charSequence) {
            return charSequence instanceof PrecomputedText || charSequence instanceof PrecomputedTextCompat;
        }
    }
}

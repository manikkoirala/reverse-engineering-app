// 
// Decompiled by Procyon v0.6.0
// 

package androidx.emoji2.text;

import androidx.core.os.TraceCompat;
import java.util.concurrent.Executor;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.Collections;
import java.util.List;
import androidx.lifecycle.LifecycleObserver;
import androidx.lifecycle.DefaultLifecycleObserver$_CC;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.DefaultLifecycleObserver;
import androidx.lifecycle.ProcessLifecycleInitializer;
import androidx.startup.AppInitializer;
import androidx.lifecycle.LifecycleOwner;
import android.os.Build$VERSION;
import android.content.Context;
import androidx.startup.Initializer;

public class EmojiCompatInitializer implements Initializer<Boolean>
{
    private static final long STARTUP_THREAD_CREATION_DELAY_MS = 500L;
    private static final String S_INITIALIZER_THREAD_NAME = "EmojiCompatInitializer";
    
    @Override
    public Boolean create(final Context context) {
        if (Build$VERSION.SDK_INT >= 19) {
            EmojiCompat.init((EmojiCompat.Config)new BackgroundDefaultConfig(context));
            this.delayUntilFirstResume(context);
            return true;
        }
        return false;
    }
    
    void delayUntilFirstResume(final Context context) {
        final Lifecycle lifecycle = AppInitializer.getInstance(context).initializeComponent((Class<? extends Initializer<LifecycleOwner>>)ProcessLifecycleInitializer.class).getLifecycle();
        lifecycle.addObserver(new DefaultLifecycleObserver(this, lifecycle) {
            final EmojiCompatInitializer this$0;
            final Lifecycle val$lifecycle;
            
            @Override
            public void onResume(final LifecycleOwner lifecycleOwner) {
                this.this$0.loadEmojiCompatAfterDelay();
                this.val$lifecycle.removeObserver(this);
            }
        });
    }
    
    @Override
    public List<Class<? extends Initializer<?>>> dependencies() {
        return (List<Class<? extends Initializer<?>>>)Collections.singletonList(ProcessLifecycleInitializer.class);
    }
    
    void loadEmojiCompatAfterDelay() {
        ConcurrencyHelpers.mainHandlerAsync().postDelayed((Runnable)new LoadEmojiCompatRunnable(), 500L);
    }
    
    static class BackgroundDefaultConfig extends Config
    {
        protected BackgroundDefaultConfig(final Context context) {
            super(new BackgroundDefaultLoader(context));
            ((EmojiCompat.Config)this).setMetadataLoadStrategy(1);
        }
    }
    
    static class BackgroundDefaultLoader implements MetadataRepoLoader
    {
        private final Context mContext;
        
        BackgroundDefaultLoader(final Context context) {
            this.mContext = context.getApplicationContext();
        }
        
        void doLoad(final MetadataRepoLoaderCallback metadataRepoLoaderCallback, final ThreadPoolExecutor loadingExecutor) {
            try {
                final FontRequestEmojiCompatConfig create = DefaultEmojiCompatConfig.create(this.mContext);
                if (create == null) {
                    throw new RuntimeException("EmojiCompat font provider not available on this device.");
                }
                create.setLoadingExecutor(loadingExecutor);
                ((EmojiCompat.Config)create).getMetadataRepoLoader().load(new MetadataRepoLoaderCallback(this, metadataRepoLoaderCallback, loadingExecutor) {
                    final BackgroundDefaultLoader this$0;
                    final ThreadPoolExecutor val$executor;
                    final MetadataRepoLoaderCallback val$loaderCallback;
                    
                    @Override
                    public void onFailed(final Throwable t) {
                        try {
                            this.val$loaderCallback.onFailed(t);
                        }
                        finally {
                            this.val$executor.shutdown();
                        }
                    }
                    
                    @Override
                    public void onLoaded(final MetadataRepo metadataRepo) {
                        try {
                            this.val$loaderCallback.onLoaded(metadataRepo);
                        }
                        finally {
                            this.val$executor.shutdown();
                        }
                    }
                });
            }
            finally {
                final Throwable t;
                metadataRepoLoaderCallback.onFailed(t);
                loadingExecutor.shutdown();
            }
        }
        
        @Override
        public void load(final MetadataRepoLoaderCallback metadataRepoLoaderCallback) {
            final ThreadPoolExecutor backgroundPriorityExecutor = ConcurrencyHelpers.createBackgroundPriorityExecutor("EmojiCompatInitializer");
            backgroundPriorityExecutor.execute(new EmojiCompatInitializer$BackgroundDefaultLoader$$ExternalSyntheticLambda0(this, metadataRepoLoaderCallback, backgroundPriorityExecutor));
        }
    }
    
    static class LoadEmojiCompatRunnable implements Runnable
    {
        @Override
        public void run() {
            try {
                TraceCompat.beginSection("EmojiCompat.EmojiCompatInitializer.run");
                if (EmojiCompat.isConfigured()) {
                    EmojiCompat.get().load();
                }
            }
            finally {
                TraceCompat.endSection();
            }
        }
    }
}

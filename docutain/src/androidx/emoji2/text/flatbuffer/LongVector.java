// 
// Decompiled by Procyon v0.6.0
// 

package androidx.emoji2.text.flatbuffer;

import java.nio.ByteBuffer;

public final class LongVector extends BaseVector
{
    public LongVector __assign(final int n, final ByteBuffer byteBuffer) {
        this.__reset(n, 8, byteBuffer);
        return this;
    }
    
    public long get(final int n) {
        return this.bb.getLong(this.__element(n));
    }
}

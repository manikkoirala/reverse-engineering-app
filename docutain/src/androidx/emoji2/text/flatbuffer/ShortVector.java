// 
// Decompiled by Procyon v0.6.0
// 

package androidx.emoji2.text.flatbuffer;

import java.nio.ByteBuffer;

public final class ShortVector extends BaseVector
{
    public ShortVector __assign(final int n, final ByteBuffer byteBuffer) {
        this.__reset(n, 2, byteBuffer);
        return this;
    }
    
    public short get(final int n) {
        return this.bb.getShort(this.__element(n));
    }
    
    public int getAsUnsigned(final int n) {
        return this.get(n) & 0xFFFF;
    }
}

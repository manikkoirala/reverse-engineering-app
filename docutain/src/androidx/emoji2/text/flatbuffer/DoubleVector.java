// 
// Decompiled by Procyon v0.6.0
// 

package androidx.emoji2.text.flatbuffer;

import java.nio.ByteBuffer;

public final class DoubleVector extends BaseVector
{
    public DoubleVector __assign(final int n, final ByteBuffer byteBuffer) {
        this.__reset(n, 8, byteBuffer);
        return this;
    }
    
    public double get(final int n) {
        return this.bb.getDouble(this.__element(n));
    }
}

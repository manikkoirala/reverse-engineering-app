// 
// Decompiled by Procyon v0.6.0
// 

package androidx.emoji2.text.flatbuffer;

import java.nio.ByteBuffer;

public abstract class Utf8
{
    private static Utf8 DEFAULT;
    
    public static Utf8 getDefault() {
        if (Utf8.DEFAULT == null) {
            Utf8.DEFAULT = new Utf8Safe();
        }
        return Utf8.DEFAULT;
    }
    
    public static void setDefault(final Utf8 default1) {
        Utf8.DEFAULT = default1;
    }
    
    public abstract String decodeUtf8(final ByteBuffer p0, final int p1, final int p2);
    
    public abstract void encodeUtf8(final CharSequence p0, final ByteBuffer p1);
    
    public abstract int encodedLength(final CharSequence p0);
    
    static class DecodeUtil
    {
        static void handleFourBytes(final byte b, final byte b2, final byte b3, final byte b4, final char[] array, final int n) throws IllegalArgumentException {
            if (!isNotTrailingByte(b2) && (b << 28) + (b2 + 112) >> 30 == 0 && !isNotTrailingByte(b3) && !isNotTrailingByte(b4)) {
                final int n2 = (b & 0x7) << 18 | trailingByteValue(b2) << 12 | trailingByteValue(b3) << 6 | trailingByteValue(b4);
                array[n] = highSurrogate(n2);
                array[n + 1] = lowSurrogate(n2);
                return;
            }
            throw new IllegalArgumentException("Invalid UTF-8");
        }
        
        static void handleOneByte(final byte b, final char[] array, final int n) {
            array[n] = (char)b;
        }
        
        static void handleThreeBytes(final byte b, final byte b2, final byte b3, final char[] array, final int n) throws IllegalArgumentException {
            if (!isNotTrailingByte(b2) && (b != -32 || b2 >= -96) && (b != -19 || b2 < -96) && !isNotTrailingByte(b3)) {
                array[n] = (char)((b & 0xF) << 12 | trailingByteValue(b2) << 6 | trailingByteValue(b3));
                return;
            }
            throw new IllegalArgumentException("Invalid UTF-8");
        }
        
        static void handleTwoBytes(final byte b, final byte b2, final char[] array, final int n) throws IllegalArgumentException {
            if (b < -62) {
                throw new IllegalArgumentException("Invalid UTF-8: Illegal leading byte in 2 bytes utf");
            }
            if (!isNotTrailingByte(b2)) {
                array[n] = (char)((b & 0x1F) << 6 | trailingByteValue(b2));
                return;
            }
            throw new IllegalArgumentException("Invalid UTF-8: Illegal trailing byte in 2 bytes utf");
        }
        
        private static char highSurrogate(final int n) {
            return (char)((n >>> 10) + 55232);
        }
        
        private static boolean isNotTrailingByte(final byte b) {
            return b > -65;
        }
        
        static boolean isOneByte(final byte b) {
            return b >= 0;
        }
        
        static boolean isThreeBytes(final byte b) {
            return b < -16;
        }
        
        static boolean isTwoBytes(final byte b) {
            return b < -32;
        }
        
        private static char lowSurrogate(final int n) {
            return (char)((n & 0x3FF) + 56320);
        }
        
        private static int trailingByteValue(final byte b) {
            return b & 0x3F;
        }
    }
    
    static class UnpairedSurrogateException extends IllegalArgumentException
    {
        UnpairedSurrogateException(final int i, final int j) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Unpaired surrogate at index ");
            sb.append(i);
            sb.append(" of ");
            sb.append(j);
            super(sb.toString());
        }
    }
}

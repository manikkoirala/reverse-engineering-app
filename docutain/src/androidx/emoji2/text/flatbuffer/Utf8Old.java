// 
// Decompiled by Procyon v0.6.0
// 

package androidx.emoji2.text.flatbuffer;

import java.nio.charset.StandardCharsets;
import java.nio.charset.CharsetEncoder;
import java.nio.charset.CoderResult;
import java.nio.CharBuffer;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.CharacterCodingException;
import java.nio.ByteBuffer;
import java.util.function.Supplier;

public class Utf8Old extends Utf8
{
    private static final ThreadLocal<Cache> CACHE;
    
    static {
        CACHE = ThreadLocal.withInitial((Supplier<? extends Cache>)new Utf8Old$$ExternalSyntheticLambda0());
    }
    
    @Override
    public String decodeUtf8(ByteBuffer duplicate, final int n, final int n2) {
        final CharsetDecoder decoder = Utf8Old.CACHE.get().decoder;
        decoder.reset();
        duplicate = duplicate.duplicate();
        duplicate.position(n);
        duplicate.limit(n + n2);
        try {
            return decoder.decode(duplicate).toString();
        }
        catch (final CharacterCodingException cause) {
            throw new IllegalArgumentException("Bad encoding", cause);
        }
    }
    
    @Override
    public void encodeUtf8(final CharSequence charSequence, final ByteBuffer byteBuffer) {
        final Cache cache = Utf8Old.CACHE.get();
        if (cache.lastInput != charSequence) {
            this.encodedLength(charSequence);
        }
        byteBuffer.put(cache.lastOutput);
    }
    
    @Override
    public int encodedLength(final CharSequence charSequence) {
        final Cache cache = Utf8Old.CACHE.get();
        final int b = (int)(charSequence.length() * cache.encoder.maxBytesPerChar());
        if (cache.lastOutput == null || cache.lastOutput.capacity() < b) {
            cache.lastOutput = ByteBuffer.allocate(Math.max(128, b));
        }
        cache.lastOutput.clear();
        cache.lastInput = charSequence;
        CharBuffer wrap;
        if (charSequence instanceof CharBuffer) {
            wrap = (CharBuffer)charSequence;
        }
        else {
            wrap = CharBuffer.wrap(charSequence);
        }
        final CoderResult encode = cache.encoder.encode(wrap, cache.lastOutput, true);
        if (encode.isError()) {
            try {
                encode.throwException();
            }
            catch (final CharacterCodingException cause) {
                throw new IllegalArgumentException("bad character encoding", cause);
            }
        }
        cache.lastOutput.flip();
        return cache.lastOutput.remaining();
    }
    
    private static class Cache
    {
        final CharsetDecoder decoder;
        final CharsetEncoder encoder;
        CharSequence lastInput;
        ByteBuffer lastOutput;
        
        Cache() {
            this.lastInput = null;
            this.lastOutput = null;
            this.encoder = StandardCharsets.UTF_8.newEncoder();
            this.decoder = StandardCharsets.UTF_8.newDecoder();
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.emoji2.text.flatbuffer;

import java.util.Arrays;

public class ArrayReadWriteBuf implements ReadWriteBuf
{
    private byte[] buffer;
    private int writePos;
    
    public ArrayReadWriteBuf() {
        this(10);
    }
    
    public ArrayReadWriteBuf(final int n) {
        this(new byte[n]);
    }
    
    public ArrayReadWriteBuf(final byte[] buffer) {
        this.buffer = buffer;
        this.writePos = 0;
    }
    
    public ArrayReadWriteBuf(final byte[] buffer, final int writePos) {
        this.buffer = buffer;
        this.writePos = writePos;
    }
    
    @Override
    public byte[] data() {
        return this.buffer;
    }
    
    @Override
    public byte get(final int n) {
        return this.buffer[n];
    }
    
    @Override
    public boolean getBoolean(final int n) {
        return this.buffer[n] != 0;
    }
    
    @Override
    public double getDouble(final int n) {
        return Double.longBitsToDouble(this.getLong(n));
    }
    
    @Override
    public float getFloat(final int n) {
        return Float.intBitsToFloat(this.getInt(n));
    }
    
    @Override
    public int getInt(final int n) {
        final byte[] buffer = this.buffer;
        return (buffer[n] & 0xFF) | (buffer[n + 3] << 24 | (buffer[n + 2] & 0xFF) << 16 | (buffer[n + 1] & 0xFF) << 8);
    }
    
    @Override
    public long getLong(int n) {
        final byte[] buffer = this.buffer;
        final int n2 = n + 1;
        final long n3 = buffer[n];
        final int n4 = n2 + 1;
        final long n5 = buffer[n2];
        n = n4 + 1;
        final long n6 = buffer[n4];
        final int n7 = n + 1;
        final long n8 = buffer[n];
        n = n7 + 1;
        final long n9 = buffer[n7];
        final int n10 = n + 1;
        return (n3 & 0xFFL) | (n5 & 0xFFL) << 8 | (n6 & 0xFFL) << 16 | (n8 & 0xFFL) << 24 | (n9 & 0xFFL) << 32 | ((long)buffer[n] & 0xFFL) << 40 | (0xFFL & (long)buffer[n10]) << 48 | (long)buffer[n10 + 1] << 56;
    }
    
    @Override
    public short getShort(final int n) {
        final byte[] buffer = this.buffer;
        return (short)((buffer[n] & 0xFF) | buffer[n + 1] << 8);
    }
    
    @Override
    public String getString(final int n, final int n2) {
        return Utf8Safe.decodeUtf8Array(this.buffer, n, n2);
    }
    
    @Override
    public int limit() {
        return this.writePos;
    }
    
    @Override
    public void put(final byte b) {
        this.set(this.writePos, b);
        ++this.writePos;
    }
    
    @Override
    public void put(final byte[] array, final int n, final int n2) {
        this.set(this.writePos, array, n, n2);
        this.writePos += n2;
    }
    
    @Override
    public void putBoolean(final boolean b) {
        this.setBoolean(this.writePos, b);
        ++this.writePos;
    }
    
    @Override
    public void putDouble(final double n) {
        this.setDouble(this.writePos, n);
        this.writePos += 8;
    }
    
    @Override
    public void putFloat(final float n) {
        this.setFloat(this.writePos, n);
        this.writePos += 4;
    }
    
    @Override
    public void putInt(final int n) {
        this.setInt(this.writePos, n);
        this.writePos += 4;
    }
    
    @Override
    public void putLong(final long n) {
        this.setLong(this.writePos, n);
        this.writePos += 8;
    }
    
    @Override
    public void putShort(final short n) {
        this.setShort(this.writePos, n);
        this.writePos += 2;
    }
    
    @Override
    public boolean requestCapacity(int length) {
        final byte[] buffer = this.buffer;
        if (buffer.length > length) {
            return true;
        }
        length = buffer.length;
        this.buffer = Arrays.copyOf(buffer, length + (length >> 1));
        return true;
    }
    
    @Override
    public void set(final int n, final byte b) {
        this.requestCapacity(n + 1);
        this.buffer[n] = b;
    }
    
    @Override
    public void set(final int n, final byte[] array, final int n2, final int n3) {
        this.requestCapacity(n3 - n2 + n);
        System.arraycopy(array, n2, this.buffer, n, n3);
    }
    
    @Override
    public void setBoolean(final int n, final boolean b) {
        this.set(n, (byte)(b ? 1 : 0));
    }
    
    @Override
    public void setDouble(int n, final double n2) {
        this.requestCapacity(n + 8);
        final long doubleToRawLongBits = Double.doubleToRawLongBits(n2);
        final int n3 = (int)doubleToRawLongBits;
        final byte[] buffer = this.buffer;
        final int n4 = n + 1;
        buffer[n] = (byte)(n3 & 0xFF);
        n = n4 + 1;
        buffer[n4] = (byte)(n3 >> 8 & 0xFF);
        final int n5 = n + 1;
        buffer[n] = (byte)(n3 >> 16 & 0xFF);
        n = n5 + 1;
        buffer[n5] = (byte)(n3 >> 24 & 0xFF);
        final int n6 = (int)(doubleToRawLongBits >> 32);
        final int n7 = n + 1;
        buffer[n] = (byte)(n6 & 0xFF);
        n = n7 + 1;
        buffer[n7] = (byte)(n6 >> 8 & 0xFF);
        buffer[n] = (byte)(n6 >> 16 & 0xFF);
        buffer[n + 1] = (byte)(n6 >> 24 & 0xFF);
    }
    
    @Override
    public void setFloat(int n, final float n2) {
        this.requestCapacity(n + 4);
        final int floatToRawIntBits = Float.floatToRawIntBits(n2);
        final byte[] buffer = this.buffer;
        final int n3 = n + 1;
        buffer[n] = (byte)(floatToRawIntBits & 0xFF);
        n = n3 + 1;
        buffer[n3] = (byte)(floatToRawIntBits >> 8 & 0xFF);
        buffer[n] = (byte)(floatToRawIntBits >> 16 & 0xFF);
        buffer[n + 1] = (byte)(floatToRawIntBits >> 24 & 0xFF);
    }
    
    @Override
    public void setInt(int n, final int n2) {
        this.requestCapacity(n + 4);
        final byte[] buffer = this.buffer;
        final int n3 = n + 1;
        buffer[n] = (byte)(n2 & 0xFF);
        n = n3 + 1;
        buffer[n3] = (byte)(n2 >> 8 & 0xFF);
        buffer[n] = (byte)(n2 >> 16 & 0xFF);
        buffer[n + 1] = (byte)(n2 >> 24 & 0xFF);
    }
    
    @Override
    public void setLong(int n, final long n2) {
        this.requestCapacity(n + 8);
        final int n3 = (int)n2;
        final byte[] buffer = this.buffer;
        final int n4 = n + 1;
        buffer[n] = (byte)(n3 & 0xFF);
        n = n4 + 1;
        buffer[n4] = (byte)(n3 >> 8 & 0xFF);
        final int n5 = n + 1;
        buffer[n] = (byte)(n3 >> 16 & 0xFF);
        n = n5 + 1;
        buffer[n5] = (byte)(n3 >> 24 & 0xFF);
        final int n6 = (int)(n2 >> 32);
        final int n7 = n + 1;
        buffer[n] = (byte)(n6 & 0xFF);
        n = n7 + 1;
        buffer[n7] = (byte)(n6 >> 8 & 0xFF);
        buffer[n] = (byte)(n6 >> 16 & 0xFF);
        buffer[n + 1] = (byte)(n6 >> 24 & 0xFF);
    }
    
    @Override
    public void setShort(final int n, final short n2) {
        this.requestCapacity(n + 2);
        final byte[] buffer = this.buffer;
        buffer[n] = (byte)(n2 & 0xFF);
        buffer[n + 1] = (byte)(n2 >> 8 & 0xFF);
    }
    
    @Override
    public int writePosition() {
        return this.writePos;
    }
}

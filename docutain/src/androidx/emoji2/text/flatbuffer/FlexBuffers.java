// 
// Decompiled by Procyon v0.6.0
// 

package androidx.emoji2.text.flatbuffer;

import java.nio.charset.StandardCharsets;
import java.nio.ByteBuffer;

public class FlexBuffers
{
    static final boolean $assertionsDisabled = false;
    private static final ReadBuf EMPTY_BB;
    public static final int FBT_BLOB = 25;
    public static final int FBT_BOOL = 26;
    public static final int FBT_FLOAT = 3;
    public static final int FBT_INDIRECT_FLOAT = 8;
    public static final int FBT_INDIRECT_INT = 6;
    public static final int FBT_INDIRECT_UINT = 7;
    public static final int FBT_INT = 1;
    public static final int FBT_KEY = 4;
    public static final int FBT_MAP = 9;
    public static final int FBT_NULL = 0;
    public static final int FBT_STRING = 5;
    public static final int FBT_UINT = 2;
    public static final int FBT_VECTOR = 10;
    public static final int FBT_VECTOR_BOOL = 36;
    public static final int FBT_VECTOR_FLOAT = 13;
    public static final int FBT_VECTOR_FLOAT2 = 18;
    public static final int FBT_VECTOR_FLOAT3 = 21;
    public static final int FBT_VECTOR_FLOAT4 = 24;
    public static final int FBT_VECTOR_INT = 11;
    public static final int FBT_VECTOR_INT2 = 16;
    public static final int FBT_VECTOR_INT3 = 19;
    public static final int FBT_VECTOR_INT4 = 22;
    public static final int FBT_VECTOR_KEY = 14;
    public static final int FBT_VECTOR_STRING_DEPRECATED = 15;
    public static final int FBT_VECTOR_UINT = 12;
    public static final int FBT_VECTOR_UINT2 = 17;
    public static final int FBT_VECTOR_UINT3 = 20;
    public static final int FBT_VECTOR_UINT4 = 23;
    
    static {
        EMPTY_BB = new ArrayReadWriteBuf(new byte[] { 0 }, 1);
    }
    
    public static Reference getRoot(final ReadBuf readBuf) {
        int n = readBuf.limit() - 1;
        final byte value = readBuf.get(n);
        --n;
        return new Reference(readBuf, n - value, value, Unsigned.byteToUnsignedInt(readBuf.get(n)));
    }
    
    @Deprecated
    public static Reference getRoot(final ByteBuffer byteBuffer) {
        ReadWriteBuf readWriteBuf;
        if (byteBuffer.hasArray()) {
            readWriteBuf = new ArrayReadWriteBuf(byteBuffer.array(), byteBuffer.limit());
        }
        else {
            readWriteBuf = new ByteBufferReadWriteBuf(byteBuffer);
        }
        return getRoot(readWriteBuf);
    }
    
    private static int indirect(final ReadBuf readBuf, final int n, final int n2) {
        return (int)(n - readUInt(readBuf, n, n2));
    }
    
    static boolean isTypeInline(final int n) {
        return n <= 3 || n == 26;
    }
    
    static boolean isTypedVector(final int n) {
        return (n >= 11 && n <= 15) || n == 36;
    }
    
    static boolean isTypedVectorElementType(final int n) {
        final boolean b = true;
        if (n >= 1) {
            final boolean b2 = b;
            if (n <= 4) {
                return b2;
            }
        }
        return n == 26 && b;
    }
    
    private static double readDouble(final ReadBuf readBuf, final int n, final int n2) {
        if (n2 == 4) {
            return readBuf.getFloat(n);
        }
        if (n2 != 8) {
            return -1.0;
        }
        return readBuf.getDouble(n);
    }
    
    private static int readInt(final ReadBuf readBuf, final int n, final int n2) {
        return (int)readLong(readBuf, n, n2);
    }
    
    private static long readLong(final ReadBuf readBuf, int n, final int n2) {
        if (n2 != 1) {
            if (n2 != 2) {
                if (n2 != 4) {
                    if (n2 != 8) {
                        return -1L;
                    }
                    return readBuf.getLong(n);
                }
                else {
                    n = readBuf.getInt(n);
                }
            }
            else {
                n = readBuf.getShort(n);
            }
        }
        else {
            n = readBuf.get(n);
        }
        return n;
    }
    
    private static long readUInt(final ReadBuf readBuf, final int n, final int n2) {
        if (n2 == 1) {
            return Unsigned.byteToUnsignedInt(readBuf.get(n));
        }
        if (n2 == 2) {
            return Unsigned.shortToUnsignedInt(readBuf.getShort(n));
        }
        if (n2 == 4) {
            return Unsigned.intToUnsignedLong(readBuf.getInt(n));
        }
        if (n2 != 8) {
            return -1L;
        }
        return readBuf.getLong(n);
    }
    
    static int toTypedVector(final int n, final int n2) {
        if (n2 == 0) {
            return n - 1 + 11;
        }
        if (n2 == 2) {
            return n - 1 + 16;
        }
        if (n2 == 3) {
            return n - 1 + 19;
        }
        if (n2 != 4) {
            return 0;
        }
        return n - 1 + 22;
    }
    
    static int toTypedVectorElementType(final int n) {
        return n - 11 + 1;
    }
    
    public static class Blob extends Sized
    {
        static final boolean $assertionsDisabled = false;
        static final Blob EMPTY;
        
        static {
            EMPTY = new Blob(FlexBuffers.EMPTY_BB, 1, 1);
        }
        
        Blob(final ReadBuf readBuf, final int n, final int n2) {
            super(readBuf, n, n2);
        }
        
        public static Blob empty() {
            return Blob.EMPTY;
        }
        
        public ByteBuffer data() {
            final ByteBuffer wrap = ByteBuffer.wrap(this.bb.data());
            wrap.position(this.end);
            wrap.limit(this.end + this.size());
            return wrap.asReadOnlyBuffer().slice();
        }
        
        public byte get(final int n) {
            return this.bb.get(this.end + n);
        }
        
        public byte[] getBytes() {
            final int size = this.size();
            final byte[] array = new byte[size];
            for (int i = 0; i < size; ++i) {
                array[i] = this.bb.get(this.end + i);
            }
            return array;
        }
        
        @Override
        public String toString() {
            return this.bb.getString(this.end, this.size());
        }
        
        @Override
        public StringBuilder toString(final StringBuilder sb) {
            sb.append('\"');
            sb.append(this.bb.getString(this.end, this.size()));
            sb.append('\"');
            return sb;
        }
    }
    
    private abstract static class Sized extends Object
    {
        protected final int size;
        
        Sized(final ReadBuf readBuf, final int n, final int n2) {
            super(readBuf, n, n2);
            this.size = readInt(this.bb, n - n2, n2);
        }
        
        public int size() {
            return this.size;
        }
    }
    
    private abstract static class Object
    {
        ReadBuf bb;
        int byteWidth;
        int end;
        
        Object(final ReadBuf bb, final int end, final int byteWidth) {
            this.bb = bb;
            this.end = end;
            this.byteWidth = byteWidth;
        }
        
        @Override
        public String toString() {
            return this.toString(new StringBuilder(128)).toString();
        }
        
        public abstract StringBuilder toString(final StringBuilder p0);
    }
    
    public static class FlexBufferException extends RuntimeException
    {
        FlexBufferException(final String message) {
            super(message);
        }
    }
    
    public static class Key extends Object
    {
        private static final Key EMPTY;
        
        static {
            EMPTY = new Key(FlexBuffers.EMPTY_BB, 0, 0);
        }
        
        Key(final ReadBuf readBuf, final int n, final int n2) {
            super(readBuf, n, n2);
        }
        
        public static Key empty() {
            return Key.EMPTY;
        }
        
        int compareTo(final byte[] array) {
            int end = this.end;
            int n = 0;
            byte value;
            byte b;
            do {
                value = this.bb.get(end);
                b = array[n];
                if (value == 0) {
                    return value - b;
                }
                ++end;
                if (++n == array.length) {
                    return value - b;
                }
            } while (value == b);
            return value - b;
        }
        
        @Override
        public boolean equals(final java.lang.Object o) {
            final boolean b = o instanceof Key;
            final boolean b2 = false;
            if (!b) {
                return false;
            }
            final Key key = (Key)o;
            boolean b3 = b2;
            if (key.end == this.end) {
                b3 = b2;
                if (key.byteWidth == this.byteWidth) {
                    b3 = true;
                }
            }
            return b3;
        }
        
        @Override
        public int hashCode() {
            return this.end ^ this.byteWidth;
        }
        
        @Override
        public String toString() {
            int end;
            for (end = this.end; this.bb.get(end) != 0; ++end) {}
            return this.bb.getString(this.end, end - this.end);
        }
        
        @Override
        public StringBuilder toString(final StringBuilder sb) {
            sb.append(this.toString());
            return sb;
        }
    }
    
    public static class KeyVector
    {
        private final TypedVector vec;
        
        KeyVector(final TypedVector vec) {
            this.vec = vec;
        }
        
        public Key get(final int n) {
            if (n >= this.size()) {
                return Key.EMPTY;
            }
            return new Key(this.vec.bb, indirect(this.vec.bb, this.vec.end + n * this.vec.byteWidth, this.vec.byteWidth), 1);
        }
        
        public int size() {
            return ((Vector)this.vec).size();
        }
        
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder();
            sb.append('[');
            for (int i = 0; i < ((Vector)this.vec).size(); ++i) {
                this.vec.get(i).toString(sb);
                if (i != ((Vector)this.vec).size() - 1) {
                    sb.append(", ");
                }
            }
            sb.append("]");
            return sb.toString();
        }
    }
    
    public static class Map extends Vector
    {
        private static final Map EMPTY_MAP;
        
        static {
            EMPTY_MAP = new Map(FlexBuffers.EMPTY_BB, 1, 1);
        }
        
        Map(final ReadBuf readBuf, final int n, final int n2) {
            super(readBuf, n, n2);
        }
        
        private int binarySearch(final KeyVector keyVector, final byte[] array) {
            int n = keyVector.size() - 1;
            int i = 0;
            while (i <= n) {
                final int n2 = i + n >>> 1;
                final int compareTo = keyVector.get(n2).compareTo(array);
                if (compareTo < 0) {
                    i = n2 + 1;
                }
                else {
                    if (compareTo <= 0) {
                        return n2;
                    }
                    n = n2 - 1;
                }
            }
            return -(i + 1);
        }
        
        public static Map empty() {
            return Map.EMPTY_MAP;
        }
        
        public Reference get(final String s) {
            return this.get(s.getBytes(StandardCharsets.UTF_8));
        }
        
        public Reference get(final byte[] array) {
            final KeyVector keys = this.keys();
            final int size = keys.size();
            final int binarySearch = this.binarySearch(keys, array);
            if (binarySearch >= 0 && binarySearch < size) {
                return ((Vector)this).get(binarySearch);
            }
            return Reference.NULL_REFERENCE;
        }
        
        public KeyVector keys() {
            final int n = this.end - this.byteWidth * 3;
            return new KeyVector(new TypedVector(this.bb, indirect(this.bb, n, this.byteWidth), readInt(this.bb, n + this.byteWidth, this.byteWidth), 4));
        }
        
        @Override
        public StringBuilder toString(final StringBuilder sb) {
            sb.append("{ ");
            final KeyVector keys = this.keys();
            final int size = ((Vector)this).size();
            final Vector values = this.values();
            for (int i = 0; i < size; ++i) {
                sb.append('\"');
                sb.append(keys.get(i).toString());
                sb.append("\" : ");
                sb.append(values.get(i).toString());
                if (i != size - 1) {
                    sb.append(", ");
                }
            }
            sb.append(" }");
            return sb;
        }
        
        public Vector values() {
            return new Vector(this.bb, this.end, this.byteWidth);
        }
    }
    
    public static class Vector extends Sized
    {
        private static final Vector EMPTY_VECTOR;
        
        static {
            EMPTY_VECTOR = new Vector(FlexBuffers.EMPTY_BB, 1, 1);
        }
        
        Vector(final ReadBuf readBuf, final int n, final int n2) {
            super(readBuf, n, n2);
        }
        
        public static Vector empty() {
            return Vector.EMPTY_VECTOR;
        }
        
        public Reference get(final int n) {
            final long n2 = this.size();
            final long n3 = n;
            if (n3 >= n2) {
                return Reference.NULL_REFERENCE;
            }
            return new Reference(this.bb, this.end + n * this.byteWidth, this.byteWidth, Unsigned.byteToUnsignedInt(this.bb.get((int)(this.end + n2 * this.byteWidth + n3))));
        }
        
        public boolean isEmpty() {
            return this == Vector.EMPTY_VECTOR;
        }
        
        @Override
        public StringBuilder toString(final StringBuilder sb) {
            sb.append("[ ");
            for (int size = this.size(), i = 0; i < size; ++i) {
                this.get(i).toString(sb);
                if (i != size - 1) {
                    sb.append(", ");
                }
            }
            sb.append(" ]");
            return sb;
        }
    }
    
    public static class Reference
    {
        private static final Reference NULL_REFERENCE;
        private ReadBuf bb;
        private int byteWidth;
        private int end;
        private int parentWidth;
        private int type;
        
        static {
            NULL_REFERENCE = new Reference(FlexBuffers.EMPTY_BB, 0, 1, 0);
        }
        
        Reference(final ReadBuf readBuf, final int n, final int n2, final int n3) {
            this(readBuf, n, n2, 1 << (n3 & 0x3), n3 >> 2);
        }
        
        Reference(final ReadBuf bb, final int end, final int parentWidth, final int byteWidth, final int type) {
            this.bb = bb;
            this.end = end;
            this.parentWidth = parentWidth;
            this.byteWidth = byteWidth;
            this.type = type;
        }
        
        public Blob asBlob() {
            if (!this.isBlob() && !this.isString()) {
                return Blob.empty();
            }
            final ReadBuf bb = this.bb;
            return new Blob(bb, indirect(bb, this.end, this.parentWidth), this.byteWidth);
        }
        
        public boolean asBoolean() {
            final boolean boolean1 = this.isBoolean();
            boolean b = true;
            final boolean b2 = true;
            if (boolean1) {
                return this.bb.get(this.end) != 0 && b2;
            }
            if (this.asUInt() == 0L) {
                b = false;
            }
            return b;
        }
        
        public double asFloat() {
            final int type = this.type;
            if (type == 3) {
                return readDouble(this.bb, this.end, this.parentWidth);
            }
            if (type != 1) {
                if (type != 2) {
                    if (type == 5) {
                        return Double.parseDouble(this.asString());
                    }
                    if (type == 6) {
                        final ReadBuf bb = this.bb;
                        return readInt(bb, indirect(bb, this.end, this.parentWidth), this.byteWidth);
                    }
                    if (type == 7) {
                        final ReadBuf bb2 = this.bb;
                        return (double)readUInt(bb2, indirect(bb2, this.end, this.parentWidth), this.byteWidth);
                    }
                    if (type == 8) {
                        final ReadBuf bb3 = this.bb;
                        return readDouble(bb3, indirect(bb3, this.end, this.parentWidth), this.byteWidth);
                    }
                    if (type == 10) {
                        return this.asVector().size();
                    }
                    if (type != 26) {
                        return 0.0;
                    }
                }
                return (double)readUInt(this.bb, this.end, this.parentWidth);
            }
            return readInt(this.bb, this.end, this.parentWidth);
        }
        
        public int asInt() {
            final int type = this.type;
            if (type == 1) {
                return readInt(this.bb, this.end, this.parentWidth);
            }
            long n;
            if (type != 2) {
                if (type == 3) {
                    return (int)readDouble(this.bb, this.end, this.parentWidth);
                }
                if (type == 5) {
                    return Integer.parseInt(this.asString());
                }
                if (type == 6) {
                    final ReadBuf bb = this.bb;
                    return readInt(bb, indirect(bb, this.end, this.parentWidth), this.byteWidth);
                }
                if (type != 7) {
                    if (type == 8) {
                        final ReadBuf bb2 = this.bb;
                        return (int)readDouble(bb2, indirect(bb2, this.end, this.parentWidth), this.byteWidth);
                    }
                    if (type == 10) {
                        return this.asVector().size();
                    }
                    if (type != 26) {
                        return 0;
                    }
                    return readInt(this.bb, this.end, this.parentWidth);
                }
                else {
                    final ReadBuf bb3 = this.bb;
                    n = readUInt(bb3, indirect(bb3, this.end, this.parentWidth), this.parentWidth);
                }
            }
            else {
                n = readUInt(this.bb, this.end, this.parentWidth);
            }
            return (int)n;
        }
        
        public Key asKey() {
            if (this.isKey()) {
                final ReadBuf bb = this.bb;
                return new Key(bb, indirect(bb, this.end, this.parentWidth), this.byteWidth);
            }
            return Key.empty();
        }
        
        public long asLong() {
            final int type = this.type;
            if (type == 1) {
                return readLong(this.bb, this.end, this.parentWidth);
            }
            if (type != 2) {
                if (type != 3) {
                    if (type != 5) {
                        if (type == 6) {
                            final ReadBuf bb = this.bb;
                            return readLong(bb, indirect(bb, this.end, this.parentWidth), this.byteWidth);
                        }
                        if (type == 7) {
                            final ReadBuf bb2 = this.bb;
                            return readUInt(bb2, indirect(bb2, this.end, this.parentWidth), this.parentWidth);
                        }
                        if (type == 8) {
                            final ReadBuf bb3 = this.bb;
                            return (long)readDouble(bb3, indirect(bb3, this.end, this.parentWidth), this.byteWidth);
                        }
                        if (type == 10) {
                            return this.asVector().size();
                        }
                        if (type != 26) {
                            return 0L;
                        }
                        return readInt(this.bb, this.end, this.parentWidth);
                    }
                    else {
                        try {
                            return Long.parseLong(this.asString());
                        }
                        catch (final NumberFormatException ex) {
                            return 0L;
                        }
                    }
                }
                return (long)readDouble(this.bb, this.end, this.parentWidth);
            }
            return readUInt(this.bb, this.end, this.parentWidth);
        }
        
        public Map asMap() {
            if (this.isMap()) {
                final ReadBuf bb = this.bb;
                return new Map(bb, indirect(bb, this.end, this.parentWidth), this.byteWidth);
            }
            return Map.empty();
        }
        
        public String asString() {
            if (this.isString()) {
                final int access$200 = indirect(this.bb, this.end, this.parentWidth);
                final ReadBuf bb = this.bb;
                final int byteWidth = this.byteWidth;
                return this.bb.getString(access$200, (int)readUInt(bb, access$200 - byteWidth, byteWidth));
            }
            if (this.isKey()) {
                int access$201;
                int n;
                for (n = (access$201 = indirect(this.bb, this.end, this.byteWidth)); this.bb.get(access$201) != 0; ++access$201) {}
                return this.bb.getString(n, access$201 - n);
            }
            return "";
        }
        
        public long asUInt() {
            final int type = this.type;
            if (type == 2) {
                return readUInt(this.bb, this.end, this.parentWidth);
            }
            if (type == 1) {
                return readLong(this.bb, this.end, this.parentWidth);
            }
            if (type == 3) {
                return (long)readDouble(this.bb, this.end, this.parentWidth);
            }
            if (type == 10) {
                return this.asVector().size();
            }
            if (type == 26) {
                return readInt(this.bb, this.end, this.parentWidth);
            }
            if (type == 5) {
                return Long.parseLong(this.asString());
            }
            if (type == 6) {
                final ReadBuf bb = this.bb;
                return readLong(bb, indirect(bb, this.end, this.parentWidth), this.byteWidth);
            }
            if (type == 7) {
                final ReadBuf bb2 = this.bb;
                return readUInt(bb2, indirect(bb2, this.end, this.parentWidth), this.byteWidth);
            }
            if (type != 8) {
                return 0L;
            }
            final ReadBuf bb3 = this.bb;
            return (long)readDouble(bb3, indirect(bb3, this.end, this.parentWidth), this.parentWidth);
        }
        
        public Vector asVector() {
            if (this.isVector()) {
                final ReadBuf bb = this.bb;
                return new Vector(bb, indirect(bb, this.end, this.parentWidth), this.byteWidth);
            }
            final int type = this.type;
            if (type == 15) {
                final ReadBuf bb2 = this.bb;
                return new TypedVector(bb2, indirect(bb2, this.end, this.parentWidth), this.byteWidth, 4);
            }
            if (FlexBuffers.isTypedVector(type)) {
                final ReadBuf bb3 = this.bb;
                return new TypedVector(bb3, indirect(bb3, this.end, this.parentWidth), this.byteWidth, FlexBuffers.toTypedVectorElementType(this.type));
            }
            return Vector.empty();
        }
        
        public int getType() {
            return this.type;
        }
        
        public boolean isBlob() {
            return this.type == 25;
        }
        
        public boolean isBoolean() {
            return this.type == 26;
        }
        
        public boolean isFloat() {
            final int type = this.type;
            return type == 3 || type == 8;
        }
        
        public boolean isInt() {
            final int type = this.type;
            boolean b = true;
            if (type != 1) {
                b = (type == 6 && b);
            }
            return b;
        }
        
        public boolean isIntOrUInt() {
            return this.isInt() || this.isUInt();
        }
        
        public boolean isKey() {
            return this.type == 4;
        }
        
        public boolean isMap() {
            return this.type == 9;
        }
        
        public boolean isNull() {
            return this.type == 0;
        }
        
        public boolean isNumeric() {
            return this.isIntOrUInt() || this.isFloat();
        }
        
        public boolean isString() {
            return this.type == 5;
        }
        
        public boolean isTypedVector() {
            return FlexBuffers.isTypedVector(this.type);
        }
        
        public boolean isUInt() {
            final int type = this.type;
            return type == 2 || type == 7;
        }
        
        public boolean isVector() {
            final int type = this.type;
            return type == 10 || type == 9;
        }
        
        @Override
        public String toString() {
            return this.toString(new StringBuilder(128)).toString();
        }
        
        StringBuilder toString(StringBuilder string) {
            final int type = this.type;
            if (type != 36) {
                switch (type) {
                    default: {
                        return string;
                    }
                    case 26: {
                        string.append(this.asBoolean());
                        return string;
                    }
                    case 25: {
                        return this.asBlob().toString(string);
                    }
                    case 16:
                    case 17:
                    case 18:
                    case 19:
                    case 20:
                    case 21:
                    case 22:
                    case 23:
                    case 24: {
                        string = new StringBuilder();
                        string.append("not_implemented:");
                        string.append(this.type);
                        throw new FlexBufferException(string.toString());
                    }
                    case 10: {
                        return this.asVector().toString(string);
                    }
                    case 9: {
                        return this.asMap().toString(string);
                    }
                    case 5: {
                        string.append('\"');
                        string.append(this.asString());
                        string.append('\"');
                        return string;
                    }
                    case 4: {
                        final Key key = this.asKey();
                        string.append('\"');
                        string = key.toString(string);
                        string.append('\"');
                        return string;
                    }
                    case 3:
                    case 8: {
                        string.append(this.asFloat());
                        return string;
                    }
                    case 2:
                    case 7: {
                        string.append(this.asUInt());
                        return string;
                    }
                    case 1:
                    case 6: {
                        string.append(this.asLong());
                        return string;
                    }
                    case 0: {
                        string.append("null");
                        return string;
                    }
                    case 11:
                    case 12:
                    case 13:
                    case 14:
                    case 15: {
                        break;
                    }
                }
            }
            string.append(this.asVector());
            return string;
        }
    }
    
    public static class TypedVector extends Vector
    {
        private static final TypedVector EMPTY_VECTOR;
        private final int elemType;
        
        static {
            EMPTY_VECTOR = new TypedVector(FlexBuffers.EMPTY_BB, 1, 1, 1);
        }
        
        TypedVector(final ReadBuf readBuf, final int n, final int n2, final int elemType) {
            super(readBuf, n, n2);
            this.elemType = elemType;
        }
        
        public static TypedVector empty() {
            return TypedVector.EMPTY_VECTOR;
        }
        
        @Override
        public Reference get(final int n) {
            if (n >= ((Vector)this).size()) {
                return Reference.NULL_REFERENCE;
            }
            return new Reference(this.bb, this.end + n * this.byteWidth, this.byteWidth, 1, this.elemType);
        }
        
        public int getElemType() {
            return this.elemType;
        }
        
        public boolean isEmptyVector() {
            return this == TypedVector.EMPTY_VECTOR;
        }
    }
    
    static class Unsigned
    {
        static int byteToUnsignedInt(final byte b) {
            return b & 0xFF;
        }
        
        static long intToUnsignedLong(final int n) {
            return (long)n & 0xFFFFFFFFL;
        }
        
        static int shortToUnsignedInt(final short n) {
            return n & 0xFFFF;
        }
    }
}

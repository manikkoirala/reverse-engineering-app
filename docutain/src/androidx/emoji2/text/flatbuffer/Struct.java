// 
// Decompiled by Procyon v0.6.0
// 

package androidx.emoji2.text.flatbuffer;

import java.nio.ByteBuffer;

public class Struct
{
    protected ByteBuffer bb;
    protected int bb_pos;
    
    public void __reset() {
        this.__reset(0, null);
    }
    
    protected void __reset(final int bb_pos, final ByteBuffer bb) {
        this.bb = bb;
        if (bb != null) {
            this.bb_pos = bb_pos;
        }
        else {
            this.bb_pos = 0;
        }
    }
}

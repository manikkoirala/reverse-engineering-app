// 
// Decompiled by Procyon v0.6.0
// 

package androidx.emoji2.text.flatbuffer;

import java.nio.ByteBuffer;

public final class ByteVector extends BaseVector
{
    public ByteVector __assign(final int n, final ByteBuffer byteBuffer) {
        this.__reset(n, 1, byteBuffer);
        return this;
    }
    
    public byte get(final int n) {
        return this.bb.get(this.__element(n));
    }
    
    public int getAsUnsigned(final int n) {
        return this.get(n) & 0xFF;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.emoji2.text.flatbuffer;

import java.nio.ByteBuffer;

public final class FloatVector extends BaseVector
{
    public FloatVector __assign(final int n, final ByteBuffer byteBuffer) {
        this.__reset(n, 4, byteBuffer);
        return this;
    }
    
    public float get(final int n) {
        return this.bb.getFloat(this.__element(n));
    }
}

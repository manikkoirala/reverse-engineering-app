// 
// Decompiled by Procyon v0.6.0
// 

package androidx.emoji2.text.flatbuffer;

import java.util.Arrays;
import java.util.Comparator;
import java.nio.ByteOrder;
import java.nio.ByteBuffer;

public class Table
{
    protected ByteBuffer bb;
    protected int bb_pos;
    Utf8 utf8;
    private int vtable_size;
    private int vtable_start;
    
    public Table() {
        this.utf8 = Utf8.getDefault();
    }
    
    protected static boolean __has_identifier(final ByteBuffer byteBuffer, final String s) {
        if (s.length() == 4) {
            for (int i = 0; i < 4; ++i) {
                if (s.charAt(i) != (char)byteBuffer.get(byteBuffer.position() + 4 + i)) {
                    return false;
                }
            }
            return true;
        }
        throw new AssertionError((Object)"FlatBuffers: file identifier must be length 4");
    }
    
    protected static int __indirect(final int n, final ByteBuffer byteBuffer) {
        return n + byteBuffer.getInt(n);
    }
    
    protected static int __offset(final int n, int n2, final ByteBuffer byteBuffer) {
        n2 = byteBuffer.capacity() - n2;
        return byteBuffer.getShort(n + n2 - byteBuffer.getInt(n2)) + n2;
    }
    
    protected static String __string(int n, final ByteBuffer byteBuffer, final Utf8 utf8) {
        n += byteBuffer.getInt(n);
        return utf8.decodeUtf8(byteBuffer, n + 4, byteBuffer.getInt(n));
    }
    
    protected static Table __union(final Table table, final int n, final ByteBuffer byteBuffer) {
        table.__reset(__indirect(n, byteBuffer), byteBuffer);
        return table;
    }
    
    protected static int compareStrings(int i, int min, final ByteBuffer byteBuffer) {
        final int n = i + byteBuffer.getInt(i);
        final int n2 = min + byteBuffer.getInt(min);
        final int int1 = byteBuffer.getInt(n);
        final int int2 = byteBuffer.getInt(n2);
        int n3;
        byte value;
        int n4;
        for (min = Math.min(int1, int2), i = 0; i < min; ++i) {
            n3 = i + (n + 4);
            value = byteBuffer.get(n3);
            n4 = i + (n2 + 4);
            if (value != byteBuffer.get(n4)) {
                return byteBuffer.get(n3) - byteBuffer.get(n4);
            }
        }
        return int1 - int2;
    }
    
    protected static int compareStrings(int i, final byte[] array, final ByteBuffer byteBuffer) {
        final int n = i + byteBuffer.getInt(i);
        final int int1 = byteBuffer.getInt(n);
        final int length = array.length;
        int min;
        int n2;
        for (min = Math.min(int1, length), i = 0; i < min; ++i) {
            n2 = i + (n + 4);
            if (byteBuffer.get(n2) != array[i]) {
                return byteBuffer.get(n2) - array[i];
            }
        }
        return int1 - length;
    }
    
    protected int __indirect(final int n) {
        return n + this.bb.getInt(n);
    }
    
    protected int __offset(int short1) {
        if (short1 < this.vtable_size) {
            short1 = this.bb.getShort(this.vtable_start + short1);
        }
        else {
            short1 = 0;
        }
        return short1;
    }
    
    public void __reset() {
        this.__reset(0, null);
    }
    
    protected void __reset(int n, final ByteBuffer bb) {
        this.bb = bb;
        if (bb != null) {
            this.bb_pos = n;
            n -= bb.getInt(n);
            this.vtable_start = n;
            this.vtable_size = this.bb.getShort(n);
        }
        else {
            this.bb_pos = 0;
            this.vtable_start = 0;
            this.vtable_size = 0;
        }
    }
    
    protected String __string(final int n) {
        return __string(n, this.bb, this.utf8);
    }
    
    protected Table __union(final Table table, final int n) {
        return __union(table, n, this.bb);
    }
    
    protected int __vector(int n) {
        n += this.bb_pos;
        return n + this.bb.getInt(n) + 4;
    }
    
    protected ByteBuffer __vector_as_bytebuffer(int _vector, final int n) {
        final int _offset = this.__offset(_vector);
        if (_offset == 0) {
            return null;
        }
        final ByteBuffer order = this.bb.duplicate().order(ByteOrder.LITTLE_ENDIAN);
        _vector = this.__vector(_offset);
        order.position(_vector);
        order.limit(_vector + this.__vector_len(_offset) * n);
        return order;
    }
    
    protected ByteBuffer __vector_in_bytebuffer(final ByteBuffer byteBuffer, int _vector, final int n) {
        final int _offset = this.__offset(_vector);
        if (_offset == 0) {
            return null;
        }
        _vector = this.__vector(_offset);
        byteBuffer.rewind();
        byteBuffer.limit(this.__vector_len(_offset) * n + _vector);
        byteBuffer.position(_vector);
        return byteBuffer;
    }
    
    protected int __vector_len(int int1) {
        final int n = int1 + this.bb_pos;
        int1 = this.bb.getInt(n);
        return this.bb.getInt(n + int1);
    }
    
    public ByteBuffer getByteBuffer() {
        return this.bb;
    }
    
    protected int keysCompare(final Integer n, final Integer n2, final ByteBuffer byteBuffer) {
        return 0;
    }
    
    protected void sortTables(final int[] array, final ByteBuffer byteBuffer) {
        final Integer[] a = new Integer[array.length];
        final int n = 0;
        for (int i = 0; i < array.length; ++i) {
            a[i] = array[i];
        }
        Arrays.sort(a, new Comparator<Integer>(this, byteBuffer) {
            final Table this$0;
            final ByteBuffer val$bb;
            
            @Override
            public int compare(final Integer n, final Integer n2) {
                return this.this$0.keysCompare(n, n2, this.val$bb);
            }
        });
        for (int j = n; j < array.length; ++j) {
            array[j] = a[j];
        }
    }
}

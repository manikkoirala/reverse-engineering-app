// 
// Decompiled by Procyon v0.6.0
// 

package androidx.emoji2.text.flatbuffer;

import java.nio.ByteBuffer;

public final class UnionVector extends BaseVector
{
    public UnionVector __assign(final int n, final int n2, final ByteBuffer byteBuffer) {
        this.__reset(n, n2, byteBuffer);
        return this;
    }
    
    public Table get(final Table table, final int n) {
        return Table.__union(table, this.__element(n), this.bb);
    }
}

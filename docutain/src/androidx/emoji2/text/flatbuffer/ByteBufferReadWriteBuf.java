// 
// Decompiled by Procyon v0.6.0
// 

package androidx.emoji2.text.flatbuffer;

import java.nio.ByteOrder;
import java.nio.ByteBuffer;

public class ByteBufferReadWriteBuf implements ReadWriteBuf
{
    private final ByteBuffer buffer;
    
    public ByteBufferReadWriteBuf(final ByteBuffer buffer) {
        (this.buffer = buffer).order(ByteOrder.LITTLE_ENDIAN);
    }
    
    @Override
    public byte[] data() {
        return this.buffer.array();
    }
    
    @Override
    public byte get(final int n) {
        return this.buffer.get(n);
    }
    
    @Override
    public boolean getBoolean(final int n) {
        return this.get(n) != 0;
    }
    
    @Override
    public double getDouble(final int n) {
        return this.buffer.getDouble(n);
    }
    
    @Override
    public float getFloat(final int n) {
        return this.buffer.getFloat(n);
    }
    
    @Override
    public int getInt(final int n) {
        return this.buffer.getInt(n);
    }
    
    @Override
    public long getLong(final int n) {
        return this.buffer.getLong(n);
    }
    
    @Override
    public short getShort(final int n) {
        return this.buffer.getShort(n);
    }
    
    @Override
    public String getString(final int n, final int n2) {
        return Utf8Safe.decodeUtf8Buffer(this.buffer, n, n2);
    }
    
    @Override
    public int limit() {
        return this.buffer.limit();
    }
    
    @Override
    public void put(final byte b) {
        this.buffer.put(b);
    }
    
    @Override
    public void put(final byte[] src, final int offset, final int length) {
        this.buffer.put(src, offset, length);
    }
    
    @Override
    public void putBoolean(final boolean b) {
        this.buffer.put((byte)(b ? 1 : 0));
    }
    
    @Override
    public void putDouble(final double n) {
        this.buffer.putDouble(n);
    }
    
    @Override
    public void putFloat(final float n) {
        this.buffer.putFloat(n);
    }
    
    @Override
    public void putInt(final int n) {
        this.buffer.putInt(n);
    }
    
    @Override
    public void putLong(final long n) {
        this.buffer.putLong(n);
    }
    
    @Override
    public void putShort(final short n) {
        this.buffer.putShort(n);
    }
    
    @Override
    public boolean requestCapacity(final int n) {
        return n <= this.buffer.limit();
    }
    
    @Override
    public void set(final int n, final byte b) {
        this.requestCapacity(n + 1);
        this.buffer.put(n, b);
    }
    
    @Override
    public void set(final int n, final byte[] src, final int offset, final int length) {
        this.requestCapacity(length - offset + n);
        final int position = this.buffer.position();
        this.buffer.position(n);
        this.buffer.put(src, offset, length);
        this.buffer.position(position);
    }
    
    @Override
    public void setBoolean(final int n, final boolean b) {
        this.set(n, (byte)(b ? 1 : 0));
    }
    
    @Override
    public void setDouble(final int n, final double n2) {
        this.requestCapacity(n + 8);
        this.buffer.putDouble(n, n2);
    }
    
    @Override
    public void setFloat(final int n, final float n2) {
        this.requestCapacity(n + 4);
        this.buffer.putFloat(n, n2);
    }
    
    @Override
    public void setInt(final int n, final int n2) {
        this.requestCapacity(n + 4);
        this.buffer.putInt(n, n2);
    }
    
    @Override
    public void setLong(final int n, final long n2) {
        this.requestCapacity(n + 8);
        this.buffer.putLong(n, n2);
    }
    
    @Override
    public void setShort(final int n, final short n2) {
        this.requestCapacity(n + 2);
        this.buffer.putShort(n, n2);
    }
    
    @Override
    public int writePosition() {
        return this.buffer.position();
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.emoji2.text.flatbuffer;

import java.nio.ByteOrder;
import java.nio.ByteBuffer;

public final class MetadataList extends Table
{
    public static void ValidateVersion() {
        Constants.FLATBUFFERS_1_12_0();
    }
    
    public static void addList(final FlatBufferBuilder flatBufferBuilder, final int n) {
        flatBufferBuilder.addOffset(1, n, 0);
    }
    
    public static void addSourceSha(final FlatBufferBuilder flatBufferBuilder, final int n) {
        flatBufferBuilder.addOffset(2, n, 0);
    }
    
    public static void addVersion(final FlatBufferBuilder flatBufferBuilder, final int n) {
        flatBufferBuilder.addInt(0, n, 0);
    }
    
    public static int createListVector(final FlatBufferBuilder flatBufferBuilder, final int[] array) {
        flatBufferBuilder.startVector(4, array.length, 4);
        for (int i = array.length - 1; i >= 0; --i) {
            flatBufferBuilder.addOffset(array[i]);
        }
        return flatBufferBuilder.endVector();
    }
    
    public static int createMetadataList(final FlatBufferBuilder flatBufferBuilder, final int n, final int n2, final int n3) {
        flatBufferBuilder.startTable(3);
        addSourceSha(flatBufferBuilder, n3);
        addList(flatBufferBuilder, n2);
        addVersion(flatBufferBuilder, n);
        return endMetadataList(flatBufferBuilder);
    }
    
    public static int endMetadataList(final FlatBufferBuilder flatBufferBuilder) {
        return flatBufferBuilder.endTable();
    }
    
    public static void finishMetadataListBuffer(final FlatBufferBuilder flatBufferBuilder, final int n) {
        flatBufferBuilder.finish(n);
    }
    
    public static void finishSizePrefixedMetadataListBuffer(final FlatBufferBuilder flatBufferBuilder, final int n) {
        flatBufferBuilder.finishSizePrefixed(n);
    }
    
    public static MetadataList getRootAsMetadataList(final ByteBuffer byteBuffer) {
        return getRootAsMetadataList(byteBuffer, new MetadataList());
    }
    
    public static MetadataList getRootAsMetadataList(final ByteBuffer byteBuffer, final MetadataList list) {
        byteBuffer.order(ByteOrder.LITTLE_ENDIAN);
        return list.__assign(byteBuffer.getInt(byteBuffer.position()) + byteBuffer.position(), byteBuffer);
    }
    
    public static void startListVector(final FlatBufferBuilder flatBufferBuilder, final int n) {
        flatBufferBuilder.startVector(4, n, 4);
    }
    
    public static void startMetadataList(final FlatBufferBuilder flatBufferBuilder) {
        flatBufferBuilder.startTable(3);
    }
    
    public MetadataList __assign(final int n, final ByteBuffer byteBuffer) {
        this.__init(n, byteBuffer);
        return this;
    }
    
    public void __init(final int n, final ByteBuffer byteBuffer) {
        this.__reset(n, byteBuffer);
    }
    
    public MetadataItem list(final int n) {
        return this.list(new MetadataItem(), n);
    }
    
    public MetadataItem list(MetadataItem _assign, final int n) {
        final int _offset = this.__offset(6);
        if (_offset != 0) {
            _assign = _assign.__assign(this.__indirect(this.__vector(_offset) + n * 4), this.bb);
        }
        else {
            _assign = null;
        }
        return _assign;
    }
    
    public int listLength() {
        final int _offset = this.__offset(6);
        int _vector_len;
        if (_offset != 0) {
            _vector_len = this.__vector_len(_offset);
        }
        else {
            _vector_len = 0;
        }
        return _vector_len;
    }
    
    public MetadataItem.Vector listVector() {
        return this.listVector(new MetadataItem.Vector());
    }
    
    public MetadataItem.Vector listVector(MetadataItem.Vector _assign) {
        final int _offset = this.__offset(6);
        if (_offset != 0) {
            _assign = _assign.__assign(this.__vector(_offset), 4, this.bb);
        }
        else {
            _assign = null;
        }
        return _assign;
    }
    
    public String sourceSha() {
        final int _offset = this.__offset(8);
        String _string;
        if (_offset != 0) {
            _string = this.__string(_offset + this.bb_pos);
        }
        else {
            _string = null;
        }
        return _string;
    }
    
    public ByteBuffer sourceShaAsByteBuffer() {
        return this.__vector_as_bytebuffer(8, 1);
    }
    
    public ByteBuffer sourceShaInByteBuffer(final ByteBuffer byteBuffer) {
        return this.__vector_in_bytebuffer(byteBuffer, 8, 1);
    }
    
    public int version() {
        final int _offset = this.__offset(4);
        int int1;
        if (_offset != 0) {
            int1 = this.bb.getInt(_offset + this.bb_pos);
        }
        else {
            int1 = 0;
        }
        return int1;
    }
    
    public static final class Vector extends BaseVector
    {
        public Vector __assign(final int n, final int n2, final ByteBuffer byteBuffer) {
            this.__reset(n, n2, byteBuffer);
            return this;
        }
        
        public MetadataList get(final int n) {
            return this.get(new MetadataList(), n);
        }
        
        public MetadataList get(final MetadataList list, final int n) {
            return list.__assign(Table.__indirect(this.__element(n), this.bb), this.bb);
        }
    }
}

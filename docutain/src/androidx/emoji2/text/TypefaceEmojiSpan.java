// 
// Decompiled by Procyon v0.6.0
// 

package androidx.emoji2.text;

import android.graphics.Canvas;
import android.graphics.Paint$Style;
import android.text.style.MetricAffectingSpan;
import android.text.style.CharacterStyle;
import android.text.Spanned;
import android.text.TextPaint;
import android.graphics.Paint;

public final class TypefaceEmojiSpan extends EmojiSpan
{
    private static Paint sDebugPaint;
    private TextPaint mWorkingPaint;
    
    public TypefaceEmojiSpan(final TypefaceEmojiRasterizer typefaceEmojiRasterizer) {
        super(typefaceEmojiRasterizer);
    }
    
    private TextPaint applyCharacterSpanStyles(final CharSequence charSequence, int i, int length, final Paint paint) {
        if (charSequence instanceof Spanned) {
            final CharacterStyle[] array = (CharacterStyle[])((Spanned)charSequence).getSpans(i, length, (Class)CharacterStyle.class);
            if (array.length != 0) {
                length = array.length;
                i = 0;
                if (length != 1 || array[0] != this) {
                    TextPaint mWorkingPaint;
                    if ((mWorkingPaint = this.mWorkingPaint) == null) {
                        mWorkingPaint = new TextPaint();
                        this.mWorkingPaint = mWorkingPaint;
                    }
                    mWorkingPaint.set(paint);
                    while (i < array.length) {
                        final CharacterStyle characterStyle = array[i];
                        if (!(characterStyle instanceof MetricAffectingSpan)) {
                            characterStyle.updateDrawState(mWorkingPaint);
                        }
                        ++i;
                    }
                    return mWorkingPaint;
                }
            }
            if (paint instanceof TextPaint) {
                return (TextPaint)paint;
            }
            return null;
        }
        else {
            if (paint instanceof TextPaint) {
                return (TextPaint)paint;
            }
            return null;
        }
    }
    
    private static Paint getDebugPaint() {
        if (TypefaceEmojiSpan.sDebugPaint == null) {
            (TypefaceEmojiSpan.sDebugPaint = (Paint)new TextPaint()).setColor(EmojiCompat.get().getEmojiSpanIndicatorColor());
            TypefaceEmojiSpan.sDebugPaint.setStyle(Paint$Style.FILL);
        }
        return TypefaceEmojiSpan.sDebugPaint;
    }
    
    public void draw(final Canvas canvas, final CharSequence charSequence, final int n, final int n2, final float n3, final int n4, final int n5, final int n6, Paint paint) {
        final TextPaint applyCharacterSpanStyles = this.applyCharacterSpanStyles(charSequence, n, n2, paint);
        if (applyCharacterSpanStyles != null && applyCharacterSpanStyles.bgColor != 0) {
            this.drawBackground(canvas, applyCharacterSpanStyles, n3, n3 + this.getWidth(), (float)n4, (float)n6);
        }
        if (EmojiCompat.get().isEmojiSpanIndicatorEnabled()) {
            canvas.drawRect(n3, (float)n4, n3 + this.getWidth(), (float)n6, getDebugPaint());
        }
        final TypefaceEmojiRasterizer typefaceRasterizer = this.getTypefaceRasterizer();
        final float n7 = (float)n5;
        if (applyCharacterSpanStyles != null) {
            paint = (Paint)applyCharacterSpanStyles;
        }
        typefaceRasterizer.draw(canvas, n3, n7, paint);
    }
    
    void drawBackground(final Canvas canvas, final TextPaint textPaint, final float n, final float n2, final float n3, final float n4) {
        final int color = textPaint.getColor();
        final Paint$Style style = textPaint.getStyle();
        textPaint.setColor(textPaint.bgColor);
        textPaint.setStyle(Paint$Style.FILL);
        canvas.drawRect(n, n3, n2, n4, (Paint)textPaint);
        textPaint.setStyle(style);
        textPaint.setColor(color);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.emoji2.text;

import java.util.Iterator;
import java.util.Collections;
import android.os.Build$VERSION;
import java.util.Set;

class EmojiExclusions
{
    private EmojiExclusions() {
    }
    
    static Set<int[]> getEmojiExclusions() {
        if (Build$VERSION.SDK_INT >= 34) {
            return EmojiExclusions_Api34.getExclusions();
        }
        return EmojiExclusions_Reflections.getExclusions();
    }
    
    private static class EmojiExclusions_Api34
    {
        static Set<int[]> getExclusions() {
            return EmojiExclusions_Reflections.getExclusions();
        }
    }
    
    private static class EmojiExclusions_Reflections
    {
        static Set<int[]> getExclusions() {
            try {
                final Object invoke = Class.forName("android.text.EmojiConsistency").getMethod("getEmojiConsistencySet", (Class<?>[])new Class[0]).invoke(null, new Object[0]);
                if (invoke == null) {}
                final Set set = (Set)invoke;
                final Iterator iterator = set.iterator();
                do {
                    final Set<Object> emptySet = set;
                    if (iterator.hasNext()) {
                        continue;
                    }
                    return (Set<int[]>)emptySet;
                } while (iterator.next() instanceof int[]);
                final Set<Object> emptySet = Collections.emptySet();
                return (Set<int[]>)emptySet;
            }
            finally {
                return Collections.emptySet();
            }
        }
    }
}

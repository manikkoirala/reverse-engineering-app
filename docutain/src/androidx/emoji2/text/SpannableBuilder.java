// 
// Decompiled by Procyon v0.6.0
// 

package androidx.emoji2.text;

import android.os.Build$VERSION;
import android.text.Spannable;
import java.util.concurrent.atomic.AtomicInteger;
import android.text.SpanWatcher;
import android.text.TextWatcher;
import java.lang.reflect.Array;
import java.io.IOException;
import android.text.Editable;
import androidx.core.util.Preconditions;
import java.util.ArrayList;
import java.util.List;
import android.text.SpannableStringBuilder;

public final class SpannableBuilder extends SpannableStringBuilder
{
    private final Class<?> mWatcherClass;
    private final List<WatcherWrapper> mWatchers;
    
    SpannableBuilder(final Class<?> mWatcherClass) {
        this.mWatchers = new ArrayList<WatcherWrapper>();
        Preconditions.checkNotNull(mWatcherClass, "watcherClass cannot be null");
        this.mWatcherClass = mWatcherClass;
    }
    
    SpannableBuilder(final Class<?> mWatcherClass, final CharSequence charSequence) {
        super(charSequence);
        this.mWatchers = new ArrayList<WatcherWrapper>();
        Preconditions.checkNotNull(mWatcherClass, "watcherClass cannot be null");
        this.mWatcherClass = mWatcherClass;
    }
    
    SpannableBuilder(final Class<?> mWatcherClass, final CharSequence charSequence, final int n, final int n2) {
        super(charSequence, n, n2);
        this.mWatchers = new ArrayList<WatcherWrapper>();
        Preconditions.checkNotNull(mWatcherClass, "watcherClass cannot be null");
        this.mWatcherClass = mWatcherClass;
    }
    
    private void blockWatchers() {
        for (int i = 0; i < this.mWatchers.size(); ++i) {
            this.mWatchers.get(i).blockCalls();
        }
    }
    
    public static SpannableBuilder create(final Class<?> clazz, final CharSequence charSequence) {
        return new SpannableBuilder(clazz, charSequence);
    }
    
    private void fireWatchers() {
        for (int i = 0; i < this.mWatchers.size(); ++i) {
            this.mWatchers.get(i).onTextChanged((CharSequence)this, 0, this.length(), this.length());
        }
    }
    
    private WatcherWrapper getWatcherFor(final Object o) {
        for (int i = 0; i < this.mWatchers.size(); ++i) {
            final WatcherWrapper watcherWrapper = this.mWatchers.get(i);
            if (watcherWrapper.mObject == o) {
                return watcherWrapper;
            }
        }
        return null;
    }
    
    private boolean isWatcher(final Class<?> clazz) {
        return this.mWatcherClass == clazz;
    }
    
    private boolean isWatcher(final Object o) {
        return o != null && this.isWatcher(o.getClass());
    }
    
    private void unblockwatchers() {
        for (int i = 0; i < this.mWatchers.size(); ++i) {
            this.mWatchers.get(i).unblockCalls();
        }
    }
    
    public SpannableStringBuilder append(final char c) {
        super.append(c);
        return this;
    }
    
    public SpannableStringBuilder append(final CharSequence charSequence) {
        super.append(charSequence);
        return this;
    }
    
    public SpannableStringBuilder append(final CharSequence charSequence, final int n, final int n2) {
        super.append(charSequence, n, n2);
        return this;
    }
    
    public SpannableStringBuilder append(final CharSequence charSequence, final Object o, final int n) {
        super.append(charSequence, o, n);
        return this;
    }
    
    public void beginBatchEdit() {
        this.blockWatchers();
    }
    
    public SpannableStringBuilder delete(final int n, final int n2) {
        super.delete(n, n2);
        return this;
    }
    
    public void endBatchEdit() {
        this.unblockwatchers();
        this.fireWatchers();
    }
    
    public int getSpanEnd(final Object o) {
        Object o2 = o;
        if (this.isWatcher(o)) {
            final WatcherWrapper watcher = this.getWatcherFor(o);
            o2 = o;
            if (watcher != null) {
                o2 = watcher;
            }
        }
        return super.getSpanEnd(o2);
    }
    
    public int getSpanFlags(final Object o) {
        Object o2 = o;
        if (this.isWatcher(o)) {
            final WatcherWrapper watcher = this.getWatcherFor(o);
            o2 = o;
            if (watcher != null) {
                o2 = watcher;
            }
        }
        return super.getSpanFlags(o2);
    }
    
    public int getSpanStart(final Object o) {
        Object o2 = o;
        if (this.isWatcher(o)) {
            final WatcherWrapper watcher = this.getWatcherFor(o);
            o2 = o;
            if (watcher != null) {
                o2 = watcher;
            }
        }
        return super.getSpanStart(o2);
    }
    
    public <T> T[] getSpans(int i, final int n, final Class<T> componentType) {
        if (this.isWatcher(componentType)) {
            final WatcherWrapper[] array = (WatcherWrapper[])super.getSpans(i, n, (Class)WatcherWrapper.class);
            final Object[] array2 = (Object[])Array.newInstance(componentType, array.length);
            for (i = 0; i < array.length; ++i) {
                array2[i] = array[i].mObject;
            }
            return (T[])array2;
        }
        return (T[])super.getSpans(i, n, (Class)componentType);
    }
    
    public SpannableStringBuilder insert(final int n, final CharSequence charSequence) {
        super.insert(n, charSequence);
        return this;
    }
    
    public SpannableStringBuilder insert(final int n, final CharSequence charSequence, final int n2, final int n3) {
        super.insert(n, charSequence, n2, n3);
        return this;
    }
    
    public int nextSpanTransition(final int n, final int n2, final Class clazz) {
        if (clazz != null) {
            final Class<WatcherWrapper> clazz2 = clazz;
            if (!this.isWatcher(clazz)) {
                return super.nextSpanTransition(n, n2, (Class)clazz2);
            }
        }
        final Class<WatcherWrapper> clazz2 = WatcherWrapper.class;
        return super.nextSpanTransition(n, n2, (Class)clazz2);
    }
    
    public void removeSpan(Object o) {
        WatcherWrapper watcherWrapper;
        if (this.isWatcher(o)) {
            final WatcherWrapper watcher = this.getWatcherFor(o);
            if ((watcherWrapper = watcher) != null) {
                o = watcher;
                watcherWrapper = watcher;
            }
        }
        else {
            watcherWrapper = null;
        }
        super.removeSpan(o);
        if (watcherWrapper != null) {
            this.mWatchers.remove(watcherWrapper);
        }
    }
    
    public SpannableStringBuilder replace(final int n, final int n2, final CharSequence charSequence) {
        this.blockWatchers();
        super.replace(n, n2, charSequence);
        this.unblockwatchers();
        return this;
    }
    
    public SpannableStringBuilder replace(final int n, final int n2, final CharSequence charSequence, final int n3, final int n4) {
        this.blockWatchers();
        super.replace(n, n2, charSequence, n3, n4);
        this.unblockwatchers();
        return this;
    }
    
    public void setSpan(final Object o, final int n, final int n2, final int n3) {
        Object o2 = o;
        if (this.isWatcher(o)) {
            o2 = new WatcherWrapper(o);
            this.mWatchers.add((WatcherWrapper)o2);
        }
        super.setSpan(o2, n, n2, n3);
    }
    
    public CharSequence subSequence(final int n, final int n2) {
        return (CharSequence)new SpannableBuilder(this.mWatcherClass, (CharSequence)this, n, n2);
    }
    
    private static class WatcherWrapper implements TextWatcher, SpanWatcher
    {
        private final AtomicInteger mBlockCalls;
        final Object mObject;
        
        WatcherWrapper(final Object mObject) {
            this.mBlockCalls = new AtomicInteger(0);
            this.mObject = mObject;
        }
        
        private boolean isEmojiSpan(final Object o) {
            return o instanceof EmojiSpan;
        }
        
        public void afterTextChanged(final Editable editable) {
            ((TextWatcher)this.mObject).afterTextChanged(editable);
        }
        
        public void beforeTextChanged(final CharSequence charSequence, final int n, final int n2, final int n3) {
            ((TextWatcher)this.mObject).beforeTextChanged(charSequence, n, n2, n3);
        }
        
        final void blockCalls() {
            this.mBlockCalls.incrementAndGet();
        }
        
        public void onSpanAdded(final Spannable spannable, final Object o, final int n, final int n2) {
            if (this.mBlockCalls.get() > 0 && this.isEmojiSpan(o)) {
                return;
            }
            ((SpanWatcher)this.mObject).onSpanAdded(spannable, o, n, n2);
        }
        
        public void onSpanChanged(final Spannable spannable, final Object o, int n, final int n2, int n3, final int n4) {
            if (this.mBlockCalls.get() > 0 && this.isEmojiSpan(o)) {
                return;
            }
            if (Build$VERSION.SDK_INT < 28) {
                if (n > n2) {
                    n = 0;
                }
                if (n3 > n4) {
                    n3 = 0;
                }
            }
            ((SpanWatcher)this.mObject).onSpanChanged(spannable, o, n, n2, n3, n4);
        }
        
        public void onSpanRemoved(final Spannable spannable, final Object o, final int n, final int n2) {
            if (this.mBlockCalls.get() > 0 && this.isEmojiSpan(o)) {
                return;
            }
            ((SpanWatcher)this.mObject).onSpanRemoved(spannable, o, n, n2);
        }
        
        public void onTextChanged(final CharSequence charSequence, final int n, final int n2, final int n3) {
            ((TextWatcher)this.mObject).onTextChanged(charSequence, n, n2, n3);
        }
        
        final void unblockCalls() {
            this.mBlockCalls.decrementAndGet();
        }
    }
}

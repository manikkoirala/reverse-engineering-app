// 
// Decompiled by Procyon v0.6.0
// 

package androidx.emoji2.viewsintegration;

import androidx.emoji2.text.EmojiCompat;
import android.text.Editable;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputConnection;
import android.widget.TextView;
import android.view.inputmethod.InputConnectionWrapper;

final class EmojiInputConnection extends InputConnectionWrapper
{
    private final EmojiCompatDeleteHelper mEmojiCompatDeleteHelper;
    private final TextView mTextView;
    
    EmojiInputConnection(final TextView textView, final InputConnection inputConnection, final EditorInfo editorInfo) {
        this(textView, inputConnection, editorInfo, new EmojiCompatDeleteHelper());
    }
    
    EmojiInputConnection(final TextView mTextView, final InputConnection inputConnection, final EditorInfo editorInfo, final EmojiCompatDeleteHelper mEmojiCompatDeleteHelper) {
        super(inputConnection, false);
        this.mTextView = mTextView;
        (this.mEmojiCompatDeleteHelper = mEmojiCompatDeleteHelper).updateEditorInfoAttrs(editorInfo);
    }
    
    private Editable getEditable() {
        return this.mTextView.getEditableText();
    }
    
    public boolean deleteSurroundingText(final int n, final int n2) {
        return this.mEmojiCompatDeleteHelper.handleDeleteSurroundingText((InputConnection)this, this.getEditable(), n, n2, false) || super.deleteSurroundingText(n, n2);
    }
    
    public boolean deleteSurroundingTextInCodePoints(final int n, final int n2) {
        return this.mEmojiCompatDeleteHelper.handleDeleteSurroundingText((InputConnection)this, this.getEditable(), n, n2, true) || super.deleteSurroundingTextInCodePoints(n, n2);
    }
    
    public static class EmojiCompatDeleteHelper
    {
        public boolean handleDeleteSurroundingText(final InputConnection inputConnection, final Editable editable, final int n, final int n2, final boolean b) {
            return EmojiCompat.handleDeleteSurroundingText(inputConnection, editable, n, n2, b);
        }
        
        public void updateEditorInfoAttrs(final EditorInfo editorInfo) {
            if (EmojiCompat.isConfigured()) {
                EmojiCompat.get().updateEditorInfo(editorInfo);
            }
        }
    }
}

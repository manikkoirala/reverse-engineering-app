// 
// Decompiled by Procyon v0.6.0
// 

package androidx.emoji2.viewsintegration;

import androidx.emoji2.text.EmojiCompat;
import android.text.method.PasswordTransformationMethod;
import android.util.SparseArray;
import android.text.method.TransformationMethod;
import android.text.InputFilter;
import android.os.Build$VERSION;
import androidx.core.util.Preconditions;
import android.widget.TextView;

public final class EmojiTextViewHelper
{
    private final HelperInternal mHelper;
    
    public EmojiTextViewHelper(final TextView textView) {
        this(textView, true);
    }
    
    public EmojiTextViewHelper(final TextView textView, final boolean b) {
        Preconditions.checkNotNull(textView, "textView cannot be null");
        if (Build$VERSION.SDK_INT < 19) {
            this.mHelper = new HelperInternal();
        }
        else if (!b) {
            this.mHelper = (HelperInternal)new SkippingHelper19(textView);
        }
        else {
            this.mHelper = (HelperInternal)new HelperInternal19(textView);
        }
    }
    
    public InputFilter[] getFilters(final InputFilter[] array) {
        return this.mHelper.getFilters(array);
    }
    
    public boolean isEnabled() {
        return this.mHelper.isEnabled();
    }
    
    public void setAllCaps(final boolean allCaps) {
        this.mHelper.setAllCaps(allCaps);
    }
    
    public void setEnabled(final boolean enabled) {
        this.mHelper.setEnabled(enabled);
    }
    
    public void updateTransformationMethod() {
        this.mHelper.updateTransformationMethod();
    }
    
    public TransformationMethod wrapTransformationMethod(final TransformationMethod transformationMethod) {
        return this.mHelper.wrapTransformationMethod(transformationMethod);
    }
    
    static class HelperInternal
    {
        InputFilter[] getFilters(final InputFilter[] array) {
            return array;
        }
        
        public boolean isEnabled() {
            return false;
        }
        
        void setAllCaps(final boolean b) {
        }
        
        void setEnabled(final boolean b) {
        }
        
        void updateTransformationMethod() {
        }
        
        TransformationMethod wrapTransformationMethod(final TransformationMethod transformationMethod) {
            return transformationMethod;
        }
    }
    
    private static class HelperInternal19 extends HelperInternal
    {
        private final EmojiInputFilter mEmojiInputFilter;
        private boolean mEnabled;
        private final TextView mTextView;
        
        HelperInternal19(final TextView mTextView) {
            this.mTextView = mTextView;
            this.mEnabled = true;
            this.mEmojiInputFilter = new EmojiInputFilter(mTextView);
        }
        
        private InputFilter[] addEmojiInputFilterIfMissing(final InputFilter[] array) {
            final int length = array.length;
            for (int i = 0; i < length; ++i) {
                if (array[i] == this.mEmojiInputFilter) {
                    return array;
                }
            }
            final InputFilter[] array2 = new InputFilter[array.length + 1];
            System.arraycopy(array, 0, array2, 0, length);
            array2[length] = (InputFilter)this.mEmojiInputFilter;
            return array2;
        }
        
        private SparseArray<InputFilter> getEmojiInputFilterPositionArray(final InputFilter[] array) {
            final SparseArray sparseArray = new SparseArray(1);
            for (int i = 0; i < array.length; ++i) {
                final InputFilter inputFilter = array[i];
                if (inputFilter instanceof EmojiInputFilter) {
                    sparseArray.put(i, (Object)inputFilter);
                }
            }
            return (SparseArray<InputFilter>)sparseArray;
        }
        
        private InputFilter[] removeEmojiInputFilterIfPresent(final InputFilter[] array) {
            final SparseArray<InputFilter> emojiInputFilterPositionArray = this.getEmojiInputFilterPositionArray(array);
            if (emojiInputFilterPositionArray.size() == 0) {
                return array;
            }
            final int length = array.length;
            final InputFilter[] array2 = new InputFilter[array.length - emojiInputFilterPositionArray.size()];
            int i = 0;
            int n = 0;
            while (i < length) {
                int n2 = n;
                if (emojiInputFilterPositionArray.indexOfKey(i) < 0) {
                    array2[n] = array[i];
                    n2 = n + 1;
                }
                ++i;
                n = n2;
            }
            return array2;
        }
        
        private TransformationMethod unwrapForDisabled(final TransformationMethod transformationMethod) {
            TransformationMethod originalTransformationMethod = transformationMethod;
            if (transformationMethod instanceof EmojiTransformationMethod) {
                originalTransformationMethod = ((EmojiTransformationMethod)transformationMethod).getOriginalTransformationMethod();
            }
            return originalTransformationMethod;
        }
        
        private void updateFilters() {
            this.mTextView.setFilters(this.getFilters(this.mTextView.getFilters()));
        }
        
        private TransformationMethod wrapForEnabled(final TransformationMethod transformationMethod) {
            if (transformationMethod instanceof EmojiTransformationMethod) {
                return transformationMethod;
            }
            if (transformationMethod instanceof PasswordTransformationMethod) {
                return transformationMethod;
            }
            return (TransformationMethod)new EmojiTransformationMethod(transformationMethod);
        }
        
        @Override
        InputFilter[] getFilters(final InputFilter[] array) {
            if (!this.mEnabled) {
                return this.removeEmojiInputFilterIfPresent(array);
            }
            return this.addEmojiInputFilterIfMissing(array);
        }
        
        @Override
        public boolean isEnabled() {
            return this.mEnabled;
        }
        
        @Override
        void setAllCaps(final boolean b) {
            if (b) {
                this.updateTransformationMethod();
            }
        }
        
        @Override
        void setEnabled(final boolean mEnabled) {
            this.mEnabled = mEnabled;
            this.updateTransformationMethod();
            this.updateFilters();
        }
        
        void setEnabledUnsafe(final boolean mEnabled) {
            this.mEnabled = mEnabled;
        }
        
        @Override
        void updateTransformationMethod() {
            this.mTextView.setTransformationMethod(this.wrapTransformationMethod(this.mTextView.getTransformationMethod()));
        }
        
        @Override
        TransformationMethod wrapTransformationMethod(final TransformationMethod transformationMethod) {
            if (this.mEnabled) {
                return this.wrapForEnabled(transformationMethod);
            }
            return this.unwrapForDisabled(transformationMethod);
        }
    }
    
    private static class SkippingHelper19 extends HelperInternal
    {
        private final HelperInternal19 mHelperDelegate;
        
        SkippingHelper19(final TextView textView) {
            this.mHelperDelegate = new HelperInternal19(textView);
        }
        
        private boolean skipBecauseEmojiCompatNotInitialized() {
            return EmojiCompat.isConfigured() ^ true;
        }
        
        @Override
        InputFilter[] getFilters(final InputFilter[] array) {
            if (this.skipBecauseEmojiCompatNotInitialized()) {
                return array;
            }
            return this.mHelperDelegate.getFilters(array);
        }
        
        @Override
        public boolean isEnabled() {
            return this.mHelperDelegate.isEnabled();
        }
        
        @Override
        void setAllCaps(final boolean allCaps) {
            if (this.skipBecauseEmojiCompatNotInitialized()) {
                return;
            }
            this.mHelperDelegate.setAllCaps(allCaps);
        }
        
        @Override
        void setEnabled(final boolean b) {
            if (this.skipBecauseEmojiCompatNotInitialized()) {
                this.mHelperDelegate.setEnabledUnsafe(b);
            }
            else {
                this.mHelperDelegate.setEnabled(b);
            }
        }
        
        @Override
        void updateTransformationMethod() {
            if (this.skipBecauseEmojiCompatNotInitialized()) {
                return;
            }
            this.mHelperDelegate.updateTransformationMethod();
        }
        
        @Override
        TransformationMethod wrapTransformationMethod(final TransformationMethod transformationMethod) {
            if (this.skipBecauseEmojiCompatNotInitialized()) {
                return transformationMethod;
            }
            return this.mHelperDelegate.wrapTransformationMethod(transformationMethod);
        }
    }
}

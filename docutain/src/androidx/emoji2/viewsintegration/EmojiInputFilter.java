// 
// Decompiled by Procyon v0.6.0
// 

package androidx.emoji2.viewsintegration;

import android.os.Handler;
import java.lang.ref.WeakReference;
import java.lang.ref.Reference;
import android.text.Spanned;
import android.text.Selection;
import android.text.Spannable;
import android.widget.TextView;
import androidx.emoji2.text.EmojiCompat;
import android.text.InputFilter;

final class EmojiInputFilter implements InputFilter
{
    private EmojiCompat.InitCallback mInitCallback;
    private final TextView mTextView;
    
    EmojiInputFilter(final TextView mTextView) {
        this.mTextView = mTextView;
    }
    
    static void updateSelection(final Spannable spannable, final int n, final int n2) {
        if (n >= 0 && n2 >= 0) {
            Selection.setSelection(spannable, n, n2);
        }
        else if (n >= 0) {
            Selection.setSelection(spannable, n);
        }
        else if (n2 >= 0) {
            Selection.setSelection(spannable, n2);
        }
    }
    
    public CharSequence filter(CharSequence subSequence, final int n, final int n2, final Spanned spanned, final int n3, final int n4) {
        if (this.mTextView.isInEditMode()) {
            return subSequence;
        }
        final int loadState = EmojiCompat.get().getLoadState();
        if (loadState != 0) {
            final int n5 = 1;
            if (loadState == 1) {
                int n6 = n5;
                if (n4 == 0) {
                    n6 = n5;
                    if (n3 == 0) {
                        n6 = n5;
                        if (spanned.length() == 0) {
                            n6 = n5;
                            if (subSequence == this.mTextView.getText()) {
                                n6 = 0;
                            }
                        }
                    }
                }
                CharSequence process = subSequence;
                if (n6 != 0 && (process = subSequence) != null) {
                    if (n != 0 || n2 != subSequence.length()) {
                        subSequence = subSequence.subSequence(n, n2);
                    }
                    process = EmojiCompat.get().process(subSequence, 0, subSequence.length());
                }
                return process;
            }
            if (loadState != 3) {
                return subSequence;
            }
        }
        EmojiCompat.get().registerInitCallback(this.getInitCallback());
        return subSequence;
    }
    
    EmojiCompat.InitCallback getInitCallback() {
        if (this.mInitCallback == null) {
            this.mInitCallback = new InitCallbackImpl(this.mTextView, this);
        }
        return this.mInitCallback;
    }
    
    static class InitCallbackImpl extends InitCallback implements Runnable
    {
        private final Reference<EmojiInputFilter> mEmojiInputFilterReference;
        private final Reference<TextView> mViewRef;
        
        InitCallbackImpl(final TextView referent, final EmojiInputFilter referent2) {
            this.mViewRef = new WeakReference<TextView>(referent);
            this.mEmojiInputFilterReference = new WeakReference<EmojiInputFilter>(referent2);
        }
        
        private boolean isInputFilterCurrentlyRegisteredOnTextView(final TextView textView, final InputFilter inputFilter) {
            if (inputFilter != null) {
                if (textView != null) {
                    final InputFilter[] filters = textView.getFilters();
                    if (filters == null) {
                        return false;
                    }
                    for (int i = 0; i < filters.length; ++i) {
                        if (filters[i] == inputFilter) {
                            return true;
                        }
                    }
                }
            }
            return false;
        }
        
        @Override
        public void onInitialized() {
            super.onInitialized();
            final TextView textView = this.mViewRef.get();
            if (textView == null) {
                return;
            }
            final Handler handler = textView.getHandler();
            if (handler != null) {
                handler.post((Runnable)this);
            }
        }
        
        @Override
        public void run() {
            final TextView textView = this.mViewRef.get();
            if (!this.isInputFilterCurrentlyRegisteredOnTextView(textView, (InputFilter)this.mEmojiInputFilterReference.get())) {
                return;
            }
            if (textView.isAttachedToWindow()) {
                final CharSequence text = textView.getText();
                final CharSequence process = EmojiCompat.get().process(text);
                if (text == process) {
                    return;
                }
                final int selectionStart = Selection.getSelectionStart(process);
                final int selectionEnd = Selection.getSelectionEnd(process);
                textView.setText(process);
                if (process instanceof Spannable) {
                    EmojiInputFilter.updateSelection((Spannable)process, selectionStart, selectionEnd);
                }
            }
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.emoji2.viewsintegration;

import android.os.Handler;
import java.lang.ref.WeakReference;
import java.lang.ref.Reference;
import android.text.Editable;
import android.text.Spannable;
import android.text.Selection;
import androidx.emoji2.text.EmojiCompat;
import android.widget.EditText;
import android.text.TextWatcher;

final class EmojiTextWatcher implements TextWatcher
{
    private final EditText mEditText;
    private int mEmojiReplaceStrategy;
    private boolean mEnabled;
    private final boolean mExpectInitializedEmojiCompat;
    private EmojiCompat.InitCallback mInitCallback;
    private int mMaxEmojiCount;
    
    EmojiTextWatcher(final EditText mEditText, final boolean mExpectInitializedEmojiCompat) {
        this.mMaxEmojiCount = Integer.MAX_VALUE;
        this.mEmojiReplaceStrategy = 0;
        this.mEditText = mEditText;
        this.mExpectInitializedEmojiCompat = mExpectInitializedEmojiCompat;
        this.mEnabled = true;
    }
    
    static void processTextOnEnablingEvent(final EditText editText, int selectionStart) {
        if (selectionStart == 1 && editText != null && editText.isAttachedToWindow()) {
            final Editable editableText = editText.getEditableText();
            selectionStart = Selection.getSelectionStart((CharSequence)editableText);
            final int selectionEnd = Selection.getSelectionEnd((CharSequence)editableText);
            EmojiCompat.get().process((CharSequence)editableText);
            EmojiInputFilter.updateSelection((Spannable)editableText, selectionStart, selectionEnd);
        }
    }
    
    private boolean shouldSkipForDisabledOrNotConfigured() {
        return !this.mEnabled || (!this.mExpectInitializedEmojiCompat && !EmojiCompat.isConfigured());
    }
    
    public void afterTextChanged(final Editable editable) {
    }
    
    public void beforeTextChanged(final CharSequence charSequence, final int n, final int n2, final int n3) {
    }
    
    int getEmojiReplaceStrategy() {
        return this.mEmojiReplaceStrategy;
    }
    
    EmojiCompat.InitCallback getInitCallback() {
        if (this.mInitCallback == null) {
            this.mInitCallback = new InitCallbackImpl(this.mEditText);
        }
        return this.mInitCallback;
    }
    
    int getMaxEmojiCount() {
        return this.mMaxEmojiCount;
    }
    
    public boolean isEnabled() {
        return this.mEnabled;
    }
    
    public void onTextChanged(final CharSequence charSequence, final int n, int loadState, final int n2) {
        if (!this.mEditText.isInEditMode()) {
            if (!this.shouldSkipForDisabledOrNotConfigured()) {
                if (loadState <= n2 && charSequence instanceof Spannable) {
                    loadState = EmojiCompat.get().getLoadState();
                    if (loadState != 0) {
                        if (loadState == 1) {
                            EmojiCompat.get().process(charSequence, n, n + n2, this.mMaxEmojiCount, this.mEmojiReplaceStrategy);
                            return;
                        }
                        if (loadState != 3) {
                            return;
                        }
                    }
                    EmojiCompat.get().registerInitCallback(this.getInitCallback());
                }
            }
        }
    }
    
    void setEmojiReplaceStrategy(final int mEmojiReplaceStrategy) {
        this.mEmojiReplaceStrategy = mEmojiReplaceStrategy;
    }
    
    public void setEnabled(final boolean mEnabled) {
        if (this.mEnabled != mEnabled) {
            if (this.mInitCallback != null) {
                EmojiCompat.get().unregisterInitCallback(this.mInitCallback);
            }
            this.mEnabled = mEnabled;
            if (mEnabled) {
                processTextOnEnablingEvent(this.mEditText, EmojiCompat.get().getLoadState());
            }
        }
    }
    
    void setMaxEmojiCount(final int mMaxEmojiCount) {
        this.mMaxEmojiCount = mMaxEmojiCount;
    }
    
    static class InitCallbackImpl extends InitCallback implements Runnable
    {
        private final Reference<EditText> mViewRef;
        
        InitCallbackImpl(final EditText referent) {
            this.mViewRef = new WeakReference<EditText>(referent);
        }
        
        @Override
        public void onInitialized() {
            super.onInitialized();
            final EditText editText = this.mViewRef.get();
            if (editText == null) {
                return;
            }
            final Handler handler = editText.getHandler();
            if (handler == null) {
                return;
            }
            handler.post((Runnable)this);
        }
        
        @Override
        public void run() {
            EmojiTextWatcher.processTextOnEnablingEvent(this.mViewRef.get(), 1);
        }
    }
}

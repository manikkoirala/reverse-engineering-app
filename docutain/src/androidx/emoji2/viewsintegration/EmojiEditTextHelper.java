// 
// Decompiled by Procyon v0.6.0
// 

package androidx.emoji2.viewsintegration;

import android.widget.TextView;
import android.text.method.NumberKeyListener;
import android.text.TextWatcher;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputConnection;
import android.text.method.KeyListener;
import android.os.Build$VERSION;
import androidx.core.util.Preconditions;
import android.widget.EditText;

public final class EmojiEditTextHelper
{
    private int mEmojiReplaceStrategy;
    private final HelperInternal mHelper;
    private int mMaxEmojiCount;
    
    public EmojiEditTextHelper(final EditText editText) {
        this(editText, true);
    }
    
    public EmojiEditTextHelper(final EditText editText, final boolean b) {
        this.mMaxEmojiCount = Integer.MAX_VALUE;
        this.mEmojiReplaceStrategy = 0;
        Preconditions.checkNotNull(editText, "editText cannot be null");
        if (Build$VERSION.SDK_INT < 19) {
            this.mHelper = new HelperInternal();
        }
        else {
            this.mHelper = (HelperInternal)new HelperInternal19(editText, b);
        }
    }
    
    public int getEmojiReplaceStrategy() {
        return this.mEmojiReplaceStrategy;
    }
    
    public KeyListener getKeyListener(final KeyListener keyListener) {
        return this.mHelper.getKeyListener(keyListener);
    }
    
    public int getMaxEmojiCount() {
        return this.mMaxEmojiCount;
    }
    
    public boolean isEnabled() {
        return this.mHelper.isEnabled();
    }
    
    public InputConnection onCreateInputConnection(final InputConnection inputConnection, final EditorInfo editorInfo) {
        if (inputConnection == null) {
            return null;
        }
        return this.mHelper.onCreateInputConnection(inputConnection, editorInfo);
    }
    
    public void setEmojiReplaceStrategy(final int n) {
        this.mEmojiReplaceStrategy = n;
        this.mHelper.setEmojiReplaceStrategy(n);
    }
    
    public void setEnabled(final boolean enabled) {
        this.mHelper.setEnabled(enabled);
    }
    
    public void setMaxEmojiCount(final int n) {
        Preconditions.checkArgumentNonnegative(n, "maxEmojiCount should be greater than 0");
        this.mMaxEmojiCount = n;
        this.mHelper.setMaxEmojiCount(n);
    }
    
    static class HelperInternal
    {
        KeyListener getKeyListener(final KeyListener keyListener) {
            return keyListener;
        }
        
        boolean isEnabled() {
            return false;
        }
        
        InputConnection onCreateInputConnection(final InputConnection inputConnection, final EditorInfo editorInfo) {
            return inputConnection;
        }
        
        void setEmojiReplaceStrategy(final int n) {
        }
        
        void setEnabled(final boolean b) {
        }
        
        void setMaxEmojiCount(final int n) {
        }
    }
    
    private static class HelperInternal19 extends HelperInternal
    {
        private final EditText mEditText;
        private final EmojiTextWatcher mTextWatcher;
        
        HelperInternal19(final EditText mEditText, final boolean b) {
            (this.mEditText = mEditText).addTextChangedListener((TextWatcher)(this.mTextWatcher = new EmojiTextWatcher(mEditText, b)));
            mEditText.setEditableFactory(EmojiEditableFactory.getInstance());
        }
        
        @Override
        KeyListener getKeyListener(final KeyListener keyListener) {
            if (keyListener instanceof EmojiKeyListener) {
                return keyListener;
            }
            if (keyListener == null) {
                return null;
            }
            if (keyListener instanceof NumberKeyListener) {
                return keyListener;
            }
            return (KeyListener)new EmojiKeyListener(keyListener);
        }
        
        @Override
        boolean isEnabled() {
            return this.mTextWatcher.isEnabled();
        }
        
        @Override
        InputConnection onCreateInputConnection(final InputConnection inputConnection, final EditorInfo editorInfo) {
            if (inputConnection instanceof EmojiInputConnection) {
                return inputConnection;
            }
            return (InputConnection)new EmojiInputConnection((TextView)this.mEditText, inputConnection, editorInfo);
        }
        
        @Override
        void setEmojiReplaceStrategy(final int emojiReplaceStrategy) {
            this.mTextWatcher.setEmojiReplaceStrategy(emojiReplaceStrategy);
        }
        
        @Override
        void setEnabled(final boolean enabled) {
            this.mTextWatcher.setEnabled(enabled);
        }
        
        @Override
        void setMaxEmojiCount(final int maxEmojiCount) {
            this.mTextWatcher.setMaxEmojiCount(maxEmojiCount);
        }
    }
}

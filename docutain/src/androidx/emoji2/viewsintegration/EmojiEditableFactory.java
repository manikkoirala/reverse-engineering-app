// 
// Decompiled by Procyon v0.6.0
// 

package androidx.emoji2.viewsintegration;

import androidx.emoji2.text.SpannableBuilder;
import android.text.Editable;
import android.text.Editable$Factory;

final class EmojiEditableFactory extends Editable$Factory
{
    private static final Object INSTANCE_LOCK;
    private static volatile Editable$Factory sInstance;
    private static Class<?> sWatcherClass;
    
    static {
        INSTANCE_LOCK = new Object();
    }
    
    private EmojiEditableFactory() {
        try {
            EmojiEditableFactory.sWatcherClass = Class.forName("android.text.DynamicLayout$ChangeWatcher", false, this.getClass().getClassLoader());
        }
        finally {}
    }
    
    public static Editable$Factory getInstance() {
        if (EmojiEditableFactory.sInstance == null) {
            synchronized (EmojiEditableFactory.INSTANCE_LOCK) {
                if (EmojiEditableFactory.sInstance == null) {
                    EmojiEditableFactory.sInstance = new EmojiEditableFactory();
                }
            }
        }
        return EmojiEditableFactory.sInstance;
    }
    
    public Editable newEditable(final CharSequence charSequence) {
        final Class<?> sWatcherClass = EmojiEditableFactory.sWatcherClass;
        if (sWatcherClass != null) {
            return (Editable)SpannableBuilder.create(sWatcherClass, charSequence);
        }
        return super.newEditable(charSequence);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.coordinatorlayout;

public final class R
{
    public static final class attr
    {
        public static final int coordinatorLayoutStyle = 2130968923;
        public static final int keylines = 2130969231;
        public static final int layout_anchor = 2130969243;
        public static final int layout_anchorGravity = 2130969244;
        public static final int layout_behavior = 2130969245;
        public static final int layout_dodgeInsetEdges = 2130969297;
        public static final int layout_insetEdge = 2130969308;
        public static final int layout_keyline = 2130969309;
        public static final int statusBarBackground = 2130970068;
    }
    
    public static final class id
    {
        public static final int bottom = 2131361954;
        public static final int end = 2131362085;
        public static final int left = 2131362180;
        public static final int none = 2131362285;
        public static final int right = 2131362944;
        public static final int start = 2131363037;
        public static final int top = 2131363106;
    }
    
    public static final class style
    {
        public static final int Widget_Support_CoordinatorLayout = 2131952908;
    }
    
    public static final class styleable
    {
        public static final int[] CoordinatorLayout;
        public static final int[] CoordinatorLayout_Layout;
        public static final int CoordinatorLayout_Layout_android_layout_gravity = 0;
        public static final int CoordinatorLayout_Layout_layout_anchor = 1;
        public static final int CoordinatorLayout_Layout_layout_anchorGravity = 2;
        public static final int CoordinatorLayout_Layout_layout_behavior = 3;
        public static final int CoordinatorLayout_Layout_layout_dodgeInsetEdges = 4;
        public static final int CoordinatorLayout_Layout_layout_insetEdge = 5;
        public static final int CoordinatorLayout_Layout_layout_keyline = 6;
        public static final int CoordinatorLayout_keylines = 0;
        public static final int CoordinatorLayout_statusBarBackground = 1;
        
        static {
            CoordinatorLayout = new int[] { 2130969231, 2130970068 };
            CoordinatorLayout_Layout = new int[] { 16842931, 2130969243, 2130969244, 2130969245, 2130969297, 2130969308, 2130969309 };
        }
    }
}

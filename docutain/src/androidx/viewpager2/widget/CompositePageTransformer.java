// 
// Decompiled by Procyon v0.6.0
// 

package androidx.viewpager2.widget;

import java.util.Iterator;
import android.view.View;
import java.util.ArrayList;
import java.util.List;

public final class CompositePageTransformer implements PageTransformer
{
    private final List<PageTransformer> mTransformers;
    
    public CompositePageTransformer() {
        this.mTransformers = new ArrayList<PageTransformer>();
    }
    
    public void addTransformer(final PageTransformer pageTransformer) {
        this.mTransformers.add(pageTransformer);
    }
    
    public void removeTransformer(final PageTransformer pageTransformer) {
        this.mTransformers.remove(pageTransformer);
    }
    
    @Override
    public void transformPage(final View view, final float n) {
        final Iterator<PageTransformer> iterator = this.mTransformers.iterator();
        while (iterator.hasNext()) {
            iterator.next().transformPage(view, n);
        }
    }
}

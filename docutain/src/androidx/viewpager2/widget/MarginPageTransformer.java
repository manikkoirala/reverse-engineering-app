// 
// Decompiled by Procyon v0.6.0
// 

package androidx.viewpager2.widget;

import android.view.ViewParent;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import androidx.core.util.Preconditions;

public final class MarginPageTransformer implements PageTransformer
{
    private final int mMarginPx;
    
    public MarginPageTransformer(final int mMarginPx) {
        Preconditions.checkArgumentNonnegative(mMarginPx, "Margin must be non-negative");
        this.mMarginPx = mMarginPx;
    }
    
    private ViewPager2 requireViewPager(final View view) {
        final ViewParent parent = view.getParent();
        final ViewParent parent2 = parent.getParent();
        if (parent instanceof RecyclerView && parent2 instanceof ViewPager2) {
            return (ViewPager2)parent2;
        }
        throw new IllegalStateException("Expected the page view to be managed by a ViewPager2 instance.");
    }
    
    @Override
    public void transformPage(final View view, float translationX) {
        final ViewPager2 requireViewPager = this.requireViewPager(view);
        final float translationY = this.mMarginPx * translationX;
        if (requireViewPager.getOrientation() == 0) {
            translationX = translationY;
            if (requireViewPager.isRtl()) {
                translationX = -translationY;
            }
            view.setTranslationX(translationX);
        }
        else {
            view.setTranslationY(translationY);
        }
    }
}

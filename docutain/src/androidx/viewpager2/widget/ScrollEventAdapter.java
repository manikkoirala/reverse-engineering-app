// 
// Decompiled by Procyon v0.6.0
// 

package androidx.viewpager2.widget;

import android.view.ViewGroup$LayoutParams;
import android.view.View;
import java.util.Locale;
import android.view.ViewGroup$MarginLayoutParams;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

final class ScrollEventAdapter extends OnScrollListener
{
    private static final int NO_POSITION = -1;
    private static final int STATE_IDLE = 0;
    private static final int STATE_IN_PROGRESS_FAKE_DRAG = 4;
    private static final int STATE_IN_PROGRESS_IMMEDIATE_SCROLL = 3;
    private static final int STATE_IN_PROGRESS_MANUAL_DRAG = 1;
    private static final int STATE_IN_PROGRESS_SMOOTH_SCROLL = 2;
    private int mAdapterState;
    private ViewPager2.OnPageChangeCallback mCallback;
    private boolean mDataSetChangeHappened;
    private boolean mDispatchSelected;
    private int mDragStartPosition;
    private boolean mFakeDragging;
    private final LinearLayoutManager mLayoutManager;
    private final RecyclerView mRecyclerView;
    private boolean mScrollHappened;
    private int mScrollState;
    private ScrollEventValues mScrollValues;
    private int mTarget;
    private final ViewPager2 mViewPager;
    
    ScrollEventAdapter(final ViewPager2 mViewPager) {
        this.mViewPager = mViewPager;
        final RecyclerView mRecyclerView = mViewPager.mRecyclerView;
        this.mRecyclerView = mRecyclerView;
        this.mLayoutManager = (LinearLayoutManager)mRecyclerView.getLayoutManager();
        this.mScrollValues = new ScrollEventValues();
        this.resetState();
    }
    
    private void dispatchScrolled(final int n, final float n2, final int n3) {
        final ViewPager2.OnPageChangeCallback mCallback = this.mCallback;
        if (mCallback != null) {
            mCallback.onPageScrolled(n, n2, n3);
        }
    }
    
    private void dispatchSelected(final int n) {
        final ViewPager2.OnPageChangeCallback mCallback = this.mCallback;
        if (mCallback != null) {
            mCallback.onPageSelected(n);
        }
    }
    
    private void dispatchStateChanged(final int mScrollState) {
        if (this.mAdapterState == 3 && this.mScrollState == 0) {
            return;
        }
        if (this.mScrollState == mScrollState) {
            return;
        }
        this.mScrollState = mScrollState;
        final ViewPager2.OnPageChangeCallback mCallback = this.mCallback;
        if (mCallback != null) {
            mCallback.onPageScrollStateChanged(mScrollState);
        }
    }
    
    private int getPosition() {
        return this.mLayoutManager.findFirstVisibleItemPosition();
    }
    
    private boolean isInAnyDraggingState() {
        final int mAdapterState = this.mAdapterState;
        boolean b = true;
        if (mAdapterState != 1) {
            b = (mAdapterState == 4 && b);
        }
        return b;
    }
    
    private void resetState() {
        this.mAdapterState = 0;
        this.mScrollState = 0;
        this.mScrollValues.reset();
        this.mDragStartPosition = -1;
        this.mTarget = -1;
        this.mDispatchSelected = false;
        this.mScrollHappened = false;
        this.mFakeDragging = false;
        this.mDataSetChangeHappened = false;
    }
    
    private void startDrag(final boolean mFakeDragging) {
        this.mFakeDragging = mFakeDragging;
        int mAdapterState;
        if (mFakeDragging) {
            mAdapterState = 4;
        }
        else {
            mAdapterState = 1;
        }
        this.mAdapterState = mAdapterState;
        final int mTarget = this.mTarget;
        if (mTarget != -1) {
            this.mDragStartPosition = mTarget;
            this.mTarget = -1;
        }
        else if (this.mDragStartPosition == -1) {
            this.mDragStartPosition = this.getPosition();
        }
        this.dispatchStateChanged(1);
    }
    
    private void updateScrollEventValues() {
        final ScrollEventValues mScrollValues = this.mScrollValues;
        mScrollValues.mPosition = this.mLayoutManager.findFirstVisibleItemPosition();
        if (mScrollValues.mPosition == -1) {
            mScrollValues.reset();
            return;
        }
        final View viewByPosition = this.mLayoutManager.findViewByPosition(mScrollValues.mPosition);
        if (viewByPosition == null) {
            mScrollValues.reset();
            return;
        }
        final int leftDecorationWidth = ((RecyclerView.LayoutManager)this.mLayoutManager).getLeftDecorationWidth(viewByPosition);
        final int rightDecorationWidth = ((RecyclerView.LayoutManager)this.mLayoutManager).getRightDecorationWidth(viewByPosition);
        final int topDecorationHeight = ((RecyclerView.LayoutManager)this.mLayoutManager).getTopDecorationHeight(viewByPosition);
        final int bottomDecorationHeight = ((RecyclerView.LayoutManager)this.mLayoutManager).getBottomDecorationHeight(viewByPosition);
        final ViewGroup$LayoutParams layoutParams = viewByPosition.getLayoutParams();
        int n = leftDecorationWidth;
        int n2 = rightDecorationWidth;
        int n3 = topDecorationHeight;
        int n4 = bottomDecorationHeight;
        if (layoutParams instanceof ViewGroup$MarginLayoutParams) {
            final ViewGroup$MarginLayoutParams viewGroup$MarginLayoutParams = (ViewGroup$MarginLayoutParams)layoutParams;
            n = leftDecorationWidth + viewGroup$MarginLayoutParams.leftMargin;
            n2 = rightDecorationWidth + viewGroup$MarginLayoutParams.rightMargin;
            n3 = topDecorationHeight + viewGroup$MarginLayoutParams.topMargin;
            n4 = bottomDecorationHeight + viewGroup$MarginLayoutParams.bottomMargin;
        }
        final int n5 = viewByPosition.getHeight() + n3 + n4;
        final int width = viewByPosition.getWidth();
        int n6;
        int n7;
        if (this.mLayoutManager.getOrientation() == 0) {
            n6 = viewByPosition.getLeft() - n - this.mRecyclerView.getPaddingLeft();
            if (this.mViewPager.isRtl()) {
                n6 = -n6;
            }
            n7 = width + n + n2;
        }
        else {
            n6 = viewByPosition.getTop() - n3 - this.mRecyclerView.getPaddingTop();
            n7 = n5;
        }
        mScrollValues.mOffsetPx = -n6;
        if (mScrollValues.mOffsetPx >= 0) {
            float mOffset;
            if (n7 == 0) {
                mOffset = 0.0f;
            }
            else {
                mOffset = mScrollValues.mOffsetPx / (float)n7;
            }
            mScrollValues.mOffset = mOffset;
            return;
        }
        if (new AnimateLayoutChangeDetector(this.mLayoutManager).mayHaveInterferingAnimations()) {
            throw new IllegalStateException("Page(s) contain a ViewGroup with a LayoutTransition (or animateLayoutChanges=\"true\"), which interferes with the scrolling animation. Make sure to call getLayoutTransition().setAnimateParentHierarchy(false) on all ViewGroups with a LayoutTransition before an animation is started.");
        }
        throw new IllegalStateException(String.format(Locale.US, "Page can only be offset by a positive amount, not by %d", mScrollValues.mOffsetPx));
    }
    
    double getRelativeScrollPosition() {
        this.updateScrollEventValues();
        return this.mScrollValues.mPosition + (double)this.mScrollValues.mOffset;
    }
    
    int getScrollState() {
        return this.mScrollState;
    }
    
    boolean isDragging() {
        final int mScrollState = this.mScrollState;
        boolean b = true;
        if (mScrollState != 1) {
            b = false;
        }
        return b;
    }
    
    boolean isFakeDragging() {
        return this.mFakeDragging;
    }
    
    boolean isIdle() {
        return this.mScrollState == 0;
    }
    
    void notifyBeginFakeDrag() {
        this.mAdapterState = 4;
        this.startDrag(true);
    }
    
    void notifyDataSetChangeHappened() {
        this.mDataSetChangeHappened = true;
    }
    
    void notifyEndFakeDrag() {
        if (this.isDragging() && !this.mFakeDragging) {
            return;
        }
        this.mFakeDragging = false;
        this.updateScrollEventValues();
        if (this.mScrollValues.mOffsetPx == 0) {
            if (this.mScrollValues.mPosition != this.mDragStartPosition) {
                this.dispatchSelected(this.mScrollValues.mPosition);
            }
            this.dispatchStateChanged(0);
            this.resetState();
        }
        else {
            this.dispatchStateChanged(2);
        }
    }
    
    void notifyProgrammaticScroll(final int mTarget, final boolean b) {
        int mAdapterState;
        if (b) {
            mAdapterState = 2;
        }
        else {
            mAdapterState = 3;
        }
        this.mAdapterState = mAdapterState;
        boolean b2 = false;
        this.mFakeDragging = false;
        if (this.mTarget != mTarget) {
            b2 = true;
        }
        this.mTarget = mTarget;
        this.dispatchStateChanged(2);
        if (b2) {
            this.dispatchSelected(mTarget);
        }
    }
    
    @Override
    public void onScrollStateChanged(final RecyclerView recyclerView, int mPosition) {
        final int mAdapterState = this.mAdapterState;
        final boolean b = true;
        if ((mAdapterState != 1 || this.mScrollState != 1) && mPosition == 1) {
            this.startDrag(false);
            return;
        }
        if (this.isInAnyDraggingState() && mPosition == 2) {
            if (this.mScrollHappened) {
                this.dispatchStateChanged(2);
                this.mDispatchSelected = true;
            }
            return;
        }
        if (this.isInAnyDraggingState() && mPosition == 0) {
            this.updateScrollEventValues();
            int n;
            if (!this.mScrollHappened) {
                n = (b ? 1 : 0);
                if (this.mScrollValues.mPosition != -1) {
                    this.dispatchScrolled(this.mScrollValues.mPosition, 0.0f, 0);
                    n = (b ? 1 : 0);
                }
            }
            else if (this.mScrollValues.mOffsetPx == 0) {
                n = (b ? 1 : 0);
                if (this.mDragStartPosition != this.mScrollValues.mPosition) {
                    this.dispatchSelected(this.mScrollValues.mPosition);
                    n = (b ? 1 : 0);
                }
            }
            else {
                n = 0;
            }
            if (n != 0) {
                this.dispatchStateChanged(0);
                this.resetState();
            }
        }
        if (this.mAdapterState == 2 && mPosition == 0 && this.mDataSetChangeHappened) {
            this.updateScrollEventValues();
            if (this.mScrollValues.mOffsetPx == 0) {
                if (this.mTarget != this.mScrollValues.mPosition) {
                    if (this.mScrollValues.mPosition == -1) {
                        mPosition = 0;
                    }
                    else {
                        mPosition = this.mScrollValues.mPosition;
                    }
                    this.dispatchSelected(mPosition);
                }
                this.dispatchStateChanged(0);
                this.resetState();
            }
        }
    }
    
    @Override
    public void onScrolled(final RecyclerView recyclerView, int mTarget, int n) {
        this.mScrollHappened = true;
        this.updateScrollEventValues();
        if (this.mDispatchSelected) {
            this.mDispatchSelected = false;
            if (n <= 0 && (n != 0 || mTarget < 0 != this.mViewPager.isRtl())) {
                mTarget = 0;
            }
            else {
                mTarget = 1;
            }
            if (mTarget != 0 && this.mScrollValues.mOffsetPx != 0) {
                mTarget = this.mScrollValues.mPosition + 1;
            }
            else {
                mTarget = this.mScrollValues.mPosition;
            }
            this.mTarget = mTarget;
            if (this.mDragStartPosition != mTarget) {
                this.dispatchSelected(mTarget);
            }
        }
        else if (this.mAdapterState == 0) {
            n = this.mScrollValues.mPosition;
            if ((mTarget = n) == -1) {
                mTarget = 0;
            }
            this.dispatchSelected(mTarget);
        }
        if (this.mScrollValues.mPosition == -1) {
            mTarget = 0;
        }
        else {
            mTarget = this.mScrollValues.mPosition;
        }
        this.dispatchScrolled(mTarget, this.mScrollValues.mOffset, this.mScrollValues.mOffsetPx);
        n = this.mScrollValues.mPosition;
        mTarget = this.mTarget;
        if ((n == mTarget || mTarget == -1) && this.mScrollValues.mOffsetPx == 0 && this.mScrollState != 1) {
            this.dispatchStateChanged(0);
            this.resetState();
        }
    }
    
    void setOnPageChangeCallback(final ViewPager2.OnPageChangeCallback mCallback) {
        this.mCallback = mCallback;
    }
    
    private static final class ScrollEventValues
    {
        float mOffset;
        int mOffsetPx;
        int mPosition;
        
        ScrollEventValues() {
        }
        
        void reset() {
            this.mPosition = -1;
            this.mOffset = 0.0f;
            this.mOffsetPx = 0;
        }
    }
}

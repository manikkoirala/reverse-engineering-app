// 
// Decompiled by Procyon v0.6.0
// 

package androidx.viewpager2.widget;

import android.os.SystemClock;
import android.view.ViewConfiguration;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import androidx.recyclerview.widget.RecyclerView;

final class FakeDrag
{
    private int mActualDraggedDistance;
    private long mFakeDragBeginTime;
    private int mMaximumVelocity;
    private final RecyclerView mRecyclerView;
    private float mRequestedDragDistance;
    private final ScrollEventAdapter mScrollEventAdapter;
    private VelocityTracker mVelocityTracker;
    private final ViewPager2 mViewPager;
    
    FakeDrag(final ViewPager2 mViewPager, final ScrollEventAdapter mScrollEventAdapter, final RecyclerView mRecyclerView) {
        this.mViewPager = mViewPager;
        this.mScrollEventAdapter = mScrollEventAdapter;
        this.mRecyclerView = mRecyclerView;
    }
    
    private void addFakeMotionEvent(final long n, final int n2, final float n3, final float n4) {
        final MotionEvent obtain = MotionEvent.obtain(this.mFakeDragBeginTime, n, n2, n3, n4, 0);
        this.mVelocityTracker.addMovement(obtain);
        obtain.recycle();
    }
    
    private void beginFakeVelocityTracker() {
        final VelocityTracker mVelocityTracker = this.mVelocityTracker;
        if (mVelocityTracker == null) {
            this.mVelocityTracker = VelocityTracker.obtain();
            this.mMaximumVelocity = ViewConfiguration.get(this.mViewPager.getContext()).getScaledMaximumFlingVelocity();
        }
        else {
            mVelocityTracker.clear();
        }
    }
    
    boolean beginFakeDrag() {
        if (this.mScrollEventAdapter.isDragging()) {
            return false;
        }
        this.mActualDraggedDistance = 0;
        this.mRequestedDragDistance = 0;
        this.mFakeDragBeginTime = SystemClock.uptimeMillis();
        this.beginFakeVelocityTracker();
        this.mScrollEventAdapter.notifyBeginFakeDrag();
        if (!this.mScrollEventAdapter.isIdle()) {
            this.mRecyclerView.stopScroll();
        }
        this.addFakeMotionEvent(this.mFakeDragBeginTime, 0, 0.0f, 0.0f);
        return true;
    }
    
    boolean endFakeDrag() {
        if (!this.mScrollEventAdapter.isFakeDragging()) {
            return false;
        }
        this.mScrollEventAdapter.notifyEndFakeDrag();
        final VelocityTracker mVelocityTracker = this.mVelocityTracker;
        mVelocityTracker.computeCurrentVelocity(1000, (float)this.mMaximumVelocity);
        if (!this.mRecyclerView.fling((int)mVelocityTracker.getXVelocity(), (int)mVelocityTracker.getYVelocity())) {
            this.mViewPager.snapToPage();
        }
        return true;
    }
    
    boolean fakeDragBy(float mRequestedDragDistance) {
        final boolean fakeDragging = this.mScrollEventAdapter.isFakeDragging();
        final int n = 0;
        if (!fakeDragging) {
            return false;
        }
        mRequestedDragDistance = this.mRequestedDragDistance - mRequestedDragDistance;
        this.mRequestedDragDistance = mRequestedDragDistance;
        int round = Math.round(mRequestedDragDistance - this.mActualDraggedDistance);
        this.mActualDraggedDistance += round;
        final long uptimeMillis = SystemClock.uptimeMillis();
        final boolean b = this.mViewPager.getOrientation() == 0;
        int n2;
        if (b) {
            n2 = round;
        }
        else {
            n2 = 0;
        }
        if (b) {
            round = n;
        }
        if (b) {
            mRequestedDragDistance = this.mRequestedDragDistance;
        }
        else {
            mRequestedDragDistance = 0.0f;
        }
        float mRequestedDragDistance2;
        if (b) {
            mRequestedDragDistance2 = 0.0f;
        }
        else {
            mRequestedDragDistance2 = this.mRequestedDragDistance;
        }
        this.mRecyclerView.scrollBy(n2, round);
        this.addFakeMotionEvent(uptimeMillis, 2, mRequestedDragDistance, mRequestedDragDistance2);
        return true;
    }
    
    boolean isFakeDragging() {
        return this.mScrollEventAdapter.isFakeDragging();
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.viewpager2.widget;

import android.os.Parcel;
import android.os.Parcelable$ClassLoaderCreator;
import android.os.Parcelable$Creator;
import android.view.View$BaseSavedState;
import android.view.MotionEvent;
import androidx.core.view.accessibility.AccessibilityViewCommand;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;
import android.view.accessibility.AccessibilityEvent;
import androidx.core.view.accessibility.AccessibilityNodeInfoCompat;
import android.os.Bundle;
import android.view.Gravity;
import android.view.accessibility.AccessibilityNodeInfo;
import android.util.SparseArray;
import android.content.res.TypedArray;
import android.os.Build$VERSION;
import androidx.viewpager2.R;
import androidx.viewpager2.adapter.StatefulAdapter;
import android.view.ViewGroup$LayoutParams;
import androidx.core.view.ViewCompat;
import android.view.View;
import android.util.AttributeSet;
import android.content.Context;
import android.graphics.Rect;
import android.os.Parcelable;
import androidx.recyclerview.widget.PagerSnapHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.ViewGroup;

public final class ViewPager2 extends ViewGroup
{
    public static final int OFFSCREEN_PAGE_LIMIT_DEFAULT = -1;
    public static final int ORIENTATION_HORIZONTAL = 0;
    public static final int ORIENTATION_VERTICAL = 1;
    public static final int SCROLL_STATE_DRAGGING = 1;
    public static final int SCROLL_STATE_IDLE = 0;
    public static final int SCROLL_STATE_SETTLING = 2;
    static boolean sFeatureEnhancedA11yEnabled = true;
    AccessibilityProvider mAccessibilityProvider;
    int mCurrentItem;
    private RecyclerView.AdapterDataObserver mCurrentItemDataSetChangeObserver;
    boolean mCurrentItemDirty;
    private CompositeOnPageChangeCallback mExternalPageChangeCallbacks;
    private FakeDrag mFakeDragger;
    private LinearLayoutManager mLayoutManager;
    private int mOffscreenPageLimit;
    private CompositeOnPageChangeCallback mPageChangeEventDispatcher;
    private PageTransformerAdapter mPageTransformerAdapter;
    private PagerSnapHelper mPagerSnapHelper;
    private Parcelable mPendingAdapterState;
    private int mPendingCurrentItem;
    RecyclerView mRecyclerView;
    private RecyclerView.ItemAnimator mSavedItemAnimator;
    private boolean mSavedItemAnimatorPresent;
    ScrollEventAdapter mScrollEventAdapter;
    private final Rect mTmpChildRect;
    private final Rect mTmpContainerRect;
    private boolean mUserInputEnabled;
    
    public ViewPager2(final Context context) {
        super(context);
        this.mTmpContainerRect = new Rect();
        this.mTmpChildRect = new Rect();
        this.mExternalPageChangeCallbacks = new CompositeOnPageChangeCallback(3);
        this.mCurrentItemDirty = false;
        this.mCurrentItemDataSetChangeObserver = new DataSetChangeObserver() {
            final ViewPager2 this$0;
            
            @Override
            public void onChanged() {
                this.this$0.mCurrentItemDirty = true;
                this.this$0.mScrollEventAdapter.notifyDataSetChangeHappened();
            }
        };
        this.mPendingCurrentItem = -1;
        this.mSavedItemAnimator = null;
        this.mSavedItemAnimatorPresent = false;
        this.mUserInputEnabled = true;
        this.mOffscreenPageLimit = -1;
        this.initialize(context, null);
    }
    
    public ViewPager2(final Context context, final AttributeSet set) {
        super(context, set);
        this.mTmpContainerRect = new Rect();
        this.mTmpChildRect = new Rect();
        this.mExternalPageChangeCallbacks = new CompositeOnPageChangeCallback(3);
        this.mCurrentItemDirty = false;
        this.mCurrentItemDataSetChangeObserver = new DataSetChangeObserver() {
            final ViewPager2 this$0;
            
            @Override
            public void onChanged() {
                this.this$0.mCurrentItemDirty = true;
                this.this$0.mScrollEventAdapter.notifyDataSetChangeHappened();
            }
        };
        this.mPendingCurrentItem = -1;
        this.mSavedItemAnimator = null;
        this.mSavedItemAnimatorPresent = false;
        this.mUserInputEnabled = true;
        this.mOffscreenPageLimit = -1;
        this.initialize(context, set);
    }
    
    public ViewPager2(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
        this.mTmpContainerRect = new Rect();
        this.mTmpChildRect = new Rect();
        this.mExternalPageChangeCallbacks = new CompositeOnPageChangeCallback(3);
        this.mCurrentItemDirty = false;
        this.mCurrentItemDataSetChangeObserver = new DataSetChangeObserver() {
            final ViewPager2 this$0;
            
            @Override
            public void onChanged() {
                this.this$0.mCurrentItemDirty = true;
                this.this$0.mScrollEventAdapter.notifyDataSetChangeHappened();
            }
        };
        this.mPendingCurrentItem = -1;
        this.mSavedItemAnimator = null;
        this.mSavedItemAnimatorPresent = false;
        this.mUserInputEnabled = true;
        this.mOffscreenPageLimit = -1;
        this.initialize(context, set);
    }
    
    public ViewPager2(final Context context, final AttributeSet set, final int n, final int n2) {
        super(context, set, n, n2);
        this.mTmpContainerRect = new Rect();
        this.mTmpChildRect = new Rect();
        this.mExternalPageChangeCallbacks = new CompositeOnPageChangeCallback(3);
        this.mCurrentItemDirty = false;
        this.mCurrentItemDataSetChangeObserver = new DataSetChangeObserver() {
            final ViewPager2 this$0;
            
            @Override
            public void onChanged() {
                this.this$0.mCurrentItemDirty = true;
                this.this$0.mScrollEventAdapter.notifyDataSetChangeHappened();
            }
        };
        this.mPendingCurrentItem = -1;
        this.mSavedItemAnimator = null;
        this.mSavedItemAnimatorPresent = false;
        this.mUserInputEnabled = true;
        this.mOffscreenPageLimit = -1;
        this.initialize(context, set);
    }
    
    private RecyclerView.OnChildAttachStateChangeListener enforceChildFillListener() {
        return new RecyclerView.OnChildAttachStateChangeListener(this) {
            final ViewPager2 this$0;
            
            @Override
            public void onChildViewAttachedToWindow(final View view) {
                final LayoutParams layoutParams = (LayoutParams)view.getLayoutParams();
                if (layoutParams.width == -1 && layoutParams.height == -1) {
                    return;
                }
                throw new IllegalStateException("Pages must fill the whole ViewPager2 (use match_parent)");
            }
            
            @Override
            public void onChildViewDetachedFromWindow(final View view) {
            }
        };
    }
    
    private void initialize(final Context context, final AttributeSet set) {
        AccessibilityProvider mAccessibilityProvider;
        if (ViewPager2.sFeatureEnhancedA11yEnabled) {
            mAccessibilityProvider = new PageAwareAccessibilityProvider();
        }
        else {
            mAccessibilityProvider = new BasicAccessibilityProvider();
        }
        this.mAccessibilityProvider = mAccessibilityProvider;
        (this.mRecyclerView = new RecyclerViewImpl(context)).setId(ViewCompat.generateViewId());
        this.mRecyclerView.setDescendantFocusability(131072);
        final LinearLayoutManagerImpl linearLayoutManagerImpl = new LinearLayoutManagerImpl(context);
        this.mLayoutManager = linearLayoutManagerImpl;
        this.mRecyclerView.setLayoutManager((RecyclerView.LayoutManager)linearLayoutManagerImpl);
        this.mRecyclerView.setScrollingTouchSlop(1);
        this.setOrientation(context, set);
        this.mRecyclerView.setLayoutParams(new ViewGroup$LayoutParams(-1, -1));
        this.mRecyclerView.addOnChildAttachStateChangeListener(this.enforceChildFillListener());
        final ScrollEventAdapter mScrollEventAdapter = new ScrollEventAdapter(this);
        this.mScrollEventAdapter = mScrollEventAdapter;
        this.mFakeDragger = new FakeDrag(this, mScrollEventAdapter, this.mRecyclerView);
        (this.mPagerSnapHelper = new PagerSnapHelperImpl()).attachToRecyclerView(this.mRecyclerView);
        this.mRecyclerView.addOnScrollListener((RecyclerView.OnScrollListener)this.mScrollEventAdapter);
        final CompositeOnPageChangeCallback compositeOnPageChangeCallback = new CompositeOnPageChangeCallback(3);
        this.mPageChangeEventDispatcher = compositeOnPageChangeCallback;
        this.mScrollEventAdapter.setOnPageChangeCallback(compositeOnPageChangeCallback);
        final OnPageChangeCallback onPageChangeCallback = new OnPageChangeCallback(this) {
            final ViewPager2 this$0;
            
            @Override
            public void onPageScrollStateChanged(final int n) {
                if (n == 0) {
                    this.this$0.updateCurrentItem();
                }
            }
            
            @Override
            public void onPageSelected(final int mCurrentItem) {
                if (this.this$0.mCurrentItem != mCurrentItem) {
                    this.this$0.mCurrentItem = mCurrentItem;
                    this.this$0.mAccessibilityProvider.onSetNewCurrentItem();
                }
            }
        };
        final OnPageChangeCallback onPageChangeCallback2 = new OnPageChangeCallback(this) {
            final ViewPager2 this$0;
            
            @Override
            public void onPageSelected(final int n) {
                this.this$0.clearFocus();
                if (this.this$0.hasFocus()) {
                    this.this$0.mRecyclerView.requestFocus(2);
                }
            }
        };
        this.mPageChangeEventDispatcher.addOnPageChangeCallback(onPageChangeCallback);
        this.mPageChangeEventDispatcher.addOnPageChangeCallback(onPageChangeCallback2);
        this.mAccessibilityProvider.onInitialize(this.mPageChangeEventDispatcher, this.mRecyclerView);
        this.mPageChangeEventDispatcher.addOnPageChangeCallback(this.mExternalPageChangeCallbacks);
        final PageTransformerAdapter mPageTransformerAdapter = new PageTransformerAdapter(this.mLayoutManager);
        this.mPageTransformerAdapter = mPageTransformerAdapter;
        this.mPageChangeEventDispatcher.addOnPageChangeCallback(mPageTransformerAdapter);
        final RecyclerView mRecyclerView = this.mRecyclerView;
        this.attachViewToParent((View)mRecyclerView, 0, mRecyclerView.getLayoutParams());
    }
    
    private void registerCurrentItemDataSetTracker(final RecyclerView.Adapter<?> adapter) {
        if (adapter != null) {
            adapter.registerAdapterDataObserver(this.mCurrentItemDataSetChangeObserver);
        }
    }
    
    private void restorePendingState() {
        if (this.mPendingCurrentItem == -1) {
            return;
        }
        final RecyclerView.Adapter adapter = this.getAdapter();
        if (adapter == null) {
            return;
        }
        final Parcelable mPendingAdapterState = this.mPendingAdapterState;
        if (mPendingAdapterState != null) {
            if (adapter instanceof StatefulAdapter) {
                ((StatefulAdapter)adapter).restoreState(mPendingAdapterState);
            }
            this.mPendingAdapterState = null;
        }
        final int max = Math.max(0, Math.min(this.mPendingCurrentItem, adapter.getItemCount() - 1));
        this.mCurrentItem = max;
        this.mPendingCurrentItem = -1;
        this.mRecyclerView.scrollToPosition(max);
        this.mAccessibilityProvider.onRestorePendingState();
    }
    
    private void setOrientation(final Context context, final AttributeSet set) {
        final TypedArray obtainStyledAttributes = context.obtainStyledAttributes(set, R.styleable.ViewPager2);
        if (Build$VERSION.SDK_INT >= 29) {
            this.saveAttributeDataForStyleable(context, R.styleable.ViewPager2, set, obtainStyledAttributes, 0, 0);
        }
        try {
            this.setOrientation(obtainStyledAttributes.getInt(R.styleable.ViewPager2_android_orientation, 0));
        }
        finally {
            obtainStyledAttributes.recycle();
        }
    }
    
    private void unregisterCurrentItemDataSetTracker(final RecyclerView.Adapter<?> adapter) {
        if (adapter != null) {
            adapter.unregisterAdapterDataObserver(this.mCurrentItemDataSetChangeObserver);
        }
    }
    
    public void addItemDecoration(final RecyclerView.ItemDecoration itemDecoration) {
        this.mRecyclerView.addItemDecoration(itemDecoration);
    }
    
    public void addItemDecoration(final RecyclerView.ItemDecoration itemDecoration, final int n) {
        this.mRecyclerView.addItemDecoration(itemDecoration, n);
    }
    
    public boolean beginFakeDrag() {
        return this.mFakeDragger.beginFakeDrag();
    }
    
    public boolean canScrollHorizontally(final int n) {
        return this.mRecyclerView.canScrollHorizontally(n);
    }
    
    public boolean canScrollVertically(final int n) {
        return this.mRecyclerView.canScrollVertically(n);
    }
    
    protected void dispatchRestoreInstanceState(final SparseArray<Parcelable> sparseArray) {
        final Parcelable parcelable = (Parcelable)sparseArray.get(this.getId());
        if (parcelable instanceof SavedState) {
            final int mRecyclerViewId = ((SavedState)parcelable).mRecyclerViewId;
            sparseArray.put(this.mRecyclerView.getId(), sparseArray.get(mRecyclerViewId));
            sparseArray.remove(mRecyclerViewId);
        }
        super.dispatchRestoreInstanceState((SparseArray)sparseArray);
        this.restorePendingState();
    }
    
    public boolean endFakeDrag() {
        return this.mFakeDragger.endFakeDrag();
    }
    
    public boolean fakeDragBy(final float n) {
        return this.mFakeDragger.fakeDragBy(n);
    }
    
    public CharSequence getAccessibilityClassName() {
        if (this.mAccessibilityProvider.handlesGetAccessibilityClassName()) {
            return this.mAccessibilityProvider.onGetAccessibilityClassName();
        }
        return super.getAccessibilityClassName();
    }
    
    public RecyclerView.Adapter getAdapter() {
        return this.mRecyclerView.getAdapter();
    }
    
    public int getCurrentItem() {
        return this.mCurrentItem;
    }
    
    public RecyclerView.ItemDecoration getItemDecorationAt(final int n) {
        return this.mRecyclerView.getItemDecorationAt(n);
    }
    
    public int getItemDecorationCount() {
        return this.mRecyclerView.getItemDecorationCount();
    }
    
    public int getOffscreenPageLimit() {
        return this.mOffscreenPageLimit;
    }
    
    public int getOrientation() {
        return this.mLayoutManager.getOrientation();
    }
    
    int getPageSize() {
        final RecyclerView mRecyclerView = this.mRecyclerView;
        int n;
        int n2;
        if (this.getOrientation() == 0) {
            n = mRecyclerView.getWidth() - mRecyclerView.getPaddingLeft();
            n2 = mRecyclerView.getPaddingRight();
        }
        else {
            n = mRecyclerView.getHeight() - mRecyclerView.getPaddingTop();
            n2 = mRecyclerView.getPaddingBottom();
        }
        return n - n2;
    }
    
    public int getScrollState() {
        return this.mScrollEventAdapter.getScrollState();
    }
    
    public void invalidateItemDecorations() {
        this.mRecyclerView.invalidateItemDecorations();
    }
    
    public boolean isFakeDragging() {
        return this.mFakeDragger.isFakeDragging();
    }
    
    boolean isRtl() {
        final int layoutDirection = ((RecyclerView.LayoutManager)this.mLayoutManager).getLayoutDirection();
        boolean b = true;
        if (layoutDirection != 1) {
            b = false;
        }
        return b;
    }
    
    public boolean isUserInputEnabled() {
        return this.mUserInputEnabled;
    }
    
    public void onInitializeAccessibilityNodeInfo(final AccessibilityNodeInfo accessibilityNodeInfo) {
        super.onInitializeAccessibilityNodeInfo(accessibilityNodeInfo);
        this.mAccessibilityProvider.onInitializeAccessibilityNodeInfo(accessibilityNodeInfo);
    }
    
    protected void onLayout(final boolean b, final int n, final int n2, final int n3, final int n4) {
        final int measuredWidth = this.mRecyclerView.getMeasuredWidth();
        final int measuredHeight = this.mRecyclerView.getMeasuredHeight();
        this.mTmpContainerRect.left = this.getPaddingLeft();
        this.mTmpContainerRect.right = n3 - n - this.getPaddingRight();
        this.mTmpContainerRect.top = this.getPaddingTop();
        this.mTmpContainerRect.bottom = n4 - n2 - this.getPaddingBottom();
        Gravity.apply(8388659, measuredWidth, measuredHeight, this.mTmpContainerRect, this.mTmpChildRect);
        this.mRecyclerView.layout(this.mTmpChildRect.left, this.mTmpChildRect.top, this.mTmpChildRect.right, this.mTmpChildRect.bottom);
        if (this.mCurrentItemDirty) {
            this.updateCurrentItem();
        }
    }
    
    protected void onMeasure(final int n, final int n2) {
        this.measureChild((View)this.mRecyclerView, n, n2);
        final int measuredWidth = this.mRecyclerView.getMeasuredWidth();
        final int measuredHeight = this.mRecyclerView.getMeasuredHeight();
        final int measuredState = this.mRecyclerView.getMeasuredState();
        this.setMeasuredDimension(resolveSizeAndState(Math.max(measuredWidth + (this.getPaddingLeft() + this.getPaddingRight()), this.getSuggestedMinimumWidth()), n, measuredState), resolveSizeAndState(Math.max(measuredHeight + (this.getPaddingTop() + this.getPaddingBottom()), this.getSuggestedMinimumHeight()), n2, measuredState << 16));
    }
    
    protected void onRestoreInstanceState(final Parcelable parcelable) {
        if (!(parcelable instanceof SavedState)) {
            super.onRestoreInstanceState(parcelable);
            return;
        }
        final SavedState savedState = (SavedState)parcelable;
        super.onRestoreInstanceState(savedState.getSuperState());
        this.mPendingCurrentItem = savedState.mCurrentItem;
        this.mPendingAdapterState = savedState.mAdapterState;
    }
    
    protected Parcelable onSaveInstanceState() {
        final SavedState savedState = new SavedState(super.onSaveInstanceState());
        savedState.mRecyclerViewId = this.mRecyclerView.getId();
        int mCurrentItem;
        if ((mCurrentItem = this.mPendingCurrentItem) == -1) {
            mCurrentItem = this.mCurrentItem;
        }
        savedState.mCurrentItem = mCurrentItem;
        final Parcelable mPendingAdapterState = this.mPendingAdapterState;
        if (mPendingAdapterState != null) {
            savedState.mAdapterState = mPendingAdapterState;
        }
        else {
            final RecyclerView.Adapter adapter = this.mRecyclerView.getAdapter();
            if (adapter instanceof StatefulAdapter) {
                savedState.mAdapterState = ((StatefulAdapter)adapter).saveState();
            }
        }
        return (Parcelable)savedState;
    }
    
    public void onViewAdded(final View view) {
        final StringBuilder sb = new StringBuilder();
        sb.append(this.getClass().getSimpleName());
        sb.append(" does not support direct child views");
        throw new IllegalStateException(sb.toString());
    }
    
    public boolean performAccessibilityAction(final int n, final Bundle bundle) {
        if (this.mAccessibilityProvider.handlesPerformAccessibilityAction(n, bundle)) {
            return this.mAccessibilityProvider.onPerformAccessibilityAction(n, bundle);
        }
        return super.performAccessibilityAction(n, bundle);
    }
    
    public void registerOnPageChangeCallback(final OnPageChangeCallback onPageChangeCallback) {
        this.mExternalPageChangeCallbacks.addOnPageChangeCallback(onPageChangeCallback);
    }
    
    public void removeItemDecoration(final RecyclerView.ItemDecoration itemDecoration) {
        this.mRecyclerView.removeItemDecoration(itemDecoration);
    }
    
    public void removeItemDecorationAt(final int n) {
        this.mRecyclerView.removeItemDecorationAt(n);
    }
    
    public void requestTransform() {
        if (this.mPageTransformerAdapter.getPageTransformer() == null) {
            return;
        }
        final double relativeScrollPosition = this.mScrollEventAdapter.getRelativeScrollPosition();
        final int n = (int)relativeScrollPosition;
        final float n2 = (float)(relativeScrollPosition - n);
        this.mPageTransformerAdapter.onPageScrolled(n, n2, Math.round(this.getPageSize() * n2));
    }
    
    public void setAdapter(final RecyclerView.Adapter adapter) {
        final RecyclerView.Adapter adapter2 = this.mRecyclerView.getAdapter();
        this.mAccessibilityProvider.onDetachAdapter(adapter2);
        this.unregisterCurrentItemDataSetTracker(adapter2);
        this.mRecyclerView.setAdapter(adapter);
        this.mCurrentItem = 0;
        this.restorePendingState();
        this.mAccessibilityProvider.onAttachAdapter(adapter);
        this.registerCurrentItemDataSetTracker(adapter);
    }
    
    public void setCurrentItem(final int n) {
        this.setCurrentItem(n, true);
    }
    
    public void setCurrentItem(final int n, final boolean b) {
        if (!this.isFakeDragging()) {
            this.setCurrentItemInternal(n, b);
            return;
        }
        throw new IllegalStateException("Cannot change current item when ViewPager2 is fake dragging");
    }
    
    void setCurrentItemInternal(int mCurrentItem, final boolean b) {
        final RecyclerView.Adapter adapter = this.getAdapter();
        if (adapter == null) {
            if (this.mPendingCurrentItem != -1) {
                this.mPendingCurrentItem = Math.max(mCurrentItem, 0);
            }
            return;
        }
        if (adapter.getItemCount() <= 0) {
            return;
        }
        final int min = Math.min(Math.max(mCurrentItem, 0), adapter.getItemCount() - 1);
        if (min == this.mCurrentItem && this.mScrollEventAdapter.isIdle()) {
            return;
        }
        mCurrentItem = this.mCurrentItem;
        if (min == mCurrentItem && b) {
            return;
        }
        double relativeScrollPosition = mCurrentItem;
        this.mCurrentItem = min;
        this.mAccessibilityProvider.onSetNewCurrentItem();
        if (!this.mScrollEventAdapter.isIdle()) {
            relativeScrollPosition = this.mScrollEventAdapter.getRelativeScrollPosition();
        }
        this.mScrollEventAdapter.notifyProgrammaticScroll(min, b);
        if (!b) {
            this.mRecyclerView.scrollToPosition(min);
            return;
        }
        final double n = min;
        if (Math.abs(n - relativeScrollPosition) > 3.0) {
            final RecyclerView mRecyclerView = this.mRecyclerView;
            if (n > relativeScrollPosition) {
                mCurrentItem = min - 3;
            }
            else {
                mCurrentItem = min + 3;
            }
            mRecyclerView.scrollToPosition(mCurrentItem);
            final RecyclerView mRecyclerView2 = this.mRecyclerView;
            mRecyclerView2.post((Runnable)new SmoothScrollToPosition(min, mRecyclerView2));
        }
        else {
            this.mRecyclerView.smoothScrollToPosition(min);
        }
    }
    
    public void setLayoutDirection(final int layoutDirection) {
        super.setLayoutDirection(layoutDirection);
        this.mAccessibilityProvider.onSetLayoutDirection();
    }
    
    public void setOffscreenPageLimit(final int mOffscreenPageLimit) {
        if (mOffscreenPageLimit < 1 && mOffscreenPageLimit != -1) {
            throw new IllegalArgumentException("Offscreen page limit must be OFFSCREEN_PAGE_LIMIT_DEFAULT or a number > 0");
        }
        this.mOffscreenPageLimit = mOffscreenPageLimit;
        this.mRecyclerView.requestLayout();
    }
    
    public void setOrientation(final int orientation) {
        this.mLayoutManager.setOrientation(orientation);
        this.mAccessibilityProvider.onSetOrientation();
    }
    
    public void setPageTransformer(final PageTransformer pageTransformer) {
        if (pageTransformer != null) {
            if (!this.mSavedItemAnimatorPresent) {
                this.mSavedItemAnimator = this.mRecyclerView.getItemAnimator();
                this.mSavedItemAnimatorPresent = true;
            }
            this.mRecyclerView.setItemAnimator(null);
        }
        else if (this.mSavedItemAnimatorPresent) {
            this.mRecyclerView.setItemAnimator(this.mSavedItemAnimator);
            this.mSavedItemAnimator = null;
            this.mSavedItemAnimatorPresent = false;
        }
        if (pageTransformer == this.mPageTransformerAdapter.getPageTransformer()) {
            return;
        }
        this.mPageTransformerAdapter.setPageTransformer(pageTransformer);
        this.requestTransform();
    }
    
    public void setUserInputEnabled(final boolean mUserInputEnabled) {
        this.mUserInputEnabled = mUserInputEnabled;
        this.mAccessibilityProvider.onSetUserInputEnabled();
    }
    
    void snapToPage() {
        final View snapView = this.mPagerSnapHelper.findSnapView(this.mLayoutManager);
        if (snapView == null) {
            return;
        }
        final int[] calculateDistanceToFinalSnap = this.mPagerSnapHelper.calculateDistanceToFinalSnap(this.mLayoutManager, snapView);
        final int n = calculateDistanceToFinalSnap[0];
        if (n != 0 || calculateDistanceToFinalSnap[1] != 0) {
            this.mRecyclerView.smoothScrollBy(n, calculateDistanceToFinalSnap[1]);
        }
    }
    
    public void unregisterOnPageChangeCallback(final OnPageChangeCallback onPageChangeCallback) {
        this.mExternalPageChangeCallbacks.removeOnPageChangeCallback(onPageChangeCallback);
    }
    
    void updateCurrentItem() {
        final PagerSnapHelper mPagerSnapHelper = this.mPagerSnapHelper;
        if (mPagerSnapHelper == null) {
            throw new IllegalStateException("Design assumption violated.");
        }
        final View snapView = mPagerSnapHelper.findSnapView(this.mLayoutManager);
        if (snapView == null) {
            return;
        }
        final int position = ((RecyclerView.LayoutManager)this.mLayoutManager).getPosition(snapView);
        if (position != this.mCurrentItem && this.getScrollState() == 0) {
            this.mPageChangeEventDispatcher.onPageSelected(position);
        }
        this.mCurrentItemDirty = false;
    }
    
    private abstract class AccessibilityProvider
    {
        final ViewPager2 this$0;
        
        private AccessibilityProvider(final ViewPager2 this$0) {
            this.this$0 = this$0;
        }
        
        boolean handlesGetAccessibilityClassName() {
            return false;
        }
        
        boolean handlesLmPerformAccessibilityAction(final int n) {
            return false;
        }
        
        boolean handlesPerformAccessibilityAction(final int n, final Bundle bundle) {
            return false;
        }
        
        boolean handlesRvGetAccessibilityClassName() {
            return false;
        }
        
        void onAttachAdapter(final RecyclerView.Adapter<?> adapter) {
        }
        
        void onDetachAdapter(final RecyclerView.Adapter<?> adapter) {
        }
        
        String onGetAccessibilityClassName() {
            throw new IllegalStateException("Not implemented.");
        }
        
        void onInitialize(final CompositeOnPageChangeCallback compositeOnPageChangeCallback, final RecyclerView recyclerView) {
        }
        
        void onInitializeAccessibilityNodeInfo(final AccessibilityNodeInfo accessibilityNodeInfo) {
        }
        
        void onLmInitializeAccessibilityNodeInfo(final AccessibilityNodeInfoCompat accessibilityNodeInfoCompat) {
        }
        
        boolean onLmPerformAccessibilityAction(final int n) {
            throw new IllegalStateException("Not implemented.");
        }
        
        boolean onPerformAccessibilityAction(final int n, final Bundle bundle) {
            throw new IllegalStateException("Not implemented.");
        }
        
        void onRestorePendingState() {
        }
        
        CharSequence onRvGetAccessibilityClassName() {
            throw new IllegalStateException("Not implemented.");
        }
        
        void onRvInitializeAccessibilityEvent(final AccessibilityEvent accessibilityEvent) {
        }
        
        void onSetLayoutDirection() {
        }
        
        void onSetNewCurrentItem() {
        }
        
        void onSetOrientation() {
        }
        
        void onSetUserInputEnabled() {
        }
    }
    
    class BasicAccessibilityProvider extends AccessibilityProvider
    {
        final ViewPager2 this$0;
        
        BasicAccessibilityProvider(final ViewPager2 this$0) {
        }
        
        public boolean handlesLmPerformAccessibilityAction(final int n) {
            return (n == 8192 || n == 4096) && !this.this$0.isUserInputEnabled();
        }
        
        public boolean handlesRvGetAccessibilityClassName() {
            return true;
        }
        
        public void onLmInitializeAccessibilityNodeInfo(final AccessibilityNodeInfoCompat accessibilityNodeInfoCompat) {
            if (!this.this$0.isUserInputEnabled()) {
                accessibilityNodeInfoCompat.removeAction(AccessibilityNodeInfoCompat.AccessibilityActionCompat.ACTION_SCROLL_BACKWARD);
                accessibilityNodeInfoCompat.removeAction(AccessibilityNodeInfoCompat.AccessibilityActionCompat.ACTION_SCROLL_FORWARD);
                accessibilityNodeInfoCompat.setScrollable(false);
            }
        }
        
        public boolean onLmPerformAccessibilityAction(final int n) {
            if (this.handlesLmPerformAccessibilityAction(n)) {
                return false;
            }
            throw new IllegalStateException();
        }
        
        public CharSequence onRvGetAccessibilityClassName() {
            if (this.handlesRvGetAccessibilityClassName()) {
                return "androidx.viewpager.widget.ViewPager";
            }
            throw new IllegalStateException();
        }
    }
    
    private abstract static class DataSetChangeObserver extends AdapterDataObserver
    {
        @Override
        public abstract void onChanged();
        
        @Override
        public final void onItemRangeChanged(final int n, final int n2) {
            this.onChanged();
        }
        
        @Override
        public final void onItemRangeChanged(final int n, final int n2, final Object o) {
            this.onChanged();
        }
        
        @Override
        public final void onItemRangeInserted(final int n, final int n2) {
            this.onChanged();
        }
        
        @Override
        public final void onItemRangeMoved(final int n, final int n2, final int n3) {
            this.onChanged();
        }
        
        @Override
        public final void onItemRangeRemoved(final int n, final int n2) {
            this.onChanged();
        }
    }
    
    private class LinearLayoutManagerImpl extends LinearLayoutManager
    {
        final ViewPager2 this$0;
        
        LinearLayoutManagerImpl(final ViewPager2 this$0, final Context context) {
            this.this$0 = this$0;
            super(context);
        }
        
        @Override
        protected void calculateExtraLayoutSpace(final State state, final int[] array) {
            final int offscreenPageLimit = this.this$0.getOffscreenPageLimit();
            if (offscreenPageLimit == -1) {
                super.calculateExtraLayoutSpace(state, array);
                return;
            }
            array[1] = (array[0] = this.this$0.getPageSize() * offscreenPageLimit);
        }
        
        @Override
        public void onInitializeAccessibilityNodeInfo(final Recycler recycler, final State state, final AccessibilityNodeInfoCompat accessibilityNodeInfoCompat) {
            super.onInitializeAccessibilityNodeInfo(recycler, state, accessibilityNodeInfoCompat);
            this.this$0.mAccessibilityProvider.onLmInitializeAccessibilityNodeInfo(accessibilityNodeInfoCompat);
        }
        
        @Override
        public boolean performAccessibilityAction(final Recycler recycler, final State state, final int n, final Bundle bundle) {
            if (this.this$0.mAccessibilityProvider.handlesLmPerformAccessibilityAction(n)) {
                return this.this$0.mAccessibilityProvider.onLmPerformAccessibilityAction(n);
            }
            return super.performAccessibilityAction(recycler, state, n, bundle);
        }
        
        @Override
        public boolean requestChildRectangleOnScreen(final RecyclerView recyclerView, final View view, final Rect rect, final boolean b, final boolean b2) {
            return false;
        }
    }
    
    @Retention(RetentionPolicy.SOURCE)
    public @interface OffscreenPageLimit {
    }
    
    public abstract static class OnPageChangeCallback
    {
        public void onPageScrollStateChanged(final int n) {
        }
        
        public void onPageScrolled(final int n, final float n2, final int n3) {
        }
        
        public void onPageSelected(final int n) {
        }
    }
    
    @Retention(RetentionPolicy.SOURCE)
    public @interface Orientation {
    }
    
    class PageAwareAccessibilityProvider extends AccessibilityProvider
    {
        private final AccessibilityViewCommand mActionPageBackward;
        private final AccessibilityViewCommand mActionPageForward;
        private RecyclerView.AdapterDataObserver mAdapterDataObserver;
        final ViewPager2 this$0;
        
        PageAwareAccessibilityProvider(final ViewPager2 this$0) {
            this.mActionPageForward = new AccessibilityViewCommand() {
                final PageAwareAccessibilityProvider this$1;
                
                @Override
                public boolean perform(final View view, final CommandArguments commandArguments) {
                    this.this$1.setCurrentItemFromAccessibilityCommand(((ViewPager2)view).getCurrentItem() + 1);
                    return true;
                }
            };
            this.mActionPageBackward = new AccessibilityViewCommand() {
                final PageAwareAccessibilityProvider this$1;
                
                @Override
                public boolean perform(final View view, final CommandArguments commandArguments) {
                    this.this$1.setCurrentItemFromAccessibilityCommand(((ViewPager2)view).getCurrentItem() - 1);
                    return true;
                }
            };
        }
        
        private void addCollectionInfo(final AccessibilityNodeInfo accessibilityNodeInfo) {
            int itemCount = 0;
            int itemCount2 = 0;
            Label_0055: {
                if (this.this$0.getAdapter() != null) {
                    if (this.this$0.getOrientation() != 1) {
                        itemCount = this.this$0.getAdapter().getItemCount();
                        itemCount2 = 0;
                        break Label_0055;
                    }
                    itemCount2 = this.this$0.getAdapter().getItemCount();
                }
                else {
                    itemCount2 = 0;
                }
                itemCount = 0;
            }
            AccessibilityNodeInfoCompat.wrap(accessibilityNodeInfo).setCollectionInfo(AccessibilityNodeInfoCompat.CollectionInfoCompat.obtain(itemCount2, itemCount, false, 0));
        }
        
        private void addScrollActions(final AccessibilityNodeInfo accessibilityNodeInfo) {
            final RecyclerView.Adapter adapter = this.this$0.getAdapter();
            if (adapter == null) {
                return;
            }
            final int itemCount = adapter.getItemCount();
            if (itemCount != 0) {
                if (this.this$0.isUserInputEnabled()) {
                    if (this.this$0.mCurrentItem > 0) {
                        accessibilityNodeInfo.addAction(8192);
                    }
                    if (this.this$0.mCurrentItem < itemCount - 1) {
                        accessibilityNodeInfo.addAction(4096);
                    }
                    accessibilityNodeInfo.setScrollable(true);
                }
            }
        }
        
        public boolean handlesGetAccessibilityClassName() {
            return true;
        }
        
        public boolean handlesPerformAccessibilityAction(final int n, final Bundle bundle) {
            return n == 8192 || n == 4096;
        }
        
        public void onAttachAdapter(final RecyclerView.Adapter<?> adapter) {
            this.updatePageAccessibilityActions();
            if (adapter != null) {
                adapter.registerAdapterDataObserver(this.mAdapterDataObserver);
            }
        }
        
        public void onDetachAdapter(final RecyclerView.Adapter<?> adapter) {
            if (adapter != null) {
                adapter.unregisterAdapterDataObserver(this.mAdapterDataObserver);
            }
        }
        
        public String onGetAccessibilityClassName() {
            if (this.handlesGetAccessibilityClassName()) {
                return "androidx.viewpager.widget.ViewPager";
            }
            throw new IllegalStateException();
        }
        
        public void onInitialize(final CompositeOnPageChangeCallback compositeOnPageChangeCallback, final RecyclerView recyclerView) {
            ViewCompat.setImportantForAccessibility((View)recyclerView, 2);
            this.mAdapterDataObserver = new DataSetChangeObserver(this) {
                final PageAwareAccessibilityProvider this$1;
                
                @Override
                public void onChanged() {
                    this.this$1.updatePageAccessibilityActions();
                }
            };
            if (ViewCompat.getImportantForAccessibility((View)this.this$0) == 0) {
                ViewCompat.setImportantForAccessibility((View)this.this$0, 1);
            }
        }
        
        public void onInitializeAccessibilityNodeInfo(final AccessibilityNodeInfo accessibilityNodeInfo) {
            this.addCollectionInfo(accessibilityNodeInfo);
            if (Build$VERSION.SDK_INT >= 16) {
                this.addScrollActions(accessibilityNodeInfo);
            }
        }
        
        public boolean onPerformAccessibilityAction(int currentItemFromAccessibilityCommand, final Bundle bundle) {
            if (this.handlesPerformAccessibilityAction(currentItemFromAccessibilityCommand, bundle)) {
                if (currentItemFromAccessibilityCommand == 8192) {
                    currentItemFromAccessibilityCommand = this.this$0.getCurrentItem() - 1;
                }
                else {
                    currentItemFromAccessibilityCommand = this.this$0.getCurrentItem() + 1;
                }
                this.setCurrentItemFromAccessibilityCommand(currentItemFromAccessibilityCommand);
                return true;
            }
            throw new IllegalStateException();
        }
        
        public void onRestorePendingState() {
            this.updatePageAccessibilityActions();
        }
        
        public void onRvInitializeAccessibilityEvent(final AccessibilityEvent accessibilityEvent) {
            accessibilityEvent.setSource((View)this.this$0);
            accessibilityEvent.setClassName((CharSequence)this.onGetAccessibilityClassName());
        }
        
        public void onSetLayoutDirection() {
            this.updatePageAccessibilityActions();
        }
        
        public void onSetNewCurrentItem() {
            this.updatePageAccessibilityActions();
        }
        
        public void onSetOrientation() {
            this.updatePageAccessibilityActions();
        }
        
        public void onSetUserInputEnabled() {
            this.updatePageAccessibilityActions();
            if (Build$VERSION.SDK_INT < 21) {
                this.this$0.sendAccessibilityEvent(2048);
            }
        }
        
        void setCurrentItemFromAccessibilityCommand(final int n) {
            if (this.this$0.isUserInputEnabled()) {
                this.this$0.setCurrentItemInternal(n, true);
            }
        }
        
        void updatePageAccessibilityActions() {
            final ViewPager2 this$0 = this.this$0;
            int n = 16908360;
            ViewCompat.removeAccessibilityAction((View)this$0, 16908360);
            ViewCompat.removeAccessibilityAction((View)this$0, 16908361);
            ViewCompat.removeAccessibilityAction((View)this$0, 16908358);
            ViewCompat.removeAccessibilityAction((View)this$0, 16908359);
            if (this.this$0.getAdapter() == null) {
                return;
            }
            final int itemCount = this.this$0.getAdapter().getItemCount();
            if (itemCount == 0) {
                return;
            }
            if (!this.this$0.isUserInputEnabled()) {
                return;
            }
            if (this.this$0.getOrientation() == 0) {
                final boolean rtl = this.this$0.isRtl();
                int n2;
                if (rtl) {
                    n2 = 16908360;
                }
                else {
                    n2 = 16908361;
                }
                if (rtl) {
                    n = 16908361;
                }
                if (this.this$0.mCurrentItem < itemCount - 1) {
                    ViewCompat.replaceAccessibilityAction((View)this$0, new AccessibilityNodeInfoCompat.AccessibilityActionCompat(n2, null), null, this.mActionPageForward);
                }
                if (this.this$0.mCurrentItem > 0) {
                    ViewCompat.replaceAccessibilityAction((View)this$0, new AccessibilityNodeInfoCompat.AccessibilityActionCompat(n, null), null, this.mActionPageBackward);
                }
            }
            else {
                if (this.this$0.mCurrentItem < itemCount - 1) {
                    ViewCompat.replaceAccessibilityAction((View)this$0, new AccessibilityNodeInfoCompat.AccessibilityActionCompat(16908359, null), null, this.mActionPageForward);
                }
                if (this.this$0.mCurrentItem > 0) {
                    ViewCompat.replaceAccessibilityAction((View)this$0, new AccessibilityNodeInfoCompat.AccessibilityActionCompat(16908358, null), null, this.mActionPageBackward);
                }
            }
        }
    }
    
    public interface PageTransformer
    {
        void transformPage(final View p0, final float p1);
    }
    
    private class PagerSnapHelperImpl extends PagerSnapHelper
    {
        final ViewPager2 this$0;
        
        PagerSnapHelperImpl(final ViewPager2 this$0) {
            this.this$0 = this$0;
        }
        
        @Override
        public View findSnapView(final LayoutManager layoutManager) {
            View snapView;
            if (this.this$0.isFakeDragging()) {
                snapView = null;
            }
            else {
                snapView = super.findSnapView(layoutManager);
            }
            return snapView;
        }
    }
    
    private class RecyclerViewImpl extends RecyclerView
    {
        final ViewPager2 this$0;
        
        RecyclerViewImpl(final ViewPager2 this$0, final Context context) {
            this.this$0 = this$0;
            super(context);
        }
        
        @Override
        public CharSequence getAccessibilityClassName() {
            if (this.this$0.mAccessibilityProvider.handlesRvGetAccessibilityClassName()) {
                return this.this$0.mAccessibilityProvider.onRvGetAccessibilityClassName();
            }
            return super.getAccessibilityClassName();
        }
        
        public void onInitializeAccessibilityEvent(final AccessibilityEvent accessibilityEvent) {
            super.onInitializeAccessibilityEvent(accessibilityEvent);
            accessibilityEvent.setFromIndex(this.this$0.mCurrentItem);
            accessibilityEvent.setToIndex(this.this$0.mCurrentItem);
            this.this$0.mAccessibilityProvider.onRvInitializeAccessibilityEvent(accessibilityEvent);
        }
        
        @Override
        public boolean onInterceptTouchEvent(final MotionEvent motionEvent) {
            return this.this$0.isUserInputEnabled() && super.onInterceptTouchEvent(motionEvent);
        }
        
        @Override
        public boolean onTouchEvent(final MotionEvent motionEvent) {
            return this.this$0.isUserInputEnabled() && super.onTouchEvent(motionEvent);
        }
    }
    
    static class SavedState extends View$BaseSavedState
    {
        public static final Parcelable$Creator<SavedState> CREATOR;
        Parcelable mAdapterState;
        int mCurrentItem;
        int mRecyclerViewId;
        
        static {
            CREATOR = (Parcelable$Creator)new Parcelable$ClassLoaderCreator<SavedState>() {
                public SavedState createFromParcel(final Parcel parcel) {
                    return this.createFromParcel(parcel, null);
                }
                
                public SavedState createFromParcel(final Parcel parcel, final ClassLoader classLoader) {
                    SavedState savedState;
                    if (Build$VERSION.SDK_INT >= 24) {
                        savedState = new SavedState(parcel, classLoader);
                    }
                    else {
                        savedState = new SavedState(parcel);
                    }
                    return savedState;
                }
                
                public SavedState[] newArray(final int n) {
                    return new SavedState[n];
                }
            };
        }
        
        SavedState(final Parcel parcel) {
            super(parcel);
            this.readValues(parcel, null);
        }
        
        SavedState(final Parcel parcel, final ClassLoader classLoader) {
            super(parcel, classLoader);
            this.readValues(parcel, classLoader);
        }
        
        SavedState(final Parcelable parcelable) {
            super(parcelable);
        }
        
        private void readValues(final Parcel parcel, final ClassLoader classLoader) {
            this.mRecyclerViewId = parcel.readInt();
            this.mCurrentItem = parcel.readInt();
            this.mAdapterState = parcel.readParcelable(classLoader);
        }
        
        public void writeToParcel(final Parcel parcel, final int n) {
            super.writeToParcel(parcel, n);
            parcel.writeInt(this.mRecyclerViewId);
            parcel.writeInt(this.mCurrentItem);
            parcel.writeParcelable(this.mAdapterState, n);
        }
    }
    
    @Retention(RetentionPolicy.SOURCE)
    public @interface ScrollState {
    }
    
    private static class SmoothScrollToPosition implements Runnable
    {
        private final int mPosition;
        private final RecyclerView mRecyclerView;
        
        SmoothScrollToPosition(final int mPosition, final RecyclerView mRecyclerView) {
            this.mPosition = mPosition;
            this.mRecyclerView = mRecyclerView;
        }
        
        @Override
        public void run() {
            this.mRecyclerView.smoothScrollToPosition(this.mPosition);
        }
    }
}

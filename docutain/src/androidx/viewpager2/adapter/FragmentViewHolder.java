// 
// Decompiled by Procyon v0.6.0
// 

package androidx.viewpager2.adapter;

import androidx.core.view.ViewCompat;
import android.view.ViewGroup$LayoutParams;
import android.view.ViewGroup;
import android.view.View;
import android.widget.FrameLayout;
import androidx.recyclerview.widget.RecyclerView;

public final class FragmentViewHolder extends ViewHolder
{
    private FragmentViewHolder(final FrameLayout frameLayout) {
        super((View)frameLayout);
    }
    
    static FragmentViewHolder create(final ViewGroup viewGroup) {
        final FrameLayout frameLayout = new FrameLayout(viewGroup.getContext());
        frameLayout.setLayoutParams(new ViewGroup$LayoutParams(-1, -1));
        frameLayout.setId(ViewCompat.generateViewId());
        frameLayout.setSaveEnabled(false);
        return new FragmentViewHolder(frameLayout);
    }
    
    FrameLayout getContainer() {
        return (FrameLayout)this.itemView;
    }
}

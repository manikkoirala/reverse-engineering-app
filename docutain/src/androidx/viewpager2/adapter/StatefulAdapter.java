// 
// Decompiled by Procyon v0.6.0
// 

package androidx.viewpager2.adapter;

import android.os.Parcelable;

public interface StatefulAdapter
{
    void restoreState(final Parcelable p0);
    
    Parcelable saveState();
}

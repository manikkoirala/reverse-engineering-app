// 
// Decompiled by Procyon v0.6.0
// 

package androidx.gridlayout;

public final class R
{
    public static final class attr
    {
        public static final int alignmentMode = 2130968627;
        public static final int alpha = 2130968632;
        public static final int columnCount = 2130968894;
        public static final int columnOrderPreserved = 2130968895;
        public static final int coordinatorLayoutStyle = 2130968923;
        public static final int font = 2130969119;
        public static final int fontProviderAuthority = 2130969121;
        public static final int fontProviderCerts = 2130969122;
        public static final int fontProviderFetchStrategy = 2130969123;
        public static final int fontProviderFetchTimeout = 2130969124;
        public static final int fontProviderPackage = 2130969125;
        public static final int fontProviderQuery = 2130969126;
        public static final int fontStyle = 2130969128;
        public static final int fontVariationSettings = 2130969129;
        public static final int fontWeight = 2130969130;
        public static final int keylines = 2130969231;
        public static final int layout_anchor = 2130969243;
        public static final int layout_anchorGravity = 2130969244;
        public static final int layout_behavior = 2130969245;
        public static final int layout_column = 2130969248;
        public static final int layout_columnSpan = 2130969249;
        public static final int layout_columnWeight = 2130969250;
        public static final int layout_dodgeInsetEdges = 2130969297;
        public static final int layout_gravity = 2130969307;
        public static final int layout_insetEdge = 2130969308;
        public static final int layout_keyline = 2130969309;
        public static final int layout_row = 2130969312;
        public static final int layout_rowSpan = 2130969313;
        public static final int layout_rowWeight = 2130969314;
        public static final int orientation = 2130969497;
        public static final int rowCount = 2130969966;
        public static final int rowOrderPreserved = 2130969967;
        public static final int statusBarBackground = 2130970068;
        public static final int ttcIndex = 2130970276;
        public static final int useDefaultMargins = 2130970281;
    }
    
    public static final class color
    {
        public static final int notification_action_color_filter = 2131100416;
        public static final int notification_icon_bg_color = 2131100417;
        public static final int ripple_material_light = 2131100582;
        public static final int secondary_text_default_material_light = 2131100585;
    }
    
    public static final class dimen
    {
        public static final int compat_button_inset_horizontal_material = 2131165272;
        public static final int compat_button_inset_vertical_material = 2131165273;
        public static final int compat_button_padding_horizontal_material = 2131165274;
        public static final int compat_button_padding_vertical_material = 2131165275;
        public static final int compat_control_corner_material = 2131165276;
        public static final int compat_notification_large_icon_max_height = 2131165277;
        public static final int compat_notification_large_icon_max_width = 2131165278;
        public static final int default_gap = 2131165280;
        public static final int notification_action_icon_size = 2131165958;
        public static final int notification_action_text_size = 2131165959;
        public static final int notification_big_circle_margin = 2131165960;
        public static final int notification_content_margin_start = 2131165961;
        public static final int notification_large_icon_height = 2131165962;
        public static final int notification_large_icon_width = 2131165963;
        public static final int notification_main_column_padding_top = 2131165964;
        public static final int notification_media_narrow_margin = 2131165965;
        public static final int notification_right_icon_size = 2131165966;
        public static final int notification_right_side_padding_top = 2131165967;
        public static final int notification_small_icon_background_padding = 2131165968;
        public static final int notification_small_icon_size_as_large = 2131165969;
        public static final int notification_subtext_size = 2131165970;
        public static final int notification_top_pad = 2131165971;
        public static final int notification_top_pad_large_text = 2131165972;
    }
    
    public static final class drawable
    {
        public static final int notification_action_background = 2131231107;
        public static final int notification_bg = 2131231108;
        public static final int notification_bg_low = 2131231109;
        public static final int notification_bg_low_normal = 2131231110;
        public static final int notification_bg_low_pressed = 2131231111;
        public static final int notification_bg_normal = 2131231112;
        public static final int notification_bg_normal_pressed = 2131231113;
        public static final int notification_icon_background = 2131231114;
        public static final int notification_template_icon_bg = 2131231116;
        public static final int notification_template_icon_low_bg = 2131231117;
        public static final int notification_tile_bg = 2131231118;
        public static final int notify_panel_notification_icon_bg = 2131231120;
    }
    
    public static final class id
    {
        public static final int action_container = 2131361875;
        public static final int action_divider = 2131361879;
        public static final int action_image = 2131361885;
        public static final int action_text = 2131361897;
        public static final int actions = 2131361898;
        public static final int alignBounds = 2131361912;
        public static final int alignMargins = 2131361913;
        public static final int async = 2131361928;
        public static final int blocking = 2131361952;
        public static final int bottom = 2131361954;
        public static final int chronometer = 2131362002;
        public static final int end = 2131362085;
        public static final int forever = 2131362123;
        public static final int horizontal = 2131362147;
        public static final int icon = 2131362151;
        public static final int icon_group = 2131362153;
        public static final int info = 2131362170;
        public static final int italic = 2131362174;
        public static final int left = 2131362180;
        public static final int line1 = 2131362184;
        public static final int line3 = 2131362185;
        public static final int none = 2131362285;
        public static final int normal = 2131362286;
        public static final int notification_background = 2131362288;
        public static final int notification_main_column = 2131362293;
        public static final int notification_main_column_container = 2131362294;
        public static final int right = 2131362944;
        public static final int right_icon = 2131362946;
        public static final int right_side = 2131362947;
        public static final int start = 2131363037;
        public static final int tag_transition_group = 2131363068;
        public static final int tag_unhandled_key_event_manager = 2131363069;
        public static final int tag_unhandled_key_listeners = 2131363070;
        public static final int text = 2131363074;
        public static final int text2 = 2131363075;
        public static final int time = 2131363097;
        public static final int title = 2131363098;
        public static final int top = 2131363106;
        public static final int vertical = 2131363129;
    }
    
    public static final class integer
    {
        public static final int status_bar_notification_info_maxnum = 2131427399;
    }
    
    public static final class layout
    {
        public static final int notification_action = 2131558556;
        public static final int notification_action_tombstone = 2131558557;
        public static final int notification_template_custom_big = 2131558564;
        public static final int notification_template_icon_group = 2131558565;
        public static final int notification_template_part_chronometer = 2131558569;
        public static final int notification_template_part_time = 2131558570;
    }
    
    public static final class string
    {
        public static final int status_bar_notification_info_overflow = 2131886711;
    }
    
    public static final class style
    {
        public static final int TextAppearance_Compat_Notification = 2131952228;
        public static final int TextAppearance_Compat_Notification_Info = 2131952229;
        public static final int TextAppearance_Compat_Notification_Line2 = 2131952231;
        public static final int TextAppearance_Compat_Notification_Time = 2131952234;
        public static final int TextAppearance_Compat_Notification_Title = 2131952236;
        public static final int Widget_Compat_NotificationActionContainer = 2131952608;
        public static final int Widget_Compat_NotificationActionText = 2131952609;
        public static final int Widget_Support_CoordinatorLayout = 2131952908;
    }
    
    public static final class styleable
    {
        public static final int[] ColorStateListItem;
        public static final int ColorStateListItem_alpha = 3;
        public static final int ColorStateListItem_android_alpha = 1;
        public static final int ColorStateListItem_android_color = 0;
        public static final int[] CoordinatorLayout;
        public static final int[] CoordinatorLayout_Layout;
        public static final int CoordinatorLayout_Layout_android_layout_gravity = 0;
        public static final int CoordinatorLayout_Layout_layout_anchor = 1;
        public static final int CoordinatorLayout_Layout_layout_anchorGravity = 2;
        public static final int CoordinatorLayout_Layout_layout_behavior = 3;
        public static final int CoordinatorLayout_Layout_layout_dodgeInsetEdges = 4;
        public static final int CoordinatorLayout_Layout_layout_insetEdge = 5;
        public static final int CoordinatorLayout_Layout_layout_keyline = 6;
        public static final int CoordinatorLayout_keylines = 0;
        public static final int CoordinatorLayout_statusBarBackground = 1;
        public static final int[] FontFamily;
        public static final int[] FontFamilyFont;
        public static final int FontFamilyFont_android_font = 0;
        public static final int FontFamilyFont_android_fontStyle = 2;
        public static final int FontFamilyFont_android_fontVariationSettings = 4;
        public static final int FontFamilyFont_android_fontWeight = 1;
        public static final int FontFamilyFont_android_ttcIndex = 3;
        public static final int FontFamilyFont_font = 5;
        public static final int FontFamilyFont_fontStyle = 6;
        public static final int FontFamilyFont_fontVariationSettings = 7;
        public static final int FontFamilyFont_fontWeight = 8;
        public static final int FontFamilyFont_ttcIndex = 9;
        public static final int FontFamily_fontProviderAuthority = 0;
        public static final int FontFamily_fontProviderCerts = 1;
        public static final int FontFamily_fontProviderFetchStrategy = 2;
        public static final int FontFamily_fontProviderFetchTimeout = 3;
        public static final int FontFamily_fontProviderPackage = 4;
        public static final int FontFamily_fontProviderQuery = 5;
        public static final int[] GradientColor;
        public static final int[] GradientColorItem;
        public static final int GradientColorItem_android_color = 0;
        public static final int GradientColorItem_android_offset = 1;
        public static final int GradientColor_android_centerColor = 7;
        public static final int GradientColor_android_centerX = 3;
        public static final int GradientColor_android_centerY = 4;
        public static final int GradientColor_android_endColor = 1;
        public static final int GradientColor_android_endX = 10;
        public static final int GradientColor_android_endY = 11;
        public static final int GradientColor_android_gradientRadius = 5;
        public static final int GradientColor_android_startColor = 0;
        public static final int GradientColor_android_startX = 8;
        public static final int GradientColor_android_startY = 9;
        public static final int GradientColor_android_tileMode = 6;
        public static final int GradientColor_android_type = 2;
        public static final int[] GridLayout;
        public static final int[] GridLayout_Layout;
        public static final int GridLayout_Layout_android_layout_height = 1;
        public static final int GridLayout_Layout_android_layout_margin = 2;
        public static final int GridLayout_Layout_android_layout_marginBottom = 6;
        public static final int GridLayout_Layout_android_layout_marginLeft = 3;
        public static final int GridLayout_Layout_android_layout_marginRight = 5;
        public static final int GridLayout_Layout_android_layout_marginTop = 4;
        public static final int GridLayout_Layout_android_layout_width = 0;
        public static final int GridLayout_Layout_layout_column = 7;
        public static final int GridLayout_Layout_layout_columnSpan = 8;
        public static final int GridLayout_Layout_layout_columnWeight = 9;
        public static final int GridLayout_Layout_layout_gravity = 10;
        public static final int GridLayout_Layout_layout_row = 11;
        public static final int GridLayout_Layout_layout_rowSpan = 12;
        public static final int GridLayout_Layout_layout_rowWeight = 13;
        public static final int GridLayout_alignmentMode = 0;
        public static final int GridLayout_columnCount = 1;
        public static final int GridLayout_columnOrderPreserved = 2;
        public static final int GridLayout_orientation = 3;
        public static final int GridLayout_rowCount = 4;
        public static final int GridLayout_rowOrderPreserved = 5;
        public static final int GridLayout_useDefaultMargins = 6;
        
        static {
            ColorStateListItem = new int[] { 16843173, 16843551, 16844359, 2130968632, 2130969232 };
            CoordinatorLayout = new int[] { 2130969231, 2130970068 };
            CoordinatorLayout_Layout = new int[] { 16842931, 2130969243, 2130969244, 2130969245, 2130969297, 2130969308, 2130969309 };
            FontFamily = new int[] { 2130969121, 2130969122, 2130969123, 2130969124, 2130969125, 2130969126, 2130969127 };
            FontFamilyFont = new int[] { 16844082, 16844083, 16844095, 16844143, 16844144, 2130969119, 2130969128, 2130969129, 2130969130, 2130970276 };
            GradientColor = new int[] { 16843165, 16843166, 16843169, 16843170, 16843171, 16843172, 16843265, 16843275, 16844048, 16844049, 16844050, 16844051 };
            GradientColorItem = new int[] { 16843173, 16844052 };
            GridLayout = new int[] { 2130968627, 2130968894, 2130968895, 2130969497, 2130969966, 2130969967, 2130970281 };
            GridLayout_Layout = new int[] { 16842996, 16842997, 16842998, 16842999, 16843000, 16843001, 16843002, 2130969248, 2130969249, 2130969250, 2130969307, 2130969312, 2130969313, 2130969314 };
        }
    }
}

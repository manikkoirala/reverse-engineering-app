// 
// Decompiled by Procyon v0.6.0
// 

package androidx.gridlayout.widget;

import java.util.HashMap;
import android.content.res.TypedArray;
import androidx.core.view.ViewGroupCompat;
import java.util.Iterator;
import java.util.List;
import android.util.Pair;
import java.util.ArrayList;
import android.view.ViewGroup$MarginLayoutParams;
import android.view.ViewGroup$LayoutParams;
import java.util.Arrays;
import androidx.legacy.widget.Space;
import android.graphics.Paint;
import android.graphics.Canvas;
import androidx.core.view.ViewCompat;
import java.lang.reflect.Array;
import android.view.View$MeasureSpec;
import android.util.AttributeSet;
import android.content.Context;
import android.view.View;
import androidx.gridlayout.R;
import android.util.LogPrinter;
import android.util.Printer;
import android.view.ViewGroup;

public class GridLayout extends ViewGroup
{
    private static final int ALIGNMENT_MODE;
    public static final int ALIGN_BOUNDS = 0;
    public static final int ALIGN_MARGINS = 1;
    public static final Alignment BASELINE;
    public static final Alignment BOTTOM;
    static final int CAN_STRETCH = 2;
    public static final Alignment CENTER;
    private static final int COLUMN_COUNT;
    private static final int COLUMN_ORDER_PRESERVED;
    private static final int DEFAULT_ALIGNMENT_MODE = 1;
    static final int DEFAULT_CONTAINER_MARGIN = 0;
    private static final int DEFAULT_COUNT = Integer.MIN_VALUE;
    static final boolean DEFAULT_ORDER_PRESERVED = true;
    private static final int DEFAULT_ORIENTATION = 0;
    private static final boolean DEFAULT_USE_DEFAULT_MARGINS = false;
    public static final Alignment END;
    public static final Alignment FILL;
    public static final int HORIZONTAL = 0;
    static final int INFLEXIBLE = 0;
    private static final Alignment LEADING;
    public static final Alignment LEFT;
    static final Printer LOG_PRINTER;
    static final int MAX_SIZE = 100000;
    static final Printer NO_PRINTER;
    private static final int ORIENTATION;
    public static final Alignment RIGHT;
    private static final int ROW_COUNT;
    private static final int ROW_ORDER_PRESERVED;
    public static final Alignment START;
    public static final Alignment TOP;
    private static final Alignment TRAILING;
    public static final int UNDEFINED = Integer.MIN_VALUE;
    static final Alignment UNDEFINED_ALIGNMENT;
    static final int UNINITIALIZED_HASH = 0;
    private static final int USE_DEFAULT_MARGINS;
    public static final int VERTICAL = 1;
    int mAlignmentMode;
    int mDefaultGap;
    final Axis mHorizontalAxis;
    int mLastLayoutParamsHashCode;
    int mOrientation;
    Printer mPrinter;
    boolean mUseDefaultMargins;
    final Axis mVerticalAxis;
    
    static {
        LOG_PRINTER = (Printer)new LogPrinter(3, GridLayout.class.getName());
        NO_PRINTER = (Printer)new Printer() {
            public void println(final String s) {
            }
        };
        ORIENTATION = R.styleable.GridLayout_orientation;
        ROW_COUNT = R.styleable.GridLayout_rowCount;
        COLUMN_COUNT = R.styleable.GridLayout_columnCount;
        USE_DEFAULT_MARGINS = R.styleable.GridLayout_useDefaultMargins;
        ALIGNMENT_MODE = R.styleable.GridLayout_alignmentMode;
        ROW_ORDER_PRESERVED = R.styleable.GridLayout_rowOrderPreserved;
        COLUMN_ORDER_PRESERVED = R.styleable.GridLayout_columnOrderPreserved;
        UNDEFINED_ALIGNMENT = (Alignment)new Alignment() {
            public int getAlignmentValue(final View view, final int n, final int n2) {
                return Integer.MIN_VALUE;
            }
            
            @Override
            String getDebugString() {
                return "UNDEFINED";
            }
            
            @Override
            int getGravityOffset(final View view, final int n) {
                return Integer.MIN_VALUE;
            }
        };
        final Alignment alignment = LEADING = (Alignment)new Alignment() {
            public int getAlignmentValue(final View view, final int n, final int n2) {
                return 0;
            }
            
            @Override
            String getDebugString() {
                return "LEADING";
            }
            
            @Override
            int getGravityOffset(final View view, final int n) {
                return 0;
            }
        };
        final Alignment alignment2 = TRAILING = (Alignment)new Alignment() {
            public int getAlignmentValue(final View view, final int n, final int n2) {
                return n;
            }
            
            @Override
            String getDebugString() {
                return "TRAILING";
            }
            
            @Override
            int getGravityOffset(final View view, final int n) {
                return n;
            }
        };
        TOP = alignment;
        BOTTOM = alignment2;
        START = alignment;
        END = alignment2;
        LEFT = createSwitchingAlignment(alignment, alignment2);
        RIGHT = createSwitchingAlignment(alignment2, alignment);
        CENTER = (Alignment)new Alignment() {
            public int getAlignmentValue(final View view, final int n, final int n2) {
                return n >> 1;
            }
            
            @Override
            String getDebugString() {
                return "CENTER";
            }
            
            @Override
            int getGravityOffset(final View view, final int n) {
                return n >> 1;
            }
        };
        BASELINE = (Alignment)new Alignment() {
            public int getAlignmentValue(final View view, int n, int baseline) {
                if (view.getVisibility() == 8) {
                    return 0;
                }
                baseline = view.getBaseline();
                if ((n = baseline) == -1) {
                    n = Integer.MIN_VALUE;
                }
                return n;
            }
            
            public Bounds getBounds() {
                return new Bounds(this) {
                    private int size;
                    final GridLayout$7 this$0;
                    
                    @Override
                    protected int getOffset(final GridLayout gridLayout, final View view, final Alignment alignment, final int n, final boolean b) {
                        return Math.max(0, super.getOffset(gridLayout, view, alignment, n, b));
                    }
                    
                    @Override
                    protected void include(final int n, final int n2) {
                        super.include(n, n2);
                        this.size = Math.max(this.size, n + n2);
                    }
                    
                    @Override
                    protected void reset() {
                        super.reset();
                        this.size = Integer.MIN_VALUE;
                    }
                    
                    @Override
                    protected int size(final boolean b) {
                        return Math.max(super.size(b), this.size);
                    }
                };
            }
            
            @Override
            String getDebugString() {
                return "BASELINE";
            }
            
            @Override
            int getGravityOffset(final View view, final int n) {
                return 0;
            }
        };
        FILL = (Alignment)new Alignment() {
            public int getAlignmentValue(final View view, final int n, final int n2) {
                return Integer.MIN_VALUE;
            }
            
            @Override
            String getDebugString() {
                return "FILL";
            }
            
            @Override
            int getGravityOffset(final View view, final int n) {
                return 0;
            }
            
            public int getSizeInCell(final View view, final int n, final int n2) {
                return n2;
            }
        };
    }
    
    public GridLayout(final Context context) {
        this(context, null);
    }
    
    public GridLayout(final Context context, final AttributeSet set) {
        this(context, set, 0);
    }
    
    public GridLayout(Context obtainStyledAttributes, final AttributeSet set, final int n) {
        super(obtainStyledAttributes, set, n);
        this.mHorizontalAxis = new Axis(true);
        this.mVerticalAxis = new Axis(false);
        this.mOrientation = 0;
        this.mUseDefaultMargins = false;
        this.mAlignmentMode = 1;
        this.mLastLayoutParamsHashCode = 0;
        this.mPrinter = GridLayout.LOG_PRINTER;
        this.mDefaultGap = obtainStyledAttributes.getResources().getDimensionPixelOffset(R.dimen.default_gap);
        obtainStyledAttributes = (Context)obtainStyledAttributes.obtainStyledAttributes(set, R.styleable.GridLayout);
        try {
            this.setRowCount(((TypedArray)obtainStyledAttributes).getInt(GridLayout.ROW_COUNT, Integer.MIN_VALUE));
            this.setColumnCount(((TypedArray)obtainStyledAttributes).getInt(GridLayout.COLUMN_COUNT, Integer.MIN_VALUE));
            this.setOrientation(((TypedArray)obtainStyledAttributes).getInt(GridLayout.ORIENTATION, 0));
            this.setUseDefaultMargins(((TypedArray)obtainStyledAttributes).getBoolean(GridLayout.USE_DEFAULT_MARGINS, false));
            this.setAlignmentMode(((TypedArray)obtainStyledAttributes).getInt(GridLayout.ALIGNMENT_MODE, 1));
            this.setRowOrderPreserved(((TypedArray)obtainStyledAttributes).getBoolean(GridLayout.ROW_ORDER_PRESERVED, true));
            this.setColumnOrderPreserved(((TypedArray)obtainStyledAttributes).getBoolean(GridLayout.COLUMN_ORDER_PRESERVED, true));
        }
        finally {
            ((TypedArray)obtainStyledAttributes).recycle();
        }
    }
    
    static int adjust(final int n, final int n2) {
        return View$MeasureSpec.makeMeasureSpec(View$MeasureSpec.getSize(n2 + n), View$MeasureSpec.getMode(n));
    }
    
    static <T> T[] append(final T[] array, final T[] array2) {
        final Object[] array3 = (Object[])Array.newInstance(array.getClass().getComponentType(), array.length + array2.length);
        System.arraycopy(array, 0, array3, 0, array.length);
        System.arraycopy(array2, 0, array3, array.length, array2.length);
        return (T[])array3;
    }
    
    static boolean canStretch(final int n) {
        return (n & 0x2) != 0x0;
    }
    
    private void checkLayoutParams(final LayoutParams layoutParams, final boolean b) {
        String str;
        if (b) {
            str = "column";
        }
        else {
            str = "row";
        }
        Spec spec;
        if (b) {
            spec = layoutParams.columnSpec;
        }
        else {
            spec = layoutParams.rowSpec;
        }
        final Interval span = spec.span;
        if (span.min != Integer.MIN_VALUE && span.min < 0) {
            final StringBuilder sb = new StringBuilder();
            sb.append(str);
            sb.append(" indices must be positive");
            handleInvalidParams(sb.toString());
        }
        Axis axis;
        if (b) {
            axis = this.mHorizontalAxis;
        }
        else {
            axis = this.mVerticalAxis;
        }
        final int definedCount = axis.definedCount;
        if (definedCount != Integer.MIN_VALUE) {
            if (span.max > definedCount) {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append(str);
                sb2.append(" indices (start + span) mustn't exceed the ");
                sb2.append(str);
                sb2.append(" count");
                handleInvalidParams(sb2.toString());
            }
            if (span.size() > definedCount) {
                final StringBuilder sb3 = new StringBuilder();
                sb3.append(str);
                sb3.append(" span mustn't exceed the ");
                sb3.append(str);
                sb3.append(" count");
                handleInvalidParams(sb3.toString());
            }
        }
    }
    
    private static int clip(final Interval interval, final boolean b, final int b2) {
        final int size = interval.size();
        if (b2 == 0) {
            return size;
        }
        int min;
        if (b) {
            min = Math.min(interval.min, b2);
        }
        else {
            min = 0;
        }
        return Math.min(size, b2 - min);
    }
    
    private int computeLayoutParamsHashCode() {
        final int childCount = this.getChildCount();
        int n = 1;
        for (int i = 0; i < childCount; ++i) {
            final View child = this.getChildAt(i);
            if (child.getVisibility() != 8) {
                n = n * 31 + ((LayoutParams)child.getLayoutParams()).hashCode();
            }
        }
        return n;
    }
    
    private void consistencyCheck() {
        final int mLastLayoutParamsHashCode = this.mLastLayoutParamsHashCode;
        if (mLastLayoutParamsHashCode == 0) {
            this.validateLayoutParams();
            this.mLastLayoutParamsHashCode = this.computeLayoutParamsHashCode();
        }
        else if (mLastLayoutParamsHashCode != this.computeLayoutParamsHashCode()) {
            this.mPrinter.println("The fields of some layout parameters were modified in between layout operations. Check the javadoc for GridLayout.LayoutParams#rowSpec.");
            this.invalidateStructure();
            this.consistencyCheck();
        }
    }
    
    private static Alignment createSwitchingAlignment(final Alignment alignment, final Alignment alignment2) {
        return (Alignment)new Alignment(alignment, alignment2) {
            final Alignment val$ltr;
            final Alignment val$rtl;
            
            public int getAlignmentValue(final View view, final int n, final int n2) {
                final int layoutDirection = ViewCompat.getLayoutDirection(view);
                boolean b = true;
                if (layoutDirection != 1) {
                    b = false;
                }
                Alignment alignment;
                if (!b) {
                    alignment = this.val$ltr;
                }
                else {
                    alignment = this.val$rtl;
                }
                return alignment.getAlignmentValue(view, n, n2);
            }
            
            @Override
            String getDebugString() {
                final StringBuilder sb = new StringBuilder();
                sb.append("SWITCHING[L:");
                sb.append(this.val$ltr.getDebugString());
                sb.append(", R:");
                sb.append(this.val$rtl.getDebugString());
                sb.append("]");
                return sb.toString();
            }
            
            @Override
            int getGravityOffset(final View view, final int n) {
                final int layoutDirection = ViewCompat.getLayoutDirection(view);
                boolean b = true;
                if (layoutDirection != 1) {
                    b = false;
                }
                Alignment alignment;
                if (!b) {
                    alignment = this.val$ltr;
                }
                else {
                    alignment = this.val$rtl;
                }
                return alignment.getGravityOffset(view, n);
            }
        };
    }
    
    private void drawLine(final Canvas canvas, final int n, final int n2, final int n3, final int n4, final Paint paint) {
        if (this.isLayoutRtlCompat()) {
            final int width = this.getWidth();
            canvas.drawLine((float)(width - n), (float)n2, (float)(width - n3), (float)n4, paint);
        }
        else {
            canvas.drawLine((float)n, (float)n2, (float)n3, (float)n4, paint);
        }
    }
    
    private static boolean fits(final int[] array, final int n, int i, final int n2) {
        if (n2 > array.length) {
            return false;
        }
        while (i < n2) {
            if (array[i] > n) {
                return false;
            }
            ++i;
        }
        return true;
    }
    
    static Alignment getAlignment(int n, final boolean b) {
        int n2;
        if (b) {
            n2 = 7;
        }
        else {
            n2 = 112;
        }
        int n3;
        if (b) {
            n3 = 0;
        }
        else {
            n3 = 4;
        }
        n = (n & n2) >> n3;
        if (n == 1) {
            return GridLayout.CENTER;
        }
        if (n == 3) {
            Alignment alignment;
            if (b) {
                alignment = GridLayout.LEFT;
            }
            else {
                alignment = GridLayout.TOP;
            }
            return alignment;
        }
        if (n == 5) {
            Alignment alignment2;
            if (b) {
                alignment2 = GridLayout.RIGHT;
            }
            else {
                alignment2 = GridLayout.BOTTOM;
            }
            return alignment2;
        }
        if (n == 7) {
            return GridLayout.FILL;
        }
        if (n == 8388611) {
            return GridLayout.START;
        }
        if (n != 8388613) {
            return GridLayout.UNDEFINED_ALIGNMENT;
        }
        return GridLayout.END;
    }
    
    private int getDefaultMargin(final View view, final LayoutParams layoutParams, final boolean b, final boolean b2) {
        final boolean mUseDefaultMargins = this.mUseDefaultMargins;
        final boolean b3 = false;
        if (!mUseDefaultMargins) {
            return 0;
        }
        Spec spec;
        if (b) {
            spec = layoutParams.columnSpec;
        }
        else {
            spec = layoutParams.rowSpec;
        }
        Axis axis;
        if (b) {
            axis = this.mHorizontalAxis;
        }
        else {
            axis = this.mVerticalAxis;
        }
        final Interval span = spec.span;
        boolean b4;
        if (b && this.isLayoutRtlCompat()) {
            b4 = !b2;
        }
        else {
            b4 = b2;
        }
        if (b4) {
            final boolean b5 = b3;
            if (span.min != 0) {
                return this.getDefaultMargin(view, b5, b, b2);
            }
        }
        else {
            final boolean b5 = b3;
            if (span.max != axis.getCount()) {
                return this.getDefaultMargin(view, b5, b, b2);
            }
        }
        final boolean b5 = true;
        return this.getDefaultMargin(view, b5, b, b2);
    }
    
    private int getDefaultMargin(final View view, final boolean b, final boolean b2) {
        if (view.getClass() != Space.class && view.getClass() != android.widget.Space.class) {
            return this.mDefaultGap / 2;
        }
        return 0;
    }
    
    private int getDefaultMargin(final View view, final boolean b, final boolean b2, final boolean b3) {
        return this.getDefaultMargin(view, b2, b3);
    }
    
    private int getMargin(final View view, final boolean b, final boolean b2) {
        if (this.mAlignmentMode == 1) {
            return this.getMargin1(view, b, b2);
        }
        Axis axis;
        if (b) {
            axis = this.mHorizontalAxis;
        }
        else {
            axis = this.mVerticalAxis;
        }
        int[] array;
        if (b2) {
            array = axis.getLeadingMargins();
        }
        else {
            array = axis.getTrailingMargins();
        }
        final LayoutParams layoutParams = this.getLayoutParams(view);
        Spec spec;
        if (b) {
            spec = layoutParams.columnSpec;
        }
        else {
            spec = layoutParams.rowSpec;
        }
        final Interval span = spec.span;
        int n;
        if (b2) {
            n = span.min;
        }
        else {
            n = span.max;
        }
        return array[n];
    }
    
    private int getMeasurement(final View view, final boolean b) {
        int n;
        if (b) {
            n = view.getMeasuredWidth();
        }
        else {
            n = view.getMeasuredHeight();
        }
        return n;
    }
    
    private int getTotalMargin(final View view, final boolean b) {
        return this.getMargin(view, b, true) + this.getMargin(view, b, false);
    }
    
    static void handleInvalidParams(final String str) {
        final StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append(". ");
        throw new IllegalArgumentException(sb.toString());
    }
    
    private void invalidateStructure() {
        this.mLastLayoutParamsHashCode = 0;
        final Axis mHorizontalAxis = this.mHorizontalAxis;
        if (mHorizontalAxis != null) {
            mHorizontalAxis.invalidateStructure();
        }
        final Axis mVerticalAxis = this.mVerticalAxis;
        if (mVerticalAxis != null) {
            mVerticalAxis.invalidateStructure();
        }
        this.invalidateValues();
    }
    
    private void invalidateValues() {
        final Axis mHorizontalAxis = this.mHorizontalAxis;
        if (mHorizontalAxis != null && this.mVerticalAxis != null) {
            mHorizontalAxis.invalidateValues();
            this.mVerticalAxis.invalidateValues();
        }
    }
    
    private boolean isLayoutRtlCompat() {
        final int layoutDirection = ViewCompat.getLayoutDirection((View)this);
        boolean b = true;
        if (layoutDirection != 1) {
            b = false;
        }
        return b;
    }
    
    static int max2(final int[] array, int i) {
        final int length = array.length;
        final int n = 0;
        int max = i;
        for (i = n; i < length; ++i) {
            max = Math.max(max, array[i]);
        }
        return max;
    }
    
    private void measureChildWithMargins2(final View view, final int n, final int n2, final int n3, final int n4) {
        view.measure(getChildMeasureSpec(n, this.getTotalMargin(view, true), n3), getChildMeasureSpec(n2, this.getTotalMargin(view, false), n4));
    }
    
    private void measureChildrenWithMargins(final int n, final int n2, final boolean b) {
        for (int childCount = this.getChildCount(), i = 0; i < childCount; ++i) {
            final View child = this.getChildAt(i);
            if (child.getVisibility() != 8) {
                final LayoutParams layoutParams = this.getLayoutParams(child);
                if (b) {
                    this.measureChildWithMargins2(child, n, n2, layoutParams.width, layoutParams.height);
                }
                else {
                    final boolean b2 = this.mOrientation == 0;
                    Spec spec;
                    if (b2) {
                        spec = layoutParams.columnSpec;
                    }
                    else {
                        spec = layoutParams.rowSpec;
                    }
                    if (spec.getAbsoluteAlignment(b2) == GridLayout.FILL) {
                        final Interval span = spec.span;
                        Axis axis;
                        if (b2) {
                            axis = this.mHorizontalAxis;
                        }
                        else {
                            axis = this.mVerticalAxis;
                        }
                        final int[] locations = axis.getLocations();
                        final int n3 = locations[span.max] - locations[span.min] - this.getTotalMargin(child, b2);
                        if (b2) {
                            this.measureChildWithMargins2(child, n, n2, n3, layoutParams.height);
                        }
                        else {
                            this.measureChildWithMargins2(child, n, n2, layoutParams.width, n3);
                        }
                    }
                }
            }
        }
    }
    
    private static void procrusteanFill(final int[] a, final int a2, final int a3, final int val) {
        final int length = a.length;
        Arrays.fill(a, Math.min(a2, length), Math.min(a3, length), val);
    }
    
    private static void setCellGroup(final LayoutParams layoutParams, final int n, final int n2, final int n3, final int n4) {
        layoutParams.setRowSpecSpan(new Interval(n, n2 + n));
        layoutParams.setColumnSpecSpan(new Interval(n3, n4 + n3));
    }
    
    public static Spec spec(final int n) {
        return spec(n, 1);
    }
    
    public static Spec spec(final int n, final float n2) {
        return spec(n, 1, n2);
    }
    
    public static Spec spec(final int n, final int n2) {
        return spec(n, n2, GridLayout.UNDEFINED_ALIGNMENT);
    }
    
    public static Spec spec(final int n, final int n2, final float n3) {
        return spec(n, n2, GridLayout.UNDEFINED_ALIGNMENT, n3);
    }
    
    public static Spec spec(final int n, final int n2, final Alignment alignment) {
        return spec(n, n2, alignment, 0.0f);
    }
    
    public static Spec spec(final int n, final int n2, final Alignment alignment, final float n3) {
        return new Spec(n != Integer.MIN_VALUE, n, n2, alignment, n3);
    }
    
    public static Spec spec(final int n, final Alignment alignment) {
        return spec(n, 1, alignment);
    }
    
    public static Spec spec(final int n, final Alignment alignment, final float n2) {
        return spec(n, 1, alignment, n2);
    }
    
    private void validateLayoutParams() {
        final boolean b = this.mOrientation == 0;
        Axis axis;
        if (b) {
            axis = this.mHorizontalAxis;
        }
        else {
            axis = this.mVerticalAxis;
        }
        int definedCount;
        if (axis.definedCount != Integer.MIN_VALUE) {
            definedCount = axis.definedCount;
        }
        else {
            definedCount = 0;
        }
        final int[] array = new int[definedCount];
        final int childCount = this.getChildCount();
        int i = 0;
        int min = 0;
        int min2 = 0;
        while (i < childCount) {
            final LayoutParams layoutParams = (LayoutParams)this.getChildAt(i).getLayoutParams();
            Spec spec;
            if (b) {
                spec = layoutParams.rowSpec;
            }
            else {
                spec = layoutParams.columnSpec;
            }
            final Interval span = spec.span;
            final boolean startDefined = spec.startDefined;
            final int size = span.size();
            if (startDefined) {
                min = span.min;
            }
            Spec spec2;
            if (b) {
                spec2 = layoutParams.columnSpec;
            }
            else {
                spec2 = layoutParams.rowSpec;
            }
            final Interval span2 = spec2.span;
            final boolean startDefined2 = spec2.startDefined;
            final int clip = clip(span2, startDefined2, definedCount);
            if (startDefined2) {
                min2 = span2.min;
            }
            int n = min;
            int n2 = min2;
            if (definedCount != 0) {
                int n3 = min;
                int n4 = min2;
                Label_0305: {
                    if (startDefined) {
                        n = min;
                        n2 = min2;
                        if (startDefined2) {
                            break Label_0305;
                        }
                        n4 = min2;
                        n3 = min;
                    }
                    while (true) {
                        final int n5 = n4 + clip;
                        n = n3;
                        n2 = n4;
                        if (fits(array, n3, n4, n5)) {
                            break;
                        }
                        if (startDefined2) {
                            ++n3;
                        }
                        else if (n5 <= definedCount) {
                            ++n4;
                        }
                        else {
                            ++n3;
                            n4 = 0;
                        }
                    }
                }
                procrusteanFill(array, n2, n2 + clip, n + size);
            }
            if (b) {
                setCellGroup(layoutParams, n, size, n2, clip);
            }
            else {
                setCellGroup(layoutParams, n2, clip, n, size);
            }
            min2 = n2 + clip;
            ++i;
            min = n;
        }
    }
    
    protected boolean checkLayoutParams(final ViewGroup$LayoutParams viewGroup$LayoutParams) {
        if (!(viewGroup$LayoutParams instanceof LayoutParams)) {
            return false;
        }
        final LayoutParams layoutParams = (LayoutParams)viewGroup$LayoutParams;
        this.checkLayoutParams(layoutParams, true);
        this.checkLayoutParams(layoutParams, false);
        return true;
    }
    
    protected LayoutParams generateDefaultLayoutParams() {
        return new LayoutParams();
    }
    
    public LayoutParams generateLayoutParams(final AttributeSet set) {
        return new LayoutParams(this.getContext(), set);
    }
    
    protected LayoutParams generateLayoutParams(final ViewGroup$LayoutParams viewGroup$LayoutParams) {
        if (viewGroup$LayoutParams instanceof LayoutParams) {
            return new LayoutParams((LayoutParams)viewGroup$LayoutParams);
        }
        if (viewGroup$LayoutParams instanceof ViewGroup$MarginLayoutParams) {
            return new LayoutParams((ViewGroup$MarginLayoutParams)viewGroup$LayoutParams);
        }
        return new LayoutParams(viewGroup$LayoutParams);
    }
    
    public int getAlignmentMode() {
        return this.mAlignmentMode;
    }
    
    public int getColumnCount() {
        return this.mHorizontalAxis.getCount();
    }
    
    final LayoutParams getLayoutParams(final View view) {
        return (LayoutParams)view.getLayoutParams();
    }
    
    int getMargin1(final View view, final boolean b, final boolean b2) {
        final LayoutParams layoutParams = this.getLayoutParams(view);
        int n;
        if (b) {
            if (b2) {
                n = layoutParams.leftMargin;
            }
            else {
                n = layoutParams.rightMargin;
            }
        }
        else if (b2) {
            n = layoutParams.topMargin;
        }
        else {
            n = layoutParams.bottomMargin;
        }
        int defaultMargin = n;
        if (n == Integer.MIN_VALUE) {
            defaultMargin = this.getDefaultMargin(view, layoutParams, b, b2);
        }
        return defaultMargin;
    }
    
    final int getMeasurementIncludingMargin(final View view, final boolean b) {
        if (view.getVisibility() == 8) {
            return 0;
        }
        return this.getMeasurement(view, b) + this.getTotalMargin(view, b);
    }
    
    public int getOrientation() {
        return this.mOrientation;
    }
    
    public Printer getPrinter() {
        return this.mPrinter;
    }
    
    public int getRowCount() {
        return this.mVerticalAxis.getCount();
    }
    
    public boolean getUseDefaultMargins() {
        return this.mUseDefaultMargins;
    }
    
    public boolean isColumnOrderPreserved() {
        return this.mHorizontalAxis.isOrderPreserved();
    }
    
    public boolean isRowOrderPreserved() {
        return this.mVerticalAxis.isOrderPreserved();
    }
    
    protected void onLayout(final boolean b, int i, int n, int n2, int childCount) {
        this.consistencyCheck();
        n2 -= i;
        final int paddingLeft = this.getPaddingLeft();
        final int paddingTop = this.getPaddingTop();
        final int paddingRight = this.getPaddingRight();
        i = this.getPaddingBottom();
        this.mHorizontalAxis.layout(n2 - paddingLeft - paddingRight);
        this.mVerticalAxis.layout(childCount - n - paddingTop - i);
        final int[] locations = this.mHorizontalAxis.getLocations();
        final int[] locations2 = this.mVerticalAxis.getLocations();
        View child;
        LayoutParams layoutParams;
        Spec columnSpec;
        Spec rowSpec;
        Interval span;
        Interval span2;
        int n3;
        int n4;
        int n5;
        int n6;
        int n7;
        int measurement;
        int measurement2;
        Alignment absoluteAlignment;
        Alignment absoluteAlignment2;
        Bounds bounds;
        Bounds bounds2;
        int gravityOffset;
        int gravityOffset2;
        int margin;
        int margin2;
        int margin3;
        int margin4;
        int n8;
        int n9;
        int offset;
        int offset2;
        int sizeInCell;
        int sizeInCell2;
        int n10;
        for (childCount = this.getChildCount(), i = 0; i < childCount; ++i) {
            child = this.getChildAt(i);
            if (child.getVisibility() != 8) {
                layoutParams = this.getLayoutParams(child);
                columnSpec = layoutParams.columnSpec;
                rowSpec = layoutParams.rowSpec;
                span = columnSpec.span;
                span2 = rowSpec.span;
                n = locations[span.min];
                n3 = locations2[span2.min];
                n4 = locations[span.max];
                n5 = locations2[span2.max];
                n6 = n4 - n;
                n7 = n5 - n3;
                measurement = this.getMeasurement(child, true);
                measurement2 = this.getMeasurement(child, false);
                absoluteAlignment = columnSpec.getAbsoluteAlignment(true);
                absoluteAlignment2 = rowSpec.getAbsoluteAlignment(false);
                bounds = (Bounds)this.mHorizontalAxis.getGroupBounds().getValue(i);
                bounds2 = (Bounds)this.mVerticalAxis.getGroupBounds().getValue(i);
                gravityOffset = absoluteAlignment.getGravityOffset(child, n6 - bounds.size(true));
                gravityOffset2 = absoluteAlignment2.getGravityOffset(child, n7 - bounds2.size(true));
                margin = this.getMargin(child, true, true);
                margin2 = this.getMargin(child, false, true);
                margin3 = this.getMargin(child, true, false);
                margin4 = this.getMargin(child, false, false);
                n8 = margin + margin3;
                n9 = margin2 + margin4;
                offset = bounds.getOffset(this, child, absoluteAlignment, measurement + n8, true);
                offset2 = bounds2.getOffset(this, child, absoluteAlignment2, measurement2 + n9, false);
                sizeInCell = absoluteAlignment.getSizeInCell(child, measurement, n6 - n8);
                sizeInCell2 = absoluteAlignment2.getSizeInCell(child, measurement2, n7 - n9);
                n = n + gravityOffset + offset;
                if (!this.isLayoutRtlCompat()) {
                    n += paddingLeft + margin;
                }
                else {
                    n = n2 - sizeInCell - paddingRight - margin3 - n;
                }
                n10 = paddingTop + n3 + gravityOffset2 + offset2 + margin2;
                if (sizeInCell != child.getMeasuredWidth() || sizeInCell2 != child.getMeasuredHeight()) {
                    child.measure(View$MeasureSpec.makeMeasureSpec(sizeInCell, 1073741824), View$MeasureSpec.makeMeasureSpec(sizeInCell2, 1073741824));
                }
                child.layout(n, n10, sizeInCell + n, sizeInCell2 + n10);
            }
        }
    }
    
    protected void onMeasure(final int n, final int n2) {
        this.consistencyCheck();
        this.invalidateValues();
        final int n3 = this.getPaddingLeft() + this.getPaddingRight();
        final int n4 = this.getPaddingTop() + this.getPaddingBottom();
        final int adjust = adjust(n, -n3);
        final int adjust2 = adjust(n2, -n4);
        this.measureChildrenWithMargins(adjust, adjust2, true);
        int n5;
        int n6;
        if (this.mOrientation == 0) {
            n5 = this.mHorizontalAxis.getMeasure(adjust);
            this.measureChildrenWithMargins(adjust, adjust2, false);
            n6 = this.mVerticalAxis.getMeasure(adjust2);
        }
        else {
            n6 = this.mVerticalAxis.getMeasure(adjust2);
            this.measureChildrenWithMargins(adjust, adjust2, false);
            n5 = this.mHorizontalAxis.getMeasure(adjust);
        }
        this.setMeasuredDimension(View.resolveSizeAndState(Math.max(n5 + n3, this.getSuggestedMinimumWidth()), n, 0), View.resolveSizeAndState(Math.max(n6 + n4, this.getSuggestedMinimumHeight()), n2, 0));
    }
    
    public void requestLayout() {
        super.requestLayout();
        this.invalidateStructure();
    }
    
    public void setAlignmentMode(final int mAlignmentMode) {
        this.mAlignmentMode = mAlignmentMode;
        this.requestLayout();
    }
    
    public void setColumnCount(final int count) {
        this.mHorizontalAxis.setCount(count);
        this.invalidateStructure();
        this.requestLayout();
    }
    
    public void setColumnOrderPreserved(final boolean orderPreserved) {
        this.mHorizontalAxis.setOrderPreserved(orderPreserved);
        this.invalidateStructure();
        this.requestLayout();
    }
    
    public void setOrientation(final int mOrientation) {
        if (this.mOrientation != mOrientation) {
            this.mOrientation = mOrientation;
            this.invalidateStructure();
            this.requestLayout();
        }
    }
    
    public void setPrinter(final Printer printer) {
        Printer no_PRINTER = printer;
        if (printer == null) {
            no_PRINTER = GridLayout.NO_PRINTER;
        }
        this.mPrinter = no_PRINTER;
    }
    
    public void setRowCount(final int count) {
        this.mVerticalAxis.setCount(count);
        this.invalidateStructure();
        this.requestLayout();
    }
    
    public void setRowOrderPreserved(final boolean orderPreserved) {
        this.mVerticalAxis.setOrderPreserved(orderPreserved);
        this.invalidateStructure();
        this.requestLayout();
    }
    
    public void setUseDefaultMargins(final boolean mUseDefaultMargins) {
        this.mUseDefaultMargins = mUseDefaultMargins;
        this.requestLayout();
    }
    
    public abstract static class Alignment
    {
        Alignment() {
        }
        
        abstract int getAlignmentValue(final View p0, final int p1, final int p2);
        
        Bounds getBounds() {
            return new Bounds();
        }
        
        abstract String getDebugString();
        
        abstract int getGravityOffset(final View p0, final int p1);
        
        int getSizeInCell(final View view, final int n, final int n2) {
            return n;
        }
        
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder();
            sb.append("Alignment:");
            sb.append(this.getDebugString());
            return sb.toString();
        }
    }
    
    static final class Arc
    {
        public final Interval span;
        public boolean valid;
        public final MutableInt value;
        
        public Arc(final Interval span, final MutableInt value) {
            this.valid = true;
            this.span = span;
            this.value = value;
        }
        
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder();
            sb.append(this.span);
            sb.append(" ");
            String str;
            if (!this.valid) {
                str = "+>";
            }
            else {
                str = "->";
            }
            sb.append(str);
            sb.append(" ");
            sb.append(this.value);
            return sb.toString();
        }
    }
    
    static final class Assoc<K, V> extends ArrayList<Pair<K, V>>
    {
        private final Class<K> keyType;
        private final Class<V> valueType;
        
        private Assoc(final Class<K> keyType, final Class<V> valueType) {
            this.keyType = keyType;
            this.valueType = valueType;
        }
        
        public static <K, V> Assoc<K, V> of(final Class<K> clazz, final Class<V> clazz2) {
            return new Assoc<K, V>(clazz, clazz2);
        }
        
        public PackedMap<K, V> pack() {
            final int size = this.size();
            final Object[] array = (Object[])Array.newInstance(this.keyType, size);
            final Object[] array2 = (Object[])Array.newInstance(this.valueType, size);
            for (int i = 0; i < size; ++i) {
                array[i] = this.get(i).first;
                array2[i] = this.get(i).second;
            }
            return (PackedMap<K, V>)new PackedMap(array, array2);
        }
        
        public void put(final K k, final V v) {
            this.add((Pair<K, V>)Pair.create((Object)k, (Object)v));
        }
    }
    
    final class Axis
    {
        static final boolean $assertionsDisabled = false;
        static final int COMPLETE = 2;
        static final int NEW = 0;
        static final int PENDING = 1;
        public Arc[] arcs;
        public boolean arcsValid;
        PackedMap<Interval, MutableInt> backwardLinks;
        public boolean backwardLinksValid;
        public int definedCount;
        public int[] deltas;
        PackedMap<Interval, MutableInt> forwardLinks;
        public boolean forwardLinksValid;
        PackedMap<Spec, Bounds> groupBounds;
        public boolean groupBoundsValid;
        public boolean hasWeights;
        public boolean hasWeightsValid;
        public final boolean horizontal;
        public int[] leadingMargins;
        public boolean leadingMarginsValid;
        public int[] locations;
        public boolean locationsValid;
        private int maxIndex;
        boolean orderPreserved;
        private MutableInt parentMax;
        private MutableInt parentMin;
        final GridLayout this$0;
        public int[] trailingMargins;
        public boolean trailingMarginsValid;
        
        Axis(final GridLayout this$0, final boolean horizontal) {
            this.this$0 = this$0;
            this.definedCount = Integer.MIN_VALUE;
            this.maxIndex = Integer.MIN_VALUE;
            this.groupBoundsValid = false;
            this.forwardLinksValid = false;
            this.backwardLinksValid = false;
            this.leadingMarginsValid = false;
            this.trailingMarginsValid = false;
            this.arcsValid = false;
            this.locationsValid = false;
            this.hasWeightsValid = false;
            this.orderPreserved = true;
            this.parentMin = new MutableInt(0);
            this.parentMax = new MutableInt(-100000);
            this.horizontal = horizontal;
        }
        
        private void addComponentSizes(final List<Arc> list, final PackedMap<Interval, MutableInt> packedMap) {
            for (int i = 0; i < packedMap.keys.length; ++i) {
                this.include(list, packedMap.keys[i], packedMap.values[i], false);
            }
        }
        
        private String arcsToString(final List<Arc> list) {
            String s;
            if (this.horizontal) {
                s = "x";
            }
            else {
                s = "y";
            }
            final StringBuilder sb = new StringBuilder();
            int n = 1;
            for (final Arc arc : list) {
                if (n != 0) {
                    n = 0;
                }
                else {
                    sb.append(", ");
                }
                final int min = arc.span.min;
                final int max = arc.span.max;
                int value = arc.value.value;
                StringBuilder sb2;
                if (min < max) {
                    sb2 = new StringBuilder();
                    sb2.append(s);
                    sb2.append(max);
                    sb2.append("-");
                    sb2.append(s);
                    sb2.append(min);
                    sb2.append(">=");
                }
                else {
                    sb2 = new StringBuilder();
                    sb2.append(s);
                    sb2.append(min);
                    sb2.append("-");
                    sb2.append(s);
                    sb2.append(max);
                    sb2.append("<=");
                    value = -value;
                }
                sb2.append(value);
                sb.append(sb2.toString());
            }
            return sb.toString();
        }
        
        private int calculateMaxIndex() {
            final int childCount = this.this$0.getChildCount();
            int i = 0;
            int max = -1;
            while (i < childCount) {
                final LayoutParams layoutParams = this.this$0.getLayoutParams(this.this$0.getChildAt(i));
                Spec spec;
                if (this.horizontal) {
                    spec = layoutParams.columnSpec;
                }
                else {
                    spec = layoutParams.rowSpec;
                }
                final Interval span = spec.span;
                max = Math.max(Math.max(Math.max(max, span.min), span.max), span.size());
                ++i;
            }
            int n;
            if ((n = max) == -1) {
                n = Integer.MIN_VALUE;
            }
            return n;
        }
        
        private float calculateTotalWeight() {
            final int childCount = this.this$0.getChildCount();
            float n = 0.0f;
            for (int i = 0; i < childCount; ++i) {
                final View child = this.this$0.getChildAt(i);
                if (child.getVisibility() != 8) {
                    final LayoutParams layoutParams = this.this$0.getLayoutParams(child);
                    Spec spec;
                    if (this.horizontal) {
                        spec = layoutParams.columnSpec;
                    }
                    else {
                        spec = layoutParams.rowSpec;
                    }
                    n += spec.weight;
                }
            }
            return n;
        }
        
        private void computeArcs() {
            this.getForwardLinks();
            this.getBackwardLinks();
        }
        
        private void computeGroupBounds() {
            final Bounds[] array = this.groupBounds.values;
            for (int i = 0; i < array.length; ++i) {
                array[i].reset();
            }
            for (int childCount = this.this$0.getChildCount(), j = 0; j < childCount; ++j) {
                final View child = this.this$0.getChildAt(j);
                final LayoutParams layoutParams = this.this$0.getLayoutParams(child);
                Spec spec;
                if (this.horizontal) {
                    spec = layoutParams.columnSpec;
                }
                else {
                    spec = layoutParams.rowSpec;
                }
                final int measurementIncludingMargin = this.this$0.getMeasurementIncludingMargin(child, this.horizontal);
                int n;
                if (spec.weight == 0.0f) {
                    n = 0;
                }
                else {
                    n = this.getDeltas()[j];
                }
                this.groupBounds.getValue(j).include(this.this$0, child, spec, this, measurementIncludingMargin + n);
            }
        }
        
        private boolean computeHasWeights() {
            for (int childCount = this.this$0.getChildCount(), i = 0; i < childCount; ++i) {
                final View child = this.this$0.getChildAt(i);
                if (child.getVisibility() != 8) {
                    final LayoutParams layoutParams = this.this$0.getLayoutParams(child);
                    Spec spec;
                    if (this.horizontal) {
                        spec = layoutParams.columnSpec;
                    }
                    else {
                        spec = layoutParams.rowSpec;
                    }
                    if (spec.weight != 0.0f) {
                        return true;
                    }
                }
            }
            return false;
        }
        
        private void computeLinks(final PackedMap<Interval, MutableInt> packedMap, final boolean b) {
            final MutableInt[] array = packedMap.values;
            final int n = 0;
            for (int i = 0; i < array.length; ++i) {
                array[i].reset();
            }
            final Bounds[] array2 = this.getGroupBounds().values;
            for (int j = n; j < array2.length; ++j) {
                int size = array2[j].size(b);
                final MutableInt mutableInt = packedMap.getValue(j);
                final int value = mutableInt.value;
                if (!b) {
                    size = -size;
                }
                mutableInt.value = Math.max(value, size);
            }
        }
        
        private void computeLocations(final int[] array) {
            if (!this.hasWeights()) {
                this.solve(array);
            }
            else {
                this.solveAndDistributeSpace(array);
            }
            if (!this.orderPreserved) {
                int i = 0;
                final int n = array[0];
                while (i < array.length) {
                    array[i] -= n;
                    ++i;
                }
            }
        }
        
        private void computeMargins(final boolean b) {
            int[] array;
            if (b) {
                array = this.leadingMargins;
            }
            else {
                array = this.trailingMargins;
            }
            for (int i = 0; i < this.this$0.getChildCount(); ++i) {
                final View child = this.this$0.getChildAt(i);
                if (child.getVisibility() != 8) {
                    final LayoutParams layoutParams = this.this$0.getLayoutParams(child);
                    Spec spec;
                    if (this.horizontal) {
                        spec = layoutParams.columnSpec;
                    }
                    else {
                        spec = layoutParams.rowSpec;
                    }
                    final Interval span = spec.span;
                    int n;
                    if (b) {
                        n = span.min;
                    }
                    else {
                        n = span.max;
                    }
                    array[n] = Math.max(array[n], this.this$0.getMargin1(child, this.horizontal, b));
                }
            }
        }
        
        private Arc[] createArcs() {
            final ArrayList list = new ArrayList();
            final ArrayList list2 = new ArrayList();
            this.addComponentSizes(list, this.getForwardLinks());
            this.addComponentSizes(list2, this.getBackwardLinks());
            if (this.orderPreserved) {
                int n;
                for (int i = 0; i < this.getCount(); i = n) {
                    n = i + 1;
                    this.include(list, new Interval(i, n), new MutableInt(0));
                }
            }
            final int count = this.getCount();
            this.include(list, new Interval(0, count), this.parentMin, false);
            this.include(list2, new Interval(count, 0), this.parentMax, false);
            return GridLayout.append(this.topologicalSort(list), this.topologicalSort(list2));
        }
        
        private PackedMap<Spec, Bounds> createGroupBounds() {
            final Assoc<Spec, Bounds> of = Assoc.of(Spec.class, Bounds.class);
            for (int childCount = this.this$0.getChildCount(), i = 0; i < childCount; ++i) {
                final LayoutParams layoutParams = this.this$0.getLayoutParams(this.this$0.getChildAt(i));
                Spec spec;
                if (this.horizontal) {
                    spec = layoutParams.columnSpec;
                }
                else {
                    spec = layoutParams.rowSpec;
                }
                of.put(spec, spec.getAbsoluteAlignment(this.horizontal).getBounds());
            }
            return of.pack();
        }
        
        private PackedMap<Interval, MutableInt> createLinks(final boolean b) {
            final Assoc<Interval, MutableInt> of = Assoc.of(Interval.class, MutableInt.class);
            final Spec[] array = this.getGroupBounds().keys;
            for (int length = array.length, i = 0; i < length; ++i) {
                Object o;
                if (b) {
                    o = array[i].span;
                }
                else {
                    o = array[i].span.inverse();
                }
                of.put((Interval)o, new MutableInt());
            }
            return of.pack();
        }
        
        private PackedMap<Interval, MutableInt> getBackwardLinks() {
            if (this.backwardLinks == null) {
                this.backwardLinks = this.createLinks(false);
            }
            if (!this.backwardLinksValid) {
                this.computeLinks(this.backwardLinks, false);
                this.backwardLinksValid = true;
            }
            return this.backwardLinks;
        }
        
        private PackedMap<Interval, MutableInt> getForwardLinks() {
            if (this.forwardLinks == null) {
                this.forwardLinks = this.createLinks(true);
            }
            if (!this.forwardLinksValid) {
                this.computeLinks(this.forwardLinks, true);
                this.forwardLinksValid = true;
            }
            return this.forwardLinks;
        }
        
        private int getMaxIndex() {
            if (this.maxIndex == Integer.MIN_VALUE) {
                this.maxIndex = Math.max(0, this.calculateMaxIndex());
            }
            return this.maxIndex;
        }
        
        private int getMeasure(final int n, final int n2) {
            this.setParentConstraints(n, n2);
            return this.size(this.getLocations());
        }
        
        private boolean hasWeights() {
            if (!this.hasWeightsValid) {
                this.hasWeights = this.computeHasWeights();
                this.hasWeightsValid = true;
            }
            return this.hasWeights;
        }
        
        private void include(final List<Arc> list, final Interval interval, final MutableInt mutableInt) {
            this.include(list, interval, mutableInt, true);
        }
        
        private void include(final List<Arc> list, final Interval interval, final MutableInt mutableInt, final boolean b) {
            if (interval.size() == 0) {
                return;
            }
            if (b) {
                final Iterator<Arc> iterator = list.iterator();
                while (iterator.hasNext()) {
                    if (iterator.next().span.equals(interval)) {
                        return;
                    }
                }
            }
            list.add(new Arc(interval, mutableInt));
        }
        
        private void init(final int[] a) {
            Arrays.fill(a, 0);
        }
        
        private void logError(final String str, final Arc[] array, final boolean[] array2) {
            final ArrayList list = new ArrayList();
            final ArrayList list2 = new ArrayList();
            for (int i = 0; i < array.length; ++i) {
                final Arc arc = array[i];
                if (array2[i]) {
                    list.add(arc);
                }
                if (!arc.valid) {
                    list2.add(arc);
                }
            }
            final Printer mPrinter = this.this$0.mPrinter;
            final StringBuilder sb = new StringBuilder();
            sb.append(str);
            sb.append(" constraints: ");
            sb.append(this.arcsToString(list));
            sb.append(" are inconsistent; permanently removing: ");
            sb.append(this.arcsToString(list2));
            sb.append(". ");
            mPrinter.println(sb.toString());
        }
        
        private boolean relax(final int[] array, final Arc arc) {
            if (!arc.valid) {
                return false;
            }
            final Interval span = arc.span;
            final int min = span.min;
            final int max = span.max;
            final int n = array[min] + arc.value.value;
            if (n > array[max]) {
                array[max] = n;
                return true;
            }
            return false;
        }
        
        private void setParentConstraints(final int value, final int n) {
            this.parentMin.value = value;
            this.parentMax.value = -n;
            this.locationsValid = false;
        }
        
        private void shareOutDelta(int i, float n) {
            final int[] deltas = this.deltas;
            final int n2 = 0;
            Arrays.fill(deltas, 0);
            final int childCount = this.this$0.getChildCount();
            float n3 = n;
            int n4 = i;
            View child;
            int n5;
            LayoutParams layoutParams;
            Spec spec;
            float weight;
            int round;
            for (i = n2; i < childCount; ++i, n4 = n5, n3 = n) {
                child = this.this$0.getChildAt(i);
                if (child.getVisibility() == 8) {
                    n5 = n4;
                    n = n3;
                }
                else {
                    layoutParams = this.this$0.getLayoutParams(child);
                    if (this.horizontal) {
                        spec = layoutParams.columnSpec;
                    }
                    else {
                        spec = layoutParams.rowSpec;
                    }
                    weight = spec.weight;
                    n5 = n4;
                    n = n3;
                    if (weight != 0.0f) {
                        round = Math.round(n4 * weight / n3);
                        this.deltas[i] = round;
                        n5 = n4 - round;
                        n = n3 - weight;
                    }
                }
            }
        }
        
        private int size(final int[] array) {
            return array[this.getCount()];
        }
        
        private boolean solve(final int[] array) {
            return this.solve(this.getArcs(), array);
        }
        
        private boolean solve(final Arc[] array, final int[] array2) {
            return this.solve(array, array2, true);
        }
        
        private boolean solve(final Arc[] array, final int[] array2, final boolean b) {
            String s;
            if (this.horizontal) {
                s = "horizontal";
            }
            else {
                s = "vertical";
            }
            final int n = this.getCount() + 1;
            boolean[] array3 = null;
            for (int i = 0; i < array.length; ++i) {
                this.init(array2);
                for (int j = 0; j < n; ++j) {
                    final int length = array.length;
                    int k = 0;
                    boolean b2 = false;
                    while (k < length) {
                        b2 |= this.relax(array2, array[k]);
                        ++k;
                    }
                    if (!b2) {
                        if (array3 != null) {
                            this.logError(s, array, array3);
                        }
                        return true;
                    }
                }
                if (!b) {
                    return false;
                }
                final boolean[] array4 = new boolean[array.length];
                for (int l = 0; l < n; ++l) {
                    for (int length2 = array.length, n2 = 0; n2 < length2; ++n2) {
                        array4[n2] |= this.relax(array2, array[n2]);
                    }
                }
                if (i == 0) {
                    array3 = array4;
                }
                for (int n3 = 0; n3 < array.length; ++n3) {
                    if (array4[n3]) {
                        final Arc arc = array[n3];
                        if (arc.span.min >= arc.span.max) {
                            arc.valid = false;
                            break;
                        }
                    }
                }
            }
            return true;
        }
        
        private void solveAndDistributeSpace(final int[] array) {
            Arrays.fill(this.getDeltas(), 0);
            this.solve(array);
            final int value = this.parentMin.value;
            final int childCount = this.this$0.getChildCount();
            boolean solve = true;
            int n = value * childCount + 1;
            if (n < 2) {
                return;
            }
            final float calculateTotalWeight = this.calculateTotalWeight();
            int n2 = -1;
            int i = 0;
            while (i < n) {
                final int n3 = (int)((i + (long)n) / 2L);
                this.invalidateValues();
                this.shareOutDelta(n3, calculateTotalWeight);
                solve = this.solve(this.getArcs(), array, false);
                if (solve) {
                    i = n3 + 1;
                    n2 = n3;
                }
                else {
                    n = n3;
                }
            }
            if (n2 > 0 && !solve) {
                this.invalidateValues();
                this.shareOutDelta(n2, calculateTotalWeight);
                this.solve(array);
            }
        }
        
        private Arc[] topologicalSort(final List<Arc> list) {
            return this.topologicalSort(list.toArray(new Arc[list.size()]));
        }
        
        private Arc[] topologicalSort(final Arc[] array) {
            return new Object(this, array) {
                static final boolean $assertionsDisabled = false;
                Arc[][] arcsByVertex;
                int cursor;
                Arc[] result;
                final Axis this$1;
                final Arc[] val$arcs;
                int[] visited;
                
                {
                    final Arc[] result = new Arc[val$arcs.length];
                    this.result = result;
                    this.cursor = result.length - 1;
                    this.arcsByVertex = this$1.groupArcsByFirstVertex(val$arcs);
                    this.visited = new int[this$1.getCount() + 1];
                }
                
                Arc[] sort() {
                    for (int length = this.arcsByVertex.length, i = 0; i < length; ++i) {
                        this.walk(i);
                    }
                    return this.result;
                }
                
                void walk(final int n) {
                    final int[] visited = this.visited;
                    if (visited[n] == 0) {
                        visited[n] = 1;
                        for (final Arc arc : this.arcsByVertex[n]) {
                            this.walk(arc.span.max);
                            this.result[this.cursor--] = arc;
                        }
                        this.visited[n] = 2;
                    }
                }
            }.sort();
        }
        
        public Arc[] getArcs() {
            if (this.arcs == null) {
                this.arcs = this.createArcs();
            }
            if (!this.arcsValid) {
                this.computeArcs();
                this.arcsValid = true;
            }
            return this.arcs;
        }
        
        public int getCount() {
            return Math.max(this.definedCount, this.getMaxIndex());
        }
        
        public int[] getDeltas() {
            if (this.deltas == null) {
                this.deltas = new int[this.this$0.getChildCount()];
            }
            return this.deltas;
        }
        
        public PackedMap<Spec, Bounds> getGroupBounds() {
            if (this.groupBounds == null) {
                this.groupBounds = this.createGroupBounds();
            }
            if (!this.groupBoundsValid) {
                this.computeGroupBounds();
                this.groupBoundsValid = true;
            }
            return this.groupBounds;
        }
        
        public int[] getLeadingMargins() {
            if (this.leadingMargins == null) {
                this.leadingMargins = new int[this.getCount() + 1];
            }
            if (!this.leadingMarginsValid) {
                this.computeMargins(true);
                this.leadingMarginsValid = true;
            }
            return this.leadingMargins;
        }
        
        public int[] getLocations() {
            if (this.locations == null) {
                this.locations = new int[this.getCount() + 1];
            }
            if (!this.locationsValid) {
                this.computeLocations(this.locations);
                this.locationsValid = true;
            }
            return this.locations;
        }
        
        public int getMeasure(int size) {
            final int mode = View$MeasureSpec.getMode(size);
            size = View$MeasureSpec.getSize(size);
            if (mode == Integer.MIN_VALUE) {
                return this.getMeasure(0, size);
            }
            if (mode == 0) {
                return this.getMeasure(0, 100000);
            }
            if (mode != 1073741824) {
                return 0;
            }
            return this.getMeasure(size, size);
        }
        
        public int[] getTrailingMargins() {
            if (this.trailingMargins == null) {
                this.trailingMargins = new int[this.getCount() + 1];
            }
            if (!this.trailingMarginsValid) {
                this.computeMargins(false);
                this.trailingMarginsValid = true;
            }
            return this.trailingMargins;
        }
        
        Arc[][] groupArcsByFirstVertex(final Arc[] array) {
            final int n = this.getCount() + 1;
            final Arc[][] array2 = new Arc[n][];
            final int[] a = new int[n];
            final int length = array.length;
            final int n2 = 0;
            for (int i = 0; i < length; ++i) {
                final int min = array[i].span.min;
                ++a[min];
            }
            for (int j = 0; j < n; ++j) {
                array2[j] = new Arc[a[j]];
            }
            Arrays.fill(a, 0);
            for (int length2 = array.length, k = n2; k < length2; ++k) {
                final Arc arc = array[k];
                final int min2 = arc.span.min;
                array2[min2][a[min2]++] = arc;
            }
            return array2;
        }
        
        public void invalidateStructure() {
            this.maxIndex = Integer.MIN_VALUE;
            this.groupBounds = null;
            this.forwardLinks = null;
            this.backwardLinks = null;
            this.leadingMargins = null;
            this.trailingMargins = null;
            this.arcs = null;
            this.locations = null;
            this.deltas = null;
            this.hasWeightsValid = false;
            this.invalidateValues();
        }
        
        public void invalidateValues() {
            this.groupBoundsValid = false;
            this.forwardLinksValid = false;
            this.backwardLinksValid = false;
            this.leadingMarginsValid = false;
            this.trailingMarginsValid = false;
            this.arcsValid = false;
            this.locationsValid = false;
        }
        
        public boolean isOrderPreserved() {
            return this.orderPreserved;
        }
        
        public void layout(final int n) {
            this.setParentConstraints(n, n);
            this.getLocations();
        }
        
        public void setCount(final int definedCount) {
            if (definedCount != Integer.MIN_VALUE && definedCount < this.getMaxIndex()) {
                final StringBuilder sb = new StringBuilder();
                String str;
                if (this.horizontal) {
                    str = "column";
                }
                else {
                    str = "row";
                }
                sb.append(str);
                sb.append("Count must be greater than or equal to the maximum of all grid indices ");
                sb.append("(and spans) defined in the LayoutParams of each child");
                GridLayout.handleInvalidParams(sb.toString());
            }
            this.definedCount = definedCount;
        }
        
        public void setOrderPreserved(final boolean orderPreserved) {
            this.orderPreserved = orderPreserved;
            this.invalidateStructure();
        }
    }
    
    static class Bounds
    {
        public int after;
        public int before;
        public int flexibility;
        
        Bounds() {
            this.reset();
        }
        
        protected int getOffset(final GridLayout gridLayout, final View view, final Alignment alignment, final int n, final boolean b) {
            return this.before - alignment.getAlignmentValue(view, n, ViewGroupCompat.getLayoutMode(gridLayout));
        }
        
        protected void include(final int b, final int b2) {
            this.before = Math.max(this.before, b);
            this.after = Math.max(this.after, b2);
        }
        
        protected final void include(final GridLayout gridLayout, final View view, final Spec spec, final Axis axis, final int n) {
            this.flexibility &= spec.getFlexibility();
            final int alignmentValue = spec.getAbsoluteAlignment(axis.horizontal).getAlignmentValue(view, n, ViewGroupCompat.getLayoutMode(gridLayout));
            this.include(alignmentValue, n - alignmentValue);
        }
        
        protected void reset() {
            this.before = Integer.MIN_VALUE;
            this.after = Integer.MIN_VALUE;
            this.flexibility = 2;
        }
        
        protected int size(final boolean b) {
            if (!b && GridLayout.canStretch(this.flexibility)) {
                return 100000;
            }
            return this.before + this.after;
        }
        
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder();
            sb.append("Bounds{before=");
            sb.append(this.before);
            sb.append(", after=");
            sb.append(this.after);
            sb.append('}');
            return sb.toString();
        }
    }
    
    static final class Interval
    {
        public final int max;
        public final int min;
        
        public Interval(final int min, final int max) {
            this.min = min;
            this.max = max;
        }
        
        @Override
        public boolean equals(final Object o) {
            if (this == o) {
                return true;
            }
            if (o != null && this.getClass() == o.getClass()) {
                final Interval interval = (Interval)o;
                return this.max == interval.max && this.min == interval.min;
            }
            return false;
        }
        
        @Override
        public int hashCode() {
            return this.min * 31 + this.max;
        }
        
        Interval inverse() {
            return new Interval(this.max, this.min);
        }
        
        int size() {
            return this.max - this.min;
        }
        
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder();
            sb.append("[");
            sb.append(this.min);
            sb.append(", ");
            sb.append(this.max);
            sb.append("]");
            return sb.toString();
        }
    }
    
    public static class LayoutParams extends ViewGroup$MarginLayoutParams
    {
        private static final int BOTTOM_MARGIN;
        private static final int COLUMN;
        private static final int COLUMN_SPAN;
        private static final int COLUMN_WEIGHT;
        private static final int DEFAULT_COLUMN = Integer.MIN_VALUE;
        private static final int DEFAULT_HEIGHT = -2;
        private static final int DEFAULT_MARGIN = Integer.MIN_VALUE;
        private static final int DEFAULT_ROW = Integer.MIN_VALUE;
        private static final Interval DEFAULT_SPAN;
        private static final int DEFAULT_SPAN_SIZE;
        private static final int DEFAULT_WIDTH = -2;
        private static final int GRAVITY;
        private static final int LEFT_MARGIN;
        private static final int MARGIN;
        private static final int RIGHT_MARGIN;
        private static final int ROW;
        private static final int ROW_SPAN;
        private static final int ROW_WEIGHT;
        private static final int TOP_MARGIN;
        public Spec columnSpec;
        public Spec rowSpec;
        
        static {
            DEFAULT_SPAN_SIZE = (DEFAULT_SPAN = new Interval(Integer.MIN_VALUE, -2147483647)).size();
            MARGIN = R.styleable.GridLayout_Layout_android_layout_margin;
            LEFT_MARGIN = R.styleable.GridLayout_Layout_android_layout_marginLeft;
            TOP_MARGIN = R.styleable.GridLayout_Layout_android_layout_marginTop;
            RIGHT_MARGIN = R.styleable.GridLayout_Layout_android_layout_marginRight;
            BOTTOM_MARGIN = R.styleable.GridLayout_Layout_android_layout_marginBottom;
            COLUMN = R.styleable.GridLayout_Layout_layout_column;
            COLUMN_SPAN = R.styleable.GridLayout_Layout_layout_columnSpan;
            COLUMN_WEIGHT = R.styleable.GridLayout_Layout_layout_columnWeight;
            ROW = R.styleable.GridLayout_Layout_layout_row;
            ROW_SPAN = R.styleable.GridLayout_Layout_layout_rowSpan;
            ROW_WEIGHT = R.styleable.GridLayout_Layout_layout_rowWeight;
            GRAVITY = R.styleable.GridLayout_Layout_layout_gravity;
        }
        
        public LayoutParams() {
            this(Spec.UNDEFINED, Spec.UNDEFINED);
        }
        
        private LayoutParams(final int n, final int n2, final int n3, final int n4, final int n5, final int n6, final Spec rowSpec, final Spec columnSpec) {
            super(n, n2);
            this.rowSpec = Spec.UNDEFINED;
            this.columnSpec = Spec.UNDEFINED;
            this.setMargins(n3, n4, n5, n6);
            this.rowSpec = rowSpec;
            this.columnSpec = columnSpec;
        }
        
        public LayoutParams(final Context context, final AttributeSet set) {
            super(context, set);
            this.rowSpec = Spec.UNDEFINED;
            this.columnSpec = Spec.UNDEFINED;
            this.reInitSuper(context, set);
            this.init(context, set);
        }
        
        public LayoutParams(final ViewGroup$LayoutParams viewGroup$LayoutParams) {
            super(viewGroup$LayoutParams);
            this.rowSpec = Spec.UNDEFINED;
            this.columnSpec = Spec.UNDEFINED;
        }
        
        public LayoutParams(final ViewGroup$MarginLayoutParams viewGroup$MarginLayoutParams) {
            super(viewGroup$MarginLayoutParams);
            this.rowSpec = Spec.UNDEFINED;
            this.columnSpec = Spec.UNDEFINED;
        }
        
        public LayoutParams(final LayoutParams layoutParams) {
            super((ViewGroup$MarginLayoutParams)layoutParams);
            this.rowSpec = Spec.UNDEFINED;
            this.columnSpec = Spec.UNDEFINED;
            this.rowSpec = layoutParams.rowSpec;
            this.columnSpec = layoutParams.columnSpec;
        }
        
        public LayoutParams(final Spec spec, final Spec spec2) {
            this(-2, -2, Integer.MIN_VALUE, Integer.MIN_VALUE, Integer.MIN_VALUE, Integer.MIN_VALUE, spec, spec2);
        }
        
        private void init(final Context context, AttributeSet obtainStyledAttributes) {
            obtainStyledAttributes = (AttributeSet)context.obtainStyledAttributes(obtainStyledAttributes, R.styleable.GridLayout_Layout);
            try {
                final int int1 = ((TypedArray)obtainStyledAttributes).getInt(LayoutParams.GRAVITY, 0);
                final int int2 = ((TypedArray)obtainStyledAttributes).getInt(LayoutParams.COLUMN, Integer.MIN_VALUE);
                final int column_SPAN = LayoutParams.COLUMN_SPAN;
                final int default_SPAN_SIZE = LayoutParams.DEFAULT_SPAN_SIZE;
                this.columnSpec = GridLayout.spec(int2, ((TypedArray)obtainStyledAttributes).getInt(column_SPAN, default_SPAN_SIZE), GridLayout.getAlignment(int1, true), ((TypedArray)obtainStyledAttributes).getFloat(LayoutParams.COLUMN_WEIGHT, 0.0f));
                this.rowSpec = GridLayout.spec(((TypedArray)obtainStyledAttributes).getInt(LayoutParams.ROW, Integer.MIN_VALUE), ((TypedArray)obtainStyledAttributes).getInt(LayoutParams.ROW_SPAN, default_SPAN_SIZE), GridLayout.getAlignment(int1, false), ((TypedArray)obtainStyledAttributes).getFloat(LayoutParams.ROW_WEIGHT, 0.0f));
            }
            finally {
                ((TypedArray)obtainStyledAttributes).recycle();
            }
        }
        
        private void reInitSuper(final Context context, AttributeSet obtainStyledAttributes) {
            obtainStyledAttributes = (AttributeSet)context.obtainStyledAttributes(obtainStyledAttributes, R.styleable.GridLayout_Layout);
            try {
                final int dimensionPixelSize = ((TypedArray)obtainStyledAttributes).getDimensionPixelSize(LayoutParams.MARGIN, Integer.MIN_VALUE);
                this.leftMargin = ((TypedArray)obtainStyledAttributes).getDimensionPixelSize(LayoutParams.LEFT_MARGIN, dimensionPixelSize);
                this.topMargin = ((TypedArray)obtainStyledAttributes).getDimensionPixelSize(LayoutParams.TOP_MARGIN, dimensionPixelSize);
                this.rightMargin = ((TypedArray)obtainStyledAttributes).getDimensionPixelSize(LayoutParams.RIGHT_MARGIN, dimensionPixelSize);
                this.bottomMargin = ((TypedArray)obtainStyledAttributes).getDimensionPixelSize(LayoutParams.BOTTOM_MARGIN, dimensionPixelSize);
            }
            finally {
                ((TypedArray)obtainStyledAttributes).recycle();
            }
        }
        
        public boolean equals(final Object o) {
            if (this == o) {
                return true;
            }
            if (o != null && this.getClass() == o.getClass()) {
                final LayoutParams layoutParams = (LayoutParams)o;
                return this.columnSpec.equals(layoutParams.columnSpec) && this.rowSpec.equals(layoutParams.rowSpec);
            }
            return false;
        }
        
        public int hashCode() {
            return this.rowSpec.hashCode() * 31 + this.columnSpec.hashCode();
        }
        
        protected void setBaseAttributes(final TypedArray typedArray, final int n, final int n2) {
            this.width = typedArray.getLayoutDimension(n, -2);
            this.height = typedArray.getLayoutDimension(n2, -2);
        }
        
        final void setColumnSpecSpan(final Interval interval) {
            this.columnSpec = this.columnSpec.copyWriteSpan(interval);
        }
        
        public void setGravity(final int n) {
            this.rowSpec = this.rowSpec.copyWriteAlignment(GridLayout.getAlignment(n, false));
            this.columnSpec = this.columnSpec.copyWriteAlignment(GridLayout.getAlignment(n, true));
        }
        
        final void setRowSpecSpan(final Interval interval) {
            this.rowSpec = this.rowSpec.copyWriteSpan(interval);
        }
    }
    
    static final class MutableInt
    {
        public int value;
        
        public MutableInt() {
            this.reset();
        }
        
        public MutableInt(final int value) {
            this.value = value;
        }
        
        public void reset() {
            this.value = Integer.MIN_VALUE;
        }
        
        @Override
        public String toString() {
            return Integer.toString(this.value);
        }
    }
    
    static final class PackedMap<K, V>
    {
        public final int[] index;
        public final K[] keys;
        public final V[] values;
        
        PackedMap(final K[] array, final V[] array2) {
            final int[] index = createIndex(array);
            this.index = index;
            this.keys = compact(array, index);
            this.values = compact(array2, index);
        }
        
        private static <K> K[] compact(final K[] array, final int[] array2) {
            final int length = array.length;
            final Object[] array3 = (Object[])Array.newInstance(array.getClass().getComponentType(), GridLayout.max2(array2, -1) + 1);
            for (int i = 0; i < length; ++i) {
                array3[array2[i]] = array[i];
            }
            return (K[])array3;
        }
        
        private static <K> int[] createIndex(final K[] array) {
            final int length = array.length;
            final int[] array2 = new int[length];
            final HashMap hashMap = new HashMap();
            for (int i = 0; i < length; ++i) {
                final K k = array[i];
                Integer value;
                if ((value = (Integer)hashMap.get(k)) == null) {
                    value = hashMap.size();
                    hashMap.put(k, value);
                }
                array2[i] = value;
            }
            return array2;
        }
        
        public V getValue(final int n) {
            return this.values[this.index[n]];
        }
    }
    
    public static class Spec
    {
        static final float DEFAULT_WEIGHT = 0.0f;
        static final Spec UNDEFINED;
        final Alignment alignment;
        final Interval span;
        final boolean startDefined;
        final float weight;
        
        static {
            UNDEFINED = GridLayout.spec(Integer.MIN_VALUE);
        }
        
        Spec(final boolean b, final int n, final int n2, final Alignment alignment, final float n3) {
            this(b, new Interval(n, n2 + n), alignment, n3);
        }
        
        private Spec(final boolean startDefined, final Interval span, final Alignment alignment, final float weight) {
            this.startDefined = startDefined;
            this.span = span;
            this.alignment = alignment;
            this.weight = weight;
        }
        
        final Spec copyWriteAlignment(final Alignment alignment) {
            return new Spec(this.startDefined, this.span, alignment, this.weight);
        }
        
        final Spec copyWriteSpan(final Interval interval) {
            return new Spec(this.startDefined, interval, this.alignment, this.weight);
        }
        
        @Override
        public boolean equals(final Object o) {
            if (this == o) {
                return true;
            }
            if (o != null && this.getClass() == o.getClass()) {
                final Spec spec = (Spec)o;
                return this.alignment.equals(spec.alignment) && this.span.equals(spec.span);
            }
            return false;
        }
        
        public Alignment getAbsoluteAlignment(final boolean b) {
            if (this.alignment != GridLayout.UNDEFINED_ALIGNMENT) {
                return this.alignment;
            }
            if (this.weight == 0.0f) {
                Alignment alignment;
                if (b) {
                    alignment = GridLayout.START;
                }
                else {
                    alignment = GridLayout.BASELINE;
                }
                return alignment;
            }
            return GridLayout.FILL;
        }
        
        final int getFlexibility() {
            int n;
            if (this.alignment == GridLayout.UNDEFINED_ALIGNMENT && this.weight == 0.0f) {
                n = 0;
            }
            else {
                n = 2;
            }
            return n;
        }
        
        @Override
        public int hashCode() {
            return this.span.hashCode() * 31 + this.alignment.hashCode();
        }
    }
}

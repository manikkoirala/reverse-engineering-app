// 
// Decompiled by Procyon v0.6.0
// 

package androidx.loader.content;

import java.util.concurrent.Executor;
import android.os.Looper;
import android.util.Log;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.CancellationException;
import android.os.Binder;
import android.os.Process;
import java.util.concurrent.Callable;
import java.util.concurrent.FutureTask;
import java.util.concurrent.atomic.AtomicBoolean;
import android.os.Handler;

abstract class ModernAsyncTask<Result>
{
    private static final String LOG_TAG = "AsyncTask";
    private static Handler sHandler;
    final AtomicBoolean mCancelled;
    private final FutureTask<Result> mFuture;
    private volatile Status mStatus;
    final AtomicBoolean mTaskInvoked;
    
    ModernAsyncTask() {
        this.mStatus = Status.PENDING;
        this.mCancelled = new AtomicBoolean();
        this.mTaskInvoked = new AtomicBoolean();
        this.mFuture = new FutureTask<Result>(this, new Callable<Result>(this) {
            final ModernAsyncTask this$0;
            
            @Override
            public Result call() {
                this.this$0.mTaskInvoked.set(true);
                Result doInBackground = null;
                try {
                    Process.setThreadPriority(10);
                    doInBackground = doInBackground;
                    final Result result = doInBackground = this.this$0.doInBackground();
                    Binder.flushPendingCommands();
                    this.this$0.postResult(result);
                    return result;
                }
                finally {
                    try {
                        this.this$0.mCancelled.set(true);
                    }
                    finally {
                        this.this$0.postResult(doInBackground);
                    }
                }
            }
        }) {
            final ModernAsyncTask this$0;
            
            @Override
            protected void done() {
                try {
                    this.this$0.postResultIfNotInvoked(this.get());
                }
                catch (final CancellationException ex) {
                    this.this$0.postResultIfNotInvoked(null);
                }
                catch (final ExecutionException ex2) {
                    throw new RuntimeException("An error occurred while executing doInBackground()", ex2.getCause());
                }
                catch (final InterruptedException ex3) {
                    Log.w("AsyncTask", (Throwable)ex3);
                }
                finally {
                    final Throwable cause;
                    throw new RuntimeException("An error occurred while executing doInBackground()", cause);
                }
            }
        };
    }
    
    private static Handler getHandler() {
        synchronized (ModernAsyncTask.class) {
            if (ModernAsyncTask.sHandler == null) {
                ModernAsyncTask.sHandler = new Handler(Looper.getMainLooper());
            }
            return ModernAsyncTask.sHandler;
        }
    }
    
    public final boolean cancel(final boolean mayInterruptIfRunning) {
        this.mCancelled.set(true);
        return this.mFuture.cancel(mayInterruptIfRunning);
    }
    
    protected abstract Result doInBackground();
    
    public final void executeOnExecutor(final Executor executor) {
        if (this.mStatus == Status.PENDING) {
            this.mStatus = Status.RUNNING;
            executor.execute(this.mFuture);
            return;
        }
        final int n = ModernAsyncTask$4.$SwitchMap$androidx$loader$content$ModernAsyncTask$Status[this.mStatus.ordinal()];
        if (n == 1) {
            throw new IllegalStateException("Cannot execute task: the task is already running.");
        }
        if (n != 2) {
            throw new IllegalStateException("We should never reach this state");
        }
        throw new IllegalStateException("Cannot execute task: the task has already been executed (a task can be executed only once)");
    }
    
    void finish(final Result result) {
        if (this.isCancelled()) {
            this.onCancelled(result);
        }
        else {
            this.onPostExecute(result);
        }
        this.mStatus = Status.FINISHED;
    }
    
    public final boolean isCancelled() {
        return this.mCancelled.get();
    }
    
    protected void onCancelled(final Result result) {
    }
    
    protected void onPostExecute(final Result result) {
    }
    
    void postResult(final Result result) {
        getHandler().post((Runnable)new Runnable(this, result) {
            final ModernAsyncTask this$0;
            final Object val$result;
            
            @Override
            public void run() {
                this.this$0.finish(this.val$result);
            }
        });
    }
    
    void postResultIfNotInvoked(final Result result) {
        if (!this.mTaskInvoked.get()) {
            this.postResult(result);
        }
    }
    
    public enum Status
    {
        private static final Status[] $VALUES;
        
        FINISHED, 
        PENDING, 
        RUNNING;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.loader.content;

import androidx.core.os.OperationCanceledException;
import android.os.AsyncTask;
import android.text.format.DateUtils;
import java.util.concurrent.TimeUnit;
import java.io.PrintWriter;
import java.io.FileDescriptor;
import android.os.SystemClock;
import android.content.Context;
import android.os.Handler;
import java.util.concurrent.Executor;

public abstract class AsyncTaskLoader<D> extends Loader<D>
{
    private static final boolean DEBUG = false;
    private static final String TAG = "AsyncTaskLoader";
    private volatile LoadTask mCancellingTask;
    private Executor mExecutor;
    private Handler mHandler;
    private long mLastLoadCompleteTime;
    private volatile LoadTask mTask;
    private long mUpdateThrottle;
    
    public AsyncTaskLoader(final Context context) {
        super(context);
        this.mLastLoadCompleteTime = -10000L;
    }
    
    public void cancelLoadInBackground() {
    }
    
    void dispatchOnCancelled(final LoadTask loadTask, final D n) {
        this.onCanceled(n);
        if (this.mCancellingTask == loadTask) {
            this.rollbackContentChanged();
            this.mLastLoadCompleteTime = SystemClock.uptimeMillis();
            this.mCancellingTask = null;
            this.deliverCancellation();
            this.executePendingTask();
        }
    }
    
    void dispatchOnLoadComplete(final LoadTask loadTask, final D n) {
        if (this.mTask != loadTask) {
            this.dispatchOnCancelled(loadTask, n);
        }
        else if (this.isAbandoned()) {
            this.onCanceled(n);
        }
        else {
            this.commitContentChanged();
            this.mLastLoadCompleteTime = SystemClock.uptimeMillis();
            this.mTask = null;
            this.deliverResult(n);
        }
    }
    
    @Deprecated
    @Override
    public void dump(String string, final FileDescriptor fileDescriptor, final PrintWriter printWriter, final String[] array) {
        super.dump(string, fileDescriptor, printWriter, array);
        if (this.mTask != null) {
            printWriter.print(string);
            printWriter.print("mTask=");
            printWriter.print(this.mTask);
            printWriter.print(" waiting=");
            printWriter.println(this.mTask.waiting);
        }
        if (this.mCancellingTask != null) {
            printWriter.print(string);
            printWriter.print("mCancellingTask=");
            printWriter.print(this.mCancellingTask);
            printWriter.print(" waiting=");
            printWriter.println(this.mCancellingTask.waiting);
        }
        if (this.mUpdateThrottle != 0L) {
            printWriter.print(string);
            printWriter.print("mUpdateThrottle=");
            printWriter.print(DateUtils.formatElapsedTime(TimeUnit.MILLISECONDS.toSeconds(this.mUpdateThrottle)));
            printWriter.print(" mLastLoadCompleteTime=");
            if (this.mLastLoadCompleteTime == -10000L) {
                string = "--";
            }
            else {
                final StringBuilder sb = new StringBuilder();
                sb.append("-");
                sb.append(DateUtils.formatElapsedTime(TimeUnit.MILLISECONDS.toSeconds(SystemClock.uptimeMillis() - this.mLastLoadCompleteTime)));
                string = sb.toString();
            }
            printWriter.print(string);
            printWriter.println();
        }
    }
    
    void executePendingTask() {
        if (this.mCancellingTask == null && this.mTask != null) {
            if (this.mTask.waiting) {
                this.mTask.waiting = false;
                this.mHandler.removeCallbacks((Runnable)this.mTask);
            }
            if (this.mUpdateThrottle > 0L && SystemClock.uptimeMillis() < this.mLastLoadCompleteTime + this.mUpdateThrottle) {
                this.mTask.waiting = true;
                this.mHandler.postAtTime((Runnable)this.mTask, this.mLastLoadCompleteTime + this.mUpdateThrottle);
                return;
            }
            if (this.mExecutor == null) {
                this.mExecutor = this.getExecutor();
            }
            this.mTask.executeOnExecutor(this.mExecutor);
        }
    }
    
    protected Executor getExecutor() {
        return AsyncTask.THREAD_POOL_EXECUTOR;
    }
    
    public boolean isLoadInBackgroundCanceled() {
        return this.mCancellingTask != null;
    }
    
    public abstract D loadInBackground();
    
    @Override
    protected boolean onCancelLoad() {
        if (this.mTask == null) {
            return false;
        }
        if (!this.isStarted()) {
            this.onContentChanged();
        }
        if (this.mCancellingTask != null) {
            if (this.mTask.waiting) {
                this.mTask.waiting = false;
                this.mHandler.removeCallbacks((Runnable)this.mTask);
            }
            this.mTask = null;
            return false;
        }
        if (this.mTask.waiting) {
            this.mTask.waiting = false;
            this.mHandler.removeCallbacks((Runnable)this.mTask);
            this.mTask = null;
            return false;
        }
        final boolean cancel = this.mTask.cancel(false);
        if (cancel) {
            this.mCancellingTask = this.mTask;
            this.cancelLoadInBackground();
        }
        this.mTask = null;
        return cancel;
    }
    
    public void onCanceled(final D n) {
    }
    
    @Override
    protected void onForceLoad() {
        super.onForceLoad();
        this.cancelLoad();
        this.mTask = new LoadTask();
        this.executePendingTask();
    }
    
    protected D onLoadInBackground() {
        return this.loadInBackground();
    }
    
    public void setUpdateThrottle(final long mUpdateThrottle) {
        this.mUpdateThrottle = mUpdateThrottle;
        if (mUpdateThrottle != 0L) {
            this.mHandler = new Handler();
        }
    }
    
    final class LoadTask extends ModernAsyncTask<D> implements Runnable
    {
        final AsyncTaskLoader this$0;
        boolean waiting;
        
        LoadTask(final AsyncTaskLoader this$0) {
            this.this$0 = this$0;
        }
        
        @Override
        protected D doInBackground() {
            try {
                return this.this$0.onLoadInBackground();
            }
            catch (final OperationCanceledException ex) {
                if (this.isCancelled()) {
                    return null;
                }
                throw ex;
            }
        }
        
        @Override
        protected void onCancelled(final D n) {
            this.this$0.dispatchOnCancelled(this, n);
        }
        
        @Override
        protected void onPostExecute(final D n) {
            this.this$0.dispatchOnLoadComplete(this, n);
        }
        
        @Override
        public void run() {
            this.waiting = false;
            this.this$0.executePendingTask();
        }
    }
}

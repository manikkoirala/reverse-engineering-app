// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.utils.widget;

import android.graphics.Path$Direction;
import android.graphics.Outline;
import android.graphics.Paint$FontMetricsInt;
import android.graphics.Paint$Style;
import android.graphics.Shader;
import android.view.View$MeasureSpec;
import android.util.Log;
import androidx.constraintlayout.motion.widget.Debug;
import android.graphics.Shader$TileMode;
import android.graphics.Canvas;
import android.graphics.Bitmap$Config;
import android.util.TypedValue;
import android.graphics.Typeface;
import android.content.res.TypedArray;
import android.os.Build$VERSION;
import androidx.constraintlayout.widget.R;
import android.graphics.Paint$FontMetrics;
import android.util.AttributeSet;
import android.content.Context;
import android.view.ViewOutlineProvider;
import android.graphics.BitmapShader;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.graphics.Rect;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Path;
import android.text.TextPaint;
import android.graphics.Matrix;
import android.text.Layout;
import androidx.constraintlayout.motion.widget.FloatLayout;
import android.view.View;

public class MotionLabel extends View implements FloatLayout
{
    private static final int MONOSPACE = 3;
    private static final int SANS = 1;
    private static final int SERIF = 2;
    static String TAG = "MotionLabel";
    private boolean mAutoSize;
    private int mAutoSizeTextType;
    float mBackgroundPanX;
    float mBackgroundPanY;
    private float mBaseTextSize;
    private float mDeltaLeft;
    private float mFloatHeight;
    private float mFloatWidth;
    private String mFontFamily;
    private int mGravity;
    private Layout mLayout;
    boolean mNotBuilt;
    Matrix mOutlinePositionMatrix;
    private int mPaddingBottom;
    private int mPaddingLeft;
    private int mPaddingRight;
    private int mPaddingTop;
    TextPaint mPaint;
    Path mPath;
    RectF mRect;
    float mRotate;
    private float mRound;
    private float mRoundPercent;
    private int mStyleIndex;
    Paint mTempPaint;
    Rect mTempRect;
    private String mText;
    private Drawable mTextBackground;
    private Bitmap mTextBackgroundBitmap;
    private Rect mTextBounds;
    private int mTextFillColor;
    private int mTextOutlineColor;
    private float mTextOutlineThickness;
    private float mTextPanX;
    private float mTextPanY;
    private BitmapShader mTextShader;
    private Matrix mTextShaderMatrix;
    private float mTextSize;
    private int mTextureEffect;
    private float mTextureHeight;
    private float mTextureWidth;
    private CharSequence mTransformed;
    private int mTypefaceIndex;
    private boolean mUseOutline;
    ViewOutlineProvider mViewOutlineProvider;
    float mZoom;
    Paint paintCache;
    float paintTextSize;
    
    public MotionLabel(final Context context) {
        super(context);
        this.mPaint = new TextPaint();
        this.mPath = new Path();
        this.mTextFillColor = 65535;
        this.mTextOutlineColor = 65535;
        this.mUseOutline = false;
        this.mRoundPercent = 0.0f;
        this.mRound = Float.NaN;
        this.mTextSize = 48.0f;
        this.mBaseTextSize = Float.NaN;
        this.mTextOutlineThickness = 0.0f;
        this.mText = "Hello World";
        this.mNotBuilt = true;
        this.mTextBounds = new Rect();
        this.mPaddingLeft = 1;
        this.mPaddingRight = 1;
        this.mPaddingTop = 1;
        this.mPaddingBottom = 1;
        this.mGravity = 8388659;
        this.mAutoSizeTextType = 0;
        this.mAutoSize = false;
        this.mTextureHeight = Float.NaN;
        this.mTextureWidth = Float.NaN;
        this.mTextPanX = 0.0f;
        this.mTextPanY = 0.0f;
        this.paintCache = new Paint();
        this.mTextureEffect = 0;
        this.mBackgroundPanX = Float.NaN;
        this.mBackgroundPanY = Float.NaN;
        this.mZoom = Float.NaN;
        this.mRotate = Float.NaN;
        this.init(context, null);
    }
    
    public MotionLabel(final Context context, final AttributeSet set) {
        super(context, set);
        this.mPaint = new TextPaint();
        this.mPath = new Path();
        this.mTextFillColor = 65535;
        this.mTextOutlineColor = 65535;
        this.mUseOutline = false;
        this.mRoundPercent = 0.0f;
        this.mRound = Float.NaN;
        this.mTextSize = 48.0f;
        this.mBaseTextSize = Float.NaN;
        this.mTextOutlineThickness = 0.0f;
        this.mText = "Hello World";
        this.mNotBuilt = true;
        this.mTextBounds = new Rect();
        this.mPaddingLeft = 1;
        this.mPaddingRight = 1;
        this.mPaddingTop = 1;
        this.mPaddingBottom = 1;
        this.mGravity = 8388659;
        this.mAutoSizeTextType = 0;
        this.mAutoSize = false;
        this.mTextureHeight = Float.NaN;
        this.mTextureWidth = Float.NaN;
        this.mTextPanX = 0.0f;
        this.mTextPanY = 0.0f;
        this.paintCache = new Paint();
        this.mTextureEffect = 0;
        this.mBackgroundPanX = Float.NaN;
        this.mBackgroundPanY = Float.NaN;
        this.mZoom = Float.NaN;
        this.mRotate = Float.NaN;
        this.init(context, set);
    }
    
    public MotionLabel(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
        this.mPaint = new TextPaint();
        this.mPath = new Path();
        this.mTextFillColor = 65535;
        this.mTextOutlineColor = 65535;
        this.mUseOutline = false;
        this.mRoundPercent = 0.0f;
        this.mRound = Float.NaN;
        this.mTextSize = 48.0f;
        this.mBaseTextSize = Float.NaN;
        this.mTextOutlineThickness = 0.0f;
        this.mText = "Hello World";
        this.mNotBuilt = true;
        this.mTextBounds = new Rect();
        this.mPaddingLeft = 1;
        this.mPaddingRight = 1;
        this.mPaddingTop = 1;
        this.mPaddingBottom = 1;
        this.mGravity = 8388659;
        this.mAutoSizeTextType = 0;
        this.mAutoSize = false;
        this.mTextureHeight = Float.NaN;
        this.mTextureWidth = Float.NaN;
        this.mTextPanX = 0.0f;
        this.mTextPanY = 0.0f;
        this.paintCache = new Paint();
        this.mTextureEffect = 0;
        this.mBackgroundPanX = Float.NaN;
        this.mBackgroundPanY = Float.NaN;
        this.mZoom = Float.NaN;
        this.mRotate = Float.NaN;
        this.init(context, set);
    }
    
    private void adjustTexture(final float n, final float n2, final float n3, final float n4) {
        if (this.mTextShaderMatrix == null) {
            return;
        }
        this.mFloatWidth = n3 - n;
        this.mFloatHeight = n4 - n2;
        this.updateShaderMatrix();
    }
    
    private float getHorizontalOffset() {
        float n;
        if (Float.isNaN(this.mBaseTextSize)) {
            n = 1.0f;
        }
        else {
            n = this.mTextSize / this.mBaseTextSize;
        }
        final TextPaint mPaint = this.mPaint;
        final String mText = this.mText;
        final float measureText = mPaint.measureText(mText, 0, mText.length());
        float mFloatWidth;
        if (Float.isNaN(this.mFloatWidth)) {
            mFloatWidth = (float)this.getMeasuredWidth();
        }
        else {
            mFloatWidth = this.mFloatWidth;
        }
        return (mFloatWidth - this.getPaddingLeft() - this.getPaddingRight() - n * measureText) * (this.mTextPanX + 1.0f) / 2.0f;
    }
    
    private float getVerticalOffset() {
        float n;
        if (Float.isNaN(this.mBaseTextSize)) {
            n = 1.0f;
        }
        else {
            n = this.mTextSize / this.mBaseTextSize;
        }
        final Paint$FontMetrics fontMetrics = this.mPaint.getFontMetrics();
        float mFloatHeight;
        if (Float.isNaN(this.mFloatHeight)) {
            mFloatHeight = (float)this.getMeasuredHeight();
        }
        else {
            mFloatHeight = this.mFloatHeight;
        }
        return (mFloatHeight - this.getPaddingTop() - this.getPaddingBottom() - (fontMetrics.descent - fontMetrics.ascent) * n) * (1.0f - this.mTextPanY) / 2.0f - n * fontMetrics.ascent;
    }
    
    private void init(final Context context, final AttributeSet set) {
        this.setUpTheme(context, set);
        if (set != null) {
            final TypedArray obtainStyledAttributes = this.getContext().obtainStyledAttributes(set, R.styleable.MotionLabel);
            for (int indexCount = obtainStyledAttributes.getIndexCount(), i = 0; i < indexCount; ++i) {
                final int index = obtainStyledAttributes.getIndex(i);
                if (index == R.styleable.MotionLabel_android_text) {
                    this.setText(obtainStyledAttributes.getText(index));
                }
                else if (index == R.styleable.MotionLabel_android_fontFamily) {
                    this.mFontFamily = obtainStyledAttributes.getString(index);
                }
                else if (index == R.styleable.MotionLabel_scaleFromTextSize) {
                    this.mBaseTextSize = (float)obtainStyledAttributes.getDimensionPixelSize(index, (int)this.mBaseTextSize);
                }
                else if (index == R.styleable.MotionLabel_android_textSize) {
                    this.mTextSize = (float)obtainStyledAttributes.getDimensionPixelSize(index, (int)this.mTextSize);
                }
                else if (index == R.styleable.MotionLabel_android_textStyle) {
                    this.mStyleIndex = obtainStyledAttributes.getInt(index, this.mStyleIndex);
                }
                else if (index == R.styleable.MotionLabel_android_typeface) {
                    this.mTypefaceIndex = obtainStyledAttributes.getInt(index, this.mTypefaceIndex);
                }
                else if (index == R.styleable.MotionLabel_android_textColor) {
                    this.mTextFillColor = obtainStyledAttributes.getColor(index, this.mTextFillColor);
                }
                else if (index == R.styleable.MotionLabel_borderRound) {
                    this.mRound = obtainStyledAttributes.getDimension(index, this.mRound);
                    if (Build$VERSION.SDK_INT >= 21) {
                        this.setRound(this.mRound);
                    }
                }
                else if (index == R.styleable.MotionLabel_borderRoundPercent) {
                    this.mRoundPercent = obtainStyledAttributes.getFloat(index, this.mRoundPercent);
                    if (Build$VERSION.SDK_INT >= 21) {
                        this.setRoundPercent(this.mRoundPercent);
                    }
                }
                else if (index == R.styleable.MotionLabel_android_gravity) {
                    this.setGravity(obtainStyledAttributes.getInt(index, -1));
                }
                else if (index == R.styleable.MotionLabel_android_autoSizeTextType) {
                    this.mAutoSizeTextType = obtainStyledAttributes.getInt(index, 0);
                }
                else if (index == R.styleable.MotionLabel_textOutlineColor) {
                    this.mTextOutlineColor = obtainStyledAttributes.getInt(index, this.mTextOutlineColor);
                    this.mUseOutline = true;
                }
                else if (index == R.styleable.MotionLabel_textOutlineThickness) {
                    this.mTextOutlineThickness = obtainStyledAttributes.getDimension(index, this.mTextOutlineThickness);
                    this.mUseOutline = true;
                }
                else if (index == R.styleable.MotionLabel_textBackground) {
                    this.mTextBackground = obtainStyledAttributes.getDrawable(index);
                    this.mUseOutline = true;
                }
                else if (index == R.styleable.MotionLabel_textBackgroundPanX) {
                    this.mBackgroundPanX = obtainStyledAttributes.getFloat(index, this.mBackgroundPanX);
                }
                else if (index == R.styleable.MotionLabel_textBackgroundPanY) {
                    this.mBackgroundPanY = obtainStyledAttributes.getFloat(index, this.mBackgroundPanY);
                }
                else if (index == R.styleable.MotionLabel_textPanX) {
                    this.mTextPanX = obtainStyledAttributes.getFloat(index, this.mTextPanX);
                }
                else if (index == R.styleable.MotionLabel_textPanY) {
                    this.mTextPanY = obtainStyledAttributes.getFloat(index, this.mTextPanY);
                }
                else if (index == R.styleable.MotionLabel_textBackgroundRotate) {
                    this.mRotate = obtainStyledAttributes.getFloat(index, this.mRotate);
                }
                else if (index == R.styleable.MotionLabel_textBackgroundZoom) {
                    this.mZoom = obtainStyledAttributes.getFloat(index, this.mZoom);
                }
                else if (index == R.styleable.MotionLabel_textureHeight) {
                    this.mTextureHeight = obtainStyledAttributes.getDimension(index, this.mTextureHeight);
                }
                else if (index == R.styleable.MotionLabel_textureWidth) {
                    this.mTextureWidth = obtainStyledAttributes.getDimension(index, this.mTextureWidth);
                }
                else if (index == R.styleable.MotionLabel_textureEffect) {
                    this.mTextureEffect = obtainStyledAttributes.getInt(index, this.mTextureEffect);
                }
            }
            obtainStyledAttributes.recycle();
        }
        this.setupTexture();
        this.setupPath();
    }
    
    private void setTypefaceFromAttrs(final String s, int style, final int n) {
        if (s != null) {
            final Typeface create = Typeface.create(s, n);
            final Typeface typeface;
            if ((typeface = create) != null) {
                this.setTypeface(create);
                return;
            }
        }
        else {
            final Typeface typeface = null;
        }
        boolean fakeBoldText = true;
        Typeface typeface;
        if (style != 1) {
            if (style != 2) {
                if (style == 3) {
                    typeface = Typeface.MONOSPACE;
                }
            }
            else {
                typeface = Typeface.SERIF;
            }
        }
        else {
            typeface = Typeface.SANS_SERIF;
        }
        float textSkewX = 0.0f;
        if (n > 0) {
            Typeface typeface2;
            if (typeface == null) {
                typeface2 = Typeface.defaultFromStyle(n);
            }
            else {
                typeface2 = Typeface.create(typeface, n);
            }
            this.setTypeface(typeface2);
            if (typeface2 != null) {
                style = typeface2.getStyle();
            }
            else {
                style = 0;
            }
            style = (~style & n);
            final TextPaint mPaint = this.mPaint;
            if ((style & 0x1) == 0x0) {
                fakeBoldText = false;
            }
            mPaint.setFakeBoldText(fakeBoldText);
            final TextPaint mPaint2 = this.mPaint;
            if ((style & 0x2) != 0x0) {
                textSkewX = -0.25f;
            }
            mPaint2.setTextSkewX(textSkewX);
        }
        else {
            this.mPaint.setFakeBoldText(false);
            this.mPaint.setTextSkewX(0.0f);
            this.setTypeface(typeface);
        }
    }
    
    private void setUpTheme(final Context context, final AttributeSet set) {
        final TypedValue typedValue = new TypedValue();
        context.getTheme().resolveAttribute(androidx.appcompat.R.attr.colorPrimary, typedValue, true);
        this.mPaint.setColor(this.mTextFillColor = typedValue.data);
    }
    
    private void setupTexture() {
        if (this.mTextBackground != null) {
            this.mTextShaderMatrix = new Matrix();
            final int intrinsicWidth = this.mTextBackground.getIntrinsicWidth();
            final int intrinsicHeight = this.mTextBackground.getIntrinsicHeight();
            final int n = 128;
            int width;
            if ((width = intrinsicWidth) <= 0 && (width = this.getWidth()) == 0) {
                if (Float.isNaN(this.mTextureWidth)) {
                    width = 128;
                }
                else {
                    width = (int)this.mTextureWidth;
                }
            }
            int height;
            if ((height = intrinsicHeight) <= 0 && (height = this.getHeight()) == 0) {
                if (Float.isNaN(this.mTextureHeight)) {
                    height = n;
                }
                else {
                    height = (int)this.mTextureHeight;
                }
            }
            int n2 = width;
            int n3 = height;
            if (this.mTextureEffect != 0) {
                n2 = width / 2;
                n3 = height / 2;
            }
            this.mTextBackgroundBitmap = Bitmap.createBitmap(n2, n3, Bitmap$Config.ARGB_8888);
            final Canvas canvas = new Canvas(this.mTextBackgroundBitmap);
            this.mTextBackground.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
            this.mTextBackground.setFilterBitmap(true);
            this.mTextBackground.draw(canvas);
            if (this.mTextureEffect != 0) {
                this.mTextBackgroundBitmap = this.blur(this.mTextBackgroundBitmap, 4);
            }
            this.mTextShader = new BitmapShader(this.mTextBackgroundBitmap, Shader$TileMode.REPEAT, Shader$TileMode.REPEAT);
        }
    }
    
    private void updateShaderMatrix() {
        final boolean naN = Float.isNaN(this.mBackgroundPanX);
        float mRotate = 0.0f;
        float mBackgroundPanX;
        if (naN) {
            mBackgroundPanX = 0.0f;
        }
        else {
            mBackgroundPanX = this.mBackgroundPanX;
        }
        float mBackgroundPanY;
        if (Float.isNaN(this.mBackgroundPanY)) {
            mBackgroundPanY = 0.0f;
        }
        else {
            mBackgroundPanY = this.mBackgroundPanY;
        }
        float mZoom;
        if (Float.isNaN(this.mZoom)) {
            mZoom = 1.0f;
        }
        else {
            mZoom = this.mZoom;
        }
        if (!Float.isNaN(this.mRotate)) {
            mRotate = this.mRotate;
        }
        this.mTextShaderMatrix.reset();
        final float n = (float)this.mTextBackgroundBitmap.getWidth();
        final float n2 = (float)this.mTextBackgroundBitmap.getHeight();
        float n3;
        if (Float.isNaN(this.mTextureWidth)) {
            n3 = this.mFloatWidth;
        }
        else {
            n3 = this.mTextureWidth;
        }
        float n4;
        if (Float.isNaN(this.mTextureHeight)) {
            n4 = this.mFloatHeight;
        }
        else {
            n4 = this.mTextureHeight;
        }
        float n5;
        if (n * n4 < n2 * n3) {
            n5 = n3 / n;
        }
        else {
            n5 = n4 / n2;
        }
        final float n6 = mZoom * n5;
        this.mTextShaderMatrix.postScale(n6, n6);
        final float n7 = n * n6;
        float n8 = n3 - n7;
        final float n9 = n6 * n2;
        float n10 = n4 - n9;
        if (!Float.isNaN(this.mTextureHeight)) {
            n10 = this.mTextureHeight / 2.0f;
        }
        if (!Float.isNaN(this.mTextureWidth)) {
            n8 = this.mTextureWidth / 2.0f;
        }
        this.mTextShaderMatrix.postTranslate((mBackgroundPanX * n8 + n3 - n7) * 0.5f, (mBackgroundPanY * n10 + n4 - n9) * 0.5f);
        this.mTextShaderMatrix.postRotate(mRotate, n3 / 2.0f, n4 / 2.0f);
        this.mTextShader.setLocalMatrix(this.mTextShaderMatrix);
    }
    
    Bitmap blur(Bitmap bitmap, final int n) {
        System.nanoTime();
        final int width = bitmap.getWidth();
        final int height = bitmap.getHeight();
        int n2 = width / 2;
        int n3 = height / 2;
        bitmap = Bitmap.createScaledBitmap(bitmap, n2, n3, true);
        for (int n4 = 0; n4 < n && n2 >= 32 && n3 >= 32; n2 /= 2, n3 /= 2, bitmap = Bitmap.createScaledBitmap(bitmap, n2, n3, true), ++n4) {}
        return bitmap;
    }
    
    void buildShape(final float f) {
        if (!this.mUseOutline && f == 1.0f) {
            return;
        }
        this.mPath.reset();
        final String mText = this.mText;
        final int length = mText.length();
        this.mPaint.getTextBounds(mText, 0, length, this.mTextBounds);
        this.mPaint.getTextPath(mText, 0, length, 0.0f, 0.0f, this.mPath);
        if (f != 1.0f) {
            final String tag = MotionLabel.TAG;
            final StringBuilder sb = new StringBuilder();
            sb.append(Debug.getLoc());
            sb.append(" scale ");
            sb.append(f);
            Log.v(tag, sb.toString());
            final Matrix matrix = new Matrix();
            matrix.postScale(f, f);
            this.mPath.transform(matrix);
        }
        final Rect mTextBounds = this.mTextBounds;
        --mTextBounds.right;
        final Rect mTextBounds2 = this.mTextBounds;
        ++mTextBounds2.left;
        final Rect mTextBounds3 = this.mTextBounds;
        ++mTextBounds3.bottom;
        final Rect mTextBounds4 = this.mTextBounds;
        --mTextBounds4.top;
        final RectF rectF = new RectF();
        rectF.bottom = (float)this.getHeight();
        rectF.right = (float)this.getWidth();
        this.mNotBuilt = false;
    }
    
    public float getRound() {
        return this.mRound;
    }
    
    public float getRoundPercent() {
        return this.mRoundPercent;
    }
    
    public float getScaleFromTextSize() {
        return this.mBaseTextSize;
    }
    
    public float getTextBackgroundPanX() {
        return this.mBackgroundPanX;
    }
    
    public float getTextBackgroundPanY() {
        return this.mBackgroundPanY;
    }
    
    public float getTextBackgroundRotate() {
        return this.mRotate;
    }
    
    public float getTextBackgroundZoom() {
        return this.mZoom;
    }
    
    public int getTextOutlineColor() {
        return this.mTextOutlineColor;
    }
    
    public float getTextPanX() {
        return this.mTextPanX;
    }
    
    public float getTextPanY() {
        return this.mTextPanY;
    }
    
    public float getTextureHeight() {
        return this.mTextureHeight;
    }
    
    public float getTextureWidth() {
        return this.mTextureWidth;
    }
    
    public Typeface getTypeface() {
        return this.mPaint.getTypeface();
    }
    
    public void layout(float n, float n2, float n3, float n4) {
        final int n5 = (int)(n + 0.5f);
        this.mDeltaLeft = n - n5;
        final int n6 = (int)(n3 + 0.5f);
        final int n7 = n6 - n5;
        final int n8 = (int)(n4 + 0.5f);
        final int n9 = (int)(0.5f + n2);
        final int n10 = n8 - n9;
        final float n11 = n3 - n;
        this.mFloatWidth = n11;
        final float n12 = n4 - n2;
        this.mFloatHeight = n12;
        this.adjustTexture(n, n2, n3, n4);
        if (this.getMeasuredHeight() == n10 && this.getMeasuredWidth() == n7) {
            super.layout(n5, n9, n6, n8);
        }
        else {
            this.measure(View$MeasureSpec.makeMeasureSpec(n7, 1073741824), View$MeasureSpec.makeMeasureSpec(n10, 1073741824));
            super.layout(n5, n9, n6, n8);
        }
        if (this.mAutoSize) {
            if (this.mTempRect == null) {
                this.mTempPaint = new Paint();
                this.mTempRect = new Rect();
                this.mTempPaint.set((Paint)this.mPaint);
                this.paintTextSize = this.mTempPaint.getTextSize();
            }
            this.mFloatWidth = n11;
            this.mFloatHeight = n12;
            final Paint mTempPaint = this.mTempPaint;
            final String mText = this.mText;
            mTempPaint.getTextBounds(mText, 0, mText.length(), this.mTempRect);
            final int width = this.mTempRect.width();
            n = this.mTempRect.height() * 1.3f;
            n2 = n11 - this.mPaddingRight - this.mPaddingLeft;
            n4 = n12 - this.mPaddingBottom - this.mPaddingTop;
            n3 = (float)width;
            if (n3 * n4 > n * n2) {
                this.mPaint.setTextSize(this.paintTextSize * n2 / n3);
            }
            else {
                this.mPaint.setTextSize(this.paintTextSize * n4 / n);
            }
            if (this.mUseOutline || !Float.isNaN(this.mBaseTextSize)) {
                if (Float.isNaN(this.mBaseTextSize)) {
                    n = 1.0f;
                }
                else {
                    n = this.mTextSize / this.mBaseTextSize;
                }
                this.buildShape(n);
            }
        }
    }
    
    public void layout(final int n, final int n2, final int n3, final int n4) {
        super.layout(n, n2, n3, n4);
        final boolean naN = Float.isNaN(this.mBaseTextSize);
        float n5;
        if (naN) {
            n5 = 1.0f;
        }
        else {
            n5 = this.mTextSize / this.mBaseTextSize;
        }
        this.mFloatWidth = (float)(n3 - n);
        this.mFloatHeight = (float)(n4 - n2);
        float n6 = n5;
        if (this.mAutoSize) {
            if (this.mTempRect == null) {
                this.mTempPaint = new Paint();
                this.mTempRect = new Rect();
                this.mTempPaint.set((Paint)this.mPaint);
                this.paintTextSize = this.mTempPaint.getTextSize();
            }
            final Paint mTempPaint = this.mTempPaint;
            final String mText = this.mText;
            mTempPaint.getTextBounds(mText, 0, mText.length(), this.mTempRect);
            final int width = this.mTempRect.width();
            final int n7 = (int)(this.mTempRect.height() * 1.3f);
            final float n8 = this.mFloatWidth - this.mPaddingRight - this.mPaddingLeft;
            final float n9 = this.mFloatHeight - this.mPaddingBottom - this.mPaddingTop;
            if (naN) {
                final float n10 = (float)width;
                final float n11 = (float)n7;
                if (n10 * n9 > n11 * n8) {
                    this.mPaint.setTextSize(this.paintTextSize * n8 / n10);
                    n6 = n5;
                }
                else {
                    this.mPaint.setTextSize(this.paintTextSize * n9 / n11);
                    n6 = n5;
                }
            }
            else {
                final float n12 = (float)width;
                final float n13 = (float)n7;
                if (n12 * n9 > n13 * n8) {
                    n6 = n8 / n12;
                }
                else {
                    n6 = n9 / n13;
                }
            }
        }
        if (this.mUseOutline || !naN) {
            this.adjustTexture((float)n, (float)n2, (float)n3, (float)n4);
            this.buildShape(n6);
        }
    }
    
    protected void onDraw(final Canvas canvas) {
        float n;
        if (Float.isNaN(this.mBaseTextSize)) {
            n = 1.0f;
        }
        else {
            n = this.mTextSize / this.mBaseTextSize;
        }
        super.onDraw(canvas);
        if (!this.mUseOutline && n == 1.0f) {
            canvas.drawText(this.mText, this.mDeltaLeft + (this.mPaddingLeft + this.getHorizontalOffset()), this.mPaddingTop + this.getVerticalOffset(), (Paint)this.mPaint);
            return;
        }
        if (this.mNotBuilt) {
            this.buildShape(n);
        }
        if (this.mOutlinePositionMatrix == null) {
            this.mOutlinePositionMatrix = new Matrix();
        }
        if (this.mUseOutline) {
            this.paintCache.set((Paint)this.mPaint);
            this.mOutlinePositionMatrix.reset();
            final float n2 = this.mPaddingLeft + this.getHorizontalOffset();
            final float n3 = this.mPaddingTop + this.getVerticalOffset();
            this.mOutlinePositionMatrix.postTranslate(n2, n3);
            this.mOutlinePositionMatrix.preScale(n, n);
            this.mPath.transform(this.mOutlinePositionMatrix);
            if (this.mTextShader != null) {
                this.mPaint.setFilterBitmap(true);
                this.mPaint.setShader((Shader)this.mTextShader);
            }
            else {
                this.mPaint.setColor(this.mTextFillColor);
            }
            this.mPaint.setStyle(Paint$Style.FILL);
            this.mPaint.setStrokeWidth(this.mTextOutlineThickness);
            canvas.drawPath(this.mPath, (Paint)this.mPaint);
            if (this.mTextShader != null) {
                this.mPaint.setShader((Shader)null);
            }
            this.mPaint.setColor(this.mTextOutlineColor);
            this.mPaint.setStyle(Paint$Style.STROKE);
            this.mPaint.setStrokeWidth(this.mTextOutlineThickness);
            canvas.drawPath(this.mPath, (Paint)this.mPaint);
            this.mOutlinePositionMatrix.reset();
            this.mOutlinePositionMatrix.postTranslate(-n2, -n3);
            this.mPath.transform(this.mOutlinePositionMatrix);
            this.mPaint.set(this.paintCache);
        }
        else {
            final float n4 = this.mPaddingLeft + this.getHorizontalOffset();
            final float n5 = this.mPaddingTop + this.getVerticalOffset();
            this.mOutlinePositionMatrix.reset();
            this.mOutlinePositionMatrix.preTranslate(n4, n5);
            this.mPath.transform(this.mOutlinePositionMatrix);
            this.mPaint.setColor(this.mTextFillColor);
            this.mPaint.setStyle(Paint$Style.FILL_AND_STROKE);
            this.mPaint.setStrokeWidth(this.mTextOutlineThickness);
            canvas.drawPath(this.mPath, (Paint)this.mPaint);
            this.mOutlinePositionMatrix.reset();
            this.mOutlinePositionMatrix.preTranslate(-n4, -n5);
            this.mPath.transform(this.mOutlinePositionMatrix);
        }
    }
    
    protected void onMeasure(int n, int b) {
        final int mode = View$MeasureSpec.getMode(n);
        final int mode2 = View$MeasureSpec.getMode(b);
        n = View$MeasureSpec.getSize(n);
        final int size = View$MeasureSpec.getSize(b);
        this.mAutoSize = false;
        this.mPaddingLeft = this.getPaddingLeft();
        this.mPaddingRight = this.getPaddingRight();
        this.mPaddingTop = this.getPaddingTop();
        this.mPaddingBottom = this.getPaddingBottom();
        int n2;
        if (mode == 1073741824 && mode2 == 1073741824) {
            b = n;
            n2 = size;
            if (this.mAutoSizeTextType != 0) {
                this.mAutoSize = true;
                b = n;
                n2 = size;
            }
        }
        else {
            final TextPaint mPaint = this.mPaint;
            final String mText = this.mText;
            mPaint.getTextBounds(mText, 0, mText.length(), this.mTextBounds);
            if (mode != 1073741824) {
                n = (int)(this.mTextBounds.width() + 0.99999f);
            }
            final int n3 = b = n + (this.mPaddingLeft + this.mPaddingRight);
            n2 = size;
            if (mode2 != 1073741824) {
                b = (n = (int)(this.mPaint.getFontMetricsInt((Paint$FontMetricsInt)null) + 0.99999f));
                if (mode2 == Integer.MIN_VALUE) {
                    n = Math.min(size, b);
                }
                n2 = this.mPaddingTop + this.mPaddingBottom + n;
                b = n3;
            }
        }
        this.setMeasuredDimension(b, n2);
    }
    
    public void setGravity(int mGravity) {
        int n = mGravity;
        if ((mGravity & 0x800007) == 0x0) {
            n = (mGravity | 0x800003);
        }
        mGravity = n;
        if ((n & 0x70) == 0x0) {
            mGravity = (n | 0x30);
        }
        if (mGravity != this.mGravity) {
            this.invalidate();
        }
        this.mGravity = mGravity;
        final int n2 = mGravity & 0x70;
        if (n2 != 48) {
            if (n2 != 80) {
                this.mTextPanY = 0.0f;
            }
            else {
                this.mTextPanY = 1.0f;
            }
        }
        else {
            this.mTextPanY = -1.0f;
        }
        mGravity &= 0x800007;
        Label_0133: {
            if (mGravity != 3) {
                if (mGravity != 5) {
                    if (mGravity == 8388611) {
                        break Label_0133;
                    }
                    if (mGravity != 8388613) {
                        this.mTextPanX = 0.0f;
                        return;
                    }
                }
                this.mTextPanX = 1.0f;
                return;
            }
        }
        this.mTextPanX = -1.0f;
    }
    
    public void setRound(float n) {
        if (Float.isNaN(n)) {
            this.mRound = n;
            n = this.mRoundPercent;
            this.mRoundPercent = -1.0f;
            this.setRoundPercent(n);
            return;
        }
        final boolean b = this.mRound != n;
        this.mRound = n;
        if (n != 0.0f) {
            if (this.mPath == null) {
                this.mPath = new Path();
            }
            if (this.mRect == null) {
                this.mRect = new RectF();
            }
            if (Build$VERSION.SDK_INT >= 21) {
                if (this.mViewOutlineProvider == null) {
                    this.setOutlineProvider(this.mViewOutlineProvider = new ViewOutlineProvider(this) {
                        final MotionLabel this$0;
                        
                        public void getOutline(final View view, final Outline outline) {
                            outline.setRoundRect(0, 0, this.this$0.getWidth(), this.this$0.getHeight(), this.this$0.mRound);
                        }
                    });
                }
                this.setClipToOutline(true);
            }
            this.mRect.set(0.0f, 0.0f, (float)this.getWidth(), (float)this.getHeight());
            this.mPath.reset();
            final Path mPath = this.mPath;
            final RectF mRect = this.mRect;
            n = this.mRound;
            mPath.addRoundRect(mRect, n, n, Path$Direction.CW);
        }
        else if (Build$VERSION.SDK_INT >= 21) {
            this.setClipToOutline(false);
        }
        if (b && Build$VERSION.SDK_INT >= 21) {
            this.invalidateOutline();
        }
    }
    
    public void setRoundPercent(float mRoundPercent) {
        final boolean b = this.mRoundPercent != mRoundPercent;
        this.mRoundPercent = mRoundPercent;
        if (mRoundPercent != 0.0f) {
            if (this.mPath == null) {
                this.mPath = new Path();
            }
            if (this.mRect == null) {
                this.mRect = new RectF();
            }
            if (Build$VERSION.SDK_INT >= 21) {
                if (this.mViewOutlineProvider == null) {
                    this.setOutlineProvider(this.mViewOutlineProvider = new ViewOutlineProvider(this) {
                        final MotionLabel this$0;
                        
                        public void getOutline(final View view, final Outline outline) {
                            final int width = this.this$0.getWidth();
                            final int height = this.this$0.getHeight();
                            outline.setRoundRect(0, 0, width, height, Math.min(width, height) * this.this$0.mRoundPercent / 2.0f);
                        }
                    });
                }
                this.setClipToOutline(true);
            }
            final int width = this.getWidth();
            final int height = this.getHeight();
            mRoundPercent = Math.min(width, height) * this.mRoundPercent / 2.0f;
            this.mRect.set(0.0f, 0.0f, (float)width, (float)height);
            this.mPath.reset();
            this.mPath.addRoundRect(this.mRect, mRoundPercent, mRoundPercent, Path$Direction.CW);
        }
        else if (Build$VERSION.SDK_INT >= 21) {
            this.setClipToOutline(false);
        }
        if (b && Build$VERSION.SDK_INT >= 21) {
            this.invalidateOutline();
        }
    }
    
    public void setScaleFromTextSize(final float mBaseTextSize) {
        this.mBaseTextSize = mBaseTextSize;
    }
    
    public void setText(final CharSequence charSequence) {
        this.mText = charSequence.toString();
        this.invalidate();
    }
    
    public void setTextBackgroundPanX(final float mBackgroundPanX) {
        this.mBackgroundPanX = mBackgroundPanX;
        this.updateShaderMatrix();
        this.invalidate();
    }
    
    public void setTextBackgroundPanY(final float mBackgroundPanY) {
        this.mBackgroundPanY = mBackgroundPanY;
        this.updateShaderMatrix();
        this.invalidate();
    }
    
    public void setTextBackgroundRotate(final float mRotate) {
        this.mRotate = mRotate;
        this.updateShaderMatrix();
        this.invalidate();
    }
    
    public void setTextBackgroundZoom(final float mZoom) {
        this.mZoom = mZoom;
        this.updateShaderMatrix();
        this.invalidate();
    }
    
    public void setTextFillColor(final int mTextFillColor) {
        this.mTextFillColor = mTextFillColor;
        this.invalidate();
    }
    
    public void setTextOutlineColor(final int mTextOutlineColor) {
        this.mTextOutlineColor = mTextOutlineColor;
        this.mUseOutline = true;
        this.invalidate();
    }
    
    public void setTextOutlineThickness(final float n) {
        this.mTextOutlineThickness = n;
        this.mUseOutline = true;
        if (Float.isNaN(n)) {
            this.mTextOutlineThickness = 1.0f;
            this.mUseOutline = false;
        }
        this.invalidate();
    }
    
    public void setTextPanX(final float mTextPanX) {
        this.mTextPanX = mTextPanX;
        this.invalidate();
    }
    
    public void setTextPanY(final float mTextPanY) {
        this.mTextPanY = mTextPanY;
        this.invalidate();
    }
    
    public void setTextSize(float mBaseTextSize) {
        this.mTextSize = mBaseTextSize;
        final String tag = MotionLabel.TAG;
        final StringBuilder sb = new StringBuilder();
        sb.append(Debug.getLoc());
        sb.append("  ");
        sb.append(mBaseTextSize);
        sb.append(" / ");
        sb.append(this.mBaseTextSize);
        Log.v(tag, sb.toString());
        final TextPaint mPaint = this.mPaint;
        if (!Float.isNaN(this.mBaseTextSize)) {
            mBaseTextSize = this.mBaseTextSize;
        }
        mPaint.setTextSize(mBaseTextSize);
        if (Float.isNaN(this.mBaseTextSize)) {
            mBaseTextSize = 1.0f;
        }
        else {
            mBaseTextSize = this.mTextSize / this.mBaseTextSize;
        }
        this.buildShape(mBaseTextSize);
        this.requestLayout();
        this.invalidate();
    }
    
    public void setTextureHeight(final float mTextureHeight) {
        this.mTextureHeight = mTextureHeight;
        this.updateShaderMatrix();
        this.invalidate();
    }
    
    public void setTextureWidth(final float mTextureWidth) {
        this.mTextureWidth = mTextureWidth;
        this.updateShaderMatrix();
        this.invalidate();
    }
    
    public void setTypeface(final Typeface typeface) {
        if (this.mPaint.getTypeface() != typeface) {
            this.mPaint.setTypeface(typeface);
            if (this.mLayout != null) {
                this.mLayout = null;
                this.requestLayout();
                this.invalidate();
            }
        }
    }
    
    void setupPath() {
        this.mPaddingLeft = this.getPaddingLeft();
        this.mPaddingRight = this.getPaddingRight();
        this.mPaddingTop = this.getPaddingTop();
        this.mPaddingBottom = this.getPaddingBottom();
        this.setTypefaceFromAttrs(this.mFontFamily, this.mTypefaceIndex, this.mStyleIndex);
        this.mPaint.setColor(this.mTextFillColor);
        this.mPaint.setStrokeWidth(this.mTextOutlineThickness);
        this.mPaint.setStyle(Paint$Style.FILL_AND_STROKE);
        this.mPaint.setFlags(128);
        this.setTextSize(this.mTextSize);
        this.mPaint.setAntiAlias(true);
    }
}

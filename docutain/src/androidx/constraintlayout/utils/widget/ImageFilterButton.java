// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.utils.widget;

import android.graphics.Path$Direction;
import android.graphics.Outline;
import android.view.View;
import android.widget.ImageView;
import androidx.appcompat.content.res.AppCompatResources;
import android.graphics.Canvas;
import android.widget.ImageView$ScaleType;
import android.graphics.Matrix;
import android.content.res.TypedArray;
import android.os.Build$VERSION;
import androidx.constraintlayout.widget.R;
import android.util.AttributeSet;
import android.content.Context;
import android.view.ViewOutlineProvider;
import android.graphics.RectF;
import android.graphics.Path;
import android.graphics.drawable.LayerDrawable;
import android.graphics.drawable.Drawable;
import androidx.appcompat.widget.AppCompatImageButton;

public class ImageFilterButton extends AppCompatImageButton
{
    private Drawable mAltDrawable;
    private float mCrossfade;
    private Drawable mDrawable;
    private ImageFilterView.ImageMatrix mImageMatrix;
    LayerDrawable mLayer;
    Drawable[] mLayers;
    private boolean mOverlay;
    private float mPanX;
    private float mPanY;
    private Path mPath;
    RectF mRect;
    private float mRotate;
    private float mRound;
    private float mRoundPercent;
    ViewOutlineProvider mViewOutlineProvider;
    private float mZoom;
    
    public ImageFilterButton(final Context context) {
        super(context);
        this.mImageMatrix = new ImageFilterView.ImageMatrix();
        this.mCrossfade = 0.0f;
        this.mRoundPercent = 0.0f;
        this.mRound = Float.NaN;
        this.mLayers = new Drawable[2];
        this.mOverlay = true;
        this.mAltDrawable = null;
        this.mDrawable = null;
        this.mPanX = Float.NaN;
        this.mPanY = Float.NaN;
        this.mZoom = Float.NaN;
        this.mRotate = Float.NaN;
        this.init(context, null);
    }
    
    public ImageFilterButton(final Context context, final AttributeSet set) {
        super(context, set);
        this.mImageMatrix = new ImageFilterView.ImageMatrix();
        this.mCrossfade = 0.0f;
        this.mRoundPercent = 0.0f;
        this.mRound = Float.NaN;
        this.mLayers = new Drawable[2];
        this.mOverlay = true;
        this.mAltDrawable = null;
        this.mDrawable = null;
        this.mPanX = Float.NaN;
        this.mPanY = Float.NaN;
        this.mZoom = Float.NaN;
        this.mRotate = Float.NaN;
        this.init(context, set);
    }
    
    public ImageFilterButton(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
        this.mImageMatrix = new ImageFilterView.ImageMatrix();
        this.mCrossfade = 0.0f;
        this.mRoundPercent = 0.0f;
        this.mRound = Float.NaN;
        this.mLayers = new Drawable[2];
        this.mOverlay = true;
        this.mAltDrawable = null;
        this.mDrawable = null;
        this.mPanX = Float.NaN;
        this.mPanY = Float.NaN;
        this.mZoom = Float.NaN;
        this.mRotate = Float.NaN;
        this.init(context, set);
    }
    
    private void init(final Context context, final AttributeSet set) {
        this.setPadding(0, 0, 0, 0);
        if (set != null) {
            final TypedArray obtainStyledAttributes = this.getContext().obtainStyledAttributes(set, R.styleable.ImageFilterView);
            final int indexCount = obtainStyledAttributes.getIndexCount();
            this.mAltDrawable = obtainStyledAttributes.getDrawable(R.styleable.ImageFilterView_altSrc);
            for (int i = 0; i < indexCount; ++i) {
                final int index = obtainStyledAttributes.getIndex(i);
                if (index == R.styleable.ImageFilterView_crossfade) {
                    this.mCrossfade = obtainStyledAttributes.getFloat(index, 0.0f);
                }
                else if (index == R.styleable.ImageFilterView_warmth) {
                    this.setWarmth(obtainStyledAttributes.getFloat(index, 0.0f));
                }
                else if (index == R.styleable.ImageFilterView_saturation) {
                    this.setSaturation(obtainStyledAttributes.getFloat(index, 0.0f));
                }
                else if (index == R.styleable.ImageFilterView_contrast) {
                    this.setContrast(obtainStyledAttributes.getFloat(index, 0.0f));
                }
                else if (index == R.styleable.ImageFilterView_round) {
                    if (Build$VERSION.SDK_INT >= 21) {
                        this.setRound(obtainStyledAttributes.getDimension(index, 0.0f));
                    }
                }
                else if (index == R.styleable.ImageFilterView_roundPercent) {
                    if (Build$VERSION.SDK_INT >= 21) {
                        this.setRoundPercent(obtainStyledAttributes.getFloat(index, 0.0f));
                    }
                }
                else if (index == R.styleable.ImageFilterView_overlay) {
                    this.setOverlay(obtainStyledAttributes.getBoolean(index, this.mOverlay));
                }
                else if (index == R.styleable.ImageFilterView_imagePanX) {
                    this.setImagePanX(obtainStyledAttributes.getFloat(index, this.mPanX));
                }
                else if (index == R.styleable.ImageFilterView_imagePanY) {
                    this.setImagePanY(obtainStyledAttributes.getFloat(index, this.mPanY));
                }
                else if (index == R.styleable.ImageFilterView_imageRotate) {
                    this.setImageRotate(obtainStyledAttributes.getFloat(index, this.mRotate));
                }
                else if (index == R.styleable.ImageFilterView_imageZoom) {
                    this.setImageZoom(obtainStyledAttributes.getFloat(index, this.mZoom));
                }
            }
            obtainStyledAttributes.recycle();
            final Drawable drawable = this.getDrawable();
            this.mDrawable = drawable;
            if (this.mAltDrawable != null && drawable != null) {
                this.mLayers[0] = (this.mDrawable = this.getDrawable().mutate());
                this.mLayers[1] = this.mAltDrawable.mutate();
                final LayerDrawable mLayer = new LayerDrawable(this.mLayers);
                this.mLayer = mLayer;
                mLayer.getDrawable(1).setAlpha((int)(this.mCrossfade * 255.0f));
                if (!this.mOverlay) {
                    this.mLayer.getDrawable(0).setAlpha((int)((1.0f - this.mCrossfade) * 255.0f));
                }
                super.setImageDrawable((Drawable)this.mLayer);
            }
            else {
                final Drawable drawable2 = this.getDrawable();
                if ((this.mDrawable = drawable2) != null) {
                    this.mLayers[0] = (this.mDrawable = drawable2.mutate());
                }
            }
        }
    }
    
    private void setMatrix() {
        if (Float.isNaN(this.mPanX) && Float.isNaN(this.mPanY) && Float.isNaN(this.mZoom) && Float.isNaN(this.mRotate)) {
            return;
        }
        final boolean naN = Float.isNaN(this.mPanX);
        float mRotate = 0.0f;
        float mPanX;
        if (naN) {
            mPanX = 0.0f;
        }
        else {
            mPanX = this.mPanX;
        }
        float mPanY;
        if (Float.isNaN(this.mPanY)) {
            mPanY = 0.0f;
        }
        else {
            mPanY = this.mPanY;
        }
        float mZoom;
        if (Float.isNaN(this.mZoom)) {
            mZoom = 1.0f;
        }
        else {
            mZoom = this.mZoom;
        }
        if (!Float.isNaN(this.mRotate)) {
            mRotate = this.mRotate;
        }
        final Matrix imageMatrix = new Matrix();
        imageMatrix.reset();
        final float n = (float)this.getDrawable().getIntrinsicWidth();
        final float n2 = (float)this.getDrawable().getIntrinsicHeight();
        final float n3 = (float)this.getWidth();
        final float n4 = (float)this.getHeight();
        float n5;
        if (n * n4 < n2 * n3) {
            n5 = n3 / n;
        }
        else {
            n5 = n4 / n2;
        }
        final float n6 = mZoom * n5;
        imageMatrix.postScale(n6, n6);
        final float n7 = n * n6;
        final float n8 = n6 * n2;
        imageMatrix.postTranslate((mPanX * (n3 - n7) + n3 - n7) * 0.5f, (mPanY * (n4 - n8) + n4 - n8) * 0.5f);
        imageMatrix.postRotate(mRotate, n3 / 2.0f, n4 / 2.0f);
        this.setImageMatrix(imageMatrix);
        this.setScaleType(ImageView$ScaleType.MATRIX);
    }
    
    private void setOverlay(final boolean mOverlay) {
        this.mOverlay = mOverlay;
    }
    
    private void updateViewMatrix() {
        if (Float.isNaN(this.mPanX) && Float.isNaN(this.mPanY) && Float.isNaN(this.mZoom) && Float.isNaN(this.mRotate)) {
            this.setScaleType(ImageView$ScaleType.FIT_CENTER);
            return;
        }
        this.setMatrix();
    }
    
    public void draw(final Canvas canvas) {
        boolean b;
        if (Build$VERSION.SDK_INT < 21 && this.mRound != 0.0f && this.mPath != null) {
            b = true;
            canvas.save();
            canvas.clipPath(this.mPath);
        }
        else {
            b = false;
        }
        super.draw(canvas);
        if (b) {
            canvas.restore();
        }
    }
    
    public float getContrast() {
        return this.mImageMatrix.mContrast;
    }
    
    public float getCrossfade() {
        return this.mCrossfade;
    }
    
    public float getImagePanX() {
        return this.mPanX;
    }
    
    public float getImagePanY() {
        return this.mPanY;
    }
    
    public float getImageRotate() {
        return this.mRotate;
    }
    
    public float getImageZoom() {
        return this.mZoom;
    }
    
    public float getRound() {
        return this.mRound;
    }
    
    public float getRoundPercent() {
        return this.mRoundPercent;
    }
    
    public float getSaturation() {
        return this.mImageMatrix.mSaturation;
    }
    
    public float getWarmth() {
        return this.mImageMatrix.mWarmth;
    }
    
    public void layout(final int n, final int n2, final int n3, final int n4) {
        super.layout(n, n2, n3, n4);
        this.setMatrix();
    }
    
    public void setAltImageResource(final int n) {
        final Drawable mutate = AppCompatResources.getDrawable(this.getContext(), n).mutate();
        this.mAltDrawable = mutate;
        final Drawable[] mLayers = this.mLayers;
        mLayers[0] = this.mDrawable;
        mLayers[1] = mutate;
        super.setImageDrawable((Drawable)(this.mLayer = new LayerDrawable(this.mLayers)));
        this.setCrossfade(this.mCrossfade);
    }
    
    public void setBrightness(final float mBrightness) {
        this.mImageMatrix.mBrightness = mBrightness;
        this.mImageMatrix.updateMatrix((ImageView)this);
    }
    
    public void setContrast(final float mContrast) {
        this.mImageMatrix.mContrast = mContrast;
        this.mImageMatrix.updateMatrix((ImageView)this);
    }
    
    public void setCrossfade(final float mCrossfade) {
        this.mCrossfade = mCrossfade;
        if (this.mLayers != null) {
            if (!this.mOverlay) {
                this.mLayer.getDrawable(0).setAlpha((int)((1.0f - this.mCrossfade) * 255.0f));
            }
            this.mLayer.getDrawable(1).setAlpha((int)(this.mCrossfade * 255.0f));
            super.setImageDrawable((Drawable)this.mLayer);
        }
    }
    
    @Override
    public void setImageDrawable(Drawable mutate) {
        if (this.mAltDrawable != null && mutate != null) {
            mutate = mutate.mutate();
            this.mDrawable = mutate;
            final Drawable[] mLayers = this.mLayers;
            mLayers[0] = mutate;
            mLayers[1] = this.mAltDrawable;
            super.setImageDrawable((Drawable)(this.mLayer = new LayerDrawable(this.mLayers)));
            this.setCrossfade(this.mCrossfade);
        }
        else {
            super.setImageDrawable(mutate);
        }
    }
    
    public void setImagePanX(final float mPanX) {
        this.mPanX = mPanX;
        this.updateViewMatrix();
    }
    
    public void setImagePanY(final float mPanY) {
        this.mPanY = mPanY;
        this.updateViewMatrix();
    }
    
    @Override
    public void setImageResource(final int imageResource) {
        if (this.mAltDrawable != null) {
            final Drawable mutate = AppCompatResources.getDrawable(this.getContext(), imageResource).mutate();
            this.mDrawable = mutate;
            final Drawable[] mLayers = this.mLayers;
            mLayers[0] = mutate;
            mLayers[1] = this.mAltDrawable;
            super.setImageDrawable((Drawable)(this.mLayer = new LayerDrawable(this.mLayers)));
            this.setCrossfade(this.mCrossfade);
        }
        else {
            super.setImageResource(imageResource);
        }
    }
    
    public void setImageRotate(final float mRotate) {
        this.mRotate = mRotate;
        this.updateViewMatrix();
    }
    
    public void setImageZoom(final float mZoom) {
        this.mZoom = mZoom;
        this.updateViewMatrix();
    }
    
    public void setRound(float n) {
        if (Float.isNaN(n)) {
            this.mRound = n;
            n = this.mRoundPercent;
            this.mRoundPercent = -1.0f;
            this.setRoundPercent(n);
            return;
        }
        final boolean b = this.mRound != n;
        this.mRound = n;
        if (n != 0.0f) {
            if (this.mPath == null) {
                this.mPath = new Path();
            }
            if (this.mRect == null) {
                this.mRect = new RectF();
            }
            if (Build$VERSION.SDK_INT >= 21) {
                if (this.mViewOutlineProvider == null) {
                    this.setOutlineProvider(this.mViewOutlineProvider = new ViewOutlineProvider(this) {
                        final ImageFilterButton this$0;
                        
                        public void getOutline(final View view, final Outline outline) {
                            outline.setRoundRect(0, 0, this.this$0.getWidth(), this.this$0.getHeight(), this.this$0.mRound);
                        }
                    });
                }
                this.setClipToOutline(true);
            }
            this.mRect.set(0.0f, 0.0f, (float)this.getWidth(), (float)this.getHeight());
            this.mPath.reset();
            final Path mPath = this.mPath;
            final RectF mRect = this.mRect;
            n = this.mRound;
            mPath.addRoundRect(mRect, n, n, Path$Direction.CW);
        }
        else if (Build$VERSION.SDK_INT >= 21) {
            this.setClipToOutline(false);
        }
        if (b && Build$VERSION.SDK_INT >= 21) {
            this.invalidateOutline();
        }
    }
    
    public void setRoundPercent(float mRoundPercent) {
        final boolean b = this.mRoundPercent != mRoundPercent;
        this.mRoundPercent = mRoundPercent;
        if (mRoundPercent != 0.0f) {
            if (this.mPath == null) {
                this.mPath = new Path();
            }
            if (this.mRect == null) {
                this.mRect = new RectF();
            }
            if (Build$VERSION.SDK_INT >= 21) {
                if (this.mViewOutlineProvider == null) {
                    this.setOutlineProvider(this.mViewOutlineProvider = new ViewOutlineProvider(this) {
                        final ImageFilterButton this$0;
                        
                        public void getOutline(final View view, final Outline outline) {
                            final int width = this.this$0.getWidth();
                            final int height = this.this$0.getHeight();
                            outline.setRoundRect(0, 0, width, height, Math.min(width, height) * this.this$0.mRoundPercent / 2.0f);
                        }
                    });
                }
                this.setClipToOutline(true);
            }
            final int width = this.getWidth();
            final int height = this.getHeight();
            mRoundPercent = Math.min(width, height) * this.mRoundPercent / 2.0f;
            this.mRect.set(0.0f, 0.0f, (float)width, (float)height);
            this.mPath.reset();
            this.mPath.addRoundRect(this.mRect, mRoundPercent, mRoundPercent, Path$Direction.CW);
        }
        else if (Build$VERSION.SDK_INT >= 21) {
            this.setClipToOutline(false);
        }
        if (b && Build$VERSION.SDK_INT >= 21) {
            this.invalidateOutline();
        }
    }
    
    public void setSaturation(final float mSaturation) {
        this.mImageMatrix.mSaturation = mSaturation;
        this.mImageMatrix.updateMatrix((ImageView)this);
    }
    
    public void setWarmth(final float mWarmth) {
        this.mImageMatrix.mWarmth = mWarmth;
        this.mImageMatrix.updateMatrix((ImageView)this);
    }
}

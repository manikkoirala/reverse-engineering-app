// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.utils.widget;

import android.view.ViewParent;
import android.view.View;
import android.graphics.Canvas;
import android.content.res.TypedArray;
import androidx.constraintlayout.widget.R;
import android.util.AttributeSet;
import android.content.Context;
import android.graphics.Paint;
import androidx.constraintlayout.motion.widget.MotionLayout;
import android.graphics.Matrix;

public class MotionTelltales extends MockView
{
    private static final String TAG = "MotionTelltales";
    Matrix mInvertMatrix;
    MotionLayout mMotionLayout;
    private Paint mPaintTelltales;
    int mTailColor;
    float mTailScale;
    int mVelocityMode;
    float[] velocity;
    
    public MotionTelltales(final Context context) {
        super(context);
        this.mPaintTelltales = new Paint();
        this.velocity = new float[2];
        this.mInvertMatrix = new Matrix();
        this.mVelocityMode = 0;
        this.mTailColor = -65281;
        this.mTailScale = 0.25f;
        this.init(context, null);
    }
    
    public MotionTelltales(final Context context, final AttributeSet set) {
        super(context, set);
        this.mPaintTelltales = new Paint();
        this.velocity = new float[2];
        this.mInvertMatrix = new Matrix();
        this.mVelocityMode = 0;
        this.mTailColor = -65281;
        this.mTailScale = 0.25f;
        this.init(context, set);
    }
    
    public MotionTelltales(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
        this.mPaintTelltales = new Paint();
        this.velocity = new float[2];
        this.mInvertMatrix = new Matrix();
        this.mVelocityMode = 0;
        this.mTailColor = -65281;
        this.mTailScale = 0.25f;
        this.init(context, set);
    }
    
    private void init(final Context context, final AttributeSet set) {
        if (set != null) {
            final TypedArray obtainStyledAttributes = context.obtainStyledAttributes(set, R.styleable.MotionTelltales);
            for (int indexCount = obtainStyledAttributes.getIndexCount(), i = 0; i < indexCount; ++i) {
                final int index = obtainStyledAttributes.getIndex(i);
                if (index == R.styleable.MotionTelltales_telltales_tailColor) {
                    this.mTailColor = obtainStyledAttributes.getColor(index, this.mTailColor);
                }
                else if (index == R.styleable.MotionTelltales_telltales_velocityMode) {
                    this.mVelocityMode = obtainStyledAttributes.getInt(index, this.mVelocityMode);
                }
                else if (index == R.styleable.MotionTelltales_telltales_tailScale) {
                    this.mTailScale = obtainStyledAttributes.getFloat(index, this.mTailScale);
                }
            }
            obtainStyledAttributes.recycle();
        }
        this.mPaintTelltales.setColor(this.mTailColor);
        this.mPaintTelltales.setStrokeWidth(5.0f);
    }
    
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
    }
    
    @Override
    public void onDraw(final Canvas canvas) {
        super.onDraw(canvas);
        this.getMatrix().invert(this.mInvertMatrix);
        if (this.mMotionLayout == null) {
            final ViewParent parent = this.getParent();
            if (parent instanceof MotionLayout) {
                this.mMotionLayout = (MotionLayout)parent;
            }
            return;
        }
        final int width = this.getWidth();
        final int height = this.getHeight();
        final float[] array2;
        final float[] array = array2 = new float[5];
        array2[0] = 0.1f;
        array2[1] = 0.25f;
        array2[2] = 0.5f;
        array2[3] = 0.75f;
        array2[4] = 0.9f;
        for (int i = 0; i < 5; ++i) {
            final float n = array[i];
            for (int j = 0; j < 5; ++j) {
                final float n2 = array[j];
                this.mMotionLayout.getViewVelocity(this, n2, n, this.velocity, this.mVelocityMode);
                this.mInvertMatrix.mapVectors(this.velocity);
                final float n3 = width * n2;
                final float n4 = height * n;
                final float[] velocity = this.velocity;
                final float n5 = velocity[0];
                final float mTailScale = this.mTailScale;
                final float n6 = velocity[1];
                this.mInvertMatrix.mapVectors(velocity);
                canvas.drawLine(n3, n4, n3 - n5 * mTailScale, n4 - n6 * mTailScale, this.mPaintTelltales);
            }
        }
    }
    
    protected void onLayout(final boolean b, final int n, final int n2, final int n3, final int n4) {
        super.onLayout(b, n, n2, n3, n4);
        this.postInvalidate();
    }
    
    public void setText(final CharSequence charSequence) {
        this.mText = charSequence.toString();
        this.requestLayout();
    }
}

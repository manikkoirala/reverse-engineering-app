// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.utils.widget;

import android.graphics.Canvas;
import android.content.res.TypedArray;
import androidx.constraintlayout.widget.R;
import android.util.AttributeSet;
import android.graphics.Color;
import android.content.Context;
import android.graphics.Rect;
import android.graphics.Paint;
import android.view.View;

public class MockView extends View
{
    private int mDiagonalsColor;
    private boolean mDrawDiagonals;
    private boolean mDrawLabel;
    private int mMargin;
    private Paint mPaintDiagonals;
    private Paint mPaintText;
    private Paint mPaintTextBackground;
    protected String mText;
    private int mTextBackgroundColor;
    private Rect mTextBounds;
    private int mTextColor;
    
    public MockView(final Context context) {
        super(context);
        this.mPaintDiagonals = new Paint();
        this.mPaintText = new Paint();
        this.mPaintTextBackground = new Paint();
        this.mDrawDiagonals = true;
        this.mDrawLabel = true;
        this.mText = null;
        this.mTextBounds = new Rect();
        this.mDiagonalsColor = Color.argb(255, 0, 0, 0);
        this.mTextColor = Color.argb(255, 200, 200, 200);
        this.mTextBackgroundColor = Color.argb(255, 50, 50, 50);
        this.mMargin = 4;
        this.init(context, null);
    }
    
    public MockView(final Context context, final AttributeSet set) {
        super(context, set);
        this.mPaintDiagonals = new Paint();
        this.mPaintText = new Paint();
        this.mPaintTextBackground = new Paint();
        this.mDrawDiagonals = true;
        this.mDrawLabel = true;
        this.mText = null;
        this.mTextBounds = new Rect();
        this.mDiagonalsColor = Color.argb(255, 0, 0, 0);
        this.mTextColor = Color.argb(255, 200, 200, 200);
        this.mTextBackgroundColor = Color.argb(255, 50, 50, 50);
        this.mMargin = 4;
        this.init(context, set);
    }
    
    public MockView(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
        this.mPaintDiagonals = new Paint();
        this.mPaintText = new Paint();
        this.mPaintTextBackground = new Paint();
        this.mDrawDiagonals = true;
        this.mDrawLabel = true;
        this.mText = null;
        this.mTextBounds = new Rect();
        this.mDiagonalsColor = Color.argb(255, 0, 0, 0);
        this.mTextColor = Color.argb(255, 200, 200, 200);
        this.mTextBackgroundColor = Color.argb(255, 50, 50, 50);
        this.mMargin = 4;
        this.init(context, set);
    }
    
    private void init(final Context context, final AttributeSet set) {
        if (set != null) {
            final TypedArray obtainStyledAttributes = context.obtainStyledAttributes(set, R.styleable.MockView);
            for (int indexCount = obtainStyledAttributes.getIndexCount(), i = 0; i < indexCount; ++i) {
                final int index = obtainStyledAttributes.getIndex(i);
                if (index == R.styleable.MockView_mock_label) {
                    this.mText = obtainStyledAttributes.getString(index);
                }
                else if (index == R.styleable.MockView_mock_showDiagonals) {
                    this.mDrawDiagonals = obtainStyledAttributes.getBoolean(index, this.mDrawDiagonals);
                }
                else if (index == R.styleable.MockView_mock_diagonalsColor) {
                    this.mDiagonalsColor = obtainStyledAttributes.getColor(index, this.mDiagonalsColor);
                }
                else if (index == R.styleable.MockView_mock_labelBackgroundColor) {
                    this.mTextBackgroundColor = obtainStyledAttributes.getColor(index, this.mTextBackgroundColor);
                }
                else if (index == R.styleable.MockView_mock_labelColor) {
                    this.mTextColor = obtainStyledAttributes.getColor(index, this.mTextColor);
                }
                else if (index == R.styleable.MockView_mock_showLabel) {
                    this.mDrawLabel = obtainStyledAttributes.getBoolean(index, this.mDrawLabel);
                }
            }
            obtainStyledAttributes.recycle();
        }
        while (true) {
            if (this.mText != null) {
                break Label_0209;
            }
            try {
                this.mText = context.getResources().getResourceEntryName(this.getId());
                this.mPaintDiagonals.setColor(this.mDiagonalsColor);
                this.mPaintDiagonals.setAntiAlias(true);
                this.mPaintText.setColor(this.mTextColor);
                this.mPaintText.setAntiAlias(true);
                this.mPaintTextBackground.setColor(this.mTextBackgroundColor);
                this.mMargin = Math.round(this.mMargin * (this.getResources().getDisplayMetrics().xdpi / 160.0f));
            }
            catch (final Exception ex) {
                continue;
            }
            break;
        }
    }
    
    public void onDraw(final Canvas canvas) {
        super.onDraw(canvas);
        final int width = this.getWidth();
        final int height = this.getHeight();
        int n = width;
        int n2 = height;
        if (this.mDrawDiagonals) {
            n = width - 1;
            n2 = height - 1;
            final float n3 = (float)n;
            final float n4 = (float)n2;
            canvas.drawLine(0.0f, 0.0f, n3, n4, this.mPaintDiagonals);
            canvas.drawLine(0.0f, n4, n3, 0.0f, this.mPaintDiagonals);
            canvas.drawLine(0.0f, 0.0f, n3, 0.0f, this.mPaintDiagonals);
            canvas.drawLine(n3, 0.0f, n3, n4, this.mPaintDiagonals);
            canvas.drawLine(n3, n4, 0.0f, n4, this.mPaintDiagonals);
            canvas.drawLine(0.0f, n4, 0.0f, 0.0f, this.mPaintDiagonals);
        }
        final String mText = this.mText;
        if (mText != null && this.mDrawLabel) {
            this.mPaintText.getTextBounds(mText, 0, mText.length(), this.mTextBounds);
            final float n5 = (n - this.mTextBounds.width()) / 2.0f;
            final float n6 = (n2 - this.mTextBounds.height()) / 2.0f + this.mTextBounds.height();
            this.mTextBounds.offset((int)n5, (int)n6);
            final Rect mTextBounds = this.mTextBounds;
            mTextBounds.set(mTextBounds.left - this.mMargin, this.mTextBounds.top - this.mMargin, this.mTextBounds.right + this.mMargin, this.mTextBounds.bottom + this.mMargin);
            canvas.drawRect(this.mTextBounds, this.mPaintTextBackground);
            canvas.drawText(this.mText, n5, n6, this.mPaintText);
        }
    }
}

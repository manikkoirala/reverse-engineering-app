// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.utils.widget;

import android.graphics.Path$Direction;
import android.graphics.Outline;
import android.view.View;
import android.graphics.Canvas;
import android.content.res.TypedArray;
import android.os.Build$VERSION;
import androidx.constraintlayout.widget.R;
import android.util.AttributeSet;
import android.content.Context;
import android.view.ViewOutlineProvider;
import android.graphics.RectF;
import android.graphics.Path;
import androidx.appcompat.widget.AppCompatButton;

public class MotionButton extends AppCompatButton
{
    private Path mPath;
    RectF mRect;
    private float mRound;
    private float mRoundPercent;
    ViewOutlineProvider mViewOutlineProvider;
    
    public MotionButton(final Context context) {
        super(context);
        this.mRoundPercent = 0.0f;
        this.mRound = Float.NaN;
        this.init(context, null);
    }
    
    public MotionButton(final Context context, final AttributeSet set) {
        super(context, set);
        this.mRoundPercent = 0.0f;
        this.mRound = Float.NaN;
        this.init(context, set);
    }
    
    public MotionButton(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
        this.mRoundPercent = 0.0f;
        this.mRound = Float.NaN;
        this.init(context, set);
    }
    
    private void init(final Context context, final AttributeSet set) {
        int i = 0;
        this.setPadding(0, 0, 0, 0);
        if (set != null) {
            TypedArray obtainStyledAttributes;
            for (obtainStyledAttributes = this.getContext().obtainStyledAttributes(set, R.styleable.ImageFilterView); i < obtainStyledAttributes.getIndexCount(); ++i) {
                final int index = obtainStyledAttributes.getIndex(i);
                if (index == R.styleable.ImageFilterView_round) {
                    if (Build$VERSION.SDK_INT >= 21) {
                        this.setRound(obtainStyledAttributes.getDimension(index, 0.0f));
                    }
                }
                else if (index == R.styleable.ImageFilterView_roundPercent && Build$VERSION.SDK_INT >= 21) {
                    this.setRoundPercent(obtainStyledAttributes.getFloat(index, 0.0f));
                }
            }
            obtainStyledAttributes.recycle();
        }
    }
    
    public void draw(final Canvas canvas) {
        boolean b;
        if (Build$VERSION.SDK_INT < 21 && this.mRound != 0.0f && this.mPath != null) {
            b = true;
            canvas.save();
            canvas.clipPath(this.mPath);
        }
        else {
            b = false;
        }
        super.draw(canvas);
        if (b) {
            canvas.restore();
        }
    }
    
    public float getRound() {
        return this.mRound;
    }
    
    public float getRoundPercent() {
        return this.mRoundPercent;
    }
    
    public void setRound(float n) {
        if (Float.isNaN(n)) {
            this.mRound = n;
            n = this.mRoundPercent;
            this.mRoundPercent = -1.0f;
            this.setRoundPercent(n);
            return;
        }
        final boolean b = this.mRound != n;
        this.mRound = n;
        if (n != 0.0f) {
            if (this.mPath == null) {
                this.mPath = new Path();
            }
            if (this.mRect == null) {
                this.mRect = new RectF();
            }
            if (Build$VERSION.SDK_INT >= 21) {
                if (this.mViewOutlineProvider == null) {
                    this.setOutlineProvider(this.mViewOutlineProvider = new ViewOutlineProvider(this) {
                        final MotionButton this$0;
                        
                        public void getOutline(final View view, final Outline outline) {
                            outline.setRoundRect(0, 0, this.this$0.getWidth(), this.this$0.getHeight(), this.this$0.mRound);
                        }
                    });
                }
                this.setClipToOutline(true);
            }
            this.mRect.set(0.0f, 0.0f, (float)this.getWidth(), (float)this.getHeight());
            this.mPath.reset();
            final Path mPath = this.mPath;
            final RectF mRect = this.mRect;
            n = this.mRound;
            mPath.addRoundRect(mRect, n, n, Path$Direction.CW);
        }
        else if (Build$VERSION.SDK_INT >= 21) {
            this.setClipToOutline(false);
        }
        if (b && Build$VERSION.SDK_INT >= 21) {
            this.invalidateOutline();
        }
    }
    
    public void setRoundPercent(float mRoundPercent) {
        final boolean b = this.mRoundPercent != mRoundPercent;
        this.mRoundPercent = mRoundPercent;
        if (mRoundPercent != 0.0f) {
            if (this.mPath == null) {
                this.mPath = new Path();
            }
            if (this.mRect == null) {
                this.mRect = new RectF();
            }
            if (Build$VERSION.SDK_INT >= 21) {
                if (this.mViewOutlineProvider == null) {
                    this.setOutlineProvider(this.mViewOutlineProvider = new ViewOutlineProvider(this) {
                        final MotionButton this$0;
                        
                        public void getOutline(final View view, final Outline outline) {
                            final int width = this.this$0.getWidth();
                            final int height = this.this$0.getHeight();
                            outline.setRoundRect(0, 0, width, height, Math.min(width, height) * this.this$0.mRoundPercent / 2.0f);
                        }
                    });
                }
                this.setClipToOutline(true);
            }
            final int width = this.getWidth();
            final int height = this.getHeight();
            mRoundPercent = Math.min(width, height) * this.mRoundPercent / 2.0f;
            this.mRect.set(0.0f, 0.0f, (float)width, (float)height);
            this.mPath.reset();
            this.mPath.addRoundRect(this.mRect, mRoundPercent, mRoundPercent, Path$Direction.CW);
        }
        else if (Build$VERSION.SDK_INT >= 21) {
            this.setClipToOutline(false);
        }
        if (b && Build$VERSION.SDK_INT >= 21) {
            this.invalidateOutline();
        }
    }
}

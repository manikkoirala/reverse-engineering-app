// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.helper.widget;

import androidx.constraintlayout.widget.ConstraintSet;
import android.util.Log;
import android.content.res.TypedArray;
import androidx.constraintlayout.widget.R;
import java.util.Iterator;
import androidx.constraintlayout.motion.widget.MotionScene;
import android.util.AttributeSet;
import android.content.Context;
import androidx.constraintlayout.motion.widget.MotionLayout;
import android.view.View;
import java.util.ArrayList;
import androidx.constraintlayout.motion.widget.MotionHelper;

public class Carousel extends MotionHelper
{
    private static final boolean DEBUG = false;
    private static final String TAG = "Carousel";
    public static final int TOUCH_UP_CARRY_ON = 2;
    public static final int TOUCH_UP_IMMEDIATE_STOP = 1;
    private int backwardTransition;
    private float dampening;
    private int emptyViewBehavior;
    private int firstViewReference;
    private int forwardTransition;
    private boolean infiniteCarousel;
    private Adapter mAdapter;
    private int mAnimateTargetDelay;
    private int mIndex;
    int mLastStartId;
    private final ArrayList<View> mList;
    private MotionLayout mMotionLayout;
    private int mPreviousIndex;
    private int mTargetIndex;
    Runnable mUpdateRunnable;
    private int nextState;
    private int previousState;
    private int startIndex;
    private int touchUpMode;
    private float velocityThreshold;
    
    public Carousel(final Context context) {
        super(context);
        this.mAdapter = null;
        this.mList = new ArrayList<View>();
        this.mPreviousIndex = 0;
        this.mIndex = 0;
        this.firstViewReference = -1;
        this.infiniteCarousel = false;
        this.backwardTransition = -1;
        this.forwardTransition = -1;
        this.previousState = -1;
        this.nextState = -1;
        this.dampening = 0.9f;
        this.startIndex = 0;
        this.emptyViewBehavior = 4;
        this.touchUpMode = 1;
        this.velocityThreshold = 2.0f;
        this.mTargetIndex = -1;
        this.mAnimateTargetDelay = 200;
        this.mLastStartId = -1;
        this.mUpdateRunnable = new Runnable() {
            final Carousel this$0;
            
            @Override
            public void run() {
                this.this$0.mMotionLayout.setProgress(0.0f);
                this.this$0.updateItems();
                this.this$0.mAdapter.onNewItem(this.this$0.mIndex);
                final float velocity = this.this$0.mMotionLayout.getVelocity();
                if (this.this$0.touchUpMode == 2 && velocity > this.this$0.velocityThreshold && this.this$0.mIndex < this.this$0.mAdapter.count() - 1) {
                    final float access$600 = this.this$0.dampening;
                    if (this.this$0.mIndex == 0 && this.this$0.mPreviousIndex > this.this$0.mIndex) {
                        return;
                    }
                    if (this.this$0.mIndex == this.this$0.mAdapter.count() - 1 && this.this$0.mPreviousIndex < this.this$0.mIndex) {
                        return;
                    }
                    this.this$0.mMotionLayout.post((Runnable)new Runnable(this, velocity * access$600) {
                        final Carousel$1 this$1;
                        final float val$v;
                        
                        @Override
                        public void run() {
                            this.this$1.this$0.mMotionLayout.touchAnimateTo(5, 1.0f, this.val$v);
                        }
                    });
                }
            }
        };
    }
    
    public Carousel(final Context context, final AttributeSet set) {
        super(context, set);
        this.mAdapter = null;
        this.mList = new ArrayList<View>();
        this.mPreviousIndex = 0;
        this.mIndex = 0;
        this.firstViewReference = -1;
        this.infiniteCarousel = false;
        this.backwardTransition = -1;
        this.forwardTransition = -1;
        this.previousState = -1;
        this.nextState = -1;
        this.dampening = 0.9f;
        this.startIndex = 0;
        this.emptyViewBehavior = 4;
        this.touchUpMode = 1;
        this.velocityThreshold = 2.0f;
        this.mTargetIndex = -1;
        this.mAnimateTargetDelay = 200;
        this.mLastStartId = -1;
        this.mUpdateRunnable = new Runnable() {
            final Carousel this$0;
            
            @Override
            public void run() {
                this.this$0.mMotionLayout.setProgress(0.0f);
                this.this$0.updateItems();
                this.this$0.mAdapter.onNewItem(this.this$0.mIndex);
                final float velocity = this.this$0.mMotionLayout.getVelocity();
                if (this.this$0.touchUpMode == 2 && velocity > this.this$0.velocityThreshold && this.this$0.mIndex < this.this$0.mAdapter.count() - 1) {
                    final float access$600 = this.this$0.dampening;
                    if (this.this$0.mIndex == 0 && this.this$0.mPreviousIndex > this.this$0.mIndex) {
                        return;
                    }
                    if (this.this$0.mIndex == this.this$0.mAdapter.count() - 1 && this.this$0.mPreviousIndex < this.this$0.mIndex) {
                        return;
                    }
                    this.this$0.mMotionLayout.post((Runnable)new Runnable(this, velocity * access$600) {
                        final Carousel$1 this$1;
                        final float val$v;
                        
                        @Override
                        public void run() {
                            this.this$1.this$0.mMotionLayout.touchAnimateTo(5, 1.0f, this.val$v);
                        }
                    });
                }
            }
        };
        this.init(context, set);
    }
    
    public Carousel(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
        this.mAdapter = null;
        this.mList = new ArrayList<View>();
        this.mPreviousIndex = 0;
        this.mIndex = 0;
        this.firstViewReference = -1;
        this.infiniteCarousel = false;
        this.backwardTransition = -1;
        this.forwardTransition = -1;
        this.previousState = -1;
        this.nextState = -1;
        this.dampening = 0.9f;
        this.startIndex = 0;
        this.emptyViewBehavior = 4;
        this.touchUpMode = 1;
        this.velocityThreshold = 2.0f;
        this.mTargetIndex = -1;
        this.mAnimateTargetDelay = 200;
        this.mLastStartId = -1;
        this.mUpdateRunnable = new Runnable() {
            final Carousel this$0;
            
            @Override
            public void run() {
                this.this$0.mMotionLayout.setProgress(0.0f);
                this.this$0.updateItems();
                this.this$0.mAdapter.onNewItem(this.this$0.mIndex);
                final float velocity = this.this$0.mMotionLayout.getVelocity();
                if (this.this$0.touchUpMode == 2 && velocity > this.this$0.velocityThreshold && this.this$0.mIndex < this.this$0.mAdapter.count() - 1) {
                    final float access$600 = this.this$0.dampening;
                    if (this.this$0.mIndex == 0 && this.this$0.mPreviousIndex > this.this$0.mIndex) {
                        return;
                    }
                    if (this.this$0.mIndex == this.this$0.mAdapter.count() - 1 && this.this$0.mPreviousIndex < this.this$0.mIndex) {
                        return;
                    }
                    this.this$0.mMotionLayout.post((Runnable)new Runnable(this, velocity * access$600) {
                        final Carousel$1 this$1;
                        final float val$v;
                        
                        @Override
                        public void run() {
                            this.this$1.this$0.mMotionLayout.touchAnimateTo(5, 1.0f, this.val$v);
                        }
                    });
                }
            }
        };
        this.init(context, set);
    }
    
    private void enableAllTransitions(final boolean enabled) {
        final Iterator<MotionScene.Transition> iterator = this.mMotionLayout.getDefinedTransitions().iterator();
        while (iterator.hasNext()) {
            iterator.next().setEnabled(enabled);
        }
    }
    
    private boolean enableTransition(final int n, final boolean enabled) {
        if (n == -1) {
            return false;
        }
        final MotionLayout mMotionLayout = this.mMotionLayout;
        if (mMotionLayout == null) {
            return false;
        }
        final MotionScene.Transition transition = mMotionLayout.getTransition(n);
        if (transition == null) {
            return false;
        }
        if (enabled == transition.isEnabled()) {
            return false;
        }
        transition.setEnabled(enabled);
        return true;
    }
    
    private void init(final Context context, final AttributeSet set) {
        if (set != null) {
            final TypedArray obtainStyledAttributes = context.obtainStyledAttributes(set, R.styleable.Carousel);
            for (int indexCount = obtainStyledAttributes.getIndexCount(), i = 0; i < indexCount; ++i) {
                final int index = obtainStyledAttributes.getIndex(i);
                if (index == R.styleable.Carousel_carousel_firstView) {
                    this.firstViewReference = obtainStyledAttributes.getResourceId(index, this.firstViewReference);
                }
                else if (index == R.styleable.Carousel_carousel_backwardTransition) {
                    this.backwardTransition = obtainStyledAttributes.getResourceId(index, this.backwardTransition);
                }
                else if (index == R.styleable.Carousel_carousel_forwardTransition) {
                    this.forwardTransition = obtainStyledAttributes.getResourceId(index, this.forwardTransition);
                }
                else if (index == R.styleable.Carousel_carousel_emptyViewsBehavior) {
                    this.emptyViewBehavior = obtainStyledAttributes.getInt(index, this.emptyViewBehavior);
                }
                else if (index == R.styleable.Carousel_carousel_previousState) {
                    this.previousState = obtainStyledAttributes.getResourceId(index, this.previousState);
                }
                else if (index == R.styleable.Carousel_carousel_nextState) {
                    this.nextState = obtainStyledAttributes.getResourceId(index, this.nextState);
                }
                else if (index == R.styleable.Carousel_carousel_touchUp_dampeningFactor) {
                    this.dampening = obtainStyledAttributes.getFloat(index, this.dampening);
                }
                else if (index == R.styleable.Carousel_carousel_touchUpMode) {
                    this.touchUpMode = obtainStyledAttributes.getInt(index, this.touchUpMode);
                }
                else if (index == R.styleable.Carousel_carousel_touchUp_velocityThreshold) {
                    this.velocityThreshold = obtainStyledAttributes.getFloat(index, this.velocityThreshold);
                }
                else if (index == R.styleable.Carousel_carousel_infinite) {
                    this.infiniteCarousel = obtainStyledAttributes.getBoolean(index, this.infiniteCarousel);
                }
            }
            obtainStyledAttributes.recycle();
        }
    }
    
    private void updateItems() {
        final Adapter mAdapter = this.mAdapter;
        if (mAdapter == null) {
            return;
        }
        if (this.mMotionLayout == null) {
            return;
        }
        if (mAdapter.count() == 0) {
            return;
        }
        for (int size = this.mList.size(), i = 0; i < size; ++i) {
            final View view = this.mList.get(i);
            final int n = this.mIndex + i - this.startIndex;
            if (this.infiniteCarousel) {
                if (n < 0) {
                    final int emptyViewBehavior = this.emptyViewBehavior;
                    if (emptyViewBehavior != 4) {
                        this.updateViewVisibility(view, emptyViewBehavior);
                    }
                    else {
                        this.updateViewVisibility(view, 0);
                    }
                    if (n % this.mAdapter.count() == 0) {
                        this.mAdapter.populate(view, 0);
                    }
                    else {
                        final Adapter mAdapter2 = this.mAdapter;
                        mAdapter2.populate(view, mAdapter2.count() + n % this.mAdapter.count());
                    }
                }
                else if (n >= this.mAdapter.count()) {
                    int n2;
                    if (n == this.mAdapter.count()) {
                        n2 = 0;
                    }
                    else if ((n2 = n) > this.mAdapter.count()) {
                        n2 = n % this.mAdapter.count();
                    }
                    final int emptyViewBehavior2 = this.emptyViewBehavior;
                    if (emptyViewBehavior2 != 4) {
                        this.updateViewVisibility(view, emptyViewBehavior2);
                    }
                    else {
                        this.updateViewVisibility(view, 0);
                    }
                    this.mAdapter.populate(view, n2);
                }
                else {
                    this.updateViewVisibility(view, 0);
                    this.mAdapter.populate(view, n);
                }
            }
            else if (n < 0) {
                this.updateViewVisibility(view, this.emptyViewBehavior);
            }
            else if (n >= this.mAdapter.count()) {
                this.updateViewVisibility(view, this.emptyViewBehavior);
            }
            else {
                this.updateViewVisibility(view, 0);
                this.mAdapter.populate(view, n);
            }
        }
        final int mTargetIndex = this.mTargetIndex;
        if (mTargetIndex != -1 && mTargetIndex != this.mIndex) {
            this.mMotionLayout.post((Runnable)new Carousel$$ExternalSyntheticLambda0(this));
        }
        else if (mTargetIndex == this.mIndex) {
            this.mTargetIndex = -1;
        }
        if (this.backwardTransition == -1 || this.forwardTransition == -1) {
            Log.w("Carousel", "No backward or forward transitions defined for Carousel!");
            return;
        }
        if (this.infiniteCarousel) {
            return;
        }
        final int count = this.mAdapter.count();
        if (this.mIndex == 0) {
            this.enableTransition(this.backwardTransition, false);
        }
        else {
            this.enableTransition(this.backwardTransition, true);
            this.mMotionLayout.setTransition(this.backwardTransition);
        }
        if (this.mIndex == count - 1) {
            this.enableTransition(this.forwardTransition, false);
        }
        else {
            this.enableTransition(this.forwardTransition, true);
            this.mMotionLayout.setTransition(this.forwardTransition);
        }
    }
    
    private boolean updateViewVisibility(final int n, final View view, final int visibility) {
        final ConstraintSet constraintSet = this.mMotionLayout.getConstraintSet(n);
        if (constraintSet == null) {
            return false;
        }
        final ConstraintSet.Constraint constraint = constraintSet.getConstraint(view.getId());
        if (constraint == null) {
            return false;
        }
        constraint.propertySet.mVisibilityMode = 1;
        view.setVisibility(visibility);
        return true;
    }
    
    private boolean updateViewVisibility(final View view, final int n) {
        final MotionLayout mMotionLayout = this.mMotionLayout;
        int i = 0;
        if (mMotionLayout == null) {
            return false;
        }
        final int[] constraintSetIds = mMotionLayout.getConstraintSetIds();
        boolean b = false;
        while (i < constraintSetIds.length) {
            b |= this.updateViewVisibility(constraintSetIds[i], view, n);
            ++i;
        }
        return b;
    }
    
    public int getCount() {
        final Adapter mAdapter = this.mAdapter;
        if (mAdapter != null) {
            return mAdapter.count();
        }
        return 0;
    }
    
    public int getCurrentIndex() {
        return this.mIndex;
    }
    
    public void jumpToIndex(final int b) {
        this.mIndex = Math.max(0, Math.min(this.getCount() - 1, b));
        this.refresh();
    }
    
    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (this.getParent() instanceof MotionLayout) {
            final MotionLayout mMotionLayout = (MotionLayout)this.getParent();
            for (int i = 0; i < this.mCount; ++i) {
                final int n = this.mIds[i];
                final View viewById = mMotionLayout.getViewById(n);
                if (this.firstViewReference == n) {
                    this.startIndex = i;
                }
                this.mList.add(viewById);
            }
            this.mMotionLayout = mMotionLayout;
            if (this.touchUpMode == 2) {
                final MotionScene.Transition transition = mMotionLayout.getTransition(this.forwardTransition);
                if (transition != null) {
                    transition.setOnTouchUp(5);
                }
                final MotionScene.Transition transition2 = this.mMotionLayout.getTransition(this.backwardTransition);
                if (transition2 != null) {
                    transition2.setOnTouchUp(5);
                }
            }
            this.updateItems();
        }
    }
    
    @Override
    public void onTransitionChange(final MotionLayout motionLayout, final int mLastStartId, final int n, final float n2) {
        this.mLastStartId = mLastStartId;
    }
    
    @Override
    public void onTransitionCompleted(final MotionLayout motionLayout, final int n) {
        final int mIndex = this.mIndex;
        this.mPreviousIndex = mIndex;
        if (n == this.nextState) {
            this.mIndex = mIndex + 1;
        }
        else if (n == this.previousState) {
            this.mIndex = mIndex - 1;
        }
        if (this.infiniteCarousel) {
            if (this.mIndex >= this.mAdapter.count()) {
                this.mIndex = 0;
            }
            if (this.mIndex < 0) {
                this.mIndex = this.mAdapter.count() - 1;
            }
        }
        else {
            if (this.mIndex >= this.mAdapter.count()) {
                this.mIndex = this.mAdapter.count() - 1;
            }
            if (this.mIndex < 0) {
                this.mIndex = 0;
            }
        }
        if (this.mPreviousIndex != this.mIndex) {
            this.mMotionLayout.post(this.mUpdateRunnable);
        }
    }
    
    public void refresh() {
        for (int size = this.mList.size(), i = 0; i < size; ++i) {
            final View view = this.mList.get(i);
            if (this.mAdapter.count() == 0) {
                this.updateViewVisibility(view, this.emptyViewBehavior);
            }
            else {
                this.updateViewVisibility(view, 0);
            }
        }
        this.mMotionLayout.rebuildScene();
        this.updateItems();
    }
    
    public void setAdapter(final Adapter mAdapter) {
        this.mAdapter = mAdapter;
    }
    
    public void transitionToIndex(final int b, int max) {
        this.mTargetIndex = Math.max(0, Math.min(this.getCount() - 1, b));
        max = Math.max(0, max);
        this.mAnimateTargetDelay = max;
        this.mMotionLayout.setTransitionDuration(max);
        if (b < this.mIndex) {
            this.mMotionLayout.transitionToState(this.previousState, this.mAnimateTargetDelay);
        }
        else {
            this.mMotionLayout.transitionToState(this.nextState, this.mAnimateTargetDelay);
        }
    }
    
    public interface Adapter
    {
        int count();
        
        void onNewItem(final int p0);
        
        void populate(final View p0, final int p1);
    }
}

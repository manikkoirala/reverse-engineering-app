// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.helper.widget;

import androidx.constraintlayout.widget.ConstraintSet;
import android.content.res.TypedArray;
import androidx.constraintlayout.widget.R;
import android.view.View;
import android.view.ViewGroup$LayoutParams;
import android.util.Log;
import java.util.Arrays;
import android.util.AttributeSet;
import android.content.Context;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.VirtualLayout;

public class CircularFlow extends VirtualLayout
{
    private static float DEFAULT_ANGLE = 0.0f;
    private static int DEFAULT_RADIUS = 0;
    private static final String TAG = "CircularFlow";
    private float[] mAngles;
    ConstraintLayout mContainer;
    private int mCountAngle;
    private int mCountRadius;
    private int[] mRadius;
    private String mReferenceAngles;
    private Float mReferenceDefaultAngle;
    private Integer mReferenceDefaultRadius;
    private String mReferenceRadius;
    int mViewCenter;
    
    public CircularFlow(final Context context) {
        super(context);
    }
    
    public CircularFlow(final Context context, final AttributeSet set) {
        super(context, set);
    }
    
    public CircularFlow(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
    }
    
    private void addAngle(final String s) {
        if (s != null) {
            if (s.length() != 0) {
                if (this.myContext == null) {
                    return;
                }
                final float[] mAngles = this.mAngles;
                if (mAngles == null) {
                    return;
                }
                if (this.mCountAngle + 1 > mAngles.length) {
                    this.mAngles = Arrays.copyOf(mAngles, mAngles.length + 1);
                }
                this.mAngles[this.mCountAngle] = (float)Integer.parseInt(s);
                ++this.mCountAngle;
            }
        }
    }
    
    private void addRadius(final String s) {
        if (s != null) {
            if (s.length() != 0) {
                if (this.myContext == null) {
                    return;
                }
                final int[] mRadius = this.mRadius;
                if (mRadius == null) {
                    return;
                }
                if (this.mCountRadius + 1 > mRadius.length) {
                    this.mRadius = Arrays.copyOf(mRadius, mRadius.length + 1);
                }
                this.mRadius[this.mCountRadius] = (int)(Integer.parseInt(s) * this.myContext.getResources().getDisplayMetrics().density);
                ++this.mCountRadius;
            }
        }
    }
    
    private void anchorReferences() {
        this.mContainer = (ConstraintLayout)this.getParent();
        for (int i = 0; i < this.mCount; ++i) {
            final View viewById = this.mContainer.getViewById(this.mIds[i]);
            if (viewById != null) {
                int default_RADIUS = CircularFlow.DEFAULT_RADIUS;
                float default_ANGLE = CircularFlow.DEFAULT_ANGLE;
                final int[] mRadius = this.mRadius;
                if (mRadius != null && i < mRadius.length) {
                    default_RADIUS = mRadius[i];
                }
                else {
                    final Integer mReferenceDefaultRadius = this.mReferenceDefaultRadius;
                    if (mReferenceDefaultRadius != null && mReferenceDefaultRadius != -1) {
                        ++this.mCountRadius;
                        if (this.mRadius == null) {
                            this.mRadius = new int[1];
                        }
                        (this.mRadius = this.getRadius())[this.mCountRadius - 1] = default_RADIUS;
                    }
                    else {
                        final StringBuilder sb = new StringBuilder();
                        sb.append("Added radius to view with id: ");
                        sb.append(this.mMap.get(viewById.getId()));
                        Log.e("CircularFlow", sb.toString());
                    }
                }
                final float[] mAngles = this.mAngles;
                if (mAngles != null && i < mAngles.length) {
                    default_ANGLE = mAngles[i];
                }
                else {
                    final Float mReferenceDefaultAngle = this.mReferenceDefaultAngle;
                    if (mReferenceDefaultAngle != null && mReferenceDefaultAngle != -1.0f) {
                        ++this.mCountAngle;
                        if (this.mAngles == null) {
                            this.mAngles = new float[1];
                        }
                        (this.mAngles = this.getAngles())[this.mCountAngle - 1] = default_ANGLE;
                    }
                    else {
                        final StringBuilder sb2 = new StringBuilder();
                        sb2.append("Added angle to view with id: ");
                        sb2.append(this.mMap.get(viewById.getId()));
                        Log.e("CircularFlow", sb2.toString());
                    }
                }
                final ConstraintLayout.LayoutParams layoutParams = (ConstraintLayout.LayoutParams)viewById.getLayoutParams();
                layoutParams.circleAngle = default_ANGLE;
                layoutParams.circleConstraint = this.mViewCenter;
                layoutParams.circleRadius = default_RADIUS;
                viewById.setLayoutParams((ViewGroup$LayoutParams)layoutParams);
            }
        }
        this.applyLayoutFeatures();
    }
    
    private float[] removeAngle(final float[] array, final int n) {
        float[] removeElementFromArray = array;
        if (array != null) {
            removeElementFromArray = array;
            if (n >= 0) {
                if (n >= this.mCountAngle) {
                    removeElementFromArray = array;
                }
                else {
                    removeElementFromArray = removeElementFromArray(array, n);
                }
            }
        }
        return removeElementFromArray;
    }
    
    public static float[] removeElementFromArray(final float[] array, final int n) {
        final float[] array2 = new float[array.length - 1];
        int i = 0;
        int n2 = 0;
        while (i < array.length) {
            if (i != n) {
                array2[n2] = array[i];
                ++n2;
            }
            ++i;
        }
        return array2;
    }
    
    public static int[] removeElementFromArray(final int[] array, final int n) {
        final int[] array2 = new int[array.length - 1];
        int i = 0;
        int n2 = 0;
        while (i < array.length) {
            if (i != n) {
                array2[n2] = array[i];
                ++n2;
            }
            ++i;
        }
        return array2;
    }
    
    private int[] removeRadius(final int[] array, final int n) {
        int[] removeElementFromArray = array;
        if (array != null) {
            removeElementFromArray = array;
            if (n >= 0) {
                if (n >= this.mCountRadius) {
                    removeElementFromArray = array;
                }
                else {
                    removeElementFromArray = removeElementFromArray(array, n);
                }
            }
        }
        return removeElementFromArray;
    }
    
    private void setAngles(final String s) {
        if (s == null) {
            return;
        }
        int beginIndex = 0;
        this.mCountAngle = 0;
        while (true) {
            final int index = s.indexOf(44, beginIndex);
            if (index == -1) {
                break;
            }
            this.addAngle(s.substring(beginIndex, index).trim());
            beginIndex = index + 1;
        }
        this.addAngle(s.substring(beginIndex).trim());
    }
    
    private void setRadius(final String s) {
        if (s == null) {
            return;
        }
        int beginIndex = 0;
        this.mCountRadius = 0;
        while (true) {
            final int index = s.indexOf(44, beginIndex);
            if (index == -1) {
                break;
            }
            this.addRadius(s.substring(beginIndex, index).trim());
            beginIndex = index + 1;
        }
        this.addRadius(s.substring(beginIndex).trim());
    }
    
    public void addViewToCircularFlow(final View view, final int n, final float n2) {
        if (this.containsId(view.getId())) {
            return;
        }
        this.addView(view);
        ++this.mCountAngle;
        (this.mAngles = this.getAngles())[this.mCountAngle - 1] = n2;
        ++this.mCountRadius;
        (this.mRadius = this.getRadius())[this.mCountRadius - 1] = (int)(n * this.myContext.getResources().getDisplayMetrics().density);
        this.anchorReferences();
    }
    
    public float[] getAngles() {
        return Arrays.copyOf(this.mAngles, this.mCountAngle);
    }
    
    public int[] getRadius() {
        return Arrays.copyOf(this.mRadius, this.mCountRadius);
    }
    
    @Override
    protected void init(final AttributeSet set) {
        super.init(set);
        if (set != null) {
            final TypedArray obtainStyledAttributes = this.getContext().obtainStyledAttributes(set, R.styleable.ConstraintLayout_Layout);
            for (int indexCount = obtainStyledAttributes.getIndexCount(), i = 0; i < indexCount; ++i) {
                final int index = obtainStyledAttributes.getIndex(i);
                if (index == R.styleable.ConstraintLayout_Layout_circularflow_viewCenter) {
                    this.mViewCenter = obtainStyledAttributes.getResourceId(index, 0);
                }
                else if (index == R.styleable.ConstraintLayout_Layout_circularflow_angles) {
                    this.setAngles(this.mReferenceAngles = obtainStyledAttributes.getString(index));
                }
                else if (index == R.styleable.ConstraintLayout_Layout_circularflow_radiusInDP) {
                    this.setRadius(this.mReferenceRadius = obtainStyledAttributes.getString(index));
                }
                else if (index == R.styleable.ConstraintLayout_Layout_circularflow_defaultAngle) {
                    final Float value = obtainStyledAttributes.getFloat(index, CircularFlow.DEFAULT_ANGLE);
                    this.mReferenceDefaultAngle = value;
                    this.setDefaultAngle(value);
                }
                else if (index == R.styleable.ConstraintLayout_Layout_circularflow_defaultRadius) {
                    final Integer value2 = obtainStyledAttributes.getDimensionPixelSize(index, CircularFlow.DEFAULT_RADIUS);
                    this.mReferenceDefaultRadius = value2;
                    this.setDefaultRadius(value2);
                }
            }
            obtainStyledAttributes.recycle();
        }
    }
    
    public boolean isUpdatable(final View view) {
        final boolean containsId = this.containsId(view.getId());
        boolean b = false;
        if (!containsId) {
            return false;
        }
        if (this.indexFromId(view.getId()) != -1) {
            b = true;
        }
        return b;
    }
    
    @Override
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        final String mReferenceAngles = this.mReferenceAngles;
        if (mReferenceAngles != null) {
            this.mAngles = new float[1];
            this.setAngles(mReferenceAngles);
        }
        final String mReferenceRadius = this.mReferenceRadius;
        if (mReferenceRadius != null) {
            this.mRadius = new int[1];
            this.setRadius(mReferenceRadius);
        }
        final Float mReferenceDefaultAngle = this.mReferenceDefaultAngle;
        if (mReferenceDefaultAngle != null) {
            this.setDefaultAngle(mReferenceDefaultAngle);
        }
        final Integer mReferenceDefaultRadius = this.mReferenceDefaultRadius;
        if (mReferenceDefaultRadius != null) {
            this.setDefaultRadius(mReferenceDefaultRadius);
        }
        this.anchorReferences();
    }
    
    @Override
    public int removeView(final View view) {
        final int removeView = super.removeView(view);
        if (removeView == -1) {
            return removeView;
        }
        final ConstraintSet set = new ConstraintSet();
        set.clone(this.mContainer);
        set.clear(view.getId(), 8);
        set.applyTo(this.mContainer);
        final float[] mAngles = this.mAngles;
        if (removeView < mAngles.length) {
            this.mAngles = this.removeAngle(mAngles, removeView);
            --this.mCountAngle;
        }
        final int[] mRadius = this.mRadius;
        if (removeView < mRadius.length) {
            this.mRadius = this.removeRadius(mRadius, removeView);
            --this.mCountRadius;
        }
        this.anchorReferences();
        return removeView;
    }
    
    public void setDefaultAngle(final float default_ANGLE) {
        CircularFlow.DEFAULT_ANGLE = default_ANGLE;
    }
    
    public void setDefaultRadius(final int default_RADIUS) {
        CircularFlow.DEFAULT_RADIUS = default_RADIUS;
    }
    
    public void updateAngle(final View view, final float n) {
        if (!this.isUpdatable(view)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("It was not possible to update angle to view with id: ");
            sb.append(view.getId());
            Log.e("CircularFlow", sb.toString());
            return;
        }
        final int indexFromId = this.indexFromId(view.getId());
        if (indexFromId > this.mAngles.length) {
            return;
        }
        (this.mAngles = this.getAngles())[indexFromId] = n;
        this.anchorReferences();
    }
    
    public void updateRadius(final View view, final int n) {
        if (!this.isUpdatable(view)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("It was not possible to update radius to view with id: ");
            sb.append(view.getId());
            Log.e("CircularFlow", sb.toString());
            return;
        }
        final int indexFromId = this.indexFromId(view.getId());
        if (indexFromId > this.mRadius.length) {
            return;
        }
        (this.mRadius = this.getRadius())[indexFromId] = (int)(n * this.myContext.getResources().getDisplayMetrics().density);
        this.anchorReferences();
    }
    
    public void updateReference(final View view, final int n, final float n2) {
        if (!this.isUpdatable(view)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("It was not possible to update radius and angle to view with id: ");
            sb.append(view.getId());
            Log.e("CircularFlow", sb.toString());
            return;
        }
        final int indexFromId = this.indexFromId(view.getId());
        if (this.getAngles().length > indexFromId) {
            (this.mAngles = this.getAngles())[indexFromId] = n2;
        }
        if (this.getRadius().length > indexFromId) {
            (this.mRadius = this.getRadius())[indexFromId] = (int)(n * this.myContext.getResources().getDisplayMetrics().density);
        }
        this.anchorReferences();
    }
}

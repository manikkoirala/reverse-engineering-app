// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.helper.widget;

import android.view.View$MeasureSpec;
import androidx.constraintlayout.core.widgets.ConstraintWidget;
import android.util.SparseArray;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.core.widgets.HelperWidget;
import androidx.constraintlayout.widget.ConstraintSet;
import android.content.res.TypedArray;
import android.os.Build$VERSION;
import androidx.constraintlayout.widget.R;
import android.util.AttributeSet;
import android.content.Context;
import androidx.constraintlayout.widget.VirtualLayout;

public class Flow extends VirtualLayout
{
    public static final int CHAIN_PACKED = 2;
    public static final int CHAIN_SPREAD = 0;
    public static final int CHAIN_SPREAD_INSIDE = 1;
    public static final int HORIZONTAL = 0;
    public static final int HORIZONTAL_ALIGN_CENTER = 2;
    public static final int HORIZONTAL_ALIGN_END = 1;
    public static final int HORIZONTAL_ALIGN_START = 0;
    private static final String TAG = "Flow";
    public static final int VERTICAL = 1;
    public static final int VERTICAL_ALIGN_BASELINE = 3;
    public static final int VERTICAL_ALIGN_BOTTOM = 1;
    public static final int VERTICAL_ALIGN_CENTER = 2;
    public static final int VERTICAL_ALIGN_TOP = 0;
    public static final int WRAP_ALIGNED = 2;
    public static final int WRAP_CHAIN = 1;
    public static final int WRAP_NONE = 0;
    private androidx.constraintlayout.core.widgets.Flow mFlow;
    
    public Flow(final Context context) {
        super(context);
    }
    
    public Flow(final Context context, final AttributeSet set) {
        super(context, set);
    }
    
    public Flow(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
    }
    
    @Override
    protected void init(final AttributeSet set) {
        super.init(set);
        this.mFlow = new androidx.constraintlayout.core.widgets.Flow();
        if (set != null) {
            final TypedArray obtainStyledAttributes = this.getContext().obtainStyledAttributes(set, R.styleable.ConstraintLayout_Layout);
            for (int indexCount = obtainStyledAttributes.getIndexCount(), i = 0; i < indexCount; ++i) {
                final int index = obtainStyledAttributes.getIndex(i);
                if (index == R.styleable.ConstraintLayout_Layout_android_orientation) {
                    this.mFlow.setOrientation(obtainStyledAttributes.getInt(index, 0));
                }
                else if (index == R.styleable.ConstraintLayout_Layout_android_padding) {
                    this.mFlow.setPadding(obtainStyledAttributes.getDimensionPixelSize(index, 0));
                }
                else if (index == R.styleable.ConstraintLayout_Layout_android_paddingStart) {
                    if (Build$VERSION.SDK_INT >= 17) {
                        this.mFlow.setPaddingStart(obtainStyledAttributes.getDimensionPixelSize(index, 0));
                    }
                }
                else if (index == R.styleable.ConstraintLayout_Layout_android_paddingEnd) {
                    if (Build$VERSION.SDK_INT >= 17) {
                        this.mFlow.setPaddingEnd(obtainStyledAttributes.getDimensionPixelSize(index, 0));
                    }
                }
                else if (index == R.styleable.ConstraintLayout_Layout_android_paddingLeft) {
                    this.mFlow.setPaddingLeft(obtainStyledAttributes.getDimensionPixelSize(index, 0));
                }
                else if (index == R.styleable.ConstraintLayout_Layout_android_paddingTop) {
                    this.mFlow.setPaddingTop(obtainStyledAttributes.getDimensionPixelSize(index, 0));
                }
                else if (index == R.styleable.ConstraintLayout_Layout_android_paddingRight) {
                    this.mFlow.setPaddingRight(obtainStyledAttributes.getDimensionPixelSize(index, 0));
                }
                else if (index == R.styleable.ConstraintLayout_Layout_android_paddingBottom) {
                    this.mFlow.setPaddingBottom(obtainStyledAttributes.getDimensionPixelSize(index, 0));
                }
                else if (index == R.styleable.ConstraintLayout_Layout_flow_wrapMode) {
                    this.mFlow.setWrapMode(obtainStyledAttributes.getInt(index, 0));
                }
                else if (index == R.styleable.ConstraintLayout_Layout_flow_horizontalStyle) {
                    this.mFlow.setHorizontalStyle(obtainStyledAttributes.getInt(index, 0));
                }
                else if (index == R.styleable.ConstraintLayout_Layout_flow_verticalStyle) {
                    this.mFlow.setVerticalStyle(obtainStyledAttributes.getInt(index, 0));
                }
                else if (index == R.styleable.ConstraintLayout_Layout_flow_firstHorizontalStyle) {
                    this.mFlow.setFirstHorizontalStyle(obtainStyledAttributes.getInt(index, 0));
                }
                else if (index == R.styleable.ConstraintLayout_Layout_flow_lastHorizontalStyle) {
                    this.mFlow.setLastHorizontalStyle(obtainStyledAttributes.getInt(index, 0));
                }
                else if (index == R.styleable.ConstraintLayout_Layout_flow_firstVerticalStyle) {
                    this.mFlow.setFirstVerticalStyle(obtainStyledAttributes.getInt(index, 0));
                }
                else if (index == R.styleable.ConstraintLayout_Layout_flow_lastVerticalStyle) {
                    this.mFlow.setLastVerticalStyle(obtainStyledAttributes.getInt(index, 0));
                }
                else if (index == R.styleable.ConstraintLayout_Layout_flow_horizontalBias) {
                    this.mFlow.setHorizontalBias(obtainStyledAttributes.getFloat(index, 0.5f));
                }
                else if (index == R.styleable.ConstraintLayout_Layout_flow_firstHorizontalBias) {
                    this.mFlow.setFirstHorizontalBias(obtainStyledAttributes.getFloat(index, 0.5f));
                }
                else if (index == R.styleable.ConstraintLayout_Layout_flow_lastHorizontalBias) {
                    this.mFlow.setLastHorizontalBias(obtainStyledAttributes.getFloat(index, 0.5f));
                }
                else if (index == R.styleable.ConstraintLayout_Layout_flow_firstVerticalBias) {
                    this.mFlow.setFirstVerticalBias(obtainStyledAttributes.getFloat(index, 0.5f));
                }
                else if (index == R.styleable.ConstraintLayout_Layout_flow_lastVerticalBias) {
                    this.mFlow.setLastVerticalBias(obtainStyledAttributes.getFloat(index, 0.5f));
                }
                else if (index == R.styleable.ConstraintLayout_Layout_flow_verticalBias) {
                    this.mFlow.setVerticalBias(obtainStyledAttributes.getFloat(index, 0.5f));
                }
                else if (index == R.styleable.ConstraintLayout_Layout_flow_horizontalAlign) {
                    this.mFlow.setHorizontalAlign(obtainStyledAttributes.getInt(index, 2));
                }
                else if (index == R.styleable.ConstraintLayout_Layout_flow_verticalAlign) {
                    this.mFlow.setVerticalAlign(obtainStyledAttributes.getInt(index, 2));
                }
                else if (index == R.styleable.ConstraintLayout_Layout_flow_horizontalGap) {
                    this.mFlow.setHorizontalGap(obtainStyledAttributes.getDimensionPixelSize(index, 0));
                }
                else if (index == R.styleable.ConstraintLayout_Layout_flow_verticalGap) {
                    this.mFlow.setVerticalGap(obtainStyledAttributes.getDimensionPixelSize(index, 0));
                }
                else if (index == R.styleable.ConstraintLayout_Layout_flow_maxElementsWrap) {
                    this.mFlow.setMaxElementsWrap(obtainStyledAttributes.getInt(index, -1));
                }
            }
            obtainStyledAttributes.recycle();
        }
        this.mHelperWidget = this.mFlow;
        this.validateParams();
    }
    
    @Override
    public void loadParameters(final ConstraintSet.Constraint constraint, final HelperWidget helperWidget, final ConstraintLayout.LayoutParams layoutParams, final SparseArray<ConstraintWidget> sparseArray) {
        super.loadParameters(constraint, helperWidget, layoutParams, sparseArray);
        if (helperWidget instanceof androidx.constraintlayout.core.widgets.Flow) {
            final androidx.constraintlayout.core.widgets.Flow flow = (androidx.constraintlayout.core.widgets.Flow)helperWidget;
            if (layoutParams.orientation != -1) {
                flow.setOrientation(layoutParams.orientation);
            }
        }
    }
    
    @Override
    protected void onMeasure(final int n, final int n2) {
        this.onMeasure(this.mFlow, n, n2);
    }
    
    @Override
    public void onMeasure(final androidx.constraintlayout.core.widgets.VirtualLayout virtualLayout, int mode, int size) {
        final int mode2 = View$MeasureSpec.getMode(mode);
        final int size2 = View$MeasureSpec.getSize(mode);
        mode = View$MeasureSpec.getMode(size);
        size = View$MeasureSpec.getSize(size);
        if (virtualLayout != null) {
            virtualLayout.measure(mode2, size2, mode, size);
            this.setMeasuredDimension(virtualLayout.getMeasuredWidth(), virtualLayout.getMeasuredHeight());
        }
        else {
            this.setMeasuredDimension(0, 0);
        }
    }
    
    @Override
    public void resolveRtl(final ConstraintWidget constraintWidget, final boolean b) {
        this.mFlow.applyRtl(b);
    }
    
    public void setFirstHorizontalBias(final float firstHorizontalBias) {
        this.mFlow.setFirstHorizontalBias(firstHorizontalBias);
        this.requestLayout();
    }
    
    public void setFirstHorizontalStyle(final int firstHorizontalStyle) {
        this.mFlow.setFirstHorizontalStyle(firstHorizontalStyle);
        this.requestLayout();
    }
    
    public void setFirstVerticalBias(final float firstVerticalBias) {
        this.mFlow.setFirstVerticalBias(firstVerticalBias);
        this.requestLayout();
    }
    
    public void setFirstVerticalStyle(final int firstVerticalStyle) {
        this.mFlow.setFirstVerticalStyle(firstVerticalStyle);
        this.requestLayout();
    }
    
    public void setHorizontalAlign(final int horizontalAlign) {
        this.mFlow.setHorizontalAlign(horizontalAlign);
        this.requestLayout();
    }
    
    public void setHorizontalBias(final float horizontalBias) {
        this.mFlow.setHorizontalBias(horizontalBias);
        this.requestLayout();
    }
    
    public void setHorizontalGap(final int horizontalGap) {
        this.mFlow.setHorizontalGap(horizontalGap);
        this.requestLayout();
    }
    
    public void setHorizontalStyle(final int horizontalStyle) {
        this.mFlow.setHorizontalStyle(horizontalStyle);
        this.requestLayout();
    }
    
    public void setLastHorizontalBias(final float lastHorizontalBias) {
        this.mFlow.setLastHorizontalBias(lastHorizontalBias);
        this.requestLayout();
    }
    
    public void setLastHorizontalStyle(final int lastHorizontalStyle) {
        this.mFlow.setLastHorizontalStyle(lastHorizontalStyle);
        this.requestLayout();
    }
    
    public void setLastVerticalBias(final float lastVerticalBias) {
        this.mFlow.setLastVerticalBias(lastVerticalBias);
        this.requestLayout();
    }
    
    public void setLastVerticalStyle(final int lastVerticalStyle) {
        this.mFlow.setLastVerticalStyle(lastVerticalStyle);
        this.requestLayout();
    }
    
    public void setMaxElementsWrap(final int maxElementsWrap) {
        this.mFlow.setMaxElementsWrap(maxElementsWrap);
        this.requestLayout();
    }
    
    public void setOrientation(final int orientation) {
        this.mFlow.setOrientation(orientation);
        this.requestLayout();
    }
    
    public void setPadding(final int padding) {
        this.mFlow.setPadding(padding);
        this.requestLayout();
    }
    
    public void setPaddingBottom(final int paddingBottom) {
        this.mFlow.setPaddingBottom(paddingBottom);
        this.requestLayout();
    }
    
    public void setPaddingLeft(final int paddingLeft) {
        this.mFlow.setPaddingLeft(paddingLeft);
        this.requestLayout();
    }
    
    public void setPaddingRight(final int paddingRight) {
        this.mFlow.setPaddingRight(paddingRight);
        this.requestLayout();
    }
    
    public void setPaddingTop(final int paddingTop) {
        this.mFlow.setPaddingTop(paddingTop);
        this.requestLayout();
    }
    
    public void setVerticalAlign(final int verticalAlign) {
        this.mFlow.setVerticalAlign(verticalAlign);
        this.requestLayout();
    }
    
    public void setVerticalBias(final float verticalBias) {
        this.mFlow.setVerticalBias(verticalBias);
        this.requestLayout();
    }
    
    public void setVerticalGap(final int verticalGap) {
        this.mFlow.setVerticalGap(verticalGap);
        this.requestLayout();
    }
    
    public void setVerticalStyle(final int verticalStyle) {
        this.mFlow.setVerticalStyle(verticalStyle);
        this.requestLayout();
    }
    
    public void setWrapMode(final int wrapMode) {
        this.mFlow.setWrapMode(wrapMode);
        this.requestLayout();
    }
}

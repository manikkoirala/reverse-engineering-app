// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.helper.widget;

import androidx.constraintlayout.core.widgets.ConstraintWidget;
import android.util.SparseArray;
import androidx.constraintlayout.core.widgets.Helper;
import androidx.constraintlayout.core.widgets.ConstraintWidgetContainer;
import android.view.View$MeasureSpec;
import android.util.AttributeSet;
import android.content.Context;
import androidx.constraintlayout.core.widgets.Placeholder;
import androidx.constraintlayout.widget.VirtualLayout;

public class MotionPlaceholder extends VirtualLayout
{
    private static final String TAG = "MotionPlaceholder";
    Placeholder mPlaceholder;
    
    public MotionPlaceholder(final Context context) {
        super(context);
    }
    
    public MotionPlaceholder(final Context context, final AttributeSet set) {
        super(context, set);
    }
    
    public MotionPlaceholder(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
    }
    
    public MotionPlaceholder(final Context context, final AttributeSet set, final int n, final int n2) {
        super(context, set, n);
    }
    
    @Override
    protected void init(final AttributeSet set) {
        super.init(set);
        this.mHelperWidget = new Placeholder();
        this.validateParams();
    }
    
    @Override
    protected void onMeasure(final int n, final int n2) {
        this.onMeasure(this.mPlaceholder, n, n2);
    }
    
    @Override
    public void onMeasure(final androidx.constraintlayout.core.widgets.VirtualLayout virtualLayout, int mode, int size) {
        final int mode2 = View$MeasureSpec.getMode(mode);
        final int size2 = View$MeasureSpec.getSize(mode);
        mode = View$MeasureSpec.getMode(size);
        size = View$MeasureSpec.getSize(size);
        if (virtualLayout != null) {
            virtualLayout.measure(mode2, size2, mode, size);
            this.setMeasuredDimension(virtualLayout.getMeasuredWidth(), virtualLayout.getMeasuredHeight());
        }
        else {
            this.setMeasuredDimension(0, 0);
        }
    }
    
    @Override
    public void updatePreLayout(final ConstraintWidgetContainer constraintWidgetContainer, final Helper helper, final SparseArray<ConstraintWidget> sparseArray) {
    }
}

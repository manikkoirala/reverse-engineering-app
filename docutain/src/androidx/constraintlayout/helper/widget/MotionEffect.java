// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.helper.widget;

import androidx.constraintlayout.motion.widget.Key;
import androidx.constraintlayout.motion.widget.KeyPosition;
import androidx.constraintlayout.motion.widget.KeyAttributes;
import android.util.Log;
import androidx.constraintlayout.motion.widget.Debug;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.motion.widget.MotionController;
import android.view.View;
import java.util.HashMap;
import androidx.constraintlayout.motion.widget.MotionLayout;
import android.content.res.TypedArray;
import androidx.constraintlayout.widget.R;
import android.util.AttributeSet;
import android.content.Context;
import androidx.constraintlayout.motion.widget.MotionHelper;

public class MotionEffect extends MotionHelper
{
    public static final int AUTO = -1;
    public static final int EAST = 2;
    public static final int NORTH = 0;
    public static final int SOUTH = 1;
    public static final String TAG = "FadeMove";
    private static final int UNSET = -1;
    public static final int WEST = 3;
    private int fadeMove;
    private float motionEffectAlpha;
    private int motionEffectEnd;
    private int motionEffectStart;
    private boolean motionEffectStrictMove;
    private int motionEffectTranslationX;
    private int motionEffectTranslationY;
    private int viewTransitionId;
    
    public MotionEffect(final Context context) {
        super(context);
        this.motionEffectAlpha = 0.1f;
        this.motionEffectStart = 49;
        this.motionEffectEnd = 50;
        this.motionEffectTranslationX = 0;
        this.motionEffectTranslationY = 0;
        this.motionEffectStrictMove = true;
        this.viewTransitionId = -1;
        this.fadeMove = -1;
    }
    
    public MotionEffect(final Context context, final AttributeSet set) {
        super(context, set);
        this.motionEffectAlpha = 0.1f;
        this.motionEffectStart = 49;
        this.motionEffectEnd = 50;
        this.motionEffectTranslationX = 0;
        this.motionEffectTranslationY = 0;
        this.motionEffectStrictMove = true;
        this.viewTransitionId = -1;
        this.fadeMove = -1;
        this.init(context, set);
    }
    
    public MotionEffect(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
        this.motionEffectAlpha = 0.1f;
        this.motionEffectStart = 49;
        this.motionEffectEnd = 50;
        this.motionEffectTranslationX = 0;
        this.motionEffectTranslationY = 0;
        this.motionEffectStrictMove = true;
        this.viewTransitionId = -1;
        this.fadeMove = -1;
        this.init(context, set);
    }
    
    private void init(final Context context, final AttributeSet set) {
        if (set != null) {
            final TypedArray obtainStyledAttributes = context.obtainStyledAttributes(set, R.styleable.MotionEffect);
            for (int indexCount = obtainStyledAttributes.getIndexCount(), i = 0; i < indexCount; ++i) {
                final int index = obtainStyledAttributes.getIndex(i);
                if (index == R.styleable.MotionEffect_motionEffect_start) {
                    final int int1 = obtainStyledAttributes.getInt(index, this.motionEffectStart);
                    this.motionEffectStart = int1;
                    this.motionEffectStart = Math.max(Math.min(int1, 99), 0);
                }
                else if (index == R.styleable.MotionEffect_motionEffect_end) {
                    final int int2 = obtainStyledAttributes.getInt(index, this.motionEffectEnd);
                    this.motionEffectEnd = int2;
                    this.motionEffectEnd = Math.max(Math.min(int2, 99), 0);
                }
                else if (index == R.styleable.MotionEffect_motionEffect_translationX) {
                    this.motionEffectTranslationX = obtainStyledAttributes.getDimensionPixelOffset(index, this.motionEffectTranslationX);
                }
                else if (index == R.styleable.MotionEffect_motionEffect_translationY) {
                    this.motionEffectTranslationY = obtainStyledAttributes.getDimensionPixelOffset(index, this.motionEffectTranslationY);
                }
                else if (index == R.styleable.MotionEffect_motionEffect_alpha) {
                    this.motionEffectAlpha = obtainStyledAttributes.getFloat(index, this.motionEffectAlpha);
                }
                else if (index == R.styleable.MotionEffect_motionEffect_move) {
                    this.fadeMove = obtainStyledAttributes.getInt(index, this.fadeMove);
                }
                else if (index == R.styleable.MotionEffect_motionEffect_strict) {
                    this.motionEffectStrictMove = obtainStyledAttributes.getBoolean(index, this.motionEffectStrictMove);
                }
                else if (index == R.styleable.MotionEffect_motionEffect_viewTransition) {
                    this.viewTransitionId = obtainStyledAttributes.getResourceId(index, this.viewTransitionId);
                }
            }
            final int motionEffectStart = this.motionEffectStart;
            final int motionEffectEnd = this.motionEffectEnd;
            if (motionEffectStart == motionEffectEnd) {
                if (motionEffectStart > 0) {
                    this.motionEffectStart = motionEffectStart - 1;
                }
                else {
                    this.motionEffectEnd = motionEffectEnd + 1;
                }
            }
            obtainStyledAttributes.recycle();
        }
    }
    
    @Override
    public boolean isDecorator() {
        return true;
    }
    
    @Override
    public void onPreSetup(final MotionLayout motionLayout, final HashMap<View, MotionController> hashMap) {
        final View[] views = this.getViews((ConstraintLayout)this.getParent());
        if (views == null) {
            final StringBuilder sb = new StringBuilder();
            sb.append(Debug.getLoc());
            sb.append(" views = null");
            Log.v("FadeMove", sb.toString());
            return;
        }
        final KeyAttributes keyAttributes = new KeyAttributes();
        final KeyAttributes keyAttributes2 = new KeyAttributes();
        keyAttributes.setValue("alpha", this.motionEffectAlpha);
        keyAttributes2.setValue("alpha", this.motionEffectAlpha);
        keyAttributes.setFramePosition(this.motionEffectStart);
        keyAttributes2.setFramePosition(this.motionEffectEnd);
        final KeyPosition keyPosition = new KeyPosition();
        keyPosition.setFramePosition(this.motionEffectStart);
        keyPosition.setType(0);
        keyPosition.setValue("percentX", 0);
        keyPosition.setValue("percentY", 0);
        final KeyPosition keyPosition2 = new KeyPosition();
        keyPosition2.setFramePosition(this.motionEffectEnd);
        keyPosition2.setType(0);
        keyPosition2.setValue("percentX", 1);
        keyPosition2.setValue("percentY", 1);
        final int motionEffectTranslationX = this.motionEffectTranslationX;
        KeyAttributes keyAttributes3 = null;
        KeyAttributes keyAttributes4;
        KeyAttributes keyAttributes5;
        if (motionEffectTranslationX > 0) {
            keyAttributes4 = new KeyAttributes();
            keyAttributes5 = new KeyAttributes();
            keyAttributes4.setValue("translationX", this.motionEffectTranslationX);
            keyAttributes4.setFramePosition(this.motionEffectEnd);
            keyAttributes5.setValue("translationX", 0);
            keyAttributes5.setFramePosition(this.motionEffectEnd - 1);
        }
        else {
            keyAttributes4 = null;
            keyAttributes5 = null;
        }
        KeyAttributes keyAttributes6;
        if (this.motionEffectTranslationY > 0) {
            keyAttributes3 = new KeyAttributes();
            keyAttributes6 = new KeyAttributes();
            keyAttributes3.setValue("translationY", this.motionEffectTranslationY);
            keyAttributes3.setFramePosition(this.motionEffectEnd);
            keyAttributes6.setValue("translationY", 0);
            keyAttributes6.setFramePosition(this.motionEffectEnd - 1);
        }
        else {
            keyAttributes6 = null;
        }
        int fadeMove;
        if ((fadeMove = this.fadeMove) == -1) {
            final int[] array = new int[4];
            for (int i = 0; i < views.length; ++i) {
                final MotionController motionController = hashMap.get(views[i]);
                if (motionController != null) {
                    final float n = motionController.getFinalX() - motionController.getStartX();
                    final float n2 = motionController.getFinalY() - motionController.getStartY();
                    if (n2 < 0.0f) {
                        ++array[1];
                    }
                    if (n2 > 0.0f) {
                        ++array[0];
                    }
                    if (n > 0.0f) {
                        ++array[3];
                    }
                    if (n < 0.0f) {
                        ++array[2];
                    }
                }
            }
            int n3 = array[0];
            int n4 = 1;
            int n5 = 0;
            while (true) {
                fadeMove = n5;
                if (n4 >= 4) {
                    break;
                }
                final int n6 = array[n4];
                int n7;
                if ((n7 = n3) < n6) {
                    n5 = n4;
                    n7 = n6;
                }
                ++n4;
                n3 = n7;
            }
        }
        for (int j = 0; j < views.length; ++j) {
            final MotionController motionController2 = hashMap.get(views[j]);
            if (motionController2 != null) {
                final float n8 = motionController2.getFinalX() - motionController2.getStartX();
                final float n9 = motionController2.getFinalY() - motionController2.getStartY();
                boolean b = false;
                Label_0761: {
                    Label_0668: {
                        if (fadeMove == 0) {
                            if (n9 <= 0.0f || (this.motionEffectStrictMove && n8 != 0.0f)) {
                                break Label_0668;
                            }
                        }
                        else if (fadeMove == 1) {
                            if (n9 >= 0.0f) {
                                break Label_0668;
                            }
                            if (this.motionEffectStrictMove) {
                                if (n8 != 0.0f) {
                                    break Label_0668;
                                }
                            }
                        }
                        else if (fadeMove == 2) {
                            if (n8 >= 0.0f) {
                                break Label_0668;
                            }
                            if (this.motionEffectStrictMove) {
                                if (n9 != 0.0f) {
                                    break Label_0668;
                                }
                            }
                        }
                        else {
                            if (fadeMove != 3 || n8 <= 0.0f) {
                                break Label_0668;
                            }
                            if (this.motionEffectStrictMove) {
                                if (n9 != 0.0f) {
                                    break Label_0668;
                                }
                            }
                        }
                        b = false;
                        break Label_0761;
                    }
                    b = true;
                }
                if (b) {
                    final int viewTransitionId = this.viewTransitionId;
                    if (viewTransitionId == -1) {
                        motionController2.addKey(keyAttributes);
                        motionController2.addKey(keyAttributes2);
                        motionController2.addKey(keyPosition);
                        motionController2.addKey(keyPosition2);
                        if (this.motionEffectTranslationX > 0) {
                            motionController2.addKey(keyAttributes4);
                            motionController2.addKey(keyAttributes5);
                        }
                        if (this.motionEffectTranslationY > 0) {
                            motionController2.addKey(keyAttributes3);
                            motionController2.addKey(keyAttributes6);
                        }
                    }
                    else {
                        motionLayout.applyViewTransition(viewTransitionId, motionController2);
                    }
                }
            }
        }
    }
}

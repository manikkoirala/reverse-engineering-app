// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.core;

import java.io.PrintStream;
import java.util.Arrays;

public class ArrayLinkedVariables implements ArrayRowVariables
{
    private static final boolean DEBUG = false;
    private static final boolean FULL_NEW_CHECK = false;
    static final int NONE = -1;
    private static float epsilon = 0.001f;
    private int ROW_SIZE;
    private SolverVariable candidate;
    int currentSize;
    private int[] mArrayIndices;
    private int[] mArrayNextIndices;
    private float[] mArrayValues;
    protected final Cache mCache;
    private boolean mDidFillOnce;
    private int mHead;
    private int mLast;
    private final ArrayRow mRow;
    
    ArrayLinkedVariables(final ArrayRow mRow, final Cache mCache) {
        this.currentSize = 0;
        this.ROW_SIZE = 8;
        this.candidate = null;
        this.mArrayIndices = new int[8];
        this.mArrayNextIndices = new int[8];
        this.mArrayValues = new float[8];
        this.mHead = -1;
        this.mLast = -1;
        this.mDidFillOnce = false;
        this.mRow = mRow;
        this.mCache = mCache;
    }
    
    @Override
    public void add(final SolverVariable solverVariable, float n, final boolean b) {
        final float epsilon = ArrayLinkedVariables.epsilon;
        if (n > -epsilon && n < epsilon) {
            return;
        }
        int mHead = this.mHead;
        if (mHead == -1) {
            this.mHead = 0;
            this.mArrayValues[0] = n;
            this.mArrayIndices[0] = solverVariable.id;
            this.mArrayNextIndices[this.mHead] = -1;
            ++solverVariable.usageInRowCount;
            solverVariable.addToRow(this.mRow);
            ++this.currentSize;
            if (!this.mDidFillOnce) {
                final int mLast = this.mLast + 1;
                this.mLast = mLast;
                final int[] mArrayIndices = this.mArrayIndices;
                if (mLast >= mArrayIndices.length) {
                    this.mDidFillOnce = true;
                    this.mLast = mArrayIndices.length - 1;
                }
            }
            return;
        }
        int n2 = 0;
        int n3 = -1;
        while (mHead != -1 && n2 < this.currentSize) {
            if (this.mArrayIndices[mHead] == solverVariable.id) {
                final float[] mArrayValues = this.mArrayValues;
                final float n4 = mArrayValues[mHead] + n;
                final float epsilon2 = ArrayLinkedVariables.epsilon;
                n = n4;
                if (n4 > -epsilon2) {
                    n = n4;
                    if (n4 < epsilon2) {
                        n = 0.0f;
                    }
                }
                mArrayValues[mHead] = n;
                if (n == 0.0f) {
                    if (mHead == this.mHead) {
                        this.mHead = this.mArrayNextIndices[mHead];
                    }
                    else {
                        final int[] mArrayNextIndices = this.mArrayNextIndices;
                        mArrayNextIndices[n3] = mArrayNextIndices[mHead];
                    }
                    if (b) {
                        solverVariable.removeFromRow(this.mRow);
                    }
                    if (this.mDidFillOnce) {
                        this.mLast = mHead;
                    }
                    --solverVariable.usageInRowCount;
                    --this.currentSize;
                }
                return;
            }
            if (this.mArrayIndices[mHead] < solverVariable.id) {
                n3 = mHead;
            }
            mHead = this.mArrayNextIndices[mHead];
            ++n2;
        }
        int n5 = this.mLast;
        if (this.mDidFillOnce) {
            final int[] mArrayIndices2 = this.mArrayIndices;
            if (mArrayIndices2[n5] != -1) {
                n5 = mArrayIndices2.length;
            }
        }
        else {
            ++n5;
        }
        final int[] mArrayIndices3 = this.mArrayIndices;
        int n6 = n5;
        if (n5 >= mArrayIndices3.length) {
            n6 = n5;
            if (this.currentSize < mArrayIndices3.length) {
                int n7 = 0;
                while (true) {
                    final int[] mArrayIndices4 = this.mArrayIndices;
                    n6 = n5;
                    if (n7 >= mArrayIndices4.length) {
                        break;
                    }
                    if (mArrayIndices4[n7] == -1) {
                        n6 = n7;
                        break;
                    }
                    ++n7;
                }
            }
        }
        final int[] mArrayIndices5 = this.mArrayIndices;
        int length;
        if ((length = n6) >= mArrayIndices5.length) {
            length = mArrayIndices5.length;
            final int n8 = this.ROW_SIZE * 2;
            this.ROW_SIZE = n8;
            this.mDidFillOnce = false;
            this.mLast = length - 1;
            this.mArrayValues = Arrays.copyOf(this.mArrayValues, n8);
            this.mArrayIndices = Arrays.copyOf(this.mArrayIndices, this.ROW_SIZE);
            this.mArrayNextIndices = Arrays.copyOf(this.mArrayNextIndices, this.ROW_SIZE);
        }
        this.mArrayIndices[length] = solverVariable.id;
        this.mArrayValues[length] = n;
        if (n3 != -1) {
            final int[] mArrayNextIndices2 = this.mArrayNextIndices;
            mArrayNextIndices2[length] = mArrayNextIndices2[n3];
            mArrayNextIndices2[n3] = length;
        }
        else {
            this.mArrayNextIndices[length] = this.mHead;
            this.mHead = length;
        }
        ++solverVariable.usageInRowCount;
        solverVariable.addToRow(this.mRow);
        ++this.currentSize;
        if (!this.mDidFillOnce) {
            ++this.mLast;
        }
        final int mLast2 = this.mLast;
        final int[] mArrayIndices6 = this.mArrayIndices;
        if (mLast2 >= mArrayIndices6.length) {
            this.mDidFillOnce = true;
            this.mLast = mArrayIndices6.length - 1;
        }
    }
    
    @Override
    public final void clear() {
        for (int mHead = this.mHead, n = 0; mHead != -1 && n < this.currentSize; mHead = this.mArrayNextIndices[mHead], ++n) {
            final SolverVariable solverVariable = this.mCache.mIndexedVariables[this.mArrayIndices[mHead]];
            if (solverVariable != null) {
                solverVariable.removeFromRow(this.mRow);
            }
        }
        this.mHead = -1;
        this.mLast = -1;
        this.mDidFillOnce = false;
        this.currentSize = 0;
    }
    
    @Override
    public boolean contains(final SolverVariable solverVariable) {
        int mHead = this.mHead;
        if (mHead == -1) {
            return false;
        }
        for (int n = 0; mHead != -1 && n < this.currentSize; mHead = this.mArrayNextIndices[mHead], ++n) {
            if (this.mArrayIndices[mHead] == solverVariable.id) {
                return true;
            }
        }
        return false;
    }
    
    @Override
    public void display() {
        final int currentSize = this.currentSize;
        System.out.print("{ ");
        for (int i = 0; i < currentSize; ++i) {
            final SolverVariable variable = this.getVariable(i);
            if (variable != null) {
                final PrintStream out = System.out;
                final StringBuilder sb = new StringBuilder();
                sb.append(variable);
                sb.append(" = ");
                sb.append(this.getVariableValue(i));
                sb.append(" ");
                out.print(sb.toString());
            }
        }
        System.out.println(" }");
    }
    
    @Override
    public void divideByAmount(final float n) {
        for (int mHead = this.mHead, n2 = 0; mHead != -1 && n2 < this.currentSize; mHead = this.mArrayNextIndices[mHead], ++n2) {
            final float[] mArrayValues = this.mArrayValues;
            mArrayValues[mHead] /= n;
        }
    }
    
    @Override
    public final float get(final SolverVariable solverVariable) {
        for (int mHead = this.mHead, n = 0; mHead != -1 && n < this.currentSize; mHead = this.mArrayNextIndices[mHead], ++n) {
            if (this.mArrayIndices[mHead] == solverVariable.id) {
                return this.mArrayValues[mHead];
            }
        }
        return 0.0f;
    }
    
    @Override
    public int getCurrentSize() {
        return this.currentSize;
    }
    
    public int getHead() {
        return this.mHead;
    }
    
    public final int getId(final int n) {
        return this.mArrayIndices[n];
    }
    
    public final int getNextIndice(final int n) {
        return this.mArrayNextIndices[n];
    }
    
    SolverVariable getPivotCandidate() {
        final SolverVariable candidate = this.candidate;
        if (candidate == null) {
            int mHead = this.mHead;
            int n = 0;
            SolverVariable solverVariable = null;
            while (mHead != -1 && n < this.currentSize) {
                SolverVariable solverVariable2 = solverVariable;
                Label_0084: {
                    if (this.mArrayValues[mHead] < 0.0f) {
                        final SolverVariable solverVariable3 = this.mCache.mIndexedVariables[this.mArrayIndices[mHead]];
                        if (solverVariable != null) {
                            solverVariable2 = solverVariable;
                            if (solverVariable.strength >= solverVariable3.strength) {
                                break Label_0084;
                            }
                        }
                        solverVariable2 = solverVariable3;
                    }
                }
                mHead = this.mArrayNextIndices[mHead];
                ++n;
                solverVariable = solverVariable2;
            }
            return solverVariable;
        }
        return candidate;
    }
    
    public final float getValue(final int n) {
        return this.mArrayValues[n];
    }
    
    @Override
    public SolverVariable getVariable(final int n) {
        for (int mHead = this.mHead, n2 = 0; mHead != -1 && n2 < this.currentSize; mHead = this.mArrayNextIndices[mHead], ++n2) {
            if (n2 == n) {
                return this.mCache.mIndexedVariables[this.mArrayIndices[mHead]];
            }
        }
        return null;
    }
    
    @Override
    public float getVariableValue(final int n) {
        for (int mHead = this.mHead, n2 = 0; mHead != -1 && n2 < this.currentSize; mHead = this.mArrayNextIndices[mHead], ++n2) {
            if (n2 == n) {
                return this.mArrayValues[mHead];
            }
        }
        return 0.0f;
    }
    
    boolean hasAtLeastOnePositiveVariable() {
        for (int mHead = this.mHead, n = 0; mHead != -1 && n < this.currentSize; mHead = this.mArrayNextIndices[mHead], ++n) {
            if (this.mArrayValues[mHead] > 0.0f) {
                return true;
            }
        }
        return false;
    }
    
    @Override
    public int indexOf(final SolverVariable solverVariable) {
        int mHead = this.mHead;
        if (mHead == -1) {
            return -1;
        }
        for (int n = 0; mHead != -1 && n < this.currentSize; mHead = this.mArrayNextIndices[mHead], ++n) {
            if (this.mArrayIndices[mHead] == solverVariable.id) {
                return mHead;
            }
        }
        return -1;
    }
    
    @Override
    public void invert() {
        for (int mHead = this.mHead, n = 0; mHead != -1 && n < this.currentSize; mHead = this.mArrayNextIndices[mHead], ++n) {
            final float[] mArrayValues = this.mArrayValues;
            mArrayValues[mHead] *= -1.0f;
        }
    }
    
    @Override
    public final void put(final SolverVariable solverVariable, final float n) {
        if (n == 0.0f) {
            this.remove(solverVariable, true);
            return;
        }
        int mHead = this.mHead;
        if (mHead == -1) {
            this.mHead = 0;
            this.mArrayValues[0] = n;
            this.mArrayIndices[0] = solverVariable.id;
            this.mArrayNextIndices[this.mHead] = -1;
            ++solverVariable.usageInRowCount;
            solverVariable.addToRow(this.mRow);
            ++this.currentSize;
            if (!this.mDidFillOnce) {
                final int mLast = this.mLast + 1;
                this.mLast = mLast;
                final int[] mArrayIndices = this.mArrayIndices;
                if (mLast >= mArrayIndices.length) {
                    this.mDidFillOnce = true;
                    this.mLast = mArrayIndices.length - 1;
                }
            }
            return;
        }
        int n2 = 0;
        int n3 = -1;
        while (mHead != -1 && n2 < this.currentSize) {
            if (this.mArrayIndices[mHead] == solverVariable.id) {
                this.mArrayValues[mHead] = n;
                return;
            }
            if (this.mArrayIndices[mHead] < solverVariable.id) {
                n3 = mHead;
            }
            mHead = this.mArrayNextIndices[mHead];
            ++n2;
        }
        int n4 = this.mLast;
        if (this.mDidFillOnce) {
            final int[] mArrayIndices2 = this.mArrayIndices;
            if (mArrayIndices2[n4] != -1) {
                n4 = mArrayIndices2.length;
            }
        }
        else {
            ++n4;
        }
        final int[] mArrayIndices3 = this.mArrayIndices;
        int n5 = n4;
        if (n4 >= mArrayIndices3.length) {
            n5 = n4;
            if (this.currentSize < mArrayIndices3.length) {
                int n6 = 0;
                while (true) {
                    final int[] mArrayIndices4 = this.mArrayIndices;
                    n5 = n4;
                    if (n6 >= mArrayIndices4.length) {
                        break;
                    }
                    if (mArrayIndices4[n6] == -1) {
                        n5 = n6;
                        break;
                    }
                    ++n6;
                }
            }
        }
        final int[] mArrayIndices5 = this.mArrayIndices;
        int length;
        if ((length = n5) >= mArrayIndices5.length) {
            length = mArrayIndices5.length;
            final int n7 = this.ROW_SIZE * 2;
            this.ROW_SIZE = n7;
            this.mDidFillOnce = false;
            this.mLast = length - 1;
            this.mArrayValues = Arrays.copyOf(this.mArrayValues, n7);
            this.mArrayIndices = Arrays.copyOf(this.mArrayIndices, this.ROW_SIZE);
            this.mArrayNextIndices = Arrays.copyOf(this.mArrayNextIndices, this.ROW_SIZE);
        }
        this.mArrayIndices[length] = solverVariable.id;
        this.mArrayValues[length] = n;
        if (n3 != -1) {
            final int[] mArrayNextIndices = this.mArrayNextIndices;
            mArrayNextIndices[length] = mArrayNextIndices[n3];
            mArrayNextIndices[n3] = length;
        }
        else {
            this.mArrayNextIndices[length] = this.mHead;
            this.mHead = length;
        }
        ++solverVariable.usageInRowCount;
        solverVariable.addToRow(this.mRow);
        final int currentSize = this.currentSize + 1;
        this.currentSize = currentSize;
        if (!this.mDidFillOnce) {
            ++this.mLast;
        }
        final int[] mArrayIndices6 = this.mArrayIndices;
        if (currentSize >= mArrayIndices6.length) {
            this.mDidFillOnce = true;
        }
        if (this.mLast >= mArrayIndices6.length) {
            this.mDidFillOnce = true;
            this.mLast = mArrayIndices6.length - 1;
        }
    }
    
    @Override
    public final float remove(final SolverVariable solverVariable, final boolean b) {
        if (this.candidate == solverVariable) {
            this.candidate = null;
        }
        int mHead = this.mHead;
        if (mHead == -1) {
            return 0.0f;
        }
        int n = 0;
        int n2 = -1;
        while (mHead != -1 && n < this.currentSize) {
            if (this.mArrayIndices[mHead] == solverVariable.id) {
                if (mHead == this.mHead) {
                    this.mHead = this.mArrayNextIndices[mHead];
                }
                else {
                    final int[] mArrayNextIndices = this.mArrayNextIndices;
                    mArrayNextIndices[n2] = mArrayNextIndices[mHead];
                }
                if (b) {
                    solverVariable.removeFromRow(this.mRow);
                }
                --solverVariable.usageInRowCount;
                --this.currentSize;
                this.mArrayIndices[mHead] = -1;
                if (this.mDidFillOnce) {
                    this.mLast = mHead;
                }
                return this.mArrayValues[mHead];
            }
            final int n3 = this.mArrayNextIndices[mHead];
            ++n;
            n2 = mHead;
            mHead = n3;
        }
        return 0.0f;
    }
    
    @Override
    public int sizeInBytes() {
        return this.mArrayIndices.length * 4 * 3 + 0 + 36;
    }
    
    @Override
    public String toString() {
        int mHead = this.mHead;
        String string = "";
        for (int n = 0; mHead != -1 && n < this.currentSize; mHead = this.mArrayNextIndices[mHead], ++n) {
            final StringBuilder sb = new StringBuilder();
            sb.append(string);
            sb.append(" -> ");
            final String string2 = sb.toString();
            final StringBuilder sb2 = new StringBuilder();
            sb2.append(string2);
            sb2.append(this.mArrayValues[mHead]);
            sb2.append(" : ");
            final String string3 = sb2.toString();
            final StringBuilder sb3 = new StringBuilder();
            sb3.append(string3);
            sb3.append(this.mCache.mIndexedVariables[this.mArrayIndices[mHead]]);
            string = sb3.toString();
        }
        return string;
    }
    
    @Override
    public float use(final ArrayRow arrayRow, final boolean b) {
        final float value = this.get(arrayRow.variable);
        this.remove(arrayRow.variable, b);
        final ArrayRow.ArrayRowVariables variables = arrayRow.variables;
        for (int currentSize = variables.getCurrentSize(), i = 0; i < currentSize; ++i) {
            final SolverVariable variable = variables.getVariable(i);
            this.add(variable, variables.get(variable) * value, b);
        }
        return value;
    }
}

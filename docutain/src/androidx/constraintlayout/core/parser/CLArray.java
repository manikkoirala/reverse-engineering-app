// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.core.parser;

import java.util.Iterator;

public class CLArray extends CLContainer
{
    public CLArray(final char[] array) {
        super(array);
    }
    
    public static CLElement allocate(final char[] array) {
        return new CLArray(array);
    }
    
    @Override
    protected String toFormattedJSON(final int n, final int n2) {
        final StringBuilder sb = new StringBuilder();
        final String json = this.toJSON();
        if (n2 <= 0 && json.length() + n < CLArray.MAX_LINE) {
            sb.append(json);
        }
        else {
            sb.append("[\n");
            final Iterator<CLElement> iterator = this.mElements.iterator();
            int n3 = 1;
            while (iterator.hasNext()) {
                final CLElement clElement = iterator.next();
                if (n3 == 0) {
                    sb.append(",\n");
                }
                else {
                    n3 = 0;
                }
                this.addIndent(sb, CLArray.BASE_INDENT + n);
                sb.append(clElement.toFormattedJSON(CLArray.BASE_INDENT + n, n2 - 1));
            }
            sb.append("\n");
            this.addIndent(sb, n);
            sb.append("]");
        }
        return sb.toString();
    }
    
    @Override
    protected String toJSON() {
        final StringBuilder sb = new StringBuilder();
        sb.append(this.getDebugName());
        sb.append("[");
        final StringBuilder obj = new StringBuilder(sb.toString());
        int n = 1;
        for (int i = 0; i < this.mElements.size(); ++i) {
            if (n == 0) {
                obj.append(", ");
            }
            else {
                n = 0;
            }
            obj.append(this.mElements.get(i).toJSON());
        }
        final StringBuilder sb2 = new StringBuilder();
        sb2.append((Object)obj);
        sb2.append("]");
        return sb2.toString();
    }
}

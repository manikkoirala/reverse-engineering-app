// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.core.parser;

public class CLToken extends CLElement
{
    int index;
    char[] tokenFalse;
    char[] tokenNull;
    char[] tokenTrue;
    Type type;
    
    public CLToken(final char[] array) {
        super(array);
        this.index = 0;
        this.type = Type.UNKNOWN;
        this.tokenTrue = "true".toCharArray();
        this.tokenFalse = "false".toCharArray();
        this.tokenNull = "null".toCharArray();
    }
    
    public static CLElement allocate(final char[] array) {
        return new CLToken(array);
    }
    
    public boolean getBoolean() throws CLParsingException {
        if (this.type == Type.TRUE) {
            return true;
        }
        if (this.type == Type.FALSE) {
            return false;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("this token is not a boolean: <");
        sb.append(this.content());
        sb.append(">");
        throw new CLParsingException(sb.toString(), this);
    }
    
    public Type getType() {
        return this.type;
    }
    
    public boolean isNull() throws CLParsingException {
        if (this.type == Type.NULL) {
            return true;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("this token is not a null: <");
        sb.append(this.content());
        sb.append(">");
        throw new CLParsingException(sb.toString(), this);
    }
    
    @Override
    protected String toFormattedJSON(final int n, final int n2) {
        final StringBuilder sb = new StringBuilder();
        this.addIndent(sb, n);
        sb.append(this.content());
        return sb.toString();
    }
    
    @Override
    protected String toJSON() {
        if (CLParser.DEBUG) {
            final StringBuilder sb = new StringBuilder();
            sb.append("<");
            sb.append(this.content());
            sb.append(">");
            return sb.toString();
        }
        return this.content();
    }
    
    public boolean validate(final char c, final long end) {
        final int n = CLToken$1.$SwitchMap$androidx$constraintlayout$core$parser$CLToken$Type[this.type.ordinal()];
        final int n2 = 0;
        int n3 = 0;
        final boolean b = false;
        final int n4 = 0;
        int n5 = 0;
        Label_0318: {
            if (n != 1) {
                if (n != 2) {
                    if (n != 3) {
                        if (n != 4) {
                            n5 = (b ? 1 : 0);
                        }
                        else {
                            final char[] tokenTrue = this.tokenTrue;
                            final int index = this.index;
                            if (tokenTrue[index] == c) {
                                this.type = Type.TRUE;
                            }
                            else if (this.tokenFalse[index] == c) {
                                this.type = Type.FALSE;
                            }
                            else {
                                n5 = (b ? 1 : 0);
                                if (this.tokenNull[index] != c) {
                                    break Label_0318;
                                }
                                this.type = Type.NULL;
                            }
                            n5 = 1;
                        }
                    }
                    else {
                        final char[] tokenNull = this.tokenNull;
                        final int index2 = this.index;
                        int n6 = n4;
                        if (tokenNull[index2] == c) {
                            n6 = 1;
                        }
                        n5 = n6;
                        if (n6 != 0) {
                            n5 = n6;
                            if (index2 + 1 == tokenNull.length) {
                                this.setEnd(end);
                                n5 = n6;
                            }
                        }
                    }
                }
                else {
                    final char[] tokenFalse = this.tokenFalse;
                    final int index3 = this.index;
                    int n7 = n2;
                    if (tokenFalse[index3] == c) {
                        n7 = 1;
                    }
                    n5 = n7;
                    if (n7 != 0) {
                        n5 = n7;
                        if (index3 + 1 == tokenFalse.length) {
                            this.setEnd(end);
                            n5 = n7;
                        }
                    }
                }
            }
            else {
                final char[] tokenTrue2 = this.tokenTrue;
                final int index4 = this.index;
                if (tokenTrue2[index4] == c) {
                    n3 = 1;
                }
                n5 = n3;
                if (n3 != 0) {
                    n5 = n3;
                    if (index4 + 1 == tokenTrue2.length) {
                        this.setEnd(end);
                        n5 = n3;
                    }
                }
            }
        }
        ++this.index;
        return n5 != 0;
    }
    
    enum Type
    {
        private static final Type[] $VALUES;
        
        FALSE, 
        NULL, 
        TRUE, 
        UNKNOWN;
    }
}

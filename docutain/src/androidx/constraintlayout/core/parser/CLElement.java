// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.core.parser;

import java.io.PrintStream;

public class CLElement
{
    protected static int BASE_INDENT = 2;
    protected static int MAX_LINE = 80;
    protected long end;
    private int line;
    protected CLContainer mContainer;
    private final char[] mContent;
    protected long start;
    
    public CLElement(final char[] mContent) {
        this.start = -1L;
        this.end = Long.MAX_VALUE;
        this.mContent = mContent;
    }
    
    protected void addIndent(final StringBuilder sb, final int n) {
        for (int i = 0; i < n; ++i) {
            sb.append(' ');
        }
    }
    
    public String content() {
        final String s = new String(this.mContent);
        final long end = this.end;
        if (end != Long.MAX_VALUE) {
            final long start = this.start;
            if (end >= start) {
                return s.substring((int)start, (int)end + 1);
            }
        }
        final long start2 = this.start;
        return s.substring((int)start2, (int)start2 + 1);
    }
    
    public CLElement getContainer() {
        return this.mContainer;
    }
    
    protected String getDebugName() {
        if (CLParser.DEBUG) {
            final StringBuilder sb = new StringBuilder();
            sb.append(this.getStrClass());
            sb.append(" -> ");
            return sb.toString();
        }
        return "";
    }
    
    public long getEnd() {
        return this.end;
    }
    
    public float getFloat() {
        if (this instanceof CLNumber) {
            return ((CLNumber)this).getFloat();
        }
        return Float.NaN;
    }
    
    public int getInt() {
        if (this instanceof CLNumber) {
            return ((CLNumber)this).getInt();
        }
        return 0;
    }
    
    public int getLine() {
        return this.line;
    }
    
    public long getStart() {
        return this.start;
    }
    
    protected String getStrClass() {
        final String string = this.getClass().toString();
        return string.substring(string.lastIndexOf(46) + 1);
    }
    
    public boolean isDone() {
        return this.end != Long.MAX_VALUE;
    }
    
    public boolean isStarted() {
        return this.start > -1L;
    }
    
    public boolean notStarted() {
        return this.start == -1L;
    }
    
    public void setContainer(final CLContainer mContainer) {
        this.mContainer = mContainer;
    }
    
    public void setEnd(final long end) {
        if (this.end != Long.MAX_VALUE) {
            return;
        }
        this.end = end;
        if (CLParser.DEBUG) {
            final PrintStream out = System.out;
            final StringBuilder sb = new StringBuilder();
            sb.append("closing ");
            sb.append(this.hashCode());
            sb.append(" -> ");
            sb.append(this);
            out.println(sb.toString());
        }
        final CLContainer mContainer = this.mContainer;
        if (mContainer != null) {
            mContainer.add(this);
        }
    }
    
    public void setLine(final int line) {
        this.line = line;
    }
    
    public void setStart(final long start) {
        this.start = start;
    }
    
    protected String toFormattedJSON(final int n, final int n2) {
        return "";
    }
    
    protected String toJSON() {
        return "";
    }
    
    @Override
    public String toString() {
        final long start = this.start;
        final long end = this.end;
        if (start <= end && end != Long.MAX_VALUE) {
            final String substring = new String(this.mContent).substring((int)this.start, (int)this.end + 1);
            final StringBuilder sb = new StringBuilder();
            sb.append(this.getStrClass());
            sb.append(" (");
            sb.append(this.start);
            sb.append(" : ");
            sb.append(this.end);
            sb.append(") <<");
            sb.append(substring);
            sb.append(">>");
            return sb.toString();
        }
        final StringBuilder sb2 = new StringBuilder();
        sb2.append(this.getClass());
        sb2.append(" (INVALID, ");
        sb2.append(this.start);
        sb2.append("-");
        sb2.append(this.end);
        sb2.append(")");
        return sb2.toString();
    }
}

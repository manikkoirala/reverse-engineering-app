// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.core.parser;

public class CLString extends CLElement
{
    public CLString(final char[] array) {
        super(array);
    }
    
    public static CLElement allocate(final char[] array) {
        return new CLString(array);
    }
    
    @Override
    protected String toFormattedJSON(final int n, final int n2) {
        final StringBuilder sb = new StringBuilder();
        this.addIndent(sb, n);
        sb.append("'");
        sb.append(this.content());
        sb.append("'");
        return sb.toString();
    }
    
    @Override
    protected String toJSON() {
        final StringBuilder sb = new StringBuilder();
        sb.append("'");
        sb.append(this.content());
        sb.append("'");
        return sb.toString();
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.core.parser;

import java.util.ArrayList;

public class CLKey extends CLContainer
{
    private static ArrayList<String> sections;
    
    static {
        (CLKey.sections = new ArrayList<String>()).add("ConstraintSets");
        CLKey.sections.add("Variables");
        CLKey.sections.add("Generate");
        CLKey.sections.add("Transitions");
        CLKey.sections.add("KeyFrames");
        CLKey.sections.add("KeyAttributes");
        CLKey.sections.add("KeyPositions");
        CLKey.sections.add("KeyCycles");
    }
    
    public CLKey(final char[] array) {
        super(array);
    }
    
    public static CLElement allocate(final String s, final CLElement clElement) {
        final CLKey clKey = new CLKey(s.toCharArray());
        clKey.setStart(0L);
        clKey.setEnd(s.length() - 1);
        clKey.set(clElement);
        return clKey;
    }
    
    public static CLElement allocate(final char[] array) {
        return new CLKey(array);
    }
    
    public String getName() {
        return this.content();
    }
    
    public CLElement getValue() {
        if (this.mElements.size() > 0) {
            return this.mElements.get(0);
        }
        return null;
    }
    
    public void set(final CLElement clElement) {
        if (this.mElements.size() > 0) {
            this.mElements.set(0, clElement);
        }
        else {
            this.mElements.add(clElement);
        }
    }
    
    @Override
    protected String toFormattedJSON(final int n, int n2) {
        final StringBuilder sb = new StringBuilder(this.getDebugName());
        this.addIndent(sb, n);
        final String content = this.content();
        if (this.mElements.size() > 0) {
            sb.append(content);
            sb.append(": ");
            if (CLKey.sections.contains(content)) {
                n2 = 3;
            }
            if (n2 > 0) {
                sb.append(this.mElements.get(0).toFormattedJSON(n, n2 - 1));
            }
            else {
                final String json = this.mElements.get(0).toJSON();
                if (json.length() + n < CLKey.MAX_LINE) {
                    sb.append(json);
                }
                else {
                    sb.append(this.mElements.get(0).toFormattedJSON(n, n2 - 1));
                }
            }
            return sb.toString();
        }
        final StringBuilder sb2 = new StringBuilder();
        sb2.append(content);
        sb2.append(": <> ");
        return sb2.toString();
    }
    
    @Override
    protected String toJSON() {
        if (this.mElements.size() > 0) {
            final StringBuilder sb = new StringBuilder();
            sb.append(this.getDebugName());
            sb.append(this.content());
            sb.append(": ");
            sb.append(this.mElements.get(0).toJSON());
            return sb.toString();
        }
        final StringBuilder sb2 = new StringBuilder();
        sb2.append(this.getDebugName());
        sb2.append(this.content());
        sb2.append(": <> ");
        return sb2.toString();
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.core.parser;

import java.util.Iterator;

public class CLObject extends CLContainer implements Iterable<CLKey>
{
    public CLObject(final char[] array) {
        super(array);
    }
    
    public static CLObject allocate(final char[] array) {
        return new CLObject(array);
    }
    
    @Override
    public Iterator iterator() {
        return new CLObjectIterator(this);
    }
    
    public String toFormattedJSON() {
        return this.toFormattedJSON(0, 0);
    }
    
    public String toFormattedJSON(final int n, final int n2) {
        final StringBuilder sb = new StringBuilder(this.getDebugName());
        sb.append("{\n");
        final Iterator<CLElement> iterator = this.mElements.iterator();
        int n3 = 1;
        while (iterator.hasNext()) {
            final CLElement clElement = iterator.next();
            if (n3 == 0) {
                sb.append(",\n");
            }
            else {
                n3 = 0;
            }
            sb.append(clElement.toFormattedJSON(CLObject.BASE_INDENT + n, n2 - 1));
        }
        sb.append("\n");
        this.addIndent(sb, n);
        sb.append("}");
        return sb.toString();
    }
    
    public String toJSON() {
        final StringBuilder sb = new StringBuilder();
        sb.append(this.getDebugName());
        sb.append("{ ");
        final StringBuilder sb2 = new StringBuilder(sb.toString());
        final Iterator<CLElement> iterator = this.mElements.iterator();
        int n = 1;
        while (iterator.hasNext()) {
            final CLElement clElement = iterator.next();
            if (n == 0) {
                sb2.append(", ");
            }
            else {
                n = 0;
            }
            sb2.append(clElement.toJSON());
        }
        sb2.append(" }");
        return sb2.toString();
    }
    
    private class CLObjectIterator implements Iterator
    {
        int index;
        CLObject myObject;
        final CLObject this$0;
        
        public CLObjectIterator(final CLObject this$0, final CLObject myObject) {
            this.this$0 = this$0;
            this.index = 0;
            this.myObject = myObject;
        }
        
        @Override
        public boolean hasNext() {
            return this.index < this.myObject.size();
        }
        
        @Override
        public Object next() {
            final CLKey clKey = this.myObject.mElements.get(this.index);
            ++this.index;
            return clKey;
        }
    }
}

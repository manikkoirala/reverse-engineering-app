// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.core.motion;

import androidx.constraintlayout.core.motion.utils.Rect;
import java.util.HashSet;
import java.util.Iterator;
import androidx.constraintlayout.core.motion.utils.Utils;
import androidx.constraintlayout.core.motion.utils.SplineSet;
import java.util.HashMap;
import androidx.constraintlayout.core.motion.utils.Easing;
import java.util.LinkedHashMap;

class MotionConstrainedPoint implements Comparable<MotionConstrainedPoint>
{
    static final int CARTESIAN = 2;
    public static final boolean DEBUG = false;
    static final int PERPENDICULAR = 1;
    public static final String TAG = "MotionPaths";
    static String[] names;
    private float alpha;
    private boolean applyElevation;
    private float elevation;
    private float height;
    private int mAnimateRelativeTo;
    LinkedHashMap<String, CustomVariable> mCustomVariable;
    private int mDrawPath;
    private Easing mKeyFrameEasing;
    int mMode;
    private float mPathRotate;
    private float mPivotX;
    private float mPivotY;
    private float mProgress;
    double[] mTempDelta;
    double[] mTempValue;
    int mVisibilityMode;
    private float position;
    private float rotation;
    private float rotationX;
    public float rotationY;
    private float scaleX;
    private float scaleY;
    private float translationX;
    private float translationY;
    private float translationZ;
    int visibility;
    private float width;
    private float x;
    private float y;
    
    static {
        MotionConstrainedPoint.names = new String[] { "position", "x", "y", "width", "height", "pathRotate" };
    }
    
    public MotionConstrainedPoint() {
        this.alpha = 1.0f;
        this.mVisibilityMode = 0;
        this.applyElevation = false;
        this.elevation = 0.0f;
        this.rotation = 0.0f;
        this.rotationX = 0.0f;
        this.rotationY = 0.0f;
        this.scaleX = 1.0f;
        this.scaleY = 1.0f;
        this.mPivotX = Float.NaN;
        this.mPivotY = Float.NaN;
        this.translationX = 0.0f;
        this.translationY = 0.0f;
        this.translationZ = 0.0f;
        this.mDrawPath = 0;
        this.mPathRotate = Float.NaN;
        this.mProgress = Float.NaN;
        this.mAnimateRelativeTo = -1;
        this.mCustomVariable = new LinkedHashMap<String, CustomVariable>();
        this.mMode = 0;
        this.mTempValue = new double[18];
        this.mTempDelta = new double[18];
    }
    
    private boolean diff(final float n, final float n2) {
        final boolean naN = Float.isNaN(n);
        boolean b = true;
        final boolean b2 = true;
        if (!naN && !Float.isNaN(n2)) {
            return Math.abs(n - n2) > 1.0E-6f && b2;
        }
        if (Float.isNaN(n) == Float.isNaN(n2)) {
            b = false;
        }
        return b;
    }
    
    public void addValues(final HashMap<String, SplineSet> hashMap, final int i) {
        for (final String str : hashMap.keySet()) {
            final SplineSet obj = hashMap.get(str);
            str.hashCode();
            int n = -1;
            switch (str) {
                case "pathRotate": {
                    n = 12;
                    break;
                }
                case "alpha": {
                    n = 11;
                    break;
                }
                case "scaleY": {
                    n = 10;
                    break;
                }
                case "scaleX": {
                    n = 9;
                    break;
                }
                case "pivotY": {
                    n = 8;
                    break;
                }
                case "pivotX": {
                    n = 7;
                    break;
                }
                case "progress": {
                    n = 6;
                    break;
                }
                case "translationZ": {
                    n = 5;
                    break;
                }
                case "translationY": {
                    n = 4;
                    break;
                }
                case "translationX": {
                    n = 3;
                    break;
                }
                case "rotationZ": {
                    n = 2;
                    break;
                }
                case "rotationY": {
                    n = 1;
                    break;
                }
                case "rotationX": {
                    n = 0;
                    break;
                }
                default:
                    break;
            }
            float n2 = 1.0f;
            final float n3 = 0.0f;
            final float n4 = 0.0f;
            final float n5 = 0.0f;
            final float n6 = 0.0f;
            final float n7 = 0.0f;
            final float n8 = 0.0f;
            final float n9 = 0.0f;
            final float n10 = 0.0f;
            final float n11 = 0.0f;
            final float n12 = 0.0f;
            switch (n) {
                default: {
                    if (!str.startsWith("CUSTOM")) {
                        final StringBuilder sb = new StringBuilder();
                        sb.append("UNKNOWN spline ");
                        sb.append(str);
                        Utils.loge("MotionPaths", sb.toString());
                        continue;
                    }
                    final String s = str.split(",")[1];
                    if (!this.mCustomVariable.containsKey(s)) {
                        continue;
                    }
                    final CustomVariable customVariable = this.mCustomVariable.get(s);
                    if (obj instanceof SplineSet.CustomSpline) {
                        ((SplineSet.CustomSpline)obj).setPoint(i, customVariable);
                        continue;
                    }
                    final StringBuilder sb2 = new StringBuilder();
                    sb2.append(str);
                    sb2.append(" ViewSpline not a CustomSet frame = ");
                    sb2.append(i);
                    sb2.append(", value");
                    sb2.append(customVariable.getValueToInterpolate());
                    sb2.append(obj);
                    Utils.loge("MotionPaths", sb2.toString());
                    continue;
                }
                case 12: {
                    float mPathRotate;
                    if (Float.isNaN(this.mPathRotate)) {
                        mPathRotate = n12;
                    }
                    else {
                        mPathRotate = this.mPathRotate;
                    }
                    obj.setPoint(i, mPathRotate);
                    continue;
                }
                case 11: {
                    if (!Float.isNaN(this.alpha)) {
                        n2 = this.alpha;
                    }
                    obj.setPoint(i, n2);
                    continue;
                }
                case 10: {
                    if (!Float.isNaN(this.scaleY)) {
                        n2 = this.scaleY;
                    }
                    obj.setPoint(i, n2);
                    continue;
                }
                case 9: {
                    if (!Float.isNaN(this.scaleX)) {
                        n2 = this.scaleX;
                    }
                    obj.setPoint(i, n2);
                    continue;
                }
                case 8: {
                    float mPivotY;
                    if (Float.isNaN(this.mPivotY)) {
                        mPivotY = n3;
                    }
                    else {
                        mPivotY = this.mPivotY;
                    }
                    obj.setPoint(i, mPivotY);
                    continue;
                }
                case 7: {
                    float mPivotX;
                    if (Float.isNaN(this.mPivotX)) {
                        mPivotX = n4;
                    }
                    else {
                        mPivotX = this.mPivotX;
                    }
                    obj.setPoint(i, mPivotX);
                    continue;
                }
                case 6: {
                    float mProgress;
                    if (Float.isNaN(this.mProgress)) {
                        mProgress = n5;
                    }
                    else {
                        mProgress = this.mProgress;
                    }
                    obj.setPoint(i, mProgress);
                    continue;
                }
                case 5: {
                    float translationZ;
                    if (Float.isNaN(this.translationZ)) {
                        translationZ = n6;
                    }
                    else {
                        translationZ = this.translationZ;
                    }
                    obj.setPoint(i, translationZ);
                    continue;
                }
                case 4: {
                    float translationY;
                    if (Float.isNaN(this.translationY)) {
                        translationY = n7;
                    }
                    else {
                        translationY = this.translationY;
                    }
                    obj.setPoint(i, translationY);
                    continue;
                }
                case 3: {
                    float translationX;
                    if (Float.isNaN(this.translationX)) {
                        translationX = n8;
                    }
                    else {
                        translationX = this.translationX;
                    }
                    obj.setPoint(i, translationX);
                    continue;
                }
                case 2: {
                    float rotation;
                    if (Float.isNaN(this.rotation)) {
                        rotation = n9;
                    }
                    else {
                        rotation = this.rotation;
                    }
                    obj.setPoint(i, rotation);
                    continue;
                }
                case 1: {
                    float rotationY;
                    if (Float.isNaN(this.rotationY)) {
                        rotationY = n10;
                    }
                    else {
                        rotationY = this.rotationY;
                    }
                    obj.setPoint(i, rotationY);
                    continue;
                }
                case 0: {
                    float rotationX;
                    if (Float.isNaN(this.rotationX)) {
                        rotationX = n11;
                    }
                    else {
                        rotationX = this.rotationX;
                    }
                    obj.setPoint(i, rotationX);
                    continue;
                }
            }
        }
    }
    
    public void applyParameters(final MotionWidget motionWidget) {
        this.visibility = motionWidget.getVisibility();
        float alpha;
        if (motionWidget.getVisibility() != 4) {
            alpha = 0.0f;
        }
        else {
            alpha = motionWidget.getAlpha();
        }
        this.alpha = alpha;
        this.applyElevation = false;
        this.rotation = motionWidget.getRotationZ();
        this.rotationX = motionWidget.getRotationX();
        this.rotationY = motionWidget.getRotationY();
        this.scaleX = motionWidget.getScaleX();
        this.scaleY = motionWidget.getScaleY();
        this.mPivotX = motionWidget.getPivotX();
        this.mPivotY = motionWidget.getPivotY();
        this.translationX = motionWidget.getTranslationX();
        this.translationY = motionWidget.getTranslationY();
        this.translationZ = motionWidget.getTranslationZ();
        for (final String key : motionWidget.getCustomAttributeNames()) {
            final CustomVariable customAttribute = motionWidget.getCustomAttribute(key);
            if (customAttribute != null && customAttribute.isContinuous()) {
                this.mCustomVariable.put(key, customAttribute);
            }
        }
    }
    
    @Override
    public int compareTo(final MotionConstrainedPoint motionConstrainedPoint) {
        return Float.compare(this.position, motionConstrainedPoint.position);
    }
    
    void different(final MotionConstrainedPoint motionConstrainedPoint, final HashSet<String> set) {
        if (this.diff(this.alpha, motionConstrainedPoint.alpha)) {
            set.add("alpha");
        }
        if (this.diff(this.elevation, motionConstrainedPoint.elevation)) {
            set.add("translationZ");
        }
        final int visibility = this.visibility;
        final int visibility2 = motionConstrainedPoint.visibility;
        if (visibility != visibility2 && this.mVisibilityMode == 0 && (visibility == 4 || visibility2 == 4)) {
            set.add("alpha");
        }
        if (this.diff(this.rotation, motionConstrainedPoint.rotation)) {
            set.add("rotationZ");
        }
        if (!Float.isNaN(this.mPathRotate) || !Float.isNaN(motionConstrainedPoint.mPathRotate)) {
            set.add("pathRotate");
        }
        if (!Float.isNaN(this.mProgress) || !Float.isNaN(motionConstrainedPoint.mProgress)) {
            set.add("progress");
        }
        if (this.diff(this.rotationX, motionConstrainedPoint.rotationX)) {
            set.add("rotationX");
        }
        if (this.diff(this.rotationY, motionConstrainedPoint.rotationY)) {
            set.add("rotationY");
        }
        if (this.diff(this.mPivotX, motionConstrainedPoint.mPivotX)) {
            set.add("pivotX");
        }
        if (this.diff(this.mPivotY, motionConstrainedPoint.mPivotY)) {
            set.add("pivotY");
        }
        if (this.diff(this.scaleX, motionConstrainedPoint.scaleX)) {
            set.add("scaleX");
        }
        if (this.diff(this.scaleY, motionConstrainedPoint.scaleY)) {
            set.add("scaleY");
        }
        if (this.diff(this.translationX, motionConstrainedPoint.translationX)) {
            set.add("translationX");
        }
        if (this.diff(this.translationY, motionConstrainedPoint.translationY)) {
            set.add("translationY");
        }
        if (this.diff(this.translationZ, motionConstrainedPoint.translationZ)) {
            set.add("translationZ");
        }
        if (this.diff(this.elevation, motionConstrainedPoint.elevation)) {
            set.add("elevation");
        }
    }
    
    void different(final MotionConstrainedPoint motionConstrainedPoint, final boolean[] array, final String[] array2) {
        array[0] |= this.diff(this.position, motionConstrainedPoint.position);
        array[1] |= this.diff(this.x, motionConstrainedPoint.x);
        array[2] |= this.diff(this.y, motionConstrainedPoint.y);
        array[3] |= this.diff(this.width, motionConstrainedPoint.width);
        array[4] |= this.diff(this.height, motionConstrainedPoint.height);
    }
    
    void fillStandard(final double[] array, final int[] array2) {
        final float position = this.position;
        int i = 0;
        final float x = this.x;
        final float y = this.y;
        final float width = this.width;
        final float height = this.height;
        final float alpha = this.alpha;
        final float elevation = this.elevation;
        final float rotation = this.rotation;
        final float rotationX = this.rotationX;
        final float rotationY = this.rotationY;
        final float scaleX = this.scaleX;
        final float scaleY = this.scaleY;
        final float mPivotX = this.mPivotX;
        final float mPivotY = this.mPivotY;
        final float translationX = this.translationX;
        final float translationY = this.translationY;
        final float translationZ = this.translationZ;
        final float mPathRotate = this.mPathRotate;
        int n = 0;
        while (i < array2.length) {
            final int n2 = array2[i];
            int n3 = n;
            if (n2 < 18) {
                array[n] = (new float[] { position, x, y, width, height, alpha, elevation, rotation, rotationX, rotationY, scaleX, scaleY, mPivotX, mPivotY, translationX, translationY, translationZ, mPathRotate })[n2];
                n3 = n + 1;
            }
            ++i;
            n = n3;
        }
    }
    
    int getCustomData(final String key, final double[] array, int n) {
        final CustomVariable customVariable = this.mCustomVariable.get(key);
        if (customVariable.numberOfInterpolatedValues() == 1) {
            array[n] = customVariable.getValueToInterpolate();
            return 1;
        }
        final int numberOfInterpolatedValues = customVariable.numberOfInterpolatedValues();
        final float[] array2 = new float[numberOfInterpolatedValues];
        customVariable.getValuesToInterpolate(array2);
        for (int i = 0; i < numberOfInterpolatedValues; ++i, ++n) {
            array[n] = array2[i];
        }
        return numberOfInterpolatedValues;
    }
    
    int getCustomDataCount(final String key) {
        return this.mCustomVariable.get(key).numberOfInterpolatedValues();
    }
    
    boolean hasCustomData(final String key) {
        return this.mCustomVariable.containsKey(key);
    }
    
    void setBounds(final float x, final float y, final float width, final float height) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
    }
    
    public void setState(final MotionWidget motionWidget) {
        this.setBounds((float)motionWidget.getX(), (float)motionWidget.getY(), (float)motionWidget.getWidth(), (float)motionWidget.getHeight());
        this.applyParameters(motionWidget);
    }
    
    public void setState(final Rect rect, final MotionWidget motionWidget, final int n, final float n2) {
        this.setBounds((float)rect.left, (float)rect.top, (float)rect.width(), (float)rect.height());
        this.applyParameters(motionWidget);
        this.mPivotX = Float.NaN;
        this.mPivotY = Float.NaN;
        if (n != 1) {
            if (n == 2) {
                this.rotation = n2 + 90.0f;
            }
        }
        else {
            this.rotation = n2 - 90.0f;
        }
    }
}

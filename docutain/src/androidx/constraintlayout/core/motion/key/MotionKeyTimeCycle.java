// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.core.motion.key;

import androidx.constraintlayout.core.motion.utils.TypedValues$CycleType$_CC;
import java.util.HashSet;
import androidx.constraintlayout.core.motion.utils.SplineSet;
import java.util.Iterator;
import androidx.constraintlayout.core.motion.utils.Utils;
import androidx.constraintlayout.core.motion.utils.TimeCycleSplineSet;
import androidx.constraintlayout.core.motion.CustomVariable;
import java.util.HashMap;

public class MotionKeyTimeCycle extends MotionKey
{
    public static final int KEY_TYPE = 3;
    static final String NAME = "KeyTimeCycle";
    private static final String TAG = "KeyTimeCycle";
    private float mAlpha;
    private int mCurveFit;
    private String mCustomWaveShape;
    private float mElevation;
    private float mProgress;
    private float mRotation;
    private float mRotationX;
    private float mRotationY;
    private float mScaleX;
    private float mScaleY;
    private String mTransitionEasing;
    private float mTransitionPathRotate;
    private float mTranslationX;
    private float mTranslationY;
    private float mTranslationZ;
    private float mWaveOffset;
    private float mWavePeriod;
    private int mWaveShape;
    
    public MotionKeyTimeCycle() {
        this.mCurveFit = -1;
        this.mAlpha = Float.NaN;
        this.mElevation = Float.NaN;
        this.mRotation = Float.NaN;
        this.mRotationX = Float.NaN;
        this.mRotationY = Float.NaN;
        this.mTransitionPathRotate = Float.NaN;
        this.mScaleX = Float.NaN;
        this.mScaleY = Float.NaN;
        this.mTranslationX = Float.NaN;
        this.mTranslationY = Float.NaN;
        this.mTranslationZ = Float.NaN;
        this.mProgress = Float.NaN;
        this.mWaveShape = 0;
        this.mCustomWaveShape = null;
        this.mWavePeriod = Float.NaN;
        this.mWaveOffset = 0.0f;
        this.mType = 3;
        this.mCustom = new HashMap<String, CustomVariable>();
    }
    
    public void addTimeValues(final HashMap<String, TimeCycleSplineSet> hashMap) {
        for (final String s : hashMap.keySet()) {
            final TimeCycleSplineSet set = hashMap.get(s);
            if (set == null) {
                continue;
            }
            final boolean startsWith = s.startsWith("CUSTOM");
            int n = 7;
            if (startsWith) {
                final CustomVariable customVariable = this.mCustom.get(s.substring(7));
                if (customVariable == null) {
                    continue;
                }
                ((TimeCycleSplineSet.CustomVarSet)set).setPoint(this.mFramePosition, customVariable, this.mWavePeriod, this.mWaveShape, this.mWaveOffset);
            }
            else {
                s.hashCode();
                Label_0458: {
                    switch (s) {
                        case "pathRotate": {
                            n = 11;
                            break Label_0458;
                        }
                        case "alpha": {
                            n = 10;
                            break Label_0458;
                        }
                        case "elevation": {
                            n = 9;
                            break Label_0458;
                        }
                        case "scaleY": {
                            n = 8;
                            break Label_0458;
                        }
                        case "scaleX": {
                            break Label_0458;
                        }
                        case "progress": {
                            n = 6;
                            break Label_0458;
                        }
                        case "translationZ": {
                            n = 5;
                            break Label_0458;
                        }
                        case "translationY": {
                            n = 4;
                            break Label_0458;
                        }
                        case "translationX": {
                            n = 3;
                            break Label_0458;
                        }
                        case "rotationZ": {
                            n = 2;
                            break Label_0458;
                        }
                        case "rotationY": {
                            n = 1;
                            break Label_0458;
                        }
                        case "rotationX": {
                            n = 0;
                            break Label_0458;
                        }
                        default:
                            break;
                    }
                    n = -1;
                }
                switch (n) {
                    default: {
                        final StringBuilder sb = new StringBuilder();
                        sb.append("UNKNOWN addValues \"");
                        sb.append(s);
                        sb.append("\"");
                        Utils.loge("KeyTimeCycles", sb.toString());
                        continue;
                    }
                    case 11: {
                        if (!Float.isNaN(this.mTransitionPathRotate)) {
                            set.setPoint(this.mFramePosition, this.mTransitionPathRotate, this.mWavePeriod, this.mWaveShape, this.mWaveOffset);
                            continue;
                        }
                        continue;
                    }
                    case 10: {
                        if (!Float.isNaN(this.mAlpha)) {
                            set.setPoint(this.mFramePosition, this.mAlpha, this.mWavePeriod, this.mWaveShape, this.mWaveOffset);
                            continue;
                        }
                        continue;
                    }
                    case 9: {
                        if (!Float.isNaN(this.mTranslationZ)) {
                            set.setPoint(this.mFramePosition, this.mTranslationZ, this.mWavePeriod, this.mWaveShape, this.mWaveOffset);
                            continue;
                        }
                        continue;
                    }
                    case 8: {
                        if (!Float.isNaN(this.mScaleY)) {
                            set.setPoint(this.mFramePosition, this.mScaleY, this.mWavePeriod, this.mWaveShape, this.mWaveOffset);
                            continue;
                        }
                        continue;
                    }
                    case 7: {
                        if (!Float.isNaN(this.mScaleX)) {
                            set.setPoint(this.mFramePosition, this.mScaleX, this.mWavePeriod, this.mWaveShape, this.mWaveOffset);
                            continue;
                        }
                        continue;
                    }
                    case 6: {
                        if (!Float.isNaN(this.mProgress)) {
                            set.setPoint(this.mFramePosition, this.mProgress, this.mWavePeriod, this.mWaveShape, this.mWaveOffset);
                            continue;
                        }
                        continue;
                    }
                    case 5: {
                        if (!Float.isNaN(this.mTranslationZ)) {
                            set.setPoint(this.mFramePosition, this.mTranslationZ, this.mWavePeriod, this.mWaveShape, this.mWaveOffset);
                            continue;
                        }
                        continue;
                    }
                    case 4: {
                        if (!Float.isNaN(this.mTranslationY)) {
                            set.setPoint(this.mFramePosition, this.mTranslationY, this.mWavePeriod, this.mWaveShape, this.mWaveOffset);
                            continue;
                        }
                        continue;
                    }
                    case 3: {
                        if (!Float.isNaN(this.mTranslationX)) {
                            set.setPoint(this.mFramePosition, this.mTranslationX, this.mWavePeriod, this.mWaveShape, this.mWaveOffset);
                            continue;
                        }
                        continue;
                    }
                    case 2: {
                        if (!Float.isNaN(this.mRotation)) {
                            set.setPoint(this.mFramePosition, this.mRotation, this.mWavePeriod, this.mWaveShape, this.mWaveOffset);
                            continue;
                        }
                        continue;
                    }
                    case 1: {
                        if (!Float.isNaN(this.mRotationY)) {
                            set.setPoint(this.mFramePosition, this.mRotationY, this.mWavePeriod, this.mWaveShape, this.mWaveOffset);
                            continue;
                        }
                        continue;
                    }
                    case 0: {
                        if (!Float.isNaN(this.mRotationX)) {
                            set.setPoint(this.mFramePosition, this.mRotationX, this.mWavePeriod, this.mWaveShape, this.mWaveOffset);
                            continue;
                        }
                        continue;
                    }
                }
            }
        }
    }
    
    @Override
    public void addValues(final HashMap<String, SplineSet> hashMap) {
    }
    
    @Override
    public MotionKey clone() {
        return new MotionKeyTimeCycle().copy(this);
    }
    
    @Override
    public MotionKeyTimeCycle copy(final MotionKey motionKey) {
        super.copy(motionKey);
        final MotionKeyTimeCycle motionKeyTimeCycle = (MotionKeyTimeCycle)motionKey;
        this.mTransitionEasing = motionKeyTimeCycle.mTransitionEasing;
        this.mCurveFit = motionKeyTimeCycle.mCurveFit;
        this.mWaveShape = motionKeyTimeCycle.mWaveShape;
        this.mWavePeriod = motionKeyTimeCycle.mWavePeriod;
        this.mWaveOffset = motionKeyTimeCycle.mWaveOffset;
        this.mProgress = motionKeyTimeCycle.mProgress;
        this.mAlpha = motionKeyTimeCycle.mAlpha;
        this.mElevation = motionKeyTimeCycle.mElevation;
        this.mRotation = motionKeyTimeCycle.mRotation;
        this.mTransitionPathRotate = motionKeyTimeCycle.mTransitionPathRotate;
        this.mRotationX = motionKeyTimeCycle.mRotationX;
        this.mRotationY = motionKeyTimeCycle.mRotationY;
        this.mScaleX = motionKeyTimeCycle.mScaleX;
        this.mScaleY = motionKeyTimeCycle.mScaleY;
        this.mTranslationX = motionKeyTimeCycle.mTranslationX;
        this.mTranslationY = motionKeyTimeCycle.mTranslationY;
        this.mTranslationZ = motionKeyTimeCycle.mTranslationZ;
        return this;
    }
    
    @Override
    public void getAttributeNames(final HashSet<String> set) {
        if (!Float.isNaN(this.mAlpha)) {
            set.add("alpha");
        }
        if (!Float.isNaN(this.mElevation)) {
            set.add("elevation");
        }
        if (!Float.isNaN(this.mRotation)) {
            set.add("rotationZ");
        }
        if (!Float.isNaN(this.mRotationX)) {
            set.add("rotationX");
        }
        if (!Float.isNaN(this.mRotationY)) {
            set.add("rotationY");
        }
        if (!Float.isNaN(this.mScaleX)) {
            set.add("scaleX");
        }
        if (!Float.isNaN(this.mScaleY)) {
            set.add("scaleY");
        }
        if (!Float.isNaN(this.mTransitionPathRotate)) {
            set.add("pathRotate");
        }
        if (!Float.isNaN(this.mTranslationX)) {
            set.add("translationX");
        }
        if (!Float.isNaN(this.mTranslationY)) {
            set.add("translationY");
        }
        if (!Float.isNaN(this.mTranslationZ)) {
            set.add("translationZ");
        }
        if (this.mCustom.size() > 0) {
            for (final String str : this.mCustom.keySet()) {
                final StringBuilder sb = new StringBuilder();
                sb.append("CUSTOM,");
                sb.append(str);
                set.add(sb.toString());
            }
        }
    }
    
    @Override
    public int getId(final String s) {
        return TypedValues$CycleType$_CC.getId(s);
    }
    
    @Override
    public boolean setValue(final int n, final float f) {
        if (n != 315) {
            if (n != 401) {
                if (n != 403) {
                    if (n != 416) {
                        if (n != 423) {
                            if (n != 424) {
                                switch (n) {
                                    default: {
                                        return super.setValue(n, f);
                                    }
                                    case 312: {
                                        this.mScaleY = this.toFloat(f);
                                        break;
                                    }
                                    case 311: {
                                        this.mScaleX = this.toFloat(f);
                                        break;
                                    }
                                    case 310: {
                                        this.mRotation = this.toFloat(f);
                                        break;
                                    }
                                    case 309: {
                                        this.mRotationY = this.toFloat(f);
                                        break;
                                    }
                                    case 308: {
                                        this.mRotationX = this.toFloat(f);
                                        break;
                                    }
                                    case 307: {
                                        this.mElevation = this.toFloat(f);
                                        break;
                                    }
                                    case 306: {
                                        this.mTranslationZ = this.toFloat(f);
                                        break;
                                    }
                                    case 305: {
                                        this.mTranslationY = this.toFloat(f);
                                        break;
                                    }
                                    case 304: {
                                        this.mTranslationX = this.toFloat(f);
                                        break;
                                    }
                                }
                            }
                            else {
                                this.mWaveOffset = this.toFloat(f);
                            }
                        }
                        else {
                            this.mWavePeriod = this.toFloat(f);
                        }
                    }
                    else {
                        this.mTransitionPathRotate = this.toFloat(f);
                    }
                }
                else {
                    this.mAlpha = f;
                }
            }
            else {
                this.mCurveFit = this.toInt(f);
            }
        }
        else {
            this.mProgress = this.toFloat(f);
        }
        return true;
    }
    
    @Override
    public boolean setValue(final int n, final int n2) {
        if (n != 100) {
            if (n != 421) {
                return super.setValue(n, n2);
            }
            this.mWaveShape = n2;
        }
        else {
            this.mFramePosition = n2;
        }
        return true;
    }
    
    @Override
    public boolean setValue(final int n, final String s) {
        if (n != 420) {
            if (n != 421) {
                return super.setValue(n, s);
            }
            this.mWaveShape = 7;
            this.mCustomWaveShape = s;
        }
        else {
            this.mTransitionEasing = s;
        }
        return true;
    }
    
    @Override
    public boolean setValue(final int n, final boolean b) {
        return super.setValue(n, b);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.core.motion.key;

import androidx.constraintlayout.core.motion.MotionWidget;
import androidx.constraintlayout.core.motion.utils.FloatRect;
import androidx.constraintlayout.core.motion.utils.TypedValues$PositionType$_CC;
import java.util.HashSet;
import androidx.constraintlayout.core.motion.utils.SplineSet;
import java.util.HashMap;

public class MotionKeyPosition extends MotionKey
{
    static final int KEY_TYPE = 2;
    static final String NAME = "KeyPosition";
    protected static final float SELECTION_SLOPE = 20.0f;
    public static final int TYPE_CARTESIAN = 0;
    public static final int TYPE_PATH = 1;
    public static final int TYPE_SCREEN = 2;
    public float mAltPercentX;
    public float mAltPercentY;
    private float mCalculatedPositionX;
    private float mCalculatedPositionY;
    public int mCurveFit;
    public int mDrawPath;
    public int mPathMotionArc;
    public float mPercentHeight;
    public float mPercentWidth;
    public float mPercentX;
    public float mPercentY;
    public int mPositionType;
    public String mTransitionEasing;
    
    public MotionKeyPosition() {
        this.mCurveFit = MotionKeyPosition.UNSET;
        this.mTransitionEasing = null;
        this.mPathMotionArc = MotionKeyPosition.UNSET;
        this.mDrawPath = 0;
        this.mPercentWidth = Float.NaN;
        this.mPercentHeight = Float.NaN;
        this.mPercentX = Float.NaN;
        this.mPercentY = Float.NaN;
        this.mAltPercentX = Float.NaN;
        this.mAltPercentY = Float.NaN;
        this.mPositionType = 0;
        this.mCalculatedPositionX = Float.NaN;
        this.mCalculatedPositionY = Float.NaN;
        this.mType = 2;
    }
    
    private void calcCartesianPosition(final float n, final float n2, float mPercentX, float mAltPercentY) {
        final float n3 = mPercentX - n;
        final float n4 = mAltPercentY - n2;
        final boolean naN = Float.isNaN(this.mPercentX);
        float mAltPercentX = 0.0f;
        if (naN) {
            mPercentX = 0.0f;
        }
        else {
            mPercentX = this.mPercentX;
        }
        if (Float.isNaN(this.mAltPercentY)) {
            mAltPercentY = 0.0f;
        }
        else {
            mAltPercentY = this.mAltPercentY;
        }
        float mPercentY;
        if (Float.isNaN(this.mPercentY)) {
            mPercentY = 0.0f;
        }
        else {
            mPercentY = this.mPercentY;
        }
        if (!Float.isNaN(this.mAltPercentX)) {
            mAltPercentX = this.mAltPercentX;
        }
        this.mCalculatedPositionX = (float)(int)(n + mPercentX * n3 + mAltPercentX * n4);
        this.mCalculatedPositionY = (float)(int)(n2 + n3 * mAltPercentY + n4 * mPercentY);
    }
    
    private void calcPathPosition(final float n, final float n2, float n3, float mPercentX) {
        n3 -= n;
        final float n4 = mPercentX - n2;
        final float n5 = -n4;
        mPercentX = this.mPercentX;
        final float mPercentY = this.mPercentY;
        this.mCalculatedPositionX = n + n3 * mPercentX + n5 * mPercentY;
        this.mCalculatedPositionY = n2 + n4 * mPercentX + n3 * mPercentY;
    }
    
    private void calcScreenPosition(final int n, final int n2) {
        final float n3 = (float)(n - 0);
        final float mPercentX = this.mPercentX;
        final float n4 = 0;
        this.mCalculatedPositionX = n3 * mPercentX + n4;
        this.mCalculatedPositionY = (n2 - 0) * mPercentX + n4;
    }
    
    @Override
    public void addValues(final HashMap<String, SplineSet> hashMap) {
    }
    
    void calcPosition(final int n, final int n2, final float n3, final float n4, final float n5, final float n6) {
        final int mPositionType = this.mPositionType;
        if (mPositionType == 1) {
            this.calcPathPosition(n3, n4, n5, n6);
            return;
        }
        if (mPositionType != 2) {
            this.calcCartesianPosition(n3, n4, n5, n6);
            return;
        }
        this.calcScreenPosition(n, n2);
    }
    
    @Override
    public MotionKey clone() {
        return new MotionKeyPosition().copy(this);
    }
    
    @Override
    public MotionKey copy(final MotionKey motionKey) {
        super.copy(motionKey);
        final MotionKeyPosition motionKeyPosition = (MotionKeyPosition)motionKey;
        this.mTransitionEasing = motionKeyPosition.mTransitionEasing;
        this.mPathMotionArc = motionKeyPosition.mPathMotionArc;
        this.mDrawPath = motionKeyPosition.mDrawPath;
        this.mPercentWidth = motionKeyPosition.mPercentWidth;
        this.mPercentHeight = Float.NaN;
        this.mPercentX = motionKeyPosition.mPercentX;
        this.mPercentY = motionKeyPosition.mPercentY;
        this.mAltPercentX = motionKeyPosition.mAltPercentX;
        this.mAltPercentY = motionKeyPosition.mAltPercentY;
        this.mCalculatedPositionX = motionKeyPosition.mCalculatedPositionX;
        this.mCalculatedPositionY = motionKeyPosition.mCalculatedPositionY;
        return this;
    }
    
    @Override
    public void getAttributeNames(final HashSet<String> set) {
    }
    
    @Override
    public int getId(final String s) {
        return TypedValues$PositionType$_CC.getId(s);
    }
    
    float getPositionX() {
        return this.mCalculatedPositionX;
    }
    
    float getPositionY() {
        return this.mCalculatedPositionY;
    }
    
    public boolean intersects(final int n, final int n2, final FloatRect floatRect, final FloatRect floatRect2, final float n3, final float n4) {
        this.calcPosition(n, n2, floatRect.centerX(), floatRect.centerY(), floatRect2.centerX(), floatRect2.centerY());
        return Math.abs(n3 - this.mCalculatedPositionX) < 20.0f && Math.abs(n4 - this.mCalculatedPositionY) < 20.0f;
    }
    
    public void positionAttributes(final MotionWidget motionWidget, final FloatRect floatRect, final FloatRect floatRect2, final float n, final float n2, final String[] array, final float[] array2) {
        final int mPositionType = this.mPositionType;
        if (mPositionType == 1) {
            this.positionPathAttributes(floatRect, floatRect2, n, n2, array, array2);
            return;
        }
        if (mPositionType != 2) {
            this.positionCartAttributes(floatRect, floatRect2, n, n2, array, array2);
            return;
        }
        this.positionScreenAttributes(motionWidget, floatRect, floatRect2, n, n2, array, array2);
    }
    
    void positionCartAttributes(final FloatRect floatRect, final FloatRect floatRect2, final float n, final float n2, final String[] array, final float[] array2) {
        final float centerX = floatRect.centerX();
        final float centerY = floatRect.centerY();
        final float centerX2 = floatRect2.centerX();
        final float centerY2 = floatRect2.centerY();
        final float n3 = centerX2 - centerX;
        final float n4 = centerY2 - centerY;
        final String anObject = array[0];
        if (anObject != null) {
            if ("percentX".equals(anObject)) {
                array2[0] = (n - centerX) / n3;
                array2[1] = (n2 - centerY) / n4;
            }
            else {
                array2[1] = (n - centerX) / n3;
                array2[0] = (n2 - centerY) / n4;
            }
        }
        else {
            array[0] = "percentX";
            array2[0] = (n - centerX) / n3;
            array[1] = "percentY";
            array2[1] = (n2 - centerY) / n4;
        }
    }
    
    void positionPathAttributes(final FloatRect floatRect, final FloatRect floatRect2, float n, float n2, final String[] array, final float[] array2) {
        final float centerX = floatRect.centerX();
        final float centerY = floatRect.centerY();
        final float centerX2 = floatRect2.centerX();
        final float centerY2 = floatRect2.centerY();
        final float n3 = centerX2 - centerX;
        final float n4 = centerY2 - centerY;
        final float n5 = (float)Math.hypot(n3, n4);
        if (n5 < 1.0E-4) {
            System.out.println("distance ~ 0");
            array2[1] = (array2[0] = 0.0f);
            return;
        }
        final float n6 = n3 / n5;
        final float n7 = n4 / n5;
        n2 -= centerY;
        final float n8 = n - centerX;
        n = (n6 * n2 - n8 * n7) / n5;
        n2 = (n6 * n8 + n7 * n2) / n5;
        final String anObject = array[0];
        if (anObject != null) {
            if ("percentX".equals(anObject)) {
                array2[0] = n2;
                array2[1] = n;
            }
        }
        else {
            array[0] = "percentX";
            array[1] = "percentY";
            array2[0] = n2;
            array2[1] = n;
        }
    }
    
    void positionScreenAttributes(MotionWidget parent, final FloatRect floatRect, final FloatRect floatRect2, final float n, final float n2, final String[] array, final float[] array2) {
        floatRect.centerX();
        floatRect.centerY();
        floatRect2.centerX();
        floatRect2.centerY();
        parent = parent.getParent();
        final int width = parent.getWidth();
        final int height = parent.getHeight();
        final String anObject = array[0];
        if (anObject != null) {
            if ("percentX".equals(anObject)) {
                array2[0] = n / width;
                array2[1] = n2 / height;
            }
            else {
                array2[1] = n / width;
                array2[0] = n2 / height;
            }
        }
        else {
            array[0] = "percentX";
            array2[0] = n / width;
            array[1] = "percentY";
            array2[1] = n2 / height;
        }
    }
    
    @Override
    public boolean setValue(final int n, final float n2) {
        switch (n) {
            default: {
                return super.setValue(n, n2);
            }
            case 507: {
                this.mPercentY = n2;
                break;
            }
            case 506: {
                this.mPercentX = n2;
                break;
            }
            case 505: {
                this.mPercentWidth = n2;
                this.mPercentHeight = n2;
                break;
            }
            case 504: {
                this.mPercentHeight = n2;
                break;
            }
            case 503: {
                this.mPercentWidth = n2;
                break;
            }
        }
        return true;
    }
    
    @Override
    public boolean setValue(final int n, final int mFramePosition) {
        if (n != 100) {
            if (n != 508) {
                if (n != 510) {
                    return super.setValue(n, mFramePosition);
                }
                this.mPositionType = mFramePosition;
            }
            else {
                this.mCurveFit = mFramePosition;
            }
        }
        else {
            this.mFramePosition = mFramePosition;
        }
        return true;
    }
    
    @Override
    public boolean setValue(final int n, final String s) {
        if (n != 501) {
            return super.setValue(n, s);
        }
        this.mTransitionEasing = s.toString();
        return true;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.core.motion.key;

import java.util.HashSet;
import androidx.constraintlayout.core.motion.utils.SplineSet;
import java.util.Iterator;
import java.util.Locale;
import androidx.constraintlayout.core.motion.MotionWidget;
import androidx.constraintlayout.core.motion.CustomVariable;
import java.util.HashMap;
import androidx.constraintlayout.core.motion.utils.FloatRect;

public class MotionKeyTrigger extends MotionKey
{
    public static final String CROSS = "CROSS";
    public static final int KEY_TYPE = 5;
    public static final String NEGATIVE_CROSS = "negativeCross";
    public static final String POSITIVE_CROSS = "positiveCross";
    public static final String POST_LAYOUT = "postLayout";
    private static final String TAG = "KeyTrigger";
    public static final String TRIGGER_COLLISION_ID = "triggerCollisionId";
    public static final String TRIGGER_COLLISION_VIEW = "triggerCollisionView";
    public static final String TRIGGER_ID = "triggerID";
    public static final String TRIGGER_RECEIVER = "triggerReceiver";
    public static final String TRIGGER_SLACK = "triggerSlack";
    public static final int TYPE_CROSS = 312;
    public static final int TYPE_NEGATIVE_CROSS = 310;
    public static final int TYPE_POSITIVE_CROSS = 309;
    public static final int TYPE_POST_LAYOUT = 304;
    public static final int TYPE_TRIGGER_COLLISION_ID = 307;
    public static final int TYPE_TRIGGER_COLLISION_VIEW = 306;
    public static final int TYPE_TRIGGER_ID = 308;
    public static final int TYPE_TRIGGER_RECEIVER = 311;
    public static final int TYPE_TRIGGER_SLACK = 305;
    public static final int TYPE_VIEW_TRANSITION_ON_CROSS = 301;
    public static final int TYPE_VIEW_TRANSITION_ON_NEGATIVE_CROSS = 303;
    public static final int TYPE_VIEW_TRANSITION_ON_POSITIVE_CROSS = 302;
    public static final String VIEW_TRANSITION_ON_CROSS = "viewTransitionOnCross";
    public static final String VIEW_TRANSITION_ON_NEGATIVE_CROSS = "viewTransitionOnNegativeCross";
    public static final String VIEW_TRANSITION_ON_POSITIVE_CROSS = "viewTransitionOnPositiveCross";
    FloatRect mCollisionRect;
    private String mCross;
    private int mCurveFit;
    private boolean mFireCrossReset;
    private float mFireLastPos;
    private boolean mFireNegativeReset;
    private boolean mFirePositiveReset;
    private float mFireThreshold;
    private String mNegativeCross;
    private String mPositiveCross;
    private boolean mPostLayout;
    FloatRect mTargetRect;
    private int mTriggerCollisionId;
    private int mTriggerID;
    private int mTriggerReceiver;
    float mTriggerSlack;
    int mViewTransitionOnCross;
    int mViewTransitionOnNegativeCross;
    int mViewTransitionOnPositiveCross;
    
    public MotionKeyTrigger() {
        this.mCurveFit = -1;
        this.mCross = null;
        this.mTriggerReceiver = MotionKeyTrigger.UNSET;
        this.mNegativeCross = null;
        this.mPositiveCross = null;
        this.mTriggerID = MotionKeyTrigger.UNSET;
        this.mTriggerCollisionId = MotionKeyTrigger.UNSET;
        this.mTriggerSlack = 0.1f;
        this.mFireCrossReset = true;
        this.mFireNegativeReset = true;
        this.mFirePositiveReset = true;
        this.mFireThreshold = Float.NaN;
        this.mPostLayout = false;
        this.mViewTransitionOnNegativeCross = MotionKeyTrigger.UNSET;
        this.mViewTransitionOnPositiveCross = MotionKeyTrigger.UNSET;
        this.mViewTransitionOnCross = MotionKeyTrigger.UNSET;
        this.mCollisionRect = new FloatRect();
        this.mTargetRect = new FloatRect();
        this.mType = 5;
        this.mCustom = new HashMap<String, CustomVariable>();
    }
    
    private void fireCustom(final String s, final MotionWidget motionWidget) {
        final boolean b = s.length() == 1;
        String lowerCase = s;
        if (!b) {
            lowerCase = s.substring(1).toLowerCase(Locale.ROOT);
        }
        for (final String key : this.mCustom.keySet()) {
            final String lowerCase2 = key.toLowerCase(Locale.ROOT);
            if (b || lowerCase2.matches(lowerCase)) {
                final CustomVariable customVariable = this.mCustom.get(key);
                if (customVariable == null) {
                    continue;
                }
                customVariable.applyToWidget(motionWidget);
            }
        }
    }
    
    @Override
    public void addValues(final HashMap<String, SplineSet> hashMap) {
    }
    
    @Override
    public MotionKey clone() {
        return new MotionKeyTrigger().copy(this);
    }
    
    public void conditionallyFire(final float n, final MotionWidget motionWidget) {
    }
    
    @Override
    public MotionKeyTrigger copy(final MotionKey motionKey) {
        super.copy(motionKey);
        final MotionKeyTrigger motionKeyTrigger = (MotionKeyTrigger)motionKey;
        this.mCurveFit = motionKeyTrigger.mCurveFit;
        this.mCross = motionKeyTrigger.mCross;
        this.mTriggerReceiver = motionKeyTrigger.mTriggerReceiver;
        this.mNegativeCross = motionKeyTrigger.mNegativeCross;
        this.mPositiveCross = motionKeyTrigger.mPositiveCross;
        this.mTriggerID = motionKeyTrigger.mTriggerID;
        this.mTriggerCollisionId = motionKeyTrigger.mTriggerCollisionId;
        this.mTriggerSlack = motionKeyTrigger.mTriggerSlack;
        this.mFireCrossReset = motionKeyTrigger.mFireCrossReset;
        this.mFireNegativeReset = motionKeyTrigger.mFireNegativeReset;
        this.mFirePositiveReset = motionKeyTrigger.mFirePositiveReset;
        this.mFireThreshold = motionKeyTrigger.mFireThreshold;
        this.mFireLastPos = motionKeyTrigger.mFireLastPos;
        this.mPostLayout = motionKeyTrigger.mPostLayout;
        this.mCollisionRect = motionKeyTrigger.mCollisionRect;
        this.mTargetRect = motionKeyTrigger.mTargetRect;
        return this;
    }
    
    @Override
    public void getAttributeNames(final HashSet<String> set) {
    }
    
    @Override
    public int getId(final String s) {
        s.hashCode();
        int n = 0;
        Label_0302: {
            switch (s) {
                case "triggerReceiver": {
                    n = 10;
                    break Label_0302;
                }
                case "postLayout": {
                    n = 9;
                    break Label_0302;
                }
                case "viewTransitionOnCross": {
                    n = 8;
                    break Label_0302;
                }
                case "triggerSlack": {
                    n = 7;
                    break Label_0302;
                }
                case "viewTransitionOnNegativeCross": {
                    n = 6;
                    break Label_0302;
                }
                case "triggerCollisionView": {
                    n = 5;
                    break Label_0302;
                }
                case "negativeCross": {
                    n = 4;
                    break Label_0302;
                }
                case "triggerID": {
                    n = 3;
                    break Label_0302;
                }
                case "triggerCollisionId": {
                    n = 2;
                    break Label_0302;
                }
                case "viewTransitionOnPositiveCross": {
                    n = 1;
                    break Label_0302;
                }
                case "positiveCross": {
                    n = 0;
                    break Label_0302;
                }
                default:
                    break;
            }
            n = -1;
        }
        switch (n) {
            default: {
                return -1;
            }
            case 10: {
                return 311;
            }
            case 9: {
                return 304;
            }
            case 8: {
                return 301;
            }
            case 7: {
                return 305;
            }
            case 6: {
                return 303;
            }
            case 5: {
                return 306;
            }
            case 4: {
                return 310;
            }
            case 3: {
                return 308;
            }
            case 2: {
                return 307;
            }
            case 1: {
                return 302;
            }
            case 0: {
                return 309;
            }
        }
    }
    
    @Override
    public boolean setValue(final int n, final float mTriggerSlack) {
        if (n != 305) {
            return super.setValue(n, mTriggerSlack);
        }
        this.mTriggerSlack = mTriggerSlack;
        return true;
    }
    
    @Override
    public boolean setValue(final int n, final int n2) {
        if (n != 307) {
            if (n != 308) {
                if (n != 311) {
                    switch (n) {
                        default: {
                            return super.setValue(n, n2);
                        }
                        case 303: {
                            this.mViewTransitionOnNegativeCross = n2;
                            break;
                        }
                        case 302: {
                            this.mViewTransitionOnPositiveCross = n2;
                            break;
                        }
                        case 301: {
                            this.mViewTransitionOnCross = n2;
                            break;
                        }
                    }
                }
                else {
                    this.mTriggerReceiver = n2;
                }
            }
            else {
                this.mTriggerID = this.toInt(n2);
            }
        }
        else {
            this.mTriggerCollisionId = n2;
        }
        return true;
    }
    
    @Override
    public boolean setValue(final int n, final String mPositiveCross) {
        if (n != 309) {
            if (n != 310) {
                if (n != 312) {
                    return super.setValue(n, mPositiveCross);
                }
                this.mCross = mPositiveCross;
            }
            else {
                this.mNegativeCross = mPositiveCross;
            }
        }
        else {
            this.mPositiveCross = mPositiveCross;
        }
        return true;
    }
    
    @Override
    public boolean setValue(final int n, final boolean mPostLayout) {
        if (n != 304) {
            return super.setValue(n, mPostLayout);
        }
        this.mPostLayout = mPostLayout;
        return true;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.core.motion.key;

import androidx.constraintlayout.core.motion.utils.TypedValues$AttributesType$_CC;
import androidx.constraintlayout.core.motion.utils.Utils;
import java.util.HashSet;
import java.io.PrintStream;
import androidx.constraintlayout.core.motion.utils.SplineSet;
import java.util.Iterator;
import androidx.constraintlayout.core.motion.utils.KeyCycleOscillator;
import androidx.constraintlayout.core.motion.CustomVariable;
import java.util.HashMap;

public class MotionKeyCycle extends MotionKey
{
    public static final int KEY_TYPE = 4;
    static final String NAME = "KeyCycle";
    public static final int SHAPE_BOUNCE = 6;
    public static final int SHAPE_COS_WAVE = 5;
    public static final int SHAPE_REVERSE_SAW_WAVE = 4;
    public static final int SHAPE_SAW_WAVE = 3;
    public static final int SHAPE_SIN_WAVE = 0;
    public static final int SHAPE_SQUARE_WAVE = 1;
    public static final int SHAPE_TRIANGLE_WAVE = 2;
    private static final String TAG = "KeyCycle";
    public static final String WAVE_OFFSET = "waveOffset";
    public static final String WAVE_PERIOD = "wavePeriod";
    public static final String WAVE_PHASE = "wavePhase";
    public static final String WAVE_SHAPE = "waveShape";
    private float mAlpha;
    private int mCurveFit;
    private String mCustomWaveShape;
    private float mElevation;
    private float mProgress;
    private float mRotation;
    private float mRotationX;
    private float mRotationY;
    private float mScaleX;
    private float mScaleY;
    private String mTransitionEasing;
    private float mTransitionPathRotate;
    private float mTranslationX;
    private float mTranslationY;
    private float mTranslationZ;
    private float mWaveOffset;
    private float mWavePeriod;
    private float mWavePhase;
    private int mWaveShape;
    
    public MotionKeyCycle() {
        this.mTransitionEasing = null;
        this.mCurveFit = 0;
        this.mWaveShape = -1;
        this.mCustomWaveShape = null;
        this.mWavePeriod = Float.NaN;
        this.mWaveOffset = 0.0f;
        this.mWavePhase = 0.0f;
        this.mProgress = Float.NaN;
        this.mAlpha = Float.NaN;
        this.mElevation = Float.NaN;
        this.mRotation = Float.NaN;
        this.mTransitionPathRotate = Float.NaN;
        this.mRotationX = Float.NaN;
        this.mRotationY = Float.NaN;
        this.mScaleX = Float.NaN;
        this.mScaleY = Float.NaN;
        this.mTranslationX = Float.NaN;
        this.mTranslationY = Float.NaN;
        this.mTranslationZ = Float.NaN;
        this.mType = 4;
        this.mCustom = new HashMap<String, CustomVariable>();
    }
    
    public void addCycleValues(final HashMap<String, KeyCycleOscillator> hashMap) {
        for (final String s : hashMap.keySet()) {
            if (s.startsWith("CUSTOM")) {
                final CustomVariable customVariable = this.mCustom.get(s.substring(7));
                if (customVariable == null) {
                    continue;
                }
                if (customVariable.getType() != 901) {
                    continue;
                }
                final KeyCycleOscillator keyCycleOscillator = hashMap.get(s);
                if (keyCycleOscillator == null) {
                    continue;
                }
                keyCycleOscillator.setPoint(this.mFramePosition, this.mWaveShape, this.mCustomWaveShape, -1, this.mWavePeriod, this.mWaveOffset, this.mWavePhase, customVariable.getValueToInterpolate(), customVariable);
            }
            else {
                final float value = this.getValue(s);
                if (Float.isNaN(value)) {
                    continue;
                }
                final KeyCycleOscillator keyCycleOscillator2 = hashMap.get(s);
                if (keyCycleOscillator2 == null) {
                    continue;
                }
                keyCycleOscillator2.setPoint(this.mFramePosition, this.mWaveShape, this.mCustomWaveShape, -1, this.mWavePeriod, this.mWaveOffset, this.mWavePhase, value);
            }
        }
    }
    
    @Override
    public void addValues(final HashMap<String, SplineSet> hashMap) {
    }
    
    @Override
    public MotionKey clone() {
        return null;
    }
    
    public void dump() {
        final PrintStream out = System.out;
        final StringBuilder sb = new StringBuilder();
        sb.append("MotionKeyCycle{mWaveShape=");
        sb.append(this.mWaveShape);
        sb.append(", mWavePeriod=");
        sb.append(this.mWavePeriod);
        sb.append(", mWaveOffset=");
        sb.append(this.mWaveOffset);
        sb.append(", mWavePhase=");
        sb.append(this.mWavePhase);
        sb.append(", mRotation=");
        sb.append(this.mRotation);
        sb.append('}');
        out.println(sb.toString());
    }
    
    @Override
    public void getAttributeNames(final HashSet<String> set) {
        if (!Float.isNaN(this.mAlpha)) {
            set.add("alpha");
        }
        if (!Float.isNaN(this.mElevation)) {
            set.add("elevation");
        }
        if (!Float.isNaN(this.mRotation)) {
            set.add("rotationZ");
        }
        if (!Float.isNaN(this.mRotationX)) {
            set.add("rotationX");
        }
        if (!Float.isNaN(this.mRotationY)) {
            set.add("rotationY");
        }
        if (!Float.isNaN(this.mScaleX)) {
            set.add("scaleX");
        }
        if (!Float.isNaN(this.mScaleY)) {
            set.add("scaleY");
        }
        if (!Float.isNaN(this.mTransitionPathRotate)) {
            set.add("pathRotate");
        }
        if (!Float.isNaN(this.mTranslationX)) {
            set.add("translationX");
        }
        if (!Float.isNaN(this.mTranslationY)) {
            set.add("translationY");
        }
        if (!Float.isNaN(this.mTranslationZ)) {
            set.add("translationZ");
        }
        if (this.mCustom.size() > 0) {
            for (final String str : this.mCustom.keySet()) {
                final StringBuilder sb = new StringBuilder();
                sb.append("CUSTOM,");
                sb.append(str);
                set.add(sb.toString());
            }
        }
    }
    
    @Override
    public int getId(final String s) {
        s.hashCode();
        int n = 0;
        Label_0578: {
            switch (s) {
                case "visibility": {
                    n = 20;
                    break Label_0578;
                }
                case "waveShape": {
                    n = 19;
                    break Label_0578;
                }
                case "pathRotate": {
                    n = 18;
                    break Label_0578;
                }
                case "curveFit": {
                    n = 17;
                    break Label_0578;
                }
                case "phase": {
                    n = 16;
                    break Label_0578;
                }
                case "alpha": {
                    n = 15;
                    break Label_0578;
                }
                case "scaleY": {
                    n = 14;
                    break Label_0578;
                }
                case "scaleX": {
                    n = 13;
                    break Label_0578;
                }
                case "pivotY": {
                    n = 12;
                    break Label_0578;
                }
                case "pivotX": {
                    n = 11;
                    break Label_0578;
                }
                case "period": {
                    n = 10;
                    break Label_0578;
                }
                case "progress": {
                    n = 9;
                    break Label_0578;
                }
                case "offset": {
                    n = 8;
                    break Label_0578;
                }
                case "translationZ": {
                    n = 7;
                    break Label_0578;
                }
                case "translationY": {
                    n = 6;
                    break Label_0578;
                }
                case "translationX": {
                    n = 5;
                    break Label_0578;
                }
                case "rotationZ": {
                    n = 4;
                    break Label_0578;
                }
                case "rotationY": {
                    n = 3;
                    break Label_0578;
                }
                case "rotationX": {
                    n = 2;
                    break Label_0578;
                }
                case "easing": {
                    n = 1;
                    break Label_0578;
                }
                case "customWave": {
                    n = 0;
                    break Label_0578;
                }
                default:
                    break;
            }
            n = -1;
        }
        switch (n) {
            default: {
                return -1;
            }
            case 20: {
                return 402;
            }
            case 19: {
                return 421;
            }
            case 18: {
                return 416;
            }
            case 17: {
                return 401;
            }
            case 16: {
                return 425;
            }
            case 15: {
                return 403;
            }
            case 14: {
                return 312;
            }
            case 13: {
                return 311;
            }
            case 12: {
                return 314;
            }
            case 11: {
                return 313;
            }
            case 10: {
                return 423;
            }
            case 9: {
                return 315;
            }
            case 8: {
                return 424;
            }
            case 7: {
                return 306;
            }
            case 6: {
                return 305;
            }
            case 5: {
                return 304;
            }
            case 4: {
                return 310;
            }
            case 3: {
                return 309;
            }
            case 2: {
                return 308;
            }
            case 1: {
                return 420;
            }
            case 0: {
                return 422;
            }
        }
    }
    
    public float getValue(final String s) {
        s.hashCode();
        final int hashCode = s.hashCode();
        int n = -1;
        switch (hashCode) {
            case 803192288: {
                if (!s.equals("pathRotate")) {
                    break;
                }
                n = 13;
                break;
            }
            case 106629499: {
                if (!s.equals("phase")) {
                    break;
                }
                n = 12;
                break;
            }
            case 92909918: {
                if (!s.equals("alpha")) {
                    break;
                }
                n = 11;
                break;
            }
            case -4379043: {
                if (!s.equals("elevation")) {
                    break;
                }
                n = 10;
                break;
            }
            case -908189617: {
                if (!s.equals("scaleY")) {
                    break;
                }
                n = 9;
                break;
            }
            case -908189618: {
                if (!s.equals("scaleX")) {
                    break;
                }
                n = 8;
                break;
            }
            case -1001078227: {
                if (!s.equals("progress")) {
                    break;
                }
                n = 7;
                break;
            }
            case -1019779949: {
                if (!s.equals("offset")) {
                    break;
                }
                n = 6;
                break;
            }
            case -1225497655: {
                if (!s.equals("translationZ")) {
                    break;
                }
                n = 5;
                break;
            }
            case -1225497656: {
                if (!s.equals("translationY")) {
                    break;
                }
                n = 4;
                break;
            }
            case -1225497657: {
                if (!s.equals("translationX")) {
                    break;
                }
                n = 3;
                break;
            }
            case -1249320804: {
                if (!s.equals("rotationZ")) {
                    break;
                }
                n = 2;
                break;
            }
            case -1249320805: {
                if (!s.equals("rotationY")) {
                    break;
                }
                n = 1;
                break;
            }
            case -1249320806: {
                if (!s.equals("rotationX")) {
                    break;
                }
                n = 0;
                break;
            }
        }
        switch (n) {
            default: {
                return Float.NaN;
            }
            case 13: {
                return this.mTransitionPathRotate;
            }
            case 12: {
                return this.mWavePhase;
            }
            case 11: {
                return this.mAlpha;
            }
            case 10: {
                return this.mElevation;
            }
            case 9: {
                return this.mScaleY;
            }
            case 8: {
                return this.mScaleX;
            }
            case 7: {
                return this.mProgress;
            }
            case 6: {
                return this.mWaveOffset;
            }
            case 5: {
                return this.mTranslationZ;
            }
            case 4: {
                return this.mTranslationY;
            }
            case 3: {
                return this.mTranslationX;
            }
            case 2: {
                return this.mRotation;
            }
            case 1: {
                return this.mRotationY;
            }
            case 0: {
                return this.mRotationX;
            }
        }
    }
    
    public void printAttributes() {
        final HashSet set = new HashSet();
        this.getAttributeNames(set);
        final StringBuilder sb = new StringBuilder();
        sb.append(" ------------- ");
        sb.append(this.mFramePosition);
        sb.append(" -------------");
        Utils.log(sb.toString());
        final StringBuilder sb2 = new StringBuilder();
        sb2.append("MotionKeyCycle{Shape=");
        sb2.append(this.mWaveShape);
        sb2.append(", Period=");
        sb2.append(this.mWavePeriod);
        sb2.append(", Offset=");
        sb2.append(this.mWaveOffset);
        sb2.append(", Phase=");
        sb2.append(this.mWavePhase);
        sb2.append('}');
        Utils.log(sb2.toString());
        int i = 0;
        for (String[] array = set.toArray(new String[0]); i < array.length; ++i) {
            TypedValues$AttributesType$_CC.getId(array[i]);
            final StringBuilder sb3 = new StringBuilder();
            sb3.append(array[i]);
            sb3.append(":");
            sb3.append(this.getValue(array[i]));
            Utils.log(sb3.toString());
        }
    }
    
    @Override
    public boolean setValue(final int n, final float mProgress) {
        Label_0224: {
            if (n != 315) {
                if (n != 403) {
                    if (n != 416) {
                        switch (n) {
                            default: {
                                switch (n) {
                                    default: {
                                        return super.setValue(n, mProgress);
                                    }
                                    case 425: {
                                        this.mWavePhase = mProgress;
                                        break Label_0224;
                                    }
                                    case 424: {
                                        this.mWaveOffset = mProgress;
                                        break Label_0224;
                                    }
                                    case 423: {
                                        this.mWavePeriod = mProgress;
                                        break Label_0224;
                                    }
                                }
                                break;
                            }
                            case 312: {
                                this.mScaleY = mProgress;
                                break;
                            }
                            case 311: {
                                this.mScaleX = mProgress;
                                break;
                            }
                            case 310: {
                                this.mRotation = mProgress;
                                break;
                            }
                            case 309: {
                                this.mRotationY = mProgress;
                                break;
                            }
                            case 308: {
                                this.mRotationX = mProgress;
                                break;
                            }
                            case 307: {
                                this.mElevation = mProgress;
                                break;
                            }
                            case 306: {
                                this.mTranslationZ = mProgress;
                                break;
                            }
                            case 305: {
                                this.mTranslationY = mProgress;
                                break;
                            }
                            case 304: {
                                this.mTranslationX = mProgress;
                                break;
                            }
                        }
                    }
                    else {
                        this.mTransitionPathRotate = mProgress;
                    }
                }
                else {
                    this.mAlpha = mProgress;
                }
            }
            else {
                this.mProgress = mProgress;
            }
        }
        return true;
    }
    
    @Override
    public boolean setValue(final int n, final int n2) {
        if (n == 401) {
            this.mCurveFit = n2;
            return true;
        }
        if (n != 421) {
            return this.setValue(n, (float)n2) || super.setValue(n, n2);
        }
        this.mWaveShape = n2;
        return true;
    }
    
    @Override
    public boolean setValue(final int n, final String s) {
        if (n == 420) {
            this.mTransitionEasing = s;
            return true;
        }
        if (n != 422) {
            return super.setValue(n, s);
        }
        this.mCustomWaveShape = s;
        return true;
    }
}

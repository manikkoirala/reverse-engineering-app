// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.core.motion.key;

import androidx.constraintlayout.core.motion.utils.TypedValues$AttributesType$_CC;
import java.util.HashSet;
import java.io.PrintStream;
import java.util.Iterator;
import androidx.constraintlayout.core.motion.utils.SplineSet;
import androidx.constraintlayout.core.motion.CustomVariable;
import java.util.HashMap;

public class MotionKeyAttributes extends MotionKey
{
    private static final boolean DEBUG = false;
    public static final int KEY_TYPE = 1;
    static final String NAME = "KeyAttribute";
    private static final String TAG = "KeyAttributes";
    private float mAlpha;
    private int mCurveFit;
    private float mElevation;
    private float mPivotX;
    private float mPivotY;
    private float mProgress;
    private float mRotation;
    private float mRotationX;
    private float mRotationY;
    private float mScaleX;
    private float mScaleY;
    private String mTransitionEasing;
    private float mTransitionPathRotate;
    private float mTranslationX;
    private float mTranslationY;
    private float mTranslationZ;
    private int mVisibility;
    
    public MotionKeyAttributes() {
        this.mCurveFit = -1;
        this.mVisibility = 0;
        this.mAlpha = Float.NaN;
        this.mElevation = Float.NaN;
        this.mRotation = Float.NaN;
        this.mRotationX = Float.NaN;
        this.mRotationY = Float.NaN;
        this.mPivotX = Float.NaN;
        this.mPivotY = Float.NaN;
        this.mTransitionPathRotate = Float.NaN;
        this.mScaleX = Float.NaN;
        this.mScaleY = Float.NaN;
        this.mTranslationX = Float.NaN;
        this.mTranslationY = Float.NaN;
        this.mTranslationZ = Float.NaN;
        this.mProgress = Float.NaN;
        this.mType = 1;
        this.mCustom = new HashMap<String, CustomVariable>();
    }
    
    private float getFloatValue(final int n) {
        if (n == 100) {
            return (float)this.mFramePosition;
        }
        switch (n) {
            default: {
                return Float.NaN;
            }
            case 316: {
                return this.mTransitionPathRotate;
            }
            case 315: {
                return this.mProgress;
            }
            case 314: {
                return this.mPivotY;
            }
            case 313: {
                return this.mPivotX;
            }
            case 312: {
                return this.mScaleY;
            }
            case 311: {
                return this.mScaleX;
            }
            case 310: {
                return this.mRotation;
            }
            case 309: {
                return this.mRotationY;
            }
            case 308: {
                return this.mRotationX;
            }
            case 307: {
                return this.mElevation;
            }
            case 306: {
                return this.mTranslationZ;
            }
            case 305: {
                return this.mTranslationY;
            }
            case 304: {
                return this.mTranslationX;
            }
            case 303: {
                return this.mAlpha;
            }
        }
    }
    
    @Override
    public void addValues(final HashMap<String, SplineSet> hashMap) {
        for (final String s : hashMap.keySet()) {
            final SplineSet set = hashMap.get(s);
            if (set == null) {
                continue;
            }
            final boolean startsWith = s.startsWith("CUSTOM");
            int n = 7;
            if (startsWith) {
                final CustomVariable customVariable = this.mCustom.get(s.substring(7));
                if (customVariable == null) {
                    continue;
                }
                ((SplineSet.CustomSpline)set).setPoint(this.mFramePosition, customVariable);
            }
            else {
                s.hashCode();
                Label_0500: {
                    switch (s) {
                        case "pathRotate": {
                            n = 13;
                            break Label_0500;
                        }
                        case "alpha": {
                            n = 12;
                            break Label_0500;
                        }
                        case "elevation": {
                            n = 11;
                            break Label_0500;
                        }
                        case "scaleY": {
                            n = 10;
                            break Label_0500;
                        }
                        case "scaleX": {
                            n = 9;
                            break Label_0500;
                        }
                        case "pivotY": {
                            n = 8;
                            break Label_0500;
                        }
                        case "pivotX": {
                            break Label_0500;
                        }
                        case "progress": {
                            n = 6;
                            break Label_0500;
                        }
                        case "translationZ": {
                            n = 5;
                            break Label_0500;
                        }
                        case "translationY": {
                            n = 4;
                            break Label_0500;
                        }
                        case "translationX": {
                            n = 3;
                            break Label_0500;
                        }
                        case "rotationZ": {
                            n = 2;
                            break Label_0500;
                        }
                        case "rotationY": {
                            n = 1;
                            break Label_0500;
                        }
                        case "rotationX": {
                            n = 0;
                            break Label_0500;
                        }
                        default:
                            break;
                    }
                    n = -1;
                }
                switch (n) {
                    default: {
                        final PrintStream err = System.err;
                        final StringBuilder sb = new StringBuilder();
                        sb.append("not supported by KeyAttributes ");
                        sb.append(s);
                        err.println(sb.toString());
                        continue;
                    }
                    case 13: {
                        if (!Float.isNaN(this.mTransitionPathRotate)) {
                            set.setPoint(this.mFramePosition, this.mTransitionPathRotate);
                            continue;
                        }
                        continue;
                    }
                    case 12: {
                        if (!Float.isNaN(this.mAlpha)) {
                            set.setPoint(this.mFramePosition, this.mAlpha);
                            continue;
                        }
                        continue;
                    }
                    case 11: {
                        if (!Float.isNaN(this.mElevation)) {
                            set.setPoint(this.mFramePosition, this.mElevation);
                            continue;
                        }
                        continue;
                    }
                    case 10: {
                        if (!Float.isNaN(this.mScaleY)) {
                            set.setPoint(this.mFramePosition, this.mScaleY);
                            continue;
                        }
                        continue;
                    }
                    case 9: {
                        if (!Float.isNaN(this.mScaleX)) {
                            set.setPoint(this.mFramePosition, this.mScaleX);
                            continue;
                        }
                        continue;
                    }
                    case 8: {
                        if (!Float.isNaN(this.mRotationY)) {
                            set.setPoint(this.mFramePosition, this.mPivotY);
                            continue;
                        }
                        continue;
                    }
                    case 7: {
                        if (!Float.isNaN(this.mRotationX)) {
                            set.setPoint(this.mFramePosition, this.mPivotX);
                            continue;
                        }
                        continue;
                    }
                    case 6: {
                        if (!Float.isNaN(this.mProgress)) {
                            set.setPoint(this.mFramePosition, this.mProgress);
                            continue;
                        }
                        continue;
                    }
                    case 5: {
                        if (!Float.isNaN(this.mTranslationZ)) {
                            set.setPoint(this.mFramePosition, this.mTranslationZ);
                            continue;
                        }
                        continue;
                    }
                    case 4: {
                        if (!Float.isNaN(this.mTranslationY)) {
                            set.setPoint(this.mFramePosition, this.mTranslationY);
                            continue;
                        }
                        continue;
                    }
                    case 3: {
                        if (!Float.isNaN(this.mTranslationX)) {
                            set.setPoint(this.mFramePosition, this.mTranslationX);
                            continue;
                        }
                        continue;
                    }
                    case 2: {
                        if (!Float.isNaN(this.mRotation)) {
                            set.setPoint(this.mFramePosition, this.mRotation);
                            continue;
                        }
                        continue;
                    }
                    case 1: {
                        if (!Float.isNaN(this.mRotationY)) {
                            set.setPoint(this.mFramePosition, this.mRotationY);
                            continue;
                        }
                        continue;
                    }
                    case 0: {
                        if (!Float.isNaN(this.mRotationX)) {
                            set.setPoint(this.mFramePosition, this.mRotationX);
                            continue;
                        }
                        continue;
                    }
                }
            }
        }
    }
    
    @Override
    public MotionKey clone() {
        return null;
    }
    
    @Override
    public void getAttributeNames(final HashSet<String> set) {
        if (!Float.isNaN(this.mAlpha)) {
            set.add("alpha");
        }
        if (!Float.isNaN(this.mElevation)) {
            set.add("elevation");
        }
        if (!Float.isNaN(this.mRotation)) {
            set.add("rotationZ");
        }
        if (!Float.isNaN(this.mRotationX)) {
            set.add("rotationX");
        }
        if (!Float.isNaN(this.mRotationY)) {
            set.add("rotationY");
        }
        if (!Float.isNaN(this.mPivotX)) {
            set.add("pivotX");
        }
        if (!Float.isNaN(this.mPivotY)) {
            set.add("pivotY");
        }
        if (!Float.isNaN(this.mTranslationX)) {
            set.add("translationX");
        }
        if (!Float.isNaN(this.mTranslationY)) {
            set.add("translationY");
        }
        if (!Float.isNaN(this.mTranslationZ)) {
            set.add("translationZ");
        }
        if (!Float.isNaN(this.mTransitionPathRotate)) {
            set.add("pathRotate");
        }
        if (!Float.isNaN(this.mScaleX)) {
            set.add("scaleX");
        }
        if (!Float.isNaN(this.mScaleY)) {
            set.add("scaleY");
        }
        if (!Float.isNaN(this.mProgress)) {
            set.add("progress");
        }
        if (this.mCustom.size() > 0) {
            for (final String str : this.mCustom.keySet()) {
                final StringBuilder sb = new StringBuilder();
                sb.append("CUSTOM,");
                sb.append(str);
                set.add(sb.toString());
            }
        }
    }
    
    public int getCurveFit() {
        return this.mCurveFit;
    }
    
    @Override
    public int getId(final String s) {
        return TypedValues$AttributesType$_CC.getId(s);
    }
    
    public void printAttributes() {
        final HashSet set = new HashSet();
        this.getAttributeNames(set);
        final PrintStream out = System.out;
        final StringBuilder sb = new StringBuilder();
        sb.append(" ------------- ");
        sb.append(this.mFramePosition);
        sb.append(" -------------");
        out.println(sb.toString());
        int i = 0;
        for (String[] array = set.toArray(new String[0]); i < array.length; ++i) {
            final int id = TypedValues$AttributesType$_CC.getId(array[i]);
            final PrintStream out2 = System.out;
            final StringBuilder sb2 = new StringBuilder();
            sb2.append(array[i]);
            sb2.append(":");
            sb2.append(this.getFloatValue(id));
            out2.println(sb2.toString());
        }
    }
    
    @Override
    public void setInterpolation(final HashMap<String, Integer> hashMap) {
        if (!Float.isNaN(this.mAlpha)) {
            hashMap.put("alpha", this.mCurveFit);
        }
        if (!Float.isNaN(this.mElevation)) {
            hashMap.put("elevation", this.mCurveFit);
        }
        if (!Float.isNaN(this.mRotation)) {
            hashMap.put("rotationZ", this.mCurveFit);
        }
        if (!Float.isNaN(this.mRotationX)) {
            hashMap.put("rotationX", this.mCurveFit);
        }
        if (!Float.isNaN(this.mRotationY)) {
            hashMap.put("rotationY", this.mCurveFit);
        }
        if (!Float.isNaN(this.mPivotX)) {
            hashMap.put("pivotX", this.mCurveFit);
        }
        if (!Float.isNaN(this.mPivotY)) {
            hashMap.put("pivotY", this.mCurveFit);
        }
        if (!Float.isNaN(this.mTranslationX)) {
            hashMap.put("translationX", this.mCurveFit);
        }
        if (!Float.isNaN(this.mTranslationY)) {
            hashMap.put("translationY", this.mCurveFit);
        }
        if (!Float.isNaN(this.mTranslationZ)) {
            hashMap.put("translationZ", this.mCurveFit);
        }
        if (!Float.isNaN(this.mTransitionPathRotate)) {
            hashMap.put("pathRotate", this.mCurveFit);
        }
        if (!Float.isNaN(this.mScaleX)) {
            hashMap.put("scaleX", this.mCurveFit);
        }
        if (!Float.isNaN(this.mScaleY)) {
            hashMap.put("scaleY", this.mCurveFit);
        }
        if (!Float.isNaN(this.mProgress)) {
            hashMap.put("progress", this.mCurveFit);
        }
        if (this.mCustom.size() > 0) {
            for (final String str : this.mCustom.keySet()) {
                final StringBuilder sb = new StringBuilder();
                sb.append("CUSTOM,");
                sb.append(str);
                hashMap.put(sb.toString(), this.mCurveFit);
            }
        }
    }
    
    @Override
    public boolean setValue(final int n, final float mTransitionPathRotate) {
        if (n != 100) {
            switch (n) {
                default: {
                    return super.setValue(n, mTransitionPathRotate);
                }
                case 316: {
                    this.mTransitionPathRotate = mTransitionPathRotate;
                    break;
                }
                case 315: {
                    this.mProgress = mTransitionPathRotate;
                    break;
                }
                case 314: {
                    this.mPivotY = mTransitionPathRotate;
                    break;
                }
                case 313: {
                    this.mPivotX = mTransitionPathRotate;
                    break;
                }
                case 312: {
                    this.mScaleY = mTransitionPathRotate;
                    break;
                }
                case 311: {
                    this.mScaleX = mTransitionPathRotate;
                    break;
                }
                case 310: {
                    this.mRotation = mTransitionPathRotate;
                    break;
                }
                case 309: {
                    this.mRotationY = mTransitionPathRotate;
                    break;
                }
                case 308: {
                    this.mRotationX = mTransitionPathRotate;
                    break;
                }
                case 307: {
                    this.mElevation = mTransitionPathRotate;
                    break;
                }
                case 306: {
                    this.mTranslationZ = mTransitionPathRotate;
                    break;
                }
                case 305: {
                    this.mTranslationY = mTransitionPathRotate;
                    break;
                }
                case 304: {
                    this.mTranslationX = mTransitionPathRotate;
                    break;
                }
                case 303: {
                    this.mAlpha = mTransitionPathRotate;
                    break;
                }
            }
        }
        else {
            this.mTransitionPathRotate = mTransitionPathRotate;
        }
        return true;
    }
    
    @Override
    public boolean setValue(final int n, final int mFramePosition) {
        if (n != 100) {
            if (n != 301) {
                if (n != 302) {
                    if (!this.setValue(n, mFramePosition)) {
                        return super.setValue(n, mFramePosition);
                    }
                }
                else {
                    this.mVisibility = mFramePosition;
                }
            }
            else {
                this.mCurveFit = mFramePosition;
            }
        }
        else {
            this.mFramePosition = mFramePosition;
        }
        return true;
    }
    
    @Override
    public boolean setValue(final int n, final String s) {
        if (n != 101) {
            if (n != 317) {
                return super.setValue(n, s);
            }
            this.mTransitionEasing = s;
        }
        else {
            this.mTargetString = s;
        }
        return true;
    }
}

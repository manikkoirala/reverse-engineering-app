// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.core.motion;

import java.util.Arrays;
import java.util.Iterator;
import androidx.constraintlayout.core.motion.key.MotionKeyPosition;
import androidx.constraintlayout.core.motion.utils.Easing;
import java.util.HashMap;

public class MotionPaths implements Comparable<MotionPaths>
{
    public static final int CARTESIAN = 0;
    public static final boolean DEBUG = false;
    static final int OFF_HEIGHT = 4;
    static final int OFF_PATH_ROTATE = 5;
    static final int OFF_POSITION = 0;
    static final int OFF_WIDTH = 3;
    static final int OFF_X = 1;
    static final int OFF_Y = 2;
    public static final boolean OLD_WAY = false;
    public static final int PERPENDICULAR = 1;
    public static final int SCREEN = 2;
    public static final String TAG = "MotionPaths";
    static String[] names;
    HashMap<String, CustomVariable> customAttributes;
    float height;
    int mAnimateCircleAngleTo;
    int mAnimateRelativeTo;
    int mDrawPath;
    Easing mKeyFrameEasing;
    int mMode;
    int mPathMotionArc;
    float mPathRotate;
    float mProgress;
    float mRelativeAngle;
    Motion mRelativeToController;
    double[] mTempDelta;
    double[] mTempValue;
    float position;
    float time;
    float width;
    float x;
    float y;
    
    static {
        MotionPaths.names = new String[] { "position", "x", "y", "width", "height", "pathRotate" };
    }
    
    public MotionPaths() {
        this.mDrawPath = 0;
        this.mPathRotate = Float.NaN;
        this.mProgress = Float.NaN;
        this.mPathMotionArc = -1;
        this.mAnimateRelativeTo = -1;
        this.mRelativeAngle = Float.NaN;
        this.mRelativeToController = null;
        this.customAttributes = new HashMap<String, CustomVariable>();
        this.mMode = 0;
        this.mTempValue = new double[18];
        this.mTempDelta = new double[18];
    }
    
    public MotionPaths(final int n, final int n2, final MotionKeyPosition motionKeyPosition, final MotionPaths motionPaths, final MotionPaths motionPaths2) {
        this.mDrawPath = 0;
        this.mPathRotate = Float.NaN;
        this.mProgress = Float.NaN;
        this.mPathMotionArc = -1;
        this.mAnimateRelativeTo = -1;
        this.mRelativeAngle = Float.NaN;
        this.mRelativeToController = null;
        this.customAttributes = new HashMap<String, CustomVariable>();
        this.mMode = 0;
        this.mTempValue = new double[18];
        this.mTempDelta = new double[18];
        if (motionPaths.mAnimateRelativeTo != -1) {
            this.initPolar(n, n2, motionKeyPosition, motionPaths, motionPaths2);
            return;
        }
        final int mPositionType = motionKeyPosition.mPositionType;
        if (mPositionType == 1) {
            this.initPath(motionKeyPosition, motionPaths, motionPaths2);
            return;
        }
        if (mPositionType != 2) {
            this.initCartesian(motionKeyPosition, motionPaths, motionPaths2);
            return;
        }
        this.initScreen(n, n2, motionKeyPosition, motionPaths, motionPaths2);
    }
    
    private boolean diff(final float n, final float n2) {
        final boolean naN = Float.isNaN(n);
        final boolean b = true;
        boolean b2 = true;
        if (!naN && !Float.isNaN(n2)) {
            if (Math.abs(n - n2) <= 1.0E-6f) {
                b2 = false;
            }
            return b2;
        }
        return Float.isNaN(n) != Float.isNaN(n2) && b;
    }
    
    private static final float xRotate(final float n, final float n2, final float n3, final float n4, final float n5, final float n6) {
        return (n5 - n3) * n2 - (n6 - n4) * n + n3;
    }
    
    private static final float yRotate(final float n, final float n2, final float n3, final float n4, final float n5, final float n6) {
        return (n5 - n3) * n + (n6 - n4) * n2 + n4;
    }
    
    public void applyParameters(final MotionWidget motionWidget) {
        this.mKeyFrameEasing = Easing.getInterpolator(motionWidget.motion.mTransitionEasing);
        this.mPathMotionArc = motionWidget.motion.mPathMotionArc;
        this.mAnimateRelativeTo = motionWidget.motion.mAnimateRelativeTo;
        this.mPathRotate = motionWidget.motion.mPathRotate;
        this.mDrawPath = motionWidget.motion.mDrawPath;
        this.mAnimateCircleAngleTo = motionWidget.motion.mAnimateCircleAngleTo;
        this.mProgress = motionWidget.propertySet.mProgress;
        this.mRelativeAngle = 0.0f;
        for (final String key : motionWidget.getCustomAttributeNames()) {
            final CustomVariable customAttribute = motionWidget.getCustomAttribute(key);
            if (customAttribute != null && customAttribute.isContinuous()) {
                this.customAttributes.put(key, customAttribute);
            }
        }
    }
    
    @Override
    public int compareTo(final MotionPaths motionPaths) {
        return Float.compare(this.position, motionPaths.position);
    }
    
    public void configureRelativeTo(final Motion motion) {
        motion.getPos(this.mProgress);
    }
    
    void different(final MotionPaths motionPaths, final boolean[] array, final String[] array2, final boolean b) {
        final boolean diff = this.diff(this.x, motionPaths.x);
        final boolean diff2 = this.diff(this.y, motionPaths.y);
        array[0] |= this.diff(this.position, motionPaths.position);
        final boolean b2 = array[1];
        final boolean b3 = diff | diff2 | b;
        array[1] = (b2 | b3);
        array[2] |= b3;
        array[3] |= this.diff(this.width, motionPaths.width);
        array[4] |= this.diff(this.height, motionPaths.height);
    }
    
    void fillStandard(final double[] array, final int[] array2) {
        final float position = this.position;
        int i = 0;
        final float x = this.x;
        final float y = this.y;
        final float width = this.width;
        final float height = this.height;
        final float mPathRotate = this.mPathRotate;
        int n = 0;
        while (i < array2.length) {
            final int n2 = array2[i];
            int n3 = n;
            if (n2 < 6) {
                array[n] = (new float[] { position, x, y, width, height, mPathRotate })[n2];
                n3 = n + 1;
            }
            ++i;
            n = n3;
        }
    }
    
    void getBounds(final int[] array, final double[] array2, final float[] array3, final int n) {
        float width = this.width;
        float height = this.height;
        for (int i = 0; i < array.length; ++i) {
            final float n2 = (float)array2[i];
            final int n3 = array[i];
            if (n3 != 3) {
                if (n3 == 4) {
                    height = n2;
                }
            }
            else {
                width = n2;
            }
        }
        array3[n] = width;
        array3[n + 1] = height;
    }
    
    void getCenter(double n, final int[] array, final double[] array2, final float[] array3, final int n2) {
        float x = this.x;
        float y = this.y;
        float width = this.width;
        float height = this.height;
        for (int i = 0; i < array.length; ++i) {
            final float n3 = (float)array2[i];
            final int n4 = array[i];
            if (n4 != 1) {
                if (n4 != 2) {
                    if (n4 != 3) {
                        if (n4 == 4) {
                            height = n3;
                        }
                    }
                    else {
                        width = n3;
                    }
                }
                else {
                    y = n3;
                }
            }
            else {
                x = n3;
            }
        }
        final Motion mRelativeToController = this.mRelativeToController;
        float n5 = x;
        float n6 = y;
        if (mRelativeToController != null) {
            final float[] array4 = new float[2];
            mRelativeToController.getCenter(n, array4, new float[2]);
            final float n7 = array4[0];
            final float n8 = array4[1];
            final double n9 = n7;
            final double n10 = x;
            n = y;
            n5 = (float)(n9 + Math.sin(n) * n10 - width / 2.0f);
            n6 = (float)(n8 - n10 * Math.cos(n) - height / 2.0f);
        }
        array3[n2] = n5 + width / 2.0f + 0.0f;
        array3[n2 + 1] = n6 + height / 2.0f + 0.0f;
    }
    
    void getCenter(double n, final int[] array, final double[] array2, final float[] array3, final double[] array4, final float[] array5) {
        float x = this.x;
        float y = this.y;
        float width = this.width;
        float height = this.height;
        int i = 0;
        float n2 = 0.0f;
        float n3 = 0.0f;
        float n4 = 0.0f;
        float n5 = 0.0f;
        while (i < array.length) {
            final float n6 = (float)array2[i];
            final float n7 = (float)array4[i];
            final int n8 = array[i];
            if (n8 != 1) {
                if (n8 != 2) {
                    if (n8 != 3) {
                        if (n8 == 4) {
                            height = n6;
                            n5 = n7;
                        }
                    }
                    else {
                        width = n6;
                        n3 = n7;
                    }
                }
                else {
                    y = n6;
                    n4 = n7;
                }
            }
            else {
                n2 = n7;
                x = n6;
            }
            ++i;
        }
        float n9 = n3 / 2.0f + n2;
        float n10 = n5 / 2.0f + n4;
        final Motion mRelativeToController = this.mRelativeToController;
        if (mRelativeToController != null) {
            final float[] array6 = new float[2];
            final float[] array7 = new float[2];
            mRelativeToController.getCenter(n, array6, array7);
            final float n11 = array6[0];
            final float n12 = array6[1];
            final float n13 = array7[0];
            final float n14 = array7[1];
            final double n15 = n11;
            final double n16 = x;
            n = y;
            x = (float)(n15 + Math.sin(n) * n16 - width / 2.0f);
            y = (float)(n12 - n16 * Math.cos(n) - height / 2.0f);
            final double n17 = n13;
            final double n18 = n2;
            final double sin = Math.sin(n);
            final double cos = Math.cos(n);
            final double n19 = n4;
            n9 = (float)(n17 + sin * n18 + cos * n19);
            n10 = (float)(n14 - n18 * Math.cos(n) + Math.sin(n) * n19);
        }
        array3[0] = x + width / 2.0f + 0.0f;
        array3[1] = y + height / 2.0f + 0.0f;
        array5[0] = n9;
        array5[1] = n10;
    }
    
    void getCenterVelocity(double n, final int[] array, final double[] array2, final float[] array3, final int n2) {
        float x = this.x;
        float y = this.y;
        float width = this.width;
        float height = this.height;
        for (int i = 0; i < array.length; ++i) {
            final float n3 = (float)array2[i];
            final int n4 = array[i];
            if (n4 != 1) {
                if (n4 != 2) {
                    if (n4 != 3) {
                        if (n4 == 4) {
                            height = n3;
                        }
                    }
                    else {
                        width = n3;
                    }
                }
                else {
                    y = n3;
                }
            }
            else {
                x = n3;
            }
        }
        final Motion mRelativeToController = this.mRelativeToController;
        float n5 = x;
        float n6 = y;
        if (mRelativeToController != null) {
            final float[] array4 = new float[2];
            mRelativeToController.getCenter(n, array4, new float[2]);
            final float n7 = array4[0];
            final float n8 = array4[1];
            final double n9 = n7;
            final double n10 = x;
            n = y;
            final float n11 = (float)(n9 + Math.sin(n) * n10 - width / 2.0f);
            n6 = (float)(n8 - n10 * Math.cos(n) - height / 2.0f);
            n5 = n11;
        }
        array3[n2] = n5 + width / 2.0f + 0.0f;
        array3[n2 + 1] = n6 + height / 2.0f + 0.0f;
    }
    
    int getCustomData(final String key, final double[] array, int n) {
        final CustomVariable customVariable = this.customAttributes.get(key);
        int i = 0;
        if (customVariable == null) {
            return 0;
        }
        if (customVariable.numberOfInterpolatedValues() == 1) {
            array[n] = customVariable.getValueToInterpolate();
            return 1;
        }
        final int numberOfInterpolatedValues = customVariable.numberOfInterpolatedValues();
        final float[] array2 = new float[numberOfInterpolatedValues];
        customVariable.getValuesToInterpolate(array2);
        while (i < numberOfInterpolatedValues) {
            array[n] = array2[i];
            ++i;
            ++n;
        }
        return numberOfInterpolatedValues;
    }
    
    int getCustomDataCount(final String key) {
        final CustomVariable customVariable = this.customAttributes.get(key);
        if (customVariable == null) {
            return 0;
        }
        return customVariable.numberOfInterpolatedValues();
    }
    
    void getRect(final int[] array, final double[] array2, final float[] array3, int n) {
        float x = this.x;
        float y = this.y;
        float width = this.width;
        float height = this.height;
        for (int i = 0; i < array.length; ++i) {
            final float n2 = (float)array2[i];
            final int n3 = array[i];
            if (n3 != 1) {
                if (n3 != 2) {
                    if (n3 != 3) {
                        if (n3 == 4) {
                            height = n2;
                        }
                    }
                    else {
                        width = n2;
                    }
                }
                else {
                    y = n2;
                }
            }
            else {
                x = n2;
            }
        }
        final Motion mRelativeToController = this.mRelativeToController;
        float n4 = x;
        float n5 = y;
        if (mRelativeToController != null) {
            final float centerX = mRelativeToController.getCenterX();
            final float centerY = this.mRelativeToController.getCenterY();
            final double n6 = centerX;
            final double n7 = x;
            final double n8 = y;
            n4 = (float)(n6 + Math.sin(n8) * n7 - width / 2.0f);
            n5 = (float)(centerY - n7 * Math.cos(n8) - height / 2.0f);
        }
        final float n9 = width + n4;
        final float n10 = height + n5;
        Float.isNaN(Float.NaN);
        Float.isNaN(Float.NaN);
        final int n11 = n + 1;
        array3[n] = n4 + 0.0f;
        n = n11 + 1;
        array3[n11] = n5 + 0.0f;
        final int n12 = n + 1;
        array3[n] = n9 + 0.0f;
        n = n12 + 1;
        array3[n12] = n5 + 0.0f;
        final int n13 = n + 1;
        array3[n] = n9 + 0.0f;
        n = n13 + 1;
        array3[n13] = n10 + 0.0f;
        array3[n] = n4 + 0.0f;
        array3[n + 1] = n10 + 0.0f;
    }
    
    boolean hasCustomData(final String key) {
        return this.customAttributes.containsKey(key);
    }
    
    void initCartesian(final MotionKeyPosition motionKeyPosition, final MotionPaths motionPaths, final MotionPaths motionPaths2) {
        float mPercentY = motionKeyPosition.mFramePosition / 100.0f;
        this.time = mPercentY;
        this.mDrawPath = motionKeyPosition.mDrawPath;
        float mPercentWidth;
        if (Float.isNaN(motionKeyPosition.mPercentWidth)) {
            mPercentWidth = mPercentY;
        }
        else {
            mPercentWidth = motionKeyPosition.mPercentWidth;
        }
        float mPercentHeight;
        if (Float.isNaN(motionKeyPosition.mPercentHeight)) {
            mPercentHeight = mPercentY;
        }
        else {
            mPercentHeight = motionKeyPosition.mPercentHeight;
        }
        final float width = motionPaths2.width;
        final float width2 = motionPaths.width;
        final float height = motionPaths2.height;
        final float height2 = motionPaths.height;
        this.position = this.time;
        final float x = motionPaths.x;
        final float n = width2 / 2.0f;
        final float y = motionPaths.y;
        final float n2 = height2 / 2.0f;
        final float x2 = motionPaths2.x;
        final float n3 = width / 2.0f;
        final float y2 = motionPaths2.y;
        final float n4 = height / 2.0f;
        final float n5 = x2 + n3 - (n + x);
        final float n6 = y2 + n4 - (y + n2);
        final float n7 = (width - width2) * mPercentWidth;
        final float n8 = n7 / 2.0f;
        this.x = (float)(int)(x + n5 * mPercentY - n8);
        final float n9 = (height - height2) * mPercentHeight;
        final float n10 = n9 / 2.0f;
        this.y = (float)(int)(y + n6 * mPercentY - n10);
        this.width = (float)(int)(width2 + n7);
        this.height = (float)(int)(height2 + n9);
        float mPercentX;
        if (Float.isNaN(motionKeyPosition.mPercentX)) {
            mPercentX = mPercentY;
        }
        else {
            mPercentX = motionKeyPosition.mPercentX;
        }
        final boolean naN = Float.isNaN(motionKeyPosition.mAltPercentY);
        float mAltPercentX = 0.0f;
        float mAltPercentY;
        if (naN) {
            mAltPercentY = 0.0f;
        }
        else {
            mAltPercentY = motionKeyPosition.mAltPercentY;
        }
        if (!Float.isNaN(motionKeyPosition.mPercentY)) {
            mPercentY = motionKeyPosition.mPercentY;
        }
        if (!Float.isNaN(motionKeyPosition.mAltPercentX)) {
            mAltPercentX = motionKeyPosition.mAltPercentX;
        }
        this.mMode = 0;
        this.x = (float)(int)(motionPaths.x + mPercentX * n5 + mAltPercentX * n6 - n8);
        this.y = (float)(int)(motionPaths.y + n5 * mAltPercentY + n6 * mPercentY - n10);
        this.mKeyFrameEasing = Easing.getInterpolator(motionKeyPosition.mTransitionEasing);
        this.mPathMotionArc = motionKeyPosition.mPathMotionArc;
    }
    
    void initPath(final MotionKeyPosition motionKeyPosition, final MotionPaths motionPaths, final MotionPaths motionPaths2) {
        float mPercentX = motionKeyPosition.mFramePosition / 100.0f;
        this.time = mPercentX;
        this.mDrawPath = motionKeyPosition.mDrawPath;
        float mPercentWidth;
        if (Float.isNaN(motionKeyPosition.mPercentWidth)) {
            mPercentWidth = mPercentX;
        }
        else {
            mPercentWidth = motionKeyPosition.mPercentWidth;
        }
        float mPercentHeight;
        if (Float.isNaN(motionKeyPosition.mPercentHeight)) {
            mPercentHeight = mPercentX;
        }
        else {
            mPercentHeight = motionKeyPosition.mPercentHeight;
        }
        final float width = motionPaths2.width;
        final float width2 = motionPaths.width;
        final float height = motionPaths2.height;
        final float height2 = motionPaths.height;
        this.position = this.time;
        if (!Float.isNaN(motionKeyPosition.mPercentX)) {
            mPercentX = motionKeyPosition.mPercentX;
        }
        final float x = motionPaths.x;
        final float width3 = motionPaths.width;
        final float n = width3 / 2.0f;
        final float y = motionPaths.y;
        final float height3 = motionPaths.height;
        final float n2 = height3 / 2.0f;
        final float x2 = motionPaths2.x;
        final float n3 = motionPaths2.width / 2.0f;
        final float y2 = motionPaths2.y;
        final float n4 = motionPaths2.height / 2.0f;
        final float n5 = x2 + n3 - (n + x);
        final float n6 = y2 + n4 - (n2 + y);
        final float n7 = n5 * mPercentX;
        final float n8 = (width - width2) * mPercentWidth;
        final float n9 = n8 / 2.0f;
        this.x = (float)(int)(x + n7 - n9);
        final float n10 = mPercentX * n6;
        final float n11 = (height - height2) * mPercentHeight;
        final float n12 = n11 / 2.0f;
        this.y = (float)(int)(y + n10 - n12);
        this.width = (float)(int)(width3 + n8);
        this.height = (float)(int)(height3 + n11);
        float mPercentY;
        if (Float.isNaN(motionKeyPosition.mPercentY)) {
            mPercentY = 0.0f;
        }
        else {
            mPercentY = motionKeyPosition.mPercentY;
        }
        final float n13 = -n6;
        this.mMode = 1;
        final float x3 = (float)(int)(motionPaths.x + n7 - n9);
        this.x = x3;
        final float n14 = (float)(int)(motionPaths.y + n10 - n12);
        this.x = x3 + n13 * mPercentY;
        this.y = n14 + n5 * mPercentY;
        this.mAnimateRelativeTo = this.mAnimateRelativeTo;
        this.mKeyFrameEasing = Easing.getInterpolator(motionKeyPosition.mTransitionEasing);
        this.mPathMotionArc = motionKeyPosition.mPathMotionArc;
    }
    
    void initPolar(int mPositionType, final int n, final MotionKeyPosition motionKeyPosition, final MotionPaths motionPaths, final MotionPaths motionPaths2) {
        float time = motionKeyPosition.mFramePosition / 100.0f;
        this.time = time;
        this.mDrawPath = motionKeyPosition.mDrawPath;
        this.mMode = motionKeyPosition.mPositionType;
        float mPercentWidth;
        if (Float.isNaN(motionKeyPosition.mPercentWidth)) {
            mPercentWidth = time;
        }
        else {
            mPercentWidth = motionKeyPosition.mPercentWidth;
        }
        float mPercentHeight;
        if (Float.isNaN(motionKeyPosition.mPercentHeight)) {
            mPercentHeight = time;
        }
        else {
            mPercentHeight = motionKeyPosition.mPercentHeight;
        }
        final float width = motionPaths2.width;
        final float width2 = motionPaths.width;
        final float height = motionPaths2.height;
        final float height2 = motionPaths.height;
        this.position = this.time;
        this.width = (float)(int)(width2 + (width - width2) * mPercentWidth);
        this.height = (float)(int)(height2 + (height - height2) * mPercentHeight);
        mPositionType = motionKeyPosition.mPositionType;
        if (mPositionType != 1) {
            if (mPositionType != 2) {
                float mPercentX;
                if (Float.isNaN(motionKeyPosition.mPercentX)) {
                    mPercentX = time;
                }
                else {
                    mPercentX = motionKeyPosition.mPercentX;
                }
                final float x = motionPaths2.x;
                final float x2 = motionPaths.x;
                this.x = mPercentX * (x - x2) + x2;
                if (!Float.isNaN(motionKeyPosition.mPercentY)) {
                    time = motionKeyPosition.mPercentY;
                }
                final float y = motionPaths2.y;
                final float y2 = motionPaths.y;
                this.y = time * (y - y2) + y2;
            }
            else {
                float x5;
                if (Float.isNaN(motionKeyPosition.mPercentX)) {
                    final float x3 = motionPaths2.x;
                    final float x4 = motionPaths.x;
                    x5 = (x3 - x4) * time + x4;
                }
                else {
                    x5 = Math.min(mPercentHeight, mPercentWidth) * motionKeyPosition.mPercentX;
                }
                this.x = x5;
                float mPercentY;
                if (Float.isNaN(motionKeyPosition.mPercentY)) {
                    final float y3 = motionPaths2.y;
                    final float y4 = motionPaths.y;
                    mPercentY = time * (y3 - y4) + y4;
                }
                else {
                    mPercentY = motionKeyPosition.mPercentY;
                }
                this.y = mPercentY;
            }
        }
        else {
            float mPercentX2;
            if (Float.isNaN(motionKeyPosition.mPercentX)) {
                mPercentX2 = time;
            }
            else {
                mPercentX2 = motionKeyPosition.mPercentX;
            }
            final float x6 = motionPaths2.x;
            final float x7 = motionPaths.x;
            this.x = mPercentX2 * (x6 - x7) + x7;
            if (!Float.isNaN(motionKeyPosition.mPercentY)) {
                time = motionKeyPosition.mPercentY;
            }
            final float y5 = motionPaths2.y;
            final float y6 = motionPaths.y;
            this.y = time * (y5 - y6) + y6;
        }
        this.mAnimateRelativeTo = motionPaths.mAnimateRelativeTo;
        this.mKeyFrameEasing = Easing.getInterpolator(motionKeyPosition.mTransitionEasing);
        this.mPathMotionArc = motionKeyPosition.mPathMotionArc;
    }
    
    void initScreen(int n, final int n2, final MotionKeyPosition motionKeyPosition, final MotionPaths motionPaths, final MotionPaths motionPaths2) {
        final float time = motionKeyPosition.mFramePosition / 100.0f;
        this.time = time;
        this.mDrawPath = motionKeyPosition.mDrawPath;
        float mPercentWidth;
        if (Float.isNaN(motionKeyPosition.mPercentWidth)) {
            mPercentWidth = time;
        }
        else {
            mPercentWidth = motionKeyPosition.mPercentWidth;
        }
        float mPercentHeight;
        if (Float.isNaN(motionKeyPosition.mPercentHeight)) {
            mPercentHeight = time;
        }
        else {
            mPercentHeight = motionKeyPosition.mPercentHeight;
        }
        final float width = motionPaths2.width;
        final float width2 = motionPaths.width;
        final float height = motionPaths2.height;
        final float height2 = motionPaths.height;
        this.position = this.time;
        final float x = motionPaths.x;
        final float n3 = width2 / 2.0f;
        final float y = motionPaths.y;
        final float n4 = height2 / 2.0f;
        final float x2 = motionPaths2.x;
        final float n5 = width / 2.0f;
        final float y2 = motionPaths2.y;
        final float n6 = height / 2.0f;
        final float n7 = (width - width2) * mPercentWidth;
        this.x = (float)(int)(x + (x2 + n5 - (n3 + x)) * time - n7 / 2.0f);
        final float n8 = (height - height2) * mPercentHeight;
        this.y = (float)(int)(y + (y2 + n6 - (y + n4)) * time - n8 / 2.0f);
        this.width = (float)(int)(width2 + n7);
        this.height = (float)(int)(height2 + n8);
        this.mMode = 2;
        if (!Float.isNaN(motionKeyPosition.mPercentX)) {
            n -= (int)this.width;
            this.x = (float)(int)(motionKeyPosition.mPercentX * n);
        }
        if (!Float.isNaN(motionKeyPosition.mPercentY)) {
            n = (int)(n2 - this.height);
            this.y = (float)(int)(motionKeyPosition.mPercentY * n);
        }
        this.mAnimateRelativeTo = this.mAnimateRelativeTo;
        this.mKeyFrameEasing = Easing.getInterpolator(motionKeyPosition.mTransitionEasing);
        this.mPathMotionArc = motionKeyPosition.mPathMotionArc;
    }
    
    void setBounds(final float x, final float y, final float width, final float height) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
    }
    
    void setDpDt(final float n, final float n2, final float[] array, final int[] array2, final double[] array3, final double[] array4) {
        int i = 0;
        float n3 = 0.0f;
        float n4 = 0.0f;
        float n5 = 0.0f;
        float n6 = 0.0f;
        while (i < array2.length) {
            final float n7 = (float)array3[i];
            final double n8 = array4[i];
            final int n9 = array2[i];
            if (n9 != 1) {
                if (n9 != 2) {
                    if (n9 != 3) {
                        if (n9 == 4) {
                            n6 = n7;
                        }
                    }
                    else {
                        n4 = n7;
                    }
                }
                else {
                    n5 = n7;
                }
            }
            else {
                n3 = n7;
            }
            ++i;
        }
        final float n10 = n3 - 0.0f * n4 / 2.0f;
        final float n11 = n5 - 0.0f * n6 / 2.0f;
        array[0] = n10 * (1.0f - n) + (n4 * 1.0f + n10) * n + 0.0f;
        array[1] = n11 * (1.0f - n2) + (n6 * 1.0f + n11) * n2 + 0.0f;
    }
    
    void setView(float n, final MotionWidget motionWidget, final int[] array, final double[] array2, final double[] array3, final double[] array4) {
        float x = this.x;
        float y = this.y;
        float width = this.width;
        float height = this.height;
        if (array.length != 0 && this.mTempValue.length <= array[array.length - 1]) {
            final int n2 = array[array.length - 1] + 1;
            this.mTempValue = new double[n2];
            this.mTempDelta = new double[n2];
        }
        Arrays.fill(this.mTempValue, Double.NaN);
        for (int i = 0; i < array.length; ++i) {
            final double[] mTempValue = this.mTempValue;
            final int n3 = array[i];
            mTempValue[n3] = array2[i];
            this.mTempDelta[n3] = array3[i];
        }
        float n4 = Float.NaN;
        int n5 = 0;
        float n6 = 0.0f;
        float n7 = 0.0f;
        float n8 = 0.0f;
        float n9 = 0.0f;
        while (true) {
            final double[] mTempValue2 = this.mTempValue;
            if (n5 >= mTempValue2.length) {
                break;
            }
            final boolean naN = Double.isNaN(mTempValue2[n5]);
            double n10 = 0.0;
            if (!naN || (array4 != null && array4[n5] != 0.0)) {
                if (array4 != null) {
                    n10 = array4[n5];
                }
                if (!Double.isNaN(this.mTempValue[n5])) {
                    n10 += this.mTempValue[n5];
                }
                final float n11 = (float)n10;
                final float n12 = (float)this.mTempDelta[n5];
                if (n5 != 1) {
                    if (n5 != 2) {
                        if (n5 != 3) {
                            if (n5 != 4) {
                                if (n5 == 5) {
                                    n4 = n11;
                                }
                            }
                            else {
                                height = n11;
                                n9 = n12;
                            }
                        }
                        else {
                            width = n11;
                            n8 = n12;
                        }
                    }
                    else {
                        y = n11;
                        n7 = n12;
                    }
                }
                else {
                    n6 = n12;
                    x = n11;
                }
            }
            ++n5;
        }
        final Motion mRelativeToController = this.mRelativeToController;
        float n19;
        if (mRelativeToController != null) {
            final float[] array5 = new float[2];
            final float[] array6 = new float[2];
            mRelativeToController.getCenter(n, array5, array6);
            final float n13 = array5[0];
            n = array5[1];
            final float n14 = array6[0];
            final float n15 = array6[1];
            final double n16 = n13;
            final double n17 = x;
            final double n18 = y;
            n19 = (float)(n16 + Math.sin(n18) * n17 - width / 2.0f);
            n = (float)(n - Math.cos(n18) * n17 - height / 2.0f);
            final double n20 = n14;
            final double n21 = n6;
            final double sin = Math.sin(n18);
            final double cos = Math.cos(n18);
            final double n22 = n7;
            final float n23 = (float)(n20 + sin * n21 + cos * n17 * n22);
            final float n24 = (float)(n15 - n21 * Math.cos(n18) + n17 * Math.sin(n18) * n22);
            if (array3.length >= 2) {
                array3[0] = n23;
                array3[1] = n24;
            }
            if (!Float.isNaN(n4)) {
                motionWidget.setRotationZ((float)(n4 + Math.toDegrees(Math.atan2(n24, n23))));
            }
        }
        else {
            n19 = x;
            n = y;
            if (!Float.isNaN(n4)) {
                n = n8 / 2.0f;
                motionWidget.setRotationZ((float)(0.0f + (n4 + Math.toDegrees(Math.atan2(n7 + n9 / 2.0f, n6 + n)))));
                n = y;
                n19 = x;
            }
        }
        final float n25 = n19 + 0.5f;
        final int n26 = (int)n25;
        n += 0.5f;
        motionWidget.layout(n26, (int)n, (int)(n25 + width), (int)(n + height));
    }
    
    public void setupRelative(final Motion mRelativeToController, final MotionPaths motionPaths) {
        final double n = this.x + this.width / 2.0f - motionPaths.x - motionPaths.width / 2.0f;
        final double n2 = this.y + this.height / 2.0f - motionPaths.y - motionPaths.height / 2.0f;
        this.mRelativeToController = mRelativeToController;
        this.x = (float)Math.hypot(n2, n);
        if (Float.isNaN(this.mRelativeAngle)) {
            this.y = (float)(Math.atan2(n2, n) + 1.5707963267948966);
        }
        else {
            this.y = (float)Math.toRadians(this.mRelativeAngle);
        }
    }
}

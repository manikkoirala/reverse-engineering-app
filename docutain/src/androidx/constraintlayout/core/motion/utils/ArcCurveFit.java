// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.core.motion.utils;

import java.util.Arrays;

public class ArcCurveFit extends CurveFit
{
    public static final int ARC_START_FLIP = 3;
    public static final int ARC_START_HORIZONTAL = 2;
    public static final int ARC_START_LINEAR = 0;
    public static final int ARC_START_VERTICAL = 1;
    private static final int START_HORIZONTAL = 2;
    private static final int START_LINEAR = 3;
    private static final int START_VERTICAL = 1;
    Arc[] mArcs;
    private boolean mExtrapolate;
    private final double[] mTime;
    
    public ArcCurveFit(final int[] array, final double[] mTime, final double[][] array2) {
        this.mExtrapolate = true;
        this.mTime = mTime;
        this.mArcs = new Arc[mTime.length - 1];
        int n = 0;
        int n2 = 1;
        int n3 = 1;
        while (true) {
            final Arc[] mArcs = this.mArcs;
            if (n >= mArcs.length) {
                break;
            }
            final int n4 = array[n];
            if (n4 != 0) {
                if (n4 != 1) {
                    if (n4 != 2) {
                        if (n4 == 3) {
                            if (n2 == 1) {
                                n2 = 2;
                            }
                            else {
                                n2 = 1;
                            }
                            n3 = n2;
                        }
                    }
                    else {
                        n2 = 2;
                        n3 = 2;
                    }
                }
                else {
                    n2 = 1;
                    n3 = 1;
                }
            }
            else {
                n3 = 3;
            }
            final double n5 = mTime[n];
            final int n6 = n + 1;
            final double n7 = mTime[n6];
            final double[] array3 = array2[n];
            final double n8 = array3[0];
            final double n9 = array3[1];
            final double[] array4 = array2[n6];
            mArcs[n] = new Arc(n3, n5, n7, n8, n9, array4[0], array4[1]);
            n = n6;
        }
    }
    
    @Override
    public double getPos(double n, final int n2) {
        final boolean mExtrapolate = this.mExtrapolate;
        final int n3 = 0;
        int n7 = 0;
        double point = 0.0;
        Label_0408: {
            if (mExtrapolate) {
                double n4;
                double n6;
                if (n < this.mArcs[0].mTime1) {
                    final double mTime1 = this.mArcs[0].mTime1;
                    n4 = n - this.mArcs[0].mTime1;
                    if (!this.mArcs[0].linear) {
                        this.mArcs[0].setPoint(mTime1);
                        double n5;
                        if (n2 == 0) {
                            n5 = this.mArcs[0].getX();
                            n = this.mArcs[0].getDX();
                        }
                        else {
                            n5 = this.mArcs[0].getY();
                            n = this.mArcs[0].getDY();
                        }
                        return n5 + n4 * n;
                    }
                    if (n2 == 0) {
                        n6 = this.mArcs[0].getLinearX(mTime1);
                        n = this.mArcs[0].getLinearDX(mTime1);
                    }
                    else {
                        n6 = this.mArcs[0].getLinearY(mTime1);
                        n = this.mArcs[0].getLinearDY(mTime1);
                    }
                }
                else {
                    final Arc[] mArcs = this.mArcs;
                    n7 = n3;
                    point = n;
                    if (n <= mArcs[mArcs.length - 1].mTime2) {
                        break Label_0408;
                    }
                    final Arc[] mArcs2 = this.mArcs;
                    final double mTime2 = mArcs2[mArcs2.length - 1].mTime2;
                    n4 = n - mTime2;
                    final Arc[] mArcs3 = this.mArcs;
                    final int n8 = mArcs3.length - 1;
                    if (n2 == 0) {
                        n6 = mArcs3[n8].getLinearX(mTime2);
                        n = this.mArcs[n8].getLinearDX(mTime2);
                    }
                    else {
                        n6 = mArcs3[n8].getLinearY(mTime2);
                        n = this.mArcs[n8].getLinearDY(mTime2);
                    }
                }
                return n6 + n4 * n;
            }
            if (n < this.mArcs[0].mTime1) {
                point = this.mArcs[0].mTime1;
                n7 = n3;
            }
            else {
                final Arc[] mArcs4 = this.mArcs;
                n7 = n3;
                point = n;
                if (n > mArcs4[mArcs4.length - 1].mTime2) {
                    final Arc[] mArcs5 = this.mArcs;
                    point = mArcs5[mArcs5.length - 1].mTime2;
                    n7 = n3;
                }
            }
        }
        while (true) {
            final Arc[] mArcs6 = this.mArcs;
            if (n7 >= mArcs6.length) {
                return Double.NaN;
            }
            if (point <= mArcs6[n7].mTime2) {
                if (this.mArcs[n7].linear) {
                    if (n2 == 0) {
                        return this.mArcs[n7].getLinearX(point);
                    }
                    return this.mArcs[n7].getLinearY(point);
                }
                else {
                    this.mArcs[n7].setPoint(point);
                    if (n2 == 0) {
                        return this.mArcs[n7].getX();
                    }
                    return this.mArcs[n7].getY();
                }
            }
            else {
                ++n7;
            }
        }
    }
    
    @Override
    public void getPos(double point, final double[] array) {
        double mTime2;
        if (this.mExtrapolate) {
            if (point < this.mArcs[0].mTime1) {
                final double mTime1 = this.mArcs[0].mTime1;
                point -= this.mArcs[0].mTime1;
                if (this.mArcs[0].linear) {
                    array[0] = this.mArcs[0].getLinearX(mTime1) + this.mArcs[0].getLinearDX(mTime1) * point;
                    array[1] = this.mArcs[0].getLinearY(mTime1) + point * this.mArcs[0].getLinearDY(mTime1);
                }
                else {
                    this.mArcs[0].setPoint(mTime1);
                    array[0] = this.mArcs[0].getX() + this.mArcs[0].getDX() * point;
                    array[1] = this.mArcs[0].getY() + point * this.mArcs[0].getDY();
                }
                return;
            }
            final Arc[] mArcs = this.mArcs;
            mTime2 = point;
            if (point > mArcs[mArcs.length - 1].mTime2) {
                final Arc[] mArcs2 = this.mArcs;
                final double mTime3 = mArcs2[mArcs2.length - 1].mTime2;
                final double n = point - mTime3;
                final Arc[] mArcs3 = this.mArcs;
                final int n2 = mArcs3.length - 1;
                if (mArcs3[n2].linear) {
                    array[0] = this.mArcs[n2].getLinearX(mTime3) + this.mArcs[n2].getLinearDX(mTime3) * n;
                    array[1] = this.mArcs[n2].getLinearY(mTime3) + n * this.mArcs[n2].getLinearDY(mTime3);
                }
                else {
                    this.mArcs[n2].setPoint(point);
                    array[0] = this.mArcs[n2].getX() + this.mArcs[n2].getDX() * n;
                    array[1] = this.mArcs[n2].getY() + n * this.mArcs[n2].getDY();
                }
                return;
            }
        }
        else {
            double mTime4 = point;
            if (point < this.mArcs[0].mTime1) {
                mTime4 = this.mArcs[0].mTime1;
            }
            final Arc[] mArcs4 = this.mArcs;
            mTime2 = mTime4;
            if (mTime4 > mArcs4[mArcs4.length - 1].mTime2) {
                final Arc[] mArcs5 = this.mArcs;
                mTime2 = mArcs5[mArcs5.length - 1].mTime2;
            }
        }
        int n3 = 0;
        while (true) {
            final Arc[] mArcs6 = this.mArcs;
            if (n3 >= mArcs6.length) {
                return;
            }
            if (mTime2 <= mArcs6[n3].mTime2) {
                if (this.mArcs[n3].linear) {
                    array[0] = this.mArcs[n3].getLinearX(mTime2);
                    array[1] = this.mArcs[n3].getLinearY(mTime2);
                    return;
                }
                this.mArcs[n3].setPoint(mTime2);
                array[0] = this.mArcs[n3].getX();
                array[1] = this.mArcs[n3].getY();
            }
            else {
                ++n3;
            }
        }
    }
    
    @Override
    public void getPos(double point, final float[] array) {
        double point2;
        if (this.mExtrapolate) {
            if (point < this.mArcs[0].mTime1) {
                final double mTime1 = this.mArcs[0].mTime1;
                point -= this.mArcs[0].mTime1;
                if (this.mArcs[0].linear) {
                    array[0] = (float)(this.mArcs[0].getLinearX(mTime1) + this.mArcs[0].getLinearDX(mTime1) * point);
                    array[1] = (float)(this.mArcs[0].getLinearY(mTime1) + point * this.mArcs[0].getLinearDY(mTime1));
                }
                else {
                    this.mArcs[0].setPoint(mTime1);
                    array[0] = (float)(this.mArcs[0].getX() + this.mArcs[0].getDX() * point);
                    array[1] = (float)(this.mArcs[0].getY() + point * this.mArcs[0].getDY());
                }
                return;
            }
            final Arc[] mArcs = this.mArcs;
            point2 = point;
            if (point > mArcs[mArcs.length - 1].mTime2) {
                final Arc[] mArcs2 = this.mArcs;
                final double mTime2 = mArcs2[mArcs2.length - 1].mTime2;
                final double n = point - mTime2;
                final Arc[] mArcs3 = this.mArcs;
                final int n2 = mArcs3.length - 1;
                if (mArcs3[n2].linear) {
                    array[0] = (float)(this.mArcs[n2].getLinearX(mTime2) + this.mArcs[n2].getLinearDX(mTime2) * n);
                    array[1] = (float)(this.mArcs[n2].getLinearY(mTime2) + n * this.mArcs[n2].getLinearDY(mTime2));
                }
                else {
                    this.mArcs[n2].setPoint(point);
                    array[0] = (float)this.mArcs[n2].getX();
                    array[1] = (float)this.mArcs[n2].getY();
                }
                return;
            }
        }
        else if (point < this.mArcs[0].mTime1) {
            point2 = this.mArcs[0].mTime1;
        }
        else {
            final Arc[] mArcs4 = this.mArcs;
            point2 = point;
            if (point > mArcs4[mArcs4.length - 1].mTime2) {
                final Arc[] mArcs5 = this.mArcs;
                point2 = mArcs5[mArcs5.length - 1].mTime2;
            }
        }
        int n3 = 0;
        while (true) {
            final Arc[] mArcs6 = this.mArcs;
            if (n3 >= mArcs6.length) {
                return;
            }
            if (point2 <= mArcs6[n3].mTime2) {
                if (this.mArcs[n3].linear) {
                    array[0] = (float)this.mArcs[n3].getLinearX(point2);
                    array[1] = (float)this.mArcs[n3].getLinearY(point2);
                    return;
                }
                this.mArcs[n3].setPoint(point2);
                array[0] = (float)this.mArcs[n3].getX();
                array[1] = (float)this.mArcs[n3].getY();
            }
            else {
                ++n3;
            }
        }
    }
    
    @Override
    public double getSlope(double mTime2, final int n) {
        final Arc[] mArcs = this.mArcs;
        final int n2 = 0;
        double mTime3 = mTime2;
        if (mTime2 < mArcs[0].mTime1) {
            mTime3 = this.mArcs[0].mTime1;
        }
        final Arc[] mArcs2 = this.mArcs;
        int n3 = n2;
        mTime2 = mTime3;
        if (mTime3 > mArcs2[mArcs2.length - 1].mTime2) {
            final Arc[] mArcs3 = this.mArcs;
            mTime2 = mArcs3[mArcs3.length - 1].mTime2;
            n3 = n2;
        }
        while (true) {
            final Arc[] mArcs4 = this.mArcs;
            if (n3 >= mArcs4.length) {
                return Double.NaN;
            }
            if (mTime2 <= mArcs4[n3].mTime2) {
                if (this.mArcs[n3].linear) {
                    if (n == 0) {
                        return this.mArcs[n3].getLinearDX(mTime2);
                    }
                    return this.mArcs[n3].getLinearDY(mTime2);
                }
                else {
                    this.mArcs[n3].setPoint(mTime2);
                    if (n == 0) {
                        return this.mArcs[n3].getDX();
                    }
                    return this.mArcs[n3].getDY();
                }
            }
            else {
                ++n3;
            }
        }
    }
    
    @Override
    public void getSlope(final double n, final double[] array) {
        double point;
        if (n < this.mArcs[0].mTime1) {
            point = this.mArcs[0].mTime1;
        }
        else {
            final Arc[] mArcs = this.mArcs;
            point = n;
            if (n > mArcs[mArcs.length - 1].mTime2) {
                final Arc[] mArcs2 = this.mArcs;
                point = mArcs2[mArcs2.length - 1].mTime2;
            }
        }
        int n2 = 0;
        while (true) {
            final Arc[] mArcs3 = this.mArcs;
            if (n2 >= mArcs3.length) {
                return;
            }
            if (point <= mArcs3[n2].mTime2) {
                if (this.mArcs[n2].linear) {
                    array[0] = this.mArcs[n2].getLinearDX(point);
                    array[1] = this.mArcs[n2].getLinearDY(point);
                    return;
                }
                this.mArcs[n2].setPoint(point);
                array[0] = this.mArcs[n2].getDX();
                array[1] = this.mArcs[n2].getDY();
            }
            else {
                ++n2;
            }
        }
    }
    
    @Override
    public double[] getTimePoints() {
        return this.mTime;
    }
    
    private static class Arc
    {
        private static final double EPSILON = 0.001;
        private static final String TAG = "Arc";
        private static double[] ourPercent;
        boolean linear;
        double mArcDistance;
        double mArcVelocity;
        double mEllipseA;
        double mEllipseB;
        double mEllipseCenterX;
        double mEllipseCenterY;
        double[] mLut;
        double mOneOverDeltaTime;
        double mTime1;
        double mTime2;
        double mTmpCosAngle;
        double mTmpSinAngle;
        boolean mVertical;
        double mX1;
        double mX2;
        double mY1;
        double mY2;
        
        static {
            Arc.ourPercent = new double[91];
        }
        
        Arc(int n, double x, double y, double n2, double mTime1, final double mx2, final double my2) {
            boolean mVertical = false;
            this.linear = false;
            final int n3 = 1;
            if (n == 1) {
                mVertical = true;
            }
            this.mVertical = mVertical;
            this.mTime1 = x;
            this.mTime2 = y;
            this.mOneOverDeltaTime = 1.0 / (y - x);
            if (3 == n) {
                this.linear = true;
            }
            y = mx2 - n2;
            x = my2 - mTime1;
            if (!this.linear && Math.abs(y) >= 0.001 && Math.abs(x) >= 0.001) {
                this.mLut = new double[101];
                final boolean mVertical2 = this.mVertical;
                if (mVertical2) {
                    n = -1;
                }
                else {
                    n = 1;
                }
                this.mEllipseA = y * n;
                if (mVertical2) {
                    n = n3;
                }
                else {
                    n = -1;
                }
                this.mEllipseB = x * n;
                if (mVertical2) {
                    x = mx2;
                }
                else {
                    x = n2;
                }
                this.mEllipseCenterX = x;
                if (mVertical2) {
                    x = mTime1;
                }
                else {
                    x = my2;
                }
                this.mEllipseCenterY = x;
                this.buildTable(n2, mTime1, mx2, my2);
                this.mArcVelocity = this.mArcDistance * this.mOneOverDeltaTime;
                return;
            }
            this.linear = true;
            this.mX1 = n2;
            this.mX2 = mx2;
            this.mY1 = mTime1;
            this.mY2 = my2;
            n2 = Math.hypot(x, y);
            this.mArcDistance = n2;
            this.mArcVelocity = n2 * this.mOneOverDeltaTime;
            n2 = this.mTime2;
            mTime1 = this.mTime1;
            this.mEllipseCenterX = y / (n2 - mTime1);
            this.mEllipseCenterY = x / (n2 - mTime1);
        }
        
        private void buildTable(double n, double n2, double key, final double n3) {
            int n4 = 0;
            double mArcDistance = 0.0;
            double n5 = 0.0;
            double n6 = 0.0;
            while (true) {
                final double[] ourPercent = Arc.ourPercent;
                if (n4 >= ourPercent.length) {
                    break;
                }
                final double radians = Math.toRadians(n4 * 90.0 / (ourPercent.length - 1));
                final double sin = Math.sin(radians);
                final double cos = Math.cos(radians);
                final double n7 = sin * (key - n);
                final double n8 = cos * (n2 - n3);
                if (n4 > 0) {
                    mArcDistance += Math.hypot(n7 - n5, n8 - n6);
                    Arc.ourPercent[n4] = mArcDistance;
                }
                ++n4;
                n6 = n8;
                n5 = n7;
            }
            this.mArcDistance = mArcDistance;
            int n9 = 0;
            while (true) {
                final double[] ourPercent2 = Arc.ourPercent;
                if (n9 >= ourPercent2.length) {
                    break;
                }
                ourPercent2[n9] /= mArcDistance;
                ++n9;
            }
            int n10 = 0;
            while (true) {
                final double[] mLut = this.mLut;
                if (n10 >= mLut.length) {
                    break;
                }
                key = n10 / (double)(mLut.length - 1);
                final int binarySearch = Arrays.binarySearch(Arc.ourPercent, key);
                if (binarySearch >= 0) {
                    this.mLut[n10] = binarySearch / (double)(Arc.ourPercent.length - 1);
                }
                else if (binarySearch == -1) {
                    this.mLut[n10] = 0.0;
                }
                else {
                    final int n11 = -binarySearch;
                    final int n12 = n11 - 2;
                    n = n12;
                    final double[] ourPercent3 = Arc.ourPercent;
                    n2 = ourPercent3[n12];
                    n = (n + (key - n2) / (ourPercent3[n11 - 1] - n2)) / (ourPercent3.length - 1);
                    this.mLut[n10] = n;
                }
                ++n10;
            }
        }
        
        double getDX() {
            final double x = this.mEllipseA * this.mTmpCosAngle;
            final double n = this.mArcVelocity / Math.hypot(x, -this.mEllipseB * this.mTmpSinAngle);
            double n2 = x;
            if (this.mVertical) {
                n2 = -x;
            }
            return n2 * n;
        }
        
        double getDY() {
            final double mEllipseA = this.mEllipseA;
            final double mTmpCosAngle = this.mTmpCosAngle;
            final double y = -this.mEllipseB * this.mTmpSinAngle;
            final double n = this.mArcVelocity / Math.hypot(mEllipseA * mTmpCosAngle, y);
            double n2;
            if (this.mVertical) {
                n2 = -y * n;
            }
            else {
                n2 = y * n;
            }
            return n2;
        }
        
        public double getLinearDX(final double n) {
            return this.mEllipseCenterX;
        }
        
        public double getLinearDY(final double n) {
            return this.mEllipseCenterY;
        }
        
        public double getLinearX(final double n) {
            final double mTime1 = this.mTime1;
            final double mOneOverDeltaTime = this.mOneOverDeltaTime;
            final double mx1 = this.mX1;
            return mx1 + (n - mTime1) * mOneOverDeltaTime * (this.mX2 - mx1);
        }
        
        public double getLinearY(final double n) {
            final double mTime1 = this.mTime1;
            final double mOneOverDeltaTime = this.mOneOverDeltaTime;
            final double my1 = this.mY1;
            return my1 + (n - mTime1) * mOneOverDeltaTime * (this.mY2 - my1);
        }
        
        double getX() {
            return this.mEllipseCenterX + this.mEllipseA * this.mTmpSinAngle;
        }
        
        double getY() {
            return this.mEllipseCenterY + this.mEllipseB * this.mTmpCosAngle;
        }
        
        double lookup(double n) {
            if (n <= 0.0) {
                return 0.0;
            }
            if (n >= 1.0) {
                return 1.0;
            }
            final double[] mLut = this.mLut;
            final double n2 = n * (mLut.length - 1);
            final int n3 = (int)n2;
            n = n3;
            final double n4 = mLut[n3];
            return n4 + (n2 - n) * (mLut[n3 + 1] - n4);
        }
        
        void setPoint(double n) {
            if (this.mVertical) {
                n = this.mTime2 - n;
            }
            else {
                n -= this.mTime1;
            }
            n = this.lookup(n * this.mOneOverDeltaTime) * 1.5707963267948966;
            this.mTmpSinAngle = Math.sin(n);
            this.mTmpCosAngle = Math.cos(n);
        }
    }
}

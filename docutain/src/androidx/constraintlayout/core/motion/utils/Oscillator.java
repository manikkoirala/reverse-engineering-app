// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.core.motion.utils;

import java.util.Arrays;

public class Oscillator
{
    public static final int BOUNCE = 6;
    public static final int COS_WAVE = 5;
    public static final int CUSTOM = 7;
    public static final int REVERSE_SAW_WAVE = 4;
    public static final int SAW_WAVE = 3;
    public static final int SIN_WAVE = 0;
    public static final int SQUARE_WAVE = 1;
    public static String TAG = "Oscillator";
    public static final int TRIANGLE_WAVE = 2;
    double PI2;
    double[] mArea;
    MonotonicCurveFit mCustomCurve;
    String mCustomType;
    private boolean mNormalized;
    float[] mPeriod;
    double[] mPosition;
    int mType;
    
    public Oscillator() {
        this.mPeriod = new float[0];
        this.mPosition = new double[0];
        this.PI2 = 6.283185307179586;
        this.mNormalized = false;
    }
    
    public void addPoint(final double key, final float n) {
        final int n2 = this.mPeriod.length + 1;
        int binarySearch;
        final int n3 = binarySearch = Arrays.binarySearch(this.mPosition, key);
        if (n3 < 0) {
            binarySearch = -n3 - 1;
        }
        this.mPosition = Arrays.copyOf(this.mPosition, n2);
        this.mPeriod = Arrays.copyOf(this.mPeriod, n2);
        this.mArea = new double[n2];
        final double[] mPosition = this.mPosition;
        System.arraycopy(mPosition, binarySearch, mPosition, binarySearch + 1, n2 - binarySearch - 1);
        this.mPosition[binarySearch] = key;
        this.mPeriod[binarySearch] = n;
        this.mNormalized = false;
    }
    
    double getDP(double n) {
        final double n2 = 0.0;
        double key;
        if (n <= 0.0) {
            key = 1.0E-5;
        }
        else {
            key = n;
            if (n >= 1.0) {
                key = 0.999999;
            }
        }
        final int binarySearch = Arrays.binarySearch(this.mPosition, key);
        if (binarySearch > 0) {
            return 0.0;
        }
        n = n2;
        if (binarySearch != 0) {
            final int n3 = -binarySearch - 1;
            final float[] mPeriod = this.mPeriod;
            final float n4 = mPeriod[n3];
            final int n5 = n3 - 1;
            final float n6 = mPeriod[n5];
            final double n7 = n4 - n6;
            final double[] mPosition = this.mPosition;
            final double n8 = mPosition[n3];
            n = mPosition[n5];
            final double n9 = n7 / (n8 - n);
            n = n6 - n9 * n + key * n9;
        }
        return n;
    }
    
    double getP(double n) {
        final double n2 = 1.0;
        double key;
        if (n < 0.0) {
            key = 0.0;
        }
        else {
            key = n;
            if (n > 1.0) {
                key = 1.0;
            }
        }
        final int binarySearch = Arrays.binarySearch(this.mPosition, key);
        if (binarySearch > 0) {
            n = n2;
        }
        else if (binarySearch != 0) {
            final int n3 = -binarySearch - 1;
            final float[] mPeriod = this.mPeriod;
            final float n4 = mPeriod[n3];
            final int n5 = n3 - 1;
            final float n6 = mPeriod[n5];
            final double n7 = n4 - n6;
            final double[] mPosition = this.mPosition;
            final double n8 = mPosition[n3];
            n = mPosition[n5];
            final double n9 = n7 / (n8 - n);
            n = this.mArea[n5] + (n6 - n9 * n) * (key - n) + n9 * (key * key - n * n) / 2.0;
        }
        else {
            n = 0.0;
        }
        return n;
    }
    
    public double getSlope(double n, double n2, double n3) {
        n2 += this.getP(n);
        n = this.getDP(n) + n3;
        switch (this.mType) {
            default: {
                n3 = this.PI2;
                return n * n3 * Math.cos(n3 * n2);
            }
            case 7: {
                return this.mCustomCurve.getSlope(n2 % 1.0, 0);
            }
            case 6: {
                return n * 4.0 * ((n2 * 4.0 + 2.0) % 4.0 - 2.0);
            }
            case 5: {
                n3 = this.PI2;
                return -n3 * n * Math.sin(n3 * n2);
            }
            case 4: {
                return -n * 2.0;
            }
            case 3: {
                return n * 2.0;
            }
            case 2: {
                return n * 4.0 * Math.signum((n2 * 4.0 + 3.0) % 4.0 - 2.0);
            }
            case 1: {
                return 0.0;
            }
        }
    }
    
    public double getValue(double abs, final double n) {
        abs = this.getP(abs) + n;
        switch (this.mType) {
            default: {
                return Math.sin(this.PI2 * abs);
            }
            case 7: {
                return this.mCustomCurve.getPos(abs % 1.0, 0);
            }
            case 6: {
                abs = 1.0 - Math.abs(abs * 4.0 % 4.0 - 2.0);
                abs *= abs;
                break;
            }
            case 5: {
                return Math.cos(this.PI2 * (n + abs));
            }
            case 4: {
                abs = (abs * 2.0 + 1.0) % 2.0;
                break;
            }
            case 3: {
                return (abs * 2.0 + 1.0) % 2.0 - 1.0;
            }
            case 2: {
                abs = Math.abs((abs * 4.0 + 1.0) % 4.0 - 2.0);
                break;
            }
            case 1: {
                return Math.signum(0.5 - abs % 1.0);
            }
        }
        return 1.0 - abs;
    }
    
    public void normalize() {
        double n = 0.0;
        int n2 = 0;
        while (true) {
            final float[] mPeriod = this.mPeriod;
            if (n2 >= mPeriod.length) {
                break;
            }
            n += mPeriod[n2];
            ++n2;
        }
        double n3 = 0.0;
        int n4 = 1;
        while (true) {
            final float[] mPeriod2 = this.mPeriod;
            if (n4 >= mPeriod2.length) {
                break;
            }
            final int n5 = n4 - 1;
            final float n6 = (mPeriod2[n5] + mPeriod2[n4]) / 2.0f;
            final double[] mPosition = this.mPosition;
            n3 += (mPosition[n4] - mPosition[n5]) * n6;
            ++n4;
        }
        int n7 = 0;
        while (true) {
            final float[] mPeriod3 = this.mPeriod;
            if (n7 >= mPeriod3.length) {
                break;
            }
            mPeriod3[n7] *= (float)(n / n3);
            ++n7;
        }
        this.mArea[0] = 0.0;
        int n8 = 1;
        while (true) {
            final float[] mPeriod4 = this.mPeriod;
            if (n8 >= mPeriod4.length) {
                break;
            }
            final int n9 = n8 - 1;
            final float n10 = (mPeriod4[n9] + mPeriod4[n8]) / 2.0f;
            final double[] mPosition2 = this.mPosition;
            final double n11 = mPosition2[n8];
            final double n12 = mPosition2[n9];
            final double[] mArea = this.mArea;
            mArea[n8] = mArea[n9] + (n11 - n12) * n10;
            ++n8;
        }
        this.mNormalized = true;
    }
    
    public void setType(final int mType, final String mCustomType) {
        this.mType = mType;
        this.mCustomType = mCustomType;
        if (mCustomType != null) {
            this.mCustomCurve = MonotonicCurveFit.buildWave(mCustomType);
        }
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("pos =");
        sb.append(Arrays.toString(this.mPosition));
        sb.append(" period=");
        sb.append(Arrays.toString(this.mPeriod));
        return sb.toString();
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.core.motion.utils;

public class VelocityMatrix
{
    private static String TAG = "VelocityMatrix";
    float mDRotate;
    float mDScaleX;
    float mDScaleY;
    float mDTranslateX;
    float mDTranslateY;
    float mRotate;
    
    public void applyTransform(float n, float n2, final int n3, final int n4, final float[] array) {
        final float n5 = array[0];
        final float n6 = array[1];
        n = (n - 0.5f) * 2.0f;
        final float n7 = (n2 - 0.5f) * 2.0f;
        final float mdTranslateX = this.mDTranslateX;
        final float mdTranslateY = this.mDTranslateY;
        final float mdScaleX = this.mDScaleX;
        final float mdScaleY = this.mDScaleY;
        final float n8 = (float)Math.toRadians(this.mRotate);
        n2 = (float)Math.toRadians(this.mDRotate);
        final double n9 = -n3 * n;
        final double n10 = n8;
        final double sin = Math.sin(n10);
        final double n11 = n4 * n7;
        final float n12 = (float)(n9 * sin - Math.cos(n10) * n11);
        final float n13 = (float)(n3 * n * Math.cos(n10) - n11 * Math.sin(n10));
        array[0] = n5 + mdTranslateX + mdScaleX * n + n12 * n2;
        array[1] = n6 + mdTranslateY + mdScaleY * n7 + n2 * n13;
    }
    
    public void clear() {
        this.mDRotate = 0.0f;
        this.mDTranslateY = 0.0f;
        this.mDTranslateX = 0.0f;
        this.mDScaleY = 0.0f;
        this.mDScaleX = 0.0f;
    }
    
    public void setRotationVelocity(final KeyCycleOscillator keyCycleOscillator, final float n) {
        if (keyCycleOscillator != null) {
            this.mDRotate = keyCycleOscillator.getSlope(n);
        }
    }
    
    public void setRotationVelocity(final SplineSet set, final float n) {
        if (set != null) {
            this.mDRotate = set.getSlope(n);
            this.mRotate = set.get(n);
        }
    }
    
    public void setScaleVelocity(final KeyCycleOscillator keyCycleOscillator, final KeyCycleOscillator keyCycleOscillator2, final float n) {
        if (keyCycleOscillator != null) {
            this.mDScaleX = keyCycleOscillator.getSlope(n);
        }
        if (keyCycleOscillator2 != null) {
            this.mDScaleY = keyCycleOscillator2.getSlope(n);
        }
    }
    
    public void setScaleVelocity(final SplineSet set, final SplineSet set2, final float n) {
        if (set != null) {
            this.mDScaleX = set.getSlope(n);
        }
        if (set2 != null) {
            this.mDScaleY = set2.getSlope(n);
        }
    }
    
    public void setTranslationVelocity(final KeyCycleOscillator keyCycleOscillator, final KeyCycleOscillator keyCycleOscillator2, final float n) {
        if (keyCycleOscillator != null) {
            this.mDTranslateX = keyCycleOscillator.getSlope(n);
        }
        if (keyCycleOscillator2 != null) {
            this.mDTranslateY = keyCycleOscillator2.getSlope(n);
        }
    }
    
    public void setTranslationVelocity(final SplineSet set, final SplineSet set2, final float n) {
        if (set != null) {
            this.mDTranslateX = set.getSlope(n);
        }
        if (set2 != null) {
            this.mDTranslateY = set2.getSlope(n);
        }
    }
}

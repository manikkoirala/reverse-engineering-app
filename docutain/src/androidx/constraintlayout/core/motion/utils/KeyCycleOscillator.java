// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.core.motion.utils;

import java.text.DecimalFormat;
import java.util.Iterator;
import java.util.List;
import java.util.Collections;
import java.util.Comparator;
import androidx.constraintlayout.core.motion.MotionWidget;
import java.util.ArrayList;

public abstract class KeyCycleOscillator
{
    private static final String TAG = "KeyCycleOscillator";
    private CurveFit mCurveFit;
    private CycleOscillator mCycleOscillator;
    private String mType;
    public int mVariesBy;
    ArrayList<WavePoint> mWavePoints;
    private int mWaveShape;
    private String mWaveString;
    
    public KeyCycleOscillator() {
        this.mWaveShape = 0;
        this.mWaveString = null;
        this.mVariesBy = 0;
        this.mWavePoints = new ArrayList<WavePoint>();
    }
    
    public static KeyCycleOscillator makeWidgetCycle(final String s) {
        if (s.equals("pathRotate")) {
            return new PathRotateSet(s);
        }
        return new CoreSpline(s);
    }
    
    public float get(final float n) {
        return (float)this.mCycleOscillator.getValues(n);
    }
    
    public CurveFit getCurveFit() {
        return this.mCurveFit;
    }
    
    public float getSlope(final float n) {
        return (float)this.mCycleOscillator.getSlope(n);
    }
    
    protected void setCustom(final Object o) {
    }
    
    public void setPoint(final int n, final int mWaveShape, final String mWaveString, final int mVariesBy, final float n2, final float n3, final float n4, final float n5) {
        this.mWavePoints.add(new WavePoint(n, n2, n3, n4, n5));
        if (mVariesBy != -1) {
            this.mVariesBy = mVariesBy;
        }
        this.mWaveShape = mWaveShape;
        this.mWaveString = mWaveString;
    }
    
    public void setPoint(final int n, final int mWaveShape, final String mWaveString, final int mVariesBy, final float n2, final float n3, final float n4, final float n5, final Object custom) {
        this.mWavePoints.add(new WavePoint(n, n2, n3, n4, n5));
        if (mVariesBy != -1) {
            this.mVariesBy = mVariesBy;
        }
        this.mWaveShape = mWaveShape;
        this.setCustom(custom);
        this.mWaveString = mWaveString;
    }
    
    public void setProperty(final MotionWidget motionWidget, final float n) {
    }
    
    public void setType(final String mType) {
        this.mType = mType;
    }
    
    public void setup(final float n) {
        final int size = this.mWavePoints.size();
        if (size == 0) {
            return;
        }
        Collections.sort(this.mWavePoints, (Comparator<? super WavePoint>)new Comparator<WavePoint>(this) {
            final KeyCycleOscillator this$0;
            
            @Override
            public int compare(final WavePoint wavePoint, final WavePoint wavePoint2) {
                return Integer.compare(wavePoint.mPosition, wavePoint2.mPosition);
            }
        });
        final double[] array = new double[size];
        final double[][] array2 = new double[size][3];
        this.mCycleOscillator = new CycleOscillator(this.mWaveShape, this.mWaveString, this.mVariesBy, size);
        final Iterator<WavePoint> iterator = this.mWavePoints.iterator();
        int n2 = 0;
        while (iterator.hasNext()) {
            final WavePoint wavePoint = iterator.next();
            array[n2] = wavePoint.mPeriod * 0.01;
            array2[n2][0] = wavePoint.mValue;
            array2[n2][1] = wavePoint.mOffset;
            array2[n2][2] = wavePoint.mPhase;
            this.mCycleOscillator.setPoint(n2, wavePoint.mPosition, wavePoint.mPeriod, wavePoint.mOffset, wavePoint.mPhase, wavePoint.mValue);
            ++n2;
        }
        this.mCycleOscillator.setup(n);
        this.mCurveFit = CurveFit.get(0, array, array2);
    }
    
    @Override
    public String toString() {
        String str = this.mType;
        final DecimalFormat decimalFormat = new DecimalFormat("##.##");
        for (final WavePoint wavePoint : this.mWavePoints) {
            final StringBuilder sb = new StringBuilder();
            sb.append(str);
            sb.append("[");
            sb.append(wavePoint.mPosition);
            sb.append(" , ");
            sb.append(decimalFormat.format(wavePoint.mValue));
            sb.append("] ");
            str = sb.toString();
        }
        return str;
    }
    
    public boolean variesByPath() {
        final int mVariesBy = this.mVariesBy;
        boolean b = true;
        if (mVariesBy != 1) {
            b = false;
        }
        return b;
    }
    
    private static class CoreSpline extends KeyCycleOscillator
    {
        String type;
        int typeId;
        
        public CoreSpline(final String type) {
            this.type = type;
            this.typeId = TypedValues$CycleType$_CC.getId(type);
        }
        
        @Override
        public void setProperty(final MotionWidget motionWidget, final float n) {
            motionWidget.setValue(this.typeId, this.get(n));
        }
    }
    
    static class CycleOscillator
    {
        private static final String TAG = "CycleOscillator";
        static final int UNSET = -1;
        private final int OFFST;
        private final int PHASE;
        private final int VALUE;
        CurveFit mCurveFit;
        float[] mOffset;
        Oscillator mOscillator;
        float mPathLength;
        float[] mPeriod;
        float[] mPhase;
        double[] mPosition;
        float[] mScale;
        double[] mSplineSlopeCache;
        double[] mSplineValueCache;
        float[] mValues;
        private final int mVariesBy;
        int mWaveShape;
        
        CycleOscillator(final int mWaveShape, final String s, final int mVariesBy, final int n) {
            final Oscillator mOscillator = new Oscillator();
            this.mOscillator = mOscillator;
            this.OFFST = 0;
            this.PHASE = 1;
            this.VALUE = 2;
            this.mWaveShape = mWaveShape;
            this.mVariesBy = mVariesBy;
            mOscillator.setType(mWaveShape, s);
            this.mValues = new float[n];
            this.mPosition = new double[n];
            this.mPeriod = new float[n];
            this.mOffset = new float[n];
            this.mPhase = new float[n];
            this.mScale = new float[n];
        }
        
        public double getLastPhase() {
            return this.mSplineValueCache[1];
        }
        
        public double getSlope(final float n) {
            final CurveFit mCurveFit = this.mCurveFit;
            if (mCurveFit != null) {
                final double n2 = n;
                mCurveFit.getSlope(n2, this.mSplineSlopeCache);
                this.mCurveFit.getPos(n2, this.mSplineValueCache);
            }
            else {
                final double[] mSplineSlopeCache = this.mSplineSlopeCache;
                mSplineSlopeCache[0] = 0.0;
                mSplineSlopeCache[2] = (mSplineSlopeCache[1] = 0.0);
            }
            final Oscillator mOscillator = this.mOscillator;
            final double n3 = n;
            final double value = mOscillator.getValue(n3, this.mSplineValueCache[1]);
            final double slope = this.mOscillator.getSlope(n3, this.mSplineValueCache[1], this.mSplineSlopeCache[1]);
            final double[] mSplineSlopeCache2 = this.mSplineSlopeCache;
            return mSplineSlopeCache2[0] + value * mSplineSlopeCache2[2] + slope * this.mSplineValueCache[2];
        }
        
        public double getValues(final float n) {
            final CurveFit mCurveFit = this.mCurveFit;
            if (mCurveFit != null) {
                mCurveFit.getPos(n, this.mSplineValueCache);
            }
            else {
                final double[] mSplineValueCache = this.mSplineValueCache;
                mSplineValueCache[0] = this.mOffset[0];
                mSplineValueCache[1] = this.mPhase[0];
                mSplineValueCache[2] = this.mValues[0];
            }
            final double[] mSplineValueCache2 = this.mSplineValueCache;
            return mSplineValueCache2[0] + this.mOscillator.getValue(n, mSplineValueCache2[1]) * this.mSplineValueCache[2];
        }
        
        public void setPoint(final int n, final int n2, final float n3, final float n4, final float n5, final float n6) {
            this.mPosition[n] = n2 / 100.0;
            this.mPeriod[n] = n3;
            this.mOffset[n] = n4;
            this.mPhase[n] = n5;
            this.mValues[n] = n6;
        }
        
        public void setup(final float mPathLength) {
            this.mPathLength = mPathLength;
            final double[][] array = new double[this.mPosition.length][3];
            final float[] mValues = this.mValues;
            this.mSplineValueCache = new double[mValues.length + 2];
            this.mSplineSlopeCache = new double[mValues.length + 2];
            if (this.mPosition[0] > 0.0) {
                this.mOscillator.addPoint(0.0, this.mPeriod[0]);
            }
            final double[] mPosition = this.mPosition;
            final int n = mPosition.length - 1;
            if (mPosition[n] < 1.0) {
                this.mOscillator.addPoint(1.0, this.mPeriod[n]);
            }
            for (int i = 0; i < array.length; ++i) {
                final double[] array2 = array[i];
                array2[0] = this.mOffset[i];
                array2[1] = this.mPhase[i];
                array2[2] = this.mValues[i];
                this.mOscillator.addPoint(this.mPosition[i], this.mPeriod[i]);
            }
            this.mOscillator.normalize();
            final double[] mPosition2 = this.mPosition;
            if (mPosition2.length > 1) {
                this.mCurveFit = CurveFit.get(0, mPosition2, array);
            }
            else {
                this.mCurveFit = null;
            }
        }
    }
    
    private static class IntDoubleSort
    {
        private static int partition(final int[] array, final float[] array2, int i, final int n) {
            final int n2 = array[n];
            int n3 = i;
            while (i < n) {
                int n4 = n3;
                if (array[i] <= n2) {
                    swap(array, array2, n3, i);
                    n4 = n3 + 1;
                }
                ++i;
                n3 = n4;
            }
            swap(array, array2, n3, n);
            return n3;
        }
        
        static void sort(final int[] array, final float[] array2, int i, int n) {
            final int[] array3 = new int[array.length + 10];
            array3[0] = n;
            array3[1] = i;
            int n2;
            int n3;
            int partition = 0;
            int n4;
            for (i = 2; i > 0; i = n + 1, array3[n] = partition - 1, n = i + 1, array3[i] = n2, n4 = n + 1, array3[n] = n3, i = n4 + 1, array3[n4] = partition + 1) {
                --i;
                n2 = array3[i];
                n = i - 1;
                n3 = array3[n];
                i = n;
                if (n2 < n3) {
                    partition = partition(array, array2, n2, n3);
                }
            }
        }
        
        private static void swap(final int[] array, final float[] array2, final int n, final int n2) {
            final int n3 = array[n];
            array[n] = array[n2];
            array[n2] = n3;
            final float n4 = array2[n];
            array2[n] = array2[n2];
            array2[n2] = n4;
        }
    }
    
    private static class IntFloatFloatSort
    {
        private static int partition(final int[] array, final float[] array2, final float[] array3, int i, final int n) {
            final int n2 = array[n];
            int n3 = i;
            while (i < n) {
                int n4 = n3;
                if (array[i] <= n2) {
                    swap(array, array2, array3, n3, i);
                    n4 = n3 + 1;
                }
                ++i;
                n3 = n4;
            }
            swap(array, array2, array3, n3, n);
            return n3;
        }
        
        static void sort(final int[] array, final float[] array2, final float[] array3, int i, int n) {
            final int[] array4 = new int[array.length + 10];
            array4[0] = n;
            array4[1] = i;
            int n2;
            int n3;
            int partition = 0;
            int n4 = 0;
            for (i = 2; i > 0; i = n4 + 1, array4[n4] = n2, n = i + 1, array4[i] = n3, i = n + 1, array4[n] = partition + 1) {
                --i;
                n2 = array4[i];
                n = i - 1;
                n3 = array4[n];
                i = n;
                if (n2 < n3) {
                    partition = partition(array, array2, array3, n2, n3);
                    n4 = n + 1;
                    array4[n] = partition - 1;
                }
            }
        }
        
        private static void swap(final int[] array, final float[] array2, final float[] array3, final int n, final int n2) {
            final int n3 = array[n];
            array[n] = array[n2];
            array[n2] = n3;
            final float n4 = array2[n];
            array2[n] = array2[n2];
            array2[n2] = n4;
            final float n5 = array3[n];
            array3[n] = array3[n2];
            array3[n2] = n5;
        }
    }
    
    public static class PathRotateSet extends KeyCycleOscillator
    {
        String type;
        int typeId;
        
        public PathRotateSet(final String type) {
            this.type = type;
            this.typeId = TypedValues$CycleType$_CC.getId(type);
        }
        
        public void setPathRotate(final MotionWidget motionWidget, final float n, final double x, final double y) {
            motionWidget.setRotationZ(this.get(n) + (float)Math.toDegrees(Math.atan2(y, x)));
        }
        
        @Override
        public void setProperty(final MotionWidget motionWidget, final float n) {
            motionWidget.setValue(this.typeId, this.get(n));
        }
    }
    
    static class WavePoint
    {
        float mOffset;
        float mPeriod;
        float mPhase;
        int mPosition;
        float mValue;
        
        public WavePoint(final int mPosition, final float mPeriod, final float mOffset, final float mPhase, final float mValue) {
            this.mPosition = mPosition;
            this.mValue = mValue;
            this.mOffset = mOffset;
            this.mPeriod = mPeriod;
            this.mPhase = mPhase;
        }
    }
}

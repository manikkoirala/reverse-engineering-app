// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.core.motion.utils;

import androidx.constraintlayout.core.motion.CustomVariable;
import androidx.constraintlayout.core.motion.MotionWidget;
import androidx.constraintlayout.core.motion.CustomAttribute;
import java.text.DecimalFormat;
import java.io.PrintStream;

public abstract class TimeCycleSplineSet
{
    protected static final int CURVE_OFFSET = 2;
    protected static final int CURVE_PERIOD = 1;
    protected static final int CURVE_VALUE = 0;
    private static final String TAG = "SplineSet";
    protected static float VAL_2PI = 6.2831855f;
    protected int count;
    protected float last_cycle;
    protected long last_time;
    protected float[] mCache;
    protected boolean mContinue;
    protected CurveFit mCurveFit;
    protected int[] mTimePoints;
    protected String mType;
    protected float[][] mValues;
    protected int mWaveShape;
    
    public TimeCycleSplineSet() {
        this.mWaveShape = 0;
        this.mTimePoints = new int[10];
        this.mValues = new float[10][3];
        this.mCache = new float[3];
        this.mContinue = false;
        this.last_cycle = Float.NaN;
    }
    
    protected float calcWave(float abs) {
        switch (this.mWaveShape) {
            default: {
                return (float)Math.sin(abs * TimeCycleSplineSet.VAL_2PI);
            }
            case 6: {
                abs = 1.0f - Math.abs(abs * 4.0f % 4.0f - 2.0f);
                abs *= abs;
                break;
            }
            case 5: {
                return (float)Math.cos(abs * TimeCycleSplineSet.VAL_2PI);
            }
            case 4: {
                abs = (abs * 2.0f + 1.0f) % 2.0f;
                break;
            }
            case 3: {
                return (abs * 2.0f + 1.0f) % 2.0f - 1.0f;
            }
            case 2: {
                abs = Math.abs(abs);
                break;
            }
            case 1: {
                return Math.signum(abs * TimeCycleSplineSet.VAL_2PI);
            }
        }
        return 1.0f - abs;
    }
    
    public CurveFit getCurveFit() {
        return this.mCurveFit;
    }
    
    public void setPoint(final int n, final float n2, final float n3, final int b, final float n4) {
        final int[] mTimePoints = this.mTimePoints;
        final int count = this.count;
        mTimePoints[count] = n;
        final float[] array = this.mValues[count];
        array[0] = n2;
        array[1] = n3;
        array[2] = n4;
        this.mWaveShape = Math.max(this.mWaveShape, b);
        ++this.count;
    }
    
    protected void setStartTime(final long last_time) {
        this.last_time = last_time;
    }
    
    public void setType(final String mType) {
        this.mType = mType;
    }
    
    public void setup(final int n) {
        final int count = this.count;
        if (count == 0) {
            final PrintStream err = System.err;
            final StringBuilder sb = new StringBuilder();
            sb.append("Error no points added to ");
            sb.append(this.mType);
            err.println(sb.toString());
            return;
        }
        Sort.doubleQuickSort(this.mTimePoints, this.mValues, 0, count - 1);
        int n2 = 1;
        int n3 = 0;
        while (true) {
            final int[] mTimePoints = this.mTimePoints;
            if (n2 >= mTimePoints.length) {
                break;
            }
            int n4 = n3;
            if (mTimePoints[n2] != mTimePoints[n2 - 1]) {
                n4 = n3 + 1;
            }
            ++n2;
            n3 = n4;
        }
        int n5;
        if ((n5 = n3) == 0) {
            n5 = 1;
        }
        final double[] array = new double[n5];
        final double[][] array2 = new double[n5][3];
        int i = 0;
        int n6 = 0;
        while (i < this.count) {
            Label_0233: {
                if (i > 0) {
                    final int[] mTimePoints2 = this.mTimePoints;
                    if (mTimePoints2[i] == mTimePoints2[i - 1]) {
                        break Label_0233;
                    }
                }
                array[n6] = this.mTimePoints[i] * 0.01;
                final double[] array3 = array2[n6];
                final float[] array4 = this.mValues[i];
                array3[0] = array4[0];
                array3[1] = array4[1];
                array3[2] = array4[2];
                ++n6;
            }
            ++i;
        }
        this.mCurveFit = CurveFit.get(n, array, array2);
    }
    
    @Override
    public String toString() {
        String str = this.mType;
        final DecimalFormat decimalFormat = new DecimalFormat("##.##");
        for (int i = 0; i < this.count; ++i) {
            final StringBuilder sb = new StringBuilder();
            sb.append(str);
            sb.append("[");
            sb.append(this.mTimePoints[i]);
            sb.append(" , ");
            sb.append(decimalFormat.format(this.mValues[i]));
            sb.append("] ");
            str = sb.toString();
        }
        return str;
    }
    
    public static class CustomSet extends TimeCycleSplineSet
    {
        String mAttributeName;
        float[] mCache;
        KeyFrameArray.CustomArray mConstraintAttributeList;
        float[] mTempValues;
        KeyFrameArray.FloatArray mWaveProperties;
        
        public CustomSet(final String s, final KeyFrameArray.CustomArray mConstraintAttributeList) {
            this.mWaveProperties = new KeyFrameArray.FloatArray();
            this.mAttributeName = s.split(",")[1];
            this.mConstraintAttributeList = mConstraintAttributeList;
        }
        
        @Override
        public void setPoint(final int n, final float n2, final float n3, final int n4, final float n5) {
            throw new RuntimeException("don't call for custom attribute call setPoint(pos, ConstraintAttribute,...)");
        }
        
        public void setPoint(final int n, final CustomAttribute customAttribute, final float n2, final int b, final float n3) {
            this.mConstraintAttributeList.append(n, customAttribute);
            this.mWaveProperties.append(n, new float[] { n2, n3 });
            this.mWaveShape = Math.max(this.mWaveShape, b);
        }
        
        public boolean setProperty(final MotionWidget motionWidget, float n, final long last_time, final KeyCache keyCache) {
            this.mCurveFit.getPos(n, this.mTempValues);
            final float[] mTempValues = this.mTempValues;
            n = mTempValues[mTempValues.length - 2];
            final float n2 = mTempValues[mTempValues.length - 1];
            final long last_time2 = this.last_time;
            if (Float.isNaN(this.last_cycle)) {
                this.last_cycle = keyCache.getFloatValue(motionWidget, this.mAttributeName, 0);
                if (Float.isNaN(this.last_cycle)) {
                    this.last_cycle = 0.0f;
                }
            }
            this.last_cycle = (float)((this.last_cycle + (last_time - last_time2) * 1.0E-9 * n) % 1.0);
            this.last_time = last_time;
            final float calcWave = this.calcWave(this.last_cycle);
            this.mContinue = false;
            for (int i = 0; i < this.mCache.length; ++i) {
                this.mContinue |= (this.mTempValues[i] != 0.0);
                this.mCache[i] = this.mTempValues[i] * calcWave + n2;
            }
            motionWidget.setInterpolatedValue(this.mConstraintAttributeList.valueAt(0), this.mCache);
            if (n != 0.0f) {
                this.mContinue = true;
            }
            return this.mContinue;
        }
        
        @Override
        public void setup(final int n) {
            final int size = this.mConstraintAttributeList.size();
            final int numberOfInterpolatedValues = this.mConstraintAttributeList.valueAt(0).numberOfInterpolatedValues();
            final double[] array = new double[size];
            final int n2 = numberOfInterpolatedValues + 2;
            this.mTempValues = new float[n2];
            this.mCache = new float[numberOfInterpolatedValues];
            final double[][] array2 = new double[size][n2];
            for (int i = 0; i < size; ++i) {
                final int key = this.mConstraintAttributeList.keyAt(i);
                final CustomAttribute value = this.mConstraintAttributeList.valueAt(i);
                final float[] value2 = this.mWaveProperties.valueAt(i);
                array[i] = key * 0.01;
                value.getValuesToInterpolate(this.mTempValues);
                int n3 = 0;
                while (true) {
                    final float[] mTempValues = this.mTempValues;
                    if (n3 >= mTempValues.length) {
                        break;
                    }
                    array2[i][n3] = mTempValues[n3];
                    ++n3;
                }
                final double[] array3 = array2[i];
                array3[numberOfInterpolatedValues] = value2[0];
                array3[numberOfInterpolatedValues + 1] = value2[1];
            }
            this.mCurveFit = CurveFit.get(n, array, array2);
        }
    }
    
    public static class CustomVarSet extends TimeCycleSplineSet
    {
        String mAttributeName;
        float[] mCache;
        KeyFrameArray.CustomVar mConstraintAttributeList;
        float[] mTempValues;
        KeyFrameArray.FloatArray mWaveProperties;
        
        public CustomVarSet(final String s, final KeyFrameArray.CustomVar mConstraintAttributeList) {
            this.mWaveProperties = new KeyFrameArray.FloatArray();
            this.mAttributeName = s.split(",")[1];
            this.mConstraintAttributeList = mConstraintAttributeList;
        }
        
        @Override
        public void setPoint(final int n, final float n2, final float n3, final int n4, final float n5) {
            throw new RuntimeException("don't call for custom attribute call setPoint(pos, ConstraintAttribute,...)");
        }
        
        public void setPoint(final int n, final CustomVariable customVariable, final float n2, final int b, final float n3) {
            this.mConstraintAttributeList.append(n, customVariable);
            this.mWaveProperties.append(n, new float[] { n2, n3 });
            this.mWaveShape = Math.max(this.mWaveShape, b);
        }
        
        public boolean setProperty(final MotionWidget motionWidget, float calcWave, final long last_time, final KeyCache keyCache) {
            this.mCurveFit.getPos(calcWave, this.mTempValues);
            final float[] mTempValues = this.mTempValues;
            final float n = mTempValues[mTempValues.length - 2];
            final float n2 = mTempValues[mTempValues.length - 1];
            final long last_time2 = this.last_time;
            if (Float.isNaN(this.last_cycle)) {
                this.last_cycle = keyCache.getFloatValue(motionWidget, this.mAttributeName, 0);
                if (Float.isNaN(this.last_cycle)) {
                    this.last_cycle = 0.0f;
                }
            }
            this.last_cycle = (float)((this.last_cycle + (last_time - last_time2) * 1.0E-9 * n) % 1.0);
            this.last_time = last_time;
            calcWave = this.calcWave(this.last_cycle);
            this.mContinue = false;
            for (int i = 0; i < this.mCache.length; ++i) {
                this.mContinue |= (this.mTempValues[i] != 0.0);
                this.mCache[i] = this.mTempValues[i] * calcWave + n2;
            }
            this.mConstraintAttributeList.valueAt(0).setInterpolatedValue(motionWidget, this.mCache);
            if (n != 0.0f) {
                this.mContinue = true;
            }
            return this.mContinue;
        }
        
        @Override
        public void setup(final int n) {
            final int size = this.mConstraintAttributeList.size();
            final int numberOfInterpolatedValues = this.mConstraintAttributeList.valueAt(0).numberOfInterpolatedValues();
            final double[] array = new double[size];
            final int n2 = numberOfInterpolatedValues + 2;
            this.mTempValues = new float[n2];
            this.mCache = new float[numberOfInterpolatedValues];
            final double[][] array2 = new double[size][n2];
            for (int i = 0; i < size; ++i) {
                final int key = this.mConstraintAttributeList.keyAt(i);
                final CustomVariable value = this.mConstraintAttributeList.valueAt(i);
                final float[] value2 = this.mWaveProperties.valueAt(i);
                array[i] = key * 0.01;
                value.getValuesToInterpolate(this.mTempValues);
                int n3 = 0;
                while (true) {
                    final float[] mTempValues = this.mTempValues;
                    if (n3 >= mTempValues.length) {
                        break;
                    }
                    array2[i][n3] = mTempValues[n3];
                    ++n3;
                }
                final double[] array3 = array2[i];
                array3[numberOfInterpolatedValues] = value2[0];
                array3[numberOfInterpolatedValues + 1] = value2[1];
            }
            this.mCurveFit = CurveFit.get(n, array, array2);
        }
    }
    
    protected static class Sort
    {
        static void doubleQuickSort(final int[] array, final float[][] array2, int i, int n) {
            final int[] array3 = new int[array.length + 10];
            array3[0] = n;
            array3[1] = i;
            int n2;
            int n3;
            int partition = 0;
            int n4;
            for (i = 2; i > 0; i = n + 1, array3[n] = partition - 1, n = i + 1, array3[i] = n2, n4 = n + 1, array3[n] = n3, i = n4 + 1, array3[n4] = partition + 1) {
                --i;
                n2 = array3[i];
                n = i - 1;
                n3 = array3[n];
                i = n;
                if (n2 < n3) {
                    partition = partition(array, array2, n2, n3);
                }
            }
        }
        
        private static int partition(final int[] array, final float[][] array2, int i, final int n) {
            final int n2 = array[n];
            int n3 = i;
            while (i < n) {
                int n4 = n3;
                if (array[i] <= n2) {
                    swap(array, array2, n3, i);
                    n4 = n3 + 1;
                }
                ++i;
                n3 = n4;
            }
            swap(array, array2, n3, n);
            return n3;
        }
        
        private static void swap(final int[] array, final float[][] array2, final int n, final int n2) {
            final int n3 = array[n];
            array[n] = array[n2];
            array[n2] = n3;
            final float[] array3 = array2[n];
            array2[n] = array2[n2];
            array2[n2] = array3;
        }
    }
}

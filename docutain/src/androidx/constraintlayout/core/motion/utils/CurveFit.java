// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.core.motion.utils;

public abstract class CurveFit
{
    public static final int CONSTANT = 2;
    public static final int LINEAR = 1;
    public static final int SPLINE = 0;
    
    public static CurveFit get(int n, final double[] array, final double[][] array2) {
        if (array.length == 1) {
            n = 2;
        }
        if (n == 0) {
            return new MonotonicCurveFit(array, array2);
        }
        if (n != 2) {
            return new LinearCurveFit(array, array2);
        }
        return new Constant(array[0], array2[0]);
    }
    
    public static CurveFit getArc(final int[] array, final double[] array2, final double[][] array3) {
        return new ArcCurveFit(array, array2, array3);
    }
    
    public abstract double getPos(final double p0, final int p1);
    
    public abstract void getPos(final double p0, final double[] p1);
    
    public abstract void getPos(final double p0, final float[] p1);
    
    public abstract double getSlope(final double p0, final int p1);
    
    public abstract void getSlope(final double p0, final double[] p1);
    
    public abstract double[] getTimePoints();
    
    static class Constant extends CurveFit
    {
        double mTime;
        double[] mValue;
        
        Constant(final double mTime, final double[] mValue) {
            this.mTime = mTime;
            this.mValue = mValue;
        }
        
        @Override
        public double getPos(final double n, final int n2) {
            return this.mValue[n2];
        }
        
        @Override
        public void getPos(final double n, final double[] array) {
            final double[] mValue = this.mValue;
            System.arraycopy(mValue, 0, array, 0, mValue.length);
        }
        
        @Override
        public void getPos(final double n, final float[] array) {
            int n2 = 0;
            while (true) {
                final double[] mValue = this.mValue;
                if (n2 >= mValue.length) {
                    break;
                }
                array[n2] = (float)mValue[n2];
                ++n2;
            }
        }
        
        @Override
        public double getSlope(final double n, final int n2) {
            return 0.0;
        }
        
        @Override
        public void getSlope(final double n, final double[] array) {
            for (int i = 0; i < this.mValue.length; ++i) {
                array[i] = 0.0;
            }
        }
        
        @Override
        public double[] getTimePoints() {
            return new double[] { this.mTime };
        }
    }
}

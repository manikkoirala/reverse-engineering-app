// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.core.motion.utils;

import java.io.PrintStream;
import java.util.Arrays;

public class Easing
{
    private static final String ACCELERATE = "cubic(0.4, 0.05, 0.8, 0.7)";
    private static final String ACCELERATE_NAME = "accelerate";
    private static final String ANTICIPATE = "cubic(0.36, 0, 0.66, -0.56)";
    private static final String ANTICIPATE_NAME = "anticipate";
    private static final String DECELERATE = "cubic(0.0, 0.0, 0.2, 0.95)";
    private static final String DECELERATE_NAME = "decelerate";
    private static final String LINEAR = "cubic(1, 1, 0, 0)";
    private static final String LINEAR_NAME = "linear";
    public static String[] NAMED_EASING;
    private static final String OVERSHOOT = "cubic(0.34, 1.56, 0.64, 1)";
    private static final String OVERSHOOT_NAME = "overshoot";
    private static final String STANDARD = "cubic(0.4, 0.0, 0.2, 1)";
    private static final String STANDARD_NAME = "standard";
    static Easing sDefault;
    String str;
    
    static {
        Easing.sDefault = new Easing();
        Easing.NAMED_EASING = new String[] { "standard", "accelerate", "decelerate", "linear" };
    }
    
    public Easing() {
        this.str = "identity";
    }
    
    public static Easing getInterpolator(final String s) {
        if (s == null) {
            return null;
        }
        if (s.startsWith("cubic")) {
            return new CubicEasing(s);
        }
        if (s.startsWith("spline")) {
            return new StepCurve(s);
        }
        if (s.startsWith("Schlick")) {
            return new Schlick(s);
        }
        s.hashCode();
        int n = -1;
        switch (s) {
            case "standard": {
                n = 5;
                break;
            }
            case "overshoot": {
                n = 4;
                break;
            }
            case "linear": {
                n = 3;
                break;
            }
            case "anticipate": {
                n = 2;
                break;
            }
            case "decelerate": {
                n = 1;
                break;
            }
            case "accelerate": {
                n = 0;
                break;
            }
            default:
                break;
        }
        switch (n) {
            default: {
                final PrintStream err = System.err;
                final StringBuilder sb = new StringBuilder();
                sb.append("transitionEasing syntax error syntax:transitionEasing=\"cubic(1.0,0.5,0.0,0.6)\" or ");
                sb.append(Arrays.toString(Easing.NAMED_EASING));
                err.println(sb.toString());
                return Easing.sDefault;
            }
            case 5: {
                return new CubicEasing("cubic(0.4, 0.0, 0.2, 1)");
            }
            case 4: {
                return new CubicEasing("cubic(0.34, 1.56, 0.64, 1)");
            }
            case 3: {
                return new CubicEasing("cubic(1, 1, 0, 0)");
            }
            case 2: {
                return new CubicEasing("cubic(0.36, 0, 0.66, -0.56)");
            }
            case 1: {
                return new CubicEasing("cubic(0.0, 0.0, 0.2, 0.95)");
            }
            case 0: {
                return new CubicEasing("cubic(0.4, 0.05, 0.8, 0.7)");
            }
        }
    }
    
    public double get(final double n) {
        return n;
    }
    
    public double getDiff(final double n) {
        return 1.0;
    }
    
    @Override
    public String toString() {
        return this.str;
    }
    
    static class CubicEasing extends Easing
    {
        private static double d_error = 1.0E-4;
        private static double error = 0.01;
        double x1;
        double x2;
        double y1;
        double y2;
        
        public CubicEasing(final double n, final double n2, final double n3, final double n4) {
            this.setup(n, n2, n3, n4);
        }
        
        CubicEasing(final String str) {
            this.str = str;
            final int index = str.indexOf(40);
            final int index2 = str.indexOf(44, index);
            this.x1 = Double.parseDouble(str.substring(index + 1, index2).trim());
            final int n = index2 + 1;
            final int index3 = str.indexOf(44, n);
            this.y1 = Double.parseDouble(str.substring(n, index3).trim());
            final int n2 = index3 + 1;
            final int index4 = str.indexOf(44, n2);
            this.x2 = Double.parseDouble(str.substring(n2, index4).trim());
            final int n3 = index4 + 1;
            this.y2 = Double.parseDouble(str.substring(n3, str.indexOf(41, n3)).trim());
        }
        
        private double getDiffX(final double n) {
            final double n2 = 1.0 - n;
            final double x1 = this.x1;
            final double x2 = this.x2;
            return n2 * 3.0 * n2 * x1 + n2 * 6.0 * n * (x2 - x1) + 3.0 * n * n * (1.0 - x2);
        }
        
        private double getDiffY(final double n) {
            final double n2 = 1.0 - n;
            final double y1 = this.y1;
            final double y2 = this.y2;
            return n2 * 3.0 * n2 * y1 + n2 * 6.0 * n * (y2 - y1) + 3.0 * n * n * (1.0 - y2);
        }
        
        private double getX(final double n) {
            final double n2 = 1.0 - n;
            final double n3 = 3.0 * n2;
            return this.x1 * (n2 * n3 * n) + this.x2 * (n3 * n * n) + n * n * n;
        }
        
        private double getY(final double n) {
            final double n2 = 1.0 - n;
            final double n3 = 3.0 * n2;
            return this.y1 * (n2 * n3 * n) + this.y2 * (n3 * n * n) + n * n * n;
        }
        
        @Override
        public double get(final double n) {
            if (n <= 0.0) {
                return 0.0;
            }
            if (n >= 1.0) {
                return 1.0;
            }
            double n2 = 0.5;
            double n3 = 0.5;
            while (n2 > CubicEasing.error) {
                final double x = this.getX(n3);
                n2 *= 0.5;
                if (x < n) {
                    n3 += n2;
                }
                else {
                    n3 -= n2;
                }
            }
            final double n4 = n3 - n2;
            final double x2 = this.getX(n4);
            final double n5 = n3 + n2;
            final double x3 = this.getX(n5);
            final double y = this.getY(n4);
            return (this.getY(n5) - y) * (n - x2) / (x3 - x2) + y;
        }
        
        @Override
        public double getDiff(double x) {
            double n = 0.5;
            double n2 = 0.5;
            while (n > CubicEasing.d_error) {
                final double x2 = this.getX(n2);
                n *= 0.5;
                if (x2 < x) {
                    n2 += n;
                }
                else {
                    n2 -= n;
                }
            }
            final double n3 = n2 - n;
            x = this.getX(n3);
            final double n4 = n2 + n;
            return (this.getY(n4) - this.getY(n3)) / (this.getX(n4) - x);
        }
        
        void setup(final double x1, final double y1, final double x2, final double y2) {
            this.x1 = x1;
            this.y1 = y1;
            this.x2 = x2;
            this.y2 = y2;
        }
    }
}

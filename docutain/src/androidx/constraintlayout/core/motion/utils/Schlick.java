// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.core.motion.utils;

public class Schlick extends Easing
{
    private static final boolean DEBUG = false;
    double eps;
    double mS;
    double mT;
    
    Schlick(final String str) {
        this.str = str;
        final int index = str.indexOf(40);
        int index2 = str.indexOf(44, index);
        this.mS = Double.parseDouble(str.substring(index + 1, index2).trim());
        ++index2;
        this.mT = Double.parseDouble(str.substring(index2, str.indexOf(44, index2)).trim());
    }
    
    private double dfunc(final double n) {
        final double mt = this.mT;
        if (n < mt) {
            final double ms = this.mS;
            return ms * mt * mt / (((mt - n) * ms + n) * (ms * (mt - n) + n));
        }
        final double ms2 = this.mS;
        return (mt - 1.0) * ms2 * (mt - 1.0) / ((-ms2 * (mt - n) - n + 1.0) * (-ms2 * (mt - n) - n + 1.0));
    }
    
    private double func(final double n) {
        final double mt = this.mT;
        if (n < mt) {
            return mt * n / (n + this.mS * (mt - n));
        }
        return (1.0 - mt) * (n - 1.0) / (1.0 - n - this.mS * (mt - n));
    }
    
    @Override
    public double get(final double n) {
        return this.func(n);
    }
    
    @Override
    public double getDiff(final double n) {
        return this.dfunc(n);
    }
}

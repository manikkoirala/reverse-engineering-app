// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.core.motion.utils;

import androidx.constraintlayout.core.motion.MotionWidget;
import androidx.constraintlayout.core.motion.CustomVariable;
import androidx.constraintlayout.core.state.WidgetFrame;
import androidx.constraintlayout.core.motion.CustomAttribute;
import java.text.DecimalFormat;
import java.util.Arrays;

public abstract class SplineSet
{
    private static final String TAG = "SplineSet";
    private int count;
    protected CurveFit mCurveFit;
    protected int[] mTimePoints;
    private String mType;
    protected float[] mValues;
    
    public SplineSet() {
        this.mTimePoints = new int[10];
        this.mValues = new float[10];
    }
    
    public static SplineSet makeCustomSpline(final String s, final KeyFrameArray.CustomArray customArray) {
        return new CustomSet(s, customArray);
    }
    
    public static SplineSet makeCustomSplineSet(final String s, final KeyFrameArray.CustomVar customVar) {
        return new CustomSpline(s, customVar);
    }
    
    public static SplineSet makeSpline(final String s, final long n) {
        return new CoreSpline(s, n);
    }
    
    public float get(final float n) {
        return (float)this.mCurveFit.getPos(n, 0);
    }
    
    public CurveFit getCurveFit() {
        return this.mCurveFit;
    }
    
    public float getSlope(final float n) {
        return (float)this.mCurveFit.getSlope(n, 0);
    }
    
    public void setPoint(final int n, final float n2) {
        final int[] mTimePoints = this.mTimePoints;
        if (mTimePoints.length < this.count + 1) {
            this.mTimePoints = Arrays.copyOf(mTimePoints, mTimePoints.length * 2);
            final float[] mValues = this.mValues;
            this.mValues = Arrays.copyOf(mValues, mValues.length * 2);
        }
        final int[] mTimePoints2 = this.mTimePoints;
        final int count = this.count;
        mTimePoints2[count] = n;
        this.mValues[count] = n2;
        this.count = count + 1;
    }
    
    public void setProperty(final TypedValues typedValues, final float n) {
        typedValues.setValue(TypedValues$AttributesType$_CC.getId(this.mType), this.get(n));
    }
    
    public void setType(final String mType) {
        this.mType = mType;
    }
    
    public void setup(final int n) {
        final int count = this.count;
        if (count == 0) {
            return;
        }
        Sort.doubleQuickSort(this.mTimePoints, this.mValues, 0, count - 1);
        int i = 1;
        int n2 = 1;
        while (i < this.count) {
            final int[] mTimePoints = this.mTimePoints;
            int n3 = n2;
            if (mTimePoints[i - 1] != mTimePoints[i]) {
                n3 = n2 + 1;
            }
            ++i;
            n2 = n3;
        }
        final double[] array = new double[n2];
        final double[][] array2 = new double[n2][1];
        int j = 0;
        int n4 = 0;
        while (j < this.count) {
            Label_0156: {
                if (j > 0) {
                    final int[] mTimePoints2 = this.mTimePoints;
                    if (mTimePoints2[j] == mTimePoints2[j - 1]) {
                        break Label_0156;
                    }
                }
                array[n4] = this.mTimePoints[j] * 0.01;
                array2[n4][0] = this.mValues[j];
                ++n4;
            }
            ++j;
        }
        this.mCurveFit = CurveFit.get(n, array, array2);
    }
    
    @Override
    public String toString() {
        String str = this.mType;
        final DecimalFormat decimalFormat = new DecimalFormat("##.##");
        for (int i = 0; i < this.count; ++i) {
            final StringBuilder sb = new StringBuilder();
            sb.append(str);
            sb.append("[");
            sb.append(this.mTimePoints[i]);
            sb.append(" , ");
            sb.append(decimalFormat.format(this.mValues[i]));
            sb.append("] ");
            str = sb.toString();
        }
        return str;
    }
    
    private static class CoreSpline extends SplineSet
    {
        long start;
        String type;
        
        public CoreSpline(final String type, final long start) {
            this.type = type;
            this.start = start;
        }
        
        @Override
        public void setProperty(final TypedValues typedValues, final float n) {
            typedValues.setValue(typedValues.getId(this.type), this.get(n));
        }
    }
    
    public static class CustomSet extends SplineSet
    {
        String mAttributeName;
        KeyFrameArray.CustomArray mConstraintAttributeList;
        float[] mTempValues;
        
        public CustomSet(final String s, final KeyFrameArray.CustomArray mConstraintAttributeList) {
            this.mAttributeName = s.split(",")[1];
            this.mConstraintAttributeList = mConstraintAttributeList;
        }
        
        @Override
        public void setPoint(final int n, final float n2) {
            throw new RuntimeException("don't call for custom attribute call setPoint(pos, ConstraintAttribute)");
        }
        
        public void setPoint(final int n, final CustomAttribute customAttribute) {
            this.mConstraintAttributeList.append(n, customAttribute);
        }
        
        public void setProperty(final WidgetFrame widgetFrame, final float n) {
            this.mCurveFit.getPos(n, this.mTempValues);
            widgetFrame.setCustomValue(this.mConstraintAttributeList.valueAt(0), this.mTempValues);
        }
        
        @Override
        public void setup(final int n) {
            final int size = this.mConstraintAttributeList.size();
            final int numberOfInterpolatedValues = this.mConstraintAttributeList.valueAt(0).numberOfInterpolatedValues();
            final double[] array = new double[size];
            this.mTempValues = new float[numberOfInterpolatedValues];
            final double[][] array2 = new double[size][numberOfInterpolatedValues];
            for (int i = 0; i < size; ++i) {
                final int key = this.mConstraintAttributeList.keyAt(i);
                final CustomAttribute value = this.mConstraintAttributeList.valueAt(i);
                array[i] = key * 0.01;
                value.getValuesToInterpolate(this.mTempValues);
                int n2 = 0;
                while (true) {
                    final float[] mTempValues = this.mTempValues;
                    if (n2 >= mTempValues.length) {
                        break;
                    }
                    array2[i][n2] = mTempValues[n2];
                    ++n2;
                }
            }
            this.mCurveFit = CurveFit.get(n, array, array2);
        }
    }
    
    public static class CustomSpline extends SplineSet
    {
        String mAttributeName;
        KeyFrameArray.CustomVar mConstraintAttributeList;
        float[] mTempValues;
        
        public CustomSpline(final String s, final KeyFrameArray.CustomVar mConstraintAttributeList) {
            this.mAttributeName = s.split(",")[1];
            this.mConstraintAttributeList = mConstraintAttributeList;
        }
        
        @Override
        public void setPoint(final int n, final float n2) {
            throw new RuntimeException("don't call for custom attribute call setPoint(pos, ConstraintAttribute)");
        }
        
        public void setPoint(final int n, final CustomVariable customVariable) {
            this.mConstraintAttributeList.append(n, customVariable);
        }
        
        public void setProperty(final MotionWidget motionWidget, final float n) {
            this.mCurveFit.getPos(n, this.mTempValues);
            this.mConstraintAttributeList.valueAt(0).setInterpolatedValue(motionWidget, this.mTempValues);
        }
        
        @Override
        public void setProperty(final TypedValues typedValues, final float n) {
            this.setProperty((MotionWidget)typedValues, n);
        }
        
        @Override
        public void setup(final int n) {
            final int size = this.mConstraintAttributeList.size();
            final int numberOfInterpolatedValues = this.mConstraintAttributeList.valueAt(0).numberOfInterpolatedValues();
            final double[] array = new double[size];
            this.mTempValues = new float[numberOfInterpolatedValues];
            final double[][] array2 = new double[size][numberOfInterpolatedValues];
            for (int i = 0; i < size; ++i) {
                final int key = this.mConstraintAttributeList.keyAt(i);
                final CustomVariable value = this.mConstraintAttributeList.valueAt(i);
                array[i] = key * 0.01;
                value.getValuesToInterpolate(this.mTempValues);
                int n2 = 0;
                while (true) {
                    final float[] mTempValues = this.mTempValues;
                    if (n2 >= mTempValues.length) {
                        break;
                    }
                    array2[i][n2] = mTempValues[n2];
                    ++n2;
                }
            }
            this.mCurveFit = CurveFit.get(n, array, array2);
        }
    }
    
    private static class Sort
    {
        static void doubleQuickSort(final int[] array, final float[] array2, int i, int n) {
            final int[] array3 = new int[array.length + 10];
            array3[0] = n;
            array3[1] = i;
            int n2;
            int n3;
            int partition = 0;
            int n4;
            for (i = 2; i > 0; i = n + 1, array3[n] = partition - 1, n = i + 1, array3[i] = n2, n4 = n + 1, array3[n] = n3, i = n4 + 1, array3[n4] = partition + 1) {
                --i;
                n2 = array3[i];
                n = i - 1;
                n3 = array3[n];
                i = n;
                if (n2 < n3) {
                    partition = partition(array, array2, n2, n3);
                }
            }
        }
        
        private static int partition(final int[] array, final float[] array2, int i, final int n) {
            final int n2 = array[n];
            int n3 = i;
            while (i < n) {
                int n4 = n3;
                if (array[i] <= n2) {
                    swap(array, array2, n3, i);
                    n4 = n3 + 1;
                }
                ++i;
                n3 = n4;
            }
            swap(array, array2, n3, n);
            return n3;
        }
        
        private static void swap(final int[] array, final float[] array2, final int n, final int n2) {
            final int n3 = array[n];
            array[n] = array[n2];
            array[n2] = n3;
            final float n4 = array2[n];
            array2[n] = array2[n2];
            array2[n2] = n4;
        }
    }
}

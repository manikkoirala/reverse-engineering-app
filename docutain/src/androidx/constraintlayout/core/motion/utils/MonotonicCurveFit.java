// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.core.motion.utils;

import java.util.Arrays;

public class MonotonicCurveFit extends CurveFit
{
    private static final String TAG = "MonotonicCurveFit";
    private boolean mExtrapolate;
    double[] mSlopeTemp;
    private double[] mT;
    private double[][] mTangent;
    private double[][] mY;
    
    public MonotonicCurveFit(final double[] mt, final double[][] my) {
        this.mExtrapolate = true;
        final int length = mt.length;
        final int length2 = my[0].length;
        this.mSlopeTemp = new double[length2];
        final int n = length - 1;
        final double[][] array = new double[n][length2];
        final double[][] mTangent = new double[length][length2];
        for (int i = 0; i < length2; ++i) {
            int n2;
            for (int j = 0; j < n; j = n2) {
                n2 = j + 1;
                final double n3 = mt[n2];
                final double n4 = mt[j];
                final double[] array2 = array[j];
                final double n5 = (my[n2][i] - my[j][i]) / (n3 - n4);
                array2[i] = n5;
                if (j == 0) {
                    mTangent[j][i] = n5;
                }
                else {
                    mTangent[j][i] = (array[j - 1][i] + n5) * 0.5;
                }
            }
            mTangent[n][i] = array[length - 2][i];
        }
        for (int k = 0; k < n; ++k) {
            for (int l = 0; l < length2; ++l) {
                final double n6 = array[k][l];
                if (n6 == 0.0) {
                    mTangent[k][l] = 0.0;
                    mTangent[k + 1][l] = 0.0;
                }
                else {
                    final double x = mTangent[k][l] / n6;
                    final int n7 = k + 1;
                    final double y = mTangent[n7][l] / n6;
                    final double hypot = Math.hypot(x, y);
                    if (hypot > 9.0) {
                        final double n8 = 3.0 / hypot;
                        final double[] array3 = mTangent[k];
                        final double[] array4 = array[k];
                        array3[l] = x * n8 * array4[l];
                        mTangent[n7][l] = n8 * y * array4[l];
                    }
                }
            }
        }
        this.mT = mt;
        this.mY = my;
        this.mTangent = mTangent;
    }
    
    public static MonotonicCurveFit buildWave(final String s) {
        final double[] original = new double[s.length() / 2];
        int fromIndex;
        int i;
        int n;
        for (fromIndex = s.indexOf(40) + 1, i = s.indexOf(44, fromIndex), n = 0; i != -1; i = s.indexOf(44, fromIndex), ++n) {
            original[n] = Double.parseDouble(s.substring(fromIndex, i).trim());
            fromIndex = i + 1;
        }
        original[n] = Double.parseDouble(s.substring(fromIndex, s.indexOf(41, fromIndex)).trim());
        return buildWave(Arrays.copyOf(original, n + 1));
    }
    
    private static MonotonicCurveFit buildWave(final double[] array) {
        final int n = array.length * 3 - 2;
        final int n2 = array.length - 1;
        final double n3 = 1.0 / n2;
        final double[][] array2 = new double[n][1];
        final double[] array3 = new double[n];
        for (int i = 0; i < array.length; ++i) {
            final double n4 = array[i];
            final int n5 = i + n2;
            array2[n5][0] = n4;
            final double n6 = i * n3;
            array3[n5] = n6;
            if (i > 0) {
                final int n7 = n2 * 2 + i;
                array2[n7][0] = n4 + 1.0;
                array3[n7] = n6 + 1.0;
                final int n8 = i - 1;
                array2[n8][0] = n4 - 1.0 - n3;
                array3[n8] = n6 - 1.0 - n3;
            }
        }
        return new MonotonicCurveFit(array3, array2);
    }
    
    private static double diff(final double n, final double n2, final double n3, final double n4, final double n5, final double n6) {
        final double n7 = n2 * n2;
        final double n8 = n2 * 6.0;
        final double n9 = 3.0 * n;
        return -6.0 * n7 * n4 + n8 * n4 + 6.0 * n7 * n3 - n8 * n3 + n9 * n6 * n7 + n9 * n5 * n7 - 2.0 * n * n6 * n2 - 4.0 * n * n5 * n2 + n * n5;
    }
    
    private static double interpolate(final double n, final double n2, final double n3, final double n4, final double n5, double n6) {
        final double n7 = n2 * n2;
        final double n8 = n7 * n2;
        final double n9 = 3.0 * n7;
        final double n10 = n * n6;
        n6 = n * n5;
        return -2.0 * n8 * n4 + n9 * n4 + n8 * 2.0 * n3 - n9 * n3 + n3 + n10 * n8 + n8 * n6 - n10 * n7 - n * 2.0 * n5 * n7 + n6 * n2;
    }
    
    @Override
    public double getPos(double n, final int n2) {
        final double[] mt = this.mT;
        final int length = mt.length;
        final boolean mExtrapolate = this.mExtrapolate;
        int i = 0;
        Label_0162: {
            if (mExtrapolate) {
                final double n3 = mt[0];
                double n4;
                double n5;
                if (n <= n3) {
                    n4 = this.mY[0][n2];
                    n -= n3;
                    n5 = this.getSlope(n3, n2);
                }
                else {
                    final int n6 = length - 1;
                    final double n7 = mt[n6];
                    if (n < n7) {
                        break Label_0162;
                    }
                    n4 = this.mY[n6][n2];
                    n -= n7;
                    n5 = this.getSlope(n7, n2);
                }
                return n4 + n * n5;
            }
            if (n <= mt[0]) {
                return this.mY[0][n2];
            }
            final int n8 = length - 1;
            if (n >= mt[n8]) {
                return this.mY[n8][n2];
            }
        }
        while (i < length - 1) {
            final double[] mt2 = this.mT;
            final double n9 = mt2[i];
            if (n == n9) {
                return this.mY[i][n2];
            }
            final int n10 = i + 1;
            final double n11 = mt2[n10];
            if (n < n11) {
                final double n12 = n11 - n9;
                n = (n - n9) / n12;
                final double[][] my = this.mY;
                final double n13 = my[i][n2];
                final double n14 = my[n10][n2];
                final double[][] mTangent = this.mTangent;
                return interpolate(n12, n, n13, n14, mTangent[i][n2], mTangent[n10][n2]);
            }
            i = n10;
        }
        return 0.0;
    }
    
    @Override
    public void getPos(double n, final double[] array) {
        final double[] mt = this.mT;
        final int length = mt.length;
        final double[][] my = this.mY;
        final int n2 = 0;
        final int n3 = 0;
        int i = 0;
        final int length2 = my[0].length;
        if (this.mExtrapolate) {
            final double n4 = mt[0];
            if (n <= n4) {
                this.getSlope(n4, this.mSlopeTemp);
                for (int j = 0; j < length2; ++j) {
                    array[j] = this.mY[0][j] + (n - this.mT[0]) * this.mSlopeTemp[j];
                }
                return;
            }
            final int n5 = length - 1;
            final double n6 = mt[n5];
            if (n >= n6) {
                this.getSlope(n6, this.mSlopeTemp);
                while (i < length2) {
                    array[i] = this.mY[n5][i] + (n - this.mT[n5]) * this.mSlopeTemp[i];
                    ++i;
                }
                return;
            }
        }
        else {
            if (n <= mt[0]) {
                for (int k = 0; k < length2; ++k) {
                    array[k] = this.mY[0][k];
                }
                return;
            }
            final int n7 = length - 1;
            if (n >= mt[n7]) {
                for (int l = n2; l < length2; ++l) {
                    array[l] = this.mY[n7][l];
                }
                return;
            }
        }
        int n10;
        for (int n8 = 0; n8 < length - 1; n8 = n10) {
            if (n == this.mT[n8]) {
                for (int n9 = 0; n9 < length2; ++n9) {
                    array[n9] = this.mY[n8][n9];
                }
            }
            final double[] mt2 = this.mT;
            n10 = n8 + 1;
            final double n11 = mt2[n10];
            if (n < n11) {
                final double n12 = mt2[n8];
                final double n13 = n11 - n12;
                n = (n - n12) / n13;
                for (int n14 = n3; n14 < length2; ++n14) {
                    final double[][] my2 = this.mY;
                    final double n15 = my2[n8][n14];
                    final double n16 = my2[n10][n14];
                    final double[][] mTangent = this.mTangent;
                    array[n14] = interpolate(n13, n, n15, n16, mTangent[n8][n14], mTangent[n10][n14]);
                }
                return;
            }
        }
    }
    
    @Override
    public void getPos(double n, final float[] array) {
        final double[] mt = this.mT;
        final int length = mt.length;
        final double[][] my = this.mY;
        final int n2 = 0;
        final int n3 = 0;
        int i = 0;
        final int length2 = my[0].length;
        if (this.mExtrapolate) {
            final double n4 = mt[0];
            if (n <= n4) {
                this.getSlope(n4, this.mSlopeTemp);
                for (int j = 0; j < length2; ++j) {
                    array[j] = (float)(this.mY[0][j] + (n - this.mT[0]) * this.mSlopeTemp[j]);
                }
                return;
            }
            final int n5 = length - 1;
            final double n6 = mt[n5];
            if (n >= n6) {
                this.getSlope(n6, this.mSlopeTemp);
                while (i < length2) {
                    array[i] = (float)(this.mY[n5][i] + (n - this.mT[n5]) * this.mSlopeTemp[i]);
                    ++i;
                }
                return;
            }
        }
        else {
            if (n <= mt[0]) {
                for (int k = 0; k < length2; ++k) {
                    array[k] = (float)this.mY[0][k];
                }
                return;
            }
            final int n7 = length - 1;
            if (n >= mt[n7]) {
                for (int l = n2; l < length2; ++l) {
                    array[l] = (float)this.mY[n7][l];
                }
                return;
            }
        }
        int n10;
        for (int n8 = 0; n8 < length - 1; n8 = n10) {
            if (n == this.mT[n8]) {
                for (int n9 = 0; n9 < length2; ++n9) {
                    array[n9] = (float)this.mY[n8][n9];
                }
            }
            final double[] mt2 = this.mT;
            n10 = n8 + 1;
            final double n11 = mt2[n10];
            if (n < n11) {
                final double n12 = mt2[n8];
                final double n13 = n11 - n12;
                n = (n - n12) / n13;
                for (int n14 = n3; n14 < length2; ++n14) {
                    final double[][] my2 = this.mY;
                    final double n15 = my2[n8][n14];
                    final double n16 = my2[n10][n14];
                    final double[][] mTangent = this.mTangent;
                    array[n14] = (float)interpolate(n13, n, n15, n16, mTangent[n8][n14], mTangent[n10][n14]);
                }
                return;
            }
        }
    }
    
    @Override
    public double getSlope(double n, final int n2) {
        final double[] mt = this.mT;
        final int length = mt.length;
        int i = 0;
        final double n3 = mt[0];
        if (n < n3) {
            n = n3;
        }
        else {
            final double n4 = mt[length - 1];
            if (n >= n4) {
                n = n4;
            }
        }
        while (i < length - 1) {
            final double[] mt2 = this.mT;
            final int n5 = i + 1;
            final double n6 = mt2[n5];
            if (n <= n6) {
                final double n7 = mt2[i];
                final double n8 = n6 - n7;
                final double n9 = (n - n7) / n8;
                final double[][] my = this.mY;
                n = my[i][n2];
                final double n10 = my[n5][n2];
                final double[][] mTangent = this.mTangent;
                return diff(n8, n9, n, n10, mTangent[i][n2], mTangent[n5][n2]) / n8;
            }
            i = n5;
        }
        return 0.0;
    }
    
    @Override
    public void getSlope(double n, final double[] array) {
        final double[] mt = this.mT;
        final int length = mt.length;
        final double[][] my = this.mY;
        int i = 0;
        final int length2 = my[0].length;
        final double n2 = mt[0];
        if (n <= n2) {
            n = n2;
        }
        else {
            final double n3 = mt[length - 1];
            if (n >= n3) {
                n = n3;
            }
        }
        int n4;
        for (int j = 0; j < length - 1; j = n4) {
            final double[] mt2 = this.mT;
            n4 = j + 1;
            final double n5 = mt2[n4];
            if (n <= n5) {
                final double n6 = mt2[j];
                final double n7 = n5 - n6;
                final double n8 = (n - n6) / n7;
                while (i < length2) {
                    final double[][] my2 = this.mY;
                    final double n9 = my2[j][i];
                    n = my2[n4][i];
                    final double[][] mTangent = this.mTangent;
                    array[i] = diff(n7, n8, n9, n, mTangent[j][i], mTangent[n4][i]) / n7;
                    ++i;
                }
                break;
            }
        }
    }
    
    @Override
    public double[] getTimePoints() {
        return this.mT;
    }
}

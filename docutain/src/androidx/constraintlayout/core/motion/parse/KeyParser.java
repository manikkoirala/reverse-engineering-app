// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.core.motion.parse;

import java.io.PrintStream;
import androidx.constraintlayout.core.parser.CLElement;
import androidx.constraintlayout.core.parser.CLObject;
import androidx.constraintlayout.core.parser.CLParsingException;
import androidx.constraintlayout.core.parser.CLKey;
import androidx.constraintlayout.core.parser.CLParser;
import androidx.constraintlayout.core.motion.utils.TypedBundle;

public class KeyParser
{
    public static void main(final String[] array) {
        parseAttributes("{frame:22,\ntarget:'widget1',\neasing:'easeIn',\ncurveFit:'spline',\nprogress:0.3,\nalpha:0.2,\nelevation:0.7,\nrotationZ:23,\nrotationX:25.0,\nrotationY:27.0,\npivotX:15,\npivotY:17,\npivotTarget:'32',\npathRotate:23,\nscaleX:0.5,\nscaleY:0.7,\ntranslationX:5,\ntranslationY:7,\ntranslationZ:11,\n}");
    }
    
    private static TypedBundle parse(final String s, final Ids ids, final DataType dataType) {
        final TypedBundle typedBundle = new TypedBundle();
        try {
            final CLObject parse = CLParser.parse(s);
            for (int size = parse.size(), i = 0; i < size; ++i) {
                final CLKey clKey = (CLKey)parse.get(i);
                final String content = clKey.content();
                final CLElement value = clKey.getValue();
                final int value2 = ids.get(content);
                if (value2 == -1) {
                    final PrintStream err = System.err;
                    final StringBuilder sb = new StringBuilder();
                    sb.append("unknown type ");
                    sb.append(content);
                    err.println(sb.toString());
                }
                else {
                    final int value3 = dataType.get(value2);
                    if (value3 != 1) {
                        if (value3 != 2) {
                            if (value3 != 4) {
                                if (value3 == 8) {
                                    typedBundle.add(value2, value.content());
                                    final PrintStream out = System.out;
                                    final StringBuilder sb2 = new StringBuilder();
                                    sb2.append("parse ");
                                    sb2.append(content);
                                    sb2.append(" STRING_MASK > ");
                                    sb2.append(value.content());
                                    out.println(sb2.toString());
                                }
                            }
                            else {
                                typedBundle.add(value2, value.getFloat());
                                final PrintStream out2 = System.out;
                                final StringBuilder sb3 = new StringBuilder();
                                sb3.append("parse ");
                                sb3.append(content);
                                sb3.append(" FLOAT_MASK > ");
                                sb3.append(value.getFloat());
                                out2.println(sb3.toString());
                            }
                        }
                        else {
                            typedBundle.add(value2, value.getInt());
                            final PrintStream out3 = System.out;
                            final StringBuilder sb4 = new StringBuilder();
                            sb4.append("parse ");
                            sb4.append(content);
                            sb4.append(" INT_MASK > ");
                            sb4.append(value.getInt());
                            out3.println(sb4.toString());
                        }
                    }
                    else {
                        typedBundle.add(value2, parse.getBoolean(i));
                    }
                }
            }
        }
        catch (final CLParsingException ex) {
            ex.printStackTrace();
        }
        return typedBundle;
    }
    
    public static TypedBundle parseAttributes(final String s) {
        return parse(s, (Ids)new KeyParser$$ExternalSyntheticLambda0(), (DataType)new KeyParser$$ExternalSyntheticLambda1());
    }
    
    private interface DataType
    {
        int get(final int p0);
    }
    
    private interface Ids
    {
        int get(final String p0);
    }
}

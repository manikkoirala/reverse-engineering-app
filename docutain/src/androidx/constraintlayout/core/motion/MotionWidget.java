// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.core.motion;

import androidx.constraintlayout.core.widgets.ConstraintWidget;
import androidx.constraintlayout.core.motion.utils.TypedValues$MotionType$_CC;
import androidx.constraintlayout.core.motion.utils.TypedValues$AttributesType$_CC;
import java.util.Set;
import androidx.constraintlayout.core.state.WidgetFrame;
import androidx.constraintlayout.core.motion.utils.TypedValues;

public class MotionWidget implements TypedValues
{
    public static final int FILL_PARENT = -1;
    public static final int GONE_UNSET = Integer.MIN_VALUE;
    private static final int INTERNAL_MATCH_CONSTRAINT = -3;
    private static final int INTERNAL_MATCH_PARENT = -1;
    private static final int INTERNAL_WRAP_CONTENT = -2;
    private static final int INTERNAL_WRAP_CONTENT_CONSTRAINED = -4;
    public static final int INVISIBLE = 0;
    public static final int MATCH_CONSTRAINT = 0;
    public static final int MATCH_CONSTRAINT_WRAP = 1;
    public static final int MATCH_PARENT = -1;
    public static final int PARENT_ID = 0;
    public static final int ROTATE_LEFT_OF_PORTRATE = 4;
    public static final int ROTATE_NONE = 0;
    public static final int ROTATE_PORTRATE_OF_LEFT = 2;
    public static final int ROTATE_PORTRATE_OF_RIGHT = 1;
    public static final int ROTATE_RIGHT_OF_PORTRATE = 3;
    public static final int UNSET = -1;
    public static final int VISIBILITY_MODE_IGNORE = 1;
    public static final int VISIBILITY_MODE_NORMAL = 0;
    public static final int VISIBLE = 4;
    public static final int WRAP_CONTENT = -2;
    private float mProgress;
    float mTransitionPathRotate;
    Motion motion;
    PropertySet propertySet;
    WidgetFrame widgetFrame;
    
    public MotionWidget() {
        this.widgetFrame = new WidgetFrame();
        this.motion = new Motion();
        this.propertySet = new PropertySet();
    }
    
    public MotionWidget(final WidgetFrame widgetFrame) {
        this.widgetFrame = new WidgetFrame();
        this.motion = new Motion();
        this.propertySet = new PropertySet();
        this.widgetFrame = widgetFrame;
    }
    
    public MotionWidget findViewById(final int n) {
        return null;
    }
    
    public float getAlpha() {
        return this.propertySet.alpha;
    }
    
    public int getBottom() {
        return this.widgetFrame.bottom;
    }
    
    public CustomVariable getCustomAttribute(final String s) {
        return this.widgetFrame.getCustomAttribute(s);
    }
    
    public Set<String> getCustomAttributeNames() {
        return this.widgetFrame.getCustomAttributeNames();
    }
    
    public int getHeight() {
        return this.widgetFrame.bottom - this.widgetFrame.top;
    }
    
    @Override
    public int getId(final String s) {
        final int id = TypedValues$AttributesType$_CC.getId(s);
        if (id != -1) {
            return id;
        }
        return TypedValues$MotionType$_CC.getId(s);
    }
    
    public int getLeft() {
        return this.widgetFrame.left;
    }
    
    public String getName() {
        return this.widgetFrame.getId();
    }
    
    public MotionWidget getParent() {
        return null;
    }
    
    public float getPivotX() {
        return this.widgetFrame.pivotX;
    }
    
    public float getPivotY() {
        return this.widgetFrame.pivotY;
    }
    
    public int getRight() {
        return this.widgetFrame.right;
    }
    
    public float getRotationX() {
        return this.widgetFrame.rotationX;
    }
    
    public float getRotationY() {
        return this.widgetFrame.rotationY;
    }
    
    public float getRotationZ() {
        return this.widgetFrame.rotationZ;
    }
    
    public float getScaleX() {
        return this.widgetFrame.scaleX;
    }
    
    public float getScaleY() {
        return this.widgetFrame.scaleY;
    }
    
    public int getTop() {
        return this.widgetFrame.top;
    }
    
    public float getTranslationX() {
        return this.widgetFrame.translationX;
    }
    
    public float getTranslationY() {
        return this.widgetFrame.translationY;
    }
    
    public float getTranslationZ() {
        return this.widgetFrame.translationZ;
    }
    
    public float getValueAttributes(final int n) {
        switch (n) {
            default: {
                return Float.NaN;
            }
            case 316: {
                return this.mTransitionPathRotate;
            }
            case 315: {
                return this.mProgress;
            }
            case 314: {
                return this.widgetFrame.pivotY;
            }
            case 313: {
                return this.widgetFrame.pivotX;
            }
            case 312: {
                return this.widgetFrame.scaleY;
            }
            case 311: {
                return this.widgetFrame.scaleX;
            }
            case 310: {
                return this.widgetFrame.rotationZ;
            }
            case 309: {
                return this.widgetFrame.rotationY;
            }
            case 308: {
                return this.widgetFrame.rotationX;
            }
            case 306: {
                return this.widgetFrame.translationZ;
            }
            case 305: {
                return this.widgetFrame.translationY;
            }
            case 304: {
                return this.widgetFrame.translationX;
            }
            case 303: {
                return this.widgetFrame.alpha;
            }
        }
    }
    
    public int getVisibility() {
        return this.propertySet.visibility;
    }
    
    public WidgetFrame getWidgetFrame() {
        return this.widgetFrame;
    }
    
    public int getWidth() {
        return this.widgetFrame.right - this.widgetFrame.left;
    }
    
    public int getX() {
        return this.widgetFrame.left;
    }
    
    public int getY() {
        return this.widgetFrame.top;
    }
    
    public void layout(final int n, final int n2, final int n3, final int n4) {
        this.setBounds(n, n2, n3, n4);
    }
    
    public void setBounds(final int left, final int top, final int right, final int bottom) {
        if (this.widgetFrame == null) {
            final ConstraintWidget constraintWidget = null;
            this.widgetFrame = new WidgetFrame((ConstraintWidget)null);
        }
        this.widgetFrame.top = top;
        this.widgetFrame.left = left;
        this.widgetFrame.right = right;
        this.widgetFrame.bottom = bottom;
    }
    
    public void setCustomAttribute(final String s, final int n, final float n2) {
        this.widgetFrame.setCustomAttribute(s, n, n2);
    }
    
    public void setCustomAttribute(final String s, final int n, final int n2) {
        this.widgetFrame.setCustomAttribute(s, n, n2);
    }
    
    public void setCustomAttribute(final String s, final int n, final String s2) {
        this.widgetFrame.setCustomAttribute(s, n, s2);
    }
    
    public void setCustomAttribute(final String s, final int n, final boolean b) {
        this.widgetFrame.setCustomAttribute(s, n, b);
    }
    
    public void setInterpolatedValue(final CustomAttribute customAttribute, final float[] array) {
        this.widgetFrame.setCustomAttribute(customAttribute.mName, 901, array[0]);
    }
    
    public void setPivotX(final float pivotX) {
        this.widgetFrame.pivotX = pivotX;
    }
    
    public void setPivotY(final float pivotY) {
        this.widgetFrame.pivotY = pivotY;
    }
    
    public void setRotationX(final float rotationX) {
        this.widgetFrame.rotationX = rotationX;
    }
    
    public void setRotationY(final float rotationY) {
        this.widgetFrame.rotationY = rotationY;
    }
    
    public void setRotationZ(final float rotationZ) {
        this.widgetFrame.rotationZ = rotationZ;
    }
    
    public void setScaleX(final float scaleX) {
        this.widgetFrame.scaleX = scaleX;
    }
    
    public void setScaleY(final float scaleY) {
        this.widgetFrame.scaleY = scaleY;
    }
    
    public void setTranslationX(final float translationX) {
        this.widgetFrame.translationX = translationX;
    }
    
    public void setTranslationY(final float translationY) {
        this.widgetFrame.translationY = translationY;
    }
    
    public void setTranslationZ(final float translationZ) {
        this.widgetFrame.translationZ = translationZ;
    }
    
    @Override
    public boolean setValue(final int n, final float n2) {
        return this.setValueAttributes(n, n2) || this.setValueMotion(n, n2);
    }
    
    @Override
    public boolean setValue(final int n, final int n2) {
        return this.setValueAttributes(n, (float)n2);
    }
    
    @Override
    public boolean setValue(final int n, final String s) {
        return this.setValueMotion(n, s);
    }
    
    @Override
    public boolean setValue(final int n, final boolean b) {
        return false;
    }
    
    public boolean setValueAttributes(final int n, final float alpha) {
        switch (n) {
            default: {
                return false;
            }
            case 316: {
                this.mTransitionPathRotate = alpha;
                break;
            }
            case 315: {
                this.mProgress = alpha;
                break;
            }
            case 314: {
                this.widgetFrame.pivotY = alpha;
                break;
            }
            case 313: {
                this.widgetFrame.pivotX = alpha;
                break;
            }
            case 312: {
                this.widgetFrame.scaleY = alpha;
                break;
            }
            case 311: {
                this.widgetFrame.scaleX = alpha;
                break;
            }
            case 310: {
                this.widgetFrame.rotationZ = alpha;
                break;
            }
            case 309: {
                this.widgetFrame.rotationY = alpha;
                break;
            }
            case 308: {
                this.widgetFrame.rotationX = alpha;
                break;
            }
            case 306: {
                this.widgetFrame.translationZ = alpha;
                break;
            }
            case 305: {
                this.widgetFrame.translationY = alpha;
                break;
            }
            case 304: {
                this.widgetFrame.translationX = alpha;
                break;
            }
            case 303: {
                this.widgetFrame.alpha = alpha;
                break;
            }
        }
        return true;
    }
    
    public boolean setValueMotion(final int n, final float mMotionStagger) {
        switch (n) {
            default: {
                return false;
            }
            case 602: {
                this.motion.mQuantizeMotionPhase = mMotionStagger;
                break;
            }
            case 601: {
                this.motion.mPathRotate = mMotionStagger;
                break;
            }
            case 600: {
                this.motion.mMotionStagger = mMotionStagger;
                break;
            }
        }
        return true;
    }
    
    public boolean setValueMotion(final int n, final int n2) {
        switch (n) {
            default: {
                return false;
            }
            case 612: {
                this.motion.mQuantizeInterpolatorID = n2;
                break;
            }
            case 611: {
                this.motion.mQuantizeInterpolatorType = n2;
                break;
            }
            case 610: {
                this.motion.mQuantizeMotionSteps = n2;
                break;
            }
            case 609: {
                this.motion.mPolarRelativeTo = n2;
                break;
            }
            case 608: {
                this.motion.mDrawPath = n2;
                break;
            }
            case 607: {
                this.motion.mPathMotionArc = n2;
                break;
            }
            case 606: {
                this.motion.mAnimateCircleAngleTo = n2;
                break;
            }
            case 605: {
                this.motion.mAnimateRelativeTo = n2;
                break;
            }
        }
        return true;
    }
    
    public boolean setValueMotion(final int n, final String s) {
        if (n != 603) {
            if (n != 604) {
                return false;
            }
            this.motion.mQuantizeInterpolatorString = s;
        }
        else {
            this.motion.mTransitionEasing = s;
        }
        return true;
    }
    
    public void setVisibility(final int visibility) {
        this.propertySet.visibility = visibility;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append(this.widgetFrame.left);
        sb.append(", ");
        sb.append(this.widgetFrame.top);
        sb.append(", ");
        sb.append(this.widgetFrame.right);
        sb.append(", ");
        sb.append(this.widgetFrame.bottom);
        return sb.toString();
    }
    
    public static class Motion
    {
        private static final int INTERPOLATOR_REFERENCE_ID = -2;
        private static final int INTERPOLATOR_UNDEFINED = -3;
        private static final int SPLINE_STRING = -1;
        public int mAnimateCircleAngleTo;
        public int mAnimateRelativeTo;
        public int mDrawPath;
        public float mMotionStagger;
        public int mPathMotionArc;
        public float mPathRotate;
        public int mPolarRelativeTo;
        public int mQuantizeInterpolatorID;
        public String mQuantizeInterpolatorString;
        public int mQuantizeInterpolatorType;
        public float mQuantizeMotionPhase;
        public int mQuantizeMotionSteps;
        public String mTransitionEasing;
        
        public Motion() {
            this.mAnimateRelativeTo = -1;
            this.mAnimateCircleAngleTo = 0;
            this.mTransitionEasing = null;
            this.mPathMotionArc = -1;
            this.mDrawPath = 0;
            this.mMotionStagger = Float.NaN;
            this.mPolarRelativeTo = -1;
            this.mPathRotate = Float.NaN;
            this.mQuantizeMotionPhase = Float.NaN;
            this.mQuantizeMotionSteps = -1;
            this.mQuantizeInterpolatorString = null;
            this.mQuantizeInterpolatorType = -3;
            this.mQuantizeInterpolatorID = -1;
        }
    }
    
    public static class PropertySet
    {
        public float alpha;
        public float mProgress;
        public int mVisibilityMode;
        public int visibility;
        
        public PropertySet() {
            this.visibility = 4;
            this.mVisibilityMode = 0;
            this.alpha = 1.0f;
            this.mProgress = Float.NaN;
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.core.widgets;

import androidx.constraintlayout.core.Cache;
import java.util.Iterator;
import androidx.constraintlayout.core.widgets.analyzer.Grouping;
import androidx.constraintlayout.core.widgets.analyzer.WidgetGroup;
import java.util.HashMap;
import java.util.ArrayList;
import androidx.constraintlayout.core.SolverVariable;
import java.util.HashSet;

public class ConstraintAnchor
{
    private static final boolean ALLOW_BINARY = false;
    private static final int UNSET_GONE_MARGIN = Integer.MIN_VALUE;
    private HashSet<ConstraintAnchor> mDependents;
    private int mFinalValue;
    int mGoneMargin;
    private boolean mHasFinalValue;
    public int mMargin;
    public final ConstraintWidget mOwner;
    SolverVariable mSolverVariable;
    public ConstraintAnchor mTarget;
    public final Type mType;
    
    public ConstraintAnchor(final ConstraintWidget mOwner, final Type mType) {
        this.mDependents = null;
        this.mMargin = 0;
        this.mGoneMargin = Integer.MIN_VALUE;
        this.mOwner = mOwner;
        this.mType = mType;
    }
    
    private boolean isConnectionToMe(final ConstraintWidget constraintWidget, final HashSet<ConstraintWidget> set) {
        if (set.contains(constraintWidget)) {
            return false;
        }
        set.add(constraintWidget);
        if (constraintWidget == this.getOwner()) {
            return true;
        }
        final ArrayList<ConstraintAnchor> anchors = constraintWidget.getAnchors();
        for (int size = anchors.size(), i = 0; i < size; ++i) {
            final ConstraintAnchor constraintAnchor = anchors.get(i);
            if (constraintAnchor.isSimilarDimensionConnection(this) && constraintAnchor.isConnected() && this.isConnectionToMe(constraintAnchor.getTarget().getOwner(), set)) {
                return true;
            }
        }
        return false;
    }
    
    public boolean connect(final ConstraintAnchor constraintAnchor, final int n) {
        return this.connect(constraintAnchor, n, Integer.MIN_VALUE, false);
    }
    
    public boolean connect(final ConstraintAnchor mTarget, final int mMargin, final int mGoneMargin, final boolean b) {
        if (mTarget == null) {
            this.reset();
            return true;
        }
        if (!b && !this.isValidConnection(mTarget)) {
            return false;
        }
        this.mTarget = mTarget;
        if (mTarget.mDependents == null) {
            mTarget.mDependents = new HashSet<ConstraintAnchor>();
        }
        final HashSet<ConstraintAnchor> mDependents = this.mTarget.mDependents;
        if (mDependents != null) {
            mDependents.add(this);
        }
        this.mMargin = mMargin;
        this.mGoneMargin = mGoneMargin;
        return true;
    }
    
    public void copyFrom(final ConstraintAnchor constraintAnchor, final HashMap<ConstraintWidget, ConstraintWidget> hashMap) {
        final ConstraintAnchor mTarget = this.mTarget;
        if (mTarget != null) {
            final HashSet<ConstraintAnchor> mDependents = mTarget.mDependents;
            if (mDependents != null) {
                mDependents.remove(this);
            }
        }
        final ConstraintAnchor mTarget2 = constraintAnchor.mTarget;
        if (mTarget2 != null) {
            this.mTarget = hashMap.get(constraintAnchor.mTarget.mOwner).getAnchor(mTarget2.getType());
        }
        else {
            this.mTarget = null;
        }
        final ConstraintAnchor mTarget3 = this.mTarget;
        if (mTarget3 != null) {
            if (mTarget3.mDependents == null) {
                mTarget3.mDependents = new HashSet<ConstraintAnchor>();
            }
            this.mTarget.mDependents.add(this);
        }
        this.mMargin = constraintAnchor.mMargin;
        this.mGoneMargin = constraintAnchor.mGoneMargin;
    }
    
    public void findDependents(final int n, final ArrayList<WidgetGroup> list, final WidgetGroup widgetGroup) {
        final HashSet<ConstraintAnchor> mDependents = this.mDependents;
        if (mDependents != null) {
            final Iterator<ConstraintAnchor> iterator = mDependents.iterator();
            while (iterator.hasNext()) {
                Grouping.findDependents(iterator.next().mOwner, n, list, widgetGroup);
            }
        }
    }
    
    public HashSet<ConstraintAnchor> getDependents() {
        return this.mDependents;
    }
    
    public int getFinalValue() {
        if (!this.mHasFinalValue) {
            return 0;
        }
        return this.mFinalValue;
    }
    
    public int getMargin() {
        if (this.mOwner.getVisibility() == 8) {
            return 0;
        }
        if (this.mGoneMargin != Integer.MIN_VALUE) {
            final ConstraintAnchor mTarget = this.mTarget;
            if (mTarget != null && mTarget.mOwner.getVisibility() == 8) {
                return this.mGoneMargin;
            }
        }
        return this.mMargin;
    }
    
    public final ConstraintAnchor getOpposite() {
        switch (ConstraintAnchor$1.$SwitchMap$androidx$constraintlayout$core$widgets$ConstraintAnchor$Type[this.mType.ordinal()]) {
            default: {
                throw new AssertionError((Object)this.mType.name());
            }
            case 5: {
                return this.mOwner.mTop;
            }
            case 4: {
                return this.mOwner.mBottom;
            }
            case 3: {
                return this.mOwner.mLeft;
            }
            case 2: {
                return this.mOwner.mRight;
            }
            case 1:
            case 6:
            case 7:
            case 8:
            case 9: {
                return null;
            }
        }
    }
    
    public ConstraintWidget getOwner() {
        return this.mOwner;
    }
    
    public SolverVariable getSolverVariable() {
        return this.mSolverVariable;
    }
    
    public ConstraintAnchor getTarget() {
        return this.mTarget;
    }
    
    public Type getType() {
        return this.mType;
    }
    
    public boolean hasCenteredDependents() {
        final HashSet<ConstraintAnchor> mDependents = this.mDependents;
        if (mDependents == null) {
            return false;
        }
        final Iterator<ConstraintAnchor> iterator = mDependents.iterator();
        while (iterator.hasNext()) {
            if (iterator.next().getOpposite().isConnected()) {
                return true;
            }
        }
        return false;
    }
    
    public boolean hasDependents() {
        final HashSet<ConstraintAnchor> mDependents = this.mDependents;
        boolean b = false;
        if (mDependents == null) {
            return false;
        }
        if (mDependents.size() > 0) {
            b = true;
        }
        return b;
    }
    
    public boolean hasFinalValue() {
        return this.mHasFinalValue;
    }
    
    public boolean isConnected() {
        return this.mTarget != null;
    }
    
    public boolean isConnectionAllowed(final ConstraintWidget constraintWidget) {
        if (this.isConnectionToMe(constraintWidget, new HashSet<ConstraintWidget>())) {
            return false;
        }
        final ConstraintWidget parent = this.getOwner().getParent();
        return parent == constraintWidget || constraintWidget.getParent() == parent;
    }
    
    public boolean isConnectionAllowed(final ConstraintWidget constraintWidget, final ConstraintAnchor constraintAnchor) {
        return this.isConnectionAllowed(constraintWidget);
    }
    
    public boolean isSideAnchor() {
        switch (ConstraintAnchor$1.$SwitchMap$androidx$constraintlayout$core$widgets$ConstraintAnchor$Type[this.mType.ordinal()]) {
            default: {
                throw new AssertionError((Object)this.mType.name());
            }
            case 2:
            case 3:
            case 4:
            case 5: {
                return true;
            }
            case 1:
            case 6:
            case 7:
            case 8:
            case 9: {
                return false;
            }
        }
    }
    
    public boolean isSimilarDimensionConnection(final ConstraintAnchor constraintAnchor) {
        final Type type = constraintAnchor.getType();
        final Type mType = this.mType;
        final boolean b = true;
        boolean b2 = true;
        final boolean b3 = true;
        if (type == mType) {
            return true;
        }
        switch (ConstraintAnchor$1.$SwitchMap$androidx$constraintlayout$core$widgets$ConstraintAnchor$Type[this.mType.ordinal()]) {
            default: {
                throw new AssertionError((Object)this.mType.name());
            }
            case 9: {
                return false;
            }
            case 4:
            case 5:
            case 6:
            case 8: {
                boolean b4 = b3;
                if (type != Type.TOP) {
                    b4 = b3;
                    if (type != Type.BOTTOM) {
                        b4 = b3;
                        if (type != Type.CENTER_Y) {
                            b4 = (type == Type.BASELINE && b3);
                        }
                    }
                }
                return b4;
            }
            case 2:
            case 3:
            case 7: {
                boolean b5 = b;
                if (type != Type.LEFT) {
                    b5 = b;
                    if (type != Type.RIGHT) {
                        b5 = (type == Type.CENTER_X && b);
                    }
                }
                return b5;
            }
            case 1: {
                if (type == Type.BASELINE) {
                    b2 = false;
                }
                return b2;
            }
        }
    }
    
    public boolean isValidConnection(final ConstraintAnchor constraintAnchor) {
        final boolean b = false;
        final boolean b2 = false;
        final boolean b3 = false;
        if (constraintAnchor == null) {
            return false;
        }
        final Type type = constraintAnchor.getType();
        final Type mType = this.mType;
        if (type == mType) {
            return mType != Type.BASELINE || (constraintAnchor.getOwner().hasBaseline() && this.getOwner().hasBaseline());
        }
        switch (ConstraintAnchor$1.$SwitchMap$androidx$constraintlayout$core$widgets$ConstraintAnchor$Type[this.mType.ordinal()]) {
            default: {
                throw new AssertionError((Object)this.mType.name());
            }
            case 7:
            case 8:
            case 9: {
                return false;
            }
            case 6: {
                return type != Type.LEFT && type != Type.RIGHT;
            }
            case 4:
            case 5: {
                boolean b4 = type == Type.TOP || type == Type.BOTTOM;
                if (constraintAnchor.getOwner() instanceof Guideline) {
                    boolean b5 = false;
                    Label_0223: {
                        if (!b4) {
                            b5 = b3;
                            if (type != Type.CENTER_Y) {
                                break Label_0223;
                            }
                        }
                        b5 = true;
                    }
                    b4 = b5;
                }
                return b4;
            }
            case 2:
            case 3: {
                boolean b6 = type == Type.LEFT || type == Type.RIGHT;
                if (constraintAnchor.getOwner() instanceof Guideline) {
                    boolean b7 = false;
                    Label_0282: {
                        if (!b6) {
                            b7 = b;
                            if (type != Type.CENTER_X) {
                                break Label_0282;
                            }
                        }
                        b7 = true;
                    }
                    b6 = b7;
                }
                return b6;
            }
            case 1: {
                boolean b8 = b2;
                if (type != Type.BASELINE) {
                    b8 = b2;
                    if (type != Type.CENTER_X) {
                        b8 = b2;
                        if (type != Type.CENTER_Y) {
                            b8 = true;
                        }
                    }
                }
                return b8;
            }
        }
    }
    
    public boolean isVerticalAnchor() {
        switch (ConstraintAnchor$1.$SwitchMap$androidx$constraintlayout$core$widgets$ConstraintAnchor$Type[this.mType.ordinal()]) {
            default: {
                throw new AssertionError((Object)this.mType.name());
            }
            case 4:
            case 5:
            case 6:
            case 8:
            case 9: {
                return true;
            }
            case 1:
            case 2:
            case 3:
            case 7: {
                return false;
            }
        }
    }
    
    public void reset() {
        final ConstraintAnchor mTarget = this.mTarget;
        if (mTarget != null) {
            final HashSet<ConstraintAnchor> mDependents = mTarget.mDependents;
            if (mDependents != null) {
                mDependents.remove(this);
                if (this.mTarget.mDependents.size() == 0) {
                    this.mTarget.mDependents = null;
                }
            }
        }
        this.mDependents = null;
        this.mTarget = null;
        this.mMargin = 0;
        this.mGoneMargin = Integer.MIN_VALUE;
        this.mHasFinalValue = false;
        this.mFinalValue = 0;
    }
    
    public void resetFinalResolution() {
        this.mHasFinalValue = false;
        this.mFinalValue = 0;
    }
    
    public void resetSolverVariable(final Cache cache) {
        final SolverVariable mSolverVariable = this.mSolverVariable;
        if (mSolverVariable == null) {
            this.mSolverVariable = new SolverVariable(SolverVariable.Type.UNRESTRICTED, null);
        }
        else {
            mSolverVariable.reset();
        }
    }
    
    public void setFinalValue(final int mFinalValue) {
        this.mFinalValue = mFinalValue;
        this.mHasFinalValue = true;
    }
    
    public void setGoneMargin(final int mGoneMargin) {
        if (this.isConnected()) {
            this.mGoneMargin = mGoneMargin;
        }
    }
    
    public void setMargin(final int mMargin) {
        if (this.isConnected()) {
            this.mMargin = mMargin;
        }
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append(this.mOwner.getDebugName());
        sb.append(":");
        sb.append(this.mType.toString());
        return sb.toString();
    }
    
    public enum Type
    {
        private static final Type[] $VALUES;
        
        BASELINE, 
        BOTTOM, 
        CENTER, 
        CENTER_X, 
        CENTER_Y, 
        LEFT, 
        NONE, 
        RIGHT, 
        TOP;
    }
}

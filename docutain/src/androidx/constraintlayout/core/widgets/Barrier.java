// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.core.widgets;

import java.util.HashMap;
import androidx.constraintlayout.core.SolverVariable;
import androidx.constraintlayout.core.LinearSystem;

public class Barrier extends HelperWidget
{
    public static final int BOTTOM = 3;
    public static final int LEFT = 0;
    public static final int RIGHT = 1;
    public static final int TOP = 2;
    private static final boolean USE_RELAX_GONE = false;
    private static final boolean USE_RESOLUTION = true;
    private boolean mAllowsGoneWidget;
    private int mBarrierType;
    private int mMargin;
    boolean resolved;
    
    public Barrier() {
        this.mBarrierType = 0;
        this.mAllowsGoneWidget = true;
        this.mMargin = 0;
        this.resolved = false;
    }
    
    public Barrier(final String debugName) {
        this.mBarrierType = 0;
        this.mAllowsGoneWidget = true;
        this.mMargin = 0;
        this.resolved = false;
        this.setDebugName(debugName);
    }
    
    @Override
    public void addToSolver(final LinearSystem linearSystem, final boolean b) {
        this.mListAnchors[0] = this.mLeft;
        this.mListAnchors[2] = this.mTop;
        this.mListAnchors[1] = this.mRight;
        this.mListAnchors[3] = this.mBottom;
        for (int i = 0; i < this.mListAnchors.length; ++i) {
            this.mListAnchors[i].mSolverVariable = linearSystem.createObjectVariable(this.mListAnchors[i]);
        }
        final int mBarrierType = this.mBarrierType;
        if (mBarrierType >= 0 && mBarrierType < 4) {
            final ConstraintAnchor constraintAnchor = this.mListAnchors[this.mBarrierType];
            if (!this.resolved) {
                this.allSolved();
            }
            if (this.resolved) {
                this.resolved = false;
                final int mBarrierType2 = this.mBarrierType;
                if (mBarrierType2 != 0 && mBarrierType2 != 1) {
                    if (mBarrierType2 == 2 || mBarrierType2 == 3) {
                        linearSystem.addEquality(this.mTop.mSolverVariable, this.mY);
                        linearSystem.addEquality(this.mBottom.mSolverVariable, this.mY);
                    }
                }
                else {
                    linearSystem.addEquality(this.mLeft.mSolverVariable, this.mX);
                    linearSystem.addEquality(this.mRight.mSolverVariable, this.mX);
                }
                return;
            }
            while (true) {
                for (int j = 0; j < this.mWidgetsCount; ++j) {
                    final ConstraintWidget constraintWidget = this.mWidgets[j];
                    if (this.mAllowsGoneWidget || constraintWidget.allowedInBarrier()) {
                        final int mBarrierType3 = this.mBarrierType;
                        if ((mBarrierType3 != 0 && mBarrierType3 != 1) || constraintWidget.getHorizontalDimensionBehaviour() != DimensionBehaviour.MATCH_CONSTRAINT || constraintWidget.mLeft.mTarget == null || constraintWidget.mRight.mTarget == null) {
                            final int mBarrierType4 = this.mBarrierType;
                            if ((mBarrierType4 != 2 && mBarrierType4 != 3) || constraintWidget.getVerticalDimensionBehaviour() != DimensionBehaviour.MATCH_CONSTRAINT || constraintWidget.mTop.mTarget == null || constraintWidget.mBottom.mTarget == null) {
                                continue;
                            }
                        }
                        final boolean b2 = true;
                        final boolean b3 = this.mLeft.hasCenteredDependents() || this.mRight.hasCenteredDependents();
                        final boolean b4 = this.mTop.hasCenteredDependents() || this.mBottom.hasCenteredDependents();
                        boolean b5 = false;
                        Label_0491: {
                            if (!b2) {
                                final int mBarrierType5 = this.mBarrierType;
                                if ((mBarrierType5 == 0 && b3) || (mBarrierType5 == 2 && b4) || (mBarrierType5 == 1 && b3) || (mBarrierType5 == 3 && b4)) {
                                    b5 = true;
                                    break Label_0491;
                                }
                            }
                            b5 = false;
                        }
                        int n = 5;
                        if (!b5) {
                            n = 4;
                        }
                        for (int k = 0; k < this.mWidgetsCount; ++k) {
                            final ConstraintWidget constraintWidget2 = this.mWidgets[k];
                            if (this.mAllowsGoneWidget || constraintWidget2.allowedInBarrier()) {
                                final SolverVariable objectVariable = linearSystem.createObjectVariable(constraintWidget2.mListAnchors[this.mBarrierType]);
                                constraintWidget2.mListAnchors[this.mBarrierType].mSolverVariable = objectVariable;
                                int n2;
                                if (constraintWidget2.mListAnchors[this.mBarrierType].mTarget != null && constraintWidget2.mListAnchors[this.mBarrierType].mTarget.mOwner == this) {
                                    n2 = constraintWidget2.mListAnchors[this.mBarrierType].mMargin + 0;
                                }
                                else {
                                    n2 = 0;
                                }
                                final int mBarrierType6 = this.mBarrierType;
                                if (mBarrierType6 != 0 && mBarrierType6 != 2) {
                                    linearSystem.addGreaterBarrier(constraintAnchor.mSolverVariable, objectVariable, this.mMargin + n2, b2);
                                }
                                else {
                                    linearSystem.addLowerBarrier(constraintAnchor.mSolverVariable, objectVariable, this.mMargin - n2, b2);
                                }
                                linearSystem.addEquality(constraintAnchor.mSolverVariable, objectVariable, this.mMargin + n2, n);
                            }
                        }
                        final int mBarrierType7 = this.mBarrierType;
                        if (mBarrierType7 == 0) {
                            linearSystem.addEquality(this.mRight.mSolverVariable, this.mLeft.mSolverVariable, 0, 8);
                            linearSystem.addEquality(this.mLeft.mSolverVariable, this.mParent.mRight.mSolverVariable, 0, 4);
                            linearSystem.addEquality(this.mLeft.mSolverVariable, this.mParent.mLeft.mSolverVariable, 0, 0);
                            return;
                        }
                        if (mBarrierType7 == 1) {
                            linearSystem.addEquality(this.mLeft.mSolverVariable, this.mRight.mSolverVariable, 0, 8);
                            linearSystem.addEquality(this.mLeft.mSolverVariable, this.mParent.mLeft.mSolverVariable, 0, 4);
                            linearSystem.addEquality(this.mLeft.mSolverVariable, this.mParent.mRight.mSolverVariable, 0, 0);
                            return;
                        }
                        if (mBarrierType7 == 2) {
                            linearSystem.addEquality(this.mBottom.mSolverVariable, this.mTop.mSolverVariable, 0, 8);
                            linearSystem.addEquality(this.mTop.mSolverVariable, this.mParent.mBottom.mSolverVariable, 0, 4);
                            linearSystem.addEquality(this.mTop.mSolverVariable, this.mParent.mTop.mSolverVariable, 0, 0);
                            return;
                        }
                        if (mBarrierType7 == 3) {
                            linearSystem.addEquality(this.mTop.mSolverVariable, this.mBottom.mSolverVariable, 0, 8);
                            linearSystem.addEquality(this.mTop.mSolverVariable, this.mParent.mTop.mSolverVariable, 0, 4);
                            linearSystem.addEquality(this.mTop.mSolverVariable, this.mParent.mBottom.mSolverVariable, 0, 0);
                        }
                        return;
                    }
                }
                final boolean b2 = false;
                continue;
            }
        }
    }
    
    public boolean allSolved() {
        int i = 0;
        int j = 0;
        int n = 1;
        while (j < this.mWidgetsCount) {
            final ConstraintWidget constraintWidget = this.mWidgets[j];
            int n2 = 0;
            Label_0103: {
                if (!this.mAllowsGoneWidget && !constraintWidget.allowedInBarrier()) {
                    n2 = n;
                }
                else {
                    final int mBarrierType = this.mBarrierType;
                    if ((mBarrierType != 0 && mBarrierType != 1) || constraintWidget.isResolvedHorizontally()) {
                        final int mBarrierType2 = this.mBarrierType;
                        if (mBarrierType2 != 2) {
                            n2 = n;
                            if (mBarrierType2 != 3) {
                                break Label_0103;
                            }
                        }
                        n2 = n;
                        if (constraintWidget.isResolvedVertically()) {
                            break Label_0103;
                        }
                    }
                    n2 = 0;
                }
            }
            ++j;
            n = n2;
        }
        if (n != 0 && this.mWidgetsCount > 0) {
            int n3 = 0;
            int n4 = 0;
            while (i < this.mWidgetsCount) {
                final ConstraintWidget constraintWidget2 = this.mWidgets[i];
                if (this.mAllowsGoneWidget || constraintWidget2.allowedInBarrier()) {
                    int n5 = n3;
                    int n6;
                    if ((n6 = n4) == 0) {
                        final int mBarrierType3 = this.mBarrierType;
                        if (mBarrierType3 == 0) {
                            n3 = constraintWidget2.getAnchor(ConstraintAnchor.Type.LEFT).getFinalValue();
                        }
                        else if (mBarrierType3 == 1) {
                            n3 = constraintWidget2.getAnchor(ConstraintAnchor.Type.RIGHT).getFinalValue();
                        }
                        else if (mBarrierType3 == 2) {
                            n3 = constraintWidget2.getAnchor(ConstraintAnchor.Type.TOP).getFinalValue();
                        }
                        else if (mBarrierType3 == 3) {
                            n3 = constraintWidget2.getAnchor(ConstraintAnchor.Type.BOTTOM).getFinalValue();
                        }
                        n6 = 1;
                        n5 = n3;
                    }
                    final int mBarrierType4 = this.mBarrierType;
                    if (mBarrierType4 == 0) {
                        n3 = Math.min(n5, constraintWidget2.getAnchor(ConstraintAnchor.Type.LEFT).getFinalValue());
                        n4 = n6;
                    }
                    else if (mBarrierType4 == 1) {
                        n3 = Math.max(n5, constraintWidget2.getAnchor(ConstraintAnchor.Type.RIGHT).getFinalValue());
                        n4 = n6;
                    }
                    else if (mBarrierType4 == 2) {
                        n3 = Math.min(n5, constraintWidget2.getAnchor(ConstraintAnchor.Type.TOP).getFinalValue());
                        n4 = n6;
                    }
                    else {
                        n3 = n5;
                        n4 = n6;
                        if (mBarrierType4 == 3) {
                            n3 = Math.max(n5, constraintWidget2.getAnchor(ConstraintAnchor.Type.BOTTOM).getFinalValue());
                            n4 = n6;
                        }
                    }
                }
                ++i;
            }
            final int n7 = n3 + this.mMargin;
            final int mBarrierType5 = this.mBarrierType;
            if (mBarrierType5 != 0 && mBarrierType5 != 1) {
                this.setFinalVertical(n7, n7);
            }
            else {
                this.setFinalHorizontal(n7, n7);
            }
            return this.resolved = true;
        }
        return false;
    }
    
    @Override
    public boolean allowedInBarrier() {
        return true;
    }
    
    @Deprecated
    public boolean allowsGoneWidget() {
        return this.mAllowsGoneWidget;
    }
    
    @Override
    public void copy(final ConstraintWidget constraintWidget, final HashMap<ConstraintWidget, ConstraintWidget> hashMap) {
        super.copy(constraintWidget, hashMap);
        final Barrier barrier = (Barrier)constraintWidget;
        this.mBarrierType = barrier.mBarrierType;
        this.mAllowsGoneWidget = barrier.mAllowsGoneWidget;
        this.mMargin = barrier.mMargin;
    }
    
    public boolean getAllowsGoneWidget() {
        return this.mAllowsGoneWidget;
    }
    
    public int getBarrierType() {
        return this.mBarrierType;
    }
    
    public int getMargin() {
        return this.mMargin;
    }
    
    public int getOrientation() {
        final int mBarrierType = this.mBarrierType;
        if (mBarrierType == 0 || mBarrierType == 1) {
            return 0;
        }
        if (mBarrierType != 2 && mBarrierType != 3) {
            return -1;
        }
        return 1;
    }
    
    @Override
    public boolean isResolvedHorizontally() {
        return this.resolved;
    }
    
    @Override
    public boolean isResolvedVertically() {
        return this.resolved;
    }
    
    protected void markWidgets() {
        for (int i = 0; i < this.mWidgetsCount; ++i) {
            final ConstraintWidget constraintWidget = this.mWidgets[i];
            if (this.mAllowsGoneWidget || constraintWidget.allowedInBarrier()) {
                final int mBarrierType = this.mBarrierType;
                if (mBarrierType != 0 && mBarrierType != 1) {
                    if (mBarrierType == 2 || mBarrierType == 3) {
                        constraintWidget.setInBarrier(1, true);
                    }
                }
                else {
                    constraintWidget.setInBarrier(0, true);
                }
            }
        }
    }
    
    public void setAllowsGoneWidget(final boolean mAllowsGoneWidget) {
        this.mAllowsGoneWidget = mAllowsGoneWidget;
    }
    
    public void setBarrierType(final int mBarrierType) {
        this.mBarrierType = mBarrierType;
    }
    
    public void setMargin(final int mMargin) {
        this.mMargin = mMargin;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("[Barrier] ");
        sb.append(this.getDebugName());
        sb.append(" {");
        String s = sb.toString();
        for (int i = 0; i < this.mWidgetsCount; ++i) {
            final ConstraintWidget constraintWidget = this.mWidgets[i];
            String string = s;
            if (i > 0) {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append(s);
                sb2.append(", ");
                string = sb2.toString();
            }
            final StringBuilder sb3 = new StringBuilder();
            sb3.append(string);
            sb3.append(constraintWidget.getDebugName());
            s = sb3.toString();
        }
        final StringBuilder sb4 = new StringBuilder();
        sb4.append(s);
        sb4.append("}");
        return sb4.toString();
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.core.widgets;

import java.util.HashSet;
import androidx.constraintlayout.core.widgets.analyzer.BasicMeasure;

public class VirtualLayout extends HelperWidget
{
    protected BasicMeasure.Measure mMeasure;
    private int mMeasuredHeight;
    private int mMeasuredWidth;
    BasicMeasure.Measurer mMeasurer;
    private boolean mNeedsCallFromSolver;
    private int mPaddingBottom;
    private int mPaddingEnd;
    private int mPaddingLeft;
    private int mPaddingRight;
    private int mPaddingStart;
    private int mPaddingTop;
    private int mResolvedPaddingLeft;
    private int mResolvedPaddingRight;
    
    public VirtualLayout() {
        this.mPaddingTop = 0;
        this.mPaddingBottom = 0;
        this.mPaddingLeft = 0;
        this.mPaddingRight = 0;
        this.mPaddingStart = 0;
        this.mPaddingEnd = 0;
        this.mResolvedPaddingLeft = 0;
        this.mResolvedPaddingRight = 0;
        this.mNeedsCallFromSolver = false;
        this.mMeasuredWidth = 0;
        this.mMeasuredHeight = 0;
        this.mMeasure = new BasicMeasure.Measure();
        this.mMeasurer = null;
    }
    
    public void applyRtl(final boolean b) {
        final int mPaddingStart = this.mPaddingStart;
        if (mPaddingStart > 0 || this.mPaddingEnd > 0) {
            if (b) {
                this.mResolvedPaddingLeft = this.mPaddingEnd;
                this.mResolvedPaddingRight = mPaddingStart;
            }
            else {
                this.mResolvedPaddingLeft = mPaddingStart;
                this.mResolvedPaddingRight = this.mPaddingEnd;
            }
        }
    }
    
    public void captureWidgets() {
        for (int i = 0; i < this.mWidgetsCount; ++i) {
            final ConstraintWidget constraintWidget = this.mWidgets[i];
            if (constraintWidget != null) {
                constraintWidget.setInVirtualLayout(true);
            }
        }
    }
    
    public boolean contains(final HashSet<ConstraintWidget> set) {
        for (int i = 0; i < this.mWidgetsCount; ++i) {
            if (set.contains(this.mWidgets[i])) {
                return true;
            }
        }
        return false;
    }
    
    public int getMeasuredHeight() {
        return this.mMeasuredHeight;
    }
    
    public int getMeasuredWidth() {
        return this.mMeasuredWidth;
    }
    
    public int getPaddingBottom() {
        return this.mPaddingBottom;
    }
    
    public int getPaddingLeft() {
        return this.mResolvedPaddingLeft;
    }
    
    public int getPaddingRight() {
        return this.mResolvedPaddingRight;
    }
    
    public int getPaddingTop() {
        return this.mPaddingTop;
    }
    
    public void measure(final int n, final int n2, final int n3, final int n4) {
    }
    
    protected void measure(final ConstraintWidget constraintWidget, final DimensionBehaviour horizontalBehavior, final int horizontalDimension, final DimensionBehaviour verticalBehavior, final int verticalDimension) {
        while (this.mMeasurer == null && this.getParent() != null) {
            this.mMeasurer = ((ConstraintWidgetContainer)this.getParent()).getMeasurer();
        }
        this.mMeasure.horizontalBehavior = horizontalBehavior;
        this.mMeasure.verticalBehavior = verticalBehavior;
        this.mMeasure.horizontalDimension = horizontalDimension;
        this.mMeasure.verticalDimension = verticalDimension;
        this.mMeasurer.measure(constraintWidget, this.mMeasure);
        constraintWidget.setWidth(this.mMeasure.measuredWidth);
        constraintWidget.setHeight(this.mMeasure.measuredHeight);
        constraintWidget.setHasBaseline(this.mMeasure.measuredHasBaseline);
        constraintWidget.setBaselineDistance(this.mMeasure.measuredBaseline);
    }
    
    protected boolean measureChildren() {
        BasicMeasure.Measurer measurer;
        if (this.mParent != null) {
            measurer = ((ConstraintWidgetContainer)this.mParent).getMeasurer();
        }
        else {
            measurer = null;
        }
        if (measurer == null) {
            return false;
        }
        int n = 0;
        while (true) {
            final int mWidgetsCount = this.mWidgetsCount;
            boolean b = true;
            if (n >= mWidgetsCount) {
                break;
            }
            final ConstraintWidget constraintWidget = this.mWidgets[n];
            if (constraintWidget != null) {
                if (!(constraintWidget instanceof Guideline)) {
                    final DimensionBehaviour dimensionBehaviour = constraintWidget.getDimensionBehaviour(0);
                    final DimensionBehaviour dimensionBehaviour2 = constraintWidget.getDimensionBehaviour(1);
                    if (dimensionBehaviour != DimensionBehaviour.MATCH_CONSTRAINT || constraintWidget.mMatchConstraintDefaultWidth == 1 || dimensionBehaviour2 != DimensionBehaviour.MATCH_CONSTRAINT || constraintWidget.mMatchConstraintDefaultHeight == 1) {
                        b = false;
                    }
                    if (!b) {
                        Enum<DimensionBehaviour> wrap_CONTENT;
                        if ((wrap_CONTENT = dimensionBehaviour) == DimensionBehaviour.MATCH_CONSTRAINT) {
                            wrap_CONTENT = DimensionBehaviour.WRAP_CONTENT;
                        }
                        Enum<DimensionBehaviour> wrap_CONTENT2;
                        if ((wrap_CONTENT2 = dimensionBehaviour2) == DimensionBehaviour.MATCH_CONSTRAINT) {
                            wrap_CONTENT2 = DimensionBehaviour.WRAP_CONTENT;
                        }
                        this.mMeasure.horizontalBehavior = (DimensionBehaviour)wrap_CONTENT;
                        this.mMeasure.verticalBehavior = (DimensionBehaviour)wrap_CONTENT2;
                        this.mMeasure.horizontalDimension = constraintWidget.getWidth();
                        this.mMeasure.verticalDimension = constraintWidget.getHeight();
                        measurer.measure(constraintWidget, this.mMeasure);
                        constraintWidget.setWidth(this.mMeasure.measuredWidth);
                        constraintWidget.setHeight(this.mMeasure.measuredHeight);
                        constraintWidget.setBaselineDistance(this.mMeasure.measuredBaseline);
                    }
                }
            }
            ++n;
        }
        return true;
    }
    
    public boolean needSolverPass() {
        return this.mNeedsCallFromSolver;
    }
    
    protected void needsCallbackFromSolver(final boolean mNeedsCallFromSolver) {
        this.mNeedsCallFromSolver = mNeedsCallFromSolver;
    }
    
    public void setMeasure(final int mMeasuredWidth, final int mMeasuredHeight) {
        this.mMeasuredWidth = mMeasuredWidth;
        this.mMeasuredHeight = mMeasuredHeight;
    }
    
    public void setPadding(final int n) {
        this.mPaddingLeft = n;
        this.mPaddingTop = n;
        this.mPaddingRight = n;
        this.mPaddingBottom = n;
        this.mPaddingStart = n;
        this.mPaddingEnd = n;
    }
    
    public void setPaddingBottom(final int mPaddingBottom) {
        this.mPaddingBottom = mPaddingBottom;
    }
    
    public void setPaddingEnd(final int mPaddingEnd) {
        this.mPaddingEnd = mPaddingEnd;
    }
    
    public void setPaddingLeft(final int n) {
        this.mPaddingLeft = n;
        this.mResolvedPaddingLeft = n;
    }
    
    public void setPaddingRight(final int n) {
        this.mPaddingRight = n;
        this.mResolvedPaddingRight = n;
    }
    
    public void setPaddingStart(final int mResolvedPaddingRight) {
        this.mPaddingStart = mResolvedPaddingRight;
        this.mResolvedPaddingLeft = mResolvedPaddingRight;
        this.mResolvedPaddingRight = mResolvedPaddingRight;
    }
    
    public void setPaddingTop(final int mPaddingTop) {
        this.mPaddingTop = mPaddingTop;
    }
    
    @Override
    public void updateConstraints(final ConstraintWidgetContainer constraintWidgetContainer) {
        this.captureWidgets();
    }
}

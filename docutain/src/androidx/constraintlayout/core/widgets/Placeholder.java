// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.core.widgets;

import androidx.constraintlayout.core.LinearSystem;

public class Placeholder extends VirtualLayout
{
    @Override
    public void addToSolver(final LinearSystem linearSystem, final boolean b) {
        super.addToSolver(linearSystem, b);
        if (this.mWidgetsCount > 0) {
            final ConstraintWidget constraintWidget = this.mWidgets[0];
            constraintWidget.resetAllConstraints();
            constraintWidget.connect(ConstraintAnchor.Type.LEFT, this, ConstraintAnchor.Type.LEFT);
            constraintWidget.connect(ConstraintAnchor.Type.RIGHT, this, ConstraintAnchor.Type.RIGHT);
            constraintWidget.connect(ConstraintAnchor.Type.TOP, this, ConstraintAnchor.Type.TOP);
            constraintWidget.connect(ConstraintAnchor.Type.BOTTOM, this, ConstraintAnchor.Type.BOTTOM);
        }
    }
    
    @Override
    public void measure(final int n, int min, final int n2, int min2) {
        final int paddingLeft = this.getPaddingLeft();
        final int paddingRight = this.getPaddingRight();
        final int paddingTop = this.getPaddingTop();
        final int paddingBottom = this.getPaddingBottom();
        boolean b = false;
        final int n3 = paddingLeft + paddingRight + 0;
        final int n4 = paddingTop + paddingBottom + 0;
        int b2 = n3;
        int b3 = n4;
        if (this.mWidgetsCount > 0) {
            b2 = n3 + this.mWidgets[0].getWidth();
            b3 = n4 + this.mWidgets[0].getHeight();
        }
        final int max = Math.max(this.getMinWidth(), b2);
        final int max2 = Math.max(this.getMinHeight(), b3);
        if (n != 1073741824) {
            if (n == Integer.MIN_VALUE) {
                min = Math.min(max, min);
            }
            else if (n == 0) {
                min = max;
            }
            else {
                min = 0;
            }
        }
        if (n2 != 1073741824) {
            if (n2 == Integer.MIN_VALUE) {
                min2 = Math.min(max2, min2);
            }
            else if (n2 == 0) {
                min2 = max2;
            }
            else {
                min2 = 0;
            }
        }
        this.setMeasure(min, min2);
        this.setWidth(min);
        this.setHeight(min2);
        if (this.mWidgetsCount > 0) {
            b = true;
        }
        this.needsCallbackFromSolver(b);
    }
}

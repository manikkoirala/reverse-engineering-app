// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.core.widgets;

import java.io.PrintStream;
import androidx.constraintlayout.core.widgets.analyzer.Grouping;
import androidx.constraintlayout.core.widgets.analyzer.Direct;
import java.util.Iterator;
import java.util.ArrayList;
import androidx.constraintlayout.core.SolverVariable;
import java.util.Arrays;
import java.util.HashSet;
import androidx.constraintlayout.core.LinearSystem;
import androidx.constraintlayout.core.Metrics;
import androidx.constraintlayout.core.widgets.analyzer.DependencyGraph;
import androidx.constraintlayout.core.widgets.analyzer.BasicMeasure;
import java.lang.ref.WeakReference;

public class ConstraintWidgetContainer extends WidgetContainer
{
    private static final boolean DEBUG = false;
    static final boolean DEBUG_GRAPH = false;
    private static final boolean DEBUG_LAYOUT = false;
    private static final int MAX_ITERATIONS = 8;
    static int myCounter;
    private WeakReference<ConstraintAnchor> horizontalWrapMax;
    private WeakReference<ConstraintAnchor> horizontalWrapMin;
    BasicMeasure mBasicMeasureSolver;
    int mDebugSolverPassCount;
    public DependencyGraph mDependencyGraph;
    public boolean mGroupsWrapOptimized;
    private boolean mHeightMeasuredTooSmall;
    ChainHead[] mHorizontalChainsArray;
    public int mHorizontalChainsSize;
    public boolean mHorizontalWrapOptimized;
    private boolean mIsRtl;
    public BasicMeasure.Measure mMeasure;
    protected BasicMeasure.Measurer mMeasurer;
    public Metrics mMetrics;
    private int mOptimizationLevel;
    int mPaddingBottom;
    int mPaddingLeft;
    int mPaddingRight;
    int mPaddingTop;
    public boolean mSkipSolver;
    protected LinearSystem mSystem;
    ChainHead[] mVerticalChainsArray;
    public int mVerticalChainsSize;
    public boolean mVerticalWrapOptimized;
    private boolean mWidthMeasuredTooSmall;
    public int mWrapFixedHeight;
    public int mWrapFixedWidth;
    private int pass;
    private WeakReference<ConstraintAnchor> verticalWrapMax;
    private WeakReference<ConstraintAnchor> verticalWrapMin;
    HashSet<ConstraintWidget> widgetsToAdd;
    
    public ConstraintWidgetContainer() {
        this.mBasicMeasureSolver = new BasicMeasure(this);
        this.mDependencyGraph = new DependencyGraph(this);
        this.mMeasurer = null;
        this.mIsRtl = false;
        this.mSystem = new LinearSystem();
        this.mHorizontalChainsSize = 0;
        this.mVerticalChainsSize = 0;
        this.mVerticalChainsArray = new ChainHead[4];
        this.mHorizontalChainsArray = new ChainHead[4];
        this.mGroupsWrapOptimized = false;
        this.mHorizontalWrapOptimized = false;
        this.mVerticalWrapOptimized = false;
        this.mWrapFixedWidth = 0;
        this.mWrapFixedHeight = 0;
        this.mOptimizationLevel = 257;
        this.mSkipSolver = false;
        this.mWidthMeasuredTooSmall = false;
        this.mHeightMeasuredTooSmall = false;
        this.mDebugSolverPassCount = 0;
        this.verticalWrapMin = null;
        this.horizontalWrapMin = null;
        this.verticalWrapMax = null;
        this.horizontalWrapMax = null;
        this.widgetsToAdd = new HashSet<ConstraintWidget>();
        this.mMeasure = new BasicMeasure.Measure();
    }
    
    public ConstraintWidgetContainer(final int n, final int n2) {
        super(n, n2);
        this.mBasicMeasureSolver = new BasicMeasure(this);
        this.mDependencyGraph = new DependencyGraph(this);
        this.mMeasurer = null;
        this.mIsRtl = false;
        this.mSystem = new LinearSystem();
        this.mHorizontalChainsSize = 0;
        this.mVerticalChainsSize = 0;
        this.mVerticalChainsArray = new ChainHead[4];
        this.mHorizontalChainsArray = new ChainHead[4];
        this.mGroupsWrapOptimized = false;
        this.mHorizontalWrapOptimized = false;
        this.mVerticalWrapOptimized = false;
        this.mWrapFixedWidth = 0;
        this.mWrapFixedHeight = 0;
        this.mOptimizationLevel = 257;
        this.mSkipSolver = false;
        this.mWidthMeasuredTooSmall = false;
        this.mHeightMeasuredTooSmall = false;
        this.mDebugSolverPassCount = 0;
        this.verticalWrapMin = null;
        this.horizontalWrapMin = null;
        this.verticalWrapMax = null;
        this.horizontalWrapMax = null;
        this.widgetsToAdd = new HashSet<ConstraintWidget>();
        this.mMeasure = new BasicMeasure.Measure();
    }
    
    public ConstraintWidgetContainer(final int n, final int n2, final int n3, final int n4) {
        super(n, n2, n3, n4);
        this.mBasicMeasureSolver = new BasicMeasure(this);
        this.mDependencyGraph = new DependencyGraph(this);
        this.mMeasurer = null;
        this.mIsRtl = false;
        this.mSystem = new LinearSystem();
        this.mHorizontalChainsSize = 0;
        this.mVerticalChainsSize = 0;
        this.mVerticalChainsArray = new ChainHead[4];
        this.mHorizontalChainsArray = new ChainHead[4];
        this.mGroupsWrapOptimized = false;
        this.mHorizontalWrapOptimized = false;
        this.mVerticalWrapOptimized = false;
        this.mWrapFixedWidth = 0;
        this.mWrapFixedHeight = 0;
        this.mOptimizationLevel = 257;
        this.mSkipSolver = false;
        this.mWidthMeasuredTooSmall = false;
        this.mHeightMeasuredTooSmall = false;
        this.mDebugSolverPassCount = 0;
        this.verticalWrapMin = null;
        this.horizontalWrapMin = null;
        this.verticalWrapMax = null;
        this.horizontalWrapMax = null;
        this.widgetsToAdd = new HashSet<ConstraintWidget>();
        this.mMeasure = new BasicMeasure.Measure();
    }
    
    public ConstraintWidgetContainer(final String debugName, final int n, final int n2) {
        super(n, n2);
        this.mBasicMeasureSolver = new BasicMeasure(this);
        this.mDependencyGraph = new DependencyGraph(this);
        this.mMeasurer = null;
        this.mIsRtl = false;
        this.mSystem = new LinearSystem();
        this.mHorizontalChainsSize = 0;
        this.mVerticalChainsSize = 0;
        this.mVerticalChainsArray = new ChainHead[4];
        this.mHorizontalChainsArray = new ChainHead[4];
        this.mGroupsWrapOptimized = false;
        this.mHorizontalWrapOptimized = false;
        this.mVerticalWrapOptimized = false;
        this.mWrapFixedWidth = 0;
        this.mWrapFixedHeight = 0;
        this.mOptimizationLevel = 257;
        this.mSkipSolver = false;
        this.mWidthMeasuredTooSmall = false;
        this.mHeightMeasuredTooSmall = false;
        this.mDebugSolverPassCount = 0;
        this.verticalWrapMin = null;
        this.horizontalWrapMin = null;
        this.verticalWrapMax = null;
        this.horizontalWrapMax = null;
        this.widgetsToAdd = new HashSet<ConstraintWidget>();
        this.mMeasure = new BasicMeasure.Measure();
        this.setDebugName(debugName);
    }
    
    private void addHorizontalChain(final ConstraintWidget constraintWidget) {
        final int mHorizontalChainsSize = this.mHorizontalChainsSize;
        final ChainHead[] mHorizontalChainsArray = this.mHorizontalChainsArray;
        if (mHorizontalChainsSize + 1 >= mHorizontalChainsArray.length) {
            this.mHorizontalChainsArray = Arrays.copyOf(mHorizontalChainsArray, mHorizontalChainsArray.length * 2);
        }
        this.mHorizontalChainsArray[this.mHorizontalChainsSize] = new ChainHead(constraintWidget, 0, this.isRtl());
        ++this.mHorizontalChainsSize;
    }
    
    private void addMaxWrap(final ConstraintAnchor constraintAnchor, final SolverVariable solverVariable) {
        this.mSystem.addGreaterThan(solverVariable, this.mSystem.createObjectVariable(constraintAnchor), 0, 5);
    }
    
    private void addMinWrap(final ConstraintAnchor constraintAnchor, final SolverVariable solverVariable) {
        this.mSystem.addGreaterThan(this.mSystem.createObjectVariable(constraintAnchor), solverVariable, 0, 5);
    }
    
    private void addVerticalChain(final ConstraintWidget constraintWidget) {
        final int mVerticalChainsSize = this.mVerticalChainsSize;
        final ChainHead[] mVerticalChainsArray = this.mVerticalChainsArray;
        if (mVerticalChainsSize + 1 >= mVerticalChainsArray.length) {
            this.mVerticalChainsArray = Arrays.copyOf(mVerticalChainsArray, mVerticalChainsArray.length * 2);
        }
        this.mVerticalChainsArray[this.mVerticalChainsSize] = new ChainHead(constraintWidget, 1, this.isRtl());
        ++this.mVerticalChainsSize;
    }
    
    public static boolean measure(int n, final ConstraintWidget constraintWidget, final BasicMeasure.Measurer measurer, final BasicMeasure.Measure measure, int measureStrategy) {
        if (measurer == null) {
            return false;
        }
        if (constraintWidget.getVisibility() != 8 && !(constraintWidget instanceof Guideline) && !(constraintWidget instanceof Barrier)) {
            measure.horizontalBehavior = constraintWidget.getHorizontalDimensionBehaviour();
            measure.verticalBehavior = constraintWidget.getVerticalDimensionBehaviour();
            measure.horizontalDimension = constraintWidget.getWidth();
            measure.verticalDimension = constraintWidget.getHeight();
            measure.measuredNeedsSolverPass = false;
            measure.measureStrategy = measureStrategy;
            if (measure.horizontalBehavior == DimensionBehaviour.MATCH_CONSTRAINT) {
                n = 1;
            }
            else {
                n = 0;
            }
            if (measure.verticalBehavior == DimensionBehaviour.MATCH_CONSTRAINT) {
                measureStrategy = 1;
            }
            else {
                measureStrategy = 0;
            }
            final boolean b = n != 0 && constraintWidget.mDimensionRatio > 0.0f;
            final boolean b2 = measureStrategy != 0 && constraintWidget.mDimensionRatio > 0.0f;
            int n2 = n;
            if (n != 0) {
                n2 = n;
                if (constraintWidget.hasDanglingDimension(0)) {
                    n2 = n;
                    if (constraintWidget.mMatchConstraintDefaultWidth == 0) {
                        n2 = n;
                        if (!b) {
                            measure.horizontalBehavior = DimensionBehaviour.WRAP_CONTENT;
                            if (measureStrategy != 0 && constraintWidget.mMatchConstraintDefaultHeight == 0) {
                                measure.horizontalBehavior = DimensionBehaviour.FIXED;
                            }
                            n2 = 0;
                        }
                    }
                }
            }
            if ((n = measureStrategy) != 0) {
                n = measureStrategy;
                if (constraintWidget.hasDanglingDimension(1)) {
                    n = measureStrategy;
                    if (constraintWidget.mMatchConstraintDefaultHeight == 0) {
                        n = measureStrategy;
                        if (!b2) {
                            measure.verticalBehavior = DimensionBehaviour.WRAP_CONTENT;
                            if (n2 != 0 && constraintWidget.mMatchConstraintDefaultWidth == 0) {
                                measure.verticalBehavior = DimensionBehaviour.FIXED;
                            }
                            n = 0;
                        }
                    }
                }
            }
            if (constraintWidget.isResolvedHorizontally()) {
                measure.horizontalBehavior = DimensionBehaviour.FIXED;
                n2 = 0;
            }
            if (constraintWidget.isResolvedVertically()) {
                measure.verticalBehavior = DimensionBehaviour.FIXED;
                n = 0;
            }
            if (b) {
                if (constraintWidget.mResolvedMatchConstraintDefault[0] == 4) {
                    measure.horizontalBehavior = DimensionBehaviour.FIXED;
                }
                else if (n == 0) {
                    if (measure.verticalBehavior == DimensionBehaviour.FIXED) {
                        n = measure.verticalDimension;
                    }
                    else {
                        measure.horizontalBehavior = DimensionBehaviour.WRAP_CONTENT;
                        measurer.measure(constraintWidget, measure);
                        n = measure.measuredHeight;
                    }
                    measure.horizontalBehavior = DimensionBehaviour.FIXED;
                    measure.horizontalDimension = (int)(constraintWidget.getDimensionRatio() * n);
                }
            }
            if (b2) {
                if (constraintWidget.mResolvedMatchConstraintDefault[1] == 4) {
                    measure.verticalBehavior = DimensionBehaviour.FIXED;
                }
                else if (n2 == 0) {
                    if (measure.horizontalBehavior == DimensionBehaviour.FIXED) {
                        n = measure.horizontalDimension;
                    }
                    else {
                        measure.verticalBehavior = DimensionBehaviour.WRAP_CONTENT;
                        measurer.measure(constraintWidget, measure);
                        n = measure.measuredWidth;
                    }
                    measure.verticalBehavior = DimensionBehaviour.FIXED;
                    if (constraintWidget.getDimensionRatioSide() == -1) {
                        measure.verticalDimension = (int)(n / constraintWidget.getDimensionRatio());
                    }
                    else {
                        measure.verticalDimension = (int)(constraintWidget.getDimensionRatio() * n);
                    }
                }
            }
            measurer.measure(constraintWidget, measure);
            constraintWidget.setWidth(measure.measuredWidth);
            constraintWidget.setHeight(measure.measuredHeight);
            constraintWidget.setHasBaseline(measure.measuredHasBaseline);
            constraintWidget.setBaselineDistance(measure.measuredBaseline);
            measure.measureStrategy = BasicMeasure.Measure.SELF_DIMENSIONS;
            return measure.measuredNeedsSolverPass;
        }
        measure.measuredWidth = 0;
        measure.measuredHeight = 0;
        return false;
    }
    
    private void resetChains() {
        this.mHorizontalChainsSize = 0;
        this.mVerticalChainsSize = 0;
    }
    
    void addChain(final ConstraintWidget constraintWidget, final int n) {
        if (n == 0) {
            this.addHorizontalChain(constraintWidget);
        }
        else if (n == 1) {
            this.addVerticalChain(constraintWidget);
        }
    }
    
    public boolean addChildrenToSolver(final LinearSystem linearSystem) {
        final boolean optimize = this.optimizeFor(64);
        this.addToSolver(linearSystem, optimize);
        final int size = this.mChildren.size();
        int i = 0;
        boolean b = false;
        while (i < size) {
            final ConstraintWidget constraintWidget = this.mChildren.get(i);
            constraintWidget.setInBarrier(0, false);
            constraintWidget.setInBarrier(1, false);
            if (constraintWidget instanceof Barrier) {
                b = true;
            }
            ++i;
        }
        if (b) {
            for (int j = 0; j < size; ++j) {
                final ConstraintWidget constraintWidget2 = this.mChildren.get(j);
                if (constraintWidget2 instanceof Barrier) {
                    ((Barrier)constraintWidget2).markWidgets();
                }
            }
        }
        this.widgetsToAdd.clear();
        for (int k = 0; k < size; ++k) {
            final ConstraintWidget e = this.mChildren.get(k);
            if (e.addFirst()) {
                if (e instanceof VirtualLayout) {
                    this.widgetsToAdd.add(e);
                }
                else {
                    e.addToSolver(linearSystem, optimize);
                }
            }
        }
        while (this.widgetsToAdd.size() > 0) {
            final int size2 = this.widgetsToAdd.size();
            for (final VirtualLayout o : this.widgetsToAdd) {
                if (o.contains(this.widgetsToAdd)) {
                    o.addToSolver(linearSystem, optimize);
                    this.widgetsToAdd.remove(o);
                    break;
                }
            }
            if (size2 == this.widgetsToAdd.size()) {
                final Iterator<ConstraintWidget> iterator2 = this.widgetsToAdd.iterator();
                while (iterator2.hasNext()) {
                    iterator2.next().addToSolver(linearSystem, optimize);
                }
                this.widgetsToAdd.clear();
            }
        }
        if (LinearSystem.USE_DEPENDENCY_ORDERING) {
            final HashSet<ConstraintWidget> set = new HashSet<ConstraintWidget>();
            for (int l = 0; l < size; ++l) {
                final ConstraintWidget e2 = this.mChildren.get(l);
                if (!e2.addFirst()) {
                    set.add(e2);
                }
            }
            int n;
            if (this.getHorizontalDimensionBehaviour() == DimensionBehaviour.WRAP_CONTENT) {
                n = 0;
            }
            else {
                n = 1;
            }
            this.addChildrenToSolverByDependency(this, linearSystem, set, n, false);
            for (final ConstraintWidget constraintWidget3 : set) {
                Optimizer.checkMatchParent(this, linearSystem, constraintWidget3);
                constraintWidget3.addToSolver(linearSystem, optimize);
            }
        }
        else {
            for (int index = 0; index < size; ++index) {
                final ConstraintWidget constraintWidget4 = this.mChildren.get(index);
                if (constraintWidget4 instanceof ConstraintWidgetContainer) {
                    final DimensionBehaviour horizontalDimensionBehaviour = constraintWidget4.mListDimensionBehaviors[0];
                    final DimensionBehaviour verticalDimensionBehaviour = constraintWidget4.mListDimensionBehaviors[1];
                    if (horizontalDimensionBehaviour == DimensionBehaviour.WRAP_CONTENT) {
                        constraintWidget4.setHorizontalDimensionBehaviour(DimensionBehaviour.FIXED);
                    }
                    if (verticalDimensionBehaviour == DimensionBehaviour.WRAP_CONTENT) {
                        constraintWidget4.setVerticalDimensionBehaviour(DimensionBehaviour.FIXED);
                    }
                    constraintWidget4.addToSolver(linearSystem, optimize);
                    if (horizontalDimensionBehaviour == DimensionBehaviour.WRAP_CONTENT) {
                        constraintWidget4.setHorizontalDimensionBehaviour(horizontalDimensionBehaviour);
                    }
                    if (verticalDimensionBehaviour == DimensionBehaviour.WRAP_CONTENT) {
                        constraintWidget4.setVerticalDimensionBehaviour(verticalDimensionBehaviour);
                    }
                }
                else {
                    Optimizer.checkMatchParent(this, linearSystem, constraintWidget4);
                    if (!constraintWidget4.addFirst()) {
                        constraintWidget4.addToSolver(linearSystem, optimize);
                    }
                }
            }
        }
        if (this.mHorizontalChainsSize > 0) {
            Chain.applyChainConstraints(this, linearSystem, null, 0);
        }
        if (this.mVerticalChainsSize > 0) {
            Chain.applyChainConstraints(this, linearSystem, null, 1);
        }
        return true;
    }
    
    public void addHorizontalWrapMaxVariable(final ConstraintAnchor referent) {
        final WeakReference<ConstraintAnchor> horizontalWrapMax = this.horizontalWrapMax;
        if (horizontalWrapMax == null || horizontalWrapMax.get() == null || referent.getFinalValue() > this.horizontalWrapMax.get().getFinalValue()) {
            this.horizontalWrapMax = new WeakReference<ConstraintAnchor>(referent);
        }
    }
    
    public void addHorizontalWrapMinVariable(final ConstraintAnchor referent) {
        final WeakReference<ConstraintAnchor> horizontalWrapMin = this.horizontalWrapMin;
        if (horizontalWrapMin == null || horizontalWrapMin.get() == null || referent.getFinalValue() > this.horizontalWrapMin.get().getFinalValue()) {
            this.horizontalWrapMin = new WeakReference<ConstraintAnchor>(referent);
        }
    }
    
    void addVerticalWrapMaxVariable(final ConstraintAnchor referent) {
        final WeakReference<ConstraintAnchor> verticalWrapMax = this.verticalWrapMax;
        if (verticalWrapMax == null || verticalWrapMax.get() == null || referent.getFinalValue() > this.verticalWrapMax.get().getFinalValue()) {
            this.verticalWrapMax = new WeakReference<ConstraintAnchor>(referent);
        }
    }
    
    void addVerticalWrapMinVariable(final ConstraintAnchor referent) {
        final WeakReference<ConstraintAnchor> verticalWrapMin = this.verticalWrapMin;
        if (verticalWrapMin == null || verticalWrapMin.get() == null || referent.getFinalValue() > this.verticalWrapMin.get().getFinalValue()) {
            this.verticalWrapMin = new WeakReference<ConstraintAnchor>(referent);
        }
    }
    
    public void defineTerminalWidgets() {
        this.mDependencyGraph.defineTerminalWidgets(this.getHorizontalDimensionBehaviour(), this.getVerticalDimensionBehaviour());
    }
    
    public boolean directMeasure(final boolean b) {
        return this.mDependencyGraph.directMeasure(b);
    }
    
    public boolean directMeasureSetup(final boolean b) {
        return this.mDependencyGraph.directMeasureSetup(b);
    }
    
    public boolean directMeasureWithOrientation(final boolean b, final int n) {
        return this.mDependencyGraph.directMeasureWithOrientation(b, n);
    }
    
    public void fillMetrics(final Metrics mMetrics) {
        this.mMetrics = mMetrics;
        this.mSystem.fillMetrics(mMetrics);
    }
    
    public ArrayList<Guideline> getHorizontalGuidelines() {
        final ArrayList list = new ArrayList();
        for (int size = this.mChildren.size(), i = 0; i < size; ++i) {
            final ConstraintWidget constraintWidget = this.mChildren.get(i);
            if (constraintWidget instanceof Guideline) {
                final Guideline e = (Guideline)constraintWidget;
                if (e.getOrientation() == 0) {
                    list.add(e);
                }
            }
        }
        return list;
    }
    
    public BasicMeasure.Measurer getMeasurer() {
        return this.mMeasurer;
    }
    
    public int getOptimizationLevel() {
        return this.mOptimizationLevel;
    }
    
    @Override
    public void getSceneString(final StringBuilder sb) {
        final StringBuilder sb2 = new StringBuilder();
        sb2.append(this.stringId);
        sb2.append(":{\n");
        sb.append(sb2.toString());
        final StringBuilder sb3 = new StringBuilder();
        sb3.append("  actualWidth:");
        sb3.append(this.mWidth);
        sb.append(sb3.toString());
        sb.append("\n");
        final StringBuilder sb4 = new StringBuilder();
        sb4.append("  actualHeight:");
        sb4.append(this.mHeight);
        sb.append(sb4.toString());
        sb.append("\n");
        final Iterator<ConstraintWidget> iterator = this.getChildren().iterator();
        while (iterator.hasNext()) {
            iterator.next().getSceneString(sb);
            sb.append(",\n");
        }
        sb.append("}");
    }
    
    public LinearSystem getSystem() {
        return this.mSystem;
    }
    
    @Override
    public String getType() {
        return "ConstraintLayout";
    }
    
    public ArrayList<Guideline> getVerticalGuidelines() {
        final ArrayList list = new ArrayList();
        for (int size = this.mChildren.size(), i = 0; i < size; ++i) {
            final ConstraintWidget constraintWidget = this.mChildren.get(i);
            if (constraintWidget instanceof Guideline) {
                final Guideline e = (Guideline)constraintWidget;
                if (e.getOrientation() == 1) {
                    list.add(e);
                }
            }
        }
        return list;
    }
    
    public boolean handlesInternalConstraints() {
        return false;
    }
    
    public void invalidateGraph() {
        this.mDependencyGraph.invalidateGraph();
    }
    
    public void invalidateMeasures() {
        this.mDependencyGraph.invalidateMeasures();
    }
    
    public boolean isHeightMeasuredTooSmall() {
        return this.mHeightMeasuredTooSmall;
    }
    
    public boolean isRtl() {
        return this.mIsRtl;
    }
    
    public boolean isWidthMeasuredTooSmall() {
        return this.mWidthMeasuredTooSmall;
    }
    
    @Override
    public void layout() {
        this.mX = 0;
        this.mY = 0;
        this.mWidthMeasuredTooSmall = false;
        this.mHeightMeasuredTooSmall = false;
        final int size = this.mChildren.size();
        final int max = Math.max(0, this.getWidth());
        final int max2 = Math.max(0, this.getHeight());
        final DimensionBehaviour dimensionBehaviour = this.mListDimensionBehaviors[1];
        final DimensionBehaviour dimensionBehaviour2 = this.mListDimensionBehaviors[0];
        final Metrics mMetrics = this.mMetrics;
        if (mMetrics != null) {
            ++mMetrics.layouts;
        }
        if (this.pass == 0 && Optimizer.enabled(this.mOptimizationLevel, 1)) {
            Direct.solvingPass(this, this.getMeasurer());
            for (int i = 0; i < size; ++i) {
                final ConstraintWidget constraintWidget = this.mChildren.get(i);
                if (constraintWidget.isMeasureRequested() && !(constraintWidget instanceof Guideline) && !(constraintWidget instanceof Barrier) && !(constraintWidget instanceof VirtualLayout) && !constraintWidget.isInVirtualLayout()) {
                    final DimensionBehaviour dimensionBehaviour3 = constraintWidget.getDimensionBehaviour(0);
                    final DimensionBehaviour dimensionBehaviour4 = constraintWidget.getDimensionBehaviour(1);
                    if (dimensionBehaviour3 != DimensionBehaviour.MATCH_CONSTRAINT || constraintWidget.mMatchConstraintDefaultWidth == 1 || dimensionBehaviour4 != DimensionBehaviour.MATCH_CONSTRAINT || constraintWidget.mMatchConstraintDefaultHeight == 1) {
                        measure(0, constraintWidget, this.mMeasurer, new BasicMeasure.Measure(), BasicMeasure.Measure.SELF_DIMENSIONS);
                    }
                }
            }
        }
        int width2;
        int n;
        int height2;
        if (size > 2 && (dimensionBehaviour2 == DimensionBehaviour.WRAP_CONTENT || dimensionBehaviour == DimensionBehaviour.WRAP_CONTENT) && Optimizer.enabled(this.mOptimizationLevel, 1024) && Grouping.simpleSolvingPass(this, this.getMeasurer())) {
            int width = max;
            if (dimensionBehaviour2 == DimensionBehaviour.WRAP_CONTENT) {
                if (max < this.getWidth() && max > 0) {
                    this.setWidth(max);
                    this.mWidthMeasuredTooSmall = true;
                    width = max;
                }
                else {
                    width = this.getWidth();
                }
            }
            int height = max2;
            if (dimensionBehaviour == DimensionBehaviour.WRAP_CONTENT) {
                if (max2 < this.getHeight() && max2 > 0) {
                    this.setHeight(max2);
                    this.mHeightMeasuredTooSmall = true;
                    height = max2;
                }
                else {
                    height = this.getHeight();
                }
            }
            width2 = width;
            n = 1;
            height2 = height;
        }
        else {
            n = 0;
            height2 = max2;
            width2 = max;
        }
        final boolean b = this.optimizeFor(64) || this.optimizeFor(128);
        this.mSystem.graphOptimizer = false;
        this.mSystem.newgraphOptimizer = false;
        if (this.mOptimizationLevel != 0 && b) {
            this.mSystem.newgraphOptimizer = true;
        }
        final ArrayList<ConstraintWidget> mChildren = this.mChildren;
        final boolean b2 = this.getHorizontalDimensionBehaviour() == DimensionBehaviour.WRAP_CONTENT || this.getVerticalDimensionBehaviour() == DimensionBehaviour.WRAP_CONTENT;
        this.resetChains();
        for (int j = 0; j < size; ++j) {
            final ConstraintWidget constraintWidget2 = this.mChildren.get(j);
            if (constraintWidget2 instanceof WidgetContainer) {
                ((WidgetContainer)constraintWidget2).layout();
            }
        }
        final boolean optimize = this.optimizeFor(64);
        int n2 = 0;
        int k = 1;
        while (k != 0) {
            final int n3 = n2 + 1;
            int addChildrenToSolver = k;
            try {
                this.mSystem.reset();
                addChildrenToSolver = k;
                this.resetChains();
                addChildrenToSolver = k;
                this.createObjectVariables(this.mSystem);
                for (int l = 0; l < size; ++l) {
                    addChildrenToSolver = k;
                    this.mChildren.get(l).createObjectVariables(this.mSystem);
                }
                addChildrenToSolver = k;
                final boolean b3 = (addChildrenToSolver = (this.addChildrenToSolver(this.mSystem) ? 1 : 0)) != 0;
                final WeakReference<ConstraintAnchor> verticalWrapMin = this.verticalWrapMin;
                if (verticalWrapMin != null) {
                    addChildrenToSolver = (b3 ? 1 : 0);
                    if (verticalWrapMin.get() != null) {
                        addChildrenToSolver = (b3 ? 1 : 0);
                        this.addMinWrap(this.verticalWrapMin.get(), this.mSystem.createObjectVariable(this.mTop));
                        addChildrenToSolver = (b3 ? 1 : 0);
                        this.verticalWrapMin = null;
                    }
                }
                addChildrenToSolver = (b3 ? 1 : 0);
                final WeakReference<ConstraintAnchor> verticalWrapMax = this.verticalWrapMax;
                if (verticalWrapMax != null) {
                    addChildrenToSolver = (b3 ? 1 : 0);
                    if (verticalWrapMax.get() != null) {
                        addChildrenToSolver = (b3 ? 1 : 0);
                        this.addMaxWrap(this.verticalWrapMax.get(), this.mSystem.createObjectVariable(this.mBottom));
                        addChildrenToSolver = (b3 ? 1 : 0);
                        this.verticalWrapMax = null;
                    }
                }
                addChildrenToSolver = (b3 ? 1 : 0);
                final WeakReference<ConstraintAnchor> horizontalWrapMin = this.horizontalWrapMin;
                if (horizontalWrapMin != null) {
                    addChildrenToSolver = (b3 ? 1 : 0);
                    if (horizontalWrapMin.get() != null) {
                        addChildrenToSolver = (b3 ? 1 : 0);
                        this.addMinWrap(this.horizontalWrapMin.get(), this.mSystem.createObjectVariable(this.mLeft));
                        addChildrenToSolver = (b3 ? 1 : 0);
                        this.horizontalWrapMin = null;
                    }
                }
                addChildrenToSolver = (b3 ? 1 : 0);
                final WeakReference<ConstraintAnchor> horizontalWrapMax = this.horizontalWrapMax;
                if (horizontalWrapMax != null) {
                    addChildrenToSolver = (b3 ? 1 : 0);
                    if (horizontalWrapMax.get() != null) {
                        addChildrenToSolver = (b3 ? 1 : 0);
                        this.addMaxWrap(this.horizontalWrapMax.get(), this.mSystem.createObjectVariable(this.mRight));
                        addChildrenToSolver = (b3 ? 1 : 0);
                        this.horizontalWrapMax = null;
                    }
                }
                if ((addChildrenToSolver = (b3 ? 1 : 0)) != 0) {
                    addChildrenToSolver = (b3 ? 1 : 0);
                    this.mSystem.minimize();
                    addChildrenToSolver = (b3 ? 1 : 0);
                }
            }
            catch (final Exception obj) {
                obj.printStackTrace();
                final PrintStream out = System.out;
                final StringBuilder sb = new StringBuilder();
                sb.append("EXCEPTION : ");
                sb.append(obj);
                out.println(sb.toString());
            }
            int updateChildrenFromSolver;
            if (addChildrenToSolver != 0) {
                updateChildrenFromSolver = (this.updateChildrenFromSolver(this.mSystem, Optimizer.flags) ? 1 : 0);
            }
            else {
                this.updateFromSolver(this.mSystem, optimize);
                for (int index = 0; index < size; ++index) {
                    this.mChildren.get(index).updateFromSolver(this.mSystem, optimize);
                }
                updateChildrenFromSolver = 0;
            }
            if (b2 && n3 < 8 && Optimizer.flags[2]) {
                int index2 = 0;
                int max3 = 0;
                int max4 = 0;
                while (index2 < size) {
                    final ConstraintWidget constraintWidget3 = this.mChildren.get(index2);
                    max4 = Math.max(max4, constraintWidget3.mX + constraintWidget3.getWidth());
                    max3 = Math.max(max3, constraintWidget3.mY + constraintWidget3.getHeight());
                    ++index2;
                }
                final int max5 = Math.max(this.mMinWidth, max4);
                final int max6 = Math.max(this.mMinHeight, max3);
                int n4 = n;
                int n5 = updateChildrenFromSolver;
                if (dimensionBehaviour2 == DimensionBehaviour.WRAP_CONTENT) {
                    n4 = n;
                    n5 = updateChildrenFromSolver;
                    if (this.getWidth() < max5) {
                        this.setWidth(max5);
                        this.mListDimensionBehaviors[0] = DimensionBehaviour.WRAP_CONTENT;
                        n4 = 1;
                        n5 = 1;
                    }
                }
                n = n4;
                updateChildrenFromSolver = n5;
                if (dimensionBehaviour == DimensionBehaviour.WRAP_CONTENT) {
                    n = n4;
                    updateChildrenFromSolver = n5;
                    if (this.getHeight() < max6) {
                        this.setHeight(max6);
                        this.mListDimensionBehaviors[1] = DimensionBehaviour.WRAP_CONTENT;
                        n = 1;
                        updateChildrenFromSolver = 1;
                    }
                }
            }
            final int max7 = Math.max(this.mMinWidth, this.getWidth());
            if (max7 > this.getWidth()) {
                this.setWidth(max7);
                this.mListDimensionBehaviors[0] = DimensionBehaviour.FIXED;
                n = 1;
                updateChildrenFromSolver = 1;
            }
            final int max8 = Math.max(this.mMinHeight, this.getHeight());
            if (max8 > this.getHeight()) {
                this.setHeight(max8);
                this.mListDimensionBehaviors[1] = DimensionBehaviour.FIXED;
                n = 1;
                updateChildrenFromSolver = 1;
            }
            int n6 = n;
            int n7 = updateChildrenFromSolver;
            int n10 = 0;
            Label_1516: {
                if (n == 0) {
                    int n8 = n;
                    int n9 = updateChildrenFromSolver;
                    if (this.mListDimensionBehaviors[0] == DimensionBehaviour.WRAP_CONTENT) {
                        n8 = n;
                        n9 = updateChildrenFromSolver;
                        if (width2 > 0) {
                            n8 = n;
                            n9 = updateChildrenFromSolver;
                            if (this.getWidth() > width2) {
                                this.mWidthMeasuredTooSmall = true;
                                this.mListDimensionBehaviors[0] = DimensionBehaviour.FIXED;
                                this.setWidth(width2);
                                n8 = 1;
                                n9 = 1;
                            }
                        }
                    }
                    n6 = n8;
                    n7 = n9;
                    if (this.mListDimensionBehaviors[1] == DimensionBehaviour.WRAP_CONTENT) {
                        n6 = n8;
                        n7 = n9;
                        if (height2 > 0) {
                            n6 = n8;
                            n7 = n9;
                            if (this.getHeight() > height2) {
                                this.mHeightMeasuredTooSmall = true;
                                this.mListDimensionBehaviors[1] = DimensionBehaviour.FIXED;
                                this.setHeight(height2);
                                n10 = 1;
                                n = 1;
                                break Label_1516;
                            }
                        }
                    }
                }
                n10 = n7;
                n = n6;
            }
            if (n3 > 8) {
                k = 0;
            }
            else {
                k = n10;
            }
            n2 = n3;
        }
        final ArrayList<ConstraintWidget> list = mChildren;
        this.mChildren = mChildren;
        if (n != 0) {
            this.mListDimensionBehaviors[0] = dimensionBehaviour2;
            this.mListDimensionBehaviors[1] = dimensionBehaviour;
        }
        this.resetSolverVariables(this.mSystem.getCache());
    }
    
    public long measure(final int n, final int n2, final int n3, final int n4, final int n5, final int n6, final int n7, final int mPaddingLeft, final int mPaddingTop) {
        this.mPaddingLeft = mPaddingLeft;
        this.mPaddingTop = mPaddingTop;
        return this.mBasicMeasureSolver.solverMeasure(this, n, mPaddingLeft, mPaddingTop, n2, n3, n4, n5, n6, n7);
    }
    
    public boolean optimizeFor(final int n) {
        return (this.mOptimizationLevel & n) == n;
    }
    
    @Override
    public void reset() {
        this.mSystem.reset();
        this.mPaddingLeft = 0;
        this.mPaddingRight = 0;
        this.mPaddingTop = 0;
        this.mPaddingBottom = 0;
        this.mSkipSolver = false;
        super.reset();
    }
    
    public void setMeasurer(final BasicMeasure.Measurer measurer) {
        this.mMeasurer = measurer;
        this.mDependencyGraph.setMeasurer(measurer);
    }
    
    public void setOptimizationLevel(final int mOptimizationLevel) {
        this.mOptimizationLevel = mOptimizationLevel;
        LinearSystem.USE_DEPENDENCY_ORDERING = this.optimizeFor(512);
    }
    
    public void setPadding(final int mPaddingLeft, final int mPaddingTop, final int mPaddingRight, final int mPaddingBottom) {
        this.mPaddingLeft = mPaddingLeft;
        this.mPaddingTop = mPaddingTop;
        this.mPaddingRight = mPaddingRight;
        this.mPaddingBottom = mPaddingBottom;
    }
    
    public void setPass(final int pass) {
        this.pass = pass;
    }
    
    public void setRtl(final boolean mIsRtl) {
        this.mIsRtl = mIsRtl;
    }
    
    public boolean updateChildrenFromSolver(final LinearSystem linearSystem, final boolean[] array) {
        int i = 0;
        array[2] = false;
        final boolean optimize = this.optimizeFor(64);
        this.updateFromSolver(linearSystem, optimize);
        final int size = this.mChildren.size();
        boolean b = false;
        while (i < size) {
            final ConstraintWidget constraintWidget = this.mChildren.get(i);
            constraintWidget.updateFromSolver(linearSystem, optimize);
            if (constraintWidget.hasDimensionOverride()) {
                b = true;
            }
            ++i;
        }
        return b;
    }
    
    @Override
    public void updateFromRuns(final boolean b, final boolean b2) {
        super.updateFromRuns(b, b2);
        for (int size = this.mChildren.size(), i = 0; i < size; ++i) {
            this.mChildren.get(i).updateFromRuns(b, b2);
        }
    }
    
    public void updateHierarchy() {
        this.mBasicMeasureSolver.updateHierarchy(this);
    }
}

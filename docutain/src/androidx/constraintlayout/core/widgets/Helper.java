// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.core.widgets;

public interface Helper
{
    void add(final ConstraintWidget p0);
    
    void removeAllIds();
    
    void updateConstraints(final ConstraintWidgetContainer p0);
}

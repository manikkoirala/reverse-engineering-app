// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.core.widgets.analyzer;

import java.util.Iterator;
import androidx.constraintlayout.core.widgets.Barrier;
import androidx.constraintlayout.core.widgets.ConstraintWidget;

class HelperReferences extends WidgetRun
{
    public HelperReferences(final ConstraintWidget constraintWidget) {
        super(constraintWidget);
    }
    
    private void addDependency(final DependencyNode dependencyNode) {
        this.start.dependencies.add(dependencyNode);
        dependencyNode.targets.add(this.start);
    }
    
    @Override
    void apply() {
        if (this.widget instanceof Barrier) {
            this.start.delegateToWidgetRun = true;
            final Barrier barrier = (Barrier)this.widget;
            final int barrierType = barrier.getBarrierType();
            final boolean allowsGoneWidget = barrier.getAllowsGoneWidget();
            int i = 0;
            final int n = 0;
            final int n2 = 0;
            final int n3 = 0;
            if (barrierType != 0) {
                if (barrierType != 1) {
                    if (barrierType != 2) {
                        if (barrierType == 3) {
                            this.start.type = DependencyNode.Type.BOTTOM;
                            for (int j = n3; j < barrier.mWidgetsCount; ++j) {
                                final ConstraintWidget constraintWidget = barrier.mWidgets[j];
                                if (allowsGoneWidget || constraintWidget.getVisibility() != 8) {
                                    final DependencyNode end = constraintWidget.verticalRun.end;
                                    end.dependencies.add(this.start);
                                    this.start.targets.add(end);
                                }
                            }
                            this.addDependency(this.widget.verticalRun.start);
                            this.addDependency(this.widget.verticalRun.end);
                        }
                    }
                    else {
                        this.start.type = DependencyNode.Type.TOP;
                        while (i < barrier.mWidgetsCount) {
                            final ConstraintWidget constraintWidget2 = barrier.mWidgets[i];
                            if (allowsGoneWidget || constraintWidget2.getVisibility() != 8) {
                                final DependencyNode start = constraintWidget2.verticalRun.start;
                                start.dependencies.add(this.start);
                                this.start.targets.add(start);
                            }
                            ++i;
                        }
                        this.addDependency(this.widget.verticalRun.start);
                        this.addDependency(this.widget.verticalRun.end);
                    }
                }
                else {
                    this.start.type = DependencyNode.Type.RIGHT;
                    for (int k = n; k < barrier.mWidgetsCount; ++k) {
                        final ConstraintWidget constraintWidget3 = barrier.mWidgets[k];
                        if (allowsGoneWidget || constraintWidget3.getVisibility() != 8) {
                            final DependencyNode end2 = constraintWidget3.horizontalRun.end;
                            end2.dependencies.add(this.start);
                            this.start.targets.add(end2);
                        }
                    }
                    this.addDependency(this.widget.horizontalRun.start);
                    this.addDependency(this.widget.horizontalRun.end);
                }
            }
            else {
                this.start.type = DependencyNode.Type.LEFT;
                for (int l = n2; l < barrier.mWidgetsCount; ++l) {
                    final ConstraintWidget constraintWidget4 = barrier.mWidgets[l];
                    if (allowsGoneWidget || constraintWidget4.getVisibility() != 8) {
                        final DependencyNode start2 = constraintWidget4.horizontalRun.start;
                        start2.dependencies.add(this.start);
                        this.start.targets.add(start2);
                    }
                }
                this.addDependency(this.widget.horizontalRun.start);
                this.addDependency(this.widget.horizontalRun.end);
            }
        }
    }
    
    public void applyToWidget() {
        if (this.widget instanceof Barrier) {
            final int barrierType = ((Barrier)this.widget).getBarrierType();
            if (barrierType != 0 && barrierType != 1) {
                this.widget.setY(this.start.value);
            }
            else {
                this.widget.setX(this.start.value);
            }
        }
    }
    
    @Override
    void clear() {
        this.runGroup = null;
        this.start.clear();
    }
    
    @Override
    void reset() {
        this.start.resolved = false;
    }
    
    @Override
    boolean supportsWrapComputation() {
        return false;
    }
    
    @Override
    public void update(final Dependency dependency) {
        final Barrier barrier = (Barrier)this.widget;
        final int barrierType = barrier.getBarrierType();
        final Iterator<DependencyNode> iterator = this.start.targets.iterator();
        int n = 0;
        int n2 = -1;
        while (iterator.hasNext()) {
            final int value = iterator.next().value;
            int n3;
            if (n2 == -1 || value < (n3 = n2)) {
                n3 = value;
            }
            n2 = n3;
            if (n < value) {
                n = value;
                n2 = n3;
            }
        }
        if (barrierType != 0 && barrierType != 2) {
            this.start.resolve(n + barrier.getMargin());
        }
        else {
            this.start.resolve(n2 + barrier.getMargin());
        }
    }
}

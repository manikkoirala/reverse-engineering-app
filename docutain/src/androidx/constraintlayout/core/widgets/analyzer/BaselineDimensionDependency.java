// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.core.widgets.analyzer;

class BaselineDimensionDependency extends DimensionDependency
{
    public BaselineDimensionDependency(final WidgetRun widgetRun) {
        super(widgetRun);
    }
    
    public void update(final DependencyNode dependencyNode) {
        ((VerticalWidgetRun)this.run).baseline.margin = this.run.widget.getBaselineDistance();
        this.resolved = true;
    }
}

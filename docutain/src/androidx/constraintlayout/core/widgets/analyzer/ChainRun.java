// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.core.widgets.analyzer;

import androidx.constraintlayout.core.widgets.ConstraintAnchor;
import java.util.Iterator;
import androidx.constraintlayout.core.widgets.ConstraintWidgetContainer;
import androidx.constraintlayout.core.widgets.ConstraintWidget;
import java.util.ArrayList;

public class ChainRun extends WidgetRun
{
    private int chainStyle;
    ArrayList<WidgetRun> widgets;
    
    public ChainRun(final ConstraintWidget constraintWidget, final int orientation) {
        super(constraintWidget);
        this.widgets = new ArrayList<WidgetRun>();
        this.orientation = orientation;
        this.build();
    }
    
    private void build() {
        ConstraintWidget widget = this.widget;
        ConstraintWidget previousChainMember2;
        for (ConstraintWidget previousChainMember = widget.getPreviousChainMember(this.orientation); previousChainMember != null; previousChainMember = previousChainMember2) {
            previousChainMember2 = previousChainMember.getPreviousChainMember(this.orientation);
            widget = previousChainMember;
        }
        this.widget = widget;
        this.widgets.add(widget.getRun(this.orientation));
        for (ConstraintWidget constraintWidget = widget.getNextChainMember(this.orientation); constraintWidget != null; constraintWidget = constraintWidget.getNextChainMember(this.orientation)) {
            this.widgets.add(constraintWidget.getRun(this.orientation));
        }
        for (final WidgetRun widgetRun : this.widgets) {
            if (this.orientation == 0) {
                widgetRun.widget.horizontalChainRun = this;
            }
            else {
                if (this.orientation != 1) {
                    continue;
                }
                widgetRun.widget.verticalChainRun = this;
            }
        }
        if (this.orientation == 0 && ((ConstraintWidgetContainer)this.widget.getParent()).isRtl() && this.widgets.size() > 1) {
            final ArrayList<WidgetRun> widgets = this.widgets;
            this.widget = ((WidgetRun)widgets.get(widgets.size() - 1)).widget;
        }
        int chainStyle;
        if (this.orientation == 0) {
            chainStyle = this.widget.getHorizontalChainStyle();
        }
        else {
            chainStyle = this.widget.getVerticalChainStyle();
        }
        this.chainStyle = chainStyle;
    }
    
    private ConstraintWidget getFirstVisibleWidget() {
        for (int i = 0; i < this.widgets.size(); ++i) {
            final WidgetRun widgetRun = this.widgets.get(i);
            if (widgetRun.widget.getVisibility() != 8) {
                return widgetRun.widget;
            }
        }
        return null;
    }
    
    private ConstraintWidget getLastVisibleWidget() {
        for (int i = this.widgets.size() - 1; i >= 0; --i) {
            final WidgetRun widgetRun = this.widgets.get(i);
            if (widgetRun.widget.getVisibility() != 8) {
                return widgetRun.widget;
            }
        }
        return null;
    }
    
    @Override
    void apply() {
        final Iterator<WidgetRun> iterator = this.widgets.iterator();
        while (iterator.hasNext()) {
            iterator.next().apply();
        }
        final int size = this.widgets.size();
        if (size < 1) {
            return;
        }
        final ConstraintWidget widget = this.widgets.get(0).widget;
        final ConstraintWidget widget2 = this.widgets.get(size - 1).widget;
        if (this.orientation == 0) {
            final ConstraintAnchor mLeft = widget.mLeft;
            final ConstraintAnchor mRight = widget2.mRight;
            final DependencyNode target = this.getTarget(mLeft, 0);
            int n = mLeft.getMargin();
            final ConstraintWidget firstVisibleWidget = this.getFirstVisibleWidget();
            if (firstVisibleWidget != null) {
                n = firstVisibleWidget.mLeft.getMargin();
            }
            if (target != null) {
                this.addTarget(this.start, target, n);
            }
            final DependencyNode target2 = this.getTarget(mRight, 0);
            int n2 = mRight.getMargin();
            final ConstraintWidget lastVisibleWidget = this.getLastVisibleWidget();
            if (lastVisibleWidget != null) {
                n2 = lastVisibleWidget.mRight.getMargin();
            }
            if (target2 != null) {
                this.addTarget(this.end, target2, -n2);
            }
        }
        else {
            final ConstraintAnchor mTop = widget.mTop;
            final ConstraintAnchor mBottom = widget2.mBottom;
            final DependencyNode target3 = this.getTarget(mTop, 1);
            int n3 = mTop.getMargin();
            final ConstraintWidget firstVisibleWidget2 = this.getFirstVisibleWidget();
            if (firstVisibleWidget2 != null) {
                n3 = firstVisibleWidget2.mTop.getMargin();
            }
            if (target3 != null) {
                this.addTarget(this.start, target3, n3);
            }
            final DependencyNode target4 = this.getTarget(mBottom, 1);
            int n4 = mBottom.getMargin();
            final ConstraintWidget lastVisibleWidget2 = this.getLastVisibleWidget();
            if (lastVisibleWidget2 != null) {
                n4 = lastVisibleWidget2.mBottom.getMargin();
            }
            if (target4 != null) {
                this.addTarget(this.end, target4, -n4);
            }
        }
        this.start.updateDelegate = this;
        this.end.updateDelegate = this;
    }
    
    public void applyToWidget() {
        for (int i = 0; i < this.widgets.size(); ++i) {
            this.widgets.get(i).applyToWidget();
        }
    }
    
    @Override
    void clear() {
        this.runGroup = null;
        final Iterator<WidgetRun> iterator = this.widgets.iterator();
        while (iterator.hasNext()) {
            iterator.next().clear();
        }
    }
    
    @Override
    public long getWrapDimension() {
        final int size = this.widgets.size();
        long n = 0L;
        for (int i = 0; i < size; ++i) {
            final WidgetRun widgetRun = this.widgets.get(i);
            n = n + widgetRun.start.margin + widgetRun.getWrapDimension() + widgetRun.end.margin;
        }
        return n;
    }
    
    @Override
    void reset() {
        this.start.resolved = false;
        this.end.resolved = false;
    }
    
    @Override
    boolean supportsWrapComputation() {
        for (int size = this.widgets.size(), i = 0; i < size; ++i) {
            if (!this.widgets.get(i).supportsWrapComputation()) {
                return false;
            }
        }
        return true;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("ChainRun ");
        String str;
        if (this.orientation == 0) {
            str = "horizontal : ";
        }
        else {
            str = "vertical : ";
        }
        sb.append(str);
        for (final WidgetRun obj : this.widgets) {
            sb.append("<");
            sb.append(obj);
            sb.append("> ");
        }
        return sb.toString();
    }
    
    @Override
    public void update(final Dependency dependency) {
        if (this.start.resolved) {
            if (this.end.resolved) {
                final ConstraintWidget parent = this.widget.getParent();
                final boolean b = parent instanceof ConstraintWidgetContainer && ((ConstraintWidgetContainer)parent).isRtl();
                final int n = this.end.value - this.start.value;
                final int size = this.widgets.size();
                int index = 0;
                int n2;
                int n3;
                while (true) {
                    n2 = -1;
                    if (index >= size) {
                        n3 = -1;
                        break;
                    }
                    n3 = index;
                    if (this.widgets.get(index).widget.getVisibility() != 8) {
                        break;
                    }
                    ++index;
                }
                int index2;
                final int n4 = index2 = size - 1;
                int n5;
                while (true) {
                    n5 = n2;
                    if (index2 < 0) {
                        break;
                    }
                    if (this.widgets.get(index2).widget.getVisibility() != 8) {
                        n5 = index2;
                        break;
                    }
                    --index2;
                }
                int i = 0;
                while (true) {
                    while (i < 2) {
                        int j = 0;
                        int n6 = 0;
                        int n7 = 0;
                        int n8 = 0;
                        float n9 = 0.0f;
                        while (j < size) {
                            final WidgetRun widgetRun = this.widgets.get(j);
                            int n10;
                            int n11;
                            if (widgetRun.widget.getVisibility() == 8) {
                                n10 = n6;
                                n11 = n7;
                            }
                            else {
                                final int n12 = n8 + 1;
                                int n13 = n6;
                                if (j > 0) {
                                    n13 = n6;
                                    if (j >= n3) {
                                        n13 = n6 + widgetRun.start.margin;
                                    }
                                }
                                final int value = widgetRun.dimension.value;
                                final boolean b2 = widgetRun.dimensionBehavior != ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT;
                                boolean b3 = false;
                                int n14 = 0;
                                int n15 = 0;
                                Label_0466: {
                                    if (b2) {
                                        if (this.orientation == 0 && !widgetRun.widget.horizontalRun.dimension.resolved) {
                                            return;
                                        }
                                        b3 = b2;
                                        n14 = value;
                                        n15 = n7;
                                        if (this.orientation == 1) {
                                            b3 = b2;
                                            n14 = value;
                                            n15 = n7;
                                            if (!widgetRun.widget.verticalRun.dimension.resolved) {
                                                return;
                                            }
                                        }
                                    }
                                    else {
                                        int n17;
                                        if (widgetRun.matchConstraintsType == 1 && i == 0) {
                                            final int wrapValue = widgetRun.dimension.wrapValue;
                                            final int n16 = n7 + 1;
                                            n17 = wrapValue;
                                            n15 = n16;
                                        }
                                        else {
                                            b3 = b2;
                                            n14 = value;
                                            n15 = n7;
                                            if (!widgetRun.dimension.resolved) {
                                                break Label_0466;
                                            }
                                            n15 = n7;
                                            n17 = value;
                                        }
                                        b3 = true;
                                        n14 = n17;
                                    }
                                }
                                int n20;
                                float n21;
                                if (!b3) {
                                    final int n18 = n15 + 1;
                                    final float n19 = widgetRun.widget.mWeight[this.orientation];
                                    n20 = n13;
                                    n15 = n18;
                                    n21 = n9;
                                    if (n19 >= 0.0f) {
                                        n21 = n9 + n19;
                                        n20 = n13;
                                        n15 = n18;
                                    }
                                }
                                else {
                                    n20 = n13 + n14;
                                    n21 = n9;
                                }
                                n10 = n20;
                                n11 = n15;
                                n8 = n12;
                                n9 = n21;
                                if (j < n4) {
                                    n10 = n20;
                                    n11 = n15;
                                    n8 = n12;
                                    n9 = n21;
                                    if (j < n5) {
                                        n10 = n20 + -widgetRun.end.margin;
                                        n9 = n21;
                                        n8 = n12;
                                        n11 = n15;
                                    }
                                }
                            }
                            ++j;
                            n6 = n10;
                            n7 = n11;
                        }
                        if (n6 >= n && n7 != 0) {
                            ++i;
                        }
                        else {
                            final int n22 = n8;
                            int n23 = n7;
                            int n24 = this.start.value;
                            if (b) {
                                n24 = this.end.value;
                            }
                            int n25 = n24;
                            if (n6 > n) {
                                if (b) {
                                    n25 = n24 + (int)((n6 - n) / 2.0f + 0.5f);
                                }
                                else {
                                    n25 = n24 - (int)((n6 - n) / 2.0f + 0.5f);
                                }
                            }
                            int n37;
                            if (n23 > 0) {
                                final float n26 = (float)(n - n6);
                                final int n27 = (int)(n26 / n23 + 0.5f);
                                int k = 0;
                                int n28 = 0;
                                final int n29 = n25;
                                while (k < size) {
                                    final WidgetRun widgetRun2 = this.widgets.get(k);
                                    if (widgetRun2.widget.getVisibility() != 8) {
                                        if (widgetRun2.dimensionBehavior == ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT && !widgetRun2.dimension.resolved) {
                                            int a;
                                            if (n9 > 0.0f) {
                                                a = (int)(widgetRun2.widget.mWeight[this.orientation] * n26 / n9 + 0.5f);
                                            }
                                            else {
                                                a = n27;
                                            }
                                            int a2;
                                            int a3;
                                            if (this.orientation == 0) {
                                                a2 = widgetRun2.widget.mMatchConstraintMaxWidth;
                                                a3 = widgetRun2.widget.mMatchConstraintMinWidth;
                                            }
                                            else {
                                                a2 = widgetRun2.widget.mMatchConstraintMaxHeight;
                                                a3 = widgetRun2.widget.mMatchConstraintMinHeight;
                                            }
                                            int min;
                                            if (widgetRun2.matchConstraintsType == 1) {
                                                min = Math.min(a, widgetRun2.dimension.wrapValue);
                                            }
                                            else {
                                                min = a;
                                            }
                                            int n30;
                                            final int b4 = n30 = Math.max(a3, min);
                                            if (a2 > 0) {
                                                n30 = Math.min(a2, b4);
                                            }
                                            int n31 = a;
                                            int n32 = n28;
                                            if (n30 != a) {
                                                n32 = n28 + 1;
                                                n31 = n30;
                                            }
                                            widgetRun2.dimension.resolve(n31);
                                            n28 = n32;
                                        }
                                    }
                                    ++k;
                                }
                                int n34;
                                if (n28 > 0) {
                                    final int n33 = n23 - n28;
                                    int l = 0;
                                    n34 = 0;
                                    while (l < size) {
                                        final WidgetRun widgetRun3 = this.widgets.get(l);
                                        if (widgetRun3.widget.getVisibility() != 8) {
                                            int n35 = n34;
                                            if (l > 0) {
                                                n35 = n34;
                                                if (l >= n3) {
                                                    n35 = n34 + widgetRun3.start.margin;
                                                }
                                            }
                                            final int n36 = n34 = n35 + widgetRun3.dimension.value;
                                            if (l < n4) {
                                                n34 = n36;
                                                if (l < n5) {
                                                    n34 = n36 + -widgetRun3.end.margin;
                                                }
                                            }
                                        }
                                        ++l;
                                    }
                                    n23 = n33;
                                }
                                else {
                                    n34 = n6;
                                }
                                if (this.chainStyle == 2 && n28 == 0) {
                                    this.chainStyle = 0;
                                    n6 = n34;
                                    n37 = n23;
                                    n25 = n29;
                                }
                                else {
                                    n6 = n34;
                                    n37 = n23;
                                    n25 = n29;
                                }
                            }
                            else {
                                n37 = n23;
                            }
                            if (n6 > n) {
                                this.chainStyle = 2;
                            }
                            if (n22 > 0 && n37 == 0 && n3 == n5) {
                                this.chainStyle = 2;
                            }
                            final int chainStyle = this.chainStyle;
                            if (chainStyle == 1) {
                                int n38;
                                if (n22 > 1) {
                                    n38 = (n - n6) / (n22 - 1);
                                }
                                else if (n22 == 1) {
                                    n38 = (n - n6) / 2;
                                }
                                else {
                                    n38 = 0;
                                }
                                int n39 = n38;
                                if (n37 > 0) {
                                    n39 = 0;
                                }
                                int n40 = 0;
                                int n41 = n25;
                                while (n40 < size) {
                                    int index3;
                                    if (b) {
                                        index3 = size - (n40 + 1);
                                    }
                                    else {
                                        index3 = n40;
                                    }
                                    final WidgetRun widgetRun4 = this.widgets.get(index3);
                                    int n42;
                                    if (widgetRun4.widget.getVisibility() == 8) {
                                        widgetRun4.start.resolve(n41);
                                        widgetRun4.end.resolve(n41);
                                        n42 = n41;
                                    }
                                    else {
                                        int n43 = n41;
                                        if (n40 > 0) {
                                            if (b) {
                                                n43 = n41 - n39;
                                            }
                                            else {
                                                n43 = n41 + n39;
                                            }
                                        }
                                        int n44 = n43;
                                        if (n40 > 0) {
                                            n44 = n43;
                                            if (n40 >= n3) {
                                                if (b) {
                                                    n44 = n43 - widgetRun4.start.margin;
                                                }
                                                else {
                                                    n44 = n43 + widgetRun4.start.margin;
                                                }
                                            }
                                        }
                                        if (b) {
                                            widgetRun4.end.resolve(n44);
                                        }
                                        else {
                                            widgetRun4.start.resolve(n44);
                                        }
                                        int n46;
                                        final int n45 = n46 = widgetRun4.dimension.value;
                                        if (widgetRun4.dimensionBehavior == ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT) {
                                            n46 = n45;
                                            if (widgetRun4.matchConstraintsType == 1) {
                                                n46 = widgetRun4.dimension.wrapValue;
                                            }
                                        }
                                        int n47;
                                        if (b) {
                                            n47 = n44 - n46;
                                        }
                                        else {
                                            n47 = n44 + n46;
                                        }
                                        if (b) {
                                            widgetRun4.start.resolve(n47);
                                        }
                                        else {
                                            widgetRun4.end.resolve(n47);
                                        }
                                        widgetRun4.resolved = true;
                                        n42 = n47;
                                        if (n40 < n4) {
                                            n42 = n47;
                                            if (n40 < n5) {
                                                if (b) {
                                                    n42 = n47 - -widgetRun4.end.margin;
                                                }
                                                else {
                                                    n42 = n47 + -widgetRun4.end.margin;
                                                }
                                            }
                                        }
                                    }
                                    ++n40;
                                    n41 = n42;
                                }
                                return;
                            }
                            if (chainStyle == 0) {
                                int n48 = (n - n6) / (n22 + 1);
                                if (n37 > 0) {
                                    n48 = 0;
                                }
                                for (int n49 = 0; n49 < size; ++n49) {
                                    int index4;
                                    if (b) {
                                        index4 = size - (n49 + 1);
                                    }
                                    else {
                                        index4 = n49;
                                    }
                                    final WidgetRun widgetRun5 = this.widgets.get(index4);
                                    if (widgetRun5.widget.getVisibility() == 8) {
                                        widgetRun5.start.resolve(n25);
                                        widgetRun5.end.resolve(n25);
                                    }
                                    else {
                                        int n50;
                                        if (b) {
                                            n50 = n25 - n48;
                                        }
                                        else {
                                            n50 = n25 + n48;
                                        }
                                        int n51 = n50;
                                        if (n49 > 0) {
                                            n51 = n50;
                                            if (n49 >= n3) {
                                                if (b) {
                                                    n51 = n50 - widgetRun5.start.margin;
                                                }
                                                else {
                                                    n51 = n50 + widgetRun5.start.margin;
                                                }
                                            }
                                        }
                                        if (b) {
                                            widgetRun5.end.resolve(n51);
                                        }
                                        else {
                                            widgetRun5.start.resolve(n51);
                                        }
                                        int n52;
                                        final int a4 = n52 = widgetRun5.dimension.value;
                                        if (widgetRun5.dimensionBehavior == ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT) {
                                            n52 = a4;
                                            if (widgetRun5.matchConstraintsType == 1) {
                                                n52 = Math.min(a4, widgetRun5.dimension.wrapValue);
                                            }
                                        }
                                        int n53;
                                        if (b) {
                                            n53 = n51 - n52;
                                        }
                                        else {
                                            n53 = n51 + n52;
                                        }
                                        if (b) {
                                            widgetRun5.start.resolve(n53);
                                        }
                                        else {
                                            widgetRun5.end.resolve(n53);
                                        }
                                        n25 = n53;
                                        if (n49 < n4) {
                                            n25 = n53;
                                            if (n49 < n5) {
                                                if (b) {
                                                    n25 = n53 - -widgetRun5.end.margin;
                                                }
                                                else {
                                                    n25 = n53 + -widgetRun5.end.margin;
                                                }
                                            }
                                        }
                                    }
                                }
                                return;
                            }
                            if (chainStyle == 2) {
                                float n54;
                                if (this.orientation == 0) {
                                    n54 = this.widget.getHorizontalBiasPercent();
                                }
                                else {
                                    n54 = this.widget.getVerticalBiasPercent();
                                }
                                float n55 = n54;
                                if (b) {
                                    n55 = 1.0f - n54;
                                }
                                int n56 = (int)((n - n6) * n55 + 0.5f);
                                if (n56 < 0 || n37 > 0) {
                                    n56 = 0;
                                }
                                int n57;
                                if (b) {
                                    n57 = n25 - n56;
                                }
                                else {
                                    n57 = n25 + n56;
                                }
                                for (int n58 = 0; n58 < size; ++n58) {
                                    int index5;
                                    if (b) {
                                        index5 = size - (n58 + 1);
                                    }
                                    else {
                                        index5 = n58;
                                    }
                                    final WidgetRun widgetRun6 = this.widgets.get(index5);
                                    if (widgetRun6.widget.getVisibility() == 8) {
                                        widgetRun6.start.resolve(n57);
                                        widgetRun6.end.resolve(n57);
                                    }
                                    else {
                                        int n59 = n57;
                                        if (n58 > 0) {
                                            n59 = n57;
                                            if (n58 >= n3) {
                                                if (b) {
                                                    n59 = n57 - widgetRun6.start.margin;
                                                }
                                                else {
                                                    n59 = n57 + widgetRun6.start.margin;
                                                }
                                            }
                                        }
                                        if (b) {
                                            widgetRun6.end.resolve(n59);
                                        }
                                        else {
                                            widgetRun6.start.resolve(n59);
                                        }
                                        int n60 = widgetRun6.dimension.value;
                                        if (widgetRun6.dimensionBehavior == ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT && widgetRun6.matchConstraintsType == 1) {
                                            n60 = widgetRun6.dimension.wrapValue;
                                        }
                                        int n61;
                                        if (b) {
                                            n61 = n59 - n60;
                                        }
                                        else {
                                            n61 = n59 + n60;
                                        }
                                        if (b) {
                                            widgetRun6.start.resolve(n61);
                                        }
                                        else {
                                            widgetRun6.end.resolve(n61);
                                        }
                                        n57 = n61;
                                        if (n58 < n4) {
                                            n57 = n61;
                                            if (n58 < n5) {
                                                if (b) {
                                                    n57 = n61 - -widgetRun6.end.margin;
                                                }
                                                else {
                                                    n57 = n61 + -widgetRun6.end.margin;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            return;
                        }
                    }
                    final int n22 = 0;
                    int n6 = 0;
                    int n23 = 0;
                    float n9 = 0.0f;
                    continue;
                }
            }
        }
    }
}

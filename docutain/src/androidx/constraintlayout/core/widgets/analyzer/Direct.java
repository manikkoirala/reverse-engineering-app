// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.core.widgets.analyzer;

import java.util.ArrayList;
import androidx.constraintlayout.core.widgets.ChainHead;
import androidx.constraintlayout.core.LinearSystem;
import androidx.constraintlayout.core.widgets.Barrier;
import java.util.Iterator;
import androidx.constraintlayout.core.widgets.Guideline;
import androidx.constraintlayout.core.widgets.ConstraintAnchor;
import androidx.constraintlayout.core.widgets.ConstraintWidgetContainer;
import androidx.constraintlayout.core.widgets.ConstraintWidget;

public class Direct
{
    private static final boolean APPLY_MATCH_PARENT = false;
    private static final boolean DEBUG = false;
    private static final boolean EARLY_TERMINATION = true;
    private static int hcount;
    private static BasicMeasure.Measure measure;
    private static int vcount;
    
    static {
        Direct.measure = new BasicMeasure.Measure();
        Direct.hcount = 0;
        Direct.vcount = 0;
    }
    
    private static boolean canMeasure(int n, final ConstraintWidget constraintWidget) {
        final ConstraintWidget.DimensionBehaviour horizontalDimensionBehaviour = constraintWidget.getHorizontalDimensionBehaviour();
        final ConstraintWidget.DimensionBehaviour verticalDimensionBehaviour = constraintWidget.getVerticalDimensionBehaviour();
        ConstraintWidgetContainer constraintWidgetContainer;
        if (constraintWidget.getParent() != null) {
            constraintWidgetContainer = (ConstraintWidgetContainer)constraintWidget.getParent();
        }
        else {
            constraintWidgetContainer = null;
        }
        if (constraintWidgetContainer != null) {
            constraintWidgetContainer.getHorizontalDimensionBehaviour();
            final ConstraintWidget.DimensionBehaviour fixed = ConstraintWidget.DimensionBehaviour.FIXED;
        }
        if (constraintWidgetContainer != null) {
            constraintWidgetContainer.getVerticalDimensionBehaviour();
            final ConstraintWidget.DimensionBehaviour fixed2 = ConstraintWidget.DimensionBehaviour.FIXED;
        }
        final ConstraintWidget.DimensionBehaviour fixed3 = ConstraintWidget.DimensionBehaviour.FIXED;
        final boolean b = false;
        if (horizontalDimensionBehaviour != fixed3 && !constraintWidget.isResolvedHorizontally() && horizontalDimensionBehaviour != ConstraintWidget.DimensionBehaviour.WRAP_CONTENT && (horizontalDimensionBehaviour != ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT || constraintWidget.mMatchConstraintDefaultWidth != 0 || constraintWidget.mDimensionRatio != 0.0f || !constraintWidget.hasDanglingDimension(0)) && (horizontalDimensionBehaviour != ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT || constraintWidget.mMatchConstraintDefaultWidth != 1 || !constraintWidget.hasResolvedTargets(0, constraintWidget.getWidth()))) {
            n = 0;
        }
        else {
            n = 1;
        }
        final boolean b2 = verticalDimensionBehaviour == ConstraintWidget.DimensionBehaviour.FIXED || constraintWidget.isResolvedVertically() || verticalDimensionBehaviour == ConstraintWidget.DimensionBehaviour.WRAP_CONTENT || (verticalDimensionBehaviour == ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT && constraintWidget.mMatchConstraintDefaultHeight == 0 && constraintWidget.mDimensionRatio == 0.0f && constraintWidget.hasDanglingDimension(1)) || (verticalDimensionBehaviour == ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT && constraintWidget.mMatchConstraintDefaultHeight == 1 && constraintWidget.hasResolvedTargets(1, constraintWidget.getHeight()));
        if (constraintWidget.mDimensionRatio > 0.0f && (n != 0 || b2)) {
            return true;
        }
        boolean b3 = b;
        if (n != 0) {
            b3 = b;
            if (b2) {
                b3 = true;
            }
        }
        return b3;
    }
    
    private static void horizontalSolvingPass(final int n, final ConstraintWidget constraintWidget, final BasicMeasure.Measurer measurer, final boolean b) {
        if (constraintWidget.isHorizontalSolvingPassDone()) {
            return;
        }
        ++Direct.hcount;
        if (!(constraintWidget instanceof ConstraintWidgetContainer) && constraintWidget.isMeasureRequested()) {
            final int n2 = n + 1;
            if (canMeasure(n2, constraintWidget)) {
                ConstraintWidgetContainer.measure(n2, constraintWidget, measurer, new BasicMeasure.Measure(), BasicMeasure.Measure.SELF_DIMENSIONS);
            }
        }
        final ConstraintAnchor anchor = constraintWidget.getAnchor(ConstraintAnchor.Type.LEFT);
        final ConstraintAnchor anchor2 = constraintWidget.getAnchor(ConstraintAnchor.Type.RIGHT);
        final int finalValue = anchor.getFinalValue();
        final int finalValue2 = anchor2.getFinalValue();
        if (anchor.getDependents() != null && anchor.hasFinalValue()) {
            for (final ConstraintAnchor constraintAnchor : anchor.getDependents()) {
                final ConstraintWidget mOwner = constraintAnchor.mOwner;
                final int n3 = n + 1;
                final boolean canMeasure = canMeasure(n3, mOwner);
                if (mOwner.isMeasureRequested() && canMeasure) {
                    ConstraintWidgetContainer.measure(n3, mOwner, measurer, new BasicMeasure.Measure(), BasicMeasure.Measure.SELF_DIMENSIONS);
                }
                final boolean b2 = (constraintAnchor == mOwner.mLeft && mOwner.mRight.mTarget != null && mOwner.mRight.mTarget.hasFinalValue()) || (constraintAnchor == mOwner.mRight && mOwner.mLeft.mTarget != null && mOwner.mLeft.mTarget.hasFinalValue());
                if (mOwner.getHorizontalDimensionBehaviour() == ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT && !canMeasure) {
                    if (mOwner.getHorizontalDimensionBehaviour() != ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT || mOwner.mMatchConstraintMaxWidth < 0 || mOwner.mMatchConstraintMinWidth < 0 || (mOwner.getVisibility() != 8 && (mOwner.mMatchConstraintDefaultWidth != 0 || mOwner.getDimensionRatio() != 0.0f)) || mOwner.isInHorizontalChain() || mOwner.isInVirtualLayout() || !b2 || mOwner.isInHorizontalChain()) {
                        continue;
                    }
                    solveHorizontalMatchConstraint(n3, constraintWidget, measurer, mOwner, b);
                }
                else {
                    if (mOwner.isMeasureRequested()) {
                        continue;
                    }
                    if (constraintAnchor == mOwner.mLeft && mOwner.mRight.mTarget == null) {
                        final int n4 = mOwner.mLeft.getMargin() + finalValue;
                        mOwner.setFinalHorizontal(n4, mOwner.getWidth() + n4);
                        horizontalSolvingPass(n3, mOwner, measurer, b);
                    }
                    else if (constraintAnchor == mOwner.mRight && mOwner.mLeft.mTarget == null) {
                        final int n5 = finalValue - mOwner.mRight.getMargin();
                        mOwner.setFinalHorizontal(n5 - mOwner.getWidth(), n5);
                        horizontalSolvingPass(n3, mOwner, measurer, b);
                    }
                    else {
                        if (!b2 || mOwner.isInHorizontalChain()) {
                            continue;
                        }
                        solveHorizontalCenterConstraints(n3, measurer, mOwner, b);
                    }
                }
            }
        }
        if (constraintWidget instanceof Guideline) {
            return;
        }
        if (anchor2.getDependents() != null && anchor2.hasFinalValue()) {
            for (final ConstraintAnchor constraintAnchor2 : anchor2.getDependents()) {
                final ConstraintWidget mOwner2 = constraintAnchor2.mOwner;
                final int n6 = n + 1;
                final boolean canMeasure2 = canMeasure(n6, mOwner2);
                if (mOwner2.isMeasureRequested() && canMeasure2) {
                    ConstraintWidgetContainer.measure(n6, mOwner2, measurer, new BasicMeasure.Measure(), BasicMeasure.Measure.SELF_DIMENSIONS);
                }
                final boolean b3 = (constraintAnchor2 == mOwner2.mLeft && mOwner2.mRight.mTarget != null && mOwner2.mRight.mTarget.hasFinalValue()) || (constraintAnchor2 == mOwner2.mRight && mOwner2.mLeft.mTarget != null && mOwner2.mLeft.mTarget.hasFinalValue());
                if (mOwner2.getHorizontalDimensionBehaviour() == ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT && !canMeasure2) {
                    if (mOwner2.getHorizontalDimensionBehaviour() != ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT || mOwner2.mMatchConstraintMaxWidth < 0 || mOwner2.mMatchConstraintMinWidth < 0 || (mOwner2.getVisibility() != 8 && (mOwner2.mMatchConstraintDefaultWidth != 0 || mOwner2.getDimensionRatio() != 0.0f)) || mOwner2.isInHorizontalChain() || mOwner2.isInVirtualLayout() || !b3 || mOwner2.isInHorizontalChain()) {
                        continue;
                    }
                    solveHorizontalMatchConstraint(n6, constraintWidget, measurer, mOwner2, b);
                }
                else {
                    if (mOwner2.isMeasureRequested()) {
                        continue;
                    }
                    if (constraintAnchor2 == mOwner2.mLeft && mOwner2.mRight.mTarget == null) {
                        final int n7 = mOwner2.mLeft.getMargin() + finalValue2;
                        mOwner2.setFinalHorizontal(n7, mOwner2.getWidth() + n7);
                        horizontalSolvingPass(n6, mOwner2, measurer, b);
                    }
                    else if (constraintAnchor2 == mOwner2.mRight && mOwner2.mLeft.mTarget == null) {
                        final int n8 = finalValue2 - mOwner2.mRight.getMargin();
                        mOwner2.setFinalHorizontal(n8 - mOwner2.getWidth(), n8);
                        horizontalSolvingPass(n6, mOwner2, measurer, b);
                    }
                    else {
                        if (!b3 || mOwner2.isInHorizontalChain()) {
                            continue;
                        }
                        solveHorizontalCenterConstraints(n6, measurer, mOwner2, b);
                    }
                }
            }
        }
        constraintWidget.markHorizontalSolvingPassDone();
    }
    
    public static String ls(final int i) {
        final StringBuilder sb = new StringBuilder();
        for (int j = 0; j < i; ++j) {
            sb.append("  ");
        }
        final StringBuilder sb2 = new StringBuilder();
        sb2.append("+-(");
        sb2.append(i);
        sb2.append(") ");
        sb.append(sb2.toString());
        return sb.toString();
    }
    
    private static void solveBarrier(final int n, final Barrier barrier, final BasicMeasure.Measurer measurer, final int n2, final boolean b) {
        if (barrier.allSolved()) {
            if (n2 == 0) {
                horizontalSolvingPass(n + 1, barrier, measurer, b);
            }
            else {
                verticalSolvingPass(n + 1, barrier, measurer);
            }
        }
    }
    
    public static boolean solveChain(final ConstraintWidgetContainer constraintWidgetContainer, final LinearSystem linearSystem, final int n, int n2, final ChainHead chainHead, final boolean b, final boolean b2, final boolean b3) {
        if (b3) {
            return false;
        }
        if (n == 0) {
            if (!constraintWidgetContainer.isResolvedHorizontally()) {
                return false;
            }
        }
        else if (!constraintWidgetContainer.isResolvedVertically()) {
            return false;
        }
        final boolean rtl = constraintWidgetContainer.isRtl();
        final ConstraintWidget first = chainHead.getFirst();
        final ConstraintWidget last = chainHead.getLast();
        final ConstraintWidget firstVisibleWidget = chainHead.getFirstVisibleWidget();
        final ConstraintWidget lastVisibleWidget = chainHead.getLastVisibleWidget();
        final ConstraintWidget head = chainHead.getHead();
        final ConstraintAnchor constraintAnchor = first.mListAnchors[n2];
        final ConstraintAnchor[] mListAnchors = last.mListAnchors;
        final int n3 = n2 + 1;
        final ConstraintAnchor constraintAnchor2 = mListAnchors[n3];
        if (constraintAnchor.mTarget != null) {
            if (constraintAnchor2.mTarget != null) {
                if (constraintAnchor.mTarget.hasFinalValue()) {
                    if (constraintAnchor2.mTarget.hasFinalValue()) {
                        if (firstVisibleWidget != null) {
                            if (lastVisibleWidget != null) {
                                final int n4 = constraintAnchor.mTarget.getFinalValue() + firstVisibleWidget.mListAnchors[n2].getMargin();
                                final int n5 = constraintAnchor2.mTarget.getFinalValue() - lastVisibleWidget.mListAnchors[n3].getMargin();
                                final int n6 = n5 - n4;
                                if (n6 <= 0) {
                                    return false;
                                }
                                final BasicMeasure.Measure measure = new BasicMeasure.Measure();
                                ConstraintWidget constraintWidget = first;
                                int n7 = 0;
                                int n8 = 0;
                                int n9 = 0;
                                int n10 = 0;
                                final ConstraintWidget constraintWidget2 = first;
                                while (true) {
                                    final ConstraintWidget constraintWidget3 = null;
                                    if (n7 == 0) {
                                        if (!canMeasure(1, constraintWidget)) {
                                            return false;
                                        }
                                        if (constraintWidget.mListDimensionBehaviors[n] == ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT) {
                                            return false;
                                        }
                                        if (constraintWidget.isMeasureRequested()) {
                                            ConstraintWidgetContainer.measure(1, constraintWidget, constraintWidgetContainer.getMeasurer(), measure, BasicMeasure.Measure.SELF_DIMENSIONS);
                                        }
                                        final int margin = constraintWidget.mListAnchors[n2].getMargin();
                                        int n11;
                                        if (n == 0) {
                                            n11 = constraintWidget.getWidth();
                                        }
                                        else {
                                            n11 = constraintWidget.getHeight();
                                        }
                                        final int n12 = n10 + margin + n11 + constraintWidget.mListAnchors[n3].getMargin();
                                        ++n9;
                                        int n13 = n8;
                                        if (constraintWidget.getVisibility() != 8) {
                                            n13 = n8 + 1;
                                        }
                                        final ConstraintAnchor mTarget = constraintWidget.mListAnchors[n3].mTarget;
                                        ConstraintWidget constraintWidget4 = constraintWidget3;
                                        if (mTarget != null) {
                                            final ConstraintWidget mOwner = mTarget.mOwner;
                                            constraintWidget4 = constraintWidget3;
                                            if (mOwner.mListAnchors[n2].mTarget != null) {
                                                if (mOwner.mListAnchors[n2].mTarget.mOwner != constraintWidget) {
                                                    constraintWidget4 = constraintWidget3;
                                                }
                                                else {
                                                    constraintWidget4 = mOwner;
                                                }
                                            }
                                        }
                                        if (constraintWidget4 != null) {
                                            constraintWidget = constraintWidget4;
                                        }
                                        else {
                                            n7 = 1;
                                        }
                                        n8 = n13;
                                        n10 = n12;
                                    }
                                    else {
                                        if (n8 == 0) {
                                            return false;
                                        }
                                        if (n8 != n9) {
                                            return false;
                                        }
                                        if (n6 < n10) {
                                            return false;
                                        }
                                        final int n14 = n6 - n10;
                                        int n15;
                                        if (b) {
                                            n15 = n14 / (n8 + 1);
                                        }
                                        else {
                                            n15 = n14;
                                            if (b2) {
                                                n15 = n14;
                                                if (n8 > 2) {
                                                    n15 = n14 / n8 - 1;
                                                }
                                            }
                                        }
                                        if (n8 == 1) {
                                            float n16;
                                            if (n == 0) {
                                                n16 = head.getHorizontalBiasPercent();
                                            }
                                            else {
                                                n16 = head.getVerticalBiasPercent();
                                            }
                                            n2 = (int)(n4 + 0.5f + n15 * n16);
                                            if (n == 0) {
                                                firstVisibleWidget.setFinalHorizontal(n2, firstVisibleWidget.getWidth() + n2);
                                            }
                                            else {
                                                firstVisibleWidget.setFinalVertical(n2, firstVisibleWidget.getHeight() + n2);
                                            }
                                            horizontalSolvingPass(1, firstVisibleWidget, constraintWidgetContainer.getMeasurer(), rtl);
                                            return true;
                                        }
                                        if (b) {
                                            int n17 = n4 + n15;
                                            int i = 0;
                                            ConstraintWidget constraintWidget5 = constraintWidget2;
                                            while (i == 0) {
                                                if (constraintWidget5.getVisibility() == 8) {
                                                    if (n == 0) {
                                                        constraintWidget5.setFinalHorizontal(n17, n17);
                                                        horizontalSolvingPass(1, constraintWidget5, constraintWidgetContainer.getMeasurer(), rtl);
                                                    }
                                                    else {
                                                        constraintWidget5.setFinalVertical(n17, n17);
                                                        verticalSolvingPass(1, constraintWidget5, constraintWidgetContainer.getMeasurer());
                                                    }
                                                }
                                                else {
                                                    final int n18 = n17 + constraintWidget5.mListAnchors[n2].getMargin();
                                                    int n19;
                                                    if (n == 0) {
                                                        constraintWidget5.setFinalHorizontal(n18, constraintWidget5.getWidth() + n18);
                                                        horizontalSolvingPass(1, constraintWidget5, constraintWidgetContainer.getMeasurer(), rtl);
                                                        n19 = constraintWidget5.getWidth();
                                                    }
                                                    else {
                                                        constraintWidget5.setFinalVertical(n18, constraintWidget5.getHeight() + n18);
                                                        verticalSolvingPass(1, constraintWidget5, constraintWidgetContainer.getMeasurer());
                                                        n19 = constraintWidget5.getHeight();
                                                    }
                                                    n17 = n18 + n19 + constraintWidget5.mListAnchors[n3].getMargin() + n15;
                                                }
                                                constraintWidget5.addToSolver(linearSystem, false);
                                                final ConstraintAnchor mTarget2 = constraintWidget5.mListAnchors[n3].mTarget;
                                                ConstraintWidget constraintWidget6 = null;
                                                Label_0915: {
                                                    if (mTarget2 != null) {
                                                        final ConstraintWidget mOwner2 = mTarget2.mOwner;
                                                        if (mOwner2.mListAnchors[n2].mTarget != null) {
                                                            constraintWidget6 = mOwner2;
                                                            if (mOwner2.mListAnchors[n2].mTarget.mOwner == constraintWidget5) {
                                                                break Label_0915;
                                                            }
                                                        }
                                                    }
                                                    constraintWidget6 = null;
                                                }
                                                if (constraintWidget6 == null) {
                                                    i = 1;
                                                    constraintWidget6 = constraintWidget5;
                                                }
                                                constraintWidget5 = constraintWidget6;
                                            }
                                        }
                                        else if (b2) {
                                            if (n8 == 2) {
                                                if (n == 0) {
                                                    firstVisibleWidget.setFinalHorizontal(n4, firstVisibleWidget.getWidth() + n4);
                                                    lastVisibleWidget.setFinalHorizontal(n5 - lastVisibleWidget.getWidth(), n5);
                                                    horizontalSolvingPass(1, firstVisibleWidget, constraintWidgetContainer.getMeasurer(), rtl);
                                                    horizontalSolvingPass(1, lastVisibleWidget, constraintWidgetContainer.getMeasurer(), rtl);
                                                }
                                                else {
                                                    firstVisibleWidget.setFinalVertical(n4, firstVisibleWidget.getHeight() + n4);
                                                    lastVisibleWidget.setFinalVertical(n5 - lastVisibleWidget.getHeight(), n5);
                                                    verticalSolvingPass(1, firstVisibleWidget, constraintWidgetContainer.getMeasurer());
                                                    verticalSolvingPass(1, lastVisibleWidget, constraintWidgetContainer.getMeasurer());
                                                }
                                                return true;
                                            }
                                            return false;
                                        }
                                        return true;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return false;
    }
    
    private static void solveHorizontalCenterConstraints(final int n, final BasicMeasure.Measurer measurer, final ConstraintWidget constraintWidget, final boolean b) {
        float horizontalBiasPercent = constraintWidget.getHorizontalBiasPercent();
        int finalValue = constraintWidget.mLeft.mTarget.getFinalValue();
        int finalValue2 = constraintWidget.mRight.mTarget.getFinalValue();
        final int margin = constraintWidget.mLeft.getMargin();
        final int margin2 = constraintWidget.mRight.getMargin();
        if (finalValue == finalValue2) {
            horizontalBiasPercent = 0.5f;
        }
        else {
            finalValue += margin;
            finalValue2 -= margin2;
        }
        final int width = constraintWidget.getWidth();
        int n2 = finalValue2 - finalValue - width;
        if (finalValue > finalValue2) {
            n2 = finalValue - finalValue2 - width;
        }
        float n3;
        if (n2 > 0) {
            n3 = horizontalBiasPercent * n2 + 0.5f;
        }
        else {
            n3 = horizontalBiasPercent * n2;
        }
        final int n4 = (int)n3 + finalValue;
        int n5 = n4 + width;
        if (finalValue > finalValue2) {
            n5 = n4 - width;
        }
        constraintWidget.setFinalHorizontal(n4, n5);
        horizontalSolvingPass(n + 1, constraintWidget, measurer, b);
    }
    
    private static void solveHorizontalMatchConstraint(final int n, final ConstraintWidget constraintWidget, final BasicMeasure.Measurer measurer, final ConstraintWidget constraintWidget2, final boolean b) {
        final float horizontalBiasPercent = constraintWidget2.getHorizontalBiasPercent();
        final int n2 = constraintWidget2.mLeft.mTarget.getFinalValue() + constraintWidget2.mLeft.getMargin();
        final int n3 = constraintWidget2.mRight.mTarget.getFinalValue() - constraintWidget2.mRight.getMargin();
        if (n3 >= n2) {
            int n5;
            final int n4 = n5 = constraintWidget2.getWidth();
            if (constraintWidget2.getVisibility() != 8) {
                int b2;
                if (constraintWidget2.mMatchConstraintDefaultWidth == 2) {
                    int n6;
                    if (constraintWidget instanceof ConstraintWidgetContainer) {
                        n6 = constraintWidget.getWidth();
                    }
                    else {
                        n6 = constraintWidget.getParent().getWidth();
                    }
                    b2 = (int)(constraintWidget2.getHorizontalBiasPercent() * 0.5f * n6);
                }
                else {
                    b2 = n4;
                    if (constraintWidget2.mMatchConstraintDefaultWidth == 0) {
                        b2 = n3 - n2;
                    }
                }
                final int b3 = n5 = Math.max(constraintWidget2.mMatchConstraintMinWidth, b2);
                if (constraintWidget2.mMatchConstraintMaxWidth > 0) {
                    n5 = Math.min(constraintWidget2.mMatchConstraintMaxWidth, b3);
                }
            }
            final int n7 = n2 + (int)(horizontalBiasPercent * (n3 - n2 - n5) + 0.5f);
            constraintWidget2.setFinalHorizontal(n7, n5 + n7);
            horizontalSolvingPass(n + 1, constraintWidget2, measurer, b);
        }
    }
    
    private static void solveVerticalCenterConstraints(final int n, final BasicMeasure.Measurer measurer, final ConstraintWidget constraintWidget) {
        float verticalBiasPercent = constraintWidget.getVerticalBiasPercent();
        int finalValue = constraintWidget.mTop.mTarget.getFinalValue();
        int finalValue2 = constraintWidget.mBottom.mTarget.getFinalValue();
        final int margin = constraintWidget.mTop.getMargin();
        final int margin2 = constraintWidget.mBottom.getMargin();
        if (finalValue == finalValue2) {
            verticalBiasPercent = 0.5f;
        }
        else {
            finalValue += margin;
            finalValue2 -= margin2;
        }
        final int height = constraintWidget.getHeight();
        int n2 = finalValue2 - finalValue - height;
        if (finalValue > finalValue2) {
            n2 = finalValue - finalValue2 - height;
        }
        float n3;
        if (n2 > 0) {
            n3 = verticalBiasPercent * n2 + 0.5f;
        }
        else {
            n3 = verticalBiasPercent * n2;
        }
        final int n4 = (int)n3;
        int n5 = finalValue + n4;
        int n6 = n5 + height;
        if (finalValue > finalValue2) {
            n5 = finalValue - n4;
            n6 = n5 - height;
        }
        constraintWidget.setFinalVertical(n5, n6);
        verticalSolvingPass(n + 1, constraintWidget, measurer);
    }
    
    private static void solveVerticalMatchConstraint(final int n, final ConstraintWidget constraintWidget, final BasicMeasure.Measurer measurer, final ConstraintWidget constraintWidget2) {
        final float verticalBiasPercent = constraintWidget2.getVerticalBiasPercent();
        final int n2 = constraintWidget2.mTop.mTarget.getFinalValue() + constraintWidget2.mTop.getMargin();
        final int n3 = constraintWidget2.mBottom.mTarget.getFinalValue() - constraintWidget2.mBottom.getMargin();
        if (n3 >= n2) {
            int n5;
            final int n4 = n5 = constraintWidget2.getHeight();
            if (constraintWidget2.getVisibility() != 8) {
                int b;
                if (constraintWidget2.mMatchConstraintDefaultHeight == 2) {
                    int n6;
                    if (constraintWidget instanceof ConstraintWidgetContainer) {
                        n6 = constraintWidget.getHeight();
                    }
                    else {
                        n6 = constraintWidget.getParent().getHeight();
                    }
                    b = (int)(verticalBiasPercent * 0.5f * n6);
                }
                else {
                    b = n4;
                    if (constraintWidget2.mMatchConstraintDefaultHeight == 0) {
                        b = n3 - n2;
                    }
                }
                final int b2 = n5 = Math.max(constraintWidget2.mMatchConstraintMinHeight, b);
                if (constraintWidget2.mMatchConstraintMaxHeight > 0) {
                    n5 = Math.min(constraintWidget2.mMatchConstraintMaxHeight, b2);
                }
            }
            final int n7 = n2 + (int)(verticalBiasPercent * (n3 - n2 - n5) + 0.5f);
            constraintWidget2.setFinalVertical(n7, n5 + n7);
            verticalSolvingPass(n + 1, constraintWidget2, measurer);
        }
    }
    
    public static void solvingPass(final ConstraintWidgetContainer constraintWidgetContainer, final BasicMeasure.Measurer measurer) {
        final ConstraintWidget.DimensionBehaviour horizontalDimensionBehaviour = constraintWidgetContainer.getHorizontalDimensionBehaviour();
        final ConstraintWidget.DimensionBehaviour verticalDimensionBehaviour = constraintWidgetContainer.getVerticalDimensionBehaviour();
        Direct.hcount = 0;
        Direct.vcount = 0;
        constraintWidgetContainer.resetFinalResolution();
        final ArrayList<ConstraintWidget> children = constraintWidgetContainer.getChildren();
        final int size = children.size();
        for (int i = 0; i < size; ++i) {
            ((ConstraintWidget)children.get(i)).resetFinalResolution();
        }
        final boolean rtl = constraintWidgetContainer.isRtl();
        if (horizontalDimensionBehaviour == ConstraintWidget.DimensionBehaviour.FIXED) {
            constraintWidgetContainer.setFinalHorizontal(0, constraintWidgetContainer.getWidth());
        }
        else {
            constraintWidgetContainer.setFinalLeft(0);
        }
        int j = 0;
        int n = 0;
        int n2 = 0;
        while (j < size) {
            final ConstraintWidget constraintWidget = children.get(j);
            int n3;
            int n4;
            if (constraintWidget instanceof Guideline) {
                final Guideline guideline = (Guideline)constraintWidget;
                n3 = n;
                n4 = n2;
                if (guideline.getOrientation() == 1) {
                    if (guideline.getRelativeBegin() != -1) {
                        guideline.setFinalValue(guideline.getRelativeBegin());
                    }
                    else if (guideline.getRelativeEnd() != -1 && constraintWidgetContainer.isResolvedHorizontally()) {
                        guideline.setFinalValue(constraintWidgetContainer.getWidth() - guideline.getRelativeEnd());
                    }
                    else if (constraintWidgetContainer.isResolvedHorizontally()) {
                        guideline.setFinalValue((int)(guideline.getRelativePercent() * constraintWidgetContainer.getWidth() + 0.5f));
                    }
                    n3 = 1;
                    n4 = n2;
                }
            }
            else {
                n3 = n;
                n4 = n2;
                if (constraintWidget instanceof Barrier) {
                    n3 = n;
                    n4 = n2;
                    if (((Barrier)constraintWidget).getOrientation() == 0) {
                        n4 = 1;
                        n3 = n;
                    }
                }
            }
            ++j;
            n = n3;
            n2 = n4;
        }
        if (n != 0) {
            for (int k = 0; k < size; ++k) {
                final ConstraintWidget constraintWidget2 = children.get(k);
                if (constraintWidget2 instanceof Guideline) {
                    final Guideline guideline2 = (Guideline)constraintWidget2;
                    if (guideline2.getOrientation() == 1) {
                        horizontalSolvingPass(0, guideline2, measurer, rtl);
                    }
                }
            }
        }
        horizontalSolvingPass(0, constraintWidgetContainer, measurer, rtl);
        if (n2 != 0) {
            for (int l = 0; l < size; ++l) {
                final ConstraintWidget constraintWidget3 = children.get(l);
                if (constraintWidget3 instanceof Barrier) {
                    final Barrier barrier = (Barrier)constraintWidget3;
                    if (barrier.getOrientation() == 0) {
                        solveBarrier(0, barrier, measurer, 0, rtl);
                    }
                }
            }
        }
        if (verticalDimensionBehaviour == ConstraintWidget.DimensionBehaviour.FIXED) {
            constraintWidgetContainer.setFinalVertical(0, constraintWidgetContainer.getHeight());
        }
        else {
            constraintWidgetContainer.setFinalTop(0);
        }
        int index = 0;
        int n5 = 0;
        int n6 = 0;
        while (index < size) {
            final ConstraintWidget constraintWidget4 = children.get(index);
            int n7;
            int n8;
            if (constraintWidget4 instanceof Guideline) {
                final Guideline guideline3 = (Guideline)constraintWidget4;
                n7 = n5;
                n8 = n6;
                if (guideline3.getOrientation() == 0) {
                    if (guideline3.getRelativeBegin() != -1) {
                        guideline3.setFinalValue(guideline3.getRelativeBegin());
                    }
                    else if (guideline3.getRelativeEnd() != -1 && constraintWidgetContainer.isResolvedVertically()) {
                        guideline3.setFinalValue(constraintWidgetContainer.getHeight() - guideline3.getRelativeEnd());
                    }
                    else if (constraintWidgetContainer.isResolvedVertically()) {
                        guideline3.setFinalValue((int)(guideline3.getRelativePercent() * constraintWidgetContainer.getHeight() + 0.5f));
                    }
                    n7 = 1;
                    n8 = n6;
                }
            }
            else {
                n7 = n5;
                n8 = n6;
                if (constraintWidget4 instanceof Barrier) {
                    n7 = n5;
                    n8 = n6;
                    if (((Barrier)constraintWidget4).getOrientation() == 1) {
                        n8 = 1;
                        n7 = n5;
                    }
                }
            }
            ++index;
            n5 = n7;
            n6 = n8;
        }
        if (n5 != 0) {
            for (int index2 = 0; index2 < size; ++index2) {
                final ConstraintWidget constraintWidget5 = children.get(index2);
                if (constraintWidget5 instanceof Guideline) {
                    final Guideline guideline4 = (Guideline)constraintWidget5;
                    if (guideline4.getOrientation() == 0) {
                        verticalSolvingPass(1, guideline4, measurer);
                    }
                }
            }
        }
        verticalSolvingPass(0, constraintWidgetContainer, measurer);
        if (n6 != 0) {
            for (int index3 = 0; index3 < size; ++index3) {
                final ConstraintWidget constraintWidget6 = children.get(index3);
                if (constraintWidget6 instanceof Barrier) {
                    final Barrier barrier2 = (Barrier)constraintWidget6;
                    if (barrier2.getOrientation() == 1) {
                        solveBarrier(0, barrier2, measurer, 1, rtl);
                    }
                }
            }
        }
        for (int index4 = 0; index4 < size; ++index4) {
            final ConstraintWidget constraintWidget7 = children.get(index4);
            if (constraintWidget7.isMeasureRequested() && canMeasure(0, constraintWidget7)) {
                ConstraintWidgetContainer.measure(0, constraintWidget7, measurer, Direct.measure, BasicMeasure.Measure.SELF_DIMENSIONS);
                if (constraintWidget7 instanceof Guideline) {
                    if (((Guideline)constraintWidget7).getOrientation() == 0) {
                        verticalSolvingPass(0, constraintWidget7, measurer);
                    }
                    else {
                        horizontalSolvingPass(0, constraintWidget7, measurer, rtl);
                    }
                }
                else {
                    horizontalSolvingPass(0, constraintWidget7, measurer, rtl);
                    verticalSolvingPass(0, constraintWidget7, measurer);
                }
            }
        }
    }
    
    private static void verticalSolvingPass(final int n, final ConstraintWidget constraintWidget, final BasicMeasure.Measurer measurer) {
        if (constraintWidget.isVerticalSolvingPassDone()) {
            return;
        }
        ++Direct.vcount;
        if (!(constraintWidget instanceof ConstraintWidgetContainer) && constraintWidget.isMeasureRequested()) {
            final int finalValue = n + 1;
            if (canMeasure(finalValue, constraintWidget)) {
                ConstraintWidgetContainer.measure(finalValue, constraintWidget, measurer, new BasicMeasure.Measure(), BasicMeasure.Measure.SELF_DIMENSIONS);
            }
        }
        final ConstraintAnchor anchor = constraintWidget.getAnchor(ConstraintAnchor.Type.TOP);
        final ConstraintAnchor anchor2 = constraintWidget.getAnchor(ConstraintAnchor.Type.BOTTOM);
        final int finalValue2 = anchor.getFinalValue();
        final int finalValue3 = anchor2.getFinalValue();
        if (anchor.getDependents() != null && anchor.hasFinalValue()) {
            for (final ConstraintAnchor constraintAnchor : anchor.getDependents()) {
                final ConstraintWidget mOwner = constraintAnchor.mOwner;
                final int n2 = n + 1;
                final boolean canMeasure = canMeasure(n2, mOwner);
                if (mOwner.isMeasureRequested() && canMeasure) {
                    ConstraintWidgetContainer.measure(n2, mOwner, measurer, new BasicMeasure.Measure(), BasicMeasure.Measure.SELF_DIMENSIONS);
                }
                int finalValue;
                if ((constraintAnchor == mOwner.mTop && mOwner.mBottom.mTarget != null && mOwner.mBottom.mTarget.hasFinalValue()) || (constraintAnchor == mOwner.mBottom && mOwner.mTop.mTarget != null && mOwner.mTop.mTarget.hasFinalValue())) {
                    finalValue = 1;
                }
                else {
                    finalValue = 0;
                }
                if (mOwner.getVerticalDimensionBehaviour() == ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT && !canMeasure) {
                    if (mOwner.getVerticalDimensionBehaviour() != ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT || mOwner.mMatchConstraintMaxHeight < 0 || mOwner.mMatchConstraintMinHeight < 0 || (mOwner.getVisibility() != 8 && (mOwner.mMatchConstraintDefaultHeight != 0 || mOwner.getDimensionRatio() != 0.0f)) || mOwner.isInVerticalChain() || mOwner.isInVirtualLayout() || finalValue == 0 || mOwner.isInVerticalChain()) {
                        continue;
                    }
                    solveVerticalMatchConstraint(n2, constraintWidget, measurer, mOwner);
                }
                else {
                    if (mOwner.isMeasureRequested()) {
                        continue;
                    }
                    if (constraintAnchor == mOwner.mTop && mOwner.mBottom.mTarget == null) {
                        finalValue = mOwner.mTop.getMargin() + finalValue2;
                        mOwner.setFinalVertical(finalValue, mOwner.getHeight() + finalValue);
                        verticalSolvingPass(n2, mOwner, measurer);
                    }
                    else if (constraintAnchor == mOwner.mBottom && mOwner.mTop.mTarget == null) {
                        finalValue = finalValue2 - mOwner.mBottom.getMargin();
                        mOwner.setFinalVertical(finalValue - mOwner.getHeight(), finalValue);
                        verticalSolvingPass(n2, mOwner, measurer);
                    }
                    else {
                        if (finalValue == 0 || mOwner.isInVerticalChain()) {
                            continue;
                        }
                        solveVerticalCenterConstraints(n2, measurer, mOwner);
                    }
                }
            }
        }
        if (constraintWidget instanceof Guideline) {
            return;
        }
        if (anchor2.getDependents() != null && anchor2.hasFinalValue()) {
            for (final ConstraintAnchor constraintAnchor2 : anchor2.getDependents()) {
                final ConstraintWidget mOwner2 = constraintAnchor2.mOwner;
                final int n3 = n + 1;
                final boolean canMeasure2 = canMeasure(n3, mOwner2);
                if (mOwner2.isMeasureRequested() && canMeasure2) {
                    ConstraintWidgetContainer.measure(n3, mOwner2, measurer, new BasicMeasure.Measure(), BasicMeasure.Measure.SELF_DIMENSIONS);
                }
                int finalValue;
                if ((constraintAnchor2 == mOwner2.mTop && mOwner2.mBottom.mTarget != null && mOwner2.mBottom.mTarget.hasFinalValue()) || (constraintAnchor2 == mOwner2.mBottom && mOwner2.mTop.mTarget != null && mOwner2.mTop.mTarget.hasFinalValue())) {
                    finalValue = 1;
                }
                else {
                    finalValue = 0;
                }
                if (mOwner2.getVerticalDimensionBehaviour() == ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT && !canMeasure2) {
                    if (mOwner2.getVerticalDimensionBehaviour() != ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT || mOwner2.mMatchConstraintMaxHeight < 0 || mOwner2.mMatchConstraintMinHeight < 0 || (mOwner2.getVisibility() != 8 && (mOwner2.mMatchConstraintDefaultHeight != 0 || mOwner2.getDimensionRatio() != 0.0f)) || mOwner2.isInVerticalChain() || mOwner2.isInVirtualLayout() || finalValue == 0 || mOwner2.isInVerticalChain()) {
                        continue;
                    }
                    solveVerticalMatchConstraint(n3, constraintWidget, measurer, mOwner2);
                }
                else {
                    if (mOwner2.isMeasureRequested()) {
                        continue;
                    }
                    if (constraintAnchor2 == mOwner2.mTop && mOwner2.mBottom.mTarget == null) {
                        finalValue = mOwner2.mTop.getMargin() + finalValue3;
                        mOwner2.setFinalVertical(finalValue, mOwner2.getHeight() + finalValue);
                        verticalSolvingPass(n3, mOwner2, measurer);
                    }
                    else if (constraintAnchor2 == mOwner2.mBottom && mOwner2.mTop.mTarget == null) {
                        finalValue = finalValue3 - mOwner2.mBottom.getMargin();
                        mOwner2.setFinalVertical(finalValue - mOwner2.getHeight(), finalValue);
                        verticalSolvingPass(n3, mOwner2, measurer);
                    }
                    else {
                        if (finalValue == 0 || mOwner2.isInVerticalChain()) {
                            continue;
                        }
                        solveVerticalCenterConstraints(n3, measurer, mOwner2);
                    }
                }
            }
        }
        Object o = constraintWidget.getAnchor(ConstraintAnchor.Type.BASELINE);
        Label_1155: {
            if (((ConstraintAnchor)o).getDependents() == null || !((ConstraintAnchor)o).hasFinalValue()) {
                break Label_1155;
            }
            final int finalValue = ((ConstraintAnchor)o).getFinalValue();
            while (true) {
                o = ((ConstraintAnchor)o).getDependents().iterator();
                ConstraintAnchor constraintAnchor3 = null;
                ConstraintWidget mOwner3 = null;
                int n4 = 0;
                Block_67: {
                    while (((Iterator)o).hasNext()) {
                        constraintAnchor3 = (ConstraintAnchor)((Iterator)o).next();
                        mOwner3 = constraintAnchor3.mOwner;
                        n4 = n + 1;
                        final boolean canMeasure3 = canMeasure(n4, mOwner3);
                        if (mOwner3.isMeasureRequested() && canMeasure3) {
                            ConstraintWidgetContainer.measure(n4, mOwner3, measurer, new BasicMeasure.Measure(), BasicMeasure.Measure.SELF_DIMENSIONS);
                        }
                        if (mOwner3.getVerticalDimensionBehaviour() != ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT || canMeasure3) {
                            if (mOwner3.isMeasureRequested()) {
                                continue;
                            }
                            if (constraintAnchor3 == mOwner3.mBaseline) {
                                break Block_67;
                            }
                            continue;
                        }
                    }
                    break Label_1155;
                }
                mOwner3.setFinalBaseline(constraintAnchor3.getMargin() + finalValue);
                try {
                    verticalSolvingPass(n4, mOwner3, measurer);
                    continue;
                    constraintWidget.markVerticalSolvingPassDone();
                }
                finally {}
                break;
            }
        }
    }
}

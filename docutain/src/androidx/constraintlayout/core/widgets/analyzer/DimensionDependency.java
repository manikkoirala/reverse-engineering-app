// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.core.widgets.analyzer;

import java.util.Iterator;

class DimensionDependency extends DependencyNode
{
    public int wrapValue;
    
    public DimensionDependency(final WidgetRun widgetRun) {
        super(widgetRun);
        if (widgetRun instanceof HorizontalWidgetRun) {
            this.type = Type.HORIZONTAL_DIMENSION;
        }
        else {
            this.type = Type.VERTICAL_DIMENSION;
        }
    }
    
    @Override
    public void resolve(final int value) {
        if (this.resolved) {
            return;
        }
        this.resolved = true;
        this.value = value;
        for (final Dependency dependency : this.dependencies) {
            dependency.update(dependency);
        }
    }
}

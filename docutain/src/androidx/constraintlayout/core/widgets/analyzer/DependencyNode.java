// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.core.widgets.analyzer;

import java.lang.constant.Constable;
import java.util.Iterator;
import java.util.ArrayList;
import java.util.List;

public class DependencyNode implements Dependency
{
    public boolean delegateToWidgetRun;
    List<Dependency> dependencies;
    int margin;
    DimensionDependency marginDependency;
    int marginFactor;
    public boolean readyToSolve;
    public boolean resolved;
    WidgetRun run;
    List<DependencyNode> targets;
    Type type;
    public Dependency updateDelegate;
    public int value;
    
    public DependencyNode(final WidgetRun run) {
        this.updateDelegate = null;
        this.delegateToWidgetRun = false;
        this.readyToSolve = false;
        this.type = Type.UNKNOWN;
        this.marginFactor = 1;
        this.marginDependency = null;
        this.resolved = false;
        this.dependencies = new ArrayList<Dependency>();
        this.targets = new ArrayList<DependencyNode>();
        this.run = run;
    }
    
    public void addDependency(final Dependency dependency) {
        this.dependencies.add(dependency);
        if (this.resolved) {
            dependency.update(dependency);
        }
    }
    
    public void clear() {
        this.targets.clear();
        this.dependencies.clear();
        this.resolved = false;
        this.value = 0;
        this.readyToSolve = false;
        this.delegateToWidgetRun = false;
    }
    
    public String name() {
        final String debugName = this.run.widget.getDebugName();
        String str;
        if (this.type != Type.LEFT && this.type != Type.RIGHT) {
            final StringBuilder sb = new StringBuilder();
            sb.append(debugName);
            sb.append("_VERTICAL");
            str = sb.toString();
        }
        else {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append(debugName);
            sb2.append("_HORIZONTAL");
            str = sb2.toString();
        }
        final StringBuilder sb3 = new StringBuilder();
        sb3.append(str);
        sb3.append(":");
        sb3.append(this.type.name());
        return sb3.toString();
    }
    
    public void resolve(final int value) {
        if (this.resolved) {
            return;
        }
        this.resolved = true;
        this.value = value;
        for (final Dependency dependency : this.dependencies) {
            dependency.update(dependency);
        }
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append(this.run.widget.getDebugName());
        sb.append(":");
        sb.append(this.type);
        sb.append("(");
        Constable value;
        if (this.resolved) {
            value = this.value;
        }
        else {
            value = "unresolved";
        }
        sb.append(value);
        sb.append(") <t=");
        sb.append(this.targets.size());
        sb.append(":d=");
        sb.append(this.dependencies.size());
        sb.append(">");
        return sb.toString();
    }
    
    @Override
    public void update(Dependency dependency) {
        final Iterator<DependencyNode> iterator = this.targets.iterator();
        while (iterator.hasNext()) {
            if (!iterator.next().resolved) {
                return;
            }
        }
        this.readyToSolve = true;
        dependency = this.updateDelegate;
        if (dependency != null) {
            dependency.update(this);
        }
        if (this.delegateToWidgetRun) {
            this.run.update(this);
            return;
        }
        DependencyNode dependencyNode = null;
        int n = 0;
        for (final DependencyNode dependencyNode2 : this.targets) {
            if (dependencyNode2 instanceof DimensionDependency) {
                continue;
            }
            ++n;
            dependencyNode = dependencyNode2;
        }
        if (dependencyNode != null && n == 1 && dependencyNode.resolved) {
            final DimensionDependency marginDependency = this.marginDependency;
            if (marginDependency != null) {
                if (!marginDependency.resolved) {
                    return;
                }
                this.margin = this.marginFactor * this.marginDependency.value;
            }
            this.resolve(dependencyNode.value + this.margin);
        }
        dependency = this.updateDelegate;
        if (dependency != null) {
            dependency.update(this);
        }
    }
    
    enum Type
    {
        private static final Type[] $VALUES;
        
        BASELINE, 
        BOTTOM, 
        HORIZONTAL_DIMENSION, 
        LEFT, 
        RIGHT, 
        TOP, 
        UNKNOWN, 
        VERTICAL_DIMENSION;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.core.widgets.analyzer;

import androidx.constraintlayout.core.widgets.Guideline;
import androidx.constraintlayout.core.widgets.ConstraintWidget;

class GuidelineReference extends WidgetRun
{
    public GuidelineReference(final ConstraintWidget constraintWidget) {
        super(constraintWidget);
        constraintWidget.horizontalRun.clear();
        constraintWidget.verticalRun.clear();
        this.orientation = ((Guideline)constraintWidget).getOrientation();
    }
    
    private void addDependency(final DependencyNode dependencyNode) {
        this.start.dependencies.add(dependencyNode);
        dependencyNode.targets.add(this.start);
    }
    
    @Override
    void apply() {
        final Guideline guideline = (Guideline)this.widget;
        final int relativeBegin = guideline.getRelativeBegin();
        final int relativeEnd = guideline.getRelativeEnd();
        guideline.getRelativePercent();
        if (guideline.getOrientation() == 1) {
            if (relativeBegin != -1) {
                this.start.targets.add(this.widget.mParent.horizontalRun.start);
                this.widget.mParent.horizontalRun.start.dependencies.add(this.start);
                this.start.margin = relativeBegin;
            }
            else if (relativeEnd != -1) {
                this.start.targets.add(this.widget.mParent.horizontalRun.end);
                this.widget.mParent.horizontalRun.end.dependencies.add(this.start);
                this.start.margin = -relativeEnd;
            }
            else {
                this.start.delegateToWidgetRun = true;
                this.start.targets.add(this.widget.mParent.horizontalRun.end);
                this.widget.mParent.horizontalRun.end.dependencies.add(this.start);
            }
            this.addDependency(this.widget.horizontalRun.start);
            this.addDependency(this.widget.horizontalRun.end);
        }
        else {
            if (relativeBegin != -1) {
                this.start.targets.add(this.widget.mParent.verticalRun.start);
                this.widget.mParent.verticalRun.start.dependencies.add(this.start);
                this.start.margin = relativeBegin;
            }
            else if (relativeEnd != -1) {
                this.start.targets.add(this.widget.mParent.verticalRun.end);
                this.widget.mParent.verticalRun.end.dependencies.add(this.start);
                this.start.margin = -relativeEnd;
            }
            else {
                this.start.delegateToWidgetRun = true;
                this.start.targets.add(this.widget.mParent.verticalRun.end);
                this.widget.mParent.verticalRun.end.dependencies.add(this.start);
            }
            this.addDependency(this.widget.verticalRun.start);
            this.addDependency(this.widget.verticalRun.end);
        }
    }
    
    public void applyToWidget() {
        if (((Guideline)this.widget).getOrientation() == 1) {
            this.widget.setX(this.start.value);
        }
        else {
            this.widget.setY(this.start.value);
        }
    }
    
    @Override
    void clear() {
        this.start.clear();
    }
    
    @Override
    void reset() {
        this.start.resolved = false;
        this.end.resolved = false;
    }
    
    @Override
    boolean supportsWrapComputation() {
        return false;
    }
    
    @Override
    public void update(final Dependency dependency) {
        if (!this.start.readyToSolve) {
            return;
        }
        if (this.start.resolved) {
            return;
        }
        this.start.resolve((int)(this.start.targets.get(0).value * ((Guideline)this.widget).getRelativePercent() + 0.5f));
    }
}

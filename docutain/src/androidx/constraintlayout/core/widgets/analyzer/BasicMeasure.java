// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.core.widgets.analyzer;

import androidx.constraintlayout.core.widgets.Helper;
import androidx.constraintlayout.core.widgets.ConstraintAnchor;
import androidx.constraintlayout.core.LinearSystem;
import androidx.constraintlayout.core.widgets.Optimizer;
import androidx.constraintlayout.core.Metrics;
import androidx.constraintlayout.core.widgets.VirtualLayout;
import androidx.constraintlayout.core.widgets.Barrier;
import androidx.constraintlayout.core.widgets.Guideline;
import androidx.constraintlayout.core.widgets.ConstraintWidget;
import java.util.ArrayList;
import androidx.constraintlayout.core.widgets.ConstraintWidgetContainer;

public class BasicMeasure
{
    public static final int AT_MOST = Integer.MIN_VALUE;
    private static final boolean DEBUG = false;
    public static final int EXACTLY = 1073741824;
    public static final int FIXED = -3;
    public static final int MATCH_PARENT = -1;
    private static final int MODE_SHIFT = 30;
    public static final int UNSPECIFIED = 0;
    public static final int WRAP_CONTENT = -2;
    private ConstraintWidgetContainer constraintWidgetContainer;
    private Measure mMeasure;
    private final ArrayList<ConstraintWidget> mVariableDimensionsWidgets;
    
    public BasicMeasure(final ConstraintWidgetContainer constraintWidgetContainer) {
        this.mVariableDimensionsWidgets = new ArrayList<ConstraintWidget>();
        this.mMeasure = new Measure();
        this.constraintWidgetContainer = constraintWidgetContainer;
    }
    
    private boolean measure(final Measurer measurer, final ConstraintWidget constraintWidget, int measureStrategy) {
        this.mMeasure.horizontalBehavior = constraintWidget.getHorizontalDimensionBehaviour();
        this.mMeasure.verticalBehavior = constraintWidget.getVerticalDimensionBehaviour();
        this.mMeasure.horizontalDimension = constraintWidget.getWidth();
        this.mMeasure.verticalDimension = constraintWidget.getHeight();
        this.mMeasure.measuredNeedsSolverPass = false;
        this.mMeasure.measureStrategy = measureStrategy;
        final boolean b = this.mMeasure.horizontalBehavior == ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT;
        if (this.mMeasure.verticalBehavior == ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT) {
            measureStrategy = 1;
        }
        else {
            measureStrategy = 0;
        }
        final boolean b2 = b && constraintWidget.mDimensionRatio > 0.0f;
        if (measureStrategy != 0 && constraintWidget.mDimensionRatio > 0.0f) {
            measureStrategy = 1;
        }
        else {
            measureStrategy = 0;
        }
        if (b2 && constraintWidget.mResolvedMatchConstraintDefault[0] == 4) {
            this.mMeasure.horizontalBehavior = ConstraintWidget.DimensionBehaviour.FIXED;
        }
        if (measureStrategy != 0 && constraintWidget.mResolvedMatchConstraintDefault[1] == 4) {
            this.mMeasure.verticalBehavior = ConstraintWidget.DimensionBehaviour.FIXED;
        }
        measurer.measure(constraintWidget, this.mMeasure);
        constraintWidget.setWidth(this.mMeasure.measuredWidth);
        constraintWidget.setHeight(this.mMeasure.measuredHeight);
        constraintWidget.setHasBaseline(this.mMeasure.measuredHasBaseline);
        constraintWidget.setBaselineDistance(this.mMeasure.measuredBaseline);
        this.mMeasure.measureStrategy = Measure.SELF_DIMENSIONS;
        return this.mMeasure.measuredNeedsSolverPass;
    }
    
    private void measureChildren(final ConstraintWidgetContainer constraintWidgetContainer) {
        final int size = constraintWidgetContainer.mChildren.size();
        final boolean optimize = constraintWidgetContainer.optimizeFor(64);
        final Measurer measurer = constraintWidgetContainer.getMeasurer();
        for (int i = 0; i < size; ++i) {
            final ConstraintWidget constraintWidget = constraintWidgetContainer.mChildren.get(i);
            if (!(constraintWidget instanceof Guideline)) {
                if (!(constraintWidget instanceof Barrier)) {
                    if (!constraintWidget.isInVirtualLayout()) {
                        if (!optimize || constraintWidget.horizontalRun == null || constraintWidget.verticalRun == null || !constraintWidget.horizontalRun.dimension.resolved || !constraintWidget.verticalRun.dimension.resolved) {
                            final ConstraintWidget.DimensionBehaviour dimensionBehaviour = constraintWidget.getDimensionBehaviour(0);
                            final int n = 1;
                            final ConstraintWidget.DimensionBehaviour dimensionBehaviour2 = constraintWidget.getDimensionBehaviour(1);
                            final boolean b = dimensionBehaviour == ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT && constraintWidget.mMatchConstraintDefaultWidth != 1 && dimensionBehaviour2 == ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT && constraintWidget.mMatchConstraintDefaultHeight != 1;
                            int n5 = 0;
                            Label_0340: {
                                int n2 = 0;
                                Label_0338: {
                                    if ((n2 = (b ? 1 : 0)) == 0) {
                                        n2 = (b ? 1 : 0);
                                        if (constraintWidgetContainer.optimizeFor(1)) {
                                            n2 = (b ? 1 : 0);
                                            if (!(constraintWidget instanceof VirtualLayout)) {
                                                int n3 = b ? 1 : 0;
                                                if (dimensionBehaviour == ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT) {
                                                    n3 = (b ? 1 : 0);
                                                    if (constraintWidget.mMatchConstraintDefaultWidth == 0) {
                                                        n3 = (b ? 1 : 0);
                                                        if (dimensionBehaviour2 != ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT) {
                                                            n3 = (b ? 1 : 0);
                                                            if (!constraintWidget.isInHorizontalChain()) {
                                                                n3 = 1;
                                                            }
                                                        }
                                                    }
                                                }
                                                int n4 = n3;
                                                if (dimensionBehaviour2 == ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT) {
                                                    n4 = n3;
                                                    if (constraintWidget.mMatchConstraintDefaultHeight == 0) {
                                                        n4 = n3;
                                                        if (dimensionBehaviour != ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT) {
                                                            n4 = n3;
                                                            if (!constraintWidget.isInHorizontalChain()) {
                                                                n4 = 1;
                                                            }
                                                        }
                                                    }
                                                }
                                                if (dimensionBehaviour != ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT) {
                                                    n2 = n4;
                                                    if (dimensionBehaviour2 != ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT) {
                                                        break Label_0338;
                                                    }
                                                }
                                                n2 = n4;
                                                if (constraintWidget.mDimensionRatio > 0.0f) {
                                                    n5 = n;
                                                    break Label_0340;
                                                }
                                            }
                                        }
                                    }
                                }
                                n5 = n2;
                            }
                            if (n5 == 0) {
                                this.measure(measurer, constraintWidget, Measure.SELF_DIMENSIONS);
                                if (constraintWidgetContainer.mMetrics != null) {
                                    final Metrics mMetrics = constraintWidgetContainer.mMetrics;
                                    ++mMetrics.measuredWidgets;
                                }
                            }
                        }
                    }
                }
            }
        }
        measurer.didMeasures();
    }
    
    private void solveLinearSystem(final ConstraintWidgetContainer constraintWidgetContainer, final String s, final int pass, final int width, final int height) {
        final int minWidth = constraintWidgetContainer.getMinWidth();
        final int minHeight = constraintWidgetContainer.getMinHeight();
        constraintWidgetContainer.setMinWidth(0);
        constraintWidgetContainer.setMinHeight(0);
        constraintWidgetContainer.setWidth(width);
        constraintWidgetContainer.setHeight(height);
        constraintWidgetContainer.setMinWidth(minWidth);
        constraintWidgetContainer.setMinHeight(minHeight);
        this.constraintWidgetContainer.setPass(pass);
        this.constraintWidgetContainer.layout();
    }
    
    public long solverMeasure(final ConstraintWidgetContainer constraintWidgetContainer, int n, int n2, int optimizationLevel, int width, int b, int i, int b2, int n3, int size) {
        final Measurer measurer = constraintWidgetContainer.getMeasurer();
        size = constraintWidgetContainer.mChildren.size();
        final int width2 = constraintWidgetContainer.getWidth();
        final int height = constraintWidgetContainer.getHeight();
        final boolean enabled = Optimizer.enabled(n, 128);
        if (!enabled && !Optimizer.enabled(n, 64)) {
            n = 0;
        }
        else {
            n = 1;
        }
        optimizationLevel = n;
        Label_0231: {
            if (n != 0) {
                n2 = 0;
                while (true) {
                    optimizationLevel = n;
                    if (n2 >= size) {
                        break Label_0231;
                    }
                    final ConstraintWidget constraintWidget = constraintWidgetContainer.mChildren.get(n2);
                    if (constraintWidget.getHorizontalDimensionBehaviour() == ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT) {
                        optimizationLevel = 1;
                    }
                    else {
                        optimizationLevel = 0;
                    }
                    if (constraintWidget.getVerticalDimensionBehaviour() == ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT) {
                        n3 = 1;
                    }
                    else {
                        n3 = 0;
                    }
                    if (optimizationLevel != 0 && n3 != 0 && constraintWidget.getDimensionRatio() > 0.0f) {
                        optimizationLevel = 1;
                    }
                    else {
                        optimizationLevel = 0;
                    }
                    if (constraintWidget.isInHorizontalChain() && optimizationLevel != 0) {
                        break;
                    }
                    if (constraintWidget.isInVerticalChain() && optimizationLevel != 0) {
                        break;
                    }
                    if (constraintWidget instanceof VirtualLayout) {
                        break;
                    }
                    if (constraintWidget.isInHorizontalChain()) {
                        break;
                    }
                    if (constraintWidget.isInVerticalChain()) {
                        break;
                    }
                    ++n2;
                }
                optimizationLevel = 0;
            }
        }
        if (optimizationLevel != 0 && LinearSystem.sMetrics != null) {
            final Metrics sMetrics = LinearSystem.sMetrics;
            ++sMetrics.measures;
        }
        if ((width == 1073741824 && i == 1073741824) || enabled) {
            n = 1;
        }
        else {
            n = 0;
        }
        final int n4 = optimizationLevel & n;
        boolean b4;
        if (n4 != 0) {
            n = Math.min(constraintWidgetContainer.getMaxWidth(), b);
            n2 = Math.min(constraintWidgetContainer.getMaxHeight(), b2);
            if (width == 1073741824 && constraintWidgetContainer.getWidth() != n) {
                constraintWidgetContainer.setWidth(n);
                constraintWidgetContainer.invalidateGraph();
            }
            if (i == 1073741824 && constraintWidgetContainer.getHeight() != n2) {
                constraintWidgetContainer.setHeight(n2);
                constraintWidgetContainer.invalidateGraph();
            }
            boolean b3;
            if (width == 1073741824 && i == 1073741824) {
                b3 = constraintWidgetContainer.directMeasure(enabled);
                n = 2;
            }
            else {
                b3 = constraintWidgetContainer.directMeasureSetup(enabled);
                if (width == 1073741824) {
                    b3 &= constraintWidgetContainer.directMeasureWithOrientation(enabled, 0);
                    n = 1;
                }
                else {
                    n = 0;
                }
                if (i == 1073741824) {
                    b3 &= constraintWidgetContainer.directMeasureWithOrientation(enabled, 1);
                    ++n;
                }
            }
            b4 = b3;
            n2 = n;
            if (b3) {
                constraintWidgetContainer.updateFromRuns(width == 1073741824, i == 1073741824);
                b4 = b3;
                n2 = n;
            }
        }
        else {
            b4 = false;
            n2 = 0;
        }
        if (!b4 || n2 != 2) {
            optimizationLevel = constraintWidgetContainer.getOptimizationLevel();
            if (size > 0) {
                this.measureChildren(constraintWidgetContainer);
            }
            this.updateHierarchy(constraintWidgetContainer);
            b2 = this.mVariableDimensionsWidgets.size();
            if (size > 0) {
                this.solveLinearSystem(constraintWidgetContainer, "First pass", 0, width2, height);
            }
            if (b2 > 0) {
                if (constraintWidgetContainer.getHorizontalDimensionBehaviour() == ConstraintWidget.DimensionBehaviour.WRAP_CONTENT) {
                    n3 = 1;
                }
                else {
                    n3 = 0;
                }
                if (constraintWidgetContainer.getVerticalDimensionBehaviour() == ConstraintWidget.DimensionBehaviour.WRAP_CONTENT) {
                    size = 1;
                }
                else {
                    size = 0;
                }
                n2 = Math.max(constraintWidgetContainer.getWidth(), this.constraintWidgetContainer.getMinWidth());
                n = Math.max(constraintWidgetContainer.getHeight(), this.constraintWidgetContainer.getMinHeight());
                i = 0;
                width = 0;
                while (i < b2) {
                    final ConstraintWidget constraintWidget2 = this.mVariableDimensionsWidgets.get(i);
                    if (!(constraintWidget2 instanceof VirtualLayout)) {
                        b = width;
                    }
                    else {
                        final int width3 = constraintWidget2.getWidth();
                        final int height2 = constraintWidget2.getHeight();
                        b = ((this.measure(measurer, constraintWidget2, Measure.TRY_GIVEN_DIMENSIONS) ? 1 : 0) | width);
                        if (constraintWidgetContainer.mMetrics != null) {
                            final Metrics mMetrics = constraintWidgetContainer.mMetrics;
                            ++mMetrics.measuredMatchWidgets;
                        }
                        width = constraintWidget2.getWidth();
                        final int height3 = constraintWidget2.getHeight();
                        if (width != width3) {
                            constraintWidget2.setWidth(width);
                            width = n2;
                            if (n3 != 0 && constraintWidget2.getRight() > (width = n2)) {
                                width = Math.max(n2, constraintWidget2.getRight() + constraintWidget2.getAnchor(ConstraintAnchor.Type.RIGHT).getMargin());
                            }
                            b = 1;
                            n2 = width;
                        }
                        width = n;
                        if (height3 != height2) {
                            constraintWidget2.setHeight(height3);
                            width = n;
                            if (size != 0 && constraintWidget2.getBottom() > (width = n)) {
                                width = Math.max(n, constraintWidget2.getBottom() + constraintWidget2.getAnchor(ConstraintAnchor.Type.BOTTOM).getMargin());
                            }
                            b = 1;
                        }
                        b |= (((VirtualLayout)constraintWidget2).needSolverPass() ? 1 : 0);
                        n = width;
                    }
                    ++i;
                    width = b;
                }
                int j = 0;
                i = width;
                width = b2;
                b = n4;
                while (j < 2) {
                    int n5;
                    for (int k = 0; k < width; ++k, n2 = n5, i = b2) {
                        final ConstraintWidget constraintWidget3 = this.mVariableDimensionsWidgets.get(k);
                        if (!(constraintWidget3 instanceof Helper) || constraintWidget3 instanceof VirtualLayout) {
                            if (!(constraintWidget3 instanceof Guideline)) {
                                if (constraintWidget3.getVisibility() != 8) {
                                    if (b == 0 || !constraintWidget3.horizontalRun.dimension.resolved || !constraintWidget3.verticalRun.dimension.resolved) {
                                        if (!(constraintWidget3 instanceof VirtualLayout)) {
                                            final int width4 = constraintWidget3.getWidth();
                                            final int height4 = constraintWidget3.getHeight();
                                            final int baselineDistance = constraintWidget3.getBaselineDistance();
                                            b2 = Measure.TRY_GIVEN_DIMENSIONS;
                                            if (j == 1) {
                                                b2 = Measure.USE_GIVEN_DIMENSIONS;
                                            }
                                            b2 = ((this.measure(measurer, constraintWidget3, b2) ? 1 : 0) | i);
                                            if (constraintWidgetContainer.mMetrics != null) {
                                                final Metrics mMetrics2 = constraintWidgetContainer.mMetrics;
                                                ++mMetrics2.measuredMatchWidgets;
                                            }
                                            final int width5 = constraintWidget3.getWidth();
                                            final int height5 = constraintWidget3.getHeight();
                                            i = n2;
                                            if (width5 != width4) {
                                                constraintWidget3.setWidth(width5);
                                                i = n2;
                                                if (n3 != 0 && constraintWidget3.getRight() > (i = n2)) {
                                                    i = Math.max(n2, constraintWidget3.getRight() + constraintWidget3.getAnchor(ConstraintAnchor.Type.RIGHT).getMargin());
                                                }
                                                b2 = 1;
                                            }
                                            n2 = n;
                                            if (height5 != height4) {
                                                constraintWidget3.setHeight(height5);
                                                n2 = n;
                                                if (size != 0 && constraintWidget3.getBottom() > (n2 = n)) {
                                                    n2 = Math.max(n, constraintWidget3.getBottom() + constraintWidget3.getAnchor(ConstraintAnchor.Type.BOTTOM).getMargin());
                                                }
                                                b2 = 1;
                                            }
                                            if (constraintWidget3.hasBaseline() && baselineDistance != constraintWidget3.getBaselineDistance()) {
                                                b2 = 1;
                                                n5 = i;
                                                n = n2;
                                                continue;
                                            }
                                            n = n2;
                                            n5 = i;
                                            continue;
                                        }
                                    }
                                }
                            }
                        }
                        n5 = n2;
                        b2 = i;
                    }
                    if (i == 0) {
                        break;
                    }
                    ++j;
                    this.solveLinearSystem(constraintWidgetContainer, "intermediate pass", j, width2, height);
                    i = 0;
                }
                n = optimizationLevel;
            }
            else {
                n = optimizationLevel;
            }
            constraintWidgetContainer.setOptimizationLevel(n);
        }
        return 0L;
    }
    
    public void updateHierarchy(final ConstraintWidgetContainer constraintWidgetContainer) {
        this.mVariableDimensionsWidgets.clear();
        for (int size = constraintWidgetContainer.mChildren.size(), i = 0; i < size; ++i) {
            final ConstraintWidget e = constraintWidgetContainer.mChildren.get(i);
            if (e.getHorizontalDimensionBehaviour() == ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT || e.getVerticalDimensionBehaviour() == ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT) {
                this.mVariableDimensionsWidgets.add(e);
            }
        }
        constraintWidgetContainer.invalidateGraph();
    }
    
    public static class Measure
    {
        public static int SELF_DIMENSIONS = 0;
        public static int TRY_GIVEN_DIMENSIONS = 1;
        public static int USE_GIVEN_DIMENSIONS = 2;
        public ConstraintWidget.DimensionBehaviour horizontalBehavior;
        public int horizontalDimension;
        public int measureStrategy;
        public int measuredBaseline;
        public boolean measuredHasBaseline;
        public int measuredHeight;
        public boolean measuredNeedsSolverPass;
        public int measuredWidth;
        public ConstraintWidget.DimensionBehaviour verticalBehavior;
        public int verticalDimension;
    }
    
    public interface Measurer
    {
        void didMeasures();
        
        void measure(final ConstraintWidget p0, final Measure p1);
    }
}

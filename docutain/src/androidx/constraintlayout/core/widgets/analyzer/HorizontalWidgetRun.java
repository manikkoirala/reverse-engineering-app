// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.core.widgets.analyzer;

import androidx.constraintlayout.core.widgets.ConstraintAnchor;
import androidx.constraintlayout.core.widgets.Helper;
import androidx.constraintlayout.core.widgets.ConstraintWidget;

public class HorizontalWidgetRun extends WidgetRun
{
    private static int[] tempDimensions;
    
    static {
        HorizontalWidgetRun.tempDimensions = new int[2];
    }
    
    public HorizontalWidgetRun(final ConstraintWidget constraintWidget) {
        super(constraintWidget);
        this.start.type = DependencyNode.Type.LEFT;
        this.end.type = DependencyNode.Type.RIGHT;
        this.orientation = 0;
    }
    
    private void computeInsetRatio(final int[] array, int n, int n2, int n3, int n4, final float n5, final int n6) {
        n = n2 - n;
        n2 = n4 - n3;
        if (n6 != -1) {
            if (n6 != 0) {
                if (n6 == 1) {
                    n2 = (int)(n * n5 + 0.5f);
                    array[0] = n;
                    array[1] = n2;
                }
            }
            else {
                array[0] = (int)(n2 * n5 + 0.5f);
                array[1] = n2;
            }
        }
        else {
            n4 = (int)(n2 * n5 + 0.5f);
            n3 = (int)(n / n5 + 0.5f);
            if (n4 <= n) {
                array[0] = n4;
                array[1] = n2;
            }
            else if (n3 <= n2) {
                array[0] = n;
                array[1] = n3;
            }
        }
    }
    
    @Override
    void apply() {
        if (this.widget.measured) {
            this.dimension.resolve(this.widget.getWidth());
        }
        if (!this.dimension.resolved) {
            super.dimensionBehavior = this.widget.getHorizontalDimensionBehaviour();
            if (super.dimensionBehavior != ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT) {
                if (this.dimensionBehavior == ConstraintWidget.DimensionBehaviour.MATCH_PARENT) {
                    final ConstraintWidget parent = this.widget.getParent();
                    if (parent != null && (parent.getHorizontalDimensionBehaviour() == ConstraintWidget.DimensionBehaviour.FIXED || parent.getHorizontalDimensionBehaviour() == ConstraintWidget.DimensionBehaviour.MATCH_PARENT)) {
                        final int width = parent.getWidth();
                        final int margin = this.widget.mLeft.getMargin();
                        final int margin2 = this.widget.mRight.getMargin();
                        this.addTarget(this.start, parent.horizontalRun.start, this.widget.mLeft.getMargin());
                        this.addTarget(this.end, parent.horizontalRun.end, -this.widget.mRight.getMargin());
                        this.dimension.resolve(width - margin - margin2);
                        return;
                    }
                }
                if (this.dimensionBehavior == ConstraintWidget.DimensionBehaviour.FIXED) {
                    this.dimension.resolve(this.widget.getWidth());
                }
            }
        }
        else if (this.dimensionBehavior == ConstraintWidget.DimensionBehaviour.MATCH_PARENT) {
            final ConstraintWidget parent2 = this.widget.getParent();
            if (parent2 != null && (parent2.getHorizontalDimensionBehaviour() == ConstraintWidget.DimensionBehaviour.FIXED || parent2.getHorizontalDimensionBehaviour() == ConstraintWidget.DimensionBehaviour.MATCH_PARENT)) {
                this.addTarget(this.start, parent2.horizontalRun.start, this.widget.mLeft.getMargin());
                this.addTarget(this.end, parent2.horizontalRun.end, -this.widget.mRight.getMargin());
                return;
            }
        }
        if (this.dimension.resolved && this.widget.measured) {
            if (this.widget.mListAnchors[0].mTarget != null && this.widget.mListAnchors[1].mTarget != null) {
                if (this.widget.isInHorizontalChain()) {
                    this.start.margin = this.widget.mListAnchors[0].getMargin();
                    this.end.margin = -this.widget.mListAnchors[1].getMargin();
                }
                else {
                    final DependencyNode target = this.getTarget(this.widget.mListAnchors[0]);
                    if (target != null) {
                        this.addTarget(this.start, target, this.widget.mListAnchors[0].getMargin());
                    }
                    final DependencyNode target2 = this.getTarget(this.widget.mListAnchors[1]);
                    if (target2 != null) {
                        this.addTarget(this.end, target2, -this.widget.mListAnchors[1].getMargin());
                    }
                    this.start.delegateToWidgetRun = true;
                    this.end.delegateToWidgetRun = true;
                }
            }
            else if (this.widget.mListAnchors[0].mTarget != null) {
                final DependencyNode target3 = this.getTarget(this.widget.mListAnchors[0]);
                if (target3 != null) {
                    this.addTarget(this.start, target3, this.widget.mListAnchors[0].getMargin());
                    this.addTarget(this.end, this.start, this.dimension.value);
                }
            }
            else if (this.widget.mListAnchors[1].mTarget != null) {
                final DependencyNode target4 = this.getTarget(this.widget.mListAnchors[1]);
                if (target4 != null) {
                    this.addTarget(this.end, target4, -this.widget.mListAnchors[1].getMargin());
                    this.addTarget(this.start, this.end, -this.dimension.value);
                }
            }
            else if (!(this.widget instanceof Helper) && this.widget.getParent() != null && this.widget.getAnchor(ConstraintAnchor.Type.CENTER).mTarget == null) {
                this.addTarget(this.start, this.widget.getParent().horizontalRun.start, this.widget.getX());
                this.addTarget(this.end, this.start, this.dimension.value);
            }
        }
        else {
            if (this.dimensionBehavior == ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT) {
                final int mMatchConstraintDefaultWidth = this.widget.mMatchConstraintDefaultWidth;
                if (mMatchConstraintDefaultWidth != 2) {
                    if (mMatchConstraintDefaultWidth == 3) {
                        if (this.widget.mMatchConstraintDefaultHeight == 3) {
                            this.start.updateDelegate = this;
                            this.end.updateDelegate = this;
                            this.widget.verticalRun.start.updateDelegate = this;
                            this.widget.verticalRun.end.updateDelegate = this;
                            this.dimension.updateDelegate = this;
                            if (this.widget.isInVerticalChain()) {
                                this.dimension.targets.add(this.widget.verticalRun.dimension);
                                this.widget.verticalRun.dimension.dependencies.add(this.dimension);
                                this.widget.verticalRun.dimension.updateDelegate = this;
                                this.dimension.targets.add(this.widget.verticalRun.start);
                                this.dimension.targets.add(this.widget.verticalRun.end);
                                this.widget.verticalRun.start.dependencies.add(this.dimension);
                                this.widget.verticalRun.end.dependencies.add(this.dimension);
                            }
                            else if (this.widget.isInHorizontalChain()) {
                                this.widget.verticalRun.dimension.targets.add(this.dimension);
                                this.dimension.dependencies.add(this.widget.verticalRun.dimension);
                            }
                            else {
                                this.widget.verticalRun.dimension.targets.add(this.dimension);
                            }
                        }
                        else {
                            final DimensionDependency dimension = this.widget.verticalRun.dimension;
                            this.dimension.targets.add(dimension);
                            dimension.dependencies.add(this.dimension);
                            this.widget.verticalRun.start.dependencies.add(this.dimension);
                            this.widget.verticalRun.end.dependencies.add(this.dimension);
                            this.dimension.delegateToWidgetRun = true;
                            this.dimension.dependencies.add(this.start);
                            this.dimension.dependencies.add(this.end);
                            this.start.targets.add(this.dimension);
                            this.end.targets.add(this.dimension);
                        }
                    }
                }
                else {
                    final ConstraintWidget parent3 = this.widget.getParent();
                    if (parent3 != null) {
                        final DimensionDependency dimension2 = parent3.verticalRun.dimension;
                        this.dimension.targets.add(dimension2);
                        dimension2.dependencies.add(this.dimension);
                        this.dimension.delegateToWidgetRun = true;
                        this.dimension.dependencies.add(this.start);
                        this.dimension.dependencies.add(this.end);
                    }
                }
            }
            if (this.widget.mListAnchors[0].mTarget != null && this.widget.mListAnchors[1].mTarget != null) {
                if (this.widget.isInHorizontalChain()) {
                    this.start.margin = this.widget.mListAnchors[0].getMargin();
                    this.end.margin = -this.widget.mListAnchors[1].getMargin();
                }
                else {
                    final DependencyNode target5 = this.getTarget(this.widget.mListAnchors[0]);
                    final DependencyNode target6 = this.getTarget(this.widget.mListAnchors[1]);
                    if (target5 != null) {
                        target5.addDependency(this);
                    }
                    if (target6 != null) {
                        target6.addDependency(this);
                    }
                    this.mRunType = RunType.CENTER;
                }
            }
            else if (this.widget.mListAnchors[0].mTarget != null) {
                final DependencyNode target7 = this.getTarget(this.widget.mListAnchors[0]);
                if (target7 != null) {
                    this.addTarget(this.start, target7, this.widget.mListAnchors[0].getMargin());
                    this.addTarget(this.end, this.start, 1, this.dimension);
                }
            }
            else if (this.widget.mListAnchors[1].mTarget != null) {
                final DependencyNode target8 = this.getTarget(this.widget.mListAnchors[1]);
                if (target8 != null) {
                    this.addTarget(this.end, target8, -this.widget.mListAnchors[1].getMargin());
                    this.addTarget(this.start, this.end, -1, this.dimension);
                }
            }
            else if (!(this.widget instanceof Helper) && this.widget.getParent() != null) {
                this.addTarget(this.start, this.widget.getParent().horizontalRun.start, this.widget.getX());
                this.addTarget(this.end, this.start, 1, this.dimension);
            }
        }
    }
    
    public void applyToWidget() {
        if (this.start.resolved) {
            this.widget.setX(this.start.value);
        }
    }
    
    @Override
    void clear() {
        this.runGroup = null;
        this.start.clear();
        this.end.clear();
        this.dimension.clear();
        this.resolved = false;
    }
    
    @Override
    void reset() {
        this.resolved = false;
        this.start.clear();
        this.start.resolved = false;
        this.end.clear();
        this.end.resolved = false;
        this.dimension.resolved = false;
    }
    
    @Override
    boolean supportsWrapComputation() {
        return super.dimensionBehavior != ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT || super.widget.mMatchConstraintDefaultWidth == 0;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("HorizontalRun ");
        sb.append(this.widget.getDebugName());
        return sb.toString();
    }
    
    @Override
    public void update(final Dependency dependency) {
        final int n = HorizontalWidgetRun$1.$SwitchMap$androidx$constraintlayout$core$widgets$analyzer$WidgetRun$RunType[this.mRunType.ordinal()];
        if (n != 1) {
            if (n != 2) {
                if (n == 3) {
                    this.updateRunCenter(dependency, this.widget.mLeft, this.widget.mRight, 0);
                    return;
                }
            }
            else {
                this.updateRunEnd(dependency);
            }
        }
        else {
            this.updateRunStart(dependency);
        }
        Label_1593: {
            if (!this.dimension.resolved && this.dimensionBehavior == ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT) {
                final int mMatchConstraintDefaultWidth = this.widget.mMatchConstraintDefaultWidth;
                if (mMatchConstraintDefaultWidth != 2) {
                    if (mMatchConstraintDefaultWidth == 3) {
                        if (this.widget.mMatchConstraintDefaultHeight != 0 && this.widget.mMatchConstraintDefaultHeight != 3) {
                            final int dimensionRatioSide = this.widget.getDimensionRatioSide();
                            int n3 = 0;
                            Label_0254: {
                                float n2 = 0.0f;
                                Label_0247: {
                                    float n4;
                                    float n5;
                                    if (dimensionRatioSide != -1) {
                                        if (dimensionRatioSide == 0) {
                                            n2 = this.widget.verticalRun.dimension.value / this.widget.getDimensionRatio();
                                            break Label_0247;
                                        }
                                        if (dimensionRatioSide != 1) {
                                            n3 = 0;
                                            break Label_0254;
                                        }
                                        n4 = (float)this.widget.verticalRun.dimension.value;
                                        n5 = this.widget.getDimensionRatio();
                                    }
                                    else {
                                        n4 = (float)this.widget.verticalRun.dimension.value;
                                        n5 = this.widget.getDimensionRatio();
                                    }
                                    n2 = n4 * n5;
                                }
                                n3 = (int)(n2 + 0.5f);
                            }
                            this.dimension.resolve(n3);
                        }
                        else {
                            final DependencyNode start = this.widget.verticalRun.start;
                            final DependencyNode end = this.widget.verticalRun.end;
                            final boolean b = this.widget.mLeft.mTarget != null;
                            final boolean b2 = this.widget.mTop.mTarget != null;
                            final boolean b3 = this.widget.mRight.mTarget != null;
                            final boolean b4 = this.widget.mBottom.mTarget != null;
                            final int dimensionRatioSide2 = this.widget.getDimensionRatioSide();
                            if (b && b2 && b3 && b4) {
                                final float dimensionRatio = this.widget.getDimensionRatio();
                                if (start.resolved && end.resolved) {
                                    if (this.start.readyToSolve) {
                                        if (this.end.readyToSolve) {
                                            this.computeInsetRatio(HorizontalWidgetRun.tempDimensions, this.start.targets.get(0).value + this.start.margin, this.end.targets.get(0).value - this.end.margin, start.value + start.margin, end.value - end.margin, dimensionRatio, dimensionRatioSide2);
                                            this.dimension.resolve(HorizontalWidgetRun.tempDimensions[0]);
                                            this.widget.verticalRun.dimension.resolve(HorizontalWidgetRun.tempDimensions[1]);
                                        }
                                    }
                                    return;
                                }
                                if (this.start.resolved && this.end.resolved) {
                                    if (!start.readyToSolve || !end.readyToSolve) {
                                        return;
                                    }
                                    this.computeInsetRatio(HorizontalWidgetRun.tempDimensions, this.start.value + this.start.margin, this.end.value - this.end.margin, start.targets.get(0).value + start.margin, end.targets.get(0).value - end.margin, dimensionRatio, dimensionRatioSide2);
                                    this.dimension.resolve(HorizontalWidgetRun.tempDimensions[0]);
                                    this.widget.verticalRun.dimension.resolve(HorizontalWidgetRun.tempDimensions[1]);
                                }
                                if (!this.start.readyToSolve || !this.end.readyToSolve || !start.readyToSolve || !end.readyToSolve) {
                                    return;
                                }
                                this.computeInsetRatio(HorizontalWidgetRun.tempDimensions, this.start.targets.get(0).value + this.start.margin, this.end.targets.get(0).value - this.end.margin, start.targets.get(0).value + start.margin, end.targets.get(0).value - end.margin, dimensionRatio, dimensionRatioSide2);
                                this.dimension.resolve(HorizontalWidgetRun.tempDimensions[0]);
                                this.widget.verticalRun.dimension.resolve(HorizontalWidgetRun.tempDimensions[1]);
                            }
                            else if (b && b3) {
                                if (!this.start.readyToSolve || !this.end.readyToSolve) {
                                    return;
                                }
                                final float dimensionRatio2 = this.widget.getDimensionRatio();
                                final int n6 = this.start.targets.get(0).value + this.start.margin;
                                final int n7 = this.end.targets.get(0).value - this.end.margin;
                                if (dimensionRatioSide2 != -1 && dimensionRatioSide2 != 0) {
                                    if (dimensionRatioSide2 == 1) {
                                        int limitedDimension = this.getLimitedDimension(n7 - n6, 0);
                                        final int n8 = (int)(limitedDimension / dimensionRatio2 + 0.5f);
                                        final int limitedDimension2 = this.getLimitedDimension(n8, 1);
                                        if (n8 != limitedDimension2) {
                                            limitedDimension = (int)(limitedDimension2 * dimensionRatio2 + 0.5f);
                                        }
                                        this.dimension.resolve(limitedDimension);
                                        this.widget.verticalRun.dimension.resolve(limitedDimension2);
                                    }
                                }
                                else {
                                    int limitedDimension3 = this.getLimitedDimension(n7 - n6, 0);
                                    final int n9 = (int)(limitedDimension3 * dimensionRatio2 + 0.5f);
                                    final int limitedDimension4 = this.getLimitedDimension(n9, 1);
                                    if (n9 != limitedDimension4) {
                                        limitedDimension3 = (int)(limitedDimension4 / dimensionRatio2 + 0.5f);
                                    }
                                    this.dimension.resolve(limitedDimension3);
                                    this.widget.verticalRun.dimension.resolve(limitedDimension4);
                                }
                            }
                            else if (b2 && b4) {
                                if (!start.readyToSolve || !end.readyToSolve) {
                                    return;
                                }
                                final float dimensionRatio3 = this.widget.getDimensionRatio();
                                final int n10 = start.targets.get(0).value + start.margin;
                                final int n11 = end.targets.get(0).value - end.margin;
                                if (dimensionRatioSide2 != -1) {
                                    if (dimensionRatioSide2 == 0) {
                                        int limitedDimension5 = this.getLimitedDimension(n11 - n10, 1);
                                        final int n12 = (int)(limitedDimension5 * dimensionRatio3 + 0.5f);
                                        final int limitedDimension6 = this.getLimitedDimension(n12, 0);
                                        if (n12 != limitedDimension6) {
                                            limitedDimension5 = (int)(limitedDimension6 / dimensionRatio3 + 0.5f);
                                        }
                                        this.dimension.resolve(limitedDimension6);
                                        this.widget.verticalRun.dimension.resolve(limitedDimension5);
                                        break Label_1593;
                                    }
                                    if (dimensionRatioSide2 != 1) {
                                        break Label_1593;
                                    }
                                }
                                int limitedDimension7 = this.getLimitedDimension(n11 - n10, 1);
                                final int n13 = (int)(limitedDimension7 / dimensionRatio3 + 0.5f);
                                final int limitedDimension8 = this.getLimitedDimension(n13, 0);
                                if (n13 != limitedDimension8) {
                                    limitedDimension7 = (int)(limitedDimension8 * dimensionRatio3 + 0.5f);
                                }
                                this.dimension.resolve(limitedDimension8);
                                this.widget.verticalRun.dimension.resolve(limitedDimension7);
                            }
                        }
                    }
                }
                else {
                    final ConstraintWidget parent = this.widget.getParent();
                    if (parent != null && parent.horizontalRun.dimension.resolved) {
                        this.dimension.resolve((int)(parent.horizontalRun.dimension.value * this.widget.mMatchConstraintPercentWidth + 0.5f));
                    }
                }
            }
        }
        if (this.start.readyToSolve) {
            if (this.end.readyToSolve) {
                if (this.start.resolved && this.end.resolved && this.dimension.resolved) {
                    return;
                }
                if (!this.dimension.resolved && this.dimensionBehavior == ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT && this.widget.mMatchConstraintDefaultWidth == 0 && !this.widget.isInHorizontalChain()) {
                    final DependencyNode dependencyNode = this.start.targets.get(0);
                    final DependencyNode dependencyNode2 = this.end.targets.get(0);
                    final int n14 = dependencyNode.value + this.start.margin;
                    final int n15 = dependencyNode2.value + this.end.margin;
                    this.start.resolve(n14);
                    this.end.resolve(n15);
                    this.dimension.resolve(n15 - n14);
                    return;
                }
                if (!this.dimension.resolved && this.dimensionBehavior == ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT && this.matchConstraintsType == 1 && this.start.targets.size() > 0 && this.end.targets.size() > 0) {
                    final int min = Math.min(this.end.targets.get(0).value + this.end.margin - (this.start.targets.get(0).value + this.start.margin), this.dimension.wrapValue);
                    final int mMatchConstraintMaxWidth = this.widget.mMatchConstraintMaxWidth;
                    int b5 = Math.max(this.widget.mMatchConstraintMinWidth, min);
                    if (mMatchConstraintMaxWidth > 0) {
                        b5 = Math.min(mMatchConstraintMaxWidth, b5);
                    }
                    this.dimension.resolve(b5);
                }
                if (!this.dimension.resolved) {
                    return;
                }
                final DependencyNode dependencyNode3 = this.start.targets.get(0);
                final DependencyNode dependencyNode4 = this.end.targets.get(0);
                int value = dependencyNode3.value + this.start.margin;
                int value2 = dependencyNode4.value + this.end.margin;
                float horizontalBiasPercent = this.widget.getHorizontalBiasPercent();
                if (dependencyNode3 == dependencyNode4) {
                    value = dependencyNode3.value;
                    value2 = dependencyNode4.value;
                    horizontalBiasPercent = 0.5f;
                }
                this.start.resolve((int)(value + 0.5f + (value2 - value - this.dimension.value) * horizontalBiasPercent));
                this.end.resolve(this.start.value + this.dimension.value);
            }
        }
    }
}

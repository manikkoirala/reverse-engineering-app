// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.core.widgets.analyzer;

import java.lang.ref.WeakReference;
import java.util.Iterator;
import androidx.constraintlayout.core.widgets.Chain;
import androidx.constraintlayout.core.widgets.ConstraintWidgetContainer;
import androidx.constraintlayout.core.LinearSystem;
import androidx.constraintlayout.core.widgets.ConstraintWidget;
import java.util.ArrayList;

public class WidgetGroup
{
    private static final boolean DEBUG = false;
    static int count;
    boolean authoritative;
    int id;
    private int moveTo;
    int orientation;
    ArrayList<MeasureResult> results;
    ArrayList<ConstraintWidget> widgets;
    
    public WidgetGroup(final int orientation) {
        this.widgets = new ArrayList<ConstraintWidget>();
        this.authoritative = false;
        this.results = null;
        this.moveTo = -1;
        final int count = WidgetGroup.count;
        WidgetGroup.count = count + 1;
        this.id = count;
        this.orientation = orientation;
    }
    
    private boolean contains(final ConstraintWidget o) {
        return this.widgets.contains(o);
    }
    
    private String getOrientationString() {
        final int orientation = this.orientation;
        if (orientation == 0) {
            return "Horizontal";
        }
        if (orientation == 1) {
            return "Vertical";
        }
        if (orientation == 2) {
            return "Both";
        }
        return "Unknown";
    }
    
    private int measureWrap(int n, final ConstraintWidget constraintWidget) {
        final ConstraintWidget.DimensionBehaviour dimensionBehaviour = constraintWidget.getDimensionBehaviour(n);
        if (dimensionBehaviour != ConstraintWidget.DimensionBehaviour.WRAP_CONTENT && dimensionBehaviour != ConstraintWidget.DimensionBehaviour.MATCH_PARENT && dimensionBehaviour != ConstraintWidget.DimensionBehaviour.FIXED) {
            return -1;
        }
        if (n == 0) {
            n = constraintWidget.getWidth();
        }
        else {
            n = constraintWidget.getHeight();
        }
        return n;
    }
    
    private int solverMeasure(final LinearSystem linearSystem, final ArrayList<ConstraintWidget> list, int n) {
        final int n2 = 0;
        final ConstraintWidgetContainer constraintWidgetContainer = (ConstraintWidgetContainer)list.get(0).getParent();
        linearSystem.reset();
        constraintWidgetContainer.addToSolver(linearSystem, false);
        for (int i = 0; i < list.size(); ++i) {
            ((ConstraintWidget)list.get(i)).addToSolver(linearSystem, false);
        }
        if (n == 0 && constraintWidgetContainer.mHorizontalChainsSize > 0) {
            Chain.applyChainConstraints(constraintWidgetContainer, linearSystem, list, 0);
        }
        if (n == 1 && constraintWidgetContainer.mVerticalChainsSize > 0) {
            Chain.applyChainConstraints(constraintWidgetContainer, linearSystem, list, 1);
        }
        try {
            linearSystem.minimize();
        }
        catch (final Exception ex) {
            ex.printStackTrace();
        }
        this.results = new ArrayList<MeasureResult>();
        for (int j = n2; j < list.size(); ++j) {
            this.results.add(new MeasureResult((ConstraintWidget)list.get(j), linearSystem, n));
        }
        int n3;
        if (n == 0) {
            n3 = linearSystem.getObjectVariableValue(constraintWidgetContainer.mLeft);
            n = linearSystem.getObjectVariableValue(constraintWidgetContainer.mRight);
            linearSystem.reset();
        }
        else {
            n3 = linearSystem.getObjectVariableValue(constraintWidgetContainer.mTop);
            n = linearSystem.getObjectVariableValue(constraintWidgetContainer.mBottom);
            linearSystem.reset();
        }
        return n - n3;
    }
    
    public boolean add(final ConstraintWidget constraintWidget) {
        if (this.widgets.contains(constraintWidget)) {
            return false;
        }
        this.widgets.add(constraintWidget);
        return true;
    }
    
    public void apply() {
        if (this.results == null) {
            return;
        }
        if (!this.authoritative) {
            return;
        }
        for (int i = 0; i < this.results.size(); ++i) {
            this.results.get(i).apply();
        }
    }
    
    public void cleanup(final ArrayList<WidgetGroup> list) {
        final int size = this.widgets.size();
        if (this.moveTo != -1 && size > 0) {
            for (int i = 0; i < list.size(); ++i) {
                final WidgetGroup widgetGroup = list.get(i);
                if (this.moveTo == widgetGroup.id) {
                    this.moveTo(this.orientation, widgetGroup);
                }
            }
        }
        if (size == 0) {
            list.remove(this);
        }
    }
    
    public void clear() {
        this.widgets.clear();
    }
    
    public int getId() {
        return this.id;
    }
    
    public int getOrientation() {
        return this.orientation;
    }
    
    public boolean intersectWith(final WidgetGroup widgetGroup) {
        for (int i = 0; i < this.widgets.size(); ++i) {
            if (widgetGroup.contains(this.widgets.get(i))) {
                return true;
            }
        }
        return false;
    }
    
    public boolean isAuthoritative() {
        return this.authoritative;
    }
    
    public int measureWrap(final LinearSystem linearSystem, final int n) {
        if (this.widgets.size() == 0) {
            return 0;
        }
        return this.solverMeasure(linearSystem, this.widgets, n);
    }
    
    public void moveTo(final int n, final WidgetGroup widgetGroup) {
        for (final ConstraintWidget constraintWidget : this.widgets) {
            widgetGroup.add(constraintWidget);
            if (n == 0) {
                constraintWidget.horizontalGroup = widgetGroup.getId();
            }
            else {
                constraintWidget.verticalGroup = widgetGroup.getId();
            }
        }
        this.moveTo = widgetGroup.id;
    }
    
    public void setAuthoritative(final boolean authoritative) {
        this.authoritative = authoritative;
    }
    
    public void setOrientation(final int orientation) {
        this.orientation = orientation;
    }
    
    public int size() {
        return this.widgets.size();
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append(this.getOrientationString());
        sb.append(" [");
        sb.append(this.id);
        sb.append("] <");
        String s = sb.toString();
        for (final ConstraintWidget constraintWidget : this.widgets) {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append(s);
            sb2.append(" ");
            sb2.append(constraintWidget.getDebugName());
            s = sb2.toString();
        }
        final StringBuilder sb3 = new StringBuilder();
        sb3.append(s);
        sb3.append(" >");
        return sb3.toString();
    }
    
    class MeasureResult
    {
        int baseline;
        int bottom;
        int left;
        int orientation;
        int right;
        final WidgetGroup this$0;
        int top;
        WeakReference<ConstraintWidget> widgetRef;
        
        public MeasureResult(final WidgetGroup this$0, final ConstraintWidget referent, final LinearSystem linearSystem, final int orientation) {
            this.this$0 = this$0;
            this.widgetRef = new WeakReference<ConstraintWidget>(referent);
            this.left = linearSystem.getObjectVariableValue(referent.mLeft);
            this.top = linearSystem.getObjectVariableValue(referent.mTop);
            this.right = linearSystem.getObjectVariableValue(referent.mRight);
            this.bottom = linearSystem.getObjectVariableValue(referent.mBottom);
            this.baseline = linearSystem.getObjectVariableValue(referent.mBaseline);
            this.orientation = orientation;
        }
        
        public void apply() {
            final ConstraintWidget constraintWidget = this.widgetRef.get();
            if (constraintWidget != null) {
                constraintWidget.setFinalFrame(this.left, this.top, this.right, this.bottom, this.baseline, this.orientation);
            }
        }
    }
}

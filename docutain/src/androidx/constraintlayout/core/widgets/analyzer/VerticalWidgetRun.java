// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.core.widgets.analyzer;

import androidx.constraintlayout.core.widgets.ConstraintAnchor;
import androidx.constraintlayout.core.widgets.Helper;
import androidx.constraintlayout.core.widgets.ConstraintWidget;

public class VerticalWidgetRun extends WidgetRun
{
    public DependencyNode baseline;
    DimensionDependency baselineDimension;
    
    public VerticalWidgetRun(final ConstraintWidget constraintWidget) {
        super(constraintWidget);
        this.baseline = new DependencyNode(this);
        this.baselineDimension = null;
        this.start.type = DependencyNode.Type.TOP;
        this.end.type = DependencyNode.Type.BOTTOM;
        this.baseline.type = DependencyNode.Type.BASELINE;
        this.orientation = 1;
    }
    
    @Override
    void apply() {
        if (this.widget.measured) {
            this.dimension.resolve(this.widget.getHeight());
        }
        if (!this.dimension.resolved) {
            super.dimensionBehavior = this.widget.getVerticalDimensionBehaviour();
            if (this.widget.hasBaseline()) {
                this.baselineDimension = new BaselineDimensionDependency(this);
            }
            if (super.dimensionBehavior != ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT) {
                if (this.dimensionBehavior == ConstraintWidget.DimensionBehaviour.MATCH_PARENT) {
                    final ConstraintWidget parent = this.widget.getParent();
                    if (parent != null && parent.getVerticalDimensionBehaviour() == ConstraintWidget.DimensionBehaviour.FIXED) {
                        final int height = parent.getHeight();
                        final int margin = this.widget.mTop.getMargin();
                        final int margin2 = this.widget.mBottom.getMargin();
                        this.addTarget(this.start, parent.verticalRun.start, this.widget.mTop.getMargin());
                        this.addTarget(this.end, parent.verticalRun.end, -this.widget.mBottom.getMargin());
                        this.dimension.resolve(height - margin - margin2);
                        return;
                    }
                }
                if (this.dimensionBehavior == ConstraintWidget.DimensionBehaviour.FIXED) {
                    this.dimension.resolve(this.widget.getHeight());
                }
            }
        }
        else if (this.dimensionBehavior == ConstraintWidget.DimensionBehaviour.MATCH_PARENT) {
            final ConstraintWidget parent2 = this.widget.getParent();
            if (parent2 != null && parent2.getVerticalDimensionBehaviour() == ConstraintWidget.DimensionBehaviour.FIXED) {
                this.addTarget(this.start, parent2.verticalRun.start, this.widget.mTop.getMargin());
                this.addTarget(this.end, parent2.verticalRun.end, -this.widget.mBottom.getMargin());
                return;
            }
        }
        if (this.dimension.resolved && this.widget.measured) {
            if (this.widget.mListAnchors[2].mTarget != null && this.widget.mListAnchors[3].mTarget != null) {
                if (this.widget.isInVerticalChain()) {
                    this.start.margin = this.widget.mListAnchors[2].getMargin();
                    this.end.margin = -this.widget.mListAnchors[3].getMargin();
                }
                else {
                    final DependencyNode target = this.getTarget(this.widget.mListAnchors[2]);
                    if (target != null) {
                        this.addTarget(this.start, target, this.widget.mListAnchors[2].getMargin());
                    }
                    final DependencyNode target2 = this.getTarget(this.widget.mListAnchors[3]);
                    if (target2 != null) {
                        this.addTarget(this.end, target2, -this.widget.mListAnchors[3].getMargin());
                    }
                    this.start.delegateToWidgetRun = true;
                    this.end.delegateToWidgetRun = true;
                }
                if (this.widget.hasBaseline()) {
                    this.addTarget(this.baseline, this.start, this.widget.getBaselineDistance());
                }
            }
            else if (this.widget.mListAnchors[2].mTarget != null) {
                final DependencyNode target3 = this.getTarget(this.widget.mListAnchors[2]);
                if (target3 != null) {
                    this.addTarget(this.start, target3, this.widget.mListAnchors[2].getMargin());
                    this.addTarget(this.end, this.start, this.dimension.value);
                    if (this.widget.hasBaseline()) {
                        this.addTarget(this.baseline, this.start, this.widget.getBaselineDistance());
                    }
                }
            }
            else if (this.widget.mListAnchors[3].mTarget != null) {
                final DependencyNode target4 = this.getTarget(this.widget.mListAnchors[3]);
                if (target4 != null) {
                    this.addTarget(this.end, target4, -this.widget.mListAnchors[3].getMargin());
                    this.addTarget(this.start, this.end, -this.dimension.value);
                }
                if (this.widget.hasBaseline()) {
                    this.addTarget(this.baseline, this.start, this.widget.getBaselineDistance());
                }
            }
            else if (this.widget.mListAnchors[4].mTarget != null) {
                final DependencyNode target5 = this.getTarget(this.widget.mListAnchors[4]);
                if (target5 != null) {
                    this.addTarget(this.baseline, target5, 0);
                    this.addTarget(this.start, this.baseline, -this.widget.getBaselineDistance());
                    this.addTarget(this.end, this.start, this.dimension.value);
                }
            }
            else if (!(this.widget instanceof Helper) && this.widget.getParent() != null && this.widget.getAnchor(ConstraintAnchor.Type.CENTER).mTarget == null) {
                this.addTarget(this.start, this.widget.getParent().verticalRun.start, this.widget.getY());
                this.addTarget(this.end, this.start, this.dimension.value);
                if (this.widget.hasBaseline()) {
                    this.addTarget(this.baseline, this.start, this.widget.getBaselineDistance());
                }
            }
        }
        else {
            if (!this.dimension.resolved && this.dimensionBehavior == ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT) {
                final int mMatchConstraintDefaultHeight = this.widget.mMatchConstraintDefaultHeight;
                if (mMatchConstraintDefaultHeight != 2) {
                    if (mMatchConstraintDefaultHeight == 3) {
                        if (!this.widget.isInVerticalChain()) {
                            if (this.widget.mMatchConstraintDefaultWidth != 3) {
                                final DimensionDependency dimension = this.widget.horizontalRun.dimension;
                                this.dimension.targets.add(dimension);
                                dimension.dependencies.add(this.dimension);
                                this.dimension.delegateToWidgetRun = true;
                                this.dimension.dependencies.add(this.start);
                                this.dimension.dependencies.add(this.end);
                            }
                        }
                    }
                }
                else {
                    final ConstraintWidget parent3 = this.widget.getParent();
                    if (parent3 != null) {
                        final DimensionDependency dimension2 = parent3.verticalRun.dimension;
                        this.dimension.targets.add(dimension2);
                        dimension2.dependencies.add(this.dimension);
                        this.dimension.delegateToWidgetRun = true;
                        this.dimension.dependencies.add(this.start);
                        this.dimension.dependencies.add(this.end);
                    }
                }
            }
            else {
                this.dimension.addDependency(this);
            }
            if (this.widget.mListAnchors[2].mTarget != null && this.widget.mListAnchors[3].mTarget != null) {
                if (this.widget.isInVerticalChain()) {
                    this.start.margin = this.widget.mListAnchors[2].getMargin();
                    this.end.margin = -this.widget.mListAnchors[3].getMargin();
                }
                else {
                    final DependencyNode target6 = this.getTarget(this.widget.mListAnchors[2]);
                    final DependencyNode target7 = this.getTarget(this.widget.mListAnchors[3]);
                    if (target6 != null) {
                        target6.addDependency(this);
                    }
                    if (target7 != null) {
                        target7.addDependency(this);
                    }
                    this.mRunType = RunType.CENTER;
                }
                if (this.widget.hasBaseline()) {
                    this.addTarget(this.baseline, this.start, 1, this.baselineDimension);
                }
            }
            else if (this.widget.mListAnchors[2].mTarget != null) {
                final DependencyNode target8 = this.getTarget(this.widget.mListAnchors[2]);
                if (target8 != null) {
                    this.addTarget(this.start, target8, this.widget.mListAnchors[2].getMargin());
                    this.addTarget(this.end, this.start, 1, this.dimension);
                    if (this.widget.hasBaseline()) {
                        this.addTarget(this.baseline, this.start, 1, this.baselineDimension);
                    }
                    if (this.dimensionBehavior == ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT && this.widget.getDimensionRatio() > 0.0f && this.widget.horizontalRun.dimensionBehavior == ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT) {
                        this.widget.horizontalRun.dimension.dependencies.add(this.dimension);
                        this.dimension.targets.add(this.widget.horizontalRun.dimension);
                        this.dimension.updateDelegate = this;
                    }
                }
            }
            else if (this.widget.mListAnchors[3].mTarget != null) {
                final DependencyNode target9 = this.getTarget(this.widget.mListAnchors[3]);
                if (target9 != null) {
                    this.addTarget(this.end, target9, -this.widget.mListAnchors[3].getMargin());
                    this.addTarget(this.start, this.end, -1, this.dimension);
                    if (this.widget.hasBaseline()) {
                        this.addTarget(this.baseline, this.start, 1, this.baselineDimension);
                    }
                }
            }
            else if (this.widget.mListAnchors[4].mTarget != null) {
                final DependencyNode target10 = this.getTarget(this.widget.mListAnchors[4]);
                if (target10 != null) {
                    this.addTarget(this.baseline, target10, 0);
                    this.addTarget(this.start, this.baseline, -1, this.baselineDimension);
                    this.addTarget(this.end, this.start, 1, this.dimension);
                }
            }
            else if (!(this.widget instanceof Helper) && this.widget.getParent() != null) {
                this.addTarget(this.start, this.widget.getParent().verticalRun.start, this.widget.getY());
                this.addTarget(this.end, this.start, 1, this.dimension);
                if (this.widget.hasBaseline()) {
                    this.addTarget(this.baseline, this.start, 1, this.baselineDimension);
                }
                if (this.dimensionBehavior == ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT && this.widget.getDimensionRatio() > 0.0f && this.widget.horizontalRun.dimensionBehavior == ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT) {
                    this.widget.horizontalRun.dimension.dependencies.add(this.dimension);
                    this.dimension.targets.add(this.widget.horizontalRun.dimension);
                    this.dimension.updateDelegate = this;
                }
            }
            if (this.dimension.targets.size() == 0) {
                this.dimension.readyToSolve = true;
            }
        }
    }
    
    public void applyToWidget() {
        if (this.start.resolved) {
            this.widget.setY(this.start.value);
        }
    }
    
    @Override
    void clear() {
        this.runGroup = null;
        this.start.clear();
        this.end.clear();
        this.baseline.clear();
        this.dimension.clear();
        this.resolved = false;
    }
    
    @Override
    void reset() {
        this.resolved = false;
        this.start.clear();
        this.start.resolved = false;
        this.end.clear();
        this.end.resolved = false;
        this.baseline.clear();
        this.baseline.resolved = false;
        this.dimension.resolved = false;
    }
    
    @Override
    boolean supportsWrapComputation() {
        return super.dimensionBehavior != ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT || super.widget.mMatchConstraintDefaultHeight == 0;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("VerticalRun ");
        sb.append(this.widget.getDebugName());
        return sb.toString();
    }
    
    @Override
    public void update(final Dependency dependency) {
        final int n = VerticalWidgetRun$1.$SwitchMap$androidx$constraintlayout$core$widgets$analyzer$WidgetRun$RunType[this.mRunType.ordinal()];
        if (n != 1) {
            if (n != 2) {
                if (n == 3) {
                    this.updateRunCenter(dependency, this.widget.mTop, this.widget.mBottom, 1);
                    return;
                }
            }
            else {
                this.updateRunEnd(dependency);
            }
        }
        else {
            this.updateRunStart(dependency);
        }
        if (this.dimension.readyToSolve && !this.dimension.resolved && this.dimensionBehavior == ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT) {
            final int mMatchConstraintDefaultHeight = this.widget.mMatchConstraintDefaultHeight;
            if (mMatchConstraintDefaultHeight != 2) {
                if (mMatchConstraintDefaultHeight == 3) {
                    if (this.widget.horizontalRun.dimension.resolved) {
                        final int dimensionRatioSide = this.widget.getDimensionRatioSide();
                        int n3 = 0;
                        Label_0257: {
                            float n2 = 0.0f;
                            Label_0249: {
                                float n4;
                                float n5;
                                if (dimensionRatioSide != -1) {
                                    if (dimensionRatioSide == 0) {
                                        n2 = this.widget.horizontalRun.dimension.value * this.widget.getDimensionRatio();
                                        break Label_0249;
                                    }
                                    if (dimensionRatioSide != 1) {
                                        n3 = 0;
                                        break Label_0257;
                                    }
                                    n4 = (float)this.widget.horizontalRun.dimension.value;
                                    n5 = this.widget.getDimensionRatio();
                                }
                                else {
                                    n4 = (float)this.widget.horizontalRun.dimension.value;
                                    n5 = this.widget.getDimensionRatio();
                                }
                                n2 = n4 / n5;
                            }
                            n3 = (int)(n2 + 0.5f);
                        }
                        this.dimension.resolve(n3);
                    }
                }
            }
            else {
                final ConstraintWidget parent = this.widget.getParent();
                if (parent != null && parent.verticalRun.dimension.resolved) {
                    this.dimension.resolve((int)(parent.verticalRun.dimension.value * this.widget.mMatchConstraintPercentHeight + 0.5f));
                }
            }
        }
        if (this.start.readyToSolve) {
            if (this.end.readyToSolve) {
                if (this.start.resolved && this.end.resolved && this.dimension.resolved) {
                    return;
                }
                if (!this.dimension.resolved && this.dimensionBehavior == ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT && this.widget.mMatchConstraintDefaultWidth == 0 && !this.widget.isInVerticalChain()) {
                    final DependencyNode dependencyNode = this.start.targets.get(0);
                    final DependencyNode dependencyNode2 = this.end.targets.get(0);
                    final int n6 = dependencyNode.value + this.start.margin;
                    final int n7 = dependencyNode2.value + this.end.margin;
                    this.start.resolve(n6);
                    this.end.resolve(n7);
                    this.dimension.resolve(n7 - n6);
                    return;
                }
                if (!this.dimension.resolved && this.dimensionBehavior == ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT && this.matchConstraintsType == 1 && this.start.targets.size() > 0 && this.end.targets.size() > 0) {
                    final int n8 = this.end.targets.get(0).value + this.end.margin - (this.start.targets.get(0).value + this.start.margin);
                    if (n8 < this.dimension.wrapValue) {
                        this.dimension.resolve(n8);
                    }
                    else {
                        this.dimension.resolve(this.dimension.wrapValue);
                    }
                }
                if (!this.dimension.resolved) {
                    return;
                }
                if (this.start.targets.size() > 0 && this.end.targets.size() > 0) {
                    final DependencyNode dependencyNode3 = this.start.targets.get(0);
                    final DependencyNode dependencyNode4 = this.end.targets.get(0);
                    int value = dependencyNode3.value + this.start.margin;
                    int value2 = dependencyNode4.value + this.end.margin;
                    float verticalBiasPercent = this.widget.getVerticalBiasPercent();
                    if (dependencyNode3 == dependencyNode4) {
                        value = dependencyNode3.value;
                        value2 = dependencyNode4.value;
                        verticalBiasPercent = 0.5f;
                    }
                    this.start.resolve((int)(value + 0.5f + (value2 - value - this.dimension.value) * verticalBiasPercent));
                    this.end.resolve(this.start.value + this.dimension.value);
                }
            }
        }
    }
}

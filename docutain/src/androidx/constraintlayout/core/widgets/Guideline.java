// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.core.widgets;

import java.util.HashMap;
import androidx.constraintlayout.core.SolverVariable;
import androidx.constraintlayout.core.LinearSystem;

public class Guideline extends ConstraintWidget
{
    public static final int HORIZONTAL = 0;
    public static final int RELATIVE_BEGIN = 1;
    public static final int RELATIVE_END = 2;
    public static final int RELATIVE_PERCENT = 0;
    public static final int RELATIVE_UNKNOWN = -1;
    public static final int VERTICAL = 1;
    protected boolean guidelineUseRtl;
    private ConstraintAnchor mAnchor;
    private int mMinimumPosition;
    private int mOrientation;
    protected int mRelativeBegin;
    protected int mRelativeEnd;
    protected float mRelativePercent;
    private boolean resolved;
    
    public Guideline() {
        this.mRelativePercent = -1.0f;
        this.mRelativeBegin = -1;
        this.mRelativeEnd = -1;
        this.guidelineUseRtl = true;
        this.mAnchor = this.mTop;
        int i = 0;
        this.mOrientation = 0;
        this.mMinimumPosition = 0;
        this.mAnchors.clear();
        this.mAnchors.add(this.mAnchor);
        while (i < this.mListAnchors.length) {
            this.mListAnchors[i] = this.mAnchor;
            ++i;
        }
    }
    
    @Override
    public void addToSolver(final LinearSystem linearSystem, final boolean b) {
        final ConstraintWidgetContainer constraintWidgetContainer = (ConstraintWidgetContainer)this.getParent();
        if (constraintWidgetContainer == null) {
            return;
        }
        ConstraintAnchor constraintAnchor = constraintWidgetContainer.getAnchor(ConstraintAnchor.Type.LEFT);
        ConstraintAnchor constraintAnchor2 = constraintWidgetContainer.getAnchor(ConstraintAnchor.Type.RIGHT);
        final ConstraintWidget mParent = this.mParent;
        final int n = 1;
        int n2;
        if (mParent != null && this.mParent.mListDimensionBehaviors[0] == DimensionBehaviour.WRAP_CONTENT) {
            n2 = 1;
        }
        else {
            n2 = 0;
        }
        if (this.mOrientation == 0) {
            constraintAnchor = constraintWidgetContainer.getAnchor(ConstraintAnchor.Type.TOP);
            constraintAnchor2 = constraintWidgetContainer.getAnchor(ConstraintAnchor.Type.BOTTOM);
            if (this.mParent != null && this.mParent.mListDimensionBehaviors[1] == DimensionBehaviour.WRAP_CONTENT) {
                n2 = n;
            }
            else {
                n2 = 0;
            }
        }
        if (this.resolved && this.mAnchor.hasFinalValue()) {
            final SolverVariable objectVariable = linearSystem.createObjectVariable(this.mAnchor);
            linearSystem.addEquality(objectVariable, this.mAnchor.getFinalValue());
            if (this.mRelativeBegin != -1) {
                if (n2 != 0) {
                    linearSystem.addGreaterThan(linearSystem.createObjectVariable(constraintAnchor2), objectVariable, 0, 5);
                }
            }
            else if (this.mRelativeEnd != -1 && n2 != 0) {
                final SolverVariable objectVariable2 = linearSystem.createObjectVariable(constraintAnchor2);
                linearSystem.addGreaterThan(objectVariable, linearSystem.createObjectVariable(constraintAnchor), 0, 5);
                linearSystem.addGreaterThan(objectVariable2, objectVariable, 0, 5);
            }
            this.resolved = false;
            return;
        }
        if (this.mRelativeBegin != -1) {
            final SolverVariable objectVariable3 = linearSystem.createObjectVariable(this.mAnchor);
            linearSystem.addEquality(objectVariable3, linearSystem.createObjectVariable(constraintAnchor), this.mRelativeBegin, 8);
            if (n2 != 0) {
                linearSystem.addGreaterThan(linearSystem.createObjectVariable(constraintAnchor2), objectVariable3, 0, 5);
            }
        }
        else if (this.mRelativeEnd != -1) {
            final SolverVariable objectVariable4 = linearSystem.createObjectVariable(this.mAnchor);
            final SolverVariable objectVariable5 = linearSystem.createObjectVariable(constraintAnchor2);
            linearSystem.addEquality(objectVariable4, objectVariable5, -this.mRelativeEnd, 8);
            if (n2 != 0) {
                linearSystem.addGreaterThan(objectVariable4, linearSystem.createObjectVariable(constraintAnchor), 0, 5);
                linearSystem.addGreaterThan(objectVariable5, objectVariable4, 0, 5);
            }
        }
        else if (this.mRelativePercent != -1.0f) {
            linearSystem.addConstraint(LinearSystem.createRowDimensionPercent(linearSystem, linearSystem.createObjectVariable(this.mAnchor), linearSystem.createObjectVariable(constraintAnchor2), this.mRelativePercent));
        }
    }
    
    @Override
    public boolean allowedInBarrier() {
        return true;
    }
    
    @Override
    public void copy(final ConstraintWidget constraintWidget, final HashMap<ConstraintWidget, ConstraintWidget> hashMap) {
        super.copy(constraintWidget, hashMap);
        final Guideline guideline = (Guideline)constraintWidget;
        this.mRelativePercent = guideline.mRelativePercent;
        this.mRelativeBegin = guideline.mRelativeBegin;
        this.mRelativeEnd = guideline.mRelativeEnd;
        this.guidelineUseRtl = guideline.guidelineUseRtl;
        this.setOrientation(guideline.mOrientation);
    }
    
    public void cyclePosition() {
        if (this.mRelativeBegin != -1) {
            this.inferRelativePercentPosition();
        }
        else if (this.mRelativePercent != -1.0f) {
            this.inferRelativeEndPosition();
        }
        else if (this.mRelativeEnd != -1) {
            this.inferRelativeBeginPosition();
        }
    }
    
    public ConstraintAnchor getAnchor() {
        return this.mAnchor;
    }
    
    @Override
    public ConstraintAnchor getAnchor(final ConstraintAnchor.Type type) {
        final int n = Guideline$1.$SwitchMap$androidx$constraintlayout$core$widgets$ConstraintAnchor$Type[type.ordinal()];
        if (n != 1 && n != 2) {
            if (n == 3 || n == 4) {
                if (this.mOrientation == 0) {
                    return this.mAnchor;
                }
            }
        }
        else if (this.mOrientation == 1) {
            return this.mAnchor;
        }
        return null;
    }
    
    public int getOrientation() {
        return this.mOrientation;
    }
    
    public int getRelativeBegin() {
        return this.mRelativeBegin;
    }
    
    public int getRelativeBehaviour() {
        if (this.mRelativePercent != -1.0f) {
            return 0;
        }
        if (this.mRelativeBegin != -1) {
            return 1;
        }
        if (this.mRelativeEnd != -1) {
            return 2;
        }
        return -1;
    }
    
    public int getRelativeEnd() {
        return this.mRelativeEnd;
    }
    
    public float getRelativePercent() {
        return this.mRelativePercent;
    }
    
    @Override
    public String getType() {
        return "Guideline";
    }
    
    void inferRelativeBeginPosition() {
        int guideBegin = this.getX();
        if (this.mOrientation == 0) {
            guideBegin = this.getY();
        }
        this.setGuideBegin(guideBegin);
    }
    
    void inferRelativeEndPosition() {
        int guideEnd = this.getParent().getWidth() - this.getX();
        if (this.mOrientation == 0) {
            guideEnd = this.getParent().getHeight() - this.getY();
        }
        this.setGuideEnd(guideEnd);
    }
    
    void inferRelativePercentPosition() {
        float guidePercent = this.getX() / (float)this.getParent().getWidth();
        if (this.mOrientation == 0) {
            guidePercent = this.getY() / (float)this.getParent().getHeight();
        }
        this.setGuidePercent(guidePercent);
    }
    
    public boolean isPercent() {
        return this.mRelativePercent != -1.0f && this.mRelativeBegin == -1 && this.mRelativeEnd == -1;
    }
    
    @Override
    public boolean isResolvedHorizontally() {
        return this.resolved;
    }
    
    @Override
    public boolean isResolvedVertically() {
        return this.resolved;
    }
    
    public void setFinalValue(final int finalValue) {
        this.mAnchor.setFinalValue(finalValue);
        this.resolved = true;
    }
    
    public void setGuideBegin(final int mRelativeBegin) {
        if (mRelativeBegin > -1) {
            this.mRelativePercent = -1.0f;
            this.mRelativeBegin = mRelativeBegin;
            this.mRelativeEnd = -1;
        }
    }
    
    public void setGuideEnd(final int mRelativeEnd) {
        if (mRelativeEnd > -1) {
            this.mRelativePercent = -1.0f;
            this.mRelativeBegin = -1;
            this.mRelativeEnd = mRelativeEnd;
        }
    }
    
    public void setGuidePercent(final float mRelativePercent) {
        if (mRelativePercent > -1.0f) {
            this.mRelativePercent = mRelativePercent;
            this.mRelativeBegin = -1;
            this.mRelativeEnd = -1;
        }
    }
    
    public void setGuidePercent(final int n) {
        this.setGuidePercent(n / 100.0f);
    }
    
    public void setMinimumPosition(final int mMinimumPosition) {
        this.mMinimumPosition = mMinimumPosition;
    }
    
    public void setOrientation(int i) {
        if (this.mOrientation == i) {
            return;
        }
        this.mOrientation = i;
        this.mAnchors.clear();
        if (this.mOrientation == 1) {
            this.mAnchor = this.mLeft;
        }
        else {
            this.mAnchor = this.mTop;
        }
        this.mAnchors.add(this.mAnchor);
        int length;
        for (length = this.mListAnchors.length, i = 0; i < length; ++i) {
            this.mListAnchors[i] = this.mAnchor;
        }
    }
    
    @Override
    public void updateFromSolver(final LinearSystem linearSystem, final boolean b) {
        if (this.getParent() == null) {
            return;
        }
        final int objectVariableValue = linearSystem.getObjectVariableValue(this.mAnchor);
        if (this.mOrientation == 1) {
            this.setX(objectVariableValue);
            this.setY(0);
            this.setHeight(this.getParent().getHeight());
            this.setWidth(0);
        }
        else {
            this.setX(0);
            this.setY(objectVariableValue);
            this.setWidth(this.getParent().getWidth());
            this.setHeight(0);
        }
    }
}

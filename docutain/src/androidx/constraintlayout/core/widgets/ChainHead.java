// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.core.widgets;

import java.util.ArrayList;

public class ChainHead
{
    private boolean mDefined;
    protected ConstraintWidget mFirst;
    protected ConstraintWidget mFirstMatchConstraintWidget;
    protected ConstraintWidget mFirstVisibleWidget;
    protected boolean mHasComplexMatchWeights;
    protected boolean mHasDefinedWeights;
    protected boolean mHasRatio;
    protected boolean mHasUndefinedWeights;
    protected ConstraintWidget mHead;
    private boolean mIsRtl;
    protected ConstraintWidget mLast;
    protected ConstraintWidget mLastMatchConstraintWidget;
    protected ConstraintWidget mLastVisibleWidget;
    boolean mOptimizable;
    private int mOrientation;
    int mTotalMargins;
    int mTotalSize;
    protected float mTotalWeight;
    int mVisibleWidgets;
    protected ArrayList<ConstraintWidget> mWeightedMatchConstraintsWidgets;
    protected int mWidgetsCount;
    protected int mWidgetsMatchCount;
    
    public ChainHead(final ConstraintWidget mFirst, final int mOrientation, final boolean mIsRtl) {
        this.mTotalWeight = 0.0f;
        this.mFirst = mFirst;
        this.mOrientation = mOrientation;
        this.mIsRtl = mIsRtl;
    }
    
    private void defineChainProperties() {
        final int n = this.mOrientation * 2;
        ConstraintWidget mFirst = this.mFirst;
        boolean mHasComplexMatchWeights = true;
        this.mOptimizable = true;
        ConstraintWidget constraintWidget = mFirst;
        int i = 0;
        while (i == 0) {
            ++this.mWidgetsCount;
            final ConstraintWidget[] mNextChainWidget = mFirst.mNextChainWidget;
            final int mOrientation = this.mOrientation;
            final ConstraintWidget constraintWidget2 = null;
            mNextChainWidget[mOrientation] = null;
            mFirst.mListNextMatchConstraintsWidget[this.mOrientation] = null;
            if (mFirst.getVisibility() != 8) {
                ++this.mVisibleWidgets;
                if (mFirst.getDimensionBehaviour(this.mOrientation) != ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT) {
                    this.mTotalSize += mFirst.getLength(this.mOrientation);
                }
                final int mTotalSize = this.mTotalSize + mFirst.mListAnchors[n].getMargin();
                this.mTotalSize = mTotalSize;
                final ConstraintAnchor[] mListAnchors = mFirst.mListAnchors;
                final int n2 = n + 1;
                this.mTotalSize = mTotalSize + mListAnchors[n2].getMargin();
                final int mTotalMargins = this.mTotalMargins + mFirst.mListAnchors[n].getMargin();
                this.mTotalMargins = mTotalMargins;
                this.mTotalMargins = mTotalMargins + mFirst.mListAnchors[n2].getMargin();
                if (this.mFirstVisibleWidget == null) {
                    this.mFirstVisibleWidget = mFirst;
                }
                this.mLastVisibleWidget = mFirst;
                if (mFirst.mListDimensionBehaviors[this.mOrientation] == ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT) {
                    if (mFirst.mResolvedMatchConstraintDefault[this.mOrientation] == 0 || mFirst.mResolvedMatchConstraintDefault[this.mOrientation] == 3 || mFirst.mResolvedMatchConstraintDefault[this.mOrientation] == 2) {
                        ++this.mWidgetsMatchCount;
                        final float n3 = mFirst.mWeight[this.mOrientation];
                        if (n3 > 0.0f) {
                            this.mTotalWeight += mFirst.mWeight[this.mOrientation];
                        }
                        if (isMatchConstraintEqualityCandidate(mFirst, this.mOrientation)) {
                            if (n3 < 0.0f) {
                                this.mHasUndefinedWeights = true;
                            }
                            else {
                                this.mHasDefinedWeights = true;
                            }
                            if (this.mWeightedMatchConstraintsWidgets == null) {
                                this.mWeightedMatchConstraintsWidgets = new ArrayList<ConstraintWidget>();
                            }
                            this.mWeightedMatchConstraintsWidgets.add(mFirst);
                        }
                        if (this.mFirstMatchConstraintWidget == null) {
                            this.mFirstMatchConstraintWidget = mFirst;
                        }
                        final ConstraintWidget mLastMatchConstraintWidget = this.mLastMatchConstraintWidget;
                        if (mLastMatchConstraintWidget != null) {
                            mLastMatchConstraintWidget.mListNextMatchConstraintsWidget[this.mOrientation] = mFirst;
                        }
                        this.mLastMatchConstraintWidget = mFirst;
                    }
                    if (this.mOrientation == 0) {
                        if (mFirst.mMatchConstraintDefaultWidth != 0) {
                            this.mOptimizable = false;
                        }
                        else if (mFirst.mMatchConstraintMinWidth != 0 || mFirst.mMatchConstraintMaxWidth != 0) {
                            this.mOptimizable = false;
                        }
                    }
                    else if (mFirst.mMatchConstraintDefaultHeight != 0) {
                        this.mOptimizable = false;
                    }
                    else if (mFirst.mMatchConstraintMinHeight != 0 || mFirst.mMatchConstraintMaxHeight != 0) {
                        this.mOptimizable = false;
                    }
                    if (mFirst.mDimensionRatio != 0.0f) {
                        this.mOptimizable = false;
                        this.mHasRatio = true;
                    }
                }
            }
            if (constraintWidget != mFirst) {
                constraintWidget.mNextChainWidget[this.mOrientation] = mFirst;
            }
            final ConstraintAnchor mTarget = mFirst.mListAnchors[n + 1].mTarget;
            ConstraintWidget constraintWidget3 = constraintWidget2;
            if (mTarget != null) {
                final ConstraintWidget mOwner = mTarget.mOwner;
                constraintWidget3 = constraintWidget2;
                if (mOwner.mListAnchors[n].mTarget != null) {
                    if (mOwner.mListAnchors[n].mTarget.mOwner != mFirst) {
                        constraintWidget3 = constraintWidget2;
                    }
                    else {
                        constraintWidget3 = mOwner;
                    }
                }
            }
            if (constraintWidget3 == null) {
                constraintWidget3 = mFirst;
                i = 1;
            }
            final ConstraintWidget constraintWidget4 = mFirst;
            mFirst = constraintWidget3;
            constraintWidget = constraintWidget4;
        }
        final ConstraintWidget mFirstVisibleWidget = this.mFirstVisibleWidget;
        if (mFirstVisibleWidget != null) {
            this.mTotalSize -= mFirstVisibleWidget.mListAnchors[n].getMargin();
        }
        final ConstraintWidget mLastVisibleWidget = this.mLastVisibleWidget;
        if (mLastVisibleWidget != null) {
            this.mTotalSize -= mLastVisibleWidget.mListAnchors[n + 1].getMargin();
        }
        this.mLast = mFirst;
        if (this.mOrientation == 0 && this.mIsRtl) {
            this.mHead = mFirst;
        }
        else {
            this.mHead = this.mFirst;
        }
        if (!this.mHasDefinedWeights || !this.mHasUndefinedWeights) {
            mHasComplexMatchWeights = false;
        }
        this.mHasComplexMatchWeights = mHasComplexMatchWeights;
    }
    
    private static boolean isMatchConstraintEqualityCandidate(final ConstraintWidget constraintWidget, final int n) {
        return constraintWidget.getVisibility() != 8 && constraintWidget.mListDimensionBehaviors[n] == ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT && (constraintWidget.mResolvedMatchConstraintDefault[n] == 0 || constraintWidget.mResolvedMatchConstraintDefault[n] == 3);
    }
    
    public void define() {
        if (!this.mDefined) {
            this.defineChainProperties();
        }
        this.mDefined = true;
    }
    
    public ConstraintWidget getFirst() {
        return this.mFirst;
    }
    
    public ConstraintWidget getFirstMatchConstraintWidget() {
        return this.mFirstMatchConstraintWidget;
    }
    
    public ConstraintWidget getFirstVisibleWidget() {
        return this.mFirstVisibleWidget;
    }
    
    public ConstraintWidget getHead() {
        return this.mHead;
    }
    
    public ConstraintWidget getLast() {
        return this.mLast;
    }
    
    public ConstraintWidget getLastMatchConstraintWidget() {
        return this.mLastMatchConstraintWidget;
    }
    
    public ConstraintWidget getLastVisibleWidget() {
        return this.mLastVisibleWidget;
    }
    
    public float getTotalWeight() {
        return this.mTotalWeight;
    }
}

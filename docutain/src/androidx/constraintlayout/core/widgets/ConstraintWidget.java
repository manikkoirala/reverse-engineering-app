// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.core.widgets;

import androidx.constraintlayout.core.Cache;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.HashSet;
import androidx.constraintlayout.core.Metrics;
import androidx.constraintlayout.core.SolverVariable;
import androidx.constraintlayout.core.LinearSystem;
import androidx.constraintlayout.core.widgets.analyzer.VerticalWidgetRun;
import androidx.constraintlayout.core.widgets.analyzer.WidgetRun;
import java.util.ArrayList;
import androidx.constraintlayout.core.widgets.analyzer.HorizontalWidgetRun;
import androidx.constraintlayout.core.widgets.analyzer.ChainRun;
import androidx.constraintlayout.core.state.WidgetFrame;

public class ConstraintWidget
{
    public static final int ANCHOR_BASELINE = 4;
    public static final int ANCHOR_BOTTOM = 3;
    public static final int ANCHOR_LEFT = 0;
    public static final int ANCHOR_RIGHT = 1;
    public static final int ANCHOR_TOP = 2;
    private static final boolean AUTOTAG_CENTER = false;
    public static final int BOTH = 2;
    public static final int CHAIN_PACKED = 2;
    public static final int CHAIN_SPREAD = 0;
    public static final int CHAIN_SPREAD_INSIDE = 1;
    public static float DEFAULT_BIAS = 0.5f;
    static final int DIMENSION_HORIZONTAL = 0;
    static final int DIMENSION_VERTICAL = 1;
    protected static final int DIRECT = 2;
    public static final int GONE = 8;
    public static final int HORIZONTAL = 0;
    public static final int INVISIBLE = 4;
    public static final int MATCH_CONSTRAINT_PERCENT = 2;
    public static final int MATCH_CONSTRAINT_RATIO = 3;
    public static final int MATCH_CONSTRAINT_RATIO_RESOLVED = 4;
    public static final int MATCH_CONSTRAINT_SPREAD = 0;
    public static final int MATCH_CONSTRAINT_WRAP = 1;
    protected static final int SOLVER = 1;
    public static final int UNKNOWN = -1;
    private static final boolean USE_WRAP_DIMENSION_FOR_SPREAD = false;
    public static final int VERTICAL = 1;
    public static final int VISIBLE = 0;
    private static final int WRAP = -2;
    public static final int WRAP_BEHAVIOR_HORIZONTAL_ONLY = 1;
    public static final int WRAP_BEHAVIOR_INCLUDED = 0;
    public static final int WRAP_BEHAVIOR_SKIPPED = 3;
    public static final int WRAP_BEHAVIOR_VERTICAL_ONLY = 2;
    private boolean OPTIMIZE_WRAP;
    private boolean OPTIMIZE_WRAP_ON_RESOLVED;
    public WidgetFrame frame;
    private boolean hasBaseline;
    public ChainRun horizontalChainRun;
    public int horizontalGroup;
    public HorizontalWidgetRun horizontalRun;
    private boolean horizontalSolvingPass;
    private boolean inPlaceholder;
    public boolean[] isTerminalWidget;
    protected ArrayList<ConstraintAnchor> mAnchors;
    private boolean mAnimated;
    public ConstraintAnchor mBaseline;
    int mBaselineDistance;
    public ConstraintAnchor mBottom;
    boolean mBottomHasCentered;
    public ConstraintAnchor mCenter;
    ConstraintAnchor mCenterX;
    ConstraintAnchor mCenterY;
    private float mCircleConstraintAngle;
    private Object mCompanionWidget;
    private int mContainerItemSkip;
    private String mDebugName;
    public float mDimensionRatio;
    protected int mDimensionRatioSide;
    int mDistToBottom;
    int mDistToLeft;
    int mDistToRight;
    int mDistToTop;
    boolean mGroupsToSolver;
    int mHeight;
    private int mHeightOverride;
    float mHorizontalBiasPercent;
    boolean mHorizontalChainFixedPosition;
    int mHorizontalChainStyle;
    ConstraintWidget mHorizontalNextWidget;
    public int mHorizontalResolution;
    boolean mHorizontalWrapVisited;
    private boolean mInVirtualLayout;
    public boolean mIsHeightWrapContent;
    private boolean[] mIsInBarrier;
    public boolean mIsWidthWrapContent;
    private int mLastHorizontalMeasureSpec;
    private int mLastVerticalMeasureSpec;
    public ConstraintAnchor mLeft;
    boolean mLeftHasCentered;
    public ConstraintAnchor[] mListAnchors;
    public DimensionBehaviour[] mListDimensionBehaviors;
    protected ConstraintWidget[] mListNextMatchConstraintsWidget;
    public int mMatchConstraintDefaultHeight;
    public int mMatchConstraintDefaultWidth;
    public int mMatchConstraintMaxHeight;
    public int mMatchConstraintMaxWidth;
    public int mMatchConstraintMinHeight;
    public int mMatchConstraintMinWidth;
    public float mMatchConstraintPercentHeight;
    public float mMatchConstraintPercentWidth;
    private int[] mMaxDimension;
    private boolean mMeasureRequested;
    protected int mMinHeight;
    protected int mMinWidth;
    protected ConstraintWidget[] mNextChainWidget;
    protected int mOffsetX;
    protected int mOffsetY;
    public ConstraintWidget mParent;
    int mRelX;
    int mRelY;
    float mResolvedDimensionRatio;
    int mResolvedDimensionRatioSide;
    boolean mResolvedHasRatio;
    public int[] mResolvedMatchConstraintDefault;
    public ConstraintAnchor mRight;
    boolean mRightHasCentered;
    public ConstraintAnchor mTop;
    boolean mTopHasCentered;
    private String mType;
    float mVerticalBiasPercent;
    boolean mVerticalChainFixedPosition;
    int mVerticalChainStyle;
    ConstraintWidget mVerticalNextWidget;
    public int mVerticalResolution;
    boolean mVerticalWrapVisited;
    private int mVisibility;
    public float[] mWeight;
    int mWidth;
    private int mWidthOverride;
    private int mWrapBehaviorInParent;
    protected int mX;
    protected int mY;
    public boolean measured;
    private boolean resolvedHorizontal;
    private boolean resolvedVertical;
    public WidgetRun[] run;
    public String stringId;
    public ChainRun verticalChainRun;
    public int verticalGroup;
    public VerticalWidgetRun verticalRun;
    private boolean verticalSolvingPass;
    
    public ConstraintWidget() {
        this.measured = false;
        this.run = new WidgetRun[2];
        this.horizontalRun = null;
        this.verticalRun = null;
        this.isTerminalWidget = new boolean[] { true, true };
        this.mResolvedHasRatio = false;
        this.mMeasureRequested = true;
        this.OPTIMIZE_WRAP = false;
        this.OPTIMIZE_WRAP_ON_RESOLVED = true;
        this.mWidthOverride = -1;
        this.mHeightOverride = -1;
        this.frame = new WidgetFrame(this);
        this.resolvedHorizontal = false;
        this.resolvedVertical = false;
        this.horizontalSolvingPass = false;
        this.verticalSolvingPass = false;
        this.mHorizontalResolution = -1;
        this.mVerticalResolution = -1;
        this.mWrapBehaviorInParent = 0;
        this.mMatchConstraintDefaultWidth = 0;
        this.mMatchConstraintDefaultHeight = 0;
        this.mResolvedMatchConstraintDefault = new int[2];
        this.mMatchConstraintMinWidth = 0;
        this.mMatchConstraintMaxWidth = 0;
        this.mMatchConstraintPercentWidth = 1.0f;
        this.mMatchConstraintMinHeight = 0;
        this.mMatchConstraintMaxHeight = 0;
        this.mMatchConstraintPercentHeight = 1.0f;
        this.mResolvedDimensionRatioSide = -1;
        this.mResolvedDimensionRatio = 1.0f;
        this.mMaxDimension = new int[] { Integer.MAX_VALUE, Integer.MAX_VALUE };
        this.mCircleConstraintAngle = 0.0f;
        this.hasBaseline = false;
        this.mInVirtualLayout = false;
        this.mLastHorizontalMeasureSpec = 0;
        this.mLastVerticalMeasureSpec = 0;
        this.mLeft = new ConstraintAnchor(this, ConstraintAnchor.Type.LEFT);
        this.mTop = new ConstraintAnchor(this, ConstraintAnchor.Type.TOP);
        this.mRight = new ConstraintAnchor(this, ConstraintAnchor.Type.RIGHT);
        this.mBottom = new ConstraintAnchor(this, ConstraintAnchor.Type.BOTTOM);
        this.mBaseline = new ConstraintAnchor(this, ConstraintAnchor.Type.BASELINE);
        this.mCenterX = new ConstraintAnchor(this, ConstraintAnchor.Type.CENTER_X);
        this.mCenterY = new ConstraintAnchor(this, ConstraintAnchor.Type.CENTER_Y);
        final ConstraintAnchor mCenter = new ConstraintAnchor(this, ConstraintAnchor.Type.CENTER);
        this.mCenter = mCenter;
        this.mListAnchors = new ConstraintAnchor[] { this.mLeft, this.mRight, this.mTop, this.mBottom, this.mBaseline, mCenter };
        this.mAnchors = new ArrayList<ConstraintAnchor>();
        this.mIsInBarrier = new boolean[2];
        this.mListDimensionBehaviors = new DimensionBehaviour[] { DimensionBehaviour.FIXED, DimensionBehaviour.FIXED };
        this.mParent = null;
        this.mWidth = 0;
        this.mHeight = 0;
        this.mDimensionRatio = 0.0f;
        this.mDimensionRatioSide = -1;
        this.mX = 0;
        this.mY = 0;
        this.mRelX = 0;
        this.mRelY = 0;
        this.mOffsetX = 0;
        this.mOffsetY = 0;
        this.mBaselineDistance = 0;
        final float default_BIAS = ConstraintWidget.DEFAULT_BIAS;
        this.mHorizontalBiasPercent = default_BIAS;
        this.mVerticalBiasPercent = default_BIAS;
        this.mContainerItemSkip = 0;
        this.mVisibility = 0;
        this.mAnimated = false;
        this.mDebugName = null;
        this.mType = null;
        this.mGroupsToSolver = false;
        this.mHorizontalChainStyle = 0;
        this.mVerticalChainStyle = 0;
        this.mWeight = new float[] { -1.0f, -1.0f };
        this.mListNextMatchConstraintsWidget = new ConstraintWidget[] { null, null };
        this.mNextChainWidget = new ConstraintWidget[] { null, null };
        this.mHorizontalNextWidget = null;
        this.mVerticalNextWidget = null;
        this.horizontalGroup = -1;
        this.verticalGroup = -1;
        this.addAnchors();
    }
    
    public ConstraintWidget(final int n, final int n2) {
        this(0, 0, n, n2);
    }
    
    public ConstraintWidget(final int mx, final int my, final int mWidth, final int mHeight) {
        this.measured = false;
        this.run = new WidgetRun[2];
        this.horizontalRun = null;
        this.verticalRun = null;
        this.isTerminalWidget = new boolean[] { true, true };
        this.mResolvedHasRatio = false;
        this.mMeasureRequested = true;
        this.OPTIMIZE_WRAP = false;
        this.OPTIMIZE_WRAP_ON_RESOLVED = true;
        this.mWidthOverride = -1;
        this.mHeightOverride = -1;
        this.frame = new WidgetFrame(this);
        this.resolvedHorizontal = false;
        this.resolvedVertical = false;
        this.horizontalSolvingPass = false;
        this.verticalSolvingPass = false;
        this.mHorizontalResolution = -1;
        this.mVerticalResolution = -1;
        this.mWrapBehaviorInParent = 0;
        this.mMatchConstraintDefaultWidth = 0;
        this.mMatchConstraintDefaultHeight = 0;
        this.mResolvedMatchConstraintDefault = new int[2];
        this.mMatchConstraintMinWidth = 0;
        this.mMatchConstraintMaxWidth = 0;
        this.mMatchConstraintPercentWidth = 1.0f;
        this.mMatchConstraintMinHeight = 0;
        this.mMatchConstraintMaxHeight = 0;
        this.mMatchConstraintPercentHeight = 1.0f;
        this.mResolvedDimensionRatioSide = -1;
        this.mResolvedDimensionRatio = 1.0f;
        this.mMaxDimension = new int[] { Integer.MAX_VALUE, Integer.MAX_VALUE };
        this.mCircleConstraintAngle = 0.0f;
        this.hasBaseline = false;
        this.mInVirtualLayout = false;
        this.mLastHorizontalMeasureSpec = 0;
        this.mLastVerticalMeasureSpec = 0;
        this.mLeft = new ConstraintAnchor(this, ConstraintAnchor.Type.LEFT);
        this.mTop = new ConstraintAnchor(this, ConstraintAnchor.Type.TOP);
        this.mRight = new ConstraintAnchor(this, ConstraintAnchor.Type.RIGHT);
        this.mBottom = new ConstraintAnchor(this, ConstraintAnchor.Type.BOTTOM);
        this.mBaseline = new ConstraintAnchor(this, ConstraintAnchor.Type.BASELINE);
        this.mCenterX = new ConstraintAnchor(this, ConstraintAnchor.Type.CENTER_X);
        this.mCenterY = new ConstraintAnchor(this, ConstraintAnchor.Type.CENTER_Y);
        final ConstraintAnchor mCenter = new ConstraintAnchor(this, ConstraintAnchor.Type.CENTER);
        this.mCenter = mCenter;
        this.mListAnchors = new ConstraintAnchor[] { this.mLeft, this.mRight, this.mTop, this.mBottom, this.mBaseline, mCenter };
        this.mAnchors = new ArrayList<ConstraintAnchor>();
        this.mIsInBarrier = new boolean[2];
        this.mListDimensionBehaviors = new DimensionBehaviour[] { DimensionBehaviour.FIXED, DimensionBehaviour.FIXED };
        this.mParent = null;
        this.mDimensionRatio = 0.0f;
        this.mDimensionRatioSide = -1;
        this.mRelX = 0;
        this.mRelY = 0;
        this.mOffsetX = 0;
        this.mOffsetY = 0;
        this.mBaselineDistance = 0;
        final float default_BIAS = ConstraintWidget.DEFAULT_BIAS;
        this.mHorizontalBiasPercent = default_BIAS;
        this.mVerticalBiasPercent = default_BIAS;
        this.mContainerItemSkip = 0;
        this.mVisibility = 0;
        this.mAnimated = false;
        this.mDebugName = null;
        this.mType = null;
        this.mGroupsToSolver = false;
        this.mHorizontalChainStyle = 0;
        this.mVerticalChainStyle = 0;
        this.mWeight = new float[] { -1.0f, -1.0f };
        this.mListNextMatchConstraintsWidget = new ConstraintWidget[] { null, null };
        this.mNextChainWidget = new ConstraintWidget[] { null, null };
        this.mHorizontalNextWidget = null;
        this.mVerticalNextWidget = null;
        this.horizontalGroup = -1;
        this.verticalGroup = -1;
        this.mX = mx;
        this.mY = my;
        this.mWidth = mWidth;
        this.mHeight = mHeight;
        this.addAnchors();
    }
    
    public ConstraintWidget(final String debugName) {
        this.measured = false;
        this.run = new WidgetRun[2];
        this.horizontalRun = null;
        this.verticalRun = null;
        this.isTerminalWidget = new boolean[] { true, true };
        this.mResolvedHasRatio = false;
        this.mMeasureRequested = true;
        this.OPTIMIZE_WRAP = false;
        this.OPTIMIZE_WRAP_ON_RESOLVED = true;
        this.mWidthOverride = -1;
        this.mHeightOverride = -1;
        this.frame = new WidgetFrame(this);
        this.resolvedHorizontal = false;
        this.resolvedVertical = false;
        this.horizontalSolvingPass = false;
        this.verticalSolvingPass = false;
        this.mHorizontalResolution = -1;
        this.mVerticalResolution = -1;
        this.mWrapBehaviorInParent = 0;
        this.mMatchConstraintDefaultWidth = 0;
        this.mMatchConstraintDefaultHeight = 0;
        this.mResolvedMatchConstraintDefault = new int[2];
        this.mMatchConstraintMinWidth = 0;
        this.mMatchConstraintMaxWidth = 0;
        this.mMatchConstraintPercentWidth = 1.0f;
        this.mMatchConstraintMinHeight = 0;
        this.mMatchConstraintMaxHeight = 0;
        this.mMatchConstraintPercentHeight = 1.0f;
        this.mResolvedDimensionRatioSide = -1;
        this.mResolvedDimensionRatio = 1.0f;
        this.mMaxDimension = new int[] { Integer.MAX_VALUE, Integer.MAX_VALUE };
        this.mCircleConstraintAngle = 0.0f;
        this.hasBaseline = false;
        this.mInVirtualLayout = false;
        this.mLastHorizontalMeasureSpec = 0;
        this.mLastVerticalMeasureSpec = 0;
        this.mLeft = new ConstraintAnchor(this, ConstraintAnchor.Type.LEFT);
        this.mTop = new ConstraintAnchor(this, ConstraintAnchor.Type.TOP);
        this.mRight = new ConstraintAnchor(this, ConstraintAnchor.Type.RIGHT);
        this.mBottom = new ConstraintAnchor(this, ConstraintAnchor.Type.BOTTOM);
        this.mBaseline = new ConstraintAnchor(this, ConstraintAnchor.Type.BASELINE);
        this.mCenterX = new ConstraintAnchor(this, ConstraintAnchor.Type.CENTER_X);
        this.mCenterY = new ConstraintAnchor(this, ConstraintAnchor.Type.CENTER_Y);
        final ConstraintAnchor mCenter = new ConstraintAnchor(this, ConstraintAnchor.Type.CENTER);
        this.mCenter = mCenter;
        this.mListAnchors = new ConstraintAnchor[] { this.mLeft, this.mRight, this.mTop, this.mBottom, this.mBaseline, mCenter };
        this.mAnchors = new ArrayList<ConstraintAnchor>();
        this.mIsInBarrier = new boolean[2];
        this.mListDimensionBehaviors = new DimensionBehaviour[] { DimensionBehaviour.FIXED, DimensionBehaviour.FIXED };
        this.mParent = null;
        this.mWidth = 0;
        this.mHeight = 0;
        this.mDimensionRatio = 0.0f;
        this.mDimensionRatioSide = -1;
        this.mX = 0;
        this.mY = 0;
        this.mRelX = 0;
        this.mRelY = 0;
        this.mOffsetX = 0;
        this.mOffsetY = 0;
        this.mBaselineDistance = 0;
        final float default_BIAS = ConstraintWidget.DEFAULT_BIAS;
        this.mHorizontalBiasPercent = default_BIAS;
        this.mVerticalBiasPercent = default_BIAS;
        this.mContainerItemSkip = 0;
        this.mVisibility = 0;
        this.mAnimated = false;
        this.mDebugName = null;
        this.mType = null;
        this.mGroupsToSolver = false;
        this.mHorizontalChainStyle = 0;
        this.mVerticalChainStyle = 0;
        this.mWeight = new float[] { -1.0f, -1.0f };
        this.mListNextMatchConstraintsWidget = new ConstraintWidget[] { null, null };
        this.mNextChainWidget = new ConstraintWidget[] { null, null };
        this.mHorizontalNextWidget = null;
        this.mVerticalNextWidget = null;
        this.horizontalGroup = -1;
        this.verticalGroup = -1;
        this.addAnchors();
        this.setDebugName(debugName);
    }
    
    public ConstraintWidget(final String debugName, final int n, final int n2) {
        this(n, n2);
        this.setDebugName(debugName);
    }
    
    public ConstraintWidget(final String debugName, final int n, final int n2, final int n3, final int n4) {
        this(n, n2, n3, n4);
        this.setDebugName(debugName);
    }
    
    private void addAnchors() {
        this.mAnchors.add(this.mLeft);
        this.mAnchors.add(this.mTop);
        this.mAnchors.add(this.mRight);
        this.mAnchors.add(this.mBottom);
        this.mAnchors.add(this.mCenterX);
        this.mAnchors.add(this.mCenterY);
        this.mAnchors.add(this.mCenter);
        this.mAnchors.add(this.mBaseline);
    }
    
    private void applyConstraints(final LinearSystem linearSystem, final boolean b, boolean b2, final boolean b3, boolean b4, SolverVariable solverVariable, final SolverVariable solverVariable2, final DimensionBehaviour dimensionBehaviour, final boolean b5, final ConstraintAnchor constraintAnchor, final ConstraintAnchor constraintAnchor2, int n, int b6, final int n2, int n3, final float n4, final boolean b7, final boolean b8, final boolean b9, final boolean b10, final boolean b11, int n5, int n6, int n7, int a, final float n8, final boolean b12) {
        final SolverVariable objectVariable = linearSystem.createObjectVariable(constraintAnchor);
        final SolverVariable objectVariable2 = linearSystem.createObjectVariable(constraintAnchor2);
        final SolverVariable objectVariable3 = linearSystem.createObjectVariable(constraintAnchor.getTarget());
        final SolverVariable objectVariable4 = linearSystem.createObjectVariable(constraintAnchor2.getTarget());
        if (LinearSystem.getMetrics() != null) {
            final Metrics metrics = LinearSystem.getMetrics();
            ++metrics.nonresolvedWidgets;
        }
        final int connected = constraintAnchor.isConnected() ? 1 : 0;
        final boolean connected2 = constraintAnchor2.isConnected();
        final boolean connected3 = this.mCenter.isConnected();
        int n9;
        if (connected2) {
            n9 = connected + 1;
        }
        else {
            n9 = connected;
        }
        int n10 = n9;
        if (connected3) {
            n10 = n9 + 1;
        }
        int n11;
        if (b7) {
            n11 = 3;
        }
        else {
            n11 = n5;
        }
        n5 = ConstraintWidget$1.$SwitchMap$androidx$constraintlayout$core$widgets$ConstraintWidget$DimensionBehaviour[dimensionBehaviour.ordinal()];
        if (n5 != 1 && n5 != 2 && n5 != 3 && n5 == 4 && n11 != 4) {
            n5 = 1;
        }
        else {
            n5 = 0;
        }
        int mWidthOverride = this.mWidthOverride;
        if (mWidthOverride != -1 && b) {
            this.mWidthOverride = -1;
            n5 = 0;
        }
        else {
            mWidthOverride = b6;
        }
        final int mHeightOverride = this.mHeightOverride;
        int n12 = mWidthOverride;
        b6 = n5;
        if (mHeightOverride != -1) {
            n12 = mWidthOverride;
            b6 = n5;
            if (!b) {
                this.mHeightOverride = -1;
                n12 = mHeightOverride;
                b6 = 0;
            }
        }
        n5 = n12;
        if (this.mVisibility == 8) {
            n5 = 0;
            b6 = 0;
        }
        if (b12) {
            if (connected == 0 && !connected2 && !connected3) {
                linearSystem.addEquality(objectVariable, n);
            }
            else if (connected != 0 && !connected2) {
                linearSystem.addEquality(objectVariable, objectVariable3, constraintAnchor.getMargin(), 8);
            }
        }
        Label_0878: {
            if (b6 == 0) {
                if (b5) {
                    linearSystem.addEquality(objectVariable2, objectVariable, 0, 3);
                    if (n2 > 0) {
                        linearSystem.addGreaterThan(objectVariable2, objectVariable, n2, 8);
                    }
                    if (n3 < Integer.MAX_VALUE) {
                        linearSystem.addLowerThan(objectVariable2, objectVariable, n3, 8);
                    }
                }
                else {
                    linearSystem.addEquality(objectVariable2, objectVariable, n5, 8);
                }
                n = b6;
            }
            else if (n10 != 2 && !b7 && (n11 == 1 || n11 == 0)) {
                b6 = (n = Math.max(n7, n5));
                if (a > 0) {
                    n = Math.min(a, b6);
                }
                linearSystem.addEquality(objectVariable2, objectVariable, n, 8);
                n = 0;
            }
            else {
                if (n7 == -2) {
                    n = n5;
                }
                else {
                    n = n7;
                }
                if (a == -2) {
                    n3 = n5;
                }
                else {
                    n3 = a;
                }
                n7 = n5;
                if (n5 > 0) {
                    n7 = n5;
                    if (n11 != 1) {
                        n7 = 0;
                    }
                }
                n5 = n7;
                if (n > 0) {
                    linearSystem.addGreaterThan(objectVariable2, objectVariable, n, 8);
                    n5 = Math.max(n7, n);
                }
                if (n3 > 0) {
                    if (b2 && n11 == 1) {
                        n7 = 0;
                    }
                    else {
                        n7 = 1;
                    }
                    if (n7 != 0) {
                        linearSystem.addLowerThan(objectVariable2, objectVariable, n3, 8);
                    }
                    n5 = Math.min(n5, n3);
                }
                if (n11 == 1) {
                    if (b2) {
                        linearSystem.addEquality(objectVariable2, objectVariable, n5, 8);
                    }
                    else if (b9) {
                        linearSystem.addEquality(objectVariable2, objectVariable, n5, 5);
                        linearSystem.addLowerThan(objectVariable2, objectVariable, n5, 8);
                    }
                    else {
                        linearSystem.addEquality(objectVariable2, objectVariable, n5, 5);
                        linearSystem.addLowerThan(objectVariable2, objectVariable, n5, 8);
                    }
                    n7 = n;
                    a = b6;
                    break Label_0878;
                }
                if (n11 == 2) {
                    SolverVariable solverVariable3;
                    SolverVariable solverVariable4;
                    if (constraintAnchor.getType() != ConstraintAnchor.Type.TOP && constraintAnchor.getType() != ConstraintAnchor.Type.BOTTOM) {
                        solverVariable3 = linearSystem.createObjectVariable(this.mParent.getAnchor(ConstraintAnchor.Type.LEFT));
                        solverVariable4 = linearSystem.createObjectVariable(this.mParent.getAnchor(ConstraintAnchor.Type.RIGHT));
                    }
                    else {
                        solverVariable3 = linearSystem.createObjectVariable(this.mParent.getAnchor(ConstraintAnchor.Type.TOP));
                        solverVariable4 = linearSystem.createObjectVariable(this.mParent.getAnchor(ConstraintAnchor.Type.BOTTOM));
                    }
                    linearSystem.addConstraint(linearSystem.createRow().createRowDimensionRatio(objectVariable2, objectVariable, solverVariable4, solverVariable3, n8));
                    if (b2) {
                        b6 = 0;
                    }
                    n7 = n;
                    a = b6;
                    break Label_0878;
                }
                b4 = true;
                a = b6;
                n7 = n;
                break Label_0878;
            }
            n3 = a;
            a = n;
        }
        if (b12 && !b9) {
            Label_2259: {
                Label_2252: {
                    Label_2248: {
                        if (connected != 0 || connected2 || connected3) {
                            if (connected != 0 && !connected2) {
                                final ConstraintWidget mOwner = constraintAnchor.mTarget.mOwner;
                                if (b2 && mOwner instanceof Barrier) {
                                    n = 8;
                                }
                                else {
                                    n = 5;
                                }
                                solverVariable = objectVariable2;
                                break Label_2259;
                            }
                            if (connected == 0 && connected2) {
                                linearSystem.addEquality(objectVariable2, objectVariable4, -constraintAnchor2.getMargin(), 8);
                                if (b2) {
                                    if (this.OPTIMIZE_WRAP && objectVariable.isFinalValue) {
                                        final ConstraintWidget mParent = this.mParent;
                                        if (mParent != null) {
                                            final ConstraintWidgetContainer constraintWidgetContainer = (ConstraintWidgetContainer)mParent;
                                            if (b) {
                                                constraintWidgetContainer.addHorizontalWrapMinVariable(constraintAnchor);
                                                break Label_2248;
                                            }
                                            constraintWidgetContainer.addVerticalWrapMinVariable(constraintAnchor);
                                            break Label_2248;
                                        }
                                    }
                                    linearSystem.addGreaterThan(objectVariable, solverVariable, 0, 5);
                                }
                            }
                            else if (connected != 0 && connected2) {
                                final ConstraintWidget mOwner2 = constraintAnchor.mTarget.mOwner;
                                final ConstraintWidget mOwner3 = constraintAnchor2.mTarget.mOwner;
                                final ConstraintWidget parent = this.getParent();
                                int n13 = 0;
                                Label_1650: {
                                    Label_1643: {
                                        Label_1637: {
                                            Label_1634: {
                                                Label_1298: {
                                                    if (a != 0) {
                                                        Label_1239: {
                                                            if (n11 == 0) {
                                                                if (n3 == 0 && n7 == 0) {
                                                                    if (objectVariable3.isFinalValue && objectVariable4.isFinalValue) {
                                                                        linearSystem.addEquality(objectVariable, objectVariable3, constraintAnchor.getMargin(), 8);
                                                                        linearSystem.addEquality(objectVariable2, objectVariable4, -constraintAnchor2.getMargin(), 8);
                                                                        return;
                                                                    }
                                                                    n = 8;
                                                                    n6 = 8;
                                                                    b6 = 0;
                                                                    n3 = 1;
                                                                    n5 = 0;
                                                                }
                                                                else {
                                                                    n = 5;
                                                                    n6 = 5;
                                                                    b6 = 1;
                                                                    n3 = 0;
                                                                    n5 = 1;
                                                                }
                                                                if (mOwner2 instanceof Barrier || mOwner3 instanceof Barrier) {
                                                                    n6 = n3;
                                                                    n3 = 6;
                                                                    n13 = b6;
                                                                    b6 = 4;
                                                                    break Label_1650;
                                                                }
                                                                final int n14 = n3;
                                                                n3 = n6;
                                                                n6 = n14;
                                                                n13 = b6;
                                                                b6 = n3;
                                                            }
                                                            else if (n11 == 2) {
                                                                if (mOwner2 instanceof Barrier) {
                                                                    break Label_1298;
                                                                }
                                                                if (mOwner3 instanceof Barrier) {
                                                                    break Label_1298;
                                                                }
                                                                n = 5;
                                                                b6 = 5;
                                                                break Label_1637;
                                                            }
                                                            else {
                                                                if (n11 == 1) {
                                                                    n = 8;
                                                                    break Label_1634;
                                                                }
                                                                if (n11 == 3) {
                                                                    Label_1368: {
                                                                        if (this.mResolvedDimensionRatioSide == -1) {
                                                                            if (b10) {
                                                                                if (b2) {
                                                                                    n = 5;
                                                                                }
                                                                                else {
                                                                                    n = 4;
                                                                                }
                                                                            }
                                                                            else {
                                                                                n = 8;
                                                                            }
                                                                            n5 = 8;
                                                                            n3 = n;
                                                                        }
                                                                        else {
                                                                            if (b7) {
                                                                                if (n6 != 2 && n6 != 1) {
                                                                                    n = 0;
                                                                                }
                                                                                else {
                                                                                    n = 1;
                                                                                }
                                                                                if (n == 0) {
                                                                                    n = 8;
                                                                                    b6 = 5;
                                                                                }
                                                                                else {
                                                                                    n = 5;
                                                                                    b6 = 4;
                                                                                }
                                                                                n13 = 1;
                                                                                n5 = 1;
                                                                                n6 = 1;
                                                                                break Label_1239;
                                                                            }
                                                                            if (n3 <= 0) {
                                                                                if (n3 == 0 && n7 == 0) {
                                                                                    if (!b10) {
                                                                                        n3 = 6;
                                                                                        n = 5;
                                                                                        b6 = 8;
                                                                                        break Label_1368;
                                                                                    }
                                                                                    if (mOwner2 != parent && mOwner3 != parent) {
                                                                                        n = 4;
                                                                                    }
                                                                                    else {
                                                                                        n = 5;
                                                                                    }
                                                                                }
                                                                                else {
                                                                                    n = 5;
                                                                                }
                                                                                n3 = 6;
                                                                                b6 = 4;
                                                                                break Label_1368;
                                                                            }
                                                                            n3 = 6;
                                                                            n5 = 5;
                                                                        }
                                                                        b6 = 5;
                                                                        n = n5;
                                                                    }
                                                                    n13 = 1;
                                                                    n5 = 1;
                                                                    n6 = 1;
                                                                    break Label_1650;
                                                                }
                                                                n = 5;
                                                                b6 = 4;
                                                                n13 = 0;
                                                                n5 = 0;
                                                                break Label_1643;
                                                            }
                                                        }
                                                        n3 = 6;
                                                        break Label_1650;
                                                    }
                                                    if (objectVariable3.isFinalValue && objectVariable4.isFinalValue) {
                                                        linearSystem.addCentering(objectVariable, objectVariable3, constraintAnchor.getMargin(), n4, objectVariable4, objectVariable2, constraintAnchor2.getMargin(), 8);
                                                        if (b2 && b4) {
                                                            if (constraintAnchor2.mTarget != null) {
                                                                n = constraintAnchor2.getMargin();
                                                            }
                                                            else {
                                                                n = 0;
                                                            }
                                                            if (objectVariable4 != solverVariable2) {
                                                                linearSystem.addGreaterThan(solverVariable2, objectVariable2, n, 5);
                                                            }
                                                        }
                                                        return;
                                                    }
                                                }
                                                n = 5;
                                            }
                                            b6 = 4;
                                        }
                                        n13 = 1;
                                        n5 = 1;
                                    }
                                    n3 = 6;
                                    n6 = 0;
                                }
                                int n15;
                                if (n5 != 0 && objectVariable3 == objectVariable4 && mOwner2 != parent) {
                                    n5 = 0;
                                    n15 = 0;
                                }
                                else {
                                    n15 = 1;
                                }
                                if (n13 != 0) {
                                    int n16;
                                    if (a == 0 && !b8 && !b10 && objectVariable3 == solverVariable && objectVariable4 == solverVariable2) {
                                        b2 = false;
                                        n3 = 8;
                                        n16 = 8;
                                        n = 0;
                                    }
                                    else {
                                        final int n17 = n15;
                                        final int n18 = n;
                                        n = n17;
                                        n16 = n3;
                                        n3 = n18;
                                    }
                                    linearSystem.addCentering(objectVariable, objectVariable3, constraintAnchor.getMargin(), n4, objectVariable4, objectVariable2, constraintAnchor2.getMargin(), n16);
                                    n15 = n;
                                    n = n3;
                                }
                                if (this.mVisibility == 8 && !constraintAnchor2.hasDependents()) {
                                    return;
                                }
                                if (n5 != 0) {
                                    n3 = n;
                                    Label_1863: {
                                        if (b2) {
                                            n3 = n;
                                            if (objectVariable3 != objectVariable4) {
                                                n3 = n;
                                                if (a == 0) {
                                                    if (!(mOwner2 instanceof Barrier)) {
                                                        n3 = n;
                                                        if (!(mOwner3 instanceof Barrier)) {
                                                            break Label_1863;
                                                        }
                                                    }
                                                    n3 = 6;
                                                }
                                            }
                                        }
                                    }
                                    linearSystem.addGreaterThan(objectVariable, objectVariable3, constraintAnchor.getMargin(), n3);
                                    linearSystem.addLowerThan(objectVariable2, objectVariable4, -constraintAnchor2.getMargin(), n3);
                                }
                                else {
                                    n3 = n;
                                }
                                final SolverVariable solverVariable5 = objectVariable2;
                                if (b2 && b11 && !(mOwner2 instanceof Barrier) && !(mOwner3 instanceof Barrier) && mOwner3 != parent) {
                                    n3 = 6;
                                    n = 6;
                                    n15 = 1;
                                }
                                else {
                                    n = b6;
                                }
                                if (n15 != 0) {
                                    b6 = n;
                                    Label_2066: {
                                        if (n6 != 0) {
                                            if (b10) {
                                                b6 = n;
                                                if (!b3) {
                                                    break Label_2066;
                                                }
                                            }
                                            if (mOwner2 != parent && mOwner3 != parent) {
                                                b6 = n;
                                            }
                                            else {
                                                b6 = 6;
                                            }
                                            if (mOwner2 instanceof Guideline || mOwner3 instanceof Guideline) {
                                                b6 = 5;
                                            }
                                            if (mOwner2 instanceof Barrier || mOwner3 instanceof Barrier) {
                                                b6 = 5;
                                            }
                                            if (b10) {
                                                b6 = 5;
                                            }
                                            b6 = Math.max(b6, n);
                                        }
                                    }
                                    n = b6;
                                    if (b2) {
                                        n = Math.min(n3, b6);
                                        if (b7 && !b10 && (mOwner2 == parent || mOwner3 == parent)) {
                                            n = 4;
                                        }
                                    }
                                    linearSystem.addEquality(objectVariable, objectVariable3, constraintAnchor.getMargin(), n);
                                    linearSystem.addEquality(solverVariable5, objectVariable4, -constraintAnchor2.getMargin(), n);
                                }
                                if (b2) {
                                    if (solverVariable == objectVariable3) {
                                        n = constraintAnchor.getMargin();
                                    }
                                    else {
                                        n = 0;
                                    }
                                    if (objectVariable3 != solverVariable) {
                                        linearSystem.addGreaterThan(objectVariable, solverVariable, n, 5);
                                    }
                                }
                                if (!b2 || a == 0 || n2 != 0 || n7 != 0) {
                                    break Label_2252;
                                }
                                if (a != 0 && n11 == 3) {
                                    linearSystem.addGreaterThan(solverVariable5, objectVariable, 0, 8);
                                    break Label_2252;
                                }
                                linearSystem.addGreaterThan(solverVariable5, objectVariable, 0, 5);
                                break Label_2252;
                            }
                        }
                    }
                    solverVariable = objectVariable2;
                }
                solverVariable = objectVariable2;
                n = 5;
            }
            if (b2 && b4) {
                if (constraintAnchor2.mTarget != null) {
                    b6 = constraintAnchor2.getMargin();
                }
                else {
                    b6 = 0;
                }
                if (objectVariable4 != solverVariable2) {
                    if (this.OPTIMIZE_WRAP && objectVariable2.isFinalValue) {
                        final ConstraintWidget mParent2 = this.mParent;
                        if (mParent2 != null) {
                            final ConstraintWidgetContainer constraintWidgetContainer2 = (ConstraintWidgetContainer)mParent2;
                            if (b) {
                                constraintWidgetContainer2.addHorizontalWrapMaxVariable(constraintAnchor2);
                            }
                            else {
                                constraintWidgetContainer2.addVerticalWrapMaxVariable(constraintAnchor2);
                            }
                            return;
                        }
                    }
                    linearSystem.addGreaterThan(solverVariable2, objectVariable2, b6, n);
                }
            }
            return;
        }
        if (n10 < 2 && b2 && b4) {
            linearSystem.addGreaterThan(objectVariable, solverVariable, 0, 8);
            if (!b && this.mBaseline.mTarget != null) {
                n = 0;
            }
            else {
                n = 1;
            }
            if (!b && this.mBaseline.mTarget != null) {
                final ConstraintWidget mOwner4 = this.mBaseline.mTarget.mOwner;
                if (mOwner4.mDimensionRatio != 0.0f && mOwner4.mListDimensionBehaviors[0] == DimensionBehaviour.MATCH_CONSTRAINT && mOwner4.mListDimensionBehaviors[1] == DimensionBehaviour.MATCH_CONSTRAINT) {
                    n = 1;
                }
                else {
                    n = 0;
                }
            }
            if (n != 0) {
                linearSystem.addGreaterThan(solverVariable2, objectVariable2, 0, 8);
            }
        }
    }
    
    private void getSceneString(final StringBuilder sb, final String str, final int n, final int n2, final int n3, final int n4, final int n5, final int n6, final float n7, final float n8) {
        sb.append(str);
        sb.append(" :  {\n");
        this.serializeAttribute(sb, "      size", n, 0);
        this.serializeAttribute(sb, "      min", n2, 0);
        this.serializeAttribute(sb, "      max", n3, Integer.MAX_VALUE);
        this.serializeAttribute(sb, "      matchMin", n5, 0);
        this.serializeAttribute(sb, "      matchDef", n6, 0);
        this.serializeAttribute(sb, "      matchPercent", n7, 1.0f);
        sb.append("    },\n");
    }
    
    private void getSceneString(final StringBuilder sb, final String str, final ConstraintAnchor constraintAnchor) {
        if (constraintAnchor.mTarget == null) {
            return;
        }
        sb.append("    ");
        sb.append(str);
        sb.append(" : [ '");
        sb.append(constraintAnchor.mTarget);
        sb.append("'");
        if (constraintAnchor.mGoneMargin != Integer.MIN_VALUE || constraintAnchor.mMargin != 0) {
            sb.append(",");
            sb.append(constraintAnchor.mMargin);
            if (constraintAnchor.mGoneMargin != Integer.MIN_VALUE) {
                sb.append(",");
                sb.append(constraintAnchor.mGoneMargin);
                sb.append(",");
            }
        }
        sb.append(" ] ,\n");
    }
    
    private boolean isChainHead(int n) {
        n *= 2;
        final ConstraintAnchor mTarget = this.mListAnchors[n].mTarget;
        boolean b = true;
        if (mTarget != null) {
            final ConstraintAnchor mTarget2 = this.mListAnchors[n].mTarget.mTarget;
            final ConstraintAnchor[] mListAnchors = this.mListAnchors;
            if (mTarget2 != mListAnchors[n]) {
                ++n;
                if (mListAnchors[n].mTarget != null && this.mListAnchors[n].mTarget.mTarget == this.mListAnchors[n]) {
                    return b;
                }
            }
        }
        b = false;
        return b;
    }
    
    private void serializeAnchor(final StringBuilder sb, final String str, final ConstraintAnchor constraintAnchor) {
        if (constraintAnchor.mTarget == null) {
            return;
        }
        sb.append(str);
        sb.append(" : [ '");
        sb.append(constraintAnchor.mTarget);
        sb.append("',");
        sb.append(constraintAnchor.mMargin);
        sb.append(",");
        sb.append(constraintAnchor.mGoneMargin);
        sb.append(",");
        sb.append(" ] ,\n");
    }
    
    private void serializeAttribute(final StringBuilder sb, final String str, final float f, final float n) {
        if (f == n) {
            return;
        }
        sb.append(str);
        sb.append(" :   ");
        sb.append(f);
        sb.append(",\n");
    }
    
    private void serializeAttribute(final StringBuilder sb, final String str, final int i, final int n) {
        if (i == n) {
            return;
        }
        sb.append(str);
        sb.append(" :   ");
        sb.append(i);
        sb.append(",\n");
    }
    
    private void serializeCircle(final StringBuilder sb, final ConstraintAnchor constraintAnchor, final float f) {
        if (constraintAnchor.mTarget == null) {
            return;
        }
        sb.append("circle : [ '");
        sb.append(constraintAnchor.mTarget);
        sb.append("',");
        sb.append(constraintAnchor.mMargin);
        sb.append(",");
        sb.append(f);
        sb.append(",");
        sb.append(" ] ,\n");
    }
    
    private void serializeDimensionRatio(final StringBuilder sb, final String str, final float f, final int i) {
        if (f == 0.0f) {
            return;
        }
        sb.append(str);
        sb.append(" :  [");
        sb.append(f);
        sb.append(",");
        sb.append(i);
        sb.append("");
        sb.append("],\n");
    }
    
    private void serializeSize(final StringBuilder sb, final String str, final int n, final int n2, final int n3, final int n4, final int n5, final int n6, final float n7, final float n8) {
        sb.append(str);
        sb.append(" :  {\n");
        this.serializeAttribute(sb, "size", n, Integer.MIN_VALUE);
        this.serializeAttribute(sb, "min", n2, 0);
        this.serializeAttribute(sb, "max", n3, Integer.MAX_VALUE);
        this.serializeAttribute(sb, "matchMin", n5, 0);
        this.serializeAttribute(sb, "matchDef", n6, 0);
        this.serializeAttribute(sb, "matchPercent", n6, 1);
        sb.append("},\n");
    }
    
    public void addChildrenToSolverByDependency(final ConstraintWidgetContainer constraintWidgetContainer, final LinearSystem linearSystem, final HashSet<ConstraintWidget> set, final int n, final boolean b) {
        if (b) {
            if (!set.contains(this)) {
                return;
            }
            Optimizer.checkMatchParent(constraintWidgetContainer, linearSystem, this);
            set.remove(this);
            this.addToSolver(linearSystem, constraintWidgetContainer.optimizeFor(64));
        }
        Object o;
        if (n == 0) {
            final HashSet<ConstraintAnchor> dependents = this.mLeft.getDependents();
            if (dependents != null) {
                final Iterator<ConstraintAnchor> iterator = dependents.iterator();
                while (iterator.hasNext()) {
                    iterator.next().mOwner.addChildrenToSolverByDependency(constraintWidgetContainer, linearSystem, set, n, true);
                }
            }
            o = this.mRight.getDependents();
            if (o != null) {
                o = ((HashSet<Object>)o).iterator();
                while (((Iterator)o).hasNext()) {
                    ((Iterator<ConstraintAnchor>)o).next().mOwner.addChildrenToSolverByDependency(constraintWidgetContainer, linearSystem, set, n, true);
                }
            }
            return;
        }
        else {
            final HashSet<ConstraintAnchor> dependents2 = this.mTop.getDependents();
            if (dependents2 != null) {
                final Iterator<ConstraintAnchor> iterator2 = dependents2.iterator();
                while (iterator2.hasNext()) {
                    iterator2.next().mOwner.addChildrenToSolverByDependency(constraintWidgetContainer, linearSystem, set, n, true);
                }
            }
            final HashSet<ConstraintAnchor> dependents3 = this.mBottom.getDependents();
            if (dependents3 != null) {
                final Iterator<ConstraintAnchor> iterator3 = dependents3.iterator();
                while (iterator3.hasNext()) {
                    iterator3.next().mOwner.addChildrenToSolverByDependency(constraintWidgetContainer, linearSystem, set, n, true);
                }
            }
            o = this.mBaseline.getDependents();
            if (o == null) {
                return;
            }
            o = ((HashSet<ConstraintAnchor>)o).iterator();
        }
        while (true) {
            if (!((Iterator)o).hasNext()) {
                return;
            }
            final ConstraintWidget mOwner = ((Iterator<ConstraintAnchor>)o).next().mOwner;
            try {
                mOwner.addChildrenToSolverByDependency(constraintWidgetContainer, linearSystem, set, n, true);
                continue;
            }
            finally {}
            break;
        }
    }
    
    boolean addFirst() {
        return this instanceof VirtualLayout || this instanceof Guideline;
    }
    
    public void addToSolver(final LinearSystem linearSystem, final boolean b) {
        final SolverVariable objectVariable = linearSystem.createObjectVariable(this.mLeft);
        final SolverVariable objectVariable2 = linearSystem.createObjectVariable(this.mRight);
        final SolverVariable objectVariable3 = linearSystem.createObjectVariable(this.mTop);
        final SolverVariable objectVariable4 = linearSystem.createObjectVariable(this.mBottom);
        final SolverVariable objectVariable5 = linearSystem.createObjectVariable(this.mBaseline);
        final ConstraintWidget mParent = this.mParent;
        boolean b4 = false;
        boolean b5 = false;
        Label_0179: {
            Label_0176: {
                if (mParent != null) {
                    final boolean b2 = mParent != null && mParent.mListDimensionBehaviors[0] == DimensionBehaviour.WRAP_CONTENT;
                    final ConstraintWidget mParent2 = this.mParent;
                    final boolean b3 = mParent2 != null && mParent2.mListDimensionBehaviors[1] == DimensionBehaviour.WRAP_CONTENT;
                    final int mWrapBehaviorInParent = this.mWrapBehaviorInParent;
                    if (mWrapBehaviorInParent == 1) {
                        b4 = b2;
                        break Label_0176;
                    }
                    if (mWrapBehaviorInParent == 2) {
                        b5 = b3;
                        b4 = false;
                        break Label_0179;
                    }
                    if (mWrapBehaviorInParent != 3) {
                        b5 = b3;
                        b4 = b2;
                        break Label_0179;
                    }
                }
                b4 = false;
            }
            b5 = false;
        }
        if (this.mVisibility == 8 && !this.mAnimated && !this.hasDependencies()) {
            final boolean[] mIsInBarrier = this.mIsInBarrier;
            if (!mIsInBarrier[0] && !mIsInBarrier[1]) {
                return;
            }
        }
        final boolean resolvedHorizontal = this.resolvedHorizontal;
        if (resolvedHorizontal || this.resolvedVertical) {
            if (resolvedHorizontal) {
                linearSystem.addEquality(objectVariable, this.mX);
                linearSystem.addEquality(objectVariable2, this.mX + this.mWidth);
                if (b4) {
                    final ConstraintWidget mParent3 = this.mParent;
                    if (mParent3 != null) {
                        if (this.OPTIMIZE_WRAP_ON_RESOLVED) {
                            final ConstraintWidgetContainer constraintWidgetContainer = (ConstraintWidgetContainer)mParent3;
                            constraintWidgetContainer.addHorizontalWrapMinVariable(this.mLeft);
                            constraintWidgetContainer.addHorizontalWrapMaxVariable(this.mRight);
                        }
                        else {
                            linearSystem.addGreaterThan(linearSystem.createObjectVariable(mParent3.mRight), objectVariable2, 0, 5);
                        }
                    }
                }
            }
            if (this.resolvedVertical) {
                linearSystem.addEquality(objectVariable3, this.mY);
                linearSystem.addEquality(objectVariable4, this.mY + this.mHeight);
                if (this.mBaseline.hasDependents()) {
                    linearSystem.addEquality(objectVariable5, this.mY + this.mBaselineDistance);
                }
                if (b5) {
                    final ConstraintWidget mParent4 = this.mParent;
                    if (mParent4 != null) {
                        if (this.OPTIMIZE_WRAP_ON_RESOLVED) {
                            final ConstraintWidgetContainer constraintWidgetContainer2 = (ConstraintWidgetContainer)mParent4;
                            constraintWidgetContainer2.addVerticalWrapMinVariable(this.mTop);
                            constraintWidgetContainer2.addVerticalWrapMaxVariable(this.mBottom);
                        }
                        else {
                            linearSystem.addGreaterThan(linearSystem.createObjectVariable(mParent4.mBottom), objectVariable4, 0, 5);
                        }
                    }
                }
            }
            if (this.resolvedHorizontal && this.resolvedVertical) {
                this.resolvedHorizontal = false;
                this.resolvedVertical = false;
                return;
            }
        }
        if (LinearSystem.sMetrics != null) {
            final Metrics sMetrics = LinearSystem.sMetrics;
            ++sMetrics.widgets;
        }
        if (b) {
            final HorizontalWidgetRun horizontalRun = this.horizontalRun;
            if (horizontalRun != null && this.verticalRun != null && horizontalRun.start.resolved && this.horizontalRun.end.resolved && this.verticalRun.start.resolved && this.verticalRun.end.resolved) {
                if (LinearSystem.sMetrics != null) {
                    final Metrics sMetrics2 = LinearSystem.sMetrics;
                    ++sMetrics2.graphSolved;
                }
                linearSystem.addEquality(objectVariable, this.horizontalRun.start.value);
                linearSystem.addEquality(objectVariable2, this.horizontalRun.end.value);
                linearSystem.addEquality(objectVariable3, this.verticalRun.start.value);
                linearSystem.addEquality(objectVariable4, this.verticalRun.end.value);
                linearSystem.addEquality(objectVariable5, this.verticalRun.baseline.value);
                if (this.mParent != null) {
                    if (b4 && this.isTerminalWidget[0] && !this.isInHorizontalChain()) {
                        linearSystem.addGreaterThan(linearSystem.createObjectVariable(this.mParent.mRight), objectVariable2, 0, 8);
                    }
                    if (b5 && this.isTerminalWidget[1] && !this.isInVerticalChain()) {
                        linearSystem.addGreaterThan(linearSystem.createObjectVariable(this.mParent.mBottom), objectVariable4, 0, 8);
                    }
                }
                this.resolvedHorizontal = false;
                this.resolvedVertical = false;
                return;
            }
        }
        if (LinearSystem.sMetrics != null) {
            final Metrics sMetrics3 = LinearSystem.sMetrics;
            ++sMetrics3.linearSolved;
        }
        boolean inVerticalChain;
        int n;
        if (this.mParent != null) {
            int inHorizontalChain;
            if (this.isChainHead(0)) {
                ((ConstraintWidgetContainer)this.mParent).addChain(this, 0);
                inHorizontalChain = 1;
            }
            else {
                inHorizontalChain = (this.isInHorizontalChain() ? 1 : 0);
            }
            if (this.isChainHead(1)) {
                ((ConstraintWidgetContainer)this.mParent).addChain(this, 1);
                inVerticalChain = true;
            }
            else {
                inVerticalChain = this.isInVerticalChain();
            }
            if (inHorizontalChain == 0 && b4 && this.mVisibility != 8 && this.mLeft.mTarget == null && this.mRight.mTarget == null) {
                linearSystem.addGreaterThan(linearSystem.createObjectVariable(this.mParent.mRight), objectVariable2, 0, 1);
            }
            if (!inVerticalChain && b5 && this.mVisibility != 8 && this.mTop.mTarget == null && this.mBottom.mTarget == null && this.mBaseline == null) {
                linearSystem.addGreaterThan(linearSystem.createObjectVariable(this.mParent.mBottom), objectVariable4, 0, 1);
            }
            n = inHorizontalChain;
        }
        else {
            inVerticalChain = false;
            n = 0;
        }
        final int mWidth = this.mWidth;
        final int mMinWidth = this.mMinWidth;
        int n2 = mWidth;
        if (mWidth < mMinWidth) {
            n2 = mMinWidth;
        }
        final int mHeight = this.mHeight;
        final int mMinHeight = this.mMinHeight;
        int n3;
        if ((n3 = mHeight) < mMinHeight) {
            n3 = mMinHeight;
        }
        final boolean b6 = this.mListDimensionBehaviors[0] != DimensionBehaviour.MATCH_CONSTRAINT;
        final boolean b7 = this.mListDimensionBehaviors[1] != DimensionBehaviour.MATCH_CONSTRAINT;
        this.mResolvedDimensionRatioSide = this.mDimensionRatioSide;
        final float mDimensionRatio = this.mDimensionRatio;
        this.mResolvedDimensionRatio = mDimensionRatio;
        int mMatchConstraintDefaultWidth = this.mMatchConstraintDefaultWidth;
        final int mMatchConstraintDefaultHeight = this.mMatchConstraintDefaultHeight;
        int n8 = 0;
        boolean mResolvedHasRatio = false;
        int n10 = 0;
        int n11 = 0;
        Label_1505: {
            if (mDimensionRatio > 0.0f && this.mVisibility != 8) {
                int n4 = mMatchConstraintDefaultWidth;
                if (this.mListDimensionBehaviors[0] == DimensionBehaviour.MATCH_CONSTRAINT && (n4 = mMatchConstraintDefaultWidth) == 0) {
                    n4 = 3;
                }
                int n5 = mMatchConstraintDefaultHeight;
                if (this.mListDimensionBehaviors[1] == DimensionBehaviour.MATCH_CONSTRAINT && (n5 = mMatchConstraintDefaultHeight) == 0) {
                    n5 = 3;
                }
                Label_1468: {
                    int n6;
                    if (this.mListDimensionBehaviors[0] == DimensionBehaviour.MATCH_CONSTRAINT && this.mListDimensionBehaviors[1] == DimensionBehaviour.MATCH_CONSTRAINT && n4 == 3 && n5 == 3) {
                        this.setupDimensionRatio(b4, b5, b6, b7);
                        n6 = n3;
                    }
                    else if (this.mListDimensionBehaviors[0] == DimensionBehaviour.MATCH_CONSTRAINT && n4 == 3) {
                        this.mResolvedDimensionRatioSide = 0;
                        final int n7 = (int)(this.mResolvedDimensionRatio * this.mHeight);
                        final DimensionBehaviour dimensionBehaviour = this.mListDimensionBehaviors[1];
                        final DimensionBehaviour match_CONSTRAINT = DimensionBehaviour.MATCH_CONSTRAINT;
                        n8 = n3;
                        final int n9 = n5;
                        if (dimensionBehaviour != match_CONSTRAINT) {
                            mResolvedHasRatio = false;
                            mMatchConstraintDefaultWidth = 4;
                            n10 = n7;
                            n11 = n9;
                            break Label_1505;
                        }
                        n10 = n7;
                        break Label_1468;
                    }
                    else {
                        n6 = n3;
                        if (this.mListDimensionBehaviors[1] == DimensionBehaviour.MATCH_CONSTRAINT) {
                            n6 = n3;
                            if (n5 == 3) {
                                this.mResolvedDimensionRatioSide = 1;
                                if (this.mDimensionRatioSide == -1) {
                                    this.mResolvedDimensionRatio = 1.0f / this.mResolvedDimensionRatio;
                                }
                                n6 = (int)(this.mResolvedDimensionRatio * this.mWidth);
                                if (this.mListDimensionBehaviors[0] != DimensionBehaviour.MATCH_CONSTRAINT) {
                                    final int n12 = n6;
                                    mMatchConstraintDefaultWidth = n4;
                                    mResolvedHasRatio = false;
                                    n11 = 4;
                                    n10 = n2;
                                    n8 = n12;
                                    break Label_1505;
                                }
                            }
                        }
                    }
                    n10 = n2;
                    n8 = n6;
                }
                final int n13 = n4;
                n11 = n5;
                mResolvedHasRatio = true;
                mMatchConstraintDefaultWidth = n13;
            }
            else {
                n11 = mMatchConstraintDefaultHeight;
                final int n14 = n2;
                mResolvedHasRatio = false;
                n8 = n3;
                n10 = n14;
            }
        }
        final int[] mResolvedMatchConstraintDefault = this.mResolvedMatchConstraintDefault;
        mResolvedMatchConstraintDefault[0] = mMatchConstraintDefaultWidth;
        mResolvedMatchConstraintDefault[1] = n11;
        this.mResolvedHasRatio = mResolvedHasRatio;
        boolean b8 = false;
        Label_1560: {
            if (mResolvedHasRatio) {
                final int mResolvedDimensionRatioSide = this.mResolvedDimensionRatioSide;
                if (mResolvedDimensionRatioSide == 0 || mResolvedDimensionRatioSide == -1) {
                    b8 = true;
                    break Label_1560;
                }
            }
            b8 = false;
        }
        boolean b9 = false;
        Label_1592: {
            if (mResolvedHasRatio) {
                final int mResolvedDimensionRatioSide2 = this.mResolvedDimensionRatioSide;
                if (mResolvedDimensionRatioSide2 == 1 || mResolvedDimensionRatioSide2 == -1) {
                    b9 = true;
                    break Label_1592;
                }
            }
            b9 = false;
        }
        final boolean b10 = this.mListDimensionBehaviors[0] == DimensionBehaviour.WRAP_CONTENT && this instanceof ConstraintWidgetContainer;
        if (b10) {
            n10 = 0;
        }
        final boolean b11 = this.mCenter.isConnected() ^ true;
        final boolean[] mIsInBarrier2 = this.mIsInBarrier;
        final boolean b12 = mIsInBarrier2[0];
        final boolean b13 = mIsInBarrier2[1];
        Label_2000: {
            if (this.mHorizontalResolution != 2 && !this.resolvedHorizontal) {
                if (b) {
                    final HorizontalWidgetRun horizontalRun2 = this.horizontalRun;
                    if (horizontalRun2 != null && horizontalRun2.start.resolved) {
                        if (this.horizontalRun.end.resolved) {
                            if (!b) {
                                break Label_2000;
                            }
                            linearSystem.addEquality(objectVariable, this.horizontalRun.start.value);
                            linearSystem.addEquality(objectVariable2, this.horizontalRun.end.value);
                            if (this.mParent != null && b4 && this.isTerminalWidget[0] && !this.isInHorizontalChain()) {
                                linearSystem.addGreaterThan(linearSystem.createObjectVariable(this.mParent.mRight), objectVariable2, 0, 8);
                            }
                            break Label_2000;
                        }
                    }
                }
                final ConstraintWidget mParent5 = this.mParent;
                SolverVariable objectVariable6;
                if (mParent5 != null) {
                    objectVariable6 = linearSystem.createObjectVariable(mParent5.mRight);
                }
                else {
                    objectVariable6 = null;
                }
                final ConstraintWidget mParent6 = this.mParent;
                SolverVariable objectVariable7;
                if (mParent6 != null) {
                    objectVariable7 = linearSystem.createObjectVariable(mParent6.mLeft);
                }
                else {
                    objectVariable7 = null;
                }
                final boolean b14 = this.isTerminalWidget[0];
                final DimensionBehaviour[] mListDimensionBehaviors = this.mListDimensionBehaviors;
                this.applyConstraints(linearSystem, true, b4, b5, b14, objectVariable7, objectVariable6, mListDimensionBehaviors[0], b10, this.mLeft, this.mRight, this.mX, n10, this.mMinWidth, this.mMaxDimension[0], this.mHorizontalBiasPercent, b8, mListDimensionBehaviors[1] == DimensionBehaviour.MATCH_CONSTRAINT, (boolean)(n != 0), inVerticalChain, b12, mMatchConstraintDefaultWidth, n11, this.mMatchConstraintMinWidth, this.mMatchConstraintMaxWidth, this.mMatchConstraintPercentWidth, b11);
            }
        }
        boolean b15 = false;
        Label_2161: {
            if (b) {
                final VerticalWidgetRun verticalRun = this.verticalRun;
                if (verticalRun != null && verticalRun.start.resolved && this.verticalRun.end.resolved) {
                    linearSystem.addEquality(objectVariable3, this.verticalRun.start.value);
                    linearSystem.addEquality(objectVariable4, this.verticalRun.end.value);
                    linearSystem.addEquality(objectVariable5, this.verticalRun.baseline.value);
                    final ConstraintWidget mParent7 = this.mParent;
                    if (mParent7 != null && !inVerticalChain && b5) {
                        if (this.isTerminalWidget[1]) {
                            linearSystem.addGreaterThan(linearSystem.createObjectVariable(mParent7.mBottom), objectVariable4, 0, 8);
                        }
                    }
                    b15 = false;
                    break Label_2161;
                }
            }
            b15 = true;
        }
        if (this.mVerticalResolution == 2) {
            b15 = false;
        }
        if (b15 && !this.resolvedVertical) {
            final boolean b16 = this.mListDimensionBehaviors[1] == DimensionBehaviour.WRAP_CONTENT && this instanceof ConstraintWidgetContainer;
            if (b16) {
                n8 = 0;
            }
            final ConstraintWidget mParent8 = this.mParent;
            SolverVariable objectVariable8;
            if (mParent8 != null) {
                objectVariable8 = linearSystem.createObjectVariable(mParent8.mBottom);
            }
            else {
                objectVariable8 = null;
            }
            final ConstraintWidget mParent9 = this.mParent;
            SolverVariable objectVariable9;
            if (mParent9 != null) {
                objectVariable9 = linearSystem.createObjectVariable(mParent9.mTop);
            }
            else {
                objectVariable9 = null;
            }
            boolean b17 = false;
            Label_2420: {
                if (this.mBaselineDistance > 0 || this.mVisibility == 8) {
                    if (this.mBaseline.mTarget != null) {
                        linearSystem.addEquality(objectVariable5, objectVariable3, this.getBaselineDistance(), 8);
                        linearSystem.addEquality(objectVariable5, linearSystem.createObjectVariable(this.mBaseline.mTarget), this.mBaseline.getMargin(), 8);
                        if (b5) {
                            linearSystem.addGreaterThan(objectVariable8, linearSystem.createObjectVariable(this.mBottom), 0, 5);
                        }
                        b17 = false;
                        break Label_2420;
                    }
                    if (this.mVisibility == 8) {
                        linearSystem.addEquality(objectVariable5, objectVariable3, this.mBaseline.getMargin(), 8);
                    }
                    else {
                        linearSystem.addEquality(objectVariable5, objectVariable3, this.getBaselineDistance(), 8);
                    }
                }
                b17 = b11;
            }
            final boolean b18 = this.isTerminalWidget[1];
            final DimensionBehaviour[] mListDimensionBehaviors2 = this.mListDimensionBehaviors;
            this.applyConstraints(linearSystem, false, b5, b4, b18, objectVariable9, objectVariable8, mListDimensionBehaviors2[1], b16, this.mTop, this.mBottom, this.mY, n8, this.mMinHeight, this.mMaxDimension[1], this.mVerticalBiasPercent, b9, mListDimensionBehaviors2[0] == DimensionBehaviour.MATCH_CONSTRAINT, inVerticalChain, (boolean)(n != 0), b13, n11, mMatchConstraintDefaultWidth, this.mMatchConstraintMinHeight, this.mMatchConstraintMaxHeight, this.mMatchConstraintPercentHeight, b17);
        }
        if (mResolvedHasRatio) {
            if (this.mResolvedDimensionRatioSide == 1) {
                linearSystem.addRatio(objectVariable4, objectVariable3, objectVariable2, objectVariable, this.mResolvedDimensionRatio, 8);
            }
            else {
                linearSystem.addRatio(objectVariable2, objectVariable, objectVariable4, objectVariable3, this.mResolvedDimensionRatio, 8);
            }
        }
        if (this.mCenter.isConnected()) {
            linearSystem.addCenterPoint(this, this.mCenter.getTarget().getOwner(), (float)Math.toRadians(this.mCircleConstraintAngle + 90.0f), this.mCenter.getMargin());
        }
        this.resolvedHorizontal = false;
        this.resolvedVertical = false;
    }
    
    public boolean allowedInBarrier() {
        return this.mVisibility != 8;
    }
    
    public void connect(final ConstraintAnchor.Type type, final ConstraintWidget constraintWidget, final ConstraintAnchor.Type type2) {
        this.connect(type, constraintWidget, type2, 0);
    }
    
    public void connect(ConstraintAnchor.Type right, final ConstraintWidget constraintWidget, final ConstraintAnchor.Type type, int n) {
        Label_0361: {
            if (right != ConstraintAnchor.Type.CENTER) {
                break Label_0361;
            }
            if (type == ConstraintAnchor.Type.CENTER) {
                final ConstraintAnchor anchor = this.getAnchor(ConstraintAnchor.Type.LEFT);
                final ConstraintAnchor anchor2 = this.getAnchor(ConstraintAnchor.Type.RIGHT);
                final ConstraintAnchor anchor3 = this.getAnchor(ConstraintAnchor.Type.TOP);
                final ConstraintAnchor anchor4 = this.getAnchor(ConstraintAnchor.Type.BOTTOM);
                boolean b = true;
                if ((anchor != null && anchor.isConnected()) || (anchor2 != null && anchor2.isConnected())) {
                    n = 0;
                }
                else {
                    this.connect(ConstraintAnchor.Type.LEFT, constraintWidget, ConstraintAnchor.Type.LEFT, 0);
                    this.connect(ConstraintAnchor.Type.RIGHT, constraintWidget, ConstraintAnchor.Type.RIGHT, 0);
                    n = 1;
                }
                if ((anchor3 != null && anchor3.isConnected()) || (anchor4 != null && anchor4.isConnected())) {
                    b = false;
                }
                else {
                    this.connect(ConstraintAnchor.Type.TOP, constraintWidget, ConstraintAnchor.Type.TOP, 0);
                    this.connect(ConstraintAnchor.Type.BOTTOM, constraintWidget, ConstraintAnchor.Type.BOTTOM, 0);
                }
                if (n != 0 && b) {
                    this.getAnchor(ConstraintAnchor.Type.CENTER).connect(constraintWidget.getAnchor(ConstraintAnchor.Type.CENTER), 0);
                    return;
                }
                if (n != 0) {
                    this.getAnchor(ConstraintAnchor.Type.CENTER_X).connect(constraintWidget.getAnchor(ConstraintAnchor.Type.CENTER_X), 0);
                    return;
                }
                if (b) {
                    this.getAnchor(ConstraintAnchor.Type.CENTER_Y).connect(constraintWidget.getAnchor(ConstraintAnchor.Type.CENTER_Y), 0);
                }
                return;
            }
            else if (type != ConstraintAnchor.Type.LEFT && type != ConstraintAnchor.Type.RIGHT) {
                if (type == ConstraintAnchor.Type.TOP || type == ConstraintAnchor.Type.BOTTOM) {
                    this.connect(ConstraintAnchor.Type.TOP, constraintWidget, type, 0);
                    this.connect(ConstraintAnchor.Type.BOTTOM, constraintWidget, type, 0);
                    this.getAnchor(ConstraintAnchor.Type.CENTER).connect(constraintWidget.getAnchor(type), 0);
                }
                return;
            }
            this.connect(ConstraintAnchor.Type.LEFT, constraintWidget, type, 0);
            right = ConstraintAnchor.Type.RIGHT;
            try {
                this.connect(right, constraintWidget, type, 0);
                this.getAnchor(ConstraintAnchor.Type.CENTER).connect(constraintWidget.getAnchor(type), 0);
                Label_0874: {
                    return;
                }
            Label_0382:
                while (true) {
                    ConstraintAnchor anchor8 = null;
                    Label_0813: {
                        ConstraintAnchor anchor5;
                        ConstraintAnchor opposite;
                        ConstraintAnchor anchor6;
                        ConstraintAnchor anchor7;
                        ConstraintAnchor anchor9 = null;
                        ConstraintAnchor anchor10 = null;
                        ConstraintAnchor anchor11;
                        ConstraintAnchor anchor12;
                        final ConstraintAnchor anchor13;
                        ConstraintAnchor opposite2;
                        ConstraintAnchor anchor14;
                        Label_0865:Block_25_Outer:Label_0833_Outer:
                        while (true) {
                            Label_0762: {
                            Block_27_Outer:
                                while (true) {
                                    while (true) {
                                        Label_0700: {
                                            Block_23: {
                                                while (true) {
                                                    Block_24: {
                                                        Block_31_Outer:Block_35_Outer:
                                                        while (true) {
                                                            iftrue(Label_0865:)(right != ConstraintAnchor.Type.LEFT && right != ConstraintAnchor.Type.RIGHT);
                                                        Block_21_Outer:
                                                            while (true) {
                                                                while (true) {
                                                                    while (true) {
                                                                        Label_0455: {
                                                                            while (true) {
                                                                                while (true) {
                                                                                    Label_0742: {
                                                                                        break Label_0742;
                                                                                        anchor5.reset();
                                                                                        break Label_0762;
                                                                                        opposite.reset();
                                                                                        anchor6.reset();
                                                                                        break Label_0865;
                                                                                        this.getAnchor(ConstraintAnchor.Type.LEFT).connect(constraintWidget.getAnchor(ConstraintAnchor.Type.LEFT), 0);
                                                                                        this.getAnchor(ConstraintAnchor.Type.RIGHT).connect(constraintWidget.getAnchor(ConstraintAnchor.Type.RIGHT), 0);
                                                                                        this.getAnchor(ConstraintAnchor.Type.CENTER_X).connect(constraintWidget.getAnchor(type), 0);
                                                                                        return;
                                                                                        Label_0434:
                                                                                        iftrue(Label_0503:)(right != ConstraintAnchor.Type.CENTER_Y || (type != ConstraintAnchor.Type.TOP && type != ConstraintAnchor.Type.BOTTOM));
                                                                                        break Label_0455;
                                                                                        anchor7.reset();
                                                                                        break Label_0700;
                                                                                    }
                                                                                    anchor5 = this.getAnchor(ConstraintAnchor.Type.CENTER);
                                                                                    iftrue(Label_0762:)(anchor5.getTarget() == anchor8);
                                                                                    continue Block_35_Outer;
                                                                                }
                                                                                Label_0647:
                                                                                anchor9 = this.getAnchor(right);
                                                                                anchor8 = constraintWidget.getAnchor(type);
                                                                                iftrue(Label_0874:)(!anchor9.isValidConnection(anchor8));
                                                                                break Block_24;
                                                                                anchor7 = this.getAnchor(ConstraintAnchor.Type.TOP);
                                                                                anchor10 = this.getAnchor(ConstraintAnchor.Type.BOTTOM);
                                                                                iftrue(Label_0700:)(anchor7 == null);
                                                                                continue Block_25_Outer;
                                                                            }
                                                                        }
                                                                        anchor11 = constraintWidget.getAnchor(type);
                                                                        this.getAnchor(ConstraintAnchor.Type.TOP).connect(anchor11, 0);
                                                                        this.getAnchor(ConstraintAnchor.Type.BOTTOM).connect(anchor11, 0);
                                                                        this.getAnchor(ConstraintAnchor.Type.CENTER_Y).connect(anchor11, 0);
                                                                        return;
                                                                        anchor12.reset();
                                                                        break Label_0813;
                                                                        Label_0503:
                                                                        iftrue(Label_0575:)(right != ConstraintAnchor.Type.CENTER_X || type != ConstraintAnchor.Type.CENTER_X);
                                                                        continue Block_25_Outer;
                                                                    }
                                                                    Label_0797:
                                                                    anchor12 = this.getAnchor(ConstraintAnchor.Type.BASELINE);
                                                                    iftrue(Label_0813:)(anchor12 == null);
                                                                    continue Label_0833_Outer;
                                                                }
                                                                opposite = this.getAnchor(right).getOpposite();
                                                                anchor6 = this.getAnchor(ConstraintAnchor.Type.CENTER_Y);
                                                                iftrue(Label_0865:)(!anchor6.isConnected());
                                                                continue Block_21_Outer;
                                                            }
                                                            Label_0575:
                                                            iftrue(Label_0647:)(right != ConstraintAnchor.Type.CENTER_Y || type != ConstraintAnchor.Type.CENTER_Y);
                                                            break Block_23;
                                                            Label_0711:
                                                            iftrue(Label_0797:)(right == ConstraintAnchor.Type.TOP || right == ConstraintAnchor.Type.BOTTOM);
                                                            continue Block_31_Outer;
                                                        }
                                                    }
                                                    iftrue(Label_0711:)(right != ConstraintAnchor.Type.BASELINE);
                                                    continue Label_0833_Outer;
                                                }
                                                anchor10.reset();
                                                break Label_0865;
                                            }
                                            this.getAnchor(ConstraintAnchor.Type.TOP).connect(constraintWidget.getAnchor(ConstraintAnchor.Type.TOP), 0);
                                            this.getAnchor(ConstraintAnchor.Type.BOTTOM).connect(constraintWidget.getAnchor(ConstraintAnchor.Type.BOTTOM), 0);
                                            this.getAnchor(ConstraintAnchor.Type.CENTER_Y).connect(constraintWidget.getAnchor(type), 0);
                                            return;
                                        }
                                        iftrue(Label_0865:)(anchor10 == null);
                                        continue;
                                    }
                                    anchor13.reset();
                                    continue Block_27_Outer;
                                }
                                anchor9.connect(anchor8, n);
                                return;
                                iftrue(Label_0434:)(right != ConstraintAnchor.Type.CENTER_X || (type != ConstraintAnchor.Type.LEFT && type != ConstraintAnchor.Type.RIGHT));
                                break Label_0382;
                            }
                            opposite2 = this.getAnchor(right).getOpposite();
                            anchor14 = this.getAnchor(ConstraintAnchor.Type.CENTER_X);
                            iftrue(Label_0865:)(!anchor14.isConnected());
                            opposite2.reset();
                            anchor14.reset();
                            continue Label_0865;
                        }
                    }
                    final ConstraintAnchor anchor13 = this.getAnchor(ConstraintAnchor.Type.CENTER);
                    iftrue(Label_0833:)(anchor13.getTarget() == anchor8);
                    continue;
                }
                final ConstraintAnchor anchor15 = this.getAnchor(ConstraintAnchor.Type.LEFT);
                final ConstraintAnchor anchor16 = constraintWidget.getAnchor(type);
                final ConstraintAnchor anchor17 = this.getAnchor(ConstraintAnchor.Type.RIGHT);
                anchor15.connect(anchor16, 0);
                anchor17.connect(anchor16, 0);
                this.getAnchor(ConstraintAnchor.Type.CENTER_X).connect(anchor16, 0);
            }
            finally {}
        }
    }
    
    public void connect(final ConstraintAnchor constraintAnchor, final ConstraintAnchor constraintAnchor2, final int n) {
        if (constraintAnchor.getOwner() == this) {
            this.connect(constraintAnchor.getType(), constraintAnchor2.getOwner(), constraintAnchor2.getType(), n);
        }
    }
    
    public void connectCircularConstraint(final ConstraintWidget constraintWidget, final float mCircleConstraintAngle, final int n) {
        this.immediateConnect(ConstraintAnchor.Type.CENTER, constraintWidget, ConstraintAnchor.Type.CENTER, n, 0);
        this.mCircleConstraintAngle = mCircleConstraintAngle;
    }
    
    public void copy(ConstraintWidget mVerticalNextWidget, final HashMap<ConstraintWidget, ConstraintWidget> hashMap) {
        this.mHorizontalResolution = mVerticalNextWidget.mHorizontalResolution;
        this.mVerticalResolution = mVerticalNextWidget.mVerticalResolution;
        this.mMatchConstraintDefaultWidth = mVerticalNextWidget.mMatchConstraintDefaultWidth;
        this.mMatchConstraintDefaultHeight = mVerticalNextWidget.mMatchConstraintDefaultHeight;
        final int[] mResolvedMatchConstraintDefault = this.mResolvedMatchConstraintDefault;
        final int[] mResolvedMatchConstraintDefault2 = mVerticalNextWidget.mResolvedMatchConstraintDefault;
        mResolvedMatchConstraintDefault[0] = mResolvedMatchConstraintDefault2[0];
        mResolvedMatchConstraintDefault[1] = mResolvedMatchConstraintDefault2[1];
        this.mMatchConstraintMinWidth = mVerticalNextWidget.mMatchConstraintMinWidth;
        this.mMatchConstraintMaxWidth = mVerticalNextWidget.mMatchConstraintMaxWidth;
        this.mMatchConstraintMinHeight = mVerticalNextWidget.mMatchConstraintMinHeight;
        this.mMatchConstraintMaxHeight = mVerticalNextWidget.mMatchConstraintMaxHeight;
        this.mMatchConstraintPercentHeight = mVerticalNextWidget.mMatchConstraintPercentHeight;
        this.mIsWidthWrapContent = mVerticalNextWidget.mIsWidthWrapContent;
        this.mIsHeightWrapContent = mVerticalNextWidget.mIsHeightWrapContent;
        this.mResolvedDimensionRatioSide = mVerticalNextWidget.mResolvedDimensionRatioSide;
        this.mResolvedDimensionRatio = mVerticalNextWidget.mResolvedDimensionRatio;
        final int[] mMaxDimension = mVerticalNextWidget.mMaxDimension;
        this.mMaxDimension = Arrays.copyOf(mMaxDimension, mMaxDimension.length);
        this.mCircleConstraintAngle = mVerticalNextWidget.mCircleConstraintAngle;
        this.hasBaseline = mVerticalNextWidget.hasBaseline;
        this.inPlaceholder = mVerticalNextWidget.inPlaceholder;
        this.mLeft.reset();
        this.mTop.reset();
        this.mRight.reset();
        this.mBottom.reset();
        this.mBaseline.reset();
        this.mCenterX.reset();
        this.mCenterY.reset();
        this.mCenter.reset();
        this.mListDimensionBehaviors = Arrays.copyOf(this.mListDimensionBehaviors, 2);
        final ConstraintWidget mParent = this.mParent;
        final ConstraintWidget constraintWidget = null;
        ConstraintWidget mParent2;
        if (mParent == null) {
            mParent2 = null;
        }
        else {
            mParent2 = hashMap.get(mVerticalNextWidget.mParent);
        }
        this.mParent = mParent2;
        this.mWidth = mVerticalNextWidget.mWidth;
        this.mHeight = mVerticalNextWidget.mHeight;
        this.mDimensionRatio = mVerticalNextWidget.mDimensionRatio;
        this.mDimensionRatioSide = mVerticalNextWidget.mDimensionRatioSide;
        this.mX = mVerticalNextWidget.mX;
        this.mY = mVerticalNextWidget.mY;
        this.mRelX = mVerticalNextWidget.mRelX;
        this.mRelY = mVerticalNextWidget.mRelY;
        this.mOffsetX = mVerticalNextWidget.mOffsetX;
        this.mOffsetY = mVerticalNextWidget.mOffsetY;
        this.mBaselineDistance = mVerticalNextWidget.mBaselineDistance;
        this.mMinWidth = mVerticalNextWidget.mMinWidth;
        this.mMinHeight = mVerticalNextWidget.mMinHeight;
        this.mHorizontalBiasPercent = mVerticalNextWidget.mHorizontalBiasPercent;
        this.mVerticalBiasPercent = mVerticalNextWidget.mVerticalBiasPercent;
        this.mCompanionWidget = mVerticalNextWidget.mCompanionWidget;
        this.mContainerItemSkip = mVerticalNextWidget.mContainerItemSkip;
        this.mVisibility = mVerticalNextWidget.mVisibility;
        this.mAnimated = mVerticalNextWidget.mAnimated;
        this.mDebugName = mVerticalNextWidget.mDebugName;
        this.mType = mVerticalNextWidget.mType;
        this.mDistToTop = mVerticalNextWidget.mDistToTop;
        this.mDistToLeft = mVerticalNextWidget.mDistToLeft;
        this.mDistToRight = mVerticalNextWidget.mDistToRight;
        this.mDistToBottom = mVerticalNextWidget.mDistToBottom;
        this.mLeftHasCentered = mVerticalNextWidget.mLeftHasCentered;
        this.mRightHasCentered = mVerticalNextWidget.mRightHasCentered;
        this.mTopHasCentered = mVerticalNextWidget.mTopHasCentered;
        this.mBottomHasCentered = mVerticalNextWidget.mBottomHasCentered;
        this.mHorizontalWrapVisited = mVerticalNextWidget.mHorizontalWrapVisited;
        this.mVerticalWrapVisited = mVerticalNextWidget.mVerticalWrapVisited;
        this.mHorizontalChainStyle = mVerticalNextWidget.mHorizontalChainStyle;
        this.mVerticalChainStyle = mVerticalNextWidget.mVerticalChainStyle;
        this.mHorizontalChainFixedPosition = mVerticalNextWidget.mHorizontalChainFixedPosition;
        this.mVerticalChainFixedPosition = mVerticalNextWidget.mVerticalChainFixedPosition;
        final float[] mWeight = this.mWeight;
        final float[] mWeight2 = mVerticalNextWidget.mWeight;
        mWeight[0] = mWeight2[0];
        mWeight[1] = mWeight2[1];
        final ConstraintWidget[] mListNextMatchConstraintsWidget = this.mListNextMatchConstraintsWidget;
        final ConstraintWidget[] mListNextMatchConstraintsWidget2 = mVerticalNextWidget.mListNextMatchConstraintsWidget;
        mListNextMatchConstraintsWidget[0] = mListNextMatchConstraintsWidget2[0];
        mListNextMatchConstraintsWidget[1] = mListNextMatchConstraintsWidget2[1];
        final ConstraintWidget[] mNextChainWidget = this.mNextChainWidget;
        final ConstraintWidget[] mNextChainWidget2 = mVerticalNextWidget.mNextChainWidget;
        mNextChainWidget[0] = mNextChainWidget2[0];
        mNextChainWidget[1] = mNextChainWidget2[1];
        final ConstraintWidget mHorizontalNextWidget = mVerticalNextWidget.mHorizontalNextWidget;
        ConstraintWidget mHorizontalNextWidget2;
        if (mHorizontalNextWidget == null) {
            mHorizontalNextWidget2 = null;
        }
        else {
            mHorizontalNextWidget2 = hashMap.get(mHorizontalNextWidget);
        }
        this.mHorizontalNextWidget = mHorizontalNextWidget2;
        mVerticalNextWidget = mVerticalNextWidget.mVerticalNextWidget;
        if (mVerticalNextWidget == null) {
            mVerticalNextWidget = constraintWidget;
        }
        else {
            mVerticalNextWidget = hashMap.get(mVerticalNextWidget);
        }
        this.mVerticalNextWidget = mVerticalNextWidget;
    }
    
    public void createObjectVariables(final LinearSystem linearSystem) {
        linearSystem.createObjectVariable(this.mLeft);
        linearSystem.createObjectVariable(this.mTop);
        linearSystem.createObjectVariable(this.mRight);
        linearSystem.createObjectVariable(this.mBottom);
        if (this.mBaselineDistance > 0) {
            linearSystem.createObjectVariable(this.mBaseline);
        }
    }
    
    public void ensureMeasureRequested() {
        this.mMeasureRequested = true;
    }
    
    public void ensureWidgetRuns() {
        if (this.horizontalRun == null) {
            this.horizontalRun = new HorizontalWidgetRun(this);
        }
        if (this.verticalRun == null) {
            this.verticalRun = new VerticalWidgetRun(this);
        }
    }
    
    public ConstraintAnchor getAnchor(final ConstraintAnchor.Type type) {
        switch (ConstraintWidget$1.$SwitchMap$androidx$constraintlayout$core$widgets$ConstraintAnchor$Type[type.ordinal()]) {
            default: {
                throw new AssertionError((Object)type.name());
            }
            case 9: {
                return null;
            }
            case 8: {
                return this.mCenterY;
            }
            case 7: {
                return this.mCenterX;
            }
            case 6: {
                return this.mCenter;
            }
            case 5: {
                return this.mBaseline;
            }
            case 4: {
                return this.mBottom;
            }
            case 3: {
                return this.mRight;
            }
            case 2: {
                return this.mTop;
            }
            case 1: {
                return this.mLeft;
            }
        }
    }
    
    public ArrayList<ConstraintAnchor> getAnchors() {
        return this.mAnchors;
    }
    
    public int getBaselineDistance() {
        return this.mBaselineDistance;
    }
    
    public float getBiasPercent(final int n) {
        if (n == 0) {
            return this.mHorizontalBiasPercent;
        }
        if (n == 1) {
            return this.mVerticalBiasPercent;
        }
        return -1.0f;
    }
    
    public int getBottom() {
        return this.getY() + this.mHeight;
    }
    
    public Object getCompanionWidget() {
        return this.mCompanionWidget;
    }
    
    public int getContainerItemSkip() {
        return this.mContainerItemSkip;
    }
    
    public String getDebugName() {
        return this.mDebugName;
    }
    
    public DimensionBehaviour getDimensionBehaviour(final int n) {
        if (n == 0) {
            return this.getHorizontalDimensionBehaviour();
        }
        if (n == 1) {
            return this.getVerticalDimensionBehaviour();
        }
        return null;
    }
    
    public float getDimensionRatio() {
        return this.mDimensionRatio;
    }
    
    public int getDimensionRatioSide() {
        return this.mDimensionRatioSide;
    }
    
    public boolean getHasBaseline() {
        return this.hasBaseline;
    }
    
    public int getHeight() {
        if (this.mVisibility == 8) {
            return 0;
        }
        return this.mHeight;
    }
    
    public float getHorizontalBiasPercent() {
        return this.mHorizontalBiasPercent;
    }
    
    public ConstraintWidget getHorizontalChainControlWidget() {
        final boolean inHorizontalChain = this.isInHorizontalChain();
        ConstraintWidget constraintWidget = null;
        if (inHorizontalChain) {
            constraintWidget = this;
            ConstraintWidget constraintWidget2 = null;
            while (constraintWidget2 == null && constraintWidget != null) {
                final ConstraintAnchor anchor = constraintWidget.getAnchor(ConstraintAnchor.Type.LEFT);
                ConstraintAnchor target;
                if (anchor == null) {
                    target = null;
                }
                else {
                    target = anchor.getTarget();
                }
                ConstraintWidget owner;
                if (target == null) {
                    owner = null;
                }
                else {
                    owner = target.getOwner();
                }
                if (owner == this.getParent()) {
                    return constraintWidget;
                }
                ConstraintAnchor target2;
                if (owner == null) {
                    target2 = null;
                }
                else {
                    target2 = owner.getAnchor(ConstraintAnchor.Type.RIGHT).getTarget();
                }
                if (target2 != null && target2.getOwner() != constraintWidget) {
                    constraintWidget2 = constraintWidget;
                }
                else {
                    constraintWidget = owner;
                }
            }
            constraintWidget = constraintWidget2;
        }
        return constraintWidget;
    }
    
    public int getHorizontalChainStyle() {
        return this.mHorizontalChainStyle;
    }
    
    public DimensionBehaviour getHorizontalDimensionBehaviour() {
        return this.mListDimensionBehaviors[0];
    }
    
    public int getHorizontalMargin() {
        final ConstraintAnchor mLeft = this.mLeft;
        int n = 0;
        if (mLeft != null) {
            n = 0 + mLeft.mMargin;
        }
        final ConstraintAnchor mRight = this.mRight;
        int n2 = n;
        if (mRight != null) {
            n2 = n + mRight.mMargin;
        }
        return n2;
    }
    
    public int getLastHorizontalMeasureSpec() {
        return this.mLastHorizontalMeasureSpec;
    }
    
    public int getLastVerticalMeasureSpec() {
        return this.mLastVerticalMeasureSpec;
    }
    
    public int getLeft() {
        return this.getX();
    }
    
    public int getLength(final int n) {
        if (n == 0) {
            return this.getWidth();
        }
        if (n == 1) {
            return this.getHeight();
        }
        return 0;
    }
    
    public int getMaxHeight() {
        return this.mMaxDimension[1];
    }
    
    public int getMaxWidth() {
        return this.mMaxDimension[0];
    }
    
    public int getMinHeight() {
        return this.mMinHeight;
    }
    
    public int getMinWidth() {
        return this.mMinWidth;
    }
    
    public ConstraintWidget getNextChainMember(final int n) {
        if (n == 0) {
            if (this.mRight.mTarget != null) {
                final ConstraintAnchor mTarget = this.mRight.mTarget.mTarget;
                final ConstraintAnchor mRight = this.mRight;
                if (mTarget == mRight) {
                    return mRight.mTarget.mOwner;
                }
            }
        }
        else if (n == 1 && this.mBottom.mTarget != null) {
            final ConstraintAnchor mTarget2 = this.mBottom.mTarget.mTarget;
            final ConstraintAnchor mBottom = this.mBottom;
            if (mTarget2 == mBottom) {
                return mBottom.mTarget.mOwner;
            }
        }
        return null;
    }
    
    public int getOptimizerWrapHeight() {
        int mHeight = this.mHeight;
        if (this.mListDimensionBehaviors[1] == DimensionBehaviour.MATCH_CONSTRAINT) {
            int mHeight2;
            if (this.mMatchConstraintDefaultHeight == 1) {
                mHeight2 = Math.max(this.mMatchConstraintMinHeight, mHeight);
            }
            else {
                mHeight2 = this.mMatchConstraintMinHeight;
                if (mHeight2 > 0) {
                    this.mHeight = mHeight2;
                }
                else {
                    mHeight2 = 0;
                }
            }
            final int mMatchConstraintMaxHeight = this.mMatchConstraintMaxHeight;
            mHeight = mHeight2;
            if (mMatchConstraintMaxHeight > 0 && mMatchConstraintMaxHeight < (mHeight = mHeight2)) {
                mHeight = mMatchConstraintMaxHeight;
            }
        }
        return mHeight;
    }
    
    public int getOptimizerWrapWidth() {
        int mWidth = this.mWidth;
        if (this.mListDimensionBehaviors[0] == DimensionBehaviour.MATCH_CONSTRAINT) {
            int mWidth2;
            if (this.mMatchConstraintDefaultWidth == 1) {
                mWidth2 = Math.max(this.mMatchConstraintMinWidth, mWidth);
            }
            else {
                mWidth2 = this.mMatchConstraintMinWidth;
                if (mWidth2 > 0) {
                    this.mWidth = mWidth2;
                }
                else {
                    mWidth2 = 0;
                }
            }
            final int mMatchConstraintMaxWidth = this.mMatchConstraintMaxWidth;
            mWidth = mWidth2;
            if (mMatchConstraintMaxWidth > 0 && mMatchConstraintMaxWidth < (mWidth = mWidth2)) {
                mWidth = mMatchConstraintMaxWidth;
            }
        }
        return mWidth;
    }
    
    public ConstraintWidget getParent() {
        return this.mParent;
    }
    
    public ConstraintWidget getPreviousChainMember(final int n) {
        if (n == 0) {
            if (this.mLeft.mTarget != null) {
                final ConstraintAnchor mTarget = this.mLeft.mTarget.mTarget;
                final ConstraintAnchor mLeft = this.mLeft;
                if (mTarget == mLeft) {
                    return mLeft.mTarget.mOwner;
                }
            }
        }
        else if (n == 1 && this.mTop.mTarget != null) {
            final ConstraintAnchor mTarget2 = this.mTop.mTarget.mTarget;
            final ConstraintAnchor mTop = this.mTop;
            if (mTarget2 == mTop) {
                return mTop.mTarget.mOwner;
            }
        }
        return null;
    }
    
    int getRelativePositioning(final int n) {
        if (n == 0) {
            return this.mRelX;
        }
        if (n == 1) {
            return this.mRelY;
        }
        return 0;
    }
    
    public int getRight() {
        return this.getX() + this.mWidth;
    }
    
    protected int getRootX() {
        return this.mX + this.mOffsetX;
    }
    
    protected int getRootY() {
        return this.mY + this.mOffsetY;
    }
    
    public WidgetRun getRun(final int n) {
        if (n == 0) {
            return this.horizontalRun;
        }
        if (n == 1) {
            return this.verticalRun;
        }
        return null;
    }
    
    public void getSceneString(final StringBuilder sb) {
        final StringBuilder sb2 = new StringBuilder();
        sb2.append("  ");
        sb2.append(this.stringId);
        sb2.append(":{\n");
        sb.append(sb2.toString());
        final StringBuilder sb3 = new StringBuilder();
        sb3.append("    actualWidth:");
        sb3.append(this.mWidth);
        sb.append(sb3.toString());
        sb.append("\n");
        final StringBuilder sb4 = new StringBuilder();
        sb4.append("    actualHeight:");
        sb4.append(this.mHeight);
        sb.append(sb4.toString());
        sb.append("\n");
        final StringBuilder sb5 = new StringBuilder();
        sb5.append("    actualLeft:");
        sb5.append(this.mX);
        sb.append(sb5.toString());
        sb.append("\n");
        final StringBuilder sb6 = new StringBuilder();
        sb6.append("    actualTop:");
        sb6.append(this.mY);
        sb.append(sb6.toString());
        sb.append("\n");
        this.getSceneString(sb, "left", this.mLeft);
        this.getSceneString(sb, "top", this.mTop);
        this.getSceneString(sb, "right", this.mRight);
        this.getSceneString(sb, "bottom", this.mBottom);
        this.getSceneString(sb, "baseline", this.mBaseline);
        this.getSceneString(sb, "centerX", this.mCenterX);
        this.getSceneString(sb, "centerY", this.mCenterY);
        this.getSceneString(sb, "    width", this.mWidth, this.mMinWidth, this.mMaxDimension[0], this.mWidthOverride, this.mMatchConstraintMinWidth, this.mMatchConstraintDefaultWidth, this.mMatchConstraintPercentWidth, this.mWeight[0]);
        this.getSceneString(sb, "    height", this.mHeight, this.mMinHeight, this.mMaxDimension[1], this.mHeightOverride, this.mMatchConstraintMinHeight, this.mMatchConstraintDefaultHeight, this.mMatchConstraintPercentHeight, this.mWeight[1]);
        this.serializeDimensionRatio(sb, "    dimensionRatio", this.mDimensionRatio, this.mDimensionRatioSide);
        this.serializeAttribute(sb, "    horizontalBias", this.mHorizontalBiasPercent, ConstraintWidget.DEFAULT_BIAS);
        this.serializeAttribute(sb, "    verticalBias", this.mVerticalBiasPercent, ConstraintWidget.DEFAULT_BIAS);
        this.serializeAttribute(sb, "    horizontalChainStyle", this.mHorizontalChainStyle, 0);
        this.serializeAttribute(sb, "    verticalChainStyle", this.mVerticalChainStyle, 0);
        sb.append("  }");
    }
    
    public int getTop() {
        return this.getY();
    }
    
    public String getType() {
        return this.mType;
    }
    
    public float getVerticalBiasPercent() {
        return this.mVerticalBiasPercent;
    }
    
    public ConstraintWidget getVerticalChainControlWidget() {
        final boolean inVerticalChain = this.isInVerticalChain();
        ConstraintWidget constraintWidget = null;
        if (inVerticalChain) {
            constraintWidget = this;
            ConstraintWidget constraintWidget2 = null;
            while (constraintWidget2 == null && constraintWidget != null) {
                final ConstraintAnchor anchor = constraintWidget.getAnchor(ConstraintAnchor.Type.TOP);
                ConstraintAnchor target;
                if (anchor == null) {
                    target = null;
                }
                else {
                    target = anchor.getTarget();
                }
                ConstraintWidget owner;
                if (target == null) {
                    owner = null;
                }
                else {
                    owner = target.getOwner();
                }
                if (owner == this.getParent()) {
                    return constraintWidget;
                }
                ConstraintAnchor target2;
                if (owner == null) {
                    target2 = null;
                }
                else {
                    target2 = owner.getAnchor(ConstraintAnchor.Type.BOTTOM).getTarget();
                }
                if (target2 != null && target2.getOwner() != constraintWidget) {
                    constraintWidget2 = constraintWidget;
                }
                else {
                    constraintWidget = owner;
                }
            }
            constraintWidget = constraintWidget2;
        }
        return constraintWidget;
    }
    
    public int getVerticalChainStyle() {
        return this.mVerticalChainStyle;
    }
    
    public DimensionBehaviour getVerticalDimensionBehaviour() {
        return this.mListDimensionBehaviors[1];
    }
    
    public int getVerticalMargin() {
        final ConstraintAnchor mLeft = this.mLeft;
        int n = 0;
        if (mLeft != null) {
            n = 0 + this.mTop.mMargin;
        }
        int n2 = n;
        if (this.mRight != null) {
            n2 = n + this.mBottom.mMargin;
        }
        return n2;
    }
    
    public int getVisibility() {
        return this.mVisibility;
    }
    
    public int getWidth() {
        if (this.mVisibility == 8) {
            return 0;
        }
        return this.mWidth;
    }
    
    public int getWrapBehaviorInParent() {
        return this.mWrapBehaviorInParent;
    }
    
    public int getX() {
        final ConstraintWidget mParent = this.mParent;
        if (mParent != null && mParent instanceof ConstraintWidgetContainer) {
            return ((ConstraintWidgetContainer)mParent).mPaddingLeft + this.mX;
        }
        return this.mX;
    }
    
    public int getY() {
        final ConstraintWidget mParent = this.mParent;
        if (mParent != null && mParent instanceof ConstraintWidgetContainer) {
            return ((ConstraintWidgetContainer)mParent).mPaddingTop + this.mY;
        }
        return this.mY;
    }
    
    public boolean hasBaseline() {
        return this.hasBaseline;
    }
    
    public boolean hasDanglingDimension(int n) {
        boolean b = true;
        final boolean b2 = true;
        if (n == 0) {
            if (this.mLeft.mTarget != null) {
                n = 1;
            }
            else {
                n = 0;
            }
            int n2;
            if (this.mRight.mTarget != null) {
                n2 = 1;
            }
            else {
                n2 = 0;
            }
            return n + n2 < 2 && b2;
        }
        if (this.mTop.mTarget != null) {
            n = 1;
        }
        else {
            n = 0;
        }
        int n3;
        if (this.mBottom.mTarget != null) {
            n3 = 1;
        }
        else {
            n3 = 0;
        }
        int n4;
        if (this.mBaseline.mTarget != null) {
            n4 = 1;
        }
        else {
            n4 = 0;
        }
        if (n + n3 + n4 >= 2) {
            b = false;
        }
        return b;
    }
    
    public boolean hasDependencies() {
        for (int size = this.mAnchors.size(), i = 0; i < size; ++i) {
            if (this.mAnchors.get(i).hasDependents()) {
                return true;
            }
        }
        return false;
    }
    
    public boolean hasDimensionOverride() {
        return this.mWidthOverride != -1 || this.mHeightOverride != -1;
    }
    
    public boolean hasResolvedTargets(final int n, final int n2) {
        boolean b = true;
        final boolean b2 = true;
        if (n == 0) {
            if (this.mLeft.mTarget != null && this.mLeft.mTarget.hasFinalValue() && this.mRight.mTarget != null && this.mRight.mTarget.hasFinalValue()) {
                return this.mRight.mTarget.getFinalValue() - this.mRight.getMargin() - (this.mLeft.mTarget.getFinalValue() + this.mLeft.getMargin()) >= n2 && b2;
            }
        }
        else if (this.mTop.mTarget != null && this.mTop.mTarget.hasFinalValue() && this.mBottom.mTarget != null && this.mBottom.mTarget.hasFinalValue()) {
            if (this.mBottom.mTarget.getFinalValue() - this.mBottom.getMargin() - (this.mTop.mTarget.getFinalValue() + this.mTop.getMargin()) < n2) {
                b = false;
            }
            return b;
        }
        return false;
    }
    
    public void immediateConnect(final ConstraintAnchor.Type type, final ConstraintWidget constraintWidget, final ConstraintAnchor.Type type2, final int n, final int n2) {
        this.getAnchor(type).connect(constraintWidget.getAnchor(type2), n, n2, true);
    }
    
    public boolean isAnimated() {
        return this.mAnimated;
    }
    
    public boolean isHeightWrapContent() {
        return this.mIsHeightWrapContent;
    }
    
    public boolean isHorizontalSolvingPassDone() {
        return this.horizontalSolvingPass;
    }
    
    public boolean isInBarrier(final int n) {
        return this.mIsInBarrier[n];
    }
    
    public boolean isInHorizontalChain() {
        return (this.mLeft.mTarget != null && this.mLeft.mTarget.mTarget == this.mLeft) || (this.mRight.mTarget != null && this.mRight.mTarget.mTarget == this.mRight);
    }
    
    public boolean isInPlaceholder() {
        return this.inPlaceholder;
    }
    
    public boolean isInVerticalChain() {
        return (this.mTop.mTarget != null && this.mTop.mTarget.mTarget == this.mTop) || (this.mBottom.mTarget != null && this.mBottom.mTarget.mTarget == this.mBottom);
    }
    
    public boolean isInVirtualLayout() {
        return this.mInVirtualLayout;
    }
    
    public boolean isMeasureRequested() {
        return this.mMeasureRequested && this.mVisibility != 8;
    }
    
    public boolean isResolvedHorizontally() {
        return this.resolvedHorizontal || (this.mLeft.hasFinalValue() && this.mRight.hasFinalValue());
    }
    
    public boolean isResolvedVertically() {
        return this.resolvedVertical || (this.mTop.hasFinalValue() && this.mBottom.hasFinalValue());
    }
    
    public boolean isRoot() {
        return this.mParent == null;
    }
    
    public boolean isSpreadHeight() {
        final int mMatchConstraintDefaultHeight = this.mMatchConstraintDefaultHeight;
        boolean b = true;
        if (mMatchConstraintDefaultHeight != 0 || this.mDimensionRatio != 0.0f || this.mMatchConstraintMinHeight != 0 || this.mMatchConstraintMaxHeight != 0 || this.mListDimensionBehaviors[1] != DimensionBehaviour.MATCH_CONSTRAINT) {
            b = false;
        }
        return b;
    }
    
    public boolean isSpreadWidth() {
        final int mMatchConstraintDefaultWidth = this.mMatchConstraintDefaultWidth;
        boolean b2;
        final boolean b = b2 = false;
        if (mMatchConstraintDefaultWidth == 0) {
            b2 = b;
            if (this.mDimensionRatio == 0.0f) {
                b2 = b;
                if (this.mMatchConstraintMinWidth == 0) {
                    b2 = b;
                    if (this.mMatchConstraintMaxWidth == 0) {
                        b2 = b;
                        if (this.mListDimensionBehaviors[0] == DimensionBehaviour.MATCH_CONSTRAINT) {
                            b2 = true;
                        }
                    }
                }
            }
        }
        return b2;
    }
    
    public boolean isVerticalSolvingPassDone() {
        return this.verticalSolvingPass;
    }
    
    public boolean isWidthWrapContent() {
        return this.mIsWidthWrapContent;
    }
    
    public void markHorizontalSolvingPassDone() {
        this.horizontalSolvingPass = true;
    }
    
    public void markVerticalSolvingPassDone() {
        this.verticalSolvingPass = true;
    }
    
    public boolean oppositeDimensionDependsOn(final int n) {
        boolean b = true;
        int n2;
        if (n == 0) {
            n2 = 1;
        }
        else {
            n2 = 0;
        }
        final DimensionBehaviour[] mListDimensionBehaviors = this.mListDimensionBehaviors;
        final DimensionBehaviour dimensionBehaviour = mListDimensionBehaviors[n];
        final DimensionBehaviour dimensionBehaviour2 = mListDimensionBehaviors[n2];
        if (dimensionBehaviour != DimensionBehaviour.MATCH_CONSTRAINT || dimensionBehaviour2 != DimensionBehaviour.MATCH_CONSTRAINT) {
            b = false;
        }
        return b;
    }
    
    public boolean oppositeDimensionsTied() {
        final DimensionBehaviour[] mListDimensionBehaviors = this.mListDimensionBehaviors;
        boolean b = false;
        if (mListDimensionBehaviors[0] == DimensionBehaviour.MATCH_CONSTRAINT) {
            b = b;
            if (this.mListDimensionBehaviors[1] == DimensionBehaviour.MATCH_CONSTRAINT) {
                b = true;
            }
        }
        return b;
    }
    
    public void reset() {
        this.mLeft.reset();
        this.mTop.reset();
        this.mRight.reset();
        this.mBottom.reset();
        this.mBaseline.reset();
        this.mCenterX.reset();
        this.mCenterY.reset();
        this.mCenter.reset();
        this.mParent = null;
        this.mCircleConstraintAngle = 0.0f;
        this.mWidth = 0;
        this.mHeight = 0;
        this.mDimensionRatio = 0.0f;
        this.mDimensionRatioSide = -1;
        this.mX = 0;
        this.mY = 0;
        this.mOffsetX = 0;
        this.mOffsetY = 0;
        this.mBaselineDistance = 0;
        this.mMinWidth = 0;
        this.mMinHeight = 0;
        final float default_BIAS = ConstraintWidget.DEFAULT_BIAS;
        this.mHorizontalBiasPercent = default_BIAS;
        this.mVerticalBiasPercent = default_BIAS;
        this.mListDimensionBehaviors[0] = DimensionBehaviour.FIXED;
        this.mListDimensionBehaviors[1] = DimensionBehaviour.FIXED;
        this.mCompanionWidget = null;
        this.mContainerItemSkip = 0;
        this.mVisibility = 0;
        this.mType = null;
        this.mHorizontalWrapVisited = false;
        this.mVerticalWrapVisited = false;
        this.mHorizontalChainStyle = 0;
        this.mVerticalChainStyle = 0;
        this.mHorizontalChainFixedPosition = false;
        this.mVerticalChainFixedPosition = false;
        final float[] mWeight = this.mWeight;
        mWeight[1] = (mWeight[0] = -1.0f);
        this.mHorizontalResolution = -1;
        this.mVerticalResolution = -1;
        final int[] mMaxDimension = this.mMaxDimension;
        mMaxDimension[1] = (mMaxDimension[0] = Integer.MAX_VALUE);
        this.mMatchConstraintDefaultWidth = 0;
        this.mMatchConstraintDefaultHeight = 0;
        this.mMatchConstraintPercentWidth = 1.0f;
        this.mMatchConstraintPercentHeight = 1.0f;
        this.mMatchConstraintMaxWidth = Integer.MAX_VALUE;
        this.mMatchConstraintMaxHeight = Integer.MAX_VALUE;
        this.mMatchConstraintMinWidth = 0;
        this.mMatchConstraintMinHeight = 0;
        this.mResolvedHasRatio = false;
        this.mResolvedDimensionRatioSide = -1;
        this.mResolvedDimensionRatio = 1.0f;
        this.mGroupsToSolver = false;
        final boolean[] isTerminalWidget = this.isTerminalWidget;
        isTerminalWidget[1] = (isTerminalWidget[0] = true);
        this.mInVirtualLayout = false;
        final boolean[] mIsInBarrier = this.mIsInBarrier;
        mIsInBarrier[1] = (mIsInBarrier[0] = false);
        this.mMeasureRequested = true;
        final int[] mResolvedMatchConstraintDefault = this.mResolvedMatchConstraintDefault;
        mResolvedMatchConstraintDefault[1] = (mResolvedMatchConstraintDefault[0] = 0);
        this.mWidthOverride = -1;
        this.mHeightOverride = -1;
    }
    
    public void resetAllConstraints() {
        this.resetAnchors();
        this.setVerticalBiasPercent(ConstraintWidget.DEFAULT_BIAS);
        this.setHorizontalBiasPercent(ConstraintWidget.DEFAULT_BIAS);
    }
    
    public void resetAnchor(final ConstraintAnchor constraintAnchor) {
        if (this.getParent() != null && this.getParent() instanceof ConstraintWidgetContainer && ((ConstraintWidgetContainer)this.getParent()).handlesInternalConstraints()) {
            return;
        }
        final ConstraintAnchor anchor = this.getAnchor(ConstraintAnchor.Type.LEFT);
        final ConstraintAnchor anchor2 = this.getAnchor(ConstraintAnchor.Type.RIGHT);
        final ConstraintAnchor anchor3 = this.getAnchor(ConstraintAnchor.Type.TOP);
        final ConstraintAnchor anchor4 = this.getAnchor(ConstraintAnchor.Type.BOTTOM);
        final ConstraintAnchor anchor5 = this.getAnchor(ConstraintAnchor.Type.CENTER);
        final ConstraintAnchor anchor6 = this.getAnchor(ConstraintAnchor.Type.CENTER_X);
        final ConstraintAnchor anchor7 = this.getAnchor(ConstraintAnchor.Type.CENTER_Y);
        if (constraintAnchor == anchor5) {
            if (anchor.isConnected() && anchor2.isConnected() && anchor.getTarget() == anchor2.getTarget()) {
                anchor.reset();
                anchor2.reset();
            }
            if (anchor3.isConnected() && anchor4.isConnected() && anchor3.getTarget() == anchor4.getTarget()) {
                anchor3.reset();
                anchor4.reset();
            }
            this.mHorizontalBiasPercent = 0.5f;
            this.mVerticalBiasPercent = 0.5f;
        }
        else if (constraintAnchor == anchor6) {
            if (anchor.isConnected() && anchor2.isConnected() && anchor.getTarget().getOwner() == anchor2.getTarget().getOwner()) {
                anchor.reset();
                anchor2.reset();
            }
            this.mHorizontalBiasPercent = 0.5f;
        }
        else if (constraintAnchor == anchor7) {
            if (anchor3.isConnected() && anchor4.isConnected() && anchor3.getTarget().getOwner() == anchor4.getTarget().getOwner()) {
                anchor3.reset();
                anchor4.reset();
            }
            this.mVerticalBiasPercent = 0.5f;
        }
        else if (constraintAnchor != anchor && constraintAnchor != anchor2) {
            if ((constraintAnchor == anchor3 || constraintAnchor == anchor4) && anchor3.isConnected() && anchor3.getTarget() == anchor4.getTarget()) {
                anchor5.reset();
            }
        }
        else if (anchor.isConnected() && anchor.getTarget() == anchor2.getTarget()) {
            anchor5.reset();
        }
        constraintAnchor.reset();
    }
    
    public void resetAnchors() {
        final ConstraintWidget parent = this.getParent();
        if (parent != null && parent instanceof ConstraintWidgetContainer && ((ConstraintWidgetContainer)this.getParent()).handlesInternalConstraints()) {
            return;
        }
        for (int i = 0; i < this.mAnchors.size(); ++i) {
            this.mAnchors.get(i).reset();
        }
    }
    
    public void resetFinalResolution() {
        int i = 0;
        this.resolvedHorizontal = false;
        this.resolvedVertical = false;
        this.horizontalSolvingPass = false;
        this.verticalSolvingPass = false;
        while (i < this.mAnchors.size()) {
            this.mAnchors.get(i).resetFinalResolution();
            ++i;
        }
    }
    
    public void resetSolverVariables(final Cache cache) {
        this.mLeft.resetSolverVariable(cache);
        this.mTop.resetSolverVariable(cache);
        this.mRight.resetSolverVariable(cache);
        this.mBottom.resetSolverVariable(cache);
        this.mBaseline.resetSolverVariable(cache);
        this.mCenter.resetSolverVariable(cache);
        this.mCenterX.resetSolverVariable(cache);
        this.mCenterY.resetSolverVariable(cache);
    }
    
    public void resetSolvingPassFlag() {
        this.horizontalSolvingPass = false;
        this.verticalSolvingPass = false;
    }
    
    public StringBuilder serialize(final StringBuilder sb) {
        sb.append("{\n");
        this.serializeAnchor(sb, "left", this.mLeft);
        this.serializeAnchor(sb, "top", this.mTop);
        this.serializeAnchor(sb, "right", this.mRight);
        this.serializeAnchor(sb, "bottom", this.mBottom);
        this.serializeAnchor(sb, "baseline", this.mBaseline);
        this.serializeAnchor(sb, "centerX", this.mCenterX);
        this.serializeAnchor(sb, "centerY", this.mCenterY);
        this.serializeCircle(sb, this.mCenter, this.mCircleConstraintAngle);
        this.serializeSize(sb, "width", this.mWidth, this.mMinWidth, this.mMaxDimension[0], this.mWidthOverride, this.mMatchConstraintMinWidth, this.mMatchConstraintDefaultWidth, this.mMatchConstraintPercentWidth, this.mWeight[0]);
        this.serializeSize(sb, "height", this.mHeight, this.mMinHeight, this.mMaxDimension[1], this.mHeightOverride, this.mMatchConstraintMinHeight, this.mMatchConstraintDefaultHeight, this.mMatchConstraintPercentHeight, this.mWeight[1]);
        this.serializeDimensionRatio(sb, "dimensionRatio", this.mDimensionRatio, this.mDimensionRatioSide);
        this.serializeAttribute(sb, "horizontalBias", this.mHorizontalBiasPercent, ConstraintWidget.DEFAULT_BIAS);
        this.serializeAttribute(sb, "verticalBias", this.mVerticalBiasPercent, ConstraintWidget.DEFAULT_BIAS);
        sb.append("}\n");
        return sb;
    }
    
    public void setAnimated(final boolean mAnimated) {
        this.mAnimated = mAnimated;
    }
    
    public void setBaselineDistance(final int mBaselineDistance) {
        this.mBaselineDistance = mBaselineDistance;
        this.hasBaseline = (mBaselineDistance > 0);
    }
    
    public void setCompanionWidget(final Object mCompanionWidget) {
        this.mCompanionWidget = mCompanionWidget;
    }
    
    public void setContainerItemSkip(final int mContainerItemSkip) {
        if (mContainerItemSkip >= 0) {
            this.mContainerItemSkip = mContainerItemSkip;
        }
        else {
            this.mContainerItemSkip = 0;
        }
    }
    
    public void setDebugName(final String mDebugName) {
        this.mDebugName = mDebugName;
    }
    
    public void setDebugSolverName(final LinearSystem linearSystem, final String s) {
        this.mDebugName = s;
        final SolverVariable objectVariable = linearSystem.createObjectVariable(this.mLeft);
        final SolverVariable objectVariable2 = linearSystem.createObjectVariable(this.mTop);
        final SolverVariable objectVariable3 = linearSystem.createObjectVariable(this.mRight);
        final SolverVariable objectVariable4 = linearSystem.createObjectVariable(this.mBottom);
        final StringBuilder sb = new StringBuilder();
        sb.append(s);
        sb.append(".left");
        objectVariable.setName(sb.toString());
        final StringBuilder sb2 = new StringBuilder();
        sb2.append(s);
        sb2.append(".top");
        objectVariable2.setName(sb2.toString());
        final StringBuilder sb3 = new StringBuilder();
        sb3.append(s);
        sb3.append(".right");
        objectVariable3.setName(sb3.toString());
        final StringBuilder sb4 = new StringBuilder();
        sb4.append(s);
        sb4.append(".bottom");
        objectVariable4.setName(sb4.toString());
        final SolverVariable objectVariable5 = linearSystem.createObjectVariable(this.mBaseline);
        final StringBuilder sb5 = new StringBuilder();
        sb5.append(s);
        sb5.append(".baseline");
        objectVariable5.setName(sb5.toString());
    }
    
    public void setDimension(int mMinHeight, final int mHeight) {
        this.mWidth = mMinHeight;
        final int mMinWidth = this.mMinWidth;
        if (mMinHeight < mMinWidth) {
            this.mWidth = mMinWidth;
        }
        this.mHeight = mHeight;
        mMinHeight = this.mMinHeight;
        if (mHeight < mMinHeight) {
            this.mHeight = mMinHeight;
        }
    }
    
    public void setDimensionRatio(final float mDimensionRatio, final int mDimensionRatioSide) {
        this.mDimensionRatio = mDimensionRatio;
        this.mDimensionRatioSide = mDimensionRatioSide;
    }
    
    public void setDimensionRatio(String s) {
        if (s != null && s.length() != 0) {
            final int n = -1;
            final int length = s.length();
            final int index = s.indexOf(44);
            final int n2 = 0;
            int mDimensionRatioSide = n;
            int n3 = n2;
            if (index > 0) {
                mDimensionRatioSide = n;
                n3 = n2;
                if (index < length - 1) {
                    final String substring = s.substring(0, index);
                    if (substring.equalsIgnoreCase("W")) {
                        mDimensionRatioSide = 0;
                    }
                    else {
                        mDimensionRatioSide = n;
                        if (substring.equalsIgnoreCase("H")) {
                            mDimensionRatioSide = 1;
                        }
                    }
                    n3 = index + 1;
                }
            }
            final int index2 = s.indexOf(58);
            float mDimensionRatio = 0.0f;
            Label_0244: {
                Label_0219: {
                    if (index2 < 0 || index2 >= length - 1) {
                        break Label_0219;
                    }
                    final String substring2 = s.substring(n3, index2);
                    s = s.substring(index2 + 1);
                    if (substring2.length() <= 0 || s.length() <= 0) {
                        break Label_0219;
                    }
                    try {
                        final float float1 = Float.parseFloat(substring2);
                        final float float2 = Float.parseFloat(s);
                        if (float1 <= 0.0f || float2 <= 0.0f) {
                            break Label_0219;
                        }
                        if (mDimensionRatioSide == 1) {
                            mDimensionRatio = Math.abs(float2 / float1);
                            break Label_0244;
                        }
                        mDimensionRatio = Math.abs(float1 / float2);
                        break Label_0244;
                        s = s.substring(n3);
                        iftrue(Label_0242:)(s.length() <= 0);
                        mDimensionRatio = Float.parseFloat(s);
                        break Label_0244;
                    }
                    catch (final NumberFormatException ex) {}
                }
                mDimensionRatio = 0.0f;
            }
            if (mDimensionRatio > 0.0f) {
                this.mDimensionRatio = mDimensionRatio;
                this.mDimensionRatioSide = mDimensionRatioSide;
            }
            return;
        }
        this.mDimensionRatio = 0.0f;
    }
    
    public void setFinalBaseline(final int finalValue) {
        if (!this.hasBaseline) {
            return;
        }
        final int n = finalValue - this.mBaselineDistance;
        final int mHeight = this.mHeight;
        this.mY = n;
        this.mTop.setFinalValue(n);
        this.mBottom.setFinalValue(mHeight + n);
        this.mBaseline.setFinalValue(finalValue);
        this.resolvedVertical = true;
    }
    
    public void setFinalFrame(final int n, final int n2, final int n3, final int n4, final int baselineDistance, final int n5) {
        this.setFrame(n, n2, n3, n4);
        this.setBaselineDistance(baselineDistance);
        if (n5 == 0) {
            this.resolvedHorizontal = true;
            this.resolvedVertical = false;
        }
        else if (n5 == 1) {
            this.resolvedHorizontal = false;
            this.resolvedVertical = true;
        }
        else if (n5 == 2) {
            this.resolvedHorizontal = true;
            this.resolvedVertical = true;
        }
        else {
            this.resolvedHorizontal = false;
            this.resolvedVertical = false;
        }
    }
    
    public void setFinalHorizontal(final int n, final int finalValue) {
        if (this.resolvedHorizontal) {
            return;
        }
        this.mLeft.setFinalValue(n);
        this.mRight.setFinalValue(finalValue);
        this.mX = n;
        this.mWidth = finalValue - n;
        this.resolvedHorizontal = true;
    }
    
    public void setFinalLeft(final int n) {
        this.mLeft.setFinalValue(n);
        this.mX = n;
    }
    
    public void setFinalTop(final int n) {
        this.mTop.setFinalValue(n);
        this.mY = n;
    }
    
    public void setFinalVertical(final int n, final int finalValue) {
        if (this.resolvedVertical) {
            return;
        }
        this.mTop.setFinalValue(n);
        this.mBottom.setFinalValue(finalValue);
        this.mY = n;
        this.mHeight = finalValue - n;
        if (this.hasBaseline) {
            this.mBaseline.setFinalValue(n + this.mBaselineDistance);
        }
        this.resolvedVertical = true;
    }
    
    public void setFrame(final int n, final int n2, final int n3) {
        if (n3 == 0) {
            this.setHorizontalDimension(n, n2);
        }
        else if (n3 == 1) {
            this.setVerticalDimension(n, n2);
        }
    }
    
    public void setFrame(int mHeight, int mWidth, int mWidthOverride, int mHeight2) {
        final int n = mWidthOverride - mHeight;
        mWidthOverride = mHeight2 - mWidth;
        this.mX = mHeight;
        this.mY = mWidth;
        if (this.mVisibility == 8) {
            this.mWidth = 0;
            this.mHeight = 0;
            return;
        }
        mHeight = n;
        if (this.mListDimensionBehaviors[0] == DimensionBehaviour.FIXED) {
            mWidth = this.mWidth;
            if ((mHeight = n) < mWidth) {
                mHeight = mWidth;
            }
        }
        mWidth = mWidthOverride;
        if (this.mListDimensionBehaviors[1] == DimensionBehaviour.FIXED) {
            mHeight2 = this.mHeight;
            if ((mWidth = mWidthOverride) < mHeight2) {
                mWidth = mHeight2;
            }
        }
        this.mWidth = mHeight;
        this.mHeight = mWidth;
        mWidthOverride = this.mMinHeight;
        if (mWidth < mWidthOverride) {
            this.mHeight = mWidthOverride;
        }
        mWidthOverride = this.mMinWidth;
        if (mHeight < mWidthOverride) {
            this.mWidth = mWidthOverride;
        }
        if (this.mMatchConstraintMaxWidth > 0 && this.mListDimensionBehaviors[0] == DimensionBehaviour.MATCH_CONSTRAINT) {
            this.mWidth = Math.min(this.mWidth, this.mMatchConstraintMaxWidth);
        }
        if (this.mMatchConstraintMaxHeight > 0 && this.mListDimensionBehaviors[1] == DimensionBehaviour.MATCH_CONSTRAINT) {
            this.mHeight = Math.min(this.mHeight, this.mMatchConstraintMaxHeight);
        }
        mWidthOverride = this.mWidth;
        if (mHeight != mWidthOverride) {
            this.mWidthOverride = mWidthOverride;
        }
        mHeight = this.mHeight;
        if (mWidth != mHeight) {
            this.mHeightOverride = mHeight;
        }
    }
    
    public void setGoneMargin(final ConstraintAnchor.Type type, final int mGoneMargin) {
        final int n = ConstraintWidget$1.$SwitchMap$androidx$constraintlayout$core$widgets$ConstraintAnchor$Type[type.ordinal()];
        if (n != 1) {
            if (n != 2) {
                if (n != 3) {
                    if (n != 4) {
                        if (n == 5) {
                            this.mBaseline.mGoneMargin = mGoneMargin;
                        }
                    }
                    else {
                        this.mBottom.mGoneMargin = mGoneMargin;
                    }
                }
                else {
                    this.mRight.mGoneMargin = mGoneMargin;
                }
            }
            else {
                this.mTop.mGoneMargin = mGoneMargin;
            }
        }
        else {
            this.mLeft.mGoneMargin = mGoneMargin;
        }
    }
    
    public void setHasBaseline(final boolean hasBaseline) {
        this.hasBaseline = hasBaseline;
    }
    
    public void setHeight(final int mHeight) {
        this.mHeight = mHeight;
        final int mMinHeight = this.mMinHeight;
        if (mHeight < mMinHeight) {
            this.mHeight = mMinHeight;
        }
    }
    
    public void setHeightWrapContent(final boolean mIsHeightWrapContent) {
        this.mIsHeightWrapContent = mIsHeightWrapContent;
    }
    
    public void setHorizontalBiasPercent(final float mHorizontalBiasPercent) {
        this.mHorizontalBiasPercent = mHorizontalBiasPercent;
    }
    
    public void setHorizontalChainStyle(final int mHorizontalChainStyle) {
        this.mHorizontalChainStyle = mHorizontalChainStyle;
    }
    
    public void setHorizontalDimension(int n, int mMinWidth) {
        this.mX = n;
        n = mMinWidth - n;
        this.mWidth = n;
        mMinWidth = this.mMinWidth;
        if (n < mMinWidth) {
            this.mWidth = mMinWidth;
        }
    }
    
    public void setHorizontalDimensionBehaviour(final DimensionBehaviour dimensionBehaviour) {
        this.mListDimensionBehaviors[0] = dimensionBehaviour;
    }
    
    public void setHorizontalMatchStyle(final int mMatchConstraintDefaultWidth, int n, final int n2, final float mMatchConstraintPercentWidth) {
        this.mMatchConstraintDefaultWidth = mMatchConstraintDefaultWidth;
        this.mMatchConstraintMinWidth = n;
        n = n2;
        if (n2 == Integer.MAX_VALUE) {
            n = 0;
        }
        this.mMatchConstraintMaxWidth = n;
        this.mMatchConstraintPercentWidth = mMatchConstraintPercentWidth;
        if (mMatchConstraintPercentWidth > 0.0f && mMatchConstraintPercentWidth < 1.0f && mMatchConstraintDefaultWidth == 0) {
            this.mMatchConstraintDefaultWidth = 2;
        }
    }
    
    public void setHorizontalWeight(final float n) {
        this.mWeight[0] = n;
    }
    
    protected void setInBarrier(final int n, final boolean b) {
        this.mIsInBarrier[n] = b;
    }
    
    public void setInPlaceholder(final boolean inPlaceholder) {
        this.inPlaceholder = inPlaceholder;
    }
    
    public void setInVirtualLayout(final boolean mInVirtualLayout) {
        this.mInVirtualLayout = mInVirtualLayout;
    }
    
    public void setLastMeasureSpec(final int mLastHorizontalMeasureSpec, final int mLastVerticalMeasureSpec) {
        this.mLastHorizontalMeasureSpec = mLastHorizontalMeasureSpec;
        this.mLastVerticalMeasureSpec = mLastVerticalMeasureSpec;
        this.setMeasureRequested(false);
    }
    
    public void setLength(final int n, final int n2) {
        if (n2 == 0) {
            this.setWidth(n);
        }
        else if (n2 == 1) {
            this.setHeight(n);
        }
    }
    
    public void setMaxHeight(final int n) {
        this.mMaxDimension[1] = n;
    }
    
    public void setMaxWidth(final int n) {
        this.mMaxDimension[0] = n;
    }
    
    public void setMeasureRequested(final boolean mMeasureRequested) {
        this.mMeasureRequested = mMeasureRequested;
    }
    
    public void setMinHeight(final int mMinHeight) {
        if (mMinHeight < 0) {
            this.mMinHeight = 0;
        }
        else {
            this.mMinHeight = mMinHeight;
        }
    }
    
    public void setMinWidth(final int mMinWidth) {
        if (mMinWidth < 0) {
            this.mMinWidth = 0;
        }
        else {
            this.mMinWidth = mMinWidth;
        }
    }
    
    public void setOffset(final int mOffsetX, final int mOffsetY) {
        this.mOffsetX = mOffsetX;
        this.mOffsetY = mOffsetY;
    }
    
    public void setOrigin(final int mx, final int my) {
        this.mX = mx;
        this.mY = my;
    }
    
    public void setParent(final ConstraintWidget mParent) {
        this.mParent = mParent;
    }
    
    void setRelativePositioning(final int n, final int n2) {
        if (n2 == 0) {
            this.mRelX = n;
        }
        else if (n2 == 1) {
            this.mRelY = n;
        }
    }
    
    public void setType(final String mType) {
        this.mType = mType;
    }
    
    public void setVerticalBiasPercent(final float mVerticalBiasPercent) {
        this.mVerticalBiasPercent = mVerticalBiasPercent;
    }
    
    public void setVerticalChainStyle(final int mVerticalChainStyle) {
        this.mVerticalChainStyle = mVerticalChainStyle;
    }
    
    public void setVerticalDimension(int n, int mMinHeight) {
        this.mY = n;
        n = mMinHeight - n;
        this.mHeight = n;
        mMinHeight = this.mMinHeight;
        if (n < mMinHeight) {
            this.mHeight = mMinHeight;
        }
    }
    
    public void setVerticalDimensionBehaviour(final DimensionBehaviour dimensionBehaviour) {
        this.mListDimensionBehaviors[1] = dimensionBehaviour;
    }
    
    public void setVerticalMatchStyle(final int mMatchConstraintDefaultHeight, int n, final int n2, final float mMatchConstraintPercentHeight) {
        this.mMatchConstraintDefaultHeight = mMatchConstraintDefaultHeight;
        this.mMatchConstraintMinHeight = n;
        n = n2;
        if (n2 == Integer.MAX_VALUE) {
            n = 0;
        }
        this.mMatchConstraintMaxHeight = n;
        this.mMatchConstraintPercentHeight = mMatchConstraintPercentHeight;
        if (mMatchConstraintPercentHeight > 0.0f && mMatchConstraintPercentHeight < 1.0f && mMatchConstraintDefaultHeight == 0) {
            this.mMatchConstraintDefaultHeight = 2;
        }
    }
    
    public void setVerticalWeight(final float n) {
        this.mWeight[1] = n;
    }
    
    public void setVisibility(final int mVisibility) {
        this.mVisibility = mVisibility;
    }
    
    public void setWidth(final int mWidth) {
        this.mWidth = mWidth;
        final int mMinWidth = this.mMinWidth;
        if (mWidth < mMinWidth) {
            this.mWidth = mMinWidth;
        }
    }
    
    public void setWidthWrapContent(final boolean mIsWidthWrapContent) {
        this.mIsWidthWrapContent = mIsWidthWrapContent;
    }
    
    public void setWrapBehaviorInParent(final int mWrapBehaviorInParent) {
        if (mWrapBehaviorInParent >= 0 && mWrapBehaviorInParent <= 3) {
            this.mWrapBehaviorInParent = mWrapBehaviorInParent;
        }
    }
    
    public void setX(final int mx) {
        this.mX = mx;
    }
    
    public void setY(final int my) {
        this.mY = my;
    }
    
    public void setupDimensionRatio(final boolean b, final boolean b2, final boolean b3, final boolean b4) {
        if (this.mResolvedDimensionRatioSide == -1) {
            if (b3 && !b4) {
                this.mResolvedDimensionRatioSide = 0;
            }
            else if (!b3 && b4) {
                this.mResolvedDimensionRatioSide = 1;
                if (this.mDimensionRatioSide == -1) {
                    this.mResolvedDimensionRatio = 1.0f / this.mResolvedDimensionRatio;
                }
            }
        }
        if (this.mResolvedDimensionRatioSide == 0 && (!this.mTop.isConnected() || !this.mBottom.isConnected())) {
            this.mResolvedDimensionRatioSide = 1;
        }
        else if (this.mResolvedDimensionRatioSide == 1 && (!this.mLeft.isConnected() || !this.mRight.isConnected())) {
            this.mResolvedDimensionRatioSide = 0;
        }
        if (this.mResolvedDimensionRatioSide == -1 && (!this.mTop.isConnected() || !this.mBottom.isConnected() || !this.mLeft.isConnected() || !this.mRight.isConnected())) {
            if (this.mTop.isConnected() && this.mBottom.isConnected()) {
                this.mResolvedDimensionRatioSide = 0;
            }
            else if (this.mLeft.isConnected() && this.mRight.isConnected()) {
                this.mResolvedDimensionRatio = 1.0f / this.mResolvedDimensionRatio;
                this.mResolvedDimensionRatioSide = 1;
            }
        }
        if (this.mResolvedDimensionRatioSide == -1) {
            final int mMatchConstraintMinWidth = this.mMatchConstraintMinWidth;
            if (mMatchConstraintMinWidth > 0 && this.mMatchConstraintMinHeight == 0) {
                this.mResolvedDimensionRatioSide = 0;
            }
            else if (mMatchConstraintMinWidth == 0 && this.mMatchConstraintMinHeight > 0) {
                this.mResolvedDimensionRatio = 1.0f / this.mResolvedDimensionRatio;
                this.mResolvedDimensionRatioSide = 1;
            }
        }
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        final String mType = this.mType;
        final String s = "";
        String string;
        if (mType != null) {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("type: ");
            sb2.append(this.mType);
            sb2.append(" ");
            string = sb2.toString();
        }
        else {
            string = "";
        }
        sb.append(string);
        String string2 = s;
        if (this.mDebugName != null) {
            final StringBuilder sb3 = new StringBuilder();
            sb3.append("id: ");
            sb3.append(this.mDebugName);
            sb3.append(" ");
            string2 = sb3.toString();
        }
        sb.append(string2);
        sb.append("(");
        sb.append(this.mX);
        sb.append(", ");
        sb.append(this.mY);
        sb.append(") - (");
        sb.append(this.mWidth);
        sb.append(" x ");
        sb.append(this.mHeight);
        sb.append(")");
        return sb.toString();
    }
    
    public void updateFromRuns(final boolean b, final boolean b2) {
        final boolean b3 = b & this.horizontalRun.isResolved();
        final boolean b4 = b2 & this.verticalRun.isResolved();
        int value = this.horizontalRun.start.value;
        int value2 = this.verticalRun.start.value;
        int value3 = this.horizontalRun.end.value;
        final int value4 = this.verticalRun.end.value;
        int n;
        if (value3 - value < 0 || value4 - value2 < 0 || value == Integer.MIN_VALUE || value == Integer.MAX_VALUE || value2 == Integer.MIN_VALUE || value2 == Integer.MAX_VALUE || value3 == Integer.MIN_VALUE || value3 == Integer.MAX_VALUE || value4 == Integer.MIN_VALUE || (n = value4) == Integer.MAX_VALUE) {
            value = 0;
            value2 = 0;
            value3 = 0;
            n = 0;
        }
        final int n2 = value3 - value;
        final int n3 = n - value2;
        if (b3) {
            this.mX = value;
        }
        if (b4) {
            this.mY = value2;
        }
        if (this.mVisibility == 8) {
            this.mWidth = 0;
            this.mHeight = 0;
            return;
        }
        if (b3) {
            int mWidth = n2;
            if (this.mListDimensionBehaviors[0] == DimensionBehaviour.FIXED) {
                final int mWidth2 = this.mWidth;
                if ((mWidth = n2) < mWidth2) {
                    mWidth = mWidth2;
                }
            }
            this.mWidth = mWidth;
            final int mMinWidth = this.mMinWidth;
            if (mWidth < mMinWidth) {
                this.mWidth = mMinWidth;
            }
        }
        if (b4) {
            int mHeight = n3;
            if (this.mListDimensionBehaviors[1] == DimensionBehaviour.FIXED) {
                final int mHeight2 = this.mHeight;
                if ((mHeight = n3) < mHeight2) {
                    mHeight = mHeight2;
                }
            }
            this.mHeight = mHeight;
            final int mMinHeight = this.mMinHeight;
            if (mHeight < mMinHeight) {
                this.mHeight = mMinHeight;
            }
        }
    }
    
    public void updateFromSolver(final LinearSystem linearSystem, final boolean b) {
        final int objectVariableValue = linearSystem.getObjectVariableValue(this.mLeft);
        final int objectVariableValue2 = linearSystem.getObjectVariableValue(this.mTop);
        final int objectVariableValue3 = linearSystem.getObjectVariableValue(this.mRight);
        final int objectVariableValue4 = linearSystem.getObjectVariableValue(this.mBottom);
        int value = objectVariableValue;
        int value2 = objectVariableValue3;
        if (b) {
            final HorizontalWidgetRun horizontalRun = this.horizontalRun;
            value = objectVariableValue;
            value2 = objectVariableValue3;
            if (horizontalRun != null) {
                value = objectVariableValue;
                value2 = objectVariableValue3;
                if (horizontalRun.start.resolved) {
                    value = objectVariableValue;
                    value2 = objectVariableValue3;
                    if (this.horizontalRun.end.resolved) {
                        value = this.horizontalRun.start.value;
                        value2 = this.horizontalRun.end.value;
                    }
                }
            }
        }
        int value3 = objectVariableValue2;
        int value4 = objectVariableValue4;
        if (b) {
            final VerticalWidgetRun verticalRun = this.verticalRun;
            value3 = objectVariableValue2;
            value4 = objectVariableValue4;
            if (verticalRun != null) {
                value3 = objectVariableValue2;
                value4 = objectVariableValue4;
                if (verticalRun.start.resolved) {
                    value3 = objectVariableValue2;
                    value4 = objectVariableValue4;
                    if (this.verticalRun.end.resolved) {
                        value3 = this.verticalRun.start.value;
                        value4 = this.verticalRun.end.value;
                    }
                }
            }
        }
        int n = 0;
        int n2 = 0;
        int n3 = 0;
        Label_0313: {
            if (value2 - value >= 0 && value4 - value3 >= 0 && value != Integer.MIN_VALUE && value != Integer.MAX_VALUE && value3 != Integer.MIN_VALUE && value3 != Integer.MAX_VALUE && value2 != Integer.MIN_VALUE && value2 != Integer.MAX_VALUE && value4 != Integer.MIN_VALUE) {
                n = value;
                n2 = value2;
                if ((n3 = value4) != Integer.MAX_VALUE) {
                    break Label_0313;
                }
            }
            n3 = 0;
            n = 0;
            value3 = 0;
            n2 = 0;
        }
        this.setFrame(n, value3, n2, n3);
    }
    
    public enum DimensionBehaviour
    {
        private static final DimensionBehaviour[] $VALUES;
        
        FIXED, 
        MATCH_CONSTRAINT, 
        MATCH_PARENT, 
        WRAP_CONTENT;
    }
}

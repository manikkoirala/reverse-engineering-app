// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.core.widgets;

import androidx.constraintlayout.core.ArrayRow;
import java.util.ArrayList;
import androidx.constraintlayout.core.SolverVariable;
import androidx.constraintlayout.core.LinearSystem;

public class Chain
{
    private static final boolean DEBUG = false;
    public static final boolean USE_CHAIN_OPTIMIZATION = false;
    
    static void applyChainConstraints(final ConstraintWidgetContainer constraintWidgetContainer, final LinearSystem linearSystem, int n, int margin, final ChainHead chainHead) {
        final ConstraintWidget mFirst = chainHead.mFirst;
        final ConstraintWidget mLast = chainHead.mLast;
        final ConstraintWidget mFirstVisibleWidget = chainHead.mFirstVisibleWidget;
        final ConstraintWidget mLastVisibleWidget = chainHead.mLastVisibleWidget;
        final ConstraintWidget mHead = chainHead.mHead;
        final float mTotalWeight = chainHead.mTotalWeight;
        final ConstraintWidget mFirstMatchConstraintWidget = chainHead.mFirstMatchConstraintWidget;
        final ConstraintWidget mLastMatchConstraintWidget = chainHead.mLastMatchConstraintWidget;
        final boolean b = constraintWidgetContainer.mListDimensionBehaviors[n] == ConstraintWidget.DimensionBehaviour.WRAP_CONTENT;
        int n4 = 0;
        int n5 = 0;
        boolean b2 = false;
        Label_0205: {
            Label_0202: {
                int n2;
                int n3;
                if (n == 0) {
                    if (mHead.mHorizontalChainStyle == 0) {
                        n2 = 1;
                    }
                    else {
                        n2 = 0;
                    }
                    if (mHead.mHorizontalChainStyle == 1) {
                        n3 = 1;
                    }
                    else {
                        n3 = 0;
                    }
                    n4 = n2;
                    n5 = n3;
                    if (mHead.mHorizontalChainStyle != 2) {
                        break Label_0202;
                    }
                }
                else {
                    if (mHead.mVerticalChainStyle == 0) {
                        n2 = 1;
                    }
                    else {
                        n2 = 0;
                    }
                    if (mHead.mVerticalChainStyle == 1) {
                        n3 = 1;
                    }
                    else {
                        n3 = 0;
                    }
                    n4 = n2;
                    n5 = n3;
                    if (mHead.mVerticalChainStyle != 2) {
                        break Label_0202;
                    }
                }
                b2 = true;
                n4 = n2;
                n5 = n3;
                break Label_0205;
            }
            b2 = false;
        }
        int n6 = 0;
        ConstraintWidget constraintWidget = mFirst;
        final int n7 = n4;
        SolverVariable solverVariable;
        while (true) {
            solverVariable = null;
            final ConstraintWidget constraintWidget2 = null;
            if (n6 != 0) {
                break;
            }
            final ConstraintAnchor constraintAnchor = constraintWidget.mListAnchors[margin];
            int n8;
            if (b2) {
                n8 = 1;
            }
            else {
                n8 = 4;
            }
            final int margin2 = constraintAnchor.getMargin();
            final boolean b3 = constraintWidget.mListDimensionBehaviors[n] == ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT && constraintWidget.mResolvedMatchConstraintDefault[n] == 0;
            int n9 = margin2;
            if (constraintAnchor.mTarget != null) {
                n9 = margin2;
                if (constraintWidget != mFirst) {
                    n9 = margin2 + constraintAnchor.mTarget.getMargin();
                }
            }
            int n10;
            if (b2 && constraintWidget != mFirst && constraintWidget != mFirstVisibleWidget) {
                n10 = 8;
            }
            else {
                n10 = n8;
            }
            if (constraintAnchor.mTarget != null) {
                if (constraintWidget == mFirstVisibleWidget) {
                    linearSystem.addGreaterThan(constraintAnchor.mSolverVariable, constraintAnchor.mTarget.mSolverVariable, n9, 6);
                }
                else {
                    linearSystem.addGreaterThan(constraintAnchor.mSolverVariable, constraintAnchor.mTarget.mSolverVariable, n9, 8);
                }
                int n11 = n10;
                if (b3) {
                    n11 = n10;
                    if (!b2) {
                        n11 = 5;
                    }
                }
                if (constraintWidget == mFirstVisibleWidget && b2 && constraintWidget.isInBarrier(n)) {
                    n11 = 5;
                }
                linearSystem.addEquality(constraintAnchor.mSolverVariable, constraintAnchor.mTarget.mSolverVariable, n9, n11);
            }
            if (b) {
                if (constraintWidget.getVisibility() != 8 && constraintWidget.mListDimensionBehaviors[n] == ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT) {
                    linearSystem.addGreaterThan(constraintWidget.mListAnchors[margin + 1].mSolverVariable, constraintWidget.mListAnchors[margin].mSolverVariable, 0, 5);
                }
                linearSystem.addGreaterThan(constraintWidget.mListAnchors[margin].mSolverVariable, constraintWidgetContainer.mListAnchors[margin].mSolverVariable, 0, 8);
            }
            final ConstraintAnchor mTarget = constraintWidget.mListAnchors[margin + 1].mTarget;
            ConstraintWidget constraintWidget3 = constraintWidget2;
            if (mTarget != null) {
                final ConstraintWidget mOwner = mTarget.mOwner;
                constraintWidget3 = constraintWidget2;
                if (mOwner.mListAnchors[margin].mTarget != null) {
                    if (mOwner.mListAnchors[margin].mTarget.mOwner != constraintWidget) {
                        constraintWidget3 = constraintWidget2;
                    }
                    else {
                        constraintWidget3 = mOwner;
                    }
                }
            }
            if (constraintWidget3 != null) {
                constraintWidget = constraintWidget3;
            }
            else {
                n6 = 1;
            }
        }
        if (mLastVisibleWidget != null) {
            final ConstraintAnchor[] mListAnchors = mLast.mListAnchors;
            final int n12 = margin + 1;
            if (mListAnchors[n12].mTarget != null) {
                final ConstraintAnchor constraintAnchor2 = mLastVisibleWidget.mListAnchors[n12];
                if (mLastVisibleWidget.mListDimensionBehaviors[n] == ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT && mLastVisibleWidget.mResolvedMatchConstraintDefault[n] == 0 && !b2 && constraintAnchor2.mTarget.mOwner == constraintWidgetContainer) {
                    linearSystem.addEquality(constraintAnchor2.mSolverVariable, constraintAnchor2.mTarget.mSolverVariable, -constraintAnchor2.getMargin(), 5);
                }
                else if (b2 && constraintAnchor2.mTarget.mOwner == constraintWidgetContainer) {
                    linearSystem.addEquality(constraintAnchor2.mSolverVariable, constraintAnchor2.mTarget.mSolverVariable, -constraintAnchor2.getMargin(), 4);
                }
                linearSystem.addLowerThan(constraintAnchor2.mSolverVariable, mLast.mListAnchors[n12].mTarget.mSolverVariable, -constraintAnchor2.getMargin(), 6);
            }
        }
        if (b) {
            final ConstraintAnchor[] mListAnchors2 = constraintWidgetContainer.mListAnchors;
            final int n13 = margin + 1;
            linearSystem.addGreaterThan(mListAnchors2[n13].mSolverVariable, mLast.mListAnchors[n13].mSolverVariable, mLast.mListAnchors[n13].getMargin(), 8);
        }
        final ArrayList<ConstraintWidget> mWeightedMatchConstraintsWidgets = chainHead.mWeightedMatchConstraintsWidgets;
        if (mWeightedMatchConstraintsWidgets != null) {
            final int size = mWeightedMatchConstraintsWidgets.size();
            if (size > 1) {
                float n14;
                if (chainHead.mHasUndefinedWeights && !chainHead.mHasComplexMatchWeights) {
                    n14 = (float)chainHead.mWidgetsMatchCount;
                }
                else {
                    n14 = mTotalWeight;
                }
                ConstraintWidget constraintWidget4 = null;
                int i = 0;
                float n15 = 0.0f;
                while (i < size) {
                    final ConstraintWidget constraintWidget5 = mWeightedMatchConstraintsWidgets.get(i);
                    float n16 = constraintWidget5.mWeight[n];
                    Label_1202: {
                        Label_1092: {
                            if (n16 < 0.0f) {
                                if (chainHead.mHasComplexMatchWeights) {
                                    linearSystem.addEquality(constraintWidget5.mListAnchors[margin + 1].mSolverVariable, constraintWidget5.mListAnchors[margin].mSolverVariable, 0, 4);
                                    break Label_1092;
                                }
                                n16 = 1.0f;
                            }
                            if (n16 != 0.0f) {
                                if (constraintWidget4 != null) {
                                    final SolverVariable mSolverVariable = constraintWidget4.mListAnchors[margin].mSolverVariable;
                                    final ConstraintAnchor[] mListAnchors3 = constraintWidget4.mListAnchors;
                                    final int n17 = margin + 1;
                                    final SolverVariable mSolverVariable2 = mListAnchors3[n17].mSolverVariable;
                                    final SolverVariable mSolverVariable3 = constraintWidget5.mListAnchors[margin].mSolverVariable;
                                    final SolverVariable mSolverVariable4 = constraintWidget5.mListAnchors[n17].mSolverVariable;
                                    final ArrayRow row = linearSystem.createRow();
                                    row.createRowEqualMatchDimensions(n15, n14, n16, mSolverVariable, mSolverVariable2, mSolverVariable3, mSolverVariable4);
                                    linearSystem.addConstraint(row);
                                }
                                constraintWidget4 = constraintWidget5;
                                break Label_1202;
                            }
                            linearSystem.addEquality(constraintWidget5.mListAnchors[margin + 1].mSolverVariable, constraintWidget5.mListAnchors[margin].mSolverVariable, 0, 8);
                        }
                        n16 = n15;
                    }
                    ++i;
                    n15 = n16;
                }
            }
        }
        if (mFirstVisibleWidget != null && (mFirstVisibleWidget == mLastVisibleWidget || b2)) {
            final ConstraintAnchor constraintAnchor3 = mFirst.mListAnchors[margin];
            final ConstraintAnchor[] mListAnchors4 = mLast.mListAnchors;
            final int n18 = margin + 1;
            ConstraintAnchor constraintAnchor4 = mListAnchors4[n18];
            SolverVariable mSolverVariable5;
            if (constraintAnchor3.mTarget != null) {
                mSolverVariable5 = constraintAnchor3.mTarget.mSolverVariable;
            }
            else {
                mSolverVariable5 = null;
            }
            SolverVariable mSolverVariable6;
            if (constraintAnchor4.mTarget != null) {
                mSolverVariable6 = constraintAnchor4.mTarget.mSolverVariable;
            }
            else {
                mSolverVariable6 = null;
            }
            final ConstraintAnchor constraintAnchor5 = mFirstVisibleWidget.mListAnchors[margin];
            if (mLastVisibleWidget != null) {
                constraintAnchor4 = mLastVisibleWidget.mListAnchors[n18];
            }
            if (mSolverVariable5 != null && mSolverVariable6 != null) {
                float n19;
                if (n == 0) {
                    n19 = mHead.mHorizontalBiasPercent;
                }
                else {
                    n19 = mHead.mVerticalBiasPercent;
                }
                final int margin3 = constraintAnchor5.getMargin();
                n = constraintAnchor4.getMargin();
                linearSystem.addCentering(constraintAnchor5.mSolverVariable, mSolverVariable5, margin3, n19, mSolverVariable6, constraintAnchor4.mSolverVariable, n, 7);
            }
        }
        else if (n7 != 0 && mFirstVisibleWidget != null) {
            final boolean b4 = chainHead.mWidgetsMatchCount > 0 && chainHead.mWidgetsCount == chainHead.mWidgetsMatchCount;
            ConstraintWidget constraintWidget7;
            ConstraintWidget constraintWidget8;
            for (ConstraintWidget constraintWidget6 = constraintWidget7 = mFirstVisibleWidget; constraintWidget7 != null; constraintWidget7 = constraintWidget8) {
                for (constraintWidget8 = constraintWidget7.mNextChainWidget[n]; constraintWidget8 != null && constraintWidget8.getVisibility() == 8; constraintWidget8 = constraintWidget8.mNextChainWidget[n]) {}
                if (constraintWidget8 != null || constraintWidget7 == mLastVisibleWidget) {
                    final ConstraintAnchor constraintAnchor6 = constraintWidget7.mListAnchors[margin];
                    final SolverVariable mSolverVariable7 = constraintAnchor6.mSolverVariable;
                    SolverVariable solverVariable2;
                    if (constraintAnchor6.mTarget != null) {
                        solverVariable2 = constraintAnchor6.mTarget.mSolverVariable;
                    }
                    else {
                        solverVariable2 = null;
                    }
                    if (constraintWidget6 != constraintWidget7) {
                        solverVariable2 = constraintWidget6.mListAnchors[margin + 1].mSolverVariable;
                    }
                    else if (constraintWidget7 == mFirstVisibleWidget) {
                        if (mFirst.mListAnchors[margin].mTarget != null) {
                            solverVariable2 = mFirst.mListAnchors[margin].mTarget.mSolverVariable;
                        }
                        else {
                            solverVariable2 = null;
                        }
                    }
                    final int margin4 = constraintAnchor6.getMargin();
                    final ConstraintAnchor[] mListAnchors5 = constraintWidget7.mListAnchors;
                    final int n20 = margin + 1;
                    final int margin5 = mListAnchors5[n20].getMargin();
                    ConstraintAnchor mTarget2;
                    SolverVariable solverVariable3;
                    if (constraintWidget8 != null) {
                        mTarget2 = constraintWidget8.mListAnchors[margin];
                        solverVariable3 = mTarget2.mSolverVariable;
                    }
                    else {
                        mTarget2 = mLast.mListAnchors[n20].mTarget;
                        if (mTarget2 != null) {
                            solverVariable3 = mTarget2.mSolverVariable;
                        }
                        else {
                            solverVariable3 = null;
                        }
                    }
                    final SolverVariable mSolverVariable8 = constraintWidget7.mListAnchors[n20].mSolverVariable;
                    int margin6 = margin5;
                    if (mTarget2 != null) {
                        margin6 = margin5 + mTarget2.getMargin();
                    }
                    int margin7 = margin4 + constraintWidget6.mListAnchors[n20].getMargin();
                    if (mSolverVariable7 != null && solverVariable2 != null && solverVariable3 != null && mSolverVariable8 != null) {
                        if (constraintWidget7 == mFirstVisibleWidget) {
                            margin7 = mFirstVisibleWidget.mListAnchors[margin].getMargin();
                        }
                        if (constraintWidget7 == mLastVisibleWidget) {
                            margin6 = mLastVisibleWidget.mListAnchors[n20].getMargin();
                        }
                        int n21;
                        if (b4) {
                            n21 = 8;
                        }
                        else {
                            n21 = 5;
                        }
                        linearSystem.addCentering(mSolverVariable7, solverVariable2, margin7, 0.5f, solverVariable3, mSolverVariable8, margin6, n21);
                    }
                }
                if (constraintWidget7.getVisibility() == 8) {
                    constraintWidget7 = constraintWidget6;
                }
                constraintWidget6 = constraintWidget7;
            }
        }
        else if (n5 != 0 && mFirstVisibleWidget != null) {
            final boolean b5 = chainHead.mWidgetsMatchCount > 0 && chainHead.mWidgetsCount == chainHead.mWidgetsMatchCount;
            ConstraintWidget constraintWidget10;
            ConstraintWidget constraintWidget11;
            for (ConstraintWidget constraintWidget9 = constraintWidget10 = mFirstVisibleWidget; constraintWidget10 != null; constraintWidget10 = constraintWidget11) {
                for (constraintWidget11 = constraintWidget10.mNextChainWidget[n]; constraintWidget11 != null && constraintWidget11.getVisibility() == 8; constraintWidget11 = constraintWidget11.mNextChainWidget[n]) {}
                if (constraintWidget10 != mFirstVisibleWidget && constraintWidget10 != mLastVisibleWidget && constraintWidget11 != null) {
                    if (constraintWidget11 == mLastVisibleWidget) {
                        constraintWidget11 = null;
                    }
                    final ConstraintAnchor constraintAnchor7 = constraintWidget10.mListAnchors[margin];
                    final SolverVariable mSolverVariable9 = constraintAnchor7.mSolverVariable;
                    if (constraintAnchor7.mTarget != null) {
                        final SolverVariable mSolverVariable10 = constraintAnchor7.mTarget.mSolverVariable;
                    }
                    final ConstraintAnchor[] mListAnchors6 = constraintWidget9.mListAnchors;
                    final int n22 = margin + 1;
                    final SolverVariable mSolverVariable11 = mListAnchors6[n22].mSolverVariable;
                    final int margin8 = constraintAnchor7.getMargin();
                    final int margin9 = constraintWidget10.mListAnchors[n22].getMargin();
                    ConstraintAnchor constraintAnchor8;
                    SolverVariable mSolverVariable12;
                    SolverVariable mSolverVariable14;
                    if (constraintWidget11 != null) {
                        constraintAnchor8 = constraintWidget11.mListAnchors[margin];
                        mSolverVariable12 = constraintAnchor8.mSolverVariable;
                        SolverVariable mSolverVariable13;
                        if (constraintAnchor8.mTarget != null) {
                            mSolverVariable13 = constraintAnchor8.mTarget.mSolverVariable;
                        }
                        else {
                            mSolverVariable13 = null;
                        }
                        mSolverVariable14 = mSolverVariable13;
                    }
                    else {
                        constraintAnchor8 = mLastVisibleWidget.mListAnchors[margin];
                        SolverVariable mSolverVariable15;
                        if (constraintAnchor8 != null) {
                            mSolverVariable15 = constraintAnchor8.mSolverVariable;
                        }
                        else {
                            mSolverVariable15 = null;
                        }
                        mSolverVariable14 = constraintWidget10.mListAnchors[n22].mSolverVariable;
                        mSolverVariable12 = mSolverVariable15;
                    }
                    int n23 = margin9;
                    if (constraintAnchor8 != null) {
                        n23 = margin9 + constraintAnchor8.getMargin();
                    }
                    final int margin10 = constraintWidget9.mListAnchors[n22].getMargin();
                    int n24;
                    if (b5) {
                        n24 = 8;
                    }
                    else {
                        n24 = 4;
                    }
                    if (mSolverVariable9 != null && mSolverVariable11 != null && mSolverVariable12 != null && mSolverVariable14 != null) {
                        linearSystem.addCentering(mSolverVariable9, mSolverVariable11, margin10 + margin8, 0.5f, mSolverVariable12, mSolverVariable14, n23, n24);
                    }
                }
                if (constraintWidget10.getVisibility() != 8) {
                    constraintWidget9 = constraintWidget10;
                }
            }
            final ConstraintAnchor constraintAnchor9 = mFirstVisibleWidget.mListAnchors[margin];
            final ConstraintAnchor mTarget3 = mFirst.mListAnchors[margin].mTarget;
            final ConstraintAnchor[] mListAnchors7 = mLastVisibleWidget.mListAnchors;
            n = margin + 1;
            final ConstraintAnchor constraintAnchor10 = mListAnchors7[n];
            final ConstraintAnchor mTarget4 = mLast.mListAnchors[n].mTarget;
            if (mTarget3 != null) {
                if (mFirstVisibleWidget != mLastVisibleWidget) {
                    linearSystem.addEquality(constraintAnchor9.mSolverVariable, mTarget3.mSolverVariable, constraintAnchor9.getMargin(), 5);
                }
                else if (mTarget4 != null) {
                    linearSystem.addCentering(constraintAnchor9.mSolverVariable, mTarget3.mSolverVariable, constraintAnchor9.getMargin(), 0.5f, constraintAnchor10.mSolverVariable, mTarget4.mSolverVariable, constraintAnchor10.getMargin(), 5);
                }
            }
            if (mTarget4 != null && mFirstVisibleWidget != mLastVisibleWidget) {
                linearSystem.addEquality(constraintAnchor10.mSolverVariable, mTarget4.mSolverVariable, -constraintAnchor10.getMargin(), 5);
            }
        }
        if ((n7 != 0 || n5 != 0) && mFirstVisibleWidget != null && mFirstVisibleWidget != mLastVisibleWidget) {
            ConstraintAnchor constraintAnchor11 = mFirstVisibleWidget.mListAnchors[margin];
            ConstraintWidget constraintWidget12;
            if ((constraintWidget12 = mLastVisibleWidget) == null) {
                constraintWidget12 = mFirstVisibleWidget;
            }
            final ConstraintAnchor[] mListAnchors8 = constraintWidget12.mListAnchors;
            n = margin + 1;
            ConstraintAnchor constraintAnchor12 = mListAnchors8[n];
            SolverVariable mSolverVariable16;
            if (constraintAnchor11.mTarget != null) {
                mSolverVariable16 = constraintAnchor11.mTarget.mSolverVariable;
            }
            else {
                mSolverVariable16 = null;
            }
            SolverVariable solverVariable4;
            if (constraintAnchor12.mTarget != null) {
                solverVariable4 = constraintAnchor12.mTarget.mSolverVariable;
            }
            else {
                solverVariable4 = null;
            }
            if (mLast != constraintWidget12) {
                final ConstraintAnchor constraintAnchor13 = mLast.mListAnchors[n];
                solverVariable4 = solverVariable;
                if (constraintAnchor13.mTarget != null) {
                    solverVariable4 = constraintAnchor13.mTarget.mSolverVariable;
                }
            }
            if (mFirstVisibleWidget == constraintWidget12) {
                constraintAnchor11 = mFirstVisibleWidget.mListAnchors[margin];
                constraintAnchor12 = mFirstVisibleWidget.mListAnchors[n];
            }
            if (mSolverVariable16 != null && solverVariable4 != null) {
                margin = constraintAnchor11.getMargin();
                n = constraintWidget12.mListAnchors[n].getMargin();
                linearSystem.addCentering(constraintAnchor11.mSolverVariable, mSolverVariable16, margin, 0.5f, solverVariable4, constraintAnchor12.mSolverVariable, n, 5);
            }
        }
    }
    
    public static void applyChainConstraints(final ConstraintWidgetContainer constraintWidgetContainer, final LinearSystem linearSystem, final ArrayList<ConstraintWidget> list, final int n) {
        int i = 0;
        int n2;
        ChainHead[] array;
        int n3;
        if (n == 0) {
            n2 = constraintWidgetContainer.mHorizontalChainsSize;
            array = constraintWidgetContainer.mHorizontalChainsArray;
            n3 = 0;
        }
        else {
            n2 = constraintWidgetContainer.mVerticalChainsSize;
            array = constraintWidgetContainer.mVerticalChainsArray;
            n3 = 2;
        }
        while (i < n2) {
            final ChainHead chainHead = array[i];
            chainHead.define();
            if (list == null || (list != null && list.contains(chainHead.mFirst))) {
                applyChainConstraints(constraintWidgetContainer, linearSystem, n, n3, chainHead);
            }
            ++i;
        }
    }
}

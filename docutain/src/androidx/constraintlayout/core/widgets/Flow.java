// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.core.widgets;

import java.util.HashMap;
import androidx.constraintlayout.core.LinearSystem;
import java.util.Arrays;
import java.util.ArrayList;

public class Flow extends VirtualLayout
{
    public static final int HORIZONTAL_ALIGN_CENTER = 2;
    public static final int HORIZONTAL_ALIGN_END = 1;
    public static final int HORIZONTAL_ALIGN_START = 0;
    public static final int VERTICAL_ALIGN_BASELINE = 3;
    public static final int VERTICAL_ALIGN_BOTTOM = 1;
    public static final int VERTICAL_ALIGN_CENTER = 2;
    public static final int VERTICAL_ALIGN_TOP = 0;
    public static final int WRAP_ALIGNED = 2;
    public static final int WRAP_CHAIN = 1;
    public static final int WRAP_CHAIN_NEW = 3;
    public static final int WRAP_NONE = 0;
    private ConstraintWidget[] mAlignedBiggestElementsInCols;
    private ConstraintWidget[] mAlignedBiggestElementsInRows;
    private int[] mAlignedDimensions;
    private ArrayList<WidgetsList> mChainList;
    private ConstraintWidget[] mDisplayedWidgets;
    private int mDisplayedWidgetsCount;
    private float mFirstHorizontalBias;
    private int mFirstHorizontalStyle;
    private float mFirstVerticalBias;
    private int mFirstVerticalStyle;
    private int mHorizontalAlign;
    private float mHorizontalBias;
    private int mHorizontalGap;
    private int mHorizontalStyle;
    private float mLastHorizontalBias;
    private int mLastHorizontalStyle;
    private float mLastVerticalBias;
    private int mLastVerticalStyle;
    private int mMaxElementsWrap;
    private int mOrientation;
    private int mVerticalAlign;
    private float mVerticalBias;
    private int mVerticalGap;
    private int mVerticalStyle;
    private int mWrapMode;
    
    public Flow() {
        this.mHorizontalStyle = -1;
        this.mVerticalStyle = -1;
        this.mFirstHorizontalStyle = -1;
        this.mFirstVerticalStyle = -1;
        this.mLastHorizontalStyle = -1;
        this.mLastVerticalStyle = -1;
        this.mHorizontalBias = 0.5f;
        this.mVerticalBias = 0.5f;
        this.mFirstHorizontalBias = 0.5f;
        this.mFirstVerticalBias = 0.5f;
        this.mLastHorizontalBias = 0.5f;
        this.mLastVerticalBias = 0.5f;
        this.mHorizontalGap = 0;
        this.mVerticalGap = 0;
        this.mHorizontalAlign = 2;
        this.mVerticalAlign = 2;
        this.mWrapMode = 0;
        this.mMaxElementsWrap = -1;
        this.mOrientation = 0;
        this.mChainList = new ArrayList<WidgetsList>();
        this.mAlignedBiggestElementsInRows = null;
        this.mAlignedBiggestElementsInCols = null;
        this.mAlignedDimensions = null;
        this.mDisplayedWidgetsCount = 0;
    }
    
    private void createAlignedConstraints(final boolean b) {
        if (this.mAlignedDimensions != null && this.mAlignedBiggestElementsInCols != null) {
            if (this.mAlignedBiggestElementsInRows != null) {
                for (int i = 0; i < this.mDisplayedWidgetsCount; ++i) {
                    this.mDisplayedWidgets[i].resetAnchors();
                }
                final int[] mAlignedDimensions = this.mAlignedDimensions;
                final int n = mAlignedDimensions[0];
                final int n2 = mAlignedDimensions[1];
                ConstraintWidget constraintWidget = null;
                float mHorizontalBias = this.mHorizontalBias;
                ConstraintWidget constraintWidget3;
                for (int j = 0; j < n; ++j, constraintWidget = constraintWidget3) {
                    int n3;
                    if (b) {
                        n3 = n - j - 1;
                        mHorizontalBias = 1.0f - this.mHorizontalBias;
                    }
                    else {
                        n3 = j;
                    }
                    final ConstraintWidget constraintWidget2 = this.mAlignedBiggestElementsInCols[n3];
                    constraintWidget3 = constraintWidget;
                    if (constraintWidget2 != null) {
                        if (constraintWidget2.getVisibility() == 8) {
                            constraintWidget3 = constraintWidget;
                        }
                        else {
                            if (j == 0) {
                                constraintWidget2.connect(constraintWidget2.mLeft, this.mLeft, this.getPaddingLeft());
                                constraintWidget2.setHorizontalChainStyle(this.mHorizontalStyle);
                                constraintWidget2.setHorizontalBiasPercent(mHorizontalBias);
                            }
                            if (j == n - 1) {
                                constraintWidget2.connect(constraintWidget2.mRight, this.mRight, this.getPaddingRight());
                            }
                            if (j > 0 && constraintWidget != null) {
                                constraintWidget2.connect(constraintWidget2.mLeft, constraintWidget.mRight, this.mHorizontalGap);
                                constraintWidget.connect(constraintWidget.mRight, constraintWidget2.mLeft, 0);
                            }
                            constraintWidget3 = constraintWidget2;
                        }
                    }
                }
                ConstraintWidget constraintWidget5;
                for (int k = 0; k < n2; ++k, constraintWidget = constraintWidget5) {
                    final ConstraintWidget constraintWidget4 = this.mAlignedBiggestElementsInRows[k];
                    constraintWidget5 = constraintWidget;
                    if (constraintWidget4 != null) {
                        if (constraintWidget4.getVisibility() == 8) {
                            constraintWidget5 = constraintWidget;
                        }
                        else {
                            if (k == 0) {
                                constraintWidget4.connect(constraintWidget4.mTop, this.mTop, this.getPaddingTop());
                                constraintWidget4.setVerticalChainStyle(this.mVerticalStyle);
                                constraintWidget4.setVerticalBiasPercent(this.mVerticalBias);
                            }
                            if (k == n2 - 1) {
                                constraintWidget4.connect(constraintWidget4.mBottom, this.mBottom, this.getPaddingBottom());
                            }
                            if (k > 0 && constraintWidget != null) {
                                constraintWidget4.connect(constraintWidget4.mTop, constraintWidget.mBottom, this.mVerticalGap);
                                constraintWidget.connect(constraintWidget.mBottom, constraintWidget4.mTop, 0);
                            }
                            constraintWidget5 = constraintWidget4;
                        }
                    }
                }
                for (int l = 0; l < n; ++l) {
                    for (int n4 = 0; n4 < n2; ++n4) {
                        int n5 = n4 * n + l;
                        if (this.mOrientation == 1) {
                            n5 = l * n2 + n4;
                        }
                        final ConstraintWidget[] mDisplayedWidgets = this.mDisplayedWidgets;
                        if (n5 < mDisplayedWidgets.length) {
                            final ConstraintWidget constraintWidget6 = mDisplayedWidgets[n5];
                            if (constraintWidget6 != null) {
                                if (constraintWidget6.getVisibility() != 8) {
                                    final ConstraintWidget constraintWidget7 = this.mAlignedBiggestElementsInCols[l];
                                    final ConstraintWidget constraintWidget8 = this.mAlignedBiggestElementsInRows[n4];
                                    if (constraintWidget6 != constraintWidget7) {
                                        constraintWidget6.connect(constraintWidget6.mLeft, constraintWidget7.mLeft, 0);
                                        constraintWidget6.connect(constraintWidget6.mRight, constraintWidget7.mRight, 0);
                                    }
                                    if (constraintWidget6 != constraintWidget8) {
                                        constraintWidget6.connect(constraintWidget6.mTop, constraintWidget8.mTop, 0);
                                        constraintWidget6.connect(constraintWidget6.mBottom, constraintWidget8.mBottom, 0);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    private final int getWidgetHeight(final ConstraintWidget constraintWidget, int n) {
        if (constraintWidget == null) {
            return 0;
        }
        if (constraintWidget.getVerticalDimensionBehaviour() == DimensionBehaviour.MATCH_CONSTRAINT) {
            if (constraintWidget.mMatchConstraintDefaultHeight == 0) {
                return 0;
            }
            if (constraintWidget.mMatchConstraintDefaultHeight == 2) {
                n *= (int)constraintWidget.mMatchConstraintPercentHeight;
                if (n != constraintWidget.getHeight()) {
                    constraintWidget.setMeasureRequested(true);
                    this.measure(constraintWidget, constraintWidget.getHorizontalDimensionBehaviour(), constraintWidget.getWidth(), DimensionBehaviour.FIXED, n);
                }
                return n;
            }
            if (constraintWidget.mMatchConstraintDefaultHeight == 1) {
                return constraintWidget.getHeight();
            }
            if (constraintWidget.mMatchConstraintDefaultHeight == 3) {
                return (int)(constraintWidget.getWidth() * constraintWidget.mDimensionRatio + 0.5f);
            }
        }
        return constraintWidget.getHeight();
    }
    
    private final int getWidgetWidth(final ConstraintWidget constraintWidget, int n) {
        if (constraintWidget == null) {
            return 0;
        }
        if (constraintWidget.getHorizontalDimensionBehaviour() == DimensionBehaviour.MATCH_CONSTRAINT) {
            if (constraintWidget.mMatchConstraintDefaultWidth == 0) {
                return 0;
            }
            if (constraintWidget.mMatchConstraintDefaultWidth == 2) {
                n *= (int)constraintWidget.mMatchConstraintPercentWidth;
                if (n != constraintWidget.getWidth()) {
                    constraintWidget.setMeasureRequested(true);
                    this.measure(constraintWidget, DimensionBehaviour.FIXED, n, constraintWidget.getVerticalDimensionBehaviour(), constraintWidget.getHeight());
                }
                return n;
            }
            if (constraintWidget.mMatchConstraintDefaultWidth == 1) {
                return constraintWidget.getWidth();
            }
            if (constraintWidget.mMatchConstraintDefaultWidth == 3) {
                return (int)(constraintWidget.getHeight() * constraintWidget.mDimensionRatio + 0.5f);
            }
        }
        return constraintWidget.getWidth();
    }
    
    private void measureAligned(final ConstraintWidget[] array, final int n, final int n2, final int n3, final int[] array2) {
        int n8;
        int n9;
        if (n2 == 0) {
            int mMaxElementsWrap;
            if ((mMaxElementsWrap = this.mMaxElementsWrap) <= 0) {
                int n4 = 0;
                int n5 = 0;
                int n6 = 0;
                while (true) {
                    mMaxElementsWrap = n4;
                    if (n5 >= n) {
                        break;
                    }
                    int n7 = n6;
                    if (n5 > 0) {
                        n7 = n6 + this.mHorizontalGap;
                    }
                    final ConstraintWidget constraintWidget = array[n5];
                    if (constraintWidget == null) {
                        n6 = n7;
                    }
                    else {
                        n6 = n7 + this.getWidgetWidth(constraintWidget, n3);
                        if (n6 > n3) {
                            mMaxElementsWrap = n4;
                            break;
                        }
                        ++n4;
                    }
                    ++n5;
                }
            }
            n8 = mMaxElementsWrap;
            n9 = 0;
        }
        else {
            int mMaxElementsWrap2;
            if ((mMaxElementsWrap2 = this.mMaxElementsWrap) <= 0) {
                int n10 = 0;
                int n11 = 0;
                int n12 = 0;
                while (true) {
                    mMaxElementsWrap2 = n10;
                    if (n11 >= n) {
                        break;
                    }
                    int n13 = n12;
                    if (n11 > 0) {
                        n13 = n12 + this.mVerticalGap;
                    }
                    final ConstraintWidget constraintWidget2 = array[n11];
                    if (constraintWidget2 == null) {
                        n12 = n13;
                    }
                    else {
                        n12 = n13 + this.getWidgetHeight(constraintWidget2, n3);
                        if (n12 > n3) {
                            mMaxElementsWrap2 = n10;
                            break;
                        }
                        ++n10;
                    }
                    ++n11;
                }
            }
            n8 = 0;
            n9 = mMaxElementsWrap2;
        }
        if (this.mAlignedDimensions == null) {
            this.mAlignedDimensions = new int[2];
        }
        while (true) {
            int n14 = 0;
            int n15 = 0;
            Label_0282: {
                Label_0265: {
                    if (n9 != 0) {
                        break Label_0265;
                    }
                    n14 = n9;
                    n15 = n8;
                    if (n2 != 1) {
                        break Label_0265;
                    }
                    break Label_0282;
                    final boolean b;
                    while (!b) {
                        if (n2 == 0) {
                            n9 = (int)Math.ceil(n / (float)n8);
                        }
                        else {
                            n8 = (int)Math.ceil(n / (float)n9);
                        }
                        final ConstraintWidget[] mAlignedBiggestElementsInCols = this.mAlignedBiggestElementsInCols;
                        if (mAlignedBiggestElementsInCols != null && mAlignedBiggestElementsInCols.length >= n8) {
                            Arrays.fill(mAlignedBiggestElementsInCols, null);
                        }
                        else {
                            this.mAlignedBiggestElementsInCols = new ConstraintWidget[n8];
                        }
                        final ConstraintWidget[] mAlignedBiggestElementsInRows = this.mAlignedBiggestElementsInRows;
                        if (mAlignedBiggestElementsInRows != null && mAlignedBiggestElementsInRows.length >= n9) {
                            Arrays.fill(mAlignedBiggestElementsInRows, null);
                        }
                        else {
                            this.mAlignedBiggestElementsInRows = new ConstraintWidget[n9];
                        }
                        for (int i = 0; i < n8; ++i) {
                            for (int j = 0; j < n9; ++j) {
                                int n16 = j * n8 + i;
                                if (n2 == 1) {
                                    n16 = i * n9 + j;
                                }
                                if (n16 < array.length) {
                                    final ConstraintWidget constraintWidget3 = array[n16];
                                    if (constraintWidget3 != null) {
                                        final int widgetWidth = this.getWidgetWidth(constraintWidget3, n3);
                                        final ConstraintWidget constraintWidget4 = this.mAlignedBiggestElementsInCols[i];
                                        if (constraintWidget4 == null || constraintWidget4.getWidth() < widgetWidth) {
                                            this.mAlignedBiggestElementsInCols[i] = constraintWidget3;
                                        }
                                        final int widgetHeight = this.getWidgetHeight(constraintWidget3, n3);
                                        final ConstraintWidget constraintWidget5 = this.mAlignedBiggestElementsInRows[j];
                                        if (constraintWidget5 == null || constraintWidget5.getHeight() < widgetHeight) {
                                            this.mAlignedBiggestElementsInRows[j] = constraintWidget3;
                                        }
                                    }
                                }
                            }
                        }
                        int k = 0;
                        int n17 = 0;
                        while (k < n8) {
                            final ConstraintWidget constraintWidget6 = this.mAlignedBiggestElementsInCols[k];
                            int n18 = n17;
                            if (constraintWidget6 != null) {
                                int n19 = n17;
                                if (k > 0) {
                                    n19 = n17 + this.mHorizontalGap;
                                }
                                n18 = n19 + this.getWidgetWidth(constraintWidget6, n3);
                            }
                            ++k;
                            n17 = n18;
                        }
                        int l = 0;
                        int n20 = 0;
                        while (l < n9) {
                            final ConstraintWidget constraintWidget7 = this.mAlignedBiggestElementsInRows[l];
                            int n21 = n20;
                            if (constraintWidget7 != null) {
                                int n22 = n20;
                                if (l > 0) {
                                    n22 = n20 + this.mVerticalGap;
                                }
                                n21 = n22 + this.getWidgetHeight(constraintWidget7, n3);
                            }
                            ++l;
                            n20 = n21;
                        }
                        array2[0] = n17;
                        array2[1] = n20;
                        if (n2 == 0) {
                            n14 = n9;
                            n15 = n8;
                            if (n17 <= n3) {
                                break Label_0282;
                            }
                            n14 = n9;
                            if ((n15 = n8) <= 1) {
                                break Label_0282;
                            }
                            --n8;
                        }
                        else {
                            n14 = n9;
                            n15 = n8;
                            if (n20 <= n3) {
                                break Label_0282;
                            }
                            n14 = n9;
                            n15 = n8;
                            if (n9 <= 1) {
                                break Label_0282;
                            }
                            --n9;
                        }
                    }
                    final int[] mAlignedDimensions = this.mAlignedDimensions;
                    mAlignedDimensions[0] = n8;
                    mAlignedDimensions[1] = n9;
                    return;
                }
                if (n8 != 0 || n2 != 0) {
                    final boolean b = false;
                    continue;
                }
                n15 = n8;
                n14 = n9;
            }
            final boolean b = true;
            n9 = n14;
            n8 = n15;
            continue;
        }
    }
    
    private void measureChainWrap(final ConstraintWidget[] array, int i, final int n, final int n2, final int[] array2) {
        if (i == 0) {
            return;
        }
        this.mChainList.clear();
        WidgetsList e = new WidgetsList(n, this.mLeft, this.mTop, this.mRight, this.mBottom, n2);
        this.mChainList.add(e);
        int n5;
        if (n == 0) {
            int n3 = 0;
            int n4 = 0;
            int startIndex = 0;
            while (true) {
                n5 = n3;
                if (startIndex >= i) {
                    break;
                }
                final ConstraintWidget constraintWidget = array[startIndex];
                final int widgetWidth = this.getWidgetWidth(constraintWidget, n2);
                int n6 = n3;
                if (constraintWidget.getHorizontalDimensionBehaviour() == DimensionBehaviour.MATCH_CONSTRAINT) {
                    n6 = n3 + 1;
                }
                boolean b2;
                final boolean b = b2 = ((n4 == n2 || this.mHorizontalGap + n4 + widgetWidth > n2) && e.biggest != null);
                if (!b) {
                    b2 = b;
                    if (startIndex > 0) {
                        final int mMaxElementsWrap = this.mMaxElementsWrap;
                        b2 = b;
                        if (mMaxElementsWrap > 0) {
                            b2 = b;
                            if (startIndex % mMaxElementsWrap == 0) {
                                b2 = true;
                            }
                        }
                    }
                }
                Label_0281: {
                    WidgetsList e2;
                    if (b2) {
                        e2 = new WidgetsList(n, this.mLeft, this.mTop, this.mRight, this.mBottom, n2);
                        e2.setStartIndex(startIndex);
                        this.mChainList.add(e2);
                    }
                    else {
                        e2 = e;
                        if (startIndex > 0) {
                            n4 += this.mHorizontalGap + widgetWidth;
                            break Label_0281;
                        }
                    }
                    n4 = widgetWidth;
                    e = e2;
                }
                e.add(constraintWidget);
                ++startIndex;
                n3 = n6;
            }
        }
        else {
            int n7 = 0;
            int n8 = 0;
            int startIndex2 = 0;
            WidgetsList list = e;
            while (true) {
                n5 = n7;
                if (startIndex2 >= i) {
                    break;
                }
                final ConstraintWidget constraintWidget2 = array[startIndex2];
                final int widgetHeight = this.getWidgetHeight(constraintWidget2, n2);
                int n9 = n7;
                if (constraintWidget2.getVerticalDimensionBehaviour() == DimensionBehaviour.MATCH_CONSTRAINT) {
                    n9 = n7 + 1;
                }
                boolean b4;
                final boolean b3 = b4 = ((n8 == n2 || this.mVerticalGap + n8 + widgetHeight > n2) && list.biggest != null);
                if (!b3) {
                    b4 = b3;
                    if (startIndex2 > 0) {
                        final int mMaxElementsWrap2 = this.mMaxElementsWrap;
                        b4 = b3;
                        if (mMaxElementsWrap2 > 0) {
                            b4 = b3;
                            if (startIndex2 % mMaxElementsWrap2 == 0) {
                                b4 = true;
                            }
                        }
                    }
                }
                WidgetsList e3 = null;
                Label_0528: {
                    if (b4) {
                        e3 = new WidgetsList(n, this.mLeft, this.mTop, this.mRight, this.mBottom, n2);
                        e3.setStartIndex(startIndex2);
                        this.mChainList.add(e3);
                    }
                    else {
                        e3 = list;
                        if (startIndex2 > 0) {
                            n8 += this.mVerticalGap + widgetHeight;
                            e3 = list;
                            break Label_0528;
                        }
                    }
                    n8 = widgetHeight;
                }
                e3.add(constraintWidget2);
                ++startIndex2;
                n7 = n9;
                list = e3;
            }
        }
        final int size = this.mChainList.size();
        ConstraintAnchor mLeft = this.mLeft;
        ConstraintAnchor mTop = this.mTop;
        ConstraintAnchor constraintAnchor = this.mRight;
        ConstraintAnchor constraintAnchor2 = this.mBottom;
        int paddingLeft = this.getPaddingLeft();
        int paddingTop = this.getPaddingTop();
        int paddingRight = this.getPaddingRight();
        int paddingBottom = this.getPaddingBottom();
        if (this.getHorizontalDimensionBehaviour() != DimensionBehaviour.WRAP_CONTENT && this.getVerticalDimensionBehaviour() != DimensionBehaviour.WRAP_CONTENT) {
            i = 0;
        }
        else {
            i = 1;
        }
        if (n5 > 0 && i != 0) {
            WidgetsList list2;
            for (i = 0; i < size; ++i) {
                list2 = this.mChainList.get(i);
                if (n == 0) {
                    list2.measureMatchConstraints(n2 - list2.getWidth());
                }
                else {
                    list2.measureMatchConstraints(n2 - list2.getHeight());
                }
            }
        }
        int a = 0;
        int a2 = 0;
        for (int j = 0; j < size; ++j, a2 = i) {
            final WidgetsList list3 = this.mChainList.get(j);
            if (n == 0) {
                if (j < size - 1) {
                    constraintAnchor2 = this.mChainList.get(j + 1).biggest.mTop;
                    i = 0;
                }
                else {
                    constraintAnchor2 = this.mBottom;
                    i = this.getPaddingBottom();
                }
                final ConstraintAnchor mBottom = list3.biggest.mBottom;
                list3.setup(n, mLeft, mTop, constraintAnchor, constraintAnchor2, paddingLeft, paddingTop, paddingRight, i, n2);
                final int max = Math.max(a2, list3.getWidth());
                final int n10 = a += list3.getHeight();
                if (j > 0) {
                    a = n10 + this.mVerticalGap;
                }
                mTop = mBottom;
                final int n11 = 0;
                paddingBottom = i;
                i = max;
                paddingTop = n11;
            }
            else {
                if (j < size - 1) {
                    constraintAnchor = this.mChainList.get(j + 1).biggest.mLeft;
                    i = 0;
                }
                else {
                    constraintAnchor = this.mRight;
                    i = this.getPaddingRight();
                }
                final ConstraintAnchor mRight = list3.biggest.mRight;
                list3.setup(n, mLeft, mTop, constraintAnchor, constraintAnchor2, paddingLeft, paddingTop, i, paddingBottom, n2);
                final int n12 = a2 + list3.getWidth();
                final int max2 = Math.max(a, list3.getHeight());
                int n13 = n12;
                if (j > 0) {
                    n13 = n12 + this.mHorizontalGap;
                }
                paddingRight = i;
                mLeft = mRight;
                final int n14 = 0;
                i = n13;
                a = max2;
                paddingLeft = n14;
            }
        }
        array2[0] = a2;
        array2[1] = a;
    }
    
    private void measureChainWrap_new(final ConstraintWidget[] array, int i, final int n, final int n2, final int[] array2) {
        if (i == 0) {
            return;
        }
        this.mChainList.clear();
        WidgetsList list = new WidgetsList(n, this.mLeft, this.mTop, this.mRight, this.mBottom, n2);
        this.mChainList.add(list);
        int n6;
        if (n == 0) {
            int n3 = 0;
            int n4 = 0;
            int n5 = 0;
            int startIndex = 0;
            while (true) {
                n6 = n4;
                if (startIndex >= i) {
                    break;
                }
                final int n7 = n3 + 1;
                final ConstraintWidget constraintWidget = array[startIndex];
                final int widgetWidth = this.getWidgetWidth(constraintWidget, n2);
                int n8 = n4;
                if (constraintWidget.getHorizontalDimensionBehaviour() == DimensionBehaviour.MATCH_CONSTRAINT) {
                    n8 = n4 + 1;
                }
                boolean b2;
                final boolean b = b2 = ((n5 == n2 || this.mHorizontalGap + n5 + widgetWidth > n2) && list.biggest != null);
                if (!b) {
                    b2 = b;
                    if (startIndex > 0) {
                        final int mMaxElementsWrap = this.mMaxElementsWrap;
                        b2 = b;
                        if (mMaxElementsWrap > 0) {
                            b2 = b;
                            if (n7 > mMaxElementsWrap) {
                                b2 = true;
                            }
                        }
                    }
                }
                int n9;
                if (b2) {
                    list = new WidgetsList(n, this.mLeft, this.mTop, this.mRight, this.mBottom, n2);
                    list.setStartIndex(startIndex);
                    this.mChainList.add(list);
                    n9 = n7;
                    n5 = widgetWidth;
                }
                else {
                    if (startIndex > 0) {
                        n5 += this.mHorizontalGap + widgetWidth;
                    }
                    else {
                        n5 = widgetWidth;
                    }
                    n9 = 0;
                }
                list.add(constraintWidget);
                ++startIndex;
                n3 = n9;
                n4 = n8;
            }
        }
        else {
            int n10 = 0;
            int n11 = 0;
            int startIndex2 = 0;
            WidgetsList list2 = list;
            while (true) {
                n6 = n11;
                if (startIndex2 >= i) {
                    break;
                }
                final ConstraintWidget constraintWidget2 = array[startIndex2];
                final int widgetHeight = this.getWidgetHeight(constraintWidget2, n2);
                int n12 = n11;
                if (constraintWidget2.getVerticalDimensionBehaviour() == DimensionBehaviour.MATCH_CONSTRAINT) {
                    n12 = n11 + 1;
                }
                boolean b4;
                final boolean b3 = b4 = ((n10 == n2 || this.mVerticalGap + n10 + widgetHeight > n2) && list2.biggest != null);
                if (!b3) {
                    b4 = b3;
                    if (startIndex2 > 0) {
                        final int mMaxElementsWrap2 = this.mMaxElementsWrap;
                        b4 = b3;
                        if (mMaxElementsWrap2 > 0) {
                            b4 = b3;
                            if (mMaxElementsWrap2 < 0) {
                                b4 = true;
                            }
                        }
                    }
                }
                WidgetsList e = null;
                int n13 = 0;
                Label_0543: {
                    if (b4) {
                        e = new WidgetsList(n, this.mLeft, this.mTop, this.mRight, this.mBottom, n2);
                        e.setStartIndex(startIndex2);
                        this.mChainList.add(e);
                    }
                    else {
                        e = list2;
                        if (startIndex2 > 0) {
                            n13 = n10 + (this.mVerticalGap + widgetHeight);
                            e = list2;
                            break Label_0543;
                        }
                    }
                    n13 = widgetHeight;
                }
                e.add(constraintWidget2);
                ++startIndex2;
                n10 = n13;
                n11 = n12;
                list2 = e;
            }
        }
        final int size = this.mChainList.size();
        ConstraintAnchor mLeft = this.mLeft;
        ConstraintAnchor mTop = this.mTop;
        ConstraintAnchor constraintAnchor = this.mRight;
        ConstraintAnchor constraintAnchor2 = this.mBottom;
        int paddingLeft = this.getPaddingLeft();
        int paddingTop = this.getPaddingTop();
        int paddingRight = this.getPaddingRight();
        int paddingBottom = this.getPaddingBottom();
        if (this.getHorizontalDimensionBehaviour() != DimensionBehaviour.WRAP_CONTENT && this.getVerticalDimensionBehaviour() != DimensionBehaviour.WRAP_CONTENT) {
            i = 0;
        }
        else {
            i = 1;
        }
        if (n6 > 0 && i != 0) {
            WidgetsList list3;
            for (i = 0; i < size; ++i) {
                list3 = this.mChainList.get(i);
                if (n == 0) {
                    list3.measureMatchConstraints(n2 - list3.getWidth());
                }
                else {
                    list3.measureMatchConstraints(n2 - list3.getHeight());
                }
            }
        }
        int a = 0;
        int a2 = 0;
        for (int j = 0; j < size; ++j, a2 = i) {
            final WidgetsList list4 = this.mChainList.get(j);
            if (n == 0) {
                if (j < size - 1) {
                    constraintAnchor2 = this.mChainList.get(j + 1).biggest.mTop;
                    i = 0;
                }
                else {
                    constraintAnchor2 = this.mBottom;
                    i = this.getPaddingBottom();
                }
                final ConstraintAnchor mBottom = list4.biggest.mBottom;
                list4.setup(n, mLeft, mTop, constraintAnchor, constraintAnchor2, paddingLeft, paddingTop, paddingRight, i, n2);
                final int max = Math.max(a2, list4.getWidth());
                final int n14 = a += list4.getHeight();
                if (j > 0) {
                    a = n14 + this.mVerticalGap;
                }
                mTop = mBottom;
                final int n15 = 0;
                paddingBottom = i;
                i = max;
                paddingTop = n15;
            }
            else {
                if (j < size - 1) {
                    constraintAnchor = this.mChainList.get(j + 1).biggest.mLeft;
                    i = 0;
                }
                else {
                    constraintAnchor = this.mRight;
                    i = this.getPaddingRight();
                }
                final ConstraintAnchor mRight = list4.biggest.mRight;
                list4.setup(n, mLeft, mTop, constraintAnchor, constraintAnchor2, paddingLeft, paddingTop, i, paddingBottom, n2);
                final int n16 = a2 + list4.getWidth();
                final int max2 = Math.max(a, list4.getHeight());
                int n17 = n16;
                if (j > 0) {
                    n17 = n16 + this.mHorizontalGap;
                }
                final int n18 = max2;
                paddingRight = i;
                mLeft = mRight;
                final int n19 = 0;
                i = n17;
                a = n18;
                paddingLeft = n19;
            }
        }
        array2[0] = a2;
        array2[1] = a;
    }
    
    private void measureNoWrap(final ConstraintWidget[] array, final int n, int i, final int n2, final int[] array2) {
        if (n == 0) {
            return;
        }
        WidgetsList e;
        if (this.mChainList.size() == 0) {
            e = new WidgetsList(i, this.mLeft, this.mTop, this.mRight, this.mBottom, n2);
            this.mChainList.add(e);
        }
        else {
            e = this.mChainList.get(0);
            e.clear();
            e.setup(i, this.mLeft, this.mTop, this.mRight, this.mBottom, this.getPaddingLeft(), this.getPaddingTop(), this.getPaddingRight(), this.getPaddingBottom(), n2);
        }
        for (i = 0; i < n; ++i) {
            e.add(array[i]);
        }
        array2[0] = e.getWidth();
        array2[1] = e.getHeight();
    }
    
    @Override
    public void addToSolver(final LinearSystem linearSystem, final boolean b) {
        super.addToSolver(linearSystem, b);
        final boolean b2 = this.getParent() != null && ((ConstraintWidgetContainer)this.getParent()).isRtl();
        final int mWrapMode = this.mWrapMode;
        if (mWrapMode != 0) {
            if (mWrapMode != 1) {
                if (mWrapMode != 2) {
                    if (mWrapMode == 3) {
                        for (int size = this.mChainList.size(), i = 0; i < size; ++i) {
                            this.mChainList.get(i).createConstraints(b2, i, i == size - 1);
                        }
                    }
                }
                else {
                    this.createAlignedConstraints(b2);
                }
            }
            else {
                for (int size2 = this.mChainList.size(), j = 0; j < size2; ++j) {
                    this.mChainList.get(j).createConstraints(b2, j, j == size2 - 1);
                }
            }
        }
        else if (this.mChainList.size() > 0) {
            this.mChainList.get(0).createConstraints(b2, 0, true);
        }
        this.needsCallbackFromSolver(false);
    }
    
    @Override
    public void copy(final ConstraintWidget constraintWidget, final HashMap<ConstraintWidget, ConstraintWidget> hashMap) {
        super.copy(constraintWidget, hashMap);
        final Flow flow = (Flow)constraintWidget;
        this.mHorizontalStyle = flow.mHorizontalStyle;
        this.mVerticalStyle = flow.mVerticalStyle;
        this.mFirstHorizontalStyle = flow.mFirstHorizontalStyle;
        this.mFirstVerticalStyle = flow.mFirstVerticalStyle;
        this.mLastHorizontalStyle = flow.mLastHorizontalStyle;
        this.mLastVerticalStyle = flow.mLastVerticalStyle;
        this.mHorizontalBias = flow.mHorizontalBias;
        this.mVerticalBias = flow.mVerticalBias;
        this.mFirstHorizontalBias = flow.mFirstHorizontalBias;
        this.mFirstVerticalBias = flow.mFirstVerticalBias;
        this.mLastHorizontalBias = flow.mLastHorizontalBias;
        this.mLastVerticalBias = flow.mLastVerticalBias;
        this.mHorizontalGap = flow.mHorizontalGap;
        this.mVerticalGap = flow.mVerticalGap;
        this.mHorizontalAlign = flow.mHorizontalAlign;
        this.mVerticalAlign = flow.mVerticalAlign;
        this.mWrapMode = flow.mWrapMode;
        this.mMaxElementsWrap = flow.mMaxElementsWrap;
        this.mOrientation = flow.mOrientation;
    }
    
    public float getMaxElementsWrap() {
        return (float)this.mMaxElementsWrap;
    }
    
    @Override
    public void measure(int min, int min2, final int n, final int b) {
        if (this.mWidgetsCount > 0 && !this.measureChildren()) {
            this.setMeasure(0, 0);
            this.needsCallbackFromSolver(false);
            return;
        }
        final int paddingLeft = this.getPaddingLeft();
        final int paddingRight = this.getPaddingRight();
        final int paddingTop = this.getPaddingTop();
        final int paddingBottom = this.getPaddingBottom();
        final int[] array = new int[2];
        int n2 = min2 - paddingLeft - paddingRight;
        final int mOrientation = this.mOrientation;
        if (mOrientation == 1) {
            n2 = b - paddingTop - paddingBottom;
        }
        if (mOrientation == 0) {
            if (this.mHorizontalStyle == -1) {
                this.mHorizontalStyle = 0;
            }
            if (this.mVerticalStyle == -1) {
                this.mVerticalStyle = 0;
            }
        }
        else {
            if (this.mHorizontalStyle == -1) {
                this.mHorizontalStyle = 0;
            }
            if (this.mVerticalStyle == -1) {
                this.mVerticalStyle = 0;
            }
        }
        ConstraintWidget[] mWidgets = this.mWidgets;
        int i = 0;
        int n3 = 0;
        while (i < this.mWidgetsCount) {
            int n4 = n3;
            if (this.mWidgets[i].getVisibility() == 8) {
                n4 = n3 + 1;
            }
            ++i;
            n3 = n4;
        }
        int mWidgetsCount = this.mWidgetsCount;
        if (n3 > 0) {
            mWidgets = new ConstraintWidget[this.mWidgetsCount - n3];
            int j = 0;
            int n5 = 0;
            while (j < this.mWidgetsCount) {
                final ConstraintWidget constraintWidget = this.mWidgets[j];
                int n6 = n5;
                if (constraintWidget.getVisibility() != 8) {
                    mWidgets[n5] = constraintWidget;
                    n6 = n5 + 1;
                }
                ++j;
                n5 = n6;
            }
            mWidgetsCount = n5;
        }
        this.mDisplayedWidgets = mWidgets;
        this.mDisplayedWidgetsCount = mWidgetsCount;
        final int mWrapMode = this.mWrapMode;
        if (mWrapMode != 0) {
            if (mWrapMode != 1) {
                if (mWrapMode != 2) {
                    if (mWrapMode == 3) {
                        this.measureChainWrap_new(mWidgets, mWidgetsCount, this.mOrientation, n2, array);
                    }
                }
                else {
                    this.measureAligned(mWidgets, mWidgetsCount, this.mOrientation, n2, array);
                }
            }
            else {
                this.measureChainWrap(mWidgets, mWidgetsCount, this.mOrientation, n2, array);
            }
        }
        else {
            this.measureNoWrap(mWidgets, mWidgetsCount, this.mOrientation, n2, array);
        }
        boolean b2 = true;
        final int a = array[0] + paddingLeft + paddingRight;
        final int a2 = array[1] + paddingTop + paddingBottom;
        if (min == 1073741824) {
            min = min2;
        }
        else if (min == Integer.MIN_VALUE) {
            min = Math.min(a, min2);
        }
        else if (min == 0) {
            min = a;
        }
        else {
            min = 0;
        }
        if (n == 1073741824) {
            min2 = b;
        }
        else if (n == Integer.MIN_VALUE) {
            min2 = Math.min(a2, b);
        }
        else if (n == 0) {
            min2 = a2;
        }
        else {
            min2 = 0;
        }
        this.setMeasure(min, min2);
        this.setWidth(min);
        this.setHeight(min2);
        if (this.mWidgetsCount <= 0) {
            b2 = false;
        }
        this.needsCallbackFromSolver(b2);
    }
    
    public void setFirstHorizontalBias(final float mFirstHorizontalBias) {
        this.mFirstHorizontalBias = mFirstHorizontalBias;
    }
    
    public void setFirstHorizontalStyle(final int mFirstHorizontalStyle) {
        this.mFirstHorizontalStyle = mFirstHorizontalStyle;
    }
    
    public void setFirstVerticalBias(final float mFirstVerticalBias) {
        this.mFirstVerticalBias = mFirstVerticalBias;
    }
    
    public void setFirstVerticalStyle(final int mFirstVerticalStyle) {
        this.mFirstVerticalStyle = mFirstVerticalStyle;
    }
    
    public void setHorizontalAlign(final int mHorizontalAlign) {
        this.mHorizontalAlign = mHorizontalAlign;
    }
    
    public void setHorizontalBias(final float mHorizontalBias) {
        this.mHorizontalBias = mHorizontalBias;
    }
    
    public void setHorizontalGap(final int mHorizontalGap) {
        this.mHorizontalGap = mHorizontalGap;
    }
    
    public void setHorizontalStyle(final int mHorizontalStyle) {
        this.mHorizontalStyle = mHorizontalStyle;
    }
    
    public void setLastHorizontalBias(final float mLastHorizontalBias) {
        this.mLastHorizontalBias = mLastHorizontalBias;
    }
    
    public void setLastHorizontalStyle(final int mLastHorizontalStyle) {
        this.mLastHorizontalStyle = mLastHorizontalStyle;
    }
    
    public void setLastVerticalBias(final float mLastVerticalBias) {
        this.mLastVerticalBias = mLastVerticalBias;
    }
    
    public void setLastVerticalStyle(final int mLastVerticalStyle) {
        this.mLastVerticalStyle = mLastVerticalStyle;
    }
    
    public void setMaxElementsWrap(final int mMaxElementsWrap) {
        this.mMaxElementsWrap = mMaxElementsWrap;
    }
    
    public void setOrientation(final int mOrientation) {
        this.mOrientation = mOrientation;
    }
    
    public void setVerticalAlign(final int mVerticalAlign) {
        this.mVerticalAlign = mVerticalAlign;
    }
    
    public void setVerticalBias(final float mVerticalBias) {
        this.mVerticalBias = mVerticalBias;
    }
    
    public void setVerticalGap(final int mVerticalGap) {
        this.mVerticalGap = mVerticalGap;
    }
    
    public void setVerticalStyle(final int mVerticalStyle) {
        this.mVerticalStyle = mVerticalStyle;
    }
    
    public void setWrapMode(final int mWrapMode) {
        this.mWrapMode = mWrapMode;
    }
    
    private class WidgetsList
    {
        private ConstraintWidget biggest;
        int biggestDimension;
        private ConstraintAnchor mBottom;
        private int mCount;
        private int mHeight;
        private ConstraintAnchor mLeft;
        private int mMax;
        private int mNbMatchConstraintsWidgets;
        private int mOrientation;
        private int mPaddingBottom;
        private int mPaddingLeft;
        private int mPaddingRight;
        private int mPaddingTop;
        private ConstraintAnchor mRight;
        private int mStartIndex;
        private ConstraintAnchor mTop;
        private int mWidth;
        final Flow this$0;
        
        public WidgetsList(final Flow this$0, final int mOrientation, final ConstraintAnchor mLeft, final ConstraintAnchor mTop, final ConstraintAnchor mRight, final ConstraintAnchor mBottom, final int mMax) {
            this.this$0 = this$0;
            this.biggest = null;
            this.biggestDimension = 0;
            this.mPaddingLeft = 0;
            this.mPaddingTop = 0;
            this.mPaddingRight = 0;
            this.mPaddingBottom = 0;
            this.mWidth = 0;
            this.mHeight = 0;
            this.mStartIndex = 0;
            this.mCount = 0;
            this.mNbMatchConstraintsWidgets = 0;
            this.mMax = 0;
            this.mOrientation = mOrientation;
            this.mLeft = mLeft;
            this.mTop = mTop;
            this.mRight = mRight;
            this.mBottom = mBottom;
            this.mPaddingLeft = this$0.getPaddingLeft();
            this.mPaddingTop = this$0.getPaddingTop();
            this.mPaddingRight = this$0.getPaddingRight();
            this.mPaddingBottom = this$0.getPaddingBottom();
            this.mMax = mMax;
        }
        
        private void recomputeDimensions() {
            this.mWidth = 0;
            this.mHeight = 0;
            this.biggest = null;
            this.biggestDimension = 0;
            for (int mCount = this.mCount, n = 0; n < mCount && this.mStartIndex + n < this.this$0.mDisplayedWidgetsCount; ++n) {
                final ConstraintWidget constraintWidget = this.this$0.mDisplayedWidgets[this.mStartIndex + n];
                if (this.mOrientation == 0) {
                    final int width = constraintWidget.getWidth();
                    int access$000 = this.this$0.mHorizontalGap;
                    if (constraintWidget.getVisibility() == 8) {
                        access$000 = 0;
                    }
                    this.mWidth += width + access$000;
                    final int access$2 = this.this$0.getWidgetHeight(constraintWidget, this.mMax);
                    if (this.biggest == null || this.biggestDimension < access$2) {
                        this.biggest = constraintWidget;
                        this.biggestDimension = access$2;
                        this.mHeight = access$2;
                    }
                }
                else {
                    final int access$3 = this.this$0.getWidgetWidth(constraintWidget, this.mMax);
                    final int access$4 = this.this$0.getWidgetHeight(constraintWidget, this.mMax);
                    int access$5 = this.this$0.mVerticalGap;
                    if (constraintWidget.getVisibility() == 8) {
                        access$5 = 0;
                    }
                    this.mHeight += access$4 + access$5;
                    if (this.biggest == null || this.biggestDimension < access$3) {
                        this.biggest = constraintWidget;
                        this.biggestDimension = access$3;
                        this.mWidth = access$3;
                    }
                }
            }
        }
        
        public void add(final ConstraintWidget constraintWidget) {
            final int mOrientation = this.mOrientation;
            int n = 0;
            final int n2 = 0;
            if (mOrientation == 0) {
                int access$200 = this.this$0.getWidgetWidth(constraintWidget, this.mMax);
                if (constraintWidget.getHorizontalDimensionBehaviour() == DimensionBehaviour.MATCH_CONSTRAINT) {
                    ++this.mNbMatchConstraintsWidgets;
                    access$200 = 0;
                }
                int access$201 = this.this$0.mHorizontalGap;
                if (constraintWidget.getVisibility() == 8) {
                    access$201 = n2;
                }
                this.mWidth += access$200 + access$201;
                final int access$202 = this.this$0.getWidgetHeight(constraintWidget, this.mMax);
                if (this.biggest == null || this.biggestDimension < access$202) {
                    this.biggest = constraintWidget;
                    this.biggestDimension = access$202;
                    this.mHeight = access$202;
                }
            }
            else {
                final int access$203 = this.this$0.getWidgetWidth(constraintWidget, this.mMax);
                int access$204 = this.this$0.getWidgetHeight(constraintWidget, this.mMax);
                if (constraintWidget.getVerticalDimensionBehaviour() == DimensionBehaviour.MATCH_CONSTRAINT) {
                    ++this.mNbMatchConstraintsWidgets;
                    access$204 = 0;
                }
                final int access$205 = this.this$0.mVerticalGap;
                if (constraintWidget.getVisibility() != 8) {
                    n = access$205;
                }
                this.mHeight += access$204 + n;
                if (this.biggest == null || this.biggestDimension < access$203) {
                    this.biggest = constraintWidget;
                    this.biggestDimension = access$203;
                    this.mWidth = access$203;
                }
            }
            ++this.mCount;
        }
        
        public void clear() {
            this.biggestDimension = 0;
            this.biggest = null;
            this.mWidth = 0;
            this.mHeight = 0;
            this.mStartIndex = 0;
            this.mCount = 0;
            this.mNbMatchConstraintsWidgets = 0;
        }
        
        public void createConstraints(final boolean b, int i, final boolean b2) {
            final int mCount = this.mCount;
            for (int n = 0; n < mCount && this.mStartIndex + n < this.this$0.mDisplayedWidgetsCount; ++n) {
                final ConstraintWidget constraintWidget = this.this$0.mDisplayedWidgets[this.mStartIndex + n];
                if (constraintWidget != null) {
                    constraintWidget.resetAnchors();
                }
            }
            if (mCount != 0) {
                if (this.biggest != null) {
                    final boolean b3 = b2 && i == 0;
                    int j = 0;
                    int n2 = -1;
                    int n3 = -1;
                    while (j < mCount) {
                        int n4;
                        if (b) {
                            n4 = mCount - 1 - j;
                        }
                        else {
                            n4 = j;
                        }
                        if (this.mStartIndex + n4 >= this.this$0.mDisplayedWidgetsCount) {
                            break;
                        }
                        final ConstraintWidget constraintWidget2 = this.this$0.mDisplayedWidgets[this.mStartIndex + n4];
                        int n5 = n2;
                        int n6 = n3;
                        if (constraintWidget2 != null) {
                            n5 = n2;
                            n6 = n3;
                            if (constraintWidget2.getVisibility() == 0) {
                                int n7;
                                if ((n7 = n2) == -1) {
                                    n7 = j;
                                }
                                n6 = j;
                                n5 = n7;
                            }
                        }
                        ++j;
                        n2 = n5;
                        n3 = n6;
                    }
                    final ConstraintWidget constraintWidget3 = null;
                    ConstraintWidget constraintWidget4 = null;
                    if (this.mOrientation == 0) {
                        final ConstraintWidget biggest = this.biggest;
                        biggest.setVerticalChainStyle(this.this$0.mVerticalStyle);
                        int mPaddingTop = this.mPaddingTop;
                        if (i > 0) {
                            mPaddingTop += this.this$0.mVerticalGap;
                        }
                        biggest.mTop.connect(this.mTop, mPaddingTop);
                        if (b2) {
                            biggest.mBottom.connect(this.mBottom, this.mPaddingBottom);
                        }
                        if (i > 0) {
                            this.mTop.mOwner.mBottom.connect(biggest.mTop, 0);
                        }
                        ConstraintWidget constraintWidget5 = null;
                        Label_0459: {
                            if (this.this$0.mVerticalAlign == 3 && !biggest.hasBaseline()) {
                                int n8;
                                for (i = 0; i < mCount; ++i) {
                                    if (b) {
                                        n8 = mCount - 1 - i;
                                    }
                                    else {
                                        n8 = i;
                                    }
                                    if (this.mStartIndex + n8 >= this.this$0.mDisplayedWidgetsCount) {
                                        break;
                                    }
                                    constraintWidget5 = this.this$0.mDisplayedWidgets[this.mStartIndex + n8];
                                    if (constraintWidget5.hasBaseline()) {
                                        break Label_0459;
                                    }
                                }
                            }
                            constraintWidget5 = biggest;
                        }
                        for (int k = 0; k < mCount; ++k) {
                            if (b) {
                                i = mCount - 1 - k;
                            }
                            else {
                                i = k;
                            }
                            if (this.mStartIndex + i >= this.this$0.mDisplayedWidgetsCount) {
                                break;
                            }
                            final ConstraintWidget constraintWidget6 = this.this$0.mDisplayedWidgets[this.mStartIndex + i];
                            if (constraintWidget6 != null) {
                                if (k == 0) {
                                    constraintWidget6.connect(constraintWidget6.mLeft, this.mLeft, this.mPaddingLeft);
                                }
                                if (i == 0) {
                                    final int access$800 = this.this$0.mHorizontalStyle;
                                    float access$801;
                                    final float n9 = access$801 = this.this$0.mHorizontalBias;
                                    if (b) {
                                        access$801 = 1.0f - n9;
                                    }
                                    float horizontalBiasPercent = 0.0f;
                                    Label_0718: {
                                        float n10 = 0.0f;
                                        Label_0646: {
                                            float n11;
                                            if (this.mStartIndex == 0 && this.this$0.mFirstHorizontalStyle != -1) {
                                                i = this.this$0.mFirstHorizontalStyle;
                                                if (!b) {
                                                    n10 = this.this$0.mFirstHorizontalBias;
                                                    break Label_0646;
                                                }
                                                n11 = this.this$0.mFirstHorizontalBias;
                                            }
                                            else {
                                                i = access$800;
                                                horizontalBiasPercent = access$801;
                                                if (!b2) {
                                                    break Label_0718;
                                                }
                                                i = access$800;
                                                horizontalBiasPercent = access$801;
                                                if (this.this$0.mLastHorizontalStyle == -1) {
                                                    break Label_0718;
                                                }
                                                i = this.this$0.mLastHorizontalStyle;
                                                if (!b) {
                                                    n10 = this.this$0.mLastHorizontalBias;
                                                    break Label_0646;
                                                }
                                                n11 = this.this$0.mLastHorizontalBias;
                                            }
                                            n10 = 1.0f - n11;
                                        }
                                        horizontalBiasPercent = n10;
                                    }
                                    constraintWidget6.setHorizontalChainStyle(i);
                                    constraintWidget6.setHorizontalBiasPercent(horizontalBiasPercent);
                                }
                                if (k == mCount - 1) {
                                    constraintWidget6.connect(constraintWidget6.mRight, this.mRight, this.mPaddingRight);
                                }
                                if (constraintWidget4 != null) {
                                    constraintWidget6.mLeft.connect(constraintWidget4.mRight, this.this$0.mHorizontalGap);
                                    if (k == n2) {
                                        constraintWidget6.mLeft.setGoneMargin(this.mPaddingLeft);
                                    }
                                    constraintWidget4.mRight.connect(constraintWidget6.mLeft, 0);
                                    if (k == n3 + 1) {
                                        constraintWidget4.mRight.setGoneMargin(this.mPaddingRight);
                                    }
                                }
                                if ((constraintWidget4 = constraintWidget6) != biggest) {
                                    if (this.this$0.mVerticalAlign == 3 && constraintWidget5.hasBaseline() && constraintWidget6 != constraintWidget5 && constraintWidget6.hasBaseline()) {
                                        constraintWidget6.mBaseline.connect(constraintWidget5.mBaseline, 0);
                                        constraintWidget4 = constraintWidget6;
                                    }
                                    else {
                                        i = this.this$0.mVerticalAlign;
                                        if (i != 0) {
                                            if (i != 1) {
                                                if (b3) {
                                                    constraintWidget6.mTop.connect(this.mTop, this.mPaddingTop);
                                                    constraintWidget6.mBottom.connect(this.mBottom, this.mPaddingBottom);
                                                    constraintWidget4 = constraintWidget6;
                                                }
                                                else {
                                                    constraintWidget6.mTop.connect(biggest.mTop, 0);
                                                    constraintWidget6.mBottom.connect(biggest.mBottom, 0);
                                                    constraintWidget4 = constraintWidget6;
                                                }
                                            }
                                            else {
                                                constraintWidget6.mBottom.connect(biggest.mBottom, 0);
                                                constraintWidget4 = constraintWidget6;
                                            }
                                        }
                                        else {
                                            constraintWidget6.mTop.connect(biggest.mTop, 0);
                                            constraintWidget4 = constraintWidget6;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else {
                        final ConstraintWidget biggest2 = this.biggest;
                        biggest2.setHorizontalChainStyle(this.this$0.mHorizontalStyle);
                        int mPaddingLeft;
                        final int n12 = mPaddingLeft = this.mPaddingLeft;
                        if (i > 0) {
                            mPaddingLeft = n12 + this.this$0.mHorizontalGap;
                        }
                        if (b) {
                            biggest2.mRight.connect(this.mRight, mPaddingLeft);
                            if (b2) {
                                biggest2.mLeft.connect(this.mLeft, this.mPaddingRight);
                            }
                            if (i > 0) {
                                this.mRight.mOwner.mLeft.connect(biggest2.mRight, 0);
                            }
                        }
                        else {
                            biggest2.mLeft.connect(this.mLeft, mPaddingLeft);
                            if (b2) {
                                biggest2.mRight.connect(this.mRight, this.mPaddingRight);
                            }
                            if (i > 0) {
                                this.mLeft.mOwner.mRight.connect(biggest2.mLeft, 0);
                            }
                        }
                        int l = 0;
                        ConstraintWidget constraintWidget7 = constraintWidget3;
                        while (l < mCount) {
                            if (this.mStartIndex + l >= this.this$0.mDisplayedWidgetsCount) {
                                break;
                            }
                            final ConstraintWidget constraintWidget8 = this.this$0.mDisplayedWidgets[this.mStartIndex + l];
                            if (constraintWidget8 != null) {
                                if (l == 0) {
                                    constraintWidget8.connect(constraintWidget8.mTop, this.mTop, this.mPaddingTop);
                                    final int access$802 = this.this$0.mVerticalStyle;
                                    final float access$803 = this.this$0.mVerticalBias;
                                    float verticalBiasPercent;
                                    if (this.mStartIndex == 0 && this.this$0.mFirstVerticalStyle != -1) {
                                        i = this.this$0.mFirstVerticalStyle;
                                        verticalBiasPercent = this.this$0.mFirstVerticalBias;
                                    }
                                    else {
                                        i = access$802;
                                        verticalBiasPercent = access$803;
                                        if (b2) {
                                            i = access$802;
                                            verticalBiasPercent = access$803;
                                            if (this.this$0.mLastVerticalStyle != -1) {
                                                i = this.this$0.mLastVerticalStyle;
                                                verticalBiasPercent = this.this$0.mLastVerticalBias;
                                            }
                                        }
                                    }
                                    constraintWidget8.setVerticalChainStyle(i);
                                    constraintWidget8.setVerticalBiasPercent(verticalBiasPercent);
                                }
                                if (l == mCount - 1) {
                                    constraintWidget8.connect(constraintWidget8.mBottom, this.mBottom, this.mPaddingBottom);
                                }
                                if (constraintWidget7 != null) {
                                    constraintWidget8.mTop.connect(constraintWidget7.mBottom, this.this$0.mVerticalGap);
                                    if (l == n2) {
                                        constraintWidget8.mTop.setGoneMargin(this.mPaddingTop);
                                    }
                                    constraintWidget7.mBottom.connect(constraintWidget8.mTop, 0);
                                    if (l == n3 + 1) {
                                        constraintWidget7.mBottom.setGoneMargin(this.mPaddingBottom);
                                    }
                                }
                                if (constraintWidget8 != biggest2) {
                                    if (b) {
                                        i = this.this$0.mHorizontalAlign;
                                        if (i != 0) {
                                            if (i != 1) {
                                                if (i == 2) {
                                                    constraintWidget8.mLeft.connect(biggest2.mLeft, 0);
                                                    constraintWidget8.mRight.connect(biggest2.mRight, 0);
                                                }
                                            }
                                            else {
                                                constraintWidget8.mLeft.connect(biggest2.mLeft, 0);
                                            }
                                        }
                                        else {
                                            constraintWidget8.mRight.connect(biggest2.mRight, 0);
                                        }
                                    }
                                    else {
                                        i = this.this$0.mHorizontalAlign;
                                        if (i != 0) {
                                            if (i != 1) {
                                                if (i == 2) {
                                                    if (b3) {
                                                        constraintWidget8.mLeft.connect(this.mLeft, this.mPaddingLeft);
                                                        constraintWidget8.mRight.connect(this.mRight, this.mPaddingRight);
                                                    }
                                                    else {
                                                        constraintWidget8.mLeft.connect(biggest2.mLeft, 0);
                                                        constraintWidget8.mRight.connect(biggest2.mRight, 0);
                                                    }
                                                }
                                            }
                                            else {
                                                constraintWidget8.mRight.connect(biggest2.mRight, 0);
                                            }
                                        }
                                        else {
                                            constraintWidget8.mLeft.connect(biggest2.mLeft, 0);
                                        }
                                    }
                                }
                                constraintWidget7 = constraintWidget8;
                            }
                            ++l;
                        }
                    }
                }
            }
        }
        
        public int getHeight() {
            if (this.mOrientation == 1) {
                return this.mHeight - this.this$0.mVerticalGap;
            }
            return this.mHeight;
        }
        
        public int getWidth() {
            if (this.mOrientation == 0) {
                return this.mWidth - this.this$0.mHorizontalGap;
            }
            return this.mWidth;
        }
        
        public void measureMatchConstraints(int n) {
            final int mNbMatchConstraintsWidgets = this.mNbMatchConstraintsWidgets;
            if (mNbMatchConstraintsWidgets == 0) {
                return;
            }
            final int mCount = this.mCount;
            final int n2 = n / mNbMatchConstraintsWidgets;
            ConstraintWidget constraintWidget;
            for (n = 0; n < mCount && this.mStartIndex + n < this.this$0.mDisplayedWidgetsCount; ++n) {
                constraintWidget = this.this$0.mDisplayedWidgets[this.mStartIndex + n];
                if (this.mOrientation == 0) {
                    if (constraintWidget != null && constraintWidget.getHorizontalDimensionBehaviour() == DimensionBehaviour.MATCH_CONSTRAINT && constraintWidget.mMatchConstraintDefaultWidth == 0) {
                        this.this$0.measure(constraintWidget, DimensionBehaviour.FIXED, n2, constraintWidget.getVerticalDimensionBehaviour(), constraintWidget.getHeight());
                    }
                }
                else if (constraintWidget != null && constraintWidget.getVerticalDimensionBehaviour() == DimensionBehaviour.MATCH_CONSTRAINT && constraintWidget.mMatchConstraintDefaultHeight == 0) {
                    this.this$0.measure(constraintWidget, constraintWidget.getHorizontalDimensionBehaviour(), constraintWidget.getWidth(), DimensionBehaviour.FIXED, n2);
                }
            }
            this.recomputeDimensions();
        }
        
        public void setStartIndex(final int mStartIndex) {
            this.mStartIndex = mStartIndex;
        }
        
        public void setup(final int mOrientation, final ConstraintAnchor mLeft, final ConstraintAnchor mTop, final ConstraintAnchor mRight, final ConstraintAnchor mBottom, final int mPaddingLeft, final int mPaddingTop, final int mPaddingRight, final int mPaddingBottom, final int mMax) {
            this.mOrientation = mOrientation;
            this.mLeft = mLeft;
            this.mTop = mTop;
            this.mRight = mRight;
            this.mBottom = mBottom;
            this.mPaddingLeft = mPaddingLeft;
            this.mPaddingTop = mPaddingTop;
            this.mPaddingRight = mPaddingRight;
            this.mPaddingBottom = mPaddingBottom;
            this.mMax = mMax;
        }
    }
}

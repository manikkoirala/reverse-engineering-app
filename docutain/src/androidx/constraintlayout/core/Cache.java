// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.core;

public class Cache
{
    Pools.Pool<ArrayRow> arrayRowPool;
    SolverVariable[] mIndexedVariables;
    Pools.Pool<ArrayRow> optimizedArrayRowPool;
    Pools.Pool<SolverVariable> solverVariablePool;
    
    public Cache() {
        this.optimizedArrayRowPool = new Pools.SimplePool<ArrayRow>(256);
        this.arrayRowPool = new Pools.SimplePool<ArrayRow>(256);
        this.solverVariablePool = new Pools.SimplePool<SolverVariable>(256);
        this.mIndexedVariables = new SolverVariable[32];
    }
}

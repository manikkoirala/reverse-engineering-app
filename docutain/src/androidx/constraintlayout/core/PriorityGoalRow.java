// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.core;

import java.util.Comparator;
import java.util.Arrays;

public class PriorityGoalRow extends ArrayRow
{
    private static final boolean DEBUG = false;
    static final int NOT_FOUND = -1;
    private static final float epsilon = 1.0E-4f;
    private int TABLE_SIZE;
    GoalVariableAccessor accessor;
    private SolverVariable[] arrayGoals;
    Cache mCache;
    private int numGoals;
    private SolverVariable[] sortArray;
    
    public PriorityGoalRow(final Cache mCache) {
        super(mCache);
        this.TABLE_SIZE = 128;
        this.arrayGoals = new SolverVariable[128];
        this.sortArray = new SolverVariable[128];
        this.numGoals = 0;
        this.accessor = new GoalVariableAccessor(this);
        this.mCache = mCache;
    }
    
    private final void addToGoal(final SolverVariable solverVariable) {
        final int numGoals = this.numGoals;
        final SolverVariable[] arrayGoals = this.arrayGoals;
        if (numGoals + 1 > arrayGoals.length) {
            final SolverVariable[] array = Arrays.copyOf(arrayGoals, arrayGoals.length * 2);
            this.arrayGoals = array;
            this.sortArray = Arrays.copyOf(array, array.length * 2);
        }
        final SolverVariable[] arrayGoals2 = this.arrayGoals;
        int numGoals2 = this.numGoals;
        arrayGoals2[numGoals2] = solverVariable;
        ++numGoals2;
        this.numGoals = numGoals2;
        if (numGoals2 > 1 && arrayGoals2[numGoals2 - 1].id > solverVariable.id) {
            final int n = 0;
            int n2 = 0;
            int numGoals3;
            while (true) {
                numGoals3 = this.numGoals;
                if (n2 >= numGoals3) {
                    break;
                }
                this.sortArray[n2] = this.arrayGoals[n2];
                ++n2;
            }
            Arrays.sort(this.sortArray, 0, numGoals3, new Comparator<SolverVariable>(this) {
                final PriorityGoalRow this$0;
                
                @Override
                public int compare(final SolverVariable solverVariable, final SolverVariable solverVariable2) {
                    return solverVariable.id - solverVariable2.id;
                }
            });
            for (int i = n; i < this.numGoals; ++i) {
                this.arrayGoals[i] = this.sortArray[i];
            }
        }
        solverVariable.inGoal = true;
        solverVariable.addToRow(this);
    }
    
    private final void removeGoal(final SolverVariable solverVariable) {
        for (int i = 0; i < this.numGoals; ++i) {
            if (this.arrayGoals[i] == solverVariable) {
                int numGoals;
                while (true) {
                    numGoals = this.numGoals;
                    if (i >= numGoals - 1) {
                        break;
                    }
                    final SolverVariable[] arrayGoals = this.arrayGoals;
                    final int n = i + 1;
                    arrayGoals[i] = arrayGoals[n];
                    i = n;
                }
                this.numGoals = numGoals - 1;
                solverVariable.inGoal = false;
                return;
            }
        }
    }
    
    @Override
    public void addError(final SolverVariable solverVariable) {
        this.accessor.init(solverVariable);
        this.accessor.reset();
        solverVariable.goalStrengthVector[solverVariable.strength] = 1.0f;
        this.addToGoal(solverVariable);
    }
    
    @Override
    public void clear() {
        this.numGoals = 0;
        this.constantValue = 0.0f;
    }
    
    @Override
    public SolverVariable getPivotCandidate(final LinearSystem linearSystem, final boolean[] array) {
        int i = 0;
        int n = -1;
        while (i < this.numGoals) {
            final SolverVariable solverVariable = this.arrayGoals[i];
            int n2 = 0;
            Label_0091: {
                if (array[solverVariable.id]) {
                    n2 = n;
                }
                else {
                    this.accessor.init(solverVariable);
                    if (n == -1) {
                        n2 = n;
                        if (!this.accessor.isNegative()) {
                            break Label_0091;
                        }
                    }
                    else {
                        n2 = n;
                        if (!this.accessor.isSmallerThan(this.arrayGoals[n])) {
                            break Label_0091;
                        }
                    }
                    n2 = i;
                }
            }
            ++i;
            n = n2;
        }
        if (n == -1) {
            return null;
        }
        return this.arrayGoals[n];
    }
    
    @Override
    public boolean isEmpty() {
        return this.numGoals == 0;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("");
        sb.append(" goal -> (");
        sb.append(this.constantValue);
        sb.append(") : ");
        String str = sb.toString();
        for (int i = 0; i < this.numGoals; ++i) {
            this.accessor.init(this.arrayGoals[i]);
            final StringBuilder sb2 = new StringBuilder();
            sb2.append(str);
            sb2.append(this.accessor);
            sb2.append(" ");
            str = sb2.toString();
        }
        return str;
    }
    
    @Override
    public void updateFromRow(final LinearSystem linearSystem, final ArrayRow arrayRow, final boolean b) {
        final SolverVariable variable = arrayRow.variable;
        if (variable == null) {
            return;
        }
        final ArrayRowVariables variables = arrayRow.variables;
        for (int currentSize = variables.getCurrentSize(), i = 0; i < currentSize; ++i) {
            final SolverVariable variable2 = variables.getVariable(i);
            final float variableValue = variables.getVariableValue(i);
            this.accessor.init(variable2);
            if (this.accessor.addToGoal(variable, variableValue)) {
                this.addToGoal(variable2);
            }
            this.constantValue += arrayRow.constantValue * variableValue;
        }
        this.removeGoal(variable);
    }
    
    class GoalVariableAccessor
    {
        PriorityGoalRow row;
        final PriorityGoalRow this$0;
        SolverVariable variable;
        
        public GoalVariableAccessor(final PriorityGoalRow this$0, final PriorityGoalRow row) {
            this.this$0 = this$0;
            this.row = row;
        }
        
        public void add(final SolverVariable solverVariable) {
            for (int i = 0; i < 9; ++i) {
                final float[] goalStrengthVector = this.variable.goalStrengthVector;
                goalStrengthVector[i] += solverVariable.goalStrengthVector[i];
                if (Math.abs(this.variable.goalStrengthVector[i]) < 1.0E-4f) {
                    this.variable.goalStrengthVector[i] = 0.0f;
                }
            }
        }
        
        public boolean addToGoal(final SolverVariable solverVariable, final float n) {
            final boolean inGoal = this.variable.inGoal;
            boolean b = true;
            int i = 0;
            if (inGoal) {
                for (int j = 0; j < 9; ++j) {
                    final float[] goalStrengthVector = this.variable.goalStrengthVector;
                    goalStrengthVector[j] += solverVariable.goalStrengthVector[j] * n;
                    if (Math.abs(this.variable.goalStrengthVector[j]) < 1.0E-4f) {
                        this.variable.goalStrengthVector[j] = 0.0f;
                    }
                    else {
                        b = false;
                    }
                }
                if (b) {
                    this.this$0.removeGoal(this.variable);
                }
                return false;
            }
            while (i < 9) {
                final float n2 = solverVariable.goalStrengthVector[i];
                if (n2 != 0.0f) {
                    float n3;
                    if (Math.abs(n3 = n2 * n) < 1.0E-4f) {
                        n3 = 0.0f;
                    }
                    this.variable.goalStrengthVector[i] = n3;
                }
                else {
                    this.variable.goalStrengthVector[i] = 0.0f;
                }
                ++i;
            }
            return true;
        }
        
        public void init(final SolverVariable variable) {
            this.variable = variable;
        }
        
        public final boolean isNegative() {
            for (int i = 8; i >= 0; --i) {
                final float n = this.variable.goalStrengthVector[i];
                if (n > 0.0f) {
                    return false;
                }
                if (n < 0.0f) {
                    return true;
                }
            }
            return false;
        }
        
        public final boolean isNull() {
            for (int i = 0; i < 9; ++i) {
                if (this.variable.goalStrengthVector[i] != 0.0f) {
                    return false;
                }
            }
            return true;
        }
        
        public final boolean isSmallerThan(final SolverVariable solverVariable) {
            int i = 8;
            while (i >= 0) {
                final float n = solverVariable.goalStrengthVector[i];
                final float n2 = this.variable.goalStrengthVector[i];
                if (n2 == n) {
                    --i;
                }
                else {
                    if (n2 < n) {
                        return true;
                    }
                    break;
                }
            }
            return false;
        }
        
        public void reset() {
            Arrays.fill(this.variable.goalStrengthVector, 0.0f);
        }
        
        @Override
        public String toString() {
            final SolverVariable variable = this.variable;
            String str;
            String string = str = "[ ";
            if (variable != null) {
                int n = 0;
                while (true) {
                    str = string;
                    if (n >= 9) {
                        break;
                    }
                    final StringBuilder sb = new StringBuilder();
                    sb.append(string);
                    sb.append(this.variable.goalStrengthVector[n]);
                    sb.append(" ");
                    string = sb.toString();
                    ++n;
                }
            }
            final StringBuilder sb2 = new StringBuilder();
            sb2.append(str);
            sb2.append("] ");
            sb2.append(this.variable);
            return sb2.toString();
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.core;

import java.io.PrintStream;
import androidx.constraintlayout.core.widgets.ConstraintAnchor;
import androidx.constraintlayout.core.widgets.ConstraintWidget;
import java.util.Arrays;
import java.util.HashMap;

public class LinearSystem
{
    public static long ARRAY_ROW_CREATION = 0L;
    public static final boolean DEBUG = false;
    private static final boolean DEBUG_CONSTRAINTS = false;
    public static final boolean FULL_DEBUG = false;
    public static final boolean MEASURE = false;
    public static long OPTIMIZED_ARRAY_ROW_CREATION = 0L;
    public static boolean OPTIMIZED_ENGINE = false;
    private static int POOL_SIZE = 1000;
    public static boolean SIMPLIFY_SYNONYMS = true;
    public static boolean SKIP_COLUMNS = true;
    public static boolean USE_BASIC_SYNONYMS = true;
    public static boolean USE_DEPENDENCY_ORDERING = false;
    public static boolean USE_SYNONYMS = true;
    public static Metrics sMetrics;
    private int TABLE_SIZE;
    public boolean graphOptimizer;
    public boolean hasSimpleDefinition;
    private boolean[] mAlreadyTestedCandidates;
    final Cache mCache;
    private Row mGoal;
    private int mMaxColumns;
    private int mMaxRows;
    int mNumColumns;
    int mNumRows;
    private SolverVariable[] mPoolVariables;
    private int mPoolVariablesCount;
    ArrayRow[] mRows;
    private Row mTempGoal;
    private HashMap<String, SolverVariable> mVariables;
    int mVariablesID;
    public boolean newgraphOptimizer;
    
    public LinearSystem() {
        this.hasSimpleDefinition = false;
        this.mVariablesID = 0;
        this.mVariables = null;
        this.TABLE_SIZE = 32;
        this.mMaxColumns = 32;
        this.mRows = null;
        this.graphOptimizer = false;
        this.newgraphOptimizer = false;
        this.mAlreadyTestedCandidates = new boolean[32];
        this.mNumColumns = 1;
        this.mNumRows = 0;
        this.mMaxRows = 32;
        this.mPoolVariables = new SolverVariable[LinearSystem.POOL_SIZE];
        this.mPoolVariablesCount = 0;
        this.mRows = new ArrayRow[32];
        this.releaseRows();
        final Cache mCache = new Cache();
        this.mCache = mCache;
        this.mGoal = (Row)new PriorityGoalRow(mCache);
        if (LinearSystem.OPTIMIZED_ENGINE) {
            this.mTempGoal = (Row)new ValuesRow(mCache);
        }
        else {
            this.mTempGoal = (Row)new ArrayRow(mCache);
        }
    }
    
    private SolverVariable acquireSolverVariable(final SolverVariable.Type type, final String s) {
        final SolverVariable solverVariable = this.mCache.solverVariablePool.acquire();
        SolverVariable solverVariable3;
        if (solverVariable == null) {
            final SolverVariable solverVariable2 = new SolverVariable(type, s);
            solverVariable2.setType(type, s);
            solverVariable3 = solverVariable2;
        }
        else {
            solverVariable.reset();
            solverVariable.setType(type, s);
            solverVariable3 = solverVariable;
        }
        final int mPoolVariablesCount = this.mPoolVariablesCount;
        final int pool_SIZE = LinearSystem.POOL_SIZE;
        if (mPoolVariablesCount >= pool_SIZE) {
            this.mPoolVariables = Arrays.copyOf(this.mPoolVariables, LinearSystem.POOL_SIZE = pool_SIZE * 2);
        }
        return this.mPoolVariables[this.mPoolVariablesCount++] = solverVariable3;
    }
    
    private void addError(final ArrayRow arrayRow) {
        arrayRow.addError(this, 0);
    }
    
    private final void addRow(ArrayRow arrayRow) {
        if (LinearSystem.SIMPLIFY_SYNONYMS && arrayRow.isSimpleDefinition) {
            arrayRow.variable.setFinalValue(this, arrayRow.constantValue);
        }
        else {
            this.mRows[this.mNumRows] = arrayRow;
            arrayRow.variable.definitionId = this.mNumRows;
            ++this.mNumRows;
            arrayRow.variable.updateReferencesWithNewDefinition(this, arrayRow);
        }
        if (LinearSystem.SIMPLIFY_SYNONYMS && this.hasSimpleDefinition) {
            int n;
            for (int i = 0; i < this.mNumRows; i = n + 1) {
                if (this.mRows[i] == null) {
                    System.out.println("WTF");
                }
                arrayRow = this.mRows[i];
                n = i;
                if (arrayRow != null) {
                    n = i;
                    if (arrayRow.isSimpleDefinition) {
                        arrayRow = this.mRows[i];
                        arrayRow.variable.setFinalValue(this, arrayRow.constantValue);
                        if (LinearSystem.OPTIMIZED_ENGINE) {
                            this.mCache.optimizedArrayRowPool.release(arrayRow);
                        }
                        else {
                            this.mCache.arrayRowPool.release(arrayRow);
                        }
                        this.mRows[i] = null;
                        int n3;
                        int n2 = n3 = i + 1;
                        int mNumRows;
                        while (true) {
                            mNumRows = this.mNumRows;
                            if (n2 >= mNumRows) {
                                break;
                            }
                            final ArrayRow[] mRows = this.mRows;
                            final int definitionId = n2 - 1;
                            final ArrayRow arrayRow2 = mRows[n2];
                            mRows[definitionId] = arrayRow2;
                            if (arrayRow2.variable.definitionId == n2) {
                                this.mRows[definitionId].variable.definitionId = definitionId;
                            }
                            n3 = n2;
                            ++n2;
                        }
                        if (n3 < mNumRows) {
                            this.mRows[n3] = null;
                        }
                        this.mNumRows = mNumRows - 1;
                        n = i - 1;
                    }
                }
            }
            this.hasSimpleDefinition = false;
        }
    }
    
    private void addSingleError(final ArrayRow arrayRow, final int n) {
        this.addSingleError(arrayRow, n, 0);
    }
    
    private void computeValues() {
        for (int i = 0; i < this.mNumRows; ++i) {
            final ArrayRow arrayRow = this.mRows[i];
            arrayRow.variable.computedValue = arrayRow.constantValue;
        }
    }
    
    public static ArrayRow createRowDimensionPercent(final LinearSystem linearSystem, final SolverVariable solverVariable, final SolverVariable solverVariable2, final float n) {
        return linearSystem.createRow().createRowDimensionPercent(solverVariable, solverVariable2, n);
    }
    
    private SolverVariable createVariable(final String s, final SolverVariable.Type type) {
        final Metrics sMetrics = LinearSystem.sMetrics;
        if (sMetrics != null) {
            ++sMetrics.variables;
        }
        if (this.mNumColumns + 1 >= this.mMaxColumns) {
            this.increaseTableSize();
        }
        final SolverVariable acquireSolverVariable = this.acquireSolverVariable(type, null);
        acquireSolverVariable.setName(s);
        final int n = this.mVariablesID + 1;
        this.mVariablesID = n;
        ++this.mNumColumns;
        acquireSolverVariable.id = n;
        if (this.mVariables == null) {
            this.mVariables = new HashMap<String, SolverVariable>();
        }
        this.mVariables.put(s, acquireSolverVariable);
        return this.mCache.mIndexedVariables[this.mVariablesID] = acquireSolverVariable;
    }
    
    private void displayRows() {
        this.displaySolverVariables();
        String string = "";
        for (int i = 0; i < this.mNumRows; ++i) {
            final StringBuilder sb = new StringBuilder();
            sb.append(string);
            sb.append(this.mRows[i]);
            final String string2 = sb.toString();
            final StringBuilder sb2 = new StringBuilder();
            sb2.append(string2);
            sb2.append("\n");
            string = sb2.toString();
        }
        final StringBuilder sb3 = new StringBuilder();
        sb3.append(string);
        sb3.append(this.mGoal);
        sb3.append("\n");
        System.out.println(sb3.toString());
    }
    
    private void displaySolverVariables() {
        final StringBuilder sb = new StringBuilder();
        sb.append("Display Rows (");
        sb.append(this.mNumRows);
        sb.append("x");
        sb.append(this.mNumColumns);
        sb.append(")\n");
        System.out.println(sb.toString());
    }
    
    private int enforceBFS(final Row row) throws Exception {
        while (true) {
            for (int i = 0; i < this.mNumRows; ++i) {
                if (this.mRows[i].variable.mType != SolverVariable.Type.UNRESTRICTED) {
                    if (this.mRows[i].constantValue < 0.0f) {
                        final boolean b = true;
                        int n;
                        if (b) {
                            int j = 0;
                            n = 0;
                            while (j == 0) {
                                final Metrics sMetrics = LinearSystem.sMetrics;
                                if (sMetrics != null) {
                                    ++sMetrics.bfs;
                                }
                                final int n2 = n + 1;
                                float n3 = Float.MAX_VALUE;
                                int k = 0;
                                int definitionId = -1;
                                int n4 = -1;
                                int n5 = 0;
                                while (k < this.mNumRows) {
                                    final ArrayRow arrayRow = this.mRows[k];
                                    float n6;
                                    int n7;
                                    int n8;
                                    int n9;
                                    if (arrayRow.variable.mType == SolverVariable.Type.UNRESTRICTED) {
                                        n6 = n3;
                                        n7 = definitionId;
                                        n8 = n4;
                                        n9 = n5;
                                    }
                                    else if (arrayRow.isSimpleDefinition) {
                                        n6 = n3;
                                        n7 = definitionId;
                                        n8 = n4;
                                        n9 = n5;
                                    }
                                    else {
                                        n6 = n3;
                                        n7 = definitionId;
                                        n8 = n4;
                                        n9 = n5;
                                        if (arrayRow.constantValue < 0.0f) {
                                            if (LinearSystem.SKIP_COLUMNS) {
                                                final int currentSize = arrayRow.variables.getCurrentSize();
                                                int n10 = 0;
                                                int n11 = n5;
                                                int id = n4;
                                                while (true) {
                                                    n6 = n3;
                                                    n7 = definitionId;
                                                    n8 = id;
                                                    n9 = n11;
                                                    if (n10 >= currentSize) {
                                                        break;
                                                    }
                                                    final SolverVariable variable = arrayRow.variables.getVariable(n10);
                                                    final float value = arrayRow.variables.get(variable);
                                                    float n12;
                                                    int n13;
                                                    int n14;
                                                    int n15;
                                                    if (value <= 0.0f) {
                                                        n12 = n3;
                                                        n13 = definitionId;
                                                        n14 = id;
                                                        n15 = n11;
                                                    }
                                                    else {
                                                        final int n16 = 0;
                                                        int n17 = definitionId;
                                                        int n18 = n16;
                                                        while (true) {
                                                            n12 = n3;
                                                            n13 = n17;
                                                            n14 = id;
                                                            n15 = n11;
                                                            if (n18 >= 9) {
                                                                break;
                                                            }
                                                            final float n19 = variable.strengthVector[n18] / value;
                                                            int n20;
                                                            if ((n19 < n3 && n18 == n11) || n18 > (n20 = n11)) {
                                                                id = variable.id;
                                                                n20 = n18;
                                                                n17 = k;
                                                                n3 = n19;
                                                            }
                                                            ++n18;
                                                            n11 = n20;
                                                        }
                                                    }
                                                    ++n10;
                                                    n3 = n12;
                                                    definitionId = n13;
                                                    id = n14;
                                                    n11 = n15;
                                                }
                                            }
                                            else {
                                                int n21 = 1;
                                                while (true) {
                                                    n6 = n3;
                                                    n7 = definitionId;
                                                    n8 = n4;
                                                    n9 = n5;
                                                    if (n21 >= this.mNumColumns) {
                                                        break;
                                                    }
                                                    final SolverVariable solverVariable = this.mCache.mIndexedVariables[n21];
                                                    final float value2 = arrayRow.variables.get(solverVariable);
                                                    float n22;
                                                    int n23;
                                                    int n24;
                                                    int n25;
                                                    if (value2 <= 0.0f) {
                                                        n22 = n3;
                                                        n23 = definitionId;
                                                        n24 = n4;
                                                        n25 = n5;
                                                    }
                                                    else {
                                                        final int n26 = 0;
                                                        int n27 = definitionId;
                                                        int n28 = n26;
                                                        while (true) {
                                                            n22 = n3;
                                                            n23 = n27;
                                                            n24 = n4;
                                                            n25 = n5;
                                                            if (n28 >= 9) {
                                                                break;
                                                            }
                                                            final float n29 = solverVariable.strengthVector[n28] / value2;
                                                            int n30;
                                                            if ((n29 < n3 && n28 == n5) || n28 > (n30 = n5)) {
                                                                n4 = n21;
                                                                n30 = n28;
                                                                n27 = k;
                                                                n3 = n29;
                                                            }
                                                            ++n28;
                                                            n5 = n30;
                                                        }
                                                    }
                                                    ++n21;
                                                    n3 = n22;
                                                    definitionId = n23;
                                                    n4 = n24;
                                                    n5 = n25;
                                                }
                                            }
                                        }
                                    }
                                    ++k;
                                    n3 = n6;
                                    definitionId = n7;
                                    n4 = n8;
                                    n5 = n9;
                                }
                                if (definitionId != -1) {
                                    final ArrayRow arrayRow2 = this.mRows[definitionId];
                                    arrayRow2.variable.definitionId = -1;
                                    final Metrics sMetrics2 = LinearSystem.sMetrics;
                                    if (sMetrics2 != null) {
                                        ++sMetrics2.pivots;
                                    }
                                    arrayRow2.pivot(this.mCache.mIndexedVariables[n4]);
                                    arrayRow2.variable.definitionId = definitionId;
                                    arrayRow2.variable.updateReferencesWithNewDefinition(this, arrayRow2);
                                }
                                else {
                                    j = 1;
                                }
                                n = n2;
                                if (n2 > this.mNumColumns / 2) {
                                    j = 1;
                                    n = n2;
                                }
                            }
                        }
                        else {
                            n = 0;
                        }
                        return n;
                    }
                }
            }
            final boolean b = false;
            continue;
        }
    }
    
    private String getDisplaySize(int i) {
        final int j = i * 4;
        i = j / 1024;
        final int k = i / 1024;
        if (k > 0) {
            final StringBuilder sb = new StringBuilder();
            sb.append("");
            sb.append(k);
            sb.append(" Mb");
            return sb.toString();
        }
        if (i > 0) {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("");
            sb2.append(i);
            sb2.append(" Kb");
            return sb2.toString();
        }
        final StringBuilder sb3 = new StringBuilder();
        sb3.append("");
        sb3.append(j);
        sb3.append(" bytes");
        return sb3.toString();
    }
    
    private String getDisplayStrength(final int n) {
        if (n == 1) {
            return "LOW";
        }
        if (n == 2) {
            return "MEDIUM";
        }
        if (n == 3) {
            return "HIGH";
        }
        if (n == 4) {
            return "HIGHEST";
        }
        if (n == 5) {
            return "EQUALITY";
        }
        if (n == 8) {
            return "FIXED";
        }
        if (n == 6) {
            return "BARRIER";
        }
        return "NONE";
    }
    
    public static Metrics getMetrics() {
        return LinearSystem.sMetrics;
    }
    
    private void increaseTableSize() {
        final int n = this.TABLE_SIZE * 2;
        this.TABLE_SIZE = n;
        this.mRows = Arrays.copyOf(this.mRows, n);
        final Cache mCache = this.mCache;
        mCache.mIndexedVariables = Arrays.copyOf(mCache.mIndexedVariables, this.TABLE_SIZE);
        final int table_SIZE = this.TABLE_SIZE;
        this.mAlreadyTestedCandidates = new boolean[table_SIZE];
        this.mMaxColumns = table_SIZE;
        this.mMaxRows = table_SIZE;
        final Metrics sMetrics = LinearSystem.sMetrics;
        if (sMetrics != null) {
            ++sMetrics.tableSizeIncrease;
            final Metrics sMetrics2 = LinearSystem.sMetrics;
            sMetrics2.maxTableSize = Math.max(sMetrics2.maxTableSize, this.TABLE_SIZE);
            final Metrics sMetrics3 = LinearSystem.sMetrics;
            sMetrics3.lastTableSize = sMetrics3.maxTableSize;
        }
    }
    
    private final int optimize(final Row row, final boolean b) {
        final Metrics sMetrics = LinearSystem.sMetrics;
        if (sMetrics != null) {
            ++sMetrics.optimize;
        }
        for (int i = 0; i < this.mNumColumns; ++i) {
            this.mAlreadyTestedCandidates[i] = false;
        }
        int j = 0;
        int n = 0;
        while (j == 0) {
            final Metrics sMetrics2 = LinearSystem.sMetrics;
            if (sMetrics2 != null) {
                ++sMetrics2.iterations;
            }
            final int n2 = n + 1;
            if (n2 >= this.mNumColumns * 2) {
                return n2;
            }
            if (row.getKey() != null) {
                this.mAlreadyTestedCandidates[row.getKey().id] = true;
            }
            final SolverVariable pivotCandidate = row.getPivotCandidate(this, this.mAlreadyTestedCandidates);
            if (pivotCandidate != null) {
                if (this.mAlreadyTestedCandidates[pivotCandidate.id]) {
                    return n2;
                }
                this.mAlreadyTestedCandidates[pivotCandidate.id] = true;
            }
            if (pivotCandidate != null) {
                float n3 = Float.MAX_VALUE;
                int k = 0;
                int definitionId = -1;
                while (k < this.mNumRows) {
                    final ArrayRow arrayRow = this.mRows[k];
                    float n4;
                    int n5;
                    if (arrayRow.variable.mType == SolverVariable.Type.UNRESTRICTED) {
                        n4 = n3;
                        n5 = definitionId;
                    }
                    else if (arrayRow.isSimpleDefinition) {
                        n4 = n3;
                        n5 = definitionId;
                    }
                    else {
                        n4 = n3;
                        n5 = definitionId;
                        if (arrayRow.hasVariable(pivotCandidate)) {
                            final float value = arrayRow.variables.get(pivotCandidate);
                            n4 = n3;
                            n5 = definitionId;
                            if (value < 0.0f) {
                                final float n6 = -arrayRow.constantValue / value;
                                n4 = n3;
                                n5 = definitionId;
                                if (n6 < n3) {
                                    n5 = k;
                                    n4 = n6;
                                }
                            }
                        }
                    }
                    ++k;
                    n3 = n4;
                    definitionId = n5;
                }
                n = n2;
                if (definitionId <= -1) {
                    continue;
                }
                final ArrayRow arrayRow2 = this.mRows[definitionId];
                arrayRow2.variable.definitionId = -1;
                final Metrics sMetrics3 = LinearSystem.sMetrics;
                if (sMetrics3 != null) {
                    ++sMetrics3.pivots;
                }
                arrayRow2.pivot(pivotCandidate);
                arrayRow2.variable.definitionId = definitionId;
                arrayRow2.variable.updateReferencesWithNewDefinition(this, arrayRow2);
                n = n2;
            }
            else {
                j = 1;
                n = n2;
            }
        }
        return n;
    }
    
    private void releaseRows() {
        final boolean optimized_ENGINE = LinearSystem.OPTIMIZED_ENGINE;
        int i = 0;
        final int n = 0;
        if (optimized_ENGINE) {
            for (int j = n; j < this.mNumRows; ++j) {
                final ArrayRow arrayRow = this.mRows[j];
                if (arrayRow != null) {
                    this.mCache.optimizedArrayRowPool.release(arrayRow);
                }
                this.mRows[j] = null;
            }
        }
        else {
            while (i < this.mNumRows) {
                final ArrayRow arrayRow2 = this.mRows[i];
                if (arrayRow2 != null) {
                    this.mCache.arrayRowPool.release(arrayRow2);
                }
                this.mRows[i] = null;
                ++i;
            }
        }
    }
    
    public void addCenterPoint(final ConstraintWidget constraintWidget, final ConstraintWidget constraintWidget2, final float n, final int n2) {
        final SolverVariable objectVariable = this.createObjectVariable(constraintWidget.getAnchor(ConstraintAnchor.Type.LEFT));
        final SolverVariable objectVariable2 = this.createObjectVariable(constraintWidget.getAnchor(ConstraintAnchor.Type.TOP));
        final SolverVariable objectVariable3 = this.createObjectVariable(constraintWidget.getAnchor(ConstraintAnchor.Type.RIGHT));
        final SolverVariable objectVariable4 = this.createObjectVariable(constraintWidget.getAnchor(ConstraintAnchor.Type.BOTTOM));
        final SolverVariable objectVariable5 = this.createObjectVariable(constraintWidget2.getAnchor(ConstraintAnchor.Type.LEFT));
        final SolverVariable objectVariable6 = this.createObjectVariable(constraintWidget2.getAnchor(ConstraintAnchor.Type.TOP));
        final SolverVariable objectVariable7 = this.createObjectVariable(constraintWidget2.getAnchor(ConstraintAnchor.Type.RIGHT));
        final SolverVariable objectVariable8 = this.createObjectVariable(constraintWidget2.getAnchor(ConstraintAnchor.Type.BOTTOM));
        final ArrayRow row = this.createRow();
        final double n3 = n;
        final double sin = Math.sin(n3);
        final double n4 = n2;
        row.createRowWithAngle(objectVariable2, objectVariable4, objectVariable6, objectVariable8, (float)(sin * n4));
        this.addConstraint(row);
        final ArrayRow row2 = this.createRow();
        row2.createRowWithAngle(objectVariable, objectVariable3, objectVariable5, objectVariable7, (float)(Math.cos(n3) * n4));
        this.addConstraint(row2);
    }
    
    public void addCentering(final SolverVariable solverVariable, final SolverVariable solverVariable2, final int n, final float n2, final SolverVariable solverVariable3, final SolverVariable solverVariable4, final int n3, final int n4) {
        final ArrayRow row = this.createRow();
        row.createRowCentering(solverVariable, solverVariable2, n, n2, solverVariable3, solverVariable4, n3);
        if (n4 != 8) {
            row.addError(this, n4);
        }
        this.addConstraint(row);
    }
    
    public void addConstraint(final ArrayRow arrayRow) {
        if (arrayRow == null) {
            return;
        }
        final Metrics sMetrics = LinearSystem.sMetrics;
        if (sMetrics != null) {
            ++sMetrics.constraints;
            if (arrayRow.isSimpleDefinition) {
                final Metrics sMetrics2 = LinearSystem.sMetrics;
                ++sMetrics2.simpleconstraints;
            }
        }
        final int mNumRows = this.mNumRows;
        final boolean b = true;
        if (mNumRows + 1 >= this.mMaxRows || this.mNumColumns + 1 >= this.mMaxColumns) {
            this.increaseTableSize();
        }
        int n = 0;
        if (!arrayRow.isSimpleDefinition) {
            arrayRow.updateFromSystem(this);
            if (arrayRow.isEmpty()) {
                return;
            }
            arrayRow.ensurePositiveConstant();
            Label_0302: {
                if (arrayRow.chooseSubject(this)) {
                    final SolverVariable extraVariable = this.createExtraVariable();
                    arrayRow.variable = extraVariable;
                    final int mNumRows2 = this.mNumRows;
                    this.addRow(arrayRow);
                    if (this.mNumRows == mNumRows2 + 1) {
                        this.mTempGoal.initFromRow((Row)arrayRow);
                        this.optimize(this.mTempGoal, true);
                        n = (b ? 1 : 0);
                        if (extraVariable.definitionId == -1) {
                            if (arrayRow.variable == extraVariable) {
                                final SolverVariable pickPivot = arrayRow.pickPivot(extraVariable);
                                if (pickPivot != null) {
                                    final Metrics sMetrics3 = LinearSystem.sMetrics;
                                    if (sMetrics3 != null) {
                                        ++sMetrics3.pivots;
                                    }
                                    arrayRow.pivot(pickPivot);
                                }
                            }
                            if (!arrayRow.isSimpleDefinition) {
                                arrayRow.variable.updateReferencesWithNewDefinition(this, arrayRow);
                            }
                            if (LinearSystem.OPTIMIZED_ENGINE) {
                                this.mCache.optimizedArrayRowPool.release(arrayRow);
                            }
                            else {
                                this.mCache.arrayRowPool.release(arrayRow);
                            }
                            --this.mNumRows;
                            n = (b ? 1 : 0);
                        }
                        break Label_0302;
                    }
                }
                n = 0;
            }
            if (!arrayRow.hasKeyVariable()) {
                return;
            }
        }
        if (n == 0) {
            this.addRow(arrayRow);
        }
    }
    
    public ArrayRow addEquality(final SolverVariable solverVariable, final SolverVariable solverVariable2, final int n, final int n2) {
        if (LinearSystem.USE_BASIC_SYNONYMS && n2 == 8 && solverVariable2.isFinalValue && solverVariable.definitionId == -1) {
            solverVariable.setFinalValue(this, solverVariable2.computedValue + n);
            return null;
        }
        final ArrayRow row = this.createRow();
        row.createRowEquals(solverVariable, solverVariable2, n);
        if (n2 != 8) {
            row.addError(this, n2);
        }
        this.addConstraint(row);
        return row;
    }
    
    public void addEquality(final SolverVariable solverVariable, int i) {
        if (LinearSystem.USE_BASIC_SYNONYMS && solverVariable.definitionId == -1) {
            final float n = (float)i;
            solverVariable.setFinalValue(this, n);
            SolverVariable solverVariable2;
            for (i = 0; i < this.mVariablesID + 1; ++i) {
                solverVariable2 = this.mCache.mIndexedVariables[i];
                if (solverVariable2 != null && solverVariable2.isSynonym && solverVariable2.synonym == solverVariable.id) {
                    solverVariable2.setFinalValue(this, solverVariable2.synonymDelta + n);
                }
            }
            return;
        }
        final int definitionId = solverVariable.definitionId;
        if (solverVariable.definitionId != -1) {
            final ArrayRow arrayRow = this.mRows[definitionId];
            if (arrayRow.isSimpleDefinition) {
                arrayRow.constantValue = (float)i;
            }
            else if (arrayRow.variables.getCurrentSize() == 0) {
                arrayRow.isSimpleDefinition = true;
                arrayRow.constantValue = (float)i;
            }
            else {
                final ArrayRow row = this.createRow();
                row.createRowEquals(solverVariable, i);
                this.addConstraint(row);
            }
        }
        else {
            final ArrayRow row2 = this.createRow();
            row2.createRowDefinition(solverVariable, i);
            this.addConstraint(row2);
        }
    }
    
    public void addGreaterBarrier(final SolverVariable solverVariable, final SolverVariable solverVariable2, final int n, final boolean b) {
        final ArrayRow row = this.createRow();
        final SolverVariable slackVariable = this.createSlackVariable();
        slackVariable.strength = 0;
        row.createRowGreaterThan(solverVariable, solverVariable2, slackVariable, n);
        this.addConstraint(row);
    }
    
    public void addGreaterThan(final SolverVariable solverVariable, final SolverVariable solverVariable2, final int n, final int n2) {
        final ArrayRow row = this.createRow();
        final SolverVariable slackVariable = this.createSlackVariable();
        slackVariable.strength = 0;
        row.createRowGreaterThan(solverVariable, solverVariable2, slackVariable, n);
        if (n2 != 8) {
            this.addSingleError(row, (int)(row.variables.get(slackVariable) * -1.0f), n2);
        }
        this.addConstraint(row);
    }
    
    public void addLowerBarrier(final SolverVariable solverVariable, final SolverVariable solverVariable2, final int n, final boolean b) {
        final ArrayRow row = this.createRow();
        final SolverVariable slackVariable = this.createSlackVariable();
        slackVariable.strength = 0;
        row.createRowLowerThan(solverVariable, solverVariable2, slackVariable, n);
        this.addConstraint(row);
    }
    
    public void addLowerThan(final SolverVariable solverVariable, final SolverVariable solverVariable2, final int n, final int n2) {
        final ArrayRow row = this.createRow();
        final SolverVariable slackVariable = this.createSlackVariable();
        slackVariable.strength = 0;
        row.createRowLowerThan(solverVariable, solverVariable2, slackVariable, n);
        if (n2 != 8) {
            this.addSingleError(row, (int)(row.variables.get(slackVariable) * -1.0f), n2);
        }
        this.addConstraint(row);
    }
    
    public void addRatio(final SolverVariable solverVariable, final SolverVariable solverVariable2, final SolverVariable solverVariable3, final SolverVariable solverVariable4, final float n, final int n2) {
        final ArrayRow row = this.createRow();
        row.createRowDimensionRatio(solverVariable, solverVariable2, solverVariable3, solverVariable4, n);
        if (n2 != 8) {
            row.addError(this, n2);
        }
        this.addConstraint(row);
    }
    
    void addSingleError(final ArrayRow arrayRow, final int n, final int n2) {
        arrayRow.addSingleError(this.createErrorVariable(n2, null), n);
    }
    
    public void addSynonym(SolverVariable solverVariable, final SolverVariable solverVariable2, final int n) {
        if (solverVariable.definitionId == -1 && n == 0) {
            SolverVariable solverVariable3 = solverVariable2;
            if (solverVariable2.isSynonym) {
                final float synonymDelta = solverVariable2.synonymDelta;
                solverVariable3 = this.mCache.mIndexedVariables[solverVariable2.synonym];
            }
            if (solverVariable.isSynonym) {
                final float synonymDelta2 = solverVariable.synonymDelta;
                solverVariable = this.mCache.mIndexedVariables[solverVariable.synonym];
            }
            else {
                solverVariable.setSynonym(this, solverVariable3, 0.0f);
            }
        }
        else {
            this.addEquality(solverVariable, solverVariable2, n, 8);
        }
    }
    
    final void cleanupRows() {
        int n;
        for (int i = 0; i < this.mNumRows; i = n + 1) {
            final ArrayRow arrayRow = this.mRows[i];
            if (arrayRow.variables.getCurrentSize() == 0) {
                arrayRow.isSimpleDefinition = true;
            }
            n = i;
            if (arrayRow.isSimpleDefinition) {
                arrayRow.variable.computedValue = arrayRow.constantValue;
                arrayRow.variable.removeFromRow(arrayRow);
                int n2 = i;
                int mNumRows;
                while (true) {
                    mNumRows = this.mNumRows;
                    if (n2 >= mNumRows - 1) {
                        break;
                    }
                    final ArrayRow[] mRows = this.mRows;
                    final int n3 = n2 + 1;
                    mRows[n2] = mRows[n3];
                    n2 = n3;
                }
                this.mRows[mNumRows - 1] = null;
                this.mNumRows = mNumRows - 1;
                n = i - 1;
                if (LinearSystem.OPTIMIZED_ENGINE) {
                    this.mCache.optimizedArrayRowPool.release(arrayRow);
                }
                else {
                    this.mCache.arrayRowPool.release(arrayRow);
                }
            }
        }
    }
    
    public SolverVariable createErrorVariable(final int strength, final String s) {
        final Metrics sMetrics = LinearSystem.sMetrics;
        if (sMetrics != null) {
            ++sMetrics.errors;
        }
        if (this.mNumColumns + 1 >= this.mMaxColumns) {
            this.increaseTableSize();
        }
        final SolverVariable acquireSolverVariable = this.acquireSolverVariable(SolverVariable.Type.ERROR, s);
        final int n = this.mVariablesID + 1;
        this.mVariablesID = n;
        ++this.mNumColumns;
        acquireSolverVariable.id = n;
        acquireSolverVariable.strength = strength;
        this.mCache.mIndexedVariables[this.mVariablesID] = acquireSolverVariable;
        this.mGoal.addError(acquireSolverVariable);
        return acquireSolverVariable;
    }
    
    public SolverVariable createExtraVariable() {
        final Metrics sMetrics = LinearSystem.sMetrics;
        if (sMetrics != null) {
            ++sMetrics.extravariables;
        }
        if (this.mNumColumns + 1 >= this.mMaxColumns) {
            this.increaseTableSize();
        }
        final SolverVariable acquireSolverVariable = this.acquireSolverVariable(SolverVariable.Type.SLACK, null);
        final int n = this.mVariablesID + 1;
        this.mVariablesID = n;
        ++this.mNumColumns;
        acquireSolverVariable.id = n;
        return this.mCache.mIndexedVariables[this.mVariablesID] = acquireSolverVariable;
    }
    
    public SolverVariable createObjectVariable(final Object o) {
        SolverVariable solverVariable = null;
        if (o == null) {
            return null;
        }
        if (this.mNumColumns + 1 >= this.mMaxColumns) {
            this.increaseTableSize();
        }
        if (o instanceof ConstraintAnchor) {
            final ConstraintAnchor constraintAnchor = (ConstraintAnchor)o;
            SolverVariable solverVariable2;
            if ((solverVariable2 = constraintAnchor.getSolverVariable()) == null) {
                constraintAnchor.resetSolverVariable(this.mCache);
                solverVariable2 = constraintAnchor.getSolverVariable();
            }
            if (solverVariable2.id != -1 && solverVariable2.id <= this.mVariablesID) {
                solverVariable = solverVariable2;
                if (this.mCache.mIndexedVariables[solverVariable2.id] != null) {
                    return solverVariable;
                }
            }
            if (solverVariable2.id != -1) {
                solverVariable2.reset();
            }
            final int n = this.mVariablesID + 1;
            this.mVariablesID = n;
            ++this.mNumColumns;
            solverVariable2.id = n;
            solverVariable2.mType = SolverVariable.Type.UNRESTRICTED;
            this.mCache.mIndexedVariables[this.mVariablesID] = solverVariable2;
            solverVariable = solverVariable2;
        }
        return solverVariable;
    }
    
    public ArrayRow createRow() {
        ArrayRow arrayRow;
        if (LinearSystem.OPTIMIZED_ENGINE) {
            arrayRow = this.mCache.optimizedArrayRowPool.acquire();
            if (arrayRow == null) {
                arrayRow = new ValuesRow(this.mCache);
                ++LinearSystem.OPTIMIZED_ARRAY_ROW_CREATION;
            }
            else {
                arrayRow.reset();
            }
        }
        else {
            arrayRow = this.mCache.arrayRowPool.acquire();
            if (arrayRow == null) {
                arrayRow = new ArrayRow(this.mCache);
                ++LinearSystem.ARRAY_ROW_CREATION;
            }
            else {
                arrayRow.reset();
            }
        }
        SolverVariable.increaseErrorId();
        return arrayRow;
    }
    
    public SolverVariable createSlackVariable() {
        final Metrics sMetrics = LinearSystem.sMetrics;
        if (sMetrics != null) {
            ++sMetrics.slackvariables;
        }
        if (this.mNumColumns + 1 >= this.mMaxColumns) {
            this.increaseTableSize();
        }
        final SolverVariable acquireSolverVariable = this.acquireSolverVariable(SolverVariable.Type.SLACK, null);
        final int n = this.mVariablesID + 1;
        this.mVariablesID = n;
        ++this.mNumColumns;
        acquireSolverVariable.id = n;
        return this.mCache.mIndexedVariables[this.mVariablesID] = acquireSolverVariable;
    }
    
    public void displayReadableRows() {
        this.displaySolverVariables();
        final StringBuilder sb = new StringBuilder();
        sb.append(" num vars ");
        sb.append(this.mVariablesID);
        sb.append("\n");
        String string = sb.toString();
        final int n = 0;
        String string2;
        for (int i = 0; i < this.mVariablesID + 1; ++i, string = string2) {
            final SolverVariable obj = this.mCache.mIndexedVariables[i];
            string2 = string;
            if (obj != null) {
                string2 = string;
                if (obj.isFinalValue) {
                    final StringBuilder sb2 = new StringBuilder();
                    sb2.append(string);
                    sb2.append(" $[");
                    sb2.append(i);
                    sb2.append("] => ");
                    sb2.append(obj);
                    sb2.append(" = ");
                    sb2.append(obj.computedValue);
                    sb2.append("\n");
                    string2 = sb2.toString();
                }
            }
        }
        final StringBuilder sb3 = new StringBuilder();
        sb3.append(string);
        sb3.append("\n");
        String string3 = sb3.toString();
        String string4;
        for (int j = 0; j < this.mVariablesID + 1; ++j, string3 = string4) {
            final SolverVariable obj2 = this.mCache.mIndexedVariables[j];
            string4 = string3;
            if (obj2 != null) {
                string4 = string3;
                if (obj2.isSynonym) {
                    final SolverVariable obj3 = this.mCache.mIndexedVariables[obj2.synonym];
                    final StringBuilder sb4 = new StringBuilder();
                    sb4.append(string3);
                    sb4.append(" ~[");
                    sb4.append(j);
                    sb4.append("] => ");
                    sb4.append(obj2);
                    sb4.append(" = ");
                    sb4.append(obj3);
                    sb4.append(" + ");
                    sb4.append(obj2.synonymDelta);
                    sb4.append("\n");
                    string4 = sb4.toString();
                }
            }
        }
        final StringBuilder sb5 = new StringBuilder();
        sb5.append(string3);
        sb5.append("\n\n #  ");
        String s = sb5.toString();
        for (int k = n; k < this.mNumRows; ++k) {
            final StringBuilder sb6 = new StringBuilder();
            sb6.append(s);
            sb6.append(this.mRows[k].toReadableString());
            final String string5 = sb6.toString();
            final StringBuilder sb7 = new StringBuilder();
            sb7.append(string5);
            sb7.append("\n #  ");
            s = sb7.toString();
        }
        String string6 = s;
        if (this.mGoal != null) {
            final StringBuilder sb8 = new StringBuilder();
            sb8.append(s);
            sb8.append("Goal: ");
            sb8.append(this.mGoal);
            sb8.append("\n");
            string6 = sb8.toString();
        }
        System.out.println(string6);
    }
    
    void displaySystemInformation() {
        int i = 0;
        int n = 0;
        while (i < this.TABLE_SIZE) {
            final ArrayRow arrayRow = this.mRows[i];
            int n2 = n;
            if (arrayRow != null) {
                n2 = n + arrayRow.sizeInBytes();
            }
            ++i;
            n = n2;
        }
        int j = 0;
        int n3 = 0;
        while (j < this.mNumRows) {
            final ArrayRow arrayRow2 = this.mRows[j];
            int n4 = n3;
            if (arrayRow2 != null) {
                n4 = n3 + arrayRow2.sizeInBytes();
            }
            ++j;
            n3 = n4;
        }
        final PrintStream out = System.out;
        final StringBuilder sb = new StringBuilder();
        sb.append("Linear System -> Table size: ");
        sb.append(this.TABLE_SIZE);
        sb.append(" (");
        final int table_SIZE = this.TABLE_SIZE;
        sb.append(this.getDisplaySize(table_SIZE * table_SIZE));
        sb.append(") -- row sizes: ");
        sb.append(this.getDisplaySize(n));
        sb.append(", actual size: ");
        sb.append(this.getDisplaySize(n3));
        sb.append(" rows: ");
        sb.append(this.mNumRows);
        sb.append("/");
        sb.append(this.mMaxRows);
        sb.append(" cols: ");
        sb.append(this.mNumColumns);
        sb.append("/");
        sb.append(this.mMaxColumns);
        sb.append(" ");
        sb.append(0);
        sb.append(" occupied cells, ");
        sb.append(this.getDisplaySize(0));
        out.println(sb.toString());
    }
    
    public void displayVariablesReadableRows() {
        this.displaySolverVariables();
        String s = "";
        String string;
        for (int i = 0; i < this.mNumRows; ++i, s = string) {
            string = s;
            if (this.mRows[i].variable.mType == SolverVariable.Type.UNRESTRICTED) {
                final StringBuilder sb = new StringBuilder();
                sb.append(s);
                sb.append(this.mRows[i].toReadableString());
                final String string2 = sb.toString();
                final StringBuilder sb2 = new StringBuilder();
                sb2.append(string2);
                sb2.append("\n");
                string = sb2.toString();
            }
        }
        final StringBuilder sb3 = new StringBuilder();
        sb3.append(s);
        sb3.append(this.mGoal);
        sb3.append("\n");
        System.out.println(sb3.toString());
    }
    
    public void fillMetrics(final Metrics sMetrics) {
        LinearSystem.sMetrics = sMetrics;
    }
    
    public Cache getCache() {
        return this.mCache;
    }
    
    Row getGoal() {
        return this.mGoal;
    }
    
    public int getMemoryUsed() {
        int i = 0;
        int n = 0;
        while (i < this.mNumRows) {
            final ArrayRow arrayRow = this.mRows[i];
            int n2 = n;
            if (arrayRow != null) {
                n2 = n + arrayRow.sizeInBytes();
            }
            ++i;
            n = n2;
        }
        return n;
    }
    
    public int getNumEquations() {
        return this.mNumRows;
    }
    
    public int getNumVariables() {
        return this.mVariablesID;
    }
    
    public int getObjectVariableValue(final Object o) {
        final SolverVariable solverVariable = ((ConstraintAnchor)o).getSolverVariable();
        if (solverVariable != null) {
            return (int)(solverVariable.computedValue + 0.5f);
        }
        return 0;
    }
    
    ArrayRow getRow(final int n) {
        return this.mRows[n];
    }
    
    float getValueFor(final String s) {
        final SolverVariable variable = this.getVariable(s, SolverVariable.Type.UNRESTRICTED);
        if (variable == null) {
            return 0.0f;
        }
        return variable.computedValue;
    }
    
    SolverVariable getVariable(final String key, final SolverVariable.Type type) {
        if (this.mVariables == null) {
            this.mVariables = new HashMap<String, SolverVariable>();
        }
        SolverVariable variable;
        if ((variable = this.mVariables.get(key)) == null) {
            variable = this.createVariable(key, type);
        }
        return variable;
    }
    
    public void minimize() throws Exception {
        final Metrics sMetrics = LinearSystem.sMetrics;
        if (sMetrics != null) {
            ++sMetrics.minimize;
        }
        if (this.mGoal.isEmpty()) {
            this.computeValues();
            return;
        }
        if (this.graphOptimizer || this.newgraphOptimizer) {
            final Metrics sMetrics2 = LinearSystem.sMetrics;
            if (sMetrics2 != null) {
                ++sMetrics2.graphOptimizer;
            }
            final int n = 0;
            int i = 0;
            while (true) {
                while (i < this.mNumRows) {
                    if (!this.mRows[i].isSimpleDefinition) {
                        final int n2 = n;
                        if (n2 == 0) {
                            this.minimizeGoal(this.mGoal);
                            return;
                        }
                        final Metrics sMetrics3 = LinearSystem.sMetrics;
                        if (sMetrics3 != null) {
                            ++sMetrics3.fullySolved;
                        }
                        this.computeValues();
                        return;
                    }
                    else {
                        ++i;
                    }
                }
                final int n2 = 1;
                continue;
            }
        }
        this.minimizeGoal(this.mGoal);
    }
    
    void minimizeGoal(final Row row) throws Exception {
        final Metrics sMetrics = LinearSystem.sMetrics;
        if (sMetrics != null) {
            ++sMetrics.minimizeGoal;
            final Metrics sMetrics2 = LinearSystem.sMetrics;
            sMetrics2.maxVariables = Math.max(sMetrics2.maxVariables, this.mNumColumns);
            final Metrics sMetrics3 = LinearSystem.sMetrics;
            sMetrics3.maxRows = Math.max(sMetrics3.maxRows, this.mNumRows);
        }
        this.enforceBFS(row);
        this.optimize(row, false);
        this.computeValues();
    }
    
    public void removeRow(final ArrayRow arrayRow) {
        if (arrayRow.isSimpleDefinition && arrayRow.variable != null) {
            if (arrayRow.variable.definitionId != -1) {
                int definitionId = arrayRow.variable.definitionId;
                int mNumRows;
                while (true) {
                    mNumRows = this.mNumRows;
                    if (definitionId >= mNumRows - 1) {
                        break;
                    }
                    final ArrayRow[] mRows = this.mRows;
                    final int n = definitionId + 1;
                    final SolverVariable variable = mRows[n].variable;
                    if (variable.definitionId == n) {
                        variable.definitionId = definitionId;
                    }
                    final ArrayRow[] mRows2 = this.mRows;
                    mRows2[definitionId] = mRows2[n];
                    definitionId = n;
                }
                this.mNumRows = mNumRows - 1;
            }
            if (!arrayRow.variable.isFinalValue) {
                arrayRow.variable.setFinalValue(this, arrayRow.constantValue);
            }
            if (LinearSystem.OPTIMIZED_ENGINE) {
                this.mCache.optimizedArrayRowPool.release(arrayRow);
            }
            else {
                this.mCache.arrayRowPool.release(arrayRow);
            }
        }
    }
    
    public void reset() {
        for (int i = 0; i < this.mCache.mIndexedVariables.length; ++i) {
            final SolverVariable solverVariable = this.mCache.mIndexedVariables[i];
            if (solverVariable != null) {
                solverVariable.reset();
            }
        }
        this.mCache.solverVariablePool.releaseAll(this.mPoolVariables, this.mPoolVariablesCount);
        this.mPoolVariablesCount = 0;
        Arrays.fill(this.mCache.mIndexedVariables, null);
        final HashMap<String, SolverVariable> mVariables = this.mVariables;
        if (mVariables != null) {
            mVariables.clear();
        }
        this.mVariablesID = 0;
        this.mGoal.clear();
        this.mNumColumns = 1;
        for (int j = 0; j < this.mNumRows; ++j) {
            final ArrayRow arrayRow = this.mRows[j];
            if (arrayRow != null) {
                arrayRow.used = false;
            }
        }
        this.releaseRows();
        this.mNumRows = 0;
        if (LinearSystem.OPTIMIZED_ENGINE) {
            this.mTempGoal = (Row)new ValuesRow(this.mCache);
        }
        else {
            this.mTempGoal = (Row)new ArrayRow(this.mCache);
        }
    }
    
    interface Row
    {
        void addError(final SolverVariable p0);
        
        void clear();
        
        SolverVariable getKey();
        
        SolverVariable getPivotCandidate(final LinearSystem p0, final boolean[] p1);
        
        void initFromRow(final Row p0);
        
        boolean isEmpty();
        
        void updateFromFinalVariable(final LinearSystem p0, final SolverVariable p1, final boolean p2);
        
        void updateFromRow(final LinearSystem p0, final ArrayRow p1, final boolean p2);
        
        void updateFromSystem(final LinearSystem p0);
    }
    
    class ValuesRow extends ArrayRow
    {
        final LinearSystem this$0;
        
        public ValuesRow(final LinearSystem this$0, final Cache cache) {
            this.this$0 = this$0;
            this.variables = (ArrayRowVariables)new SolverVariableValues(this, cache);
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.core.state;

import androidx.constraintlayout.core.widgets.ConstraintWidget;
import java.util.Collection;
import java.util.Collections;
import java.util.ArrayList;
import androidx.constraintlayout.core.widgets.HelperWidget;
import androidx.constraintlayout.core.state.helpers.Facade;

public class HelperReference extends ConstraintReference implements Facade
{
    private HelperWidget mHelperWidget;
    protected ArrayList<Object> mReferences;
    protected final State mState;
    final State.Helper mType;
    
    public HelperReference(final State mState, final State.Helper mType) {
        super(mState);
        this.mReferences = new ArrayList<Object>();
        this.mState = mState;
        this.mType = mType;
    }
    
    public HelperReference add(final Object... elements) {
        Collections.addAll(this.mReferences, elements);
        return this;
    }
    
    @Override
    public void apply() {
    }
    
    @Override
    public ConstraintWidget getConstraintWidget() {
        return this.getHelperWidget();
    }
    
    public HelperWidget getHelperWidget() {
        return this.mHelperWidget;
    }
    
    public State.Helper getType() {
        return this.mType;
    }
    
    public void setHelperWidget(final HelperWidget mHelperWidget) {
        this.mHelperWidget = mHelperWidget;
    }
}

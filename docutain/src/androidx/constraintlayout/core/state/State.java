// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.core.state;

import androidx.constraintlayout.core.state.helpers.HorizontalChainReference;
import androidx.constraintlayout.core.state.helpers.VerticalChainReference;
import androidx.constraintlayout.core.state.helpers.AlignVerticallyReference;
import androidx.constraintlayout.core.state.helpers.AlignHorizontallyReference;
import androidx.constraintlayout.core.state.helpers.Facade;
import androidx.constraintlayout.core.state.helpers.BarrierReference;
import java.io.PrintStream;
import androidx.constraintlayout.core.widgets.HelperWidget;
import java.util.Iterator;
import androidx.constraintlayout.core.state.helpers.GuidelineReference;
import androidx.constraintlayout.core.widgets.ConstraintWidget;
import androidx.constraintlayout.core.widgets.ConstraintWidgetContainer;
import java.util.ArrayList;
import java.util.HashMap;

public class State
{
    static final int CONSTRAINT_RATIO = 2;
    static final int CONSTRAINT_SPREAD = 0;
    static final int CONSTRAINT_WRAP = 1;
    public static final Integer PARENT;
    static final int UNKNOWN = -1;
    protected HashMap<Object, HelperReference> mHelperReferences;
    public final ConstraintReference mParent;
    protected HashMap<Object, Reference> mReferences;
    HashMap<String, ArrayList<String>> mTags;
    private int numHelpers;
    
    static {
        PARENT = 0;
    }
    
    public State() {
        this.mReferences = new HashMap<Object, Reference>();
        this.mHelperReferences = new HashMap<Object, HelperReference>();
        this.mTags = new HashMap<String, ArrayList<String>>();
        final ConstraintReference constraintReference = new ConstraintReference(this);
        this.mParent = constraintReference;
        this.numHelpers = 0;
        this.mReferences.put(State.PARENT, constraintReference);
    }
    
    private String createHelperKey() {
        final StringBuilder sb = new StringBuilder();
        sb.append("__HELPER_KEY_");
        sb.append(this.numHelpers++);
        sb.append("__");
        return sb.toString();
    }
    
    public void apply(final ConstraintWidgetContainer constraintWidget) {
        constraintWidget.removeAllChildren();
        this.mParent.getWidth().apply(this, constraintWidget, 0);
        this.mParent.getHeight().apply(this, constraintWidget, 1);
        for (final Object next : this.mHelperReferences.keySet()) {
            final HelperWidget helperWidget = this.mHelperReferences.get(next).getHelperWidget();
            if (helperWidget != null) {
                Reference constraints;
                if ((constraints = this.mReferences.get(next)) == null) {
                    constraints = this.constraints(next);
                }
                constraints.setConstraintWidget(helperWidget);
            }
        }
        for (final Object next2 : this.mReferences.keySet()) {
            final Reference reference = this.mReferences.get(next2);
            if (reference != this.mParent && reference.getFacade() instanceof HelperReference) {
                final HelperWidget helperWidget2 = ((HelperReference)reference.getFacade()).getHelperWidget();
                if (helperWidget2 == null) {
                    continue;
                }
                Reference constraints2;
                if ((constraints2 = this.mReferences.get(next2)) == null) {
                    constraints2 = this.constraints(next2);
                }
                constraints2.setConstraintWidget(helperWidget2);
            }
        }
        final Iterator<Object> iterator3 = this.mReferences.keySet().iterator();
        while (iterator3.hasNext()) {
            final Reference reference2 = this.mReferences.get(iterator3.next());
            if (reference2 != this.mParent) {
                final ConstraintWidget constraintWidget2 = reference2.getConstraintWidget();
                constraintWidget2.setDebugName(reference2.getKey().toString());
                constraintWidget2.setParent(null);
                if (reference2.getFacade() instanceof GuidelineReference) {
                    reference2.apply();
                }
                constraintWidget.add(constraintWidget2);
            }
            else {
                reference2.setConstraintWidget(constraintWidget);
            }
        }
        final Iterator<Object> iterator4 = this.mHelperReferences.keySet().iterator();
        while (iterator4.hasNext()) {
            final HelperReference helperReference = this.mHelperReferences.get(iterator4.next());
            if (helperReference.getHelperWidget() != null) {
                final Iterator<Object> iterator5 = helperReference.mReferences.iterator();
                while (iterator5.hasNext()) {
                    helperReference.getHelperWidget().add(this.mReferences.get(iterator5.next()).getConstraintWidget());
                }
                helperReference.apply();
            }
            else {
                helperReference.apply();
            }
        }
        final Iterator<Object> iterator6 = this.mReferences.keySet().iterator();
        while (iterator6.hasNext()) {
            final Reference reference3 = this.mReferences.get(iterator6.next());
            if (reference3 != this.mParent && reference3.getFacade() instanceof HelperReference) {
                final HelperReference helperReference2 = (HelperReference)reference3.getFacade();
                final HelperWidget helperWidget3 = helperReference2.getHelperWidget();
                if (helperWidget3 == null) {
                    continue;
                }
                for (final Reference next3 : helperReference2.mReferences) {
                    final Reference reference4 = this.mReferences.get(next3);
                    if (reference4 != null) {
                        helperWidget3.add(reference4.getConstraintWidget());
                    }
                    else if (next3 instanceof Reference) {
                        helperWidget3.add(next3.getConstraintWidget());
                    }
                    else {
                        final PrintStream out = System.out;
                        final StringBuilder sb = new StringBuilder();
                        sb.append("couldn't find reference for ");
                        sb.append(next3);
                        out.println(sb.toString());
                    }
                }
                reference3.apply();
            }
        }
        for (final Object next4 : this.mReferences.keySet()) {
            final Reference reference5 = this.mReferences.get(next4);
            reference5.apply();
            final ConstraintWidget constraintWidget3 = reference5.getConstraintWidget();
            if (constraintWidget3 != null && next4 != null) {
                constraintWidget3.stringId = next4.toString();
            }
        }
    }
    
    public BarrierReference barrier(final Object o, final Direction barrierDirection) {
        final ConstraintReference constraints = this.constraints(o);
        if (constraints.getFacade() == null || !(constraints.getFacade() instanceof BarrierReference)) {
            final BarrierReference facade = new BarrierReference(this);
            facade.setBarrierDirection(barrierDirection);
            constraints.setFacade(facade);
        }
        return (BarrierReference)constraints.getFacade();
    }
    
    public AlignHorizontallyReference centerHorizontally(final Object... array) {
        final AlignHorizontallyReference alignHorizontallyReference = (AlignHorizontallyReference)this.helper(null, Helper.ALIGN_HORIZONTALLY);
        alignHorizontallyReference.add(array);
        return alignHorizontallyReference;
    }
    
    public AlignVerticallyReference centerVertically(final Object... array) {
        final AlignVerticallyReference alignVerticallyReference = (AlignVerticallyReference)this.helper(null, Helper.ALIGN_VERTICALLY);
        alignVerticallyReference.add(array);
        return alignVerticallyReference;
    }
    
    public ConstraintReference constraints(final Object key) {
        Reference constraintReference;
        if ((constraintReference = this.mReferences.get(key)) == null) {
            constraintReference = this.createConstraintReference(key);
            this.mReferences.put(key, constraintReference);
            constraintReference.setKey(key);
        }
        if (constraintReference instanceof ConstraintReference) {
            return (ConstraintReference)constraintReference;
        }
        return null;
    }
    
    public int convertDimension(final Object o) {
        if (o instanceof Float) {
            return ((Float)o).intValue();
        }
        if (o instanceof Integer) {
            return (int)o;
        }
        return 0;
    }
    
    public ConstraintReference createConstraintReference(final Object o) {
        return new ConstraintReference(this);
    }
    
    public void directMapping() {
        for (final Object next : this.mReferences.keySet()) {
            final ConstraintReference constraints = this.constraints(next);
            if (!(constraints instanceof ConstraintReference)) {
                continue;
            }
            final ConstraintReference constraintReference = constraints;
            constraints.setView(next);
        }
    }
    
    public ArrayList<String> getIdsForTag(final String s) {
        if (this.mTags.containsKey(s)) {
            return this.mTags.get(s);
        }
        return null;
    }
    
    public GuidelineReference guideline(final Object key, final int orientation) {
        final ConstraintReference constraints = this.constraints(key);
        if (constraints.getFacade() == null || !(constraints.getFacade() instanceof GuidelineReference)) {
            final GuidelineReference facade = new GuidelineReference(this);
            facade.setOrientation(orientation);
            facade.setKey(key);
            constraints.setFacade(facade);
        }
        return (GuidelineReference)constraints.getFacade();
    }
    
    public State height(final Dimension height) {
        return this.setHeight(height);
    }
    
    public HelperReference helper(Object value, final Helper helper) {
        Object helperKey = value;
        if (value == null) {
            helperKey = this.createHelperKey();
        }
        if ((value = this.mHelperReferences.get(helperKey)) == null) {
            final int n = State$1.$SwitchMap$androidx$constraintlayout$core$state$State$Helper[helper.ordinal()];
            if (n != 1) {
                if (n != 2) {
                    if (n != 3) {
                        if (n != 4) {
                            if (n != 5) {
                                value = new HelperReference(this, helper);
                            }
                            else {
                                value = new BarrierReference(this);
                            }
                        }
                        else {
                            value = new AlignVerticallyReference(this);
                        }
                    }
                    else {
                        value = new AlignHorizontallyReference(this);
                    }
                }
                else {
                    value = new VerticalChainReference(this);
                }
            }
            else {
                value = new HorizontalChainReference(this);
            }
            ((ConstraintReference)value).setKey(helperKey);
            this.mHelperReferences.put(helperKey, (HelperReference)value);
        }
        return (HelperReference)value;
    }
    
    public HorizontalChainReference horizontalChain() {
        return (HorizontalChainReference)this.helper(null, Helper.HORIZONTAL_CHAIN);
    }
    
    public HorizontalChainReference horizontalChain(final Object... array) {
        final HorizontalChainReference horizontalChainReference = (HorizontalChainReference)this.helper(null, Helper.HORIZONTAL_CHAIN);
        horizontalChainReference.add(array);
        return horizontalChainReference;
    }
    
    public GuidelineReference horizontalGuideline(final Object o) {
        return this.guideline(o, 0);
    }
    
    public void map(final Object o, final Object view) {
        final ConstraintReference constraints = this.constraints(o);
        if (constraints instanceof ConstraintReference) {
            final ConstraintReference constraintReference = constraints;
            constraints.setView(view);
        }
    }
    
    Reference reference(final Object key) {
        return this.mReferences.get(key);
    }
    
    public void reset() {
        this.mHelperReferences.clear();
        this.mTags.clear();
    }
    
    public boolean sameFixedHeight(final int n) {
        return this.mParent.getHeight().equalsFixedValue(n);
    }
    
    public boolean sameFixedWidth(final int n) {
        return this.mParent.getWidth().equalsFixedValue(n);
    }
    
    public State setHeight(final Dimension height) {
        this.mParent.setHeight(height);
        return this;
    }
    
    public void setTag(final String e, final String s) {
        final ConstraintReference constraints = this.constraints(e);
        if (constraints instanceof ConstraintReference) {
            final ConstraintReference constraintReference = constraints;
            constraints.setTag(s);
            ArrayList list;
            if (!this.mTags.containsKey(s)) {
                final ArrayList value = new ArrayList();
                this.mTags.put(s, value);
                list = value;
            }
            else {
                list = this.mTags.get(s);
            }
            list.add(e);
        }
    }
    
    public State setWidth(final Dimension width) {
        this.mParent.setWidth(width);
        return this;
    }
    
    public VerticalChainReference verticalChain() {
        return (VerticalChainReference)this.helper(null, Helper.VERTICAL_CHAIN);
    }
    
    public VerticalChainReference verticalChain(final Object... array) {
        final VerticalChainReference verticalChainReference = (VerticalChainReference)this.helper(null, Helper.VERTICAL_CHAIN);
        verticalChainReference.add(array);
        return verticalChainReference;
    }
    
    public GuidelineReference verticalGuideline(final Object o) {
        return this.guideline(o, 1);
    }
    
    public State width(final Dimension width) {
        return this.setWidth(width);
    }
    
    public enum Chain
    {
        private static final Chain[] $VALUES;
        
        PACKED, 
        SPREAD, 
        SPREAD_INSIDE;
    }
    
    public enum Constraint
    {
        private static final Constraint[] $VALUES;
        
        BASELINE_TO_BASELINE, 
        BASELINE_TO_BOTTOM, 
        BASELINE_TO_TOP, 
        BOTTOM_TO_BOTTOM, 
        BOTTOM_TO_TOP, 
        CENTER_HORIZONTALLY, 
        CENTER_VERTICALLY, 
        CIRCULAR_CONSTRAINT, 
        END_TO_END, 
        END_TO_START, 
        LEFT_TO_LEFT, 
        LEFT_TO_RIGHT, 
        RIGHT_TO_LEFT, 
        RIGHT_TO_RIGHT, 
        START_TO_END, 
        START_TO_START, 
        TOP_TO_BOTTOM, 
        TOP_TO_TOP;
    }
    
    public enum Direction
    {
        private static final Direction[] $VALUES;
        
        BOTTOM, 
        END, 
        LEFT, 
        RIGHT, 
        START, 
        TOP;
    }
    
    public enum Helper
    {
        private static final Helper[] $VALUES;
        
        ALIGN_HORIZONTALLY, 
        ALIGN_VERTICALLY, 
        BARRIER, 
        FLOW, 
        HORIZONTAL_CHAIN, 
        LAYER, 
        VERTICAL_CHAIN;
    }
}

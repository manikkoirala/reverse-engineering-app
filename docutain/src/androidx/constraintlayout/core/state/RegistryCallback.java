// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.core.state;

public interface RegistryCallback
{
    String currentLayoutInformation();
    
    String currentMotionScene();
    
    long getLastModified();
    
    void onDimensions(final int p0, final int p1);
    
    void onNewMotionScene(final String p0);
    
    void onProgress(final float p0);
    
    void setDrawDebug(final int p0);
    
    void setLayoutInformationMode(final int p0);
}

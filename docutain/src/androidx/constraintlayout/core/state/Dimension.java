// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.core.state;

import androidx.constraintlayout.core.widgets.ConstraintWidget;

public class Dimension
{
    public static final Object FIXED_DIMENSION;
    public static final Object PARENT_DIMENSION;
    public static final Object PERCENT_DIMENSION;
    public static final Object RATIO_DIMENSION;
    public static final Object SPREAD_DIMENSION;
    public static final Object WRAP_DIMENSION;
    private final int WRAP_CONTENT;
    Object mInitialValue;
    boolean mIsSuggested;
    int mMax;
    int mMin;
    float mPercent;
    String mRatioString;
    int mValue;
    
    static {
        FIXED_DIMENSION = new Object();
        WRAP_DIMENSION = new Object();
        SPREAD_DIMENSION = new Object();
        PARENT_DIMENSION = new Object();
        PERCENT_DIMENSION = new Object();
        RATIO_DIMENSION = new Object();
    }
    
    private Dimension() {
        this.WRAP_CONTENT = -2;
        this.mMin = 0;
        this.mMax = Integer.MAX_VALUE;
        this.mPercent = 1.0f;
        this.mValue = 0;
        this.mRatioString = null;
        this.mInitialValue = Dimension.WRAP_DIMENSION;
        this.mIsSuggested = false;
    }
    
    private Dimension(final Object mInitialValue) {
        this.WRAP_CONTENT = -2;
        this.mMin = 0;
        this.mMax = Integer.MAX_VALUE;
        this.mPercent = 1.0f;
        this.mValue = 0;
        this.mRatioString = null;
        this.mIsSuggested = false;
        this.mInitialValue = mInitialValue;
    }
    
    public static Dimension Fixed(final int n) {
        final Dimension dimension = new Dimension(Dimension.FIXED_DIMENSION);
        dimension.fixed(n);
        return dimension;
    }
    
    public static Dimension Fixed(final Object o) {
        final Dimension dimension = new Dimension(Dimension.FIXED_DIMENSION);
        dimension.fixed(o);
        return dimension;
    }
    
    public static Dimension Parent() {
        return new Dimension(Dimension.PARENT_DIMENSION);
    }
    
    public static Dimension Percent(final Object o, final float n) {
        final Dimension dimension = new Dimension(Dimension.PERCENT_DIMENSION);
        dimension.percent(o, n);
        return dimension;
    }
    
    public static Dimension Ratio(final String s) {
        final Dimension dimension = new Dimension(Dimension.RATIO_DIMENSION);
        dimension.ratio(s);
        return dimension;
    }
    
    public static Dimension Spread() {
        return new Dimension(Dimension.SPREAD_DIMENSION);
    }
    
    public static Dimension Suggested(final int n) {
        final Dimension dimension = new Dimension();
        dimension.suggested(n);
        return dimension;
    }
    
    public static Dimension Suggested(final Object o) {
        final Dimension dimension = new Dimension();
        dimension.suggested(o);
        return dimension;
    }
    
    public static Dimension Wrap() {
        return new Dimension(Dimension.WRAP_DIMENSION);
    }
    
    public void apply(final State state, final ConstraintWidget constraintWidget, int n) {
        final String mRatioString = this.mRatioString;
        if (mRatioString != null) {
            constraintWidget.setDimensionRatio(mRatioString);
        }
        final int n2 = 2;
        if (n == 0) {
            if (this.mIsSuggested) {
                constraintWidget.setHorizontalDimensionBehaviour(ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT);
                final Object mInitialValue = this.mInitialValue;
                if (mInitialValue == Dimension.WRAP_DIMENSION) {
                    n = 1;
                }
                else if (mInitialValue == Dimension.PERCENT_DIMENSION) {
                    n = n2;
                }
                else {
                    n = 0;
                }
                constraintWidget.setHorizontalMatchStyle(n, this.mMin, this.mMax, this.mPercent);
            }
            else {
                n = this.mMin;
                if (n > 0) {
                    constraintWidget.setMinWidth(n);
                }
                n = this.mMax;
                if (n < Integer.MAX_VALUE) {
                    constraintWidget.setMaxWidth(n);
                }
                final Object mInitialValue2 = this.mInitialValue;
                if (mInitialValue2 == Dimension.WRAP_DIMENSION) {
                    constraintWidget.setHorizontalDimensionBehaviour(ConstraintWidget.DimensionBehaviour.WRAP_CONTENT);
                }
                else if (mInitialValue2 == Dimension.PARENT_DIMENSION) {
                    constraintWidget.setHorizontalDimensionBehaviour(ConstraintWidget.DimensionBehaviour.MATCH_PARENT);
                }
                else if (mInitialValue2 == null) {
                    constraintWidget.setHorizontalDimensionBehaviour(ConstraintWidget.DimensionBehaviour.FIXED);
                    constraintWidget.setWidth(this.mValue);
                }
            }
        }
        else if (this.mIsSuggested) {
            constraintWidget.setVerticalDimensionBehaviour(ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT);
            final Object mInitialValue3 = this.mInitialValue;
            if (mInitialValue3 == Dimension.WRAP_DIMENSION) {
                n = 1;
            }
            else if (mInitialValue3 == Dimension.PERCENT_DIMENSION) {
                n = n2;
            }
            else {
                n = 0;
            }
            constraintWidget.setVerticalMatchStyle(n, this.mMin, this.mMax, this.mPercent);
        }
        else {
            n = this.mMin;
            if (n > 0) {
                constraintWidget.setMinHeight(n);
            }
            n = this.mMax;
            if (n < Integer.MAX_VALUE) {
                constraintWidget.setMaxHeight(n);
            }
            final Object mInitialValue4 = this.mInitialValue;
            if (mInitialValue4 == Dimension.WRAP_DIMENSION) {
                constraintWidget.setVerticalDimensionBehaviour(ConstraintWidget.DimensionBehaviour.WRAP_CONTENT);
            }
            else if (mInitialValue4 == Dimension.PARENT_DIMENSION) {
                constraintWidget.setVerticalDimensionBehaviour(ConstraintWidget.DimensionBehaviour.MATCH_PARENT);
            }
            else if (mInitialValue4 == null) {
                constraintWidget.setVerticalDimensionBehaviour(ConstraintWidget.DimensionBehaviour.FIXED);
                constraintWidget.setHeight(this.mValue);
            }
        }
    }
    
    public boolean equalsFixedValue(final int n) {
        return this.mInitialValue == null && this.mValue == n;
    }
    
    public Dimension fixed(final int mValue) {
        this.mInitialValue = null;
        this.mValue = mValue;
        return this;
    }
    
    public Dimension fixed(final Object mInitialValue) {
        this.mInitialValue = mInitialValue;
        if (mInitialValue instanceof Integer) {
            this.mValue = (int)mInitialValue;
            this.mInitialValue = null;
        }
        return this;
    }
    
    int getValue() {
        return this.mValue;
    }
    
    public Dimension max(final int mMax) {
        if (this.mMax >= 0) {
            this.mMax = mMax;
        }
        return this;
    }
    
    public Dimension max(final Object o) {
        final Object wrap_DIMENSION = Dimension.WRAP_DIMENSION;
        if (o == wrap_DIMENSION && this.mIsSuggested) {
            this.mInitialValue = wrap_DIMENSION;
            this.mMax = Integer.MAX_VALUE;
        }
        return this;
    }
    
    public Dimension min(final int mMin) {
        if (mMin >= 0) {
            this.mMin = mMin;
        }
        return this;
    }
    
    public Dimension min(final Object o) {
        if (o == Dimension.WRAP_DIMENSION) {
            this.mMin = -2;
        }
        return this;
    }
    
    public Dimension percent(final Object o, final float mPercent) {
        this.mPercent = mPercent;
        return this;
    }
    
    public Dimension ratio(final String mRatioString) {
        this.mRatioString = mRatioString;
        return this;
    }
    
    void setValue(final int mValue) {
        this.mIsSuggested = false;
        this.mInitialValue = null;
        this.mValue = mValue;
    }
    
    public Dimension suggested(final int mMax) {
        this.mIsSuggested = true;
        if (mMax >= 0) {
            this.mMax = mMax;
        }
        return this;
    }
    
    public Dimension suggested(final Object mInitialValue) {
        this.mInitialValue = mInitialValue;
        this.mIsSuggested = true;
        return this;
    }
    
    public enum Type
    {
        private static final Type[] $VALUES;
        
        FIXED, 
        MATCH_CONSTRAINT, 
        MATCH_PARENT, 
        WRAP;
    }
}

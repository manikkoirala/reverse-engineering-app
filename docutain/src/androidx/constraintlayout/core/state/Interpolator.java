// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.core.state;

public interface Interpolator
{
    float getInterpolation(final float p0);
}

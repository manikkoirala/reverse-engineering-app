// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.core.state.helpers;

import androidx.constraintlayout.core.state.ConstraintReference;
import java.util.Iterator;
import androidx.constraintlayout.core.state.State;
import androidx.constraintlayout.core.state.HelperReference;

public class AlignHorizontallyReference extends HelperReference
{
    private float mBias;
    
    public AlignHorizontallyReference(final State state) {
        super(state, State.Helper.ALIGN_VERTICALLY);
        this.mBias = 0.5f;
    }
    
    @Override
    public void apply() {
        final Iterator<Object> iterator = this.mReferences.iterator();
        while (iterator.hasNext()) {
            final ConstraintReference constraints = this.mState.constraints(iterator.next());
            constraints.clearHorizontal();
            if (this.mStartToStart != null) {
                constraints.startToStart(this.mStartToStart);
            }
            else if (this.mStartToEnd != null) {
                constraints.startToEnd(this.mStartToEnd);
            }
            else {
                constraints.startToStart(State.PARENT);
            }
            if (this.mEndToStart != null) {
                constraints.endToStart(this.mEndToStart);
            }
            else if (this.mEndToEnd != null) {
                constraints.endToEnd(this.mEndToEnd);
            }
            else {
                constraints.endToEnd(State.PARENT);
            }
            final float mBias = this.mBias;
            if (mBias != 0.5f) {
                constraints.horizontalBias(mBias);
            }
        }
    }
}

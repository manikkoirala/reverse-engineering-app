// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.core.state.helpers;

import androidx.constraintlayout.core.state.ConstraintReference;
import androidx.constraintlayout.core.state.State;
import androidx.constraintlayout.core.state.HelperReference;

public class ChainReference extends HelperReference
{
    protected float mBias;
    protected State.Chain mStyle;
    
    public ChainReference(final State state, final State.Helper helper) {
        super(state, helper);
        this.mBias = 0.5f;
        this.mStyle = State.Chain.SPREAD;
    }
    
    @Override
    public ChainReference bias(final float mBias) {
        this.mBias = mBias;
        return this;
    }
    
    public float getBias() {
        return this.mBias;
    }
    
    public State.Chain getStyle() {
        return State.Chain.SPREAD;
    }
    
    public ChainReference style(final State.Chain mStyle) {
        this.mStyle = mStyle;
        return this;
    }
}

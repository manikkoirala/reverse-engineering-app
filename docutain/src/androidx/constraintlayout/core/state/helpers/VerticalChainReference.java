// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.core.state.helpers;

import androidx.constraintlayout.core.state.ConstraintReference;
import java.util.Iterator;
import androidx.constraintlayout.core.state.State;

public class VerticalChainReference extends ChainReference
{
    public VerticalChainReference(final State state) {
        super(state, State.Helper.VERTICAL_CHAIN);
    }
    
    @Override
    public void apply() {
        final Iterator<Object> iterator = this.mReferences.iterator();
        while (iterator.hasNext()) {
            this.mState.constraints(iterator.next()).clearVertical();
        }
        final Iterator<Object> iterator2 = this.mReferences.iterator();
        ConstraintReference constraintReference = null;
        ConstraintReference constraintReference2 = null;
        while (iterator2.hasNext()) {
            final ConstraintReference constraints = this.mState.constraints(iterator2.next());
            ConstraintReference constraintReference3;
            if ((constraintReference3 = constraintReference2) == null) {
                if (this.mTopToTop != null) {
                    constraints.topToTop(this.mTopToTop).margin(this.mMarginTop).marginGone(this.mMarginTopGone);
                }
                else if (this.mTopToBottom != null) {
                    constraints.topToBottom(this.mTopToBottom).margin(this.mMarginTop).marginGone(this.mMarginTopGone);
                }
                else {
                    constraints.topToTop(State.PARENT);
                }
                constraintReference3 = constraints;
            }
            if (constraintReference != null) {
                constraintReference.bottomToTop(constraints.getKey());
                constraints.topToBottom(constraintReference.getKey());
            }
            constraintReference = constraints;
            constraintReference2 = constraintReference3;
        }
        if (constraintReference != null) {
            if (this.mBottomToTop != null) {
                constraintReference.bottomToTop(this.mBottomToTop).margin(this.mMarginBottom).marginGone(this.mMarginBottomGone);
            }
            else if (this.mBottomToBottom != null) {
                constraintReference.bottomToBottom(this.mBottomToBottom).margin(this.mMarginBottom).marginGone(this.mMarginBottomGone);
            }
            else {
                constraintReference.bottomToBottom(State.PARENT);
            }
        }
        if (constraintReference2 == null) {
            return;
        }
        if (this.mBias != 0.5f) {
            constraintReference2.verticalBias(this.mBias);
        }
        final int n = VerticalChainReference$1.$SwitchMap$androidx$constraintlayout$core$state$State$Chain[this.mStyle.ordinal()];
        if (n != 1) {
            if (n != 2) {
                if (n == 3) {
                    constraintReference2.setVerticalChainStyle(2);
                }
            }
            else {
                constraintReference2.setVerticalChainStyle(1);
            }
        }
        else {
            constraintReference2.setVerticalChainStyle(0);
        }
    }
}

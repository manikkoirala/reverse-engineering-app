// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.core.state.helpers;

import androidx.constraintlayout.core.state.ConstraintReference;
import androidx.constraintlayout.core.widgets.HelperWidget;
import androidx.constraintlayout.core.state.State;
import androidx.constraintlayout.core.widgets.Barrier;
import androidx.constraintlayout.core.state.HelperReference;

public class BarrierReference extends HelperReference
{
    private Barrier mBarrierWidget;
    private State.Direction mDirection;
    private int mMargin;
    
    public BarrierReference(final State state) {
        super(state, State.Helper.BARRIER);
    }
    
    @Override
    public void apply() {
        this.getHelperWidget();
        final int n = BarrierReference$1.$SwitchMap$androidx$constraintlayout$core$state$State$Direction[this.mDirection.ordinal()];
        int barrierType = 3;
        if (n != 3 && n != 4) {
            if (n != 5) {
                if (n != 6) {
                    barrierType = 0;
                }
            }
            else {
                barrierType = 2;
            }
        }
        else {
            barrierType = 1;
        }
        this.mBarrierWidget.setBarrierType(barrierType);
        this.mBarrierWidget.setMargin(this.mMargin);
    }
    
    @Override
    public HelperWidget getHelperWidget() {
        if (this.mBarrierWidget == null) {
            this.mBarrierWidget = new Barrier();
        }
        return this.mBarrierWidget;
    }
    
    @Override
    public ConstraintReference margin(final int mMargin) {
        this.mMargin = mMargin;
        return this;
    }
    
    @Override
    public ConstraintReference margin(final Object o) {
        this.margin(this.mState.convertDimension(o));
        return this;
    }
    
    public void setBarrierDirection(final State.Direction mDirection) {
        this.mDirection = mDirection;
    }
}

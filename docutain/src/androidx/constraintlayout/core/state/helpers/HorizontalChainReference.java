// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.core.state.helpers;

import androidx.constraintlayout.core.state.ConstraintReference;
import java.util.Iterator;
import androidx.constraintlayout.core.state.State;

public class HorizontalChainReference extends ChainReference
{
    public HorizontalChainReference(final State state) {
        super(state, State.Helper.HORIZONTAL_CHAIN);
    }
    
    @Override
    public void apply() {
        final Iterator<Object> iterator = this.mReferences.iterator();
        while (iterator.hasNext()) {
            this.mState.constraints(iterator.next()).clearHorizontal();
        }
        final Iterator<Object> iterator2 = this.mReferences.iterator();
        ConstraintReference constraintReference = null;
        ConstraintReference constraintReference2 = null;
        while (iterator2.hasNext()) {
            final ConstraintReference constraints = this.mState.constraints(iterator2.next());
            ConstraintReference constraintReference3;
            if ((constraintReference3 = constraintReference2) == null) {
                if (this.mStartToStart != null) {
                    constraints.startToStart(this.mStartToStart).margin(this.mMarginStart).marginGone(this.mMarginStartGone);
                }
                else if (this.mStartToEnd != null) {
                    constraints.startToEnd(this.mStartToEnd).margin(this.mMarginStart).marginGone(this.mMarginStartGone);
                }
                else if (this.mLeftToLeft != null) {
                    constraints.startToStart(this.mLeftToLeft).margin(this.mMarginLeft).marginGone(this.mMarginLeftGone);
                }
                else if (this.mLeftToRight != null) {
                    constraints.startToEnd(this.mLeftToRight).margin(this.mMarginLeft).marginGone(this.mMarginLeftGone);
                }
                else {
                    constraints.startToStart(State.PARENT);
                }
                constraintReference3 = constraints;
            }
            if (constraintReference != null) {
                constraintReference.endToStart(constraints.getKey());
                constraints.startToEnd(constraintReference.getKey());
            }
            constraintReference = constraints;
            constraintReference2 = constraintReference3;
        }
        if (constraintReference != null) {
            if (this.mEndToStart != null) {
                constraintReference.endToStart(this.mEndToStart).margin(this.mMarginEnd).marginGone(this.mMarginEndGone);
            }
            else if (this.mEndToEnd != null) {
                constraintReference.endToEnd(this.mEndToEnd).margin(this.mMarginEnd).marginGone(this.mMarginEndGone);
            }
            else if (this.mRightToLeft != null) {
                constraintReference.endToStart(this.mRightToLeft).margin(this.mMarginRight).marginGone(this.mMarginRightGone);
            }
            else if (this.mRightToRight != null) {
                constraintReference.endToEnd(this.mRightToRight).margin(this.mMarginRight).marginGone(this.mMarginRightGone);
            }
            else {
                constraintReference.endToEnd(State.PARENT);
            }
        }
        if (constraintReference2 == null) {
            return;
        }
        if (this.mBias != 0.5f) {
            constraintReference2.horizontalBias(this.mBias);
        }
        final int n = HorizontalChainReference$1.$SwitchMap$androidx$constraintlayout$core$state$State$Chain[this.mStyle.ordinal()];
        if (n != 1) {
            if (n != 2) {
                if (n == 3) {
                    constraintReference2.setHorizontalChainStyle(2);
                }
            }
            else {
                constraintReference2.setHorizontalChainStyle(1);
            }
        }
        else {
            constraintReference2.setHorizontalChainStyle(0);
        }
    }
}

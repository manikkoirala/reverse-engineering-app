// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.core.state.helpers;

import androidx.constraintlayout.core.widgets.ConstraintWidget;
import androidx.constraintlayout.core.state.State;
import androidx.constraintlayout.core.widgets.Guideline;
import androidx.constraintlayout.core.state.Reference;

public class GuidelineReference implements Facade, Reference
{
    private Object key;
    private int mEnd;
    private Guideline mGuidelineWidget;
    private int mOrientation;
    private float mPercent;
    private int mStart;
    final State mState;
    
    public GuidelineReference(final State mState) {
        this.mStart = -1;
        this.mEnd = -1;
        this.mPercent = 0.0f;
        this.mState = mState;
    }
    
    @Override
    public void apply() {
        this.mGuidelineWidget.setOrientation(this.mOrientation);
        final int mStart = this.mStart;
        if (mStart != -1) {
            this.mGuidelineWidget.setGuideBegin(mStart);
        }
        else {
            final int mEnd = this.mEnd;
            if (mEnd != -1) {
                this.mGuidelineWidget.setGuideEnd(mEnd);
            }
            else {
                this.mGuidelineWidget.setGuidePercent(this.mPercent);
            }
        }
    }
    
    public GuidelineReference end(final Object o) {
        this.mStart = -1;
        this.mEnd = this.mState.convertDimension(o);
        this.mPercent = 0.0f;
        return this;
    }
    
    @Override
    public ConstraintWidget getConstraintWidget() {
        if (this.mGuidelineWidget == null) {
            this.mGuidelineWidget = new Guideline();
        }
        return this.mGuidelineWidget;
    }
    
    @Override
    public Facade getFacade() {
        return null;
    }
    
    @Override
    public Object getKey() {
        return this.key;
    }
    
    public int getOrientation() {
        return this.mOrientation;
    }
    
    public GuidelineReference percent(final float mPercent) {
        this.mStart = -1;
        this.mEnd = -1;
        this.mPercent = mPercent;
        return this;
    }
    
    @Override
    public void setConstraintWidget(final ConstraintWidget constraintWidget) {
        if (constraintWidget instanceof Guideline) {
            this.mGuidelineWidget = (Guideline)constraintWidget;
        }
        else {
            this.mGuidelineWidget = null;
        }
    }
    
    @Override
    public void setKey(final Object key) {
        this.key = key;
    }
    
    public void setOrientation(final int mOrientation) {
        this.mOrientation = mOrientation;
    }
    
    public GuidelineReference start(final Object o) {
        this.mStart = this.mState.convertDimension(o);
        this.mEnd = -1;
        this.mPercent = 0.0f;
        return this;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.core.state.helpers;

import androidx.constraintlayout.core.state.ConstraintReference;
import java.util.Iterator;
import androidx.constraintlayout.core.state.State;
import androidx.constraintlayout.core.state.HelperReference;

public class AlignVerticallyReference extends HelperReference
{
    private float mBias;
    
    public AlignVerticallyReference(final State state) {
        super(state, State.Helper.ALIGN_VERTICALLY);
        this.mBias = 0.5f;
    }
    
    @Override
    public void apply() {
        final Iterator<Object> iterator = this.mReferences.iterator();
        while (iterator.hasNext()) {
            final ConstraintReference constraints = this.mState.constraints(iterator.next());
            constraints.clearVertical();
            if (this.mTopToTop != null) {
                constraints.topToTop(this.mTopToTop);
            }
            else if (this.mTopToBottom != null) {
                constraints.topToBottom(this.mTopToBottom);
            }
            else {
                constraints.topToTop(State.PARENT);
            }
            if (this.mBottomToTop != null) {
                constraints.bottomToTop(this.mBottomToTop);
            }
            else if (this.mBottomToBottom != null) {
                constraints.bottomToBottom(this.mBottomToBottom);
            }
            else {
                constraints.bottomToBottom(State.PARENT);
            }
            final float mBias = this.mBias;
            if (mBias != 0.5f) {
                constraints.verticalBias(mBias);
            }
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.core;

import java.io.PrintStream;
import java.util.Arrays;

public class SolverVariableValues implements ArrayRowVariables
{
    private static final boolean DEBUG = false;
    private static final boolean HASH = true;
    private static float epsilon = 0.001f;
    private int HASH_SIZE;
    private final int NONE;
    private int SIZE;
    int head;
    int[] keys;
    protected final Cache mCache;
    int mCount;
    private final ArrayRow mRow;
    int[] next;
    int[] nextKeys;
    int[] previous;
    float[] values;
    int[] variables;
    
    SolverVariableValues(final ArrayRow mRow, final Cache mCache) {
        this.NONE = -1;
        this.SIZE = 16;
        this.HASH_SIZE = 16;
        this.keys = new int[16];
        this.nextKeys = new int[16];
        this.variables = new int[16];
        this.values = new float[16];
        this.previous = new int[16];
        this.next = new int[16];
        this.mCount = 0;
        this.head = -1;
        this.mRow = mRow;
        this.mCache = mCache;
        this.clear();
    }
    
    private void addToHashMap(final SolverVariable solverVariable, final int n) {
        final int n2 = solverVariable.id % this.HASH_SIZE;
        final int[] keys = this.keys;
        int n3;
        if ((n3 = keys[n2]) == -1) {
            keys[n2] = n;
        }
        else {
            int[] nextKeys;
            while (true) {
                nextKeys = this.nextKeys;
                final int n4 = nextKeys[n3];
                if (n4 == -1) {
                    break;
                }
                n3 = n4;
            }
            nextKeys[n3] = n;
        }
        this.nextKeys[n] = -1;
    }
    
    private void addVariable(final int n, final SolverVariable solverVariable, final float n2) {
        this.variables[n] = solverVariable.id;
        this.values[n] = n2;
        this.previous[n] = -1;
        this.next[n] = -1;
        solverVariable.addToRow(this.mRow);
        ++solverVariable.usageInRowCount;
        ++this.mCount;
    }
    
    private void displayHash() {
        for (int i = 0; i < this.HASH_SIZE; ++i) {
            if (this.keys[i] != -1) {
                final StringBuilder sb = new StringBuilder();
                sb.append(this.hashCode());
                sb.append(" hash [");
                sb.append(i);
                sb.append("] => ");
                String s = sb.toString();
                int n = this.keys[i];
                int j = 0;
                while (j == 0) {
                    final StringBuilder sb2 = new StringBuilder();
                    sb2.append(s);
                    sb2.append(" ");
                    sb2.append(this.variables[n]);
                    s = sb2.toString();
                    final int n2 = this.nextKeys[n];
                    if (n2 != -1) {
                        n = n2;
                    }
                    else {
                        j = 1;
                    }
                }
                System.out.println(s);
            }
        }
    }
    
    private int findEmptySlot() {
        for (int i = 0; i < this.SIZE; ++i) {
            if (this.variables[i] == -1) {
                return i;
            }
        }
        return -1;
    }
    
    private void increaseSize() {
        final int n = this.SIZE * 2;
        this.variables = Arrays.copyOf(this.variables, n);
        this.values = Arrays.copyOf(this.values, n);
        this.previous = Arrays.copyOf(this.previous, n);
        this.next = Arrays.copyOf(this.next, n);
        this.nextKeys = Arrays.copyOf(this.nextKeys, n);
        for (int i = this.SIZE; i < n; ++i) {
            this.variables[i] = -1;
            this.nextKeys[i] = -1;
        }
        this.SIZE = n;
    }
    
    private void insertVariable(int n, final SolverVariable solverVariable, final float n2) {
        final int emptySlot = this.findEmptySlot();
        this.addVariable(emptySlot, solverVariable, n2);
        if (n != -1) {
            this.previous[emptySlot] = n;
            final int[] next = this.next;
            next[emptySlot] = next[n];
            next[n] = emptySlot;
        }
        else {
            this.previous[emptySlot] = -1;
            if (this.mCount > 0) {
                this.next[emptySlot] = this.head;
                this.head = emptySlot;
            }
            else {
                this.next[emptySlot] = -1;
            }
        }
        n = this.next[emptySlot];
        if (n != -1) {
            this.previous[n] = emptySlot;
        }
        this.addToHashMap(solverVariable, emptySlot);
    }
    
    private void removeFromHashMap(final SolverVariable solverVariable) {
        final int n = solverVariable.id % this.HASH_SIZE;
        final int n2 = this.keys[n];
        if (n2 == -1) {
            return;
        }
        final int id = solverVariable.id;
        int n3 = n2;
        if (this.variables[n2] == id) {
            final int[] keys = this.keys;
            final int[] nextKeys = this.nextKeys;
            keys[n] = nextKeys[n2];
            nextKeys[n2] = -1;
        }
        else {
            int[] nextKeys2;
            int n4;
            while (true) {
                nextKeys2 = this.nextKeys;
                n4 = nextKeys2[n3];
                if (n4 == -1 || this.variables[n4] == id) {
                    break;
                }
                n3 = n4;
            }
            if (n4 != -1 && this.variables[n4] == id) {
                nextKeys2[n3] = nextKeys2[n4];
                nextKeys2[n4] = -1;
            }
        }
    }
    
    @Override
    public void add(final SolverVariable solverVariable, float epsilon, final boolean b) {
        final float epsilon2 = SolverVariableValues.epsilon;
        if (epsilon > -epsilon2 && epsilon < epsilon2) {
            return;
        }
        final int index = this.indexOf(solverVariable);
        if (index == -1) {
            this.put(solverVariable, epsilon);
        }
        else {
            final float[] values = this.values;
            final float n = values[index] + epsilon;
            values[index] = n;
            epsilon = SolverVariableValues.epsilon;
            if (n > -epsilon && n < epsilon) {
                values[index] = 0.0f;
                this.remove(solverVariable, b);
            }
        }
    }
    
    @Override
    public void clear() {
        for (int mCount = this.mCount, i = 0; i < mCount; ++i) {
            final SolverVariable variable = this.getVariable(i);
            if (variable != null) {
                variable.removeFromRow(this.mRow);
            }
        }
        for (int j = 0; j < this.SIZE; ++j) {
            this.variables[j] = -1;
            this.nextKeys[j] = -1;
        }
        for (int k = 0; k < this.HASH_SIZE; ++k) {
            this.keys[k] = -1;
        }
        this.mCount = 0;
        this.head = -1;
    }
    
    @Override
    public boolean contains(final SolverVariable solverVariable) {
        return this.indexOf(solverVariable) != -1;
    }
    
    @Override
    public void display() {
        final int mCount = this.mCount;
        System.out.print("{ ");
        for (int i = 0; i < mCount; ++i) {
            final SolverVariable variable = this.getVariable(i);
            if (variable != null) {
                final PrintStream out = System.out;
                final StringBuilder sb = new StringBuilder();
                sb.append(variable);
                sb.append(" = ");
                sb.append(this.getVariableValue(i));
                sb.append(" ");
                out.print(sb.toString());
            }
        }
        System.out.println(" }");
    }
    
    @Override
    public void divideByAmount(final float n) {
        final int mCount = this.mCount;
        int head = this.head;
        for (int i = 0; i < mCount; ++i) {
            final float[] values = this.values;
            values[head] /= n;
            head = this.next[head];
            if (head == -1) {
                break;
            }
        }
    }
    
    @Override
    public float get(final SolverVariable solverVariable) {
        final int index = this.indexOf(solverVariable);
        if (index != -1) {
            return this.values[index];
        }
        return 0.0f;
    }
    
    @Override
    public int getCurrentSize() {
        return this.mCount;
    }
    
    @Override
    public SolverVariable getVariable(final int n) {
        final int mCount = this.mCount;
        if (mCount == 0) {
            return null;
        }
        int head = this.head;
        for (int i = 0; i < mCount; ++i) {
            if (i == n && head != -1) {
                return this.mCache.mIndexedVariables[this.variables[head]];
            }
            head = this.next[head];
            if (head == -1) {
                break;
            }
        }
        return null;
    }
    
    @Override
    public float getVariableValue(final int n) {
        final int mCount = this.mCount;
        int head = this.head;
        for (int i = 0; i < mCount; ++i) {
            if (i == n) {
                return this.values[head];
            }
            head = this.next[head];
            if (head == -1) {
                break;
            }
        }
        return 0.0f;
    }
    
    @Override
    public int indexOf(final SolverVariable solverVariable) {
        if (this.mCount != 0) {
            if (solverVariable != null) {
                final int id = solverVariable.id;
                final int n = this.keys[id % this.HASH_SIZE];
                if (n == -1) {
                    return -1;
                }
                int n2 = n;
                if (this.variables[n] == id) {
                    return n;
                }
                do {
                    n2 = this.nextKeys[n2];
                } while (n2 != -1 && this.variables[n2] != id);
                if (n2 == -1) {
                    return -1;
                }
                if (this.variables[n2] == id) {
                    return n2;
                }
            }
        }
        return -1;
    }
    
    @Override
    public void invert() {
        final int mCount = this.mCount;
        int head = this.head;
        for (int i = 0; i < mCount; ++i) {
            final float[] values = this.values;
            values[head] *= -1.0f;
            head = this.next[head];
            if (head == -1) {
                break;
            }
        }
    }
    
    @Override
    public void put(final SolverVariable solverVariable, final float n) {
        final float epsilon = SolverVariableValues.epsilon;
        if (n > -epsilon && n < epsilon) {
            this.remove(solverVariable, true);
            return;
        }
        final int mCount = this.mCount;
        int n2 = 0;
        if (mCount == 0) {
            this.addVariable(0, solverVariable, n);
            this.addToHashMap(solverVariable, 0);
            this.head = 0;
        }
        else {
            final int index = this.indexOf(solverVariable);
            if (index != -1) {
                this.values[index] = n;
            }
            else {
                if (this.mCount + 1 >= this.SIZE) {
                    this.increaseSize();
                }
                final int mCount2 = this.mCount;
                int head = this.head;
                int n3 = -1;
                int n4;
                while (true) {
                    n4 = n3;
                    if (n2 >= mCount2) {
                        break;
                    }
                    if (this.variables[head] == solverVariable.id) {
                        this.values[head] = n;
                        return;
                    }
                    if (this.variables[head] < solverVariable.id) {
                        n3 = head;
                    }
                    head = this.next[head];
                    if (head == -1) {
                        n4 = n3;
                        break;
                    }
                    ++n2;
                }
                this.insertVariable(n4, solverVariable, n);
            }
        }
    }
    
    @Override
    public float remove(final SolverVariable solverVariable, final boolean b) {
        final int index = this.indexOf(solverVariable);
        if (index == -1) {
            return 0.0f;
        }
        this.removeFromHashMap(solverVariable);
        final float n = this.values[index];
        if (this.head == index) {
            this.head = this.next[index];
        }
        this.variables[index] = -1;
        final int[] previous = this.previous;
        final int n2 = previous[index];
        if (n2 != -1) {
            final int[] next = this.next;
            next[n2] = next[index];
        }
        final int n3 = this.next[index];
        if (n3 != -1) {
            previous[n3] = previous[index];
        }
        --this.mCount;
        --solverVariable.usageInRowCount;
        if (b) {
            solverVariable.removeFromRow(this.mRow);
        }
        return n;
    }
    
    @Override
    public int sizeInBytes() {
        return 0;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append(this.hashCode());
        sb.append(" { ");
        String s = sb.toString();
        for (int mCount = this.mCount, i = 0; i < mCount; ++i) {
            final SolverVariable variable = this.getVariable(i);
            if (variable != null) {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append(s);
                sb2.append(variable);
                sb2.append(" = ");
                sb2.append(this.getVariableValue(i));
                sb2.append(" ");
                final String string = sb2.toString();
                final int index = this.indexOf(variable);
                final StringBuilder sb3 = new StringBuilder();
                sb3.append(string);
                sb3.append("[p: ");
                final String string2 = sb3.toString();
                String str;
                if (this.previous[index] != -1) {
                    final StringBuilder sb4 = new StringBuilder();
                    sb4.append(string2);
                    sb4.append(this.mCache.mIndexedVariables[this.variables[this.previous[index]]]);
                    str = sb4.toString();
                }
                else {
                    final StringBuilder sb5 = new StringBuilder();
                    sb5.append(string2);
                    sb5.append("none");
                    str = sb5.toString();
                }
                final StringBuilder sb6 = new StringBuilder();
                sb6.append(str);
                sb6.append(", n: ");
                final String string3 = sb6.toString();
                String str2;
                if (this.next[index] != -1) {
                    final StringBuilder sb7 = new StringBuilder();
                    sb7.append(string3);
                    sb7.append(this.mCache.mIndexedVariables[this.variables[this.next[index]]]);
                    str2 = sb7.toString();
                }
                else {
                    final StringBuilder sb8 = new StringBuilder();
                    sb8.append(string3);
                    sb8.append("none");
                    str2 = sb8.toString();
                }
                final StringBuilder sb9 = new StringBuilder();
                sb9.append(str2);
                sb9.append("]");
                s = sb9.toString();
            }
        }
        final StringBuilder sb10 = new StringBuilder();
        sb10.append(s);
        sb10.append(" }");
        return sb10.toString();
    }
    
    @Override
    public float use(final ArrayRow arrayRow, final boolean b) {
        final float value = this.get(arrayRow.variable);
        this.remove(arrayRow.variable, b);
        final SolverVariableValues solverVariableValues = (SolverVariableValues)arrayRow.variables;
        final int currentSize = solverVariableValues.getCurrentSize();
        final int head = solverVariableValues.head;
        int i = 0;
        int n = 0;
        while (i < currentSize) {
            int n2 = i;
            if (solverVariableValues.variables[n] != -1) {
                this.add(this.mCache.mIndexedVariables[solverVariableValues.variables[n]], solverVariableValues.values[n] * value, b);
                n2 = i + 1;
            }
            ++n;
            i = n2;
        }
        return value;
    }
}

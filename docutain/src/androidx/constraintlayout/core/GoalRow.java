// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.core;

public class GoalRow extends ArrayRow
{
    public GoalRow(final Cache cache) {
        super(cache);
    }
    
    @Override
    public void addError(final SolverVariable solverVariable) {
        super.addError(solverVariable);
        --solverVariable.usageInRowCount;
    }
}

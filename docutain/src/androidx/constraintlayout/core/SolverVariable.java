// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.core;

import java.util.Arrays;
import java.util.HashSet;

public class SolverVariable implements Comparable<SolverVariable>
{
    private static final boolean INTERNAL_DEBUG = false;
    static final int MAX_STRENGTH = 9;
    public static final int STRENGTH_BARRIER = 6;
    public static final int STRENGTH_CENTERING = 7;
    public static final int STRENGTH_EQUALITY = 5;
    public static final int STRENGTH_FIXED = 8;
    public static final int STRENGTH_HIGH = 3;
    public static final int STRENGTH_HIGHEST = 4;
    public static final int STRENGTH_LOW = 1;
    public static final int STRENGTH_MEDIUM = 2;
    public static final int STRENGTH_NONE = 0;
    private static final boolean VAR_USE_HASH = false;
    private static int uniqueConstantId = 1;
    private static int uniqueErrorId = 1;
    private static int uniqueId = 1;
    private static int uniqueSlackId = 1;
    private static int uniqueUnrestrictedId = 1;
    public float computedValue;
    int definitionId;
    float[] goalStrengthVector;
    public int id;
    public boolean inGoal;
    HashSet<ArrayRow> inRows;
    public boolean isFinalValue;
    boolean isSynonym;
    ArrayRow[] mClientEquations;
    int mClientEquationsCount;
    private String mName;
    Type mType;
    public int strength;
    float[] strengthVector;
    int synonym;
    float synonymDelta;
    public int usageInRowCount;
    
    public SolverVariable(final Type mType, final String s) {
        this.id = -1;
        this.definitionId = -1;
        this.strength = 0;
        this.isFinalValue = false;
        this.strengthVector = new float[9];
        this.goalStrengthVector = new float[9];
        this.mClientEquations = new ArrayRow[16];
        this.mClientEquationsCount = 0;
        this.usageInRowCount = 0;
        this.isSynonym = false;
        this.synonym = -1;
        this.synonymDelta = 0.0f;
        this.inRows = null;
        this.mType = mType;
    }
    
    public SolverVariable(final String mName, final Type mType) {
        this.id = -1;
        this.definitionId = -1;
        this.strength = 0;
        this.isFinalValue = false;
        this.strengthVector = new float[9];
        this.goalStrengthVector = new float[9];
        this.mClientEquations = new ArrayRow[16];
        this.mClientEquationsCount = 0;
        this.usageInRowCount = 0;
        this.isSynonym = false;
        this.synonym = -1;
        this.synonymDelta = 0.0f;
        this.inRows = null;
        this.mName = mName;
        this.mType = mType;
    }
    
    private static String getUniqueName(final Type type, final String str) {
        if (str != null) {
            final StringBuilder sb = new StringBuilder();
            sb.append(str);
            sb.append(SolverVariable.uniqueErrorId);
            return sb.toString();
        }
        final int n = SolverVariable$1.$SwitchMap$androidx$constraintlayout$core$SolverVariable$Type[type.ordinal()];
        if (n == 1) {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("U");
            sb2.append(++SolverVariable.uniqueUnrestrictedId);
            return sb2.toString();
        }
        if (n == 2) {
            final StringBuilder sb3 = new StringBuilder();
            sb3.append("C");
            sb3.append(++SolverVariable.uniqueConstantId);
            return sb3.toString();
        }
        if (n == 3) {
            final StringBuilder sb4 = new StringBuilder();
            sb4.append("S");
            sb4.append(++SolverVariable.uniqueSlackId);
            return sb4.toString();
        }
        if (n == 4) {
            final StringBuilder sb5 = new StringBuilder();
            sb5.append("e");
            sb5.append(++SolverVariable.uniqueErrorId);
            return sb5.toString();
        }
        if (n == 5) {
            final StringBuilder sb6 = new StringBuilder();
            sb6.append("V");
            sb6.append(++SolverVariable.uniqueId);
            return sb6.toString();
        }
        throw new AssertionError((Object)type.name());
    }
    
    static void increaseErrorId() {
        ++SolverVariable.uniqueErrorId;
    }
    
    public final void addToRow(final ArrayRow arrayRow) {
        int n = 0;
        while (true) {
            final int mClientEquationsCount = this.mClientEquationsCount;
            if (n >= mClientEquationsCount) {
                final ArrayRow[] mClientEquations = this.mClientEquations;
                if (mClientEquationsCount >= mClientEquations.length) {
                    this.mClientEquations = Arrays.copyOf(mClientEquations, mClientEquations.length * 2);
                }
                final ArrayRow[] mClientEquations2 = this.mClientEquations;
                final int mClientEquationsCount2 = this.mClientEquationsCount;
                mClientEquations2[mClientEquationsCount2] = arrayRow;
                this.mClientEquationsCount = mClientEquationsCount2 + 1;
                return;
            }
            if (this.mClientEquations[n] == arrayRow) {
                return;
            }
            ++n;
        }
    }
    
    void clearStrengths() {
        for (int i = 0; i < 9; ++i) {
            this.strengthVector[i] = 0.0f;
        }
    }
    
    @Override
    public int compareTo(final SolverVariable solverVariable) {
        return this.id - solverVariable.id;
    }
    
    public String getName() {
        return this.mName;
    }
    
    public final void removeFromRow(final ArrayRow arrayRow) {
        for (int mClientEquationsCount = this.mClientEquationsCount, i = 0; i < mClientEquationsCount; ++i) {
            if (this.mClientEquations[i] == arrayRow) {
                while (i < mClientEquationsCount - 1) {
                    final ArrayRow[] mClientEquations = this.mClientEquations;
                    final int n = i + 1;
                    mClientEquations[i] = mClientEquations[n];
                    i = n;
                }
                --this.mClientEquationsCount;
                return;
            }
        }
    }
    
    public void reset() {
        this.mName = null;
        this.mType = Type.UNKNOWN;
        this.strength = 0;
        this.id = -1;
        this.definitionId = -1;
        this.computedValue = 0.0f;
        this.isFinalValue = false;
        this.isSynonym = false;
        this.synonym = -1;
        this.synonymDelta = 0.0f;
        for (int mClientEquationsCount = this.mClientEquationsCount, i = 0; i < mClientEquationsCount; ++i) {
            this.mClientEquations[i] = null;
        }
        this.mClientEquationsCount = 0;
        this.usageInRowCount = 0;
        this.inGoal = false;
        Arrays.fill(this.goalStrengthVector, 0.0f);
    }
    
    public void setFinalValue(final LinearSystem linearSystem, final float computedValue) {
        this.computedValue = computedValue;
        this.isFinalValue = true;
        this.isSynonym = false;
        this.synonym = -1;
        this.synonymDelta = 0.0f;
        final int mClientEquationsCount = this.mClientEquationsCount;
        this.definitionId = -1;
        for (int i = 0; i < mClientEquationsCount; ++i) {
            this.mClientEquations[i].updateFromFinalVariable(linearSystem, this, false);
        }
        this.mClientEquationsCount = 0;
    }
    
    public void setName(final String mName) {
        this.mName = mName;
    }
    
    public void setSynonym(final LinearSystem linearSystem, final SolverVariable solverVariable, final float synonymDelta) {
        this.isSynonym = true;
        this.synonym = solverVariable.id;
        this.synonymDelta = synonymDelta;
        final int mClientEquationsCount = this.mClientEquationsCount;
        this.definitionId = -1;
        for (int i = 0; i < mClientEquationsCount; ++i) {
            this.mClientEquations[i].updateFromSynonymVariable(linearSystem, this, false);
        }
        this.mClientEquationsCount = 0;
        linearSystem.displayReadableRows();
    }
    
    public void setType(final Type mType, final String s) {
        this.mType = mType;
    }
    
    String strengthsToString() {
        final StringBuilder sb = new StringBuilder();
        sb.append(this);
        sb.append("[");
        String s = sb.toString();
        int i = 0;
        boolean b = false;
        boolean b2 = true;
        while (i < this.strengthVector.length) {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append(s);
            sb2.append(this.strengthVector[i]);
            final String string = sb2.toString();
            final float[] strengthVector = this.strengthVector;
            final float n = strengthVector[i];
            if (n > 0.0f) {
                b = false;
            }
            else if (n < 0.0f) {
                b = true;
            }
            if (n != 0.0f) {
                b2 = false;
            }
            if (i < strengthVector.length - 1) {
                final StringBuilder sb3 = new StringBuilder();
                sb3.append(string);
                sb3.append(", ");
                s = sb3.toString();
            }
            else {
                final StringBuilder sb4 = new StringBuilder();
                sb4.append(string);
                sb4.append("] ");
                s = sb4.toString();
            }
            ++i;
        }
        String string2 = s;
        if (b) {
            final StringBuilder sb5 = new StringBuilder();
            sb5.append(s);
            sb5.append(" (-)");
            string2 = sb5.toString();
        }
        String string3 = string2;
        if (b2) {
            final StringBuilder sb6 = new StringBuilder();
            sb6.append(string2);
            sb6.append(" (*)");
            string3 = sb6.toString();
        }
        return string3;
    }
    
    @Override
    public String toString() {
        String s;
        if (this.mName != null) {
            final StringBuilder sb = new StringBuilder();
            sb.append("");
            sb.append(this.mName);
            s = sb.toString();
        }
        else {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("");
            sb2.append(this.id);
            s = sb2.toString();
        }
        return s;
    }
    
    public final void updateReferencesWithNewDefinition(final LinearSystem linearSystem, final ArrayRow arrayRow) {
        for (int mClientEquationsCount = this.mClientEquationsCount, i = 0; i < mClientEquationsCount; ++i) {
            this.mClientEquations[i].updateFromRow(linearSystem, arrayRow, false);
        }
        this.mClientEquationsCount = 0;
    }
    
    public enum Type
    {
        private static final Type[] $VALUES;
        
        CONSTANT, 
        ERROR, 
        SLACK, 
        UNKNOWN, 
        UNRESTRICTED;
    }
}

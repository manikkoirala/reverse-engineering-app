// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.motion.widget;

import android.view.ViewGroup;
import java.io.PrintStream;
import android.view.View;
import androidx.constraintlayout.widget.ConstraintSet;
import android.util.Pair;
import java.util.HashMap;

public class DesignTool implements ProxyInterface
{
    private static final boolean DEBUG = false;
    private static final String TAG = "DesignTool";
    static final HashMap<Pair<Integer, Integer>, String> allAttributes;
    static final HashMap<String, String> allMargins;
    private String mLastEndState;
    private int mLastEndStateId;
    private String mLastStartState;
    private int mLastStartStateId;
    private final MotionLayout mMotionLayout;
    private MotionScene mSceneCache;
    
    static {
        final HashMap<Pair<Integer, Integer>, String> hashMap = allAttributes = new HashMap<Pair<Integer, Integer>, String>();
        final HashMap<String, String> hashMap2 = allMargins = new HashMap<String, String>();
        final Integer value = 4;
        hashMap.put((Pair<Integer, Integer>)Pair.create((Object)value, (Object)value), "layout_constraintBottom_toBottomOf");
        final Integer value2 = 3;
        hashMap.put((Pair<Integer, Integer>)Pair.create((Object)value, (Object)value2), "layout_constraintBottom_toTopOf");
        hashMap.put((Pair<Integer, Integer>)Pair.create((Object)value2, (Object)value), "layout_constraintTop_toBottomOf");
        hashMap.put((Pair<Integer, Integer>)Pair.create((Object)value2, (Object)value2), "layout_constraintTop_toTopOf");
        final Integer value3 = 6;
        hashMap.put((Pair<Integer, Integer>)Pair.create((Object)value3, (Object)value3), "layout_constraintStart_toStartOf");
        final Integer value4 = 7;
        hashMap.put((Pair<Integer, Integer>)Pair.create((Object)value3, (Object)value4), "layout_constraintStart_toEndOf");
        hashMap.put((Pair<Integer, Integer>)Pair.create((Object)value4, (Object)value3), "layout_constraintEnd_toStartOf");
        hashMap.put((Pair<Integer, Integer>)Pair.create((Object)value4, (Object)value4), "layout_constraintEnd_toEndOf");
        final Integer value5 = 1;
        hashMap.put((Pair<Integer, Integer>)Pair.create((Object)value5, (Object)value5), "layout_constraintLeft_toLeftOf");
        final Integer value6 = 2;
        hashMap.put((Pair<Integer, Integer>)Pair.create((Object)value5, (Object)value6), "layout_constraintLeft_toRightOf");
        hashMap.put((Pair<Integer, Integer>)Pair.create((Object)value6, (Object)value6), "layout_constraintRight_toRightOf");
        hashMap.put((Pair<Integer, Integer>)Pair.create((Object)value6, (Object)value5), "layout_constraintRight_toLeftOf");
        final Integer value7 = 5;
        hashMap.put((Pair<Integer, Integer>)Pair.create((Object)value7, (Object)value7), "layout_constraintBaseline_toBaselineOf");
        hashMap2.put("layout_constraintBottom_toBottomOf", "layout_marginBottom");
        hashMap2.put("layout_constraintBottom_toTopOf", "layout_marginBottom");
        hashMap2.put("layout_constraintTop_toBottomOf", "layout_marginTop");
        hashMap2.put("layout_constraintTop_toTopOf", "layout_marginTop");
        hashMap2.put("layout_constraintStart_toStartOf", "layout_marginStart");
        hashMap2.put("layout_constraintStart_toEndOf", "layout_marginStart");
        hashMap2.put("layout_constraintEnd_toStartOf", "layout_marginEnd");
        hashMap2.put("layout_constraintEnd_toEndOf", "layout_marginEnd");
        hashMap2.put("layout_constraintLeft_toLeftOf", "layout_marginLeft");
        hashMap2.put("layout_constraintLeft_toRightOf", "layout_marginLeft");
        hashMap2.put("layout_constraintRight_toRightOf", "layout_marginRight");
        hashMap2.put("layout_constraintRight_toLeftOf", "layout_marginRight");
    }
    
    public DesignTool(final MotionLayout mMotionLayout) {
        this.mLastStartState = null;
        this.mLastEndState = null;
        this.mLastStartStateId = -1;
        this.mLastEndStateId = -1;
        this.mMotionLayout = mMotionLayout;
    }
    
    private static void Connect(int getPxFromDp, final ConstraintSet set, final View view, final HashMap<String, String> hashMap, final int i, final int j) {
        final String s = DesignTool.allAttributes.get(Pair.create((Object)i, (Object)j));
        final String s2 = hashMap.get(s);
        if (s2 != null) {
            final String key = DesignTool.allMargins.get(s);
            if (key != null) {
                getPxFromDp = GetPxFromDp(getPxFromDp, hashMap.get(key));
            }
            else {
                getPxFromDp = 0;
            }
            set.connect(view.getId(), i, Integer.parseInt(s2), j, getPxFromDp);
        }
    }
    
    private static int GetPxFromDp(final int n, final String s) {
        if (s == null) {
            return 0;
        }
        final int index = s.indexOf(100);
        if (index == -1) {
            return 0;
        }
        return (int)(Integer.valueOf(s.substring(0, index)) * n / 160.0f);
    }
    
    private static void SetAbsolutePositions(final int n, final ConstraintSet set, final View view, final HashMap<String, String> hashMap) {
        final String s = hashMap.get("layout_editor_absoluteX");
        if (s != null) {
            set.setEditorAbsoluteX(view.getId(), GetPxFromDp(n, s));
        }
        final String s2 = hashMap.get("layout_editor_absoluteY");
        if (s2 != null) {
            set.setEditorAbsoluteY(view.getId(), GetPxFromDp(n, s2));
        }
    }
    
    private static void SetBias(final ConstraintSet set, final View view, final HashMap<String, String> hashMap, final int n) {
        String key;
        if (n == 1) {
            key = "layout_constraintVertical_bias";
        }
        else {
            key = "layout_constraintHorizontal_bias";
        }
        final String s = hashMap.get(key);
        if (s != null) {
            if (n == 0) {
                set.setHorizontalBias(view.getId(), Float.parseFloat(s));
            }
            else if (n == 1) {
                set.setVerticalBias(view.getId(), Float.parseFloat(s));
            }
        }
    }
    
    private static void SetDimensions(final int n, final ConstraintSet set, final View view, final HashMap<String, String> hashMap, final int n2) {
        String key;
        if (n2 == 1) {
            key = "layout_height";
        }
        else {
            key = "layout_width";
        }
        final String s = hashMap.get(key);
        if (s != null) {
            int getPxFromDp = -2;
            if (!s.equalsIgnoreCase("wrap_content")) {
                getPxFromDp = GetPxFromDp(n, s);
            }
            if (n2 == 0) {
                set.constrainWidth(view.getId(), getPxFromDp);
            }
            else {
                set.constrainHeight(view.getId(), getPxFromDp);
            }
        }
    }
    
    @Override
    public int designAccess(int n, final String s, final Object o, final float[] array, final int n2, final float[] array2, final int n3) {
        final View key = (View)o;
        MotionController motionController;
        if (n != 0) {
            if (this.mMotionLayout.mScene == null) {
                return -1;
            }
            if (key == null || (motionController = this.mMotionLayout.mFrameArrayList.get(key)) == null) {
                return -1;
            }
        }
        else {
            motionController = null;
        }
        if (n == 0) {
            return 1;
        }
        if (n == 1) {
            n = this.mMotionLayout.mScene.getDuration() / 16;
            motionController.buildPath(array2, n);
            return n;
        }
        if (n == 2) {
            n = this.mMotionLayout.mScene.getDuration() / 16;
            motionController.buildKeyFrames(array2, null);
            return n;
        }
        if (n != 3) {
            return -1;
        }
        n = this.mMotionLayout.mScene.getDuration() / 16;
        return motionController.getAttributeValues(s, array2, n3);
    }
    
    public void disableAutoTransition(final boolean b) {
        this.mMotionLayout.disableAutoTransition(b);
    }
    
    public void dumpConstraintSet(final String str) {
        if (this.mMotionLayout.mScene == null) {
            this.mMotionLayout.mScene = this.mSceneCache;
        }
        final int lookUpConstraintId = this.mMotionLayout.lookUpConstraintId(str);
        final PrintStream out = System.out;
        final StringBuilder sb = new StringBuilder();
        sb.append(" dumping  ");
        sb.append(str);
        sb.append(" (");
        sb.append(lookUpConstraintId);
        sb.append(")");
        out.println(sb.toString());
        try {
            this.mMotionLayout.mScene.getConstraintSet(lookUpConstraintId).dump(this.mMotionLayout.mScene, new int[0]);
        }
        catch (final Exception ex) {
            ex.printStackTrace();
        }
    }
    
    public int getAnimationKeyFrames(final Object key, final float[] array) {
        if (this.mMotionLayout.mScene == null) {
            return -1;
        }
        final int n = this.mMotionLayout.mScene.getDuration() / 16;
        final MotionController motionController = this.mMotionLayout.mFrameArrayList.get(key);
        if (motionController == null) {
            return 0;
        }
        motionController.buildKeyFrames(array, null);
        return n;
    }
    
    public int getAnimationPath(final Object key, final float[] array, final int n) {
        if (this.mMotionLayout.mScene == null) {
            return -1;
        }
        final MotionController motionController = this.mMotionLayout.mFrameArrayList.get(key);
        if (motionController == null) {
            return 0;
        }
        motionController.buildPath(array, n);
        return n;
    }
    
    public void getAnimationRectangles(final Object key, final float[] array) {
        if (this.mMotionLayout.mScene == null) {
            return;
        }
        final int n = this.mMotionLayout.mScene.getDuration() / 16;
        final MotionController motionController = this.mMotionLayout.mFrameArrayList.get(key);
        if (motionController == null) {
            return;
        }
        motionController.buildRectangles(array, n);
    }
    
    public String getEndState() {
        final int endState = this.mMotionLayout.getEndState();
        if (this.mLastEndStateId == endState) {
            return this.mLastEndState;
        }
        final String constraintSetNames = this.mMotionLayout.getConstraintSetNames(endState);
        if (constraintSetNames != null) {
            this.mLastEndState = constraintSetNames;
            this.mLastEndStateId = endState;
        }
        return constraintSetNames;
    }
    
    public int getKeyFrameInfo(final Object o, final int n, final int[] array) {
        final MotionController motionController = this.mMotionLayout.mFrameArrayList.get(o);
        if (motionController == null) {
            return 0;
        }
        return motionController.getKeyFrameInfo(n, array);
    }
    
    @Override
    public float getKeyFramePosition(final Object o, final int n, final float n2, final float n3) {
        if (!(o instanceof View)) {
            return 0.0f;
        }
        final MotionController motionController = this.mMotionLayout.mFrameArrayList.get(o);
        if (motionController == null) {
            return 0.0f;
        }
        return motionController.getKeyFrameParameter(n, n2, n3);
    }
    
    public int getKeyFramePositions(final Object o, final int[] array, final float[] array2) {
        final MotionController motionController = this.mMotionLayout.mFrameArrayList.get(o);
        if (motionController == null) {
            return 0;
        }
        return motionController.getKeyFramePositions(array, array2);
    }
    
    public Object getKeyframe(final int n, final int n2, final int n3) {
        if (this.mMotionLayout.mScene == null) {
            return null;
        }
        return this.mMotionLayout.mScene.getKeyFrame(this.mMotionLayout.getContext(), n, n2, n3);
    }
    
    public Object getKeyframe(final Object o, final int n, final int n2) {
        if (this.mMotionLayout.mScene == null) {
            return null;
        }
        return this.mMotionLayout.mScene.getKeyFrame(this.mMotionLayout.getContext(), n, ((View)o).getId(), n2);
    }
    
    @Override
    public Object getKeyframeAtLocation(final Object o, final float n, final float n2) {
        final View key = (View)o;
        if (this.mMotionLayout.mScene == null) {
            return -1;
        }
        if (key == null) {
            return null;
        }
        final MotionController motionController = this.mMotionLayout.mFrameArrayList.get(key);
        if (motionController == null) {
            return null;
        }
        final ViewGroup viewGroup = (ViewGroup)key.getParent();
        return motionController.getPositionKeyframe(viewGroup.getWidth(), viewGroup.getHeight(), n, n2);
    }
    
    @Override
    public Boolean getPositionKeyframe(final Object o, final Object o2, final float n, final float n2, final String[] array, final float[] array2) {
        if (o instanceof KeyPositionBase) {
            final KeyPositionBase keyPositionBase = (KeyPositionBase)o;
            final HashMap<View, MotionController> mFrameArrayList = this.mMotionLayout.mFrameArrayList;
            final View key = (View)o2;
            mFrameArrayList.get(key).positionKeyframe(key, keyPositionBase, n, n2, array, array2);
            this.mMotionLayout.rebuildScene();
            this.mMotionLayout.mInTransition = true;
            return true;
        }
        return false;
    }
    
    public float getProgress() {
        return this.mMotionLayout.getProgress();
    }
    
    public String getStartState() {
        final int startState = this.mMotionLayout.getStartState();
        if (this.mLastStartStateId == startState) {
            return this.mLastStartState;
        }
        final String constraintSetNames = this.mMotionLayout.getConstraintSetNames(startState);
        if (constraintSetNames != null) {
            this.mLastStartState = constraintSetNames;
            this.mLastStartStateId = startState;
        }
        return this.mMotionLayout.getConstraintSetNames(startState);
    }
    
    public String getState() {
        if (this.mLastStartState != null && this.mLastEndState != null) {
            final float progress = this.getProgress();
            if (progress <= 0.01f) {
                return this.mLastStartState;
            }
            if (progress >= 0.99f) {
                return this.mLastEndState;
            }
        }
        return this.mLastStartState;
    }
    
    @Override
    public long getTransitionTimeMs() {
        return this.mMotionLayout.getTransitionTimeMs();
    }
    
    public boolean isInTransition() {
        return this.mLastStartState != null && this.mLastEndState != null;
    }
    
    @Override
    public void setAttributes(final int n, final String s, final Object o, final Object o2) {
        final View view = (View)o;
        final HashMap hashMap = (HashMap)o2;
        final int lookUpConstraintId = this.mMotionLayout.lookUpConstraintId(s);
        final ConstraintSet constraintSet = this.mMotionLayout.mScene.getConstraintSet(lookUpConstraintId);
        if (constraintSet == null) {
            return;
        }
        constraintSet.clear(view.getId());
        SetDimensions(n, constraintSet, view, hashMap, 0);
        SetDimensions(n, constraintSet, view, hashMap, 1);
        Connect(n, constraintSet, view, hashMap, 6, 6);
        Connect(n, constraintSet, view, hashMap, 6, 7);
        Connect(n, constraintSet, view, hashMap, 7, 7);
        Connect(n, constraintSet, view, hashMap, 7, 6);
        Connect(n, constraintSet, view, hashMap, 1, 1);
        Connect(n, constraintSet, view, hashMap, 1, 2);
        Connect(n, constraintSet, view, hashMap, 2, 2);
        Connect(n, constraintSet, view, hashMap, 2, 1);
        Connect(n, constraintSet, view, hashMap, 3, 3);
        Connect(n, constraintSet, view, hashMap, 3, 4);
        Connect(n, constraintSet, view, hashMap, 4, 3);
        Connect(n, constraintSet, view, hashMap, 4, 4);
        Connect(n, constraintSet, view, hashMap, 5, 5);
        SetBias(constraintSet, view, hashMap, 0);
        SetBias(constraintSet, view, hashMap, 1);
        SetAbsolutePositions(n, constraintSet, view, hashMap);
        this.mMotionLayout.updateState(lookUpConstraintId, constraintSet);
        this.mMotionLayout.requestLayout();
    }
    
    @Override
    public void setKeyFrame(final Object o, final int n, final String s, final Object o2) {
        if (this.mMotionLayout.mScene != null) {
            this.mMotionLayout.mScene.setKeyframe((View)o, n, s, o2);
            this.mMotionLayout.mTransitionGoalPosition = n / 100.0f;
            this.mMotionLayout.mTransitionLastPosition = 0.0f;
            this.mMotionLayout.rebuildScene();
            this.mMotionLayout.evaluate(true);
        }
    }
    
    @Override
    public boolean setKeyFramePosition(final Object key, int n, final int n2, float keyFrameParameter, final float n3) {
        if (!(key instanceof View)) {
            return false;
        }
        if (this.mMotionLayout.mScene != null) {
            final MotionController motionController = this.mMotionLayout.mFrameArrayList.get(key);
            n = (int)(this.mMotionLayout.mTransitionPosition * 100.0f);
            if (motionController != null) {
                final MotionScene mScene = this.mMotionLayout.mScene;
                final View view = (View)key;
                if (mScene.hasKeyFramePosition(view, n)) {
                    final float keyFrameParameter2 = motionController.getKeyFrameParameter(2, keyFrameParameter, n3);
                    keyFrameParameter = motionController.getKeyFrameParameter(5, keyFrameParameter, n3);
                    this.mMotionLayout.mScene.setKeyframe(view, n, "motion:percentX", keyFrameParameter2);
                    this.mMotionLayout.mScene.setKeyframe(view, n, "motion:percentY", keyFrameParameter);
                    this.mMotionLayout.rebuildScene();
                    this.mMotionLayout.evaluate(true);
                    this.mMotionLayout.invalidate();
                    return true;
                }
            }
        }
        return false;
    }
    
    public void setKeyframe(final Object o, final String s, final Object o2) {
        if (o instanceof Key) {
            ((Key)o).setValue(s, o2);
            this.mMotionLayout.rebuildScene();
            this.mMotionLayout.mInTransition = true;
        }
    }
    
    public void setState(final String s) {
        String mLastStartState = s;
        if (s == null) {
            mLastStartState = "motion_base";
        }
        if (this.mLastStartState == mLastStartState) {
            return;
        }
        this.mLastStartState = mLastStartState;
        this.mLastEndState = null;
        if (this.mMotionLayout.mScene == null) {
            this.mMotionLayout.mScene = this.mSceneCache;
        }
        final int lookUpConstraintId = this.mMotionLayout.lookUpConstraintId(mLastStartState);
        if ((this.mLastStartStateId = lookUpConstraintId) != 0) {
            if (lookUpConstraintId == this.mMotionLayout.getStartState()) {
                this.mMotionLayout.setProgress(0.0f);
            }
            else if (lookUpConstraintId == this.mMotionLayout.getEndState()) {
                this.mMotionLayout.setProgress(1.0f);
            }
            else {
                this.mMotionLayout.transitionToState(lookUpConstraintId);
                this.mMotionLayout.setProgress(1.0f);
            }
        }
        this.mMotionLayout.requestLayout();
    }
    
    @Override
    public void setToolPosition(final float progress) {
        if (this.mMotionLayout.mScene == null) {
            this.mMotionLayout.mScene = this.mSceneCache;
        }
        this.mMotionLayout.setProgress(progress);
        this.mMotionLayout.evaluate(true);
        this.mMotionLayout.requestLayout();
        this.mMotionLayout.invalidate();
    }
    
    public void setTransition(final String mLastStartState, final String mLastEndState) {
        if (this.mMotionLayout.mScene == null) {
            this.mMotionLayout.mScene = this.mSceneCache;
        }
        final int lookUpConstraintId = this.mMotionLayout.lookUpConstraintId(mLastStartState);
        final int lookUpConstraintId2 = this.mMotionLayout.lookUpConstraintId(mLastEndState);
        this.mMotionLayout.setTransition(lookUpConstraintId, lookUpConstraintId2);
        this.mLastStartStateId = lookUpConstraintId;
        this.mLastEndStateId = lookUpConstraintId2;
        this.mLastStartState = mLastStartState;
        this.mLastEndState = mLastEndState;
    }
    
    public void setViewDebug(final Object key, final int drawPath) {
        if (!(key instanceof View)) {
            return;
        }
        final MotionController motionController = this.mMotionLayout.mFrameArrayList.get(key);
        if (motionController != null) {
            motionController.setDrawPath(drawPath);
            this.mMotionLayout.invalidate();
        }
    }
}

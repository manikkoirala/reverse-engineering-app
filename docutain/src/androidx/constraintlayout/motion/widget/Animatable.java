// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.motion.widget;

public interface Animatable
{
    float getProgress();
    
    void setProgress(final float p0);
}

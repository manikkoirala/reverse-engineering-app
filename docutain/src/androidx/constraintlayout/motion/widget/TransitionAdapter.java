// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.motion.widget;

public abstract class TransitionAdapter implements TransitionListener
{
    @Override
    public void onTransitionChange(final MotionLayout motionLayout, final int n, final int n2, final float n3) {
    }
    
    @Override
    public void onTransitionCompleted(final MotionLayout motionLayout, final int n) {
    }
    
    @Override
    public void onTransitionStarted(final MotionLayout motionLayout, final int n, final int n2) {
    }
    
    @Override
    public void onTransitionTrigger(final MotionLayout motionLayout, final int n, final boolean b, final float n2) {
    }
}

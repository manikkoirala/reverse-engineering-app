// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.motion.widget;

import android.graphics.Rect;
import java.util.HashSet;
import androidx.constraintlayout.widget.ConstraintSet;
import android.os.Build$VERSION;
import android.view.View;
import java.util.Iterator;
import android.util.Log;
import androidx.constraintlayout.motion.utils.ViewSpline;
import java.util.HashMap;
import androidx.constraintlayout.core.motion.utils.Easing;
import androidx.constraintlayout.widget.ConstraintAttribute;
import java.util.LinkedHashMap;

class MotionConstrainedPoint implements Comparable<MotionConstrainedPoint>
{
    static final int CARTESIAN = 2;
    public static final boolean DEBUG = false;
    static final int PERPENDICULAR = 1;
    public static final String TAG = "MotionPaths";
    static String[] names;
    private float alpha;
    private boolean applyElevation;
    LinkedHashMap<String, ConstraintAttribute> attributes;
    private float elevation;
    private float height;
    private int mAnimateRelativeTo;
    private int mDrawPath;
    private Easing mKeyFrameEasing;
    int mMode;
    private float mPathRotate;
    private float mPivotX;
    private float mPivotY;
    private float mProgress;
    double[] mTempDelta;
    double[] mTempValue;
    int mVisibilityMode;
    private float position;
    private float rotation;
    private float rotationX;
    public float rotationY;
    private float scaleX;
    private float scaleY;
    private float translationX;
    private float translationY;
    private float translationZ;
    int visibility;
    private float width;
    private float x;
    private float y;
    
    static {
        MotionConstrainedPoint.names = new String[] { "position", "x", "y", "width", "height", "pathRotate" };
    }
    
    public MotionConstrainedPoint() {
        this.alpha = 1.0f;
        this.mVisibilityMode = 0;
        this.applyElevation = false;
        this.elevation = 0.0f;
        this.rotation = 0.0f;
        this.rotationX = 0.0f;
        this.rotationY = 0.0f;
        this.scaleX = 1.0f;
        this.scaleY = 1.0f;
        this.mPivotX = Float.NaN;
        this.mPivotY = Float.NaN;
        this.translationX = 0.0f;
        this.translationY = 0.0f;
        this.translationZ = 0.0f;
        this.mDrawPath = 0;
        this.mPathRotate = Float.NaN;
        this.mProgress = Float.NaN;
        this.mAnimateRelativeTo = -1;
        this.attributes = new LinkedHashMap<String, ConstraintAttribute>();
        this.mMode = 0;
        this.mTempValue = new double[18];
        this.mTempDelta = new double[18];
    }
    
    private boolean diff(final float n, final float n2) {
        final boolean naN = Float.isNaN(n);
        boolean b = true;
        final boolean b2 = true;
        if (!naN && !Float.isNaN(n2)) {
            return Math.abs(n - n2) > 1.0E-6f && b2;
        }
        if (Float.isNaN(n) == Float.isNaN(n2)) {
            b = false;
        }
        return b;
    }
    
    public void addValues(final HashMap<String, ViewSpline> hashMap, final int i) {
        for (final String str : hashMap.keySet()) {
            final ViewSpline obj = hashMap.get(str);
            str.hashCode();
            int n = -1;
            switch (str) {
                case "alpha": {
                    n = 13;
                    break;
                }
                case "transitionPathRotate": {
                    n = 12;
                    break;
                }
                case "elevation": {
                    n = 11;
                    break;
                }
                case "rotation": {
                    n = 10;
                    break;
                }
                case "transformPivotY": {
                    n = 9;
                    break;
                }
                case "transformPivotX": {
                    n = 8;
                    break;
                }
                case "scaleY": {
                    n = 7;
                    break;
                }
                case "scaleX": {
                    n = 6;
                    break;
                }
                case "progress": {
                    n = 5;
                    break;
                }
                case "translationZ": {
                    n = 4;
                    break;
                }
                case "translationY": {
                    n = 3;
                    break;
                }
                case "translationX": {
                    n = 2;
                    break;
                }
                case "rotationY": {
                    n = 1;
                    break;
                }
                case "rotationX": {
                    n = 0;
                    break;
                }
                default:
                    break;
            }
            float n2 = 1.0f;
            final float n3 = 0.0f;
            final float n4 = 0.0f;
            final float n5 = 0.0f;
            final float n6 = 0.0f;
            final float n7 = 0.0f;
            final float n8 = 0.0f;
            final float n9 = 0.0f;
            final float n10 = 0.0f;
            final float n11 = 0.0f;
            final float n12 = 0.0f;
            final float n13 = 0.0f;
            switch (n) {
                default: {
                    if (!str.startsWith("CUSTOM")) {
                        final StringBuilder sb = new StringBuilder();
                        sb.append("UNKNOWN spline ");
                        sb.append(str);
                        Log.e("MotionPaths", sb.toString());
                        continue;
                    }
                    final String s = str.split(",")[1];
                    if (!this.attributes.containsKey(s)) {
                        continue;
                    }
                    final ConstraintAttribute constraintAttribute = this.attributes.get(s);
                    if (obj instanceof ViewSpline.CustomSet) {
                        ((ViewSpline.CustomSet)obj).setPoint(i, constraintAttribute);
                        continue;
                    }
                    final StringBuilder sb2 = new StringBuilder();
                    sb2.append(str);
                    sb2.append(" ViewSpline not a CustomSet frame = ");
                    sb2.append(i);
                    sb2.append(", value");
                    sb2.append(constraintAttribute.getValueToInterpolate());
                    sb2.append(obj);
                    Log.e("MotionPaths", sb2.toString());
                    continue;
                }
                case 13: {
                    if (!Float.isNaN(this.alpha)) {
                        n2 = this.alpha;
                    }
                    obj.setPoint(i, n2);
                    continue;
                }
                case 12: {
                    float mPathRotate;
                    if (Float.isNaN(this.mPathRotate)) {
                        mPathRotate = n13;
                    }
                    else {
                        mPathRotate = this.mPathRotate;
                    }
                    obj.setPoint(i, mPathRotate);
                    continue;
                }
                case 11: {
                    float elevation;
                    if (Float.isNaN(this.elevation)) {
                        elevation = n3;
                    }
                    else {
                        elevation = this.elevation;
                    }
                    obj.setPoint(i, elevation);
                    continue;
                }
                case 10: {
                    float rotation;
                    if (Float.isNaN(this.rotation)) {
                        rotation = n4;
                    }
                    else {
                        rotation = this.rotation;
                    }
                    obj.setPoint(i, rotation);
                    continue;
                }
                case 9: {
                    float mPivotY;
                    if (Float.isNaN(this.mPivotY)) {
                        mPivotY = n5;
                    }
                    else {
                        mPivotY = this.mPivotY;
                    }
                    obj.setPoint(i, mPivotY);
                    continue;
                }
                case 8: {
                    float mPivotX;
                    if (Float.isNaN(this.mPivotX)) {
                        mPivotX = n6;
                    }
                    else {
                        mPivotX = this.mPivotX;
                    }
                    obj.setPoint(i, mPivotX);
                    continue;
                }
                case 7: {
                    if (!Float.isNaN(this.scaleY)) {
                        n2 = this.scaleY;
                    }
                    obj.setPoint(i, n2);
                    continue;
                }
                case 6: {
                    if (!Float.isNaN(this.scaleX)) {
                        n2 = this.scaleX;
                    }
                    obj.setPoint(i, n2);
                    continue;
                }
                case 5: {
                    float mProgress;
                    if (Float.isNaN(this.mProgress)) {
                        mProgress = n7;
                    }
                    else {
                        mProgress = this.mProgress;
                    }
                    obj.setPoint(i, mProgress);
                    continue;
                }
                case 4: {
                    float translationZ;
                    if (Float.isNaN(this.translationZ)) {
                        translationZ = n8;
                    }
                    else {
                        translationZ = this.translationZ;
                    }
                    obj.setPoint(i, translationZ);
                    continue;
                }
                case 3: {
                    float translationY;
                    if (Float.isNaN(this.translationY)) {
                        translationY = n9;
                    }
                    else {
                        translationY = this.translationY;
                    }
                    obj.setPoint(i, translationY);
                    continue;
                }
                case 2: {
                    float translationX;
                    if (Float.isNaN(this.translationX)) {
                        translationX = n10;
                    }
                    else {
                        translationX = this.translationX;
                    }
                    obj.setPoint(i, translationX);
                    continue;
                }
                case 1: {
                    float rotationY;
                    if (Float.isNaN(this.rotationY)) {
                        rotationY = n11;
                    }
                    else {
                        rotationY = this.rotationY;
                    }
                    obj.setPoint(i, rotationY);
                    continue;
                }
                case 0: {
                    float rotationX;
                    if (Float.isNaN(this.rotationX)) {
                        rotationX = n12;
                    }
                    else {
                        rotationX = this.rotationX;
                    }
                    obj.setPoint(i, rotationX);
                    continue;
                }
            }
        }
    }
    
    public void applyParameters(final View view) {
        this.visibility = view.getVisibility();
        float alpha;
        if (view.getVisibility() != 0) {
            alpha = 0.0f;
        }
        else {
            alpha = view.getAlpha();
        }
        this.alpha = alpha;
        this.applyElevation = false;
        if (Build$VERSION.SDK_INT >= 21) {
            this.elevation = view.getElevation();
        }
        this.rotation = view.getRotation();
        this.rotationX = view.getRotationX();
        this.rotationY = view.getRotationY();
        this.scaleX = view.getScaleX();
        this.scaleY = view.getScaleY();
        this.mPivotX = view.getPivotX();
        this.mPivotY = view.getPivotY();
        this.translationX = view.getTranslationX();
        this.translationY = view.getTranslationY();
        if (Build$VERSION.SDK_INT >= 21) {
            this.translationZ = view.getTranslationZ();
        }
    }
    
    public void applyParameters(final ConstraintSet.Constraint constraint) {
        this.mVisibilityMode = constraint.propertySet.mVisibilityMode;
        this.visibility = constraint.propertySet.visibility;
        float alpha;
        if (constraint.propertySet.visibility != 0 && this.mVisibilityMode == 0) {
            alpha = 0.0f;
        }
        else {
            alpha = constraint.propertySet.alpha;
        }
        this.alpha = alpha;
        this.applyElevation = constraint.transform.applyElevation;
        this.elevation = constraint.transform.elevation;
        this.rotation = constraint.transform.rotation;
        this.rotationX = constraint.transform.rotationX;
        this.rotationY = constraint.transform.rotationY;
        this.scaleX = constraint.transform.scaleX;
        this.scaleY = constraint.transform.scaleY;
        this.mPivotX = constraint.transform.transformPivotX;
        this.mPivotY = constraint.transform.transformPivotY;
        this.translationX = constraint.transform.translationX;
        this.translationY = constraint.transform.translationY;
        this.translationZ = constraint.transform.translationZ;
        this.mKeyFrameEasing = Easing.getInterpolator(constraint.motion.mTransitionEasing);
        this.mPathRotate = constraint.motion.mPathRotate;
        this.mDrawPath = constraint.motion.mDrawPath;
        this.mAnimateRelativeTo = constraint.motion.mAnimateRelativeTo;
        this.mProgress = constraint.propertySet.mProgress;
        for (final String s : constraint.mCustomConstraints.keySet()) {
            final ConstraintAttribute value = constraint.mCustomConstraints.get(s);
            if (value.isContinuous()) {
                this.attributes.put(s, value);
            }
        }
    }
    
    @Override
    public int compareTo(final MotionConstrainedPoint motionConstrainedPoint) {
        return Float.compare(this.position, motionConstrainedPoint.position);
    }
    
    void different(final MotionConstrainedPoint motionConstrainedPoint, final HashSet<String> set) {
        if (this.diff(this.alpha, motionConstrainedPoint.alpha)) {
            set.add("alpha");
        }
        if (this.diff(this.elevation, motionConstrainedPoint.elevation)) {
            set.add("elevation");
        }
        final int visibility = this.visibility;
        final int visibility2 = motionConstrainedPoint.visibility;
        if (visibility != visibility2 && this.mVisibilityMode == 0 && (visibility == 0 || visibility2 == 0)) {
            set.add("alpha");
        }
        if (this.diff(this.rotation, motionConstrainedPoint.rotation)) {
            set.add("rotation");
        }
        if (!Float.isNaN(this.mPathRotate) || !Float.isNaN(motionConstrainedPoint.mPathRotate)) {
            set.add("transitionPathRotate");
        }
        if (!Float.isNaN(this.mProgress) || !Float.isNaN(motionConstrainedPoint.mProgress)) {
            set.add("progress");
        }
        if (this.diff(this.rotationX, motionConstrainedPoint.rotationX)) {
            set.add("rotationX");
        }
        if (this.diff(this.rotationY, motionConstrainedPoint.rotationY)) {
            set.add("rotationY");
        }
        if (this.diff(this.mPivotX, motionConstrainedPoint.mPivotX)) {
            set.add("transformPivotX");
        }
        if (this.diff(this.mPivotY, motionConstrainedPoint.mPivotY)) {
            set.add("transformPivotY");
        }
        if (this.diff(this.scaleX, motionConstrainedPoint.scaleX)) {
            set.add("scaleX");
        }
        if (this.diff(this.scaleY, motionConstrainedPoint.scaleY)) {
            set.add("scaleY");
        }
        if (this.diff(this.translationX, motionConstrainedPoint.translationX)) {
            set.add("translationX");
        }
        if (this.diff(this.translationY, motionConstrainedPoint.translationY)) {
            set.add("translationY");
        }
        if (this.diff(this.translationZ, motionConstrainedPoint.translationZ)) {
            set.add("translationZ");
        }
    }
    
    void different(final MotionConstrainedPoint motionConstrainedPoint, final boolean[] array, final String[] array2) {
        array[0] |= this.diff(this.position, motionConstrainedPoint.position);
        array[1] |= this.diff(this.x, motionConstrainedPoint.x);
        array[2] |= this.diff(this.y, motionConstrainedPoint.y);
        array[3] |= this.diff(this.width, motionConstrainedPoint.width);
        array[4] |= this.diff(this.height, motionConstrainedPoint.height);
    }
    
    void fillStandard(final double[] array, final int[] array2) {
        final float position = this.position;
        int i = 0;
        final float x = this.x;
        final float y = this.y;
        final float width = this.width;
        final float height = this.height;
        final float alpha = this.alpha;
        final float elevation = this.elevation;
        final float rotation = this.rotation;
        final float rotationX = this.rotationX;
        final float rotationY = this.rotationY;
        final float scaleX = this.scaleX;
        final float scaleY = this.scaleY;
        final float mPivotX = this.mPivotX;
        final float mPivotY = this.mPivotY;
        final float translationX = this.translationX;
        final float translationY = this.translationY;
        final float translationZ = this.translationZ;
        final float mPathRotate = this.mPathRotate;
        int n = 0;
        while (i < array2.length) {
            final int n2 = array2[i];
            int n3 = n;
            if (n2 < 18) {
                array[n] = (new float[] { position, x, y, width, height, alpha, elevation, rotation, rotationX, rotationY, scaleX, scaleY, mPivotX, mPivotY, translationX, translationY, translationZ, mPathRotate })[n2];
                n3 = n + 1;
            }
            ++i;
            n = n3;
        }
    }
    
    int getCustomData(final String key, final double[] array, int n) {
        final ConstraintAttribute constraintAttribute = this.attributes.get(key);
        if (constraintAttribute.numberOfInterpolatedValues() == 1) {
            array[n] = constraintAttribute.getValueToInterpolate();
            return 1;
        }
        final int numberOfInterpolatedValues = constraintAttribute.numberOfInterpolatedValues();
        final float[] array2 = new float[numberOfInterpolatedValues];
        constraintAttribute.getValuesToInterpolate(array2);
        for (int i = 0; i < numberOfInterpolatedValues; ++i, ++n) {
            array[n] = array2[i];
        }
        return numberOfInterpolatedValues;
    }
    
    int getCustomDataCount(final String key) {
        return this.attributes.get(key).numberOfInterpolatedValues();
    }
    
    boolean hasCustomData(final String key) {
        return this.attributes.containsKey(key);
    }
    
    void setBounds(final float x, final float y, final float width, final float height) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
    }
    
    public void setState(final Rect rect, final View view, final int n, final float n2) {
        this.setBounds((float)rect.left, (float)rect.top, (float)rect.width(), (float)rect.height());
        this.applyParameters(view);
        this.mPivotX = Float.NaN;
        this.mPivotY = Float.NaN;
        if (n != 1) {
            if (n == 2) {
                this.rotation = n2 + 90.0f;
            }
        }
        else {
            this.rotation = n2 - 90.0f;
        }
    }
    
    public void setState(final Rect rect, final ConstraintSet set, final int n, final int n2) {
        this.setBounds((float)rect.left, (float)rect.top, (float)rect.width(), (float)rect.height());
        this.applyParameters(set.getParameters(n2));
        Label_0095: {
            if (n != 1) {
                if (n != 2) {
                    if (n == 3) {
                        break Label_0095;
                    }
                    if (n != 4) {
                        return;
                    }
                }
                final float rotation = this.rotation + 90.0f;
                this.rotation = rotation;
                if (rotation > 180.0f) {
                    this.rotation = rotation - 360.0f;
                }
                return;
            }
        }
        this.rotation -= 90.0f;
    }
    
    public void setState(final View view) {
        this.setBounds(view.getX(), view.getY(), (float)view.getWidth(), (float)view.getHeight());
        this.applyParameters(view);
    }
}

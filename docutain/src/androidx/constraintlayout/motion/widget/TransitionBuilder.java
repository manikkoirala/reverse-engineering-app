// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.motion.widget;

import androidx.constraintlayout.widget.ConstraintSet;

public class TransitionBuilder
{
    private static final String TAG = "TransitionBuilder";
    
    public static MotionScene.Transition buildTransition(final MotionScene motionScene, final int n, final int n2, final ConstraintSet set, final int n3, final ConstraintSet set2) {
        final MotionScene.Transition transition = new MotionScene.Transition(n, motionScene, n2, n3);
        updateConstraintSetInMotionScene(motionScene, transition, set, set2);
        return transition;
    }
    
    private static void updateConstraintSetInMotionScene(final MotionScene motionScene, final MotionScene.Transition transition, final ConstraintSet set, final ConstraintSet set2) {
        final int startConstraintSetId = transition.getStartConstraintSetId();
        final int endConstraintSetId = transition.getEndConstraintSetId();
        motionScene.setConstraintSet(startConstraintSetId, set);
        motionScene.setConstraintSet(endConstraintSetId, set2);
    }
    
    public static void validate(final MotionLayout motionLayout) {
        if (motionLayout.mScene == null) {
            throw new RuntimeException("Invalid motion layout. Layout missing Motion Scene.");
        }
        final MotionScene mScene = motionLayout.mScene;
        if (!mScene.validateLayout(motionLayout)) {
            throw new RuntimeException("MotionLayout doesn't have the right motion scene.");
        }
        if (mScene.mCurrentTransition != null && !mScene.getDefinedTransitions().isEmpty()) {
            return;
        }
        throw new RuntimeException("Invalid motion layout. Motion Scene doesn't have any transition.");
    }
}

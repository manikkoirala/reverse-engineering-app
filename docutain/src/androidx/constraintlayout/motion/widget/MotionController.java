// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.motion.widget;

import android.util.SparseArray;
import java.util.HashSet;
import android.view.ViewGroup$LayoutParams;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.motion.utils.ViewState;
import androidx.constraintlayout.widget.ConstraintSet;
import android.view.View$MeasureSpec;
import androidx.constraintlayout.motion.utils.CustomSupport;
import androidx.constraintlayout.widget.ConstraintAttribute;
import androidx.constraintlayout.core.motion.utils.KeyCache;
import androidx.constraintlayout.core.motion.utils.VelocityMatrix;
import android.graphics.RectF;
import java.util.Arrays;
import androidx.constraintlayout.core.motion.utils.KeyCycleOscillator;
import androidx.constraintlayout.core.motion.utils.SplineSet;
import java.util.Collection;
import android.util.Log;
import java.util.List;
import java.util.Collections;
import android.view.animation.OvershootInterpolator;
import android.view.animation.BounceInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.AnimationUtils;
import android.content.Context;
import java.util.Iterator;
import androidx.constraintlayout.core.motion.utils.Easing;
import android.view.View;
import androidx.constraintlayout.motion.utils.ViewTimeCycle;
import android.graphics.Rect;
import android.view.animation.Interpolator;
import java.util.ArrayList;
import androidx.constraintlayout.motion.utils.ViewOscillator;
import androidx.constraintlayout.motion.utils.ViewSpline;
import java.util.HashMap;
import androidx.constraintlayout.core.motion.utils.CurveFit;

public class MotionController
{
    static final int BOUNCE = 4;
    private static final boolean DEBUG = false;
    public static final int DRAW_PATH_AS_CONFIGURED = 4;
    public static final int DRAW_PATH_BASIC = 1;
    public static final int DRAW_PATH_CARTESIAN = 3;
    public static final int DRAW_PATH_NONE = 0;
    public static final int DRAW_PATH_RECTANGLE = 5;
    public static final int DRAW_PATH_RELATIVE = 2;
    public static final int DRAW_PATH_SCREEN = 6;
    static final int EASE_IN = 1;
    static final int EASE_IN_OUT = 0;
    static final int EASE_OUT = 2;
    private static final boolean FAVOR_FIXED_SIZE_VIEWS = false;
    public static final int HORIZONTAL_PATH_X = 2;
    public static final int HORIZONTAL_PATH_Y = 3;
    private static final int INTERPOLATOR_REFERENCE_ID = -2;
    private static final int INTERPOLATOR_UNDEFINED = -3;
    static final int LINEAR = 3;
    static final int OVERSHOOT = 5;
    public static final int PATH_PERCENT = 0;
    public static final int PATH_PERPENDICULAR = 1;
    public static final int ROTATION_LEFT = 2;
    public static final int ROTATION_RIGHT = 1;
    private static final int SPLINE_STRING = -1;
    private static final String TAG = "MotionController";
    public static final int VERTICAL_PATH_X = 4;
    public static final int VERTICAL_PATH_Y = 5;
    private int MAX_DIMENSION;
    String[] attributeTable;
    private CurveFit mArcSpline;
    private int[] mAttributeInterpolatorCount;
    private String[] mAttributeNames;
    private HashMap<String, ViewSpline> mAttributesMap;
    String mConstraintTag;
    float mCurrentCenterX;
    float mCurrentCenterY;
    private int mCurveFitType;
    private HashMap<String, ViewOscillator> mCycleMap;
    private MotionPaths mEndMotionPath;
    private MotionConstrainedPoint mEndPoint;
    boolean mForceMeasure;
    int mId;
    private double[] mInterpolateData;
    private int[] mInterpolateVariables;
    private double[] mInterpolateVelocity;
    private ArrayList<Key> mKeyList;
    private KeyTrigger[] mKeyTriggers;
    private ArrayList<MotionPaths> mMotionPaths;
    float mMotionStagger;
    private boolean mNoMovement;
    private int mPathMotionArc;
    private Interpolator mQuantizeMotionInterpolator;
    private float mQuantizeMotionPhase;
    private int mQuantizeMotionSteps;
    private CurveFit[] mSpline;
    float mStaggerOffset;
    float mStaggerScale;
    private MotionPaths mStartMotionPath;
    private MotionConstrainedPoint mStartPoint;
    Rect mTempRect;
    private HashMap<String, ViewTimeCycle> mTimeCycleAttributesMap;
    private int mTransformPivotTarget;
    private View mTransformPivotView;
    private float[] mValuesBuff;
    private float[] mVelocity;
    View mView;
    
    MotionController(final View view) {
        this.mTempRect = new Rect();
        this.mForceMeasure = false;
        this.mCurveFitType = -1;
        this.mStartMotionPath = new MotionPaths();
        this.mEndMotionPath = new MotionPaths();
        this.mStartPoint = new MotionConstrainedPoint();
        this.mEndPoint = new MotionConstrainedPoint();
        this.mMotionStagger = Float.NaN;
        this.mStaggerOffset = 0.0f;
        this.mStaggerScale = 1.0f;
        this.MAX_DIMENSION = 4;
        this.mValuesBuff = new float[4];
        this.mMotionPaths = new ArrayList<MotionPaths>();
        this.mVelocity = new float[1];
        this.mKeyList = new ArrayList<Key>();
        this.mPathMotionArc = Key.UNSET;
        this.mTransformPivotTarget = Key.UNSET;
        this.mTransformPivotView = null;
        this.mQuantizeMotionSteps = Key.UNSET;
        this.mQuantizeMotionPhase = Float.NaN;
        this.mQuantizeMotionInterpolator = null;
        this.mNoMovement = false;
        this.setView(view);
    }
    
    private float getAdjustedPosition(float time, final float[] array) {
        final float n = 0.0f;
        final float n2 = 1.0f;
        float min;
        if (array != null) {
            array[0] = 1.0f;
            min = time;
        }
        else {
            final float mStaggerScale = this.mStaggerScale;
            min = time;
            if (mStaggerScale != 1.0) {
                final float mStaggerOffset = this.mStaggerOffset;
                float n3 = time;
                if (time < mStaggerOffset) {
                    n3 = 0.0f;
                }
                min = n3;
                if (n3 > mStaggerOffset) {
                    min = n3;
                    if (n3 < 1.0) {
                        min = Math.min((n3 - mStaggerOffset) * mStaggerScale, 1.0f);
                    }
                }
            }
        }
        Easing easing = this.mStartMotionPath.mKeyFrameEasing;
        time = Float.NaN;
        final Iterator<MotionPaths> iterator = this.mMotionPaths.iterator();
        float time2 = n;
        while (iterator.hasNext()) {
            final MotionPaths motionPaths = iterator.next();
            if (motionPaths.mKeyFrameEasing != null) {
                if (motionPaths.time < min) {
                    easing = motionPaths.mKeyFrameEasing;
                    time2 = motionPaths.time;
                }
                else {
                    if (!Float.isNaN(time)) {
                        continue;
                    }
                    time = motionPaths.time;
                }
            }
        }
        float n4 = min;
        if (easing != null) {
            if (Float.isNaN(time)) {
                time = n2;
            }
            time -= time2;
            final double n5 = (min - time2) / time;
            time = (n4 = (float)easing.get(n5) * time + time2);
            if (array != null) {
                array[0] = (float)easing.getDiff(n5);
                n4 = time;
            }
        }
        return n4;
    }
    
    private static Interpolator getInterpolator(final Context context, final int n, final String s, final int n2) {
        if (n == -2) {
            return AnimationUtils.loadInterpolator(context, n2);
        }
        if (n == -1) {
            return (Interpolator)new Interpolator(Easing.getInterpolator(s)) {
                final Easing val$easing;
                
                public float getInterpolation(final float n) {
                    return (float)this.val$easing.get(n);
                }
            };
        }
        if (n == 0) {
            return (Interpolator)new AccelerateDecelerateInterpolator();
        }
        if (n == 1) {
            return (Interpolator)new AccelerateInterpolator();
        }
        if (n == 2) {
            return (Interpolator)new DecelerateInterpolator();
        }
        if (n == 4) {
            return (Interpolator)new BounceInterpolator();
        }
        if (n != 5) {
            return null;
        }
        return (Interpolator)new OvershootInterpolator();
    }
    
    private float getPreCycleDistance() {
        final float[] array = new float[2];
        final float n = 1.0f / 99;
        double n2 = 0.0;
        double n3 = 0.0;
        float n4 = 0.0f;
        for (int i = 0; i < 100; ++i) {
            final float n5 = i * n;
            double n6 = n5;
            Easing mKeyFrameEasing = this.mStartMotionPath.mKeyFrameEasing;
            final Iterator<MotionPaths> iterator = this.mMotionPaths.iterator();
            float n7 = Float.NaN;
            float n8 = 0.0f;
            while (iterator.hasNext()) {
                final MotionPaths motionPaths = iterator.next();
                Easing mKeyFrameEasing2 = mKeyFrameEasing;
                float time = n7;
                float time2 = n8;
                if (motionPaths.mKeyFrameEasing != null) {
                    if (motionPaths.time < n5) {
                        mKeyFrameEasing2 = motionPaths.mKeyFrameEasing;
                        time2 = motionPaths.time;
                        time = n7;
                    }
                    else {
                        mKeyFrameEasing2 = mKeyFrameEasing;
                        time = n7;
                        time2 = n8;
                        if (Float.isNaN(n7)) {
                            time = motionPaths.time;
                            time2 = n8;
                            mKeyFrameEasing2 = mKeyFrameEasing;
                        }
                    }
                }
                mKeyFrameEasing = mKeyFrameEasing2;
                n7 = time;
                n8 = time2;
            }
            if (mKeyFrameEasing != null) {
                float n9 = n7;
                if (Float.isNaN(n7)) {
                    n9 = 1.0f;
                }
                final float n10 = n9 - n8;
                n6 = (float)mKeyFrameEasing.get((n5 - n8) / n10) * n10 + n8;
            }
            this.mSpline[0].getPos(n6, this.mInterpolateData);
            this.mStartMotionPath.getCenter(n6, this.mInterpolateVariables, this.mInterpolateData, array, 0);
            if (i > 0) {
                n4 += (float)Math.hypot(n3 - array[1], n2 - array[0]);
            }
            n2 = array[0];
            n3 = array[1];
        }
        return n4;
    }
    
    private void insertKey(final MotionPaths motionPaths) {
        final int binarySearch = Collections.binarySearch(this.mMotionPaths, motionPaths);
        if (binarySearch == 0) {
            final StringBuilder sb = new StringBuilder();
            sb.append(" KeyPath position \"");
            sb.append(motionPaths.position);
            sb.append("\" outside of range");
            Log.e("MotionController", sb.toString());
        }
        this.mMotionPaths.add(-binarySearch - 1, motionPaths);
    }
    
    private void readView(final MotionPaths motionPaths) {
        motionPaths.setBounds((float)(int)this.mView.getX(), (float)(int)this.mView.getY(), (float)this.mView.getWidth(), (float)this.mView.getHeight());
    }
    
    public void addKey(final Key e) {
        this.mKeyList.add(e);
    }
    
    void addKeys(final ArrayList<Key> c) {
        this.mKeyList.addAll(c);
    }
    
    void buildBounds(final float[] array, final int n) {
        final float n2 = 1.0f / (n - 1);
        final HashMap<String, ViewSpline> mAttributesMap = this.mAttributesMap;
        if (mAttributesMap != null) {
            final SplineSet set = mAttributesMap.get("translationX");
        }
        final HashMap<String, ViewSpline> mAttributesMap2 = this.mAttributesMap;
        if (mAttributesMap2 != null) {
            final SplineSet set2 = mAttributesMap2.get("translationY");
        }
        final HashMap<String, ViewOscillator> mCycleMap = this.mCycleMap;
        if (mCycleMap != null) {
            final ViewOscillator viewOscillator = mCycleMap.get("translationX");
        }
        final HashMap<String, ViewOscillator> mCycleMap2 = this.mCycleMap;
        if (mCycleMap2 != null) {
            final ViewOscillator viewOscillator2 = mCycleMap2.get("translationY");
        }
        for (int i = 0; i < n; ++i) {
            final float n3 = i * n2;
            final float mStaggerScale = this.mStaggerScale;
            final float n4 = 0.0f;
            float min = n3;
            if (mStaggerScale != 1.0f) {
                final float mStaggerOffset = this.mStaggerOffset;
                float n5 = n3;
                if (n3 < mStaggerOffset) {
                    n5 = 0.0f;
                }
                min = n5;
                if (n5 > mStaggerOffset) {
                    min = n5;
                    if (n5 < 1.0) {
                        min = Math.min((n5 - mStaggerOffset) * mStaggerScale, 1.0f);
                    }
                }
            }
            double n6 = min;
            Easing easing = this.mStartMotionPath.mKeyFrameEasing;
            float time = Float.NaN;
            final Iterator<MotionPaths> iterator = this.mMotionPaths.iterator();
            float time2 = n4;
            while (iterator.hasNext()) {
                final MotionPaths motionPaths = iterator.next();
                if (motionPaths.mKeyFrameEasing != null) {
                    if (motionPaths.time < min) {
                        easing = motionPaths.mKeyFrameEasing;
                        time2 = motionPaths.time;
                    }
                    else {
                        if (!Float.isNaN(time)) {
                            continue;
                        }
                        time = motionPaths.time;
                    }
                }
            }
            if (easing != null) {
                float n7 = time;
                if (Float.isNaN(time)) {
                    n7 = 1.0f;
                }
                final float n8 = n7 - time2;
                n6 = (float)easing.get((min - time2) / n8) * n8 + time2;
            }
            this.mSpline[0].getPos(n6, this.mInterpolateData);
            final CurveFit mArcSpline = this.mArcSpline;
            if (mArcSpline != null) {
                final double[] mInterpolateData = this.mInterpolateData;
                if (mInterpolateData.length > 0) {
                    mArcSpline.getPos(n6, mInterpolateData);
                }
            }
            this.mStartMotionPath.getBounds(this.mInterpolateVariables, this.mInterpolateData, array, i * 2);
        }
    }
    
    int buildKeyBounds(final float[] array, final int[] array2) {
        if (array != null) {
            final double[] timePoints = this.mSpline[0].getTimePoints();
            if (array2 != null) {
                final Iterator<MotionPaths> iterator = this.mMotionPaths.iterator();
                int n = 0;
                while (iterator.hasNext()) {
                    array2[n] = iterator.next().mMode;
                    ++n;
                }
            }
            int i = 0;
            int n2 = 0;
            while (i < timePoints.length) {
                this.mSpline[0].getPos(timePoints[i], this.mInterpolateData);
                this.mStartMotionPath.getBounds(this.mInterpolateVariables, this.mInterpolateData, array, n2);
                n2 += 2;
                ++i;
            }
            return n2 / 2;
        }
        return 0;
    }
    
    int buildKeyFrames(final float[] array, final int[] array2) {
        if (array != null) {
            final double[] timePoints = this.mSpline[0].getTimePoints();
            if (array2 != null) {
                final Iterator<MotionPaths> iterator = this.mMotionPaths.iterator();
                int n = 0;
                while (iterator.hasNext()) {
                    array2[n] = iterator.next().mMode;
                    ++n;
                }
            }
            int i = 0;
            int n2 = 0;
            while (i < timePoints.length) {
                this.mSpline[0].getPos(timePoints[i], this.mInterpolateData);
                this.mStartMotionPath.getCenter(timePoints[i], this.mInterpolateVariables, this.mInterpolateData, array, n2);
                n2 += 2;
                ++i;
            }
            return n2 / 2;
        }
        return 0;
    }
    
    void buildPath(final float[] array, final int n) {
        final float n2 = 1.0f / (n - 1);
        final HashMap<String, ViewSpline> mAttributesMap = this.mAttributesMap;
        KeyCycleOscillator keyCycleOscillator = null;
        SplineSet set;
        if (mAttributesMap == null) {
            set = null;
        }
        else {
            set = mAttributesMap.get("translationX");
        }
        final HashMap<String, ViewSpline> mAttributesMap2 = this.mAttributesMap;
        SplineSet set2;
        if (mAttributesMap2 == null) {
            set2 = null;
        }
        else {
            set2 = mAttributesMap2.get("translationY");
        }
        final HashMap<String, ViewOscillator> mCycleMap = this.mCycleMap;
        KeyCycleOscillator keyCycleOscillator2;
        if (mCycleMap == null) {
            keyCycleOscillator2 = null;
        }
        else {
            keyCycleOscillator2 = mCycleMap.get("translationX");
        }
        final HashMap<String, ViewOscillator> mCycleMap2 = this.mCycleMap;
        if (mCycleMap2 != null) {
            keyCycleOscillator = mCycleMap2.get("translationY");
        }
        for (int i = 0; i < n; ++i) {
            final float n3 = i * n2;
            final float mStaggerScale = this.mStaggerScale;
            float min = n3;
            if (mStaggerScale != 1.0f) {
                final float mStaggerOffset = this.mStaggerOffset;
                float n4 = n3;
                if (n3 < mStaggerOffset) {
                    n4 = 0.0f;
                }
                min = n4;
                if (n4 > mStaggerOffset) {
                    min = n4;
                    if (n4 < 1.0) {
                        min = Math.min((n4 - mStaggerOffset) * mStaggerScale, 1.0f);
                    }
                }
            }
            double n5 = min;
            Easing mKeyFrameEasing = this.mStartMotionPath.mKeyFrameEasing;
            float n6 = Float.NaN;
            final Iterator<MotionPaths> iterator = this.mMotionPaths.iterator();
            float n7 = 0.0f;
            while (iterator.hasNext()) {
                final MotionPaths motionPaths = iterator.next();
                Easing mKeyFrameEasing2 = mKeyFrameEasing;
                float time = n6;
                float time2 = n7;
                if (motionPaths.mKeyFrameEasing != null) {
                    if (motionPaths.time < min) {
                        mKeyFrameEasing2 = motionPaths.mKeyFrameEasing;
                        time2 = motionPaths.time;
                        time = n6;
                    }
                    else {
                        mKeyFrameEasing2 = mKeyFrameEasing;
                        time = n6;
                        time2 = n7;
                        if (Float.isNaN(n6)) {
                            time = motionPaths.time;
                            time2 = n7;
                            mKeyFrameEasing2 = mKeyFrameEasing;
                        }
                    }
                }
                mKeyFrameEasing = mKeyFrameEasing2;
                n6 = time;
                n7 = time2;
            }
            if (mKeyFrameEasing != null) {
                float n8 = n6;
                if (Float.isNaN(n6)) {
                    n8 = 1.0f;
                }
                final float n9 = n8 - n7;
                n5 = (float)mKeyFrameEasing.get((min - n7) / n9) * n9 + n7;
            }
            this.mSpline[0].getPos(n5, this.mInterpolateData);
            final CurveFit mArcSpline = this.mArcSpline;
            if (mArcSpline != null) {
                final double[] mInterpolateData = this.mInterpolateData;
                if (mInterpolateData.length > 0) {
                    mArcSpline.getPos(n5, mInterpolateData);
                }
            }
            final MotionPaths mStartMotionPath = this.mStartMotionPath;
            final int[] mInterpolateVariables = this.mInterpolateVariables;
            final double[] mInterpolateData2 = this.mInterpolateData;
            int n10 = i * 2;
            mStartMotionPath.getCenter(n5, mInterpolateVariables, mInterpolateData2, array, n10);
            if (keyCycleOscillator2 != null) {
                array[n10] += keyCycleOscillator2.get(min);
            }
            else if (set != null) {
                array[n10] += set.get(min);
            }
            if (keyCycleOscillator != null) {
                ++n10;
                array[n10] += keyCycleOscillator.get(min);
            }
            else if (set2 != null) {
                ++n10;
                array[n10] += set2.get(min);
            }
        }
    }
    
    void buildRect(float adjustedPosition, final float[] array, final int n) {
        adjustedPosition = this.getAdjustedPosition(adjustedPosition, null);
        this.mSpline[0].getPos(adjustedPosition, this.mInterpolateData);
        this.mStartMotionPath.getRect(this.mInterpolateVariables, this.mInterpolateData, array, n);
    }
    
    void buildRectangles(final float[] array, final int n) {
        final float n2 = 1.0f / (n - 1);
        for (int i = 0; i < n; ++i) {
            this.mSpline[0].getPos(this.getAdjustedPosition(i * n2, null), this.mInterpolateData);
            this.mStartMotionPath.getRect(this.mInterpolateVariables, this.mInterpolateData, array, i * 8);
        }
    }
    
    void endTrigger(final boolean b) {
        if ("button".equals(Debug.getName(this.mView)) && this.mKeyTriggers != null) {
            int n = 0;
            while (true) {
                final KeyTrigger[] mKeyTriggers = this.mKeyTriggers;
                if (n >= mKeyTriggers.length) {
                    break;
                }
                final KeyTrigger keyTrigger = mKeyTriggers[n];
                float n2;
                if (b) {
                    n2 = -100.0f;
                }
                else {
                    n2 = 100.0f;
                }
                keyTrigger.conditionallyFire(n2, this.mView);
                ++n;
            }
        }
    }
    
    public int getAnimateRelativeTo() {
        return this.mStartMotionPath.mAnimateRelativeTo;
    }
    
    int getAttributeValues(final String key, final float[] array, int i) {
        final SplineSet set = this.mAttributesMap.get(key);
        if (set == null) {
            return -1;
        }
        for (i = 0; i < array.length; ++i) {
            array[i] = set.get((float)(i / (array.length - 1)));
        }
        return array.length;
    }
    
    public void getCenter(final double n, final float[] array, final float[] a) {
        final double[] array2 = new double[4];
        final double[] array3 = new double[4];
        this.mSpline[0].getPos(n, array2);
        this.mSpline[0].getSlope(n, array3);
        Arrays.fill(a, 0.0f);
        this.mStartMotionPath.getCenter(n, this.mInterpolateVariables, array2, array, array3, a);
    }
    
    public float getCenterX() {
        return this.mCurrentCenterX;
    }
    
    public float getCenterY() {
        return this.mCurrentCenterY;
    }
    
    void getDpDt(float n, final float n2, final float n3, final float[] array) {
        n = this.getAdjustedPosition(n, this.mVelocity);
        final CurveFit[] mSpline = this.mSpline;
        int n4 = 0;
        if (mSpline == null) {
            final float n5 = this.mEndMotionPath.x - this.mStartMotionPath.x;
            final float n6 = this.mEndMotionPath.y - this.mStartMotionPath.y;
            final float width = this.mEndMotionPath.width;
            final float width2 = this.mStartMotionPath.width;
            n = this.mEndMotionPath.height;
            final float height = this.mStartMotionPath.height;
            array[0] = n5 * (1.0f - n2) + (width - width2 + n5) * n2;
            array[1] = n6 * (1.0f - n3) + (n - height + n6) * n3;
            return;
        }
        final CurveFit curveFit = mSpline[0];
        final double n7 = n;
        curveFit.getSlope(n7, this.mInterpolateVelocity);
        this.mSpline[0].getPos(n7, this.mInterpolateData);
        n = this.mVelocity[0];
        double[] mInterpolateVelocity;
        while (true) {
            mInterpolateVelocity = this.mInterpolateVelocity;
            if (n4 >= mInterpolateVelocity.length) {
                break;
            }
            mInterpolateVelocity[n4] *= n;
            ++n4;
        }
        final CurveFit mArcSpline = this.mArcSpline;
        if (mArcSpline != null) {
            final double[] mInterpolateData = this.mInterpolateData;
            if (mInterpolateData.length > 0) {
                mArcSpline.getPos(n7, mInterpolateData);
                this.mArcSpline.getSlope(n7, this.mInterpolateVelocity);
                this.mStartMotionPath.setDpDt(n2, n3, array, this.mInterpolateVariables, this.mInterpolateVelocity, this.mInterpolateData);
            }
            return;
        }
        this.mStartMotionPath.setDpDt(n2, n3, array, this.mInterpolateVariables, mInterpolateVelocity, this.mInterpolateData);
    }
    
    public int getDrawPath() {
        int n = this.mStartMotionPath.mDrawPath;
        final Iterator<MotionPaths> iterator = this.mMotionPaths.iterator();
        while (iterator.hasNext()) {
            n = Math.max(n, iterator.next().mDrawPath);
        }
        return Math.max(n, this.mEndMotionPath.mDrawPath);
    }
    
    public float getFinalHeight() {
        return this.mEndMotionPath.height;
    }
    
    public float getFinalWidth() {
        return this.mEndMotionPath.width;
    }
    
    public float getFinalX() {
        return this.mEndMotionPath.x;
    }
    
    public float getFinalY() {
        return this.mEndMotionPath.y;
    }
    
    MotionPaths getKeyFrame(final int index) {
        return this.mMotionPaths.get(index);
    }
    
    public int getKeyFrameInfo(final int n, final int[] array) {
        final float[] array2 = new float[2];
        final Iterator<Key> iterator = this.mKeyList.iterator();
        int n2 = 0;
        int n3 = 0;
        while (iterator.hasNext()) {
            final Key key = iterator.next();
            if (key.mType != n && n == -1) {
                continue;
            }
            array[n3] = 0;
            int n4 = n3 + 1;
            array[n4] = key.mType;
            ++n4;
            array[n4] = key.mFramePosition;
            final float n5 = key.mFramePosition / 100.0f;
            final CurveFit curveFit = this.mSpline[0];
            final double n6 = n5;
            curveFit.getPos(n6, this.mInterpolateData);
            this.mStartMotionPath.getCenter(n6, this.mInterpolateVariables, this.mInterpolateData, array2, 0);
            ++n4;
            array[n4] = Float.floatToIntBits(array2[0]);
            final int n7 = n4 + 1;
            array[n7] = Float.floatToIntBits(array2[1]);
            int n8 = n7;
            if (key instanceof KeyPosition) {
                final KeyPosition keyPosition = (KeyPosition)key;
                n8 = n7 + 1;
                array[n8] = keyPosition.mPositionType;
                ++n8;
                array[n8] = Float.floatToIntBits(keyPosition.mPercentX);
                ++n8;
                array[n8] = Float.floatToIntBits(keyPosition.mPercentY);
            }
            ++n8;
            array[n3] = n8 - n3;
            ++n2;
            n3 = n8;
        }
        return n2;
    }
    
    float getKeyFrameParameter(final int n, float n2, float n3) {
        final float n4 = this.mEndMotionPath.x - this.mStartMotionPath.x;
        final float n5 = this.mEndMotionPath.y - this.mStartMotionPath.y;
        final float x = this.mStartMotionPath.x;
        final float n6 = this.mStartMotionPath.width / 2.0f;
        final float y = this.mStartMotionPath.y;
        final float n7 = this.mStartMotionPath.height / 2.0f;
        final float n8 = (float)Math.hypot(n4, n5);
        if (n8 < 1.0E-7) {
            return Float.NaN;
        }
        n2 -= x + n6;
        n3 -= y + n7;
        if ((float)Math.hypot(n2, n3) == 0.0f) {
            return 0.0f;
        }
        final float n9 = n2 * n4 + n3 * n5;
        if (n == 0) {
            return n9 / n8;
        }
        if (n == 1) {
            return (float)Math.sqrt(n8 * n8 - n9 * n9);
        }
        if (n == 2) {
            return n2 / n4;
        }
        if (n == 3) {
            return n3 / n4;
        }
        if (n == 4) {
            return n2 / n5;
        }
        if (n != 5) {
            return 0.0f;
        }
        return n3 / n5;
    }
    
    public int getKeyFramePositions(final int[] array, final float[] array2) {
        final Iterator<Key> iterator = this.mKeyList.iterator();
        int n = 0;
        int n2 = 0;
        while (iterator.hasNext()) {
            final Key key = iterator.next();
            array[n] = key.mFramePosition + key.mType * 1000;
            final float n3 = key.mFramePosition / 100.0f;
            final CurveFit curveFit = this.mSpline[0];
            final double n4 = n3;
            curveFit.getPos(n4, this.mInterpolateData);
            this.mStartMotionPath.getCenter(n4, this.mInterpolateVariables, this.mInterpolateData, array2, n2);
            n2 += 2;
            ++n;
        }
        return n;
    }
    
    double[] getPos(final double n) {
        this.mSpline[0].getPos(n, this.mInterpolateData);
        final CurveFit mArcSpline = this.mArcSpline;
        if (mArcSpline != null) {
            final double[] mInterpolateData = this.mInterpolateData;
            if (mInterpolateData.length > 0) {
                mArcSpline.getPos(n, mInterpolateData);
            }
        }
        return this.mInterpolateData;
    }
    
    KeyPositionBase getPositionKeyframe(final int n, final int n2, final float n3, final float n4) {
        final RectF rectF = new RectF();
        rectF.left = this.mStartMotionPath.x;
        rectF.top = this.mStartMotionPath.y;
        rectF.right = rectF.left + this.mStartMotionPath.width;
        rectF.bottom = rectF.top + this.mStartMotionPath.height;
        final RectF rectF2 = new RectF();
        rectF2.left = this.mEndMotionPath.x;
        rectF2.top = this.mEndMotionPath.y;
        rectF2.right = rectF2.left + this.mEndMotionPath.width;
        rectF2.bottom = rectF2.top + this.mEndMotionPath.height;
        for (final Key key : this.mKeyList) {
            if (key instanceof KeyPositionBase) {
                final KeyPositionBase keyPositionBase = (KeyPositionBase)key;
                if (keyPositionBase.intersects(n, n2, rectF, rectF2, n3, n4)) {
                    return keyPositionBase;
                }
                continue;
            }
        }
        return null;
    }
    
    void getPostLayoutDvDp(float n, final int n2, final int n3, final float n4, final float n5, final float[] array) {
        final float adjustedPosition = this.getAdjustedPosition(n, this.mVelocity);
        final HashMap<String, ViewSpline> mAttributesMap = this.mAttributesMap;
        KeyCycleOscillator keyCycleOscillator = null;
        SplineSet set;
        if (mAttributesMap == null) {
            set = null;
        }
        else {
            set = mAttributesMap.get("translationX");
        }
        final HashMap<String, ViewSpline> mAttributesMap2 = this.mAttributesMap;
        SplineSet set2;
        if (mAttributesMap2 == null) {
            set2 = null;
        }
        else {
            set2 = mAttributesMap2.get("translationY");
        }
        final HashMap<String, ViewSpline> mAttributesMap3 = this.mAttributesMap;
        SplineSet set3;
        if (mAttributesMap3 == null) {
            set3 = null;
        }
        else {
            set3 = mAttributesMap3.get("rotation");
        }
        final HashMap<String, ViewSpline> mAttributesMap4 = this.mAttributesMap;
        SplineSet set4;
        if (mAttributesMap4 == null) {
            set4 = null;
        }
        else {
            set4 = mAttributesMap4.get("scaleX");
        }
        final HashMap<String, ViewSpline> mAttributesMap5 = this.mAttributesMap;
        SplineSet set5;
        if (mAttributesMap5 == null) {
            set5 = null;
        }
        else {
            set5 = mAttributesMap5.get("scaleY");
        }
        final HashMap<String, ViewOscillator> mCycleMap = this.mCycleMap;
        KeyCycleOscillator keyCycleOscillator2;
        if (mCycleMap == null) {
            keyCycleOscillator2 = null;
        }
        else {
            keyCycleOscillator2 = mCycleMap.get("translationX");
        }
        final HashMap<String, ViewOscillator> mCycleMap2 = this.mCycleMap;
        KeyCycleOscillator keyCycleOscillator3;
        if (mCycleMap2 == null) {
            keyCycleOscillator3 = null;
        }
        else {
            keyCycleOscillator3 = mCycleMap2.get("translationY");
        }
        final HashMap<String, ViewOscillator> mCycleMap3 = this.mCycleMap;
        KeyCycleOscillator keyCycleOscillator4;
        if (mCycleMap3 == null) {
            keyCycleOscillator4 = null;
        }
        else {
            keyCycleOscillator4 = mCycleMap3.get("rotation");
        }
        final HashMap<String, ViewOscillator> mCycleMap4 = this.mCycleMap;
        KeyCycleOscillator keyCycleOscillator5;
        if (mCycleMap4 == null) {
            keyCycleOscillator5 = null;
        }
        else {
            keyCycleOscillator5 = mCycleMap4.get("scaleX");
        }
        final HashMap<String, ViewOscillator> mCycleMap5 = this.mCycleMap;
        if (mCycleMap5 != null) {
            keyCycleOscillator = mCycleMap5.get("scaleY");
        }
        final VelocityMatrix velocityMatrix = new VelocityMatrix();
        velocityMatrix.clear();
        velocityMatrix.setRotationVelocity(set3, adjustedPosition);
        velocityMatrix.setTranslationVelocity(set, set2, adjustedPosition);
        velocityMatrix.setScaleVelocity(set4, set5, adjustedPosition);
        velocityMatrix.setRotationVelocity(keyCycleOscillator4, adjustedPosition);
        velocityMatrix.setTranslationVelocity(keyCycleOscillator2, keyCycleOscillator3, adjustedPosition);
        velocityMatrix.setScaleVelocity(keyCycleOscillator5, keyCycleOscillator, adjustedPosition);
        final CurveFit mArcSpline = this.mArcSpline;
        if (mArcSpline != null) {
            final double[] mInterpolateData = this.mInterpolateData;
            if (mInterpolateData.length > 0) {
                final double n6 = adjustedPosition;
                mArcSpline.getPos(n6, mInterpolateData);
                this.mArcSpline.getSlope(n6, this.mInterpolateVelocity);
                this.mStartMotionPath.setDpDt(n4, n5, array, this.mInterpolateVariables, this.mInterpolateVelocity, this.mInterpolateData);
            }
            velocityMatrix.applyTransform(n4, n5, n2, n3, array);
            return;
        }
        final CurveFit[] mSpline = this.mSpline;
        int n7 = 0;
        if (mSpline != null) {
            n = this.getAdjustedPosition(adjustedPosition, this.mVelocity);
            final CurveFit curveFit = this.mSpline[0];
            final double n8 = n;
            curveFit.getSlope(n8, this.mInterpolateVelocity);
            this.mSpline[0].getPos(n8, this.mInterpolateData);
            n = this.mVelocity[0];
            double[] mInterpolateVelocity;
            while (true) {
                mInterpolateVelocity = this.mInterpolateVelocity;
                if (n7 >= mInterpolateVelocity.length) {
                    break;
                }
                mInterpolateVelocity[n7] *= n;
                ++n7;
            }
            this.mStartMotionPath.setDpDt(n4, n5, array, this.mInterpolateVariables, mInterpolateVelocity, this.mInterpolateData);
            velocityMatrix.applyTransform(n4, n5, n2, n3, array);
            return;
        }
        final float n9 = this.mEndMotionPath.x - this.mStartMotionPath.x;
        final float n10 = this.mEndMotionPath.y - this.mStartMotionPath.y;
        final float width = this.mEndMotionPath.width;
        final float width2 = this.mStartMotionPath.width;
        final float height = this.mEndMotionPath.height;
        n = this.mStartMotionPath.height;
        array[0] = n9 * (1.0f - n4) + (width - width2 + n9) * n4;
        array[1] = n10 * (1.0f - n5) + (height - n + n10) * n5;
        velocityMatrix.clear();
        velocityMatrix.setRotationVelocity(set3, adjustedPosition);
        velocityMatrix.setTranslationVelocity(set, set2, adjustedPosition);
        velocityMatrix.setScaleVelocity(set4, set5, adjustedPosition);
        velocityMatrix.setRotationVelocity(keyCycleOscillator4, adjustedPosition);
        velocityMatrix.setTranslationVelocity(keyCycleOscillator2, keyCycleOscillator3, adjustedPosition);
        velocityMatrix.setScaleVelocity(keyCycleOscillator5, keyCycleOscillator, adjustedPosition);
        velocityMatrix.applyTransform(n4, n5, n2, n3, array);
    }
    
    public float getStartHeight() {
        return this.mStartMotionPath.height;
    }
    
    public float getStartWidth() {
        return this.mStartMotionPath.width;
    }
    
    public float getStartX() {
        return this.mStartMotionPath.x;
    }
    
    public float getStartY() {
        return this.mStartMotionPath.y;
    }
    
    public int getTransformPivotTarget() {
        return this.mTransformPivotTarget;
    }
    
    public View getView() {
        return this.mView;
    }
    
    boolean interpolate(final View view, float n, final long n2, final KeyCache keyCache) {
        final float n3 = n = this.getAdjustedPosition(n, null);
        if (this.mQuantizeMotionSteps != Key.UNSET) {
            final float n4 = 1.0f / this.mQuantizeMotionSteps;
            final float n5 = (float)Math.floor(n3 / n4);
            final float n6 = n = n3 % n4 / n4;
            if (!Float.isNaN(this.mQuantizeMotionPhase)) {
                n = (n6 + this.mQuantizeMotionPhase) % 1.0f;
            }
            final Interpolator mQuantizeMotionInterpolator = this.mQuantizeMotionInterpolator;
            if (mQuantizeMotionInterpolator != null) {
                n = mQuantizeMotionInterpolator.getInterpolation(n);
            }
            else if (n > 0.5) {
                n = 1.0f;
            }
            else {
                n = 0.0f;
            }
            n = n * n4 + n5 * n4;
        }
        final HashMap<String, ViewSpline> mAttributesMap = this.mAttributesMap;
        if (mAttributesMap != null) {
            final Iterator<ViewSpline> iterator = mAttributesMap.values().iterator();
            while (iterator.hasNext()) {
                iterator.next().setProperty(view, n);
            }
        }
        final HashMap<String, ViewTimeCycle> mTimeCycleAttributesMap = this.mTimeCycleAttributesMap;
        ViewTimeCycle viewTimeCycle;
        int n7;
        if (mTimeCycleAttributesMap != null) {
            final Iterator<ViewTimeCycle> iterator2 = mTimeCycleAttributesMap.values().iterator();
            viewTimeCycle = null;
            n7 = 0;
            while (iterator2.hasNext()) {
                final ViewTimeCycle viewTimeCycle2 = iterator2.next();
                if (viewTimeCycle2 instanceof ViewTimeCycle.PathRotate) {
                    viewTimeCycle = viewTimeCycle2;
                }
                else {
                    n7 |= (viewTimeCycle2.setProperty(view, n, n2, keyCache) ? 1 : 0);
                }
            }
        }
        else {
            viewTimeCycle = null;
            n7 = (false ? 1 : 0);
        }
        final CurveFit[] mSpline = this.mSpline;
        boolean b;
        if (mSpline != null) {
            final CurveFit curveFit = mSpline[0];
            final double n8 = n;
            curveFit.getPos(n8, this.mInterpolateData);
            this.mSpline[0].getSlope(n8, this.mInterpolateVelocity);
            final CurveFit mArcSpline = this.mArcSpline;
            if (mArcSpline != null) {
                final double[] mInterpolateData = this.mInterpolateData;
                if (mInterpolateData.length > 0) {
                    mArcSpline.getPos(n8, mInterpolateData);
                    this.mArcSpline.getSlope(n8, this.mInterpolateVelocity);
                }
            }
            if (!this.mNoMovement) {
                this.mStartMotionPath.setView(n, view, this.mInterpolateVariables, this.mInterpolateData, this.mInterpolateVelocity, null, this.mForceMeasure);
                this.mForceMeasure = false;
            }
            if (this.mTransformPivotTarget != Key.UNSET) {
                if (this.mTransformPivotView == null) {
                    this.mTransformPivotView = ((View)view.getParent()).findViewById(this.mTransformPivotTarget);
                }
                final View mTransformPivotView = this.mTransformPivotView;
                if (mTransformPivotView != null) {
                    final float n9 = (mTransformPivotView.getTop() + this.mTransformPivotView.getBottom()) / 2.0f;
                    final float n10 = (this.mTransformPivotView.getLeft() + this.mTransformPivotView.getRight()) / 2.0f;
                    if (view.getRight() - view.getLeft() > 0 && view.getBottom() - view.getTop() > 0) {
                        final float n11 = (float)view.getLeft();
                        final float n12 = (float)view.getTop();
                        view.setPivotX(n10 - n11);
                        view.setPivotY(n9 - n12);
                    }
                }
            }
            final HashMap<String, ViewSpline> mAttributesMap2 = this.mAttributesMap;
            if (mAttributesMap2 != null) {
                for (final SplineSet set : mAttributesMap2.values()) {
                    if (set instanceof ViewSpline.PathRotate) {
                        final double[] mInterpolateVelocity = this.mInterpolateVelocity;
                        if (mInterpolateVelocity.length <= 1) {
                            continue;
                        }
                        ((ViewSpline.PathRotate)set).setPathRotate(view, n, mInterpolateVelocity[0], mInterpolateVelocity[1]);
                    }
                }
            }
            if (viewTimeCycle != null) {
                final double[] mInterpolateVelocity2 = this.mInterpolateVelocity;
                n7 |= (((ViewTimeCycle.PathRotate)viewTimeCycle).setPathRotate(view, keyCache, n, n2, mInterpolateVelocity2[0], mInterpolateVelocity2[1]) ? 1 : 0);
            }
            int n13 = 1;
            while (true) {
                final CurveFit[] mSpline2 = this.mSpline;
                if (n13 >= mSpline2.length) {
                    break;
                }
                mSpline2[n13].getPos(n8, this.mValuesBuff);
                CustomSupport.setInterpolatedValue(this.mStartMotionPath.attributes.get(this.mAttributeNames[n13 - 1]), view, this.mValuesBuff);
                ++n13;
            }
            if (this.mStartPoint.mVisibilityMode == 0) {
                if (n <= 0.0f) {
                    view.setVisibility(this.mStartPoint.visibility);
                }
                else if (n >= 1.0f) {
                    view.setVisibility(this.mEndPoint.visibility);
                }
                else if (this.mEndPoint.visibility != this.mStartPoint.visibility) {
                    view.setVisibility(0);
                }
            }
            b = (n7 != 0);
            if (this.mKeyTriggers != null) {
                int n14 = 0;
                while (true) {
                    final KeyTrigger[] mKeyTriggers = this.mKeyTriggers;
                    b = (n7 != 0);
                    if (n14 >= mKeyTriggers.length) {
                        break;
                    }
                    mKeyTriggers[n14].conditionallyFire(n, view);
                    ++n14;
                }
            }
        }
        else {
            final float x = this.mStartMotionPath.x;
            final float x2 = this.mEndMotionPath.x;
            final float x3 = this.mStartMotionPath.x;
            final float y = this.mStartMotionPath.y;
            final float y2 = this.mEndMotionPath.y;
            final float y3 = this.mStartMotionPath.y;
            final float width = this.mStartMotionPath.width;
            final float width2 = this.mEndMotionPath.width;
            final float width3 = this.mStartMotionPath.width;
            final float height = this.mStartMotionPath.height;
            final float height2 = this.mEndMotionPath.height;
            final float height3 = this.mStartMotionPath.height;
            final float n15 = x + (x2 - x3) * n + 0.5f;
            final int n16 = (int)n15;
            final float n17 = y + (y2 - y3) * n + 0.5f;
            final int n18 = (int)n17;
            final int n19 = (int)(n15 + (width + (width2 - width3) * n));
            final int n20 = (int)(n17 + (height + (height2 - height3) * n));
            if (this.mEndMotionPath.width != this.mStartMotionPath.width || this.mEndMotionPath.height != this.mStartMotionPath.height || this.mForceMeasure) {
                view.measure(View$MeasureSpec.makeMeasureSpec(n19 - n16, 1073741824), View$MeasureSpec.makeMeasureSpec(n20 - n18, 1073741824));
                this.mForceMeasure = false;
            }
            view.layout(n16, n18, n19, n20);
            b = (n7 != 0);
        }
        final HashMap<String, ViewOscillator> mCycleMap = this.mCycleMap;
        if (mCycleMap != null) {
            for (final ViewOscillator viewOscillator : mCycleMap.values()) {
                if (viewOscillator instanceof ViewOscillator.PathRotateSet) {
                    final ViewOscillator.PathRotateSet set2 = (ViewOscillator.PathRotateSet)viewOscillator;
                    final double[] mInterpolateVelocity3 = this.mInterpolateVelocity;
                    set2.setPathRotate(view, n, mInterpolateVelocity3[0], mInterpolateVelocity3[1]);
                }
                else {
                    viewOscillator.setProperty(view, n);
                }
            }
        }
        return b;
    }
    
    String name() {
        return this.mView.getContext().getResources().getResourceEntryName(this.mView.getId());
    }
    
    void positionKeyframe(final View view, final KeyPositionBase keyPositionBase, final float n, final float n2, final String[] array, final float[] array2) {
        final RectF rectF = new RectF();
        rectF.left = this.mStartMotionPath.x;
        rectF.top = this.mStartMotionPath.y;
        rectF.right = rectF.left + this.mStartMotionPath.width;
        rectF.bottom = rectF.top + this.mStartMotionPath.height;
        final RectF rectF2 = new RectF();
        rectF2.left = this.mEndMotionPath.x;
        rectF2.top = this.mEndMotionPath.y;
        rectF2.right = rectF2.left + this.mEndMotionPath.width;
        rectF2.bottom = rectF2.top + this.mEndMotionPath.height;
        keyPositionBase.positionAttributes(view, rectF, rectF2, n, n2, array, array2);
    }
    
    public void remeasure() {
        this.mForceMeasure = true;
    }
    
    void rotate(final Rect rect, final Rect rect2, int n, int n2, int n3) {
        if (n != 1) {
            if (n != 2) {
                if (n != 3) {
                    if (n == 4) {
                        n3 = rect.left;
                        n = rect.right;
                        rect2.left = n2 - (rect.bottom + rect.top + rect.width()) / 2;
                        rect2.top = (n3 + n - rect.height()) / 2;
                        rect2.right = rect2.left + rect.width();
                        rect2.bottom = rect2.top + rect.height();
                    }
                }
                else {
                    n = rect.left + rect.right;
                    n2 = rect.top;
                    n2 = rect.bottom;
                    rect2.left = rect.height() / 2 + rect.top - n / 2;
                    rect2.top = n3 - (n + rect.height()) / 2;
                    rect2.right = rect2.left + rect.width();
                    rect2.bottom = rect2.top + rect.height();
                }
            }
            else {
                n = rect.left;
                n3 = rect.right;
                rect2.left = n2 - (rect.top + rect.bottom + rect.width()) / 2;
                rect2.top = (n + n3 - rect.height()) / 2;
                rect2.right = rect2.left + rect.width();
                rect2.bottom = rect2.top + rect.height();
            }
        }
        else {
            n = rect.left;
            n2 = rect.right;
            rect2.left = (rect.top + rect.bottom - rect.width()) / 2;
            rect2.top = n3 - (n + n2 + rect.height()) / 2;
            rect2.right = rect2.left + rect.width();
            rect2.bottom = rect2.top + rect.height();
        }
    }
    
    void setBothStates(final View view) {
        this.mStartMotionPath.time = 0.0f;
        this.mStartMotionPath.position = 0.0f;
        this.mNoMovement = true;
        this.mStartMotionPath.setBounds(view.getX(), view.getY(), (float)view.getWidth(), (float)view.getHeight());
        this.mEndMotionPath.setBounds(view.getX(), view.getY(), (float)view.getWidth(), (float)view.getHeight());
        this.mStartPoint.setState(view);
        this.mEndPoint.setState(view);
    }
    
    public void setDrawPath(final int mDrawPath) {
        this.mStartMotionPath.mDrawPath = mDrawPath;
    }
    
    void setEndState(final Rect rect, final ConstraintSet set, final int n, final int n2) {
        final int mRotate = set.mRotate;
        Rect mTempRect = rect;
        if (mRotate != 0) {
            this.rotate(rect, this.mTempRect, mRotate, n, n2);
            mTempRect = this.mTempRect;
        }
        this.mEndMotionPath.time = 1.0f;
        this.mEndMotionPath.position = 1.0f;
        this.readView(this.mEndMotionPath);
        this.mEndMotionPath.setBounds((float)mTempRect.left, (float)mTempRect.top, (float)mTempRect.width(), (float)mTempRect.height());
        this.mEndMotionPath.applyParameters(set.getParameters(this.mId));
        this.mEndPoint.setState(mTempRect, set, mRotate, this.mId);
    }
    
    public void setPathMotionArc(final int mPathMotionArc) {
        this.mPathMotionArc = mPathMotionArc;
    }
    
    void setStartCurrentState(final View state) {
        this.mStartMotionPath.time = 0.0f;
        this.mStartMotionPath.position = 0.0f;
        this.mStartMotionPath.setBounds(state.getX(), state.getY(), (float)state.getWidth(), (float)state.getHeight());
        this.mStartPoint.setState(state);
    }
    
    void setStartState(final Rect rect, final ConstraintSet set, final int n, final int n2) {
        final int mRotate = set.mRotate;
        if (mRotate != 0) {
            this.rotate(rect, this.mTempRect, mRotate, n, n2);
        }
        this.mStartMotionPath.time = 0.0f;
        this.mStartMotionPath.position = 0.0f;
        this.readView(this.mStartMotionPath);
        this.mStartMotionPath.setBounds((float)rect.left, (float)rect.top, (float)rect.width(), (float)rect.height());
        final ConstraintSet.Constraint parameters = set.getParameters(this.mId);
        this.mStartMotionPath.applyParameters(parameters);
        this.mMotionStagger = parameters.motion.mMotionStagger;
        this.mStartPoint.setState(rect, set, mRotate, this.mId);
        this.mTransformPivotTarget = parameters.transform.transformPivotTarget;
        this.mQuantizeMotionSteps = parameters.motion.mQuantizeMotionSteps;
        this.mQuantizeMotionPhase = parameters.motion.mQuantizeMotionPhase;
        this.mQuantizeMotionInterpolator = getInterpolator(this.mView.getContext(), parameters.motion.mQuantizeInterpolatorType, parameters.motion.mQuantizeInterpolatorString, parameters.motion.mQuantizeInterpolatorID);
    }
    
    public void setStartState(final ViewState viewState, final View view, final int n, int left, int left2) {
        this.mStartMotionPath.time = 0.0f;
        this.mStartMotionPath.position = 0.0f;
        final Rect rect = new Rect();
        if (n != 1) {
            if (n == 2) {
                left = viewState.left;
                final int right = viewState.right;
                rect.left = left2 - (viewState.top + viewState.bottom + viewState.width()) / 2;
                rect.top = (left + right - viewState.height()) / 2;
                rect.right = rect.left + viewState.width();
                rect.bottom = rect.top + viewState.height();
            }
        }
        else {
            left2 = viewState.left;
            final int right2 = viewState.right;
            rect.left = (viewState.top + viewState.bottom - viewState.width()) / 2;
            rect.top = left - (left2 + right2 + viewState.height()) / 2;
            rect.right = rect.left + viewState.width();
            rect.bottom = rect.top + viewState.height();
        }
        this.mStartMotionPath.setBounds((float)rect.left, (float)rect.top, (float)rect.width(), (float)rect.height());
        this.mStartPoint.setState(rect, view, n, viewState.rotation);
    }
    
    public void setTransformPivotTarget(final int mTransformPivotTarget) {
        this.mTransformPivotTarget = mTransformPivotTarget;
        this.mTransformPivotView = null;
    }
    
    public void setView(final View mView) {
        this.mView = mView;
        this.mId = mView.getId();
        final ViewGroup$LayoutParams layoutParams = mView.getLayoutParams();
        if (layoutParams instanceof ConstraintLayout.LayoutParams) {
            this.mConstraintTag = ((ConstraintLayout.LayoutParams)layoutParams).getConstraintTag();
        }
    }
    
    public void setup(int i, int j, float preCycleDistance, final long n) {
        new HashSet();
        final HashSet set = new HashSet();
        final HashSet set2 = new HashSet();
        final HashSet set3 = new HashSet();
        final HashMap interpolation = new HashMap();
        if (this.mPathMotionArc != Key.UNSET) {
            this.mStartMotionPath.mPathMotionArc = this.mPathMotionArc;
        }
        this.mStartPoint.different(this.mEndPoint, set2);
        final ArrayList<Key> mKeyList = this.mKeyList;
        ArrayList<KeyTrigger> list2;
        if (mKeyList != null) {
            final Iterator<Key> iterator = mKeyList.iterator();
            ArrayList<KeyTrigger> list = null;
            while (true) {
                list2 = list;
                if (!iterator.hasNext()) {
                    break;
                }
                final Key key = iterator.next();
                if (key instanceof KeyPosition) {
                    final KeyPosition keyPosition = (KeyPosition)key;
                    this.insertKey(new MotionPaths(i, j, keyPosition, this.mStartMotionPath, this.mEndMotionPath));
                    if (keyPosition.mCurveFit == Key.UNSET) {
                        continue;
                    }
                    this.mCurveFitType = keyPosition.mCurveFit;
                }
                else if (key instanceof KeyCycle) {
                    key.getAttributeNames(set3);
                }
                else if (key instanceof KeyTimeCycle) {
                    key.getAttributeNames(set);
                }
                else if (key instanceof KeyTrigger) {
                    ArrayList<KeyTrigger> list3;
                    if ((list3 = list) == null) {
                        list3 = new ArrayList<KeyTrigger>();
                    }
                    list3.add((KeyTrigger)key);
                    list = list3;
                }
                else {
                    key.setInterpolation(interpolation);
                    key.getAttributeNames(set2);
                }
            }
        }
        else {
            list2 = null;
        }
        if (list2 != null) {
            this.mKeyTriggers = list2.toArray(new KeyTrigger[0]);
        }
        if (!set2.isEmpty()) {
            this.mAttributesMap = new HashMap<String, ViewSpline>();
            for (final String s : set2) {
                ViewSpline value;
                if (s.startsWith("CUSTOM,")) {
                    final SparseArray sparseArray = new SparseArray();
                    final String key2 = s.split(",")[1];
                    for (final Key key3 : this.mKeyList) {
                        if (key3.mCustomConstraints == null) {
                            continue;
                        }
                        final ConstraintAttribute constraintAttribute = key3.mCustomConstraints.get(key2);
                        if (constraintAttribute == null) {
                            continue;
                        }
                        sparseArray.append(key3.mFramePosition, (Object)constraintAttribute);
                    }
                    value = ViewSpline.makeCustomSpline(s, (SparseArray<ConstraintAttribute>)sparseArray);
                }
                else {
                    value = ViewSpline.makeSpline(s);
                }
                if (value == null) {
                    continue;
                }
                value.setType(s);
                this.mAttributesMap.put(s, value);
            }
            final ArrayList<Key> mKeyList2 = this.mKeyList;
            if (mKeyList2 != null) {
                for (final Key key4 : mKeyList2) {
                    if (key4 instanceof KeyAttributes) {
                        key4.addValues(this.mAttributesMap);
                    }
                }
            }
            this.mStartPoint.addValues(this.mAttributesMap, 0);
            this.mEndPoint.addValues(this.mAttributesMap, 100);
            for (final String key5 : this.mAttributesMap.keySet()) {
                Label_0672: {
                    if (interpolation.containsKey(key5)) {
                        final Integer n2 = interpolation.get(key5);
                        if (n2 != null) {
                            i = n2;
                            break Label_0672;
                        }
                    }
                    i = 0;
                }
                final SplineSet set4 = this.mAttributesMap.get(key5);
                if (set4 != null) {
                    set4.setup(i);
                }
            }
        }
        if (!set.isEmpty()) {
            if (this.mTimeCycleAttributesMap == null) {
                this.mTimeCycleAttributesMap = new HashMap<String, ViewTimeCycle>();
            }
            for (final String key6 : set) {
                if (this.mTimeCycleAttributesMap.containsKey(key6)) {
                    continue;
                }
                ViewTimeCycle value2;
                if (key6.startsWith("CUSTOM,")) {
                    final SparseArray sparseArray2 = new SparseArray();
                    final String key7 = key6.split(",")[1];
                    for (final Key key8 : this.mKeyList) {
                        if (key8.mCustomConstraints == null) {
                            continue;
                        }
                        final ConstraintAttribute constraintAttribute2 = key8.mCustomConstraints.get(key7);
                        if (constraintAttribute2 == null) {
                            continue;
                        }
                        sparseArray2.append(key8.mFramePosition, (Object)constraintAttribute2);
                    }
                    value2 = ViewTimeCycle.makeCustomSpline(key6, (SparseArray<ConstraintAttribute>)sparseArray2);
                }
                else {
                    value2 = ViewTimeCycle.makeSpline(key6, n);
                }
                if (value2 == null) {
                    continue;
                }
                value2.setType(key6);
                this.mTimeCycleAttributesMap.put(key6, value2);
            }
            final ArrayList<Key> mKeyList3 = this.mKeyList;
            if (mKeyList3 != null) {
                for (final Key key9 : mKeyList3) {
                    if (key9 instanceof KeyTimeCycle) {
                        ((KeyTimeCycle)key9).addTimeValues(this.mTimeCycleAttributesMap);
                    }
                }
            }
            for (final String key10 : this.mTimeCycleAttributesMap.keySet()) {
                if (interpolation.containsKey(key10)) {
                    i = (int)interpolation.get(key10);
                }
                else {
                    i = 0;
                }
                this.mTimeCycleAttributesMap.get(key10).setup(i);
            }
        }
        final int n3 = this.mMotionPaths.size() + 2;
        final MotionPaths[] array = new MotionPaths[n3];
        array[0] = this.mStartMotionPath;
        array[n3 - 1] = this.mEndMotionPath;
        if (this.mMotionPaths.size() > 0 && this.mCurveFitType == -1) {
            this.mCurveFitType = 0;
        }
        final Iterator<MotionPaths> iterator10 = this.mMotionPaths.iterator();
        i = 1;
        while (iterator10.hasNext()) {
            array[i] = iterator10.next();
            ++i;
        }
        final HashSet<String> set5 = new HashSet<String>();
        for (final String e : this.mEndMotionPath.attributes.keySet()) {
            if (this.mStartMotionPath.attributes.containsKey(e)) {
                final StringBuilder sb = new StringBuilder();
                sb.append("CUSTOM,");
                sb.append(e);
                if (set2.contains(sb.toString())) {
                    continue;
                }
                set5.add(e);
            }
        }
        final String[] mAttributeNames = set5.toArray(new String[0]);
        this.mAttributeNames = mAttributeNames;
        this.mAttributeInterpolatorCount = new int[mAttributeNames.length];
        i = 0;
        while (true) {
            final String[] mAttributeNames2 = this.mAttributeNames;
            if (i >= mAttributeNames2.length) {
                break;
            }
            final String s2 = mAttributeNames2[i];
            this.mAttributeInterpolatorCount[i] = 0;
            ConstraintAttribute constraintAttribute3;
            int[] mAttributeInterpolatorCount;
            for (j = 0; j < n3; ++j) {
                if (array[j].attributes.containsKey(s2)) {
                    constraintAttribute3 = array[j].attributes.get(s2);
                    if (constraintAttribute3 != null) {
                        mAttributeInterpolatorCount = this.mAttributeInterpolatorCount;
                        mAttributeInterpolatorCount[i] += constraintAttribute3.numberOfInterpolatedValues();
                        break;
                    }
                }
            }
            ++i;
        }
        final boolean b = array[0].mPathMotionArc != Key.UNSET;
        final int n4 = 18 + this.mAttributeNames.length;
        final boolean[] array2 = new boolean[n4];
        for (i = 1; i < n3; ++i) {
            array[i].different(array[i - 1], array2, this.mAttributeNames, b);
        }
        i = 1;
        int b2 = 0;
        while (i < n4) {
            j = b2;
            if (array2[i]) {
                j = b2 + 1;
            }
            ++i;
            b2 = j;
        }
        this.mInterpolateVariables = new int[b2];
        i = Math.max(2, b2);
        this.mInterpolateData = new double[i];
        this.mInterpolateVelocity = new double[i];
        int k = 1;
        i = 0;
        while (k < n4) {
            j = i;
            if (array2[k]) {
                this.mInterpolateVariables[i] = k;
                j = i + 1;
            }
            ++k;
            i = j;
        }
        i = this.mInterpolateVariables.length;
        final double[][] array3 = new double[n3][i];
        final double[] array4 = new double[n3];
        for (i = 0; i < n3; ++i) {
            array[i].fillStandard(array3[i], this.mInterpolateVariables);
            array4[i] = array[i].time;
        }
        i = 0;
        while (true) {
            final int[] mInterpolateVariables = this.mInterpolateVariables;
            if (i >= mInterpolateVariables.length) {
                break;
            }
            if (mInterpolateVariables[i] < MotionPaths.names.length) {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append(MotionPaths.names[this.mInterpolateVariables[i]]);
                sb2.append(" [");
                String str = sb2.toString();
                StringBuilder sb3;
                for (j = 0; j < n3; ++j) {
                    sb3 = new StringBuilder();
                    sb3.append(str);
                    sb3.append(array3[j][i]);
                    str = sb3.toString();
                }
            }
            ++i;
        }
        this.mSpline = new CurveFit[this.mAttributeNames.length + 1];
        i = 0;
        while (true) {
            final String[] mAttributeNames3 = this.mAttributeNames;
            if (i >= mAttributeNames3.length) {
                break;
            }
            final String s3 = mAttributeNames3[i];
            int l = 0;
            double[] original = null;
            j = 0;
            double[][] original2 = null;
            while (l < n3) {
                double[] array5 = original;
                int n5 = j;
                double[][] array6 = original2;
                if (array[l].hasCustomData(s3)) {
                    if ((array6 = original2) == null) {
                        original = new double[n3];
                        array6 = new double[n3][array[l].getCustomDataCount(s3)];
                    }
                    original[j] = array[l].time;
                    array[l].getCustomData(s3, array6[j], 0);
                    n5 = j + 1;
                    array5 = original;
                }
                ++l;
                original = array5;
                j = n5;
                original2 = array6;
            }
            final double[] copy = Arrays.copyOf(original, j);
            final double[][] array7 = Arrays.copyOf(original2, j);
            final CurveFit[] mSpline = this.mSpline;
            ++i;
            mSpline[i] = CurveFit.get(this.mCurveFitType, copy, array7);
        }
        this.mSpline[0] = CurveFit.get(this.mCurveFitType, array4, array3);
        if (array[0].mPathMotionArc != Key.UNSET) {
            final int[] array8 = new int[n3];
            final double[] array9 = new double[n3];
            final double[][] array10 = new double[n3][2];
            for (i = 0; i < n3; ++i) {
                array8[i] = array[i].mPathMotionArc;
                array9[i] = array[i].time;
                array10[i][0] = array[i].x;
                array10[i][1] = array[i].y;
            }
            this.mArcSpline = CurveFit.getArc(array8, array9, array10);
        }
        float v = Float.NaN;
        this.mCycleMap = new HashMap<String, ViewOscillator>();
        if (this.mKeyList != null) {
            for (final String s4 : set3) {
                final ViewOscillator spline = ViewOscillator.makeSpline(s4);
                if (spline == null) {
                    continue;
                }
                preCycleDistance = v;
                if (spline.variesByPath()) {
                    preCycleDistance = v;
                    if (Float.isNaN(v)) {
                        preCycleDistance = this.getPreCycleDistance();
                    }
                }
                spline.setType(s4);
                this.mCycleMap.put(s4, spline);
                v = preCycleDistance;
            }
            for (final Key key11 : this.mKeyList) {
                if (key11 instanceof KeyCycle) {
                    ((KeyCycle)key11).addCycleValues(this.mCycleMap);
                }
            }
            final Iterator<ViewOscillator> iterator14 = this.mCycleMap.values().iterator();
            while (iterator14.hasNext()) {
                iterator14.next().setup(v);
            }
        }
    }
    
    public void setupRelative(final MotionController motionController) {
        this.mStartMotionPath.setupRelative(motionController, motionController.mStartMotionPath);
        this.mEndMotionPath.setupRelative(motionController, motionController.mEndMotionPath);
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append(" start: x: ");
        sb.append(this.mStartMotionPath.x);
        sb.append(" y: ");
        sb.append(this.mStartMotionPath.y);
        sb.append(" end: x: ");
        sb.append(this.mEndMotionPath.x);
        sb.append(" y: ");
        sb.append(this.mEndMotionPath.y);
        return sb.toString();
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.motion.widget;

import java.util.Set;
import java.util.Iterator;
import androidx.constraintlayout.widget.ConstraintLayout;
import org.xmlpull.v1.XmlPullParserException;
import java.io.IOException;
import androidx.constraintlayout.widget.ConstraintAttribute;
import android.util.Xml;
import org.xmlpull.v1.XmlPullParser;
import android.content.Context;
import android.util.Log;
import java.util.ArrayList;
import java.lang.reflect.Constructor;
import java.util.HashMap;

public class KeyFrames
{
    private static final String CUSTOM_ATTRIBUTE = "CustomAttribute";
    private static final String CUSTOM_METHOD = "CustomMethod";
    private static final String TAG = "KeyFrames";
    public static final int UNSET = -1;
    static HashMap<String, Constructor<? extends Key>> sKeyMakers;
    private HashMap<Integer, ArrayList<Key>> mFramesMap;
    
    static {
        final HashMap<String, Constructor<? extends Key>> hashMap = KeyFrames.sKeyMakers = new HashMap<String, Constructor<? extends Key>>();
        try {
            hashMap.put("KeyAttribute", KeyAttributes.class.getConstructor((Class<?>[])new Class[0]));
            KeyFrames.sKeyMakers.put("KeyPosition", KeyPosition.class.getConstructor((Class<?>[])new Class[0]));
            KeyFrames.sKeyMakers.put("KeyCycle", KeyCycle.class.getConstructor((Class<?>[])new Class[0]));
            KeyFrames.sKeyMakers.put("KeyTimeCycle", KeyTimeCycle.class.getConstructor((Class<?>[])new Class[0]));
            KeyFrames.sKeyMakers.put("KeyTrigger", KeyTrigger.class.getConstructor((Class<?>[])new Class[0]));
        }
        catch (final NoSuchMethodException ex) {
            Log.e("KeyFrames", "unable to load", (Throwable)ex);
        }
    }
    
    public KeyFrames() {
        this.mFramesMap = new HashMap<Integer, ArrayList<Key>>();
    }
    
    public KeyFrames(final Context context, final XmlPullParser xmlPullParser) {
        this.mFramesMap = new HashMap<Integer, ArrayList<Key>>();
        Key key = null;
        try {
            Key key2;
            for (int i = xmlPullParser.getEventType(); i != 1; i = xmlPullParser.next(), key = key2) {
                if (i != 2) {
                    if (i != 3) {
                        key2 = key;
                    }
                    else {
                        key2 = key;
                        if ("KeyFrameSet".equals(xmlPullParser.getName())) {
                            return;
                        }
                    }
                }
                else {
                    final String name = xmlPullParser.getName();
                    if (KeyFrames.sKeyMakers.containsKey(name)) {
                        Label_0221: {
                            Exception ex = null;
                            Label_0211: {
                                try {
                                    final Constructor constructor = KeyFrames.sKeyMakers.get(name);
                                    if (constructor != null) {
                                        final Key key3 = (Key)constructor.newInstance(new Object[0]);
                                        try {
                                            key3.load(context, Xml.asAttributeSet(xmlPullParser));
                                            this.addKey(key3);
                                            key = key3;
                                            break Label_0221;
                                        }
                                        catch (final Exception ex) {
                                            key = key3;
                                            break Label_0211;
                                        }
                                    }
                                    final StringBuilder sb = new StringBuilder();
                                    sb.append("Keymaker for ");
                                    sb.append(name);
                                    sb.append(" not found");
                                    throw new NullPointerException(sb.toString());
                                }
                                catch (final Exception ex2) {
                                    ex = ex2;
                                }
                            }
                            Log.e("KeyFrames", "unable to create ", (Throwable)ex);
                        }
                        key2 = key;
                    }
                    else if (name.equalsIgnoreCase("CustomAttribute")) {
                        if ((key2 = key) != null) {
                            key2 = key;
                            if (key.mCustomConstraints != null) {
                                ConstraintAttribute.parse(context, xmlPullParser, key.mCustomConstraints);
                                key2 = key;
                            }
                        }
                    }
                    else {
                        key2 = key;
                        if (name.equalsIgnoreCase("CustomMethod") && (key2 = key) != null) {
                            key2 = key;
                            if (key.mCustomConstraints != null) {
                                ConstraintAttribute.parse(context, xmlPullParser, key.mCustomConstraints);
                                key2 = key;
                            }
                        }
                    }
                }
            }
        }
        catch (final IOException ex3) {
            ex3.printStackTrace();
        }
        catch (final XmlPullParserException ex4) {
            ex4.printStackTrace();
        }
    }
    
    static String name(final int n, final Context context) {
        return context.getResources().getResourceEntryName(n);
    }
    
    public void addAllFrames(final MotionController motionController) {
        final ArrayList list = this.mFramesMap.get(-1);
        if (list != null) {
            motionController.addKeys(list);
        }
    }
    
    public void addFrames(final MotionController motionController) {
        final ArrayList list = this.mFramesMap.get(motionController.mId);
        if (list != null) {
            motionController.addKeys(list);
        }
        final ArrayList list2 = this.mFramesMap.get(-1);
        if (list2 != null) {
            for (final Key key : list2) {
                if (key.matches(((ConstraintLayout.LayoutParams)motionController.mView.getLayoutParams()).constraintTag)) {
                    motionController.addKey(key);
                }
            }
        }
    }
    
    public void addKey(final Key e) {
        if (!this.mFramesMap.containsKey(e.mTargetId)) {
            this.mFramesMap.put(e.mTargetId, new ArrayList<Key>());
        }
        final ArrayList list = this.mFramesMap.get(e.mTargetId);
        if (list != null) {
            list.add(e);
        }
    }
    
    public ArrayList<Key> getKeyFramesForView(final int i) {
        return this.mFramesMap.get(i);
    }
    
    public Set<Integer> getKeys() {
        return this.mFramesMap.keySet();
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.motion.widget;

import android.view.VelocityTracker;
import android.view.ViewGroup$LayoutParams;
import androidx.constraintlayout.core.widgets.HelperWidget;
import androidx.constraintlayout.core.widgets.Placeholder;
import androidx.constraintlayout.core.widgets.Flow;
import androidx.constraintlayout.core.widgets.Guideline;
import androidx.constraintlayout.core.widgets.Helper;
import androidx.constraintlayout.core.widgets.VirtualLayout;
import androidx.constraintlayout.widget.Barrier;
import androidx.constraintlayout.widget.ConstraintHelper;
import android.view.View$MeasureSpec;
import androidx.constraintlayout.widget.Constraints;
import androidx.constraintlayout.core.widgets.ConstraintAnchor;
import android.widget.TextView;
import android.graphics.PathEffect;
import android.graphics.Paint$Style;
import android.graphics.Path;
import android.graphics.DashPathEffect;
import java.util.Arrays;
import android.view.Display;
import android.os.Build$VERSION;
import android.os.Bundle;
import android.graphics.Paint;
import android.graphics.Canvas;
import android.util.SparseBooleanArray;
import android.content.res.TypedArray;
import androidx.constraintlayout.widget.R;
import android.view.ViewGroup;
import androidx.constraintlayout.widget.ConstraintSet;
import java.util.Iterator;
import android.util.SparseIntArray;
import android.util.Log;
import android.view.MotionEvent;
import android.util.SparseArray;
import androidx.constraintlayout.core.widgets.ConstraintWidget;
import androidx.constraintlayout.core.widgets.ConstraintWidgetContainer;
import android.util.AttributeSet;
import android.content.Context;
import java.util.concurrent.CopyOnWriteArrayList;
import android.graphics.Rect;
import androidx.constraintlayout.motion.utils.StopLogic;
import androidx.constraintlayout.motion.utils.ViewState;
import androidx.constraintlayout.core.motion.utils.KeyCache;
import android.graphics.Matrix;
import android.view.animation.Interpolator;
import android.view.View;
import java.util.HashMap;
import java.util.ArrayList;
import android.graphics.RectF;
import androidx.core.view.NestedScrollingParent3;
import androidx.constraintlayout.widget.ConstraintLayout;

public class MotionLayout extends ConstraintLayout implements NestedScrollingParent3
{
    private static final boolean DEBUG = false;
    public static final int DEBUG_SHOW_NONE = 0;
    public static final int DEBUG_SHOW_PATH = 2;
    public static final int DEBUG_SHOW_PROGRESS = 1;
    private static final float EPSILON = 1.0E-5f;
    public static boolean IS_IN_EDIT_MODE = false;
    static final int MAX_KEY_FRAMES = 50;
    static final String TAG = "MotionLayout";
    public static final int TOUCH_UP_COMPLETE = 0;
    public static final int TOUCH_UP_COMPLETE_TO_END = 2;
    public static final int TOUCH_UP_COMPLETE_TO_START = 1;
    public static final int TOUCH_UP_DECELERATE = 4;
    public static final int TOUCH_UP_DECELERATE_AND_COMPLETE = 5;
    public static final int TOUCH_UP_NEVER_TO_END = 7;
    public static final int TOUCH_UP_NEVER_TO_START = 6;
    public static final int TOUCH_UP_STOP = 3;
    public static final int VELOCITY_LAYOUT = 1;
    public static final int VELOCITY_POST_LAYOUT = 0;
    public static final int VELOCITY_STATIC_LAYOUT = 3;
    public static final int VELOCITY_STATIC_POST_LAYOUT = 2;
    boolean firstDown;
    private float lastPos;
    private float lastY;
    private long mAnimationStartTime;
    private int mBeginState;
    private RectF mBoundsCheck;
    int mCurrentState;
    int mDebugPath;
    private DecelerateInterpolator mDecelerateLogic;
    private ArrayList<MotionHelper> mDecoratorsHelpers;
    private boolean mDelayedApply;
    private DesignTool mDesignTool;
    DevModeDraw mDevModeDraw;
    private int mEndState;
    int mEndWrapHeight;
    int mEndWrapWidth;
    HashMap<View, MotionController> mFrameArrayList;
    private int mFrames;
    int mHeightMeasureMode;
    private boolean mInLayout;
    private boolean mInRotation;
    boolean mInTransition;
    boolean mIndirectTransition;
    private boolean mInteractionEnabled;
    Interpolator mInterpolator;
    private Matrix mInverseMatrix;
    boolean mIsAnimating;
    private boolean mKeepAnimating;
    private KeyCache mKeyCache;
    private long mLastDrawTime;
    private float mLastFps;
    private int mLastHeightMeasureSpec;
    int mLastLayoutHeight;
    int mLastLayoutWidth;
    float mLastVelocity;
    private int mLastWidthMeasureSpec;
    private float mListenerPosition;
    private int mListenerState;
    protected boolean mMeasureDuringTransition;
    Model mModel;
    private boolean mNeedsFireTransitionCompleted;
    int mOldHeight;
    int mOldWidth;
    private Runnable mOnComplete;
    private ArrayList<MotionHelper> mOnHideHelpers;
    private ArrayList<MotionHelper> mOnShowHelpers;
    float mPostInterpolationPosition;
    HashMap<View, ViewState> mPreRotate;
    private int mPreRotateHeight;
    private int mPreRotateWidth;
    private int mPreviouseRotation;
    Interpolator mProgressInterpolator;
    private View mRegionView;
    int mRotatMode;
    MotionScene mScene;
    private int[] mScheduledTransitionTo;
    int mScheduledTransitions;
    float mScrollTargetDT;
    float mScrollTargetDX;
    float mScrollTargetDY;
    long mScrollTargetTime;
    int mStartWrapHeight;
    int mStartWrapWidth;
    private StateCache mStateCache;
    private StopLogic mStopLogic;
    Rect mTempRect;
    private boolean mTemporalInterpolator;
    ArrayList<Integer> mTransitionCompleted;
    private float mTransitionDuration;
    float mTransitionGoalPosition;
    private boolean mTransitionInstantly;
    float mTransitionLastPosition;
    private long mTransitionLastTime;
    private TransitionListener mTransitionListener;
    private CopyOnWriteArrayList<TransitionListener> mTransitionListeners;
    float mTransitionPosition;
    TransitionState mTransitionState;
    boolean mUndergoingMotion;
    int mWidthMeasureMode;
    
    public MotionLayout(final Context context) {
        super(context);
        this.mProgressInterpolator = null;
        this.mLastVelocity = 0.0f;
        this.mBeginState = -1;
        this.mCurrentState = -1;
        this.mEndState = -1;
        this.mLastWidthMeasureSpec = 0;
        this.mLastHeightMeasureSpec = 0;
        this.mInteractionEnabled = true;
        this.mFrameArrayList = new HashMap<View, MotionController>();
        this.mAnimationStartTime = 0L;
        this.mTransitionDuration = 1.0f;
        this.mTransitionPosition = 0.0f;
        this.mTransitionLastPosition = 0.0f;
        this.mTransitionGoalPosition = 0.0f;
        this.mInTransition = false;
        this.mIndirectTransition = false;
        this.mDebugPath = 0;
        this.mTemporalInterpolator = false;
        this.mStopLogic = new StopLogic();
        this.mDecelerateLogic = new DecelerateInterpolator();
        this.firstDown = true;
        this.mUndergoingMotion = false;
        this.mKeepAnimating = false;
        this.mOnShowHelpers = null;
        this.mOnHideHelpers = null;
        this.mDecoratorsHelpers = null;
        this.mTransitionListeners = null;
        this.mFrames = 0;
        this.mLastDrawTime = -1L;
        this.mLastFps = 0.0f;
        this.mListenerState = 0;
        this.mListenerPosition = 0.0f;
        this.mIsAnimating = false;
        this.mMeasureDuringTransition = false;
        this.mKeyCache = new KeyCache();
        this.mInLayout = false;
        this.mOnComplete = null;
        this.mScheduledTransitionTo = null;
        this.mScheduledTransitions = 0;
        this.mInRotation = false;
        this.mRotatMode = 0;
        this.mPreRotate = new HashMap<View, ViewState>();
        this.mTempRect = new Rect();
        this.mDelayedApply = false;
        this.mTransitionState = TransitionState.UNDEFINED;
        this.mModel = new Model();
        this.mNeedsFireTransitionCompleted = false;
        this.mBoundsCheck = new RectF();
        this.mRegionView = null;
        this.mInverseMatrix = null;
        this.mTransitionCompleted = new ArrayList<Integer>();
        this.init(null);
    }
    
    public MotionLayout(final Context context, final AttributeSet set) {
        super(context, set);
        this.mProgressInterpolator = null;
        this.mLastVelocity = 0.0f;
        this.mBeginState = -1;
        this.mCurrentState = -1;
        this.mEndState = -1;
        this.mLastWidthMeasureSpec = 0;
        this.mLastHeightMeasureSpec = 0;
        this.mInteractionEnabled = true;
        this.mFrameArrayList = new HashMap<View, MotionController>();
        this.mAnimationStartTime = 0L;
        this.mTransitionDuration = 1.0f;
        this.mTransitionPosition = 0.0f;
        this.mTransitionLastPosition = 0.0f;
        this.mTransitionGoalPosition = 0.0f;
        this.mInTransition = false;
        this.mIndirectTransition = false;
        this.mDebugPath = 0;
        this.mTemporalInterpolator = false;
        this.mStopLogic = new StopLogic();
        this.mDecelerateLogic = new DecelerateInterpolator();
        this.firstDown = true;
        this.mUndergoingMotion = false;
        this.mKeepAnimating = false;
        this.mOnShowHelpers = null;
        this.mOnHideHelpers = null;
        this.mDecoratorsHelpers = null;
        this.mTransitionListeners = null;
        this.mFrames = 0;
        this.mLastDrawTime = -1L;
        this.mLastFps = 0.0f;
        this.mListenerState = 0;
        this.mListenerPosition = 0.0f;
        this.mIsAnimating = false;
        this.mMeasureDuringTransition = false;
        this.mKeyCache = new KeyCache();
        this.mInLayout = false;
        this.mOnComplete = null;
        this.mScheduledTransitionTo = null;
        this.mScheduledTransitions = 0;
        this.mInRotation = false;
        this.mRotatMode = 0;
        this.mPreRotate = new HashMap<View, ViewState>();
        this.mTempRect = new Rect();
        this.mDelayedApply = false;
        this.mTransitionState = TransitionState.UNDEFINED;
        this.mModel = new Model();
        this.mNeedsFireTransitionCompleted = false;
        this.mBoundsCheck = new RectF();
        this.mRegionView = null;
        this.mInverseMatrix = null;
        this.mTransitionCompleted = new ArrayList<Integer>();
        this.init(set);
    }
    
    public MotionLayout(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
        this.mProgressInterpolator = null;
        this.mLastVelocity = 0.0f;
        this.mBeginState = -1;
        this.mCurrentState = -1;
        this.mEndState = -1;
        this.mLastWidthMeasureSpec = 0;
        this.mLastHeightMeasureSpec = 0;
        this.mInteractionEnabled = true;
        this.mFrameArrayList = new HashMap<View, MotionController>();
        this.mAnimationStartTime = 0L;
        this.mTransitionDuration = 1.0f;
        this.mTransitionPosition = 0.0f;
        this.mTransitionLastPosition = 0.0f;
        this.mTransitionGoalPosition = 0.0f;
        this.mInTransition = false;
        this.mIndirectTransition = false;
        this.mDebugPath = 0;
        this.mTemporalInterpolator = false;
        this.mStopLogic = new StopLogic();
        this.mDecelerateLogic = new DecelerateInterpolator();
        this.firstDown = true;
        this.mUndergoingMotion = false;
        this.mKeepAnimating = false;
        this.mOnShowHelpers = null;
        this.mOnHideHelpers = null;
        this.mDecoratorsHelpers = null;
        this.mTransitionListeners = null;
        this.mFrames = 0;
        this.mLastDrawTime = -1L;
        this.mLastFps = 0.0f;
        this.mListenerState = 0;
        this.mListenerPosition = 0.0f;
        this.mIsAnimating = false;
        this.mMeasureDuringTransition = false;
        this.mKeyCache = new KeyCache();
        this.mInLayout = false;
        this.mOnComplete = null;
        this.mScheduledTransitionTo = null;
        this.mScheduledTransitions = 0;
        this.mInRotation = false;
        this.mRotatMode = 0;
        this.mPreRotate = new HashMap<View, ViewState>();
        this.mTempRect = new Rect();
        this.mDelayedApply = false;
        this.mTransitionState = TransitionState.UNDEFINED;
        this.mModel = new Model();
        this.mNeedsFireTransitionCompleted = false;
        this.mBoundsCheck = new RectF();
        this.mRegionView = null;
        this.mInverseMatrix = null;
        this.mTransitionCompleted = new ArrayList<Integer>();
        this.init(set);
    }
    
    private boolean callTransformedTouchEvent(final View view, MotionEvent obtain, final float n, final float n2) {
        final Matrix matrix = view.getMatrix();
        if (matrix.isIdentity()) {
            obtain.offsetLocation(n, n2);
            final boolean onTouchEvent = view.onTouchEvent(obtain);
            obtain.offsetLocation(-n, -n2);
            return onTouchEvent;
        }
        obtain = MotionEvent.obtain(obtain);
        obtain.offsetLocation(n, n2);
        if (this.mInverseMatrix == null) {
            this.mInverseMatrix = new Matrix();
        }
        matrix.invert(this.mInverseMatrix);
        obtain.transform(this.mInverseMatrix);
        final boolean onTouchEvent2 = view.onTouchEvent(obtain);
        obtain.recycle();
        return onTouchEvent2;
    }
    
    private void checkStructure() {
        final MotionScene mScene = this.mScene;
        if (mScene == null) {
            Log.e("MotionLayout", "CHECK: motion scene not set! set \"app:layoutDescription=\"@xml/file\"");
            return;
        }
        final int startId = mScene.getStartId();
        final MotionScene mScene2 = this.mScene;
        this.checkStructure(startId, mScene2.getConstraintSet(mScene2.getStartId()));
        final SparseIntArray sparseIntArray = new SparseIntArray();
        final SparseIntArray sparseIntArray2 = new SparseIntArray();
        for (final MotionScene.Transition transition : this.mScene.getDefinedTransitions()) {
            if (transition == this.mScene.mCurrentTransition) {
                Log.v("MotionLayout", "CHECK: CURRENT");
            }
            this.checkStructure(transition);
            final int startConstraintSetId = transition.getStartConstraintSetId();
            final int endConstraintSetId = transition.getEndConstraintSetId();
            final String name = Debug.getName(this.getContext(), startConstraintSetId);
            final String name2 = Debug.getName(this.getContext(), endConstraintSetId);
            if (sparseIntArray.get(startConstraintSetId) == endConstraintSetId) {
                final StringBuilder sb = new StringBuilder();
                sb.append("CHECK: two transitions with the same start and end ");
                sb.append(name);
                sb.append("->");
                sb.append(name2);
                Log.e("MotionLayout", sb.toString());
            }
            if (sparseIntArray2.get(endConstraintSetId) == startConstraintSetId) {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("CHECK: you can't have reverse transitions");
                sb2.append(name);
                sb2.append("->");
                sb2.append(name2);
                Log.e("MotionLayout", sb2.toString());
            }
            sparseIntArray.put(startConstraintSetId, endConstraintSetId);
            sparseIntArray2.put(endConstraintSetId, startConstraintSetId);
            if (this.mScene.getConstraintSet(startConstraintSetId) == null) {
                final StringBuilder sb3 = new StringBuilder();
                sb3.append(" no such constraintSetStart ");
                sb3.append(name);
                Log.e("MotionLayout", sb3.toString());
            }
            if (this.mScene.getConstraintSet(endConstraintSetId) == null) {
                final StringBuilder sb4 = new StringBuilder();
                sb4.append(" no such constraintSetEnd ");
                sb4.append(name);
                Log.e("MotionLayout", sb4.toString());
            }
        }
    }
    
    private void checkStructure(int i, final ConstraintSet set) {
        final String name = Debug.getName(this.getContext(), i);
        final int childCount = this.getChildCount();
        final int n = 0;
        View child;
        int id;
        StringBuilder sb;
        StringBuilder sb2;
        for (i = 0; i < childCount; ++i) {
            child = this.getChildAt(i);
            id = child.getId();
            if (id == -1) {
                sb = new StringBuilder();
                sb.append("CHECK: ");
                sb.append(name);
                sb.append(" ALL VIEWS SHOULD HAVE ID's ");
                sb.append(child.getClass().getName());
                sb.append(" does not!");
                Log.w("MotionLayout", sb.toString());
            }
            if (set.getConstraint(id) == null) {
                sb2 = new StringBuilder();
                sb2.append("CHECK: ");
                sb2.append(name);
                sb2.append(" NO CONSTRAINTS for ");
                sb2.append(Debug.getName(child));
                Log.w("MotionLayout", sb2.toString());
            }
        }
        int[] knownIds;
        int n2;
        String name2;
        StringBuilder sb3;
        StringBuilder sb4;
        StringBuilder sb5;
        for (knownIds = set.getKnownIds(), i = n; i < knownIds.length; ++i) {
            n2 = knownIds[i];
            name2 = Debug.getName(this.getContext(), n2);
            if (this.findViewById(knownIds[i]) == null) {
                sb3 = new StringBuilder();
                sb3.append("CHECK: ");
                sb3.append(name);
                sb3.append(" NO View matches id ");
                sb3.append(name2);
                Log.w("MotionLayout", sb3.toString());
            }
            if (set.getHeight(n2) == -1) {
                sb4 = new StringBuilder();
                sb4.append("CHECK: ");
                sb4.append(name);
                sb4.append("(");
                sb4.append(name2);
                sb4.append(") no LAYOUT_HEIGHT");
                Log.w("MotionLayout", sb4.toString());
            }
            if (set.getWidth(n2) == -1) {
                sb5 = new StringBuilder();
                sb5.append("CHECK: ");
                sb5.append(name);
                sb5.append("(");
                sb5.append(name2);
                sb5.append(") no LAYOUT_HEIGHT");
                Log.w("MotionLayout", sb5.toString());
            }
        }
    }
    
    private void checkStructure(final MotionScene.Transition transition) {
        if (transition.getStartConstraintSetId() == transition.getEndConstraintSetId()) {
            Log.e("MotionLayout", "CHECK: start and end constraint set should not be the same!");
        }
    }
    
    private void computeCurrentPositions() {
        for (int childCount = this.getChildCount(), i = 0; i < childCount; ++i) {
            final View child = this.getChildAt(i);
            final MotionController motionController = this.mFrameArrayList.get(child);
            if (motionController != null) {
                motionController.setStartCurrentState(child);
            }
        }
    }
    
    private void debugPos() {
        for (int i = 0; i < this.getChildCount(); ++i) {
            final View child = this.getChildAt(i);
            final StringBuilder sb = new StringBuilder();
            sb.append(" ");
            sb.append(Debug.getLocation());
            sb.append(" ");
            sb.append(Debug.getName((View)this));
            sb.append(" ");
            sb.append(Debug.getName(this.getContext(), this.mCurrentState));
            sb.append(" ");
            sb.append(Debug.getName(child));
            sb.append(child.getLeft());
            sb.append(" ");
            sb.append(child.getTop());
            Log.v("MotionLayout", sb.toString());
        }
    }
    
    private void evaluateLayout() {
        final float signum = Math.signum(this.mTransitionGoalPosition - this.mTransitionLastPosition);
        final long nanoTime = this.getNanoTime();
        final Interpolator mInterpolator = this.mInterpolator;
        float n;
        if (!(mInterpolator instanceof StopLogic)) {
            n = (nanoTime - this.mTransitionLastTime) * signum * 1.0E-9f / this.mTransitionDuration;
        }
        else {
            n = 0.0f;
        }
        float n2 = this.mTransitionLastPosition + n;
        if (this.mTransitionInstantly) {
            n2 = this.mTransitionGoalPosition;
        }
        final int n3 = 0;
        final float n4 = fcmpl(signum, 0.0f);
        boolean b;
        if ((n4 > 0 && n2 >= this.mTransitionGoalPosition) || (signum <= 0.0f && n2 <= this.mTransitionGoalPosition)) {
            n2 = this.mTransitionGoalPosition;
            b = true;
        }
        else {
            b = false;
        }
        float n5 = n2;
        if (mInterpolator != null) {
            n5 = n2;
            if (!b) {
                if (this.mTemporalInterpolator) {
                    n5 = mInterpolator.getInterpolation((nanoTime - this.mAnimationStartTime) * 1.0E-9f);
                }
                else {
                    n5 = mInterpolator.getInterpolation(n2);
                }
            }
        }
        float mPostInterpolationPosition = 0.0f;
        Label_0219: {
            if (n4 <= 0 || n5 < this.mTransitionGoalPosition) {
                mPostInterpolationPosition = n5;
                if (signum > 0.0f) {
                    break Label_0219;
                }
                mPostInterpolationPosition = n5;
                if (n5 > this.mTransitionGoalPosition) {
                    break Label_0219;
                }
            }
            mPostInterpolationPosition = this.mTransitionGoalPosition;
        }
        this.mPostInterpolationPosition = mPostInterpolationPosition;
        final int childCount = this.getChildCount();
        final long nanoTime2 = this.getNanoTime();
        final Interpolator mProgressInterpolator = this.mProgressInterpolator;
        int i;
        if (mProgressInterpolator == null) {
            i = n3;
        }
        else {
            mPostInterpolationPosition = mProgressInterpolator.getInterpolation(mPostInterpolationPosition);
            i = n3;
        }
        while (i < childCount) {
            final View child = this.getChildAt(i);
            final MotionController motionController = this.mFrameArrayList.get(child);
            if (motionController != null) {
                motionController.interpolate(child, mPostInterpolationPosition, nanoTime2, this.mKeyCache);
            }
            ++i;
        }
        if (this.mMeasureDuringTransition) {
            this.requestLayout();
        }
    }
    
    private void fireTransitionChange() {
        if (this.mTransitionListener == null) {
            final CopyOnWriteArrayList<TransitionListener> mTransitionListeners = this.mTransitionListeners;
            if (mTransitionListeners == null || mTransitionListeners.isEmpty()) {
                return;
            }
        }
        if (this.mListenerPosition != this.mTransitionPosition) {
            if (this.mListenerState != -1) {
                final TransitionListener mTransitionListener = this.mTransitionListener;
                if (mTransitionListener != null) {
                    mTransitionListener.onTransitionStarted(this, this.mBeginState, this.mEndState);
                }
                final CopyOnWriteArrayList<TransitionListener> mTransitionListeners2 = this.mTransitionListeners;
                if (mTransitionListeners2 != null) {
                    final Iterator<TransitionListener> iterator = mTransitionListeners2.iterator();
                    while (iterator.hasNext()) {
                        iterator.next().onTransitionStarted(this, this.mBeginState, this.mEndState);
                    }
                }
                this.mIsAnimating = true;
            }
            this.mListenerState = -1;
            final float mTransitionPosition = this.mTransitionPosition;
            this.mListenerPosition = mTransitionPosition;
            final TransitionListener mTransitionListener2 = this.mTransitionListener;
            if (mTransitionListener2 != null) {
                mTransitionListener2.onTransitionChange(this, this.mBeginState, this.mEndState, mTransitionPosition);
            }
            final CopyOnWriteArrayList<TransitionListener> mTransitionListeners3 = this.mTransitionListeners;
            if (mTransitionListeners3 != null) {
                final Iterator<TransitionListener> iterator2 = mTransitionListeners3.iterator();
                while (iterator2.hasNext()) {
                    iterator2.next().onTransitionChange(this, this.mBeginState, this.mEndState, this.mTransitionPosition);
                }
            }
            this.mIsAnimating = true;
        }
    }
    
    private void fireTransitionStarted(final MotionLayout motionLayout, final int n, final int n2) {
        final TransitionListener mTransitionListener = this.mTransitionListener;
        if (mTransitionListener != null) {
            mTransitionListener.onTransitionStarted(this, n, n2);
        }
        final CopyOnWriteArrayList<TransitionListener> mTransitionListeners = this.mTransitionListeners;
        if (mTransitionListeners != null) {
            final Iterator<TransitionListener> iterator = mTransitionListeners.iterator();
            while (iterator.hasNext()) {
                iterator.next().onTransitionStarted(motionLayout, n, n2);
            }
        }
    }
    
    private boolean handlesTouchEvent(final float n, final float n2, final View view, final MotionEvent motionEvent) {
        final boolean b = view instanceof ViewGroup;
        final boolean b2 = true;
        boolean b3 = false;
        Label_0097: {
            if (b) {
                final ViewGroup viewGroup = (ViewGroup)view;
                for (int i = viewGroup.getChildCount() - 1; i >= 0; --i) {
                    final View child = viewGroup.getChildAt(i);
                    if (this.handlesTouchEvent(child.getLeft() + n - view.getScrollX(), child.getTop() + n2 - view.getScrollY(), child, motionEvent)) {
                        b3 = true;
                        break Label_0097;
                    }
                }
            }
            b3 = false;
        }
        if (!b3) {
            this.mBoundsCheck.set(n, n2, view.getRight() + n - view.getLeft(), view.getBottom() + n2 - view.getTop());
            if ((motionEvent.getAction() != 0 || this.mBoundsCheck.contains(motionEvent.getX(), motionEvent.getY())) && this.callTransformedTouchEvent(view, motionEvent, -n, -n2)) {
                b3 = b2;
            }
        }
        return b3;
    }
    
    private void init(final AttributeSet set) {
        MotionLayout.IS_IN_EDIT_MODE = this.isInEditMode();
        if (set != null) {
            final TypedArray obtainStyledAttributes = this.getContext().obtainStyledAttributes(set, R.styleable.MotionLayout);
            final int indexCount = obtainStyledAttributes.getIndexCount();
            int i = 0;
            int n = 1;
            while (i < indexCount) {
                final int index = obtainStyledAttributes.getIndex(i);
                int boolean1;
                if (index == R.styleable.MotionLayout_layoutDescription) {
                    this.mScene = new MotionScene(this.getContext(), this, obtainStyledAttributes.getResourceId(index, -1));
                    boolean1 = n;
                }
                else if (index == R.styleable.MotionLayout_currentState) {
                    this.mCurrentState = obtainStyledAttributes.getResourceId(index, -1);
                    boolean1 = n;
                }
                else if (index == R.styleable.MotionLayout_motionProgress) {
                    this.mTransitionGoalPosition = obtainStyledAttributes.getFloat(index, 0.0f);
                    this.mInTransition = true;
                    boolean1 = n;
                }
                else if (index == R.styleable.MotionLayout_applyMotionScene) {
                    boolean1 = (obtainStyledAttributes.getBoolean(index, (boolean)(n != 0)) ? 1 : 0);
                }
                else if (index == R.styleable.MotionLayout_showPaths) {
                    boolean1 = n;
                    if (this.mDebugPath == 0) {
                        int mDebugPath;
                        if (obtainStyledAttributes.getBoolean(index, false)) {
                            mDebugPath = 2;
                        }
                        else {
                            mDebugPath = 0;
                        }
                        this.mDebugPath = mDebugPath;
                        boolean1 = n;
                    }
                }
                else {
                    boolean1 = n;
                    if (index == R.styleable.MotionLayout_motionDebug) {
                        this.mDebugPath = obtainStyledAttributes.getInt(index, 0);
                        boolean1 = n;
                    }
                }
                ++i;
                n = boolean1;
            }
            obtainStyledAttributes.recycle();
            if (this.mScene == null) {
                Log.e("MotionLayout", "WARNING NO app:layoutDescription tag");
            }
            if (n == 0) {
                this.mScene = null;
            }
        }
        if (this.mDebugPath != 0) {
            this.checkStructure();
        }
        if (this.mCurrentState == -1) {
            final MotionScene mScene = this.mScene;
            if (mScene != null) {
                this.mCurrentState = mScene.getStartId();
                this.mBeginState = this.mScene.getStartId();
                this.mEndState = this.mScene.getEndId();
            }
        }
    }
    
    private void processTransitionCompleted() {
        if (this.mTransitionListener == null) {
            final CopyOnWriteArrayList<TransitionListener> mTransitionListeners = this.mTransitionListeners;
            if (mTransitionListeners == null || mTransitionListeners.isEmpty()) {
                return;
            }
        }
        this.mIsAnimating = false;
        for (final Integer n : this.mTransitionCompleted) {
            final TransitionListener mTransitionListener = this.mTransitionListener;
            if (mTransitionListener != null) {
                mTransitionListener.onTransitionCompleted(this, n);
            }
            final CopyOnWriteArrayList<TransitionListener> mTransitionListeners2 = this.mTransitionListeners;
            if (mTransitionListeners2 != null) {
                final Iterator<TransitionListener> iterator2 = mTransitionListeners2.iterator();
                while (iterator2.hasNext()) {
                    iterator2.next().onTransitionCompleted(this, n);
                }
            }
        }
        this.mTransitionCompleted.clear();
    }
    
    private void setupMotionViews() {
        final int childCount = this.getChildCount();
        this.mModel.build();
        final int n = 1;
        this.mInTransition = true;
        final SparseArray sparseArray = new SparseArray();
        final int n2 = 0;
        final int n3 = 0;
        for (int i = 0; i < childCount; ++i) {
            final View child = this.getChildAt(i);
            sparseArray.put(child.getId(), (Object)this.mFrameArrayList.get(child));
        }
        final int width = this.getWidth();
        final int height = this.getHeight();
        final int gatPathMotionArc = this.mScene.gatPathMotionArc();
        if (gatPathMotionArc != -1) {
            for (int j = 0; j < childCount; ++j) {
                final MotionController motionController = this.mFrameArrayList.get(this.getChildAt(j));
                if (motionController != null) {
                    motionController.setPathMotionArc(gatPathMotionArc);
                }
            }
        }
        final SparseBooleanArray sparseBooleanArray = new SparseBooleanArray();
        final int[] array = new int[this.mFrameArrayList.size()];
        int k = 0;
        int n4 = 0;
        while (k < childCount) {
            final MotionController motionController2 = this.mFrameArrayList.get(this.getChildAt(k));
            int n5 = n4;
            if (motionController2.getAnimateRelativeTo() != -1) {
                sparseBooleanArray.put(motionController2.getAnimateRelativeTo(), true);
                array[n4] = motionController2.getAnimateRelativeTo();
                n5 = n4 + 1;
            }
            ++k;
            n4 = n5;
        }
        if (this.mDecoratorsHelpers != null) {
            for (int l = 0; l < n4; ++l) {
                final MotionController motionController3 = this.mFrameArrayList.get(this.findViewById(array[l]));
                if (motionController3 != null) {
                    this.mScene.getKeyFrames(motionController3);
                }
            }
            final Iterator<MotionHelper> iterator = this.mDecoratorsHelpers.iterator();
            while (iterator.hasNext()) {
                iterator.next().onPreSetup(this, this.mFrameArrayList);
            }
            for (int n6 = 0; n6 < n4; ++n6) {
                final MotionController motionController4 = this.mFrameArrayList.get(this.findViewById(array[n6]));
                if (motionController4 != null) {
                    motionController4.setup(width, height, this.mTransitionDuration, this.getNanoTime());
                }
            }
        }
        else {
            for (int n7 = 0; n7 < n4; ++n7) {
                final MotionController motionController5 = this.mFrameArrayList.get(this.findViewById(array[n7]));
                if (motionController5 != null) {
                    this.mScene.getKeyFrames(motionController5);
                    motionController5.setup(width, height, this.mTransitionDuration, this.getNanoTime());
                }
            }
        }
        for (int n8 = 0; n8 < childCount; ++n8) {
            final View child2 = this.getChildAt(n8);
            final MotionController motionController6 = this.mFrameArrayList.get(child2);
            if (!sparseBooleanArray.get(child2.getId())) {
                if (motionController6 != null) {
                    this.mScene.getKeyFrames(motionController6);
                    motionController6.setup(width, height, this.mTransitionDuration, this.getNanoTime());
                }
            }
        }
        final float staggered = this.mScene.getStaggered();
        if (staggered != 0.0f) {
            final boolean b = staggered < 0.0;
            final float abs = Math.abs(staggered);
            final float n9 = -3.4028235E38f;
            final float n10 = Float.MAX_VALUE;
            int n11 = 0;
            float min = Float.MAX_VALUE;
            float max = -3.4028235E38f;
            while (true) {
                while (n11 < childCount) {
                    final MotionController motionController7 = this.mFrameArrayList.get(this.getChildAt(n11));
                    if (!Float.isNaN(motionController7.mMotionStagger)) {
                        final int n12 = n;
                        int n13 = n2;
                        if (n12 != 0) {
                            int n14 = 0;
                            float a = n10;
                            float a2 = n9;
                            int n15;
                            while (true) {
                                n15 = n3;
                                if (n14 >= childCount) {
                                    break;
                                }
                                final MotionController motionController8 = this.mFrameArrayList.get(this.getChildAt(n14));
                                float max2 = a2;
                                float min2 = a;
                                if (!Float.isNaN(motionController8.mMotionStagger)) {
                                    min2 = Math.min(a, motionController8.mMotionStagger);
                                    max2 = Math.max(a2, motionController8.mMotionStagger);
                                }
                                ++n14;
                                a2 = max2;
                                a = min2;
                            }
                            while (n15 < childCount) {
                                final MotionController motionController9 = this.mFrameArrayList.get(this.getChildAt(n15));
                                if (!Float.isNaN(motionController9.mMotionStagger)) {
                                    motionController9.mStaggerScale = 1.0f / (1.0f - abs);
                                    if (b) {
                                        motionController9.mStaggerOffset = abs - (a2 - motionController9.mMotionStagger) / (a2 - a) * abs;
                                    }
                                    else {
                                        motionController9.mStaggerOffset = abs - (motionController9.mMotionStagger - a) * abs / (a2 - a);
                                    }
                                }
                                ++n15;
                            }
                            return;
                        }
                        while (n13 < childCount) {
                            final MotionController motionController10 = this.mFrameArrayList.get(this.getChildAt(n13));
                            final float finalX = motionController10.getFinalX();
                            final float finalY = motionController10.getFinalY();
                            float n16;
                            if (b) {
                                n16 = finalY - finalX;
                            }
                            else {
                                n16 = finalY + finalX;
                            }
                            motionController10.mStaggerScale = 1.0f / (1.0f - abs);
                            motionController10.mStaggerOffset = abs - (n16 - min) * abs / (max - min);
                            ++n13;
                        }
                        return;
                    }
                    else {
                        final float finalX2 = motionController7.getFinalX();
                        final float finalY2 = motionController7.getFinalY();
                        float n17;
                        if (b) {
                            n17 = finalY2 - finalX2;
                        }
                        else {
                            n17 = finalY2 + finalX2;
                        }
                        min = Math.min(min, n17);
                        max = Math.max(max, n17);
                        ++n11;
                    }
                }
                final int n12 = 0;
                continue;
            }
        }
    }
    
    private Rect toRect(final ConstraintWidget constraintWidget) {
        this.mTempRect.top = constraintWidget.getY();
        this.mTempRect.left = constraintWidget.getX();
        this.mTempRect.right = constraintWidget.getWidth() + this.mTempRect.left;
        this.mTempRect.bottom = constraintWidget.getHeight() + this.mTempRect.top;
        return this.mTempRect;
    }
    
    private static boolean willJump(final float n, final float n2, final float n3) {
        boolean b = true;
        final boolean b2 = true;
        if (n > 0.0f) {
            final float n4 = n / n3;
            return n2 + (n * n4 - n3 * n4 * n4 / 2.0f) > 1.0f && b2;
        }
        final float n5 = -n / n3;
        if (n2 + (n * n5 + n3 * n5 * n5 / 2.0f) >= 0.0f) {
            b = false;
        }
        return b;
    }
    
    public void addTransitionListener(final TransitionListener e) {
        if (this.mTransitionListeners == null) {
            this.mTransitionListeners = new CopyOnWriteArrayList<TransitionListener>();
        }
        this.mTransitionListeners.add(e);
    }
    
    void animateTo(final float mTransitionGoalPosition) {
        final MotionScene mScene = this.mScene;
        if (mScene == null) {
            return;
        }
        final float mTransitionLastPosition = this.mTransitionLastPosition;
        final float mTransitionPosition = this.mTransitionPosition;
        if (mTransitionLastPosition != mTransitionPosition && this.mTransitionInstantly) {
            this.mTransitionLastPosition = mTransitionPosition;
        }
        final float mTransitionLastPosition2 = this.mTransitionLastPosition;
        if (mTransitionLastPosition2 == mTransitionGoalPosition) {
            return;
        }
        this.mTemporalInterpolator = false;
        this.mTransitionGoalPosition = mTransitionGoalPosition;
        this.mTransitionDuration = mScene.getDuration() / 1000.0f;
        this.setProgress(this.mTransitionGoalPosition);
        this.mInterpolator = null;
        this.mProgressInterpolator = this.mScene.getInterpolator();
        this.mTransitionInstantly = false;
        this.mAnimationStartTime = this.getNanoTime();
        this.mInTransition = true;
        this.mTransitionPosition = mTransitionLastPosition2;
        this.mTransitionLastPosition = mTransitionLastPosition2;
        this.invalidate();
    }
    
    public boolean applyViewTransition(final int n, final MotionController motionController) {
        final MotionScene mScene = this.mScene;
        return mScene != null && mScene.applyViewTransition(n, motionController);
    }
    
    public ConstraintSet cloneConstraintSet(final int n) {
        final MotionScene mScene = this.mScene;
        if (mScene == null) {
            return null;
        }
        final ConstraintSet constraintSet = mScene.getConstraintSet(n);
        final ConstraintSet set = new ConstraintSet();
        set.clone(constraintSet);
        return set;
    }
    
    void disableAutoTransition(final boolean b) {
        final MotionScene mScene = this.mScene;
        if (mScene == null) {
            return;
        }
        mScene.disableAutoTransition(b);
    }
    
    @Override
    protected void dispatchDraw(final Canvas canvas) {
        final ArrayList<MotionHelper> mDecoratorsHelpers = this.mDecoratorsHelpers;
        if (mDecoratorsHelpers != null) {
            final Iterator<MotionHelper> iterator = mDecoratorsHelpers.iterator();
            while (iterator.hasNext()) {
                iterator.next().onPreDraw(canvas);
            }
        }
        this.evaluate(false);
        final MotionScene mScene = this.mScene;
        if (mScene != null && mScene.mViewTransitionController != null) {
            this.mScene.mViewTransitionController.animate();
        }
        super.dispatchDraw(canvas);
        if (this.mScene == null) {
            return;
        }
        if ((this.mDebugPath & 0x1) == 0x1 && !this.isInEditMode()) {
            ++this.mFrames;
            final long nanoTime = this.getNanoTime();
            final long mLastDrawTime = this.mLastDrawTime;
            if (mLastDrawTime != -1L) {
                final long n = nanoTime - mLastDrawTime;
                if (n > 200000000L) {
                    this.mLastFps = (int)(this.mFrames / (n * 1.0E-9f) * 100.0f) / 100.0f;
                    this.mFrames = 0;
                    this.mLastDrawTime = nanoTime;
                }
            }
            else {
                this.mLastDrawTime = nanoTime;
            }
            final Paint paint = new Paint();
            paint.setTextSize(42.0f);
            final float f = (int)(this.getProgress() * 1000.0f) / 10.0f;
            final StringBuilder sb = new StringBuilder();
            sb.append(this.mLastFps);
            sb.append(" fps ");
            sb.append(Debug.getState(this, this.mBeginState));
            sb.append(" -> ");
            final String string = sb.toString();
            final StringBuilder sb2 = new StringBuilder();
            sb2.append(string);
            sb2.append(Debug.getState(this, this.mEndState));
            sb2.append(" (progress: ");
            sb2.append(f);
            sb2.append(" ) state=");
            final int mCurrentState = this.mCurrentState;
            String state;
            if (mCurrentState == -1) {
                state = "undefined";
            }
            else {
                state = Debug.getState(this, mCurrentState);
            }
            sb2.append(state);
            final String string2 = sb2.toString();
            paint.setColor(-16777216);
            canvas.drawText(string2, 11.0f, (float)(this.getHeight() - 29), paint);
            paint.setColor(-7864184);
            canvas.drawText(string2, 10.0f, (float)(this.getHeight() - 30), paint);
        }
        if (this.mDebugPath > 1) {
            if (this.mDevModeDraw == null) {
                this.mDevModeDraw = new DevModeDraw();
            }
            this.mDevModeDraw.draw(canvas, this.mFrameArrayList, this.mScene.getDuration(), this.mDebugPath);
        }
        final ArrayList<MotionHelper> mDecoratorsHelpers2 = this.mDecoratorsHelpers;
        if (mDecoratorsHelpers2 != null) {
            final Iterator<MotionHelper> iterator2 = mDecoratorsHelpers2.iterator();
            while (iterator2.hasNext()) {
                iterator2.next().onPostDraw(canvas);
            }
        }
    }
    
    public void enableTransition(final int n, final boolean b) {
        final MotionScene.Transition transition = this.getTransition(n);
        if (b) {
            transition.setEnabled(true);
            return;
        }
        if (transition == this.mScene.mCurrentTransition) {
            for (final MotionScene.Transition mCurrentTransition : this.mScene.getTransitionsWithState(this.mCurrentState)) {
                if (mCurrentTransition.isEnabled()) {
                    this.mScene.mCurrentTransition = mCurrentTransition;
                    break;
                }
            }
        }
        transition.setEnabled(false);
    }
    
    public void enableViewTransition(final int n, final boolean b) {
        final MotionScene mScene = this.mScene;
        if (mScene != null) {
            mScene.enableViewTransition(n, b);
        }
    }
    
    void endTrigger(final boolean b) {
        for (int childCount = this.getChildCount(), i = 0; i < childCount; ++i) {
            final MotionController motionController = this.mFrameArrayList.get(this.getChildAt(i));
            if (motionController != null) {
                motionController.endTrigger(b);
            }
        }
    }
    
    void evaluate(final boolean b) {
        if (this.mTransitionLastTime == -1L) {
            this.mTransitionLastTime = this.getNanoTime();
        }
        final float mTransitionLastPosition = this.mTransitionLastPosition;
        if (mTransitionLastPosition > 0.0f && mTransitionLastPosition < 1.0f) {
            this.mCurrentState = -1;
        }
        final boolean mKeepAnimating = this.mKeepAnimating;
        final boolean b2 = true;
        final boolean b3 = true;
        final boolean b4 = false;
        final boolean b5 = false;
        boolean b6 = false;
        Label_1146: {
            if (!mKeepAnimating) {
                b6 = b4;
                if (!this.mInTransition) {
                    break Label_1146;
                }
                if (!b) {
                    b6 = b4;
                    if (this.mTransitionGoalPosition == mTransitionLastPosition) {
                        break Label_1146;
                    }
                }
            }
            final float signum = Math.signum(this.mTransitionGoalPosition - mTransitionLastPosition);
            final long nanoTime = this.getNanoTime();
            final Interpolator mInterpolator = this.mInterpolator;
            float mLastVelocity;
            if (!(mInterpolator instanceof MotionInterpolator)) {
                mLastVelocity = (nanoTime - this.mTransitionLastTime) * signum * 1.0E-9f / this.mTransitionDuration;
            }
            else {
                mLastVelocity = 0.0f;
            }
            float n = this.mTransitionLastPosition + mLastVelocity;
            if (this.mTransitionInstantly) {
                n = this.mTransitionGoalPosition;
            }
            final float n2 = fcmpl(signum, 0.0f);
            boolean b7;
            if ((n2 > 0 && n >= this.mTransitionGoalPosition) || (signum <= 0.0f && n <= this.mTransitionGoalPosition)) {
                n = this.mTransitionGoalPosition;
                this.mInTransition = false;
                b7 = true;
            }
            else {
                b7 = false;
            }
            this.mTransitionLastPosition = n;
            this.mTransitionPosition = n;
            this.mTransitionLastTime = nanoTime;
            int n3 = 0;
            Label_0535: {
                if (mInterpolator != null && !b7) {
                    if (this.mTemporalInterpolator) {
                        final float interpolation = mInterpolator.getInterpolation((nanoTime - this.mAnimationStartTime) * 1.0E-9f);
                        final Interpolator mInterpolator2 = this.mInterpolator;
                        final StopLogic mStopLogic = this.mStopLogic;
                        if (mInterpolator2 == mStopLogic) {
                            if (mStopLogic.isStopped()) {
                                n3 = 2;
                            }
                            else {
                                n3 = 1;
                            }
                        }
                        else {
                            n3 = 0;
                        }
                        this.mTransitionLastPosition = interpolation;
                        this.mTransitionLastTime = nanoTime;
                        final Interpolator mInterpolator3 = this.mInterpolator;
                        float n4 = interpolation;
                        if (mInterpolator3 instanceof MotionInterpolator) {
                            final float velocity = ((MotionInterpolator)mInterpolator3).getVelocity();
                            this.mLastVelocity = velocity;
                            if (Math.abs(velocity) * this.mTransitionDuration <= 1.0E-5f && n3 == 2) {
                                this.mInTransition = false;
                            }
                            float n5 = interpolation;
                            if (velocity > 0.0f) {
                                n5 = interpolation;
                                if (interpolation >= 1.0f) {
                                    this.mTransitionLastPosition = 1.0f;
                                    this.mInTransition = false;
                                    n5 = 1.0f;
                                }
                            }
                            n4 = n5;
                            if (velocity < 0.0f) {
                                n4 = n5;
                                if (n5 <= 0.0f) {
                                    this.mTransitionLastPosition = 0.0f;
                                    this.mInTransition = false;
                                    n = 0.0f;
                                    break Label_0535;
                                }
                            }
                        }
                        n = n4;
                        break Label_0535;
                    }
                    final float interpolation2 = mInterpolator.getInterpolation(n);
                    final Interpolator mInterpolator4 = this.mInterpolator;
                    if (mInterpolator4 instanceof MotionInterpolator) {
                        this.mLastVelocity = ((MotionInterpolator)mInterpolator4).getVelocity();
                    }
                    else {
                        this.mLastVelocity = (mInterpolator4.getInterpolation(n + mLastVelocity) - interpolation2) * signum / mLastVelocity;
                    }
                    n = interpolation2;
                }
                else {
                    this.mLastVelocity = mLastVelocity;
                }
                n3 = 0;
            }
            if (Math.abs(this.mLastVelocity) > 1.0E-5f) {
                this.setState(TransitionState.MOVING);
            }
            float mPostInterpolationPosition = n;
            Label_0642: {
                if (n3 != 1) {
                    float mTransitionGoalPosition = 0.0f;
                    Label_0610: {
                        if (n2 <= 0 || n < this.mTransitionGoalPosition) {
                            mTransitionGoalPosition = n;
                            if (signum > 0.0f) {
                                break Label_0610;
                            }
                            mTransitionGoalPosition = n;
                            if (n > this.mTransitionGoalPosition) {
                                break Label_0610;
                            }
                        }
                        mTransitionGoalPosition = this.mTransitionGoalPosition;
                        this.mInTransition = false;
                    }
                    if (mTransitionGoalPosition < 1.0f) {
                        mPostInterpolationPosition = mTransitionGoalPosition;
                        if (mTransitionGoalPosition > 0.0f) {
                            break Label_0642;
                        }
                    }
                    this.mInTransition = false;
                    this.setState(TransitionState.FINISHED);
                    mPostInterpolationPosition = mTransitionGoalPosition;
                }
            }
            final int childCount = this.getChildCount();
            this.mKeepAnimating = false;
            final long nanoTime2 = this.getNanoTime();
            this.mPostInterpolationPosition = mPostInterpolationPosition;
            final Interpolator mProgressInterpolator = this.mProgressInterpolator;
            float interpolation3;
            if (mProgressInterpolator == null) {
                interpolation3 = mPostInterpolationPosition;
            }
            else {
                interpolation3 = mProgressInterpolator.getInterpolation(mPostInterpolationPosition);
            }
            final Interpolator mProgressInterpolator2 = this.mProgressInterpolator;
            if (mProgressInterpolator2 != null) {
                final float interpolation4 = mProgressInterpolator2.getInterpolation(signum / this.mTransitionDuration + mPostInterpolationPosition);
                this.mLastVelocity = interpolation4;
                this.mLastVelocity = interpolation4 - this.mProgressInterpolator.getInterpolation(mPostInterpolationPosition);
            }
            for (int i = 0; i < childCount; ++i) {
                final View child = this.getChildAt(i);
                final MotionController motionController = this.mFrameArrayList.get(child);
                if (motionController != null) {
                    this.mKeepAnimating |= motionController.interpolate(child, interpolation3, nanoTime2, this.mKeyCache);
                }
            }
            final boolean b8 = (n2 > 0 && mPostInterpolationPosition >= this.mTransitionGoalPosition) || (signum <= 0.0f && mPostInterpolationPosition <= this.mTransitionGoalPosition);
            if (!this.mKeepAnimating && !this.mInTransition && b8) {
                this.setState(TransitionState.FINISHED);
            }
            if (this.mMeasureDuringTransition) {
                this.requestLayout();
            }
            this.mKeepAnimating |= (b8 ^ true);
            boolean b9 = b5;
            if (mPostInterpolationPosition <= 0.0f) {
                final int mBeginState = this.mBeginState;
                b9 = b5;
                if (mBeginState != -1) {
                    b9 = b5;
                    if (this.mCurrentState != mBeginState) {
                        this.mCurrentState = mBeginState;
                        this.mScene.getConstraintSet(mBeginState).applyCustomAttributes(this);
                        this.setState(TransitionState.FINISHED);
                        b9 = true;
                    }
                }
            }
            boolean b10 = b9;
            if (mPostInterpolationPosition >= 1.0) {
                final int mCurrentState = this.mCurrentState;
                final int mEndState = this.mEndState;
                b10 = b9;
                if (mCurrentState != mEndState) {
                    this.mCurrentState = mEndState;
                    this.mScene.getConstraintSet(mEndState).applyCustomAttributes(this);
                    this.setState(TransitionState.FINISHED);
                    b10 = true;
                }
            }
            if (!this.mKeepAnimating && !this.mInTransition) {
                if ((n2 > 0 && mPostInterpolationPosition == 1.0f) || (signum < 0.0f && mPostInterpolationPosition == 0.0f)) {
                    this.setState(TransitionState.FINISHED);
                }
            }
            else {
                this.invalidate();
            }
            b6 = b10;
            if (!this.mKeepAnimating) {
                b6 = b10;
                if (!this.mInTransition) {
                    if (n2 <= 0 || mPostInterpolationPosition != 1.0f) {
                        b6 = b10;
                        if (signum >= 0.0f) {
                            break Label_1146;
                        }
                        b6 = b10;
                        if (mPostInterpolationPosition != 0.0f) {
                            break Label_1146;
                        }
                    }
                    this.onNewStateAttachHandlers();
                    b6 = b10;
                }
            }
        }
        final float mTransitionLastPosition2 = this.mTransitionLastPosition;
        boolean b11 = false;
        Label_1241: {
            if (mTransitionLastPosition2 >= 1.0f) {
                final int mCurrentState2 = this.mCurrentState;
                final int mEndState2 = this.mEndState;
                if (mCurrentState2 != mEndState2) {
                    b6 = b3;
                }
                this.mCurrentState = mEndState2;
            }
            else {
                b11 = b6;
                if (mTransitionLastPosition2 > 0.0f) {
                    break Label_1241;
                }
                final int mCurrentState3 = this.mCurrentState;
                final int mBeginState2 = this.mBeginState;
                if (mCurrentState3 != mBeginState2) {
                    b6 = b2;
                }
                this.mCurrentState = mBeginState2;
            }
            b11 = b6;
        }
        this.mNeedsFireTransitionCompleted |= b11;
        if (b11 && !this.mInLayout) {
            this.requestLayout();
        }
        this.mTransitionPosition = this.mTransitionLastPosition;
    }
    
    protected void fireTransitionCompleted() {
        Label_0103: {
            if (this.mTransitionListener == null) {
                final CopyOnWriteArrayList<TransitionListener> mTransitionListeners = this.mTransitionListeners;
                if (mTransitionListeners == null || mTransitionListeners.isEmpty()) {
                    break Label_0103;
                }
            }
            if (this.mListenerState == -1) {
                this.mListenerState = this.mCurrentState;
                int intValue;
                if (!this.mTransitionCompleted.isEmpty()) {
                    final ArrayList<Integer> mTransitionCompleted = this.mTransitionCompleted;
                    intValue = mTransitionCompleted.get(mTransitionCompleted.size() - 1);
                }
                else {
                    intValue = -1;
                }
                final int mCurrentState = this.mCurrentState;
                if (intValue != mCurrentState && mCurrentState != -1) {
                    this.mTransitionCompleted.add(mCurrentState);
                }
            }
        }
        this.processTransitionCompleted();
        final Runnable mOnComplete = this.mOnComplete;
        if (mOnComplete != null) {
            mOnComplete.run();
        }
        final int[] mScheduledTransitionTo = this.mScheduledTransitionTo;
        if (mScheduledTransitionTo != null && this.mScheduledTransitions > 0) {
            this.transitionToState(mScheduledTransitionTo[0]);
            final int[] mScheduledTransitionTo2 = this.mScheduledTransitionTo;
            System.arraycopy(mScheduledTransitionTo2, 1, mScheduledTransitionTo2, 0, mScheduledTransitionTo2.length - 1);
            --this.mScheduledTransitions;
        }
    }
    
    public void fireTrigger(final int n, final boolean b, final float n2) {
        final TransitionListener mTransitionListener = this.mTransitionListener;
        if (mTransitionListener != null) {
            mTransitionListener.onTransitionTrigger(this, n, b, n2);
        }
        final CopyOnWriteArrayList<TransitionListener> mTransitionListeners = this.mTransitionListeners;
        if (mTransitionListeners != null) {
            final Iterator<TransitionListener> iterator = mTransitionListeners.iterator();
            while (iterator.hasNext()) {
                iterator.next().onTransitionTrigger(this, n, b, n2);
            }
        }
    }
    
    void getAnchorDpDt(final int i, final float lastPos, float y, final float n, final float[] array) {
        final HashMap<View, MotionController> mFrameArrayList = this.mFrameArrayList;
        final View viewById = this.getViewById(i);
        final MotionController motionController = mFrameArrayList.get(viewById);
        if (motionController != null) {
            motionController.getDpDt(lastPos, y, n, array);
            y = viewById.getY();
            this.lastPos = lastPos;
            this.lastY = y;
        }
        else {
            String str;
            if (viewById == null) {
                final StringBuilder sb = new StringBuilder();
                sb.append("");
                sb.append(i);
                str = sb.toString();
            }
            else {
                str = viewById.getContext().getResources().getResourceName(i);
            }
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("WARNING could not find view id ");
            sb2.append(str);
            Log.w("MotionLayout", sb2.toString());
        }
    }
    
    public ConstraintSet getConstraintSet(final int n) {
        final MotionScene mScene = this.mScene;
        if (mScene == null) {
            return null;
        }
        return mScene.getConstraintSet(n);
    }
    
    public int[] getConstraintSetIds() {
        final MotionScene mScene = this.mScene;
        if (mScene == null) {
            return null;
        }
        return mScene.getConstraintSetIds();
    }
    
    String getConstraintSetNames(final int n) {
        final MotionScene mScene = this.mScene;
        if (mScene == null) {
            return null;
        }
        return mScene.lookUpConstraintName(n);
    }
    
    public int getCurrentState() {
        return this.mCurrentState;
    }
    
    public void getDebugMode(final boolean b) {
        int mDebugPath;
        if (b) {
            mDebugPath = 2;
        }
        else {
            mDebugPath = 1;
        }
        this.mDebugPath = mDebugPath;
        this.invalidate();
    }
    
    public ArrayList<MotionScene.Transition> getDefinedTransitions() {
        final MotionScene mScene = this.mScene;
        if (mScene == null) {
            return null;
        }
        return mScene.getDefinedTransitions();
    }
    
    public DesignTool getDesignTool() {
        if (this.mDesignTool == null) {
            this.mDesignTool = new DesignTool(this);
        }
        return this.mDesignTool;
    }
    
    public int getEndState() {
        return this.mEndState;
    }
    
    MotionController getMotionController(final int n) {
        return this.mFrameArrayList.get(this.findViewById(n));
    }
    
    protected long getNanoTime() {
        return System.nanoTime();
    }
    
    public float getProgress() {
        return this.mTransitionLastPosition;
    }
    
    public MotionScene getScene() {
        return this.mScene;
    }
    
    public int getStartState() {
        return this.mBeginState;
    }
    
    public float getTargetPosition() {
        return this.mTransitionGoalPosition;
    }
    
    public MotionScene.Transition getTransition(final int n) {
        return this.mScene.getTransitionById(n);
    }
    
    public Bundle getTransitionState() {
        if (this.mStateCache == null) {
            this.mStateCache = new StateCache();
        }
        this.mStateCache.recordState();
        return this.mStateCache.getTransitionState();
    }
    
    public long getTransitionTimeMs() {
        final MotionScene mScene = this.mScene;
        if (mScene != null) {
            this.mTransitionDuration = mScene.getDuration() / 1000.0f;
        }
        return (long)(this.mTransitionDuration * 1000.0f);
    }
    
    public float getVelocity() {
        return this.mLastVelocity;
    }
    
    public void getViewVelocity(final View key, final float n, final float n2, final float[] array, final int n3) {
        float n4 = this.mLastVelocity;
        float n5 = this.mTransitionLastPosition;
        if (this.mInterpolator != null) {
            final float signum = Math.signum(this.mTransitionGoalPosition - n5);
            final float interpolation = this.mInterpolator.getInterpolation(this.mTransitionLastPosition + 1.0E-5f);
            n5 = this.mInterpolator.getInterpolation(this.mTransitionLastPosition);
            n4 = signum * ((interpolation - n5) / 1.0E-5f) / this.mTransitionDuration;
        }
        final Interpolator mInterpolator = this.mInterpolator;
        if (mInterpolator instanceof MotionInterpolator) {
            n4 = ((MotionInterpolator)mInterpolator).getVelocity();
        }
        final MotionController motionController = this.mFrameArrayList.get(key);
        if ((n3 & 0x1) == 0x0) {
            motionController.getPostLayoutDvDp(n5, key.getWidth(), key.getHeight(), n, n2, array);
        }
        else {
            motionController.getDpDt(n5, n, n2, array);
        }
        if (n3 < 2) {
            array[0] *= n4;
            array[1] *= n4;
        }
    }
    
    public boolean isAttachedToWindow() {
        if (Build$VERSION.SDK_INT >= 19) {
            return super.isAttachedToWindow();
        }
        return this.getWindowToken() != null;
    }
    
    public boolean isDelayedApplicationOfInitialState() {
        return this.mDelayedApply;
    }
    
    public boolean isInRotation() {
        return this.mInRotation;
    }
    
    public boolean isInteractionEnabled() {
        return this.mInteractionEnabled;
    }
    
    public boolean isViewTransitionEnabled(final int n) {
        final MotionScene mScene = this.mScene;
        return mScene != null && mScene.isViewTransitionEnabled(n);
    }
    
    public void jumpToState(final int mCurrentState) {
        if (!this.isAttachedToWindow()) {
            this.mCurrentState = mCurrentState;
        }
        if (this.mBeginState == mCurrentState) {
            this.setProgress(0.0f);
        }
        else if (this.mEndState == mCurrentState) {
            this.setProgress(1.0f);
        }
        else {
            this.setTransition(mCurrentState, mCurrentState);
        }
    }
    
    @Override
    public void loadLayoutDescription(int rotation) {
        if (rotation != 0) {
            try {
                final MotionScene mScene = new MotionScene(this.getContext(), this, rotation);
                this.mScene = mScene;
                if (this.mCurrentState == -1) {
                    this.mCurrentState = mScene.getStartId();
                    this.mBeginState = this.mScene.getStartId();
                    this.mEndState = this.mScene.getEndId();
                }
                if (Build$VERSION.SDK_INT >= 19) {
                    if (!this.isAttachedToWindow()) {
                        this.mScene = null;
                        return;
                    }
                }
                try {
                    if (Build$VERSION.SDK_INT >= 17) {
                        final Display display = this.getDisplay();
                        if (display == null) {
                            rotation = 0;
                        }
                        else {
                            rotation = display.getRotation();
                        }
                        this.mPreviouseRotation = rotation;
                    }
                    final MotionScene mScene2 = this.mScene;
                    if (mScene2 != null) {
                        final ConstraintSet constraintSet = mScene2.getConstraintSet(this.mCurrentState);
                        this.mScene.readFallback(this);
                        final ArrayList<MotionHelper> mDecoratorsHelpers = this.mDecoratorsHelpers;
                        if (mDecoratorsHelpers != null) {
                            final Iterator<MotionHelper> iterator = mDecoratorsHelpers.iterator();
                            while (iterator.hasNext()) {
                                iterator.next().onFinishedMotionScene(this);
                            }
                        }
                        if (constraintSet != null) {
                            constraintSet.applyTo(this);
                        }
                        this.mBeginState = this.mCurrentState;
                    }
                    this.onNewStateAttachHandlers();
                    final StateCache mStateCache = this.mStateCache;
                    if (mStateCache != null) {
                        if (this.mDelayedApply) {
                            this.post((Runnable)new Runnable(this) {
                                final MotionLayout this$0;
                                
                                @Override
                                public void run() {
                                    this.this$0.mStateCache.apply();
                                }
                            });
                            return;
                        }
                        mStateCache.apply();
                        return;
                    }
                    else {
                        final MotionScene mScene3 = this.mScene;
                        if (mScene3 != null && mScene3.mCurrentTransition != null && this.mScene.mCurrentTransition.getAutoTransition() == 4) {
                            this.transitionToEnd();
                            this.setState(TransitionState.SETUP);
                            this.setState(TransitionState.MOVING);
                        }
                        return;
                    }
                }
                catch (final Exception cause) {
                    throw new IllegalArgumentException("unable to parse MotionScene file", cause);
                }
            }
            catch (final Exception cause2) {
                throw new IllegalArgumentException("unable to parse MotionScene file", cause2);
            }
        }
        this.mScene = null;
    }
    
    int lookUpConstraintId(final String s) {
        final MotionScene mScene = this.mScene;
        if (mScene == null) {
            return 0;
        }
        return mScene.lookUpConstraintId(s);
    }
    
    protected MotionTracker obtainVelocityTracker() {
        return (MotionTracker)MyTracker.obtain();
    }
    
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (Build$VERSION.SDK_INT >= 17) {
            final Display display = this.getDisplay();
            if (display != null) {
                this.mPreviouseRotation = display.getRotation();
            }
        }
        final MotionScene mScene = this.mScene;
        if (mScene != null) {
            final int mCurrentState = this.mCurrentState;
            if (mCurrentState != -1) {
                final ConstraintSet constraintSet = mScene.getConstraintSet(mCurrentState);
                this.mScene.readFallback(this);
                final ArrayList<MotionHelper> mDecoratorsHelpers = this.mDecoratorsHelpers;
                if (mDecoratorsHelpers != null) {
                    final Iterator<MotionHelper> iterator = mDecoratorsHelpers.iterator();
                    while (iterator.hasNext()) {
                        iterator.next().onFinishedMotionScene(this);
                    }
                }
                if (constraintSet != null) {
                    constraintSet.applyTo(this);
                }
                this.mBeginState = this.mCurrentState;
            }
        }
        this.onNewStateAttachHandlers();
        final StateCache mStateCache = this.mStateCache;
        if (mStateCache != null) {
            if (this.mDelayedApply) {
                this.post((Runnable)new Runnable(this) {
                    final MotionLayout this$0;
                    
                    @Override
                    public void run() {
                        this.this$0.mStateCache.apply();
                    }
                });
            }
            else {
                mStateCache.apply();
            }
        }
        else {
            final MotionScene mScene2 = this.mScene;
            if (mScene2 != null && mScene2.mCurrentTransition != null && this.mScene.mCurrentTransition.getAutoTransition() == 4) {
                this.transitionToEnd();
                this.setState(TransitionState.SETUP);
                this.setState(TransitionState.MOVING);
            }
        }
    }
    
    public boolean onInterceptTouchEvent(final MotionEvent motionEvent) {
        final MotionScene mScene = this.mScene;
        if (mScene != null) {
            if (this.mInteractionEnabled) {
                if (mScene.mViewTransitionController != null) {
                    this.mScene.mViewTransitionController.touchEvent(motionEvent);
                }
                final MotionScene.Transition mCurrentTransition = this.mScene.mCurrentTransition;
                if (mCurrentTransition != null && mCurrentTransition.isEnabled()) {
                    final TouchResponse touchResponse = mCurrentTransition.getTouchResponse();
                    if (touchResponse != null) {
                        if (motionEvent.getAction() == 0) {
                            final RectF touchRegion = touchResponse.getTouchRegion(this, new RectF());
                            if (touchRegion != null && !touchRegion.contains(motionEvent.getX(), motionEvent.getY())) {
                                return false;
                            }
                        }
                        final int touchRegionId = touchResponse.getTouchRegionId();
                        if (touchRegionId != -1) {
                            final View mRegionView = this.mRegionView;
                            if (mRegionView == null || mRegionView.getId() != touchRegionId) {
                                this.mRegionView = this.findViewById(touchRegionId);
                            }
                            final View mRegionView2 = this.mRegionView;
                            if (mRegionView2 != null) {
                                this.mBoundsCheck.set((float)mRegionView2.getLeft(), (float)this.mRegionView.getTop(), (float)this.mRegionView.getRight(), (float)this.mRegionView.getBottom());
                                if (this.mBoundsCheck.contains(motionEvent.getX(), motionEvent.getY()) && !this.handlesTouchEvent((float)this.mRegionView.getLeft(), (float)this.mRegionView.getTop(), this.mRegionView, motionEvent)) {
                                    return this.onTouchEvent(motionEvent);
                                }
                            }
                        }
                    }
                }
            }
        }
        return false;
    }
    
    @Override
    protected void onLayout(final boolean b, int n, int n2, final int n3, final int n4) {
        this.mInLayout = true;
        try {
            if (this.mScene == null) {
                super.onLayout(b, n, n2, n3, n4);
                return;
            }
            n = n3 - n;
            n2 = n4 - n2;
            if (this.mLastLayoutWidth != n || this.mLastLayoutHeight != n2) {
                this.rebuildScene();
                this.evaluate(true);
            }
            this.mLastLayoutWidth = n;
            this.mLastLayoutHeight = n2;
            this.mOldWidth = n;
            this.mOldHeight = n2;
        }
        finally {
            this.mInLayout = false;
        }
    }
    
    @Override
    protected void onMeasure(int mLastWidthMeasureSpec, int mLastHeightMeasureSpec) {
        if (this.mScene == null) {
            super.onMeasure(mLastWidthMeasureSpec, mLastHeightMeasureSpec);
            return;
        }
        final int mLastWidthMeasureSpec2 = this.mLastWidthMeasureSpec;
        final int n = 0;
        boolean b = mLastWidthMeasureSpec2 != mLastWidthMeasureSpec || this.mLastHeightMeasureSpec != mLastHeightMeasureSpec;
        if (this.mNeedsFireTransitionCompleted) {
            this.mNeedsFireTransitionCompleted = false;
            this.onNewStateAttachHandlers();
            this.processTransitionCompleted();
            b = true;
        }
        if (this.mDirtyHierarchy) {
            b = true;
        }
        this.mLastWidthMeasureSpec = mLastWidthMeasureSpec;
        this.mLastHeightMeasureSpec = mLastHeightMeasureSpec;
        final int startId = this.mScene.getStartId();
        final int endId = this.mScene.getEndId();
        if ((b || this.mModel.isNotConfiguredWith(startId, endId)) && this.mBeginState != -1) {
            super.onMeasure(mLastWidthMeasureSpec, mLastHeightMeasureSpec);
            this.mModel.initFrom(this.mLayoutWidget, this.mScene.getConstraintSet(startId), this.mScene.getConstraintSet(endId));
            this.mModel.reEvaluateState();
            this.mModel.setMeasuredId(startId, endId);
            mLastWidthMeasureSpec = n;
        }
        else {
            if (b) {
                super.onMeasure(mLastWidthMeasureSpec, mLastHeightMeasureSpec);
            }
            mLastWidthMeasureSpec = 1;
        }
        if (this.mMeasureDuringTransition || mLastWidthMeasureSpec != 0) {
            final int paddingTop = this.getPaddingTop();
            mLastHeightMeasureSpec = this.getPaddingBottom();
            final int paddingLeft = this.getPaddingLeft();
            mLastWidthMeasureSpec = this.getPaddingRight();
            mLastWidthMeasureSpec = this.mLayoutWidget.getWidth() + (paddingLeft + mLastWidthMeasureSpec);
            mLastHeightMeasureSpec = this.mLayoutWidget.getHeight() + (paddingTop + mLastHeightMeasureSpec);
            final int mWidthMeasureMode = this.mWidthMeasureMode;
            if (mWidthMeasureMode == Integer.MIN_VALUE || mWidthMeasureMode == 0) {
                mLastWidthMeasureSpec = this.mStartWrapWidth;
                mLastWidthMeasureSpec += (int)(this.mPostInterpolationPosition * (this.mEndWrapWidth - mLastWidthMeasureSpec));
                this.requestLayout();
            }
            final int mHeightMeasureMode = this.mHeightMeasureMode;
            if (mHeightMeasureMode == Integer.MIN_VALUE || mHeightMeasureMode == 0) {
                mLastHeightMeasureSpec = this.mStartWrapHeight;
                mLastHeightMeasureSpec += (int)(this.mPostInterpolationPosition * (this.mEndWrapHeight - mLastHeightMeasureSpec));
                this.requestLayout();
            }
            this.setMeasuredDimension(mLastWidthMeasureSpec, mLastHeightMeasureSpec);
        }
        this.evaluateLayout();
    }
    
    public boolean onNestedFling(final View view, final float n, final float n2, final boolean b) {
        return false;
    }
    
    public boolean onNestedPreFling(final View view, final float n, final float n2) {
        return false;
    }
    
    public void onNestedPreScroll(final View view, final int n, final int n2, final int[] array, int touchRegionId) {
        final MotionScene mScene = this.mScene;
        if (mScene == null) {
            return;
        }
        final MotionScene.Transition mCurrentTransition = mScene.mCurrentTransition;
        if (mCurrentTransition != null) {
            if (mCurrentTransition.isEnabled()) {
                final boolean enabled = mCurrentTransition.isEnabled();
                final int n3 = -1;
                if (enabled) {
                    final TouchResponse touchResponse = mCurrentTransition.getTouchResponse();
                    if (touchResponse != null) {
                        touchRegionId = touchResponse.getTouchRegionId();
                        if (touchRegionId != -1 && view.getId() != touchRegionId) {
                            return;
                        }
                    }
                }
                if (mScene.getMoveWhenScrollAtTop()) {
                    final TouchResponse touchResponse2 = mCurrentTransition.getTouchResponse();
                    touchRegionId = n3;
                    if (touchResponse2 != null) {
                        touchRegionId = n3;
                        if ((touchResponse2.getFlags() & 0x4) != 0x0) {
                            touchRegionId = n2;
                        }
                    }
                    final float mTransitionPosition = this.mTransitionPosition;
                    if ((mTransitionPosition == 1.0f || mTransitionPosition == 0.0f) && view.canScrollVertically(touchRegionId)) {
                        return;
                    }
                }
                if (mCurrentTransition.getTouchResponse() != null && (mCurrentTransition.getTouchResponse().getFlags() & 0x1) != 0x0) {
                    final float progressDirection = mScene.getProgressDirection((float)n, (float)n2);
                    final float mTransitionLastPosition = this.mTransitionLastPosition;
                    if ((mTransitionLastPosition <= 0.0f && progressDirection < 0.0f) || (mTransitionLastPosition >= 1.0f && progressDirection > 0.0f)) {
                        if (Build$VERSION.SDK_INT >= 21) {
                            view.setNestedScrollingEnabled(false);
                            view.post((Runnable)new Runnable(this, view) {
                                final View val$target;
                                
                                @Override
                                public void run() {
                                    this.val$target.setNestedScrollingEnabled(true);
                                }
                            });
                        }
                        return;
                    }
                }
                final float mTransitionPosition2 = this.mTransitionPosition;
                final long nanoTime = this.getNanoTime();
                final float mScrollTargetDX = (float)n;
                this.mScrollTargetDX = mScrollTargetDX;
                final float mScrollTargetDY = (float)n2;
                this.mScrollTargetDY = mScrollTargetDY;
                this.mScrollTargetDT = (float)((nanoTime - this.mScrollTargetTime) * 1.0E-9);
                this.mScrollTargetTime = nanoTime;
                mScene.processScrollMove(mScrollTargetDX, mScrollTargetDY);
                if (mTransitionPosition2 != this.mTransitionPosition) {
                    array[0] = n;
                    array[1] = n2;
                }
                this.evaluate(false);
                if (array[0] != 0 || array[1] != 0) {
                    this.mUndergoingMotion = true;
                }
            }
        }
    }
    
    public void onNestedScroll(final View view, final int n, final int n2, final int n3, final int n4, final int n5) {
    }
    
    @Override
    public void onNestedScroll(final View view, final int n, final int n2, final int n3, final int n4, final int n5, final int[] array) {
        if (this.mUndergoingMotion || n != 0 || n2 != 0) {
            array[0] += n3;
            array[1] += n4;
        }
        this.mUndergoingMotion = false;
    }
    
    public void onNestedScrollAccepted(final View view, final View view2, final int n, final int n2) {
        this.mScrollTargetTime = this.getNanoTime();
        this.mScrollTargetDT = 0.0f;
        this.mScrollTargetDX = 0.0f;
        this.mScrollTargetDY = 0.0f;
    }
    
    void onNewStateAttachHandlers() {
        final MotionScene mScene = this.mScene;
        if (mScene == null) {
            return;
        }
        if (mScene.autoTransition(this, this.mCurrentState)) {
            this.requestLayout();
            return;
        }
        final int mCurrentState = this.mCurrentState;
        if (mCurrentState != -1) {
            this.mScene.addOnClickListeners(this, mCurrentState);
        }
        if (this.mScene.supportTouch()) {
            this.mScene.setupTouch();
        }
    }
    
    public void onRtlPropertiesChanged(final int n) {
        final MotionScene mScene = this.mScene;
        if (mScene != null) {
            mScene.setRtl(this.isRtl());
        }
    }
    
    public boolean onStartNestedScroll(final View view, final View view2, final int n, final int n2) {
        final MotionScene mScene = this.mScene;
        return mScene != null && mScene.mCurrentTransition != null && this.mScene.mCurrentTransition.getTouchResponse() != null && (this.mScene.mCurrentTransition.getTouchResponse().getFlags() & 0x2) == 0x0;
    }
    
    public void onStopNestedScroll(final View view, final int n) {
        final MotionScene mScene = this.mScene;
        if (mScene != null) {
            final float mScrollTargetDT = this.mScrollTargetDT;
            if (mScrollTargetDT != 0.0f) {
                mScene.processScrollUp(this.mScrollTargetDX / mScrollTargetDT, this.mScrollTargetDY / mScrollTargetDT);
            }
        }
    }
    
    public boolean onTouchEvent(final MotionEvent motionEvent) {
        final MotionScene mScene = this.mScene;
        if (mScene == null || !this.mInteractionEnabled || !mScene.supportTouch()) {
            return super.onTouchEvent(motionEvent);
        }
        final MotionScene.Transition mCurrentTransition = this.mScene.mCurrentTransition;
        if (mCurrentTransition != null && !mCurrentTransition.isEnabled()) {
            return super.onTouchEvent(motionEvent);
        }
        this.mScene.processTouchEvent(motionEvent, this.getCurrentState(), this);
        return !this.mScene.mCurrentTransition.isTransitionFlag(4) || this.mScene.mCurrentTransition.getTouchResponse().isDragStarted();
    }
    
    @Override
    public void onViewAdded(final View view) {
        super.onViewAdded(view);
        if (view instanceof MotionHelper) {
            final MotionHelper motionHelper = (MotionHelper)view;
            if (this.mTransitionListeners == null) {
                this.mTransitionListeners = new CopyOnWriteArrayList<TransitionListener>();
            }
            this.mTransitionListeners.add((TransitionListener)motionHelper);
            if (motionHelper.isUsedOnShow()) {
                if (this.mOnShowHelpers == null) {
                    this.mOnShowHelpers = new ArrayList<MotionHelper>();
                }
                this.mOnShowHelpers.add(motionHelper);
            }
            if (motionHelper.isUseOnHide()) {
                if (this.mOnHideHelpers == null) {
                    this.mOnHideHelpers = new ArrayList<MotionHelper>();
                }
                this.mOnHideHelpers.add(motionHelper);
            }
            if (motionHelper.isDecorator()) {
                if (this.mDecoratorsHelpers == null) {
                    this.mDecoratorsHelpers = new ArrayList<MotionHelper>();
                }
                this.mDecoratorsHelpers.add(motionHelper);
            }
        }
    }
    
    @Override
    public void onViewRemoved(final View view) {
        super.onViewRemoved(view);
        final ArrayList<MotionHelper> mOnShowHelpers = this.mOnShowHelpers;
        if (mOnShowHelpers != null) {
            mOnShowHelpers.remove(view);
        }
        final ArrayList<MotionHelper> mOnHideHelpers = this.mOnHideHelpers;
        if (mOnHideHelpers != null) {
            mOnHideHelpers.remove(view);
        }
    }
    
    @Override
    protected void parseLayoutDescription(final int n) {
        this.mConstraintLayoutSpec = null;
    }
    
    @Deprecated
    public void rebuildMotion() {
        Log.e("MotionLayout", "This method is deprecated. Please call rebuildScene() instead.");
        this.rebuildScene();
    }
    
    public void rebuildScene() {
        this.mModel.reEvaluateState();
        this.invalidate();
    }
    
    public boolean removeTransitionListener(final TransitionListener o) {
        final CopyOnWriteArrayList<TransitionListener> mTransitionListeners = this.mTransitionListeners;
        return mTransitionListeners != null && mTransitionListeners.remove(o);
    }
    
    @Override
    public void requestLayout() {
        if (!this.mMeasureDuringTransition && this.mCurrentState == -1) {
            final MotionScene mScene = this.mScene;
            if (mScene != null && mScene.mCurrentTransition != null) {
                final int layoutDuringTransition = this.mScene.mCurrentTransition.getLayoutDuringTransition();
                if (layoutDuringTransition == 0) {
                    return;
                }
                if (layoutDuringTransition == 2) {
                    for (int childCount = this.getChildCount(), i = 0; i < childCount; ++i) {
                        this.mFrameArrayList.get(this.getChildAt(i)).remeasure();
                    }
                    return;
                }
            }
        }
        super.requestLayout();
    }
    
    public void rotateTo(final int mEndState, final int n) {
        int mRotatMode = 1;
        this.mInRotation = true;
        this.mPreRotateWidth = this.getWidth();
        this.mPreRotateHeight = this.getHeight();
        final int rotation = this.getDisplay().getRotation();
        if ((rotation + 1) % 4 <= (this.mPreviouseRotation + 1) % 4) {
            mRotatMode = 2;
        }
        this.mRotatMode = mRotatMode;
        this.mPreviouseRotation = rotation;
        for (int childCount = this.getChildCount(), i = 0; i < childCount; ++i) {
            final View child = this.getChildAt(i);
            ViewState value;
            if ((value = this.mPreRotate.get(child)) == null) {
                value = new ViewState();
                this.mPreRotate.put(child, value);
            }
            value.getState(child);
        }
        this.mBeginState = -1;
        this.mEndState = mEndState;
        this.mScene.setTransition(-1, mEndState);
        this.mModel.initFrom(this.mLayoutWidget, null, this.mScene.getConstraintSet(this.mEndState));
        this.mTransitionPosition = 0.0f;
        this.mTransitionLastPosition = 0.0f;
        this.invalidate();
        this.transitionToEnd(new Runnable(this) {
            final MotionLayout this$0;
            
            @Override
            public void run() {
                this.this$0.mInRotation = false;
            }
        });
        if (n > 0) {
            this.mTransitionDuration = n / 1000.0f;
        }
    }
    
    public void scheduleTransitionTo(final int n) {
        if (this.getCurrentState() == -1) {
            this.transitionToState(n);
        }
        else {
            final int[] mScheduledTransitionTo = this.mScheduledTransitionTo;
            if (mScheduledTransitionTo == null) {
                this.mScheduledTransitionTo = new int[4];
            }
            else if (mScheduledTransitionTo.length <= this.mScheduledTransitions) {
                this.mScheduledTransitionTo = Arrays.copyOf(mScheduledTransitionTo, mScheduledTransitionTo.length * 2);
            }
            this.mScheduledTransitionTo[this.mScheduledTransitions++] = n;
        }
    }
    
    public void setDebugMode(final int mDebugPath) {
        this.mDebugPath = mDebugPath;
        this.invalidate();
    }
    
    public void setDelayedApplicationOfInitialState(final boolean mDelayedApply) {
        this.mDelayedApply = mDelayedApply;
    }
    
    public void setInteractionEnabled(final boolean mInteractionEnabled) {
        this.mInteractionEnabled = mInteractionEnabled;
    }
    
    public void setInterpolatedProgress(final float progress) {
        if (this.mScene != null) {
            this.setState(TransitionState.MOVING);
            final Interpolator interpolator = this.mScene.getInterpolator();
            if (interpolator != null) {
                this.setProgress(interpolator.getInterpolation(progress));
                return;
            }
        }
        this.setProgress(progress);
    }
    
    public void setOnHide(final float progress) {
        final ArrayList<MotionHelper> mOnHideHelpers = this.mOnHideHelpers;
        if (mOnHideHelpers != null) {
            for (int size = mOnHideHelpers.size(), i = 0; i < size; ++i) {
                this.mOnHideHelpers.get(i).setProgress(progress);
            }
        }
    }
    
    public void setOnShow(final float progress) {
        final ArrayList<MotionHelper> mOnShowHelpers = this.mOnShowHelpers;
        if (mOnShowHelpers != null) {
            for (int size = mOnShowHelpers.size(), i = 0; i < size; ++i) {
                this.mOnShowHelpers.get(i).setProgress(progress);
            }
        }
    }
    
    public void setProgress(final float mTransitionPosition) {
        final float n = fcmpg(mTransitionPosition, 0.0f);
        if (n < 0 || mTransitionPosition > 1.0f) {
            Log.w("MotionLayout", "Warning! Progress is defined for values between 0.0 and 1.0 inclusive");
        }
        if (!this.isAttachedToWindow()) {
            if (this.mStateCache == null) {
                this.mStateCache = new StateCache();
            }
            this.mStateCache.setProgress(mTransitionPosition);
            return;
        }
        if (n <= 0) {
            if (this.mTransitionLastPosition == 1.0f && this.mCurrentState == this.mEndState) {
                this.setState(TransitionState.MOVING);
            }
            this.mCurrentState = this.mBeginState;
            if (this.mTransitionLastPosition == 0.0f) {
                this.setState(TransitionState.FINISHED);
            }
        }
        else if (mTransitionPosition >= 1.0f) {
            if (this.mTransitionLastPosition == 0.0f && this.mCurrentState == this.mBeginState) {
                this.setState(TransitionState.MOVING);
            }
            this.mCurrentState = this.mEndState;
            if (this.mTransitionLastPosition == 1.0f) {
                this.setState(TransitionState.FINISHED);
            }
        }
        else {
            this.mCurrentState = -1;
            this.setState(TransitionState.MOVING);
        }
        if (this.mScene == null) {
            return;
        }
        this.mTransitionInstantly = true;
        this.mTransitionGoalPosition = mTransitionPosition;
        this.mTransitionPosition = mTransitionPosition;
        this.mTransitionLastTime = -1L;
        this.mAnimationStartTime = -1L;
        this.mInterpolator = null;
        this.mInTransition = true;
        this.invalidate();
    }
    
    public void setProgress(final float n, final float n2) {
        if (!this.isAttachedToWindow()) {
            if (this.mStateCache == null) {
                this.mStateCache = new StateCache();
            }
            this.mStateCache.setProgress(n);
            this.mStateCache.setVelocity(n2);
            return;
        }
        this.setProgress(n);
        this.setState(TransitionState.MOVING);
        this.mLastVelocity = n2;
        float n3 = 1.0f;
        final float n4 = fcmpl(n2, 0.0f);
        if (n4 != 0) {
            if (n4 <= 0) {
                n3 = 0.0f;
            }
            this.animateTo(n3);
        }
        else if (n != 0.0f && n != 1.0f) {
            if (n <= 0.5f) {
                n3 = 0.0f;
            }
            this.animateTo(n3);
        }
    }
    
    public void setScene(final MotionScene mScene) {
        (this.mScene = mScene).setRtl(this.isRtl());
        this.rebuildScene();
    }
    
    void setStartState(final int mCurrentState) {
        if (!this.isAttachedToWindow()) {
            if (this.mStateCache == null) {
                this.mStateCache = new StateCache();
            }
            this.mStateCache.setStartState(mCurrentState);
            this.mStateCache.setEndState(mCurrentState);
            return;
        }
        this.mCurrentState = mCurrentState;
    }
    
    @Override
    public void setState(final int mCurrentState, final int n, final int n2) {
        this.setState(TransitionState.SETUP);
        this.mCurrentState = mCurrentState;
        this.mBeginState = -1;
        this.mEndState = -1;
        if (this.mConstraintLayoutSpec != null) {
            this.mConstraintLayoutSpec.updateConstraints(mCurrentState, (float)n, (float)n2);
        }
        else {
            final MotionScene mScene = this.mScene;
            if (mScene != null) {
                mScene.getConstraintSet(mCurrentState).applyTo(this);
            }
        }
    }
    
    void setState(final TransitionState mTransitionState) {
        if (mTransitionState == TransitionState.FINISHED && this.mCurrentState == -1) {
            return;
        }
        final TransitionState mTransitionState2 = this.mTransitionState;
        this.mTransitionState = mTransitionState;
        if (mTransitionState2 == TransitionState.MOVING && mTransitionState == TransitionState.MOVING) {
            this.fireTransitionChange();
        }
        final int n = MotionLayout$5.$SwitchMap$androidx$constraintlayout$motion$widget$MotionLayout$TransitionState[mTransitionState2.ordinal()];
        if (n != 1 && n != 2) {
            if (n == 3) {
                if (mTransitionState == TransitionState.FINISHED) {
                    this.fireTransitionCompleted();
                }
            }
        }
        else {
            if (mTransitionState == TransitionState.MOVING) {
                this.fireTransitionChange();
            }
            if (mTransitionState == TransitionState.FINISHED) {
                this.fireTransitionCompleted();
            }
        }
    }
    
    public void setTransition(int mCurrentState) {
        if (this.mScene != null) {
            final MotionScene.Transition transition = this.getTransition(mCurrentState);
            this.mBeginState = transition.getStartConstraintSetId();
            this.mEndState = transition.getEndConstraintSetId();
            if (!this.isAttachedToWindow()) {
                if (this.mStateCache == null) {
                    this.mStateCache = new StateCache();
                }
                this.mStateCache.setStartState(this.mBeginState);
                this.mStateCache.setEndState(this.mEndState);
                return;
            }
            float progress = Float.NaN;
            mCurrentState = this.mCurrentState;
            final int mBeginState = this.mBeginState;
            float mTransitionLastPosition = 0.0f;
            if (mCurrentState == mBeginState) {
                progress = 0.0f;
            }
            else if (mCurrentState == this.mEndState) {
                progress = 1.0f;
            }
            this.mScene.setTransition(transition);
            this.mModel.initFrom(this.mLayoutWidget, this.mScene.getConstraintSet(this.mBeginState), this.mScene.getConstraintSet(this.mEndState));
            this.rebuildScene();
            if (this.mTransitionLastPosition != progress) {
                if (progress == 0.0f) {
                    this.endTrigger(true);
                    this.mScene.getConstraintSet(this.mBeginState).applyTo(this);
                }
                else if (progress == 1.0f) {
                    this.endTrigger(false);
                    this.mScene.getConstraintSet(this.mEndState).applyTo(this);
                }
            }
            if (!Float.isNaN(progress)) {
                mTransitionLastPosition = progress;
            }
            this.mTransitionLastPosition = mTransitionLastPosition;
            if (Float.isNaN(progress)) {
                final StringBuilder sb = new StringBuilder();
                sb.append(Debug.getLocation());
                sb.append(" transitionToStart ");
                Log.v("MotionLayout", sb.toString());
                this.transitionToStart();
            }
            else {
                this.setProgress(progress);
            }
        }
    }
    
    public void setTransition(final int n, final int n2) {
        if (!this.isAttachedToWindow()) {
            if (this.mStateCache == null) {
                this.mStateCache = new StateCache();
            }
            this.mStateCache.setStartState(n);
            this.mStateCache.setEndState(n2);
            return;
        }
        final MotionScene mScene = this.mScene;
        if (mScene != null) {
            mScene.setTransition(this.mBeginState = n, this.mEndState = n2);
            this.mModel.initFrom(this.mLayoutWidget, this.mScene.getConstraintSet(n), this.mScene.getConstraintSet(n2));
            this.rebuildScene();
            this.mTransitionLastPosition = 0.0f;
            this.transitionToStart();
        }
    }
    
    protected void setTransition(final MotionScene.Transition transition) {
        this.mScene.setTransition(transition);
        this.setState(TransitionState.SETUP);
        if (this.mCurrentState == this.mScene.getEndId()) {
            this.mTransitionLastPosition = 1.0f;
            this.mTransitionPosition = 1.0f;
            this.mTransitionGoalPosition = 1.0f;
        }
        else {
            this.mTransitionLastPosition = 0.0f;
            this.mTransitionPosition = 0.0f;
            this.mTransitionGoalPosition = 0.0f;
        }
        long nanoTime;
        if (transition.isTransitionFlag(1)) {
            nanoTime = -1L;
        }
        else {
            nanoTime = this.getNanoTime();
        }
        this.mTransitionLastTime = nanoTime;
        final int startId = this.mScene.getStartId();
        final int endId = this.mScene.getEndId();
        if (startId == this.mBeginState && endId == this.mEndState) {
            return;
        }
        this.mBeginState = startId;
        this.mEndState = endId;
        this.mScene.setTransition(startId, endId);
        this.mModel.initFrom(this.mLayoutWidget, this.mScene.getConstraintSet(this.mBeginState), this.mScene.getConstraintSet(this.mEndState));
        this.mModel.setMeasuredId(this.mBeginState, this.mEndState);
        this.mModel.reEvaluateState();
        this.rebuildScene();
    }
    
    public void setTransitionDuration(final int duration) {
        final MotionScene mScene = this.mScene;
        if (mScene == null) {
            Log.e("MotionLayout", "MotionScene not defined");
            return;
        }
        mScene.setDuration(duration);
    }
    
    public void setTransitionListener(final TransitionListener mTransitionListener) {
        this.mTransitionListener = mTransitionListener;
    }
    
    public void setTransitionState(final Bundle transitionState) {
        if (this.mStateCache == null) {
            this.mStateCache = new StateCache();
        }
        this.mStateCache.setTransitionState(transitionState);
        if (this.isAttachedToWindow()) {
            this.mStateCache.apply();
        }
    }
    
    public String toString() {
        final Context context = this.getContext();
        final StringBuilder sb = new StringBuilder();
        sb.append(Debug.getName(context, this.mBeginState));
        sb.append("->");
        sb.append(Debug.getName(context, this.mEndState));
        sb.append(" (pos:");
        sb.append(this.mTransitionLastPosition);
        sb.append(" Dpos/Dt:");
        sb.append(this.mLastVelocity);
        return sb.toString();
    }
    
    public void touchAnimateTo(int n, float mTransitionGoalPosition, final float n2) {
        if (this.mScene == null) {
            return;
        }
        if (this.mTransitionLastPosition == mTransitionGoalPosition) {
            return;
        }
        this.mTemporalInterpolator = true;
        this.mAnimationStartTime = this.getNanoTime();
        this.mTransitionDuration = this.mScene.getDuration() / 1000.0f;
        this.mTransitionGoalPosition = mTransitionGoalPosition;
        this.mInTransition = true;
        Label_0383: {
            if (n != 0 && n != 1 && n != 2) {
                if (n == 4) {
                    this.mDecelerateLogic.config(n2, this.mTransitionLastPosition, this.mScene.getMaxAcceleration());
                    this.mInterpolator = (Interpolator)this.mDecelerateLogic;
                    break Label_0383;
                }
                if (n != 5) {
                    if (n != 6 && n != 7) {
                        break Label_0383;
                    }
                }
                else {
                    if (willJump(n2, this.mTransitionLastPosition, this.mScene.getMaxAcceleration())) {
                        this.mDecelerateLogic.config(n2, this.mTransitionLastPosition, this.mScene.getMaxAcceleration());
                        this.mInterpolator = (Interpolator)this.mDecelerateLogic;
                        break Label_0383;
                    }
                    this.mStopLogic.config(this.mTransitionLastPosition, mTransitionGoalPosition, n2, this.mTransitionDuration, this.mScene.getMaxAcceleration(), this.mScene.getMaxVelocity());
                    this.mLastVelocity = 0.0f;
                    n = this.mCurrentState;
                    this.mTransitionGoalPosition = mTransitionGoalPosition;
                    this.mCurrentState = n;
                    this.mInterpolator = (Interpolator)this.mStopLogic;
                    break Label_0383;
                }
            }
            if (n != 1 && n != 7) {
                if (n == 2 || n == 6) {
                    mTransitionGoalPosition = 1.0f;
                }
            }
            else {
                mTransitionGoalPosition = 0.0f;
            }
            if (this.mScene.getAutoCompleteMode() == 0) {
                this.mStopLogic.config(this.mTransitionLastPosition, mTransitionGoalPosition, n2, this.mTransitionDuration, this.mScene.getMaxAcceleration(), this.mScene.getMaxVelocity());
            }
            else {
                this.mStopLogic.springConfig(this.mTransitionLastPosition, mTransitionGoalPosition, n2, this.mScene.getSpringMass(), this.mScene.getSpringStiffiness(), this.mScene.getSpringDamping(), this.mScene.getSpringStopThreshold(), this.mScene.getSpringBoundary());
            }
            n = this.mCurrentState;
            this.mTransitionGoalPosition = mTransitionGoalPosition;
            this.mCurrentState = n;
            this.mInterpolator = (Interpolator)this.mStopLogic;
        }
        this.mTransitionInstantly = false;
        this.mAnimationStartTime = this.getNanoTime();
        this.invalidate();
    }
    
    public void touchSpringTo(final float n, final float n2) {
        if (this.mScene == null) {
            return;
        }
        if (this.mTransitionLastPosition == n) {
            return;
        }
        this.mTemporalInterpolator = true;
        this.mAnimationStartTime = this.getNanoTime();
        this.mTransitionDuration = this.mScene.getDuration() / 1000.0f;
        this.mTransitionGoalPosition = n;
        this.mInTransition = true;
        this.mStopLogic.springConfig(this.mTransitionLastPosition, n, n2, this.mScene.getSpringMass(), this.mScene.getSpringStiffiness(), this.mScene.getSpringDamping(), this.mScene.getSpringStopThreshold(), this.mScene.getSpringBoundary());
        final int mCurrentState = this.mCurrentState;
        this.mTransitionGoalPosition = n;
        this.mCurrentState = mCurrentState;
        this.mInterpolator = (Interpolator)this.mStopLogic;
        this.mTransitionInstantly = false;
        this.mAnimationStartTime = this.getNanoTime();
        this.invalidate();
    }
    
    public void transitionToEnd() {
        this.animateTo(1.0f);
        this.mOnComplete = null;
    }
    
    public void transitionToEnd(final Runnable mOnComplete) {
        this.animateTo(1.0f);
        this.mOnComplete = mOnComplete;
    }
    
    public void transitionToStart() {
        this.animateTo(0.0f);
    }
    
    public void transitionToState(final int endState) {
        if (!this.isAttachedToWindow()) {
            if (this.mStateCache == null) {
                this.mStateCache = new StateCache();
            }
            this.mStateCache.setEndState(endState);
            return;
        }
        this.transitionToState(endState, -1, -1);
    }
    
    public void transitionToState(final int endState, final int n) {
        if (!this.isAttachedToWindow()) {
            if (this.mStateCache == null) {
                this.mStateCache = new StateCache();
            }
            this.mStateCache.setEndState(endState);
            return;
        }
        this.transitionToState(endState, -1, -1, n);
    }
    
    public void transitionToState(final int n, final int n2, final int n3) {
        this.transitionToState(n, n2, n3, -1);
    }
    
    public void transitionToState(int i, int n, int n2, int childCount) {
        final MotionScene mScene = this.mScene;
        int mEndState = i;
        if (mScene != null) {
            mEndState = i;
            if (mScene.mStateSet != null) {
                n = this.mScene.mStateSet.convertToConstraintSet(this.mCurrentState, i, (float)n, (float)n2);
                mEndState = i;
                if (n != -1) {
                    mEndState = n;
                }
            }
        }
        i = this.mCurrentState;
        if (i == mEndState) {
            return;
        }
        if (this.mBeginState == mEndState) {
            this.animateTo(0.0f);
            if (childCount > 0) {
                this.mTransitionDuration = childCount / 1000.0f;
            }
            return;
        }
        if (this.mEndState == mEndState) {
            this.animateTo(1.0f);
            if (childCount > 0) {
                this.mTransitionDuration = childCount / 1000.0f;
            }
            return;
        }
        this.mEndState = mEndState;
        if (i != -1) {
            this.setTransition(i, mEndState);
            this.animateTo(1.0f);
            this.mTransitionLastPosition = 0.0f;
            this.transitionToEnd();
            if (childCount > 0) {
                this.mTransitionDuration = childCount / 1000.0f;
            }
            return;
        }
        n2 = 0;
        this.mTemporalInterpolator = false;
        this.mTransitionGoalPosition = 1.0f;
        this.mTransitionPosition = 0.0f;
        this.mTransitionLastPosition = 0.0f;
        this.mTransitionLastTime = this.getNanoTime();
        this.mAnimationStartTime = this.getNanoTime();
        this.mTransitionInstantly = false;
        this.mInterpolator = null;
        if (childCount == -1) {
            this.mTransitionDuration = this.mScene.getDuration() / 1000.0f;
        }
        this.mBeginState = -1;
        this.mScene.setTransition(-1, this.mEndState);
        final SparseArray sparseArray = new SparseArray();
        if (childCount == 0) {
            this.mTransitionDuration = this.mScene.getDuration() / 1000.0f;
        }
        else if (childCount > 0) {
            this.mTransitionDuration = childCount / 1000.0f;
        }
        childCount = this.getChildCount();
        this.mFrameArrayList.clear();
        View child;
        for (i = 0; i < childCount; ++i) {
            child = this.getChildAt(i);
            this.mFrameArrayList.put(child, new MotionController(child));
            sparseArray.put(child.getId(), (Object)this.mFrameArrayList.get(child));
        }
        this.mInTransition = true;
        this.mModel.initFrom(this.mLayoutWidget, null, this.mScene.getConstraintSet(mEndState));
        this.rebuildScene();
        this.mModel.build();
        this.computeCurrentPositions();
        n = this.getWidth();
        final int height = this.getHeight();
        if (this.mDecoratorsHelpers != null) {
            MotionController motionController;
            for (i = 0; i < childCount; ++i) {
                motionController = this.mFrameArrayList.get(this.getChildAt(i));
                if (motionController != null) {
                    this.mScene.getKeyFrames(motionController);
                }
            }
            final Iterator<MotionHelper> iterator = this.mDecoratorsHelpers.iterator();
            while (iterator.hasNext()) {
                iterator.next().onPreSetup(this, this.mFrameArrayList);
            }
            MotionController motionController2;
            for (i = 0; i < childCount; ++i) {
                motionController2 = this.mFrameArrayList.get(this.getChildAt(i));
                if (motionController2 != null) {
                    motionController2.setup(n, height, this.mTransitionDuration, this.getNanoTime());
                }
            }
        }
        else {
            MotionController motionController3;
            for (i = 0; i < childCount; ++i) {
                motionController3 = this.mFrameArrayList.get(this.getChildAt(i));
                if (motionController3 != null) {
                    this.mScene.getKeyFrames(motionController3);
                    motionController3.setup(n, height, this.mTransitionDuration, this.getNanoTime());
                }
            }
        }
        final float staggered = this.mScene.getStaggered();
        if (staggered != 0.0f) {
            float min = Float.MAX_VALUE;
            float max = -3.4028235E38f;
            n = 0;
            while (true) {
                i = n2;
                if (n >= childCount) {
                    break;
                }
                final MotionController motionController4 = this.mFrameArrayList.get(this.getChildAt(n));
                final float n3 = motionController4.getFinalY() + motionController4.getFinalX();
                min = Math.min(min, n3);
                max = Math.max(max, n3);
                ++n;
            }
            while (i < childCount) {
                final MotionController motionController5 = this.mFrameArrayList.get(this.getChildAt(i));
                final float finalX = motionController5.getFinalX();
                final float finalY = motionController5.getFinalY();
                motionController5.mStaggerScale = 1.0f / (1.0f - staggered);
                motionController5.mStaggerOffset = staggered - (finalX + finalY - min) * staggered / (max - min);
                ++i;
            }
        }
        this.mTransitionPosition = 0.0f;
        this.mTransitionLastPosition = 0.0f;
        this.mInTransition = true;
        this.invalidate();
    }
    
    public void updateState() {
        this.mModel.initFrom(this.mLayoutWidget, this.mScene.getConstraintSet(this.mBeginState), this.mScene.getConstraintSet(this.mEndState));
        this.rebuildScene();
    }
    
    public void updateState(final int n, final ConstraintSet set) {
        final MotionScene mScene = this.mScene;
        if (mScene != null) {
            mScene.setConstraintSet(n, set);
        }
        this.updateState();
        if (this.mCurrentState == n) {
            set.applyTo(this);
        }
    }
    
    public void updateStateAnimate(final int n, final ConstraintSet set, final int duration) {
        if (this.mScene == null) {
            return;
        }
        if (this.mCurrentState == n) {
            this.updateState(R.id.view_transition, this.getConstraintSet(n));
            this.setState(R.id.view_transition, -1, -1);
            this.updateState(n, set);
            final MotionScene.Transition transition = new MotionScene.Transition(-1, this.mScene, R.id.view_transition, n);
            transition.setDuration(duration);
            this.setTransition(transition);
            this.transitionToEnd();
        }
    }
    
    public void viewTransition(final int n, final View... array) {
        final MotionScene mScene = this.mScene;
        if (mScene != null) {
            mScene.viewTransition(n, array);
        }
        else {
            Log.e("MotionLayout", " no motionScene");
        }
    }
    
    class DecelerateInterpolator extends MotionInterpolator
    {
        float currentP;
        float initalV;
        float maxA;
        final MotionLayout this$0;
        
        DecelerateInterpolator(final MotionLayout this$0) {
            this.this$0 = this$0;
            this.initalV = 0.0f;
            this.currentP = 0.0f;
        }
        
        public void config(final float initalV, final float currentP, final float maxA) {
            this.initalV = initalV;
            this.currentP = currentP;
            this.maxA = maxA;
        }
        
        @Override
        public float getInterpolation(float n) {
            final float initalV = this.initalV;
            float n3;
            if (initalV > 0.0f) {
                final float maxA = this.maxA;
                float n2 = n;
                if (initalV / maxA < n) {
                    n2 = initalV / maxA;
                }
                this.this$0.mLastVelocity = initalV - maxA * n2;
                n = this.initalV * n2 - this.maxA * n2 * n2 / 2.0f;
                n3 = this.currentP;
            }
            else {
                final float n4 = -initalV;
                final float maxA2 = this.maxA;
                float n5 = n;
                if (n4 / maxA2 < n) {
                    n5 = -initalV / maxA2;
                }
                this.this$0.mLastVelocity = initalV + maxA2 * n5;
                n = this.initalV * n5 + this.maxA * n5 * n5 / 2.0f;
                n3 = this.currentP;
            }
            return n + n3;
        }
        
        @Override
        public float getVelocity() {
            return this.this$0.mLastVelocity;
        }
    }
    
    private class DevModeDraw
    {
        private static final int DEBUG_PATH_TICKS_PER_MS = 16;
        final int DIAMOND_SIZE;
        final int GRAPH_COLOR;
        final int KEYFRAME_COLOR;
        final int RED_COLOR;
        final int SHADOW_COLOR;
        Rect mBounds;
        DashPathEffect mDashPathEffect;
        Paint mFillPaint;
        int mKeyFrameCount;
        float[] mKeyFramePoints;
        Paint mPaint;
        Paint mPaintGraph;
        Paint mPaintKeyframes;
        Path mPath;
        int[] mPathMode;
        float[] mPoints;
        boolean mPresentationMode;
        private float[] mRectangle;
        int mShadowTranslate;
        Paint mTextPaint;
        final MotionLayout this$0;
        
        public DevModeDraw(final MotionLayout this$0) {
            this.this$0 = this$0;
            this.RED_COLOR = -21965;
            this.KEYFRAME_COLOR = -2067046;
            this.GRAPH_COLOR = -13391360;
            this.SHADOW_COLOR = 1996488704;
            this.DIAMOND_SIZE = 10;
            this.mBounds = new Rect();
            this.mPresentationMode = false;
            this.mShadowTranslate = 1;
            (this.mPaint = new Paint()).setAntiAlias(true);
            this.mPaint.setColor(-21965);
            this.mPaint.setStrokeWidth(2.0f);
            this.mPaint.setStyle(Paint$Style.STROKE);
            (this.mPaintKeyframes = new Paint()).setAntiAlias(true);
            this.mPaintKeyframes.setColor(-2067046);
            this.mPaintKeyframes.setStrokeWidth(2.0f);
            this.mPaintKeyframes.setStyle(Paint$Style.STROKE);
            (this.mPaintGraph = new Paint()).setAntiAlias(true);
            this.mPaintGraph.setColor(-13391360);
            this.mPaintGraph.setStrokeWidth(2.0f);
            this.mPaintGraph.setStyle(Paint$Style.STROKE);
            (this.mTextPaint = new Paint()).setAntiAlias(true);
            this.mTextPaint.setColor(-13391360);
            this.mTextPaint.setTextSize(this$0.getContext().getResources().getDisplayMetrics().density * 12.0f);
            this.mRectangle = new float[8];
            (this.mFillPaint = new Paint()).setAntiAlias(true);
            final DashPathEffect dashPathEffect = new DashPathEffect(new float[] { 4.0f, 8.0f }, 0.0f);
            this.mDashPathEffect = dashPathEffect;
            this.mPaintGraph.setPathEffect((PathEffect)dashPathEffect);
            this.mKeyFramePoints = new float[100];
            this.mPathMode = new int[50];
            if (this.mPresentationMode) {
                this.mPaint.setStrokeWidth(8.0f);
                this.mFillPaint.setStrokeWidth(8.0f);
                this.mPaintKeyframes.setStrokeWidth(8.0f);
                this.mShadowTranslate = 4;
            }
        }
        
        private void drawBasicPath(final Canvas canvas) {
            canvas.drawLines(this.mPoints, this.mPaint);
        }
        
        private void drawPathAsConfigured(final Canvas canvas) {
            int i = 0;
            boolean b = false;
            boolean b2 = false;
            while (i < this.mKeyFrameCount) {
                final int n = this.mPathMode[i];
                if (n == 1) {
                    b = true;
                }
                if (n == 0) {
                    b2 = true;
                }
                ++i;
            }
            if (b) {
                this.drawPathRelative(canvas);
            }
            if (b2) {
                this.drawPathCartesian(canvas);
            }
        }
        
        private void drawPathCartesian(final Canvas canvas) {
            final float[] mPoints = this.mPoints;
            final float n = mPoints[0];
            final float n2 = mPoints[1];
            final float n3 = mPoints[mPoints.length - 2];
            final float n4 = mPoints[mPoints.length - 1];
            canvas.drawLine(Math.min(n, n3), Math.max(n2, n4), Math.max(n, n3), Math.max(n2, n4), this.mPaintGraph);
            canvas.drawLine(Math.min(n, n3), Math.min(n2, n4), Math.min(n, n3), Math.max(n2, n4), this.mPaintGraph);
        }
        
        private void drawPathCartesianTicks(final Canvas canvas, final float n, final float n2) {
            final float[] mPoints = this.mPoints;
            final float a = mPoints[0];
            final float a2 = mPoints[1];
            final float b = mPoints[mPoints.length - 2];
            final float b2 = mPoints[mPoints.length - 1];
            final float min = Math.min(a, b);
            final float max = Math.max(a2, b2);
            final float n3 = n - Math.min(a, b);
            final float n4 = Math.max(a2, b2) - n2;
            final StringBuilder sb = new StringBuilder();
            sb.append("");
            sb.append((int)(n3 * 100.0f / Math.abs(b - a) + 0.5) / 100.0f);
            final String string = sb.toString();
            this.getTextBounds(string, this.mTextPaint);
            canvas.drawText(string, n3 / 2.0f - this.mBounds.width() / 2 + min, n2 - 20.0f, this.mTextPaint);
            canvas.drawLine(n, n2, Math.min(a, b), n2, this.mPaintGraph);
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("");
            sb2.append((int)(n4 * 100.0f / Math.abs(b2 - a2) + 0.5) / 100.0f);
            final String string2 = sb2.toString();
            this.getTextBounds(string2, this.mTextPaint);
            canvas.drawText(string2, n + 5.0f, max - (n4 / 2.0f - this.mBounds.height() / 2), this.mTextPaint);
            canvas.drawLine(n, n2, n, Math.max(a2, b2), this.mPaintGraph);
        }
        
        private void drawPathRelative(final Canvas canvas) {
            final float[] mPoints = this.mPoints;
            canvas.drawLine(mPoints[0], mPoints[1], mPoints[mPoints.length - 2], mPoints[mPoints.length - 1], this.mPaintGraph);
        }
        
        private void drawPathRelativeTicks(final Canvas canvas, final float n, final float n2) {
            final float[] mPoints = this.mPoints;
            final float n3 = mPoints[0];
            final float n4 = mPoints[1];
            final float n5 = mPoints[mPoints.length - 2];
            final float n6 = mPoints[mPoints.length - 1];
            final float n7 = (float)Math.hypot(n3 - n5, n4 - n6);
            final float n8 = n5 - n3;
            final float n9 = n6 - n4;
            final float n10 = ((n - n3) * n8 + (n2 - n4) * n9) / (n7 * n7);
            final float n11 = n3 + n8 * n10;
            final float n12 = n4 + n10 * n9;
            final Path path = new Path();
            path.moveTo(n, n2);
            path.lineTo(n11, n12);
            final float n13 = (float)Math.hypot(n11 - n, n12 - n2);
            final StringBuilder sb = new StringBuilder();
            sb.append("");
            sb.append((int)(n13 * 100.0f / n7) / 100.0f);
            final String string = sb.toString();
            this.getTextBounds(string, this.mTextPaint);
            canvas.drawTextOnPath(string, path, n13 / 2.0f - this.mBounds.width() / 2, -20.0f, this.mTextPaint);
            canvas.drawLine(n, n2, n11, n12, this.mPaintGraph);
        }
        
        private void drawPathScreenTicks(final Canvas canvas, final float n, final float n2, final int n3, final int n4) {
            final StringBuilder sb = new StringBuilder();
            sb.append("");
            sb.append((int)((n - n3 / 2) * 100.0f / (this.this$0.getWidth() - n3) + 0.5) / 100.0f);
            final String string = sb.toString();
            this.getTextBounds(string, this.mTextPaint);
            canvas.drawText(string, n / 2.0f - this.mBounds.width() / 2 + 0.0f, n2 - 20.0f, this.mTextPaint);
            canvas.drawLine(n, n2, Math.min(0.0f, 1.0f), n2, this.mPaintGraph);
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("");
            sb2.append((int)((n2 - n4 / 2) * 100.0f / (this.this$0.getHeight() - n4) + 0.5) / 100.0f);
            final String string2 = sb2.toString();
            this.getTextBounds(string2, this.mTextPaint);
            canvas.drawText(string2, n + 5.0f, 0.0f - (n2 / 2.0f - this.mBounds.height() / 2), this.mTextPaint);
            canvas.drawLine(n, n2, n, Math.max(0.0f, 1.0f), this.mPaintGraph);
        }
        
        private void drawRectangle(final Canvas canvas, final MotionController motionController) {
            this.mPath.reset();
            for (int i = 0; i <= 50; ++i) {
                motionController.buildRect(i / (float)50, this.mRectangle, 0);
                final Path mPath = this.mPath;
                final float[] mRectangle = this.mRectangle;
                mPath.moveTo(mRectangle[0], mRectangle[1]);
                final Path mPath2 = this.mPath;
                final float[] mRectangle2 = this.mRectangle;
                mPath2.lineTo(mRectangle2[2], mRectangle2[3]);
                final Path mPath3 = this.mPath;
                final float[] mRectangle3 = this.mRectangle;
                mPath3.lineTo(mRectangle3[4], mRectangle3[5]);
                final Path mPath4 = this.mPath;
                final float[] mRectangle4 = this.mRectangle;
                mPath4.lineTo(mRectangle4[6], mRectangle4[7]);
                this.mPath.close();
            }
            this.mPaint.setColor(1140850688);
            canvas.translate(2.0f, 2.0f);
            canvas.drawPath(this.mPath, this.mPaint);
            canvas.translate(-2.0f, -2.0f);
            this.mPaint.setColor(-65536);
            canvas.drawPath(this.mPath, this.mPaint);
        }
        
        private void drawTicks(final Canvas canvas, final int n, final int n2, final MotionController motionController) {
            int width;
            int height;
            if (motionController.mView != null) {
                width = motionController.mView.getWidth();
                height = motionController.mView.getHeight();
            }
            else {
                width = 0;
                height = 0;
            }
            for (int i = 1; i < n2 - 1; ++i) {
                if (n != 4 || this.mPathMode[i - 1] != 0) {
                    final float[] mKeyFramePoints = this.mKeyFramePoints;
                    final int n3 = i * 2;
                    final float n4 = mKeyFramePoints[n3];
                    final float n5 = mKeyFramePoints[n3 + 1];
                    this.mPath.reset();
                    this.mPath.moveTo(n4, n5 + 10.0f);
                    this.mPath.lineTo(n4 + 10.0f, n5);
                    this.mPath.lineTo(n4, n5 - 10.0f);
                    this.mPath.lineTo(n4 - 10.0f, n5);
                    this.mPath.close();
                    final int n6 = i - 1;
                    motionController.getKeyFrame(n6);
                    if (n == 4) {
                        final int n7 = this.mPathMode[n6];
                        if (n7 == 1) {
                            this.drawPathRelativeTicks(canvas, n4 - 0.0f, n5 - 0.0f);
                        }
                        else if (n7 == 0) {
                            this.drawPathCartesianTicks(canvas, n4 - 0.0f, n5 - 0.0f);
                        }
                        else if (n7 == 2) {
                            this.drawPathScreenTicks(canvas, n4 - 0.0f, n5 - 0.0f, width, height);
                        }
                        canvas.drawPath(this.mPath, this.mFillPaint);
                    }
                    if (n == 2) {
                        this.drawPathRelativeTicks(canvas, n4 - 0.0f, n5 - 0.0f);
                    }
                    if (n == 3) {
                        this.drawPathCartesianTicks(canvas, n4 - 0.0f, n5 - 0.0f);
                    }
                    if (n == 6) {
                        this.drawPathScreenTicks(canvas, n4 - 0.0f, n5 - 0.0f, width, height);
                    }
                    canvas.drawPath(this.mPath, this.mFillPaint);
                }
            }
            final float[] mPoints = this.mPoints;
            if (mPoints.length > 1) {
                canvas.drawCircle(mPoints[0], mPoints[1], 8.0f, this.mPaintKeyframes);
                final float[] mPoints2 = this.mPoints;
                canvas.drawCircle(mPoints2[mPoints2.length - 2], mPoints2[mPoints2.length - 1], 8.0f, this.mPaintKeyframes);
            }
        }
        
        private void drawTranslation(final Canvas canvas, final float n, final float n2, final float n3, final float n4) {
            canvas.drawRect(n, n2, n3, n4, this.mPaintGraph);
            canvas.drawLine(n, n2, n3, n4, this.mPaintGraph);
        }
        
        public void draw(final Canvas canvas, final HashMap<View, MotionController> hashMap, final int n, final int n2) {
            if (hashMap != null) {
                if (hashMap.size() != 0) {
                    canvas.save();
                    if (!this.this$0.isInEditMode() && (n2 & 0x1) == 0x2) {
                        final StringBuilder sb = new StringBuilder();
                        sb.append(this.this$0.getContext().getResources().getResourceName(this.this$0.mEndState));
                        sb.append(":");
                        sb.append(this.this$0.getProgress());
                        final String string = sb.toString();
                        canvas.drawText(string, 10.0f, (float)(this.this$0.getHeight() - 30), this.mTextPaint);
                        canvas.drawText(string, 11.0f, (float)(this.this$0.getHeight() - 29), this.mPaint);
                    }
                    for (final MotionController motionController : hashMap.values()) {
                        int drawPath;
                        final int n3 = drawPath = motionController.getDrawPath();
                        if (n2 > 0 && (drawPath = n3) == 0) {
                            drawPath = 1;
                        }
                        if (drawPath == 0) {
                            continue;
                        }
                        this.mKeyFrameCount = motionController.buildKeyFrames(this.mKeyFramePoints, this.mPathMode);
                        if (drawPath < 1) {
                            continue;
                        }
                        final int n4 = n / 16;
                        final float[] mPoints = this.mPoints;
                        if (mPoints == null || mPoints.length != n4 * 2) {
                            this.mPoints = new float[n4 * 2];
                            this.mPath = new Path();
                        }
                        final int mShadowTranslate = this.mShadowTranslate;
                        canvas.translate((float)mShadowTranslate, (float)mShadowTranslate);
                        this.mPaint.setColor(1996488704);
                        this.mFillPaint.setColor(1996488704);
                        this.mPaintKeyframes.setColor(1996488704);
                        this.mPaintGraph.setColor(1996488704);
                        motionController.buildPath(this.mPoints, n4);
                        this.drawAll(canvas, drawPath, this.mKeyFrameCount, motionController);
                        this.mPaint.setColor(-21965);
                        this.mPaintKeyframes.setColor(-2067046);
                        this.mFillPaint.setColor(-2067046);
                        this.mPaintGraph.setColor(-13391360);
                        final int mShadowTranslate2 = this.mShadowTranslate;
                        canvas.translate((float)(-mShadowTranslate2), (float)(-mShadowTranslate2));
                        this.drawAll(canvas, drawPath, this.mKeyFrameCount, motionController);
                        if (drawPath != 5) {
                            continue;
                        }
                        this.drawRectangle(canvas, motionController);
                    }
                    canvas.restore();
                }
            }
        }
        
        public void drawAll(final Canvas canvas, final int n, final int n2, final MotionController motionController) {
            if (n == 4) {
                this.drawPathAsConfigured(canvas);
            }
            if (n == 2) {
                this.drawPathRelative(canvas);
            }
            if (n == 3) {
                this.drawPathCartesian(canvas);
            }
            this.drawBasicPath(canvas);
            this.drawTicks(canvas, n, n2, motionController);
        }
        
        void getTextBounds(final String s, final Paint paint) {
            paint.getTextBounds(s, 0, s.length(), this.mBounds);
        }
    }
    
    class Model
    {
        ConstraintSet mEnd;
        int mEndId;
        ConstraintWidgetContainer mLayoutEnd;
        ConstraintWidgetContainer mLayoutStart;
        ConstraintSet mStart;
        int mStartId;
        final MotionLayout this$0;
        
        Model(final MotionLayout this$0) {
            this.this$0 = this$0;
            this.mLayoutStart = new ConstraintWidgetContainer();
            this.mLayoutEnd = new ConstraintWidgetContainer();
            this.mStart = null;
            this.mEnd = null;
        }
        
        private void computeStartEndSize(int n, final int n2) {
            final int optimizationLevel = this.this$0.getOptimizationLevel();
            if (this.this$0.mCurrentState == this.this$0.getStartState()) {
                final MotionLayout this$0 = this.this$0;
                final ConstraintWidgetContainer mLayoutEnd = this.mLayoutEnd;
                final ConstraintSet mEnd = this.mEnd;
                int n3;
                if (mEnd != null && mEnd.mRotate != 0) {
                    n3 = n2;
                }
                else {
                    n3 = n;
                }
                final ConstraintSet mEnd2 = this.mEnd;
                int n4;
                if (mEnd2 != null && mEnd2.mRotate != 0) {
                    n4 = n;
                }
                else {
                    n4 = n2;
                }
                this$0.resolveSystem(mLayoutEnd, optimizationLevel, n3, n4);
                final ConstraintSet mStart = this.mStart;
                if (mStart != null) {
                    final MotionLayout this$2 = this.this$0;
                    final ConstraintWidgetContainer mLayoutStart = this.mLayoutStart;
                    int n5;
                    if (mStart.mRotate == 0) {
                        n5 = n;
                    }
                    else {
                        n5 = n2;
                    }
                    if (this.mStart.mRotate == 0) {
                        n = n2;
                    }
                    this$2.resolveSystem(mLayoutStart, optimizationLevel, n5, n);
                }
            }
            else {
                final ConstraintSet mStart2 = this.mStart;
                if (mStart2 != null) {
                    final MotionLayout this$3 = this.this$0;
                    final ConstraintWidgetContainer mLayoutStart2 = this.mLayoutStart;
                    int n6;
                    if (mStart2.mRotate == 0) {
                        n6 = n;
                    }
                    else {
                        n6 = n2;
                    }
                    int n7;
                    if (this.mStart.mRotate == 0) {
                        n7 = n2;
                    }
                    else {
                        n7 = n;
                    }
                    this$3.resolveSystem(mLayoutStart2, optimizationLevel, n6, n7);
                }
                final MotionLayout this$4 = this.this$0;
                final ConstraintWidgetContainer mLayoutEnd2 = this.mLayoutEnd;
                final ConstraintSet mEnd3 = this.mEnd;
                int n8;
                if (mEnd3 != null && mEnd3.mRotate != 0) {
                    n8 = n2;
                }
                else {
                    n8 = n;
                }
                final ConstraintSet mEnd4 = this.mEnd;
                if (mEnd4 == null || mEnd4.mRotate == 0) {
                    n = n2;
                }
                this$4.resolveSystem(mLayoutEnd2, optimizationLevel, n8, n);
            }
        }
        
        private void debugLayout(String str, final ConstraintWidgetContainer obj) {
            final View view = (View)obj.getCompanionWidget();
            final StringBuilder sb = new StringBuilder();
            sb.append(str);
            sb.append(" ");
            sb.append(Debug.getName(view));
            final String string = sb.toString();
            final StringBuilder sb2 = new StringBuilder();
            sb2.append(string);
            sb2.append("  ========= ");
            sb2.append(obj);
            Log.v("MotionLayout", sb2.toString());
            for (int size = obj.getChildren().size(), i = 0; i < size; ++i) {
                final StringBuilder sb3 = new StringBuilder();
                sb3.append(string);
                sb3.append("[");
                sb3.append(i);
                sb3.append("] ");
                final String string2 = sb3.toString();
                final ConstraintWidget obj2 = obj.getChildren().get(i);
                final StringBuilder sb4 = new StringBuilder();
                sb4.append("");
                final ConstraintAnchor mTarget = obj2.mTop.mTarget;
                final String s = "_";
                if (mTarget != null) {
                    str = "T";
                }
                else {
                    str = "_";
                }
                sb4.append(str);
                str = sb4.toString();
                final StringBuilder sb5 = new StringBuilder();
                sb5.append(str);
                if (obj2.mBottom.mTarget != null) {
                    str = "B";
                }
                else {
                    str = "_";
                }
                sb5.append(str);
                str = sb5.toString();
                final StringBuilder sb6 = new StringBuilder();
                sb6.append(str);
                if (obj2.mLeft.mTarget != null) {
                    str = "L";
                }
                else {
                    str = "_";
                }
                sb6.append(str);
                str = sb6.toString();
                final StringBuilder sb7 = new StringBuilder();
                sb7.append(str);
                str = s;
                if (obj2.mRight.mTarget != null) {
                    str = "R";
                }
                sb7.append(str);
                final String string3 = sb7.toString();
                final View view2 = (View)obj2.getCompanionWidget();
                final String str2 = str = Debug.getName(view2);
                if (view2 instanceof TextView) {
                    final StringBuilder sb8 = new StringBuilder();
                    sb8.append(str2);
                    sb8.append("(");
                    sb8.append((Object)((TextView)view2).getText());
                    sb8.append(")");
                    str = sb8.toString();
                }
                final StringBuilder sb9 = new StringBuilder();
                sb9.append(string2);
                sb9.append("  ");
                sb9.append(str);
                sb9.append(" ");
                sb9.append(obj2);
                sb9.append(" ");
                sb9.append(string3);
                Log.v("MotionLayout", sb9.toString());
            }
            final StringBuilder sb10 = new StringBuilder();
            sb10.append(string);
            sb10.append(" done. ");
            Log.v("MotionLayout", sb10.toString());
        }
        
        private void debugLayoutParam(final String str, final LayoutParams layoutParams) {
            final StringBuilder sb = new StringBuilder();
            sb.append(" ");
            String str2;
            if (layoutParams.startToStart != -1) {
                str2 = "SS";
            }
            else {
                str2 = "__";
            }
            sb.append(str2);
            final String string = sb.toString();
            final StringBuilder sb2 = new StringBuilder();
            sb2.append(string);
            final int startToEnd = layoutParams.startToEnd;
            final String s = "|__";
            String str3;
            if (startToEnd != -1) {
                str3 = "|SE";
            }
            else {
                str3 = "|__";
            }
            sb2.append(str3);
            final String string2 = sb2.toString();
            final StringBuilder sb3 = new StringBuilder();
            sb3.append(string2);
            String str4;
            if (layoutParams.endToStart != -1) {
                str4 = "|ES";
            }
            else {
                str4 = "|__";
            }
            sb3.append(str4);
            final String string3 = sb3.toString();
            final StringBuilder sb4 = new StringBuilder();
            sb4.append(string3);
            String str5;
            if (layoutParams.endToEnd != -1) {
                str5 = "|EE";
            }
            else {
                str5 = "|__";
            }
            sb4.append(str5);
            final String string4 = sb4.toString();
            final StringBuilder sb5 = new StringBuilder();
            sb5.append(string4);
            String str6;
            if (layoutParams.leftToLeft != -1) {
                str6 = "|LL";
            }
            else {
                str6 = "|__";
            }
            sb5.append(str6);
            final String string5 = sb5.toString();
            final StringBuilder sb6 = new StringBuilder();
            sb6.append(string5);
            String str7;
            if (layoutParams.leftToRight != -1) {
                str7 = "|LR";
            }
            else {
                str7 = "|__";
            }
            sb6.append(str7);
            final String string6 = sb6.toString();
            final StringBuilder sb7 = new StringBuilder();
            sb7.append(string6);
            String str8;
            if (layoutParams.rightToLeft != -1) {
                str8 = "|RL";
            }
            else {
                str8 = "|__";
            }
            sb7.append(str8);
            final String string7 = sb7.toString();
            final StringBuilder sb8 = new StringBuilder();
            sb8.append(string7);
            String str9;
            if (layoutParams.rightToRight != -1) {
                str9 = "|RR";
            }
            else {
                str9 = "|__";
            }
            sb8.append(str9);
            final String string8 = sb8.toString();
            final StringBuilder sb9 = new StringBuilder();
            sb9.append(string8);
            String str10;
            if (layoutParams.topToTop != -1) {
                str10 = "|TT";
            }
            else {
                str10 = "|__";
            }
            sb9.append(str10);
            final String string9 = sb9.toString();
            final StringBuilder sb10 = new StringBuilder();
            sb10.append(string9);
            String str11;
            if (layoutParams.topToBottom != -1) {
                str11 = "|TB";
            }
            else {
                str11 = "|__";
            }
            sb10.append(str11);
            final String string10 = sb10.toString();
            final StringBuilder sb11 = new StringBuilder();
            sb11.append(string10);
            String str12;
            if (layoutParams.bottomToTop != -1) {
                str12 = "|BT";
            }
            else {
                str12 = "|__";
            }
            sb11.append(str12);
            final String string11 = sb11.toString();
            final StringBuilder sb12 = new StringBuilder();
            sb12.append(string11);
            String str13 = s;
            if (layoutParams.bottomToBottom != -1) {
                str13 = "|BB";
            }
            sb12.append(str13);
            final String string12 = sb12.toString();
            final StringBuilder sb13 = new StringBuilder();
            sb13.append(str);
            sb13.append(string12);
            Log.v("MotionLayout", sb13.toString());
        }
        
        private void debugWidget(final String str, final ConstraintWidget obj) {
            final StringBuilder sb = new StringBuilder();
            sb.append(" ");
            final ConstraintAnchor mTarget = obj.mTop.mTarget;
            final String s = "B";
            final String s2 = "__";
            String string;
            if (mTarget != null) {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("T");
                String str2;
                if (obj.mTop.mTarget.mType == ConstraintAnchor.Type.TOP) {
                    str2 = "T";
                }
                else {
                    str2 = "B";
                }
                sb2.append(str2);
                string = sb2.toString();
            }
            else {
                string = "__";
            }
            sb.append(string);
            final String string2 = sb.toString();
            final StringBuilder sb3 = new StringBuilder();
            sb3.append(string2);
            String string3;
            if (obj.mBottom.mTarget != null) {
                final StringBuilder sb4 = new StringBuilder();
                sb4.append("B");
                String str3 = s;
                if (obj.mBottom.mTarget.mType == ConstraintAnchor.Type.TOP) {
                    str3 = "T";
                }
                sb4.append(str3);
                string3 = sb4.toString();
            }
            else {
                string3 = "__";
            }
            sb3.append(string3);
            final String string4 = sb3.toString();
            final StringBuilder sb5 = new StringBuilder();
            sb5.append(string4);
            final ConstraintAnchor mTarget2 = obj.mLeft.mTarget;
            final String s3 = "R";
            String string5;
            if (mTarget2 != null) {
                final StringBuilder sb6 = new StringBuilder();
                sb6.append("L");
                String str4;
                if (obj.mLeft.mTarget.mType == ConstraintAnchor.Type.LEFT) {
                    str4 = "L";
                }
                else {
                    str4 = "R";
                }
                sb6.append(str4);
                string5 = sb6.toString();
            }
            else {
                string5 = "__";
            }
            sb5.append(string5);
            final String string6 = sb5.toString();
            final StringBuilder sb7 = new StringBuilder();
            sb7.append(string6);
            String string7 = s2;
            if (obj.mRight.mTarget != null) {
                final StringBuilder sb8 = new StringBuilder();
                sb8.append("R");
                String str5 = s3;
                if (obj.mRight.mTarget.mType == ConstraintAnchor.Type.LEFT) {
                    str5 = "L";
                }
                sb8.append(str5);
                string7 = sb8.toString();
            }
            sb7.append(string7);
            final String string8 = sb7.toString();
            final StringBuilder sb9 = new StringBuilder();
            sb9.append(str);
            sb9.append(string8);
            sb9.append(" ---  ");
            sb9.append(obj);
            Log.v("MotionLayout", sb9.toString());
        }
        
        private void setupConstraintWidget(final ConstraintWidgetContainer constraintWidgetContainer, final ConstraintSet set) {
            final SparseArray sparseArray = new SparseArray();
            final Constraints.LayoutParams layoutParams = new Constraints.LayoutParams(-2, -2);
            sparseArray.clear();
            sparseArray.put(0, (Object)constraintWidgetContainer);
            sparseArray.put(this.this$0.getId(), (Object)constraintWidgetContainer);
            if (set != null && set.mRotate != 0) {
                final MotionLayout this$0 = this.this$0;
                this$0.resolveSystem(this.mLayoutEnd, this$0.getOptimizationLevel(), View$MeasureSpec.makeMeasureSpec(this.this$0.getHeight(), 1073741824), View$MeasureSpec.makeMeasureSpec(this.this$0.getWidth(), 1073741824));
            }
            for (final ConstraintWidget constraintWidget : constraintWidgetContainer.getChildren()) {
                constraintWidget.setAnimated(true);
                sparseArray.put(((View)constraintWidget.getCompanionWidget()).getId(), (Object)constraintWidget);
            }
            for (final ConstraintWidget constraintWidget2 : constraintWidgetContainer.getChildren()) {
                final View view = (View)constraintWidget2.getCompanionWidget();
                set.applyToLayoutParams(view.getId(), layoutParams);
                constraintWidget2.setWidth(set.getWidth(view.getId()));
                constraintWidget2.setHeight(set.getHeight(view.getId()));
                if (view instanceof ConstraintHelper) {
                    set.applyToHelper((ConstraintHelper)view, constraintWidget2, layoutParams, (SparseArray<ConstraintWidget>)sparseArray);
                    if (view instanceof Barrier) {
                        ((Barrier)view).validateParams();
                    }
                }
                if (Build$VERSION.SDK_INT >= 17) {
                    ((LayoutParams)layoutParams).resolveLayoutDirection(this.this$0.getLayoutDirection());
                }
                else {
                    ((LayoutParams)layoutParams).resolveLayoutDirection(0);
                }
                this.this$0.applyConstraintsFromLayoutParams(false, view, constraintWidget2, layoutParams, (SparseArray<ConstraintWidget>)sparseArray);
                if (set.getVisibilityMode(view.getId()) == 1) {
                    constraintWidget2.setVisibility(view.getVisibility());
                }
                else {
                    constraintWidget2.setVisibility(set.getVisibility(view.getId()));
                }
            }
            for (final ConstraintWidget constraintWidget3 : constraintWidgetContainer.getChildren()) {
                if (constraintWidget3 instanceof VirtualLayout) {
                    final ConstraintHelper constraintHelper = (ConstraintHelper)constraintWidget3.getCompanionWidget();
                    final Helper helper = (Helper)constraintWidget3;
                    constraintHelper.updatePreLayout(constraintWidgetContainer, helper, (SparseArray<ConstraintWidget>)sparseArray);
                    ((VirtualLayout)helper).captureWidgets();
                }
            }
        }
        
        public void build() {
            final int childCount = this.this$0.getChildCount();
            this.this$0.mFrameArrayList.clear();
            final SparseArray sparseArray = new SparseArray();
            final int[] array = new int[childCount];
            for (int i = 0; i < childCount; ++i) {
                final View child = this.this$0.getChildAt(i);
                final MotionController value = new MotionController(child);
                sparseArray.put(array[i] = child.getId(), (Object)value);
                this.this$0.mFrameArrayList.put(child, value);
            }
            for (int j = 0; j < childCount; ++j) {
                final View child2 = this.this$0.getChildAt(j);
                final MotionController motionController = this.this$0.mFrameArrayList.get(child2);
                if (motionController != null) {
                    if (this.mStart != null) {
                        final ConstraintWidget widget = this.getWidget(this.mLayoutStart, child2);
                        if (widget != null) {
                            motionController.setStartState(this.this$0.toRect(widget), this.mStart, this.this$0.getWidth(), this.this$0.getHeight());
                        }
                        else if (this.this$0.mDebugPath != 0) {
                            final StringBuilder sb = new StringBuilder();
                            sb.append(Debug.getLocation());
                            sb.append("no widget for  ");
                            sb.append(Debug.getName(child2));
                            sb.append(" (");
                            sb.append(child2.getClass().getName());
                            sb.append(")");
                            Log.e("MotionLayout", sb.toString());
                        }
                    }
                    else if (this.this$0.mInRotation) {
                        motionController.setStartState(this.this$0.mPreRotate.get(child2), child2, this.this$0.mRotatMode, this.this$0.mPreRotateWidth, this.this$0.mPreRotateHeight);
                    }
                    if (this.mEnd != null) {
                        final ConstraintWidget widget2 = this.getWidget(this.mLayoutEnd, child2);
                        if (widget2 != null) {
                            motionController.setEndState(this.this$0.toRect(widget2), this.mEnd, this.this$0.getWidth(), this.this$0.getHeight());
                        }
                        else if (this.this$0.mDebugPath != 0) {
                            final StringBuilder sb2 = new StringBuilder();
                            sb2.append(Debug.getLocation());
                            sb2.append("no widget for  ");
                            sb2.append(Debug.getName(child2));
                            sb2.append(" (");
                            sb2.append(child2.getClass().getName());
                            sb2.append(")");
                            Log.e("MotionLayout", sb2.toString());
                        }
                    }
                }
            }
            for (int k = 0; k < childCount; ++k) {
                final MotionController motionController2 = (MotionController)sparseArray.get(array[k]);
                final int animateRelativeTo = motionController2.getAnimateRelativeTo();
                if (animateRelativeTo != -1) {
                    motionController2.setupRelative((MotionController)sparseArray.get(animateRelativeTo));
                }
            }
        }
        
        void copy(final ConstraintWidgetContainer key, final ConstraintWidgetContainer value) {
            final ArrayList<ConstraintWidget> children = key.getChildren();
            final HashMap hashMap = new HashMap();
            hashMap.put(key, value);
            value.getChildren().clear();
            value.copy(key, hashMap);
            for (final ConstraintWidget key2 : children) {
                ConstraintWidget value2;
                if (key2 instanceof androidx.constraintlayout.core.widgets.Barrier) {
                    value2 = new androidx.constraintlayout.core.widgets.Barrier();
                }
                else if (key2 instanceof Guideline) {
                    value2 = new Guideline();
                }
                else if (key2 instanceof Flow) {
                    value2 = new Flow();
                }
                else if (key2 instanceof Placeholder) {
                    value2 = new Placeholder();
                }
                else if (key2 instanceof Helper) {
                    value2 = new HelperWidget();
                }
                else {
                    value2 = new ConstraintWidget();
                }
                value.add(value2);
                hashMap.put(key2, value2);
            }
            for (final ConstraintWidget key3 : children) {
                ((ConstraintWidget)hashMap.get(key3)).copy(key3, hashMap);
            }
        }
        
        ConstraintWidget getWidget(final ConstraintWidgetContainer constraintWidgetContainer, final View view) {
            if (constraintWidgetContainer.getCompanionWidget() == view) {
                return constraintWidgetContainer;
            }
            final ArrayList<ConstraintWidget> children = constraintWidgetContainer.getChildren();
            for (int size = children.size(), i = 0; i < size; ++i) {
                final ConstraintWidget constraintWidget = children.get(i);
                if (constraintWidget.getCompanionWidget() == view) {
                    return constraintWidget;
                }
            }
            return null;
        }
        
        void initFrom(final ConstraintWidgetContainer constraintWidgetContainer, final ConstraintSet mStart, final ConstraintSet mEnd) {
            this.mStart = mStart;
            this.mEnd = mEnd;
            this.mLayoutStart = new ConstraintWidgetContainer();
            this.mLayoutEnd = new ConstraintWidgetContainer();
            this.mLayoutStart.setMeasurer(this.this$0.mLayoutWidget.getMeasurer());
            this.mLayoutEnd.setMeasurer(this.this$0.mLayoutWidget.getMeasurer());
            this.mLayoutStart.removeAllChildren();
            this.mLayoutEnd.removeAllChildren();
            this.copy(this.this$0.mLayoutWidget, this.mLayoutStart);
            this.copy(this.this$0.mLayoutWidget, this.mLayoutEnd);
            if (this.this$0.mTransitionLastPosition > 0.5) {
                if (mStart != null) {
                    this.setupConstraintWidget(this.mLayoutStart, mStart);
                }
                this.setupConstraintWidget(this.mLayoutEnd, mEnd);
            }
            else {
                this.setupConstraintWidget(this.mLayoutEnd, mEnd);
                if (mStart != null) {
                    this.setupConstraintWidget(this.mLayoutStart, mStart);
                }
            }
            this.mLayoutStart.setRtl(this.this$0.isRtl());
            this.mLayoutStart.updateHierarchy();
            this.mLayoutEnd.setRtl(this.this$0.isRtl());
            this.mLayoutEnd.updateHierarchy();
            final ViewGroup$LayoutParams layoutParams = this.this$0.getLayoutParams();
            if (layoutParams != null) {
                if (layoutParams.width == -2) {
                    this.mLayoutStart.setHorizontalDimensionBehaviour(ConstraintWidget.DimensionBehaviour.WRAP_CONTENT);
                    this.mLayoutEnd.setHorizontalDimensionBehaviour(ConstraintWidget.DimensionBehaviour.WRAP_CONTENT);
                }
                if (layoutParams.height == -2) {
                    this.mLayoutStart.setVerticalDimensionBehaviour(ConstraintWidget.DimensionBehaviour.WRAP_CONTENT);
                    this.mLayoutEnd.setVerticalDimensionBehaviour(ConstraintWidget.DimensionBehaviour.WRAP_CONTENT);
                }
            }
        }
        
        public boolean isNotConfiguredWith(final int n, final int n2) {
            return n != this.mStartId || n2 != this.mEndId;
        }
        
        public void measure(final int n, final int n2) {
            final int mode = View$MeasureSpec.getMode(n);
            final int mode2 = View$MeasureSpec.getMode(n2);
            this.this$0.mWidthMeasureMode = mode;
            this.this$0.mHeightMeasureMode = mode2;
            this.this$0.getOptimizationLevel();
            this.computeStartEndSize(n, n2);
            if (!(this.this$0.getParent() instanceof MotionLayout) || mode != 1073741824 || mode2 != 1073741824) {
                this.computeStartEndSize(n, n2);
                this.this$0.mStartWrapWidth = this.mLayoutStart.getWidth();
                this.this$0.mStartWrapHeight = this.mLayoutStart.getHeight();
                this.this$0.mEndWrapWidth = this.mLayoutEnd.getWidth();
                this.this$0.mEndWrapHeight = this.mLayoutEnd.getHeight();
                final MotionLayout this$0 = this.this$0;
                this$0.mMeasureDuringTransition = (this$0.mStartWrapWidth != this.this$0.mEndWrapWidth || this.this$0.mStartWrapHeight != this.this$0.mEndWrapHeight);
            }
            int mStartWrapWidth = this.this$0.mStartWrapWidth;
            int mStartWrapHeight = this.this$0.mStartWrapHeight;
            if (this.this$0.mWidthMeasureMode == Integer.MIN_VALUE || this.this$0.mWidthMeasureMode == 0) {
                mStartWrapWidth = (int)(this.this$0.mStartWrapWidth + this.this$0.mPostInterpolationPosition * (this.this$0.mEndWrapWidth - this.this$0.mStartWrapWidth));
            }
            if (this.this$0.mHeightMeasureMode == Integer.MIN_VALUE || this.this$0.mHeightMeasureMode == 0) {
                mStartWrapHeight = (int)(this.this$0.mStartWrapHeight + this.this$0.mPostInterpolationPosition * (this.this$0.mEndWrapHeight - this.this$0.mStartWrapHeight));
            }
            this.this$0.resolveMeasuredDimension(n, n2, mStartWrapWidth, mStartWrapHeight, this.mLayoutStart.isWidthMeasuredTooSmall() || this.mLayoutEnd.isWidthMeasuredTooSmall(), this.mLayoutStart.isHeightMeasuredTooSmall() || this.mLayoutEnd.isHeightMeasuredTooSmall());
        }
        
        public void reEvaluateState() {
            this.measure(this.this$0.mLastWidthMeasureSpec, this.this$0.mLastHeightMeasureSpec);
            this.this$0.setupMotionViews();
        }
        
        public void setMeasuredId(final int mStartId, final int mEndId) {
            this.mStartId = mStartId;
            this.mEndId = mEndId;
        }
    }
    
    protected interface MotionTracker
    {
        void addMovement(final MotionEvent p0);
        
        void clear();
        
        void computeCurrentVelocity(final int p0);
        
        void computeCurrentVelocity(final int p0, final float p1);
        
        float getXVelocity();
        
        float getXVelocity(final int p0);
        
        float getYVelocity();
        
        float getYVelocity(final int p0);
        
        void recycle();
    }
    
    private static class MyTracker implements MotionTracker
    {
        private static MyTracker me;
        VelocityTracker tracker;
        
        static {
            MyTracker.me = new MyTracker();
        }
        
        public static MyTracker obtain() {
            MyTracker.me.tracker = VelocityTracker.obtain();
            return MyTracker.me;
        }
        
        @Override
        public void addMovement(final MotionEvent motionEvent) {
            final VelocityTracker tracker = this.tracker;
            if (tracker != null) {
                tracker.addMovement(motionEvent);
            }
        }
        
        @Override
        public void clear() {
            final VelocityTracker tracker = this.tracker;
            if (tracker != null) {
                tracker.clear();
            }
        }
        
        @Override
        public void computeCurrentVelocity(final int n) {
            final VelocityTracker tracker = this.tracker;
            if (tracker != null) {
                tracker.computeCurrentVelocity(n);
            }
        }
        
        @Override
        public void computeCurrentVelocity(final int n, final float n2) {
            final VelocityTracker tracker = this.tracker;
            if (tracker != null) {
                tracker.computeCurrentVelocity(n, n2);
            }
        }
        
        @Override
        public float getXVelocity() {
            final VelocityTracker tracker = this.tracker;
            if (tracker != null) {
                return tracker.getXVelocity();
            }
            return 0.0f;
        }
        
        @Override
        public float getXVelocity(final int n) {
            final VelocityTracker tracker = this.tracker;
            if (tracker != null) {
                return tracker.getXVelocity(n);
            }
            return 0.0f;
        }
        
        @Override
        public float getYVelocity() {
            final VelocityTracker tracker = this.tracker;
            if (tracker != null) {
                return tracker.getYVelocity();
            }
            return 0.0f;
        }
        
        @Override
        public float getYVelocity(final int n) {
            if (this.tracker != null) {
                return this.getYVelocity(n);
            }
            return 0.0f;
        }
        
        @Override
        public void recycle() {
            final VelocityTracker tracker = this.tracker;
            if (tracker != null) {
                tracker.recycle();
                this.tracker = null;
            }
        }
    }
    
    class StateCache
    {
        final String KeyEndState;
        final String KeyProgress;
        final String KeyStartState;
        final String KeyVelocity;
        int endState;
        float mProgress;
        float mVelocity;
        int startState;
        final MotionLayout this$0;
        
        StateCache(final MotionLayout this$0) {
            this.this$0 = this$0;
            this.mProgress = Float.NaN;
            this.mVelocity = Float.NaN;
            this.startState = -1;
            this.endState = -1;
            this.KeyProgress = "motion.progress";
            this.KeyVelocity = "motion.velocity";
            this.KeyStartState = "motion.StartState";
            this.KeyEndState = "motion.EndState";
        }
        
        void apply() {
            final int startState = this.startState;
            if (startState != -1 || this.endState != -1) {
                if (startState == -1) {
                    this.this$0.transitionToState(this.endState);
                }
                else {
                    final int endState = this.endState;
                    if (endState == -1) {
                        this.this$0.setState(startState, -1, -1);
                    }
                    else {
                        this.this$0.setTransition(startState, endState);
                    }
                }
                this.this$0.setState(TransitionState.SETUP);
            }
            if (!Float.isNaN(this.mVelocity)) {
                this.this$0.setProgress(this.mProgress, this.mVelocity);
                this.mProgress = Float.NaN;
                this.mVelocity = Float.NaN;
                this.startState = -1;
                this.endState = -1;
                return;
            }
            if (Float.isNaN(this.mProgress)) {
                return;
            }
            this.this$0.setProgress(this.mProgress);
        }
        
        public Bundle getTransitionState() {
            final Bundle bundle = new Bundle();
            bundle.putFloat("motion.progress", this.mProgress);
            bundle.putFloat("motion.velocity", this.mVelocity);
            bundle.putInt("motion.StartState", this.startState);
            bundle.putInt("motion.EndState", this.endState);
            return bundle;
        }
        
        public void recordState() {
            this.endState = this.this$0.mEndState;
            this.startState = this.this$0.mBeginState;
            this.mVelocity = this.this$0.getVelocity();
            this.mProgress = this.this$0.getProgress();
        }
        
        public void setEndState(final int endState) {
            this.endState = endState;
        }
        
        public void setProgress(final float mProgress) {
            this.mProgress = mProgress;
        }
        
        public void setStartState(final int startState) {
            this.startState = startState;
        }
        
        public void setTransitionState(final Bundle bundle) {
            this.mProgress = bundle.getFloat("motion.progress");
            this.mVelocity = bundle.getFloat("motion.velocity");
            this.startState = bundle.getInt("motion.StartState");
            this.endState = bundle.getInt("motion.EndState");
        }
        
        public void setVelocity(final float mVelocity) {
            this.mVelocity = mVelocity;
        }
    }
    
    public interface TransitionListener
    {
        void onTransitionChange(final MotionLayout p0, final int p1, final int p2, final float p3);
        
        void onTransitionCompleted(final MotionLayout p0, final int p1);
        
        void onTransitionStarted(final MotionLayout p0, final int p1, final int p2);
        
        void onTransitionTrigger(final MotionLayout p0, final int p1, final boolean p2, final float p3);
    }
    
    enum TransitionState
    {
        private static final TransitionState[] $VALUES;
        
        FINISHED, 
        MOVING, 
        SETUP, 
        UNDEFINED;
        
        private static /* synthetic */ TransitionState[] $values() {
            return new TransitionState[] { TransitionState.UNDEFINED, TransitionState.SETUP, TransitionState.MOVING, TransitionState.FINISHED };
        }
        
        static {
            $VALUES = $values();
        }
    }
}

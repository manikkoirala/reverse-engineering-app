// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.motion.widget;

import android.os.Build$VERSION;
import android.util.Log;
import android.content.res.TypedArray;
import android.util.SparseIntArray;
import androidx.constraintlayout.widget.R;
import android.util.AttributeSet;
import android.content.Context;
import java.util.HashSet;
import java.util.Iterator;
import androidx.constraintlayout.core.motion.utils.SplineSet;
import androidx.constraintlayout.motion.utils.ViewSpline;
import androidx.constraintlayout.widget.ConstraintAttribute;
import java.util.HashMap;

public class KeyAttributes extends Key
{
    private static final boolean DEBUG = false;
    public static final int KEY_TYPE = 1;
    static final String NAME = "KeyAttribute";
    private static final String TAG = "KeyAttributes";
    private float mAlpha;
    private int mCurveFit;
    private float mElevation;
    private float mPivotX;
    private float mPivotY;
    private float mProgress;
    private float mRotation;
    private float mRotationX;
    private float mRotationY;
    private float mScaleX;
    private float mScaleY;
    private String mTransitionEasing;
    private float mTransitionPathRotate;
    private float mTranslationX;
    private float mTranslationY;
    private float mTranslationZ;
    private boolean mVisibility;
    
    public KeyAttributes() {
        this.mCurveFit = -1;
        this.mVisibility = false;
        this.mAlpha = Float.NaN;
        this.mElevation = Float.NaN;
        this.mRotation = Float.NaN;
        this.mRotationX = Float.NaN;
        this.mRotationY = Float.NaN;
        this.mPivotX = Float.NaN;
        this.mPivotY = Float.NaN;
        this.mTransitionPathRotate = Float.NaN;
        this.mScaleX = Float.NaN;
        this.mScaleY = Float.NaN;
        this.mTranslationX = Float.NaN;
        this.mTranslationY = Float.NaN;
        this.mTranslationZ = Float.NaN;
        this.mProgress = Float.NaN;
        this.mType = 1;
        this.mCustomConstraints = new HashMap<String, ConstraintAttribute>();
    }
    
    @Override
    public void addValues(final HashMap<String, ViewSpline> hashMap) {
        for (final String key : hashMap.keySet()) {
            final SplineSet set = hashMap.get(key);
            if (set == null) {
                continue;
            }
            final boolean startsWith = key.startsWith("CUSTOM");
            int n = 7;
            if (startsWith) {
                final ConstraintAttribute constraintAttribute = this.mCustomConstraints.get(key.substring(7));
                if (constraintAttribute == null) {
                    continue;
                }
                ((ViewSpline.CustomSet)set).setPoint(this.mFramePosition, constraintAttribute);
            }
            else {
                key.hashCode();
                Label_0500: {
                    switch (key) {
                        case "alpha": {
                            n = 13;
                            break Label_0500;
                        }
                        case "transitionPathRotate": {
                            n = 12;
                            break Label_0500;
                        }
                        case "elevation": {
                            n = 11;
                            break Label_0500;
                        }
                        case "rotation": {
                            n = 10;
                            break Label_0500;
                        }
                        case "transformPivotY": {
                            n = 9;
                            break Label_0500;
                        }
                        case "transformPivotX": {
                            n = 8;
                            break Label_0500;
                        }
                        case "scaleY": {
                            break Label_0500;
                        }
                        case "scaleX": {
                            n = 6;
                            break Label_0500;
                        }
                        case "progress": {
                            n = 5;
                            break Label_0500;
                        }
                        case "translationZ": {
                            n = 4;
                            break Label_0500;
                        }
                        case "translationY": {
                            n = 3;
                            break Label_0500;
                        }
                        case "translationX": {
                            n = 2;
                            break Label_0500;
                        }
                        case "rotationY": {
                            n = 1;
                            break Label_0500;
                        }
                        case "rotationX": {
                            n = 0;
                            break Label_0500;
                        }
                        default:
                            break;
                    }
                    n = -1;
                }
                switch (n) {
                    default: {
                        continue;
                    }
                    case 13: {
                        if (!Float.isNaN(this.mAlpha)) {
                            set.setPoint(this.mFramePosition, this.mAlpha);
                            continue;
                        }
                        continue;
                    }
                    case 12: {
                        if (!Float.isNaN(this.mTransitionPathRotate)) {
                            set.setPoint(this.mFramePosition, this.mTransitionPathRotate);
                            continue;
                        }
                        continue;
                    }
                    case 11: {
                        if (!Float.isNaN(this.mElevation)) {
                            set.setPoint(this.mFramePosition, this.mElevation);
                            continue;
                        }
                        continue;
                    }
                    case 10: {
                        if (!Float.isNaN(this.mRotation)) {
                            set.setPoint(this.mFramePosition, this.mRotation);
                            continue;
                        }
                        continue;
                    }
                    case 9: {
                        if (!Float.isNaN(this.mRotationY)) {
                            set.setPoint(this.mFramePosition, this.mPivotY);
                            continue;
                        }
                        continue;
                    }
                    case 8: {
                        if (!Float.isNaN(this.mRotationX)) {
                            set.setPoint(this.mFramePosition, this.mPivotX);
                            continue;
                        }
                        continue;
                    }
                    case 7: {
                        if (!Float.isNaN(this.mScaleY)) {
                            set.setPoint(this.mFramePosition, this.mScaleY);
                            continue;
                        }
                        continue;
                    }
                    case 6: {
                        if (!Float.isNaN(this.mScaleX)) {
                            set.setPoint(this.mFramePosition, this.mScaleX);
                            continue;
                        }
                        continue;
                    }
                    case 5: {
                        if (!Float.isNaN(this.mProgress)) {
                            set.setPoint(this.mFramePosition, this.mProgress);
                            continue;
                        }
                        continue;
                    }
                    case 4: {
                        if (!Float.isNaN(this.mTranslationZ)) {
                            set.setPoint(this.mFramePosition, this.mTranslationZ);
                            continue;
                        }
                        continue;
                    }
                    case 3: {
                        if (!Float.isNaN(this.mTranslationY)) {
                            set.setPoint(this.mFramePosition, this.mTranslationY);
                            continue;
                        }
                        continue;
                    }
                    case 2: {
                        if (!Float.isNaN(this.mTranslationX)) {
                            set.setPoint(this.mFramePosition, this.mTranslationX);
                            continue;
                        }
                        continue;
                    }
                    case 1: {
                        if (!Float.isNaN(this.mRotationY)) {
                            set.setPoint(this.mFramePosition, this.mRotationY);
                            continue;
                        }
                        continue;
                    }
                    case 0: {
                        if (!Float.isNaN(this.mRotationX)) {
                            set.setPoint(this.mFramePosition, this.mRotationX);
                            continue;
                        }
                        continue;
                    }
                }
            }
        }
    }
    
    @Override
    public Key clone() {
        return new KeyAttributes().copy(this);
    }
    
    @Override
    public Key copy(final Key key) {
        super.copy(key);
        final KeyAttributes keyAttributes = (KeyAttributes)key;
        this.mCurveFit = keyAttributes.mCurveFit;
        this.mVisibility = keyAttributes.mVisibility;
        this.mAlpha = keyAttributes.mAlpha;
        this.mElevation = keyAttributes.mElevation;
        this.mRotation = keyAttributes.mRotation;
        this.mRotationX = keyAttributes.mRotationX;
        this.mRotationY = keyAttributes.mRotationY;
        this.mPivotX = keyAttributes.mPivotX;
        this.mPivotY = keyAttributes.mPivotY;
        this.mTransitionPathRotate = keyAttributes.mTransitionPathRotate;
        this.mScaleX = keyAttributes.mScaleX;
        this.mScaleY = keyAttributes.mScaleY;
        this.mTranslationX = keyAttributes.mTranslationX;
        this.mTranslationY = keyAttributes.mTranslationY;
        this.mTranslationZ = keyAttributes.mTranslationZ;
        this.mProgress = keyAttributes.mProgress;
        return this;
    }
    
    public void getAttributeNames(final HashSet<String> set) {
        if (!Float.isNaN(this.mAlpha)) {
            set.add("alpha");
        }
        if (!Float.isNaN(this.mElevation)) {
            set.add("elevation");
        }
        if (!Float.isNaN(this.mRotation)) {
            set.add("rotation");
        }
        if (!Float.isNaN(this.mRotationX)) {
            set.add("rotationX");
        }
        if (!Float.isNaN(this.mRotationY)) {
            set.add("rotationY");
        }
        if (!Float.isNaN(this.mPivotX)) {
            set.add("transformPivotX");
        }
        if (!Float.isNaN(this.mPivotY)) {
            set.add("transformPivotY");
        }
        if (!Float.isNaN(this.mTranslationX)) {
            set.add("translationX");
        }
        if (!Float.isNaN(this.mTranslationY)) {
            set.add("translationY");
        }
        if (!Float.isNaN(this.mTranslationZ)) {
            set.add("translationZ");
        }
        if (!Float.isNaN(this.mTransitionPathRotate)) {
            set.add("transitionPathRotate");
        }
        if (!Float.isNaN(this.mScaleX)) {
            set.add("scaleX");
        }
        if (!Float.isNaN(this.mScaleY)) {
            set.add("scaleY");
        }
        if (!Float.isNaN(this.mProgress)) {
            set.add("progress");
        }
        if (this.mCustomConstraints.size() > 0) {
            for (final String str : this.mCustomConstraints.keySet()) {
                final StringBuilder sb = new StringBuilder();
                sb.append("CUSTOM,");
                sb.append(str);
                set.add(sb.toString());
            }
        }
    }
    
    int getCurveFit() {
        return this.mCurveFit;
    }
    
    public void load(final Context context, final AttributeSet set) {
        Loader.read(this, context.obtainStyledAttributes(set, R.styleable.KeyAttribute));
    }
    
    @Override
    public void setInterpolation(final HashMap<String, Integer> hashMap) {
        if (this.mCurveFit == -1) {
            return;
        }
        if (!Float.isNaN(this.mAlpha)) {
            hashMap.put("alpha", this.mCurveFit);
        }
        if (!Float.isNaN(this.mElevation)) {
            hashMap.put("elevation", this.mCurveFit);
        }
        if (!Float.isNaN(this.mRotation)) {
            hashMap.put("rotation", this.mCurveFit);
        }
        if (!Float.isNaN(this.mRotationX)) {
            hashMap.put("rotationX", this.mCurveFit);
        }
        if (!Float.isNaN(this.mRotationY)) {
            hashMap.put("rotationY", this.mCurveFit);
        }
        if (!Float.isNaN(this.mPivotX)) {
            hashMap.put("transformPivotX", this.mCurveFit);
        }
        if (!Float.isNaN(this.mPivotY)) {
            hashMap.put("transformPivotY", this.mCurveFit);
        }
        if (!Float.isNaN(this.mTranslationX)) {
            hashMap.put("translationX", this.mCurveFit);
        }
        if (!Float.isNaN(this.mTranslationY)) {
            hashMap.put("translationY", this.mCurveFit);
        }
        if (!Float.isNaN(this.mTranslationZ)) {
            hashMap.put("translationZ", this.mCurveFit);
        }
        if (!Float.isNaN(this.mTransitionPathRotate)) {
            hashMap.put("transitionPathRotate", this.mCurveFit);
        }
        if (!Float.isNaN(this.mScaleX)) {
            hashMap.put("scaleX", this.mCurveFit);
        }
        if (!Float.isNaN(this.mScaleY)) {
            hashMap.put("scaleY", this.mCurveFit);
        }
        if (!Float.isNaN(this.mProgress)) {
            hashMap.put("progress", this.mCurveFit);
        }
        if (this.mCustomConstraints.size() > 0) {
            for (final String str : this.mCustomConstraints.keySet()) {
                final StringBuilder sb = new StringBuilder();
                sb.append("CUSTOM,");
                sb.append(str);
                hashMap.put(sb.toString(), this.mCurveFit);
            }
        }
    }
    
    @Override
    public void setValue(final String s, final Object o) {
        s.hashCode();
        final int hashCode = s.hashCode();
        int n = -1;
        switch (hashCode) {
            case 1941332754: {
                if (!s.equals("visibility")) {
                    break;
                }
                n = 16;
                break;
            }
            case 579057826: {
                if (!s.equals("curveFit")) {
                    break;
                }
                n = 15;
                break;
            }
            case 92909918: {
                if (!s.equals("alpha")) {
                    break;
                }
                n = 14;
                break;
            }
            case 37232917: {
                if (!s.equals("transitionPathRotate")) {
                    break;
                }
                n = 13;
                break;
            }
            case -4379043: {
                if (!s.equals("elevation")) {
                    break;
                }
                n = 12;
                break;
            }
            case -40300674: {
                if (!s.equals("rotation")) {
                    break;
                }
                n = 11;
                break;
            }
            case -760884509: {
                if (!s.equals("transformPivotY")) {
                    break;
                }
                n = 10;
                break;
            }
            case -760884510: {
                if (!s.equals("transformPivotX")) {
                    break;
                }
                n = 9;
                break;
            }
            case -908189617: {
                if (!s.equals("scaleY")) {
                    break;
                }
                n = 8;
                break;
            }
            case -908189618: {
                if (!s.equals("scaleX")) {
                    break;
                }
                n = 7;
                break;
            }
            case -1225497655: {
                if (!s.equals("translationZ")) {
                    break;
                }
                n = 6;
                break;
            }
            case -1225497656: {
                if (!s.equals("translationY")) {
                    break;
                }
                n = 5;
                break;
            }
            case -1225497657: {
                if (!s.equals("translationX")) {
                    break;
                }
                n = 4;
                break;
            }
            case -1249320805: {
                if (!s.equals("rotationY")) {
                    break;
                }
                n = 3;
                break;
            }
            case -1249320806: {
                if (!s.equals("rotationX")) {
                    break;
                }
                n = 2;
                break;
            }
            case -1812823328: {
                if (!s.equals("transitionEasing")) {
                    break;
                }
                n = 1;
                break;
            }
            case -1913008125: {
                if (!s.equals("motionProgress")) {
                    break;
                }
                n = 0;
                break;
            }
        }
        switch (n) {
            case 16: {
                this.mVisibility = this.toBoolean(o);
                break;
            }
            case 15: {
                this.mCurveFit = this.toInt(o);
                break;
            }
            case 14: {
                this.mAlpha = this.toFloat(o);
                break;
            }
            case 13: {
                this.mTransitionPathRotate = this.toFloat(o);
                break;
            }
            case 12: {
                this.mElevation = this.toFloat(o);
                break;
            }
            case 11: {
                this.mRotation = this.toFloat(o);
                break;
            }
            case 10: {
                this.mPivotY = this.toFloat(o);
                break;
            }
            case 9: {
                this.mPivotX = this.toFloat(o);
                break;
            }
            case 8: {
                this.mScaleY = this.toFloat(o);
                break;
            }
            case 7: {
                this.mScaleX = this.toFloat(o);
                break;
            }
            case 6: {
                this.mTranslationZ = this.toFloat(o);
                break;
            }
            case 5: {
                this.mTranslationY = this.toFloat(o);
                break;
            }
            case 4: {
                this.mTranslationX = this.toFloat(o);
                break;
            }
            case 3: {
                this.mRotationY = this.toFloat(o);
                break;
            }
            case 2: {
                this.mRotationX = this.toFloat(o);
                break;
            }
            case 1: {
                this.mTransitionEasing = o.toString();
                break;
            }
            case 0: {
                this.mProgress = this.toFloat(o);
                break;
            }
        }
    }
    
    private static class Loader
    {
        private static final int ANDROID_ALPHA = 1;
        private static final int ANDROID_ELEVATION = 2;
        private static final int ANDROID_PIVOT_X = 19;
        private static final int ANDROID_PIVOT_Y = 20;
        private static final int ANDROID_ROTATION = 4;
        private static final int ANDROID_ROTATION_X = 5;
        private static final int ANDROID_ROTATION_Y = 6;
        private static final int ANDROID_SCALE_X = 7;
        private static final int ANDROID_SCALE_Y = 14;
        private static final int ANDROID_TRANSLATION_X = 15;
        private static final int ANDROID_TRANSLATION_Y = 16;
        private static final int ANDROID_TRANSLATION_Z = 17;
        private static final int CURVE_FIT = 13;
        private static final int FRAME_POSITION = 12;
        private static final int PROGRESS = 18;
        private static final int TARGET_ID = 10;
        private static final int TRANSITION_EASING = 9;
        private static final int TRANSITION_PATH_ROTATE = 8;
        private static SparseIntArray mAttrMap;
        
        static {
            (Loader.mAttrMap = new SparseIntArray()).append(R.styleable.KeyAttribute_android_alpha, 1);
            Loader.mAttrMap.append(R.styleable.KeyAttribute_android_elevation, 2);
            Loader.mAttrMap.append(R.styleable.KeyAttribute_android_rotation, 4);
            Loader.mAttrMap.append(R.styleable.KeyAttribute_android_rotationX, 5);
            Loader.mAttrMap.append(R.styleable.KeyAttribute_android_rotationY, 6);
            Loader.mAttrMap.append(R.styleable.KeyAttribute_android_transformPivotX, 19);
            Loader.mAttrMap.append(R.styleable.KeyAttribute_android_transformPivotY, 20);
            Loader.mAttrMap.append(R.styleable.KeyAttribute_android_scaleX, 7);
            Loader.mAttrMap.append(R.styleable.KeyAttribute_transitionPathRotate, 8);
            Loader.mAttrMap.append(R.styleable.KeyAttribute_transitionEasing, 9);
            Loader.mAttrMap.append(R.styleable.KeyAttribute_motionTarget, 10);
            Loader.mAttrMap.append(R.styleable.KeyAttribute_framePosition, 12);
            Loader.mAttrMap.append(R.styleable.KeyAttribute_curveFit, 13);
            Loader.mAttrMap.append(R.styleable.KeyAttribute_android_scaleY, 14);
            Loader.mAttrMap.append(R.styleable.KeyAttribute_android_translationX, 15);
            Loader.mAttrMap.append(R.styleable.KeyAttribute_android_translationY, 16);
            Loader.mAttrMap.append(R.styleable.KeyAttribute_android_translationZ, 17);
            Loader.mAttrMap.append(R.styleable.KeyAttribute_motionProgress, 18);
        }
        
        public static void read(final KeyAttributes keyAttributes, final TypedArray typedArray) {
            for (int indexCount = typedArray.getIndexCount(), i = 0; i < indexCount; ++i) {
                final int index = typedArray.getIndex(i);
                switch (Loader.mAttrMap.get(index)) {
                    default: {
                        final StringBuilder sb = new StringBuilder();
                        sb.append("unused attribute 0x");
                        sb.append(Integer.toHexString(index));
                        sb.append("   ");
                        sb.append(Loader.mAttrMap.get(index));
                        Log.e("KeyAttribute", sb.toString());
                        break;
                    }
                    case 20: {
                        keyAttributes.mPivotY = typedArray.getDimension(index, keyAttributes.mPivotY);
                        break;
                    }
                    case 19: {
                        keyAttributes.mPivotX = typedArray.getDimension(index, keyAttributes.mPivotX);
                        break;
                    }
                    case 18: {
                        keyAttributes.mProgress = typedArray.getFloat(index, keyAttributes.mProgress);
                        break;
                    }
                    case 17: {
                        if (Build$VERSION.SDK_INT >= 21) {
                            keyAttributes.mTranslationZ = typedArray.getDimension(index, keyAttributes.mTranslationZ);
                            break;
                        }
                        break;
                    }
                    case 16: {
                        keyAttributes.mTranslationY = typedArray.getDimension(index, keyAttributes.mTranslationY);
                        break;
                    }
                    case 15: {
                        keyAttributes.mTranslationX = typedArray.getDimension(index, keyAttributes.mTranslationX);
                        break;
                    }
                    case 14: {
                        keyAttributes.mScaleY = typedArray.getFloat(index, keyAttributes.mScaleY);
                        break;
                    }
                    case 13: {
                        keyAttributes.mCurveFit = typedArray.getInteger(index, keyAttributes.mCurveFit);
                        break;
                    }
                    case 12: {
                        keyAttributes.mFramePosition = typedArray.getInt(index, keyAttributes.mFramePosition);
                        break;
                    }
                    case 10: {
                        if (MotionLayout.IS_IN_EDIT_MODE) {
                            keyAttributes.mTargetId = typedArray.getResourceId(index, keyAttributes.mTargetId);
                            if (keyAttributes.mTargetId == -1) {
                                keyAttributes.mTargetString = typedArray.getString(index);
                                break;
                            }
                            break;
                        }
                        else {
                            if (typedArray.peekValue(index).type == 3) {
                                keyAttributes.mTargetString = typedArray.getString(index);
                                break;
                            }
                            keyAttributes.mTargetId = typedArray.getResourceId(index, keyAttributes.mTargetId);
                            break;
                        }
                        break;
                    }
                    case 9: {
                        keyAttributes.mTransitionEasing = typedArray.getString(index);
                        break;
                    }
                    case 8: {
                        keyAttributes.mTransitionPathRotate = typedArray.getFloat(index, keyAttributes.mTransitionPathRotate);
                        break;
                    }
                    case 7: {
                        keyAttributes.mScaleX = typedArray.getFloat(index, keyAttributes.mScaleX);
                        break;
                    }
                    case 6: {
                        keyAttributes.mRotationY = typedArray.getFloat(index, keyAttributes.mRotationY);
                        break;
                    }
                    case 5: {
                        keyAttributes.mRotationX = typedArray.getFloat(index, keyAttributes.mRotationX);
                        break;
                    }
                    case 4: {
                        keyAttributes.mRotation = typedArray.getFloat(index, keyAttributes.mRotation);
                        break;
                    }
                    case 2: {
                        keyAttributes.mElevation = typedArray.getDimension(index, keyAttributes.mElevation);
                        break;
                    }
                    case 1: {
                        keyAttributes.mAlpha = typedArray.getFloat(index, keyAttributes.mAlpha);
                        break;
                    }
                }
            }
        }
    }
}

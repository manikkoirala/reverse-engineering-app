// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.motion.widget;

import android.view.ViewGroup;
import androidx.constraintlayout.widget.ConstraintLayout;
import java.util.HashMap;
import android.graphics.Canvas;
import android.content.res.TypedArray;
import androidx.constraintlayout.widget.R;
import android.util.AttributeSet;
import android.content.Context;
import android.view.View;
import androidx.constraintlayout.widget.ConstraintHelper;

public class MotionHelper extends ConstraintHelper implements MotionHelperInterface
{
    private float mProgress;
    private boolean mUseOnHide;
    private boolean mUseOnShow;
    protected View[] views;
    
    public MotionHelper(final Context context) {
        super(context);
        this.mUseOnShow = false;
        this.mUseOnHide = false;
    }
    
    public MotionHelper(final Context context, final AttributeSet set) {
        super(context, set);
        this.mUseOnShow = false;
        this.mUseOnHide = false;
        this.init(set);
    }
    
    public MotionHelper(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
        this.mUseOnShow = false;
        this.mUseOnHide = false;
        this.init(set);
    }
    
    public float getProgress() {
        return this.mProgress;
    }
    
    @Override
    protected void init(final AttributeSet set) {
        super.init(set);
        if (set != null) {
            final TypedArray obtainStyledAttributes = this.getContext().obtainStyledAttributes(set, R.styleable.MotionHelper);
            for (int indexCount = obtainStyledAttributes.getIndexCount(), i = 0; i < indexCount; ++i) {
                final int index = obtainStyledAttributes.getIndex(i);
                if (index == R.styleable.MotionHelper_onShow) {
                    this.mUseOnShow = obtainStyledAttributes.getBoolean(index, this.mUseOnShow);
                }
                else if (index == R.styleable.MotionHelper_onHide) {
                    this.mUseOnHide = obtainStyledAttributes.getBoolean(index, this.mUseOnHide);
                }
            }
            obtainStyledAttributes.recycle();
        }
    }
    
    @Override
    public boolean isDecorator() {
        return false;
    }
    
    @Override
    public boolean isUseOnHide() {
        return this.mUseOnHide;
    }
    
    @Override
    public boolean isUsedOnShow() {
        return this.mUseOnShow;
    }
    
    @Override
    public void onFinishedMotionScene(final MotionLayout motionLayout) {
    }
    
    @Override
    public void onPostDraw(final Canvas canvas) {
    }
    
    @Override
    public void onPreDraw(final Canvas canvas) {
    }
    
    @Override
    public void onPreSetup(final MotionLayout motionLayout, final HashMap<View, MotionController> hashMap) {
    }
    
    public void onTransitionChange(final MotionLayout motionLayout, final int n, final int n2, final float n3) {
    }
    
    public void onTransitionCompleted(final MotionLayout motionLayout, final int n) {
    }
    
    public void onTransitionStarted(final MotionLayout motionLayout, final int n, final int n2) {
    }
    
    public void onTransitionTrigger(final MotionLayout motionLayout, final int n, final boolean b, final float n2) {
    }
    
    public void setProgress(final float mProgress) {
        this.mProgress = mProgress;
        final int mCount = this.mCount;
        int i = 0;
        final int n = 0;
        if (mCount > 0) {
            this.views = this.getViews((ConstraintLayout)this.getParent());
            for (int j = n; j < this.mCount; ++j) {
                this.setProgress(this.views[j], mProgress);
            }
        }
        else {
            for (ViewGroup viewGroup = (ViewGroup)this.getParent(); i < viewGroup.getChildCount(); ++i) {
                final View child = viewGroup.getChildAt(i);
                if (!(child instanceof MotionHelper)) {
                    this.setProgress(child, mProgress);
                }
            }
        }
    }
    
    public void setProgress(final View view, final float n) {
    }
}

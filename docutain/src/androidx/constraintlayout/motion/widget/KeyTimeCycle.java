// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.motion.widget;

import android.os.Build$VERSION;
import android.content.res.TypedArray;
import android.util.SparseIntArray;
import androidx.constraintlayout.widget.R;
import android.util.AttributeSet;
import android.content.Context;
import java.util.HashSet;
import androidx.constraintlayout.motion.utils.ViewSpline;
import java.util.Iterator;
import android.util.Log;
import androidx.constraintlayout.motion.utils.ViewTimeCycle;
import androidx.constraintlayout.widget.ConstraintAttribute;
import java.util.HashMap;

public class KeyTimeCycle extends Key
{
    public static final int KEY_TYPE = 3;
    static final String NAME = "KeyTimeCycle";
    public static final int SHAPE_BOUNCE = 6;
    public static final int SHAPE_COS_WAVE = 5;
    public static final int SHAPE_REVERSE_SAW_WAVE = 4;
    public static final int SHAPE_SAW_WAVE = 3;
    public static final int SHAPE_SIN_WAVE = 0;
    public static final int SHAPE_SQUARE_WAVE = 1;
    public static final int SHAPE_TRIANGLE_WAVE = 2;
    private static final String TAG = "KeyTimeCycle";
    public static final String WAVE_OFFSET = "waveOffset";
    public static final String WAVE_PERIOD = "wavePeriod";
    public static final String WAVE_SHAPE = "waveShape";
    private float mAlpha;
    private int mCurveFit;
    private String mCustomWaveShape;
    private float mElevation;
    private float mProgress;
    private float mRotation;
    private float mRotationX;
    private float mRotationY;
    private float mScaleX;
    private float mScaleY;
    private String mTransitionEasing;
    private float mTransitionPathRotate;
    private float mTranslationX;
    private float mTranslationY;
    private float mTranslationZ;
    private float mWaveOffset;
    private float mWavePeriod;
    private int mWaveShape;
    
    public KeyTimeCycle() {
        this.mCurveFit = -1;
        this.mAlpha = Float.NaN;
        this.mElevation = Float.NaN;
        this.mRotation = Float.NaN;
        this.mRotationX = Float.NaN;
        this.mRotationY = Float.NaN;
        this.mTransitionPathRotate = Float.NaN;
        this.mScaleX = Float.NaN;
        this.mScaleY = Float.NaN;
        this.mTranslationX = Float.NaN;
        this.mTranslationY = Float.NaN;
        this.mTranslationZ = Float.NaN;
        this.mProgress = Float.NaN;
        this.mWaveShape = 0;
        this.mCustomWaveShape = null;
        this.mWavePeriod = Float.NaN;
        this.mWaveOffset = 0.0f;
        this.mType = 3;
        this.mCustomConstraints = new HashMap<String, ConstraintAttribute>();
    }
    
    public void addTimeValues(final HashMap<String, ViewTimeCycle> hashMap) {
        for (final String s : hashMap.keySet()) {
            final ViewTimeCycle viewTimeCycle = hashMap.get(s);
            if (viewTimeCycle == null) {
                continue;
            }
            final boolean startsWith = s.startsWith("CUSTOM");
            int n = 7;
            if (startsWith) {
                final ConstraintAttribute constraintAttribute = this.mCustomConstraints.get(s.substring(7));
                if (constraintAttribute == null) {
                    continue;
                }
                ((ViewTimeCycle.CustomSet)viewTimeCycle).setPoint(this.mFramePosition, constraintAttribute, this.mWavePeriod, this.mWaveShape, this.mWaveOffset);
            }
            else {
                s.hashCode();
                Label_0458: {
                    switch (s) {
                        case "alpha": {
                            n = 11;
                            break Label_0458;
                        }
                        case "transitionPathRotate": {
                            n = 10;
                            break Label_0458;
                        }
                        case "elevation": {
                            n = 9;
                            break Label_0458;
                        }
                        case "rotation": {
                            n = 8;
                            break Label_0458;
                        }
                        case "scaleY": {
                            break Label_0458;
                        }
                        case "scaleX": {
                            n = 6;
                            break Label_0458;
                        }
                        case "progress": {
                            n = 5;
                            break Label_0458;
                        }
                        case "translationZ": {
                            n = 4;
                            break Label_0458;
                        }
                        case "translationY": {
                            n = 3;
                            break Label_0458;
                        }
                        case "translationX": {
                            n = 2;
                            break Label_0458;
                        }
                        case "rotationY": {
                            n = 1;
                            break Label_0458;
                        }
                        case "rotationX": {
                            n = 0;
                            break Label_0458;
                        }
                        default:
                            break;
                    }
                    n = -1;
                }
                switch (n) {
                    default: {
                        final StringBuilder sb = new StringBuilder();
                        sb.append("UNKNOWN addValues \"");
                        sb.append(s);
                        sb.append("\"");
                        Log.e("KeyTimeCycles", sb.toString());
                        continue;
                    }
                    case 11: {
                        if (!Float.isNaN(this.mAlpha)) {
                            viewTimeCycle.setPoint(this.mFramePosition, this.mAlpha, this.mWavePeriod, this.mWaveShape, this.mWaveOffset);
                            continue;
                        }
                        continue;
                    }
                    case 10: {
                        if (!Float.isNaN(this.mTransitionPathRotate)) {
                            viewTimeCycle.setPoint(this.mFramePosition, this.mTransitionPathRotate, this.mWavePeriod, this.mWaveShape, this.mWaveOffset);
                            continue;
                        }
                        continue;
                    }
                    case 9: {
                        if (!Float.isNaN(this.mElevation)) {
                            viewTimeCycle.setPoint(this.mFramePosition, this.mElevation, this.mWavePeriod, this.mWaveShape, this.mWaveOffset);
                            continue;
                        }
                        continue;
                    }
                    case 8: {
                        if (!Float.isNaN(this.mRotation)) {
                            viewTimeCycle.setPoint(this.mFramePosition, this.mRotation, this.mWavePeriod, this.mWaveShape, this.mWaveOffset);
                            continue;
                        }
                        continue;
                    }
                    case 7: {
                        if (!Float.isNaN(this.mScaleY)) {
                            viewTimeCycle.setPoint(this.mFramePosition, this.mScaleY, this.mWavePeriod, this.mWaveShape, this.mWaveOffset);
                            continue;
                        }
                        continue;
                    }
                    case 6: {
                        if (!Float.isNaN(this.mScaleX)) {
                            viewTimeCycle.setPoint(this.mFramePosition, this.mScaleX, this.mWavePeriod, this.mWaveShape, this.mWaveOffset);
                            continue;
                        }
                        continue;
                    }
                    case 5: {
                        if (!Float.isNaN(this.mProgress)) {
                            viewTimeCycle.setPoint(this.mFramePosition, this.mProgress, this.mWavePeriod, this.mWaveShape, this.mWaveOffset);
                            continue;
                        }
                        continue;
                    }
                    case 4: {
                        if (!Float.isNaN(this.mTranslationZ)) {
                            viewTimeCycle.setPoint(this.mFramePosition, this.mTranslationZ, this.mWavePeriod, this.mWaveShape, this.mWaveOffset);
                            continue;
                        }
                        continue;
                    }
                    case 3: {
                        if (!Float.isNaN(this.mTranslationY)) {
                            viewTimeCycle.setPoint(this.mFramePosition, this.mTranslationY, this.mWavePeriod, this.mWaveShape, this.mWaveOffset);
                            continue;
                        }
                        continue;
                    }
                    case 2: {
                        if (!Float.isNaN(this.mTranslationX)) {
                            viewTimeCycle.setPoint(this.mFramePosition, this.mTranslationX, this.mWavePeriod, this.mWaveShape, this.mWaveOffset);
                            continue;
                        }
                        continue;
                    }
                    case 1: {
                        if (!Float.isNaN(this.mRotationY)) {
                            viewTimeCycle.setPoint(this.mFramePosition, this.mRotationY, this.mWavePeriod, this.mWaveShape, this.mWaveOffset);
                            continue;
                        }
                        continue;
                    }
                    case 0: {
                        if (!Float.isNaN(this.mRotationX)) {
                            viewTimeCycle.setPoint(this.mFramePosition, this.mRotationX, this.mWavePeriod, this.mWaveShape, this.mWaveOffset);
                            continue;
                        }
                        continue;
                    }
                }
            }
        }
    }
    
    @Override
    public void addValues(final HashMap<String, ViewSpline> hashMap) {
        throw new IllegalArgumentException(" KeyTimeCycles do not support SplineSet");
    }
    
    @Override
    public Key clone() {
        return new KeyTimeCycle().copy(this);
    }
    
    @Override
    public Key copy(final Key key) {
        super.copy(key);
        final KeyTimeCycle keyTimeCycle = (KeyTimeCycle)key;
        this.mTransitionEasing = keyTimeCycle.mTransitionEasing;
        this.mCurveFit = keyTimeCycle.mCurveFit;
        this.mWaveShape = keyTimeCycle.mWaveShape;
        this.mWavePeriod = keyTimeCycle.mWavePeriod;
        this.mWaveOffset = keyTimeCycle.mWaveOffset;
        this.mProgress = keyTimeCycle.mProgress;
        this.mAlpha = keyTimeCycle.mAlpha;
        this.mElevation = keyTimeCycle.mElevation;
        this.mRotation = keyTimeCycle.mRotation;
        this.mTransitionPathRotate = keyTimeCycle.mTransitionPathRotate;
        this.mRotationX = keyTimeCycle.mRotationX;
        this.mRotationY = keyTimeCycle.mRotationY;
        this.mScaleX = keyTimeCycle.mScaleX;
        this.mScaleY = keyTimeCycle.mScaleY;
        this.mTranslationX = keyTimeCycle.mTranslationX;
        this.mTranslationY = keyTimeCycle.mTranslationY;
        this.mTranslationZ = keyTimeCycle.mTranslationZ;
        return this;
    }
    
    public void getAttributeNames(final HashSet<String> set) {
        if (!Float.isNaN(this.mAlpha)) {
            set.add("alpha");
        }
        if (!Float.isNaN(this.mElevation)) {
            set.add("elevation");
        }
        if (!Float.isNaN(this.mRotation)) {
            set.add("rotation");
        }
        if (!Float.isNaN(this.mRotationX)) {
            set.add("rotationX");
        }
        if (!Float.isNaN(this.mRotationY)) {
            set.add("rotationY");
        }
        if (!Float.isNaN(this.mTranslationX)) {
            set.add("translationX");
        }
        if (!Float.isNaN(this.mTranslationY)) {
            set.add("translationY");
        }
        if (!Float.isNaN(this.mTranslationZ)) {
            set.add("translationZ");
        }
        if (!Float.isNaN(this.mTransitionPathRotate)) {
            set.add("transitionPathRotate");
        }
        if (!Float.isNaN(this.mScaleX)) {
            set.add("scaleX");
        }
        if (!Float.isNaN(this.mScaleY)) {
            set.add("scaleY");
        }
        if (!Float.isNaN(this.mProgress)) {
            set.add("progress");
        }
        if (this.mCustomConstraints.size() > 0) {
            for (final String str : this.mCustomConstraints.keySet()) {
                final StringBuilder sb = new StringBuilder();
                sb.append("CUSTOM,");
                sb.append(str);
                set.add(sb.toString());
            }
        }
    }
    
    public void load(final Context context, final AttributeSet set) {
        Loader.read(this, context.obtainStyledAttributes(set, R.styleable.KeyTimeCycle));
    }
    
    @Override
    public void setInterpolation(final HashMap<String, Integer> hashMap) {
        if (this.mCurveFit == -1) {
            return;
        }
        if (!Float.isNaN(this.mAlpha)) {
            hashMap.put("alpha", this.mCurveFit);
        }
        if (!Float.isNaN(this.mElevation)) {
            hashMap.put("elevation", this.mCurveFit);
        }
        if (!Float.isNaN(this.mRotation)) {
            hashMap.put("rotation", this.mCurveFit);
        }
        if (!Float.isNaN(this.mRotationX)) {
            hashMap.put("rotationX", this.mCurveFit);
        }
        if (!Float.isNaN(this.mRotationY)) {
            hashMap.put("rotationY", this.mCurveFit);
        }
        if (!Float.isNaN(this.mTranslationX)) {
            hashMap.put("translationX", this.mCurveFit);
        }
        if (!Float.isNaN(this.mTranslationY)) {
            hashMap.put("translationY", this.mCurveFit);
        }
        if (!Float.isNaN(this.mTranslationZ)) {
            hashMap.put("translationZ", this.mCurveFit);
        }
        if (!Float.isNaN(this.mTransitionPathRotate)) {
            hashMap.put("transitionPathRotate", this.mCurveFit);
        }
        if (!Float.isNaN(this.mScaleX)) {
            hashMap.put("scaleX", this.mCurveFit);
        }
        if (!Float.isNaN(this.mScaleX)) {
            hashMap.put("scaleY", this.mCurveFit);
        }
        if (!Float.isNaN(this.mProgress)) {
            hashMap.put("progress", this.mCurveFit);
        }
        if (this.mCustomConstraints.size() > 0) {
            for (final String str : this.mCustomConstraints.keySet()) {
                final StringBuilder sb = new StringBuilder();
                sb.append("CUSTOM,");
                sb.append(str);
                hashMap.put(sb.toString(), this.mCurveFit);
            }
        }
    }
    
    @Override
    public void setValue(final String s, final Object o) {
        s.hashCode();
        final int hashCode = s.hashCode();
        int n = -1;
        switch (hashCode) {
            case 1532805160: {
                if (!s.equals("waveShape")) {
                    break;
                }
                n = 16;
                break;
            }
            case 579057826: {
                if (!s.equals("curveFit")) {
                    break;
                }
                n = 15;
                break;
            }
            case 184161818: {
                if (!s.equals("wavePeriod")) {
                    break;
                }
                n = 14;
                break;
            }
            case 156108012: {
                if (!s.equals("waveOffset")) {
                    break;
                }
                n = 13;
                break;
            }
            case 92909918: {
                if (!s.equals("alpha")) {
                    break;
                }
                n = 12;
                break;
            }
            case 37232917: {
                if (!s.equals("transitionPathRotate")) {
                    break;
                }
                n = 11;
                break;
            }
            case -4379043: {
                if (!s.equals("elevation")) {
                    break;
                }
                n = 10;
                break;
            }
            case -40300674: {
                if (!s.equals("rotation")) {
                    break;
                }
                n = 9;
                break;
            }
            case -908189617: {
                if (!s.equals("scaleY")) {
                    break;
                }
                n = 8;
                break;
            }
            case -908189618: {
                if (!s.equals("scaleX")) {
                    break;
                }
                n = 7;
                break;
            }
            case -1225497655: {
                if (!s.equals("translationZ")) {
                    break;
                }
                n = 6;
                break;
            }
            case -1225497656: {
                if (!s.equals("translationY")) {
                    break;
                }
                n = 5;
                break;
            }
            case -1225497657: {
                if (!s.equals("translationX")) {
                    break;
                }
                n = 4;
                break;
            }
            case -1249320805: {
                if (!s.equals("rotationY")) {
                    break;
                }
                n = 3;
                break;
            }
            case -1249320806: {
                if (!s.equals("rotationX")) {
                    break;
                }
                n = 2;
                break;
            }
            case -1812823328: {
                if (!s.equals("transitionEasing")) {
                    break;
                }
                n = 1;
                break;
            }
            case -1913008125: {
                if (!s.equals("motionProgress")) {
                    break;
                }
                n = 0;
                break;
            }
        }
        switch (n) {
            case 16: {
                if (o instanceof Integer) {
                    this.mWaveShape = this.toInt(o);
                    break;
                }
                this.mWaveShape = 7;
                this.mCustomWaveShape = o.toString();
                break;
            }
            case 15: {
                this.mCurveFit = this.toInt(o);
                break;
            }
            case 14: {
                this.mWavePeriod = this.toFloat(o);
                break;
            }
            case 13: {
                this.mWaveOffset = this.toFloat(o);
                break;
            }
            case 12: {
                this.mAlpha = this.toFloat(o);
                break;
            }
            case 11: {
                this.mTransitionPathRotate = this.toFloat(o);
                break;
            }
            case 10: {
                this.mElevation = this.toFloat(o);
                break;
            }
            case 9: {
                this.mRotation = this.toFloat(o);
                break;
            }
            case 8: {
                this.mScaleY = this.toFloat(o);
                break;
            }
            case 7: {
                this.mScaleX = this.toFloat(o);
                break;
            }
            case 6: {
                this.mTranslationZ = this.toFloat(o);
                break;
            }
            case 5: {
                this.mTranslationY = this.toFloat(o);
                break;
            }
            case 4: {
                this.mTranslationX = this.toFloat(o);
                break;
            }
            case 3: {
                this.mRotationY = this.toFloat(o);
                break;
            }
            case 2: {
                this.mRotationX = this.toFloat(o);
                break;
            }
            case 1: {
                this.mTransitionEasing = o.toString();
                break;
            }
            case 0: {
                this.mProgress = this.toFloat(o);
                break;
            }
        }
    }
    
    private static class Loader
    {
        private static final int ANDROID_ALPHA = 1;
        private static final int ANDROID_ELEVATION = 2;
        private static final int ANDROID_ROTATION = 4;
        private static final int ANDROID_ROTATION_X = 5;
        private static final int ANDROID_ROTATION_Y = 6;
        private static final int ANDROID_SCALE_X = 7;
        private static final int ANDROID_SCALE_Y = 14;
        private static final int ANDROID_TRANSLATION_X = 15;
        private static final int ANDROID_TRANSLATION_Y = 16;
        private static final int ANDROID_TRANSLATION_Z = 17;
        private static final int CURVE_FIT = 13;
        private static final int FRAME_POSITION = 12;
        private static final int PROGRESS = 18;
        private static final int TARGET_ID = 10;
        private static final int TRANSITION_EASING = 9;
        private static final int TRANSITION_PATH_ROTATE = 8;
        private static final int WAVE_OFFSET = 21;
        private static final int WAVE_PERIOD = 20;
        private static final int WAVE_SHAPE = 19;
        private static SparseIntArray mAttrMap;
        
        static {
            (Loader.mAttrMap = new SparseIntArray()).append(R.styleable.KeyTimeCycle_android_alpha, 1);
            Loader.mAttrMap.append(R.styleable.KeyTimeCycle_android_elevation, 2);
            Loader.mAttrMap.append(R.styleable.KeyTimeCycle_android_rotation, 4);
            Loader.mAttrMap.append(R.styleable.KeyTimeCycle_android_rotationX, 5);
            Loader.mAttrMap.append(R.styleable.KeyTimeCycle_android_rotationY, 6);
            Loader.mAttrMap.append(R.styleable.KeyTimeCycle_android_scaleX, 7);
            Loader.mAttrMap.append(R.styleable.KeyTimeCycle_transitionPathRotate, 8);
            Loader.mAttrMap.append(R.styleable.KeyTimeCycle_transitionEasing, 9);
            Loader.mAttrMap.append(R.styleable.KeyTimeCycle_motionTarget, 10);
            Loader.mAttrMap.append(R.styleable.KeyTimeCycle_framePosition, 12);
            Loader.mAttrMap.append(R.styleable.KeyTimeCycle_curveFit, 13);
            Loader.mAttrMap.append(R.styleable.KeyTimeCycle_android_scaleY, 14);
            Loader.mAttrMap.append(R.styleable.KeyTimeCycle_android_translationX, 15);
            Loader.mAttrMap.append(R.styleable.KeyTimeCycle_android_translationY, 16);
            Loader.mAttrMap.append(R.styleable.KeyTimeCycle_android_translationZ, 17);
            Loader.mAttrMap.append(R.styleable.KeyTimeCycle_motionProgress, 18);
            Loader.mAttrMap.append(R.styleable.KeyTimeCycle_wavePeriod, 20);
            Loader.mAttrMap.append(R.styleable.KeyTimeCycle_waveOffset, 21);
            Loader.mAttrMap.append(R.styleable.KeyTimeCycle_waveShape, 19);
        }
        
        public static void read(final KeyTimeCycle keyTimeCycle, final TypedArray typedArray) {
            for (int indexCount = typedArray.getIndexCount(), i = 0; i < indexCount; ++i) {
                final int index = typedArray.getIndex(i);
                switch (Loader.mAttrMap.get(index)) {
                    default: {
                        final StringBuilder sb = new StringBuilder();
                        sb.append("unused attribute 0x");
                        sb.append(Integer.toHexString(index));
                        sb.append("   ");
                        sb.append(Loader.mAttrMap.get(index));
                        Log.e("KeyTimeCycle", sb.toString());
                        break;
                    }
                    case 21: {
                        if (typedArray.peekValue(index).type == 5) {
                            keyTimeCycle.mWaveOffset = typedArray.getDimension(index, keyTimeCycle.mWaveOffset);
                            break;
                        }
                        keyTimeCycle.mWaveOffset = typedArray.getFloat(index, keyTimeCycle.mWaveOffset);
                        break;
                    }
                    case 20: {
                        keyTimeCycle.mWavePeriod = typedArray.getFloat(index, keyTimeCycle.mWavePeriod);
                        break;
                    }
                    case 19: {
                        if (typedArray.peekValue(index).type == 3) {
                            keyTimeCycle.mCustomWaveShape = typedArray.getString(index);
                            keyTimeCycle.mWaveShape = 7;
                            break;
                        }
                        keyTimeCycle.mWaveShape = typedArray.getInt(index, keyTimeCycle.mWaveShape);
                        break;
                    }
                    case 18: {
                        keyTimeCycle.mProgress = typedArray.getFloat(index, keyTimeCycle.mProgress);
                        break;
                    }
                    case 17: {
                        if (Build$VERSION.SDK_INT >= 21) {
                            keyTimeCycle.mTranslationZ = typedArray.getDimension(index, keyTimeCycle.mTranslationZ);
                            break;
                        }
                        break;
                    }
                    case 16: {
                        keyTimeCycle.mTranslationY = typedArray.getDimension(index, keyTimeCycle.mTranslationY);
                        break;
                    }
                    case 15: {
                        keyTimeCycle.mTranslationX = typedArray.getDimension(index, keyTimeCycle.mTranslationX);
                        break;
                    }
                    case 14: {
                        keyTimeCycle.mScaleY = typedArray.getFloat(index, keyTimeCycle.mScaleY);
                        break;
                    }
                    case 13: {
                        keyTimeCycle.mCurveFit = typedArray.getInteger(index, keyTimeCycle.mCurveFit);
                        break;
                    }
                    case 12: {
                        keyTimeCycle.mFramePosition = typedArray.getInt(index, keyTimeCycle.mFramePosition);
                        break;
                    }
                    case 10: {
                        if (MotionLayout.IS_IN_EDIT_MODE) {
                            keyTimeCycle.mTargetId = typedArray.getResourceId(index, keyTimeCycle.mTargetId);
                            if (keyTimeCycle.mTargetId == -1) {
                                keyTimeCycle.mTargetString = typedArray.getString(index);
                                break;
                            }
                            break;
                        }
                        else {
                            if (typedArray.peekValue(index).type == 3) {
                                keyTimeCycle.mTargetString = typedArray.getString(index);
                                break;
                            }
                            keyTimeCycle.mTargetId = typedArray.getResourceId(index, keyTimeCycle.mTargetId);
                            break;
                        }
                        break;
                    }
                    case 9: {
                        keyTimeCycle.mTransitionEasing = typedArray.getString(index);
                        break;
                    }
                    case 8: {
                        keyTimeCycle.mTransitionPathRotate = typedArray.getFloat(index, keyTimeCycle.mTransitionPathRotate);
                        break;
                    }
                    case 7: {
                        keyTimeCycle.mScaleX = typedArray.getFloat(index, keyTimeCycle.mScaleX);
                        break;
                    }
                    case 6: {
                        keyTimeCycle.mRotationY = typedArray.getFloat(index, keyTimeCycle.mRotationY);
                        break;
                    }
                    case 5: {
                        keyTimeCycle.mRotationX = typedArray.getFloat(index, keyTimeCycle.mRotationX);
                        break;
                    }
                    case 4: {
                        keyTimeCycle.mRotation = typedArray.getFloat(index, keyTimeCycle.mRotation);
                        break;
                    }
                    case 2: {
                        keyTimeCycle.mElevation = typedArray.getDimension(index, keyTimeCycle.mElevation);
                        break;
                    }
                    case 1: {
                        keyTimeCycle.mAlpha = typedArray.getFloat(index, keyTimeCycle.mAlpha);
                        break;
                    }
                }
            }
        }
    }
}

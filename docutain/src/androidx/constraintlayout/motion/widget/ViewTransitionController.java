// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.motion.widget;

import android.graphics.Rect;
import android.view.MotionEvent;
import java.util.Iterator;
import java.util.Collection;
import android.util.Log;
import androidx.constraintlayout.widget.ConstraintSet;
import androidx.constraintlayout.widget.SharedValues;
import androidx.constraintlayout.widget.ConstraintLayout;
import android.view.View;
import java.util.HashSet;
import java.util.ArrayList;

public class ViewTransitionController
{
    private String TAG;
    ArrayList<ViewTransition.Animate> animations;
    private final MotionLayout mMotionLayout;
    private HashSet<View> mRelatedViews;
    ArrayList<ViewTransition.Animate> removeList;
    private ArrayList<ViewTransition> viewTransitions;
    
    public ViewTransitionController(final MotionLayout mMotionLayout) {
        this.viewTransitions = new ArrayList<ViewTransition>();
        this.TAG = "ViewTransitionController";
        this.removeList = new ArrayList<ViewTransition.Animate>();
        this.mMotionLayout = mMotionLayout;
    }
    
    private void listenForSharedVariable(final ViewTransition viewTransition, final boolean b) {
        ConstraintLayout.getSharedValues().addListener(viewTransition.getSharedValueID(), (SharedValues.SharedValuesListener)new SharedValues.SharedValuesListener(this, viewTransition, viewTransition.getSharedValueID(), b, viewTransition.getSharedValue()) {
            final ViewTransitionController this$0;
            final boolean val$isSet;
            final int val$listen_for_id;
            final int val$listen_for_value;
            final ViewTransition val$viewTransition;
            
            @Override
            public void onNewValue(int i, int sharedValueCurrent, int n) {
                n = this.val$viewTransition.getSharedValueCurrent();
                this.val$viewTransition.setSharedValueCurrent(sharedValueCurrent);
                if (this.val$listen_for_id == i && n != sharedValueCurrent) {
                    if (this.val$isSet) {
                        if (this.val$listen_for_value == sharedValueCurrent) {
                            View child;
                            ConstraintSet constraintSet;
                            ViewTransition val$viewTransition;
                            ViewTransitionController this$0;
                            for (sharedValueCurrent = this.this$0.mMotionLayout.getChildCount(), i = 0; i < sharedValueCurrent; ++i) {
                                child = this.this$0.mMotionLayout.getChildAt(i);
                                if (this.val$viewTransition.matchesView(child)) {
                                    n = this.this$0.mMotionLayout.getCurrentState();
                                    constraintSet = this.this$0.mMotionLayout.getConstraintSet(n);
                                    val$viewTransition = this.val$viewTransition;
                                    this$0 = this.this$0;
                                    val$viewTransition.applyTransition(this$0, this$0.mMotionLayout, n, constraintSet, child);
                                }
                            }
                        }
                    }
                    else if (this.val$listen_for_value != sharedValueCurrent) {
                        View child2;
                        ConstraintSet constraintSet2;
                        ViewTransition val$viewTransition2;
                        ViewTransitionController this$2;
                        for (sharedValueCurrent = this.this$0.mMotionLayout.getChildCount(), i = 0; i < sharedValueCurrent; ++i) {
                            child2 = this.this$0.mMotionLayout.getChildAt(i);
                            if (this.val$viewTransition.matchesView(child2)) {
                                n = this.this$0.mMotionLayout.getCurrentState();
                                constraintSet2 = this.this$0.mMotionLayout.getConstraintSet(n);
                                val$viewTransition2 = this.val$viewTransition;
                                this$2 = this.this$0;
                                val$viewTransition2.applyTransition(this$2, this$2.mMotionLayout, n, constraintSet2, child2);
                            }
                        }
                    }
                }
            }
        });
    }
    
    private void viewTransition(final ViewTransition viewTransition, final View... array) {
        final int currentState = this.mMotionLayout.getCurrentState();
        if (viewTransition.mViewTransitionMode != 2) {
            if (currentState == -1) {
                final String tag = this.TAG;
                final StringBuilder sb = new StringBuilder();
                sb.append("No support for ViewTransition within transition yet. Currently: ");
                sb.append(this.mMotionLayout.toString());
                Log.w(tag, sb.toString());
                return;
            }
            final ConstraintSet constraintSet = this.mMotionLayout.getConstraintSet(currentState);
            if (constraintSet == null) {
                return;
            }
            viewTransition.applyTransition(this, this.mMotionLayout, currentState, constraintSet, array);
        }
        else {
            viewTransition.applyTransition(this, this.mMotionLayout, currentState, null, array);
        }
    }
    
    public void add(final ViewTransition e) {
        this.viewTransitions.add(e);
        this.mRelatedViews = null;
        if (e.getStateTransition() == 4) {
            this.listenForSharedVariable(e, true);
        }
        else if (e.getStateTransition() == 5) {
            this.listenForSharedVariable(e, false);
        }
    }
    
    void addAnimation(final ViewTransition.Animate e) {
        if (this.animations == null) {
            this.animations = new ArrayList<ViewTransition.Animate>();
        }
        this.animations.add(e);
    }
    
    void animate() {
        final ArrayList<ViewTransition.Animate> animations = this.animations;
        if (animations == null) {
            return;
        }
        final Iterator<ViewTransition.Animate> iterator = animations.iterator();
        while (iterator.hasNext()) {
            iterator.next().mutate();
        }
        this.animations.removeAll(this.removeList);
        this.removeList.clear();
        if (this.animations.isEmpty()) {
            this.animations = null;
        }
    }
    
    boolean applyViewTransition(final int n, final MotionController motionController) {
        for (final ViewTransition viewTransition : this.viewTransitions) {
            if (viewTransition.getId() == n) {
                viewTransition.mKeyFrames.addAllFrames(motionController);
                return true;
            }
        }
        return false;
    }
    
    void enableViewTransition(final int n, final boolean enabled) {
        for (final ViewTransition viewTransition : this.viewTransitions) {
            if (viewTransition.getId() == n) {
                viewTransition.setEnabled(enabled);
                break;
            }
        }
    }
    
    void invalidate() {
        this.mMotionLayout.invalidate();
    }
    
    boolean isViewTransitionEnabled(final int n) {
        for (final ViewTransition viewTransition : this.viewTransitions) {
            if (viewTransition.getId() == n) {
                return viewTransition.isEnabled();
            }
        }
        return false;
    }
    
    void remove(final int n) {
        while (true) {
            for (final ViewTransition o : this.viewTransitions) {
                if (o.getId() == n) {
                    if (o != null) {
                        this.mRelatedViews = null;
                        this.viewTransitions.remove(o);
                    }
                    return;
                }
            }
            ViewTransition o = null;
            continue;
        }
    }
    
    void removeAnimation(final ViewTransition.Animate e) {
        this.removeList.add(e);
    }
    
    void touchEvent(final MotionEvent motionEvent) {
        final int currentState = this.mMotionLayout.getCurrentState();
        if (currentState == -1) {
            return;
        }
        if (this.mRelatedViews == null) {
            this.mRelatedViews = new HashSet<View>();
            for (final ViewTransition viewTransition : this.viewTransitions) {
                for (int childCount = this.mMotionLayout.getChildCount(), i = 0; i < childCount; ++i) {
                    final View child = this.mMotionLayout.getChildAt(i);
                    if (viewTransition.matchesView(child)) {
                        child.getId();
                        this.mRelatedViews.add(child);
                    }
                }
            }
        }
        final float x = motionEvent.getX();
        final float y = motionEvent.getY();
        final Rect rect = new Rect();
        final int action = motionEvent.getAction();
        final ArrayList<ViewTransition.Animate> animations = this.animations;
        if (animations != null && !animations.isEmpty()) {
            final Iterator<ViewTransition.Animate> iterator2 = this.animations.iterator();
            while (iterator2.hasNext()) {
                iterator2.next().reactTo(action, x, y);
            }
        }
        if (action == 0 || action == 1) {
            final ConstraintSet constraintSet = this.mMotionLayout.getConstraintSet(currentState);
            for (final ViewTransition viewTransition2 : this.viewTransitions) {
                if (viewTransition2.supports(action)) {
                    for (final View view : this.mRelatedViews) {
                        if (!viewTransition2.matchesView(view)) {
                            continue;
                        }
                        view.getHitRect(rect);
                        if (!rect.contains((int)x, (int)y)) {
                            continue;
                        }
                        viewTransition2.applyTransition(this, this.mMotionLayout, currentState, constraintSet, view);
                    }
                }
            }
        }
    }
    
    void viewTransition(final int n, final View... array) {
        final ArrayList list = new ArrayList();
        final Iterator<ViewTransition> iterator = this.viewTransitions.iterator();
        ViewTransition viewTransition = null;
        while (iterator.hasNext()) {
            final ViewTransition viewTransition2 = iterator.next();
            if (viewTransition2.getId() == n) {
                for (final View e : array) {
                    if (viewTransition2.checkTags(e)) {
                        list.add(e);
                    }
                }
                if (!list.isEmpty()) {
                    this.viewTransition(viewTransition2, (View[])list.toArray(new View[0]));
                    list.clear();
                }
                viewTransition = viewTransition2;
            }
        }
        if (viewTransition == null) {
            Log.e(this.TAG, " Could not find ViewTransition");
        }
    }
}

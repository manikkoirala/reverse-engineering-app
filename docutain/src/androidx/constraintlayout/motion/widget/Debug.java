// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.motion.widget;

import java.io.PrintStream;
import java.nio.CharBuffer;
import android.view.View;
import android.util.Log;
import android.content.res.Resources$NotFoundException;
import android.content.Context;
import android.view.MotionEvent;
import android.view.ViewGroup;
import android.view.ViewGroup$LayoutParams;

public class Debug
{
    public static void dumpLayoutParams(final ViewGroup$LayoutParams p0, final String p1) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     3: dup            
        //     4: invokespecial   java/lang/Throwable.<init>:()V
        //     7: invokevirtual   java/lang/Throwable.getStackTrace:()[Ljava/lang/StackTraceElement;
        //    10: iconst_1       
        //    11: aaload         
        //    12: astore_3       
        //    13: new             Ljava/lang/StringBuilder;
        //    16: dup            
        //    17: invokespecial   java/lang/StringBuilder.<init>:()V
        //    20: astore          4
        //    22: aload           4
        //    24: ldc             ".("
        //    26: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    29: pop            
        //    30: aload           4
        //    32: aload_3        
        //    33: invokevirtual   java/lang/StackTraceElement.getFileName:()Ljava/lang/String;
        //    36: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    39: pop            
        //    40: aload           4
        //    42: ldc             ":"
        //    44: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    47: pop            
        //    48: aload           4
        //    50: aload_3        
        //    51: invokevirtual   java/lang/StackTraceElement.getLineNumber:()I
        //    54: invokevirtual   java/lang/StringBuilder.append:(I)Ljava/lang/StringBuilder;
        //    57: pop            
        //    58: aload           4
        //    60: ldc             ") "
        //    62: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    65: pop            
        //    66: aload           4
        //    68: aload_1        
        //    69: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    72: pop            
        //    73: aload           4
        //    75: ldc             "  "
        //    77: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    80: pop            
        //    81: aload           4
        //    83: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //    86: astore_1       
        //    87: getstatic       java/lang/System.out:Ljava/io/PrintStream;
        //    90: astore_3       
        //    91: new             Ljava/lang/StringBuilder;
        //    94: dup            
        //    95: invokespecial   java/lang/StringBuilder.<init>:()V
        //    98: astore          4
        //   100: aload           4
        //   102: ldc             " >>>>>>>>>>>>>>>>>>. dump "
        //   104: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   107: pop            
        //   108: aload           4
        //   110: aload_1        
        //   111: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   114: pop            
        //   115: aload           4
        //   117: ldc             "  "
        //   119: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   122: pop            
        //   123: aload           4
        //   125: aload_0        
        //   126: invokevirtual   java/lang/Object.getClass:()Ljava/lang/Class;
        //   129: invokevirtual   java/lang/Class.getName:()Ljava/lang/String;
        //   132: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   135: pop            
        //   136: aload_3        
        //   137: aload           4
        //   139: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   142: invokevirtual   java/io/PrintStream.println:(Ljava/lang/String;)V
        //   145: aload_0        
        //   146: invokevirtual   java/lang/Object.getClass:()Ljava/lang/Class;
        //   149: invokevirtual   java/lang/Class.getFields:()[Ljava/lang/reflect/Field;
        //   152: astore_3       
        //   153: iconst_0       
        //   154: istore_2       
        //   155: iload_2        
        //   156: aload_3        
        //   157: arraylength    
        //   158: if_icmpge       280
        //   161: aload_3        
        //   162: iload_2        
        //   163: aaload         
        //   164: astore          5
        //   166: aload           5
        //   168: aload_0        
        //   169: invokevirtual   java/lang/reflect/Field.get:(Ljava/lang/Object;)Ljava/lang/Object;
        //   172: astore          4
        //   174: aload           5
        //   176: invokevirtual   java/lang/reflect/Field.getName:()Ljava/lang/String;
        //   179: astore          7
        //   181: aload           7
        //   183: ldc             "To"
        //   185: invokevirtual   java/lang/String.contains:(Ljava/lang/CharSequence;)Z
        //   188: ifne            194
        //   191: goto            274
        //   194: aload           4
        //   196: invokevirtual   java/lang/Object.toString:()Ljava/lang/String;
        //   199: ldc             "-1"
        //   201: invokevirtual   java/lang/String.equals:(Ljava/lang/Object;)Z
        //   204: ifeq            210
        //   207: goto            274
        //   210: getstatic       java/lang/System.out:Ljava/io/PrintStream;
        //   213: astore          6
        //   215: new             Ljava/lang/StringBuilder;
        //   218: astore          5
        //   220: aload           5
        //   222: invokespecial   java/lang/StringBuilder.<init>:()V
        //   225: aload           5
        //   227: aload_1        
        //   228: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   231: pop            
        //   232: aload           5
        //   234: ldc             "       "
        //   236: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   239: pop            
        //   240: aload           5
        //   242: aload           7
        //   244: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   247: pop            
        //   248: aload           5
        //   250: ldc             " "
        //   252: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   255: pop            
        //   256: aload           5
        //   258: aload           4
        //   260: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //   263: pop            
        //   264: aload           6
        //   266: aload           5
        //   268: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   271: invokevirtual   java/io/PrintStream.println:(Ljava/lang/String;)V
        //   274: iinc            2, 1
        //   277: goto            155
        //   280: getstatic       java/lang/System.out:Ljava/io/PrintStream;
        //   283: astore_3       
        //   284: new             Ljava/lang/StringBuilder;
        //   287: dup            
        //   288: invokespecial   java/lang/StringBuilder.<init>:()V
        //   291: astore_0       
        //   292: aload_0        
        //   293: ldc             " <<<<<<<<<<<<<<<<< dump "
        //   295: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   298: pop            
        //   299: aload_0        
        //   300: aload_1        
        //   301: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   304: pop            
        //   305: aload_3        
        //   306: aload_0        
        //   307: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   310: invokevirtual   java/io/PrintStream.println:(Ljava/lang/String;)V
        //   313: return         
        //   314: astore          4
        //   316: goto            274
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                              
        //  -----  -----  -----  -----  ----------------------------------
        //  166    191    314    319    Ljava/lang/IllegalAccessException;
        //  194    207    314    319    Ljava/lang/IllegalAccessException;
        //  210    274    314    319    Ljava/lang/IllegalAccessException;
        // 
        // The error that occurred was:
        // 
        // java.lang.NullPointerException: Cannot invoke "com.strobel.assembler.metadata.TypeReference.getSimpleType()" because "type" is null
        //     at com.strobel.assembler.ir.StackMappingVisitor.push(StackMappingVisitor.java:290)
        //     at com.strobel.assembler.ir.StackMappingVisitor$InstructionAnalyzer.execute(StackMappingVisitor.java:837)
        //     at com.strobel.assembler.ir.StackMappingVisitor$InstructionAnalyzer.visit(StackMappingVisitor.java:398)
        //     at com.strobel.decompiler.ast.AstBuilder.performStackAnalysis(AstBuilder.java:2086)
        //     at com.strobel.decompiler.ast.AstBuilder.build(AstBuilder.java:108)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:203)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public static void dumpLayoutParams(final ViewGroup p0, final String p1) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     3: dup            
        //     4: invokespecial   java/lang/Throwable.<init>:()V
        //     7: invokevirtual   java/lang/Throwable.getStackTrace:()[Ljava/lang/StackTraceElement;
        //    10: iconst_1       
        //    11: aaload         
        //    12: astore          5
        //    14: new             Ljava/lang/StringBuilder;
        //    17: dup            
        //    18: invokespecial   java/lang/StringBuilder.<init>:()V
        //    21: astore          6
        //    23: aload           6
        //    25: ldc             ".("
        //    27: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    30: pop            
        //    31: aload           6
        //    33: aload           5
        //    35: invokevirtual   java/lang/StackTraceElement.getFileName:()Ljava/lang/String;
        //    38: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    41: pop            
        //    42: aload           6
        //    44: ldc             ":"
        //    46: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    49: pop            
        //    50: aload           6
        //    52: aload           5
        //    54: invokevirtual   java/lang/StackTraceElement.getLineNumber:()I
        //    57: invokevirtual   java/lang/StringBuilder.append:(I)Ljava/lang/StringBuilder;
        //    60: pop            
        //    61: aload           6
        //    63: ldc             ") "
        //    65: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    68: pop            
        //    69: aload           6
        //    71: aload_1        
        //    72: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    75: pop            
        //    76: aload           6
        //    78: ldc             "  "
        //    80: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    83: pop            
        //    84: aload           6
        //    86: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //    89: astore          5
        //    91: aload_0        
        //    92: invokevirtual   android/view/ViewGroup.getChildCount:()I
        //    95: istore          4
        //    97: getstatic       java/lang/System.out:Ljava/io/PrintStream;
        //   100: astore          6
        //   102: new             Ljava/lang/StringBuilder;
        //   105: dup            
        //   106: invokespecial   java/lang/StringBuilder.<init>:()V
        //   109: astore          7
        //   111: aload           7
        //   113: aload_1        
        //   114: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   117: pop            
        //   118: aload           7
        //   120: ldc             " children "
        //   122: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   125: pop            
        //   126: aload           7
        //   128: iload           4
        //   130: invokevirtual   java/lang/StringBuilder.append:(I)Ljava/lang/StringBuilder;
        //   133: pop            
        //   134: aload           6
        //   136: aload           7
        //   138: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   141: invokevirtual   java/io/PrintStream.println:(Ljava/lang/String;)V
        //   144: iconst_0       
        //   145: istore_2       
        //   146: iload_2        
        //   147: iload           4
        //   149: if_icmpge       357
        //   152: aload_0        
        //   153: iload_2        
        //   154: invokevirtual   android/view/ViewGroup.getChildAt:(I)Landroid/view/View;
        //   157: astore_1       
        //   158: getstatic       java/lang/System.out:Ljava/io/PrintStream;
        //   161: astore          6
        //   163: new             Ljava/lang/StringBuilder;
        //   166: dup            
        //   167: invokespecial   java/lang/StringBuilder.<init>:()V
        //   170: astore          7
        //   172: aload           7
        //   174: aload           5
        //   176: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   179: pop            
        //   180: aload           7
        //   182: ldc             "     "
        //   184: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   187: pop            
        //   188: aload           7
        //   190: aload_1        
        //   191: invokestatic    androidx/constraintlayout/motion/widget/Debug.getName:(Landroid/view/View;)Ljava/lang/String;
        //   194: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   197: pop            
        //   198: aload           6
        //   200: aload           7
        //   202: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   205: invokevirtual   java/io/PrintStream.println:(Ljava/lang/String;)V
        //   208: aload_1        
        //   209: invokevirtual   android/view/View.getLayoutParams:()Landroid/view/ViewGroup$LayoutParams;
        //   212: astore          6
        //   214: aload           6
        //   216: invokevirtual   java/lang/Object.getClass:()Ljava/lang/Class;
        //   219: invokevirtual   java/lang/Class.getFields:()[Ljava/lang/reflect/Field;
        //   222: astore_1       
        //   223: iconst_0       
        //   224: istore_3       
        //   225: iload_3        
        //   226: aload_1        
        //   227: arraylength    
        //   228: if_icmpge       351
        //   231: aload_1        
        //   232: iload_3        
        //   233: aaload         
        //   234: astore          9
        //   236: aload           9
        //   238: aload           6
        //   240: invokevirtual   java/lang/reflect/Field.get:(Ljava/lang/Object;)Ljava/lang/Object;
        //   243: astore          7
        //   245: aload           9
        //   247: invokevirtual   java/lang/reflect/Field.getName:()Ljava/lang/String;
        //   250: ldc             "To"
        //   252: invokevirtual   java/lang/String.contains:(Ljava/lang/CharSequence;)Z
        //   255: ifne            261
        //   258: goto            345
        //   261: aload           7
        //   263: invokevirtual   java/lang/Object.toString:()Ljava/lang/String;
        //   266: ldc             "-1"
        //   268: invokevirtual   java/lang/String.equals:(Ljava/lang/Object;)Z
        //   271: ifeq            277
        //   274: goto            345
        //   277: getstatic       java/lang/System.out:Ljava/io/PrintStream;
        //   280: astore          8
        //   282: new             Ljava/lang/StringBuilder;
        //   285: astore          10
        //   287: aload           10
        //   289: invokespecial   java/lang/StringBuilder.<init>:()V
        //   292: aload           10
        //   294: aload           5
        //   296: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   299: pop            
        //   300: aload           10
        //   302: ldc             "       "
        //   304: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   307: pop            
        //   308: aload           10
        //   310: aload           9
        //   312: invokevirtual   java/lang/reflect/Field.getName:()Ljava/lang/String;
        //   315: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   318: pop            
        //   319: aload           10
        //   321: ldc             " "
        //   323: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   326: pop            
        //   327: aload           10
        //   329: aload           7
        //   331: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //   334: pop            
        //   335: aload           8
        //   337: aload           10
        //   339: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   342: invokevirtual   java/io/PrintStream.println:(Ljava/lang/String;)V
        //   345: iinc            3, 1
        //   348: goto            225
        //   351: iinc            2, 1
        //   354: goto            146
        //   357: return         
        //   358: astore          7
        //   360: goto            345
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                              
        //  -----  -----  -----  -----  ----------------------------------
        //  236    258    358    363    Ljava/lang/IllegalAccessException;
        //  261    274    358    363    Ljava/lang/IllegalAccessException;
        //  277    345    358    363    Ljava/lang/IllegalAccessException;
        // 
        // The error that occurred was:
        // 
        // java.lang.NullPointerException: Cannot invoke "com.strobel.assembler.metadata.TypeReference.getSimpleType()" because "type" is null
        //     at com.strobel.assembler.ir.StackMappingVisitor.push(StackMappingVisitor.java:290)
        //     at com.strobel.assembler.ir.StackMappingVisitor$InstructionAnalyzer.execute(StackMappingVisitor.java:837)
        //     at com.strobel.assembler.ir.StackMappingVisitor$InstructionAnalyzer.visit(StackMappingVisitor.java:398)
        //     at com.strobel.decompiler.ast.AstBuilder.performStackAnalysis(AstBuilder.java:2086)
        //     at com.strobel.decompiler.ast.AstBuilder.build(AstBuilder.java:108)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:203)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public static void dumpPoc(final Object p0) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     3: dup            
        //     4: invokespecial   java/lang/Throwable.<init>:()V
        //     7: invokevirtual   java/lang/Throwable.getStackTrace:()[Ljava/lang/StackTraceElement;
        //    10: iconst_1       
        //    11: aaload         
        //    12: astore_3       
        //    13: new             Ljava/lang/StringBuilder;
        //    16: dup            
        //    17: invokespecial   java/lang/StringBuilder.<init>:()V
        //    20: astore_2       
        //    21: aload_2        
        //    22: ldc             ".("
        //    24: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    27: pop            
        //    28: aload_2        
        //    29: aload_3        
        //    30: invokevirtual   java/lang/StackTraceElement.getFileName:()Ljava/lang/String;
        //    33: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    36: pop            
        //    37: aload_2        
        //    38: ldc             ":"
        //    40: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    43: pop            
        //    44: aload_2        
        //    45: aload_3        
        //    46: invokevirtual   java/lang/StackTraceElement.getLineNumber:()I
        //    49: invokevirtual   java/lang/StringBuilder.append:(I)Ljava/lang/StringBuilder;
        //    52: pop            
        //    53: aload_2        
        //    54: ldc             ")"
        //    56: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    59: pop            
        //    60: aload_2        
        //    61: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //    64: astore_2       
        //    65: aload_0        
        //    66: invokevirtual   java/lang/Object.getClass:()Ljava/lang/Class;
        //    69: astore_3       
        //    70: getstatic       java/lang/System.out:Ljava/io/PrintStream;
        //    73: astore          5
        //    75: new             Ljava/lang/StringBuilder;
        //    78: dup            
        //    79: invokespecial   java/lang/StringBuilder.<init>:()V
        //    82: astore          4
        //    84: aload           4
        //    86: aload_2        
        //    87: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    90: pop            
        //    91: aload           4
        //    93: ldc             "------------- "
        //    95: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    98: pop            
        //    99: aload           4
        //   101: aload_3        
        //   102: invokevirtual   java/lang/Class.getName:()Ljava/lang/String;
        //   105: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   108: pop            
        //   109: aload           4
        //   111: ldc             " --------------------"
        //   113: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   116: pop            
        //   117: aload           5
        //   119: aload           4
        //   121: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   124: invokevirtual   java/io/PrintStream.println:(Ljava/lang/String;)V
        //   127: aload_3        
        //   128: invokevirtual   java/lang/Class.getFields:()[Ljava/lang/reflect/Field;
        //   131: astore          4
        //   133: iconst_0       
        //   134: istore_1       
        //   135: iload_1        
        //   136: aload           4
        //   138: arraylength    
        //   139: if_icmpge       341
        //   142: aload           4
        //   144: iload_1        
        //   145: aaload         
        //   146: astore          6
        //   148: aload           6
        //   150: aload_0        
        //   151: invokevirtual   java/lang/reflect/Field.get:(Ljava/lang/Object;)Ljava/lang/Object;
        //   154: astore          5
        //   156: aload           6
        //   158: invokevirtual   java/lang/reflect/Field.getName:()Ljava/lang/String;
        //   161: ldc             "layout_constraint"
        //   163: invokevirtual   java/lang/String.startsWith:(Ljava/lang/String;)Z
        //   166: ifne            172
        //   169: goto            335
        //   172: aload           5
        //   174: instanceof      Ljava/lang/Integer;
        //   177: ifeq            196
        //   180: aload           5
        //   182: invokevirtual   java/lang/Object.toString:()Ljava/lang/String;
        //   185: ldc             "-1"
        //   187: invokevirtual   java/lang/String.equals:(Ljava/lang/Object;)Z
        //   190: ifeq            196
        //   193: goto            335
        //   196: aload           5
        //   198: instanceof      Ljava/lang/Integer;
        //   201: ifeq            220
        //   204: aload           5
        //   206: invokevirtual   java/lang/Object.toString:()Ljava/lang/String;
        //   209: ldc             "0"
        //   211: invokevirtual   java/lang/String.equals:(Ljava/lang/Object;)Z
        //   214: ifeq            220
        //   217: goto            335
        //   220: aload           5
        //   222: instanceof      Ljava/lang/Float;
        //   225: ifeq            244
        //   228: aload           5
        //   230: invokevirtual   java/lang/Object.toString:()Ljava/lang/String;
        //   233: ldc             "1.0"
        //   235: invokevirtual   java/lang/String.equals:(Ljava/lang/Object;)Z
        //   238: ifeq            244
        //   241: goto            335
        //   244: aload           5
        //   246: instanceof      Ljava/lang/Float;
        //   249: ifeq            268
        //   252: aload           5
        //   254: invokevirtual   java/lang/Object.toString:()Ljava/lang/String;
        //   257: ldc             "0.5"
        //   259: invokevirtual   java/lang/String.equals:(Ljava/lang/Object;)Z
        //   262: ifeq            268
        //   265: goto            335
        //   268: getstatic       java/lang/System.out:Ljava/io/PrintStream;
        //   271: astore          7
        //   273: new             Ljava/lang/StringBuilder;
        //   276: astore          8
        //   278: aload           8
        //   280: invokespecial   java/lang/StringBuilder.<init>:()V
        //   283: aload           8
        //   285: aload_2        
        //   286: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   289: pop            
        //   290: aload           8
        //   292: ldc             "    "
        //   294: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   297: pop            
        //   298: aload           8
        //   300: aload           6
        //   302: invokevirtual   java/lang/reflect/Field.getName:()Ljava/lang/String;
        //   305: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   308: pop            
        //   309: aload           8
        //   311: ldc             " "
        //   313: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   316: pop            
        //   317: aload           8
        //   319: aload           5
        //   321: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //   324: pop            
        //   325: aload           7
        //   327: aload           8
        //   329: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   332: invokevirtual   java/io/PrintStream.println:(Ljava/lang/String;)V
        //   335: iinc            1, 1
        //   338: goto            135
        //   341: getstatic       java/lang/System.out:Ljava/io/PrintStream;
        //   344: astore          4
        //   346: new             Ljava/lang/StringBuilder;
        //   349: dup            
        //   350: invokespecial   java/lang/StringBuilder.<init>:()V
        //   353: astore_0       
        //   354: aload_0        
        //   355: aload_2        
        //   356: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   359: pop            
        //   360: aload_0        
        //   361: ldc             "------------- "
        //   363: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   366: pop            
        //   367: aload_0        
        //   368: aload_3        
        //   369: invokevirtual   java/lang/Class.getSimpleName:()Ljava/lang/String;
        //   372: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   375: pop            
        //   376: aload_0        
        //   377: ldc             " --------------------"
        //   379: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   382: pop            
        //   383: aload           4
        //   385: aload_0        
        //   386: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   389: invokevirtual   java/io/PrintStream.println:(Ljava/lang/String;)V
        //   392: return         
        //   393: astore          5
        //   395: goto            335
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                              
        //  -----  -----  -----  -----  ----------------------------------
        //  148    169    393    398    Ljava/lang/IllegalAccessException;
        //  172    193    393    398    Ljava/lang/IllegalAccessException;
        //  196    217    393    398    Ljava/lang/IllegalAccessException;
        //  220    241    393    398    Ljava/lang/IllegalAccessException;
        //  244    265    393    398    Ljava/lang/IllegalAccessException;
        //  268    335    393    398    Ljava/lang/IllegalAccessException;
        // 
        // The error that occurred was:
        // 
        // java.lang.NullPointerException: Cannot invoke "com.strobel.assembler.metadata.TypeReference.getSimpleType()" because "type" is null
        //     at com.strobel.assembler.ir.StackMappingVisitor.push(StackMappingVisitor.java:290)
        //     at com.strobel.assembler.ir.StackMappingVisitor$InstructionAnalyzer.execute(StackMappingVisitor.java:837)
        //     at com.strobel.assembler.ir.StackMappingVisitor$InstructionAnalyzer.visit(StackMappingVisitor.java:398)
        //     at com.strobel.decompiler.ast.AstBuilder.performStackAnalysis(AstBuilder.java:2086)
        //     at com.strobel.decompiler.ast.AstBuilder.build(AstBuilder.java:108)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:203)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public static String getActionType(final MotionEvent p0) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: invokevirtual   android/view/MotionEvent.getAction:()I
        //     4: istore_2       
        //     5: ldc             Landroid/view/MotionEvent;.class
        //     7: invokevirtual   java/lang/Class.getFields:()[Ljava/lang/reflect/Field;
        //    10: astore_0       
        //    11: iconst_0       
        //    12: istore_1       
        //    13: iload_1        
        //    14: aload_0        
        //    15: arraylength    
        //    16: if_icmpge       68
        //    19: aload_0        
        //    20: iload_1        
        //    21: aaload         
        //    22: astore_3       
        //    23: aload_3        
        //    24: invokevirtual   java/lang/reflect/Field.getModifiers:()I
        //    27: invokestatic    java/lang/reflect/Modifier.isStatic:(I)Z
        //    30: ifeq            62
        //    33: aload_3        
        //    34: invokevirtual   java/lang/reflect/Field.getType:()Ljava/lang/Class;
        //    37: getstatic       java/lang/Integer.TYPE:Ljava/lang/Class;
        //    40: invokevirtual   java/lang/Object.equals:(Ljava/lang/Object;)Z
        //    43: ifeq            62
        //    46: aload_3        
        //    47: aconst_null    
        //    48: invokevirtual   java/lang/reflect/Field.getInt:(Ljava/lang/Object;)I
        //    51: iload_2        
        //    52: if_icmpne       62
        //    55: aload_3        
        //    56: invokevirtual   java/lang/reflect/Field.getName:()Ljava/lang/String;
        //    59: astore_3       
        //    60: aload_3        
        //    61: areturn        
        //    62: iinc            1, 1
        //    65: goto            13
        //    68: ldc             "---"
        //    70: areturn        
        //    71: astore_3       
        //    72: goto            62
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                              
        //  -----  -----  -----  -----  ----------------------------------
        //  23     60     71     75     Ljava/lang/IllegalAccessException;
        // 
        // The error that occurred was:
        // 
        // java.lang.NullPointerException: Cannot invoke "com.strobel.assembler.metadata.TypeReference.getSimpleType()" because "type" is null
        //     at com.strobel.assembler.ir.StackMappingVisitor.push(StackMappingVisitor.java:290)
        //     at com.strobel.assembler.ir.StackMappingVisitor$InstructionAnalyzer.execute(StackMappingVisitor.java:837)
        //     at com.strobel.assembler.ir.StackMappingVisitor$InstructionAnalyzer.visit(StackMappingVisitor.java:398)
        //     at com.strobel.decompiler.ast.AstBuilder.performStackAnalysis(AstBuilder.java:2086)
        //     at com.strobel.decompiler.ast.AstBuilder.build(AstBuilder.java:108)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:203)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public static String getCallFrom(final int n) {
        final StackTraceElement stackTraceElement = new Throwable().getStackTrace()[n + 2];
        final StringBuilder sb = new StringBuilder();
        sb.append(".(");
        sb.append(stackTraceElement.getFileName());
        sb.append(":");
        sb.append(stackTraceElement.getLineNumber());
        sb.append(")");
        return sb.toString();
    }
    
    public static String getLoc() {
        final StackTraceElement stackTraceElement = new Throwable().getStackTrace()[1];
        final StringBuilder sb = new StringBuilder();
        sb.append(".(");
        sb.append(stackTraceElement.getFileName());
        sb.append(":");
        sb.append(stackTraceElement.getLineNumber());
        sb.append(") ");
        sb.append(stackTraceElement.getMethodName());
        sb.append("()");
        return sb.toString();
    }
    
    public static String getLocation() {
        final StackTraceElement stackTraceElement = new Throwable().getStackTrace()[1];
        final StringBuilder sb = new StringBuilder();
        sb.append(".(");
        sb.append(stackTraceElement.getFileName());
        sb.append(":");
        sb.append(stackTraceElement.getLineNumber());
        sb.append(")");
        return sb.toString();
    }
    
    public static String getLocation2() {
        final StackTraceElement stackTraceElement = new Throwable().getStackTrace()[2];
        final StringBuilder sb = new StringBuilder();
        sb.append(".(");
        sb.append(stackTraceElement.getFileName());
        sb.append(":");
        sb.append(stackTraceElement.getLineNumber());
        sb.append(")");
        return sb.toString();
    }
    
    public static String getName(final Context context, final int i) {
        if (i == -1) {
            return "UNKNOWN";
        }
        try {
            return context.getResources().getResourceEntryName(i);
        }
        catch (final Exception ex) {
            final StringBuilder sb = new StringBuilder();
            sb.append("?");
            sb.append(i);
            return sb.toString();
        }
    }
    
    public static String getName(final Context context, final int[] array) {
        try {
            final StringBuilder sb = new StringBuilder();
            sb.append(array.length);
            sb.append("[");
            String s = sb.toString();
            for (int i = 0; i < array.length; ++i) {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append(s);
                String str;
                if (i == 0) {
                    str = "";
                }
                else {
                    str = " ";
                }
                sb2.append(str);
                final String string = sb2.toString();
                String str2;
                try {
                    str2 = context.getResources().getResourceEntryName(array[i]);
                }
                catch (final Resources$NotFoundException ex) {
                    final StringBuilder sb3 = new StringBuilder();
                    sb3.append("? ");
                    sb3.append(array[i]);
                    sb3.append(" ");
                    str2 = sb3.toString();
                }
                final StringBuilder sb4 = new StringBuilder();
                sb4.append(string);
                sb4.append(str2);
                s = sb4.toString();
            }
            final StringBuilder sb5 = new StringBuilder();
            sb5.append(s);
            sb5.append("]");
            return sb5.toString();
        }
        catch (final Exception ex2) {
            Log.v("DEBUG", ex2.toString());
            return "UNKNOWN";
        }
    }
    
    public static String getName(final View view) {
        try {
            return view.getContext().getResources().getResourceEntryName(view.getId());
        }
        catch (final Exception ex) {
            return "UNKNOWN";
        }
    }
    
    public static String getState(final MotionLayout motionLayout, final int n) {
        return getState(motionLayout, n, -1);
    }
    
    public static String getState(final MotionLayout motionLayout, int length, final int n) {
        if (length == -1) {
            return "UNDEFINED";
        }
        String s2;
        final String s = s2 = motionLayout.getContext().getResources().getResourceEntryName(length);
        if (n != -1) {
            String replaceAll = s;
            if (s.length() > n) {
                replaceAll = s.replaceAll("([^_])[aeiou]+", "$1");
            }
            s2 = replaceAll;
            if (replaceAll.length() > n) {
                length = replaceAll.replaceAll("[^_]", "").length();
                s2 = replaceAll;
                if (length > 0) {
                    length = (replaceAll.length() - n) / length;
                    final StringBuilder sb = new StringBuilder();
                    sb.append(CharBuffer.allocate(length).toString().replace('\0', '.'));
                    sb.append("_");
                    s2 = replaceAll.replaceAll(sb.toString(), "_");
                }
            }
        }
        return s2;
    }
    
    public static void logStack(final String s, final String str, int i) {
        final StackTraceElement[] stackTrace = new Throwable().getStackTrace();
        final int length = stackTrace.length;
        final int n = 1;
        final int min = Math.min(i, length - 1);
        String string = " ";
        StackTraceElement stackTraceElement;
        StringBuilder sb;
        String string2;
        StringBuilder sb2;
        StringBuilder sb3;
        for (i = n; i <= min; ++i) {
            stackTraceElement = stackTrace[i];
            sb = new StringBuilder();
            sb.append(".(");
            sb.append(stackTrace[i].getFileName());
            sb.append(":");
            sb.append(stackTrace[i].getLineNumber());
            sb.append(") ");
            sb.append(stackTrace[i].getMethodName());
            string2 = sb.toString();
            sb2 = new StringBuilder();
            sb2.append(string);
            sb2.append(" ");
            string = sb2.toString();
            sb3 = new StringBuilder();
            sb3.append(str);
            sb3.append(string);
            sb3.append(string2);
            sb3.append(string);
            Log.v(s, sb3.toString());
        }
    }
    
    public static void printStack(final String str, int i) {
        final StackTraceElement[] stackTrace = new Throwable().getStackTrace();
        final int length = stackTrace.length;
        final int n = 1;
        final int min = Math.min(i, length - 1);
        String string = " ";
        StackTraceElement stackTraceElement;
        StringBuilder sb;
        String string2;
        StringBuilder sb2;
        PrintStream out;
        StringBuilder sb3;
        for (i = n; i <= min; ++i) {
            stackTraceElement = stackTrace[i];
            sb = new StringBuilder();
            sb.append(".(");
            sb.append(stackTrace[i].getFileName());
            sb.append(":");
            sb.append(stackTrace[i].getLineNumber());
            sb.append(") ");
            string2 = sb.toString();
            sb2 = new StringBuilder();
            sb2.append(string);
            sb2.append(" ");
            string = sb2.toString();
            out = System.out;
            sb3 = new StringBuilder();
            sb3.append(str);
            sb3.append(string);
            sb3.append(string2);
            sb3.append(string);
            out.println(sb3.toString());
        }
    }
}

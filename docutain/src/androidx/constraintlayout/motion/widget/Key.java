// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.motion.widget;

import android.util.AttributeSet;
import android.content.Context;
import java.util.HashSet;
import androidx.constraintlayout.motion.utils.ViewSpline;
import androidx.constraintlayout.widget.ConstraintAttribute;
import java.util.HashMap;

public abstract class Key
{
    public static final String ALPHA = "alpha";
    public static final String CURVEFIT = "curveFit";
    public static final String CUSTOM = "CUSTOM";
    public static final String ELEVATION = "elevation";
    public static final String MOTIONPROGRESS = "motionProgress";
    public static final String PIVOT_X = "transformPivotX";
    public static final String PIVOT_Y = "transformPivotY";
    public static final String PROGRESS = "progress";
    public static final String ROTATION = "rotation";
    public static final String ROTATION_X = "rotationX";
    public static final String ROTATION_Y = "rotationY";
    public static final String SCALE_X = "scaleX";
    public static final String SCALE_Y = "scaleY";
    public static final String TRANSITIONEASING = "transitionEasing";
    public static final String TRANSITION_PATH_ROTATE = "transitionPathRotate";
    public static final String TRANSLATION_X = "translationX";
    public static final String TRANSLATION_Y = "translationY";
    public static final String TRANSLATION_Z = "translationZ";
    public static int UNSET = -1;
    public static final String VISIBILITY = "visibility";
    public static final String WAVE_OFFSET = "waveOffset";
    public static final String WAVE_PERIOD = "wavePeriod";
    public static final String WAVE_PHASE = "wavePhase";
    public static final String WAVE_VARIES_BY = "waveVariesBy";
    HashMap<String, ConstraintAttribute> mCustomConstraints;
    int mFramePosition;
    int mTargetId;
    String mTargetString;
    protected int mType;
    
    public Key() {
        final int unset = Key.UNSET;
        this.mFramePosition = unset;
        this.mTargetId = unset;
        this.mTargetString = null;
    }
    
    public abstract void addValues(final HashMap<String, ViewSpline> p0);
    
    public abstract Key clone();
    
    public Key copy(final Key key) {
        this.mFramePosition = key.mFramePosition;
        this.mTargetId = key.mTargetId;
        this.mTargetString = key.mTargetString;
        this.mType = key.mType;
        this.mCustomConstraints = key.mCustomConstraints;
        return this;
    }
    
    abstract void getAttributeNames(final HashSet<String> p0);
    
    public int getFramePosition() {
        return this.mFramePosition;
    }
    
    abstract void load(final Context p0, final AttributeSet p1);
    
    boolean matches(final String s) {
        final String mTargetString = this.mTargetString;
        return mTargetString != null && s != null && s.matches(mTargetString);
    }
    
    public void setFramePosition(final int mFramePosition) {
        this.mFramePosition = mFramePosition;
    }
    
    public void setInterpolation(final HashMap<String, Integer> hashMap) {
    }
    
    public abstract void setValue(final String p0, final Object p1);
    
    public Key setViewId(final int mTargetId) {
        this.mTargetId = mTargetId;
        return this;
    }
    
    boolean toBoolean(final Object o) {
        boolean b;
        if (o instanceof Boolean) {
            b = (boolean)o;
        }
        else {
            b = Boolean.parseBoolean(o.toString());
        }
        return b;
    }
    
    float toFloat(final Object o) {
        float n;
        if (o instanceof Float) {
            n = (float)o;
        }
        else {
            n = Float.parseFloat(o.toString());
        }
        return n;
    }
    
    int toInt(final Object o) {
        int n;
        if (o instanceof Integer) {
            n = (int)o;
        }
        else {
            n = Integer.parseInt(o.toString());
        }
        return n;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.motion.utils;

import java.lang.reflect.Method;
import java.lang.reflect.InvocationTargetException;
import android.util.Log;
import androidx.constraintlayout.motion.widget.MotionLayout;
import android.os.Build$VERSION;
import androidx.constraintlayout.core.motion.utils.CurveFit;
import android.view.View;
import androidx.constraintlayout.widget.ConstraintAttribute;
import android.util.SparseArray;
import androidx.constraintlayout.core.motion.utils.SplineSet;

public abstract class ViewSpline extends SplineSet
{
    private static final String TAG = "ViewSpline";
    
    public static ViewSpline makeCustomSpline(final String s, final SparseArray<ConstraintAttribute> sparseArray) {
        return new CustomSet(s, sparseArray);
    }
    
    public static ViewSpline makeSpline(final String s) {
        s.hashCode();
        final int hashCode = s.hashCode();
        int n = -1;
        switch (hashCode) {
            case 156108012: {
                if (!s.equals("waveOffset")) {
                    break;
                }
                n = 15;
                break;
            }
            case 92909918: {
                if (!s.equals("alpha")) {
                    break;
                }
                n = 14;
                break;
            }
            case 37232917: {
                if (!s.equals("transitionPathRotate")) {
                    break;
                }
                n = 13;
                break;
            }
            case -4379043: {
                if (!s.equals("elevation")) {
                    break;
                }
                n = 12;
                break;
            }
            case -40300674: {
                if (!s.equals("rotation")) {
                    break;
                }
                n = 11;
                break;
            }
            case -760884509: {
                if (!s.equals("transformPivotY")) {
                    break;
                }
                n = 10;
                break;
            }
            case -760884510: {
                if (!s.equals("transformPivotX")) {
                    break;
                }
                n = 9;
                break;
            }
            case -797520672: {
                if (!s.equals("waveVariesBy")) {
                    break;
                }
                n = 8;
                break;
            }
            case -908189617: {
                if (!s.equals("scaleY")) {
                    break;
                }
                n = 7;
                break;
            }
            case -908189618: {
                if (!s.equals("scaleX")) {
                    break;
                }
                n = 6;
                break;
            }
            case -1001078227: {
                if (!s.equals("progress")) {
                    break;
                }
                n = 5;
                break;
            }
            case -1225497655: {
                if (!s.equals("translationZ")) {
                    break;
                }
                n = 4;
                break;
            }
            case -1225497656: {
                if (!s.equals("translationY")) {
                    break;
                }
                n = 3;
                break;
            }
            case -1225497657: {
                if (!s.equals("translationX")) {
                    break;
                }
                n = 2;
                break;
            }
            case -1249320805: {
                if (!s.equals("rotationY")) {
                    break;
                }
                n = 1;
                break;
            }
            case -1249320806: {
                if (!s.equals("rotationX")) {
                    break;
                }
                n = 0;
                break;
            }
        }
        switch (n) {
            default: {
                return null;
            }
            case 15: {
                return new AlphaSet();
            }
            case 14: {
                return new AlphaSet();
            }
            case 13: {
                return new PathRotate();
            }
            case 12: {
                return new ElevationSet();
            }
            case 11: {
                return new RotationSet();
            }
            case 10: {
                return new PivotYset();
            }
            case 9: {
                return new PivotXset();
            }
            case 8: {
                return new AlphaSet();
            }
            case 7: {
                return new ScaleYset();
            }
            case 6: {
                return new ScaleXset();
            }
            case 5: {
                return new ProgressSet();
            }
            case 4: {
                return new TranslationZset();
            }
            case 3: {
                return new TranslationYset();
            }
            case 2: {
                return new TranslationXset();
            }
            case 1: {
                return new RotationYset();
            }
            case 0: {
                return new RotationXset();
            }
        }
    }
    
    public abstract void setProperty(final View p0, final float p1);
    
    static class AlphaSet extends ViewSpline
    {
        @Override
        public void setProperty(final View view, final float n) {
            view.setAlpha(this.get(n));
        }
    }
    
    public static class CustomSet extends ViewSpline
    {
        String mAttributeName;
        SparseArray<ConstraintAttribute> mConstraintAttributeList;
        float[] mTempValues;
        
        public CustomSet(final String s, final SparseArray<ConstraintAttribute> mConstraintAttributeList) {
            this.mAttributeName = s.split(",")[1];
            this.mConstraintAttributeList = mConstraintAttributeList;
        }
        
        @Override
        public void setPoint(final int n, final float n2) {
            throw new RuntimeException("don't call for custom attribute call setPoint(pos, ConstraintAttribute)");
        }
        
        public void setPoint(final int n, final ConstraintAttribute constraintAttribute) {
            this.mConstraintAttributeList.append(n, (Object)constraintAttribute);
        }
        
        @Override
        public void setProperty(final View view, final float n) {
            this.mCurveFit.getPos(n, this.mTempValues);
            CustomSupport.setInterpolatedValue((ConstraintAttribute)this.mConstraintAttributeList.valueAt(0), view, this.mTempValues);
        }
        
        @Override
        public void setup(final int n) {
            final int size = this.mConstraintAttributeList.size();
            final int numberOfInterpolatedValues = ((ConstraintAttribute)this.mConstraintAttributeList.valueAt(0)).numberOfInterpolatedValues();
            final double[] array = new double[size];
            this.mTempValues = new float[numberOfInterpolatedValues];
            final double[][] array2 = new double[size][numberOfInterpolatedValues];
            for (int i = 0; i < size; ++i) {
                final int key = this.mConstraintAttributeList.keyAt(i);
                final ConstraintAttribute constraintAttribute = (ConstraintAttribute)this.mConstraintAttributeList.valueAt(i);
                array[i] = key * 0.01;
                constraintAttribute.getValuesToInterpolate(this.mTempValues);
                int n2 = 0;
                while (true) {
                    final float[] mTempValues = this.mTempValues;
                    if (n2 >= mTempValues.length) {
                        break;
                    }
                    array2[i][n2] = mTempValues[n2];
                    ++n2;
                }
            }
            this.mCurveFit = CurveFit.get(n, array, array2);
        }
    }
    
    static class ElevationSet extends ViewSpline
    {
        @Override
        public void setProperty(final View view, final float n) {
            if (Build$VERSION.SDK_INT >= 21) {
                view.setElevation(this.get(n));
            }
        }
    }
    
    public static class PathRotate extends ViewSpline
    {
        public void setPathRotate(final View view, final float n, final double x, final double y) {
            view.setRotation(this.get(n) + (float)Math.toDegrees(Math.atan2(y, x)));
        }
        
        @Override
        public void setProperty(final View view, final float n) {
        }
    }
    
    static class PivotXset extends ViewSpline
    {
        @Override
        public void setProperty(final View view, final float n) {
            view.setPivotX(this.get(n));
        }
    }
    
    static class PivotYset extends ViewSpline
    {
        @Override
        public void setProperty(final View view, final float n) {
            view.setPivotY(this.get(n));
        }
    }
    
    static class ProgressSet extends ViewSpline
    {
        boolean mNoMethod;
        
        ProgressSet() {
            this.mNoMethod = false;
        }
        
        @Override
        public void setProperty(final View obj, final float n) {
            if (obj instanceof MotionLayout) {
                ((MotionLayout)obj).setProgress(this.get(n));
            }
            else {
                if (this.mNoMethod) {
                    return;
                }
                Method method = null;
                try {
                    method = obj.getClass().getMethod("setProgress", Float.TYPE);
                }
                catch (final NoSuchMethodException ex) {
                    this.mNoMethod = true;
                }
                if (method != null) {
                    try {
                        method.invoke(obj, this.get(n));
                    }
                    catch (final InvocationTargetException ex2) {
                        Log.e("ViewSpline", "unable to setProgress", (Throwable)ex2);
                    }
                    catch (final IllegalAccessException ex3) {
                        Log.e("ViewSpline", "unable to setProgress", (Throwable)ex3);
                    }
                }
            }
        }
    }
    
    static class RotationSet extends ViewSpline
    {
        @Override
        public void setProperty(final View view, final float n) {
            view.setRotation(this.get(n));
        }
    }
    
    static class RotationXset extends ViewSpline
    {
        @Override
        public void setProperty(final View view, final float n) {
            view.setRotationX(this.get(n));
        }
    }
    
    static class RotationYset extends ViewSpline
    {
        @Override
        public void setProperty(final View view, final float n) {
            view.setRotationY(this.get(n));
        }
    }
    
    static class ScaleXset extends ViewSpline
    {
        @Override
        public void setProperty(final View view, final float n) {
            view.setScaleX(this.get(n));
        }
    }
    
    static class ScaleYset extends ViewSpline
    {
        @Override
        public void setProperty(final View view, final float n) {
            view.setScaleY(this.get(n));
        }
    }
    
    static class TranslationXset extends ViewSpline
    {
        @Override
        public void setProperty(final View view, final float n) {
            view.setTranslationX(this.get(n));
        }
    }
    
    static class TranslationYset extends ViewSpline
    {
        @Override
        public void setProperty(final View view, final float n) {
            view.setTranslationY(this.get(n));
        }
    }
    
    static class TranslationZset extends ViewSpline
    {
        @Override
        public void setProperty(final View view, final float n) {
            if (Build$VERSION.SDK_INT >= 21) {
                view.setTranslationZ(this.get(n));
            }
        }
    }
}

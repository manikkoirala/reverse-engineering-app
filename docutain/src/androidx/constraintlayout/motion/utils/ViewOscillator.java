// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.motion.utils;

import java.lang.reflect.Method;
import java.lang.reflect.InvocationTargetException;
import android.util.Log;
import androidx.constraintlayout.motion.widget.MotionLayout;
import android.os.Build$VERSION;
import androidx.constraintlayout.widget.ConstraintAttribute;
import android.view.View;
import androidx.constraintlayout.core.motion.utils.KeyCycleOscillator;

public abstract class ViewOscillator extends KeyCycleOscillator
{
    private static final String TAG = "ViewOscillator";
    
    public static ViewOscillator makeSpline(final String s) {
        if (s.startsWith("CUSTOM")) {
            return new CustomSet();
        }
        s.hashCode();
        int n = -1;
        switch (s) {
            case "waveOffset": {
                n = 13;
                break;
            }
            case "alpha": {
                n = 12;
                break;
            }
            case "transitionPathRotate": {
                n = 11;
                break;
            }
            case "elevation": {
                n = 10;
                break;
            }
            case "rotation": {
                n = 9;
                break;
            }
            case "waveVariesBy": {
                n = 8;
                break;
            }
            case "scaleY": {
                n = 7;
                break;
            }
            case "scaleX": {
                n = 6;
                break;
            }
            case "progress": {
                n = 5;
                break;
            }
            case "translationZ": {
                n = 4;
                break;
            }
            case "translationY": {
                n = 3;
                break;
            }
            case "translationX": {
                n = 2;
                break;
            }
            case "rotationY": {
                n = 1;
                break;
            }
            case "rotationX": {
                n = 0;
                break;
            }
            default:
                break;
        }
        switch (n) {
            default: {
                return null;
            }
            case 13: {
                return new AlphaSet();
            }
            case 12: {
                return new AlphaSet();
            }
            case 11: {
                return new PathRotateSet();
            }
            case 10: {
                return new ElevationSet();
            }
            case 9: {
                return new RotationSet();
            }
            case 8: {
                return new AlphaSet();
            }
            case 7: {
                return new ScaleYset();
            }
            case 6: {
                return new ScaleXset();
            }
            case 5: {
                return new ProgressSet();
            }
            case 4: {
                return new TranslationZset();
            }
            case 3: {
                return new TranslationYset();
            }
            case 2: {
                return new TranslationXset();
            }
            case 1: {
                return new RotationYset();
            }
            case 0: {
                return new RotationXset();
            }
        }
    }
    
    public abstract void setProperty(final View p0, final float p1);
    
    static class AlphaSet extends ViewOscillator
    {
        @Override
        public void setProperty(final View view, final float n) {
            view.setAlpha(this.get(n));
        }
    }
    
    static class CustomSet extends ViewOscillator
    {
        protected ConstraintAttribute mCustom;
        float[] value;
        
        CustomSet() {
            this.value = new float[1];
        }
        
        @Override
        protected void setCustom(final Object o) {
            this.mCustom = (ConstraintAttribute)o;
        }
        
        @Override
        public void setProperty(final View view, final float n) {
            this.value[0] = this.get(n);
            CustomSupport.setInterpolatedValue(this.mCustom, view, this.value);
        }
    }
    
    static class ElevationSet extends ViewOscillator
    {
        @Override
        public void setProperty(final View view, final float n) {
            if (Build$VERSION.SDK_INT >= 21) {
                view.setElevation(this.get(n));
            }
        }
    }
    
    public static class PathRotateSet extends ViewOscillator
    {
        public void setPathRotate(final View view, final float n, final double x, final double y) {
            view.setRotation(this.get(n) + (float)Math.toDegrees(Math.atan2(y, x)));
        }
        
        @Override
        public void setProperty(final View view, final float n) {
        }
    }
    
    static class ProgressSet extends ViewOscillator
    {
        boolean mNoMethod;
        
        ProgressSet() {
            this.mNoMethod = false;
        }
        
        @Override
        public void setProperty(final View obj, final float n) {
            if (obj instanceof MotionLayout) {
                ((MotionLayout)obj).setProgress(this.get(n));
            }
            else {
                if (this.mNoMethod) {
                    return;
                }
                Method method = null;
                try {
                    method = obj.getClass().getMethod("setProgress", Float.TYPE);
                }
                catch (final NoSuchMethodException ex) {
                    this.mNoMethod = true;
                }
                if (method != null) {
                    try {
                        method.invoke(obj, this.get(n));
                    }
                    catch (final InvocationTargetException ex2) {
                        Log.e("ViewOscillator", "unable to setProgress", (Throwable)ex2);
                    }
                    catch (final IllegalAccessException ex3) {
                        Log.e("ViewOscillator", "unable to setProgress", (Throwable)ex3);
                    }
                }
            }
        }
    }
    
    static class RotationSet extends ViewOscillator
    {
        @Override
        public void setProperty(final View view, final float n) {
            view.setRotation(this.get(n));
        }
    }
    
    static class RotationXset extends ViewOscillator
    {
        @Override
        public void setProperty(final View view, final float n) {
            view.setRotationX(this.get(n));
        }
    }
    
    static class RotationYset extends ViewOscillator
    {
        @Override
        public void setProperty(final View view, final float n) {
            view.setRotationY(this.get(n));
        }
    }
    
    static class ScaleXset extends ViewOscillator
    {
        @Override
        public void setProperty(final View view, final float n) {
            view.setScaleX(this.get(n));
        }
    }
    
    static class ScaleYset extends ViewOscillator
    {
        @Override
        public void setProperty(final View view, final float n) {
            view.setScaleY(this.get(n));
        }
    }
    
    static class TranslationXset extends ViewOscillator
    {
        @Override
        public void setProperty(final View view, final float n) {
            view.setTranslationX(this.get(n));
        }
    }
    
    static class TranslationYset extends ViewOscillator
    {
        @Override
        public void setProperty(final View view, final float n) {
            view.setTranslationY(this.get(n));
        }
    }
    
    static class TranslationZset extends ViewOscillator
    {
        @Override
        public void setProperty(final View view, final float n) {
            if (Build$VERSION.SDK_INT >= 21) {
                view.setTranslationZ(this.get(n));
            }
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.motion.utils;

import java.lang.reflect.Method;
import java.lang.reflect.InvocationTargetException;
import android.util.Log;
import androidx.constraintlayout.motion.widget.MotionLayout;
import android.os.Build$VERSION;
import androidx.constraintlayout.core.motion.utils.CurveFit;
import androidx.constraintlayout.core.motion.utils.KeyCache;
import android.view.View;
import androidx.constraintlayout.widget.ConstraintAttribute;
import android.util.SparseArray;
import androidx.constraintlayout.core.motion.utils.TimeCycleSplineSet;

public abstract class ViewTimeCycle extends TimeCycleSplineSet
{
    private static final String TAG = "ViewTimeCycle";
    
    public static ViewTimeCycle makeCustomSpline(final String s, final SparseArray<ConstraintAttribute> sparseArray) {
        return new CustomSet(s, sparseArray);
    }
    
    public static ViewTimeCycle makeSpline(final String s, final long startTime) {
        s.hashCode();
        final int hashCode = s.hashCode();
        int n = -1;
        switch (hashCode) {
            case 92909918: {
                if (!s.equals("alpha")) {
                    break;
                }
                n = 11;
                break;
            }
            case 37232917: {
                if (!s.equals("transitionPathRotate")) {
                    break;
                }
                n = 10;
                break;
            }
            case -4379043: {
                if (!s.equals("elevation")) {
                    break;
                }
                n = 9;
                break;
            }
            case -40300674: {
                if (!s.equals("rotation")) {
                    break;
                }
                n = 8;
                break;
            }
            case -908189617: {
                if (!s.equals("scaleY")) {
                    break;
                }
                n = 7;
                break;
            }
            case -908189618: {
                if (!s.equals("scaleX")) {
                    break;
                }
                n = 6;
                break;
            }
            case -1001078227: {
                if (!s.equals("progress")) {
                    break;
                }
                n = 5;
                break;
            }
            case -1225497655: {
                if (!s.equals("translationZ")) {
                    break;
                }
                n = 4;
                break;
            }
            case -1225497656: {
                if (!s.equals("translationY")) {
                    break;
                }
                n = 3;
                break;
            }
            case -1225497657: {
                if (!s.equals("translationX")) {
                    break;
                }
                n = 2;
                break;
            }
            case -1249320805: {
                if (!s.equals("rotationY")) {
                    break;
                }
                n = 1;
                break;
            }
            case -1249320806: {
                if (!s.equals("rotationX")) {
                    break;
                }
                n = 0;
                break;
            }
        }
        ViewTimeCycle viewTimeCycle = null;
        switch (n) {
            default: {
                return null;
            }
            case 11: {
                viewTimeCycle = new AlphaSet();
                break;
            }
            case 10: {
                viewTimeCycle = new PathRotate();
                break;
            }
            case 9: {
                viewTimeCycle = new ElevationSet();
                break;
            }
            case 8: {
                viewTimeCycle = new RotationSet();
                break;
            }
            case 7: {
                viewTimeCycle = new ScaleYset();
                break;
            }
            case 6: {
                viewTimeCycle = new ScaleXset();
                break;
            }
            case 5: {
                viewTimeCycle = new ProgressSet();
                break;
            }
            case 4: {
                viewTimeCycle = new TranslationZset();
                break;
            }
            case 3: {
                viewTimeCycle = new TranslationYset();
                break;
            }
            case 2: {
                viewTimeCycle = new TranslationXset();
                break;
            }
            case 1: {
                viewTimeCycle = new RotationYset();
                break;
            }
            case 0: {
                viewTimeCycle = new RotationXset();
                break;
            }
        }
        viewTimeCycle.setStartTime(startTime);
        return viewTimeCycle;
    }
    
    public float get(float calcWave, final long last_time, final View view, final KeyCache keyCache) {
        this.mCurveFit.getPos(calcWave, this.mCache);
        calcWave = this.mCache[1];
        final float n = fcmpl(calcWave, 0.0f);
        if (n == 0) {
            this.mContinue = false;
            return this.mCache[2];
        }
        if (Float.isNaN(this.last_cycle)) {
            this.last_cycle = keyCache.getFloatValue(view, this.mType, 0);
            if (Float.isNaN(this.last_cycle)) {
                this.last_cycle = 0.0f;
            }
        }
        this.last_cycle = (float)((this.last_cycle + (last_time - this.last_time) * 1.0E-9 * calcWave) % 1.0);
        keyCache.setFloatValue(view, this.mType, 0, this.last_cycle);
        this.last_time = last_time;
        final float n2 = this.mCache[0];
        calcWave = this.calcWave(this.last_cycle);
        final float n3 = this.mCache[2];
        this.mContinue = (n2 != 0.0f || n != 0);
        return calcWave * n2 + n3;
    }
    
    public abstract boolean setProperty(final View p0, final float p1, final long p2, final KeyCache p3);
    
    static class AlphaSet extends ViewTimeCycle
    {
        @Override
        public boolean setProperty(final View view, final float n, final long n2, final KeyCache keyCache) {
            view.setAlpha(this.get(n, n2, view, keyCache));
            return this.mContinue;
        }
    }
    
    public static class CustomSet extends ViewTimeCycle
    {
        String mAttributeName;
        float[] mCache;
        SparseArray<ConstraintAttribute> mConstraintAttributeList;
        float[] mTempValues;
        SparseArray<float[]> mWaveProperties;
        
        public CustomSet(final String s, final SparseArray<ConstraintAttribute> mConstraintAttributeList) {
            this.mWaveProperties = (SparseArray<float[]>)new SparseArray();
            this.mAttributeName = s.split(",")[1];
            this.mConstraintAttributeList = mConstraintAttributeList;
        }
        
        @Override
        public void setPoint(final int n, final float n2, final float n3, final int n4, final float n5) {
            throw new RuntimeException("don't call for custom attribute call setPoint(pos, ConstraintAttribute,...)");
        }
        
        public void setPoint(final int n, final ConstraintAttribute constraintAttribute, final float n2, final int b, final float n3) {
            this.mConstraintAttributeList.append(n, (Object)constraintAttribute);
            this.mWaveProperties.append(n, (Object)new float[] { n2, n3 });
            this.mWaveShape = Math.max(this.mWaveShape, b);
        }
        
        @Override
        public boolean setProperty(final View view, float calcWave, final long last_time, final KeyCache keyCache) {
            this.mCurveFit.getPos(calcWave, this.mTempValues);
            final float[] mTempValues = this.mTempValues;
            final float n = mTempValues[mTempValues.length - 2];
            final float n2 = mTempValues[mTempValues.length - 1];
            final long last_time2 = this.last_time;
            if (Float.isNaN(this.last_cycle)) {
                this.last_cycle = keyCache.getFloatValue(view, this.mAttributeName, 0);
                if (Float.isNaN(this.last_cycle)) {
                    this.last_cycle = 0.0f;
                }
            }
            this.last_cycle = (float)((this.last_cycle + (last_time - last_time2) * 1.0E-9 * n) % 1.0);
            this.last_time = last_time;
            calcWave = this.calcWave(this.last_cycle);
            this.mContinue = false;
            for (int i = 0; i < this.mCache.length; ++i) {
                this.mContinue |= (this.mTempValues[i] != 0.0);
                this.mCache[i] = this.mTempValues[i] * calcWave + n2;
            }
            CustomSupport.setInterpolatedValue((ConstraintAttribute)this.mConstraintAttributeList.valueAt(0), view, this.mCache);
            if (n != 0.0f) {
                this.mContinue = true;
            }
            return this.mContinue;
        }
        
        @Override
        public void setup(final int n) {
            final int size = this.mConstraintAttributeList.size();
            final int numberOfInterpolatedValues = ((ConstraintAttribute)this.mConstraintAttributeList.valueAt(0)).numberOfInterpolatedValues();
            final double[] array = new double[size];
            final int n2 = numberOfInterpolatedValues + 2;
            this.mTempValues = new float[n2];
            this.mCache = new float[numberOfInterpolatedValues];
            final double[][] array2 = new double[size][n2];
            for (int i = 0; i < size; ++i) {
                final int key = this.mConstraintAttributeList.keyAt(i);
                final ConstraintAttribute constraintAttribute = (ConstraintAttribute)this.mConstraintAttributeList.valueAt(i);
                final float[] array3 = (float[])this.mWaveProperties.valueAt(i);
                array[i] = key * 0.01;
                constraintAttribute.getValuesToInterpolate(this.mTempValues);
                int n3 = 0;
                while (true) {
                    final float[] mTempValues = this.mTempValues;
                    if (n3 >= mTempValues.length) {
                        break;
                    }
                    array2[i][n3] = mTempValues[n3];
                    ++n3;
                }
                final double[] array4 = array2[i];
                array4[numberOfInterpolatedValues] = array3[0];
                array4[numberOfInterpolatedValues + 1] = array3[1];
            }
            this.mCurveFit = CurveFit.get(n, array, array2);
        }
    }
    
    static class ElevationSet extends ViewTimeCycle
    {
        @Override
        public boolean setProperty(final View view, final float n, final long n2, final KeyCache keyCache) {
            if (Build$VERSION.SDK_INT >= 21) {
                view.setElevation(this.get(n, n2, view, keyCache));
            }
            return this.mContinue;
        }
    }
    
    public static class PathRotate extends ViewTimeCycle
    {
        public boolean setPathRotate(final View view, final KeyCache keyCache, final float n, final long n2, final double x, final double y) {
            view.setRotation(this.get(n, n2, view, keyCache) + (float)Math.toDegrees(Math.atan2(y, x)));
            return this.mContinue;
        }
        
        @Override
        public boolean setProperty(final View view, final float n, final long n2, final KeyCache keyCache) {
            return this.mContinue;
        }
    }
    
    static class ProgressSet extends ViewTimeCycle
    {
        boolean mNoMethod;
        
        ProgressSet() {
            this.mNoMethod = false;
        }
        
        @Override
        public boolean setProperty(final View obj, final float n, final long n2, final KeyCache keyCache) {
            if (obj instanceof MotionLayout) {
                ((MotionLayout)obj).setProgress(this.get(n, n2, obj, keyCache));
            }
            else {
                if (this.mNoMethod) {
                    return false;
                }
                Method method = null;
                try {
                    method = obj.getClass().getMethod("setProgress", Float.TYPE);
                }
                catch (final NoSuchMethodException ex) {
                    this.mNoMethod = true;
                }
                if (method != null) {
                    try {
                        method.invoke(obj, this.get(n, n2, obj, keyCache));
                    }
                    catch (final InvocationTargetException ex2) {
                        Log.e("ViewTimeCycle", "unable to setProgress", (Throwable)ex2);
                    }
                    catch (final IllegalAccessException ex3) {
                        Log.e("ViewTimeCycle", "unable to setProgress", (Throwable)ex3);
                    }
                }
            }
            return this.mContinue;
        }
    }
    
    static class RotationSet extends ViewTimeCycle
    {
        @Override
        public boolean setProperty(final View view, final float n, final long n2, final KeyCache keyCache) {
            view.setRotation(this.get(n, n2, view, keyCache));
            return this.mContinue;
        }
    }
    
    static class RotationXset extends ViewTimeCycle
    {
        @Override
        public boolean setProperty(final View view, final float n, final long n2, final KeyCache keyCache) {
            view.setRotationX(this.get(n, n2, view, keyCache));
            return this.mContinue;
        }
    }
    
    static class RotationYset extends ViewTimeCycle
    {
        @Override
        public boolean setProperty(final View view, final float n, final long n2, final KeyCache keyCache) {
            view.setRotationY(this.get(n, n2, view, keyCache));
            return this.mContinue;
        }
    }
    
    static class ScaleXset extends ViewTimeCycle
    {
        @Override
        public boolean setProperty(final View view, final float n, final long n2, final KeyCache keyCache) {
            view.setScaleX(this.get(n, n2, view, keyCache));
            return this.mContinue;
        }
    }
    
    static class ScaleYset extends ViewTimeCycle
    {
        @Override
        public boolean setProperty(final View view, final float n, final long n2, final KeyCache keyCache) {
            view.setScaleY(this.get(n, n2, view, keyCache));
            return this.mContinue;
        }
    }
    
    static class TranslationXset extends ViewTimeCycle
    {
        @Override
        public boolean setProperty(final View view, final float n, final long n2, final KeyCache keyCache) {
            view.setTranslationX(this.get(n, n2, view, keyCache));
            return this.mContinue;
        }
    }
    
    static class TranslationYset extends ViewTimeCycle
    {
        @Override
        public boolean setProperty(final View view, final float n, final long n2, final KeyCache keyCache) {
            view.setTranslationY(this.get(n, n2, view, keyCache));
            return this.mContinue;
        }
    }
    
    static class TranslationZset extends ViewTimeCycle
    {
        @Override
        public boolean setProperty(final View view, final float n, final long n2, final KeyCache keyCache) {
            if (Build$VERSION.SDK_INT >= 21) {
                view.setTranslationZ(this.get(n, n2, view, keyCache));
            }
            return this.mContinue;
        }
    }
}

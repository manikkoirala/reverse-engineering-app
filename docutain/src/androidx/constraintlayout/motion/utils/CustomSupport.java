// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.motion.utils;

import java.lang.reflect.Method;
import android.util.Log;
import androidx.constraintlayout.motion.widget.Debug;
import java.lang.reflect.InvocationTargetException;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.view.View;
import androidx.constraintlayout.widget.ConstraintAttribute;

public class CustomSupport
{
    private static final String TAG = "CustomSupport";
    
    private static int clamp(int n) {
        n = (n & ~(n >> 31)) - 255;
        return (n & n >> 31) + 255;
    }
    
    public static void setInterpolatedValue(final ConstraintAttribute constraintAttribute, final View view, final float[] array) {
        final Class<? extends View> class1 = view.getClass();
        final StringBuilder sb = new StringBuilder();
        sb.append("set");
        sb.append(constraintAttribute.getName());
        final String string = sb.toString();
        try {
            final int n = CustomSupport$1.$SwitchMap$androidx$constraintlayout$widget$ConstraintAttribute$AttributeType[constraintAttribute.getType().ordinal()];
            boolean b = true;
            switch (n) {
                case 7: {
                    class1.getMethod(string, Float.TYPE).invoke(view, array[0]);
                    break;
                }
                case 6: {
                    final Method method = class1.getMethod(string, Boolean.TYPE);
                    if (array[0] <= 0.5f) {
                        b = false;
                    }
                    method.invoke(view, b);
                    break;
                }
                case 5: {
                    final StringBuilder sb2 = new StringBuilder();
                    sb2.append("unable to interpolate strings ");
                    sb2.append(constraintAttribute.getName());
                    throw new RuntimeException(sb2.toString());
                }
                case 4: {
                    class1.getMethod(string, Integer.TYPE).invoke(view, clamp((int)((float)Math.pow(array[0], 0.45454545454545453) * 255.0f)) << 16 | clamp((int)(array[3] * 255.0f)) << 24 | clamp((int)((float)Math.pow(array[1], 0.45454545454545453) * 255.0f)) << 8 | clamp((int)((float)Math.pow(array[2], 0.45454545454545453) * 255.0f)));
                    break;
                }
                case 3: {
                    final Method method2 = class1.getMethod(string, Drawable.class);
                    final int clamp = clamp((int)((float)Math.pow(array[0], 0.45454545454545453) * 255.0f));
                    final int clamp2 = clamp((int)((float)Math.pow(array[1], 0.45454545454545453) * 255.0f));
                    final int clamp3 = clamp((int)((float)Math.pow(array[2], 0.45454545454545453) * 255.0f));
                    final int clamp4 = clamp((int)(array[3] * 255.0f));
                    final ColorDrawable colorDrawable = new ColorDrawable();
                    colorDrawable.setColor(clamp << 16 | clamp4 << 24 | clamp2 << 8 | clamp3);
                    method2.invoke(view, colorDrawable);
                    break;
                }
                case 2: {
                    class1.getMethod(string, Float.TYPE).invoke(view, array[0]);
                    break;
                }
                case 1: {
                    class1.getMethod(string, Integer.TYPE).invoke(view, (int)array[0]);
                    break;
                }
            }
        }
        catch (final InvocationTargetException ex) {
            ex.printStackTrace();
        }
        catch (final IllegalAccessException ex2) {
            final StringBuilder sb3 = new StringBuilder();
            sb3.append("cannot access method ");
            sb3.append(string);
            sb3.append(" on View \"");
            sb3.append(Debug.getName(view));
            sb3.append("\"");
            Log.e("CustomSupport", sb3.toString());
            ex2.printStackTrace();
        }
        catch (final NoSuchMethodException ex3) {
            final StringBuilder sb4 = new StringBuilder();
            sb4.append("no method ");
            sb4.append(string);
            sb4.append(" on View \"");
            sb4.append(Debug.getName(view));
            sb4.append("\"");
            Log.e("CustomSupport", sb4.toString());
            ex3.printStackTrace();
        }
    }
}

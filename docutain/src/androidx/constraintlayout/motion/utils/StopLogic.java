// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.motion.utils;

import androidx.constraintlayout.core.motion.utils.StopLogicEngine;
import androidx.constraintlayout.core.motion.utils.SpringStopEngine;
import androidx.constraintlayout.core.motion.utils.StopEngine;
import androidx.constraintlayout.motion.widget.MotionInterpolator;

public class StopLogic extends MotionInterpolator
{
    private StopEngine mEngine;
    private SpringStopEngine mSpringStopEngine;
    private StopLogicEngine mStopLogicEngine;
    
    public StopLogic() {
        final StopLogicEngine stopLogicEngine = new StopLogicEngine();
        this.mStopLogicEngine = stopLogicEngine;
        this.mEngine = stopLogicEngine;
    }
    
    public void config(final float n, final float n2, final float n3, final float n4, final float n5, final float n6) {
        ((StopLogicEngine)(this.mEngine = this.mStopLogicEngine)).config(n, n2, n3, n4, n5, n6);
    }
    
    public String debug(final String s, final float n) {
        return this.mEngine.debug(s, n);
    }
    
    @Override
    public float getInterpolation(final float n) {
        return this.mEngine.getInterpolation(n);
    }
    
    @Override
    public float getVelocity() {
        return this.mEngine.getVelocity();
    }
    
    public float getVelocity(final float n) {
        return this.mEngine.getVelocity(n);
    }
    
    public boolean isStopped() {
        return this.mEngine.isStopped();
    }
    
    public void springConfig(final float n, final float n2, final float n3, final float n4, final float n5, final float n6, final float n7, final int n8) {
        if (this.mSpringStopEngine == null) {
            this.mSpringStopEngine = new SpringStopEngine();
        }
        ((SpringStopEngine)(this.mEngine = this.mSpringStopEngine)).springConfig(n, n2, n3, n4, n5, n6, n7, n8);
    }
}

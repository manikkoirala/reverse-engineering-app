// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.widget;

import android.graphics.Color;
import java.lang.reflect.Method;
import android.util.Log;
import android.graphics.drawable.Drawable;
import java.io.Serializable;
import android.content.res.TypedArray;
import android.util.TypedValue;
import android.util.Xml;
import org.xmlpull.v1.XmlPullParser;
import android.content.Context;
import java.util.Iterator;
import java.lang.reflect.InvocationTargetException;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import java.util.HashMap;

public class ConstraintAttribute
{
    private static final String TAG = "TransitionLayout";
    boolean mBooleanValue;
    private int mColorValue;
    private float mFloatValue;
    private int mIntegerValue;
    private boolean mMethod;
    String mName;
    private String mStringValue;
    private AttributeType mType;
    
    public ConstraintAttribute(final ConstraintAttribute constraintAttribute, final Object value) {
        this.mMethod = false;
        this.mName = constraintAttribute.mName;
        this.mType = constraintAttribute.mType;
        this.setValue(value);
    }
    
    public ConstraintAttribute(final String mName, final AttributeType mType) {
        this.mMethod = false;
        this.mName = mName;
        this.mType = mType;
    }
    
    public ConstraintAttribute(final String mName, final AttributeType mType, final Object value, final boolean mMethod) {
        this.mName = mName;
        this.mType = mType;
        this.mMethod = mMethod;
        this.setValue(value);
    }
    
    private static int clamp(int n) {
        n = (n & ~(n >> 31)) - 255;
        return (n & n >> 31) + 255;
    }
    
    public static HashMap<String, ConstraintAttribute> extractAttributes(final HashMap<String, ConstraintAttribute> hashMap, final View obj) {
        final HashMap hashMap2 = new HashMap();
        final Class<? extends View> class1 = obj.getClass();
        for (final String s : hashMap.keySet()) {
            final ConstraintAttribute constraintAttribute = hashMap.get(s);
            try {
                if (s.equals("BackgroundColor")) {
                    hashMap2.put(s, new ConstraintAttribute(constraintAttribute, ((ColorDrawable)obj.getBackground()).getColor()));
                }
                else {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("getMap");
                    sb.append(s);
                    hashMap2.put(s, new ConstraintAttribute(constraintAttribute, class1.getMethod(sb.toString(), (Class<?>[])new Class[0]).invoke(obj, new Object[0])));
                }
            }
            catch (final InvocationTargetException ex) {
                ex.printStackTrace();
            }
            catch (final IllegalAccessException ex2) {
                ex2.printStackTrace();
            }
            catch (final NoSuchMethodException ex3) {
                ex3.printStackTrace();
            }
        }
        return hashMap2;
    }
    
    public static void parse(final Context context, final XmlPullParser xmlPullParser, final HashMap<String, ConstraintAttribute> hashMap) {
        final TypedArray obtainStyledAttributes = context.obtainStyledAttributes(Xml.asAttributeSet(xmlPullParser), R.styleable.CustomAttribute);
        final int indexCount = obtainStyledAttributes.getIndexCount();
        String key = null;
        Serializable s = null;
        AttributeType attributeType = null;
        int i = 0;
        int n = 0;
        while (i < indexCount) {
            final int index = obtainStyledAttributes.getIndex(i);
            String s3 = null;
            Serializable s4 = null;
            AttributeType boolean_TYPE = null;
            int n2 = 0;
            Label_0509: {
                if (index == R.styleable.CustomAttribute_attributeName) {
                    final String s2 = s3 = obtainStyledAttributes.getString(index);
                    s4 = s;
                    boolean_TYPE = attributeType;
                    n2 = n;
                    if (s2 != null) {
                        s3 = s2;
                        s4 = s;
                        boolean_TYPE = attributeType;
                        n2 = n;
                        if (s2.length() > 0) {
                            final StringBuilder sb = new StringBuilder();
                            sb.append(Character.toUpperCase(s2.charAt(0)));
                            sb.append(s2.substring(1));
                            s3 = sb.toString();
                            s4 = s;
                            boolean_TYPE = attributeType;
                            n2 = n;
                        }
                    }
                }
                else if (index == R.styleable.CustomAttribute_methodName) {
                    s3 = obtainStyledAttributes.getString(index);
                    n2 = 1;
                    s4 = s;
                    boolean_TYPE = attributeType;
                }
                else if (index == R.styleable.CustomAttribute_customBoolean) {
                    s4 = obtainStyledAttributes.getBoolean(index, false);
                    boolean_TYPE = AttributeType.BOOLEAN_TYPE;
                    s3 = key;
                    n2 = n;
                }
                else {
                    AttributeType attributeType2;
                    if (index == R.styleable.CustomAttribute_customColorValue) {
                        attributeType2 = AttributeType.COLOR_TYPE;
                        s4 = obtainStyledAttributes.getColor(index, 0);
                    }
                    else if (index == R.styleable.CustomAttribute_customColorDrawableValue) {
                        attributeType2 = AttributeType.COLOR_DRAWABLE_TYPE;
                        s4 = obtainStyledAttributes.getColor(index, 0);
                    }
                    else if (index == R.styleable.CustomAttribute_customPixelDimension) {
                        attributeType2 = AttributeType.DIMENSION_TYPE;
                        s4 = TypedValue.applyDimension(1, obtainStyledAttributes.getDimension(index, 0.0f), context.getResources().getDisplayMetrics());
                    }
                    else if (index == R.styleable.CustomAttribute_customDimension) {
                        attributeType2 = AttributeType.DIMENSION_TYPE;
                        s4 = obtainStyledAttributes.getDimension(index, 0.0f);
                    }
                    else if (index == R.styleable.CustomAttribute_customFloatValue) {
                        attributeType2 = AttributeType.FLOAT_TYPE;
                        s4 = obtainStyledAttributes.getFloat(index, Float.NaN);
                    }
                    else if (index == R.styleable.CustomAttribute_customIntegerValue) {
                        attributeType2 = AttributeType.INT_TYPE;
                        s4 = obtainStyledAttributes.getInteger(index, -1);
                    }
                    else if (index == R.styleable.CustomAttribute_customStringValue) {
                        attributeType2 = AttributeType.STRING_TYPE;
                        s4 = obtainStyledAttributes.getString(index);
                    }
                    else {
                        s3 = key;
                        s4 = s;
                        boolean_TYPE = attributeType;
                        n2 = n;
                        if (index != R.styleable.CustomAttribute_customReference) {
                            break Label_0509;
                        }
                        attributeType2 = AttributeType.REFERENCE_TYPE;
                        int j;
                        if ((j = obtainStyledAttributes.getResourceId(index, -1)) == -1) {
                            j = obtainStyledAttributes.getInt(index, -1);
                        }
                        s4 = j;
                    }
                    boolean_TYPE = attributeType2;
                    s3 = key;
                    n2 = n;
                }
            }
            ++i;
            key = s3;
            s = s4;
            attributeType = boolean_TYPE;
            n = n2;
        }
        if (key != null && s != null) {
            hashMap.put(key, new ConstraintAttribute(key, attributeType, s, (boolean)(n != 0)));
        }
        obtainStyledAttributes.recycle();
    }
    
    public static void setAttributes(final View view, final HashMap<String, ConstraintAttribute> hashMap) {
        final Class<? extends View> class1 = view.getClass();
        for (final String str : hashMap.keySet()) {
            final ConstraintAttribute constraintAttribute = hashMap.get(str);
            String string;
            if (!constraintAttribute.mMethod) {
                final StringBuilder sb = new StringBuilder();
                sb.append("set");
                sb.append(str);
                string = sb.toString();
            }
            else {
                string = str;
            }
            try {
                switch (ConstraintAttribute$1.$SwitchMap$androidx$constraintlayout$widget$ConstraintAttribute$AttributeType[constraintAttribute.mType.ordinal()]) {
                    default: {
                        continue;
                    }
                    case 8: {
                        class1.getMethod(string, Float.TYPE).invoke(view, constraintAttribute.mFloatValue);
                        continue;
                    }
                    case 7: {
                        class1.getMethod(string, Float.TYPE).invoke(view, constraintAttribute.mFloatValue);
                        continue;
                    }
                    case 6: {
                        class1.getMethod(string, Integer.TYPE).invoke(view, constraintAttribute.mIntegerValue);
                        continue;
                    }
                    case 5: {
                        final Method method = class1.getMethod(string, Drawable.class);
                        final ColorDrawable colorDrawable = new ColorDrawable();
                        colorDrawable.setColor(constraintAttribute.mColorValue);
                        method.invoke(view, colorDrawable);
                        continue;
                    }
                    case 4: {
                        class1.getMethod(string, Integer.TYPE).invoke(view, constraintAttribute.mColorValue);
                        continue;
                    }
                    case 3: {
                        class1.getMethod(string, CharSequence.class).invoke(view, constraintAttribute.mStringValue);
                        continue;
                    }
                    case 2: {
                        class1.getMethod(string, Boolean.TYPE).invoke(view, constraintAttribute.mBooleanValue);
                        continue;
                    }
                    case 1: {
                        class1.getMethod(string, Integer.TYPE).invoke(view, constraintAttribute.mIntegerValue);
                        continue;
                    }
                }
            }
            catch (final InvocationTargetException ex) {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append(" Custom Attribute \"");
                sb2.append(str);
                sb2.append("\" not found on ");
                sb2.append(class1.getName());
                Log.e("TransitionLayout", sb2.toString());
                ex.printStackTrace();
            }
            catch (final IllegalAccessException ex2) {
                final StringBuilder sb3 = new StringBuilder();
                sb3.append(" Custom Attribute \"");
                sb3.append(str);
                sb3.append("\" not found on ");
                sb3.append(class1.getName());
                Log.e("TransitionLayout", sb3.toString());
                ex2.printStackTrace();
            }
            catch (final NoSuchMethodException ex3) {
                Log.e("TransitionLayout", ex3.getMessage());
                final StringBuilder sb4 = new StringBuilder();
                sb4.append(" Custom Attribute \"");
                sb4.append(str);
                sb4.append("\" not found on ");
                sb4.append(class1.getName());
                Log.e("TransitionLayout", sb4.toString());
                final StringBuilder sb5 = new StringBuilder();
                sb5.append(class1.getName());
                sb5.append(" must have a method ");
                sb5.append(string);
                Log.e("TransitionLayout", sb5.toString());
            }
        }
    }
    
    public void applyCustom(final View obj) {
        final Class<? extends View> class1 = obj.getClass();
        final String mName = this.mName;
        String string;
        if (!this.mMethod) {
            final StringBuilder sb = new StringBuilder();
            sb.append("set");
            sb.append(mName);
            string = sb.toString();
        }
        else {
            string = mName;
        }
        try {
            switch (ConstraintAttribute$1.$SwitchMap$androidx$constraintlayout$widget$ConstraintAttribute$AttributeType[this.mType.ordinal()]) {
                case 8: {
                    class1.getMethod(string, Float.TYPE).invoke(obj, this.mFloatValue);
                    break;
                }
                case 7: {
                    class1.getMethod(string, Float.TYPE).invoke(obj, this.mFloatValue);
                    break;
                }
                case 5: {
                    final Method method = class1.getMethod(string, Drawable.class);
                    final ColorDrawable colorDrawable = new ColorDrawable();
                    colorDrawable.setColor(this.mColorValue);
                    method.invoke(obj, colorDrawable);
                    break;
                }
                case 4: {
                    class1.getMethod(string, Integer.TYPE).invoke(obj, this.mColorValue);
                    break;
                }
                case 3: {
                    class1.getMethod(string, CharSequence.class).invoke(obj, this.mStringValue);
                    break;
                }
                case 2: {
                    class1.getMethod(string, Boolean.TYPE).invoke(obj, this.mBooleanValue);
                    break;
                }
                case 1:
                case 6: {
                    class1.getMethod(string, Integer.TYPE).invoke(obj, this.mIntegerValue);
                    break;
                }
            }
        }
        catch (final InvocationTargetException ex) {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append(" Custom Attribute \"");
            sb2.append(mName);
            sb2.append("\" not found on ");
            sb2.append(class1.getName());
            Log.e("TransitionLayout", sb2.toString());
            ex.printStackTrace();
        }
        catch (final IllegalAccessException ex2) {
            final StringBuilder sb3 = new StringBuilder();
            sb3.append(" Custom Attribute \"");
            sb3.append(mName);
            sb3.append("\" not found on ");
            sb3.append(class1.getName());
            Log.e("TransitionLayout", sb3.toString());
            ex2.printStackTrace();
        }
        catch (final NoSuchMethodException ex3) {
            Log.e("TransitionLayout", ex3.getMessage());
            final StringBuilder sb4 = new StringBuilder();
            sb4.append(" Custom Attribute \"");
            sb4.append(mName);
            sb4.append("\" not found on ");
            sb4.append(class1.getName());
            Log.e("TransitionLayout", sb4.toString());
            final StringBuilder sb5 = new StringBuilder();
            sb5.append(class1.getName());
            sb5.append(" must have a method ");
            sb5.append(string);
            Log.e("TransitionLayout", sb5.toString());
        }
    }
    
    public boolean diff(final ConstraintAttribute constraintAttribute) {
        final boolean b = false;
        final boolean b2 = false;
        final boolean b3 = false;
        final boolean b4 = false;
        final boolean b5 = false;
        final boolean b6 = false;
        boolean b7 = b5;
        if (constraintAttribute != null) {
            if (this.mType != constraintAttribute.mType) {
                b7 = b5;
            }
            else {
                switch (ConstraintAttribute$1.$SwitchMap$androidx$constraintlayout$widget$ConstraintAttribute$AttributeType[this.mType.ordinal()]) {
                    default: {
                        return false;
                    }
                    case 8: {
                        boolean b8 = b6;
                        if (this.mFloatValue == constraintAttribute.mFloatValue) {
                            b8 = true;
                        }
                        return b8;
                    }
                    case 7: {
                        boolean b9 = b;
                        if (this.mFloatValue == constraintAttribute.mFloatValue) {
                            b9 = true;
                        }
                        return b9;
                    }
                    case 4:
                    case 5: {
                        boolean b10 = b2;
                        if (this.mColorValue == constraintAttribute.mColorValue) {
                            b10 = true;
                        }
                        return b10;
                    }
                    case 3: {
                        boolean b11 = b3;
                        if (this.mIntegerValue == constraintAttribute.mIntegerValue) {
                            b11 = true;
                        }
                        return b11;
                    }
                    case 2: {
                        boolean b12 = b4;
                        if (this.mBooleanValue == constraintAttribute.mBooleanValue) {
                            b12 = true;
                        }
                        return b12;
                    }
                    case 1:
                    case 6: {
                        b7 = b5;
                        if (this.mIntegerValue == constraintAttribute.mIntegerValue) {
                            b7 = true;
                            break;
                        }
                        break;
                    }
                }
            }
        }
        return b7;
    }
    
    public int getColorValue() {
        return this.mColorValue;
    }
    
    public float getFloatValue() {
        return this.mFloatValue;
    }
    
    public int getIntegerValue() {
        return this.mIntegerValue;
    }
    
    public String getName() {
        return this.mName;
    }
    
    public String getStringValue() {
        return this.mStringValue;
    }
    
    public AttributeType getType() {
        return this.mType;
    }
    
    public float getValueToInterpolate() {
        switch (ConstraintAttribute$1.$SwitchMap$androidx$constraintlayout$widget$ConstraintAttribute$AttributeType[this.mType.ordinal()]) {
            default: {
                return Float.NaN;
            }
            case 8: {
                return this.mFloatValue;
            }
            case 7: {
                return this.mFloatValue;
            }
            case 6: {
                return (float)this.mIntegerValue;
            }
            case 4:
            case 5: {
                throw new RuntimeException("Color does not have a single color to interpolate");
            }
            case 3: {
                throw new RuntimeException("Cannot interpolate String");
            }
            case 2: {
                float n;
                if (this.mBooleanValue) {
                    n = 1.0f;
                }
                else {
                    n = 0.0f;
                }
                return n;
            }
        }
    }
    
    public void getValuesToInterpolate(final float[] array) {
        switch (ConstraintAttribute$1.$SwitchMap$androidx$constraintlayout$widget$ConstraintAttribute$AttributeType[this.mType.ordinal()]) {
            case 8: {
                array[0] = this.mFloatValue;
                break;
            }
            case 7: {
                array[0] = this.mFloatValue;
                break;
            }
            case 6: {
                array[0] = (float)this.mIntegerValue;
                break;
            }
            case 4:
            case 5: {
                final int mColorValue = this.mColorValue;
                final float n = (float)Math.pow((mColorValue >> 16 & 0xFF) / 255.0f, 2.2);
                final float n2 = (float)Math.pow((mColorValue >> 8 & 0xFF) / 255.0f, 2.2);
                final float n3 = (float)Math.pow((mColorValue & 0xFF) / 255.0f, 2.2);
                array[0] = n;
                array[1] = n2;
                array[2] = n3;
                array[3] = (mColorValue >> 24 & 0xFF) / 255.0f;
                break;
            }
            case 3: {
                throw new RuntimeException("Color does not have a single color to interpolate");
            }
            case 2: {
                float n4;
                if (this.mBooleanValue) {
                    n4 = 1.0f;
                }
                else {
                    n4 = 0.0f;
                }
                array[0] = n4;
                break;
            }
        }
    }
    
    public boolean isBooleanValue() {
        return this.mBooleanValue;
    }
    
    public boolean isContinuous() {
        final int n = ConstraintAttribute$1.$SwitchMap$androidx$constraintlayout$widget$ConstraintAttribute$AttributeType[this.mType.ordinal()];
        return n != 1 && n != 2 && n != 3;
    }
    
    public boolean isMethod() {
        return this.mMethod;
    }
    
    public int numberOfInterpolatedValues() {
        final int n = ConstraintAttribute$1.$SwitchMap$androidx$constraintlayout$widget$ConstraintAttribute$AttributeType[this.mType.ordinal()];
        if (n != 4 && n != 5) {
            return 1;
        }
        return 4;
    }
    
    public void setColorValue(final int mColorValue) {
        this.mColorValue = mColorValue;
    }
    
    public void setFloatValue(final float mFloatValue) {
        this.mFloatValue = mFloatValue;
    }
    
    public void setIntValue(final int mIntegerValue) {
        this.mIntegerValue = mIntegerValue;
    }
    
    public void setStringValue(final String mStringValue) {
        this.mStringValue = mStringValue;
    }
    
    public void setValue(final Object o) {
        switch (ConstraintAttribute$1.$SwitchMap$androidx$constraintlayout$widget$ConstraintAttribute$AttributeType[this.mType.ordinal()]) {
            case 8: {
                this.mFloatValue = (float)o;
                break;
            }
            case 7: {
                this.mFloatValue = (float)o;
                break;
            }
            case 4:
            case 5: {
                this.mColorValue = (int)o;
                break;
            }
            case 3: {
                this.mStringValue = (String)o;
                break;
            }
            case 2: {
                this.mBooleanValue = (boolean)o;
                break;
            }
            case 1:
            case 6: {
                this.mIntegerValue = (int)o;
                break;
            }
        }
    }
    
    public void setValue(final float[] array) {
        final int n = ConstraintAttribute$1.$SwitchMap$androidx$constraintlayout$widget$ConstraintAttribute$AttributeType[this.mType.ordinal()];
        boolean mBooleanValue = false;
        switch (n) {
            case 8: {
                this.mFloatValue = array[0];
                break;
            }
            case 7: {
                this.mFloatValue = array[0];
                break;
            }
            case 4:
            case 5: {
                final int hsvToColor = Color.HSVToColor(array);
                this.mColorValue = hsvToColor;
                this.mColorValue = (clamp((int)(array[3] * 255.0f)) << 24 | (hsvToColor & 0xFFFFFF));
                break;
            }
            case 3: {
                throw new RuntimeException("Color does not have a single color to interpolate");
            }
            case 2: {
                if (array[0] > 0.5) {
                    mBooleanValue = true;
                }
                this.mBooleanValue = mBooleanValue;
                break;
            }
            case 1:
            case 6: {
                this.mIntegerValue = (int)array[0];
                break;
            }
        }
    }
    
    public enum AttributeType
    {
        private static final AttributeType[] $VALUES;
        
        BOOLEAN_TYPE, 
        COLOR_DRAWABLE_TYPE, 
        COLOR_TYPE, 
        DIMENSION_TYPE, 
        FLOAT_TYPE, 
        INT_TYPE, 
        REFERENCE_TYPE, 
        STRING_TYPE;
        
        private static /* synthetic */ AttributeType[] $values() {
            return new AttributeType[] { AttributeType.INT_TYPE, AttributeType.FLOAT_TYPE, AttributeType.COLOR_TYPE, AttributeType.COLOR_DRAWABLE_TYPE, AttributeType.STRING_TYPE, AttributeType.BOOLEAN_TYPE, AttributeType.DIMENSION_TYPE, AttributeType.REFERENCE_TYPE };
        }
        
        static {
            $VALUES = $values();
        }
    }
}

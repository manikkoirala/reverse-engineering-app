// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.widget;

import android.util.Log;
import java.util.ArrayList;
import java.util.Iterator;
import android.content.res.TypedArray;
import org.xmlpull.v1.XmlPullParserException;
import java.io.IOException;
import android.util.Xml;
import org.xmlpull.v1.XmlPullParser;
import android.content.Context;
import android.util.SparseArray;

public class StateSet
{
    private static final boolean DEBUG = false;
    public static final String TAG = "ConstraintLayoutStates";
    private SparseArray<ConstraintSet> mConstraintSetMap;
    private ConstraintsChangedListener mConstraintsChangedListener;
    int mCurrentConstraintNumber;
    int mCurrentStateId;
    ConstraintSet mDefaultConstraintSet;
    int mDefaultState;
    private SparseArray<State> mStateList;
    
    public StateSet(final Context context, final XmlPullParser xmlPullParser) {
        this.mDefaultState = -1;
        this.mCurrentStateId = -1;
        this.mCurrentConstraintNumber = -1;
        this.mStateList = (SparseArray<State>)new SparseArray();
        this.mConstraintSetMap = (SparseArray<ConstraintSet>)new SparseArray();
        this.mConstraintsChangedListener = null;
        this.load(context, xmlPullParser);
    }
    
    private void load(final Context context, final XmlPullParser xmlPullParser) {
        final TypedArray obtainStyledAttributes = context.obtainStyledAttributes(Xml.asAttributeSet(xmlPullParser), R.styleable.StateSet);
        for (int indexCount = obtainStyledAttributes.getIndexCount(), i = 0; i < indexCount; ++i) {
            final int index = obtainStyledAttributes.getIndex(i);
            if (index == R.styleable.StateSet_defaultState) {
                this.mDefaultState = obtainStyledAttributes.getResourceId(index, this.mDefaultState);
            }
        }
        obtainStyledAttributes.recycle();
        State state = null;
        try {
            int n = xmlPullParser.getEventType();
            while (true) {
                final int n2 = 1;
                if (n == 1) {
                    return;
                }
                State state2;
                if (n != 0) {
                    if (n != 2) {
                        if (n != 3) {
                            state2 = state;
                        }
                        else {
                            state2 = state;
                            if ("StateSet".equals(xmlPullParser.getName())) {
                                break;
                            }
                        }
                    }
                    else {
                        final String name = xmlPullParser.getName();
                        int n3 = 0;
                        Label_0250: {
                            switch (name.hashCode()) {
                                case 1901439077: {
                                    if (name.equals("Variant")) {
                                        n3 = 3;
                                        break Label_0250;
                                    }
                                    break;
                                }
                                case 1382829617: {
                                    if (name.equals("StateSet")) {
                                        n3 = n2;
                                        break Label_0250;
                                    }
                                    break;
                                }
                                case 1301459538: {
                                    if (name.equals("LayoutDescription")) {
                                        n3 = 0;
                                        break Label_0250;
                                    }
                                    break;
                                }
                                case 80204913: {
                                    if (name.equals("State")) {
                                        n3 = 2;
                                        break Label_0250;
                                    }
                                    break;
                                }
                            }
                            n3 = -1;
                        }
                        if (n3 != 2) {
                            if (n3 != 3) {
                                state2 = state;
                            }
                            else {
                                final Variant variant = new Variant(context, xmlPullParser);
                                if ((state2 = state) != null) {
                                    state.add(variant);
                                    state2 = state;
                                }
                            }
                        }
                        else {
                            state2 = new State(context, xmlPullParser);
                            this.mStateList.put(state2.mId, (Object)state2);
                        }
                    }
                }
                else {
                    xmlPullParser.getName();
                    state2 = state;
                }
                n = xmlPullParser.next();
                state = state2;
            }
        }
        catch (final IOException ex) {
            ex.printStackTrace();
        }
        catch (final XmlPullParserException ex2) {
            ex2.printStackTrace();
        }
    }
    
    public int convertToConstraintSet(final int n, final int n2, final float n3, final float n4) {
        final State state = (State)this.mStateList.get(n2);
        if (state == null) {
            return n2;
        }
        if (n3 != -1.0f && n4 != -1.0f) {
            Variant variant = null;
            for (final Variant variant2 : state.mVariants) {
                if (variant2.match(n3, n4)) {
                    if (n == variant2.mConstraintID) {
                        return n;
                    }
                    variant = variant2;
                }
            }
            if (variant != null) {
                return variant.mConstraintID;
            }
            return state.mConstraintID;
        }
        else {
            if (state.mConstraintID == n) {
                return n;
            }
            final Iterator<Variant> iterator2 = state.mVariants.iterator();
            while (iterator2.hasNext()) {
                if (n == ((Variant)iterator2.next()).mConstraintID) {
                    return n;
                }
            }
            return state.mConstraintID;
        }
    }
    
    public boolean needsToChange(final int n, final float n2, final float n3) {
        final int mCurrentStateId = this.mCurrentStateId;
        if (mCurrentStateId != n) {
            return true;
        }
        Object o;
        if (n == -1) {
            o = this.mStateList.valueAt(0);
        }
        else {
            o = this.mStateList.get(mCurrentStateId);
        }
        final State state = (State)o;
        return (this.mCurrentConstraintNumber == -1 || !((Variant)state.mVariants.get(this.mCurrentConstraintNumber)).match(n2, n3)) && this.mCurrentConstraintNumber != state.findMatch(n2, n3);
    }
    
    public void setOnConstraintsChanged(final ConstraintsChangedListener mConstraintsChangedListener) {
        this.mConstraintsChangedListener = mConstraintsChangedListener;
    }
    
    public int stateGetConstraintID(final int n, final int n2, final int n3) {
        return this.updateConstraints(-1, n, (float)n2, (float)n3);
    }
    
    public int updateConstraints(int n, int match, final float n2, final float n3) {
        if (n == match) {
            State state;
            if (match == -1) {
                state = (State)this.mStateList.valueAt(0);
            }
            else {
                state = (State)this.mStateList.get(this.mCurrentStateId);
            }
            if (state == null) {
                return -1;
            }
            if (this.mCurrentConstraintNumber != -1 && ((Variant)state.mVariants.get(n)).match(n2, n3)) {
                return n;
            }
            match = state.findMatch(n2, n3);
            if (n == match) {
                return n;
            }
            if (match == -1) {
                n = state.mConstraintID;
            }
            else {
                n = ((Variant)state.mVariants.get(match)).mConstraintID;
            }
            return n;
        }
        else {
            final State state2 = (State)this.mStateList.get(match);
            if (state2 == null) {
                return -1;
            }
            n = state2.findMatch(n2, n3);
            if (n == -1) {
                n = state2.mConstraintID;
            }
            else {
                n = ((Variant)state2.mVariants.get(n)).mConstraintID;
            }
            return n;
        }
    }
    
    static class State
    {
        int mConstraintID;
        int mId;
        boolean mIsLayout;
        ArrayList<Variant> mVariants;
        
        public State(final Context context, final XmlPullParser xmlPullParser) {
            this.mVariants = new ArrayList<Variant>();
            this.mConstraintID = -1;
            int i = 0;
            this.mIsLayout = false;
            TypedArray obtainStyledAttributes;
            for (obtainStyledAttributes = context.obtainStyledAttributes(Xml.asAttributeSet(xmlPullParser), R.styleable.State); i < obtainStyledAttributes.getIndexCount(); ++i) {
                final int index = obtainStyledAttributes.getIndex(i);
                if (index == R.styleable.State_android_id) {
                    this.mId = obtainStyledAttributes.getResourceId(index, this.mId);
                }
                else if (index == R.styleable.State_constraints) {
                    this.mConstraintID = obtainStyledAttributes.getResourceId(index, this.mConstraintID);
                    final String resourceTypeName = context.getResources().getResourceTypeName(this.mConstraintID);
                    context.getResources().getResourceName(this.mConstraintID);
                    if ("layout".equals(resourceTypeName)) {
                        this.mIsLayout = true;
                    }
                }
            }
            obtainStyledAttributes.recycle();
        }
        
        void add(final Variant e) {
            this.mVariants.add(e);
        }
        
        public int findMatch(final float n, final float n2) {
            for (int i = 0; i < this.mVariants.size(); ++i) {
                if (this.mVariants.get(i).match(n, n2)) {
                    return i;
                }
            }
            return -1;
        }
    }
    
    static class Variant
    {
        int mConstraintID;
        int mId;
        boolean mIsLayout;
        float mMaxHeight;
        float mMaxWidth;
        float mMinHeight;
        float mMinWidth;
        
        public Variant(final Context context, final XmlPullParser xmlPullParser) {
            this.mMinWidth = Float.NaN;
            this.mMinHeight = Float.NaN;
            this.mMaxWidth = Float.NaN;
            this.mMaxHeight = Float.NaN;
            this.mConstraintID = -1;
            int i = 0;
            this.mIsLayout = false;
            TypedArray obtainStyledAttributes;
            for (obtainStyledAttributes = context.obtainStyledAttributes(Xml.asAttributeSet(xmlPullParser), R.styleable.Variant); i < obtainStyledAttributes.getIndexCount(); ++i) {
                final int index = obtainStyledAttributes.getIndex(i);
                if (index == R.styleable.Variant_constraints) {
                    this.mConstraintID = obtainStyledAttributes.getResourceId(index, this.mConstraintID);
                    final String resourceTypeName = context.getResources().getResourceTypeName(this.mConstraintID);
                    context.getResources().getResourceName(this.mConstraintID);
                    if ("layout".equals(resourceTypeName)) {
                        this.mIsLayout = true;
                    }
                }
                else if (index == R.styleable.Variant_region_heightLessThan) {
                    this.mMaxHeight = obtainStyledAttributes.getDimension(index, this.mMaxHeight);
                }
                else if (index == R.styleable.Variant_region_heightMoreThan) {
                    this.mMinHeight = obtainStyledAttributes.getDimension(index, this.mMinHeight);
                }
                else if (index == R.styleable.Variant_region_widthLessThan) {
                    this.mMaxWidth = obtainStyledAttributes.getDimension(index, this.mMaxWidth);
                }
                else if (index == R.styleable.Variant_region_widthMoreThan) {
                    this.mMinWidth = obtainStyledAttributes.getDimension(index, this.mMinWidth);
                }
                else {
                    Log.v("ConstraintLayoutStates", "Unknown tag");
                }
            }
            obtainStyledAttributes.recycle();
        }
        
        boolean match(final float n, final float n2) {
            return (Float.isNaN(this.mMinWidth) || n >= this.mMinWidth) && (Float.isNaN(this.mMinHeight) || n2 >= this.mMinHeight) && (Float.isNaN(this.mMaxWidth) || n <= this.mMaxWidth) && (Float.isNaN(this.mMaxHeight) || n2 <= this.mMaxHeight);
        }
    }
}

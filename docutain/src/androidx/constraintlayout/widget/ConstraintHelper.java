// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.widget;

import androidx.constraintlayout.core.widgets.ConstraintWidgetContainer;
import android.graphics.Canvas;
import androidx.constraintlayout.core.widgets.ConstraintWidget;
import android.util.SparseArray;
import androidx.constraintlayout.core.widgets.HelperWidget;
import android.content.res.TypedArray;
import android.os.Build$VERSION;
import android.view.ViewParent;
import android.content.res.Resources;
import android.content.res.Resources$NotFoundException;
import android.view.ViewGroup$LayoutParams;
import java.util.Arrays;
import android.util.Log;
import android.util.AttributeSet;
import android.content.Context;
import java.util.HashMap;
import androidx.constraintlayout.core.widgets.Helper;
import android.view.View;

public abstract class ConstraintHelper extends View
{
    protected int mCount;
    protected Helper mHelperWidget;
    protected int[] mIds;
    protected HashMap<Integer, String> mMap;
    protected String mReferenceIds;
    protected String mReferenceTags;
    protected boolean mUseViewMeasure;
    private View[] mViews;
    protected Context myContext;
    
    public ConstraintHelper(final Context myContext) {
        super(myContext);
        this.mIds = new int[32];
        this.mUseViewMeasure = false;
        this.mViews = null;
        this.mMap = new HashMap<Integer, String>();
        this.myContext = myContext;
        this.init(null);
    }
    
    public ConstraintHelper(final Context myContext, final AttributeSet set) {
        super(myContext, set);
        this.mIds = new int[32];
        this.mUseViewMeasure = false;
        this.mViews = null;
        this.mMap = new HashMap<Integer, String>();
        this.myContext = myContext;
        this.init(set);
    }
    
    public ConstraintHelper(final Context myContext, final AttributeSet set, final int n) {
        super(myContext, set, n);
        this.mIds = new int[32];
        this.mUseViewMeasure = false;
        this.mViews = null;
        this.mMap = new HashMap<Integer, String>();
        this.myContext = myContext;
        this.init(set);
    }
    
    private void addID(String trim) {
        if (trim != null) {
            if (trim.length() != 0) {
                if (this.myContext == null) {
                    return;
                }
                trim = trim.trim();
                if (this.getParent() instanceof ConstraintLayout) {
                    final ConstraintLayout constraintLayout = (ConstraintLayout)this.getParent();
                }
                final int id = this.findId(trim);
                if (id != 0) {
                    this.mMap.put(id, trim);
                    this.addRscID(id);
                }
                else {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Could not find id of \"");
                    sb.append(trim);
                    sb.append("\"");
                    Log.w("ConstraintHelper", sb.toString());
                }
            }
        }
    }
    
    private void addRscID(final int n) {
        if (n == this.getId()) {
            return;
        }
        final int mCount = this.mCount;
        final int[] mIds = this.mIds;
        if (mCount + 1 > mIds.length) {
            this.mIds = Arrays.copyOf(mIds, mIds.length * 2);
        }
        final int[] mIds2 = this.mIds;
        final int mCount2 = this.mCount;
        mIds2[mCount2] = n;
        this.mCount = mCount2 + 1;
    }
    
    private void addTag(final String s) {
        if (s != null) {
            if (s.length() != 0) {
                if (this.myContext == null) {
                    return;
                }
                final String trim = s.trim();
                ConstraintLayout constraintLayout = null;
                if (this.getParent() instanceof ConstraintLayout) {
                    constraintLayout = (ConstraintLayout)this.getParent();
                }
                if (constraintLayout == null) {
                    Log.w("ConstraintHelper", "Parent not a ConstraintLayout");
                    return;
                }
                for (int childCount = constraintLayout.getChildCount(), i = 0; i < childCount; ++i) {
                    final View child = constraintLayout.getChildAt(i);
                    final ViewGroup$LayoutParams layoutParams = child.getLayoutParams();
                    if (layoutParams instanceof ConstraintLayout.LayoutParams && trim.equals(((ConstraintLayout.LayoutParams)layoutParams).constraintTag)) {
                        if (child.getId() == -1) {
                            final StringBuilder sb = new StringBuilder();
                            sb.append("to use ConstraintTag view ");
                            sb.append(child.getClass().getSimpleName());
                            sb.append(" must have an ID");
                            Log.w("ConstraintHelper", sb.toString());
                        }
                        else {
                            this.addRscID(child.getId());
                        }
                    }
                }
            }
        }
    }
    
    private int[] convertReferenceString(final View view, final String s) {
        final String[] split = s.split(",");
        view.getContext();
        final int[] original = new int[split.length];
        int i = 0;
        int newLength = 0;
        while (i < split.length) {
            final int id = this.findId(split[i].trim());
            int n = newLength;
            if (id != 0) {
                original[newLength] = id;
                n = newLength + 1;
            }
            ++i;
            newLength = n;
        }
        int[] copy = original;
        if (newLength != split.length) {
            copy = Arrays.copyOf(original, newLength);
        }
        return copy;
    }
    
    private int findId(final ConstraintLayout constraintLayout, final String s) {
        if (s != null) {
            if (constraintLayout != null) {
                final Resources resources = this.myContext.getResources();
                if (resources == null) {
                    return 0;
                }
                for (int childCount = constraintLayout.getChildCount(), i = 0; i < childCount; ++i) {
                    final View child = constraintLayout.getChildAt(i);
                    if (child.getId() != -1) {
                        Object resourceEntryName = null;
                        try {
                            resourceEntryName = resources.getResourceEntryName(child.getId());
                        }
                        catch (final Resources$NotFoundException ex) {}
                        if (s.equals(resourceEntryName)) {
                            return child.getId();
                        }
                    }
                }
            }
        }
        return 0;
    }
    
    private int findId(final String name) {
        ConstraintLayout constraintLayout;
        if (this.getParent() instanceof ConstraintLayout) {
            constraintLayout = (ConstraintLayout)this.getParent();
        }
        else {
            constraintLayout = null;
        }
        final boolean inEditMode = this.isInEditMode();
        int intValue;
        final int n = intValue = 0;
        if (inEditMode) {
            intValue = n;
            if (constraintLayout != null) {
                final Object designInformation = constraintLayout.getDesignInformation(0, name);
                intValue = n;
                if (designInformation instanceof Integer) {
                    intValue = (int)designInformation;
                }
            }
        }
        int id;
        if ((id = intValue) == 0) {
            id = intValue;
            if (constraintLayout != null) {
                id = this.findId(constraintLayout, name);
            }
        }
        int int1;
        if ((int1 = id) == 0) {
            try {
                int1 = R.id.class.getField(name).getInt(null);
            }
            catch (final Exception ex) {
                int1 = id;
            }
        }
        int identifier;
        if ((identifier = int1) == 0) {
            identifier = this.myContext.getResources().getIdentifier(name, "id", this.myContext.getPackageName());
        }
        return identifier;
    }
    
    public void addView(final View view) {
        if (view == this) {
            return;
        }
        if (view.getId() == -1) {
            Log.e("ConstraintHelper", "Views added to a ConstraintHelper need to have an id");
            return;
        }
        if (view.getParent() == null) {
            Log.e("ConstraintHelper", "Views added to a ConstraintHelper need to have a parent");
            return;
        }
        this.mReferenceIds = null;
        this.addRscID(view.getId());
        this.requestLayout();
    }
    
    protected void applyLayoutFeatures() {
        final ViewParent parent = this.getParent();
        if (parent != null && parent instanceof ConstraintLayout) {
            this.applyLayoutFeatures((ConstraintLayout)parent);
        }
    }
    
    protected void applyLayoutFeatures(final ConstraintLayout constraintLayout) {
        final int visibility = this.getVisibility();
        float elevation;
        if (Build$VERSION.SDK_INT >= 21) {
            elevation = this.getElevation();
        }
        else {
            elevation = 0.0f;
        }
        for (int i = 0; i < this.mCount; ++i) {
            final View viewById = constraintLayout.getViewById(this.mIds[i]);
            if (viewById != null) {
                viewById.setVisibility(visibility);
                if (elevation > 0.0f && Build$VERSION.SDK_INT >= 21) {
                    viewById.setTranslationZ(viewById.getTranslationZ() + elevation);
                }
            }
        }
    }
    
    protected void applyLayoutFeaturesInConstraintSet(final ConstraintLayout constraintLayout) {
    }
    
    public boolean containsId(final int n) {
        final int[] mIds = this.mIds;
        final int length = mIds.length;
        final boolean b = false;
        int n2 = 0;
        boolean b2;
        while (true) {
            b2 = b;
            if (n2 >= length) {
                break;
            }
            if (mIds[n2] == n) {
                b2 = true;
                break;
            }
            ++n2;
        }
        return b2;
    }
    
    public int[] getReferencedIds() {
        return Arrays.copyOf(this.mIds, this.mCount);
    }
    
    protected View[] getViews(final ConstraintLayout constraintLayout) {
        final View[] mViews = this.mViews;
        if (mViews == null || mViews.length != this.mCount) {
            this.mViews = new View[this.mCount];
        }
        for (int i = 0; i < this.mCount; ++i) {
            this.mViews[i] = constraintLayout.getViewById(this.mIds[i]);
        }
        return this.mViews;
    }
    
    public int indexFromId(final int n) {
        final int[] mIds = this.mIds;
        final int length = mIds.length;
        int n2 = -1;
        for (final int n3 : mIds) {
            ++n2;
            if (n3 == n) {
                return n2;
            }
        }
        return n2;
    }
    
    protected void init(final AttributeSet set) {
        if (set != null) {
            final TypedArray obtainStyledAttributes = this.getContext().obtainStyledAttributes(set, R.styleable.ConstraintLayout_Layout);
            for (int indexCount = obtainStyledAttributes.getIndexCount(), i = 0; i < indexCount; ++i) {
                final int index = obtainStyledAttributes.getIndex(i);
                if (index == R.styleable.ConstraintLayout_Layout_constraint_referenced_ids) {
                    this.setIds(this.mReferenceIds = obtainStyledAttributes.getString(index));
                }
                else if (index == R.styleable.ConstraintLayout_Layout_constraint_referenced_tags) {
                    this.setReferenceTags(this.mReferenceTags = obtainStyledAttributes.getString(index));
                }
            }
            obtainStyledAttributes.recycle();
        }
    }
    
    public void loadParameters(final ConstraintSet.Constraint constraint, final HelperWidget helperWidget, final ConstraintLayout.LayoutParams layoutParams, final SparseArray<ConstraintWidget> sparseArray) {
        if (constraint.layout.mReferenceIds != null) {
            this.setReferencedIds(constraint.layout.mReferenceIds);
        }
        else if (constraint.layout.mReferenceIdString != null) {
            if (constraint.layout.mReferenceIdString.length() > 0) {
                constraint.layout.mReferenceIds = this.convertReferenceString(this, constraint.layout.mReferenceIdString);
            }
            else {
                constraint.layout.mReferenceIds = null;
            }
        }
        if (helperWidget != null) {
            helperWidget.removeAllIds();
            if (constraint.layout.mReferenceIds != null) {
                for (int i = 0; i < constraint.layout.mReferenceIds.length; ++i) {
                    final ConstraintWidget constraintWidget = (ConstraintWidget)sparseArray.get(constraint.layout.mReferenceIds[i]);
                    if (constraintWidget != null) {
                        helperWidget.add(constraintWidget);
                    }
                }
            }
        }
    }
    
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        final String mReferenceIds = this.mReferenceIds;
        if (mReferenceIds != null) {
            this.setIds(mReferenceIds);
        }
        final String mReferenceTags = this.mReferenceTags;
        if (mReferenceTags != null) {
            this.setReferenceTags(mReferenceTags);
        }
    }
    
    public void onDraw(final Canvas canvas) {
    }
    
    protected void onMeasure(final int n, final int n2) {
        if (this.mUseViewMeasure) {
            super.onMeasure(n, n2);
        }
        else {
            this.setMeasuredDimension(0, 0);
        }
    }
    
    public int removeView(final View view) {
        final int id = view.getId();
        final int n = -1;
        if (id == -1) {
            return -1;
        }
        this.mReferenceIds = null;
        int n2 = 0;
        int n3;
        while (true) {
            n3 = n;
            if (n2 >= this.mCount) {
                break;
            }
            if (this.mIds[n2] == id) {
                int n4 = n2;
                int mCount;
                while (true) {
                    mCount = this.mCount;
                    if (n4 >= mCount - 1) {
                        break;
                    }
                    final int[] mIds = this.mIds;
                    final int n5 = n4 + 1;
                    mIds[n4] = mIds[n5];
                    n4 = n5;
                }
                this.mIds[mCount - 1] = 0;
                this.mCount = mCount - 1;
                n3 = n2;
                break;
            }
            ++n2;
        }
        this.requestLayout();
        return n3;
    }
    
    public void resolveRtl(final ConstraintWidget constraintWidget, final boolean b) {
    }
    
    protected void setIds(final String mReferenceIds) {
        this.mReferenceIds = mReferenceIds;
        if (mReferenceIds == null) {
            return;
        }
        int beginIndex = 0;
        this.mCount = 0;
        while (true) {
            final int index = mReferenceIds.indexOf(44, beginIndex);
            if (index == -1) {
                break;
            }
            this.addID(mReferenceIds.substring(beginIndex, index));
            beginIndex = index + 1;
        }
        this.addID(mReferenceIds.substring(beginIndex));
    }
    
    protected void setReferenceTags(final String mReferenceTags) {
        this.mReferenceTags = mReferenceTags;
        if (mReferenceTags == null) {
            return;
        }
        int beginIndex = 0;
        this.mCount = 0;
        while (true) {
            final int index = mReferenceTags.indexOf(44, beginIndex);
            if (index == -1) {
                break;
            }
            this.addTag(mReferenceTags.substring(beginIndex, index));
            beginIndex = index + 1;
        }
        this.addTag(mReferenceTags.substring(beginIndex));
    }
    
    public void setReferencedIds(final int[] array) {
        this.mReferenceIds = null;
        int i = 0;
        this.mCount = 0;
        while (i < array.length) {
            this.addRscID(array[i]);
            ++i;
        }
    }
    
    public void setTag(final int n, final Object o) {
        super.setTag(n, o);
        if (o == null && this.mReferenceIds == null) {
            this.addRscID(n);
        }
    }
    
    public void updatePostConstraints(final ConstraintLayout constraintLayout) {
    }
    
    public void updatePostLayout(final ConstraintLayout constraintLayout) {
    }
    
    public void updatePostMeasure(final ConstraintLayout constraintLayout) {
    }
    
    public void updatePreDraw(final ConstraintLayout constraintLayout) {
    }
    
    public void updatePreLayout(final ConstraintWidgetContainer constraintWidgetContainer, final Helper helper, final SparseArray<ConstraintWidget> sparseArray) {
        helper.removeAllIds();
        for (int i = 0; i < this.mCount; ++i) {
            helper.add((ConstraintWidget)sparseArray.get(this.mIds[i]));
        }
    }
    
    public void updatePreLayout(final ConstraintLayout constraintLayout) {
        if (this.isInEditMode()) {
            this.setIds(this.mReferenceIds);
        }
        final Helper mHelperWidget = this.mHelperWidget;
        if (mHelperWidget == null) {
            return;
        }
        mHelperWidget.removeAllIds();
        for (int i = 0; i < this.mCount; ++i) {
            final int j = this.mIds[i];
            final View viewById = constraintLayout.getViewById(j);
            View viewById2;
            if ((viewById2 = viewById) == null) {
                final String value = this.mMap.get(j);
                final int id = this.findId(constraintLayout, value);
                viewById2 = viewById;
                if (id != 0) {
                    this.mIds[i] = id;
                    this.mMap.put(id, value);
                    viewById2 = constraintLayout.getViewById(id);
                }
            }
            if (viewById2 != null) {
                this.mHelperWidget.add(constraintLayout.getViewWidget(viewById2));
            }
        }
        this.mHelperWidget.updateConstraints(constraintLayout.mLayoutWidget);
    }
    
    public void validateParams() {
        if (this.mHelperWidget == null) {
            return;
        }
        final ViewGroup$LayoutParams layoutParams = this.getLayoutParams();
        if (layoutParams instanceof ConstraintLayout.LayoutParams) {
            ((ConstraintLayout.LayoutParams)layoutParams).widget = (ConstraintWidget)this.mHelperWidget;
        }
    }
}

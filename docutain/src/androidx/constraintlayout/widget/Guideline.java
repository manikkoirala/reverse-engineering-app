// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.widget;

import android.view.ViewGroup$LayoutParams;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.content.Context;
import android.view.View;

public class Guideline extends View
{
    private boolean mFilterRedundantCalls;
    
    public Guideline(final Context context) {
        super(context);
        this.mFilterRedundantCalls = true;
        super.setVisibility(8);
    }
    
    public Guideline(final Context context, final AttributeSet set) {
        super(context, set);
        this.mFilterRedundantCalls = true;
        super.setVisibility(8);
    }
    
    public Guideline(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
        this.mFilterRedundantCalls = true;
        super.setVisibility(8);
    }
    
    public Guideline(final Context context, final AttributeSet set, final int n, final int n2) {
        super(context, set, n);
        this.mFilterRedundantCalls = true;
        super.setVisibility(8);
    }
    
    public void draw(final Canvas canvas) {
    }
    
    protected void onMeasure(final int n, final int n2) {
        this.setMeasuredDimension(0, 0);
    }
    
    public void setFilterRedundantCalls(final boolean mFilterRedundantCalls) {
        this.mFilterRedundantCalls = mFilterRedundantCalls;
    }
    
    public void setGuidelineBegin(final int guideBegin) {
        final ConstraintLayout.LayoutParams layoutParams = (ConstraintLayout.LayoutParams)this.getLayoutParams();
        if (this.mFilterRedundantCalls && layoutParams.guideBegin == guideBegin) {
            return;
        }
        layoutParams.guideBegin = guideBegin;
        this.setLayoutParams((ViewGroup$LayoutParams)layoutParams);
    }
    
    public void setGuidelineEnd(final int guideEnd) {
        final ConstraintLayout.LayoutParams layoutParams = (ConstraintLayout.LayoutParams)this.getLayoutParams();
        if (this.mFilterRedundantCalls && layoutParams.guideEnd == guideEnd) {
            return;
        }
        layoutParams.guideEnd = guideEnd;
        this.setLayoutParams((ViewGroup$LayoutParams)layoutParams);
    }
    
    public void setGuidelinePercent(final float guidePercent) {
        final ConstraintLayout.LayoutParams layoutParams = (ConstraintLayout.LayoutParams)this.getLayoutParams();
        if (this.mFilterRedundantCalls && layoutParams.guidePercent == guidePercent) {
            return;
        }
        layoutParams.guidePercent = guidePercent;
        this.setLayoutParams((ViewGroup$LayoutParams)layoutParams);
    }
    
    public void setVisibility(final int n) {
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.widget;

import androidx.constraintlayout.core.widgets.Optimizer;
import android.util.SparseIntArray;
import android.content.res.TypedArray;
import android.view.ViewGroup$MarginLayoutParams;
import android.view.View$MeasureSpec;
import java.util.Iterator;
import android.util.Log;
import android.graphics.Paint;
import android.graphics.Canvas;
import android.view.ViewGroup$LayoutParams;
import androidx.constraintlayout.core.widgets.Guideline;
import androidx.constraintlayout.core.widgets.ConstraintAnchor;
import android.content.res.Resources$NotFoundException;
import androidx.constraintlayout.core.widgets.analyzer.BasicMeasure;
import android.os.Build$VERSION;
import android.util.AttributeSet;
import android.content.Context;
import androidx.constraintlayout.core.widgets.ConstraintWidget;
import androidx.constraintlayout.core.Metrics;
import androidx.constraintlayout.core.widgets.ConstraintWidgetContainer;
import java.util.HashMap;
import java.util.ArrayList;
import android.view.View;
import android.util.SparseArray;
import android.view.ViewGroup;

public class ConstraintLayout extends ViewGroup
{
    private static final boolean DEBUG = false;
    private static final boolean DEBUG_DRAW_CONSTRAINTS = false;
    public static final int DESIGN_INFO_ID = 0;
    private static final boolean MEASURE = false;
    private static final boolean OPTIMIZE_HEIGHT_CHANGE = false;
    private static final String TAG = "ConstraintLayout";
    private static final boolean USE_CONSTRAINTS_HELPER = true;
    public static final String VERSION = "ConstraintLayout-2.1.4";
    private static SharedValues sSharedValues;
    SparseArray<View> mChildrenByIds;
    private ArrayList<ConstraintHelper> mConstraintHelpers;
    protected ConstraintLayoutStates mConstraintLayoutSpec;
    private ConstraintSet mConstraintSet;
    private int mConstraintSetId;
    private ConstraintsChangedListener mConstraintsChangedListener;
    private HashMap<String, Integer> mDesignIds;
    protected boolean mDirtyHierarchy;
    private int mLastMeasureHeight;
    int mLastMeasureHeightMode;
    int mLastMeasureHeightSize;
    private int mLastMeasureWidth;
    int mLastMeasureWidthMode;
    int mLastMeasureWidthSize;
    protected ConstraintWidgetContainer mLayoutWidget;
    private int mMaxHeight;
    private int mMaxWidth;
    Measurer mMeasurer;
    private Metrics mMetrics;
    private int mMinHeight;
    private int mMinWidth;
    private int mOnMeasureHeightMeasureSpec;
    private int mOnMeasureWidthMeasureSpec;
    private int mOptimizationLevel;
    private SparseArray<ConstraintWidget> mTempMapIdToWidget;
    
    public ConstraintLayout(final Context context) {
        super(context);
        this.mChildrenByIds = (SparseArray<View>)new SparseArray();
        this.mConstraintHelpers = new ArrayList<ConstraintHelper>(4);
        this.mLayoutWidget = new ConstraintWidgetContainer();
        this.mMinWidth = 0;
        this.mMinHeight = 0;
        this.mMaxWidth = Integer.MAX_VALUE;
        this.mMaxHeight = Integer.MAX_VALUE;
        this.mDirtyHierarchy = true;
        this.mOptimizationLevel = 257;
        this.mConstraintSet = null;
        this.mConstraintLayoutSpec = null;
        this.mConstraintSetId = -1;
        this.mDesignIds = new HashMap<String, Integer>();
        this.mLastMeasureWidth = -1;
        this.mLastMeasureHeight = -1;
        this.mLastMeasureWidthSize = -1;
        this.mLastMeasureHeightSize = -1;
        this.mLastMeasureWidthMode = 0;
        this.mLastMeasureHeightMode = 0;
        this.mTempMapIdToWidget = (SparseArray<ConstraintWidget>)new SparseArray();
        this.mMeasurer = new Measurer(this);
        this.mOnMeasureWidthMeasureSpec = 0;
        this.init(null, this.mOnMeasureHeightMeasureSpec = 0, 0);
    }
    
    public ConstraintLayout(final Context context, final AttributeSet set) {
        super(context, set);
        this.mChildrenByIds = (SparseArray<View>)new SparseArray();
        this.mConstraintHelpers = new ArrayList<ConstraintHelper>(4);
        this.mLayoutWidget = new ConstraintWidgetContainer();
        this.mMinWidth = 0;
        this.mMinHeight = 0;
        this.mMaxWidth = Integer.MAX_VALUE;
        this.mMaxHeight = Integer.MAX_VALUE;
        this.mDirtyHierarchy = true;
        this.mOptimizationLevel = 257;
        this.mConstraintSet = null;
        this.mConstraintLayoutSpec = null;
        this.mConstraintSetId = -1;
        this.mDesignIds = new HashMap<String, Integer>();
        this.mLastMeasureWidth = -1;
        this.mLastMeasureHeight = -1;
        this.mLastMeasureWidthSize = -1;
        this.mLastMeasureHeightSize = -1;
        this.mLastMeasureWidthMode = 0;
        this.mLastMeasureHeightMode = 0;
        this.mTempMapIdToWidget = (SparseArray<ConstraintWidget>)new SparseArray();
        this.mMeasurer = new Measurer(this);
        this.mOnMeasureWidthMeasureSpec = 0;
        this.init(set, this.mOnMeasureHeightMeasureSpec = 0, 0);
    }
    
    public ConstraintLayout(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
        this.mChildrenByIds = (SparseArray<View>)new SparseArray();
        this.mConstraintHelpers = new ArrayList<ConstraintHelper>(4);
        this.mLayoutWidget = new ConstraintWidgetContainer();
        this.mMinWidth = 0;
        this.mMinHeight = 0;
        this.mMaxWidth = Integer.MAX_VALUE;
        this.mMaxHeight = Integer.MAX_VALUE;
        this.mDirtyHierarchy = true;
        this.mOptimizationLevel = 257;
        this.mConstraintSet = null;
        this.mConstraintLayoutSpec = null;
        this.mConstraintSetId = -1;
        this.mDesignIds = new HashMap<String, Integer>();
        this.mLastMeasureWidth = -1;
        this.mLastMeasureHeight = -1;
        this.mLastMeasureWidthSize = -1;
        this.mLastMeasureHeightSize = -1;
        this.mLastMeasureWidthMode = 0;
        this.mLastMeasureHeightMode = 0;
        this.mTempMapIdToWidget = (SparseArray<ConstraintWidget>)new SparseArray();
        this.mMeasurer = new Measurer(this);
        this.mOnMeasureWidthMeasureSpec = 0;
        this.init(set, n, this.mOnMeasureHeightMeasureSpec = 0);
    }
    
    public ConstraintLayout(final Context context, final AttributeSet set, final int n, final int n2) {
        super(context, set, n, n2);
        this.mChildrenByIds = (SparseArray<View>)new SparseArray();
        this.mConstraintHelpers = new ArrayList<ConstraintHelper>(4);
        this.mLayoutWidget = new ConstraintWidgetContainer();
        this.mMinWidth = 0;
        this.mMinHeight = 0;
        this.mMaxWidth = Integer.MAX_VALUE;
        this.mMaxHeight = Integer.MAX_VALUE;
        this.mDirtyHierarchy = true;
        this.mOptimizationLevel = 257;
        this.mConstraintSet = null;
        this.mConstraintLayoutSpec = null;
        this.mConstraintSetId = -1;
        this.mDesignIds = new HashMap<String, Integer>();
        this.mLastMeasureWidth = -1;
        this.mLastMeasureHeight = -1;
        this.mLastMeasureWidthSize = -1;
        this.mLastMeasureHeightSize = -1;
        this.mLastMeasureWidthMode = 0;
        this.mLastMeasureHeightMode = 0;
        this.mTempMapIdToWidget = (SparseArray<ConstraintWidget>)new SparseArray();
        this.mMeasurer = new Measurer(this);
        this.mOnMeasureWidthMeasureSpec = 0;
        this.mOnMeasureHeightMeasureSpec = 0;
        this.init(set, n, n2);
    }
    
    private int getPaddingWidth() {
        final int paddingLeft = this.getPaddingLeft();
        int n = 0;
        int n2 = Math.max(0, paddingLeft) + Math.max(0, this.getPaddingRight());
        if (Build$VERSION.SDK_INT >= 17) {
            n = Math.max(0, this.getPaddingEnd()) + Math.max(0, this.getPaddingStart());
        }
        if (n > 0) {
            n2 = n;
        }
        return n2;
    }
    
    public static SharedValues getSharedValues() {
        if (ConstraintLayout.sSharedValues == null) {
            ConstraintLayout.sSharedValues = new SharedValues();
        }
        return ConstraintLayout.sSharedValues;
    }
    
    private final ConstraintWidget getTargetWidget(final int n) {
        if (n == 0) {
            return this.mLayoutWidget;
        }
        View view;
        if ((view = (View)this.mChildrenByIds.get(n)) == null) {
            final View viewById = this.findViewById(n);
            if ((view = viewById) != null && (view = viewById) != this) {
                view = viewById;
                if (viewById.getParent() == this) {
                    this.onViewAdded(viewById);
                    view = viewById;
                }
            }
        }
        if (view == this) {
            return this.mLayoutWidget;
        }
        ConstraintWidget widget;
        if (view == null) {
            widget = null;
        }
        else {
            widget = ((LayoutParams)view.getLayoutParams()).widget;
        }
        return widget;
    }
    
    private void init(AttributeSet obtainStyledAttributes, int i, int indexCount) {
        this.mLayoutWidget.setCompanionWidget(this);
        this.mLayoutWidget.setMeasurer(this.mMeasurer);
        this.mChildrenByIds.put(this.getId(), (Object)this);
        this.mConstraintSet = null;
        if (obtainStyledAttributes != null) {
            obtainStyledAttributes = (AttributeSet)this.getContext().obtainStyledAttributes(obtainStyledAttributes, R.styleable.ConstraintLayout_Layout, i, indexCount);
            int index;
            int resourceId;
            int resourceId2;
            for (indexCount = ((TypedArray)obtainStyledAttributes).getIndexCount(), i = 0; i < indexCount; ++i) {
                index = ((TypedArray)obtainStyledAttributes).getIndex(i);
                if (index == R.styleable.ConstraintLayout_Layout_android_minWidth) {
                    this.mMinWidth = ((TypedArray)obtainStyledAttributes).getDimensionPixelOffset(index, this.mMinWidth);
                }
                else if (index == R.styleable.ConstraintLayout_Layout_android_minHeight) {
                    this.mMinHeight = ((TypedArray)obtainStyledAttributes).getDimensionPixelOffset(index, this.mMinHeight);
                }
                else if (index == R.styleable.ConstraintLayout_Layout_android_maxWidth) {
                    this.mMaxWidth = ((TypedArray)obtainStyledAttributes).getDimensionPixelOffset(index, this.mMaxWidth);
                }
                else if (index == R.styleable.ConstraintLayout_Layout_android_maxHeight) {
                    this.mMaxHeight = ((TypedArray)obtainStyledAttributes).getDimensionPixelOffset(index, this.mMaxHeight);
                }
                else if (index == R.styleable.ConstraintLayout_Layout_layout_optimizationLevel) {
                    this.mOptimizationLevel = ((TypedArray)obtainStyledAttributes).getInt(index, this.mOptimizationLevel);
                }
                else if (index == R.styleable.ConstraintLayout_Layout_layoutDescription) {
                    resourceId = ((TypedArray)obtainStyledAttributes).getResourceId(index, 0);
                    if (resourceId != 0) {
                        try {
                            this.parseLayoutDescription(resourceId);
                        }
                        catch (final Resources$NotFoundException ex) {
                            this.mConstraintLayoutSpec = null;
                        }
                    }
                }
                else if (index == R.styleable.ConstraintLayout_Layout_constraintSet) {
                    resourceId2 = ((TypedArray)obtainStyledAttributes).getResourceId(index, 0);
                    try {
                        (this.mConstraintSet = new ConstraintSet()).load(this.getContext(), resourceId2);
                    }
                    catch (final Resources$NotFoundException ex2) {
                        this.mConstraintSet = null;
                    }
                    this.mConstraintSetId = resourceId2;
                }
            }
            ((TypedArray)obtainStyledAttributes).recycle();
        }
        this.mLayoutWidget.setOptimizationLevel(this.mOptimizationLevel);
    }
    
    private void markHierarchyDirty() {
        this.mDirtyHierarchy = true;
        this.mLastMeasureWidth = -1;
        this.mLastMeasureHeight = -1;
        this.mLastMeasureWidthSize = -1;
        this.mLastMeasureHeightSize = -1;
        this.mLastMeasureWidthMode = 0;
        this.mLastMeasureHeightMode = 0;
    }
    
    private void setChildrenConstraints() {
        final boolean inEditMode = this.isInEditMode();
        final int childCount = this.getChildCount();
        for (int i = 0; i < childCount; ++i) {
            final ConstraintWidget viewWidget = this.getViewWidget(this.getChildAt(i));
            if (viewWidget != null) {
                viewWidget.reset();
            }
        }
        Label_0143: {
            if (!inEditMode) {
                break Label_0143;
            }
            int i = 0;
        Label_0137_Outer:
            while (true) {
                if (i >= childCount) {
                    break Label_0143;
                }
                final View child = this.getChildAt(i);
                while (true) {
                    try {
                        final String resourceName = this.getResources().getResourceName(child.getId());
                        this.setDesignInformation(0, resourceName, child.getId());
                        final int index = resourceName.indexOf(47);
                        String substring = resourceName;
                        if (index != -1) {
                            substring = resourceName.substring(index + 1);
                        }
                        this.getTargetWidget(child.getId()).setDebugName(substring);
                        ++i;
                        continue Label_0137_Outer;
                        View child2 = null;
                        View child3;
                        View child4;
                        ConstraintSet mConstraintSet;
                        View child5;
                        ConstraintWidget viewWidget2;
                        int size = 0;
                        LayoutParams layoutParams;
                        Block_17_Outer:Block_13_Outer:Label_0153_Outer:
                        while (true) {
                            this.mConstraintSet = ((Constraints)child2).getConstraintSet();
                            Label_0153:Block_18_Outer:Block_19_Outer:
                            while (true) {
                            Block_19:
                                while (true) {
                                    while (true) {
                                        Label_0383: {
                                            while (true) {
                                                Label_0221: {
                                                    while (true) {
                                                        Label_0242: {
                                                            while (true) {
                                                            Label_0270:
                                                                while (true) {
                                                                    while (true) {
                                                                        Label_0197: {
                                                                            break Label_0197;
                                                                        Label_0299:
                                                                            while (true) {
                                                                                ((Placeholder)child3).updatePreLayout(this);
                                                                                break Label_0299;
                                                                                Label_0305: {
                                                                                    this.mTempMapIdToWidget.clear();
                                                                                }
                                                                                this.mTempMapIdToWidget.put(0, (Object)this.mLayoutWidget);
                                                                                this.mTempMapIdToWidget.put(this.getId(), (Object)this.mLayoutWidget);
                                                                                i = 0;
                                                                                break Label_0270;
                                                                                child4 = this.getChildAt(i);
                                                                                this.mTempMapIdToWidget.put(child4.getId(), (Object)this.getViewWidget(child4));
                                                                                ++i;
                                                                                break Label_0270;
                                                                                mConstraintSet.applyToInternal(this, true);
                                                                                break Label_0221;
                                                                                i = 0;
                                                                                break Label_0242;
                                                                                iftrue(Label_0305:)(i >= childCount);
                                                                                child3 = this.getChildAt(i);
                                                                                iftrue(Label_0299:)(!(child3 instanceof Placeholder));
                                                                                continue Block_17_Outer;
                                                                            }
                                                                            ++i;
                                                                            continue Label_0270;
                                                                            iftrue(Label_0203:)(i >= childCount);
                                                                            break Label_0153;
                                                                        }
                                                                        ++i;
                                                                        continue Label_0153;
                                                                        this.mConstraintHelpers.get(i).updatePreLayout(this);
                                                                        ++i;
                                                                        break Label_0242;
                                                                        Label_0203: {
                                                                            mConstraintSet = this.mConstraintSet;
                                                                        }
                                                                        iftrue(Label_0221:)(mConstraintSet == null);
                                                                        continue Block_13_Outer;
                                                                    }
                                                                    child5 = this.getChildAt(i);
                                                                    viewWidget2 = this.getViewWidget(child5);
                                                                    iftrue(Label_0411:)(viewWidget2 != null);
                                                                    break Block_19;
                                                                    Label_0381: {
                                                                        i = 0;
                                                                    }
                                                                    break Label_0383;
                                                                    Label_0268:
                                                                    i = 0;
                                                                    continue Label_0270;
                                                                }
                                                                iftrue(Label_0381:)(i >= childCount);
                                                                continue Block_13_Outer;
                                                            }
                                                            ++i;
                                                            break Label_0383;
                                                        }
                                                        iftrue(Label_0268:)(i >= size);
                                                        continue Block_18_Outer;
                                                    }
                                                }
                                                this.mLayoutWidget.removeAllChildren();
                                                size = this.mConstraintHelpers.size();
                                                iftrue(Label_0268:)(size <= 0);
                                                continue Label_0153_Outer;
                                            }
                                            iftrue(Label_0203:)(this.mConstraintSetId == -1);
                                            break Block_19;
                                        }
                                        iftrue(Label_0452:)(i >= childCount);
                                        continue Block_19_Outer;
                                    }
                                    Label_0411: {
                                        layoutParams = (LayoutParams)child5.getLayoutParams();
                                    }
                                    this.mLayoutWidget.add(viewWidget2);
                                    this.applyConstraintsFromLayoutParams(inEditMode, child5, viewWidget2, layoutParams, this.mTempMapIdToWidget);
                                    continue Block_19;
                                }
                                i = 0;
                                continue Label_0153;
                            }
                            child2 = this.getChildAt(i);
                            iftrue(Label_0197:)(child2.getId() != this.mConstraintSetId || !(child2 instanceof Constraints));
                            continue Block_17_Outer;
                        }
                        Label_0452:;
                    }
                    catch (final Resources$NotFoundException ex) {
                        continue;
                    }
                    break;
                }
                break;
            }
        }
    }
    
    private void setWidgetBaseline(final ConstraintWidget constraintWidget, final LayoutParams layoutParams, final SparseArray<ConstraintWidget> sparseArray, final int n, final ConstraintAnchor.Type type) {
        final View view = (View)this.mChildrenByIds.get(n);
        final ConstraintWidget constraintWidget2 = (ConstraintWidget)sparseArray.get(n);
        if (constraintWidget2 != null && view != null && view.getLayoutParams() instanceof LayoutParams) {
            layoutParams.needsBaseline = true;
            if (type == ConstraintAnchor.Type.BASELINE) {
                final LayoutParams layoutParams2 = (LayoutParams)view.getLayoutParams();
                layoutParams2.needsBaseline = true;
                layoutParams2.widget.setHasBaseline(true);
            }
            constraintWidget.getAnchor(ConstraintAnchor.Type.BASELINE).connect(constraintWidget2.getAnchor(type), layoutParams.baselineMargin, layoutParams.goneBaselineMargin, true);
            constraintWidget.setHasBaseline(true);
            constraintWidget.getAnchor(ConstraintAnchor.Type.TOP).reset();
            constraintWidget.getAnchor(ConstraintAnchor.Type.BOTTOM).reset();
        }
    }
    
    private boolean updateHierarchy() {
        final int childCount = this.getChildCount();
        final boolean b = false;
        int n = 0;
        boolean b2;
        while (true) {
            b2 = b;
            if (n >= childCount) {
                break;
            }
            if (this.getChildAt(n).isLayoutRequested()) {
                b2 = true;
                break;
            }
            ++n;
        }
        if (b2) {
            this.setChildrenConstraints();
        }
        return b2;
    }
    
    protected void applyConstraintsFromLayoutParams(final boolean b, final View companionWidget, final ConstraintWidget constraintWidget, final LayoutParams layoutParams, final SparseArray<ConstraintWidget> sparseArray) {
        layoutParams.validate();
        layoutParams.helped = false;
        constraintWidget.setVisibility(companionWidget.getVisibility());
        if (layoutParams.isInPlaceholder) {
            constraintWidget.setInPlaceholder(true);
            constraintWidget.setVisibility(8);
        }
        constraintWidget.setCompanionWidget(companionWidget);
        if (companionWidget instanceof ConstraintHelper) {
            ((ConstraintHelper)companionWidget).resolveRtl(constraintWidget, this.mLayoutWidget.isRtl());
        }
        if (layoutParams.isGuideline) {
            final Guideline guideline = (Guideline)constraintWidget;
            int guideBegin = layoutParams.resolvedGuideBegin;
            int guideEnd = layoutParams.resolvedGuideEnd;
            float guidePercent = layoutParams.resolvedGuidePercent;
            if (Build$VERSION.SDK_INT < 17) {
                guideBegin = layoutParams.guideBegin;
                guideEnd = layoutParams.guideEnd;
                guidePercent = layoutParams.guidePercent;
            }
            if (guidePercent != -1.0f) {
                guideline.setGuidePercent(guidePercent);
            }
            else if (guideBegin != -1) {
                guideline.setGuideBegin(guideBegin);
            }
            else if (guideEnd != -1) {
                guideline.setGuideEnd(guideEnd);
            }
        }
        else {
            int resolvedLeftToLeft = layoutParams.resolvedLeftToLeft;
            int n = layoutParams.resolvedLeftToRight;
            int resolvedRightToLeft = layoutParams.resolvedRightToLeft;
            int n2 = layoutParams.resolvedRightToRight;
            int resolveGoneLeftMargin = layoutParams.resolveGoneLeftMargin;
            int resolveGoneRightMargin = layoutParams.resolveGoneRightMargin;
            float horizontalBiasPercent = layoutParams.resolvedHorizontalBias;
            if (Build$VERSION.SDK_INT < 17) {
                final int leftToLeft = layoutParams.leftToLeft;
                final int leftToRight = layoutParams.leftToRight;
                final int rightToLeft = layoutParams.rightToLeft;
                final int rightToRight = layoutParams.rightToRight;
                final int goneLeftMargin = layoutParams.goneLeftMargin;
                final int goneRightMargin = layoutParams.goneRightMargin;
                horizontalBiasPercent = layoutParams.horizontalBias;
                int startToStart = leftToLeft;
                n = leftToRight;
                if (leftToLeft == -1) {
                    startToStart = leftToLeft;
                    if ((n = leftToRight) == -1) {
                        if (layoutParams.startToStart != -1) {
                            startToStart = layoutParams.startToStart;
                            n = leftToRight;
                        }
                        else {
                            startToStart = leftToLeft;
                            n = leftToRight;
                            if (layoutParams.startToEnd != -1) {
                                n = layoutParams.startToEnd;
                                startToStart = leftToLeft;
                            }
                        }
                    }
                }
                int endToStart = rightToLeft;
                n2 = rightToRight;
                if (rightToLeft == -1) {
                    endToStart = rightToLeft;
                    if ((n2 = rightToRight) == -1) {
                        if (layoutParams.endToStart != -1) {
                            endToStart = layoutParams.endToStart;
                            n2 = rightToRight;
                        }
                        else {
                            endToStart = rightToLeft;
                            n2 = rightToRight;
                            if (layoutParams.endToEnd != -1) {
                                n2 = layoutParams.endToEnd;
                                endToStart = rightToLeft;
                            }
                        }
                    }
                }
                resolvedRightToLeft = endToStart;
                resolveGoneRightMargin = goneRightMargin;
                resolvedLeftToLeft = startToStart;
                resolveGoneLeftMargin = goneLeftMargin;
            }
            if (layoutParams.circleConstraint != -1) {
                final ConstraintWidget constraintWidget2 = (ConstraintWidget)sparseArray.get(layoutParams.circleConstraint);
                if (constraintWidget2 != null) {
                    constraintWidget.connectCircularConstraint(constraintWidget2, layoutParams.circleAngle, layoutParams.circleRadius);
                }
            }
            else {
                if (resolvedLeftToLeft != -1) {
                    final ConstraintWidget constraintWidget3 = (ConstraintWidget)sparseArray.get(resolvedLeftToLeft);
                    if (constraintWidget3 != null) {
                        constraintWidget.immediateConnect(ConstraintAnchor.Type.LEFT, constraintWidget3, ConstraintAnchor.Type.LEFT, layoutParams.leftMargin, resolveGoneLeftMargin);
                    }
                }
                else if (n != -1) {
                    final ConstraintWidget constraintWidget4 = (ConstraintWidget)sparseArray.get(n);
                    if (constraintWidget4 != null) {
                        constraintWidget.immediateConnect(ConstraintAnchor.Type.LEFT, constraintWidget4, ConstraintAnchor.Type.RIGHT, layoutParams.leftMargin, resolveGoneLeftMargin);
                    }
                }
                if (resolvedRightToLeft != -1) {
                    final ConstraintWidget constraintWidget5 = (ConstraintWidget)sparseArray.get(resolvedRightToLeft);
                    if (constraintWidget5 != null) {
                        constraintWidget.immediateConnect(ConstraintAnchor.Type.RIGHT, constraintWidget5, ConstraintAnchor.Type.LEFT, layoutParams.rightMargin, resolveGoneRightMargin);
                    }
                }
                else if (n2 != -1) {
                    final ConstraintWidget constraintWidget6 = (ConstraintWidget)sparseArray.get(n2);
                    if (constraintWidget6 != null) {
                        constraintWidget.immediateConnect(ConstraintAnchor.Type.RIGHT, constraintWidget6, ConstraintAnchor.Type.RIGHT, layoutParams.rightMargin, resolveGoneRightMargin);
                    }
                }
                if (layoutParams.topToTop != -1) {
                    final ConstraintWidget constraintWidget7 = (ConstraintWidget)sparseArray.get(layoutParams.topToTop);
                    if (constraintWidget7 != null) {
                        constraintWidget.immediateConnect(ConstraintAnchor.Type.TOP, constraintWidget7, ConstraintAnchor.Type.TOP, layoutParams.topMargin, layoutParams.goneTopMargin);
                    }
                }
                else if (layoutParams.topToBottom != -1) {
                    final ConstraintWidget constraintWidget8 = (ConstraintWidget)sparseArray.get(layoutParams.topToBottom);
                    if (constraintWidget8 != null) {
                        constraintWidget.immediateConnect(ConstraintAnchor.Type.TOP, constraintWidget8, ConstraintAnchor.Type.BOTTOM, layoutParams.topMargin, layoutParams.goneTopMargin);
                    }
                }
                if (layoutParams.bottomToTop != -1) {
                    final ConstraintWidget constraintWidget9 = (ConstraintWidget)sparseArray.get(layoutParams.bottomToTop);
                    if (constraintWidget9 != null) {
                        constraintWidget.immediateConnect(ConstraintAnchor.Type.BOTTOM, constraintWidget9, ConstraintAnchor.Type.TOP, layoutParams.bottomMargin, layoutParams.goneBottomMargin);
                    }
                }
                else if (layoutParams.bottomToBottom != -1) {
                    final ConstraintWidget constraintWidget10 = (ConstraintWidget)sparseArray.get(layoutParams.bottomToBottom);
                    if (constraintWidget10 != null) {
                        constraintWidget.immediateConnect(ConstraintAnchor.Type.BOTTOM, constraintWidget10, ConstraintAnchor.Type.BOTTOM, layoutParams.bottomMargin, layoutParams.goneBottomMargin);
                    }
                }
                if (layoutParams.baselineToBaseline != -1) {
                    this.setWidgetBaseline(constraintWidget, layoutParams, sparseArray, layoutParams.baselineToBaseline, ConstraintAnchor.Type.BASELINE);
                }
                else if (layoutParams.baselineToTop != -1) {
                    this.setWidgetBaseline(constraintWidget, layoutParams, sparseArray, layoutParams.baselineToTop, ConstraintAnchor.Type.TOP);
                }
                else if (layoutParams.baselineToBottom != -1) {
                    this.setWidgetBaseline(constraintWidget, layoutParams, sparseArray, layoutParams.baselineToBottom, ConstraintAnchor.Type.BOTTOM);
                }
                if (horizontalBiasPercent >= 0.0f) {
                    constraintWidget.setHorizontalBiasPercent(horizontalBiasPercent);
                }
                if (layoutParams.verticalBias >= 0.0f) {
                    constraintWidget.setVerticalBiasPercent(layoutParams.verticalBias);
                }
            }
            if (b && (layoutParams.editorAbsoluteX != -1 || layoutParams.editorAbsoluteY != -1)) {
                constraintWidget.setOrigin(layoutParams.editorAbsoluteX, layoutParams.editorAbsoluteY);
            }
            if (!layoutParams.horizontalDimensionFixed) {
                if (layoutParams.width == -1) {
                    if (layoutParams.constrainedWidth) {
                        constraintWidget.setHorizontalDimensionBehaviour(ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT);
                    }
                    else {
                        constraintWidget.setHorizontalDimensionBehaviour(ConstraintWidget.DimensionBehaviour.MATCH_PARENT);
                    }
                    constraintWidget.getAnchor(ConstraintAnchor.Type.LEFT).mMargin = layoutParams.leftMargin;
                    constraintWidget.getAnchor(ConstraintAnchor.Type.RIGHT).mMargin = layoutParams.rightMargin;
                }
                else {
                    constraintWidget.setHorizontalDimensionBehaviour(ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT);
                    constraintWidget.setWidth(0);
                }
            }
            else {
                constraintWidget.setHorizontalDimensionBehaviour(ConstraintWidget.DimensionBehaviour.FIXED);
                constraintWidget.setWidth(layoutParams.width);
                if (layoutParams.width == -2) {
                    constraintWidget.setHorizontalDimensionBehaviour(ConstraintWidget.DimensionBehaviour.WRAP_CONTENT);
                }
            }
            if (!layoutParams.verticalDimensionFixed) {
                if (layoutParams.height == -1) {
                    if (layoutParams.constrainedHeight) {
                        constraintWidget.setVerticalDimensionBehaviour(ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT);
                    }
                    else {
                        constraintWidget.setVerticalDimensionBehaviour(ConstraintWidget.DimensionBehaviour.MATCH_PARENT);
                    }
                    constraintWidget.getAnchor(ConstraintAnchor.Type.TOP).mMargin = layoutParams.topMargin;
                    constraintWidget.getAnchor(ConstraintAnchor.Type.BOTTOM).mMargin = layoutParams.bottomMargin;
                }
                else {
                    constraintWidget.setVerticalDimensionBehaviour(ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT);
                    constraintWidget.setHeight(0);
                }
            }
            else {
                constraintWidget.setVerticalDimensionBehaviour(ConstraintWidget.DimensionBehaviour.FIXED);
                constraintWidget.setHeight(layoutParams.height);
                if (layoutParams.height == -2) {
                    constraintWidget.setVerticalDimensionBehaviour(ConstraintWidget.DimensionBehaviour.WRAP_CONTENT);
                }
            }
            constraintWidget.setDimensionRatio(layoutParams.dimensionRatio);
            constraintWidget.setHorizontalWeight(layoutParams.horizontalWeight);
            constraintWidget.setVerticalWeight(layoutParams.verticalWeight);
            constraintWidget.setHorizontalChainStyle(layoutParams.horizontalChainStyle);
            constraintWidget.setVerticalChainStyle(layoutParams.verticalChainStyle);
            constraintWidget.setWrapBehaviorInParent(layoutParams.wrapBehaviorInParent);
            constraintWidget.setHorizontalMatchStyle(layoutParams.matchConstraintDefaultWidth, layoutParams.matchConstraintMinWidth, layoutParams.matchConstraintMaxWidth, layoutParams.matchConstraintPercentWidth);
            constraintWidget.setVerticalMatchStyle(layoutParams.matchConstraintDefaultHeight, layoutParams.matchConstraintMinHeight, layoutParams.matchConstraintMaxHeight, layoutParams.matchConstraintPercentHeight);
        }
    }
    
    protected boolean checkLayoutParams(final ViewGroup$LayoutParams viewGroup$LayoutParams) {
        return viewGroup$LayoutParams instanceof LayoutParams;
    }
    
    protected void dispatchDraw(final Canvas canvas) {
        final ArrayList<ConstraintHelper> mConstraintHelpers = this.mConstraintHelpers;
        if (mConstraintHelpers != null) {
            final int size = mConstraintHelpers.size();
            if (size > 0) {
                for (int i = 0; i < size; ++i) {
                    this.mConstraintHelpers.get(i).updatePreDraw(this);
                }
            }
        }
        super.dispatchDraw(canvas);
        if (this.isInEditMode()) {
            final float n = (float)this.getWidth();
            final float n2 = (float)this.getHeight();
            for (int childCount = this.getChildCount(), j = 0; j < childCount; ++j) {
                final View child = this.getChildAt(j);
                if (child.getVisibility() != 8) {
                    final Object tag = child.getTag();
                    if (tag != null && tag instanceof String) {
                        final String[] split = ((String)tag).split(",");
                        if (split.length == 4) {
                            final int int1 = Integer.parseInt(split[0]);
                            final int int2 = Integer.parseInt(split[1]);
                            final int int3 = Integer.parseInt(split[2]);
                            final int int4 = Integer.parseInt(split[3]);
                            final int n3 = (int)(int1 / 1080.0f * n);
                            final int n4 = (int)(int2 / 1920.0f * n2);
                            final int n5 = (int)(int3 / 1080.0f * n);
                            final int n6 = (int)(int4 / 1920.0f * n2);
                            final Paint paint = new Paint();
                            paint.setColor(-65536);
                            final float n7 = (float)n3;
                            final float n8 = (float)n4;
                            final float n9 = (float)(n3 + n5);
                            canvas.drawLine(n7, n8, n9, n8, paint);
                            final float n10 = (float)(n4 + n6);
                            canvas.drawLine(n9, n8, n9, n10, paint);
                            canvas.drawLine(n9, n10, n7, n10, paint);
                            canvas.drawLine(n7, n10, n7, n8, paint);
                            paint.setColor(-16711936);
                            canvas.drawLine(n7, n8, n9, n10, paint);
                            canvas.drawLine(n7, n10, n9, n8, paint);
                        }
                    }
                }
            }
        }
    }
    
    public void fillMetrics(final Metrics mMetrics) {
        this.mMetrics = mMetrics;
        this.mLayoutWidget.fillMetrics(mMetrics);
    }
    
    public void forceLayout() {
        this.markHierarchyDirty();
        super.forceLayout();
    }
    
    protected LayoutParams generateDefaultLayoutParams() {
        return new LayoutParams(-2, -2);
    }
    
    protected ViewGroup$LayoutParams generateLayoutParams(final ViewGroup$LayoutParams viewGroup$LayoutParams) {
        return (ViewGroup$LayoutParams)new LayoutParams(viewGroup$LayoutParams);
    }
    
    public LayoutParams generateLayoutParams(final AttributeSet set) {
        return new LayoutParams(this.getContext(), set);
    }
    
    public Object getDesignInformation(final int n, final Object o) {
        if (n == 0 && o instanceof String) {
            final String s = (String)o;
            final HashMap<String, Integer> mDesignIds = this.mDesignIds;
            if (mDesignIds != null && mDesignIds.containsKey(s)) {
                return this.mDesignIds.get(s);
            }
        }
        return null;
    }
    
    public int getMaxHeight() {
        return this.mMaxHeight;
    }
    
    public int getMaxWidth() {
        return this.mMaxWidth;
    }
    
    public int getMinHeight() {
        return this.mMinHeight;
    }
    
    public int getMinWidth() {
        return this.mMinWidth;
    }
    
    public int getOptimizationLevel() {
        return this.mLayoutWidget.getOptimizationLevel();
    }
    
    public String getSceneString() {
        final StringBuilder sb = new StringBuilder();
        if (this.mLayoutWidget.stringId == null) {
            final int id = this.getId();
            if (id != -1) {
                this.mLayoutWidget.stringId = this.getContext().getResources().getResourceEntryName(id);
            }
            else {
                this.mLayoutWidget.stringId = "parent";
            }
        }
        if (this.mLayoutWidget.getDebugName() == null) {
            final ConstraintWidgetContainer mLayoutWidget = this.mLayoutWidget;
            mLayoutWidget.setDebugName(mLayoutWidget.stringId);
            final StringBuilder sb2 = new StringBuilder();
            sb2.append(" setDebugName ");
            sb2.append(this.mLayoutWidget.getDebugName());
            Log.v("ConstraintLayout", sb2.toString());
        }
        for (final ConstraintWidget constraintWidget : this.mLayoutWidget.getChildren()) {
            final View view = (View)constraintWidget.getCompanionWidget();
            if (view != null) {
                if (constraintWidget.stringId == null) {
                    final int id2 = view.getId();
                    if (id2 != -1) {
                        constraintWidget.stringId = this.getContext().getResources().getResourceEntryName(id2);
                    }
                }
                if (constraintWidget.getDebugName() != null) {
                    continue;
                }
                constraintWidget.setDebugName(constraintWidget.stringId);
                final StringBuilder sb3 = new StringBuilder();
                sb3.append(" setDebugName ");
                sb3.append(constraintWidget.getDebugName());
                Log.v("ConstraintLayout", sb3.toString());
            }
        }
        this.mLayoutWidget.getSceneString(sb);
        return sb.toString();
    }
    
    public View getViewById(final int n) {
        return (View)this.mChildrenByIds.get(n);
    }
    
    public final ConstraintWidget getViewWidget(final View view) {
        if (view == this) {
            return this.mLayoutWidget;
        }
        if (view != null) {
            if (view.getLayoutParams() instanceof LayoutParams) {
                return ((LayoutParams)view.getLayoutParams()).widget;
            }
            view.setLayoutParams(this.generateLayoutParams(view.getLayoutParams()));
            if (view.getLayoutParams() instanceof LayoutParams) {
                return ((LayoutParams)view.getLayoutParams()).widget;
            }
        }
        return null;
    }
    
    protected boolean isRtl() {
        final int sdk_INT = Build$VERSION.SDK_INT;
        boolean b2;
        final boolean b = b2 = false;
        if (sdk_INT >= 17) {
            final boolean b3 = (this.getContext().getApplicationInfo().flags & 0x400000) != 0x0;
            b2 = b;
            if (b3) {
                b2 = b;
                if (1 == this.getLayoutDirection()) {
                    b2 = true;
                }
            }
        }
        return b2;
    }
    
    public void loadLayoutDescription(final int n) {
        if (n != 0) {
            try {
                this.mConstraintLayoutSpec = new ConstraintLayoutStates(this.getContext(), this, n);
            }
            catch (final Resources$NotFoundException ex) {
                this.mConstraintLayoutSpec = null;
            }
        }
        else {
            this.mConstraintLayoutSpec = null;
        }
    }
    
    protected void onLayout(final boolean b, int i, int n, int n2, int x) {
        n2 = this.getChildCount();
        final boolean inEditMode = this.isInEditMode();
        n = 0;
        View child;
        LayoutParams layoutParams;
        ConstraintWidget widget;
        int y;
        int n3;
        int n4;
        View content;
        for (i = 0; i < n2; ++i) {
            child = this.getChildAt(i);
            layoutParams = (LayoutParams)child.getLayoutParams();
            widget = layoutParams.widget;
            if (child.getVisibility() != 8 || layoutParams.isGuideline || layoutParams.isHelper || layoutParams.isVirtualGroup || inEditMode) {
                if (!layoutParams.isInPlaceholder) {
                    x = widget.getX();
                    y = widget.getY();
                    n3 = widget.getWidth() + x;
                    n4 = widget.getHeight() + y;
                    child.layout(x, y, n3, n4);
                    if (child instanceof Placeholder) {
                        content = ((Placeholder)child).getContent();
                        if (content != null) {
                            content.setVisibility(0);
                            content.layout(x, y, n3, n4);
                        }
                    }
                }
            }
        }
        n2 = this.mConstraintHelpers.size();
        if (n2 > 0) {
            for (i = n; i < n2; ++i) {
                this.mConstraintHelpers.get(i).updatePostLayout(this);
            }
        }
    }
    
    protected void onMeasure(final int mOnMeasureWidthMeasureSpec, final int mOnMeasureHeightMeasureSpec) {
        if (this.mOnMeasureWidthMeasureSpec == mOnMeasureWidthMeasureSpec) {
            final int mOnMeasureHeightMeasureSpec2 = this.mOnMeasureHeightMeasureSpec;
        }
        if (!this.mDirtyHierarchy) {
            for (int childCount = this.getChildCount(), i = 0; i < childCount; ++i) {
                if (this.getChildAt(i).isLayoutRequested()) {
                    this.mDirtyHierarchy = true;
                    break;
                }
            }
        }
        final boolean mDirtyHierarchy = this.mDirtyHierarchy;
        this.mOnMeasureWidthMeasureSpec = mOnMeasureWidthMeasureSpec;
        this.mOnMeasureHeightMeasureSpec = mOnMeasureHeightMeasureSpec;
        this.mLayoutWidget.setRtl(this.isRtl());
        if (this.mDirtyHierarchy) {
            this.mDirtyHierarchy = false;
            if (this.updateHierarchy()) {
                this.mLayoutWidget.updateHierarchy();
            }
        }
        this.resolveSystem(this.mLayoutWidget, this.mOptimizationLevel, mOnMeasureWidthMeasureSpec, mOnMeasureHeightMeasureSpec);
        this.resolveMeasuredDimension(mOnMeasureWidthMeasureSpec, mOnMeasureHeightMeasureSpec, this.mLayoutWidget.getWidth(), this.mLayoutWidget.getHeight(), this.mLayoutWidget.isWidthMeasuredTooSmall(), this.mLayoutWidget.isHeightMeasuredTooSmall());
    }
    
    public void onViewAdded(final View view) {
        super.onViewAdded(view);
        final ConstraintWidget viewWidget = this.getViewWidget(view);
        if (view instanceof androidx.constraintlayout.widget.Guideline && !(viewWidget instanceof Guideline)) {
            final LayoutParams layoutParams = (LayoutParams)view.getLayoutParams();
            layoutParams.widget = new Guideline();
            layoutParams.isGuideline = true;
            ((Guideline)layoutParams.widget).setOrientation(layoutParams.orientation);
        }
        if (view instanceof ConstraintHelper) {
            final ConstraintHelper constraintHelper = (ConstraintHelper)view;
            constraintHelper.validateParams();
            ((LayoutParams)view.getLayoutParams()).isHelper = true;
            if (!this.mConstraintHelpers.contains(constraintHelper)) {
                this.mConstraintHelpers.add(constraintHelper);
            }
        }
        this.mChildrenByIds.put(view.getId(), (Object)view);
        this.mDirtyHierarchy = true;
    }
    
    public void onViewRemoved(final View o) {
        super.onViewRemoved(o);
        this.mChildrenByIds.remove(o.getId());
        this.mLayoutWidget.remove(this.getViewWidget(o));
        this.mConstraintHelpers.remove(o);
        this.mDirtyHierarchy = true;
    }
    
    protected void parseLayoutDescription(final int n) {
        this.mConstraintLayoutSpec = new ConstraintLayoutStates(this.getContext(), this, n);
    }
    
    public void requestLayout() {
        this.markHierarchyDirty();
        super.requestLayout();
    }
    
    protected void resolveMeasuredDimension(int resolveSizeAndState, int min, int n, final int n2, final boolean b, final boolean b2) {
        final int paddingHeight = this.mMeasurer.paddingHeight;
        resolveSizeAndState = resolveSizeAndState(n + this.mMeasurer.paddingWidth, resolveSizeAndState, 0);
        n = resolveSizeAndState(n2 + paddingHeight, min, 0);
        min = Math.min(this.mMaxWidth, resolveSizeAndState & 0xFFFFFF);
        n = Math.min(this.mMaxHeight, n & 0xFFFFFF);
        resolveSizeAndState = min;
        if (b) {
            resolveSizeAndState = (min | 0x1000000);
        }
        min = n;
        if (b2) {
            min = (n | 0x1000000);
        }
        this.setMeasuredDimension(resolveSizeAndState, min);
        this.mLastMeasureWidth = resolveSizeAndState;
        this.mLastMeasureHeight = min;
    }
    
    protected void resolveSystem(final ConstraintWidgetContainer constraintWidgetContainer, final int n, int n2, int max) {
        final int mode = View$MeasureSpec.getMode(n2);
        final int size = View$MeasureSpec.getSize(n2);
        final int mode2 = View$MeasureSpec.getMode(max);
        final int size2 = View$MeasureSpec.getSize(max);
        final int max2 = Math.max(0, this.getPaddingTop());
        final int max3 = Math.max(0, this.getPaddingBottom());
        final int n3 = max2 + max3;
        final int paddingWidth = this.getPaddingWidth();
        this.mMeasurer.captureLayoutInfo(n2, max, max2, max3, paddingWidth, n3);
        if (Build$VERSION.SDK_INT >= 17) {
            n2 = Math.max(0, this.getPaddingStart());
            max = Math.max(0, this.getPaddingEnd());
            if (n2 <= 0 && max <= 0) {
                n2 = Math.max(0, this.getPaddingLeft());
            }
            else if (this.isRtl()) {
                n2 = max;
            }
        }
        else {
            n2 = Math.max(0, this.getPaddingLeft());
        }
        max = size - paddingWidth;
        final int n4 = size2 - n3;
        this.setSelfDimensionBehaviour(constraintWidgetContainer, mode, max, mode2, n4);
        constraintWidgetContainer.measure(n, mode, max, mode2, n4, this.mLastMeasureWidth, this.mLastMeasureHeight, n2, max2);
    }
    
    public void setConstraintSet(final ConstraintSet mConstraintSet) {
        this.mConstraintSet = mConstraintSet;
    }
    
    public void setDesignInformation(int i, final Object o, final Object o2) {
        if (i == 0 && o instanceof String && o2 instanceof Integer) {
            if (this.mDesignIds == null) {
                this.mDesignIds = new HashMap<String, Integer>();
            }
            final String s = (String)o;
            i = s.indexOf("/");
            String substring = s;
            if (i != -1) {
                substring = s.substring(i + 1);
            }
            i = (int)o2;
            this.mDesignIds.put(substring, i);
        }
    }
    
    public void setId(final int id) {
        this.mChildrenByIds.remove(this.getId());
        super.setId(id);
        this.mChildrenByIds.put(this.getId(), (Object)this);
    }
    
    public void setMaxHeight(final int mMaxHeight) {
        if (mMaxHeight == this.mMaxHeight) {
            return;
        }
        this.mMaxHeight = mMaxHeight;
        this.requestLayout();
    }
    
    public void setMaxWidth(final int mMaxWidth) {
        if (mMaxWidth == this.mMaxWidth) {
            return;
        }
        this.mMaxWidth = mMaxWidth;
        this.requestLayout();
    }
    
    public void setMinHeight(final int mMinHeight) {
        if (mMinHeight == this.mMinHeight) {
            return;
        }
        this.mMinHeight = mMinHeight;
        this.requestLayout();
    }
    
    public void setMinWidth(final int mMinWidth) {
        if (mMinWidth == this.mMinWidth) {
            return;
        }
        this.mMinWidth = mMinWidth;
        this.requestLayout();
    }
    
    public void setOnConstraintsChanged(final ConstraintsChangedListener constraintsChangedListener) {
        this.mConstraintsChangedListener = constraintsChangedListener;
        final ConstraintLayoutStates mConstraintLayoutSpec = this.mConstraintLayoutSpec;
        if (mConstraintLayoutSpec != null) {
            mConstraintLayoutSpec.setOnConstraintsChanged(constraintsChangedListener);
        }
    }
    
    public void setOptimizationLevel(final int n) {
        this.mOptimizationLevel = n;
        this.mLayoutWidget.setOptimizationLevel(n);
    }
    
    protected void setSelfDimensionBehaviour(final ConstraintWidgetContainer constraintWidgetContainer, final int n, int n2, final int n3, int n4) {
        final int paddingHeight = this.mMeasurer.paddingHeight;
        final int paddingWidth = this.mMeasurer.paddingWidth;
        ConstraintWidget.DimensionBehaviour horizontalDimensionBehaviour = ConstraintWidget.DimensionBehaviour.FIXED;
        ConstraintWidget.DimensionBehaviour verticalDimensionBehaviour = ConstraintWidget.DimensionBehaviour.FIXED;
        final int childCount = this.getChildCount();
        Label_0129: {
            if (n != Integer.MIN_VALUE) {
                if (n != 0) {
                    if (n == 1073741824) {
                        n2 = Math.min(this.mMaxWidth - paddingWidth, n2);
                        break Label_0129;
                    }
                }
                else {
                    final ConstraintWidget.DimensionBehaviour dimensionBehaviour = horizontalDimensionBehaviour = ConstraintWidget.DimensionBehaviour.WRAP_CONTENT;
                    if (childCount == 0) {
                        n2 = Math.max(0, this.mMinWidth);
                        horizontalDimensionBehaviour = dimensionBehaviour;
                        break Label_0129;
                    }
                }
                n2 = 0;
            }
            else {
                final ConstraintWidget.DimensionBehaviour dimensionBehaviour2 = horizontalDimensionBehaviour = ConstraintWidget.DimensionBehaviour.WRAP_CONTENT;
                if (childCount == 0) {
                    n2 = Math.max(0, this.mMinWidth);
                    horizontalDimensionBehaviour = dimensionBehaviour2;
                }
            }
        }
        Label_0232: {
            if (n3 != Integer.MIN_VALUE) {
                if (n3 != 0) {
                    if (n3 == 1073741824) {
                        n4 = Math.min(this.mMaxHeight - paddingHeight, n4);
                        break Label_0232;
                    }
                }
                else {
                    final ConstraintWidget.DimensionBehaviour dimensionBehaviour3 = verticalDimensionBehaviour = ConstraintWidget.DimensionBehaviour.WRAP_CONTENT;
                    if (childCount == 0) {
                        n4 = Math.max(0, this.mMinHeight);
                        verticalDimensionBehaviour = dimensionBehaviour3;
                        break Label_0232;
                    }
                }
                n4 = 0;
            }
            else {
                final ConstraintWidget.DimensionBehaviour dimensionBehaviour4 = verticalDimensionBehaviour = ConstraintWidget.DimensionBehaviour.WRAP_CONTENT;
                if (childCount == 0) {
                    n4 = Math.max(0, this.mMinHeight);
                    verticalDimensionBehaviour = dimensionBehaviour4;
                }
            }
        }
        if (n2 != constraintWidgetContainer.getWidth() || n4 != constraintWidgetContainer.getHeight()) {
            constraintWidgetContainer.invalidateMeasures();
        }
        constraintWidgetContainer.setX(0);
        constraintWidgetContainer.setY(0);
        constraintWidgetContainer.setMaxWidth(this.mMaxWidth - paddingWidth);
        constraintWidgetContainer.setMaxHeight(this.mMaxHeight - paddingHeight);
        constraintWidgetContainer.setMinWidth(0);
        constraintWidgetContainer.setMinHeight(0);
        constraintWidgetContainer.setHorizontalDimensionBehaviour(horizontalDimensionBehaviour);
        constraintWidgetContainer.setWidth(n2);
        constraintWidgetContainer.setVerticalDimensionBehaviour(verticalDimensionBehaviour);
        constraintWidgetContainer.setHeight(n4);
        constraintWidgetContainer.setMinWidth(this.mMinWidth - paddingWidth);
        constraintWidgetContainer.setMinHeight(this.mMinHeight - paddingHeight);
    }
    
    public void setState(final int n, final int n2, final int n3) {
        final ConstraintLayoutStates mConstraintLayoutSpec = this.mConstraintLayoutSpec;
        if (mConstraintLayoutSpec != null) {
            mConstraintLayoutSpec.updateConstraints(n, (float)n2, (float)n3);
        }
    }
    
    public boolean shouldDelayChildPressedState() {
        return false;
    }
    
    public static class LayoutParams extends ViewGroup$MarginLayoutParams
    {
        public static final int BASELINE = 5;
        public static final int BOTTOM = 4;
        public static final int CHAIN_PACKED = 2;
        public static final int CHAIN_SPREAD = 0;
        public static final int CHAIN_SPREAD_INSIDE = 1;
        public static final int CIRCLE = 8;
        public static final int END = 7;
        public static final int GONE_UNSET = Integer.MIN_VALUE;
        public static final int HORIZONTAL = 0;
        public static final int LEFT = 1;
        public static final int MATCH_CONSTRAINT = 0;
        public static final int MATCH_CONSTRAINT_PERCENT = 2;
        public static final int MATCH_CONSTRAINT_SPREAD = 0;
        public static final int MATCH_CONSTRAINT_WRAP = 1;
        public static final int PARENT_ID = 0;
        public static final int RIGHT = 2;
        public static final int START = 6;
        public static final int TOP = 3;
        public static final int UNSET = -1;
        public static final int VERTICAL = 1;
        public static final int WRAP_BEHAVIOR_HORIZONTAL_ONLY = 1;
        public static final int WRAP_BEHAVIOR_INCLUDED = 0;
        public static final int WRAP_BEHAVIOR_SKIPPED = 3;
        public static final int WRAP_BEHAVIOR_VERTICAL_ONLY = 2;
        public int baselineMargin;
        public int baselineToBaseline;
        public int baselineToBottom;
        public int baselineToTop;
        public int bottomToBottom;
        public int bottomToTop;
        public float circleAngle;
        public int circleConstraint;
        public int circleRadius;
        public boolean constrainedHeight;
        public boolean constrainedWidth;
        public String constraintTag;
        public String dimensionRatio;
        int dimensionRatioSide;
        float dimensionRatioValue;
        public int editorAbsoluteX;
        public int editorAbsoluteY;
        public int endToEnd;
        public int endToStart;
        public int goneBaselineMargin;
        public int goneBottomMargin;
        public int goneEndMargin;
        public int goneLeftMargin;
        public int goneRightMargin;
        public int goneStartMargin;
        public int goneTopMargin;
        public int guideBegin;
        public int guideEnd;
        public float guidePercent;
        public boolean guidelineUseRtl;
        boolean heightSet;
        public boolean helped;
        public float horizontalBias;
        public int horizontalChainStyle;
        boolean horizontalDimensionFixed;
        public float horizontalWeight;
        boolean isGuideline;
        boolean isHelper;
        boolean isInPlaceholder;
        boolean isVirtualGroup;
        public int leftToLeft;
        public int leftToRight;
        public int matchConstraintDefaultHeight;
        public int matchConstraintDefaultWidth;
        public int matchConstraintMaxHeight;
        public int matchConstraintMaxWidth;
        public int matchConstraintMinHeight;
        public int matchConstraintMinWidth;
        public float matchConstraintPercentHeight;
        public float matchConstraintPercentWidth;
        boolean needsBaseline;
        public int orientation;
        int resolveGoneLeftMargin;
        int resolveGoneRightMargin;
        int resolvedGuideBegin;
        int resolvedGuideEnd;
        float resolvedGuidePercent;
        float resolvedHorizontalBias;
        int resolvedLeftToLeft;
        int resolvedLeftToRight;
        int resolvedRightToLeft;
        int resolvedRightToRight;
        public int rightToLeft;
        public int rightToRight;
        public int startToEnd;
        public int startToStart;
        public int topToBottom;
        public int topToTop;
        public float verticalBias;
        public int verticalChainStyle;
        boolean verticalDimensionFixed;
        public float verticalWeight;
        ConstraintWidget widget;
        boolean widthSet;
        public int wrapBehaviorInParent;
        
        public LayoutParams(final int n, final int n2) {
            super(n, n2);
            this.guideBegin = -1;
            this.guideEnd = -1;
            this.guidePercent = -1.0f;
            this.guidelineUseRtl = true;
            this.leftToLeft = -1;
            this.leftToRight = -1;
            this.rightToLeft = -1;
            this.rightToRight = -1;
            this.topToTop = -1;
            this.topToBottom = -1;
            this.bottomToTop = -1;
            this.bottomToBottom = -1;
            this.baselineToBaseline = -1;
            this.baselineToTop = -1;
            this.baselineToBottom = -1;
            this.circleConstraint = -1;
            this.circleRadius = 0;
            this.circleAngle = 0.0f;
            this.startToEnd = -1;
            this.startToStart = -1;
            this.endToStart = -1;
            this.endToEnd = -1;
            this.goneLeftMargin = Integer.MIN_VALUE;
            this.goneTopMargin = Integer.MIN_VALUE;
            this.goneRightMargin = Integer.MIN_VALUE;
            this.goneBottomMargin = Integer.MIN_VALUE;
            this.goneStartMargin = Integer.MIN_VALUE;
            this.goneEndMargin = Integer.MIN_VALUE;
            this.goneBaselineMargin = Integer.MIN_VALUE;
            this.baselineMargin = 0;
            this.widthSet = true;
            this.heightSet = true;
            this.horizontalBias = 0.5f;
            this.verticalBias = 0.5f;
            this.dimensionRatio = null;
            this.dimensionRatioValue = 0.0f;
            this.dimensionRatioSide = 1;
            this.horizontalWeight = -1.0f;
            this.verticalWeight = -1.0f;
            this.horizontalChainStyle = 0;
            this.verticalChainStyle = 0;
            this.matchConstraintDefaultWidth = 0;
            this.matchConstraintDefaultHeight = 0;
            this.matchConstraintMinWidth = 0;
            this.matchConstraintMinHeight = 0;
            this.matchConstraintMaxWidth = 0;
            this.matchConstraintMaxHeight = 0;
            this.matchConstraintPercentWidth = 1.0f;
            this.matchConstraintPercentHeight = 1.0f;
            this.editorAbsoluteX = -1;
            this.editorAbsoluteY = -1;
            this.orientation = -1;
            this.constrainedWidth = false;
            this.constrainedHeight = false;
            this.constraintTag = null;
            this.wrapBehaviorInParent = 0;
            this.horizontalDimensionFixed = true;
            this.verticalDimensionFixed = true;
            this.needsBaseline = false;
            this.isGuideline = false;
            this.isHelper = false;
            this.isInPlaceholder = false;
            this.isVirtualGroup = false;
            this.resolvedLeftToLeft = -1;
            this.resolvedLeftToRight = -1;
            this.resolvedRightToLeft = -1;
            this.resolvedRightToRight = -1;
            this.resolveGoneLeftMargin = Integer.MIN_VALUE;
            this.resolveGoneRightMargin = Integer.MIN_VALUE;
            this.resolvedHorizontalBias = 0.5f;
            this.widget = new ConstraintWidget();
            this.helped = false;
        }
        
        public LayoutParams(Context obtainStyledAttributes, final AttributeSet set) {
            super(obtainStyledAttributes, set);
            this.guideBegin = -1;
            this.guideEnd = -1;
            this.guidePercent = -1.0f;
            this.guidelineUseRtl = true;
            this.leftToLeft = -1;
            this.leftToRight = -1;
            this.rightToLeft = -1;
            this.rightToRight = -1;
            this.topToTop = -1;
            this.topToBottom = -1;
            this.bottomToTop = -1;
            this.bottomToBottom = -1;
            this.baselineToBaseline = -1;
            this.baselineToTop = -1;
            this.baselineToBottom = -1;
            this.circleConstraint = -1;
            this.circleRadius = 0;
            this.circleAngle = 0.0f;
            this.startToEnd = -1;
            this.startToStart = -1;
            this.endToStart = -1;
            this.endToEnd = -1;
            this.goneLeftMargin = Integer.MIN_VALUE;
            this.goneTopMargin = Integer.MIN_VALUE;
            this.goneRightMargin = Integer.MIN_VALUE;
            this.goneBottomMargin = Integer.MIN_VALUE;
            this.goneStartMargin = Integer.MIN_VALUE;
            this.goneEndMargin = Integer.MIN_VALUE;
            this.goneBaselineMargin = Integer.MIN_VALUE;
            this.baselineMargin = 0;
            this.widthSet = true;
            this.heightSet = true;
            this.horizontalBias = 0.5f;
            this.verticalBias = 0.5f;
            this.dimensionRatio = null;
            this.dimensionRatioValue = 0.0f;
            this.dimensionRatioSide = 1;
            this.horizontalWeight = -1.0f;
            this.verticalWeight = -1.0f;
            this.horizontalChainStyle = 0;
            this.verticalChainStyle = 0;
            this.matchConstraintDefaultWidth = 0;
            this.matchConstraintDefaultHeight = 0;
            this.matchConstraintMinWidth = 0;
            this.matchConstraintMinHeight = 0;
            this.matchConstraintMaxWidth = 0;
            this.matchConstraintMaxHeight = 0;
            this.matchConstraintPercentWidth = 1.0f;
            this.matchConstraintPercentHeight = 1.0f;
            this.editorAbsoluteX = -1;
            this.editorAbsoluteY = -1;
            this.orientation = -1;
            this.constrainedWidth = false;
            this.constrainedHeight = false;
            this.constraintTag = null;
            this.wrapBehaviorInParent = 0;
            this.horizontalDimensionFixed = true;
            this.verticalDimensionFixed = true;
            this.needsBaseline = false;
            this.isGuideline = false;
            this.isHelper = false;
            this.isInPlaceholder = false;
            this.isVirtualGroup = false;
            this.resolvedLeftToLeft = -1;
            this.resolvedLeftToRight = -1;
            this.resolvedRightToLeft = -1;
            this.resolvedRightToRight = -1;
            this.resolveGoneLeftMargin = Integer.MIN_VALUE;
            this.resolveGoneRightMargin = Integer.MIN_VALUE;
            this.resolvedHorizontalBias = 0.5f;
            this.widget = new ConstraintWidget();
            this.helped = false;
            obtainStyledAttributes = (Context)obtainStyledAttributes.obtainStyledAttributes(set, R.styleable.ConstraintLayout_Layout);
            for (int indexCount = ((TypedArray)obtainStyledAttributes).getIndexCount(), i = 0; i < indexCount; ++i) {
                final int index = ((TypedArray)obtainStyledAttributes).getIndex(i);
                final int value = Table.map.get(index);
                switch (value) {
                    default: {
                        switch (value) {
                            default: {
                                switch (value) {
                                    default: {
                                        continue;
                                    }
                                    case 67: {
                                        this.guidelineUseRtl = ((TypedArray)obtainStyledAttributes).getBoolean(index, this.guidelineUseRtl);
                                        continue;
                                    }
                                    case 66: {
                                        this.wrapBehaviorInParent = ((TypedArray)obtainStyledAttributes).getInt(index, this.wrapBehaviorInParent);
                                        continue;
                                    }
                                    case 65: {
                                        ConstraintSet.parseDimensionConstraints(this, (TypedArray)obtainStyledAttributes, index, 1);
                                        this.heightSet = true;
                                        continue;
                                    }
                                    case 64: {
                                        ConstraintSet.parseDimensionConstraints(this, (TypedArray)obtainStyledAttributes, index, 0);
                                        this.widthSet = true;
                                        continue;
                                    }
                                }
                                break;
                            }
                            case 55: {
                                this.goneBaselineMargin = ((TypedArray)obtainStyledAttributes).getDimensionPixelSize(index, this.goneBaselineMargin);
                                continue;
                            }
                            case 54: {
                                this.baselineMargin = ((TypedArray)obtainStyledAttributes).getDimensionPixelSize(index, this.baselineMargin);
                                continue;
                            }
                            case 53: {
                                final int resourceId = ((TypedArray)obtainStyledAttributes).getResourceId(index, this.baselineToBottom);
                                this.baselineToBottom = resourceId;
                                if (resourceId == -1) {
                                    this.baselineToBottom = ((TypedArray)obtainStyledAttributes).getInt(index, -1);
                                    continue;
                                }
                                continue;
                            }
                            case 52: {
                                final int resourceId2 = ((TypedArray)obtainStyledAttributes).getResourceId(index, this.baselineToTop);
                                this.baselineToTop = resourceId2;
                                if (resourceId2 == -1) {
                                    this.baselineToTop = ((TypedArray)obtainStyledAttributes).getInt(index, -1);
                                    continue;
                                }
                                continue;
                            }
                            case 51: {
                                this.constraintTag = ((TypedArray)obtainStyledAttributes).getString(index);
                                continue;
                            }
                            case 50: {
                                this.editorAbsoluteY = ((TypedArray)obtainStyledAttributes).getDimensionPixelOffset(index, this.editorAbsoluteY);
                                continue;
                            }
                            case 49: {
                                this.editorAbsoluteX = ((TypedArray)obtainStyledAttributes).getDimensionPixelOffset(index, this.editorAbsoluteX);
                                continue;
                            }
                            case 48: {
                                this.verticalChainStyle = ((TypedArray)obtainStyledAttributes).getInt(index, 0);
                                continue;
                            }
                            case 47: {
                                this.horizontalChainStyle = ((TypedArray)obtainStyledAttributes).getInt(index, 0);
                                continue;
                            }
                            case 46: {
                                this.verticalWeight = ((TypedArray)obtainStyledAttributes).getFloat(index, this.verticalWeight);
                                continue;
                            }
                            case 45: {
                                this.horizontalWeight = ((TypedArray)obtainStyledAttributes).getFloat(index, this.horizontalWeight);
                                continue;
                            }
                            case 44: {
                                ConstraintSet.parseDimensionRatioString(this, ((TypedArray)obtainStyledAttributes).getString(index));
                                continue;
                            }
                        }
                        break;
                    }
                    case 38: {
                        this.matchConstraintPercentHeight = Math.max(0.0f, ((TypedArray)obtainStyledAttributes).getFloat(index, this.matchConstraintPercentHeight));
                        this.matchConstraintDefaultHeight = 2;
                        break;
                    }
                    case 37: {
                        try {
                            this.matchConstraintMaxHeight = ((TypedArray)obtainStyledAttributes).getDimensionPixelSize(index, this.matchConstraintMaxHeight);
                        }
                        catch (final Exception ex) {
                            if (((TypedArray)obtainStyledAttributes).getInt(index, this.matchConstraintMaxHeight) == -2) {
                                this.matchConstraintMaxHeight = -2;
                            }
                        }
                        break;
                    }
                    case 36: {
                        try {
                            this.matchConstraintMinHeight = ((TypedArray)obtainStyledAttributes).getDimensionPixelSize(index, this.matchConstraintMinHeight);
                        }
                        catch (final Exception ex2) {
                            if (((TypedArray)obtainStyledAttributes).getInt(index, this.matchConstraintMinHeight) == -2) {
                                this.matchConstraintMinHeight = -2;
                            }
                        }
                        break;
                    }
                    case 35: {
                        this.matchConstraintPercentWidth = Math.max(0.0f, ((TypedArray)obtainStyledAttributes).getFloat(index, this.matchConstraintPercentWidth));
                        this.matchConstraintDefaultWidth = 2;
                        break;
                    }
                    case 34: {
                        try {
                            this.matchConstraintMaxWidth = ((TypedArray)obtainStyledAttributes).getDimensionPixelSize(index, this.matchConstraintMaxWidth);
                        }
                        catch (final Exception ex3) {
                            if (((TypedArray)obtainStyledAttributes).getInt(index, this.matchConstraintMaxWidth) == -2) {
                                this.matchConstraintMaxWidth = -2;
                            }
                        }
                        break;
                    }
                    case 33: {
                        try {
                            this.matchConstraintMinWidth = ((TypedArray)obtainStyledAttributes).getDimensionPixelSize(index, this.matchConstraintMinWidth);
                        }
                        catch (final Exception ex4) {
                            if (((TypedArray)obtainStyledAttributes).getInt(index, this.matchConstraintMinWidth) == -2) {
                                this.matchConstraintMinWidth = -2;
                            }
                        }
                        break;
                    }
                    case 32: {
                        final int int1 = ((TypedArray)obtainStyledAttributes).getInt(index, 0);
                        this.matchConstraintDefaultHeight = int1;
                        if (int1 == 1) {
                            Log.e("ConstraintLayout", "layout_constraintHeight_default=\"wrap\" is deprecated.\nUse layout_height=\"WRAP_CONTENT\" and layout_constrainedHeight=\"true\" instead.");
                            break;
                        }
                        break;
                    }
                    case 31: {
                        final int int2 = ((TypedArray)obtainStyledAttributes).getInt(index, 0);
                        this.matchConstraintDefaultWidth = int2;
                        if (int2 == 1) {
                            Log.e("ConstraintLayout", "layout_constraintWidth_default=\"wrap\" is deprecated.\nUse layout_width=\"WRAP_CONTENT\" and layout_constrainedWidth=\"true\" instead.");
                            break;
                        }
                        break;
                    }
                    case 30: {
                        this.verticalBias = ((TypedArray)obtainStyledAttributes).getFloat(index, this.verticalBias);
                        break;
                    }
                    case 29: {
                        this.horizontalBias = ((TypedArray)obtainStyledAttributes).getFloat(index, this.horizontalBias);
                        break;
                    }
                    case 28: {
                        this.constrainedHeight = ((TypedArray)obtainStyledAttributes).getBoolean(index, this.constrainedHeight);
                        break;
                    }
                    case 27: {
                        this.constrainedWidth = ((TypedArray)obtainStyledAttributes).getBoolean(index, this.constrainedWidth);
                        break;
                    }
                    case 26: {
                        this.goneEndMargin = ((TypedArray)obtainStyledAttributes).getDimensionPixelSize(index, this.goneEndMargin);
                        break;
                    }
                    case 25: {
                        this.goneStartMargin = ((TypedArray)obtainStyledAttributes).getDimensionPixelSize(index, this.goneStartMargin);
                        break;
                    }
                    case 24: {
                        this.goneBottomMargin = ((TypedArray)obtainStyledAttributes).getDimensionPixelSize(index, this.goneBottomMargin);
                        break;
                    }
                    case 23: {
                        this.goneRightMargin = ((TypedArray)obtainStyledAttributes).getDimensionPixelSize(index, this.goneRightMargin);
                        break;
                    }
                    case 22: {
                        this.goneTopMargin = ((TypedArray)obtainStyledAttributes).getDimensionPixelSize(index, this.goneTopMargin);
                        break;
                    }
                    case 21: {
                        this.goneLeftMargin = ((TypedArray)obtainStyledAttributes).getDimensionPixelSize(index, this.goneLeftMargin);
                        break;
                    }
                    case 20: {
                        final int resourceId3 = ((TypedArray)obtainStyledAttributes).getResourceId(index, this.endToEnd);
                        this.endToEnd = resourceId3;
                        if (resourceId3 == -1) {
                            this.endToEnd = ((TypedArray)obtainStyledAttributes).getInt(index, -1);
                            break;
                        }
                        break;
                    }
                    case 19: {
                        final int resourceId4 = ((TypedArray)obtainStyledAttributes).getResourceId(index, this.endToStart);
                        this.endToStart = resourceId4;
                        if (resourceId4 == -1) {
                            this.endToStart = ((TypedArray)obtainStyledAttributes).getInt(index, -1);
                            break;
                        }
                        break;
                    }
                    case 18: {
                        final int resourceId5 = ((TypedArray)obtainStyledAttributes).getResourceId(index, this.startToStart);
                        this.startToStart = resourceId5;
                        if (resourceId5 == -1) {
                            this.startToStart = ((TypedArray)obtainStyledAttributes).getInt(index, -1);
                            break;
                        }
                        break;
                    }
                    case 17: {
                        final int resourceId6 = ((TypedArray)obtainStyledAttributes).getResourceId(index, this.startToEnd);
                        this.startToEnd = resourceId6;
                        if (resourceId6 == -1) {
                            this.startToEnd = ((TypedArray)obtainStyledAttributes).getInt(index, -1);
                            break;
                        }
                        break;
                    }
                    case 16: {
                        final int resourceId7 = ((TypedArray)obtainStyledAttributes).getResourceId(index, this.baselineToBaseline);
                        this.baselineToBaseline = resourceId7;
                        if (resourceId7 == -1) {
                            this.baselineToBaseline = ((TypedArray)obtainStyledAttributes).getInt(index, -1);
                            break;
                        }
                        break;
                    }
                    case 15: {
                        final int resourceId8 = ((TypedArray)obtainStyledAttributes).getResourceId(index, this.bottomToBottom);
                        this.bottomToBottom = resourceId8;
                        if (resourceId8 == -1) {
                            this.bottomToBottom = ((TypedArray)obtainStyledAttributes).getInt(index, -1);
                            break;
                        }
                        break;
                    }
                    case 14: {
                        final int resourceId9 = ((TypedArray)obtainStyledAttributes).getResourceId(index, this.bottomToTop);
                        this.bottomToTop = resourceId9;
                        if (resourceId9 == -1) {
                            this.bottomToTop = ((TypedArray)obtainStyledAttributes).getInt(index, -1);
                            break;
                        }
                        break;
                    }
                    case 13: {
                        final int resourceId10 = ((TypedArray)obtainStyledAttributes).getResourceId(index, this.topToBottom);
                        this.topToBottom = resourceId10;
                        if (resourceId10 == -1) {
                            this.topToBottom = ((TypedArray)obtainStyledAttributes).getInt(index, -1);
                            break;
                        }
                        break;
                    }
                    case 12: {
                        final int resourceId11 = ((TypedArray)obtainStyledAttributes).getResourceId(index, this.topToTop);
                        this.topToTop = resourceId11;
                        if (resourceId11 == -1) {
                            this.topToTop = ((TypedArray)obtainStyledAttributes).getInt(index, -1);
                            break;
                        }
                        break;
                    }
                    case 11: {
                        final int resourceId12 = ((TypedArray)obtainStyledAttributes).getResourceId(index, this.rightToRight);
                        this.rightToRight = resourceId12;
                        if (resourceId12 == -1) {
                            this.rightToRight = ((TypedArray)obtainStyledAttributes).getInt(index, -1);
                            break;
                        }
                        break;
                    }
                    case 10: {
                        final int resourceId13 = ((TypedArray)obtainStyledAttributes).getResourceId(index, this.rightToLeft);
                        this.rightToLeft = resourceId13;
                        if (resourceId13 == -1) {
                            this.rightToLeft = ((TypedArray)obtainStyledAttributes).getInt(index, -1);
                            break;
                        }
                        break;
                    }
                    case 9: {
                        final int resourceId14 = ((TypedArray)obtainStyledAttributes).getResourceId(index, this.leftToRight);
                        this.leftToRight = resourceId14;
                        if (resourceId14 == -1) {
                            this.leftToRight = ((TypedArray)obtainStyledAttributes).getInt(index, -1);
                            break;
                        }
                        break;
                    }
                    case 8: {
                        final int resourceId15 = ((TypedArray)obtainStyledAttributes).getResourceId(index, this.leftToLeft);
                        this.leftToLeft = resourceId15;
                        if (resourceId15 == -1) {
                            this.leftToLeft = ((TypedArray)obtainStyledAttributes).getInt(index, -1);
                            break;
                        }
                        break;
                    }
                    case 7: {
                        this.guidePercent = ((TypedArray)obtainStyledAttributes).getFloat(index, this.guidePercent);
                        break;
                    }
                    case 6: {
                        this.guideEnd = ((TypedArray)obtainStyledAttributes).getDimensionPixelOffset(index, this.guideEnd);
                        break;
                    }
                    case 5: {
                        this.guideBegin = ((TypedArray)obtainStyledAttributes).getDimensionPixelOffset(index, this.guideBegin);
                        break;
                    }
                    case 4: {
                        final float circleAngle = ((TypedArray)obtainStyledAttributes).getFloat(index, this.circleAngle) % 360.0f;
                        this.circleAngle = circleAngle;
                        if (circleAngle < 0.0f) {
                            this.circleAngle = (360.0f - circleAngle) % 360.0f;
                            break;
                        }
                        break;
                    }
                    case 3: {
                        this.circleRadius = ((TypedArray)obtainStyledAttributes).getDimensionPixelSize(index, this.circleRadius);
                        break;
                    }
                    case 2: {
                        final int resourceId16 = ((TypedArray)obtainStyledAttributes).getResourceId(index, this.circleConstraint);
                        this.circleConstraint = resourceId16;
                        if (resourceId16 == -1) {
                            this.circleConstraint = ((TypedArray)obtainStyledAttributes).getInt(index, -1);
                            break;
                        }
                        break;
                    }
                    case 1: {
                        this.orientation = ((TypedArray)obtainStyledAttributes).getInt(index, this.orientation);
                        break;
                    }
                }
            }
            ((TypedArray)obtainStyledAttributes).recycle();
            this.validate();
        }
        
        public LayoutParams(final ViewGroup$LayoutParams viewGroup$LayoutParams) {
            super(viewGroup$LayoutParams);
            this.guideBegin = -1;
            this.guideEnd = -1;
            this.guidePercent = -1.0f;
            this.guidelineUseRtl = true;
            this.leftToLeft = -1;
            this.leftToRight = -1;
            this.rightToLeft = -1;
            this.rightToRight = -1;
            this.topToTop = -1;
            this.topToBottom = -1;
            this.bottomToTop = -1;
            this.bottomToBottom = -1;
            this.baselineToBaseline = -1;
            this.baselineToTop = -1;
            this.baselineToBottom = -1;
            this.circleConstraint = -1;
            this.circleRadius = 0;
            this.circleAngle = 0.0f;
            this.startToEnd = -1;
            this.startToStart = -1;
            this.endToStart = -1;
            this.endToEnd = -1;
            this.goneLeftMargin = Integer.MIN_VALUE;
            this.goneTopMargin = Integer.MIN_VALUE;
            this.goneRightMargin = Integer.MIN_VALUE;
            this.goneBottomMargin = Integer.MIN_VALUE;
            this.goneStartMargin = Integer.MIN_VALUE;
            this.goneEndMargin = Integer.MIN_VALUE;
            this.goneBaselineMargin = Integer.MIN_VALUE;
            this.baselineMargin = 0;
            this.widthSet = true;
            this.heightSet = true;
            this.horizontalBias = 0.5f;
            this.verticalBias = 0.5f;
            this.dimensionRatio = null;
            this.dimensionRatioValue = 0.0f;
            this.dimensionRatioSide = 1;
            this.horizontalWeight = -1.0f;
            this.verticalWeight = -1.0f;
            this.horizontalChainStyle = 0;
            this.verticalChainStyle = 0;
            this.matchConstraintDefaultWidth = 0;
            this.matchConstraintDefaultHeight = 0;
            this.matchConstraintMinWidth = 0;
            this.matchConstraintMinHeight = 0;
            this.matchConstraintMaxWidth = 0;
            this.matchConstraintMaxHeight = 0;
            this.matchConstraintPercentWidth = 1.0f;
            this.matchConstraintPercentHeight = 1.0f;
            this.editorAbsoluteX = -1;
            this.editorAbsoluteY = -1;
            this.orientation = -1;
            this.constrainedWidth = false;
            this.constrainedHeight = false;
            this.constraintTag = null;
            this.wrapBehaviorInParent = 0;
            this.horizontalDimensionFixed = true;
            this.verticalDimensionFixed = true;
            this.needsBaseline = false;
            this.isGuideline = false;
            this.isHelper = false;
            this.isInPlaceholder = false;
            this.isVirtualGroup = false;
            this.resolvedLeftToLeft = -1;
            this.resolvedLeftToRight = -1;
            this.resolvedRightToLeft = -1;
            this.resolvedRightToRight = -1;
            this.resolveGoneLeftMargin = Integer.MIN_VALUE;
            this.resolveGoneRightMargin = Integer.MIN_VALUE;
            this.resolvedHorizontalBias = 0.5f;
            this.widget = new ConstraintWidget();
            this.helped = false;
        }
        
        public LayoutParams(final LayoutParams layoutParams) {
            super((ViewGroup$MarginLayoutParams)layoutParams);
            this.guideBegin = -1;
            this.guideEnd = -1;
            this.guidePercent = -1.0f;
            this.guidelineUseRtl = true;
            this.leftToLeft = -1;
            this.leftToRight = -1;
            this.rightToLeft = -1;
            this.rightToRight = -1;
            this.topToTop = -1;
            this.topToBottom = -1;
            this.bottomToTop = -1;
            this.bottomToBottom = -1;
            this.baselineToBaseline = -1;
            this.baselineToTop = -1;
            this.baselineToBottom = -1;
            this.circleConstraint = -1;
            this.circleRadius = 0;
            this.circleAngle = 0.0f;
            this.startToEnd = -1;
            this.startToStart = -1;
            this.endToStart = -1;
            this.endToEnd = -1;
            this.goneLeftMargin = Integer.MIN_VALUE;
            this.goneTopMargin = Integer.MIN_VALUE;
            this.goneRightMargin = Integer.MIN_VALUE;
            this.goneBottomMargin = Integer.MIN_VALUE;
            this.goneStartMargin = Integer.MIN_VALUE;
            this.goneEndMargin = Integer.MIN_VALUE;
            this.goneBaselineMargin = Integer.MIN_VALUE;
            this.baselineMargin = 0;
            this.widthSet = true;
            this.heightSet = true;
            this.horizontalBias = 0.5f;
            this.verticalBias = 0.5f;
            this.dimensionRatio = null;
            this.dimensionRatioValue = 0.0f;
            this.dimensionRatioSide = 1;
            this.horizontalWeight = -1.0f;
            this.verticalWeight = -1.0f;
            this.horizontalChainStyle = 0;
            this.verticalChainStyle = 0;
            this.matchConstraintDefaultWidth = 0;
            this.matchConstraintDefaultHeight = 0;
            this.matchConstraintMinWidth = 0;
            this.matchConstraintMinHeight = 0;
            this.matchConstraintMaxWidth = 0;
            this.matchConstraintMaxHeight = 0;
            this.matchConstraintPercentWidth = 1.0f;
            this.matchConstraintPercentHeight = 1.0f;
            this.editorAbsoluteX = -1;
            this.editorAbsoluteY = -1;
            this.orientation = -1;
            this.constrainedWidth = false;
            this.constrainedHeight = false;
            this.constraintTag = null;
            this.wrapBehaviorInParent = 0;
            this.horizontalDimensionFixed = true;
            this.verticalDimensionFixed = true;
            this.needsBaseline = false;
            this.isGuideline = false;
            this.isHelper = false;
            this.isInPlaceholder = false;
            this.isVirtualGroup = false;
            this.resolvedLeftToLeft = -1;
            this.resolvedLeftToRight = -1;
            this.resolvedRightToLeft = -1;
            this.resolvedRightToRight = -1;
            this.resolveGoneLeftMargin = Integer.MIN_VALUE;
            this.resolveGoneRightMargin = Integer.MIN_VALUE;
            this.resolvedHorizontalBias = 0.5f;
            this.widget = new ConstraintWidget();
            this.helped = false;
            this.guideBegin = layoutParams.guideBegin;
            this.guideEnd = layoutParams.guideEnd;
            this.guidePercent = layoutParams.guidePercent;
            this.guidelineUseRtl = layoutParams.guidelineUseRtl;
            this.leftToLeft = layoutParams.leftToLeft;
            this.leftToRight = layoutParams.leftToRight;
            this.rightToLeft = layoutParams.rightToLeft;
            this.rightToRight = layoutParams.rightToRight;
            this.topToTop = layoutParams.topToTop;
            this.topToBottom = layoutParams.topToBottom;
            this.bottomToTop = layoutParams.bottomToTop;
            this.bottomToBottom = layoutParams.bottomToBottom;
            this.baselineToBaseline = layoutParams.baselineToBaseline;
            this.baselineToTop = layoutParams.baselineToTop;
            this.baselineToBottom = layoutParams.baselineToBottom;
            this.circleConstraint = layoutParams.circleConstraint;
            this.circleRadius = layoutParams.circleRadius;
            this.circleAngle = layoutParams.circleAngle;
            this.startToEnd = layoutParams.startToEnd;
            this.startToStart = layoutParams.startToStart;
            this.endToStart = layoutParams.endToStart;
            this.endToEnd = layoutParams.endToEnd;
            this.goneLeftMargin = layoutParams.goneLeftMargin;
            this.goneTopMargin = layoutParams.goneTopMargin;
            this.goneRightMargin = layoutParams.goneRightMargin;
            this.goneBottomMargin = layoutParams.goneBottomMargin;
            this.goneStartMargin = layoutParams.goneStartMargin;
            this.goneEndMargin = layoutParams.goneEndMargin;
            this.goneBaselineMargin = layoutParams.goneBaselineMargin;
            this.baselineMargin = layoutParams.baselineMargin;
            this.horizontalBias = layoutParams.horizontalBias;
            this.verticalBias = layoutParams.verticalBias;
            this.dimensionRatio = layoutParams.dimensionRatio;
            this.dimensionRatioValue = layoutParams.dimensionRatioValue;
            this.dimensionRatioSide = layoutParams.dimensionRatioSide;
            this.horizontalWeight = layoutParams.horizontalWeight;
            this.verticalWeight = layoutParams.verticalWeight;
            this.horizontalChainStyle = layoutParams.horizontalChainStyle;
            this.verticalChainStyle = layoutParams.verticalChainStyle;
            this.constrainedWidth = layoutParams.constrainedWidth;
            this.constrainedHeight = layoutParams.constrainedHeight;
            this.matchConstraintDefaultWidth = layoutParams.matchConstraintDefaultWidth;
            this.matchConstraintDefaultHeight = layoutParams.matchConstraintDefaultHeight;
            this.matchConstraintMinWidth = layoutParams.matchConstraintMinWidth;
            this.matchConstraintMaxWidth = layoutParams.matchConstraintMaxWidth;
            this.matchConstraintMinHeight = layoutParams.matchConstraintMinHeight;
            this.matchConstraintMaxHeight = layoutParams.matchConstraintMaxHeight;
            this.matchConstraintPercentWidth = layoutParams.matchConstraintPercentWidth;
            this.matchConstraintPercentHeight = layoutParams.matchConstraintPercentHeight;
            this.editorAbsoluteX = layoutParams.editorAbsoluteX;
            this.editorAbsoluteY = layoutParams.editorAbsoluteY;
            this.orientation = layoutParams.orientation;
            this.horizontalDimensionFixed = layoutParams.horizontalDimensionFixed;
            this.verticalDimensionFixed = layoutParams.verticalDimensionFixed;
            this.needsBaseline = layoutParams.needsBaseline;
            this.isGuideline = layoutParams.isGuideline;
            this.resolvedLeftToLeft = layoutParams.resolvedLeftToLeft;
            this.resolvedLeftToRight = layoutParams.resolvedLeftToRight;
            this.resolvedRightToLeft = layoutParams.resolvedRightToLeft;
            this.resolvedRightToRight = layoutParams.resolvedRightToRight;
            this.resolveGoneLeftMargin = layoutParams.resolveGoneLeftMargin;
            this.resolveGoneRightMargin = layoutParams.resolveGoneRightMargin;
            this.resolvedHorizontalBias = layoutParams.resolvedHorizontalBias;
            this.constraintTag = layoutParams.constraintTag;
            this.wrapBehaviorInParent = layoutParams.wrapBehaviorInParent;
            this.widget = layoutParams.widget;
            this.widthSet = layoutParams.widthSet;
            this.heightSet = layoutParams.heightSet;
        }
        
        public String getConstraintTag() {
            return this.constraintTag;
        }
        
        public ConstraintWidget getConstraintWidget() {
            return this.widget;
        }
        
        public void reset() {
            final ConstraintWidget widget = this.widget;
            if (widget != null) {
                widget.reset();
            }
        }
        
        public void resolveLayoutDirection(int resolvedLeftToRight) {
            final int leftMargin = this.leftMargin;
            final int rightMargin = this.rightMargin;
            final int sdk_INT = Build$VERSION.SDK_INT;
            final int n = 0;
            Label_0047: {
                if (sdk_INT >= 17) {
                    super.resolveLayoutDirection(resolvedLeftToRight);
                    if (1 == this.getLayoutDirection()) {
                        resolvedLeftToRight = 1;
                        break Label_0047;
                    }
                }
                resolvedLeftToRight = 0;
            }
            this.resolvedRightToLeft = -1;
            this.resolvedRightToRight = -1;
            this.resolvedLeftToLeft = -1;
            this.resolvedLeftToRight = -1;
            this.resolveGoneLeftMargin = this.goneLeftMargin;
            this.resolveGoneRightMargin = this.goneRightMargin;
            final float horizontalBias = this.horizontalBias;
            this.resolvedHorizontalBias = horizontalBias;
            final int guideBegin = this.guideBegin;
            this.resolvedGuideBegin = guideBegin;
            final int guideEnd = this.guideEnd;
            this.resolvedGuideEnd = guideEnd;
            final float guidePercent = this.guidePercent;
            this.resolvedGuidePercent = guidePercent;
            if (resolvedLeftToRight != 0) {
                resolvedLeftToRight = this.startToEnd;
                Label_0175: {
                    if (resolvedLeftToRight != -1) {
                        this.resolvedRightToLeft = resolvedLeftToRight;
                    }
                    else {
                        final int startToStart = this.startToStart;
                        resolvedLeftToRight = n;
                        if (startToStart == -1) {
                            break Label_0175;
                        }
                        this.resolvedRightToRight = startToStart;
                    }
                    resolvedLeftToRight = 1;
                }
                final int endToStart = this.endToStart;
                if (endToStart != -1) {
                    this.resolvedLeftToRight = endToStart;
                    resolvedLeftToRight = 1;
                }
                final int endToEnd = this.endToEnd;
                if (endToEnd != -1) {
                    this.resolvedLeftToLeft = endToEnd;
                    resolvedLeftToRight = 1;
                }
                final int goneStartMargin = this.goneStartMargin;
                if (goneStartMargin != Integer.MIN_VALUE) {
                    this.resolveGoneRightMargin = goneStartMargin;
                }
                final int goneEndMargin = this.goneEndMargin;
                if (goneEndMargin != Integer.MIN_VALUE) {
                    this.resolveGoneLeftMargin = goneEndMargin;
                }
                if (resolvedLeftToRight != 0) {
                    this.resolvedHorizontalBias = 1.0f - horizontalBias;
                }
                if (this.isGuideline && this.orientation == 1 && this.guidelineUseRtl) {
                    if (guidePercent != -1.0f) {
                        this.resolvedGuidePercent = 1.0f - guidePercent;
                        this.resolvedGuideBegin = -1;
                        this.resolvedGuideEnd = -1;
                    }
                    else if (guideBegin != -1) {
                        this.resolvedGuideEnd = guideBegin;
                        this.resolvedGuideBegin = -1;
                        this.resolvedGuidePercent = -1.0f;
                    }
                    else if (guideEnd != -1) {
                        this.resolvedGuideBegin = guideEnd;
                        this.resolvedGuideEnd = -1;
                        this.resolvedGuidePercent = -1.0f;
                    }
                }
            }
            else {
                resolvedLeftToRight = this.startToEnd;
                if (resolvedLeftToRight != -1) {
                    this.resolvedLeftToRight = resolvedLeftToRight;
                }
                resolvedLeftToRight = this.startToStart;
                if (resolvedLeftToRight != -1) {
                    this.resolvedLeftToLeft = resolvedLeftToRight;
                }
                resolvedLeftToRight = this.endToStart;
                if (resolvedLeftToRight != -1) {
                    this.resolvedRightToLeft = resolvedLeftToRight;
                }
                resolvedLeftToRight = this.endToEnd;
                if (resolvedLeftToRight != -1) {
                    this.resolvedRightToRight = resolvedLeftToRight;
                }
                resolvedLeftToRight = this.goneStartMargin;
                if (resolvedLeftToRight != Integer.MIN_VALUE) {
                    this.resolveGoneLeftMargin = resolvedLeftToRight;
                }
                resolvedLeftToRight = this.goneEndMargin;
                if (resolvedLeftToRight != Integer.MIN_VALUE) {
                    this.resolveGoneRightMargin = resolvedLeftToRight;
                }
            }
            if (this.endToStart == -1 && this.endToEnd == -1 && this.startToStart == -1 && this.startToEnd == -1) {
                resolvedLeftToRight = this.rightToLeft;
                if (resolvedLeftToRight != -1) {
                    this.resolvedRightToLeft = resolvedLeftToRight;
                    if (this.rightMargin <= 0 && rightMargin > 0) {
                        this.rightMargin = rightMargin;
                    }
                }
                else {
                    resolvedLeftToRight = this.rightToRight;
                    if (resolvedLeftToRight != -1) {
                        this.resolvedRightToRight = resolvedLeftToRight;
                        if (this.rightMargin <= 0 && rightMargin > 0) {
                            this.rightMargin = rightMargin;
                        }
                    }
                }
                resolvedLeftToRight = this.leftToLeft;
                if (resolvedLeftToRight != -1) {
                    this.resolvedLeftToLeft = resolvedLeftToRight;
                    if (this.leftMargin <= 0 && leftMargin > 0) {
                        this.leftMargin = leftMargin;
                    }
                }
                else {
                    resolvedLeftToRight = this.leftToRight;
                    if (resolvedLeftToRight != -1) {
                        this.resolvedLeftToRight = resolvedLeftToRight;
                        if (this.leftMargin <= 0 && leftMargin > 0) {
                            this.leftMargin = leftMargin;
                        }
                    }
                }
            }
        }
        
        public void setWidgetDebugName(final String debugName) {
            this.widget.setDebugName(debugName);
        }
        
        public void validate() {
            this.isGuideline = false;
            this.horizontalDimensionFixed = true;
            this.verticalDimensionFixed = true;
            if (this.width == -2 && this.constrainedWidth) {
                this.horizontalDimensionFixed = false;
                if (this.matchConstraintDefaultWidth == 0) {
                    this.matchConstraintDefaultWidth = 1;
                }
            }
            if (this.height == -2 && this.constrainedHeight) {
                this.verticalDimensionFixed = false;
                if (this.matchConstraintDefaultHeight == 0) {
                    this.matchConstraintDefaultHeight = 1;
                }
            }
            if (this.width == 0 || this.width == -1) {
                this.horizontalDimensionFixed = false;
                if (this.width == 0 && this.matchConstraintDefaultWidth == 1) {
                    this.width = -2;
                    this.constrainedWidth = true;
                }
            }
            if (this.height == 0 || this.height == -1) {
                this.verticalDimensionFixed = false;
                if (this.height == 0 && this.matchConstraintDefaultHeight == 1) {
                    this.height = -2;
                    this.constrainedHeight = true;
                }
            }
            if (this.guidePercent != -1.0f || this.guideBegin != -1 || this.guideEnd != -1) {
                this.isGuideline = true;
                this.horizontalDimensionFixed = true;
                this.verticalDimensionFixed = true;
                if (!(this.widget instanceof Guideline)) {
                    this.widget = new Guideline();
                }
                ((Guideline)this.widget).setOrientation(this.orientation);
            }
        }
        
        private static class Table
        {
            public static final int ANDROID_ORIENTATION = 1;
            public static final int GUIDELINE_USE_RTL = 67;
            public static final int LAYOUT_CONSTRAINED_HEIGHT = 28;
            public static final int LAYOUT_CONSTRAINED_WIDTH = 27;
            public static final int LAYOUT_CONSTRAINT_BASELINE_CREATOR = 43;
            public static final int LAYOUT_CONSTRAINT_BASELINE_TO_BASELINE_OF = 16;
            public static final int LAYOUT_CONSTRAINT_BASELINE_TO_BOTTOM_OF = 53;
            public static final int LAYOUT_CONSTRAINT_BASELINE_TO_TOP_OF = 52;
            public static final int LAYOUT_CONSTRAINT_BOTTOM_CREATOR = 42;
            public static final int LAYOUT_CONSTRAINT_BOTTOM_TO_BOTTOM_OF = 15;
            public static final int LAYOUT_CONSTRAINT_BOTTOM_TO_TOP_OF = 14;
            public static final int LAYOUT_CONSTRAINT_CIRCLE = 2;
            public static final int LAYOUT_CONSTRAINT_CIRCLE_ANGLE = 4;
            public static final int LAYOUT_CONSTRAINT_CIRCLE_RADIUS = 3;
            public static final int LAYOUT_CONSTRAINT_DIMENSION_RATIO = 44;
            public static final int LAYOUT_CONSTRAINT_END_TO_END_OF = 20;
            public static final int LAYOUT_CONSTRAINT_END_TO_START_OF = 19;
            public static final int LAYOUT_CONSTRAINT_GUIDE_BEGIN = 5;
            public static final int LAYOUT_CONSTRAINT_GUIDE_END = 6;
            public static final int LAYOUT_CONSTRAINT_GUIDE_PERCENT = 7;
            public static final int LAYOUT_CONSTRAINT_HEIGHT = 65;
            public static final int LAYOUT_CONSTRAINT_HEIGHT_DEFAULT = 32;
            public static final int LAYOUT_CONSTRAINT_HEIGHT_MAX = 37;
            public static final int LAYOUT_CONSTRAINT_HEIGHT_MIN = 36;
            public static final int LAYOUT_CONSTRAINT_HEIGHT_PERCENT = 38;
            public static final int LAYOUT_CONSTRAINT_HORIZONTAL_BIAS = 29;
            public static final int LAYOUT_CONSTRAINT_HORIZONTAL_CHAINSTYLE = 47;
            public static final int LAYOUT_CONSTRAINT_HORIZONTAL_WEIGHT = 45;
            public static final int LAYOUT_CONSTRAINT_LEFT_CREATOR = 39;
            public static final int LAYOUT_CONSTRAINT_LEFT_TO_LEFT_OF = 8;
            public static final int LAYOUT_CONSTRAINT_LEFT_TO_RIGHT_OF = 9;
            public static final int LAYOUT_CONSTRAINT_RIGHT_CREATOR = 41;
            public static final int LAYOUT_CONSTRAINT_RIGHT_TO_LEFT_OF = 10;
            public static final int LAYOUT_CONSTRAINT_RIGHT_TO_RIGHT_OF = 11;
            public static final int LAYOUT_CONSTRAINT_START_TO_END_OF = 17;
            public static final int LAYOUT_CONSTRAINT_START_TO_START_OF = 18;
            public static final int LAYOUT_CONSTRAINT_TAG = 51;
            public static final int LAYOUT_CONSTRAINT_TOP_CREATOR = 40;
            public static final int LAYOUT_CONSTRAINT_TOP_TO_BOTTOM_OF = 13;
            public static final int LAYOUT_CONSTRAINT_TOP_TO_TOP_OF = 12;
            public static final int LAYOUT_CONSTRAINT_VERTICAL_BIAS = 30;
            public static final int LAYOUT_CONSTRAINT_VERTICAL_CHAINSTYLE = 48;
            public static final int LAYOUT_CONSTRAINT_VERTICAL_WEIGHT = 46;
            public static final int LAYOUT_CONSTRAINT_WIDTH = 64;
            public static final int LAYOUT_CONSTRAINT_WIDTH_DEFAULT = 31;
            public static final int LAYOUT_CONSTRAINT_WIDTH_MAX = 34;
            public static final int LAYOUT_CONSTRAINT_WIDTH_MIN = 33;
            public static final int LAYOUT_CONSTRAINT_WIDTH_PERCENT = 35;
            public static final int LAYOUT_EDITOR_ABSOLUTEX = 49;
            public static final int LAYOUT_EDITOR_ABSOLUTEY = 50;
            public static final int LAYOUT_GONE_MARGIN_BASELINE = 55;
            public static final int LAYOUT_GONE_MARGIN_BOTTOM = 24;
            public static final int LAYOUT_GONE_MARGIN_END = 26;
            public static final int LAYOUT_GONE_MARGIN_LEFT = 21;
            public static final int LAYOUT_GONE_MARGIN_RIGHT = 23;
            public static final int LAYOUT_GONE_MARGIN_START = 25;
            public static final int LAYOUT_GONE_MARGIN_TOP = 22;
            public static final int LAYOUT_MARGIN_BASELINE = 54;
            public static final int LAYOUT_WRAP_BEHAVIOR_IN_PARENT = 66;
            public static final int UNUSED = 0;
            public static final SparseIntArray map;
            
            static {
                final SparseIntArray map2 = new SparseIntArray();
                (map = map2).append(R.styleable.ConstraintLayout_Layout_layout_constraintWidth, 64);
                map2.append(R.styleable.ConstraintLayout_Layout_layout_constraintHeight, 65);
                map2.append(R.styleable.ConstraintLayout_Layout_layout_constraintLeft_toLeftOf, 8);
                map2.append(R.styleable.ConstraintLayout_Layout_layout_constraintLeft_toRightOf, 9);
                map2.append(R.styleable.ConstraintLayout_Layout_layout_constraintRight_toLeftOf, 10);
                map2.append(R.styleable.ConstraintLayout_Layout_layout_constraintRight_toRightOf, 11);
                map2.append(R.styleable.ConstraintLayout_Layout_layout_constraintTop_toTopOf, 12);
                map2.append(R.styleable.ConstraintLayout_Layout_layout_constraintTop_toBottomOf, 13);
                map2.append(R.styleable.ConstraintLayout_Layout_layout_constraintBottom_toTopOf, 14);
                map2.append(R.styleable.ConstraintLayout_Layout_layout_constraintBottom_toBottomOf, 15);
                map2.append(R.styleable.ConstraintLayout_Layout_layout_constraintBaseline_toBaselineOf, 16);
                map2.append(R.styleable.ConstraintLayout_Layout_layout_constraintBaseline_toTopOf, 52);
                map2.append(R.styleable.ConstraintLayout_Layout_layout_constraintBaseline_toBottomOf, 53);
                map2.append(R.styleable.ConstraintLayout_Layout_layout_constraintCircle, 2);
                map2.append(R.styleable.ConstraintLayout_Layout_layout_constraintCircleRadius, 3);
                map2.append(R.styleable.ConstraintLayout_Layout_layout_constraintCircleAngle, 4);
                map2.append(R.styleable.ConstraintLayout_Layout_layout_editor_absoluteX, 49);
                map2.append(R.styleable.ConstraintLayout_Layout_layout_editor_absoluteY, 50);
                map2.append(R.styleable.ConstraintLayout_Layout_layout_constraintGuide_begin, 5);
                map2.append(R.styleable.ConstraintLayout_Layout_layout_constraintGuide_end, 6);
                map2.append(R.styleable.ConstraintLayout_Layout_layout_constraintGuide_percent, 7);
                map2.append(R.styleable.ConstraintLayout_Layout_guidelineUseRtl, 67);
                map2.append(R.styleable.ConstraintLayout_Layout_android_orientation, 1);
                map2.append(R.styleable.ConstraintLayout_Layout_layout_constraintStart_toEndOf, 17);
                map2.append(R.styleable.ConstraintLayout_Layout_layout_constraintStart_toStartOf, 18);
                map2.append(R.styleable.ConstraintLayout_Layout_layout_constraintEnd_toStartOf, 19);
                map2.append(R.styleable.ConstraintLayout_Layout_layout_constraintEnd_toEndOf, 20);
                map2.append(R.styleable.ConstraintLayout_Layout_layout_goneMarginLeft, 21);
                map2.append(R.styleable.ConstraintLayout_Layout_layout_goneMarginTop, 22);
                map2.append(R.styleable.ConstraintLayout_Layout_layout_goneMarginRight, 23);
                map2.append(R.styleable.ConstraintLayout_Layout_layout_goneMarginBottom, 24);
                map2.append(R.styleable.ConstraintLayout_Layout_layout_goneMarginStart, 25);
                map2.append(R.styleable.ConstraintLayout_Layout_layout_goneMarginEnd, 26);
                map2.append(R.styleable.ConstraintLayout_Layout_layout_goneMarginBaseline, 55);
                map2.append(R.styleable.ConstraintLayout_Layout_layout_marginBaseline, 54);
                map2.append(R.styleable.ConstraintLayout_Layout_layout_constraintHorizontal_bias, 29);
                map2.append(R.styleable.ConstraintLayout_Layout_layout_constraintVertical_bias, 30);
                map2.append(R.styleable.ConstraintLayout_Layout_layout_constraintDimensionRatio, 44);
                map2.append(R.styleable.ConstraintLayout_Layout_layout_constraintHorizontal_weight, 45);
                map2.append(R.styleable.ConstraintLayout_Layout_layout_constraintVertical_weight, 46);
                map2.append(R.styleable.ConstraintLayout_Layout_layout_constraintHorizontal_chainStyle, 47);
                map2.append(R.styleable.ConstraintLayout_Layout_layout_constraintVertical_chainStyle, 48);
                map2.append(R.styleable.ConstraintLayout_Layout_layout_constrainedWidth, 27);
                map2.append(R.styleable.ConstraintLayout_Layout_layout_constrainedHeight, 28);
                map2.append(R.styleable.ConstraintLayout_Layout_layout_constraintWidth_default, 31);
                map2.append(R.styleable.ConstraintLayout_Layout_layout_constraintHeight_default, 32);
                map2.append(R.styleable.ConstraintLayout_Layout_layout_constraintWidth_min, 33);
                map2.append(R.styleable.ConstraintLayout_Layout_layout_constraintWidth_max, 34);
                map2.append(R.styleable.ConstraintLayout_Layout_layout_constraintWidth_percent, 35);
                map2.append(R.styleable.ConstraintLayout_Layout_layout_constraintHeight_min, 36);
                map2.append(R.styleable.ConstraintLayout_Layout_layout_constraintHeight_max, 37);
                map2.append(R.styleable.ConstraintLayout_Layout_layout_constraintHeight_percent, 38);
                map2.append(R.styleable.ConstraintLayout_Layout_layout_constraintLeft_creator, 39);
                map2.append(R.styleable.ConstraintLayout_Layout_layout_constraintTop_creator, 40);
                map2.append(R.styleable.ConstraintLayout_Layout_layout_constraintRight_creator, 41);
                map2.append(R.styleable.ConstraintLayout_Layout_layout_constraintBottom_creator, 42);
                map2.append(R.styleable.ConstraintLayout_Layout_layout_constraintBaseline_creator, 43);
                map2.append(R.styleable.ConstraintLayout_Layout_layout_constraintTag, 51);
                map2.append(R.styleable.ConstraintLayout_Layout_layout_wrapBehaviorInParent, 66);
            }
        }
    }
    
    class Measurer implements BasicMeasure.Measurer
    {
        ConstraintLayout layout;
        int layoutHeightSpec;
        int layoutWidthSpec;
        int paddingBottom;
        int paddingHeight;
        int paddingTop;
        int paddingWidth;
        final ConstraintLayout this$0;
        
        public Measurer(final ConstraintLayout this$0, final ConstraintLayout layout) {
            this.this$0 = this$0;
            this.layout = layout;
        }
        
        private boolean isSimilarSpec(int mode, int size, final int n) {
            if (mode == size) {
                return true;
            }
            final int mode2 = View$MeasureSpec.getMode(mode);
            View$MeasureSpec.getSize(mode);
            mode = View$MeasureSpec.getMode(size);
            size = View$MeasureSpec.getSize(size);
            return mode == 1073741824 && (mode2 == Integer.MIN_VALUE || mode2 == 0) && n == size;
        }
        
        public void captureLayoutInfo(final int layoutWidthSpec, final int layoutHeightSpec, final int paddingTop, final int paddingBottom, final int paddingWidth, final int paddingHeight) {
            this.paddingTop = paddingTop;
            this.paddingBottom = paddingBottom;
            this.paddingWidth = paddingWidth;
            this.paddingHeight = paddingHeight;
            this.layoutWidthSpec = layoutWidthSpec;
            this.layoutHeightSpec = layoutHeightSpec;
        }
        
        @Override
        public final void didMeasures() {
            final int childCount = this.layout.getChildCount();
            final int n = 0;
            for (int i = 0; i < childCount; ++i) {
                final View child = this.layout.getChildAt(i);
                if (child instanceof Placeholder) {
                    ((Placeholder)child).updatePostMeasure(this.layout);
                }
            }
            final int size = this.layout.mConstraintHelpers.size();
            if (size > 0) {
                for (int j = n; j < size; ++j) {
                    ((ConstraintHelper)this.layout.mConstraintHelpers.get(j)).updatePostMeasure(this.layout);
                }
            }
        }
        
        @Override
        public final void measure(final ConstraintWidget constraintWidget, final Measure measure) {
            if (constraintWidget == null) {
                return;
            }
            if (constraintWidget.getVisibility() == 8 && !constraintWidget.isInPlaceholder()) {
                measure.measuredWidth = 0;
                measure.measuredHeight = 0;
                measure.measuredBaseline = 0;
                return;
            }
            if (constraintWidget.getParent() == null) {
                return;
            }
            final ConstraintWidget.DimensionBehaviour horizontalBehavior = measure.horizontalBehavior;
            final ConstraintWidget.DimensionBehaviour verticalBehavior = measure.verticalBehavior;
            final int horizontalDimension = measure.horizontalDimension;
            final int verticalDimension = measure.verticalDimension;
            final int n = this.paddingTop + this.paddingBottom;
            final int paddingWidth = this.paddingWidth;
            final View view = (View)constraintWidget.getCompanionWidget();
            final int n2 = ConstraintLayout$1.$SwitchMap$androidx$constraintlayout$core$widgets$ConstraintWidget$DimensionBehaviour[horizontalBehavior.ordinal()];
            int n3 = 0;
            Label_0331: {
                if (n2 != 1) {
                    if (n2 != 2) {
                        if (n2 != 3) {
                            if (n2 != 4) {
                                n3 = 0;
                            }
                            else {
                                final int childMeasureSpec = ViewGroup.getChildMeasureSpec(this.layoutWidthSpec, paddingWidth, -2);
                                final boolean b = constraintWidget.mMatchConstraintDefaultWidth == 1;
                                if (measure.measureStrategy != Measure.TRY_GIVEN_DIMENSIONS) {
                                    n3 = childMeasureSpec;
                                    if (measure.measureStrategy != Measure.USE_GIVEN_DIMENSIONS) {
                                        break Label_0331;
                                    }
                                }
                                final boolean b2 = view.getMeasuredHeight() == constraintWidget.getHeight();
                                final boolean b3 = measure.measureStrategy == Measure.USE_GIVEN_DIMENSIONS || !b || (b && b2) || view instanceof Placeholder || constraintWidget.isResolvedHorizontally();
                                n3 = childMeasureSpec;
                                if (b3) {
                                    n3 = View$MeasureSpec.makeMeasureSpec(constraintWidget.getWidth(), 1073741824);
                                }
                            }
                        }
                        else {
                            n3 = ViewGroup.getChildMeasureSpec(this.layoutWidthSpec, paddingWidth + constraintWidget.getHorizontalMargin(), -1);
                        }
                    }
                    else {
                        n3 = ViewGroup.getChildMeasureSpec(this.layoutWidthSpec, paddingWidth, -2);
                    }
                }
                else {
                    n3 = View$MeasureSpec.makeMeasureSpec(horizontalDimension, 1073741824);
                }
            }
            final int n4 = ConstraintLayout$1.$SwitchMap$androidx$constraintlayout$core$widgets$ConstraintWidget$DimensionBehaviour[verticalBehavior.ordinal()];
            int n5 = 0;
            Label_0567: {
                if (n4 != 1) {
                    if (n4 != 2) {
                        if (n4 != 3) {
                            if (n4 != 4) {
                                n5 = 0;
                            }
                            else {
                                final int childMeasureSpec2 = ViewGroup.getChildMeasureSpec(this.layoutHeightSpec, n, -2);
                                final boolean b4 = constraintWidget.mMatchConstraintDefaultHeight == 1;
                                if (measure.measureStrategy != Measure.TRY_GIVEN_DIMENSIONS) {
                                    n5 = childMeasureSpec2;
                                    if (measure.measureStrategy != Measure.USE_GIVEN_DIMENSIONS) {
                                        break Label_0567;
                                    }
                                }
                                final boolean b5 = view.getMeasuredWidth() == constraintWidget.getWidth();
                                final boolean b6 = measure.measureStrategy == Measure.USE_GIVEN_DIMENSIONS || !b4 || (b4 && b5) || view instanceof Placeholder || constraintWidget.isResolvedVertically();
                                n5 = childMeasureSpec2;
                                if (b6) {
                                    n5 = View$MeasureSpec.makeMeasureSpec(constraintWidget.getHeight(), 1073741824);
                                }
                            }
                        }
                        else {
                            n5 = ViewGroup.getChildMeasureSpec(this.layoutHeightSpec, n + constraintWidget.getVerticalMargin(), -1);
                        }
                    }
                    else {
                        n5 = ViewGroup.getChildMeasureSpec(this.layoutHeightSpec, n, -2);
                    }
                }
                else {
                    n5 = View$MeasureSpec.makeMeasureSpec(verticalDimension, 1073741824);
                }
            }
            final ConstraintWidgetContainer constraintWidgetContainer = (ConstraintWidgetContainer)constraintWidget.getParent();
            if (constraintWidgetContainer != null && Optimizer.enabled(this.this$0.mOptimizationLevel, 256) && view.getMeasuredWidth() == constraintWidget.getWidth() && view.getMeasuredWidth() < constraintWidgetContainer.getWidth() && view.getMeasuredHeight() == constraintWidget.getHeight() && view.getMeasuredHeight() < constraintWidgetContainer.getHeight() && view.getBaseline() == constraintWidget.getBaselineDistance() && !constraintWidget.isMeasureRequested() && (this.isSimilarSpec(constraintWidget.getLastHorizontalMeasureSpec(), n3, constraintWidget.getWidth()) && this.isSimilarSpec(constraintWidget.getLastVerticalMeasureSpec(), n5, constraintWidget.getHeight()))) {
                measure.measuredWidth = constraintWidget.getWidth();
                measure.measuredHeight = constraintWidget.getHeight();
                measure.measuredBaseline = constraintWidget.getBaselineDistance();
                return;
            }
            final boolean b7 = horizontalBehavior == ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT;
            final boolean b8 = verticalBehavior == ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT;
            final boolean b9 = verticalBehavior == ConstraintWidget.DimensionBehaviour.MATCH_PARENT || verticalBehavior == ConstraintWidget.DimensionBehaviour.FIXED;
            final boolean b10 = horizontalBehavior == ConstraintWidget.DimensionBehaviour.MATCH_PARENT || horizontalBehavior == ConstraintWidget.DimensionBehaviour.FIXED;
            final boolean b11 = b7 && constraintWidget.mDimensionRatio > 0.0f;
            final boolean b12 = b8 && constraintWidget.mDimensionRatio > 0.0f;
            if (view == null) {
                return;
            }
            final LayoutParams layoutParams = (LayoutParams)view.getLayoutParams();
            int baseline = 0;
            int measuredWidth = 0;
            int measuredHeight = 0;
            Label_1331: {
                if (measure.measureStrategy != Measure.TRY_GIVEN_DIMENSIONS && measure.measureStrategy != Measure.USE_GIVEN_DIMENSIONS && b7 && constraintWidget.mMatchConstraintDefaultWidth == 0 && b8 && constraintWidget.mMatchConstraintDefaultHeight == 0) {
                    baseline = 0;
                    measuredWidth = 0;
                    measuredHeight = 0;
                }
                else {
                    if (view instanceof VirtualLayout && constraintWidget instanceof androidx.constraintlayout.core.widgets.VirtualLayout) {
                        ((VirtualLayout)view).onMeasure((androidx.constraintlayout.core.widgets.VirtualLayout)constraintWidget, n3, n5);
                    }
                    else {
                        view.measure(n3, n5);
                    }
                    constraintWidget.setLastMeasureSpec(n3, n5);
                    final int measuredWidth2 = view.getMeasuredWidth();
                    final int measuredHeight2 = view.getMeasuredHeight();
                    final int baseline2 = view.getBaseline();
                    int max;
                    if (constraintWidget.mMatchConstraintMinWidth > 0) {
                        max = Math.max(constraintWidget.mMatchConstraintMinWidth, measuredWidth2);
                    }
                    else {
                        max = measuredWidth2;
                    }
                    int min = max;
                    if (constraintWidget.mMatchConstraintMaxWidth > 0) {
                        min = Math.min(constraintWidget.mMatchConstraintMaxWidth, max);
                    }
                    int max2;
                    if (constraintWidget.mMatchConstraintMinHeight > 0) {
                        max2 = Math.max(constraintWidget.mMatchConstraintMinHeight, measuredHeight2);
                    }
                    else {
                        max2 = measuredHeight2;
                    }
                    int min2 = max2;
                    if (constraintWidget.mMatchConstraintMaxHeight > 0) {
                        min2 = Math.min(constraintWidget.mMatchConstraintMaxHeight, max2);
                    }
                    int n6 = min;
                    int n7 = min2;
                    if (!Optimizer.enabled(this.this$0.mOptimizationLevel, 1)) {
                        if (b11 && b9) {
                            n6 = (int)(min2 * constraintWidget.mDimensionRatio + 0.5f);
                            n7 = min2;
                        }
                        else {
                            n6 = min;
                            n7 = min2;
                            if (b12) {
                                n6 = min;
                                n7 = min2;
                                if (b10) {
                                    n7 = (int)(min / constraintWidget.mDimensionRatio + 0.5f);
                                    n6 = min;
                                }
                            }
                        }
                    }
                    Label_1255: {
                        if (measuredWidth2 != n6) {
                            break Label_1255;
                        }
                        baseline = baseline2;
                        measuredWidth = n6;
                        if (measuredHeight2 != (measuredHeight = n7)) {
                            break Label_1255;
                        }
                        break Label_1331;
                    }
                    if (measuredWidth2 != n6) {
                        n3 = View$MeasureSpec.makeMeasureSpec(n6, 1073741824);
                    }
                    if (measuredHeight2 != n7) {
                        n5 = View$MeasureSpec.makeMeasureSpec(n7, 1073741824);
                    }
                    view.measure(n3, n5);
                    constraintWidget.setLastMeasureSpec(n3, n5);
                    measuredWidth = view.getMeasuredWidth();
                    measuredHeight = view.getMeasuredHeight();
                    baseline = view.getBaseline();
                }
            }
            boolean measuredHasBaseline = baseline != -1;
            measure.measuredNeedsSolverPass = (measuredWidth != measure.horizontalDimension || measuredHeight != measure.verticalDimension);
            if (layoutParams.needsBaseline) {
                measuredHasBaseline = true;
            }
            if (measuredHasBaseline && baseline != -1 && constraintWidget.getBaselineDistance() != baseline) {
                measure.measuredNeedsSolverPass = true;
            }
            measure.measuredWidth = measuredWidth;
            measure.measuredHeight = measuredHeight;
            measure.measuredHasBaseline = measuredHasBaseline;
            measure.measuredBaseline = baseline;
        }
    }
}

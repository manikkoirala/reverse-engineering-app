// 
// Decompiled by Procyon v0.6.0
// 

package androidx.customview.poolingcontainer;

import android.view.ViewParent;
import androidx.core.view.ViewGroupKt;
import android.view.ViewGroup;
import java.util.Iterator;
import androidx.core.view.ViewKt;
import kotlin.jvm.internal.Intrinsics;
import android.view.View;
import kotlin.Metadata;

@Metadata(d1 = { "\u00006\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\u001a\u0014\u0010\u000f\u001a\u00020\u0010*\u00020\u00062\u0006\u0010\u0011\u001a\u00020\u0012H\u0007\u001a\n\u0010\u0013\u001a\u00020\u0010*\u00020\u0006\u001a\n\u0010\u0014\u001a\u00020\u0010*\u00020\u0015\u001a\u0014\u0010\u0016\u001a\u00020\u0010*\u00020\u00062\u0006\u0010\u0011\u001a\u00020\u0012H\u0007\"\u000e\u0010\u0000\u001a\u00020\u0001X\u0082\u0004¢\u0006\u0002\n\u0000\"\u000e\u0010\u0002\u001a\u00020\u0001X\u0082\u0004¢\u0006\u0002\n\u0000\"(\u0010\u0005\u001a\u00020\u0004*\u00020\u00062\u0006\u0010\u0003\u001a\u00020\u00048F@FX\u0086\u000e¢\u0006\f\u001a\u0004\b\u0005\u0010\u0007\"\u0004\b\b\u0010\t\"\u0015\u0010\n\u001a\u00020\u0004*\u00020\u00068F¢\u0006\u0006\u001a\u0004\b\n\u0010\u0007\"\u0018\u0010\u000b\u001a\u00020\f*\u00020\u00068BX\u0082\u0004¢\u0006\u0006\u001a\u0004\b\r\u0010\u000e¨\u0006\u0017" }, d2 = { "IsPoolingContainerTag", "", "PoolingContainerListenerHolderTag", "value", "", "isPoolingContainer", "Landroid/view/View;", "(Landroid/view/View;)Z", "setPoolingContainer", "(Landroid/view/View;Z)V", "isWithinPoolingContainer", "poolingContainerListenerHolder", "Landroidx/customview/poolingcontainer/PoolingContainerListenerHolder;", "getPoolingContainerListenerHolder", "(Landroid/view/View;)Landroidx/customview/poolingcontainer/PoolingContainerListenerHolder;", "addPoolingContainerListener", "", "listener", "Landroidx/customview/poolingcontainer/PoolingContainerListener;", "callPoolingContainerOnRelease", "callPoolingContainerOnReleaseForChildren", "Landroid/view/ViewGroup;", "removePoolingContainerListener", "customview-poolingcontainer_release" }, k = 2, mv = { 1, 6, 0 }, xi = 48)
public final class PoolingContainer
{
    private static final int IsPoolingContainerTag;
    private static final int PoolingContainerListenerHolderTag;
    
    static {
        PoolingContainerListenerHolderTag = R.id.pooling_container_listener_holder_tag;
        IsPoolingContainerTag = R.id.is_pooling_container_tag;
    }
    
    public static final void addPoolingContainerListener(final View view, final PoolingContainerListener poolingContainerListener) {
        Intrinsics.checkNotNullParameter((Object)view, "<this>");
        Intrinsics.checkNotNullParameter((Object)poolingContainerListener, "listener");
        getPoolingContainerListenerHolder(view).addListener(poolingContainerListener);
    }
    
    public static final void callPoolingContainerOnRelease(final View view) {
        Intrinsics.checkNotNullParameter((Object)view, "<this>");
        final Iterator iterator = ViewKt.getAllViews(view).iterator();
        while (iterator.hasNext()) {
            getPoolingContainerListenerHolder((View)iterator.next()).onRelease();
        }
    }
    
    public static final void callPoolingContainerOnReleaseForChildren(final ViewGroup viewGroup) {
        Intrinsics.checkNotNullParameter((Object)viewGroup, "<this>");
        final Iterator iterator = ViewGroupKt.getChildren(viewGroup).iterator();
        while (iterator.hasNext()) {
            getPoolingContainerListenerHolder((View)iterator.next()).onRelease();
        }
    }
    
    private static final PoolingContainerListenerHolder getPoolingContainerListenerHolder(final View view) {
        final int poolingContainerListenerHolderTag = PoolingContainer.PoolingContainerListenerHolderTag;
        PoolingContainerListenerHolder poolingContainerListenerHolder;
        if ((poolingContainerListenerHolder = (PoolingContainerListenerHolder)view.getTag(poolingContainerListenerHolderTag)) == null) {
            poolingContainerListenerHolder = new PoolingContainerListenerHolder();
            view.setTag(poolingContainerListenerHolderTag, (Object)poolingContainerListenerHolder);
        }
        return poolingContainerListenerHolder;
    }
    
    public static final boolean isPoolingContainer(final View view) {
        Intrinsics.checkNotNullParameter((Object)view, "<this>");
        final Object tag = view.getTag(PoolingContainer.IsPoolingContainerTag);
        Boolean b;
        if (tag instanceof Boolean) {
            b = (Boolean)tag;
        }
        else {
            b = null;
        }
        return b != null && b;
    }
    
    public static final boolean isWithinPoolingContainer(final View view) {
        Intrinsics.checkNotNullParameter((Object)view, "<this>");
        for (final ViewParent viewParent : ViewKt.getAncestors(view)) {
            if (viewParent instanceof View && isPoolingContainer((View)viewParent)) {
                return true;
            }
        }
        return false;
    }
    
    public static final void removePoolingContainerListener(final View view, final PoolingContainerListener poolingContainerListener) {
        Intrinsics.checkNotNullParameter((Object)view, "<this>");
        Intrinsics.checkNotNullParameter((Object)poolingContainerListener, "listener");
        getPoolingContainerListenerHolder(view).removeListener(poolingContainerListener);
    }
    
    public static final void setPoolingContainer(final View view, final boolean b) {
        Intrinsics.checkNotNullParameter((Object)view, "<this>");
        view.setTag(PoolingContainer.IsPoolingContainerTag, (Object)b);
    }
}

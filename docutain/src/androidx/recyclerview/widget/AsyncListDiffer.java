// 
// Decompiled by Procyon v0.6.0
// 

package androidx.recyclerview.widget;

import android.os.Looper;
import android.os.Handler;
import java.util.Iterator;
import java.util.Collections;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.List;
import java.util.concurrent.Executor;

public class AsyncListDiffer<T>
{
    private static final Executor sMainThreadExecutor;
    final AsyncDifferConfig<T> mConfig;
    private List<T> mList;
    private final List<ListListener<T>> mListeners;
    Executor mMainThreadExecutor;
    int mMaxScheduledGeneration;
    private List<T> mReadOnlyList;
    private final ListUpdateCallback mUpdateCallback;
    
    static {
        sMainThreadExecutor = new MainThreadExecutor();
    }
    
    public AsyncListDiffer(final ListUpdateCallback mUpdateCallback, final AsyncDifferConfig<T> mConfig) {
        this.mListeners = new CopyOnWriteArrayList<ListListener<T>>();
        this.mReadOnlyList = Collections.emptyList();
        this.mUpdateCallback = mUpdateCallback;
        this.mConfig = mConfig;
        if (mConfig.getMainThreadExecutor() != null) {
            this.mMainThreadExecutor = mConfig.getMainThreadExecutor();
        }
        else {
            this.mMainThreadExecutor = AsyncListDiffer.sMainThreadExecutor;
        }
    }
    
    public AsyncListDiffer(final RecyclerView.Adapter adapter, final DiffUtil.ItemCallback<T> itemCallback) {
        this(new AdapterListUpdateCallback(adapter), new AsyncDifferConfig.Builder<T>(itemCallback).build());
    }
    
    private void onCurrentListChanged(final List<T> list, final Runnable runnable) {
        final Iterator<ListListener<T>> iterator = this.mListeners.iterator();
        while (iterator.hasNext()) {
            iterator.next().onCurrentListChanged(list, this.mReadOnlyList);
        }
        if (runnable != null) {
            runnable.run();
        }
    }
    
    public void addListListener(final ListListener<T> listListener) {
        this.mListeners.add(listListener);
    }
    
    public List<T> getCurrentList() {
        return this.mReadOnlyList;
    }
    
    void latchList(final List<T> list, final DiffUtil.DiffResult diffResult, final Runnable runnable) {
        final List<T> mReadOnlyList = this.mReadOnlyList;
        this.mList = list;
        this.mReadOnlyList = Collections.unmodifiableList((List<? extends T>)list);
        diffResult.dispatchUpdatesTo(this.mUpdateCallback);
        this.onCurrentListChanged(mReadOnlyList, runnable);
    }
    
    public void removeListListener(final ListListener<T> listListener) {
        this.mListeners.remove(listListener);
    }
    
    public void submitList(final List<T> list) {
        this.submitList(list, null);
    }
    
    public void submitList(final List<T> list, final Runnable runnable) {
        final int mMaxScheduledGeneration = this.mMaxScheduledGeneration + 1;
        this.mMaxScheduledGeneration = mMaxScheduledGeneration;
        final List<T> mList = this.mList;
        if (list == mList) {
            if (runnable != null) {
                runnable.run();
            }
            return;
        }
        final List<T> mReadOnlyList = this.mReadOnlyList;
        if (list == null) {
            final int size = mList.size();
            this.mList = null;
            this.mReadOnlyList = Collections.emptyList();
            this.mUpdateCallback.onRemoved(0, size);
            this.onCurrentListChanged(mReadOnlyList, runnable);
            return;
        }
        if (mList == null) {
            this.mList = list;
            this.mReadOnlyList = Collections.unmodifiableList((List<? extends T>)list);
            this.mUpdateCallback.onInserted(0, list.size());
            this.onCurrentListChanged(mReadOnlyList, runnable);
            return;
        }
        this.mConfig.getBackgroundThreadExecutor().execute(new Runnable(this, mList, list, mMaxScheduledGeneration, runnable) {
            final AsyncListDiffer this$0;
            final Runnable val$commitCallback;
            final List val$newList;
            final List val$oldList;
            final int val$runGeneration;
            
            @Override
            public void run() {
                this.this$0.mMainThreadExecutor.execute(new Runnable(this, DiffUtil.calculateDiff((DiffUtil.Callback)new DiffUtil.Callback(this) {
                    final AsyncListDiffer$1 this$1;
                    
                    @Override
                    public boolean areContentsTheSame(final int n, final int n2) {
                        final T value = this.this$1.val$oldList.get(n);
                        final T value2 = this.this$1.val$newList.get(n2);
                        if (value != null && value2 != null) {
                            return this.this$1.this$0.mConfig.getDiffCallback().areContentsTheSame(value, value2);
                        }
                        if (value == null && value2 == null) {
                            return true;
                        }
                        throw new AssertionError();
                    }
                    
                    @Override
                    public boolean areItemsTheSame(final int n, final int n2) {
                        final T value = this.this$1.val$oldList.get(n);
                        final T value2 = this.this$1.val$newList.get(n2);
                        if (value != null && value2 != null) {
                            return this.this$1.this$0.mConfig.getDiffCallback().areItemsTheSame(value, value2);
                        }
                        return value == null && value2 == null;
                    }
                    
                    @Override
                    public Object getChangePayload(final int n, final int n2) {
                        final T value = this.this$1.val$oldList.get(n);
                        final T value2 = this.this$1.val$newList.get(n2);
                        if (value != null && value2 != null) {
                            return this.this$1.this$0.mConfig.getDiffCallback().getChangePayload(value, value2);
                        }
                        throw new AssertionError();
                    }
                    
                    @Override
                    public int getNewListSize() {
                        return this.this$1.val$newList.size();
                    }
                    
                    @Override
                    public int getOldListSize() {
                        return this.this$1.val$oldList.size();
                    }
                })) {
                    final AsyncListDiffer$1 this$1;
                    final DiffUtil.DiffResult val$result;
                    
                    @Override
                    public void run() {
                        if (this.this$1.this$0.mMaxScheduledGeneration == this.this$1.val$runGeneration) {
                            this.this$1.this$0.latchList(this.this$1.val$newList, this.val$result, this.this$1.val$commitCallback);
                        }
                    }
                });
            }
        });
    }
    
    public interface ListListener<T>
    {
        void onCurrentListChanged(final List<T> p0, final List<T> p1);
    }
    
    private static class MainThreadExecutor implements Executor
    {
        final Handler mHandler;
        
        MainThreadExecutor() {
            this.mHandler = new Handler(Looper.getMainLooper());
        }
        
        @Override
        public void execute(final Runnable runnable) {
            this.mHandler.post(runnable);
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.recyclerview.widget;

import android.view.ViewGroup;
import androidx.core.util.Preconditions;

class NestedAdapterWrapper
{
    public final RecyclerView.Adapter<RecyclerView.ViewHolder> adapter;
    private RecyclerView.AdapterDataObserver mAdapterObserver;
    int mCachedItemCount;
    final Callback mCallback;
    private final StableIdStorage.StableIdLookup mStableIdLookup;
    private final ViewTypeStorage.ViewTypeLookup mViewTypeLookup;
    
    NestedAdapterWrapper(final RecyclerView.Adapter<RecyclerView.ViewHolder> adapter, final Callback mCallback, final ViewTypeStorage viewTypeStorage, final StableIdStorage.StableIdLookup mStableIdLookup) {
        this.mAdapterObserver = new RecyclerView.AdapterDataObserver() {
            final NestedAdapterWrapper this$0;
            
            @Override
            public void onChanged() {
                final NestedAdapterWrapper this$0 = this.this$0;
                this$0.mCachedItemCount = this$0.adapter.getItemCount();
                this.this$0.mCallback.onChanged(this.this$0);
            }
            
            @Override
            public void onItemRangeChanged(final int n, final int n2) {
                this.this$0.mCallback.onItemRangeChanged(this.this$0, n, n2, null);
            }
            
            @Override
            public void onItemRangeChanged(final int n, final int n2, final Object o) {
                this.this$0.mCallback.onItemRangeChanged(this.this$0, n, n2, o);
            }
            
            @Override
            public void onItemRangeInserted(final int n, final int n2) {
                final NestedAdapterWrapper this$0 = this.this$0;
                this$0.mCachedItemCount += n2;
                this.this$0.mCallback.onItemRangeInserted(this.this$0, n, n2);
                if (this.this$0.mCachedItemCount > 0 && this.this$0.adapter.getStateRestorationPolicy() == StateRestorationPolicy.PREVENT_WHEN_EMPTY) {
                    this.this$0.mCallback.onStateRestorationPolicyChanged(this.this$0);
                }
            }
            
            @Override
            public void onItemRangeMoved(final int n, final int n2, final int n3) {
                boolean b = true;
                if (n3 != 1) {
                    b = false;
                }
                Preconditions.checkArgument(b, (Object)"moving more than 1 item is not supported in RecyclerView");
                this.this$0.mCallback.onItemRangeMoved(this.this$0, n, n2);
            }
            
            @Override
            public void onItemRangeRemoved(final int n, final int n2) {
                final NestedAdapterWrapper this$0 = this.this$0;
                this$0.mCachedItemCount -= n2;
                this.this$0.mCallback.onItemRangeRemoved(this.this$0, n, n2);
                if (this.this$0.mCachedItemCount < 1 && this.this$0.adapter.getStateRestorationPolicy() == StateRestorationPolicy.PREVENT_WHEN_EMPTY) {
                    this.this$0.mCallback.onStateRestorationPolicyChanged(this.this$0);
                }
            }
            
            @Override
            public void onStateRestorationPolicyChanged() {
                this.this$0.mCallback.onStateRestorationPolicyChanged(this.this$0);
            }
        };
        this.adapter = adapter;
        this.mCallback = mCallback;
        this.mViewTypeLookup = viewTypeStorage.createViewTypeWrapper(this);
        this.mStableIdLookup = mStableIdLookup;
        this.mCachedItemCount = adapter.getItemCount();
        adapter.registerAdapterDataObserver(this.mAdapterObserver);
    }
    
    void dispose() {
        this.adapter.unregisterAdapterDataObserver(this.mAdapterObserver);
        this.mViewTypeLookup.dispose();
    }
    
    int getCachedItemCount() {
        return this.mCachedItemCount;
    }
    
    public long getItemId(final int n) {
        return this.mStableIdLookup.localToGlobal(this.adapter.getItemId(n));
    }
    
    int getItemViewType(final int n) {
        return this.mViewTypeLookup.localToGlobal(this.adapter.getItemViewType(n));
    }
    
    void onBindViewHolder(final RecyclerView.ViewHolder viewHolder, final int n) {
        this.adapter.bindViewHolder(viewHolder, n);
    }
    
    RecyclerView.ViewHolder onCreateViewHolder(final ViewGroup viewGroup, int globalToLocal) {
        globalToLocal = this.mViewTypeLookup.globalToLocal(globalToLocal);
        return this.adapter.onCreateViewHolder(viewGroup, globalToLocal);
    }
    
    interface Callback
    {
        void onChanged(final NestedAdapterWrapper p0);
        
        void onItemRangeChanged(final NestedAdapterWrapper p0, final int p1, final int p2);
        
        void onItemRangeChanged(final NestedAdapterWrapper p0, final int p1, final int p2, final Object p3);
        
        void onItemRangeInserted(final NestedAdapterWrapper p0, final int p1, final int p2);
        
        void onItemRangeMoved(final NestedAdapterWrapper p0, final int p1, final int p2);
        
        void onItemRangeRemoved(final NestedAdapterWrapper p0, final int p1, final int p2);
        
        void onStateRestorationPolicyChanged(final NestedAdapterWrapper p0);
    }
}

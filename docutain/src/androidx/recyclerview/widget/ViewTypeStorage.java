// 
// Decompiled by Procyon v0.6.0
// 

package androidx.recyclerview.widget;

import java.util.ArrayList;
import java.util.List;
import android.util.SparseIntArray;
import android.util.SparseArray;

interface ViewTypeStorage
{
    ViewTypeLookup createViewTypeWrapper(final NestedAdapterWrapper p0);
    
    NestedAdapterWrapper getWrapperForGlobalType(final int p0);
    
    public static class IsolatedViewTypeStorage implements ViewTypeStorage
    {
        SparseArray<NestedAdapterWrapper> mGlobalTypeToWrapper;
        int mNextViewType;
        
        public IsolatedViewTypeStorage() {
            this.mGlobalTypeToWrapper = (SparseArray<NestedAdapterWrapper>)new SparseArray();
            this.mNextViewType = 0;
        }
        
        @Override
        public ViewTypeLookup createViewTypeWrapper(final NestedAdapterWrapper nestedAdapterWrapper) {
            return new WrapperViewTypeLookup(nestedAdapterWrapper);
        }
        
        @Override
        public NestedAdapterWrapper getWrapperForGlobalType(final int i) {
            final NestedAdapterWrapper nestedAdapterWrapper = (NestedAdapterWrapper)this.mGlobalTypeToWrapper.get(i);
            if (nestedAdapterWrapper != null) {
                return nestedAdapterWrapper;
            }
            final StringBuilder sb = new StringBuilder();
            sb.append("Cannot find the wrapper for global view type ");
            sb.append(i);
            throw new IllegalArgumentException(sb.toString());
        }
        
        int obtainViewType(final NestedAdapterWrapper nestedAdapterWrapper) {
            final int n = this.mNextViewType++;
            this.mGlobalTypeToWrapper.put(n, (Object)nestedAdapterWrapper);
            return n;
        }
        
        void removeWrapper(final NestedAdapterWrapper nestedAdapterWrapper) {
            for (int i = this.mGlobalTypeToWrapper.size() - 1; i >= 0; --i) {
                if (this.mGlobalTypeToWrapper.valueAt(i) == nestedAdapterWrapper) {
                    this.mGlobalTypeToWrapper.removeAt(i);
                }
            }
        }
        
        class WrapperViewTypeLookup implements ViewTypeLookup
        {
            private SparseIntArray mGlobalToLocalMapping;
            private SparseIntArray mLocalToGlobalMapping;
            final NestedAdapterWrapper mWrapper;
            final IsolatedViewTypeStorage this$0;
            
            WrapperViewTypeLookup(final IsolatedViewTypeStorage this$0, final NestedAdapterWrapper mWrapper) {
                this.this$0 = this$0;
                this.mLocalToGlobalMapping = new SparseIntArray(1);
                this.mGlobalToLocalMapping = new SparseIntArray(1);
                this.mWrapper = mWrapper;
            }
            
            @Override
            public void dispose() {
                this.this$0.removeWrapper(this.mWrapper);
            }
            
            @Override
            public int globalToLocal(final int i) {
                final int indexOfKey = this.mGlobalToLocalMapping.indexOfKey(i);
                if (indexOfKey >= 0) {
                    return this.mGlobalToLocalMapping.valueAt(indexOfKey);
                }
                final StringBuilder sb = new StringBuilder();
                sb.append("requested global type ");
                sb.append(i);
                sb.append(" does not belong to the adapter:");
                sb.append(this.mWrapper.adapter);
                throw new IllegalStateException(sb.toString());
            }
            
            @Override
            public int localToGlobal(final int n) {
                final int indexOfKey = this.mLocalToGlobalMapping.indexOfKey(n);
                if (indexOfKey > -1) {
                    return this.mLocalToGlobalMapping.valueAt(indexOfKey);
                }
                final int obtainViewType = this.this$0.obtainViewType(this.mWrapper);
                this.mLocalToGlobalMapping.put(n, obtainViewType);
                this.mGlobalToLocalMapping.put(obtainViewType, n);
                return obtainViewType;
            }
        }
    }
    
    public static class SharedIdRangeViewTypeStorage implements ViewTypeStorage
    {
        SparseArray<List<NestedAdapterWrapper>> mGlobalTypeToWrapper;
        
        public SharedIdRangeViewTypeStorage() {
            this.mGlobalTypeToWrapper = (SparseArray<List<NestedAdapterWrapper>>)new SparseArray();
        }
        
        @Override
        public ViewTypeLookup createViewTypeWrapper(final NestedAdapterWrapper nestedAdapterWrapper) {
            return new WrapperViewTypeLookup(nestedAdapterWrapper);
        }
        
        @Override
        public NestedAdapterWrapper getWrapperForGlobalType(final int i) {
            final List list = (List)this.mGlobalTypeToWrapper.get(i);
            if (list != null && !list.isEmpty()) {
                return (NestedAdapterWrapper)list.get(0);
            }
            final StringBuilder sb = new StringBuilder();
            sb.append("Cannot find the wrapper for global view type ");
            sb.append(i);
            throw new IllegalArgumentException(sb.toString());
        }
        
        void removeWrapper(final NestedAdapterWrapper nestedAdapterWrapper) {
            for (int i = this.mGlobalTypeToWrapper.size() - 1; i >= 0; --i) {
                final List list = (List)this.mGlobalTypeToWrapper.valueAt(i);
                if (list.remove(nestedAdapterWrapper) && list.isEmpty()) {
                    this.mGlobalTypeToWrapper.removeAt(i);
                }
            }
        }
        
        class WrapperViewTypeLookup implements ViewTypeLookup
        {
            final NestedAdapterWrapper mWrapper;
            final SharedIdRangeViewTypeStorage this$0;
            
            WrapperViewTypeLookup(final SharedIdRangeViewTypeStorage this$0, final NestedAdapterWrapper mWrapper) {
                this.this$0 = this$0;
                this.mWrapper = mWrapper;
            }
            
            @Override
            public void dispose() {
                this.this$0.removeWrapper(this.mWrapper);
            }
            
            @Override
            public int globalToLocal(final int n) {
                return n;
            }
            
            @Override
            public int localToGlobal(final int n) {
                List list;
                if ((list = (List)this.this$0.mGlobalTypeToWrapper.get(n)) == null) {
                    list = new ArrayList();
                    this.this$0.mGlobalTypeToWrapper.put(n, (Object)list);
                }
                if (!list.contains(this.mWrapper)) {
                    list.add(this.mWrapper);
                }
                return n;
            }
        }
    }
    
    public interface ViewTypeLookup
    {
        void dispose();
        
        int globalToLocal(final int p0);
        
        int localToGlobal(final int p0);
    }
}

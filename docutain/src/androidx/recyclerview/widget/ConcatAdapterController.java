// 
// Decompiled by Procyon v0.6.0
// 

package androidx.recyclerview.widget;

import android.view.ViewGroup;
import android.util.Pair;
import java.util.Collections;
import android.util.Log;
import androidx.core.util.Preconditions;
import java.util.Iterator;
import java.util.ArrayList;
import java.util.IdentityHashMap;
import java.lang.ref.WeakReference;
import java.util.List;

class ConcatAdapterController implements Callback
{
    private List<WeakReference<RecyclerView>> mAttachedRecyclerViews;
    private final IdentityHashMap<RecyclerView.ViewHolder, NestedAdapterWrapper> mBinderLookup;
    private final ConcatAdapter mConcatAdapter;
    private WrapperAndLocalPosition mReusableHolder;
    private final ConcatAdapter.Config.StableIdMode mStableIdMode;
    private final StableIdStorage mStableIdStorage;
    private final ViewTypeStorage mViewTypeStorage;
    private List<NestedAdapterWrapper> mWrappers;
    
    ConcatAdapterController(final ConcatAdapter mConcatAdapter, final ConcatAdapter.Config config) {
        this.mAttachedRecyclerViews = new ArrayList<WeakReference<RecyclerView>>();
        this.mBinderLookup = new IdentityHashMap<RecyclerView.ViewHolder, NestedAdapterWrapper>();
        this.mWrappers = new ArrayList<NestedAdapterWrapper>();
        this.mReusableHolder = new WrapperAndLocalPosition();
        this.mConcatAdapter = mConcatAdapter;
        if (config.isolateViewTypes) {
            this.mViewTypeStorage = new ViewTypeStorage.IsolatedViewTypeStorage();
        }
        else {
            this.mViewTypeStorage = new ViewTypeStorage.SharedIdRangeViewTypeStorage();
        }
        this.mStableIdMode = config.stableIdMode;
        if (config.stableIdMode == ConcatAdapter.Config.StableIdMode.NO_STABLE_IDS) {
            this.mStableIdStorage = new StableIdStorage.NoStableIdStorage();
        }
        else if (config.stableIdMode == ConcatAdapter.Config.StableIdMode.ISOLATED_STABLE_IDS) {
            this.mStableIdStorage = new StableIdStorage.IsolatedStableIdStorage();
        }
        else {
            if (config.stableIdMode != ConcatAdapter.Config.StableIdMode.SHARED_STABLE_IDS) {
                throw new IllegalArgumentException("unknown stable id mode");
            }
            this.mStableIdStorage = new StableIdStorage.SharedPoolStableIdStorage();
        }
    }
    
    private void calculateAndUpdateStateRestorationPolicy() {
        final RecyclerView.Adapter.StateRestorationPolicy computeStateRestorationPolicy = this.computeStateRestorationPolicy();
        if (computeStateRestorationPolicy != ((RecyclerView.Adapter)this.mConcatAdapter).getStateRestorationPolicy()) {
            this.mConcatAdapter.internalSetStateRestorationPolicy(computeStateRestorationPolicy);
        }
    }
    
    private RecyclerView.Adapter.StateRestorationPolicy computeStateRestorationPolicy() {
        for (final NestedAdapterWrapper nestedAdapterWrapper : this.mWrappers) {
            final RecyclerView.Adapter.StateRestorationPolicy stateRestorationPolicy = nestedAdapterWrapper.adapter.getStateRestorationPolicy();
            if (stateRestorationPolicy == RecyclerView.Adapter.StateRestorationPolicy.PREVENT) {
                return RecyclerView.Adapter.StateRestorationPolicy.PREVENT;
            }
            if (stateRestorationPolicy == RecyclerView.Adapter.StateRestorationPolicy.PREVENT_WHEN_EMPTY && nestedAdapterWrapper.getCachedItemCount() == 0) {
                return RecyclerView.Adapter.StateRestorationPolicy.PREVENT;
            }
        }
        return RecyclerView.Adapter.StateRestorationPolicy.ALLOW;
    }
    
    private int countItemsBefore(final NestedAdapterWrapper nestedAdapterWrapper) {
        final Iterator<NestedAdapterWrapper> iterator = this.mWrappers.iterator();
        int n = 0;
        while (iterator.hasNext()) {
            final NestedAdapterWrapper nestedAdapterWrapper2 = iterator.next();
            if (nestedAdapterWrapper2 == nestedAdapterWrapper) {
                break;
            }
            n += nestedAdapterWrapper2.getCachedItemCount();
        }
        return n;
    }
    
    private WrapperAndLocalPosition findWrapperAndLocalPosition(final int i) {
        WrapperAndLocalPosition mReusableHolder;
        if (this.mReusableHolder.mInUse) {
            mReusableHolder = new WrapperAndLocalPosition();
        }
        else {
            this.mReusableHolder.mInUse = true;
            mReusableHolder = this.mReusableHolder;
        }
        final Iterator<NestedAdapterWrapper> iterator = this.mWrappers.iterator();
        int mLocalPosition = i;
        while (iterator.hasNext()) {
            final NestedAdapterWrapper mWrapper = iterator.next();
            if (mWrapper.getCachedItemCount() > mLocalPosition) {
                mReusableHolder.mWrapper = mWrapper;
                mReusableHolder.mLocalPosition = mLocalPosition;
                break;
            }
            mLocalPosition -= mWrapper.getCachedItemCount();
        }
        if (mReusableHolder.mWrapper != null) {
            return mReusableHolder;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Cannot find wrapper for ");
        sb.append(i);
        throw new IllegalArgumentException(sb.toString());
    }
    
    private NestedAdapterWrapper findWrapperFor(final RecyclerView.Adapter<RecyclerView.ViewHolder> adapter) {
        final int indexOfWrapper = this.indexOfWrapper(adapter);
        if (indexOfWrapper == -1) {
            return null;
        }
        return this.mWrappers.get(indexOfWrapper);
    }
    
    private NestedAdapterWrapper getWrapper(final RecyclerView.ViewHolder viewHolder) {
        final NestedAdapterWrapper nestedAdapterWrapper = this.mBinderLookup.get(viewHolder);
        if (nestedAdapterWrapper != null) {
            return nestedAdapterWrapper;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Cannot find wrapper for ");
        sb.append(viewHolder);
        sb.append(", seems like it is not bound by this adapter: ");
        sb.append(this);
        throw new IllegalStateException(sb.toString());
    }
    
    private int indexOfWrapper(final RecyclerView.Adapter<RecyclerView.ViewHolder> adapter) {
        for (int size = this.mWrappers.size(), i = 0; i < size; ++i) {
            if (this.mWrappers.get(i).adapter == adapter) {
                return i;
            }
        }
        return -1;
    }
    
    private boolean isAttachedTo(final RecyclerView recyclerView) {
        final Iterator<WeakReference<RecyclerView>> iterator = this.mAttachedRecyclerViews.iterator();
        while (iterator.hasNext()) {
            if (iterator.next().get() == recyclerView) {
                return true;
            }
        }
        return false;
    }
    
    private void releaseWrapperAndLocalPosition(final WrapperAndLocalPosition mReusableHolder) {
        mReusableHolder.mInUse = false;
        mReusableHolder.mWrapper = null;
        mReusableHolder.mLocalPosition = -1;
        this.mReusableHolder = mReusableHolder;
    }
    
    boolean addAdapter(final int i, final RecyclerView.Adapter<RecyclerView.ViewHolder> adapter) {
        if (i < 0 || i > this.mWrappers.size()) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Index must be between 0 and ");
            sb.append(this.mWrappers.size());
            sb.append(". Given:");
            sb.append(i);
            throw new IndexOutOfBoundsException(sb.toString());
        }
        if (this.hasStableIds()) {
            Preconditions.checkArgument(adapter.hasStableIds(), (Object)"All sub adapters must have stable ids when stable id mode is ISOLATED_STABLE_IDS or SHARED_STABLE_IDS");
        }
        else if (adapter.hasStableIds()) {
            Log.w("ConcatAdapter", "Stable ids in the adapter will be ignored as the ConcatAdapter is configured not to have stable ids");
        }
        if (this.findWrapperFor(adapter) != null) {
            return false;
        }
        final NestedAdapterWrapper nestedAdapterWrapper = new NestedAdapterWrapper(adapter, (NestedAdapterWrapper.Callback)this, this.mViewTypeStorage, this.mStableIdStorage.createStableIdLookup());
        this.mWrappers.add(i, nestedAdapterWrapper);
        final Iterator<WeakReference<RecyclerView>> iterator = this.mAttachedRecyclerViews.iterator();
        while (iterator.hasNext()) {
            final RecyclerView recyclerView = iterator.next().get();
            if (recyclerView != null) {
                adapter.onAttachedToRecyclerView(recyclerView);
            }
        }
        if (nestedAdapterWrapper.getCachedItemCount() > 0) {
            ((RecyclerView.Adapter)this.mConcatAdapter).notifyItemRangeInserted(this.countItemsBefore(nestedAdapterWrapper), nestedAdapterWrapper.getCachedItemCount());
        }
        this.calculateAndUpdateStateRestorationPolicy();
        return true;
    }
    
    boolean addAdapter(final RecyclerView.Adapter<RecyclerView.ViewHolder> adapter) {
        return this.addAdapter(this.mWrappers.size(), adapter);
    }
    
    public boolean canRestoreState() {
        final Iterator<NestedAdapterWrapper> iterator = this.mWrappers.iterator();
        while (iterator.hasNext()) {
            if (!iterator.next().adapter.canRestoreState()) {
                return false;
            }
        }
        return true;
    }
    
    public RecyclerView.Adapter<? extends RecyclerView.ViewHolder> getBoundAdapter(final RecyclerView.ViewHolder key) {
        final NestedAdapterWrapper nestedAdapterWrapper = this.mBinderLookup.get(key);
        if (nestedAdapterWrapper == null) {
            return null;
        }
        return nestedAdapterWrapper.adapter;
    }
    
    public List<RecyclerView.Adapter<? extends RecyclerView.ViewHolder>> getCopyOfAdapters() {
        if (this.mWrappers.isEmpty()) {
            return Collections.emptyList();
        }
        final ArrayList list = new ArrayList(this.mWrappers.size());
        final Iterator<NestedAdapterWrapper> iterator = this.mWrappers.iterator();
        while (iterator.hasNext()) {
            list.add(iterator.next().adapter);
        }
        return list;
    }
    
    public long getItemId(final int n) {
        final WrapperAndLocalPosition wrapperAndLocalPosition = this.findWrapperAndLocalPosition(n);
        final long itemId = wrapperAndLocalPosition.mWrapper.getItemId(wrapperAndLocalPosition.mLocalPosition);
        this.releaseWrapperAndLocalPosition(wrapperAndLocalPosition);
        return itemId;
    }
    
    public int getItemViewType(int itemViewType) {
        final WrapperAndLocalPosition wrapperAndLocalPosition = this.findWrapperAndLocalPosition(itemViewType);
        itemViewType = wrapperAndLocalPosition.mWrapper.getItemViewType(wrapperAndLocalPosition.mLocalPosition);
        this.releaseWrapperAndLocalPosition(wrapperAndLocalPosition);
        return itemViewType;
    }
    
    public int getLocalAdapterPosition(final RecyclerView.Adapter<? extends RecyclerView.ViewHolder> obj, final RecyclerView.ViewHolder viewHolder, int itemCount) {
        final NestedAdapterWrapper nestedAdapterWrapper = this.mBinderLookup.get(viewHolder);
        if (nestedAdapterWrapper == null) {
            return -1;
        }
        final int i = itemCount - this.countItemsBefore(nestedAdapterWrapper);
        itemCount = nestedAdapterWrapper.adapter.getItemCount();
        if (i >= 0 && i < itemCount) {
            return nestedAdapterWrapper.adapter.findRelativeAdapterPositionIn(obj, viewHolder, i);
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Detected inconsistent adapter updates. The local position of the view holder maps to ");
        sb.append(i);
        sb.append(" which is out of bounds for the adapter with size ");
        sb.append(itemCount);
        sb.append(".Make sure to immediately call notify methods in your adapter when you change the backing dataviewHolder:");
        sb.append(viewHolder);
        sb.append("adapter:");
        sb.append(obj);
        throw new IllegalStateException(sb.toString());
    }
    
    public int getTotalCount() {
        final Iterator<NestedAdapterWrapper> iterator = this.mWrappers.iterator();
        int n = 0;
        while (iterator.hasNext()) {
            n += iterator.next().getCachedItemCount();
        }
        return n;
    }
    
    public Pair<RecyclerView.Adapter<? extends RecyclerView.ViewHolder>, Integer> getWrappedAdapterAndPosition(final int n) {
        final WrapperAndLocalPosition wrapperAndLocalPosition = this.findWrapperAndLocalPosition(n);
        final Pair pair = new Pair((Object)wrapperAndLocalPosition.mWrapper.adapter, (Object)wrapperAndLocalPosition.mLocalPosition);
        this.releaseWrapperAndLocalPosition(wrapperAndLocalPosition);
        return (Pair<RecyclerView.Adapter<? extends RecyclerView.ViewHolder>, Integer>)pair;
    }
    
    public boolean hasStableIds() {
        return this.mStableIdMode != ConcatAdapter.Config.StableIdMode.NO_STABLE_IDS;
    }
    
    public void onAttachedToRecyclerView(final RecyclerView referent) {
        if (this.isAttachedTo(referent)) {
            return;
        }
        this.mAttachedRecyclerViews.add(new WeakReference<RecyclerView>(referent));
        final Iterator<NestedAdapterWrapper> iterator = this.mWrappers.iterator();
        while (iterator.hasNext()) {
            iterator.next().adapter.onAttachedToRecyclerView(referent);
        }
    }
    
    public void onBindViewHolder(final RecyclerView.ViewHolder key, final int n) {
        final WrapperAndLocalPosition wrapperAndLocalPosition = this.findWrapperAndLocalPosition(n);
        this.mBinderLookup.put(key, wrapperAndLocalPosition.mWrapper);
        wrapperAndLocalPosition.mWrapper.onBindViewHolder(key, wrapperAndLocalPosition.mLocalPosition);
        this.releaseWrapperAndLocalPosition(wrapperAndLocalPosition);
    }
    
    @Override
    public void onChanged(final NestedAdapterWrapper nestedAdapterWrapper) {
        ((RecyclerView.Adapter)this.mConcatAdapter).notifyDataSetChanged();
        this.calculateAndUpdateStateRestorationPolicy();
    }
    
    public RecyclerView.ViewHolder onCreateViewHolder(final ViewGroup viewGroup, final int n) {
        return this.mViewTypeStorage.getWrapperForGlobalType(n).onCreateViewHolder(viewGroup, n);
    }
    
    public void onDetachedFromRecyclerView(final RecyclerView recyclerView) {
        for (int i = this.mAttachedRecyclerViews.size() - 1; i >= 0; --i) {
            final WeakReference weakReference = this.mAttachedRecyclerViews.get(i);
            if (weakReference.get() == null) {
                this.mAttachedRecyclerViews.remove(i);
            }
            else if (weakReference.get() == recyclerView) {
                this.mAttachedRecyclerViews.remove(i);
                break;
            }
        }
        final Iterator<NestedAdapterWrapper> iterator = this.mWrappers.iterator();
        while (iterator.hasNext()) {
            iterator.next().adapter.onDetachedFromRecyclerView(recyclerView);
        }
    }
    
    public boolean onFailedToRecycleView(final RecyclerView.ViewHolder obj) {
        final NestedAdapterWrapper nestedAdapterWrapper = this.mBinderLookup.get(obj);
        if (nestedAdapterWrapper != null) {
            final boolean onFailedToRecycleView = nestedAdapterWrapper.adapter.onFailedToRecycleView(obj);
            this.mBinderLookup.remove(obj);
            return onFailedToRecycleView;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Cannot find wrapper for ");
        sb.append(obj);
        sb.append(", seems like it is not bound by this adapter: ");
        sb.append(this);
        throw new IllegalStateException(sb.toString());
    }
    
    @Override
    public void onItemRangeChanged(final NestedAdapterWrapper nestedAdapterWrapper, final int n, final int n2) {
        ((RecyclerView.Adapter)this.mConcatAdapter).notifyItemRangeChanged(n + this.countItemsBefore(nestedAdapterWrapper), n2);
    }
    
    @Override
    public void onItemRangeChanged(final NestedAdapterWrapper nestedAdapterWrapper, final int n, final int n2, final Object o) {
        ((RecyclerView.Adapter)this.mConcatAdapter).notifyItemRangeChanged(n + this.countItemsBefore(nestedAdapterWrapper), n2, o);
    }
    
    @Override
    public void onItemRangeInserted(final NestedAdapterWrapper nestedAdapterWrapper, final int n, final int n2) {
        ((RecyclerView.Adapter)this.mConcatAdapter).notifyItemRangeInserted(n + this.countItemsBefore(nestedAdapterWrapper), n2);
    }
    
    @Override
    public void onItemRangeMoved(final NestedAdapterWrapper nestedAdapterWrapper, final int n, final int n2) {
        final int countItemsBefore = this.countItemsBefore(nestedAdapterWrapper);
        ((RecyclerView.Adapter)this.mConcatAdapter).notifyItemMoved(n + countItemsBefore, n2 + countItemsBefore);
    }
    
    @Override
    public void onItemRangeRemoved(final NestedAdapterWrapper nestedAdapterWrapper, final int n, final int n2) {
        ((RecyclerView.Adapter)this.mConcatAdapter).notifyItemRangeRemoved(n + this.countItemsBefore(nestedAdapterWrapper), n2);
    }
    
    @Override
    public void onStateRestorationPolicyChanged(final NestedAdapterWrapper nestedAdapterWrapper) {
        this.calculateAndUpdateStateRestorationPolicy();
    }
    
    public void onViewAttachedToWindow(final RecyclerView.ViewHolder viewHolder) {
        this.getWrapper(viewHolder).adapter.onViewAttachedToWindow(viewHolder);
    }
    
    public void onViewDetachedFromWindow(final RecyclerView.ViewHolder viewHolder) {
        this.getWrapper(viewHolder).adapter.onViewDetachedFromWindow(viewHolder);
    }
    
    public void onViewRecycled(final RecyclerView.ViewHolder obj) {
        final NestedAdapterWrapper nestedAdapterWrapper = this.mBinderLookup.get(obj);
        if (nestedAdapterWrapper != null) {
            nestedAdapterWrapper.adapter.onViewRecycled(obj);
            this.mBinderLookup.remove(obj);
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Cannot find wrapper for ");
        sb.append(obj);
        sb.append(", seems like it is not bound by this adapter: ");
        sb.append(this);
        throw new IllegalStateException(sb.toString());
    }
    
    boolean removeAdapter(final RecyclerView.Adapter<RecyclerView.ViewHolder> adapter) {
        final int indexOfWrapper = this.indexOfWrapper(adapter);
        if (indexOfWrapper == -1) {
            return false;
        }
        final NestedAdapterWrapper nestedAdapterWrapper = this.mWrappers.get(indexOfWrapper);
        final int countItemsBefore = this.countItemsBefore(nestedAdapterWrapper);
        this.mWrappers.remove(indexOfWrapper);
        ((RecyclerView.Adapter)this.mConcatAdapter).notifyItemRangeRemoved(countItemsBefore, nestedAdapterWrapper.getCachedItemCount());
        final Iterator<WeakReference<RecyclerView>> iterator = this.mAttachedRecyclerViews.iterator();
        while (iterator.hasNext()) {
            final RecyclerView recyclerView = iterator.next().get();
            if (recyclerView != null) {
                adapter.onDetachedFromRecyclerView(recyclerView);
            }
        }
        nestedAdapterWrapper.dispose();
        this.calculateAndUpdateStateRestorationPolicy();
        return true;
    }
    
    static class WrapperAndLocalPosition
    {
        boolean mInUse;
        int mLocalPosition;
        NestedAdapterWrapper mWrapper;
    }
}

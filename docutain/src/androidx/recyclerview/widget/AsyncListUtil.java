// 
// Decompiled by Procyon v0.6.0
// 

package androidx.recyclerview.widget;

import android.util.SparseBooleanArray;
import android.util.Log;
import android.util.SparseIntArray;

public class AsyncListUtil<T>
{
    static final boolean DEBUG = false;
    static final String TAG = "AsyncListUtil";
    boolean mAllowScrollHints;
    private final ThreadUtil.BackgroundCallback<T> mBackgroundCallback;
    final ThreadUtil.BackgroundCallback<T> mBackgroundProxy;
    final DataCallback<T> mDataCallback;
    int mDisplayedGeneration;
    int mItemCount;
    private final ThreadUtil.MainThreadCallback<T> mMainThreadCallback;
    final ThreadUtil.MainThreadCallback<T> mMainThreadProxy;
    final SparseIntArray mMissingPositions;
    final int[] mPrevRange;
    int mRequestedGeneration;
    private int mScrollHint;
    final Class<T> mTClass;
    final TileList<T> mTileList;
    final int mTileSize;
    final int[] mTmpRange;
    final int[] mTmpRangeExtended;
    final ViewCallback mViewCallback;
    
    public AsyncListUtil(final Class<T> mtClass, final int mTileSize, final DataCallback<T> mDataCallback, final ViewCallback mViewCallback) {
        this.mTmpRange = new int[2];
        this.mPrevRange = new int[2];
        this.mTmpRangeExtended = new int[2];
        this.mScrollHint = 0;
        this.mItemCount = 0;
        this.mDisplayedGeneration = 0;
        this.mRequestedGeneration = 0;
        this.mMissingPositions = new SparseIntArray();
        final ThreadUtil.MainThreadCallback<T> mMainThreadCallback = new ThreadUtil.MainThreadCallback<T>() {
            final AsyncListUtil this$0;
            
            private boolean isRequestedGeneration(final int n) {
                return n == this.this$0.mRequestedGeneration;
            }
            
            private void recycleAllTiles() {
                for (int i = 0; i < this.this$0.mTileList.size(); ++i) {
                    this.this$0.mBackgroundProxy.recycleTile(this.this$0.mTileList.getAtIndex(i));
                }
                this.this$0.mTileList.clear();
            }
            
            @Override
            public void addTile(int i, final TileList.Tile<T> tile) {
                if (!this.isRequestedGeneration(i)) {
                    this.this$0.mBackgroundProxy.recycleTile(tile);
                    return;
                }
                final TileList.Tile<T> addOrReplace = this.this$0.mTileList.addOrReplace(tile);
                if (addOrReplace != null) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("duplicate tile @");
                    sb.append(addOrReplace.mStartPosition);
                    Log.e("AsyncListUtil", sb.toString());
                    this.this$0.mBackgroundProxy.recycleTile(addOrReplace);
                }
                final int mStartPosition = tile.mStartPosition;
                final int mItemCount = tile.mItemCount;
                i = 0;
                while (i < this.this$0.mMissingPositions.size()) {
                    final int key = this.this$0.mMissingPositions.keyAt(i);
                    if (tile.mStartPosition <= key && key < mStartPosition + mItemCount) {
                        this.this$0.mMissingPositions.removeAt(i);
                        this.this$0.mViewCallback.onItemLoaded(key);
                    }
                    else {
                        ++i;
                    }
                }
            }
            
            @Override
            public void removeTile(final int n, final int i) {
                if (!this.isRequestedGeneration(n)) {
                    return;
                }
                final TileList.Tile<T> removeAtPos = this.this$0.mTileList.removeAtPos(i);
                if (removeAtPos == null) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("tile not found @");
                    sb.append(i);
                    Log.e("AsyncListUtil", sb.toString());
                    return;
                }
                this.this$0.mBackgroundProxy.recycleTile(removeAtPos);
            }
            
            @Override
            public void updateItemCount(final int n, final int mItemCount) {
                if (!this.isRequestedGeneration(n)) {
                    return;
                }
                this.this$0.mItemCount = mItemCount;
                this.this$0.mViewCallback.onDataRefresh();
                final AsyncListUtil this$0 = this.this$0;
                this$0.mDisplayedGeneration = this$0.mRequestedGeneration;
                this.recycleAllTiles();
                this.this$0.mAllowScrollHints = false;
                this.this$0.updateRange();
            }
        };
        this.mMainThreadCallback = mMainThreadCallback;
        final ThreadUtil.BackgroundCallback<T> mBackgroundCallback = new ThreadUtil.BackgroundCallback<T>() {
            private int mFirstRequiredTileStart;
            private int mGeneration;
            private int mItemCount;
            private int mLastRequiredTileStart;
            final SparseBooleanArray mLoadedTiles = new SparseBooleanArray();
            private TileList.Tile<T> mRecycledRoot;
            final AsyncListUtil this$0;
            
            private TileList.Tile<T> acquireTile() {
                final TileList.Tile<T> mRecycledRoot = this.mRecycledRoot;
                if (mRecycledRoot != null) {
                    this.mRecycledRoot = mRecycledRoot.mNext;
                    return mRecycledRoot;
                }
                return (TileList.Tile<T>)new TileList.Tile((Class<Object>)this.this$0.mTClass, this.this$0.mTileSize);
            }
            
            private void addTile(final TileList.Tile<T> tile) {
                this.mLoadedTiles.put(tile.mStartPosition, true);
                this.this$0.mMainThreadProxy.addTile(this.mGeneration, tile);
            }
            
            private void flushTileCache(final int n) {
                while (this.mLoadedTiles.size() >= this.this$0.mDataCallback.getMaxCachedTiles()) {
                    final int key = this.mLoadedTiles.keyAt(0);
                    final SparseBooleanArray mLoadedTiles = this.mLoadedTiles;
                    final int key2 = mLoadedTiles.keyAt(mLoadedTiles.size() - 1);
                    final int n2 = this.mFirstRequiredTileStart - key;
                    final int n3 = key2 - this.mLastRequiredTileStart;
                    if (n2 > 0 && (n2 >= n3 || n == 2)) {
                        this.removeTile(key);
                    }
                    else {
                        if (n3 <= 0 || (n2 >= n3 && n != 1)) {
                            break;
                        }
                        this.removeTile(key2);
                    }
                }
            }
            
            private int getTileStart(final int n) {
                return n - n % this.this$0.mTileSize;
            }
            
            private boolean isTileLoaded(final int n) {
                return this.mLoadedTiles.get(n);
            }
            
            private void log(final String format, final Object... args) {
                final StringBuilder sb = new StringBuilder();
                sb.append("[BKGR] ");
                sb.append(String.format(format, args));
                Log.d("AsyncListUtil", sb.toString());
            }
            
            private void removeTile(final int n) {
                this.mLoadedTiles.delete(n);
                this.this$0.mMainThreadProxy.removeTile(this.mGeneration, n);
            }
            
            private void requestTiles(final int n, final int n2, final int n3, final boolean b) {
                for (int i = n; i <= n2; i += this.this$0.mTileSize) {
                    int n4;
                    if (b) {
                        n4 = n2 + n - i;
                    }
                    else {
                        n4 = i;
                    }
                    this.this$0.mBackgroundProxy.loadTile(n4, n3);
                }
            }
            
            @Override
            public void loadTile(final int mStartPosition, final int n) {
                if (this.isTileLoaded(mStartPosition)) {
                    return;
                }
                final TileList.Tile<T> acquireTile = this.acquireTile();
                acquireTile.mStartPosition = mStartPosition;
                acquireTile.mItemCount = Math.min(this.this$0.mTileSize, this.mItemCount - acquireTile.mStartPosition);
                this.this$0.mDataCallback.fillData(acquireTile.mItems, acquireTile.mStartPosition, acquireTile.mItemCount);
                this.flushTileCache(n);
                this.addTile(acquireTile);
            }
            
            @Override
            public void recycleTile(final TileList.Tile<T> mRecycledRoot) {
                this.this$0.mDataCallback.recycleData(mRecycledRoot.mItems, mRecycledRoot.mItemCount);
                mRecycledRoot.mNext = this.mRecycledRoot;
                this.mRecycledRoot = mRecycledRoot;
            }
            
            @Override
            public void refresh(final int mGeneration) {
                this.mGeneration = mGeneration;
                this.mLoadedTiles.clear();
                this.mItemCount = this.this$0.mDataCallback.refreshData();
                this.this$0.mMainThreadProxy.updateItemCount(this.mGeneration, this.mItemCount);
            }
            
            @Override
            public void updateRange(int tileStart, int tileStart2, int tileStart3, final int n, final int n2) {
                if (tileStart > tileStart2) {
                    return;
                }
                tileStart = this.getTileStart(tileStart);
                tileStart2 = this.getTileStart(tileStart2);
                this.mFirstRequiredTileStart = this.getTileStart(tileStart3);
                tileStart3 = this.getTileStart(n);
                this.mLastRequiredTileStart = tileStart3;
                if (n2 == 1) {
                    this.requestTiles(this.mFirstRequiredTileStart, tileStart2, n2, true);
                    this.requestTiles(tileStart2 + this.this$0.mTileSize, this.mLastRequiredTileStart, n2, false);
                }
                else {
                    this.requestTiles(tileStart, tileStart3, n2, false);
                    this.requestTiles(this.mFirstRequiredTileStart, tileStart - this.this$0.mTileSize, n2, true);
                }
            }
        };
        this.mBackgroundCallback = mBackgroundCallback;
        this.mTClass = mtClass;
        this.mTileSize = mTileSize;
        this.mDataCallback = mDataCallback;
        this.mViewCallback = mViewCallback;
        this.mTileList = new TileList<T>(mTileSize);
        final MessageThreadUtil messageThreadUtil = new MessageThreadUtil();
        this.mMainThreadProxy = (ThreadUtil.MainThreadCallback<T>)messageThreadUtil.getMainThreadProxy((ThreadUtil.MainThreadCallback)mMainThreadCallback);
        this.mBackgroundProxy = (ThreadUtil.BackgroundCallback<T>)messageThreadUtil.getBackgroundProxy((ThreadUtil.BackgroundCallback)mBackgroundCallback);
        this.refresh();
    }
    
    private boolean isRefreshPending() {
        return this.mRequestedGeneration != this.mDisplayedGeneration;
    }
    
    public T getItem(final int i) {
        if (i >= 0 && i < this.mItemCount) {
            final T item = this.mTileList.getItemAt(i);
            if (item == null && !this.isRefreshPending()) {
                this.mMissingPositions.put(i, 0);
            }
            return item;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append(i);
        sb.append(" is not within 0 and ");
        sb.append(this.mItemCount);
        throw new IndexOutOfBoundsException(sb.toString());
    }
    
    public int getItemCount() {
        return this.mItemCount;
    }
    
    void log(final String format, final Object... args) {
        final StringBuilder sb = new StringBuilder();
        sb.append("[MAIN] ");
        sb.append(String.format(format, args));
        Log.d("AsyncListUtil", sb.toString());
    }
    
    public void onRangeChanged() {
        if (this.isRefreshPending()) {
            return;
        }
        this.updateRange();
        this.mAllowScrollHints = true;
    }
    
    public void refresh() {
        this.mMissingPositions.clear();
        this.mBackgroundProxy.refresh(++this.mRequestedGeneration);
    }
    
    void updateRange() {
        this.mViewCallback.getItemRangeInto(this.mTmpRange);
        final int[] mTmpRange = this.mTmpRange;
        final int n = mTmpRange[0];
        final int n2 = mTmpRange[1];
        if (n <= n2) {
            if (n >= 0) {
                if (n2 >= this.mItemCount) {
                    return;
                }
                Label_0121: {
                    if (!this.mAllowScrollHints) {
                        this.mScrollHint = 0;
                    }
                    else {
                        final int[] mPrevRange = this.mPrevRange;
                        if (n <= mPrevRange[1]) {
                            final int n3 = mPrevRange[0];
                            if (n3 <= n2) {
                                if (n < n3) {
                                    this.mScrollHint = 1;
                                    break Label_0121;
                                }
                                if (n > n3) {
                                    this.mScrollHint = 2;
                                }
                                break Label_0121;
                            }
                        }
                        this.mScrollHint = 0;
                    }
                }
                final int[] mPrevRange2 = this.mPrevRange;
                mPrevRange2[0] = n;
                mPrevRange2[1] = n2;
                this.mViewCallback.extendRangeInto(mTmpRange, this.mTmpRangeExtended, this.mScrollHint);
                final int[] mTmpRangeExtended = this.mTmpRangeExtended;
                mTmpRangeExtended[0] = Math.min(this.mTmpRange[0], Math.max(mTmpRangeExtended[0], 0));
                final int[] mTmpRangeExtended2 = this.mTmpRangeExtended;
                mTmpRangeExtended2[1] = Math.max(this.mTmpRange[1], Math.min(mTmpRangeExtended2[1], this.mItemCount - 1));
                final ThreadUtil.BackgroundCallback<T> mBackgroundProxy = this.mBackgroundProxy;
                final int[] mTmpRange2 = this.mTmpRange;
                final int n4 = mTmpRange2[0];
                final int n5 = mTmpRange2[1];
                final int[] mTmpRangeExtended3 = this.mTmpRangeExtended;
                mBackgroundProxy.updateRange(n4, n5, mTmpRangeExtended3[0], mTmpRangeExtended3[1], this.mScrollHint);
            }
        }
    }
    
    public abstract static class DataCallback<T>
    {
        public abstract void fillData(final T[] p0, final int p1, final int p2);
        
        public int getMaxCachedTiles() {
            return 10;
        }
        
        public void recycleData(final T[] array, final int n) {
        }
        
        public abstract int refreshData();
    }
    
    public abstract static class ViewCallback
    {
        public static final int HINT_SCROLL_ASC = 2;
        public static final int HINT_SCROLL_DESC = 1;
        public static final int HINT_SCROLL_NONE = 0;
        
        public void extendRangeInto(final int[] array, final int[] array2, final int n) {
            final int n2 = array[1];
            final int n3 = array[0];
            final int n4 = n2 - n3 + 1;
            int n5 = n4 / 2;
            int n6;
            if (n == 1) {
                n6 = n4;
            }
            else {
                n6 = n5;
            }
            array2[0] = n3 - n6;
            if (n == 2) {
                n5 = n4;
            }
            array2[1] = n2 + n5;
        }
        
        public abstract void getItemRangeInto(final int[] p0);
        
        public abstract void onDataRefresh();
        
        public abstract void onItemLoaded(final int p0);
    }
}

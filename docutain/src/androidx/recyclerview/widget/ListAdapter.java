// 
// Decompiled by Procyon v0.6.0
// 

package androidx.recyclerview.widget;

import java.util.List;

public abstract class ListAdapter<T, VH extends ViewHolder> extends Adapter<VH>
{
    final AsyncListDiffer<T> mDiffer;
    private final AsyncListDiffer.ListListener<T> mListener;
    
    protected ListAdapter(final AsyncDifferConfig<T> asyncDifferConfig) {
        final AsyncListDiffer.ListListener<T> mListener = new AsyncListDiffer.ListListener<T>() {
            final ListAdapter this$0;
            
            @Override
            public void onCurrentListChanged(final List<T> list, final List<T> list2) {
                this.this$0.onCurrentListChanged(list, list2);
            }
        };
        this.mListener = mListener;
        (this.mDiffer = new AsyncListDiffer<T>(new AdapterListUpdateCallback(this), asyncDifferConfig)).addListListener((AsyncListDiffer.ListListener<T>)mListener);
    }
    
    protected ListAdapter(final DiffUtil.ItemCallback<T> itemCallback) {
        final AsyncListDiffer.ListListener<T> mListener = new AsyncListDiffer.ListListener<T>() {
            final ListAdapter this$0;
            
            @Override
            public void onCurrentListChanged(final List<T> list, final List<T> list2) {
                this.this$0.onCurrentListChanged(list, list2);
            }
        };
        this.mListener = mListener;
        (this.mDiffer = new AsyncListDiffer<T>(new AdapterListUpdateCallback(this), new AsyncDifferConfig.Builder<T>(itemCallback).build())).addListListener((AsyncListDiffer.ListListener<T>)mListener);
    }
    
    public List<T> getCurrentList() {
        return this.mDiffer.getCurrentList();
    }
    
    protected T getItem(final int n) {
        return this.mDiffer.getCurrentList().get(n);
    }
    
    @Override
    public int getItemCount() {
        return this.mDiffer.getCurrentList().size();
    }
    
    public void onCurrentListChanged(final List<T> list, final List<T> list2) {
    }
    
    public void submitList(final List<T> list) {
        this.mDiffer.submitList(list);
    }
    
    public void submitList(final List<T> list, final Runnable runnable) {
        this.mDiffer.submitList(list, runnable);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.recyclerview.widget;

import android.view.View;

public abstract class PdfViewHolderBindDirtyReporter extends ViewHolder
{
    private static final int[] DIRTY_FLAGS;
    
    static {
        DIRTY_FLAGS = new int[] { 1, 2, 4, 32 };
    }
    
    protected PdfViewHolderBindDirtyReporter(final View view) {
        super(view);
    }
    
    private static boolean isBindDirty(final int n) {
        for (final int n2 : PdfViewHolderBindDirtyReporter.DIRTY_FLAGS) {
            if ((n & n2) == n2) {
                return true;
            }
        }
        return false;
    }
    
    private void validate(final int n) {
        if (isBindDirty(n)) {
            this.onViewHolderBindDirty();
        }
    }
    
    @Override
    void addFlags(final int n) {
        super.addFlags(n);
        this.validate(n);
    }
    
    @Override
    void offsetPosition(final int n, final boolean b) {
        super.offsetPosition(n, b);
        this.onViewHolderBindDirty();
    }
    
    protected abstract void onViewHolderBindDirty();
    
    @Override
    void setFlags(final int n, final int n2) {
        super.setFlags(n, n2);
        this.validate(n & n2);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.recyclerview;

public final class R
{
    public static final class attr
    {
        public static final int fastScrollEnabled = 2130969076;
        public static final int fastScrollHorizontalThumbDrawable = 2130969077;
        public static final int fastScrollHorizontalTrackDrawable = 2130969078;
        public static final int fastScrollVerticalThumbDrawable = 2130969079;
        public static final int fastScrollVerticalTrackDrawable = 2130969080;
        public static final int layoutManager = 2130969242;
        public static final int recyclerViewStyle = 2130969955;
        public static final int reverseLayout = 2130969961;
        public static final int spanCount = 2130970030;
        public static final int stackFromEnd = 2130970049;
    }
    
    public static final class dimen
    {
        public static final int fastscroll_default_thickness = 2131165331;
        public static final int fastscroll_margin = 2131165332;
        public static final int fastscroll_minimum_range = 2131165333;
        public static final int item_touch_helper_max_drag_scroll_per_frame = 2131165360;
        public static final int item_touch_helper_swipe_escape_max_velocity = 2131165361;
        public static final int item_touch_helper_swipe_escape_velocity = 2131165362;
    }
    
    public static final class id
    {
        public static final int item_touch_helper_previous_elevation = 2131362175;
    }
    
    public static final class styleable
    {
        public static final int[] RecyclerView;
        public static final int RecyclerView_android_clipToPadding = 1;
        public static final int RecyclerView_android_descendantFocusability = 2;
        public static final int RecyclerView_android_orientation = 0;
        public static final int RecyclerView_fastScrollEnabled = 3;
        public static final int RecyclerView_fastScrollHorizontalThumbDrawable = 4;
        public static final int RecyclerView_fastScrollHorizontalTrackDrawable = 5;
        public static final int RecyclerView_fastScrollVerticalThumbDrawable = 6;
        public static final int RecyclerView_fastScrollVerticalTrackDrawable = 7;
        public static final int RecyclerView_layoutManager = 8;
        public static final int RecyclerView_reverseLayout = 9;
        public static final int RecyclerView_spanCount = 10;
        public static final int RecyclerView_stackFromEnd = 11;
        
        static {
            RecyclerView = new int[] { 16842948, 16842987, 16842993, 2130969076, 2130969077, 2130969078, 2130969079, 2130969080, 2130969242, 2130969961, 2130970030, 2130970049 };
        }
    }
}

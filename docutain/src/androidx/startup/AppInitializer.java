// 
// Decompiled by Procyon v0.6.0
// 

package androidx.startup;

import android.os.Bundle;
import android.content.pm.PackageManager$NameNotFoundException;
import android.content.ComponentName;
import java.util.Iterator;
import java.util.List;
import androidx.tracing.Trace;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import android.content.Context;

public final class AppInitializer
{
    private static final String SECTION_NAME = "Startup";
    private static volatile AppInitializer sInstance;
    private static final Object sLock;
    final Context mContext;
    final Set<Class<? extends Initializer<?>>> mDiscovered;
    final Map<Class<?>, Object> mInitialized;
    
    static {
        sLock = new Object();
    }
    
    AppInitializer(final Context context) {
        this.mContext = context.getApplicationContext();
        this.mDiscovered = new HashSet<Class<? extends Initializer<?>>>();
        this.mInitialized = new HashMap<Class<?>, Object>();
    }
    
    private <T> T doInitialize(final Class<? extends Initializer<?>> clazz, final Set<Class<?>> set) {
        Label_0013: {
            if (!Trace.isEnabled()) {
                break Label_0013;
            }
            try {
                Trace.beginSection(clazz.getSimpleName());
                if (!set.contains(clazz)) {
                    if (!this.mInitialized.containsKey(clazz)) {
                        set.add(clazz);
                        try {
                            final Initializer initializer = (Initializer)clazz.getDeclaredConstructor((Class<?>[])new Class[0]).newInstance(new Object[0]);
                            final List dependencies = initializer.dependencies();
                            if (!dependencies.isEmpty()) {
                                for (final Class clazz2 : dependencies) {
                                    if (!this.mInitialized.containsKey(clazz2)) {
                                        this.doInitialize(clazz2, set);
                                    }
                                }
                            }
                            final Object create = initializer.create(this.mContext);
                            set.remove(clazz);
                            this.mInitialized.put(clazz, create);
                            final Object value;
                            return (T)value;
                        }
                        finally {
                            final Throwable t;
                            throw new StartupException(t);
                        }
                    }
                    final Object value = this.mInitialized.get(clazz);
                    return (T)value;
                }
                throw new IllegalStateException(String.format("Cannot initialize %s. Cycle detected.", clazz.getName()));
            }
            finally {
                Trace.endSection();
            }
        }
    }
    
    public static AppInitializer getInstance(final Context context) {
        if (AppInitializer.sInstance == null) {
            synchronized (AppInitializer.sLock) {
                if (AppInitializer.sInstance == null) {
                    AppInitializer.sInstance = new AppInitializer(context);
                }
            }
        }
        return AppInitializer.sInstance;
    }
    
    static void setDelegate(final AppInitializer sInstance) {
        synchronized (AppInitializer.sLock) {
            AppInitializer.sInstance = sInstance;
        }
    }
    
    void discoverAndInitialize() {
        try {
            try {
                Trace.beginSection("Startup");
                this.discoverAndInitialize(this.mContext.getPackageManager().getProviderInfo(new ComponentName(this.mContext.getPackageName(), InitializationProvider.class.getName()), 128).metaData);
                Trace.endSection();
                return;
            }
            finally {}
        }
        catch (final PackageManager$NameNotFoundException ex) {
            throw new StartupException((Throwable)ex);
        }
        Trace.endSection();
    }
    
    void discoverAndInitialize(final Bundle bundle) {
        final String string = this.mContext.getString(R.string.androidx_startup);
        if (bundle != null) {
            try {
                final HashSet<Class<?>> set = new HashSet<Class<?>>();
                for (final String className : bundle.keySet()) {
                    if (string.equals(bundle.getString(className, (String)null))) {
                        final Class<?> forName = Class.forName(className);
                        if (!Initializer.class.isAssignableFrom(forName)) {
                            continue;
                        }
                        this.mDiscovered.add((Class<? extends Initializer<?>>)forName);
                    }
                }
                final Iterator<Class<? extends Initializer<?>>> iterator2 = this.mDiscovered.iterator();
                while (iterator2.hasNext()) {
                    this.doInitialize(iterator2.next(), set);
                }
            }
            catch (final ClassNotFoundException ex) {
                throw new StartupException(ex);
            }
        }
    }
    
     <T> T doInitialize(final Class<? extends Initializer<?>> clazz) {
        synchronized (AppInitializer.sLock) {
            Object o;
            if ((o = this.mInitialized.get(clazz)) == null) {
                o = this.doInitialize(clazz, new HashSet<Class<?>>());
            }
            return (T)o;
        }
    }
    
    public <T> T initializeComponent(final Class<? extends Initializer<T>> clazz) {
        return (T)this.doInitialize(clazz);
    }
    
    public boolean isEagerlyInitialized(final Class<? extends Initializer<?>> clazz) {
        return this.mDiscovered.contains(clazz);
    }
}

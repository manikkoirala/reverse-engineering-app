// 
// Decompiled by Procyon v0.6.0
// 

package androidx.startup;

import java.util.List;
import android.content.Context;

public interface Initializer<T>
{
    T create(final Context p0);
    
    List<Class<? extends Initializer<?>>> dependencies();
}

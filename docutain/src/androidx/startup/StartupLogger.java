// 
// Decompiled by Procyon v0.6.0
// 

package androidx.startup;

import android.util.Log;

public final class StartupLogger
{
    static final boolean DEBUG = false;
    private static final String TAG = "StartupLogger";
    
    private StartupLogger() {
    }
    
    public static void e(final String s, final Throwable t) {
        Log.e("StartupLogger", s, t);
    }
    
    public static void i(final String s) {
        Log.i("StartupLogger", s);
    }
    
    public static void w(final String s) {
        Log.w("StartupLogger", s);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.window;

public final class R
{
    public static final class attr
    {
        public static final int activityAction = 2130968615;
        public static final int activityName = 2130968617;
        public static final int alwaysExpand = 2130968635;
        public static final int animationBackgroundColor = 2130968640;
        public static final int clearTop = 2130968805;
        public static final int finishPrimaryWithPlaceholder = 2130969081;
        public static final int finishPrimaryWithSecondary = 2130969082;
        public static final int finishSecondaryWithPrimary = 2130969083;
        public static final int placeholderActivityName = 2130969526;
        public static final int primaryActivityName = 2130969556;
        public static final int secondaryActivityAction = 2130969980;
        public static final int secondaryActivityName = 2130969981;
        public static final int splitLayoutDirection = 2130970034;
        public static final int splitMaxAspectRatioInLandscape = 2130970035;
        public static final int splitMaxAspectRatioInPortrait = 2130970036;
        public static final int splitMinHeightDp = 2130970037;
        public static final int splitMinSmallestWidthDp = 2130970038;
        public static final int splitMinWidthDp = 2130970039;
        public static final int splitRatio = 2130970040;
        public static final int stickyPlaceholder = 2130970071;
        public static final int tag = 2130970130;
    }
    
    public static final class id
    {
        public static final int adjacent = 2131361905;
        public static final int always = 2131361917;
        public static final int alwaysAllow = 2131361918;
        public static final int alwaysDisallow = 2131361919;
        public static final int androidx_window_activity_scope = 2131361920;
        public static final int bottomToTop = 2131361955;
        public static final int locale = 2131362194;
        public static final int ltr = 2131362195;
        public static final int never = 2131362280;
        public static final int rtl = 2131362953;
        public static final int topToBottom = 2131363110;
    }
    
    public static final class styleable
    {
        public static final int[] ActivityFilter;
        public static final int ActivityFilter_activityAction = 0;
        public static final int ActivityFilter_activityName = 1;
        public static final int[] ActivityRule;
        public static final int ActivityRule_alwaysExpand = 0;
        public static final int ActivityRule_tag = 1;
        public static final int[] SplitPairFilter;
        public static final int SplitPairFilter_primaryActivityName = 0;
        public static final int SplitPairFilter_secondaryActivityAction = 1;
        public static final int SplitPairFilter_secondaryActivityName = 2;
        public static final int[] SplitPairRule;
        public static final int SplitPairRule_animationBackgroundColor = 0;
        public static final int SplitPairRule_clearTop = 1;
        public static final int SplitPairRule_finishPrimaryWithSecondary = 2;
        public static final int SplitPairRule_finishSecondaryWithPrimary = 3;
        public static final int SplitPairRule_splitLayoutDirection = 4;
        public static final int SplitPairRule_splitMaxAspectRatioInLandscape = 5;
        public static final int SplitPairRule_splitMaxAspectRatioInPortrait = 6;
        public static final int SplitPairRule_splitMinHeightDp = 7;
        public static final int SplitPairRule_splitMinSmallestWidthDp = 8;
        public static final int SplitPairRule_splitMinWidthDp = 9;
        public static final int SplitPairRule_splitRatio = 10;
        public static final int SplitPairRule_tag = 11;
        public static final int[] SplitPlaceholderRule;
        public static final int SplitPlaceholderRule_animationBackgroundColor = 0;
        public static final int SplitPlaceholderRule_finishPrimaryWithPlaceholder = 1;
        public static final int SplitPlaceholderRule_placeholderActivityName = 2;
        public static final int SplitPlaceholderRule_splitLayoutDirection = 3;
        public static final int SplitPlaceholderRule_splitMaxAspectRatioInLandscape = 4;
        public static final int SplitPlaceholderRule_splitMaxAspectRatioInPortrait = 5;
        public static final int SplitPlaceholderRule_splitMinHeightDp = 6;
        public static final int SplitPlaceholderRule_splitMinSmallestWidthDp = 7;
        public static final int SplitPlaceholderRule_splitMinWidthDp = 8;
        public static final int SplitPlaceholderRule_splitRatio = 9;
        public static final int SplitPlaceholderRule_stickyPlaceholder = 10;
        public static final int SplitPlaceholderRule_tag = 11;
        
        static {
            ActivityFilter = new int[] { 2130968615, 2130968617 };
            ActivityRule = new int[] { 2130968635, 2130970130 };
            SplitPairFilter = new int[] { 2130969556, 2130969980, 2130969981 };
            SplitPairRule = new int[] { 2130968640, 2130968805, 2130969082, 2130969083, 2130970034, 2130970035, 2130970036, 2130970037, 2130970038, 2130970039, 2130970040, 2130970130 };
            SplitPlaceholderRule = new int[] { 2130968640, 2130969081, 2130969526, 2130970034, 2130970035, 2130970036, 2130970037, 2130970038, 2130970039, 2130970040, 2130970071, 2130970130 };
        }
    }
}

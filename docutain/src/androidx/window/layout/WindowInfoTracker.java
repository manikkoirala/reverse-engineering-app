// 
// Decompiled by Procyon v0.6.0
// 

package androidx.window.layout;

import kotlin.jvm.JvmStatic;
import androidx.window.layout.adapter.sidecar.SidecarWindowBackend;
import kotlin.jvm.internal.Intrinsics;
import androidx.window.layout.adapter.WindowBackend;
import kotlin.LazyKt;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.Reflection;
import androidx.window.layout.adapter.extensions.ExtensionWindowLayoutInfoBackend;
import kotlin.Lazy;
import android.content.Context;
import kotlinx.coroutines.flow.Flow;
import android.app.Activity;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\bf\u0018\u0000 \t2\u00020\u0001:\u0001\tJ\u0016\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u00032\u0006\u0010\u0005\u001a\u00020\u0006H&J\u0016\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u00032\u0006\u0010\u0007\u001a\u00020\bH\u0017\u00f8\u0001\u0000\u0082\u0002\u0006\n\u0004\b!0\u0001¨\u0006\n\u00c0\u0006\u0001" }, d2 = { "Landroidx/window/layout/WindowInfoTracker;", "", "windowLayoutInfo", "Lkotlinx/coroutines/flow/Flow;", "Landroidx/window/layout/WindowLayoutInfo;", "activity", "Landroid/app/Activity;", "context", "Landroid/content/Context;", "Companion", "window_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
public interface WindowInfoTracker
{
    public static final Companion Companion = WindowInfoTracker.Companion.$$INSTANCE;
    
    Flow<WindowLayoutInfo> windowLayoutInfo(final Activity p0);
    
    Flow<WindowLayoutInfo> windowLayoutInfo(final Context p0);
    
    @Metadata(d1 = { "\u0000:\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0010\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u0013H\u0007J\u0010\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\bH\u0007J\b\u0010\u0017\u001a\u00020\u0015H\u0007R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082D¢\u0006\u0002\n\u0000R\u0010\u0010\u0005\u001a\u0004\u0018\u00010\u0006X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\bX\u0082\u000e¢\u0006\u0002\n\u0000R#\u0010\t\u001a\u0004\u0018\u00010\n8@X\u0080\u0084\u0002¢\u0006\u0012\n\u0004\b\u000e\u0010\u000f\u0012\u0004\b\u000b\u0010\u0002\u001a\u0004\b\f\u0010\r¨\u0006\u0018" }, d2 = { "Landroidx/window/layout/WindowInfoTracker$Companion;", "", "()V", "DEBUG", "", "TAG", "", "decorator", "Landroidx/window/layout/WindowInfoTrackerDecorator;", "extensionBackend", "Landroidx/window/layout/adapter/WindowBackend;", "getExtensionBackend$window_release$annotations", "getExtensionBackend$window_release", "()Landroidx/window/layout/adapter/WindowBackend;", "extensionBackend$delegate", "Lkotlin/Lazy;", "getOrCreate", "Landroidx/window/layout/WindowInfoTracker;", "context", "Landroid/content/Context;", "overrideDecorator", "", "overridingDecorator", "reset", "window_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
    public static final class Companion
    {
        static final Companion $$INSTANCE;
        private static final boolean DEBUG = false;
        private static final String TAG;
        private static WindowInfoTrackerDecorator decorator;
        private static final Lazy<ExtensionWindowLayoutInfoBackend> extensionBackend$delegate;
        
        static {
            $$INSTANCE = new Companion();
            TAG = Reflection.getOrCreateKotlinClass((Class)WindowInfoTracker.class).getSimpleName();
            extensionBackend$delegate = LazyKt.lazy((Function0)WindowInfoTracker$Companion$extensionBackend.WindowInfoTracker$Companion$extensionBackend$2.INSTANCE);
            Companion.decorator = EmptyDecorator.INSTANCE;
        }
        
        private Companion() {
        }
        
        public final WindowBackend getExtensionBackend$window_release() {
            return (WindowBackend)Companion.extensionBackend$delegate.getValue();
        }
        
        @JvmStatic
        public final WindowInfoTracker getOrCreate(final Context context) {
            Intrinsics.checkNotNullParameter((Object)context, "context");
            WindowBackend extensionBackend$window_release;
            if ((extensionBackend$window_release = this.getExtensionBackend$window_release()) == null) {
                extensionBackend$window_release = SidecarWindowBackend.Companion.getInstance(context);
            }
            return Companion.decorator.decorate(new WindowInfoTrackerImpl(WindowMetricsCalculatorCompat.INSTANCE, extensionBackend$window_release));
        }
        
        @JvmStatic
        public final void overrideDecorator(final WindowInfoTrackerDecorator decorator) {
            Intrinsics.checkNotNullParameter((Object)decorator, "overridingDecorator");
            Companion.decorator = decorator;
        }
        
        @JvmStatic
        public final void reset() {
            Companion.decorator = EmptyDecorator.INSTANCE;
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.window.layout.adapter.extensions;

import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;
import kotlin.reflect.KClass;
import kotlin.jvm.internal.Reflection;
import kotlin.collections.CollectionsKt;
import android.app.Activity;
import kotlin.jvm.functions.Function1;
import androidx.window.core.ExtensionsUtil;
import kotlin.Unit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.Executor;
import java.util.LinkedHashMap;
import kotlin.jvm.internal.Intrinsics;
import java.util.concurrent.locks.ReentrantLock;
import android.content.Context;
import androidx.window.extensions.layout.WindowLayoutInfo;
import androidx.window.extensions.core.util.function.Consumer;
import java.util.Map;
import androidx.window.core.ConsumerAdapter;
import androidx.window.extensions.layout.WindowLayoutComponent;
import kotlin.Metadata;
import androidx.window.layout.adapter.WindowBackend;

@Metadata(d1 = { "\u0000\\\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010%\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\b\u0000\u0018\u00002\u00020\u0001:\u0001\u001eB\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\b\u0010\u0015\u001a\u00020\u0016H\u0007J&\u0010\u0017\u001a\u00020\u00182\u0006\u0010\u0019\u001a\u00020\u000f2\u0006\u0010\u001a\u001a\u00020\u001b2\f\u0010\u001c\u001a\b\u0012\u0004\u0012\u00020\u00140\u0013H\u0016J\u0016\u0010\u001d\u001a\u00020\u00182\f\u0010\u001c\u001a\b\u0012\u0004\u0012\u00020\u00140\u0013H\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004¢\u0006\u0002\n\u0000R\"\u0010\u0007\u001a\u0014\u0012\u0004\u0012\u00020\t\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u000b0\n0\b8\u0002X\u0083\u0004¢\u0006\u0002\n\u0000R\u001c\u0010\f\u001a\u000e\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\r0\b8\u0002X\u0083\u0004¢\u0006\u0002\n\u0000R\u001c\u0010\u000e\u001a\u000e\u0012\u0004\u0012\u00020\u000f\u0012\u0004\u0012\u00020\t0\b8\u0002X\u0083\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0011X\u0082\u0004¢\u0006\u0002\n\u0000R\"\u0010\u0012\u001a\u0014\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00140\u0013\u0012\u0004\u0012\u00020\u000f0\b8\u0002X\u0083\u0004¢\u0006\u0002\n\u0000¨\u0006\u001f" }, d2 = { "Landroidx/window/layout/adapter/extensions/ExtensionWindowLayoutInfoBackend;", "Landroidx/window/layout/adapter/WindowBackend;", "component", "Landroidx/window/extensions/layout/WindowLayoutComponent;", "consumerAdapter", "Landroidx/window/core/ConsumerAdapter;", "(Landroidx/window/extensions/layout/WindowLayoutComponent;Landroidx/window/core/ConsumerAdapter;)V", "consumerToOemConsumer", "", "Landroidx/window/layout/adapter/extensions/ExtensionWindowLayoutInfoBackend$MulticastConsumer;", "Landroidx/window/extensions/core/util/function/Consumer;", "Landroidx/window/extensions/layout/WindowLayoutInfo;", "consumerToToken", "Landroidx/window/core/ConsumerAdapter$Subscription;", "contextToListeners", "Landroid/content/Context;", "extensionWindowBackendLock", "Ljava/util/concurrent/locks/ReentrantLock;", "listenerToContext", "Landroidx/core/util/Consumer;", "Landroidx/window/layout/WindowLayoutInfo;", "hasRegisteredListeners", "", "registerLayoutChangeCallback", "", "context", "executor", "Ljava/util/concurrent/Executor;", "callback", "unregisterLayoutChangeCallback", "MulticastConsumer", "window_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
public final class ExtensionWindowLayoutInfoBackend implements WindowBackend
{
    private final WindowLayoutComponent component;
    private final ConsumerAdapter consumerAdapter;
    private final Map<MulticastConsumer, Consumer<WindowLayoutInfo>> consumerToOemConsumer;
    private final Map<MulticastConsumer, ConsumerAdapter.Subscription> consumerToToken;
    private final Map<Context, MulticastConsumer> contextToListeners;
    private final ReentrantLock extensionWindowBackendLock;
    private final Map<androidx.core.util.Consumer<androidx.window.layout.WindowLayoutInfo>, Context> listenerToContext;
    
    public ExtensionWindowLayoutInfoBackend(final WindowLayoutComponent component, final ConsumerAdapter consumerAdapter) {
        Intrinsics.checkNotNullParameter((Object)component, "component");
        Intrinsics.checkNotNullParameter((Object)consumerAdapter, "consumerAdapter");
        this.component = component;
        this.consumerAdapter = consumerAdapter;
        this.extensionWindowBackendLock = new ReentrantLock();
        this.contextToListeners = new LinkedHashMap<Context, MulticastConsumer>();
        this.listenerToContext = new LinkedHashMap<androidx.core.util.Consumer<androidx.window.layout.WindowLayoutInfo>, Context>();
        this.consumerToToken = new LinkedHashMap<MulticastConsumer, ConsumerAdapter.Subscription>();
        this.consumerToOemConsumer = new LinkedHashMap<MulticastConsumer, Consumer<WindowLayoutInfo>>();
    }
    
    private static final void registerLayoutChangeCallback$lambda$3$lambda$2$lambda$1(final MulticastConsumer multicastConsumer, final WindowLayoutInfo windowLayoutInfo) {
        Intrinsics.checkNotNullParameter((Object)multicastConsumer, "$consumer");
        Intrinsics.checkNotNullExpressionValue((Object)windowLayoutInfo, "info");
        multicastConsumer.accept(windowLayoutInfo);
    }
    
    public final boolean hasRegisteredListeners() {
        return !this.contextToListeners.isEmpty() || !this.listenerToContext.isEmpty() || !this.consumerToToken.isEmpty();
    }
    
    @Override
    public void registerLayoutChangeCallback(final Context context, final Executor executor, final androidx.core.util.Consumer<androidx.window.layout.WindowLayoutInfo> consumer) {
        Intrinsics.checkNotNullParameter((Object)context, "context");
        Intrinsics.checkNotNullParameter((Object)executor, "executor");
        Intrinsics.checkNotNullParameter((Object)consumer, "callback");
        final Lock lock = this.extensionWindowBackendLock;
        lock.lock();
        try {
            final MulticastConsumer multicastConsumer = this.contextToListeners.get(context);
            Unit instance;
            if (multicastConsumer != null) {
                multicastConsumer.addListener(consumer);
                this.listenerToContext.put(consumer, context);
                instance = Unit.INSTANCE;
            }
            else {
                instance = null;
            }
            if (instance == null) {
                final ExtensionWindowLayoutInfoBackend extensionWindowLayoutInfoBackend = this;
                final MulticastConsumer multicastConsumer2 = new MulticastConsumer(context);
                this.contextToListeners.put(context, multicastConsumer2);
                this.listenerToContext.put(consumer, context);
                multicastConsumer2.addListener(consumer);
                if (ExtensionsUtil.INSTANCE.getSafeVendorApiLevel() < 2) {
                    final Function1 function1 = (Function1)new ExtensionWindowLayoutInfoBackend$registerLayoutChangeCallback$1$2$consumeWindowLayoutInfo.ExtensionWindowLayoutInfoBackend$registerLayoutChangeCallback$1$2$consumeWindowLayoutInfo$1(multicastConsumer2);
                    if (!(context instanceof Activity)) {
                        multicastConsumer2.accept(new WindowLayoutInfo(CollectionsKt.emptyList()));
                        return;
                    }
                    this.consumerToToken.put(multicastConsumer2, this.consumerAdapter.createSubscription(this.component, (kotlin.reflect.KClass<Object>)Reflection.getOrCreateKotlinClass((Class)WindowLayoutInfo.class), "addWindowLayoutInfoListener", "removeWindowLayoutInfoListener", (Activity)context, (kotlin.jvm.functions.Function1<? super Object, Unit>)function1));
                }
                else {
                    final ExtensionWindowLayoutInfoBackend$$ExternalSyntheticLambda0 extensionWindowLayoutInfoBackend$$ExternalSyntheticLambda0 = new ExtensionWindowLayoutInfoBackend$$ExternalSyntheticLambda0(multicastConsumer2);
                    this.consumerToOemConsumer.put(multicastConsumer2, extensionWindowLayoutInfoBackend$$ExternalSyntheticLambda0);
                    this.component.addWindowLayoutInfoListener(context, (Consumer)extensionWindowLayoutInfoBackend$$ExternalSyntheticLambda0);
                }
            }
            final Unit instance2 = Unit.INSTANCE;
        }
        finally {
            lock.unlock();
        }
    }
    
    @Override
    public void unregisterLayoutChangeCallback(final androidx.core.util.Consumer<androidx.window.layout.WindowLayoutInfo> consumer) {
        Intrinsics.checkNotNullParameter((Object)consumer, "callback");
        final Lock lock = this.extensionWindowBackendLock;
        lock.lock();
        try {
            final Context context = this.listenerToContext.get(consumer);
            if (context == null) {
                return;
            }
            final MulticastConsumer multicastConsumer = this.contextToListeners.get(context);
            if (multicastConsumer == null) {
                return;
            }
            multicastConsumer.removeListener(consumer);
            this.listenerToContext.remove(consumer);
            if (multicastConsumer.isEmpty()) {
                this.contextToListeners.remove(context);
                if (ExtensionsUtil.INSTANCE.getSafeVendorApiLevel() < 2) {
                    final ConsumerAdapter.Subscription subscription = this.consumerToToken.remove(multicastConsumer);
                    if (subscription != null) {
                        subscription.dispose();
                    }
                }
                else {
                    final Consumer consumer2 = this.consumerToOemConsumer.remove(multicastConsumer);
                    if (consumer2 != null) {
                        this.component.removeWindowLayoutInfoListener((Consumer)consumer2);
                    }
                }
            }
            final Unit instance = Unit.INSTANCE;
        }
        finally {
            lock.unlock();
        }
    }
    
    @Metadata(d1 = { "\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010#\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0002\b\u0002\b\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004¢\u0006\u0002\u0010\u0005J\u0010\u0010\f\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u0002H\u0016J\u0014\u0010\u000f\u001a\u00020\r2\f\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u00070\u0001J\u0006\u0010\u0011\u001a\u00020\u0012J\u0014\u0010\u0013\u001a\u00020\r2\f\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u00070\u0001R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004¢\u0006\u0002\n\u0000R\u0014\u0010\u0006\u001a\u0004\u0018\u00010\u00078\u0002@\u0002X\u0083\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u0082\u0004¢\u0006\u0002\n\u0000R\u001c\u0010\n\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00070\u00010\u000b8\u0002X\u0083\u0004¢\u0006\u0002\n\u0000¨\u0006\u0014" }, d2 = { "Landroidx/window/layout/adapter/extensions/ExtensionWindowLayoutInfoBackend$MulticastConsumer;", "Landroidx/core/util/Consumer;", "Landroidx/window/extensions/layout/WindowLayoutInfo;", "context", "Landroid/content/Context;", "(Landroid/content/Context;)V", "lastKnownValue", "Landroidx/window/layout/WindowLayoutInfo;", "multicastConsumerLock", "Ljava/util/concurrent/locks/ReentrantLock;", "registeredListeners", "", "accept", "", "value", "addListener", "listener", "isEmpty", "", "removeListener", "window_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
    private static final class MulticastConsumer implements Consumer<WindowLayoutInfo>
    {
        private final Context context;
        private androidx.window.layout.WindowLayoutInfo lastKnownValue;
        private final ReentrantLock multicastConsumerLock;
        private final Set<Consumer<androidx.window.layout.WindowLayoutInfo>> registeredListeners;
        
        public MulticastConsumer(final Context context) {
            Intrinsics.checkNotNullParameter((Object)context, "context");
            this.context = context;
            this.multicastConsumerLock = new ReentrantLock();
            this.registeredListeners = new LinkedHashSet<Consumer<androidx.window.layout.WindowLayoutInfo>>();
        }
        
        @Override
        public void accept(final WindowLayoutInfo windowLayoutInfo) {
            Intrinsics.checkNotNullParameter((Object)windowLayoutInfo, "value");
            final Lock lock = this.multicastConsumerLock;
            lock.lock();
            try {
                this.lastKnownValue = ExtensionsWindowLayoutInfoAdapter.INSTANCE.translate$window_release(this.context, windowLayoutInfo);
                final Iterator iterator = this.registeredListeners.iterator();
                while (iterator.hasNext()) {
                    ((Consumer<androidx.window.layout.WindowLayoutInfo>)iterator.next()).accept(this.lastKnownValue);
                }
                final Unit instance = Unit.INSTANCE;
            }
            finally {
                lock.unlock();
            }
        }
        
        public final void addListener(final Consumer<androidx.window.layout.WindowLayoutInfo> consumer) {
            Intrinsics.checkNotNullParameter((Object)consumer, "listener");
            final Lock lock = this.multicastConsumerLock;
            lock.lock();
            try {
                final androidx.window.layout.WindowLayoutInfo lastKnownValue = this.lastKnownValue;
                if (lastKnownValue != null) {
                    consumer.accept(lastKnownValue);
                }
                this.registeredListeners.add(consumer);
            }
            finally {
                lock.unlock();
            }
        }
        
        public final boolean isEmpty() {
            return this.registeredListeners.isEmpty();
        }
        
        public final void removeListener(final Consumer<androidx.window.layout.WindowLayoutInfo> consumer) {
            Intrinsics.checkNotNullParameter((Object)consumer, "listener");
            final Lock lock = this.multicastConsumerLock;
            lock.lock();
            try {
                this.registeredListeners.remove(consumer);
            }
            finally {
                lock.unlock();
            }
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.window.layout.adapter.extensions;

import java.util.Iterator;
import java.util.List;
import androidx.window.extensions.layout.DisplayFeature;
import java.util.ArrayList;
import java.util.Collection;
import android.app.Activity;
import androidx.window.layout.WindowMetricsCalculatorCompat;
import android.os.Build$VERSION;
import androidx.window.extensions.layout.WindowLayoutInfo;
import android.content.Context;
import androidx.window.layout.HardwareFoldingFeature;
import kotlin.jvm.internal.Intrinsics;
import androidx.window.extensions.layout.FoldingFeature;
import android.graphics.Rect;
import androidx.window.core.Bounds;
import androidx.window.layout.WindowMetrics;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000<\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u00c0\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u001d\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bH\u0000¢\u0006\u0002\b\tJ\u001f\u0010\u0003\u001a\u0004\u0018\u00010\n2\u0006\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u000eH\u0000¢\u0006\u0002\b\tJ\u001d\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u000b\u001a\u00020\f2\u0006\u0010\u0007\u001a\u00020\bH\u0000¢\u0006\u0002\b\tJ\u0018\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u000b\u001a\u00020\f2\u0006\u0010\u0011\u001a\u00020\u0012H\u0002¨\u0006\u0013" }, d2 = { "Landroidx/window/layout/adapter/extensions/ExtensionsWindowLayoutInfoAdapter;", "", "()V", "translate", "Landroidx/window/layout/WindowLayoutInfo;", "context", "Landroid/content/Context;", "info", "Landroidx/window/extensions/layout/WindowLayoutInfo;", "translate$window_release", "Landroidx/window/layout/FoldingFeature;", "windowMetrics", "Landroidx/window/layout/WindowMetrics;", "oemFeature", "Landroidx/window/extensions/layout/FoldingFeature;", "validBounds", "", "bounds", "Landroidx/window/core/Bounds;", "window_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
public final class ExtensionsWindowLayoutInfoAdapter
{
    public static final ExtensionsWindowLayoutInfoAdapter INSTANCE;
    
    static {
        INSTANCE = new ExtensionsWindowLayoutInfoAdapter();
    }
    
    private ExtensionsWindowLayoutInfoAdapter() {
    }
    
    private final boolean validBounds(final WindowMetrics windowMetrics, final Bounds bounds) {
        final Rect bounds2 = windowMetrics.getBounds();
        return !bounds.isZero() && (bounds.getWidth() == bounds2.width() || bounds.getHeight() == bounds2.height()) && (bounds.getWidth() >= bounds2.width() || bounds.getHeight() >= bounds2.height()) && (bounds.getWidth() != bounds2.width() || bounds.getHeight() != bounds2.height());
    }
    
    public final androidx.window.layout.FoldingFeature translate$window_release(final WindowMetrics windowMetrics, final FoldingFeature foldingFeature) {
        Intrinsics.checkNotNullParameter((Object)windowMetrics, "windowMetrics");
        Intrinsics.checkNotNullParameter((Object)foldingFeature, "oemFeature");
        final int type = foldingFeature.getType();
        androidx.window.layout.FoldingFeature foldingFeature2 = null;
        HardwareFoldingFeature.Type type2;
        if (type != 1) {
            if (type != 2) {
                return null;
            }
            type2 = HardwareFoldingFeature.Type.Companion.getHINGE();
        }
        else {
            type2 = HardwareFoldingFeature.Type.Companion.getFOLD();
        }
        final int state = foldingFeature.getState();
        androidx.window.layout.FoldingFeature.State state2;
        if (state != 1) {
            if (state != 2) {
                return null;
            }
            state2 = androidx.window.layout.FoldingFeature.State.HALF_OPENED;
        }
        else {
            state2 = androidx.window.layout.FoldingFeature.State.FLAT;
        }
        final Rect bounds = foldingFeature.getBounds();
        Intrinsics.checkNotNullExpressionValue((Object)bounds, "oemFeature.bounds");
        if (this.validBounds(windowMetrics, new Bounds(bounds))) {
            final Rect bounds2 = foldingFeature.getBounds();
            Intrinsics.checkNotNullExpressionValue((Object)bounds2, "oemFeature.bounds");
            foldingFeature2 = new HardwareFoldingFeature(new Bounds(bounds2), type2, state2);
        }
        return foldingFeature2;
    }
    
    public final androidx.window.layout.WindowLayoutInfo translate$window_release(final Context context, final WindowLayoutInfo windowLayoutInfo) {
        Intrinsics.checkNotNullParameter((Object)context, "context");
        Intrinsics.checkNotNullParameter((Object)windowLayoutInfo, "info");
        androidx.window.layout.WindowLayoutInfo windowLayoutInfo2;
        if (Build$VERSION.SDK_INT >= 30) {
            windowLayoutInfo2 = this.translate$window_release(WindowMetricsCalculatorCompat.INSTANCE.computeCurrentWindowMetrics(context), windowLayoutInfo);
        }
        else {
            if (Build$VERSION.SDK_INT < 29 || !(context instanceof Activity)) {
                throw new UnsupportedOperationException("Display Features are only supported after Q. Display features for non-Activity contexts are not expected to be reported on devices running Q.");
            }
            windowLayoutInfo2 = this.translate$window_release(WindowMetricsCalculatorCompat.INSTANCE.computeCurrentWindowMetrics((Activity)context), windowLayoutInfo);
        }
        return windowLayoutInfo2;
    }
    
    public final androidx.window.layout.WindowLayoutInfo translate$window_release(final WindowMetrics windowMetrics, final WindowLayoutInfo windowLayoutInfo) {
        Intrinsics.checkNotNullParameter((Object)windowMetrics, "windowMetrics");
        Intrinsics.checkNotNullParameter((Object)windowLayoutInfo, "info");
        final List displayFeatures = windowLayoutInfo.getDisplayFeatures();
        Intrinsics.checkNotNullExpressionValue((Object)displayFeatures, "info.displayFeatures");
        final Iterable iterable = displayFeatures;
        final Collection collection = new ArrayList<androidx.window.layout.FoldingFeature>();
        for (final DisplayFeature displayFeature : iterable) {
            androidx.window.layout.FoldingFeature translate$window_release;
            if (displayFeature instanceof FoldingFeature) {
                final ExtensionsWindowLayoutInfoAdapter instance = ExtensionsWindowLayoutInfoAdapter.INSTANCE;
                Intrinsics.checkNotNullExpressionValue((Object)displayFeature, "feature");
                translate$window_release = instance.translate$window_release(windowMetrics, (FoldingFeature)displayFeature);
            }
            else {
                translate$window_release = null;
            }
            if (translate$window_release != null) {
                collection.add(translate$window_release);
            }
        }
        return new androidx.window.layout.WindowLayoutInfo((List<? extends androidx.window.layout.DisplayFeature>)collection);
    }
}

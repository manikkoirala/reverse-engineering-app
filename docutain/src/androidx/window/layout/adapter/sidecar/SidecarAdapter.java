// 
// Decompiled by Procyon v0.6.0
// 

package androidx.window.layout.adapter.sidecar;

import java.lang.reflect.Method;
import java.lang.reflect.InvocationTargetException;
import android.graphics.Rect;
import androidx.window.core.Bounds;
import androidx.window.layout.FoldingFeature;
import androidx.window.layout.HardwareFoldingFeature;
import kotlin.jvm.functions.Function1;
import androidx.window.core.Logger;
import androidx.window.core.SpecificationComputer;
import java.util.Iterator;
import java.util.ArrayList;
import java.util.Collection;
import androidx.window.layout.DisplayFeature;
import kotlin.collections.CollectionsKt;
import androidx.window.layout.WindowLayoutInfo;
import androidx.window.sidecar.SidecarWindowLayoutInfo;
import androidx.window.sidecar.SidecarDeviceState;
import java.util.List;
import androidx.window.sidecar.SidecarDisplayFeature;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.DefaultConstructorMarker;
import androidx.window.core.VerificationMode;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000B\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0000\u0018\u0000 \u00192\u00020\u0001:\u0001\u0019B\u000f\u0012\b\b\u0002\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u001a\u0010\u0005\u001a\u00020\u00062\b\u0010\u0007\u001a\u0004\u0018\u00010\b2\b\u0010\t\u001a\u0004\u0018\u00010\bJ\u001c\u0010\n\u001a\u00020\u00062\b\u0010\u0007\u001a\u0004\u0018\u00010\u000b2\b\u0010\t\u001a\u0004\u0018\u00010\u000bH\u0002J(\u0010\f\u001a\u00020\u00062\u000e\u0010\u0007\u001a\n\u0012\u0004\u0012\u00020\u000b\u0018\u00010\r2\u000e\u0010\t\u001a\n\u0012\u0004\u0012\u00020\u000b\u0018\u00010\rH\u0002J\u001a\u0010\u000e\u001a\u00020\u00062\b\u0010\u0007\u001a\u0004\u0018\u00010\u000f2\b\u0010\t\u001a\u0004\u0018\u00010\u000fJ\u001f\u0010\u0010\u001a\u0004\u0018\u00010\u00112\u0006\u0010\u0012\u001a\u00020\u000b2\u0006\u0010\u0013\u001a\u00020\bH\u0000¢\u0006\u0002\b\u0014J\u0018\u0010\u0010\u001a\u00020\u00152\b\u0010\u0016\u001a\u0004\u0018\u00010\u000f2\u0006\u0010\u0017\u001a\u00020\bJ\"\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u00110\r2\f\u0010\u0018\u001a\b\u0012\u0004\u0012\u00020\u000b0\r2\u0006\u0010\u0013\u001a\u00020\bR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u001a" }, d2 = { "Landroidx/window/layout/adapter/sidecar/SidecarAdapter;", "", "verificationMode", "Landroidx/window/core/VerificationMode;", "(Landroidx/window/core/VerificationMode;)V", "isEqualSidecarDeviceState", "", "first", "Landroidx/window/sidecar/SidecarDeviceState;", "second", "isEqualSidecarDisplayFeature", "Landroidx/window/sidecar/SidecarDisplayFeature;", "isEqualSidecarDisplayFeatures", "", "isEqualSidecarWindowLayoutInfo", "Landroidx/window/sidecar/SidecarWindowLayoutInfo;", "translate", "Landroidx/window/layout/DisplayFeature;", "feature", "deviceState", "translate$window_release", "Landroidx/window/layout/WindowLayoutInfo;", "extensionInfo", "state", "sidecarDisplayFeatures", "Companion", "window_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
public final class SidecarAdapter
{
    public static final Companion Companion;
    private static final String TAG;
    private final VerificationMode verificationMode;
    
    static {
        Companion = new Companion(null);
        TAG = "SidecarAdapter";
    }
    
    public SidecarAdapter() {
        this(null, 1, null);
    }
    
    public SidecarAdapter(final VerificationMode verificationMode) {
        Intrinsics.checkNotNullParameter((Object)verificationMode, "verificationMode");
        this.verificationMode = verificationMode;
    }
    
    private final boolean isEqualSidecarDisplayFeature(final SidecarDisplayFeature sidecarDisplayFeature, final SidecarDisplayFeature sidecarDisplayFeature2) {
        return Intrinsics.areEqual((Object)sidecarDisplayFeature, (Object)sidecarDisplayFeature2) || (sidecarDisplayFeature != null && sidecarDisplayFeature2 != null && sidecarDisplayFeature.getType() == sidecarDisplayFeature2.getType() && Intrinsics.areEqual((Object)sidecarDisplayFeature.getRect(), (Object)sidecarDisplayFeature2.getRect()));
    }
    
    private final boolean isEqualSidecarDisplayFeatures(final List<SidecarDisplayFeature> list, final List<SidecarDisplayFeature> list2) {
        if (list == list2) {
            return true;
        }
        if (list == null) {
            return false;
        }
        if (list2 == null) {
            return false;
        }
        if (list.size() != list2.size()) {
            return false;
        }
        for (int size = list.size(), i = 0; i < size; ++i) {
            if (!this.isEqualSidecarDisplayFeature((SidecarDisplayFeature)list.get(i), (SidecarDisplayFeature)list2.get(i))) {
                return false;
            }
        }
        return true;
    }
    
    public final boolean isEqualSidecarDeviceState(final SidecarDeviceState sidecarDeviceState, final SidecarDeviceState sidecarDeviceState2) {
        final boolean equal = Intrinsics.areEqual((Object)sidecarDeviceState, (Object)sidecarDeviceState2);
        boolean b = true;
        if (equal) {
            return true;
        }
        if (sidecarDeviceState == null) {
            return false;
        }
        if (sidecarDeviceState2 == null) {
            return false;
        }
        final Companion companion = SidecarAdapter.Companion;
        if (companion.getSidecarDevicePosture$window_release(sidecarDeviceState) != companion.getSidecarDevicePosture$window_release(sidecarDeviceState2)) {
            b = false;
        }
        return b;
    }
    
    public final boolean isEqualSidecarWindowLayoutInfo(final SidecarWindowLayoutInfo sidecarWindowLayoutInfo, final SidecarWindowLayoutInfo sidecarWindowLayoutInfo2) {
        if (Intrinsics.areEqual((Object)sidecarWindowLayoutInfo, (Object)sidecarWindowLayoutInfo2)) {
            return true;
        }
        if (sidecarWindowLayoutInfo == null) {
            return false;
        }
        if (sidecarWindowLayoutInfo2 == null) {
            return false;
        }
        final Companion companion = SidecarAdapter.Companion;
        return this.isEqualSidecarDisplayFeatures(companion.getSidecarDisplayFeatures(sidecarWindowLayoutInfo), companion.getSidecarDisplayFeatures(sidecarWindowLayoutInfo2));
    }
    
    public final WindowLayoutInfo translate(final SidecarWindowLayoutInfo sidecarWindowLayoutInfo, final SidecarDeviceState sidecarDeviceState) {
        Intrinsics.checkNotNullParameter((Object)sidecarDeviceState, "state");
        if (sidecarWindowLayoutInfo == null) {
            return new WindowLayoutInfo(CollectionsKt.emptyList());
        }
        final SidecarDeviceState sidecarDeviceState2 = new SidecarDeviceState();
        final Companion companion = SidecarAdapter.Companion;
        companion.setSidecarDevicePosture(sidecarDeviceState2, companion.getSidecarDevicePosture$window_release(sidecarDeviceState));
        return new WindowLayoutInfo(this.translate(companion.getSidecarDisplayFeatures(sidecarWindowLayoutInfo), sidecarDeviceState2));
    }
    
    public final List<DisplayFeature> translate(final List<SidecarDisplayFeature> list, final SidecarDeviceState sidecarDeviceState) {
        Intrinsics.checkNotNullParameter((Object)list, "sidecarDisplayFeatures");
        Intrinsics.checkNotNullParameter((Object)sidecarDeviceState, "deviceState");
        final Iterable iterable = list;
        final Collection collection = new ArrayList();
        final Iterator iterator = iterable.iterator();
        while (iterator.hasNext()) {
            final DisplayFeature translate$window_release = this.translate$window_release((SidecarDisplayFeature)iterator.next(), sidecarDeviceState);
            if (translate$window_release != null) {
                collection.add(translate$window_release);
            }
        }
        return (List<DisplayFeature>)collection;
    }
    
    public final DisplayFeature translate$window_release(final SidecarDisplayFeature sidecarDisplayFeature, final SidecarDeviceState sidecarDeviceState) {
        Intrinsics.checkNotNullParameter((Object)sidecarDisplayFeature, "feature");
        Intrinsics.checkNotNullParameter((Object)sidecarDeviceState, "deviceState");
        final SpecificationComputer.Companion companion = SpecificationComputer.Companion;
        final String tag = SidecarAdapter.TAG;
        Intrinsics.checkNotNullExpressionValue((Object)tag, "TAG");
        final SidecarDisplayFeature sidecarDisplayFeature2 = SpecificationComputer.Companion.startSpecification$default(companion, sidecarDisplayFeature, tag, this.verificationMode, null, 4, null).require("Type must be either TYPE_FOLD or TYPE_HINGE", (kotlin.jvm.functions.Function1<? super SidecarDisplayFeature, Boolean>)SidecarAdapter$translate$checkedFeature.SidecarAdapter$translate$checkedFeature$1.INSTANCE).require("Feature bounds must not be 0", (kotlin.jvm.functions.Function1<? super SidecarDisplayFeature, Boolean>)SidecarAdapter$translate$checkedFeature.SidecarAdapter$translate$checkedFeature$2.INSTANCE).require("TYPE_FOLD must have 0 area", (kotlin.jvm.functions.Function1<? super SidecarDisplayFeature, Boolean>)SidecarAdapter$translate$checkedFeature.SidecarAdapter$translate$checkedFeature$3.INSTANCE).require("Feature be pinned to either left or top", (kotlin.jvm.functions.Function1<? super SidecarDisplayFeature, Boolean>)SidecarAdapter$translate$checkedFeature.SidecarAdapter$translate$checkedFeature$4.INSTANCE).compute();
        final DisplayFeature displayFeature = null;
        if (sidecarDisplayFeature2 == null) {
            return null;
        }
        final int type = sidecarDisplayFeature2.getType();
        HardwareFoldingFeature.Type type2;
        if (type != 1) {
            if (type != 2) {
                return null;
            }
            type2 = HardwareFoldingFeature.Type.Companion.getHINGE();
        }
        else {
            type2 = HardwareFoldingFeature.Type.Companion.getFOLD();
        }
        final int sidecarDevicePosture$window_release = SidecarAdapter.Companion.getSidecarDevicePosture$window_release(sidecarDeviceState);
        DisplayFeature displayFeature2 = displayFeature;
        if (sidecarDevicePosture$window_release != 0) {
            displayFeature2 = displayFeature;
            if (sidecarDevicePosture$window_release != 1) {
                FoldingFeature.State state;
                if (sidecarDevicePosture$window_release != 2) {
                    if (sidecarDevicePosture$window_release != 3) {
                        displayFeature2 = displayFeature;
                        if (sidecarDevicePosture$window_release == 4) {
                            return displayFeature2;
                        }
                        state = FoldingFeature.State.FLAT;
                    }
                    else {
                        state = FoldingFeature.State.FLAT;
                    }
                }
                else {
                    state = FoldingFeature.State.HALF_OPENED;
                }
                final Rect rect = sidecarDisplayFeature.getRect();
                Intrinsics.checkNotNullExpressionValue((Object)rect, "feature.rect");
                displayFeature2 = new HardwareFoldingFeature(new Bounds(rect), type2, state);
            }
        }
        return displayFeature2;
    }
    
    @Metadata(d1 = { "\u0000:\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0010\u0010\u0006\u001a\u00020\u00072\u0006\u0010\b\u001a\u00020\tH\u0007J\u0015\u0010\n\u001a\u00020\u00072\u0006\u0010\b\u001a\u00020\tH\u0000¢\u0006\u0002\b\u000bJ\u0016\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u000e0\r2\u0006\u0010\u000f\u001a\u00020\u0010H\u0007J\u0018\u0010\u0011\u001a\u00020\u00122\u0006\u0010\b\u001a\u00020\t2\u0006\u0010\u0013\u001a\u00020\u0007H\u0007J \u0010\u0014\u001a\u00020\u00122\u0006\u0010\u000f\u001a\u00020\u00102\u000e\u0010\u0015\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u000e0\rH\u0007R\u0016\u0010\u0003\u001a\n \u0005*\u0004\u0018\u00010\u00040\u0004X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u0016" }, d2 = { "Landroidx/window/layout/adapter/sidecar/SidecarAdapter$Companion;", "", "()V", "TAG", "", "kotlin.jvm.PlatformType", "getRawSidecarDevicePosture", "", "sidecarDeviceState", "Landroidx/window/sidecar/SidecarDeviceState;", "getSidecarDevicePosture", "getSidecarDevicePosture$window_release", "getSidecarDisplayFeatures", "", "Landroidx/window/sidecar/SidecarDisplayFeature;", "info", "Landroidx/window/sidecar/SidecarWindowLayoutInfo;", "setSidecarDevicePosture", "", "posture", "setSidecarDisplayFeatures", "displayFeatures", "window_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
    public static final class Companion
    {
        private Companion() {
        }
        
        public final int getRawSidecarDevicePosture(final SidecarDeviceState obj) {
            Intrinsics.checkNotNullParameter((Object)obj, "sidecarDeviceState");
            try {
                return obj.posture;
            }
            catch (final NoSuchFieldError noSuchFieldError) {
                try {
                    final Object invoke = SidecarDeviceState.class.getMethod("getPosture", (Class<?>[])new Class[0]).invoke(obj, new Object[0]);
                    Intrinsics.checkNotNull(invoke, "null cannot be cast to non-null type kotlin.Int");
                    return (int)invoke;
                }
                catch (final NoSuchMethodException | IllegalAccessException | InvocationTargetException ex) {
                    return 0;
                }
            }
        }
        
        public final int getSidecarDevicePosture$window_release(final SidecarDeviceState sidecarDeviceState) {
            Intrinsics.checkNotNullParameter((Object)sidecarDeviceState, "sidecarDeviceState");
            final int rawSidecarDevicePosture = this.getRawSidecarDevicePosture(sidecarDeviceState);
            int n;
            if (rawSidecarDevicePosture < 0 || (n = rawSidecarDevicePosture) > 4) {
                n = 0;
            }
            return n;
        }
        
        public final List<SidecarDisplayFeature> getSidecarDisplayFeatures(final SidecarWindowLayoutInfo obj) {
            Intrinsics.checkNotNullParameter((Object)obj, "info");
            try {
                List list;
                if ((list = obj.displayFeatures) == null) {
                    list = CollectionsKt.emptyList();
                }
                return list;
            }
            catch (final NoSuchFieldError noSuchFieldError) {
                try {
                    final Object invoke = SidecarWindowLayoutInfo.class.getMethod("getDisplayFeatures", (Class<?>[])new Class[0]).invoke(obj, new Object[0]);
                    Intrinsics.checkNotNull(invoke, "null cannot be cast to non-null type kotlin.collections.List<androidx.window.sidecar.SidecarDisplayFeature>");
                    return (List<SidecarDisplayFeature>)invoke;
                }
                catch (final NoSuchMethodException | IllegalAccessException | InvocationTargetException ex) {
                    return CollectionsKt.emptyList();
                }
            }
        }
        
        public final void setSidecarDevicePosture(final SidecarDeviceState sidecarDeviceState, final int posture) {
            Intrinsics.checkNotNullParameter((Object)sidecarDeviceState, "sidecarDeviceState");
            try {
                sidecarDeviceState.posture = posture;
                return;
            }
            catch (final NoSuchFieldError noSuchFieldError) {
                final Class<SidecarDeviceState> clazz = SidecarDeviceState.class;
                final String s = "setPosture";
                final int n = 1;
                final Class[] array = new Class[n];
                final int n2 = 0;
                final Class<Integer> clazz2 = Integer.TYPE;
                array[n2] = clazz2;
                final Method method = clazz.getMethod(s, (Class[])array);
                final SidecarDeviceState sidecarDeviceState2 = sidecarDeviceState;
                final int n3 = 1;
                final Object[] array2 = new Object[n3];
                final int n4 = 0;
                final int n5 = posture;
                final Integer n6 = n5;
                array2[n4] = n6;
                method.invoke(sidecarDeviceState2, array2);
            }
            try {
                final Class<SidecarDeviceState> clazz = SidecarDeviceState.class;
                final String s = "setPosture";
                final int n = 1;
                final Class[] array = new Class[n];
                final int n2 = 0;
                final Class<Integer> clazz2 = Integer.TYPE;
                array[n2] = clazz2;
                final Method method = clazz.getMethod(s, (Class[])array);
                final SidecarDeviceState sidecarDeviceState2 = sidecarDeviceState;
                final int n3 = 1;
                final Object[] array2 = new Object[n3];
                final int n4 = 0;
                final int n5 = posture;
                final Integer n6 = n5;
                array2[n4] = n6;
                method.invoke(sidecarDeviceState2, array2);
            }
            catch (final NoSuchMethodException | IllegalAccessException | InvocationTargetException ex) {}
        }
        
        public final void setSidecarDisplayFeatures(final SidecarWindowLayoutInfo sidecarWindowLayoutInfo, final List<SidecarDisplayFeature> displayFeatures) {
            Intrinsics.checkNotNullParameter((Object)sidecarWindowLayoutInfo, "info");
            Intrinsics.checkNotNullParameter((Object)displayFeatures, "displayFeatures");
            try {
                sidecarWindowLayoutInfo.displayFeatures = displayFeatures;
                return;
            }
            catch (final NoSuchFieldError noSuchFieldError) {
                final Class<SidecarWindowLayoutInfo> clazz = SidecarWindowLayoutInfo.class;
                final String s = "setDisplayFeatures";
                final int n = 1;
                final Class[] array = new Class[n];
                final int n2 = 0;
                final Class<List> clazz2 = List.class;
                array[n2] = clazz2;
                final Method method = clazz.getMethod(s, (Class[])array);
                final SidecarWindowLayoutInfo sidecarWindowLayoutInfo2 = sidecarWindowLayoutInfo;
                final int n3 = 1;
                final Object[] array2 = new Object[n3];
                final int n4 = 0;
                final List<SidecarDisplayFeature> list = displayFeatures;
                array2[n4] = list;
                method.invoke(sidecarWindowLayoutInfo2, array2);
            }
            try {
                final Class<SidecarWindowLayoutInfo> clazz = SidecarWindowLayoutInfo.class;
                final String s = "setDisplayFeatures";
                final int n = 1;
                final Class[] array = new Class[n];
                final int n2 = 0;
                final Class<List> clazz2 = List.class;
                array[n2] = clazz2;
                final Method method = clazz.getMethod(s, (Class[])array);
                final SidecarWindowLayoutInfo sidecarWindowLayoutInfo2 = sidecarWindowLayoutInfo;
                final int n3 = 1;
                final Object[] array2 = new Object[n3];
                final int n4 = 0;
                final List<SidecarDisplayFeature> list = displayFeatures;
                array2[n4] = list;
                method.invoke(sidecarWindowLayoutInfo2, array2);
            }
            catch (final NoSuchMethodException | IllegalAccessException | InvocationTargetException ex) {}
        }
    }
}

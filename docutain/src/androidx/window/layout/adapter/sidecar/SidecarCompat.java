// 
// Decompiled by Procyon v0.6.0
// 

package androidx.window.layout.adapter.sidecar;

import android.util.Log;
import java.util.Iterator;
import android.view.View;
import java.lang.ref.WeakReference;
import kotlin.Unit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.WeakHashMap;
import android.text.TextUtils;
import androidx.window.core.Version;
import androidx.window.sidecar.SidecarProvider;
import android.view.WindowManager$LayoutParams;
import android.view.Window;
import android.graphics.Rect;
import java.lang.reflect.Method;
import java.util.ArrayList;
import androidx.window.sidecar.SidecarDisplayFeature;
import androidx.window.sidecar.SidecarInterface$SidecarCallback;
import android.view.View$OnAttachStateChangeListener;
import androidx.window.sidecar.SidecarWindowLayoutInfo;
import androidx.window.sidecar.SidecarDeviceState;
import androidx.window.layout.DisplayFeature;
import java.util.List;
import kotlin.collections.CollectionsKt;
import androidx.window.layout.WindowLayoutInfo;
import androidx.core.content.OnConfigurationChangedProvider;
import java.util.LinkedHashMap;
import androidx.window.core.VerificationMode;
import kotlin.jvm.internal.Intrinsics;
import android.content.Context;
import kotlin.jvm.internal.DefaultConstructorMarker;
import android.os.IBinder;
import androidx.window.sidecar.SidecarInterface;
import android.content.res.Configuration;
import androidx.core.util.Consumer;
import android.app.Activity;
import java.util.Map;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000`\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010%\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0005\b\u0000\u0018\u0000 #2\u00020\u0001:\u0004#$%&B\u000f\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004B\u0019\b\u0007\u0012\b\u0010\u0005\u001a\u0004\u0018\u00010\u0006\u0012\u0006\u0010\u0007\u001a\u00020\b¢\u0006\u0002\u0010\tJ\u0010\u0010\u0015\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\fH\u0007J\u0010\u0010\u0018\u001a\u00020\u00192\u0006\u0010\u0017\u001a\u00020\fH\u0016J\u0010\u0010\u001a\u001a\u00020\u00192\u0006\u0010\u0017\u001a\u00020\fH\u0016J\u0016\u0010\u001b\u001a\u00020\u00192\u0006\u0010\u001c\u001a\u00020\u00142\u0006\u0010\u0017\u001a\u00020\fJ\u0010\u0010\u001d\u001a\u00020\u00192\u0006\u0010\u0017\u001a\u00020\fH\u0002J\u0010\u0010\u001e\u001a\u00020\u00192\u0006\u0010\u000f\u001a\u00020\u001fH\u0016J\u0010\u0010 \u001a\u00020\u00192\u0006\u0010\u0017\u001a\u00020\fH\u0002J\b\u0010!\u001a\u00020\"H\u0017R \u0010\n\u001a\u0014\u0012\u0004\u0012\u00020\f\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u000e0\r0\u000bX\u0082\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\u000f\u001a\u0004\u0018\u00010\u0010X\u0082\u000e¢\u0006\u0002\n\u0000R\u0015\u0010\u0005\u001a\u0004\u0018\u00010\u00068\u0007¢\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\u0012R\u000e\u0010\u0007\u001a\u00020\bX\u0082\u0004¢\u0006\u0002\n\u0000R\u001a\u0010\u0013\u001a\u000e\u0012\u0004\u0012\u00020\u0014\u0012\u0004\u0012\u00020\f0\u000bX\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006'" }, d2 = { "Landroidx/window/layout/adapter/sidecar/SidecarCompat;", "Landroidx/window/layout/adapter/sidecar/ExtensionInterfaceCompat;", "context", "Landroid/content/Context;", "(Landroid/content/Context;)V", "sidecar", "Landroidx/window/sidecar/SidecarInterface;", "sidecarAdapter", "Landroidx/window/layout/adapter/sidecar/SidecarAdapter;", "(Landroidx/window/sidecar/SidecarInterface;Landroidx/window/layout/adapter/sidecar/SidecarAdapter;)V", "componentCallbackMap", "", "Landroid/app/Activity;", "Landroidx/core/util/Consumer;", "Landroid/content/res/Configuration;", "extensionCallback", "Landroidx/window/layout/adapter/sidecar/SidecarCompat$DistinctElementCallback;", "getSidecar", "()Landroidx/window/sidecar/SidecarInterface;", "windowListenerRegisteredContexts", "Landroid/os/IBinder;", "getWindowLayoutInfo", "Landroidx/window/layout/WindowLayoutInfo;", "activity", "onWindowLayoutChangeListenerAdded", "", "onWindowLayoutChangeListenerRemoved", "register", "windowToken", "registerConfigurationChangeListener", "setExtensionCallback", "Landroidx/window/layout/adapter/sidecar/ExtensionInterfaceCompat$ExtensionCallbackInterface;", "unregisterComponentCallback", "validateExtensionInterface", "", "Companion", "DistinctElementCallback", "FirstAttachAdapter", "TranslatingCallback", "window_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
public final class SidecarCompat implements ExtensionInterfaceCompat
{
    public static final Companion Companion;
    private static final String TAG = "SidecarCompat";
    private final Map<Activity, Consumer<Configuration>> componentCallbackMap;
    private DistinctElementCallback extensionCallback;
    private final SidecarInterface sidecar;
    private final SidecarAdapter sidecarAdapter;
    private final Map<IBinder, Activity> windowListenerRegisteredContexts;
    
    static {
        Companion = new Companion(null);
    }
    
    public SidecarCompat(final Context context) {
        Intrinsics.checkNotNullParameter((Object)context, "context");
        this(SidecarCompat.Companion.getSidecarCompat$window_release(context), new SidecarAdapter(null, 1, null));
    }
    
    public SidecarCompat(final SidecarInterface sidecar, final SidecarAdapter sidecarAdapter) {
        Intrinsics.checkNotNullParameter((Object)sidecarAdapter, "sidecarAdapter");
        this.sidecar = sidecar;
        this.sidecarAdapter = sidecarAdapter;
        this.windowListenerRegisteredContexts = new LinkedHashMap<IBinder, Activity>();
        this.componentCallbackMap = new LinkedHashMap<Activity, Consumer<Configuration>>();
    }
    
    public static final /* synthetic */ DistinctElementCallback access$getExtensionCallback$p(final SidecarCompat sidecarCompat) {
        return sidecarCompat.extensionCallback;
    }
    
    public static final /* synthetic */ SidecarAdapter access$getSidecarAdapter$p(final SidecarCompat sidecarCompat) {
        return sidecarCompat.sidecarAdapter;
    }
    
    public static final /* synthetic */ Map access$getWindowListenerRegisteredContexts$p(final SidecarCompat sidecarCompat) {
        return sidecarCompat.windowListenerRegisteredContexts;
    }
    
    private final void registerConfigurationChangeListener(final Activity activity) {
        if (this.componentCallbackMap.get(activity) == null && activity instanceof OnConfigurationChangedProvider) {
            final SidecarCompat$$ExternalSyntheticLambda0 sidecarCompat$$ExternalSyntheticLambda0 = new SidecarCompat$$ExternalSyntheticLambda0(this, activity);
            this.componentCallbackMap.put(activity, sidecarCompat$$ExternalSyntheticLambda0);
            ((OnConfigurationChangedProvider)activity).addOnConfigurationChangedListener(sidecarCompat$$ExternalSyntheticLambda0);
        }
    }
    
    private static final void registerConfigurationChangeListener$lambda$0(final SidecarCompat sidecarCompat, final Activity activity, final Configuration configuration) {
        Intrinsics.checkNotNullParameter((Object)sidecarCompat, "this$0");
        Intrinsics.checkNotNullParameter((Object)activity, "$activity");
        final DistinctElementCallback extensionCallback = sidecarCompat.extensionCallback;
        if (extensionCallback != null) {
            extensionCallback.onWindowLayoutChanged(activity, sidecarCompat.getWindowLayoutInfo(activity));
        }
    }
    
    private final void unregisterComponentCallback(final Activity activity) {
        final Consumer consumer = this.componentCallbackMap.get(activity);
        if (consumer == null) {
            return;
        }
        if (activity instanceof OnConfigurationChangedProvider) {
            ((OnConfigurationChangedProvider)activity).removeOnConfigurationChangedListener(consumer);
        }
        this.componentCallbackMap.remove(activity);
    }
    
    public final SidecarInterface getSidecar() {
        return this.sidecar;
    }
    
    public final WindowLayoutInfo getWindowLayoutInfo(final Activity activity) {
        Intrinsics.checkNotNullParameter((Object)activity, "activity");
        final IBinder activityWindowToken$window_release = SidecarCompat.Companion.getActivityWindowToken$window_release(activity);
        if (activityWindowToken$window_release == null) {
            return new WindowLayoutInfo(CollectionsKt.emptyList());
        }
        final SidecarInterface sidecar = this.sidecar;
        SidecarWindowLayoutInfo windowLayoutInfo;
        if (sidecar != null) {
            windowLayoutInfo = sidecar.getWindowLayoutInfo(activityWindowToken$window_release);
        }
        else {
            windowLayoutInfo = null;
        }
        final SidecarAdapter sidecarAdapter = this.sidecarAdapter;
        final SidecarInterface sidecar2 = this.sidecar;
        SidecarDeviceState deviceState;
        if (sidecar2 == null || (deviceState = sidecar2.getDeviceState()) == null) {
            deviceState = new SidecarDeviceState();
        }
        return sidecarAdapter.translate(windowLayoutInfo, deviceState);
    }
    
    @Override
    public void onWindowLayoutChangeListenerAdded(final Activity activity) {
        Intrinsics.checkNotNullParameter((Object)activity, "activity");
        final IBinder activityWindowToken$window_release = SidecarCompat.Companion.getActivityWindowToken$window_release(activity);
        if (activityWindowToken$window_release != null) {
            this.register(activityWindowToken$window_release, activity);
        }
        else {
            activity.getWindow().getDecorView().addOnAttachStateChangeListener((View$OnAttachStateChangeListener)new FirstAttachAdapter(this, activity));
        }
    }
    
    @Override
    public void onWindowLayoutChangeListenerRemoved(final Activity activity) {
        Intrinsics.checkNotNullParameter((Object)activity, "activity");
        final IBinder activityWindowToken$window_release = SidecarCompat.Companion.getActivityWindowToken$window_release(activity);
        if (activityWindowToken$window_release == null) {
            return;
        }
        final SidecarInterface sidecar = this.sidecar;
        if (sidecar != null) {
            sidecar.onWindowLayoutChangeListenerRemoved(activityWindowToken$window_release);
        }
        this.unregisterComponentCallback(activity);
        final DistinctElementCallback extensionCallback = this.extensionCallback;
        if (extensionCallback != null) {
            extensionCallback.clearWindowLayoutInfo(activity);
        }
        final boolean b = this.windowListenerRegisteredContexts.size() == 1;
        this.windowListenerRegisteredContexts.remove(activityWindowToken$window_release);
        if (b) {
            final SidecarInterface sidecar2 = this.sidecar;
            if (sidecar2 != null) {
                sidecar2.onDeviceStateListenersChanged(true);
            }
        }
    }
    
    public final void register(final IBinder binder, final Activity activity) {
        Intrinsics.checkNotNullParameter((Object)binder, "windowToken");
        Intrinsics.checkNotNullParameter((Object)activity, "activity");
        this.windowListenerRegisteredContexts.put(binder, activity);
        final SidecarInterface sidecar = this.sidecar;
        if (sidecar != null) {
            sidecar.onWindowLayoutChangeListenerAdded(binder);
        }
        if (this.windowListenerRegisteredContexts.size() == 1) {
            final SidecarInterface sidecar2 = this.sidecar;
            if (sidecar2 != null) {
                sidecar2.onDeviceStateListenersChanged(false);
            }
        }
        final DistinctElementCallback extensionCallback = this.extensionCallback;
        if (extensionCallback != null) {
            extensionCallback.onWindowLayoutChanged(activity, this.getWindowLayoutInfo(activity));
        }
        this.registerConfigurationChangeListener(activity);
    }
    
    @Override
    public void setExtensionCallback(final ExtensionCallbackInterface extensionCallbackInterface) {
        Intrinsics.checkNotNullParameter((Object)extensionCallbackInterface, "extensionCallback");
        this.extensionCallback = new DistinctElementCallback(extensionCallbackInterface);
        final SidecarInterface sidecar = this.sidecar;
        if (sidecar != null) {
            sidecar.setSidecarCallback((SidecarInterface$SidecarCallback)new DistinctElementSidecarCallback(this.sidecarAdapter, (SidecarInterface$SidecarCallback)new TranslatingCallback()));
        }
    }
    
    @Override
    public boolean validateExtensionInterface() {
        boolean b = false;
        try {
            final SidecarInterface sidecar = this.sidecar;
            final Object o = null;
            Method method = null;
            Label_0045: {
                if (sidecar != null) {
                    final Class<? extends SidecarInterface> class1 = sidecar.getClass();
                    if (class1 != null) {
                        method = class1.getMethod("setSidecarCallback", SidecarInterface$SidecarCallback.class);
                        break Label_0045;
                    }
                }
                method = null;
            }
            Class<?> returnType;
            if (method != null) {
                returnType = method.getReturnType();
            }
            else {
                returnType = null;
            }
            if (!Intrinsics.areEqual((Object)returnType, (Object)Void.TYPE)) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Illegal return type for 'setSidecarCallback': ");
                sb.append(returnType);
                throw new NoSuchMethodException(sb.toString());
            }
            final SidecarInterface sidecar2 = this.sidecar;
            if (sidecar2 != null) {
                sidecar2.getDeviceState();
            }
            final SidecarInterface sidecar3 = this.sidecar;
            if (sidecar3 != null) {
                sidecar3.onDeviceStateListenersChanged(true);
            }
            final SidecarInterface sidecar4 = this.sidecar;
            Method method2 = null;
            Label_0142: {
                if (sidecar4 != null) {
                    final Class<? extends SidecarInterface> class2 = sidecar4.getClass();
                    if (class2 != null) {
                        method2 = class2.getMethod("getWindowLayoutInfo", IBinder.class);
                        break Label_0142;
                    }
                }
                method2 = null;
            }
            Class<?> returnType2;
            if (method2 != null) {
                returnType2 = method2.getReturnType();
            }
            else {
                returnType2 = null;
            }
            if (!Intrinsics.areEqual((Object)returnType2, (Object)SidecarWindowLayoutInfo.class)) {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("Illegal return type for 'getWindowLayoutInfo': ");
                sb2.append(returnType2);
                throw new NoSuchMethodException(sb2.toString());
            }
            final SidecarInterface sidecar5 = this.sidecar;
            Method method3 = null;
            Label_0207: {
                if (sidecar5 != null) {
                    final Class<? extends SidecarInterface> class3 = sidecar5.getClass();
                    if (class3 != null) {
                        method3 = class3.getMethod("onWindowLayoutChangeListenerAdded", IBinder.class);
                        break Label_0207;
                    }
                }
                method3 = null;
            }
            Class<?> returnType3;
            if (method3 != null) {
                returnType3 = method3.getReturnType();
            }
            else {
                returnType3 = null;
            }
            if (!Intrinsics.areEqual((Object)returnType3, (Object)Void.TYPE)) {
                final StringBuilder sb3 = new StringBuilder();
                sb3.append("Illegal return type for 'onWindowLayoutChangeListenerAdded': ");
                sb3.append(returnType3);
                throw new NoSuchMethodException(sb3.toString());
            }
            final SidecarInterface sidecar6 = this.sidecar;
            Method method4 = null;
            Label_0272: {
                if (sidecar6 != null) {
                    final Class<? extends SidecarInterface> class4 = sidecar6.getClass();
                    if (class4 != null) {
                        method4 = class4.getMethod("onWindowLayoutChangeListenerRemoved", IBinder.class);
                        break Label_0272;
                    }
                }
                method4 = null;
            }
            Object returnType4 = o;
            if (method4 != null) {
                returnType4 = method4.getReturnType();
            }
            if (Intrinsics.areEqual(returnType4, (Object)Void.TYPE)) {
                final SidecarDeviceState sidecarDeviceState = new SidecarDeviceState();
                try {
                    sidecarDeviceState.posture = 3;
                }
                catch (final NoSuchFieldError noSuchFieldError) {
                    SidecarDeviceState.class.getMethod("setPosture", Integer.TYPE).invoke(sidecarDeviceState, 3);
                    final Object invoke = SidecarDeviceState.class.getMethod("getPosture", (Class<?>[])new Class[0]).invoke(sidecarDeviceState, new Object[0]);
                    Intrinsics.checkNotNull(invoke, "null cannot be cast to non-null type kotlin.Int");
                    if ((int)invoke != 3) {
                        throw new Exception("Invalid device posture getter/setter");
                    }
                }
                final SidecarDisplayFeature sidecarDisplayFeature = new SidecarDisplayFeature();
                final Rect rect = sidecarDisplayFeature.getRect();
                Intrinsics.checkNotNullExpressionValue((Object)rect, "displayFeature.rect");
                sidecarDisplayFeature.setRect(rect);
                sidecarDisplayFeature.getType();
                sidecarDisplayFeature.setType(1);
                final SidecarWindowLayoutInfo sidecarWindowLayoutInfo = new SidecarWindowLayoutInfo();
                try {
                    final List displayFeatures = sidecarWindowLayoutInfo.displayFeatures;
                    return b;
                }
                catch (final NoSuchFieldError noSuchFieldError2) {
                    final List list = new ArrayList();
                    list.add(sidecarDisplayFeature);
                    SidecarWindowLayoutInfo.class.getMethod("setDisplayFeatures", List.class).invoke(sidecarWindowLayoutInfo, list);
                    final Object invoke2 = SidecarWindowLayoutInfo.class.getMethod("getDisplayFeatures", (Class<?>[])new Class[0]).invoke(sidecarWindowLayoutInfo, new Object[0]);
                    Intrinsics.checkNotNull(invoke2, "null cannot be cast to non-null type kotlin.collections.List<androidx.window.sidecar.SidecarDisplayFeature>");
                    if (Intrinsics.areEqual((Object)list, (Object)invoke2)) {
                        return b;
                    }
                    throw new Exception("Invalid display feature getter/setter");
                }
                throw new Exception("Invalid device posture getter/setter");
            }
            final StringBuilder sb4 = new StringBuilder();
            sb4.append("Illegal return type for 'onWindowLayoutChangeListenerRemoved': ");
            sb4.append(returnType4);
            throw new NoSuchMethodException(sb4.toString());
        }
        finally {
            b = false;
        }
        return b;
    }
    
    @Metadata(d1 = { "\u00006\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0019\u0010\t\u001a\u0004\u0018\u00010\n2\b\u0010\u000b\u001a\u0004\u0018\u00010\fH\u0000¢\u0006\u0002\b\rJ\u0017\u0010\u000e\u001a\u0004\u0018\u00010\u000f2\u0006\u0010\u0010\u001a\u00020\u0011H\u0000¢\u0006\u0002\b\u0012R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082T¢\u0006\u0002\n\u0000R\u0013\u0010\u0005\u001a\u0004\u0018\u00010\u00068F¢\u0006\u0006\u001a\u0004\b\u0007\u0010\b¨\u0006\u0013" }, d2 = { "Landroidx/window/layout/adapter/sidecar/SidecarCompat$Companion;", "", "()V", "TAG", "", "sidecarVersion", "Landroidx/window/core/Version;", "getSidecarVersion", "()Landroidx/window/core/Version;", "getActivityWindowToken", "Landroid/os/IBinder;", "activity", "Landroid/app/Activity;", "getActivityWindowToken$window_release", "getSidecarCompat", "Landroidx/window/sidecar/SidecarInterface;", "context", "Landroid/content/Context;", "getSidecarCompat$window_release", "window_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
    public static final class Companion
    {
        private Companion() {
        }
        
        public final IBinder getActivityWindowToken$window_release(final Activity activity) {
            if (activity != null) {
                final Window window = activity.getWindow();
                if (window != null) {
                    final WindowManager$LayoutParams attributes = window.getAttributes();
                    if (attributes != null) {
                        return attributes.token;
                    }
                }
            }
            return null;
        }
        
        public final SidecarInterface getSidecarCompat$window_release(final Context context) {
            Intrinsics.checkNotNullParameter((Object)context, "context");
            return SidecarProvider.getSidecarImpl(context.getApplicationContext());
        }
        
        public final Version getSidecarVersion() {
            final Version version = null;
            try {
                final String apiVersion = SidecarProvider.getApiVersion();
                Version parse = version;
                if (!TextUtils.isEmpty((CharSequence)apiVersion)) {
                    parse = Version.Companion.parse(apiVersion);
                }
                return parse;
            }
            catch (final NoClassDefFoundError | UnsupportedOperationException ex) {
                return version;
            }
        }
    }
    
    @Metadata(d1 = { "\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\b\u0002\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0001¢\u0006\u0002\u0010\u0003J\u000e\u0010\n\u001a\u00020\u000b2\u0006\u0010\f\u001a\u00020\u0006J\u0018\u0010\r\u001a\u00020\u000b2\u0006\u0010\f\u001a\u00020\u00062\u0006\u0010\u000e\u001a\u00020\u0007H\u0016R\u001c\u0010\u0004\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00070\u00058\u0002X\u0083\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0001X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u000f" }, d2 = { "Landroidx/window/layout/adapter/sidecar/SidecarCompat$DistinctElementCallback;", "Landroidx/window/layout/adapter/sidecar/ExtensionInterfaceCompat$ExtensionCallbackInterface;", "callbackInterface", "(Landroidx/window/layout/adapter/sidecar/ExtensionInterfaceCompat$ExtensionCallbackInterface;)V", "activityWindowLayoutInfo", "Ljava/util/WeakHashMap;", "Landroid/app/Activity;", "Landroidx/window/layout/WindowLayoutInfo;", "lock", "Ljava/util/concurrent/locks/ReentrantLock;", "clearWindowLayoutInfo", "", "activity", "onWindowLayoutChanged", "newLayout", "window_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
    private static final class DistinctElementCallback implements ExtensionCallbackInterface
    {
        private final WeakHashMap<Activity, WindowLayoutInfo> activityWindowLayoutInfo;
        private final ExtensionCallbackInterface callbackInterface;
        private final ReentrantLock lock;
        
        public DistinctElementCallback(final ExtensionCallbackInterface callbackInterface) {
            Intrinsics.checkNotNullParameter((Object)callbackInterface, "callbackInterface");
            this.callbackInterface = callbackInterface;
            this.lock = new ReentrantLock();
            this.activityWindowLayoutInfo = new WeakHashMap<Activity, WindowLayoutInfo>();
        }
        
        public final void clearWindowLayoutInfo(final Activity activity) {
            Intrinsics.checkNotNullParameter((Object)activity, "activity");
            final Lock lock = this.lock;
            lock.lock();
            try {
                this.activityWindowLayoutInfo.put(activity, (Object)null);
                final Unit instance = Unit.INSTANCE;
            }
            finally {
                lock.unlock();
            }
        }
        
        @Override
        public void onWindowLayoutChanged(final Activity activity, final WindowLayoutInfo value) {
            Intrinsics.checkNotNullParameter((Object)activity, "activity");
            Intrinsics.checkNotNullParameter((Object)value, "newLayout");
            final Lock lock = this.lock;
            lock.lock();
            try {
                if (Intrinsics.areEqual((Object)value, (Object)this.activityWindowLayoutInfo.get(activity))) {
                    return;
                }
                final WindowLayoutInfo windowLayoutInfo = this.activityWindowLayoutInfo.put(activity, value);
                lock.unlock();
                this.callbackInterface.onWindowLayoutChanged(activity, value);
            }
            finally {
                lock.unlock();
            }
        }
    }
    
    @Metadata(d1 = { "\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0002\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\u0010\u0010\n\u001a\u00020\u000b2\u0006\u0010\f\u001a\u00020\rH\u0016J\u0010\u0010\u000e\u001a\u00020\u000b2\u0006\u0010\f\u001a\u00020\rH\u0016R\u001c\u0010\u0007\u001a\u0010\u0012\f\u0012\n \t*\u0004\u0018\u00010\u00050\u00050\bX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u000f" }, d2 = { "Landroidx/window/layout/adapter/sidecar/SidecarCompat$FirstAttachAdapter;", "Landroid/view/View$OnAttachStateChangeListener;", "sidecarCompat", "Landroidx/window/layout/adapter/sidecar/SidecarCompat;", "activity", "Landroid/app/Activity;", "(Landroidx/window/layout/adapter/sidecar/SidecarCompat;Landroid/app/Activity;)V", "activityWeakReference", "Ljava/lang/ref/WeakReference;", "kotlin.jvm.PlatformType", "onViewAttachedToWindow", "", "view", "Landroid/view/View;", "onViewDetachedFromWindow", "window_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
    private static final class FirstAttachAdapter implements View$OnAttachStateChangeListener
    {
        private final WeakReference<Activity> activityWeakReference;
        private final SidecarCompat sidecarCompat;
        
        public FirstAttachAdapter(final SidecarCompat sidecarCompat, final Activity referent) {
            Intrinsics.checkNotNullParameter((Object)sidecarCompat, "sidecarCompat");
            Intrinsics.checkNotNullParameter((Object)referent, "activity");
            this.sidecarCompat = sidecarCompat;
            this.activityWeakReference = new WeakReference<Activity>(referent);
        }
        
        public void onViewAttachedToWindow(final View view) {
            Intrinsics.checkNotNullParameter((Object)view, "view");
            view.removeOnAttachStateChangeListener((View$OnAttachStateChangeListener)this);
            final Activity activity = this.activityWeakReference.get();
            final IBinder activityWindowToken$window_release = SidecarCompat.Companion.getActivityWindowToken$window_release(activity);
            if (activity == null) {
                return;
            }
            if (activityWindowToken$window_release == null) {
                return;
            }
            this.sidecarCompat.register(activityWindowToken$window_release, activity);
        }
        
        public void onViewDetachedFromWindow(final View view) {
            Intrinsics.checkNotNullParameter((Object)view, "view");
        }
    }
    
    @Metadata(d1 = { "\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0080\u0004\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0017J\u0018\u0010\u0007\u001a\u00020\u00042\u0006\u0010\b\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000bH\u0017¨\u0006\f" }, d2 = { "Landroidx/window/layout/adapter/sidecar/SidecarCompat$TranslatingCallback;", "Landroidx/window/sidecar/SidecarInterface$SidecarCallback;", "(Landroidx/window/layout/adapter/sidecar/SidecarCompat;)V", "onDeviceStateChanged", "", "newDeviceState", "Landroidx/window/sidecar/SidecarDeviceState;", "onWindowLayoutChanged", "windowToken", "Landroid/os/IBinder;", "newLayout", "Landroidx/window/sidecar/SidecarWindowLayoutInfo;", "window_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
    public final class TranslatingCallback implements SidecarInterface$SidecarCallback
    {
        final SidecarCompat this$0;
        
        public TranslatingCallback(final SidecarCompat this$0) {
            this.this$0 = this$0;
        }
        
        public void onDeviceStateChanged(final SidecarDeviceState sidecarDeviceState) {
            Intrinsics.checkNotNullParameter((Object)sidecarDeviceState, "newDeviceState");
            final Iterable iterable = SidecarCompat.access$getWindowListenerRegisteredContexts$p(this.this$0).values();
            final SidecarCompat this$0 = this.this$0;
            for (final Activity activity : iterable) {
                final IBinder activityWindowToken$window_release = SidecarCompat.Companion.getActivityWindowToken$window_release(activity);
                SidecarWindowLayoutInfo windowLayoutInfo = null;
                if (activityWindowToken$window_release != null) {
                    final SidecarInterface sidecar = this$0.getSidecar();
                    windowLayoutInfo = windowLayoutInfo;
                    if (sidecar != null) {
                        windowLayoutInfo = sidecar.getWindowLayoutInfo(activityWindowToken$window_release);
                    }
                }
                final DistinctElementCallback access$getExtensionCallback$p = SidecarCompat.access$getExtensionCallback$p(this$0);
                if (access$getExtensionCallback$p != null) {
                    access$getExtensionCallback$p.onWindowLayoutChanged(activity, SidecarCompat.access$getSidecarAdapter$p(this$0).translate(windowLayoutInfo, sidecarDeviceState));
                }
            }
        }
        
        public void onWindowLayoutChanged(final IBinder binder, final SidecarWindowLayoutInfo sidecarWindowLayoutInfo) {
            Intrinsics.checkNotNullParameter((Object)binder, "windowToken");
            Intrinsics.checkNotNullParameter((Object)sidecarWindowLayoutInfo, "newLayout");
            final Activity activity = SidecarCompat.access$getWindowListenerRegisteredContexts$p(this.this$0).get(binder);
            if (activity == null) {
                Log.w("SidecarCompat", "Unable to resolve activity from window token. Missing a call to #onWindowLayoutChangeListenerAdded()?");
                return;
            }
            final SidecarAdapter access$getSidecarAdapter$p = SidecarCompat.access$getSidecarAdapter$p(this.this$0);
            final SidecarInterface sidecar = this.this$0.getSidecar();
            SidecarDeviceState deviceState;
            if (sidecar == null || (deviceState = sidecar.getDeviceState()) == null) {
                deviceState = new SidecarDeviceState();
            }
            final WindowLayoutInfo translate = access$getSidecarAdapter$p.translate(sidecarWindowLayoutInfo, deviceState);
            final DistinctElementCallback access$getExtensionCallback$p = SidecarCompat.access$getExtensionCallback$p(this.this$0);
            if (access$getExtensionCallback$p != null) {
                access$getExtensionCallback$p.onWindowLayoutChanged(activity, translate);
            }
        }
    }
}

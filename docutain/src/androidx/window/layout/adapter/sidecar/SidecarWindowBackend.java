// 
// Decompiled by Procyon v0.6.0
// 

package androidx.window.layout.adapter.sidecar;

import androidx.window.core.Version;
import java.util.ArrayList;
import kotlin.Unit;
import androidx.window.layout.DisplayFeature;
import java.util.List;
import kotlin.collections.CollectionsKt;
import java.util.concurrent.locks.Lock;
import androidx.window.layout.WindowLayoutInfo;
import androidx.core.util.Consumer;
import java.util.concurrent.Executor;
import android.content.Context;
import java.util.Iterator;
import kotlin.jvm.internal.Intrinsics;
import java.util.Collection;
import android.app.Activity;
import kotlin.jvm.internal.DefaultConstructorMarker;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.locks.ReentrantLock;
import kotlin.Metadata;
import androidx.window.layout.adapter.WindowBackend;

@Metadata(d1 = { "\u0000J\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0000\u0018\u0000 \u001e2\u00020\u0001:\u0003\u001e\u001f B\u0011\b\u0007\u0012\b\u0010\u0002\u001a\u0004\u0018\u00010\u0003¢\u0006\u0002\u0010\u0004J\u0010\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u0012H\u0003J\u0010\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0011\u001a\u00020\u0012H\u0002J&\u0010\u0015\u001a\u00020\u00102\u0006\u0010\u0016\u001a\u00020\u00172\u0006\u0010\u0018\u001a\u00020\u00192\f\u0010\u001a\u001a\b\u0012\u0004\u0012\u00020\u001c0\u001bH\u0016J\u0016\u0010\u001d\u001a\u00020\u00102\f\u0010\u001a\u001a\b\u0012\u0004\u0012\u00020\u001c0\u001bH\u0016R \u0010\u0002\u001a\u0004\u0018\u00010\u00038\u0006@\u0006X\u0087\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\u0004R\"\u0010\b\u001a\b\u0012\u0004\u0012\u00020\n0\t8\u0006X\u0087\u0004¢\u0006\u000e\n\u0000\u0012\u0004\b\u000b\u0010\f\u001a\u0004\b\r\u0010\u000e¨\u0006!" }, d2 = { "Landroidx/window/layout/adapter/sidecar/SidecarWindowBackend;", "Landroidx/window/layout/adapter/WindowBackend;", "windowExtension", "Landroidx/window/layout/adapter/sidecar/ExtensionInterfaceCompat;", "(Landroidx/window/layout/adapter/sidecar/ExtensionInterfaceCompat;)V", "getWindowExtension", "()Landroidx/window/layout/adapter/sidecar/ExtensionInterfaceCompat;", "setWindowExtension", "windowLayoutChangeCallbacks", "Ljava/util/concurrent/CopyOnWriteArrayList;", "Landroidx/window/layout/adapter/sidecar/SidecarWindowBackend$WindowLayoutChangeCallbackWrapper;", "getWindowLayoutChangeCallbacks$annotations", "()V", "getWindowLayoutChangeCallbacks", "()Ljava/util/concurrent/CopyOnWriteArrayList;", "callbackRemovedForActivity", "", "activity", "Landroid/app/Activity;", "isActivityRegistered", "", "registerLayoutChangeCallback", "context", "Landroid/content/Context;", "executor", "Ljava/util/concurrent/Executor;", "callback", "Landroidx/core/util/Consumer;", "Landroidx/window/layout/WindowLayoutInfo;", "unregisterLayoutChangeCallback", "Companion", "ExtensionListenerImpl", "WindowLayoutChangeCallbackWrapper", "window_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
public final class SidecarWindowBackend implements WindowBackend
{
    public static final Companion Companion;
    public static final boolean DEBUG = false;
    private static final String TAG = "WindowServer";
    private static volatile SidecarWindowBackend globalInstance;
    private static final ReentrantLock globalLock;
    private ExtensionInterfaceCompat windowExtension;
    private final CopyOnWriteArrayList<WindowLayoutChangeCallbackWrapper> windowLayoutChangeCallbacks;
    
    static {
        Companion = new Companion(null);
        globalLock = new ReentrantLock();
    }
    
    public SidecarWindowBackend(ExtensionInterfaceCompat windowExtension) {
        this.windowExtension = windowExtension;
        this.windowLayoutChangeCallbacks = new CopyOnWriteArrayList<WindowLayoutChangeCallbackWrapper>();
        windowExtension = this.windowExtension;
        if (windowExtension != null) {
            windowExtension.setExtensionCallback((ExtensionInterfaceCompat.ExtensionCallbackInterface)new ExtensionListenerImpl());
        }
    }
    
    public static final /* synthetic */ SidecarWindowBackend access$getGlobalInstance$cp() {
        return SidecarWindowBackend.globalInstance;
    }
    
    public static final /* synthetic */ ReentrantLock access$getGlobalLock$cp() {
        return SidecarWindowBackend.globalLock;
    }
    
    public static final /* synthetic */ void access$setGlobalInstance$cp(final SidecarWindowBackend globalInstance) {
        SidecarWindowBackend.globalInstance = globalInstance;
    }
    
    private final void callbackRemovedForActivity(final Activity activity) {
        final Iterable iterable = this.windowLayoutChangeCallbacks;
        final boolean b = iterable instanceof Collection;
        final boolean b2 = false;
        int n = 0;
        Label_0084: {
            if (b && ((Collection)iterable).isEmpty()) {
                n = (b2 ? 1 : 0);
            }
            else {
                final Iterator iterator = iterable.iterator();
                do {
                    n = (b2 ? 1 : 0);
                    if (iterator.hasNext()) {
                        continue;
                    }
                    break Label_0084;
                } while (!Intrinsics.areEqual((Object)((WindowLayoutChangeCallbackWrapper)iterator.next()).getActivity(), (Object)activity));
                n = 1;
            }
        }
        if (n != 0) {
            return;
        }
        final ExtensionInterfaceCompat windowExtension = this.windowExtension;
        if (windowExtension != null) {
            windowExtension.onWindowLayoutChangeListenerRemoved(activity);
        }
    }
    
    private final boolean isActivityRegistered(final Activity activity) {
        final Iterable iterable = this.windowLayoutChangeCallbacks;
        final boolean b = iterable instanceof Collection;
        final boolean b2 = false;
        boolean b3;
        if (b && ((Collection)iterable).isEmpty()) {
            b3 = b2;
        }
        else {
            final Iterator iterator = iterable.iterator();
            do {
                b3 = b2;
                if (iterator.hasNext()) {
                    continue;
                }
                return b3;
            } while (!Intrinsics.areEqual((Object)((WindowLayoutChangeCallbackWrapper)iterator.next()).getActivity(), (Object)activity));
            b3 = true;
        }
        return b3;
    }
    
    public final ExtensionInterfaceCompat getWindowExtension() {
        return this.windowExtension;
    }
    
    public final CopyOnWriteArrayList<WindowLayoutChangeCallbackWrapper> getWindowLayoutChangeCallbacks() {
        return this.windowLayoutChangeCallbacks;
    }
    
    @Override
    public void registerLayoutChangeCallback(final Context context, final Executor executor, final Consumer<WindowLayoutInfo> consumer) {
        Intrinsics.checkNotNullParameter((Object)context, "context");
        Intrinsics.checkNotNullParameter((Object)executor, "executor");
        Intrinsics.checkNotNullParameter((Object)consumer, "callback");
        final boolean b = context instanceof Activity;
        Object instance = null;
        final WindowLayoutInfo windowLayoutInfo = null;
        Activity activity;
        if (b) {
            activity = (Activity)context;
        }
        else {
            activity = null;
        }
        if (activity != null) {
            instance = SidecarWindowBackend.globalLock;
            ((Lock)instance).lock();
            try {
                final ExtensionInterfaceCompat windowExtension = this.windowExtension;
                if (windowExtension == null) {
                    consumer.accept(new WindowLayoutInfo(CollectionsKt.emptyList()));
                    return;
                }
                final boolean activityRegistered = this.isActivityRegistered(activity);
                final WindowLayoutChangeCallbackWrapper e = new WindowLayoutChangeCallbackWrapper(activity, executor, consumer);
                this.windowLayoutChangeCallbacks.add(e);
                Label_0227: {
                    if (activityRegistered) {
                        while (true) {
                            for (final Object next : this.windowLayoutChangeCallbacks) {
                                if (Intrinsics.areEqual((Object)activity, (Object)((WindowLayoutChangeCallbackWrapper)next).getActivity())) {
                                    final WindowLayoutChangeCallbackWrapper windowLayoutChangeCallbackWrapper = (WindowLayoutChangeCallbackWrapper)next;
                                    final WindowLayoutChangeCallbackWrapper windowLayoutChangeCallbackWrapper2 = windowLayoutChangeCallbackWrapper;
                                    WindowLayoutInfo lastInfo = windowLayoutInfo;
                                    if (windowLayoutChangeCallbackWrapper2 != null) {
                                        lastInfo = windowLayoutChangeCallbackWrapper2.getLastInfo();
                                    }
                                    if (lastInfo != null) {
                                        e.accept(lastInfo);
                                    }
                                    break Label_0227;
                                }
                            }
                            final WindowLayoutChangeCallbackWrapper windowLayoutChangeCallbackWrapper = null;
                            continue;
                        }
                    }
                    windowExtension.onWindowLayoutChangeListenerAdded(activity);
                }
                final Unit instance2 = Unit.INSTANCE;
                ((Lock)instance).unlock();
                instance = Unit.INSTANCE;
            }
            finally {
                ((Lock)instance).unlock();
            }
        }
        if (instance == null) {
            final SidecarWindowBackend sidecarWindowBackend = this;
            consumer.accept(new WindowLayoutInfo(CollectionsKt.emptyList()));
        }
    }
    
    public final void setWindowExtension(final ExtensionInterfaceCompat windowExtension) {
        this.windowExtension = windowExtension;
    }
    
    @Override
    public void unregisterLayoutChangeCallback(final Consumer<WindowLayoutInfo> consumer) {
        Intrinsics.checkNotNullParameter((Object)consumer, "callback");
        synchronized (SidecarWindowBackend.globalLock) {
            if (this.windowExtension == null) {
                return;
            }
            final List list = new ArrayList();
            for (final WindowLayoutChangeCallbackWrapper windowLayoutChangeCallbackWrapper : this.windowLayoutChangeCallbacks) {
                if (windowLayoutChangeCallbackWrapper.getCallback() == consumer) {
                    Intrinsics.checkNotNullExpressionValue((Object)windowLayoutChangeCallbackWrapper, "callbackWrapper");
                    list.add(windowLayoutChangeCallbackWrapper);
                }
            }
            this.windowLayoutChangeCallbacks.removeAll(list);
            final Iterator iterator2 = list.iterator();
            while (iterator2.hasNext()) {
                this.callbackRemovedForActivity(((WindowLayoutChangeCallbackWrapper)iterator2.next()).getActivity());
            }
            final Unit instance = Unit.INSTANCE;
        }
    }
    
    @Metadata(d1 = { "\u0000@\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u000e\u0010\u000b\u001a\u00020\b2\u0006\u0010\f\u001a\u00020\rJ\u0010\u0010\u000e\u001a\u0004\u0018\u00010\u000f2\u0006\u0010\f\u001a\u00020\rJ\u0012\u0010\u0010\u001a\u00020\u00042\b\u0010\u0011\u001a\u0004\u0018\u00010\u0012H\u0007J\b\u0010\u0013\u001a\u00020\u0014H\u0007R\u000e\u0010\u0003\u001a\u00020\u0004X\u0086T¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082T¢\u0006\u0002\n\u0000R\u0010\u0010\u0007\u001a\u0004\u0018\u00010\bX\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u0015" }, d2 = { "Landroidx/window/layout/adapter/sidecar/SidecarWindowBackend$Companion;", "", "()V", "DEBUG", "", "TAG", "", "globalInstance", "Landroidx/window/layout/adapter/sidecar/SidecarWindowBackend;", "globalLock", "Ljava/util/concurrent/locks/ReentrantLock;", "getInstance", "context", "Landroid/content/Context;", "initAndVerifyExtension", "Landroidx/window/layout/adapter/sidecar/ExtensionInterfaceCompat;", "isSidecarVersionSupported", "sidecarVersion", "Landroidx/window/core/Version;", "resetInstance", "", "window_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
    public static final class Companion
    {
        private Companion() {
        }
        
        public final SidecarWindowBackend getInstance(final Context context) {
            Intrinsics.checkNotNullParameter((Object)context, "context");
            if (SidecarWindowBackend.access$getGlobalInstance$cp() == null) {
                final Lock lock = SidecarWindowBackend.access$getGlobalLock$cp();
                lock.lock();
                try {
                    if (SidecarWindowBackend.access$getGlobalInstance$cp() == null) {
                        final ExtensionInterfaceCompat initAndVerifyExtension = SidecarWindowBackend.Companion.initAndVerifyExtension(context);
                        final Companion companion = SidecarWindowBackend.Companion;
                        SidecarWindowBackend.access$setGlobalInstance$cp(new SidecarWindowBackend(initAndVerifyExtension));
                    }
                    final Unit instance = Unit.INSTANCE;
                }
                finally {
                    lock.unlock();
                }
            }
            final SidecarWindowBackend access$getGlobalInstance$cp = SidecarWindowBackend.access$getGlobalInstance$cp();
            Intrinsics.checkNotNull((Object)access$getGlobalInstance$cp);
            return access$getGlobalInstance$cp;
        }
        
        public final ExtensionInterfaceCompat initAndVerifyExtension(final Context context) {
            Intrinsics.checkNotNullParameter((Object)context, "context");
            ExtensionInterfaceCompat extensionInterfaceCompat2;
            final ExtensionInterfaceCompat extensionInterfaceCompat = extensionInterfaceCompat2 = null;
            try {
                if (this.isSidecarVersionSupported(SidecarCompat.Companion.getSidecarVersion())) {
                    extensionInterfaceCompat2 = new SidecarCompat(context);
                    if (!((SidecarCompat)extensionInterfaceCompat2).validateExtensionInterface()) {}
                }
                return extensionInterfaceCompat2;
            }
            finally {
                extensionInterfaceCompat2 = extensionInterfaceCompat;
                return extensionInterfaceCompat2;
            }
        }
        
        public final boolean isSidecarVersionSupported(final Version version) {
            boolean b = false;
            if (version == null) {
                return false;
            }
            if (version.compareTo(Version.Companion.getVERSION_0_1()) >= 0) {
                b = true;
            }
            return b;
        }
        
        public final void resetInstance() {
            SidecarWindowBackend.access$setGlobalInstance$cp(null);
        }
    }
    
    @Metadata(d1 = { "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0081\u0004\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\u0018\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bH\u0017¨\u0006\t" }, d2 = { "Landroidx/window/layout/adapter/sidecar/SidecarWindowBackend$ExtensionListenerImpl;", "Landroidx/window/layout/adapter/sidecar/ExtensionInterfaceCompat$ExtensionCallbackInterface;", "(Landroidx/window/layout/adapter/sidecar/SidecarWindowBackend;)V", "onWindowLayoutChanged", "", "activity", "Landroid/app/Activity;", "newLayout", "Landroidx/window/layout/WindowLayoutInfo;", "window_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
    public final class ExtensionListenerImpl implements ExtensionCallbackInterface
    {
        final SidecarWindowBackend this$0;
        
        public ExtensionListenerImpl(final SidecarWindowBackend this$0) {
            this.this$0 = this$0;
        }
        
        @Override
        public void onWindowLayoutChanged(final Activity activity, final WindowLayoutInfo windowLayoutInfo) {
            Intrinsics.checkNotNullParameter((Object)activity, "activity");
            Intrinsics.checkNotNullParameter((Object)windowLayoutInfo, "newLayout");
            for (final WindowLayoutChangeCallbackWrapper windowLayoutChangeCallbackWrapper : this.this$0.getWindowLayoutChangeCallbacks()) {
                if (!Intrinsics.areEqual((Object)windowLayoutChangeCallbackWrapper.getActivity(), (Object)activity)) {
                    continue;
                }
                windowLayoutChangeCallbackWrapper.accept(windowLayoutInfo);
            }
        }
    }
    
    @Metadata(d1 = { "\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u000b\n\u0002\u0010\u0002\n\u0002\b\u0002\b\u0000\u0018\u00002\u00020\u0001B#\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\f\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\b0\u0007¢\u0006\u0002\u0010\tJ\u000e\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\bR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000bR\u0017\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\b0\u0007¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\rR\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004¢\u0006\u0002\n\u0000R\u001c\u0010\u000e\u001a\u0004\u0018\u00010\bX\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u000f\u0010\u0010\"\u0004\b\u0011\u0010\u0012¨\u0006\u0016" }, d2 = { "Landroidx/window/layout/adapter/sidecar/SidecarWindowBackend$WindowLayoutChangeCallbackWrapper;", "", "activity", "Landroid/app/Activity;", "executor", "Ljava/util/concurrent/Executor;", "callback", "Landroidx/core/util/Consumer;", "Landroidx/window/layout/WindowLayoutInfo;", "(Landroid/app/Activity;Ljava/util/concurrent/Executor;Landroidx/core/util/Consumer;)V", "getActivity", "()Landroid/app/Activity;", "getCallback", "()Landroidx/core/util/Consumer;", "lastInfo", "getLastInfo", "()Landroidx/window/layout/WindowLayoutInfo;", "setLastInfo", "(Landroidx/window/layout/WindowLayoutInfo;)V", "accept", "", "newLayoutInfo", "window_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
    public static final class WindowLayoutChangeCallbackWrapper
    {
        private final Activity activity;
        private final Consumer<WindowLayoutInfo> callback;
        private final Executor executor;
        private WindowLayoutInfo lastInfo;
        
        public WindowLayoutChangeCallbackWrapper(final Activity activity, final Executor executor, final Consumer<WindowLayoutInfo> callback) {
            Intrinsics.checkNotNullParameter((Object)activity, "activity");
            Intrinsics.checkNotNullParameter((Object)executor, "executor");
            Intrinsics.checkNotNullParameter((Object)callback, "callback");
            this.activity = activity;
            this.executor = executor;
            this.callback = callback;
        }
        
        private static final void accept$lambda$0(final WindowLayoutChangeCallbackWrapper windowLayoutChangeCallbackWrapper, final WindowLayoutInfo windowLayoutInfo) {
            Intrinsics.checkNotNullParameter((Object)windowLayoutChangeCallbackWrapper, "this$0");
            Intrinsics.checkNotNullParameter((Object)windowLayoutInfo, "$newLayoutInfo");
            windowLayoutChangeCallbackWrapper.callback.accept(windowLayoutInfo);
        }
        
        public final void accept(final WindowLayoutInfo lastInfo) {
            Intrinsics.checkNotNullParameter((Object)lastInfo, "newLayoutInfo");
            this.lastInfo = lastInfo;
            this.executor.execute(new SidecarWindowBackend$WindowLayoutChangeCallbackWrapper$$ExternalSyntheticLambda0(this, lastInfo));
        }
        
        public final Activity getActivity() {
            return this.activity;
        }
        
        public final Consumer<WindowLayoutInfo> getCallback() {
            return this.callback;
        }
        
        public final WindowLayoutInfo getLastInfo() {
            return this.lastInfo;
        }
        
        public final void setLastInfo(final WindowLayoutInfo lastInfo) {
            this.lastInfo = lastInfo;
        }
    }
}

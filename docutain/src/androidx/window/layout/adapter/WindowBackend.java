// 
// Decompiled by Procyon v0.6.0
// 

package androidx.window.layout.adapter;

import androidx.window.layout.WindowLayoutInfo;
import androidx.core.util.Consumer;
import java.util.concurrent.Executor;
import android.content.Context;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\b`\u0018\u00002\u00020\u0001J&\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u00072\f\u0010\b\u001a\b\u0012\u0004\u0012\u00020\n0\tH&J\u0016\u0010\u000b\u001a\u00020\u00032\f\u0010\b\u001a\b\u0012\u0004\u0012\u00020\n0\tH&\u00f8\u0001\u0000\u0082\u0002\u0006\n\u0004\b!0\u0001¨\u0006\f\u00c0\u0006\u0001" }, d2 = { "Landroidx/window/layout/adapter/WindowBackend;", "", "registerLayoutChangeCallback", "", "context", "Landroid/content/Context;", "executor", "Ljava/util/concurrent/Executor;", "callback", "Landroidx/core/util/Consumer;", "Landroidx/window/layout/WindowLayoutInfo;", "unregisterLayoutChangeCallback", "window_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
public interface WindowBackend
{
    void registerLayoutChangeCallback(final Context p0, final Executor p1, final Consumer<WindowLayoutInfo> p2);
    
    void unregisterLayoutChangeCallback(final Consumer<WindowLayoutInfo> p0);
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.window.layout;

import android.graphics.Rect;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.DefaultConstructorMarker;
import androidx.window.core.Bounds;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000R\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\b\u0000\u0018\u0000 #2\u00020\u0001:\u0002#$B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007¢\u0006\u0002\u0010\bJ\u0013\u0010\u001c\u001a\u00020\u000e2\b\u0010\u001d\u001a\u0004\u0018\u00010\u001eH\u0096\u0002J\b\u0010\u001f\u001a\u00020 H\u0016J\b\u0010!\u001a\u00020\"H\u0016R\u0014\u0010\t\u001a\u00020\n8VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u000b\u0010\fR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004¢\u0006\u0002\n\u0000R\u0014\u0010\r\u001a\u00020\u000e8VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\r\u0010\u000fR\u0014\u0010\u0010\u001a\u00020\u00118VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u0012\u0010\u0013R\u0014\u0010\u0014\u001a\u00020\u00158VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u0016\u0010\u0017R\u0014\u0010\u0006\u001a\u00020\u0007X\u0096\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0018\u0010\u0019R\u0014\u0010\u0004\u001a\u00020\u0005X\u0080\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u001a\u0010\u001b¨\u0006%" }, d2 = { "Landroidx/window/layout/HardwareFoldingFeature;", "Landroidx/window/layout/FoldingFeature;", "featureBounds", "Landroidx/window/core/Bounds;", "type", "Landroidx/window/layout/HardwareFoldingFeature$Type;", "state", "Landroidx/window/layout/FoldingFeature$State;", "(Landroidx/window/core/Bounds;Landroidx/window/layout/HardwareFoldingFeature$Type;Landroidx/window/layout/FoldingFeature$State;)V", "bounds", "Landroid/graphics/Rect;", "getBounds", "()Landroid/graphics/Rect;", "isSeparating", "", "()Z", "occlusionType", "Landroidx/window/layout/FoldingFeature$OcclusionType;", "getOcclusionType", "()Landroidx/window/layout/FoldingFeature$OcclusionType;", "orientation", "Landroidx/window/layout/FoldingFeature$Orientation;", "getOrientation", "()Landroidx/window/layout/FoldingFeature$Orientation;", "getState", "()Landroidx/window/layout/FoldingFeature$State;", "getType$window_release", "()Landroidx/window/layout/HardwareFoldingFeature$Type;", "equals", "other", "", "hashCode", "", "toString", "", "Companion", "Type", "window_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
public final class HardwareFoldingFeature implements FoldingFeature
{
    public static final Companion Companion;
    private final Bounds featureBounds;
    private final State state;
    private final Type type;
    
    static {
        Companion = new Companion(null);
    }
    
    public HardwareFoldingFeature(final Bounds featureBounds, final Type type, final State state) {
        Intrinsics.checkNotNullParameter((Object)featureBounds, "featureBounds");
        Intrinsics.checkNotNullParameter((Object)type, "type");
        Intrinsics.checkNotNullParameter((Object)state, "state");
        this.featureBounds = featureBounds;
        this.type = type;
        this.state = state;
        HardwareFoldingFeature.Companion.validateFeatureBounds$window_release(featureBounds);
    }
    
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        final Class<? extends HardwareFoldingFeature> class1 = this.getClass();
        Class<?> class2;
        if (o != null) {
            class2 = o.getClass();
        }
        else {
            class2 = null;
        }
        if (!Intrinsics.areEqual((Object)class1, (Object)class2)) {
            return false;
        }
        Intrinsics.checkNotNull(o, "null cannot be cast to non-null type androidx.window.layout.HardwareFoldingFeature");
        final HardwareFoldingFeature hardwareFoldingFeature = (HardwareFoldingFeature)o;
        return Intrinsics.areEqual((Object)this.featureBounds, (Object)hardwareFoldingFeature.featureBounds) && Intrinsics.areEqual((Object)this.type, (Object)hardwareFoldingFeature.type) && Intrinsics.areEqual((Object)this.getState(), (Object)hardwareFoldingFeature.getState());
    }
    
    @Override
    public Rect getBounds() {
        return this.featureBounds.toRect();
    }
    
    @Override
    public OcclusionType getOcclusionType() {
        OcclusionType occlusionType;
        if (this.featureBounds.getWidth() != 0 && this.featureBounds.getHeight() != 0) {
            occlusionType = OcclusionType.FULL;
        }
        else {
            occlusionType = OcclusionType.NONE;
        }
        return occlusionType;
    }
    
    @Override
    public Orientation getOrientation() {
        Orientation orientation;
        if (this.featureBounds.getWidth() > this.featureBounds.getHeight()) {
            orientation = Orientation.HORIZONTAL;
        }
        else {
            orientation = Orientation.VERTICAL;
        }
        return orientation;
    }
    
    @Override
    public State getState() {
        return this.state;
    }
    
    public final Type getType$window_release() {
        return this.type;
    }
    
    @Override
    public int hashCode() {
        return (this.featureBounds.hashCode() * 31 + this.type.hashCode()) * 31 + this.getState().hashCode();
    }
    
    @Override
    public boolean isSeparating() {
        final boolean equal = Intrinsics.areEqual((Object)this.type, (Object)Type.Companion.getHINGE());
        boolean b = true;
        if (!equal) {
            if (!Intrinsics.areEqual((Object)this.type, (Object)Type.Companion.getFOLD()) || !Intrinsics.areEqual((Object)this.getState(), (Object)State.HALF_OPENED)) {
                b = false;
            }
        }
        return b;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("HardwareFoldingFeature");
        sb.append(" { ");
        sb.append(this.featureBounds);
        sb.append(", type=");
        sb.append(this.type);
        sb.append(", state=");
        sb.append(this.getState());
        sb.append(" }");
        return sb.toString();
    }
    
    @Metadata(d1 = { "\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0080\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0015\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0000¢\u0006\u0002\b\u0007¨\u0006\b" }, d2 = { "Landroidx/window/layout/HardwareFoldingFeature$Companion;", "", "()V", "validateFeatureBounds", "", "bounds", "Landroidx/window/core/Bounds;", "validateFeatureBounds$window_release", "window_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
    public static final class Companion
    {
        private Companion() {
        }
        
        public final void validateFeatureBounds$window_release(final Bounds bounds) {
            Intrinsics.checkNotNullParameter((Object)bounds, "bounds");
            final int width = bounds.getWidth();
            final int n = 0;
            if (width == 0 && bounds.getHeight() == 0) {
                throw new IllegalArgumentException("Bounds must be non zero".toString());
            }
            int n2 = 0;
            Label_0056: {
                if (bounds.getLeft() != 0) {
                    n2 = n;
                    if (bounds.getTop() != 0) {
                        break Label_0056;
                    }
                }
                n2 = 1;
            }
            if (n2 != 0) {
                return;
            }
            throw new IllegalArgumentException("Bounding rectangle must start at the top or left window edge for folding features".toString());
        }
    }
    
    @Metadata(d1 = { "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0004\b\u0000\u0018\u0000 \u00062\u00020\u0001:\u0001\u0006B\u000f\b\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\b\u0010\u0005\u001a\u00020\u0003H\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u0007" }, d2 = { "Landroidx/window/layout/HardwareFoldingFeature$Type;", "", "description", "", "(Ljava/lang/String;)V", "toString", "Companion", "window_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
    public static final class Type
    {
        public static final Companion Companion;
        private static final Type FOLD;
        private static final Type HINGE;
        private final String description;
        
        static {
            Companion = new Companion(null);
            FOLD = new Type("FOLD");
            HINGE = new Type("HINGE");
        }
        
        private Type(final String description) {
            this.description = description;
        }
        
        public static final /* synthetic */ Type access$getFOLD$cp() {
            return Type.FOLD;
        }
        
        public static final /* synthetic */ Type access$getHINGE$cp() {
            return Type.HINGE;
        }
        
        @Override
        public String toString() {
            return this.description;
        }
        
        @Metadata(d1 = { "\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0080\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u0011\u0010\u0003\u001a\u00020\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006R\u0011\u0010\u0007\u001a\u00020\u0004¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010\u0006¨\u0006\t" }, d2 = { "Landroidx/window/layout/HardwareFoldingFeature$Type$Companion;", "", "()V", "FOLD", "Landroidx/window/layout/HardwareFoldingFeature$Type;", "getFOLD", "()Landroidx/window/layout/HardwareFoldingFeature$Type;", "HINGE", "getHINGE", "window_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
        public static final class Companion
        {
            private Companion() {
            }
            
            public final Type getFOLD() {
                return Type.access$getFOLD$cp();
            }
            
            public final Type getHINGE() {
                return Type.access$getHINGE$cp();
            }
        }
    }
}

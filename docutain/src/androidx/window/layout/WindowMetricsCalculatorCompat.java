// 
// Decompiled by Procyon v0.6.0
// 

package androidx.window.layout;

import androidx.window.layout.util.DisplayCompatHelperApi28;
import androidx.window.layout.util.DisplayCompatHelperApi17;
import android.content.res.Configuration;
import androidx.window.layout.util.ActivityCompatHelperApi24;
import android.graphics.Point;
import kotlin.jvm.internal.DefaultConstructorMarker;
import android.view.WindowManager;
import android.inputmethodservice.InputMethodService;
import androidx.window.layout.util.ContextUtils;
import androidx.window.core.Bounds;
import androidx.window.layout.util.ContextCompatHelperApi30;
import android.os.Build$VERSION;
import android.graphics.Rect;
import android.app.Activity;
import android.content.res.Resources;
import android.content.Context;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import android.util.Log;
import android.view.DisplayCutout;
import android.view.Display;
import kotlin.collections.CollectionsKt;
import androidx.core.view.WindowInsetsCompat;
import kotlin.jvm.internal.Intrinsics;
import java.util.ArrayList;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000d\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\b\u00c0\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0010\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u000eH\u0016J\u0010\u0010\u000b\u001a\u00020\f2\u0006\u0010\u000f\u001a\u00020\u0010H\u0016J\u0010\u0010\u0011\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u000eH\u0016J\u0010\u0010\u0011\u001a\u00020\f2\u0006\u0010\u000f\u001a\u00020\u0010H\u0016J\u0015\u0010\u0012\u001a\u00020\u00132\u0006\u0010\r\u001a\u00020\u000eH\u0000¢\u0006\u0002\b\u0014J\u0015\u0010\u0015\u001a\u00020\u00132\u0006\u0010\r\u001a\u00020\u000eH\u0001¢\u0006\u0002\b\u0016J\u0015\u0010\u0017\u001a\u00020\u00132\u0006\u0010\r\u001a\u00020\u000eH\u0001¢\u0006\u0002\b\u0018J\u0015\u0010\u0019\u001a\u00020\u00132\u0006\u0010\r\u001a\u00020\u000eH\u0001¢\u0006\u0002\b\u001aJ\u0015\u0010\u001b\u001a\u00020\u001c2\u0006\u0010\u000f\u001a\u00020\u0010H\u0001¢\u0006\u0002\b\u001dJ\u0012\u0010\u001e\u001a\u0004\u0018\u00010\u001f2\u0006\u0010 \u001a\u00020!H\u0003J\u0010\u0010\"\u001a\u00020\u00072\u0006\u0010\u000f\u001a\u00020\u0010H\u0002J\u0015\u0010#\u001a\u00020$2\u0006\u0010 \u001a\u00020!H\u0001¢\u0006\u0002\b%J\u0018\u0010&\u001a\u00020'2\u0006\u0010\r\u001a\u00020\u000e2\u0006\u0010(\u001a\u00020\u0013H\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004¢\u0006\u0002\n\u0000R$\u0010\u0005\u001a\u0012\u0012\u0004\u0012\u00020\u00070\u0006j\b\u0012\u0004\u0012\u00020\u0007`\bX\u0080\u0004¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\n¨\u0006)" }, d2 = { "Landroidx/window/layout/WindowMetricsCalculatorCompat;", "Landroidx/window/layout/WindowMetricsCalculator;", "()V", "TAG", "", "insetsTypeMasks", "Ljava/util/ArrayList;", "", "Lkotlin/collections/ArrayList;", "getInsetsTypeMasks$window_release", "()Ljava/util/ArrayList;", "computeCurrentWindowMetrics", "Landroidx/window/layout/WindowMetrics;", "activity", "Landroid/app/Activity;", "context", "Landroid/content/Context;", "computeMaximumWindowMetrics", "computeWindowBoundsIceCreamSandwich", "Landroid/graphics/Rect;", "computeWindowBoundsIceCreamSandwich$window_release", "computeWindowBoundsN", "computeWindowBoundsN$window_release", "computeWindowBoundsP", "computeWindowBoundsP$window_release", "computeWindowBoundsQ", "computeWindowBoundsQ$window_release", "computeWindowInsetsCompat", "Landroidx/core/view/WindowInsetsCompat;", "computeWindowInsetsCompat$window_release", "getCutoutForDisplay", "Landroid/view/DisplayCutout;", "display", "Landroid/view/Display;", "getNavigationBarHeight", "getRealSizeForDisplay", "Landroid/graphics/Point;", "getRealSizeForDisplay$window_release", "getRectSizeFromDisplay", "", "bounds", "window_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
public final class WindowMetricsCalculatorCompat implements WindowMetricsCalculator
{
    public static final WindowMetricsCalculatorCompat INSTANCE;
    private static final String TAG;
    private static final ArrayList<Integer> insetsTypeMasks;
    
    static {
        INSTANCE = new WindowMetricsCalculatorCompat();
        Intrinsics.checkNotNullExpressionValue((Object)"WindowMetricsCalculatorCompat", "WindowMetricsCalculatorC\u2026at::class.java.simpleName");
        TAG = "WindowMetricsCalculatorCompat";
        insetsTypeMasks = CollectionsKt.arrayListOf((Object[])new Integer[] { WindowInsetsCompat.Type.statusBars(), WindowInsetsCompat.Type.navigationBars(), WindowInsetsCompat.Type.captionBar(), WindowInsetsCompat.Type.ime(), WindowInsetsCompat.Type.systemGestures(), WindowInsetsCompat.Type.mandatorySystemGestures(), WindowInsetsCompat.Type.tappableElement(), WindowInsetsCompat.Type.displayCutout() });
    }
    
    private WindowMetricsCalculatorCompat() {
    }
    
    private final DisplayCutout getCutoutForDisplay(final Display obj) {
        try {
            final Constructor<?> constructor = Class.forName("android.view.DisplayInfo").getConstructor((Class<?>[])new Class[0]);
            constructor.setAccessible(true);
            final Object instance = constructor.newInstance(new Object[0]);
            final Method declaredMethod = obj.getClass().getDeclaredMethod("getDisplayInfo", instance.getClass());
            declaredMethod.setAccessible(true);
            declaredMethod.invoke(obj, instance);
            final Field declaredField = instance.getClass().getDeclaredField("displayCutout");
            declaredField.setAccessible(true);
            final Object value = declaredField.get(instance);
            if (value instanceof DisplayCutout) {
                return (DisplayCutout)value;
            }
        }
        catch (final InstantiationException ex) {
            Log.w(WindowMetricsCalculatorCompat.TAG, (Throwable)ex);
        }
        catch (final InvocationTargetException ex2) {
            Log.w(WindowMetricsCalculatorCompat.TAG, (Throwable)ex2);
        }
        catch (final IllegalAccessException ex3) {
            Log.w(WindowMetricsCalculatorCompat.TAG, (Throwable)ex3);
        }
        catch (final NoSuchFieldException ex4) {
            Log.w(WindowMetricsCalculatorCompat.TAG, (Throwable)ex4);
        }
        catch (final NoSuchMethodException ex5) {
            Log.w(WindowMetricsCalculatorCompat.TAG, (Throwable)ex5);
        }
        catch (final ClassNotFoundException ex6) {
            Log.w(WindowMetricsCalculatorCompat.TAG, (Throwable)ex6);
        }
        return null;
    }
    
    private final int getNavigationBarHeight(final Context context) {
        final Resources resources = context.getResources();
        final int identifier = resources.getIdentifier("navigation_bar_height", "dimen", "android");
        int dimensionPixelSize;
        if (identifier > 0) {
            dimensionPixelSize = resources.getDimensionPixelSize(identifier);
        }
        else {
            dimensionPixelSize = 0;
        }
        return dimensionPixelSize;
    }
    
    private final void getRectSizeFromDisplay(final Activity activity, final Rect rect) {
        activity.getWindowManager().getDefaultDisplay().getRectSize(rect);
    }
    
    @Override
    public WindowMetrics computeCurrentWindowMetrics(final Activity activity) {
        Intrinsics.checkNotNullParameter((Object)activity, "activity");
        Rect rect;
        if (Build$VERSION.SDK_INT >= 30) {
            rect = ContextCompatHelperApi30.INSTANCE.currentWindowBounds((Context)activity);
        }
        else if (Build$VERSION.SDK_INT >= 29) {
            rect = this.computeWindowBoundsQ$window_release(activity);
        }
        else if (Build$VERSION.SDK_INT >= 28) {
            rect = this.computeWindowBoundsP$window_release(activity);
        }
        else if (Build$VERSION.SDK_INT >= 24) {
            rect = this.computeWindowBoundsN$window_release(activity);
        }
        else {
            rect = this.computeWindowBoundsIceCreamSandwich$window_release(activity);
        }
        WindowInsetsCompat windowInsetsCompat;
        if (Build$VERSION.SDK_INT >= 30) {
            windowInsetsCompat = this.computeWindowInsetsCompat$window_release((Context)activity);
        }
        else {
            windowInsetsCompat = new WindowInsetsCompat.Builder().build();
            Intrinsics.checkNotNullExpressionValue((Object)windowInsetsCompat, "{\n            WindowInse\u2026ilder().build()\n        }");
        }
        return new WindowMetrics(new Bounds(rect), windowInsetsCompat);
    }
    
    @Override
    public WindowMetrics computeCurrentWindowMetrics(final Context obj) {
        Intrinsics.checkNotNullParameter((Object)obj, "context");
        if (Build$VERSION.SDK_INT >= 30) {
            return ContextCompatHelperApi30.INSTANCE.currentWindowMetrics(obj);
        }
        final Context unwrapUiContext$window_release = ContextUtils.INSTANCE.unwrapUiContext$window_release(obj);
        if (unwrapUiContext$window_release instanceof Activity) {
            return this.computeCurrentWindowMetrics((Activity)obj);
        }
        if (unwrapUiContext$window_release instanceof InputMethodService) {
            final Object systemService = obj.getSystemService("window");
            Intrinsics.checkNotNull(systemService, "null cannot be cast to non-null type android.view.WindowManager");
            final Display defaultDisplay = ((WindowManager)systemService).getDefaultDisplay();
            Intrinsics.checkNotNullExpressionValue((Object)defaultDisplay, "wm.defaultDisplay");
            final Point realSizeForDisplay$window_release = this.getRealSizeForDisplay$window_release(defaultDisplay);
            return new WindowMetrics(new Rect(0, 0, realSizeForDisplay$window_release.x, realSizeForDisplay$window_release.y), null, 2, null);
        }
        final StringBuilder sb = new StringBuilder();
        sb.append(obj);
        sb.append(" is not a UiContext");
        throw new IllegalArgumentException(sb.toString());
    }
    
    @Override
    public WindowMetrics computeMaximumWindowMetrics(final Activity activity) {
        Intrinsics.checkNotNullParameter((Object)activity, "activity");
        return this.computeMaximumWindowMetrics((Context)activity);
    }
    
    @Override
    public WindowMetrics computeMaximumWindowMetrics(final Context context) {
        Intrinsics.checkNotNullParameter((Object)context, "context");
        Rect maximumWindowBounds;
        if (Build$VERSION.SDK_INT >= 30) {
            maximumWindowBounds = ContextCompatHelperApi30.INSTANCE.maximumWindowBounds(context);
        }
        else {
            final Object systemService = context.getSystemService("window");
            Intrinsics.checkNotNull(systemService, "null cannot be cast to non-null type android.view.WindowManager");
            final Display defaultDisplay = ((WindowManager)systemService).getDefaultDisplay();
            Intrinsics.checkNotNullExpressionValue((Object)defaultDisplay, "display");
            final Point realSizeForDisplay$window_release = this.getRealSizeForDisplay$window_release(defaultDisplay);
            maximumWindowBounds = new Rect(0, 0, realSizeForDisplay$window_release.x, realSizeForDisplay$window_release.y);
        }
        WindowInsetsCompat windowInsetsCompat;
        if (Build$VERSION.SDK_INT >= 30) {
            windowInsetsCompat = this.computeWindowInsetsCompat$window_release(context);
        }
        else {
            windowInsetsCompat = new WindowInsetsCompat.Builder().build();
            Intrinsics.checkNotNullExpressionValue((Object)windowInsetsCompat, "{\n            WindowInse\u2026ilder().build()\n        }");
        }
        return new WindowMetrics(new Bounds(maximumWindowBounds), windowInsetsCompat);
    }
    
    public final Rect computeWindowBoundsIceCreamSandwich$window_release(final Activity activity) {
        Intrinsics.checkNotNullParameter((Object)activity, "activity");
        final Display defaultDisplay = activity.getWindowManager().getDefaultDisplay();
        Intrinsics.checkNotNullExpressionValue((Object)defaultDisplay, "defaultDisplay");
        final Point realSizeForDisplay$window_release = this.getRealSizeForDisplay$window_release(defaultDisplay);
        final Rect rect = new Rect();
        if (realSizeForDisplay$window_release.x != 0 && realSizeForDisplay$window_release.y != 0) {
            rect.right = realSizeForDisplay$window_release.x;
            rect.bottom = realSizeForDisplay$window_release.y;
        }
        else {
            defaultDisplay.getRectSize(rect);
        }
        return rect;
    }
    
    public final Rect computeWindowBoundsN$window_release(final Activity activity) {
        Intrinsics.checkNotNullParameter((Object)activity, "activity");
        final Rect rect = new Rect();
        final Display defaultDisplay = activity.getWindowManager().getDefaultDisplay();
        defaultDisplay.getRectSize(rect);
        if (!ActivityCompatHelperApi24.INSTANCE.isInMultiWindowMode(activity)) {
            Intrinsics.checkNotNullExpressionValue((Object)defaultDisplay, "defaultDisplay");
            final Point realSizeForDisplay$window_release = this.getRealSizeForDisplay$window_release(defaultDisplay);
            final int navigationBarHeight = this.getNavigationBarHeight((Context)activity);
            if (rect.bottom + navigationBarHeight == realSizeForDisplay$window_release.y) {
                rect.bottom += navigationBarHeight;
            }
            else if (rect.right + navigationBarHeight == realSizeForDisplay$window_release.x) {
                rect.right += navigationBarHeight;
            }
        }
        return rect;
    }
    
    public final Rect computeWindowBoundsP$window_release(final Activity activity) {
        Intrinsics.checkNotNullParameter((Object)activity, "activity");
        final Rect rect = new Rect();
        final Configuration configuration = activity.getResources().getConfiguration();
        try {
            final Field declaredField = Configuration.class.getDeclaredField("windowConfiguration");
            declaredField.setAccessible(true);
            final Object value = declaredField.get(configuration);
            if (ActivityCompatHelperApi24.INSTANCE.isInMultiWindowMode(activity)) {
                final Object invoke = value.getClass().getDeclaredMethod("getBounds", (Class<?>[])new Class[0]).invoke(value, new Object[0]);
                Intrinsics.checkNotNull(invoke, "null cannot be cast to non-null type android.graphics.Rect");
                rect.set((Rect)invoke);
            }
            else {
                final Object invoke2 = value.getClass().getDeclaredMethod("getAppBounds", (Class<?>[])new Class[0]).invoke(value, new Object[0]);
                Intrinsics.checkNotNull(invoke2, "null cannot be cast to non-null type android.graphics.Rect");
                rect.set((Rect)invoke2);
            }
        }
        catch (final InvocationTargetException ex) {
            Log.w(WindowMetricsCalculatorCompat.TAG, (Throwable)ex);
            this.getRectSizeFromDisplay(activity, rect);
        }
        catch (final IllegalAccessException ex2) {
            Log.w(WindowMetricsCalculatorCompat.TAG, (Throwable)ex2);
            this.getRectSizeFromDisplay(activity, rect);
        }
        catch (final NoSuchMethodException ex3) {
            Log.w(WindowMetricsCalculatorCompat.TAG, (Throwable)ex3);
            this.getRectSizeFromDisplay(activity, rect);
        }
        catch (final NoSuchFieldException ex4) {
            Log.w(WindowMetricsCalculatorCompat.TAG, (Throwable)ex4);
            this.getRectSizeFromDisplay(activity, rect);
        }
        final Display defaultDisplay = activity.getWindowManager().getDefaultDisplay();
        final Point point = new Point();
        final DisplayCompatHelperApi17 instance = DisplayCompatHelperApi17.INSTANCE;
        Intrinsics.checkNotNullExpressionValue((Object)defaultDisplay, "currentDisplay");
        instance.getRealSize(defaultDisplay, point);
        if (!ActivityCompatHelperApi24.INSTANCE.isInMultiWindowMode(activity)) {
            final int navigationBarHeight = this.getNavigationBarHeight((Context)activity);
            if (rect.bottom + navigationBarHeight == point.y) {
                rect.bottom += navigationBarHeight;
            }
            else if (rect.right + navigationBarHeight == point.x) {
                rect.right += navigationBarHeight;
            }
            else if (rect.left == navigationBarHeight) {
                rect.left = 0;
            }
        }
        if ((rect.width() < point.x || rect.height() < point.y) && !ActivityCompatHelperApi24.INSTANCE.isInMultiWindowMode(activity)) {
            final DisplayCutout cutoutForDisplay = this.getCutoutForDisplay(defaultDisplay);
            if (cutoutForDisplay != null) {
                if (rect.left == DisplayCompatHelperApi28.INSTANCE.safeInsetLeft(cutoutForDisplay)) {
                    rect.left = 0;
                }
                if (point.x - rect.right == DisplayCompatHelperApi28.INSTANCE.safeInsetRight(cutoutForDisplay)) {
                    rect.right += DisplayCompatHelperApi28.INSTANCE.safeInsetRight(cutoutForDisplay);
                }
                if (rect.top == DisplayCompatHelperApi28.INSTANCE.safeInsetTop(cutoutForDisplay)) {
                    rect.top = 0;
                }
                if (point.y - rect.bottom == DisplayCompatHelperApi28.INSTANCE.safeInsetBottom(cutoutForDisplay)) {
                    rect.bottom += DisplayCompatHelperApi28.INSTANCE.safeInsetBottom(cutoutForDisplay);
                }
            }
        }
        return rect;
    }
    
    public final Rect computeWindowBoundsQ$window_release(Activity activity) {
        Intrinsics.checkNotNullParameter((Object)activity, "activity");
        final Configuration configuration = activity.getResources().getConfiguration();
        try {
            final Field declaredField = Configuration.class.getDeclaredField("windowConfiguration");
            declaredField.setAccessible(true);
            final Object value = declaredField.get(configuration);
            final Object invoke = value.getClass().getDeclaredMethod("getBounds", (Class<?>[])new Class[0]).invoke(value, new Object[0]);
            Intrinsics.checkNotNull(invoke, "null cannot be cast to non-null type android.graphics.Rect");
            activity = (Activity)new Rect((Rect)invoke);
        }
        catch (final InvocationTargetException ex) {
            Log.w(WindowMetricsCalculatorCompat.TAG, (Throwable)ex);
            activity = (Activity)this.computeWindowBoundsP$window_release(activity);
        }
        catch (final IllegalAccessException ex2) {
            Log.w(WindowMetricsCalculatorCompat.TAG, (Throwable)ex2);
            activity = (Activity)this.computeWindowBoundsP$window_release(activity);
        }
        catch (final NoSuchMethodException ex3) {
            Log.w(WindowMetricsCalculatorCompat.TAG, (Throwable)ex3);
            activity = (Activity)this.computeWindowBoundsP$window_release(activity);
        }
        catch (final NoSuchFieldException ex4) {
            Log.w(WindowMetricsCalculatorCompat.TAG, (Throwable)ex4);
            activity = (Activity)this.computeWindowBoundsP$window_release(activity);
        }
        return (Rect)activity;
    }
    
    public final WindowInsetsCompat computeWindowInsetsCompat$window_release(final Context context) {
        Intrinsics.checkNotNullParameter((Object)context, "context");
        if (Build$VERSION.SDK_INT >= 30) {
            return ContextCompatHelperApi30.INSTANCE.currentWindowInsets(context);
        }
        throw new Exception("Incompatible SDK version");
    }
    
    public final ArrayList<Integer> getInsetsTypeMasks$window_release() {
        return WindowMetricsCalculatorCompat.insetsTypeMasks;
    }
    
    public final Point getRealSizeForDisplay$window_release(final Display obj) {
        Intrinsics.checkNotNullParameter((Object)obj, "display");
        final Point point = new Point();
        if (Build$VERSION.SDK_INT >= 17) {
            DisplayCompatHelperApi17.INSTANCE.getRealSize(obj, point);
        }
        else {
            try {
                final Method declaredMethod = Display.class.getDeclaredMethod("getRealSize", Point.class);
                declaredMethod.setAccessible(true);
                declaredMethod.invoke(obj, point);
            }
            catch (final InvocationTargetException ex) {
                Log.w(WindowMetricsCalculatorCompat.TAG, (Throwable)ex);
            }
            catch (final IllegalAccessException ex2) {
                Log.w(WindowMetricsCalculatorCompat.TAG, (Throwable)ex2);
            }
            catch (final NoSuchMethodException ex3) {
                Log.w(WindowMetricsCalculatorCompat.TAG, (Throwable)ex3);
            }
        }
        return point;
    }
}

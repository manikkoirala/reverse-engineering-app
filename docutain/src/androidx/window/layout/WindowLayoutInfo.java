// 
// Decompiled by Procyon v0.6.0
// 

package androidx.window.layout;

import kotlin.jvm.functions.Function1;
import kotlin.collections.CollectionsKt;
import kotlin.jvm.internal.Intrinsics;
import java.util.List;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\u0018\u00002\u00020\u0001B\u0015\b\u0007\u0012\f\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003¢\u0006\u0002\u0010\u0005J\u0013\u0010\b\u001a\u00020\t2\b\u0010\n\u001a\u0004\u0018\u00010\u0001H\u0096\u0002J\b\u0010\u000b\u001a\u00020\fH\u0016J\b\u0010\r\u001a\u00020\u000eH\u0016R\u0017\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0006\u0010\u0007¨\u0006\u000f" }, d2 = { "Landroidx/window/layout/WindowLayoutInfo;", "", "displayFeatures", "", "Landroidx/window/layout/DisplayFeature;", "(Ljava/util/List;)V", "getDisplayFeatures", "()Ljava/util/List;", "equals", "", "other", "hashCode", "", "toString", "", "window_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
public final class WindowLayoutInfo
{
    private final List<DisplayFeature> displayFeatures;
    
    public WindowLayoutInfo(final List<? extends DisplayFeature> displayFeatures) {
        Intrinsics.checkNotNullParameter((Object)displayFeatures, "displayFeatures");
        this.displayFeatures = (List<DisplayFeature>)displayFeatures;
    }
    
    @Override
    public boolean equals(final Object o) {
        return this == o || (o != null && Intrinsics.areEqual((Object)this.getClass(), (Object)o.getClass()) && Intrinsics.areEqual((Object)this.displayFeatures, (Object)((WindowLayoutInfo)o).displayFeatures));
    }
    
    public final List<DisplayFeature> getDisplayFeatures() {
        return this.displayFeatures;
    }
    
    @Override
    public int hashCode() {
        return this.displayFeatures.hashCode();
    }
    
    @Override
    public String toString() {
        return CollectionsKt.joinToString$default((Iterable)this.displayFeatures, (CharSequence)", ", (CharSequence)"WindowLayoutInfo{ DisplayFeatures[", (CharSequence)"] }", 0, (CharSequence)null, (Function1)null, 56, (Object)null);
    }
}

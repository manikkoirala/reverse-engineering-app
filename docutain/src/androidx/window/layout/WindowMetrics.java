// 
// Decompiled by Procyon v0.6.0
// 

package androidx.window.layout;

import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import android.graphics.Rect;
import androidx.core.view.WindowInsetsCompat;
import androidx.window.core.Bounds;
import kotlin.Metadata;

@Metadata(d1 = { "\u00004\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\u0018\u00002\u00020\u0001B\u0019\b\u0017\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\b\b\u0002\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006B\u0017\b\u0000\u0012\u0006\u0010\u0007\u001a\u00020\b\u0012\u0006\u0010\t\u001a\u00020\u0005¢\u0006\u0002\u0010\nJ\u0013\u0010\r\u001a\u00020\u000e2\b\u0010\u000f\u001a\u0004\u0018\u00010\u0001H\u0096\u0002J\b\u0010\u0010\u001a\u00020\u0005H\u0007J\b\u0010\u0011\u001a\u00020\u0012H\u0016J\b\u0010\u0013\u001a\u00020\u0014H\u0016R\u000e\u0010\u0007\u001a\u00020\bX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\u0005X\u0082\u0004¢\u0006\u0002\n\u0000R\u0011\u0010\u0002\u001a\u00020\u00038F¢\u0006\u0006\u001a\u0004\b\u000b\u0010\f¨\u0006\u0015" }, d2 = { "Landroidx/window/layout/WindowMetrics;", "", "bounds", "Landroid/graphics/Rect;", "insets", "Landroidx/core/view/WindowInsetsCompat;", "(Landroid/graphics/Rect;Landroidx/core/view/WindowInsetsCompat;)V", "_bounds", "Landroidx/window/core/Bounds;", "_windowInsetsCompat", "(Landroidx/window/core/Bounds;Landroidx/core/view/WindowInsetsCompat;)V", "getBounds", "()Landroid/graphics/Rect;", "equals", "", "other", "getWindowInsets", "hashCode", "", "toString", "", "window_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
public final class WindowMetrics
{
    private final Bounds _bounds;
    private final WindowInsetsCompat _windowInsetsCompat;
    
    public WindowMetrics(final Rect rect, final WindowInsetsCompat windowInsetsCompat) {
        Intrinsics.checkNotNullParameter((Object)rect, "bounds");
        Intrinsics.checkNotNullParameter((Object)windowInsetsCompat, "insets");
        this(new Bounds(rect), windowInsetsCompat);
    }
    
    public WindowMetrics(final Bounds bounds, final WindowInsetsCompat windowInsetsCompat) {
        Intrinsics.checkNotNullParameter((Object)bounds, "_bounds");
        Intrinsics.checkNotNullParameter((Object)windowInsetsCompat, "_windowInsetsCompat");
        this._bounds = bounds;
        this._windowInsetsCompat = windowInsetsCompat;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        final Class<? extends WindowMetrics> class1 = this.getClass();
        Class<?> class2;
        if (o != null) {
            class2 = o.getClass();
        }
        else {
            class2 = null;
        }
        if (!Intrinsics.areEqual((Object)class1, (Object)class2)) {
            return false;
        }
        Intrinsics.checkNotNull(o, "null cannot be cast to non-null type androidx.window.layout.WindowMetrics");
        final WindowMetrics windowMetrics = (WindowMetrics)o;
        return Intrinsics.areEqual((Object)this._bounds, (Object)windowMetrics._bounds) && Intrinsics.areEqual((Object)this._windowInsetsCompat, (Object)windowMetrics._windowInsetsCompat);
    }
    
    public final Rect getBounds() {
        return this._bounds.toRect();
    }
    
    public final WindowInsetsCompat getWindowInsets() {
        return this._windowInsetsCompat;
    }
    
    @Override
    public int hashCode() {
        return this._bounds.hashCode() * 31 + this._windowInsetsCompat.hashCode();
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("WindowMetrics( bounds=");
        sb.append(this._bounds);
        sb.append(", windowInsetsCompat=");
        sb.append(this._windowInsetsCompat);
        sb.append(')');
        return sb.toString();
    }
}

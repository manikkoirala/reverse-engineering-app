// 
// Decompiled by Procyon v0.6.0
// 

package androidx.window.layout;

import android.graphics.Rect;
import androidx.core.view.WindowInsetsCompat;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.JvmStatic;
import kotlin.jvm.functions.Function1;
import android.content.Context;
import android.app.Activity;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\bf\u0018\u0000 \t2\u00020\u0001:\u0001\tJ\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0006\u001a\u00020\u0007H\u0016J\u0010\u0010\b\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&J\u0010\u0010\b\u001a\u00020\u00032\u0006\u0010\u0006\u001a\u00020\u0007H\u0016\u00f8\u0001\u0000\u0082\u0002\u0006\n\u0004\b!0\u0001¨\u0006\n\u00c0\u0006\u0001" }, d2 = { "Landroidx/window/layout/WindowMetricsCalculator;", "", "computeCurrentWindowMetrics", "Landroidx/window/layout/WindowMetrics;", "activity", "Landroid/app/Activity;", "context", "Landroid/content/Context;", "computeMaximumWindowMetrics", "Companion", "window_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
public interface WindowMetricsCalculator
{
    public static final Companion Companion = WindowMetricsCalculator.Companion.$$INSTANCE;
    
    WindowMetrics computeCurrentWindowMetrics(final Activity p0);
    
    WindowMetrics computeCurrentWindowMetrics(final Context p0);
    
    WindowMetrics computeMaximumWindowMetrics(final Activity p0);
    
    WindowMetrics computeMaximumWindowMetrics(final Context p0);
    
    @Metadata(d1 = { "\u00004\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\b\u0010\u0006\u001a\u00020\u0005H\u0007J\u0010\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\nH\u0007J\b\u0010\u000b\u001a\u00020\bH\u0007J\u0015\u0010\f\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u000fH\u0001¢\u0006\u0002\b\u0010R\u001a\u0010\u0003\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00050\u0004X\u0082\u000e¢\u0006\u0002\n\u0000¨\u0006\u0011" }, d2 = { "Landroidx/window/layout/WindowMetricsCalculator$Companion;", "", "()V", "decorator", "Lkotlin/Function1;", "Landroidx/window/layout/WindowMetricsCalculator;", "getOrCreate", "overrideDecorator", "", "overridingDecorator", "Landroidx/window/layout/WindowMetricsCalculatorDecorator;", "reset", "translateWindowMetrics", "Landroidx/window/layout/WindowMetrics;", "windowMetrics", "Landroid/view/WindowMetrics;", "translateWindowMetrics$window_release", "window_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
    public static final class Companion
    {
        static final Companion $$INSTANCE;
        private static Function1<? super WindowMetricsCalculator, ? extends WindowMetricsCalculator> decorator;
        
        static {
            $$INSTANCE = new Companion();
            Companion.decorator = (Function1<? super WindowMetricsCalculator, ? extends WindowMetricsCalculator>)WindowMetricsCalculator$Companion$decorator.WindowMetricsCalculator$Companion$decorator$1.INSTANCE;
        }
        
        private Companion() {
        }
        
        @JvmStatic
        public final WindowMetricsCalculator getOrCreate() {
            return (WindowMetricsCalculator)Companion.decorator.invoke((Object)WindowMetricsCalculatorCompat.INSTANCE);
        }
        
        @JvmStatic
        public final void overrideDecorator(final WindowMetricsCalculatorDecorator windowMetricsCalculatorDecorator) {
            Intrinsics.checkNotNullParameter((Object)windowMetricsCalculatorDecorator, "overridingDecorator");
            Companion.decorator = (Function1<? super WindowMetricsCalculator, ? extends WindowMetricsCalculator>)new WindowMetricsCalculator$Companion$overrideDecorator.WindowMetricsCalculator$Companion$overrideDecorator$1((Object)windowMetricsCalculatorDecorator);
        }
        
        @JvmStatic
        public final void reset() {
            Companion.decorator = (Function1<? super WindowMetricsCalculator, ? extends WindowMetricsCalculator>)WindowMetricsCalculator$Companion$reset.WindowMetricsCalculator$Companion$reset$1.INSTANCE;
        }
        
        public final WindowMetrics translateWindowMetrics$window_release(final android.view.WindowMetrics windowMetrics) {
            Intrinsics.checkNotNullParameter((Object)windowMetrics, "windowMetrics");
            final Rect bounds = windowMetrics.getBounds();
            Intrinsics.checkNotNullExpressionValue((Object)bounds, "windowMetrics.bounds");
            final WindowInsetsCompat windowInsetsCompat = WindowInsetsCompat.toWindowInsetsCompat(windowMetrics.getWindowInsets());
            Intrinsics.checkNotNullExpressionValue((Object)windowInsetsCompat, "toWindowInsetsCompat(windowMetrics.windowInsets)");
            return new WindowMetrics(bounds, windowInsetsCompat);
        }
    }
}

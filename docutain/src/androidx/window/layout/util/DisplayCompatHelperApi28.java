// 
// Decompiled by Procyon v0.6.0
// 

package androidx.window.layout.util;

import kotlin.jvm.internal.Intrinsics;
import android.view.DisplayCutout;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\b\u00c1\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u000e\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006J\u000e\u0010\u0007\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006J\u000e\u0010\b\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006J\u000e\u0010\t\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006¨\u0006\n" }, d2 = { "Landroidx/window/layout/util/DisplayCompatHelperApi28;", "", "()V", "safeInsetBottom", "", "displayCutout", "Landroid/view/DisplayCutout;", "safeInsetLeft", "safeInsetRight", "safeInsetTop", "window_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
public final class DisplayCompatHelperApi28
{
    public static final DisplayCompatHelperApi28 INSTANCE;
    
    static {
        INSTANCE = new DisplayCompatHelperApi28();
    }
    
    private DisplayCompatHelperApi28() {
    }
    
    public final int safeInsetBottom(final DisplayCutout displayCutout) {
        Intrinsics.checkNotNullParameter((Object)displayCutout, "displayCutout");
        return displayCutout.getSafeInsetBottom();
    }
    
    public final int safeInsetLeft(final DisplayCutout displayCutout) {
        Intrinsics.checkNotNullParameter((Object)displayCutout, "displayCutout");
        return displayCutout.getSafeInsetLeft();
    }
    
    public final int safeInsetRight(final DisplayCutout displayCutout) {
        Intrinsics.checkNotNullParameter((Object)displayCutout, "displayCutout");
        return displayCutout.getSafeInsetRight();
    }
    
    public final int safeInsetTop(final DisplayCutout displayCutout) {
        Intrinsics.checkNotNullParameter((Object)displayCutout, "displayCutout");
        return displayCutout.getSafeInsetTop();
    }
}

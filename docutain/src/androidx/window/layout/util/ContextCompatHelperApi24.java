// 
// Decompiled by Procyon v0.6.0
// 

package androidx.window.layout.util;

import kotlin.jvm.internal.Intrinsics;
import android.app.Activity;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u00c1\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u000e\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006¨\u0006\u0007" }, d2 = { "Landroidx/window/layout/util/ContextCompatHelperApi24;", "", "()V", "isInMultiWindowMode", "", "activity", "Landroid/app/Activity;", "window_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
public final class ContextCompatHelperApi24
{
    public static final ContextCompatHelperApi24 INSTANCE;
    
    static {
        INSTANCE = new ContextCompatHelperApi24();
    }
    
    private ContextCompatHelperApi24() {
    }
    
    public final boolean isInMultiWindowMode(final Activity activity) {
        Intrinsics.checkNotNullParameter((Object)activity, "activity");
        return activity.isInMultiWindowMode();
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.window.layout.util;

import android.view.WindowInsets;
import androidx.core.view.WindowInsetsCompat;
import android.view.WindowManager;
import kotlin.jvm.internal.Intrinsics;
import android.graphics.Rect;
import android.content.Context;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u00c1\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u000e\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006J\u0010\u0010\u0007\u001a\u00020\b2\u0006\u0010\u0005\u001a\u00020\u0006H\u0007J\u000e\u0010\t\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006¨\u0006\n" }, d2 = { "Landroidx/window/layout/util/ContextCompatHelper;", "", "()V", "currentWindowBounds", "Landroid/graphics/Rect;", "context", "Landroid/content/Context;", "currentWindowInsets", "Landroidx/core/view/WindowInsetsCompat;", "maximumWindowBounds", "window_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
public final class ContextCompatHelper
{
    public static final ContextCompatHelper INSTANCE;
    
    static {
        INSTANCE = new ContextCompatHelper();
    }
    
    private ContextCompatHelper() {
    }
    
    public final Rect currentWindowBounds(final Context context) {
        Intrinsics.checkNotNullParameter((Object)context, "context");
        final Rect bounds = ((WindowManager)context.getSystemService((Class)WindowManager.class)).getCurrentWindowMetrics().getBounds();
        Intrinsics.checkNotNullExpressionValue((Object)bounds, "wm.currentWindowMetrics.bounds");
        return bounds;
    }
    
    public final WindowInsetsCompat currentWindowInsets(final Context context) {
        Intrinsics.checkNotNullParameter((Object)context, "context");
        final WindowInsets windowInsets = ((WindowManager)context.getSystemService((Class)WindowManager.class)).getCurrentWindowMetrics().getWindowInsets();
        Intrinsics.checkNotNullExpressionValue((Object)windowInsets, "wm.currentWindowMetrics.windowInsets");
        final WindowInsetsCompat windowInsetsCompat = WindowInsetsCompat.toWindowInsetsCompat(windowInsets);
        Intrinsics.checkNotNullExpressionValue((Object)windowInsetsCompat, "toWindowInsetsCompat(platformInsets)");
        return windowInsetsCompat;
    }
    
    public final Rect maximumWindowBounds(final Context context) {
        Intrinsics.checkNotNullParameter((Object)context, "context");
        final Rect bounds = ((WindowManager)context.getSystemService((Class)WindowManager.class)).getMaximumWindowMetrics().getBounds();
        Intrinsics.checkNotNullExpressionValue((Object)bounds, "wm.maximumWindowMetrics.bounds");
        return bounds;
    }
}

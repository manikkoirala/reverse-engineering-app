// 
// Decompiled by Procyon v0.6.0
// 

package androidx.window.layout.util;

import androidx.window.layout.WindowMetrics;
import android.view.WindowInsets;
import androidx.core.view.WindowInsetsCompat;
import android.app.Activity;
import android.view.WindowManager;
import kotlin.jvm.internal.Intrinsics;
import android.graphics.Rect;
import android.content.Context;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u00c1\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u000e\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006J\u0010\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\nH\u0007J\u000e\u0010\u0007\u001a\u00020\b2\u0006\u0010\u0005\u001a\u00020\u0006J\u000e\u0010\u000b\u001a\u00020\f2\u0006\u0010\u0005\u001a\u00020\u0006J\u000e\u0010\r\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006¨\u0006\u000e" }, d2 = { "Landroidx/window/layout/util/ContextCompatHelperApi30;", "", "()V", "currentWindowBounds", "Landroid/graphics/Rect;", "context", "Landroid/content/Context;", "currentWindowInsets", "Landroidx/core/view/WindowInsetsCompat;", "activity", "Landroid/app/Activity;", "currentWindowMetrics", "Landroidx/window/layout/WindowMetrics;", "maximumWindowBounds", "window_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
public final class ContextCompatHelperApi30
{
    public static final ContextCompatHelperApi30 INSTANCE;
    
    static {
        INSTANCE = new ContextCompatHelperApi30();
    }
    
    private ContextCompatHelperApi30() {
    }
    
    public final Rect currentWindowBounds(final Context context) {
        Intrinsics.checkNotNullParameter((Object)context, "context");
        final Rect bounds = ((WindowManager)context.getSystemService((Class)WindowManager.class)).getCurrentWindowMetrics().getBounds();
        Intrinsics.checkNotNullExpressionValue((Object)bounds, "wm.currentWindowMetrics.bounds");
        return bounds;
    }
    
    public final WindowInsetsCompat currentWindowInsets(final Activity activity) {
        Intrinsics.checkNotNullParameter((Object)activity, "activity");
        final WindowInsets windowInsets = activity.getWindowManager().getCurrentWindowMetrics().getWindowInsets();
        Intrinsics.checkNotNullExpressionValue((Object)windowInsets, "activity.windowManager.c\u2026indowMetrics.windowInsets");
        final WindowInsetsCompat windowInsetsCompat = WindowInsetsCompat.toWindowInsetsCompat(windowInsets);
        Intrinsics.checkNotNullExpressionValue((Object)windowInsetsCompat, "toWindowInsetsCompat(platformInsets)");
        return windowInsetsCompat;
    }
    
    public final WindowInsetsCompat currentWindowInsets(final Context context) {
        Intrinsics.checkNotNullParameter((Object)context, "context");
        final WindowInsetsCompat windowInsetsCompat = WindowInsetsCompat.toWindowInsetsCompat(((WindowManager)context.getSystemService((Class)WindowManager.class)).getCurrentWindowMetrics().getWindowInsets());
        Intrinsics.checkNotNullExpressionValue((Object)windowInsetsCompat, "toWindowInsetsCompat(wm.\u2026ndowMetrics.windowInsets)");
        return windowInsetsCompat;
    }
    
    public final WindowMetrics currentWindowMetrics(final Context context) {
        Intrinsics.checkNotNullParameter((Object)context, "context");
        final WindowManager windowManager = (WindowManager)context.getSystemService((Class)WindowManager.class);
        final WindowInsetsCompat windowInsetsCompat = WindowInsetsCompat.toWindowInsetsCompat(windowManager.getCurrentWindowMetrics().getWindowInsets());
        Intrinsics.checkNotNullExpressionValue((Object)windowInsetsCompat, "toWindowInsetsCompat(wm.\u2026ndowMetrics.windowInsets)");
        final Rect bounds = windowManager.getCurrentWindowMetrics().getBounds();
        Intrinsics.checkNotNullExpressionValue((Object)bounds, "wm.currentWindowMetrics.bounds");
        return new WindowMetrics(bounds, windowInsetsCompat);
    }
    
    public final Rect maximumWindowBounds(final Context context) {
        Intrinsics.checkNotNullParameter((Object)context, "context");
        final Rect bounds = ((WindowManager)context.getSystemService((Class)WindowManager.class)).getMaximumWindowMetrics().getBounds();
        Intrinsics.checkNotNullExpressionValue((Object)bounds, "wm.maximumWindowMetrics.bounds");
        return bounds;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.window.layout;

import kotlin.jvm.internal.Intrinsics;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u00c2\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0004H\u0016¨\u0006\u0006" }, d2 = { "Landroidx/window/layout/EmptyDecorator;", "Landroidx/window/layout/WindowInfoTrackerDecorator;", "()V", "decorate", "Landroidx/window/layout/WindowInfoTracker;", "tracker", "window_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
final class EmptyDecorator implements WindowInfoTrackerDecorator
{
    public static final EmptyDecorator INSTANCE;
    
    static {
        INSTANCE = new EmptyDecorator();
    }
    
    private EmptyDecorator() {
    }
    
    @Override
    public WindowInfoTracker decorate(final WindowInfoTracker windowInfoTracker) {
        Intrinsics.checkNotNullParameter((Object)windowInfoTracker, "tracker");
        return windowInfoTracker;
    }
}

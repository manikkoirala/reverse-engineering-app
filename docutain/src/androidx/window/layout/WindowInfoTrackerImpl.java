// 
// Decompiled by Procyon v0.6.0
// 

package androidx.window.layout;

import android.content.Context;
import kotlinx.coroutines.flow.FlowKt;
import kotlin.coroutines.Continuation;
import kotlin.jvm.functions.Function2;
import kotlinx.coroutines.flow.Flow;
import android.app.Activity;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.DefaultConstructorMarker;
import androidx.window.layout.adapter.WindowBackend;
import kotlin.Metadata;

@Metadata(d1 = { "\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0000\u0018\u0000 \u000e2\u00020\u0001:\u0001\u000eB\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\u0016\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\t0\b2\u0006\u0010\n\u001a\u00020\u000bH\u0016J\u0016\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\t0\b2\u0006\u0010\f\u001a\u00020\rH\u0017R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u000f" }, d2 = { "Landroidx/window/layout/WindowInfoTrackerImpl;", "Landroidx/window/layout/WindowInfoTracker;", "windowMetricsCalculator", "Landroidx/window/layout/WindowMetricsCalculator;", "windowBackend", "Landroidx/window/layout/adapter/WindowBackend;", "(Landroidx/window/layout/WindowMetricsCalculator;Landroidx/window/layout/adapter/WindowBackend;)V", "windowLayoutInfo", "Lkotlinx/coroutines/flow/Flow;", "Landroidx/window/layout/WindowLayoutInfo;", "activity", "Landroid/app/Activity;", "context", "Landroid/content/Context;", "Companion", "window_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
public final class WindowInfoTrackerImpl implements WindowInfoTracker
{
    private static final int BUFFER_CAPACITY = 10;
    public static final Companion Companion;
    private final WindowBackend windowBackend;
    private final WindowMetricsCalculator windowMetricsCalculator;
    
    static {
        Companion = new Companion(null);
    }
    
    public WindowInfoTrackerImpl(final WindowMetricsCalculator windowMetricsCalculator, final WindowBackend windowBackend) {
        Intrinsics.checkNotNullParameter((Object)windowMetricsCalculator, "windowMetricsCalculator");
        Intrinsics.checkNotNullParameter((Object)windowBackend, "windowBackend");
        this.windowMetricsCalculator = windowMetricsCalculator;
        this.windowBackend = windowBackend;
    }
    
    @Override
    public Flow<WindowLayoutInfo> windowLayoutInfo(final Activity activity) {
        Intrinsics.checkNotNullParameter((Object)activity, "activity");
        return (Flow<WindowLayoutInfo>)FlowKt.callbackFlow((Function2)new WindowInfoTrackerImpl$windowLayoutInfo.WindowInfoTrackerImpl$windowLayoutInfo$2(this, activity, (Continuation)null));
    }
    
    @Override
    public Flow<WindowLayoutInfo> windowLayoutInfo(final Context context) {
        Intrinsics.checkNotNullParameter((Object)context, "context");
        return (Flow<WindowLayoutInfo>)FlowKt.callbackFlow((Function2)new WindowInfoTrackerImpl$windowLayoutInfo.WindowInfoTrackerImpl$windowLayoutInfo$1(this, context, (Continuation)null));
    }
    
    @Metadata(d1 = { "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\b\u0080\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082T¢\u0006\u0002\n\u0000¨\u0006\u0005" }, d2 = { "Landroidx/window/layout/WindowInfoTrackerImpl$Companion;", "", "()V", "BUFFER_CAPACITY", "", "window_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
    public static final class Companion
    {
        private Companion() {
        }
    }
}

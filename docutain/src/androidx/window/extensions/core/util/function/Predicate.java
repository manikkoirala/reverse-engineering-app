// 
// Decompiled by Procyon v0.6.0
// 

package androidx.window.extensions.core.util.function;

@FunctionalInterface
public interface Predicate<T>
{
    boolean test(final T p0);
}

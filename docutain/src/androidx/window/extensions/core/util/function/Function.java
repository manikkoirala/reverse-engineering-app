// 
// Decompiled by Procyon v0.6.0
// 

package androidx.window.extensions.core.util.function;

@FunctionalInterface
public interface Function<T, R>
{
    R apply(final T p0);
}

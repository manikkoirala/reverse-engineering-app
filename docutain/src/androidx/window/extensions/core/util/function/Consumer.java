// 
// Decompiled by Procyon v0.6.0
// 

package androidx.window.extensions.core.util.function;

@FunctionalInterface
public interface Consumer<T>
{
    void accept(final T p0);
}

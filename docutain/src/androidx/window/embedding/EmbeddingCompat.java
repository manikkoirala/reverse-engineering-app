// 
// Decompiled by Procyon v0.6.0
// 

package androidx.window.embedding;

import androidx.window.extensions.WindowExtensions;
import androidx.window.extensions.WindowExtensionsProvider;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Proxy;
import java.lang.reflect.Method;
import androidx.window.extensions.core.util.function.Function;
import java.util.Iterator;
import android.util.Log;
import androidx.window.core.VerificationMode;
import androidx.window.core.BuildConfig;
import java.util.Set;
import androidx.window.extensions.core.util.function.Consumer;
import kotlin.Unit;
import kotlin.reflect.KClass;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Reflection;
import androidx.window.core.ExtensionsUtil;
import android.app.Activity;
import androidx.window.extensions.embedding.SplitInfo;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.DefaultConstructorMarker;
import java.util.List;
import androidx.window.extensions.embedding.ActivityEmbeddingComponent;
import androidx.window.core.ConsumerAdapter;
import android.content.Context;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000\\\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\"\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0000\u0018\u0000 \u001e2\u00020\u0001:\u0001\u001eB%\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\t¢\u0006\u0002\u0010\nJ\b\u0010\u000b\u001a\u00020\fH\u0016J\u0010\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0010H\u0016J\b\u0010\u0011\u001a\u00020\u000eH\u0016J\u0010\u0010\u0012\u001a\u00020\f2\u0006\u0010\u0013\u001a\u00020\u0014H\u0016J\u0016\u0010\u0015\u001a\u00020\f2\f\u0010\u0016\u001a\b\u0012\u0004\u0012\u00020\u00180\u0017H\u0016J\u001c\u0010\u0019\u001a\u00020\f2\u0012\u0010\u001a\u001a\u000e\u0012\u0004\u0012\u00020\u001c\u0012\u0004\u0012\u00020\u001d0\u001bH\u0017R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u001f" }, d2 = { "Landroidx/window/embedding/EmbeddingCompat;", "Landroidx/window/embedding/EmbeddingInterfaceCompat;", "embeddingExtension", "Landroidx/window/extensions/embedding/ActivityEmbeddingComponent;", "adapter", "Landroidx/window/embedding/EmbeddingAdapter;", "consumerAdapter", "Landroidx/window/core/ConsumerAdapter;", "applicationContext", "Landroid/content/Context;", "(Landroidx/window/extensions/embedding/ActivityEmbeddingComponent;Landroidx/window/embedding/EmbeddingAdapter;Landroidx/window/core/ConsumerAdapter;Landroid/content/Context;)V", "clearSplitAttributesCalculator", "", "isActivityEmbedded", "", "activity", "Landroid/app/Activity;", "isSplitAttributesCalculatorSupported", "setEmbeddingCallback", "embeddingCallback", "Landroidx/window/embedding/EmbeddingInterfaceCompat$EmbeddingCallbackInterface;", "setRules", "rules", "", "Landroidx/window/embedding/EmbeddingRule;", "setSplitAttributesCalculator", "calculator", "Lkotlin/Function1;", "Landroidx/window/embedding/SplitAttributesCalculatorParams;", "Landroidx/window/embedding/SplitAttributes;", "Companion", "window_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
public final class EmbeddingCompat implements EmbeddingInterfaceCompat
{
    public static final Companion Companion;
    public static final boolean DEBUG = true;
    private static final String TAG = "EmbeddingCompat";
    private final EmbeddingAdapter adapter;
    private final Context applicationContext;
    private final ConsumerAdapter consumerAdapter;
    private final ActivityEmbeddingComponent embeddingExtension;
    
    static {
        Companion = new Companion(null);
    }
    
    public EmbeddingCompat(final ActivityEmbeddingComponent embeddingExtension, final EmbeddingAdapter adapter, final ConsumerAdapter consumerAdapter, final Context applicationContext) {
        Intrinsics.checkNotNullParameter((Object)embeddingExtension, "embeddingExtension");
        Intrinsics.checkNotNullParameter((Object)adapter, "adapter");
        Intrinsics.checkNotNullParameter((Object)consumerAdapter, "consumerAdapter");
        Intrinsics.checkNotNullParameter((Object)applicationContext, "applicationContext");
        this.embeddingExtension = embeddingExtension;
        this.adapter = adapter;
        this.consumerAdapter = consumerAdapter;
        this.applicationContext = applicationContext;
    }
    
    private static final void setEmbeddingCallback$lambda$0(final EmbeddingCallbackInterface embeddingCallbackInterface, final EmbeddingCompat embeddingCompat, final List list) {
        Intrinsics.checkNotNullParameter((Object)embeddingCallbackInterface, "$embeddingCallback");
        Intrinsics.checkNotNullParameter((Object)embeddingCompat, "this$0");
        final EmbeddingAdapter adapter = embeddingCompat.adapter;
        Intrinsics.checkNotNullExpressionValue((Object)list, "splitInfoList");
        embeddingCallbackInterface.onSplitInfoChanged(adapter.translate(list));
    }
    
    @Override
    public void clearSplitAttributesCalculator() {
        if (this.isSplitAttributesCalculatorSupported()) {
            this.embeddingExtension.clearSplitAttributesCalculator();
            return;
        }
        throw new UnsupportedOperationException("#clearSplitAttributesCalculator is not supported on the device.");
    }
    
    @Override
    public boolean isActivityEmbedded(final Activity activity) {
        Intrinsics.checkNotNullParameter((Object)activity, "activity");
        return this.embeddingExtension.isActivityEmbedded(activity);
    }
    
    @Override
    public boolean isSplitAttributesCalculatorSupported() {
        return ExtensionsUtil.INSTANCE.getSafeVendorApiLevel() >= 2;
    }
    
    @Override
    public void setEmbeddingCallback(final EmbeddingCallbackInterface embeddingCallbackInterface) {
        Intrinsics.checkNotNullParameter((Object)embeddingCallbackInterface, "embeddingCallback");
        if (ExtensionsUtil.INSTANCE.getSafeVendorApiLevel() < 2) {
            this.consumerAdapter.addConsumer(this.embeddingExtension, (kotlin.reflect.KClass<Object>)Reflection.getOrCreateKotlinClass((Class)List.class), "setSplitInfoCallback", (kotlin.jvm.functions.Function1<? super Object, Unit>)new EmbeddingCompat$setEmbeddingCallback.EmbeddingCompat$setEmbeddingCallback$1(embeddingCallbackInterface, this));
        }
        else {
            this.embeddingExtension.setSplitInfoCallback((Consumer)new EmbeddingCompat$$ExternalSyntheticLambda0(embeddingCallbackInterface, this));
        }
    }
    
    @Override
    public void setRules(final Set<? extends EmbeddingRule> set) {
        Intrinsics.checkNotNullParameter((Object)set, "rules");
        final Iterator<? extends EmbeddingRule> iterator = set.iterator();
        while (true) {
            while (iterator.hasNext()) {
                if (((EmbeddingRule)iterator.next()) instanceof SplitRule) {
                    final boolean b = true;
                    if (b && !Intrinsics.areEqual((Object)SplitController.Companion.getInstance(this.applicationContext).getSplitSupportStatus(), (Object)SplitController.SplitSupportStatus.SPLIT_AVAILABLE)) {
                        if (BuildConfig.INSTANCE.getVerificationMode() == VerificationMode.LOG) {
                            Log.w("EmbeddingCompat", "Cannot set SplitRule because ActivityEmbedding Split is not supported or PROPERTY_ACTIVITY_EMBEDDING_SPLITS_ENABLED is not set.");
                        }
                        return;
                    }
                    this.embeddingExtension.setEmbeddingRules((Set)this.adapter.translate(this.applicationContext, set));
                    return;
                }
            }
            final boolean b = false;
            continue;
        }
    }
    
    @Override
    public void setSplitAttributesCalculator(final Function1<? super SplitAttributesCalculatorParams, SplitAttributes> function1) {
        Intrinsics.checkNotNullParameter((Object)function1, "calculator");
        if (this.isSplitAttributesCalculatorSupported()) {
            this.embeddingExtension.setSplitAttributesCalculator((Function)this.adapter.translateSplitAttributesCalculator(function1));
            return;
        }
        throw new UnsupportedOperationException("#setSplitAttributesCalculator is not supported on the device.");
    }
    
    @Metadata(d1 = { "\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0006\u0010\u0007\u001a\u00020\bJ\b\u0010\t\u001a\u00020\bH\u0002J\u0006\u0010\n\u001a\u00020\u0004R\u000e\u0010\u0003\u001a\u00020\u0004X\u0086T¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082T¢\u0006\u0002\n\u0000¨\u0006\u000b" }, d2 = { "Landroidx/window/embedding/EmbeddingCompat$Companion;", "", "()V", "DEBUG", "", "TAG", "", "embeddingComponent", "Landroidx/window/extensions/embedding/ActivityEmbeddingComponent;", "emptyActivityEmbeddingProxy", "isEmbeddingAvailable", "window_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
    public static final class Companion
    {
        private Companion() {
        }
        
        private final ActivityEmbeddingComponent emptyActivityEmbeddingProxy() {
            final Object proxyInstance = Proxy.newProxyInstance(EmbeddingCompat.class.getClassLoader(), new Class[] { ActivityEmbeddingComponent.class }, new EmbeddingCompat$Companion$$ExternalSyntheticLambda0());
            Intrinsics.checkNotNull(proxyInstance, "null cannot be cast to non-null type androidx.window.extensions.embedding.ActivityEmbeddingComponent");
            return (ActivityEmbeddingComponent)proxyInstance;
        }
        
        private static final Unit emptyActivityEmbeddingProxy$lambda$2(final Object o, final Method method, final Object[] array) {
            return Unit.INSTANCE;
        }
        
        public final ActivityEmbeddingComponent embeddingComponent() {
            ActivityEmbeddingComponent activityEmbeddingComponent;
            if (this.isEmbeddingAvailable()) {
                final ClassLoader classLoader = EmbeddingCompat.class.getClassLoader();
                if (classLoader != null) {
                    final ConsumerAdapter consumerAdapter = new ConsumerAdapter(classLoader);
                    final WindowExtensions windowExtensions = WindowExtensionsProvider.getWindowExtensions();
                    Intrinsics.checkNotNullExpressionValue((Object)windowExtensions, "getWindowExtensions()");
                    if ((activityEmbeddingComponent = new SafeActivityEmbeddingComponentProvider(classLoader, consumerAdapter, windowExtensions).getActivityEmbeddingComponent()) != null) {
                        return activityEmbeddingComponent;
                    }
                }
                activityEmbeddingComponent = this.emptyActivityEmbeddingProxy();
            }
            else {
                activityEmbeddingComponent = this.emptyActivityEmbeddingProxy();
            }
            return activityEmbeddingComponent;
        }
        
        public final boolean isEmbeddingAvailable() {
            final boolean b = false;
            boolean b2;
            try {
                final ClassLoader classLoader = EmbeddingCompat.class.getClassLoader();
                b2 = b;
                if (classLoader != null) {
                    final ConsumerAdapter consumerAdapter = new ConsumerAdapter(classLoader);
                    final WindowExtensions windowExtensions = WindowExtensionsProvider.getWindowExtensions();
                    Intrinsics.checkNotNullExpressionValue((Object)windowExtensions, "getWindowExtensions()");
                    final ActivityEmbeddingComponent activityEmbeddingComponent = new SafeActivityEmbeddingComponentProvider(classLoader, consumerAdapter, windowExtensions).getActivityEmbeddingComponent();
                    b2 = b;
                    if (activityEmbeddingComponent != null) {
                        b2 = true;
                    }
                }
            }
            catch (final UnsupportedOperationException ex) {
                Log.d("EmbeddingCompat", "Stub Extension");
                b2 = b;
            }
            catch (final NoClassDefFoundError noClassDefFoundError) {
                Log.d("EmbeddingCompat", "Embedding extension version not found");
                b2 = b;
            }
            return b2;
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.window.embedding;

import android.content.res.Resources;
import android.content.res.Resources$NotFoundException;
import android.content.Intent;
import java.util.Set;
import kotlin.collections.SetsKt;
import android.content.res.TypedArray;
import androidx.window.R;
import android.util.AttributeSet;
import android.content.res.XmlResourceParser;
import android.content.Context;
import kotlin.text.StringsKt;
import android.content.ComponentName;
import java.util.Iterator;
import kotlin.jvm.internal.Intrinsics;
import java.util.HashSet;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000j\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\r\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\"\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u00c0\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u001a\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\b\u0010\u0007\u001a\u0004\u0018\u00010\bH\u0002J\u0018\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u000eH\u0002J\u0018\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u000eH\u0002J'\u0010\u0011\u001a\n\u0012\u0004\u0012\u00020\u0013\u0018\u00010\u00122\u0006\u0010\u000b\u001a\u00020\f2\b\b\u0001\u0010\u0014\u001a\u00020\u0015H\u0000¢\u0006\u0002\b\u0016J\u0018\u0010\u0017\u001a\u00020\u00182\u0006\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u000eH\u0002J\u0018\u0010\u0019\u001a\u00020\u001a2\u0006\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u000eH\u0002J\u0018\u0010\u001b\u001a\u00020\u001c2\u0006\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u000eH\u0002J$\u0010\u001d\u001a\u00020\u001e*\u0012\u0012\u0004\u0012\u00020\u00130\u001fj\b\u0012\u0004\u0012\u00020\u0013` 2\u0006\u0010!\u001a\u00020\u0013H\u0002¨\u0006\"" }, d2 = { "Landroidx/window/embedding/RuleParser;", "", "()V", "buildClassName", "Landroid/content/ComponentName;", "pkg", "", "clsSeq", "", "parseActivityFilter", "Landroidx/window/embedding/ActivityFilter;", "context", "Landroid/content/Context;", "parser", "Landroid/content/res/XmlResourceParser;", "parseActivityRule", "Landroidx/window/embedding/ActivityRule;", "parseRules", "", "Landroidx/window/embedding/EmbeddingRule;", "staticRuleResourceId", "", "parseRules$window_release", "parseSplitPairFilter", "Landroidx/window/embedding/SplitPairFilter;", "parseSplitPairRule", "Landroidx/window/embedding/SplitPairRule;", "parseSplitPlaceholderRule", "Landroidx/window/embedding/SplitPlaceholderRule;", "addRuleWithDuplicatedTagCheck", "", "Ljava/util/HashSet;", "Lkotlin/collections/HashSet;", "rule", "window_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
public final class RuleParser
{
    public static final RuleParser INSTANCE;
    
    static {
        INSTANCE = new RuleParser();
    }
    
    private RuleParser() {
    }
    
    private final void addRuleWithDuplicatedTagCheck(final HashSet<EmbeddingRule> set, final EmbeddingRule embeddingRule) {
        final String tag = embeddingRule.getTag();
        for (final EmbeddingRule embeddingRule2 : set) {
            if (tag != null) {
                if (!Intrinsics.areEqual((Object)tag, (Object)embeddingRule2.getTag())) {
                    continue;
                }
                final StringBuilder sb = new StringBuilder();
                sb.append("Duplicated tag: ");
                sb.append(tag);
                sb.append(" for ");
                sb.append(embeddingRule);
                sb.append(". The tag must be unique in XML rule definition.");
                throw new IllegalArgumentException(sb.toString());
            }
        }
        set.add(embeddingRule);
    }
    
    private final ComponentName buildClassName(String substring, final CharSequence charSequence) {
        if (charSequence == null || charSequence.length() == 0) {
            throw new IllegalArgumentException("Activity name must not be null");
        }
        final String string = charSequence.toString();
        if (string.charAt(0) == '.') {
            final StringBuilder sb = new StringBuilder();
            sb.append(substring);
            sb.append(string);
            return new ComponentName(substring, sb.toString());
        }
        final int indexOf$default = StringsKt.indexOf$default((CharSequence)string, '/', 0, false, 6, (Object)null);
        String substring2 = substring;
        substring = string;
        if (indexOf$default > 0) {
            substring2 = string.substring(0, indexOf$default);
            Intrinsics.checkNotNullExpressionValue((Object)substring2, "this as java.lang.String\u2026ing(startIndex, endIndex)");
            substring = string.substring(indexOf$default + 1);
            Intrinsics.checkNotNullExpressionValue((Object)substring, "this as java.lang.String).substring(startIndex)");
        }
        if (!Intrinsics.areEqual((Object)substring, (Object)"*") && StringsKt.indexOf$default((CharSequence)substring, '.', 0, false, 6, (Object)null) < 0) {
            final StringBuilder sb2 = new StringBuilder(substring2);
            sb2.append('.');
            sb2.append(substring);
            return new ComponentName(substring2, sb2.toString());
        }
        return new ComponentName(substring2, substring);
    }
    
    private final ActivityFilter parseActivityFilter(final Context context, final XmlResourceParser xmlResourceParser) {
        final TypedArray obtainStyledAttributes = context.getTheme().obtainStyledAttributes((AttributeSet)xmlResourceParser, R.styleable.ActivityFilter, 0, 0);
        final String string = obtainStyledAttributes.getString(R.styleable.ActivityFilter_activityName);
        final String string2 = obtainStyledAttributes.getString(R.styleable.ActivityFilter_activityAction);
        final String packageName = context.getApplicationContext().getPackageName();
        Intrinsics.checkNotNullExpressionValue((Object)packageName, "packageName");
        return new ActivityFilter(this.buildClassName(packageName, string), string2);
    }
    
    private final ActivityRule parseActivityRule(final Context context, final XmlResourceParser xmlResourceParser) {
        final TypedArray obtainStyledAttributes = context.getTheme().obtainStyledAttributes((AttributeSet)xmlResourceParser, R.styleable.ActivityRule, 0, 0);
        final String string = obtainStyledAttributes.getString(R.styleable.ActivityRule_tag);
        final boolean boolean1 = obtainStyledAttributes.getBoolean(R.styleable.ActivityRule_alwaysExpand, false);
        obtainStyledAttributes.recycle();
        final ActivityRule.Builder setAlwaysExpand = new ActivityRule.Builder(SetsKt.emptySet()).setAlwaysExpand(boolean1);
        if (string != null) {
            setAlwaysExpand.setTag(string);
        }
        return setAlwaysExpand.build();
    }
    
    private final SplitPairFilter parseSplitPairFilter(final Context context, final XmlResourceParser xmlResourceParser) {
        final TypedArray obtainStyledAttributes = context.getTheme().obtainStyledAttributes((AttributeSet)xmlResourceParser, R.styleable.SplitPairFilter, 0, 0);
        final String string = obtainStyledAttributes.getString(R.styleable.SplitPairFilter_primaryActivityName);
        final String string2 = obtainStyledAttributes.getString(R.styleable.SplitPairFilter_secondaryActivityName);
        final String string3 = obtainStyledAttributes.getString(R.styleable.SplitPairFilter_secondaryActivityAction);
        final String packageName = context.getApplicationContext().getPackageName();
        Intrinsics.checkNotNullExpressionValue((Object)packageName, "packageName");
        return new SplitPairFilter(this.buildClassName(packageName, string), this.buildClassName(packageName, string2), string3);
    }
    
    private final SplitPairRule parseSplitPairRule(final Context context, final XmlResourceParser xmlResourceParser) {
        final TypedArray obtainStyledAttributes = context.getTheme().obtainStyledAttributes((AttributeSet)xmlResourceParser, R.styleable.SplitPairRule, 0, 0);
        return new SplitPairRule.Builder(SetsKt.emptySet()).setTag(obtainStyledAttributes.getString(R.styleable.SplitPairRule_tag)).setMinWidthDp(obtainStyledAttributes.getInteger(R.styleable.SplitPairRule_splitMinWidthDp, 600)).setMinHeightDp(obtainStyledAttributes.getInteger(R.styleable.SplitPairRule_splitMinHeightDp, 600)).setMinSmallestWidthDp(obtainStyledAttributes.getInteger(R.styleable.SplitPairRule_splitMinSmallestWidthDp, 600)).setMaxAspectRatioInPortrait(EmbeddingAspectRatio.Companion.buildAspectRatioFromValue$window_release(obtainStyledAttributes.getFloat(R.styleable.SplitPairRule_splitMaxAspectRatioInPortrait, SplitRule.SPLIT_MAX_ASPECT_RATIO_PORTRAIT_DEFAULT.getValue$window_release()))).setMaxAspectRatioInLandscape(EmbeddingAspectRatio.Companion.buildAspectRatioFromValue$window_release(obtainStyledAttributes.getFloat(R.styleable.SplitPairRule_splitMaxAspectRatioInLandscape, SplitRule.SPLIT_MAX_ASPECT_RATIO_LANDSCAPE_DEFAULT.getValue$window_release()))).setFinishPrimaryWithSecondary(SplitRule.FinishBehavior.Companion.getFinishBehaviorFromValue$window_release(obtainStyledAttributes.getInt(R.styleable.SplitPairRule_finishPrimaryWithSecondary, SplitRule.FinishBehavior.NEVER.getValue$window_release()))).setFinishSecondaryWithPrimary(SplitRule.FinishBehavior.Companion.getFinishBehaviorFromValue$window_release(obtainStyledAttributes.getInt(R.styleable.SplitPairRule_finishSecondaryWithPrimary, SplitRule.FinishBehavior.ALWAYS.getValue$window_release()))).setClearTop(obtainStyledAttributes.getBoolean(R.styleable.SplitPairRule_clearTop, false)).setDefaultSplitAttributes(new SplitAttributes.Builder().setSplitType(SplitAttributes.SplitType.Companion.buildSplitTypeFromValue$window_release(obtainStyledAttributes.getFloat(R.styleable.SplitPairRule_splitRatio, 0.5f))).setLayoutDirection(SplitAttributes.LayoutDirection.Companion.getLayoutDirectionFromValue$window_release(obtainStyledAttributes.getInt(R.styleable.SplitPairRule_splitLayoutDirection, SplitAttributes.LayoutDirection.LOCALE.getValue$window_release()))).build()).build();
    }
    
    private final SplitPlaceholderRule parseSplitPlaceholderRule(final Context context, final XmlResourceParser xmlResourceParser) {
        final TypedArray obtainStyledAttributes = context.getTheme().obtainStyledAttributes((AttributeSet)xmlResourceParser, R.styleable.SplitPlaceholderRule, 0, 0);
        final String string = obtainStyledAttributes.getString(R.styleable.SplitPlaceholderRule_tag);
        final String string2 = obtainStyledAttributes.getString(R.styleable.SplitPlaceholderRule_placeholderActivityName);
        final boolean boolean1 = obtainStyledAttributes.getBoolean(R.styleable.SplitPlaceholderRule_stickyPlaceholder, false);
        final int int1 = obtainStyledAttributes.getInt(R.styleable.SplitPlaceholderRule_finishPrimaryWithPlaceholder, SplitRule.FinishBehavior.ALWAYS.getValue$window_release());
        if (int1 != SplitRule.FinishBehavior.NEVER.getValue$window_release()) {
            final float float1 = obtainStyledAttributes.getFloat(R.styleable.SplitPlaceholderRule_splitRatio, 0.5f);
            final int integer = obtainStyledAttributes.getInteger(R.styleable.SplitPlaceholderRule_splitMinWidthDp, 600);
            final int integer2 = obtainStyledAttributes.getInteger(R.styleable.SplitPlaceholderRule_splitMinHeightDp, 600);
            final int integer3 = obtainStyledAttributes.getInteger(R.styleable.SplitPlaceholderRule_splitMinSmallestWidthDp, 600);
            final float float2 = obtainStyledAttributes.getFloat(R.styleable.SplitPlaceholderRule_splitMaxAspectRatioInPortrait, SplitRule.SPLIT_MAX_ASPECT_RATIO_PORTRAIT_DEFAULT.getValue$window_release());
            final float float3 = obtainStyledAttributes.getFloat(R.styleable.SplitPlaceholderRule_splitMaxAspectRatioInLandscape, SplitRule.SPLIT_MAX_ASPECT_RATIO_LANDSCAPE_DEFAULT.getValue$window_release());
            final SplitAttributes build = new SplitAttributes.Builder().setSplitType(SplitAttributes.SplitType.Companion.buildSplitTypeFromValue$window_release(float1)).setLayoutDirection(SplitAttributes.LayoutDirection.Companion.getLayoutDirectionFromValue$window_release(obtainStyledAttributes.getInt(R.styleable.SplitPlaceholderRule_splitLayoutDirection, SplitAttributes.LayoutDirection.LOCALE.getValue$window_release()))).build();
            final String packageName = context.getApplicationContext().getPackageName();
            final RuleParser instance = RuleParser.INSTANCE;
            Intrinsics.checkNotNullExpressionValue((Object)packageName, "packageName");
            final ComponentName buildClassName = instance.buildClassName(packageName, string2);
            final Set emptySet = SetsKt.emptySet();
            final Intent setComponent = new Intent().setComponent(buildClassName);
            Intrinsics.checkNotNullExpressionValue((Object)setComponent, "Intent().setComponent(pl\u2026eholderActivityClassName)");
            return new SplitPlaceholderRule.Builder(emptySet, setComponent).setTag(string).setMinWidthDp(integer).setMinHeightDp(integer2).setMinSmallestWidthDp(integer3).setMaxAspectRatioInPortrait(EmbeddingAspectRatio.Companion.buildAspectRatioFromValue$window_release(float2)).setMaxAspectRatioInLandscape(EmbeddingAspectRatio.Companion.buildAspectRatioFromValue$window_release(float3)).setSticky(boolean1).setFinishPrimaryWithPlaceholder(SplitRule.FinishBehavior.Companion.getFinishBehaviorFromValue$window_release(int1)).setDefaultSplitAttributes(build).build();
        }
        throw new IllegalArgumentException("Never is not a valid configuration for Placeholder activities. Please use FINISH_ALWAYS or FINISH_ADJACENT instead or refer to the current API");
    }
    
    public final Set<EmbeddingRule> parseRules$window_release(final Context context, int n) {
        Intrinsics.checkNotNullParameter((Object)context, "context");
        final Resources resources = context.getResources();
        try {
            final XmlResourceParser xml = resources.getXml(n);
            Intrinsics.checkNotNullExpressionValue((Object)xml, "resources.getXml(staticRuleResourceId)");
            final HashSet set = new HashSet();
            final int depth = xml.getDepth();
            n = xml.next();
            ActivityRule o = null;
            SplitPlaceholderRule o3;
            SplitRule o2 = o3 = null;
            while (n != 1 && (n != 3 || xml.getDepth() > depth)) {
                if (xml.getEventType() == 2 && !Intrinsics.areEqual((Object)"split-config", (Object)xml.getName())) {
                    final String name = xml.getName();
                    ActivityRule activityRule = o;
                    SplitPairRule splitPairRule = (SplitPairRule)o2;
                    SplitPlaceholderRule splitPlaceholderRule = o3;
                    Label_0633: {
                        if (name != null) {
                            SplitPairRule splitPairRule2 = null;
                            Label_0475: {
                                Label_0326: {
                                    SplitPlaceholderRule splitPlaceholderRule2 = null;
                                    switch (name.hashCode()) {
                                        default: {
                                            activityRule = o;
                                            splitPairRule = (SplitPairRule)o2;
                                            splitPlaceholderRule = o3;
                                            break Label_0633;
                                        }
                                        case 2050988213: {
                                            if (!name.equals("SplitPlaceholderRule")) {
                                                activityRule = o;
                                                splitPairRule = (SplitPairRule)o2;
                                                splitPlaceholderRule = o3;
                                                break Label_0633;
                                            }
                                            splitPlaceholderRule2 = this.parseSplitPlaceholderRule(context, xml);
                                            this.addRuleWithDuplicatedTagCheck(set, splitPlaceholderRule2);
                                            activityRule = null;
                                            o2 = null;
                                            break;
                                        }
                                        case 1793077963: {
                                            if (!name.equals("ActivityRule")) {
                                                activityRule = o;
                                                splitPairRule = (SplitPairRule)o2;
                                                splitPlaceholderRule = o3;
                                                break Label_0633;
                                            }
                                            activityRule = this.parseActivityRule(context, xml);
                                            this.addRuleWithDuplicatedTagCheck(set, activityRule);
                                            o2 = null;
                                            o3 = null;
                                            break Label_0326;
                                        }
                                        case 1579230604: {
                                            if (!name.equals("SplitPairFilter")) {
                                                activityRule = o;
                                                splitPairRule = (SplitPairRule)o2;
                                                splitPlaceholderRule = o3;
                                                break Label_0633;
                                            }
                                            if (o2 != null) {
                                                final SplitPairFilter splitPairFilter = this.parseSplitPairFilter(context, xml);
                                                set.remove(o2);
                                                splitPairRule2 = ((SplitPairRule)o2).plus$window_release(splitPairFilter);
                                                this.addRuleWithDuplicatedTagCheck(set, splitPairRule2);
                                                activityRule = o;
                                                break Label_0475;
                                            }
                                            throw new IllegalArgumentException("Found orphaned SplitPairFilter outside of SplitPairRule");
                                        }
                                        case 520447504: {
                                            if (!name.equals("SplitPairRule")) {
                                                activityRule = o;
                                                splitPairRule = (SplitPairRule)o2;
                                                splitPlaceholderRule = o3;
                                                break Label_0633;
                                            }
                                            splitPairRule2 = this.parseSplitPairRule(context, xml);
                                            this.addRuleWithDuplicatedTagCheck(set, splitPairRule2);
                                            activityRule = null;
                                            o3 = null;
                                            break Label_0475;
                                        }
                                        case 511422343: {
                                            if (!name.equals("ActivityFilter")) {
                                                activityRule = o;
                                                splitPairRule = (SplitPairRule)o2;
                                                splitPlaceholderRule = o3;
                                                break Label_0633;
                                            }
                                            if (o == null && o3 == null) {
                                                throw new IllegalArgumentException("Found orphaned ActivityFilter");
                                            }
                                            final ActivityFilter activityFilter = this.parseActivityFilter(context, xml);
                                            if (o != null) {
                                                set.remove(o);
                                                activityRule = o.plus$window_release(activityFilter);
                                                this.addRuleWithDuplicatedTagCheck(set, activityRule);
                                                break Label_0326;
                                            }
                                            activityRule = o;
                                            splitPairRule = (SplitPairRule)o2;
                                            if ((splitPlaceholderRule = o3) != null) {
                                                set.remove(o3);
                                                splitPlaceholderRule2 = o3.plus$window_release(activityFilter);
                                                this.addRuleWithDuplicatedTagCheck(set, splitPlaceholderRule2);
                                                activityRule = o;
                                                break;
                                            }
                                            break Label_0633;
                                        }
                                    }
                                    splitPairRule = (SplitPairRule)o2;
                                    splitPlaceholderRule = splitPlaceholderRule2;
                                    break Label_0633;
                                }
                                splitPairRule = (SplitPairRule)o2;
                                splitPlaceholderRule = o3;
                                break Label_0633;
                            }
                            splitPairRule = splitPairRule2;
                            splitPlaceholderRule = o3;
                        }
                    }
                    n = xml.next();
                    o = activityRule;
                    o2 = splitPairRule;
                    o3 = splitPlaceholderRule;
                }
                else {
                    n = xml.next();
                }
            }
            return set;
        }
        catch (final Resources$NotFoundException ex) {
            return null;
        }
    }
}

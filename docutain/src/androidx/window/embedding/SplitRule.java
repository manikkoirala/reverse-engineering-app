// 
// Decompiled by Procyon v0.6.0
// 

package androidx.window.embedding;

import kotlin.jvm.JvmStatic;
import android.os.Build$VERSION;
import android.view.WindowMetrics;
import android.content.Context;
import android.graphics.Rect;
import androidx.core.util.Preconditions;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000R\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u000b\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0007\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0000\n\u0002\b\u0006\b\u0016\u0018\u0000 ,2\u00020\u0001:\u0003+,-BM\b\u0000\u0012\n\b\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\b\b\u0003\u0010\u0004\u001a\u00020\u0005\u0012\b\b\u0003\u0010\u0006\u001a\u00020\u0005\u0012\b\b\u0003\u0010\u0007\u001a\u00020\u0005\u0012\b\b\u0002\u0010\b\u001a\u00020\t\u0012\b\b\u0002\u0010\n\u001a\u00020\t\u0012\u0006\u0010\u000b\u001a\u00020\f¢\u0006\u0002\u0010\rJ\u001d\u0010\u0017\u001a\u00020\u00182\u0006\u0010\u0019\u001a\u00020\u001a2\u0006\u0010\u001b\u001a\u00020\u001cH\u0000¢\u0006\u0002\b\u001dJ\u001d\u0010\u001e\u001a\u00020\u00182\u0006\u0010\u001f\u001a\u00020 2\u0006\u0010!\u001a\u00020\"H\u0001¢\u0006\u0002\b#J\u001a\u0010$\u001a\u00020\u00052\u0006\u0010\u0019\u001a\u00020\u001a2\b\b\u0001\u0010%\u001a\u00020\u0005H\u0002J\u0013\u0010&\u001a\u00020\u00182\b\u0010'\u001a\u0004\u0018\u00010(H\u0096\u0002J\b\u0010)\u001a\u00020\u0005H\u0016J\b\u0010*\u001a\u00020\u0003H\u0016R\u0011\u0010\u000b\u001a\u00020\f¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\u000fR\u0011\u0010\n\u001a\u00020\t¢\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\u0011R\u0011\u0010\b\u001a\u00020\t¢\u0006\b\n\u0000\u001a\u0004\b\u0012\u0010\u0011R\u0011\u0010\u0006\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0013\u0010\u0014R\u0011\u0010\u0007\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0015\u0010\u0014R\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0016\u0010\u0014¨\u0006." }, d2 = { "Landroidx/window/embedding/SplitRule;", "Landroidx/window/embedding/EmbeddingRule;", "tag", "", "minWidthDp", "", "minHeightDp", "minSmallestWidthDp", "maxAspectRatioInPortrait", "Landroidx/window/embedding/EmbeddingAspectRatio;", "maxAspectRatioInLandscape", "defaultSplitAttributes", "Landroidx/window/embedding/SplitAttributes;", "(Ljava/lang/String;IIILandroidx/window/embedding/EmbeddingAspectRatio;Landroidx/window/embedding/EmbeddingAspectRatio;Landroidx/window/embedding/SplitAttributes;)V", "getDefaultSplitAttributes", "()Landroidx/window/embedding/SplitAttributes;", "getMaxAspectRatioInLandscape", "()Landroidx/window/embedding/EmbeddingAspectRatio;", "getMaxAspectRatioInPortrait", "getMinHeightDp", "()I", "getMinSmallestWidthDp", "getMinWidthDp", "checkParentBounds", "", "density", "", "bounds", "Landroid/graphics/Rect;", "checkParentBounds$window_release", "checkParentMetrics", "context", "Landroid/content/Context;", "parentMetrics", "Landroid/view/WindowMetrics;", "checkParentMetrics$window_release", "convertDpToPx", "dimensionDp", "equals", "other", "", "hashCode", "toString", "Api30Impl", "Companion", "FinishBehavior", "window_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
public class SplitRule extends EmbeddingRule
{
    public static final Companion Companion;
    public static final EmbeddingAspectRatio SPLIT_MAX_ASPECT_RATIO_LANDSCAPE_DEFAULT;
    public static final EmbeddingAspectRatio SPLIT_MAX_ASPECT_RATIO_PORTRAIT_DEFAULT;
    public static final int SPLIT_MIN_DIMENSION_ALWAYS_ALLOW = 0;
    public static final int SPLIT_MIN_DIMENSION_DP_DEFAULT = 600;
    private final SplitAttributes defaultSplitAttributes;
    private final EmbeddingAspectRatio maxAspectRatioInLandscape;
    private final EmbeddingAspectRatio maxAspectRatioInPortrait;
    private final int minHeightDp;
    private final int minSmallestWidthDp;
    private final int minWidthDp;
    
    static {
        Companion = new Companion(null);
        SPLIT_MAX_ASPECT_RATIO_PORTRAIT_DEFAULT = EmbeddingAspectRatio.Companion.ratio(1.4f);
        SPLIT_MAX_ASPECT_RATIO_LANDSCAPE_DEFAULT = EmbeddingAspectRatio.ALWAYS_ALLOW;
    }
    
    public SplitRule(final String s, final int minWidthDp, final int minHeightDp, final int minSmallestWidthDp, final EmbeddingAspectRatio maxAspectRatioInPortrait, final EmbeddingAspectRatio maxAspectRatioInLandscape, final SplitAttributes defaultSplitAttributes) {
        Intrinsics.checkNotNullParameter((Object)maxAspectRatioInPortrait, "maxAspectRatioInPortrait");
        Intrinsics.checkNotNullParameter((Object)maxAspectRatioInLandscape, "maxAspectRatioInLandscape");
        Intrinsics.checkNotNullParameter((Object)defaultSplitAttributes, "defaultSplitAttributes");
        super(s);
        this.minWidthDp = minWidthDp;
        this.minHeightDp = minHeightDp;
        this.minSmallestWidthDp = minSmallestWidthDp;
        this.maxAspectRatioInPortrait = maxAspectRatioInPortrait;
        this.maxAspectRatioInLandscape = maxAspectRatioInLandscape;
        this.defaultSplitAttributes = defaultSplitAttributes;
        Preconditions.checkArgumentNonnegative(minWidthDp, "minWidthDp must be non-negative");
        Preconditions.checkArgumentNonnegative(minHeightDp, "minHeightDp must be non-negative");
        Preconditions.checkArgumentNonnegative(minSmallestWidthDp, "minSmallestWidthDp must be non-negative");
    }
    
    private final int convertDpToPx(final float n, final int n2) {
        return (int)(n2 * n + 0.5f);
    }
    
    public final boolean checkParentBounds$window_release(final float n, final Rect rect) {
        Intrinsics.checkNotNullParameter((Object)rect, "bounds");
        final int width = rect.width();
        final int height = rect.height();
        boolean b2;
        final boolean b = b2 = false;
        if (width != 0) {
            if (height == 0) {
                b2 = b;
            }
            else {
                final int convertDpToPx = this.convertDpToPx(n, this.minWidthDp);
                final int convertDpToPx2 = this.convertDpToPx(n, this.minHeightDp);
                final int convertDpToPx3 = this.convertDpToPx(n, this.minSmallestWidthDp);
                final boolean b3 = this.minWidthDp == 0 || width >= convertDpToPx;
                final boolean b4 = this.minHeightDp == 0 || height >= convertDpToPx2;
                final boolean b5 = this.minSmallestWidthDp == 0 || Math.min(width, height) >= convertDpToPx3;
                boolean b6 = false;
                Label_0242: {
                    Label_0239: {
                        if (height >= width) {
                            if (Intrinsics.areEqual((Object)this.maxAspectRatioInPortrait, (Object)EmbeddingAspectRatio.ALWAYS_ALLOW)) {
                                break Label_0239;
                            }
                            if (height * 1.0f / width <= this.maxAspectRatioInPortrait.getValue$window_release()) {
                                break Label_0239;
                            }
                        }
                        else {
                            if (Intrinsics.areEqual((Object)this.maxAspectRatioInLandscape, (Object)EmbeddingAspectRatio.ALWAYS_ALLOW)) {
                                break Label_0239;
                            }
                            if (width * 1.0f / height <= this.maxAspectRatioInLandscape.getValue$window_release()) {
                                break Label_0239;
                            }
                        }
                        b6 = false;
                        break Label_0242;
                    }
                    b6 = true;
                }
                b2 = b;
                if (b3) {
                    b2 = b;
                    if (b4) {
                        b2 = b;
                        if (b5) {
                            b2 = b;
                            if (b6) {
                                b2 = true;
                            }
                        }
                    }
                }
            }
        }
        return b2;
    }
    
    public final boolean checkParentMetrics$window_release(final Context context, final WindowMetrics windowMetrics) {
        Intrinsics.checkNotNullParameter((Object)context, "context");
        Intrinsics.checkNotNullParameter((Object)windowMetrics, "parentMetrics");
        return Build$VERSION.SDK_INT > 30 && this.checkParentBounds$window_release(context.getResources().getDisplayMetrics().density, Api30Impl.INSTANCE.getBounds(windowMetrics));
    }
    
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof SplitRule)) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        final int minWidthDp = this.minWidthDp;
        final SplitRule splitRule = (SplitRule)o;
        return minWidthDp == splitRule.minWidthDp && this.minHeightDp == splitRule.minHeightDp && this.minSmallestWidthDp == splitRule.minSmallestWidthDp && Intrinsics.areEqual((Object)this.maxAspectRatioInPortrait, (Object)splitRule.maxAspectRatioInPortrait) && Intrinsics.areEqual((Object)this.maxAspectRatioInLandscape, (Object)splitRule.maxAspectRatioInLandscape) && Intrinsics.areEqual((Object)this.defaultSplitAttributes, (Object)splitRule.defaultSplitAttributes);
    }
    
    public final SplitAttributes getDefaultSplitAttributes() {
        return this.defaultSplitAttributes;
    }
    
    public final EmbeddingAspectRatio getMaxAspectRatioInLandscape() {
        return this.maxAspectRatioInLandscape;
    }
    
    public final EmbeddingAspectRatio getMaxAspectRatioInPortrait() {
        return this.maxAspectRatioInPortrait;
    }
    
    public final int getMinHeightDp() {
        return this.minHeightDp;
    }
    
    public final int getMinSmallestWidthDp() {
        return this.minSmallestWidthDp;
    }
    
    public final int getMinWidthDp() {
        return this.minWidthDp;
    }
    
    @Override
    public int hashCode() {
        return (((((super.hashCode() * 31 + this.minWidthDp) * 31 + this.minHeightDp) * 31 + this.minSmallestWidthDp) * 31 + this.maxAspectRatioInPortrait.hashCode()) * 31 + this.maxAspectRatioInLandscape.hashCode()) * 31 + this.defaultSplitAttributes.hashCode();
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("SplitRule");
        sb.append("{ tag=");
        sb.append(this.getTag());
        sb.append(", defaultSplitAttributes=");
        sb.append(this.defaultSplitAttributes);
        sb.append(", minWidthDp=");
        sb.append(this.minWidthDp);
        sb.append(", minHeightDp=");
        sb.append(this.minHeightDp);
        sb.append(", minSmallestWidthDp=");
        sb.append(this.minSmallestWidthDp);
        sb.append(", maxAspectRatioInPortrait=");
        sb.append(this.maxAspectRatioInPortrait);
        sb.append(", maxAspectRatioInLandscape=");
        sb.append(this.maxAspectRatioInLandscape);
        sb.append('}');
        return sb.toString();
    }
    
    @Metadata(d1 = { "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u00c1\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0007¨\u0006\u0007" }, d2 = { "Landroidx/window/embedding/SplitRule$Api30Impl;", "", "()V", "getBounds", "Landroid/graphics/Rect;", "windowMetrics", "Landroid/view/WindowMetrics;", "window_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
    public static final class Api30Impl
    {
        public static final Api30Impl INSTANCE;
        
        static {
            INSTANCE = new Api30Impl();
        }
        
        private Api30Impl() {
        }
        
        public final Rect getBounds(final WindowMetrics windowMetrics) {
            Intrinsics.checkNotNullParameter((Object)windowMetrics, "windowMetrics");
            final Rect bounds = windowMetrics.getBounds();
            Intrinsics.checkNotNullExpressionValue((Object)bounds, "windowMetrics.bounds");
            return bounds;
        }
    }
    
    @Metadata(d1 = { "\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u0010\u0010\u0003\u001a\u00020\u00048\u0006X\u0087\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\u0005\u001a\u00020\u00048\u0006X\u0087\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0086T¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0007X\u0086T¢\u0006\u0002\n\u0000¨\u0006\t" }, d2 = { "Landroidx/window/embedding/SplitRule$Companion;", "", "()V", "SPLIT_MAX_ASPECT_RATIO_LANDSCAPE_DEFAULT", "Landroidx/window/embedding/EmbeddingAspectRatio;", "SPLIT_MAX_ASPECT_RATIO_PORTRAIT_DEFAULT", "SPLIT_MIN_DIMENSION_ALWAYS_ALLOW", "", "SPLIT_MIN_DIMENSION_DP_DEFAULT", "window_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
    public static final class Companion
    {
        private Companion() {
        }
    }
    
    @Metadata(d1 = { "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0002\b\u0006\u0018\u0000 \n2\u00020\u0001:\u0001\nB\u0017\b\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\b\u0010\t\u001a\u00020\u0003H\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004¢\u0006\u0002\n\u0000R\u0014\u0010\u0004\u001a\u00020\u0005X\u0080\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\b¨\u0006\u000b" }, d2 = { "Landroidx/window/embedding/SplitRule$FinishBehavior;", "", "description", "", "value", "", "(Ljava/lang/String;I)V", "getValue$window_release", "()I", "toString", "Companion", "window_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
    public static final class FinishBehavior
    {
        public static final FinishBehavior ADJACENT;
        public static final FinishBehavior ALWAYS;
        public static final Companion Companion;
        public static final FinishBehavior NEVER;
        private final String description;
        private final int value;
        
        static {
            Companion = new Companion(null);
            NEVER = new FinishBehavior("NEVER", 0);
            ALWAYS = new FinishBehavior("ALWAYS", 1);
            ADJACENT = new FinishBehavior("ADJACENT", 2);
        }
        
        private FinishBehavior(final String description, final int value) {
            this.description = description;
            this.value = value;
        }
        
        @JvmStatic
        public static final FinishBehavior getFinishBehaviorFromValue$window_release(final int n) {
            return FinishBehavior.Companion.getFinishBehaviorFromValue$window_release(n);
        }
        
        public final int getValue$window_release() {
            return this.value;
        }
        
        @Override
        public String toString() {
            return this.description;
        }
        
        @Metadata(d1 = { "\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\b\n\u0002\b\u0002\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0017\u0010\u0007\u001a\u00020\u00042\b\b\u0001\u0010\b\u001a\u00020\tH\u0001¢\u0006\u0002\b\nR\u0010\u0010\u0003\u001a\u00020\u00048\u0006X\u0087\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\u0005\u001a\u00020\u00048\u0006X\u0087\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\u0006\u001a\u00020\u00048\u0006X\u0087\u0004¢\u0006\u0002\n\u0000¨\u0006\u000b" }, d2 = { "Landroidx/window/embedding/SplitRule$FinishBehavior$Companion;", "", "()V", "ADJACENT", "Landroidx/window/embedding/SplitRule$FinishBehavior;", "ALWAYS", "NEVER", "getFinishBehaviorFromValue", "value", "", "getFinishBehaviorFromValue$window_release", "window_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
        public static final class Companion
        {
            private Companion() {
            }
            
            @JvmStatic
            public final FinishBehavior getFinishBehaviorFromValue$window_release(final int i) {
                FinishBehavior finishBehavior;
                if (i == FinishBehavior.NEVER.getValue$window_release()) {
                    finishBehavior = FinishBehavior.NEVER;
                }
                else if (i == FinishBehavior.ALWAYS.getValue$window_release()) {
                    finishBehavior = FinishBehavior.ALWAYS;
                }
                else {
                    if (i != FinishBehavior.ADJACENT.getValue$window_release()) {
                        final StringBuilder sb = new StringBuilder();
                        sb.append("Unknown finish behavior:");
                        sb.append(i);
                        throw new IllegalArgumentException(sb.toString());
                    }
                    finishBehavior = FinishBehavior.ADJACENT;
                }
                return finishBehavior;
            }
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.window.embedding;

import kotlin.collections.CollectionsKt;
import kotlin.collections.SetsKt;
import kotlin.jvm.internal.Intrinsics;
import java.util.Set;
import kotlin.jvm.JvmStatic;
import android.content.Context;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\"\n\u0002\b\u0005\u0018\u0000 \u000f2\u00020\u0001:\u0001\u000fB\u000f\b\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u000e\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bJ\u0006\u0010\t\u001a\u00020\u0006J\f\u0010\n\u001a\b\u0012\u0004\u0012\u00020\b0\u000bJ\u000e\u0010\f\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bJ\u0014\u0010\r\u001a\u00020\u00062\f\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\b0\u000bR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u0010" }, d2 = { "Landroidx/window/embedding/RuleController;", "", "embeddingBackend", "Landroidx/window/embedding/EmbeddingBackend;", "(Landroidx/window/embedding/EmbeddingBackend;)V", "addRule", "", "rule", "Landroidx/window/embedding/EmbeddingRule;", "clearRules", "getRules", "", "removeRule", "setRules", "rules", "Companion", "window_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
public final class RuleController
{
    public static final Companion Companion;
    private final EmbeddingBackend embeddingBackend;
    
    static {
        Companion = new Companion(null);
    }
    
    private RuleController(final EmbeddingBackend embeddingBackend) {
        this.embeddingBackend = embeddingBackend;
    }
    
    @JvmStatic
    public static final RuleController getInstance(final Context context) {
        return RuleController.Companion.getInstance(context);
    }
    
    @JvmStatic
    public static final Set<EmbeddingRule> parseRules(final Context context, final int n) {
        return RuleController.Companion.parseRules(context, n);
    }
    
    public final void addRule(final EmbeddingRule embeddingRule) {
        Intrinsics.checkNotNullParameter((Object)embeddingRule, "rule");
        this.embeddingBackend.addRule(embeddingRule);
    }
    
    public final void clearRules() {
        this.embeddingBackend.setRules(SetsKt.emptySet());
    }
    
    public final Set<EmbeddingRule> getRules() {
        return CollectionsKt.toSet((Iterable)this.embeddingBackend.getRules());
    }
    
    public final void removeRule(final EmbeddingRule embeddingRule) {
        Intrinsics.checkNotNullParameter((Object)embeddingRule, "rule");
        this.embeddingBackend.removeRule(embeddingRule);
    }
    
    public final void setRules(final Set<? extends EmbeddingRule> rules) {
        Intrinsics.checkNotNullParameter((Object)rules, "rules");
        this.embeddingBackend.setRules(rules);
    }
    
    @Metadata(d1 = { "\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\"\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0007J \u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\t0\b2\u0006\u0010\u0005\u001a\u00020\u00062\b\b\u0001\u0010\n\u001a\u00020\u000bH\u0007¨\u0006\f" }, d2 = { "Landroidx/window/embedding/RuleController$Companion;", "", "()V", "getInstance", "Landroidx/window/embedding/RuleController;", "context", "Landroid/content/Context;", "parseRules", "", "Landroidx/window/embedding/EmbeddingRule;", "staticRuleResourceId", "", "window_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
    public static final class Companion
    {
        private Companion() {
        }
        
        @JvmStatic
        public final RuleController getInstance(Context applicationContext) {
            Intrinsics.checkNotNullParameter((Object)applicationContext, "context");
            applicationContext = applicationContext.getApplicationContext();
            final EmbeddingBackend.Companion companion = EmbeddingBackend.Companion;
            Intrinsics.checkNotNullExpressionValue((Object)applicationContext, "applicationContext");
            return new RuleController(companion.getInstance(applicationContext), null);
        }
        
        @JvmStatic
        public final Set<EmbeddingRule> parseRules(Context applicationContext, final int n) {
            Intrinsics.checkNotNullParameter((Object)applicationContext, "context");
            final RuleParser instance = RuleParser.INSTANCE;
            applicationContext = applicationContext.getApplicationContext();
            Intrinsics.checkNotNullExpressionValue((Object)applicationContext, "context.applicationContext");
            Set<EmbeddingRule> set;
            if ((set = instance.parseRules$window_release(applicationContext, n)) == null) {
                set = SetsKt.emptySet();
            }
            return set;
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.window.embedding;

import java.util.Collection;
import java.util.LinkedHashSet;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.collections.CollectionsKt;
import androidx.core.util.Preconditions;
import kotlin.jvm.internal.Intrinsics;
import android.content.Intent;
import java.util.Set;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000L\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\"\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0010\u0000\n\u0002\b\u0007\u0018\u00002\u00020\u0001:\u0001&Bu\b\u0010\u0012\n\b\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005\u0012\u0006\u0010\u0007\u001a\u00020\b\u0012\u0006\u0010\t\u001a\u00020\n\u0012\b\b\u0002\u0010\u000b\u001a\u00020\f\u0012\b\b\u0003\u0010\r\u001a\u00020\u000e\u0012\b\b\u0003\u0010\u000f\u001a\u00020\u000e\u0012\b\b\u0003\u0010\u0010\u001a\u00020\u000e\u0012\b\b\u0002\u0010\u0011\u001a\u00020\u0012\u0012\b\b\u0002\u0010\u0013\u001a\u00020\u0012\u0012\u0006\u0010\u0014\u001a\u00020\u0015¢\u0006\u0002\u0010\u0016J\u0013\u0010\u001e\u001a\u00020\n2\b\u0010\u001f\u001a\u0004\u0018\u00010 H\u0096\u0002J\b\u0010!\u001a\u00020\u000eH\u0016J\u0016\u0010\"\u001a\u00020\u00002\u0006\u0010#\u001a\u00020\u0006H\u0080\u0002¢\u0006\u0002\b$J\b\u0010%\u001a\u00020\u0003H\u0016R\u0017\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0017\u0010\u0018R\u0011\u0010\u000b\u001a\u00020\f¢\u0006\b\n\u0000\u001a\u0004\b\u0019\u0010\u001aR\u0011\u0010\t\u001a\u00020\n¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\u001bR\u0011\u0010\u0007\u001a\u00020\b¢\u0006\b\n\u0000\u001a\u0004\b\u001c\u0010\u001d¨\u0006'" }, d2 = { "Landroidx/window/embedding/SplitPlaceholderRule;", "Landroidx/window/embedding/SplitRule;", "tag", "", "filters", "", "Landroidx/window/embedding/ActivityFilter;", "placeholderIntent", "Landroid/content/Intent;", "isSticky", "", "finishPrimaryWithPlaceholder", "Landroidx/window/embedding/SplitRule$FinishBehavior;", "minWidthDp", "", "minHeightDp", "minSmallestWidthDp", "maxAspectRatioInPortrait", "Landroidx/window/embedding/EmbeddingAspectRatio;", "maxAspectRatioInLandscape", "defaultSplitAttributes", "Landroidx/window/embedding/SplitAttributes;", "(Ljava/lang/String;Ljava/util/Set;Landroid/content/Intent;ZLandroidx/window/embedding/SplitRule$FinishBehavior;IIILandroidx/window/embedding/EmbeddingAspectRatio;Landroidx/window/embedding/EmbeddingAspectRatio;Landroidx/window/embedding/SplitAttributes;)V", "getFilters", "()Ljava/util/Set;", "getFinishPrimaryWithPlaceholder", "()Landroidx/window/embedding/SplitRule$FinishBehavior;", "()Z", "getPlaceholderIntent", "()Landroid/content/Intent;", "equals", "other", "", "hashCode", "plus", "filter", "plus$window_release", "toString", "Builder", "window_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
public final class SplitPlaceholderRule extends SplitRule
{
    private final Set<ActivityFilter> filters;
    private final FinishBehavior finishPrimaryWithPlaceholder;
    private final boolean isSticky;
    private final Intent placeholderIntent;
    
    public SplitPlaceholderRule(final String s, final Set<ActivityFilter> set, final Intent placeholderIntent, final boolean isSticky, final FinishBehavior finishPrimaryWithPlaceholder, final int n, final int n2, final int n3, final EmbeddingAspectRatio embeddingAspectRatio, final EmbeddingAspectRatio embeddingAspectRatio2, final SplitAttributes splitAttributes) {
        Intrinsics.checkNotNullParameter((Object)set, "filters");
        Intrinsics.checkNotNullParameter((Object)placeholderIntent, "placeholderIntent");
        Intrinsics.checkNotNullParameter((Object)finishPrimaryWithPlaceholder, "finishPrimaryWithPlaceholder");
        Intrinsics.checkNotNullParameter((Object)embeddingAspectRatio, "maxAspectRatioInPortrait");
        Intrinsics.checkNotNullParameter((Object)embeddingAspectRatio2, "maxAspectRatioInLandscape");
        Intrinsics.checkNotNullParameter((Object)splitAttributes, "defaultSplitAttributes");
        super(s, n, n2, n3, embeddingAspectRatio, embeddingAspectRatio2, splitAttributes);
        Preconditions.checkArgument(Intrinsics.areEqual((Object)finishPrimaryWithPlaceholder, (Object)FinishBehavior.NEVER) ^ true, "NEVER is not a valid configuration for SplitPlaceholderRule. Please use FINISH_ALWAYS or FINISH_ADJACENT instead or refer to the current API.", new Object[0]);
        this.filters = CollectionsKt.toSet((Iterable)set);
        this.placeholderIntent = placeholderIntent;
        this.isSticky = isSticky;
        this.finishPrimaryWithPlaceholder = finishPrimaryWithPlaceholder;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof SplitPlaceholderRule)) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        final Intent placeholderIntent = this.placeholderIntent;
        final SplitPlaceholderRule splitPlaceholderRule = (SplitPlaceholderRule)o;
        return Intrinsics.areEqual((Object)placeholderIntent, (Object)splitPlaceholderRule.placeholderIntent) && this.isSticky == splitPlaceholderRule.isSticky && Intrinsics.areEqual((Object)this.finishPrimaryWithPlaceholder, (Object)splitPlaceholderRule.finishPrimaryWithPlaceholder) && Intrinsics.areEqual((Object)this.filters, (Object)splitPlaceholderRule.filters);
    }
    
    public final Set<ActivityFilter> getFilters() {
        return this.filters;
    }
    
    public final FinishBehavior getFinishPrimaryWithPlaceholder() {
        return this.finishPrimaryWithPlaceholder;
    }
    
    public final Intent getPlaceholderIntent() {
        return this.placeholderIntent;
    }
    
    @Override
    public int hashCode() {
        return (((super.hashCode() * 31 + this.placeholderIntent.hashCode()) * 31 + ActivityRule$$ExternalSyntheticBackport0.m(this.isSticky)) * 31 + this.finishPrimaryWithPlaceholder.hashCode()) * 31 + this.filters.hashCode();
    }
    
    public final boolean isSticky() {
        return this.isSticky;
    }
    
    public final SplitPlaceholderRule plus$window_release(final ActivityFilter activityFilter) {
        Intrinsics.checkNotNullParameter((Object)activityFilter, "filter");
        final Set set = new LinkedHashSet();
        set.addAll(this.filters);
        set.add(activityFilter);
        return new Builder(CollectionsKt.toSet((Iterable)set), this.placeholderIntent).setTag(this.getTag()).setMinWidthDp(this.getMinWidthDp()).setMinHeightDp(this.getMinHeightDp()).setMinSmallestWidthDp(this.getMinSmallestWidthDp()).setMaxAspectRatioInPortrait(this.getMaxAspectRatioInPortrait()).setMaxAspectRatioInLandscape(this.getMaxAspectRatioInLandscape()).setSticky(this.isSticky).setFinishPrimaryWithPlaceholder(this.finishPrimaryWithPlaceholder).setDefaultSplitAttributes(this.getDefaultSplitAttributes()).build();
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("SplitPlaceholderRule{tag=");
        sb.append(this.getTag());
        sb.append(", defaultSplitAttributes=");
        sb.append(this.getDefaultSplitAttributes());
        sb.append(", minWidthDp=");
        sb.append(this.getMinWidthDp());
        sb.append(", minHeightDp=");
        sb.append(this.getMinHeightDp());
        sb.append(", minSmallestWidthDp=");
        sb.append(this.getMinSmallestWidthDp());
        sb.append(", maxAspectRatioInPortrait=");
        sb.append(this.getMaxAspectRatioInPortrait());
        sb.append(", maxAspectRatioInLandscape=");
        sb.append(this.getMaxAspectRatioInLandscape());
        sb.append(", placeholderIntent=");
        sb.append(this.placeholderIntent);
        sb.append(", isSticky=");
        sb.append(this.isSticky);
        sb.append(", finishPrimaryWithPlaceholder=");
        sb.append(this.finishPrimaryWithPlaceholder);
        sb.append(", filters=");
        sb.append(this.filters);
        sb.append('}');
        return sb.toString();
    }
    
    @Metadata(d1 = { "\u0000L\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\"\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u000b\u0018\u00002\u00020\u0001B\u001b\u0012\f\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006¢\u0006\u0002\u0010\u0007J\u0006\u0010\u0017\u001a\u00020\u0018J\u000e\u0010\u0019\u001a\u00020\u00002\u0006\u0010\b\u001a\u00020\tJ\u000e\u0010\u001a\u001a\u00020\u00002\u0006\u0010\n\u001a\u00020\u000bJ\u000e\u0010\u001b\u001a\u00020\u00002\u0006\u0010\u001c\u001a\u00020\u000fJ\u000e\u0010\u001d\u001a\u00020\u00002\u0006\u0010\u001c\u001a\u00020\u000fJ\u0010\u0010\u001e\u001a\u00020\u00002\b\b\u0001\u0010\u0011\u001a\u00020\u0012J\u0010\u0010\u001f\u001a\u00020\u00002\b\b\u0001\u0010\u0013\u001a\u00020\u0012J\u0010\u0010 \u001a\u00020\u00002\b\b\u0001\u0010\u0014\u001a\u00020\u0012J\u000e\u0010!\u001a\u00020\u00002\u0006\u0010\f\u001a\u00020\rJ\u0010\u0010\"\u001a\u00020\u00002\b\u0010\u0015\u001a\u0004\u0018\u00010\u0016R\u000e\u0010\b\u001a\u00020\tX\u0082\u000e¢\u0006\u0002\n\u0000R\u0014\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\rX\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u000fX\u0082\u000e¢\u0006\u0002\n\u0000R\u0012\u0010\u0011\u001a\u00020\u00128\u0002@\u0002X\u0083\u000e¢\u0006\u0002\n\u0000R\u0012\u0010\u0013\u001a\u00020\u00128\u0002@\u0002X\u0083\u000e¢\u0006\u0002\n\u0000R\u0012\u0010\u0014\u001a\u00020\u00128\u0002@\u0002X\u0083\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\u0015\u001a\u0004\u0018\u00010\u0016X\u0082\u000e¢\u0006\u0002\n\u0000¨\u0006#" }, d2 = { "Landroidx/window/embedding/SplitPlaceholderRule$Builder;", "", "filters", "", "Landroidx/window/embedding/ActivityFilter;", "placeholderIntent", "Landroid/content/Intent;", "(Ljava/util/Set;Landroid/content/Intent;)V", "defaultSplitAttributes", "Landroidx/window/embedding/SplitAttributes;", "finishPrimaryWithPlaceholder", "Landroidx/window/embedding/SplitRule$FinishBehavior;", "isSticky", "", "maxAspectRatioInLandscape", "Landroidx/window/embedding/EmbeddingAspectRatio;", "maxAspectRatioInPortrait", "minHeightDp", "", "minSmallestWidthDp", "minWidthDp", "tag", "", "build", "Landroidx/window/embedding/SplitPlaceholderRule;", "setDefaultSplitAttributes", "setFinishPrimaryWithPlaceholder", "setMaxAspectRatioInLandscape", "aspectRatio", "setMaxAspectRatioInPortrait", "setMinHeightDp", "setMinSmallestWidthDp", "setMinWidthDp", "setSticky", "setTag", "window_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
    public static final class Builder
    {
        private SplitAttributes defaultSplitAttributes;
        private final Set<ActivityFilter> filters;
        private FinishBehavior finishPrimaryWithPlaceholder;
        private boolean isSticky;
        private EmbeddingAspectRatio maxAspectRatioInLandscape;
        private EmbeddingAspectRatio maxAspectRatioInPortrait;
        private int minHeightDp;
        private int minSmallestWidthDp;
        private int minWidthDp;
        private final Intent placeholderIntent;
        private String tag;
        
        public Builder(final Set<ActivityFilter> filters, final Intent placeholderIntent) {
            Intrinsics.checkNotNullParameter((Object)filters, "filters");
            Intrinsics.checkNotNullParameter((Object)placeholderIntent, "placeholderIntent");
            this.filters = filters;
            this.placeholderIntent = placeholderIntent;
            this.minWidthDp = 600;
            this.minHeightDp = 600;
            this.minSmallestWidthDp = 600;
            this.maxAspectRatioInPortrait = SplitRule.SPLIT_MAX_ASPECT_RATIO_PORTRAIT_DEFAULT;
            this.maxAspectRatioInLandscape = SplitRule.SPLIT_MAX_ASPECT_RATIO_LANDSCAPE_DEFAULT;
            this.finishPrimaryWithPlaceholder = FinishBehavior.ALWAYS;
            this.defaultSplitAttributes = new SplitAttributes.Builder().build();
        }
        
        public final SplitPlaceholderRule build() {
            return new SplitPlaceholderRule(this.tag, this.filters, this.placeholderIntent, this.isSticky, this.finishPrimaryWithPlaceholder, this.minWidthDp, this.minHeightDp, this.minSmallestWidthDp, this.maxAspectRatioInPortrait, this.maxAspectRatioInLandscape, this.defaultSplitAttributes);
        }
        
        public final Builder setDefaultSplitAttributes(final SplitAttributes defaultSplitAttributes) {
            Intrinsics.checkNotNullParameter((Object)defaultSplitAttributes, "defaultSplitAttributes");
            final Builder builder = this;
            this.defaultSplitAttributes = defaultSplitAttributes;
            return this;
        }
        
        public final Builder setFinishPrimaryWithPlaceholder(final FinishBehavior finishPrimaryWithPlaceholder) {
            Intrinsics.checkNotNullParameter((Object)finishPrimaryWithPlaceholder, "finishPrimaryWithPlaceholder");
            final Builder builder = this;
            this.finishPrimaryWithPlaceholder = finishPrimaryWithPlaceholder;
            return this;
        }
        
        public final Builder setMaxAspectRatioInLandscape(final EmbeddingAspectRatio maxAspectRatioInLandscape) {
            Intrinsics.checkNotNullParameter((Object)maxAspectRatioInLandscape, "aspectRatio");
            final Builder builder = this;
            this.maxAspectRatioInLandscape = maxAspectRatioInLandscape;
            return this;
        }
        
        public final Builder setMaxAspectRatioInPortrait(final EmbeddingAspectRatio maxAspectRatioInPortrait) {
            Intrinsics.checkNotNullParameter((Object)maxAspectRatioInPortrait, "aspectRatio");
            final Builder builder = this;
            this.maxAspectRatioInPortrait = maxAspectRatioInPortrait;
            return this;
        }
        
        public final Builder setMinHeightDp(final int minHeightDp) {
            final Builder builder = this;
            this.minHeightDp = minHeightDp;
            return this;
        }
        
        public final Builder setMinSmallestWidthDp(final int minSmallestWidthDp) {
            final Builder builder = this;
            this.minSmallestWidthDp = minSmallestWidthDp;
            return this;
        }
        
        public final Builder setMinWidthDp(final int minWidthDp) {
            final Builder builder = this;
            this.minWidthDp = minWidthDp;
            return this;
        }
        
        public final Builder setSticky(final boolean isSticky) {
            final Builder builder = this;
            this.isSticky = isSticky;
            return this;
        }
        
        public final Builder setTag(final String tag) {
            final Builder builder = this;
            this.tag = tag;
            return this;
        }
    }
}

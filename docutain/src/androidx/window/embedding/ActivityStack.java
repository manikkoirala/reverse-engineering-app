// 
// Decompiled by Procyon v0.6.0
// 

package androidx.window.embedding;

import kotlin.jvm.internal.Intrinsics;
import android.app.Activity;
import java.util.List;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\t\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\u0018\u00002\u00020\u0001B\u001d\b\u0007\u0012\f\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006¢\u0006\u0002\u0010\u0007J\u0011\u0010\u000b\u001a\u00020\u00062\u0006\u0010\f\u001a\u00020\u0004H\u0086\u0002J\u0013\u0010\r\u001a\u00020\u00062\b\u0010\u000e\u001a\u0004\u0018\u00010\u0001H\u0096\u0002J\b\u0010\u000f\u001a\u00020\u0010H\u0016J\b\u0010\u0011\u001a\u00020\u0012H\u0016R\u001a\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003X\u0080\u0004¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010\tR\u0011\u0010\u0005\u001a\u00020\u0006¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\n¨\u0006\u0013" }, d2 = { "Landroidx/window/embedding/ActivityStack;", "", "activitiesInProcess", "", "Landroid/app/Activity;", "isEmpty", "", "(Ljava/util/List;Z)V", "getActivitiesInProcess$window_release", "()Ljava/util/List;", "()Z", "contains", "activity", "equals", "other", "hashCode", "", "toString", "", "window_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
public final class ActivityStack
{
    private final List<Activity> activitiesInProcess;
    private final boolean isEmpty;
    
    public ActivityStack(final List<? extends Activity> activitiesInProcess, final boolean isEmpty) {
        Intrinsics.checkNotNullParameter((Object)activitiesInProcess, "activitiesInProcess");
        this.activitiesInProcess = (List<Activity>)activitiesInProcess;
        this.isEmpty = isEmpty;
    }
    
    public final boolean contains(final Activity activity) {
        Intrinsics.checkNotNullParameter((Object)activity, "activity");
        return this.activitiesInProcess.contains(activity);
    }
    
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ActivityStack)) {
            return false;
        }
        final List<Activity> activitiesInProcess = this.activitiesInProcess;
        final ActivityStack activityStack = (ActivityStack)o;
        return Intrinsics.areEqual((Object)activitiesInProcess, (Object)activityStack.activitiesInProcess) && this.isEmpty == activityStack.isEmpty;
    }
    
    public final List<Activity> getActivitiesInProcess$window_release() {
        return this.activitiesInProcess;
    }
    
    @Override
    public int hashCode() {
        return this.activitiesInProcess.hashCode() * 31 + ActivityRule$$ExternalSyntheticBackport0.m(this.isEmpty);
    }
    
    public final boolean isEmpty() {
        return this.isEmpty;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("ActivityStack{activitiesInProcess=");
        sb.append(this.activitiesInProcess);
        sb.append(", isEmpty=");
        sb.append(this.isEmpty);
        sb.append('}');
        return sb.toString();
    }
}

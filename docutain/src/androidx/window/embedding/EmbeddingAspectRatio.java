// 
// Decompiled by Procyon v0.6.0
// 

package androidx.window.embedding;

import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.JvmStatic;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0007\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\u0018\u0000 \u00112\u00020\u0001:\u0001\u0011B\u0017\b\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\u0013\u0010\u000b\u001a\u00020\f2\b\u0010\r\u001a\u0004\u0018\u00010\u0001H\u0096\u0002J\b\u0010\u000e\u001a\u00020\u000fH\u0016J\b\u0010\u0010\u001a\u00020\u0003H\u0016R\u0014\u0010\u0002\u001a\u00020\u0003X\u0080\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0014\u0010\u0004\u001a\u00020\u0005X\u0080\u0004¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\n¨\u0006\u0012" }, d2 = { "Landroidx/window/embedding/EmbeddingAspectRatio;", "", "description", "", "value", "", "(Ljava/lang/String;F)V", "getDescription$window_release", "()Ljava/lang/String;", "getValue$window_release", "()F", "equals", "", "other", "hashCode", "", "toString", "Companion", "window_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
public final class EmbeddingAspectRatio
{
    public static final EmbeddingAspectRatio ALWAYS_ALLOW;
    public static final EmbeddingAspectRatio ALWAYS_DISALLOW;
    public static final Companion Companion;
    private final String description;
    private final float value;
    
    static {
        Companion = new Companion(null);
        ALWAYS_ALLOW = new EmbeddingAspectRatio("ALWAYS_ALLOW", 0.0f);
        ALWAYS_DISALLOW = new EmbeddingAspectRatio("ALWAYS_DISALLOW", -1.0f);
    }
    
    private EmbeddingAspectRatio(final String description, final float value) {
        this.description = description;
        this.value = value;
    }
    
    @JvmStatic
    public static final EmbeddingAspectRatio ratio(final float n) {
        return EmbeddingAspectRatio.Companion.ratio(n);
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o == this) {
            return true;
        }
        if (!(o instanceof EmbeddingAspectRatio)) {
            return false;
        }
        final float value = this.value;
        final EmbeddingAspectRatio embeddingAspectRatio = (EmbeddingAspectRatio)o;
        if (value != embeddingAspectRatio.value || !Intrinsics.areEqual((Object)this.description, (Object)embeddingAspectRatio.description)) {
            b = false;
        }
        return b;
    }
    
    public final String getDescription$window_release() {
        return this.description;
    }
    
    public final float getValue$window_release() {
        return this.value;
    }
    
    @Override
    public int hashCode() {
        return this.description.hashCode() + Float.floatToIntBits(this.value) * 31;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("EmbeddingAspectRatio(");
        sb.append(this.description);
        sb.append(')');
        return sb.toString();
    }
    
    @Metadata(d1 = { "\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0007\n\u0002\b\u0003\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0015\u0010\u0006\u001a\u00020\u00042\u0006\u0010\u0007\u001a\u00020\bH\u0000¢\u0006\u0002\b\tJ\u0012\u0010\n\u001a\u00020\u00042\b\b\u0001\u0010\n\u001a\u00020\bH\u0007R\u0010\u0010\u0003\u001a\u00020\u00048\u0006X\u0087\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\u0005\u001a\u00020\u00048\u0006X\u0087\u0004¢\u0006\u0002\n\u0000¨\u0006\u000b" }, d2 = { "Landroidx/window/embedding/EmbeddingAspectRatio$Companion;", "", "()V", "ALWAYS_ALLOW", "Landroidx/window/embedding/EmbeddingAspectRatio;", "ALWAYS_DISALLOW", "buildAspectRatioFromValue", "value", "", "buildAspectRatioFromValue$window_release", "ratio", "window_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
    public static final class Companion
    {
        private Companion() {
        }
        
        public final EmbeddingAspectRatio buildAspectRatioFromValue$window_release(final float n) {
            final float value$window_release = EmbeddingAspectRatio.ALWAYS_ALLOW.getValue$window_release();
            final int n2 = 1;
            EmbeddingAspectRatio embeddingAspectRatio;
            if (n == value$window_release) {
                embeddingAspectRatio = EmbeddingAspectRatio.ALWAYS_ALLOW;
            }
            else {
                int n3;
                if (n == EmbeddingAspectRatio.ALWAYS_DISALLOW.getValue$window_release()) {
                    n3 = n2;
                }
                else {
                    n3 = 0;
                }
                if (n3 != 0) {
                    embeddingAspectRatio = EmbeddingAspectRatio.ALWAYS_DISALLOW;
                }
                else {
                    embeddingAspectRatio = this.ratio(n);
                }
            }
            return embeddingAspectRatio;
        }
        
        @JvmStatic
        public final EmbeddingAspectRatio ratio(final float f) {
            if (f > 1.0f) {
                final StringBuilder sb = new StringBuilder();
                sb.append("ratio:");
                sb.append(f);
                return new EmbeddingAspectRatio(sb.toString(), f, null);
            }
            throw new IllegalArgumentException("Ratio must be greater than 1.".toString());
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.window.embedding;

import kotlin.jvm.internal.Intrinsics;
import androidx.window.layout.WindowMetrics;
import androidx.window.layout.WindowLayoutInfo;
import android.content.res.Configuration;
import kotlin.Metadata;

@Metadata(d1 = { "\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u000e\b\u0007\u0018\u00002\u00020\u0001B9\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\b\u0010\f\u001a\u0004\u0018\u00010\r¢\u0006\u0002\u0010\u000eJ\b\u0010\u001a\u001a\u00020\rH\u0016R\u0013\u0010\n\u001a\u00020\u000b8\u0007¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000fR\u0011\u0010\b\u001a\u00020\t¢\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\u0011R\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0012\u0010\u0013R\u0011\u0010\u0006\u001a\u00020\u0007¢\u0006\b\n\u0000\u001a\u0004\b\u0014\u0010\u0015R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0016\u0010\u0017R\u0013\u0010\f\u001a\u0004\u0018\u00010\r¢\u0006\b\n\u0000\u001a\u0004\b\u0018\u0010\u0019¨\u0006\u001b" }, d2 = { "Landroidx/window/embedding/SplitAttributesCalculatorParams;", "", "parentWindowMetrics", "Landroidx/window/layout/WindowMetrics;", "parentConfiguration", "Landroid/content/res/Configuration;", "parentWindowLayoutInfo", "Landroidx/window/layout/WindowLayoutInfo;", "defaultSplitAttributes", "Landroidx/window/embedding/SplitAttributes;", "areDefaultConstraintsSatisfied", "", "splitRuleTag", "", "(Landroidx/window/layout/WindowMetrics;Landroid/content/res/Configuration;Landroidx/window/layout/WindowLayoutInfo;Landroidx/window/embedding/SplitAttributes;ZLjava/lang/String;)V", "()Z", "getDefaultSplitAttributes", "()Landroidx/window/embedding/SplitAttributes;", "getParentConfiguration", "()Landroid/content/res/Configuration;", "getParentWindowLayoutInfo", "()Landroidx/window/layout/WindowLayoutInfo;", "getParentWindowMetrics", "()Landroidx/window/layout/WindowMetrics;", "getSplitRuleTag", "()Ljava/lang/String;", "toString", "window_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
public final class SplitAttributesCalculatorParams
{
    private final boolean areDefaultConstraintsSatisfied;
    private final SplitAttributes defaultSplitAttributes;
    private final Configuration parentConfiguration;
    private final WindowLayoutInfo parentWindowLayoutInfo;
    private final WindowMetrics parentWindowMetrics;
    private final String splitRuleTag;
    
    public SplitAttributesCalculatorParams(final WindowMetrics parentWindowMetrics, final Configuration parentConfiguration, final WindowLayoutInfo parentWindowLayoutInfo, final SplitAttributes defaultSplitAttributes, final boolean areDefaultConstraintsSatisfied, final String splitRuleTag) {
        Intrinsics.checkNotNullParameter((Object)parentWindowMetrics, "parentWindowMetrics");
        Intrinsics.checkNotNullParameter((Object)parentConfiguration, "parentConfiguration");
        Intrinsics.checkNotNullParameter((Object)parentWindowLayoutInfo, "parentWindowLayoutInfo");
        Intrinsics.checkNotNullParameter((Object)defaultSplitAttributes, "defaultSplitAttributes");
        this.parentWindowMetrics = parentWindowMetrics;
        this.parentConfiguration = parentConfiguration;
        this.parentWindowLayoutInfo = parentWindowLayoutInfo;
        this.defaultSplitAttributes = defaultSplitAttributes;
        this.areDefaultConstraintsSatisfied = areDefaultConstraintsSatisfied;
        this.splitRuleTag = splitRuleTag;
    }
    
    public final boolean areDefaultConstraintsSatisfied() {
        return this.areDefaultConstraintsSatisfied;
    }
    
    public final SplitAttributes getDefaultSplitAttributes() {
        return this.defaultSplitAttributes;
    }
    
    public final Configuration getParentConfiguration() {
        return this.parentConfiguration;
    }
    
    public final WindowLayoutInfo getParentWindowLayoutInfo() {
        return this.parentWindowLayoutInfo;
    }
    
    public final WindowMetrics getParentWindowMetrics() {
        return this.parentWindowMetrics;
    }
    
    public final String getSplitRuleTag() {
        return this.splitRuleTag;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("SplitAttributesCalculatorParams");
        sb.append(":{windowMetrics=");
        sb.append(this.parentWindowMetrics);
        sb.append(", configuration=");
        sb.append(this.parentConfiguration);
        sb.append(", windowLayoutInfo=");
        sb.append(this.parentWindowLayoutInfo);
        sb.append(", defaultSplitAttributes=");
        sb.append(this.defaultSplitAttributes);
        sb.append(", areDefaultConstraintsSatisfied=");
        sb.append(this.areDefaultConstraintsSatisfied);
        sb.append(", tag=");
        sb.append(this.splitRuleTag);
        sb.append('}');
        return sb.toString();
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.window.embedding;

import android.content.Intent;
import android.app.Activity;
import kotlin.jvm.internal.Intrinsics;
import android.content.ComponentName;
import androidx.window.core.ActivityComponentInfo;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000@\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001B!\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\b\u0010\u0005\u001a\u0004\u0018\u00010\u0006¢\u0006\u0002\u0010\u0007B!\b\u0000\u0012\u0006\u0010\b\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\t\u0012\b\u0010\u0005\u001a\u0004\u0018\u00010\u0006¢\u0006\u0002\u0010\u000bJ\u0013\u0010\u0011\u001a\u00020\u00122\b\u0010\u0013\u001a\u0004\u0018\u00010\u0001H\u0096\u0002J\b\u0010\u0014\u001a\u00020\u0015H\u0016J\u0016\u0010\u0016\u001a\u00020\u00122\u0006\u0010\u0017\u001a\u00020\u00182\u0006\u0010\u0019\u001a\u00020\u001aJ\u0016\u0010\u001b\u001a\u00020\u00122\u0006\u0010\u0017\u001a\u00020\u00182\u0006\u0010\u001c\u001a\u00020\u0018J\b\u0010\u001d\u001a\u00020\u0006H\u0016R\u000e\u0010\b\u001a\u00020\tX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\tX\u0082\u0004¢\u0006\u0002\n\u0000R\u0011\u0010\u0002\u001a\u00020\u00038F¢\u0006\u0006\u001a\u0004\b\f\u0010\rR\u0013\u0010\u0005\u001a\u0004\u0018\u00010\u0006¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\u000fR\u0011\u0010\u0004\u001a\u00020\u00038F¢\u0006\u0006\u001a\u0004\b\u0010\u0010\r¨\u0006\u001e" }, d2 = { "Landroidx/window/embedding/SplitPairFilter;", "", "primaryActivityName", "Landroid/content/ComponentName;", "secondaryActivityName", "secondaryActivityIntentAction", "", "(Landroid/content/ComponentName;Landroid/content/ComponentName;Ljava/lang/String;)V", "_primaryActivityName", "Landroidx/window/core/ActivityComponentInfo;", "_secondaryActivityName", "(Landroidx/window/core/ActivityComponentInfo;Landroidx/window/core/ActivityComponentInfo;Ljava/lang/String;)V", "getPrimaryActivityName", "()Landroid/content/ComponentName;", "getSecondaryActivityIntentAction", "()Ljava/lang/String;", "getSecondaryActivityName", "equals", "", "other", "hashCode", "", "matchesActivityIntentPair", "primaryActivity", "Landroid/app/Activity;", "secondaryActivityIntent", "Landroid/content/Intent;", "matchesActivityPair", "secondaryActivity", "toString", "window_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
public final class SplitPairFilter
{
    private final ActivityComponentInfo _primaryActivityName;
    private final ActivityComponentInfo _secondaryActivityName;
    private final String secondaryActivityIntentAction;
    
    public SplitPairFilter(final ComponentName componentName, final ComponentName componentName2, final String s) {
        Intrinsics.checkNotNullParameter((Object)componentName, "primaryActivityName");
        Intrinsics.checkNotNullParameter((Object)componentName2, "secondaryActivityName");
        this(new ActivityComponentInfo(componentName), new ActivityComponentInfo(componentName2), s);
    }
    
    public SplitPairFilter(final ActivityComponentInfo primaryActivityName, final ActivityComponentInfo secondaryActivityName, final String secondaryActivityIntentAction) {
        Intrinsics.checkNotNullParameter((Object)primaryActivityName, "_primaryActivityName");
        Intrinsics.checkNotNullParameter((Object)secondaryActivityName, "_secondaryActivityName");
        this._primaryActivityName = primaryActivityName;
        this._secondaryActivityName = secondaryActivityName;
        this.secondaryActivityIntentAction = secondaryActivityIntentAction;
        MatcherUtils.INSTANCE.validateComponentName$window_release(primaryActivityName.getPackageName(), primaryActivityName.getClassName());
        MatcherUtils.INSTANCE.validateComponentName$window_release(secondaryActivityName.getPackageName(), secondaryActivityName.getClassName());
    }
    
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        final Class<? extends SplitPairFilter> class1 = this.getClass();
        Class<?> class2;
        if (o != null) {
            class2 = o.getClass();
        }
        else {
            class2 = null;
        }
        if (!Intrinsics.areEqual((Object)class1, (Object)class2)) {
            return false;
        }
        Intrinsics.checkNotNull(o, "null cannot be cast to non-null type androidx.window.embedding.SplitPairFilter");
        final SplitPairFilter splitPairFilter = (SplitPairFilter)o;
        return Intrinsics.areEqual((Object)this._primaryActivityName, (Object)splitPairFilter._primaryActivityName) && Intrinsics.areEqual((Object)this._secondaryActivityName, (Object)splitPairFilter._secondaryActivityName) && Intrinsics.areEqual((Object)this.secondaryActivityIntentAction, (Object)splitPairFilter.secondaryActivityIntentAction);
    }
    
    public final ComponentName getPrimaryActivityName() {
        return new ComponentName(this._primaryActivityName.getPackageName(), this._primaryActivityName.getClassName());
    }
    
    public final String getSecondaryActivityIntentAction() {
        return this.secondaryActivityIntentAction;
    }
    
    public final ComponentName getSecondaryActivityName() {
        return new ComponentName(this._secondaryActivityName.getPackageName(), this._secondaryActivityName.getClassName());
    }
    
    @Override
    public int hashCode() {
        final int hashCode = this._primaryActivityName.hashCode();
        final int hashCode2 = this._secondaryActivityName.hashCode();
        final String secondaryActivityIntentAction = this.secondaryActivityIntentAction;
        int hashCode3;
        if (secondaryActivityIntentAction != null) {
            hashCode3 = secondaryActivityIntentAction.hashCode();
        }
        else {
            hashCode3 = 0;
        }
        return (hashCode * 31 + hashCode2) * 31 + hashCode3;
    }
    
    public final boolean matchesActivityIntentPair(final Activity activity, final Intent intent) {
        Intrinsics.checkNotNullParameter((Object)activity, "primaryActivity");
        Intrinsics.checkNotNullParameter((Object)intent, "secondaryActivityIntent");
        final boolean activityMatching$window_release = MatcherUtils.INSTANCE.isActivityMatching$window_release(activity, this._primaryActivityName);
        boolean b = false;
        if (activityMatching$window_release) {
            if (MatcherUtils.INSTANCE.isIntentMatching$window_release(intent, this._secondaryActivityName)) {
                final String secondaryActivityIntentAction = this.secondaryActivityIntentAction;
                if (secondaryActivityIntentAction == null || Intrinsics.areEqual((Object)secondaryActivityIntentAction, (Object)intent.getAction())) {
                    b = true;
                }
            }
        }
        return b;
    }
    
    public final boolean matchesActivityPair(final Activity activity, final Activity activity2) {
        Intrinsics.checkNotNullParameter((Object)activity, "primaryActivity");
        Intrinsics.checkNotNullParameter((Object)activity2, "secondaryActivity");
        final boolean activityMatching$window_release = MatcherUtils.INSTANCE.isActivityMatching$window_release(activity, this._primaryActivityName);
        boolean b = false;
        if (activityMatching$window_release) {
            if (MatcherUtils.INSTANCE.isActivityMatching$window_release(activity2, this._secondaryActivityName)) {
                final String secondaryActivityIntentAction = this.secondaryActivityIntentAction;
                if (secondaryActivityIntentAction != null) {
                    final Intent intent = activity2.getIntent();
                    String action;
                    if (intent != null) {
                        action = intent.getAction();
                    }
                    else {
                        action = null;
                    }
                    if (!Intrinsics.areEqual((Object)secondaryActivityIntentAction, (Object)action)) {
                        return b;
                    }
                }
                b = true;
            }
        }
        return b;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("SplitPairFilter{primaryActivityName=");
        sb.append(this.getPrimaryActivityName());
        sb.append(", secondaryActivityName=");
        sb.append(this.getSecondaryActivityName());
        sb.append(", secondaryActivityAction=");
        sb.append(this.secondaryActivityIntentAction);
        sb.append('}');
        return sb.toString();
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.window.embedding;

import kotlin.jvm.functions.Function1;
import androidx.window.core.Logger;
import androidx.window.core.VerificationMode;
import androidx.window.core.SpecificationComputer;
import kotlin.jvm.JvmStatic;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0005\u0018\u0000 \u00132\u00020\u0001:\u0004\u0012\u0013\u0014\u0015B\u001b\b\u0007\u0012\b\b\u0002\u0010\u0002\u001a\u00020\u0003\u0012\b\b\u0002\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\u0013\u0010\u000b\u001a\u00020\f2\b\u0010\r\u001a\u0004\u0018\u00010\u0001H\u0096\u0002J\b\u0010\u000e\u001a\u00020\u000fH\u0016J\b\u0010\u0010\u001a\u00020\u0011H\u0016R\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\n¨\u0006\u0016" }, d2 = { "Landroidx/window/embedding/SplitAttributes;", "", "splitType", "Landroidx/window/embedding/SplitAttributes$SplitType;", "layoutDirection", "Landroidx/window/embedding/SplitAttributes$LayoutDirection;", "(Landroidx/window/embedding/SplitAttributes$SplitType;Landroidx/window/embedding/SplitAttributes$LayoutDirection;)V", "getLayoutDirection", "()Landroidx/window/embedding/SplitAttributes$LayoutDirection;", "getSplitType", "()Landroidx/window/embedding/SplitAttributes$SplitType;", "equals", "", "other", "hashCode", "", "toString", "", "Builder", "Companion", "LayoutDirection", "SplitType", "window_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
public final class SplitAttributes
{
    public static final Companion Companion;
    private static final String TAG;
    private final LayoutDirection layoutDirection;
    private final SplitType splitType;
    
    static {
        Companion = new Companion(null);
        TAG = "SplitAttributes";
    }
    
    public SplitAttributes() {
        this(null, null, 3, null);
    }
    
    public SplitAttributes(final SplitType splitType, final LayoutDirection layoutDirection) {
        Intrinsics.checkNotNullParameter((Object)splitType, "splitType");
        Intrinsics.checkNotNullParameter((Object)layoutDirection, "layoutDirection");
        this.splitType = splitType;
        this.layoutDirection = layoutDirection;
    }
    
    public static final /* synthetic */ String access$getTAG$cp() {
        return SplitAttributes.TAG;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (this == o) {
            return true;
        }
        if (!(o instanceof SplitAttributes)) {
            return false;
        }
        final SplitType splitType = this.splitType;
        final SplitAttributes splitAttributes = (SplitAttributes)o;
        if (!Intrinsics.areEqual((Object)splitType, (Object)splitAttributes.splitType) || !Intrinsics.areEqual((Object)this.layoutDirection, (Object)splitAttributes.layoutDirection)) {
            b = false;
        }
        return b;
    }
    
    public final LayoutDirection getLayoutDirection() {
        return this.layoutDirection;
    }
    
    public final SplitType getSplitType() {
        return this.splitType;
    }
    
    @Override
    public int hashCode() {
        return this.splitType.hashCode() * 31 + this.layoutDirection.hashCode();
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("SplitAttributes");
        sb.append(":{splitType=");
        sb.append(this.splitType);
        sb.append(", layoutDir=");
        sb.append(this.layoutDirection);
        sb.append(" }");
        return sb.toString();
    }
    
    @Metadata(d1 = { "\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\u0006\u0010\u0007\u001a\u00020\bJ\u000e\u0010\t\u001a\u00020\u00002\u0006\u0010\u0003\u001a\u00020\u0004J\u000e\u0010\n\u001a\u00020\u00002\u0006\u0010\u000b\u001a\u00020\u0006R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u000e¢\u0006\u0002\n\u0000¨\u0006\f" }, d2 = { "Landroidx/window/embedding/SplitAttributes$Builder;", "", "()V", "layoutDirection", "Landroidx/window/embedding/SplitAttributes$LayoutDirection;", "splitType", "Landroidx/window/embedding/SplitAttributes$SplitType;", "build", "Landroidx/window/embedding/SplitAttributes;", "setLayoutDirection", "setSplitType", "type", "window_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
    public static final class Builder
    {
        private LayoutDirection layoutDirection;
        private SplitType splitType;
        
        public Builder() {
            this.splitType = SplitType.SPLIT_TYPE_EQUAL;
            this.layoutDirection = LayoutDirection.LOCALE;
        }
        
        public final SplitAttributes build() {
            return new SplitAttributes(this.splitType, this.layoutDirection);
        }
        
        public final Builder setLayoutDirection(final LayoutDirection layoutDirection) {
            Intrinsics.checkNotNullParameter((Object)layoutDirection, "layoutDirection");
            final Builder builder = this;
            this.layoutDirection = layoutDirection;
            return this;
        }
        
        public final Builder setSplitType(final SplitType splitType) {
            Intrinsics.checkNotNullParameter((Object)splitType, "type");
            final Builder builder = this;
            this.splitType = splitType;
            return this;
        }
    }
    
    @Metadata(d1 = { "\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u0016\u0010\u0003\u001a\n \u0005*\u0004\u0018\u00010\u00040\u0004X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u0006" }, d2 = { "Landroidx/window/embedding/SplitAttributes$Companion;", "", "()V", "TAG", "", "kotlin.jvm.PlatformType", "window_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
    public static final class Companion
    {
        private Companion() {
        }
    }
    
    @Metadata(d1 = { "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0002\b\u0006\u0018\u0000 \n2\u00020\u0001:\u0001\nB\u0017\b\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\b\u0010\t\u001a\u00020\u0003H\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004¢\u0006\u0002\n\u0000R\u0014\u0010\u0004\u001a\u00020\u0005X\u0080\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\b¨\u0006\u000b" }, d2 = { "Landroidx/window/embedding/SplitAttributes$LayoutDirection;", "", "description", "", "value", "", "(Ljava/lang/String;I)V", "getValue$window_release", "()I", "toString", "Companion", "window_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
    public static final class LayoutDirection
    {
        public static final LayoutDirection BOTTOM_TO_TOP;
        public static final Companion Companion;
        public static final LayoutDirection LEFT_TO_RIGHT;
        public static final LayoutDirection LOCALE;
        public static final LayoutDirection RIGHT_TO_LEFT;
        public static final LayoutDirection TOP_TO_BOTTOM;
        private final String description;
        private final int value;
        
        static {
            Companion = new Companion(null);
            LOCALE = new LayoutDirection("LOCALE", 0);
            LEFT_TO_RIGHT = new LayoutDirection("LEFT_TO_RIGHT", 1);
            RIGHT_TO_LEFT = new LayoutDirection("RIGHT_TO_LEFT", 2);
            TOP_TO_BOTTOM = new LayoutDirection("TOP_TO_BOTTOM", 3);
            BOTTOM_TO_TOP = new LayoutDirection("BOTTOM_TO_TOP", 4);
        }
        
        private LayoutDirection(final String description, final int value) {
            this.description = description;
            this.value = value;
        }
        
        @JvmStatic
        public static final LayoutDirection getLayoutDirectionFromValue$window_release(final int n) {
            return LayoutDirection.Companion.getLayoutDirectionFromValue$window_release(n);
        }
        
        public final int getValue$window_release() {
            return this.value;
        }
        
        @Override
        public String toString() {
            return this.description;
        }
        
        @Metadata(d1 = { "\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\b\n\u0002\b\u0002\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0017\u0010\t\u001a\u00020\u00042\b\b\u0001\u0010\n\u001a\u00020\u000bH\u0001¢\u0006\u0002\b\fR\u0010\u0010\u0003\u001a\u00020\u00048\u0006X\u0087\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\u0005\u001a\u00020\u00048\u0006X\u0087\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\u0006\u001a\u00020\u00048\u0006X\u0087\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\u0007\u001a\u00020\u00048\u0006X\u0087\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\b\u001a\u00020\u00048\u0006X\u0087\u0004¢\u0006\u0002\n\u0000¨\u0006\r" }, d2 = { "Landroidx/window/embedding/SplitAttributes$LayoutDirection$Companion;", "", "()V", "BOTTOM_TO_TOP", "Landroidx/window/embedding/SplitAttributes$LayoutDirection;", "LEFT_TO_RIGHT", "LOCALE", "RIGHT_TO_LEFT", "TOP_TO_BOTTOM", "getLayoutDirectionFromValue", "value", "", "getLayoutDirectionFromValue$window_release", "window_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
        public static final class Companion
        {
            private Companion() {
            }
            
            @JvmStatic
            public final LayoutDirection getLayoutDirectionFromValue$window_release(final int i) {
                LayoutDirection layoutDirection;
                if (i == LayoutDirection.LEFT_TO_RIGHT.getValue$window_release()) {
                    layoutDirection = LayoutDirection.LEFT_TO_RIGHT;
                }
                else if (i == LayoutDirection.RIGHT_TO_LEFT.getValue$window_release()) {
                    layoutDirection = LayoutDirection.RIGHT_TO_LEFT;
                }
                else if (i == LayoutDirection.LOCALE.getValue$window_release()) {
                    layoutDirection = LayoutDirection.LOCALE;
                }
                else if (i == LayoutDirection.TOP_TO_BOTTOM.getValue$window_release()) {
                    layoutDirection = LayoutDirection.TOP_TO_BOTTOM;
                }
                else {
                    if (i != LayoutDirection.BOTTOM_TO_TOP.getValue$window_release()) {
                        final StringBuilder sb = new StringBuilder();
                        sb.append("Undefined value:");
                        sb.append(i);
                        throw new IllegalArgumentException(sb.toString());
                    }
                    layoutDirection = LayoutDirection.BOTTOM_TO_TOP;
                }
                return layoutDirection;
            }
        }
    }
    
    @Metadata(d1 = { "\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0007\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\u0018\u0000 \u00112\u00020\u0001:\u0001\u0011B\u0017\b\u0000\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\u0013\u0010\u000b\u001a\u00020\f2\b\u0010\r\u001a\u0004\u0018\u00010\u0001H\u0096\u0002J\b\u0010\u000e\u001a\u00020\u000fH\u0016J\b\u0010\u0010\u001a\u00020\u0003H\u0016R\u0014\u0010\u0002\u001a\u00020\u0003X\u0080\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0014\u0010\u0004\u001a\u00020\u0005X\u0080\u0004¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\n¨\u0006\u0012" }, d2 = { "Landroidx/window/embedding/SplitAttributes$SplitType;", "", "description", "", "value", "", "(Ljava/lang/String;F)V", "getDescription$window_release", "()Ljava/lang/String;", "getValue$window_release", "()F", "equals", "", "other", "hashCode", "", "toString", "Companion", "window_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
    public static final class SplitType
    {
        public static final Companion Companion;
        public static final SplitType SPLIT_TYPE_EQUAL;
        public static final SplitType SPLIT_TYPE_EXPAND;
        public static final SplitType SPLIT_TYPE_HINGE;
        private final String description;
        private final float value;
        
        static {
            final Companion companion = Companion = new Companion(null);
            SPLIT_TYPE_EXPAND = new SplitType("expandContainers", 0.0f);
            SPLIT_TYPE_EQUAL = companion.ratio(0.5f);
            SPLIT_TYPE_HINGE = new SplitType("hinge", -1.0f);
        }
        
        public SplitType(final String description, final float value) {
            Intrinsics.checkNotNullParameter((Object)description, "description");
            this.description = description;
            this.value = value;
        }
        
        @JvmStatic
        public static final SplitType ratio(final float n) {
            return SplitType.Companion.ratio(n);
        }
        
        @Override
        public boolean equals(final Object o) {
            boolean b = true;
            if (o == this) {
                return true;
            }
            if (!(o instanceof SplitType)) {
                return false;
            }
            final float value = this.value;
            final SplitType splitType = (SplitType)o;
            if (value != splitType.value || !Intrinsics.areEqual((Object)this.description, (Object)splitType.description)) {
                b = false;
            }
            return b;
        }
        
        public final String getDescription$window_release() {
            return this.description;
        }
        
        public final float getValue$window_release() {
            return this.value;
        }
        
        @Override
        public int hashCode() {
            return this.description.hashCode() + Float.floatToIntBits(this.value) * 31;
        }
        
        @Override
        public String toString() {
            return this.description;
        }
        
        @Metadata(d1 = { "\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u0007\n\u0002\b\u0003\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0017\u0010\u0007\u001a\u00020\u00042\b\b\u0001\u0010\b\u001a\u00020\tH\u0001¢\u0006\u0002\b\nJ\u0012\u0010\u000b\u001a\u00020\u00042\b\b\u0001\u0010\u000b\u001a\u00020\tH\u0007R\u0010\u0010\u0003\u001a\u00020\u00048\u0006X\u0087\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\u0005\u001a\u00020\u00048\u0006X\u0087\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\u0006\u001a\u00020\u00048\u0006X\u0087\u0004¢\u0006\u0002\n\u0000¨\u0006\f" }, d2 = { "Landroidx/window/embedding/SplitAttributes$SplitType$Companion;", "", "()V", "SPLIT_TYPE_EQUAL", "Landroidx/window/embedding/SplitAttributes$SplitType;", "SPLIT_TYPE_EXPAND", "SPLIT_TYPE_HINGE", "buildSplitTypeFromValue", "value", "", "buildSplitTypeFromValue$window_release", "ratio", "window_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
        public static final class Companion
        {
            private Companion() {
            }
            
            public final SplitType buildSplitTypeFromValue$window_release(final float n) {
                SplitType splitType;
                if (n == SplitType.SPLIT_TYPE_EXPAND.getValue$window_release()) {
                    splitType = SplitType.SPLIT_TYPE_EXPAND;
                }
                else {
                    splitType = this.ratio(n);
                }
                return splitType;
            }
            
            @JvmStatic
            public final SplitType ratio(float floatValue) {
                final SpecificationComputer.Companion companion = SpecificationComputer.Companion;
                final String access$getTAG$cp = SplitAttributes.access$getTAG$cp();
                Intrinsics.checkNotNullExpressionValue((Object)access$getTAG$cp, "TAG");
                final Number compute = SpecificationComputer.Companion.startSpecification$default(companion, floatValue, access$getTAG$cp, VerificationMode.STRICT, null, 4, null).require("Ratio must be in range (0.0, 1.0). Use SplitType.expandContainers() instead of 0 or 1.", (kotlin.jvm.functions.Function1<? super Number, Boolean>)new SplitAttributes$SplitType$Companion$ratio$checkedRatio.SplitAttributes$SplitType$Companion$ratio$checkedRatio$1(floatValue)).compute();
                Intrinsics.checkNotNull((Object)compute);
                floatValue = compute.floatValue();
                final StringBuilder sb = new StringBuilder();
                sb.append("ratio:");
                sb.append(floatValue);
                return new SplitType(sb.toString(), floatValue);
            }
        }
    }
}

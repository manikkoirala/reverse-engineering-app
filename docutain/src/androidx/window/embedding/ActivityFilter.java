// 
// Decompiled by Procyon v0.6.0
// 

package androidx.window.embedding;

import android.content.Intent;
import android.app.Activity;
import kotlin.jvm.internal.Intrinsics;
import android.content.ComponentName;
import androidx.window.core.ActivityComponentInfo;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000@\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\u0019\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\b\u0010\u0004\u001a\u0004\u0018\u00010\u0005¢\u0006\u0002\u0010\u0006B\u0019\b\u0000\u0012\u0006\u0010\u0007\u001a\u00020\b\u0012\b\u0010\u0004\u001a\u0004\u0018\u00010\u0005¢\u0006\u0002\u0010\tJ\u0013\u0010\u0010\u001a\u00020\u00112\b\u0010\u0012\u001a\u0004\u0018\u00010\u0001H\u0096\u0002J\b\u0010\u0013\u001a\u00020\u0014H\u0016J\u000e\u0010\u0015\u001a\u00020\u00112\u0006\u0010\u0016\u001a\u00020\u0017J\u000e\u0010\u0018\u001a\u00020\u00112\u0006\u0010\u0019\u001a\u00020\u001aJ\b\u0010\u001b\u001a\u00020\u0005H\u0016R\u0014\u0010\u0007\u001a\u00020\bX\u0080\u0004¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000bR\u0011\u0010\u0002\u001a\u00020\u00038F¢\u0006\u0006\u001a\u0004\b\f\u0010\rR\u0013\u0010\u0004\u001a\u0004\u0018\u00010\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\u000f¨\u0006\u001c" }, d2 = { "Landroidx/window/embedding/ActivityFilter;", "", "componentName", "Landroid/content/ComponentName;", "intentAction", "", "(Landroid/content/ComponentName;Ljava/lang/String;)V", "activityComponentInfo", "Landroidx/window/core/ActivityComponentInfo;", "(Landroidx/window/core/ActivityComponentInfo;Ljava/lang/String;)V", "getActivityComponentInfo$window_release", "()Landroidx/window/core/ActivityComponentInfo;", "getComponentName", "()Landroid/content/ComponentName;", "getIntentAction", "()Ljava/lang/String;", "equals", "", "other", "hashCode", "", "matchesActivity", "activity", "Landroid/app/Activity;", "matchesIntent", "intent", "Landroid/content/Intent;", "toString", "window_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
public final class ActivityFilter
{
    private final ActivityComponentInfo activityComponentInfo;
    private final String intentAction;
    
    public ActivityFilter(final ComponentName componentName, final String s) {
        Intrinsics.checkNotNullParameter((Object)componentName, "componentName");
        this(new ActivityComponentInfo(componentName), s);
    }
    
    public ActivityFilter(final ActivityComponentInfo activityComponentInfo, final String intentAction) {
        Intrinsics.checkNotNullParameter((Object)activityComponentInfo, "activityComponentInfo");
        this.activityComponentInfo = activityComponentInfo;
        this.intentAction = intentAction;
        MatcherUtils.INSTANCE.validateComponentName$window_release(activityComponentInfo.getPackageName(), activityComponentInfo.getClassName());
    }
    
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ActivityFilter)) {
            return false;
        }
        final ActivityComponentInfo activityComponentInfo = this.activityComponentInfo;
        final ActivityFilter activityFilter = (ActivityFilter)o;
        return Intrinsics.areEqual((Object)activityComponentInfo, (Object)activityFilter.activityComponentInfo) && Intrinsics.areEqual((Object)this.intentAction, (Object)activityFilter.intentAction);
    }
    
    public final ActivityComponentInfo getActivityComponentInfo$window_release() {
        return this.activityComponentInfo;
    }
    
    public final ComponentName getComponentName() {
        return new ComponentName(this.activityComponentInfo.getPackageName(), this.activityComponentInfo.getClassName());
    }
    
    public final String getIntentAction() {
        return this.intentAction;
    }
    
    @Override
    public int hashCode() {
        final int hashCode = this.activityComponentInfo.hashCode();
        final String intentAction = this.intentAction;
        int hashCode2;
        if (intentAction != null) {
            hashCode2 = intentAction.hashCode();
        }
        else {
            hashCode2 = 0;
        }
        return hashCode * 31 + hashCode2;
    }
    
    public final boolean matchesActivity(final Activity activity) {
        Intrinsics.checkNotNullParameter((Object)activity, "activity");
        if (MatcherUtils.INSTANCE.isActivityMatching$window_release(activity, this.activityComponentInfo)) {
            final String intentAction = this.intentAction;
            if (intentAction != null) {
                final Intent intent = activity.getIntent();
                String action;
                if (intent != null) {
                    action = intent.getAction();
                }
                else {
                    action = null;
                }
                if (!Intrinsics.areEqual((Object)intentAction, (Object)action)) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }
    
    public final boolean matchesIntent(final Intent intent) {
        Intrinsics.checkNotNullParameter((Object)intent, "intent");
        final boolean intentMatching$window_release = MatcherUtils.INSTANCE.isIntentMatching$window_release(intent, this.activityComponentInfo);
        boolean b = false;
        if (intentMatching$window_release) {
            final String intentAction = this.intentAction;
            if (intentAction == null || Intrinsics.areEqual((Object)intentAction, (Object)intent.getAction())) {
                b = true;
            }
        }
        return b;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("ActivityFilter(componentName=");
        sb.append(this.activityComponentInfo);
        sb.append(", intentAction=");
        sb.append(this.intentAction);
        sb.append(')');
        return sb.toString();
    }
}

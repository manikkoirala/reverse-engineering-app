// 
// Decompiled by Procyon v0.6.0
// 

package androidx.window.embedding;

import kotlin.collections.CollectionsKt;
import java.util.Collection;
import java.util.LinkedHashSet;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import java.util.Set;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000F\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\"\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u000b\n\u0002\u0010\u0000\n\u0002\b\u0007\u0018\u00002\u00020\u0001:\u0001%By\b\u0000\u0012\f\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\n\b\u0002\u0010\u0007\u001a\u0004\u0018\u00010\b\u0012\b\b\u0002\u0010\t\u001a\u00020\n\u0012\b\b\u0002\u0010\u000b\u001a\u00020\n\u0012\b\b\u0002\u0010\f\u001a\u00020\r\u0012\b\b\u0003\u0010\u000e\u001a\u00020\u000f\u0012\b\b\u0003\u0010\u0010\u001a\u00020\u000f\u0012\b\b\u0003\u0010\u0011\u001a\u00020\u000f\u0012\b\b\u0002\u0010\u0012\u001a\u00020\u0013\u0012\b\b\u0002\u0010\u0014\u001a\u00020\u0013¢\u0006\u0002\u0010\u0015J\u0013\u0010\u001d\u001a\u00020\r2\b\u0010\u001e\u001a\u0004\u0018\u00010\u001fH\u0096\u0002J\b\u0010 \u001a\u00020\u000fH\u0016J\u0016\u0010!\u001a\u00020\u00002\u0006\u0010\"\u001a\u00020\u0004H\u0080\u0002¢\u0006\u0002\b#J\b\u0010$\u001a\u00020\bH\u0016R\u0011\u0010\f\u001a\u00020\r¢\u0006\b\n\u0000\u001a\u0004\b\u0016\u0010\u0017R\u0017\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0018\u0010\u0019R\u0011\u0010\t\u001a\u00020\n¢\u0006\b\n\u0000\u001a\u0004\b\u001a\u0010\u001bR\u0011\u0010\u000b\u001a\u00020\n¢\u0006\b\n\u0000\u001a\u0004\b\u001c\u0010\u001b¨\u0006&" }, d2 = { "Landroidx/window/embedding/SplitPairRule;", "Landroidx/window/embedding/SplitRule;", "filters", "", "Landroidx/window/embedding/SplitPairFilter;", "defaultSplitAttributes", "Landroidx/window/embedding/SplitAttributes;", "tag", "", "finishPrimaryWithSecondary", "Landroidx/window/embedding/SplitRule$FinishBehavior;", "finishSecondaryWithPrimary", "clearTop", "", "minWidthDp", "", "minHeightDp", "minSmallestWidthDp", "maxAspectRatioInPortrait", "Landroidx/window/embedding/EmbeddingAspectRatio;", "maxAspectRatioInLandscape", "(Ljava/util/Set;Landroidx/window/embedding/SplitAttributes;Ljava/lang/String;Landroidx/window/embedding/SplitRule$FinishBehavior;Landroidx/window/embedding/SplitRule$FinishBehavior;ZIIILandroidx/window/embedding/EmbeddingAspectRatio;Landroidx/window/embedding/EmbeddingAspectRatio;)V", "getClearTop", "()Z", "getFilters", "()Ljava/util/Set;", "getFinishPrimaryWithSecondary", "()Landroidx/window/embedding/SplitRule$FinishBehavior;", "getFinishSecondaryWithPrimary", "equals", "other", "", "hashCode", "plus", "filter", "plus$window_release", "toString", "Builder", "window_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
public final class SplitPairRule extends SplitRule
{
    private final boolean clearTop;
    private final Set<SplitPairFilter> filters;
    private final FinishBehavior finishPrimaryWithSecondary;
    private final FinishBehavior finishSecondaryWithPrimary;
    
    public SplitPairRule(final Set<SplitPairFilter> filters, final SplitAttributes splitAttributes, final String s, final FinishBehavior finishPrimaryWithSecondary, final FinishBehavior finishSecondaryWithPrimary, final boolean clearTop, final int n, final int n2, final int n3, final EmbeddingAspectRatio embeddingAspectRatio, final EmbeddingAspectRatio embeddingAspectRatio2) {
        Intrinsics.checkNotNullParameter((Object)filters, "filters");
        Intrinsics.checkNotNullParameter((Object)splitAttributes, "defaultSplitAttributes");
        Intrinsics.checkNotNullParameter((Object)finishPrimaryWithSecondary, "finishPrimaryWithSecondary");
        Intrinsics.checkNotNullParameter((Object)finishSecondaryWithPrimary, "finishSecondaryWithPrimary");
        Intrinsics.checkNotNullParameter((Object)embeddingAspectRatio, "maxAspectRatioInPortrait");
        Intrinsics.checkNotNullParameter((Object)embeddingAspectRatio2, "maxAspectRatioInLandscape");
        super(s, n, n2, n3, embeddingAspectRatio, embeddingAspectRatio2, splitAttributes);
        this.filters = filters;
        this.finishPrimaryWithSecondary = finishPrimaryWithSecondary;
        this.finishSecondaryWithPrimary = finishSecondaryWithPrimary;
        this.clearTop = clearTop;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof SplitPairRule)) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        final Set<SplitPairFilter> filters = this.filters;
        final SplitPairRule splitPairRule = (SplitPairRule)o;
        return Intrinsics.areEqual((Object)filters, (Object)splitPairRule.filters) && Intrinsics.areEqual((Object)this.finishPrimaryWithSecondary, (Object)splitPairRule.finishPrimaryWithSecondary) && Intrinsics.areEqual((Object)this.finishSecondaryWithPrimary, (Object)splitPairRule.finishSecondaryWithPrimary) && this.clearTop == splitPairRule.clearTop;
    }
    
    public final boolean getClearTop() {
        return this.clearTop;
    }
    
    public final Set<SplitPairFilter> getFilters() {
        return this.filters;
    }
    
    public final FinishBehavior getFinishPrimaryWithSecondary() {
        return this.finishPrimaryWithSecondary;
    }
    
    public final FinishBehavior getFinishSecondaryWithPrimary() {
        return this.finishSecondaryWithPrimary;
    }
    
    @Override
    public int hashCode() {
        return (((super.hashCode() * 31 + this.filters.hashCode()) * 31 + this.finishPrimaryWithSecondary.hashCode()) * 31 + this.finishSecondaryWithPrimary.hashCode()) * 31 + ActivityRule$$ExternalSyntheticBackport0.m(this.clearTop);
    }
    
    public final SplitPairRule plus$window_release(final SplitPairFilter splitPairFilter) {
        Intrinsics.checkNotNullParameter((Object)splitPairFilter, "filter");
        final Set set = new LinkedHashSet();
        set.addAll(this.filters);
        set.add(splitPairFilter);
        return new Builder(CollectionsKt.toSet((Iterable)set)).setTag(this.getTag()).setMinWidthDp(this.getMinWidthDp()).setMinHeightDp(this.getMinHeightDp()).setMinSmallestWidthDp(this.getMinSmallestWidthDp()).setMaxAspectRatioInPortrait(this.getMaxAspectRatioInPortrait()).setMaxAspectRatioInLandscape(this.getMaxAspectRatioInLandscape()).setFinishPrimaryWithSecondary(this.finishPrimaryWithSecondary).setFinishSecondaryWithPrimary(this.finishSecondaryWithPrimary).setClearTop(this.clearTop).setDefaultSplitAttributes(this.getDefaultSplitAttributes()).build();
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("SplitPairRule");
        sb.append("{tag=");
        sb.append(this.getTag());
        sb.append(", defaultSplitAttributes=");
        sb.append(this.getDefaultSplitAttributes());
        sb.append(", minWidthDp=");
        sb.append(this.getMinWidthDp());
        sb.append(", minHeightDp=");
        sb.append(this.getMinHeightDp());
        sb.append(", minSmallestWidthDp=");
        sb.append(this.getMinSmallestWidthDp());
        sb.append(", maxAspectRatioInPortrait=");
        sb.append(this.getMaxAspectRatioInPortrait());
        sb.append(", maxAspectRatioInLandscape=");
        sb.append(this.getMaxAspectRatioInLandscape());
        sb.append(", clearTop=");
        sb.append(this.clearTop);
        sb.append(", finishPrimaryWithSecondary=");
        sb.append(this.finishPrimaryWithSecondary);
        sb.append(", finishSecondaryWithPrimary=");
        sb.append(this.finishSecondaryWithPrimary);
        sb.append(", filters=");
        sb.append(this.filters);
        sb.append('}');
        return sb.toString();
    }
    
    @Metadata(d1 = { "\u0000H\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\"\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\f\u0018\u00002\u00020\u0001B\u0013\u0012\f\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003¢\u0006\u0002\u0010\u0005J\u0006\u0010\u0016\u001a\u00020\u0017J\u000e\u0010\u0018\u001a\u00020\u00002\u0006\u0010\u0006\u001a\u00020\u0007J\u000e\u0010\u0019\u001a\u00020\u00002\u0006\u0010\b\u001a\u00020\tJ\u000e\u0010\u001a\u001a\u00020\u00002\u0006\u0010\n\u001a\u00020\u000bJ\u000e\u0010\u001b\u001a\u00020\u00002\u0006\u0010\f\u001a\u00020\u000bJ\u000e\u0010\u001c\u001a\u00020\u00002\u0006\u0010\u001d\u001a\u00020\u000eJ\u000e\u0010\u001e\u001a\u00020\u00002\u0006\u0010\u001d\u001a\u00020\u000eJ\u0010\u0010\u001f\u001a\u00020\u00002\b\b\u0001\u0010\u0010\u001a\u00020\u0011J\u0010\u0010 \u001a\u00020\u00002\b\b\u0001\u0010\u0012\u001a\u00020\u0011J\u0010\u0010!\u001a\u00020\u00002\b\b\u0001\u0010\u0013\u001a\u00020\u0011J\u0010\u0010\"\u001a\u00020\u00002\b\u0010\u0014\u001a\u0004\u0018\u00010\u0015R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u0082\u000e¢\u0006\u0002\n\u0000R\u0014\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\u000bX\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u000eX\u0082\u000e¢\u0006\u0002\n\u0000R\u0012\u0010\u0010\u001a\u00020\u00118\u0002@\u0002X\u0083\u000e¢\u0006\u0002\n\u0000R\u0012\u0010\u0012\u001a\u00020\u00118\u0002@\u0002X\u0083\u000e¢\u0006\u0002\n\u0000R\u0012\u0010\u0013\u001a\u00020\u00118\u0002@\u0002X\u0083\u000e¢\u0006\u0002\n\u0000R\u0010\u0010\u0014\u001a\u0004\u0018\u00010\u0015X\u0082\u000e¢\u0006\u0002\n\u0000¨\u0006#" }, d2 = { "Landroidx/window/embedding/SplitPairRule$Builder;", "", "filters", "", "Landroidx/window/embedding/SplitPairFilter;", "(Ljava/util/Set;)V", "clearTop", "", "defaultSplitAttributes", "Landroidx/window/embedding/SplitAttributes;", "finishPrimaryWithSecondary", "Landroidx/window/embedding/SplitRule$FinishBehavior;", "finishSecondaryWithPrimary", "maxAspectRatioInLandscape", "Landroidx/window/embedding/EmbeddingAspectRatio;", "maxAspectRatioInPortrait", "minHeightDp", "", "minSmallestWidthDp", "minWidthDp", "tag", "", "build", "Landroidx/window/embedding/SplitPairRule;", "setClearTop", "setDefaultSplitAttributes", "setFinishPrimaryWithSecondary", "setFinishSecondaryWithPrimary", "setMaxAspectRatioInLandscape", "aspectRatio", "setMaxAspectRatioInPortrait", "setMinHeightDp", "setMinSmallestWidthDp", "setMinWidthDp", "setTag", "window_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
    public static final class Builder
    {
        private boolean clearTop;
        private SplitAttributes defaultSplitAttributes;
        private final Set<SplitPairFilter> filters;
        private FinishBehavior finishPrimaryWithSecondary;
        private FinishBehavior finishSecondaryWithPrimary;
        private EmbeddingAspectRatio maxAspectRatioInLandscape;
        private EmbeddingAspectRatio maxAspectRatioInPortrait;
        private int minHeightDp;
        private int minSmallestWidthDp;
        private int minWidthDp;
        private String tag;
        
        public Builder(final Set<SplitPairFilter> filters) {
            Intrinsics.checkNotNullParameter((Object)filters, "filters");
            this.filters = filters;
            this.minWidthDp = 600;
            this.minHeightDp = 600;
            this.minSmallestWidthDp = 600;
            this.maxAspectRatioInPortrait = SplitRule.SPLIT_MAX_ASPECT_RATIO_PORTRAIT_DEFAULT;
            this.maxAspectRatioInLandscape = SplitRule.SPLIT_MAX_ASPECT_RATIO_LANDSCAPE_DEFAULT;
            this.finishPrimaryWithSecondary = FinishBehavior.NEVER;
            this.finishSecondaryWithPrimary = FinishBehavior.ALWAYS;
            this.defaultSplitAttributes = new SplitAttributes.Builder().build();
        }
        
        public final SplitPairRule build() {
            return new SplitPairRule(this.filters, this.defaultSplitAttributes, this.tag, this.finishPrimaryWithSecondary, this.finishSecondaryWithPrimary, this.clearTop, this.minWidthDp, this.minHeightDp, this.minSmallestWidthDp, this.maxAspectRatioInPortrait, this.maxAspectRatioInLandscape);
        }
        
        public final Builder setClearTop(final boolean clearTop) {
            final Builder builder = this;
            this.clearTop = clearTop;
            return this;
        }
        
        public final Builder setDefaultSplitAttributes(final SplitAttributes defaultSplitAttributes) {
            Intrinsics.checkNotNullParameter((Object)defaultSplitAttributes, "defaultSplitAttributes");
            final Builder builder = this;
            this.defaultSplitAttributes = defaultSplitAttributes;
            return this;
        }
        
        public final Builder setFinishPrimaryWithSecondary(final FinishBehavior finishPrimaryWithSecondary) {
            Intrinsics.checkNotNullParameter((Object)finishPrimaryWithSecondary, "finishPrimaryWithSecondary");
            final Builder builder = this;
            this.finishPrimaryWithSecondary = finishPrimaryWithSecondary;
            return this;
        }
        
        public final Builder setFinishSecondaryWithPrimary(final FinishBehavior finishSecondaryWithPrimary) {
            Intrinsics.checkNotNullParameter((Object)finishSecondaryWithPrimary, "finishSecondaryWithPrimary");
            final Builder builder = this;
            this.finishSecondaryWithPrimary = finishSecondaryWithPrimary;
            return this;
        }
        
        public final Builder setMaxAspectRatioInLandscape(final EmbeddingAspectRatio maxAspectRatioInLandscape) {
            Intrinsics.checkNotNullParameter((Object)maxAspectRatioInLandscape, "aspectRatio");
            final Builder builder = this;
            this.maxAspectRatioInLandscape = maxAspectRatioInLandscape;
            return this;
        }
        
        public final Builder setMaxAspectRatioInPortrait(final EmbeddingAspectRatio maxAspectRatioInPortrait) {
            Intrinsics.checkNotNullParameter((Object)maxAspectRatioInPortrait, "aspectRatio");
            final Builder builder = this;
            this.maxAspectRatioInPortrait = maxAspectRatioInPortrait;
            return this;
        }
        
        public final Builder setMinHeightDp(final int minHeightDp) {
            final Builder builder = this;
            this.minHeightDp = minHeightDp;
            return this;
        }
        
        public final Builder setMinSmallestWidthDp(final int minSmallestWidthDp) {
            final Builder builder = this;
            this.minSmallestWidthDp = minSmallestWidthDp;
            return this;
        }
        
        public final Builder setMinWidthDp(final int minWidthDp) {
            final Builder builder = this;
            this.minWidthDp = minWidthDp;
            return this;
        }
        
        public final Builder setTag(final String tag) {
            final Builder builder = this;
            this.tag = tag;
            return this;
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.window.embedding;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;
import java.util.HashMap;
import androidx.collection.ArraySet;
import androidx.window.core.ConsumerAdapter;
import androidx.window.core.PredicateAdapter;
import androidx.window.core.ExtensionsUtil;
import android.content.pm.PackageManager$Property;
import android.content.pm.PackageManager$NameNotFoundException;
import androidx.window.core.VerificationMode;
import androidx.window.core.BuildConfig;
import kotlin.jvm.functions.Function1;
import java.util.Iterator;
import kotlin.collections.CollectionsKt;
import android.util.Log;
import java.util.List;
import androidx.core.util.Consumer;
import java.util.concurrent.Executor;
import android.app.Activity;
import kotlin.Unit;
import java.util.Set;
import java.util.concurrent.locks.Lock;
import kotlin.LazyKt;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.Lazy;
import java.util.concurrent.CopyOnWriteArrayList;
import android.content.Context;
import java.util.concurrent.locks.ReentrantLock;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000\u0080\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\"\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\b\u0000\u0018\u0000 ;2\u00020\u0001:\u0005:;<=>B\u0019\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\b\u0010\u0004\u001a\u0004\u0018\u00010\u0005¢\u0006\u0002\u0010\u0006J\u0010\u0010\u001c\u001a\u00020\u001d2\u0006\u0010\u001e\u001a\u00020\u001fH\u0017J,\u0010 \u001a\u00020\u001d2\u0006\u0010!\u001a\u00020\"2\u0006\u0010#\u001a\u00020$2\u0012\u0010%\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020(0'0&H\u0016J\b\u0010)\u001a\u00020*H\u0002J\b\u0010+\u001a\u00020\u001dH\u0016J\u000e\u0010,\u001a\b\u0012\u0004\u0012\u00020\u001f0-H\u0017J\u0010\u0010.\u001a\u00020*2\u0006\u0010!\u001a\u00020\"H\u0016J\b\u0010/\u001a\u00020*H\u0016J\u0010\u00100\u001a\u00020\u001d2\u0006\u0010\u001e\u001a\u00020\u001fH\u0017J\u001c\u00101\u001a\u00020\u001d2\u0012\u00102\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020(0'0&H\u0016J\u0016\u00103\u001a\u00020\u001d2\f\u00104\u001a\b\u0012\u0004\u0012\u00020\u001f0-H\u0017J\u001c\u00105\u001a\u00020\u001d2\u0012\u00106\u001a\u000e\u0012\u0004\u0012\u000208\u0012\u0004\u0012\u00020907H\u0017R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004¢\u0006\u0002\n\u0000R \u0010\u0004\u001a\u0004\u0018\u00010\u00058\u0006@\u0006X\u0087\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0007\u0010\b\"\u0004\b\t\u0010\nR\u0010\u0010\u000b\u001a\u00020\f8\u0002X\u0083\u0004¢\u0006\u0002\n\u0000R\"\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u000f0\u000e8\u0006X\u0087\u0004¢\u0006\u000e\n\u0000\u0012\u0004\b\u0010\u0010\u0011\u001a\u0004\b\u0012\u0010\u0013R\u0012\u0010\u0014\u001a\u00060\u0015R\u00020\u0000X\u0082\u0004¢\u0006\u0002\n\u0000R\u001b\u0010\u0016\u001a\u00020\u00178VX\u0096\u0084\u0002¢\u0006\f\n\u0004\b\u001a\u0010\u001b\u001a\u0004\b\u0018\u0010\u0019¨\u0006?" }, d2 = { "Landroidx/window/embedding/ExtensionEmbeddingBackend;", "Landroidx/window/embedding/EmbeddingBackend;", "applicationContext", "Landroid/content/Context;", "embeddingExtension", "Landroidx/window/embedding/EmbeddingInterfaceCompat;", "(Landroid/content/Context;Landroidx/window/embedding/EmbeddingInterfaceCompat;)V", "getEmbeddingExtension", "()Landroidx/window/embedding/EmbeddingInterfaceCompat;", "setEmbeddingExtension", "(Landroidx/window/embedding/EmbeddingInterfaceCompat;)V", "ruleTracker", "Landroidx/window/embedding/ExtensionEmbeddingBackend$RuleTracker;", "splitChangeCallbacks", "Ljava/util/concurrent/CopyOnWriteArrayList;", "Landroidx/window/embedding/ExtensionEmbeddingBackend$SplitListenerWrapper;", "getSplitChangeCallbacks$annotations", "()V", "getSplitChangeCallbacks", "()Ljava/util/concurrent/CopyOnWriteArrayList;", "splitInfoEmbeddingCallback", "Landroidx/window/embedding/ExtensionEmbeddingBackend$EmbeddingCallbackImpl;", "splitSupportStatus", "Landroidx/window/embedding/SplitController$SplitSupportStatus;", "getSplitSupportStatus", "()Landroidx/window/embedding/SplitController$SplitSupportStatus;", "splitSupportStatus$delegate", "Lkotlin/Lazy;", "addRule", "", "rule", "Landroidx/window/embedding/EmbeddingRule;", "addSplitListenerForActivity", "activity", "Landroid/app/Activity;", "executor", "Ljava/util/concurrent/Executor;", "callback", "Landroidx/core/util/Consumer;", "", "Landroidx/window/embedding/SplitInfo;", "areExtensionsAvailable", "", "clearSplitAttributesCalculator", "getRules", "", "isActivityEmbedded", "isSplitAttributesCalculatorSupported", "removeRule", "removeSplitListenerForActivity", "consumer", "setRules", "rules", "setSplitAttributesCalculator", "calculator", "Lkotlin/Function1;", "Landroidx/window/embedding/SplitAttributesCalculatorParams;", "Landroidx/window/embedding/SplitAttributes;", "Api31Impl", "Companion", "EmbeddingCallbackImpl", "RuleTracker", "SplitListenerWrapper", "window_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
public final class ExtensionEmbeddingBackend implements EmbeddingBackend
{
    public static final Companion Companion;
    private static final String TAG = "EmbeddingBackend";
    private static volatile ExtensionEmbeddingBackend globalInstance;
    private static final ReentrantLock globalLock;
    private final Context applicationContext;
    private EmbeddingInterfaceCompat embeddingExtension;
    private final RuleTracker ruleTracker;
    private final CopyOnWriteArrayList<SplitListenerWrapper> splitChangeCallbacks;
    private final EmbeddingCallbackImpl splitInfoEmbeddingCallback;
    private final Lazy splitSupportStatus$delegate;
    
    static {
        Companion = new Companion(null);
        globalLock = new ReentrantLock();
    }
    
    public ExtensionEmbeddingBackend(final Context applicationContext, final EmbeddingInterfaceCompat embeddingExtension) {
        Intrinsics.checkNotNullParameter((Object)applicationContext, "applicationContext");
        this.applicationContext = applicationContext;
        this.embeddingExtension = embeddingExtension;
        final EmbeddingCallbackImpl splitInfoEmbeddingCallback = new EmbeddingCallbackImpl();
        this.splitInfoEmbeddingCallback = splitInfoEmbeddingCallback;
        this.splitChangeCallbacks = new CopyOnWriteArrayList<SplitListenerWrapper>();
        final EmbeddingInterfaceCompat embeddingExtension2 = this.embeddingExtension;
        if (embeddingExtension2 != null) {
            embeddingExtension2.setEmbeddingCallback((EmbeddingInterfaceCompat.EmbeddingCallbackInterface)splitInfoEmbeddingCallback);
        }
        this.ruleTracker = new RuleTracker();
        this.splitSupportStatus$delegate = LazyKt.lazy((Function0)new ExtensionEmbeddingBackend$splitSupportStatus.ExtensionEmbeddingBackend$splitSupportStatus$2(this));
    }
    
    public static final /* synthetic */ ExtensionEmbeddingBackend access$getGlobalInstance$cp() {
        return ExtensionEmbeddingBackend.globalInstance;
    }
    
    public static final /* synthetic */ ReentrantLock access$getGlobalLock$cp() {
        return ExtensionEmbeddingBackend.globalLock;
    }
    
    public static final /* synthetic */ void access$setGlobalInstance$cp(final ExtensionEmbeddingBackend globalInstance) {
        ExtensionEmbeddingBackend.globalInstance = globalInstance;
    }
    
    private final boolean areExtensionsAvailable() {
        return this.embeddingExtension != null;
    }
    
    @Override
    public void addRule(final EmbeddingRule embeddingRule) {
        Intrinsics.checkNotNullParameter((Object)embeddingRule, "rule");
        final Lock lock = ExtensionEmbeddingBackend.globalLock;
        lock.lock();
        try {
            if (!this.ruleTracker.contains(embeddingRule)) {
                RuleTracker.addOrUpdateRule$default(this.ruleTracker, embeddingRule, false, 2, null);
                final EmbeddingInterfaceCompat embeddingExtension = this.embeddingExtension;
                if (embeddingExtension != null) {
                    embeddingExtension.setRules(this.getRules());
                }
            }
            final Unit instance = Unit.INSTANCE;
        }
        finally {
            lock.unlock();
        }
    }
    
    @Override
    public void addSplitListenerForActivity(final Activity activity, final Executor executor, final Consumer<List<SplitInfo>> consumer) {
        Intrinsics.checkNotNullParameter((Object)activity, "activity");
        Intrinsics.checkNotNullParameter((Object)executor, "executor");
        Intrinsics.checkNotNullParameter((Object)consumer, "callback");
        final Lock lock = ExtensionEmbeddingBackend.globalLock;
        lock.lock();
        try {
            if (this.embeddingExtension == null) {
                Log.v("EmbeddingBackend", "Extension not loaded, skipping callback registration.");
                consumer.accept(CollectionsKt.emptyList());
                return;
            }
            final SplitListenerWrapper e = new SplitListenerWrapper(activity, executor, consumer);
            this.splitChangeCallbacks.add(e);
            if (this.splitInfoEmbeddingCallback.getLastInfo() != null) {
                final List<SplitInfo> lastInfo = this.splitInfoEmbeddingCallback.getLastInfo();
                Intrinsics.checkNotNull((Object)lastInfo);
                e.accept(lastInfo);
            }
            else {
                e.accept(CollectionsKt.emptyList());
            }
            final Unit instance = Unit.INSTANCE;
        }
        finally {
            lock.unlock();
        }
    }
    
    @Override
    public void clearSplitAttributesCalculator() {
        final Lock lock = ExtensionEmbeddingBackend.globalLock;
        lock.lock();
        try {
            final EmbeddingInterfaceCompat embeddingExtension = this.embeddingExtension;
            if (embeddingExtension != null) {
                embeddingExtension.clearSplitAttributesCalculator();
                final Unit instance = Unit.INSTANCE;
            }
        }
        finally {
            lock.unlock();
        }
    }
    
    public final EmbeddingInterfaceCompat getEmbeddingExtension() {
        return this.embeddingExtension;
    }
    
    @Override
    public Set<EmbeddingRule> getRules() {
        final Lock lock = ExtensionEmbeddingBackend.globalLock;
        lock.lock();
        try {
            return this.ruleTracker.getSplitRules();
        }
        finally {
            lock.unlock();
        }
    }
    
    public final CopyOnWriteArrayList<SplitListenerWrapper> getSplitChangeCallbacks() {
        return this.splitChangeCallbacks;
    }
    
    @Override
    public SplitController.SplitSupportStatus getSplitSupportStatus() {
        return (SplitController.SplitSupportStatus)this.splitSupportStatus$delegate.getValue();
    }
    
    @Override
    public boolean isActivityEmbedded(final Activity activity) {
        Intrinsics.checkNotNullParameter((Object)activity, "activity");
        final EmbeddingInterfaceCompat embeddingExtension = this.embeddingExtension;
        return embeddingExtension != null && embeddingExtension.isActivityEmbedded(activity);
    }
    
    @Override
    public boolean isSplitAttributesCalculatorSupported() {
        final EmbeddingInterfaceCompat embeddingExtension = this.embeddingExtension;
        return embeddingExtension != null && embeddingExtension.isSplitAttributesCalculatorSupported();
    }
    
    @Override
    public void removeRule(final EmbeddingRule embeddingRule) {
        Intrinsics.checkNotNullParameter((Object)embeddingRule, "rule");
        final Lock lock = ExtensionEmbeddingBackend.globalLock;
        lock.lock();
        try {
            if (this.ruleTracker.contains(embeddingRule)) {
                this.ruleTracker.removeRule(embeddingRule);
                final EmbeddingInterfaceCompat embeddingExtension = this.embeddingExtension;
                if (embeddingExtension != null) {
                    embeddingExtension.setRules(this.getRules());
                }
            }
            final Unit instance = Unit.INSTANCE;
        }
        finally {
            lock.unlock();
        }
    }
    
    @Override
    public void removeSplitListenerForActivity(final Consumer<List<SplitInfo>> consumer) {
        Intrinsics.checkNotNullParameter((Object)consumer, "consumer");
        final Lock lock = ExtensionEmbeddingBackend.globalLock;
        lock.lock();
        try {
            for (final SplitListenerWrapper o : this.splitChangeCallbacks) {
                if (Intrinsics.areEqual((Object)o.getCallback(), (Object)consumer)) {
                    this.splitChangeCallbacks.remove(o);
                    break;
                }
            }
            final Unit instance = Unit.INSTANCE;
        }
        finally {
            lock.unlock();
        }
    }
    
    public final void setEmbeddingExtension(final EmbeddingInterfaceCompat embeddingExtension) {
        this.embeddingExtension = embeddingExtension;
    }
    
    @Override
    public void setRules(final Set<? extends EmbeddingRule> rules) {
        Intrinsics.checkNotNullParameter((Object)rules, "rules");
        final Lock lock = ExtensionEmbeddingBackend.globalLock;
        lock.lock();
        try {
            this.ruleTracker.setRules(rules);
            final EmbeddingInterfaceCompat embeddingExtension = this.embeddingExtension;
            if (embeddingExtension != null) {
                embeddingExtension.setRules(this.getRules());
                final Unit instance = Unit.INSTANCE;
            }
        }
        finally {
            lock.unlock();
        }
    }
    
    @Override
    public void setSplitAttributesCalculator(final Function1<? super SplitAttributesCalculatorParams, SplitAttributes> splitAttributesCalculator) {
        Intrinsics.checkNotNullParameter((Object)splitAttributesCalculator, "calculator");
        final Lock lock = ExtensionEmbeddingBackend.globalLock;
        lock.lock();
        try {
            final EmbeddingInterfaceCompat embeddingExtension = this.embeddingExtension;
            if (embeddingExtension != null) {
                embeddingExtension.setSplitAttributesCalculator(splitAttributesCalculator);
                final Unit instance = Unit.INSTANCE;
            }
        }
        finally {
            lock.unlock();
        }
    }
    
    @Metadata(d1 = { "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u00c3\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0007¨\u0006\u0007" }, d2 = { "Landroidx/window/embedding/ExtensionEmbeddingBackend$Api31Impl;", "", "()V", "isSplitPropertyEnabled", "Landroidx/window/embedding/SplitController$SplitSupportStatus;", "context", "Landroid/content/Context;", "window_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
    private static final class Api31Impl
    {
        public static final Api31Impl INSTANCE;
        
        static {
            INSTANCE = new Api31Impl();
        }
        
        public final SplitController.SplitSupportStatus isSplitPropertyEnabled(final Context context) {
            Intrinsics.checkNotNullParameter((Object)context, "context");
            try {
                final PackageManager$Property property = context.getPackageManager().getProperty("android.window.PROPERTY_ACTIVITY_EMBEDDING_SPLITS_ENABLED", context.getPackageName());
                Intrinsics.checkNotNullExpressionValue((Object)property, "try {\n                co\u2026OT_DECLARED\n            }");
                if (!property.isBoolean()) {
                    if (BuildConfig.INSTANCE.getVerificationMode() == VerificationMode.LOG) {
                        Log.w("EmbeddingBackend", "android.window.PROPERTY_ACTIVITY_EMBEDDING_SPLITS_ENABLED must have a boolean value");
                    }
                    return SplitController.SplitSupportStatus.SPLIT_ERROR_PROPERTY_NOT_DECLARED;
                }
                SplitController.SplitSupportStatus splitSupportStatus;
                if (property.getBoolean()) {
                    splitSupportStatus = SplitController.SplitSupportStatus.SPLIT_AVAILABLE;
                }
                else {
                    splitSupportStatus = SplitController.SplitSupportStatus.SPLIT_UNAVAILABLE;
                }
                return splitSupportStatus;
            }
            catch (final Exception ex) {
                if (BuildConfig.INSTANCE.getVerificationMode() == VerificationMode.LOG) {
                    Log.e("EmbeddingBackend", "PackageManager.getProperty is not supported", (Throwable)ex);
                }
                return SplitController.SplitSupportStatus.SPLIT_ERROR_PROPERTY_NOT_DECLARED;
            }
            catch (final PackageManager$NameNotFoundException ex2) {
                if (BuildConfig.INSTANCE.getVerificationMode() == VerificationMode.LOG) {
                    Log.w("EmbeddingBackend", "android.window.PROPERTY_ACTIVITY_EMBEDDING_SPLITS_ENABLED must be set and enabled in AndroidManifest.xml to use splits APIs.");
                }
                return SplitController.SplitSupportStatus.SPLIT_ERROR_PROPERTY_NOT_DECLARED;
            }
        }
    }
    
    @Metadata(d1 = { "\u0000@\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u000e\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\fJ\u0012\u0010\r\u001a\u0004\u0018\u00010\u000e2\u0006\u0010\u000f\u001a\u00020\fH\u0002J\u0017\u0010\u0010\u001a\u00020\u00112\b\u0010\u0012\u001a\u0004\u0018\u00010\u0013H\u0007¢\u0006\u0002\u0010\u0014R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082T¢\u0006\u0002\n\u0000R\u0010\u0010\u0005\u001a\u0004\u0018\u00010\u0006X\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\bX\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u0015" }, d2 = { "Landroidx/window/embedding/ExtensionEmbeddingBackend$Companion;", "", "()V", "TAG", "", "globalInstance", "Landroidx/window/embedding/ExtensionEmbeddingBackend;", "globalLock", "Ljava/util/concurrent/locks/ReentrantLock;", "getInstance", "Landroidx/window/embedding/EmbeddingBackend;", "context", "Landroid/content/Context;", "initAndVerifyEmbeddingExtension", "Landroidx/window/embedding/EmbeddingInterfaceCompat;", "applicationContext", "isExtensionVersionSupported", "", "extensionVersion", "", "(Ljava/lang/Integer;)Z", "window_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
    public static final class Companion
    {
        private Companion() {
        }
        
        private final EmbeddingInterfaceCompat initAndVerifyEmbeddingExtension(final Context context) {
            final EmbeddingInterfaceCompat embeddingInterfaceCompat = null;
            EmbeddingInterfaceCompat embeddingInterfaceCompat4 = null;
            try {
                if (this.isExtensionVersionSupported(ExtensionsUtil.INSTANCE.getSafeVendorApiLevel()) && EmbeddingCompat.Companion.isEmbeddingAvailable()) {
                    final ClassLoader classLoader = EmbeddingBackend.class.getClassLoader();
                    EmbeddingInterfaceCompat embeddingInterfaceCompat2;
                    if (classLoader != null) {
                        embeddingInterfaceCompat2 = new EmbeddingCompat(EmbeddingCompat.Companion.embeddingComponent(), new EmbeddingAdapter(new PredicateAdapter(classLoader)), new ConsumerAdapter(classLoader), context);
                    }
                    else {
                        embeddingInterfaceCompat2 = null;
                    }
                    final EmbeddingInterfaceCompat embeddingInterfaceCompat3 = embeddingInterfaceCompat2;
                }
            }
            finally {
                final StringBuilder sb = new StringBuilder();
                sb.append("Failed to load embedding extension: ");
                final Throwable obj;
                sb.append(obj);
                Log.d("EmbeddingBackend", sb.toString());
                embeddingInterfaceCompat4 = embeddingInterfaceCompat;
            }
            if (embeddingInterfaceCompat4 == null) {
                Log.d("EmbeddingBackend", "No supported embedding extension found");
            }
            return embeddingInterfaceCompat4;
        }
        
        public final EmbeddingBackend getInstance(Context applicationContext) {
            Intrinsics.checkNotNullParameter((Object)applicationContext, "context");
            if (ExtensionEmbeddingBackend.access$getGlobalInstance$cp() == null) {
                final Lock lock = ExtensionEmbeddingBackend.access$getGlobalLock$cp();
                lock.lock();
                try {
                    if (ExtensionEmbeddingBackend.access$getGlobalInstance$cp() == null) {
                        applicationContext = applicationContext.getApplicationContext();
                        final Companion companion = ExtensionEmbeddingBackend.Companion;
                        Intrinsics.checkNotNullExpressionValue((Object)applicationContext, "applicationContext");
                        final EmbeddingInterfaceCompat initAndVerifyEmbeddingExtension = companion.initAndVerifyEmbeddingExtension(applicationContext);
                        final Companion companion2 = ExtensionEmbeddingBackend.Companion;
                        ExtensionEmbeddingBackend.access$setGlobalInstance$cp(new ExtensionEmbeddingBackend(applicationContext, initAndVerifyEmbeddingExtension));
                    }
                    final Unit instance = Unit.INSTANCE;
                }
                finally {
                    lock.unlock();
                }
            }
            final ExtensionEmbeddingBackend access$getGlobalInstance$cp = ExtensionEmbeddingBackend.access$getGlobalInstance$cp();
            Intrinsics.checkNotNull((Object)access$getGlobalInstance$cp);
            return access$getGlobalInstance$cp;
        }
        
        public final boolean isExtensionVersionSupported(final Integer n) {
            boolean b = false;
            if (n == null) {
                return false;
            }
            if (n >= 1) {
                b = true;
            }
            return b;
        }
    }
    
    @Metadata(d1 = { "\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0002\b\u0002\b\u0080\u0004\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\u0016\u0010\n\u001a\u00020\u000b2\f\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004H\u0016R\"\u0010\u0003\u001a\n\u0012\u0004\u0012\u00020\u0005\u0018\u00010\u0004X\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0006\u0010\u0007\"\u0004\b\b\u0010\t¨\u0006\r" }, d2 = { "Landroidx/window/embedding/ExtensionEmbeddingBackend$EmbeddingCallbackImpl;", "Landroidx/window/embedding/EmbeddingInterfaceCompat$EmbeddingCallbackInterface;", "(Landroidx/window/embedding/ExtensionEmbeddingBackend;)V", "lastInfo", "", "Landroidx/window/embedding/SplitInfo;", "getLastInfo", "()Ljava/util/List;", "setLastInfo", "(Ljava/util/List;)V", "onSplitInfoChanged", "", "splitInfo", "window_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
    public final class EmbeddingCallbackImpl implements EmbeddingCallbackInterface
    {
        private List<SplitInfo> lastInfo;
        final ExtensionEmbeddingBackend this$0;
        
        public EmbeddingCallbackImpl(final ExtensionEmbeddingBackend this$0) {
            this.this$0 = this$0;
        }
        
        public final List<SplitInfo> getLastInfo() {
            return this.lastInfo;
        }
        
        @Override
        public void onSplitInfoChanged(final List<SplitInfo> lastInfo) {
            Intrinsics.checkNotNullParameter((Object)lastInfo, "splitInfo");
            this.lastInfo = lastInfo;
            final Iterator<SplitListenerWrapper> iterator = this.this$0.getSplitChangeCallbacks().iterator();
            while (iterator.hasNext()) {
                ((SplitListenerWrapper)iterator.next()).accept(lastInfo);
            }
        }
        
        public final void setLastInfo(final List<SplitInfo> lastInfo) {
            this.lastInfo = lastInfo;
        }
    }
    
    @Metadata(d1 = { "\u0000<\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0010\"\n\u0000\b\u0002\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\u0018\u0010\f\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u00052\b\b\u0002\u0010\u000f\u001a\u00020\u0010J\u0006\u0010\u0011\u001a\u00020\rJ\u0011\u0010\u0012\u001a\u00020\u00102\u0006\u0010\u000e\u001a\u00020\u0005H\u0086\u0002J\u000e\u0010\u0013\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u0005J\u0014\u0010\u0014\u001a\u00020\r2\f\u0010\u0015\u001a\b\u0012\u0004\u0012\u00020\u00050\u0016R\u0017\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0006\u0010\u0007R*\u0010\b\u001a\u001e\u0012\u0004\u0012\u00020\n\u0012\u0004\u0012\u00020\u00050\tj\u000e\u0012\u0004\u0012\u00020\n\u0012\u0004\u0012\u00020\u0005`\u000bX\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u0017" }, d2 = { "Landroidx/window/embedding/ExtensionEmbeddingBackend$RuleTracker;", "", "()V", "splitRules", "Landroidx/collection/ArraySet;", "Landroidx/window/embedding/EmbeddingRule;", "getSplitRules", "()Landroidx/collection/ArraySet;", "tagRuleMap", "Ljava/util/HashMap;", "", "Lkotlin/collections/HashMap;", "addOrUpdateRule", "", "rule", "throwOnDuplicateTag", "", "clearRules", "contains", "removeRule", "setRules", "rules", "", "window_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
    private static final class RuleTracker
    {
        private final ArraySet<EmbeddingRule> splitRules;
        private final HashMap<String, EmbeddingRule> tagRuleMap;
        
        public RuleTracker() {
            this.splitRules = new ArraySet<EmbeddingRule>();
            this.tagRuleMap = new HashMap<String, EmbeddingRule>();
        }
        
        public static /* synthetic */ void addOrUpdateRule$default(final RuleTracker ruleTracker, final EmbeddingRule embeddingRule, boolean b, final int n, final Object o) {
            if ((n & 0x2) != 0x0) {
                b = false;
            }
            ruleTracker.addOrUpdateRule(embeddingRule, b);
        }
        
        public final void addOrUpdateRule(final EmbeddingRule embeddingRule, final boolean b) {
            Intrinsics.checkNotNullParameter((Object)embeddingRule, "rule");
            if (this.splitRules.contains(embeddingRule)) {
                return;
            }
            final String tag = embeddingRule.getTag();
            if (tag == null) {
                this.splitRules.add(embeddingRule);
            }
            else if (this.tagRuleMap.containsKey(tag)) {
                if (b) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Duplicated tag: ");
                    sb.append(tag);
                    sb.append(". Tag must be unique among all registered rules");
                    throw new IllegalArgumentException(sb.toString());
                }
                this.splitRules.remove(this.tagRuleMap.get(tag));
                this.tagRuleMap.put(tag, embeddingRule);
                this.splitRules.add(embeddingRule);
            }
            else {
                this.tagRuleMap.put(tag, embeddingRule);
                this.splitRules.add(embeddingRule);
            }
        }
        
        public final void clearRules() {
            this.splitRules.clear();
            this.tagRuleMap.clear();
        }
        
        public final boolean contains(final EmbeddingRule embeddingRule) {
            Intrinsics.checkNotNullParameter((Object)embeddingRule, "rule");
            return this.splitRules.contains(embeddingRule);
        }
        
        public final ArraySet<EmbeddingRule> getSplitRules() {
            return this.splitRules;
        }
        
        public final void removeRule(final EmbeddingRule embeddingRule) {
            Intrinsics.checkNotNullParameter((Object)embeddingRule, "rule");
            if (!this.splitRules.contains(embeddingRule)) {
                return;
            }
            this.splitRules.remove(embeddingRule);
            if (embeddingRule.getTag() != null) {
                this.tagRuleMap.remove(embeddingRule.getTag());
            }
        }
        
        public final void setRules(final Set<? extends EmbeddingRule> set) {
            Intrinsics.checkNotNullParameter((Object)set, "rules");
            this.clearRules();
            final Iterator iterator = set.iterator();
            while (iterator.hasNext()) {
                this.addOrUpdateRule((EmbeddingRule)iterator.next(), true);
            }
        }
    }
    
    @Metadata(d1 = { "\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0002\b\u0002\b\u0000\u0018\u00002\u00020\u0001B)\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0012\u0010\u0006\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\t0\b0\u0007¢\u0006\u0002\u0010\nJ\u0014\u0010\u000e\u001a\u00020\u000f2\f\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\t0\bR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004¢\u0006\u0002\n\u0000R\u001d\u0010\u0006\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\t0\b0\u0007¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004¢\u0006\u0002\n\u0000R\u0016\u0010\r\u001a\n\u0012\u0004\u0012\u00020\t\u0018\u00010\bX\u0082\u000e¢\u0006\u0002\n\u0000¨\u0006\u0011" }, d2 = { "Landroidx/window/embedding/ExtensionEmbeddingBackend$SplitListenerWrapper;", "", "activity", "Landroid/app/Activity;", "executor", "Ljava/util/concurrent/Executor;", "callback", "Landroidx/core/util/Consumer;", "", "Landroidx/window/embedding/SplitInfo;", "(Landroid/app/Activity;Ljava/util/concurrent/Executor;Landroidx/core/util/Consumer;)V", "getCallback", "()Landroidx/core/util/Consumer;", "lastValue", "accept", "", "splitInfoList", "window_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
    public static final class SplitListenerWrapper
    {
        private final Activity activity;
        private final Consumer<List<SplitInfo>> callback;
        private final Executor executor;
        private List<SplitInfo> lastValue;
        
        public SplitListenerWrapper(final Activity activity, final Executor executor, final Consumer<List<SplitInfo>> callback) {
            Intrinsics.checkNotNullParameter((Object)activity, "activity");
            Intrinsics.checkNotNullParameter((Object)executor, "executor");
            Intrinsics.checkNotNullParameter((Object)callback, "callback");
            this.activity = activity;
            this.executor = executor;
            this.callback = callback;
        }
        
        private static final void accept$lambda$1(final SplitListenerWrapper splitListenerWrapper, final List list) {
            Intrinsics.checkNotNullParameter((Object)splitListenerWrapper, "this$0");
            Intrinsics.checkNotNullParameter((Object)list, "$splitsWithActivity");
            splitListenerWrapper.callback.accept(list);
        }
        
        public final void accept(final List<SplitInfo> list) {
            Intrinsics.checkNotNullParameter((Object)list, "splitInfoList");
            final Iterable iterable = list;
            final Collection collection = new ArrayList();
            for (final Object next : iterable) {
                if (((SplitInfo)next).contains(this.activity)) {
                    collection.add(next);
                }
            }
            final List lastValue = (List)collection;
            if (Intrinsics.areEqual((Object)lastValue, (Object)this.lastValue)) {
                return;
            }
            this.lastValue = lastValue;
            this.executor.execute(new ExtensionEmbeddingBackend$SplitListenerWrapper$$ExternalSyntheticLambda0(this, lastValue));
        }
        
        public final Consumer<List<SplitInfo>> getCallback() {
            return this.callback;
        }
    }
}

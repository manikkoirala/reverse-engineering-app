// 
// Decompiled by Procyon v0.6.0
// 

package androidx.window.embedding;

import kotlinx.coroutines.flow.FlowKt;
import kotlinx.coroutines.flow.Flow;
import kotlin.jvm.functions.Function1;
import java.util.concurrent.CancellationException;
import kotlinx.coroutines.Job$DefaultImpls;
import kotlin.ReplaceWith;
import kotlin.Deprecated;
import kotlin.Unit;
import kotlinx.coroutines.CoroutineStart;
import kotlinx.coroutines.BuildersKt;
import kotlin.coroutines.Continuation;
import kotlin.jvm.functions.Function2;
import kotlinx.coroutines.CoroutineScopeKt;
import kotlinx.coroutines.ExecutorsKt;
import kotlin.coroutines.CoroutineContext;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.Executor;
import android.app.Activity;
import kotlin.jvm.JvmStatic;
import android.content.Context;
import java.util.LinkedHashMap;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.DefaultConstructorMarker;
import java.util.concurrent.locks.ReentrantLock;
import kotlinx.coroutines.Job;
import java.util.List;
import androidx.core.util.Consumer;
import java.util.Map;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000h\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010%\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u0000 $2\u00020\u0001:\u0002$%B\u000f\b\u0000\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J,\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u00162\u0012\u0010\u0017\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\t0\b0\u0007H\u0007J\b\u0010\u0018\u001a\u00020\u0012H\u0007J\b\u0010\u0019\u001a\u00020\u001aH\u0007J\b\u0010\u001b\u001a\u00020\u001aH\u0007J\u001c\u0010\u001c\u001a\u00020\u00122\u0012\u0010\u0017\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\t0\b0\u0007H\u0007J\u001c\u0010\u001d\u001a\u00020\u00122\u0012\u0010\u001e\u001a\u000e\u0012\u0004\u0012\u00020 \u0012\u0004\u0012\u00020!0\u001fH\u0007J\u001a\u0010\"\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\t0\b0#2\u0006\u0010\u0013\u001a\u00020\u0014R(\u0010\u0005\u001a\u001a\u0012\u0010\u0012\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\t0\b0\u0007\u0012\u0004\u0012\u00020\n0\u00068\u0002X\u0083\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\fX\u0082\u0004¢\u0006\u0002\n\u0000R\u0011\u0010\r\u001a\u00020\u000e8F¢\u0006\u0006\u001a\u0004\b\u000f\u0010\u0010¨\u0006&" }, d2 = { "Landroidx/window/embedding/SplitController;", "", "embeddingBackend", "Landroidx/window/embedding/EmbeddingBackend;", "(Landroidx/window/embedding/EmbeddingBackend;)V", "consumerToJobMap", "", "Landroidx/core/util/Consumer;", "", "Landroidx/window/embedding/SplitInfo;", "Lkotlinx/coroutines/Job;", "lock", "Ljava/util/concurrent/locks/ReentrantLock;", "splitSupportStatus", "Landroidx/window/embedding/SplitController$SplitSupportStatus;", "getSplitSupportStatus", "()Landroidx/window/embedding/SplitController$SplitSupportStatus;", "addSplitListener", "", "activity", "Landroid/app/Activity;", "executor", "Ljava/util/concurrent/Executor;", "consumer", "clearSplitAttributesCalculator", "isSplitAttributesCalculatorSupported", "", "isSplitSupported", "removeSplitListener", "setSplitAttributesCalculator", "calculator", "Lkotlin/Function1;", "Landroidx/window/embedding/SplitAttributesCalculatorParams;", "Landroidx/window/embedding/SplitAttributes;", "splitInfoList", "Lkotlinx/coroutines/flow/Flow;", "Companion", "SplitSupportStatus", "window_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
public final class SplitController
{
    public static final Companion Companion;
    public static final boolean sDebug = false;
    private final Map<Consumer<List<SplitInfo>>, Job> consumerToJobMap;
    private final EmbeddingBackend embeddingBackend;
    private final ReentrantLock lock;
    
    static {
        Companion = new Companion(null);
    }
    
    public SplitController(final EmbeddingBackend embeddingBackend) {
        Intrinsics.checkNotNullParameter((Object)embeddingBackend, "embeddingBackend");
        this.embeddingBackend = embeddingBackend;
        this.lock = new ReentrantLock();
        this.consumerToJobMap = new LinkedHashMap<Consumer<List<SplitInfo>>, Job>();
    }
    
    @JvmStatic
    public static final SplitController getInstance(final Context context) {
        return SplitController.Companion.getInstance(context);
    }
    
    @Deprecated(message = "Replace to provide Flow API to get SplitInfo list", replaceWith = @ReplaceWith(expression = "splitInfoList", imports = { "androidx.window.embedding.SplitController" }))
    public final void addSplitListener(final Activity activity, final Executor executor, final Consumer<List<SplitInfo>> consumer) {
        Intrinsics.checkNotNullParameter((Object)activity, "activity");
        Intrinsics.checkNotNullParameter((Object)executor, "executor");
        Intrinsics.checkNotNullParameter((Object)consumer, "consumer");
        final Lock lock = this.lock;
        lock.lock();
        try {
            if (this.consumerToJobMap.get(consumer) != null) {
                return;
            }
            this.consumerToJobMap.put(consumer, BuildersKt.launch$default(CoroutineScopeKt.CoroutineScope((CoroutineContext)ExecutorsKt.from(executor)), (CoroutineContext)null, (CoroutineStart)null, (Function2)new SplitController$addSplitListener$1.SplitController$addSplitListener$1$1(this, activity, (Consumer)consumer, (Continuation)null), 3, (Object)null));
            final Unit instance = Unit.INSTANCE;
        }
        finally {
            lock.unlock();
        }
    }
    
    public final void clearSplitAttributesCalculator() {
        this.embeddingBackend.clearSplitAttributesCalculator();
    }
    
    public final SplitSupportStatus getSplitSupportStatus() {
        return this.embeddingBackend.getSplitSupportStatus();
    }
    
    public final boolean isSplitAttributesCalculatorSupported() {
        return this.embeddingBackend.isSplitAttributesCalculatorSupported();
    }
    
    @Deprecated(message = "Use splitSupportStatus instead", replaceWith = @ReplaceWith(expression = "splitSupportStatus", imports = {}))
    public final boolean isSplitSupported() {
        return Intrinsics.areEqual((Object)this.getSplitSupportStatus(), (Object)SplitSupportStatus.SPLIT_AVAILABLE);
    }
    
    @Deprecated(message = "Replace to provide Flow API to get SplitInfo list", replaceWith = @ReplaceWith(expression = "splitInfoList", imports = { "androidx.window.embedding.SplitController" }))
    public final void removeSplitListener(final Consumer<List<SplitInfo>> consumer) {
        Intrinsics.checkNotNullParameter((Object)consumer, "consumer");
        final Lock lock = this.lock;
        lock.lock();
        try {
            final Job job = this.consumerToJobMap.get(consumer);
            if (job != null) {
                Job$DefaultImpls.cancel$default(job, (CancellationException)null, 1, (Object)null);
            }
            final Job job2 = this.consumerToJobMap.remove(consumer);
        }
        finally {
            lock.unlock();
        }
    }
    
    public final void setSplitAttributesCalculator(final Function1<? super SplitAttributesCalculatorParams, SplitAttributes> splitAttributesCalculator) {
        Intrinsics.checkNotNullParameter((Object)splitAttributesCalculator, "calculator");
        this.embeddingBackend.setSplitAttributesCalculator(splitAttributesCalculator);
    }
    
    public final Flow<List<SplitInfo>> splitInfoList(final Activity activity) {
        Intrinsics.checkNotNullParameter((Object)activity, "activity");
        return (Flow<List<SplitInfo>>)FlowKt.callbackFlow((Function2)new SplitController$splitInfoList.SplitController$splitInfoList$1(this, activity, (Continuation)null));
    }
    
    @Metadata(d1 = { "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0010\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bH\u0007R\u000e\u0010\u0003\u001a\u00020\u0004X\u0080T¢\u0006\u0002\n\u0000¨\u0006\t" }, d2 = { "Landroidx/window/embedding/SplitController$Companion;", "", "()V", "sDebug", "", "getInstance", "Landroidx/window/embedding/SplitController;", "context", "Landroid/content/Context;", "window_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
    public static final class Companion
    {
        private Companion() {
        }
        
        @JvmStatic
        public final SplitController getInstance(final Context context) {
            Intrinsics.checkNotNullParameter((Object)context, "context");
            return new SplitController(EmbeddingBackend.Companion.getInstance(context));
        }
    }
    
    @Metadata(d1 = { "\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\u0018\u0000 \u00072\u00020\u0001:\u0001\u0007B\u000f\b\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\b\u0010\u0005\u001a\u00020\u0006H\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\b" }, d2 = { "Landroidx/window/embedding/SplitController$SplitSupportStatus;", "", "rawValue", "", "(I)V", "toString", "", "Companion", "window_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
    public static final class SplitSupportStatus
    {
        public static final Companion Companion;
        public static final SplitSupportStatus SPLIT_AVAILABLE;
        public static final SplitSupportStatus SPLIT_ERROR_PROPERTY_NOT_DECLARED;
        public static final SplitSupportStatus SPLIT_UNAVAILABLE;
        private final int rawValue;
        
        static {
            Companion = new Companion(null);
            SPLIT_AVAILABLE = new SplitSupportStatus(0);
            SPLIT_UNAVAILABLE = new SplitSupportStatus(1);
            SPLIT_ERROR_PROPERTY_NOT_DECLARED = new SplitSupportStatus(2);
        }
        
        private SplitSupportStatus(final int rawValue) {
            this.rawValue = rawValue;
        }
        
        @Override
        public String toString() {
            final int rawValue = this.rawValue;
            String s;
            if (rawValue != 0) {
                if (rawValue != 1) {
                    if (rawValue != 2) {
                        s = "UNKNOWN";
                    }
                    else {
                        s = "SplitSupportStatus: ERROR_SPLIT_PROPERTY_NOT_DECLARED";
                    }
                }
                else {
                    s = "SplitSupportStatus: UNAVAILABLE";
                }
            }
            else {
                s = "SplitSupportStatus: AVAILABLE";
            }
            return s;
        }
        
        @Metadata(d1 = { "\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u0010\u0010\u0003\u001a\u00020\u00048\u0006X\u0087\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\u0005\u001a\u00020\u00048\u0006X\u0087\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\u0006\u001a\u00020\u00048\u0006X\u0087\u0004¢\u0006\u0002\n\u0000¨\u0006\u0007" }, d2 = { "Landroidx/window/embedding/SplitController$SplitSupportStatus$Companion;", "", "()V", "SPLIT_AVAILABLE", "Landroidx/window/embedding/SplitController$SplitSupportStatus;", "SPLIT_ERROR_PROPERTY_NOT_DECLARED", "SPLIT_UNAVAILABLE", "window_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
        public static final class Companion
        {
            private Companion() {
            }
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.window.embedding;

import kotlin.collections.SetsKt;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import java.util.Set;
import kotlin.Metadata;

@Metadata(d1 = { "\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\"\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0007\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0006\u0018\u00002\u00020\u0001:\u0001\u0017B)\b\u0000\u0012\b\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005\u0012\b\b\u0002\u0010\u0007\u001a\u00020\b¢\u0006\u0002\u0010\tJ\u0013\u0010\u000e\u001a\u00020\b2\b\u0010\u000f\u001a\u0004\u0018\u00010\u0010H\u0096\u0002J\b\u0010\u0011\u001a\u00020\u0012H\u0016J\u0016\u0010\u0013\u001a\u00020\u00002\u0006\u0010\u0014\u001a\u00020\u0006H\u0080\u0002¢\u0006\u0002\b\u0015J\b\u0010\u0016\u001a\u00020\u0003H\u0016R\u0011\u0010\u0007\u001a\u00020\b¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000bR\u0017\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\r¨\u0006\u0018" }, d2 = { "Landroidx/window/embedding/ActivityRule;", "Landroidx/window/embedding/EmbeddingRule;", "tag", "", "filters", "", "Landroidx/window/embedding/ActivityFilter;", "alwaysExpand", "", "(Ljava/lang/String;Ljava/util/Set;Z)V", "getAlwaysExpand", "()Z", "getFilters", "()Ljava/util/Set;", "equals", "other", "", "hashCode", "", "plus", "filter", "plus$window_release", "toString", "Builder", "window_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
public final class ActivityRule extends EmbeddingRule
{
    private final boolean alwaysExpand;
    private final Set<ActivityFilter> filters;
    
    public ActivityRule(final String s, final Set<ActivityFilter> filters, final boolean alwaysExpand) {
        Intrinsics.checkNotNullParameter((Object)filters, "filters");
        super(s);
        this.filters = filters;
        this.alwaysExpand = alwaysExpand;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ActivityRule)) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        final Set<ActivityFilter> filters = this.filters;
        final ActivityRule activityRule = (ActivityRule)o;
        return Intrinsics.areEqual((Object)filters, (Object)activityRule.filters) && this.alwaysExpand == activityRule.alwaysExpand;
    }
    
    public final boolean getAlwaysExpand() {
        return this.alwaysExpand;
    }
    
    public final Set<ActivityFilter> getFilters() {
        return this.filters;
    }
    
    @Override
    public int hashCode() {
        return (super.hashCode() * 31 + this.filters.hashCode()) * 31 + ActivityRule$$ExternalSyntheticBackport0.m(this.alwaysExpand);
    }
    
    public final ActivityRule plus$window_release(final ActivityFilter activityFilter) {
        Intrinsics.checkNotNullParameter((Object)activityFilter, "filter");
        return new ActivityRule(this.getTag(), SetsKt.plus((Set)this.filters, (Object)activityFilter), this.alwaysExpand);
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("ActivityRule:{tag={");
        sb.append(this.getTag());
        sb.append("},filters={");
        sb.append(this.filters);
        sb.append("}, alwaysExpand={");
        sb.append(this.alwaysExpand);
        sb.append("}}");
        return sb.toString();
    }
    
    @Metadata(d1 = { "\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\"\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0013\u0012\f\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003¢\u0006\u0002\u0010\u0005J\u0006\u0010\n\u001a\u00020\u000bJ\u000e\u0010\f\u001a\u00020\u00002\u0006\u0010\u0006\u001a\u00020\u0007J\u0010\u0010\r\u001a\u00020\u00002\b\u0010\b\u001a\u0004\u0018\u00010\tR\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u000e¢\u0006\u0002\n\u0000R\u0014\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003X\u0082\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\b\u001a\u0004\u0018\u00010\tX\u0082\u000e¢\u0006\u0002\n\u0000¨\u0006\u000e" }, d2 = { "Landroidx/window/embedding/ActivityRule$Builder;", "", "filters", "", "Landroidx/window/embedding/ActivityFilter;", "(Ljava/util/Set;)V", "alwaysExpand", "", "tag", "", "build", "Landroidx/window/embedding/ActivityRule;", "setAlwaysExpand", "setTag", "window_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
    public static final class Builder
    {
        private boolean alwaysExpand;
        private final Set<ActivityFilter> filters;
        private String tag;
        
        public Builder(final Set<ActivityFilter> filters) {
            Intrinsics.checkNotNullParameter((Object)filters, "filters");
            this.filters = filters;
        }
        
        public final ActivityRule build() {
            return new ActivityRule(this.tag, this.filters, this.alwaysExpand);
        }
        
        public final Builder setAlwaysExpand(final boolean alwaysExpand) {
            final Builder builder = this;
            this.alwaysExpand = alwaysExpand;
            return this;
        }
        
        public final Builder setTag(final String tag) {
            final Builder builder = this;
            this.tag = tag;
            return this;
        }
    }
}

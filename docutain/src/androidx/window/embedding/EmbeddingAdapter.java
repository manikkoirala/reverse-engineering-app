// 
// Decompiled by Procyon v0.6.0
// 

package androidx.window.embedding;

import kotlin.reflect.KClass;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.internal.Reflection;
import kotlin.collections.ArraysKt;
import androidx.window.extensions.core.util.function.Function;
import androidx.window.extensions.embedding.SplitAttributes$Builder;
import kotlin.collections.SetsKt;
import java.util.Set;
import java.util.ArrayList;
import kotlin.collections.CollectionsKt;
import androidx.window.extensions.layout.WindowLayoutInfo;
import android.content.res.Configuration;
import androidx.window.layout.adapter.extensions.ExtensionsWindowLayoutInfoAdapter;
import androidx.window.layout.WindowMetricsCalculator;
import androidx.window.extensions.embedding.SplitAttributes$SplitType$RatioSplitType;
import androidx.window.extensions.embedding.SplitAttributes$SplitType$ExpandContainersSplitType;
import androidx.window.extensions.embedding.SplitAttributes$SplitType$HingeSplitType;
import androidx.window.extensions.embedding.SplitAttributes$SplitType;
import androidx.window.extensions.embedding.SplitPlaceholderRule$Builder;
import androidx.window.extensions.embedding.SplitPairRule$Builder;
import java.util.Iterator;
import java.util.Collection;
import androidx.window.extensions.core.util.function.Predicate;
import androidx.window.extensions.embedding.ActivityRule$Builder;
import java.util.List;
import androidx.window.extensions.embedding.SplitInfo;
import androidx.window.core.ExtensionsUtil;
import kotlin.jvm.internal.Intrinsics;
import android.content.Intent;
import android.view.WindowMetrics;
import android.content.Context;
import android.app.Activity;
import androidx.window.extensions.embedding.SplitAttributes;
import androidx.window.extensions.embedding.SplitAttributesCalculatorParams;
import kotlin.jvm.functions.Function1;
import android.util.Pair;
import androidx.window.core.PredicateAdapter;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000ª\u0001\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\"\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\b\u0000\u0018\u00002\u00020\u0001:\u000267B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\"\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\r0\f2\u0006\u0010\u000e\u001a\u00020\u000f2\f\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u00110\fJ\u0015\u0010\u000b\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u0014H\u0000¢\u0006\u0002\b\u0015J\u0010\u0010\u000b\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\u0018H\u0007J\u0010\u0010\u000b\u001a\u00020\u00192\u0006\u0010\u001a\u001a\u00020\u001bH\u0002J\u001a\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\u00190\u001c2\f\u0010\u001d\u001a\b\u0012\u0004\u0012\u00020\u001b0\u001cJ\u001c\u0010\u001e\u001a\u00020\u001f2\u0006\u0010 \u001a\u00020!2\n\u0010\"\u001a\u0006\u0012\u0002\b\u00030#H\u0002J\u000e\u0010$\u001a\u00020\n2\u0006\u0010%\u001a\u00020&J\u000e\u0010'\u001a\u00020\u00142\u0006\u0010\u0013\u001a\u00020\u0012J&\u0010(\u001a\u000e\u0012\u0004\u0012\u00020\u0018\u0012\u0004\u0012\u00020\u00140)2\u0012\u0010*\u001a\u000e\u0012\u0004\u0012\u00020\u0016\u0012\u0004\u0012\u00020\u00120+J$\u0010,\u001a\u00020-2\u0006\u0010\u000e\u001a\u00020\u000f2\u0006\u0010 \u001a\u00020.2\n\u0010\"\u001a\u0006\u0012\u0002\b\u00030#H\u0002J$\u0010/\u001a\u0002002\u0006\u0010\u000e\u001a\u00020\u000f2\u0006\u0010 \u001a\u0002012\n\u0010\"\u001a\u0006\u0012\u0002\b\u00030#H\u0002J\u0010\u00102\u001a\u0002032\u0006\u00104\u001a\u000205H\u0002R\u0012\u0010\u0005\u001a\u00060\u0006R\u00020\u0000X\u0082\u0004¢\u0006\u0002\n\u0000R\u0012\u0010\u0007\u001a\u00060\bR\u00020\u0000X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004¢\u0006\u0002\n\u0000¨\u00068" }, d2 = { "Landroidx/window/embedding/EmbeddingAdapter;", "", "predicateAdapter", "Landroidx/window/core/PredicateAdapter;", "(Landroidx/window/core/PredicateAdapter;)V", "api1Impl", "Landroidx/window/embedding/EmbeddingAdapter$VendorApiLevel1Impl;", "api2Impl", "Landroidx/window/embedding/EmbeddingAdapter$VendorApiLevel2Impl;", "vendorApiLevel", "", "translate", "", "Landroidx/window/extensions/embedding/EmbeddingRule;", "context", "Landroid/content/Context;", "rules", "Landroidx/window/embedding/EmbeddingRule;", "Landroidx/window/embedding/SplitAttributes;", "splitAttributes", "Landroidx/window/extensions/embedding/SplitAttributes;", "translate$window_release", "Landroidx/window/embedding/SplitAttributesCalculatorParams;", "params", "Landroidx/window/extensions/embedding/SplitAttributesCalculatorParams;", "Landroidx/window/embedding/SplitInfo;", "splitInfo", "Landroidx/window/extensions/embedding/SplitInfo;", "", "splitInfoList", "translateActivityRule", "Landroidx/window/extensions/embedding/ActivityRule;", "rule", "Landroidx/window/embedding/ActivityRule;", "predicateClass", "Ljava/lang/Class;", "translateFinishBehavior", "behavior", "Landroidx/window/embedding/SplitRule$FinishBehavior;", "translateSplitAttributes", "translateSplitAttributesCalculator", "Landroidx/window/extensions/core/util/function/Function;", "calculator", "Lkotlin/Function1;", "translateSplitPairRule", "Landroidx/window/extensions/embedding/SplitPairRule;", "Landroidx/window/embedding/SplitPairRule;", "translateSplitPlaceholderRule", "Landroidx/window/extensions/embedding/SplitPlaceholderRule;", "Landroidx/window/embedding/SplitPlaceholderRule;", "translateSplitType", "Landroidx/window/extensions/embedding/SplitAttributes$SplitType;", "splitType", "Landroidx/window/embedding/SplitAttributes$SplitType;", "VendorApiLevel1Impl", "VendorApiLevel2Impl", "window_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
public final class EmbeddingAdapter
{
    private final VendorApiLevel1Impl api1Impl;
    private final VendorApiLevel2Impl api2Impl;
    private final PredicateAdapter predicateAdapter;
    private final int vendorApiLevel;
    
    public EmbeddingAdapter(final PredicateAdapter predicateAdapter) {
        Intrinsics.checkNotNullParameter((Object)predicateAdapter, "predicateAdapter");
        this.predicateAdapter = predicateAdapter;
        this.vendorApiLevel = ExtensionsUtil.INSTANCE.getSafeVendorApiLevel();
        this.api1Impl = new VendorApiLevel1Impl(predicateAdapter);
        this.api2Impl = new VendorApiLevel2Impl();
    }
    
    private final androidx.window.embedding.SplitInfo translate(final SplitInfo splitInfo) {
        final int vendorApiLevel = this.vendorApiLevel;
        androidx.window.embedding.SplitInfo splitInfo2;
        if (vendorApiLevel != 1) {
            if (vendorApiLevel != 2) {
                final androidx.window.extensions.embedding.ActivityStack primaryActivityStack = splitInfo.getPrimaryActivityStack();
                Intrinsics.checkNotNullExpressionValue((Object)primaryActivityStack, "splitInfo.primaryActivityStack");
                final androidx.window.extensions.embedding.ActivityStack secondaryActivityStack = splitInfo.getSecondaryActivityStack();
                Intrinsics.checkNotNullExpressionValue((Object)secondaryActivityStack, "splitInfo.secondaryActivityStack");
                final List activities = primaryActivityStack.getActivities();
                Intrinsics.checkNotNullExpressionValue((Object)activities, "primaryActivityStack.activities");
                final ActivityStack activityStack = new ActivityStack(activities, primaryActivityStack.isEmpty());
                final List activities2 = secondaryActivityStack.getActivities();
                Intrinsics.checkNotNullExpressionValue((Object)activities2, "secondaryActivityStack.activities");
                final ActivityStack activityStack2 = new ActivityStack(activities2, secondaryActivityStack.isEmpty());
                final SplitAttributes splitAttributes = splitInfo.getSplitAttributes();
                Intrinsics.checkNotNullExpressionValue((Object)splitAttributes, "splitInfo.splitAttributes");
                splitInfo2 = new androidx.window.embedding.SplitInfo(activityStack, activityStack2, this.translate$window_release(splitAttributes));
            }
            else {
                splitInfo2 = this.api2Impl.translateCompat(splitInfo);
            }
        }
        else {
            splitInfo2 = this.api1Impl.translateCompat(splitInfo);
        }
        return splitInfo2;
    }
    
    private final androidx.window.extensions.embedding.ActivityRule translateActivityRule(final ActivityRule activityRule, final Class<?> clazz) {
        if (this.vendorApiLevel < 2) {
            return this.api1Impl.translateActivityRuleCompat(activityRule, clazz);
        }
        final ActivityRule$Builder setShouldAlwaysExpand = new ActivityRule$Builder((Predicate)new EmbeddingAdapter$$ExternalSyntheticLambda4(activityRule), (Predicate)new EmbeddingAdapter$$ExternalSyntheticLambda5(activityRule)).setShouldAlwaysExpand(activityRule.getAlwaysExpand());
        Intrinsics.checkNotNullExpressionValue((Object)setShouldAlwaysExpand, "ActivityRuleBuilder(acti\u2026Expand(rule.alwaysExpand)");
        final String tag = activityRule.getTag();
        if (tag != null) {
            setShouldAlwaysExpand.setTag(tag);
        }
        final androidx.window.extensions.embedding.ActivityRule build = setShouldAlwaysExpand.build();
        Intrinsics.checkNotNullExpressionValue((Object)build, "builder.build()");
        return build;
    }
    
    private static final boolean translateActivityRule$lambda$13(final ActivityRule activityRule, final Activity activity) {
        Intrinsics.checkNotNullParameter((Object)activityRule, "$rule");
        final Iterable iterable = activityRule.getFilters();
        final boolean b = iterable instanceof Collection;
        final boolean b2 = false;
        boolean b3;
        if (b && ((Collection)iterable).isEmpty()) {
            b3 = b2;
        }
        else {
            final Iterator iterator = iterable.iterator();
            ActivityFilter activityFilter;
            do {
                b3 = b2;
                if (!iterator.hasNext()) {
                    return b3;
                }
                activityFilter = (ActivityFilter)iterator.next();
                Intrinsics.checkNotNullExpressionValue((Object)activity, "activity");
            } while (!activityFilter.matchesActivity(activity));
            b3 = true;
        }
        return b3;
    }
    
    private static final boolean translateActivityRule$lambda$15(final ActivityRule activityRule, final Intent intent) {
        Intrinsics.checkNotNullParameter((Object)activityRule, "$rule");
        final Iterable iterable = activityRule.getFilters();
        final boolean b = iterable instanceof Collection;
        final boolean b2 = false;
        boolean b3;
        if (b && ((Collection)iterable).isEmpty()) {
            b3 = b2;
        }
        else {
            final Iterator iterator = iterable.iterator();
            ActivityFilter activityFilter;
            do {
                b3 = b2;
                if (!iterator.hasNext()) {
                    return b3;
                }
                activityFilter = (ActivityFilter)iterator.next();
                Intrinsics.checkNotNullExpressionValue((Object)intent, "intent");
            } while (!activityFilter.matchesIntent(intent));
            b3 = true;
        }
        return b3;
    }
    
    private static final SplitAttributes translateSplitAttributesCalculator$lambda$0(final EmbeddingAdapter embeddingAdapter, final Function1 function1, final SplitAttributesCalculatorParams splitAttributesCalculatorParams) {
        Intrinsics.checkNotNullParameter((Object)embeddingAdapter, "this$0");
        Intrinsics.checkNotNullParameter((Object)function1, "$calculator");
        Intrinsics.checkNotNullExpressionValue((Object)splitAttributesCalculatorParams, "oemParams");
        return embeddingAdapter.translateSplitAttributes((androidx.window.embedding.SplitAttributes)function1.invoke((Object)embeddingAdapter.translate(splitAttributesCalculatorParams)));
    }
    
    private final androidx.window.extensions.embedding.SplitPairRule translateSplitPairRule(final Context context, final SplitPairRule splitPairRule, final Class<?> clazz) {
        if (this.vendorApiLevel < 2) {
            return this.api1Impl.translateSplitPairRuleCompat(context, splitPairRule, clazz);
        }
        final EmbeddingAdapter$$ExternalSyntheticLambda0 embeddingAdapter$$ExternalSyntheticLambda0 = new EmbeddingAdapter$$ExternalSyntheticLambda0(splitPairRule);
        final EmbeddingAdapter$$ExternalSyntheticLambda1 embeddingAdapter$$ExternalSyntheticLambda2 = new EmbeddingAdapter$$ExternalSyntheticLambda1(splitPairRule);
        final EmbeddingAdapter$$ExternalSyntheticLambda2 embeddingAdapter$$ExternalSyntheticLambda3 = new EmbeddingAdapter$$ExternalSyntheticLambda2(splitPairRule, context);
        final String tag = splitPairRule.getTag();
        final SplitPairRule$Builder setShouldClearTop = new SplitPairRule$Builder((Predicate)embeddingAdapter$$ExternalSyntheticLambda0, (Predicate)embeddingAdapter$$ExternalSyntheticLambda2, (Predicate)embeddingAdapter$$ExternalSyntheticLambda3).setDefaultSplitAttributes(this.translateSplitAttributes(splitPairRule.getDefaultSplitAttributes())).setFinishPrimaryWithSecondary(this.translateFinishBehavior(splitPairRule.getFinishPrimaryWithSecondary())).setFinishSecondaryWithPrimary(this.translateFinishBehavior(splitPairRule.getFinishSecondaryWithPrimary())).setShouldClearTop(splitPairRule.getClearTop());
        Intrinsics.checkNotNullExpressionValue((Object)setShouldClearTop, "SplitPairRuleBuilder(\n  \u2026ldClearTop(rule.clearTop)");
        if (tag != null) {
            setShouldClearTop.setTag(tag);
        }
        final androidx.window.extensions.embedding.SplitPairRule build = setShouldClearTop.build();
        Intrinsics.checkNotNullExpressionValue((Object)build, "builder.build()");
        return build;
    }
    
    private static final boolean translateSplitPairRule$lambda$3(final SplitPairRule splitPairRule, final Pair pair) {
        Intrinsics.checkNotNullParameter((Object)splitPairRule, "$rule");
        final Iterable iterable = splitPairRule.getFilters();
        final boolean b = iterable instanceof Collection;
        final boolean b2 = false;
        boolean b3;
        if (b && ((Collection)iterable).isEmpty()) {
            b3 = b2;
        }
        else {
            final Iterator iterator = iterable.iterator();
            SplitPairFilter splitPairFilter;
            Activity activity;
            Object second;
            do {
                b3 = b2;
                if (!iterator.hasNext()) {
                    return b3;
                }
                splitPairFilter = (SplitPairFilter)iterator.next();
                final Object first = pair.first;
                Intrinsics.checkNotNullExpressionValue(first, "activitiesPair.first");
                activity = (Activity)first;
                second = pair.second;
                Intrinsics.checkNotNullExpressionValue(second, "activitiesPair.second");
            } while (!splitPairFilter.matchesActivityPair(activity, (Activity)second));
            b3 = true;
        }
        return b3;
    }
    
    private static final boolean translateSplitPairRule$lambda$5(final SplitPairRule splitPairRule, final Pair pair) {
        Intrinsics.checkNotNullParameter((Object)splitPairRule, "$rule");
        final Iterable iterable = splitPairRule.getFilters();
        final boolean b = iterable instanceof Collection;
        final boolean b2 = false;
        boolean b3;
        if (b && ((Collection)iterable).isEmpty()) {
            b3 = b2;
        }
        else {
            final Iterator iterator = iterable.iterator();
            SplitPairFilter splitPairFilter;
            Activity activity;
            Object second;
            do {
                b3 = b2;
                if (!iterator.hasNext()) {
                    return b3;
                }
                splitPairFilter = (SplitPairFilter)iterator.next();
                final Object first = pair.first;
                Intrinsics.checkNotNullExpressionValue(first, "activityIntentPair.first");
                activity = (Activity)first;
                second = pair.second;
                Intrinsics.checkNotNullExpressionValue(second, "activityIntentPair.second");
            } while (!splitPairFilter.matchesActivityIntentPair(activity, (Intent)second));
            b3 = true;
        }
        return b3;
    }
    
    private static final boolean translateSplitPairRule$lambda$6(final SplitPairRule splitPairRule, final Context context, final WindowMetrics windowMetrics) {
        Intrinsics.checkNotNullParameter((Object)splitPairRule, "$rule");
        Intrinsics.checkNotNullParameter((Object)context, "$context");
        Intrinsics.checkNotNullExpressionValue((Object)windowMetrics, "windowMetrics");
        return splitPairRule.checkParentMetrics$window_release(context, windowMetrics);
    }
    
    private final androidx.window.extensions.embedding.SplitPlaceholderRule translateSplitPlaceholderRule(final Context context, final SplitPlaceholderRule splitPlaceholderRule, final Class<?> clazz) {
        if (this.vendorApiLevel < 2) {
            return this.api1Impl.translateSplitPlaceholderRuleCompat(context, splitPlaceholderRule, clazz);
        }
        final EmbeddingAdapter$$ExternalSyntheticLambda6 embeddingAdapter$$ExternalSyntheticLambda6 = new EmbeddingAdapter$$ExternalSyntheticLambda6(splitPlaceholderRule);
        final EmbeddingAdapter$$ExternalSyntheticLambda7 embeddingAdapter$$ExternalSyntheticLambda7 = new EmbeddingAdapter$$ExternalSyntheticLambda7(splitPlaceholderRule);
        final EmbeddingAdapter$$ExternalSyntheticLambda8 embeddingAdapter$$ExternalSyntheticLambda8 = new EmbeddingAdapter$$ExternalSyntheticLambda8(splitPlaceholderRule, context);
        final String tag = splitPlaceholderRule.getTag();
        final SplitPlaceholderRule$Builder setFinishPrimaryWithPlaceholder = new SplitPlaceholderRule$Builder(splitPlaceholderRule.getPlaceholderIntent(), (Predicate)embeddingAdapter$$ExternalSyntheticLambda6, (Predicate)embeddingAdapter$$ExternalSyntheticLambda7, (Predicate)embeddingAdapter$$ExternalSyntheticLambda8).setSticky(splitPlaceholderRule.isSticky()).setDefaultSplitAttributes(this.translateSplitAttributes(splitPlaceholderRule.getDefaultSplitAttributes())).setFinishPrimaryWithPlaceholder(this.translateFinishBehavior(splitPlaceholderRule.getFinishPrimaryWithPlaceholder()));
        Intrinsics.checkNotNullExpressionValue((Object)setFinishPrimaryWithPlaceholder, "SplitPlaceholderRuleBuil\u2026holder)\n                )");
        if (tag != null) {
            setFinishPrimaryWithPlaceholder.setTag(tag);
        }
        final androidx.window.extensions.embedding.SplitPlaceholderRule build = setFinishPrimaryWithPlaceholder.build();
        Intrinsics.checkNotNullExpressionValue((Object)build, "builder.build()");
        return build;
    }
    
    private static final boolean translateSplitPlaceholderRule$lambda$10(final SplitPlaceholderRule splitPlaceholderRule, final Intent intent) {
        Intrinsics.checkNotNullParameter((Object)splitPlaceholderRule, "$rule");
        final Iterable iterable = splitPlaceholderRule.getFilters();
        final boolean b = iterable instanceof Collection;
        final boolean b2 = false;
        boolean b3;
        if (b && ((Collection)iterable).isEmpty()) {
            b3 = b2;
        }
        else {
            final Iterator iterator = iterable.iterator();
            ActivityFilter activityFilter;
            do {
                b3 = b2;
                if (!iterator.hasNext()) {
                    return b3;
                }
                activityFilter = (ActivityFilter)iterator.next();
                Intrinsics.checkNotNullExpressionValue((Object)intent, "intent");
            } while (!activityFilter.matchesIntent(intent));
            b3 = true;
        }
        return b3;
    }
    
    private static final boolean translateSplitPlaceholderRule$lambda$11(final SplitPlaceholderRule splitPlaceholderRule, final Context context, final WindowMetrics windowMetrics) {
        Intrinsics.checkNotNullParameter((Object)splitPlaceholderRule, "$rule");
        Intrinsics.checkNotNullParameter((Object)context, "$context");
        Intrinsics.checkNotNullExpressionValue((Object)windowMetrics, "windowMetrics");
        return splitPlaceholderRule.checkParentMetrics$window_release(context, windowMetrics);
    }
    
    private static final boolean translateSplitPlaceholderRule$lambda$8(final SplitPlaceholderRule splitPlaceholderRule, final Activity activity) {
        Intrinsics.checkNotNullParameter((Object)splitPlaceholderRule, "$rule");
        final Iterable iterable = splitPlaceholderRule.getFilters();
        final boolean b = iterable instanceof Collection;
        final boolean b2 = false;
        boolean b3;
        if (b && ((Collection)iterable).isEmpty()) {
            b3 = b2;
        }
        else {
            final Iterator iterator = iterable.iterator();
            ActivityFilter activityFilter;
            do {
                b3 = b2;
                if (!iterator.hasNext()) {
                    return b3;
                }
                activityFilter = (ActivityFilter)iterator.next();
                Intrinsics.checkNotNullExpressionValue((Object)activity, "activity");
            } while (!activityFilter.matchesActivity(activity));
            b3 = true;
        }
        return b3;
    }
    
    private final SplitAttributes$SplitType translateSplitType(final androidx.window.embedding.SplitAttributes.SplitType obj) {
        if (this.vendorApiLevel >= 2) {
            SplitAttributes$SplitType splitAttributes$SplitType;
            if (Intrinsics.areEqual((Object)obj, (Object)androidx.window.embedding.SplitAttributes.SplitType.SPLIT_TYPE_HINGE)) {
                splitAttributes$SplitType = (SplitAttributes$SplitType)new SplitAttributes$SplitType$HingeSplitType(this.translateSplitType(androidx.window.embedding.SplitAttributes.SplitType.SPLIT_TYPE_EQUAL));
            }
            else if (Intrinsics.areEqual((Object)obj, (Object)androidx.window.embedding.SplitAttributes.SplitType.SPLIT_TYPE_EXPAND)) {
                splitAttributes$SplitType = (SplitAttributes$SplitType)new SplitAttributes$SplitType$ExpandContainersSplitType();
            }
            else {
                final float value$window_release = obj.getValue$window_release();
                final double n = value$window_release;
                if (n <= 0.0 || n >= 1.0) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Unsupported SplitType: ");
                    sb.append(obj);
                    sb.append(" with value: ");
                    sb.append(obj.getValue$window_release());
                    throw new IllegalArgumentException(sb.toString());
                }
                splitAttributes$SplitType = (SplitAttributes$SplitType)new SplitAttributes$SplitType$RatioSplitType(value$window_release);
            }
            return splitAttributes$SplitType;
        }
        throw new IllegalArgumentException("Failed requirement.".toString());
    }
    
    public final androidx.window.embedding.SplitAttributesCalculatorParams translate(final SplitAttributesCalculatorParams splitAttributesCalculatorParams) {
        Intrinsics.checkNotNullParameter((Object)splitAttributesCalculatorParams, "params");
        final EmbeddingAdapter embeddingAdapter = this;
        final WindowMetrics parentWindowMetrics = splitAttributesCalculatorParams.getParentWindowMetrics();
        Intrinsics.checkNotNullExpressionValue((Object)parentWindowMetrics, "params.parentWindowMetrics");
        final Configuration parentConfiguration = splitAttributesCalculatorParams.getParentConfiguration();
        Intrinsics.checkNotNullExpressionValue((Object)parentConfiguration, "params.parentConfiguration");
        final WindowLayoutInfo parentWindowLayoutInfo = splitAttributesCalculatorParams.getParentWindowLayoutInfo();
        Intrinsics.checkNotNullExpressionValue((Object)parentWindowLayoutInfo, "params.parentWindowLayoutInfo");
        final SplitAttributes defaultSplitAttributes = splitAttributesCalculatorParams.getDefaultSplitAttributes();
        Intrinsics.checkNotNullExpressionValue((Object)defaultSplitAttributes, "params.defaultSplitAttributes");
        final boolean defaultConstraintsSatisfied = splitAttributesCalculatorParams.areDefaultConstraintsSatisfied();
        final String splitRuleTag = splitAttributesCalculatorParams.getSplitRuleTag();
        final androidx.window.layout.WindowMetrics translateWindowMetrics$window_release = WindowMetricsCalculator.Companion.translateWindowMetrics$window_release(parentWindowMetrics);
        return new androidx.window.embedding.SplitAttributesCalculatorParams(translateWindowMetrics$window_release, parentConfiguration, ExtensionsWindowLayoutInfoAdapter.INSTANCE.translate$window_release(translateWindowMetrics$window_release, parentWindowLayoutInfo), this.translate$window_release(defaultSplitAttributes), defaultConstraintsSatisfied, splitRuleTag);
    }
    
    public final List<androidx.window.embedding.SplitInfo> translate(final List<? extends SplitInfo> list) {
        Intrinsics.checkNotNullParameter((Object)list, "splitInfoList");
        final Iterable iterable = list;
        final Collection collection = new ArrayList(CollectionsKt.collectionSizeOrDefault(iterable, 10));
        final Iterator iterator = iterable.iterator();
        while (iterator.hasNext()) {
            collection.add(this.translate((SplitInfo)iterator.next()));
        }
        return (List<androidx.window.embedding.SplitInfo>)collection;
    }
    
    public final Set<androidx.window.extensions.embedding.EmbeddingRule> translate(final Context context, final Set<? extends EmbeddingRule> set) {
        Intrinsics.checkNotNullParameter((Object)context, "context");
        Intrinsics.checkNotNullParameter((Object)set, "rules");
        final Class<?> predicateClassOrNull$window_release = this.predicateAdapter.predicateClassOrNull$window_release();
        if (predicateClassOrNull$window_release == null) {
            return SetsKt.emptySet();
        }
        final Iterable iterable = set;
        final Collection collection = new ArrayList(CollectionsKt.collectionSizeOrDefault(iterable, 10));
        for (final EmbeddingRule embeddingRule : iterable) {
            androidx.window.extensions.embedding.EmbeddingRule embeddingRule2;
            if (embeddingRule instanceof SplitPairRule) {
                embeddingRule2 = (androidx.window.extensions.embedding.EmbeddingRule)this.translateSplitPairRule(context, (SplitPairRule)embeddingRule, predicateClassOrNull$window_release);
            }
            else if (embeddingRule instanceof SplitPlaceholderRule) {
                embeddingRule2 = (androidx.window.extensions.embedding.EmbeddingRule)this.translateSplitPlaceholderRule(context, (SplitPlaceholderRule)embeddingRule, predicateClassOrNull$window_release);
            }
            else {
                if (!(embeddingRule instanceof ActivityRule)) {
                    throw new IllegalArgumentException("Unsupported rule type");
                }
                embeddingRule2 = (androidx.window.extensions.embedding.EmbeddingRule)this.translateActivityRule((ActivityRule)embeddingRule, predicateClassOrNull$window_release);
            }
            collection.add(embeddingRule2);
        }
        return CollectionsKt.toSet((Iterable)collection);
    }
    
    public final androidx.window.embedding.SplitAttributes translate$window_release(final SplitAttributes splitAttributes) {
        Intrinsics.checkNotNullParameter((Object)splitAttributes, "splitAttributes");
        final androidx.window.embedding.SplitAttributes.Builder builder = new androidx.window.embedding.SplitAttributes.Builder();
        final SplitAttributes$SplitType splitType = splitAttributes.getSplitType();
        Intrinsics.checkNotNullExpressionValue((Object)splitType, "splitAttributes.splitType");
        androidx.window.embedding.SplitAttributes.SplitType splitType2;
        if (splitType instanceof SplitAttributes$SplitType$HingeSplitType) {
            splitType2 = androidx.window.embedding.SplitAttributes.SplitType.SPLIT_TYPE_HINGE;
        }
        else if (splitType instanceof SplitAttributes$SplitType$ExpandContainersSplitType) {
            splitType2 = androidx.window.embedding.SplitAttributes.SplitType.SPLIT_TYPE_EXPAND;
        }
        else {
            if (!(splitType instanceof SplitAttributes$SplitType$RatioSplitType)) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Unknown split type: ");
                sb.append(splitType);
                throw new IllegalArgumentException(sb.toString());
            }
            splitType2 = androidx.window.embedding.SplitAttributes.SplitType.Companion.ratio(((SplitAttributes$SplitType$RatioSplitType)splitType).getRatio());
        }
        final androidx.window.embedding.SplitAttributes.Builder setSplitType = builder.setSplitType(splitType2);
        final int layoutDirection = splitAttributes.getLayoutDirection();
        androidx.window.embedding.SplitAttributes.LayoutDirection layoutDirection2;
        if (layoutDirection != 0) {
            if (layoutDirection != 1) {
                if (layoutDirection != 3) {
                    if (layoutDirection != 4) {
                        if (layoutDirection != 5) {
                            final StringBuilder sb2 = new StringBuilder();
                            sb2.append("Unknown layout direction: ");
                            sb2.append(layoutDirection);
                            throw new IllegalArgumentException(sb2.toString());
                        }
                        layoutDirection2 = androidx.window.embedding.SplitAttributes.LayoutDirection.BOTTOM_TO_TOP;
                    }
                    else {
                        layoutDirection2 = androidx.window.embedding.SplitAttributes.LayoutDirection.TOP_TO_BOTTOM;
                    }
                }
                else {
                    layoutDirection2 = androidx.window.embedding.SplitAttributes.LayoutDirection.LOCALE;
                }
            }
            else {
                layoutDirection2 = androidx.window.embedding.SplitAttributes.LayoutDirection.RIGHT_TO_LEFT;
            }
        }
        else {
            layoutDirection2 = androidx.window.embedding.SplitAttributes.LayoutDirection.LEFT_TO_RIGHT;
        }
        return setSplitType.setLayoutDirection(layoutDirection2).build();
    }
    
    public final int translateFinishBehavior(final SplitRule.FinishBehavior obj) {
        Intrinsics.checkNotNullParameter((Object)obj, "behavior");
        int n;
        if (Intrinsics.areEqual((Object)obj, (Object)SplitRule.FinishBehavior.NEVER)) {
            n = 0;
        }
        else if (Intrinsics.areEqual((Object)obj, (Object)SplitRule.FinishBehavior.ALWAYS)) {
            n = 1;
        }
        else {
            if (!Intrinsics.areEqual((Object)obj, (Object)SplitRule.FinishBehavior.ADJACENT)) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Unknown finish behavior:");
                sb.append(obj);
                throw new IllegalArgumentException(sb.toString());
            }
            n = 2;
        }
        return n;
    }
    
    public final SplitAttributes translateSplitAttributes(final androidx.window.embedding.SplitAttributes obj) {
        Intrinsics.checkNotNullParameter((Object)obj, "splitAttributes");
        final int vendorApiLevel = this.vendorApiLevel;
        final int n = 1;
        if (vendorApiLevel >= 2) {
            final SplitAttributes$Builder setSplitType = new SplitAttributes$Builder().setSplitType(this.translateSplitType(obj.getSplitType()));
            final androidx.window.embedding.SplitAttributes.LayoutDirection layoutDirection = obj.getLayoutDirection();
            int layoutDirection2;
            if (Intrinsics.areEqual((Object)layoutDirection, (Object)androidx.window.embedding.SplitAttributes.LayoutDirection.LOCALE)) {
                layoutDirection2 = 3;
            }
            else if (Intrinsics.areEqual((Object)layoutDirection, (Object)androidx.window.embedding.SplitAttributes.LayoutDirection.LEFT_TO_RIGHT)) {
                layoutDirection2 = 0;
            }
            else if (Intrinsics.areEqual((Object)layoutDirection, (Object)androidx.window.embedding.SplitAttributes.LayoutDirection.RIGHT_TO_LEFT)) {
                layoutDirection2 = n;
            }
            else if (Intrinsics.areEqual((Object)layoutDirection, (Object)androidx.window.embedding.SplitAttributes.LayoutDirection.TOP_TO_BOTTOM)) {
                layoutDirection2 = 4;
            }
            else {
                if (!Intrinsics.areEqual((Object)layoutDirection, (Object)androidx.window.embedding.SplitAttributes.LayoutDirection.BOTTOM_TO_TOP)) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Unsupported layoutDirection:");
                    sb.append(obj);
                    sb.append(".layoutDirection");
                    throw new IllegalArgumentException(sb.toString());
                }
                layoutDirection2 = 5;
            }
            final SplitAttributes build = setSplitType.setLayoutDirection(layoutDirection2).build();
            Intrinsics.checkNotNullExpressionValue((Object)build, "Builder()\n            .s\u2026   )\n            .build()");
            return build;
        }
        throw new IllegalArgumentException("Failed requirement.".toString());
    }
    
    public final Function<SplitAttributesCalculatorParams, SplitAttributes> translateSplitAttributesCalculator(final Function1<? super androidx.window.embedding.SplitAttributesCalculatorParams, androidx.window.embedding.SplitAttributes> function1) {
        Intrinsics.checkNotNullParameter((Object)function1, "calculator");
        return new EmbeddingAdapter$$ExternalSyntheticLambda3(this, function1);
    }
    
    @Metadata(d1 = { "\u0000\u008c\u0001\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0010\"\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0007\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0082\u0004\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u000e\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\nJ\u0010\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\bH\u0002J\u0016\u0010\u000e\u001a\u00020\u00012\f\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u00110\u0010H\u0003J\u0016\u0010\u0012\u001a\u00020\u00012\f\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u00110\u0010H\u0003J\u0016\u0010\u0013\u001a\u00020\u00012\f\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\u00150\u0010H\u0003J\u001a\u0010\u0016\u001a\u00020\u00172\u0006\u0010\u0018\u001a\u00020\u00192\n\u0010\u001a\u001a\u0006\u0012\u0002\b\u00030\u001bJ\u000e\u0010\u001c\u001a\u00020\u001d2\u0006\u0010\t\u001a\u00020\nJ\u0016\u0010\u001e\u001a\u00020\u00012\f\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\u00150\u0010H\u0003J\u0018\u0010\u001f\u001a\u00020\u00012\u0006\u0010 \u001a\u00020!2\u0006\u0010\"\u001a\u00020#H\u0003J\u001c\u0010$\u001a\u000e\u0012\u0004\u0012\u00020&\u0012\u0004\u0012\u00020'0%2\u0006\u0010\r\u001a\u00020\bH\u0002J\"\u0010(\u001a\u00020)2\u0006\u0010 \u001a\u00020!2\u0006\u0010\u0018\u001a\u00020*2\n\u0010\u001a\u001a\u0006\u0012\u0002\b\u00030\u001bJ\"\u0010+\u001a\u00020,2\u0006\u0010 \u001a\u00020!2\u0006\u0010\u0018\u001a\u00020-2\n\u0010\u001a\u001a\u0006\u0012\u0002\b\u00030\u001bJ\u0014\u0010.\u001a\u00020/*\u00020/2\u0006\u00100\u001a\u00020\bH\u0002J\u0014\u0010.\u001a\u000201*\u0002012\u0006\u00100\u001a\u00020\bH\u0002R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006¨\u00062" }, d2 = { "Landroidx/window/embedding/EmbeddingAdapter$VendorApiLevel1Impl;", "", "predicateAdapter", "Landroidx/window/core/PredicateAdapter;", "(Landroidx/window/embedding/EmbeddingAdapter;Landroidx/window/core/PredicateAdapter;)V", "getPredicateAdapter", "()Landroidx/window/core/PredicateAdapter;", "getSplitAttributesCompat", "Landroidx/window/embedding/SplitAttributes;", "splitInfo", "Landroidx/window/extensions/embedding/SplitInfo;", "isSplitAttributesSupported", "", "attrs", "translateActivityIntentPredicates", "splitPairFilters", "", "Landroidx/window/embedding/SplitPairFilter;", "translateActivityPairPredicates", "translateActivityPredicates", "activityFilters", "Landroidx/window/embedding/ActivityFilter;", "translateActivityRuleCompat", "Landroidx/window/extensions/embedding/ActivityRule;", "rule", "Landroidx/window/embedding/ActivityRule;", "predicateClass", "Ljava/lang/Class;", "translateCompat", "Landroidx/window/embedding/SplitInfo;", "translateIntentPredicates", "translateParentMetricsPredicate", "context", "Landroid/content/Context;", "splitRule", "Landroidx/window/embedding/SplitRule;", "translateSplitAttributesCompatInternal", "Lkotlin/Pair;", "", "", "translateSplitPairRuleCompat", "Landroidx/window/extensions/embedding/SplitPairRule;", "Landroidx/window/embedding/SplitPairRule;", "translateSplitPlaceholderRuleCompat", "Landroidx/window/extensions/embedding/SplitPlaceholderRule;", "Landroidx/window/embedding/SplitPlaceholderRule;", "setDefaultSplitAttributesCompat", "Landroidx/window/extensions/embedding/SplitPairRule$Builder;", "defaultAttrs", "Landroidx/window/extensions/embedding/SplitPlaceholderRule$Builder;", "window_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
    private final class VendorApiLevel1Impl
    {
        private final PredicateAdapter predicateAdapter;
        final EmbeddingAdapter this$0;
        
        public VendorApiLevel1Impl(final EmbeddingAdapter this$0, final PredicateAdapter predicateAdapter) {
            Intrinsics.checkNotNullParameter((Object)predicateAdapter, "predicateAdapter");
            this.this$0 = this$0;
            this.predicateAdapter = predicateAdapter;
        }
        
        private final boolean isSplitAttributesSupported(final androidx.window.embedding.SplitAttributes splitAttributes) {
            final double n = splitAttributes.getSplitType().getValue$window_release();
            boolean b = true;
            if (0.0 > n || n > 1.0 || splitAttributes.getSplitType().getValue$window_release() == 1.0f || !ArraysKt.contains((Object[])new androidx.window.embedding.SplitAttributes.LayoutDirection[] { androidx.window.embedding.SplitAttributes.LayoutDirection.LEFT_TO_RIGHT, androidx.window.embedding.SplitAttributes.LayoutDirection.RIGHT_TO_LEFT, androidx.window.embedding.SplitAttributes.LayoutDirection.LOCALE }, (Object)splitAttributes.getLayoutDirection())) {
                b = false;
            }
            return b;
        }
        
        private final SplitPairRule$Builder setDefaultSplitAttributesCompat(final SplitPairRule$Builder splitPairRule$Builder, final androidx.window.embedding.SplitAttributes splitAttributes) {
            final kotlin.Pair<Float, Integer> translateSplitAttributesCompatInternal = this.translateSplitAttributesCompatInternal(splitAttributes);
            final float floatValue = ((Number)translateSplitAttributesCompatInternal.component1()).floatValue();
            final int intValue = ((Number)translateSplitAttributesCompatInternal.component2()).intValue();
            splitPairRule$Builder.setSplitRatio(floatValue);
            splitPairRule$Builder.setLayoutDirection(intValue);
            return splitPairRule$Builder;
        }
        
        private final SplitPlaceholderRule$Builder setDefaultSplitAttributesCompat(final SplitPlaceholderRule$Builder splitPlaceholderRule$Builder, final androidx.window.embedding.SplitAttributes splitAttributes) {
            final kotlin.Pair<Float, Integer> translateSplitAttributesCompatInternal = this.translateSplitAttributesCompatInternal(splitAttributes);
            final float floatValue = ((Number)translateSplitAttributesCompatInternal.component1()).floatValue();
            final int intValue = ((Number)translateSplitAttributesCompatInternal.component2()).intValue();
            splitPlaceholderRule$Builder.setSplitRatio(floatValue);
            splitPlaceholderRule$Builder.setLayoutDirection(intValue);
            return splitPlaceholderRule$Builder;
        }
        
        private final Object translateActivityIntentPredicates(final Set<SplitPairFilter> set) {
            return this.predicateAdapter.buildPairPredicate((kotlin.reflect.KClass<Object>)Reflection.getOrCreateKotlinClass((Class)Activity.class), (kotlin.reflect.KClass<Object>)Reflection.getOrCreateKotlinClass((Class)Intent.class), (kotlin.jvm.functions.Function2<? super Object, ? super Object, Boolean>)new EmbeddingAdapter$VendorApiLevel1Impl$translateActivityIntentPredicates.EmbeddingAdapter$VendorApiLevel1Impl$translateActivityIntentPredicates$1((Set)set));
        }
        
        private final Object translateActivityPairPredicates(final Set<SplitPairFilter> set) {
            return this.predicateAdapter.buildPairPredicate((kotlin.reflect.KClass<Object>)Reflection.getOrCreateKotlinClass((Class)Activity.class), (kotlin.reflect.KClass<Object>)Reflection.getOrCreateKotlinClass((Class)Activity.class), (kotlin.jvm.functions.Function2<? super Object, ? super Object, Boolean>)new EmbeddingAdapter$VendorApiLevel1Impl$translateActivityPairPredicates.EmbeddingAdapter$VendorApiLevel1Impl$translateActivityPairPredicates$1((Set)set));
        }
        
        private final Object translateActivityPredicates(final Set<ActivityFilter> set) {
            return this.predicateAdapter.buildPredicate((kotlin.reflect.KClass<Object>)Reflection.getOrCreateKotlinClass((Class)Activity.class), (kotlin.jvm.functions.Function1<? super Object, Boolean>)new EmbeddingAdapter$VendorApiLevel1Impl$translateActivityPredicates.EmbeddingAdapter$VendorApiLevel1Impl$translateActivityPredicates$1((Set)set));
        }
        
        private final Object translateIntentPredicates(final Set<ActivityFilter> set) {
            return this.predicateAdapter.buildPredicate((kotlin.reflect.KClass<Object>)Reflection.getOrCreateKotlinClass((Class)Intent.class), (kotlin.jvm.functions.Function1<? super Object, Boolean>)new EmbeddingAdapter$VendorApiLevel1Impl$translateIntentPredicates.EmbeddingAdapter$VendorApiLevel1Impl$translateIntentPredicates$1((Set)set));
        }
        
        private final Object translateParentMetricsPredicate(final Context context, final SplitRule splitRule) {
            return this.predicateAdapter.buildPredicate((kotlin.reflect.KClass<Object>)Reflection.getOrCreateKotlinClass((Class)WindowMetrics.class), (kotlin.jvm.functions.Function1<? super Object, Boolean>)new EmbeddingAdapter$VendorApiLevel1Impl$translateParentMetricsPredicate.EmbeddingAdapter$VendorApiLevel1Impl$translateParentMetricsPredicate$1(splitRule, context));
        }
        
        private final kotlin.Pair<Float, Integer> translateSplitAttributesCompatInternal(final androidx.window.embedding.SplitAttributes splitAttributes) {
            final boolean splitAttributesSupported = this.isSplitAttributesSupported(splitAttributes);
            int i = 3;
            kotlin.Pair pair;
            if (!splitAttributesSupported) {
                pair = new kotlin.Pair((Object)0.0f, (Object)3);
            }
            else {
                final float value$window_release = splitAttributes.getSplitType().getValue$window_release();
                final androidx.window.embedding.SplitAttributes.LayoutDirection layoutDirection = splitAttributes.getLayoutDirection();
                if (!Intrinsics.areEqual((Object)layoutDirection, (Object)androidx.window.embedding.SplitAttributes.LayoutDirection.LOCALE)) {
                    if (Intrinsics.areEqual((Object)layoutDirection, (Object)androidx.window.embedding.SplitAttributes.LayoutDirection.LEFT_TO_RIGHT)) {
                        i = 0;
                    }
                    else {
                        if (!Intrinsics.areEqual((Object)layoutDirection, (Object)androidx.window.embedding.SplitAttributes.LayoutDirection.RIGHT_TO_LEFT)) {
                            throw new IllegalStateException("Unsupported layout direction must be covered in @isSplitAttributesSupported!");
                        }
                        i = 1;
                    }
                }
                pair = new kotlin.Pair((Object)value$window_release, (Object)i);
            }
            return (kotlin.Pair<Float, Integer>)pair;
        }
        
        public final PredicateAdapter getPredicateAdapter() {
            return this.predicateAdapter;
        }
        
        public final androidx.window.embedding.SplitAttributes getSplitAttributesCompat(final SplitInfo splitInfo) {
            Intrinsics.checkNotNullParameter((Object)splitInfo, "splitInfo");
            return new androidx.window.embedding.SplitAttributes.Builder().setSplitType(androidx.window.embedding.SplitAttributes.SplitType.Companion.buildSplitTypeFromValue$window_release(splitInfo.getSplitRatio())).setLayoutDirection(androidx.window.embedding.SplitAttributes.LayoutDirection.LOCALE).build();
        }
        
        public final androidx.window.extensions.embedding.ActivityRule translateActivityRuleCompat(final ActivityRule activityRule, final Class<?> clazz) {
            Intrinsics.checkNotNullParameter((Object)activityRule, "rule");
            Intrinsics.checkNotNullParameter((Object)clazz, "predicateClass");
            final androidx.window.extensions.embedding.ActivityRule build = ActivityRule$Builder.class.getConstructor(clazz, clazz).newInstance(this.translateActivityPredicates(activityRule.getFilters()), this.translateIntentPredicates(activityRule.getFilters())).setShouldAlwaysExpand(activityRule.getAlwaysExpand()).build();
            Intrinsics.checkNotNullExpressionValue((Object)build, "ActivityRuleBuilder::cla\u2026\n                .build()");
            return build;
        }
        
        public final androidx.window.embedding.SplitInfo translateCompat(final SplitInfo splitInfo) {
            Intrinsics.checkNotNullParameter((Object)splitInfo, "splitInfo");
            final List activities = splitInfo.getPrimaryActivityStack().getActivities();
            Intrinsics.checkNotNullExpressionValue((Object)activities, "splitInfo.primaryActivityStack.activities");
            final ActivityStack activityStack = new ActivityStack(activities, splitInfo.getPrimaryActivityStack().isEmpty());
            final List activities2 = splitInfo.getSecondaryActivityStack().getActivities();
            Intrinsics.checkNotNullExpressionValue((Object)activities2, "splitInfo.secondaryActivityStack.activities");
            return new androidx.window.embedding.SplitInfo(activityStack, new ActivityStack(activities2, splitInfo.getSecondaryActivityStack().isEmpty()), this.getSplitAttributesCompat(splitInfo));
        }
        
        public final androidx.window.extensions.embedding.SplitPairRule translateSplitPairRuleCompat(final Context context, final SplitPairRule splitPairRule, final Class<?> clazz) {
            Intrinsics.checkNotNullParameter((Object)context, "context");
            Intrinsics.checkNotNullParameter((Object)splitPairRule, "rule");
            Intrinsics.checkNotNullParameter((Object)clazz, "predicateClass");
            final SplitPairRule$Builder instance = SplitPairRule$Builder.class.getConstructor(clazz, clazz, clazz).newInstance(this.translateActivityPairPredicates(splitPairRule.getFilters()), this.translateActivityIntentPredicates(splitPairRule.getFilters()), this.translateParentMetricsPredicate(context, splitPairRule));
            Intrinsics.checkNotNullExpressionValue((Object)instance, "SplitPairRuleBuilder::cl\u2026text, rule)\n            )");
            final androidx.window.extensions.embedding.SplitPairRule build = this.setDefaultSplitAttributesCompat(instance, splitPairRule.getDefaultSplitAttributes()).setShouldClearTop(splitPairRule.getClearTop()).setFinishPrimaryWithSecondary(this.this$0.translateFinishBehavior(splitPairRule.getFinishPrimaryWithSecondary())).setFinishSecondaryWithPrimary(this.this$0.translateFinishBehavior(splitPairRule.getFinishSecondaryWithPrimary())).build();
            Intrinsics.checkNotNullExpressionValue((Object)build, "SplitPairRuleBuilder::cl\u2026                ).build()");
            return build;
        }
        
        public final androidx.window.extensions.embedding.SplitPlaceholderRule translateSplitPlaceholderRuleCompat(final Context context, final SplitPlaceholderRule splitPlaceholderRule, final Class<?> clazz) {
            Intrinsics.checkNotNullParameter((Object)context, "context");
            Intrinsics.checkNotNullParameter((Object)splitPlaceholderRule, "rule");
            Intrinsics.checkNotNullParameter((Object)clazz, "predicateClass");
            final SplitPlaceholderRule$Builder setFinishPrimaryWithSecondary = SplitPlaceholderRule$Builder.class.getConstructor(Intent.class, clazz, clazz, clazz).newInstance(splitPlaceholderRule.getPlaceholderIntent(), this.translateActivityPredicates(splitPlaceholderRule.getFilters()), this.translateIntentPredicates(splitPlaceholderRule.getFilters()), this.translateParentMetricsPredicate(context, splitPlaceholderRule)).setSticky(splitPlaceholderRule.isSticky()).setFinishPrimaryWithSecondary(this.this$0.translateFinishBehavior(splitPlaceholderRule.getFinishPrimaryWithPlaceholder()));
            Intrinsics.checkNotNullExpressionValue((Object)setFinishPrimaryWithSecondary, "SplitPlaceholderRuleBuil\u2026holder)\n                )");
            final androidx.window.extensions.embedding.SplitPlaceholderRule build = this.setDefaultSplitAttributesCompat(setFinishPrimaryWithSecondary, splitPlaceholderRule.getDefaultSplitAttributes()).build();
            Intrinsics.checkNotNullExpressionValue((Object)build, "SplitPlaceholderRuleBuil\u2026\n                .build()");
            return build;
        }
    }
    
    @Metadata(d1 = { "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0082\u0004\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\u000e\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006¨\u0006\u0007" }, d2 = { "Landroidx/window/embedding/EmbeddingAdapter$VendorApiLevel2Impl;", "", "(Landroidx/window/embedding/EmbeddingAdapter;)V", "translateCompat", "Landroidx/window/embedding/SplitInfo;", "splitInfo", "Landroidx/window/extensions/embedding/SplitInfo;", "window_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
    private final class VendorApiLevel2Impl
    {
        final EmbeddingAdapter this$0;
        
        public VendorApiLevel2Impl(final EmbeddingAdapter this$0) {
            this.this$0 = this$0;
        }
        
        public final androidx.window.embedding.SplitInfo translateCompat(final SplitInfo splitInfo) {
            Intrinsics.checkNotNullParameter((Object)splitInfo, "splitInfo");
            final androidx.window.extensions.embedding.ActivityStack primaryActivityStack = splitInfo.getPrimaryActivityStack();
            Intrinsics.checkNotNullExpressionValue((Object)primaryActivityStack, "splitInfo.primaryActivityStack");
            final List activities = primaryActivityStack.getActivities();
            Intrinsics.checkNotNullExpressionValue((Object)activities, "primaryActivityStack.activities");
            final ActivityStack activityStack = new ActivityStack(activities, primaryActivityStack.isEmpty());
            final androidx.window.extensions.embedding.ActivityStack secondaryActivityStack = splitInfo.getSecondaryActivityStack();
            Intrinsics.checkNotNullExpressionValue((Object)secondaryActivityStack, "splitInfo.secondaryActivityStack");
            final List activities2 = secondaryActivityStack.getActivities();
            Intrinsics.checkNotNullExpressionValue((Object)activities2, "secondaryActivityStack.activities");
            final ActivityStack activityStack2 = new ActivityStack(activities2, secondaryActivityStack.isEmpty());
            final EmbeddingAdapter this$0 = this.this$0;
            final SplitAttributes splitAttributes = splitInfo.getSplitAttributes();
            Intrinsics.checkNotNullExpressionValue((Object)splitAttributes, "splitInfo.splitAttributes");
            return new androidx.window.embedding.SplitInfo(activityStack, activityStack2, this$0.translate$window_release(splitAttributes));
        }
    }
}

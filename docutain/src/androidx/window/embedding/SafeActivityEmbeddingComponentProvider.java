// 
// Decompiled by Procyon v0.6.0
// 

package androidx.window.embedding;

import androidx.window.extensions.embedding.ActivityEmbeddingComponent;
import androidx.window.reflection.ReflectionUtils;
import kotlin.jvm.functions.Function0;
import androidx.window.core.ExtensionsUtil;
import kotlin.jvm.internal.Intrinsics;
import androidx.window.extensions.WindowExtensions;
import androidx.window.core.ConsumerAdapter;
import kotlin.Metadata;

@Metadata(d1 = { "\u00006\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\u000b\n\u0002\b\u000b\b\u0000\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007¢\u0006\u0002\u0010\bJ\b\u0010\u0015\u001a\u00020\u0016H\u0002J\b\u0010\u0017\u001a\u00020\u0016H\u0002J\b\u0010\u0018\u001a\u00020\u0016H\u0002J\b\u0010\u0019\u001a\u00020\u0016H\u0002J\b\u0010\u001a\u001a\u00020\u0016H\u0002J\b\u0010\u001b\u001a\u00020\u0016H\u0002J\b\u0010\u001c\u001a\u00020\u0016H\u0002J\b\u0010\u001d\u001a\u00020\u0016H\u0002J\b\u0010\u001e\u001a\u00020\u0016H\u0002J\b\u0010\u001f\u001a\u00020\u0016H\u0002J\b\u0010 \u001a\u00020\u0016H\u0002R\u0013\u0010\t\u001a\u0004\u0018\u00010\n8F¢\u0006\u0006\u001a\u0004\b\u000b\u0010\fR\u0018\u0010\r\u001a\u0006\u0012\u0002\b\u00030\u000e8BX\u0082\u0004¢\u0006\u0006\u001a\u0004\b\u000f\u0010\u0010R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004¢\u0006\u0002\n\u0000R\u0018\u0010\u0011\u001a\u0006\u0012\u0002\b\u00030\u000e8BX\u0082\u0004¢\u0006\u0006\u001a\u0004\b\u0012\u0010\u0010R\u0018\u0010\u0013\u001a\u0006\u0012\u0002\b\u00030\u000e8BX\u0082\u0004¢\u0006\u0006\u001a\u0004\b\u0014\u0010\u0010¨\u0006!" }, d2 = { "Landroidx/window/embedding/SafeActivityEmbeddingComponentProvider;", "", "loader", "Ljava/lang/ClassLoader;", "consumerAdapter", "Landroidx/window/core/ConsumerAdapter;", "windowExtensions", "Landroidx/window/extensions/WindowExtensions;", "(Ljava/lang/ClassLoader;Landroidx/window/core/ConsumerAdapter;Landroidx/window/extensions/WindowExtensions;)V", "activityEmbeddingComponent", "Landroidx/window/extensions/embedding/ActivityEmbeddingComponent;", "getActivityEmbeddingComponent", "()Landroidx/window/extensions/embedding/ActivityEmbeddingComponent;", "activityEmbeddingComponentClass", "Ljava/lang/Class;", "getActivityEmbeddingComponentClass", "()Ljava/lang/Class;", "windowExtensionsClass", "getWindowExtensionsClass", "windowExtensionsProviderClass", "getWindowExtensionsProviderClass", "canUseActivityEmbeddingComponent", "", "hasValidVendorApiLevel1", "hasValidVendorApiLevel2", "isActivityEmbeddingComponentValid", "isMethodClearSplitInfoCallbackValid", "isMethodIsActivityEmbeddedValid", "isMethodSetEmbeddingRulesValid", "isMethodSetSplitInfoCallbackJavaConsumerValid", "isMethodSetSplitInfoCallbackWindowConsumerValid", "isMethodSplitAttributesCalculatorValid", "isWindowExtensionsValid", "window_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
public final class SafeActivityEmbeddingComponentProvider
{
    private final ConsumerAdapter consumerAdapter;
    private final ClassLoader loader;
    private final WindowExtensions windowExtensions;
    
    public SafeActivityEmbeddingComponentProvider(final ClassLoader loader, final ConsumerAdapter consumerAdapter, final WindowExtensions windowExtensions) {
        Intrinsics.checkNotNullParameter((Object)loader, "loader");
        Intrinsics.checkNotNullParameter((Object)consumerAdapter, "consumerAdapter");
        Intrinsics.checkNotNullParameter((Object)windowExtensions, "windowExtensions");
        this.loader = loader;
        this.consumerAdapter = consumerAdapter;
        this.windowExtensions = windowExtensions;
    }
    
    private final boolean canUseActivityEmbeddingComponent() {
        final boolean windowExtensionsValid = this.isWindowExtensionsValid();
        boolean b2;
        final boolean b = b2 = false;
        if (windowExtensionsValid) {
            if (!this.isActivityEmbeddingComponentValid()) {
                b2 = b;
            }
            else {
                final int safeVendorApiLevel = ExtensionsUtil.INSTANCE.getSafeVendorApiLevel();
                boolean b3 = true;
                if (safeVendorApiLevel == 1) {
                    b2 = this.hasValidVendorApiLevel1();
                }
                else {
                    if (2 > safeVendorApiLevel || safeVendorApiLevel > Integer.MAX_VALUE) {
                        b3 = false;
                    }
                    b2 = b;
                    if (b3) {
                        b2 = this.hasValidVendorApiLevel2();
                    }
                }
            }
        }
        return b2;
    }
    
    private final Class<?> getActivityEmbeddingComponentClass() {
        final Class<?> loadClass = this.loader.loadClass("androidx.window.extensions.embedding.ActivityEmbeddingComponent");
        Intrinsics.checkNotNullExpressionValue((Object)loadClass, "loader.loadClass(ACTIVIT\u2026MBEDDING_COMPONENT_CLASS)");
        return loadClass;
    }
    
    private final Class<?> getWindowExtensionsClass() {
        final Class<?> loadClass = this.loader.loadClass("androidx.window.extensions.WindowExtensions");
        Intrinsics.checkNotNullExpressionValue((Object)loadClass, "loader.loadClass(WINDOW_EXTENSIONS_CLASS)");
        return loadClass;
    }
    
    private final Class<?> getWindowExtensionsProviderClass() {
        final Class<?> loadClass = this.loader.loadClass("androidx.window.extensions.WindowExtensionsProvider");
        Intrinsics.checkNotNullExpressionValue((Object)loadClass, "loader.loadClass(WINDOW_EXTENSIONS_PROVIDER_CLASS)");
        return loadClass;
    }
    
    private final boolean hasValidVendorApiLevel1() {
        return this.isMethodSetEmbeddingRulesValid() && this.isMethodIsActivityEmbeddedValid() && this.isMethodSetSplitInfoCallbackJavaConsumerValid();
    }
    
    private final boolean hasValidVendorApiLevel2() {
        return this.hasValidVendorApiLevel1() && this.isMethodSetSplitInfoCallbackWindowConsumerValid() && this.isMethodClearSplitInfoCallbackValid() && this.isMethodSplitAttributesCalculatorValid();
    }
    
    private final boolean isActivityEmbeddingComponentValid() {
        return ReflectionUtils.validateReflection$window_release("WindowExtensions#getActivityEmbeddingComponent is not valid", (Function0<Boolean>)new SafeActivityEmbeddingComponentProvider$isActivityEmbeddingComponentValid.SafeActivityEmbeddingComponentProvider$isActivityEmbeddingComponentValid$1(this));
    }
    
    private final boolean isMethodClearSplitInfoCallbackValid() {
        return ReflectionUtils.validateReflection$window_release("ActivityEmbeddingComponent#clearSplitInfoCallback is not valid", (Function0<Boolean>)new SafeActivityEmbeddingComponentProvider$isMethodClearSplitInfoCallbackValid.SafeActivityEmbeddingComponentProvider$isMethodClearSplitInfoCallbackValid$1(this));
    }
    
    private final boolean isMethodIsActivityEmbeddedValid() {
        return ReflectionUtils.validateReflection$window_release("ActivityEmbeddingComponent#isActivityEmbedded is not valid", (Function0<Boolean>)new SafeActivityEmbeddingComponentProvider$isMethodIsActivityEmbeddedValid.SafeActivityEmbeddingComponentProvider$isMethodIsActivityEmbeddedValid$1(this));
    }
    
    private final boolean isMethodSetEmbeddingRulesValid() {
        return ReflectionUtils.validateReflection$window_release("ActivityEmbeddingComponent#setEmbeddingRules is not valid", (Function0<Boolean>)new SafeActivityEmbeddingComponentProvider$isMethodSetEmbeddingRulesValid.SafeActivityEmbeddingComponentProvider$isMethodSetEmbeddingRulesValid$1(this));
    }
    
    private final boolean isMethodSetSplitInfoCallbackJavaConsumerValid() {
        return ReflectionUtils.validateReflection$window_release("ActivityEmbeddingComponent#setSplitInfoCallback is not valid", (Function0<Boolean>)new SafeActivityEmbeddingComponentProvider$isMethodSetSplitInfoCallbackJavaConsumerValid.SafeActivityEmbeddingComponentProvider$isMethodSetSplitInfoCallbackJavaConsumerValid$1(this));
    }
    
    private final boolean isMethodSetSplitInfoCallbackWindowConsumerValid() {
        return ReflectionUtils.validateReflection$window_release("ActivityEmbeddingComponent#setSplitInfoCallback is not valid", (Function0<Boolean>)new SafeActivityEmbeddingComponentProvider$isMethodSetSplitInfoCallbackWindowConsumerValid.SafeActivityEmbeddingComponentProvider$isMethodSetSplitInfoCallbackWindowConsumerValid$1(this));
    }
    
    private final boolean isMethodSplitAttributesCalculatorValid() {
        return ReflectionUtils.validateReflection$window_release("ActivityEmbeddingComponent#setSplitAttributesCalculator is not valid", (Function0<Boolean>)new SafeActivityEmbeddingComponentProvider$isMethodSplitAttributesCalculatorValid.SafeActivityEmbeddingComponentProvider$isMethodSplitAttributesCalculatorValid$1(this));
    }
    
    private final boolean isWindowExtensionsValid() {
        return ReflectionUtils.validateReflection$window_release("WindowExtensionsProvider#getWindowExtensions is not valid", (Function0<Boolean>)new SafeActivityEmbeddingComponentProvider$isWindowExtensionsValid.SafeActivityEmbeddingComponentProvider$isWindowExtensionsValid$1(this));
    }
    
    public final ActivityEmbeddingComponent getActivityEmbeddingComponent() {
        final boolean canUseActivityEmbeddingComponent = this.canUseActivityEmbeddingComponent();
        ActivityEmbeddingComponent activityEmbeddingComponent = null;
        if (!canUseActivityEmbeddingComponent) {
            return activityEmbeddingComponent;
        }
        try {
            activityEmbeddingComponent = this.windowExtensions.getActivityEmbeddingComponent();
            return activityEmbeddingComponent;
        }
        catch (final UnsupportedOperationException ex) {
            activityEmbeddingComponent = activityEmbeddingComponent;
            return activityEmbeddingComponent;
        }
    }
}

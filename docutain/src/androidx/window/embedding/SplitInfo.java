// 
// Decompiled by Procyon v0.6.0
// 

package androidx.window.embedding;

import android.app.Activity;
import kotlin.jvm.internal.Intrinsics;
import kotlin.Metadata;

@Metadata(d1 = { "\u00004\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\u0018\u00002\u00020\u0001B\u001f\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006¢\u0006\u0002\u0010\u0007J\u0011\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0010H\u0086\u0002J\u0013\u0010\u0011\u001a\u00020\u000e2\b\u0010\u0012\u001a\u0004\u0018\u00010\u0001H\u0096\u0002J\b\u0010\u0013\u001a\u00020\u0014H\u0016J\b\u0010\u0015\u001a\u00020\u0016H\u0016R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010\tR\u0011\u0010\u0004\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\tR\u0011\u0010\u0005\u001a\u00020\u0006¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\f¨\u0006\u0017" }, d2 = { "Landroidx/window/embedding/SplitInfo;", "", "primaryActivityStack", "Landroidx/window/embedding/ActivityStack;", "secondaryActivityStack", "splitAttributes", "Landroidx/window/embedding/SplitAttributes;", "(Landroidx/window/embedding/ActivityStack;Landroidx/window/embedding/ActivityStack;Landroidx/window/embedding/SplitAttributes;)V", "getPrimaryActivityStack", "()Landroidx/window/embedding/ActivityStack;", "getSecondaryActivityStack", "getSplitAttributes", "()Landroidx/window/embedding/SplitAttributes;", "contains", "", "activity", "Landroid/app/Activity;", "equals", "other", "hashCode", "", "toString", "", "window_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
public final class SplitInfo
{
    private final ActivityStack primaryActivityStack;
    private final ActivityStack secondaryActivityStack;
    private final SplitAttributes splitAttributes;
    
    public SplitInfo(final ActivityStack primaryActivityStack, final ActivityStack secondaryActivityStack, final SplitAttributes splitAttributes) {
        Intrinsics.checkNotNullParameter((Object)primaryActivityStack, "primaryActivityStack");
        Intrinsics.checkNotNullParameter((Object)secondaryActivityStack, "secondaryActivityStack");
        Intrinsics.checkNotNullParameter((Object)splitAttributes, "splitAttributes");
        this.primaryActivityStack = primaryActivityStack;
        this.secondaryActivityStack = secondaryActivityStack;
        this.splitAttributes = splitAttributes;
    }
    
    public final boolean contains(final Activity activity) {
        Intrinsics.checkNotNullParameter((Object)activity, "activity");
        return this.primaryActivityStack.contains(activity) || this.secondaryActivityStack.contains(activity);
    }
    
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof SplitInfo)) {
            return false;
        }
        final ActivityStack primaryActivityStack = this.primaryActivityStack;
        final SplitInfo splitInfo = (SplitInfo)o;
        return Intrinsics.areEqual((Object)primaryActivityStack, (Object)splitInfo.primaryActivityStack) && Intrinsics.areEqual((Object)this.secondaryActivityStack, (Object)splitInfo.secondaryActivityStack) && Intrinsics.areEqual((Object)this.splitAttributes, (Object)splitInfo.splitAttributes);
    }
    
    public final ActivityStack getPrimaryActivityStack() {
        return this.primaryActivityStack;
    }
    
    public final ActivityStack getSecondaryActivityStack() {
        return this.secondaryActivityStack;
    }
    
    public final SplitAttributes getSplitAttributes() {
        return this.splitAttributes;
    }
    
    @Override
    public int hashCode() {
        return (this.primaryActivityStack.hashCode() * 31 + this.secondaryActivityStack.hashCode()) * 31 + this.splitAttributes.hashCode();
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("SplitInfo:{");
        final StringBuilder sb2 = new StringBuilder();
        sb2.append("primaryActivityStack=");
        sb2.append(this.primaryActivityStack);
        sb2.append(", ");
        sb.append(sb2.toString());
        final StringBuilder sb3 = new StringBuilder();
        sb3.append("secondaryActivityStack=");
        sb3.append(this.secondaryActivityStack);
        sb3.append(", ");
        sb.append(sb3.toString());
        final StringBuilder sb4 = new StringBuilder();
        sb4.append("splitAttributes=");
        sb4.append(this.splitAttributes);
        sb4.append(", ");
        sb.append(sb4.toString());
        sb.append("}");
        final String string = sb.toString();
        Intrinsics.checkNotNullExpressionValue((Object)string, "StringBuilder().apply(builderAction).toString()");
        return string;
    }
}

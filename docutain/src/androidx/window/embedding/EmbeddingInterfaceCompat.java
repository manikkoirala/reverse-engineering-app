// 
// Decompiled by Procyon v0.6.0
// 

package androidx.window.embedding;

import java.util.List;
import kotlin.jvm.functions.Function1;
import java.util.Set;
import android.app.Activity;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000B\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\"\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\b`\u0018\u00002\u00020\u0001:\u0001\u0015J\b\u0010\u0002\u001a\u00020\u0003H&J\u0010\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0007H&J\b\u0010\b\u001a\u00020\u0005H&J\u0010\u0010\t\u001a\u00020\u00032\u0006\u0010\n\u001a\u00020\u000bH&J\u0016\u0010\f\u001a\u00020\u00032\f\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u000f0\u000eH&J\u001c\u0010\u0010\u001a\u00020\u00032\u0012\u0010\u0011\u001a\u000e\u0012\u0004\u0012\u00020\u0013\u0012\u0004\u0012\u00020\u00140\u0012H'\u00f8\u0001\u0000\u0082\u0002\u0006\n\u0004\b!0\u0001¨\u0006\u0016\u00c0\u0006\u0001" }, d2 = { "Landroidx/window/embedding/EmbeddingInterfaceCompat;", "", "clearSplitAttributesCalculator", "", "isActivityEmbedded", "", "activity", "Landroid/app/Activity;", "isSplitAttributesCalculatorSupported", "setEmbeddingCallback", "embeddingCallback", "Landroidx/window/embedding/EmbeddingInterfaceCompat$EmbeddingCallbackInterface;", "setRules", "rules", "", "Landroidx/window/embedding/EmbeddingRule;", "setSplitAttributesCalculator", "calculator", "Lkotlin/Function1;", "Landroidx/window/embedding/SplitAttributesCalculatorParams;", "Landroidx/window/embedding/SplitAttributes;", "EmbeddingCallbackInterface", "window_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
public interface EmbeddingInterfaceCompat
{
    void clearSplitAttributesCalculator();
    
    boolean isActivityEmbedded(final Activity p0);
    
    boolean isSplitAttributesCalculatorSupported();
    
    void setEmbeddingCallback(final EmbeddingCallbackInterface p0);
    
    void setRules(final Set<? extends EmbeddingRule> p0);
    
    void setSplitAttributesCalculator(final Function1<? super SplitAttributesCalculatorParams, SplitAttributes> p0);
    
    @Metadata(d1 = { "\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\bf\u0018\u00002\u00020\u0001J\u0016\u0010\u0002\u001a\u00020\u00032\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005H&\u00f8\u0001\u0000\u0082\u0002\u0006\n\u0004\b!0\u0001¨\u0006\u0007\u00c0\u0006\u0001" }, d2 = { "Landroidx/window/embedding/EmbeddingInterfaceCompat$EmbeddingCallbackInterface;", "", "onSplitInfoChanged", "", "splitInfo", "", "Landroidx/window/embedding/SplitInfo;", "window_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
    public interface EmbeddingCallbackInterface
    {
        void onSplitInfoChanged(final List<SplitInfo> p0);
    }
}

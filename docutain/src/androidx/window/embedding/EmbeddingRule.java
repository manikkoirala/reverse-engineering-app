// 
// Decompiled by Procyon v0.6.0
// 

package androidx.window.embedding;

import kotlin.jvm.internal.Intrinsics;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\b&\u0018\u00002\u00020\u0001B\u0011\b\u0000\u0012\b\u0010\u0002\u001a\u0004\u0018\u00010\u0003¢\u0006\u0002\u0010\u0004J\u0013\u0010\u0007\u001a\u00020\b2\b\u0010\t\u001a\u0004\u0018\u00010\u0001H\u0096\u0002J\b\u0010\n\u001a\u00020\u000bH\u0016R\u0013\u0010\u0002\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006¨\u0006\f" }, d2 = { "Landroidx/window/embedding/EmbeddingRule;", "", "tag", "", "(Ljava/lang/String;)V", "getTag", "()Ljava/lang/String;", "equals", "", "other", "hashCode", "", "window_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
public abstract class EmbeddingRule
{
    private final String tag;
    
    public EmbeddingRule(final String tag) {
        this.tag = tag;
    }
    
    @Override
    public boolean equals(final Object o) {
        return this == o || (o instanceof EmbeddingRule && Intrinsics.areEqual((Object)this.tag, (Object)((EmbeddingRule)o).tag));
    }
    
    public final String getTag() {
        return this.tag;
    }
    
    @Override
    public int hashCode() {
        final String tag = this.tag;
        int hashCode;
        if (tag != null) {
            hashCode = tag.hashCode();
        }
        else {
            hashCode = 0;
        }
        return hashCode;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.window.core;

import kotlin.jvm.internal.Intrinsics;
import android.graphics.Rect;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\t\n\u0002\u0010\u000b\n\u0002\b\f\n\u0002\u0010\u000e\n\u0000\b\u0000\u0018\u00002\u00020\u0001B\u000f\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004B%\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0006\u0012\u0006\u0010\b\u001a\u00020\u0006\u0012\u0006\u0010\t\u001a\u00020\u0006¢\u0006\u0002\u0010\nJ\u0013\u0010\u0018\u001a\u00020\u00102\b\u0010\u0019\u001a\u0004\u0018\u00010\u0001H\u0096\u0002J\b\u0010\u001a\u001a\u00020\u0006H\u0016J\u0006\u0010\u001b\u001a\u00020\u0003J\b\u0010\u001c\u001a\u00020\u001dH\u0016R\u0011\u0010\t\u001a\u00020\u0006¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0011\u0010\r\u001a\u00020\u00068F¢\u0006\u0006\u001a\u0004\b\u000e\u0010\fR\u0011\u0010\u000f\u001a\u00020\u00108F¢\u0006\u0006\u001a\u0004\b\u000f\u0010\u0011R\u0011\u0010\u0012\u001a\u00020\u00108F¢\u0006\u0006\u001a\u0004\b\u0012\u0010\u0011R\u0011\u0010\u0005\u001a\u00020\u0006¢\u0006\b\n\u0000\u001a\u0004\b\u0013\u0010\fR\u0011\u0010\b\u001a\u00020\u0006¢\u0006\b\n\u0000\u001a\u0004\b\u0014\u0010\fR\u0011\u0010\u0007\u001a\u00020\u0006¢\u0006\b\n\u0000\u001a\u0004\b\u0015\u0010\fR\u0011\u0010\u0016\u001a\u00020\u00068F¢\u0006\u0006\u001a\u0004\b\u0017\u0010\f¨\u0006\u001e" }, d2 = { "Landroidx/window/core/Bounds;", "", "rect", "Landroid/graphics/Rect;", "(Landroid/graphics/Rect;)V", "left", "", "top", "right", "bottom", "(IIII)V", "getBottom", "()I", "height", "getHeight", "isEmpty", "", "()Z", "isZero", "getLeft", "getRight", "getTop", "width", "getWidth", "equals", "other", "hashCode", "toRect", "toString", "", "window_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
public final class Bounds
{
    private final int bottom;
    private final int left;
    private final int right;
    private final int top;
    
    public Bounds(int n, final int n2, final int n3, final int n4) {
        this.left = n;
        this.top = n2;
        this.right = n3;
        this.bottom = n4;
        final int n5 = 1;
        if (n > n3) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Left must be less than or equal to right, left: ");
            sb.append(n);
            sb.append(", right: ");
            sb.append(n3);
            throw new IllegalArgumentException(sb.toString().toString());
        }
        if (n2 <= n4) {
            n = n5;
        }
        else {
            n = 0;
        }
        if (n != 0) {
            return;
        }
        final StringBuilder sb2 = new StringBuilder();
        sb2.append("top must be less than or equal to bottom, top: ");
        sb2.append(n2);
        sb2.append(", bottom: ");
        sb2.append(n4);
        throw new IllegalArgumentException(sb2.toString().toString());
    }
    
    public Bounds(final Rect rect) {
        Intrinsics.checkNotNullParameter((Object)rect, "rect");
        this(rect.left, rect.top, rect.right, rect.bottom);
    }
    
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        final Class<? extends Bounds> class1 = this.getClass();
        Class<?> class2;
        if (o != null) {
            class2 = o.getClass();
        }
        else {
            class2 = null;
        }
        if (!Intrinsics.areEqual((Object)class1, (Object)class2)) {
            return false;
        }
        Intrinsics.checkNotNull(o, "null cannot be cast to non-null type androidx.window.core.Bounds");
        final Bounds bounds = (Bounds)o;
        return this.left == bounds.left && this.top == bounds.top && this.right == bounds.right && this.bottom == bounds.bottom;
    }
    
    public final int getBottom() {
        return this.bottom;
    }
    
    public final int getHeight() {
        return this.bottom - this.top;
    }
    
    public final int getLeft() {
        return this.left;
    }
    
    public final int getRight() {
        return this.right;
    }
    
    public final int getTop() {
        return this.top;
    }
    
    public final int getWidth() {
        return this.right - this.left;
    }
    
    @Override
    public int hashCode() {
        return ((this.left * 31 + this.top) * 31 + this.right) * 31 + this.bottom;
    }
    
    public final boolean isEmpty() {
        return this.getHeight() == 0 || this.getWidth() == 0;
    }
    
    public final boolean isZero() {
        return this.getHeight() == 0 && this.getWidth() == 0;
    }
    
    public final Rect toRect() {
        return new Rect(this.left, this.top, this.right, this.bottom);
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("Bounds");
        sb.append(" { [");
        sb.append(this.left);
        sb.append(',');
        sb.append(this.top);
        sb.append(',');
        sb.append(this.right);
        sb.append(',');
        sb.append(this.bottom);
        sb.append("] }");
        return sb.toString();
    }
}

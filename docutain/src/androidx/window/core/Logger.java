// 
// Decompiled by Procyon v0.6.0
// 

package androidx.window.core;

import kotlin.Metadata;

@Metadata(d1 = { "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\b`\u0018\u00002\u00020\u0001J\u0018\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0005H&\u00f8\u0001\u0000\u0082\u0002\u0006\n\u0004\b!0\u0001¨\u0006\u0007\u00c0\u0006\u0001" }, d2 = { "Landroidx/window/core/Logger;", "", "debug", "", "tag", "", "message", "window_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
public interface Logger
{
    void debug(final String p0, final String p1);
}

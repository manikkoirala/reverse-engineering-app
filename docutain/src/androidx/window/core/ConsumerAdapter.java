// 
// Decompiled by Procyon v0.6.0
// 

package androidx.window.core;

import kotlin.reflect.KClasses;
import java.lang.reflect.Method;
import android.content.Context;
import android.app.Activity;
import java.lang.reflect.Proxy;
import java.lang.reflect.InvocationHandler;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.reflect.KClass;
import kotlin.jvm.internal.Intrinsics;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000L\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0001\u0018\u00002\u00020\u0001:\u0002\u001e\u001fB\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004JB\u0010\u0005\u001a\u00020\u0006\"\b\b\u0000\u0010\u0007*\u00020\u00012\u0006\u0010\b\u001a\u00020\u00012\f\u0010\t\u001a\b\u0012\u0004\u0012\u0002H\u00070\n2\u0006\u0010\u000b\u001a\u00020\f2\u0012\u0010\r\u001a\u000e\u0012\u0004\u0012\u0002H\u0007\u0012\u0004\u0012\u00020\u00060\u000eJ4\u0010\u000f\u001a\u00020\u0001\"\b\b\u0000\u0010\u0007*\u00020\u00012\f\u0010\t\u001a\b\u0012\u0004\u0012\u0002H\u00070\n2\u0012\u0010\r\u001a\u000e\u0012\u0004\u0012\u0002H\u0007\u0012\u0004\u0012\u00020\u00060\u000eH\u0002J\u0013\u0010\u0010\u001a\b\u0012\u0002\b\u0003\u0018\u00010\u0011H\u0000¢\u0006\u0002\b\u0012JJ\u0010\u0013\u001a\u00020\u0006\"\b\b\u0000\u0010\u0007*\u00020\u00012\u0006\u0010\b\u001a\u00020\u00012\f\u0010\t\u001a\b\u0012\u0004\u0012\u0002H\u00070\n2\u0006\u0010\u0014\u001a\u00020\f2\u0006\u0010\u0015\u001a\u00020\u00162\u0012\u0010\r\u001a\u000e\u0012\u0004\u0012\u0002H\u0007\u0012\u0004\u0012\u00020\u00060\u000eJT\u0010\u0017\u001a\u00020\u0018\"\b\b\u0000\u0010\u0007*\u00020\u00012\u0006\u0010\b\u001a\u00020\u00012\f\u0010\t\u001a\b\u0012\u0004\u0012\u0002H\u00070\n2\u0006\u0010\u0014\u001a\u00020\f2\u0006\u0010\u0019\u001a\u00020\f2\u0006\u0010\u0015\u001a\u00020\u00162\u0012\u0010\r\u001a\u000e\u0012\u0004\u0012\u0002H\u0007\u0012\u0004\u0012\u00020\u00060\u000eH\u0007JT\u0010\u0017\u001a\u00020\u0018\"\b\b\u0000\u0010\u0007*\u00020\u00012\u0006\u0010\b\u001a\u00020\u00012\f\u0010\t\u001a\b\u0012\u0004\u0012\u0002H\u00070\n2\u0006\u0010\u0014\u001a\u00020\f2\u0006\u0010\u0019\u001a\u00020\f2\u0006\u0010\u001a\u001a\u00020\u001b2\u0012\u0010\r\u001a\u000e\u0012\u0004\u0012\u0002H\u0007\u0012\u0004\u0012\u00020\u00060\u000eH\u0007JL\u0010\u001c\u001a\u00020\u0018\"\b\b\u0000\u0010\u0007*\u00020\u00012\u0006\u0010\b\u001a\u00020\u00012\f\u0010\t\u001a\b\u0012\u0004\u0012\u0002H\u00070\n2\u0006\u0010\u0014\u001a\u00020\f2\u0006\u0010\u0019\u001a\u00020\f2\u0012\u0010\r\u001a\u000e\u0012\u0004\u0012\u0002H\u0007\u0012\u0004\u0012\u00020\u00060\u000eH\u0007J\f\u0010\u001d\u001a\u0006\u0012\u0002\b\u00030\u0011H\u0002R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006 " }, d2 = { "Landroidx/window/core/ConsumerAdapter;", "", "loader", "Ljava/lang/ClassLoader;", "(Ljava/lang/ClassLoader;)V", "addConsumer", "", "T", "obj", "clazz", "Lkotlin/reflect/KClass;", "methodName", "", "consumer", "Lkotlin/Function1;", "buildConsumer", "consumerClassOrNull", "Ljava/lang/Class;", "consumerClassOrNull$window_release", "createConsumer", "addMethodName", "activity", "Landroid/app/Activity;", "createSubscription", "Landroidx/window/core/ConsumerAdapter$Subscription;", "removeMethodName", "context", "Landroid/content/Context;", "createSubscriptionNoActivity", "unsafeConsumerClass", "ConsumerHandler", "Subscription", "window_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
public final class ConsumerAdapter
{
    private final ClassLoader loader;
    
    public ConsumerAdapter(final ClassLoader loader) {
        Intrinsics.checkNotNullParameter((Object)loader, "loader");
        this.loader = loader;
    }
    
    private final <T> Object buildConsumer(final KClass<T> kClass, final Function1<? super T, Unit> function1) {
        final Object proxyInstance = Proxy.newProxyInstance(this.loader, new Class[] { this.unsafeConsumerClass() }, new ConsumerHandler<Object>(kClass, function1));
        Intrinsics.checkNotNullExpressionValue(proxyInstance, "newProxyInstance(loader,\u2026onsumerClass()), handler)");
        return proxyInstance;
    }
    
    private final Class<?> unsafeConsumerClass() {
        final Class<?> loadClass = this.loader.loadClass("java.util.function.Consumer");
        Intrinsics.checkNotNullExpressionValue((Object)loadClass, "loader.loadClass(\"java.util.function.Consumer\")");
        return loadClass;
    }
    
    public final <T> void addConsumer(final Object obj, final KClass<T> kClass, final String name, final Function1<? super T, Unit> function1) {
        Intrinsics.checkNotNullParameter(obj, "obj");
        Intrinsics.checkNotNullParameter((Object)kClass, "clazz");
        Intrinsics.checkNotNullParameter((Object)name, "methodName");
        Intrinsics.checkNotNullParameter((Object)function1, "consumer");
        obj.getClass().getMethod(name, this.unsafeConsumerClass()).invoke(obj, this.buildConsumer((kotlin.reflect.KClass<Object>)kClass, (kotlin.jvm.functions.Function1<? super Object, Unit>)function1));
    }
    
    public final Class<?> consumerClassOrNull$window_release() {
        Class<?> unsafeConsumerClass;
        try {
            unsafeConsumerClass = this.unsafeConsumerClass();
        }
        catch (final ClassNotFoundException ex) {
            unsafeConsumerClass = null;
        }
        return unsafeConsumerClass;
    }
    
    public final <T> void createConsumer(final Object obj, final KClass<T> kClass, final String name, final Activity activity, final Function1<? super T, Unit> function1) {
        Intrinsics.checkNotNullParameter(obj, "obj");
        Intrinsics.checkNotNullParameter((Object)kClass, "clazz");
        Intrinsics.checkNotNullParameter((Object)name, "addMethodName");
        Intrinsics.checkNotNullParameter((Object)activity, "activity");
        Intrinsics.checkNotNullParameter((Object)function1, "consumer");
        obj.getClass().getMethod(name, Activity.class, this.unsafeConsumerClass()).invoke(obj, activity, this.buildConsumer((kotlin.reflect.KClass<Object>)kClass, (kotlin.jvm.functions.Function1<? super Object, Unit>)function1));
    }
    
    public final <T> Subscription createSubscription(final Object obj, final KClass<T> kClass, final String name, final String name2, final Activity activity, final Function1<? super T, Unit> function1) {
        Intrinsics.checkNotNullParameter(obj, "obj");
        Intrinsics.checkNotNullParameter((Object)kClass, "clazz");
        Intrinsics.checkNotNullParameter((Object)name, "addMethodName");
        Intrinsics.checkNotNullParameter((Object)name2, "removeMethodName");
        Intrinsics.checkNotNullParameter((Object)activity, "activity");
        Intrinsics.checkNotNullParameter((Object)function1, "consumer");
        final Object buildConsumer = this.buildConsumer((kotlin.reflect.KClass<Object>)kClass, (kotlin.jvm.functions.Function1<? super Object, Unit>)function1);
        obj.getClass().getMethod(name, Activity.class, this.unsafeConsumerClass()).invoke(obj, activity, buildConsumer);
        return (Subscription)new ConsumerAdapter$createSubscription.ConsumerAdapter$createSubscription$1(obj.getClass().getMethod(name2, this.unsafeConsumerClass()), obj, buildConsumer);
    }
    
    public final <T> Subscription createSubscription(final Object obj, final KClass<T> kClass, final String name, final String name2, final Context context, final Function1<? super T, Unit> function1) {
        Intrinsics.checkNotNullParameter(obj, "obj");
        Intrinsics.checkNotNullParameter((Object)kClass, "clazz");
        Intrinsics.checkNotNullParameter((Object)name, "addMethodName");
        Intrinsics.checkNotNullParameter((Object)name2, "removeMethodName");
        Intrinsics.checkNotNullParameter((Object)context, "context");
        Intrinsics.checkNotNullParameter((Object)function1, "consumer");
        final Object buildConsumer = this.buildConsumer((kotlin.reflect.KClass<Object>)kClass, (kotlin.jvm.functions.Function1<? super Object, Unit>)function1);
        obj.getClass().getMethod(name, Context.class, this.unsafeConsumerClass()).invoke(obj, context, buildConsumer);
        return (Subscription)new ConsumerAdapter$createSubscription.ConsumerAdapter$createSubscription$2(obj.getClass().getMethod(name2, this.unsafeConsumerClass()), obj, buildConsumer);
    }
    
    public final <T> Subscription createSubscriptionNoActivity(final Object obj, final KClass<T> kClass, final String name, final String name2, final Function1<? super T, Unit> function1) {
        Intrinsics.checkNotNullParameter(obj, "obj");
        Intrinsics.checkNotNullParameter((Object)kClass, "clazz");
        Intrinsics.checkNotNullParameter((Object)name, "addMethodName");
        Intrinsics.checkNotNullParameter((Object)name2, "removeMethodName");
        Intrinsics.checkNotNullParameter((Object)function1, "consumer");
        final Object buildConsumer = this.buildConsumer((kotlin.reflect.KClass<Object>)kClass, (kotlin.jvm.functions.Function1<? super Object, Unit>)function1);
        obj.getClass().getMethod(name, this.unsafeConsumerClass()).invoke(obj, buildConsumer);
        return (Subscription)new ConsumerAdapter$createSubscriptionNoActivity.ConsumerAdapter$createSubscriptionNoActivity$1(obj.getClass().getMethod(name2, this.unsafeConsumerClass()), obj, buildConsumer);
    }
    
    @Metadata(d1 = { "\u00008\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0011\n\u0002\b\u0005\n\u0002\u0010\u000b\n\u0002\b\u0006\b\u0002\u0018\u0000*\b\b\u0000\u0010\u0001*\u00020\u00022\u00020\u0003B'\u0012\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00028\u00000\u0005\u0012\u0012\u0010\u0006\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00020\b0\u0007¢\u0006\u0002\u0010\tJ0\u0010\n\u001a\u00020\u00022\u0006\u0010\u000b\u001a\u00020\u00022\u0006\u0010\f\u001a\u00020\r2\u0010\u0010\u000e\u001a\f\u0012\u0006\b\u0001\u0012\u00020\u0002\u0018\u00010\u000fH\u0096\u0002¢\u0006\u0002\u0010\u0010J\u0013\u0010\u0011\u001a\u00020\b2\u0006\u0010\u0012\u001a\u00028\u0000¢\u0006\u0002\u0010\u0013J#\u0010\u0014\u001a\u00020\u0015*\u00020\r2\u0010\u0010\u0016\u001a\f\u0012\u0006\b\u0001\u0012\u00020\u0002\u0018\u00010\u000fH\u0002¢\u0006\u0002\u0010\u0017J#\u0010\u0018\u001a\u00020\u0015*\u00020\r2\u0010\u0010\u0016\u001a\f\u0012\u0006\b\u0001\u0012\u00020\u0002\u0018\u00010\u000fH\u0002¢\u0006\u0002\u0010\u0017J#\u0010\u0019\u001a\u00020\u0015*\u00020\r2\u0010\u0010\u0016\u001a\f\u0012\u0006\b\u0001\u0012\u00020\u0002\u0018\u00010\u000fH\u0002¢\u0006\u0002\u0010\u0017J#\u0010\u001a\u001a\u00020\u0015*\u00020\r2\u0010\u0010\u0016\u001a\f\u0012\u0006\b\u0001\u0012\u00020\u0002\u0018\u00010\u000fH\u0002¢\u0006\u0002\u0010\u0017R\u0014\u0010\u0004\u001a\b\u0012\u0004\u0012\u00028\u00000\u0005X\u0082\u0004¢\u0006\u0002\n\u0000R\u001a\u0010\u0006\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00020\b0\u0007X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u001b" }, d2 = { "Landroidx/window/core/ConsumerAdapter$ConsumerHandler;", "T", "", "Ljava/lang/reflect/InvocationHandler;", "clazz", "Lkotlin/reflect/KClass;", "consumer", "Lkotlin/Function1;", "", "(Lkotlin/reflect/KClass;Lkotlin/jvm/functions/Function1;)V", "invoke", "obj", "method", "Ljava/lang/reflect/Method;", "parameters", "", "(Ljava/lang/Object;Ljava/lang/reflect/Method;[Ljava/lang/Object;)Ljava/lang/Object;", "invokeAccept", "parameter", "(Ljava/lang/Object;)V", "isAccept", "", "args", "(Ljava/lang/reflect/Method;[Ljava/lang/Object;)Z", "isEquals", "isHashCode", "isToString", "window_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
    private static final class ConsumerHandler<T> implements InvocationHandler
    {
        private final KClass<T> clazz;
        private final Function1<T, Unit> consumer;
        
        public ConsumerHandler(final KClass<T> clazz, final Function1<? super T, Unit> consumer) {
            Intrinsics.checkNotNullParameter((Object)clazz, "clazz");
            Intrinsics.checkNotNullParameter((Object)consumer, "consumer");
            this.clazz = clazz;
            this.consumer = (Function1<T, Unit>)consumer;
        }
        
        private final boolean isAccept(final Method method, final Object[] array) {
            final boolean equal = Intrinsics.areEqual((Object)method.getName(), (Object)"accept");
            boolean b = true;
            if (!equal || (array == null || array.length != 1)) {
                b = false;
            }
            return b;
        }
        
        private final boolean isEquals(final Method method, final Object[] array) {
            final boolean equal = Intrinsics.areEqual((Object)method.getName(), (Object)"equals");
            boolean b = true;
            if (!equal || !method.getReturnType().equals(Boolean.TYPE) || (array == null || array.length != 1)) {
                b = false;
            }
            return b;
        }
        
        private final boolean isHashCode(final Method method, final Object[] array) {
            return Intrinsics.areEqual((Object)method.getName(), (Object)"hashCode") && method.getReturnType().equals(Integer.TYPE) && array == null;
        }
        
        private final boolean isToString(final Method method, final Object[] array) {
            return Intrinsics.areEqual((Object)method.getName(), (Object)"toString") && method.getReturnType().equals(String.class) && array == null;
        }
        
        @Override
        public Object invoke(Object obj, final Method obj2, final Object[] obj3) {
            Intrinsics.checkNotNullParameter(obj, "obj");
            Intrinsics.checkNotNullParameter((Object)obj2, "method");
            final boolean accept = this.isAccept(obj2, obj3);
            final Object o = null;
            final Object o2 = null;
            boolean b = false;
            Object o3;
            if (accept) {
                final KClass<T> clazz = this.clazz;
                obj = o2;
                if (obj3 != null) {
                    obj = obj3[0];
                }
                this.invokeAccept(KClasses.cast((KClass)clazz, obj));
                o3 = Unit.INSTANCE;
            }
            else if (this.isEquals(obj2, obj3)) {
                Object o4 = o;
                if (obj3 != null) {
                    o4 = obj3[0];
                }
                if (obj == o4) {
                    b = true;
                }
                o3 = b;
            }
            else if (this.isHashCode(obj2, obj3)) {
                o3 = this.consumer.hashCode();
            }
            else {
                if (!this.isToString(obj2, obj3)) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Unexpected method call object:");
                    sb.append(obj);
                    sb.append(", method: ");
                    sb.append(obj2);
                    sb.append(", args: ");
                    sb.append(obj3);
                    throw new UnsupportedOperationException(sb.toString());
                }
                o3 = this.consumer.toString();
            }
            return o3;
        }
        
        public final void invokeAccept(final T t) {
            Intrinsics.checkNotNullParameter((Object)t, "parameter");
            this.consumer.invoke((Object)t);
        }
    }
    
    @Metadata(d1 = { "\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\b`\u0018\u00002\u00020\u0001J\b\u0010\u0002\u001a\u00020\u0003H&\u00f8\u0001\u0000\u0082\u0002\u0006\n\u0004\b!0\u0001¨\u0006\u0004\u00c0\u0006\u0001" }, d2 = { "Landroidx/window/core/ConsumerAdapter$Subscription;", "", "dispose", "", "window_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
    public interface Subscription
    {
        void dispose();
    }
}

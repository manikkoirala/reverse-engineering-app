// 
// Decompiled by Procyon v0.6.0
// 

package androidx.window.core;

import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import kotlin.Metadata;

@Metadata(d1 = { "\u00004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u000e\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\u0018\u0002\n\u0000\b\u0002\u0018\u0000*\b\b\u0000\u0010\u0001*\u00020\u00022\b\u0012\u0004\u0012\u0002H\u00010\u0003B%\u0012\u0006\u0010\u0004\u001a\u00028\u0000\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\b\u0012\u0006\u0010\t\u001a\u00020\n¢\u0006\u0002\u0010\u000bJ\r\u0010\u0015\u001a\u00028\u0000H\u0016¢\u0006\u0002\u0010\u0011J/\u0010\u0016\u001a\b\u0012\u0004\u0012\u00028\u00000\u00032\u0006\u0010\u0017\u001a\u00020\u00062\u0017\u0010\u0018\u001a\u0013\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00020\u001a0\u0019¢\u0006\u0002\b\u001bH\u0016R\u0011\u0010\t\u001a\u00020\n¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\rR\u0011\u0010\u0005\u001a\u00020\u0006¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\u000fR\u0013\u0010\u0004\u001a\u00028\u0000¢\u0006\n\n\u0002\u0010\u0012\u001a\u0004\b\u0010\u0010\u0011R\u0011\u0010\u0007\u001a\u00020\b¢\u0006\b\n\u0000\u001a\u0004\b\u0013\u0010\u0014¨\u0006\u001c" }, d2 = { "Landroidx/window/core/ValidSpecification;", "T", "", "Landroidx/window/core/SpecificationComputer;", "value", "tag", "", "verificationMode", "Landroidx/window/core/VerificationMode;", "logger", "Landroidx/window/core/Logger;", "(Ljava/lang/Object;Ljava/lang/String;Landroidx/window/core/VerificationMode;Landroidx/window/core/Logger;)V", "getLogger", "()Landroidx/window/core/Logger;", "getTag", "()Ljava/lang/String;", "getValue", "()Ljava/lang/Object;", "Ljava/lang/Object;", "getVerificationMode", "()Landroidx/window/core/VerificationMode;", "compute", "require", "message", "condition", "Lkotlin/Function1;", "", "Lkotlin/ExtensionFunctionType;", "window_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
final class ValidSpecification<T> extends SpecificationComputer<T>
{
    private final Logger logger;
    private final String tag;
    private final T value;
    private final VerificationMode verificationMode;
    
    public ValidSpecification(final T value, final String tag, final VerificationMode verificationMode, final Logger logger) {
        Intrinsics.checkNotNullParameter((Object)value, "value");
        Intrinsics.checkNotNullParameter((Object)tag, "tag");
        Intrinsics.checkNotNullParameter((Object)verificationMode, "verificationMode");
        Intrinsics.checkNotNullParameter((Object)logger, "logger");
        this.value = value;
        this.tag = tag;
        this.verificationMode = verificationMode;
        this.logger = logger;
    }
    
    @Override
    public T compute() {
        return this.value;
    }
    
    public final Logger getLogger() {
        return this.logger;
    }
    
    public final String getTag() {
        return this.tag;
    }
    
    public final T getValue() {
        return this.value;
    }
    
    public final VerificationMode getVerificationMode() {
        return this.verificationMode;
    }
    
    @Override
    public SpecificationComputer<T> require(final String s, final Function1<? super T, Boolean> function1) {
        Intrinsics.checkNotNullParameter((Object)s, "message");
        Intrinsics.checkNotNullParameter((Object)function1, "condition");
        SpecificationComputer specificationComputer;
        if (function1.invoke((Object)this.value)) {
            specificationComputer = this;
        }
        else {
            specificationComputer = new FailedSpecification(this.value, this.tag, s, this.logger, this.verificationMode);
        }
        return specificationComputer;
    }
}

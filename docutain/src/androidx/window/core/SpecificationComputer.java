// 
// Decompiled by Procyon v0.6.0
// 

package androidx.window.core;

import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000&\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0004\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\u0018\u0002\n\u0002\b\u0002\b \u0018\u0000 \u000f*\b\b\u0000\u0010\u0001*\u00020\u00022\u00020\u0002:\u0001\u000fB\u0005¢\u0006\u0002\u0010\u0003J\u000f\u0010\u0004\u001a\u0004\u0018\u00018\u0000H&¢\u0006\u0002\u0010\u0005J\u0018\u0010\u0006\u001a\u00020\u00072\u0006\u0010\b\u001a\u00020\u00022\u0006\u0010\t\u001a\u00020\u0007H\u0004J/\u0010\n\u001a\b\u0012\u0004\u0012\u00028\u00000\u00002\u0006\u0010\t\u001a\u00020\u00072\u0017\u0010\u000b\u001a\u0013\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00020\r0\f¢\u0006\u0002\b\u000eH&¨\u0006\u0010" }, d2 = { "Landroidx/window/core/SpecificationComputer;", "T", "", "()V", "compute", "()Ljava/lang/Object;", "createMessage", "", "value", "message", "require", "condition", "Lkotlin/Function1;", "", "Lkotlin/ExtensionFunctionType;", "Companion", "window_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
public abstract class SpecificationComputer<T>
{
    public static final Companion Companion;
    
    static {
        Companion = new Companion(null);
    }
    
    public abstract T compute();
    
    protected final String createMessage(final Object obj, final String str) {
        Intrinsics.checkNotNullParameter(obj, "value");
        Intrinsics.checkNotNullParameter((Object)str, "message");
        final StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append(" value: ");
        sb.append(obj);
        return sb.toString();
    }
    
    public abstract SpecificationComputer<T> require(final String p0, final Function1<? super T, Boolean> p1);
    
    @Metadata(d1 = { "\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J;\u0010\u0003\u001a\b\u0012\u0004\u0012\u0002H\u00050\u0004\"\b\b\u0001\u0010\u0005*\u00020\u0001*\u0002H\u00052\u0006\u0010\u0006\u001a\u00020\u00072\b\b\u0002\u0010\b\u001a\u00020\t2\b\b\u0002\u0010\n\u001a\u00020\u000b¢\u0006\u0002\u0010\f¨\u0006\r" }, d2 = { "Landroidx/window/core/SpecificationComputer$Companion;", "", "()V", "startSpecification", "Landroidx/window/core/SpecificationComputer;", "T", "tag", "", "verificationMode", "Landroidx/window/core/VerificationMode;", "logger", "Landroidx/window/core/Logger;", "(Ljava/lang/Object;Ljava/lang/String;Landroidx/window/core/VerificationMode;Landroidx/window/core/Logger;)Landroidx/window/core/SpecificationComputer;", "window_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
    public static final class Companion
    {
        private Companion() {
        }
        
        public final <T> SpecificationComputer<T> startSpecification(final T t, final String s, final VerificationMode verificationMode, final Logger logger) {
            Intrinsics.checkNotNullParameter((Object)t, "<this>");
            Intrinsics.checkNotNullParameter((Object)s, "tag");
            Intrinsics.checkNotNullParameter((Object)verificationMode, "verificationMode");
            Intrinsics.checkNotNullParameter((Object)logger, "logger");
            return new ValidSpecification<T>(t, s, verificationMode, logger);
        }
    }
}

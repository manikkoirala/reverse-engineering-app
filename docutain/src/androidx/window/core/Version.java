// 
// Decompiled by Procyon v0.6.0
// 

package androidx.window.core;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import kotlin.text.StringsKt;
import kotlin.jvm.JvmStatic;
import kotlin.jvm.internal.Intrinsics;
import java.math.BigInteger;
import kotlin.LazyKt;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.Lazy;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u000f\n\u0000\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\r\n\u0002\u0010\u000b\n\u0002\u0010\u0000\n\u0002\b\u0004\b\u0000\u0018\u0000 \u001c2\b\u0012\u0004\u0012\u00020\u00000\u0001:\u0001\u001cB'\b\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0003\u0012\u0006\u0010\u0006\u001a\u00020\u0007¢\u0006\u0002\u0010\bJ\u0011\u0010\u0015\u001a\u00020\u00032\u0006\u0010\u0016\u001a\u00020\u0000H\u0096\u0002J\u0013\u0010\u0017\u001a\u00020\u00182\b\u0010\u0016\u001a\u0004\u0018\u00010\u0019H\u0096\u0002J\b\u0010\u001a\u001a\u00020\u0003H\u0016J\b\u0010\u001b\u001a\u00020\u0007H\u0016R\u001b\u0010\t\u001a\u00020\n8BX\u0082\u0084\u0002¢\u0006\f\n\u0004\b\r\u0010\u000e\u001a\u0004\b\u000b\u0010\fR\u0011\u0010\u0006\u001a\u00020\u0007¢\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u0010R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\u0012R\u0011\u0010\u0004\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0013\u0010\u0012R\u0011\u0010\u0005\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0014\u0010\u0012¨\u0006\u001d" }, d2 = { "Landroidx/window/core/Version;", "", "major", "", "minor", "patch", "description", "", "(IIILjava/lang/String;)V", "bigInteger", "Ljava/math/BigInteger;", "getBigInteger", "()Ljava/math/BigInteger;", "bigInteger$delegate", "Lkotlin/Lazy;", "getDescription", "()Ljava/lang/String;", "getMajor", "()I", "getMinor", "getPatch", "compareTo", "other", "equals", "", "", "hashCode", "toString", "Companion", "window_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
public final class Version implements Comparable<Version>
{
    private static final Version CURRENT;
    public static final Companion Companion;
    private static final Version UNKNOWN;
    private static final Version VERSION_0_1;
    private static final Version VERSION_1_0;
    private static final String VERSION_PATTERN_STRING = "(\\d+)(?:\\.(\\d+))(?:\\.(\\d+))(?:-(.+))?";
    private final Lazy bigInteger$delegate;
    private final String description;
    private final int major;
    private final int minor;
    private final int patch;
    
    static {
        Companion = new Companion(null);
        UNKNOWN = new Version(0, 0, 0, "");
        VERSION_0_1 = new Version(0, 1, 0, "");
        CURRENT = (VERSION_1_0 = new Version(1, 0, 0, ""));
    }
    
    private Version(final int major, final int minor, final int patch, final String description) {
        this.major = major;
        this.minor = minor;
        this.patch = patch;
        this.description = description;
        this.bigInteger$delegate = LazyKt.lazy((Function0)new Version$bigInteger.Version$bigInteger$2(this));
    }
    
    public static final /* synthetic */ Version access$getCURRENT$cp() {
        return Version.CURRENT;
    }
    
    public static final /* synthetic */ Version access$getUNKNOWN$cp() {
        return Version.UNKNOWN;
    }
    
    public static final /* synthetic */ Version access$getVERSION_0_1$cp() {
        return Version.VERSION_0_1;
    }
    
    public static final /* synthetic */ Version access$getVERSION_1_0$cp() {
        return Version.VERSION_1_0;
    }
    
    private final BigInteger getBigInteger() {
        final Object value = this.bigInteger$delegate.getValue();
        Intrinsics.checkNotNullExpressionValue(value, "<get-bigInteger>(...)");
        return (BigInteger)value;
    }
    
    @JvmStatic
    public static final Version parse(final String s) {
        return Version.Companion.parse(s);
    }
    
    @Override
    public int compareTo(final Version version) {
        Intrinsics.checkNotNullParameter((Object)version, "other");
        return this.getBigInteger().compareTo(version.getBigInteger());
    }
    
    @Override
    public boolean equals(final Object o) {
        final boolean b = o instanceof Version;
        final boolean b2 = false;
        if (!b) {
            return false;
        }
        final int major = this.major;
        final Version version = (Version)o;
        boolean b3 = b2;
        if (major == version.major) {
            b3 = b2;
            if (this.minor == version.minor) {
                b3 = b2;
                if (this.patch == version.patch) {
                    b3 = true;
                }
            }
        }
        return b3;
    }
    
    public final String getDescription() {
        return this.description;
    }
    
    public final int getMajor() {
        return this.major;
    }
    
    public final int getMinor() {
        return this.minor;
    }
    
    public final int getPatch() {
        return this.patch;
    }
    
    @Override
    public int hashCode() {
        return ((527 + this.major) * 31 + this.minor) * 31 + this.patch;
    }
    
    @Override
    public String toString() {
        String string;
        if (StringsKt.isBlank((CharSequence)this.description) ^ true) {
            final StringBuilder sb = new StringBuilder();
            sb.append('-');
            sb.append(this.description);
            string = sb.toString();
        }
        else {
            string = "";
        }
        final StringBuilder sb2 = new StringBuilder();
        sb2.append(this.major);
        sb2.append('.');
        sb2.append(this.minor);
        sb2.append('.');
        sb2.append(this.patch);
        sb2.append(string);
        return sb2.toString();
    }
    
    @Metadata(d1 = { "\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010\u000e\n\u0002\b\u0003\b\u0080\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0014\u0010\u000f\u001a\u0004\u0018\u00010\u00042\b\u0010\u0010\u001a\u0004\u0018\u00010\u000eH\u0007R\u0011\u0010\u0003\u001a\u00020\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006R\u0011\u0010\u0007\u001a\u00020\u0004¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010\u0006R\u0011\u0010\t\u001a\u00020\u0004¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u0006R\u0011\u0010\u000b\u001a\u00020\u0004¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\u0006R\u000e\u0010\r\u001a\u00020\u000eX\u0082T¢\u0006\u0002\n\u0000¨\u0006\u0011" }, d2 = { "Landroidx/window/core/Version$Companion;", "", "()V", "CURRENT", "Landroidx/window/core/Version;", "getCURRENT", "()Landroidx/window/core/Version;", "UNKNOWN", "getUNKNOWN", "VERSION_0_1", "getVERSION_0_1", "VERSION_1_0", "getVERSION_1_0", "VERSION_PATTERN_STRING", "", "parse", "versionString", "window_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
    public static final class Companion
    {
        private Companion() {
        }
        
        public final Version getCURRENT() {
            return Version.access$getCURRENT$cp();
        }
        
        public final Version getUNKNOWN() {
            return Version.access$getUNKNOWN$cp();
        }
        
        public final Version getVERSION_0_1() {
            return Version.access$getVERSION_0_1$cp();
        }
        
        public final Version getVERSION_1_0() {
            return Version.access$getVERSION_1_0$cp();
        }
        
        @JvmStatic
        public final Version parse(String group) {
            if (group != null) {
                final CharSequence input = group;
                if (!StringsKt.isBlank(input)) {
                    final Matcher matcher = Pattern.compile("(\\d+)(?:\\.(\\d+))(?:\\.(\\d+))(?:-(.+))?").matcher(input);
                    if (!matcher.matches()) {
                        return null;
                    }
                    final String group2 = matcher.group(1);
                    if (group2 != null) {
                        final int int1 = Integer.parseInt(group2);
                        final String group3 = matcher.group(2);
                        if (group3 != null) {
                            final int int2 = Integer.parseInt(group3);
                            final String group4 = matcher.group(3);
                            if (group4 != null) {
                                final int int3 = Integer.parseInt(group4);
                                if (matcher.group(4) != null) {
                                    group = matcher.group(4);
                                }
                                else {
                                    group = "";
                                }
                                Intrinsics.checkNotNullExpressionValue((Object)group, "description");
                                return new Version(int1, int2, int3, group, null);
                            }
                        }
                    }
                }
            }
            return null;
        }
    }
}

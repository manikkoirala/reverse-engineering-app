// 
// Decompiled by Procyon v0.6.0
// 

package androidx.window.core;

import kotlin.jvm.internal.Intrinsics;
import android.content.ComponentName;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\b\u0000\u0018\u00002\u00020\u0001B\u000f\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004B\u0015\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0006¢\u0006\u0002\u0010\bJ\u0013\u0010\f\u001a\u00020\r2\b\u0010\u000e\u001a\u0004\u0018\u00010\u0001H\u0096\u0002J\b\u0010\u000f\u001a\u00020\u0010H\u0016J\b\u0010\u0011\u001a\u00020\u0006H\u0016R\u0011\u0010\u0007\u001a\u00020\u0006¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0011\u0010\u0005\u001a\u00020\u0006¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\n¨\u0006\u0012" }, d2 = { "Landroidx/window/core/ActivityComponentInfo;", "", "componentName", "Landroid/content/ComponentName;", "(Landroid/content/ComponentName;)V", "packageName", "", "className", "(Ljava/lang/String;Ljava/lang/String;)V", "getClassName", "()Ljava/lang/String;", "getPackageName", "equals", "", "other", "hashCode", "", "toString", "window_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
public final class ActivityComponentInfo
{
    private final String className;
    private final String packageName;
    
    public ActivityComponentInfo(final ComponentName componentName) {
        Intrinsics.checkNotNullParameter((Object)componentName, "componentName");
        final String packageName = componentName.getPackageName();
        Intrinsics.checkNotNullExpressionValue((Object)packageName, "componentName.packageName");
        final String className = componentName.getClassName();
        Intrinsics.checkNotNullExpressionValue((Object)className, "componentName.className");
        this(packageName, className);
    }
    
    public ActivityComponentInfo(final String packageName, final String className) {
        Intrinsics.checkNotNullParameter((Object)packageName, "packageName");
        Intrinsics.checkNotNullParameter((Object)className, "className");
        this.packageName = packageName;
        this.className = className;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        final Class<? extends ActivityComponentInfo> class1 = this.getClass();
        Class<?> class2;
        if (o != null) {
            class2 = o.getClass();
        }
        else {
            class2 = null;
        }
        if (!Intrinsics.areEqual((Object)class1, (Object)class2)) {
            return false;
        }
        Intrinsics.checkNotNull(o, "null cannot be cast to non-null type androidx.window.core.ActivityComponentInfo");
        final ActivityComponentInfo activityComponentInfo = (ActivityComponentInfo)o;
        return Intrinsics.areEqual((Object)this.packageName, (Object)activityComponentInfo.packageName) && Intrinsics.areEqual((Object)this.className, (Object)activityComponentInfo.className);
    }
    
    public final String getClassName() {
        return this.className;
    }
    
    public final String getPackageName() {
        return this.packageName;
    }
    
    @Override
    public int hashCode() {
        return this.packageName.hashCode() * 31 + this.className.hashCode();
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("ClassInfo { packageName: ");
        sb.append(this.packageName);
        sb.append(", className: ");
        sb.append(this.className);
        sb.append(" }");
        return sb.toString();
    }
}

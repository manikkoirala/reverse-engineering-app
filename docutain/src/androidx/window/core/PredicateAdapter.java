// 
// Decompiled by Procyon v0.6.0
// 

package androidx.window.core;

import kotlin.jvm.internal.Reflection;
import android.util.Pair;
import java.io.Serializable;
import kotlin.reflect.KClasses;
import java.lang.reflect.Method;
import kotlin.jvm.functions.Function1;
import java.lang.reflect.Proxy;
import java.lang.reflect.InvocationHandler;
import kotlin.jvm.functions.Function2;
import kotlin.reflect.KClass;
import kotlin.jvm.internal.Intrinsics;
import kotlin.Metadata;

@Metadata(d1 = { "\u00004\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\b\u0001\u0018\u00002\u00020\u0001:\u0003\u0015\u0016\u0017B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004JP\u0010\u0005\u001a\u00020\u0001\"\b\b\u0000\u0010\u0006*\u00020\u0001\"\b\b\u0001\u0010\u0007*\u00020\u00012\f\u0010\b\u001a\b\u0012\u0004\u0012\u0002H\u00060\t2\f\u0010\n\u001a\b\u0012\u0004\u0012\u0002H\u00070\t2\u0018\u0010\u000b\u001a\u0014\u0012\u0004\u0012\u0002H\u0006\u0012\u0004\u0012\u0002H\u0007\u0012\u0004\u0012\u00020\r0\fJ2\u0010\u000e\u001a\u00020\u0001\"\b\b\u0000\u0010\u0006*\u00020\u00012\f\u0010\u000f\u001a\b\u0012\u0004\u0012\u0002H\u00060\t2\u0012\u0010\u000b\u001a\u000e\u0012\u0004\u0012\u0002H\u0006\u0012\u0004\u0012\u00020\r0\u0010J\u0013\u0010\u0011\u001a\b\u0012\u0002\b\u0003\u0018\u00010\u0012H\u0000¢\u0006\u0002\b\u0013J\f\u0010\u0014\u001a\u0006\u0012\u0002\b\u00030\u0012H\u0002R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u0018" }, d2 = { "Landroidx/window/core/PredicateAdapter;", "", "loader", "Ljava/lang/ClassLoader;", "(Ljava/lang/ClassLoader;)V", "buildPairPredicate", "T", "U", "firstClazz", "Lkotlin/reflect/KClass;", "secondClazz", "predicate", "Lkotlin/Function2;", "", "buildPredicate", "clazz", "Lkotlin/Function1;", "predicateClassOrNull", "Ljava/lang/Class;", "predicateClassOrNull$window_release", "predicateClassOrThrow", "BaseHandler", "PairPredicateStubHandler", "PredicateStubHandler", "window_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
public final class PredicateAdapter
{
    private final ClassLoader loader;
    
    public PredicateAdapter(final ClassLoader loader) {
        Intrinsics.checkNotNullParameter((Object)loader, "loader");
        this.loader = loader;
    }
    
    private final Class<?> predicateClassOrThrow() {
        final Class<?> loadClass = this.loader.loadClass("java.util.function.Predicate");
        Intrinsics.checkNotNullExpressionValue((Object)loadClass, "loader.loadClass(\"java.util.function.Predicate\")");
        return loadClass;
    }
    
    public final <T, U> Object buildPairPredicate(final KClass<T> kClass, final KClass<U> kClass2, final Function2<? super T, ? super U, Boolean> function2) {
        Intrinsics.checkNotNullParameter((Object)kClass, "firstClazz");
        Intrinsics.checkNotNullParameter((Object)kClass2, "secondClazz");
        Intrinsics.checkNotNullParameter((Object)function2, "predicate");
        final Object proxyInstance = Proxy.newProxyInstance(this.loader, new Class[] { this.predicateClassOrThrow() }, new PairPredicateStubHandler<Object, Object>(kClass, kClass2, function2));
        Intrinsics.checkNotNullExpressionValue(proxyInstance, "newProxyInstance(loader,\u2026row()), predicateHandler)");
        return proxyInstance;
    }
    
    public final <T> Object buildPredicate(final KClass<T> kClass, final Function1<? super T, Boolean> function1) {
        Intrinsics.checkNotNullParameter((Object)kClass, "clazz");
        Intrinsics.checkNotNullParameter((Object)function1, "predicate");
        final Object proxyInstance = Proxy.newProxyInstance(this.loader, new Class[] { this.predicateClassOrThrow() }, new PredicateStubHandler<Object>(kClass, function1));
        Intrinsics.checkNotNullExpressionValue(proxyInstance, "newProxyInstance(loader,\u2026row()), predicateHandler)");
        return proxyInstance;
    }
    
    public final Class<?> predicateClassOrNull$window_release() {
        Class<?> predicateClassOrThrow;
        try {
            predicateClassOrThrow = this.predicateClassOrThrow();
        }
        catch (final ClassNotFoundException ex) {
            predicateClassOrThrow = null;
        }
        return predicateClassOrThrow;
    }
    
    @Metadata(d1 = { "\u0000.\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0011\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\t\b\"\u0018\u0000*\b\b\u0000\u0010\u0001*\u00020\u00022\u00020\u0003B\u0013\u0012\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00028\u00000\u0005¢\u0006\u0002\u0010\u0006J0\u0010\u0007\u001a\u00020\u00022\u0006\u0010\b\u001a\u00020\u00022\u0006\u0010\t\u001a\u00020\n2\u0010\u0010\u000b\u001a\f\u0012\u0006\b\u0001\u0012\u00020\u0002\u0018\u00010\fH\u0096\u0002¢\u0006\u0002\u0010\rJ\u001d\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\b\u001a\u00020\u00022\u0006\u0010\u0010\u001a\u00028\u0000H&¢\u0006\u0002\u0010\u0011J#\u0010\u0012\u001a\u00020\u000f*\u00020\n2\u0010\u0010\u0013\u001a\f\u0012\u0006\b\u0001\u0012\u00020\u0002\u0018\u00010\fH\u0004¢\u0006\u0002\u0010\u0014J#\u0010\u0015\u001a\u00020\u000f*\u00020\n2\u0010\u0010\u0013\u001a\f\u0012\u0006\b\u0001\u0012\u00020\u0002\u0018\u00010\fH\u0004¢\u0006\u0002\u0010\u0014J#\u0010\u0016\u001a\u00020\u000f*\u00020\n2\u0010\u0010\u0013\u001a\f\u0012\u0006\b\u0001\u0012\u00020\u0002\u0018\u00010\fH\u0004¢\u0006\u0002\u0010\u0014J#\u0010\u0017\u001a\u00020\u000f*\u00020\n2\u0010\u0010\u0013\u001a\f\u0012\u0006\b\u0001\u0012\u00020\u0002\u0018\u00010\fH\u0004¢\u0006\u0002\u0010\u0014R\u0014\u0010\u0004\u001a\b\u0012\u0004\u0012\u00028\u00000\u0005X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u0018" }, d2 = { "Landroidx/window/core/PredicateAdapter$BaseHandler;", "T", "", "Ljava/lang/reflect/InvocationHandler;", "clazz", "Lkotlin/reflect/KClass;", "(Lkotlin/reflect/KClass;)V", "invoke", "obj", "method", "Ljava/lang/reflect/Method;", "parameters", "", "(Ljava/lang/Object;Ljava/lang/reflect/Method;[Ljava/lang/Object;)Ljava/lang/Object;", "invokeTest", "", "parameter", "(Ljava/lang/Object;Ljava/lang/Object;)Z", "isEquals", "args", "(Ljava/lang/reflect/Method;[Ljava/lang/Object;)Z", "isHashCode", "isTest", "isToString", "window_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
    private abstract static class BaseHandler<T> implements InvocationHandler
    {
        private final KClass<T> clazz;
        
        public BaseHandler(final KClass<T> clazz) {
            Intrinsics.checkNotNullParameter((Object)clazz, "clazz");
            this.clazz = clazz;
        }
        
        @Override
        public Object invoke(final Object obj, final Method obj2, final Object[] obj3) {
            Intrinsics.checkNotNullParameter(obj, "obj");
            Intrinsics.checkNotNullParameter((Object)obj2, "method");
            final boolean test = this.isTest(obj2, obj3);
            final Object o = null;
            final Object o2 = null;
            boolean b = false;
            Serializable s;
            if (test) {
                final KClass<T> clazz = this.clazz;
                Object o3 = o2;
                if (obj3 != null) {
                    o3 = obj3[0];
                }
                s = this.invokeTest(obj, KClasses.cast((KClass)clazz, o3));
            }
            else if (this.isEquals(obj2, obj3)) {
                Object o4 = o;
                if (obj3 != null) {
                    o4 = obj3[0];
                }
                Intrinsics.checkNotNull(o4);
                if (obj == o4) {
                    b = true;
                }
                s = b;
            }
            else if (this.isHashCode(obj2, obj3)) {
                s = this.hashCode();
            }
            else {
                if (!this.isToString(obj2, obj3)) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Unexpected method call object:");
                    sb.append(obj);
                    sb.append(", method: ");
                    sb.append(obj2);
                    sb.append(", args: ");
                    sb.append(obj3);
                    throw new UnsupportedOperationException(sb.toString());
                }
                s = this.toString();
            }
            return s;
        }
        
        public abstract boolean invokeTest(final Object p0, final T p1);
        
        protected final boolean isEquals(final Method method, final Object[] array) {
            Intrinsics.checkNotNullParameter((Object)method, "<this>");
            final boolean equal = Intrinsics.areEqual((Object)method.getName(), (Object)"equals");
            boolean b = true;
            if (!equal || !method.getReturnType().equals(Boolean.TYPE) || (array == null || array.length != 1)) {
                b = false;
            }
            return b;
        }
        
        protected final boolean isHashCode(final Method method, final Object[] array) {
            Intrinsics.checkNotNullParameter((Object)method, "<this>");
            return Intrinsics.areEqual((Object)method.getName(), (Object)"hashCode") && method.getReturnType().equals(Integer.TYPE) && array == null;
        }
        
        protected final boolean isTest(final Method method, final Object[] array) {
            Intrinsics.checkNotNullParameter((Object)method, "<this>");
            final boolean equal = Intrinsics.areEqual((Object)method.getName(), (Object)"test");
            boolean b = true;
            if (!equal || !method.getReturnType().equals(Boolean.TYPE) || (array == null || array.length != 1)) {
                b = false;
            }
            return b;
        }
        
        protected final boolean isToString(final Method method, final Object[] array) {
            Intrinsics.checkNotNullParameter((Object)method, "<this>");
            return Intrinsics.areEqual((Object)method.getName(), (Object)"toString") && method.getReturnType().equals(String.class) && array == null;
        }
    }
    
    @Metadata(d1 = { "\u00008\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0010\u000e\n\u0000\b\u0002\u0018\u0000*\b\b\u0000\u0010\u0001*\u00020\u0002*\b\b\u0001\u0010\u0003*\u00020\u00022\u0010\u0012\f\u0012\n\u0012\u0002\b\u0003\u0012\u0002\b\u00030\u00050\u0004B;\u0012\f\u0010\u0006\u001a\b\u0012\u0004\u0012\u00028\u00000\u0007\u0012\f\u0010\b\u001a\b\u0012\u0004\u0012\u00028\u00010\u0007\u0012\u0018\u0010\t\u001a\u0014\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u0001\u0012\u0004\u0012\u00020\u000b0\n¢\u0006\u0002\u0010\fJ\b\u0010\r\u001a\u00020\u000eH\u0016J \u0010\u000f\u001a\u00020\u000b2\u0006\u0010\u0010\u001a\u00020\u00022\u000e\u0010\u0011\u001a\n\u0012\u0002\b\u0003\u0012\u0002\b\u00030\u0005H\u0016J\b\u0010\u0012\u001a\u00020\u0013H\u0016R\u0014\u0010\u0006\u001a\b\u0012\u0004\u0012\u00028\u00000\u0007X\u0082\u0004¢\u0006\u0002\n\u0000R\u0014\u0010\b\u001a\b\u0012\u0004\u0012\u00028\u00010\u0007X\u0082\u0004¢\u0006\u0002\n\u0000R \u0010\t\u001a\u0014\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u0001\u0012\u0004\u0012\u00020\u000b0\nX\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u0014" }, d2 = { "Landroidx/window/core/PredicateAdapter$PairPredicateStubHandler;", "T", "", "U", "Landroidx/window/core/PredicateAdapter$BaseHandler;", "Landroid/util/Pair;", "clazzT", "Lkotlin/reflect/KClass;", "clazzU", "predicate", "Lkotlin/Function2;", "", "(Lkotlin/reflect/KClass;Lkotlin/reflect/KClass;Lkotlin/jvm/functions/Function2;)V", "hashCode", "", "invokeTest", "obj", "parameter", "toString", "", "window_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
    private static final class PairPredicateStubHandler<T, U> extends BaseHandler<Pair<?, ?>>
    {
        private final KClass<T> clazzT;
        private final KClass<U> clazzU;
        private final Function2<T, U, Boolean> predicate;
        
        public PairPredicateStubHandler(final KClass<T> clazzT, final KClass<U> clazzU, final Function2<? super T, ? super U, Boolean> predicate) {
            Intrinsics.checkNotNullParameter((Object)clazzT, "clazzT");
            Intrinsics.checkNotNullParameter((Object)clazzU, "clazzU");
            Intrinsics.checkNotNullParameter((Object)predicate, "predicate");
            super(Reflection.getOrCreateKotlinClass((Class)Pair.class));
            this.clazzT = clazzT;
            this.clazzU = clazzU;
            this.predicate = (Function2<T, U, Boolean>)predicate;
        }
        
        @Override
        public int hashCode() {
            return this.predicate.hashCode();
        }
        
        public boolean invokeTest(Object cast, final Pair<?, ?> pair) {
            Intrinsics.checkNotNullParameter(cast, "obj");
            Intrinsics.checkNotNullParameter((Object)pair, "parameter");
            cast = KClasses.cast((KClass)this.clazzT, pair.first);
            return (boolean)this.predicate.invoke(cast, KClasses.cast((KClass)this.clazzU, pair.second));
        }
        
        @Override
        public String toString() {
            return this.predicate.toString();
        }
    }
    
    @Metadata(d1 = { "\u00000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0000\b\u0002\u0018\u0000*\b\b\u0000\u0010\u0001*\u00020\u00022\b\u0012\u0004\u0012\u0002H\u00010\u0003B'\u0012\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00028\u00000\u0005\u0012\u0012\u0010\u0006\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00020\b0\u0007¢\u0006\u0002\u0010\tJ\b\u0010\n\u001a\u00020\u000bH\u0016J\u001d\u0010\f\u001a\u00020\b2\u0006\u0010\r\u001a\u00020\u00022\u0006\u0010\u000e\u001a\u00028\u0000H\u0016¢\u0006\u0002\u0010\u000fJ\b\u0010\u0010\u001a\u00020\u0011H\u0016R\u001a\u0010\u0006\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00020\b0\u0007X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u0012" }, d2 = { "Landroidx/window/core/PredicateAdapter$PredicateStubHandler;", "T", "", "Landroidx/window/core/PredicateAdapter$BaseHandler;", "clazzT", "Lkotlin/reflect/KClass;", "predicate", "Lkotlin/Function1;", "", "(Lkotlin/reflect/KClass;Lkotlin/jvm/functions/Function1;)V", "hashCode", "", "invokeTest", "obj", "parameter", "(Ljava/lang/Object;Ljava/lang/Object;)Z", "toString", "", "window_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
    private static final class PredicateStubHandler<T> extends BaseHandler<T>
    {
        private final Function1<T, Boolean> predicate;
        
        public PredicateStubHandler(final KClass<T> kClass, final Function1<? super T, Boolean> predicate) {
            Intrinsics.checkNotNullParameter((Object)kClass, "clazzT");
            Intrinsics.checkNotNullParameter((Object)predicate, "predicate");
            super(kClass);
            this.predicate = (Function1<T, Boolean>)predicate;
        }
        
        @Override
        public int hashCode() {
            return this.predicate.hashCode();
        }
        
        @Override
        public boolean invokeTest(final Object o, final T t) {
            Intrinsics.checkNotNullParameter(o, "obj");
            Intrinsics.checkNotNullParameter((Object)t, "parameter");
            return (boolean)this.predicate.invoke((Object)t);
        }
        
        @Override
        public String toString() {
            return this.predicate.toString();
        }
    }
}

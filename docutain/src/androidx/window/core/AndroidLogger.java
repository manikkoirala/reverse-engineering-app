// 
// Decompiled by Procyon v0.6.0
// 

package androidx.window.core;

import android.util.Log;
import kotlin.jvm.internal.Intrinsics;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\b\u00c0\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0018\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0006H\u0016¨\u0006\b" }, d2 = { "Landroidx/window/core/AndroidLogger;", "Landroidx/window/core/Logger;", "()V", "debug", "", "tag", "", "message", "window_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
public final class AndroidLogger implements Logger
{
    public static final AndroidLogger INSTANCE;
    
    static {
        INSTANCE = new AndroidLogger();
    }
    
    private AndroidLogger() {
    }
    
    @Override
    public void debug(final String s, final String s2) {
        Intrinsics.checkNotNullParameter((Object)s, "tag");
        Intrinsics.checkNotNullParameter((Object)s2, "message");
        Log.d(s, s2);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.window.area;

import kotlin.Metadata;

@Metadata(d1 = { "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\bg\u0018\u00002\u00020\u0001J\b\u0010\u0002\u001a\u00020\u0003H&J\u0010\u0010\u0004\u001a\u00020\u00032\u0006\u0010\u0005\u001a\u00020\u0006H&\u00f8\u0001\u0000\u0082\u0002\u0006\n\u0004\b!0\u0001¨\u0006\u0007\u00c0\u0006\u0001" }, d2 = { "Landroidx/window/area/WindowAreaSessionCallback;", "", "onSessionEnded", "", "onSessionStarted", "session", "Landroidx/window/area/WindowAreaSession;", "window_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
public interface WindowAreaSessionCallback
{
    void onSessionEnded();
    
    void onSessionStarted(final WindowAreaSession p0);
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.window.area;

import kotlinx.coroutines.flow.FlowKt;
import kotlinx.coroutines.flow.Flow;
import kotlin.jvm.internal.Intrinsics;
import java.util.concurrent.Executor;
import android.app.Activity;
import kotlin.Metadata;

@Metadata(d1 = { "\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b\u0001\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J \u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\nH\u0016J\u000e\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\r0\fH\u0016¨\u0006\u000e" }, d2 = { "Landroidx/window/area/EmptyWindowAreaControllerImpl;", "Landroidx/window/area/WindowAreaController;", "()V", "rearDisplayMode", "", "activity", "Landroid/app/Activity;", "executor", "Ljava/util/concurrent/Executor;", "windowAreaSessionCallback", "Landroidx/window/area/WindowAreaSessionCallback;", "rearDisplayStatus", "Lkotlinx/coroutines/flow/Flow;", "Landroidx/window/area/WindowAreaStatus;", "window_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
public final class EmptyWindowAreaControllerImpl implements WindowAreaController
{
    @Override
    public void rearDisplayMode(final Activity activity, final Executor executor, final WindowAreaSessionCallback windowAreaSessionCallback) {
        Intrinsics.checkNotNullParameter((Object)activity, "activity");
        Intrinsics.checkNotNullParameter((Object)executor, "executor");
        Intrinsics.checkNotNullParameter((Object)windowAreaSessionCallback, "windowAreaSessionCallback");
        throw new UnsupportedOperationException("Rear Display mode cannot be enabled currently");
    }
    
    @Override
    public Flow<WindowAreaStatus> rearDisplayStatus() {
        return (Flow<WindowAreaStatus>)FlowKt.flowOf((Object)WindowAreaStatus.UNSUPPORTED);
    }
}

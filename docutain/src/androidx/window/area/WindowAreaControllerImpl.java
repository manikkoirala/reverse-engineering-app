// 
// Decompiled by Procyon v0.6.0
// 

package androidx.window.area;

import android.util.Log;
import androidx.window.core.VerificationMode;
import androidx.window.core.BuildConfig;
import kotlinx.coroutines.flow.FlowKt;
import kotlin.coroutines.Continuation;
import kotlin.jvm.functions.Function2;
import kotlinx.coroutines.flow.Flow;
import androidx.window.extensions.core.util.function.Consumer;
import java.util.concurrent.Executor;
import android.app.Activity;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Reflection;
import kotlin.jvm.internal.DefaultConstructorMarker;
import androidx.window.extensions.area.WindowAreaComponent;
import kotlin.Metadata;

@Metadata(d1 = { "\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\b\u0001\u0018\u0000 \u00112\u00020\u0001:\u0002\u0011\u0012B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J \u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u000eH\u0016J\u000e\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u00060\u0010H\u0016R\u0010\u0010\u0005\u001a\u0004\u0018\u00010\u0006X\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u0013" }, d2 = { "Landroidx/window/area/WindowAreaControllerImpl;", "Landroidx/window/area/WindowAreaController;", "windowAreaComponent", "Landroidx/window/extensions/area/WindowAreaComponent;", "(Landroidx/window/extensions/area/WindowAreaComponent;)V", "currentStatus", "Landroidx/window/area/WindowAreaStatus;", "rearDisplayMode", "", "activity", "Landroid/app/Activity;", "executor", "Ljava/util/concurrent/Executor;", "windowAreaSessionCallback", "Landroidx/window/area/WindowAreaSessionCallback;", "rearDisplayStatus", "Lkotlinx/coroutines/flow/Flow;", "Companion", "RearDisplaySessionConsumer", "window_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
public final class WindowAreaControllerImpl implements WindowAreaController
{
    public static final Companion Companion;
    private static final String TAG;
    private WindowAreaStatus currentStatus;
    private final WindowAreaComponent windowAreaComponent;
    
    static {
        Companion = new Companion(null);
        TAG = Reflection.getOrCreateKotlinClass((Class)WindowAreaControllerImpl.class).getSimpleName();
    }
    
    public WindowAreaControllerImpl(final WindowAreaComponent windowAreaComponent) {
        Intrinsics.checkNotNullParameter((Object)windowAreaComponent, "windowAreaComponent");
        this.windowAreaComponent = windowAreaComponent;
    }
    
    public static final /* synthetic */ String access$getTAG$cp() {
        return WindowAreaControllerImpl.TAG;
    }
    
    @Override
    public void rearDisplayMode(final Activity activity, final Executor executor, final WindowAreaSessionCallback windowAreaSessionCallback) {
        Intrinsics.checkNotNullParameter((Object)activity, "activity");
        Intrinsics.checkNotNullParameter((Object)executor, "executor");
        Intrinsics.checkNotNullParameter((Object)windowAreaSessionCallback, "windowAreaSessionCallback");
        final WindowAreaStatus currentStatus = this.currentStatus;
        if (currentStatus != null && !Intrinsics.areEqual((Object)currentStatus, (Object)WindowAreaStatus.AVAILABLE)) {
            throw new UnsupportedOperationException("Rear Display mode cannot be enabled currently");
        }
        this.windowAreaComponent.startRearDisplaySession(activity, (Consumer)new RearDisplaySessionConsumer(executor, windowAreaSessionCallback, this.windowAreaComponent));
    }
    
    @Override
    public Flow<WindowAreaStatus> rearDisplayStatus() {
        return (Flow<WindowAreaStatus>)FlowKt.distinctUntilChanged(FlowKt.callbackFlow((Function2)new WindowAreaControllerImpl$rearDisplayStatus.WindowAreaControllerImpl$rearDisplayStatus$1(this, (Continuation)null)));
    }
    
    @Metadata(d1 = { "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\b\u0080\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u0010\u0010\u0003\u001a\u0004\u0018\u00010\u0004X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u0005" }, d2 = { "Landroidx/window/area/WindowAreaControllerImpl$Companion;", "", "()V", "TAG", "", "window_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
    public static final class Companion
    {
        private Companion() {
        }
    }
    
    @Metadata(d1 = { "\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\b\u0000\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\u001d\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\b¢\u0006\u0002\u0010\tJ\u0010\u0010\f\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u0002H\u0016J\b\u0010\u000f\u001a\u00020\rH\u0002J\b\u0010\u0010\u001a\u00020\rH\u0002R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\bX\u0082\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\n\u001a\u0004\u0018\u00010\u000bX\u0082\u000e¢\u0006\u0002\n\u0000¨\u0006\u0011" }, d2 = { "Landroidx/window/area/WindowAreaControllerImpl$RearDisplaySessionConsumer;", "Landroidx/window/extensions/core/util/function/Consumer;", "", "executor", "Ljava/util/concurrent/Executor;", "appCallback", "Landroidx/window/area/WindowAreaSessionCallback;", "extensionsComponent", "Landroidx/window/extensions/area/WindowAreaComponent;", "(Ljava/util/concurrent/Executor;Landroidx/window/area/WindowAreaSessionCallback;Landroidx/window/extensions/area/WindowAreaComponent;)V", "session", "Landroidx/window/area/WindowAreaSession;", "accept", "", "t", "onSessionFinished", "onSessionStarted", "window_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
    public static final class RearDisplaySessionConsumer implements Consumer<Integer>
    {
        private final WindowAreaSessionCallback appCallback;
        private final Executor executor;
        private final WindowAreaComponent extensionsComponent;
        private WindowAreaSession session;
        
        public RearDisplaySessionConsumer(final Executor executor, final WindowAreaSessionCallback appCallback, final WindowAreaComponent extensionsComponent) {
            Intrinsics.checkNotNullParameter((Object)executor, "executor");
            Intrinsics.checkNotNullParameter((Object)appCallback, "appCallback");
            Intrinsics.checkNotNullParameter((Object)extensionsComponent, "extensionsComponent");
            this.executor = executor;
            this.appCallback = appCallback;
            this.extensionsComponent = extensionsComponent;
        }
        
        private final void onSessionFinished() {
            this.session = null;
            this.executor.execute(new WindowAreaControllerImpl$RearDisplaySessionConsumer$$ExternalSyntheticLambda1(this));
        }
        
        private static final void onSessionFinished$lambda$2(final RearDisplaySessionConsumer rearDisplaySessionConsumer) {
            Intrinsics.checkNotNullParameter((Object)rearDisplaySessionConsumer, "this$0");
            rearDisplaySessionConsumer.appCallback.onSessionEnded();
        }
        
        private final void onSessionStarted() {
            final WindowAreaSession session = new RearDisplaySessionImpl(this.extensionsComponent);
            this.session = session;
            this.executor.execute(new WindowAreaControllerImpl$RearDisplaySessionConsumer$$ExternalSyntheticLambda0(this, session));
        }
        
        private static final void onSessionStarted$lambda$1$lambda$0(final RearDisplaySessionConsumer rearDisplaySessionConsumer, final WindowAreaSession windowAreaSession) {
            Intrinsics.checkNotNullParameter((Object)rearDisplaySessionConsumer, "this$0");
            Intrinsics.checkNotNullParameter((Object)windowAreaSession, "$it");
            rearDisplaySessionConsumer.appCallback.onSessionStarted(windowAreaSession);
        }
        
        public void accept(final int i) {
            if (i != 0) {
                if (i != 1) {
                    if (BuildConfig.INSTANCE.getVerificationMode() == VerificationMode.STRICT) {
                        final String access$getTAG$cp = WindowAreaControllerImpl.access$getTAG$cp();
                        final StringBuilder sb = new StringBuilder();
                        sb.append("Received an unknown session status value: ");
                        sb.append(i);
                        Log.d(access$getTAG$cp, sb.toString());
                    }
                    this.onSessionFinished();
                }
                else {
                    this.onSessionStarted();
                }
            }
            else {
                this.onSessionFinished();
            }
        }
    }
}

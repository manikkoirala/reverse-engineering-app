// 
// Decompiled by Procyon v0.6.0
// 

package androidx.window.reflection;

import java.lang.reflect.Modifier;
import kotlin.jvm.JvmClassMappingKt;
import kotlin.reflect.KClass;
import java.lang.reflect.Method;
import kotlin.jvm.JvmStatic;
import android.util.Log;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.functions.Function0;
import kotlin.Metadata;

@Metadata(d1 = { "\u00002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\b\u00c0\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u001f\u0010\b\u001a\u00020\u00042\u0010\u0010\t\u001a\f\u0012\b\u0012\u0006\u0012\u0002\b\u00030\u000b0\nH\u0000¢\u0006\u0002\b\fJ'\u0010\r\u001a\u00020\u00042\n\b\u0002\u0010\u000e\u001a\u0004\u0018\u00010\u000f2\f\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u00040\nH\u0001¢\u0006\u0002\b\u0011J\u001d\u0010\u0012\u001a\u00020\u0004*\u00020\u00052\n\u0010\u0013\u001a\u0006\u0012\u0002\b\u00030\u000bH\u0000¢\u0006\u0002\b\u0014J\u001d\u0010\u0012\u001a\u00020\u0004*\u00020\u00052\n\u0010\u0013\u001a\u0006\u0012\u0002\b\u00030\u0015H\u0000¢\u0006\u0002\b\u0014R\u0018\u0010\u0003\u001a\u00020\u0004*\u00020\u00058@X\u0080\u0004¢\u0006\u0006\u001a\u0004\b\u0006\u0010\u0007¨\u0006\u0016" }, d2 = { "Landroidx/window/reflection/ReflectionUtils;", "", "()V", "isPublic", "", "Ljava/lang/reflect/Method;", "isPublic$window_release", "(Ljava/lang/reflect/Method;)Z", "checkIsPresent", "classLoader", "Lkotlin/Function0;", "Ljava/lang/Class;", "checkIsPresent$window_release", "validateReflection", "errorMessage", "", "block", "validateReflection$window_release", "doesReturn", "clazz", "doesReturn$window_release", "Lkotlin/reflect/KClass;", "window_release" }, k = 1, mv = { 1, 8, 0 }, xi = 48)
public final class ReflectionUtils
{
    public static final ReflectionUtils INSTANCE;
    
    static {
        INSTANCE = new ReflectionUtils();
    }
    
    private ReflectionUtils() {
    }
    
    @JvmStatic
    public static final boolean validateReflection$window_release(final String s, final Function0<Boolean> function0) {
        Intrinsics.checkNotNullParameter((Object)function0, "block");
        final boolean b = false;
        boolean booleanValue;
        try {
            booleanValue = (boolean)function0.invoke();
            if (!booleanValue && s != null) {
                Log.e("ReflectionGuard", s);
            }
        }
        catch (final NoSuchMethodException ex) {
            final StringBuilder sb = new StringBuilder();
            sb.append("NoSuchMethod: ");
            String str = s;
            if (s == null) {
                str = "";
            }
            sb.append(str);
            Log.e("ReflectionGuard", sb.toString());
            booleanValue = b;
        }
        catch (final ClassNotFoundException ex2) {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("ClassNotFound: ");
            String str2 = s;
            if (s == null) {
                str2 = "";
            }
            sb2.append(str2);
            Log.e("ReflectionGuard", sb2.toString());
            booleanValue = b;
        }
        return booleanValue;
    }
    
    public final boolean checkIsPresent$window_release(final Function0<? extends Class<?>> function0) {
        Intrinsics.checkNotNullParameter((Object)function0, "classLoader");
        boolean b = false;
        try {
            function0.invoke();
            b = true;
            return b;
        }
        catch (final ClassNotFoundException | NoClassDefFoundError classNotFoundException | NoClassDefFoundError) {
            return b;
        }
    }
    
    public final boolean doesReturn$window_release(final Method method, final Class<?> obj) {
        Intrinsics.checkNotNullParameter((Object)method, "<this>");
        Intrinsics.checkNotNullParameter((Object)obj, "clazz");
        return method.getReturnType().equals(obj);
    }
    
    public final boolean doesReturn$window_release(final Method method, final KClass<?> kClass) {
        Intrinsics.checkNotNullParameter((Object)method, "<this>");
        Intrinsics.checkNotNullParameter((Object)kClass, "clazz");
        return this.doesReturn$window_release(method, JvmClassMappingKt.getJavaClass((KClass)kClass));
    }
    
    public final boolean isPublic$window_release(final Method method) {
        Intrinsics.checkNotNullParameter((Object)method, "<this>");
        return Modifier.isPublic(method.getModifiers());
    }
}

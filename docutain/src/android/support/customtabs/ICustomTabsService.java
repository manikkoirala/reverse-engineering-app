// 
// Decompiled by Procyon v0.6.0
// 

package android.support.customtabs;

import android.os.Parcelable$Creator;
import android.os.Parcelable;
import android.os.Parcel;
import android.os.Binder;
import android.os.IBinder;
import java.util.List;
import android.net.Uri;
import android.os.RemoteException;
import android.os.Bundle;
import android.os.IInterface;

public interface ICustomTabsService extends IInterface
{
    public static final String DESCRIPTOR = "android.support.customtabs.ICustomTabsService";
    
    Bundle extraCommand(final String p0, final Bundle p1) throws RemoteException;
    
    boolean mayLaunchUrl(final ICustomTabsCallback p0, final Uri p1, final Bundle p2, final List<Bundle> p3) throws RemoteException;
    
    boolean newSession(final ICustomTabsCallback p0) throws RemoteException;
    
    boolean newSessionWithExtras(final ICustomTabsCallback p0, final Bundle p1) throws RemoteException;
    
    int postMessage(final ICustomTabsCallback p0, final String p1, final Bundle p2) throws RemoteException;
    
    boolean receiveFile(final ICustomTabsCallback p0, final Uri p1, final int p2, final Bundle p3) throws RemoteException;
    
    boolean requestPostMessageChannel(final ICustomTabsCallback p0, final Uri p1) throws RemoteException;
    
    boolean requestPostMessageChannelWithExtras(final ICustomTabsCallback p0, final Uri p1, final Bundle p2) throws RemoteException;
    
    boolean updateVisuals(final ICustomTabsCallback p0, final Bundle p1) throws RemoteException;
    
    boolean validateRelationship(final ICustomTabsCallback p0, final int p1, final Uri p2, final Bundle p3) throws RemoteException;
    
    boolean warmup(final long p0) throws RemoteException;
    
    public static class Default implements ICustomTabsService
    {
        public IBinder asBinder() {
            return null;
        }
        
        @Override
        public Bundle extraCommand(final String s, final Bundle bundle) throws RemoteException {
            return null;
        }
        
        @Override
        public boolean mayLaunchUrl(final ICustomTabsCallback customTabsCallback, final Uri uri, final Bundle bundle, final List<Bundle> list) throws RemoteException {
            return false;
        }
        
        @Override
        public boolean newSession(final ICustomTabsCallback customTabsCallback) throws RemoteException {
            return false;
        }
        
        @Override
        public boolean newSessionWithExtras(final ICustomTabsCallback customTabsCallback, final Bundle bundle) throws RemoteException {
            return false;
        }
        
        @Override
        public int postMessage(final ICustomTabsCallback customTabsCallback, final String s, final Bundle bundle) throws RemoteException {
            return 0;
        }
        
        @Override
        public boolean receiveFile(final ICustomTabsCallback customTabsCallback, final Uri uri, final int n, final Bundle bundle) throws RemoteException {
            return false;
        }
        
        @Override
        public boolean requestPostMessageChannel(final ICustomTabsCallback customTabsCallback, final Uri uri) throws RemoteException {
            return false;
        }
        
        @Override
        public boolean requestPostMessageChannelWithExtras(final ICustomTabsCallback customTabsCallback, final Uri uri, final Bundle bundle) throws RemoteException {
            return false;
        }
        
        @Override
        public boolean updateVisuals(final ICustomTabsCallback customTabsCallback, final Bundle bundle) throws RemoteException {
            return false;
        }
        
        @Override
        public boolean validateRelationship(final ICustomTabsCallback customTabsCallback, final int n, final Uri uri, final Bundle bundle) throws RemoteException {
            return false;
        }
        
        @Override
        public boolean warmup(final long n) throws RemoteException {
            return false;
        }
    }
    
    public abstract static class Stub extends Binder implements ICustomTabsService
    {
        static final int TRANSACTION_extraCommand = 5;
        static final int TRANSACTION_mayLaunchUrl = 4;
        static final int TRANSACTION_newSession = 3;
        static final int TRANSACTION_newSessionWithExtras = 10;
        static final int TRANSACTION_postMessage = 8;
        static final int TRANSACTION_receiveFile = 12;
        static final int TRANSACTION_requestPostMessageChannel = 7;
        static final int TRANSACTION_requestPostMessageChannelWithExtras = 11;
        static final int TRANSACTION_updateVisuals = 6;
        static final int TRANSACTION_validateRelationship = 9;
        static final int TRANSACTION_warmup = 2;
        
        public Stub() {
            this.attachInterface((IInterface)this, "android.support.customtabs.ICustomTabsService");
        }
        
        public static ICustomTabsService asInterface(final IBinder binder) {
            if (binder == null) {
                return null;
            }
            final IInterface queryLocalInterface = binder.queryLocalInterface("android.support.customtabs.ICustomTabsService");
            if (queryLocalInterface != null && queryLocalInterface instanceof ICustomTabsService) {
                return (ICustomTabsService)queryLocalInterface;
            }
            return new Proxy(binder);
        }
        
        public IBinder asBinder() {
            return (IBinder)this;
        }
        
        public boolean onTransact(int n, final Parcel parcel, final Parcel parcel2, final int n2) throws RemoteException {
            if (n >= 1 && n <= 16777215) {
                parcel.enforceInterface("android.support.customtabs.ICustomTabsService");
            }
            if (n != 1598968902) {
                switch (n) {
                    default: {
                        return super.onTransact(n, parcel, parcel2, n2);
                    }
                    case 12: {
                        n = (this.receiveFile(ICustomTabsCallback.Stub.asInterface(parcel.readStrongBinder()), (Uri)readTypedObject(parcel, (android.os.Parcelable$Creator<Object>)Uri.CREATOR), parcel.readInt(), (Bundle)readTypedObject(parcel, (android.os.Parcelable$Creator<Object>)Bundle.CREATOR)) ? 1 : 0);
                        parcel2.writeNoException();
                        parcel2.writeInt(n);
                        break;
                    }
                    case 11: {
                        n = (this.requestPostMessageChannelWithExtras(ICustomTabsCallback.Stub.asInterface(parcel.readStrongBinder()), (Uri)readTypedObject(parcel, (android.os.Parcelable$Creator<Object>)Uri.CREATOR), (Bundle)readTypedObject(parcel, (android.os.Parcelable$Creator<Object>)Bundle.CREATOR)) ? 1 : 0);
                        parcel2.writeNoException();
                        parcel2.writeInt(n);
                        break;
                    }
                    case 10: {
                        n = (this.newSessionWithExtras(ICustomTabsCallback.Stub.asInterface(parcel.readStrongBinder()), (Bundle)readTypedObject(parcel, (android.os.Parcelable$Creator<Object>)Bundle.CREATOR)) ? 1 : 0);
                        parcel2.writeNoException();
                        parcel2.writeInt(n);
                        break;
                    }
                    case 9: {
                        n = (this.validateRelationship(ICustomTabsCallback.Stub.asInterface(parcel.readStrongBinder()), parcel.readInt(), (Uri)readTypedObject(parcel, (android.os.Parcelable$Creator<Object>)Uri.CREATOR), (Bundle)readTypedObject(parcel, (android.os.Parcelable$Creator<Object>)Bundle.CREATOR)) ? 1 : 0);
                        parcel2.writeNoException();
                        parcel2.writeInt(n);
                        break;
                    }
                    case 8: {
                        n = this.postMessage(ICustomTabsCallback.Stub.asInterface(parcel.readStrongBinder()), parcel.readString(), (Bundle)readTypedObject(parcel, (android.os.Parcelable$Creator<Object>)Bundle.CREATOR));
                        parcel2.writeNoException();
                        parcel2.writeInt(n);
                        break;
                    }
                    case 7: {
                        n = (this.requestPostMessageChannel(ICustomTabsCallback.Stub.asInterface(parcel.readStrongBinder()), (Uri)readTypedObject(parcel, (android.os.Parcelable$Creator<Object>)Uri.CREATOR)) ? 1 : 0);
                        parcel2.writeNoException();
                        parcel2.writeInt(n);
                        break;
                    }
                    case 6: {
                        n = (this.updateVisuals(ICustomTabsCallback.Stub.asInterface(parcel.readStrongBinder()), (Bundle)readTypedObject(parcel, (android.os.Parcelable$Creator<Object>)Bundle.CREATOR)) ? 1 : 0);
                        parcel2.writeNoException();
                        parcel2.writeInt(n);
                        break;
                    }
                    case 5: {
                        final Bundle extraCommand = this.extraCommand(parcel.readString(), (Bundle)readTypedObject(parcel, (android.os.Parcelable$Creator<Object>)Bundle.CREATOR));
                        parcel2.writeNoException();
                        writeTypedObject(parcel2, (Parcelable)extraCommand, 1);
                        break;
                    }
                    case 4: {
                        n = (this.mayLaunchUrl(ICustomTabsCallback.Stub.asInterface(parcel.readStrongBinder()), (Uri)readTypedObject(parcel, (android.os.Parcelable$Creator<Object>)Uri.CREATOR), (Bundle)readTypedObject(parcel, (android.os.Parcelable$Creator<Object>)Bundle.CREATOR), parcel.createTypedArrayList(Bundle.CREATOR)) ? 1 : 0);
                        parcel2.writeNoException();
                        parcel2.writeInt(n);
                        break;
                    }
                    case 3: {
                        n = (this.newSession(ICustomTabsCallback.Stub.asInterface(parcel.readStrongBinder())) ? 1 : 0);
                        parcel2.writeNoException();
                        parcel2.writeInt(n);
                        break;
                    }
                    case 2: {
                        n = (this.warmup(parcel.readLong()) ? 1 : 0);
                        parcel2.writeNoException();
                        parcel2.writeInt(n);
                        break;
                    }
                }
                return true;
            }
            parcel2.writeString("android.support.customtabs.ICustomTabsService");
            return true;
        }
        
        private static class Proxy implements ICustomTabsService
        {
            private IBinder mRemote;
            
            Proxy(final IBinder mRemote) {
                this.mRemote = mRemote;
            }
            
            public IBinder asBinder() {
                return this.mRemote;
            }
            
            @Override
            public Bundle extraCommand(final String s, final Bundle bundle) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                final Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("android.support.customtabs.ICustomTabsService");
                    obtain.writeString(s);
                    writeTypedObject(obtain, (Parcelable)bundle, 0);
                    this.mRemote.transact(5, obtain, obtain2, 0);
                    obtain2.readException();
                    return (Bundle)readTypedObject(obtain2, (android.os.Parcelable$Creator<Object>)Bundle.CREATOR);
                }
                finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
            
            public String getInterfaceDescriptor() {
                return "android.support.customtabs.ICustomTabsService";
            }
            
            @Override
            public boolean mayLaunchUrl(final ICustomTabsCallback customTabsCallback, final Uri uri, final Bundle bundle, final List<Bundle> list) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                final Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("android.support.customtabs.ICustomTabsService");
                    obtain.writeStrongInterface((IInterface)customTabsCallback);
                    boolean b = false;
                    writeTypedObject(obtain, (Parcelable)uri, 0);
                    writeTypedObject(obtain, (Parcelable)bundle, 0);
                    obtain.writeTypedList((List)list);
                    this.mRemote.transact(4, obtain, obtain2, 0);
                    obtain2.readException();
                    if (obtain2.readInt() != 0) {
                        b = true;
                    }
                    return b;
                }
                finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
            
            @Override
            public boolean newSession(final ICustomTabsCallback customTabsCallback) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                final Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("android.support.customtabs.ICustomTabsService");
                    obtain.writeStrongInterface((IInterface)customTabsCallback);
                    final IBinder mRemote = this.mRemote;
                    boolean b = false;
                    mRemote.transact(3, obtain, obtain2, 0);
                    obtain2.readException();
                    if (obtain2.readInt() != 0) {
                        b = true;
                    }
                    return b;
                }
                finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
            
            @Override
            public boolean newSessionWithExtras(final ICustomTabsCallback customTabsCallback, final Bundle bundle) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                final Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("android.support.customtabs.ICustomTabsService");
                    obtain.writeStrongInterface((IInterface)customTabsCallback);
                    boolean b = false;
                    writeTypedObject(obtain, (Parcelable)bundle, 0);
                    this.mRemote.transact(10, obtain, obtain2, 0);
                    obtain2.readException();
                    if (obtain2.readInt() != 0) {
                        b = true;
                    }
                    return b;
                }
                finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
            
            @Override
            public int postMessage(final ICustomTabsCallback customTabsCallback, final String s, final Bundle bundle) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                final Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("android.support.customtabs.ICustomTabsService");
                    obtain.writeStrongInterface((IInterface)customTabsCallback);
                    obtain.writeString(s);
                    writeTypedObject(obtain, (Parcelable)bundle, 0);
                    this.mRemote.transact(8, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readInt();
                }
                finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
            
            @Override
            public boolean receiveFile(final ICustomTabsCallback customTabsCallback, final Uri uri, int int1, final Bundle bundle) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                final Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("android.support.customtabs.ICustomTabsService");
                    obtain.writeStrongInterface((IInterface)customTabsCallback);
                    boolean b = false;
                    writeTypedObject(obtain, (Parcelable)uri, 0);
                    obtain.writeInt(int1);
                    writeTypedObject(obtain, (Parcelable)bundle, 0);
                    this.mRemote.transact(12, obtain, obtain2, 0);
                    obtain2.readException();
                    int1 = obtain2.readInt();
                    if (int1 != 0) {
                        b = true;
                    }
                    return b;
                }
                finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
            
            @Override
            public boolean requestPostMessageChannel(final ICustomTabsCallback customTabsCallback, final Uri uri) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                final Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("android.support.customtabs.ICustomTabsService");
                    obtain.writeStrongInterface((IInterface)customTabsCallback);
                    boolean b = false;
                    writeTypedObject(obtain, (Parcelable)uri, 0);
                    this.mRemote.transact(7, obtain, obtain2, 0);
                    obtain2.readException();
                    if (obtain2.readInt() != 0) {
                        b = true;
                    }
                    return b;
                }
                finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
            
            @Override
            public boolean requestPostMessageChannelWithExtras(final ICustomTabsCallback customTabsCallback, final Uri uri, final Bundle bundle) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                final Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("android.support.customtabs.ICustomTabsService");
                    obtain.writeStrongInterface((IInterface)customTabsCallback);
                    boolean b = false;
                    writeTypedObject(obtain, (Parcelable)uri, 0);
                    writeTypedObject(obtain, (Parcelable)bundle, 0);
                    this.mRemote.transact(11, obtain, obtain2, 0);
                    obtain2.readException();
                    if (obtain2.readInt() != 0) {
                        b = true;
                    }
                    return b;
                }
                finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
            
            @Override
            public boolean updateVisuals(final ICustomTabsCallback customTabsCallback, final Bundle bundle) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                final Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("android.support.customtabs.ICustomTabsService");
                    obtain.writeStrongInterface((IInterface)customTabsCallback);
                    boolean b = false;
                    writeTypedObject(obtain, (Parcelable)bundle, 0);
                    this.mRemote.transact(6, obtain, obtain2, 0);
                    obtain2.readException();
                    if (obtain2.readInt() != 0) {
                        b = true;
                    }
                    return b;
                }
                finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
            
            @Override
            public boolean validateRelationship(final ICustomTabsCallback customTabsCallback, int int1, final Uri uri, final Bundle bundle) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                final Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("android.support.customtabs.ICustomTabsService");
                    obtain.writeStrongInterface((IInterface)customTabsCallback);
                    obtain.writeInt(int1);
                    boolean b = false;
                    writeTypedObject(obtain, (Parcelable)uri, 0);
                    writeTypedObject(obtain, (Parcelable)bundle, 0);
                    this.mRemote.transact(9, obtain, obtain2, 0);
                    obtain2.readException();
                    int1 = obtain2.readInt();
                    if (int1 != 0) {
                        b = true;
                    }
                    return b;
                }
                finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
            
            @Override
            public boolean warmup(final long n) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                final Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("android.support.customtabs.ICustomTabsService");
                    obtain.writeLong(n);
                    final IBinder mRemote = this.mRemote;
                    boolean b = false;
                    mRemote.transact(2, obtain, obtain2, 0);
                    obtain2.readException();
                    if (obtain2.readInt() != 0) {
                        b = true;
                    }
                    return b;
                }
                finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
        }
    }
    
    public static class _Parcel
    {
        private static <T> T readTypedObject(final Parcel parcel, final Parcelable$Creator<T> parcelable$Creator) {
            if (parcel.readInt() != 0) {
                return (T)parcelable$Creator.createFromParcel(parcel);
            }
            return null;
        }
        
        private static <T extends Parcelable> void writeTypedObject(final Parcel parcel, final T t, final int n) {
            if (t != null) {
                parcel.writeInt(1);
                t.writeToParcel(parcel, n);
            }
            else {
                parcel.writeInt(0);
            }
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package android.support.customtabs.trusted;

import android.os.Parcelable$Creator;
import android.os.Parcelable;
import android.os.Parcel;
import android.os.Binder;
import android.os.IBinder;
import android.os.RemoteException;
import android.os.Bundle;
import android.os.IInterface;

public interface ITrustedWebActivityCallback extends IInterface
{
    public static final String DESCRIPTOR = "android.support.customtabs.trusted.ITrustedWebActivityCallback";
    
    void onExtraCallback(final String p0, final Bundle p1) throws RemoteException;
    
    public static class Default implements ITrustedWebActivityCallback
    {
        public IBinder asBinder() {
            return null;
        }
        
        @Override
        public void onExtraCallback(final String s, final Bundle bundle) throws RemoteException {
        }
    }
    
    public abstract static class Stub extends Binder implements ITrustedWebActivityCallback
    {
        static final int TRANSACTION_onExtraCallback = 2;
        
        public Stub() {
            this.attachInterface((IInterface)this, "android.support.customtabs.trusted.ITrustedWebActivityCallback");
        }
        
        public static ITrustedWebActivityCallback asInterface(final IBinder binder) {
            if (binder == null) {
                return null;
            }
            final IInterface queryLocalInterface = binder.queryLocalInterface("android.support.customtabs.trusted.ITrustedWebActivityCallback");
            if (queryLocalInterface != null && queryLocalInterface instanceof ITrustedWebActivityCallback) {
                return (ITrustedWebActivityCallback)queryLocalInterface;
            }
            return new Proxy(binder);
        }
        
        public IBinder asBinder() {
            return (IBinder)this;
        }
        
        public boolean onTransact(final int n, final Parcel parcel, final Parcel parcel2, final int n2) throws RemoteException {
            if (n >= 1 && n <= 16777215) {
                parcel.enforceInterface("android.support.customtabs.trusted.ITrustedWebActivityCallback");
            }
            if (n == 1598968902) {
                parcel2.writeString("android.support.customtabs.trusted.ITrustedWebActivityCallback");
                return true;
            }
            if (n != 2) {
                return super.onTransact(n, parcel, parcel2, n2);
            }
            this.onExtraCallback(parcel.readString(), (Bundle)readTypedObject(parcel, (android.os.Parcelable$Creator<Object>)Bundle.CREATOR));
            parcel2.writeNoException();
            return true;
        }
        
        private static class Proxy implements ITrustedWebActivityCallback
        {
            private IBinder mRemote;
            
            Proxy(final IBinder mRemote) {
                this.mRemote = mRemote;
            }
            
            public IBinder asBinder() {
                return this.mRemote;
            }
            
            public String getInterfaceDescriptor() {
                return "android.support.customtabs.trusted.ITrustedWebActivityCallback";
            }
            
            @Override
            public void onExtraCallback(final String s, final Bundle bundle) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                final Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("android.support.customtabs.trusted.ITrustedWebActivityCallback");
                    obtain.writeString(s);
                    writeTypedObject(obtain, (Parcelable)bundle, 0);
                    this.mRemote.transact(2, obtain, obtain2, 0);
                    obtain2.readException();
                }
                finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
        }
    }
    
    public static class _Parcel
    {
        private static <T> T readTypedObject(final Parcel parcel, final Parcelable$Creator<T> parcelable$Creator) {
            if (parcel.readInt() != 0) {
                return (T)parcelable$Creator.createFromParcel(parcel);
            }
            return null;
        }
        
        private static <T extends Parcelable> void writeTypedObject(final Parcel parcel, final T t, final int n) {
            if (t != null) {
                parcel.writeInt(1);
                t.writeToParcel(parcel, n);
            }
            else {
                parcel.writeInt(0);
            }
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package android.support.customtabs.trusted;

import android.os.Parcelable$Creator;
import android.os.Parcelable;
import android.os.Parcel;
import android.os.Binder;
import android.os.IBinder;
import android.os.RemoteException;
import android.os.Bundle;
import android.os.IInterface;

public interface ITrustedWebActivityService extends IInterface
{
    public static final String DESCRIPTOR = "android.support.customtabs.trusted.ITrustedWebActivityService";
    
    Bundle areNotificationsEnabled(final Bundle p0) throws RemoteException;
    
    void cancelNotification(final Bundle p0) throws RemoteException;
    
    Bundle extraCommand(final String p0, final Bundle p1, final IBinder p2) throws RemoteException;
    
    Bundle getActiveNotifications() throws RemoteException;
    
    Bundle getSmallIconBitmap() throws RemoteException;
    
    int getSmallIconId() throws RemoteException;
    
    Bundle notifyNotificationWithChannel(final Bundle p0) throws RemoteException;
    
    public static class Default implements ITrustedWebActivityService
    {
        @Override
        public Bundle areNotificationsEnabled(final Bundle bundle) throws RemoteException {
            return null;
        }
        
        public IBinder asBinder() {
            return null;
        }
        
        @Override
        public void cancelNotification(final Bundle bundle) throws RemoteException {
        }
        
        @Override
        public Bundle extraCommand(final String s, final Bundle bundle, final IBinder binder) throws RemoteException {
            return null;
        }
        
        @Override
        public Bundle getActiveNotifications() throws RemoteException {
            return null;
        }
        
        @Override
        public Bundle getSmallIconBitmap() throws RemoteException {
            return null;
        }
        
        @Override
        public int getSmallIconId() throws RemoteException {
            return 0;
        }
        
        @Override
        public Bundle notifyNotificationWithChannel(final Bundle bundle) throws RemoteException {
            return null;
        }
    }
    
    public abstract static class Stub extends Binder implements ITrustedWebActivityService
    {
        static final int TRANSACTION_areNotificationsEnabled = 6;
        static final int TRANSACTION_cancelNotification = 3;
        static final int TRANSACTION_extraCommand = 9;
        static final int TRANSACTION_getActiveNotifications = 5;
        static final int TRANSACTION_getSmallIconBitmap = 7;
        static final int TRANSACTION_getSmallIconId = 4;
        static final int TRANSACTION_notifyNotificationWithChannel = 2;
        
        public Stub() {
            this.attachInterface((IInterface)this, "android.support.customtabs.trusted.ITrustedWebActivityService");
        }
        
        public static ITrustedWebActivityService asInterface(final IBinder binder) {
            if (binder == null) {
                return null;
            }
            final IInterface queryLocalInterface = binder.queryLocalInterface("android.support.customtabs.trusted.ITrustedWebActivityService");
            if (queryLocalInterface != null && queryLocalInterface instanceof ITrustedWebActivityService) {
                return (ITrustedWebActivityService)queryLocalInterface;
            }
            return new Proxy(binder);
        }
        
        public IBinder asBinder() {
            return (IBinder)this;
        }
        
        public boolean onTransact(int smallIconId, final Parcel parcel, final Parcel parcel2, final int n) throws RemoteException {
            if (smallIconId >= 1 && smallIconId <= 16777215) {
                parcel.enforceInterface("android.support.customtabs.trusted.ITrustedWebActivityService");
            }
            if (smallIconId != 1598968902) {
                switch (smallIconId) {
                    default: {
                        return super.onTransact(smallIconId, parcel, parcel2, n);
                    }
                    case 9: {
                        final Bundle extraCommand = this.extraCommand(parcel.readString(), (Bundle)readTypedObject(parcel, (android.os.Parcelable$Creator<Object>)Bundle.CREATOR), parcel.readStrongBinder());
                        parcel2.writeNoException();
                        writeTypedObject(parcel2, (Parcelable)extraCommand, 1);
                        break;
                    }
                    case 7: {
                        final Bundle smallIconBitmap = this.getSmallIconBitmap();
                        parcel2.writeNoException();
                        writeTypedObject(parcel2, (Parcelable)smallIconBitmap, 1);
                        break;
                    }
                    case 6: {
                        final Bundle notificationsEnabled = this.areNotificationsEnabled((Bundle)readTypedObject(parcel, (android.os.Parcelable$Creator<Object>)Bundle.CREATOR));
                        parcel2.writeNoException();
                        writeTypedObject(parcel2, (Parcelable)notificationsEnabled, 1);
                        break;
                    }
                    case 5: {
                        final Bundle activeNotifications = this.getActiveNotifications();
                        parcel2.writeNoException();
                        writeTypedObject(parcel2, (Parcelable)activeNotifications, 1);
                        break;
                    }
                    case 4: {
                        smallIconId = this.getSmallIconId();
                        parcel2.writeNoException();
                        parcel2.writeInt(smallIconId);
                        break;
                    }
                    case 3: {
                        this.cancelNotification((Bundle)readTypedObject(parcel, (android.os.Parcelable$Creator<Object>)Bundle.CREATOR));
                        parcel2.writeNoException();
                        break;
                    }
                    case 2: {
                        final Bundle notifyNotificationWithChannel = this.notifyNotificationWithChannel((Bundle)readTypedObject(parcel, (android.os.Parcelable$Creator<Object>)Bundle.CREATOR));
                        parcel2.writeNoException();
                        writeTypedObject(parcel2, (Parcelable)notifyNotificationWithChannel, 1);
                        break;
                    }
                }
                return true;
            }
            parcel2.writeString("android.support.customtabs.trusted.ITrustedWebActivityService");
            return true;
        }
        
        private static class Proxy implements ITrustedWebActivityService
        {
            private IBinder mRemote;
            
            Proxy(final IBinder mRemote) {
                this.mRemote = mRemote;
            }
            
            @Override
            public Bundle areNotificationsEnabled(Bundle bundle) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                final Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("android.support.customtabs.trusted.ITrustedWebActivityService");
                    writeTypedObject(obtain, (Parcelable)bundle, 0);
                    this.mRemote.transact(6, obtain, obtain2, 0);
                    obtain2.readException();
                    bundle = (Bundle)readTypedObject(obtain2, (android.os.Parcelable$Creator<Object>)Bundle.CREATOR);
                    return bundle;
                }
                finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
            
            public IBinder asBinder() {
                return this.mRemote;
            }
            
            @Override
            public void cancelNotification(final Bundle bundle) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                final Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("android.support.customtabs.trusted.ITrustedWebActivityService");
                    writeTypedObject(obtain, (Parcelable)bundle, 0);
                    this.mRemote.transact(3, obtain, obtain2, 0);
                    obtain2.readException();
                }
                finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
            
            @Override
            public Bundle extraCommand(final String s, final Bundle bundle, final IBinder binder) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                final Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("android.support.customtabs.trusted.ITrustedWebActivityService");
                    obtain.writeString(s);
                    writeTypedObject(obtain, (Parcelable)bundle, 0);
                    obtain.writeStrongBinder(binder);
                    this.mRemote.transact(9, obtain, obtain2, 0);
                    obtain2.readException();
                    return (Bundle)readTypedObject(obtain2, (android.os.Parcelable$Creator<Object>)Bundle.CREATOR);
                }
                finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
            
            @Override
            public Bundle getActiveNotifications() throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                final Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("android.support.customtabs.trusted.ITrustedWebActivityService");
                    this.mRemote.transact(5, obtain, obtain2, 0);
                    obtain2.readException();
                    return (Bundle)readTypedObject(obtain2, (android.os.Parcelable$Creator<Object>)Bundle.CREATOR);
                }
                finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
            
            public String getInterfaceDescriptor() {
                return "android.support.customtabs.trusted.ITrustedWebActivityService";
            }
            
            @Override
            public Bundle getSmallIconBitmap() throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                final Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("android.support.customtabs.trusted.ITrustedWebActivityService");
                    this.mRemote.transact(7, obtain, obtain2, 0);
                    obtain2.readException();
                    return (Bundle)readTypedObject(obtain2, (android.os.Parcelable$Creator<Object>)Bundle.CREATOR);
                }
                finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
            
            @Override
            public int getSmallIconId() throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                final Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("android.support.customtabs.trusted.ITrustedWebActivityService");
                    this.mRemote.transact(4, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readInt();
                }
                finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
            
            @Override
            public Bundle notifyNotificationWithChannel(Bundle bundle) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                final Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("android.support.customtabs.trusted.ITrustedWebActivityService");
                    writeTypedObject(obtain, (Parcelable)bundle, 0);
                    this.mRemote.transact(2, obtain, obtain2, 0);
                    obtain2.readException();
                    bundle = (Bundle)readTypedObject(obtain2, (android.os.Parcelable$Creator<Object>)Bundle.CREATOR);
                    return bundle;
                }
                finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
        }
    }
    
    public static class _Parcel
    {
        private static <T> T readTypedObject(final Parcel parcel, final Parcelable$Creator<T> parcelable$Creator) {
            if (parcel.readInt() != 0) {
                return (T)parcelable$Creator.createFromParcel(parcel);
            }
            return null;
        }
        
        private static <T extends Parcelable> void writeTypedObject(final Parcel parcel, final T t, final int n) {
            if (t != null) {
                parcel.writeInt(1);
                t.writeToParcel(parcel, n);
            }
            else {
                parcel.writeInt(0);
            }
        }
    }
}

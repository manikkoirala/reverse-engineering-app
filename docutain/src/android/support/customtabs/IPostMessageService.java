// 
// Decompiled by Procyon v0.6.0
// 

package android.support.customtabs;

import android.os.Parcelable$Creator;
import android.os.Parcelable;
import android.os.Parcel;
import android.os.Binder;
import android.os.IBinder;
import android.os.RemoteException;
import android.os.Bundle;
import android.os.IInterface;

public interface IPostMessageService extends IInterface
{
    public static final String DESCRIPTOR = "android.support.customtabs.IPostMessageService";
    
    void onMessageChannelReady(final ICustomTabsCallback p0, final Bundle p1) throws RemoteException;
    
    void onPostMessage(final ICustomTabsCallback p0, final String p1, final Bundle p2) throws RemoteException;
    
    public static class Default implements IPostMessageService
    {
        public IBinder asBinder() {
            return null;
        }
        
        @Override
        public void onMessageChannelReady(final ICustomTabsCallback customTabsCallback, final Bundle bundle) throws RemoteException {
        }
        
        @Override
        public void onPostMessage(final ICustomTabsCallback customTabsCallback, final String s, final Bundle bundle) throws RemoteException {
        }
    }
    
    public abstract static class Stub extends Binder implements IPostMessageService
    {
        static final int TRANSACTION_onMessageChannelReady = 2;
        static final int TRANSACTION_onPostMessage = 3;
        
        public Stub() {
            this.attachInterface((IInterface)this, "android.support.customtabs.IPostMessageService");
        }
        
        public static IPostMessageService asInterface(final IBinder binder) {
            if (binder == null) {
                return null;
            }
            final IInterface queryLocalInterface = binder.queryLocalInterface("android.support.customtabs.IPostMessageService");
            if (queryLocalInterface != null && queryLocalInterface instanceof IPostMessageService) {
                return (IPostMessageService)queryLocalInterface;
            }
            return new Proxy(binder);
        }
        
        public IBinder asBinder() {
            return (IBinder)this;
        }
        
        public boolean onTransact(final int n, final Parcel parcel, final Parcel parcel2, final int n2) throws RemoteException {
            if (n >= 1 && n <= 16777215) {
                parcel.enforceInterface("android.support.customtabs.IPostMessageService");
            }
            if (n != 1598968902) {
                if (n != 2) {
                    if (n != 3) {
                        return super.onTransact(n, parcel, parcel2, n2);
                    }
                    this.onPostMessage(ICustomTabsCallback.Stub.asInterface(parcel.readStrongBinder()), parcel.readString(), (Bundle)readTypedObject(parcel, (android.os.Parcelable$Creator<Object>)Bundle.CREATOR));
                    parcel2.writeNoException();
                }
                else {
                    this.onMessageChannelReady(ICustomTabsCallback.Stub.asInterface(parcel.readStrongBinder()), (Bundle)readTypedObject(parcel, (android.os.Parcelable$Creator<Object>)Bundle.CREATOR));
                    parcel2.writeNoException();
                }
                return true;
            }
            parcel2.writeString("android.support.customtabs.IPostMessageService");
            return true;
        }
        
        private static class Proxy implements IPostMessageService
        {
            private IBinder mRemote;
            
            Proxy(final IBinder mRemote) {
                this.mRemote = mRemote;
            }
            
            public IBinder asBinder() {
                return this.mRemote;
            }
            
            public String getInterfaceDescriptor() {
                return "android.support.customtabs.IPostMessageService";
            }
            
            @Override
            public void onMessageChannelReady(final ICustomTabsCallback customTabsCallback, final Bundle bundle) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                final Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("android.support.customtabs.IPostMessageService");
                    obtain.writeStrongInterface((IInterface)customTabsCallback);
                    writeTypedObject(obtain, (Parcelable)bundle, 0);
                    this.mRemote.transact(2, obtain, obtain2, 0);
                    obtain2.readException();
                }
                finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
            
            @Override
            public void onPostMessage(final ICustomTabsCallback customTabsCallback, final String s, final Bundle bundle) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                final Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("android.support.customtabs.IPostMessageService");
                    obtain.writeStrongInterface((IInterface)customTabsCallback);
                    obtain.writeString(s);
                    writeTypedObject(obtain, (Parcelable)bundle, 0);
                    this.mRemote.transact(3, obtain, obtain2, 0);
                    obtain2.readException();
                }
                finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
        }
    }
    
    public static class _Parcel
    {
        private static <T> T readTypedObject(final Parcel parcel, final Parcelable$Creator<T> parcelable$Creator) {
            if (parcel.readInt() != 0) {
                return (T)parcelable$Creator.createFromParcel(parcel);
            }
            return null;
        }
        
        private static <T extends Parcelable> void writeTypedObject(final Parcel parcel, final T t, final int n) {
            if (t != null) {
                parcel.writeInt(1);
                t.writeToParcel(parcel, n);
            }
            else {
                parcel.writeInt(0);
            }
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package android.support.customtabs;

import android.os.Parcelable$Creator;
import android.os.Parcelable;
import android.os.Parcel;
import android.os.Binder;
import android.os.IBinder;
import android.net.Uri;
import android.os.RemoteException;
import android.os.Bundle;
import android.os.IInterface;

public interface ICustomTabsCallback extends IInterface
{
    public static final String DESCRIPTOR = "android.support.customtabs.ICustomTabsCallback";
    
    void extraCallback(final String p0, final Bundle p1) throws RemoteException;
    
    Bundle extraCallbackWithResult(final String p0, final Bundle p1) throws RemoteException;
    
    void onActivityResized(final int p0, final int p1, final Bundle p2) throws RemoteException;
    
    void onMessageChannelReady(final Bundle p0) throws RemoteException;
    
    void onNavigationEvent(final int p0, final Bundle p1) throws RemoteException;
    
    void onPostMessage(final String p0, final Bundle p1) throws RemoteException;
    
    void onRelationshipValidationResult(final int p0, final Uri p1, final boolean p2, final Bundle p3) throws RemoteException;
    
    public static class Default implements ICustomTabsCallback
    {
        public IBinder asBinder() {
            return null;
        }
        
        @Override
        public void extraCallback(final String s, final Bundle bundle) throws RemoteException {
        }
        
        @Override
        public Bundle extraCallbackWithResult(final String s, final Bundle bundle) throws RemoteException {
            return null;
        }
        
        @Override
        public void onActivityResized(final int n, final int n2, final Bundle bundle) throws RemoteException {
        }
        
        @Override
        public void onMessageChannelReady(final Bundle bundle) throws RemoteException {
        }
        
        @Override
        public void onNavigationEvent(final int n, final Bundle bundle) throws RemoteException {
        }
        
        @Override
        public void onPostMessage(final String s, final Bundle bundle) throws RemoteException {
        }
        
        @Override
        public void onRelationshipValidationResult(final int n, final Uri uri, final boolean b, final Bundle bundle) throws RemoteException {
        }
    }
    
    public abstract static class Stub extends Binder implements ICustomTabsCallback
    {
        static final int TRANSACTION_extraCallback = 3;
        static final int TRANSACTION_extraCallbackWithResult = 7;
        static final int TRANSACTION_onActivityResized = 8;
        static final int TRANSACTION_onMessageChannelReady = 4;
        static final int TRANSACTION_onNavigationEvent = 2;
        static final int TRANSACTION_onPostMessage = 5;
        static final int TRANSACTION_onRelationshipValidationResult = 6;
        
        public Stub() {
            this.attachInterface((IInterface)this, "android.support.customtabs.ICustomTabsCallback");
        }
        
        public static ICustomTabsCallback asInterface(final IBinder binder) {
            if (binder == null) {
                return null;
            }
            final IInterface queryLocalInterface = binder.queryLocalInterface("android.support.customtabs.ICustomTabsCallback");
            if (queryLocalInterface != null && queryLocalInterface instanceof ICustomTabsCallback) {
                return (ICustomTabsCallback)queryLocalInterface;
            }
            return new Proxy(binder);
        }
        
        public IBinder asBinder() {
            return (IBinder)this;
        }
        
        public boolean onTransact(int int1, final Parcel parcel, final Parcel parcel2, final int n) throws RemoteException {
            if (int1 >= 1 && int1 <= 16777215) {
                parcel.enforceInterface("android.support.customtabs.ICustomTabsCallback");
            }
            if (int1 != 1598968902) {
                switch (int1) {
                    default: {
                        return super.onTransact(int1, parcel, parcel2, n);
                    }
                    case 8: {
                        this.onActivityResized(parcel.readInt(), parcel.readInt(), (Bundle)readTypedObject(parcel, (android.os.Parcelable$Creator<Object>)Bundle.CREATOR));
                        break;
                    }
                    case 7: {
                        final Bundle extraCallbackWithResult = this.extraCallbackWithResult(parcel.readString(), (Bundle)readTypedObject(parcel, (android.os.Parcelable$Creator<Object>)Bundle.CREATOR));
                        parcel2.writeNoException();
                        writeTypedObject(parcel2, (Parcelable)extraCallbackWithResult, 1);
                        break;
                    }
                    case 6: {
                        int1 = parcel.readInt();
                        this.onRelationshipValidationResult(int1, (Uri)readTypedObject(parcel, (android.os.Parcelable$Creator<Object>)Uri.CREATOR), parcel.readInt() != 0, (Bundle)readTypedObject(parcel, (android.os.Parcelable$Creator<Object>)Bundle.CREATOR));
                        break;
                    }
                    case 5: {
                        this.onPostMessage(parcel.readString(), (Bundle)readTypedObject(parcel, (android.os.Parcelable$Creator<Object>)Bundle.CREATOR));
                        parcel2.writeNoException();
                        break;
                    }
                    case 4: {
                        this.onMessageChannelReady((Bundle)readTypedObject(parcel, (android.os.Parcelable$Creator<Object>)Bundle.CREATOR));
                        parcel2.writeNoException();
                        break;
                    }
                    case 3: {
                        this.extraCallback(parcel.readString(), (Bundle)readTypedObject(parcel, (android.os.Parcelable$Creator<Object>)Bundle.CREATOR));
                        break;
                    }
                    case 2: {
                        this.onNavigationEvent(parcel.readInt(), (Bundle)readTypedObject(parcel, (android.os.Parcelable$Creator<Object>)Bundle.CREATOR));
                        break;
                    }
                }
                return true;
            }
            parcel2.writeString("android.support.customtabs.ICustomTabsCallback");
            return true;
        }
        
        private static class Proxy implements ICustomTabsCallback
        {
            private IBinder mRemote;
            
            Proxy(final IBinder mRemote) {
                this.mRemote = mRemote;
            }
            
            public IBinder asBinder() {
                return this.mRemote;
            }
            
            @Override
            public void extraCallback(final String s, final Bundle bundle) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("android.support.customtabs.ICustomTabsCallback");
                    obtain.writeString(s);
                    writeTypedObject(obtain, (Parcelable)bundle, 0);
                    this.mRemote.transact(3, obtain, (Parcel)null, 1);
                }
                finally {
                    obtain.recycle();
                }
            }
            
            @Override
            public Bundle extraCallbackWithResult(final String s, final Bundle bundle) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                final Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("android.support.customtabs.ICustomTabsCallback");
                    obtain.writeString(s);
                    writeTypedObject(obtain, (Parcelable)bundle, 0);
                    this.mRemote.transact(7, obtain, obtain2, 0);
                    obtain2.readException();
                    return (Bundle)readTypedObject(obtain2, (android.os.Parcelable$Creator<Object>)Bundle.CREATOR);
                }
                finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
            
            public String getInterfaceDescriptor() {
                return "android.support.customtabs.ICustomTabsCallback";
            }
            
            @Override
            public void onActivityResized(final int n, final int n2, final Bundle bundle) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("android.support.customtabs.ICustomTabsCallback");
                    obtain.writeInt(n);
                    obtain.writeInt(n2);
                    writeTypedObject(obtain, (Parcelable)bundle, 0);
                    this.mRemote.transact(8, obtain, (Parcel)null, 1);
                }
                finally {
                    obtain.recycle();
                }
            }
            
            @Override
            public void onMessageChannelReady(final Bundle bundle) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                final Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("android.support.customtabs.ICustomTabsCallback");
                    writeTypedObject(obtain, (Parcelable)bundle, 0);
                    this.mRemote.transact(4, obtain, obtain2, 0);
                    obtain2.readException();
                }
                finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
            
            @Override
            public void onNavigationEvent(final int n, final Bundle bundle) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("android.support.customtabs.ICustomTabsCallback");
                    obtain.writeInt(n);
                    writeTypedObject(obtain, (Parcelable)bundle, 0);
                    this.mRemote.transact(2, obtain, (Parcel)null, 1);
                }
                finally {
                    obtain.recycle();
                }
            }
            
            @Override
            public void onPostMessage(final String s, final Bundle bundle) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                final Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("android.support.customtabs.ICustomTabsCallback");
                    obtain.writeString(s);
                    writeTypedObject(obtain, (Parcelable)bundle, 0);
                    this.mRemote.transact(5, obtain, obtain2, 0);
                    obtain2.readException();
                }
                finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
            
            @Override
            public void onRelationshipValidationResult(int n, final Uri uri, final boolean b, final Bundle bundle) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("android.support.customtabs.ICustomTabsCallback");
                    obtain.writeInt(n);
                    writeTypedObject(obtain, (Parcelable)uri, 0);
                    if (b) {
                        n = 1;
                    }
                    else {
                        n = 0;
                    }
                    obtain.writeInt(n);
                    writeTypedObject(obtain, (Parcelable)bundle, 0);
                    this.mRemote.transact(6, obtain, (Parcel)null, 1);
                }
                finally {
                    obtain.recycle();
                }
            }
        }
    }
    
    public static class _Parcel
    {
        private static <T> T readTypedObject(final Parcel parcel, final Parcelable$Creator<T> parcelable$Creator) {
            if (parcel.readInt() != 0) {
                return (T)parcelable$Creator.createFromParcel(parcel);
            }
            return null;
        }
        
        private static <T extends Parcelable> void writeTypedObject(final Parcel parcel, final T t, final int n) {
            if (t != null) {
                parcel.writeInt(1);
                t.writeToParcel(parcel, n);
            }
            else {
                parcel.writeInt(0);
            }
        }
    }
}

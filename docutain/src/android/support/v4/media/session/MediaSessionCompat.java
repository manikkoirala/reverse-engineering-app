// 
// Decompiled by Procyon v0.6.0
// 

package android.support.v4.media.session;

import android.media.MediaMetadataEditor;
import android.media.session.MediaSession$Token;
import android.media.MediaDescription;
import android.os.Parcel;
import android.media.session.MediaSession$QueueItem;
import android.os.Parcelable$Creator;
import android.media.VolumeProvider;
import android.media.AudioAttributes$Builder;
import android.media.MediaMetadata;
import java.lang.reflect.Field;
import android.media.session.MediaSession;
import android.media.RemoteControlClient$OnMetadataUpdateListener;
import android.os.Parcelable;
import android.os.IInterface;
import android.os.RemoteException;
import android.os.Binder;
import android.graphics.Bitmap;
import android.media.RemoteControlClient$MetadataEditor;
import android.media.RemoteControlClient;
import android.os.RemoteCallbackList;
import android.media.AudioManager;
import android.media.RemoteControlClient$OnPlaybackPositionUpdateListener;
import android.media.Rating;
import android.os.IBinder;
import androidx.versionedparcelable.ParcelUtils;
import androidx.core.app.BundleCompat;
import android.os.Message;
import android.support.v4.media.RatingCompat;
import android.net.Uri;
import android.view.ViewConfiguration;
import android.view.KeyEvent;
import android.os.ResultReceiver;
import android.support.v4.media.MediaDescriptionCompat;
import java.lang.ref.WeakReference;
import android.media.session.MediaSession$Callback;
import java.util.HashSet;
import java.util.List;
import androidx.media.VolumeProviderCompat;
import java.util.Iterator;
import androidx.media.MediaSessionManager;
import android.os.BadParcelableException;
import android.os.SystemClock;
import android.support.v4.media.MediaMetadataCompat;
import android.util.TypedValue;
import android.os.Handler;
import android.os.Looper;
import android.os.Build$VERSION;
import android.content.Intent;
import android.util.Log;
import androidx.media.session.MediaButtonReceiver;
import android.text.TextUtils;
import androidx.versionedparcelable.VersionedParcelable;
import android.os.Bundle;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import java.util.ArrayList;

public class MediaSessionCompat
{
    public static final String ACTION_ARGUMENT_CAPTIONING_ENABLED = "android.support.v4.media.session.action.ARGUMENT_CAPTIONING_ENABLED";
    public static final String ACTION_ARGUMENT_EXTRAS = "android.support.v4.media.session.action.ARGUMENT_EXTRAS";
    public static final String ACTION_ARGUMENT_MEDIA_ID = "android.support.v4.media.session.action.ARGUMENT_MEDIA_ID";
    public static final String ACTION_ARGUMENT_PLAYBACK_SPEED = "android.support.v4.media.session.action.ARGUMENT_PLAYBACK_SPEED";
    public static final String ACTION_ARGUMENT_QUERY = "android.support.v4.media.session.action.ARGUMENT_QUERY";
    public static final String ACTION_ARGUMENT_RATING = "android.support.v4.media.session.action.ARGUMENT_RATING";
    public static final String ACTION_ARGUMENT_REPEAT_MODE = "android.support.v4.media.session.action.ARGUMENT_REPEAT_MODE";
    public static final String ACTION_ARGUMENT_SHUFFLE_MODE = "android.support.v4.media.session.action.ARGUMENT_SHUFFLE_MODE";
    public static final String ACTION_ARGUMENT_URI = "android.support.v4.media.session.action.ARGUMENT_URI";
    public static final String ACTION_FLAG_AS_INAPPROPRIATE = "android.support.v4.media.session.action.FLAG_AS_INAPPROPRIATE";
    public static final String ACTION_FOLLOW = "android.support.v4.media.session.action.FOLLOW";
    public static final String ACTION_PLAY_FROM_URI = "android.support.v4.media.session.action.PLAY_FROM_URI";
    public static final String ACTION_PREPARE = "android.support.v4.media.session.action.PREPARE";
    public static final String ACTION_PREPARE_FROM_MEDIA_ID = "android.support.v4.media.session.action.PREPARE_FROM_MEDIA_ID";
    public static final String ACTION_PREPARE_FROM_SEARCH = "android.support.v4.media.session.action.PREPARE_FROM_SEARCH";
    public static final String ACTION_PREPARE_FROM_URI = "android.support.v4.media.session.action.PREPARE_FROM_URI";
    public static final String ACTION_SET_CAPTIONING_ENABLED = "android.support.v4.media.session.action.SET_CAPTIONING_ENABLED";
    public static final String ACTION_SET_PLAYBACK_SPEED = "android.support.v4.media.session.action.SET_PLAYBACK_SPEED";
    public static final String ACTION_SET_RATING = "android.support.v4.media.session.action.SET_RATING";
    public static final String ACTION_SET_REPEAT_MODE = "android.support.v4.media.session.action.SET_REPEAT_MODE";
    public static final String ACTION_SET_SHUFFLE_MODE = "android.support.v4.media.session.action.SET_SHUFFLE_MODE";
    public static final String ACTION_SKIP_AD = "android.support.v4.media.session.action.SKIP_AD";
    public static final String ACTION_UNFOLLOW = "android.support.v4.media.session.action.UNFOLLOW";
    public static final String ARGUMENT_MEDIA_ATTRIBUTE = "android.support.v4.media.session.ARGUMENT_MEDIA_ATTRIBUTE";
    public static final String ARGUMENT_MEDIA_ATTRIBUTE_VALUE = "android.support.v4.media.session.ARGUMENT_MEDIA_ATTRIBUTE_VALUE";
    private static final String DATA_CALLING_PACKAGE = "data_calling_pkg";
    private static final String DATA_CALLING_PID = "data_calling_pid";
    private static final String DATA_CALLING_UID = "data_calling_uid";
    private static final String DATA_EXTRAS = "data_extras";
    @Deprecated
    public static final int FLAG_HANDLES_MEDIA_BUTTONS = 1;
    public static final int FLAG_HANDLES_QUEUE_COMMANDS = 4;
    @Deprecated
    public static final int FLAG_HANDLES_TRANSPORT_CONTROLS = 2;
    public static final String KEY_EXTRA_BINDER = "android.support.v4.media.session.EXTRA_BINDER";
    public static final String KEY_SESSION2_TOKEN = "android.support.v4.media.session.SESSION_TOKEN2";
    public static final String KEY_TOKEN = "android.support.v4.media.session.TOKEN";
    private static final int MAX_BITMAP_SIZE_IN_DP = 320;
    public static final int MEDIA_ATTRIBUTE_ALBUM = 1;
    public static final int MEDIA_ATTRIBUTE_ARTIST = 0;
    public static final int MEDIA_ATTRIBUTE_PLAYLIST = 2;
    static final String TAG = "MediaSessionCompat";
    static int sMaxBitmapSize;
    private final ArrayList<OnActiveChangeListener> mActiveListeners;
    private final MediaControllerCompat mController;
    private final MediaSessionImpl mImpl;
    
    private MediaSessionCompat(final Context context, final MediaSessionImpl mImpl) {
        this.mActiveListeners = new ArrayList<OnActiveChangeListener>();
        this.mImpl = mImpl;
        this.mController = new MediaControllerCompat(context, this);
    }
    
    public MediaSessionCompat(final Context context, final String s) {
        this(context, s, null, null);
    }
    
    public MediaSessionCompat(final Context context, final String s, final ComponentName componentName, final PendingIntent pendingIntent) {
        this(context, s, componentName, pendingIntent, null);
    }
    
    public MediaSessionCompat(final Context context, final String s, final ComponentName componentName, final PendingIntent pendingIntent, final Bundle bundle) {
        this(context, s, componentName, pendingIntent, bundle, null);
    }
    
    public MediaSessionCompat(final Context context, final String s, ComponentName mediaButtonReceiverComponent, final PendingIntent pendingIntent, final Bundle bundle, final VersionedParcelable versionedParcelable) {
        this.mActiveListeners = new ArrayList<OnActiveChangeListener>();
        if (context == null) {
            throw new IllegalArgumentException("context must not be null");
        }
        if (!TextUtils.isEmpty((CharSequence)s)) {
            ComponentName component;
            if ((component = mediaButtonReceiverComponent) == null) {
                mediaButtonReceiverComponent = MediaButtonReceiver.getMediaButtonReceiverComponent(context);
                if ((component = mediaButtonReceiverComponent) == null) {
                    Log.w("MediaSessionCompat", "Couldn't find a unique registered media button receiver in the given context.");
                    component = mediaButtonReceiverComponent;
                }
            }
            PendingIntent broadcast = pendingIntent;
            if (component != null && (broadcast = pendingIntent) == null) {
                final Intent intent = new Intent("android.intent.action.MEDIA_BUTTON");
                intent.setComponent(component);
                int n;
                if (Build$VERSION.SDK_INT >= 31) {
                    n = 33554432;
                }
                else {
                    n = 0;
                }
                broadcast = PendingIntent.getBroadcast(context, 0, intent, n);
            }
            if (Build$VERSION.SDK_INT >= 21) {
                if (Build$VERSION.SDK_INT >= 29) {
                    this.mImpl = (MediaSessionImpl)new MediaSessionImplApi29(context, s, versionedParcelable, bundle);
                }
                else if (Build$VERSION.SDK_INT >= 28) {
                    this.mImpl = (MediaSessionImpl)new MediaSessionImplApi28(context, s, versionedParcelable, bundle);
                }
                else if (Build$VERSION.SDK_INT >= 22) {
                    this.mImpl = (MediaSessionImpl)new MediaSessionImplApi22(context, s, versionedParcelable, bundle);
                }
                else {
                    this.mImpl = (MediaSessionImpl)new MediaSessionImplApi21(context, s, versionedParcelable, bundle);
                }
                Looper looper;
                if (Looper.myLooper() != null) {
                    looper = Looper.myLooper();
                }
                else {
                    looper = Looper.getMainLooper();
                }
                this.setCallback((Callback)new Callback(this) {
                    final MediaSessionCompat this$0;
                }, new Handler(looper));
                this.mImpl.setMediaButtonReceiver(broadcast);
            }
            else if (Build$VERSION.SDK_INT >= 19) {
                this.mImpl = (MediaSessionImpl)new MediaSessionImplApi19(context, s, component, broadcast, versionedParcelable, bundle);
            }
            else if (Build$VERSION.SDK_INT >= 18) {
                this.mImpl = (MediaSessionImpl)new MediaSessionImplApi18(context, s, component, broadcast, versionedParcelable, bundle);
            }
            else {
                this.mImpl = (MediaSessionImpl)new MediaSessionImplBase(context, s, component, broadcast, versionedParcelable, bundle);
            }
            this.mController = new MediaControllerCompat(context, this);
            if (MediaSessionCompat.sMaxBitmapSize == 0) {
                MediaSessionCompat.sMaxBitmapSize = (int)(TypedValue.applyDimension(1, 320.0f, context.getResources().getDisplayMetrics()) + 0.5f);
            }
            return;
        }
        throw new IllegalArgumentException("tag must not be null or empty");
    }
    
    public static void ensureClassLoader(final Bundle bundle) {
        if (bundle != null) {
            bundle.setClassLoader(MediaSessionCompat.class.getClassLoader());
        }
    }
    
    public static MediaSessionCompat fromMediaSession(final Context context, final Object o) {
        if (Build$VERSION.SDK_INT >= 21 && context != null && o != null) {
            MediaSessionImpl mediaSessionImpl;
            if (Build$VERSION.SDK_INT >= 29) {
                mediaSessionImpl = new MediaSessionImplApi29(o);
            }
            else if (Build$VERSION.SDK_INT >= 28) {
                mediaSessionImpl = new MediaSessionImplApi28(o);
            }
            else {
                mediaSessionImpl = new MediaSessionImplApi21(o);
            }
            return new MediaSessionCompat(context, mediaSessionImpl);
        }
        return null;
    }
    
    static PlaybackStateCompat getStateWithUpdatedPosition(final PlaybackStateCompat playbackStateCompat, final MediaMetadataCompat mediaMetadataCompat) {
        PlaybackStateCompat build = playbackStateCompat;
        if (playbackStateCompat != null) {
            final long position = playbackStateCompat.getPosition();
            final long n = -1L;
            if (position == -1L) {
                build = playbackStateCompat;
            }
            else {
                if (playbackStateCompat.getState() != 3 && playbackStateCompat.getState() != 4) {
                    build = playbackStateCompat;
                    if (playbackStateCompat.getState() != 5) {
                        return build;
                    }
                }
                final long lastPositionUpdateTime = playbackStateCompat.getLastPositionUpdateTime();
                build = playbackStateCompat;
                if (lastPositionUpdateTime > 0L) {
                    final long elapsedRealtime = SystemClock.elapsedRealtime();
                    final long n2 = (long)(playbackStateCompat.getPlaybackSpeed() * (elapsedRealtime - lastPositionUpdateTime)) + playbackStateCompat.getPosition();
                    long long1 = n;
                    if (mediaMetadataCompat != null) {
                        long1 = n;
                        if (mediaMetadataCompat.containsKey("android.media.metadata.DURATION")) {
                            long1 = mediaMetadataCompat.getLong("android.media.metadata.DURATION");
                        }
                    }
                    if (long1 < 0L || n2 <= long1) {
                        if (n2 < 0L) {
                            long1 = 0L;
                        }
                        else {
                            long1 = n2;
                        }
                    }
                    build = new PlaybackStateCompat.Builder(playbackStateCompat).setState(playbackStateCompat.getState(), long1, playbackStateCompat.getPlaybackSpeed(), elapsedRealtime).build();
                }
            }
        }
        return build;
    }
    
    public static Bundle unparcelWithClassLoader(final Bundle bundle) {
        if (bundle == null) {
            return null;
        }
        ensureClassLoader(bundle);
        try {
            bundle.isEmpty();
            return bundle;
        }
        catch (final BadParcelableException ex) {
            Log.e("MediaSessionCompat", "Could not unparcel the data.");
            return null;
        }
    }
    
    public void addOnActiveChangeListener(final OnActiveChangeListener e) {
        if (e != null) {
            this.mActiveListeners.add(e);
            return;
        }
        throw new IllegalArgumentException("Listener may not be null");
    }
    
    public String getCallingPackage() {
        return this.mImpl.getCallingPackage();
    }
    
    public MediaControllerCompat getController() {
        return this.mController;
    }
    
    public final MediaSessionManager.RemoteUserInfo getCurrentControllerInfo() {
        return this.mImpl.getCurrentControllerInfo();
    }
    
    public Object getMediaSession() {
        return this.mImpl.getMediaSession();
    }
    
    public Object getRemoteControlClient() {
        return this.mImpl.getRemoteControlClient();
    }
    
    public Token getSessionToken() {
        return this.mImpl.getSessionToken();
    }
    
    public boolean isActive() {
        return this.mImpl.isActive();
    }
    
    public void release() {
        this.mImpl.release();
    }
    
    public void removeOnActiveChangeListener(final OnActiveChangeListener o) {
        if (o != null) {
            this.mActiveListeners.remove(o);
            return;
        }
        throw new IllegalArgumentException("Listener may not be null");
    }
    
    public void sendSessionEvent(final String s, final Bundle bundle) {
        if (!TextUtils.isEmpty((CharSequence)s)) {
            this.mImpl.sendSessionEvent(s, bundle);
            return;
        }
        throw new IllegalArgumentException("event cannot be null or empty");
    }
    
    public void setActive(final boolean active) {
        this.mImpl.setActive(active);
        final Iterator<OnActiveChangeListener> iterator = this.mActiveListeners.iterator();
        while (iterator.hasNext()) {
            iterator.next().onActiveChanged();
        }
    }
    
    public void setCallback(final Callback callback) {
        this.setCallback(callback, null);
    }
    
    public void setCallback(final Callback callback, Handler handler) {
        if (callback == null) {
            this.mImpl.setCallback(null, null);
        }
        else {
            final MediaSessionImpl mImpl = this.mImpl;
            if (handler == null) {
                handler = new Handler();
            }
            mImpl.setCallback(callback, handler);
        }
    }
    
    public void setCaptioningEnabled(final boolean captioningEnabled) {
        this.mImpl.setCaptioningEnabled(captioningEnabled);
    }
    
    public void setExtras(final Bundle extras) {
        this.mImpl.setExtras(extras);
    }
    
    public void setFlags(final int flags) {
        this.mImpl.setFlags(flags);
    }
    
    public void setMediaButtonReceiver(final PendingIntent mediaButtonReceiver) {
        this.mImpl.setMediaButtonReceiver(mediaButtonReceiver);
    }
    
    public void setMetadata(final MediaMetadataCompat metadata) {
        this.mImpl.setMetadata(metadata);
    }
    
    public void setPlaybackState(final PlaybackStateCompat playbackState) {
        this.mImpl.setPlaybackState(playbackState);
    }
    
    public void setPlaybackToLocal(final int playbackToLocal) {
        this.mImpl.setPlaybackToLocal(playbackToLocal);
    }
    
    public void setPlaybackToRemote(final VolumeProviderCompat playbackToRemote) {
        if (playbackToRemote != null) {
            this.mImpl.setPlaybackToRemote(playbackToRemote);
            return;
        }
        throw new IllegalArgumentException("volumeProvider may not be null!");
    }
    
    public void setQueue(final List<QueueItem> queue) {
        if (queue != null) {
            final HashSet set = new HashSet();
            for (final QueueItem queueItem : queue) {
                if (queueItem == null) {
                    throw new IllegalArgumentException("queue shouldn't have null items");
                }
                if (set.contains(queueItem.getQueueId())) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Found duplicate queue id: ");
                    sb.append(queueItem.getQueueId());
                    Log.e("MediaSessionCompat", sb.toString(), (Throwable)new IllegalArgumentException("id of each queue item should be unique"));
                }
                set.add(queueItem.getQueueId());
            }
        }
        this.mImpl.setQueue(queue);
    }
    
    public void setQueueTitle(final CharSequence queueTitle) {
        this.mImpl.setQueueTitle(queueTitle);
    }
    
    public void setRatingType(final int ratingType) {
        this.mImpl.setRatingType(ratingType);
    }
    
    public void setRegistrationCallback(final RegistrationCallback registrationCallback, final Handler handler) {
        this.mImpl.setRegistrationCallback(registrationCallback, handler);
    }
    
    public void setRepeatMode(final int repeatMode) {
        this.mImpl.setRepeatMode(repeatMode);
    }
    
    public void setSessionActivity(final PendingIntent sessionActivity) {
        this.mImpl.setSessionActivity(sessionActivity);
    }
    
    public void setShuffleMode(final int shuffleMode) {
        this.mImpl.setShuffleMode(shuffleMode);
    }
    
    public abstract static class Callback
    {
        final MediaSession$Callback mCallbackFwk;
        CallbackHandler mCallbackHandler;
        final Object mLock;
        private boolean mMediaPlayPausePendingOnHandler;
        WeakReference<MediaSessionImpl> mSessionImpl;
        
        public Callback() {
            this.mLock = new Object();
            if (Build$VERSION.SDK_INT >= 21) {
                this.mCallbackFwk = new MediaSessionCallbackApi21();
            }
            else {
                this.mCallbackFwk = null;
            }
            this.mSessionImpl = new WeakReference<MediaSessionImpl>(null);
        }
        
        void handleMediaPlayPauseIfPendingOnHandler(final MediaSessionImpl mediaSessionImpl, final Handler handler) {
            if (!this.mMediaPlayPausePendingOnHandler) {
                return;
            }
            boolean b = false;
            this.mMediaPlayPausePendingOnHandler = false;
            handler.removeMessages(1);
            final PlaybackStateCompat playbackState = mediaSessionImpl.getPlaybackState();
            long actions;
            if (playbackState == null) {
                actions = 0L;
            }
            else {
                actions = playbackState.getActions();
            }
            final boolean b2 = playbackState != null && playbackState.getState() == 3;
            final boolean b3 = (0x204L & actions) != 0x0L;
            if ((actions & 0x202L) != 0x0L) {
                b = true;
            }
            if (b2 && b) {
                this.onPause();
            }
            else if (!b2 && b3) {
                this.onPlay();
            }
        }
        
        public void onAddQueueItem(final MediaDescriptionCompat mediaDescriptionCompat) {
        }
        
        public void onAddQueueItem(final MediaDescriptionCompat mediaDescriptionCompat, final int n) {
        }
        
        public void onCommand(final String s, final Bundle bundle, final ResultReceiver resultReceiver) {
        }
        
        public void onCustomAction(final String s, final Bundle bundle) {
        }
        
        public void onFastForward() {
        }
        
        public boolean onMediaButtonEvent(final Intent intent) {
            if (Build$VERSION.SDK_INT >= 27) {
                return false;
            }
            Object mLock = this.mLock;
            synchronized (mLock) {
                final MediaSessionImpl mediaSessionImpl = this.mSessionImpl.get();
                final CallbackHandler mCallbackHandler = this.mCallbackHandler;
                monitorexit(mLock);
                if (mediaSessionImpl != null) {
                    if (mCallbackHandler != null) {
                        mLock = intent.getParcelableExtra("android.intent.extra.KEY_EVENT");
                        if (mLock != null) {
                            if (((KeyEvent)mLock).getAction() == 0) {
                                final MediaSessionManager.RemoteUserInfo currentControllerInfo = mediaSessionImpl.getCurrentControllerInfo();
                                final int keyCode = ((KeyEvent)mLock).getKeyCode();
                                if (keyCode != 79 && keyCode != 85) {
                                    this.handleMediaPlayPauseIfPendingOnHandler(mediaSessionImpl, mCallbackHandler);
                                    return false;
                                }
                                if (((KeyEvent)mLock).getRepeatCount() == 0) {
                                    if (this.mMediaPlayPausePendingOnHandler) {
                                        mCallbackHandler.removeMessages(1);
                                        this.mMediaPlayPausePendingOnHandler = false;
                                        final PlaybackStateCompat playbackState = mediaSessionImpl.getPlaybackState();
                                        long actions;
                                        if (playbackState == null) {
                                            actions = 0L;
                                        }
                                        else {
                                            actions = playbackState.getActions();
                                        }
                                        if ((actions & 0x20L) != 0x0L) {
                                            this.onSkipToNext();
                                        }
                                    }
                                    else {
                                        this.mMediaPlayPausePendingOnHandler = true;
                                        mCallbackHandler.sendMessageDelayed(mCallbackHandler.obtainMessage(1, (Object)currentControllerInfo), (long)ViewConfiguration.getDoubleTapTimeout());
                                    }
                                }
                                else {
                                    this.handleMediaPlayPauseIfPendingOnHandler(mediaSessionImpl, mCallbackHandler);
                                }
                                return true;
                            }
                        }
                    }
                }
                return false;
            }
        }
        
        public void onPause() {
        }
        
        public void onPlay() {
        }
        
        public void onPlayFromMediaId(final String s, final Bundle bundle) {
        }
        
        public void onPlayFromSearch(final String s, final Bundle bundle) {
        }
        
        public void onPlayFromUri(final Uri uri, final Bundle bundle) {
        }
        
        public void onPrepare() {
        }
        
        public void onPrepareFromMediaId(final String s, final Bundle bundle) {
        }
        
        public void onPrepareFromSearch(final String s, final Bundle bundle) {
        }
        
        public void onPrepareFromUri(final Uri uri, final Bundle bundle) {
        }
        
        public void onRemoveQueueItem(final MediaDescriptionCompat mediaDescriptionCompat) {
        }
        
        @Deprecated
        public void onRemoveQueueItemAt(final int n) {
        }
        
        public void onRewind() {
        }
        
        public void onSeekTo(final long n) {
        }
        
        public void onSetCaptioningEnabled(final boolean b) {
        }
        
        public void onSetPlaybackSpeed(final float n) {
        }
        
        public void onSetRating(final RatingCompat ratingCompat) {
        }
        
        public void onSetRating(final RatingCompat ratingCompat, final Bundle bundle) {
        }
        
        public void onSetRepeatMode(final int n) {
        }
        
        public void onSetShuffleMode(final int n) {
        }
        
        public void onSkipToNext() {
        }
        
        public void onSkipToPrevious() {
        }
        
        public void onSkipToQueueItem(final long n) {
        }
        
        public void onStop() {
        }
        
        void setSessionImpl(final MediaSessionImpl referent, final Handler handler) {
            synchronized (this.mLock) {
                this.mSessionImpl = new WeakReference<MediaSessionImpl>(referent);
                final CallbackHandler mCallbackHandler = this.mCallbackHandler;
                final CallbackHandler callbackHandler = null;
                if (mCallbackHandler != null) {
                    mCallbackHandler.removeCallbacksAndMessages((Object)null);
                }
                CallbackHandler mCallbackHandler2 = callbackHandler;
                if (referent != null) {
                    if (handler == null) {
                        mCallbackHandler2 = callbackHandler;
                    }
                    else {
                        mCallbackHandler2 = new CallbackHandler(handler.getLooper());
                    }
                }
                this.mCallbackHandler = mCallbackHandler2;
            }
        }
        
        private class CallbackHandler extends Handler
        {
            private static final int MSG_MEDIA_PLAY_PAUSE_KEY_DOUBLE_TAP_TIMEOUT = 1;
            final Callback this$0;
            
            CallbackHandler(final Callback this$0, final Looper looper) {
                this.this$0 = this$0;
                super(looper);
            }
            
            public void handleMessage(final Message message) {
                if (message.what == 1) {
                    synchronized (this.this$0.mLock) {
                        final MediaSessionImpl mediaSessionImpl = this.this$0.mSessionImpl.get();
                        final CallbackHandler mCallbackHandler = this.this$0.mCallbackHandler;
                        monitorexit(this.this$0.mLock);
                        if (mediaSessionImpl == null || this.this$0 != mediaSessionImpl.getCallback() || mCallbackHandler == null) {
                            return;
                        }
                        mediaSessionImpl.setCurrentControllerInfo((MediaSessionManager.RemoteUserInfo)message.obj);
                        this.this$0.handleMediaPlayPauseIfPendingOnHandler(mediaSessionImpl, mCallbackHandler);
                        mediaSessionImpl.setCurrentControllerInfo(null);
                    }
                }
            }
        }
        
        private class MediaSessionCallbackApi21 extends MediaSession$Callback
        {
            final Callback this$0;
            
            MediaSessionCallbackApi21(final Callback this$0) {
                this.this$0 = this$0;
            }
            
            private void clearCurrentControllerInfo(final MediaSessionImpl mediaSessionImpl) {
                mediaSessionImpl.setCurrentControllerInfo(null);
            }
            
            private MediaSessionImplApi21 getSessionImplIfCallbackIsSet() {
                synchronized (this.this$0.mLock) {
                    MediaSessionImpl mediaSessionImpl = this.this$0.mSessionImpl.get();
                    monitorexit(this.this$0.mLock);
                    if (mediaSessionImpl == null || this.this$0 != ((MediaSessionImplApi21)mediaSessionImpl).getCallback()) {
                        mediaSessionImpl = null;
                    }
                    return (MediaSessionImplApi21)mediaSessionImpl;
                }
            }
            
            private void setCurrentControllerInfo(final MediaSessionImpl mediaSessionImpl) {
                if (Build$VERSION.SDK_INT >= 28) {
                    return;
                }
                String callingPackage;
                if (TextUtils.isEmpty((CharSequence)(callingPackage = mediaSessionImpl.getCallingPackage()))) {
                    callingPackage = "android.media.session.MediaController";
                }
                mediaSessionImpl.setCurrentControllerInfo(new MediaSessionManager.RemoteUserInfo(callingPackage, -1, -1));
            }
            
            public void onCommand(final String s, final Bundle bundle, final ResultReceiver resultReceiver) {
                final MediaSessionImplApi21 sessionImplIfCallbackIsSet = this.getSessionImplIfCallbackIsSet();
                if (sessionImplIfCallbackIsSet == null) {
                    return;
                }
                MediaSessionCompat.ensureClassLoader(bundle);
                this.setCurrentControllerInfo(sessionImplIfCallbackIsSet);
                try {
                    final boolean equals = s.equals("android.support.v4.media.session.command.GET_EXTRA_BINDER");
                    final QueueItem queueItem = null;
                    final IBinder binder = null;
                    if (equals) {
                        final Bundle bundle2 = new Bundle();
                        final Token sessionToken = sessionImplIfCallbackIsSet.getSessionToken();
                        final IMediaSession extraBinder = sessionToken.getExtraBinder();
                        IBinder binder2;
                        if (extraBinder == null) {
                            binder2 = binder;
                        }
                        else {
                            binder2 = extraBinder.asBinder();
                        }
                        BundleCompat.putBinder(bundle2, "android.support.v4.media.session.EXTRA_BINDER", binder2);
                        ParcelUtils.putVersionedParcelable(bundle2, "android.support.v4.media.session.SESSION_TOKEN2", sessionToken.getSession2Token());
                        resultReceiver.send(0, bundle2);
                    }
                    else if (s.equals("android.support.v4.media.session.command.ADD_QUEUE_ITEM")) {
                        this.this$0.onAddQueueItem((MediaDescriptionCompat)bundle.getParcelable("android.support.v4.media.session.command.ARGUMENT_MEDIA_DESCRIPTION"));
                    }
                    else if (s.equals("android.support.v4.media.session.command.ADD_QUEUE_ITEM_AT")) {
                        this.this$0.onAddQueueItem((MediaDescriptionCompat)bundle.getParcelable("android.support.v4.media.session.command.ARGUMENT_MEDIA_DESCRIPTION"), bundle.getInt("android.support.v4.media.session.command.ARGUMENT_INDEX"));
                    }
                    else if (s.equals("android.support.v4.media.session.command.REMOVE_QUEUE_ITEM")) {
                        this.this$0.onRemoveQueueItem((MediaDescriptionCompat)bundle.getParcelable("android.support.v4.media.session.command.ARGUMENT_MEDIA_DESCRIPTION"));
                    }
                    else if (s.equals("android.support.v4.media.session.command.REMOVE_QUEUE_ITEM_AT")) {
                        if (sessionImplIfCallbackIsSet.mQueue != null) {
                            final int int1 = bundle.getInt("android.support.v4.media.session.command.ARGUMENT_INDEX", -1);
                            Object o = queueItem;
                            if (int1 >= 0) {
                                o = queueItem;
                                if (int1 < sessionImplIfCallbackIsSet.mQueue.size()) {
                                    o = sessionImplIfCallbackIsSet.mQueue.get(int1);
                                }
                            }
                            if (o != null) {
                                this.this$0.onRemoveQueueItem(((QueueItem)o).getDescription());
                            }
                        }
                    }
                    else {
                        this.this$0.onCommand(s, bundle, resultReceiver);
                    }
                }
                catch (final BadParcelableException ex) {
                    Log.e("MediaSessionCompat", "Could not unparcel the extra data.");
                }
                this.clearCurrentControllerInfo(sessionImplIfCallbackIsSet);
            }
            
            public void onCustomAction(String s, Bundle bundle) {
                final MediaSessionImplApi21 sessionImplIfCallbackIsSet = this.getSessionImplIfCallbackIsSet();
                if (sessionImplIfCallbackIsSet == null) {
                    return;
                }
                MediaSessionCompat.ensureClassLoader(bundle);
                this.setCurrentControllerInfo(sessionImplIfCallbackIsSet);
                try {
                    if (s.equals("android.support.v4.media.session.action.PLAY_FROM_URI")) {
                        final Uri uri = (Uri)bundle.getParcelable("android.support.v4.media.session.action.ARGUMENT_URI");
                        bundle = bundle.getBundle("android.support.v4.media.session.action.ARGUMENT_EXTRAS");
                        MediaSessionCompat.ensureClassLoader(bundle);
                        this.this$0.onPlayFromUri(uri, bundle);
                    }
                    else if (s.equals("android.support.v4.media.session.action.PREPARE")) {
                        this.this$0.onPrepare();
                    }
                    else if (s.equals("android.support.v4.media.session.action.PREPARE_FROM_MEDIA_ID")) {
                        s = bundle.getString("android.support.v4.media.session.action.ARGUMENT_MEDIA_ID");
                        bundle = bundle.getBundle("android.support.v4.media.session.action.ARGUMENT_EXTRAS");
                        MediaSessionCompat.ensureClassLoader(bundle);
                        this.this$0.onPrepareFromMediaId(s, bundle);
                    }
                    else if (s.equals("android.support.v4.media.session.action.PREPARE_FROM_SEARCH")) {
                        s = bundle.getString("android.support.v4.media.session.action.ARGUMENT_QUERY");
                        bundle = bundle.getBundle("android.support.v4.media.session.action.ARGUMENT_EXTRAS");
                        MediaSessionCompat.ensureClassLoader(bundle);
                        this.this$0.onPrepareFromSearch(s, bundle);
                    }
                    else if (s.equals("android.support.v4.media.session.action.PREPARE_FROM_URI")) {
                        final Uri uri2 = (Uri)bundle.getParcelable("android.support.v4.media.session.action.ARGUMENT_URI");
                        bundle = bundle.getBundle("android.support.v4.media.session.action.ARGUMENT_EXTRAS");
                        MediaSessionCompat.ensureClassLoader(bundle);
                        this.this$0.onPrepareFromUri(uri2, bundle);
                    }
                    else if (s.equals("android.support.v4.media.session.action.SET_CAPTIONING_ENABLED")) {
                        this.this$0.onSetCaptioningEnabled(bundle.getBoolean("android.support.v4.media.session.action.ARGUMENT_CAPTIONING_ENABLED"));
                    }
                    else if (s.equals("android.support.v4.media.session.action.SET_REPEAT_MODE")) {
                        this.this$0.onSetRepeatMode(bundle.getInt("android.support.v4.media.session.action.ARGUMENT_REPEAT_MODE"));
                    }
                    else if (s.equals("android.support.v4.media.session.action.SET_SHUFFLE_MODE")) {
                        this.this$0.onSetShuffleMode(bundle.getInt("android.support.v4.media.session.action.ARGUMENT_SHUFFLE_MODE"));
                    }
                    else if (s.equals("android.support.v4.media.session.action.SET_RATING")) {
                        final RatingCompat ratingCompat = (RatingCompat)bundle.getParcelable("android.support.v4.media.session.action.ARGUMENT_RATING");
                        bundle = bundle.getBundle("android.support.v4.media.session.action.ARGUMENT_EXTRAS");
                        MediaSessionCompat.ensureClassLoader(bundle);
                        this.this$0.onSetRating(ratingCompat, bundle);
                    }
                    else if (s.equals("android.support.v4.media.session.action.SET_PLAYBACK_SPEED")) {
                        this.this$0.onSetPlaybackSpeed(bundle.getFloat("android.support.v4.media.session.action.ARGUMENT_PLAYBACK_SPEED", 1.0f));
                    }
                    else {
                        this.this$0.onCustomAction(s, bundle);
                    }
                }
                catch (final BadParcelableException ex) {
                    Log.e("MediaSessionCompat", "Could not unparcel the data.");
                }
                this.clearCurrentControllerInfo(sessionImplIfCallbackIsSet);
            }
            
            public void onFastForward() {
                final MediaSessionImplApi21 sessionImplIfCallbackIsSet = this.getSessionImplIfCallbackIsSet();
                if (sessionImplIfCallbackIsSet == null) {
                    return;
                }
                this.setCurrentControllerInfo(sessionImplIfCallbackIsSet);
                this.this$0.onFastForward();
                this.clearCurrentControllerInfo(sessionImplIfCallbackIsSet);
            }
            
            public boolean onMediaButtonEvent(final Intent intent) {
                final MediaSessionImplApi21 sessionImplIfCallbackIsSet = this.getSessionImplIfCallbackIsSet();
                boolean b = false;
                if (sessionImplIfCallbackIsSet == null) {
                    return false;
                }
                this.setCurrentControllerInfo(sessionImplIfCallbackIsSet);
                final boolean onMediaButtonEvent = this.this$0.onMediaButtonEvent(intent);
                this.clearCurrentControllerInfo(sessionImplIfCallbackIsSet);
                if (onMediaButtonEvent || super.onMediaButtonEvent(intent)) {
                    b = true;
                }
                return b;
            }
            
            public void onPause() {
                final MediaSessionImplApi21 sessionImplIfCallbackIsSet = this.getSessionImplIfCallbackIsSet();
                if (sessionImplIfCallbackIsSet == null) {
                    return;
                }
                this.setCurrentControllerInfo(sessionImplIfCallbackIsSet);
                this.this$0.onPause();
                this.clearCurrentControllerInfo(sessionImplIfCallbackIsSet);
            }
            
            public void onPlay() {
                final MediaSessionImplApi21 sessionImplIfCallbackIsSet = this.getSessionImplIfCallbackIsSet();
                if (sessionImplIfCallbackIsSet == null) {
                    return;
                }
                this.setCurrentControllerInfo(sessionImplIfCallbackIsSet);
                this.this$0.onPlay();
                this.clearCurrentControllerInfo(sessionImplIfCallbackIsSet);
            }
            
            public void onPlayFromMediaId(final String s, final Bundle bundle) {
                final MediaSessionImplApi21 sessionImplIfCallbackIsSet = this.getSessionImplIfCallbackIsSet();
                if (sessionImplIfCallbackIsSet == null) {
                    return;
                }
                MediaSessionCompat.ensureClassLoader(bundle);
                this.setCurrentControllerInfo(sessionImplIfCallbackIsSet);
                this.this$0.onPlayFromMediaId(s, bundle);
                this.clearCurrentControllerInfo(sessionImplIfCallbackIsSet);
            }
            
            public void onPlayFromSearch(final String s, final Bundle bundle) {
                final MediaSessionImplApi21 sessionImplIfCallbackIsSet = this.getSessionImplIfCallbackIsSet();
                if (sessionImplIfCallbackIsSet == null) {
                    return;
                }
                MediaSessionCompat.ensureClassLoader(bundle);
                this.setCurrentControllerInfo(sessionImplIfCallbackIsSet);
                this.this$0.onPlayFromSearch(s, bundle);
                this.clearCurrentControllerInfo(sessionImplIfCallbackIsSet);
            }
            
            public void onPlayFromUri(final Uri uri, final Bundle bundle) {
                final MediaSessionImplApi21 sessionImplIfCallbackIsSet = this.getSessionImplIfCallbackIsSet();
                if (sessionImplIfCallbackIsSet == null) {
                    return;
                }
                MediaSessionCompat.ensureClassLoader(bundle);
                this.setCurrentControllerInfo(sessionImplIfCallbackIsSet);
                this.this$0.onPlayFromUri(uri, bundle);
                this.clearCurrentControllerInfo(sessionImplIfCallbackIsSet);
            }
            
            public void onPrepare() {
                final MediaSessionImplApi21 sessionImplIfCallbackIsSet = this.getSessionImplIfCallbackIsSet();
                if (sessionImplIfCallbackIsSet == null) {
                    return;
                }
                this.setCurrentControllerInfo(sessionImplIfCallbackIsSet);
                this.this$0.onPrepare();
                this.clearCurrentControllerInfo(sessionImplIfCallbackIsSet);
            }
            
            public void onPrepareFromMediaId(final String s, final Bundle bundle) {
                final MediaSessionImplApi21 sessionImplIfCallbackIsSet = this.getSessionImplIfCallbackIsSet();
                if (sessionImplIfCallbackIsSet == null) {
                    return;
                }
                MediaSessionCompat.ensureClassLoader(bundle);
                this.setCurrentControllerInfo(sessionImplIfCallbackIsSet);
                this.this$0.onPrepareFromMediaId(s, bundle);
                this.clearCurrentControllerInfo(sessionImplIfCallbackIsSet);
            }
            
            public void onPrepareFromSearch(final String s, final Bundle bundle) {
                final MediaSessionImplApi21 sessionImplIfCallbackIsSet = this.getSessionImplIfCallbackIsSet();
                if (sessionImplIfCallbackIsSet == null) {
                    return;
                }
                MediaSessionCompat.ensureClassLoader(bundle);
                this.setCurrentControllerInfo(sessionImplIfCallbackIsSet);
                this.this$0.onPrepareFromSearch(s, bundle);
                this.clearCurrentControllerInfo(sessionImplIfCallbackIsSet);
            }
            
            public void onPrepareFromUri(final Uri uri, final Bundle bundle) {
                final MediaSessionImplApi21 sessionImplIfCallbackIsSet = this.getSessionImplIfCallbackIsSet();
                if (sessionImplIfCallbackIsSet == null) {
                    return;
                }
                MediaSessionCompat.ensureClassLoader(bundle);
                this.setCurrentControllerInfo(sessionImplIfCallbackIsSet);
                this.this$0.onPrepareFromUri(uri, bundle);
                this.clearCurrentControllerInfo(sessionImplIfCallbackIsSet);
            }
            
            public void onRewind() {
                final MediaSessionImplApi21 sessionImplIfCallbackIsSet = this.getSessionImplIfCallbackIsSet();
                if (sessionImplIfCallbackIsSet == null) {
                    return;
                }
                this.setCurrentControllerInfo(sessionImplIfCallbackIsSet);
                this.this$0.onRewind();
                this.clearCurrentControllerInfo(sessionImplIfCallbackIsSet);
            }
            
            public void onSeekTo(final long n) {
                final MediaSessionImplApi21 sessionImplIfCallbackIsSet = this.getSessionImplIfCallbackIsSet();
                if (sessionImplIfCallbackIsSet == null) {
                    return;
                }
                this.setCurrentControllerInfo(sessionImplIfCallbackIsSet);
                this.this$0.onSeekTo(n);
                this.clearCurrentControllerInfo(sessionImplIfCallbackIsSet);
            }
            
            public void onSetPlaybackSpeed(final float n) {
                final MediaSessionImplApi21 sessionImplIfCallbackIsSet = this.getSessionImplIfCallbackIsSet();
                if (sessionImplIfCallbackIsSet == null) {
                    return;
                }
                this.setCurrentControllerInfo(sessionImplIfCallbackIsSet);
                this.this$0.onSetPlaybackSpeed(n);
                this.clearCurrentControllerInfo(sessionImplIfCallbackIsSet);
            }
            
            public void onSetRating(final Rating rating) {
                final MediaSessionImplApi21 sessionImplIfCallbackIsSet = this.getSessionImplIfCallbackIsSet();
                if (sessionImplIfCallbackIsSet == null) {
                    return;
                }
                this.setCurrentControllerInfo(sessionImplIfCallbackIsSet);
                this.this$0.onSetRating(RatingCompat.fromRating(rating));
                this.clearCurrentControllerInfo(sessionImplIfCallbackIsSet);
            }
            
            public void onSetRating(final Rating rating, final Bundle bundle) {
            }
            
            public void onSkipToNext() {
                final MediaSessionImplApi21 sessionImplIfCallbackIsSet = this.getSessionImplIfCallbackIsSet();
                if (sessionImplIfCallbackIsSet == null) {
                    return;
                }
                this.setCurrentControllerInfo(sessionImplIfCallbackIsSet);
                this.this$0.onSkipToNext();
                this.clearCurrentControllerInfo(sessionImplIfCallbackIsSet);
            }
            
            public void onSkipToPrevious() {
                final MediaSessionImplApi21 sessionImplIfCallbackIsSet = this.getSessionImplIfCallbackIsSet();
                if (sessionImplIfCallbackIsSet == null) {
                    return;
                }
                this.setCurrentControllerInfo(sessionImplIfCallbackIsSet);
                this.this$0.onSkipToPrevious();
                this.clearCurrentControllerInfo(sessionImplIfCallbackIsSet);
            }
            
            public void onSkipToQueueItem(final long n) {
                final MediaSessionImplApi21 sessionImplIfCallbackIsSet = this.getSessionImplIfCallbackIsSet();
                if (sessionImplIfCallbackIsSet == null) {
                    return;
                }
                this.setCurrentControllerInfo(sessionImplIfCallbackIsSet);
                this.this$0.onSkipToQueueItem(n);
                this.clearCurrentControllerInfo(sessionImplIfCallbackIsSet);
            }
            
            public void onStop() {
                final MediaSessionImplApi21 sessionImplIfCallbackIsSet = this.getSessionImplIfCallbackIsSet();
                if (sessionImplIfCallbackIsSet == null) {
                    return;
                }
                this.setCurrentControllerInfo(sessionImplIfCallbackIsSet);
                this.this$0.onStop();
                this.clearCurrentControllerInfo(sessionImplIfCallbackIsSet);
            }
        }
    }
    
    interface MediaSessionImpl
    {
        Callback getCallback();
        
        String getCallingPackage();
        
        MediaSessionManager.RemoteUserInfo getCurrentControllerInfo();
        
        Object getMediaSession();
        
        PlaybackStateCompat getPlaybackState();
        
        Object getRemoteControlClient();
        
        Token getSessionToken();
        
        boolean isActive();
        
        void release();
        
        void sendSessionEvent(final String p0, final Bundle p1);
        
        void setActive(final boolean p0);
        
        void setCallback(final Callback p0, final Handler p1);
        
        void setCaptioningEnabled(final boolean p0);
        
        void setCurrentControllerInfo(final MediaSessionManager.RemoteUserInfo p0);
        
        void setExtras(final Bundle p0);
        
        void setFlags(final int p0);
        
        void setMediaButtonReceiver(final PendingIntent p0);
        
        void setMetadata(final MediaMetadataCompat p0);
        
        void setPlaybackState(final PlaybackStateCompat p0);
        
        void setPlaybackToLocal(final int p0);
        
        void setPlaybackToRemote(final VolumeProviderCompat p0);
        
        void setQueue(final List<QueueItem> p0);
        
        void setQueueTitle(final CharSequence p0);
        
        void setRatingType(final int p0);
        
        void setRegistrationCallback(final RegistrationCallback p0, final Handler p1);
        
        void setRepeatMode(final int p0);
        
        void setSessionActivity(final PendingIntent p0);
        
        void setShuffleMode(final int p0);
    }
    
    static class MediaSessionImplApi18 extends MediaSessionImplBase
    {
        private static boolean sIsMbrPendingIntentSupported = true;
        
        MediaSessionImplApi18(final Context context, final String s, final ComponentName componentName, final PendingIntent pendingIntent, final VersionedParcelable versionedParcelable, final Bundle bundle) {
            super(context, s, componentName, pendingIntent, versionedParcelable, bundle);
        }
        
        @Override
        int getRccTransportControlFlagsFromActions(final long n) {
            int rccTransportControlFlagsFromActions = super.getRccTransportControlFlagsFromActions(n);
            if ((n & 0x100L) != 0x0L) {
                rccTransportControlFlagsFromActions |= 0x100;
            }
            return rccTransportControlFlagsFromActions;
        }
        
        @Override
        void registerMediaButtonEventReceiver(final PendingIntent pendingIntent, final ComponentName componentName) {
            if (MediaSessionImplApi18.sIsMbrPendingIntentSupported) {
                try {
                    this.mAudioManager.registerMediaButtonEventReceiver(pendingIntent);
                }
                catch (final NullPointerException ex) {
                    Log.w("MediaSessionCompat", "Unable to register media button event receiver with PendingIntent, falling back to ComponentName.");
                    MediaSessionImplApi18.sIsMbrPendingIntentSupported = false;
                }
            }
            if (!MediaSessionImplApi18.sIsMbrPendingIntentSupported) {
                super.registerMediaButtonEventReceiver(pendingIntent, componentName);
            }
        }
        
        @Override
        public void setCallback(final Callback callback, final Handler handler) {
            super.setCallback(callback, handler);
            if (callback == null) {
                this.mRcc.setPlaybackPositionUpdateListener((RemoteControlClient$OnPlaybackPositionUpdateListener)null);
            }
            else {
                this.mRcc.setPlaybackPositionUpdateListener((RemoteControlClient$OnPlaybackPositionUpdateListener)new RemoteControlClient$OnPlaybackPositionUpdateListener(this) {
                    final MediaSessionImplApi18 this$0;
                    
                    public void onPlaybackPositionUpdate(final long l) {
                        ((MediaSessionImplBase)this.this$0).postToHandler(18, -1, -1, l, null);
                    }
                });
            }
        }
        
        @Override
        void setRccState(final PlaybackStateCompat playbackStateCompat) {
            final long position = playbackStateCompat.getPosition();
            final float playbackSpeed = playbackStateCompat.getPlaybackSpeed();
            final long lastPositionUpdateTime = playbackStateCompat.getLastPositionUpdateTime();
            final long elapsedRealtime = SystemClock.elapsedRealtime();
            long n = position;
            if (playbackStateCompat.getState() == 3) {
                final long n2 = 0L;
                n = position;
                if (position > 0L) {
                    long n3 = n2;
                    if (lastPositionUpdateTime > 0L) {
                        final long n4 = n3 = elapsedRealtime - lastPositionUpdateTime;
                        if (playbackSpeed > 0.0f) {
                            n3 = n4;
                            if (playbackSpeed != 1.0f) {
                                n3 = (long)(n4 * playbackSpeed);
                            }
                        }
                    }
                    n = position + n3;
                }
            }
            this.mRcc.setPlaybackState(((MediaSessionImplBase)this).getRccStateFromState(playbackStateCompat.getState()), n, playbackSpeed);
        }
        
        @Override
        void unregisterMediaButtonEventReceiver(final PendingIntent pendingIntent, final ComponentName componentName) {
            if (MediaSessionImplApi18.sIsMbrPendingIntentSupported) {
                this.mAudioManager.unregisterMediaButtonEventReceiver(pendingIntent);
            }
            else {
                super.unregisterMediaButtonEventReceiver(pendingIntent, componentName);
            }
        }
    }
    
    static class MediaSessionImplBase implements MediaSessionImpl
    {
        static final int RCC_PLAYSTATE_NONE = 0;
        final AudioManager mAudioManager;
        volatile Callback mCallback;
        boolean mCaptioningEnabled;
        private final Context mContext;
        final RemoteCallbackList<IMediaControllerCallback> mControllerCallbacks;
        boolean mDestroyed;
        Bundle mExtras;
        int mFlags;
        private MessageHandler mHandler;
        boolean mIsActive;
        int mLocalStream;
        final Object mLock;
        private final ComponentName mMediaButtonReceiverComponentName;
        private final PendingIntent mMediaButtonReceiverIntent;
        MediaMetadataCompat mMetadata;
        final String mPackageName;
        List<QueueItem> mQueue;
        CharSequence mQueueTitle;
        int mRatingType;
        final RemoteControlClient mRcc;
        RegistrationCallbackHandler mRegistrationCallbackHandler;
        private MediaSessionManager.RemoteUserInfo mRemoteUserInfo;
        int mRepeatMode;
        PendingIntent mSessionActivity;
        final Bundle mSessionInfo;
        int mShuffleMode;
        PlaybackStateCompat mState;
        private final MediaSessionStub mStub;
        final String mTag;
        private final Token mToken;
        private VolumeProviderCompat.Callback mVolumeCallback;
        VolumeProviderCompat mVolumeProvider;
        int mVolumeType;
        
        public MediaSessionImplBase(final Context mContext, final String mTag, final ComponentName mMediaButtonReceiverComponentName, final PendingIntent mMediaButtonReceiverIntent, final VersionedParcelable versionedParcelable, final Bundle mSessionInfo) {
            this.mLock = new Object();
            this.mControllerCallbacks = (RemoteCallbackList<IMediaControllerCallback>)new RemoteCallbackList();
            this.mDestroyed = false;
            this.mIsActive = false;
            this.mFlags = 3;
            this.mVolumeCallback = new VolumeProviderCompat.Callback() {
                final MediaSessionImplBase this$0;
                
                @Override
                public void onVolumeChanged(final VolumeProviderCompat volumeProviderCompat) {
                    if (this.this$0.mVolumeProvider != volumeProviderCompat) {
                        return;
                    }
                    this.this$0.sendVolumeInfoChanged(new ParcelableVolumeInfo(this.this$0.mVolumeType, this.this$0.mLocalStream, volumeProviderCompat.getVolumeControl(), volumeProviderCompat.getMaxVolume(), volumeProviderCompat.getCurrentVolume()));
                }
            };
            if (mMediaButtonReceiverComponentName != null) {
                this.mContext = mContext;
                this.mPackageName = mContext.getPackageName();
                this.mSessionInfo = mSessionInfo;
                this.mAudioManager = (AudioManager)mContext.getSystemService("audio");
                this.mTag = mTag;
                this.mMediaButtonReceiverComponentName = mMediaButtonReceiverComponentName;
                this.mMediaButtonReceiverIntent = mMediaButtonReceiverIntent;
                final MediaSessionStub mStub = new MediaSessionStub();
                this.mStub = mStub;
                this.mToken = new Token(mStub, null, versionedParcelable);
                this.mRatingType = 0;
                this.mVolumeType = 1;
                this.mLocalStream = 3;
                this.mRcc = new RemoteControlClient(mMediaButtonReceiverIntent);
                return;
            }
            throw new IllegalArgumentException("MediaButtonReceiver component may not be null");
        }
        
        private void sendCaptioningEnabled(final boolean p0) {
            // 
            // This method could not be decompiled.
            // 
            // Original Bytecode:
            // 
            //     1: getfield        android/support/v4/media/session/MediaSessionCompat$MediaSessionImplBase.mLock:Ljava/lang/Object;
            //     4: astore_3       
            //     5: aload_3        
            //     6: monitorenter   
            //     7: aload_0        
            //     8: getfield        android/support/v4/media/session/MediaSessionCompat$MediaSessionImplBase.mControllerCallbacks:Landroid/os/RemoteCallbackList;
            //    11: invokevirtual   android/os/RemoteCallbackList.beginBroadcast:()I
            //    14: iconst_1       
            //    15: isub           
            //    16: istore_2       
            //    17: iload_2        
            //    18: iflt            48
            //    21: aload_0        
            //    22: getfield        android/support/v4/media/session/MediaSessionCompat$MediaSessionImplBase.mControllerCallbacks:Landroid/os/RemoteCallbackList;
            //    25: iload_2        
            //    26: invokevirtual   android/os/RemoteCallbackList.getBroadcastItem:(I)Landroid/os/IInterface;
            //    29: checkcast       Landroid/support/v4/media/session/IMediaControllerCallback;
            //    32: astore          4
            //    34: aload           4
            //    36: iload_1        
            //    37: invokeinterface android/support/v4/media/session/IMediaControllerCallback.onCaptioningEnabledChanged:(Z)V
            //    42: iinc            2, -1
            //    45: goto            17
            //    48: aload_0        
            //    49: getfield        android/support/v4/media/session/MediaSessionCompat$MediaSessionImplBase.mControllerCallbacks:Landroid/os/RemoteCallbackList;
            //    52: invokevirtual   android/os/RemoteCallbackList.finishBroadcast:()V
            //    55: aload_3        
            //    56: monitorexit    
            //    57: return         
            //    58: astore          4
            //    60: aload_3        
            //    61: monitorexit    
            //    62: aload           4
            //    64: athrow         
            //    65: astore          4
            //    67: goto            42
            //    Exceptions:
            //  Try           Handler
            //  Start  End    Start  End    Type                        
            //  -----  -----  -----  -----  ----------------------------
            //  7      17     58     65     Any
            //  21     34     58     65     Any
            //  34     42     65     70     Landroid/os/RemoteException;
            //  34     42     58     65     Any
            //  48     57     58     65     Any
            //  60     62     58     65     Any
            // 
            // The error that occurred was:
            // 
            // java.lang.IllegalStateException: Expression is linked from several locations: Label_0042:
            //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
            //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2604)
            //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
            //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
            //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:206)
            //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:662)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
            //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
            //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
            //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
            //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
            //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
            // 
            throw new IllegalStateException("An error occurred while decompiling this method.");
        }
        
        private void sendEvent(final String p0, final Bundle p1) {
            // 
            // This method could not be decompiled.
            // 
            // Original Bytecode:
            // 
            //     1: getfield        android/support/v4/media/session/MediaSessionCompat$MediaSessionImplBase.mLock:Ljava/lang/Object;
            //     4: astore          4
            //     6: aload           4
            //     8: monitorenter   
            //     9: aload_0        
            //    10: getfield        android/support/v4/media/session/MediaSessionCompat$MediaSessionImplBase.mControllerCallbacks:Landroid/os/RemoteCallbackList;
            //    13: invokevirtual   android/os/RemoteCallbackList.beginBroadcast:()I
            //    16: iconst_1       
            //    17: isub           
            //    18: istore_3       
            //    19: iload_3        
            //    20: iflt            51
            //    23: aload_0        
            //    24: getfield        android/support/v4/media/session/MediaSessionCompat$MediaSessionImplBase.mControllerCallbacks:Landroid/os/RemoteCallbackList;
            //    27: iload_3        
            //    28: invokevirtual   android/os/RemoteCallbackList.getBroadcastItem:(I)Landroid/os/IInterface;
            //    31: checkcast       Landroid/support/v4/media/session/IMediaControllerCallback;
            //    34: astore          5
            //    36: aload           5
            //    38: aload_1        
            //    39: aload_2        
            //    40: invokeinterface android/support/v4/media/session/IMediaControllerCallback.onEvent:(Ljava/lang/String;Landroid/os/Bundle;)V
            //    45: iinc            3, -1
            //    48: goto            19
            //    51: aload_0        
            //    52: getfield        android/support/v4/media/session/MediaSessionCompat$MediaSessionImplBase.mControllerCallbacks:Landroid/os/RemoteCallbackList;
            //    55: invokevirtual   android/os/RemoteCallbackList.finishBroadcast:()V
            //    58: aload           4
            //    60: monitorexit    
            //    61: return         
            //    62: astore_1       
            //    63: aload           4
            //    65: monitorexit    
            //    66: aload_1        
            //    67: athrow         
            //    68: astore          5
            //    70: goto            45
            //    Exceptions:
            //  Try           Handler
            //  Start  End    Start  End    Type                        
            //  -----  -----  -----  -----  ----------------------------
            //  9      19     62     68     Any
            //  23     36     62     68     Any
            //  36     45     68     73     Landroid/os/RemoteException;
            //  36     45     62     68     Any
            //  51     61     62     68     Any
            //  63     66     62     68     Any
            // 
            // The error that occurred was:
            // 
            // java.lang.IllegalStateException: Expression is linked from several locations: Label_0045:
            //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
            //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2604)
            //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
            //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
            //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:206)
            //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:662)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
            //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
            //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
            //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
            //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
            //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
            // 
            throw new IllegalStateException("An error occurred while decompiling this method.");
        }
        
        private void sendExtras(final Bundle p0) {
            // 
            // This method could not be decompiled.
            // 
            // Original Bytecode:
            // 
            //     1: getfield        android/support/v4/media/session/MediaSessionCompat$MediaSessionImplBase.mLock:Ljava/lang/Object;
            //     4: astore_3       
            //     5: aload_3        
            //     6: monitorenter   
            //     7: aload_0        
            //     8: getfield        android/support/v4/media/session/MediaSessionCompat$MediaSessionImplBase.mControllerCallbacks:Landroid/os/RemoteCallbackList;
            //    11: invokevirtual   android/os/RemoteCallbackList.beginBroadcast:()I
            //    14: iconst_1       
            //    15: isub           
            //    16: istore_2       
            //    17: iload_2        
            //    18: iflt            48
            //    21: aload_0        
            //    22: getfield        android/support/v4/media/session/MediaSessionCompat$MediaSessionImplBase.mControllerCallbacks:Landroid/os/RemoteCallbackList;
            //    25: iload_2        
            //    26: invokevirtual   android/os/RemoteCallbackList.getBroadcastItem:(I)Landroid/os/IInterface;
            //    29: checkcast       Landroid/support/v4/media/session/IMediaControllerCallback;
            //    32: astore          4
            //    34: aload           4
            //    36: aload_1        
            //    37: invokeinterface android/support/v4/media/session/IMediaControllerCallback.onExtrasChanged:(Landroid/os/Bundle;)V
            //    42: iinc            2, -1
            //    45: goto            17
            //    48: aload_0        
            //    49: getfield        android/support/v4/media/session/MediaSessionCompat$MediaSessionImplBase.mControllerCallbacks:Landroid/os/RemoteCallbackList;
            //    52: invokevirtual   android/os/RemoteCallbackList.finishBroadcast:()V
            //    55: aload_3        
            //    56: monitorexit    
            //    57: return         
            //    58: astore_1       
            //    59: aload_3        
            //    60: monitorexit    
            //    61: aload_1        
            //    62: athrow         
            //    63: astore          4
            //    65: goto            42
            //    Exceptions:
            //  Try           Handler
            //  Start  End    Start  End    Type                        
            //  -----  -----  -----  -----  ----------------------------
            //  7      17     58     63     Any
            //  21     34     58     63     Any
            //  34     42     63     68     Landroid/os/RemoteException;
            //  34     42     58     63     Any
            //  48     57     58     63     Any
            //  59     61     58     63     Any
            // 
            // The error that occurred was:
            // 
            // java.lang.IllegalStateException: Expression is linked from several locations: Label_0042:
            //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
            //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2604)
            //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
            //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
            //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:206)
            //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:662)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
            //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
            //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
            //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
            //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
            //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
            // 
            throw new IllegalStateException("An error occurred while decompiling this method.");
        }
        
        private void sendMetadata(final MediaMetadataCompat p0) {
            // 
            // This method could not be decompiled.
            // 
            // Original Bytecode:
            // 
            //     1: getfield        android/support/v4/media/session/MediaSessionCompat$MediaSessionImplBase.mLock:Ljava/lang/Object;
            //     4: astore_3       
            //     5: aload_3        
            //     6: monitorenter   
            //     7: aload_0        
            //     8: getfield        android/support/v4/media/session/MediaSessionCompat$MediaSessionImplBase.mControllerCallbacks:Landroid/os/RemoteCallbackList;
            //    11: invokevirtual   android/os/RemoteCallbackList.beginBroadcast:()I
            //    14: iconst_1       
            //    15: isub           
            //    16: istore_2       
            //    17: iload_2        
            //    18: iflt            48
            //    21: aload_0        
            //    22: getfield        android/support/v4/media/session/MediaSessionCompat$MediaSessionImplBase.mControllerCallbacks:Landroid/os/RemoteCallbackList;
            //    25: iload_2        
            //    26: invokevirtual   android/os/RemoteCallbackList.getBroadcastItem:(I)Landroid/os/IInterface;
            //    29: checkcast       Landroid/support/v4/media/session/IMediaControllerCallback;
            //    32: astore          4
            //    34: aload           4
            //    36: aload_1        
            //    37: invokeinterface android/support/v4/media/session/IMediaControllerCallback.onMetadataChanged:(Landroid/support/v4/media/MediaMetadataCompat;)V
            //    42: iinc            2, -1
            //    45: goto            17
            //    48: aload_0        
            //    49: getfield        android/support/v4/media/session/MediaSessionCompat$MediaSessionImplBase.mControllerCallbacks:Landroid/os/RemoteCallbackList;
            //    52: invokevirtual   android/os/RemoteCallbackList.finishBroadcast:()V
            //    55: aload_3        
            //    56: monitorexit    
            //    57: return         
            //    58: astore_1       
            //    59: aload_3        
            //    60: monitorexit    
            //    61: aload_1        
            //    62: athrow         
            //    63: astore          4
            //    65: goto            42
            //    Exceptions:
            //  Try           Handler
            //  Start  End    Start  End    Type                        
            //  -----  -----  -----  -----  ----------------------------
            //  7      17     58     63     Any
            //  21     34     58     63     Any
            //  34     42     63     68     Landroid/os/RemoteException;
            //  34     42     58     63     Any
            //  48     57     58     63     Any
            //  59     61     58     63     Any
            // 
            // The error that occurred was:
            // 
            // java.lang.IllegalStateException: Expression is linked from several locations: Label_0042:
            //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
            //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2604)
            //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
            //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
            //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:206)
            //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:662)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
            //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
            //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
            //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
            //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
            //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
            // 
            throw new IllegalStateException("An error occurred while decompiling this method.");
        }
        
        private void sendQueue(final List<QueueItem> p0) {
            // 
            // This method could not be decompiled.
            // 
            // Original Bytecode:
            // 
            //     1: getfield        android/support/v4/media/session/MediaSessionCompat$MediaSessionImplBase.mLock:Ljava/lang/Object;
            //     4: astore_3       
            //     5: aload_3        
            //     6: monitorenter   
            //     7: aload_0        
            //     8: getfield        android/support/v4/media/session/MediaSessionCompat$MediaSessionImplBase.mControllerCallbacks:Landroid/os/RemoteCallbackList;
            //    11: invokevirtual   android/os/RemoteCallbackList.beginBroadcast:()I
            //    14: iconst_1       
            //    15: isub           
            //    16: istore_2       
            //    17: iload_2        
            //    18: iflt            48
            //    21: aload_0        
            //    22: getfield        android/support/v4/media/session/MediaSessionCompat$MediaSessionImplBase.mControllerCallbacks:Landroid/os/RemoteCallbackList;
            //    25: iload_2        
            //    26: invokevirtual   android/os/RemoteCallbackList.getBroadcastItem:(I)Landroid/os/IInterface;
            //    29: checkcast       Landroid/support/v4/media/session/IMediaControllerCallback;
            //    32: astore          4
            //    34: aload           4
            //    36: aload_1        
            //    37: invokeinterface android/support/v4/media/session/IMediaControllerCallback.onQueueChanged:(Ljava/util/List;)V
            //    42: iinc            2, -1
            //    45: goto            17
            //    48: aload_0        
            //    49: getfield        android/support/v4/media/session/MediaSessionCompat$MediaSessionImplBase.mControllerCallbacks:Landroid/os/RemoteCallbackList;
            //    52: invokevirtual   android/os/RemoteCallbackList.finishBroadcast:()V
            //    55: aload_3        
            //    56: monitorexit    
            //    57: return         
            //    58: astore_1       
            //    59: aload_3        
            //    60: monitorexit    
            //    61: aload_1        
            //    62: athrow         
            //    63: astore          4
            //    65: goto            42
            //    Signature:
            //  (Ljava/util/List<Landroid/support/v4/media/session/MediaSessionCompat$QueueItem;>;)V
            //    Exceptions:
            //  Try           Handler
            //  Start  End    Start  End    Type                        
            //  -----  -----  -----  -----  ----------------------------
            //  7      17     58     63     Any
            //  21     34     58     63     Any
            //  34     42     63     68     Landroid/os/RemoteException;
            //  34     42     58     63     Any
            //  48     57     58     63     Any
            //  59     61     58     63     Any
            // 
            // The error that occurred was:
            // 
            // java.lang.IllegalStateException: Expression is linked from several locations: Label_0042:
            //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
            //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2604)
            //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
            //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
            //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:206)
            //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:662)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
            //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
            //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
            //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
            //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
            //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
            // 
            throw new IllegalStateException("An error occurred while decompiling this method.");
        }
        
        private void sendQueueTitle(final CharSequence p0) {
            // 
            // This method could not be decompiled.
            // 
            // Original Bytecode:
            // 
            //     1: getfield        android/support/v4/media/session/MediaSessionCompat$MediaSessionImplBase.mLock:Ljava/lang/Object;
            //     4: astore_3       
            //     5: aload_3        
            //     6: monitorenter   
            //     7: aload_0        
            //     8: getfield        android/support/v4/media/session/MediaSessionCompat$MediaSessionImplBase.mControllerCallbacks:Landroid/os/RemoteCallbackList;
            //    11: invokevirtual   android/os/RemoteCallbackList.beginBroadcast:()I
            //    14: iconst_1       
            //    15: isub           
            //    16: istore_2       
            //    17: iload_2        
            //    18: iflt            48
            //    21: aload_0        
            //    22: getfield        android/support/v4/media/session/MediaSessionCompat$MediaSessionImplBase.mControllerCallbacks:Landroid/os/RemoteCallbackList;
            //    25: iload_2        
            //    26: invokevirtual   android/os/RemoteCallbackList.getBroadcastItem:(I)Landroid/os/IInterface;
            //    29: checkcast       Landroid/support/v4/media/session/IMediaControllerCallback;
            //    32: astore          4
            //    34: aload           4
            //    36: aload_1        
            //    37: invokeinterface android/support/v4/media/session/IMediaControllerCallback.onQueueTitleChanged:(Ljava/lang/CharSequence;)V
            //    42: iinc            2, -1
            //    45: goto            17
            //    48: aload_0        
            //    49: getfield        android/support/v4/media/session/MediaSessionCompat$MediaSessionImplBase.mControllerCallbacks:Landroid/os/RemoteCallbackList;
            //    52: invokevirtual   android/os/RemoteCallbackList.finishBroadcast:()V
            //    55: aload_3        
            //    56: monitorexit    
            //    57: return         
            //    58: astore_1       
            //    59: aload_3        
            //    60: monitorexit    
            //    61: aload_1        
            //    62: athrow         
            //    63: astore          4
            //    65: goto            42
            //    Exceptions:
            //  Try           Handler
            //  Start  End    Start  End    Type                        
            //  -----  -----  -----  -----  ----------------------------
            //  7      17     58     63     Any
            //  21     34     58     63     Any
            //  34     42     63     68     Landroid/os/RemoteException;
            //  34     42     58     63     Any
            //  48     57     58     63     Any
            //  59     61     58     63     Any
            // 
            // The error that occurred was:
            // 
            // java.lang.IllegalStateException: Expression is linked from several locations: Label_0042:
            //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
            //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2604)
            //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
            //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
            //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:206)
            //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:662)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
            //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
            //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
            //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
            //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
            //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
            // 
            throw new IllegalStateException("An error occurred while decompiling this method.");
        }
        
        private void sendRepeatMode(final int p0) {
            // 
            // This method could not be decompiled.
            // 
            // Original Bytecode:
            // 
            //     1: getfield        android/support/v4/media/session/MediaSessionCompat$MediaSessionImplBase.mLock:Ljava/lang/Object;
            //     4: astore_3       
            //     5: aload_3        
            //     6: monitorenter   
            //     7: aload_0        
            //     8: getfield        android/support/v4/media/session/MediaSessionCompat$MediaSessionImplBase.mControllerCallbacks:Landroid/os/RemoteCallbackList;
            //    11: invokevirtual   android/os/RemoteCallbackList.beginBroadcast:()I
            //    14: iconst_1       
            //    15: isub           
            //    16: istore_2       
            //    17: iload_2        
            //    18: iflt            48
            //    21: aload_0        
            //    22: getfield        android/support/v4/media/session/MediaSessionCompat$MediaSessionImplBase.mControllerCallbacks:Landroid/os/RemoteCallbackList;
            //    25: iload_2        
            //    26: invokevirtual   android/os/RemoteCallbackList.getBroadcastItem:(I)Landroid/os/IInterface;
            //    29: checkcast       Landroid/support/v4/media/session/IMediaControllerCallback;
            //    32: astore          4
            //    34: aload           4
            //    36: iload_1        
            //    37: invokeinterface android/support/v4/media/session/IMediaControllerCallback.onRepeatModeChanged:(I)V
            //    42: iinc            2, -1
            //    45: goto            17
            //    48: aload_0        
            //    49: getfield        android/support/v4/media/session/MediaSessionCompat$MediaSessionImplBase.mControllerCallbacks:Landroid/os/RemoteCallbackList;
            //    52: invokevirtual   android/os/RemoteCallbackList.finishBroadcast:()V
            //    55: aload_3        
            //    56: monitorexit    
            //    57: return         
            //    58: astore          4
            //    60: aload_3        
            //    61: monitorexit    
            //    62: aload           4
            //    64: athrow         
            //    65: astore          4
            //    67: goto            42
            //    Exceptions:
            //  Try           Handler
            //  Start  End    Start  End    Type                        
            //  -----  -----  -----  -----  ----------------------------
            //  7      17     58     65     Any
            //  21     34     58     65     Any
            //  34     42     65     70     Landroid/os/RemoteException;
            //  34     42     58     65     Any
            //  48     57     58     65     Any
            //  60     62     58     65     Any
            // 
            // The error that occurred was:
            // 
            // java.lang.IllegalStateException: Expression is linked from several locations: Label_0042:
            //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
            //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2604)
            //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
            //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
            //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:206)
            //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:662)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
            //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
            //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
            //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
            //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
            //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
            // 
            throw new IllegalStateException("An error occurred while decompiling this method.");
        }
        
        private void sendSessionDestroyed() {
            // 
            // This method could not be decompiled.
            // 
            // Original Bytecode:
            // 
            //     1: getfield        android/support/v4/media/session/MediaSessionCompat$MediaSessionImplBase.mLock:Ljava/lang/Object;
            //     4: astore_2       
            //     5: aload_2        
            //     6: monitorenter   
            //     7: aload_0        
            //     8: getfield        android/support/v4/media/session/MediaSessionCompat$MediaSessionImplBase.mControllerCallbacks:Landroid/os/RemoteCallbackList;
            //    11: invokevirtual   android/os/RemoteCallbackList.beginBroadcast:()I
            //    14: iconst_1       
            //    15: isub           
            //    16: istore_1       
            //    17: iload_1        
            //    18: iflt            45
            //    21: aload_0        
            //    22: getfield        android/support/v4/media/session/MediaSessionCompat$MediaSessionImplBase.mControllerCallbacks:Landroid/os/RemoteCallbackList;
            //    25: iload_1        
            //    26: invokevirtual   android/os/RemoteCallbackList.getBroadcastItem:(I)Landroid/os/IInterface;
            //    29: checkcast       Landroid/support/v4/media/session/IMediaControllerCallback;
            //    32: astore_3       
            //    33: aload_3        
            //    34: invokeinterface android/support/v4/media/session/IMediaControllerCallback.onSessionDestroyed:()V
            //    39: iinc            1, -1
            //    42: goto            17
            //    45: aload_0        
            //    46: getfield        android/support/v4/media/session/MediaSessionCompat$MediaSessionImplBase.mControllerCallbacks:Landroid/os/RemoteCallbackList;
            //    49: invokevirtual   android/os/RemoteCallbackList.finishBroadcast:()V
            //    52: aload_0        
            //    53: getfield        android/support/v4/media/session/MediaSessionCompat$MediaSessionImplBase.mControllerCallbacks:Landroid/os/RemoteCallbackList;
            //    56: invokevirtual   android/os/RemoteCallbackList.kill:()V
            //    59: aload_2        
            //    60: monitorexit    
            //    61: return         
            //    62: astore_3       
            //    63: aload_2        
            //    64: monitorexit    
            //    65: aload_3        
            //    66: athrow         
            //    67: astore_3       
            //    68: goto            39
            //    Exceptions:
            //  Try           Handler
            //  Start  End    Start  End    Type                        
            //  -----  -----  -----  -----  ----------------------------
            //  7      17     62     67     Any
            //  21     33     62     67     Any
            //  33     39     67     71     Landroid/os/RemoteException;
            //  33     39     62     67     Any
            //  45     61     62     67     Any
            //  63     65     62     67     Any
            // 
            // The error that occurred was:
            // 
            // java.lang.IllegalStateException: Expression is linked from several locations: Label_0039:
            //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
            //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2604)
            //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
            //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
            //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:206)
            //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:662)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
            //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
            //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
            //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
            //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
            //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
            // 
            throw new IllegalStateException("An error occurred while decompiling this method.");
        }
        
        private void sendShuffleMode(final int p0) {
            // 
            // This method could not be decompiled.
            // 
            // Original Bytecode:
            // 
            //     1: getfield        android/support/v4/media/session/MediaSessionCompat$MediaSessionImplBase.mLock:Ljava/lang/Object;
            //     4: astore_3       
            //     5: aload_3        
            //     6: monitorenter   
            //     7: aload_0        
            //     8: getfield        android/support/v4/media/session/MediaSessionCompat$MediaSessionImplBase.mControllerCallbacks:Landroid/os/RemoteCallbackList;
            //    11: invokevirtual   android/os/RemoteCallbackList.beginBroadcast:()I
            //    14: iconst_1       
            //    15: isub           
            //    16: istore_2       
            //    17: iload_2        
            //    18: iflt            48
            //    21: aload_0        
            //    22: getfield        android/support/v4/media/session/MediaSessionCompat$MediaSessionImplBase.mControllerCallbacks:Landroid/os/RemoteCallbackList;
            //    25: iload_2        
            //    26: invokevirtual   android/os/RemoteCallbackList.getBroadcastItem:(I)Landroid/os/IInterface;
            //    29: checkcast       Landroid/support/v4/media/session/IMediaControllerCallback;
            //    32: astore          4
            //    34: aload           4
            //    36: iload_1        
            //    37: invokeinterface android/support/v4/media/session/IMediaControllerCallback.onShuffleModeChanged:(I)V
            //    42: iinc            2, -1
            //    45: goto            17
            //    48: aload_0        
            //    49: getfield        android/support/v4/media/session/MediaSessionCompat$MediaSessionImplBase.mControllerCallbacks:Landroid/os/RemoteCallbackList;
            //    52: invokevirtual   android/os/RemoteCallbackList.finishBroadcast:()V
            //    55: aload_3        
            //    56: monitorexit    
            //    57: return         
            //    58: astore          4
            //    60: aload_3        
            //    61: monitorexit    
            //    62: aload           4
            //    64: athrow         
            //    65: astore          4
            //    67: goto            42
            //    Exceptions:
            //  Try           Handler
            //  Start  End    Start  End    Type                        
            //  -----  -----  -----  -----  ----------------------------
            //  7      17     58     65     Any
            //  21     34     58     65     Any
            //  34     42     65     70     Landroid/os/RemoteException;
            //  34     42     58     65     Any
            //  48     57     58     65     Any
            //  60     62     58     65     Any
            // 
            // The error that occurred was:
            // 
            // java.lang.IllegalStateException: Expression is linked from several locations: Label_0042:
            //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
            //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2604)
            //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
            //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
            //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:206)
            //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:662)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
            //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
            //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
            //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
            //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
            //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
            // 
            throw new IllegalStateException("An error occurred while decompiling this method.");
        }
        
        private void sendState(final PlaybackStateCompat p0) {
            // 
            // This method could not be decompiled.
            // 
            // Original Bytecode:
            // 
            //     1: getfield        android/support/v4/media/session/MediaSessionCompat$MediaSessionImplBase.mLock:Ljava/lang/Object;
            //     4: astore_3       
            //     5: aload_3        
            //     6: monitorenter   
            //     7: aload_0        
            //     8: getfield        android/support/v4/media/session/MediaSessionCompat$MediaSessionImplBase.mControllerCallbacks:Landroid/os/RemoteCallbackList;
            //    11: invokevirtual   android/os/RemoteCallbackList.beginBroadcast:()I
            //    14: iconst_1       
            //    15: isub           
            //    16: istore_2       
            //    17: iload_2        
            //    18: iflt            48
            //    21: aload_0        
            //    22: getfield        android/support/v4/media/session/MediaSessionCompat$MediaSessionImplBase.mControllerCallbacks:Landroid/os/RemoteCallbackList;
            //    25: iload_2        
            //    26: invokevirtual   android/os/RemoteCallbackList.getBroadcastItem:(I)Landroid/os/IInterface;
            //    29: checkcast       Landroid/support/v4/media/session/IMediaControllerCallback;
            //    32: astore          4
            //    34: aload           4
            //    36: aload_1        
            //    37: invokeinterface android/support/v4/media/session/IMediaControllerCallback.onPlaybackStateChanged:(Landroid/support/v4/media/session/PlaybackStateCompat;)V
            //    42: iinc            2, -1
            //    45: goto            17
            //    48: aload_0        
            //    49: getfield        android/support/v4/media/session/MediaSessionCompat$MediaSessionImplBase.mControllerCallbacks:Landroid/os/RemoteCallbackList;
            //    52: invokevirtual   android/os/RemoteCallbackList.finishBroadcast:()V
            //    55: aload_3        
            //    56: monitorexit    
            //    57: return         
            //    58: astore_1       
            //    59: aload_3        
            //    60: monitorexit    
            //    61: aload_1        
            //    62: athrow         
            //    63: astore          4
            //    65: goto            42
            //    Exceptions:
            //  Try           Handler
            //  Start  End    Start  End    Type                        
            //  -----  -----  -----  -----  ----------------------------
            //  7      17     58     63     Any
            //  21     34     58     63     Any
            //  34     42     63     68     Landroid/os/RemoteException;
            //  34     42     58     63     Any
            //  48     57     58     63     Any
            //  59     61     58     63     Any
            // 
            // The error that occurred was:
            // 
            // java.lang.IllegalStateException: Expression is linked from several locations: Label_0042:
            //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
            //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2604)
            //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
            //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
            //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:206)
            //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:662)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
            //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
            //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
            //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
            //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
            //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
            // 
            throw new IllegalStateException("An error occurred while decompiling this method.");
        }
        
        void adjustVolume(final int n, final int n2) {
            if (this.mVolumeType == 2) {
                final VolumeProviderCompat mVolumeProvider = this.mVolumeProvider;
                if (mVolumeProvider != null) {
                    mVolumeProvider.onAdjustVolume(n);
                }
            }
            else {
                this.mAudioManager.adjustStreamVolume(this.mLocalStream, n, n2);
            }
        }
        
        RemoteControlClient$MetadataEditor buildRccMetadata(final Bundle bundle) {
            final RemoteControlClient$MetadataEditor editMetadata = this.mRcc.editMetadata(true);
            if (bundle == null) {
                return editMetadata;
            }
            if (bundle.containsKey("android.media.metadata.ART")) {
                final Bitmap bitmap = (Bitmap)bundle.getParcelable("android.media.metadata.ART");
                Bitmap copy;
                if ((copy = bitmap) != null) {
                    copy = bitmap.copy(bitmap.getConfig(), false);
                }
                editMetadata.putBitmap(100, copy);
            }
            else if (bundle.containsKey("android.media.metadata.ALBUM_ART")) {
                final Bitmap bitmap2 = (Bitmap)bundle.getParcelable("android.media.metadata.ALBUM_ART");
                Bitmap copy2;
                if ((copy2 = bitmap2) != null) {
                    copy2 = bitmap2.copy(bitmap2.getConfig(), false);
                }
                editMetadata.putBitmap(100, copy2);
            }
            if (bundle.containsKey("android.media.metadata.ALBUM")) {
                editMetadata.putString(1, bundle.getString("android.media.metadata.ALBUM"));
            }
            if (bundle.containsKey("android.media.metadata.ALBUM_ARTIST")) {
                editMetadata.putString(13, bundle.getString("android.media.metadata.ALBUM_ARTIST"));
            }
            if (bundle.containsKey("android.media.metadata.ARTIST")) {
                editMetadata.putString(2, bundle.getString("android.media.metadata.ARTIST"));
            }
            if (bundle.containsKey("android.media.metadata.AUTHOR")) {
                editMetadata.putString(3, bundle.getString("android.media.metadata.AUTHOR"));
            }
            if (bundle.containsKey("android.media.metadata.COMPILATION")) {
                editMetadata.putString(15, bundle.getString("android.media.metadata.COMPILATION"));
            }
            if (bundle.containsKey("android.media.metadata.COMPOSER")) {
                editMetadata.putString(4, bundle.getString("android.media.metadata.COMPOSER"));
            }
            if (bundle.containsKey("android.media.metadata.DATE")) {
                editMetadata.putString(5, bundle.getString("android.media.metadata.DATE"));
            }
            if (bundle.containsKey("android.media.metadata.DISC_NUMBER")) {
                editMetadata.putLong(14, bundle.getLong("android.media.metadata.DISC_NUMBER"));
            }
            if (bundle.containsKey("android.media.metadata.DURATION")) {
                editMetadata.putLong(9, bundle.getLong("android.media.metadata.DURATION"));
            }
            if (bundle.containsKey("android.media.metadata.GENRE")) {
                editMetadata.putString(6, bundle.getString("android.media.metadata.GENRE"));
            }
            if (bundle.containsKey("android.media.metadata.TITLE")) {
                editMetadata.putString(7, bundle.getString("android.media.metadata.TITLE"));
            }
            if (bundle.containsKey("android.media.metadata.TRACK_NUMBER")) {
                editMetadata.putLong(0, bundle.getLong("android.media.metadata.TRACK_NUMBER"));
            }
            if (bundle.containsKey("android.media.metadata.WRITER")) {
                editMetadata.putString(11, bundle.getString("android.media.metadata.WRITER"));
            }
            return editMetadata;
        }
        
        @Override
        public Callback getCallback() {
            synchronized (this.mLock) {
                return this.mCallback;
            }
        }
        
        @Override
        public String getCallingPackage() {
            return null;
        }
        
        @Override
        public MediaSessionManager.RemoteUserInfo getCurrentControllerInfo() {
            synchronized (this.mLock) {
                return this.mRemoteUserInfo;
            }
        }
        
        @Override
        public Object getMediaSession() {
            return null;
        }
        
        String getPackageNameForUid(final int n) {
            String nameForUid;
            if (TextUtils.isEmpty((CharSequence)(nameForUid = this.mContext.getPackageManager().getNameForUid(n)))) {
                nameForUid = "android.media.session.MediaController";
            }
            return nameForUid;
        }
        
        @Override
        public PlaybackStateCompat getPlaybackState() {
            synchronized (this.mLock) {
                return this.mState;
            }
        }
        
        int getRccStateFromState(final int n) {
            switch (n) {
                default: {
                    return -1;
                }
                case 10:
                case 11: {
                    return 6;
                }
                case 9: {
                    return 7;
                }
                case 7: {
                    return 9;
                }
                case 6:
                case 8: {
                    return 8;
                }
                case 5: {
                    return 5;
                }
                case 4: {
                    return 4;
                }
                case 3: {
                    return 3;
                }
                case 2: {
                    return 2;
                }
                case 1: {
                    return 1;
                }
                case 0: {
                    return 0;
                }
            }
        }
        
        int getRccTransportControlFlagsFromActions(final long n) {
            int n2;
            if ((0x1L & n) != 0x0L) {
                n2 = 32;
            }
            else {
                n2 = 0;
            }
            int n3 = n2;
            if ((0x2L & n) != 0x0L) {
                n3 = (n2 | 0x10);
            }
            int n4 = n3;
            if ((0x4L & n) != 0x0L) {
                n4 = (n3 | 0x4);
            }
            int n5 = n4;
            if ((0x8L & n) != 0x0L) {
                n5 = (n4 | 0x2);
            }
            int n6 = n5;
            if ((0x10L & n) != 0x0L) {
                n6 = (n5 | 0x1);
            }
            int n7 = n6;
            if ((0x20L & n) != 0x0L) {
                n7 = (n6 | 0x80);
            }
            int n8 = n7;
            if ((0x40L & n) != 0x0L) {
                n8 = (n7 | 0x40);
            }
            int n9 = n8;
            if ((n & 0x200L) != 0x0L) {
                n9 = (n8 | 0x8);
            }
            return n9;
        }
        
        @Override
        public Object getRemoteControlClient() {
            return null;
        }
        
        @Override
        public Token getSessionToken() {
            return this.mToken;
        }
        
        @Override
        public boolean isActive() {
            return this.mIsActive;
        }
        
        void postToHandler(int n, final int n2, final int n3, Object data, final Bundle bundle) {
            synchronized (this.mLock) {
                final MessageHandler mHandler = this.mHandler;
                if (mHandler != null) {
                    final Message obtainMessage = mHandler.obtainMessage(n, n2, n3, data);
                    data = new Bundle();
                    n = Binder.getCallingUid();
                    ((Bundle)data).putInt("data_calling_uid", n);
                    ((Bundle)data).putString("data_calling_pkg", this.getPackageNameForUid(n));
                    n = Binder.getCallingPid();
                    if (n > 0) {
                        ((Bundle)data).putInt("data_calling_pid", n);
                    }
                    else {
                        ((Bundle)data).putInt("data_calling_pid", -1);
                    }
                    if (bundle != null) {
                        ((Bundle)data).putBundle("data_extras", bundle);
                    }
                    obtainMessage.setData((Bundle)data);
                    obtainMessage.sendToTarget();
                }
            }
        }
        
        void registerMediaButtonEventReceiver(final PendingIntent pendingIntent, final ComponentName componentName) {
            this.mAudioManager.registerMediaButtonEventReceiver(componentName);
        }
        
        @Override
        public void release() {
            this.mIsActive = false;
            this.mDestroyed = true;
            this.updateMbrAndRcc();
            this.sendSessionDestroyed();
            this.setCallback(null, null);
        }
        
        @Override
        public void sendSessionEvent(final String s, final Bundle bundle) {
            this.sendEvent(s, bundle);
        }
        
        void sendVolumeInfoChanged(final ParcelableVolumeInfo p0) {
            // 
            // This method could not be decompiled.
            // 
            // Original Bytecode:
            // 
            //     4: astore_3       
            //     5: aload_3        
            //     6: monitorenter   
            //     7: aload_0        
            //     8: getfield        android/support/v4/media/session/MediaSessionCompat$MediaSessionImplBase.mControllerCallbacks:Landroid/os/RemoteCallbackList;
            //    11: invokevirtual   android/os/RemoteCallbackList.beginBroadcast:()I
            //    14: iconst_1       
            //    15: isub           
            //    16: istore_2       
            //    17: iload_2        
            //    18: iflt            48
            //    21: aload_0        
            //    22: getfield        android/support/v4/media/session/MediaSessionCompat$MediaSessionImplBase.mControllerCallbacks:Landroid/os/RemoteCallbackList;
            //    25: iload_2        
            //    26: invokevirtual   android/os/RemoteCallbackList.getBroadcastItem:(I)Landroid/os/IInterface;
            //    29: checkcast       Landroid/support/v4/media/session/IMediaControllerCallback;
            //    32: astore          4
            //    34: aload           4
            //    36: aload_1        
            //    37: invokeinterface android/support/v4/media/session/IMediaControllerCallback.onVolumeInfoChanged:(Landroid/support/v4/media/session/ParcelableVolumeInfo;)V
            //    42: iinc            2, -1
            //    45: goto            17
            //    48: aload_0        
            //    49: getfield        android/support/v4/media/session/MediaSessionCompat$MediaSessionImplBase.mControllerCallbacks:Landroid/os/RemoteCallbackList;
            //    52: invokevirtual   android/os/RemoteCallbackList.finishBroadcast:()V
            //    55: aload_3        
            //    56: monitorexit    
            //    57: return         
            //    58: astore_1       
            //    59: aload_3        
            //    60: monitorexit    
            //    61: aload_1        
            //    62: athrow         
            //    63: astore          4
            //    65: goto            42
            //    Exceptions:
            //  Try           Handler
            //  Start  End    Start  End    Type                        
            //  -----  -----  -----  -----  ----------------------------
            //  7      17     58     63     Any
            //  21     34     58     63     Any
            //  34     42     63     68     Landroid/os/RemoteException;
            //  34     42     58     63     Any
            //  48     57     58     63     Any
            //  59     61     58     63     Any
            // 
            // The error that occurred was:
            // 
            // java.lang.IllegalStateException: Expression is linked from several locations: Label_0042:
            //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
            //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2604)
            //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
            //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
            //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:206)
            //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:662)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
            //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
            //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
            //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
            //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
            //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
            // 
            throw new IllegalStateException("An error occurred while decompiling this method.");
        }
        
        @Override
        public void setActive(final boolean mIsActive) {
            if (mIsActive == this.mIsActive) {
                return;
            }
            this.mIsActive = mIsActive;
            this.updateMbrAndRcc();
        }
        
        @Override
        public void setCallback(final Callback mCallback, final Handler handler) {
            synchronized (this.mLock) {
                final MessageHandler mHandler = this.mHandler;
                if (mHandler != null) {
                    mHandler.removeCallbacksAndMessages((Object)null);
                }
                MessageHandler mHandler2;
                if (mCallback != null && handler != null) {
                    mHandler2 = new MessageHandler(handler.getLooper());
                }
                else {
                    mHandler2 = null;
                }
                this.mHandler = mHandler2;
                if (this.mCallback != mCallback && this.mCallback != null) {
                    this.mCallback.setSessionImpl(null, null);
                }
                this.mCallback = mCallback;
                if (this.mCallback != null) {
                    this.mCallback.setSessionImpl(this, handler);
                }
            }
        }
        
        @Override
        public void setCaptioningEnabled(final boolean mCaptioningEnabled) {
            if (this.mCaptioningEnabled != mCaptioningEnabled) {
                this.sendCaptioningEnabled(this.mCaptioningEnabled = mCaptioningEnabled);
            }
        }
        
        @Override
        public void setCurrentControllerInfo(final MediaSessionManager.RemoteUserInfo mRemoteUserInfo) {
            synchronized (this.mLock) {
                this.mRemoteUserInfo = mRemoteUserInfo;
            }
        }
        
        @Override
        public void setExtras(final Bundle mExtras) {
            this.sendExtras(this.mExtras = mExtras);
        }
        
        @Override
        public void setFlags(final int n) {
            synchronized (this.mLock) {
                this.mFlags = (n | 0x1 | 0x2);
            }
        }
        
        @Override
        public void setMediaButtonReceiver(final PendingIntent pendingIntent) {
        }
        
        @Override
        public void setMetadata(MediaMetadataCompat mediaMetadataCompat) {
            MediaMetadataCompat build = mediaMetadataCompat;
            if (mediaMetadataCompat != null) {
                build = new MediaMetadataCompat.Builder(mediaMetadataCompat, MediaSessionCompat.sMaxBitmapSize).build();
            }
            mediaMetadataCompat = (MediaMetadataCompat)this.mLock;
            synchronized (mediaMetadataCompat) {
                this.mMetadata = build;
                monitorexit(mediaMetadataCompat);
                this.sendMetadata(build);
                if (!this.mIsActive) {
                    return;
                }
                if (build == null) {
                    mediaMetadataCompat = null;
                }
                else {
                    mediaMetadataCompat = (MediaMetadataCompat)build.getBundle();
                }
                this.buildRccMetadata((Bundle)mediaMetadataCompat).apply();
            }
        }
        
        @Override
        public void setPlaybackState(final PlaybackStateCompat playbackStateCompat) {
            synchronized (this.mLock) {
                this.mState = playbackStateCompat;
                monitorexit(this.mLock);
                this.sendState(playbackStateCompat);
                if (!this.mIsActive) {
                    return;
                }
                if (playbackStateCompat == null) {
                    this.mRcc.setPlaybackState(0);
                    this.mRcc.setTransportControlFlags(0);
                }
                else {
                    this.setRccState(playbackStateCompat);
                    this.mRcc.setTransportControlFlags(this.getRccTransportControlFlagsFromActions(playbackStateCompat.getActions()));
                }
            }
        }
        
        @Override
        public void setPlaybackToLocal(int mLocalStream) {
            final VolumeProviderCompat mVolumeProvider = this.mVolumeProvider;
            if (mVolumeProvider != null) {
                mVolumeProvider.setCallback(null);
            }
            this.mLocalStream = mLocalStream;
            this.mVolumeType = 1;
            final int mVolumeType = this.mVolumeType;
            mLocalStream = this.mLocalStream;
            this.sendVolumeInfoChanged(new ParcelableVolumeInfo(mVolumeType, mLocalStream, 2, this.mAudioManager.getStreamMaxVolume(mLocalStream), this.mAudioManager.getStreamVolume(this.mLocalStream)));
        }
        
        @Override
        public void setPlaybackToRemote(final VolumeProviderCompat mVolumeProvider) {
            if (mVolumeProvider != null) {
                final VolumeProviderCompat mVolumeProvider2 = this.mVolumeProvider;
                if (mVolumeProvider2 != null) {
                    mVolumeProvider2.setCallback(null);
                }
                this.mVolumeType = 2;
                this.mVolumeProvider = mVolumeProvider;
                this.sendVolumeInfoChanged(new ParcelableVolumeInfo(this.mVolumeType, this.mLocalStream, this.mVolumeProvider.getVolumeControl(), this.mVolumeProvider.getMaxVolume(), this.mVolumeProvider.getCurrentVolume()));
                mVolumeProvider.setCallback(this.mVolumeCallback);
                return;
            }
            throw new IllegalArgumentException("volumeProvider may not be null");
        }
        
        @Override
        public void setQueue(final List<QueueItem> mQueue) {
            this.sendQueue(this.mQueue = mQueue);
        }
        
        @Override
        public void setQueueTitle(final CharSequence mQueueTitle) {
            this.sendQueueTitle(this.mQueueTitle = mQueueTitle);
        }
        
        @Override
        public void setRatingType(final int mRatingType) {
            this.mRatingType = mRatingType;
        }
        
        void setRccState(final PlaybackStateCompat playbackStateCompat) {
            this.mRcc.setPlaybackState(this.getRccStateFromState(playbackStateCompat.getState()));
        }
        
        @Override
        public void setRegistrationCallback(final RegistrationCallback registrationCallback, final Handler handler) {
            synchronized (this.mLock) {
                final RegistrationCallbackHandler mRegistrationCallbackHandler = this.mRegistrationCallbackHandler;
                if (mRegistrationCallbackHandler != null) {
                    mRegistrationCallbackHandler.removeCallbacksAndMessages((Object)null);
                }
                if (registrationCallback != null) {
                    this.mRegistrationCallbackHandler = new RegistrationCallbackHandler(handler.getLooper(), registrationCallback);
                }
                else {
                    this.mRegistrationCallbackHandler = null;
                }
            }
        }
        
        @Override
        public void setRepeatMode(final int mRepeatMode) {
            if (this.mRepeatMode != mRepeatMode) {
                this.sendRepeatMode(this.mRepeatMode = mRepeatMode);
            }
        }
        
        @Override
        public void setSessionActivity(final PendingIntent mSessionActivity) {
            synchronized (this.mLock) {
                this.mSessionActivity = mSessionActivity;
            }
        }
        
        @Override
        public void setShuffleMode(final int mShuffleMode) {
            if (this.mShuffleMode != mShuffleMode) {
                this.sendShuffleMode(this.mShuffleMode = mShuffleMode);
            }
        }
        
        void setVolumeTo(final int n, final int n2) {
            if (this.mVolumeType == 2) {
                final VolumeProviderCompat mVolumeProvider = this.mVolumeProvider;
                if (mVolumeProvider != null) {
                    mVolumeProvider.onSetVolumeTo(n);
                }
            }
            else {
                this.mAudioManager.setStreamVolume(this.mLocalStream, n, n2);
            }
        }
        
        void unregisterMediaButtonEventReceiver(final PendingIntent pendingIntent, final ComponentName componentName) {
            this.mAudioManager.unregisterMediaButtonEventReceiver(componentName);
        }
        
        void updateMbrAndRcc() {
            if (this.mIsActive) {
                this.registerMediaButtonEventReceiver(this.mMediaButtonReceiverIntent, this.mMediaButtonReceiverComponentName);
                this.mAudioManager.registerRemoteControlClient(this.mRcc);
                this.setMetadata(this.mMetadata);
                this.setPlaybackState(this.mState);
            }
            else {
                this.unregisterMediaButtonEventReceiver(this.mMediaButtonReceiverIntent, this.mMediaButtonReceiverComponentName);
                this.mRcc.setPlaybackState(0);
                this.mAudioManager.unregisterRemoteControlClient(this.mRcc);
            }
        }
        
        private static final class Command
        {
            public final String command;
            public final Bundle extras;
            public final ResultReceiver stub;
            
            public Command(final String command, final Bundle extras, final ResultReceiver stub) {
                this.command = command;
                this.extras = extras;
                this.stub = stub;
            }
        }
        
        class MediaSessionStub extends Stub
        {
            final MediaSessionImplBase this$0;
            
            MediaSessionStub(final MediaSessionImplBase this$0) {
                this.this$0 = this$0;
            }
            
            public void addQueueItem(final MediaDescriptionCompat mediaDescriptionCompat) {
                this.postToHandler(25, mediaDescriptionCompat);
            }
            
            public void addQueueItemAt(final MediaDescriptionCompat mediaDescriptionCompat, final int n) {
                this.postToHandler(26, mediaDescriptionCompat, n);
            }
            
            public void adjustVolume(final int n, final int n2, final String s) {
                this.this$0.adjustVolume(n, n2);
            }
            
            public void fastForward() throws RemoteException {
                this.postToHandler(16);
            }
            
            public Bundle getExtras() {
                synchronized (this.this$0.mLock) {
                    return this.this$0.mExtras;
                }
            }
            
            public long getFlags() {
                synchronized (this.this$0.mLock) {
                    return this.this$0.mFlags;
                }
            }
            
            public PendingIntent getLaunchPendingIntent() {
                synchronized (this.this$0.mLock) {
                    return this.this$0.mSessionActivity;
                }
            }
            
            public MediaMetadataCompat getMetadata() {
                return this.this$0.mMetadata;
            }
            
            public String getPackageName() {
                return this.this$0.mPackageName;
            }
            
            public PlaybackStateCompat getPlaybackState() {
                synchronized (this.this$0.mLock) {
                    final PlaybackStateCompat mState = this.this$0.mState;
                    final MediaMetadataCompat mMetadata = this.this$0.mMetadata;
                    monitorexit(this.this$0.mLock);
                    return MediaSessionCompat.getStateWithUpdatedPosition(mState, mMetadata);
                }
            }
            
            public List<QueueItem> getQueue() {
                synchronized (this.this$0.mLock) {
                    return this.this$0.mQueue;
                }
            }
            
            public CharSequence getQueueTitle() {
                return this.this$0.mQueueTitle;
            }
            
            public int getRatingType() {
                return this.this$0.mRatingType;
            }
            
            public int getRepeatMode() {
                return this.this$0.mRepeatMode;
            }
            
            public Bundle getSessionInfo() {
                Bundle bundle;
                if (this.this$0.mSessionInfo == null) {
                    bundle = null;
                }
                else {
                    bundle = new Bundle(this.this$0.mSessionInfo);
                }
                return bundle;
            }
            
            public int getShuffleMode() {
                return this.this$0.mShuffleMode;
            }
            
            public String getTag() {
                return this.this$0.mTag;
            }
            
            public ParcelableVolumeInfo getVolumeAttributes() {
                synchronized (this.this$0.mLock) {
                    final int mVolumeType = this.this$0.mVolumeType;
                    final int mLocalStream = this.this$0.mLocalStream;
                    final VolumeProviderCompat mVolumeProvider = this.this$0.mVolumeProvider;
                    int volumeControl;
                    int n;
                    int n2;
                    if (mVolumeType == 2) {
                        volumeControl = mVolumeProvider.getVolumeControl();
                        n = mVolumeProvider.getMaxVolume();
                        n2 = mVolumeProvider.getCurrentVolume();
                    }
                    else {
                        n = this.this$0.mAudioManager.getStreamMaxVolume(mLocalStream);
                        n2 = this.this$0.mAudioManager.getStreamVolume(mLocalStream);
                        volumeControl = 2;
                    }
                    monitorexit(this.this$0.mLock);
                    return new ParcelableVolumeInfo(mVolumeType, mLocalStream, volumeControl, n, n2);
                }
            }
            
            public boolean isCaptioningEnabled() {
                return this.this$0.mCaptioningEnabled;
            }
            
            public boolean isShuffleModeEnabledRemoved() {
                return false;
            }
            
            public boolean isTransportControlEnabled() {
                return true;
            }
            
            public void next() throws RemoteException {
                this.postToHandler(14);
            }
            
            public void pause() throws RemoteException {
                this.postToHandler(12);
            }
            
            public void play() throws RemoteException {
                this.postToHandler(7);
            }
            
            public void playFromMediaId(final String s, final Bundle bundle) throws RemoteException {
                this.postToHandler(8, s, bundle);
            }
            
            public void playFromSearch(final String s, final Bundle bundle) throws RemoteException {
                this.postToHandler(9, s, bundle);
            }
            
            public void playFromUri(final Uri uri, final Bundle bundle) throws RemoteException {
                this.postToHandler(10, uri, bundle);
            }
            
            void postToHandler(final int n) {
                this.this$0.postToHandler(n, 0, 0, null, null);
            }
            
            void postToHandler(final int n, final int n2) {
                this.this$0.postToHandler(n, n2, 0, null, null);
            }
            
            void postToHandler(final int n, final int n2, final int n3) {
                this.this$0.postToHandler(n, n2, n3, null, null);
            }
            
            void postToHandler(final int n, final Object o) {
                this.this$0.postToHandler(n, 0, 0, o, null);
            }
            
            void postToHandler(final int n, final Object o, final int n2) {
                this.this$0.postToHandler(n, n2, 0, o, null);
            }
            
            void postToHandler(final int n, final Object o, final Bundle bundle) {
                this.this$0.postToHandler(n, 0, 0, o, bundle);
            }
            
            public void prepare() throws RemoteException {
                this.postToHandler(3);
            }
            
            public void prepareFromMediaId(final String s, final Bundle bundle) throws RemoteException {
                this.postToHandler(4, s, bundle);
            }
            
            public void prepareFromSearch(final String s, final Bundle bundle) throws RemoteException {
                this.postToHandler(5, s, bundle);
            }
            
            public void prepareFromUri(final Uri uri, final Bundle bundle) throws RemoteException {
                this.postToHandler(6, uri, bundle);
            }
            
            public void previous() throws RemoteException {
                this.postToHandler(15);
            }
            
            public void rate(final RatingCompat ratingCompat) throws RemoteException {
                this.postToHandler(19, ratingCompat);
            }
            
            public void rateWithExtras(final RatingCompat ratingCompat, final Bundle bundle) throws RemoteException {
                this.postToHandler(31, ratingCompat, bundle);
            }
            
            public void registerCallbackListener(final IMediaControllerCallback mediaControllerCallback) {
                Label_0017: {
                    if (!this.this$0.mDestroyed) {
                        break Label_0017;
                    }
                    try {
                        mediaControllerCallback.onSessionDestroyed();
                        return;
                        final int callingPid = Binder.getCallingPid();
                        final int callingUid = Binder.getCallingUid();
                        this.this$0.mControllerCallbacks.register((IInterface)mediaControllerCallback, (Object)new MediaSessionManager.RemoteUserInfo(this.this$0.getPackageNameForUid(callingUid), callingPid, callingUid));
                        synchronized (this.this$0.mLock) {
                            if (this.this$0.mRegistrationCallbackHandler != null) {
                                this.this$0.mRegistrationCallbackHandler.postCallbackRegistered(callingPid, callingUid);
                            }
                        }
                    }
                    catch (final Exception ex) {}
                }
            }
            
            public void removeQueueItem(final MediaDescriptionCompat mediaDescriptionCompat) {
                this.postToHandler(27, mediaDescriptionCompat);
            }
            
            public void removeQueueItemAt(final int n) {
                this.postToHandler(28, n);
            }
            
            public void rewind() throws RemoteException {
                this.postToHandler(17);
            }
            
            public void seekTo(final long l) throws RemoteException {
                this.postToHandler(18, l);
            }
            
            public void sendCommand(final String s, final Bundle bundle, final ResultReceiverWrapper resultReceiverWrapper) {
                ResultReceiver mResultReceiver;
                if (resultReceiverWrapper == null) {
                    mResultReceiver = null;
                }
                else {
                    mResultReceiver = resultReceiverWrapper.mResultReceiver;
                }
                this.postToHandler(1, new Command(s, bundle, mResultReceiver));
            }
            
            public void sendCustomAction(final String s, final Bundle bundle) throws RemoteException {
                this.postToHandler(20, s, bundle);
            }
            
            public boolean sendMediaButton(final KeyEvent keyEvent) {
                this.postToHandler(21, keyEvent);
                return true;
            }
            
            public void setCaptioningEnabled(final boolean b) throws RemoteException {
                this.postToHandler(29, b);
            }
            
            public void setPlaybackSpeed(final float f) throws RemoteException {
                this.postToHandler(32, f);
            }
            
            public void setRepeatMode(final int n) throws RemoteException {
                this.postToHandler(23, n);
            }
            
            public void setShuffleMode(final int n) throws RemoteException {
                this.postToHandler(30, n);
            }
            
            public void setShuffleModeEnabledRemoved(final boolean b) throws RemoteException {
            }
            
            public void setVolumeTo(final int n, final int n2, final String s) {
                this.this$0.setVolumeTo(n, n2);
            }
            
            public void skipToQueueItem(final long l) {
                this.postToHandler(11, l);
            }
            
            public void stop() throws RemoteException {
                this.postToHandler(13);
            }
            
            public void unregisterCallbackListener(final IMediaControllerCallback mediaControllerCallback) {
                this.this$0.mControllerCallbacks.unregister((IInterface)mediaControllerCallback);
                final int callingPid = Binder.getCallingPid();
                final int callingUid = Binder.getCallingUid();
                synchronized (this.this$0.mLock) {
                    if (this.this$0.mRegistrationCallbackHandler != null) {
                        this.this$0.mRegistrationCallbackHandler.postCallbackUnregistered(callingPid, callingUid);
                    }
                }
            }
        }
        
        class MessageHandler extends Handler
        {
            private static final int KEYCODE_MEDIA_PAUSE = 127;
            private static final int KEYCODE_MEDIA_PLAY = 126;
            private static final int MSG_ADD_QUEUE_ITEM = 25;
            private static final int MSG_ADD_QUEUE_ITEM_AT = 26;
            private static final int MSG_ADJUST_VOLUME = 2;
            private static final int MSG_COMMAND = 1;
            private static final int MSG_CUSTOM_ACTION = 20;
            private static final int MSG_FAST_FORWARD = 16;
            private static final int MSG_MEDIA_BUTTON = 21;
            private static final int MSG_NEXT = 14;
            private static final int MSG_PAUSE = 12;
            private static final int MSG_PLAY = 7;
            private static final int MSG_PLAY_MEDIA_ID = 8;
            private static final int MSG_PLAY_SEARCH = 9;
            private static final int MSG_PLAY_URI = 10;
            private static final int MSG_PREPARE = 3;
            private static final int MSG_PREPARE_MEDIA_ID = 4;
            private static final int MSG_PREPARE_SEARCH = 5;
            private static final int MSG_PREPARE_URI = 6;
            private static final int MSG_PREVIOUS = 15;
            private static final int MSG_RATE = 19;
            private static final int MSG_RATE_EXTRA = 31;
            private static final int MSG_REMOVE_QUEUE_ITEM = 27;
            private static final int MSG_REMOVE_QUEUE_ITEM_AT = 28;
            private static final int MSG_REWIND = 17;
            private static final int MSG_SEEK_TO = 18;
            private static final int MSG_SET_CAPTIONING_ENABLED = 29;
            private static final int MSG_SET_PLAYBACK_SPEED = 32;
            private static final int MSG_SET_REPEAT_MODE = 23;
            private static final int MSG_SET_SHUFFLE_MODE = 30;
            private static final int MSG_SET_VOLUME = 22;
            private static final int MSG_SKIP_TO_ITEM = 11;
            private static final int MSG_STOP = 13;
            final MediaSessionImplBase this$0;
            
            public MessageHandler(final MediaSessionImplBase this$0, final Looper looper) {
                this.this$0 = this$0;
                super(looper);
            }
            
            private void onMediaButtonEvent(final KeyEvent keyEvent, final Callback callback) {
                if (keyEvent != null) {
                    if (keyEvent.getAction() == 0) {
                        long actions;
                        if (this.this$0.mState == null) {
                            actions = 0L;
                        }
                        else {
                            actions = this.this$0.mState.getActions();
                        }
                        final int keyCode = keyEvent.getKeyCode();
                        if (keyCode != 79) {
                            if (keyCode != 126) {
                                if (keyCode != 127) {
                                    switch (keyCode) {
                                        default: {
                                            return;
                                        }
                                        case 90: {
                                            if ((actions & 0x40L) != 0x0L) {
                                                callback.onFastForward();
                                            }
                                            return;
                                        }
                                        case 89: {
                                            if ((actions & 0x8L) != 0x0L) {
                                                callback.onRewind();
                                            }
                                            return;
                                        }
                                        case 88: {
                                            if ((actions & 0x10L) != 0x0L) {
                                                callback.onSkipToPrevious();
                                            }
                                            return;
                                        }
                                        case 87: {
                                            if ((actions & 0x20L) != 0x0L) {
                                                callback.onSkipToNext();
                                            }
                                            return;
                                        }
                                        case 86: {
                                            if ((actions & 0x1L) != 0x0L) {
                                                callback.onStop();
                                            }
                                            return;
                                        }
                                        case 85: {
                                            break;
                                        }
                                    }
                                }
                                else {
                                    if ((actions & 0x2L) != 0x0L) {
                                        callback.onPause();
                                    }
                                    return;
                                }
                            }
                            else {
                                if ((actions & 0x4L) != 0x0L) {
                                    callback.onPlay();
                                }
                                return;
                            }
                        }
                        Log.w("MediaSessionCompat", "KEYCODE_MEDIA_PLAY_PAUSE and KEYCODE_HEADSETHOOK are handled already");
                    }
                }
            }
            
            public void handleMessage(final Message message) {
                final Callback mCallback = this.this$0.mCallback;
                if (mCallback == null) {
                    return;
                }
                final Bundle data = message.getData();
                MediaSessionCompat.ensureClassLoader(data);
                this.this$0.setCurrentControllerInfo(new MediaSessionManager.RemoteUserInfo(data.getString("data_calling_pkg"), data.getInt("data_calling_pid"), data.getInt("data_calling_uid")));
                final Bundle bundle = data.getBundle("data_extras");
                MediaSessionCompat.ensureClassLoader(bundle);
                try {
                    switch (message.what) {
                        case 32: {
                            mCallback.onSetPlaybackSpeed((float)message.obj);
                            break;
                        }
                        case 31: {
                            mCallback.onSetRating((RatingCompat)message.obj, bundle);
                            break;
                        }
                        case 30: {
                            mCallback.onSetShuffleMode(message.arg1);
                            break;
                        }
                        case 29: {
                            mCallback.onSetCaptioningEnabled((boolean)message.obj);
                            break;
                        }
                        case 28: {
                            if (this.this$0.mQueue == null) {
                                break;
                            }
                            QueueItem queueItem;
                            if (message.arg1 >= 0 && message.arg1 < this.this$0.mQueue.size()) {
                                queueItem = this.this$0.mQueue.get(message.arg1);
                            }
                            else {
                                queueItem = null;
                            }
                            if (queueItem != null) {
                                mCallback.onRemoveQueueItem(queueItem.getDescription());
                                break;
                            }
                            break;
                        }
                        case 27: {
                            mCallback.onRemoveQueueItem((MediaDescriptionCompat)message.obj);
                            break;
                        }
                        case 26: {
                            mCallback.onAddQueueItem((MediaDescriptionCompat)message.obj, message.arg1);
                            break;
                        }
                        case 25: {
                            mCallback.onAddQueueItem((MediaDescriptionCompat)message.obj);
                            break;
                        }
                        case 23: {
                            mCallback.onSetRepeatMode(message.arg1);
                            break;
                        }
                        case 22: {
                            this.this$0.setVolumeTo(message.arg1, 0);
                            break;
                        }
                        case 21: {
                            final KeyEvent keyEvent = (KeyEvent)message.obj;
                            final Intent intent = new Intent("android.intent.action.MEDIA_BUTTON");
                            intent.putExtra("android.intent.extra.KEY_EVENT", (Parcelable)keyEvent);
                            if (!mCallback.onMediaButtonEvent(intent)) {
                                this.onMediaButtonEvent(keyEvent, mCallback);
                                break;
                            }
                            break;
                        }
                        case 20: {
                            mCallback.onCustomAction((String)message.obj, bundle);
                            break;
                        }
                        case 19: {
                            mCallback.onSetRating((RatingCompat)message.obj);
                            break;
                        }
                        case 18: {
                            mCallback.onSeekTo((long)message.obj);
                            break;
                        }
                        case 17: {
                            mCallback.onRewind();
                            break;
                        }
                        case 16: {
                            mCallback.onFastForward();
                            break;
                        }
                        case 15: {
                            mCallback.onSkipToPrevious();
                            break;
                        }
                        case 14: {
                            mCallback.onSkipToNext();
                            break;
                        }
                        case 13: {
                            mCallback.onStop();
                            break;
                        }
                        case 12: {
                            mCallback.onPause();
                            break;
                        }
                        case 11: {
                            mCallback.onSkipToQueueItem((long)message.obj);
                            break;
                        }
                        case 10: {
                            mCallback.onPlayFromUri((Uri)message.obj, bundle);
                            break;
                        }
                        case 9: {
                            mCallback.onPlayFromSearch((String)message.obj, bundle);
                            break;
                        }
                        case 8: {
                            mCallback.onPlayFromMediaId((String)message.obj, bundle);
                            break;
                        }
                        case 7: {
                            mCallback.onPlay();
                            break;
                        }
                        case 6: {
                            mCallback.onPrepareFromUri((Uri)message.obj, bundle);
                            break;
                        }
                        case 5: {
                            mCallback.onPrepareFromSearch((String)message.obj, bundle);
                            break;
                        }
                        case 4: {
                            mCallback.onPrepareFromMediaId((String)message.obj, bundle);
                            break;
                        }
                        case 3: {
                            mCallback.onPrepare();
                            break;
                        }
                        case 2: {
                            this.this$0.adjustVolume(message.arg1, 0);
                            break;
                        }
                        case 1: {
                            final Command command = (Command)message.obj;
                            mCallback.onCommand(command.command, command.extras, command.stub);
                            break;
                        }
                    }
                }
                finally {
                    this.this$0.setCurrentControllerInfo(null);
                }
            }
        }
    }
    
    static class MediaSessionImplApi19 extends MediaSessionImplApi18
    {
        MediaSessionImplApi19(final Context context, final String s, final ComponentName componentName, final PendingIntent pendingIntent, final VersionedParcelable versionedParcelable, final Bundle bundle) {
            super(context, s, componentName, pendingIntent, versionedParcelable, bundle);
        }
        
        @Override
        RemoteControlClient$MetadataEditor buildRccMetadata(final Bundle bundle) {
            final RemoteControlClient$MetadataEditor buildRccMetadata = super.buildRccMetadata(bundle);
            long actions;
            if (this.mState == null) {
                actions = 0L;
            }
            else {
                actions = this.mState.getActions();
            }
            if ((actions & 0x80L) != 0x0L) {
                buildRccMetadata.addEditableKey(268435457);
            }
            if (bundle == null) {
                return buildRccMetadata;
            }
            if (bundle.containsKey("android.media.metadata.YEAR")) {
                buildRccMetadata.putLong(8, bundle.getLong("android.media.metadata.YEAR"));
            }
            if (bundle.containsKey("android.media.metadata.RATING")) {
                ((MediaMetadataEditor)buildRccMetadata).putObject(101, (Object)bundle.getParcelable("android.media.metadata.RATING"));
            }
            if (bundle.containsKey("android.media.metadata.USER_RATING")) {
                ((MediaMetadataEditor)buildRccMetadata).putObject(268435457, (Object)bundle.getParcelable("android.media.metadata.USER_RATING"));
            }
            return buildRccMetadata;
        }
        
        @Override
        int getRccTransportControlFlagsFromActions(final long n) {
            int rccTransportControlFlagsFromActions = super.getRccTransportControlFlagsFromActions(n);
            if ((n & 0x80L) != 0x0L) {
                rccTransportControlFlagsFromActions |= 0x200;
            }
            return rccTransportControlFlagsFromActions;
        }
        
        @Override
        public void setCallback(final Callback callback, final Handler handler) {
            super.setCallback(callback, handler);
            if (callback == null) {
                this.mRcc.setMetadataUpdateListener((RemoteControlClient$OnMetadataUpdateListener)null);
            }
            else {
                this.mRcc.setMetadataUpdateListener((RemoteControlClient$OnMetadataUpdateListener)new RemoteControlClient$OnMetadataUpdateListener(this) {
                    final MediaSessionImplApi19 this$0;
                    
                    public void onMetadataUpdate(final int n, final Object o) {
                        if (n == 268435457 && o instanceof Rating) {
                            ((MediaSessionImplBase)this.this$0).postToHandler(19, -1, -1, RatingCompat.fromRating(o), null);
                        }
                    }
                });
            }
        }
    }
    
    static class MediaSessionImplApi21 implements MediaSessionImpl
    {
        Callback mCallback;
        boolean mCaptioningEnabled;
        boolean mDestroyed;
        final RemoteCallbackList<IMediaControllerCallback> mExtraControllerCallbacks;
        final Object mLock;
        MediaMetadataCompat mMetadata;
        PlaybackStateCompat mPlaybackState;
        List<QueueItem> mQueue;
        int mRatingType;
        RegistrationCallbackHandler mRegistrationCallbackHandler;
        MediaSessionManager.RemoteUserInfo mRemoteUserInfo;
        int mRepeatMode;
        final MediaSession mSessionFwk;
        Bundle mSessionInfo;
        int mShuffleMode;
        final Token mToken;
        
        MediaSessionImplApi21(final Context context, final String s, final VersionedParcelable versionedParcelable, final Bundle mSessionInfo) {
            this.mLock = new Object();
            this.mDestroyed = false;
            this.mExtraControllerCallbacks = (RemoteCallbackList<IMediaControllerCallback>)new RemoteCallbackList();
            final MediaSession fwkMediaSession = this.createFwkMediaSession(context, s, mSessionInfo);
            this.mSessionFwk = fwkMediaSession;
            this.mToken = new Token(fwkMediaSession.getSessionToken(), new ExtraSession(), versionedParcelable);
            this.mSessionInfo = mSessionInfo;
            this.setFlags(3);
        }
        
        MediaSessionImplApi21(final Object o) {
            this.mLock = new Object();
            this.mDestroyed = false;
            this.mExtraControllerCallbacks = (RemoteCallbackList<IMediaControllerCallback>)new RemoteCallbackList();
            if (o instanceof MediaSession) {
                final MediaSession mSessionFwk = (MediaSession)o;
                this.mSessionFwk = mSessionFwk;
                this.mToken = new Token(mSessionFwk.getSessionToken(), new ExtraSession());
                this.mSessionInfo = null;
                this.setFlags(3);
                return;
            }
            throw new IllegalArgumentException("mediaSession is not a valid MediaSession object");
        }
        
        public MediaSession createFwkMediaSession(final Context context, final String s, final Bundle bundle) {
            return new MediaSession(context, s);
        }
        
        @Override
        public Callback getCallback() {
            synchronized (this.mLock) {
                return this.mCallback;
            }
        }
        
        @Override
        public String getCallingPackage() {
            if (Build$VERSION.SDK_INT < 24) {
                return null;
            }
            try {
                return (String)this.mSessionFwk.getClass().getMethod("getCallingPackage", (Class<?>[])new Class[0]).invoke(this.mSessionFwk, new Object[0]);
            }
            catch (final Exception ex) {
                Log.e("MediaSessionCompat", "Cannot execute MediaSession.getCallingPackage()", (Throwable)ex);
                return null;
            }
        }
        
        @Override
        public MediaSessionManager.RemoteUserInfo getCurrentControllerInfo() {
            synchronized (this.mLock) {
                return this.mRemoteUserInfo;
            }
        }
        
        @Override
        public Object getMediaSession() {
            return this.mSessionFwk;
        }
        
        @Override
        public PlaybackStateCompat getPlaybackState() {
            return this.mPlaybackState;
        }
        
        @Override
        public Object getRemoteControlClient() {
            return null;
        }
        
        @Override
        public Token getSessionToken() {
            return this.mToken;
        }
        
        @Override
        public boolean isActive() {
            return this.mSessionFwk.isActive();
        }
        
        @Override
        public void release() {
            this.mDestroyed = true;
            this.mExtraControllerCallbacks.kill();
            if (Build$VERSION.SDK_INT == 27) {
                try {
                    final Field declaredField = this.mSessionFwk.getClass().getDeclaredField("mCallback");
                    declaredField.setAccessible(true);
                    final Handler handler = (Handler)declaredField.get(this.mSessionFwk);
                    if (handler != null) {
                        handler.removeCallbacksAndMessages((Object)null);
                    }
                }
                catch (final Exception ex) {
                    Log.w("MediaSessionCompat", "Exception happened while accessing MediaSession.mCallback.", (Throwable)ex);
                }
            }
            this.mSessionFwk.setCallback((MediaSession$Callback)null);
            this.mSessionFwk.release();
        }
        
        @Override
        public void sendSessionEvent(final String p0, final Bundle p1) {
            // 
            // This method could not be decompiled.
            // 
            // Original Bytecode:
            // 
            //     3: bipush          23
            //     5: if_icmpge       78
            //     8: aload_0        
            //     9: getfield        android/support/v4/media/session/MediaSessionCompat$MediaSessionImplApi21.mLock:Ljava/lang/Object;
            //    12: astore          4
            //    14: aload           4
            //    16: monitorenter   
            //    17: aload_0        
            //    18: getfield        android/support/v4/media/session/MediaSessionCompat$MediaSessionImplApi21.mExtraControllerCallbacks:Landroid/os/RemoteCallbackList;
            //    21: invokevirtual   android/os/RemoteCallbackList.beginBroadcast:()I
            //    24: iconst_1       
            //    25: isub           
            //    26: istore_3       
            //    27: iload_3        
            //    28: iflt            59
            //    31: aload_0        
            //    32: getfield        android/support/v4/media/session/MediaSessionCompat$MediaSessionImplApi21.mExtraControllerCallbacks:Landroid/os/RemoteCallbackList;
            //    35: iload_3        
            //    36: invokevirtual   android/os/RemoteCallbackList.getBroadcastItem:(I)Landroid/os/IInterface;
            //    39: checkcast       Landroid/support/v4/media/session/IMediaControllerCallback;
            //    42: astore          5
            //    44: aload           5
            //    46: aload_1        
            //    47: aload_2        
            //    48: invokeinterface android/support/v4/media/session/IMediaControllerCallback.onEvent:(Ljava/lang/String;Landroid/os/Bundle;)V
            //    53: iinc            3, -1
            //    56: goto            27
            //    59: aload_0        
            //    60: getfield        android/support/v4/media/session/MediaSessionCompat$MediaSessionImplApi21.mExtraControllerCallbacks:Landroid/os/RemoteCallbackList;
            //    63: invokevirtual   android/os/RemoteCallbackList.finishBroadcast:()V
            //    66: aload           4
            //    68: monitorexit    
            //    69: goto            78
            //    72: astore_1       
            //    73: aload           4
            //    75: monitorexit    
            //    76: aload_1        
            //    77: athrow         
            //    78: aload_0        
            //    79: getfield        android/support/v4/media/session/MediaSessionCompat$MediaSessionImplApi21.mSessionFwk:Landroid/media/session/MediaSession;
            //    82: aload_1        
            //    83: aload_2        
            //    84: invokevirtual   android/media/session/MediaSession.sendSessionEvent:(Ljava/lang/String;Landroid/os/Bundle;)V
            //    87: return         
            //    88: astore          5
            //    90: goto            53
            //    Exceptions:
            //  Try           Handler
            //  Start  End    Start  End    Type                        
            //  -----  -----  -----  -----  ----------------------------
            //  17     27     72     78     Any
            //  31     44     72     78     Any
            //  44     53     88     93     Landroid/os/RemoteException;
            //  44     53     72     78     Any
            //  59     69     72     78     Any
            //  73     76     72     78     Any
            // 
            // The error that occurred was:
            // 
            // java.lang.IllegalStateException: Expression is linked from several locations: Label_0053:
            //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
            //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2604)
            //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
            //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
            //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:206)
            //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:662)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
            //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
            //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
            //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
            //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
            //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
            // 
            throw new IllegalStateException("An error occurred while decompiling this method.");
        }
        
        @Override
        public void setActive(final boolean active) {
            this.mSessionFwk.setActive(active);
        }
        
        @Override
        public void setCallback(final Callback mCallback, final Handler handler) {
            synchronized (this.mLock) {
                this.mCallback = mCallback;
                final MediaSession mSessionFwk = this.mSessionFwk;
                MediaSession$Callback mCallbackFwk;
                if (mCallback == null) {
                    mCallbackFwk = null;
                }
                else {
                    mCallbackFwk = mCallback.mCallbackFwk;
                }
                mSessionFwk.setCallback(mCallbackFwk, handler);
                if (mCallback != null) {
                    mCallback.setSessionImpl(this, handler);
                }
            }
        }
        
        @Override
        public void setCaptioningEnabled(final boolean p0) {
            // 
            // This method could not be decompiled.
            // 
            // Original Bytecode:
            // 
            //     1: getfield        android/support/v4/media/session/MediaSessionCompat$MediaSessionImplApi21.mCaptioningEnabled:Z
            //     4: iload_1        
            //     5: if_icmpeq       80
            //     8: aload_0        
            //     9: iload_1        
            //    10: putfield        android/support/v4/media/session/MediaSessionCompat$MediaSessionImplApi21.mCaptioningEnabled:Z
            //    13: aload_0        
            //    14: getfield        android/support/v4/media/session/MediaSessionCompat$MediaSessionImplApi21.mLock:Ljava/lang/Object;
            //    17: astore_3       
            //    18: aload_3        
            //    19: monitorenter   
            //    20: aload_0        
            //    21: getfield        android/support/v4/media/session/MediaSessionCompat$MediaSessionImplApi21.mExtraControllerCallbacks:Landroid/os/RemoteCallbackList;
            //    24: invokevirtual   android/os/RemoteCallbackList.beginBroadcast:()I
            //    27: iconst_1       
            //    28: isub           
            //    29: istore_2       
            //    30: iload_2        
            //    31: iflt            61
            //    34: aload_0        
            //    35: getfield        android/support/v4/media/session/MediaSessionCompat$MediaSessionImplApi21.mExtraControllerCallbacks:Landroid/os/RemoteCallbackList;
            //    38: iload_2        
            //    39: invokevirtual   android/os/RemoteCallbackList.getBroadcastItem:(I)Landroid/os/IInterface;
            //    42: checkcast       Landroid/support/v4/media/session/IMediaControllerCallback;
            //    45: astore          4
            //    47: aload           4
            //    49: iload_1        
            //    50: invokeinterface android/support/v4/media/session/IMediaControllerCallback.onCaptioningEnabledChanged:(Z)V
            //    55: iinc            2, -1
            //    58: goto            30
            //    61: aload_0        
            //    62: getfield        android/support/v4/media/session/MediaSessionCompat$MediaSessionImplApi21.mExtraControllerCallbacks:Landroid/os/RemoteCallbackList;
            //    65: invokevirtual   android/os/RemoteCallbackList.finishBroadcast:()V
            //    68: aload_3        
            //    69: monitorexit    
            //    70: goto            80
            //    73: astore          4
            //    75: aload_3        
            //    76: monitorexit    
            //    77: aload           4
            //    79: athrow         
            //    80: return         
            //    81: astore          4
            //    83: goto            55
            //    Exceptions:
            //  Try           Handler
            //  Start  End    Start  End    Type                        
            //  -----  -----  -----  -----  ----------------------------
            //  20     30     73     80     Any
            //  34     47     73     80     Any
            //  47     55     81     86     Landroid/os/RemoteException;
            //  47     55     73     80     Any
            //  61     70     73     80     Any
            //  75     77     73     80     Any
            // 
            // The error that occurred was:
            // 
            // java.lang.IllegalStateException: Expression is linked from several locations: Label_0055:
            //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
            //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2604)
            //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
            //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
            //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:206)
            //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:662)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
            //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
            //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
            //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
            //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
            //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
            // 
            throw new IllegalStateException("An error occurred while decompiling this method.");
        }
        
        @Override
        public void setCurrentControllerInfo(final MediaSessionManager.RemoteUserInfo mRemoteUserInfo) {
            synchronized (this.mLock) {
                this.mRemoteUserInfo = mRemoteUserInfo;
            }
        }
        
        @Override
        public void setExtras(final Bundle extras) {
            this.mSessionFwk.setExtras(extras);
        }
        
        @Override
        public void setFlags(final int n) {
            this.mSessionFwk.setFlags(n | 0x1 | 0x2);
        }
        
        @Override
        public void setMediaButtonReceiver(final PendingIntent mediaButtonReceiver) {
            this.mSessionFwk.setMediaButtonReceiver(mediaButtonReceiver);
        }
        
        @Override
        public void setMetadata(final MediaMetadataCompat mMetadata) {
            this.mMetadata = mMetadata;
            final MediaSession mSessionFwk = this.mSessionFwk;
            MediaMetadata metadata;
            if (mMetadata == null) {
                metadata = null;
            }
            else {
                metadata = (MediaMetadata)mMetadata.getMediaMetadata();
            }
            mSessionFwk.setMetadata(metadata);
        }
        
        @Override
        public void setPlaybackState(final PlaybackStateCompat p0) {
            // 
            // This method could not be decompiled.
            // 
            // Original Bytecode:
            // 
            //     1: aload_1        
            //     2: putfield        android/support/v4/media/session/MediaSessionCompat$MediaSessionImplApi21.mPlaybackState:Landroid/support/v4/media/session/PlaybackStateCompat;
            //     5: aload_0        
            //     6: getfield        android/support/v4/media/session/MediaSessionCompat$MediaSessionImplApi21.mLock:Ljava/lang/Object;
            //     9: astore_3       
            //    10: aload_3        
            //    11: monitorenter   
            //    12: aload_0        
            //    13: getfield        android/support/v4/media/session/MediaSessionCompat$MediaSessionImplApi21.mExtraControllerCallbacks:Landroid/os/RemoteCallbackList;
            //    16: invokevirtual   android/os/RemoteCallbackList.beginBroadcast:()I
            //    19: iconst_1       
            //    20: isub           
            //    21: istore_2       
            //    22: iload_2        
            //    23: iflt            53
            //    26: aload_0        
            //    27: getfield        android/support/v4/media/session/MediaSessionCompat$MediaSessionImplApi21.mExtraControllerCallbacks:Landroid/os/RemoteCallbackList;
            //    30: iload_2        
            //    31: invokevirtual   android/os/RemoteCallbackList.getBroadcastItem:(I)Landroid/os/IInterface;
            //    34: checkcast       Landroid/support/v4/media/session/IMediaControllerCallback;
            //    37: astore          4
            //    39: aload           4
            //    41: aload_1        
            //    42: invokeinterface android/support/v4/media/session/IMediaControllerCallback.onPlaybackStateChanged:(Landroid/support/v4/media/session/PlaybackStateCompat;)V
            //    47: iinc            2, -1
            //    50: goto            22
            //    53: aload_0        
            //    54: getfield        android/support/v4/media/session/MediaSessionCompat$MediaSessionImplApi21.mExtraControllerCallbacks:Landroid/os/RemoteCallbackList;
            //    57: invokevirtual   android/os/RemoteCallbackList.finishBroadcast:()V
            //    60: aload_3        
            //    61: monitorexit    
            //    62: aload_0        
            //    63: getfield        android/support/v4/media/session/MediaSessionCompat$MediaSessionImplApi21.mSessionFwk:Landroid/media/session/MediaSession;
            //    66: astore_3       
            //    67: aload_1        
            //    68: ifnonnull       76
            //    71: aconst_null    
            //    72: astore_1       
            //    73: goto            84
            //    76: aload_1        
            //    77: invokevirtual   android/support/v4/media/session/PlaybackStateCompat.getPlaybackState:()Ljava/lang/Object;
            //    80: checkcast       Landroid/media/session/PlaybackState;
            //    83: astore_1       
            //    84: aload_3        
            //    85: aload_1        
            //    86: invokevirtual   android/media/session/MediaSession.setPlaybackState:(Landroid/media/session/PlaybackState;)V
            //    89: return         
            //    90: astore_1       
            //    91: aload_3        
            //    92: monitorexit    
            //    93: aload_1        
            //    94: athrow         
            //    95: astore          4
            //    97: goto            47
            //    Exceptions:
            //  Try           Handler
            //  Start  End    Start  End    Type                        
            //  -----  -----  -----  -----  ----------------------------
            //  12     22     90     95     Any
            //  26     39     90     95     Any
            //  39     47     95     100    Landroid/os/RemoteException;
            //  39     47     90     95     Any
            //  53     62     90     95     Any
            //  91     93     90     95     Any
            // 
            // The error that occurred was:
            // 
            // java.lang.IllegalStateException: Expression is linked from several locations: Label_0047:
            //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
            //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2604)
            //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
            //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
            //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:206)
            //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:662)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
            //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
            //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
            //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
            //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
            //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
            // 
            throw new IllegalStateException("An error occurred while decompiling this method.");
        }
        
        @Override
        public void setPlaybackToLocal(final int legacyStreamType) {
            final AudioAttributes$Builder audioAttributes$Builder = new AudioAttributes$Builder();
            audioAttributes$Builder.setLegacyStreamType(legacyStreamType);
            this.mSessionFwk.setPlaybackToLocal(audioAttributes$Builder.build());
        }
        
        @Override
        public void setPlaybackToRemote(final VolumeProviderCompat volumeProviderCompat) {
            this.mSessionFwk.setPlaybackToRemote((VolumeProvider)volumeProviderCompat.getVolumeProvider());
        }
        
        @Override
        public void setQueue(final List<QueueItem> mQueue) {
            this.mQueue = mQueue;
            if (mQueue == null) {
                this.mSessionFwk.setQueue((List)null);
                return;
            }
            final ArrayList queue = new ArrayList(mQueue.size());
            final Iterator iterator = mQueue.iterator();
            while (iterator.hasNext()) {
                queue.add(((QueueItem)iterator.next()).getQueueItem());
            }
            this.mSessionFwk.setQueue((List)queue);
        }
        
        @Override
        public void setQueueTitle(final CharSequence queueTitle) {
            this.mSessionFwk.setQueueTitle(queueTitle);
        }
        
        @Override
        public void setRatingType(final int mRatingType) {
            this.mRatingType = mRatingType;
        }
        
        @Override
        public void setRegistrationCallback(final RegistrationCallback registrationCallback, final Handler handler) {
            synchronized (this.mLock) {
                final RegistrationCallbackHandler mRegistrationCallbackHandler = this.mRegistrationCallbackHandler;
                if (mRegistrationCallbackHandler != null) {
                    mRegistrationCallbackHandler.removeCallbacksAndMessages((Object)null);
                }
                if (registrationCallback != null) {
                    this.mRegistrationCallbackHandler = new RegistrationCallbackHandler(handler.getLooper(), registrationCallback);
                }
                else {
                    this.mRegistrationCallbackHandler = null;
                }
            }
        }
        
        @Override
        public void setRepeatMode(final int p0) {
            // 
            // This method could not be decompiled.
            // 
            // Original Bytecode:
            // 
            //     1: getfield        android/support/v4/media/session/MediaSessionCompat$MediaSessionImplApi21.mRepeatMode:I
            //     4: iload_1        
            //     5: if_icmpeq       80
            //     8: aload_0        
            //     9: iload_1        
            //    10: putfield        android/support/v4/media/session/MediaSessionCompat$MediaSessionImplApi21.mRepeatMode:I
            //    13: aload_0        
            //    14: getfield        android/support/v4/media/session/MediaSessionCompat$MediaSessionImplApi21.mLock:Ljava/lang/Object;
            //    17: astore_3       
            //    18: aload_3        
            //    19: monitorenter   
            //    20: aload_0        
            //    21: getfield        android/support/v4/media/session/MediaSessionCompat$MediaSessionImplApi21.mExtraControllerCallbacks:Landroid/os/RemoteCallbackList;
            //    24: invokevirtual   android/os/RemoteCallbackList.beginBroadcast:()I
            //    27: iconst_1       
            //    28: isub           
            //    29: istore_2       
            //    30: iload_2        
            //    31: iflt            61
            //    34: aload_0        
            //    35: getfield        android/support/v4/media/session/MediaSessionCompat$MediaSessionImplApi21.mExtraControllerCallbacks:Landroid/os/RemoteCallbackList;
            //    38: iload_2        
            //    39: invokevirtual   android/os/RemoteCallbackList.getBroadcastItem:(I)Landroid/os/IInterface;
            //    42: checkcast       Landroid/support/v4/media/session/IMediaControllerCallback;
            //    45: astore          4
            //    47: aload           4
            //    49: iload_1        
            //    50: invokeinterface android/support/v4/media/session/IMediaControllerCallback.onRepeatModeChanged:(I)V
            //    55: iinc            2, -1
            //    58: goto            30
            //    61: aload_0        
            //    62: getfield        android/support/v4/media/session/MediaSessionCompat$MediaSessionImplApi21.mExtraControllerCallbacks:Landroid/os/RemoteCallbackList;
            //    65: invokevirtual   android/os/RemoteCallbackList.finishBroadcast:()V
            //    68: aload_3        
            //    69: monitorexit    
            //    70: goto            80
            //    73: astore          4
            //    75: aload_3        
            //    76: monitorexit    
            //    77: aload           4
            //    79: athrow         
            //    80: return         
            //    81: astore          4
            //    83: goto            55
            //    Exceptions:
            //  Try           Handler
            //  Start  End    Start  End    Type                        
            //  -----  -----  -----  -----  ----------------------------
            //  20     30     73     80     Any
            //  34     47     73     80     Any
            //  47     55     81     86     Landroid/os/RemoteException;
            //  47     55     73     80     Any
            //  61     70     73     80     Any
            //  75     77     73     80     Any
            // 
            // The error that occurred was:
            // 
            // java.lang.IllegalStateException: Expression is linked from several locations: Label_0055:
            //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
            //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2604)
            //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
            //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
            //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:206)
            //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:662)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
            //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
            //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
            //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
            //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
            //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
            // 
            throw new IllegalStateException("An error occurred while decompiling this method.");
        }
        
        @Override
        public void setSessionActivity(final PendingIntent sessionActivity) {
            this.mSessionFwk.setSessionActivity(sessionActivity);
        }
        
        @Override
        public void setShuffleMode(final int p0) {
            // 
            // This method could not be decompiled.
            // 
            // Original Bytecode:
            // 
            //     1: getfield        android/support/v4/media/session/MediaSessionCompat$MediaSessionImplApi21.mShuffleMode:I
            //     4: iload_1        
            //     5: if_icmpeq       80
            //     8: aload_0        
            //     9: iload_1        
            //    10: putfield        android/support/v4/media/session/MediaSessionCompat$MediaSessionImplApi21.mShuffleMode:I
            //    13: aload_0        
            //    14: getfield        android/support/v4/media/session/MediaSessionCompat$MediaSessionImplApi21.mLock:Ljava/lang/Object;
            //    17: astore_3       
            //    18: aload_3        
            //    19: monitorenter   
            //    20: aload_0        
            //    21: getfield        android/support/v4/media/session/MediaSessionCompat$MediaSessionImplApi21.mExtraControllerCallbacks:Landroid/os/RemoteCallbackList;
            //    24: invokevirtual   android/os/RemoteCallbackList.beginBroadcast:()I
            //    27: iconst_1       
            //    28: isub           
            //    29: istore_2       
            //    30: iload_2        
            //    31: iflt            61
            //    34: aload_0        
            //    35: getfield        android/support/v4/media/session/MediaSessionCompat$MediaSessionImplApi21.mExtraControllerCallbacks:Landroid/os/RemoteCallbackList;
            //    38: iload_2        
            //    39: invokevirtual   android/os/RemoteCallbackList.getBroadcastItem:(I)Landroid/os/IInterface;
            //    42: checkcast       Landroid/support/v4/media/session/IMediaControllerCallback;
            //    45: astore          4
            //    47: aload           4
            //    49: iload_1        
            //    50: invokeinterface android/support/v4/media/session/IMediaControllerCallback.onShuffleModeChanged:(I)V
            //    55: iinc            2, -1
            //    58: goto            30
            //    61: aload_0        
            //    62: getfield        android/support/v4/media/session/MediaSessionCompat$MediaSessionImplApi21.mExtraControllerCallbacks:Landroid/os/RemoteCallbackList;
            //    65: invokevirtual   android/os/RemoteCallbackList.finishBroadcast:()V
            //    68: aload_3        
            //    69: monitorexit    
            //    70: goto            80
            //    73: astore          4
            //    75: aload_3        
            //    76: monitorexit    
            //    77: aload           4
            //    79: athrow         
            //    80: return         
            //    81: astore          4
            //    83: goto            55
            //    Exceptions:
            //  Try           Handler
            //  Start  End    Start  End    Type                        
            //  -----  -----  -----  -----  ----------------------------
            //  20     30     73     80     Any
            //  34     47     73     80     Any
            //  47     55     81     86     Landroid/os/RemoteException;
            //  47     55     73     80     Any
            //  61     70     73     80     Any
            //  75     77     73     80     Any
            // 
            // The error that occurred was:
            // 
            // java.lang.IllegalStateException: Expression is linked from several locations: Label_0055:
            //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
            //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2604)
            //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
            //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
            //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:206)
            //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:662)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
            //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
            //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
            //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
            //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
            //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
            // 
            throw new IllegalStateException("An error occurred while decompiling this method.");
        }
        
        class ExtraSession extends Stub
        {
            final MediaSessionImplApi21 this$0;
            
            ExtraSession(final MediaSessionImplApi21 this$0) {
                this.this$0 = this$0;
            }
            
            public void addQueueItem(final MediaDescriptionCompat mediaDescriptionCompat) {
                throw new AssertionError();
            }
            
            public void addQueueItemAt(final MediaDescriptionCompat mediaDescriptionCompat, final int n) {
                throw new AssertionError();
            }
            
            public void adjustVolume(final int n, final int n2, final String s) {
                throw new AssertionError();
            }
            
            public void fastForward() throws RemoteException {
                throw new AssertionError();
            }
            
            public Bundle getExtras() {
                throw new AssertionError();
            }
            
            public long getFlags() {
                throw new AssertionError();
            }
            
            public PendingIntent getLaunchPendingIntent() {
                throw new AssertionError();
            }
            
            public MediaMetadataCompat getMetadata() {
                throw new AssertionError();
            }
            
            public String getPackageName() {
                throw new AssertionError();
            }
            
            public PlaybackStateCompat getPlaybackState() {
                return MediaSessionCompat.getStateWithUpdatedPosition(this.this$0.mPlaybackState, this.this$0.mMetadata);
            }
            
            public List<QueueItem> getQueue() {
                return null;
            }
            
            public CharSequence getQueueTitle() {
                throw new AssertionError();
            }
            
            public int getRatingType() {
                return this.this$0.mRatingType;
            }
            
            public int getRepeatMode() {
                return this.this$0.mRepeatMode;
            }
            
            public Bundle getSessionInfo() {
                Bundle bundle;
                if (this.this$0.mSessionInfo == null) {
                    bundle = null;
                }
                else {
                    bundle = new Bundle(this.this$0.mSessionInfo);
                }
                return bundle;
            }
            
            public int getShuffleMode() {
                return this.this$0.mShuffleMode;
            }
            
            public String getTag() {
                throw new AssertionError();
            }
            
            public ParcelableVolumeInfo getVolumeAttributes() {
                throw new AssertionError();
            }
            
            public boolean isCaptioningEnabled() {
                return this.this$0.mCaptioningEnabled;
            }
            
            public boolean isShuffleModeEnabledRemoved() {
                return false;
            }
            
            public boolean isTransportControlEnabled() {
                throw new AssertionError();
            }
            
            public void next() throws RemoteException {
                throw new AssertionError();
            }
            
            public void pause() throws RemoteException {
                throw new AssertionError();
            }
            
            public void play() throws RemoteException {
                throw new AssertionError();
            }
            
            public void playFromMediaId(final String s, final Bundle bundle) throws RemoteException {
                throw new AssertionError();
            }
            
            public void playFromSearch(final String s, final Bundle bundle) throws RemoteException {
                throw new AssertionError();
            }
            
            public void playFromUri(final Uri uri, final Bundle bundle) throws RemoteException {
                throw new AssertionError();
            }
            
            public void prepare() throws RemoteException {
                throw new AssertionError();
            }
            
            public void prepareFromMediaId(final String s, final Bundle bundle) throws RemoteException {
                throw new AssertionError();
            }
            
            public void prepareFromSearch(final String s, final Bundle bundle) throws RemoteException {
                throw new AssertionError();
            }
            
            public void prepareFromUri(final Uri uri, final Bundle bundle) throws RemoteException {
                throw new AssertionError();
            }
            
            public void previous() throws RemoteException {
                throw new AssertionError();
            }
            
            public void rate(final RatingCompat ratingCompat) throws RemoteException {
                throw new AssertionError();
            }
            
            public void rateWithExtras(final RatingCompat ratingCompat, final Bundle bundle) throws RemoteException {
                throw new AssertionError();
            }
            
            public void registerCallbackListener(final IMediaControllerCallback mediaControllerCallback) {
                if (this.this$0.mDestroyed) {
                    return;
                }
                final int callingPid = Binder.getCallingPid();
                final int callingUid = Binder.getCallingUid();
                this.this$0.mExtraControllerCallbacks.register((IInterface)mediaControllerCallback, (Object)new MediaSessionManager.RemoteUserInfo("android.media.session.MediaController", callingPid, callingUid));
                synchronized (this.this$0.mLock) {
                    if (this.this$0.mRegistrationCallbackHandler != null) {
                        this.this$0.mRegistrationCallbackHandler.postCallbackRegistered(callingPid, callingUid);
                    }
                }
            }
            
            public void removeQueueItem(final MediaDescriptionCompat mediaDescriptionCompat) {
                throw new AssertionError();
            }
            
            public void removeQueueItemAt(final int n) {
                throw new AssertionError();
            }
            
            public void rewind() throws RemoteException {
                throw new AssertionError();
            }
            
            public void seekTo(final long n) throws RemoteException {
                throw new AssertionError();
            }
            
            public void sendCommand(final String s, final Bundle bundle, final ResultReceiverWrapper resultReceiverWrapper) {
                throw new AssertionError();
            }
            
            public void sendCustomAction(final String s, final Bundle bundle) throws RemoteException {
                throw new AssertionError();
            }
            
            public boolean sendMediaButton(final KeyEvent keyEvent) {
                throw new AssertionError();
            }
            
            public void setCaptioningEnabled(final boolean b) throws RemoteException {
                throw new AssertionError();
            }
            
            public void setPlaybackSpeed(final float n) throws RemoteException {
                throw new AssertionError();
            }
            
            public void setRepeatMode(final int n) throws RemoteException {
                throw new AssertionError();
            }
            
            public void setShuffleMode(final int n) throws RemoteException {
                throw new AssertionError();
            }
            
            public void setShuffleModeEnabledRemoved(final boolean b) throws RemoteException {
            }
            
            public void setVolumeTo(final int n, final int n2, final String s) {
                throw new AssertionError();
            }
            
            public void skipToQueueItem(final long n) {
                throw new AssertionError();
            }
            
            public void stop() throws RemoteException {
                throw new AssertionError();
            }
            
            public void unregisterCallbackListener(final IMediaControllerCallback mediaControllerCallback) {
                this.this$0.mExtraControllerCallbacks.unregister((IInterface)mediaControllerCallback);
                final int callingPid = Binder.getCallingPid();
                final int callingUid = Binder.getCallingUid();
                synchronized (this.this$0.mLock) {
                    if (this.this$0.mRegistrationCallbackHandler != null) {
                        this.this$0.mRegistrationCallbackHandler.postCallbackUnregistered(callingPid, callingUid);
                    }
                }
            }
        }
    }
    
    static class MediaSessionImplApi22 extends MediaSessionImplApi21
    {
        MediaSessionImplApi22(final Context context, final String s, final VersionedParcelable versionedParcelable, final Bundle bundle) {
            super(context, s, versionedParcelable, bundle);
        }
        
        MediaSessionImplApi22(final Object o) {
            super(o);
        }
        
        @Override
        public void setRatingType(final int ratingType) {
            this.mSessionFwk.setRatingType(ratingType);
        }
    }
    
    static class MediaSessionImplApi28 extends MediaSessionImplApi22
    {
        MediaSessionImplApi28(final Context context, final String s, final VersionedParcelable versionedParcelable, final Bundle bundle) {
            super(context, s, versionedParcelable, bundle);
        }
        
        MediaSessionImplApi28(final Object o) {
            super(o);
        }
        
        @Override
        public final MediaSessionManager.RemoteUserInfo getCurrentControllerInfo() {
            return new MediaSessionManager.RemoteUserInfo(this.mSessionFwk.getCurrentControllerInfo());
        }
        
        @Override
        public void setCurrentControllerInfo(final MediaSessionManager.RemoteUserInfo remoteUserInfo) {
        }
    }
    
    static class MediaSessionImplApi29 extends MediaSessionImplApi28
    {
        MediaSessionImplApi29(final Context context, final String s, final VersionedParcelable versionedParcelable, final Bundle bundle) {
            super(context, s, versionedParcelable, bundle);
        }
        
        MediaSessionImplApi29(final Object o) {
            super(o);
            this.mSessionInfo = ((MediaSession)o).getController().getSessionInfo();
        }
        
        @Override
        public MediaSession createFwkMediaSession(final Context context, final String s, final Bundle bundle) {
            return new MediaSession(context, s, bundle);
        }
    }
    
    public interface OnActiveChangeListener
    {
        void onActiveChanged();
    }
    
    public static final class QueueItem implements Parcelable
    {
        public static final Parcelable$Creator<QueueItem> CREATOR;
        public static final int UNKNOWN_ID = -1;
        private final MediaDescriptionCompat mDescription;
        private final long mId;
        private MediaSession$QueueItem mItemFwk;
        
        static {
            CREATOR = (Parcelable$Creator)new Parcelable$Creator<QueueItem>() {
                public QueueItem createFromParcel(final Parcel parcel) {
                    return new QueueItem(parcel);
                }
                
                public QueueItem[] newArray(final int n) {
                    return new QueueItem[n];
                }
            };
        }
        
        private QueueItem(final MediaSession$QueueItem mItemFwk, final MediaDescriptionCompat mDescription, final long mId) {
            if (mDescription == null) {
                throw new IllegalArgumentException("Description cannot be null");
            }
            if (mId != -1L) {
                this.mDescription = mDescription;
                this.mId = mId;
                this.mItemFwk = mItemFwk;
                return;
            }
            throw new IllegalArgumentException("Id cannot be QueueItem.UNKNOWN_ID");
        }
        
        QueueItem(final Parcel parcel) {
            this.mDescription = (MediaDescriptionCompat)MediaDescriptionCompat.CREATOR.createFromParcel(parcel);
            this.mId = parcel.readLong();
        }
        
        public QueueItem(final MediaDescriptionCompat mediaDescriptionCompat, final long n) {
            this(null, mediaDescriptionCompat, n);
        }
        
        public static QueueItem fromQueueItem(final Object o) {
            if (o != null && Build$VERSION.SDK_INT >= 21) {
                final MediaSession$QueueItem mediaSession$QueueItem = (MediaSession$QueueItem)o;
                return new QueueItem(mediaSession$QueueItem, MediaDescriptionCompat.fromMediaDescription(Api21Impl.getDescription(mediaSession$QueueItem)), Api21Impl.getQueueId(mediaSession$QueueItem));
            }
            return null;
        }
        
        public static List<QueueItem> fromQueueItemList(final List<?> list) {
            if (list != null && Build$VERSION.SDK_INT >= 21) {
                final ArrayList list2 = new ArrayList(list.size());
                final Iterator iterator = list.iterator();
                while (iterator.hasNext()) {
                    list2.add(fromQueueItem(iterator.next()));
                }
                return list2;
            }
            return null;
        }
        
        public int describeContents() {
            return 0;
        }
        
        public MediaDescriptionCompat getDescription() {
            return this.mDescription;
        }
        
        public long getQueueId() {
            return this.mId;
        }
        
        public Object getQueueItem() {
            if (this.mItemFwk == null && Build$VERSION.SDK_INT >= 21) {
                return this.mItemFwk = Api21Impl.createQueueItem((MediaDescription)this.mDescription.getMediaDescription(), this.mId);
            }
            return this.mItemFwk;
        }
        
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder();
            sb.append("MediaSession.QueueItem {Description=");
            sb.append(this.mDescription);
            sb.append(", Id=");
            sb.append(this.mId);
            sb.append(" }");
            return sb.toString();
        }
        
        public void writeToParcel(final Parcel parcel, final int n) {
            this.mDescription.writeToParcel(parcel, n);
            parcel.writeLong(this.mId);
        }
        
        private static class Api21Impl
        {
            static MediaSession$QueueItem createQueueItem(final MediaDescription mediaDescription, final long n) {
                return new MediaSession$QueueItem(mediaDescription, n);
            }
            
            static MediaDescription getDescription(final MediaSession$QueueItem mediaSession$QueueItem) {
                return mediaSession$QueueItem.getDescription();
            }
            
            static long getQueueId(final MediaSession$QueueItem mediaSession$QueueItem) {
                return mediaSession$QueueItem.getQueueId();
            }
        }
    }
    
    public interface RegistrationCallback
    {
        void onCallbackRegistered(final int p0, final int p1);
        
        void onCallbackUnregistered(final int p0, final int p1);
    }
    
    static final class RegistrationCallbackHandler extends Handler
    {
        private static final int MSG_CALLBACK_REGISTERED = 1001;
        private static final int MSG_CALLBACK_UNREGISTERED = 1002;
        private final RegistrationCallback mCallback;
        
        RegistrationCallbackHandler(final Looper looper, final RegistrationCallback mCallback) {
            super(looper);
            this.mCallback = mCallback;
        }
        
        public void handleMessage(final Message message) {
            super.handleMessage(message);
            final int what = message.what;
            if (what != 1001) {
                if (what == 1002) {
                    this.mCallback.onCallbackUnregistered(message.arg1, message.arg2);
                }
            }
            else {
                this.mCallback.onCallbackRegistered(message.arg1, message.arg2);
            }
        }
        
        public void postCallbackRegistered(final int n, final int n2) {
            this.obtainMessage(1001, n, n2).sendToTarget();
        }
        
        public void postCallbackUnregistered(final int n, final int n2) {
            this.obtainMessage(1002, n, n2).sendToTarget();
        }
    }
    
    static final class ResultReceiverWrapper implements Parcelable
    {
        public static final Parcelable$Creator<ResultReceiverWrapper> CREATOR;
        ResultReceiver mResultReceiver;
        
        static {
            CREATOR = (Parcelable$Creator)new Parcelable$Creator<ResultReceiverWrapper>() {
                public ResultReceiverWrapper createFromParcel(final Parcel parcel) {
                    return new ResultReceiverWrapper(parcel);
                }
                
                public ResultReceiverWrapper[] newArray(final int n) {
                    return new ResultReceiverWrapper[n];
                }
            };
        }
        
        ResultReceiverWrapper(final Parcel parcel) {
            this.mResultReceiver = (ResultReceiver)ResultReceiver.CREATOR.createFromParcel(parcel);
        }
        
        public ResultReceiverWrapper(final ResultReceiver mResultReceiver) {
            this.mResultReceiver = mResultReceiver;
        }
        
        public int describeContents() {
            return 0;
        }
        
        public void writeToParcel(final Parcel parcel, final int n) {
            this.mResultReceiver.writeToParcel(parcel, n);
        }
    }
    
    public static final class Token implements Parcelable
    {
        public static final Parcelable$Creator<Token> CREATOR;
        private IMediaSession mExtraBinder;
        private final Object mInner;
        private final Object mLock;
        private VersionedParcelable mSession2Token;
        
        static {
            CREATOR = (Parcelable$Creator)new Parcelable$Creator<Token>() {
                public Token createFromParcel(final Parcel parcel) {
                    Object o;
                    if (Build$VERSION.SDK_INT >= 21) {
                        o = parcel.readParcelable((ClassLoader)null);
                    }
                    else {
                        o = parcel.readStrongBinder();
                    }
                    return new Token(o);
                }
                
                public Token[] newArray(final int n) {
                    return new Token[n];
                }
            };
        }
        
        Token(final Object o) {
            this(o, null, null);
        }
        
        Token(final Object o, final IMediaSession mediaSession) {
            this(o, mediaSession, null);
        }
        
        Token(final Object mInner, final IMediaSession mExtraBinder, final VersionedParcelable mSession2Token) {
            this.mLock = new Object();
            this.mInner = mInner;
            this.mExtraBinder = mExtraBinder;
            this.mSession2Token = mSession2Token;
        }
        
        public static Token fromBundle(final Bundle bundle) {
            final Token token = null;
            if (bundle == null) {
                return null;
            }
            bundle.setClassLoader(Token.class.getClassLoader());
            final IMediaSession interface1 = IMediaSession.Stub.asInterface(BundleCompat.getBinder(bundle, "android.support.v4.media.session.EXTRA_BINDER"));
            final VersionedParcelable versionedParcelable = ParcelUtils.getVersionedParcelable(bundle, "android.support.v4.media.session.SESSION_TOKEN2");
            final Token token2 = (Token)bundle.getParcelable("android.support.v4.media.session.TOKEN");
            Token token3;
            if (token2 == null) {
                token3 = token;
            }
            else {
                token3 = new Token(token2.mInner, interface1, versionedParcelable);
            }
            return token3;
        }
        
        public static Token fromToken(final Object o) {
            return fromToken(o, null);
        }
        
        public static Token fromToken(final Object o, final IMediaSession mediaSession) {
            if (o == null || Build$VERSION.SDK_INT < 21) {
                return null;
            }
            if (o instanceof MediaSession$Token) {
                return new Token(o, mediaSession);
            }
            throw new IllegalArgumentException("token is not a valid MediaSession.Token object");
        }
        
        public int describeContents() {
            return 0;
        }
        
        @Override
        public boolean equals(Object mInner) {
            boolean b = true;
            if (this == mInner) {
                return true;
            }
            if (!(mInner instanceof Token)) {
                return false;
            }
            final Token token = (Token)mInner;
            mInner = this.mInner;
            if (mInner == null) {
                if (token.mInner != null) {
                    b = false;
                }
                return b;
            }
            final Object mInner2 = token.mInner;
            return mInner2 != null && mInner.equals(mInner2);
        }
        
        public IMediaSession getExtraBinder() {
            synchronized (this.mLock) {
                return this.mExtraBinder;
            }
        }
        
        public VersionedParcelable getSession2Token() {
            synchronized (this.mLock) {
                return this.mSession2Token;
            }
        }
        
        public Object getToken() {
            return this.mInner;
        }
        
        @Override
        public int hashCode() {
            final Object mInner = this.mInner;
            if (mInner == null) {
                return 0;
            }
            return mInner.hashCode();
        }
        
        public void setExtraBinder(final IMediaSession mExtraBinder) {
            synchronized (this.mLock) {
                this.mExtraBinder = mExtraBinder;
            }
        }
        
        public void setSession2Token(final VersionedParcelable mSession2Token) {
            synchronized (this.mLock) {
                this.mSession2Token = mSession2Token;
            }
        }
        
        public Bundle toBundle() {
            final Bundle bundle = new Bundle();
            bundle.putParcelable("android.support.v4.media.session.TOKEN", (Parcelable)this);
            synchronized (this.mLock) {
                final IMediaSession mExtraBinder = this.mExtraBinder;
                if (mExtraBinder != null) {
                    BundleCompat.putBinder(bundle, "android.support.v4.media.session.EXTRA_BINDER", mExtraBinder.asBinder());
                }
                final VersionedParcelable mSession2Token = this.mSession2Token;
                if (mSession2Token != null) {
                    ParcelUtils.putVersionedParcelable(bundle, "android.support.v4.media.session.SESSION_TOKEN2", mSession2Token);
                }
                return bundle;
            }
        }
        
        public void writeToParcel(final Parcel parcel, final int n) {
            if (Build$VERSION.SDK_INT >= 21) {
                parcel.writeParcelable((Parcelable)this.mInner, n);
            }
            else {
                parcel.writeStrongBinder((IBinder)this.mInner);
            }
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package android.support.v4.media;

import android.media.MediaDescription$Builder;
import android.support.v4.media.session.MediaSessionCompat;
import android.text.TextUtils;
import android.os.Build$VERSION;
import android.os.Parcel;
import android.net.Uri;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.media.MediaDescription;
import android.os.Parcelable$Creator;
import android.os.Parcelable;

public final class MediaDescriptionCompat implements Parcelable
{
    public static final long BT_FOLDER_TYPE_ALBUMS = 2L;
    public static final long BT_FOLDER_TYPE_ARTISTS = 3L;
    public static final long BT_FOLDER_TYPE_GENRES = 4L;
    public static final long BT_FOLDER_TYPE_MIXED = 0L;
    public static final long BT_FOLDER_TYPE_PLAYLISTS = 5L;
    public static final long BT_FOLDER_TYPE_TITLES = 1L;
    public static final long BT_FOLDER_TYPE_YEARS = 6L;
    public static final Parcelable$Creator<MediaDescriptionCompat> CREATOR;
    public static final String DESCRIPTION_KEY_MEDIA_URI = "android.support.v4.media.description.MEDIA_URI";
    public static final String DESCRIPTION_KEY_NULL_BUNDLE_FLAG = "android.support.v4.media.description.NULL_BUNDLE_FLAG";
    public static final String EXTRA_BT_FOLDER_TYPE = "android.media.extra.BT_FOLDER_TYPE";
    public static final String EXTRA_DOWNLOAD_STATUS = "android.media.extra.DOWNLOAD_STATUS";
    public static final long STATUS_DOWNLOADED = 2L;
    public static final long STATUS_DOWNLOADING = 1L;
    public static final long STATUS_NOT_DOWNLOADED = 0L;
    private static final String TAG = "MediaDescriptionCompat";
    private final CharSequence mDescription;
    private MediaDescription mDescriptionFwk;
    private final Bundle mExtras;
    private final Bitmap mIcon;
    private final Uri mIconUri;
    private final String mMediaId;
    private final Uri mMediaUri;
    private final CharSequence mSubtitle;
    private final CharSequence mTitle;
    
    static {
        CREATOR = (Parcelable$Creator)new Parcelable$Creator<MediaDescriptionCompat>() {
            public MediaDescriptionCompat createFromParcel(final Parcel parcel) {
                if (Build$VERSION.SDK_INT < 21) {
                    return new MediaDescriptionCompat(parcel);
                }
                return MediaDescriptionCompat.fromMediaDescription(MediaDescription.CREATOR.createFromParcel(parcel));
            }
            
            public MediaDescriptionCompat[] newArray(final int n) {
                return new MediaDescriptionCompat[n];
            }
        };
    }
    
    MediaDescriptionCompat(final Parcel parcel) {
        this.mMediaId = parcel.readString();
        this.mTitle = (CharSequence)TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(parcel);
        this.mSubtitle = (CharSequence)TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(parcel);
        this.mDescription = (CharSequence)TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(parcel);
        final ClassLoader classLoader = this.getClass().getClassLoader();
        this.mIcon = (Bitmap)parcel.readParcelable(classLoader);
        this.mIconUri = (Uri)parcel.readParcelable(classLoader);
        this.mExtras = parcel.readBundle(classLoader);
        this.mMediaUri = (Uri)parcel.readParcelable(classLoader);
    }
    
    MediaDescriptionCompat(final String mMediaId, final CharSequence mTitle, final CharSequence mSubtitle, final CharSequence mDescription, final Bitmap mIcon, final Uri mIconUri, final Bundle mExtras, final Uri mMediaUri) {
        this.mMediaId = mMediaId;
        this.mTitle = mTitle;
        this.mSubtitle = mSubtitle;
        this.mDescription = mDescription;
        this.mIcon = mIcon;
        this.mIconUri = mIconUri;
        this.mExtras = mExtras;
        this.mMediaUri = mMediaUri;
    }
    
    public static MediaDescriptionCompat fromMediaDescription(final Object o) {
        final MediaDescriptionCompat mediaDescriptionCompat = null;
        final Bundle bundle = null;
        MediaDescriptionCompat build = mediaDescriptionCompat;
        if (o != null) {
            build = mediaDescriptionCompat;
            if (Build$VERSION.SDK_INT >= 21) {
                final Builder builder = new Builder();
                final MediaDescription mDescriptionFwk = (MediaDescription)o;
                builder.setMediaId(Api21Impl.getMediaId(mDescriptionFwk));
                builder.setTitle(Api21Impl.getTitle(mDescriptionFwk));
                builder.setSubtitle(Api21Impl.getSubtitle(mDescriptionFwk));
                builder.setDescription(Api21Impl.getDescription(mDescriptionFwk));
                builder.setIconBitmap(Api21Impl.getIconBitmap(mDescriptionFwk));
                builder.setIconUri(Api21Impl.getIconUri(mDescriptionFwk));
                final Bundle extras = Api21Impl.getExtras(mDescriptionFwk);
                Bundle unparcelWithClassLoader;
                if ((unparcelWithClassLoader = extras) != null) {
                    unparcelWithClassLoader = MediaSessionCompat.unparcelWithClassLoader(extras);
                }
                Uri mediaUri;
                if (unparcelWithClassLoader != null) {
                    mediaUri = (Uri)unparcelWithClassLoader.getParcelable("android.support.v4.media.description.MEDIA_URI");
                }
                else {
                    mediaUri = null;
                }
                if (mediaUri != null) {
                    if (unparcelWithClassLoader.containsKey("android.support.v4.media.description.NULL_BUNDLE_FLAG") && unparcelWithClassLoader.size() == 2) {
                        unparcelWithClassLoader = bundle;
                    }
                    else {
                        unparcelWithClassLoader.remove("android.support.v4.media.description.MEDIA_URI");
                        unparcelWithClassLoader.remove("android.support.v4.media.description.NULL_BUNDLE_FLAG");
                    }
                }
                builder.setExtras(unparcelWithClassLoader);
                if (mediaUri != null) {
                    builder.setMediaUri(mediaUri);
                }
                else if (Build$VERSION.SDK_INT >= 23) {
                    builder.setMediaUri(Api23Impl.getMediaUri(mDescriptionFwk));
                }
                build = builder.build();
                build.mDescriptionFwk = mDescriptionFwk;
            }
        }
        return build;
    }
    
    public int describeContents() {
        return 0;
    }
    
    public CharSequence getDescription() {
        return this.mDescription;
    }
    
    public Bundle getExtras() {
        return this.mExtras;
    }
    
    public Bitmap getIconBitmap() {
        return this.mIcon;
    }
    
    public Uri getIconUri() {
        return this.mIconUri;
    }
    
    public Object getMediaDescription() {
        if (this.mDescriptionFwk == null && Build$VERSION.SDK_INT >= 21) {
            final MediaDescription$Builder builder = Api21Impl.createBuilder();
            Api21Impl.setMediaId(builder, this.mMediaId);
            Api21Impl.setTitle(builder, this.mTitle);
            Api21Impl.setSubtitle(builder, this.mSubtitle);
            Api21Impl.setDescription(builder, this.mDescription);
            Api21Impl.setIconBitmap(builder, this.mIcon);
            Api21Impl.setIconUri(builder, this.mIconUri);
            if (Build$VERSION.SDK_INT < 23 && this.mMediaUri != null) {
                Bundle bundle;
                if (this.mExtras == null) {
                    bundle = new Bundle();
                    bundle.putBoolean("android.support.v4.media.description.NULL_BUNDLE_FLAG", true);
                }
                else {
                    bundle = new Bundle(this.mExtras);
                }
                bundle.putParcelable("android.support.v4.media.description.MEDIA_URI", (Parcelable)this.mMediaUri);
                Api21Impl.setExtras(builder, bundle);
            }
            else {
                Api21Impl.setExtras(builder, this.mExtras);
            }
            if (Build$VERSION.SDK_INT >= 23) {
                Api23Impl.setMediaUri(builder, this.mMediaUri);
            }
            return this.mDescriptionFwk = Api21Impl.build(builder);
        }
        return this.mDescriptionFwk;
    }
    
    public String getMediaId() {
        return this.mMediaId;
    }
    
    public Uri getMediaUri() {
        return this.mMediaUri;
    }
    
    public CharSequence getSubtitle() {
        return this.mSubtitle;
    }
    
    public CharSequence getTitle() {
        return this.mTitle;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append((Object)this.mTitle);
        sb.append(", ");
        sb.append((Object)this.mSubtitle);
        sb.append(", ");
        sb.append((Object)this.mDescription);
        return sb.toString();
    }
    
    public void writeToParcel(final Parcel parcel, final int n) {
        if (Build$VERSION.SDK_INT < 21) {
            parcel.writeString(this.mMediaId);
            TextUtils.writeToParcel(this.mTitle, parcel, n);
            TextUtils.writeToParcel(this.mSubtitle, parcel, n);
            TextUtils.writeToParcel(this.mDescription, parcel, n);
            parcel.writeParcelable((Parcelable)this.mIcon, n);
            parcel.writeParcelable((Parcelable)this.mIconUri, n);
            parcel.writeBundle(this.mExtras);
            parcel.writeParcelable((Parcelable)this.mMediaUri, n);
        }
        else {
            ((MediaDescription)this.getMediaDescription()).writeToParcel(parcel, n);
        }
    }
    
    private static class Api21Impl
    {
        static MediaDescription build(final MediaDescription$Builder mediaDescription$Builder) {
            return mediaDescription$Builder.build();
        }
        
        static MediaDescription$Builder createBuilder() {
            return new MediaDescription$Builder();
        }
        
        static CharSequence getDescription(final MediaDescription mediaDescription) {
            return mediaDescription.getDescription();
        }
        
        static Bundle getExtras(final MediaDescription mediaDescription) {
            return mediaDescription.getExtras();
        }
        
        static Bitmap getIconBitmap(final MediaDescription mediaDescription) {
            return mediaDescription.getIconBitmap();
        }
        
        static Uri getIconUri(final MediaDescription mediaDescription) {
            return mediaDescription.getIconUri();
        }
        
        static String getMediaId(final MediaDescription mediaDescription) {
            return mediaDescription.getMediaId();
        }
        
        static CharSequence getSubtitle(final MediaDescription mediaDescription) {
            return mediaDescription.getSubtitle();
        }
        
        static CharSequence getTitle(final MediaDescription mediaDescription) {
            return mediaDescription.getTitle();
        }
        
        static void setDescription(final MediaDescription$Builder mediaDescription$Builder, final CharSequence description) {
            mediaDescription$Builder.setDescription(description);
        }
        
        static void setExtras(final MediaDescription$Builder mediaDescription$Builder, final Bundle extras) {
            mediaDescription$Builder.setExtras(extras);
        }
        
        static void setIconBitmap(final MediaDescription$Builder mediaDescription$Builder, final Bitmap iconBitmap) {
            mediaDescription$Builder.setIconBitmap(iconBitmap);
        }
        
        static void setIconUri(final MediaDescription$Builder mediaDescription$Builder, final Uri iconUri) {
            mediaDescription$Builder.setIconUri(iconUri);
        }
        
        static void setMediaId(final MediaDescription$Builder mediaDescription$Builder, final String mediaId) {
            mediaDescription$Builder.setMediaId(mediaId);
        }
        
        static void setSubtitle(final MediaDescription$Builder mediaDescription$Builder, final CharSequence subtitle) {
            mediaDescription$Builder.setSubtitle(subtitle);
        }
        
        static void setTitle(final MediaDescription$Builder mediaDescription$Builder, final CharSequence title) {
            mediaDescription$Builder.setTitle(title);
        }
    }
    
    private static class Api23Impl
    {
        static Uri getMediaUri(final MediaDescription mediaDescription) {
            return mediaDescription.getMediaUri();
        }
        
        static void setMediaUri(final MediaDescription$Builder mediaDescription$Builder, final Uri mediaUri) {
            mediaDescription$Builder.setMediaUri(mediaUri);
        }
    }
    
    public static final class Builder
    {
        private CharSequence mDescription;
        private Bundle mExtras;
        private Bitmap mIcon;
        private Uri mIconUri;
        private String mMediaId;
        private Uri mMediaUri;
        private CharSequence mSubtitle;
        private CharSequence mTitle;
        
        public MediaDescriptionCompat build() {
            return new MediaDescriptionCompat(this.mMediaId, this.mTitle, this.mSubtitle, this.mDescription, this.mIcon, this.mIconUri, this.mExtras, this.mMediaUri);
        }
        
        public Builder setDescription(final CharSequence mDescription) {
            this.mDescription = mDescription;
            return this;
        }
        
        public Builder setExtras(final Bundle mExtras) {
            this.mExtras = mExtras;
            return this;
        }
        
        public Builder setIconBitmap(final Bitmap mIcon) {
            this.mIcon = mIcon;
            return this;
        }
        
        public Builder setIconUri(final Uri mIconUri) {
            this.mIconUri = mIconUri;
            return this;
        }
        
        public Builder setMediaId(final String mMediaId) {
            this.mMediaId = mMediaId;
            return this;
        }
        
        public Builder setMediaUri(final Uri mMediaUri) {
            this.mMediaUri = mMediaUri;
            return this;
        }
        
        public Builder setSubtitle(final CharSequence mSubtitle) {
            this.mSubtitle = mSubtitle;
            return this;
        }
        
        public Builder setTitle(final CharSequence mTitle) {
            this.mTitle = mTitle;
            return this;
        }
    }
}

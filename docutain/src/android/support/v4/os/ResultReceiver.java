// 
// Decompiled by Procyon v0.6.0
// 

package android.support.v4.os;

import android.os.RemoteException;
import android.os.Bundle;
import android.os.Parcel;
import androidx.core.os.IResultReceiver;
import android.os.Handler;
import android.os.Parcelable$Creator;
import android.os.Parcelable;

public class ResultReceiver implements Parcelable
{
    public static final Parcelable$Creator<ResultReceiver> CREATOR;
    final Handler mHandler;
    final boolean mLocal;
    IResultReceiver mReceiver;
    
    static {
        CREATOR = (Parcelable$Creator)new Parcelable$Creator<ResultReceiver>() {
            public ResultReceiver createFromParcel(final Parcel parcel) {
                return new ResultReceiver(parcel);
            }
            
            public ResultReceiver[] newArray(final int n) {
                return new ResultReceiver[n];
            }
        };
    }
    
    public ResultReceiver(final Handler mHandler) {
        this.mLocal = true;
        this.mHandler = mHandler;
    }
    
    ResultReceiver(final Parcel parcel) {
        this.mLocal = false;
        this.mHandler = null;
        this.mReceiver = IResultReceiver.Stub.asInterface(parcel.readStrongBinder());
    }
    
    public int describeContents() {
        return 0;
    }
    
    protected void onReceiveResult(final int n, final Bundle bundle) {
    }
    
    public void send(final int n, final Bundle bundle) {
        if (this.mLocal) {
            final Handler mHandler = this.mHandler;
            if (mHandler != null) {
                mHandler.post((Runnable)new MyRunnable(n, bundle));
            }
            else {
                this.onReceiveResult(n, bundle);
            }
            return;
        }
        final IResultReceiver mReceiver = this.mReceiver;
        if (mReceiver == null) {
            return;
        }
        try {
            mReceiver.send(n, bundle);
        }
        catch (final RemoteException ex) {}
    }
    
    public void writeToParcel(final Parcel parcel, final int n) {
        synchronized (this) {
            if (this.mReceiver == null) {
                this.mReceiver = new MyResultReceiver();
            }
            parcel.writeStrongBinder(this.mReceiver.asBinder());
        }
    }
    
    class MyResultReceiver extends Stub
    {
        final ResultReceiver this$0;
        
        MyResultReceiver(final ResultReceiver this$0) {
            this.this$0 = this$0;
        }
        
        public void send(final int n, final Bundle bundle) {
            if (this.this$0.mHandler != null) {
                this.this$0.mHandler.post((Runnable)this.this$0.new MyRunnable(n, bundle));
            }
            else {
                this.this$0.onReceiveResult(n, bundle);
            }
        }
    }
    
    class MyRunnable implements Runnable
    {
        final int mResultCode;
        final Bundle mResultData;
        final ResultReceiver this$0;
        
        MyRunnable(final ResultReceiver this$0, final int mResultCode, final Bundle mResultData) {
            this.this$0 = this$0;
            this.mResultCode = mResultCode;
            this.mResultData = mResultData;
        }
        
        @Override
        public void run() {
            this.this$0.onReceiveResult(this.mResultCode, this.mResultData);
        }
    }
}

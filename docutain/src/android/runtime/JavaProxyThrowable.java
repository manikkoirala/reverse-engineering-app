// 
// Decompiled by Procyon v0.6.0
// 

package android.runtime;

import mono.android.TypeManager;
import mono.android.Runtime;
import java.util.ArrayList;
import mono.android.IGCUserPeer;

public class JavaProxyThrowable extends Error implements IGCUserPeer
{
    public static final String __md_methods = "";
    private ArrayList refList;
    
    static {
        Runtime.register("Android.Runtime.JavaProxyThrowable, Mono.Android", (Class)JavaProxyThrowable.class, "");
    }
    
    public JavaProxyThrowable() {
        if (this.getClass() == JavaProxyThrowable.class) {
            TypeManager.Activate("Android.Runtime.JavaProxyThrowable, Mono.Android", "", (Object)this, new Object[0]);
        }
    }
    
    public JavaProxyThrowable(final String message) {
        super(message);
        if (this.getClass() == JavaProxyThrowable.class) {
            TypeManager.Activate("Android.Runtime.JavaProxyThrowable, Mono.Android", "System.String, mscorlib", (Object)this, new Object[] { message });
        }
    }
    
    public JavaProxyThrowable(final String message, final Throwable cause) {
        super(message, cause);
        if (this.getClass() == JavaProxyThrowable.class) {
            TypeManager.Activate("Android.Runtime.JavaProxyThrowable, Mono.Android", "System.String, mscorlib:Java.Lang.Throwable, Mono.Android", (Object)this, new Object[] { message, cause });
        }
    }
    
    public JavaProxyThrowable(final String message, final Throwable cause, final boolean b, final boolean b2) {
        super(message, cause, b, b2);
        if (this.getClass() == JavaProxyThrowable.class) {
            TypeManager.Activate("Android.Runtime.JavaProxyThrowable, Mono.Android", "System.String, mscorlib:Java.Lang.Throwable, Mono.Android:System.Boolean, mscorlib:System.Boolean, mscorlib", (Object)this, new Object[] { message, cause, b, b2 });
        }
    }
    
    public JavaProxyThrowable(final Throwable cause) {
        super(cause);
        if (this.getClass() == JavaProxyThrowable.class) {
            TypeManager.Activate("Android.Runtime.JavaProxyThrowable, Mono.Android", "Java.Lang.Throwable, Mono.Android", (Object)this, new Object[] { cause });
        }
    }
    
    public void monodroidAddReference(final Object e) {
        if (this.refList == null) {
            this.refList = new ArrayList();
        }
        this.refList.add(e);
    }
    
    public void monodroidClearReferences() {
        final ArrayList refList = this.refList;
        if (refList != null) {
            refList.clear();
        }
    }
}

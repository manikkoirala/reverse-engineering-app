// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide.gifdecoder;

import java.util.Iterator;
import java.nio.ByteOrder;
import java.io.IOException;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import android.util.Log;
import java.util.Arrays;
import java.nio.ByteBuffer;
import android.graphics.Bitmap;
import android.graphics.Bitmap$Config;

public class StandardGifDecoder implements GifDecoder
{
    private static final int BYTES_PER_INTEGER = 4;
    private static final int COLOR_TRANSPARENT_BLACK = 0;
    private static final int INITIAL_FRAME_POINTER = -1;
    private static final int MASK_INT_LOWEST_BYTE = 255;
    private static final int MAX_STACK_SIZE = 4096;
    private static final int NULL_CODE = -1;
    private static final String TAG = "StandardGifDecoder";
    private int[] act;
    private Bitmap$Config bitmapConfig;
    private final BitmapProvider bitmapProvider;
    private byte[] block;
    private int downsampledHeight;
    private int downsampledWidth;
    private int framePointer;
    private GifHeader header;
    private Boolean isFirstFrameTransparent;
    private byte[] mainPixels;
    private int[] mainScratch;
    private GifHeaderParser parser;
    private final int[] pct;
    private byte[] pixelStack;
    private short[] prefix;
    private Bitmap previousImage;
    private ByteBuffer rawData;
    private int sampleSize;
    private boolean savePrevious;
    private int status;
    private byte[] suffix;
    
    public StandardGifDecoder(final BitmapProvider bitmapProvider) {
        this.pct = new int[256];
        this.bitmapConfig = Bitmap$Config.ARGB_8888;
        this.bitmapProvider = bitmapProvider;
        this.header = new GifHeader();
    }
    
    public StandardGifDecoder(final BitmapProvider bitmapProvider, final GifHeader gifHeader, final ByteBuffer byteBuffer) {
        this(bitmapProvider, gifHeader, byteBuffer, 1);
    }
    
    public StandardGifDecoder(final BitmapProvider bitmapProvider, final GifHeader gifHeader, final ByteBuffer byteBuffer, final int n) {
        this(bitmapProvider);
        this.setData(gifHeader, byteBuffer, n);
    }
    
    private int averageColorsNear(int i, final int n, int n2) {
        int j = i;
        int n3 = 0;
        int n4 = 0;
        int n5 = 0;
        int n6 = 0;
        int n7 = 0;
        while (j < this.sampleSize + i) {
            final byte[] mainPixels = this.mainPixels;
            if (j >= mainPixels.length || j >= n) {
                break;
            }
            final int n8 = this.act[mainPixels[j] & 0xFF];
            int n9 = n3;
            int n10 = n4;
            int n11 = n5;
            int n12 = n6;
            int n13 = n7;
            if (n8 != 0) {
                n9 = n3 + (n8 >> 24 & 0xFF);
                n10 = n4 + (n8 >> 16 & 0xFF);
                n11 = n5 + (n8 >> 8 & 0xFF);
                n12 = n6 + (n8 & 0xFF);
                n13 = n7 + 1;
            }
            ++j;
            n3 = n9;
            n4 = n10;
            n5 = n11;
            n6 = n12;
            n7 = n13;
        }
        final int n14 = i += n2;
        int n15 = n3;
        while (i < this.sampleSize + n14) {
            final byte[] mainPixels2 = this.mainPixels;
            if (i >= mainPixels2.length || i >= n) {
                break;
            }
            n2 = mainPixels2[i];
            final int n16 = this.act[n2 & 0xFF];
            int n17 = n15;
            int n18 = n4;
            int n19 = n5;
            int n20 = n6;
            n2 = n7;
            if (n16 != 0) {
                n17 = n15 + (n16 >> 24 & 0xFF);
                n18 = n4 + (n16 >> 16 & 0xFF);
                n19 = n5 + (n16 >> 8 & 0xFF);
                n20 = n6 + (n16 & 0xFF);
                n2 = n7 + 1;
            }
            ++i;
            n15 = n17;
            n4 = n18;
            n5 = n19;
            n6 = n20;
            n7 = n2;
        }
        if (n7 == 0) {
            return 0;
        }
        return n15 / n7 << 24 | n4 / n7 << 16 | n5 / n7 << 8 | n6 / n7;
    }
    
    private void copyCopyIntoScratchRobust(final GifFrame gifFrame) {
        final int[] mainScratch = this.mainScratch;
        final int n = gifFrame.ih / this.sampleSize;
        int n2 = gifFrame.iy / this.sampleSize;
        int n3 = gifFrame.iw / this.sampleSize;
        final int ix = gifFrame.ix;
        final int sampleSize = this.sampleSize;
        final int n4 = ix / sampleSize;
        final int framePointer = this.framePointer;
        final Boolean value = true;
        final boolean b = framePointer == 0;
        final int downsampledWidth = this.downsampledWidth;
        final int downsampledHeight = this.downsampledHeight;
        final byte[] mainPixels = this.mainPixels;
        final int[] act = this.act;
        Boolean isFirstFrameTransparent = this.isFirstFrameTransparent;
        int n5 = 8;
        int i = 0;
        int n6 = 0;
        int n7 = 1;
        while (i < n) {
            int n8;
            int n9;
            if (gifFrame.interlace) {
                if (n6 >= n) {
                    if (++n7 != 2) {
                        if (n7 != 3) {
                            if (n7 == 4) {
                                n6 = 1;
                                n5 = 2;
                            }
                        }
                        else {
                            n6 = 2;
                            n5 = 4;
                        }
                    }
                    else {
                        n6 = 4;
                    }
                }
                n8 = n6 + n5;
                n9 = n7;
            }
            else {
                n8 = n6;
                n6 = i;
                n9 = n7;
            }
            final int n10 = n6 + n2;
            final boolean b2 = sampleSize == 1;
            Boolean b3 = null;
            int n26 = 0;
            int n27 = 0;
            Label_0508: {
                int n18;
                if (n10 < downsampledHeight) {
                    final int n11 = n10 * downsampledWidth;
                    final int n12 = n11 + n4;
                    final int n13 = n12 + n3;
                    final int n14 = n11 + downsampledWidth;
                    int n15;
                    if (n14 < (n15 = n13)) {
                        n15 = n14;
                    }
                    int n16 = i * sampleSize * gifFrame.iw;
                    if (b2) {
                        int n17 = n12;
                        while (true) {
                            n18 = n2;
                            b3 = isFirstFrameTransparent;
                            if (n17 >= n15) {
                                break;
                            }
                            final int n19 = act[mainPixels[n16] & 0xFF];
                            Boolean b4;
                            if (n19 != 0) {
                                mainScratch[n17] = n19;
                                b4 = isFirstFrameTransparent;
                            }
                            else {
                                b4 = isFirstFrameTransparent;
                                if (b && (b4 = isFirstFrameTransparent) == null) {
                                    b4 = value;
                                }
                            }
                            n16 += sampleSize;
                            ++n17;
                            isFirstFrameTransparent = b4;
                        }
                    }
                    else {
                        final int n20 = n2;
                        final int n21 = n12;
                        int n22 = n16;
                        final int n23 = n3;
                        int n24 = n21;
                        while (true) {
                            final int n25 = n22;
                            n26 = n20;
                            b3 = isFirstFrameTransparent;
                            n27 = n23;
                            if (n24 >= n15) {
                                break Label_0508;
                            }
                            final int averageColorsNear = this.averageColorsNear(n25, (n15 - n12) * sampleSize + n16, gifFrame.iw);
                            Boolean b5;
                            if (averageColorsNear != 0) {
                                mainScratch[n24] = averageColorsNear;
                                b5 = isFirstFrameTransparent;
                            }
                            else {
                                b5 = isFirstFrameTransparent;
                                if (b && (b5 = isFirstFrameTransparent) == null) {
                                    b5 = value;
                                }
                            }
                            n22 = n25 + sampleSize;
                            ++n24;
                            isFirstFrameTransparent = b5;
                        }
                    }
                }
                else {
                    b3 = isFirstFrameTransparent;
                    n18 = n2;
                }
                n27 = n3;
                n26 = n18;
            }
            isFirstFrameTransparent = b3;
            ++i;
            final int n28 = n26;
            n3 = n27;
            n6 = n8;
            n2 = n28;
            n7 = n9;
        }
        if (this.isFirstFrameTransparent == null) {
            this.isFirstFrameTransparent = (isFirstFrameTransparent != null && isFirstFrameTransparent);
        }
    }
    
    private void copyIntoScratchFast(final GifFrame gifFrame) {
        final int[] mainScratch = this.mainScratch;
        final int ih = gifFrame.ih;
        final int iy = gifFrame.iy;
        final int iw = gifFrame.iw;
        final int ix = gifFrame.ix;
        final boolean b = this.framePointer == 0;
        final int downsampledWidth = this.downsampledWidth;
        final byte[] mainPixels = this.mainPixels;
        final int[] act = this.act;
        int i = 0;
        int n = -1;
        while (i < ih) {
            final int n2 = (i + iy) * downsampledWidth;
            int j = n2 + ix;
            final int n3 = j + iw;
            final int n4 = n2 + downsampledWidth;
            int n5;
            if (n4 < (n5 = n3)) {
                n5 = n4;
            }
            int n6 = gifFrame.iw * i;
            while (j < n5) {
                final byte b2 = mainPixels[n6];
                final int n7 = b2 & 0xFF;
                int n8;
                if (n7 != (n8 = n)) {
                    final int n9 = act[n7];
                    if (n9 != 0) {
                        mainScratch[j] = n9;
                        n8 = n;
                    }
                    else {
                        n8 = b2;
                    }
                }
                ++n6;
                ++j;
                n = n8;
            }
            ++i;
        }
        final Boolean isFirstFrameTransparent = this.isFirstFrameTransparent;
        this.isFirstFrameTransparent = ((isFirstFrameTransparent != null && isFirstFrameTransparent) || (this.isFirstFrameTransparent == null && b && n != -1));
    }
    
    private void decodeBitmapData(final GifFrame gifFrame) {
        if (gifFrame != null) {
            this.rawData.position(gifFrame.bufferFrameStart);
        }
        int toIndex;
        if (gifFrame == null) {
            toIndex = this.header.width * this.header.height;
        }
        else {
            toIndex = gifFrame.ih * gifFrame.iw;
        }
        final byte[] mainPixels = this.mainPixels;
        if (mainPixels == null || mainPixels.length < toIndex) {
            this.mainPixels = this.bitmapProvider.obtainByteArray(toIndex);
        }
        final byte[] mainPixels2 = this.mainPixels;
        if (this.prefix == null) {
            this.prefix = new short[4096];
        }
        final short[] prefix = this.prefix;
        if (this.suffix == null) {
            this.suffix = new byte[4096];
        }
        final byte[] suffix = this.suffix;
        if (this.pixelStack == null) {
            this.pixelStack = new byte[4097];
        }
        final byte[] pixelStack = this.pixelStack;
        final int byte1 = this.readByte();
        final int n = 1 << byte1;
        int n2 = n + 2;
        final int n3 = byte1 + 1;
        final int n4 = (1 << n3) - 1;
        int i = 0;
        for (int j = 0; j < n; ++j) {
            prefix[j] = 0;
            suffix[j] = (byte)j;
        }
        final byte[] block = this.block;
        int n5 = n3;
        int n6 = n2;
        int n7 = n4;
        int n8 = 0;
        int n9 = 0;
        int k = 0;
        int n10 = 0;
        int fromIndex = 0;
        int n11 = -1;
        int n12 = 0;
        int n13 = 0;
    Label_0253:
        while (i < toIndex) {
            int block2;
            if ((block2 = n8) == 0) {
                block2 = this.readBlock();
                if (block2 <= 0) {
                    this.status = 3;
                    break;
                }
                n9 = 0;
            }
            n10 += (block[n9] & 0xFF) << k;
            final int n14 = n9 + 1;
            final int n15 = block2 - 1;
            final int n16 = k + 8;
            final int n17 = n6;
            int n18 = n11;
            final int n19 = n12;
            int n20 = n5;
            int n21 = i;
            int n22 = n2;
            int n23 = n17;
            int n24 = n19;
            k = n16;
            while (k >= n20) {
                final int n25 = n10 & n7;
                n10 >>= n20;
                k -= n20;
                if (n25 == n) {
                    n7 = n4;
                    n20 = n3;
                    final int n26 = n22;
                    n18 = -1;
                    n23 = n22;
                    n22 = n26;
                }
                else {
                    if (n25 == n + 1) {
                        final int n27 = n24;
                        final int n28 = n23;
                        n2 = n22;
                        final int n29 = n18;
                        n5 = n20;
                        i = n21;
                        n8 = n15;
                        n9 = n14;
                        n6 = n28;
                        n11 = n29;
                        n12 = n27;
                        continue Label_0253;
                    }
                    if (n18 == -1) {
                        mainPixels2[fromIndex] = suffix[n25];
                        ++fromIndex;
                        ++n21;
                        n18 = (n24 = n25);
                    }
                    else {
                        int n30;
                        int l;
                        if (n25 >= n23) {
                            pixelStack[n13] = (byte)n24;
                            n30 = n13 + 1;
                            l = n18;
                        }
                        else {
                            final int n31 = n25;
                            n30 = n13;
                            l = n31;
                        }
                        while (l >= n) {
                            pixelStack[n30] = suffix[l];
                            ++n30;
                            l = prefix[l];
                        }
                        final int n32 = suffix[l] & 0xFF;
                        final byte b = (byte)n32;
                        mainPixels2[fromIndex] = b;
                        n13 = n30;
                        while (true) {
                            ++fromIndex;
                            ++n21;
                            if (n13 <= 0) {
                                break;
                            }
                            --n13;
                            mainPixels2[fromIndex] = pixelStack[n13];
                        }
                        int n33 = n23;
                        int n34 = n20;
                        int n35 = n7;
                        if (n23 < 4096) {
                            prefix[n23] = (short)n18;
                            suffix[n23] = b;
                            final int n36 = n33 = n23 + 1;
                            n34 = n20;
                            n35 = n7;
                            if ((n36 & n7) == 0x0) {
                                n33 = n36;
                                n34 = n20;
                                n35 = n7;
                                if (n36 < 4096) {
                                    n34 = n20 + 1;
                                    n35 = n7 + n36;
                                    n33 = n36;
                                }
                            }
                        }
                        n18 = n25;
                        n24 = n32;
                        n23 = n33;
                        n20 = n34;
                        n7 = n35;
                    }
                }
            }
            final int n37 = n18;
            n6 = n23;
            n12 = n24;
            n2 = n22;
            i = n21;
            n8 = n15;
            n9 = n14;
            n5 = n20;
            n11 = n37;
        }
        Arrays.fill(mainPixels2, fromIndex, toIndex, (byte)0);
    }
    
    private GifHeaderParser getHeaderParser() {
        if (this.parser == null) {
            this.parser = new GifHeaderParser();
        }
        return this.parser;
    }
    
    private Bitmap getNextBitmap() {
        final Boolean isFirstFrameTransparent = this.isFirstFrameTransparent;
        Bitmap$Config bitmap$Config;
        if (isFirstFrameTransparent != null && !isFirstFrameTransparent) {
            bitmap$Config = this.bitmapConfig;
        }
        else {
            bitmap$Config = Bitmap$Config.ARGB_8888;
        }
        final Bitmap obtain = this.bitmapProvider.obtain(this.downsampledWidth, this.downsampledHeight, bitmap$Config);
        obtain.setHasAlpha(true);
        return obtain;
    }
    
    private int readBlock() {
        final int byte1 = this.readByte();
        if (byte1 <= 0) {
            return byte1;
        }
        final ByteBuffer rawData = this.rawData;
        rawData.get(this.block, 0, Math.min(byte1, rawData.remaining()));
        return byte1;
    }
    
    private int readByte() {
        return this.rawData.get() & 0xFF;
    }
    
    private Bitmap setPixels(final GifFrame gifFrame, final GifFrame gifFrame2) {
        final int[] mainScratch = this.mainScratch;
        final boolean b = false;
        if (gifFrame2 == null) {
            final Bitmap previousImage = this.previousImage;
            if (previousImage != null) {
                this.bitmapProvider.release(previousImage);
            }
            this.previousImage = null;
            Arrays.fill(mainScratch, 0);
        }
        if (gifFrame2 != null && gifFrame2.dispose == 3 && this.previousImage == null) {
            Arrays.fill(mainScratch, 0);
        }
        if (gifFrame2 != null && gifFrame2.dispose > 0) {
            if (gifFrame2.dispose == 2) {
                int bgColor = b ? 1 : 0;
                if (!gifFrame.transparency) {
                    bgColor = this.header.bgColor;
                    if (gifFrame.lct != null && this.header.bgIndex == gifFrame.transIndex) {
                        bgColor = (b ? 1 : 0);
                    }
                }
                final int n = gifFrame2.ih / this.sampleSize;
                final int n2 = gifFrame2.iy / this.sampleSize;
                final int n3 = gifFrame2.iw / this.sampleSize;
                int i = 0;
                for (int n4 = gifFrame2.ix / this.sampleSize, downsampledWidth = this.downsampledWidth; i < n * downsampledWidth + (i = n2 * downsampledWidth + n4); i += this.downsampledWidth) {
                    for (int j = i; j < i + n3; ++j) {
                        mainScratch[j] = bgColor;
                    }
                }
            }
            else if (gifFrame2.dispose == 3) {
                final Bitmap previousImage2 = this.previousImage;
                if (previousImage2 != null) {
                    final int downsampledWidth2 = this.downsampledWidth;
                    previousImage2.getPixels(mainScratch, 0, downsampledWidth2, 0, 0, downsampledWidth2, this.downsampledHeight);
                }
            }
        }
        this.decodeBitmapData(gifFrame);
        if (!gifFrame.interlace && this.sampleSize == 1) {
            this.copyIntoScratchFast(gifFrame);
        }
        else {
            this.copyCopyIntoScratchRobust(gifFrame);
        }
        if (this.savePrevious && (gifFrame.dispose == 0 || gifFrame.dispose == 1)) {
            if (this.previousImage == null) {
                this.previousImage = this.getNextBitmap();
            }
            final Bitmap previousImage3 = this.previousImage;
            final int downsampledWidth3 = this.downsampledWidth;
            previousImage3.setPixels(mainScratch, 0, downsampledWidth3, 0, 0, downsampledWidth3, this.downsampledHeight);
        }
        final Bitmap nextBitmap = this.getNextBitmap();
        final int downsampledWidth4 = this.downsampledWidth;
        nextBitmap.setPixels(mainScratch, 0, downsampledWidth4, 0, 0, downsampledWidth4, this.downsampledHeight);
        return nextBitmap;
    }
    
    @Override
    public void advance() {
        this.framePointer = (this.framePointer + 1) % this.header.frameCount;
    }
    
    @Override
    public void clear() {
        this.header = null;
        final byte[] mainPixels = this.mainPixels;
        if (mainPixels != null) {
            this.bitmapProvider.release(mainPixels);
        }
        final int[] mainScratch = this.mainScratch;
        if (mainScratch != null) {
            this.bitmapProvider.release(mainScratch);
        }
        final Bitmap previousImage = this.previousImage;
        if (previousImage != null) {
            this.bitmapProvider.release(previousImage);
        }
        this.previousImage = null;
        this.rawData = null;
        this.isFirstFrameTransparent = null;
        final byte[] block = this.block;
        if (block != null) {
            this.bitmapProvider.release(block);
        }
    }
    
    @Override
    public int getByteSize() {
        return this.rawData.limit() + this.mainPixels.length + this.mainScratch.length * 4;
    }
    
    @Override
    public int getCurrentFrameIndex() {
        return this.framePointer;
    }
    
    @Override
    public ByteBuffer getData() {
        return this.rawData;
    }
    
    @Override
    public int getDelay(int delay) {
        if (delay >= 0 && delay < this.header.frameCount) {
            delay = this.header.frames.get(delay).delay;
        }
        else {
            delay = -1;
        }
        return delay;
    }
    
    @Override
    public int getFrameCount() {
        return this.header.frameCount;
    }
    
    @Override
    public int getHeight() {
        return this.header.height;
    }
    
    @Deprecated
    @Override
    public int getLoopCount() {
        if (this.header.loopCount == -1) {
            return 1;
        }
        return this.header.loopCount;
    }
    
    @Override
    public int getNetscapeLoopCount() {
        return this.header.loopCount;
    }
    
    @Override
    public int getNextDelay() {
        if (this.header.frameCount > 0) {
            final int framePointer = this.framePointer;
            if (framePointer >= 0) {
                return this.getDelay(framePointer);
            }
        }
        return 0;
    }
    
    @Override
    public Bitmap getNextFrame() {
        synchronized (this) {
            if (this.header.frameCount <= 0 || this.framePointer < 0) {
                final String tag = StandardGifDecoder.TAG;
                if (Log.isLoggable(tag, 3)) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Unable to decode frame, frameCount=");
                    sb.append(this.header.frameCount);
                    sb.append(", framePointer=");
                    sb.append(this.framePointer);
                    Log.d(tag, sb.toString());
                }
                this.status = 1;
            }
            final int status = this.status;
            if (status == 1 || status == 2) {
                final String tag2 = StandardGifDecoder.TAG;
                if (Log.isLoggable(tag2, 3)) {
                    final StringBuilder sb2 = new StringBuilder();
                    sb2.append("Unable to decode frame, status=");
                    sb2.append(this.status);
                    Log.d(tag2, sb2.toString());
                }
                return null;
            }
            this.status = 0;
            if (this.block == null) {
                this.block = this.bitmapProvider.obtainByteArray(255);
            }
            final GifFrame gifFrame = this.header.frames.get(this.framePointer);
            final int n = this.framePointer - 1;
            GifFrame gifFrame2;
            if (n >= 0) {
                gifFrame2 = this.header.frames.get(n);
            }
            else {
                gifFrame2 = null;
            }
            int[] act;
            if (gifFrame.lct != null) {
                act = gifFrame.lct;
            }
            else {
                act = this.header.gct;
            }
            this.act = act;
            if (act == null) {
                final String tag3 = StandardGifDecoder.TAG;
                if (Log.isLoggable(tag3, 3)) {
                    final StringBuilder sb3 = new StringBuilder();
                    sb3.append("No valid color table found for frame #");
                    sb3.append(this.framePointer);
                    Log.d(tag3, sb3.toString());
                }
                this.status = 1;
                return null;
            }
            if (gifFrame.transparency) {
                final int[] act2 = this.act;
                System.arraycopy(act2, 0, this.pct, 0, act2.length);
                (this.act = this.pct)[gifFrame.transIndex] = 0;
                if (gifFrame.dispose == 2 && this.framePointer == 0) {
                    this.isFirstFrameTransparent = true;
                }
            }
            return this.setPixels(gifFrame, gifFrame2);
        }
    }
    
    @Override
    public int getStatus() {
        return this.status;
    }
    
    @Override
    public int getTotalIterationCount() {
        if (this.header.loopCount == -1) {
            return 1;
        }
        if (this.header.loopCount == 0) {
            return 0;
        }
        return this.header.loopCount + 1;
    }
    
    @Override
    public int getWidth() {
        return this.header.width;
    }
    
    @Override
    public int read(final InputStream inputStream, int read) {
        if (inputStream != null) {
            if (read > 0) {
                read += 4096;
            }
            else {
                read = 16384;
            }
            try {
                final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(read);
                final byte[] array = new byte[16384];
                while (true) {
                    read = inputStream.read(array, 0, 16384);
                    if (read == -1) {
                        break;
                    }
                    byteArrayOutputStream.write(array, 0, read);
                }
                byteArrayOutputStream.flush();
                this.read(byteArrayOutputStream.toByteArray());
            }
            catch (final IOException ex) {
                Log.w(StandardGifDecoder.TAG, "Error reading data from stream", (Throwable)ex);
            }
        }
        else {
            this.status = 2;
        }
        if (inputStream != null) {
            try {
                inputStream.close();
            }
            catch (final IOException ex2) {
                Log.w(StandardGifDecoder.TAG, "Error closing stream", (Throwable)ex2);
            }
        }
        return this.status;
    }
    
    @Override
    public int read(final byte[] data) {
        synchronized (this) {
            final GifHeader header = this.getHeaderParser().setData(data).parseHeader();
            this.header = header;
            if (data != null) {
                this.setData(header, data);
            }
            return this.status;
        }
    }
    
    @Override
    public void resetFrameIndex() {
        this.framePointer = -1;
    }
    
    @Override
    public void setData(final GifHeader gifHeader, final ByteBuffer byteBuffer) {
        synchronized (this) {
            this.setData(gifHeader, byteBuffer, 1);
        }
    }
    
    @Override
    public void setData(final GifHeader header, ByteBuffer readOnlyBuffer, int highestOneBit) {
        monitorenter(this);
        Label_0174: {
            if (highestOneBit > 0) {
                Label_0210: {
                    try {
                        highestOneBit = Integer.highestOneBit(highestOneBit);
                        this.status = 0;
                        this.header = header;
                        this.framePointer = -1;
                        readOnlyBuffer = readOnlyBuffer.asReadOnlyBuffer();
                        (this.rawData = readOnlyBuffer).position(0);
                        this.rawData.order(ByteOrder.LITTLE_ENDIAN);
                        this.savePrevious = false;
                        final Iterator<GifFrame> iterator = header.frames.iterator();
                        while (iterator.hasNext()) {
                            if (iterator.next().dispose == 3) {
                                this.savePrevious = true;
                                break;
                            }
                        }
                        this.sampleSize = highestOneBit;
                        this.downsampledWidth = header.width / highestOneBit;
                        this.downsampledHeight = header.height / highestOneBit;
                        this.mainPixels = this.bitmapProvider.obtainByteArray(header.width * header.height);
                        this.mainScratch = this.bitmapProvider.obtainIntArray(this.downsampledWidth * this.downsampledHeight);
                        monitorexit(this);
                        return;
                    }
                    finally {
                        break Label_0210;
                    }
                    break Label_0174;
                }
                monitorexit(this);
            }
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Sample size must be >=0, not: ");
        sb.append(highestOneBit);
        throw new IllegalArgumentException(sb.toString());
    }
    
    @Override
    public void setData(final GifHeader gifHeader, final byte[] array) {
        synchronized (this) {
            this.setData(gifHeader, ByteBuffer.wrap(array));
        }
    }
    
    @Override
    public void setDefaultBitmapConfig(final Bitmap$Config bitmap$Config) {
        if (bitmap$Config != Bitmap$Config.ARGB_8888 && bitmap$Config != Bitmap$Config.RGB_565) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Unsupported format: ");
            sb.append(bitmap$Config);
            sb.append(", must be one of ");
            sb.append(Bitmap$Config.ARGB_8888);
            sb.append(" or ");
            sb.append(Bitmap$Config.RGB_565);
            throw new IllegalArgumentException(sb.toString());
        }
        this.bitmapConfig = bitmap$Config;
    }
}

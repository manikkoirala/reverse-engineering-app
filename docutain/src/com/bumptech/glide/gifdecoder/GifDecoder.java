// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide.gifdecoder;

import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;
import android.graphics.Bitmap$Config;
import java.io.InputStream;
import android.graphics.Bitmap;
import java.nio.ByteBuffer;

public interface GifDecoder
{
    public static final int STATUS_FORMAT_ERROR = 1;
    public static final int STATUS_OK = 0;
    public static final int STATUS_OPEN_ERROR = 2;
    public static final int STATUS_PARTIAL_DECODE = 3;
    public static final int TOTAL_ITERATION_COUNT_FOREVER = 0;
    
    void advance();
    
    void clear();
    
    int getByteSize();
    
    int getCurrentFrameIndex();
    
    ByteBuffer getData();
    
    int getDelay(final int p0);
    
    int getFrameCount();
    
    int getHeight();
    
    @Deprecated
    int getLoopCount();
    
    int getNetscapeLoopCount();
    
    int getNextDelay();
    
    Bitmap getNextFrame();
    
    int getStatus();
    
    int getTotalIterationCount();
    
    int getWidth();
    
    int read(final InputStream p0, final int p1);
    
    int read(final byte[] p0);
    
    void resetFrameIndex();
    
    void setData(final GifHeader p0, final ByteBuffer p1);
    
    void setData(final GifHeader p0, final ByteBuffer p1, final int p2);
    
    void setData(final GifHeader p0, final byte[] p1);
    
    void setDefaultBitmapConfig(final Bitmap$Config p0);
    
    public interface BitmapProvider
    {
        Bitmap obtain(final int p0, final int p1, final Bitmap$Config p2);
        
        byte[] obtainByteArray(final int p0);
        
        int[] obtainIntArray(final int p0);
        
        void release(final Bitmap p0);
        
        void release(final byte[] p0);
        
        void release(final int[] p0);
    }
    
    @Retention(RetentionPolicy.SOURCE)
    public @interface GifDecodeStatus {
    }
}

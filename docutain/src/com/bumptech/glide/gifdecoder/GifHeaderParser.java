// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide.gifdecoder;

import java.nio.ByteOrder;
import java.util.Arrays;
import java.nio.BufferUnderflowException;
import android.util.Log;
import java.nio.ByteBuffer;

public class GifHeaderParser
{
    static final int DEFAULT_FRAME_DELAY = 10;
    private static final int DESCRIPTOR_MASK_INTERLACE_FLAG = 64;
    private static final int DESCRIPTOR_MASK_LCT_FLAG = 128;
    private static final int DESCRIPTOR_MASK_LCT_SIZE = 7;
    private static final int EXTENSION_INTRODUCER = 33;
    private static final int GCE_DISPOSAL_METHOD_SHIFT = 2;
    private static final int GCE_MASK_DISPOSAL_METHOD = 28;
    private static final int GCE_MASK_TRANSPARENT_COLOR_FLAG = 1;
    private static final int IMAGE_SEPARATOR = 44;
    private static final int LABEL_APPLICATION_EXTENSION = 255;
    private static final int LABEL_COMMENT_EXTENSION = 254;
    private static final int LABEL_GRAPHIC_CONTROL_EXTENSION = 249;
    private static final int LABEL_PLAIN_TEXT_EXTENSION = 1;
    private static final int LSD_MASK_GCT_FLAG = 128;
    private static final int LSD_MASK_GCT_SIZE = 7;
    private static final int MASK_INT_LOWEST_BYTE = 255;
    private static final int MAX_BLOCK_SIZE = 256;
    static final int MIN_FRAME_DELAY = 2;
    private static final String TAG = "GifHeaderParser";
    private static final int TRAILER = 59;
    private final byte[] block;
    private int blockSize;
    private GifHeader header;
    private ByteBuffer rawData;
    
    public GifHeaderParser() {
        this.block = new byte[256];
        this.blockSize = 0;
    }
    
    private boolean err() {
        return this.header.status != 0;
    }
    
    private int read() {
        int n;
        try {
            n = (this.rawData.get() & 0xFF);
        }
        catch (final Exception ex) {
            this.header.status = 1;
            n = 0;
        }
        return n;
    }
    
    private void readBitmap() {
        this.header.currentFrame.ix = this.readShort();
        this.header.currentFrame.iy = this.readShort();
        this.header.currentFrame.iw = this.readShort();
        this.header.currentFrame.ih = this.readShort();
        final int read = this.read();
        boolean interlace = false;
        final boolean b = (read & 0x80) != 0x0;
        final int n = (int)Math.pow(2.0, (read & 0x7) + 1);
        final GifFrame currentFrame = this.header.currentFrame;
        if ((read & 0x40) != 0x0) {
            interlace = true;
        }
        currentFrame.interlace = interlace;
        if (b) {
            this.header.currentFrame.lct = this.readColorTable(n);
        }
        else {
            this.header.currentFrame.lct = null;
        }
        this.header.currentFrame.bufferFrameStart = this.rawData.position();
        this.skipImageData();
        if (this.err()) {
            return;
        }
        final GifHeader header = this.header;
        ++header.frameCount;
        this.header.frames.add(this.header.currentFrame);
    }
    
    private void readBlock() {
        final int read = this.read();
        this.blockSize = read;
        if (read > 0) {
            int n = 0;
            int i = 0;
            try {
                while (true) {
                    final int blockSize = this.blockSize;
                    if (n >= blockSize) {
                        break;
                    }
                    final int length = i = blockSize - n;
                    this.rawData.get(this.block, n, length);
                    n += length;
                    i = length;
                }
            }
            catch (final Exception ex) {
                if (Log.isLoggable("GifHeaderParser", 3)) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Error Reading Block n: ");
                    sb.append(n);
                    sb.append(" count: ");
                    sb.append(i);
                    sb.append(" blockSize: ");
                    sb.append(this.blockSize);
                    Log.d("GifHeaderParser", sb.toString(), (Throwable)ex);
                }
                this.header.status = 1;
            }
        }
    }
    
    private int[] readColorTable(final int n) {
        final byte[] dst = new byte[n * 3];
        int[] array = null;
        try {
            this.rawData.get(dst);
            array = array;
            final int[] array2 = new int[256];
            int n2 = 0;
            int n3 = 0;
            while (true) {
                array = array2;
                if (n2 >= n) {
                    break;
                }
                final int n4 = n3 + 1;
                final byte b = dst[n3];
                final int n5 = n4 + 1;
                array2[n2] = ((b & 0xFF) << 16 | 0xFF000000 | (dst[n4] & 0xFF) << 8 | (dst[n5] & 0xFF));
                n3 = n5 + 1;
                ++n2;
            }
        }
        catch (final BufferUnderflowException ex) {
            if (Log.isLoggable("GifHeaderParser", 3)) {
                Log.d("GifHeaderParser", "Format Error Reading Color Table", (Throwable)ex);
            }
            this.header.status = 1;
        }
        return array;
    }
    
    private void readContents() {
        this.readContents(Integer.MAX_VALUE);
    }
    
    private void readContents(final int n) {
        int n2 = 0;
        while (n2 == 0 && !this.err() && this.header.frameCount <= n) {
            final int read = this.read();
            if (read != 33) {
                if (read != 44) {
                    if (read != 59) {
                        this.header.status = 1;
                    }
                    else {
                        n2 = 1;
                    }
                }
                else {
                    if (this.header.currentFrame == null) {
                        this.header.currentFrame = new GifFrame();
                    }
                    this.readBitmap();
                }
            }
            else {
                final int read2 = this.read();
                if (read2 != 1) {
                    if (read2 != 249) {
                        if (read2 != 254) {
                            if (read2 != 255) {
                                this.skip();
                            }
                            else {
                                this.readBlock();
                                final StringBuilder sb = new StringBuilder();
                                for (int i = 0; i < 11; ++i) {
                                    sb.append((char)this.block[i]);
                                }
                                if (sb.toString().equals("NETSCAPE2.0")) {
                                    this.readNetscapeExt();
                                }
                                else {
                                    this.skip();
                                }
                            }
                        }
                        else {
                            this.skip();
                        }
                    }
                    else {
                        this.header.currentFrame = new GifFrame();
                        this.readGraphicControlExt();
                    }
                }
                else {
                    this.skip();
                }
            }
        }
    }
    
    private void readGraphicControlExt() {
        this.read();
        final int read = this.read();
        this.header.currentFrame.dispose = (read & 0x1C) >> 2;
        final int dispose = this.header.currentFrame.dispose;
        boolean transparency = true;
        if (dispose == 0) {
            this.header.currentFrame.dispose = 1;
        }
        final GifFrame currentFrame = this.header.currentFrame;
        if ((read & 0x1) == 0x0) {
            transparency = false;
        }
        currentFrame.transparency = transparency;
        int short1;
        if ((short1 = this.readShort()) < 2) {
            short1 = 10;
        }
        this.header.currentFrame.delay = short1 * 10;
        this.header.currentFrame.transIndex = this.read();
        this.read();
    }
    
    private void readHeader() {
        final StringBuilder sb = new StringBuilder();
        for (int i = 0; i < 6; ++i) {
            sb.append((char)this.read());
        }
        if (!sb.toString().startsWith("GIF")) {
            this.header.status = 1;
            return;
        }
        this.readLSD();
        if (this.header.gctFlag && !this.err()) {
            final GifHeader header = this.header;
            header.gct = this.readColorTable(header.gctSize);
            final GifHeader header2 = this.header;
            header2.bgColor = header2.gct[this.header.bgIndex];
        }
    }
    
    private void readLSD() {
        this.header.width = this.readShort();
        this.header.height = this.readShort();
        final int read = this.read();
        this.header.gctFlag = ((read & 0x80) != 0x0);
        this.header.gctSize = (int)Math.pow(2.0, (read & 0x7) + 1);
        this.header.bgIndex = this.read();
        this.header.pixelAspect = this.read();
    }
    
    private void readNetscapeExt() {
        do {
            this.readBlock();
            final byte[] block = this.block;
            if (block[0] == 1) {
                this.header.loopCount = ((block[2] & 0xFF) << 8 | (block[1] & 0xFF));
            }
        } while (this.blockSize > 0 && !this.err());
    }
    
    private int readShort() {
        return this.rawData.getShort();
    }
    
    private void reset() {
        this.rawData = null;
        Arrays.fill(this.block, (byte)0);
        this.header = new GifHeader();
        this.blockSize = 0;
    }
    
    private void skip() {
        int i;
        do {
            i = this.read();
            this.rawData.position(Math.min(this.rawData.position() + i, this.rawData.limit()));
        } while (i > 0);
    }
    
    private void skipImageData() {
        this.read();
        this.skip();
    }
    
    public void clear() {
        this.rawData = null;
        this.header = null;
    }
    
    public boolean isAnimated() {
        this.readHeader();
        if (!this.err()) {
            this.readContents(2);
        }
        final int frameCount = this.header.frameCount;
        boolean b = true;
        if (frameCount <= 1) {
            b = false;
        }
        return b;
    }
    
    public GifHeader parseHeader() {
        if (this.rawData == null) {
            throw new IllegalStateException("You must call setData() before parseHeader()");
        }
        if (this.err()) {
            return this.header;
        }
        this.readHeader();
        if (!this.err()) {
            this.readContents();
            if (this.header.frameCount < 0) {
                this.header.status = 1;
            }
        }
        return this.header;
    }
    
    public GifHeaderParser setData(ByteBuffer readOnlyBuffer) {
        this.reset();
        readOnlyBuffer = readOnlyBuffer.asReadOnlyBuffer();
        (this.rawData = readOnlyBuffer).position(0);
        this.rawData.order(ByteOrder.LITTLE_ENDIAN);
        return this;
    }
    
    public GifHeaderParser setData(final byte[] array) {
        if (array != null) {
            this.setData(ByteBuffer.wrap(array));
        }
        else {
            this.rawData = null;
            this.header.status = 2;
        }
        return this;
    }
}

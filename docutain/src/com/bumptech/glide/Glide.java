// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide;

import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.bumptech.glide.load.resource.bitmap.Downsampler;
import com.bumptech.glide.load.DecodeFormat;
import com.bumptech.glide.load.engine.prefill.PreFillType;
import android.content.res.Configuration;
import com.bumptech.glide.util.Util;
import androidx.fragment.app.FragmentActivity;
import android.view.View;
import android.app.Fragment;
import android.app.Activity;
import java.util.Iterator;
import java.util.Set;
import android.content.ComponentCallbacks;
import com.bumptech.glide.module.ManifestParser;
import java.util.Collections;
import com.bumptech.glide.util.Preconditions;
import java.io.File;
import android.util.Log;
import java.lang.reflect.InvocationTargetException;
import com.bumptech.glide.load.resource.bitmap.HardwareConfigState;
import com.bumptech.glide.request.target.ImageViewTargetFactory;
import java.util.ArrayList;
import com.bumptech.glide.module.AppGlideModule;
import com.bumptech.glide.module.GlideModule;
import com.bumptech.glide.request.RequestListener;
import java.util.Map;
import android.content.Context;
import com.bumptech.glide.manager.RequestManagerRetriever;
import com.bumptech.glide.load.engine.cache.MemoryCache;
import java.util.List;
import com.bumptech.glide.load.engine.Engine;
import com.bumptech.glide.manager.ConnectivityMonitorFactory;
import com.bumptech.glide.load.engine.prefill.BitmapPreFiller;
import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPool;
import com.bumptech.glide.load.engine.bitmap_recycle.ArrayPool;
import android.content.ComponentCallbacks2;

public class Glide implements ComponentCallbacks2
{
    private static final String DEFAULT_DISK_CACHE_DIR = "image_manager_disk_cache";
    private static final String TAG = "Glide";
    private static volatile Glide glide;
    private static volatile boolean isInitializing;
    private final ArrayPool arrayPool;
    private final BitmapPool bitmapPool;
    private BitmapPreFiller bitmapPreFiller;
    private final ConnectivityMonitorFactory connectivityMonitorFactory;
    private final RequestOptionsFactory defaultRequestOptionsFactory;
    private final Engine engine;
    private final GlideContext glideContext;
    private final List<RequestManager> managers;
    private final MemoryCache memoryCache;
    private MemoryCategory memoryCategory;
    private final RequestManagerRetriever requestManagerRetriever;
    
    Glide(final Context context, final Engine engine, final MemoryCache memoryCache, final BitmapPool bitmapPool, final ArrayPool arrayPool, final RequestManagerRetriever requestManagerRetriever, final ConnectivityMonitorFactory connectivityMonitorFactory, final int n, final RequestOptionsFactory defaultRequestOptionsFactory, final Map<Class<?>, TransitionOptions<?, ?>> map, final List<RequestListener<Object>> list, final List<GlideModule> list2, final AppGlideModule appGlideModule, final GlideExperiments glideExperiments) {
        this.managers = new ArrayList<RequestManager>();
        this.memoryCategory = MemoryCategory.NORMAL;
        this.engine = engine;
        this.bitmapPool = bitmapPool;
        this.arrayPool = arrayPool;
        this.memoryCache = memoryCache;
        this.requestManagerRetriever = requestManagerRetriever;
        this.connectivityMonitorFactory = connectivityMonitorFactory;
        this.defaultRequestOptionsFactory = defaultRequestOptionsFactory;
        this.glideContext = new GlideContext(context, arrayPool, RegistryFactory.lazilyCreateAndInitializeRegistry(this, list2, appGlideModule), new ImageViewTargetFactory(), defaultRequestOptionsFactory, map, list, engine, glideExperiments, n);
    }
    
    static void checkAndInitializeGlide(final Context context, final GeneratedAppGlideModule generatedAppGlideModule) {
        if (!Glide.isInitializing) {
            Glide.isInitializing = true;
            try {
                initializeGlide(context, generatedAppGlideModule);
                return;
            }
            finally {
                Glide.isInitializing = false;
            }
        }
        throw new IllegalStateException("Glide has been called recursively, this is probably an internal library error!");
    }
    
    public static void enableHardwareBitmaps() {
        HardwareConfigState.getInstance().unblockHardwareBitmaps();
    }
    
    public static Glide get(final Context context) {
        if (Glide.glide == null) {
            final GeneratedAppGlideModule annotationGeneratedGlideModules = getAnnotationGeneratedGlideModules(context.getApplicationContext());
            synchronized (Glide.class) {
                if (Glide.glide == null) {
                    checkAndInitializeGlide(context, annotationGeneratedGlideModules);
                }
            }
        }
        return Glide.glide;
    }
    
    private static GeneratedAppGlideModule getAnnotationGeneratedGlideModules(final Context context) {
        try {
            return (GeneratedAppGlideModule)Class.forName("com.bumptech.glide.GeneratedAppGlideModuleImpl").getDeclaredConstructor(Context.class).newInstance(context.getApplicationContext());
        }
        catch (final InvocationTargetException ex) {
            throwIncorrectGlideModule(ex);
        }
        catch (final NoSuchMethodException ex2) {
            throwIncorrectGlideModule(ex2);
        }
        catch (final IllegalAccessException ex3) {
            throwIncorrectGlideModule(ex3);
        }
        catch (final InstantiationException ex4) {
            throwIncorrectGlideModule(ex4);
        }
        catch (final ClassNotFoundException ex5) {
            if (Log.isLoggable("Glide", 5)) {
                Log.w("Glide", "Failed to find GeneratedAppGlideModule. You should include an annotationProcessor compile dependency on com.github.bumptech.glide:compiler in your application and a @GlideModule annotated AppGlideModule implementation or LibraryGlideModules will be silently ignored");
            }
        }
        return null;
    }
    
    public static File getPhotoCacheDir(final Context context) {
        return getPhotoCacheDir(context, "image_manager_disk_cache");
    }
    
    public static File getPhotoCacheDir(final Context context, final String child) {
        final File cacheDir = context.getCacheDir();
        if (cacheDir == null) {
            if (Log.isLoggable("Glide", 6)) {
                Log.e("Glide", "default disk cache dir is null");
            }
            return null;
        }
        final File file = new File(cacheDir, child);
        if (!file.isDirectory() && !file.mkdirs()) {
            return null;
        }
        return file;
    }
    
    private static RequestManagerRetriever getRetriever(final Context context) {
        Preconditions.checkNotNull(context, "You cannot start a load on a not yet attached View or a Fragment where getActivity() returns null (which usually occurs when getActivity() is called before the Fragment is attached or after the Fragment is destroyed).");
        return get(context).getRequestManagerRetriever();
    }
    
    public static void init(final Context context, final GlideBuilder glideBuilder) {
        final GeneratedAppGlideModule annotationGeneratedGlideModules = getAnnotationGeneratedGlideModules(context);
        synchronized (Glide.class) {
            if (Glide.glide != null) {
                tearDown();
            }
            initializeGlide(context, glideBuilder, annotationGeneratedGlideModules);
        }
    }
    
    @Deprecated
    public static void init(final Glide glide) {
        synchronized (Glide.class) {
            if (Glide.glide != null) {
                tearDown();
            }
            Glide.glide = glide;
        }
    }
    
    private static void initializeGlide(final Context context, final GeneratedAppGlideModule generatedAppGlideModule) {
        initializeGlide(context, new GlideBuilder(), generatedAppGlideModule);
    }
    
    private static void initializeGlide(final Context context, final GlideBuilder glideBuilder, final GeneratedAppGlideModule generatedAppGlideModule) {
        final Context applicationContext = context.getApplicationContext();
        Object o = Collections.emptyList();
        if (generatedAppGlideModule == null || generatedAppGlideModule.isManifestParsingEnabled()) {
            o = new ManifestParser(applicationContext).parse();
        }
        if (generatedAppGlideModule != null && !generatedAppGlideModule.getExcludedModuleClasses().isEmpty()) {
            final Set<Class<?>> excludedModuleClasses = generatedAppGlideModule.getExcludedModuleClasses();
            final Iterator iterator = ((List)o).iterator();
            while (iterator.hasNext()) {
                final GlideModule obj = (GlideModule)iterator.next();
                if (!excludedModuleClasses.contains(obj.getClass())) {
                    continue;
                }
                if (Log.isLoggable("Glide", 3)) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("AppGlideModule excludes manifest GlideModule: ");
                    sb.append(obj);
                    Log.d("Glide", sb.toString());
                }
                iterator.remove();
            }
        }
        if (Log.isLoggable("Glide", 3)) {
            for (final GlideModule glideModule : o) {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("Discovered GlideModule from manifest: ");
                sb2.append(glideModule.getClass());
                Log.d("Glide", sb2.toString());
            }
        }
        RequestManagerRetriever.RequestManagerFactory requestManagerFactory;
        if (generatedAppGlideModule != null) {
            requestManagerFactory = generatedAppGlideModule.getRequestManagerFactory();
        }
        else {
            requestManagerFactory = null;
        }
        glideBuilder.setRequestManagerFactory(requestManagerFactory);
        final Iterator iterator3 = ((List)o).iterator();
        while (iterator3.hasNext()) {
            ((GlideModule)iterator3.next()).applyOptions(applicationContext, glideBuilder);
        }
        if (generatedAppGlideModule != null) {
            generatedAppGlideModule.applyOptions(applicationContext, glideBuilder);
        }
        final Glide build = glideBuilder.build(applicationContext, (List<GlideModule>)o, generatedAppGlideModule);
        applicationContext.registerComponentCallbacks((ComponentCallbacks)build);
        Glide.glide = build;
    }
    
    public static void tearDown() {
        synchronized (Glide.class) {
            if (Glide.glide != null) {
                Glide.glide.getContext().getApplicationContext().unregisterComponentCallbacks((ComponentCallbacks)Glide.glide);
                Glide.glide.engine.shutdown();
            }
            Glide.glide = null;
        }
    }
    
    private static void throwIncorrectGlideModule(final Exception cause) {
        throw new IllegalStateException("GeneratedAppGlideModuleImpl is implemented incorrectly. If you've manually implemented this class, remove your implementation. The Annotation processor will generate a correct implementation.", cause);
    }
    
    @Deprecated
    public static RequestManager with(final Activity activity) {
        return getRetriever((Context)activity).get(activity);
    }
    
    @Deprecated
    public static RequestManager with(final Fragment fragment) {
        return getRetriever((Context)fragment.getActivity()).get(fragment);
    }
    
    public static RequestManager with(final Context context) {
        return getRetriever(context).get(context);
    }
    
    public static RequestManager with(final View view) {
        return getRetriever(view.getContext()).get(view);
    }
    
    public static RequestManager with(final androidx.fragment.app.Fragment fragment) {
        return getRetriever(fragment.getContext()).get(fragment);
    }
    
    public static RequestManager with(final FragmentActivity fragmentActivity) {
        return getRetriever((Context)fragmentActivity).get(fragmentActivity);
    }
    
    public void clearDiskCache() {
        Util.assertBackgroundThread();
        this.engine.clearDiskCache();
    }
    
    public void clearMemory() {
        Util.assertMainThread();
        this.memoryCache.clearMemory();
        this.bitmapPool.clearMemory();
        this.arrayPool.clearMemory();
    }
    
    public ArrayPool getArrayPool() {
        return this.arrayPool;
    }
    
    public BitmapPool getBitmapPool() {
        return this.bitmapPool;
    }
    
    ConnectivityMonitorFactory getConnectivityMonitorFactory() {
        return this.connectivityMonitorFactory;
    }
    
    public Context getContext() {
        return this.glideContext.getBaseContext();
    }
    
    GlideContext getGlideContext() {
        return this.glideContext;
    }
    
    public Registry getRegistry() {
        return this.glideContext.getRegistry();
    }
    
    public RequestManagerRetriever getRequestManagerRetriever() {
        return this.requestManagerRetriever;
    }
    
    public void onConfigurationChanged(final Configuration configuration) {
    }
    
    public void onLowMemory() {
        this.clearMemory();
    }
    
    public void onTrimMemory(final int n) {
        this.trimMemory(n);
    }
    
    public void preFillBitmapPool(final PreFillType.Builder... array) {
        synchronized (this) {
            if (this.bitmapPreFiller == null) {
                this.bitmapPreFiller = new BitmapPreFiller(this.memoryCache, this.bitmapPool, this.defaultRequestOptionsFactory.build().getOptions().get(Downsampler.DECODE_FORMAT));
            }
            this.bitmapPreFiller.preFill(array);
        }
    }
    
    void registerRequestManager(final RequestManager requestManager) {
        synchronized (this.managers) {
            if (!this.managers.contains(requestManager)) {
                this.managers.add(requestManager);
                return;
            }
            throw new IllegalStateException("Cannot register already registered manager");
        }
    }
    
    boolean removeFromManagers(final Target<?> target) {
        synchronized (this.managers) {
            final Iterator<RequestManager> iterator = this.managers.iterator();
            while (iterator.hasNext()) {
                if (iterator.next().untrack(target)) {
                    return true;
                }
            }
            return false;
        }
    }
    
    public MemoryCategory setMemoryCategory(final MemoryCategory memoryCategory) {
        Util.assertMainThread();
        this.memoryCache.setSizeMultiplier(memoryCategory.getMultiplier());
        this.bitmapPool.setSizeMultiplier(memoryCategory.getMultiplier());
        final MemoryCategory memoryCategory2 = this.memoryCategory;
        this.memoryCategory = memoryCategory;
        return memoryCategory2;
    }
    
    public void trimMemory(final int n) {
        Util.assertMainThread();
        synchronized (this.managers) {
            final Iterator<RequestManager> iterator = this.managers.iterator();
            while (iterator.hasNext()) {
                iterator.next().onTrimMemory(n);
            }
            monitorexit(this.managers);
            this.memoryCache.trimMemory(n);
            this.bitmapPool.trimMemory(n);
            this.arrayPool.trimMemory(n);
        }
    }
    
    void unregisterRequestManager(final RequestManager requestManager) {
        synchronized (this.managers) {
            if (this.managers.contains(requestManager)) {
                this.managers.remove(requestManager);
                return;
            }
            throw new IllegalStateException("Cannot unregister not yet registered manager");
        }
    }
    
    public interface RequestOptionsFactory
    {
        RequestOptions build();
    }
}

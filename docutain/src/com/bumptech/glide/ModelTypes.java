// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide;

import java.net.URL;
import java.io.File;
import android.net.Uri;
import android.graphics.drawable.Drawable;
import android.graphics.Bitmap;

interface ModelTypes<T>
{
    T load(final Bitmap p0);
    
    T load(final Drawable p0);
    
    T load(final Uri p0);
    
    T load(final File p0);
    
    T load(final Integer p0);
    
    T load(final Object p0);
    
    T load(final String p0);
    
    @Deprecated
    T load(final URL p0);
    
    T load(final byte[] p0);
}

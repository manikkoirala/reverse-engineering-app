// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide;

import android.os.Build$VERSION;
import com.bumptech.glide.util.Preconditions;
import java.util.Collections;
import com.bumptech.glide.load.engine.cache.InternalCacheDiskCacheFactory;
import com.bumptech.glide.load.engine.cache.LruResourceCache;
import com.bumptech.glide.load.engine.bitmap_recycle.LruArrayPool;
import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPoolAdapter;
import com.bumptech.glide.load.engine.bitmap_recycle.LruBitmapPool;
import com.bumptech.glide.manager.DefaultConnectivityMonitorFactory;
import com.bumptech.glide.module.AppGlideModule;
import com.bumptech.glide.module.GlideModule;
import android.content.Context;
import java.util.ArrayList;
import com.bumptech.glide.request.RequestOptions;
import androidx.collection.ArrayMap;
import com.bumptech.glide.manager.RequestManagerRetriever;
import com.bumptech.glide.load.engine.cache.MemorySizeCalculator;
import com.bumptech.glide.load.engine.cache.MemoryCache;
import com.bumptech.glide.load.engine.Engine;
import com.bumptech.glide.load.engine.cache.DiskCache;
import java.util.Map;
import com.bumptech.glide.request.RequestListener;
import java.util.List;
import com.bumptech.glide.manager.ConnectivityMonitorFactory;
import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPool;
import com.bumptech.glide.load.engine.bitmap_recycle.ArrayPool;
import com.bumptech.glide.load.engine.executor.GlideExecutor;

public final class GlideBuilder
{
    private GlideExecutor animationExecutor;
    private ArrayPool arrayPool;
    private BitmapPool bitmapPool;
    private ConnectivityMonitorFactory connectivityMonitorFactory;
    private List<RequestListener<Object>> defaultRequestListeners;
    private Glide.RequestOptionsFactory defaultRequestOptionsFactory;
    private final Map<Class<?>, TransitionOptions<?, ?>> defaultTransitionOptions;
    private GlideExecutor diskCacheExecutor;
    private DiskCache.Factory diskCacheFactory;
    private Engine engine;
    private final GlideExperiments.Builder glideExperimentsBuilder;
    private boolean isActiveResourceRetentionAllowed;
    private int logLevel;
    private MemoryCache memoryCache;
    private MemorySizeCalculator memorySizeCalculator;
    private RequestManagerRetriever.RequestManagerFactory requestManagerFactory;
    private GlideExecutor sourceExecutor;
    
    public GlideBuilder() {
        this.defaultTransitionOptions = new ArrayMap<Class<?>, TransitionOptions<?, ?>>();
        this.glideExperimentsBuilder = new GlideExperiments.Builder();
        this.logLevel = 4;
        this.defaultRequestOptionsFactory = new Glide.RequestOptionsFactory() {
            final GlideBuilder this$0;
            
            @Override
            public RequestOptions build() {
                return new RequestOptions();
            }
        };
    }
    
    public GlideBuilder addGlobalRequestListener(final RequestListener<Object> requestListener) {
        if (this.defaultRequestListeners == null) {
            this.defaultRequestListeners = new ArrayList<RequestListener<Object>>();
        }
        this.defaultRequestListeners.add(requestListener);
        return this;
    }
    
    Glide build(final Context context, final List<GlideModule> list, final AppGlideModule appGlideModule) {
        if (this.sourceExecutor == null) {
            this.sourceExecutor = GlideExecutor.newSourceExecutor();
        }
        if (this.diskCacheExecutor == null) {
            this.diskCacheExecutor = GlideExecutor.newDiskCacheExecutor();
        }
        if (this.animationExecutor == null) {
            this.animationExecutor = GlideExecutor.newAnimationExecutor();
        }
        if (this.memorySizeCalculator == null) {
            this.memorySizeCalculator = new MemorySizeCalculator.Builder(context).build();
        }
        if (this.connectivityMonitorFactory == null) {
            this.connectivityMonitorFactory = new DefaultConnectivityMonitorFactory();
        }
        if (this.bitmapPool == null) {
            final int bitmapPoolSize = this.memorySizeCalculator.getBitmapPoolSize();
            if (bitmapPoolSize > 0) {
                this.bitmapPool = new LruBitmapPool(bitmapPoolSize);
            }
            else {
                this.bitmapPool = new BitmapPoolAdapter();
            }
        }
        if (this.arrayPool == null) {
            this.arrayPool = new LruArrayPool(this.memorySizeCalculator.getArrayPoolSizeInBytes());
        }
        if (this.memoryCache == null) {
            this.memoryCache = new LruResourceCache(this.memorySizeCalculator.getMemoryCacheSize());
        }
        if (this.diskCacheFactory == null) {
            this.diskCacheFactory = new InternalCacheDiskCacheFactory(context);
        }
        if (this.engine == null) {
            this.engine = new Engine(this.memoryCache, this.diskCacheFactory, this.diskCacheExecutor, this.sourceExecutor, GlideExecutor.newUnlimitedSourceExecutor(), this.animationExecutor, this.isActiveResourceRetentionAllowed);
        }
        final List<RequestListener<Object>> defaultRequestListeners = this.defaultRequestListeners;
        if (defaultRequestListeners == null) {
            this.defaultRequestListeners = Collections.emptyList();
        }
        else {
            this.defaultRequestListeners = (List<RequestListener<Object>>)Collections.unmodifiableList((List<?>)defaultRequestListeners);
        }
        final GlideExperiments build = this.glideExperimentsBuilder.build();
        return new Glide(context, this.engine, this.memoryCache, this.bitmapPool, this.arrayPool, new RequestManagerRetriever(this.requestManagerFactory, build), this.connectivityMonitorFactory, this.logLevel, this.defaultRequestOptionsFactory, this.defaultTransitionOptions, this.defaultRequestListeners, list, appGlideModule, build);
    }
    
    public GlideBuilder setAnimationExecutor(final GlideExecutor animationExecutor) {
        this.animationExecutor = animationExecutor;
        return this;
    }
    
    public GlideBuilder setArrayPool(final ArrayPool arrayPool) {
        this.arrayPool = arrayPool;
        return this;
    }
    
    public GlideBuilder setBitmapPool(final BitmapPool bitmapPool) {
        this.bitmapPool = bitmapPool;
        return this;
    }
    
    public GlideBuilder setConnectivityMonitorFactory(final ConnectivityMonitorFactory connectivityMonitorFactory) {
        this.connectivityMonitorFactory = connectivityMonitorFactory;
        return this;
    }
    
    public GlideBuilder setDefaultRequestOptions(final Glide.RequestOptionsFactory requestOptionsFactory) {
        this.defaultRequestOptionsFactory = Preconditions.checkNotNull(requestOptionsFactory);
        return this;
    }
    
    public GlideBuilder setDefaultRequestOptions(final RequestOptions requestOptions) {
        return this.setDefaultRequestOptions(new Glide.RequestOptionsFactory(this, requestOptions) {
            final GlideBuilder this$0;
            final RequestOptions val$requestOptions;
            
            @Override
            public RequestOptions build() {
                RequestOptions val$requestOptions = this.val$requestOptions;
                if (val$requestOptions == null) {
                    val$requestOptions = new RequestOptions();
                }
                return val$requestOptions;
            }
        });
    }
    
    public <T> GlideBuilder setDefaultTransitionOptions(final Class<T> clazz, final TransitionOptions<?, T> transitionOptions) {
        this.defaultTransitionOptions.put(clazz, transitionOptions);
        return this;
    }
    
    public GlideBuilder setDiskCache(final DiskCache.Factory diskCacheFactory) {
        this.diskCacheFactory = diskCacheFactory;
        return this;
    }
    
    public GlideBuilder setDiskCacheExecutor(final GlideExecutor diskCacheExecutor) {
        this.diskCacheExecutor = diskCacheExecutor;
        return this;
    }
    
    GlideBuilder setEngine(final Engine engine) {
        this.engine = engine;
        return this;
    }
    
    public GlideBuilder setImageDecoderEnabledForBitmaps(final boolean b) {
        this.glideExperimentsBuilder.update(new EnableImageDecoderForBitmaps(), b && Build$VERSION.SDK_INT >= 29);
        return this;
    }
    
    public GlideBuilder setIsActiveResourceRetentionAllowed(final boolean isActiveResourceRetentionAllowed) {
        this.isActiveResourceRetentionAllowed = isActiveResourceRetentionAllowed;
        return this;
    }
    
    public GlideBuilder setLogLevel(final int logLevel) {
        if (logLevel >= 2 && logLevel <= 6) {
            this.logLevel = logLevel;
            return this;
        }
        throw new IllegalArgumentException("Log level must be one of Log.VERBOSE, Log.DEBUG, Log.INFO, Log.WARN, or Log.ERROR");
    }
    
    public GlideBuilder setLogRequestOrigins(final boolean b) {
        this.glideExperimentsBuilder.update(new LogRequestOrigins(), b);
        return this;
    }
    
    public GlideBuilder setMemoryCache(final MemoryCache memoryCache) {
        this.memoryCache = memoryCache;
        return this;
    }
    
    public GlideBuilder setMemorySizeCalculator(final MemorySizeCalculator.Builder builder) {
        return this.setMemorySizeCalculator(builder.build());
    }
    
    public GlideBuilder setMemorySizeCalculator(final MemorySizeCalculator memorySizeCalculator) {
        this.memorySizeCalculator = memorySizeCalculator;
        return this;
    }
    
    void setRequestManagerFactory(final RequestManagerRetriever.RequestManagerFactory requestManagerFactory) {
        this.requestManagerFactory = requestManagerFactory;
    }
    
    @Deprecated
    public GlideBuilder setResizeExecutor(final GlideExecutor sourceExecutor) {
        return this.setSourceExecutor(sourceExecutor);
    }
    
    public GlideBuilder setSourceExecutor(final GlideExecutor sourceExecutor) {
        this.sourceExecutor = sourceExecutor;
        return this;
    }
    
    static final class EnableImageDecoderForBitmaps implements Experiment
    {
    }
    
    public static final class LogRequestOrigins implements Experiment
    {
    }
    
    static final class ManualOverrideHardwareBitmapMaxFdCount implements Experiment
    {
        final int fdCount;
        
        ManualOverrideHardwareBitmapMaxFdCount(final int fdCount) {
            this.fdCount = fdCount;
        }
    }
    
    public static final class UseDirectResourceLoader implements Experiment
    {
    }
    
    public static final class WaitForFramesAfterTrimMemory implements Experiment
    {
        private WaitForFramesAfterTrimMemory() {
        }
    }
}

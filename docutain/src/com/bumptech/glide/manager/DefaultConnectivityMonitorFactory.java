// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide.manager;

import android.util.Log;
import androidx.core.content.ContextCompat;
import android.content.Context;

public class DefaultConnectivityMonitorFactory implements ConnectivityMonitorFactory
{
    private static final String NETWORK_PERMISSION = "android.permission.ACCESS_NETWORK_STATE";
    private static final String TAG = "ConnectivityMonitor";
    
    @Override
    public ConnectivityMonitor build(final Context context, final ConnectivityMonitor.ConnectivityListener connectivityListener) {
        final boolean b = ContextCompat.checkSelfPermission(context, "android.permission.ACCESS_NETWORK_STATE") == 0;
        if (Log.isLoggable("ConnectivityMonitor", 3)) {
            String s;
            if (b) {
                s = "ACCESS_NETWORK_STATE permission granted, registering connectivity monitor";
            }
            else {
                s = "ACCESS_NETWORK_STATE permission missing, cannot register connectivity monitor";
            }
            Log.d("ConnectivityMonitor", s);
        }
        ConnectivityMonitor connectivityMonitor;
        if (b) {
            connectivityMonitor = new DefaultConnectivityMonitor(context, connectivityListener);
        }
        else {
            connectivityMonitor = new NullConnectivityMonitor();
        }
        return connectivityMonitor;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide.manager;

public interface Lifecycle
{
    void addListener(final LifecycleListener p0);
    
    void removeListener(final LifecycleListener p0);
}

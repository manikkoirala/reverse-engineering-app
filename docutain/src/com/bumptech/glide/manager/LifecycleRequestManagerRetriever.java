// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide.manager;

import java.util.HashSet;
import java.util.List;
import androidx.fragment.app.Fragment;
import java.util.Set;
import androidx.fragment.app.FragmentManager;
import com.bumptech.glide.Glide;
import android.content.Context;
import com.bumptech.glide.util.Util;
import java.util.HashMap;
import com.bumptech.glide.RequestManager;
import androidx.lifecycle.Lifecycle;
import java.util.Map;

final class LifecycleRequestManagerRetriever
{
    private final RequestManagerRetriever.RequestManagerFactory factory;
    final Map<Lifecycle, RequestManager> lifecycleToRequestManager;
    
    LifecycleRequestManagerRetriever(final RequestManagerRetriever.RequestManagerFactory factory) {
        this.lifecycleToRequestManager = new HashMap<Lifecycle, RequestManager>();
        this.factory = factory;
    }
    
    RequestManager getOnly(final Lifecycle lifecycle) {
        Util.assertMainThread();
        return this.lifecycleToRequestManager.get(lifecycle);
    }
    
    RequestManager getOrCreate(final Context context, final Glide glide, final Lifecycle lifecycle, final FragmentManager fragmentManager, final boolean b) {
        Util.assertMainThread();
        RequestManager requestManager;
        if ((requestManager = this.getOnly(lifecycle)) == null) {
            final LifecycleLifecycle lifecycleLifecycle = new LifecycleLifecycle(lifecycle);
            requestManager = this.factory.build(glide, lifecycleLifecycle, new SupportRequestManagerTreeNode(fragmentManager), context);
            this.lifecycleToRequestManager.put(lifecycle, requestManager);
            lifecycleLifecycle.addListener(new LifecycleListener(this, lifecycle) {
                final LifecycleRequestManagerRetriever this$0;
                final Lifecycle val$lifecycle;
                
                @Override
                public void onDestroy() {
                    this.this$0.lifecycleToRequestManager.remove(this.val$lifecycle);
                }
                
                @Override
                public void onStart() {
                }
                
                @Override
                public void onStop() {
                }
            });
            if (b) {
                requestManager.onStart();
            }
        }
        return requestManager;
    }
    
    private final class SupportRequestManagerTreeNode implements RequestManagerTreeNode
    {
        private final FragmentManager childFragmentManager;
        final LifecycleRequestManagerRetriever this$0;
        
        SupportRequestManagerTreeNode(final LifecycleRequestManagerRetriever this$0, final FragmentManager childFragmentManager) {
            this.this$0 = this$0;
            this.childFragmentManager = childFragmentManager;
        }
        
        private void getChildFragmentsRecursive(final FragmentManager fragmentManager, final Set<RequestManager> set) {
            final List<Fragment> fragments = fragmentManager.getFragments();
            for (int size = fragments.size(), i = 0; i < size; ++i) {
                final Fragment fragment = fragments.get(i);
                this.getChildFragmentsRecursive(fragment.getChildFragmentManager(), set);
                final RequestManager only = this.this$0.getOnly(fragment.getLifecycle());
                if (only != null) {
                    set.add(only);
                }
            }
        }
        
        @Override
        public Set<RequestManager> getDescendants() {
            final HashSet set = new HashSet();
            this.getChildFragmentsRecursive(this.childFragmentManager, set);
            return set;
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide.manager;

import android.content.Context;

final class DefaultConnectivityMonitor implements ConnectivityMonitor
{
    private final Context context;
    final ConnectivityListener listener;
    
    DefaultConnectivityMonitor(final Context context, final ConnectivityListener listener) {
        this.context = context.getApplicationContext();
        this.listener = listener;
    }
    
    private void register() {
        SingletonConnectivityReceiver.get(this.context).register(this.listener);
    }
    
    private void unregister() {
        SingletonConnectivityReceiver.get(this.context).unregister(this.listener);
    }
    
    @Override
    public void onDestroy() {
    }
    
    @Override
    public void onStart() {
        this.register();
    }
    
    @Override
    public void onStop() {
        this.unregister();
    }
}

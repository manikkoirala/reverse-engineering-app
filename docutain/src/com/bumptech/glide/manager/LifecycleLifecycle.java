// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide.manager;

import androidx.lifecycle.OnLifecycleEvent;
import java.util.Iterator;
import java.util.Collection;
import com.bumptech.glide.util.Util;
import androidx.lifecycle.LifecycleOwner;
import java.util.HashSet;
import java.util.Set;
import androidx.lifecycle.LifecycleObserver;

final class LifecycleLifecycle implements Lifecycle, LifecycleObserver
{
    private final androidx.lifecycle.Lifecycle lifecycle;
    private final Set<LifecycleListener> lifecycleListeners;
    
    LifecycleLifecycle(final androidx.lifecycle.Lifecycle lifecycle) {
        this.lifecycleListeners = new HashSet<LifecycleListener>();
        (this.lifecycle = lifecycle).addObserver(this);
    }
    
    @Override
    public void addListener(final LifecycleListener lifecycleListener) {
        this.lifecycleListeners.add(lifecycleListener);
        if (this.lifecycle.getCurrentState() == androidx.lifecycle.Lifecycle.State.DESTROYED) {
            lifecycleListener.onDestroy();
        }
        else if (this.lifecycle.getCurrentState().isAtLeast(androidx.lifecycle.Lifecycle.State.STARTED)) {
            lifecycleListener.onStart();
        }
        else {
            lifecycleListener.onStop();
        }
    }
    
    @OnLifecycleEvent(androidx.lifecycle.Lifecycle.Event.ON_DESTROY)
    public void onDestroy(final LifecycleOwner lifecycleOwner) {
        final Iterator<LifecycleListener> iterator = Util.getSnapshot(this.lifecycleListeners).iterator();
        while (iterator.hasNext()) {
            iterator.next().onDestroy();
        }
        lifecycleOwner.getLifecycle().removeObserver(this);
    }
    
    @OnLifecycleEvent(androidx.lifecycle.Lifecycle.Event.ON_START)
    public void onStart(final LifecycleOwner lifecycleOwner) {
        final Iterator<LifecycleListener> iterator = Util.getSnapshot(this.lifecycleListeners).iterator();
        while (iterator.hasNext()) {
            iterator.next().onStart();
        }
    }
    
    @OnLifecycleEvent(androidx.lifecycle.Lifecycle.Event.ON_STOP)
    public void onStop(final LifecycleOwner lifecycleOwner) {
        final Iterator<LifecycleListener> iterator = Util.getSnapshot(this.lifecycleListeners).iterator();
        while (iterator.hasNext()) {
            iterator.next().onStop();
        }
    }
    
    @Override
    public void removeListener(final LifecycleListener lifecycleListener) {
        this.lifecycleListeners.remove(lifecycleListener);
    }
}

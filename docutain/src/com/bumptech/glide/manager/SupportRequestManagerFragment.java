// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide.manager;

import android.util.Log;
import java.util.Iterator;
import java.util.Collections;
import com.bumptech.glide.Glide;
import android.content.Context;
import androidx.fragment.app.FragmentManager;
import java.util.HashSet;
import com.bumptech.glide.RequestManager;
import java.util.Set;
import androidx.fragment.app.Fragment;

public class SupportRequestManagerFragment extends Fragment
{
    private static final String TAG = "SupportRMFragment";
    private final Set<SupportRequestManagerFragment> childRequestManagerFragments;
    private final ActivityFragmentLifecycle lifecycle;
    private Fragment parentFragmentHint;
    private RequestManager requestManager;
    private final RequestManagerTreeNode requestManagerTreeNode;
    private SupportRequestManagerFragment rootRequestManagerFragment;
    
    public SupportRequestManagerFragment() {
        this(new ActivityFragmentLifecycle());
    }
    
    public SupportRequestManagerFragment(final ActivityFragmentLifecycle lifecycle) {
        this.requestManagerTreeNode = new SupportFragmentRequestManagerTreeNode();
        this.childRequestManagerFragments = new HashSet<SupportRequestManagerFragment>();
        this.lifecycle = lifecycle;
    }
    
    private void addChildRequestManagerFragment(final SupportRequestManagerFragment supportRequestManagerFragment) {
        this.childRequestManagerFragments.add(supportRequestManagerFragment);
    }
    
    private Fragment getParentFragmentUsingHint() {
        Fragment fragment = this.getParentFragment();
        if (fragment == null) {
            fragment = this.parentFragmentHint;
        }
        return fragment;
    }
    
    private static FragmentManager getRootFragmentManager(Fragment parentFragment) {
        while (parentFragment.getParentFragment() != null) {
            parentFragment = parentFragment.getParentFragment();
        }
        return parentFragment.getFragmentManager();
    }
    
    private boolean isDescendant(Fragment parentFragment) {
        final Fragment parentFragmentUsingHint = this.getParentFragmentUsingHint();
        while (true) {
            final Fragment parentFragment2 = parentFragment.getParentFragment();
            if (parentFragment2 == null) {
                return false;
            }
            if (parentFragment2.equals(parentFragmentUsingHint)) {
                return true;
            }
            parentFragment = parentFragment.getParentFragment();
        }
    }
    
    private void registerFragmentWithRoot(final Context context, final FragmentManager fragmentManager) {
        this.unregisterFragmentWithRoot();
        final SupportRequestManagerFragment supportRequestManagerFragment = Glide.get(context).getRequestManagerRetriever().getSupportRequestManagerFragment(fragmentManager);
        this.rootRequestManagerFragment = supportRequestManagerFragment;
        if (!this.equals(supportRequestManagerFragment)) {
            this.rootRequestManagerFragment.addChildRequestManagerFragment(this);
        }
    }
    
    private void removeChildRequestManagerFragment(final SupportRequestManagerFragment supportRequestManagerFragment) {
        this.childRequestManagerFragments.remove(supportRequestManagerFragment);
    }
    
    private void unregisterFragmentWithRoot() {
        final SupportRequestManagerFragment rootRequestManagerFragment = this.rootRequestManagerFragment;
        if (rootRequestManagerFragment != null) {
            rootRequestManagerFragment.removeChildRequestManagerFragment(this);
            this.rootRequestManagerFragment = null;
        }
    }
    
    Set<SupportRequestManagerFragment> getDescendantRequestManagerFragments() {
        final SupportRequestManagerFragment rootRequestManagerFragment = this.rootRequestManagerFragment;
        if (rootRequestManagerFragment == null) {
            return Collections.emptySet();
        }
        if (this.equals(rootRequestManagerFragment)) {
            return Collections.unmodifiableSet((Set<? extends SupportRequestManagerFragment>)this.childRequestManagerFragments);
        }
        final HashSet s = new HashSet();
        for (final SupportRequestManagerFragment supportRequestManagerFragment : this.rootRequestManagerFragment.getDescendantRequestManagerFragments()) {
            if (this.isDescendant(supportRequestManagerFragment.getParentFragmentUsingHint())) {
                s.add(supportRequestManagerFragment);
            }
        }
        return (Set<SupportRequestManagerFragment>)Collections.unmodifiableSet((Set<?>)s);
    }
    
    ActivityFragmentLifecycle getGlideLifecycle() {
        return this.lifecycle;
    }
    
    public RequestManager getRequestManager() {
        return this.requestManager;
    }
    
    public RequestManagerTreeNode getRequestManagerTreeNode() {
        return this.requestManagerTreeNode;
    }
    
    @Override
    public void onAttach(final Context context) {
        super.onAttach(context);
        final FragmentManager rootFragmentManager = getRootFragmentManager(this);
        if (rootFragmentManager == null) {
            if (Log.isLoggable("SupportRMFragment", 5)) {
                Log.w("SupportRMFragment", "Unable to register fragment with root, ancestor detached");
            }
            return;
        }
        try {
            this.registerFragmentWithRoot(this.getContext(), rootFragmentManager);
        }
        catch (final IllegalStateException ex) {
            if (Log.isLoggable("SupportRMFragment", 5)) {
                Log.w("SupportRMFragment", "Unable to register fragment with root", (Throwable)ex);
            }
        }
    }
    
    @Override
    public void onDestroy() {
        super.onDestroy();
        this.lifecycle.onDestroy();
        this.unregisterFragmentWithRoot();
    }
    
    @Override
    public void onDetach() {
        super.onDetach();
        this.parentFragmentHint = null;
        this.unregisterFragmentWithRoot();
    }
    
    @Override
    public void onStart() {
        super.onStart();
        this.lifecycle.onStart();
    }
    
    @Override
    public void onStop() {
        super.onStop();
        this.lifecycle.onStop();
    }
    
    void setParentFragmentHint(final Fragment parentFragmentHint) {
        this.parentFragmentHint = parentFragmentHint;
        if (parentFragmentHint != null) {
            if (parentFragmentHint.getContext() != null) {
                final FragmentManager rootFragmentManager = getRootFragmentManager(parentFragmentHint);
                if (rootFragmentManager == null) {
                    return;
                }
                this.registerFragmentWithRoot(parentFragmentHint.getContext(), rootFragmentManager);
            }
        }
    }
    
    public void setRequestManager(final RequestManager requestManager) {
        this.requestManager = requestManager;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append(super.toString());
        sb.append("{parent=");
        sb.append(this.getParentFragmentUsingHint());
        sb.append("}");
        return sb.toString();
    }
    
    private class SupportFragmentRequestManagerTreeNode implements RequestManagerTreeNode
    {
        final SupportRequestManagerFragment this$0;
        
        SupportFragmentRequestManagerTreeNode(final SupportRequestManagerFragment this$0) {
            this.this$0 = this$0;
        }
        
        @Override
        public Set<RequestManager> getDescendants() {
            final Set<SupportRequestManagerFragment> descendantRequestManagerFragments = this.this$0.getDescendantRequestManagerFragments();
            final HashSet set = new HashSet(descendantRequestManagerFragments.size());
            for (final SupportRequestManagerFragment supportRequestManagerFragment : descendantRequestManagerFragments) {
                if (supportRequestManagerFragment.getRequestManager() != null) {
                    set.add((Object)supportRequestManagerFragment.getRequestManager());
                }
            }
            return (Set<RequestManager>)set;
        }
        
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder();
            sb.append(super.toString());
            sb.append("{fragment=");
            sb.append(this.this$0);
            sb.append("}");
            return sb.toString();
        }
    }
}

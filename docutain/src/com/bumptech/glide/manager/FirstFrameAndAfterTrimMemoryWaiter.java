// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide.manager;

import android.app.Activity;
import android.content.res.Configuration;
import android.content.ComponentCallbacks2;

final class FirstFrameAndAfterTrimMemoryWaiter implements FrameWaiter, ComponentCallbacks2
{
    public void onConfigurationChanged(final Configuration configuration) {
    }
    
    public void onLowMemory() {
        this.onTrimMemory(20);
    }
    
    public void onTrimMemory(final int n) {
    }
    
    @Override
    public void registerSelf(final Activity activity) {
    }
}

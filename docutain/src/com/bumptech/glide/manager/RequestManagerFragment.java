// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide.manager;

import android.util.Log;
import java.util.Iterator;
import java.util.Collections;
import android.content.Context;
import com.bumptech.glide.Glide;
import android.app.Activity;
import android.os.Build$VERSION;
import java.util.HashSet;
import com.bumptech.glide.RequestManager;
import java.util.Set;
import android.app.Fragment;

@Deprecated
public class RequestManagerFragment extends Fragment
{
    private static final String TAG = "RMFragment";
    private final Set<RequestManagerFragment> childRequestManagerFragments;
    private final ActivityFragmentLifecycle lifecycle;
    private Fragment parentFragmentHint;
    private RequestManager requestManager;
    private final RequestManagerTreeNode requestManagerTreeNode;
    private RequestManagerFragment rootRequestManagerFragment;
    
    public RequestManagerFragment() {
        this(new ActivityFragmentLifecycle());
    }
    
    RequestManagerFragment(final ActivityFragmentLifecycle lifecycle) {
        this.requestManagerTreeNode = new FragmentRequestManagerTreeNode();
        this.childRequestManagerFragments = new HashSet<RequestManagerFragment>();
        this.lifecycle = lifecycle;
    }
    
    private void addChildRequestManagerFragment(final RequestManagerFragment requestManagerFragment) {
        this.childRequestManagerFragments.add(requestManagerFragment);
    }
    
    private Fragment getParentFragmentUsingHint() {
        Fragment fragment;
        if (Build$VERSION.SDK_INT >= 17) {
            fragment = this.getParentFragment();
        }
        else {
            fragment = null;
        }
        if (fragment == null) {
            fragment = this.parentFragmentHint;
        }
        return fragment;
    }
    
    private boolean isDescendant(Fragment parentFragment) {
        final Fragment parentFragment2 = this.getParentFragment();
        while (true) {
            final Fragment parentFragment3 = parentFragment.getParentFragment();
            if (parentFragment3 == null) {
                return false;
            }
            if (parentFragment3.equals((Object)parentFragment2)) {
                return true;
            }
            parentFragment = parentFragment.getParentFragment();
        }
    }
    
    private void registerFragmentWithRoot(final Activity activity) {
        this.unregisterFragmentWithRoot();
        final RequestManagerFragment requestManagerFragment = Glide.get((Context)activity).getRequestManagerRetriever().getRequestManagerFragment(activity);
        this.rootRequestManagerFragment = requestManagerFragment;
        if (!this.equals((Object)requestManagerFragment)) {
            this.rootRequestManagerFragment.addChildRequestManagerFragment(this);
        }
    }
    
    private void removeChildRequestManagerFragment(final RequestManagerFragment requestManagerFragment) {
        this.childRequestManagerFragments.remove(requestManagerFragment);
    }
    
    private void unregisterFragmentWithRoot() {
        final RequestManagerFragment rootRequestManagerFragment = this.rootRequestManagerFragment;
        if (rootRequestManagerFragment != null) {
            rootRequestManagerFragment.removeChildRequestManagerFragment(this);
            this.rootRequestManagerFragment = null;
        }
    }
    
    Set<RequestManagerFragment> getDescendantRequestManagerFragments() {
        if (this.equals((Object)this.rootRequestManagerFragment)) {
            return Collections.unmodifiableSet((Set<? extends RequestManagerFragment>)this.childRequestManagerFragments);
        }
        if (this.rootRequestManagerFragment != null && Build$VERSION.SDK_INT >= 17) {
            final HashSet s = new HashSet();
            for (final RequestManagerFragment requestManagerFragment : this.rootRequestManagerFragment.getDescendantRequestManagerFragments()) {
                if (this.isDescendant(requestManagerFragment.getParentFragment())) {
                    s.add(requestManagerFragment);
                }
            }
            return (Set<RequestManagerFragment>)Collections.unmodifiableSet((Set<?>)s);
        }
        return Collections.emptySet();
    }
    
    ActivityFragmentLifecycle getGlideLifecycle() {
        return this.lifecycle;
    }
    
    public RequestManager getRequestManager() {
        return this.requestManager;
    }
    
    public RequestManagerTreeNode getRequestManagerTreeNode() {
        return this.requestManagerTreeNode;
    }
    
    public void onAttach(final Activity activity) {
        super.onAttach(activity);
        try {
            this.registerFragmentWithRoot(activity);
        }
        catch (final IllegalStateException ex) {
            if (Log.isLoggable("RMFragment", 5)) {
                Log.w("RMFragment", "Unable to register fragment with root", (Throwable)ex);
            }
        }
    }
    
    public void onDestroy() {
        super.onDestroy();
        this.lifecycle.onDestroy();
        this.unregisterFragmentWithRoot();
    }
    
    public void onDetach() {
        super.onDetach();
        this.unregisterFragmentWithRoot();
    }
    
    public void onStart() {
        super.onStart();
        this.lifecycle.onStart();
    }
    
    public void onStop() {
        super.onStop();
        this.lifecycle.onStop();
    }
    
    void setParentFragmentHint(final Fragment parentFragmentHint) {
        this.parentFragmentHint = parentFragmentHint;
        if (parentFragmentHint != null && parentFragmentHint.getActivity() != null) {
            this.registerFragmentWithRoot(parentFragmentHint.getActivity());
        }
    }
    
    public void setRequestManager(final RequestManager requestManager) {
        this.requestManager = requestManager;
    }
    
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append(super.toString());
        sb.append("{parent=");
        sb.append(this.getParentFragmentUsingHint());
        sb.append("}");
        return sb.toString();
    }
    
    private class FragmentRequestManagerTreeNode implements RequestManagerTreeNode
    {
        final RequestManagerFragment this$0;
        
        FragmentRequestManagerTreeNode(final RequestManagerFragment this$0) {
            this.this$0 = this$0;
        }
        
        @Override
        public Set<RequestManager> getDescendants() {
            final Set<RequestManagerFragment> descendantRequestManagerFragments = this.this$0.getDescendantRequestManagerFragments();
            final HashSet set = new HashSet(descendantRequestManagerFragments.size());
            for (final RequestManagerFragment requestManagerFragment : descendantRequestManagerFragments) {
                if (requestManagerFragment.getRequestManager() != null) {
                    set.add((Object)requestManagerFragment.getRequestManager());
                }
            }
            return (Set<RequestManager>)set;
        }
        
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder();
            sb.append(super.toString());
            sb.append("{fragment=");
            sb.append(this.this$0);
            sb.append("}");
            return sb.toString();
        }
    }
}

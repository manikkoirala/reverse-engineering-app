// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide.manager;

import java.util.Iterator;
import android.content.IntentFilter;
import android.net.NetworkInfo;
import android.content.Intent;
import android.os.AsyncTask;
import android.content.BroadcastReceiver;
import java.util.concurrent.Executor;
import android.util.Log;
import android.net.Network;
import android.net.ConnectivityManager$NetworkCallback;
import android.os.Build$VERSION;
import java.util.Collection;
import java.util.ArrayList;
import com.bumptech.glide.util.Util;
import android.net.ConnectivityManager;
import com.bumptech.glide.util.GlideSuppliers;
import java.util.HashSet;
import android.content.Context;
import java.util.Set;

final class SingletonConnectivityReceiver
{
    private static final String TAG = "ConnectivityMonitor";
    private static volatile SingletonConnectivityReceiver instance;
    private final FrameworkConnectivityMonitor frameworkConnectivityMonitor;
    private boolean isRegistered;
    final Set<ConnectivityMonitor.ConnectivityListener> listeners;
    
    private SingletonConnectivityReceiver(final Context context) {
        this.listeners = new HashSet<ConnectivityMonitor.ConnectivityListener>();
        final GlideSuppliers.GlideSupplier<ConnectivityManager> memorize = GlideSuppliers.memorize((GlideSuppliers.GlideSupplier<ConnectivityManager>)new GlideSuppliers.GlideSupplier<ConnectivityManager>(this, context) {
            final SingletonConnectivityReceiver this$0;
            final Context val$context;
            
            public ConnectivityManager get() {
                return (ConnectivityManager)this.val$context.getSystemService("connectivity");
            }
        });
        final ConnectivityMonitor.ConnectivityListener connectivityListener = new ConnectivityMonitor.ConnectivityListener(this) {
            final SingletonConnectivityReceiver this$0;
            
            @Override
            public void onConnectivityChanged(final boolean b) {
                Util.assertMainThread();
                Object o = this.this$0;
                synchronized (o) {
                    final ArrayList list = new ArrayList(this.this$0.listeners);
                    monitorexit(o);
                    o = list.iterator();
                    while (((Iterator)o).hasNext()) {
                        ((ConnectivityListener)((Iterator)o).next()).onConnectivityChanged(b);
                    }
                }
            }
        };
        FrameworkConnectivityMonitor frameworkConnectivityMonitor;
        if (Build$VERSION.SDK_INT >= 24) {
            frameworkConnectivityMonitor = new FrameworkConnectivityMonitorPostApi24(memorize, connectivityListener);
        }
        else {
            frameworkConnectivityMonitor = new FrameworkConnectivityMonitorPreApi24(context, memorize, connectivityListener);
        }
        this.frameworkConnectivityMonitor = frameworkConnectivityMonitor;
    }
    
    static SingletonConnectivityReceiver get(final Context context) {
        if (SingletonConnectivityReceiver.instance == null) {
            synchronized (SingletonConnectivityReceiver.class) {
                if (SingletonConnectivityReceiver.instance == null) {
                    SingletonConnectivityReceiver.instance = new SingletonConnectivityReceiver(context.getApplicationContext());
                }
            }
        }
        return SingletonConnectivityReceiver.instance;
    }
    
    private void maybeRegisterReceiver() {
        if (!this.isRegistered) {
            if (!this.listeners.isEmpty()) {
                this.isRegistered = this.frameworkConnectivityMonitor.register();
            }
        }
    }
    
    private void maybeUnregisterReceiver() {
        if (this.isRegistered) {
            if (this.listeners.isEmpty()) {
                this.frameworkConnectivityMonitor.unregister();
                this.isRegistered = false;
            }
        }
    }
    
    static void reset() {
        SingletonConnectivityReceiver.instance = null;
    }
    
    void register(final ConnectivityMonitor.ConnectivityListener connectivityListener) {
        synchronized (this) {
            this.listeners.add(connectivityListener);
            this.maybeRegisterReceiver();
        }
    }
    
    void unregister(final ConnectivityMonitor.ConnectivityListener connectivityListener) {
        synchronized (this) {
            this.listeners.remove(connectivityListener);
            this.maybeUnregisterReceiver();
        }
    }
    
    private interface FrameworkConnectivityMonitor
    {
        boolean register();
        
        void unregister();
    }
    
    private static final class FrameworkConnectivityMonitorPostApi24 implements FrameworkConnectivityMonitor
    {
        private final GlideSuppliers.GlideSupplier<ConnectivityManager> connectivityManager;
        boolean isConnected;
        final ConnectivityMonitor.ConnectivityListener listener;
        private final ConnectivityManager$NetworkCallback networkCallback;
        
        FrameworkConnectivityMonitorPostApi24(final GlideSuppliers.GlideSupplier<ConnectivityManager> connectivityManager, final ConnectivityMonitor.ConnectivityListener listener) {
            this.networkCallback = new ConnectivityManager$NetworkCallback() {
                final FrameworkConnectivityMonitorPostApi24 this$0;
                
                private void postOnConnectivityChange(final boolean b) {
                    Util.postOnUiThread(new Runnable(this, b) {
                        final SingletonConnectivityReceiver$FrameworkConnectivityMonitorPostApi24$1 this$1;
                        final boolean val$newState;
                        
                        @Override
                        public void run() {
                            this.this$1.onConnectivityChange(this.val$newState);
                        }
                    });
                }
                
                public void onAvailable(final Network network) {
                    this.postOnConnectivityChange(true);
                }
                
                void onConnectivityChange(final boolean isConnected) {
                    Util.assertMainThread();
                    final boolean isConnected2 = this.this$0.isConnected;
                    this.this$0.isConnected = isConnected;
                    if (isConnected2 != isConnected) {
                        this.this$0.listener.onConnectivityChanged(isConnected);
                    }
                }
                
                public void onLost(final Network network) {
                    this.postOnConnectivityChange(false);
                }
            };
            this.connectivityManager = connectivityManager;
            this.listener = listener;
        }
        
        @Override
        public boolean register() {
            this.isConnected = (this.connectivityManager.get().getActiveNetwork() != null);
            try {
                this.connectivityManager.get().registerDefaultNetworkCallback(this.networkCallback);
                return true;
            }
            catch (final RuntimeException ex) {
                if (Log.isLoggable("ConnectivityMonitor", 5)) {
                    Log.w("ConnectivityMonitor", "Failed to register callback", (Throwable)ex);
                }
                return false;
            }
        }
        
        @Override
        public void unregister() {
            this.connectivityManager.get().unregisterNetworkCallback(this.networkCallback);
        }
    }
    
    private static final class FrameworkConnectivityMonitorPreApi24 implements FrameworkConnectivityMonitor
    {
        static final Executor EXECUTOR;
        private final GlideSuppliers.GlideSupplier<ConnectivityManager> connectivityManager;
        final BroadcastReceiver connectivityReceiver;
        final Context context;
        volatile boolean isConnected;
        volatile boolean isRegistered;
        final ConnectivityMonitor.ConnectivityListener listener;
        
        static {
            EXECUTOR = AsyncTask.SERIAL_EXECUTOR;
        }
        
        FrameworkConnectivityMonitorPreApi24(final Context context, final GlideSuppliers.GlideSupplier<ConnectivityManager> connectivityManager, final ConnectivityMonitor.ConnectivityListener listener) {
            this.connectivityReceiver = new BroadcastReceiver() {
                final FrameworkConnectivityMonitorPreApi24 this$0;
                
                public void onReceive(final Context context, final Intent intent) {
                    this.this$0.onConnectivityChange();
                }
            };
            this.context = context.getApplicationContext();
            this.connectivityManager = connectivityManager;
            this.listener = listener;
        }
        
        boolean isConnected() {
            boolean b = true;
            try {
                final NetworkInfo activeNetworkInfo = this.connectivityManager.get().getActiveNetworkInfo();
                if (activeNetworkInfo == null || !activeNetworkInfo.isConnected()) {
                    b = false;
                }
                return b;
            }
            catch (final RuntimeException ex) {
                if (Log.isLoggable("ConnectivityMonitor", 5)) {
                    Log.w("ConnectivityMonitor", "Failed to determine connectivity status when connectivity changed", (Throwable)ex);
                }
                return true;
            }
        }
        
        void notifyChangeOnUiThread(final boolean b) {
            Util.postOnUiThread(new Runnable(this, b) {
                final FrameworkConnectivityMonitorPreApi24 this$0;
                final boolean val$isConnected;
                
                @Override
                public void run() {
                    this.this$0.listener.onConnectivityChanged(this.val$isConnected);
                }
            });
        }
        
        void onConnectivityChange() {
            FrameworkConnectivityMonitorPreApi24.EXECUTOR.execute(new Runnable(this) {
                final FrameworkConnectivityMonitorPreApi24 this$0;
                
                @Override
                public void run() {
                    final boolean isConnected = this.this$0.isConnected;
                    final FrameworkConnectivityMonitorPreApi24 this$0 = this.this$0;
                    this$0.isConnected = this$0.isConnected();
                    if (isConnected != this.this$0.isConnected) {
                        if (Log.isLoggable("ConnectivityMonitor", 3)) {
                            final StringBuilder sb = new StringBuilder();
                            sb.append("connectivity changed, isConnected: ");
                            sb.append(this.this$0.isConnected);
                            Log.d("ConnectivityMonitor", sb.toString());
                        }
                        final FrameworkConnectivityMonitorPreApi24 this$2 = this.this$0;
                        this$2.notifyChangeOnUiThread(this$2.isConnected);
                    }
                }
            });
        }
        
        @Override
        public boolean register() {
            FrameworkConnectivityMonitorPreApi24.EXECUTOR.execute(new Runnable(this) {
                final FrameworkConnectivityMonitorPreApi24 this$0;
                
                @Override
                public void run() {
                    final FrameworkConnectivityMonitorPreApi24 this$0 = this.this$0;
                    this$0.isConnected = this$0.isConnected();
                    try {
                        this.this$0.context.registerReceiver(this.this$0.connectivityReceiver, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
                        this.this$0.isRegistered = true;
                    }
                    catch (final SecurityException ex) {
                        if (Log.isLoggable("ConnectivityMonitor", 5)) {
                            Log.w("ConnectivityMonitor", "Failed to register", (Throwable)ex);
                        }
                        this.this$0.isRegistered = false;
                    }
                }
            });
            return true;
        }
        
        @Override
        public void unregister() {
            FrameworkConnectivityMonitorPreApi24.EXECUTOR.execute(new Runnable(this) {
                final FrameworkConnectivityMonitorPreApi24 this$0;
                
                @Override
                public void run() {
                    if (!this.this$0.isRegistered) {
                        return;
                    }
                    this.this$0.isRegistered = false;
                    this.this$0.context.unregisterReceiver(this.this$0.connectivityReceiver);
                }
            });
        }
    }
}

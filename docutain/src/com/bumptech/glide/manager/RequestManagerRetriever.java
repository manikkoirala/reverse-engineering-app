// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide.manager;

import android.os.Message;
import com.bumptech.glide.util.Preconditions;
import android.app.Application;
import com.bumptech.glide.util.Util;
import android.app.FragmentTransaction;
import android.util.Log;
import androidx.fragment.app.FragmentActivity;
import java.util.Collection;
import java.util.Iterator;
import android.content.ContextWrapper;
import com.bumptech.glide.GlideBuilder;
import com.bumptech.glide.load.resource.bitmap.HardwareConfigState;
import android.os.Build$VERSION;
import android.app.Activity;
import android.os.Looper;
import java.util.HashMap;
import com.bumptech.glide.GlideExperiments;
import android.content.Context;
import com.bumptech.glide.Glide;
import android.app.Fragment;
import android.view.View;
import androidx.collection.ArrayMap;
import android.os.Bundle;
import android.app.FragmentManager;
import java.util.Map;
import android.os.Handler;
import com.bumptech.glide.RequestManager;
import android.os.Handler$Callback;

public class RequestManagerRetriever implements Handler$Callback
{
    private static final RequestManagerFactory DEFAULT_FACTORY;
    private static final String FRAGMENT_INDEX_KEY = "key";
    static final String FRAGMENT_TAG = "com.bumptech.glide.manager";
    private static final int HAS_ATTEMPTED_TO_ADD_FRAGMENT_TWICE = 1;
    private static final int ID_REMOVE_FRAGMENT_MANAGER = 1;
    private static final int ID_REMOVE_SUPPORT_FRAGMENT_MANAGER = 2;
    private static final String TAG = "RMRetriever";
    private volatile RequestManager applicationManager;
    private final RequestManagerFactory factory;
    private final FrameWaiter frameWaiter;
    private final Handler handler;
    private final LifecycleRequestManagerRetriever lifecycleRequestManagerRetriever;
    final Map<FragmentManager, RequestManagerFragment> pendingRequestManagerFragments;
    final Map<androidx.fragment.app.FragmentManager, SupportRequestManagerFragment> pendingSupportRequestManagerFragments;
    private final Bundle tempBundle;
    private final ArrayMap<View, Fragment> tempViewToFragment;
    private final ArrayMap<View, androidx.fragment.app.Fragment> tempViewToSupportFragment;
    
    static {
        DEFAULT_FACTORY = (RequestManagerFactory)new RequestManagerFactory() {
            @Override
            public RequestManager build(final Glide glide, final Lifecycle lifecycle, final RequestManagerTreeNode requestManagerTreeNode, final Context context) {
                return new RequestManager(glide, lifecycle, requestManagerTreeNode, context);
            }
        };
    }
    
    public RequestManagerRetriever(RequestManagerFactory default_FACTORY, final GlideExperiments glideExperiments) {
        this.pendingRequestManagerFragments = new HashMap<FragmentManager, RequestManagerFragment>();
        this.pendingSupportRequestManagerFragments = new HashMap<androidx.fragment.app.FragmentManager, SupportRequestManagerFragment>();
        this.tempViewToSupportFragment = new ArrayMap<View, androidx.fragment.app.Fragment>();
        this.tempViewToFragment = new ArrayMap<View, Fragment>();
        this.tempBundle = new Bundle();
        if (default_FACTORY == null) {
            default_FACTORY = RequestManagerRetriever.DEFAULT_FACTORY;
        }
        this.factory = default_FACTORY;
        this.handler = new Handler(Looper.getMainLooper(), (Handler$Callback)this);
        this.lifecycleRequestManagerRetriever = new LifecycleRequestManagerRetriever(default_FACTORY);
        this.frameWaiter = buildFrameWaiter(glideExperiments);
    }
    
    private static void assertNotDestroyed(final Activity activity) {
        if (Build$VERSION.SDK_INT >= 17 && activity.isDestroyed()) {
            throw new IllegalArgumentException("You cannot start a load for a destroyed activity");
        }
    }
    
    private static FrameWaiter buildFrameWaiter(final GlideExperiments glideExperiments) {
        if (HardwareConfigState.HARDWARE_BITMAPS_SUPPORTED && HardwareConfigState.BLOCK_HARDWARE_BITMAPS_WHEN_GL_CONTEXT_MIGHT_NOT_BE_INITIALIZED) {
            FrameWaiter frameWaiter;
            if (glideExperiments.isEnabled((Class<? extends GlideExperiments.Experiment>)GlideBuilder.WaitForFramesAfterTrimMemory.class)) {
                frameWaiter = new FirstFrameAndAfterTrimMemoryWaiter();
            }
            else {
                frameWaiter = new FirstFrameWaiter();
            }
            return frameWaiter;
        }
        return new DoNothingFirstFrameWaiter();
    }
    
    private static Activity findActivity(final Context context) {
        if (context instanceof Activity) {
            return (Activity)context;
        }
        if (context instanceof ContextWrapper) {
            return findActivity(((ContextWrapper)context).getBaseContext());
        }
        return null;
    }
    
    @Deprecated
    private void findAllFragmentsWithViews(final FragmentManager fragmentManager, final ArrayMap<View, Fragment> arrayMap) {
        if (Build$VERSION.SDK_INT >= 26) {
            for (final Fragment fragment : fragmentManager.getFragments()) {
                if (fragment.getView() != null) {
                    arrayMap.put(fragment.getView(), fragment);
                    this.findAllFragmentsWithViews(fragment.getChildFragmentManager(), arrayMap);
                }
            }
        }
        else {
            this.findAllFragmentsWithViewsPreO(fragmentManager, arrayMap);
        }
    }
    
    @Deprecated
    private void findAllFragmentsWithViewsPreO(final FragmentManager fragmentManager, final ArrayMap<View, Fragment> arrayMap) {
        int n = 0;
        while (true) {
            this.tempBundle.putInt("key", n);
            Fragment fragment = null;
            try {
                fragment = fragmentManager.getFragment(this.tempBundle, "key");
            }
            catch (final Exception ex) {}
            if (fragment == null) {
                break;
            }
            if (fragment.getView() != null) {
                arrayMap.put(fragment.getView(), fragment);
                if (Build$VERSION.SDK_INT >= 17) {
                    this.findAllFragmentsWithViews(fragment.getChildFragmentManager(), arrayMap);
                }
            }
            ++n;
        }
    }
    
    private static void findAllSupportFragmentsWithViews(final Collection<androidx.fragment.app.Fragment> collection, final Map<View, androidx.fragment.app.Fragment> map) {
        if (collection == null) {
            return;
        }
        for (final androidx.fragment.app.Fragment fragment : collection) {
            if (fragment != null) {
                if (fragment.getView() == null) {
                    continue;
                }
                map.put(fragment.getView(), fragment);
                findAllSupportFragmentsWithViews(fragment.getChildFragmentManager().getFragments(), map);
            }
        }
    }
    
    @Deprecated
    private Fragment findFragment(final View view, final Activity activity) {
        this.tempViewToFragment.clear();
        this.findAllFragmentsWithViews(activity.getFragmentManager(), this.tempViewToFragment);
        final View viewById = activity.findViewById(16908290);
        final Fragment fragment = null;
        View view2 = view;
        Fragment fragment2 = fragment;
        Fragment fragment3;
        while (true) {
            fragment3 = fragment2;
            if (view2.equals(viewById)) {
                break;
            }
            fragment2 = this.tempViewToFragment.get(view2);
            if (fragment2 != null) {
                fragment3 = fragment2;
                break;
            }
            fragment3 = fragment2;
            if (!(view2.getParent() instanceof View)) {
                break;
            }
            view2 = (View)view2.getParent();
        }
        this.tempViewToFragment.clear();
        return fragment3;
    }
    
    private androidx.fragment.app.Fragment findSupportFragment(final View view, final FragmentActivity fragmentActivity) {
        this.tempViewToSupportFragment.clear();
        findAllSupportFragmentsWithViews(fragmentActivity.getSupportFragmentManager().getFragments(), this.tempViewToSupportFragment);
        final View viewById = fragmentActivity.findViewById(16908290);
        final androidx.fragment.app.Fragment fragment = null;
        View view2 = view;
        androidx.fragment.app.Fragment fragment2 = fragment;
        androidx.fragment.app.Fragment fragment3;
        while (true) {
            fragment3 = fragment2;
            if (view2.equals(viewById)) {
                break;
            }
            fragment2 = this.tempViewToSupportFragment.get(view2);
            if (fragment2 != null) {
                fragment3 = fragment2;
                break;
            }
            fragment3 = fragment2;
            if (!(view2.getParent() instanceof View)) {
                break;
            }
            view2 = (View)view2.getParent();
        }
        this.tempViewToSupportFragment.clear();
        return fragment3;
    }
    
    @Deprecated
    private RequestManager fragmentGet(final Context context, final FragmentManager fragmentManager, final Fragment fragment, final boolean b) {
        final RequestManagerFragment requestManagerFragment = this.getRequestManagerFragment(fragmentManager, fragment);
        RequestManager requestManager;
        if ((requestManager = requestManagerFragment.getRequestManager()) == null) {
            requestManager = this.factory.build(Glide.get(context), requestManagerFragment.getGlideLifecycle(), requestManagerFragment.getRequestManagerTreeNode(), context);
            if (b) {
                requestManager.onStart();
            }
            requestManagerFragment.setRequestManager(requestManager);
        }
        return requestManager;
    }
    
    private RequestManager getApplicationManager(final Context context) {
        if (this.applicationManager == null) {
            synchronized (this) {
                if (this.applicationManager == null) {
                    this.applicationManager = this.factory.build(Glide.get(context.getApplicationContext()), new ApplicationLifecycle(), new EmptyRequestManagerTreeNode(), context.getApplicationContext());
                }
            }
        }
        return this.applicationManager;
    }
    
    private RequestManagerFragment getRequestManagerFragment(final FragmentManager fragmentManager, final Fragment parentFragmentHint) {
        RequestManagerFragment requestManagerFragment;
        if ((requestManagerFragment = this.pendingRequestManagerFragments.get(fragmentManager)) == null && (requestManagerFragment = (RequestManagerFragment)fragmentManager.findFragmentByTag("com.bumptech.glide.manager")) == null) {
            requestManagerFragment = new RequestManagerFragment();
            requestManagerFragment.setParentFragmentHint(parentFragmentHint);
            this.pendingRequestManagerFragments.put(fragmentManager, requestManagerFragment);
            fragmentManager.beginTransaction().add((Fragment)requestManagerFragment, "com.bumptech.glide.manager").commitAllowingStateLoss();
            this.handler.obtainMessage(1, (Object)fragmentManager).sendToTarget();
        }
        return requestManagerFragment;
    }
    
    private SupportRequestManagerFragment getSupportRequestManagerFragment(final androidx.fragment.app.FragmentManager fragmentManager, final androidx.fragment.app.Fragment parentFragmentHint) {
        SupportRequestManagerFragment supportRequestManagerFragment;
        if ((supportRequestManagerFragment = this.pendingSupportRequestManagerFragments.get(fragmentManager)) == null && (supportRequestManagerFragment = (SupportRequestManagerFragment)fragmentManager.findFragmentByTag("com.bumptech.glide.manager")) == null) {
            supportRequestManagerFragment = new SupportRequestManagerFragment();
            supportRequestManagerFragment.setParentFragmentHint(parentFragmentHint);
            this.pendingSupportRequestManagerFragments.put(fragmentManager, supportRequestManagerFragment);
            fragmentManager.beginTransaction().add(supportRequestManagerFragment, "com.bumptech.glide.manager").commitAllowingStateLoss();
            this.handler.obtainMessage(2, (Object)fragmentManager).sendToTarget();
        }
        return supportRequestManagerFragment;
    }
    
    private static boolean isActivityVisible(final Context context) {
        final Activity activity = findActivity(context);
        return activity == null || !activity.isFinishing();
    }
    
    private boolean verifyOurFragmentWasAddedOrCantBeAdded(final FragmentManager fragmentManager, final boolean b) {
        final RequestManagerFragment obj = this.pendingRequestManagerFragments.get(fragmentManager);
        final RequestManagerFragment obj2 = (RequestManagerFragment)fragmentManager.findFragmentByTag("com.bumptech.glide.manager");
        if (obj2 == obj) {
            return true;
        }
        if (obj2 != null && obj2.getRequestManager() != null) {
            final StringBuilder sb = new StringBuilder();
            sb.append("We've added two fragments with requests! Old: ");
            sb.append(obj2);
            sb.append(" New: ");
            sb.append(obj);
            throw new IllegalStateException(sb.toString());
        }
        if (!b && !fragmentManager.isDestroyed()) {
            final FragmentTransaction add = fragmentManager.beginTransaction().add((Fragment)obj, "com.bumptech.glide.manager");
            if (obj2 != null) {
                add.remove((Fragment)obj2);
            }
            add.commitAllowingStateLoss();
            this.handler.obtainMessage(1, 1, 0, (Object)fragmentManager).sendToTarget();
            if (Log.isLoggable("RMRetriever", 3)) {
                Log.d("RMRetriever", "We failed to add our Fragment the first time around, trying again...");
            }
            return false;
        }
        if (Log.isLoggable("RMRetriever", 5)) {
            if (fragmentManager.isDestroyed()) {
                Log.w("RMRetriever", "Parent was destroyed before our Fragment could be added");
            }
            else {
                Log.w("RMRetriever", "Tried adding Fragment twice and failed twice, giving up!");
            }
        }
        obj.getGlideLifecycle().onDestroy();
        return true;
    }
    
    private boolean verifyOurSupportFragmentWasAddedOrCantBeAdded(final androidx.fragment.app.FragmentManager fragmentManager, final boolean b) {
        final SupportRequestManagerFragment obj = this.pendingSupportRequestManagerFragments.get(fragmentManager);
        final SupportRequestManagerFragment obj2 = (SupportRequestManagerFragment)fragmentManager.findFragmentByTag("com.bumptech.glide.manager");
        if (obj2 == obj) {
            return true;
        }
        if (obj2 != null && obj2.getRequestManager() != null) {
            final StringBuilder sb = new StringBuilder();
            sb.append("We've added two fragments with requests! Old: ");
            sb.append(obj2);
            sb.append(" New: ");
            sb.append(obj);
            throw new IllegalStateException(sb.toString());
        }
        if (!b && !fragmentManager.isDestroyed()) {
            final androidx.fragment.app.FragmentTransaction add = fragmentManager.beginTransaction().add(obj, "com.bumptech.glide.manager");
            if (obj2 != null) {
                add.remove(obj2);
            }
            add.commitNowAllowingStateLoss();
            this.handler.obtainMessage(2, 1, 0, (Object)fragmentManager).sendToTarget();
            if (Log.isLoggable("RMRetriever", 3)) {
                Log.d("RMRetriever", "We failed to add our Fragment the first time around, trying again...");
            }
            return false;
        }
        if (fragmentManager.isDestroyed()) {
            if (Log.isLoggable("RMRetriever", 5)) {
                Log.w("RMRetriever", "Parent was destroyed before our Fragment could be added, all requests for the destroyed parent are cancelled");
            }
        }
        else if (Log.isLoggable("RMRetriever", 6)) {
            Log.e("RMRetriever", "ERROR: Tried adding Fragment twice and failed twice, giving up and cancelling all associated requests! This probably means you're starting loads in a unit test with an Activity that you haven't created and never create. If you're using Robolectric, create the Activity as part of your test setup");
        }
        obj.getGlideLifecycle().onDestroy();
        return true;
    }
    
    @Deprecated
    public RequestManager get(final Activity activity) {
        if (Util.isOnBackgroundThread()) {
            return this.get(activity.getApplicationContext());
        }
        if (activity instanceof FragmentActivity) {
            return this.get((FragmentActivity)activity);
        }
        assertNotDestroyed(activity);
        this.frameWaiter.registerSelf(activity);
        return this.fragmentGet((Context)activity, activity.getFragmentManager(), null, isActivityVisible((Context)activity));
    }
    
    @Deprecated
    public RequestManager get(final Fragment fragment) {
        if (fragment.getActivity() == null) {
            throw new IllegalArgumentException("You cannot start a load on a fragment before it is attached");
        }
        if (!Util.isOnBackgroundThread() && Build$VERSION.SDK_INT >= 17) {
            if (fragment.getActivity() != null) {
                this.frameWaiter.registerSelf(fragment.getActivity());
            }
            return this.fragmentGet((Context)fragment.getActivity(), fragment.getChildFragmentManager(), fragment, fragment.isVisible());
        }
        return this.get(fragment.getActivity().getApplicationContext());
    }
    
    public RequestManager get(final Context context) {
        if (context != null) {
            if (Util.isOnMainThread() && !(context instanceof Application)) {
                if (context instanceof FragmentActivity) {
                    return this.get((FragmentActivity)context);
                }
                if (context instanceof Activity) {
                    return this.get((Activity)context);
                }
                if (context instanceof ContextWrapper) {
                    final ContextWrapper contextWrapper = (ContextWrapper)context;
                    if (contextWrapper.getBaseContext().getApplicationContext() != null) {
                        return this.get(contextWrapper.getBaseContext());
                    }
                }
            }
            return this.getApplicationManager(context);
        }
        throw new IllegalArgumentException("You cannot start a load on a null Context");
    }
    
    public RequestManager get(final View view) {
        if (Util.isOnBackgroundThread()) {
            return this.get(view.getContext().getApplicationContext());
        }
        Preconditions.checkNotNull(view);
        Preconditions.checkNotNull(view.getContext(), "Unable to obtain a request manager for a view without a Context");
        final Activity activity = findActivity(view.getContext());
        if (activity == null) {
            return this.get(view.getContext().getApplicationContext());
        }
        if (activity instanceof FragmentActivity) {
            final FragmentActivity fragmentActivity = (FragmentActivity)activity;
            final androidx.fragment.app.Fragment supportFragment = this.findSupportFragment(view, fragmentActivity);
            RequestManager requestManager;
            if (supportFragment != null) {
                requestManager = this.get(supportFragment);
            }
            else {
                requestManager = this.get(fragmentActivity);
            }
            return requestManager;
        }
        final Fragment fragment = this.findFragment(view, activity);
        if (fragment == null) {
            return this.get(activity);
        }
        return this.get(fragment);
    }
    
    public RequestManager get(final androidx.fragment.app.Fragment fragment) {
        Preconditions.checkNotNull(fragment.getContext(), "You cannot start a load on a fragment before it is attached or after it is destroyed");
        if (Util.isOnBackgroundThread()) {
            return this.get(fragment.getContext().getApplicationContext());
        }
        if (fragment.getActivity() != null) {
            this.frameWaiter.registerSelf(fragment.getActivity());
        }
        final androidx.fragment.app.FragmentManager childFragmentManager = fragment.getChildFragmentManager();
        final Context context = fragment.getContext();
        return this.lifecycleRequestManagerRetriever.getOrCreate(context, Glide.get(context.getApplicationContext()), fragment.getLifecycle(), childFragmentManager, fragment.isVisible());
    }
    
    public RequestManager get(final FragmentActivity fragmentActivity) {
        if (Util.isOnBackgroundThread()) {
            return this.get(fragmentActivity.getApplicationContext());
        }
        assertNotDestroyed(fragmentActivity);
        this.frameWaiter.registerSelf(fragmentActivity);
        return this.lifecycleRequestManagerRetriever.getOrCreate((Context)fragmentActivity, Glide.get(fragmentActivity.getApplicationContext()), fragmentActivity.getLifecycle(), fragmentActivity.getSupportFragmentManager(), isActivityVisible((Context)fragmentActivity));
    }
    
    @Deprecated
    RequestManagerFragment getRequestManagerFragment(final Activity activity) {
        return this.getRequestManagerFragment(activity.getFragmentManager(), null);
    }
    
    SupportRequestManagerFragment getSupportRequestManagerFragment(final androidx.fragment.app.FragmentManager fragmentManager) {
        return this.getSupportRequestManagerFragment(fragmentManager, null);
    }
    
    public boolean handleMessage(final Message message) {
        final int arg1 = message.arg1;
        boolean b = false;
        final boolean b2 = true;
        final boolean b3 = arg1 == 1;
        final int what = message.what;
        Object o = null;
        Object obj = null;
        boolean b4 = false;
        Label_0128: {
            Label_0122: {
                if (what != 1) {
                    if (what != 2) {
                        obj = null;
                        b4 = false;
                        break Label_0128;
                    }
                    obj = message.obj;
                    if (!this.verifyOurSupportFragmentWasAddedOrCantBeAdded((androidx.fragment.app.FragmentManager)obj, b3)) {
                        break Label_0122;
                    }
                    o = this.pendingSupportRequestManagerFragments.remove(obj);
                }
                else {
                    obj = message.obj;
                    if (!this.verifyOurFragmentWasAddedOrCantBeAdded((FragmentManager)obj, b3)) {
                        break Label_0122;
                    }
                    o = this.pendingRequestManagerFragments.remove(obj);
                }
                b = true;
                b4 = b2;
                break Label_0128;
            }
            obj = null;
            b4 = b2;
        }
        if (Log.isLoggable("RMRetriever", 5) && b && o == null) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Failed to remove expected request manager fragment, manager: ");
            sb.append(obj);
            Log.w("RMRetriever", sb.toString());
        }
        return b4;
    }
    
    public interface RequestManagerFactory
    {
        RequestManager build(final Glide p0, final Lifecycle p1, final RequestManagerTreeNode p2, final Context p3);
    }
}

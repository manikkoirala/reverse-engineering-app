// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide.manager;

import android.util.Log;
import java.util.Iterator;
import java.util.Collection;
import com.bumptech.glide.util.Util;
import java.util.HashSet;
import java.util.Map;
import java.util.Collections;
import java.util.WeakHashMap;
import com.bumptech.glide.request.Request;
import java.util.Set;

public class RequestTracker
{
    private static final String TAG = "RequestTracker";
    private boolean isPaused;
    private final Set<Request> pendingRequests;
    private final Set<Request> requests;
    
    public RequestTracker() {
        this.requests = Collections.newSetFromMap(new WeakHashMap<Request, Boolean>());
        this.pendingRequests = new HashSet<Request>();
    }
    
    void addRequest(final Request request) {
        this.requests.add(request);
    }
    
    public boolean clearAndRemove(final Request request) {
        final boolean b = true;
        if (request == null) {
            return true;
        }
        final boolean remove = this.requests.remove(request);
        boolean b2 = b;
        if (!this.pendingRequests.remove(request)) {
            b2 = (remove && b);
        }
        if (b2) {
            request.clear();
        }
        return b2;
    }
    
    public void clearRequests() {
        final Iterator<Request> iterator = Util.getSnapshot(this.requests).iterator();
        while (iterator.hasNext()) {
            this.clearAndRemove(iterator.next());
        }
        this.pendingRequests.clear();
    }
    
    public boolean isPaused() {
        return this.isPaused;
    }
    
    public void pauseAllRequests() {
        this.isPaused = true;
        for (final Request request : Util.getSnapshot(this.requests)) {
            if (request.isRunning() || request.isComplete()) {
                request.clear();
                this.pendingRequests.add(request);
            }
        }
    }
    
    public void pauseRequests() {
        this.isPaused = true;
        for (final Request request : Util.getSnapshot(this.requests)) {
            if (request.isRunning()) {
                request.pause();
                this.pendingRequests.add(request);
            }
        }
    }
    
    public void restartRequests() {
        for (final Request request : Util.getSnapshot(this.requests)) {
            if (!request.isComplete() && !request.isCleared()) {
                request.clear();
                if (!this.isPaused) {
                    request.begin();
                }
                else {
                    this.pendingRequests.add(request);
                }
            }
        }
    }
    
    public void resumeRequests() {
        this.isPaused = false;
        for (final Request request : Util.getSnapshot(this.requests)) {
            if (!request.isComplete() && !request.isRunning()) {
                request.begin();
            }
        }
        this.pendingRequests.clear();
    }
    
    public void runRequest(final Request request) {
        this.requests.add(request);
        if (!this.isPaused) {
            request.begin();
        }
        else {
            request.clear();
            if (Log.isLoggable("RequestTracker", 2)) {
                Log.v("RequestTracker", "Paused, delaying request");
            }
            this.pendingRequests.add(request);
        }
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append(super.toString());
        sb.append("{numRequests=");
        sb.append(this.requests.size());
        sb.append(", isPaused=");
        sb.append(this.isPaused);
        sb.append("}");
        return sb.toString();
    }
}

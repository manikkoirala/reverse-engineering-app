// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide.load.engine;

import java.util.Collections;
import java.util.HashMap;
import com.bumptech.glide.load.Key;
import java.util.Map;

final class Jobs
{
    private final Map<Key, EngineJob<?>> jobs;
    private final Map<Key, EngineJob<?>> onlyCacheJobs;
    
    Jobs() {
        this.jobs = new HashMap<Key, EngineJob<?>>();
        this.onlyCacheJobs = new HashMap<Key, EngineJob<?>>();
    }
    
    private Map<Key, EngineJob<?>> getJobMap(final boolean b) {
        Map<Key, EngineJob<?>> map;
        if (b) {
            map = this.onlyCacheJobs;
        }
        else {
            map = this.jobs;
        }
        return map;
    }
    
    EngineJob<?> get(final Key key, final boolean b) {
        return this.getJobMap(b).get(key);
    }
    
    Map<Key, EngineJob<?>> getAll() {
        return Collections.unmodifiableMap((Map<? extends Key, ? extends EngineJob<?>>)this.jobs);
    }
    
    void put(final Key key, final EngineJob<?> engineJob) {
        this.getJobMap(engineJob.onlyRetrieveFromCache()).put(key, engineJob);
    }
    
    void removeIfCurrent(final Key key, final EngineJob<?> engineJob) {
        final Map<Key, EngineJob<?>> jobMap = this.getJobMap(engineJob.onlyRetrieveFromCache());
        if (engineJob.equals(jobMap.get(key))) {
            jobMap.remove(key);
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide.load.engine;

import java.security.MessageDigest;
import com.bumptech.glide.load.Key;

final class DataCacheKey implements Key
{
    private final Key signature;
    private final Key sourceKey;
    
    DataCacheKey(final Key sourceKey, final Key signature) {
        this.sourceKey = sourceKey;
        this.signature = signature;
    }
    
    @Override
    public boolean equals(final Object o) {
        final boolean b = o instanceof DataCacheKey;
        boolean b3;
        final boolean b2 = b3 = false;
        if (b) {
            final DataCacheKey dataCacheKey = (DataCacheKey)o;
            b3 = b2;
            if (this.sourceKey.equals(dataCacheKey.sourceKey)) {
                b3 = b2;
                if (this.signature.equals(dataCacheKey.signature)) {
                    b3 = true;
                }
            }
        }
        return b3;
    }
    
    Key getSourceKey() {
        return this.sourceKey;
    }
    
    @Override
    public int hashCode() {
        return this.sourceKey.hashCode() * 31 + this.signature.hashCode();
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("DataCacheKey{sourceKey=");
        sb.append(this.sourceKey);
        sb.append(", signature=");
        sb.append(this.signature);
        sb.append('}');
        return sb.toString();
    }
    
    @Override
    public void updateDiskCacheKey(final MessageDigest messageDigest) {
        this.sourceKey.updateDiskCacheKey(messageDigest);
        this.signature.updateDiskCacheKey(messageDigest);
    }
}

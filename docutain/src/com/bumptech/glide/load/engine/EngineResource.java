// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide.load.engine;

import com.bumptech.glide.util.Preconditions;
import com.bumptech.glide.load.Key;

class EngineResource<Z> implements Resource<Z>
{
    private int acquired;
    private final boolean isMemoryCacheable;
    private final boolean isRecyclable;
    private boolean isRecycled;
    private final Key key;
    private final ResourceListener listener;
    private final Resource<Z> resource;
    
    EngineResource(final Resource<Z> resource, final boolean isMemoryCacheable, final boolean isRecyclable, final Key key, final ResourceListener resourceListener) {
        this.resource = Preconditions.checkNotNull(resource);
        this.isMemoryCacheable = isMemoryCacheable;
        this.isRecyclable = isRecyclable;
        this.key = key;
        this.listener = Preconditions.checkNotNull(resourceListener);
    }
    
    void acquire() {
        synchronized (this) {
            if (!this.isRecycled) {
                ++this.acquired;
                return;
            }
            throw new IllegalStateException("Cannot acquire a recycled resource");
        }
    }
    
    @Override
    public Z get() {
        return this.resource.get();
    }
    
    Resource<Z> getResource() {
        return this.resource;
    }
    
    @Override
    public Class<Z> getResourceClass() {
        return this.resource.getResourceClass();
    }
    
    @Override
    public int getSize() {
        return this.resource.getSize();
    }
    
    boolean isMemoryCacheable() {
        return this.isMemoryCacheable;
    }
    
    @Override
    public void recycle() {
        synchronized (this) {
            if (this.acquired > 0) {
                throw new IllegalStateException("Cannot recycle a resource while it is still acquired");
            }
            if (!this.isRecycled) {
                this.isRecycled = true;
                if (this.isRecyclable) {
                    this.resource.recycle();
                }
                return;
            }
            throw new IllegalStateException("Cannot recycle a resource that has already been recycled");
        }
    }
    
    void release() {
        synchronized (this) {
            int acquired = this.acquired;
            if (acquired > 0) {
                boolean b = true;
                --acquired;
                if ((this.acquired = acquired) != 0) {
                    b = false;
                }
                monitorexit(this);
                if (b) {
                    this.listener.onResourceReleased(this.key, this);
                }
                return;
            }
            throw new IllegalStateException("Cannot release a recycled or not yet acquired resource");
        }
    }
    
    @Override
    public String toString() {
        synchronized (this) {
            final StringBuilder sb = new StringBuilder();
            sb.append("EngineResource{isMemoryCacheable=");
            sb.append(this.isMemoryCacheable);
            sb.append(", listener=");
            sb.append(this.listener);
            sb.append(", key=");
            sb.append(this.key);
            sb.append(", acquired=");
            sb.append(this.acquired);
            sb.append(", isRecycled=");
            sb.append(this.isRecycled);
            sb.append(", resource=");
            sb.append(this.resource);
            sb.append('}');
            return sb.toString();
        }
    }
    
    interface ResourceListener
    {
        void onResourceReleased(final Key p0, final EngineResource<?> p1);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide.load.engine.prefill;

import android.graphics.Bitmap$Config;
import java.util.Map;
import java.util.HashMap;
import com.bumptech.glide.util.Util;
import com.bumptech.glide.load.engine.cache.MemoryCache;
import com.bumptech.glide.load.DecodeFormat;
import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPool;

public final class BitmapPreFiller
{
    private final BitmapPool bitmapPool;
    private BitmapPreFillRunner current;
    private final DecodeFormat defaultFormat;
    private final MemoryCache memoryCache;
    
    public BitmapPreFiller(final MemoryCache memoryCache, final BitmapPool bitmapPool, final DecodeFormat defaultFormat) {
        this.memoryCache = memoryCache;
        this.bitmapPool = bitmapPool;
        this.defaultFormat = defaultFormat;
    }
    
    private static int getSizeInBytes(final PreFillType preFillType) {
        return Util.getBitmapByteSize(preFillType.getWidth(), preFillType.getHeight(), preFillType.getConfig());
    }
    
    PreFillQueue generateAllocationOrder(final PreFillType... array) {
        final long maxSize = this.memoryCache.getMaxSize();
        final long currentSize = this.memoryCache.getCurrentSize();
        final long maxSize2 = this.bitmapPool.getMaxSize();
        final int length = array.length;
        final int n = 0;
        int i = 0;
        int n2 = 0;
        while (i < length) {
            n2 += array[i].getWeight();
            ++i;
        }
        final float n3 = (maxSize - currentSize + maxSize2) / (float)n2;
        final HashMap hashMap = new HashMap();
        for (int length2 = array.length, j = n; j < length2; ++j) {
            final PreFillType preFillType = array[j];
            hashMap.put(preFillType, Math.round(preFillType.getWeight() * n3) / getSizeInBytes(preFillType));
        }
        return new PreFillQueue(hashMap);
    }
    
    public void preFill(final PreFillType.Builder... array) {
        final BitmapPreFillRunner current = this.current;
        if (current != null) {
            current.cancel();
        }
        final PreFillType[] array2 = new PreFillType[array.length];
        for (int i = 0; i < array.length; ++i) {
            final PreFillType.Builder builder = array[i];
            if (builder.getConfig() == null) {
                Bitmap$Config config;
                if (this.defaultFormat == DecodeFormat.PREFER_ARGB_8888) {
                    config = Bitmap$Config.ARGB_8888;
                }
                else {
                    config = Bitmap$Config.RGB_565;
                }
                builder.setConfig(config);
            }
            array2[i] = builder.build();
        }
        Util.postOnUiThread(this.current = new BitmapPreFillRunner(this.bitmapPool, this.memoryCache, this.generateAllocationOrder(array2)));
    }
}

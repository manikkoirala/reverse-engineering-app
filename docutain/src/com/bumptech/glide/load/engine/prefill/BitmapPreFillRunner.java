// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide.load.engine.prefill;

import java.security.MessageDigest;
import android.os.SystemClock;
import android.util.Log;
import com.bumptech.glide.load.engine.Resource;
import com.bumptech.glide.load.Key;
import com.bumptech.glide.load.resource.bitmap.BitmapResource;
import com.bumptech.glide.util.Util;
import android.graphics.Bitmap;
import java.util.HashSet;
import android.os.Looper;
import java.util.concurrent.TimeUnit;
import java.util.Set;
import com.bumptech.glide.load.engine.cache.MemoryCache;
import android.os.Handler;
import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPool;

final class BitmapPreFillRunner implements Runnable
{
    static final int BACKOFF_RATIO = 4;
    private static final Clock DEFAULT_CLOCK;
    static final long INITIAL_BACKOFF_MS = 40L;
    static final long MAX_BACKOFF_MS;
    static final long MAX_DURATION_MS = 32L;
    static final String TAG = "PreFillRunner";
    private final BitmapPool bitmapPool;
    private final Clock clock;
    private long currentDelay;
    private final Handler handler;
    private boolean isCancelled;
    private final MemoryCache memoryCache;
    private final Set<PreFillType> seenTypes;
    private final PreFillQueue toPrefill;
    
    static {
        DEFAULT_CLOCK = new Clock();
        MAX_BACKOFF_MS = TimeUnit.SECONDS.toMillis(1L);
    }
    
    public BitmapPreFillRunner(final BitmapPool bitmapPool, final MemoryCache memoryCache, final PreFillQueue preFillQueue) {
        this(bitmapPool, memoryCache, preFillQueue, BitmapPreFillRunner.DEFAULT_CLOCK, new Handler(Looper.getMainLooper()));
    }
    
    BitmapPreFillRunner(final BitmapPool bitmapPool, final MemoryCache memoryCache, final PreFillQueue toPrefill, final Clock clock, final Handler handler) {
        this.seenTypes = new HashSet<PreFillType>();
        this.currentDelay = 40L;
        this.bitmapPool = bitmapPool;
        this.memoryCache = memoryCache;
        this.toPrefill = toPrefill;
        this.clock = clock;
        this.handler = handler;
    }
    
    private long getFreeMemoryCacheBytes() {
        return this.memoryCache.getMaxSize() - this.memoryCache.getCurrentSize();
    }
    
    private long getNextDelay() {
        final long currentDelay = this.currentDelay;
        this.currentDelay = Math.min(4L * currentDelay, BitmapPreFillRunner.MAX_BACKOFF_MS);
        return currentDelay;
    }
    
    private boolean isGcDetected(final long n) {
        return this.clock.now() - n >= 32L;
    }
    
    boolean allocate() {
        final long now = this.clock.now();
        while (!this.toPrefill.isEmpty() && !this.isGcDetected(now)) {
            final PreFillType remove = this.toPrefill.remove();
            Bitmap bitmap;
            if (!this.seenTypes.contains(remove)) {
                this.seenTypes.add(remove);
                bitmap = this.bitmapPool.getDirty(remove.getWidth(), remove.getHeight(), remove.getConfig());
            }
            else {
                bitmap = Bitmap.createBitmap(remove.getWidth(), remove.getHeight(), remove.getConfig());
            }
            final int bitmapByteSize = Util.getBitmapByteSize(bitmap);
            if (this.getFreeMemoryCacheBytes() >= bitmapByteSize) {
                this.memoryCache.put(new UniqueKey(), BitmapResource.obtain(bitmap, this.bitmapPool));
            }
            else {
                this.bitmapPool.put(bitmap);
            }
            if (Log.isLoggable("PreFillRunner", 3)) {
                final StringBuilder sb = new StringBuilder();
                sb.append("allocated [");
                sb.append(remove.getWidth());
                sb.append("x");
                sb.append(remove.getHeight());
                sb.append("] ");
                sb.append(remove.getConfig());
                sb.append(" size: ");
                sb.append(bitmapByteSize);
                Log.d("PreFillRunner", sb.toString());
            }
        }
        return !this.isCancelled && !this.toPrefill.isEmpty();
    }
    
    public void cancel() {
        this.isCancelled = true;
    }
    
    @Override
    public void run() {
        if (this.allocate()) {
            this.handler.postDelayed((Runnable)this, this.getNextDelay());
        }
    }
    
    static class Clock
    {
        long now() {
            return SystemClock.currentThreadTimeMillis();
        }
    }
    
    private static final class UniqueKey implements Key
    {
        UniqueKey() {
        }
        
        @Override
        public void updateDiskCacheKey(final MessageDigest messageDigest) {
            throw new UnsupportedOperationException();
        }
    }
}

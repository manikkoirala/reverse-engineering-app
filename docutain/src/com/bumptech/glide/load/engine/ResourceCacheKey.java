// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide.load.engine;

import java.nio.ByteBuffer;
import java.security.MessageDigest;
import com.bumptech.glide.util.Util;
import com.bumptech.glide.load.Transformation;
import com.bumptech.glide.load.Options;
import com.bumptech.glide.load.engine.bitmap_recycle.ArrayPool;
import com.bumptech.glide.util.LruCache;
import com.bumptech.glide.load.Key;

final class ResourceCacheKey implements Key
{
    private static final LruCache<Class<?>, byte[]> RESOURCE_CLASS_BYTES;
    private final ArrayPool arrayPool;
    private final Class<?> decodedResourceClass;
    private final int height;
    private final Options options;
    private final Key signature;
    private final Key sourceKey;
    private final Transformation<?> transformation;
    private final int width;
    
    static {
        RESOURCE_CLASS_BYTES = new LruCache<Class<?>, byte[]>(50L);
    }
    
    ResourceCacheKey(final ArrayPool arrayPool, final Key sourceKey, final Key signature, final int width, final int height, final Transformation<?> transformation, final Class<?> decodedResourceClass, final Options options) {
        this.arrayPool = arrayPool;
        this.sourceKey = sourceKey;
        this.signature = signature;
        this.width = width;
        this.height = height;
        this.transformation = transformation;
        this.decodedResourceClass = decodedResourceClass;
        this.options = options;
    }
    
    private byte[] getResourceClassBytes() {
        final LruCache<Class<?>, byte[]> resource_CLASS_BYTES = ResourceCacheKey.RESOURCE_CLASS_BYTES;
        byte[] bytes;
        if ((bytes = resource_CLASS_BYTES.get(this.decodedResourceClass)) == null) {
            bytes = this.decodedResourceClass.getName().getBytes(ResourceCacheKey.CHARSET);
            resource_CLASS_BYTES.put(this.decodedResourceClass, bytes);
        }
        return bytes;
    }
    
    @Override
    public boolean equals(final Object o) {
        final boolean b = o instanceof ResourceCacheKey;
        boolean b3;
        final boolean b2 = b3 = false;
        if (b) {
            final ResourceCacheKey resourceCacheKey = (ResourceCacheKey)o;
            b3 = b2;
            if (this.height == resourceCacheKey.height) {
                b3 = b2;
                if (this.width == resourceCacheKey.width) {
                    b3 = b2;
                    if (Util.bothNullOrEqual(this.transformation, resourceCacheKey.transformation)) {
                        b3 = b2;
                        if (this.decodedResourceClass.equals(resourceCacheKey.decodedResourceClass)) {
                            b3 = b2;
                            if (this.sourceKey.equals(resourceCacheKey.sourceKey)) {
                                b3 = b2;
                                if (this.signature.equals(resourceCacheKey.signature)) {
                                    b3 = b2;
                                    if (this.options.equals(resourceCacheKey.options)) {
                                        b3 = true;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return b3;
    }
    
    @Override
    public int hashCode() {
        final int n = ((this.sourceKey.hashCode() * 31 + this.signature.hashCode()) * 31 + this.width) * 31 + this.height;
        final Transformation<?> transformation = this.transformation;
        int n2 = n;
        if (transformation != null) {
            n2 = n * 31 + transformation.hashCode();
        }
        return (n2 * 31 + this.decodedResourceClass.hashCode()) * 31 + this.options.hashCode();
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("ResourceCacheKey{sourceKey=");
        sb.append(this.sourceKey);
        sb.append(", signature=");
        sb.append(this.signature);
        sb.append(", width=");
        sb.append(this.width);
        sb.append(", height=");
        sb.append(this.height);
        sb.append(", decodedResourceClass=");
        sb.append(this.decodedResourceClass);
        sb.append(", transformation='");
        sb.append(this.transformation);
        sb.append('\'');
        sb.append(", options=");
        sb.append(this.options);
        sb.append('}');
        return sb.toString();
    }
    
    @Override
    public void updateDiskCacheKey(final MessageDigest messageDigest) {
        final byte[] array = this.arrayPool.getExact(8, byte[].class);
        ByteBuffer.wrap(array).putInt(this.width).putInt(this.height).array();
        this.signature.updateDiskCacheKey(messageDigest);
        this.sourceKey.updateDiskCacheKey(messageDigest);
        messageDigest.update(array);
        final Transformation<?> transformation = this.transformation;
        if (transformation != null) {
            transformation.updateDiskCacheKey(messageDigest);
        }
        this.options.updateDiskCacheKey(messageDigest);
        messageDigest.update(this.getResourceClassBytes());
        this.arrayPool.put(array);
    }
}

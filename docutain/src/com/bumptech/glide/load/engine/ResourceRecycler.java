// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide.load.engine;

import android.os.Message;
import android.os.Handler$Callback;
import android.os.Looper;
import android.os.Handler;

class ResourceRecycler
{
    private final Handler handler;
    private boolean isRecycling;
    
    ResourceRecycler() {
        this.handler = new Handler(Looper.getMainLooper(), (Handler$Callback)new ResourceRecyclerCallback());
    }
    
    void recycle(final Resource<?> resource, final boolean b) {
        synchronized (this) {
            if (!this.isRecycling && !b) {
                this.isRecycling = true;
                resource.recycle();
                this.isRecycling = false;
            }
            else {
                this.handler.obtainMessage(1, (Object)resource).sendToTarget();
            }
        }
    }
    
    private static final class ResourceRecyclerCallback implements Handler$Callback
    {
        static final int RECYCLE_RESOURCE = 1;
        
        ResourceRecyclerCallback() {
        }
        
        public boolean handleMessage(final Message message) {
            if (message.what == 1) {
                ((Resource)message.obj).recycle();
                return true;
            }
            return false;
        }
    }
}

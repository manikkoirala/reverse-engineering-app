// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide.load.engine;

import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.data.DataFetcher;
import java.io.IOException;
import com.bumptech.glide.load.Encoder;
import com.bumptech.glide.load.data.DataRewinder;
import java.util.Collections;
import android.util.Log;
import com.bumptech.glide.load.engine.cache.DiskCache;
import com.bumptech.glide.load.Key;
import com.bumptech.glide.util.LogTime;
import com.bumptech.glide.load.model.ModelLoader;

class SourceGenerator implements DataFetcherGenerator, FetcherReadyCallback
{
    private static final String TAG = "SourceGenerator";
    private final FetcherReadyCallback cb;
    private volatile Object dataToCache;
    private final DecodeHelper<?> helper;
    private volatile ModelLoader.LoadData<?> loadData;
    private volatile int loadDataListIndex;
    private volatile DataCacheKey originalKey;
    private volatile DataCacheGenerator sourceCacheGenerator;
    
    SourceGenerator(final DecodeHelper<?> helper, final FetcherReadyCallback cb) {
        this.helper = helper;
        this.cb = cb;
    }
    
    private boolean cacheData(Object obj) throws IOException {
        final long logTime = LogTime.getLogTime();
        boolean b;
        try {
            final DataRewinder<Object> rewinder = this.helper.getRewinder(obj);
            final Object rewindAndGet = rewinder.rewindAndGet();
            final Encoder<Object> sourceEncoder = this.helper.getSourceEncoder(rewindAndGet);
            final DataCacheWriter dataCacheWriter = new DataCacheWriter(sourceEncoder, rewindAndGet, this.helper.getOptions());
            final DataCacheKey dataCacheKey = new DataCacheKey(this.loadData.sourceKey, this.helper.getSignature());
            final DiskCache diskCache = this.helper.getDiskCache();
            diskCache.put(dataCacheKey, (DiskCache.Writer)dataCacheWriter);
            if (Log.isLoggable("SourceGenerator", 2)) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Finished encoding source to cache, key: ");
                sb.append(dataCacheKey);
                sb.append(", data: ");
                sb.append(obj);
                sb.append(", encoder: ");
                sb.append(sourceEncoder);
                sb.append(", duration: ");
                sb.append(LogTime.getElapsedMillis(logTime));
                Log.v("SourceGenerator", sb.toString());
            }
            if (diskCache.get(dataCacheKey) != null) {
                this.originalKey = dataCacheKey;
                obj = new DataCacheGenerator(Collections.singletonList(this.loadData.sourceKey), this.helper, this);
                this.sourceCacheGenerator = (DataCacheGenerator)obj;
                this.loadData.fetcher.cleanup();
                return true;
            }
            if (Log.isLoggable("SourceGenerator", 3)) {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("Attempt to write: ");
                sb2.append(this.originalKey);
                sb2.append(", data: ");
                sb2.append(obj);
                sb2.append(" to the disk cache failed, maybe the disk cache is disabled? Trying to decode the data directly...");
                Log.d("SourceGenerator", sb2.toString());
            }
            try {
                this.cb.onDataFetcherReady(this.loadData.sourceKey, rewinder.rewindAndGet(), this.loadData.fetcher, this.loadData.fetcher.getDataSource(), this.loadData.sourceKey);
                return false;
            }
            finally {}
        }
        finally {
            b = false;
        }
        if (!b) {
            this.loadData.fetcher.cleanup();
        }
    }
    
    private boolean hasNextModelLoader() {
        return this.loadDataListIndex < this.helper.getLoadData().size();
    }
    
    private void startNextLoad(final ModelLoader.LoadData<?> loadData) {
        this.loadData.fetcher.loadData(this.helper.getPriority(), (DataFetcher.DataCallback<?>)new DataFetcher.DataCallback<Object>(this, loadData) {
            final SourceGenerator this$0;
            final ModelLoader.LoadData val$toStart;
            
            @Override
            public void onDataReady(final Object o) {
                if (this.this$0.isCurrentRequest(this.val$toStart)) {
                    this.this$0.onDataReadyInternal(this.val$toStart, o);
                }
            }
            
            @Override
            public void onLoadFailed(final Exception ex) {
                if (this.this$0.isCurrentRequest(this.val$toStart)) {
                    this.this$0.onLoadFailedInternal(this.val$toStart, ex);
                }
            }
        });
    }
    
    @Override
    public void cancel() {
        final ModelLoader.LoadData<?> loadData = this.loadData;
        if (loadData != null) {
            loadData.fetcher.cancel();
        }
    }
    
    boolean isCurrentRequest(final ModelLoader.LoadData<?> loadData) {
        final ModelLoader.LoadData<?> loadData2 = this.loadData;
        return loadData2 != null && loadData2 == loadData;
    }
    
    @Override
    public void onDataFetcherFailed(final Key key, final Exception ex, final DataFetcher<?> dataFetcher, final DataSource dataSource) {
        this.cb.onDataFetcherFailed(key, ex, dataFetcher, this.loadData.fetcher.getDataSource());
    }
    
    @Override
    public void onDataFetcherReady(final Key key, final Object o, final DataFetcher<?> dataFetcher, final DataSource dataSource, final Key key2) {
        this.cb.onDataFetcherReady(key, o, dataFetcher, this.loadData.fetcher.getDataSource(), key);
    }
    
    void onDataReadyInternal(final ModelLoader.LoadData<?> loadData, final Object dataToCache) {
        final DiskCacheStrategy diskCacheStrategy = this.helper.getDiskCacheStrategy();
        if (dataToCache != null && diskCacheStrategy.isDataCacheable(loadData.fetcher.getDataSource())) {
            this.dataToCache = dataToCache;
            this.cb.reschedule();
        }
        else {
            this.cb.onDataFetcherReady(loadData.sourceKey, dataToCache, loadData.fetcher, loadData.fetcher.getDataSource(), this.originalKey);
        }
    }
    
    void onLoadFailedInternal(final ModelLoader.LoadData<?> loadData, final Exception ex) {
        this.cb.onDataFetcherFailed(this.originalKey, ex, loadData.fetcher, loadData.fetcher.getDataSource());
    }
    
    @Override
    public void reschedule() {
        throw new UnsupportedOperationException();
    }
    
    @Override
    public boolean startNext() {
        if (this.dataToCache != null) {
            final Object dataToCache = this.dataToCache;
            this.dataToCache = null;
            try {
                if (!this.cacheData(dataToCache)) {
                    return true;
                }
            }
            catch (final IOException ex) {
                if (Log.isLoggable("SourceGenerator", 3)) {
                    Log.d("SourceGenerator", "Failed to properly rewind or write data to cache", (Throwable)ex);
                }
            }
        }
        if (this.sourceCacheGenerator != null && this.sourceCacheGenerator.startNext()) {
            return true;
        }
        this.sourceCacheGenerator = null;
        this.loadData = null;
        boolean b;
        for (b = false; !b && this.hasNextModelLoader(); b = true) {
            this.loadData = (ModelLoader.LoadData<?>)(ModelLoader.LoadData)this.helper.getLoadData().get(this.loadDataListIndex++);
            if (this.loadData != null && (this.helper.getDiskCacheStrategy().isDataCacheable(this.loadData.fetcher.getDataSource()) || this.helper.hasLoadPath(this.loadData.fetcher.getDataClass()))) {
                this.startNextLoad(this.loadData);
            }
        }
        return b;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide.load.engine;

import com.bumptech.glide.load.Transformation;
import com.bumptech.glide.util.pool.GlideTrace;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.Key;
import java.util.List;
import com.bumptech.glide.load.model.ModelLoader;
import java.io.File;
import com.bumptech.glide.load.data.DataFetcher;

class ResourceCacheGenerator implements DataFetcherGenerator, DataCallback<Object>
{
    private File cacheFile;
    private final FetcherReadyCallback cb;
    private ResourceCacheKey currentKey;
    private final DecodeHelper<?> helper;
    private volatile ModelLoader.LoadData<?> loadData;
    private int modelLoaderIndex;
    private List<ModelLoader<File, ?>> modelLoaders;
    private int resourceClassIndex;
    private int sourceIdIndex;
    private Key sourceKey;
    
    ResourceCacheGenerator(final DecodeHelper<?> helper, final FetcherReadyCallback cb) {
        this.resourceClassIndex = -1;
        this.helper = helper;
        this.cb = cb;
    }
    
    private boolean hasNextModelLoader() {
        return this.modelLoaderIndex < this.modelLoaders.size();
    }
    
    @Override
    public void cancel() {
        final ModelLoader.LoadData<?> loadData = this.loadData;
        if (loadData != null) {
            loadData.fetcher.cancel();
        }
    }
    
    @Override
    public void onDataReady(final Object o) {
        this.cb.onDataFetcherReady(this.sourceKey, o, this.loadData.fetcher, DataSource.RESOURCE_DISK_CACHE, this.currentKey);
    }
    
    @Override
    public void onLoadFailed(final Exception ex) {
        this.cb.onDataFetcherFailed(this.currentKey, ex, this.loadData.fetcher, DataSource.RESOURCE_DISK_CACHE);
    }
    
    @Override
    public boolean startNext() {
        GlideTrace.beginSection("ResourceCacheGenerator.startNext");
        try {
            final List<Key> cacheKeys = this.helper.getCacheKeys();
            final boolean empty = cacheKeys.isEmpty();
            boolean b = false;
            if (empty) {
                return false;
            }
            final List<Class<?>> registeredResourceClasses = this.helper.getRegisteredResourceClasses();
            if (!registeredResourceClasses.isEmpty()) {
                while (this.modelLoaders == null || !this.hasNextModelLoader()) {
                    if (++this.resourceClassIndex >= registeredResourceClasses.size()) {
                        if (++this.sourceIdIndex >= cacheKeys.size()) {
                            return false;
                        }
                        this.resourceClassIndex = 0;
                    }
                    final Key sourceKey = cacheKeys.get(this.sourceIdIndex);
                    final Class clazz = registeredResourceClasses.get(this.resourceClassIndex);
                    this.currentKey = new ResourceCacheKey(this.helper.getArrayPool(), sourceKey, this.helper.getSignature(), this.helper.getWidth(), this.helper.getHeight(), this.helper.getTransformation((Class<Object>)clazz), clazz, this.helper.getOptions());
                    final File value = this.helper.getDiskCache().get(this.currentKey);
                    if ((this.cacheFile = value) != null) {
                        this.sourceKey = sourceKey;
                        this.modelLoaders = this.helper.getModelLoaders(value);
                        this.modelLoaderIndex = 0;
                    }
                }
                this.loadData = null;
                while (!b && this.hasNextModelLoader()) {
                    this.loadData = this.modelLoaders.get(this.modelLoaderIndex++).buildLoadData(this.cacheFile, this.helper.getWidth(), this.helper.getHeight(), this.helper.getOptions());
                    if (this.loadData != null && this.helper.hasLoadPath(this.loadData.fetcher.getDataClass())) {
                        this.loadData.fetcher.loadData(this.helper.getPriority(), (DataFetcher.DataCallback<?>)this);
                        b = true;
                    }
                }
                return b;
            }
            if (File.class.equals(this.helper.getTranscodeClass())) {
                return false;
            }
            final StringBuilder sb = new StringBuilder();
            sb.append("Failed to find any load path from ");
            sb.append(this.helper.getModelClass());
            sb.append(" to ");
            sb.append(this.helper.getTranscodeClass());
            throw new IllegalStateException(sb.toString());
        }
        finally {
            GlideTrace.endSection();
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide.load.engine;

import java.util.Collection;
import com.bumptech.glide.util.Executors;
import java.util.ArrayList;
import java.util.List;
import java.util.Iterator;
import com.bumptech.glide.util.Preconditions;
import java.util.concurrent.Executor;
import com.bumptech.glide.request.ResourceCallback;
import com.bumptech.glide.util.pool.StateVerifier;
import androidx.core.util.Pools;
import java.util.concurrent.atomic.AtomicInteger;
import com.bumptech.glide.load.Key;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.executor.GlideExecutor;
import com.bumptech.glide.util.pool.FactoryPools;

class EngineJob<R> implements Callback<R>, Poolable
{
    private static final EngineResourceFactory DEFAULT_FACTORY;
    private final GlideExecutor animationExecutor;
    final ResourceCallbacksAndExecutors cbs;
    DataSource dataSource;
    private DecodeJob<R> decodeJob;
    private final GlideExecutor diskCacheExecutor;
    private final EngineJobListener engineJobListener;
    EngineResource<?> engineResource;
    private final EngineResourceFactory engineResourceFactory;
    GlideException exception;
    private boolean hasLoadFailed;
    private boolean hasResource;
    private boolean isCacheable;
    private volatile boolean isCancelled;
    private boolean isLoadedFromAlternateCacheKey;
    private Key key;
    private boolean onlyRetrieveFromCache;
    private final AtomicInteger pendingCallbacks;
    private final Pools.Pool<EngineJob<?>> pool;
    private Resource<?> resource;
    private final EngineResource.ResourceListener resourceListener;
    private final GlideExecutor sourceExecutor;
    private final GlideExecutor sourceUnlimitedExecutor;
    private final StateVerifier stateVerifier;
    private boolean useAnimationPool;
    private boolean useUnlimitedSourceGeneratorPool;
    
    static {
        DEFAULT_FACTORY = new EngineResourceFactory();
    }
    
    EngineJob(final GlideExecutor glideExecutor, final GlideExecutor glideExecutor2, final GlideExecutor glideExecutor3, final GlideExecutor glideExecutor4, final EngineJobListener engineJobListener, final EngineResource.ResourceListener resourceListener, final Pools.Pool<EngineJob<?>> pool) {
        this(glideExecutor, glideExecutor2, glideExecutor3, glideExecutor4, engineJobListener, resourceListener, pool, EngineJob.DEFAULT_FACTORY);
    }
    
    EngineJob(final GlideExecutor diskCacheExecutor, final GlideExecutor sourceExecutor, final GlideExecutor sourceUnlimitedExecutor, final GlideExecutor animationExecutor, final EngineJobListener engineJobListener, final EngineResource.ResourceListener resourceListener, final Pools.Pool<EngineJob<?>> pool, final EngineResourceFactory engineResourceFactory) {
        this.cbs = new ResourceCallbacksAndExecutors();
        this.stateVerifier = StateVerifier.newInstance();
        this.pendingCallbacks = new AtomicInteger();
        this.diskCacheExecutor = diskCacheExecutor;
        this.sourceExecutor = sourceExecutor;
        this.sourceUnlimitedExecutor = sourceUnlimitedExecutor;
        this.animationExecutor = animationExecutor;
        this.engineJobListener = engineJobListener;
        this.resourceListener = resourceListener;
        this.pool = pool;
        this.engineResourceFactory = engineResourceFactory;
    }
    
    private GlideExecutor getActiveSourceExecutor() {
        GlideExecutor glideExecutor;
        if (this.useUnlimitedSourceGeneratorPool) {
            glideExecutor = this.sourceUnlimitedExecutor;
        }
        else if (this.useAnimationPool) {
            glideExecutor = this.animationExecutor;
        }
        else {
            glideExecutor = this.sourceExecutor;
        }
        return glideExecutor;
    }
    
    private boolean isDone() {
        return this.hasLoadFailed || this.hasResource || this.isCancelled;
    }
    
    private void release() {
        synchronized (this) {
            if (this.key != null) {
                this.cbs.clear();
                this.key = null;
                this.engineResource = null;
                this.resource = null;
                this.hasLoadFailed = false;
                this.isCancelled = false;
                this.hasResource = false;
                this.isLoadedFromAlternateCacheKey = false;
                this.decodeJob.release(false);
                this.decodeJob = null;
                this.exception = null;
                this.dataSource = null;
                this.pool.release(this);
                return;
            }
            throw new IllegalArgumentException();
        }
    }
    
    void addCallback(final ResourceCallback resourceCallback, final Executor executor) {
        synchronized (this) {
            this.stateVerifier.throwIfRecycled();
            this.cbs.add(resourceCallback, executor);
            final boolean hasResource = this.hasResource;
            boolean b = true;
            if (hasResource) {
                this.incrementPendingCallbacks(1);
                executor.execute(new CallResourceReady(resourceCallback));
            }
            else if (this.hasLoadFailed) {
                this.incrementPendingCallbacks(1);
                executor.execute(new CallLoadFailed(resourceCallback));
            }
            else {
                if (this.isCancelled) {
                    b = false;
                }
                Preconditions.checkArgument(b, "Cannot add callbacks to a cancelled EngineJob");
            }
        }
    }
    
    void callCallbackOnLoadFailed(final ResourceCallback resourceCallback) {
        try {
            resourceCallback.onLoadFailed(this.exception);
        }
        finally {
            final Throwable t;
            throw new CallbackException(t);
        }
    }
    
    void callCallbackOnResourceReady(final ResourceCallback resourceCallback) {
        try {
            resourceCallback.onResourceReady(this.engineResource, this.dataSource, this.isLoadedFromAlternateCacheKey);
        }
        finally {
            final Throwable t;
            throw new CallbackException(t);
        }
    }
    
    void cancel() {
        if (this.isDone()) {
            return;
        }
        this.isCancelled = true;
        this.decodeJob.cancel();
        this.engineJobListener.onEngineJobCancelled(this, this.key);
    }
    
    void decrementPendingCallbacks() {
        synchronized (this) {
            this.stateVerifier.throwIfRecycled();
            Preconditions.checkArgument(this.isDone(), "Not yet complete!");
            final int decrementAndGet = this.pendingCallbacks.decrementAndGet();
            Preconditions.checkArgument(decrementAndGet >= 0, "Can't decrement below 0");
            EngineResource<?> engineResource;
            if (decrementAndGet == 0) {
                engineResource = this.engineResource;
                this.release();
            }
            else {
                engineResource = null;
            }
            monitorexit(this);
            if (engineResource != null) {
                engineResource.release();
            }
        }
    }
    
    @Override
    public StateVerifier getVerifier() {
        return this.stateVerifier;
    }
    
    void incrementPendingCallbacks(final int delta) {
        synchronized (this) {
            Preconditions.checkArgument(this.isDone(), "Not yet complete!");
            if (this.pendingCallbacks.getAndAdd(delta) == 0) {
                final EngineResource<?> engineResource = this.engineResource;
                if (engineResource != null) {
                    engineResource.acquire();
                }
            }
        }
    }
    
    EngineJob<R> init(final Key key, final boolean isCacheable, final boolean useUnlimitedSourceGeneratorPool, final boolean useAnimationPool, final boolean onlyRetrieveFromCache) {
        synchronized (this) {
            this.key = key;
            this.isCacheable = isCacheable;
            this.useUnlimitedSourceGeneratorPool = useUnlimitedSourceGeneratorPool;
            this.useAnimationPool = useAnimationPool;
            this.onlyRetrieveFromCache = onlyRetrieveFromCache;
            return this;
        }
    }
    
    boolean isCancelled() {
        synchronized (this) {
            return this.isCancelled;
        }
    }
    
    void notifyCallbacksOfException() {
        synchronized (this) {
            this.stateVerifier.throwIfRecycled();
            if (this.isCancelled) {
                this.release();
                return;
            }
            if (this.cbs.isEmpty()) {
                throw new IllegalStateException("Received an exception without any callbacks to notify");
            }
            if (!this.hasLoadFailed) {
                this.hasLoadFailed = true;
                final Key key = this.key;
                final ResourceCallbacksAndExecutors copy = this.cbs.copy();
                this.incrementPendingCallbacks(copy.size() + 1);
                monitorexit(this);
                this.engineJobListener.onEngineJobComplete(this, key, null);
                for (final ResourceCallbackAndExecutor resourceCallbackAndExecutor : copy) {
                    resourceCallbackAndExecutor.executor.execute(new CallLoadFailed(resourceCallbackAndExecutor.cb));
                }
                this.decrementPendingCallbacks();
                return;
            }
            throw new IllegalStateException("Already failed once");
        }
    }
    
    void notifyCallbacksOfResult() {
        synchronized (this) {
            this.stateVerifier.throwIfRecycled();
            if (this.isCancelled) {
                this.resource.recycle();
                this.release();
                return;
            }
            if (this.cbs.isEmpty()) {
                throw new IllegalStateException("Received a resource without any callbacks to notify");
            }
            if (!this.hasResource) {
                this.engineResource = this.engineResourceFactory.build(this.resource, this.isCacheable, this.key, this.resourceListener);
                this.hasResource = true;
                final ResourceCallbacksAndExecutors copy = this.cbs.copy();
                this.incrementPendingCallbacks(copy.size() + 1);
                final Key key = this.key;
                final EngineResource<?> engineResource = this.engineResource;
                monitorexit(this);
                this.engineJobListener.onEngineJobComplete(this, key, engineResource);
                for (final ResourceCallbackAndExecutor resourceCallbackAndExecutor : copy) {
                    resourceCallbackAndExecutor.executor.execute(new CallResourceReady(resourceCallbackAndExecutor.cb));
                }
                this.decrementPendingCallbacks();
                return;
            }
            throw new IllegalStateException("Already have resource");
        }
    }
    
    @Override
    public void onLoadFailed(final GlideException exception) {
        synchronized (this) {
            this.exception = exception;
            monitorexit(this);
            this.notifyCallbacksOfException();
        }
    }
    
    @Override
    public void onResourceReady(final Resource<R> resource, final DataSource dataSource, final boolean isLoadedFromAlternateCacheKey) {
        synchronized (this) {
            this.resource = resource;
            this.dataSource = dataSource;
            this.isLoadedFromAlternateCacheKey = isLoadedFromAlternateCacheKey;
            monitorexit(this);
            this.notifyCallbacksOfResult();
        }
    }
    
    boolean onlyRetrieveFromCache() {
        return this.onlyRetrieveFromCache;
    }
    
    void removeCallback(final ResourceCallback resourceCallback) {
        synchronized (this) {
            this.stateVerifier.throwIfRecycled();
            this.cbs.remove(resourceCallback);
            if (this.cbs.isEmpty()) {
                this.cancel();
                if ((this.hasResource || this.hasLoadFailed) && this.pendingCallbacks.get() == 0) {
                    this.release();
                }
            }
        }
    }
    
    @Override
    public void reschedule(final DecodeJob<?> decodeJob) {
        this.getActiveSourceExecutor().execute(decodeJob);
    }
    
    public void start(final DecodeJob<R> decodeJob) {
        synchronized (this) {
            this.decodeJob = decodeJob;
            GlideExecutor glideExecutor;
            if (decodeJob.willDecodeFromCache()) {
                glideExecutor = this.diskCacheExecutor;
            }
            else {
                glideExecutor = this.getActiveSourceExecutor();
            }
            glideExecutor.execute(decodeJob);
        }
    }
    
    private class CallLoadFailed implements Runnable
    {
        private final ResourceCallback cb;
        final EngineJob this$0;
        
        CallLoadFailed(final EngineJob this$0, final ResourceCallback cb) {
            this.this$0 = this$0;
            this.cb = cb;
        }
        
        @Override
        public void run() {
            synchronized (this.cb.getLock()) {
                synchronized (this.this$0) {
                    if (this.this$0.cbs.contains(this.cb)) {
                        this.this$0.callCallbackOnLoadFailed(this.cb);
                    }
                    this.this$0.decrementPendingCallbacks();
                }
            }
        }
    }
    
    private class CallResourceReady implements Runnable
    {
        private final ResourceCallback cb;
        final EngineJob this$0;
        
        CallResourceReady(final EngineJob this$0, final ResourceCallback cb) {
            this.this$0 = this$0;
            this.cb = cb;
        }
        
        @Override
        public void run() {
            synchronized (this.cb.getLock()) {
                synchronized (this.this$0) {
                    if (this.this$0.cbs.contains(this.cb)) {
                        this.this$0.engineResource.acquire();
                        this.this$0.callCallbackOnResourceReady(this.cb);
                        this.this$0.removeCallback(this.cb);
                    }
                    this.this$0.decrementPendingCallbacks();
                }
            }
        }
    }
    
    static class EngineResourceFactory
    {
        public <R> EngineResource<R> build(final Resource<R> resource, final boolean b, final Key key, final EngineResource.ResourceListener resourceListener) {
            return new EngineResource<R>(resource, b, true, key, resourceListener);
        }
    }
    
    static final class ResourceCallbackAndExecutor
    {
        final ResourceCallback cb;
        final Executor executor;
        
        ResourceCallbackAndExecutor(final ResourceCallback cb, final Executor executor) {
            this.cb = cb;
            this.executor = executor;
        }
        
        @Override
        public boolean equals(final Object o) {
            return o instanceof ResourceCallbackAndExecutor && this.cb.equals(((ResourceCallbackAndExecutor)o).cb);
        }
        
        @Override
        public int hashCode() {
            return this.cb.hashCode();
        }
    }
    
    static final class ResourceCallbacksAndExecutors implements Iterable<ResourceCallbackAndExecutor>
    {
        private final List<ResourceCallbackAndExecutor> callbacksAndExecutors;
        
        ResourceCallbacksAndExecutors() {
            this(new ArrayList<ResourceCallbackAndExecutor>(2));
        }
        
        ResourceCallbacksAndExecutors(final List<ResourceCallbackAndExecutor> callbacksAndExecutors) {
            this.callbacksAndExecutors = callbacksAndExecutors;
        }
        
        private static ResourceCallbackAndExecutor defaultCallbackAndExecutor(final ResourceCallback resourceCallback) {
            return new ResourceCallbackAndExecutor(resourceCallback, Executors.directExecutor());
        }
        
        void add(final ResourceCallback resourceCallback, final Executor executor) {
            this.callbacksAndExecutors.add(new ResourceCallbackAndExecutor(resourceCallback, executor));
        }
        
        void clear() {
            this.callbacksAndExecutors.clear();
        }
        
        boolean contains(final ResourceCallback resourceCallback) {
            return this.callbacksAndExecutors.contains(defaultCallbackAndExecutor(resourceCallback));
        }
        
        ResourceCallbacksAndExecutors copy() {
            return new ResourceCallbacksAndExecutors(new ArrayList<ResourceCallbackAndExecutor>(this.callbacksAndExecutors));
        }
        
        boolean isEmpty() {
            return this.callbacksAndExecutors.isEmpty();
        }
        
        @Override
        public Iterator<ResourceCallbackAndExecutor> iterator() {
            return this.callbacksAndExecutors.iterator();
        }
        
        void remove(final ResourceCallback resourceCallback) {
            this.callbacksAndExecutors.remove(defaultCallbackAndExecutor(resourceCallback));
        }
        
        int size() {
            return this.callbacksAndExecutors.size();
        }
    }
}

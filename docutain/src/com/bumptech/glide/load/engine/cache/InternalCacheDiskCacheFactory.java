// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide.load.engine.cache;

import java.io.File;
import android.content.Context;

public final class InternalCacheDiskCacheFactory extends DiskLruCacheFactory
{
    public InternalCacheDiskCacheFactory(final Context context) {
        this(context, "image_manager_disk_cache", 262144000L);
    }
    
    public InternalCacheDiskCacheFactory(final Context context, final long n) {
        this(context, "image_manager_disk_cache", n);
    }
    
    public InternalCacheDiskCacheFactory(final Context context, final String s, final long n) {
        super((CacheDirectoryGetter)new CacheDirectoryGetter(context, s) {
            final Context val$context;
            final String val$diskCacheName;
            
            @Override
            public File getCacheDirectory() {
                final File cacheDir = this.val$context.getCacheDir();
                if (cacheDir == null) {
                    return null;
                }
                if (this.val$diskCacheName != null) {
                    return new File(cacheDir, this.val$diskCacheName);
                }
                return cacheDir;
            }
        }, n);
    }
}

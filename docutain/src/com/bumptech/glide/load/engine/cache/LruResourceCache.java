// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide.load.engine.cache;

import com.bumptech.glide.load.engine.Resource;
import com.bumptech.glide.load.Key;
import com.bumptech.glide.util.LruCache;

public class LruResourceCache extends LruCache<Key, Resource<?>> implements MemoryCache
{
    private ResourceRemovedListener listener;
    
    public LruResourceCache(final long n) {
        super(n);
    }
    
    @Override
    protected int getSize(final Resource<?> resource) {
        if (resource == null) {
            return super.getSize(null);
        }
        return resource.getSize();
    }
    
    @Override
    protected void onItemEvicted(final Key key, final Resource<?> resource) {
        final ResourceRemovedListener listener = this.listener;
        if (listener != null && resource != null) {
            listener.onResourceRemoved(resource);
        }
    }
    
    @Override
    public void setResourceRemovedListener(final ResourceRemovedListener listener) {
        this.listener = listener;
    }
    
    @Override
    public void trimMemory(final int n) {
        if (n >= 40) {
            this.clearMemory();
        }
        else if (n >= 20 || n == 15) {
            this.trimToSize(this.getMaxSize() / 2L);
        }
    }
}

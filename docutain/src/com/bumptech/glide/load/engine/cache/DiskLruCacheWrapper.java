// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide.load.engine.cache;

import android.util.Log;
import com.bumptech.glide.load.Key;
import java.io.IOException;
import com.bumptech.glide.disklrucache.DiskLruCache;
import java.io.File;

public class DiskLruCacheWrapper implements DiskCache
{
    private static final int APP_VERSION = 1;
    private static final String TAG = "DiskLruCacheWrapper";
    private static final int VALUE_COUNT = 1;
    private static DiskLruCacheWrapper wrapper;
    private final File directory;
    private DiskLruCache diskLruCache;
    private final long maxSize;
    private final SafeKeyGenerator safeKeyGenerator;
    private final DiskCacheWriteLocker writeLocker;
    
    @Deprecated
    protected DiskLruCacheWrapper(final File directory, final long maxSize) {
        this.writeLocker = new DiskCacheWriteLocker();
        this.directory = directory;
        this.maxSize = maxSize;
        this.safeKeyGenerator = new SafeKeyGenerator();
    }
    
    public static DiskCache create(final File file, final long n) {
        return new DiskLruCacheWrapper(file, n);
    }
    
    @Deprecated
    public static DiskCache get(final File file, final long n) {
        synchronized (DiskLruCacheWrapper.class) {
            if (DiskLruCacheWrapper.wrapper == null) {
                DiskLruCacheWrapper.wrapper = new DiskLruCacheWrapper(file, n);
            }
            return DiskLruCacheWrapper.wrapper;
        }
    }
    
    private DiskLruCache getDiskCache() throws IOException {
        synchronized (this) {
            if (this.diskLruCache == null) {
                this.diskLruCache = DiskLruCache.open(this.directory, 1, 1, this.maxSize);
            }
            return this.diskLruCache;
        }
    }
    
    private void resetDiskCache() {
        synchronized (this) {
            this.diskLruCache = null;
        }
    }
    
    @Override
    public void clear() {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: monitorenter   
        //     2: aload_0        
        //     3: invokespecial   com/bumptech/glide/load/engine/cache/DiskLruCacheWrapper.getDiskCache:()Lcom/bumptech/glide/disklrucache/DiskLruCache;
        //     6: invokevirtual   com/bumptech/glide/disklrucache/DiskLruCache.delete:()V
        //     9: aload_0        
        //    10: invokespecial   com/bumptech/glide/load/engine/cache/DiskLruCacheWrapper.resetDiskCache:()V
        //    13: goto            42
        //    16: astore_1       
        //    17: goto            45
        //    20: astore_1       
        //    21: ldc             "DiskLruCacheWrapper"
        //    23: iconst_5       
        //    24: invokestatic    android/util/Log.isLoggable:(Ljava/lang/String;I)Z
        //    27: ifeq            9
        //    30: ldc             "DiskLruCacheWrapper"
        //    32: ldc             "Unable to clear disk cache or disk cache cleared externally"
        //    34: aload_1        
        //    35: invokestatic    android/util/Log.w:(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //    38: pop            
        //    39: goto            9
        //    42: aload_0        
        //    43: monitorexit    
        //    44: return         
        //    45: aload_0        
        //    46: invokespecial   com/bumptech/glide/load/engine/cache/DiskLruCacheWrapper.resetDiskCache:()V
        //    49: aload_1        
        //    50: athrow         
        //    51: astore_1       
        //    52: aload_0        
        //    53: monitorexit    
        //    54: aload_1        
        //    55: athrow         
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                 
        //  -----  -----  -----  -----  ---------------------
        //  2      9      20     42     Ljava/io/IOException;
        //  2      9      16     20     Any
        //  9      13     51     56     Any
        //  21     39     16     20     Any
        //  45     51     51     56     Any
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0009:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2604)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:206)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    @Override
    public void delete(final Key key) {
        final String safeKey = this.safeKeyGenerator.getSafeKey(key);
        try {
            this.getDiskCache().remove(safeKey);
        }
        catch (final IOException ex) {
            if (Log.isLoggable("DiskLruCacheWrapper", 5)) {
                Log.w("DiskLruCacheWrapper", "Unable to delete from disk cache", (Throwable)ex);
            }
        }
    }
    
    @Override
    public File get(final Key obj) {
        final String safeKey = this.safeKeyGenerator.getSafeKey(obj);
        if (Log.isLoggable("DiskLruCacheWrapper", 2)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Get: Obtained: ");
            sb.append(safeKey);
            sb.append(" for for Key: ");
            sb.append(obj);
            Log.v("DiskLruCacheWrapper", sb.toString());
        }
        final File file = null;
        File file2;
        try {
            final DiskLruCache.Value value = this.getDiskCache().get(safeKey);
            file2 = file;
            if (value != null) {
                file2 = value.getFile(0);
            }
        }
        catch (final IOException ex) {
            file2 = file;
            if (Log.isLoggable("DiskLruCacheWrapper", 5)) {
                Log.w("DiskLruCacheWrapper", "Unable to get from disk cache", (Throwable)ex);
                file2 = file;
            }
        }
        return file2;
    }
    
    @Override
    public void put(Key edit, final Writer writer) {
        final String safeKey = this.safeKeyGenerator.getSafeKey(edit);
        this.writeLocker.acquire(safeKey);
        try {
            if (Log.isLoggable("DiskLruCacheWrapper", 2)) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Put: Obtained: ");
                sb.append(safeKey);
                sb.append(" for for Key: ");
                sb.append(edit);
                Log.v("DiskLruCacheWrapper", sb.toString());
            }
            try {
                final DiskLruCache diskCache = this.getDiskCache();
                if (diskCache.get(safeKey) != null) {
                    return;
                }
                edit = (Key)diskCache.edit(safeKey);
                if (edit != null) {
                    try {
                        if (writer.write(((DiskLruCache.Editor)edit).getFile(0))) {
                            ((DiskLruCache.Editor)edit).commit();
                        }
                        return;
                    }
                    finally {
                        ((DiskLruCache.Editor)edit).abortUnlessCommitted();
                    }
                }
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("Had two simultaneous puts for: ");
                sb2.append(safeKey);
                throw new IllegalStateException(sb2.toString());
            }
            catch (final IOException ex) {
                if (Log.isLoggable("DiskLruCacheWrapper", 5)) {
                    Log.w("DiskLruCacheWrapper", "Unable to put to disk cache", (Throwable)ex);
                }
            }
        }
        finally {
            this.writeLocker.release(safeKey);
        }
    }
}

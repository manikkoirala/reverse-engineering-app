// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide.load.engine.cache;

import com.bumptech.glide.load.engine.Resource;
import com.bumptech.glide.load.Key;

public class MemoryCacheAdapter implements MemoryCache
{
    private ResourceRemovedListener listener;
    
    @Override
    public void clearMemory() {
    }
    
    @Override
    public long getCurrentSize() {
        return 0L;
    }
    
    @Override
    public long getMaxSize() {
        return 0L;
    }
    
    @Override
    public Resource<?> put(final Key key, final Resource<?> resource) {
        if (resource != null) {
            this.listener.onResourceRemoved(resource);
        }
        return null;
    }
    
    @Override
    public Resource<?> remove(final Key key) {
        return null;
    }
    
    @Override
    public void setResourceRemovedListener(final ResourceRemovedListener listener) {
        this.listener = listener;
    }
    
    @Override
    public void setSizeMultiplier(final float n) {
    }
    
    @Override
    public void trimMemory(final int n) {
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide.load.engine.cache;

import java.io.File;
import android.content.Context;

@Deprecated
public final class ExternalCacheDiskCacheFactory extends DiskLruCacheFactory
{
    public ExternalCacheDiskCacheFactory(final Context context) {
        this(context, "image_manager_disk_cache", 262144000);
    }
    
    public ExternalCacheDiskCacheFactory(final Context context, final int n) {
        this(context, "image_manager_disk_cache", n);
    }
    
    public ExternalCacheDiskCacheFactory(final Context context, final String s, final int n) {
        super((CacheDirectoryGetter)new CacheDirectoryGetter(context, s) {
            final Context val$context;
            final String val$diskCacheName;
            
            @Override
            public File getCacheDirectory() {
                final File externalCacheDir = this.val$context.getExternalCacheDir();
                if (externalCacheDir == null) {
                    return null;
                }
                if (this.val$diskCacheName != null) {
                    return new File(externalCacheDir, this.val$diskCacheName);
                }
                return externalCacheDir;
            }
        }, n);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide.load.engine.cache;

import java.io.File;
import com.bumptech.glide.load.Key;

public class DiskCacheAdapter implements DiskCache
{
    @Override
    public void clear() {
    }
    
    @Override
    public void delete(final Key key) {
    }
    
    @Override
    public File get(final Key key) {
        return null;
    }
    
    @Override
    public void put(final Key key, final Writer writer) {
    }
    
    public static final class Factory implements DiskCache.Factory
    {
        @Override
        public DiskCache build() {
            return new DiskCacheAdapter();
        }
    }
}

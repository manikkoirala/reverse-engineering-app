// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide.load.engine.cache;

import java.io.File;

public class DiskLruCacheFactory implements Factory
{
    private final CacheDirectoryGetter cacheDirectoryGetter;
    private final long diskCacheSize;
    
    public DiskLruCacheFactory(final CacheDirectoryGetter cacheDirectoryGetter, final long diskCacheSize) {
        this.diskCacheSize = diskCacheSize;
        this.cacheDirectoryGetter = cacheDirectoryGetter;
    }
    
    public DiskLruCacheFactory(final String s, final long n) {
        this((CacheDirectoryGetter)new CacheDirectoryGetter(s) {
            final String val$diskCacheFolder;
            
            @Override
            public File getCacheDirectory() {
                return new File(this.val$diskCacheFolder);
            }
        }, n);
    }
    
    public DiskLruCacheFactory(final String s, final String s2, final long n) {
        this((CacheDirectoryGetter)new CacheDirectoryGetter(s, s2) {
            final String val$diskCacheFolder;
            final String val$diskCacheName;
            
            @Override
            public File getCacheDirectory() {
                return new File(this.val$diskCacheFolder, this.val$diskCacheName);
            }
        }, n);
    }
    
    @Override
    public DiskCache build() {
        final File cacheDirectory = this.cacheDirectoryGetter.getCacheDirectory();
        if (cacheDirectory == null) {
            return null;
        }
        if (!cacheDirectory.isDirectory() && !cacheDirectory.mkdirs()) {
            return null;
        }
        return DiskLruCacheWrapper.create(cacheDirectory, this.diskCacheSize);
    }
    
    public interface CacheDirectoryGetter
    {
        File getCacheDirectory();
    }
}

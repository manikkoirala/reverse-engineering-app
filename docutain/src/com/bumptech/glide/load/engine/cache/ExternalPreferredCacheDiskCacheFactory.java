// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide.load.engine.cache;

import java.io.File;
import android.content.Context;

public final class ExternalPreferredCacheDiskCacheFactory extends DiskLruCacheFactory
{
    public ExternalPreferredCacheDiskCacheFactory(final Context context) {
        this(context, "image_manager_disk_cache", 262144000L);
    }
    
    public ExternalPreferredCacheDiskCacheFactory(final Context context, final long n) {
        this(context, "image_manager_disk_cache", n);
    }
    
    public ExternalPreferredCacheDiskCacheFactory(final Context context, final String s, final long n) {
        super((CacheDirectoryGetter)new CacheDirectoryGetter(context, s) {
            final Context val$context;
            final String val$diskCacheName;
            
            private File getInternalCacheDirectory() {
                final File cacheDir = this.val$context.getCacheDir();
                if (cacheDir == null) {
                    return null;
                }
                if (this.val$diskCacheName != null) {
                    return new File(cacheDir, this.val$diskCacheName);
                }
                return cacheDir;
            }
            
            @Override
            public File getCacheDirectory() {
                final File internalCacheDirectory = this.getInternalCacheDirectory();
                if (internalCacheDirectory != null && internalCacheDirectory.exists()) {
                    return internalCacheDirectory;
                }
                final File externalCacheDir = this.val$context.getExternalCacheDir();
                if (externalCacheDir == null || !externalCacheDir.canWrite()) {
                    return internalCacheDirectory;
                }
                if (this.val$diskCacheName != null) {
                    return new File(externalCacheDir, this.val$diskCacheName);
                }
                return externalCacheDir;
            }
        }, n);
    }
}

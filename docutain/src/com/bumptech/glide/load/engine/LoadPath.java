// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide.load.engine;

import java.util.Arrays;
import java.util.Collection;
import java.util.ArrayList;
import com.bumptech.glide.load.Options;
import com.bumptech.glide.load.data.DataRewinder;
import com.bumptech.glide.util.Preconditions;
import androidx.core.util.Pools;
import java.util.List;

public class LoadPath<Data, ResourceType, Transcode>
{
    private final Class<Data> dataClass;
    private final List<? extends DecodePath<Data, ResourceType, Transcode>> decodePaths;
    private final String failureMessage;
    private final Pools.Pool<List<Throwable>> listPool;
    
    public LoadPath(final Class<Data> dataClass, final Class<ResourceType> clazz, final Class<Transcode> clazz2, final List<DecodePath<Data, ResourceType, Transcode>> list, final Pools.Pool<List<Throwable>> listPool) {
        this.dataClass = dataClass;
        this.listPool = listPool;
        this.decodePaths = Preconditions.checkNotEmpty(list);
        final StringBuilder sb = new StringBuilder();
        sb.append("Failed LoadPath{");
        sb.append(dataClass.getSimpleName());
        sb.append("->");
        sb.append(clazz.getSimpleName());
        sb.append("->");
        sb.append(clazz2.getSimpleName());
        sb.append("}");
        this.failureMessage = sb.toString();
    }
    
    private Resource<Transcode> loadWithExceptionList(final DataRewinder<Data> dataRewinder, final Options options, final int n, final int n2, final DecodePath.DecodeCallback<ResourceType> decodeCallback, final List<Throwable> c) throws GlideException {
        final int size = this.decodePaths.size();
        Resource<Object> decode = null;
        int n3 = 0;
        Resource<Object> resource;
        while (true) {
            resource = decode;
            if (n3 >= size) {
                break;
            }
            final DecodePath decodePath = (DecodePath)this.decodePaths.get(n3);
            try {
                decode = decodePath.decode(dataRewinder, n, n2, options, (DecodePath.DecodeCallback<Object>)decodeCallback);
            }
            catch (final GlideException ex) {
                c.add(ex);
            }
            if (decode != null) {
                resource = decode;
                break;
            }
            ++n3;
        }
        if (resource != null) {
            return (Resource<Transcode>)resource;
        }
        throw new GlideException(this.failureMessage, new ArrayList<Throwable>(c));
    }
    
    public Class<Data> getDataClass() {
        return this.dataClass;
    }
    
    public Resource<Transcode> load(final DataRewinder<Data> dataRewinder, final Options options, final int n, final int n2, final DecodePath.DecodeCallback<ResourceType> decodeCallback) throws GlideException {
        final List<Throwable> list = Preconditions.checkNotNull(this.listPool.acquire());
        try {
            return this.loadWithExceptionList(dataRewinder, options, n, n2, decodeCallback, list);
        }
        finally {
            this.listPool.release(list);
        }
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("LoadPath{decodePaths=");
        sb.append(Arrays.toString(this.decodePaths.toArray()));
        sb.append('}');
        return sb.toString();
    }
}

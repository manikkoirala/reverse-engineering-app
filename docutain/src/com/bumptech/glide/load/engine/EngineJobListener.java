// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide.load.engine;

import com.bumptech.glide.load.Key;

interface EngineJobListener
{
    void onEngineJobCancelled(final EngineJob<?> p0, final Key p1);
    
    void onEngineJobComplete(final EngineJob<?> p0, final Key p1, final EngineResource<?> p2);
}

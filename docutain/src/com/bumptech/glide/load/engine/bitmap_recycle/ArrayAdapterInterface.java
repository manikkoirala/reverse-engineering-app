// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide.load.engine.bitmap_recycle;

interface ArrayAdapterInterface<T>
{
    int getArrayLength(final T p0);
    
    int getElementSizeInBytes();
    
    String getTag();
    
    T newArray(final int p0);
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide.load.engine.bitmap_recycle;

import java.util.Collections;
import java.util.Collection;
import java.util.HashSet;
import java.util.Arrays;
import android.util.Log;
import android.graphics.Bitmap;
import android.os.Build$VERSION;
import java.util.Set;
import android.graphics.Bitmap$Config;

public class LruBitmapPool implements BitmapPool
{
    private static final Bitmap$Config DEFAULT_CONFIG;
    private static final String TAG = "LruBitmapPool";
    private final Set<Bitmap$Config> allowedConfigs;
    private long currentSize;
    private int evictions;
    private int hits;
    private final long initialMaxSize;
    private long maxSize;
    private int misses;
    private int puts;
    private final LruPoolStrategy strategy;
    private final BitmapTracker tracker;
    
    static {
        DEFAULT_CONFIG = Bitmap$Config.ARGB_8888;
    }
    
    public LruBitmapPool(final long n) {
        this(n, getDefaultStrategy(), getDefaultAllowedConfigs());
    }
    
    LruBitmapPool(final long n, final LruPoolStrategy strategy, final Set<Bitmap$Config> allowedConfigs) {
        this.initialMaxSize = n;
        this.maxSize = n;
        this.strategy = strategy;
        this.allowedConfigs = allowedConfigs;
        this.tracker = (BitmapTracker)new NullBitmapTracker();
    }
    
    public LruBitmapPool(final long n, final Set<Bitmap$Config> set) {
        this(n, getDefaultStrategy(), set);
    }
    
    private static void assertNotHardwareConfig(final Bitmap$Config obj) {
        if (Build$VERSION.SDK_INT < 26) {
            return;
        }
        if (obj != Bitmap$Config.HARDWARE) {
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Cannot create a mutable Bitmap with config: ");
        sb.append(obj);
        sb.append(". Consider setting Downsampler#ALLOW_HARDWARE_CONFIG to false in your RequestOptions and/or in GlideBuilder.setDefaultRequestOptions");
        throw new IllegalArgumentException(sb.toString());
    }
    
    private static Bitmap createBitmap(final int n, final int n2, Bitmap$Config default_CONFIG) {
        if (default_CONFIG == null) {
            default_CONFIG = LruBitmapPool.DEFAULT_CONFIG;
        }
        return Bitmap.createBitmap(n, n2, default_CONFIG);
    }
    
    private void dump() {
        if (Log.isLoggable("LruBitmapPool", 2)) {
            this.dumpUnchecked();
        }
    }
    
    private void dumpUnchecked() {
        final StringBuilder sb = new StringBuilder();
        sb.append("Hits=");
        sb.append(this.hits);
        sb.append(", misses=");
        sb.append(this.misses);
        sb.append(", puts=");
        sb.append(this.puts);
        sb.append(", evictions=");
        sb.append(this.evictions);
        sb.append(", currentSize=");
        sb.append(this.currentSize);
        sb.append(", maxSize=");
        sb.append(this.maxSize);
        sb.append("\nStrategy=");
        sb.append(this.strategy);
        Log.v("LruBitmapPool", sb.toString());
    }
    
    private void evict() {
        this.trimToSize(this.maxSize);
    }
    
    private static Set<Bitmap$Config> getDefaultAllowedConfigs() {
        final HashSet s = new HashSet((Collection<? extends E>)Arrays.asList(Bitmap$Config.values()));
        if (Build$VERSION.SDK_INT >= 19) {
            s.add(null);
        }
        if (Build$VERSION.SDK_INT >= 26) {
            s.remove(Bitmap$Config.HARDWARE);
        }
        return (Set<Bitmap$Config>)Collections.unmodifiableSet((Set<?>)s);
    }
    
    private static LruPoolStrategy getDefaultStrategy() {
        LruPoolStrategy lruPoolStrategy;
        if (Build$VERSION.SDK_INT >= 19) {
            lruPoolStrategy = new SizeConfigStrategy();
        }
        else {
            lruPoolStrategy = new AttributeStrategy();
        }
        return lruPoolStrategy;
    }
    
    private Bitmap getDirtyOrNull(final int n, final int n2, final Bitmap$Config bitmap$Config) {
        synchronized (this) {
            assertNotHardwareConfig(bitmap$Config);
            final LruPoolStrategy strategy = this.strategy;
            Bitmap$Config default_CONFIG;
            if (bitmap$Config != null) {
                default_CONFIG = bitmap$Config;
            }
            else {
                default_CONFIG = LruBitmapPool.DEFAULT_CONFIG;
            }
            final Bitmap value = strategy.get(n, n2, default_CONFIG);
            if (value == null) {
                if (Log.isLoggable("LruBitmapPool", 3)) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Missing bitmap=");
                    sb.append(this.strategy.logBitmap(n, n2, bitmap$Config));
                    Log.d("LruBitmapPool", sb.toString());
                }
                ++this.misses;
            }
            else {
                ++this.hits;
                this.currentSize -= this.strategy.getSize(value);
                this.tracker.remove(value);
                normalize(value);
            }
            if (Log.isLoggable("LruBitmapPool", 2)) {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("Get bitmap=");
                sb2.append(this.strategy.logBitmap(n, n2, bitmap$Config));
                Log.v("LruBitmapPool", sb2.toString());
            }
            this.dump();
            return value;
        }
    }
    
    private static void maybeSetPreMultiplied(final Bitmap bitmap) {
        if (Build$VERSION.SDK_INT >= 19) {
            bitmap.setPremultiplied(true);
        }
    }
    
    private static void normalize(final Bitmap bitmap) {
        bitmap.setHasAlpha(true);
        maybeSetPreMultiplied(bitmap);
    }
    
    private void trimToSize(final long n) {
        synchronized (this) {
            while (this.currentSize > n) {
                final Bitmap removeLast = this.strategy.removeLast();
                if (removeLast == null) {
                    if (Log.isLoggable("LruBitmapPool", 5)) {
                        Log.w("LruBitmapPool", "Size mismatch, resetting");
                        this.dumpUnchecked();
                    }
                    this.currentSize = 0L;
                    return;
                }
                this.tracker.remove(removeLast);
                this.currentSize -= this.strategy.getSize(removeLast);
                ++this.evictions;
                if (Log.isLoggable("LruBitmapPool", 3)) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Evicting bitmap=");
                    sb.append(this.strategy.logBitmap(removeLast));
                    Log.d("LruBitmapPool", sb.toString());
                }
                this.dump();
                removeLast.recycle();
            }
        }
    }
    
    @Override
    public void clearMemory() {
        if (Log.isLoggable("LruBitmapPool", 3)) {
            Log.d("LruBitmapPool", "clearMemory");
        }
        this.trimToSize(0L);
    }
    
    public long evictionCount() {
        return this.evictions;
    }
    
    @Override
    public Bitmap get(final int n, final int n2, final Bitmap$Config bitmap$Config) {
        final Bitmap dirtyOrNull = this.getDirtyOrNull(n, n2, bitmap$Config);
        Bitmap bitmap;
        if (dirtyOrNull != null) {
            dirtyOrNull.eraseColor(0);
            bitmap = dirtyOrNull;
        }
        else {
            bitmap = createBitmap(n, n2, bitmap$Config);
        }
        return bitmap;
    }
    
    public long getCurrentSize() {
        return this.currentSize;
    }
    
    @Override
    public Bitmap getDirty(final int n, final int n2, final Bitmap$Config bitmap$Config) {
        Bitmap bitmap;
        if ((bitmap = this.getDirtyOrNull(n, n2, bitmap$Config)) == null) {
            bitmap = createBitmap(n, n2, bitmap$Config);
        }
        return bitmap;
    }
    
    @Override
    public long getMaxSize() {
        return this.maxSize;
    }
    
    public long hitCount() {
        return this.hits;
    }
    
    public long missCount() {
        return this.misses;
    }
    
    @Override
    public void put(final Bitmap bitmap) {
        monitorenter(this);
        if (bitmap != null) {
            Label_0301: {
                try {
                    if (bitmap.isRecycled()) {
                        throw new IllegalStateException("Cannot pool recycled bitmap");
                    }
                    if (bitmap.isMutable() && this.strategy.getSize(bitmap) <= this.maxSize && this.allowedConfigs.contains(bitmap.getConfig())) {
                        final int size = this.strategy.getSize(bitmap);
                        this.strategy.put(bitmap);
                        this.tracker.add(bitmap);
                        ++this.puts;
                        this.currentSize += size;
                        if (Log.isLoggable("LruBitmapPool", 2)) {
                            final StringBuilder sb = new StringBuilder();
                            sb.append("Put bitmap in pool=");
                            sb.append(this.strategy.logBitmap(bitmap));
                            Log.v("LruBitmapPool", sb.toString());
                        }
                        this.dump();
                        this.evict();
                        monitorexit(this);
                        return;
                    }
                    if (Log.isLoggable("LruBitmapPool", 2)) {
                        final StringBuilder sb2 = new StringBuilder();
                        sb2.append("Reject bitmap from pool, bitmap: ");
                        sb2.append(this.strategy.logBitmap(bitmap));
                        sb2.append(", is mutable: ");
                        sb2.append(bitmap.isMutable());
                        sb2.append(", is allowed config: ");
                        sb2.append(this.allowedConfigs.contains(bitmap.getConfig()));
                        Log.v("LruBitmapPool", sb2.toString());
                    }
                    bitmap.recycle();
                    monitorexit(this);
                    return;
                }
                finally {
                    break Label_0301;
                }
                throw new NullPointerException("Bitmap must not be null");
            }
            monitorexit(this);
        }
        throw new NullPointerException("Bitmap must not be null");
    }
    
    @Override
    public void setSizeMultiplier(final float n) {
        synchronized (this) {
            this.maxSize = Math.round(this.initialMaxSize * n);
            this.evict();
        }
    }
    
    @Override
    public void trimMemory(final int i) {
        if (Log.isLoggable("LruBitmapPool", 3)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("trimMemory, level=");
            sb.append(i);
            Log.d("LruBitmapPool", sb.toString());
        }
        if (i < 40 && (Build$VERSION.SDK_INT < 23 || i < 20)) {
            if (i >= 20 || i == 15) {
                this.trimToSize(this.getMaxSize() / 2L);
            }
        }
        else {
            this.clearMemory();
        }
    }
    
    private interface BitmapTracker
    {
        void add(final Bitmap p0);
        
        void remove(final Bitmap p0);
    }
    
    private static final class NullBitmapTracker implements BitmapTracker
    {
        NullBitmapTracker() {
        }
        
        @Override
        public void add(final Bitmap bitmap) {
        }
        
        @Override
        public void remove(final Bitmap bitmap) {
        }
    }
    
    private static class ThrowingBitmapTracker implements BitmapTracker
    {
        private final Set<Bitmap> bitmaps;
        
        private ThrowingBitmapTracker() {
            this.bitmaps = Collections.synchronizedSet(new HashSet<Bitmap>());
        }
        
        @Override
        public void add(final Bitmap obj) {
            if (!this.bitmaps.contains(obj)) {
                this.bitmaps.add(obj);
                return;
            }
            final StringBuilder sb = new StringBuilder();
            sb.append("Can't add already added bitmap: ");
            sb.append(obj);
            sb.append(" [");
            sb.append(obj.getWidth());
            sb.append("x");
            sb.append(obj.getHeight());
            sb.append("]");
            throw new IllegalStateException(sb.toString());
        }
        
        @Override
        public void remove(final Bitmap bitmap) {
            if (this.bitmaps.contains(bitmap)) {
                this.bitmaps.remove(bitmap);
                return;
            }
            throw new IllegalStateException("Cannot remove bitmap not in tracker");
        }
    }
}

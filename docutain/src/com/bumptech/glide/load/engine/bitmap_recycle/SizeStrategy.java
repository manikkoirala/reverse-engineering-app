// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide.load.engine.bitmap_recycle;

import android.graphics.Bitmap$Config;
import com.bumptech.glide.util.Util;
import java.util.NavigableMap;
import android.graphics.Bitmap;

final class SizeStrategy implements LruPoolStrategy
{
    private static final int MAX_SIZE_MULTIPLE = 8;
    private final GroupedLinkedMap<Key, Bitmap> groupedMap;
    private final KeyPool keyPool;
    private final NavigableMap<Integer, Integer> sortedSizes;
    
    SizeStrategy() {
        this.keyPool = new KeyPool();
        this.groupedMap = new GroupedLinkedMap<Key, Bitmap>();
        this.sortedSizes = new PrettyPrintTreeMap<Integer, Integer>();
    }
    
    private void decrementBitmapOfSize(final Integer n) {
        final Integer n2 = this.sortedSizes.get(n);
        if (n2 == 1) {
            this.sortedSizes.remove(n);
        }
        else {
            this.sortedSizes.put(n, n2 - 1);
        }
    }
    
    static String getBitmapString(final int i) {
        final StringBuilder sb = new StringBuilder();
        sb.append("[");
        sb.append(i);
        sb.append("]");
        return sb.toString();
    }
    
    private static String getBitmapString(final Bitmap bitmap) {
        return getBitmapString(Util.getBitmapByteSize(bitmap));
    }
    
    @Override
    public Bitmap get(final int n, final int n2, final Bitmap$Config bitmap$Config) {
        final int bitmapByteSize = Util.getBitmapByteSize(n, n2, bitmap$Config);
        final Key value = this.keyPool.get(bitmapByteSize);
        final Integer n3 = this.sortedSizes.ceilingKey(bitmapByteSize);
        Poolable value2 = value;
        if (n3 != null) {
            value2 = value;
            if (n3 != bitmapByteSize) {
                value2 = value;
                if (n3 <= bitmapByteSize * 8) {
                    this.keyPool.offer(value);
                    value2 = this.keyPool.get(n3);
                }
            }
        }
        final Bitmap bitmap = this.groupedMap.get((Key)value2);
        if (bitmap != null) {
            bitmap.reconfigure(n, n2, bitmap$Config);
            this.decrementBitmapOfSize(n3);
        }
        return bitmap;
    }
    
    @Override
    public int getSize(final Bitmap bitmap) {
        return Util.getBitmapByteSize(bitmap);
    }
    
    @Override
    public String logBitmap(final int n, final int n2, final Bitmap$Config bitmap$Config) {
        return getBitmapString(Util.getBitmapByteSize(n, n2, bitmap$Config));
    }
    
    @Override
    public String logBitmap(final Bitmap bitmap) {
        return getBitmapString(bitmap);
    }
    
    @Override
    public void put(final Bitmap bitmap) {
        final Key value = this.keyPool.get(Util.getBitmapByteSize(bitmap));
        this.groupedMap.put(value, bitmap);
        final Integer n = this.sortedSizes.get(value.size);
        final NavigableMap<Integer, Integer> sortedSizes = this.sortedSizes;
        final int size = value.size;
        int i = 1;
        if (n != null) {
            i = 1 + n;
        }
        sortedSizes.put(size, i);
    }
    
    @Override
    public Bitmap removeLast() {
        final Bitmap bitmap = this.groupedMap.removeLast();
        if (bitmap != null) {
            this.decrementBitmapOfSize(Util.getBitmapByteSize(bitmap));
        }
        return bitmap;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("SizeStrategy:\n  ");
        sb.append(this.groupedMap);
        sb.append("\n  SortedSizes");
        sb.append(this.sortedSizes);
        return sb.toString();
    }
    
    static final class Key implements Poolable
    {
        private final KeyPool pool;
        int size;
        
        Key(final KeyPool pool) {
            this.pool = pool;
        }
        
        @Override
        public boolean equals(final Object o) {
            final boolean b = o instanceof Key;
            boolean b2 = false;
            if (b) {
                final Key key = (Key)o;
                b2 = b2;
                if (this.size == key.size) {
                    b2 = true;
                }
            }
            return b2;
        }
        
        @Override
        public int hashCode() {
            return this.size;
        }
        
        public void init(final int size) {
            this.size = size;
        }
        
        @Override
        public void offer() {
            this.pool.offer(this);
        }
        
        @Override
        public String toString() {
            return SizeStrategy.getBitmapString(this.size);
        }
    }
    
    static class KeyPool extends BaseKeyPool<Key>
    {
        protected Key create() {
            return new Key(this);
        }
        
        public Key get(final int n) {
            final Key key = super.get();
            key.init(n);
            return key;
        }
    }
}

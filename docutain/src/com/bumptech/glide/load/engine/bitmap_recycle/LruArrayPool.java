// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide.load.engine.bitmap_recycle;

import java.util.Iterator;
import java.util.TreeMap;
import android.util.Log;
import com.bumptech.glide.util.Preconditions;
import java.util.HashMap;
import java.util.NavigableMap;
import java.util.Map;

public final class LruArrayPool implements ArrayPool
{
    private static final int DEFAULT_SIZE = 4194304;
    static final int MAX_OVER_SIZE_MULTIPLE = 8;
    private static final int SINGLE_ARRAY_MAX_SIZE_DIVISOR = 2;
    private final Map<Class<?>, ArrayAdapterInterface<?>> adapters;
    private int currentSize;
    private final GroupedLinkedMap<Key, Object> groupedMap;
    private final KeyPool keyPool;
    private final int maxSize;
    private final Map<Class<?>, NavigableMap<Integer, Integer>> sortedSizes;
    
    public LruArrayPool() {
        this.groupedMap = new GroupedLinkedMap<Key, Object>();
        this.keyPool = new KeyPool();
        this.sortedSizes = new HashMap<Class<?>, NavigableMap<Integer, Integer>>();
        this.adapters = new HashMap<Class<?>, ArrayAdapterInterface<?>>();
        this.maxSize = 4194304;
    }
    
    public LruArrayPool(final int maxSize) {
        this.groupedMap = new GroupedLinkedMap<Key, Object>();
        this.keyPool = new KeyPool();
        this.sortedSizes = new HashMap<Class<?>, NavigableMap<Integer, Integer>>();
        this.adapters = new HashMap<Class<?>, ArrayAdapterInterface<?>>();
        this.maxSize = maxSize;
    }
    
    private void decrementArrayOfSize(final int n, final Class<?> clazz) {
        final NavigableMap<Integer, Integer> sizesForAdapter = this.getSizesForAdapter(clazz);
        final Integer n2 = sizesForAdapter.get(n);
        if (n2 != null) {
            if (n2 == 1) {
                sizesForAdapter.remove(n);
            }
            else {
                sizesForAdapter.put(n, n2 - 1);
            }
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Tried to decrement empty size, size: ");
        sb.append(n);
        sb.append(", this: ");
        sb.append(this);
        throw new NullPointerException(sb.toString());
    }
    
    private void evict() {
        this.evictToSize(this.maxSize);
    }
    
    private void evictToSize(final int n) {
        while (this.currentSize > n) {
            final Object removeLast = this.groupedMap.removeLast();
            Preconditions.checkNotNull(removeLast);
            final ArrayAdapterInterface<Object> adapterFromObject = this.getAdapterFromObject(removeLast);
            this.currentSize -= adapterFromObject.getArrayLength(removeLast) * adapterFromObject.getElementSizeInBytes();
            this.decrementArrayOfSize(adapterFromObject.getArrayLength(removeLast), removeLast.getClass());
            if (Log.isLoggable(adapterFromObject.getTag(), 2)) {
                final String tag = adapterFromObject.getTag();
                final StringBuilder sb = new StringBuilder();
                sb.append("evicted: ");
                sb.append(adapterFromObject.getArrayLength(removeLast));
                Log.v(tag, sb.toString());
            }
        }
    }
    
    private <T> ArrayAdapterInterface<T> getAdapterFromObject(final T t) {
        return this.getAdapterFromType(t.getClass());
    }
    
    private <T> ArrayAdapterInterface<T> getAdapterFromType(final Class<T> clazz) {
        ArrayAdapterInterface arrayAdapterInterface;
        if ((arrayAdapterInterface = this.adapters.get(clazz)) == null) {
            if (clazz.equals(int[].class)) {
                arrayAdapterInterface = new IntegerArrayAdapter();
            }
            else {
                if (!clazz.equals(byte[].class)) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("No array pool found for: ");
                    sb.append(clazz.getSimpleName());
                    throw new IllegalArgumentException(sb.toString());
                }
                arrayAdapterInterface = new ByteArrayAdapter();
            }
            this.adapters.put(clazz, arrayAdapterInterface);
        }
        return arrayAdapterInterface;
    }
    
    private <T> T getArrayForKey(final Key key) {
        return (T)this.groupedMap.get(key);
    }
    
    private <T> T getForKey(final Key key, final Class<T> clazz) {
        final ArrayAdapterInterface<T> adapterFromType = this.getAdapterFromType(clazz);
        final T arrayForKey = this.getArrayForKey(key);
        if (arrayForKey != null) {
            this.currentSize -= adapterFromType.getArrayLength(arrayForKey) * adapterFromType.getElementSizeInBytes();
            this.decrementArrayOfSize(adapterFromType.getArrayLength(arrayForKey), clazz);
        }
        Object array;
        if ((array = arrayForKey) == null) {
            if (Log.isLoggable(adapterFromType.getTag(), 2)) {
                final String tag = adapterFromType.getTag();
                final StringBuilder sb = new StringBuilder();
                sb.append("Allocated ");
                sb.append(key.size);
                sb.append(" bytes");
                Log.v(tag, sb.toString());
            }
            array = adapterFromType.newArray(key.size);
        }
        return (T)array;
    }
    
    private NavigableMap<Integer, Integer> getSizesForAdapter(final Class<?> clazz) {
        NavigableMap navigableMap;
        if ((navigableMap = this.sortedSizes.get(clazz)) == null) {
            navigableMap = new TreeMap();
            this.sortedSizes.put(clazz, navigableMap);
        }
        return navigableMap;
    }
    
    private boolean isNoMoreThanHalfFull() {
        final int currentSize = this.currentSize;
        return currentSize == 0 || this.maxSize / currentSize >= 2;
    }
    
    private boolean isSmallEnoughForReuse(final int n) {
        return n <= this.maxSize / 2;
    }
    
    private boolean mayFillRequest(final int n, final Integer n2) {
        return n2 != null && (this.isNoMoreThanHalfFull() || n2 <= n * 8);
    }
    
    @Override
    public void clearMemory() {
        synchronized (this) {
            this.evictToSize(0);
        }
    }
    
    @Override
    public <T> T get(final int i, final Class<T> clazz) {
        synchronized (this) {
            final Integer n = this.getSizesForAdapter(clazz).ceilingKey(i);
            Key key;
            if (this.mayFillRequest(i, n)) {
                key = this.keyPool.get(n, clazz);
            }
            else {
                key = this.keyPool.get(i, clazz);
            }
            return this.getForKey(key, clazz);
        }
    }
    
    int getCurrentSize() {
        final Iterator<Class<?>> iterator = this.sortedSizes.keySet().iterator();
        int n = 0;
        while (iterator.hasNext()) {
            final Class clazz = iterator.next();
            final Iterator iterator2 = this.sortedSizes.get(clazz).keySet().iterator();
            int n2 = n;
            while (true) {
                n = n2;
                if (!iterator2.hasNext()) {
                    break;
                }
                final Integer n3 = (Integer)iterator2.next();
                n2 += n3 * (int)this.sortedSizes.get(clazz).get(n3) * this.getAdapterFromType((Class<Object>)clazz).getElementSizeInBytes();
            }
        }
        return n;
    }
    
    @Override
    public <T> T getExact(final int n, final Class<T> clazz) {
        synchronized (this) {
            return this.getForKey(this.keyPool.get(n, clazz), clazz);
        }
    }
    
    @Override
    public <T> void put(final T t) {
        synchronized (this) {
            final Class<?> class1 = t.getClass();
            final ArrayAdapterInterface<T> adapterFromType = this.getAdapterFromType(class1);
            final int arrayLength = adapterFromType.getArrayLength(t);
            final int n = adapterFromType.getElementSizeInBytes() * arrayLength;
            if (!this.isSmallEnoughForReuse(n)) {
                return;
            }
            final Key value = this.keyPool.get(arrayLength, class1);
            this.groupedMap.put(value, t);
            final NavigableMap<Integer, Integer> sizesForAdapter = this.getSizesForAdapter(class1);
            final Integer n2 = sizesForAdapter.get(value.size);
            final int size = value.size;
            int i = 1;
            if (n2 != null) {
                i = 1 + n2;
            }
            sizesForAdapter.put(size, i);
            this.currentSize += n;
            this.evict();
        }
    }
    
    @Deprecated
    @Override
    public <T> void put(final T t, final Class<T> clazz) {
        this.put(t);
    }
    
    @Override
    public void trimMemory(final int n) {
        monitorenter(this);
        Label_0041: {
            Label_0019: {
                if (n >= 40) {
                    Label_0044: {
                        try {
                            this.clearMemory();
                            break Label_0041;
                        }
                        finally {
                            break Label_0044;
                        }
                        break Label_0019;
                    }
                    monitorexit(this);
                }
            }
            if (n >= 20 || n == 15) {
                this.evictToSize(this.maxSize / 2);
            }
        }
        monitorexit(this);
    }
    
    private static final class Key implements Poolable
    {
        private Class<?> arrayClass;
        private final KeyPool pool;
        int size;
        
        Key(final KeyPool pool) {
            this.pool = pool;
        }
        
        @Override
        public boolean equals(final Object o) {
            final boolean b = o instanceof Key;
            boolean b3;
            final boolean b2 = b3 = false;
            if (b) {
                final Key key = (Key)o;
                b3 = b2;
                if (this.size == key.size) {
                    b3 = b2;
                    if (this.arrayClass == key.arrayClass) {
                        b3 = true;
                    }
                }
            }
            return b3;
        }
        
        @Override
        public int hashCode() {
            final int size = this.size;
            final Class<?> arrayClass = this.arrayClass;
            int hashCode;
            if (arrayClass != null) {
                hashCode = arrayClass.hashCode();
            }
            else {
                hashCode = 0;
            }
            return size * 31 + hashCode;
        }
        
        void init(final int size, final Class<?> arrayClass) {
            this.size = size;
            this.arrayClass = arrayClass;
        }
        
        @Override
        public void offer() {
            this.pool.offer(this);
        }
        
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder();
            sb.append("Key{size=");
            sb.append(this.size);
            sb.append("array=");
            sb.append(this.arrayClass);
            sb.append('}');
            return sb.toString();
        }
    }
    
    private static final class KeyPool extends BaseKeyPool<Key>
    {
        KeyPool() {
        }
        
        protected Key create() {
            return new Key(this);
        }
        
        Key get(final int n, final Class<?> clazz) {
            final Key key = this.get();
            key.init(n, clazz);
            return key;
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide.load.engine.bitmap_recycle;

public interface ArrayPool
{
    public static final int STANDARD_BUFFER_SIZE_BYTES = 65536;
    
    void clearMemory();
    
     <T> T get(final int p0, final Class<T> p1);
    
     <T> T getExact(final int p0, final Class<T> p1);
    
     <T> void put(final T p0);
    
    @Deprecated
     <T> void put(final T p0, final Class<T> p1);
    
    void trimMemory(final int p0);
}

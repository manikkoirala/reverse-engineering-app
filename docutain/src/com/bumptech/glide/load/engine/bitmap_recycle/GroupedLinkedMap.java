// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide.load.engine.bitmap_recycle;

import java.util.ArrayList;
import java.util.List;
import java.util.HashMap;
import java.util.Map;

class GroupedLinkedMap<K extends Poolable, V>
{
    private final LinkedEntry<K, V> head;
    private final Map<K, LinkedEntry<K, V>> keyToEntry;
    
    GroupedLinkedMap() {
        this.head = new LinkedEntry<K, V>();
        this.keyToEntry = new HashMap<K, LinkedEntry<K, V>>();
    }
    
    private void makeHead(final LinkedEntry<K, V> linkedEntry) {
        removeEntry(linkedEntry);
        linkedEntry.prev = this.head;
        linkedEntry.next = this.head.next;
        updateEntry(linkedEntry);
    }
    
    private void makeTail(final LinkedEntry<K, V> linkedEntry) {
        removeEntry(linkedEntry);
        linkedEntry.prev = this.head.prev;
        linkedEntry.next = this.head;
        updateEntry(linkedEntry);
    }
    
    private static <K, V> void removeEntry(final LinkedEntry<K, V> linkedEntry) {
        linkedEntry.prev.next = linkedEntry.next;
        linkedEntry.next.prev = linkedEntry.prev;
    }
    
    private static <K, V> void updateEntry(final LinkedEntry<K, V> linkedEntry) {
        linkedEntry.next.prev = linkedEntry;
        linkedEntry.prev.next = linkedEntry;
    }
    
    public V get(final K k) {
        final LinkedEntry linkedEntry = this.keyToEntry.get(k);
        LinkedEntry linkedEntry3;
        if (linkedEntry == null) {
            final LinkedEntry linkedEntry2 = new LinkedEntry((K)k);
            this.keyToEntry.put(k, linkedEntry2);
            linkedEntry3 = linkedEntry2;
        }
        else {
            k.offer();
            linkedEntry3 = linkedEntry;
        }
        this.makeHead(linkedEntry3);
        return (V)linkedEntry3.removeLast();
    }
    
    public void put(final K k, final V v) {
        final LinkedEntry linkedEntry = this.keyToEntry.get(k);
        LinkedEntry linkedEntry3;
        if (linkedEntry == null) {
            final LinkedEntry linkedEntry2 = new LinkedEntry((K)k);
            this.makeTail(linkedEntry2);
            this.keyToEntry.put(k, linkedEntry2);
            linkedEntry3 = linkedEntry2;
        }
        else {
            k.offer();
            linkedEntry3 = linkedEntry;
        }
        linkedEntry3.add(v);
    }
    
    public V removeLast() {
        for (LinkedEntry<K, V> linkedEntry = this.head.prev; !linkedEntry.equals(this.head); linkedEntry = linkedEntry.prev) {
            final V removeLast = linkedEntry.removeLast();
            if (removeLast != null) {
                return removeLast;
            }
            removeEntry((LinkedEntry<Object, Object>)linkedEntry);
            this.keyToEntry.remove(linkedEntry.key);
            linkedEntry.key.offer();
        }
        return null;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("GroupedLinkedMap( ");
        LinkedEntry<K, V> linkedEntry = this.head.next;
        boolean b = false;
        while (!linkedEntry.equals(this.head)) {
            b = true;
            sb.append('{');
            sb.append(linkedEntry.key);
            sb.append(':');
            sb.append(linkedEntry.size());
            sb.append("}, ");
            linkedEntry = linkedEntry.next;
        }
        if (b) {
            sb.delete(sb.length() - 2, sb.length());
        }
        sb.append(" )");
        return sb.toString();
    }
    
    private static class LinkedEntry<K, V>
    {
        final K key;
        LinkedEntry<K, V> next;
        LinkedEntry<K, V> prev;
        private List<V> values;
        
        LinkedEntry() {
            this(null);
        }
        
        LinkedEntry(final K key) {
            this.prev = this;
            this.next = this;
            this.key = key;
        }
        
        public void add(final V v) {
            if (this.values == null) {
                this.values = new ArrayList<V>();
            }
            this.values.add(v);
        }
        
        public V removeLast() {
            final int size = this.size();
            V remove;
            if (size > 0) {
                remove = this.values.remove(size - 1);
            }
            else {
                remove = null;
            }
            return remove;
        }
        
        public int size() {
            final List<V> values = this.values;
            int size;
            if (values != null) {
                size = values.size();
            }
            else {
                size = 0;
            }
            return size;
        }
    }
}

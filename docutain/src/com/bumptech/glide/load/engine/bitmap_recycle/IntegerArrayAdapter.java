// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide.load.engine.bitmap_recycle;

public final class IntegerArrayAdapter implements ArrayAdapterInterface<int[]>
{
    private static final String TAG = "IntegerArrayPool";
    
    @Override
    public int getArrayLength(final int[] array) {
        return array.length;
    }
    
    @Override
    public int getElementSizeInBytes() {
        return 4;
    }
    
    @Override
    public String getTag() {
        return "IntegerArrayPool";
    }
    
    @Override
    public int[] newArray(final int n) {
        return new int[n];
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide.load.engine;

import com.bumptech.glide.load.engine.cache.DiskCacheAdapter;
import java.util.concurrent.ExecutorService;
import com.bumptech.glide.util.Executors;
import com.bumptech.glide.util.Preconditions;
import com.bumptech.glide.util.pool.FactoryPools;
import androidx.core.util.Pools;
import com.bumptech.glide.load.DataSource;
import java.util.concurrent.Executor;
import com.bumptech.glide.request.ResourceCallback;
import com.bumptech.glide.load.Options;
import com.bumptech.glide.load.Transformation;
import java.util.Map;
import com.bumptech.glide.Priority;
import com.bumptech.glide.GlideContext;
import com.bumptech.glide.util.LogTime;
import com.bumptech.glide.load.Key;
import com.bumptech.glide.load.engine.executor.GlideExecutor;
import com.bumptech.glide.load.engine.cache.DiskCache;
import android.util.Log;
import com.bumptech.glide.load.engine.cache.MemoryCache;

public class Engine implements EngineJobListener, ResourceRemovedListener, ResourceListener
{
    private static final int JOB_POOL_SIZE = 150;
    private static final String TAG = "Engine";
    private static final boolean VERBOSE_IS_LOGGABLE;
    private final ActiveResources activeResources;
    private final MemoryCache cache;
    private final DecodeJobFactory decodeJobFactory;
    private final LazyDiskCacheProvider diskCacheProvider;
    private final EngineJobFactory engineJobFactory;
    private final Jobs jobs;
    private final EngineKeyFactory keyFactory;
    private final ResourceRecycler resourceRecycler;
    
    static {
        VERBOSE_IS_LOGGABLE = Log.isLoggable("Engine", 2);
    }
    
    Engine(final MemoryCache cache, final DiskCache.Factory factory, final GlideExecutor glideExecutor, final GlideExecutor glideExecutor2, final GlideExecutor glideExecutor3, final GlideExecutor glideExecutor4, Jobs jobs, EngineKeyFactory keyFactory, final ActiveResources activeResources, final EngineJobFactory engineJobFactory, DecodeJobFactory decodeJobFactory, final ResourceRecycler resourceRecycler, final boolean b) {
        this.cache = cache;
        final LazyDiskCacheProvider diskCacheProvider = new LazyDiskCacheProvider(factory);
        this.diskCacheProvider = diskCacheProvider;
        ActiveResources activeResources2;
        if (activeResources == null) {
            activeResources2 = new ActiveResources(b);
        }
        else {
            activeResources2 = activeResources;
        }
        (this.activeResources = activeResources2).setListener(this);
        if (keyFactory == null) {
            keyFactory = new EngineKeyFactory();
        }
        this.keyFactory = keyFactory;
        if (jobs == null) {
            jobs = new Jobs();
        }
        this.jobs = jobs;
        EngineJobFactory engineJobFactory2;
        if (engineJobFactory == null) {
            engineJobFactory2 = new EngineJobFactory(glideExecutor, glideExecutor2, glideExecutor3, glideExecutor4, this, this);
        }
        else {
            engineJobFactory2 = engineJobFactory;
        }
        this.engineJobFactory = engineJobFactory2;
        if (decodeJobFactory == null) {
            decodeJobFactory = new DecodeJobFactory(diskCacheProvider);
        }
        this.decodeJobFactory = decodeJobFactory;
        ResourceRecycler resourceRecycler2;
        if (resourceRecycler == null) {
            resourceRecycler2 = new ResourceRecycler();
        }
        else {
            resourceRecycler2 = resourceRecycler;
        }
        this.resourceRecycler = resourceRecycler2;
        cache.setResourceRemovedListener((MemoryCache.ResourceRemovedListener)this);
    }
    
    public Engine(final MemoryCache memoryCache, final DiskCache.Factory factory, final GlideExecutor glideExecutor, final GlideExecutor glideExecutor2, final GlideExecutor glideExecutor3, final GlideExecutor glideExecutor4, final boolean b) {
        this(memoryCache, factory, glideExecutor, glideExecutor2, glideExecutor3, glideExecutor4, null, null, null, null, null, null, b);
    }
    
    private EngineResource<?> getEngineResourceFromCache(final Key key) {
        final Resource<?> remove = this.cache.remove(key);
        EngineResource<?> engineResource;
        if (remove == null) {
            engineResource = null;
        }
        else if (remove instanceof EngineResource) {
            engineResource = (EngineResource<?>)remove;
        }
        else {
            engineResource = new EngineResource<Object>(remove, true, true, key, (EngineResource.ResourceListener)this);
        }
        return engineResource;
    }
    
    private EngineResource<?> loadFromActiveResources(final Key key) {
        final EngineResource<?> value = this.activeResources.get(key);
        if (value != null) {
            value.acquire();
        }
        return value;
    }
    
    private EngineResource<?> loadFromCache(final Key key) {
        final EngineResource<?> engineResourceFromCache = this.getEngineResourceFromCache(key);
        if (engineResourceFromCache != null) {
            engineResourceFromCache.acquire();
            this.activeResources.activate(key, engineResourceFromCache);
        }
        return engineResourceFromCache;
    }
    
    private EngineResource<?> loadFromMemory(final EngineKey engineKey, final boolean b, final long n) {
        if (!b) {
            return null;
        }
        final EngineResource<?> loadFromActiveResources = this.loadFromActiveResources(engineKey);
        if (loadFromActiveResources != null) {
            if (Engine.VERBOSE_IS_LOGGABLE) {
                logWithTimeAndKey("Loaded resource from active resources", n, engineKey);
            }
            return loadFromActiveResources;
        }
        final EngineResource<?> loadFromCache = this.loadFromCache(engineKey);
        if (loadFromCache != null) {
            if (Engine.VERBOSE_IS_LOGGABLE) {
                logWithTimeAndKey("Loaded resource from cache", n, engineKey);
            }
            return loadFromCache;
        }
        return null;
    }
    
    private static void logWithTimeAndKey(final String str, final long n, final Key obj) {
        final StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append(" in ");
        sb.append(LogTime.getElapsedMillis(n));
        sb.append("ms, key: ");
        sb.append(obj);
        Log.v("Engine", sb.toString());
    }
    
    private <R> LoadStatus waitForExistingOrStartNewJob(final GlideContext glideContext, final Object o, final Key key, final int n, final int n2, final Class<?> clazz, final Class<R> clazz2, final Priority priority, final DiskCacheStrategy diskCacheStrategy, final Map<Class<?>, Transformation<?>> map, final boolean b, final boolean b2, final Options options, final boolean b3, final boolean b4, final boolean b5, final boolean b6, final ResourceCallback resourceCallback, final Executor executor, final EngineKey engineKey, final long n3) {
        final EngineJob<?> value = this.jobs.get(engineKey, b6);
        if (value != null) {
            value.addCallback(resourceCallback, executor);
            if (Engine.VERBOSE_IS_LOGGABLE) {
                logWithTimeAndKey("Added to existing load", n3, engineKey);
            }
            return new LoadStatus(resourceCallback, value);
        }
        final EngineJob<Object> build = this.engineJobFactory.build(engineKey, b3, b4, b5, b6);
        final DecodeJob<R> build2 = this.decodeJobFactory.build(glideContext, o, engineKey, key, n, n2, clazz, clazz2, priority, diskCacheStrategy, map, b, b2, b6, options, (DecodeJob.Callback<R>)build);
        this.jobs.put(engineKey, build);
        build.addCallback(resourceCallback, executor);
        build.start((DecodeJob<Object>)build2);
        if (Engine.VERBOSE_IS_LOGGABLE) {
            logWithTimeAndKey("Started new load", n3, engineKey);
        }
        return new LoadStatus(resourceCallback, build);
    }
    
    public void clearDiskCache() {
        this.diskCacheProvider.getDiskCache().clear();
    }
    
    public <R> LoadStatus load(final GlideContext glideContext, final Object o, final Key key, final int n, final int n2, final Class<?> clazz, final Class<R> clazz2, final Priority priority, final DiskCacheStrategy diskCacheStrategy, final Map<Class<?>, Transformation<?>> map, final boolean b, final boolean b2, final Options options, final boolean b3, final boolean b4, final boolean b5, final boolean b6, final ResourceCallback resourceCallback, final Executor executor) {
        long logTime;
        if (Engine.VERBOSE_IS_LOGGABLE) {
            logTime = LogTime.getLogTime();
        }
        else {
            logTime = 0L;
        }
        final EngineKey buildKey = this.keyFactory.buildKey(o, key, n, n2, map, clazz, clazz2, options);
        synchronized (this) {
            final EngineResource<?> loadFromMemory = this.loadFromMemory(buildKey, b3, logTime);
            if (loadFromMemory == null) {
                return this.waitForExistingOrStartNewJob(glideContext, o, key, n, n2, clazz, clazz2, priority, diskCacheStrategy, map, b, b2, options, b3, b4, b5, b6, resourceCallback, executor, buildKey, logTime);
            }
            monitorexit(this);
            resourceCallback.onResourceReady(loadFromMemory, DataSource.MEMORY_CACHE, false);
            return null;
        }
    }
    
    @Override
    public void onEngineJobCancelled(final EngineJob<?> engineJob, final Key key) {
        synchronized (this) {
            this.jobs.removeIfCurrent(key, engineJob);
        }
    }
    
    @Override
    public void onEngineJobComplete(final EngineJob<?> engineJob, final Key key, final EngineResource<?> engineResource) {
        monitorenter(this);
        Label_0022: {
            if (engineResource == null) {
                break Label_0022;
            }
            try {
                if (engineResource.isMemoryCacheable()) {
                    this.activeResources.activate(key, engineResource);
                }
                this.jobs.removeIfCurrent(key, engineJob);
            }
            finally {
                monitorexit(this);
            }
        }
    }
    
    @Override
    public void onResourceReleased(final Key key, final EngineResource<?> engineResource) {
        this.activeResources.deactivate(key);
        if (engineResource.isMemoryCacheable()) {
            this.cache.put(key, engineResource);
        }
        else {
            this.resourceRecycler.recycle(engineResource, false);
        }
    }
    
    @Override
    public void onResourceRemoved(final Resource<?> resource) {
        this.resourceRecycler.recycle(resource, true);
    }
    
    public void release(final Resource<?> resource) {
        if (resource instanceof EngineResource) {
            ((EngineResource)resource).release();
            return;
        }
        throw new IllegalArgumentException("Cannot release anything but an EngineResource");
    }
    
    public void shutdown() {
        this.engineJobFactory.shutdown();
        this.diskCacheProvider.clearDiskCacheIfCreated();
        this.activeResources.shutdown();
    }
    
    static class DecodeJobFactory
    {
        private int creationOrder;
        final DecodeJob.DiskCacheProvider diskCacheProvider;
        final Pools.Pool<DecodeJob<?>> pool;
        
        DecodeJobFactory(final DecodeJob.DiskCacheProvider diskCacheProvider) {
            this.pool = FactoryPools.threadSafe(150, (FactoryPools.Factory<DecodeJob<?>>)new FactoryPools.Factory<DecodeJob<?>>() {
                final DecodeJobFactory this$0;
                
                public DecodeJob<?> create() {
                    return new DecodeJob<Object>(this.this$0.diskCacheProvider, this.this$0.pool);
                }
            });
            this.diskCacheProvider = diskCacheProvider;
        }
        
         <R> DecodeJob<R> build(final GlideContext glideContext, final Object o, final EngineKey engineKey, final Key key, final int n, final int n2, final Class<?> clazz, final Class<R> clazz2, final Priority priority, final DiskCacheStrategy diskCacheStrategy, final Map<Class<?>, Transformation<?>> map, final boolean b, final boolean b2, final boolean b3, final Options options, final DecodeJob.Callback<R> callback) {
            return Preconditions.checkNotNull(this.pool.acquire()).init(glideContext, o, engineKey, key, n, n2, clazz, clazz2, priority, diskCacheStrategy, map, b, b2, b3, options, (DecodeJob.Callback)callback, this.creationOrder++);
        }
    }
    
    static class EngineJobFactory
    {
        final GlideExecutor animationExecutor;
        final GlideExecutor diskCacheExecutor;
        final EngineJobListener engineJobListener;
        final Pools.Pool<EngineJob<?>> pool;
        final ResourceListener resourceListener;
        final GlideExecutor sourceExecutor;
        final GlideExecutor sourceUnlimitedExecutor;
        
        EngineJobFactory(final GlideExecutor diskCacheExecutor, final GlideExecutor sourceExecutor, final GlideExecutor sourceUnlimitedExecutor, final GlideExecutor animationExecutor, final EngineJobListener engineJobListener, final ResourceListener resourceListener) {
            this.pool = FactoryPools.threadSafe(150, (FactoryPools.Factory<EngineJob<?>>)new FactoryPools.Factory<EngineJob<?>>() {
                final EngineJobFactory this$0;
                
                public EngineJob<?> create() {
                    return new EngineJob<Object>(this.this$0.diskCacheExecutor, this.this$0.sourceExecutor, this.this$0.sourceUnlimitedExecutor, this.this$0.animationExecutor, this.this$0.engineJobListener, this.this$0.resourceListener, this.this$0.pool);
                }
            });
            this.diskCacheExecutor = diskCacheExecutor;
            this.sourceExecutor = sourceExecutor;
            this.sourceUnlimitedExecutor = sourceUnlimitedExecutor;
            this.animationExecutor = animationExecutor;
            this.engineJobListener = engineJobListener;
            this.resourceListener = resourceListener;
        }
        
         <R> EngineJob<R> build(final Key key, final boolean b, final boolean b2, final boolean b3, final boolean b4) {
            return Preconditions.checkNotNull(this.pool.acquire()).init(key, b, b2, b3, b4);
        }
        
        void shutdown() {
            Executors.shutdownAndAwaitTermination(this.diskCacheExecutor);
            Executors.shutdownAndAwaitTermination(this.sourceExecutor);
            Executors.shutdownAndAwaitTermination(this.sourceUnlimitedExecutor);
            Executors.shutdownAndAwaitTermination(this.animationExecutor);
        }
    }
    
    private static class LazyDiskCacheProvider implements DiskCacheProvider
    {
        private volatile DiskCache diskCache;
        private final DiskCache.Factory factory;
        
        LazyDiskCacheProvider(final DiskCache.Factory factory) {
            this.factory = factory;
        }
        
        void clearDiskCacheIfCreated() {
            synchronized (this) {
                if (this.diskCache == null) {
                    return;
                }
                this.diskCache.clear();
            }
        }
        
        @Override
        public DiskCache getDiskCache() {
            if (this.diskCache == null) {
                synchronized (this) {
                    if (this.diskCache == null) {
                        this.diskCache = this.factory.build();
                    }
                    if (this.diskCache == null) {
                        this.diskCache = new DiskCacheAdapter();
                    }
                }
            }
            return this.diskCache;
        }
    }
    
    public class LoadStatus
    {
        private final ResourceCallback cb;
        private final EngineJob<?> engineJob;
        final Engine this$0;
        
        LoadStatus(final Engine this$0, final ResourceCallback cb, final EngineJob<?> engineJob) {
            this.this$0 = this$0;
            this.cb = cb;
            this.engineJob = engineJob;
        }
        
        public void cancel() {
            synchronized (this.this$0) {
                this.engineJob.removeCallback(this.cb);
            }
        }
    }
}

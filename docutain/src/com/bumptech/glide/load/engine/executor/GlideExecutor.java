// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide.load.engine.executor;

import android.util.Log;
import android.os.StrictMode;
import android.os.StrictMode$ThreadPolicy$Builder;
import java.util.concurrent.atomic.AtomicInteger;
import android.os.Process;
import java.util.concurrent.PriorityBlockingQueue;
import android.text.TextUtils;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.Collection;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.ExecutorService;

public final class GlideExecutor implements ExecutorService
{
    static final String DEFAULT_ANIMATION_EXECUTOR_NAME = "animation";
    static final String DEFAULT_DISK_CACHE_EXECUTOR_NAME = "disk-cache";
    static final int DEFAULT_DISK_CACHE_EXECUTOR_THREADS = 1;
    static final String DEFAULT_SOURCE_EXECUTOR_NAME = "source";
    private static final String DEFAULT_SOURCE_UNLIMITED_EXECUTOR_NAME = "source-unlimited";
    private static final long KEEP_ALIVE_TIME_MS;
    private static final int MAXIMUM_AUTOMATIC_THREAD_COUNT = 4;
    private static final String TAG = "GlideExecutor";
    private static volatile int bestThreadCount;
    private final ExecutorService delegate;
    
    static {
        KEEP_ALIVE_TIME_MS = TimeUnit.SECONDS.toMillis(10L);
    }
    
    GlideExecutor(final ExecutorService delegate) {
        this.delegate = delegate;
    }
    
    static int calculateAnimationExecutorThreadCount() {
        int n;
        if (calculateBestThreadCount() >= 4) {
            n = 2;
        }
        else {
            n = 1;
        }
        return n;
    }
    
    public static int calculateBestThreadCount() {
        if (GlideExecutor.bestThreadCount == 0) {
            GlideExecutor.bestThreadCount = Math.min(4, RuntimeCompat.availableProcessors());
        }
        return GlideExecutor.bestThreadCount;
    }
    
    public static Builder newAnimationBuilder() {
        return new Builder(true).setThreadCount(calculateAnimationExecutorThreadCount()).setName("animation");
    }
    
    public static GlideExecutor newAnimationExecutor() {
        return newAnimationBuilder().build();
    }
    
    @Deprecated
    public static GlideExecutor newAnimationExecutor(final int threadCount, final UncaughtThrowableStrategy uncaughtThrowableStrategy) {
        return newAnimationBuilder().setThreadCount(threadCount).setUncaughtThrowableStrategy(uncaughtThrowableStrategy).build();
    }
    
    public static Builder newDiskCacheBuilder() {
        return new Builder(true).setThreadCount(1).setName("disk-cache");
    }
    
    public static GlideExecutor newDiskCacheExecutor() {
        return newDiskCacheBuilder().build();
    }
    
    @Deprecated
    public static GlideExecutor newDiskCacheExecutor(final int threadCount, final String name, final UncaughtThrowableStrategy uncaughtThrowableStrategy) {
        return newDiskCacheBuilder().setThreadCount(threadCount).setName(name).setUncaughtThrowableStrategy(uncaughtThrowableStrategy).build();
    }
    
    @Deprecated
    public static GlideExecutor newDiskCacheExecutor(final UncaughtThrowableStrategy uncaughtThrowableStrategy) {
        return newDiskCacheBuilder().setUncaughtThrowableStrategy(uncaughtThrowableStrategy).build();
    }
    
    public static Builder newSourceBuilder() {
        return new Builder(false).setThreadCount(calculateBestThreadCount()).setName("source");
    }
    
    public static GlideExecutor newSourceExecutor() {
        return newSourceBuilder().build();
    }
    
    @Deprecated
    public static GlideExecutor newSourceExecutor(final int threadCount, final String name, final UncaughtThrowableStrategy uncaughtThrowableStrategy) {
        return newSourceBuilder().setThreadCount(threadCount).setName(name).setUncaughtThrowableStrategy(uncaughtThrowableStrategy).build();
    }
    
    @Deprecated
    public static GlideExecutor newSourceExecutor(final UncaughtThrowableStrategy uncaughtThrowableStrategy) {
        return newSourceBuilder().setUncaughtThrowableStrategy(uncaughtThrowableStrategy).build();
    }
    
    public static GlideExecutor newUnlimitedSourceExecutor() {
        return new GlideExecutor(new ThreadPoolExecutor(0, Integer.MAX_VALUE, GlideExecutor.KEEP_ALIVE_TIME_MS, TimeUnit.MILLISECONDS, new SynchronousQueue<Runnable>(), new DefaultThreadFactory(new DefaultPriorityThreadFactory(), "source-unlimited", UncaughtThrowableStrategy.DEFAULT, false)));
    }
    
    @Override
    public boolean awaitTermination(final long n, final TimeUnit timeUnit) throws InterruptedException {
        return this.delegate.awaitTermination(n, timeUnit);
    }
    
    @Override
    public void execute(final Runnable runnable) {
        this.delegate.execute(runnable);
    }
    
    @Override
    public <T> List<Future<T>> invokeAll(final Collection<? extends Callable<T>> collection) throws InterruptedException {
        return this.delegate.invokeAll(collection);
    }
    
    @Override
    public <T> List<Future<T>> invokeAll(final Collection<? extends Callable<T>> collection, final long n, final TimeUnit timeUnit) throws InterruptedException {
        return this.delegate.invokeAll(collection, n, timeUnit);
    }
    
    @Override
    public <T> T invokeAny(final Collection<? extends Callable<T>> collection) throws InterruptedException, ExecutionException {
        return this.delegate.invokeAny(collection);
    }
    
    @Override
    public <T> T invokeAny(final Collection<? extends Callable<T>> collection, final long n, final TimeUnit timeUnit) throws InterruptedException, ExecutionException, TimeoutException {
        return this.delegate.invokeAny(collection, n, timeUnit);
    }
    
    @Override
    public boolean isShutdown() {
        return this.delegate.isShutdown();
    }
    
    @Override
    public boolean isTerminated() {
        return this.delegate.isTerminated();
    }
    
    @Override
    public void shutdown() {
        this.delegate.shutdown();
    }
    
    @Override
    public List<Runnable> shutdownNow() {
        return this.delegate.shutdownNow();
    }
    
    @Override
    public Future<?> submit(final Runnable runnable) {
        return this.delegate.submit(runnable);
    }
    
    @Override
    public <T> Future<T> submit(final Runnable runnable, final T t) {
        return this.delegate.submit(runnable, t);
    }
    
    @Override
    public <T> Future<T> submit(final Callable<T> callable) {
        return this.delegate.submit(callable);
    }
    
    @Override
    public String toString() {
        return this.delegate.toString();
    }
    
    public static final class Builder
    {
        public static final long NO_THREAD_TIMEOUT = 0L;
        private int corePoolSize;
        private int maximumPoolSize;
        private String name;
        private final boolean preventNetworkOperations;
        private final ThreadFactory threadFactory;
        private long threadTimeoutMillis;
        private UncaughtThrowableStrategy uncaughtThrowableStrategy;
        
        Builder(final boolean preventNetworkOperations) {
            this.threadFactory = new DefaultPriorityThreadFactory();
            this.uncaughtThrowableStrategy = UncaughtThrowableStrategy.DEFAULT;
            this.preventNetworkOperations = preventNetworkOperations;
        }
        
        public GlideExecutor build() {
            if (!TextUtils.isEmpty((CharSequence)this.name)) {
                final ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(this.corePoolSize, this.maximumPoolSize, this.threadTimeoutMillis, TimeUnit.MILLISECONDS, new PriorityBlockingQueue<Runnable>(), new DefaultThreadFactory(this.threadFactory, this.name, this.uncaughtThrowableStrategy, this.preventNetworkOperations));
                if (this.threadTimeoutMillis != 0L) {
                    threadPoolExecutor.allowCoreThreadTimeOut(true);
                }
                return new GlideExecutor(threadPoolExecutor);
            }
            final StringBuilder sb = new StringBuilder();
            sb.append("Name must be non-null and non-empty, but given: ");
            sb.append(this.name);
            throw new IllegalArgumentException(sb.toString());
        }
        
        public Builder setName(final String name) {
            this.name = name;
            return this;
        }
        
        public Builder setThreadCount(final int n) {
            this.corePoolSize = n;
            this.maximumPoolSize = n;
            return this;
        }
        
        public Builder setThreadTimeoutMillis(final long threadTimeoutMillis) {
            this.threadTimeoutMillis = threadTimeoutMillis;
            return this;
        }
        
        public Builder setUncaughtThrowableStrategy(final UncaughtThrowableStrategy uncaughtThrowableStrategy) {
            this.uncaughtThrowableStrategy = uncaughtThrowableStrategy;
            return this;
        }
    }
    
    private static final class DefaultPriorityThreadFactory implements ThreadFactory
    {
        private static final int DEFAULT_PRIORITY = 9;
        
        @Override
        public Thread newThread(final Runnable runnable) {
            return new Thread(this, runnable) {
                final DefaultPriorityThreadFactory this$0;
                
                @Override
                public void run() {
                    Process.setThreadPriority(9);
                    super.run();
                }
            };
        }
    }
    
    private static final class DefaultThreadFactory implements ThreadFactory
    {
        private final ThreadFactory delegate;
        private final String name;
        final boolean preventNetworkOperations;
        private final AtomicInteger threadNum;
        final UncaughtThrowableStrategy uncaughtThrowableStrategy;
        
        DefaultThreadFactory(final ThreadFactory delegate, final String name, final UncaughtThrowableStrategy uncaughtThrowableStrategy, final boolean preventNetworkOperations) {
            this.threadNum = new AtomicInteger();
            this.delegate = delegate;
            this.name = name;
            this.uncaughtThrowableStrategy = uncaughtThrowableStrategy;
            this.preventNetworkOperations = preventNetworkOperations;
        }
        
        @Override
        public Thread newThread(final Runnable runnable) {
            final Thread thread = this.delegate.newThread(new Runnable(this, runnable) {
                final DefaultThreadFactory this$0;
                final Runnable val$runnable;
                
                @Override
                public void run() {
                    if (this.this$0.preventNetworkOperations) {
                        StrictMode.setThreadPolicy(new StrictMode$ThreadPolicy$Builder().detectNetwork().penaltyDeath().build());
                    }
                    try {
                        this.val$runnable.run();
                    }
                    finally {
                        final Throwable t;
                        this.this$0.uncaughtThrowableStrategy.handle(t);
                    }
                }
            });
            final StringBuilder sb = new StringBuilder();
            sb.append("glide-");
            sb.append(this.name);
            sb.append("-thread-");
            sb.append(this.threadNum.getAndIncrement());
            thread.setName(sb.toString());
            return thread;
        }
    }
    
    public interface UncaughtThrowableStrategy
    {
        public static final UncaughtThrowableStrategy DEFAULT = default1;
        public static final UncaughtThrowableStrategy IGNORE = new UncaughtThrowableStrategy() {
            @Override
            public void handle(final Throwable t) {
            }
        };
        public static final UncaughtThrowableStrategy LOG;
        public static final UncaughtThrowableStrategy THROW = new UncaughtThrowableStrategy() {
            @Override
            public void handle(final Throwable cause) {
                if (cause == null) {
                    return;
                }
                throw new RuntimeException("Request threw uncaught throwable", cause);
            }
        };
        
        default static {
            final UncaughtThrowableStrategy default1 = LOG = new UncaughtThrowableStrategy() {
                @Override
                public void handle(final Throwable t) {
                    if (t != null && Log.isLoggable("GlideExecutor", 6)) {
                        Log.e("GlideExecutor", "Request threw uncaught throwable", t);
                    }
                }
            };
        }
        
        void handle(final Throwable p0);
    }
}

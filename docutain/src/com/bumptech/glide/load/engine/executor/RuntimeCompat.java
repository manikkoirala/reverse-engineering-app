// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide.load.engine.executor;

import android.os.Build$VERSION;

final class RuntimeCompat
{
    private static final String CPU_LOCATION = "/sys/devices/system/cpu/";
    private static final String CPU_NAME_REGEX = "cpu[0-9]+";
    private static final String TAG = "GlideRuntimeCompat";
    
    private RuntimeCompat() {
    }
    
    static int availableProcessors() {
        int b = Runtime.getRuntime().availableProcessors();
        if (Build$VERSION.SDK_INT < 17) {
            b = Math.max(getCoreCountPre17(), b);
        }
        return b;
    }
    
    private static int getCoreCountPre17() {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     3: astore_2       
        //     4: new             Ljava/io/File;
        //     7: astore_3       
        //     8: aload_3        
        //     9: ldc             "/sys/devices/system/cpu/"
        //    11: invokespecial   java/io/File.<init>:(Ljava/lang/String;)V
        //    14: ldc             "cpu[0-9]+"
        //    16: invokestatic    java/util/regex/Pattern.compile:(Ljava/lang/String;)Ljava/util/regex/Pattern;
        //    19: astore          4
        //    21: new             Lcom/bumptech/glide/load/engine/executor/RuntimeCompat$1;
        //    24: astore_1       
        //    25: aload_1        
        //    26: aload           4
        //    28: invokespecial   com/bumptech/glide/load/engine/executor/RuntimeCompat$1.<init>:(Ljava/util/regex/Pattern;)V
        //    31: aload_3        
        //    32: aload_1        
        //    33: invokevirtual   java/io/File.listFiles:(Ljava/io/FilenameFilter;)[Ljava/io/File;
        //    36: astore_1       
        //    37: aload_2        
        //    38: invokestatic    android/os/StrictMode.setThreadPolicy:(Landroid/os/StrictMode$ThreadPolicy;)V
        //    41: goto            70
        //    44: astore_1       
        //    45: ldc             "GlideRuntimeCompat"
        //    47: bipush          6
        //    49: invokestatic    android/util/Log.isLoggable:(Ljava/lang/String;I)Z
        //    52: ifeq            64
        //    55: ldc             "GlideRuntimeCompat"
        //    57: ldc             "Failed to calculate accurate cpu count"
        //    59: aload_1        
        //    60: invokestatic    android/util/Log.e:(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //    63: pop            
        //    64: aload_2        
        //    65: invokestatic    android/os/StrictMode.setThreadPolicy:(Landroid/os/StrictMode$ThreadPolicy;)V
        //    68: aconst_null    
        //    69: astore_1       
        //    70: aload_1        
        //    71: ifnull          80
        //    74: aload_1        
        //    75: arraylength    
        //    76: istore_0       
        //    77: goto            82
        //    80: iconst_0       
        //    81: istore_0       
        //    82: iconst_1       
        //    83: iload_0        
        //    84: invokestatic    java/lang/Math.max:(II)I
        //    87: ireturn        
        //    88: astore_1       
        //    89: aload_2        
        //    90: invokestatic    android/os/StrictMode.setThreadPolicy:(Landroid/os/StrictMode$ThreadPolicy;)V
        //    93: aload_1        
        //    94: athrow         
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type
        //  -----  -----  -----  -----  ----
        //  4      37     44     70     Any
        //  45     64     88     95     Any
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0064:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2604)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:206)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
}

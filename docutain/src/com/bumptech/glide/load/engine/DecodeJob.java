// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide.load.engine;

import com.bumptech.glide.load.engine.cache.DiskCache;
import com.bumptech.glide.load.Encoder;
import com.bumptech.glide.load.ResourceEncoder;
import com.bumptech.glide.Registry;
import com.bumptech.glide.load.EncodeStrategy;
import android.content.Context;
import com.bumptech.glide.load.Transformation;
import java.util.Map;
import com.bumptech.glide.load.data.DataRewinder;
import java.util.Collection;
import com.bumptech.glide.util.pool.GlideTrace;
import com.bumptech.glide.load.resource.bitmap.Downsampler;
import android.os.Build$VERSION;
import android.util.Log;
import com.bumptech.glide.util.LogTime;
import java.util.ArrayList;
import java.util.List;
import com.bumptech.glide.util.pool.StateVerifier;
import com.bumptech.glide.Priority;
import androidx.core.util.Pools;
import com.bumptech.glide.load.Options;
import com.bumptech.glide.GlideContext;
import com.bumptech.glide.load.data.DataFetcher;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.Key;
import com.bumptech.glide.util.pool.FactoryPools;

class DecodeJob<R> implements FetcherReadyCallback, Runnable, Comparable<DecodeJob<?>>, Poolable
{
    private static final String TAG = "DecodeJob";
    private Callback<R> callback;
    private Key currentAttemptingKey;
    private Object currentData;
    private DataSource currentDataSource;
    private DataFetcher<?> currentFetcher;
    private volatile DataFetcherGenerator currentGenerator;
    private Key currentSourceKey;
    private Thread currentThread;
    private final DecodeHelper<R> decodeHelper;
    private final DeferredEncodeManager<?> deferredEncodeManager;
    private final DiskCacheProvider diskCacheProvider;
    private DiskCacheStrategy diskCacheStrategy;
    private GlideContext glideContext;
    private int height;
    private volatile boolean isCallbackNotified;
    private volatile boolean isCancelled;
    private boolean isLoadingFromAlternateCacheKey;
    private EngineKey loadKey;
    private Object model;
    private boolean onlyRetrieveFromCache;
    private Options options;
    private int order;
    private final Pools.Pool<DecodeJob<?>> pool;
    private Priority priority;
    private final ReleaseManager releaseManager;
    private RunReason runReason;
    private Key signature;
    private Stage stage;
    private long startFetchTime;
    private final StateVerifier stateVerifier;
    private final List<Throwable> throwables;
    private int width;
    
    DecodeJob(final DiskCacheProvider diskCacheProvider, final Pools.Pool<DecodeJob<?>> pool) {
        this.decodeHelper = new DecodeHelper<R>();
        this.throwables = new ArrayList<Throwable>();
        this.stateVerifier = StateVerifier.newInstance();
        this.deferredEncodeManager = new DeferredEncodeManager<Object>();
        this.releaseManager = new ReleaseManager();
        this.diskCacheProvider = diskCacheProvider;
        this.pool = pool;
    }
    
    private <Data> Resource<R> decodeFromData(final DataFetcher<?> dataFetcher, final Data data, final DataSource dataSource) throws GlideException {
        if (data == null) {
            dataFetcher.cleanup();
            return null;
        }
        try {
            final long logTime = LogTime.getLogTime();
            final Resource<R> decodeFromFetcher = this.decodeFromFetcher(data, dataSource);
            if (Log.isLoggable("DecodeJob", 2)) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Decoded result ");
                sb.append(decodeFromFetcher);
                this.logWithTimeAndKey(sb.toString(), logTime);
            }
            return decodeFromFetcher;
        }
        finally {
            dataFetcher.cleanup();
        }
    }
    
    private <Data> Resource<R> decodeFromFetcher(final Data data, final DataSource dataSource) throws GlideException {
        return this.runLoadPath(data, dataSource, (LoadPath<Data, ?, R>)this.decodeHelper.getLoadPath(data.getClass()));
    }
    
    private void decodeFromRetrievedData() {
        if (Log.isLoggable("DecodeJob", 2)) {
            final long startFetchTime = this.startFetchTime;
            final StringBuilder sb = new StringBuilder();
            sb.append("data: ");
            sb.append(this.currentData);
            sb.append(", cache key: ");
            sb.append(this.currentSourceKey);
            sb.append(", fetcher: ");
            sb.append(this.currentFetcher);
            this.logWithTimeAndKey("Retrieved data", startFetchTime, sb.toString());
        }
        Resource<R> decodeFromData = null;
        try {
            decodeFromData = this.decodeFromData(this.currentFetcher, this.currentData, this.currentDataSource);
        }
        catch (final GlideException ex) {
            ex.setLoggingDetails(this.currentAttemptingKey, this.currentDataSource);
            this.throwables.add(ex);
        }
        if (decodeFromData != null) {
            this.notifyEncodeAndRelease(decodeFromData, this.currentDataSource, this.isLoadingFromAlternateCacheKey);
        }
        else {
            this.runGenerators();
        }
    }
    
    private DataFetcherGenerator getNextGenerator() {
        final int n = DecodeJob$1.$SwitchMap$com$bumptech$glide$load$engine$DecodeJob$Stage[this.stage.ordinal()];
        if (n == 1) {
            return new ResourceCacheGenerator(this.decodeHelper, this);
        }
        if (n == 2) {
            return new DataCacheGenerator(this.decodeHelper, this);
        }
        if (n == 3) {
            return new SourceGenerator(this.decodeHelper, this);
        }
        if (n == 4) {
            return null;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Unrecognized stage: ");
        sb.append(this.stage);
        throw new IllegalStateException(sb.toString());
    }
    
    private Stage getNextStage(Stage obj) {
        final int n = DecodeJob$1.$SwitchMap$com$bumptech$glide$load$engine$DecodeJob$Stage[obj.ordinal()];
        if (n == 1) {
            if (this.diskCacheStrategy.decodeCachedData()) {
                obj = Stage.DATA_CACHE;
            }
            else {
                obj = this.getNextStage(Stage.DATA_CACHE);
            }
            return obj;
        }
        if (n == 2) {
            if (this.onlyRetrieveFromCache) {
                obj = Stage.FINISHED;
            }
            else {
                obj = Stage.SOURCE;
            }
            return obj;
        }
        if (n == 3 || n == 4) {
            return Stage.FINISHED;
        }
        if (n == 5) {
            if (this.diskCacheStrategy.decodeCachedResource()) {
                obj = Stage.RESOURCE_CACHE;
            }
            else {
                obj = this.getNextStage(Stage.RESOURCE_CACHE);
            }
            return obj;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Unrecognized stage: ");
        sb.append(obj);
        throw new IllegalArgumentException(sb.toString());
    }
    
    private Options getOptionsWithHardwareConfig(final DataSource dataSource) {
        final Options options = this.options;
        if (Build$VERSION.SDK_INT < 26) {
            return options;
        }
        final boolean b = dataSource == DataSource.RESOURCE_DISK_CACHE || this.decodeHelper.isScaleOnlyOrNoTransform();
        final Boolean b2 = options.get(Downsampler.ALLOW_HARDWARE_CONFIG);
        if (b2 != null && (!b2 || b)) {
            return options;
        }
        final Options options2 = new Options();
        options2.putAll(this.options);
        options2.set(Downsampler.ALLOW_HARDWARE_CONFIG, b);
        return options2;
    }
    
    private int getPriority() {
        return this.priority.ordinal();
    }
    
    private void logWithTimeAndKey(final String s, final long n) {
        this.logWithTimeAndKey(s, n, null);
    }
    
    private void logWithTimeAndKey(String string, final long n, final String str) {
        final StringBuilder sb = new StringBuilder();
        sb.append(string);
        sb.append(" in ");
        sb.append(LogTime.getElapsedMillis(n));
        sb.append(", load key: ");
        sb.append(this.loadKey);
        if (str != null) {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append(", ");
            sb2.append(str);
            string = sb2.toString();
        }
        else {
            string = "";
        }
        sb.append(string);
        sb.append(", thread: ");
        sb.append(Thread.currentThread().getName());
        Log.v("DecodeJob", sb.toString());
    }
    
    private void notifyComplete(final Resource<R> resource, final DataSource dataSource, final boolean b) {
        this.setNotifiedOrThrow();
        this.callback.onResourceReady(resource, dataSource, b);
    }
    
    private void notifyEncodeAndRelease(final Resource<R> resource, final DataSource dataSource, final boolean b) {
        GlideTrace.beginSection("DecodeJob.notifyEncodeAndRelease");
        try {
            if (resource instanceof Initializable) {
                ((Initializable)resource).initialize();
            }
            LockedResource<R> obtain = null;
            Resource<R> resource2 = resource;
            if (this.deferredEncodeManager.hasResourceToEncode()) {
                resource2 = (obtain = LockedResource.obtain(resource));
            }
            this.notifyComplete(resource2, dataSource, b);
            this.stage = Stage.ENCODE;
            try {
                if (this.deferredEncodeManager.hasResourceToEncode()) {
                    this.deferredEncodeManager.encode(this.diskCacheProvider, this.options);
                }
                if (obtain != null) {
                    obtain.unlock();
                }
                this.onEncodeComplete();
            }
            finally {
                if (obtain != null) {
                    obtain.unlock();
                }
            }
        }
        finally {
            GlideTrace.endSection();
        }
    }
    
    private void notifyFailed() {
        this.setNotifiedOrThrow();
        this.callback.onLoadFailed(new GlideException("Failed to load resource", new ArrayList<Throwable>(this.throwables)));
        this.onLoadFailed();
    }
    
    private void onEncodeComplete() {
        if (this.releaseManager.onEncodeComplete()) {
            this.releaseInternal();
        }
    }
    
    private void onLoadFailed() {
        if (this.releaseManager.onFailed()) {
            this.releaseInternal();
        }
    }
    
    private void releaseInternal() {
        this.releaseManager.reset();
        this.deferredEncodeManager.clear();
        this.decodeHelper.clear();
        this.isCallbackNotified = false;
        this.glideContext = null;
        this.signature = null;
        this.options = null;
        this.priority = null;
        this.loadKey = null;
        this.callback = null;
        this.stage = null;
        this.currentGenerator = null;
        this.currentThread = null;
        this.currentSourceKey = null;
        this.currentData = null;
        this.currentDataSource = null;
        this.currentFetcher = null;
        this.startFetchTime = 0L;
        this.isCancelled = false;
        this.model = null;
        this.throwables.clear();
        this.pool.release(this);
    }
    
    private void reschedule(final RunReason runReason) {
        this.runReason = runReason;
        this.callback.reschedule(this);
    }
    
    private void runGenerators() {
        this.currentThread = Thread.currentThread();
        this.startFetchTime = LogTime.getLogTime();
        boolean startNext = false;
        do {
            boolean b = startNext;
            if (!this.isCancelled) {
                b = startNext;
                if (this.currentGenerator != null) {
                    startNext = this.currentGenerator.startNext();
                    if (!(b = startNext)) {
                        this.stage = this.getNextStage(this.stage);
                        this.currentGenerator = this.getNextGenerator();
                        continue;
                    }
                }
            }
            if ((this.stage == Stage.FINISHED || this.isCancelled) && !b) {
                this.notifyFailed();
            }
            return;
        } while (this.stage != Stage.SOURCE);
        this.reschedule(RunReason.SWITCH_TO_SOURCE_SERVICE);
    }
    
    private <Data, ResourceType> Resource<R> runLoadPath(Data rewinder, final DataSource dataSource, final LoadPath<Data, ResourceType, R> loadPath) throws GlideException {
        final Options optionsWithHardwareConfig = this.getOptionsWithHardwareConfig(dataSource);
        rewinder = (Data)this.glideContext.getRegistry().getRewinder(rewinder);
        try {
            return loadPath.load((DataRewinder<Data>)rewinder, optionsWithHardwareConfig, this.width, this.height, new DecodeCallback<ResourceType>(dataSource));
        }
        finally {
            ((DataRewinder)rewinder).cleanup();
        }
    }
    
    private void runWrapped() {
        final int n = DecodeJob$1.$SwitchMap$com$bumptech$glide$load$engine$DecodeJob$RunReason[this.runReason.ordinal()];
        if (n != 1) {
            if (n != 2) {
                if (n != 3) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Unrecognized run reason: ");
                    sb.append(this.runReason);
                    throw new IllegalStateException(sb.toString());
                }
                this.decodeFromRetrievedData();
            }
            else {
                this.runGenerators();
            }
        }
        else {
            this.stage = this.getNextStage(Stage.INITIALIZE);
            this.currentGenerator = this.getNextGenerator();
            this.runGenerators();
        }
    }
    
    private void setNotifiedOrThrow() {
        this.stateVerifier.throwIfRecycled();
        if (this.isCallbackNotified) {
            Throwable cause;
            if (this.throwables.isEmpty()) {
                cause = null;
            }
            else {
                final List<Throwable> throwables = this.throwables;
                cause = throwables.get(throwables.size() - 1);
            }
            throw new IllegalStateException("Already notified", cause);
        }
        this.isCallbackNotified = true;
    }
    
    public void cancel() {
        this.isCancelled = true;
        final DataFetcherGenerator currentGenerator = this.currentGenerator;
        if (currentGenerator != null) {
            currentGenerator.cancel();
        }
    }
    
    @Override
    public int compareTo(final DecodeJob<?> decodeJob) {
        int n;
        if ((n = this.getPriority() - decodeJob.getPriority()) == 0) {
            n = this.order - decodeJob.order;
        }
        return n;
    }
    
    @Override
    public StateVerifier getVerifier() {
        return this.stateVerifier;
    }
    
    DecodeJob<R> init(final GlideContext glideContext, final Object model, final EngineKey loadKey, final Key signature, final int width, final int height, final Class<?> clazz, final Class<R> clazz2, final Priority priority, final DiskCacheStrategy diskCacheStrategy, final Map<Class<?>, Transformation<?>> map, final boolean b, final boolean b2, final boolean onlyRetrieveFromCache, final Options options, final Callback<R> callback, final int order) {
        this.decodeHelper.init(glideContext, model, signature, width, height, diskCacheStrategy, clazz, clazz2, priority, options, map, b, b2, this.diskCacheProvider);
        this.glideContext = glideContext;
        this.signature = signature;
        this.priority = priority;
        this.loadKey = loadKey;
        this.width = width;
        this.height = height;
        this.diskCacheStrategy = diskCacheStrategy;
        this.onlyRetrieveFromCache = onlyRetrieveFromCache;
        this.options = options;
        this.callback = callback;
        this.order = order;
        this.runReason = RunReason.INITIALIZE;
        this.model = model;
        return this;
    }
    
    @Override
    public void onDataFetcherFailed(final Key key, final Exception ex, final DataFetcher<?> dataFetcher, final DataSource dataSource) {
        dataFetcher.cleanup();
        final GlideException ex2 = new GlideException("Fetching data failed", ex);
        ex2.setLoggingDetails(key, dataSource, dataFetcher.getDataClass());
        this.throwables.add(ex2);
        if (Thread.currentThread() != this.currentThread) {
            this.reschedule(RunReason.SWITCH_TO_SOURCE_SERVICE);
        }
        else {
            this.runGenerators();
        }
    }
    
    @Override
    public void onDataFetcherReady(final Key currentSourceKey, final Object currentData, final DataFetcher<?> currentFetcher, final DataSource currentDataSource, final Key currentAttemptingKey) {
        this.currentSourceKey = currentSourceKey;
        this.currentData = currentData;
        this.currentFetcher = currentFetcher;
        this.currentDataSource = currentDataSource;
        this.currentAttemptingKey = currentAttemptingKey;
        final List<Key> cacheKeys = this.decodeHelper.getCacheKeys();
        boolean isLoadingFromAlternateCacheKey = false;
        if (currentSourceKey != cacheKeys.get(0)) {
            isLoadingFromAlternateCacheKey = true;
        }
        this.isLoadingFromAlternateCacheKey = isLoadingFromAlternateCacheKey;
        if (Thread.currentThread() != this.currentThread) {
            this.reschedule(RunReason.DECODE_DATA);
            return;
        }
        GlideTrace.beginSection("DecodeJob.decodeFromRetrievedData");
        try {
            this.decodeFromRetrievedData();
        }
        finally {
            GlideTrace.endSection();
        }
    }
    
     <Z> Resource<Z> onResourceDecoded(final DataSource dataSource, final Resource<Z> resource) {
        final Class<?> class1 = resource.get().getClass();
        final DataSource resource_DISK_CACHE = DataSource.RESOURCE_DISK_CACHE;
        final ResourceEncoder resourceEncoder = null;
        Transformation<Z> transformation;
        Resource<Z> transform;
        if (dataSource != resource_DISK_CACHE) {
            transformation = this.decodeHelper.getTransformation(class1);
            transform = transformation.transform((Context)this.glideContext, resource, this.width, this.height);
        }
        else {
            transform = resource;
            transformation = null;
        }
        if (!resource.equals(transform)) {
            resource.recycle();
        }
        ResourceEncoder<Z> resultEncoder;
        EncodeStrategy obj;
        if (this.decodeHelper.isResourceEncoderAvailable(transform)) {
            resultEncoder = this.decodeHelper.getResultEncoder(transform);
            obj = resultEncoder.getEncodeStrategy(this.options);
        }
        else {
            obj = EncodeStrategy.NONE;
            resultEncoder = resourceEncoder;
        }
        final boolean sourceKey = this.decodeHelper.isSourceKey(this.currentSourceKey);
        Resource<Z> obtain = (Resource<Z>)transform;
        if (this.diskCacheStrategy.isResourceCacheable(sourceKey ^ true, dataSource, obj)) {
            if (resultEncoder == null) {
                throw new Registry.NoResultEncoderAvailableException(transform.get().getClass());
            }
            final int n = DecodeJob$1.$SwitchMap$com$bumptech$glide$load$EncodeStrategy[obj.ordinal()];
            Key key;
            if (n != 1) {
                if (n != 2) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Unknown strategy: ");
                    sb.append(obj);
                    throw new IllegalArgumentException(sb.toString());
                }
                key = new ResourceCacheKey(this.decodeHelper.getArrayPool(), this.currentSourceKey, this.signature, this.width, this.height, transformation, class1, this.options);
            }
            else {
                key = new DataCacheKey(this.currentSourceKey, this.signature);
            }
            obtain = (Resource<Z>)LockedResource.obtain(transform);
            this.deferredEncodeManager.init(key, (ResourceEncoder<Object>)resultEncoder, (LockedResource<Object>)obtain);
        }
        return (Resource<Z>)obtain;
    }
    
    void release(final boolean b) {
        if (this.releaseManager.release(b)) {
            this.releaseInternal();
        }
    }
    
    @Override
    public void reschedule() {
        this.reschedule(RunReason.SWITCH_TO_SOURCE_SERVICE);
    }
    
    @Override
    public void run() {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     3: aload_0        
        //     4: getfield        com/bumptech/glide/load/engine/DecodeJob.runReason:Lcom/bumptech/glide/load/engine/DecodeJob$RunReason;
        //     7: aload_0        
        //     8: getfield        com/bumptech/glide/load/engine/DecodeJob.model:Ljava/lang/Object;
        //    11: invokestatic    com/bumptech/glide/util/pool/GlideTrace.beginSectionFormat:(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V
        //    14: aload_0        
        //    15: getfield        com/bumptech/glide/load/engine/DecodeJob.currentFetcher:Lcom/bumptech/glide/load/data/DataFetcher;
        //    18: astore_1       
        //    19: aload_0        
        //    20: getfield        com/bumptech/glide/load/engine/DecodeJob.isCancelled:Z
        //    23: ifeq            44
        //    26: aload_0        
        //    27: invokespecial   com/bumptech/glide/load/engine/DecodeJob.notifyFailed:()V
        //    30: aload_1        
        //    31: ifnull          40
        //    34: aload_1        
        //    35: invokeinterface com/bumptech/glide/load/data/DataFetcher.cleanup:()V
        //    40: invokestatic    com/bumptech/glide/util/pool/GlideTrace.endSection:()V
        //    43: return         
        //    44: aload_0        
        //    45: invokespecial   com/bumptech/glide/load/engine/DecodeJob.runWrapped:()V
        //    48: aload_1        
        //    49: ifnull          58
        //    52: aload_1        
        //    53: invokeinterface com/bumptech/glide/load/data/DataFetcher.cleanup:()V
        //    58: invokestatic    com/bumptech/glide/util/pool/GlideTrace.endSection:()V
        //    61: return         
        //    62: astore_3       
        //    63: ldc             "DecodeJob"
        //    65: iconst_3       
        //    66: invokestatic    android/util/Log.isLoggable:(Ljava/lang/String;I)Z
        //    69: ifeq            125
        //    72: new             Ljava/lang/StringBuilder;
        //    75: astore_2       
        //    76: aload_2        
        //    77: invokespecial   java/lang/StringBuilder.<init>:()V
        //    80: aload_2        
        //    81: ldc_w           "DecodeJob threw unexpectedly, isCancelled: "
        //    84: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    87: pop            
        //    88: aload_2        
        //    89: aload_0        
        //    90: getfield        com/bumptech/glide/load/engine/DecodeJob.isCancelled:Z
        //    93: invokevirtual   java/lang/StringBuilder.append:(Z)Ljava/lang/StringBuilder;
        //    96: pop            
        //    97: aload_2        
        //    98: ldc_w           ", stage: "
        //   101: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   104: pop            
        //   105: aload_2        
        //   106: aload_0        
        //   107: getfield        com/bumptech/glide/load/engine/DecodeJob.stage:Lcom/bumptech/glide/load/engine/DecodeJob$Stage;
        //   110: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //   113: pop            
        //   114: ldc             "DecodeJob"
        //   116: aload_2        
        //   117: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   120: aload_3        
        //   121: invokestatic    android/util/Log.d:(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //   124: pop            
        //   125: aload_0        
        //   126: getfield        com/bumptech/glide/load/engine/DecodeJob.stage:Lcom/bumptech/glide/load/engine/DecodeJob$Stage;
        //   129: getstatic       com/bumptech/glide/load/engine/DecodeJob$Stage.ENCODE:Lcom/bumptech/glide/load/engine/DecodeJob$Stage;
        //   132: if_acmpeq       150
        //   135: aload_0        
        //   136: getfield        com/bumptech/glide/load/engine/DecodeJob.throwables:Ljava/util/List;
        //   139: aload_3        
        //   140: invokeinterface java/util/List.add:(Ljava/lang/Object;)Z
        //   145: pop            
        //   146: aload_0        
        //   147: invokespecial   com/bumptech/glide/load/engine/DecodeJob.notifyFailed:()V
        //   150: aload_0        
        //   151: getfield        com/bumptech/glide/load/engine/DecodeJob.isCancelled:Z
        //   154: ifne            159
        //   157: aload_3        
        //   158: athrow         
        //   159: aload_3        
        //   160: athrow         
        //   161: astore_2       
        //   162: aload_2        
        //   163: athrow         
        //   164: astore_2       
        //   165: aload_1        
        //   166: ifnull          175
        //   169: aload_1        
        //   170: invokeinterface com/bumptech/glide/load/data/DataFetcher.cleanup:()V
        //   175: invokestatic    com/bumptech/glide/util/pool/GlideTrace.endSection:()V
        //   178: aload_2        
        //   179: athrow         
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                                              
        //  -----  -----  -----  -----  --------------------------------------------------
        //  19     30     161    164    Lcom/bumptech/glide/load/engine/CallbackException;
        //  19     30     62     161    Any
        //  44     48     161    164    Lcom/bumptech/glide/load/engine/CallbackException;
        //  44     48     62     161    Any
        //  63     125    164    180    Any
        //  125    150    164    180    Any
        //  150    159    164    180    Any
        //  159    161    164    180    Any
        //  162    164    164    180    Any
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0125:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2604)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:206)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    boolean willDecodeFromCache() {
        final Stage nextStage = this.getNextStage(Stage.INITIALIZE);
        return nextStage == Stage.RESOURCE_CACHE || nextStage == Stage.DATA_CACHE;
    }
    
    interface Callback<R>
    {
        void onLoadFailed(final GlideException p0);
        
        void onResourceReady(final Resource<R> p0, final DataSource p1, final boolean p2);
        
        void reschedule(final DecodeJob<?> p0);
    }
    
    private final class DecodeCallback<Z> implements DecodePath.DecodeCallback<Z>
    {
        private final DataSource dataSource;
        final DecodeJob this$0;
        
        DecodeCallback(final DecodeJob this$0, final DataSource dataSource) {
            this.this$0 = this$0;
            this.dataSource = dataSource;
        }
        
        @Override
        public Resource<Z> onResourceDecoded(final Resource<Z> resource) {
            return this.this$0.onResourceDecoded(this.dataSource, resource);
        }
    }
    
    private static class DeferredEncodeManager<Z>
    {
        private ResourceEncoder<Z> encoder;
        private Key key;
        private LockedResource<Z> toEncode;
        
        DeferredEncodeManager() {
        }
        
        void clear() {
            this.key = null;
            this.encoder = null;
            this.toEncode = null;
        }
        
        void encode(final DiskCacheProvider diskCacheProvider, final Options options) {
            GlideTrace.beginSection("DecodeJob.encode");
            try {
                diskCacheProvider.getDiskCache().put(this.key, (DiskCache.Writer)new DataCacheWriter((Encoder<Object>)this.encoder, this.toEncode, options));
            }
            finally {
                this.toEncode.unlock();
                GlideTrace.endSection();
            }
        }
        
        boolean hasResourceToEncode() {
            return this.toEncode != null;
        }
        
         <X> void init(final Key key, final ResourceEncoder<X> encoder, final LockedResource<X> toEncode) {
            this.key = key;
            this.encoder = (ResourceEncoder<Z>)encoder;
            this.toEncode = (LockedResource<Z>)toEncode;
        }
    }
    
    interface DiskCacheProvider
    {
        DiskCache getDiskCache();
    }
    
    private static class ReleaseManager
    {
        private boolean isEncodeComplete;
        private boolean isFailed;
        private boolean isReleased;
        
        ReleaseManager() {
        }
        
        private boolean isComplete(final boolean b) {
            return (this.isFailed || b || this.isEncodeComplete) && this.isReleased;
        }
        
        boolean onEncodeComplete() {
            synchronized (this) {
                this.isEncodeComplete = true;
                return this.isComplete(false);
            }
        }
        
        boolean onFailed() {
            synchronized (this) {
                this.isFailed = true;
                return this.isComplete(false);
            }
        }
        
        boolean release(final boolean b) {
            synchronized (this) {
                this.isReleased = true;
                return this.isComplete(b);
            }
        }
        
        void reset() {
            synchronized (this) {
                this.isEncodeComplete = false;
                this.isReleased = false;
                this.isFailed = false;
            }
        }
    }
    
    private enum RunReason
    {
        private static final RunReason[] $VALUES;
        
        DECODE_DATA, 
        INITIALIZE, 
        SWITCH_TO_SOURCE_SERVICE;
    }
    
    private enum Stage
    {
        private static final Stage[] $VALUES;
        
        DATA_CACHE, 
        ENCODE, 
        FINISHED, 
        INITIALIZE, 
        RESOURCE_CACHE, 
        SOURCE;
    }
}

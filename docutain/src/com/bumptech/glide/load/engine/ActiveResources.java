// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide.load.engine;

import java.lang.ref.Reference;
import com.bumptech.glide.util.Preconditions;
import java.lang.ref.WeakReference;
import java.util.concurrent.ExecutorService;
import java.util.HashMap;
import java.util.concurrent.Executors;
import android.os.Process;
import java.util.concurrent.ThreadFactory;
import java.lang.ref.ReferenceQueue;
import java.util.concurrent.Executor;
import com.bumptech.glide.load.Key;
import java.util.Map;

final class ActiveResources
{
    final Map<Key, ResourceWeakReference> activeEngineResources;
    private volatile DequeuedResourceCallback cb;
    private final boolean isActiveResourceRetentionAllowed;
    private volatile boolean isShutdown;
    private EngineResource.ResourceListener listener;
    private final Executor monitorClearedResourcesExecutor;
    private final ReferenceQueue<EngineResource<?>> resourceReferenceQueue;
    
    ActiveResources(final boolean b) {
        this(b, Executors.newSingleThreadExecutor(new ThreadFactory() {
            @Override
            public Thread newThread(final Runnable runnable) {
                return new Thread(new Runnable(this, runnable) {
                    final ActiveResources$1 this$1;
                    final Runnable val$r;
                    
                    @Override
                    public void run() {
                        Process.setThreadPriority(10);
                        this.val$r.run();
                    }
                }, "glide-active-resources");
            }
        }));
    }
    
    ActiveResources(final boolean isActiveResourceRetentionAllowed, final Executor monitorClearedResourcesExecutor) {
        this.activeEngineResources = new HashMap<Key, ResourceWeakReference>();
        this.resourceReferenceQueue = new ReferenceQueue<EngineResource<?>>();
        this.isActiveResourceRetentionAllowed = isActiveResourceRetentionAllowed;
        (this.monitorClearedResourcesExecutor = monitorClearedResourcesExecutor).execute(new Runnable(this) {
            final ActiveResources this$0;
            
            @Override
            public void run() {
                this.this$0.cleanReferenceQueue();
            }
        });
    }
    
    void activate(final Key key, final EngineResource<?> engineResource) {
        synchronized (this) {
            final ResourceWeakReference resourceWeakReference = this.activeEngineResources.put(key, new ResourceWeakReference(key, engineResource, this.resourceReferenceQueue, this.isActiveResourceRetentionAllowed));
            if (resourceWeakReference != null) {
                resourceWeakReference.reset();
            }
        }
    }
    
    void cleanReferenceQueue() {
        while (!this.isShutdown) {
            try {
                this.cleanupActiveReference((ResourceWeakReference)this.resourceReferenceQueue.remove());
                final DequeuedResourceCallback cb = this.cb;
                if (cb == null) {
                    continue;
                }
                cb.onResourceDequeued();
            }
            catch (final InterruptedException ex) {
                Thread.currentThread().interrupt();
            }
        }
    }
    
    void cleanupActiveReference(final ResourceWeakReference resourceWeakReference) {
        synchronized (this) {
            this.activeEngineResources.remove(resourceWeakReference.key);
            if (resourceWeakReference.isCacheable && resourceWeakReference.resource != null) {
                monitorexit(this);
                this.listener.onResourceReleased(resourceWeakReference.key, new EngineResource<Object>(resourceWeakReference.resource, true, false, resourceWeakReference.key, this.listener));
            }
        }
    }
    
    void deactivate(final Key key) {
        synchronized (this) {
            final ResourceWeakReference resourceWeakReference = this.activeEngineResources.remove(key);
            if (resourceWeakReference != null) {
                resourceWeakReference.reset();
            }
        }
    }
    
    EngineResource<?> get(final Key key) {
        synchronized (this) {
            final ResourceWeakReference resourceWeakReference = this.activeEngineResources.get(key);
            if (resourceWeakReference == null) {
                return null;
            }
            final EngineResource engineResource = ((Reference<EngineResource>)resourceWeakReference).get();
            if (engineResource == null) {
                this.cleanupActiveReference(resourceWeakReference);
            }
            return engineResource;
        }
    }
    
    void setDequeuedResourceCallback(final DequeuedResourceCallback cb) {
        this.cb = cb;
    }
    
    void setListener(final EngineResource.ResourceListener listener) {
        synchronized (listener) {
            synchronized (this) {
                this.listener = listener;
            }
        }
    }
    
    void shutdown() {
        this.isShutdown = true;
        final Executor monitorClearedResourcesExecutor = this.monitorClearedResourcesExecutor;
        if (monitorClearedResourcesExecutor instanceof ExecutorService) {
            com.bumptech.glide.util.Executors.shutdownAndAwaitTermination((ExecutorService)monitorClearedResourcesExecutor);
        }
    }
    
    interface DequeuedResourceCallback
    {
        void onResourceDequeued();
    }
    
    static final class ResourceWeakReference extends WeakReference<EngineResource<?>>
    {
        final boolean isCacheable;
        final Key key;
        Resource<?> resource;
        
        ResourceWeakReference(final Key key, final EngineResource<?> referent, final ReferenceQueue<? super EngineResource<?>> q, final boolean b) {
            super(referent, q);
            this.key = Preconditions.checkNotNull(key);
            Resource resource;
            if (referent.isMemoryCacheable() && b) {
                resource = Preconditions.checkNotNull(referent.getResource());
            }
            else {
                resource = null;
            }
            this.resource = resource;
            this.isCacheable = referent.isMemoryCacheable();
        }
        
        void reset() {
            this.resource = null;
            this.clear();
        }
    }
}

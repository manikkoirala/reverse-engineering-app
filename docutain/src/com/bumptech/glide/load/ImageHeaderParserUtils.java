// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide.load;

import com.bumptech.glide.util.ByteBufferUtil;
import java.nio.ByteBuffer;
import java.io.IOException;
import java.io.InputStream;
import com.bumptech.glide.load.resource.bitmap.RecyclableBufferedInputStream;
import java.io.FileInputStream;
import com.bumptech.glide.load.engine.bitmap_recycle.ArrayPool;
import com.bumptech.glide.load.data.ParcelFileDescriptorRewinder;
import java.util.List;

public final class ImageHeaderParserUtils
{
    private static final int MARK_READ_LIMIT = 5242880;
    
    private ImageHeaderParserUtils() {
    }
    
    public static int getOrientation(final List<ImageHeaderParser> list, final ParcelFileDescriptorRewinder parcelFileDescriptorRewinder, final ArrayPool arrayPool) throws IOException {
        return getOrientationInternal(list, (OrientationReader)new OrientationReader(parcelFileDescriptorRewinder, arrayPool) {
            final ArrayPool val$byteArrayPool;
            final ParcelFileDescriptorRewinder val$parcelFileDescriptorRewinder;
            
            @Override
            public int getOrientationAndRewind(final ImageHeaderParser imageHeaderParser) throws IOException {
                RecyclableBufferedInputStream recyclableBufferedInputStream = null;
                try {
                    final RecyclableBufferedInputStream recyclableBufferedInputStream2 = new RecyclableBufferedInputStream(new FileInputStream(this.val$parcelFileDescriptorRewinder.rewindAndGet().getFileDescriptor()), this.val$byteArrayPool);
                    try {
                        final int orientation = imageHeaderParser.getOrientation(recyclableBufferedInputStream2, this.val$byteArrayPool);
                        recyclableBufferedInputStream2.release();
                        this.val$parcelFileDescriptorRewinder.rewindAndGet();
                        return orientation;
                    }
                    finally {
                        recyclableBufferedInputStream = recyclableBufferedInputStream2;
                    }
                }
                finally {}
                if (recyclableBufferedInputStream != null) {
                    recyclableBufferedInputStream.release();
                }
                this.val$parcelFileDescriptorRewinder.rewindAndGet();
            }
        });
    }
    
    public static int getOrientation(final List<ImageHeaderParser> list, final InputStream inputStream, final ArrayPool arrayPool) throws IOException {
        if (inputStream == null) {
            return -1;
        }
        InputStream inputStream2 = inputStream;
        if (!inputStream.markSupported()) {
            inputStream2 = new RecyclableBufferedInputStream(inputStream, arrayPool);
        }
        inputStream2.mark(5242880);
        return getOrientationInternal(list, (OrientationReader)new OrientationReader(inputStream2, arrayPool) {
            final ArrayPool val$byteArrayPool;
            final InputStream val$finalIs;
            
            @Override
            public int getOrientationAndRewind(final ImageHeaderParser imageHeaderParser) throws IOException {
                try {
                    return imageHeaderParser.getOrientation(this.val$finalIs, this.val$byteArrayPool);
                }
                finally {
                    this.val$finalIs.reset();
                }
            }
        });
    }
    
    public static int getOrientation(final List<ImageHeaderParser> list, final ByteBuffer byteBuffer, final ArrayPool arrayPool) throws IOException {
        if (byteBuffer == null) {
            return -1;
        }
        return getOrientationInternal(list, (OrientationReader)new OrientationReader(byteBuffer, arrayPool) {
            final ArrayPool val$arrayPool;
            final ByteBuffer val$buffer;
            
            @Override
            public int getOrientationAndRewind(final ImageHeaderParser imageHeaderParser) throws IOException {
                try {
                    return imageHeaderParser.getOrientation(this.val$buffer, this.val$arrayPool);
                }
                finally {
                    ByteBufferUtil.rewind(this.val$buffer);
                }
            }
        });
    }
    
    private static int getOrientationInternal(final List<ImageHeaderParser> list, final OrientationReader orientationReader) throws IOException {
        for (int size = list.size(), i = 0; i < size; ++i) {
            final int orientationAndRewind = orientationReader.getOrientationAndRewind(list.get(i));
            if (orientationAndRewind != -1) {
                return orientationAndRewind;
            }
        }
        return -1;
    }
    
    public static ImageHeaderParser.ImageType getType(final List<ImageHeaderParser> list, final ParcelFileDescriptorRewinder parcelFileDescriptorRewinder, final ArrayPool arrayPool) throws IOException {
        return getTypeInternal(list, (TypeReader)new TypeReader(parcelFileDescriptorRewinder, arrayPool) {
            final ArrayPool val$byteArrayPool;
            final ParcelFileDescriptorRewinder val$parcelFileDescriptorRewinder;
            
            @Override
            public ImageHeaderParser.ImageType getTypeAndRewind(final ImageHeaderParser imageHeaderParser) throws IOException {
                RecyclableBufferedInputStream recyclableBufferedInputStream = null;
                try {
                    final RecyclableBufferedInputStream recyclableBufferedInputStream2 = new RecyclableBufferedInputStream(new FileInputStream(this.val$parcelFileDescriptorRewinder.rewindAndGet().getFileDescriptor()), this.val$byteArrayPool);
                    try {
                        final ImageHeaderParser.ImageType type = imageHeaderParser.getType(recyclableBufferedInputStream2);
                        recyclableBufferedInputStream2.release();
                        this.val$parcelFileDescriptorRewinder.rewindAndGet();
                        return type;
                    }
                    finally {
                        recyclableBufferedInputStream = recyclableBufferedInputStream2;
                    }
                }
                finally {}
                if (recyclableBufferedInputStream != null) {
                    recyclableBufferedInputStream.release();
                }
                this.val$parcelFileDescriptorRewinder.rewindAndGet();
            }
        });
    }
    
    public static ImageHeaderParser.ImageType getType(final List<ImageHeaderParser> list, final InputStream inputStream, final ArrayPool arrayPool) throws IOException {
        if (inputStream == null) {
            return ImageHeaderParser.ImageType.UNKNOWN;
        }
        InputStream inputStream2 = inputStream;
        if (!inputStream.markSupported()) {
            inputStream2 = new RecyclableBufferedInputStream(inputStream, arrayPool);
        }
        inputStream2.mark(5242880);
        return getTypeInternal(list, (TypeReader)new TypeReader(inputStream2) {
            final InputStream val$finalIs;
            
            @Override
            public ImageHeaderParser.ImageType getTypeAndRewind(final ImageHeaderParser imageHeaderParser) throws IOException {
                try {
                    return imageHeaderParser.getType(this.val$finalIs);
                }
                finally {
                    this.val$finalIs.reset();
                }
            }
        });
    }
    
    public static ImageHeaderParser.ImageType getType(final List<ImageHeaderParser> list, final ByteBuffer byteBuffer) throws IOException {
        if (byteBuffer == null) {
            return ImageHeaderParser.ImageType.UNKNOWN;
        }
        return getTypeInternal(list, (TypeReader)new TypeReader(byteBuffer) {
            final ByteBuffer val$buffer;
            
            @Override
            public ImageHeaderParser.ImageType getTypeAndRewind(final ImageHeaderParser imageHeaderParser) throws IOException {
                try {
                    return imageHeaderParser.getType(this.val$buffer);
                }
                finally {
                    ByteBufferUtil.rewind(this.val$buffer);
                }
            }
        });
    }
    
    private static ImageHeaderParser.ImageType getTypeInternal(final List<ImageHeaderParser> list, final TypeReader typeReader) throws IOException {
        for (int size = list.size(), i = 0; i < size; ++i) {
            final ImageHeaderParser.ImageType typeAndRewind = typeReader.getTypeAndRewind(list.get(i));
            if (typeAndRewind != ImageHeaderParser.ImageType.UNKNOWN) {
                return typeAndRewind;
            }
        }
        return ImageHeaderParser.ImageType.UNKNOWN;
    }
    
    private interface OrientationReader
    {
        int getOrientationAndRewind(final ImageHeaderParser p0) throws IOException;
    }
    
    private interface TypeReader
    {
        ImageHeaderParser.ImageType getTypeAndRewind(final ImageHeaderParser p0) throws IOException;
    }
}

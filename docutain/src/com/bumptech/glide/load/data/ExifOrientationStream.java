// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide.load.data;

import java.io.IOException;
import java.io.InputStream;
import java.io.FilterInputStream;

public final class ExifOrientationStream extends FilterInputStream
{
    private static final byte[] EXIF_SEGMENT;
    private static final int ORIENTATION_POSITION;
    private static final int SEGMENT_LENGTH;
    private static final int SEGMENT_START_POSITION = 2;
    private final byte orientation;
    private int position;
    
    static {
        final byte[] array;
        final byte[] exif_SEGMENT = array = new byte[29];
        array[0] = -1;
        array[1] = -31;
        array[2] = 0;
        array[3] = 28;
        array[4] = 69;
        array[5] = 120;
        array[6] = 105;
        array[7] = 102;
        array[9] = (array[8] = 0);
        array[11] = (array[10] = 77);
        array[12] = 0;
        array[14] = (array[13] = 0);
        array[16] = (array[15] = 0);
        array[17] = 8;
        array[18] = 0;
        array[20] = (array[19] = 1);
        array[21] = 18;
        array[22] = 0;
        array[23] = 2;
        array[24] = 0;
        array[26] = (array[25] = 0);
        array[27] = 1;
        array[28] = 0;
        EXIF_SEGMENT = exif_SEGMENT;
        ORIENTATION_POSITION = (SEGMENT_LENGTH = exif_SEGMENT.length) + 2;
    }
    
    public ExifOrientationStream(final InputStream in, final int i) {
        super(in);
        if (i >= -1 && i <= 8) {
            this.orientation = (byte)i;
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Cannot add invalid orientation: ");
        sb.append(i);
        throw new IllegalArgumentException(sb.toString());
    }
    
    @Override
    public void mark(final int n) {
        throw new UnsupportedOperationException();
    }
    
    @Override
    public boolean markSupported() {
        return false;
    }
    
    @Override
    public int read() throws IOException {
        final int position = this.position;
        int n = 0;
        Label_0055: {
            if (position >= 2) {
                final int orientation_POSITION = ExifOrientationStream.ORIENTATION_POSITION;
                if (position <= orientation_POSITION) {
                    if (position == orientation_POSITION) {
                        n = this.orientation;
                        break Label_0055;
                    }
                    n = (ExifOrientationStream.EXIF_SEGMENT[position - 2] & 0xFF);
                    break Label_0055;
                }
            }
            n = super.read();
        }
        if (n != -1) {
            ++this.position;
        }
        return n;
    }
    
    @Override
    public int read(final byte[] array, int n, int min) throws IOException {
        final int position = this.position;
        final int orientation_POSITION = ExifOrientationStream.ORIENTATION_POSITION;
        if (position > orientation_POSITION) {
            n = super.read(array, n, min);
        }
        else if (position == orientation_POSITION) {
            array[n] = this.orientation;
            n = 1;
        }
        else if (position < 2) {
            n = super.read(array, n, 2 - position);
        }
        else {
            min = Math.min(orientation_POSITION - position, min);
            System.arraycopy(ExifOrientationStream.EXIF_SEGMENT, this.position - 2, array, n, min);
            n = min;
        }
        if (n > 0) {
            this.position += n;
        }
        return n;
    }
    
    @Override
    public void reset() throws IOException {
        throw new UnsupportedOperationException();
    }
    
    @Override
    public long skip(long skip) throws IOException {
        skip = super.skip(skip);
        if (skip > 0L) {
            this.position += (int)skip;
        }
        return skip;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide.load.data;

import java.io.IOException;
import com.bumptech.glide.load.engine.bitmap_recycle.ArrayPool;
import java.io.OutputStream;

public final class BufferedOutputStream extends OutputStream
{
    private ArrayPool arrayPool;
    private byte[] buffer;
    private int index;
    private final OutputStream out;
    
    public BufferedOutputStream(final OutputStream outputStream, final ArrayPool arrayPool) {
        this(outputStream, arrayPool, 65536);
    }
    
    BufferedOutputStream(final OutputStream out, final ArrayPool arrayPool, final int n) {
        this.out = out;
        this.arrayPool = arrayPool;
        this.buffer = arrayPool.get(n, byte[].class);
    }
    
    private void flushBuffer() throws IOException {
        final int index = this.index;
        if (index > 0) {
            this.out.write(this.buffer, 0, index);
            this.index = 0;
        }
    }
    
    private void maybeFlushBuffer() throws IOException {
        if (this.index == this.buffer.length) {
            this.flushBuffer();
        }
    }
    
    private void release() {
        final byte[] buffer = this.buffer;
        if (buffer != null) {
            this.arrayPool.put(buffer);
            this.buffer = null;
        }
    }
    
    @Override
    public void close() throws IOException {
        try {
            this.flush();
            this.out.close();
            this.release();
        }
        finally {
            this.out.close();
        }
    }
    
    @Override
    public void flush() throws IOException {
        this.flushBuffer();
        this.out.flush();
    }
    
    @Override
    public void write(final int n) throws IOException {
        this.buffer[this.index++] = (byte)n;
        this.maybeFlushBuffer();
    }
    
    @Override
    public void write(final byte[] array) throws IOException {
        this.write(array, 0, array.length);
    }
    
    @Override
    public void write(final byte[] b, final int n, final int n2) throws IOException {
        int n3 = 0;
        int n4;
        do {
            final int n5 = n2 - n3;
            final int off = n + n3;
            final int index = this.index;
            if (index == 0 && n5 >= this.buffer.length) {
                this.out.write(b, off, n5);
                return;
            }
            final int min = Math.min(n5, this.buffer.length - index);
            System.arraycopy(b, off, this.buffer, this.index, min);
            this.index += min;
            n4 = n3 + min;
            this.maybeFlushBuffer();
        } while ((n3 = n4) < n2);
    }
}

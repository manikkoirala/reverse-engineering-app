// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide.load.data;

import com.bumptech.glide.util.LogTime;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.DataSource;
import java.net.URISyntaxException;
import java.net.MalformedURLException;
import com.bumptech.glide.util.ContentLengthInputStream;
import android.text.TextUtils;
import android.util.Log;
import java.util.Iterator;
import java.io.IOException;
import com.bumptech.glide.load.HttpException;
import java.util.Map;
import java.net.URL;
import java.net.HttpURLConnection;
import com.bumptech.glide.load.model.GlideUrl;
import java.io.InputStream;

public class HttpUrlFetcher implements DataFetcher<InputStream>
{
    static final HttpUrlConnectionFactory DEFAULT_CONNECTION_FACTORY;
    static final int INVALID_STATUS_CODE = -1;
    private static final int MAXIMUM_REDIRECTS = 5;
    static final String REDIRECT_HEADER_FIELD = "Location";
    private static final String TAG = "HttpUrlFetcher";
    private final HttpUrlConnectionFactory connectionFactory;
    private final GlideUrl glideUrl;
    private volatile boolean isCancelled;
    private InputStream stream;
    private final int timeout;
    private HttpURLConnection urlConnection;
    
    static {
        DEFAULT_CONNECTION_FACTORY = (HttpUrlConnectionFactory)new DefaultHttpUrlConnectionFactory();
    }
    
    public HttpUrlFetcher(final GlideUrl glideUrl, final int n) {
        this(glideUrl, n, HttpUrlFetcher.DEFAULT_CONNECTION_FACTORY);
    }
    
    HttpUrlFetcher(final GlideUrl glideUrl, final int timeout, final HttpUrlConnectionFactory connectionFactory) {
        this.glideUrl = glideUrl;
        this.timeout = timeout;
        this.connectionFactory = connectionFactory;
    }
    
    private HttpURLConnection buildAndConfigureConnection(final URL url, final Map<String, String> map) throws HttpException {
        try {
            final HttpURLConnection build = this.connectionFactory.build(url);
            for (final Map.Entry entry : map.entrySet()) {
                build.addRequestProperty((String)entry.getKey(), (String)entry.getValue());
            }
            build.setConnectTimeout(this.timeout);
            build.setReadTimeout(this.timeout);
            build.setUseCaches(false);
            build.setDoInput(true);
            build.setInstanceFollowRedirects(false);
            return build;
        }
        catch (final IOException ex) {
            throw new HttpException("URL.openConnection threw", 0, ex);
        }
    }
    
    private static int getHttpStatusCodeOrInvalid(final HttpURLConnection httpURLConnection) {
        try {
            return httpURLConnection.getResponseCode();
        }
        catch (final IOException ex) {
            if (Log.isLoggable("HttpUrlFetcher", 3)) {
                Log.d("HttpUrlFetcher", "Failed to get a response code", (Throwable)ex);
            }
            return -1;
        }
    }
    
    private InputStream getStreamForSuccessfulRequest(final HttpURLConnection httpURLConnection) throws HttpException {
        try {
            if (TextUtils.isEmpty((CharSequence)httpURLConnection.getContentEncoding())) {
                this.stream = ContentLengthInputStream.obtain(httpURLConnection.getInputStream(), httpURLConnection.getContentLength());
            }
            else {
                if (Log.isLoggable("HttpUrlFetcher", 3)) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Got non empty content encoding: ");
                    sb.append(httpURLConnection.getContentEncoding());
                    Log.d("HttpUrlFetcher", sb.toString());
                }
                this.stream = httpURLConnection.getInputStream();
            }
            return this.stream;
        }
        catch (final IOException ex) {
            throw new HttpException("Failed to obtain InputStream", getHttpStatusCodeOrInvalid(httpURLConnection), ex);
        }
    }
    
    private static boolean isHttpOk(final int n) {
        return n / 100 == 2;
    }
    
    private static boolean isHttpRedirect(final int n) {
        return n / 100 == 3;
    }
    
    private InputStream loadDataWithRedirects(final URL context, final int n, URL headerField, Map<String, String> sb) throws HttpException {
        Label_0278: {
            if (n >= 5) {
                break Label_0278;
            }
            while (true) {
                if (headerField == null) {
                    break Label_0039;
                }
                try {
                    if (!((URL)context).toURI().equals(headerField.toURI())) {
                        final HttpURLConnection buildAndConfigureConnection = this.buildAndConfigureConnection((URL)context, (Map<String, String>)sb);
                        this.urlConnection = buildAndConfigureConnection;
                        try {
                            buildAndConfigureConnection.connect();
                            this.stream = this.urlConnection.getInputStream();
                            if (this.isCancelled) {
                                return null;
                            }
                            final int httpStatusCodeOrInvalid = getHttpStatusCodeOrInvalid(this.urlConnection);
                            if (isHttpOk(httpStatusCodeOrInvalid)) {
                                return this.getStreamForSuccessfulRequest(this.urlConnection);
                            }
                            if (isHttpRedirect(httpStatusCodeOrInvalid)) {
                                headerField = (URL)this.urlConnection.getHeaderField("Location");
                                if (!TextUtils.isEmpty((CharSequence)headerField)) {
                                    try {
                                        final URL url = new URL((URL)context, (String)headerField);
                                        this.cleanup();
                                        return this.loadDataWithRedirects(url, n + 1, (URL)context, (Map<String, String>)sb);
                                    }
                                    catch (final MalformedURLException ex) {
                                        sb = new StringBuilder();
                                        sb.append("Bad redirect url: ");
                                        sb.append((String)headerField);
                                        throw new HttpException(sb.toString(), httpStatusCodeOrInvalid, ex);
                                    }
                                }
                                throw new HttpException("Received empty or null redirect url", httpStatusCodeOrInvalid);
                            }
                            if (httpStatusCodeOrInvalid == -1) {
                                throw new HttpException(httpStatusCodeOrInvalid);
                            }
                            try {
                                throw new HttpException(this.urlConnection.getResponseMessage(), httpStatusCodeOrInvalid);
                            }
                            catch (final IOException ex2) {
                                throw new HttpException("Failed to get a response message", httpStatusCodeOrInvalid, ex2);
                            }
                        }
                        catch (final IOException context) {
                            throw new HttpException("Failed to connect or obtain data", getHttpStatusCodeOrInvalid(this.urlConnection), context);
                        }
                        throw new HttpException("Too many (> 5) redirects!", -1);
                    }
                    throw new HttpException("In re-direct loop", -1);
                }
                catch (final URISyntaxException ex3) {
                    continue;
                }
                break;
            }
        }
    }
    
    @Override
    public void cancel() {
        this.isCancelled = true;
    }
    
    @Override
    public void cleanup() {
        final InputStream stream = this.stream;
        if (stream != null) {
            try {
                stream.close();
            }
            catch (final IOException ex) {}
        }
        final HttpURLConnection urlConnection = this.urlConnection;
        if (urlConnection != null) {
            urlConnection.disconnect();
        }
        this.urlConnection = null;
    }
    
    @Override
    public Class<InputStream> getDataClass() {
        return InputStream.class;
    }
    
    @Override
    public DataSource getDataSource() {
        return DataSource.REMOTE;
    }
    
    @Override
    public void loadData(final Priority priority, final DataCallback<? super InputStream> dataCallback) {
        final long logTime = LogTime.getLogTime();
        try {
            Label_0098: {
                try {
                    dataCallback.onDataReady(this.loadDataWithRedirects(this.glideUrl.toURL(), 0, null, this.glideUrl.getHeaders()));
                    if (Log.isLoggable("HttpUrlFetcher", 2)) {
                        final StringBuilder sb = new StringBuilder();
                        break Label_0098;
                    }
                }
                finally {
                    if (Log.isLoggable("HttpUrlFetcher", 2)) {
                        final StringBuilder sb2 = new StringBuilder();
                        sb2.append("Finished http url fetcher fetch in ");
                        sb2.append(LogTime.getElapsedMillis(logTime));
                        Log.v("HttpUrlFetcher", sb2.toString());
                    }
                    final IOException ex;
                    Log.d("HttpUrlFetcher", "Failed to load data for url", (Throwable)ex);
                    dataCallback.onLoadFailed(ex);
                    iftrue(Label_0125:)(!Log.isLoggable("HttpUrlFetcher", 2));
                    final StringBuilder sb = new StringBuilder();
                    break Label_0098;
                    Label_0125: {
                        return;
                    }
                    sb.append("Finished http url fetcher fetch in ");
                    sb.append(LogTime.getElapsedMillis(logTime));
                    Log.v("HttpUrlFetcher", sb.toString());
                }
            }
        }
        catch (final IOException ex2) {}
    }
    
    private static class DefaultHttpUrlConnectionFactory implements HttpUrlConnectionFactory
    {
        DefaultHttpUrlConnectionFactory() {
        }
        
        @Override
        public HttpURLConnection build(final URL url) throws IOException {
            return (HttpURLConnection)url.openConnection();
        }
    }
    
    interface HttpUrlConnectionFactory
    {
        HttpURLConnection build(final URL p0) throws IOException;
    }
}

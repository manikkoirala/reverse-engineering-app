// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide.load.data;

import java.io.IOException;
import android.content.res.AssetManager;
import android.content.res.AssetFileDescriptor;

public class FileDescriptorAssetPathFetcher extends AssetPathFetcher<AssetFileDescriptor>
{
    public FileDescriptorAssetPathFetcher(final AssetManager assetManager, final String s) {
        super(assetManager, s);
    }
    
    @Override
    protected void close(final AssetFileDescriptor assetFileDescriptor) throws IOException {
        assetFileDescriptor.close();
    }
    
    @Override
    public Class<AssetFileDescriptor> getDataClass() {
        return AssetFileDescriptor.class;
    }
    
    @Override
    protected AssetFileDescriptor loadResource(final AssetManager assetManager, final String s) throws IOException {
        return assetManager.openFd(s);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide.load.data;

import java.io.FileNotFoundException;
import java.io.IOException;
import android.net.Uri;
import android.content.ContentResolver;
import android.content.res.AssetFileDescriptor;

public final class AssetFileDescriptorLocalUriFetcher extends LocalUriFetcher<AssetFileDescriptor>
{
    public AssetFileDescriptorLocalUriFetcher(final ContentResolver contentResolver, final Uri uri) {
        super(contentResolver, uri);
    }
    
    @Override
    protected void close(final AssetFileDescriptor assetFileDescriptor) throws IOException {
        assetFileDescriptor.close();
    }
    
    @Override
    public Class<AssetFileDescriptor> getDataClass() {
        return AssetFileDescriptor.class;
    }
    
    @Override
    protected AssetFileDescriptor loadResource(final Uri obj, final ContentResolver contentResolver) throws FileNotFoundException {
        final AssetFileDescriptor openAssetFileDescriptor = contentResolver.openAssetFileDescriptor(obj, "r");
        if (openAssetFileDescriptor != null) {
            return openAssetFileDescriptor;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("FileDescriptor is null for: ");
        sb.append(obj);
        throw new FileNotFoundException(sb.toString());
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide.load.data;

import android.system.ErrnoException;
import android.system.Os;
import android.system.OsConstants;
import java.io.IOException;
import android.os.Build;
import android.os.Build$VERSION;
import android.os.ParcelFileDescriptor;

public final class ParcelFileDescriptorRewinder implements DataRewinder<ParcelFileDescriptor>
{
    private final InternalRewinder rewinder;
    
    public ParcelFileDescriptorRewinder(final ParcelFileDescriptor parcelFileDescriptor) {
        this.rewinder = new InternalRewinder(parcelFileDescriptor);
    }
    
    public static boolean isSupported() {
        return Build$VERSION.SDK_INT >= 21 && !"robolectric".equals(Build.FINGERPRINT);
    }
    
    @Override
    public void cleanup() {
    }
    
    @Override
    public ParcelFileDescriptor rewindAndGet() throws IOException {
        return this.rewinder.rewind();
    }
    
    public static final class Factory implements DataRewinder.Factory<ParcelFileDescriptor>
    {
        public DataRewinder<ParcelFileDescriptor> build(final ParcelFileDescriptor parcelFileDescriptor) {
            return new ParcelFileDescriptorRewinder(parcelFileDescriptor);
        }
        
        @Override
        public Class<ParcelFileDescriptor> getDataClass() {
            return ParcelFileDescriptor.class;
        }
    }
    
    private static final class InternalRewinder
    {
        private final ParcelFileDescriptor parcelFileDescriptor;
        
        InternalRewinder(final ParcelFileDescriptor parcelFileDescriptor) {
            this.parcelFileDescriptor = parcelFileDescriptor;
        }
        
        ParcelFileDescriptor rewind() throws IOException {
            try {
                Os.lseek(this.parcelFileDescriptor.getFileDescriptor(), 0L, OsConstants.SEEK_SET);
                return this.parcelFileDescriptor;
            }
            catch (final ErrnoException cause) {
                throw new IOException((Throwable)cause);
            }
        }
    }
}

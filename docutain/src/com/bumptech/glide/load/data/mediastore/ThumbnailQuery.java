// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide.load.data.mediastore;

import android.database.Cursor;
import android.net.Uri;

interface ThumbnailQuery
{
    Cursor query(final Uri p0);
}

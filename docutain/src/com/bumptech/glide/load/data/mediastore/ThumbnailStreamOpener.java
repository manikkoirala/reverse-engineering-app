// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide.load.data.mediastore;

import java.io.FileNotFoundException;
import android.text.TextUtils;
import java.io.InputStream;
import java.io.File;
import android.net.Uri;
import com.bumptech.glide.load.ImageHeaderParser;
import java.util.List;
import android.content.ContentResolver;
import com.bumptech.glide.load.engine.bitmap_recycle.ArrayPool;

class ThumbnailStreamOpener
{
    private static final FileService DEFAULT_SERVICE;
    private static final String TAG = "ThumbStreamOpener";
    private final ArrayPool byteArrayPool;
    private final ContentResolver contentResolver;
    private final List<ImageHeaderParser> parsers;
    private final ThumbnailQuery query;
    private final FileService service;
    
    static {
        DEFAULT_SERVICE = new FileService();
    }
    
    ThumbnailStreamOpener(final List<ImageHeaderParser> parsers, final FileService service, final ThumbnailQuery query, final ArrayPool byteArrayPool, final ContentResolver contentResolver) {
        this.service = service;
        this.query = query;
        this.byteArrayPool = byteArrayPool;
        this.contentResolver = contentResolver;
        this.parsers = parsers;
    }
    
    ThumbnailStreamOpener(final List<ImageHeaderParser> list, final ThumbnailQuery thumbnailQuery, final ArrayPool arrayPool, final ContentResolver contentResolver) {
        this(list, ThumbnailStreamOpener.DEFAULT_SERVICE, thumbnailQuery, arrayPool, contentResolver);
    }
    
    private String getPath(final Uri p0) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: astore_2       
        //     2: aload_0        
        //     3: getfield        com/bumptech/glide/load/data/mediastore/ThumbnailStreamOpener.query:Lcom/bumptech/glide/load/data/mediastore/ThumbnailQuery;
        //     6: aload_1        
        //     7: invokeinterface com/bumptech/glide/load/data/mediastore/ThumbnailQuery.query:(Landroid/net/Uri;)Landroid/database/Cursor;
        //    12: astore_3       
        //    13: aload_3        
        //    14: ifnull          57
        //    17: aload_3        
        //    18: astore_2       
        //    19: aload_3        
        //    20: invokeinterface android/database/Cursor.moveToFirst:()Z
        //    25: ifeq            57
        //    28: aload_3        
        //    29: astore_2       
        //    30: aload_3        
        //    31: iconst_0       
        //    32: invokeinterface android/database/Cursor.getString:(I)Ljava/lang/String;
        //    37: astore          4
        //    39: aload_3        
        //    40: ifnull          49
        //    43: aload_3        
        //    44: invokeinterface android/database/Cursor.close:()V
        //    49: aload           4
        //    51: areturn        
        //    52: astore          4
        //    54: goto            77
        //    57: aload_3        
        //    58: ifnull          67
        //    61: aload_3        
        //    62: invokeinterface android/database/Cursor.close:()V
        //    67: aconst_null    
        //    68: areturn        
        //    69: astore_1       
        //    70: goto            149
        //    73: astore          4
        //    75: aconst_null    
        //    76: astore_3       
        //    77: aload_3        
        //    78: astore_2       
        //    79: ldc             "ThumbStreamOpener"
        //    81: iconst_3       
        //    82: invokestatic    android/util/Log.isLoggable:(Ljava/lang/String;I)Z
        //    85: ifeq            136
        //    88: aload_3        
        //    89: astore_2       
        //    90: new             Ljava/lang/StringBuilder;
        //    93: astore          5
        //    95: aload_3        
        //    96: astore_2       
        //    97: aload           5
        //    99: invokespecial   java/lang/StringBuilder.<init>:()V
        //   102: aload_3        
        //   103: astore_2       
        //   104: aload           5
        //   106: ldc             "Failed to query for thumbnail for Uri: "
        //   108: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   111: pop            
        //   112: aload_3        
        //   113: astore_2       
        //   114: aload           5
        //   116: aload_1        
        //   117: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //   120: pop            
        //   121: aload_3        
        //   122: astore_2       
        //   123: ldc             "ThumbStreamOpener"
        //   125: aload           5
        //   127: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   130: aload           4
        //   132: invokestatic    android/util/Log.d:(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //   135: pop            
        //   136: aload_3        
        //   137: ifnull          146
        //   140: aload_3        
        //   141: invokeinterface android/database/Cursor.close:()V
        //   146: aconst_null    
        //   147: areturn        
        //   148: astore_1       
        //   149: aload_2        
        //   150: ifnull          159
        //   153: aload_2        
        //   154: invokeinterface android/database/Cursor.close:()V
        //   159: aload_1        
        //   160: athrow         
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                         
        //  -----  -----  -----  -----  -----------------------------
        //  2      13     73     77     Ljava/lang/SecurityException;
        //  2      13     69     73     Any
        //  19     28     52     57     Ljava/lang/SecurityException;
        //  19     28     148    149    Any
        //  30     39     52     57     Ljava/lang/SecurityException;
        //  30     39     148    149    Any
        //  79     88     148    149    Any
        //  90     95     148    149    Any
        //  97     102    148    149    Any
        //  104    112    148    149    Any
        //  114    121    148    149    Any
        //  123    136    148    149    Any
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0049:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2604)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:206)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    private boolean isValid(final File file) {
        return this.service.exists(file) && 0L < this.service.length(file);
    }
    
    int getOrientation(final Uri p0) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     3: aconst_null    
        //     4: astore          5
        //     6: aconst_null    
        //     7: astore_3       
        //     8: aload_0        
        //     9: getfield        com/bumptech/glide/load/data/mediastore/ThumbnailStreamOpener.contentResolver:Landroid/content/ContentResolver;
        //    12: aload_1        
        //    13: invokevirtual   android/content/ContentResolver.openInputStream:(Landroid/net/Uri;)Ljava/io/InputStream;
        //    16: astore          6
        //    18: aload           6
        //    20: astore_3       
        //    21: aload           6
        //    23: astore          4
        //    25: aload           6
        //    27: astore          5
        //    29: aload_0        
        //    30: getfield        com/bumptech/glide/load/data/mediastore/ThumbnailStreamOpener.parsers:Ljava/util/List;
        //    33: aload           6
        //    35: aload_0        
        //    36: getfield        com/bumptech/glide/load/data/mediastore/ThumbnailStreamOpener.byteArrayPool:Lcom/bumptech/glide/load/engine/bitmap_recycle/ArrayPool;
        //    39: invokestatic    com/bumptech/glide/load/ImageHeaderParserUtils.getOrientation:(Ljava/util/List;Ljava/io/InputStream;Lcom/bumptech/glide/load/engine/bitmap_recycle/ArrayPool;)I
        //    42: istore_2       
        //    43: aload           6
        //    45: ifnull          53
        //    48: aload           6
        //    50: invokevirtual   java/io/InputStream.close:()V
        //    53: iload_2        
        //    54: ireturn        
        //    55: astore_1       
        //    56: goto            147
        //    59: astore          6
        //    61: goto            70
        //    64: astore          6
        //    66: aload           5
        //    68: astore          4
        //    70: aload           4
        //    72: astore_3       
        //    73: ldc             "ThumbStreamOpener"
        //    75: iconst_3       
        //    76: invokestatic    android/util/Log.isLoggable:(Ljava/lang/String;I)Z
        //    79: ifeq            135
        //    82: aload           4
        //    84: astore_3       
        //    85: new             Ljava/lang/StringBuilder;
        //    88: astore          5
        //    90: aload           4
        //    92: astore_3       
        //    93: aload           5
        //    95: invokespecial   java/lang/StringBuilder.<init>:()V
        //    98: aload           4
        //   100: astore_3       
        //   101: aload           5
        //   103: ldc             "Failed to open uri: "
        //   105: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   108: pop            
        //   109: aload           4
        //   111: astore_3       
        //   112: aload           5
        //   114: aload_1        
        //   115: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //   118: pop            
        //   119: aload           4
        //   121: astore_3       
        //   122: ldc             "ThumbStreamOpener"
        //   124: aload           5
        //   126: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   129: aload           6
        //   131: invokestatic    android/util/Log.d:(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //   134: pop            
        //   135: aload           4
        //   137: ifnull          145
        //   140: aload           4
        //   142: invokevirtual   java/io/InputStream.close:()V
        //   145: iconst_m1      
        //   146: ireturn        
        //   147: aload_3        
        //   148: ifnull          155
        //   151: aload_3        
        //   152: invokevirtual   java/io/InputStream.close:()V
        //   155: aload_1        
        //   156: athrow         
        //   157: astore_1       
        //   158: goto            53
        //   161: astore_1       
        //   162: goto            145
        //   165: astore_3       
        //   166: goto            155
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                            
        //  -----  -----  -----  -----  --------------------------------
        //  8      18     64     70     Ljava/io/IOException;
        //  8      18     59     64     Ljava/lang/NullPointerException;
        //  8      18     55     157    Any
        //  29     43     64     70     Ljava/io/IOException;
        //  29     43     59     64     Ljava/lang/NullPointerException;
        //  29     43     55     157    Any
        //  48     53     157    161    Ljava/io/IOException;
        //  73     82     55     157    Any
        //  85     90     55     157    Any
        //  93     98     55     157    Any
        //  101    109    55     157    Any
        //  112    119    55     157    Any
        //  122    135    55     157    Any
        //  140    145    161    165    Ljava/io/IOException;
        //  151    155    165    169    Ljava/io/IOException;
        // 
        // The error that occurred was:
        // 
        // java.util.ConcurrentModificationException
        //     at java.base/java.util.ArrayList$Itr.checkForComodification(ArrayList.java:1013)
        //     at java.base/java.util.ArrayList$Itr.next(ArrayList.java:967)
        //     at com.strobel.decompiler.ast.AstBuilder.convertLocalVariables(AstBuilder.java:2913)
        //     at com.strobel.decompiler.ast.AstBuilder.performStackAnalysis(AstBuilder.java:2501)
        //     at com.strobel.decompiler.ast.AstBuilder.build(AstBuilder.java:108)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:203)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public InputStream open(final Uri obj) throws FileNotFoundException {
        final String path = this.getPath(obj);
        if (TextUtils.isEmpty((CharSequence)path)) {
            return null;
        }
        final File value = this.service.get(path);
        if (!this.isValid(value)) {
            return null;
        }
        final Uri fromFile = Uri.fromFile(value);
        try {
            return this.contentResolver.openInputStream(fromFile);
        }
        catch (final NullPointerException cause) {
            final StringBuilder sb = new StringBuilder();
            sb.append("NPE opening uri: ");
            sb.append(obj);
            sb.append(" -> ");
            sb.append(fromFile);
            throw (FileNotFoundException)new FileNotFoundException(sb.toString()).initCause(cause);
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide.load.data.mediastore;

import android.net.Uri;

public final class MediaStoreUtil
{
    private static final int MINI_THUMB_HEIGHT = 384;
    private static final int MINI_THUMB_WIDTH = 512;
    
    private MediaStoreUtil() {
    }
    
    public static boolean isMediaStoreImageUri(final Uri uri) {
        return isMediaStoreUri(uri) && !isVideoUri(uri);
    }
    
    public static boolean isMediaStoreUri(final Uri uri) {
        return uri != null && "content".equals(uri.getScheme()) && "media".equals(uri.getAuthority());
    }
    
    public static boolean isMediaStoreVideoUri(final Uri uri) {
        return isMediaStoreUri(uri) && isVideoUri(uri);
    }
    
    public static boolean isThumbnailSize(final int n, final int n2) {
        return n != Integer.MIN_VALUE && n2 != Integer.MIN_VALUE && n <= 512 && n2 <= 384;
    }
    
    private static boolean isVideoUri(final Uri uri) {
        return uri.getPathSegments().contains("video");
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide.load.data;

import java.io.IOException;
import java.io.FileNotFoundException;
import android.provider.ContactsContract$Contacts;
import android.net.Uri;
import android.content.ContentResolver;
import android.content.UriMatcher;
import java.io.InputStream;

public class StreamLocalUriFetcher extends LocalUriFetcher<InputStream>
{
    private static final int ID_CONTACTS_CONTACT = 3;
    private static final int ID_CONTACTS_LOOKUP = 1;
    private static final int ID_CONTACTS_PHOTO = 4;
    private static final int ID_CONTACTS_THUMBNAIL = 2;
    private static final int ID_LOOKUP_BY_PHONE = 5;
    private static final UriMatcher URI_MATCHER;
    
    static {
        final UriMatcher uri_MATCHER = new UriMatcher(-1);
        (URI_MATCHER = uri_MATCHER).addURI("com.android.contacts", "contacts/lookup/*/#", 1);
        uri_MATCHER.addURI("com.android.contacts", "contacts/lookup/*", 1);
        uri_MATCHER.addURI("com.android.contacts", "contacts/#/photo", 2);
        uri_MATCHER.addURI("com.android.contacts", "contacts/#", 3);
        uri_MATCHER.addURI("com.android.contacts", "contacts/#/display_photo", 4);
        uri_MATCHER.addURI("com.android.contacts", "phone_lookup/*", 5);
    }
    
    public StreamLocalUriFetcher(final ContentResolver contentResolver, final Uri uri) {
        super(contentResolver, uri);
    }
    
    private InputStream loadResourceFromUri(Uri lookupContact, final ContentResolver contentResolver) throws FileNotFoundException {
        final int match = StreamLocalUriFetcher.URI_MATCHER.match(lookupContact);
        if (match != 1) {
            if (match == 3) {
                return this.openContactPhotoInputStream(contentResolver, lookupContact);
            }
            if (match != 5) {
                return contentResolver.openInputStream(lookupContact);
            }
        }
        lookupContact = ContactsContract$Contacts.lookupContact(contentResolver, lookupContact);
        if (lookupContact != null) {
            return this.openContactPhotoInputStream(contentResolver, lookupContact);
        }
        throw new FileNotFoundException("Contact cannot be found");
    }
    
    private InputStream openContactPhotoInputStream(final ContentResolver contentResolver, final Uri uri) {
        return ContactsContract$Contacts.openContactPhotoInputStream(contentResolver, uri, true);
    }
    
    @Override
    protected void close(final InputStream inputStream) throws IOException {
        inputStream.close();
    }
    
    @Override
    public Class<InputStream> getDataClass() {
        return InputStream.class;
    }
    
    @Override
    protected InputStream loadResource(final Uri obj, final ContentResolver contentResolver) throws FileNotFoundException {
        final InputStream loadResourceFromUri = this.loadResourceFromUri(obj, contentResolver);
        if (loadResourceFromUri != null) {
            return loadResourceFromUri;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("InputStream is null for ");
        sb.append(obj);
        throw new FileNotFoundException(sb.toString());
    }
}

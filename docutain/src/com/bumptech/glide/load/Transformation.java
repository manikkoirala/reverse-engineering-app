// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide.load;

import com.bumptech.glide.load.engine.Resource;
import android.content.Context;

public interface Transformation<T> extends Key
{
    Resource<T> transform(final Context p0, final Resource<T> p1, final int p2, final int p3);
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide.load.resource.transcode;

import com.bumptech.glide.load.resource.bitmap.LazyBitmapDrawableResource;
import com.bumptech.glide.load.Options;
import com.bumptech.glide.load.engine.Resource;
import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPool;
import com.bumptech.glide.util.Preconditions;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.Bitmap;

public class BitmapDrawableTranscoder implements ResourceTranscoder<Bitmap, BitmapDrawable>
{
    private final Resources resources;
    
    public BitmapDrawableTranscoder(final Context context) {
        this(context.getResources());
    }
    
    public BitmapDrawableTranscoder(final Resources resources) {
        this.resources = Preconditions.checkNotNull(resources);
    }
    
    @Deprecated
    public BitmapDrawableTranscoder(final Resources resources, final BitmapPool bitmapPool) {
        this(resources);
    }
    
    @Override
    public Resource<BitmapDrawable> transcode(final Resource<Bitmap> resource, final Options options) {
        return LazyBitmapDrawableResource.obtain(this.resources, resource);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide.load.resource.transcode;

import com.bumptech.glide.load.resource.bytes.BytesResource;
import com.bumptech.glide.util.ByteBufferUtil;
import com.bumptech.glide.load.Options;
import com.bumptech.glide.load.engine.Resource;
import com.bumptech.glide.load.resource.gif.GifDrawable;

public class GifDrawableBytesTranscoder implements ResourceTranscoder<GifDrawable, byte[]>
{
    @Override
    public Resource<byte[]> transcode(final Resource<GifDrawable> resource, final Options options) {
        return new BytesResource(ByteBufferUtil.toBytes(resource.get().getBuffer()));
    }
}

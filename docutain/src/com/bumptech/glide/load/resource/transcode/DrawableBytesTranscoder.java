// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide.load.resource.transcode;

import com.bumptech.glide.load.resource.bitmap.BitmapResource;
import android.graphics.drawable.BitmapDrawable;
import com.bumptech.glide.load.Options;
import com.bumptech.glide.load.engine.Resource;
import com.bumptech.glide.load.resource.gif.GifDrawable;
import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPool;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;

public final class DrawableBytesTranscoder implements ResourceTranscoder<Drawable, byte[]>
{
    private final ResourceTranscoder<Bitmap, byte[]> bitmapBytesTranscoder;
    private final BitmapPool bitmapPool;
    private final ResourceTranscoder<GifDrawable, byte[]> gifDrawableBytesTranscoder;
    
    public DrawableBytesTranscoder(final BitmapPool bitmapPool, final ResourceTranscoder<Bitmap, byte[]> bitmapBytesTranscoder, final ResourceTranscoder<GifDrawable, byte[]> gifDrawableBytesTranscoder) {
        this.bitmapPool = bitmapPool;
        this.bitmapBytesTranscoder = bitmapBytesTranscoder;
        this.gifDrawableBytesTranscoder = gifDrawableBytesTranscoder;
    }
    
    private static Resource<GifDrawable> toGifDrawableResource(final Resource<Drawable> resource) {
        return (Resource<GifDrawable>)resource;
    }
    
    @Override
    public Resource<byte[]> transcode(final Resource<Drawable> resource, final Options options) {
        final Drawable drawable = resource.get();
        if (drawable instanceof BitmapDrawable) {
            return this.bitmapBytesTranscoder.transcode(BitmapResource.obtain(((BitmapDrawable)drawable).getBitmap(), this.bitmapPool), options);
        }
        if (drawable instanceof GifDrawable) {
            return this.gifDrawableBytesTranscoder.transcode(toGifDrawableResource(resource), options);
        }
        return null;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide.load.resource.drawable;

import com.bumptech.glide.load.resource.gif.GifDrawable;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable$ConstantState;
import com.bumptech.glide.util.Preconditions;
import com.bumptech.glide.load.engine.Initializable;
import com.bumptech.glide.load.engine.Resource;
import android.graphics.drawable.Drawable;

public abstract class DrawableResource<T extends Drawable> implements Resource<T>, Initializable
{
    protected final T drawable;
    
    public DrawableResource(final T t) {
        this.drawable = Preconditions.checkNotNull(t);
    }
    
    @Override
    public final T get() {
        final Drawable$ConstantState constantState = this.drawable.getConstantState();
        if (constantState == null) {
            return this.drawable;
        }
        return (T)constantState.newDrawable();
    }
    
    @Override
    public void initialize() {
        final Drawable drawable = this.drawable;
        if (drawable instanceof BitmapDrawable) {
            ((BitmapDrawable)drawable).getBitmap().prepareToDraw();
        }
        else if (drawable instanceof GifDrawable) {
            ((GifDrawable)drawable).getFirstFrame().prepareToDraw();
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide.load.resource.drawable;

import java.io.IOException;
import com.bumptech.glide.load.engine.Resource;
import com.bumptech.glide.load.Options;
import android.graphics.drawable.Drawable;
import com.bumptech.glide.load.ResourceDecoder;

public class UnitDrawableDecoder implements ResourceDecoder<Drawable, Drawable>
{
    @Override
    public Resource<Drawable> decode(final Drawable drawable, final int n, final int n2, final Options options) {
        return NonOwnedDrawableResource.newInstance(drawable);
    }
    
    @Override
    public boolean handles(final Drawable drawable, final Options options) {
        return true;
    }
}

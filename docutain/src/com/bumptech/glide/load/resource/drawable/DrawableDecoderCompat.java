// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide.load.resource.drawable;

import androidx.appcompat.content.res.AppCompatResources;
import androidx.appcompat.view.ContextThemeWrapper;
import android.os.Build$VERSION;
import androidx.core.content.res.ResourcesCompat;
import androidx.core.content.ContextCompat;
import android.content.res.Resources$NotFoundException;
import android.graphics.drawable.Drawable;
import android.content.res.Resources$Theme;
import android.content.Context;

public final class DrawableDecoderCompat
{
    private static volatile boolean shouldCallAppCompatResources = true;
    
    private DrawableDecoderCompat() {
    }
    
    public static Drawable getDrawable(final Context context, final int n, final Resources$Theme resources$Theme) {
        return getDrawable(context, context, n, resources$Theme);
    }
    
    public static Drawable getDrawable(final Context context, final Context context2, final int n) {
        return getDrawable(context, context2, n, null);
    }
    
    private static Drawable getDrawable(final Context context, final Context context2, final int n, Resources$Theme theme) {
        try {
            if (DrawableDecoderCompat.shouldCallAppCompatResources) {
                return loadDrawableV7(context2, n, theme);
            }
        }
        catch (final Resources$NotFoundException ex) {}
        catch (final IllegalStateException ex2) {
            if (!context.getPackageName().equals(context2.getPackageName())) {
                return ContextCompat.getDrawable(context2, n);
            }
            throw ex2;
        }
        catch (final NoClassDefFoundError noClassDefFoundError) {
            DrawableDecoderCompat.shouldCallAppCompatResources = false;
        }
        if (theme == null) {
            theme = context2.getTheme();
        }
        return loadDrawableV4(context2, n, theme);
    }
    
    private static Drawable loadDrawableV4(final Context context, final int n, final Resources$Theme resources$Theme) {
        return ResourcesCompat.getDrawable(context.getResources(), n, resources$Theme);
    }
    
    private static Drawable loadDrawableV7(final Context context, final int n, final Resources$Theme resources$Theme) {
        Object o = context;
        if (resources$Theme != null) {
            o = context;
            if (Build$VERSION.SDK_INT >= 21) {
                o = new ContextThemeWrapper(context, resources$Theme);
                ((ContextThemeWrapper)o).applyOverrideConfiguration(resources$Theme.getResources().getConfiguration());
            }
        }
        return AppCompatResources.getDrawable((Context)o, n);
    }
}

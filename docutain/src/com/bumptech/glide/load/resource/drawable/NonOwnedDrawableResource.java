// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide.load.resource.drawable;

import com.bumptech.glide.load.engine.Resource;
import android.graphics.drawable.Drawable;

final class NonOwnedDrawableResource extends DrawableResource<Drawable>
{
    private NonOwnedDrawableResource(final Drawable drawable) {
        super(drawable);
    }
    
    static Resource<Drawable> newInstance(final Drawable drawable) {
        NonOwnedDrawableResource nonOwnedDrawableResource;
        if (drawable != null) {
            nonOwnedDrawableResource = new NonOwnedDrawableResource(drawable);
        }
        else {
            nonOwnedDrawableResource = null;
        }
        return nonOwnedDrawableResource;
    }
    
    @Override
    public Class<Drawable> getResourceClass() {
        return (Class<Drawable>)this.drawable.getClass();
    }
    
    @Override
    public int getSize() {
        return Math.max(1, this.drawable.getIntrinsicWidth() * this.drawable.getIntrinsicHeight() * 4);
    }
    
    @Override
    public void recycle() {
    }
}

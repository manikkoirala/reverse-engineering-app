// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide.load.resource.drawable;

import java.io.IOException;
import com.bumptech.glide.util.Preconditions;
import android.text.TextUtils;
import com.bumptech.glide.load.engine.Resource;
import com.bumptech.glide.load.Options;
import android.content.res.Resources;
import java.util.List;
import android.content.pm.PackageManager$NameNotFoundException;
import android.content.Context;
import android.content.res.Resources$Theme;
import com.bumptech.glide.load.Option;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import com.bumptech.glide.load.ResourceDecoder;

public class ResourceDrawableDecoder implements ResourceDecoder<Uri, Drawable>
{
    private static final String ANDROID_PACKAGE_NAME = "android";
    private static final int ID_PATH_SEGMENTS = 1;
    private static final int MISSING_RESOURCE_ID = 0;
    private static final int NAME_PATH_SEGMENT_INDEX = 1;
    private static final int NAME_URI_PATH_SEGMENTS = 2;
    private static final int RESOURCE_ID_SEGMENT_INDEX = 0;
    public static final Option<Resources$Theme> THEME;
    private static final int TYPE_PATH_SEGMENT_INDEX = 0;
    private final Context context;
    
    static {
        THEME = Option.memory("com.bumptech.glide.load.resource.bitmap.Downsampler.Theme");
    }
    
    public ResourceDrawableDecoder(final Context context) {
        this.context = context.getApplicationContext();
    }
    
    private Context findContextForPackage(final Uri obj, final String s) {
        if (s.equals(this.context.getPackageName())) {
            return this.context;
        }
        try {
            return this.context.createPackageContext(s, 0);
        }
        catch (final PackageManager$NameNotFoundException cause) {
            if (s.contains(this.context.getPackageName())) {
                return this.context;
            }
            final StringBuilder sb = new StringBuilder();
            sb.append("Failed to obtain context or unrecognized Uri format for: ");
            sb.append(obj);
            throw new IllegalArgumentException(sb.toString(), (Throwable)cause);
        }
    }
    
    private int findResourceIdFromResourceIdUri(final Uri obj) {
        final List pathSegments = obj.getPathSegments();
        try {
            return Integer.parseInt((String)pathSegments.get(0));
        }
        catch (final NumberFormatException cause) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Unrecognized Uri format: ");
            sb.append(obj);
            throw new IllegalArgumentException(sb.toString(), cause);
        }
    }
    
    private int findResourceIdFromTypeAndNameResourceUri(final Context context, final Uri obj) {
        final List pathSegments = obj.getPathSegments();
        final String authority = obj.getAuthority();
        final String s = pathSegments.get(0);
        final String s2 = pathSegments.get(1);
        int n;
        if ((n = context.getResources().getIdentifier(s2, s, authority)) == 0) {
            n = Resources.getSystem().getIdentifier(s2, s, "android");
        }
        if (n != 0) {
            return n;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Failed to find resource id for: ");
        sb.append(obj);
        throw new IllegalArgumentException(sb.toString());
    }
    
    private int findResourceIdFromUri(final Context context, final Uri obj) {
        final List pathSegments = obj.getPathSegments();
        if (pathSegments.size() == 2) {
            return this.findResourceIdFromTypeAndNameResourceUri(context, obj);
        }
        if (pathSegments.size() == 1) {
            return this.findResourceIdFromResourceIdUri(obj);
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Unrecognized Uri format: ");
        sb.append(obj);
        throw new IllegalArgumentException(sb.toString());
    }
    
    @Override
    public Resource<Drawable> decode(final Uri obj, int resourceIdFromUri, final int n, final Options options) {
        final String authority = obj.getAuthority();
        if (!TextUtils.isEmpty((CharSequence)authority)) {
            final Context contextForPackage = this.findContextForPackage(obj, authority);
            resourceIdFromUri = this.findResourceIdFromUri(contextForPackage, obj);
            Resources$Theme resources$Theme;
            if (Preconditions.checkNotNull(authority).equals(this.context.getPackageName())) {
                resources$Theme = options.get(ResourceDrawableDecoder.THEME);
            }
            else {
                resources$Theme = null;
            }
            Drawable drawable;
            if (resources$Theme == null) {
                drawable = DrawableDecoderCompat.getDrawable(this.context, contextForPackage, resourceIdFromUri);
            }
            else {
                drawable = DrawableDecoderCompat.getDrawable(this.context, resourceIdFromUri, resources$Theme);
            }
            return NonOwnedDrawableResource.newInstance(drawable);
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Package name for ");
        sb.append(obj);
        sb.append(" is null or empty");
        throw new IllegalStateException(sb.toString());
    }
    
    @Override
    public boolean handles(final Uri uri, final Options options) {
        final String scheme = uri.getScheme();
        return scheme != null && scheme.equals("android.resource");
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide.load.resource.drawable;

import com.bumptech.glide.request.transition.DrawableCrossFadeFactory;
import com.bumptech.glide.request.transition.TransitionFactory;
import android.graphics.drawable.Drawable;
import com.bumptech.glide.TransitionOptions;

public final class DrawableTransitionOptions extends TransitionOptions<DrawableTransitionOptions, Drawable>
{
    public static DrawableTransitionOptions with(final TransitionFactory<Drawable> transitionFactory) {
        return ((TransitionOptions<DrawableTransitionOptions, Object>)new DrawableTransitionOptions()).transition(transitionFactory);
    }
    
    public static DrawableTransitionOptions withCrossFade() {
        return new DrawableTransitionOptions().crossFade();
    }
    
    public static DrawableTransitionOptions withCrossFade(final int n) {
        return new DrawableTransitionOptions().crossFade(n);
    }
    
    public static DrawableTransitionOptions withCrossFade(final DrawableCrossFadeFactory.Builder builder) {
        return new DrawableTransitionOptions().crossFade(builder);
    }
    
    public static DrawableTransitionOptions withCrossFade(final DrawableCrossFadeFactory drawableCrossFadeFactory) {
        return new DrawableTransitionOptions().crossFade(drawableCrossFadeFactory);
    }
    
    public DrawableTransitionOptions crossFade() {
        return this.crossFade(new DrawableCrossFadeFactory.Builder());
    }
    
    public DrawableTransitionOptions crossFade(final int n) {
        return this.crossFade(new DrawableCrossFadeFactory.Builder(n));
    }
    
    public DrawableTransitionOptions crossFade(final DrawableCrossFadeFactory.Builder builder) {
        return this.crossFade(builder.build());
    }
    
    public DrawableTransitionOptions crossFade(final DrawableCrossFadeFactory drawableCrossFadeFactory) {
        return ((TransitionOptions<DrawableTransitionOptions, Object>)this).transition(drawableCrossFadeFactory);
    }
    
    @Override
    public boolean equals(final Object o) {
        return o instanceof DrawableTransitionOptions && super.equals(o);
    }
    
    @Override
    public int hashCode() {
        return super.hashCode();
    }
}

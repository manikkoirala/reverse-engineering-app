// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide.load.resource.drawable;

import com.bumptech.glide.util.ByteBufferUtil;
import com.bumptech.glide.util.Util;
import android.graphics.Bitmap$Config;
import com.bumptech.glide.load.ImageHeaderParserUtils;
import java.io.IOException;
import android.graphics.drawable.AnimatedImageDrawable;
import android.graphics.ImageDecoder$OnHeaderDecodedListener;
import android.graphics.ImageDecoder;
import com.bumptech.glide.load.resource.DefaultOnHeaderDecodedListener;
import com.bumptech.glide.load.engine.Resource;
import com.bumptech.glide.load.Options;
import android.graphics.ImageDecoder$Source;
import java.io.InputStream;
import android.graphics.drawable.Drawable;
import java.nio.ByteBuffer;
import com.bumptech.glide.load.ResourceDecoder;
import com.bumptech.glide.load.ImageHeaderParser;
import java.util.List;
import com.bumptech.glide.load.engine.bitmap_recycle.ArrayPool;

@Deprecated
public final class AnimatedWebpDecoder
{
    private final ArrayPool arrayPool;
    private final List<ImageHeaderParser> imageHeaderParsers;
    
    private AnimatedWebpDecoder(final List<ImageHeaderParser> imageHeaderParsers, final ArrayPool arrayPool) {
        this.imageHeaderParsers = imageHeaderParsers;
        this.arrayPool = arrayPool;
    }
    
    public static ResourceDecoder<ByteBuffer, Drawable> byteBufferDecoder(final List<ImageHeaderParser> list, final ArrayPool arrayPool) {
        return new ByteBufferAnimatedWebpDecoder(new AnimatedWebpDecoder(list, arrayPool));
    }
    
    private boolean isHandled(final ImageHeaderParser.ImageType imageType) {
        return imageType == ImageHeaderParser.ImageType.ANIMATED_WEBP;
    }
    
    public static ResourceDecoder<InputStream, Drawable> streamDecoder(final List<ImageHeaderParser> list, final ArrayPool arrayPool) {
        return new StreamAnimatedWebpDecoder(new AnimatedWebpDecoder(list, arrayPool));
    }
    
    Resource<Drawable> decode(final ImageDecoder$Source imageDecoder$Source, final int n, final int n2, final Options options) throws IOException {
        final Drawable decodeDrawable = ImageDecoder.decodeDrawable(imageDecoder$Source, (ImageDecoder$OnHeaderDecodedListener)new DefaultOnHeaderDecodedListener(n, n2, options));
        if (decodeDrawable instanceof AnimatedImageDrawable) {
            return new AnimatedImageDrawableResource((AnimatedImageDrawable)decodeDrawable);
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Received unexpected drawable type for animated webp, failing: ");
        sb.append(decodeDrawable);
        throw new IOException(sb.toString());
    }
    
    boolean handles(final InputStream inputStream) throws IOException {
        return this.isHandled(ImageHeaderParserUtils.getType(this.imageHeaderParsers, inputStream, this.arrayPool));
    }
    
    boolean handles(final ByteBuffer byteBuffer) throws IOException {
        return this.isHandled(ImageHeaderParserUtils.getType(this.imageHeaderParsers, byteBuffer));
    }
    
    private static final class AnimatedImageDrawableResource implements Resource<Drawable>
    {
        private static final int ESTIMATED_NUMBER_OF_FRAMES = 2;
        private final AnimatedImageDrawable imageDrawable;
        
        AnimatedImageDrawableResource(final AnimatedImageDrawable imageDrawable) {
            this.imageDrawable = imageDrawable;
        }
        
        public AnimatedImageDrawable get() {
            return this.imageDrawable;
        }
        
        @Override
        public Class<Drawable> getResourceClass() {
            return Drawable.class;
        }
        
        @Override
        public int getSize() {
            return this.imageDrawable.getIntrinsicWidth() * this.imageDrawable.getIntrinsicHeight() * Util.getBytesPerPixel(Bitmap$Config.ARGB_8888) * 2;
        }
        
        @Override
        public void recycle() {
            this.imageDrawable.stop();
            this.imageDrawable.clearAnimationCallbacks();
        }
    }
    
    private static final class ByteBufferAnimatedWebpDecoder implements ResourceDecoder<ByteBuffer, Drawable>
    {
        private final AnimatedWebpDecoder delegate;
        
        ByteBufferAnimatedWebpDecoder(final AnimatedWebpDecoder delegate) {
            this.delegate = delegate;
        }
        
        @Override
        public Resource<Drawable> decode(final ByteBuffer byteBuffer, final int n, final int n2, final Options options) throws IOException {
            return this.delegate.decode(ImageDecoder.createSource(byteBuffer), n, n2, options);
        }
        
        @Override
        public boolean handles(final ByteBuffer byteBuffer, final Options options) throws IOException {
            return this.delegate.handles(byteBuffer);
        }
    }
    
    private static final class StreamAnimatedWebpDecoder implements ResourceDecoder<InputStream, Drawable>
    {
        private final AnimatedWebpDecoder delegate;
        
        StreamAnimatedWebpDecoder(final AnimatedWebpDecoder delegate) {
            this.delegate = delegate;
        }
        
        @Override
        public Resource<Drawable> decode(final InputStream inputStream, final int n, final int n2, final Options options) throws IOException {
            return this.delegate.decode(ImageDecoder.createSource(ByteBufferUtil.fromStream(inputStream)), n, n2, options);
        }
        
        @Override
        public boolean handles(final InputStream inputStream, final Options options) throws IOException {
            return this.delegate.handles(inputStream);
        }
    }
}

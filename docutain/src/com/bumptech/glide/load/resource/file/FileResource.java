// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide.load.resource.file;

import java.io.File;
import com.bumptech.glide.load.resource.SimpleResource;

public class FileResource extends SimpleResource<File>
{
    public FileResource(final File file) {
        super(file);
    }
}

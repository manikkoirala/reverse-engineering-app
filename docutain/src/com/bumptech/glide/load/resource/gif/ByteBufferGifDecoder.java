// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide.load.resource.gif;

import com.bumptech.glide.util.Util;
import java.util.Queue;
import com.bumptech.glide.gifdecoder.StandardGifDecoder;
import com.bumptech.glide.load.ImageHeaderParserUtils;
import java.io.IOException;
import com.bumptech.glide.load.engine.Resource;
import com.bumptech.glide.gifdecoder.GifHeader;
import android.util.Log;
import android.graphics.Bitmap;
import com.bumptech.glide.load.Transformation;
import com.bumptech.glide.load.resource.UnitTransformation;
import com.bumptech.glide.gifdecoder.GifDecoder;
import android.graphics.Bitmap$Config;
import com.bumptech.glide.load.DecodeFormat;
import com.bumptech.glide.util.LogTime;
import com.bumptech.glide.load.Options;
import com.bumptech.glide.gifdecoder.GifHeaderParser;
import com.bumptech.glide.load.engine.bitmap_recycle.ArrayPool;
import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPool;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.ImageHeaderParser;
import java.util.List;
import android.content.Context;
import java.nio.ByteBuffer;
import com.bumptech.glide.load.ResourceDecoder;

public class ByteBufferGifDecoder implements ResourceDecoder<ByteBuffer, GifDrawable>
{
    private static final GifDecoderFactory GIF_DECODER_FACTORY;
    private static final GifHeaderParserPool PARSER_POOL;
    private static final String TAG = "BufferGifDecoder";
    private final Context context;
    private final GifDecoderFactory gifDecoderFactory;
    private final GifHeaderParserPool parserPool;
    private final List<ImageHeaderParser> parsers;
    private final GifBitmapProvider provider;
    
    static {
        GIF_DECODER_FACTORY = new GifDecoderFactory();
        PARSER_POOL = new GifHeaderParserPool();
    }
    
    public ByteBufferGifDecoder(final Context context) {
        this(context, Glide.get(context).getRegistry().getImageHeaderParsers(), Glide.get(context).getBitmapPool(), Glide.get(context).getArrayPool());
    }
    
    public ByteBufferGifDecoder(final Context context, final List<ImageHeaderParser> list, final BitmapPool bitmapPool, final ArrayPool arrayPool) {
        this(context, list, bitmapPool, arrayPool, ByteBufferGifDecoder.PARSER_POOL, ByteBufferGifDecoder.GIF_DECODER_FACTORY);
    }
    
    ByteBufferGifDecoder(final Context context, final List<ImageHeaderParser> parsers, final BitmapPool bitmapPool, final ArrayPool arrayPool, final GifHeaderParserPool parserPool, final GifDecoderFactory gifDecoderFactory) {
        this.context = context.getApplicationContext();
        this.parsers = parsers;
        this.gifDecoderFactory = gifDecoderFactory;
        this.provider = new GifBitmapProvider(bitmapPool, arrayPool);
        this.parserPool = parserPool;
    }
    
    private GifDrawableResource decode(final ByteBuffer byteBuffer, final int n, final int n2, final GifHeaderParser gifHeaderParser, final Options options) {
        final long logTime = LogTime.getLogTime();
        try {
            final GifHeader header = gifHeaderParser.parseHeader();
            if (header.getNumFrames() <= 0 || header.getStatus() != 0) {
                return null;
            }
            Bitmap$Config defaultBitmapConfig;
            if (options.get(GifOptions.DECODE_FORMAT) == DecodeFormat.PREFER_RGB_565) {
                defaultBitmapConfig = Bitmap$Config.RGB_565;
            }
            else {
                defaultBitmapConfig = Bitmap$Config.ARGB_8888;
            }
            final GifDecoder build = this.gifDecoderFactory.build(this.provider, header, byteBuffer, getSampleSize(header, n, n2));
            build.setDefaultBitmapConfig(defaultBitmapConfig);
            build.advance();
            final Bitmap nextFrame = build.getNextFrame();
            if (nextFrame == null) {
                return null;
            }
            return new GifDrawableResource(new GifDrawable(this.context, build, (Transformation<Bitmap>)UnitTransformation.get(), n, n2, nextFrame));
        }
        finally {
            if (Log.isLoggable("BufferGifDecoder", 2)) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Decoded GIF from stream in ");
                sb.append(LogTime.getElapsedMillis(logTime));
                Log.v("BufferGifDecoder", sb.toString());
            }
        }
    }
    
    private static int getSampleSize(final GifHeader gifHeader, final int i, final int j) {
        final int min = Math.min(gifHeader.getHeight() / j, gifHeader.getWidth() / i);
        int highestOneBit;
        if (min == 0) {
            highestOneBit = 0;
        }
        else {
            highestOneBit = Integer.highestOneBit(min);
        }
        final int max = Math.max(1, highestOneBit);
        if (Log.isLoggable("BufferGifDecoder", 2) && max > 1) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Downsampling GIF, sampleSize: ");
            sb.append(max);
            sb.append(", target dimens: [");
            sb.append(i);
            sb.append("x");
            sb.append(j);
            sb.append("], actual dimens: [");
            sb.append(gifHeader.getWidth());
            sb.append("x");
            sb.append(gifHeader.getHeight());
            sb.append("]");
            Log.v("BufferGifDecoder", sb.toString());
        }
        return max;
    }
    
    @Override
    public GifDrawableResource decode(final ByteBuffer byteBuffer, final int n, final int n2, final Options options) {
        final GifHeaderParser obtain = this.parserPool.obtain(byteBuffer);
        try {
            return this.decode(byteBuffer, n, n2, obtain, options);
        }
        finally {
            this.parserPool.release(obtain);
        }
    }
    
    @Override
    public boolean handles(final ByteBuffer byteBuffer, final Options options) throws IOException {
        return !options.get(GifOptions.DISABLE_ANIMATION) && ImageHeaderParserUtils.getType(this.parsers, byteBuffer) == ImageHeaderParser.ImageType.GIF;
    }
    
    static class GifDecoderFactory
    {
        GifDecoder build(final GifDecoder.BitmapProvider bitmapProvider, final GifHeader gifHeader, final ByteBuffer byteBuffer, final int n) {
            return new StandardGifDecoder(bitmapProvider, gifHeader, byteBuffer, n);
        }
    }
    
    static class GifHeaderParserPool
    {
        private final Queue<GifHeaderParser> pool;
        
        GifHeaderParserPool() {
            this.pool = Util.createQueue(0);
        }
        
        GifHeaderParser obtain(final ByteBuffer data) {
            synchronized (this) {
                GifHeaderParser gifHeaderParser;
                if ((gifHeaderParser = this.pool.poll()) == null) {
                    gifHeaderParser = new GifHeaderParser();
                }
                return gifHeaderParser.setData(data);
            }
        }
        
        void release(final GifHeaderParser gifHeaderParser) {
            synchronized (this) {
                gifHeaderParser.clear();
                this.pool.offer(gifHeaderParser);
            }
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide.load.resource.gif;

import java.security.MessageDigest;
import com.bumptech.glide.load.resource.bitmap.BitmapResource;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.Resource;
import android.content.Context;
import com.bumptech.glide.util.Preconditions;
import android.graphics.Bitmap;
import com.bumptech.glide.load.Transformation;

public class GifDrawableTransformation implements Transformation<GifDrawable>
{
    private final Transformation<Bitmap> wrapped;
    
    public GifDrawableTransformation(final Transformation<Bitmap> transformation) {
        this.wrapped = Preconditions.checkNotNull(transformation);
    }
    
    @Override
    public boolean equals(final Object o) {
        return o instanceof GifDrawableTransformation && this.wrapped.equals(((GifDrawableTransformation)o).wrapped);
    }
    
    @Override
    public int hashCode() {
        return this.wrapped.hashCode();
    }
    
    @Override
    public Resource<GifDrawable> transform(final Context context, final Resource<GifDrawable> resource, final int n, final int n2) {
        final GifDrawable gifDrawable = resource.get();
        final BitmapResource bitmapResource = new BitmapResource(gifDrawable.getFirstFrame(), Glide.get(context).getBitmapPool());
        final Resource<Bitmap> transform = this.wrapped.transform(context, bitmapResource, n, n2);
        if (!bitmapResource.equals(transform)) {
            bitmapResource.recycle();
        }
        gifDrawable.setFrameTransformation(this.wrapped, transform.get());
        return resource;
    }
    
    @Override
    public void updateDiskCacheKey(final MessageDigest messageDigest) {
        this.wrapped.updateDiskCacheKey(messageDigest);
    }
}

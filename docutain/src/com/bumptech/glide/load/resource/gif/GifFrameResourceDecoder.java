// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide.load.resource.gif;

import java.io.IOException;
import com.bumptech.glide.load.resource.bitmap.BitmapResource;
import com.bumptech.glide.load.engine.Resource;
import com.bumptech.glide.load.Options;
import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPool;
import android.graphics.Bitmap;
import com.bumptech.glide.gifdecoder.GifDecoder;
import com.bumptech.glide.load.ResourceDecoder;

public final class GifFrameResourceDecoder implements ResourceDecoder<GifDecoder, Bitmap>
{
    private final BitmapPool bitmapPool;
    
    public GifFrameResourceDecoder(final BitmapPool bitmapPool) {
        this.bitmapPool = bitmapPool;
    }
    
    @Override
    public Resource<Bitmap> decode(final GifDecoder gifDecoder, final int n, final int n2, final Options options) {
        return BitmapResource.obtain(gifDecoder.getNextFrame(), this.bitmapPool);
    }
    
    @Override
    public boolean handles(final GifDecoder gifDecoder, final Options options) {
        return true;
    }
}

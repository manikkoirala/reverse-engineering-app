// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide.load.resource.gif;

import com.bumptech.glide.load.EncodeStrategy;
import java.io.IOException;
import android.util.Log;
import com.bumptech.glide.util.ByteBufferUtil;
import com.bumptech.glide.load.Options;
import java.io.File;
import com.bumptech.glide.load.engine.Resource;
import com.bumptech.glide.load.ResourceEncoder;

public class GifDrawableEncoder implements ResourceEncoder<GifDrawable>
{
    private static final String TAG = "GifEncoder";
    
    @Override
    public boolean encode(final Resource<GifDrawable> resource, final File file, final Options options) {
        final GifDrawable gifDrawable = resource.get();
        boolean b;
        try {
            ByteBufferUtil.toFile(gifDrawable.getBuffer(), file);
            b = true;
        }
        catch (final IOException ex) {
            if (Log.isLoggable("GifEncoder", 5)) {
                Log.w("GifEncoder", "Failed to encode GIF drawable data", (Throwable)ex);
            }
            b = false;
        }
        return b;
    }
    
    @Override
    public EncodeStrategy getEncodeStrategy(final Options options) {
        return EncodeStrategy.SOURCE;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide.load.resource.gif;

import com.bumptech.glide.load.ImageHeaderParserUtils;
import com.bumptech.glide.load.engine.Resource;
import com.bumptech.glide.load.Options;
import java.io.IOException;
import android.util.Log;
import java.io.ByteArrayOutputStream;
import com.bumptech.glide.load.ImageHeaderParser;
import java.util.List;
import java.nio.ByteBuffer;
import com.bumptech.glide.load.engine.bitmap_recycle.ArrayPool;
import java.io.InputStream;
import com.bumptech.glide.load.ResourceDecoder;

public class StreamGifDecoder implements ResourceDecoder<InputStream, GifDrawable>
{
    private static final String TAG = "StreamGifDecoder";
    private final ArrayPool byteArrayPool;
    private final ResourceDecoder<ByteBuffer, GifDrawable> byteBufferDecoder;
    private final List<ImageHeaderParser> parsers;
    
    public StreamGifDecoder(final List<ImageHeaderParser> parsers, final ResourceDecoder<ByteBuffer, GifDrawable> byteBufferDecoder, final ArrayPool byteArrayPool) {
        this.parsers = parsers;
        this.byteBufferDecoder = byteBufferDecoder;
        this.byteArrayPool = byteArrayPool;
    }
    
    private static byte[] inputStreamToBytes(final InputStream inputStream) {
        final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(16384);
        try {
            final byte[] array = new byte[16384];
            while (true) {
                final int read = inputStream.read(array);
                if (read == -1) {
                    break;
                }
                byteArrayOutputStream.write(array, 0, read);
            }
            byteArrayOutputStream.flush();
            return byteArrayOutputStream.toByteArray();
        }
        catch (final IOException ex) {
            if (Log.isLoggable("StreamGifDecoder", 5)) {
                Log.w("StreamGifDecoder", "Error reading data from stream", (Throwable)ex);
            }
            return null;
        }
    }
    
    @Override
    public Resource<GifDrawable> decode(final InputStream inputStream, final int n, final int n2, final Options options) throws IOException {
        final byte[] inputStreamToBytes = inputStreamToBytes(inputStream);
        if (inputStreamToBytes == null) {
            return null;
        }
        return this.byteBufferDecoder.decode(ByteBuffer.wrap(inputStreamToBytes), n, n2, options);
    }
    
    @Override
    public boolean handles(final InputStream inputStream, final Options options) throws IOException {
        return !options.get(GifOptions.DISABLE_ANIMATION) && ImageHeaderParserUtils.getType(this.parsers, inputStream, this.byteArrayPool) == ImageHeaderParser.ImageType.GIF;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide.load.resource.gif;

import android.graphics.Bitmap;
import android.graphics.Bitmap$Config;
import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPool;
import com.bumptech.glide.load.engine.bitmap_recycle.ArrayPool;
import com.bumptech.glide.gifdecoder.GifDecoder;

public final class GifBitmapProvider implements BitmapProvider
{
    private final ArrayPool arrayPool;
    private final BitmapPool bitmapPool;
    
    public GifBitmapProvider(final BitmapPool bitmapPool) {
        this(bitmapPool, null);
    }
    
    public GifBitmapProvider(final BitmapPool bitmapPool, final ArrayPool arrayPool) {
        this.bitmapPool = bitmapPool;
        this.arrayPool = arrayPool;
    }
    
    @Override
    public Bitmap obtain(final int n, final int n2, final Bitmap$Config bitmap$Config) {
        return this.bitmapPool.getDirty(n, n2, bitmap$Config);
    }
    
    @Override
    public byte[] obtainByteArray(final int n) {
        final ArrayPool arrayPool = this.arrayPool;
        if (arrayPool == null) {
            return new byte[n];
        }
        return arrayPool.get(n, byte[].class);
    }
    
    @Override
    public int[] obtainIntArray(final int n) {
        final ArrayPool arrayPool = this.arrayPool;
        if (arrayPool == null) {
            return new int[n];
        }
        return arrayPool.get(n, int[].class);
    }
    
    @Override
    public void release(final Bitmap bitmap) {
        this.bitmapPool.put(bitmap);
    }
    
    @Override
    public void release(final byte[] array) {
        final ArrayPool arrayPool = this.arrayPool;
        if (arrayPool == null) {
            return;
        }
        arrayPool.put(array);
    }
    
    @Override
    public void release(final int[] array) {
        final ArrayPool arrayPool = this.arrayPool;
        if (arrayPool == null) {
            return;
        }
        arrayPool.put(array);
    }
}

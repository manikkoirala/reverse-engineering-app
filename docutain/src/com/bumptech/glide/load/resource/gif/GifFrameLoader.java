// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide.load.resource.gif;

import android.os.Message;
import com.bumptech.glide.request.transition.Transition;
import android.graphics.drawable.Drawable;
import com.bumptech.glide.request.target.CustomTarget;
import com.bumptech.glide.util.Util;
import java.nio.ByteBuffer;
import com.bumptech.glide.request.target.Target;
import com.bumptech.glide.request.BaseRequestOptions;
import android.os.SystemClock;
import com.bumptech.glide.util.Preconditions;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.signature.ObjectKey;
import com.bumptech.glide.load.Key;
import android.os.Handler$Callback;
import android.os.Looper;
import java.util.ArrayList;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.Transformation;
import com.bumptech.glide.RequestManager;
import com.bumptech.glide.RequestBuilder;
import android.os.Handler;
import com.bumptech.glide.gifdecoder.GifDecoder;
import android.graphics.Bitmap;
import java.util.List;
import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPool;

class GifFrameLoader
{
    private final BitmapPool bitmapPool;
    private final List<FrameCallback> callbacks;
    private DelayTarget current;
    private Bitmap firstFrame;
    private int firstFrameSize;
    private final GifDecoder gifDecoder;
    private final Handler handler;
    private int height;
    private boolean isCleared;
    private boolean isLoadPending;
    private boolean isRunning;
    private DelayTarget next;
    private OnEveryFrameListener onEveryFrameListener;
    private DelayTarget pendingTarget;
    private RequestBuilder<Bitmap> requestBuilder;
    final RequestManager requestManager;
    private boolean startFromFirstFrame;
    private Transformation<Bitmap> transformation;
    private int width;
    
    GifFrameLoader(final Glide glide, final GifDecoder gifDecoder, final int n, final int n2, final Transformation<Bitmap> transformation, final Bitmap bitmap) {
        this(glide.getBitmapPool(), Glide.with(glide.getContext()), gifDecoder, null, getRequestBuilder(Glide.with(glide.getContext()), n, n2), transformation, bitmap);
    }
    
    GifFrameLoader(final BitmapPool bitmapPool, final RequestManager requestManager, final GifDecoder gifDecoder, final Handler handler, final RequestBuilder<Bitmap> requestBuilder, final Transformation<Bitmap> transformation, final Bitmap bitmap) {
        this.callbacks = new ArrayList<FrameCallback>();
        this.requestManager = requestManager;
        Handler handler2 = handler;
        if (handler == null) {
            handler2 = new Handler(Looper.getMainLooper(), (Handler$Callback)new FrameLoaderCallback());
        }
        this.bitmapPool = bitmapPool;
        this.handler = handler2;
        this.requestBuilder = requestBuilder;
        this.gifDecoder = gifDecoder;
        this.setFrameTransformation(transformation, bitmap);
    }
    
    private static Key getFrameSignature() {
        return new ObjectKey(Math.random());
    }
    
    private static RequestBuilder<Bitmap> getRequestBuilder(final RequestManager requestManager, final int n, final int n2) {
        return requestManager.asBitmap().apply(((BaseRequestOptions<BaseRequestOptions<?>>)RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.NONE).useAnimationPool(true).skipMemoryCache(true)).override(n, n2));
    }
    
    private void loadNextFrame() {
        if (this.isRunning) {
            if (!this.isLoadPending) {
                if (this.startFromFirstFrame) {
                    Preconditions.checkArgument(this.pendingTarget == null, "Pending target must be null when starting from the first frame");
                    this.gifDecoder.resetFrameIndex();
                    this.startFromFirstFrame = false;
                }
                final DelayTarget pendingTarget = this.pendingTarget;
                if (pendingTarget != null) {
                    this.pendingTarget = null;
                    this.onFrameReady(pendingTarget);
                    return;
                }
                this.isLoadPending = true;
                final int nextDelay = this.gifDecoder.getNextDelay();
                final long uptimeMillis = SystemClock.uptimeMillis();
                final long n = nextDelay;
                this.gifDecoder.advance();
                this.next = new DelayTarget(this.handler, this.gifDecoder.getCurrentFrameIndex(), uptimeMillis + n);
                this.requestBuilder.apply(RequestOptions.signatureOf(getFrameSignature())).load(this.gifDecoder).into(this.next);
            }
        }
    }
    
    private void recycleFirstFrame() {
        final Bitmap firstFrame = this.firstFrame;
        if (firstFrame != null) {
            this.bitmapPool.put(firstFrame);
            this.firstFrame = null;
        }
    }
    
    private void start() {
        if (this.isRunning) {
            return;
        }
        this.isRunning = true;
        this.isCleared = false;
        this.loadNextFrame();
    }
    
    private void stop() {
        this.isRunning = false;
    }
    
    void clear() {
        this.callbacks.clear();
        this.recycleFirstFrame();
        this.stop();
        final DelayTarget current = this.current;
        if (current != null) {
            this.requestManager.clear(current);
            this.current = null;
        }
        final DelayTarget next = this.next;
        if (next != null) {
            this.requestManager.clear(next);
            this.next = null;
        }
        final DelayTarget pendingTarget = this.pendingTarget;
        if (pendingTarget != null) {
            this.requestManager.clear(pendingTarget);
            this.pendingTarget = null;
        }
        this.gifDecoder.clear();
        this.isCleared = true;
    }
    
    ByteBuffer getBuffer() {
        return this.gifDecoder.getData().asReadOnlyBuffer();
    }
    
    Bitmap getCurrentFrame() {
        final DelayTarget current = this.current;
        Bitmap bitmap;
        if (current != null) {
            bitmap = current.getResource();
        }
        else {
            bitmap = this.firstFrame;
        }
        return bitmap;
    }
    
    int getCurrentIndex() {
        final DelayTarget current = this.current;
        int index;
        if (current != null) {
            index = current.index;
        }
        else {
            index = -1;
        }
        return index;
    }
    
    Bitmap getFirstFrame() {
        return this.firstFrame;
    }
    
    int getFrameCount() {
        return this.gifDecoder.getFrameCount();
    }
    
    Transformation<Bitmap> getFrameTransformation() {
        return this.transformation;
    }
    
    int getHeight() {
        return this.height;
    }
    
    int getLoopCount() {
        return this.gifDecoder.getTotalIterationCount();
    }
    
    int getSize() {
        return this.gifDecoder.getByteSize() + this.firstFrameSize;
    }
    
    int getWidth() {
        return this.width;
    }
    
    void onFrameReady(final DelayTarget delayTarget) {
        final OnEveryFrameListener onEveryFrameListener = this.onEveryFrameListener;
        if (onEveryFrameListener != null) {
            onEveryFrameListener.onFrameReady();
        }
        this.isLoadPending = false;
        if (this.isCleared) {
            this.handler.obtainMessage(2, (Object)delayTarget).sendToTarget();
            return;
        }
        if (!this.isRunning) {
            if (this.startFromFirstFrame) {
                this.handler.obtainMessage(2, (Object)delayTarget).sendToTarget();
            }
            else {
                this.pendingTarget = delayTarget;
            }
            return;
        }
        if (delayTarget.getResource() != null) {
            this.recycleFirstFrame();
            final DelayTarget current = this.current;
            this.current = delayTarget;
            for (int i = this.callbacks.size() - 1; i >= 0; --i) {
                this.callbacks.get(i).onFrameReady();
            }
            if (current != null) {
                this.handler.obtainMessage(2, (Object)current).sendToTarget();
            }
        }
        this.loadNextFrame();
    }
    
    void setFrameTransformation(final Transformation<Bitmap> transformation, final Bitmap bitmap) {
        this.transformation = Preconditions.checkNotNull(transformation);
        this.firstFrame = Preconditions.checkNotNull(bitmap);
        this.requestBuilder = this.requestBuilder.apply(((BaseRequestOptions<BaseRequestOptions<?>>)new RequestOptions()).transform(transformation));
        this.firstFrameSize = Util.getBitmapByteSize(bitmap);
        this.width = bitmap.getWidth();
        this.height = bitmap.getHeight();
    }
    
    void setNextStartFromFirstFrame() {
        Preconditions.checkArgument(this.isRunning ^ true, "Can't restart a running animation");
        this.startFromFirstFrame = true;
        final DelayTarget pendingTarget = this.pendingTarget;
        if (pendingTarget != null) {
            this.requestManager.clear(pendingTarget);
            this.pendingTarget = null;
        }
    }
    
    void setOnEveryFrameReadyListener(final OnEveryFrameListener onEveryFrameListener) {
        this.onEveryFrameListener = onEveryFrameListener;
    }
    
    void subscribe(final FrameCallback frameCallback) {
        if (this.isCleared) {
            throw new IllegalStateException("Cannot subscribe to a cleared frame loader");
        }
        if (!this.callbacks.contains(frameCallback)) {
            final boolean empty = this.callbacks.isEmpty();
            this.callbacks.add(frameCallback);
            if (empty) {
                this.start();
            }
            return;
        }
        throw new IllegalStateException("Cannot subscribe twice in a row");
    }
    
    void unsubscribe(final FrameCallback frameCallback) {
        this.callbacks.remove(frameCallback);
        if (this.callbacks.isEmpty()) {
            this.stop();
        }
    }
    
    static class DelayTarget extends CustomTarget<Bitmap>
    {
        private final Handler handler;
        final int index;
        private Bitmap resource;
        private final long targetTime;
        
        DelayTarget(final Handler handler, final int index, final long targetTime) {
            this.handler = handler;
            this.index = index;
            this.targetTime = targetTime;
        }
        
        Bitmap getResource() {
            return this.resource;
        }
        
        @Override
        public void onLoadCleared(final Drawable drawable) {
            this.resource = null;
        }
        
        @Override
        public void onResourceReady(final Bitmap resource, final Transition<? super Bitmap> transition) {
            this.resource = resource;
            this.handler.sendMessageAtTime(this.handler.obtainMessage(1, (Object)this), this.targetTime);
        }
    }
    
    public interface FrameCallback
    {
        void onFrameReady();
    }
    
    private class FrameLoaderCallback implements Handler$Callback
    {
        static final int MSG_CLEAR = 2;
        static final int MSG_DELAY = 1;
        final GifFrameLoader this$0;
        
        FrameLoaderCallback(final GifFrameLoader this$0) {
            this.this$0 = this$0;
        }
        
        public boolean handleMessage(final Message message) {
            if (message.what == 1) {
                this.this$0.onFrameReady((DelayTarget)message.obj);
                return true;
            }
            if (message.what == 2) {
                this.this$0.requestManager.clear((Target<?>)message.obj);
            }
            return false;
        }
    }
    
    interface OnEveryFrameListener
    {
        void onFrameReady();
    }
}

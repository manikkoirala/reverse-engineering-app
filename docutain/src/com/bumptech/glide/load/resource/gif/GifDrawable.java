// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide.load.resource.gif;

import android.content.res.Resources;
import android.graphics.ColorFilter;
import java.util.ArrayList;
import android.graphics.drawable.Drawable$ConstantState;
import java.nio.ByteBuffer;
import android.view.Gravity;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable$Callback;
import com.bumptech.glide.util.Preconditions;
import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPool;
import com.bumptech.glide.Glide;
import android.graphics.Bitmap;
import com.bumptech.glide.load.Transformation;
import com.bumptech.glide.gifdecoder.GifDecoder;
import android.content.Context;
import android.graphics.Paint;
import android.graphics.Rect;
import java.util.List;
import androidx.vectordrawable.graphics.drawable.Animatable2Compat;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.Drawable;

public class GifDrawable extends Drawable implements FrameCallback, Animatable, Animatable2Compat
{
    private static final int GRAVITY = 119;
    public static final int LOOP_FOREVER = -1;
    public static final int LOOP_INTRINSIC = 0;
    private List<AnimationCallback> animationCallbacks;
    private boolean applyGravity;
    private Rect destRect;
    private boolean isRecycled;
    private boolean isRunning;
    private boolean isStarted;
    private boolean isVisible;
    private int loopCount;
    private int maxLoopCount;
    private Paint paint;
    private final GifState state;
    
    public GifDrawable(final Context context, final GifDecoder gifDecoder, final Transformation<Bitmap> transformation, final int n, final int n2, final Bitmap bitmap) {
        this(new GifState(new GifFrameLoader(Glide.get(context), gifDecoder, n, n2, transformation, bitmap)));
    }
    
    @Deprecated
    public GifDrawable(final Context context, final GifDecoder gifDecoder, final BitmapPool bitmapPool, final Transformation<Bitmap> transformation, final int n, final int n2, final Bitmap bitmap) {
        this(context, gifDecoder, transformation, n, n2, bitmap);
    }
    
    GifDrawable(final GifState gifState) {
        this.isVisible = true;
        this.maxLoopCount = -1;
        this.state = Preconditions.checkNotNull(gifState);
    }
    
    GifDrawable(final GifFrameLoader gifFrameLoader, final Paint paint) {
        this(new GifState(gifFrameLoader));
        this.paint = paint;
    }
    
    private Drawable$Callback findCallback() {
        Drawable$Callback drawable$Callback;
        for (drawable$Callback = this.getCallback(); drawable$Callback instanceof Drawable; drawable$Callback = ((Drawable)drawable$Callback).getCallback()) {}
        return drawable$Callback;
    }
    
    private Rect getDestRect() {
        if (this.destRect == null) {
            this.destRect = new Rect();
        }
        return this.destRect;
    }
    
    private Paint getPaint() {
        if (this.paint == null) {
            this.paint = new Paint(2);
        }
        return this.paint;
    }
    
    private void notifyAnimationEndToListeners() {
        final List<AnimationCallback> animationCallbacks = this.animationCallbacks;
        if (animationCallbacks != null) {
            for (int i = 0; i < animationCallbacks.size(); ++i) {
                this.animationCallbacks.get(i).onAnimationEnd(this);
            }
        }
    }
    
    private void resetLoopCount() {
        this.loopCount = 0;
    }
    
    private void startRunning() {
        Preconditions.checkArgument(this.isRecycled ^ true, "You cannot start a recycled Drawable. Ensure thatyou clear any references to the Drawable when clearing the corresponding request.");
        if (this.state.frameLoader.getFrameCount() == 1) {
            this.invalidateSelf();
        }
        else if (!this.isRunning) {
            this.isRunning = true;
            this.state.frameLoader.subscribe((GifFrameLoader.FrameCallback)this);
            this.invalidateSelf();
        }
    }
    
    private void stopRunning() {
        this.isRunning = false;
        this.state.frameLoader.unsubscribe((GifFrameLoader.FrameCallback)this);
    }
    
    public void clearAnimationCallbacks() {
        final List<AnimationCallback> animationCallbacks = this.animationCallbacks;
        if (animationCallbacks != null) {
            animationCallbacks.clear();
        }
    }
    
    public void draw(final Canvas canvas) {
        if (this.isRecycled) {
            return;
        }
        if (this.applyGravity) {
            Gravity.apply(119, this.getIntrinsicWidth(), this.getIntrinsicHeight(), this.getBounds(), this.getDestRect());
            this.applyGravity = false;
        }
        canvas.drawBitmap(this.state.frameLoader.getCurrentFrame(), (Rect)null, this.getDestRect(), this.getPaint());
    }
    
    public ByteBuffer getBuffer() {
        return this.state.frameLoader.getBuffer();
    }
    
    public Drawable$ConstantState getConstantState() {
        return this.state;
    }
    
    public Bitmap getFirstFrame() {
        return this.state.frameLoader.getFirstFrame();
    }
    
    public int getFrameCount() {
        return this.state.frameLoader.getFrameCount();
    }
    
    public int getFrameIndex() {
        return this.state.frameLoader.getCurrentIndex();
    }
    
    public Transformation<Bitmap> getFrameTransformation() {
        return this.state.frameLoader.getFrameTransformation();
    }
    
    public int getIntrinsicHeight() {
        return this.state.frameLoader.getHeight();
    }
    
    public int getIntrinsicWidth() {
        return this.state.frameLoader.getWidth();
    }
    
    public int getOpacity() {
        return -2;
    }
    
    public int getSize() {
        return this.state.frameLoader.getSize();
    }
    
    boolean isRecycled() {
        return this.isRecycled;
    }
    
    public boolean isRunning() {
        return this.isRunning;
    }
    
    protected void onBoundsChange(final Rect rect) {
        super.onBoundsChange(rect);
        this.applyGravity = true;
    }
    
    public void onFrameReady() {
        if (this.findCallback() == null) {
            this.stop();
            this.invalidateSelf();
            return;
        }
        this.invalidateSelf();
        if (this.getFrameIndex() == this.getFrameCount() - 1) {
            ++this.loopCount;
        }
        final int maxLoopCount = this.maxLoopCount;
        if (maxLoopCount != -1 && this.loopCount >= maxLoopCount) {
            this.notifyAnimationEndToListeners();
            this.stop();
        }
    }
    
    public void recycle() {
        this.isRecycled = true;
        this.state.frameLoader.clear();
    }
    
    public void registerAnimationCallback(final AnimationCallback animationCallback) {
        if (animationCallback == null) {
            return;
        }
        if (this.animationCallbacks == null) {
            this.animationCallbacks = new ArrayList<AnimationCallback>();
        }
        this.animationCallbacks.add(animationCallback);
    }
    
    public void setAlpha(final int alpha) {
        this.getPaint().setAlpha(alpha);
    }
    
    public void setColorFilter(final ColorFilter colorFilter) {
        this.getPaint().setColorFilter(colorFilter);
    }
    
    public void setFrameTransformation(final Transformation<Bitmap> transformation, final Bitmap bitmap) {
        this.state.frameLoader.setFrameTransformation(transformation, bitmap);
    }
    
    void setIsRunning(final boolean isRunning) {
        this.isRunning = isRunning;
    }
    
    public void setLoopCount(int loopCount) {
        final int n = -1;
        if (loopCount <= 0 && loopCount != -1 && loopCount != 0) {
            throw new IllegalArgumentException("Loop count must be greater than 0, or equal to GlideDrawable.LOOP_FOREVER, or equal to GlideDrawable.LOOP_INTRINSIC");
        }
        if (loopCount == 0) {
            loopCount = this.state.frameLoader.getLoopCount();
            if (loopCount == 0) {
                loopCount = n;
            }
            this.maxLoopCount = loopCount;
        }
        else {
            this.maxLoopCount = loopCount;
        }
    }
    
    public boolean setVisible(final boolean isVisible, final boolean b) {
        Preconditions.checkArgument(this.isRecycled ^ true, "Cannot change the visibility of a recycled resource. Ensure that you unset the Drawable from your View before changing the View's visibility.");
        if (!(this.isVisible = isVisible)) {
            this.stopRunning();
        }
        else if (this.isStarted) {
            this.startRunning();
        }
        return super.setVisible(isVisible, b);
    }
    
    public void start() {
        this.isStarted = true;
        this.resetLoopCount();
        if (this.isVisible) {
            this.startRunning();
        }
    }
    
    public void startFromFirstFrame() {
        Preconditions.checkArgument(this.isRunning ^ true, "You cannot restart a currently running animation.");
        this.state.frameLoader.setNextStartFromFirstFrame();
        this.start();
    }
    
    public void stop() {
        this.isStarted = false;
        this.stopRunning();
    }
    
    public boolean unregisterAnimationCallback(final AnimationCallback animationCallback) {
        final List<AnimationCallback> animationCallbacks = this.animationCallbacks;
        return animationCallbacks != null && animationCallback != null && animationCallbacks.remove(animationCallback);
    }
    
    static final class GifState extends Drawable$ConstantState
    {
        final GifFrameLoader frameLoader;
        
        GifState(final GifFrameLoader frameLoader) {
            this.frameLoader = frameLoader;
        }
        
        public int getChangingConfigurations() {
            return 0;
        }
        
        public Drawable newDrawable() {
            return new GifDrawable(this);
        }
        
        public Drawable newDrawable(final Resources resources) {
            return this.newDrawable();
        }
    }
}

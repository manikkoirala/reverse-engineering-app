// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide.load.resource.bitmap;

import android.graphics.ImageDecoder;
import java.io.IOException;
import com.bumptech.glide.load.engine.Resource;
import com.bumptech.glide.load.Options;
import android.graphics.Bitmap;
import java.nio.ByteBuffer;
import com.bumptech.glide.load.ResourceDecoder;

public final class ByteBufferBitmapImageDecoderResourceDecoder implements ResourceDecoder<ByteBuffer, Bitmap>
{
    private final BitmapImageDecoderResourceDecoder wrapped;
    
    public ByteBufferBitmapImageDecoderResourceDecoder() {
        this.wrapped = new BitmapImageDecoderResourceDecoder();
    }
    
    @Override
    public Resource<Bitmap> decode(final ByteBuffer byteBuffer, final int n, final int n2, final Options options) throws IOException {
        return this.wrapped.decode(ImageDecoder.createSource(byteBuffer), n, n2, options);
    }
    
    @Override
    public boolean handles(final ByteBuffer byteBuffer, final Options options) throws IOException {
        return true;
    }
}

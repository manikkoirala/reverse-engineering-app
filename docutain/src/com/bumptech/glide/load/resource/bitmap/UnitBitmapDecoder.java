// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide.load.resource.bitmap;

import com.bumptech.glide.util.Util;
import java.io.IOException;
import com.bumptech.glide.load.engine.Resource;
import com.bumptech.glide.load.Options;
import android.graphics.Bitmap;
import com.bumptech.glide.load.ResourceDecoder;

public final class UnitBitmapDecoder implements ResourceDecoder<Bitmap, Bitmap>
{
    @Override
    public Resource<Bitmap> decode(final Bitmap bitmap, final int n, final int n2, final Options options) {
        return new NonOwnedBitmapResource(bitmap);
    }
    
    @Override
    public boolean handles(final Bitmap bitmap, final Options options) {
        return true;
    }
    
    private static final class NonOwnedBitmapResource implements Resource<Bitmap>
    {
        private final Bitmap bitmap;
        
        NonOwnedBitmapResource(final Bitmap bitmap) {
            this.bitmap = bitmap;
        }
        
        @Override
        public Bitmap get() {
            return this.bitmap;
        }
        
        @Override
        public Class<Bitmap> getResourceClass() {
            return Bitmap.class;
        }
        
        @Override
        public int getSize() {
            return Util.getBitmapByteSize(this.bitmap);
        }
        
        @Override
        public void recycle() {
        }
    }
}

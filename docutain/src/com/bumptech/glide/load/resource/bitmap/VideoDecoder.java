// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide.load.resource.bitmap;

import android.media.MediaDataSource;
import java.io.IOException;
import com.bumptech.glide.load.engine.Resource;
import com.bumptech.glide.load.Options;
import android.os.ParcelFileDescriptor;
import android.media.MediaExtractor;
import java.util.Iterator;
import android.os.Build;
import android.os.Build$VERSION;
import android.graphics.Matrix;
import android.util.Log;
import android.media.MediaMetadataRetriever;
import android.content.res.AssetFileDescriptor;
import java.util.Collections;
import java.util.Arrays;
import java.security.MessageDigest;
import java.nio.ByteBuffer;
import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPool;
import java.util.List;
import com.bumptech.glide.load.Option;
import android.graphics.Bitmap;
import com.bumptech.glide.load.ResourceDecoder;

public class VideoDecoder<T> implements ResourceDecoder<T, Bitmap>
{
    private static final MediaMetadataRetrieverFactory DEFAULT_FACTORY;
    public static final long DEFAULT_FRAME = -1L;
    static final int DEFAULT_FRAME_OPTION = 2;
    public static final Option<Integer> FRAME_OPTION;
    private static final List<String> PIXEL_T_BUILD_ID_PREFIXES_REQUIRING_HDR_180_ROTATION_FIX;
    private static final String TAG = "VideoDecoder";
    public static final Option<Long> TARGET_FRAME;
    private static final String WEBM_MIME_TYPE = "video/webm";
    private final BitmapPool bitmapPool;
    private final MediaMetadataRetrieverFactory factory;
    private final MediaInitializer<T> initializer;
    
    static {
        TARGET_FRAME = Option.disk("com.bumptech.glide.load.resource.bitmap.VideoBitmapDecode.TargetFrame", -1L, (Option.CacheKeyUpdater<Long>)new Option.CacheKeyUpdater<Long>() {
            private final ByteBuffer buffer = ByteBuffer.allocate(8);
            
            public void update(final byte[] input, final Long n, final MessageDigest messageDigest) {
                messageDigest.update(input);
                synchronized (this.buffer) {
                    this.buffer.position(0);
                    messageDigest.update(this.buffer.putLong(n).array());
                }
            }
        });
        FRAME_OPTION = Option.disk("com.bumptech.glide.load.resource.bitmap.VideoBitmapDecode.FrameOption", 2, (Option.CacheKeyUpdater<Integer>)new Option.CacheKeyUpdater<Integer>() {
            private final ByteBuffer buffer = ByteBuffer.allocate(4);
            
            public void update(final byte[] input, final Integer n, final MessageDigest messageDigest) {
                if (n == null) {
                    return;
                }
                messageDigest.update(input);
                synchronized (this.buffer) {
                    this.buffer.position(0);
                    messageDigest.update(this.buffer.putInt(n).array());
                }
            }
        });
        DEFAULT_FACTORY = new MediaMetadataRetrieverFactory();
        PIXEL_T_BUILD_ID_PREFIXES_REQUIRING_HDR_180_ROTATION_FIX = Collections.unmodifiableList((List<? extends String>)Arrays.asList("TP1A", "TD1A.220804.031"));
    }
    
    VideoDecoder(final BitmapPool bitmapPool, final MediaInitializer<T> mediaInitializer) {
        this(bitmapPool, mediaInitializer, VideoDecoder.DEFAULT_FACTORY);
    }
    
    VideoDecoder(final BitmapPool bitmapPool, final MediaInitializer<T> initializer, final MediaMetadataRetrieverFactory factory) {
        this.bitmapPool = bitmapPool;
        this.initializer = initializer;
        this.factory = factory;
    }
    
    public static ResourceDecoder<AssetFileDescriptor, Bitmap> asset(final BitmapPool bitmapPool) {
        return new VideoDecoder<AssetFileDescriptor>(bitmapPool, (MediaInitializer<AssetFileDescriptor>)new AssetFileDescriptorInitializer());
    }
    
    public static ResourceDecoder<ByteBuffer, Bitmap> byteBuffer(final BitmapPool bitmapPool) {
        return new VideoDecoder<ByteBuffer>(bitmapPool, (MediaInitializer<ByteBuffer>)new ByteBufferInitializer());
    }
    
    private static Bitmap correctHdr180DegVideoFrameOrientation(final MediaMetadataRetriever mediaMetadataRetriever, final Bitmap bitmap) {
        if (!isHdr180RotationFixRequired()) {
            return bitmap;
        }
        int n2;
        final int n = n2 = 0;
        try {
            if (isHDR(mediaMetadataRetriever)) {
                final int abs = Math.abs(Integer.parseInt(mediaMetadataRetriever.extractMetadata(24)));
                n2 = n;
                if (abs == 180) {
                    n2 = 1;
                }
            }
        }
        catch (final NumberFormatException ex) {
            n2 = n;
            if (Log.isLoggable("VideoDecoder", 3)) {
                Log.d("VideoDecoder", "Exception trying to extract HDR transfer function or rotation");
                n2 = n;
            }
        }
        if (n2 == 0) {
            return bitmap;
        }
        if (Log.isLoggable("VideoDecoder", 3)) {
            Log.d("VideoDecoder", "Applying HDR 180 deg thumbnail correction");
        }
        final Matrix matrix = new Matrix();
        matrix.postRotate(180.0f, bitmap.getWidth() / 2.0f, bitmap.getHeight() / 2.0f);
        return Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
    }
    
    private Bitmap decodeFrame(final T t, final MediaMetadataRetriever mediaMetadataRetriever, final long n, final int n2, final int n3, final int n4, final DownsampleStrategy downsampleStrategy) {
        if (this.isUnsupportedFormat(t, mediaMetadataRetriever)) {
            throw new IllegalStateException("Cannot decode VP8 video on CrOS.");
        }
        Bitmap decodeScaledFrame;
        final Bitmap bitmap = decodeScaledFrame = null;
        if (Build$VERSION.SDK_INT >= 27) {
            decodeScaledFrame = bitmap;
            if (n3 != Integer.MIN_VALUE) {
                decodeScaledFrame = bitmap;
                if (n4 != Integer.MIN_VALUE) {
                    decodeScaledFrame = bitmap;
                    if (downsampleStrategy != DownsampleStrategy.NONE) {
                        decodeScaledFrame = decodeScaledFrame(mediaMetadataRetriever, n, n2, n3, n4, downsampleStrategy);
                    }
                }
            }
        }
        Bitmap decodeOriginalFrame;
        if ((decodeOriginalFrame = decodeScaledFrame) == null) {
            decodeOriginalFrame = decodeOriginalFrame(mediaMetadataRetriever, n, n2);
        }
        final Bitmap correctHdr180DegVideoFrameOrientation = correctHdr180DegVideoFrameOrientation(mediaMetadataRetriever, decodeOriginalFrame);
        if (correctHdr180DegVideoFrameOrientation != null) {
            return correctHdr180DegVideoFrameOrientation;
        }
        throw new VideoDecoderException();
    }
    
    private static Bitmap decodeOriginalFrame(final MediaMetadataRetriever mediaMetadataRetriever, final long n, final int n2) {
        return mediaMetadataRetriever.getFrameAtTime(n, n2);
    }
    
    private static Bitmap decodeScaledFrame(final MediaMetadataRetriever mediaMetadataRetriever, final long n, final int n2, final int n3, final int n4, final DownsampleStrategy downsampleStrategy) {
        try {
            final int int1 = Integer.parseInt(mediaMetadataRetriever.extractMetadata(18));
            final int int2 = Integer.parseInt(mediaMetadataRetriever.extractMetadata(19));
            final int int3 = Integer.parseInt(mediaMetadataRetriever.extractMetadata(24));
            int n5 = 0;
            int n6 = 0;
            Label_0064: {
                if (int3 != 90) {
                    n5 = int1;
                    n6 = int2;
                    if (int3 != 270) {
                        break Label_0064;
                    }
                }
                n6 = int1;
                n5 = int2;
            }
            final float scaleFactor = downsampleStrategy.getScaleFactor(n5, n6, n3, n4);
            return mediaMetadataRetriever.getScaledFrameAtTime(n, n2, Math.round(n5 * scaleFactor), Math.round(scaleFactor * n6));
        }
        finally {
            if (Log.isLoggable("VideoDecoder", 3)) {
                final Throwable t;
                Log.d("VideoDecoder", "Exception trying to decode a scaled frame on oreo+, falling back to a fullsize frame", t);
            }
            return null;
        }
    }
    
    private static boolean isHDR(final MediaMetadataRetriever mediaMetadataRetriever) throws NumberFormatException {
        final String metadata = mediaMetadataRetriever.extractMetadata(36);
        final String metadata2 = mediaMetadataRetriever.extractMetadata(35);
        final int int1 = Integer.parseInt(metadata);
        final int int2 = Integer.parseInt(metadata2);
        return (int1 == 7 || int1 == 6) && int2 == 6;
    }
    
    static boolean isHdr180RotationFixRequired() {
        if (Build.MODEL.startsWith("Pixel") && Build$VERSION.SDK_INT == 33) {
            return isTBuildRequiringRotationFix();
        }
        return Build$VERSION.SDK_INT >= 30 && Build$VERSION.SDK_INT < 33;
    }
    
    private static boolean isTBuildRequiringRotationFix() {
        final Iterator<String> iterator = VideoDecoder.PIXEL_T_BUILD_ID_PREFIXES_REQUIRING_HDR_180_ROTATION_FIX.iterator();
        while (iterator.hasNext()) {
            if (Build.ID.startsWith(iterator.next())) {
                return true;
            }
        }
        return false;
    }
    
    private boolean isUnsupportedFormat(T t, final MediaMetadataRetriever mediaMetadataRetriever) {
        if (Build$VERSION.SDK_INT < 16) {
            return false;
        }
        if (Build.DEVICE == null || !Build.DEVICE.matches(".+_cheets|cheets_.+")) {
            return false;
        }
        final T t2 = null;
        try {
            if (!"video/webm".equals(mediaMetadataRetriever.extractMetadata(12))) {
                return false;
            }
            final MediaExtractor mediaExtractor = new MediaExtractor();
            try {
                this.initializer.initializeExtractor(mediaExtractor, t);
                for (int trackCount = mediaExtractor.getTrackCount(), i = 0; i < trackCount; ++i) {
                    if ("video/x-vnd.on2.vp8".equals(mediaExtractor.getTrackFormat(i).getString("mime"))) {
                        mediaExtractor.release();
                        return true;
                    }
                }
                mediaExtractor.release();
            }
            finally {}
        }
        finally {
            t = t2;
        }
        try {
            if (Log.isLoggable("VideoDecoder", 3)) {
                final Throwable t3;
                Log.d("VideoDecoder", "Exception trying to extract track info for a webm video on CrOS.", t3);
            }
            if (t != null) {
                ((MediaExtractor)t).release();
            }
            return false;
        }
        finally {
            if (t != null) {
                ((MediaExtractor)t).release();
            }
        }
    }
    
    public static ResourceDecoder<ParcelFileDescriptor, Bitmap> parcel(final BitmapPool bitmapPool) {
        return new VideoDecoder<ParcelFileDescriptor>(bitmapPool, (MediaInitializer<ParcelFileDescriptor>)new ParcelFileDescriptorInitializer());
    }
    
    @Override
    public Resource<Bitmap> decode(final T t, final int n, final int n2, final Options options) throws IOException {
        final long longValue = options.get(VideoDecoder.TARGET_FRAME);
        if (longValue < 0L && longValue != -1L) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Requested frame must be non-negative, or DEFAULT_FRAME, given: ");
            sb.append(longValue);
            throw new IllegalArgumentException(sb.toString());
        }
        Integer value;
        if ((value = options.get(VideoDecoder.FRAME_OPTION)) == null) {
            value = 2;
        }
        DownsampleStrategy default1;
        if ((default1 = options.get(DownsampleStrategy.OPTION)) == null) {
            default1 = DownsampleStrategy.DEFAULT;
        }
        final MediaMetadataRetriever build = this.factory.build();
        try {
            this.initializer.initializeRetriever(build, t);
            final Bitmap decodeFrame = this.decodeFrame(t, build, longValue, value, n, n2, default1);
            if (Build$VERSION.SDK_INT >= 29) {
                build.close();
            }
            else {
                build.release();
            }
            return BitmapResource.obtain(decodeFrame, this.bitmapPool);
        }
        finally {
            if (Build$VERSION.SDK_INT >= 29) {
                build.close();
            }
            else {
                build.release();
            }
        }
    }
    
    @Override
    public boolean handles(final T t, final Options options) {
        return true;
    }
    
    private static final class AssetFileDescriptorInitializer implements MediaInitializer<AssetFileDescriptor>
    {
        public void initializeExtractor(final MediaExtractor mediaExtractor, final AssetFileDescriptor assetFileDescriptor) throws IOException {
            mediaExtractor.setDataSource(assetFileDescriptor.getFileDescriptor(), assetFileDescriptor.getStartOffset(), assetFileDescriptor.getLength());
        }
        
        public void initializeRetriever(final MediaMetadataRetriever mediaMetadataRetriever, final AssetFileDescriptor assetFileDescriptor) {
            mediaMetadataRetriever.setDataSource(assetFileDescriptor.getFileDescriptor(), assetFileDescriptor.getStartOffset(), assetFileDescriptor.getLength());
        }
    }
    
    static final class ByteBufferInitializer implements MediaInitializer<ByteBuffer>
    {
        private MediaDataSource getMediaDataSource(final ByteBuffer byteBuffer) {
            return new MediaDataSource(this, byteBuffer) {
                final ByteBufferInitializer this$0;
                final ByteBuffer val$data;
                
                public void close() {
                }
                
                public long getSize() {
                    return this.val$data.limit();
                }
                
                public int readAt(final long n, final byte[] dst, final int offset, int min) {
                    if (n >= this.val$data.limit()) {
                        return -1;
                    }
                    this.val$data.position((int)n);
                    min = Math.min(min, this.val$data.remaining());
                    this.val$data.get(dst, offset, min);
                    return min;
                }
            };
        }
        
        public void initializeExtractor(final MediaExtractor mediaExtractor, final ByteBuffer byteBuffer) throws IOException {
            mediaExtractor.setDataSource(this.getMediaDataSource(byteBuffer));
        }
        
        public void initializeRetriever(final MediaMetadataRetriever mediaMetadataRetriever, final ByteBuffer byteBuffer) {
            mediaMetadataRetriever.setDataSource(this.getMediaDataSource(byteBuffer));
        }
    }
    
    interface MediaInitializer<T>
    {
        void initializeExtractor(final MediaExtractor p0, final T p1) throws IOException;
        
        void initializeRetriever(final MediaMetadataRetriever p0, final T p1);
    }
    
    static class MediaMetadataRetrieverFactory
    {
        public MediaMetadataRetriever build() {
            return new MediaMetadataRetriever();
        }
    }
    
    static final class ParcelFileDescriptorInitializer implements MediaInitializer<ParcelFileDescriptor>
    {
        public void initializeExtractor(final MediaExtractor mediaExtractor, final ParcelFileDescriptor parcelFileDescriptor) throws IOException {
            mediaExtractor.setDataSource(parcelFileDescriptor.getFileDescriptor());
        }
        
        public void initializeRetriever(final MediaMetadataRetriever mediaMetadataRetriever, final ParcelFileDescriptor parcelFileDescriptor) {
            mediaMetadataRetriever.setDataSource(parcelFileDescriptor.getFileDescriptor());
        }
    }
    
    private static final class VideoDecoderException extends RuntimeException
    {
        private static final long serialVersionUID = -2556382523004027815L;
        
        VideoDecoderException() {
            super("MediaMetadataRetriever failed to retrieve a frame without throwing, check the adb logs for .*MetadataRetriever.* prior to this exception for details");
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide.load.resource.bitmap;

import java.nio.ByteBuffer;
import java.security.MessageDigest;
import android.graphics.Bitmap;
import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPool;
import com.bumptech.glide.util.Util;

public final class GranularRoundedCorners extends BitmapTransformation
{
    private static final String ID = "com.bumptech.glide.load.resource.bitmap.GranularRoundedCorners";
    private static final byte[] ID_BYTES;
    private final float bottomLeft;
    private final float bottomRight;
    private final float topLeft;
    private final float topRight;
    
    static {
        ID_BYTES = "com.bumptech.glide.load.resource.bitmap.GranularRoundedCorners".getBytes(GranularRoundedCorners.CHARSET);
    }
    
    public GranularRoundedCorners(final float topLeft, final float topRight, final float bottomRight, final float bottomLeft) {
        this.topLeft = topLeft;
        this.topRight = topRight;
        this.bottomRight = bottomRight;
        this.bottomLeft = bottomLeft;
    }
    
    @Override
    public boolean equals(final Object o) {
        final boolean b = o instanceof GranularRoundedCorners;
        boolean b3;
        final boolean b2 = b3 = false;
        if (b) {
            final GranularRoundedCorners granularRoundedCorners = (GranularRoundedCorners)o;
            b3 = b2;
            if (this.topLeft == granularRoundedCorners.topLeft) {
                b3 = b2;
                if (this.topRight == granularRoundedCorners.topRight) {
                    b3 = b2;
                    if (this.bottomRight == granularRoundedCorners.bottomRight) {
                        b3 = b2;
                        if (this.bottomLeft == granularRoundedCorners.bottomLeft) {
                            b3 = true;
                        }
                    }
                }
            }
        }
        return b3;
    }
    
    @Override
    public int hashCode() {
        return Util.hashCode(this.bottomLeft, Util.hashCode(this.bottomRight, Util.hashCode(this.topRight, Util.hashCode(-2013597734, Util.hashCode(this.topLeft)))));
    }
    
    @Override
    protected Bitmap transform(final BitmapPool bitmapPool, final Bitmap bitmap, final int n, final int n2) {
        return TransformationUtils.roundedCorners(bitmapPool, bitmap, this.topLeft, this.topRight, this.bottomRight, this.bottomLeft);
    }
    
    @Override
    public void updateDiskCacheKey(final MessageDigest messageDigest) {
        messageDigest.update(GranularRoundedCorners.ID_BYTES);
        messageDigest.update(ByteBuffer.allocate(16).putFloat(this.topLeft).putFloat(this.topRight).putFloat(this.bottomRight).putFloat(this.bottomLeft).array());
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide.load.resource.bitmap;

import com.bumptech.glide.load.data.ParcelFileDescriptorRewinder;
import java.io.File;
import java.nio.ByteBuffer;
import java.io.InputStream;
import android.os.ParcelFileDescriptor;
import android.graphics.ColorSpace;
import android.graphics.ColorSpace$Named;
import com.bumptech.glide.util.LogTime;
import com.bumptech.glide.load.engine.Resource;
import com.bumptech.glide.load.Options;
import android.graphics.Bitmap$Config;
import java.io.IOException;
import android.util.Log;
import android.os.Build$VERSION;
import com.bumptech.glide.util.Preconditions;
import com.bumptech.glide.util.Util;
import java.util.EnumSet;
import android.graphics.Bitmap;
import java.util.Collections;
import java.util.Collection;
import java.util.HashSet;
import java.util.Arrays;
import java.util.List;
import android.util.DisplayMetrics;
import com.bumptech.glide.load.engine.bitmap_recycle.ArrayPool;
import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPool;
import com.bumptech.glide.load.ImageHeaderParser;
import com.bumptech.glide.load.PreferredColorSpace;
import android.graphics.BitmapFactory$Options;
import java.util.Queue;
import java.util.Set;
import com.bumptech.glide.load.DecodeFormat;
import com.bumptech.glide.load.Option;

public final class Downsampler
{
    public static final Option<Boolean> ALLOW_HARDWARE_CONFIG;
    public static final Option<DecodeFormat> DECODE_FORMAT;
    @Deprecated
    public static final Option<DownsampleStrategy> DOWNSAMPLE_STRATEGY;
    private static final DecodeCallbacks EMPTY_CALLBACKS;
    public static final Option<Boolean> FIX_BITMAP_SIZE_TO_REQUESTED_DIMENSIONS;
    private static final String ICO_MIME_TYPE = "image/x-ico";
    private static final Set<String> NO_DOWNSAMPLE_PRE_N_MIME_TYPES;
    private static final Queue<BitmapFactory$Options> OPTIONS_QUEUE;
    public static final Option<PreferredColorSpace> PREFERRED_COLOR_SPACE;
    static final String TAG = "Downsampler";
    private static final Set<ImageHeaderParser.ImageType> TYPES_THAT_USE_POOL_PRE_KITKAT;
    private static final String WBMP_MIME_TYPE = "image/vnd.wap.wbmp";
    private final BitmapPool bitmapPool;
    private final ArrayPool byteArrayPool;
    private final DisplayMetrics displayMetrics;
    private final HardwareConfigState hardwareConfigState;
    private final List<ImageHeaderParser> parsers;
    
    static {
        DECODE_FORMAT = Option.memory("com.bumptech.glide.load.resource.bitmap.Downsampler.DecodeFormat", DecodeFormat.DEFAULT);
        PREFERRED_COLOR_SPACE = Option.memory("com.bumptech.glide.load.resource.bitmap.Downsampler.PreferredColorSpace");
        DOWNSAMPLE_STRATEGY = DownsampleStrategy.OPTION;
        final Boolean value = false;
        FIX_BITMAP_SIZE_TO_REQUESTED_DIMENSIONS = Option.memory("com.bumptech.glide.load.resource.bitmap.Downsampler.FixBitmapSize", value);
        ALLOW_HARDWARE_CONFIG = Option.memory("com.bumptech.glide.load.resource.bitmap.Downsampler.AllowHardwareDecode", value);
        NO_DOWNSAMPLE_PRE_N_MIME_TYPES = Collections.unmodifiableSet((Set<? extends String>)new HashSet<String>(Arrays.asList("image/vnd.wap.wbmp", "image/x-ico")));
        EMPTY_CALLBACKS = (DecodeCallbacks)new DecodeCallbacks() {
            @Override
            public void onDecodeComplete(final BitmapPool bitmapPool, final Bitmap bitmap) {
            }
            
            @Override
            public void onObtainBounds() {
            }
        };
        TYPES_THAT_USE_POOL_PRE_KITKAT = Collections.unmodifiableSet((Set<? extends ImageHeaderParser.ImageType>)EnumSet.of(ImageHeaderParser.ImageType.JPEG, ImageHeaderParser.ImageType.PNG_A, ImageHeaderParser.ImageType.PNG));
        OPTIONS_QUEUE = Util.createQueue(0);
    }
    
    public Downsampler(final List<ImageHeaderParser> parsers, final DisplayMetrics displayMetrics, final BitmapPool bitmapPool, final ArrayPool arrayPool) {
        this.hardwareConfigState = HardwareConfigState.getInstance();
        this.parsers = parsers;
        this.displayMetrics = Preconditions.checkNotNull(displayMetrics);
        this.bitmapPool = Preconditions.checkNotNull(bitmapPool);
        this.byteArrayPool = Preconditions.checkNotNull(arrayPool);
    }
    
    private static int adjustTargetDensityForError(final double n) {
        final int densityMultiplier = getDensityMultiplier(n);
        final int round = round(densityMultiplier * n);
        return round(n / (round / (float)densityMultiplier) * round);
    }
    
    private void calculateConfig(final ImageReader imageReader, final DecodeFormat obj, final boolean b, boolean b2, final BitmapFactory$Options bitmapFactory$Options, final int n, final int n2) {
        if (this.hardwareConfigState.setHardwareConfigIfAllowed(n, n2, bitmapFactory$Options, b, b2)) {
            return;
        }
        if (obj != DecodeFormat.PREFER_ARGB_8888 && Build$VERSION.SDK_INT != 16) {
            b2 = false;
            boolean hasAlpha;
            try {
                hasAlpha = imageReader.getImageType().hasAlpha();
            }
            catch (final IOException ex) {
                hasAlpha = b2;
                if (Log.isLoggable("Downsampler", 3)) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Cannot determine whether the image has alpha or not from header, format ");
                    sb.append(obj);
                    Log.d("Downsampler", sb.toString(), (Throwable)ex);
                    hasAlpha = b2;
                }
            }
            Bitmap$Config inPreferredConfig;
            if (hasAlpha) {
                inPreferredConfig = Bitmap$Config.ARGB_8888;
            }
            else {
                inPreferredConfig = Bitmap$Config.RGB_565;
            }
            bitmapFactory$Options.inPreferredConfig = inPreferredConfig;
            if (bitmapFactory$Options.inPreferredConfig == Bitmap$Config.RGB_565) {
                bitmapFactory$Options.inDither = true;
            }
            return;
        }
        bitmapFactory$Options.inPreferredConfig = Bitmap$Config.ARGB_8888;
    }
    
    private static void calculateScaling(final ImageHeaderParser.ImageType obj, final ImageReader imageReader, final DecodeCallbacks decodeCallbacks, final BitmapPool bitmapPool, final DownsampleStrategy obj2, final int i, final int n, final int n2, final int j, final int k, final BitmapFactory$Options bitmapFactory$Options) throws IOException {
        if (n <= 0 || n2 <= 0) {
            if (Log.isLoggable("Downsampler", 3)) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Unable to determine dimensions for: ");
                sb.append(obj);
                sb.append(" with target [");
                sb.append(j);
                sb.append("x");
                sb.append(k);
                sb.append("]");
                Log.d("Downsampler", sb.toString());
            }
            return;
        }
        int n3;
        int n4;
        if (isRotationRequired(i)) {
            n3 = n;
            n4 = n2;
        }
        else {
            n4 = n;
            n3 = n2;
        }
        final float scaleFactor = obj2.getScaleFactor(n4, n3, j, k);
        if (scaleFactor <= 0.0f) {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("Cannot scale with factor: ");
            sb2.append(scaleFactor);
            sb2.append(" from: ");
            sb2.append(obj2);
            sb2.append(", source: [");
            sb2.append(n);
            sb2.append("x");
            sb2.append(n2);
            sb2.append("], target: [");
            sb2.append(j);
            sb2.append("x");
            sb2.append(k);
            sb2.append("]");
            throw new IllegalArgumentException(sb2.toString());
        }
        final DownsampleStrategy.SampleSizeRounding sampleSizeRounding = obj2.getSampleSizeRounding(n4, n3, j, k);
        if (sampleSizeRounding != null) {
            final float n5 = (float)n4;
            final int round = round(scaleFactor * n5);
            final float n6 = (float)n3;
            final int round2 = round(scaleFactor * n6);
            final int n7 = n4 / round;
            final int n8 = n3 / round2;
            int l;
            if (sampleSizeRounding == DownsampleStrategy.SampleSizeRounding.MEMORY) {
                l = Math.max(n7, n8);
            }
            else {
                l = Math.min(n7, n8);
            }
            int max;
            if (Build$VERSION.SDK_INT <= 23 && Downsampler.NO_DOWNSAMPLE_PRE_N_MIME_TYPES.contains(bitmapFactory$Options.outMimeType)) {
                max = 1;
            }
            else {
                final int n9 = max = Math.max(1, Integer.highestOneBit(l));
                if (sampleSizeRounding == DownsampleStrategy.SampleSizeRounding.MEMORY) {
                    max = n9;
                    if (n9 < 1.0f / scaleFactor) {
                        max = n9 << 1;
                    }
                }
            }
            bitmapFactory$Options.inSampleSize = max;
            int round3 = 0;
            int round4 = 0;
            Label_0498: {
                if (obj == ImageHeaderParser.ImageType.JPEG) {
                    final float n10 = (float)Math.min(max, 8);
                    final int n11 = (int)Math.ceil(n5 / n10);
                    final int n12 = (int)Math.ceil(n6 / n10);
                    final int n13 = max / 8;
                    round3 = n12;
                    round4 = n11;
                    if (n13 > 0) {
                        round4 = n11 / n13;
                        round3 = n12 / n13;
                    }
                }
                else {
                    double n16;
                    if (obj != ImageHeaderParser.ImageType.PNG && obj != ImageHeaderParser.ImageType.PNG_A) {
                        if (obj.isWebp()) {
                            if (Build$VERSION.SDK_INT >= 24) {
                                final float n14 = (float)max;
                                round4 = Math.round(n5 / n14);
                                round3 = Math.round(n6 / n14);
                                break Label_0498;
                            }
                            final float n15 = (float)max;
                            round4 = (int)Math.floor(n5 / n15);
                            n16 = Math.floor(n6 / n15);
                        }
                        else {
                            if (n4 % max == 0 && n3 % max == 0) {
                                round4 = n4 / max;
                                round3 = n3 / max;
                                break Label_0498;
                            }
                            final int[] dimensions = getDimensions(imageReader, bitmapFactory$Options, decodeCallbacks, bitmapPool);
                            round4 = dimensions[0];
                            round3 = dimensions[1];
                            break Label_0498;
                        }
                    }
                    else {
                        final float n17 = (float)max;
                        round4 = (int)Math.floor(n5 / n17);
                        n16 = Math.floor(n6 / n17);
                    }
                    round3 = (int)n16;
                }
            }
            final double d = obj2.getScaleFactor(round4, round3, j, k);
            if (Build$VERSION.SDK_INT >= 19) {
                bitmapFactory$Options.inTargetDensity = adjustTargetDensityForError(d);
                bitmapFactory$Options.inDensity = getDensityMultiplier(d);
            }
            if (isScaling(bitmapFactory$Options)) {
                bitmapFactory$Options.inScaled = true;
            }
            else {
                bitmapFactory$Options.inTargetDensity = 0;
                bitmapFactory$Options.inDensity = 0;
            }
            if (Log.isLoggable("Downsampler", 2)) {
                final StringBuilder sb3 = new StringBuilder();
                sb3.append("Calculate scaling, source: [");
                sb3.append(n);
                sb3.append("x");
                sb3.append(n2);
                sb3.append("], degreesToRotate: ");
                sb3.append(i);
                sb3.append(", target: [");
                sb3.append(j);
                sb3.append("x");
                sb3.append(k);
                sb3.append("], power of two scaled: [");
                sb3.append(round4);
                sb3.append("x");
                sb3.append(round3);
                sb3.append("], exact scale factor: ");
                sb3.append(scaleFactor);
                sb3.append(", power of 2 sample size: ");
                sb3.append(max);
                sb3.append(", adjusted scale factor: ");
                sb3.append(d);
                sb3.append(", target density: ");
                sb3.append(bitmapFactory$Options.inTargetDensity);
                sb3.append(", density: ");
                sb3.append(bitmapFactory$Options.inDensity);
                Log.v("Downsampler", sb3.toString());
            }
            return;
        }
        throw new IllegalArgumentException("Cannot round with null rounding");
    }
    
    private Resource<Bitmap> decode(final ImageReader imageReader, final int n, final int n2, final Options options, final DecodeCallbacks decodeCallbacks) throws IOException {
        final byte[] inTempStorage = this.byteArrayPool.get(65536, byte[].class);
        final BitmapFactory$Options defaultOptions = getDefaultOptions();
        defaultOptions.inTempStorage = inTempStorage;
        final DecodeFormat decodeFormat = options.get(Downsampler.DECODE_FORMAT);
        final PreferredColorSpace preferredColorSpace = options.get(Downsampler.PREFERRED_COLOR_SPACE);
        final DownsampleStrategy downsampleStrategy = options.get(DownsampleStrategy.OPTION);
        final boolean booleanValue = options.get(Downsampler.FIX_BITMAP_SIZE_TO_REQUESTED_DIMENSIONS);
        final Option<Boolean> allow_HARDWARE_CONFIG = Downsampler.ALLOW_HARDWARE_CONFIG;
        boolean b;
        if (options.get((Option<Object>)allow_HARDWARE_CONFIG) != null && options.get(allow_HARDWARE_CONFIG)) {
            b = true;
        }
        else {
            b = false;
        }
        try {
            return BitmapResource.obtain(this.decodeFromWrappedStreams(imageReader, defaultOptions, downsampleStrategy, decodeFormat, preferredColorSpace, b, n, n2, booleanValue, decodeCallbacks), this.bitmapPool);
        }
        finally {
            releaseOptions(defaultOptions);
            this.byteArrayPool.put(inTempStorage);
        }
    }
    
    private Bitmap decodeFromWrappedStreams(final ImageReader imageReader, final BitmapFactory$Options bitmapFactory$Options, final DownsampleStrategy downsampleStrategy, final DecodeFormat decodeFormat, final PreferredColorSpace preferredColorSpace, boolean b, final int n, final int n2, final boolean b2, final DecodeCallbacks decodeCallbacks) throws IOException {
        final long logTime = LogTime.getLogTime();
        final int[] dimensions = getDimensions(imageReader, bitmapFactory$Options, decodeCallbacks, this.bitmapPool);
        final boolean b3 = false;
        final int i = dimensions[0];
        final int j = dimensions[1];
        final String outMimeType = bitmapFactory$Options.outMimeType;
        if (i == -1 || j == -1) {
            b = false;
        }
        final int imageOrientation = imageReader.getImageOrientation();
        final int exifOrientationDegrees = TransformationUtils.getExifOrientationDegrees(imageOrientation);
        final boolean exifOrientationRequired = TransformationUtils.isExifOrientationRequired(imageOrientation);
        int n3;
        if (n == Integer.MIN_VALUE) {
            if (isRotationRequired(exifOrientationDegrees)) {
                n3 = j;
            }
            else {
                n3 = i;
            }
        }
        else {
            n3 = n;
        }
        int n4;
        if (n2 == Integer.MIN_VALUE) {
            if (isRotationRequired(exifOrientationDegrees)) {
                n4 = i;
            }
            else {
                n4 = j;
            }
        }
        else {
            n4 = n2;
        }
        final ImageHeaderParser.ImageType imageType = imageReader.getImageType();
        calculateScaling(imageType, imageReader, decodeCallbacks, this.bitmapPool, downsampleStrategy, exifOrientationDegrees, i, j, n3, n4, bitmapFactory$Options);
        this.calculateConfig(imageReader, decodeFormat, b, exifOrientationRequired, bitmapFactory$Options, n3, n4);
        final boolean b4 = Build$VERSION.SDK_INT >= 19;
        if ((bitmapFactory$Options.inSampleSize == 1 || b4) && this.shouldUsePool(imageType)) {
            if (i < 0 || j < 0 || !b2 || !b4) {
                float f;
                if (isScaling(bitmapFactory$Options)) {
                    f = bitmapFactory$Options.inTargetDensity / (float)bitmapFactory$Options.inDensity;
                }
                else {
                    f = 1.0f;
                }
                final int inSampleSize = bitmapFactory$Options.inSampleSize;
                final float n5 = (float)i;
                final float n6 = (float)inSampleSize;
                final int n7 = (int)Math.ceil(n5 / n6);
                final int n8 = (int)Math.ceil(j / n6);
                final int round = Math.round(n7 * f);
                final int round2 = Math.round(n8 * f);
                n3 = round;
                n4 = round2;
                if (Log.isLoggable("Downsampler", 2)) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Calculated target [");
                    sb.append(round);
                    sb.append("x");
                    sb.append(round2);
                    sb.append("] for source [");
                    sb.append(i);
                    sb.append("x");
                    sb.append(j);
                    sb.append("], sampleSize: ");
                    sb.append(inSampleSize);
                    sb.append(", targetDensity: ");
                    sb.append(bitmapFactory$Options.inTargetDensity);
                    sb.append(", density: ");
                    sb.append(bitmapFactory$Options.inDensity);
                    sb.append(", density multiplier: ");
                    sb.append(f);
                    Log.v("Downsampler", sb.toString());
                    n4 = round2;
                    n3 = round;
                }
            }
            if (n3 > 0 && n4 > 0) {
                setInBitmap(bitmapFactory$Options, this.bitmapPool, n3, n4);
            }
        }
        if (preferredColorSpace != null) {
            if (Build$VERSION.SDK_INT >= 28) {
                int n9 = b3 ? 1 : 0;
                if (preferredColorSpace == PreferredColorSpace.DISPLAY_P3) {
                    n9 = (b3 ? 1 : 0);
                    if (bitmapFactory$Options.outColorSpace != null) {
                        n9 = (b3 ? 1 : 0);
                        if (bitmapFactory$Options.outColorSpace.isWideGamut()) {
                            n9 = 1;
                        }
                    }
                }
                ColorSpace$Named colorSpace$Named;
                if (n9 != 0) {
                    colorSpace$Named = ColorSpace$Named.DISPLAY_P3;
                }
                else {
                    colorSpace$Named = ColorSpace$Named.SRGB;
                }
                bitmapFactory$Options.inPreferredColorSpace = ColorSpace.get(colorSpace$Named);
            }
            else if (Build$VERSION.SDK_INT >= 26) {
                bitmapFactory$Options.inPreferredColorSpace = ColorSpace.get(ColorSpace$Named.SRGB);
            }
        }
        final Bitmap decodeStream = decodeStream(imageReader, bitmapFactory$Options, decodeCallbacks, this.bitmapPool);
        decodeCallbacks.onDecodeComplete(this.bitmapPool, decodeStream);
        if (Log.isLoggable("Downsampler", 2)) {
            logDecode(i, j, outMimeType, bitmapFactory$Options, decodeStream, n, n2, logTime);
        }
        Bitmap rotateImageExif = null;
        if (decodeStream != null) {
            decodeStream.setDensity(this.displayMetrics.densityDpi);
            final Bitmap obj = rotateImageExif = TransformationUtils.rotateImageExif(this.bitmapPool, decodeStream, imageOrientation);
            if (!decodeStream.equals(obj)) {
                this.bitmapPool.put(decodeStream);
                rotateImageExif = obj;
            }
        }
        return rotateImageExif;
    }
    
    private static Bitmap decodeStream(final ImageReader imageReader, final BitmapFactory$Options bitmapFactory$Options, final DecodeCallbacks decodeCallbacks, final BitmapPool bitmapPool) throws IOException {
        if (!bitmapFactory$Options.inJustDecodeBounds) {
            decodeCallbacks.onObtainBounds();
            imageReader.stopGrowingBuffers();
        }
        final int outWidth = bitmapFactory$Options.outWidth;
        final int outHeight = bitmapFactory$Options.outHeight;
        final String outMimeType = bitmapFactory$Options.outMimeType;
        TransformationUtils.getBitmapDrawableLock().lock();
        try {
            try {
                final Bitmap decodeBitmap = imageReader.decodeBitmap(bitmapFactory$Options);
                TransformationUtils.getBitmapDrawableLock().unlock();
                return decodeBitmap;
            }
            finally {}
        }
        catch (final IllegalArgumentException ex) {
            final IOException ioExceptionForInBitmapAssertion = newIoExceptionForInBitmapAssertion(ex, outWidth, outHeight, outMimeType, bitmapFactory$Options);
            if (Log.isLoggable("Downsampler", 3)) {
                Log.d("Downsampler", "Failed to decode with inBitmap, trying again without Bitmap re-use", (Throwable)ioExceptionForInBitmapAssertion);
            }
            if (bitmapFactory$Options.inBitmap != null) {
                try {
                    bitmapPool.put(bitmapFactory$Options.inBitmap);
                    bitmapFactory$Options.inBitmap = null;
                    final Bitmap decodeStream = decodeStream(imageReader, bitmapFactory$Options, decodeCallbacks, bitmapPool);
                    TransformationUtils.getBitmapDrawableLock().unlock();
                    return decodeStream;
                }
                catch (final IOException ex2) {
                    throw ioExceptionForInBitmapAssertion;
                }
            }
            throw ioExceptionForInBitmapAssertion;
        }
        TransformationUtils.getBitmapDrawableLock().unlock();
    }
    
    private static String getBitmapString(final Bitmap bitmap) {
        if (bitmap == null) {
            return null;
        }
        String string;
        if (Build$VERSION.SDK_INT >= 19) {
            final StringBuilder sb = new StringBuilder();
            sb.append(" (");
            sb.append(bitmap.getAllocationByteCount());
            sb.append(")");
            string = sb.toString();
        }
        else {
            string = "";
        }
        final StringBuilder sb2 = new StringBuilder();
        sb2.append("[");
        sb2.append(bitmap.getWidth());
        sb2.append("x");
        sb2.append(bitmap.getHeight());
        sb2.append("] ");
        sb2.append(bitmap.getConfig());
        sb2.append(string);
        return sb2.toString();
    }
    
    private static BitmapFactory$Options getDefaultOptions() {
        synchronized (Downsampler.class) {
            Object options_QUEUE = Downsampler.OPTIONS_QUEUE;
            synchronized (options_QUEUE) {
                final BitmapFactory$Options bitmapFactory$Options = ((Queue<BitmapFactory$Options>)options_QUEUE).poll();
                monitorexit(options_QUEUE);
                options_QUEUE = bitmapFactory$Options;
                if (bitmapFactory$Options == null) {
                    options_QUEUE = new BitmapFactory$Options();
                    resetOptions((BitmapFactory$Options)options_QUEUE);
                }
                return (BitmapFactory$Options)options_QUEUE;
            }
        }
    }
    
    private static int getDensityMultiplier(double n) {
        if (n > 1.0) {
            n = 1.0 / n;
        }
        return (int)Math.round(n * 2.147483647E9);
    }
    
    private static int[] getDimensions(final ImageReader imageReader, final BitmapFactory$Options bitmapFactory$Options, final DecodeCallbacks decodeCallbacks, final BitmapPool bitmapPool) throws IOException {
        bitmapFactory$Options.inJustDecodeBounds = true;
        decodeStream(imageReader, bitmapFactory$Options, decodeCallbacks, bitmapPool);
        bitmapFactory$Options.inJustDecodeBounds = false;
        return new int[] { bitmapFactory$Options.outWidth, bitmapFactory$Options.outHeight };
    }
    
    private static String getInBitmapString(final BitmapFactory$Options bitmapFactory$Options) {
        return getBitmapString(bitmapFactory$Options.inBitmap);
    }
    
    private static boolean isRotationRequired(final int n) {
        return n == 90 || n == 270;
    }
    
    private static boolean isScaling(final BitmapFactory$Options bitmapFactory$Options) {
        return bitmapFactory$Options.inTargetDensity > 0 && bitmapFactory$Options.inDensity > 0 && bitmapFactory$Options.inTargetDensity != bitmapFactory$Options.inDensity;
    }
    
    private static void logDecode(final int i, final int j, final String str, final BitmapFactory$Options bitmapFactory$Options, final Bitmap bitmap, final int k, final int l, final long n) {
        final StringBuilder sb = new StringBuilder();
        sb.append("Decoded ");
        sb.append(getBitmapString(bitmap));
        sb.append(" from [");
        sb.append(i);
        sb.append("x");
        sb.append(j);
        sb.append("] ");
        sb.append(str);
        sb.append(" with inBitmap ");
        sb.append(getInBitmapString(bitmapFactory$Options));
        sb.append(" for [");
        sb.append(k);
        sb.append("x");
        sb.append(l);
        sb.append("], sample size: ");
        sb.append(bitmapFactory$Options.inSampleSize);
        sb.append(", density: ");
        sb.append(bitmapFactory$Options.inDensity);
        sb.append(", target density: ");
        sb.append(bitmapFactory$Options.inTargetDensity);
        sb.append(", thread: ");
        sb.append(Thread.currentThread().getName());
        sb.append(", duration: ");
        sb.append(LogTime.getElapsedMillis(n));
        Log.v("Downsampler", sb.toString());
    }
    
    private static IOException newIoExceptionForInBitmapAssertion(final IllegalArgumentException cause, final int i, final int j, final String str, final BitmapFactory$Options bitmapFactory$Options) {
        final StringBuilder sb = new StringBuilder();
        sb.append("Exception decoding bitmap, outWidth: ");
        sb.append(i);
        sb.append(", outHeight: ");
        sb.append(j);
        sb.append(", outMimeType: ");
        sb.append(str);
        sb.append(", inBitmap: ");
        sb.append(getInBitmapString(bitmapFactory$Options));
        return new IOException(sb.toString(), cause);
    }
    
    private static void releaseOptions(final BitmapFactory$Options bitmapFactory$Options) {
        resetOptions(bitmapFactory$Options);
        final Queue<BitmapFactory$Options> options_QUEUE = Downsampler.OPTIONS_QUEUE;
        synchronized (options_QUEUE) {
            options_QUEUE.offer(bitmapFactory$Options);
        }
    }
    
    private static void resetOptions(final BitmapFactory$Options bitmapFactory$Options) {
        bitmapFactory$Options.inTempStorage = null;
        bitmapFactory$Options.inDither = false;
        bitmapFactory$Options.inScaled = false;
        bitmapFactory$Options.inSampleSize = 1;
        bitmapFactory$Options.inPreferredConfig = null;
        bitmapFactory$Options.inJustDecodeBounds = false;
        bitmapFactory$Options.inDensity = 0;
        bitmapFactory$Options.inTargetDensity = 0;
        if (Build$VERSION.SDK_INT >= 26) {
            bitmapFactory$Options.inPreferredColorSpace = null;
            bitmapFactory$Options.outColorSpace = null;
            bitmapFactory$Options.outConfig = null;
        }
        bitmapFactory$Options.outWidth = 0;
        bitmapFactory$Options.outHeight = 0;
        bitmapFactory$Options.outMimeType = null;
        bitmapFactory$Options.inBitmap = null;
        bitmapFactory$Options.inMutable = true;
    }
    
    private static int round(final double n) {
        return (int)(n + 0.5);
    }
    
    private static void setInBitmap(final BitmapFactory$Options bitmapFactory$Options, final BitmapPool bitmapPool, final int n, final int n2) {
        Bitmap$Config outConfig;
        if (Build$VERSION.SDK_INT >= 26) {
            if (bitmapFactory$Options.inPreferredConfig == Bitmap$Config.HARDWARE) {
                return;
            }
            outConfig = bitmapFactory$Options.outConfig;
        }
        else {
            outConfig = null;
        }
        Bitmap$Config inPreferredConfig = outConfig;
        if (outConfig == null) {
            inPreferredConfig = bitmapFactory$Options.inPreferredConfig;
        }
        bitmapFactory$Options.inBitmap = bitmapPool.getDirty(n, n2, inPreferredConfig);
    }
    
    private boolean shouldUsePool(final ImageHeaderParser.ImageType imageType) {
        return Build$VERSION.SDK_INT >= 19 || Downsampler.TYPES_THAT_USE_POOL_PRE_KITKAT.contains(imageType);
    }
    
    public Resource<Bitmap> decode(final ParcelFileDescriptor parcelFileDescriptor, final int n, final int n2, final Options options) throws IOException {
        return this.decode(new ImageReader.ParcelFileDescriptorImageReader(parcelFileDescriptor, this.parsers, this.byteArrayPool), n, n2, options, Downsampler.EMPTY_CALLBACKS);
    }
    
    public Resource<Bitmap> decode(final InputStream inputStream, final int n, final int n2, final Options options) throws IOException {
        return this.decode(inputStream, n, n2, options, Downsampler.EMPTY_CALLBACKS);
    }
    
    public Resource<Bitmap> decode(final InputStream inputStream, final int n, final int n2, final Options options, final DecodeCallbacks decodeCallbacks) throws IOException {
        return this.decode(new ImageReader.InputStreamImageReader(inputStream, this.parsers, this.byteArrayPool), n, n2, options, decodeCallbacks);
    }
    
    public Resource<Bitmap> decode(final ByteBuffer byteBuffer, final int n, final int n2, final Options options) throws IOException {
        return this.decode(new ImageReader.ByteBufferReader(byteBuffer, this.parsers, this.byteArrayPool), n, n2, options, Downsampler.EMPTY_CALLBACKS);
    }
    
    void decode(final File file, final int n, final int n2, final Options options) throws IOException {
        this.decode(new ImageReader.FileReader(file, this.parsers, this.byteArrayPool), n, n2, options, Downsampler.EMPTY_CALLBACKS);
    }
    
    void decode(final byte[] array, final int n, final int n2, final Options options) throws IOException {
        this.decode(new ImageReader.ByteArrayReader(array, this.parsers, this.byteArrayPool), n, n2, options, Downsampler.EMPTY_CALLBACKS);
    }
    
    public boolean handles(final ParcelFileDescriptor parcelFileDescriptor) {
        return ParcelFileDescriptorRewinder.isSupported();
    }
    
    public boolean handles(final InputStream inputStream) {
        return true;
    }
    
    public boolean handles(final ByteBuffer byteBuffer) {
        return true;
    }
    
    public interface DecodeCallbacks
    {
        void onDecodeComplete(final BitmapPool p0, final Bitmap p1) throws IOException;
        
        void onObtainBounds();
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide.load.resource.bitmap;

import java.io.IOException;
import android.graphics.drawable.Drawable;
import com.bumptech.glide.load.engine.Resource;
import com.bumptech.glide.load.Options;
import com.bumptech.glide.load.resource.drawable.ResourceDrawableDecoder;
import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPool;
import android.graphics.Bitmap;
import android.net.Uri;
import com.bumptech.glide.load.ResourceDecoder;

public class ResourceBitmapDecoder implements ResourceDecoder<Uri, Bitmap>
{
    private final BitmapPool bitmapPool;
    private final ResourceDrawableDecoder drawableDecoder;
    
    public ResourceBitmapDecoder(final ResourceDrawableDecoder drawableDecoder, final BitmapPool bitmapPool) {
        this.drawableDecoder = drawableDecoder;
        this.bitmapPool = bitmapPool;
    }
    
    @Override
    public Resource<Bitmap> decode(final Uri uri, final int n, final int n2, final Options options) {
        final Resource<Drawable> decode = this.drawableDecoder.decode(uri, n, n2, options);
        if (decode == null) {
            return null;
        }
        return DrawableToBitmapConverter.convert(this.bitmapPool, decode.get(), n, n2);
    }
    
    @Override
    public boolean handles(final Uri uri, final Options options) {
        return "android.resource".equals(uri.getScheme());
    }
}

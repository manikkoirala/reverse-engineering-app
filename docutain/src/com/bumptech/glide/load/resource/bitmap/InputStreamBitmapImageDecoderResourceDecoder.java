// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide.load.resource.bitmap;

import java.io.IOException;
import android.graphics.ImageDecoder;
import com.bumptech.glide.util.ByteBufferUtil;
import com.bumptech.glide.load.engine.Resource;
import com.bumptech.glide.load.Options;
import android.graphics.Bitmap;
import java.io.InputStream;
import com.bumptech.glide.load.ResourceDecoder;

public final class InputStreamBitmapImageDecoderResourceDecoder implements ResourceDecoder<InputStream, Bitmap>
{
    private final BitmapImageDecoderResourceDecoder wrapped;
    
    public InputStreamBitmapImageDecoderResourceDecoder() {
        this.wrapped = new BitmapImageDecoderResourceDecoder();
    }
    
    @Override
    public Resource<Bitmap> decode(final InputStream inputStream, final int n, final int n2, final Options options) throws IOException {
        return this.wrapped.decode(ImageDecoder.createSource(ByteBufferUtil.fromStream(inputStream)), n, n2, options);
    }
    
    @Override
    public boolean handles(final InputStream inputStream, final Options options) throws IOException {
        return true;
    }
}

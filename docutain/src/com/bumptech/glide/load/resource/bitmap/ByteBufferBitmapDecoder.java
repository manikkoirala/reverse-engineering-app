// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide.load.resource.bitmap;

import java.io.IOException;
import com.bumptech.glide.load.engine.Resource;
import com.bumptech.glide.load.Options;
import android.graphics.Bitmap;
import java.nio.ByteBuffer;
import com.bumptech.glide.load.ResourceDecoder;

public class ByteBufferBitmapDecoder implements ResourceDecoder<ByteBuffer, Bitmap>
{
    private final Downsampler downsampler;
    
    public ByteBufferBitmapDecoder(final Downsampler downsampler) {
        this.downsampler = downsampler;
    }
    
    @Override
    public Resource<Bitmap> decode(final ByteBuffer byteBuffer, final int n, final int n2, final Options options) throws IOException {
        return this.downsampler.decode(byteBuffer, n, n2, options);
    }
    
    @Override
    public boolean handles(final ByteBuffer byteBuffer, final Options options) {
        return this.downsampler.handles(byteBuffer);
    }
}

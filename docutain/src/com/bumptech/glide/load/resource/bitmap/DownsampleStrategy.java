// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide.load.resource.bitmap;

import android.os.Build$VERSION;
import com.bumptech.glide.load.Option;

public abstract class DownsampleStrategy
{
    public static final DownsampleStrategy AT_LEAST;
    public static final DownsampleStrategy AT_MOST;
    public static final DownsampleStrategy CENTER_INSIDE;
    public static final DownsampleStrategy CENTER_OUTSIDE;
    public static final DownsampleStrategy DEFAULT;
    public static final DownsampleStrategy FIT_CENTER;
    static final boolean IS_BITMAP_FACTORY_SCALING_SUPPORTED;
    public static final DownsampleStrategy NONE;
    public static final Option<DownsampleStrategy> OPTION;
    
    static {
        AT_LEAST = new AtLeast();
        AT_MOST = new AtMost();
        FIT_CENTER = new FitCenter();
        CENTER_INSIDE = new CenterInside();
        final DownsampleStrategy default1 = CENTER_OUTSIDE = new CenterOutside();
        NONE = new None();
        DEFAULT = default1;
        OPTION = Option.memory("com.bumptech.glide.load.resource.bitmap.Downsampler.DownsampleStrategy", default1);
        IS_BITMAP_FACTORY_SCALING_SUPPORTED = (Build$VERSION.SDK_INT >= 19);
    }
    
    public abstract SampleSizeRounding getSampleSizeRounding(final int p0, final int p1, final int p2, final int p3);
    
    public abstract float getScaleFactor(final int p0, final int p1, final int p2, final int p3);
    
    private static class AtLeast extends DownsampleStrategy
    {
        AtLeast() {
        }
        
        @Override
        public SampleSizeRounding getSampleSizeRounding(final int n, final int n2, final int n3, final int n4) {
            return SampleSizeRounding.QUALITY;
        }
        
        @Override
        public float getScaleFactor(int min, final int n, final int n2, final int n3) {
            min = Math.min(n / n3, min / n2);
            float n4 = 1.0f;
            if (min != 0) {
                n4 = 1.0f / Integer.highestOneBit(min);
            }
            return n4;
        }
    }
    
    private static class AtMost extends DownsampleStrategy
    {
        AtMost() {
        }
        
        @Override
        public SampleSizeRounding getSampleSizeRounding(final int n, final int n2, final int n3, final int n4) {
            return SampleSizeRounding.MEMORY;
        }
        
        @Override
        public float getScaleFactor(int n, int b, int i, final int n2) {
            i = (int)Math.ceil(Math.max(b / (float)n2, n / (float)i));
            b = Integer.highestOneBit(i);
            n = 1;
            b = Math.max(1, b);
            if (b >= i) {
                n = 0;
            }
            return 1.0f / (b << n);
        }
    }
    
    private static class CenterInside extends DownsampleStrategy
    {
        CenterInside() {
        }
        
        @Override
        public SampleSizeRounding getSampleSizeRounding(final int n, final int n2, final int n3, final int n4) {
            SampleSizeRounding sampleSizeRounding;
            if (this.getScaleFactor(n, n2, n3, n4) == 1.0f) {
                sampleSizeRounding = SampleSizeRounding.QUALITY;
            }
            else {
                sampleSizeRounding = CenterInside.FIT_CENTER.getSampleSizeRounding(n, n2, n3, n4);
            }
            return sampleSizeRounding;
        }
        
        @Override
        public float getScaleFactor(final int n, final int n2, final int n3, final int n4) {
            return Math.min(1.0f, CenterInside.FIT_CENTER.getScaleFactor(n, n2, n3, n4));
        }
    }
    
    private static class CenterOutside extends DownsampleStrategy
    {
        CenterOutside() {
        }
        
        @Override
        public SampleSizeRounding getSampleSizeRounding(final int n, final int n2, final int n3, final int n4) {
            return SampleSizeRounding.QUALITY;
        }
        
        @Override
        public float getScaleFactor(final int n, final int n2, final int n3, final int n4) {
            return Math.max(n3 / (float)n, n4 / (float)n2);
        }
    }
    
    private static class FitCenter extends DownsampleStrategy
    {
        FitCenter() {
        }
        
        @Override
        public SampleSizeRounding getSampleSizeRounding(final int n, final int n2, final int n3, final int n4) {
            if (FitCenter.IS_BITMAP_FACTORY_SCALING_SUPPORTED) {
                return SampleSizeRounding.QUALITY;
            }
            return SampleSizeRounding.MEMORY;
        }
        
        @Override
        public float getScaleFactor(int max, final int n, final int n2, final int n3) {
            if (FitCenter.IS_BITMAP_FACTORY_SCALING_SUPPORTED) {
                return Math.min(n2 / (float)max, n3 / (float)n);
            }
            max = Math.max(n / n3, max / n2);
            float n4 = 1.0f;
            if (max != 0) {
                n4 = 1.0f / Integer.highestOneBit(max);
            }
            return n4;
        }
    }
    
    private static class None extends DownsampleStrategy
    {
        None() {
        }
        
        @Override
        public SampleSizeRounding getSampleSizeRounding(final int n, final int n2, final int n3, final int n4) {
            return SampleSizeRounding.QUALITY;
        }
        
        @Override
        public float getScaleFactor(final int n, final int n2, final int n3, final int n4) {
            return 1.0f;
        }
    }
    
    public enum SampleSizeRounding
    {
        private static final SampleSizeRounding[] $VALUES;
        
        MEMORY, 
        QUALITY;
    }
}

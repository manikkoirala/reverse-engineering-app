// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide.load.resource.bitmap;

import java.io.IOException;
import com.bumptech.glide.load.engine.Resource;
import com.bumptech.glide.load.Options;
import android.os.Build;
import android.graphics.Bitmap;
import android.os.ParcelFileDescriptor;
import com.bumptech.glide.load.ResourceDecoder;

public final class ParcelFileDescriptorBitmapDecoder implements ResourceDecoder<ParcelFileDescriptor, Bitmap>
{
    private static final int MAXIMUM_FILE_BYTE_SIZE_FOR_FILE_DESCRIPTOR_DECODER = 536870912;
    private final Downsampler downsampler;
    
    public ParcelFileDescriptorBitmapDecoder(final Downsampler downsampler) {
        this.downsampler = downsampler;
    }
    
    private boolean isSafeToTryDecoding(final ParcelFileDescriptor parcelFileDescriptor) {
        final boolean equalsIgnoreCase = "HUAWEI".equalsIgnoreCase(Build.MANUFACTURER);
        boolean b = true;
        if (!equalsIgnoreCase && !"HONOR".equalsIgnoreCase(Build.MANUFACTURER)) {
            return true;
        }
        if (parcelFileDescriptor.getStatSize() > 536870912L) {
            b = false;
        }
        return b;
    }
    
    @Override
    public Resource<Bitmap> decode(final ParcelFileDescriptor parcelFileDescriptor, final int n, final int n2, final Options options) throws IOException {
        return this.downsampler.decode(parcelFileDescriptor, n, n2, options);
    }
    
    @Override
    public boolean handles(final ParcelFileDescriptor parcelFileDescriptor, final Options options) {
        return this.isSafeToTryDecoding(parcelFileDescriptor) && this.downsampler.handles(parcelFileDescriptor);
    }
}

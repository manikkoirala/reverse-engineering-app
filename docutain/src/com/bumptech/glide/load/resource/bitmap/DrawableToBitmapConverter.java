// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide.load.resource.bitmap;

import java.util.concurrent.locks.Lock;
import android.graphics.Canvas;
import android.graphics.Bitmap$Config;
import android.util.Log;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.BitmapDrawable;
import com.bumptech.glide.load.engine.Resource;
import android.graphics.drawable.Drawable;
import android.graphics.Bitmap;
import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPoolAdapter;
import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPool;

final class DrawableToBitmapConverter
{
    private static final BitmapPool NO_RECYCLE_BITMAP_POOL;
    private static final String TAG = "DrawableToBitmap";
    
    static {
        NO_RECYCLE_BITMAP_POOL = new BitmapPoolAdapter() {
            @Override
            public void put(final Bitmap bitmap) {
            }
        };
    }
    
    private DrawableToBitmapConverter() {
    }
    
    static Resource<Bitmap> convert(BitmapPool no_RECYCLE_BITMAP_POOL, Drawable current, int n, final int n2) {
        current = current.getCurrent();
        final boolean b = current instanceof BitmapDrawable;
        final int n3 = 0;
        Bitmap bitmap;
        if (b) {
            bitmap = ((BitmapDrawable)current).getBitmap();
            n = n3;
        }
        else if (!(current instanceof Animatable)) {
            bitmap = drawToBitmap(no_RECYCLE_BITMAP_POOL, current, n, n2);
            n = 1;
        }
        else {
            bitmap = null;
            n = n3;
        }
        if (n == 0) {
            no_RECYCLE_BITMAP_POOL = DrawableToBitmapConverter.NO_RECYCLE_BITMAP_POOL;
        }
        return BitmapResource.obtain(bitmap, no_RECYCLE_BITMAP_POOL);
    }
    
    private static Bitmap drawToBitmap(final BitmapPool bitmapPool, final Drawable drawable, int intrinsicWidth, int intrinsicHeight) {
        if (intrinsicWidth == Integer.MIN_VALUE && drawable.getIntrinsicWidth() <= 0) {
            if (Log.isLoggable("DrawableToBitmap", 5)) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Unable to draw ");
                sb.append(drawable);
                sb.append(" to Bitmap with Target.SIZE_ORIGINAL because the Drawable has no intrinsic width");
                Log.w("DrawableToBitmap", sb.toString());
            }
            return null;
        }
        if (intrinsicHeight == Integer.MIN_VALUE && drawable.getIntrinsicHeight() <= 0) {
            if (Log.isLoggable("DrawableToBitmap", 5)) {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("Unable to draw ");
                sb2.append(drawable);
                sb2.append(" to Bitmap with Target.SIZE_ORIGINAL because the Drawable has no intrinsic height");
                Log.w("DrawableToBitmap", sb2.toString());
            }
            return null;
        }
        if (drawable.getIntrinsicWidth() > 0) {
            intrinsicWidth = drawable.getIntrinsicWidth();
        }
        if (drawable.getIntrinsicHeight() > 0) {
            intrinsicHeight = drawable.getIntrinsicHeight();
        }
        final Lock bitmapDrawableLock = TransformationUtils.getBitmapDrawableLock();
        bitmapDrawableLock.lock();
        final Bitmap value = bitmapPool.get(intrinsicWidth, intrinsicHeight, Bitmap$Config.ARGB_8888);
        try {
            final Canvas canvas = new Canvas(value);
            drawable.setBounds(0, 0, intrinsicWidth, intrinsicHeight);
            drawable.draw(canvas);
            canvas.setBitmap((Bitmap)null);
            return value;
        }
        finally {
            bitmapDrawableLock.unlock();
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide.load.resource.bitmap;

import java.security.MessageDigest;
import android.content.Context;
import com.bumptech.glide.load.engine.Resource;
import com.bumptech.glide.util.Preconditions;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.BitmapDrawable;
import com.bumptech.glide.load.Transformation;

@Deprecated
public class BitmapDrawableTransformation implements Transformation<BitmapDrawable>
{
    private final Transformation<Drawable> wrapped;
    
    public BitmapDrawableTransformation(final Transformation<Bitmap> transformation) {
        this.wrapped = Preconditions.checkNotNull(new DrawableTransformation(transformation, false));
    }
    
    private static Resource<BitmapDrawable> convertToBitmapDrawableResource(final Resource<Drawable> resource) {
        if (resource.get() instanceof BitmapDrawable) {
            return (Resource<BitmapDrawable>)resource;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Wrapped transformation unexpectedly returned a non BitmapDrawable resource: ");
        sb.append(resource.get());
        throw new IllegalArgumentException(sb.toString());
    }
    
    private static Resource<Drawable> convertToDrawableResource(final Resource<BitmapDrawable> resource) {
        return (Resource<Drawable>)resource;
    }
    
    @Override
    public boolean equals(final Object o) {
        return o instanceof BitmapDrawableTransformation && this.wrapped.equals(((BitmapDrawableTransformation)o).wrapped);
    }
    
    @Override
    public int hashCode() {
        return this.wrapped.hashCode();
    }
    
    @Override
    public Resource<BitmapDrawable> transform(final Context context, final Resource<BitmapDrawable> resource, final int n, final int n2) {
        return convertToBitmapDrawableResource(this.wrapped.transform(context, convertToDrawableResource(resource), n, n2));
    }
    
    @Override
    public void updateDiskCacheKey(final MessageDigest messageDigest) {
        this.wrapped.updateDiskCacheKey(messageDigest);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide.load.resource.bitmap;

import java.nio.ByteBuffer;
import com.bumptech.glide.util.Preconditions;
import java.io.InputStream;
import java.nio.ByteOrder;
import java.io.IOException;
import android.util.Log;
import com.bumptech.glide.load.engine.bitmap_recycle.ArrayPool;
import java.nio.charset.Charset;
import com.bumptech.glide.load.ImageHeaderParser;

public final class DefaultImageHeaderParser implements ImageHeaderParser
{
    private static final int AVIF_BRAND = 1635150182;
    private static final int AVIS_BRAND = 1635150195;
    private static final int[] BYTES_PER_FORMAT;
    static final int EXIF_MAGIC_NUMBER = 65496;
    static final int EXIF_SEGMENT_TYPE = 225;
    private static final int FTYP_HEADER = 1718909296;
    private static final int GIF_HEADER = 4671814;
    private static final int INTEL_TIFF_MAGIC_NUMBER = 18761;
    private static final String JPEG_EXIF_SEGMENT_PREAMBLE = "Exif\u0000\u0000";
    static final byte[] JPEG_EXIF_SEGMENT_PREAMBLE_BYTES;
    private static final int MARKER_EOI = 217;
    private static final int MOTOROLA_TIFF_MAGIC_NUMBER = 19789;
    private static final int ORIENTATION_TAG_TYPE = 274;
    private static final int PNG_HEADER = -1991225785;
    private static final int RIFF_HEADER = 1380533830;
    private static final int SEGMENT_SOS = 218;
    static final int SEGMENT_START_ID = 255;
    private static final String TAG = "DfltImageHeaderParser";
    private static final int VP8_HEADER = 1448097792;
    private static final int VP8_HEADER_MASK = -256;
    private static final int VP8_HEADER_TYPE_EXTENDED = 88;
    private static final int VP8_HEADER_TYPE_LOSSLESS = 76;
    private static final int VP8_HEADER_TYPE_MASK = 255;
    private static final int WEBP_EXTENDED_ALPHA_FLAG = 16;
    private static final int WEBP_EXTENDED_ANIMATION_FLAG = 2;
    private static final int WEBP_HEADER = 1464156752;
    private static final int WEBP_LOSSLESS_ALPHA_FLAG = 8;
    
    static {
        JPEG_EXIF_SEGMENT_PREAMBLE_BYTES = "Exif\u0000\u0000".getBytes(Charset.forName("UTF-8"));
        BYTES_PER_FORMAT = new int[] { 0, 1, 1, 2, 4, 8, 1, 1, 2, 4, 8, 4, 8 };
    }
    
    private static int calcTagOffset(final int n, final int n2) {
        return n + 2 + n2 * 12;
    }
    
    private int getOrientation(final Reader reader, final ArrayPool arrayPool) throws IOException {
        try {
            final int uInt16 = reader.getUInt16();
            if (!handles(uInt16)) {
                if (Log.isLoggable("DfltImageHeaderParser", 3)) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Parser doesn't handle magic number: ");
                    sb.append(uInt16);
                    Log.d("DfltImageHeaderParser", sb.toString());
                }
                return -1;
            }
            final int moveToExifSegmentAndGetLength = this.moveToExifSegmentAndGetLength(reader);
            if (moveToExifSegmentAndGetLength == -1) {
                if (Log.isLoggable("DfltImageHeaderParser", 3)) {
                    Log.d("DfltImageHeaderParser", "Failed to parse exif segment length, or exif segment not found");
                }
                return -1;
            }
            final byte[] array = arrayPool.get(moveToExifSegmentAndGetLength, byte[].class);
            try {
                return this.parseExifSegment(reader, array, moveToExifSegmentAndGetLength);
            }
            finally {
                arrayPool.put(array);
            }
        }
        catch (final EndOfFileException ex) {
            return -1;
        }
    }
    
    private ImageType getType(final Reader reader) throws IOException {
        try {
            final int uInt16 = reader.getUInt16();
            if (uInt16 == 65496) {
                return ImageType.JPEG;
            }
            final int n = uInt16 << 8 | reader.getUInt8();
            if (n == 4671814) {
                return ImageType.GIF;
            }
            final int n2 = n << 8 | reader.getUInt8();
            if (n2 == -1991225785) {
                reader.skip(21L);
                try {
                    ImageType imageType;
                    if (reader.getUInt8() >= 3) {
                        imageType = ImageType.PNG_A;
                    }
                    else {
                        imageType = ImageType.PNG;
                    }
                    return imageType;
                }
                catch (final EndOfFileException ex) {
                    return ImageType.PNG;
                }
            }
            if (n2 != 1380533830) {
                return this.sniffAvif(reader, n2);
            }
            reader.skip(4L);
            if ((reader.getUInt16() << 16 | reader.getUInt16()) != 0x57454250) {
                return ImageType.UNKNOWN;
            }
            final int n3 = reader.getUInt16() << 16 | reader.getUInt16();
            if ((n3 & 0xFFFFFF00) != 0x56503800) {
                return ImageType.UNKNOWN;
            }
            final int n4 = n3 & 0xFF;
            if (n4 == 88) {
                reader.skip(4L);
                final short uInt17 = reader.getUInt8();
                if ((uInt17 & 0x2) != 0x0) {
                    return ImageType.ANIMATED_WEBP;
                }
                if ((uInt17 & 0x10) != 0x0) {
                    return ImageType.WEBP_A;
                }
                return ImageType.WEBP;
            }
            else {
                if (n4 == 76) {
                    reader.skip(4L);
                    ImageType imageType2;
                    if ((reader.getUInt8() & 0x8) != 0x0) {
                        imageType2 = ImageType.WEBP_A;
                    }
                    else {
                        imageType2 = ImageType.WEBP;
                    }
                    return imageType2;
                }
                return ImageType.WEBP;
            }
        }
        catch (final EndOfFileException ex2) {
            return ImageType.UNKNOWN;
        }
    }
    
    private static boolean handles(final int n) {
        return (n & 0xFFD8) == 0xFFD8 || n == 19789 || n == 18761;
    }
    
    private boolean hasJpegExifPreamble(final byte[] array, int n) {
        final boolean b = false;
        boolean b2 = array != null && n > DefaultImageHeaderParser.JPEG_EXIF_SEGMENT_PREAMBLE_BYTES.length;
        if (b2) {
            n = 0;
            while (true) {
                final byte[] jpeg_EXIF_SEGMENT_PREAMBLE_BYTES = DefaultImageHeaderParser.JPEG_EXIF_SEGMENT_PREAMBLE_BYTES;
                if (n >= jpeg_EXIF_SEGMENT_PREAMBLE_BYTES.length) {
                    break;
                }
                if (array[n] != jpeg_EXIF_SEGMENT_PREAMBLE_BYTES[n]) {
                    b2 = b;
                    break;
                }
                ++n;
            }
        }
        return b2;
    }
    
    private int moveToExifSegmentAndGetLength(final Reader reader) throws IOException {
        long skip;
        long n;
        short uInt9;
        int i;
        do {
            final short uInt8 = reader.getUInt8();
            if (uInt8 != 255) {
                if (Log.isLoggable("DfltImageHeaderParser", 3)) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Unknown segmentId=");
                    sb.append(uInt8);
                    Log.d("DfltImageHeaderParser", sb.toString());
                }
                return -1;
            }
            uInt9 = reader.getUInt8();
            if (uInt9 == 218) {
                return -1;
            }
            if (uInt9 == 217) {
                if (Log.isLoggable("DfltImageHeaderParser", 3)) {
                    Log.d("DfltImageHeaderParser", "Found MARKER_EOI in exif segment");
                }
                return -1;
            }
            i = reader.getUInt16() - 2;
            if (uInt9 == 225) {
                return i;
            }
            n = i;
            skip = reader.skip(n);
        } while (skip == n);
        if (Log.isLoggable("DfltImageHeaderParser", 3)) {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("Unable to skip enough data, type: ");
            sb2.append(uInt9);
            sb2.append(", wanted to skip: ");
            sb2.append(i);
            sb2.append(", but actually skipped: ");
            sb2.append(skip);
            Log.d("DfltImageHeaderParser", sb2.toString());
        }
        return -1;
    }
    
    private static int parseExifSegment(final RandomAccessReader randomAccessReader) {
        final short int16 = randomAccessReader.getInt16(6);
        ByteOrder byteOrder;
        if (int16 != 18761) {
            if (int16 != 19789) {
                if (Log.isLoggable("DfltImageHeaderParser", 3)) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Unknown endianness = ");
                    sb.append(int16);
                    Log.d("DfltImageHeaderParser", sb.toString());
                }
                byteOrder = ByteOrder.BIG_ENDIAN;
            }
            else {
                byteOrder = ByteOrder.BIG_ENDIAN;
            }
        }
        else {
            byteOrder = ByteOrder.LITTLE_ENDIAN;
        }
        randomAccessReader.order(byteOrder);
        final int n = randomAccessReader.getInt32(10) + 6;
        for (short int17 = randomAccessReader.getInt16(n), i = 0; i < int17; ++i) {
            final int calcTagOffset = calcTagOffset(n, i);
            final short int18 = randomAccessReader.getInt16(calcTagOffset);
            if (int18 == 274) {
                final short int19 = randomAccessReader.getInt16(calcTagOffset + 2);
                if (int19 >= 1 && int19 <= 12) {
                    final int int20 = randomAccessReader.getInt32(calcTagOffset + 4);
                    if (int20 < 0) {
                        if (Log.isLoggable("DfltImageHeaderParser", 3)) {
                            Log.d("DfltImageHeaderParser", "Negative tiff component count");
                        }
                    }
                    else {
                        if (Log.isLoggable("DfltImageHeaderParser", 3)) {
                            final StringBuilder sb2 = new StringBuilder();
                            sb2.append("Got tagIndex=");
                            sb2.append(i);
                            sb2.append(" tagType=");
                            sb2.append(int18);
                            sb2.append(" formatCode=");
                            sb2.append(int19);
                            sb2.append(" componentCount=");
                            sb2.append(int20);
                            Log.d("DfltImageHeaderParser", sb2.toString());
                        }
                        final int n2 = int20 + DefaultImageHeaderParser.BYTES_PER_FORMAT[int19];
                        if (n2 > 4) {
                            if (Log.isLoggable("DfltImageHeaderParser", 3)) {
                                final StringBuilder sb3 = new StringBuilder();
                                sb3.append("Got byte count > 4, not orientation, continuing, formatCode=");
                                sb3.append(int19);
                                Log.d("DfltImageHeaderParser", sb3.toString());
                            }
                        }
                        else {
                            final int j = calcTagOffset + 8;
                            if (j >= 0 && j <= randomAccessReader.length()) {
                                if (n2 >= 0 && n2 + j <= randomAccessReader.length()) {
                                    return randomAccessReader.getInt16(j);
                                }
                                if (Log.isLoggable("DfltImageHeaderParser", 3)) {
                                    final StringBuilder sb4 = new StringBuilder();
                                    sb4.append("Illegal number of bytes for TI tag data tagType=");
                                    sb4.append(int18);
                                    Log.d("DfltImageHeaderParser", sb4.toString());
                                }
                            }
                            else if (Log.isLoggable("DfltImageHeaderParser", 3)) {
                                final StringBuilder sb5 = new StringBuilder();
                                sb5.append("Illegal tagValueOffset=");
                                sb5.append(j);
                                sb5.append(" tagType=");
                                sb5.append(int18);
                                Log.d("DfltImageHeaderParser", sb5.toString());
                            }
                        }
                    }
                }
                else if (Log.isLoggable("DfltImageHeaderParser", 3)) {
                    final StringBuilder sb6 = new StringBuilder();
                    sb6.append("Got invalid format code = ");
                    sb6.append(int19);
                    Log.d("DfltImageHeaderParser", sb6.toString());
                }
            }
        }
        return -1;
    }
    
    private int parseExifSegment(final Reader reader, final byte[] array, final int i) throws IOException {
        final int read = reader.read(array, i);
        if (read != i) {
            if (Log.isLoggable("DfltImageHeaderParser", 3)) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Unable to read exif segment data, length: ");
                sb.append(i);
                sb.append(", actually read: ");
                sb.append(read);
                Log.d("DfltImageHeaderParser", sb.toString());
            }
            return -1;
        }
        if (this.hasJpegExifPreamble(array, i)) {
            return parseExifSegment(new RandomAccessReader(array, i));
        }
        if (Log.isLoggable("DfltImageHeaderParser", 3)) {
            Log.d("DfltImageHeaderParser", "Missing jpeg exif preamble");
        }
        return -1;
    }
    
    private ImageType sniffAvif(final Reader reader, int n) throws IOException {
        if ((reader.getUInt16() << 16 | reader.getUInt16()) != 0x66747970) {
            return ImageType.UNKNOWN;
        }
        final int n2 = reader.getUInt16() << 16 | reader.getUInt16();
        if (n2 == 1635150195) {
            return ImageType.ANIMATED_AVIF;
        }
        int n3 = 0;
        int n4;
        if (n2 == 1635150182) {
            n4 = 1;
        }
        else {
            n4 = 0;
        }
        reader.skip(4L);
        n -= 16;
        int n5 = n4;
        if (n % 4 == 0) {
            while (true) {
                n5 = n4;
                if (n3 >= 5) {
                    break;
                }
                n5 = n4;
                if (n <= 0) {
                    break;
                }
                final int n6 = reader.getUInt16() << 16 | reader.getUInt16();
                if (n6 == 1635150195) {
                    return ImageType.ANIMATED_AVIF;
                }
                if (n6 == 1635150182) {
                    n4 = 1;
                }
                ++n3;
                n -= 4;
            }
        }
        ImageType imageType;
        if (n5 != 0) {
            imageType = ImageType.AVIF;
        }
        else {
            imageType = ImageType.UNKNOWN;
        }
        return imageType;
    }
    
    @Override
    public int getOrientation(final InputStream inputStream, final ArrayPool arrayPool) throws IOException {
        return this.getOrientation((Reader)new StreamReader(Preconditions.checkNotNull(inputStream)), Preconditions.checkNotNull(arrayPool));
    }
    
    @Override
    public int getOrientation(final ByteBuffer byteBuffer, final ArrayPool arrayPool) throws IOException {
        return this.getOrientation((Reader)new ByteBufferReader(Preconditions.checkNotNull(byteBuffer)), Preconditions.checkNotNull(arrayPool));
    }
    
    @Override
    public ImageType getType(final InputStream inputStream) throws IOException {
        return this.getType((Reader)new StreamReader(Preconditions.checkNotNull(inputStream)));
    }
    
    @Override
    public ImageType getType(final ByteBuffer byteBuffer) throws IOException {
        return this.getType((Reader)new ByteBufferReader(Preconditions.checkNotNull(byteBuffer)));
    }
    
    private static final class ByteBufferReader implements Reader
    {
        private final ByteBuffer byteBuffer;
        
        ByteBufferReader(final ByteBuffer byteBuffer) {
            (this.byteBuffer = byteBuffer).order(ByteOrder.BIG_ENDIAN);
        }
        
        @Override
        public int getUInt16() throws EndOfFileException {
            return this.getUInt8() << 8 | this.getUInt8();
        }
        
        @Override
        public short getUInt8() throws EndOfFileException {
            if (this.byteBuffer.remaining() >= 1) {
                return (short)(this.byteBuffer.get() & 0xFF);
            }
            throw new EndOfFileException();
        }
        
        @Override
        public int read(final byte[] dst, int min) {
            min = Math.min(min, this.byteBuffer.remaining());
            if (min == 0) {
                return -1;
            }
            this.byteBuffer.get(dst, 0, min);
            return min;
        }
        
        @Override
        public long skip(final long b) {
            final int n = (int)Math.min(this.byteBuffer.remaining(), b);
            final ByteBuffer byteBuffer = this.byteBuffer;
            byteBuffer.position(byteBuffer.position() + n);
            return n;
        }
    }
    
    private static final class RandomAccessReader
    {
        private final ByteBuffer data;
        
        RandomAccessReader(final byte[] array, final int n) {
            this.data = (ByteBuffer)ByteBuffer.wrap(array).order(ByteOrder.BIG_ENDIAN).limit(n);
        }
        
        private boolean isAvailable(final int n, final int n2) {
            return this.data.remaining() - n >= n2;
        }
        
        short getInt16(final int n) {
            short short1;
            if (this.isAvailable(n, 2)) {
                short1 = this.data.getShort(n);
            }
            else {
                short1 = -1;
            }
            return short1;
        }
        
        int getInt32(int int1) {
            if (this.isAvailable(int1, 4)) {
                int1 = this.data.getInt(int1);
            }
            else {
                int1 = -1;
            }
            return int1;
        }
        
        int length() {
            return this.data.remaining();
        }
        
        void order(final ByteOrder bo) {
            this.data.order(bo);
        }
    }
    
    private interface Reader
    {
        int getUInt16() throws IOException;
        
        short getUInt8() throws IOException;
        
        int read(final byte[] p0, final int p1) throws IOException;
        
        long skip(final long p0) throws IOException;
        
        public static final class EndOfFileException extends IOException
        {
            private static final long serialVersionUID = 1L;
            
            EndOfFileException() {
                super("Unexpectedly reached end of a file");
            }
        }
    }
    
    private static final class StreamReader implements Reader
    {
        private final InputStream is;
        
        StreamReader(final InputStream is) {
            this.is = is;
        }
        
        @Override
        public int getUInt16() throws IOException {
            return this.getUInt8() << 8 | this.getUInt8();
        }
        
        @Override
        public short getUInt8() throws IOException {
            final int read = this.is.read();
            if (read != -1) {
                return (short)read;
            }
            throw new EndOfFileException();
        }
        
        @Override
        public int read(final byte[] b, final int n) throws IOException {
            int off = 0;
            int read = 0;
            int n2;
            while (true) {
                n2 = read;
                if (off >= n) {
                    break;
                }
                read = this.is.read(b, off, n - off);
                if ((n2 = read) == -1) {
                    break;
                }
                off += read;
            }
            if (off == 0 && n2 == -1) {
                throw new EndOfFileException();
            }
            return off;
        }
        
        @Override
        public long skip(final long n) throws IOException {
            if (n < 0L) {
                return 0L;
            }
            long n2;
            long skip;
            for (n2 = n; n2 > 0L; n2 -= skip) {
                skip = this.is.skip(n2);
                if (skip <= 0L) {
                    if (this.is.read() == -1) {
                        break;
                    }
                    skip = 1L;
                }
            }
            return n - n2;
        }
    }
}

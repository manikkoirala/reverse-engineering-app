// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide.load.resource.bitmap;

import android.graphics.Bitmap$Config;
import android.graphics.BitmapFactory$Options;
import com.bumptech.glide.util.Util;
import java.util.Iterator;
import android.os.Build;
import java.util.Arrays;
import android.util.Log;
import android.os.Build$VERSION;
import java.util.concurrent.atomic.AtomicBoolean;
import java.io.File;

public final class HardwareConfigState
{
    public static final boolean BLOCK_HARDWARE_BITMAPS_WHEN_GL_CONTEXT_MIGHT_NOT_BE_INITIALIZED;
    private static final File FD_SIZE_LIST;
    public static final boolean HARDWARE_BITMAPS_SUPPORTED;
    private static final int MAXIMUM_FDS_FOR_HARDWARE_CONFIGS_O = 700;
    private static final int MAXIMUM_FDS_FOR_HARDWARE_CONFIGS_P = 20000;
    private static final int MINIMUM_DECODES_BETWEEN_FD_CHECKS = 50;
    static final int MIN_HARDWARE_DIMENSION_O = 128;
    private static final int MIN_HARDWARE_DIMENSION_P = 0;
    public static final int NO_MAX_FD_COUNT = -1;
    private static final String TAG = "HardwareConfig";
    private static volatile HardwareConfigState instance;
    private static volatile int manualOverrideMaxFdCount;
    private int decodesSinceLastFdCheck;
    private boolean isFdSizeBelowHardwareLimit;
    private final AtomicBoolean isHardwareConfigAllowedByAppState;
    private final boolean isHardwareConfigAllowedByDeviceModel;
    private final int minHardwareDimension;
    private final int sdkBasedMaxFdCount;
    
    static {
        final int sdk_INT = Build$VERSION.SDK_INT;
        final boolean b = true;
        BLOCK_HARDWARE_BITMAPS_WHEN_GL_CONTEXT_MIGHT_NOT_BE_INITIALIZED = (sdk_INT < 29);
        HARDWARE_BITMAPS_SUPPORTED = (Build$VERSION.SDK_INT >= 26 && b);
        FD_SIZE_LIST = new File("/proc/self/fd");
        HardwareConfigState.manualOverrideMaxFdCount = -1;
    }
    
    HardwareConfigState() {
        this.isFdSizeBelowHardwareLimit = true;
        this.isHardwareConfigAllowedByAppState = new AtomicBoolean(false);
        this.isHardwareConfigAllowedByDeviceModel = isHardwareConfigAllowedByDeviceModel();
        if (Build$VERSION.SDK_INT >= 28) {
            this.sdkBasedMaxFdCount = 20000;
            this.minHardwareDimension = 0;
        }
        else {
            this.sdkBasedMaxFdCount = 700;
            this.minHardwareDimension = 128;
        }
    }
    
    private boolean areHardwareBitmapsBlockedByAppState() {
        return HardwareConfigState.BLOCK_HARDWARE_BITMAPS_WHEN_GL_CONTEXT_MIGHT_NOT_BE_INITIALIZED && !this.isHardwareConfigAllowedByAppState.get();
    }
    
    public static HardwareConfigState getInstance() {
        if (HardwareConfigState.instance == null) {
            synchronized (HardwareConfigState.class) {
                if (HardwareConfigState.instance == null) {
                    HardwareConfigState.instance = new HardwareConfigState();
                }
            }
        }
        return HardwareConfigState.instance;
    }
    
    private int getMaxFdCount() {
        int n;
        if (HardwareConfigState.manualOverrideMaxFdCount != -1) {
            n = HardwareConfigState.manualOverrideMaxFdCount;
        }
        else {
            n = this.sdkBasedMaxFdCount;
        }
        return n;
    }
    
    private boolean isFdSizeBelowHardwareLimit() {
        synchronized (this) {
            int decodesSinceLastFdCheck = this.decodesSinceLastFdCheck;
            boolean isFdSizeBelowHardwareLimit = true;
            ++decodesSinceLastFdCheck;
            this.decodesSinceLastFdCheck = decodesSinceLastFdCheck;
            if (decodesSinceLastFdCheck >= 50) {
                this.decodesSinceLastFdCheck = 0;
                final int length = HardwareConfigState.FD_SIZE_LIST.list().length;
                final long lng = this.getMaxFdCount();
                if (length >= lng) {
                    isFdSizeBelowHardwareLimit = false;
                }
                this.isFdSizeBelowHardwareLimit = isFdSizeBelowHardwareLimit;
                if (!isFdSizeBelowHardwareLimit && Log.isLoggable("Downsampler", 5)) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Excluding HARDWARE bitmap config because we're over the file descriptor limit, file descriptors ");
                    sb.append(length);
                    sb.append(", limit ");
                    sb.append(lng);
                    Log.w("Downsampler", sb.toString());
                }
            }
            return this.isFdSizeBelowHardwareLimit;
        }
    }
    
    private static boolean isHardwareConfigAllowedByDeviceModel() {
        return !isHardwareConfigDisallowedByB112551574() && !isHardwareConfigDisallowedByB147430447();
    }
    
    private static boolean isHardwareConfigDisallowedByB112551574() {
        if (Build$VERSION.SDK_INT != 26) {
            return false;
        }
        final Iterator<String> iterator = Arrays.asList("SC-04J", "SM-N935", "SM-J720", "SM-G570F", "SM-G570M", "SM-G960", "SM-G965", "SM-G935", "SM-G930", "SM-A520", "SM-A720F", "moto e5", "moto e5 play", "moto e5 plus", "moto e5 cruise", "moto g(6) forge", "moto g(6) play").iterator();
        while (iterator.hasNext()) {
            if (Build.MODEL.startsWith(iterator.next())) {
                return true;
            }
        }
        return false;
    }
    
    private static boolean isHardwareConfigDisallowedByB147430447() {
        return Build$VERSION.SDK_INT == 27 && Arrays.asList("LG-M250", "LG-M320", "LG-Q710AL", "LG-Q710PL", "LGM-K121K", "LGM-K121L", "LGM-K121S", "LGM-X320K", "LGM-X320L", "LGM-X320S", "LGM-X401L", "LGM-X401S", "LM-Q610.FG", "LM-Q610.FGN", "LM-Q617.FG", "LM-Q617.FGN", "LM-Q710.FG", "LM-Q710.FGN", "LM-X220PM", "LM-X220QMA", "LM-X410PM").contains(Build.MODEL);
    }
    
    public boolean areHardwareBitmapsBlocked() {
        Util.assertMainThread();
        return this.isHardwareConfigAllowedByAppState.get() ^ true;
    }
    
    public void blockHardwareBitmaps() {
        Util.assertMainThread();
        this.isHardwareConfigAllowedByAppState.set(false);
    }
    
    public boolean isHardwareConfigAllowed(final int n, final int n2, final boolean b, final boolean b2) {
        if (!b) {
            if (Log.isLoggable("HardwareConfig", 2)) {
                Log.v("HardwareConfig", "Hardware config disallowed by caller");
            }
            return false;
        }
        if (!this.isHardwareConfigAllowedByDeviceModel) {
            if (Log.isLoggable("HardwareConfig", 2)) {
                Log.v("HardwareConfig", "Hardware config disallowed by device model");
            }
            return false;
        }
        if (!HardwareConfigState.HARDWARE_BITMAPS_SUPPORTED) {
            if (Log.isLoggable("HardwareConfig", 2)) {
                Log.v("HardwareConfig", "Hardware config disallowed by sdk");
            }
            return false;
        }
        if (this.areHardwareBitmapsBlockedByAppState()) {
            if (Log.isLoggable("HardwareConfig", 2)) {
                Log.v("HardwareConfig", "Hardware config disallowed by app state");
            }
            return false;
        }
        if (b2) {
            if (Log.isLoggable("HardwareConfig", 2)) {
                Log.v("HardwareConfig", "Hardware config disallowed because exif orientation is required");
            }
            return false;
        }
        final int minHardwareDimension = this.minHardwareDimension;
        if (n < minHardwareDimension) {
            if (Log.isLoggable("HardwareConfig", 2)) {
                Log.v("HardwareConfig", "Hardware config disallowed because width is too small");
            }
            return false;
        }
        if (n2 < minHardwareDimension) {
            if (Log.isLoggable("HardwareConfig", 2)) {
                Log.v("HardwareConfig", "Hardware config disallowed because height is too small");
            }
            return false;
        }
        if (!this.isFdSizeBelowHardwareLimit()) {
            if (Log.isLoggable("HardwareConfig", 2)) {
                Log.v("HardwareConfig", "Hardware config disallowed because there are insufficient FDs");
            }
            return false;
        }
        return true;
    }
    
    boolean setHardwareConfigIfAllowed(final int n, final int n2, final BitmapFactory$Options bitmapFactory$Options, final boolean b, final boolean b2) {
        final boolean hardwareConfigAllowed = this.isHardwareConfigAllowed(n, n2, b, b2);
        if (hardwareConfigAllowed) {
            bitmapFactory$Options.inPreferredConfig = Bitmap$Config.HARDWARE;
            bitmapFactory$Options.inMutable = false;
        }
        return hardwareConfigAllowed;
    }
    
    public void unblockHardwareBitmaps() {
        Util.assertMainThread();
        this.isHardwareConfigAllowedByAppState.set(true);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide.load.resource.bitmap;

import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPool;
import com.bumptech.glide.Glide;
import android.content.Context;
import com.bumptech.glide.util.Preconditions;
import android.content.res.Resources;
import android.graphics.Bitmap;
import com.bumptech.glide.load.engine.Initializable;
import android.graphics.drawable.BitmapDrawable;
import com.bumptech.glide.load.engine.Resource;

public final class LazyBitmapDrawableResource implements Resource<BitmapDrawable>, Initializable
{
    private final Resource<Bitmap> bitmapResource;
    private final Resources resources;
    
    private LazyBitmapDrawableResource(final Resources resources, final Resource<Bitmap> resource) {
        this.resources = Preconditions.checkNotNull(resources);
        this.bitmapResource = Preconditions.checkNotNull(resource);
    }
    
    public static Resource<BitmapDrawable> obtain(final Resources resources, final Resource<Bitmap> resource) {
        if (resource == null) {
            return null;
        }
        return new LazyBitmapDrawableResource(resources, resource);
    }
    
    @Deprecated
    public static LazyBitmapDrawableResource obtain(final Context context, final Bitmap bitmap) {
        return (LazyBitmapDrawableResource)obtain(context.getResources(), BitmapResource.obtain(bitmap, Glide.get(context).getBitmapPool()));
    }
    
    @Deprecated
    public static LazyBitmapDrawableResource obtain(final Resources resources, final BitmapPool bitmapPool, final Bitmap bitmap) {
        return (LazyBitmapDrawableResource)obtain(resources, BitmapResource.obtain(bitmap, bitmapPool));
    }
    
    @Override
    public BitmapDrawable get() {
        return new BitmapDrawable(this.resources, (Bitmap)this.bitmapResource.get());
    }
    
    @Override
    public Class<BitmapDrawable> getResourceClass() {
        return BitmapDrawable.class;
    }
    
    @Override
    public int getSize() {
        return this.bitmapResource.getSize();
    }
    
    @Override
    public void initialize() {
        final Resource<Bitmap> bitmapResource = this.bitmapResource;
        if (bitmapResource instanceof Initializable) {
            ((Initializable)bitmapResource).initialize();
        }
    }
    
    @Override
    public void recycle() {
        this.bitmapResource.recycle();
    }
}

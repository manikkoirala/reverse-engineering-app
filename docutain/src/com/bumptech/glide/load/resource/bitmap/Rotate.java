// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide.load.resource.bitmap;

import java.nio.ByteBuffer;
import java.security.MessageDigest;
import android.graphics.Bitmap;
import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPool;
import com.bumptech.glide.util.Util;

public class Rotate extends BitmapTransformation
{
    private static final String ID = "com.bumptech.glide.load.resource.bitmap.Rotate";
    private static final byte[] ID_BYTES;
    private final int degreesToRotate;
    
    static {
        ID_BYTES = "com.bumptech.glide.load.resource.bitmap.Rotate".getBytes(Rotate.CHARSET);
    }
    
    public Rotate(final int degreesToRotate) {
        this.degreesToRotate = degreesToRotate;
    }
    
    @Override
    public boolean equals(final Object o) {
        final boolean b = o instanceof Rotate;
        boolean b2 = false;
        if (b) {
            final Rotate rotate = (Rotate)o;
            b2 = b2;
            if (this.degreesToRotate == rotate.degreesToRotate) {
                b2 = true;
            }
        }
        return b2;
    }
    
    @Override
    public int hashCode() {
        return Util.hashCode(-950519196, Util.hashCode(this.degreesToRotate));
    }
    
    @Override
    protected Bitmap transform(final BitmapPool bitmapPool, final Bitmap bitmap, final int n, final int n2) {
        return TransformationUtils.rotateImage(bitmap, this.degreesToRotate);
    }
    
    @Override
    public void updateDiskCacheKey(final MessageDigest messageDigest) {
        messageDigest.update(Rotate.ID_BYTES);
        messageDigest.update(ByteBuffer.allocate(4).putInt(this.degreesToRotate).array());
    }
}

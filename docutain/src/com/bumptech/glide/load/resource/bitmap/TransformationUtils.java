// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide.load.resource.bitmap;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import android.graphics.Shader;
import android.graphics.BitmapShader;
import android.graphics.Shader$TileMode;
import com.bumptech.glide.util.Preconditions;
import android.graphics.Path$Direction;
import android.graphics.Path;
import android.os.Build$VERSION;
import android.graphics.Bitmap$Config;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.Log;
import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPool;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Bitmap;
import android.graphics.Xfermode;
import android.graphics.PorterDuffXfermode;
import android.graphics.PorterDuff$Mode;
import java.util.concurrent.locks.ReentrantLock;
import android.os.Build;
import java.util.Collection;
import java.util.HashSet;
import java.util.Arrays;
import java.util.Set;
import android.graphics.Paint;
import java.util.concurrent.locks.Lock;

public final class TransformationUtils
{
    private static final Lock BITMAP_DRAWABLE_LOCK;
    private static final Paint CIRCLE_CROP_BITMAP_PAINT;
    private static final int CIRCLE_CROP_PAINT_FLAGS = 7;
    private static final Paint CIRCLE_CROP_SHAPE_PAINT;
    private static final Paint DEFAULT_PAINT;
    private static final Set<String> MODELS_REQUIRING_BITMAP_LOCK;
    public static final int PAINT_FLAGS = 6;
    private static final String TAG = "TransformationUtils";
    
    static {
        DEFAULT_PAINT = new Paint(6);
        CIRCLE_CROP_SHAPE_PAINT = new Paint(7);
        Lock bitmap_DRAWABLE_LOCK;
        if ((MODELS_REQUIRING_BITMAP_LOCK = new HashSet<String>(Arrays.asList("XT1085", "XT1092", "XT1093", "XT1094", "XT1095", "XT1096", "XT1097", "XT1098", "XT1031", "XT1028", "XT937C", "XT1032", "XT1008", "XT1033", "XT1035", "XT1034", "XT939G", "XT1039", "XT1040", "XT1042", "XT1045", "XT1063", "XT1064", "XT1068", "XT1069", "XT1072", "XT1077", "XT1078", "XT1079"))).contains(Build.MODEL)) {
            bitmap_DRAWABLE_LOCK = new ReentrantLock();
        }
        else {
            bitmap_DRAWABLE_LOCK = new NoLock();
        }
        BITMAP_DRAWABLE_LOCK = bitmap_DRAWABLE_LOCK;
        (CIRCLE_CROP_BITMAP_PAINT = new Paint(7)).setXfermode((Xfermode)new PorterDuffXfermode(PorterDuff$Mode.SRC_IN));
    }
    
    private TransformationUtils() {
    }
    
    private static void applyMatrix(final Bitmap bitmap, final Bitmap bitmap2, final Matrix matrix) {
        final Lock bitmap_DRAWABLE_LOCK = TransformationUtils.BITMAP_DRAWABLE_LOCK;
        bitmap_DRAWABLE_LOCK.lock();
        try {
            final Canvas canvas = new Canvas(bitmap2);
            canvas.drawBitmap(bitmap, matrix, TransformationUtils.DEFAULT_PAINT);
            clear(canvas);
            bitmap_DRAWABLE_LOCK.unlock();
        }
        finally {
            TransformationUtils.BITMAP_DRAWABLE_LOCK.unlock();
        }
    }
    
    public static Bitmap centerCrop(final BitmapPool bitmapPool, final Bitmap bitmap, final int n, final int n2) {
        if (bitmap.getWidth() == n && bitmap.getHeight() == n2) {
            return bitmap;
        }
        final Matrix matrix = new Matrix();
        final int width = bitmap.getWidth();
        final int height = bitmap.getHeight();
        float n3 = 0.0f;
        float n4;
        float n5;
        if (width * n2 > height * n) {
            n4 = n2 / (float)bitmap.getHeight();
            n3 = (n - bitmap.getWidth() * n4) * 0.5f;
            n5 = 0.0f;
        }
        else {
            n4 = n / (float)bitmap.getWidth();
            n5 = (n2 - bitmap.getHeight() * n4) * 0.5f;
        }
        matrix.setScale(n4, n4);
        matrix.postTranslate((float)(int)(n3 + 0.5f), (float)(int)(n5 + 0.5f));
        final Bitmap value = bitmapPool.get(n, n2, getNonNullConfig(bitmap));
        setAlpha(bitmap, value);
        applyMatrix(bitmap, value, matrix);
        return value;
    }
    
    public static Bitmap centerInside(final BitmapPool bitmapPool, final Bitmap bitmap, final int n, final int n2) {
        if (bitmap.getWidth() <= n && bitmap.getHeight() <= n2) {
            if (Log.isLoggable("TransformationUtils", 2)) {
                Log.v("TransformationUtils", "requested target size larger or equal to input, returning input");
            }
            return bitmap;
        }
        if (Log.isLoggable("TransformationUtils", 2)) {
            Log.v("TransformationUtils", "requested target size too big for input, fit centering instead");
        }
        return fitCenter(bitmapPool, bitmap, n, n2);
    }
    
    public static Bitmap circleCrop(final BitmapPool bitmapPool, final Bitmap obj, int width, int min) {
        min = Math.min(width, min);
        final float n = (float)min;
        final float n2 = n / 2.0f;
        width = obj.getWidth();
        final int height = obj.getHeight();
        final float n3 = (float)width;
        final float a = n / n3;
        final float n4 = (float)height;
        final float max = Math.max(a, n / n4);
        final float n5 = n3 * max;
        final float n6 = max * n4;
        final float n7 = (n - n5) / 2.0f;
        final float n8 = (n - n6) / 2.0f;
        final RectF rectF = new RectF(n7, n8, n5 + n7, n6 + n8);
        final Bitmap alphaSafeBitmap = getAlphaSafeBitmap(bitmapPool, obj);
        final Bitmap value = bitmapPool.get(min, min, getAlphaSafeConfig(obj));
        value.setHasAlpha(true);
        final Lock bitmap_DRAWABLE_LOCK = TransformationUtils.BITMAP_DRAWABLE_LOCK;
        bitmap_DRAWABLE_LOCK.lock();
        try {
            final Canvas canvas = new Canvas(value);
            canvas.drawCircle(n2, n2, n2, TransformationUtils.CIRCLE_CROP_SHAPE_PAINT);
            canvas.drawBitmap(alphaSafeBitmap, (Rect)null, rectF, TransformationUtils.CIRCLE_CROP_BITMAP_PAINT);
            clear(canvas);
            bitmap_DRAWABLE_LOCK.unlock();
            if (!alphaSafeBitmap.equals(obj)) {
                bitmapPool.put(alphaSafeBitmap);
            }
            return value;
        }
        finally {
            TransformationUtils.BITMAP_DRAWABLE_LOCK.unlock();
        }
    }
    
    private static void clear(final Canvas canvas) {
        canvas.setBitmap((Bitmap)null);
    }
    
    public static Bitmap fitCenter(final BitmapPool bitmapPool, final Bitmap bitmap, final int i, final int j) {
        if (bitmap.getWidth() == i && bitmap.getHeight() == j) {
            if (Log.isLoggable("TransformationUtils", 2)) {
                Log.v("TransformationUtils", "requested target size matches input, returning input");
            }
            return bitmap;
        }
        final float min = Math.min(i / (float)bitmap.getWidth(), j / (float)bitmap.getHeight());
        final int round = Math.round(bitmap.getWidth() * min);
        final int round2 = Math.round(bitmap.getHeight() * min);
        if (bitmap.getWidth() == round && bitmap.getHeight() == round2) {
            if (Log.isLoggable("TransformationUtils", 2)) {
                Log.v("TransformationUtils", "adjusted target size matches input, returning input");
            }
            return bitmap;
        }
        final Bitmap value = bitmapPool.get((int)(bitmap.getWidth() * min), (int)(bitmap.getHeight() * min), getNonNullConfig(bitmap));
        setAlpha(bitmap, value);
        if (Log.isLoggable("TransformationUtils", 2)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("request: ");
            sb.append(i);
            sb.append("x");
            sb.append(j);
            Log.v("TransformationUtils", sb.toString());
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("toFit:   ");
            sb2.append(bitmap.getWidth());
            sb2.append("x");
            sb2.append(bitmap.getHeight());
            Log.v("TransformationUtils", sb2.toString());
            final StringBuilder sb3 = new StringBuilder();
            sb3.append("toReuse: ");
            sb3.append(value.getWidth());
            sb3.append("x");
            sb3.append(value.getHeight());
            Log.v("TransformationUtils", sb3.toString());
            final StringBuilder sb4 = new StringBuilder();
            sb4.append("minPct:   ");
            sb4.append(min);
            Log.v("TransformationUtils", sb4.toString());
        }
        final Matrix matrix = new Matrix();
        matrix.setScale(min, min);
        applyMatrix(bitmap, value, matrix);
        return value;
    }
    
    private static Bitmap getAlphaSafeBitmap(final BitmapPool bitmapPool, final Bitmap bitmap) {
        final Bitmap$Config alphaSafeConfig = getAlphaSafeConfig(bitmap);
        if (alphaSafeConfig.equals((Object)bitmap.getConfig())) {
            return bitmap;
        }
        final Bitmap value = bitmapPool.get(bitmap.getWidth(), bitmap.getHeight(), alphaSafeConfig);
        new Canvas(value).drawBitmap(bitmap, 0.0f, 0.0f, (Paint)null);
        return value;
    }
    
    private static Bitmap$Config getAlphaSafeConfig(final Bitmap bitmap) {
        if (Build$VERSION.SDK_INT >= 26 && Bitmap$Config.RGBA_F16.equals((Object)bitmap.getConfig())) {
            return Bitmap$Config.RGBA_F16;
        }
        return Bitmap$Config.ARGB_8888;
    }
    
    public static Lock getBitmapDrawableLock() {
        return TransformationUtils.BITMAP_DRAWABLE_LOCK;
    }
    
    public static int getExifOrientationDegrees(int n) {
        switch (n) {
            default: {
                n = 0;
                break;
            }
            case 7:
            case 8: {
                n = 270;
                break;
            }
            case 5:
            case 6: {
                n = 90;
                break;
            }
            case 3:
            case 4: {
                n = 180;
                break;
            }
        }
        return n;
    }
    
    private static Bitmap$Config getNonNullConfig(final Bitmap bitmap) {
        Bitmap$Config bitmap$Config;
        if (bitmap.getConfig() != null) {
            bitmap$Config = bitmap.getConfig();
        }
        else {
            bitmap$Config = Bitmap$Config.ARGB_8888;
        }
        return bitmap$Config;
    }
    
    static void initializeMatrixForRotation(final int n, final Matrix matrix) {
        switch (n) {
            case 8: {
                matrix.setRotate(-90.0f);
                break;
            }
            case 7: {
                matrix.setRotate(-90.0f);
                matrix.postScale(-1.0f, 1.0f);
                break;
            }
            case 6: {
                matrix.setRotate(90.0f);
                break;
            }
            case 5: {
                matrix.setRotate(90.0f);
                matrix.postScale(-1.0f, 1.0f);
                break;
            }
            case 4: {
                matrix.setRotate(180.0f);
                matrix.postScale(-1.0f, 1.0f);
                break;
            }
            case 3: {
                matrix.setRotate(180.0f);
                break;
            }
            case 2: {
                matrix.setScale(-1.0f, 1.0f);
                break;
            }
        }
    }
    
    public static boolean isExifOrientationRequired(final int n) {
        switch (n) {
            default: {
                return false;
            }
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8: {
                return true;
            }
        }
    }
    
    public static Bitmap rotateImage(final Bitmap bitmap, final int n) {
        Bitmap bitmap2 = bitmap;
        if (n != 0) {
            try {
                final Matrix matrix = new Matrix();
                matrix.setRotate((float)n);
                bitmap2 = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
            }
            catch (final Exception ex) {
                bitmap2 = bitmap;
                if (Log.isLoggable("TransformationUtils", 6)) {
                    Log.e("TransformationUtils", "Exception when trying to orient image", (Throwable)ex);
                    bitmap2 = bitmap;
                }
            }
        }
        return bitmap2;
    }
    
    public static Bitmap rotateImageExif(final BitmapPool bitmapPool, final Bitmap bitmap, final int n) {
        if (!isExifOrientationRequired(n)) {
            return bitmap;
        }
        final Matrix matrix = new Matrix();
        initializeMatrixForRotation(n, matrix);
        final RectF rectF = new RectF(0.0f, 0.0f, (float)bitmap.getWidth(), (float)bitmap.getHeight());
        matrix.mapRect(rectF);
        final Bitmap value = bitmapPool.get(Math.round(rectF.width()), Math.round(rectF.height()), getNonNullConfig(bitmap));
        matrix.postTranslate(-rectF.left, -rectF.top);
        value.setHasAlpha(bitmap.hasAlpha());
        applyMatrix(bitmap, value, matrix);
        return value;
    }
    
    public static Bitmap roundedCorners(final BitmapPool bitmapPool, final Bitmap bitmap, final float n, final float n2, final float n3, final float n4) {
        return roundedCorners(bitmapPool, bitmap, (DrawRoundedCornerFn)new DrawRoundedCornerFn(n, n2, n3, n4) {
            final float val$bottomLeft;
            final float val$bottomRight;
            final float val$topLeft;
            final float val$topRight;
            
            @Override
            public void drawRoundedCorners(final Canvas canvas, final Paint paint, final RectF rectF) {
                final Path path = new Path();
                final float val$topLeft = this.val$topLeft;
                final float val$topRight = this.val$topRight;
                final float val$bottomRight = this.val$bottomRight;
                final float val$bottomLeft = this.val$bottomLeft;
                path.addRoundRect(rectF, new float[] { val$topLeft, val$topLeft, val$topRight, val$topRight, val$bottomRight, val$bottomRight, val$bottomLeft, val$bottomLeft }, Path$Direction.CW);
                canvas.drawPath(path, paint);
            }
        });
    }
    
    public static Bitmap roundedCorners(final BitmapPool bitmapPool, final Bitmap bitmap, final int n) {
        Preconditions.checkArgument(n > 0, "roundingRadius must be greater than 0.");
        return roundedCorners(bitmapPool, bitmap, (DrawRoundedCornerFn)new DrawRoundedCornerFn(n) {
            final int val$roundingRadius;
            
            @Override
            public void drawRoundedCorners(final Canvas canvas, final Paint paint, final RectF rectF) {
                final int val$roundingRadius = this.val$roundingRadius;
                canvas.drawRoundRect(rectF, (float)val$roundingRadius, (float)val$roundingRadius, paint);
            }
        });
    }
    
    @Deprecated
    public static Bitmap roundedCorners(final BitmapPool bitmapPool, final Bitmap bitmap, final int n, final int n2, final int n3) {
        return roundedCorners(bitmapPool, bitmap, n3);
    }
    
    private static Bitmap roundedCorners(final BitmapPool bitmapPool, final Bitmap obj, final DrawRoundedCornerFn drawRoundedCornerFn) {
        final Bitmap$Config alphaSafeConfig = getAlphaSafeConfig(obj);
        final Bitmap alphaSafeBitmap = getAlphaSafeBitmap(bitmapPool, obj);
        final Bitmap value = bitmapPool.get(alphaSafeBitmap.getWidth(), alphaSafeBitmap.getHeight(), alphaSafeConfig);
        value.setHasAlpha(true);
        final BitmapShader shader = new BitmapShader(alphaSafeBitmap, Shader$TileMode.CLAMP, Shader$TileMode.CLAMP);
        final Paint paint = new Paint();
        paint.setAntiAlias(true);
        paint.setShader((Shader)shader);
        final RectF rectF = new RectF(0.0f, 0.0f, (float)value.getWidth(), (float)value.getHeight());
        final Lock bitmap_DRAWABLE_LOCK = TransformationUtils.BITMAP_DRAWABLE_LOCK;
        bitmap_DRAWABLE_LOCK.lock();
        try {
            final Canvas canvas = new Canvas(value);
            canvas.drawColor(0, PorterDuff$Mode.CLEAR);
            drawRoundedCornerFn.drawRoundedCorners(canvas, paint, rectF);
            clear(canvas);
            bitmap_DRAWABLE_LOCK.unlock();
            if (!alphaSafeBitmap.equals(obj)) {
                bitmapPool.put(alphaSafeBitmap);
            }
            return value;
        }
        finally {
            TransformationUtils.BITMAP_DRAWABLE_LOCK.unlock();
        }
    }
    
    public static void setAlpha(final Bitmap bitmap, final Bitmap bitmap2) {
        bitmap2.setHasAlpha(bitmap.hasAlpha());
    }
    
    private interface DrawRoundedCornerFn
    {
        void drawRoundedCorners(final Canvas p0, final Paint p1, final RectF p2);
    }
    
    private static final class NoLock implements Lock
    {
        NoLock() {
        }
        
        @Override
        public void lock() {
        }
        
        @Override
        public void lockInterruptibly() throws InterruptedException {
        }
        
        @Override
        public Condition newCondition() {
            throw new UnsupportedOperationException("Should not be called");
        }
        
        @Override
        public boolean tryLock() {
            return true;
        }
        
        @Override
        public boolean tryLock(final long n, final TimeUnit timeUnit) throws InterruptedException {
            return true;
        }
        
        @Override
        public void unlock() {
        }
    }
}

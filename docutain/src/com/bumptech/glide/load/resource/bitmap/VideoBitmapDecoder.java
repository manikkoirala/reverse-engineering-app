// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide.load.resource.bitmap;

import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPool;
import com.bumptech.glide.Glide;
import android.content.Context;
import android.os.ParcelFileDescriptor;

@Deprecated
public class VideoBitmapDecoder extends VideoDecoder<ParcelFileDescriptor>
{
    public VideoBitmapDecoder(final Context context) {
        this(Glide.get(context).getBitmapPool());
    }
    
    public VideoBitmapDecoder(final BitmapPool bitmapPool) {
        super(bitmapPool, (MediaInitializer)new ParcelFileDescriptorInitializer());
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide.load.resource.bitmap;

import java.io.IOException;
import java.io.InputStream;
import com.bumptech.glide.load.engine.bitmap_recycle.ArrayPool;
import java.io.FilterInputStream;

public class RecyclableBufferedInputStream extends FilterInputStream
{
    private volatile byte[] buf;
    private final ArrayPool byteArrayPool;
    private int count;
    private int marklimit;
    private int markpos;
    private int pos;
    
    public RecyclableBufferedInputStream(final InputStream inputStream, final ArrayPool arrayPool) {
        this(inputStream, arrayPool, 65536);
    }
    
    RecyclableBufferedInputStream(final InputStream in, final ArrayPool byteArrayPool, final int n) {
        super(in);
        this.markpos = -1;
        this.byteArrayPool = byteArrayPool;
        this.buf = byteArrayPool.get(n, byte[].class);
    }
    
    private int fillbuf(final InputStream inputStream, final byte[] b) throws IOException {
        final int markpos = this.markpos;
        if (markpos != -1) {
            final int pos = this.pos;
            int marklimit = this.marklimit;
            if (pos - markpos < marklimit) {
                byte[] array;
                if (markpos == 0 && marklimit > b.length && this.count == b.length) {
                    final int n = b.length * 2;
                    if (n <= marklimit) {
                        marklimit = n;
                    }
                    array = this.byteArrayPool.get(marklimit, byte[].class);
                    System.arraycopy(b, 0, array, 0, b.length);
                    this.buf = array;
                    this.byteArrayPool.put(b);
                }
                else {
                    array = b;
                    if (markpos > 0) {
                        System.arraycopy(b, markpos, b, 0, b.length - markpos);
                        array = b;
                    }
                }
                final int n2 = this.pos - this.markpos;
                this.pos = n2;
                this.markpos = 0;
                this.count = 0;
                final int read = inputStream.read(array, n2, array.length - n2);
                int pos2 = this.pos;
                if (read > 0) {
                    pos2 += read;
                }
                this.count = pos2;
                return read;
            }
        }
        final int read2 = inputStream.read(b);
        if (read2 > 0) {
            this.markpos = -1;
            this.pos = 0;
            this.count = read2;
        }
        return read2;
    }
    
    private static IOException streamClosed() throws IOException {
        throw new IOException("BufferedInputStream is closed");
    }
    
    @Override
    public int available() throws IOException {
        synchronized (this) {
            final InputStream in = this.in;
            if (this.buf != null && in != null) {
                final int count = this.count;
                final int pos = this.pos;
                final int available = in.available();
                monitorexit(this);
                return count - pos + available;
            }
            throw streamClosed();
        }
    }
    
    @Override
    public void close() throws IOException {
        if (this.buf != null) {
            this.byteArrayPool.put(this.buf);
            this.buf = null;
        }
        final InputStream in = this.in;
        this.in = null;
        if (in != null) {
            in.close();
        }
    }
    
    public void fixMarkLimit() {
        synchronized (this) {
            this.marklimit = this.buf.length;
        }
    }
    
    @Override
    public void mark(final int b) {
        synchronized (this) {
            this.marklimit = Math.max(this.marklimit, b);
            this.markpos = this.pos;
        }
    }
    
    @Override
    public boolean markSupported() {
        return true;
    }
    
    @Override
    public int read() throws IOException {
        synchronized (this) {
            final byte[] buf = this.buf;
            final InputStream in = this.in;
            if (buf == null || in == null) {
                throw streamClosed();
            }
            if (this.pos >= this.count && this.fillbuf(in, buf) == -1) {
                return -1;
            }
            byte[] buf2;
            if ((buf2 = buf) != this.buf) {
                buf2 = this.buf;
                if (buf2 == null) {
                    throw streamClosed();
                }
            }
            final int count = this.count;
            final int pos = this.pos;
            if (count - pos > 0) {
                this.pos = pos + 1;
                final byte b = buf2[pos];
                monitorexit(this);
                return b & 0xFF;
            }
            return -1;
        }
    }
    
    @Override
    public int read(final byte[] b, int len, final int n) throws IOException {
        synchronized (this) {
            byte[] buf = this.buf;
            if (buf == null) {
                throw streamClosed();
            }
            if (n == 0) {
                return 0;
            }
            final InputStream in = this.in;
            if (in == null) {
                throw streamClosed();
            }
            final int pos = this.pos;
            final int count = this.count;
            int off;
            if (pos < count) {
                int n2;
                if (count - pos >= n) {
                    n2 = n;
                }
                else {
                    n2 = count - pos;
                }
                System.arraycopy(buf, pos, b, len, n2);
                this.pos += n2;
                if (n2 == n || in.available() == 0) {
                    return n2;
                }
                final int n3 = len + n2;
                len = n - n2;
                off = n3;
            }
            else {
                off = len;
                len = n;
            }
            while (true) {
                final int markpos = this.markpos;
                int n4 = -1;
                byte[] buf2;
                int n5;
                if (markpos == -1 && len >= buf.length) {
                    final int read = in.read(b, off, len);
                    buf2 = buf;
                    if ((n5 = read) == -1) {
                        if (len != n) {
                            n4 = n - len;
                        }
                        return n4;
                    }
                }
                else {
                    if (this.fillbuf(in, buf) == -1) {
                        if (len != n) {
                            n4 = n - len;
                        }
                        return n4;
                    }
                    if ((buf2 = buf) != this.buf) {
                        buf2 = this.buf;
                        if (buf2 == null) {
                            throw streamClosed();
                        }
                    }
                    final int count2 = this.count;
                    final int pos2 = this.pos;
                    int n6;
                    if (count2 - pos2 >= len) {
                        n6 = len;
                    }
                    else {
                        n6 = count2 - pos2;
                    }
                    System.arraycopy(buf2, pos2, b, off, n6);
                    this.pos += n6;
                    n5 = n6;
                }
                len -= n5;
                if (len == 0) {
                    return n;
                }
                if (in.available() == 0) {
                    monitorexit(this);
                    return n - len;
                }
                off += n5;
                buf = buf2;
            }
        }
    }
    
    public void release() {
        synchronized (this) {
            if (this.buf != null) {
                this.byteArrayPool.put(this.buf);
                this.buf = null;
            }
        }
    }
    
    @Override
    public void reset() throws IOException {
        synchronized (this) {
            if (this.buf == null) {
                throw new IOException("Stream is closed");
            }
            final int markpos = this.markpos;
            if (-1 != markpos) {
                this.pos = markpos;
                return;
            }
            final StringBuilder sb = new StringBuilder();
            sb.append("Mark has been invalidated, pos: ");
            sb.append(this.pos);
            sb.append(" markLimit: ");
            sb.append(this.marklimit);
            throw new InvalidMarkException(sb.toString());
        }
    }
    
    @Override
    public long skip(long skip) throws IOException {
        monitorenter(this);
        if (skip < 1L) {
            monitorexit(this);
            return 0L;
        }
        try {
            final byte[] buf = this.buf;
            if (buf == null) {
                throw streamClosed();
            }
            final InputStream in = this.in;
            if (in == null) {
                throw streamClosed();
            }
            final int count = this.count;
            final int pos = this.pos;
            if (count - pos >= skip) {
                this.pos = (int)(pos + skip);
                return skip;
            }
            final long n = count - (long)pos;
            this.pos = count;
            if (this.markpos == -1 || skip > this.marklimit) {
                skip = in.skip(skip - n);
                if (skip > 0L) {
                    this.markpos = -1;
                }
                monitorexit(this);
                return n + skip;
            }
            if (this.fillbuf(in, buf) == -1) {
                return n;
            }
            final int count2 = this.count;
            final int pos2 = this.pos;
            if (count2 - pos2 >= skip - n) {
                this.pos = (int)(pos2 + skip - n);
                return skip;
            }
            final long n2 = count2;
            skip = pos2;
            this.pos = count2;
            monitorexit(this);
            return n + n2 - skip;
        }
        finally {
            monitorexit(this);
        }
    }
    
    static class InvalidMarkException extends IOException
    {
        private static final long serialVersionUID = -4338378848813561757L;
        
        InvalidMarkException(final String message) {
            super(message);
        }
    }
}

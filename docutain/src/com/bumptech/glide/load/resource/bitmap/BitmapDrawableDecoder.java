// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide.load.resource.bitmap;

import java.io.IOException;
import com.bumptech.glide.load.engine.Resource;
import com.bumptech.glide.load.Options;
import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPool;
import com.bumptech.glide.util.Preconditions;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import com.bumptech.glide.load.ResourceDecoder;

public class BitmapDrawableDecoder<DataType> implements ResourceDecoder<DataType, BitmapDrawable>
{
    private final ResourceDecoder<DataType, Bitmap> decoder;
    private final Resources resources;
    
    public BitmapDrawableDecoder(final Context context, final ResourceDecoder<DataType, Bitmap> resourceDecoder) {
        this(context.getResources(), resourceDecoder);
    }
    
    public BitmapDrawableDecoder(final Resources resources, final ResourceDecoder<DataType, Bitmap> resourceDecoder) {
        this.resources = Preconditions.checkNotNull(resources);
        this.decoder = Preconditions.checkNotNull(resourceDecoder);
    }
    
    @Deprecated
    public BitmapDrawableDecoder(final Resources resources, final BitmapPool bitmapPool, final ResourceDecoder<DataType, Bitmap> resourceDecoder) {
        this(resources, resourceDecoder);
    }
    
    @Override
    public Resource<BitmapDrawable> decode(final DataType dataType, final int n, final int n2, final Options options) throws IOException {
        return LazyBitmapDrawableResource.obtain(this.resources, this.decoder.decode(dataType, n, n2, options));
    }
    
    @Override
    public boolean handles(final DataType dataType, final Options options) throws IOException {
        return this.decoder.handles(dataType, options);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide.load.resource.bitmap;

import com.bumptech.glide.request.transition.BitmapTransitionFactory;
import android.graphics.drawable.Drawable;
import com.bumptech.glide.request.transition.DrawableCrossFadeFactory;
import com.bumptech.glide.request.transition.TransitionFactory;
import android.graphics.Bitmap;
import com.bumptech.glide.TransitionOptions;

public final class BitmapTransitionOptions extends TransitionOptions<BitmapTransitionOptions, Bitmap>
{
    public static BitmapTransitionOptions with(final TransitionFactory<Bitmap> transitionFactory) {
        return ((TransitionOptions<BitmapTransitionOptions, Object>)new BitmapTransitionOptions()).transition(transitionFactory);
    }
    
    public static BitmapTransitionOptions withCrossFade() {
        return new BitmapTransitionOptions().crossFade();
    }
    
    public static BitmapTransitionOptions withCrossFade(final int n) {
        return new BitmapTransitionOptions().crossFade(n);
    }
    
    public static BitmapTransitionOptions withCrossFade(final DrawableCrossFadeFactory.Builder builder) {
        return new BitmapTransitionOptions().crossFade(builder);
    }
    
    public static BitmapTransitionOptions withCrossFade(final DrawableCrossFadeFactory drawableCrossFadeFactory) {
        return new BitmapTransitionOptions().crossFade(drawableCrossFadeFactory);
    }
    
    public static BitmapTransitionOptions withWrapped(final TransitionFactory<Drawable> transitionFactory) {
        return new BitmapTransitionOptions().transitionUsing(transitionFactory);
    }
    
    public BitmapTransitionOptions crossFade() {
        return this.crossFade(new DrawableCrossFadeFactory.Builder());
    }
    
    public BitmapTransitionOptions crossFade(final int n) {
        return this.crossFade(new DrawableCrossFadeFactory.Builder(n));
    }
    
    public BitmapTransitionOptions crossFade(final DrawableCrossFadeFactory.Builder builder) {
        return this.transitionUsing(builder.build());
    }
    
    public BitmapTransitionOptions crossFade(final DrawableCrossFadeFactory drawableCrossFadeFactory) {
        return this.transitionUsing(drawableCrossFadeFactory);
    }
    
    @Override
    public boolean equals(final Object o) {
        return o instanceof BitmapTransitionOptions && super.equals(o);
    }
    
    @Override
    public int hashCode() {
        return super.hashCode();
    }
    
    public BitmapTransitionOptions transitionUsing(final TransitionFactory<Drawable> transitionFactory) {
        return ((TransitionOptions<BitmapTransitionOptions, Object>)this).transition(new BitmapTransitionFactory(transitionFactory));
    }
}

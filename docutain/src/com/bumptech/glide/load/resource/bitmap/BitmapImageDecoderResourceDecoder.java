// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide.load.resource.bitmap;

import java.io.IOException;
import android.util.Log;
import android.graphics.ImageDecoder$OnHeaderDecodedListener;
import android.graphics.ImageDecoder;
import com.bumptech.glide.load.resource.DefaultOnHeaderDecodedListener;
import com.bumptech.glide.load.engine.Resource;
import com.bumptech.glide.load.Options;
import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPoolAdapter;
import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPool;
import android.graphics.Bitmap;
import android.graphics.ImageDecoder$Source;
import com.bumptech.glide.load.ResourceDecoder;

public final class BitmapImageDecoderResourceDecoder implements ResourceDecoder<ImageDecoder$Source, Bitmap>
{
    private static final String TAG = "BitmapImageDecoder";
    private final BitmapPool bitmapPool;
    
    public BitmapImageDecoderResourceDecoder() {
        this.bitmapPool = new BitmapPoolAdapter();
    }
    
    @Override
    public Resource<Bitmap> decode(final ImageDecoder$Source imageDecoder$Source, final int i, final int j, final Options options) throws IOException {
        final Bitmap decodeBitmap = ImageDecoder.decodeBitmap(imageDecoder$Source, (ImageDecoder$OnHeaderDecodedListener)new DefaultOnHeaderDecodedListener(i, j, options));
        if (Log.isLoggable("BitmapImageDecoder", 2)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Decoded [");
            sb.append(decodeBitmap.getWidth());
            sb.append("x");
            sb.append(decodeBitmap.getHeight());
            sb.append("] for [");
            sb.append(i);
            sb.append("x");
            sb.append(j);
            sb.append("]");
            Log.v("BitmapImageDecoder", sb.toString());
        }
        return new BitmapResource(decodeBitmap, this.bitmapPool);
    }
    
    @Override
    public boolean handles(final ImageDecoder$Source imageDecoder$Source, final Options options) throws IOException {
        return true;
    }
}

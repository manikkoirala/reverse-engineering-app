// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide.load.resource.bitmap;

import com.bumptech.glide.util.ByteBufferUtil;
import java.nio.ByteBuffer;
import java.io.IOException;
import androidx.exifinterface.media.ExifInterface;
import com.bumptech.glide.load.engine.bitmap_recycle.ArrayPool;
import java.io.InputStream;
import com.bumptech.glide.load.ImageHeaderParser;

public final class ExifInterfaceImageHeaderParser implements ImageHeaderParser
{
    @Override
    public int getOrientation(final InputStream inputStream, final ArrayPool arrayPool) throws IOException {
        int attributeInt;
        if ((attributeInt = new ExifInterface(inputStream).getAttributeInt("Orientation", 1)) == 0) {
            attributeInt = -1;
        }
        return attributeInt;
    }
    
    @Override
    public int getOrientation(final ByteBuffer byteBuffer, final ArrayPool arrayPool) throws IOException {
        return this.getOrientation(ByteBufferUtil.toStream(byteBuffer), arrayPool);
    }
    
    @Override
    public ImageType getType(final InputStream inputStream) {
        return ImageType.UNKNOWN;
    }
    
    @Override
    public ImageType getType(final ByteBuffer byteBuffer) {
        return ImageType.UNKNOWN;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide.load.resource.bitmap;

import java.security.MessageDigest;
import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPool;
import com.bumptech.glide.Glide;
import android.graphics.drawable.BitmapDrawable;
import com.bumptech.glide.load.engine.Resource;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import com.bumptech.glide.load.Transformation;

public class DrawableTransformation implements Transformation<Drawable>
{
    private final boolean isRequired;
    private final Transformation<Bitmap> wrapped;
    
    public DrawableTransformation(final Transformation<Bitmap> wrapped, final boolean isRequired) {
        this.wrapped = wrapped;
        this.isRequired = isRequired;
    }
    
    private Resource<Drawable> newDrawableResource(final Context context, final Resource<Bitmap> resource) {
        return (Resource<Drawable>)LazyBitmapDrawableResource.obtain(context.getResources(), resource);
    }
    
    public Transformation<BitmapDrawable> asBitmapDrawable() {
        return (Transformation<BitmapDrawable>)this;
    }
    
    @Override
    public boolean equals(final Object o) {
        return o instanceof DrawableTransformation && this.wrapped.equals(((DrawableTransformation)o).wrapped);
    }
    
    @Override
    public int hashCode() {
        return this.wrapped.hashCode();
    }
    
    @Override
    public Resource<Drawable> transform(final Context context, final Resource<Drawable> resource, final int n, final int n2) {
        final BitmapPool bitmapPool = Glide.get(context).getBitmapPool();
        final Drawable obj = resource.get();
        final Resource<Bitmap> convert = DrawableToBitmapConverter.convert(bitmapPool, obj, n, n2);
        if (convert == null) {
            if (!this.isRequired) {
                return resource;
            }
            final StringBuilder sb = new StringBuilder();
            sb.append("Unable to convert ");
            sb.append(obj);
            sb.append(" to a Bitmap");
            throw new IllegalArgumentException(sb.toString());
        }
        else {
            final Resource<Bitmap> transform = this.wrapped.transform(context, convert, n, n2);
            if (transform.equals(convert)) {
                transform.recycle();
                return resource;
            }
            return this.newDrawableResource(context, transform);
        }
    }
    
    @Override
    public void updateDiskCacheKey(final MessageDigest messageDigest) {
        this.wrapped.updateDiskCacheKey(messageDigest);
    }
}

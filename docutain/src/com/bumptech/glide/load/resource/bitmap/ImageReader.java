// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide.load.resource.bitmap;

import android.os.ParcelFileDescriptor;
import com.bumptech.glide.load.data.ParcelFileDescriptorRewinder;
import com.bumptech.glide.util.Preconditions;
import com.bumptech.glide.load.data.InputStreamRewinder;
import java.io.FileNotFoundException;
import java.io.File;
import android.graphics.Rect;
import com.bumptech.glide.util.ByteBufferUtil;
import java.io.InputStream;
import com.bumptech.glide.load.ImageHeaderParserUtils;
import java.nio.ByteBuffer;
import android.graphics.BitmapFactory;
import java.util.List;
import com.bumptech.glide.load.engine.bitmap_recycle.ArrayPool;
import com.bumptech.glide.load.ImageHeaderParser;
import java.io.IOException;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory$Options;

interface ImageReader
{
    Bitmap decodeBitmap(final BitmapFactory$Options p0) throws IOException;
    
    int getImageOrientation() throws IOException;
    
    ImageHeaderParser.ImageType getImageType() throws IOException;
    
    void stopGrowingBuffers();
    
    public static final class ByteArrayReader implements ImageReader
    {
        private final ArrayPool byteArrayPool;
        private final byte[] bytes;
        private final List<ImageHeaderParser> parsers;
        
        ByteArrayReader(final byte[] bytes, final List<ImageHeaderParser> parsers, final ArrayPool byteArrayPool) {
            this.bytes = bytes;
            this.parsers = parsers;
            this.byteArrayPool = byteArrayPool;
        }
        
        @Override
        public Bitmap decodeBitmap(final BitmapFactory$Options bitmapFactory$Options) {
            final byte[] bytes = this.bytes;
            return BitmapFactory.decodeByteArray(bytes, 0, bytes.length, bitmapFactory$Options);
        }
        
        @Override
        public int getImageOrientation() throws IOException {
            return ImageHeaderParserUtils.getOrientation(this.parsers, ByteBuffer.wrap(this.bytes), this.byteArrayPool);
        }
        
        @Override
        public ImageHeaderParser.ImageType getImageType() throws IOException {
            return ImageHeaderParserUtils.getType(this.parsers, ByteBuffer.wrap(this.bytes));
        }
        
        @Override
        public void stopGrowingBuffers() {
        }
    }
    
    public static final class ByteBufferReader implements ImageReader
    {
        private final ByteBuffer buffer;
        private final ArrayPool byteArrayPool;
        private final List<ImageHeaderParser> parsers;
        
        ByteBufferReader(final ByteBuffer buffer, final List<ImageHeaderParser> parsers, final ArrayPool byteArrayPool) {
            this.buffer = buffer;
            this.parsers = parsers;
            this.byteArrayPool = byteArrayPool;
        }
        
        private InputStream stream() {
            return ByteBufferUtil.toStream(ByteBufferUtil.rewind(this.buffer));
        }
        
        @Override
        public Bitmap decodeBitmap(final BitmapFactory$Options bitmapFactory$Options) {
            return BitmapFactory.decodeStream(this.stream(), (Rect)null, bitmapFactory$Options);
        }
        
        @Override
        public int getImageOrientation() throws IOException {
            return ImageHeaderParserUtils.getOrientation(this.parsers, ByteBufferUtil.rewind(this.buffer), this.byteArrayPool);
        }
        
        @Override
        public ImageHeaderParser.ImageType getImageType() throws IOException {
            return ImageHeaderParserUtils.getType(this.parsers, ByteBufferUtil.rewind(this.buffer));
        }
        
        @Override
        public void stopGrowingBuffers() {
        }
    }
    
    public static final class FileReader implements ImageReader
    {
        private final ArrayPool byteArrayPool;
        private final File file;
        private final List<ImageHeaderParser> parsers;
        
        FileReader(final File file, final List<ImageHeaderParser> parsers, final ArrayPool byteArrayPool) {
            this.file = file;
            this.parsers = parsers;
            this.byteArrayPool = byteArrayPool;
        }
        
        @Override
        public Bitmap decodeBitmap(final BitmapFactory$Options p0) throws FileNotFoundException {
            // 
            // This method could not be decompiled.
            // 
            // Original Bytecode:
            // 
            //     1: astore_3       
            //     2: new             Lcom/bumptech/glide/load/resource/bitmap/RecyclableBufferedInputStream;
            //     5: astore_2       
            //     6: new             Ljava/io/FileInputStream;
            //     9: astore          4
            //    11: aload           4
            //    13: aload_0        
            //    14: getfield        com/bumptech/glide/load/resource/bitmap/ImageReader$FileReader.file:Ljava/io/File;
            //    17: invokespecial   java/io/FileInputStream.<init>:(Ljava/io/File;)V
            //    20: aload_2        
            //    21: aload           4
            //    23: aload_0        
            //    24: getfield        com/bumptech/glide/load/resource/bitmap/ImageReader$FileReader.byteArrayPool:Lcom/bumptech/glide/load/engine/bitmap_recycle/ArrayPool;
            //    27: invokespecial   com/bumptech/glide/load/resource/bitmap/RecyclableBufferedInputStream.<init>:(Ljava/io/InputStream;Lcom/bumptech/glide/load/engine/bitmap_recycle/ArrayPool;)V
            //    30: aload_2        
            //    31: aconst_null    
            //    32: aload_1        
            //    33: invokestatic    android/graphics/BitmapFactory.decodeStream:(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
            //    36: astore_1       
            //    37: aload_2        
            //    38: invokevirtual   java/io/InputStream.close:()V
            //    41: aload_1        
            //    42: areturn        
            //    43: astore_1       
            //    44: goto            50
            //    47: astore_1       
            //    48: aload_3        
            //    49: astore_2       
            //    50: aload_2        
            //    51: ifnull          58
            //    54: aload_2        
            //    55: invokevirtual   java/io/InputStream.close:()V
            //    58: aload_1        
            //    59: athrow         
            //    60: astore_2       
            //    61: goto            41
            //    64: astore_2       
            //    65: goto            58
            //    Exceptions:
            //  throws java.io.FileNotFoundException
            //    Exceptions:
            //  Try           Handler
            //  Start  End    Start  End    Type                 
            //  -----  -----  -----  -----  ---------------------
            //  2      30     47     50     Any
            //  30     37     43     47     Any
            //  37     41     60     64     Ljava/io/IOException;
            //  54     58     64     68     Ljava/io/IOException;
            // 
            // The error that occurred was:
            // 
            // java.lang.IllegalStateException: Expression is linked from several locations: Label_0041:
            //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
            //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2604)
            //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
            //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
            //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:206)
            //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:662)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
            //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
            //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
            //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
            //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
            //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
            // 
            throw new IllegalStateException("An error occurred while decompiling this method.");
        }
        
        @Override
        public int getImageOrientation() throws IOException {
            // 
            // This method could not be decompiled.
            // 
            // Original Bytecode:
            // 
            //     3: astore_3       
            //     4: new             Ljava/io/FileInputStream;
            //     7: astore_2       
            //     8: aload_2        
            //     9: aload_0        
            //    10: getfield        com/bumptech/glide/load/resource/bitmap/ImageReader$FileReader.file:Ljava/io/File;
            //    13: invokespecial   java/io/FileInputStream.<init>:(Ljava/io/File;)V
            //    16: aload_3        
            //    17: aload_2        
            //    18: aload_0        
            //    19: getfield        com/bumptech/glide/load/resource/bitmap/ImageReader$FileReader.byteArrayPool:Lcom/bumptech/glide/load/engine/bitmap_recycle/ArrayPool;
            //    22: invokespecial   com/bumptech/glide/load/resource/bitmap/RecyclableBufferedInputStream.<init>:(Ljava/io/InputStream;Lcom/bumptech/glide/load/engine/bitmap_recycle/ArrayPool;)V
            //    25: aload_0        
            //    26: getfield        com/bumptech/glide/load/resource/bitmap/ImageReader$FileReader.parsers:Ljava/util/List;
            //    29: aload_3        
            //    30: aload_0        
            //    31: getfield        com/bumptech/glide/load/resource/bitmap/ImageReader$FileReader.byteArrayPool:Lcom/bumptech/glide/load/engine/bitmap_recycle/ArrayPool;
            //    34: invokestatic    com/bumptech/glide/load/ImageHeaderParserUtils.getOrientation:(Ljava/util/List;Ljava/io/InputStream;Lcom/bumptech/glide/load/engine/bitmap_recycle/ArrayPool;)I
            //    37: istore_1       
            //    38: aload_3        
            //    39: invokevirtual   java/io/InputStream.close:()V
            //    42: iload_1        
            //    43: ireturn        
            //    44: astore_2       
            //    45: goto            51
            //    48: astore_2       
            //    49: aconst_null    
            //    50: astore_3       
            //    51: aload_3        
            //    52: ifnull          59
            //    55: aload_3        
            //    56: invokevirtual   java/io/InputStream.close:()V
            //    59: aload_2        
            //    60: athrow         
            //    61: astore_2       
            //    62: goto            42
            //    65: astore_3       
            //    66: goto            59
            //    Exceptions:
            //  throws java.io.IOException
            //    Exceptions:
            //  Try           Handler
            //  Start  End    Start  End    Type                 
            //  -----  -----  -----  -----  ---------------------
            //  0      25     48     51     Any
            //  25     38     44     48     Any
            //  38     42     61     65     Ljava/io/IOException;
            //  55     59     65     69     Ljava/io/IOException;
            // 
            // The error that occurred was:
            // 
            // java.lang.IllegalStateException: Expression is linked from several locations: Label_0042:
            //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
            //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2604)
            //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
            //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
            //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:206)
            //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:662)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
            //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
            //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
            //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
            //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
            //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
            // 
            throw new IllegalStateException("An error occurred while decompiling this method.");
        }
        
        @Override
        public ImageHeaderParser.ImageType getImageType() throws IOException {
            // 
            // This method could not be decompiled.
            // 
            // Original Bytecode:
            // 
            //     3: astore_2       
            //     4: new             Ljava/io/FileInputStream;
            //     7: astore_1       
            //     8: aload_1        
            //     9: aload_0        
            //    10: getfield        com/bumptech/glide/load/resource/bitmap/ImageReader$FileReader.file:Ljava/io/File;
            //    13: invokespecial   java/io/FileInputStream.<init>:(Ljava/io/File;)V
            //    16: aload_2        
            //    17: aload_1        
            //    18: aload_0        
            //    19: getfield        com/bumptech/glide/load/resource/bitmap/ImageReader$FileReader.byteArrayPool:Lcom/bumptech/glide/load/engine/bitmap_recycle/ArrayPool;
            //    22: invokespecial   com/bumptech/glide/load/resource/bitmap/RecyclableBufferedInputStream.<init>:(Ljava/io/InputStream;Lcom/bumptech/glide/load/engine/bitmap_recycle/ArrayPool;)V
            //    25: aload_0        
            //    26: getfield        com/bumptech/glide/load/resource/bitmap/ImageReader$FileReader.parsers:Ljava/util/List;
            //    29: aload_2        
            //    30: aload_0        
            //    31: getfield        com/bumptech/glide/load/resource/bitmap/ImageReader$FileReader.byteArrayPool:Lcom/bumptech/glide/load/engine/bitmap_recycle/ArrayPool;
            //    34: invokestatic    com/bumptech/glide/load/ImageHeaderParserUtils.getType:(Ljava/util/List;Ljava/io/InputStream;Lcom/bumptech/glide/load/engine/bitmap_recycle/ArrayPool;)Lcom/bumptech/glide/load/ImageHeaderParser$ImageType;
            //    37: astore_1       
            //    38: aload_2        
            //    39: invokevirtual   java/io/InputStream.close:()V
            //    42: aload_1        
            //    43: areturn        
            //    44: astore_1       
            //    45: goto            51
            //    48: astore_1       
            //    49: aconst_null    
            //    50: astore_2       
            //    51: aload_2        
            //    52: ifnull          59
            //    55: aload_2        
            //    56: invokevirtual   java/io/InputStream.close:()V
            //    59: aload_1        
            //    60: athrow         
            //    61: astore_2       
            //    62: goto            42
            //    65: astore_2       
            //    66: goto            59
            //    Exceptions:
            //  throws java.io.IOException
            //    Exceptions:
            //  Try           Handler
            //  Start  End    Start  End    Type                 
            //  -----  -----  -----  -----  ---------------------
            //  0      25     48     51     Any
            //  25     38     44     48     Any
            //  38     42     61     65     Ljava/io/IOException;
            //  55     59     65     69     Ljava/io/IOException;
            // 
            // The error that occurred was:
            // 
            // java.lang.IllegalStateException: Expression is linked from several locations: Label_0042:
            //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
            //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2604)
            //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
            //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
            //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:206)
            //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:662)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
            //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
            //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
            //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
            //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
            //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
            // 
            throw new IllegalStateException("An error occurred while decompiling this method.");
        }
        
        @Override
        public void stopGrowingBuffers() {
        }
    }
    
    public static final class InputStreamImageReader implements ImageReader
    {
        private final ArrayPool byteArrayPool;
        private final InputStreamRewinder dataRewinder;
        private final List<ImageHeaderParser> parsers;
        
        InputStreamImageReader(final InputStream inputStream, final List<ImageHeaderParser> list, final ArrayPool arrayPool) {
            this.byteArrayPool = Preconditions.checkNotNull(arrayPool);
            this.parsers = Preconditions.checkNotNull(list);
            this.dataRewinder = new InputStreamRewinder(inputStream, arrayPool);
        }
        
        @Override
        public Bitmap decodeBitmap(final BitmapFactory$Options bitmapFactory$Options) throws IOException {
            return BitmapFactory.decodeStream(this.dataRewinder.rewindAndGet(), (Rect)null, bitmapFactory$Options);
        }
        
        @Override
        public int getImageOrientation() throws IOException {
            return ImageHeaderParserUtils.getOrientation(this.parsers, this.dataRewinder.rewindAndGet(), this.byteArrayPool);
        }
        
        @Override
        public ImageHeaderParser.ImageType getImageType() throws IOException {
            return ImageHeaderParserUtils.getType(this.parsers, this.dataRewinder.rewindAndGet(), this.byteArrayPool);
        }
        
        @Override
        public void stopGrowingBuffers() {
            this.dataRewinder.fixMarkLimits();
        }
    }
    
    public static final class ParcelFileDescriptorImageReader implements ImageReader
    {
        private final ArrayPool byteArrayPool;
        private final ParcelFileDescriptorRewinder dataRewinder;
        private final List<ImageHeaderParser> parsers;
        
        ParcelFileDescriptorImageReader(final ParcelFileDescriptor parcelFileDescriptor, final List<ImageHeaderParser> list, final ArrayPool arrayPool) {
            this.byteArrayPool = Preconditions.checkNotNull(arrayPool);
            this.parsers = Preconditions.checkNotNull(list);
            this.dataRewinder = new ParcelFileDescriptorRewinder(parcelFileDescriptor);
        }
        
        @Override
        public Bitmap decodeBitmap(final BitmapFactory$Options bitmapFactory$Options) throws IOException {
            return BitmapFactory.decodeFileDescriptor(this.dataRewinder.rewindAndGet().getFileDescriptor(), (Rect)null, bitmapFactory$Options);
        }
        
        @Override
        public int getImageOrientation() throws IOException {
            return ImageHeaderParserUtils.getOrientation(this.parsers, this.dataRewinder, this.byteArrayPool);
        }
        
        @Override
        public ImageHeaderParser.ImageType getImageType() throws IOException {
            return ImageHeaderParserUtils.getType(this.parsers, this.dataRewinder, this.byteArrayPool);
        }
        
        @Override
        public void stopGrowingBuffers() {
        }
    }
}

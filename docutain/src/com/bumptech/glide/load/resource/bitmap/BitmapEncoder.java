// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide.load.resource.bitmap;

import com.bumptech.glide.load.EncodeStrategy;
import java.io.File;
import com.bumptech.glide.load.engine.Resource;
import com.bumptech.glide.load.Options;
import com.bumptech.glide.load.engine.bitmap_recycle.ArrayPool;
import android.graphics.Bitmap$CompressFormat;
import com.bumptech.glide.load.Option;
import android.graphics.Bitmap;
import com.bumptech.glide.load.ResourceEncoder;

public class BitmapEncoder implements ResourceEncoder<Bitmap>
{
    public static final Option<Bitmap$CompressFormat> COMPRESSION_FORMAT;
    public static final Option<Integer> COMPRESSION_QUALITY;
    private static final String TAG = "BitmapEncoder";
    private final ArrayPool arrayPool;
    
    static {
        COMPRESSION_QUALITY = Option.memory("com.bumptech.glide.load.resource.bitmap.BitmapEncoder.CompressionQuality", 90);
        COMPRESSION_FORMAT = Option.memory("com.bumptech.glide.load.resource.bitmap.BitmapEncoder.CompressionFormat");
    }
    
    @Deprecated
    public BitmapEncoder() {
        this.arrayPool = null;
    }
    
    public BitmapEncoder(final ArrayPool arrayPool) {
        this.arrayPool = arrayPool;
    }
    
    private Bitmap$CompressFormat getFormat(final Bitmap bitmap, final Options options) {
        final Bitmap$CompressFormat bitmap$CompressFormat = options.get(BitmapEncoder.COMPRESSION_FORMAT);
        if (bitmap$CompressFormat != null) {
            return bitmap$CompressFormat;
        }
        if (bitmap.hasAlpha()) {
            return Bitmap$CompressFormat.PNG;
        }
        return Bitmap$CompressFormat.JPEG;
    }
    
    @Override
    public boolean encode(final Resource<Bitmap> p0, final File p1, final Options p2) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: invokeinterface com/bumptech/glide/load/engine/Resource.get:()Ljava/lang/Object;
        //     6: checkcast       Landroid/graphics/Bitmap;
        //     9: astore          13
        //    11: aload_0        
        //    12: aload           13
        //    14: aload_3        
        //    15: invokespecial   com/bumptech/glide/load/resource/bitmap/BitmapEncoder.getFormat:(Landroid/graphics/Bitmap;Lcom/bumptech/glide/load/Options;)Landroid/graphics/Bitmap$CompressFormat;
        //    18: astore          14
        //    20: ldc             "encode: [%dx%d] %s"
        //    22: aload           13
        //    24: invokevirtual   android/graphics/Bitmap.getWidth:()I
        //    27: invokestatic    java/lang/Integer.valueOf:(I)Ljava/lang/Integer;
        //    30: aload           13
        //    32: invokevirtual   android/graphics/Bitmap.getHeight:()I
        //    35: invokestatic    java/lang/Integer.valueOf:(I)Ljava/lang/Integer;
        //    38: aload           14
        //    40: invokestatic    com/bumptech/glide/util/pool/GlideTrace.beginSectionFormat:(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
        //    43: invokestatic    com/bumptech/glide/util/LogTime.getLogTime:()J
        //    46: lstore          7
        //    48: aload_3        
        //    49: getstatic       com/bumptech/glide/load/resource/bitmap/BitmapEncoder.COMPRESSION_QUALITY:Lcom/bumptech/glide/load/Option;
        //    52: invokevirtual   com/bumptech/glide/load/Options.get:(Lcom/bumptech/glide/load/Option;)Ljava/lang/Object;
        //    55: checkcast       Ljava/lang/Integer;
        //    58: invokevirtual   java/lang/Integer.intValue:()I
        //    61: istore          4
        //    63: iconst_0       
        //    64: istore          5
        //    66: iconst_0       
        //    67: istore          6
        //    69: aconst_null    
        //    70: astore          12
        //    72: aconst_null    
        //    73: astore          11
        //    75: aload           11
        //    77: astore          9
        //    79: aload           12
        //    81: astore          10
        //    83: new             Ljava/io/FileOutputStream;
        //    86: astore_1       
        //    87: aload           11
        //    89: astore          9
        //    91: aload           12
        //    93: astore          10
        //    95: aload_1        
        //    96: aload_2        
        //    97: invokespecial   java/io/FileOutputStream.<init>:(Ljava/io/File;)V
        //   100: aload_0        
        //   101: getfield        com/bumptech/glide/load/resource/bitmap/BitmapEncoder.arrayPool:Lcom/bumptech/glide/load/engine/bitmap_recycle/ArrayPool;
        //   104: ifnull          125
        //   107: new             Lcom/bumptech/glide/load/data/BufferedOutputStream;
        //   110: astore_2       
        //   111: aload_2        
        //   112: aload_1        
        //   113: aload_0        
        //   114: getfield        com/bumptech/glide/load/resource/bitmap/BitmapEncoder.arrayPool:Lcom/bumptech/glide/load/engine/bitmap_recycle/ArrayPool;
        //   117: invokespecial   com/bumptech/glide/load/data/BufferedOutputStream.<init>:(Ljava/io/OutputStream;Lcom/bumptech/glide/load/engine/bitmap_recycle/ArrayPool;)V
        //   120: aload_2        
        //   121: astore_1       
        //   122: goto            125
        //   125: aload_1        
        //   126: astore          9
        //   128: aload_1        
        //   129: astore          10
        //   131: aload           13
        //   133: aload           14
        //   135: iload           4
        //   137: aload_1        
        //   138: invokevirtual   android/graphics/Bitmap.compress:(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z
        //   141: pop            
        //   142: aload_1        
        //   143: astore          9
        //   145: aload_1        
        //   146: astore          10
        //   148: aload_1        
        //   149: invokevirtual   java/io/OutputStream.close:()V
        //   152: iconst_1       
        //   153: istore          5
        //   155: aload_1        
        //   156: invokevirtual   java/io/OutputStream.close:()V
        //   159: goto            218
        //   162: astore_2       
        //   163: aload_1        
        //   164: astore          9
        //   166: aload_2        
        //   167: astore_1       
        //   168: goto            335
        //   171: astore_2       
        //   172: goto            183
        //   175: astore_1       
        //   176: goto            335
        //   179: astore_2       
        //   180: aload           10
        //   182: astore_1       
        //   183: aload_1        
        //   184: astore          9
        //   186: ldc             "BitmapEncoder"
        //   188: iconst_3       
        //   189: invokestatic    android/util/Log.isLoggable:(Ljava/lang/String;I)Z
        //   192: ifeq            207
        //   195: aload_1        
        //   196: astore          9
        //   198: ldc             "BitmapEncoder"
        //   200: ldc             "Failed to encode Bitmap"
        //   202: aload_2        
        //   203: invokestatic    android/util/Log.d:(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //   206: pop            
        //   207: aload_1        
        //   208: ifnull          218
        //   211: iload           6
        //   213: istore          5
        //   215: goto            155
        //   218: ldc             "BitmapEncoder"
        //   220: iconst_2       
        //   221: invokestatic    android/util/Log.isLoggable:(Ljava/lang/String;I)Z
        //   224: ifeq            329
        //   227: new             Ljava/lang/StringBuilder;
        //   230: astore_1       
        //   231: aload_1        
        //   232: invokespecial   java/lang/StringBuilder.<init>:()V
        //   235: aload_1        
        //   236: ldc             "Compressed with type: "
        //   238: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   241: pop            
        //   242: aload_1        
        //   243: aload           14
        //   245: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //   248: pop            
        //   249: aload_1        
        //   250: ldc             " of size "
        //   252: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   255: pop            
        //   256: aload_1        
        //   257: aload           13
        //   259: invokestatic    com/bumptech/glide/util/Util.getBitmapByteSize:(Landroid/graphics/Bitmap;)I
        //   262: invokevirtual   java/lang/StringBuilder.append:(I)Ljava/lang/StringBuilder;
        //   265: pop            
        //   266: aload_1        
        //   267: ldc             " in "
        //   269: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   272: pop            
        //   273: aload_1        
        //   274: lload           7
        //   276: invokestatic    com/bumptech/glide/util/LogTime.getElapsedMillis:(J)D
        //   279: invokevirtual   java/lang/StringBuilder.append:(D)Ljava/lang/StringBuilder;
        //   282: pop            
        //   283: aload_1        
        //   284: ldc             ", options format: "
        //   286: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   289: pop            
        //   290: aload_1        
        //   291: aload_3        
        //   292: getstatic       com/bumptech/glide/load/resource/bitmap/BitmapEncoder.COMPRESSION_FORMAT:Lcom/bumptech/glide/load/Option;
        //   295: invokevirtual   com/bumptech/glide/load/Options.get:(Lcom/bumptech/glide/load/Option;)Ljava/lang/Object;
        //   298: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //   301: pop            
        //   302: aload_1        
        //   303: ldc             ", hasAlpha: "
        //   305: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   308: pop            
        //   309: aload_1        
        //   310: aload           13
        //   312: invokevirtual   android/graphics/Bitmap.hasAlpha:()Z
        //   315: invokevirtual   java/lang/StringBuilder.append:(Z)Ljava/lang/StringBuilder;
        //   318: pop            
        //   319: ldc             "BitmapEncoder"
        //   321: aload_1        
        //   322: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   325: invokestatic    android/util/Log.v:(Ljava/lang/String;Ljava/lang/String;)I
        //   328: pop            
        //   329: invokestatic    com/bumptech/glide/util/pool/GlideTrace.endSection:()V
        //   332: iload           5
        //   334: ireturn        
        //   335: aload           9
        //   337: ifnull          345
        //   340: aload           9
        //   342: invokevirtual   java/io/OutputStream.close:()V
        //   345: aload_1        
        //   346: athrow         
        //   347: astore_1       
        //   348: invokestatic    com/bumptech/glide/util/pool/GlideTrace.endSection:()V
        //   351: aload_1        
        //   352: athrow         
        //   353: astore_1       
        //   354: goto            218
        //   357: astore_2       
        //   358: goto            345
        //    Signature:
        //  (Lcom/bumptech/glide/load/engine/Resource<Landroid/graphics/Bitmap;>;Ljava/io/File;Lcom/bumptech/glide/load/Options;)Z
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                 
        //  -----  -----  -----  -----  ---------------------
        //  43     63     347    353    Any
        //  83     87     179    183    Ljava/io/IOException;
        //  83     87     175    179    Any
        //  95     100    179    183    Ljava/io/IOException;
        //  95     100    175    179    Any
        //  100    120    171    175    Ljava/io/IOException;
        //  100    120    162    171    Any
        //  131    142    179    183    Ljava/io/IOException;
        //  131    142    175    179    Any
        //  148    152    179    183    Ljava/io/IOException;
        //  148    152    175    179    Any
        //  155    159    353    357    Ljava/io/IOException;
        //  155    159    347    353    Any
        //  186    195    175    179    Any
        //  198    207    175    179    Any
        //  218    329    347    353    Any
        //  340    345    357    361    Ljava/io/IOException;
        //  340    345    347    353    Any
        //  345    347    347    353    Any
        // 
        // The error that occurred was:
        // 
        // java.lang.IndexOutOfBoundsException: Index 187 out of bounds for length 187
        //     at java.base/jdk.internal.util.Preconditions.outOfBounds(Preconditions.java:64)
        //     at java.base/jdk.internal.util.Preconditions.outOfBoundsCheckIndex(Preconditions.java:70)
        //     at java.base/jdk.internal.util.Preconditions.checkIndex(Preconditions.java:266)
        //     at java.base/java.util.Objects.checkIndex(Objects.java:361)
        //     at java.base/java.util.ArrayList.get(ArrayList.java:427)
        //     at com.strobel.decompiler.ast.AstBuilder.convertToAst(AstBuilder.java:3362)
        //     at com.strobel.decompiler.ast.AstBuilder.build(AstBuilder.java:112)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:203)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    @Override
    public EncodeStrategy getEncodeStrategy(final Options options) {
        return EncodeStrategy.TRANSFORMED;
    }
}

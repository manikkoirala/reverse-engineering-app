// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide.load.resource;

import android.util.Size;
import android.graphics.ColorSpace;
import android.graphics.ColorSpace$Named;
import android.os.Build$VERSION;
import android.util.Log;
import android.graphics.ImageDecoder$DecodeException;
import android.graphics.ImageDecoder$OnPartialImageListener;
import android.graphics.ImageDecoder$Source;
import android.graphics.ImageDecoder$ImageInfo;
import android.graphics.ImageDecoder;
import com.bumptech.glide.load.resource.bitmap.Downsampler;
import com.bumptech.glide.load.Options;
import com.bumptech.glide.load.resource.bitmap.DownsampleStrategy;
import com.bumptech.glide.load.PreferredColorSpace;
import com.bumptech.glide.load.resource.bitmap.HardwareConfigState;
import com.bumptech.glide.load.DecodeFormat;
import android.graphics.ImageDecoder$OnHeaderDecodedListener;

public final class DefaultOnHeaderDecodedListener implements ImageDecoder$OnHeaderDecodedListener
{
    private static final String TAG = "ImageDecoder";
    private final DecodeFormat decodeFormat;
    private final HardwareConfigState hardwareConfigState;
    private final boolean isHardwareConfigAllowed;
    private final PreferredColorSpace preferredColorSpace;
    private final int requestedHeight;
    private final int requestedWidth;
    private final DownsampleStrategy strategy;
    
    public DefaultOnHeaderDecodedListener(final int requestedWidth, final int requestedHeight, final Options options) {
        this.hardwareConfigState = HardwareConfigState.getInstance();
        this.requestedWidth = requestedWidth;
        this.requestedHeight = requestedHeight;
        this.decodeFormat = options.get(Downsampler.DECODE_FORMAT);
        this.strategy = options.get(DownsampleStrategy.OPTION);
        this.isHardwareConfigAllowed = (options.get(Downsampler.ALLOW_HARDWARE_CONFIG) != null && options.get(Downsampler.ALLOW_HARDWARE_CONFIG));
        this.preferredColorSpace = options.get(Downsampler.PREFERRED_COLOR_SPACE);
    }
    
    public void onHeaderDecoded(final ImageDecoder imageDecoder, final ImageDecoder$ImageInfo imageDecoder$ImageInfo, final ImageDecoder$Source imageDecoder$Source) {
        final HardwareConfigState hardwareConfigState = this.hardwareConfigState;
        final int requestedWidth = this.requestedWidth;
        final int requestedHeight = this.requestedHeight;
        final boolean isHardwareConfigAllowed = this.isHardwareConfigAllowed;
        final boolean b = false;
        if (hardwareConfigState.isHardwareConfigAllowed(requestedWidth, requestedHeight, isHardwareConfigAllowed, false)) {
            imageDecoder.setAllocator(3);
        }
        else {
            imageDecoder.setAllocator(1);
        }
        if (this.decodeFormat == DecodeFormat.PREFER_RGB_565) {
            imageDecoder.setMemorySizePolicy(0);
        }
        imageDecoder.setOnPartialImageListener((ImageDecoder$OnPartialImageListener)new ImageDecoder$OnPartialImageListener(this) {
            final DefaultOnHeaderDecodedListener this$0;
            
            public boolean onPartialImage(final ImageDecoder$DecodeException ex) {
                return false;
            }
        });
        final Size size = imageDecoder$ImageInfo.getSize();
        int n;
        if ((n = this.requestedWidth) == Integer.MIN_VALUE) {
            n = size.getWidth();
        }
        int n2;
        if ((n2 = this.requestedHeight) == Integer.MIN_VALUE) {
            n2 = size.getHeight();
        }
        final float scaleFactor = this.strategy.getScaleFactor(size.getWidth(), size.getHeight(), n, n2);
        final int round = Math.round(size.getWidth() * scaleFactor);
        final int round2 = Math.round(size.getHeight() * scaleFactor);
        if (Log.isLoggable("ImageDecoder", 2)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Resizing from [");
            sb.append(size.getWidth());
            sb.append("x");
            sb.append(size.getHeight());
            sb.append("] to [");
            sb.append(round);
            sb.append("x");
            sb.append(round2);
            sb.append("] scaleFactor: ");
            sb.append(scaleFactor);
            Log.v("ImageDecoder", sb.toString());
        }
        imageDecoder.setTargetSize(round, round2);
        if (this.preferredColorSpace != null) {
            if (Build$VERSION.SDK_INT >= 28) {
                int n3 = b ? 1 : 0;
                if (this.preferredColorSpace == PreferredColorSpace.DISPLAY_P3) {
                    n3 = (b ? 1 : 0);
                    if (imageDecoder$ImageInfo.getColorSpace() != null) {
                        n3 = (b ? 1 : 0);
                        if (imageDecoder$ImageInfo.getColorSpace().isWideGamut()) {
                            n3 = 1;
                        }
                    }
                }
                ColorSpace$Named colorSpace$Named;
                if (n3 != 0) {
                    colorSpace$Named = ColorSpace$Named.DISPLAY_P3;
                }
                else {
                    colorSpace$Named = ColorSpace$Named.SRGB;
                }
                imageDecoder.setTargetColorSpace(ColorSpace.get(colorSpace$Named));
            }
            else if (Build$VERSION.SDK_INT >= 26) {
                imageDecoder.setTargetColorSpace(ColorSpace.get(ColorSpace$Named.SRGB));
            }
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide.load.model;

import java.io.ByteArrayInputStream;
import android.util.Base64;
import java.io.InputStream;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.DataSource;
import java.io.IOException;
import com.bumptech.glide.load.data.DataFetcher;
import com.bumptech.glide.load.Key;
import com.bumptech.glide.signature.ObjectKey;
import com.bumptech.glide.load.Options;

public final class DataUrlLoader<Model, Data> implements ModelLoader<Model, Data>
{
    private static final String BASE64_TAG = ";base64";
    private static final String DATA_SCHEME_IMAGE = "data:image";
    private final DataDecoder<Data> dataDecoder;
    
    public DataUrlLoader(final DataDecoder<Data> dataDecoder) {
        this.dataDecoder = dataDecoder;
    }
    
    @Override
    public LoadData<Data> buildLoadData(final Model model, final int n, final int n2, final Options options) {
        return (LoadData<Data>)new LoadData(new ObjectKey(model), new DataUriFetcher<Object>(model.toString(), (DataDecoder<Object>)this.dataDecoder));
    }
    
    @Override
    public boolean handles(final Model model) {
        return model.toString().startsWith("data:image");
    }
    
    public interface DataDecoder<Data>
    {
        void close(final Data p0) throws IOException;
        
        Data decode(final String p0) throws IllegalArgumentException;
        
        Class<Data> getDataClass();
    }
    
    private static final class DataUriFetcher<Data> implements DataFetcher<Data>
    {
        private Data data;
        private final String dataUri;
        private final DataDecoder<Data> reader;
        
        DataUriFetcher(final String dataUri, final DataDecoder<Data> reader) {
            this.dataUri = dataUri;
            this.reader = reader;
        }
        
        @Override
        public void cancel() {
        }
        
        @Override
        public void cleanup() {
            try {
                this.reader.close(this.data);
            }
            catch (final IOException ex) {}
        }
        
        @Override
        public Class<Data> getDataClass() {
            return this.reader.getDataClass();
        }
        
        @Override
        public DataSource getDataSource() {
            return DataSource.LOCAL;
        }
        
        @Override
        public void loadData(final Priority priority, final DataCallback<? super Data> dataCallback) {
            try {
                dataCallback.onDataReady(this.data = this.reader.decode(this.dataUri));
            }
            catch (final IllegalArgumentException ex) {
                dataCallback.onLoadFailed(ex);
            }
        }
    }
    
    public static final class StreamFactory<Model> implements ModelLoaderFactory<Model, InputStream>
    {
        private final DataDecoder<InputStream> opener;
        
        public StreamFactory() {
            this.opener = new DataDecoder<InputStream>(this) {
                final StreamFactory this$0;
                
                public void close(final InputStream inputStream) throws IOException {
                    inputStream.close();
                }
                
                public InputStream decode(final String s) {
                    if (!s.startsWith("data:image")) {
                        throw new IllegalArgumentException("Not a valid image data URL.");
                    }
                    final int index = s.indexOf(44);
                    if (index == -1) {
                        throw new IllegalArgumentException("Missing comma in data URL.");
                    }
                    if (s.substring(0, index).endsWith(";base64")) {
                        return new ByteArrayInputStream(Base64.decode(s.substring(index + 1), 0));
                    }
                    throw new IllegalArgumentException("Not a base64 image data URL.");
                }
                
                @Override
                public Class<InputStream> getDataClass() {
                    return InputStream.class;
                }
            };
        }
        
        @Override
        public ModelLoader<Model, InputStream> build(final MultiModelLoaderFactory multiModelLoaderFactory) {
            return new DataUrlLoader<Model, InputStream>(this.opener);
        }
        
        @Override
        public void teardown() {
        }
    }
}

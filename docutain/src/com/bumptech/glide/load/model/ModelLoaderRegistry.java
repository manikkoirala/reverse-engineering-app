// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide.load.model;

import java.util.HashMap;
import java.util.Map;
import java.util.ArrayList;
import com.bumptech.glide.Registry;
import java.util.Iterator;
import java.util.Collections;
import java.util.List;
import androidx.core.util.Pools;

public class ModelLoaderRegistry
{
    private final ModelLoaderCache cache;
    private final MultiModelLoaderFactory multiModelLoaderFactory;
    
    public ModelLoaderRegistry(final Pools.Pool<List<Throwable>> pool) {
        this(new MultiModelLoaderFactory(pool));
    }
    
    private ModelLoaderRegistry(final MultiModelLoaderFactory multiModelLoaderFactory) {
        this.cache = new ModelLoaderCache();
        this.multiModelLoaderFactory = multiModelLoaderFactory;
    }
    
    private static <A> Class<A> getClass(final A a) {
        return (Class<A>)a.getClass();
    }
    
    private <A> List<ModelLoader<A, ?>> getModelLoadersForClass(final Class<A> clazz) {
        synchronized (this) {
            Object o;
            if ((o = this.cache.get(clazz)) == null) {
                o = Collections.unmodifiableList((List<? extends ModelLoader<A, ?>>)this.multiModelLoaderFactory.build(clazz));
                this.cache.put(clazz, (List<ModelLoader<A, ?>>)o);
            }
            return (List<ModelLoader<A, ?>>)o;
        }
    }
    
    private <Model, Data> void tearDown(final List<ModelLoaderFactory<? extends Model, ? extends Data>> list) {
        final Iterator<ModelLoaderFactory<? extends Model, ? extends Data>> iterator = list.iterator();
        while (iterator.hasNext()) {
            iterator.next().teardown();
        }
    }
    
    public <Model, Data> void append(final Class<Model> clazz, final Class<Data> clazz2, final ModelLoaderFactory<? extends Model, ? extends Data> modelLoaderFactory) {
        synchronized (this) {
            this.multiModelLoaderFactory.append(clazz, clazz2, modelLoaderFactory);
            this.cache.clear();
        }
    }
    
    public <Model, Data> ModelLoader<Model, Data> build(final Class<Model> clazz, final Class<Data> clazz2) {
        synchronized (this) {
            return this.multiModelLoaderFactory.build(clazz, clazz2);
        }
    }
    
    public List<Class<?>> getDataClasses(final Class<?> clazz) {
        synchronized (this) {
            return this.multiModelLoaderFactory.getDataClasses(clazz);
        }
    }
    
    public <A> List<ModelLoader<A, ?>> getModelLoaders(final A a) {
        final List<ModelLoader<Object, ?>> modelLoadersForClass = this.getModelLoadersForClass((Class<Object>)getClass((A)a));
        if (modelLoadersForClass.isEmpty()) {
            throw new Registry.NoModelLoaderAvailableException(a);
        }
        final int size = modelLoadersForClass.size();
        Object emptyList = Collections.emptyList();
        int n = 1;
        Object o;
        int n2;
        for (int i = 0; i < size; ++i, emptyList = o, n = n2) {
            final ModelLoader modelLoader = modelLoadersForClass.get(i);
            o = emptyList;
            n2 = n;
            if (modelLoader.handles(a)) {
                if ((n2 = n) != 0) {
                    emptyList = new ArrayList<ModelLoader>(size - i);
                    n2 = 0;
                }
                ((List<ModelLoader>)emptyList).add(modelLoader);
                o = emptyList;
            }
        }
        if (!((List)emptyList).isEmpty()) {
            return (List<ModelLoader<A, ?>>)emptyList;
        }
        throw new Registry.NoModelLoaderAvailableException((M)a, (List<ModelLoader<M, ?>>)modelLoadersForClass);
    }
    
    public <Model, Data> void prepend(final Class<Model> clazz, final Class<Data> clazz2, final ModelLoaderFactory<? extends Model, ? extends Data> modelLoaderFactory) {
        synchronized (this) {
            this.multiModelLoaderFactory.prepend(clazz, clazz2, modelLoaderFactory);
            this.cache.clear();
        }
    }
    
    public <Model, Data> void remove(final Class<Model> clazz, final Class<Data> clazz2) {
        synchronized (this) {
            this.tearDown((List<ModelLoaderFactory<?, ?>>)this.multiModelLoaderFactory.remove(clazz, clazz2));
            this.cache.clear();
        }
    }
    
    public <Model, Data> void replace(final Class<Model> clazz, final Class<Data> clazz2, final ModelLoaderFactory<? extends Model, ? extends Data> modelLoaderFactory) {
        synchronized (this) {
            this.tearDown((List<ModelLoaderFactory<?, ?>>)this.multiModelLoaderFactory.replace(clazz, clazz2, modelLoaderFactory));
            this.cache.clear();
        }
    }
    
    private static class ModelLoaderCache
    {
        private final Map<Class<?>, Entry<?>> cachedModelLoaders;
        
        ModelLoaderCache() {
            this.cachedModelLoaders = new HashMap<Class<?>, Entry<?>>();
        }
        
        public void clear() {
            this.cachedModelLoaders.clear();
        }
        
        public <Model> List<ModelLoader<Model, ?>> get(final Class<Model> clazz) {
            final Entry entry = this.cachedModelLoaders.get(clazz);
            Object loaders;
            if (entry == null) {
                loaders = null;
            }
            else {
                loaders = entry.loaders;
            }
            return (List<ModelLoader<Model, ?>>)loaders;
        }
        
        public <Model> void put(final Class<Model> obj, final List<ModelLoader<Model, ?>> list) {
            if (this.cachedModelLoaders.put(obj, new Entry<Object>((List<ModelLoader<Object, ?>>)list)) == null) {
                return;
            }
            final StringBuilder sb = new StringBuilder();
            sb.append("Already cached loaders for model: ");
            sb.append(obj);
            throw new IllegalStateException(sb.toString());
        }
        
        private static class Entry<Model>
        {
            final List<ModelLoader<Model, ?>> loaders;
            
            public Entry(final List<ModelLoader<Model, ?>> loaders) {
                this.loaders = loaders;
            }
        }
    }
}

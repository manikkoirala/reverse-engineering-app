// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide.load.model;

import com.bumptech.glide.util.Util;
import java.util.Queue;
import com.bumptech.glide.util.LruCache;

public class ModelCache<A, B>
{
    private static final int DEFAULT_SIZE = 250;
    private final LruCache<ModelKey<A>, B> cache;
    
    public ModelCache() {
        this(250L);
    }
    
    public ModelCache(final long n) {
        this.cache = new LruCache<ModelKey<A>, B>(this, n) {
            final ModelCache this$0;
            
            @Override
            protected void onItemEvicted(final ModelKey<A> modelKey, final B b) {
                modelKey.release();
            }
        };
    }
    
    public void clear() {
        this.cache.clearMemory();
    }
    
    public B get(final A a, final int n, final int n2) {
        final ModelKey<A> value = ModelKey.get(a, n, n2);
        final B value2 = this.cache.get(value);
        value.release();
        return value2;
    }
    
    public void put(final A a, final int n, final int n2, final B b) {
        this.cache.put(ModelKey.get(a, n, n2), b);
    }
    
    static final class ModelKey<A>
    {
        private static final Queue<ModelKey<?>> KEY_QUEUE;
        private int height;
        private A model;
        private int width;
        
        static {
            KEY_QUEUE = Util.createQueue(0);
        }
        
        private ModelKey() {
        }
        
        static <A> ModelKey<A> get(final A a, final int n, final int n2) {
            Object key_QUEUE = ModelKey.KEY_QUEUE;
            synchronized (key_QUEUE) {
                final ModelKey<A> modelKey = ((Queue<ModelKey<A>>)key_QUEUE).poll();
                monitorexit(key_QUEUE);
                key_QUEUE = modelKey;
                if (modelKey == null) {
                    key_QUEUE = new ModelKey();
                }
                ((ModelKey<A>)key_QUEUE).init(a, n, n2);
                return (ModelKey<A>)key_QUEUE;
            }
        }
        
        private void init(final A model, final int width, final int height) {
            this.model = model;
            this.width = width;
            this.height = height;
        }
        
        @Override
        public boolean equals(final Object o) {
            final boolean b = o instanceof ModelKey;
            boolean b3;
            final boolean b2 = b3 = false;
            if (b) {
                final ModelKey modelKey = (ModelKey)o;
                b3 = b2;
                if (this.width == modelKey.width) {
                    b3 = b2;
                    if (this.height == modelKey.height) {
                        b3 = b2;
                        if (this.model.equals(modelKey.model)) {
                            b3 = true;
                        }
                    }
                }
            }
            return b3;
        }
        
        @Override
        public int hashCode() {
            return (this.height * 31 + this.width) * 31 + this.model.hashCode();
        }
        
        public void release() {
            final Queue<ModelKey<?>> key_QUEUE = ModelKey.KEY_QUEUE;
            synchronized (key_QUEUE) {
                key_QUEUE.offer(this);
            }
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide.load.model;

import android.content.res.Resources$NotFoundException;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.resource.drawable.DrawableDecoderCompat;
import java.io.IOException;
import android.content.res.Resources;
import com.bumptech.glide.load.data.DataFetcher;
import com.bumptech.glide.load.Key;
import com.bumptech.glide.signature.ObjectKey;
import android.os.Build$VERSION;
import com.bumptech.glide.load.resource.drawable.ResourceDrawableDecoder;
import android.content.res.Resources$Theme;
import com.bumptech.glide.load.Options;
import java.io.InputStream;
import android.graphics.drawable.Drawable;
import android.content.res.AssetFileDescriptor;
import android.content.Context;

public final class DirectResourceLoader<DataT> implements ModelLoader<Integer, DataT>
{
    private final Context context;
    private final ResourceOpener<DataT> resourceOpener;
    
    DirectResourceLoader(final Context context, final ResourceOpener<DataT> resourceOpener) {
        this.context = context.getApplicationContext();
        this.resourceOpener = resourceOpener;
    }
    
    public static ModelLoaderFactory<Integer, AssetFileDescriptor> assetFileDescriptorFactory(final Context context) {
        return new AssetFileDescriptorFactory(context);
    }
    
    public static ModelLoaderFactory<Integer, Drawable> drawableFactory(final Context context) {
        return new DrawableFactory(context);
    }
    
    public static ModelLoaderFactory<Integer, InputStream> inputStreamFactory(final Context context) {
        return new InputStreamFactory(context);
    }
    
    public LoadData<DataT> buildLoadData(final Integer n, final int n2, final int n3, final Options options) {
        final Resources$Theme resources$Theme = options.get(ResourceDrawableDecoder.THEME);
        Resources resources;
        if (Build$VERSION.SDK_INT >= 21 && resources$Theme != null) {
            resources = resources$Theme.getResources();
        }
        else {
            resources = this.context.getResources();
        }
        return (LoadData<DataT>)new LoadData(new ObjectKey(n), new ResourceDataFetcher<Object>(resources$Theme, resources, (ResourceOpener<Object>)this.resourceOpener, n));
    }
    
    @Override
    public boolean handles(final Integer n) {
        return true;
    }
    
    private static final class AssetFileDescriptorFactory implements ModelLoaderFactory<Integer, AssetFileDescriptor>, ResourceOpener<AssetFileDescriptor>
    {
        private final Context context;
        
        AssetFileDescriptorFactory(final Context context) {
            this.context = context;
        }
        
        @Override
        public ModelLoader<Integer, AssetFileDescriptor> build(final MultiModelLoaderFactory multiModelLoaderFactory) {
            return new DirectResourceLoader<AssetFileDescriptor>(this.context, (ResourceOpener<AssetFileDescriptor>)this);
        }
        
        public void close(final AssetFileDescriptor assetFileDescriptor) throws IOException {
            assetFileDescriptor.close();
        }
        
        @Override
        public Class<AssetFileDescriptor> getDataClass() {
            return AssetFileDescriptor.class;
        }
        
        public AssetFileDescriptor open(final Resources$Theme resources$Theme, final Resources resources, final int n) {
            return resources.openRawResourceFd(n);
        }
        
        @Override
        public void teardown() {
        }
    }
    
    private static final class DrawableFactory implements ModelLoaderFactory<Integer, Drawable>, ResourceOpener<Drawable>
    {
        private final Context context;
        
        DrawableFactory(final Context context) {
            this.context = context;
        }
        
        @Override
        public ModelLoader<Integer, Drawable> build(final MultiModelLoaderFactory multiModelLoaderFactory) {
            return new DirectResourceLoader<Drawable>(this.context, (ResourceOpener<Drawable>)this);
        }
        
        public void close(final Drawable drawable) throws IOException {
        }
        
        @Override
        public Class<Drawable> getDataClass() {
            return Drawable.class;
        }
        
        public Drawable open(final Resources$Theme resources$Theme, final Resources resources, final int n) {
            return DrawableDecoderCompat.getDrawable(this.context, n, resources$Theme);
        }
        
        @Override
        public void teardown() {
        }
    }
    
    private static final class InputStreamFactory implements ModelLoaderFactory<Integer, InputStream>, ResourceOpener<InputStream>
    {
        private final Context context;
        
        InputStreamFactory(final Context context) {
            this.context = context;
        }
        
        @Override
        public ModelLoader<Integer, InputStream> build(final MultiModelLoaderFactory multiModelLoaderFactory) {
            return new DirectResourceLoader<InputStream>(this.context, (ResourceOpener<InputStream>)this);
        }
        
        public void close(final InputStream inputStream) throws IOException {
            inputStream.close();
        }
        
        @Override
        public Class<InputStream> getDataClass() {
            return InputStream.class;
        }
        
        public InputStream open(final Resources$Theme resources$Theme, final Resources resources, final int n) {
            return resources.openRawResource(n);
        }
        
        @Override
        public void teardown() {
        }
    }
    
    private static final class ResourceDataFetcher<DataT> implements DataFetcher<DataT>
    {
        private DataT data;
        private final int resourceId;
        private final ResourceOpener<DataT> resourceOpener;
        private final Resources resources;
        private final Resources$Theme theme;
        
        ResourceDataFetcher(final Resources$Theme theme, final Resources resources, final ResourceOpener<DataT> resourceOpener, final int resourceId) {
            this.theme = theme;
            this.resources = resources;
            this.resourceOpener = resourceOpener;
            this.resourceId = resourceId;
        }
        
        @Override
        public void cancel() {
        }
        
        @Override
        public void cleanup() {
            final DataT data = this.data;
            if (data == null) {
                return;
            }
            try {
                this.resourceOpener.close(data);
            }
            catch (final IOException ex) {}
        }
        
        @Override
        public Class<DataT> getDataClass() {
            return this.resourceOpener.getDataClass();
        }
        
        @Override
        public DataSource getDataSource() {
            return DataSource.LOCAL;
        }
        
        @Override
        public void loadData(final Priority priority, final DataCallback<? super DataT> dataCallback) {
            try {
                dataCallback.onDataReady(this.data = this.resourceOpener.open(this.theme, this.resources, this.resourceId));
            }
            catch (final Resources$NotFoundException ex) {
                dataCallback.onLoadFailed((Exception)ex);
            }
        }
    }
    
    private interface ResourceOpener<DataT>
    {
        void close(final DataT p0) throws IOException;
        
        Class<DataT> getDataClass();
        
        DataT open(final Resources$Theme p0, final Resources p1, final int p2);
    }
}

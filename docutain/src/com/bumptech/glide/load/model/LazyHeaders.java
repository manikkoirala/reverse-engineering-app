// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide.load.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.HashMap;
import android.text.TextUtils;
import java.util.Collections;
import java.util.List;
import java.util.Map;

public final class LazyHeaders implements Headers
{
    private volatile Map<String, String> combinedHeaders;
    private final Map<String, List<LazyHeaderFactory>> headers;
    
    LazyHeaders(final Map<String, List<LazyHeaderFactory>> m) {
        this.headers = Collections.unmodifiableMap((Map<? extends String, ? extends List<LazyHeaderFactory>>)m);
    }
    
    private String buildHeaderValue(final List<LazyHeaderFactory> list) {
        final StringBuilder sb = new StringBuilder();
        for (int size = list.size(), i = 0; i < size; ++i) {
            final String buildHeader = list.get(i).buildHeader();
            if (!TextUtils.isEmpty((CharSequence)buildHeader)) {
                sb.append(buildHeader);
                if (i != list.size() - 1) {
                    sb.append(',');
                }
            }
        }
        return sb.toString();
    }
    
    private Map<String, String> generateHeaders() {
        final HashMap hashMap = new HashMap();
        for (final Map.Entry<K, List> entry : this.headers.entrySet()) {
            final String buildHeaderValue = this.buildHeaderValue(entry.getValue());
            if (!TextUtils.isEmpty((CharSequence)buildHeaderValue)) {
                hashMap.put(entry.getKey(), buildHeaderValue);
            }
        }
        return hashMap;
    }
    
    @Override
    public boolean equals(final Object o) {
        return o instanceof LazyHeaders && this.headers.equals(((LazyHeaders)o).headers);
    }
    
    @Override
    public Map<String, String> getHeaders() {
        if (this.combinedHeaders == null) {
            synchronized (this) {
                if (this.combinedHeaders == null) {
                    this.combinedHeaders = Collections.unmodifiableMap((Map<? extends String, ? extends String>)this.generateHeaders());
                }
            }
        }
        return this.combinedHeaders;
    }
    
    @Override
    public int hashCode() {
        return this.headers.hashCode();
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("LazyHeaders{headers=");
        sb.append(this.headers);
        sb.append('}');
        return sb.toString();
    }
    
    public static final class Builder
    {
        private static final Map<String, List<LazyHeaderFactory>> DEFAULT_HEADERS;
        private static final String DEFAULT_USER_AGENT;
        private static final String USER_AGENT_HEADER = "User-Agent";
        private boolean copyOnModify;
        private Map<String, List<LazyHeaderFactory>> headers;
        private boolean isUserAgentDefault;
        
        static {
            final String s = DEFAULT_USER_AGENT = getSanitizedUserAgent();
            final HashMap m = new HashMap(2);
            if (!TextUtils.isEmpty((CharSequence)s)) {
                m.put("User-Agent", Collections.singletonList(new StringHeaderFactory(s)));
            }
            DEFAULT_HEADERS = Collections.unmodifiableMap((Map<?, ?>)m);
        }
        
        public Builder() {
            this.copyOnModify = true;
            this.headers = Builder.DEFAULT_HEADERS;
            this.isUserAgentDefault = true;
        }
        
        private Map<String, List<LazyHeaderFactory>> copyHeaders() {
            final HashMap hashMap = new HashMap(this.headers.size());
            for (final Map.Entry<Object, V> entry : this.headers.entrySet()) {
                hashMap.put(entry.getKey(), new ArrayList((Collection<?>)entry.getValue()));
            }
            return hashMap;
        }
        
        private void copyIfNecessary() {
            if (this.copyOnModify) {
                this.copyOnModify = false;
                this.headers = this.copyHeaders();
            }
        }
        
        private List<LazyHeaderFactory> getFactories(final String s) {
            List list;
            if ((list = this.headers.get(s)) == null) {
                list = new ArrayList();
                this.headers.put(s, list);
            }
            return list;
        }
        
        static String getSanitizedUserAgent() {
            final String property = System.getProperty("http.agent");
            if (TextUtils.isEmpty((CharSequence)property)) {
                return property;
            }
            final int length = property.length();
            final StringBuilder sb = new StringBuilder(property.length());
            for (int i = 0; i < length; ++i) {
                final char char1 = property.charAt(i);
                if ((char1 > '\u001f' || char1 == '\t') && char1 < '\u007f') {
                    sb.append(char1);
                }
                else {
                    sb.append('?');
                }
            }
            return sb.toString();
        }
        
        public Builder addHeader(final String anotherString, final LazyHeaderFactory lazyHeaderFactory) {
            if (this.isUserAgentDefault && "User-Agent".equalsIgnoreCase(anotherString)) {
                return this.setHeader(anotherString, lazyHeaderFactory);
            }
            this.copyIfNecessary();
            this.getFactories(anotherString).add(lazyHeaderFactory);
            return this;
        }
        
        public Builder addHeader(final String s, final String s2) {
            return this.addHeader(s, new StringHeaderFactory(s2));
        }
        
        public LazyHeaders build() {
            this.copyOnModify = true;
            return new LazyHeaders(this.headers);
        }
        
        public Builder setHeader(final String anotherString, final LazyHeaderFactory lazyHeaderFactory) {
            this.copyIfNecessary();
            if (lazyHeaderFactory == null) {
                this.headers.remove(anotherString);
            }
            else {
                final List<LazyHeaderFactory> factories = this.getFactories(anotherString);
                factories.clear();
                factories.add(lazyHeaderFactory);
            }
            if (this.isUserAgentDefault && "User-Agent".equalsIgnoreCase(anotherString)) {
                this.isUserAgentDefault = false;
            }
            return this;
        }
        
        public Builder setHeader(final String s, final String s2) {
            LazyHeaderFactory lazyHeaderFactory;
            if (s2 == null) {
                lazyHeaderFactory = null;
            }
            else {
                lazyHeaderFactory = new StringHeaderFactory(s2);
            }
            return this.setHeader(s, lazyHeaderFactory);
        }
    }
    
    static final class StringHeaderFactory implements LazyHeaderFactory
    {
        private final String value;
        
        StringHeaderFactory(final String value) {
            this.value = value;
        }
        
        @Override
        public String buildHeader() {
            return this.value;
        }
        
        @Override
        public boolean equals(final Object o) {
            return o instanceof StringHeaderFactory && this.value.equals(((StringHeaderFactory)o).value);
        }
        
        @Override
        public int hashCode() {
            return this.value.hashCode();
        }
        
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder();
            sb.append("StringHeaderFactory{value='");
            sb.append(this.value);
            sb.append('\'');
            sb.append('}');
            return sb.toString();
        }
    }
}

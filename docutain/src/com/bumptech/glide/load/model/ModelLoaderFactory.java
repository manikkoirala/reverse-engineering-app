// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide.load.model;

public interface ModelLoaderFactory<T, Y>
{
    ModelLoader<T, Y> build(final MultiModelLoaderFactory p0);
    
    void teardown();
}

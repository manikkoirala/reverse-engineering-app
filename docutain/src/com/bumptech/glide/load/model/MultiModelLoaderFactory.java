// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide.load.model;

import com.bumptech.glide.load.Options;
import java.util.Iterator;
import com.bumptech.glide.Registry;
import com.bumptech.glide.util.Preconditions;
import java.util.HashSet;
import java.util.ArrayList;
import androidx.core.util.Pools;
import java.util.List;
import java.util.Set;

public class MultiModelLoaderFactory
{
    private static final Factory DEFAULT_FACTORY;
    private static final ModelLoader<Object, Object> EMPTY_MODEL_LOADER;
    private final Set<Entry<?, ?>> alreadyUsedEntries;
    private final List<Entry<?, ?>> entries;
    private final Factory factory;
    private final Pools.Pool<List<Throwable>> throwableListPool;
    
    static {
        DEFAULT_FACTORY = new Factory();
        EMPTY_MODEL_LOADER = new EmptyModelLoader();
    }
    
    public MultiModelLoaderFactory(final Pools.Pool<List<Throwable>> pool) {
        this(pool, MultiModelLoaderFactory.DEFAULT_FACTORY);
    }
    
    MultiModelLoaderFactory(final Pools.Pool<List<Throwable>> throwableListPool, final Factory factory) {
        this.entries = new ArrayList<Entry<?, ?>>();
        this.alreadyUsedEntries = new HashSet<Entry<?, ?>>();
        this.throwableListPool = throwableListPool;
        this.factory = factory;
    }
    
    private <Model, Data> void add(final Class<Model> clazz, final Class<Data> clazz2, final ModelLoaderFactory<? extends Model, ? extends Data> modelLoaderFactory, final boolean b) {
        final Entry entry = new Entry((Class<Model>)clazz, (Class<Data>)clazz2, (ModelLoaderFactory<? extends Model, ? extends Data>)modelLoaderFactory);
        final List<Entry<?, ?>> entries = this.entries;
        int size;
        if (b) {
            size = entries.size();
        }
        else {
            size = 0;
        }
        entries.add(size, entry);
    }
    
    private <Model, Data> ModelLoader<Model, Data> build(final Entry<?, ?> entry) {
        return Preconditions.checkNotNull(entry.factory.build(this));
    }
    
    private static <Model, Data> ModelLoader<Model, Data> emptyModelLoader() {
        return (ModelLoader<Model, Data>)MultiModelLoaderFactory.EMPTY_MODEL_LOADER;
    }
    
    private <Model, Data> ModelLoaderFactory<Model, Data> getFactory(final Entry<?, ?> entry) {
        return (ModelLoaderFactory<Model, Data>)entry.factory;
    }
    
     <Model, Data> void append(final Class<Model> clazz, final Class<Data> clazz2, final ModelLoaderFactory<? extends Model, ? extends Data> modelLoaderFactory) {
        synchronized (this) {
            this.add(clazz, clazz2, modelLoaderFactory, true);
        }
    }
    
    public <Model, Data> ModelLoader<Model, Data> build(final Class<Model> clazz, final Class<Data> clazz2) {
        monitorenter(this);
        try {
            final ArrayList list = new ArrayList();
            final Iterator<Entry<?, ?>> iterator = this.entries.iterator();
            boolean b = false;
            while (iterator.hasNext()) {
                final Entry entry = iterator.next();
                if (this.alreadyUsedEntries.contains(entry)) {
                    b = true;
                }
                else {
                    if (!entry.handles(clazz, clazz2)) {
                        continue;
                    }
                    this.alreadyUsedEntries.add(entry);
                    list.add(this.build(entry));
                    this.alreadyUsedEntries.remove(entry);
                }
            }
            if (list.size() > 1) {
                final MultiModelLoader<Object, Object> build = this.factory.build((List<ModelLoader<Object, Object>>)list, this.throwableListPool);
                monitorexit(this);
                return (ModelLoader<Model, Data>)build;
            }
            if (list.size() == 1) {
                final ModelLoader modelLoader = (ModelLoader)list.get(0);
                monitorexit(this);
                return modelLoader;
            }
            if (b) {
                final ModelLoader<Object, Object> emptyModelLoader = emptyModelLoader();
                monitorexit(this);
                return (ModelLoader<Model, Data>)emptyModelLoader;
            }
            throw new Registry.NoModelLoaderAvailableException(clazz, clazz2);
        }
        finally {
            try {
                this.alreadyUsedEntries.clear();
            }
            finally {
                monitorexit(this);
            }
        }
    }
    
     <Model> List<ModelLoader<Model, ?>> build(final Class<Model> clazz) {
        monitorenter(this);
        try {
            final ArrayList list = new ArrayList();
            for (final Entry entry : this.entries) {
                if (this.alreadyUsedEntries.contains(entry)) {
                    continue;
                }
                if (!entry.handles(clazz)) {
                    continue;
                }
                this.alreadyUsedEntries.add(entry);
                list.add(this.build(entry));
                this.alreadyUsedEntries.remove(entry);
            }
            monitorexit(this);
            return list;
        }
        finally {
            try {
                this.alreadyUsedEntries.clear();
            }
            finally {
                monitorexit(this);
            }
        }
    }
    
    List<Class<?>> getDataClasses(final Class<?> clazz) {
        synchronized (this) {
            final ArrayList list = new ArrayList();
            for (final Entry entry : this.entries) {
                if (!list.contains(entry.dataClass) && entry.handles(clazz)) {
                    list.add(entry.dataClass);
                }
            }
            return list;
        }
    }
    
     <Model, Data> void prepend(final Class<Model> clazz, final Class<Data> clazz2, final ModelLoaderFactory<? extends Model, ? extends Data> modelLoaderFactory) {
        synchronized (this) {
            this.add(clazz, clazz2, modelLoaderFactory, false);
        }
    }
    
     <Model, Data> List<ModelLoaderFactory<? extends Model, ? extends Data>> remove(final Class<Model> clazz, final Class<Data> clazz2) {
        synchronized (this) {
            final ArrayList list = new ArrayList();
            final Iterator<Entry<?, ?>> iterator = this.entries.iterator();
            while (iterator.hasNext()) {
                final Entry entry = iterator.next();
                if (entry.handles(clazz, clazz2)) {
                    iterator.remove();
                    list.add(this.getFactory(entry));
                }
            }
            return list;
        }
    }
    
     <Model, Data> List<ModelLoaderFactory<? extends Model, ? extends Data>> replace(final Class<Model> clazz, final Class<Data> clazz2, final ModelLoaderFactory<? extends Model, ? extends Data> modelLoaderFactory) {
        synchronized (this) {
            final List<ModelLoaderFactory<? extends Model, ? extends Data>> remove = this.remove(clazz, clazz2);
            this.append(clazz, clazz2, modelLoaderFactory);
            return remove;
        }
    }
    
    private static class EmptyModelLoader implements ModelLoader<Object, Object>
    {
        EmptyModelLoader() {
        }
        
        @Override
        public LoadData<Object> buildLoadData(final Object o, final int n, final int n2, final Options options) {
            return null;
        }
        
        @Override
        public boolean handles(final Object o) {
            return false;
        }
    }
    
    private static class Entry<Model, Data>
    {
        final Class<Data> dataClass;
        final ModelLoaderFactory<? extends Model, ? extends Data> factory;
        private final Class<Model> modelClass;
        
        public Entry(final Class<Model> modelClass, final Class<Data> dataClass, final ModelLoaderFactory<? extends Model, ? extends Data> factory) {
            this.modelClass = modelClass;
            this.dataClass = dataClass;
            this.factory = factory;
        }
        
        public boolean handles(final Class<?> clazz) {
            return this.modelClass.isAssignableFrom(clazz);
        }
        
        public boolean handles(final Class<?> clazz, final Class<?> clazz2) {
            return this.handles(clazz) && this.dataClass.isAssignableFrom(clazz2);
        }
    }
    
    static class Factory
    {
        public <Model, Data> MultiModelLoader<Model, Data> build(final List<ModelLoader<Model, Data>> list, final Pools.Pool<List<Throwable>> pool) {
            return new MultiModelLoader<Model, Data>(list, pool);
        }
    }
}

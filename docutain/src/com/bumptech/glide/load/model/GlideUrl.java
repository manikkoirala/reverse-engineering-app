// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide.load.model;

import java.security.MessageDigest;
import java.util.Map;
import java.net.MalformedURLException;
import android.net.Uri;
import android.text.TextUtils;
import com.bumptech.glide.util.Preconditions;
import java.net.URL;
import com.bumptech.glide.load.Key;

public class GlideUrl implements Key
{
    private static final String ALLOWED_URI_CHARS = "@#&=*+-_.,:!?()/~'%;$";
    private volatile byte[] cacheKeyBytes;
    private int hashCode;
    private final Headers headers;
    private String safeStringUrl;
    private URL safeUrl;
    private final String stringUrl;
    private final URL url;
    
    public GlideUrl(final String s) {
        this(s, Headers.DEFAULT);
    }
    
    public GlideUrl(final String s, final Headers headers) {
        this.url = null;
        this.stringUrl = Preconditions.checkNotEmpty(s);
        this.headers = Preconditions.checkNotNull(headers);
    }
    
    public GlideUrl(final URL url) {
        this(url, Headers.DEFAULT);
    }
    
    public GlideUrl(final URL url, final Headers headers) {
        this.url = Preconditions.checkNotNull(url);
        this.stringUrl = null;
        this.headers = Preconditions.checkNotNull(headers);
    }
    
    private byte[] getCacheKeyBytes() {
        if (this.cacheKeyBytes == null) {
            this.cacheKeyBytes = this.getCacheKey().getBytes(GlideUrl.CHARSET);
        }
        return this.cacheKeyBytes;
    }
    
    private String getSafeStringUrl() {
        if (TextUtils.isEmpty((CharSequence)this.safeStringUrl)) {
            String s;
            if (TextUtils.isEmpty((CharSequence)(s = this.stringUrl))) {
                s = Preconditions.checkNotNull(this.url).toString();
            }
            this.safeStringUrl = Uri.encode(s, "@#&=*+-_.,:!?()/~'%;$");
        }
        return this.safeStringUrl;
    }
    
    private URL getSafeUrl() throws MalformedURLException {
        if (this.safeUrl == null) {
            this.safeUrl = new URL(this.getSafeStringUrl());
        }
        return this.safeUrl;
    }
    
    @Override
    public boolean equals(final Object o) {
        final boolean b = o instanceof GlideUrl;
        boolean b3;
        final boolean b2 = b3 = false;
        if (b) {
            final GlideUrl glideUrl = (GlideUrl)o;
            b3 = b2;
            if (this.getCacheKey().equals(glideUrl.getCacheKey())) {
                b3 = b2;
                if (this.headers.equals(glideUrl.headers)) {
                    b3 = true;
                }
            }
        }
        return b3;
    }
    
    public String getCacheKey() {
        String s = this.stringUrl;
        if (s == null) {
            s = Preconditions.checkNotNull(this.url).toString();
        }
        return s;
    }
    
    public Map<String, String> getHeaders() {
        return this.headers.getHeaders();
    }
    
    @Override
    public int hashCode() {
        if (this.hashCode == 0) {
            final int hashCode = this.getCacheKey().hashCode();
            this.hashCode = hashCode;
            this.hashCode = hashCode * 31 + this.headers.hashCode();
        }
        return this.hashCode;
    }
    
    @Override
    public String toString() {
        return this.getCacheKey();
    }
    
    public String toStringUrl() {
        return this.getSafeStringUrl();
    }
    
    public URL toURL() throws MalformedURLException {
        return this.getSafeUrl();
    }
    
    @Override
    public void updateDiskCacheKey(final MessageDigest messageDigest) {
        messageDigest.update(this.getCacheKeyBytes());
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide.load.model.stream;

import com.bumptech.glide.Priority;
import com.bumptech.glide.load.DataSource;
import android.database.Cursor;
import android.text.TextUtils;
import java.io.FileNotFoundException;
import android.provider.MediaStore;
import android.os.Environment;
import java.io.InputStream;
import android.os.ParcelFileDescriptor;
import com.bumptech.glide.load.model.MultiModelLoaderFactory;
import com.bumptech.glide.load.model.ModelLoaderFactory;
import com.bumptech.glide.load.data.mediastore.MediaStoreUtil;
import android.os.Build$VERSION;
import com.bumptech.glide.load.data.DataFetcher;
import com.bumptech.glide.load.Key;
import com.bumptech.glide.signature.ObjectKey;
import com.bumptech.glide.load.Options;
import java.io.File;
import android.content.Context;
import android.net.Uri;
import com.bumptech.glide.load.model.ModelLoader;

public final class QMediaStoreUriLoader<DataT> implements ModelLoader<Uri, DataT>
{
    private final Context context;
    private final Class<DataT> dataClass;
    private final ModelLoader<File, DataT> fileDelegate;
    private final ModelLoader<Uri, DataT> uriDelegate;
    
    QMediaStoreUriLoader(final Context context, final ModelLoader<File, DataT> fileDelegate, final ModelLoader<Uri, DataT> uriDelegate, final Class<DataT> dataClass) {
        this.context = context.getApplicationContext();
        this.fileDelegate = fileDelegate;
        this.uriDelegate = uriDelegate;
        this.dataClass = dataClass;
    }
    
    public LoadData<DataT> buildLoadData(final Uri uri, final int n, final int n2, final Options options) {
        return (LoadData<DataT>)new LoadData(new ObjectKey(uri), new QMediaStoreUriFetcher<Object>(this.context, (ModelLoader<File, Object>)this.fileDelegate, (ModelLoader<Uri, Object>)this.uriDelegate, uri, n, n2, options, (Class<Object>)this.dataClass));
    }
    
    @Override
    public boolean handles(final Uri uri) {
        return Build$VERSION.SDK_INT >= 29 && MediaStoreUtil.isMediaStoreUri(uri);
    }
    
    private abstract static class Factory<DataT> implements ModelLoaderFactory<Uri, DataT>
    {
        private final Context context;
        private final Class<DataT> dataClass;
        
        Factory(final Context context, final Class<DataT> dataClass) {
            this.context = context;
            this.dataClass = dataClass;
        }
        
        @Override
        public final ModelLoader<Uri, DataT> build(final MultiModelLoaderFactory multiModelLoaderFactory) {
            return new QMediaStoreUriLoader<DataT>(this.context, (ModelLoader<File, DataT>)multiModelLoaderFactory.build(File.class, (Class<DataT>)this.dataClass), (ModelLoader<Uri, DataT>)multiModelLoaderFactory.build(Uri.class, (Class<DataT>)this.dataClass), this.dataClass);
        }
        
        @Override
        public final void teardown() {
        }
    }
    
    public static final class FileDescriptorFactory extends Factory<ParcelFileDescriptor>
    {
        public FileDescriptorFactory(final Context context) {
            super(context, ParcelFileDescriptor.class);
        }
    }
    
    public static final class InputStreamFactory extends Factory<InputStream>
    {
        public InputStreamFactory(final Context context) {
            super(context, InputStream.class);
        }
    }
    
    private static final class QMediaStoreUriFetcher<DataT> implements DataFetcher<DataT>
    {
        private static final String[] PROJECTION;
        private final Context context;
        private final Class<DataT> dataClass;
        private volatile DataFetcher<DataT> delegate;
        private final ModelLoader<File, DataT> fileDelegate;
        private final int height;
        private volatile boolean isCancelled;
        private final Options options;
        private final Uri uri;
        private final ModelLoader<Uri, DataT> uriDelegate;
        private final int width;
        
        static {
            PROJECTION = new String[] { "_data" };
        }
        
        QMediaStoreUriFetcher(final Context context, final ModelLoader<File, DataT> fileDelegate, final ModelLoader<Uri, DataT> uriDelegate, final Uri uri, final int width, final int height, final Options options, final Class<DataT> dataClass) {
            this.context = context.getApplicationContext();
            this.fileDelegate = fileDelegate;
            this.uriDelegate = uriDelegate;
            this.uri = uri;
            this.width = width;
            this.height = height;
            this.options = options;
            this.dataClass = dataClass;
        }
        
        private LoadData<DataT> buildDelegateData() throws FileNotFoundException {
            if (Environment.isExternalStorageLegacy()) {
                return this.fileDelegate.buildLoadData(this.queryForFilePath(this.uri), this.width, this.height, this.options);
            }
            Uri uri;
            if (this.isAccessMediaLocationGranted()) {
                uri = MediaStore.setRequireOriginal(this.uri);
            }
            else {
                uri = this.uri;
            }
            return this.uriDelegate.buildLoadData(uri, this.width, this.height, this.options);
        }
        
        private DataFetcher<DataT> buildDelegateFetcher() throws FileNotFoundException {
            final LoadData<DataT> buildDelegateData = this.buildDelegateData();
            DataFetcher<DataT> fetcher;
            if (buildDelegateData != null) {
                fetcher = buildDelegateData.fetcher;
            }
            else {
                fetcher = null;
            }
            return fetcher;
        }
        
        private boolean isAccessMediaLocationGranted() {
            return this.context.checkSelfPermission("android.permission.ACCESS_MEDIA_LOCATION") == 0;
        }
        
        private File queryForFilePath(final Uri uri) throws FileNotFoundException {
            Cursor cursor = null;
            try {
                final Cursor query = this.context.getContentResolver().query(uri, QMediaStoreUriFetcher.PROJECTION, (String)null, (String[])null, (String)null);
                if (query != null) {
                    cursor = query;
                    if (query.moveToFirst()) {
                        cursor = query;
                        final String string = query.getString(query.getColumnIndexOrThrow("_data"));
                        cursor = query;
                        if (!TextUtils.isEmpty((CharSequence)string)) {
                            cursor = query;
                            return new File(string);
                        }
                        cursor = query;
                        cursor = query;
                        cursor = query;
                        final StringBuilder sb = new StringBuilder();
                        cursor = query;
                        sb.append("File path was empty in media store for: ");
                        cursor = query;
                        sb.append(uri);
                        cursor = query;
                        final FileNotFoundException ex = new FileNotFoundException(sb.toString());
                        cursor = query;
                        throw ex;
                    }
                }
                cursor = query;
                cursor = query;
                cursor = query;
                final StringBuilder sb2 = new StringBuilder();
                cursor = query;
                sb2.append("Failed to media store entry for: ");
                cursor = query;
                sb2.append(uri);
                cursor = query;
                final FileNotFoundException ex2 = new FileNotFoundException(sb2.toString());
                cursor = query;
                throw ex2;
            }
            finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }
        
        @Override
        public void cancel() {
            this.isCancelled = true;
            final DataFetcher<DataT> delegate = this.delegate;
            if (delegate != null) {
                delegate.cancel();
            }
        }
        
        @Override
        public void cleanup() {
            final DataFetcher<DataT> delegate = this.delegate;
            if (delegate != null) {
                delegate.cleanup();
            }
        }
        
        @Override
        public Class<DataT> getDataClass() {
            return this.dataClass;
        }
        
        @Override
        public DataSource getDataSource() {
            return DataSource.LOCAL;
        }
        
        @Override
        public void loadData(final Priority priority, final DataCallback<? super DataT> dataCallback) {
            try {
                final DataFetcher<DataT> buildDelegateFetcher = this.buildDelegateFetcher();
                if (buildDelegateFetcher == null) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Failed to build fetcher for: ");
                    sb.append(this.uri);
                    dataCallback.onLoadFailed(new IllegalArgumentException(sb.toString()));
                    return;
                }
                this.delegate = buildDelegateFetcher;
                if (this.isCancelled) {
                    this.cancel();
                }
                else {
                    buildDelegateFetcher.loadData(priority, dataCallback);
                }
            }
            catch (final FileNotFoundException ex) {
                dataCallback.onLoadFailed(ex);
            }
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide.load.model.stream;

import com.bumptech.glide.load.model.GlideUrl;
import com.bumptech.glide.load.model.ModelLoader;
import java.io.InputStream;
import com.bumptech.glide.load.model.UrlUriLoader;

@Deprecated
public class HttpUriLoader extends UrlUriLoader<InputStream>
{
    public HttpUriLoader(final ModelLoader<GlideUrl, InputStream> modelLoader) {
        super(modelLoader);
    }
    
    @Deprecated
    public static class Factory extends StreamFactory
    {
    }
}

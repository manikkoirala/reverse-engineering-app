// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide.load.model.stream;

import com.bumptech.glide.load.model.Headers;
import java.util.Collections;
import com.bumptech.glide.load.data.DataFetcher;
import android.text.TextUtils;
import com.bumptech.glide.load.Options;
import java.util.Iterator;
import java.util.ArrayList;
import com.bumptech.glide.load.Key;
import java.util.List;
import java.util.Collection;
import com.bumptech.glide.load.model.ModelCache;
import com.bumptech.glide.load.model.GlideUrl;
import java.io.InputStream;
import com.bumptech.glide.load.model.ModelLoader;

public abstract class BaseGlideUrlLoader<Model> implements ModelLoader<Model, InputStream>
{
    private final ModelLoader<GlideUrl, InputStream> concreteLoader;
    private final ModelCache<Model, GlideUrl> modelCache;
    
    protected BaseGlideUrlLoader(final ModelLoader<GlideUrl, InputStream> modelLoader) {
        this(modelLoader, null);
    }
    
    protected BaseGlideUrlLoader(final ModelLoader<GlideUrl, InputStream> concreteLoader, final ModelCache<Model, GlideUrl> modelCache) {
        this.concreteLoader = concreteLoader;
        this.modelCache = modelCache;
    }
    
    private static List<Key> getAlternateKeys(final Collection<String> collection) {
        final ArrayList list = new ArrayList(collection.size());
        final Iterator iterator = collection.iterator();
        while (iterator.hasNext()) {
            list.add(new GlideUrl((String)iterator.next()));
        }
        return list;
    }
    
    @Override
    public LoadData<InputStream> buildLoadData(final Model model, final int n, final int n2, final Options options) {
        final ModelCache<Model, GlideUrl> modelCache = this.modelCache;
        GlideUrl glideUrl;
        if (modelCache != null) {
            glideUrl = modelCache.get(model, n, n2);
        }
        else {
            glideUrl = null;
        }
        GlideUrl glideUrl2 = glideUrl;
        if (glideUrl == null) {
            final String url = this.getUrl(model, n, n2, options);
            if (TextUtils.isEmpty((CharSequence)url)) {
                return null;
            }
            glideUrl2 = new GlideUrl(url, this.getHeaders(model, n, n2, options));
            final ModelCache<Model, GlideUrl> modelCache2 = this.modelCache;
            if (modelCache2 != null) {
                modelCache2.put(model, n, n2, glideUrl2);
            }
        }
        final List<String> alternateUrls = this.getAlternateUrls(model, n, n2, options);
        final LoadData<InputStream> buildLoadData = this.concreteLoader.buildLoadData(glideUrl2, n, n2, options);
        if (buildLoadData != null && !alternateUrls.isEmpty()) {
            return (LoadData<InputStream>)new LoadData(buildLoadData.sourceKey, getAlternateKeys(alternateUrls), (DataFetcher<Object>)buildLoadData.fetcher);
        }
        return buildLoadData;
    }
    
    protected List<String> getAlternateUrls(final Model model, final int n, final int n2, final Options options) {
        return Collections.emptyList();
    }
    
    protected Headers getHeaders(final Model model, final int n, final int n2, final Options options) {
        return Headers.DEFAULT;
    }
    
    protected abstract String getUrl(final Model p0, final int p1, final int p2, final Options p3);
}

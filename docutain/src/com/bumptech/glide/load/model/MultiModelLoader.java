// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide.load.model;

import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import java.util.Collection;
import com.bumptech.glide.util.Preconditions;
import com.bumptech.glide.Priority;
import java.util.Arrays;
import java.util.Iterator;
import com.bumptech.glide.load.Key;
import com.bumptech.glide.load.data.DataFetcher;
import java.util.ArrayList;
import com.bumptech.glide.load.Options;
import java.util.List;
import androidx.core.util.Pools;

class MultiModelLoader<Model, Data> implements ModelLoader<Model, Data>
{
    private final Pools.Pool<List<Throwable>> exceptionListPool;
    private final List<ModelLoader<Model, Data>> modelLoaders;
    
    MultiModelLoader(final List<ModelLoader<Model, Data>> modelLoaders, final Pools.Pool<List<Throwable>> exceptionListPool) {
        this.modelLoaders = modelLoaders;
        this.exceptionListPool = exceptionListPool;
    }
    
    @Override
    public LoadData<Data> buildLoadData(final Model model, final int n, final int n2, final Options options) {
        final int size = this.modelLoaders.size();
        final ArrayList list = new ArrayList(size);
        final LoadData<Data> loadData = null;
        int i = 0;
        Key key = null;
        while (i < size) {
            final ModelLoader modelLoader = this.modelLoaders.get(i);
            Key sourceKey = key;
            if (modelLoader.handles(model)) {
                final LoadData buildLoadData = modelLoader.buildLoadData(model, n, n2, options);
                sourceKey = key;
                if (buildLoadData != null) {
                    sourceKey = ((LoadData)buildLoadData).sourceKey;
                    list.add((Object)((LoadData)buildLoadData).fetcher);
                }
            }
            ++i;
            key = sourceKey;
        }
        Object o = loadData;
        if (!list.isEmpty()) {
            o = loadData;
            if (key != null) {
                o = new LoadData(key, new MultiFetcher<Object>((List<DataFetcher<Object>>)list, this.exceptionListPool));
            }
        }
        return (LoadData<Data>)o;
    }
    
    @Override
    public boolean handles(final Model model) {
        final Iterator<ModelLoader<Model, Data>> iterator = this.modelLoaders.iterator();
        while (iterator.hasNext()) {
            if (((ModelLoader<Model, Data>)iterator.next()).handles(model)) {
                return true;
            }
        }
        return false;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("MultiModelLoader{modelLoaders=");
        sb.append(Arrays.toString(this.modelLoaders.toArray()));
        sb.append('}');
        return sb.toString();
    }
    
    static class MultiFetcher<Data> implements DataFetcher<Data>, DataCallback<Data>
    {
        private DataCallback<? super Data> callback;
        private int currentIndex;
        private List<Throwable> exceptions;
        private final List<DataFetcher<Data>> fetchers;
        private boolean isCancelled;
        private Priority priority;
        private final Pools.Pool<List<Throwable>> throwableListPool;
        
        MultiFetcher(final List<DataFetcher<Data>> fetchers, final Pools.Pool<List<Throwable>> throwableListPool) {
            this.throwableListPool = throwableListPool;
            Preconditions.checkNotEmpty(fetchers);
            this.fetchers = fetchers;
            this.currentIndex = 0;
        }
        
        private void startNextOrFail() {
            if (this.isCancelled) {
                return;
            }
            if (this.currentIndex < this.fetchers.size() - 1) {
                ++this.currentIndex;
                this.loadData(this.priority, this.callback);
            }
            else {
                Preconditions.checkNotNull(this.exceptions);
                this.callback.onLoadFailed(new GlideException("Fetch failed", new ArrayList<Throwable>(this.exceptions)));
            }
        }
        
        @Override
        public void cancel() {
            this.isCancelled = true;
            final Iterator<DataFetcher<Data>> iterator = this.fetchers.iterator();
            while (iterator.hasNext()) {
                iterator.next().cancel();
            }
        }
        
        @Override
        public void cleanup() {
            final List<Throwable> exceptions = this.exceptions;
            if (exceptions != null) {
                this.throwableListPool.release(exceptions);
            }
            this.exceptions = null;
            final Iterator<DataFetcher<Data>> iterator = this.fetchers.iterator();
            while (iterator.hasNext()) {
                iterator.next().cleanup();
            }
        }
        
        @Override
        public Class<Data> getDataClass() {
            return this.fetchers.get(0).getDataClass();
        }
        
        @Override
        public DataSource getDataSource() {
            return this.fetchers.get(0).getDataSource();
        }
        
        @Override
        public void loadData(final Priority priority, final DataCallback<? super Data> callback) {
            this.priority = priority;
            this.callback = callback;
            this.exceptions = this.throwableListPool.acquire();
            this.fetchers.get(this.currentIndex).loadData(priority, (DataCallback)this);
            if (this.isCancelled) {
                this.cancel();
            }
        }
        
        @Override
        public void onDataReady(final Data data) {
            if (data != null) {
                this.callback.onDataReady(data);
            }
            else {
                this.startNextOrFail();
            }
        }
        
        @Override
        public void onLoadFailed(final Exception ex) {
            Preconditions.checkNotNull(this.exceptions).add(ex);
            this.startNextOrFail();
        }
    }
}

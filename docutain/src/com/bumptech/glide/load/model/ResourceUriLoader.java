// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide.load.model;

import java.util.List;
import android.util.Log;
import com.bumptech.glide.load.Options;
import java.io.InputStream;
import android.content.res.AssetFileDescriptor;
import android.content.Context;
import android.net.Uri;

public final class ResourceUriLoader<DataT> implements ModelLoader<Uri, DataT>
{
    private static final int INVALID_RESOURCE_ID = 0;
    private static final String TAG = "ResourceUriLoader";
    private final Context context;
    private final ModelLoader<Integer, DataT> delegate;
    
    ResourceUriLoader(final Context context, final ModelLoader<Integer, DataT> delegate) {
        this.context = context.getApplicationContext();
        this.delegate = delegate;
    }
    
    public static ModelLoaderFactory<Uri, AssetFileDescriptor> newAssetFileDescriptorFactory(final Context context) {
        return new AssetFileDescriptorFactory(context);
    }
    
    public static ModelLoaderFactory<Uri, InputStream> newStreamFactory(final Context context) {
        return new InputStreamFactory(context);
    }
    
    private LoadData<DataT> parseResourceIdUri(final Uri uri, final int n, final int n2, final Options options) {
        try {
            final int int1 = Integer.parseInt(uri.getPathSegments().get(0));
            if (int1 == 0) {
                if (Log.isLoggable("ResourceUriLoader", 5)) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Failed to parse a valid non-0 resource id from: ");
                    sb.append(uri);
                    Log.w("ResourceUriLoader", sb.toString());
                }
                return null;
            }
            return this.delegate.buildLoadData(int1, n, n2, options);
        }
        catch (final NumberFormatException ex) {
            if (Log.isLoggable("ResourceUriLoader", 5)) {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("Failed to parse resource id from: ");
                sb2.append(uri);
                Log.w("ResourceUriLoader", sb2.toString(), (Throwable)ex);
            }
            return null;
        }
    }
    
    private LoadData<DataT> parseResourceNameUri(final Uri obj, final int n, final int n2, final Options options) {
        final List pathSegments = obj.getPathSegments();
        final int identifier = this.context.getResources().getIdentifier((String)pathSegments.get(1), (String)pathSegments.get(0), this.context.getPackageName());
        if (identifier == 0) {
            if (Log.isLoggable("ResourceUriLoader", 5)) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Failed to find resource id for: ");
                sb.append(obj);
                Log.w("ResourceUriLoader", sb.toString());
            }
            return null;
        }
        return this.delegate.buildLoadData(identifier, n, n2, options);
    }
    
    public LoadData<DataT> buildLoadData(final Uri obj, final int n, final int n2, final Options options) {
        final List pathSegments = obj.getPathSegments();
        if (pathSegments.size() == 1) {
            return this.parseResourceIdUri(obj, n, n2, options);
        }
        if (pathSegments.size() == 2) {
            return this.parseResourceNameUri(obj, n, n2, options);
        }
        if (Log.isLoggable("ResourceUriLoader", 5)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Failed to parse resource uri: ");
            sb.append(obj);
            Log.w("ResourceUriLoader", sb.toString());
        }
        return null;
    }
    
    @Override
    public boolean handles(final Uri uri) {
        return "android.resource".equals(uri.getScheme()) && this.context.getPackageName().equals(uri.getAuthority());
    }
    
    private static final class AssetFileDescriptorFactory implements ModelLoaderFactory<Uri, AssetFileDescriptor>
    {
        private final Context context;
        
        AssetFileDescriptorFactory(final Context context) {
            this.context = context;
        }
        
        @Override
        public ModelLoader<Uri, AssetFileDescriptor> build(final MultiModelLoaderFactory multiModelLoaderFactory) {
            return new ResourceUriLoader<AssetFileDescriptor>(this.context, (ModelLoader<Integer, AssetFileDescriptor>)multiModelLoaderFactory.build(Integer.class, (Class<DataT>)AssetFileDescriptor.class));
        }
        
        @Override
        public void teardown() {
        }
    }
    
    private static final class InputStreamFactory implements ModelLoaderFactory<Uri, InputStream>
    {
        private final Context context;
        
        InputStreamFactory(final Context context) {
            this.context = context;
        }
        
        @Override
        public ModelLoader<Uri, InputStream> build(final MultiModelLoaderFactory multiModelLoaderFactory) {
            return new ResourceUriLoader<InputStream>(this.context, (ModelLoader<Integer, InputStream>)multiModelLoaderFactory.build(Integer.class, (Class<DataT>)InputStream.class));
        }
        
        @Override
        public void teardown() {
        }
    }
}

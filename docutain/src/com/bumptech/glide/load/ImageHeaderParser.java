// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide.load;

import java.nio.ByteBuffer;
import java.io.IOException;
import com.bumptech.glide.load.engine.bitmap_recycle.ArrayPool;
import java.io.InputStream;

public interface ImageHeaderParser
{
    public static final int UNKNOWN_ORIENTATION = -1;
    
    int getOrientation(final InputStream p0, final ArrayPool p1) throws IOException;
    
    int getOrientation(final ByteBuffer p0, final ArrayPool p1) throws IOException;
    
    ImageType getType(final InputStream p0) throws IOException;
    
    ImageType getType(final ByteBuffer p0) throws IOException;
    
    public enum ImageType
    {
        private static final ImageType[] $VALUES;
        
        ANIMATED_AVIF(true), 
        ANIMATED_WEBP(true), 
        AVIF(true), 
        GIF(true), 
        JPEG(false), 
        PNG(false), 
        PNG_A(true), 
        RAW(false), 
        UNKNOWN(false), 
        WEBP(false), 
        WEBP_A(true);
        
        private final boolean hasAlpha;
        
        private ImageType(final boolean hasAlpha) {
            this.hasAlpha = hasAlpha;
        }
        
        public boolean hasAlpha() {
            return this.hasAlpha;
        }
        
        public boolean isWebp() {
            final int n = ImageHeaderParser$1.$SwitchMap$com$bumptech$glide$load$ImageHeaderParser$ImageType[this.ordinal()];
            return n == 1 || n == 2 || n == 3;
        }
    }
}

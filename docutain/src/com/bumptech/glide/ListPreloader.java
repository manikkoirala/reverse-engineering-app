// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide;

import com.bumptech.glide.util.Util;
import java.util.Queue;
import com.bumptech.glide.request.transition.Transition;
import android.graphics.drawable.Drawable;
import com.bumptech.glide.request.target.SizeReadyCallback;
import com.bumptech.glide.request.Request;
import android.widget.AbsListView;
import java.util.List;
import com.bumptech.glide.request.target.Target;
import android.widget.AbsListView$OnScrollListener;

public class ListPreloader<T> implements AbsListView$OnScrollListener
{
    private boolean isIncreasing;
    private int lastEnd;
    private int lastFirstVisible;
    private int lastStart;
    private final int maxPreload;
    private final PreloadSizeProvider<T> preloadDimensionProvider;
    private final PreloadModelProvider<T> preloadModelProvider;
    private final PreloadTargetQueue preloadTargetQueue;
    private final RequestManager requestManager;
    private int totalItemCount;
    
    public ListPreloader(final RequestManager requestManager, final PreloadModelProvider<T> preloadModelProvider, final PreloadSizeProvider<T> preloadDimensionProvider, final int maxPreload) {
        this.lastFirstVisible = -1;
        this.isIncreasing = true;
        this.requestManager = requestManager;
        this.preloadModelProvider = preloadModelProvider;
        this.preloadDimensionProvider = preloadDimensionProvider;
        this.maxPreload = maxPreload;
        this.preloadTargetQueue = new PreloadTargetQueue(maxPreload + 1);
    }
    
    private void cancelAll() {
        for (int i = 0; i < this.preloadTargetQueue.queue.size(); ++i) {
            this.requestManager.clear(this.preloadTargetQueue.next(0, 0));
        }
    }
    
    private void preload(int i, final int n) {
        int max;
        int min;
        if (i < n) {
            max = Math.max(this.lastEnd, i);
            min = n;
        }
        else {
            min = Math.min(this.lastStart, i);
            max = n;
        }
        final int min2 = Math.min(this.totalItemCount, min);
        final int min3 = Math.min(this.totalItemCount, Math.max(0, max));
        if (i < n) {
            for (i = min3; i < min2; ++i) {
                this.preloadAdapterPosition(this.preloadModelProvider.getPreloadItems(i), i, true);
            }
        }
        else {
            for (i = min2 - 1; i >= min3; --i) {
                this.preloadAdapterPosition(this.preloadModelProvider.getPreloadItems(i), i, false);
            }
        }
        this.lastStart = min3;
        this.lastEnd = min2;
    }
    
    private void preload(final int n, final boolean isIncreasing) {
        if (this.isIncreasing != isIncreasing) {
            this.isIncreasing = isIncreasing;
            this.cancelAll();
        }
        int maxPreload;
        if (isIncreasing) {
            maxPreload = this.maxPreload;
        }
        else {
            maxPreload = -this.maxPreload;
        }
        this.preload(n, maxPreload + n);
    }
    
    private void preloadAdapterPosition(final List<T> list, final int n, final boolean b) {
        final int size = list.size();
        if (b) {
            for (int i = 0; i < size; ++i) {
                this.preloadItem((T)list.get(i), n, i);
            }
        }
        else {
            for (int j = size - 1; j >= 0; --j) {
                this.preloadItem((T)list.get(j), n, j);
            }
        }
    }
    
    private void preloadItem(final T t, final int n, final int n2) {
        if (t == null) {
            return;
        }
        final int[] preloadSize = this.preloadDimensionProvider.getPreloadSize(t, n, n2);
        if (preloadSize == null) {
            return;
        }
        final RequestBuilder<?> preloadRequestBuilder = this.preloadModelProvider.getPreloadRequestBuilder(t);
        if (preloadRequestBuilder == null) {
            return;
        }
        preloadRequestBuilder.into(this.preloadTargetQueue.next(preloadSize[0], preloadSize[1]));
    }
    
    public void onScroll(final AbsListView absListView, final int lastFirstVisible, final int n, int lastFirstVisible2) {
        if (this.totalItemCount == 0 && lastFirstVisible2 == 0) {
            return;
        }
        this.totalItemCount = lastFirstVisible2;
        lastFirstVisible2 = this.lastFirstVisible;
        if (lastFirstVisible > lastFirstVisible2) {
            this.preload(n + lastFirstVisible, true);
        }
        else if (lastFirstVisible < lastFirstVisible2) {
            this.preload(lastFirstVisible, false);
        }
        this.lastFirstVisible = lastFirstVisible;
    }
    
    public void onScrollStateChanged(final AbsListView absListView, final int n) {
    }
    
    public interface PreloadModelProvider<U>
    {
        List<U> getPreloadItems(final int p0);
        
        RequestBuilder<?> getPreloadRequestBuilder(final U p0);
    }
    
    public interface PreloadSizeProvider<T>
    {
        int[] getPreloadSize(final T p0, final int p1, final int p2);
    }
    
    private static final class PreloadTarget implements Target<Object>
    {
        int photoHeight;
        int photoWidth;
        private Request request;
        
        PreloadTarget() {
        }
        
        @Override
        public Request getRequest() {
            return this.request;
        }
        
        @Override
        public void getSize(final SizeReadyCallback sizeReadyCallback) {
            sizeReadyCallback.onSizeReady(this.photoWidth, this.photoHeight);
        }
        
        @Override
        public void onDestroy() {
        }
        
        @Override
        public void onLoadCleared(final Drawable drawable) {
        }
        
        @Override
        public void onLoadFailed(final Drawable drawable) {
        }
        
        @Override
        public void onLoadStarted(final Drawable drawable) {
        }
        
        @Override
        public void onResourceReady(final Object o, final Transition<? super Object> transition) {
        }
        
        @Override
        public void onStart() {
        }
        
        @Override
        public void onStop() {
        }
        
        @Override
        public void removeCallback(final SizeReadyCallback sizeReadyCallback) {
        }
        
        @Override
        public void setRequest(final Request request) {
            this.request = request;
        }
    }
    
    private static final class PreloadTargetQueue
    {
        final Queue<PreloadTarget> queue;
        
        PreloadTargetQueue(final int n) {
            this.queue = Util.createQueue(n);
            for (int i = 0; i < n; ++i) {
                this.queue.offer(new PreloadTarget());
            }
        }
        
        public PreloadTarget next(final int photoWidth, final int photoHeight) {
            final PreloadTarget preloadTarget = this.queue.poll();
            this.queue.offer(preloadTarget);
            preloadTarget.photoWidth = photoWidth;
            preloadTarget.photoHeight = photoHeight;
            return preloadTarget;
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide.signature;

import java.nio.ByteBuffer;
import java.security.MessageDigest;
import com.bumptech.glide.load.Key;

public class MediaStoreSignature implements Key
{
    private final long dateModified;
    private final String mimeType;
    private final int orientation;
    
    public MediaStoreSignature(final String s, final long dateModified, final int orientation) {
        String mimeType = s;
        if (s == null) {
            mimeType = "";
        }
        this.mimeType = mimeType;
        this.dateModified = dateModified;
        this.orientation = orientation;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o != null && this.getClass() == o.getClass()) {
            final MediaStoreSignature mediaStoreSignature = (MediaStoreSignature)o;
            return this.dateModified == mediaStoreSignature.dateModified && this.orientation == mediaStoreSignature.orientation && this.mimeType.equals(mediaStoreSignature.mimeType);
        }
        return false;
    }
    
    @Override
    public int hashCode() {
        final int hashCode = this.mimeType.hashCode();
        final long dateModified = this.dateModified;
        return (hashCode * 31 + (int)(dateModified ^ dateModified >>> 32)) * 31 + this.orientation;
    }
    
    @Override
    public void updateDiskCacheKey(final MessageDigest messageDigest) {
        messageDigest.update(ByteBuffer.allocate(12).putLong(this.dateModified).putInt(this.orientation).array());
        messageDigest.update(this.mimeType.getBytes(MediaStoreSignature.CHARSET));
    }
}

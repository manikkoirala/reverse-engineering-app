// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide.signature;

import java.util.UUID;
import android.content.pm.PackageManager$NameNotFoundException;
import android.util.Log;
import android.content.pm.PackageInfo;
import android.content.Context;
import java.util.concurrent.ConcurrentHashMap;
import com.bumptech.glide.load.Key;
import java.util.concurrent.ConcurrentMap;

public final class ApplicationVersionSignature
{
    private static final ConcurrentMap<String, Key> PACKAGE_NAME_TO_KEY;
    private static final String TAG = "AppVersionSignature";
    
    static {
        PACKAGE_NAME_TO_KEY = new ConcurrentHashMap<String, Key>();
    }
    
    private ApplicationVersionSignature() {
    }
    
    private static PackageInfo getPackageInfo(final Context context) {
        try {
            return context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
        }
        catch (final PackageManager$NameNotFoundException ex) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Cannot resolve info for");
            sb.append(context.getPackageName());
            Log.e("AppVersionSignature", sb.toString(), (Throwable)ex);
            return null;
        }
    }
    
    private static String getVersionCode(final PackageInfo packageInfo) {
        String s;
        if (packageInfo != null) {
            s = String.valueOf(packageInfo.versionCode);
        }
        else {
            s = UUID.randomUUID().toString();
        }
        return s;
    }
    
    public static Key obtain(final Context context) {
        final String packageName = context.getPackageName();
        final ConcurrentMap<String, Key> package_NAME_TO_KEY = ApplicationVersionSignature.PACKAGE_NAME_TO_KEY;
        Key obtainVersionSignature;
        if ((obtainVersionSignature = package_NAME_TO_KEY.get(packageName)) == null) {
            obtainVersionSignature = obtainVersionSignature(context);
            final Key key = package_NAME_TO_KEY.putIfAbsent(packageName, obtainVersionSignature);
            if (key != null) {
                obtainVersionSignature = key;
            }
        }
        return obtainVersionSignature;
    }
    
    private static Key obtainVersionSignature(final Context context) {
        return new ObjectKey(getVersionCode(getPackageInfo(context)));
    }
    
    static void reset() {
        ApplicationVersionSignature.PACKAGE_NAME_TO_KEY.clear();
    }
}

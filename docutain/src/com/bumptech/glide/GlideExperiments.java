// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class GlideExperiments
{
    private final Map<Class<?>, Experiment> experiments;
    
    GlideExperiments(final Builder builder) {
        this.experiments = Collections.unmodifiableMap((Map<? extends Class<?>, ? extends Experiment>)new HashMap<Class<?>, Experiment>(builder.experiments));
    }
    
     <T extends Experiment> T get(final Class<T> clazz) {
        return (T)this.experiments.get(clazz);
    }
    
    public boolean isEnabled(final Class<? extends Experiment> clazz) {
        return this.experiments.containsKey(clazz);
    }
    
    static final class Builder
    {
        private final Map<Class<?>, Experiment> experiments;
        
        Builder() {
            this.experiments = new HashMap<Class<?>, Experiment>();
        }
        
        Builder add(final Experiment experiment) {
            this.experiments.put(experiment.getClass(), experiment);
            return this;
        }
        
        GlideExperiments build() {
            return new GlideExperiments(this);
        }
        
        Builder update(final Experiment experiment, final boolean b) {
            if (b) {
                this.add(experiment);
            }
            else {
                this.experiments.remove(experiment.getClass());
            }
            return this;
        }
    }
    
    interface Experiment
    {
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide.module;

import java.util.Iterator;
import java.util.ArrayList;
import android.util.Log;
import java.util.List;
import java.lang.reflect.InvocationTargetException;
import android.content.pm.PackageManager$NameNotFoundException;
import android.content.pm.ApplicationInfo;
import android.content.Context;

@Deprecated
public final class ManifestParser
{
    private static final String GLIDE_MODULE_VALUE = "GlideModule";
    private static final String TAG = "ManifestParser";
    private final Context context;
    
    public ManifestParser(final Context context) {
        this.context = context;
    }
    
    private ApplicationInfo getOurApplicationInfo() throws PackageManager$NameNotFoundException {
        return this.context.getPackageManager().getApplicationInfo(this.context.getPackageName(), 128);
    }
    
    private static GlideModule parseModule(String instance) {
        try {
            final Class<?> forName = Class.forName(instance);
            instance = null;
            try {
                instance = (String)forName.getDeclaredConstructor((Class<?>[])new Class[0]).newInstance(new Object[0]);
            }
            catch (final InvocationTargetException ex) {
                throwInstantiateGlideModuleException(forName, ex);
            }
            catch (final NoSuchMethodException ex2) {
                throwInstantiateGlideModuleException(forName, ex2);
            }
            catch (final IllegalAccessException ex3) {
                throwInstantiateGlideModuleException(forName, ex3);
            }
            catch (final InstantiationException ex4) {
                throwInstantiateGlideModuleException(forName, ex4);
            }
            if (instance instanceof GlideModule) {
                return (GlideModule)instance;
            }
            final StringBuilder sb = new StringBuilder();
            sb.append("Expected instanceof GlideModule, but found: ");
            sb.append((Object)instance);
            throw new RuntimeException(sb.toString());
        }
        catch (final ClassNotFoundException cause) {
            throw new IllegalArgumentException("Unable to find GlideModule implementation", cause);
        }
    }
    
    private static void throwInstantiateGlideModuleException(final Class<?> obj, final Exception cause) {
        final StringBuilder sb = new StringBuilder();
        sb.append("Unable to instantiate GlideModule implementation for ");
        sb.append(obj);
        throw new RuntimeException(sb.toString(), cause);
    }
    
    public List<GlideModule> parse() {
        if (Log.isLoggable("ManifestParser", 3)) {
            Log.d("ManifestParser", "Loading Glide modules");
        }
        final ArrayList list = new ArrayList();
        try {
            final ApplicationInfo ourApplicationInfo = this.getOurApplicationInfo();
            if (ourApplicationInfo != null && ourApplicationInfo.metaData != null) {
                if (Log.isLoggable("ManifestParser", 2)) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Got app info metadata: ");
                    sb.append(ourApplicationInfo.metaData);
                    Log.v("ManifestParser", sb.toString());
                }
                for (final String str : ourApplicationInfo.metaData.keySet()) {
                    if ("GlideModule".equals(ourApplicationInfo.metaData.get(str))) {
                        list.add(parseModule(str));
                        if (!Log.isLoggable("ManifestParser", 3)) {
                            continue;
                        }
                        final StringBuilder sb2 = new StringBuilder();
                        sb2.append("Loaded Glide module: ");
                        sb2.append(str);
                        Log.d("ManifestParser", sb2.toString());
                    }
                }
                if (Log.isLoggable("ManifestParser", 3)) {
                    Log.d("ManifestParser", "Finished loading Glide modules");
                }
                return list;
            }
            if (Log.isLoggable("ManifestParser", 3)) {
                Log.d("ManifestParser", "Got null app info metadata");
            }
            return list;
        }
        catch (final PackageManager$NameNotFoundException cause) {
            throw new RuntimeException("Unable to find metadata to parse GlideModules", (Throwable)cause);
        }
    }
}

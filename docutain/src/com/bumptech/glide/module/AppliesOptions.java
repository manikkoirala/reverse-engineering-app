// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide.module;

import com.bumptech.glide.GlideBuilder;
import android.content.Context;

@Deprecated
interface AppliesOptions
{
    void applyOptions(final Context p0, final GlideBuilder p1);
}

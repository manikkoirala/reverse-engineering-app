// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide.module;

import com.bumptech.glide.Registry;
import com.bumptech.glide.Glide;
import android.content.Context;

@Deprecated
interface RegistersComponents
{
    void registerComponents(final Context p0, final Glide p1, final Registry p2);
}

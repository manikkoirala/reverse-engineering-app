// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide;

import com.bumptech.glide.request.transition.ViewPropertyTransition;
import com.bumptech.glide.request.transition.TransitionFactory;

public final class GenericTransitionOptions<TranscodeType> extends TransitionOptions<GenericTransitionOptions<TranscodeType>, TranscodeType>
{
    public static <TranscodeType> GenericTransitionOptions<TranscodeType> with(final int n) {
        return (GenericTransitionOptions)new GenericTransitionOptions().transition(n);
    }
    
    public static <TranscodeType> GenericTransitionOptions<TranscodeType> with(final TransitionFactory<? super TranscodeType> transitionFactory) {
        return (GenericTransitionOptions)new GenericTransitionOptions().transition((TransitionFactory<? super Object>)transitionFactory);
    }
    
    public static <TranscodeType> GenericTransitionOptions<TranscodeType> with(final ViewPropertyTransition.Animator animator) {
        return (GenericTransitionOptions)new GenericTransitionOptions().transition(animator);
    }
    
    public static <TranscodeType> GenericTransitionOptions<TranscodeType> withNoTransition() {
        return (GenericTransitionOptions)new GenericTransitionOptions().dontTransition();
    }
    
    @Override
    public boolean equals(final Object o) {
        return o instanceof GenericTransitionOptions && super.equals(o);
    }
    
    @Override
    public int hashCode() {
        return super.hashCode();
    }
}

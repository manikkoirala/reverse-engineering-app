// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide;

import com.bumptech.glide.request.transition.ViewPropertyAnimationFactory;
import com.bumptech.glide.request.transition.ViewPropertyTransition;
import com.bumptech.glide.util.Preconditions;
import com.bumptech.glide.request.transition.ViewAnimationFactory;
import com.bumptech.glide.util.Util;
import com.bumptech.glide.request.transition.NoTransition;
import com.bumptech.glide.request.transition.TransitionFactory;

public abstract class TransitionOptions<CHILD extends TransitionOptions<CHILD, TranscodeType>, TranscodeType> implements Cloneable
{
    private TransitionFactory<? super TranscodeType> transitionFactory;
    
    public TransitionOptions() {
        this.transitionFactory = NoTransition.getFactory();
    }
    
    private CHILD self() {
        return (CHILD)this;
    }
    
    public final CHILD clone() {
        try {
            return (CHILD)super.clone();
        }
        catch (final CloneNotSupportedException cause) {
            throw new RuntimeException(cause);
        }
    }
    
    public final CHILD dontTransition() {
        return this.transition(NoTransition.getFactory());
    }
    
    @Override
    public boolean equals(final Object o) {
        return o instanceof TransitionOptions && Util.bothNullOrEqual(this.transitionFactory, ((TransitionOptions)o).transitionFactory);
    }
    
    final TransitionFactory<? super TranscodeType> getTransitionFactory() {
        return this.transitionFactory;
    }
    
    @Override
    public int hashCode() {
        final TransitionFactory<? super TranscodeType> transitionFactory = this.transitionFactory;
        int hashCode;
        if (transitionFactory != null) {
            hashCode = transitionFactory.hashCode();
        }
        else {
            hashCode = 0;
        }
        return hashCode;
    }
    
    public final CHILD transition(final int n) {
        return this.transition(new ViewAnimationFactory<Object>(n));
    }
    
    public final CHILD transition(final TransitionFactory<? super TranscodeType> transitionFactory) {
        this.transitionFactory = Preconditions.checkNotNull(transitionFactory);
        return this.self();
    }
    
    public final CHILD transition(final ViewPropertyTransition.Animator animator) {
        return this.transition(new ViewPropertyAnimationFactory<Object>(animator));
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide.request;

public interface Request
{
    void begin();
    
    void clear();
    
    boolean isAnyResourceSet();
    
    boolean isCleared();
    
    boolean isComplete();
    
    boolean isEquivalentTo(final Request p0);
    
    boolean isRunning();
    
    void pause();
}

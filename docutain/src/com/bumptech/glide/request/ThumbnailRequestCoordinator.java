// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide.request;

public class ThumbnailRequestCoordinator implements RequestCoordinator, Request
{
    private volatile Request full;
    private RequestState fullState;
    private boolean isRunningDuringBegin;
    private final RequestCoordinator parent;
    private final Object requestLock;
    private volatile Request thumb;
    private RequestState thumbState;
    
    public ThumbnailRequestCoordinator(final Object requestLock, final RequestCoordinator parent) {
        this.fullState = RequestState.CLEARED;
        this.thumbState = RequestState.CLEARED;
        this.requestLock = requestLock;
        this.parent = parent;
    }
    
    private boolean parentCanNotifyCleared() {
        final RequestCoordinator parent = this.parent;
        return parent == null || parent.canNotifyCleared(this);
    }
    
    private boolean parentCanNotifyStatusChanged() {
        final RequestCoordinator parent = this.parent;
        return parent == null || parent.canNotifyStatusChanged(this);
    }
    
    private boolean parentCanSetImage() {
        final RequestCoordinator parent = this.parent;
        return parent == null || parent.canSetImage(this);
    }
    
    @Override
    public void begin() {
        synchronized (this.requestLock) {
            this.isRunningDuringBegin = true;
            try {
                if (this.fullState != RequestState.SUCCESS && this.thumbState != RequestState.RUNNING) {
                    this.thumbState = RequestState.RUNNING;
                    this.thumb.begin();
                }
                if (this.isRunningDuringBegin && this.fullState != RequestState.RUNNING) {
                    this.fullState = RequestState.RUNNING;
                    this.full.begin();
                }
            }
            finally {
                this.isRunningDuringBegin = false;
            }
        }
    }
    
    @Override
    public boolean canNotifyCleared(final Request request) {
        synchronized (this.requestLock) {
            return this.parentCanNotifyCleared() && request.equals(this.full) && this.fullState != RequestState.PAUSED;
        }
    }
    
    @Override
    public boolean canNotifyStatusChanged(final Request request) {
        synchronized (this.requestLock) {
            return this.parentCanNotifyStatusChanged() && request.equals(this.full) && !this.isAnyResourceSet();
        }
    }
    
    @Override
    public boolean canSetImage(final Request request) {
        synchronized (this.requestLock) {
            return this.parentCanSetImage() && (request.equals(this.full) || this.fullState != RequestState.SUCCESS);
        }
    }
    
    @Override
    public void clear() {
        synchronized (this.requestLock) {
            this.isRunningDuringBegin = false;
            this.fullState = RequestState.CLEARED;
            this.thumbState = RequestState.CLEARED;
            this.thumb.clear();
            this.full.clear();
        }
    }
    
    @Override
    public RequestCoordinator getRoot() {
        synchronized (this.requestLock) {
            final RequestCoordinator parent = this.parent;
            RequestCoordinator root;
            if (parent != null) {
                root = parent.getRoot();
            }
            else {
                root = this;
            }
            return root;
        }
    }
    
    @Override
    public boolean isAnyResourceSet() {
        synchronized (this.requestLock) {
            return this.thumb.isAnyResourceSet() || this.full.isAnyResourceSet();
        }
    }
    
    @Override
    public boolean isCleared() {
        synchronized (this.requestLock) {
            return this.fullState == RequestState.CLEARED;
        }
    }
    
    @Override
    public boolean isComplete() {
        synchronized (this.requestLock) {
            return this.fullState == RequestState.SUCCESS;
        }
    }
    
    @Override
    public boolean isEquivalentTo(final Request request) {
        final boolean b = request instanceof ThumbnailRequestCoordinator;
        boolean b3;
        final boolean b2 = b3 = false;
        if (b) {
            final ThumbnailRequestCoordinator thumbnailRequestCoordinator = (ThumbnailRequestCoordinator)request;
            if (this.full == null) {
                b3 = b2;
                if (thumbnailRequestCoordinator.full != null) {
                    return b3;
                }
            }
            else {
                b3 = b2;
                if (!this.full.isEquivalentTo(thumbnailRequestCoordinator.full)) {
                    return b3;
                }
            }
            if (this.thumb == null) {
                b3 = b2;
                if (thumbnailRequestCoordinator.thumb != null) {
                    return b3;
                }
            }
            else {
                b3 = b2;
                if (!this.thumb.isEquivalentTo(thumbnailRequestCoordinator.thumb)) {
                    return b3;
                }
            }
            b3 = true;
        }
        return b3;
    }
    
    @Override
    public boolean isRunning() {
        synchronized (this.requestLock) {
            return this.fullState == RequestState.RUNNING;
        }
    }
    
    @Override
    public void onRequestFailed(final Request request) {
        synchronized (this.requestLock) {
            if (!request.equals(this.full)) {
                this.thumbState = RequestState.FAILED;
                return;
            }
            this.fullState = RequestState.FAILED;
            final RequestCoordinator parent = this.parent;
            if (parent != null) {
                parent.onRequestFailed(this);
            }
        }
    }
    
    @Override
    public void onRequestSuccess(final Request request) {
        synchronized (this.requestLock) {
            if (request.equals(this.thumb)) {
                this.thumbState = RequestState.SUCCESS;
                return;
            }
            this.fullState = RequestState.SUCCESS;
            final RequestCoordinator parent = this.parent;
            if (parent != null) {
                parent.onRequestSuccess(this);
            }
            if (!this.thumbState.isComplete()) {
                this.thumb.clear();
            }
        }
    }
    
    @Override
    public void pause() {
        synchronized (this.requestLock) {
            if (!this.thumbState.isComplete()) {
                this.thumbState = RequestState.PAUSED;
                this.thumb.pause();
            }
            if (!this.fullState.isComplete()) {
                this.fullState = RequestState.PAUSED;
                this.full.pause();
            }
        }
    }
    
    public void setRequests(final Request full, final Request thumb) {
        this.full = full;
        this.thumb = thumb;
    }
}

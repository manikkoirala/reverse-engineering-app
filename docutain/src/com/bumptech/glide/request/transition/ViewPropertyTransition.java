// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide.request.transition;

import android.view.View;

public class ViewPropertyTransition<R> implements Transition<R>
{
    private final Animator animator;
    
    public ViewPropertyTransition(final Animator animator) {
        this.animator = animator;
    }
    
    @Override
    public boolean transition(final R r, final ViewAdapter viewAdapter) {
        if (viewAdapter.getView() != null) {
            this.animator.animate(viewAdapter.getView());
        }
        return false;
    }
    
    public interface Animator
    {
        void animate(final View p0);
    }
}

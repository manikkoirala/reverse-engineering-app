// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide.request.transition;

import android.view.animation.AnimationUtils;
import android.content.Context;
import com.bumptech.glide.load.DataSource;
import android.view.animation.Animation;

public class ViewAnimationFactory<R> implements TransitionFactory<R>
{
    private Transition<R> transition;
    private final ViewTransition.ViewTransitionAnimationFactory viewTransitionAnimationFactory;
    
    public ViewAnimationFactory(final int n) {
        this(new ResourceViewTransitionAnimationFactory(n));
    }
    
    public ViewAnimationFactory(final Animation animation) {
        this(new ConcreteViewTransitionAnimationFactory(animation));
    }
    
    ViewAnimationFactory(final ViewTransition.ViewTransitionAnimationFactory viewTransitionAnimationFactory) {
        this.viewTransitionAnimationFactory = viewTransitionAnimationFactory;
    }
    
    @Override
    public Transition<R> build(final DataSource dataSource, final boolean b) {
        if (dataSource != DataSource.MEMORY_CACHE && b) {
            if (this.transition == null) {
                this.transition = new ViewTransition<R>(this.viewTransitionAnimationFactory);
            }
            return this.transition;
        }
        return NoTransition.get();
    }
    
    private static class ConcreteViewTransitionAnimationFactory implements ViewTransitionAnimationFactory
    {
        private final Animation animation;
        
        ConcreteViewTransitionAnimationFactory(final Animation animation) {
            this.animation = animation;
        }
        
        @Override
        public Animation build(final Context context) {
            return this.animation;
        }
    }
    
    private static class ResourceViewTransitionAnimationFactory implements ViewTransitionAnimationFactory
    {
        private final int animationId;
        
        ResourceViewTransitionAnimationFactory(final int animationId) {
            this.animationId = animationId;
        }
        
        @Override
        public Animation build(final Context context) {
            return AnimationUtils.loadAnimation(context, this.animationId);
        }
    }
}

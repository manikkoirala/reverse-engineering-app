// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide.request.transition;

import android.graphics.drawable.TransitionDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;

public class DrawableCrossFadeTransition implements Transition<Drawable>
{
    private final int duration;
    private final boolean isCrossFadeEnabled;
    
    public DrawableCrossFadeTransition(final int duration, final boolean isCrossFadeEnabled) {
        this.duration = duration;
        this.isCrossFadeEnabled = isCrossFadeEnabled;
    }
    
    @Override
    public boolean transition(final Drawable drawable, final ViewAdapter viewAdapter) {
        Object currentDrawable;
        if ((currentDrawable = viewAdapter.getCurrentDrawable()) == null) {
            currentDrawable = new ColorDrawable(0);
        }
        final TransitionDrawable drawable2 = new TransitionDrawable(new Drawable[] { (Drawable)currentDrawable, drawable });
        drawable2.setCrossFadeEnabled(this.isCrossFadeEnabled);
        drawable2.startTransition(this.duration);
        viewAdapter.setDrawable((Drawable)drawable2);
        return true;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide.request.transition;

import android.graphics.drawable.BitmapDrawable;
import android.graphics.Bitmap;
import com.bumptech.glide.load.DataSource;
import android.graphics.drawable.Drawable;

public abstract class BitmapContainerTransitionFactory<R> implements TransitionFactory<R>
{
    private final TransitionFactory<Drawable> realFactory;
    
    public BitmapContainerTransitionFactory(final TransitionFactory<Drawable> realFactory) {
        this.realFactory = realFactory;
    }
    
    @Override
    public Transition<R> build(final DataSource dataSource, final boolean b) {
        return new BitmapGlideAnimation(this.realFactory.build(dataSource, b));
    }
    
    protected abstract Bitmap getBitmap(final R p0);
    
    private final class BitmapGlideAnimation implements Transition<R>
    {
        final BitmapContainerTransitionFactory this$0;
        private final Transition<Drawable> transition;
        
        BitmapGlideAnimation(final BitmapContainerTransitionFactory this$0, final Transition<Drawable> transition) {
            this.this$0 = this$0;
            this.transition = transition;
        }
        
        @Override
        public boolean transition(final R r, final ViewAdapter viewAdapter) {
            return this.transition.transition((Drawable)new BitmapDrawable(viewAdapter.getView().getResources(), this.this$0.getBitmap(r)), viewAdapter);
        }
    }
}

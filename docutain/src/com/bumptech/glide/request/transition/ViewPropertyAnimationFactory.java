// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide.request.transition;

import com.bumptech.glide.load.DataSource;

public class ViewPropertyAnimationFactory<R> implements TransitionFactory<R>
{
    private ViewPropertyTransition<R> animation;
    private final ViewPropertyTransition.Animator animator;
    
    public ViewPropertyAnimationFactory(final ViewPropertyTransition.Animator animator) {
        this.animator = animator;
    }
    
    @Override
    public Transition<R> build(final DataSource dataSource, final boolean b) {
        if (dataSource != DataSource.MEMORY_CACHE && b) {
            if (this.animation == null) {
                this.animation = new ViewPropertyTransition<R>(this.animator);
            }
            return this.animation;
        }
        return NoTransition.get();
    }
}

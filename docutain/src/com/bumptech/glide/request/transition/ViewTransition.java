// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide.request.transition;

import android.view.animation.Animation;
import android.content.Context;
import android.view.View;

public class ViewTransition<R> implements Transition<R>
{
    private final ViewTransitionAnimationFactory viewTransitionAnimationFactory;
    
    ViewTransition(final ViewTransitionAnimationFactory viewTransitionAnimationFactory) {
        this.viewTransitionAnimationFactory = viewTransitionAnimationFactory;
    }
    
    @Override
    public boolean transition(final R r, final ViewAdapter viewAdapter) {
        final View view = viewAdapter.getView();
        if (view != null) {
            view.clearAnimation();
            view.startAnimation(this.viewTransitionAnimationFactory.build(view.getContext()));
        }
        return false;
    }
    
    interface ViewTransitionAnimationFactory
    {
        Animation build(final Context p0);
    }
}

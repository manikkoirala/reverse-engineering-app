// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide.request.transition;

import com.bumptech.glide.load.DataSource;
import android.graphics.drawable.Drawable;

public class DrawableCrossFadeFactory implements TransitionFactory<Drawable>
{
    private final int duration;
    private final boolean isCrossFadeEnabled;
    private DrawableCrossFadeTransition resourceTransition;
    
    protected DrawableCrossFadeFactory(final int duration, final boolean isCrossFadeEnabled) {
        this.duration = duration;
        this.isCrossFadeEnabled = isCrossFadeEnabled;
    }
    
    private Transition<Drawable> getResourceTransition() {
        if (this.resourceTransition == null) {
            this.resourceTransition = new DrawableCrossFadeTransition(this.duration, this.isCrossFadeEnabled);
        }
        return this.resourceTransition;
    }
    
    @Override
    public Transition<Drawable> build(final DataSource dataSource, final boolean b) {
        Object o;
        if (dataSource == DataSource.MEMORY_CACHE) {
            o = NoTransition.get();
        }
        else {
            o = this.getResourceTransition();
        }
        return (Transition<Drawable>)o;
    }
    
    public static class Builder
    {
        private static final int DEFAULT_DURATION_MS = 300;
        private final int durationMillis;
        private boolean isCrossFadeEnabled;
        
        public Builder() {
            this(300);
        }
        
        public Builder(final int durationMillis) {
            this.durationMillis = durationMillis;
        }
        
        public DrawableCrossFadeFactory build() {
            return new DrawableCrossFadeFactory(this.durationMillis, this.isCrossFadeEnabled);
        }
        
        public Builder setCrossFadeEnabled(final boolean isCrossFadeEnabled) {
            this.isCrossFadeEnabled = isCrossFadeEnabled;
            return this;
        }
    }
}

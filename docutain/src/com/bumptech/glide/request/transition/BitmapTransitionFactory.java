// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide.request.transition;

import android.graphics.drawable.Drawable;
import android.graphics.Bitmap;

public class BitmapTransitionFactory extends BitmapContainerTransitionFactory<Bitmap>
{
    public BitmapTransitionFactory(final TransitionFactory<Drawable> transitionFactory) {
        super(transitionFactory);
    }
    
    @Override
    protected Bitmap getBitmap(final Bitmap bitmap) {
        return bitmap;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide.request.transition;

import android.view.View;
import android.graphics.drawable.Drawable;

public interface Transition<R>
{
    boolean transition(final R p0, final ViewAdapter p1);
    
    public interface ViewAdapter
    {
        Drawable getCurrentDrawable();
        
        View getView();
        
        void setDrawable(final Drawable p0);
    }
}

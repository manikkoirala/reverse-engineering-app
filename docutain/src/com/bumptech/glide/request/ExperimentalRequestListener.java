// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide.request;

import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.request.target.Target;

@Deprecated
public abstract class ExperimentalRequestListener<ResourceT> implements RequestListener<ResourceT>
{
    public void onRequestStarted(final Object o) {
    }
    
    public abstract boolean onResourceReady(final ResourceT p0, final Object p1, final Target<ResourceT> p2, final DataSource p3, final boolean p4, final boolean p5);
}

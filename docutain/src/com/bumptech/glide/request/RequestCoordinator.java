// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide.request;

public interface RequestCoordinator
{
    boolean canNotifyCleared(final Request p0);
    
    boolean canNotifyStatusChanged(final Request p0);
    
    boolean canSetImage(final Request p0);
    
    RequestCoordinator getRoot();
    
    boolean isAnyResourceSet();
    
    void onRequestFailed(final Request p0);
    
    void onRequestSuccess(final Request p0);
    
    public enum RequestState
    {
        private static final RequestState[] $VALUES;
        
        CLEARED(false), 
        FAILED(true), 
        PAUSED(false), 
        RUNNING(false), 
        SUCCESS(true);
        
        private final boolean isComplete;
        
        private RequestState(final boolean isComplete) {
            this.isComplete = isComplete;
        }
        
        boolean isComplete() {
            return this.isComplete;
        }
    }
}

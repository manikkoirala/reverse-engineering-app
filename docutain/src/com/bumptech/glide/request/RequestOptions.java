// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide.request;

import com.bumptech.glide.load.Key;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.Option;
import com.bumptech.glide.load.DecodeFormat;
import android.graphics.drawable.Drawable;
import android.graphics.Bitmap$CompressFormat;
import com.bumptech.glide.load.resource.bitmap.DownsampleStrategy;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import android.graphics.Bitmap;
import com.bumptech.glide.load.Transformation;

public class RequestOptions extends BaseRequestOptions<RequestOptions>
{
    private static RequestOptions centerCropOptions;
    private static RequestOptions centerInsideOptions;
    private static RequestOptions circleCropOptions;
    private static RequestOptions fitCenterOptions;
    private static RequestOptions noAnimationOptions;
    private static RequestOptions noTransformOptions;
    private static RequestOptions skipMemoryCacheFalseOptions;
    private static RequestOptions skipMemoryCacheTrueOptions;
    
    public static RequestOptions bitmapTransform(final Transformation<Bitmap> transformation) {
        return new RequestOptions().transform(transformation);
    }
    
    public static RequestOptions centerCropTransform() {
        if (RequestOptions.centerCropOptions == null) {
            RequestOptions.centerCropOptions = new RequestOptions().centerCrop().autoClone();
        }
        return RequestOptions.centerCropOptions;
    }
    
    public static RequestOptions centerInsideTransform() {
        if (RequestOptions.centerInsideOptions == null) {
            RequestOptions.centerInsideOptions = new RequestOptions().centerInside().autoClone();
        }
        return RequestOptions.centerInsideOptions;
    }
    
    public static RequestOptions circleCropTransform() {
        if (RequestOptions.circleCropOptions == null) {
            RequestOptions.circleCropOptions = new RequestOptions().circleCrop().autoClone();
        }
        return RequestOptions.circleCropOptions;
    }
    
    public static RequestOptions decodeTypeOf(final Class<?> clazz) {
        return new RequestOptions().decode(clazz);
    }
    
    public static RequestOptions diskCacheStrategyOf(final DiskCacheStrategy diskCacheStrategy) {
        return new RequestOptions().diskCacheStrategy(diskCacheStrategy);
    }
    
    public static RequestOptions downsampleOf(final DownsampleStrategy downsampleStrategy) {
        return new RequestOptions().downsample(downsampleStrategy);
    }
    
    public static RequestOptions encodeFormatOf(final Bitmap$CompressFormat bitmap$CompressFormat) {
        return new RequestOptions().encodeFormat(bitmap$CompressFormat);
    }
    
    public static RequestOptions encodeQualityOf(final int n) {
        return new RequestOptions().encodeQuality(n);
    }
    
    public static RequestOptions errorOf(final int n) {
        return new RequestOptions().error(n);
    }
    
    public static RequestOptions errorOf(final Drawable drawable) {
        return new RequestOptions().error(drawable);
    }
    
    public static RequestOptions fitCenterTransform() {
        if (RequestOptions.fitCenterOptions == null) {
            RequestOptions.fitCenterOptions = new RequestOptions().fitCenter().autoClone();
        }
        return RequestOptions.fitCenterOptions;
    }
    
    public static RequestOptions formatOf(final DecodeFormat decodeFormat) {
        return new RequestOptions().format(decodeFormat);
    }
    
    public static RequestOptions frameOf(final long n) {
        return new RequestOptions().frame(n);
    }
    
    public static RequestOptions noAnimation() {
        if (RequestOptions.noAnimationOptions == null) {
            RequestOptions.noAnimationOptions = new RequestOptions().dontAnimate().autoClone();
        }
        return RequestOptions.noAnimationOptions;
    }
    
    public static RequestOptions noTransformation() {
        if (RequestOptions.noTransformOptions == null) {
            RequestOptions.noTransformOptions = new RequestOptions().dontTransform().autoClone();
        }
        return RequestOptions.noTransformOptions;
    }
    
    public static <T> RequestOptions option(final Option<T> option, final T t) {
        return new RequestOptions().set(option, t);
    }
    
    public static RequestOptions overrideOf(final int n) {
        return overrideOf(n, n);
    }
    
    public static RequestOptions overrideOf(final int n, final int n2) {
        return new RequestOptions().override(n, n2);
    }
    
    public static RequestOptions placeholderOf(final int n) {
        return new RequestOptions().placeholder(n);
    }
    
    public static RequestOptions placeholderOf(final Drawable drawable) {
        return new RequestOptions().placeholder(drawable);
    }
    
    public static RequestOptions priorityOf(final Priority priority) {
        return new RequestOptions().priority(priority);
    }
    
    public static RequestOptions signatureOf(final Key key) {
        return new RequestOptions().signature(key);
    }
    
    public static RequestOptions sizeMultiplierOf(final float n) {
        return new RequestOptions().sizeMultiplier(n);
    }
    
    public static RequestOptions skipMemoryCacheOf(final boolean b) {
        if (b) {
            if (RequestOptions.skipMemoryCacheTrueOptions == null) {
                RequestOptions.skipMemoryCacheTrueOptions = new RequestOptions().skipMemoryCache(true).autoClone();
            }
            return RequestOptions.skipMemoryCacheTrueOptions;
        }
        if (RequestOptions.skipMemoryCacheFalseOptions == null) {
            RequestOptions.skipMemoryCacheFalseOptions = new RequestOptions().skipMemoryCache(false).autoClone();
        }
        return RequestOptions.skipMemoryCacheFalseOptions;
    }
    
    public static RequestOptions timeoutOf(final int n) {
        return new RequestOptions().timeout(n);
    }
    
    @Override
    public boolean equals(final Object o) {
        return o instanceof RequestOptions && super.equals(o);
    }
    
    @Override
    public int hashCode() {
        return super.hashCode();
    }
}

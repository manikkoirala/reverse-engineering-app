// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide.request.target;

import android.widget.ImageView;
import android.graphics.drawable.Drawable;

public class DrawableThumbnailImageViewTarget extends ThumbnailImageViewTarget<Drawable>
{
    public DrawableThumbnailImageViewTarget(final ImageView imageView) {
        super(imageView);
    }
    
    @Deprecated
    public DrawableThumbnailImageViewTarget(final ImageView imageView, final boolean b) {
        super(imageView, b);
    }
    
    @Override
    protected Drawable getDrawable(final Drawable drawable) {
        return drawable;
    }
}

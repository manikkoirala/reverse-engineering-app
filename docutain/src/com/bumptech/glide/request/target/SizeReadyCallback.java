// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide.request.target;

public interface SizeReadyCallback
{
    void onSizeReady(final int p0, final int p1);
}

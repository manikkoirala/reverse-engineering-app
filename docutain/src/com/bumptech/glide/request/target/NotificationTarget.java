// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide.request.target;

import com.bumptech.glide.request.transition.Transition;
import android.graphics.drawable.Drawable;
import android.app.NotificationManager;
import com.bumptech.glide.util.Preconditions;
import android.widget.RemoteViews;
import android.app.Notification;
import android.content.Context;
import android.graphics.Bitmap;

public class NotificationTarget extends CustomTarget<Bitmap>
{
    private final Context context;
    private final Notification notification;
    private final int notificationId;
    private final String notificationTag;
    private final RemoteViews remoteViews;
    private final int viewId;
    
    public NotificationTarget(final Context context, final int n, final int n2, final int viewId, final RemoteViews remoteViews, final Notification notification, final int notificationId, final String notificationTag) {
        super(n, n2);
        this.context = Preconditions.checkNotNull(context, "Context must not be null!");
        this.notification = Preconditions.checkNotNull(notification, "Notification object can not be null!");
        this.remoteViews = Preconditions.checkNotNull(remoteViews, "RemoteViews object can not be null!");
        this.viewId = viewId;
        this.notificationId = notificationId;
        this.notificationTag = notificationTag;
    }
    
    public NotificationTarget(final Context context, final int n, final RemoteViews remoteViews, final Notification notification, final int n2) {
        this(context, n, remoteViews, notification, n2, null);
    }
    
    public NotificationTarget(final Context context, final int n, final RemoteViews remoteViews, final Notification notification, final int n2, final String s) {
        this(context, Integer.MIN_VALUE, Integer.MIN_VALUE, n, remoteViews, notification, n2, s);
    }
    
    private void setBitmap(final Bitmap bitmap) {
        this.remoteViews.setImageViewBitmap(this.viewId, bitmap);
        this.update();
    }
    
    private void update() {
        Preconditions.checkNotNull(this.context.getSystemService("notification")).notify(this.notificationTag, this.notificationId, this.notification);
    }
    
    @Override
    public void onLoadCleared(final Drawable drawable) {
        this.setBitmap(null);
    }
    
    @Override
    public void onResourceReady(final Bitmap bitmap, final Transition<? super Bitmap> transition) {
        this.setBitmap(bitmap);
    }
}

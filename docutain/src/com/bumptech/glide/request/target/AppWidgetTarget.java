// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide.request.target;

import com.bumptech.glide.request.transition.Transition;
import android.graphics.drawable.Drawable;
import android.appwidget.AppWidgetManager;
import com.bumptech.glide.util.Preconditions;
import android.widget.RemoteViews;
import android.content.Context;
import android.content.ComponentName;
import android.graphics.Bitmap;

public class AppWidgetTarget extends CustomTarget<Bitmap>
{
    private final ComponentName componentName;
    private final Context context;
    private final RemoteViews remoteViews;
    private final int viewId;
    private final int[] widgetIds;
    
    public AppWidgetTarget(final Context context, final int n, final int n2, final int viewId, final RemoteViews remoteViews, final ComponentName componentName) {
        super(n, n2);
        this.context = Preconditions.checkNotNull(context, "Context can not be null!");
        this.remoteViews = Preconditions.checkNotNull(remoteViews, "RemoteViews object can not be null!");
        this.componentName = Preconditions.checkNotNull(componentName, "ComponentName can not be null!");
        this.viewId = viewId;
        this.widgetIds = null;
    }
    
    public AppWidgetTarget(final Context context, final int n, final int n2, final int viewId, final RemoteViews remoteViews, final int... array) {
        super(n, n2);
        if (array.length != 0) {
            this.context = Preconditions.checkNotNull(context, "Context can not be null!");
            this.remoteViews = Preconditions.checkNotNull(remoteViews, "RemoteViews object can not be null!");
            this.widgetIds = Preconditions.checkNotNull(array, "WidgetIds can not be null!");
            this.viewId = viewId;
            this.componentName = null;
            return;
        }
        throw new IllegalArgumentException("WidgetIds must have length > 0");
    }
    
    public AppWidgetTarget(final Context context, final int n, final RemoteViews remoteViews, final ComponentName componentName) {
        this(context, Integer.MIN_VALUE, Integer.MIN_VALUE, n, remoteViews, componentName);
    }
    
    public AppWidgetTarget(final Context context, final int n, final RemoteViews remoteViews, final int... array) {
        this(context, Integer.MIN_VALUE, Integer.MIN_VALUE, n, remoteViews, array);
    }
    
    private void setBitmap(final Bitmap bitmap) {
        this.remoteViews.setImageViewBitmap(this.viewId, bitmap);
        this.update();
    }
    
    private void update() {
        final AppWidgetManager instance = AppWidgetManager.getInstance(this.context);
        final ComponentName componentName = this.componentName;
        if (componentName != null) {
            instance.updateAppWidget(componentName, this.remoteViews);
        }
        else {
            instance.updateAppWidget(this.widgetIds, this.remoteViews);
        }
    }
    
    @Override
    public void onLoadCleared(final Drawable drawable) {
        this.setBitmap(null);
    }
    
    @Override
    public void onResourceReady(final Bitmap bitmap, final Transition<? super Bitmap> transition) {
        this.setBitmap(bitmap);
    }
}

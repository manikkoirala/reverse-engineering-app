// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide.request.target;

import android.graphics.drawable.Drawable;
import android.view.View;
import android.graphics.drawable.Animatable;
import com.bumptech.glide.request.transition.Transition;
import android.widget.ImageView;

public abstract class ImageViewTarget<Z> extends ViewTarget<ImageView, Z> implements ViewAdapter
{
    private Animatable animatable;
    
    public ImageViewTarget(final ImageView imageView) {
        super((View)imageView);
    }
    
    @Deprecated
    public ImageViewTarget(final ImageView imageView, final boolean b) {
        super((View)imageView, b);
    }
    
    private void maybeUpdateAnimatable(final Z b) {
        if (b instanceof Animatable) {
            (this.animatable = (Animatable)b).start();
        }
        else {
            this.animatable = null;
        }
    }
    
    private void setResourceInternal(final Z resource) {
        this.setResource(resource);
        this.maybeUpdateAnimatable(resource);
    }
    
    @Override
    public Drawable getCurrentDrawable() {
        return ((ImageView)this.view).getDrawable();
    }
    
    @Override
    public void onLoadCleared(final Drawable drawable) {
        super.onLoadCleared(drawable);
        final Animatable animatable = this.animatable;
        if (animatable != null) {
            animatable.stop();
        }
        this.setResourceInternal(null);
        this.setDrawable(drawable);
    }
    
    @Override
    public void onLoadFailed(final Drawable drawable) {
        super.onLoadFailed(drawable);
        this.setResourceInternal(null);
        this.setDrawable(drawable);
    }
    
    @Override
    public void onLoadStarted(final Drawable drawable) {
        super.onLoadStarted(drawable);
        this.setResourceInternal(null);
        this.setDrawable(drawable);
    }
    
    @Override
    public void onResourceReady(final Z resourceInternal, final Transition<? super Z> transition) {
        if (transition != null && transition.transition(resourceInternal, (Transition.ViewAdapter)this)) {
            this.maybeUpdateAnimatable(resourceInternal);
        }
        else {
            this.setResourceInternal(resourceInternal);
        }
    }
    
    @Override
    public void onStart() {
        final Animatable animatable = this.animatable;
        if (animatable != null) {
            animatable.start();
        }
    }
    
    @Override
    public void onStop() {
        final Animatable animatable = this.animatable;
        if (animatable != null) {
            animatable.stop();
        }
    }
    
    @Override
    public void setDrawable(final Drawable imageDrawable) {
        ((ImageView)this.view).setImageDrawable(imageDrawable);
    }
    
    protected abstract void setResource(final Z p0);
}

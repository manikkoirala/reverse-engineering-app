// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide.request.target;

import android.widget.ImageView;
import android.graphics.Bitmap;

public class BitmapImageViewTarget extends ImageViewTarget<Bitmap>
{
    public BitmapImageViewTarget(final ImageView imageView) {
        super(imageView);
    }
    
    @Deprecated
    public BitmapImageViewTarget(final ImageView imageView, final boolean b) {
        super(imageView, b);
    }
    
    @Override
    protected void setResource(final Bitmap imageBitmap) {
        ((ImageView)this.view).setImageBitmap(imageBitmap);
    }
}

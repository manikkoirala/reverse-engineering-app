// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide.request.target;

import android.graphics.drawable.Drawable;
import com.bumptech.glide.util.Util;
import com.bumptech.glide.request.Request;

public abstract class CustomTarget<T> implements Target<T>
{
    private final int height;
    private Request request;
    private final int width;
    
    public CustomTarget() {
        this(Integer.MIN_VALUE, Integer.MIN_VALUE);
    }
    
    public CustomTarget(final int n, final int n2) {
        if (Util.isValidDimensions(n, n2)) {
            this.width = n;
            this.height = n2;
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Width and height must both be > 0 or Target#SIZE_ORIGINAL, but given width: ");
        sb.append(n);
        sb.append(" and height: ");
        sb.append(n2);
        throw new IllegalArgumentException(sb.toString());
    }
    
    @Override
    public final Request getRequest() {
        return this.request;
    }
    
    @Override
    public final void getSize(final SizeReadyCallback sizeReadyCallback) {
        sizeReadyCallback.onSizeReady(this.width, this.height);
    }
    
    @Override
    public void onDestroy() {
    }
    
    @Override
    public void onLoadFailed(final Drawable drawable) {
    }
    
    @Override
    public void onLoadStarted(final Drawable drawable) {
    }
    
    @Override
    public void onStart() {
    }
    
    @Override
    public void onStop() {
    }
    
    @Override
    public final void removeCallback(final SizeReadyCallback sizeReadyCallback) {
    }
    
    @Override
    public final void setRequest(final Request request) {
        this.request = request;
    }
}

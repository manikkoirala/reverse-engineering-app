// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide.request.target;

import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;
import android.graphics.Bitmap;

public class BitmapThumbnailImageViewTarget extends ThumbnailImageViewTarget<Bitmap>
{
    public BitmapThumbnailImageViewTarget(final ImageView imageView) {
        super(imageView);
    }
    
    @Deprecated
    public BitmapThumbnailImageViewTarget(final ImageView imageView, final boolean b) {
        super(imageView, b);
    }
    
    @Override
    protected Drawable getDrawable(final Bitmap bitmap) {
        return (Drawable)new BitmapDrawable(((ImageView)this.view).getResources(), bitmap);
    }
}

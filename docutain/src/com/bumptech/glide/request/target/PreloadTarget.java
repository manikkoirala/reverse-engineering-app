// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide.request.target;

import com.bumptech.glide.request.Request;
import com.bumptech.glide.request.transition.Transition;
import android.graphics.drawable.Drawable;
import android.os.Message;
import android.os.Handler$Callback;
import android.os.Looper;
import com.bumptech.glide.RequestManager;
import android.os.Handler;

public final class PreloadTarget<Z> extends CustomTarget<Z>
{
    private static final Handler HANDLER;
    private static final int MESSAGE_CLEAR = 1;
    private final RequestManager requestManager;
    
    static {
        HANDLER = new Handler(Looper.getMainLooper(), (Handler$Callback)new Handler$Callback() {
            public boolean handleMessage(final Message message) {
                if (message.what == 1) {
                    ((PreloadTarget)message.obj).clear();
                    return true;
                }
                return false;
            }
        });
    }
    
    private PreloadTarget(final RequestManager requestManager, final int n, final int n2) {
        super(n, n2);
        this.requestManager = requestManager;
    }
    
    public static <Z> PreloadTarget<Z> obtain(final RequestManager requestManager, final int n, final int n2) {
        return new PreloadTarget<Z>(requestManager, n, n2);
    }
    
    void clear() {
        this.requestManager.clear(this);
    }
    
    @Override
    public void onLoadCleared(final Drawable drawable) {
    }
    
    @Override
    public void onResourceReady(final Z b, final Transition<? super Z> transition) {
        final Request request = this.getRequest();
        if (request != null && request.isComplete()) {
            PreloadTarget.HANDLER.obtainMessage(1, (Object)this).sendToTarget();
        }
    }
}

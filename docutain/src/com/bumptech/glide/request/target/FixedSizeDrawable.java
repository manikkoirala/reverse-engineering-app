// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide.request.target;

import android.content.res.Resources;
import android.graphics.ColorFilter;
import android.graphics.PorterDuff$Mode;
import android.graphics.Rect;
import android.graphics.drawable.Drawable$ConstantState;
import android.graphics.drawable.Drawable$Callback;
import android.graphics.Canvas;
import android.graphics.Matrix$ScaleToFit;
import com.bumptech.glide.util.Preconditions;
import android.graphics.Matrix;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;

public class FixedSizeDrawable extends Drawable
{
    private final RectF bounds;
    private final Matrix matrix;
    private boolean mutated;
    private State state;
    private Drawable wrapped;
    private final RectF wrappedRect;
    
    public FixedSizeDrawable(final Drawable drawable, final int n, final int n2) {
        this(new State(drawable.getConstantState(), n, n2), drawable);
    }
    
    FixedSizeDrawable(final State state, final Drawable drawable) {
        this.state = Preconditions.checkNotNull(state);
        this.wrapped = Preconditions.checkNotNull(drawable);
        drawable.setBounds(0, 0, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());
        this.matrix = new Matrix();
        this.wrappedRect = new RectF(0.0f, 0.0f, (float)drawable.getIntrinsicWidth(), (float)drawable.getIntrinsicHeight());
        this.bounds = new RectF();
    }
    
    private void updateMatrix() {
        this.matrix.setRectToRect(this.wrappedRect, this.bounds, Matrix$ScaleToFit.CENTER);
    }
    
    public void clearColorFilter() {
        this.wrapped.clearColorFilter();
    }
    
    public void draw(final Canvas canvas) {
        canvas.save();
        canvas.concat(this.matrix);
        this.wrapped.draw(canvas);
        canvas.restore();
    }
    
    public int getAlpha() {
        return this.wrapped.getAlpha();
    }
    
    public Drawable$Callback getCallback() {
        return this.wrapped.getCallback();
    }
    
    public int getChangingConfigurations() {
        return this.wrapped.getChangingConfigurations();
    }
    
    public Drawable$ConstantState getConstantState() {
        return this.state;
    }
    
    public Drawable getCurrent() {
        return this.wrapped.getCurrent();
    }
    
    public int getIntrinsicHeight() {
        return this.state.height;
    }
    
    public int getIntrinsicWidth() {
        return this.state.width;
    }
    
    public int getMinimumHeight() {
        return this.wrapped.getMinimumHeight();
    }
    
    public int getMinimumWidth() {
        return this.wrapped.getMinimumWidth();
    }
    
    public int getOpacity() {
        return this.wrapped.getOpacity();
    }
    
    public boolean getPadding(final Rect rect) {
        return this.wrapped.getPadding(rect);
    }
    
    public void invalidateSelf() {
        super.invalidateSelf();
        this.wrapped.invalidateSelf();
    }
    
    public Drawable mutate() {
        if (!this.mutated && super.mutate() == this) {
            this.wrapped = this.wrapped.mutate();
            this.state = new State(this.state);
            this.mutated = true;
        }
        return this;
    }
    
    public void scheduleSelf(final Runnable runnable, final long n) {
        super.scheduleSelf(runnable, n);
        this.wrapped.scheduleSelf(runnable, n);
    }
    
    public void setAlpha(final int alpha) {
        this.wrapped.setAlpha(alpha);
    }
    
    public void setBounds(final int n, final int n2, final int n3, final int n4) {
        super.setBounds(n, n2, n3, n4);
        this.bounds.set((float)n, (float)n2, (float)n3, (float)n4);
        this.updateMatrix();
    }
    
    public void setBounds(final Rect bounds) {
        super.setBounds(bounds);
        this.bounds.set(bounds);
        this.updateMatrix();
    }
    
    public void setChangingConfigurations(final int changingConfigurations) {
        this.wrapped.setChangingConfigurations(changingConfigurations);
    }
    
    public void setColorFilter(final int n, final PorterDuff$Mode porterDuff$Mode) {
        this.wrapped.setColorFilter(n, porterDuff$Mode);
    }
    
    public void setColorFilter(final ColorFilter colorFilter) {
        this.wrapped.setColorFilter(colorFilter);
    }
    
    @Deprecated
    public void setDither(final boolean dither) {
        this.wrapped.setDither(dither);
    }
    
    public void setFilterBitmap(final boolean filterBitmap) {
        this.wrapped.setFilterBitmap(filterBitmap);
    }
    
    public boolean setVisible(final boolean b, final boolean b2) {
        return this.wrapped.setVisible(b, b2);
    }
    
    public void unscheduleSelf(final Runnable runnable) {
        super.unscheduleSelf(runnable);
        this.wrapped.unscheduleSelf(runnable);
    }
    
    static final class State extends Drawable$ConstantState
    {
        final int height;
        final int width;
        private final Drawable$ConstantState wrapped;
        
        State(final Drawable$ConstantState wrapped, final int width, final int height) {
            this.wrapped = wrapped;
            this.width = width;
            this.height = height;
        }
        
        State(final State state) {
            this(state.wrapped, state.width, state.height);
        }
        
        public int getChangingConfigurations() {
            return 0;
        }
        
        public Drawable newDrawable() {
            return new FixedSizeDrawable(this, this.wrapped.newDrawable());
        }
        
        public Drawable newDrawable(final Resources resources) {
            return new FixedSizeDrawable(this, this.wrapped.newDrawable(resources));
        }
    }
}

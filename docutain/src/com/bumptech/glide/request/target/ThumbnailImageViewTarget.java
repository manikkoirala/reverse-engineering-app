// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide.request.target;

import android.view.ViewGroup$LayoutParams;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;

public abstract class ThumbnailImageViewTarget<T> extends ImageViewTarget<T>
{
    public ThumbnailImageViewTarget(final ImageView imageView) {
        super(imageView);
    }
    
    @Deprecated
    public ThumbnailImageViewTarget(final ImageView imageView, final boolean b) {
        super(imageView, b);
    }
    
    protected abstract Drawable getDrawable(final T p0);
    
    @Override
    protected void setResource(final T t) {
        final ViewGroup$LayoutParams layoutParams = ((ImageView)this.view).getLayoutParams();
        Drawable drawable2;
        final Drawable drawable = drawable2 = this.getDrawable(t);
        if (layoutParams != null) {
            drawable2 = drawable;
            if (layoutParams.width > 0) {
                drawable2 = drawable;
                if (layoutParams.height > 0) {
                    drawable2 = new FixedSizeDrawable(drawable, layoutParams.width, layoutParams.height);
                }
            }
        }
        ((ImageView)this.view).setImageDrawable(drawable2);
    }
}

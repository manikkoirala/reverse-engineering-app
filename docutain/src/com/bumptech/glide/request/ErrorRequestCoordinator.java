// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide.request;

public final class ErrorRequestCoordinator implements RequestCoordinator, Request
{
    private volatile Request error;
    private RequestState errorState;
    private final RequestCoordinator parent;
    private volatile Request primary;
    private RequestState primaryState;
    private final Object requestLock;
    
    public ErrorRequestCoordinator(final Object requestLock, final RequestCoordinator parent) {
        this.primaryState = RequestState.CLEARED;
        this.errorState = RequestState.CLEARED;
        this.requestLock = requestLock;
        this.parent = parent;
    }
    
    private boolean isValidRequestForStatusChanged(final Request request) {
        if (this.primaryState != RequestState.FAILED) {
            return request.equals(this.primary);
        }
        return request.equals(this.error) && (this.errorState == RequestState.SUCCESS || this.errorState == RequestState.FAILED);
    }
    
    private boolean parentCanNotifyCleared() {
        final RequestCoordinator parent = this.parent;
        return parent == null || parent.canNotifyCleared(this);
    }
    
    private boolean parentCanNotifyStatusChanged() {
        final RequestCoordinator parent = this.parent;
        return parent == null || parent.canNotifyStatusChanged(this);
    }
    
    private boolean parentCanSetImage() {
        final RequestCoordinator parent = this.parent;
        return parent == null || parent.canSetImage(this);
    }
    
    @Override
    public void begin() {
        synchronized (this.requestLock) {
            if (this.primaryState != RequestState.RUNNING) {
                this.primaryState = RequestState.RUNNING;
                this.primary.begin();
            }
        }
    }
    
    @Override
    public boolean canNotifyCleared(final Request request) {
        synchronized (this.requestLock) {
            return this.parentCanNotifyCleared() && request.equals(this.primary);
        }
    }
    
    @Override
    public boolean canNotifyStatusChanged(final Request request) {
        synchronized (this.requestLock) {
            return this.parentCanNotifyStatusChanged() && this.isValidRequestForStatusChanged(request);
        }
    }
    
    @Override
    public boolean canSetImage(final Request request) {
        synchronized (this.requestLock) {
            return this.parentCanSetImage();
        }
    }
    
    @Override
    public void clear() {
        synchronized (this.requestLock) {
            this.primaryState = RequestState.CLEARED;
            this.primary.clear();
            if (this.errorState != RequestState.CLEARED) {
                this.errorState = RequestState.CLEARED;
                this.error.clear();
            }
        }
    }
    
    @Override
    public RequestCoordinator getRoot() {
        synchronized (this.requestLock) {
            final RequestCoordinator parent = this.parent;
            RequestCoordinator root;
            if (parent != null) {
                root = parent.getRoot();
            }
            else {
                root = this;
            }
            return root;
        }
    }
    
    @Override
    public boolean isAnyResourceSet() {
        synchronized (this.requestLock) {
            return this.primary.isAnyResourceSet() || this.error.isAnyResourceSet();
        }
    }
    
    @Override
    public boolean isCleared() {
        synchronized (this.requestLock) {
            return this.primaryState == RequestState.CLEARED && this.errorState == RequestState.CLEARED;
        }
    }
    
    @Override
    public boolean isComplete() {
        synchronized (this.requestLock) {
            return this.primaryState == RequestState.SUCCESS || this.errorState == RequestState.SUCCESS;
        }
    }
    
    @Override
    public boolean isEquivalentTo(final Request request) {
        final boolean b = request instanceof ErrorRequestCoordinator;
        boolean b3;
        final boolean b2 = b3 = false;
        if (b) {
            final ErrorRequestCoordinator errorRequestCoordinator = (ErrorRequestCoordinator)request;
            b3 = b2;
            if (this.primary.isEquivalentTo(errorRequestCoordinator.primary)) {
                b3 = b2;
                if (this.error.isEquivalentTo(errorRequestCoordinator.error)) {
                    b3 = true;
                }
            }
        }
        return b3;
    }
    
    @Override
    public boolean isRunning() {
        synchronized (this.requestLock) {
            return this.primaryState == RequestState.RUNNING || this.errorState == RequestState.RUNNING;
        }
    }
    
    @Override
    public void onRequestFailed(final Request request) {
        synchronized (this.requestLock) {
            if (!request.equals(this.error)) {
                this.primaryState = RequestState.FAILED;
                if (this.errorState != RequestState.RUNNING) {
                    this.errorState = RequestState.RUNNING;
                    this.error.begin();
                }
                return;
            }
            this.errorState = RequestState.FAILED;
            final RequestCoordinator parent = this.parent;
            if (parent != null) {
                parent.onRequestFailed(this);
            }
        }
    }
    
    @Override
    public void onRequestSuccess(final Request request) {
        synchronized (this.requestLock) {
            if (request.equals(this.primary)) {
                this.primaryState = RequestState.SUCCESS;
            }
            else if (request.equals(this.error)) {
                this.errorState = RequestState.SUCCESS;
            }
            final RequestCoordinator parent = this.parent;
            if (parent != null) {
                parent.onRequestSuccess(this);
            }
        }
    }
    
    @Override
    public void pause() {
        synchronized (this.requestLock) {
            if (this.primaryState == RequestState.RUNNING) {
                this.primaryState = RequestState.PAUSED;
                this.primary.pause();
            }
            if (this.errorState == RequestState.RUNNING) {
                this.errorState = RequestState.PAUSED;
                this.error.pause();
            }
        }
    }
    
    public void setRequests(final Request primary, final Request error) {
        this.primary = primary;
        this.error = error;
    }
}

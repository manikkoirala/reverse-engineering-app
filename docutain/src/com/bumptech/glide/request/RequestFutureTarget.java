// 
// Decompiled by Procyon v0.6.0
// 

package com.bumptech.glide.request;

import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.request.transition.Transition;
import com.bumptech.glide.request.target.Target;
import android.graphics.drawable.Drawable;
import com.bumptech.glide.request.target.SizeReadyCallback;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.CancellationException;
import com.bumptech.glide.util.Util;
import com.bumptech.glide.load.engine.GlideException;

public class RequestFutureTarget<R> implements FutureTarget<R>, RequestListener<R>
{
    private static final Waiter DEFAULT_WAITER;
    private final boolean assertBackgroundThread;
    private GlideException exception;
    private final int height;
    private boolean isCancelled;
    private boolean loadFailed;
    private Request request;
    private R resource;
    private boolean resultReceived;
    private final Waiter waiter;
    private final int width;
    
    static {
        DEFAULT_WAITER = new Waiter();
    }
    
    public RequestFutureTarget(final int n, final int n2) {
        this(n, n2, true, RequestFutureTarget.DEFAULT_WAITER);
    }
    
    RequestFutureTarget(final int width, final int height, final boolean assertBackgroundThread, final Waiter waiter) {
        this.width = width;
        this.height = height;
        this.assertBackgroundThread = assertBackgroundThread;
        this.waiter = waiter;
    }
    
    private R doGet(final Long n) throws ExecutionException, InterruptedException, TimeoutException {
        synchronized (this) {
            if (this.assertBackgroundThread && !this.isDone()) {
                Util.assertBackgroundThread();
            }
            if (this.isCancelled) {
                throw new CancellationException();
            }
            if (this.loadFailed) {
                throw new ExecutionException(this.exception);
            }
            if (this.resultReceived) {
                return this.resource;
            }
            if (n == null) {
                this.waiter.waitForTimeout(this, 0L);
            }
            else if (n > 0L) {
                for (long n2 = System.currentTimeMillis(), n3 = n + n2; !this.isDone() && n2 < n3; n2 = System.currentTimeMillis()) {
                    this.waiter.waitForTimeout(this, n3 - n2);
                }
            }
            if (Thread.interrupted()) {
                throw new InterruptedException();
            }
            if (this.loadFailed) {
                throw new ExecutionException(this.exception);
            }
            if (this.isCancelled) {
                throw new CancellationException();
            }
            if (this.resultReceived) {
                return this.resource;
            }
            throw new TimeoutException();
        }
    }
    
    @Override
    public boolean cancel(final boolean b) {
        synchronized (this) {
            if (this.isDone()) {
                return false;
            }
            this.isCancelled = true;
            this.waiter.notifyAll(this);
            Request request = null;
            if (b) {
                request = this.request;
                this.request = null;
            }
            monitorexit(this);
            if (request != null) {
                request.clear();
            }
            return true;
        }
    }
    
    @Override
    public R get() throws InterruptedException, ExecutionException {
        try {
            return this.doGet(null);
        }
        catch (final TimeoutException detailMessage) {
            throw new AssertionError((Object)detailMessage);
        }
    }
    
    @Override
    public R get(final long duration, final TimeUnit timeUnit) throws InterruptedException, ExecutionException, TimeoutException {
        return this.doGet(timeUnit.toMillis(duration));
    }
    
    @Override
    public Request getRequest() {
        synchronized (this) {
            return this.request;
        }
    }
    
    @Override
    public void getSize(final SizeReadyCallback sizeReadyCallback) {
        sizeReadyCallback.onSizeReady(this.width, this.height);
    }
    
    @Override
    public boolean isCancelled() {
        synchronized (this) {
            return this.isCancelled;
        }
    }
    
    @Override
    public boolean isDone() {
        synchronized (this) {
            return this.isCancelled || this.resultReceived || this.loadFailed;
        }
    }
    
    @Override
    public void onDestroy() {
    }
    
    @Override
    public void onLoadCleared(final Drawable drawable) {
    }
    
    @Override
    public void onLoadFailed(final Drawable drawable) {
        monitorenter(this);
        monitorexit(this);
    }
    
    @Override
    public boolean onLoadFailed(final GlideException exception, final Object o, final Target<R> target, final boolean b) {
        synchronized (this) {
            this.loadFailed = true;
            this.exception = exception;
            this.waiter.notifyAll(this);
            return false;
        }
    }
    
    @Override
    public void onLoadStarted(final Drawable drawable) {
    }
    
    @Override
    public void onResourceReady(final R r, final Transition<? super R> transition) {
        monitorenter(this);
        monitorexit(this);
    }
    
    @Override
    public boolean onResourceReady(final R resource, final Object o, final Target<R> target, final DataSource dataSource, final boolean b) {
        synchronized (this) {
            this.resultReceived = true;
            this.resource = resource;
            this.waiter.notifyAll(this);
            return false;
        }
    }
    
    @Override
    public void onStart() {
    }
    
    @Override
    public void onStop() {
    }
    
    @Override
    public void removeCallback(final SizeReadyCallback sizeReadyCallback) {
    }
    
    @Override
    public void setRequest(final Request request) {
        synchronized (this) {
            this.request = request;
        }
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append(super.toString());
        sb.append("[status=");
        final String string = sb.toString();
        synchronized (this) {
            final boolean isCancelled = this.isCancelled;
            Object request = null;
            String s;
            if (isCancelled) {
                s = "CANCELLED";
            }
            else if (this.loadFailed) {
                s = "FAILURE";
            }
            else if (this.resultReceived) {
                s = "SUCCESS";
            }
            else {
                s = "PENDING";
                request = this.request;
            }
            monitorexit(this);
            if (request != null) {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append(string);
                sb2.append(s);
                sb2.append(", request=[");
                sb2.append(request);
                sb2.append("]]");
                return sb2.toString();
            }
            final StringBuilder sb3 = new StringBuilder();
            sb3.append(string);
            sb3.append(s);
            sb3.append("]");
            return sb3.toString();
        }
    }
    
    static class Waiter
    {
        void notifyAll(final Object o) {
            o.notifyAll();
        }
        
        void waitForTimeout(final Object o, final long n) throws InterruptedException {
            o.wait(n);
        }
    }
}
